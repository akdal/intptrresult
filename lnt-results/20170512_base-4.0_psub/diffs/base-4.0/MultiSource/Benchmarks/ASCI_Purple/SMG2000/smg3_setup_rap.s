	.text
	.file	"smg3_setup_rap.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI0_1:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
.LCPI0_2:
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
.LCPI0_3:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
.LCPI0_4:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
.LCPI0_5:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
.LCPI0_6:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
.LCPI0_7:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
.LCPI0_8:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI0_9:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
	.long	4294967295              # 0xffffffff
.LCPI0_10:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_11:
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
.LCPI0_12:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI0_13:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
.LCPI0_14:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
.LCPI0_15:
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_16:
	.long	4294967295              # 0xffffffff
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
.LCPI0_17:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	4294967295              # 0xffffffff
.LCPI0_18:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	hypre_SMG3CreateRAPOp
	.p2align	4, 0x90
	.type	hypre_SMG3CreateRAPOp,@function
hypre_SMG3CreateRAPOp:                  # @hypre_SMG3CreateRAPOp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 64
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %r15
	movq	.Lhypre_SMG3CreateRAPOp.RAP_num_ghost+16(%rip), %rax
	movq	%rax, 16(%rsp)
	movdqa	.Lhypre_SMG3CreateRAPOp.RAP_num_ghost(%rip), %xmm0
	movdqa	%xmm0, (%rsp)
	movq	24(%r15), %rax
	movl	8(%rax), %eax
	cmpl	$0, 72(%r15)
	je	.LBB0_1
# BB#8:
	cmpl	$16, %eax
	jge	.LBB0_10
# BB#9:                                 # %.loopexit.loopexit184221
	movl	$8, %ebp
	movl	$8, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movaps	.LCPI0_9(%rip), %xmm0   # xmm0 = [0,4294967295,4294967295,4294967295]
	movups	%xmm0, (%rax)
	movaps	.LCPI0_10(%rip), %xmm0  # xmm0 = [0,4294967295,0,0]
	movups	%xmm0, 16(%rax)
	movaps	.LCPI0_11(%rip), %xmm0  # xmm0 = [4294967295,1,0,4294967295]
	movups	%xmm0, 32(%rax)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [0,1,4294967295,0]
	movups	%xmm0, 48(%rax)
	movdqa	.LCPI0_12(%rip), %xmm0  # xmm0 = [4294967295,0,4294967295,0]
	movdqu	%xmm0, 64(%rax)
	movq	$0, 80(%rax)
	movl	$0, 88(%rax)
	movq	%rax, %rcx
	addq	$92, %rcx
	xorl	%edx, %edx
	jmp	.LBB0_3
.LBB0_1:
	cmpl	$16, %eax
	jge	.LBB0_2
# BB#7:                                 # %.loopexit.loopexit261
	movl	$15, %ebp
	movl	$15, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	movq	%rcx, (%rax)
	movq	$-1, 8(%rax)
	movq	%rcx, 16(%rax)
	movq	$0, 24(%rax)
	movabsq	$8589934591, %rdx       # imm = 0x1FFFFFFFF
	movq	%rdx, 32(%rax)
	movq	%rcx, 40(%rax)
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	movq	%rdx, 48(%rax)
	movl	$4294967295, %esi       # imm = 0xFFFFFFFF
	movq	%rsi, 56(%rax)
	movq	%rsi, 64(%rax)
	movq	%rsi, 72(%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 80(%rax)
	movq	$1, 96(%rax)
	movq	$0, 104(%rax)
	movq	$1, 112(%rax)
	movq	%rcx, 120(%rax)
	movabsq	$-4294967295, %rcx      # imm = 0xFFFFFFFF00000001
	movq	%rcx, 128(%rax)
	movq	%rdx, 136(%rax)
	movq	$0, 144(%rax)
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 152(%rax)
	movq	%rdx, 160(%rax)
	movq	%rdx, 168(%rax)
	movl	$1, 176(%rax)
	jmp	.LBB0_4
.LBB0_10:                               # %.us-lcssa169.us.1
	movl	$14, %ebp
	movl	$14, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [4294967295,4294967295,4294967295,0]
	movups	%xmm0, (%rax)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [4294967295,4294967295,1,4294967295]
	movups	%xmm0, 16(%rax)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [4294967295,4294967295,0,4294967295]
	movups	%xmm0, 32(%rax)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [0,0,4294967295,1]
	movups	%xmm0, 48(%rax)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [0,4294967295,4294967295,1]
	movups	%xmm0, 64(%rax)
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [4294967295,0,1,4294967295]
	movups	%xmm0, 80(%rax)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [1,1,4294967295,4294967295]
	movups	%xmm0, 96(%rax)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [4294967295,0,0,4294967295]
	movups	%xmm0, 112(%rax)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [0,1,4294967295,0]
	movups	%xmm0, 128(%rax)
	movl	$-1, %ecx
	movd	%ecx, %xmm0
	movdqu	%xmm0, 144(%rax)
	movl	$0, 160(%rax)
	movq	%rax, %rcx
	addq	$164, %rcx
	xorl	%edx, %edx
	jmp	.LBB0_3
.LBB0_2:                                # %.preheader133
	movl	$27, %ebp
	movl	$27, %edi
	movl	$12, %esi
	callq	hypre_CAlloc
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [4294967295,4294967295,4294967295,0]
	movups	%xmm0, (%rax)
	movaps	.LCPI0_1(%rip), %xmm0   # xmm0 = [4294967295,4294967295,1,4294967295]
	movups	%xmm0, 16(%rax)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [4294967295,4294967295,0,4294967295]
	movups	%xmm0, 32(%rax)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [0,0,4294967295,1]
	movups	%xmm0, 48(%rax)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [0,4294967295,4294967295,1]
	movups	%xmm0, 64(%rax)
	movaps	.LCPI0_5(%rip), %xmm1   # xmm1 = [4294967295,0,1,4294967295]
	movups	%xmm1, 80(%rax)
	movaps	.LCPI0_6(%rip), %xmm1   # xmm1 = [1,1,4294967295,4294967295]
	movups	%xmm1, 96(%rax)
	movaps	.LCPI0_7(%rip), %xmm1   # xmm1 = [4294967295,0,0,4294967295]
	movups	%xmm1, 112(%rax)
	movaps	.LCPI0_8(%rip), %xmm1   # xmm1 = [0,1,4294967295,0]
	movups	%xmm1, 128(%rax)
	movl	$-1, 144(%rax)
	xorps	%xmm1, %xmm1
	movups	%xmm1, 148(%rax)
	movl	$0, 164(%rax)
	movaps	.LCPI0_13(%rip), %xmm1  # xmm1 = [1,0,0,4294967295]
	movups	%xmm1, 168(%rax)
	movaps	.LCPI0_14(%rip), %xmm1  # xmm1 = [1,0,0,1]
	movups	%xmm1, 184(%rax)
	movl	$0, 200(%rax)
	movl	$1, 204(%rax)
	movl	$1, 208(%rax)
	movups	%xmm0, 212(%rax)
	movaps	.LCPI0_15(%rip), %xmm0  # xmm0 = [0,4294967295,1,1]
	movups	%xmm0, 228(%rax)
	movaps	.LCPI0_16(%rip), %xmm0  # xmm0 = [4294967295,1,4294967295,0]
	movups	%xmm0, 244(%rax)
	movups	%xmm1, 260(%rax)
	movaps	.LCPI0_17(%rip), %xmm0  # xmm0 = [1,0,1,4294967295]
	movups	%xmm0, 276(%rax)
	movdqa	.LCPI0_18(%rip), %xmm0  # xmm0 = [1,1,0,1]
	movdqu	%xmm0, 292(%rax)
	movabsq	$4294967297, %rcx       # imm = 0x100000001
	movq	%rcx, 308(%rax)
	movl	$1, 316(%rax)
	movq	%rax, %rcx
	addq	$320, %rcx              # imm = 0x140
	movl	$1, %edx
.LBB0_3:                                # %.loopexit.sink.split
	movl	%edx, (%rcx)
.LBB0_4:                                # %.loopexit
	movl	$3, %edi
	movl	%ebp, %esi
	movq	%rax, %rdx
	callq	hypre_StructStencilCreate
	movq	%rax, %rbx
	movl	(%r15), %edi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	hypre_StructMatrixCreate
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	hypre_StructStencilDestroy
	movl	72(%r15), %eax
	movl	%eax, 72(%rbp)
	cmpl	$0, 72(%r15)
	je	.LBB0_6
# BB#5:
	movl	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movl	$0, 20(%rsp)
.LBB0_6:
	movq	%rsp, %rsi
	movq	%rbp, %rdi
	callq	hypre_StructMatrixSetNumGhost
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	hypre_SMG3CreateRAPOp, .Lfunc_end0-hypre_SMG3CreateRAPOp
	.cfi_endproc

	.globl	hypre_SMG3BuildRAPSym
	.p2align	4, 0x90
	.type	hypre_SMG3BuildRAPSym,@function
hypre_SMG3BuildRAPSym:                  # @hypre_SMG3BuildRAPSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$1496, %rsp             # imm = 0x5D8
.Lcfi15:
	.cfi_def_cfa_offset 1552
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%r9, 736(%rsp)          # 8-byte Spill
	movq	%r8, 1360(%rsp)         # 8-byte Spill
	movq	%rcx, %r14
	movq	%rdx, 1304(%rsp)        # 8-byte Spill
	movq	%rsi, 1352(%rsp)        # 8-byte Spill
	movq	%rdi, %r15
	movq	8(%r14), %rax
	movq	8(%rax), %rsi
	cmpl	$0, 8(%rsi)
	jle	.LBB1_65
# BB#1:                                 # %.preheader2760.lr.ph
	movq	8(%r15), %rcx
	movq	24(%r15), %rdx
	movl	8(%rdx), %ebp
	movq	16(%rcx), %rdx
	movq	16(%rax), %rax
	movq	%rax, 1320(%rsp)        # 8-byte Spill
	movl	$0, %edi
	movl	$0, %ecx
                                        # implicit-def: %RAX
	movq	%rax, 464(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 456(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 320(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 448(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 176(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 440(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 432(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 424(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 992(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 984(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1184(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1176(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 896(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 888(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1168(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1160(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1272(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1264(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1256(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1248(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1240(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1232(%rsp)        # 8-byte Spill
	movq	%r14, 1296(%rsp)        # 8-byte Spill
	movq	%r15, 1344(%rsp)        # 8-byte Spill
	movq	%rsi, 1336(%rsp)        # 8-byte Spill
	movl	%ebp, 860(%rsp)         # 4-byte Spill
	movq	%rdx, 1328(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader2760
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_3 Depth 2
                                        #     Child Loop BB1_55 Depth 2
                                        #       Child Loop BB1_58 Depth 3
                                        #         Child Loop BB1_60 Depth 4
                                        #     Child Loop BB1_19 Depth 2
                                        #       Child Loop BB1_22 Depth 3
                                        #         Child Loop BB1_24 Depth 4
                                        #     Child Loop BB1_31 Depth 2
                                        #       Child Loop BB1_34 Depth 3
                                        #         Child Loop BB1_36 Depth 4
                                        #     Child Loop BB1_43 Depth 2
                                        #       Child Loop BB1_46 Depth 3
                                        #         Child Loop BB1_48 Depth 4
	movq	1320(%rsp), %rax        # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movslq	%ecx, %rcx
	leaq	-1(%rcx), %rbx
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %r13
	.p2align	4, 0x90
.LBB1_3:                                #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$24, %r13
	cmpl	%eax, 4(%rdx,%rbx,4)
	leaq	1(%rbx), %rbx
	jne	.LBB1_3
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	(%rsi), %rcx
	movq	%rdi, 720(%rsp)         # 8-byte Spill
	leaq	(,%rdi,8), %rax
	leaq	(%rax,%rax,2), %rax
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rax), %rdi
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	1360(%rsp), %rsi        # 8-byte Reload
	movq	736(%rsp), %rdx         # 8-byte Reload
	leaq	412(%rsp), %rcx
	callq	hypre_StructMapCoarseToFine
	movq	40(%r15), %rax
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	1352(%rsp), %rbp        # 8-byte Reload
	movq	40(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	1304(%rsp), %rax        # 8-byte Reload
	movq	40(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	40(%r14), %rax
	movq	(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%rbx, %r14
	leaq	4(%rsp), %r12
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movq	%r12, %rbp
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	1304(%rsp), %rbx        # 8-byte Reload
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 976(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 968(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	movq	%rbx, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %r12       # imm = 0x100000000
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r15, %r12
	movq	%r15, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movl	860(%rsp), %r15d        # 4-byte Reload
	cmpl	$8, %r15d
	movq	%r14, 1312(%rsp)        # 8-byte Spill
	jl	.LBB1_5
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%r12, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, %rbx
	movq	%rbx, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%r12, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 456(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r12, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r12, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movq	%rbx, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r12, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r12, %rdi
	movl	%r14d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 440(%rsp)         # 8-byte Spill
	cmpl	$16, %r15d
	movq	%rbp, %rax
	movq	%r14, %rbp
	jl	.LBB1_7
# BB#8:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	$-1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movq	%rax, %r14
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 432(%rsp)         # 8-byte Spill
	movabsq	$-4294967295, %rbx      # imm = 0xFFFFFFFF00000001
	movq	%rbx, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r12, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movq	%r12, %rbp
	movabsq	$8589934591, %r12       # imm = 0x1FFFFFFFF
	movq	1312(%rsp), %rbx        # 8-byte Reload
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 992(%rsp)         # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 984(%rsp)         # 8-byte Spill
	cmpl	$20, %r15d
	movq	%r14, %r15
	jl	.LBB1_9
# BB#10:                                #   in Loop: Header=BB1_2 Depth=1
	movq	$-1, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1184(%rsp)        # 8-byte Spill
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, %r14
	movq	%r14, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1176(%rsp)        # 8-byte Spill
	movq	$-1, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 896(%rsp)         # 8-byte Spill
	movq	%r14, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 888(%rsp)         # 8-byte Spill
	movq	%r12, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1168(%rsp)        # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rbx
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1160(%rsp)        # 8-byte Spill
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%rbp, %rbx
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_11
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, %rbx
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_11
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	movq	%r15, %rbx
	movb	$1, %al
	movl	%eax, 16(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_11:                               # %.thread2692
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	1296(%rsp), %rbp        # 8-byte Reload
	movq	%rbp, %rdi
	movq	720(%rsp), %r12         # 8-byte Reload
	movl	%r12d, %esi
	movq	%rbx, %r15
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 960(%rsp)         # 8-byte Spill
	movl	$4294967295, %r14d      # imm = 0xFFFFFFFF
	movq	%r14, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 952(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	movq	%rbx, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 944(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 936(%rsp)         # 8-byte Spill
	movq	%r14, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 928(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 920(%rsp)         # 8-byte Spill
	movq	%rbx, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 912(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rax       # imm = 0x100000000
	movq	%rax, %rbx
	movq	%rbx, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 904(%rsp)         # 8-byte Spill
	cmpb	$0, 16(%rsp)            # 1-byte Folded Reload
	movq	%rbx, %r14
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_2 Depth=1
	movq	$-1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movq	720(%rsp), %rbx         # 8-byte Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1272(%rsp)        # 8-byte Spill
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, %r12
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1264(%rsp)        # 8-byte Spill
	movq	$-1, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1256(%rsp)        # 8-byte Spill
	movq	%r12, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1248(%rsp)        # 8-byte Spill
	movabsq	$8589934591, %rax       # imm = 0x1FFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1240(%rsp)        # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1232(%rsp)        # 8-byte Spill
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=1
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	-12(%rbx,%r13), %r10d
	movl	-8(%rbx,%r13), %edx
	movl	-24(%rbx,%r13), %esi
	movl	%r10d, %eax
	subl	%esi, %eax
	incl	%eax
	cmpl	%esi, %r10d
	movl	-20(%rbx,%r13), %esi
	movl	$0, %r8d
	cmovsl	%r8d, %eax
	movl	%edx, %ecx
	subl	%esi, %ecx
	incl	%ecx
	cmpl	%esi, %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	-12(%rsi,%r13), %ebp
	movl	-8(%rsi,%r13), %r9d
	movl	-24(%rsi,%r13), %edi
	cmovsl	%r8d, %ecx
	movl	%ebp, %edx
	subl	%edi, %edx
	incl	%edx
	cmpl	%edi, %ebp
	movl	-20(%rsi,%r13), %edi
	cmovsl	%r8d, %edx
	movl	%r9d, %ebp
	subl	%edi, %ebp
	incl	%ebp
	cmpl	%edi, %r9d
	cmovsl	%r8d, %ebp
	movq	%r14, 4(%rsp)
	movl	$0, 12(%rsp)
	movl	-24(%rbx,%r13), %esi
	movl	%r10d, %edi
	subl	%esi, %edi
	incl	%edi
	cmpl	%esi, %r10d
	cmovsl	%r8d, %edi
	imull	%edx, %ebp
	imull	%eax, %ecx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	leaq	(%rax,%rbx), %r14
	movl	$1, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movl	860(%rsp), %eax         # 4-byte Reload
	cmpl	$19, %eax
	movl	%ebp, 152(%rsp)         # 4-byte Spill
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movl	%edi, 136(%rsp)         # 4-byte Spill
	je	.LBB1_40
# BB#14:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$15, %eax
	je	.LBB1_28
# BB#15:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$7, %eax
	jne	.LBB1_52
# BB#16:                                #   in Loop: Header=BB1_2 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	212(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r14), %r8d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	4(%rcx,%rbx), %r9d
	movl	12(%rcx,%rbx), %eax
	movl	%eax, %r12d
	subl	%r8d, %r12d
	incl	%r12d
	cmpl	%r8d, %eax
	movl	16(%rcx,%rbx), %eax
	movl	$0, %ebx
	cmovsl	%ebx, %r12d
	movl	%eax, %edx
	subl	%r9d, %edx
	incl	%edx
	cmpl	%r9d, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %r11d
	movl	-20(%rcx,%r13), %r10d
	movl	-12(%rcx,%r13), %eax
	cmovsl	%ebx, %edx
	movl	%edx, 264(%rsp)         # 4-byte Spill
	movl	%eax, %edx
	subl	%r11d, %edx
	incl	%edx
	cmpl	%r11d, %eax
	movl	-8(%rcx,%r13), %eax
	cmovsl	%ebx, %edx
	movl	%edx, 256(%rsp)         # 4-byte Spill
	movl	%eax, %r14d
	subl	%r10d, %r14d
	incl	%r14d
	cmpl	%r10d, %eax
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	-24(%rax,%r13), %r15d
	movl	-20(%rax,%r13), %ebp
	movl	-12(%rax,%r13), %ecx
	cmovsl	%ebx, %r14d
	movl	%ecx, %edx
	subl	%r15d, %edx
	incl	%edx
	cmpl	%r15d, %ecx
	movl	-8(%rax,%r13), %ecx
	cmovsl	%ebx, %edx
	movl	%edx, 160(%rsp)         # 4-byte Spill
	movl	%ecx, %esi
	subl	%ebp, %esi
	incl	%esi
	cmpl	%ebp, %ecx
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	-24(%rax,%r13), %edx
	movl	-12(%rax,%r13), %ecx
	cmovsl	%ebx, %esi
	movl	%esi, 248(%rsp)         # 4-byte Spill
	movl	%ecx, %esi
	subl	%edx, %esi
	incl	%esi
	cmpl	%edx, %ecx
	movl	-20(%rax,%r13), %ecx
	cmovsl	%ebx, %esi
	movl	%esi, 192(%rsp)         # 4-byte Spill
	movl	-8(%rax,%r13), %esi
	movl	%esi, %eax
	subl	%ecx, %eax
	incl	%eax
	cmpl	%ecx, %esi
	cmovsl	%ebx, %eax
	movl	%eax, 240(%rsp)         # 4-byte Spill
	movl	212(%rsp), %eax
	movl	216(%rsp), %edi
	cmpl	%eax, %edi
	movl	%eax, %esi
	movl	%edi, 304(%rsp)         # 4-byte Spill
	cmovgel	%edi, %esi
	movl	220(%rsp), %edi
	cmpl	%esi, %edi
	movl	%edi, 392(%rsp)         # 4-byte Spill
	cmovgel	%edi, %esi
	testl	%esi, %esi
	jle	.LBB1_64
# BB#17:                                # %.lr.ph2848
                                        #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, 392(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_64
# BB#18:                                # %.preheader2754.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	104(%rsp), %rbx         # 8-byte Reload
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rdi         # 8-byte Reload
	movl	8(%rbx,%rdi), %esi
	movl	%r10d, 96(%rsp)         # 4-byte Spill
	movl	%ebp, 88(%rsp)          # 4-byte Spill
	movl	%esi, %r10d
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r10d
	movl	%r9d, 80(%rsp)          # 4-byte Spill
	movl	%r8d, 72(%rsp)          # 4-byte Spill
	movl	%esi, %r8d
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r8d
	movl	%r11d, 56(%rsp)         # 4-byte Spill
	movl	420(%rsp), %r9d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r9d
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ebp
	movl	%ebp, %eax
	movl	%ebp, %r11d
	subl	%edx, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	subl	8(%rax,%rdi), %esi
	movl	4(%rbx,%rdi), %edx
	movl	%edx, %eax
	subl	%ecx, %eax
	movl	240(%rsp), %ebp         # 4-byte Reload
	imull	%ebp, %r10d
	addl	%eax, %r10d
	movl	%r11d, %ecx
	movl	%ecx, %eax
	subl	%r15d, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%edx, %eax
	subl	88(%rsp), %eax          # 4-byte Folded Reload
	movl	248(%rsp), %r11d        # 4-byte Reload
	imull	%r11d, %r8d
	addl	%eax, %r8d
	movl	412(%rsp), %eax
	subl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	416(%rsp), %eax
	subl	96(%rsp), %eax          # 4-byte Folded Reload
	imull	%r14d, %r9d
	addl	%eax, %r9d
	subl	72(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movl	192(%rsp), %eax         # 4-byte Reload
	imull	%eax, %r10d
	movl	%eax, %r13d
	imull	160(%rsp), %r8d         # 4-byte Folded Reload
	movl	256(%rsp), %ebx         # 4-byte Reload
	imull	%ebx, %r9d
	subl	80(%rsp), %edx          # 4-byte Folded Reload
	movl	264(%rsp), %ecx         # 4-byte Reload
	imull	%ecx, %esi
	addl	%edx, %esi
	imull	%r12d, %esi
	movq	736(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rdx
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movl	4(%rax), %r15d
	imull	%ebx, %r15d
	imull	%r14d, %ebx
	movl	%r8d, %edi
	movl	%r10d, %edx
	imull	8(%rax), %ebx
	movl	304(%rsp), %r10d        # 4-byte Reload
	subl	%r10d, %ebp
	imull	%r13d, %ebp
	movl	%ebp, 240(%rsp)         # 4-byte Spill
	subl	%r10d, %r11d
	movl	160(%rsp), %ebp         # 4-byte Reload
	imull	%ebp, %r11d
	movl	%r11d, 248(%rsp)        # 4-byte Spill
	movl	%r10d, %eax
	imull	%r15d, %eax
	subl	%eax, %ebx
	movl	%ebx, 256(%rsp)         # 4-byte Spill
	subl	%r10d, %ecx
	imull	%r12d, %ecx
	movl	%ecx, 264(%rsp)         # 4-byte Spill
	movq	200(%rsp), %r8          # 8-byte Reload
	movl	%r8d, %eax
	imull	16(%rsp), %eax          # 4-byte Folded Reload
	subl	%eax, %r15d
	movl	%r15d, 624(%rsp)        # 4-byte Spill
	movl	%ebp, %eax
	movl	%ebp, %r14d
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 616(%rsp)         # 4-byte Spill
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	movslq	144(%rsp), %r15         # 4-byte Folded Reload
	addl	24(%rsp), %esi          # 4-byte Folded Reload
	movq	%r9, %rbx
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %ecx
	incq	%rcx
	addl	40(%rsp), %ebx          # 4-byte Folded Reload
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	imulq	%r8, %rcx
	movq	%rcx, 600(%rsp)         # 8-byte Spill
	addl	32(%rsp), %edi          # 4-byte Folded Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	addl	48(%rsp), %edx          # 4-byte Folded Reload
	movl	$0, 400(%rsp)           # 4-byte Folded Spill
	testl	%ebp, %ebp
	movl	$0, %r9d
	cmovnsl	%ebp, %r9d
	shlq	$3, %rax
	shlq	$3, %r15
	movq	%rax, %r10
	subq	%r15, %r10
	movq	376(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rcx
	subq	%r15, %rcx
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	negq	%r15
	subq	%rax, %r15
	movq	368(%rsp), %rcx         # 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, 576(%rsp)         # 8-byte Spill
	movq	%r11, %rcx
	subq	%rax, %rcx
	movq	%rcx, 568(%rsp)         # 8-byte Spill
	addl	%r9d, %r14d
	movl	%r14d, 160(%rsp)        # 4-byte Spill
	addl	%r9d, %r13d
	movl	%r13d, 192(%rsp)        # 4-byte Spill
	addl	%r12d, %r9d
	movslq	152(%rsp), %r12         # 4-byte Folded Reload
	leaq	(,%r12,8), %r8
	movq	120(%rsp), %rax         # 8-byte Reload
	subq	%r8, %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	subq	%r8, %rcx
	movq	%rcx, 280(%rsp)         # 8-byte Spill
	movq	360(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rcx
	subq	%r8, %rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	328(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rcx
	subq	%r8, %rcx
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	subq	%r8, 352(%rsp)          # 8-byte Folded Spill
	subq	%r8, 344(%rsp)          # 8-byte Folded Spill
	movq	336(%rsp), %rcx         # 8-byte Reload
	subq	%r8, %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	movq	200(%rsp), %r8          # 8-byte Reload
	subl	%ebp, 160(%rsp)         # 4-byte Folded Spill
	subl	%ebp, 192(%rsp)         # 4-byte Folded Spill
	addq	%r11, %r10
	movq	%r10, 592(%rsp)         # 8-byte Spill
	addq	%r11, %r15
	movq	%r15, 184(%rsp)         # 8-byte Spill
	subl	%ebp, %r9d
	movl	%r9d, 608(%rsp)         # 4-byte Spill
	shlq	$3, %r8
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %rax
	movq	%rax, 544(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %rax
	movq	%rax, 536(%rsp)         # 8-byte Spill
	leaq	(%r13,%r12,8), %rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	leaq	(%r14,%r12,8), %rax
	movq	%rax, 520(%rsp)         # 8-byte Spill
	movq	%r8, 200(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_19:                               # %.preheader2754
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_22 Depth 3
                                        #         Child Loop BB1_24 Depth 4
	cmpl	$0, 304(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_20
# BB#21:                                # %.preheader2750.preheader
                                        #   in Loop: Header=BB1_19 Depth=2
	xorl	%ecx, %ecx
	movl	%edi, %eax
	.p2align	4, 0x90
.LBB1_22:                               # %.preheader2750
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_19 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_24 Depth 4
	testl	%ebp, %ebp
	jle	.LBB1_26
# BB#23:                                # %.lr.ph2824.preheader
                                        #   in Loop: Header=BB1_22 Depth=3
	movl	%ecx, 632(%rsp)         # 4-byte Spill
	movl	%eax, 640(%rsp)         # 4-byte Spill
	movl	%edi, 656(%rsp)         # 4-byte Spill
	movslq	%edi, %rax
	movq	968(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r11
	movq	976(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r15
	movl	%edx, 664(%rsp)         # 4-byte Spill
	movslq	%edx, %r12
	shlq	$3, %r12
	movl	%esi, 672(%rsp)         # 4-byte Spill
	movslq	%esi, %r9
	shlq	$3, %r9
	movslq	%ebx, %rdi
	movq	592(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	584(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12), %rax
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r12), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	576(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r12), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	568(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r12), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	368(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r12), %r10
	addq	376(%rsp), %r12         # 8-byte Folded Reload
	movq	912(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	928(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	936(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	920(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	904(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	944(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	952(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	addq	960(%rsp), %r9          # 8-byte Folded Reload
	movq	600(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi), %rcx
	movq	%rcx, 648(%rsp)         # 8-byte Spill
	movq	288(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	280(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rsi
	movq	272(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdx
	movq	560(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	352(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	344(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	552(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	328(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %r13
	movq	120(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 312(%rsp)         # 8-byte Spill
	movq	360(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 712(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 704(%rsp)         # 8-byte Spill
	movq	544(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 696(%rsp)         # 8-byte Spill
	movq	536(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 688(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,8), %rbp
	movq	%rbp, 680(%rsp)         # 8-byte Spill
	movq	520(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rdi,8), %r8
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_24:                               # %.lr.ph2824
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_19 Depth=2
                                        #       Parent Loop BB1_22 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r11,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rcx,%rbx), %xmm0
	movq	32(%rsp), %rbp          # 8-byte Reload
	mulsd	(%rbp,%rdi,8), %xmm0
	movq	112(%rsp), %rbp         # 8-byte Reload
	movsd	%xmm0, (%rbp,%rdi,8)
	movsd	(%r11,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rsi,%rbx), %xmm0
	mulsd	-8(%rax,%rdi,8), %xmm0
	movq	64(%rsp), %rbp          # 8-byte Reload
	movsd	%xmm0, (%rbp,%rdi,8)
	movsd	(%rax,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r13,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r11,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rdx,%rbx), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	96(%rsp), %rbp          # 8-byte Reload
	mulsd	(%rbp,%rbx), %xmm2
	addsd	%xmm3, %xmm2
	movq	56(%rsp), %rbp          # 8-byte Reload
	movsd	%xmm2, (%rbp,%rdi,8)
	movsd	(%r11,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	88(%rsp), %rbp          # 8-byte Reload
	mulsd	(%rbp,%rbx), %xmm0
	mulsd	8(%rax,%rdi,8), %xmm0
	movq	104(%rsp), %rbp         # 8-byte Reload
	movsd	%xmm0, (%rbp,%rdi,8)
	movsd	(%r11,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movq	80(%rsp), %rbp          # 8-byte Reload
	mulsd	(%rbp,%rbx), %xmm0
	movq	48(%rsp), %r14          # 8-byte Reload
	mulsd	(%r14,%rdi,8), %xmm0
	movq	152(%rsp), %rbp         # 8-byte Reload
	movsd	%xmm0, (%rbp,%rdi,8)
	movsd	(%r11,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rcx,%rbx), %xmm0
	movq	40(%rsp), %rbp          # 8-byte Reload
	mulsd	(%rbp,%rdi,8), %xmm0
	movq	168(%rsp), %rbp         # 8-byte Reload
	addsd	(%rbp,%rbx), %xmm0
	movsd	(%r15,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movq	696(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rbx), %xmm1
	movq	24(%rsp), %rbp          # 8-byte Reload
	mulsd	(%rbp,%rdi,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	144(%rsp), %rbp         # 8-byte Reload
	movsd	%xmm1, (%rbp,%rdi,8)
	movsd	(%r11,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rsi,%rbx), %xmm0
	mulsd	-8(%r10,%rdi,8), %xmm0
	movq	312(%rsp), %rbp         # 8-byte Reload
	addsd	(%rbp,%rbx), %xmm0
	movsd	(%r15,%rdi,8), %xmm1    # xmm1 = mem[0],zero
	movq	688(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rbx), %xmm1
	mulsd	-8(%r12,%rdi,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	136(%rsp), %rbp         # 8-byte Reload
	movsd	%xmm1, (%rbp,%rdi,8)
	movsd	(%r11,%rdi,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdx,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r10,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	712(%rsp), %rbp         # 8-byte Reload
	addsd	(%rbp,%rbx), %xmm1
	movsd	(%r15,%rdi,8), %xmm3    # xmm3 = mem[0],zero
	movq	680(%rsp), %rbp         # 8-byte Reload
	movsd	(%rbp,%rbx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	(%r12,%rdi,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	72(%rsp), %rbp          # 8-byte Reload
	mulsd	(%rbp,%rbx), %xmm0
	addsd	%xmm4, %xmm0
	mulsd	(%r8,%rbx), %xmm3
	addsd	%xmm0, %xmm3
	mulsd	(%r13,%rbx), %xmm2
	addsd	%xmm3, %xmm2
	movq	704(%rsp), %rbp         # 8-byte Reload
	mulsd	(%rbp,%rbx), %xmm5
	addsd	%xmm2, %xmm5
	movsd	%xmm5, (%r9,%rdi,8)
	incq	%rdi
	addq	200(%rsp), %rbx         # 8-byte Folded Reload
	cmpl	%edi, 16(%rsp)          # 4-byte Folded Reload
	jne	.LBB1_24
# BB#25:                                # %._crit_edge2825.loopexit
                                        #   in Loop: Header=BB1_22 Depth=3
	movl	640(%rsp), %eax         # 4-byte Reload
	addl	296(%rsp), %eax         # 4-byte Folded Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	672(%rsp), %esi         # 4-byte Reload
	movl	664(%rsp), %edx         # 4-byte Reload
	movl	656(%rsp), %edi         # 4-byte Reload
	movq	648(%rsp), %rbx         # 8-byte Reload
	movl	632(%rsp), %ecx         # 4-byte Reload
.LBB1_26:                               # %._crit_edge2825
                                        #   in Loop: Header=BB1_22 Depth=3
	addl	192(%rsp), %edx         # 4-byte Folded Reload
	addl	616(%rsp), %eax         # 4-byte Folded Reload
	addl	624(%rsp), %ebx         # 4-byte Folded Reload
	addl	608(%rsp), %esi         # 4-byte Folded Reload
	incl	%ecx
	addl	160(%rsp), %edi         # 4-byte Folded Reload
	cmpl	304(%rsp), %ecx         # 4-byte Folded Reload
	jne	.LBB1_22
	jmp	.LBB1_27
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_19 Depth=2
	movl	%edi, %eax
.LBB1_27:                               # %._crit_edge2835
                                        #   in Loop: Header=BB1_19 Depth=2
	addl	240(%rsp), %edx         # 4-byte Folded Reload
	addl	248(%rsp), %eax         # 4-byte Folded Reload
	addl	256(%rsp), %ebx         # 4-byte Folded Reload
	addl	264(%rsp), %esi         # 4-byte Folded Reload
	movl	400(%rsp), %edi         # 4-byte Reload
	incl	%edi
	movl	%edi, 400(%rsp)         # 4-byte Spill
	cmpl	392(%rsp), %edi         # 4-byte Folded Reload
	movl	%eax, %edi
	jne	.LBB1_19
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_28:                               #   in Loop: Header=BB1_2 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	212(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r14), %r9d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%rbx), %esi
	movl	12(%rax,%rbx), %ecx
	movl	%ecx, %r10d
	subl	%r9d, %r10d
	incl	%r10d
	cmpl	%r9d, %ecx
	movl	16(%rax,%rbx), %edx
	movl	$0, %eax
	cmovsl	%eax, %r10d
	movl	%edx, %ebx
	subl	%esi, %ebx
	incl	%ebx
	movl	%esi, 72(%rsp)          # 4-byte Spill
	cmpl	%esi, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %esi
	movl	-20(%rcx,%r13), %edi
	movl	-12(%rcx,%r13), %edx
	cmovsl	%eax, %ebx
	movl	%edx, %r14d
	subl	%esi, %r14d
	incl	%r14d
	movl	%esi, 48(%rsp)          # 4-byte Spill
	cmpl	%esi, %edx
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %r14d
	movl	%esi, %r12d
	subl	%edi, %r12d
	incl	%r12d
	movl	%edi, 96(%rsp)          # 4-byte Spill
	cmpl	%edi, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebp
	movl	-20(%rcx,%r13), %edx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %r12d
	movl	%esi, %edi
	subl	%ebp, %edi
	incl	%edi
	movl	%ebp, 80(%rsp)          # 4-byte Spill
	cmpl	%ebp, %esi
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %edi
	movl	%edi, 280(%rsp)         # 4-byte Spill
	movl	%esi, %r11d
	subl	%edx, %r11d
	incl	%r11d
	movl	%edx, 88(%rsp)          # 4-byte Spill
	cmpl	%edx, %esi
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebp
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %r11d
	movl	%esi, %edi
	subl	%ebp, %edi
	incl	%edi
	cmpl	%ebp, %esi
	movl	-20(%rcx,%r13), %edx
	cmovsl	%eax, %edi
	movl	%edi, 272(%rsp)         # 4-byte Spill
	movl	-8(%rcx,%r13), %esi
	movl	%esi, %r8d
	subl	%edx, %r8d
	incl	%r8d
	cmpl	%edx, %esi
	cmovsl	%eax, %r8d
	movl	212(%rsp), %r15d
	movl	216(%rsp), %eax
	cmpl	%r15d, %eax
	movl	%r15d, %esi
	movl	%eax, 288(%rsp)         # 4-byte Spill
	cmovgel	%eax, %esi
	movl	220(%rsp), %eax
	cmpl	%esi, %eax
	movl	%eax, 384(%rsp)         # 4-byte Spill
	cmovgel	%eax, %esi
	testl	%esi, %esi
	jle	.LBB1_64
# BB#29:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, 384(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_64
# BB#30:                                # %.preheader2755.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	8(%rax,%rdi), %ecx
	movq	720(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax,2), %rsi
	movl	%ecx, %r15d
	movq	24(%rsp), %rax          # 8-byte Reload
	subl	8(%rax,%rsi,8), %r15d
	movl	%r10d, 168(%rsp)        # 4-byte Spill
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	%r9d, 312(%rsp)         # 4-byte Spill
	movl	420(%rsp), %r9d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r9d
	movl	%ecx, %r10d
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r10d
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %ecx
	movl	%ecx, 296(%rsp)         # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	4(%rax,%rdi), %ecx
	movq	736(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %esi
	imull	%r14d, %esi
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movl	%r14d, %edx
	imull	%r12d, %edx
	imull	8(%rax), %edx
	movl	288(%rsp), %ebx         # 4-byte Reload
	movl	%ebx, %edi
	imull	%esi, %edi
	subl	%edi, %edx
	movl	%edx, 232(%rsp)         # 4-byte Spill
	movslq	(%rax), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movl	%eax, %edi
	imull	16(%rsp), %edi          # 4-byte Folded Reload
	subl	%edi, %esi
	movl	%esi, 560(%rsp)         # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, %edi
	subl	312(%rsp), %edi         # 4-byte Folded Reload
	movl	%ecx, %esi
	subl	72(%rsp), %esi          # 4-byte Folded Reload
	imull	24(%rsp), %r15d         # 4-byte Folded Reload
	addl	%esi, %r15d
	movl	168(%rsp), %r13d        # 4-byte Reload
	imull	%r13d, %r15d
	addl	%edi, %r15d
	movl	412(%rsp), %esi
	subl	48(%rsp), %esi          # 4-byte Folded Reload
	movl	416(%rsp), %edi
	subl	96(%rsp), %edi          # 4-byte Folded Reload
	imull	%r12d, %r9d
	addl	%edi, %r9d
	imull	%r14d, %r9d
	addl	%esi, %r9d
	movl	%eax, %edx
	subl	80(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, %esi
	subl	88(%rsp), %esi          # 4-byte Folded Reload
	imull	%r11d, %r10d
	addl	%esi, %r10d
	movl	280(%rsp), %esi         # 4-byte Reload
	imull	%esi, %r10d
	addl	%edx, %r10d
	subl	%ebp, %eax
	movl	296(%rsp), %edi         # 4-byte Reload
	subl	32(%rsp), %ecx          # 4-byte Folded Reload
	imull	%r8d, %edi
	addl	%ecx, %edi
	subl	%ebx, %r8d
	movl	272(%rsp), %ecx         # 4-byte Reload
	imull	%ecx, %r8d
	movl	%r8d, 224(%rsp)         # 4-byte Spill
	subl	%ebx, %r11d
	imull	%esi, %r11d
	movl	%r11d, 776(%rsp)        # 4-byte Spill
	movl	24(%rsp), %r11d         # 4-byte Reload
	subl	%ebx, %r11d
	movl	%r13d, %r14d
	imull	%r14d, %r11d
	movl	%r11d, 768(%rsp)        # 4-byte Spill
	imull	%ecx, %edi
	addl	%eax, %edi
	movl	%esi, %eax
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 552(%rsp)         # 4-byte Spill
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	movslq	144(%rsp), %rdx         # 4-byte Folded Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	leal	-1(%rbp), %ebp
	incq	%rbp
	movq	%rbp, 536(%rsp)         # 8-byte Spill
	imulq	184(%rsp), %rbp         # 8-byte Folded Reload
	movq	%rbp, 528(%rsp)         # 8-byte Spill
	movl	$0, 728(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	$0, %r8d
	cmovnsl	16(%rsp), %r8d          # 4-byte Folded Reload
	movq	%rax, %r13
	subq	%rdx, %r13
	shlq	$3, %rdx
	movq	376(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rbx
	subq	%rdx, %rbx
	shlq	$3, %rax
	movq	%rbx, 520(%rsp)         # 8-byte Spill
	subq	%rax, %rbx
	movq	%rbx, 400(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	movq	%rbp, %rdx
	subq	%rax, %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	addl	%r8d, %esi
	addl	%r8d, %ecx
	addl	%r14d, %r8d
	movslq	152(%rsp), %rax         # 4-byte Folded Reload
	leaq	(,%rax,8), %rdx
	movq	120(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 248(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 240(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 392(%rsp)         # 8-byte Spill
	movq	320(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rbx
	subq	%rdx, %rbx
	movq	%rbx, 848(%rsp)         # 8-byte Spill
	movq	360(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rbx
	subq	%rdx, %rbx
	movq	%rbx, 840(%rsp)         # 8-byte Spill
	movq	328(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rbx
	subq	%rdx, %rbx
	movq	%rbx, 512(%rsp)         # 8-byte Spill
	subq	%rdx, 352(%rsp)         # 8-byte Folded Spill
	movq	448(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 504(%rsp)         # 8-byte Spill
	subq	%rdx, 344(%rsp)         # 8-byte Folded Spill
	movq	440(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 496(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 832(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 824(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 816(%rsp)         # 8-byte Spill
	subl	16(%rsp), %esi          # 4-byte Folded Reload
	movl	%esi, 280(%rsp)         # 4-byte Spill
	movq	%r9, %rsi
	movq	184(%rsp), %r9          # 8-byte Reload
	leaq	(%rbp,%r13,8), %rbp
	movq	%rbp, 808(%rsp)         # 8-byte Spill
	subl	16(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 272(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %r8d
	movl	%r8d, 544(%rsp)         # 4-byte Spill
	shlq	$3, %r9
	movq	120(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rbp
	movq	%rbp, 488(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rbp
	movq	%rbp, 480(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rax,8), %rbp
	movq	%rbp, 472(%rsp)         # 8-byte Spill
	leaq	(%r14,%rax,8), %rbp
	movq	%rbp, 800(%rsp)         # 8-byte Spill
	leaq	(%r11,%rax,8), %rbp
	movq	%rbp, 792(%rsp)         # 8-byte Spill
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 784(%rsp)         # 8-byte Spill
	movl	%r10d, %eax
	movq	%r9, 184(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_31:                               # %.preheader2755
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_34 Depth 3
                                        #         Child Loop BB1_36 Depth 4
	cmpl	$0, 288(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_32
# BB#33:                                # %.preheader2751.preheader
                                        #   in Loop: Header=BB1_31 Depth=2
	xorl	%ebx, %ebx
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB1_34:                               # %.preheader2751
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_31 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_36 Depth 4
	testl	%ecx, %ecx
	jle	.LBB1_38
# BB#35:                                # %.lr.ph2794.preheader
                                        #   in Loop: Header=BB1_34 Depth=3
	movl	%ebx, 568(%rsp)         # 4-byte Spill
	movl	%ebp, 576(%rsp)         # 4-byte Spill
	movl	%eax, 584(%rsp)         # 4-byte Spill
	cltq
	movq	968(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r13
	movq	976(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r8
	movl	%edi, 296(%rsp)         # 4-byte Spill
	movslq	%edi, %r11
	shlq	$3, %r11
	movl	%r15d, 600(%rsp)        # 4-byte Spill
	movslq	%r15d, %r14
	shlq	$3, %r14
	movslq	%esi, %rcx
	movq	808(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	520(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %r9
	movq	400(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	264(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	368(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %r15
	addq	376(%rsp), %r11         # 8-byte Folded Reload
	movq	912(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	928(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	936(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	920(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	904(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	944(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	952(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	addq	960(%rsp), %r14         # 8-byte Folded Reload
	movq	528(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	%rax, 592(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rbx
	movq	240(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	392(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rdi
	movq	848(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	840(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rsi
	movq	512(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	352(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	504(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	496(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 712(%rsp)         # 8-byte Spill
	movq	832(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 704(%rsp)         # 8-byte Spill
	movq	824(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 696(%rsp)         # 8-byte Spill
	movq	816(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 688(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	320(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r10
	movq	328(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rbp
	movq	448(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 680(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 672(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 664(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 656(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 648(%rsp)         # 8-byte Spill
	movq	360(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 640(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 632(%rsp)         # 8-byte Spill
	movq	488(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 304(%rsp)         # 8-byte Spill
	movq	480(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	472(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	800(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 624(%rsp)         # 8-byte Spill
	movq	792(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 616(%rsp)         # 8-byte Spill
	movq	784(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rcx
	movq	%rcx, 608(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_36:                               # %.lr.ph2794
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_31 Depth=2
                                        #       Parent Loop BB1_34 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	32(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	96(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%rax,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	112(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%r12,8)
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdi,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r9,%r12,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	88(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%r10,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	64(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%r12,8)
	movsd	(%r9,%r12,8), %xmm0     # xmm0 = mem[0],zero
	movsd	(%rbp,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r13,%r12,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rsi,%rcx), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	80(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm2
	addsd	%xmm3, %xmm2
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%r12,8)
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movq	72(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%r9,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	168(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	movq	680(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	104(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%r12,8)
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movq	312(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	712(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	movq	200(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	152(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%r12,8)
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	672(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%rcx), %xmm1
	movsd	(%r8,%r12,8), %xmm3     # xmm3 = mem[0],zero
	movq	304(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rcx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	24(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r12,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%rax,%rcx), %xmm2
	addsd	%xmm4, %xmm2
	movq	664(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm5
	addsd	%xmm2, %xmm5
	movq	704(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm5, %xmm0
	movq	160(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm3
	addsd	%xmm0, %xmm3
	movq	144(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm3, (%rdx,%r12,8)
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdi,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r15,%r12,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	656(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%rcx), %xmm1
	movsd	(%r8,%r12,8), %xmm3     # xmm3 = mem[0],zero
	movq	192(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rcx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	-8(%r11,%r12,8), %xmm5  # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%r10,%rcx), %xmm2
	addsd	%xmm4, %xmm2
	movq	648(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm5
	addsd	%xmm2, %xmm5
	movq	696(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm5, %xmm0
	movq	624(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm3
	addsd	%xmm0, %xmm3
	movq	136(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm3, (%rdx,%r12,8)
	movsd	(%r13,%r12,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rsi,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r15,%r12,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	640(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%rcx), %xmm1
	movsd	(%r8,%r12,8), %xmm3     # xmm3 = mem[0],zero
	movq	616(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rcx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	(%r11,%r12,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	688(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm4, %xmm0
	movq	608(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm3
	addsd	%xmm0, %xmm3
	mulsd	(%rbp,%rcx), %xmm2
	addsd	%xmm3, %xmm2
	movq	632(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm5
	addsd	%xmm2, %xmm5
	movsd	%xmm5, (%r14,%r12,8)
	incq	%r12
	addq	184(%rsp), %rcx         # 8-byte Folded Reload
	cmpl	%r12d, 16(%rsp)         # 4-byte Folded Reload
	jne	.LBB1_36
# BB#37:                                # %._crit_edge2795.loopexit
                                        #   in Loop: Header=BB1_34 Depth=3
	movl	576(%rsp), %ebp         # 4-byte Reload
	addl	536(%rsp), %ebp         # 4-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	296(%rsp), %edi         # 4-byte Reload
	movl	600(%rsp), %r15d        # 4-byte Reload
	movq	592(%rsp), %rsi         # 8-byte Reload
	movl	584(%rsp), %eax         # 4-byte Reload
	movl	568(%rsp), %ebx         # 4-byte Reload
.LBB1_38:                               # %._crit_edge2795
                                        #   in Loop: Header=BB1_34 Depth=3
	addl	272(%rsp), %edi         # 4-byte Folded Reload
	addl	552(%rsp), %ebp         # 4-byte Folded Reload
	addl	560(%rsp), %esi         # 4-byte Folded Reload
	addl	544(%rsp), %r15d        # 4-byte Folded Reload
	incl	%ebx
	addl	280(%rsp), %eax         # 4-byte Folded Reload
	cmpl	288(%rsp), %ebx         # 4-byte Folded Reload
	jne	.LBB1_34
	jmp	.LBB1_39
	.p2align	4, 0x90
.LBB1_32:                               #   in Loop: Header=BB1_31 Depth=2
	movl	%eax, %ebp
.LBB1_39:                               # %._crit_edge2805
                                        #   in Loop: Header=BB1_31 Depth=2
	addl	224(%rsp), %edi         # 4-byte Folded Reload
	addl	776(%rsp), %ebp         # 4-byte Folded Reload
	addl	232(%rsp), %esi         # 4-byte Folded Reload
	addl	768(%rsp), %r15d        # 4-byte Folded Reload
	movl	728(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 728(%rsp)         # 4-byte Spill
	cmpl	384(%rsp), %eax         # 4-byte Folded Reload
	movl	%ebp, %eax
	jne	.LBB1_31
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_40:                               #   in Loop: Header=BB1_2 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	212(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r14), %r9d
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%rbx), %r15d
	movl	12(%rax,%rbx), %ecx
	movl	%ecx, %r10d
	subl	%r9d, %r10d
	incl	%r10d
	cmpl	%r9d, %ecx
	movl	16(%rax,%rbx), %edx
	movl	$0, %eax
	cmovsl	%eax, %r10d
	movl	%edx, %r11d
	subl	%r15d, %r11d
	incl	%r11d
	cmpl	%r15d, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebx
	movl	-20(%rcx,%r13), %edi
	movl	-12(%rcx,%r13), %edx
	cmovsl	%eax, %r11d
	movl	%edx, %r12d
	subl	%ebx, %r12d
	incl	%r12d
	cmpl	%ebx, %edx
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %r12d
	movl	%esi, %r14d
	subl	%edi, %r14d
	incl	%r14d
	movl	%edi, 48(%rsp)          # 4-byte Spill
	cmpl	%edi, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebp
	movl	-20(%rcx,%r13), %edx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %r14d
	movl	%esi, %edi
	subl	%ebp, %edi
	incl	%edi
	movl	%ebp, 96(%rsp)          # 4-byte Spill
	cmpl	%ebp, %esi
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %edi
	movl	%edi, 480(%rsp)         # 4-byte Spill
	movl	%esi, %ebp
	subl	%edx, %ebp
	incl	%ebp
	movl	%edx, 80(%rsp)          # 4-byte Spill
	cmpl	%edx, %esi
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %edx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %ebp
	movl	%esi, %edi
	subl	%edx, %edi
	incl	%edi
	movl	%edx, 88(%rsp)          # 4-byte Spill
	cmpl	%edx, %esi
	movl	-20(%rcx,%r13), %edx
	cmovsl	%eax, %edi
	movl	%edi, 472(%rsp)         # 4-byte Spill
	movl	-8(%rcx,%r13), %esi
	movl	%esi, %r8d
	subl	%edx, %r8d
	incl	%r8d
	cmpl	%edx, %esi
	cmovsl	%eax, %r8d
	movl	212(%rsp), %eax
	movl	216(%rsp), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %esi
	movl	%ecx, 488(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %esi
	movl	220(%rsp), %ecx
	cmpl	%esi, %ecx
	movl	%ecx, 872(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %esi
	testl	%esi, %esi
	movq	%rax, %rsi
	jle	.LBB1_64
# BB#41:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, 872(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_64
# BB#42:                                # %.preheader2756.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movl	8(%rcx,%rax), %edi
	movq	720(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,2), %rsi
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	movl	%edi, %ecx
	movq	24(%rsp), %rax          # 8-byte Reload
	subl	8(%rax,%rsi,8), %ecx
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	movl	420(%rsp), %r15d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r15d
	movl	%edi, %ebx
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %edi
	movl	%edi, 504(%rsp)         # 4-byte Spill
	movl	%r9d, 40(%rsp)          # 4-byte Spill
	movl	%ecx, %r9d
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	104(%rsp), %rcx         # 8-byte Reload
	movl	4(%rcx,%rax), %ecx
	movq	736(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %esi
	imull	%r12d, %esi
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movl	%r12d, %edx
	imull	%r14d, %edx
	imull	8(%rax), %edx
	movl	488(%rsp), %r13d        # 4-byte Reload
	movl	%r13d, %edi
	imull	%esi, %edi
	subl	%edi, %edx
	movl	%edx, 1024(%rsp)        # 4-byte Spill
	movslq	(%rax), %rax
	movq	%rax, 512(%rsp)         # 8-byte Spill
	movl	%eax, %edi
	imull	16(%rsp), %edi          # 4-byte Folded Reload
	subl	%edi, %esi
	movl	%esi, 800(%rsp)         # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, %edi
	subl	40(%rsp), %edi          # 4-byte Folded Reload
	movl	%ecx, %esi
	subl	24(%rsp), %esi          # 4-byte Folded Reload
	imull	%r11d, %r9d
	addl	%esi, %r9d
	imull	%r10d, %r9d
	addl	%edi, %r9d
	movl	412(%rsp), %esi
	subl	72(%rsp), %esi          # 4-byte Folded Reload
	movl	416(%rsp), %edi
	subl	48(%rsp), %edi          # 4-byte Folded Reload
	imull	%r14d, %r15d
	addl	%edi, %r15d
	imull	%r12d, %r15d
	addl	%esi, %r15d
	movl	%eax, %edx
	subl	96(%rsp), %edx          # 4-byte Folded Reload
	movl	%ecx, %esi
	subl	80(%rsp), %esi          # 4-byte Folded Reload
	movl	%ebx, %edi
	imull	%ebp, %edi
	addl	%esi, %edi
	movl	480(%rsp), %ebx         # 4-byte Reload
	imull	%ebx, %edi
	addl	%edx, %edi
	movl	%edi, 496(%rsp)         # 4-byte Spill
	subl	88(%rsp), %eax          # 4-byte Folded Reload
	movl	504(%rsp), %edx         # 4-byte Reload
	subl	32(%rsp), %ecx          # 4-byte Folded Reload
	imull	%r8d, %edx
	addl	%ecx, %edx
	subl	%r13d, %r8d
	movl	%r10d, %edi
	movl	472(%rsp), %r10d        # 4-byte Reload
	imull	%r10d, %r8d
	movl	%r8d, 1016(%rsp)        # 4-byte Spill
	subl	%r13d, %ebp
	imull	%ebx, %ebp
	movl	%ebp, 1008(%rsp)        # 4-byte Spill
	subl	%r13d, %r11d
	imull	%edi, %r11d
	movl	%r11d, 1000(%rsp)       # 4-byte Spill
	imull	%r10d, %edx
	addl	%eax, %edx
	movl	%ebx, %eax
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 792(%rsp)         # 4-byte Spill
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	movslq	144(%rsp), %rsi         # 4-byte Folded Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %ecx
	incq	%rcx
	movq	%rcx, 384(%rsp)         # 8-byte Spill
	movq	512(%rsp), %r12         # 8-byte Reload
	imulq	%r12, %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movl	$0, 880(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	$0, %r8d
	cmovnsl	16(%rsp), %r8d          # 4-byte Folded Reload
	shlq	$3, %rax
	shlq	$3, %rsi
	movq	%rax, %r14
	subq	%rsi, %r14
	movq	376(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rcx
	subq	%rsi, %rcx
	movq	%rcx, 776(%rsp)         # 8-byte Spill
	negq	%rsi
	subq	%rax, %rsi
	movq	368(%rsp), %rcx         # 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, 768(%rsp)         # 8-byte Spill
	movq	%r11, %rcx
	subq	%rax, %rcx
	movq	%rcx, 1224(%rsp)        # 8-byte Spill
	addl	%r8d, %ebx
	addl	%r8d, %r10d
	addl	%edi, %r8d
	movslq	152(%rsp), %rax         # 4-byte Folded Reload
	leaq	(,%rax,8), %rcx
	movq	432(%rsp), %rbp         # 8-byte Reload
	subq	%rcx, %rbp
	movq	%rbp, 1216(%rsp)        # 8-byte Spill
	movq	120(%rsp), %rbp         # 8-byte Reload
	subq	%rcx, %rbp
	movq	%rbp, 760(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rbp         # 8-byte Reload
	subq	%rcx, %rbp
	movq	%rbp, 752(%rsp)         # 8-byte Spill
	movq	424(%rsp), %rbp         # 8-byte Reload
	subq	%rcx, %rbp
	movq	%rbp, 744(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rbp         # 8-byte Reload
	subq	%rcx, %rbp
	movq	%rbp, 1208(%rsp)        # 8-byte Spill
	movq	320(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1200(%rsp)        # 8-byte Spill
	movq	360(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	subq	%rcx, %rdi
	movq	%rdi, 1192(%rsp)        # 8-byte Spill
	movq	328(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdi
	subq	%rcx, %rdi
	movq	%rdi, 1152(%rsp)        # 8-byte Spill
	subq	%rcx, 352(%rsp)         # 8-byte Folded Spill
	movq	448(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1144(%rsp)        # 8-byte Spill
	movq	992(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1136(%rsp)        # 8-byte Spill
	subq	%rcx, 344(%rsp)         # 8-byte Folded Spill
	movq	440(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1128(%rsp)        # 8-byte Spill
	movq	984(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1120(%rsp)        # 8-byte Spill
	movq	456(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1112(%rsp)        # 8-byte Spill
	movq	464(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1104(%rsp)        # 8-byte Spill
	movq	336(%rsp), %rdi         # 8-byte Reload
	subq	%rcx, %rdi
	movq	%rdi, 1096(%rsp)        # 8-byte Spill
	subl	16(%rsp), %ebx          # 4-byte Folded Reload
	movl	%ebx, 480(%rsp)         # 4-byte Spill
	movq	%r15, %rbx
	subl	16(%rsp), %r10d         # 4-byte Folded Reload
	movl	%r10d, 472(%rsp)        # 4-byte Spill
	addq	%r11, %r14
	movq	%r14, 224(%rsp)         # 8-byte Spill
	addq	%r11, %rsi
	movq	%rsi, 728(%rsp)         # 8-byte Spill
	subl	16(%rsp), %r8d          # 4-byte Folded Reload
	movl	%r8d, 784(%rsp)         # 4-byte Spill
	shlq	$3, %r12
	movq	432(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	%rsi, 1088(%rsp)        # 8-byte Spill
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	%rsi, 1080(%rsp)        # 8-byte Spill
	movq	176(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	%rsi, 1072(%rsp)        # 8-byte Spill
	movq	424(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	%rsi, 1064(%rsp)        # 8-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	%rsi, 1056(%rsp)        # 8-byte Spill
	movq	320(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	%rsi, 1048(%rsp)        # 8-byte Spill
	leaq	(%rbp,%rax,8), %rsi
	movq	%rsi, 1040(%rsp)        # 8-byte Spill
	leaq	(%r13,%rax,8), %rax
	movq	%rax, 1032(%rsp)        # 8-byte Spill
	movl	496(%rsp), %eax         # 4-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r12, 512(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_43:                               # %.preheader2756
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_46 Depth 3
                                        #         Child Loop BB1_48 Depth 4
	cmpl	$0, 488(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_44
# BB#45:                                # %.preheader2752.preheader
                                        #   in Loop: Header=BB1_43 Depth=2
	xorl	%ebp, %ebp
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB1_46:                               # %.preheader2752
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_43 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_48 Depth 4
	testl	%ecx, %ecx
	jle	.LBB1_50
# BB#47:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_46 Depth=3
	movl	%ebp, 808(%rsp)         # 4-byte Spill
	movl	%esi, 816(%rsp)         # 4-byte Spill
	movl	%eax, 496(%rsp)         # 4-byte Spill
	cltq
	movq	968(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r10
	movq	976(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r15
	movl	%edx, 504(%rsp)         # 4-byte Spill
	movslq	%edx, %rdi
	shlq	$3, %rdi
	movl	%r9d, 832(%rsp)         # 4-byte Spill
	movslq	%r9d, %rsi
	shlq	$3, %rsi
	movslq	%ebx, %rcx
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	776(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	728(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	768(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movq	1224(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi), %rax
	movq	%rax, 840(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	addq	376(%rsp), %rdi         # 8-byte Folded Reload
	movq	%rdi, 712(%rsp)         # 8-byte Spill
	movq	1256(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	912(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	1248(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	928(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	936(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	920(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	1240(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movq	904(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	1232(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	1272(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	944(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	1264(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	952(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	addq	960(%rsp), %rsi         # 8-byte Folded Reload
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	232(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rdx
	movq	%rdx, 824(%rsp)         # 8-byte Spill
	movq	1216(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	760(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	752(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 704(%rsp)         # 8-byte Spill
	movq	744(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 696(%rsp)         # 8-byte Spill
	movq	1208(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 688(%rsp)         # 8-byte Spill
	movq	1200(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 680(%rsp)         # 8-byte Spill
	movq	1192(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 624(%rsp)         # 8-byte Spill
	movq	1152(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movq	352(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 672(%rsp)         # 8-byte Spill
	movq	1144(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 664(%rsp)         # 8-byte Spill
	movq	1136(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 656(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 648(%rsp)         # 8-byte Spill
	movq	1128(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 640(%rsp)         # 8-byte Spill
	movq	1120(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 632(%rsp)         # 8-byte Spill
	movq	1112(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 304(%rsp)         # 8-byte Spill
	movq	1104(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	1096(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movq	320(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	328(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 848(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 616(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 608(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 296(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 600(%rsp)         # 8-byte Spill
	movq	424(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 592(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 584(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 576(%rsp)         # 8-byte Spill
	movq	360(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 568(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	1088(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	movq	1080(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	movq	1072(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 560(%rsp)         # 8-byte Spill
	movq	1064(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 552(%rsp)         # 8-byte Spill
	movq	1056(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 544(%rsp)         # 8-byte Spill
	movq	1048(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 536(%rsp)         # 8-byte Spill
	movq	1040(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rdx
	movq	%rdx, 528(%rsp)         # 8-byte Spill
	movq	1032(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rcx
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_48:                               # %.lr.ph
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_43 Depth=2
                                        #       Parent Loop BB1_46 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	168(%rsp), %rdi         # 8-byte Reload
	mulsd	(%rdi,%rcx), %xmm0
	movq	248(%rsp), %rbx         # 8-byte Reload
	mulsd	-8(%rbx,%r13,8), %xmm0
	movq	40(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm0, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	312(%rsp), %r11         # 8-byte Reload
	movsd	(%r11,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rbx,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	704(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	movq	400(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm2, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	696(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	mulsd	8(%rbx,%r13,8), %xmm0
	movq	112(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm0, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	688(%rsp), %r8          # 8-byte Reload
	movsd	(%r8,%rcx), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	256(%rsp), %rbx         # 8-byte Reload
	movsd	-8(%rbx,%r13,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	680(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	movq	240(%rsp), %r12         # 8-byte Reload
	mulsd	(%r12,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	64(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm2, (%rsi,%r13,8)
	movsd	(%rbx,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	848(%rsp), %rbp         # 8-byte Reload
	movsd	(%rbp,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r10,%r13,8), %xmm2    # xmm2 = mem[0],zero
	movq	624(%rsp), %r14         # 8-byte Reload
	movsd	(%r14,%rcx), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	200(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm2
	addsd	%xmm3, %xmm2
	movq	56(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm2, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	672(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%rbx,%r13,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	664(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	movq	616(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	104(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	656(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm0
	movq	264(%rsp), %r9          # 8-byte Reload
	mulsd	-8(%r9,%r13,8), %xmm0
	movq	152(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm0, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	648(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r9,%r13,8), %xmm2     # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	640(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm0
	addsd	%xmm1, %xmm0
	movq	608(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm2
	addsd	%xmm0, %xmm2
	movq	144(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movq	632(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm0
	mulsd	8(%r9,%r13,8), %xmm0
	movq	136(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm0, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rdi,%rcx), %xmm0
	movq	392(%rsp), %rdi         # 8-byte Reload
	mulsd	-8(%rdi,%r13,8), %xmm0
	movq	184(%rsp), %rsi         # 8-byte Reload
	addsd	(%rsi,%rcx), %xmm0
	movsd	(%r15,%r13,8), %xmm1    # xmm1 = mem[0],zero
	movq	280(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm1
	movq	840(%rsp), %rbx         # 8-byte Reload
	mulsd	-8(%rbx,%r13,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	96(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm1, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r11,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rdi,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	296(%rsp), %rsi         # 8-byte Reload
	addsd	(%rsi,%rcx), %xmm1
	movsd	(%r15,%r13,8), %xmm3    # xmm3 = mem[0],zero
	movq	272(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rcx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	(%rbx,%r13,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%rax,%rcx), %xmm2
	addsd	%xmm4, %xmm2
	movq	600(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm5
	addsd	%xmm2, %xmm5
	movq	304(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm0
	addsd	%xmm5, %xmm0
	movq	560(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rcx), %xmm3
	addsd	%xmm0, %xmm3
	movq	88(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm3, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rdx,%rcx), %xmm0
	mulsd	8(%rdi,%r13,8), %xmm0
	movq	592(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%rcx), %xmm0
	movsd	(%r15,%r13,8), %xmm1    # xmm1 = mem[0],zero
	movq	552(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm1
	mulsd	8(%rbx,%r13,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	80(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm1, (%rsi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r8,%rcx), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	32(%rsp), %rsi          # 8-byte Reload
	movsd	-8(%rsi,%r13,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	584(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%rcx), %xmm1
	movsd	(%r15,%r13,8), %xmm3    # xmm3 = mem[0],zero
	movq	544(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rcx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	712(%rsp), %rdx         # 8-byte Reload
	movsd	-8(%rdx,%r13,8), %xmm5  # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%r12,%rcx), %xmm2
	addsd	%xmm4, %xmm2
	movq	576(%rsp), %rdi         # 8-byte Reload
	mulsd	(%rdi,%rcx), %xmm5
	addsd	%xmm2, %xmm5
	movq	160(%rsp), %rdi         # 8-byte Reload
	mulsd	(%rdi,%rcx), %xmm0
	addsd	%xmm5, %xmm0
	movq	536(%rsp), %rdi         # 8-byte Reload
	mulsd	(%rdi,%rcx), %xmm3
	addsd	%xmm0, %xmm3
	movq	72(%rsp), %rdi          # 8-byte Reload
	movsd	%xmm3, (%rdi,%r13,8)
	movsd	(%r10,%r13,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rcx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rsi,%r13,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	568(%rsp), %rsi         # 8-byte Reload
	addsd	(%rsi,%rcx), %xmm1
	movsd	(%r15,%r13,8), %xmm3    # xmm3 = mem[0],zero
	movq	528(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rcx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	(%rdx,%r13,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	192(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm0
	addsd	%xmm4, %xmm0
	movq	520(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm3
	addsd	%xmm0, %xmm3
	mulsd	(%rbp,%rcx), %xmm2
	addsd	%xmm3, %xmm2
	movq	288(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rcx), %xmm5
	addsd	%xmm2, %xmm5
	movq	48(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm5, (%rsi,%r13,8)
	incq	%r13
	addq	512(%rsp), %rcx         # 8-byte Folded Reload
	cmpl	%r13d, 16(%rsp)         # 4-byte Folded Reload
	jne	.LBB1_48
# BB#49:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB1_46 Depth=3
	movl	816(%rsp), %esi         # 4-byte Reload
	addl	384(%rsp), %esi         # 4-byte Folded Reload
	movl	504(%rsp), %edx         # 4-byte Reload
	movl	832(%rsp), %r9d         # 4-byte Reload
	movq	824(%rsp), %rbx         # 8-byte Reload
	movl	496(%rsp), %eax         # 4-byte Reload
	movl	808(%rsp), %ebp         # 4-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB1_50:                               # %._crit_edge
                                        #   in Loop: Header=BB1_46 Depth=3
	addl	472(%rsp), %edx         # 4-byte Folded Reload
	addl	792(%rsp), %esi         # 4-byte Folded Reload
	addl	800(%rsp), %ebx         # 4-byte Folded Reload
	addl	784(%rsp), %r9d         # 4-byte Folded Reload
	incl	%ebp
	addl	480(%rsp), %eax         # 4-byte Folded Reload
	cmpl	488(%rsp), %ebp         # 4-byte Folded Reload
	jne	.LBB1_46
	jmp	.LBB1_51
	.p2align	4, 0x90
.LBB1_44:                               #   in Loop: Header=BB1_43 Depth=2
	movl	%eax, %esi
.LBB1_51:                               # %._crit_edge2775
                                        #   in Loop: Header=BB1_43 Depth=2
	addl	1016(%rsp), %edx        # 4-byte Folded Reload
	addl	1008(%rsp), %esi        # 4-byte Folded Reload
	addl	1024(%rsp), %ebx        # 4-byte Folded Reload
	addl	1000(%rsp), %r9d        # 4-byte Folded Reload
	movl	880(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 880(%rsp)         # 4-byte Spill
	cmpl	872(%rsp), %eax         # 4-byte Folded Reload
	movl	%esi, %eax
	jne	.LBB1_43
	jmp	.LBB1_64
	.p2align	4, 0x90
.LBB1_52:                               #   in Loop: Header=BB1_2 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	212(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r14), %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%rbx), %r15d
	movl	12(%rax,%rbx), %ecx
	movl	%ecx, %r10d
	subl	%edx, %r10d
	incl	%r10d
	movl	%edx, 72(%rsp)          # 4-byte Spill
	cmpl	%edx, %ecx
	movl	16(%rax,%rbx), %edx
	movl	$0, %eax
	cmovsl	%eax, %r10d
	movl	%edx, %r11d
	subl	%r15d, %r11d
	incl	%r11d
	cmpl	%r15d, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %esi
	movl	-20(%rcx,%r13), %edi
	movl	-12(%rcx,%r13), %edx
	cmovsl	%eax, %r11d
	movl	%edx, %r14d
	subl	%esi, %r14d
	incl	%r14d
	movl	%esi, 88(%rsp)          # 4-byte Spill
	cmpl	%esi, %edx
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %r14d
	movl	%esi, %r12d
	subl	%edi, %r12d
	incl	%r12d
	movl	%edi, 16(%rsp)          # 4-byte Spill
	cmpl	%edi, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %edx
	movl	-20(%rcx,%r13), %ebx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %r12d
	movl	%esi, %edi
	subl	%edx, %edi
	incl	%edi
	movl	%edx, 168(%rsp)         # 4-byte Spill
	cmpl	%edx, %esi
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %edi
	movl	%edi, 752(%rsp)         # 4-byte Spill
	movl	%esi, %ebp
	subl	%ebx, %ebp
	incl	%ebp
	movl	%ebx, 96(%rsp)          # 4-byte Spill
	cmpl	%ebx, %esi
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %ebp
	movl	%esi, %edi
	subl	%ebx, %edi
	incl	%edi
	cmpl	%ebx, %esi
	movl	-20(%rcx,%r13), %r9d
	cmovsl	%eax, %edi
	movl	%edi, 744(%rsp)         # 4-byte Spill
	movl	-8(%rcx,%r13), %esi
	movl	%esi, %r8d
	subl	%r9d, %r8d
	incl	%r8d
	movl	%r9d, 80(%rsp)          # 4-byte Spill
	cmpl	%r9d, %esi
	cmovsl	%eax, %r8d
	movl	212(%rsp), %eax
	movl	216(%rsp), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %esi
	movl	%ecx, 760(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %esi
	movl	220(%rsp), %ecx
	cmpl	%esi, %ecx
	movl	%ecx, 864(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %esi
	testl	%esi, %esi
	jle	.LBB1_64
# BB#53:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	$0, 864(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_64
# BB#54:                                # %.preheader2753.preheader
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	112(%rsp), %rdi         # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	8(%rax,%rdi), %ecx
	movq	720(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax,2), %rsi
	movl	%ecx, %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	subl	8(%rax,%rsi,8), %edx
	movl	%edx, 224(%rsp)         # 4-byte Spill
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	movl	420(%rsp), %r15d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r15d
	movl	%ecx, %r9d
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r9d
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %ecx
	movl	%ecx, 232(%rsp)         # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	4(%rax,%rdi), %ecx
	movq	736(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %esi
	imull	%r14d, %esi
	movl	%r14d, %edx
	imull	%r12d, %edx
	imull	8(%rax), %edx
	movl	%ebx, %r13d
	movl	760(%rsp), %ebx         # 4-byte Reload
	movl	%ebx, %edi
	imull	%esi, %edi
	subl	%edi, %edx
	movl	%edx, 1292(%rsp)        # 4-byte Spill
	movslq	(%rax), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movl	%eax, %edi
	imull	48(%rsp), %edi          # 4-byte Folded Reload
	subl	%edi, %esi
	movl	%esi, 1208(%rsp)        # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, %edi
	subl	72(%rsp), %edi          # 4-byte Folded Reload
	movl	%ecx, %esi
	subl	24(%rsp), %esi          # 4-byte Folded Reload
	movl	224(%rsp), %edx         # 4-byte Reload
	imull	%r11d, %edx
	addl	%esi, %edx
	imull	%r10d, %edx
	addl	%edi, %edx
	movl	%edx, 224(%rsp)         # 4-byte Spill
	movl	412(%rsp), %esi
	subl	88(%rsp), %esi          # 4-byte Folded Reload
	movl	416(%rsp), %edi
	subl	16(%rsp), %edi          # 4-byte Folded Reload
	imull	%r12d, %r15d
	addl	%edi, %r15d
	imull	%r14d, %r15d
	addl	%esi, %r15d
	movl	%eax, %edx
	subl	168(%rsp), %edx         # 4-byte Folded Reload
	movl	%ecx, %esi
	subl	96(%rsp), %esi          # 4-byte Folded Reload
	imull	%ebp, %r9d
	addl	%esi, %r9d
	movl	752(%rsp), %esi         # 4-byte Reload
	imull	%esi, %r9d
	addl	%edx, %r9d
	subl	%r13d, %eax
	subl	80(%rsp), %ecx          # 4-byte Folded Reload
	movl	232(%rsp), %edx         # 4-byte Reload
	imull	%r8d, %edx
	addl	%ecx, %edx
	subl	%ebx, %r8d
	movl	744(%rsp), %edi         # 4-byte Reload
	imull	%edi, %r8d
	movl	%r8d, 1288(%rsp)        # 4-byte Spill
	subl	%ebx, %ebp
	imull	%esi, %ebp
	movl	%ebp, 1284(%rsp)        # 4-byte Spill
	subl	%ebx, %r11d
	imull	%r10d, %r11d
	movl	%r11d, 1280(%rsp)       # 4-byte Spill
	imull	%edi, %edx
	addl	%eax, %edx
	movl	%edx, 232(%rsp)         # 4-byte Spill
	movl	%esi, %eax
	subl	48(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 1200(%rsp)        # 4-byte Spill
	movslq	136(%rsp), %rax         # 4-byte Folded Reload
	movslq	144(%rsp), %rdx         # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %ecx
	incq	%rcx
	movq	%rcx, 1144(%rsp)        # 8-byte Spill
	movq	384(%rsp), %r14         # 8-byte Reload
	imulq	%r14, %rcx
	movq	%rcx, 1136(%rsp)        # 8-byte Spill
	movl	$0, 868(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movl	$0, %r8d
	cmovnsl	48(%rsp), %r8d          # 4-byte Folded Reload
	shlq	$3, %rax
	shlq	$3, %rdx
	movq	%rax, %rbp
	subq	%rdx, %rbp
	movq	376(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rcx
	subq	%rdx, %rcx
	movq	%rcx, 1120(%rsp)        # 8-byte Spill
	negq	%rdx
	subq	%rax, %rdx
	movq	368(%rsp), %rcx         # 8-byte Reload
	subq	%rax, %rcx
	movq	%rcx, 1112(%rsp)        # 8-byte Spill
	movq	%r11, %rcx
	subq	%rax, %rcx
	movq	%rcx, 1104(%rsp)        # 8-byte Spill
	addl	%r8d, %esi
	addl	%r8d, %edi
	addl	%r10d, %r8d
	movslq	152(%rsp), %rax         # 4-byte Folded Reload
	leaq	(,%rax,8), %rcx
	movq	432(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1096(%rsp)        # 8-byte Spill
	movq	896(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1088(%rsp)        # 8-byte Spill
	movq	120(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1080(%rsp)        # 8-byte Spill
	movq	176(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1072(%rsp)        # 8-byte Spill
	movq	424(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1064(%rsp)        # 8-byte Spill
	movq	888(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1056(%rsp)        # 8-byte Spill
	movq	128(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1048(%rsp)        # 8-byte Spill
	movq	320(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rbx
	subq	%rcx, %rbx
	movq	%rbx, 1040(%rsp)        # 8-byte Spill
	movq	360(%rsp), %r10         # 8-byte Reload
	movq	%r10, %rbx
	subq	%rcx, %rbx
	movq	%rbx, 1032(%rsp)        # 8-byte Spill
	movq	328(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rbx
	subq	%rcx, %rbx
	movq	%rbx, 880(%rsp)         # 8-byte Spill
	subq	%rcx, 352(%rsp)         # 8-byte Folded Spill
	movq	448(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 872(%rsp)         # 8-byte Spill
	movq	992(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1024(%rsp)        # 8-byte Spill
	movq	1168(%rsp), %rbx        # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1016(%rsp)        # 8-byte Spill
	subq	%rcx, 344(%rsp)         # 8-byte Folded Spill
	movq	440(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1008(%rsp)        # 8-byte Spill
	movq	984(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1000(%rsp)        # 8-byte Spill
	movq	1160(%rsp), %rbx        # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1488(%rsp)        # 8-byte Spill
	movq	1184(%rsp), %rbx        # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1480(%rsp)        # 8-byte Spill
	movq	456(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1472(%rsp)        # 8-byte Spill
	movq	1176(%rsp), %rbx        # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1464(%rsp)        # 8-byte Spill
	movq	464(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1456(%rsp)        # 8-byte Spill
	movq	336(%rsp), %rbx         # 8-byte Reload
	subq	%rcx, %rbx
	movq	%rbx, 1448(%rsp)        # 8-byte Spill
	movq	48(%rsp), %rcx          # 8-byte Reload
	subl	%ecx, %esi
	movl	%esi, 752(%rsp)         # 4-byte Spill
	movl	224(%rsp), %esi         # 4-byte Reload
	subl	%ecx, %edi
	movl	%edi, 744(%rsp)         # 4-byte Spill
	addq	%r11, %rbp
	movq	%rbp, 1128(%rsp)        # 8-byte Spill
	addq	%r11, %rdx
	movq	%rdx, 1152(%rsp)        # 8-byte Spill
	subl	%ecx, %r8d
	movl	%r8d, 1192(%rsp)        # 4-byte Spill
	shlq	$3, %r14
	movq	432(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	%rdx, 1440(%rsp)        # 8-byte Spill
	movq	896(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	%rdx, 1432(%rsp)        # 8-byte Spill
	movq	120(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	%rdx, 1424(%rsp)        # 8-byte Spill
	movq	176(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	%rdx, 1416(%rsp)        # 8-byte Spill
	movq	424(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	%rdx, 1408(%rsp)        # 8-byte Spill
	movq	888(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	%rdx, 1400(%rsp)        # 8-byte Spill
	movq	128(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rdx
	movq	%rdx, 1392(%rsp)        # 8-byte Spill
	leaq	(%r13,%rax,8), %rdx
	movq	%rdx, 1384(%rsp)        # 8-byte Spill
	leaq	(%r10,%rax,8), %rdx
	movq	%rdx, 1376(%rsp)        # 8-byte Spill
	movl	232(%rsp), %edx         # 4-byte Reload
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 1368(%rsp)        # 8-byte Spill
	movl	%r9d, %eax
	movq	%r14, 384(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_55:                               # %.preheader2753
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_58 Depth 3
                                        #         Child Loop BB1_60 Depth 4
	cmpl	$0, 760(%rsp)           # 4-byte Folded Reload
	jle	.LBB1_56
# BB#57:                                # %.preheader.preheader
                                        #   in Loop: Header=BB1_55 Depth=2
	xorl	%ebx, %ebx
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB1_58:                               # %.preheader
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_55 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_60 Depth 4
	testl	%ecx, %ecx
	jle	.LBB1_62
# BB#59:                                # %.lr.ph2854.preheader
                                        #   in Loop: Header=BB1_58 Depth=3
	movl	%ebx, 1216(%rsp)        # 4-byte Spill
	movl	%ebp, 1224(%rsp)        # 4-byte Spill
	movl	%eax, 768(%rsp)         # 4-byte Spill
	cltq
	movq	968(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r13
	movq	976(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r10
	movl	%edx, 232(%rsp)         # 4-byte Spill
	movslq	%edx, %rbx
	shlq	$3, %rbx
	movl	%esi, 224(%rsp)         # 4-byte Spill
	movslq	%esi, %rsi
	shlq	$3, %rsi
	movslq	%r15d, %rcx
	movq	1128(%rsp), %rax        # 8-byte Reload
	leaq	(%rax,%rbx), %rax
	movq	%rax, 520(%rsp)         # 8-byte Spill
	movq	1120(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rbx), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	movq	1152(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rbx), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	1112(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rbx), %rax
	movq	%rax, 848(%rsp)         # 8-byte Spill
	movq	1104(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rbx), %rax
	movq	%rax, 504(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rbx), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	addq	376(%rsp), %rbx         # 8-byte Folded Reload
	movq	%rbx, 672(%rsp)         # 8-byte Spill
	movq	1256(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	912(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movq	1248(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	928(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	936(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	920(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movq	1240(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	904(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	1232(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movq	1272(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	944(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	1264(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	952(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	addq	960(%rsp), %rsi         # 8-byte Folded Reload
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	1136(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx), %rdx
	movq	%rdx, 776(%rsp)         # 8-byte Spill
	movq	1096(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	1088(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rdx
	movq	%rdx, 712(%rsp)         # 8-byte Spill
	movq	1080(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 704(%rsp)         # 8-byte Spill
	movq	1072(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 696(%rsp)         # 8-byte Spill
	movq	1064(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 688(%rsp)         # 8-byte Spill
	movq	1056(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 680(%rsp)         # 8-byte Spill
	movq	1048(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	1040(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 664(%rsp)         # 8-byte Spill
	movq	1032(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 656(%rsp)         # 8-byte Spill
	movq	880(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 648(%rsp)         # 8-byte Spill
	movq	352(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 640(%rsp)         # 8-byte Spill
	movq	872(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 632(%rsp)         # 8-byte Spill
	movq	1024(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	1016(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	1008(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 624(%rsp)         # 8-byte Spill
	movq	1000(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 616(%rsp)         # 8-byte Spill
	movq	1488(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 608(%rsp)         # 8-byte Spill
	movq	1480(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	1472(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	1464(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 600(%rsp)         # 8-byte Spill
	movq	1456(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 592(%rsp)         # 8-byte Spill
	movq	1448(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	movq	896(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	888(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,8), %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	movq	320(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 512(%rsp)         # 8-byte Spill
	movq	328(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	448(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 576(%rsp)         # 8-byte Spill
	movq	1168(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 568(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	1160(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	1184(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 560(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 552(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 544(%rsp)         # 8-byte Spill
	movq	424(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 536(%rsp)         # 8-byte Spill
	movq	1176(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 840(%rsp)         # 8-byte Spill
	movq	360(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 496(%rsp)         # 8-byte Spill
	movq	336(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 832(%rsp)         # 8-byte Spill
	movq	1440(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 824(%rsp)         # 8-byte Spill
	movq	1432(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 816(%rsp)         # 8-byte Spill
	movq	1424(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 808(%rsp)         # 8-byte Spill
	movq	1416(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movq	1408(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	movq	1400(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movq	1392(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 800(%rsp)         # 8-byte Spill
	movq	1384(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 792(%rsp)         # 8-byte Spill
	movq	1376(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 784(%rsp)         # 8-byte Spill
	movq	1368(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx,8), %rax
	movq	%rax, 728(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_60:                               # %.lr.ph2854
                                        #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_55 Depth=2
                                        #       Parent Loop BB1_58 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	312(%rsp), %rbp         # 8-byte Reload
	movsd	(%rbp,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	248(%rsp), %rdi         # 8-byte Reload
	movsd	-8(%rdi,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	712(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	addsd	%xmm1, %xmm0
	movq	528(%rsp), %r15         # 8-byte Reload
	mulsd	(%r15,%rsi), %xmm2
	addsd	%xmm0, %xmm2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	704(%rsp), %rax         # 8-byte Reload
	movsd	(%rax,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rdi,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	696(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	addsd	%xmm1, %xmm0
	movq	256(%rsp), %r14         # 8-byte Reload
	mulsd	(%r14,%rsi), %xmm2
	addsd	%xmm0, %xmm2
	movq	112(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	688(%rsp), %rbx         # 8-byte Reload
	movsd	(%rbx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%rdi,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	680(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	addsd	%xmm1, %xmm0
	movq	392(%rsp), %r8          # 8-byte Reload
	mulsd	(%r8,%rsi), %xmm2
	addsd	%xmm0, %xmm2
	movq	64(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	200(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	400(%rsp), %r9          # 8-byte Reload
	movsd	-8(%r9,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	664(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	addsd	%xmm1, %xmm0
	movq	512(%rsp), %r11         # 8-byte Reload
	mulsd	(%r11,%rsi), %xmm2
	addsd	%xmm0, %xmm2
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r9,%rcx,8), %xmm0     # xmm0 = mem[0],zero
	movq	16(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r13,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	movq	656(%rsp), %r12         # 8-byte Reload
	movsd	(%r12,%rsi), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	648(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm2
	addsd	%xmm3, %xmm2
	movq	104(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	640(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%r9,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	632(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	movq	576(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
	movq	152(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	304(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	520(%rsp), %r9          # 8-byte Reload
	movsd	-8(%r9,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	movq	160(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	movq	568(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm2
	addsd	%xmm0, %xmm2
	movq	144(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	192(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r9,%rcx,8), %xmm2     # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	624(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	movq	288(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
	movq	136(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	616(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%r9,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	movq	608(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	movq	280(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm2
	addsd	%xmm0, %xmm2
	movq	96(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbp,%rsi), %xmm1      # xmm1 = mem[0],zero
	movq	848(%rsp), %rbp         # 8-byte Reload
	movsd	-8(%rbp,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	movsd	(%r10,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movq	824(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm4      # xmm4 = mem[0],zero
	movq	272(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%rsi), %xmm1
	mulsd	%xmm3, %xmm4
	movq	504(%rsp), %r9          # 8-byte Reload
	movsd	-8(%r9,%rcx,8), %xmm5   # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%r15,%rsi), %xmm2
	addsd	%xmm4, %xmm2
	movq	560(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm5
	addsd	%xmm2, %xmm5
	movq	184(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	addsd	%xmm5, %xmm0
	movq	816(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm3
	addsd	%xmm0, %xmm3
	movq	88(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm3, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rax,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rbp,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	(%r10,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movq	552(%rsp), %rax         # 8-byte Reload
	addsd	(%rax,%rsi), %xmm1
	movq	808(%rsp), %rax         # 8-byte Reload
	movsd	(%rax,%rsi), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	(%r9,%rcx,8), %xmm5     # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%r14,%rsi), %xmm2
	movq	544(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm5
	addsd	%xmm4, %xmm2
	addsd	%xmm2, %xmm5
	movq	296(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm0
	movq	488(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm3
	addsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm3
	movq	80(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm3, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rbx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%rbp,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	movsd	(%r10,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	480(%rsp), %rax         # 8-byte Reload
	movsd	(%rax,%rsi), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	8(%r9,%rcx,8), %xmm5    # xmm5 = mem[0],zero
	movq	536(%rsp), %rax         # 8-byte Reload
	addsd	(%rax,%rsi), %xmm1
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	(%r8,%rsi), %xmm2
	addsd	%xmm4, %xmm2
	movq	264(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm5
	addsd	%xmm2, %xmm5
	movq	600(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm0
	addsd	%xmm5, %xmm0
	movq	472(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm3
	addsd	%xmm0, %xmm3
	movq	72(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm3, (%rdx,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdi,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movsd	-8(%rdx,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	(%r10,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movq	800(%rsp), %rax         # 8-byte Reload
	movsd	(%rax,%rsi), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	240(%rsp), %rax         # 8-byte Reload
	addsd	(%rax,%rsi), %xmm1
	movq	672(%rsp), %rax         # 8-byte Reload
	movsd	-8(%rax,%rcx,8), %xmm5  # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	mulsd	(%r11,%rsi), %xmm2
	addsd	%xmm1, %xmm4
	addsd	%xmm4, %xmm2
	movq	840(%rsp), %rdi         # 8-byte Reload
	mulsd	(%rdi,%rsi), %xmm5
	movq	592(%rsp), %rdi         # 8-byte Reload
	mulsd	(%rdi,%rsi), %xmm0
	addsd	%xmm2, %xmm5
	addsd	%xmm5, %xmm0
	movq	792(%rsp), %rdi         # 8-byte Reload
	mulsd	(%rdi,%rsi), %xmm3
	addsd	%xmm0, %xmm3
	movq	168(%rsp), %rbp         # 8-byte Reload
	movsd	%xmm3, (%rbp,%rcx,8)
	movsd	(%r13,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r12,%rsi), %xmm1      # xmm1 = mem[0],zero
	movsd	(%rdx,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	movsd	(%r10,%rcx,8), %xmm3    # xmm3 = mem[0],zero
	movq	784(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm4      # xmm4 = mem[0],zero
	movq	496(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%rsi), %xmm1
	mulsd	%xmm3, %xmm4
	movsd	(%rax,%rcx,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	584(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm0
	addsd	%xmm4, %xmm0
	movq	728(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm3
	addsd	%xmm0, %xmm3
	movq	16(%rsp), %rax          # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm2
	addsd	%xmm3, %xmm2
	movq	832(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rsi), %xmm5
	addsd	%xmm2, %xmm5
	movq	32(%rsp), %r12          # 8-byte Reload
	movsd	%xmm5, (%r12,%rcx,8)
	incq	%rcx
	addq	384(%rsp), %rsi         # 8-byte Folded Reload
	cmpl	%ecx, 48(%rsp)          # 4-byte Folded Reload
	jne	.LBB1_60
# BB#61:                                # %._crit_edge2855.loopexit
                                        #   in Loop: Header=BB1_58 Depth=3
	movl	1224(%rsp), %ebp        # 4-byte Reload
	addl	1144(%rsp), %ebp        # 4-byte Folded Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	232(%rsp), %edx         # 4-byte Reload
	movl	224(%rsp), %esi         # 4-byte Reload
	movq	776(%rsp), %r15         # 8-byte Reload
	movl	768(%rsp), %eax         # 4-byte Reload
	movl	1216(%rsp), %ebx        # 4-byte Reload
.LBB1_62:                               # %._crit_edge2855
                                        #   in Loop: Header=BB1_58 Depth=3
	addl	744(%rsp), %edx         # 4-byte Folded Reload
	addl	1200(%rsp), %ebp        # 4-byte Folded Reload
	addl	1208(%rsp), %r15d       # 4-byte Folded Reload
	addl	1192(%rsp), %esi        # 4-byte Folded Reload
	incl	%ebx
	addl	752(%rsp), %eax         # 4-byte Folded Reload
	cmpl	760(%rsp), %ebx         # 4-byte Folded Reload
	jne	.LBB1_58
	jmp	.LBB1_63
	.p2align	4, 0x90
.LBB1_56:                               #   in Loop: Header=BB1_55 Depth=2
	movl	%eax, %ebp
.LBB1_63:                               # %._crit_edge2865
                                        #   in Loop: Header=BB1_55 Depth=2
	addl	1288(%rsp), %edx        # 4-byte Folded Reload
	addl	1284(%rsp), %ebp        # 4-byte Folded Reload
	addl	1292(%rsp), %r15d       # 4-byte Folded Reload
	addl	1280(%rsp), %esi        # 4-byte Folded Reload
	movl	868(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 868(%rsp)         # 4-byte Spill
	cmpl	864(%rsp), %eax         # 4-byte Folded Reload
	movl	%ebp, %eax
	jne	.LBB1_55
	.p2align	4, 0x90
.LBB1_64:                               # %.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	720(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	movq	1336(%rsp), %rsi        # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rdi
	movq	1296(%rsp), %r14        # 8-byte Reload
	movq	1344(%rsp), %r15        # 8-byte Reload
	movq	1328(%rsp), %rdx        # 8-byte Reload
	movq	1312(%rsp), %rcx        # 8-byte Reload
	jl	.LBB1_2
.LBB1_65:                               # %._crit_edge2911
	xorl	%eax, %eax
	addq	$1496, %rsp             # imm = 0x5D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_SMG3BuildRAPSym, .Lfunc_end1-hypre_SMG3BuildRAPSym
	.cfi_endproc

	.globl	hypre_SMG3BuildRAPNoSym
	.p2align	4, 0x90
	.type	hypre_SMG3BuildRAPNoSym,@function
hypre_SMG3BuildRAPNoSym:                # @hypre_SMG3BuildRAPNoSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
	subq	$1400, %rsp             # imm = 0x578
.Lcfi28:
	.cfi_def_cfa_offset 1456
.Lcfi29:
	.cfi_offset %rbx, -56
.Lcfi30:
	.cfi_offset %r12, -48
.Lcfi31:
	.cfi_offset %r13, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%r9, 664(%rsp)          # 8-byte Spill
	movq	%r8, 1248(%rsp)         # 8-byte Spill
	movq	%rcx, %r12
	movq	%rdx, 1240(%rsp)        # 8-byte Spill
	movq	%rsi, 1232(%rsp)        # 8-byte Spill
	movq	%rdi, %r15
	movq	8(%r12), %rax
	movq	8(%rax), %rsi
	cmpl	$0, 8(%rsi)
	jle	.LBB2_67
# BB#1:                                 # %.preheader2624.lr.ph
	movq	8(%r15), %rcx
	movq	24(%r15), %rdx
	movl	8(%rdx), %ebp
	movq	16(%rcx), %rdx
	movq	16(%rax), %rax
	movq	%rax, 1200(%rsp)        # 8-byte Spill
	movl	$0, %edi
	movl	$0, %ecx
                                        # implicit-def: %RAX
	movq	%rax, 464(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 456(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 448(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 248(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 440(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 432(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 896(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 888(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 424(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 416(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1080(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1072(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1064(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 808(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1056(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1048(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1176(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1168(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1160(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1152(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1144(%rsp)        # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 1136(%rsp)        # 8-byte Spill
	movq	%r12, 672(%rsp)         # 8-byte Spill
	movq	%r15, 1224(%rsp)        # 8-byte Spill
	movq	%rsi, 1216(%rsp)        # 8-byte Spill
	movl	%ebp, 364(%rsp)         # 4-byte Spill
	movq	%rdx, 1208(%rsp)        # 8-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader2624
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_3 Depth 2
                                        #     Child Loop BB2_57 Depth 2
                                        #       Child Loop BB2_60 Depth 3
                                        #         Child Loop BB2_62 Depth 4
                                        #     Child Loop BB2_21 Depth 2
                                        #       Child Loop BB2_24 Depth 3
                                        #         Child Loop BB2_26 Depth 4
                                        #     Child Loop BB2_33 Depth 2
                                        #       Child Loop BB2_36 Depth 3
                                        #         Child Loop BB2_38 Depth 4
                                        #     Child Loop BB2_45 Depth 2
                                        #       Child Loop BB2_48 Depth 3
                                        #         Child Loop BB2_50 Depth 4
	movq	1200(%rsp), %rax        # 8-byte Reload
	movl	(%rax,%rdi,4), %eax
	movslq	%ecx, %rcx
	leaq	-1(%rcx), %rbx
	shlq	$3, %rcx
	leaq	(%rcx,%rcx,2), %r13
	.p2align	4, 0x90
.LBB2_3:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$24, %r13
	cmpl	%eax, 4(%rdx,%rbx,4)
	leaq	1(%rbx), %rbx
	jne	.LBB2_3
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%rsi), %rcx
	movq	%rdi, 112(%rsp)         # 8-byte Spill
	leaq	(,%rdi,8), %rax
	leaq	(%rax,%rax,2), %rax
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax), %rdi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	1248(%rsp), %rsi        # 8-byte Reload
	movq	664(%rsp), %rdx         # 8-byte Reload
	leaq	396(%rsp), %rcx
	callq	hypre_StructMapCoarseToFine
	movq	40(%r15), %rax
	movq	(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	1232(%rsp), %rbp        # 8-byte Reload
	movq	40(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	1240(%rsp), %r14        # 8-byte Reload
	movq	40(%r14), %rax
	movq	(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	40(%r12), %rax
	movq	(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%ebx, %esi
	leaq	4(%rsp), %r12
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbp, %rdi
	movq	%rbx, %rbp
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 880(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 872(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	movq	%rbx, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	movq	%r12, %r14
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %r12       # imm = 0x100000000
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%r15, %rbx
	movq	%r15, %rdi
	movl	%ebp, %esi
	movq	%r14, %r15
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 368(%rsp)         # 8-byte Spill
	cmpl	$8, 364(%rsp)           # 4-byte Folded Reload
	movq	%rbp, 1256(%rsp)        # 8-byte Spill
	jl	.LBB2_5
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 464(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 456(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 448(%rsp)         # 8-byte Spill
	movq	%r12, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 440(%rsp)         # 8-byte Spill
	movq	%r12, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 432(%rsp)         # 8-byte Spill
	cmpl	$16, 364(%rsp)          # 4-byte Folded Reload
	movq	%r15, %r14
	jl	.LBB2_7
# BB#9:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	$-1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 896(%rsp)         # 8-byte Spill
	movabsq	$-4294967295, %r12      # imm = 0xFFFFFFFF00000001
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 888(%rsp)         # 8-byte Spill
	movabsq	$8589934591, %rax       # imm = 0x1FFFFFFFF
	movq	%rax, %r15
	movq	%r15, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 424(%rsp)         # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 416(%rsp)         # 8-byte Spill
	movq	%r14, %r12
	cmpl	$20, 364(%rsp)          # 4-byte Folded Reload
	jl	.LBB2_10
# BB#11:                                #   in Loop: Header=BB2_2 Depth=1
	movq	$-1, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1080(%rsp)        # 8-byte Spill
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1072(%rsp)        # 8-byte Spill
	movq	%r15, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1064(%rsp)        # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, %r14
	movq	%r14, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 808(%rsp)         # 8-byte Spill
	movq	%r15, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1056(%rsp)        # 8-byte Spill
	movq	%r14, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%r12, %r14
	movq	%r12, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1048(%rsp)        # 8-byte Spill
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_5:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r15, %r14
	xorl	%ebx, %ebx
	movq	%r12, %r15
	jmp	.LBB2_8
	.p2align	4, 0x90
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	xorl	%ebx, %ebx
	movabsq	$4294967296, %r15       # imm = 0x100000000
.LBB2_8:                                # %.thread2556
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	672(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB2_13
.LBB2_10:                               #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %r14
.LBB2_12:                               # %.thread2556
                                        #   in Loop: Header=BB2_2 Depth=1
	movabsq	$4294967296, %r15       # imm = 0x100000000
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	672(%rsp), %rbp         # 8-byte Reload
	movb	$1, %bl
.LBB2_13:                               # %.thread2556
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	$1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 864(%rsp)         # 8-byte Spill
	movq	%r15, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 856(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 848(%rsp)         # 8-byte Spill
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 840(%rsp)         # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 832(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	movq	%rax, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 824(%rsp)         # 8-byte Spill
	movq	%r15, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 816(%rsp)         # 8-byte Spill
	testb	%bl, %bl
	je	.LBB2_15
# BB#14:                                #   in Loop: Header=BB2_2 Depth=1
	movabsq	$8589934591, %rbx       # imm = 0x1FFFFFFFF
	movq	%rbx, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movl	%r12d, %esi
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1176(%rsp)        # 8-byte Spill
	movabsq	$4294967297, %r12       # imm = 0x100000001
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%rbp, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1168(%rsp)        # 8-byte Spill
	movq	$-1, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1160(%rsp)        # 8-byte Spill
	movabsq	$-4294967295, %rax      # imm = 0xFFFFFFFF00000001
	movq	%rax, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1152(%rsp)        # 8-byte Spill
	movq	%rbx, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1144(%rsp)        # 8-byte Spill
	movq	%r12, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	%rbp, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 1136(%rsp)        # 8-byte Spill
.LBB2_15:                               #   in Loop: Header=BB2_2 Depth=1
	movq	$0, 4(%rsp)
	movl	$1, 12(%rsp)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	-12(%rbx,%r13), %r10d
	movl	-8(%rbx,%r13), %edx
	movl	-24(%rbx,%r13), %esi
	movl	%r10d, %eax
	subl	%esi, %eax
	incl	%eax
	cmpl	%esi, %r10d
	movl	-20(%rbx,%r13), %esi
	movl	$0, %r8d
	cmovsl	%r8d, %eax
	movl	%edx, %ebp
	subl	%esi, %ebp
	incl	%ebp
	cmpl	%esi, %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	-12(%rsi,%r13), %ecx
	movl	-8(%rsi,%r13), %r9d
	movl	-24(%rsi,%r13), %edi
	cmovsl	%r8d, %ebp
	movl	%ecx, %edx
	subl	%edi, %edx
	incl	%edx
	cmpl	%edi, %ecx
	movl	-20(%rsi,%r13), %ecx
	cmovsl	%r8d, %edx
	movl	%r9d, %edi
	subl	%ecx, %edi
	incl	%edi
	cmpl	%ecx, %r9d
	cmovsl	%r8d, %edi
	movq	%r15, 4(%rsp)
	movl	$0, 12(%rsp)
	movl	-24(%rbx,%r13), %esi
	movl	%r10d, %ebx
	subl	%esi, %ebx
	incl	%ebx
	cmpl	%esi, %r10d
	cmovsl	%r8d, %ebx
	imull	%edx, %edi
	imull	%eax, %ebp
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax), %r15
	movl	$1, 4(%rsp)
	movl	$0, 8(%rsp)
	movl	$0, 12(%rsp)
	movl	364(%rsp), %eax         # 4-byte Reload
	cmpl	$19, %eax
	movl	%ebp, 152(%rsp)         # 4-byte Spill
	movl	%edi, 144(%rsp)         # 4-byte Spill
	movl	%ebx, 136(%rsp)         # 4-byte Spill
	je	.LBB2_42
# BB#16:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$15, %eax
	je	.LBB2_30
# BB#17:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$7, %eax
	jne	.LBB2_54
# BB#18:                                #   in Loop: Header=BB2_2 Depth=1
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	228(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r15), %r15d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	4(%rdx,%rcx), %r8d
	movl	12(%rdx,%rcx), %eax
	movl	%eax, %r12d
	subl	%r15d, %r12d
	incl	%r12d
	cmpl	%r15d, %eax
	movl	16(%rdx,%rcx), %eax
	movl	$0, %ebp
	cmovsl	%ebp, %r12d
	movl	%eax, %edx
	subl	%r8d, %edx
	incl	%edx
	cmpl	%r8d, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %r10d
	movl	-20(%rcx,%r13), %r11d
	movl	-12(%rcx,%r13), %eax
	cmovsl	%ebp, %edx
	movl	%edx, 288(%rsp)         # 4-byte Spill
	movl	%eax, %edx
	subl	%r10d, %edx
	incl	%edx
	cmpl	%r10d, %eax
	movl	-8(%rcx,%r13), %eax
	cmovsl	%ebp, %edx
	movl	%edx, 280(%rsp)         # 4-byte Spill
	movl	%eax, %r14d
	subl	%r11d, %r14d
	incl	%r14d
	cmpl	%r11d, %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %r9d
	movl	-20(%rcx,%r13), %edi
	movl	-12(%rcx,%r13), %eax
	cmovsl	%ebp, %r14d
	movl	%eax, %edx
	subl	%r9d, %edx
	incl	%edx
	cmpl	%r9d, %eax
	movl	-8(%rcx,%r13), %eax
	cmovsl	%ebp, %edx
	movl	%edx, 336(%rsp)         # 4-byte Spill
	movl	%eax, %edx
	subl	%edi, %edx
	incl	%edx
	cmpl	%edi, %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebx
	movl	-12(%rcx,%r13), %eax
	cmovsl	%ebp, %edx
	movl	%edx, 272(%rsp)         # 4-byte Spill
	movl	%eax, %esi
	subl	%ebx, %esi
	incl	%esi
	cmpl	%ebx, %eax
	movl	-20(%rcx,%r13), %edx
	cmovsl	%ebp, %esi
	movl	%esi, 328(%rsp)         # 4-byte Spill
	movl	-8(%rcx,%r13), %eax
	movl	%eax, %ecx
	subl	%edx, %ecx
	incl	%ecx
	cmpl	%edx, %eax
	cmovsl	%ebp, %ecx
	movl	%ecx, 264(%rsp)         # 4-byte Spill
	movl	228(%rsp), %esi
	movl	232(%rsp), %ecx
	cmpl	%esi, %ecx
	movl	%esi, %eax
	movl	%ecx, 344(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %eax
	movl	236(%rsp), %ecx
	cmpl	%eax, %ecx
	movl	%ecx, 376(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %eax
	testl	%eax, %eax
	jle	.LBB2_66
# BB#19:                                # %.lr.ph2712
                                        #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 376(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_66
# BB#20:                                # %.preheader2618.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	104(%rsp), %rbp         # 8-byte Reload
	movl	%r11d, 88(%rsp)         # 4-byte Spill
	movl	%edi, 80(%rsp)          # 4-byte Spill
	movq	24(%rsp), %r11          # 8-byte Reload
	movl	%r15d, 96(%rsp)         # 4-byte Spill
	movl	8(%rbp,%r11), %edi
	movl	%edi, %r15d
	movq	48(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r15d
	movl	%r8d, 64(%rsp)          # 4-byte Spill
	movl	%edi, %r8d
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r8d
	movl	%r10d, 56(%rsp)         # 4-byte Spill
	movl	404(%rsp), %r10d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r10d
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r13d
	movl	%r13d, %eax
	subl	%ebx, %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	subl	8(%rcx,%r11), %edi
	movl	4(%rbp,%r11), %ebx
	movl	%ebx, %ebp
	subl	%edx, %ebp
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	264(%rsp), %r11d        # 4-byte Reload
	imull	%r11d, %r15d
	addl	%ebp, %r15d
	movl	%r13d, %eax
	subl	%r9d, %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%ebx, %ecx
	subl	80(%rsp), %ecx          # 4-byte Folded Reload
	movl	272(%rsp), %r9d         # 4-byte Reload
	imull	%r9d, %r8d
	addl	%ecx, %r8d
	movl	396(%rsp), %eax
	subl	56(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	400(%rsp), %esi
	subl	88(%rsp), %esi          # 4-byte Folded Reload
	imull	%r14d, %r10d
	addl	%esi, %r10d
	subl	96(%rsp), %r13d         # 4-byte Folded Reload
	subl	64(%rsp), %ebx          # 4-byte Folded Reload
	movl	288(%rsp), %edx         # 4-byte Reload
	imull	%edx, %edi
	addl	%ebx, %edi
	movl	280(%rsp), %ebx         # 4-byte Reload
	imull	%ebx, %r10d
	movq	664(%rsp), %rbp         # 8-byte Reload
	movl	4(%rbp), %eax
	imull	%ebx, %eax
	imull	%r14d, %ebx
	movl	%r8d, %ecx
	imull	8(%rbp), %ebx
	movl	344(%rsp), %r8d         # 4-byte Reload
	movl	%r8d, %esi
	imull	%eax, %esi
	subl	%esi, %ebx
	movl	%ebx, 280(%rsp)         # 4-byte Spill
	movslq	(%rbp), %rbx
	movl	%ebx, %esi
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	imull	16(%rsp), %esi          # 4-byte Folded Reload
	subl	%esi, %eax
	movl	%eax, 616(%rsp)         # 4-byte Spill
	imull	%r12d, %edi
	addl	%r13d, %edi
	addl	40(%rsp), %r10d         # 4-byte Folded Reload
	movl	328(%rsp), %r14d        # 4-byte Reload
	imull	%r14d, %r15d
	movl	336(%rsp), %r13d        # 4-byte Reload
	imull	%r13d, %ecx
	subl	%r8d, %r11d
	imull	%r14d, %r11d
	movl	%r11d, 264(%rsp)        # 4-byte Spill
	movl	%r15d, %esi
	movq	16(%rsp), %rbp          # 8-byte Reload
	subl	%r8d, %r9d
	imull	%r13d, %r9d
	movl	%r9d, 272(%rsp)         # 4-byte Spill
	subl	%r8d, %edx
	imull	%r12d, %edx
	movl	%edx, 288(%rsp)         # 4-byte Spill
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	movl	%r13d, %edx
	subl	%ebp, %edx
	movl	%edx, 608(%rsp)         # 4-byte Spill
	movslq	136(%rsp), %r11         # 4-byte Folded Reload
	movslq	152(%rsp), %r8          # 4-byte Folded Reload
	addl	48(%rsp), %esi          # 4-byte Folded Reload
	leal	-1(%rbp), %edx
	incq	%rdx
	movq	%rdx, 592(%rsp)         # 8-byte Spill
	imulq	%rbx, %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movl	$0, 384(%rsp)           # 4-byte Folded Spill
	testl	%ebp, %ebp
	movl	$0, %eax
	cmovnsl	%ebp, %eax
	movq	256(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r8,8), %rdx
	leaq	(,%r11,8), %r8
	movq	%rdx, %rbx
	subq	%r8, %rbx
	movq	%rbx, 584(%rsp)         # 8-byte Spill
	addl	%eax, %r13d
	addl	%eax, %r14d
	addl	%r12d, %eax
	movslq	144(%rsp), %r12         # 4-byte Folded Reload
	leaq	(,%r12,8), %r8
	movq	184(%rsp), %r9          # 8-byte Reload
	movq	%r9, %rbx
	subq	%r8, %rbx
	movq	%rbx, 576(%rsp)         # 8-byte Spill
	movq	208(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rbx
	subq	%r8, %rbx
	movq	%rbx, 568(%rsp)         # 8-byte Spill
	subl	%ebp, %r13d
	movl	%r13d, 336(%rsp)        # 4-byte Spill
	subl	%ebp, %r14d
	movl	%r14d, 328(%rsp)        # 4-byte Spill
	movq	%rdx, 320(%rsp)         # 8-byte Spill
	leaq	(%rdx,%r11,8), %rdx
	movq	%rdx, 560(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r11,8), %rdx
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	216(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r11,8), %rdx
	movq	%rdx, 304(%rsp)         # 8-byte Spill
	movq	%r10, %rdx
	subl	%ebp, %eax
	movl	%eax, 600(%rsp)         # 4-byte Spill
	shlq	$3, %r8
	movq	176(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %rax
	movq	%rax, 552(%rsp)         # 8-byte Spill
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %rax
	movq	%rax, 544(%rsp)         # 8-byte Spill
	leaq	(%r9,%r12,8), %rax
	movq	%rax, 536(%rsp)         # 8-byte Spill
	leaq	(%r15,%r12,8), %rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r12,8), %rax
	movq	%rax, 520(%rsp)         # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_21:                               # %.preheader2618
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_24 Depth 3
                                        #         Child Loop BB2_26 Depth 4
	cmpl	$0, 344(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_22
# BB#23:                                # %.preheader2614.preheader
                                        #   in Loop: Header=BB2_21 Depth=2
	xorl	%ebx, %ebx
	movl	%ecx, %eax
	.p2align	4, 0x90
.LBB2_24:                               # %.preheader2614
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_21 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_26 Depth 4
	testl	%ebp, %ebp
	jle	.LBB2_28
# BB#25:                                # %.lr.ph2688.preheader
                                        #   in Loop: Header=BB2_24 Depth=3
	movl	%ebx, 624(%rsp)         # 4-byte Spill
	movl	%eax, 632(%rsp)         # 4-byte Spill
	movl	%ecx, 352(%rsp)         # 4-byte Spill
	movslq	%ecx, %rax
	movq	880(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r10
	movq	872(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r12
	movl	%esi, 200(%rsp)         # 4-byte Spill
	movslq	%esi, %r11
	shlq	$3, %r11
	movl	%edi, 128(%rsp)         # 4-byte Spill
	movslq	%edi, %r9
	shlq	$3, %r9
	movslq	%edx, %rcx
	movq	560(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	320(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rdi
	movq	584(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	304(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	addq	216(%rsp), %r11         # 8-byte Folded Reload
	movq	816(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	832(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	848(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	840(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	824(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	856(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	addq	864(%rsp), %r9          # 8-byte Folded Reload
	movq	192(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	%rax, 640(%rsp)         # 8-byte Spill
	movq	576(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	568(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	536(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rbp
	movq	528(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %r8
	movq	296(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rbx
	movq	520(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,8), %rax
	movq	552(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %r13
	movq	544(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,8), %r14
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_26:                               # %.lr.ph2688
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_21 Depth=2
                                        #       Parent Loop BB2_24 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r10,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rbp,%rsi), %xmm0
	movq	24(%rsp), %r15          # 8-byte Reload
	mulsd	(%r15,%rcx,8), %xmm0
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm0, (%rdx,%rcx,8)
	movsd	(%r10,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r8,%rsi), %xmm0
	mulsd	8(%rdi,%rcx,8), %xmm0
	movq	104(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm0, (%rdx,%rcx,8)
	movsd	(%rdi,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	96(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%rsi), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r10,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%rbx,%rsi), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	mulsd	(%rax,%rsi), %xmm2
	addsd	%xmm3, %xmm2
	movq	176(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%rcx,8)
	movsd	(%r10,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r13,%rsi), %xmm0
	mulsd	-8(%rdi,%rcx,8), %xmm0
	movq	168(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm0, (%rdx,%rcx,8)
	movsd	(%r10,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%r14,%rsi), %xmm0
	movq	48(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rcx,8), %xmm0
	movq	160(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm0, (%rdx,%rcx,8)
	movsd	(%r12,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	144(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	movq	40(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rcx,8), %xmm0
	movq	88(%rsp), %rdx          # 8-byte Reload
	addsd	(%rdx,%rsi), %xmm0
	movsd	(%r10,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rbp,%rsi), %xmm1
	movq	32(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%rcx,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	152(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm1, (%rdx,%rcx,8)
	movsd	(%r12,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movq	136(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%rsi), %xmm0
	movq	72(%rsp), %rdx          # 8-byte Reload
	mulsd	8(%rdx,%rcx,8), %xmm0
	movq	80(%rsp), %rdx          # 8-byte Reload
	addsd	(%rdx,%rsi), %xmm0
	movsd	(%r10,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%r8,%rsi), %xmm1
	mulsd	8(%r11,%rcx,8), %xmm1
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%r9,%rcx,8)
	incq	%rcx
	addq	64(%rsp), %rsi          # 8-byte Folded Reload
	cmpl	%ecx, 16(%rsp)          # 4-byte Folded Reload
	jne	.LBB2_26
# BB#27:                                # %._crit_edge2689.loopexit
                                        #   in Loop: Header=BB2_24 Depth=3
	movl	632(%rsp), %eax         # 4-byte Reload
	addl	592(%rsp), %eax         # 4-byte Folded Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	128(%rsp), %edi         # 4-byte Reload
	movl	200(%rsp), %esi         # 4-byte Reload
	movl	352(%rsp), %ecx         # 4-byte Reload
	movq	640(%rsp), %rdx         # 8-byte Reload
	movl	624(%rsp), %ebx         # 4-byte Reload
.LBB2_28:                               # %._crit_edge2689
                                        #   in Loop: Header=BB2_24 Depth=3
	addl	328(%rsp), %esi         # 4-byte Folded Reload
	addl	608(%rsp), %eax         # 4-byte Folded Reload
	addl	616(%rsp), %edx         # 4-byte Folded Reload
	addl	600(%rsp), %edi         # 4-byte Folded Reload
	incl	%ebx
	addl	336(%rsp), %ecx         # 4-byte Folded Reload
	cmpl	344(%rsp), %ebx         # 4-byte Folded Reload
	jne	.LBB2_24
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_22:                               #   in Loop: Header=BB2_21 Depth=2
	movl	%ecx, %eax
.LBB2_29:                               # %._crit_edge2699
                                        #   in Loop: Header=BB2_21 Depth=2
	addl	264(%rsp), %esi         # 4-byte Folded Reload
	addl	272(%rsp), %eax         # 4-byte Folded Reload
	addl	280(%rsp), %edx         # 4-byte Folded Reload
	addl	288(%rsp), %edi         # 4-byte Folded Reload
	movl	384(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	movl	%ecx, 384(%rsp)         # 4-byte Spill
	cmpl	376(%rsp), %ecx         # 4-byte Folded Reload
	movl	%eax, %ecx
	jne	.LBB2_21
	jmp	.LBB2_66
	.p2align	4, 0x90
.LBB2_30:                               #   in Loop: Header=BB2_2 Depth=1
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	228(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r15), %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	4(%rdx,%rcx), %edi
	movl	12(%rdx,%rcx), %eax
	movl	%eax, %r10d
	subl	%esi, %r10d
	incl	%r10d
	movl	%esi, 96(%rsp)          # 4-byte Spill
	cmpl	%esi, %eax
	movl	16(%rdx,%rcx), %ecx
	movl	$0, %eax
	cmovsl	%eax, %r10d
	movl	%ecx, %r11d
	subl	%edi, %r11d
	incl	%r11d
	movl	%edi, 64(%rsp)          # 4-byte Spill
	cmpl	%edi, %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	-24(%rdx,%r13), %esi
	movl	-20(%rdx,%r13), %r12d
	movl	-12(%rdx,%r13), %ecx
	cmovsl	%eax, %r11d
	movl	%ecx, %r9d
	subl	%esi, %r9d
	incl	%r9d
	movl	%esi, 128(%rsp)         # 4-byte Spill
	cmpl	%esi, %ecx
	movl	-8(%rdx,%r13), %edx
	cmovsl	%eax, %r9d
	movl	%edx, %ecx
	subl	%r12d, %ecx
	incl	%ecx
	cmpl	%r12d, %edx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movl	-24(%rsi,%r13), %ebp
	movl	-20(%rsi,%r13), %ebx
	movl	-12(%rsi,%r13), %edx
	cmovsl	%eax, %ecx
	movl	%edx, %edi
	subl	%ebp, %edi
	incl	%edi
	movl	%ebp, 88(%rsp)          # 4-byte Spill
	cmpl	%ebp, %edx
	movl	-8(%rsi,%r13), %edx
	cmovsl	%eax, %edi
	movl	%edi, 304(%rsp)         # 4-byte Spill
	movl	%edx, %ebp
	subl	%ebx, %ebp
	incl	%ebp
	movl	%ebx, 80(%rsp)          # 4-byte Spill
	cmpl	%ebx, %edx
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	-24(%rsi,%r13), %r15d
	movl	-12(%rsi,%r13), %edx
	cmovsl	%eax, %ebp
	movl	%edx, %edi
	subl	%r15d, %edi
	incl	%edi
	cmpl	%r15d, %edx
	movl	-20(%rsi,%r13), %edx
	cmovsl	%eax, %edi
	movl	%edi, 296(%rsp)         # 4-byte Spill
	movl	-8(%rsi,%r13), %edi
	movl	%edi, %r8d
	subl	%edx, %r8d
	incl	%r8d
	cmpl	%edx, %edi
	cmovsl	%eax, %r8d
	movl	228(%rsp), %r14d
	movl	232(%rsp), %eax
	cmpl	%r14d, %eax
	movl	%r14d, %edi
	movl	%eax, 312(%rsp)         # 4-byte Spill
	cmovgel	%eax, %edi
	movl	236(%rsp), %eax
	cmpl	%edi, %eax
	movl	%eax, 648(%rsp)         # 4-byte Spill
	cmovgel	%eax, %edi
	testl	%edi, %edi
	jle	.LBB2_66
# BB#31:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 648(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_66
# BB#32:                                # %.preheader2619.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movl	%edx, 200(%rsp)         # 4-byte Spill
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	8(%rax,%rsi), %edx
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax,2), %rdi
	movl	%edx, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	8(%rax,%rdi,8), %ebx
	movl	%ebx, 192(%rsp)         # 4-byte Spill
	movl	%r12d, 32(%rsp)         # 4-byte Spill
	movl	404(%rsp), %r12d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r12d
	movl	%edx, %r14d
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r14d
	movq	48(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %edx
	movl	%edx, 320(%rsp)         # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	4(%rax,%rsi), %eax
	movq	664(%rsp), %rsi         # 8-byte Reload
	movl	4(%rsi), %edi
	imull	%r9d, %edi
	movl	%r15d, 24(%rsp)         # 4-byte Spill
	movl	%r9d, %ebx
	imull	%ecx, %ebx
	imull	8(%rsi), %ebx
	movl	312(%rsp), %r13d        # 4-byte Reload
	movl	%r13d, %edx
	imull	%edi, %edx
	subl	%edx, %ebx
	movl	%ebx, 472(%rsp)         # 4-byte Spill
	movslq	(%rsi), %r15
	movl	%r15d, %edx
	imull	16(%rsp), %edx          # 4-byte Folded Reload
	subl	%edx, %edi
	movl	%edi, 552(%rsp)         # 4-byte Spill
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %edx
	movl	%edx, %esi
	subl	96(%rsp), %esi          # 4-byte Folded Reload
	movl	%eax, %edi
	subl	64(%rsp), %edi          # 4-byte Folded Reload
	movl	192(%rsp), %ebx         # 4-byte Reload
	imull	%r11d, %ebx
	addl	%edi, %ebx
	imull	%r10d, %ebx
	addl	%esi, %ebx
	movl	%ebx, 192(%rsp)         # 4-byte Spill
	movl	396(%rsp), %esi
	subl	128(%rsp), %esi         # 4-byte Folded Reload
	movl	400(%rsp), %edi
	subl	32(%rsp), %edi          # 4-byte Folded Reload
	imull	%ecx, %r12d
	addl	%edi, %r12d
	imull	%r9d, %r12d
	addl	%esi, %r12d
	movl	%edx, %ecx
	subl	88(%rsp), %ecx          # 4-byte Folded Reload
	movl	%eax, %esi
	subl	80(%rsp), %esi          # 4-byte Folded Reload
	movl	320(%rsp), %edi         # 4-byte Reload
	imull	%ebp, %r14d
	addl	%esi, %r14d
	movl	304(%rsp), %r9d         # 4-byte Reload
	imull	%r9d, %r14d
	addl	%ecx, %r14d
	subl	24(%rsp), %edx          # 4-byte Folded Reload
	subl	200(%rsp), %eax         # 4-byte Folded Reload
	imull	%r8d, %edi
	addl	%eax, %edi
	movl	%r10d, %esi
	movl	296(%rsp), %r10d        # 4-byte Reload
	imull	%r10d, %edi
	addl	%edx, %edi
	subl	%r13d, %r8d
	imull	%r10d, %r8d
	movl	%r8d, 240(%rsp)         # 4-byte Spill
	subl	%r13d, %ebp
	imull	%r9d, %ebp
	movl	%ebp, 408(%rsp)         # 4-byte Spill
	subl	%r13d, %r11d
	imull	%esi, %r11d
	movl	%r11d, 704(%rsp)        # 4-byte Spill
	movl	%r9d, %eax
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 544(%rsp)         # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	incq	%rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	imulq	%r15, %rax
	movq	%rax, 520(%rsp)         # 8-byte Spill
	movl	$0, 656(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	$0, %ecx
	cmovnsl	16(%rsp), %ecx          # 4-byte Folded Reload
	addl	%ecx, %r9d
	addl	%ecx, %r10d
	addl	%esi, %ecx
	movslq	144(%rsp), %rax         # 4-byte Folded Reload
	leaq	(,%rax,8), %rdx
	movq	184(%rsp), %rbx         # 8-byte Reload
	subq	%rdx, %rbx
	movq	%rbx, 384(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rsi
	subq	%rdx, %rsi
	movq	%rsi, 288(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rsi
	subq	%rdx, %rsi
	movq	%rsi, 280(%rsp)         # 8-byte Spill
	movq	456(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rsi
	subq	%rdx, %rsi
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	movslq	136(%rsp), %r13         # 4-byte Folded Reload
	movslq	152(%rsp), %r8          # 4-byte Folded Reload
	subl	16(%rsp), %r9d          # 4-byte Folded Reload
	movl	%r9d, 304(%rsp)         # 4-byte Spill
	leaq	(,%r13,8), %r9
	leaq	(,%r8,8), %rsi
	subq	%r9, %rsi
	leaq	(%r9,%r8,8), %rdx
	subl	16(%rsp), %r10d         # 4-byte Folded Reload
	movl	%r10d, 296(%rsp)        # 4-byte Spill
	movq	256(%rsp), %r9          # 8-byte Reload
	addq	%r9, %rdx
	movq	%rdx, 376(%rsp)         # 8-byte Spill
	leaq	(%r9,%r8,8), %rdx
	movq	%rdx, 776(%rsp)         # 8-byte Spill
	addq	%r9, %rsi
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	leaq	(%r9,%r13,8), %rdx
	movq	%rdx, 512(%rsp)         # 8-byte Spill
	movl	192(%rsp), %esi         # 4-byte Reload
	movq	216(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r13,8), %rdx
	movq	%rdx, 504(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rdx          # 8-byte Reload
	subl	%edx, %ecx
	movl	%ecx, 536(%rsp)         # 4-byte Spill
	shlq	$3, %r15
	movq	176(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 768(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 760(%rsp)         # 8-byte Spill
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 752(%rsp)         # 8-byte Spill
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 744(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rax,8), %rcx
	movq	%rcx, 736(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rax,8), %rcx
	movq	%rcx, 496(%rsp)         # 8-byte Spill
	leaq	(%r11,%rax,8), %rcx
	movq	%rcx, 488(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 480(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 728(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 720(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 712(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movl	%r14d, %eax
	movq	%r15, 592(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_33:                               # %.preheader2619
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_36 Depth 3
                                        #         Child Loop BB2_38 Depth 4
	cmpl	$0, 312(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_34
# BB#35:                                # %.preheader2615.preheader
                                        #   in Loop: Header=BB2_33 Depth=2
	xorl	%ebp, %ebp
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB2_36:                               # %.preheader2615
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_33 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_38 Depth 4
	testl	%edx, %edx
	jle	.LBB2_40
# BB#37:                                # %.lr.ph2658.preheader
                                        #   in Loop: Header=BB2_36 Depth=3
	movl	%ebp, 560(%rsp)         # 4-byte Spill
	movl	%ecx, 568(%rsp)         # 4-byte Spill
	movl	%eax, 576(%rsp)         # 4-byte Spill
	cltq
	movq	880(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r10
	movq	872(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r8
	movl	%edi, 320(%rsp)         # 4-byte Spill
	movslq	%edi, %r11
	shlq	$3, %r11
	movl	%esi, 192(%rsp)         # 4-byte Spill
	movslq	%esi, %r14
	shlq	$3, %r14
	movslq	%r12d, %rbx
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	776(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %r9
	movq	264(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	512(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	504(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	256(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	addq	216(%rsp), %r11         # 8-byte Folded Reload
	movq	816(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	832(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	848(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	840(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	824(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	856(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	addq	864(%rsp), %r14         # 8-byte Folded Reload
	movq	520(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx), %rax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	movq	384(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	288(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	280(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	248(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdi
	movq	456(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %r12
	movq	368(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	464(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	448(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 640(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 632(%rsp)         # 8-byte Spill
	movq	744(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %r15
	movq	736(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 624(%rsp)         # 8-byte Spill
	movq	496(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rdx
	movq	488(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movq	768(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	480(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movq	760(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 616(%rsp)         # 8-byte Spill
	movq	728(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 608(%rsp)         # 8-byte Spill
	movq	752(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	%rax, 600(%rsp)         # 8-byte Spill
	movq	720(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rcx
	movq	712(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	120(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rbx,8), %r13
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_38:                               # %.lr.ph2658
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_33 Depth=2
                                        #       Parent Loop BB2_36 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r10,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r15,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	624(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%rdi,%rbx), %xmm2
	addsd	%xmm0, %xmm2
	movq	56(%rsp), %rsi          # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbp,8)
	movsd	(%r10,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%rdx,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	8(%r9,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	344(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm0
	addsd	%xmm1, %xmm0
	mulsd	(%r12,%rbx), %xmm2
	addsd	%xmm0, %xmm2
	movq	104(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbp,8)
	movsd	(%r9,%rbp,8), %xmm0     # xmm0 = mem[0],zero
	movq	80(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r10,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	movq	336(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rbx), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	328(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm2
	addsd	%xmm3, %xmm2
	movq	176(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbp,8)
	movsd	(%r10,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movq	616(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r9,%rbp,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	608(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm0
	addsd	%xmm1, %xmm0
	movq	64(%rsp), %rsi          # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm2
	addsd	%xmm0, %xmm2
	movq	168(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbp,8)
	movsd	(%r10,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	movq	600(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	48(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	mulsd	(%rcx,%rbx), %xmm0
	addsd	%xmm1, %xmm0
	movq	128(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm2
	addsd	%xmm0, %xmm2
	movq	160(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm2, (%rsi,%rbp,8)
	movsd	(%r8,%rbp,8), %xmm0     # xmm0 = mem[0],zero
	movq	144(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	40(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbp,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	200(%rsp), %rsi         # 8-byte Reload
	addsd	(%rsi,%rbx), %xmm1
	movsd	(%r10,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%r15,%rbx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	32(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbp,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	352(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm2
	addsd	%xmm4, %xmm2
	mulsd	(%rdi,%rbx), %xmm5
	addsd	%xmm2, %xmm5
	movq	136(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm0
	addsd	%xmm5, %xmm0
	mulsd	(%rax,%rbx), %xmm3
	addsd	%xmm0, %xmm3
	movq	152(%rsp), %rsi         # 8-byte Reload
	movsd	%xmm3, (%rsi,%rbp,8)
	movsd	(%r8,%rbp,8), %xmm0     # xmm0 = mem[0],zero
	movq	96(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rbx), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	72(%rsp), %rsi          # 8-byte Reload
	movsd	8(%rsi,%rbp,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	640(%rsp), %rsi         # 8-byte Reload
	addsd	(%rsi,%rbx), %xmm1
	movsd	(%r10,%rbp,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%rdx,%rbx), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	8(%r11,%rbp,8), %xmm5   # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	632(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm2
	addsd	%xmm4, %xmm2
	mulsd	(%r12,%rbx), %xmm5
	addsd	%xmm2, %xmm5
	movq	88(%rsp), %rsi          # 8-byte Reload
	mulsd	(%rsi,%rbx), %xmm0
	addsd	%xmm5, %xmm0
	mulsd	(%r13,%rbx), %xmm3
	addsd	%xmm0, %xmm3
	movsd	%xmm3, (%r14,%rbp,8)
	incq	%rbp
	addq	592(%rsp), %rbx         # 8-byte Folded Reload
	cmpl	%ebp, 16(%rsp)          # 4-byte Folded Reload
	jne	.LBB2_38
# BB#39:                                # %._crit_edge2659.loopexit
                                        #   in Loop: Header=BB2_36 Depth=3
	movl	568(%rsp), %ecx         # 4-byte Reload
	addl	528(%rsp), %ecx         # 4-byte Folded Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	320(%rsp), %edi         # 4-byte Reload
	movl	192(%rsp), %esi         # 4-byte Reload
	movq	584(%rsp), %r12         # 8-byte Reload
	movl	576(%rsp), %eax         # 4-byte Reload
	movl	560(%rsp), %ebp         # 4-byte Reload
.LBB2_40:                               # %._crit_edge2659
                                        #   in Loop: Header=BB2_36 Depth=3
	addl	296(%rsp), %edi         # 4-byte Folded Reload
	addl	544(%rsp), %ecx         # 4-byte Folded Reload
	addl	552(%rsp), %r12d        # 4-byte Folded Reload
	addl	536(%rsp), %esi         # 4-byte Folded Reload
	incl	%ebp
	addl	304(%rsp), %eax         # 4-byte Folded Reload
	cmpl	312(%rsp), %ebp         # 4-byte Folded Reload
	jne	.LBB2_36
	jmp	.LBB2_41
	.p2align	4, 0x90
.LBB2_34:                               #   in Loop: Header=BB2_33 Depth=2
	movl	%eax, %ecx
.LBB2_41:                               # %._crit_edge2669
                                        #   in Loop: Header=BB2_33 Depth=2
	addl	240(%rsp), %edi         # 4-byte Folded Reload
	addl	408(%rsp), %ecx         # 4-byte Folded Reload
	addl	472(%rsp), %r12d        # 4-byte Folded Reload
	addl	704(%rsp), %esi         # 4-byte Folded Reload
	movl	656(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 656(%rsp)         # 4-byte Spill
	cmpl	648(%rsp), %eax         # 4-byte Folded Reload
	movl	%ecx, %eax
	jne	.LBB2_33
	jmp	.LBB2_66
	.p2align	4, 0x90
.LBB2_42:                               #   in Loop: Header=BB2_2 Depth=1
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	228(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r15), %esi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	4(%rdx,%rax), %r15d
	movl	12(%rdx,%rax), %ecx
	movl	%ecx, %r9d
	subl	%esi, %r9d
	incl	%r9d
	movl	%esi, 88(%rsp)          # 4-byte Spill
	cmpl	%esi, %ecx
	movl	16(%rdx,%rax), %edx
	movl	$0, %eax
	cmovsl	%eax, %r9d
	movl	%edx, %r11d
	subl	%r15d, %r11d
	incl	%r11d
	cmpl	%r15d, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %esi
	movl	-20(%rcx,%r13), %r12d
	movl	-12(%rcx,%r13), %edx
	cmovsl	%eax, %r11d
	movl	%edx, %ebp
	subl	%esi, %ebp
	incl	%ebp
	movl	%esi, 16(%rsp)          # 4-byte Spill
	cmpl	%esi, %edx
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %ebp
	movl	%esi, %edx
	subl	%r12d, %edx
	incl	%edx
	cmpl	%r12d, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %r10d
	movl	-20(%rcx,%r13), %ebx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %edx
	movl	%esi, %edi
	subl	%r10d, %edi
	incl	%edi
	cmpl	%r10d, %esi
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %edi
	movl	%edi, 488(%rsp)         # 4-byte Spill
	movl	%esi, %r14d
	subl	%ebx, %r14d
	incl	%r14d
	movl	%ebx, 96(%rsp)          # 4-byte Spill
	cmpl	%ebx, %esi
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %r14d
	movl	%esi, %edi
	subl	%ebx, %edi
	incl	%edi
	cmpl	%ebx, %esi
	movl	-20(%rcx,%r13), %esi
	cmovsl	%eax, %edi
	movl	%edi, 480(%rsp)         # 4-byte Spill
	movl	-8(%rcx,%r13), %edi
	movl	%edi, %r8d
	subl	%esi, %r8d
	incl	%r8d
	cmpl	%esi, %edi
	cmovsl	%eax, %r8d
	movl	228(%rsp), %eax
	movl	232(%rsp), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %edi
	movl	%ecx, 496(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %edi
	movl	236(%rsp), %ecx
	cmpl	%edi, %ecx
	movl	%ecx, 792(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %edi
	testl	%edi, %edi
	movq	%rax, %rdi
	jle	.LBB2_66
# BB#43:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 792(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_66
# BB#44:                                # %.preheader2620.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%esi, 80(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	8(%rax,%rsi), %ecx
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,2), %rdi
	movl	%r12d, 128(%rsp)        # 4-byte Spill
	movl	%r10d, 64(%rsp)         # 4-byte Spill
	movl	%ecx, %r12d
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	8(%rax,%rdi,8), %r12d
	movl	%r15d, 32(%rsp)         # 4-byte Spill
	movl	404(%rsp), %r10d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r10d
	movl	%ecx, %r15d
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r15d
	movq	48(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %ecx
	movl	%ecx, 504(%rsp)         # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	4(%rax,%rsi), %ecx
	movq	664(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %edi
	imull	%ebp, %edi
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	%ebp, %ebx
	imull	%edx, %ebx
	imull	8(%rax), %ebx
	movl	496(%rsp), %r13d        # 4-byte Reload
	movl	%r13d, %esi
	imull	%edi, %esi
	subl	%esi, %ebx
	movl	%ebx, 928(%rsp)         # 4-byte Spill
	movslq	(%rax), %rax
	movq	%rax, 512(%rsp)         # 8-byte Spill
	movl	%eax, %esi
	imull	120(%rsp), %esi         # 4-byte Folded Reload
	subl	%esi, %edi
	movl	%edi, 728(%rsp)         # 4-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %esi
	movl	%esi, %eax
	subl	88(%rsp), %eax          # 4-byte Folded Reload
	movl	%ecx, %edi
	subl	32(%rsp), %edi          # 4-byte Folded Reload
	imull	%r11d, %r12d
	addl	%edi, %r12d
	imull	%r9d, %r12d
	addl	%eax, %r12d
	movl	396(%rsp), %eax
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	400(%rsp), %edi
	subl	128(%rsp), %edi         # 4-byte Folded Reload
	imull	%edx, %r10d
	addl	%edi, %r10d
	imull	%ebp, %r10d
	addl	%eax, %r10d
	movl	%esi, %eax
	subl	64(%rsp), %eax          # 4-byte Folded Reload
	movl	%ecx, %edx
	subl	96(%rsp), %edx          # 4-byte Folded Reload
	movl	504(%rsp), %ebp         # 4-byte Reload
	imull	%r14d, %r15d
	addl	%edx, %r15d
	movl	488(%rsp), %ebx         # 4-byte Reload
	imull	%ebx, %r15d
	addl	%eax, %r15d
	subl	24(%rsp), %esi          # 4-byte Folded Reload
	subl	80(%rsp), %ecx          # 4-byte Folded Reload
	imull	%r8d, %ebp
	addl	%ecx, %ebp
	movl	480(%rsp), %edi         # 4-byte Reload
	imull	%edi, %ebp
	addl	%esi, %ebp
	subl	%r13d, %r8d
	imull	%edi, %r8d
	movl	%r8d, 920(%rsp)         # 4-byte Spill
	subl	%r13d, %r14d
	imull	%ebx, %r14d
	movl	%r14d, 912(%rsp)        # 4-byte Spill
	subl	%r13d, %r11d
	imull	%r9d, %r11d
	movl	%r11d, 904(%rsp)        # 4-byte Spill
	movl	%ebx, %eax
	subl	120(%rsp), %eax         # 4-byte Folded Reload
	movl	%eax, 720(%rsp)         # 4-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	leal	-1(%rax), %eax
	incq	%rax
	movq	%rax, 656(%rsp)         # 8-byte Spill
	movq	512(%rsp), %r13         # 8-byte Reload
	imulq	%r13, %rax
	movq	%rax, 648(%rsp)         # 8-byte Spill
	movl	$0, 800(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 120(%rsp)           # 4-byte Folded Reload
	movl	$0, %r8d
	cmovnsl	120(%rsp), %r8d         # 4-byte Folded Reload
	addl	%r8d, %ebx
	addl	%r8d, %edi
	addl	%r9d, %r8d
	movslq	144(%rsp), %rax         # 4-byte Folded Reload
	leaq	(,%rax,8), %rcx
	movq	416(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 472(%rsp)         # 8-byte Spill
	movq	184(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 408(%rsp)         # 8-byte Spill
	movq	424(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 704(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 1128(%rsp)        # 8-byte Spill
	movq	456(%rsp), %r11         # 8-byte Reload
	movq	%r11, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 1120(%rsp)        # 8-byte Spill
	movslq	136(%rsp), %r9          # 4-byte Folded Reload
	movslq	152(%rsp), %rdx         # 4-byte Folded Reload
	subl	120(%rsp), %ebx         # 4-byte Folded Reload
	movl	%ebx, 488(%rsp)         # 4-byte Spill
	leaq	(,%r9,8), %rsi
	leaq	(,%rdx,8), %rbx
	subq	%rsi, %rbx
	leaq	(%rsi,%rdx,8), %rcx
	subl	120(%rsp), %edi         # 4-byte Folded Reload
	movl	%edi, 480(%rsp)         # 4-byte Spill
	movq	120(%rsp), %rdi         # 8-byte Reload
	movq	256(%rsp), %rsi         # 8-byte Reload
	addq	%rsi, %rcx
	movq	%rcx, 696(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 688(%rsp)         # 8-byte Spill
	addq	%rsi, %rbx
	movq	%rbx, 1112(%rsp)        # 8-byte Spill
	leaq	(%rsi,%r9,8), %rcx
	movq	%rcx, 680(%rsp)         # 8-byte Spill
	movq	%r10, %r14
	movq	216(%rsp), %rdx         # 8-byte Reload
	leaq	8(%rdx,%r9,8), %rcx
	movq	%rcx, 1104(%rsp)        # 8-byte Spill
	addq	$8, %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	subl	%edi, %r8d
	movl	%r8d, 712(%rsp)         # 4-byte Spill
	shlq	$3, %r13
	movq	176(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1096(%rsp)        # 8-byte Spill
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1088(%rsp)        # 8-byte Spill
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1040(%rsp)        # 8-byte Spill
	movq	416(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1032(%rsp)        # 8-byte Spill
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1024(%rsp)        # 8-byte Spill
	movq	248(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1016(%rsp)        # 8-byte Spill
	movq	424(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1008(%rsp)        # 8-byte Spill
	movq	208(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1000(%rsp)        # 8-byte Spill
	leaq	(%r11,%rax,8), %rcx
	movq	%rcx, 992(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 984(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 976(%rsp)         # 8-byte Spill
	movq	888(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 968(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 960(%rsp)         # 8-byte Spill
	movq	896(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 952(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 944(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 936(%rsp)         # 8-byte Spill
	movl	%r15d, %eax
	movq	%r13, 512(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_45:                               # %.preheader2620
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_48 Depth 3
                                        #         Child Loop BB2_50 Depth 4
	cmpl	$0, 496(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_46
# BB#47:                                # %.preheader2616.preheader
                                        #   in Loop: Header=BB2_45 Depth=2
	xorl	%ebx, %ebx
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB2_48:                               # %.preheader2616
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_45 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_50 Depth 4
	testl	%edi, %edi
	jle	.LBB2_52
# BB#49:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_48 Depth=3
	movl	%ebx, 736(%rsp)         # 4-byte Spill
	movl	%ecx, 744(%rsp)         # 4-byte Spill
	movl	%eax, 752(%rsp)         # 4-byte Spill
	cltq
	movq	880(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r10
	movq	872(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r8
	movl	%ebp, 504(%rsp)         # 4-byte Spill
	movslq	%ebp, %rsi
	shlq	$3, %rsi
	movl	%r12d, 768(%rsp)        # 4-byte Spill
	movslq	%r12d, %rdx
	shlq	$3, %rdx
	movslq	%r14d, %rdi
	movq	696(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	688(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	1112(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movq	680(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 776(%rsp)         # 8-byte Spill
	movq	1104(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rsi), %rbp
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	addq	216(%rsp), %rsi         # 8-byte Folded Reload
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	1136(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	816(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	1144(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	832(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	848(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	840(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	1152(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	824(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	1160(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	1168(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	856(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	1176(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	addq	864(%rsp), %rdx         # 8-byte Folded Reload
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	648(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi), %rcx
	movq	%rcx, 760(%rsp)         # 8-byte Spill
	movq	472(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	240(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	408(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	704(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	1128(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	1120(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 640(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 632(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 624(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 344(%rsp)         # 8-byte Spill
	movq	416(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 616(%rsp)         # 8-byte Spill
	movq	424(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 608(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 600(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 592(%rsp)         # 8-byte Spill
	movq	1032(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	movq	1024(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rcx
	movq	%rcx, 384(%rsp)         # 8-byte Spill
	movq	1016(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	1008(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	1000(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	992(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdx
	movq	%rdx, 320(%rsp)         # 8-byte Spill
	movq	1096(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdx
	movq	%rdx, 584(%rsp)         # 8-byte Spill
	movq	984(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdx
	movq	%rdx, 576(%rsp)         # 8-byte Spill
	movq	1088(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rdx
	movq	%rdx, 568(%rsp)         # 8-byte Spill
	movq	976(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rcx
	movq	%rcx, 312(%rsp)         # 8-byte Spill
	movq	968(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rcx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movq	1040(%rsp), %rdx        # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movq	960(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	movq	952(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rcx
	movq	%rcx, 544(%rsp)         # 8-byte Spill
	movq	944(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rcx
	movq	%rcx, 536(%rsp)         # 8-byte Spill
	movq	936(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,8), %rcx
	movq	%rcx, 528(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rdi         # 8-byte Reload
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_50:                               # %.lr.ph
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_45 Depth=2
                                        #       Parent Loop BB2_48 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	520(%rsp), %rsi         # 8-byte Reload
	mulsd	(%rsi,%r11), %xmm0
	movq	280(%rsp), %rcx         # 8-byte Reload
	mulsd	8(%rcx,%r14,8), %xmm0
	movq	40(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm0, (%rdx,%r14,8)
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	384(%rsp), %r15         # 8-byte Reload
	movsd	(%r15,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rcx,%r14,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	192(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	addsd	%xmm1, %xmm0
	movq	640(%rsp), %r12         # 8-byte Reload
	mulsd	(%r12,%r11), %xmm2
	addsd	%xmm0, %xmm2
	movq	32(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%r14,8)
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	288(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%r11), %xmm0
	mulsd	-8(%rcx,%r14,8), %xmm0
	movq	72(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm0, (%rdx,%r14,8)
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	272(%rsp), %r9          # 8-byte Reload
	movsd	(%r9,%r11), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	264(%rsp), %r13         # 8-byte Reload
	movsd	8(%r13,%r14,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	320(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	addsd	%xmm1, %xmm0
	movq	560(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r11), %xmm2
	addsd	%xmm0, %xmm2
	movq	56(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm2, (%rdx,%r14,8)
	movsd	(%r13,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	632(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r10,%r14,8), %xmm2    # xmm2 = mem[0],zero
	movq	584(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%r11), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	576(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm2
	addsd	%xmm3, %xmm2
	movq	104(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%r14,8)
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	568(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r13,%r14,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	312(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	addsd	%xmm1, %xmm0
	movq	624(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm2
	addsd	%xmm0, %xmm2
	movq	176(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%r14,8)
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	304(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	movq	376(%rsp), %rbx         # 8-byte Reload
	mulsd	8(%rbx,%r14,8), %xmm0
	movq	168(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm0, (%rdx,%r14,8)
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	296(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rbx,%r14,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	552(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	addsd	%xmm1, %xmm0
	movq	344(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm2
	addsd	%xmm0, %xmm2
	movq	160(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm2, (%rdx,%r14,8)
	movsd	(%r10,%r14,8), %xmm0    # xmm0 = mem[0],zero
	movq	544(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	mulsd	-8(%rbx,%r14,8), %xmm0
	movq	152(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm0, (%rdx,%r14,8)
	movsd	(%r8,%r14,8), %xmm0     # xmm0 = mem[0],zero
	movq	88(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	movq	776(%rsp), %rbx         # 8-byte Reload
	mulsd	8(%rbx,%r14,8), %xmm0
	movq	336(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%r11), %xmm0
	movsd	(%r10,%r14,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rsi,%r11), %xmm1
	mulsd	(%rbp,%r14,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	144(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm1, (%rdx,%r14,8)
	movsd	(%r8,%r14,8), %xmm0     # xmm0 = mem[0],zero
	movq	80(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rbx,%r14,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	328(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%r11), %xmm1
	movsd	(%r10,%r14,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%r15,%r11), %xmm4      # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	-8(%rbp,%r14,8), %xmm5  # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	616(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm2
	addsd	%xmm4, %xmm2
	mulsd	(%r12,%r11), %xmm5
	addsd	%xmm2, %xmm5
	movq	64(%rsp), %rdx          # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	addsd	%xmm5, %xmm0
	movq	536(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm3
	addsd	%xmm0, %xmm3
	movq	136(%rsp), %rdx         # 8-byte Reload
	movsd	%xmm3, (%rdx,%r14,8)
	movsd	(%r8,%r14,8), %xmm0     # xmm0 = mem[0],zero
	movq	128(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	mulsd	-8(%rbx,%r14,8), %xmm0
	movq	608(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%r11), %xmm0
	movsd	(%r10,%r14,8), %xmm1    # xmm1 = mem[0],zero
	mulsd	(%rax,%r11), %xmm1
	mulsd	-16(%rbp,%r14,8), %xmm1
	addsd	%xmm0, %xmm1
	movq	96(%rsp), %rdx          # 8-byte Reload
	movsd	%xmm1, (%rdx,%r14,8)
	movsd	(%r8,%r14,8), %xmm0     # xmm0 = mem[0],zero
	movq	200(%rsp), %rdx         # 8-byte Reload
	movsd	(%rdx,%r11), %xmm1      # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movsd	8(%rdx,%r14,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	600(%rsp), %rdx         # 8-byte Reload
	addsd	(%rdx,%r11), %xmm1
	movsd	(%r10,%r14,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%r9,%r11), %xmm4       # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	24(%rsp), %rdx          # 8-byte Reload
	movsd	(%rdx,%r14,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	592(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm2
	addsd	%xmm4, %xmm2
	mulsd	(%rcx,%r11), %xmm5
	addsd	%xmm2, %xmm5
	movq	352(%rsp), %rdx         # 8-byte Reload
	mulsd	(%rdx,%r11), %xmm0
	addsd	%xmm5, %xmm0
	movq	528(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r11), %xmm3
	addsd	%xmm0, %xmm3
	movq	%rdi, %rdx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movsd	%xmm3, (%rdi,%r14,8)
	movq	%rdx, %rdi
	incq	%r14
	addq	512(%rsp), %r11         # 8-byte Folded Reload
	cmpl	%r14d, %edi
	jne	.LBB2_50
# BB#51:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_48 Depth=3
	movl	744(%rsp), %ecx         # 4-byte Reload
	addl	656(%rsp), %ecx         # 4-byte Folded Reload
	movl	504(%rsp), %ebp         # 4-byte Reload
	movl	768(%rsp), %r12d        # 4-byte Reload
	movq	760(%rsp), %r14         # 8-byte Reload
	movl	752(%rsp), %eax         # 4-byte Reload
	movl	736(%rsp), %ebx         # 4-byte Reload
.LBB2_52:                               # %._crit_edge
                                        #   in Loop: Header=BB2_48 Depth=3
	addl	480(%rsp), %ebp         # 4-byte Folded Reload
	addl	720(%rsp), %ecx         # 4-byte Folded Reload
	addl	728(%rsp), %r14d        # 4-byte Folded Reload
	addl	712(%rsp), %r12d        # 4-byte Folded Reload
	incl	%ebx
	addl	488(%rsp), %eax         # 4-byte Folded Reload
	cmpl	496(%rsp), %ebx         # 4-byte Folded Reload
	jne	.LBB2_48
	jmp	.LBB2_53
	.p2align	4, 0x90
.LBB2_46:                               #   in Loop: Header=BB2_45 Depth=2
	movl	%eax, %ecx
.LBB2_53:                               # %._crit_edge2639
                                        #   in Loop: Header=BB2_45 Depth=2
	addl	920(%rsp), %ebp         # 4-byte Folded Reload
	addl	912(%rsp), %ecx         # 4-byte Folded Reload
	addl	928(%rsp), %r14d        # 4-byte Folded Reload
	addl	904(%rsp), %r12d        # 4-byte Folded Reload
	movl	800(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 800(%rsp)         # 4-byte Spill
	cmpl	792(%rsp), %eax         # 4-byte Folded Reload
	movl	%ecx, %eax
	jne	.LBB2_45
	jmp	.LBB2_66
	.p2align	4, 0x90
.LBB2_54:                               #   in Loop: Header=BB2_2 Depth=1
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	228(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r15), %r14d
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	4(%rdx,%rax), %r10d
	movl	12(%rdx,%rax), %ecx
	movl	%ecx, %r12d
	subl	%r14d, %r12d
	incl	%r12d
	cmpl	%r14d, %ecx
	movl	16(%rdx,%rax), %edx
	movl	$0, %eax
	cmovsl	%eax, %r12d
	movl	%edx, %r11d
	subl	%r10d, %r11d
	incl	%r11d
	cmpl	%r10d, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %r9d
	movl	-20(%rcx,%r13), %edi
	movl	-12(%rcx,%r13), %edx
	cmovsl	%eax, %r11d
	movl	%edx, %r15d
	subl	%r9d, %r15d
	incl	%r15d
	cmpl	%r9d, %edx
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %r15d
	movl	%esi, %edx
	subl	%edi, %edx
	incl	%edx
	movl	%edi, 80(%rsp)          # 4-byte Spill
	cmpl	%edi, %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebp
	movl	-20(%rcx,%r13), %ebx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %edx
	movl	%esi, %edi
	subl	%ebp, %edi
	incl	%edi
	movl	%ebp, 96(%rsp)          # 4-byte Spill
	cmpl	%ebp, %esi
	movl	-8(%rcx,%r13), %esi
	cmovsl	%eax, %edi
	movl	%edi, 688(%rsp)         # 4-byte Spill
	movl	%esi, %ebp
	subl	%ebx, %ebp
	incl	%ebp
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	cmpl	%ebx, %esi
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	-24(%rcx,%r13), %ebx
	movl	-12(%rcx,%r13), %esi
	cmovsl	%eax, %ebp
	movl	%esi, %edi
	subl	%ebx, %edi
	incl	%edi
	cmpl	%ebx, %esi
	movl	-20(%rcx,%r13), %esi
	cmovsl	%eax, %edi
	movl	%edi, 680(%rsp)         # 4-byte Spill
	movl	-8(%rcx,%r13), %edi
	movl	%edi, %r8d
	subl	%esi, %r8d
	incl	%r8d
	cmpl	%esi, %edi
	cmovsl	%eax, %r8d
	movl	228(%rsp), %eax
	movl	232(%rsp), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %edi
	movl	%ecx, 696(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %edi
	movl	236(%rsp), %ecx
	cmpl	%edi, %ecx
	movl	%ecx, 784(%rsp)         # 4-byte Spill
	cmovgel	%ecx, %edi
	testl	%edi, %edi
	jle	.LBB2_66
# BB#55:                                #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 784(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_66
# BB#56:                                # %.preheader2617.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%esi, 64(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r14d, 128(%rsp)        # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	8(%rax,%rsi), %ecx
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rax,2), %rdi
	movl	%r10d, 352(%rsp)        # 4-byte Spill
	movl	%r9d, 200(%rsp)         # 4-byte Spill
	movl	%ecx, %r10d
	movq	32(%rsp), %rax          # 8-byte Reload
	subl	8(%rax,%rdi,8), %r10d
	movl	404(%rsp), %r14d
	movq	40(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r14d
	movl	%ecx, %r9d
	movq	56(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %r9d
	movq	48(%rsp), %rax          # 8-byte Reload
	subl	-16(%rax,%r13), %ecx
	movl	%ecx, 240(%rsp)         # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	4(%rax,%rsi), %ecx
	movq	664(%rsp), %rax         # 8-byte Reload
	movl	4(%rax), %edi
	imull	%r15d, %edi
	movl	%ebx, 24(%rsp)          # 4-byte Spill
	movl	%r15d, %ebx
	imull	%edx, %ebx
	imull	8(%rax), %ebx
	movl	696(%rsp), %r13d        # 4-byte Reload
	movl	%r13d, %esi
	imull	%edi, %esi
	subl	%esi, %ebx
	movl	%ebx, 1196(%rsp)        # 4-byte Spill
	movslq	(%rax), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movl	%eax, %esi
	imull	16(%rsp), %esi          # 4-byte Folded Reload
	subl	%esi, %edi
	movl	%edi, 1104(%rsp)        # 4-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %esi
	movl	%esi, %eax
	subl	128(%rsp), %eax         # 4-byte Folded Reload
	movl	%ecx, %edi
	subl	352(%rsp), %edi         # 4-byte Folded Reload
	imull	%r11d, %r10d
	addl	%edi, %r10d
	imull	%r12d, %r10d
	addl	%eax, %r10d
	movl	396(%rsp), %eax
	subl	200(%rsp), %eax         # 4-byte Folded Reload
	movl	400(%rsp), %edi
	subl	80(%rsp), %edi          # 4-byte Folded Reload
	imull	%edx, %r14d
	addl	%edi, %r14d
	imull	%r15d, %r14d
	addl	%eax, %r14d
	movl	%esi, %eax
	subl	96(%rsp), %eax          # 4-byte Folded Reload
	movl	%ecx, %edx
	subl	88(%rsp), %edx          # 4-byte Folded Reload
	imull	%ebp, %r9d
	addl	%edx, %r9d
	movl	688(%rsp), %ebx         # 4-byte Reload
	imull	%ebx, %r9d
	addl	%eax, %r9d
	movl	%r9d, 408(%rsp)         # 4-byte Spill
	subl	24(%rsp), %esi          # 4-byte Folded Reload
	subl	64(%rsp), %ecx          # 4-byte Folded Reload
	movl	240(%rsp), %eax         # 4-byte Reload
	imull	%r8d, %eax
	addl	%ecx, %eax
	movl	680(%rsp), %edi         # 4-byte Reload
	imull	%edi, %eax
	addl	%esi, %eax
	movl	%eax, 240(%rsp)         # 4-byte Spill
	subl	%r13d, %r8d
	imull	%edi, %r8d
	movl	%r8d, 1192(%rsp)        # 4-byte Spill
	subl	%r13d, %ebp
	imull	%ebx, %ebp
	movl	%ebp, 1188(%rsp)        # 4-byte Spill
	subl	%r13d, %r11d
	imull	%r12d, %r11d
	movl	%r11d, 1184(%rsp)       # 4-byte Spill
	movl	%ebx, %eax
	subl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 1096(%rsp)        # 4-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	incq	%rax
	movq	%rax, 1040(%rsp)        # 8-byte Spill
	movq	472(%rsp), %r13         # 8-byte Reload
	imulq	%r13, %rax
	movq	%rax, 1032(%rsp)        # 8-byte Spill
	movl	$0, 788(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movl	$0, %r8d
	cmovnsl	16(%rsp), %r8d          # 4-byte Folded Reload
	addl	%r8d, %ebx
	addl	%r8d, %edi
	addl	%r12d, %r8d
	movslq	144(%rsp), %rax         # 4-byte Folded Reload
	leaq	(,%rax,8), %rcx
	movq	416(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 1024(%rsp)        # 8-byte Spill
	movq	808(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 1016(%rsp)        # 8-byte Spill
	movq	184(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 1008(%rsp)        # 8-byte Spill
	movq	248(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 1000(%rsp)        # 8-byte Spill
	movq	424(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 992(%rsp)         # 8-byte Spill
	movq	1064(%rsp), %rbp        # 8-byte Reload
	movq	%rbp, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 984(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rdx         # 8-byte Reload
	subq	%rcx, %rdx
	movq	%rdx, 976(%rsp)         # 8-byte Spill
	movq	456(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rdx
	subq	%rcx, %rdx
	movq	%rdx, 968(%rsp)         # 8-byte Spill
	movslq	136(%rsp), %r9          # 4-byte Folded Reload
	movslq	152(%rsp), %rdx         # 4-byte Folded Reload
	subl	16(%rsp), %ebx          # 4-byte Folded Reload
	movl	%ebx, 688(%rsp)         # 4-byte Spill
	leaq	(,%r9,8), %rsi
	leaq	(,%rdx,8), %rbx
	subq	%rsi, %rbx
	leaq	(%rsi,%rdx,8), %rcx
	subl	16(%rsp), %edi          # 4-byte Folded Reload
	movl	%edi, 680(%rsp)         # 4-byte Spill
	movl	%r10d, %r15d
	movq	256(%rsp), %rsi         # 8-byte Reload
	addq	%rsi, %rcx
	movq	%rcx, 952(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 944(%rsp)         # 8-byte Spill
	addq	%rsi, %rbx
	movq	%rbx, 960(%rsp)         # 8-byte Spill
	leaq	(%rsi,%r9,8), %rcx
	movq	%rcx, 936(%rsp)         # 8-byte Spill
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	216(%rsp), %rdx         # 8-byte Reload
	leaq	8(%rdx,%r9,8), %rcx
	movq	%rcx, 800(%rsp)         # 8-byte Spill
	addq	$8, %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	movl	240(%rsp), %edx         # 4-byte Reload
	subl	%r11d, %r8d
	movl	%r8d, 1088(%rsp)        # 4-byte Spill
	shlq	$3, %r13
	movq	176(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 792(%rsp)         # 8-byte Spill
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 928(%rsp)         # 8-byte Spill
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 920(%rsp)         # 8-byte Spill
	movq	416(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 912(%rsp)         # 8-byte Spill
	movq	808(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 904(%rsp)         # 8-byte Spill
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1392(%rsp)        # 8-byte Spill
	movq	248(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1384(%rsp)        # 8-byte Spill
	movq	424(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1376(%rsp)        # 8-byte Spill
	leaq	(%rbp,%rax,8), %rcx
	movq	%rcx, 1368(%rsp)        # 8-byte Spill
	movq	208(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1360(%rsp)        # 8-byte Spill
	leaq	(%r12,%rax,8), %rcx
	movq	%rcx, 1352(%rsp)        # 8-byte Spill
	movq	368(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1344(%rsp)        # 8-byte Spill
	movq	464(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1336(%rsp)        # 8-byte Spill
	movq	888(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1328(%rsp)        # 8-byte Spill
	movq	1072(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1320(%rsp)        # 8-byte Spill
	movq	448(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1312(%rsp)        # 8-byte Spill
	movq	896(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1304(%rsp)        # 8-byte Spill
	movq	1080(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1296(%rsp)        # 8-byte Spill
	movq	1048(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1288(%rsp)        # 8-byte Spill
	movq	432(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1280(%rsp)        # 8-byte Spill
	movq	1056(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rax,8), %rcx
	movq	%rcx, 1272(%rsp)        # 8-byte Spill
	movq	440(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 1264(%rsp)        # 8-byte Spill
	movl	408(%rsp), %eax         # 4-byte Reload
	movq	%r13, 472(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_57:                               # %.preheader2617
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_60 Depth 3
                                        #         Child Loop BB2_62 Depth 4
	cmpl	$0, 696(%rsp)           # 4-byte Folded Reload
	jle	.LBB2_58
# BB#59:                                # %.preheader.preheader
                                        #   in Loop: Header=BB2_57 Depth=2
	xorl	%ebp, %ebp
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB2_60:                               # %.preheader
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_57 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_62 Depth 4
	testl	%r11d, %r11d
	jle	.LBB2_64
# BB#61:                                # %.lr.ph2718.preheader
                                        #   in Loop: Header=BB2_60 Depth=3
	movl	%ebp, 1112(%rsp)        # 4-byte Spill
	movl	%ecx, 1120(%rsp)        # 4-byte Spill
	movl	%eax, 408(%rsp)         # 4-byte Spill
	cltq
	movq	880(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r12
	movq	872(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %r10
	movl	%edx, 240(%rsp)         # 4-byte Spill
	movslq	%edx, %rsi
	shlq	$3, %rsi
	movl	%r15d, 704(%rsp)        # 4-byte Spill
	movslq	%r15d, %rbx
	shlq	$3, %rbx
	movslq	%r14d, %rdx
	movq	952(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	movq	944(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 720(%rsp)         # 8-byte Spill
	movq	960(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 712(%rsp)         # 8-byte Spill
	movq	936(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	800(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 648(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	addq	216(%rsp), %rsi         # 8-byte Folded Reload
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	1136(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	816(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	1144(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	832(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	848(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	movq	840(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	1152(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	824(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	1160(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	1168(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	856(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	1176(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rbx), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	addq	864(%rsp), %rbx         # 8-byte Folded Reload
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	1032(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	movq	%rcx, 1128(%rsp)        # 8-byte Spill
	movq	1024(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	1016(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	1008(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movq	1000(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	movq	992(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	movq	984(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 640(%rsp)         # 8-byte Spill
	movq	976(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 632(%rsp)         # 8-byte Spill
	movq	968(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 624(%rsp)         # 8-byte Spill
	movq	808(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 344(%rsp)         # 8-byte Spill
	movq	248(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movq	1064(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 536(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 280(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 616(%rsp)         # 8-byte Spill
	movq	1072(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 608(%rsp)         # 8-byte Spill
	movq	448(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 600(%rsp)         # 8-byte Spill
	movq	1080(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 592(%rsp)         # 8-byte Spill
	movq	416(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	movq	1048(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	movq	184(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 584(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 576(%rsp)         # 8-byte Spill
	movq	424(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 568(%rsp)         # 8-byte Spill
	movq	1056(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	movq	208(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 312(%rsp)         # 8-byte Spill
	movq	440(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rcx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	movq	912(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 496(%rsp)         # 8-byte Spill
	movq	904(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 296(%rsp)         # 8-byte Spill
	movq	1392(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movq	1384(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	movq	1376(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rax
	movq	%rax, 728(%rsp)         # 8-byte Spill
	movq	1368(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 544(%rsp)         # 8-byte Spill
	movq	1360(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rax
	movq	%rax, 656(%rsp)         # 8-byte Spill
	movq	1352(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 528(%rsp)         # 8-byte Spill
	movq	792(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 520(%rsp)         # 8-byte Spill
	movq	1344(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 384(%rsp)         # 8-byte Spill
	movq	928(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movq	1336(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movq	1328(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	movq	1320(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	movq	920(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 776(%rsp)         # 8-byte Spill
	movq	1312(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 512(%rsp)         # 8-byte Spill
	movq	1304(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 504(%rsp)         # 8-byte Spill
	movq	1296(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 768(%rsp)         # 8-byte Spill
	movq	1288(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 760(%rsp)         # 8-byte Spill
	movq	1280(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 752(%rsp)         # 8-byte Spill
	movq	1272(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 744(%rsp)         # 8-byte Spill
	movq	1264(%rsp), %rcx        # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rcx
	movq	%rcx, 736(%rsp)         # 8-byte Spill
	xorl	%r8d, %r8d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_62:                               # %.lr.ph2718
                                        #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_57 Depth=2
                                        #       Parent Loop BB2_60 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	496(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	480(%rsp), %rbx         # 8-byte Reload
	movsd	8(%rbx,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	movq	296(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	movq	344(%rsp), %r14         # 8-byte Reload
	mulsd	(%r14,%r8), %xmm2
	addsd	%xmm0, %xmm2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	488(%rsp), %rax         # 8-byte Reload
	movsd	(%rax,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rbx,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	552(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	movq	336(%rsp), %r13         # 8-byte Reload
	mulsd	(%r13,%r8), %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
	movq	72(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	728(%rsp), %r9          # 8-byte Reload
	movsd	(%r9,%r8), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%rbx,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	movq	544(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	mulsd	%xmm2, %xmm1
	addsd	%xmm1, %xmm0
	movq	536(%rsp), %rbx         # 8-byte Reload
	mulsd	(%rbx,%r8), %xmm2
	addsd	%xmm0, %xmm2
	movq	56(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	656(%rsp), %rdi         # 8-byte Reload
	movsd	(%rdi,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	720(%rsp), %r15         # 8-byte Reload
	movsd	8(%r15,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	528(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	movq	280(%rsp), %r11         # 8-byte Reload
	mulsd	(%r11,%r8), %xmm2
	addsd	%xmm1, %xmm0
	addsd	%xmm0, %xmm2
	movq	104(%rsp), %rcx         # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r15,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	328(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%r12,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	movq	520(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm3       # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	addsd	%xmm1, %xmm3
	movq	384(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm3, %xmm2
	movq	176(%rsp), %rcx         # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	288(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%r15,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	272(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	addsd	%xmm1, %xmm0
	movq	616(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm0, %xmm2
	movq	168(%rsp), %rcx         # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	264(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	712(%rsp), %rbp         # 8-byte Reload
	movsd	8(%rbp,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	376(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	addsd	%xmm1, %xmm0
	movq	608(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm0, %xmm2
	movq	160(%rsp), %rcx         # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	776(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rbp,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	512(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	addsd	%xmm1, %xmm0
	movq	600(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm0, %xmm2
	movq	152(%rsp), %rcx         # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r12,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	504(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%rbp,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movq	768(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	addsd	%xmm1, %xmm0
	movq	592(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm0, %xmm2
	movq	144(%rsp), %rcx         # 8-byte Reload
	movsd	%xmm2, (%rcx,%rdx,8)
	movsd	(%r10,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	80(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movq	120(%rsp), %rbp         # 8-byte Reload
	movsd	8(%rbp,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	(%r12,%rdx,8), %xmm3    # xmm3 = mem[0],zero
	movq	192(%rsp), %rcx         # 8-byte Reload
	addsd	(%rcx,%r8), %xmm1
	movsd	(%rsi,%r8), %xmm4       # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	648(%rsp), %rsi         # 8-byte Reload
	movsd	(%rsi,%rdx,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	320(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	mulsd	(%r14,%r8), %xmm5
	addsd	%xmm4, %xmm2
	addsd	%xmm2, %xmm5
	movq	64(%rsp), %rcx          # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	movq	760(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm3
	addsd	%xmm5, %xmm0
	addsd	%xmm0, %xmm3
	movq	136(%rsp), %rcx         # 8-byte Reload
	movsd	%xmm3, (%rcx,%rdx,8)
	movsd	(%r10,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	128(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	(%rbp,%rdx,8), %xmm2    # xmm2 = mem[0],zero
	movsd	(%r12,%rdx,8), %xmm3    # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	(%rax,%r8), %xmm4       # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	-8(%rsi,%rdx,8), %xmm5  # xmm5 = mem[0],zero
	movq	584(%rsp), %rcx         # 8-byte Reload
	addsd	(%rcx,%r8), %xmm1
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	576(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm4, %xmm2
	mulsd	(%r13,%r8), %xmm5
	addsd	%xmm2, %xmm5
	movq	200(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	addsd	%xmm5, %xmm0
	movq	752(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm3
	addsd	%xmm0, %xmm3
	movq	96(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm3, (%rcx,%rdx,8)
	movsd	(%r10,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	352(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	-8(%rbp,%rdx,8), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	(%r12,%rdx,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%r9,%r8), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movq	568(%rsp), %rcx         # 8-byte Reload
	addsd	(%rcx,%r8), %xmm1
	movsd	-16(%rsi,%rdx,8), %xmm5 # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	movq	560(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm1, %xmm4
	addsd	%xmm4, %xmm2
	mulsd	(%rbx,%r8), %xmm5
	movq	640(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	addsd	%xmm2, %xmm5
	addsd	%xmm5, %xmm0
	movq	744(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm3
	addsd	%xmm0, %xmm3
	movq	88(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm3, (%rcx,%rdx,8)
	movsd	(%r10,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movq	632(%rsp), %rcx         # 8-byte Reload
	movsd	(%rcx,%r8), %xmm1       # xmm1 = mem[0],zero
	movq	40(%rsp), %rcx          # 8-byte Reload
	movsd	8(%rcx,%rdx,8), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	movsd	(%r12,%rdx,8), %xmm3    # xmm3 = mem[0],zero
	movsd	(%rdi,%r8), %xmm4       # xmm4 = mem[0],zero
	movq	312(%rsp), %rcx         # 8-byte Reload
	addsd	(%rcx,%r8), %xmm1
	mulsd	%xmm3, %xmm4
	movq	48(%rsp), %rcx          # 8-byte Reload
	movsd	(%rcx,%rdx,8), %xmm5    # xmm5 = mem[0],zero
	mulsd	%xmm5, %xmm4
	addsd	%xmm1, %xmm4
	movq	304(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm2
	addsd	%xmm4, %xmm2
	mulsd	(%r11,%r8), %xmm5
	addsd	%xmm2, %xmm5
	movq	624(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm0
	addsd	%xmm5, %xmm0
	movq	736(%rsp), %rcx         # 8-byte Reload
	mulsd	(%rcx,%r8), %xmm3
	addsd	%xmm0, %xmm3
	movq	24(%rsp), %rcx          # 8-byte Reload
	movsd	%xmm3, (%rcx,%rdx,8)
	incq	%rdx
	addq	472(%rsp), %r8          # 8-byte Folded Reload
	cmpl	%edx, 16(%rsp)          # 4-byte Folded Reload
	jne	.LBB2_62
# BB#63:                                # %._crit_edge2719.loopexit
                                        #   in Loop: Header=BB2_60 Depth=3
	movl	1120(%rsp), %ecx        # 4-byte Reload
	addl	1040(%rsp), %ecx        # 4-byte Folded Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	240(%rsp), %edx         # 4-byte Reload
	movl	704(%rsp), %r15d        # 4-byte Reload
	movq	1128(%rsp), %r14        # 8-byte Reload
	movl	408(%rsp), %eax         # 4-byte Reload
	movl	1112(%rsp), %ebp        # 4-byte Reload
.LBB2_64:                               # %._crit_edge2719
                                        #   in Loop: Header=BB2_60 Depth=3
	addl	680(%rsp), %edx         # 4-byte Folded Reload
	addl	1096(%rsp), %ecx        # 4-byte Folded Reload
	addl	1104(%rsp), %r14d       # 4-byte Folded Reload
	addl	1088(%rsp), %r15d       # 4-byte Folded Reload
	incl	%ebp
	addl	688(%rsp), %eax         # 4-byte Folded Reload
	cmpl	696(%rsp), %ebp         # 4-byte Folded Reload
	jne	.LBB2_60
	jmp	.LBB2_65
	.p2align	4, 0x90
.LBB2_58:                               #   in Loop: Header=BB2_57 Depth=2
	movl	%eax, %ecx
.LBB2_65:                               # %._crit_edge2729
                                        #   in Loop: Header=BB2_57 Depth=2
	addl	1192(%rsp), %edx        # 4-byte Folded Reload
	addl	1188(%rsp), %ecx        # 4-byte Folded Reload
	addl	1196(%rsp), %r14d       # 4-byte Folded Reload
	addl	1184(%rsp), %r15d       # 4-byte Folded Reload
	movl	788(%rsp), %eax         # 4-byte Reload
	incl	%eax
	movl	%eax, 788(%rsp)         # 4-byte Spill
	cmpl	784(%rsp), %eax         # 4-byte Folded Reload
	movl	%ecx, %eax
	jne	.LBB2_57
	.p2align	4, 0x90
.LBB2_66:                               # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	112(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	movq	1216(%rsp), %rsi        # 8-byte Reload
	movslq	8(%rsi), %rax
	cmpq	%rax, %rdi
	movq	672(%rsp), %r12         # 8-byte Reload
	movq	1224(%rsp), %r15        # 8-byte Reload
	movq	1208(%rsp), %rdx        # 8-byte Reload
	movq	1256(%rsp), %rcx        # 8-byte Reload
	jl	.LBB2_2
.LBB2_67:                               # %._crit_edge2775
	xorl	%eax, %eax
	addq	$1400, %rsp             # imm = 0x578
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_SMG3BuildRAPNoSym, .Lfunc_end2-hypre_SMG3BuildRAPNoSym
	.cfi_endproc

	.globl	hypre_SMG3RAPPeriodicSym
	.p2align	4, 0x90
	.type	hypre_SMG3RAPPeriodicSym,@function
hypre_SMG3RAPPeriodicSym:               # @hypre_SMG3RAPPeriodicSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi41:
	.cfi_def_cfa_offset 368
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rdi, 232(%rsp)         # 8-byte Spill
	movq	8(%rdi), %rax
	cmpl	$1, 64(%rax)
	jne	.LBB3_60
# BB#1:
	movq	232(%rsp), %rdi         # 8-byte Reload
	movq	24(%rdi), %rcx
	movl	8(%rcx), %ecx
	movl	%ecx, 208(%rsp)         # 4-byte Spill
	movq	8(%rax), %rbp
	callq	hypre_StructMatrixAssemble
	cmpl	$0, 8(%rbp)
	jle	.LBB3_60
# BB#2:                                 # %.lr.ph1040
	movl	$0, %ebx
                                        # implicit-def: %RAX
	movq	%rax, 184(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 176(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 160(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 120(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 288(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	%rbp, 296(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_9 Depth 2
                                        #       Child Loop BB3_11 Depth 3
                                        #         Child Loop BB3_12 Depth 4
                                        #     Child Loop BB3_20 Depth 2
                                        #       Child Loop BB3_22 Depth 3
                                        #         Child Loop BB3_27 Depth 4
                                        #     Child Loop BB3_36 Depth 2
                                        #       Child Loop BB3_38 Depth 3
                                        #         Child Loop BB3_39 Depth 4
                                        #     Child Loop BB3_48 Depth 2
                                        #       Child Loop BB3_49 Depth 3
                                        #         Child Loop BB3_68 Depth 4
                                        #         Child Loop BB3_52 Depth 4
                                        #         Child Loop BB3_56 Depth 4
	movq	(%rbp), %r8
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %rdi
	movq	232(%rsp), %r13         # 8-byte Reload
	movq	40(%r13), %rax
	movq	(%rax), %rsi
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	movq	%rbp, %r14
	movq	%r14, 4(%rsp)
	movl	$0, 12(%rsp)
	movl	12(%rsi,%rdi), %eax
	movl	(%rsi,%rdi), %ecx
	movl	%eax, %edx
	subl	%ecx, %edx
	incl	%edx
	cmpl	%ecx, %eax
	movl	$0, %eax
	cmovsl	%eax, %edx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	leaq	(%r8,%rdi), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	16(%rsi,%rdi), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rdi), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	leaq	4(%rsp), %rbp
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$4294967295, %r15d      # imm = 0xFFFFFFFF
	movq	%r15, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	$1, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movabsq	$-4294967296, %r12      # imm = 0xFFFFFFFF00000000
	movq	%r12, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r14, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	$0, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r15, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%r12, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r13, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 192(%rsp)         # 8-byte Spill
	cmpl	$27, 208(%rsp)          # 4-byte Folded Reload
	jne	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	$-1, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	232(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	movl	%ebx, %esi
	leaq	4(%rsp), %rbp
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movabsq	$-4294967295, %r14      # imm = 0xFFFFFFFF00000001
	movq	%r14, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movabsq	$8589934591, %rax       # imm = 0x1FFFFFFFF
	movq	%rax, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 4(%rsp)
	movl	$-1, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	$-1, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	%r14, 4(%rsp)
	movl	$0, 12(%rsp)
	movq	%r15, %rdi
	movl	%ebx, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 280(%rsp)         # 8-byte Spill
.LBB3_5:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%rbx, 304(%rsp)         # 8-byte Spill
	movq	128(%rsp), %rdi         # 8-byte Reload
	leaq	212(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	272(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %esi
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	-4(%rcx), %eax
	movl	%eax, %edx
	subl	%esi, %edx
	incl	%edx
	cmpl	%esi, %eax
	movl	-12(%rcx), %edi
	movl	(%rcx), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edx
	movl	%edx, 32(%rsp)          # 4-byte Spill
	movl	%eax, %r11d
	subl	%edi, %r11d
	incl	%r11d
	movl	%edi, 204(%rsp)         # 4-byte Spill
	cmpl	%edi, %eax
	cmovsl	%ecx, %r11d
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rax,%rcx), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	8(%rax,%rcx), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movl	212(%rsp), %r14d
	movl	216(%rsp), %eax
	movl	220(%rsp), %ecx
	cmpl	%r14d, %eax
	movl	%r14d, %edx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmovgel	%eax, %edx
	cmpl	%edx, %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	cmovgel	%ecx, %edx
	testl	%edx, %edx
	jle	.LBB3_31
# BB#6:                                 # %.lr.ph909
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%r11d, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	subl	%eax, %ecx
	imull	32(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	leal	-1(%rax), %eax
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_17
# BB#7:                                 # %.lr.ph909
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_17
# BB#8:                                 # %.preheader884.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%edx, 260(%rsp)         # 4-byte Spill
	movl	32(%rsp), %edi          # 4-byte Reload
	movl	%edi, %ecx
	subl	%r14d, %ecx
	movl	%eax, 264(%rsp)         # 4-byte Spill
	imull	%eax, %ecx
	addl	%edi, %ecx
	subl	%r14d, %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movslq	224(%rsp), %rax         # 4-byte Folded Reload
	movl	%edi, %ecx
	imull	16(%rsp), %ecx          # 4-byte Folded Reload
	movl	%ecx, 108(%rsp)         # 4-byte Spill
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %ecx
	movl	%esi, 268(%rsp)         # 4-byte Spill
	subl	%esi, %ecx
	movq	240(%rsp), %rsi         # 8-byte Reload
	movl	(%rsi), %ebx
	subl	204(%rsp), %ebx         # 4-byte Folded Reload
	movl	4(%rsi), %edx
	movq	248(%rsp), %rsi         # 8-byte Reload
	subl	(%rsi), %edx
	imull	%r11d, %edx
	addl	%ebx, %edx
	imull	%edi, %edx
	addl	%ecx, %edx
	shlq	$3, %rax
	movq	168(%rsp), %rcx         # 8-byte Reload
	subq	%rax, %rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	-8(%rax), %rdi
	xorl	%esi, %esi
	movl	%r11d, 152(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB3_9:                                # %.preheader884.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_11 Depth 3
                                        #         Child Loop BB3_12 Depth 4
	testl	%r14d, %r14d
	movl	64(%rsp), %eax          # 4-byte Reload
	jle	.LBB3_15
# BB#10:                                # %.preheader880.us.us.preheader
                                        #   in Loop: Header=BB3_9 Depth=2
	movl	%esi, 136(%rsp)         # 4-byte Spill
	xorl	%ebx, %ebx
	movl	%edx, 144(%rsp)         # 4-byte Spill
	movl	%edx, %r11d
	.p2align	4, 0x90
.LBB3_11:                               # %.preheader880.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_12 Depth 4
	movslq	%r11d, %rax
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %r10
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %r9
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %r12
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %r8
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %r13
	movq	192(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %rbp
	leaq	(%rcx,%rax,8), %r15
	leaq	(%rdi,%rax,8), %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_9 Depth=2
                                        #       Parent Loop BB3_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r10,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm0
	addsd	(%r9,%rsi,8), %xmm0
	movsd	%xmm0, (%r9,%rsi,8)
	movsd	(%r12,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rax,%rsi,8), %xmm0
	addsd	(%r8,%rsi,8), %xmm0
	movsd	%xmm0, (%r8,%rsi,8)
	movsd	(%r13,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r15,%rsi,8), %xmm0
	addsd	(%rbp,%rsi,8), %xmm0
	movsd	%xmm0, (%rbp,%rsi,8)
	incq	%rsi
	cmpl	%esi, %r14d
	jne	.LBB3_12
# BB#13:                                # %._crit_edge.us.us
                                        #   in Loop: Header=BB3_11 Depth=3
	incl	%ebx
	addl	32(%rsp), %r11d         # 4-byte Folded Reload
	cmpl	16(%rsp), %ebx          # 4-byte Folded Reload
	jne	.LBB3_11
# BB#14:                                #   in Loop: Header=BB3_9 Depth=2
	movl	108(%rsp), %eax         # 4-byte Reload
	movl	152(%rsp), %r11d        # 4-byte Reload
	movl	144(%rsp), %edx         # 4-byte Reload
	movl	136(%rsp), %esi         # 4-byte Reload
.LBB3_15:                               # %._crit_edge890.us
                                        #   in Loop: Header=BB3_9 Depth=2
	addl	%edx, %eax
	addl	112(%rsp), %eax         # 4-byte Folded Reload
	incl	%esi
	cmpl	40(%rsp), %esi          # 4-byte Folded Reload
	movl	%eax, %edx
	jne	.LBB3_9
# BB#16:                                # %._crit_edge910
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 260(%rsp)           # 4-byte Folded Reload
	movl	268(%rsp), %esi         # 4-byte Reload
	movl	264(%rsp), %eax         # 4-byte Reload
	jle	.LBB3_31
.LBB3_17:                               # %.lr.ph937
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_31
# BB#18:                                # %.lr.ph937
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_31
# BB#19:                                # %.preheader883.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	32(%rsp), %edi          # 4-byte Reload
	movl	%edi, %ecx
	subl	%r14d, %ecx
	imull	%eax, %ecx
	addl	%edi, %ecx
	subl	%r14d, %ecx
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movl	%edi, %eax
	imull	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	subl	%esi, %eax
	movq	240(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %ecx
	subl	204(%rsp), %ecx         # 4-byte Folded Reload
	movl	4(%rdx), %edx
	movq	248(%rsp), %rsi         # 8-byte Reload
	subl	(%rsi), %edx
	imull	%edx, %r11d
	addl	%ecx, %r11d
	imull	%edi, %r11d
	addl	%eax, %r11d
	movl	%r14d, %r8d
	andl	$1, %r8d
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$8, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	addq	$8, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	48(%rsp), %r10          # 8-byte Reload
	addq	$8, %r10
	movq	88(%rsp), %r12          # 8-byte Reload
	addq	$8, %r12
	movq	168(%rsp), %r15         # 8-byte Reload
	addq	$8, %r15
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_20:                               # %.preheader883.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_22 Depth 3
                                        #         Child Loop BB3_27 Depth 4
	testl	%r14d, %r14d
	movl	144(%rsp), %eax         # 4-byte Reload
	jle	.LBB3_30
# BB#21:                                # %.preheader879.us.us.preheader
                                        #   in Loop: Header=BB3_20 Depth=2
	movl	%ecx, 192(%rsp)         # 4-byte Spill
	xorl	%ecx, %ecx
	movl	%r11d, 152(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB3_22:                               # %.preheader879.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_20 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_27 Depth 4
	testl	%r8d, %r8d
	movslq	%r11d, %rbx
	jne	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_22 Depth=3
	xorl	%ebp, %ebp
	cmpl	$1, %r14d
	jne	.LBB3_26
	jmp	.LBB3_28
	.p2align	4, 0x90
.LBB3_24:                               #   in Loop: Header=BB3_22 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax,%rbx,8)
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax,%rbx,8)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax,%rbx,8)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax,%rbx,8)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	$0, (%rax,%rbx,8)
	incq	%rbx
	movl	$1, %ebp
	cmpl	$1, %r14d
	je	.LBB3_28
.LBB3_26:                               # %.preheader879.us.us.new
                                        #   in Loop: Header=BB3_22 Depth=3
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rbx,8), %rax
	movq	72(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbx,8), %rdi
	leaq	(%r10,%rbx,8), %rsi
	leaq	(%r12,%rbx,8), %r9
	leaq	(%r15,%rbx,8), %rbx
	movl	%r14d, %edx
	subl	%ebp, %edx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB3_27:                               #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_20 Depth=2
                                        #       Parent Loop BB3_22 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	$0, -8(%rax,%r13,8)
	movq	$0, -8(%rdi,%r13,8)
	movq	$0, -8(%rsi,%r13,8)
	movq	$0, -8(%r9,%r13,8)
	movq	$0, -8(%rbx,%r13,8)
	movq	$0, (%rax,%r13,8)
	movq	$0, (%rdi,%r13,8)
	movq	$0, (%rsi,%r13,8)
	movq	$0, (%r9,%r13,8)
	movq	$0, (%rbx,%r13,8)
	addq	$2, %r13
	cmpl	%r13d, %edx
	jne	.LBB3_27
.LBB3_28:                               # %._crit_edge914.us.us
                                        #   in Loop: Header=BB3_22 Depth=3
	addl	32(%rsp), %r11d         # 4-byte Folded Reload
	incl	%ecx
	cmpl	16(%rsp), %ecx          # 4-byte Folded Reload
	jne	.LBB3_22
# BB#29:                                #   in Loop: Header=BB3_20 Depth=2
	movl	136(%rsp), %eax         # 4-byte Reload
	movl	152(%rsp), %r11d        # 4-byte Reload
	movl	192(%rsp), %ecx         # 4-byte Reload
.LBB3_30:                               # %._crit_edge918.us
                                        #   in Loop: Header=BB3_20 Depth=2
	addl	%r11d, %eax
	addl	112(%rsp), %eax         # 4-byte Folded Reload
	incl	%ecx
	cmpl	40(%rsp), %ecx          # 4-byte Folded Reload
	movl	%eax, %r11d
	jne	.LBB3_20
.LBB3_31:                               # %._crit_edge938
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$27, 208(%rsp)          # 4-byte Folded Reload
	jne	.LBB3_59
# BB#32:                                #   in Loop: Header=BB3_3 Depth=1
	movq	128(%rsp), %rdi         # 8-byte Reload
	leaq	212(%rsp), %rsi
	callq	hypre_BoxGetSize
	xorps	%xmm1, %xmm1
	movq	272(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %ecx
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	-4(%rdx), %eax
	movl	%eax, %r14d
	subl	%ecx, %r14d
	incl	%r14d
	cmpl	%ecx, %eax
	movl	-12(%rdx), %esi
	movl	(%rdx), %eax
	movl	$0, %edx
	cmovsl	%edx, %r14d
	movl	%eax, %edi
	subl	%esi, %edi
	incl	%edi
	cmpl	%esi, %eax
	cmovsl	%edx, %edi
	movl	%edi, 56(%rsp)          # 4-byte Spill
	movl	212(%rsp), %r13d
	movl	216(%rsp), %eax
	movl	220(%rsp), %edx
	cmpl	%r13d, %eax
	movl	%r13d, %edi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmovgel	%eax, %edi
	cmpl	%edi, %edx
	movl	%edx, 48(%rsp)          # 4-byte Spill
	cmovgel	%edx, %edi
	testl	%edi, %edi
	jle	.LBB3_59
# BB#33:                                # %.lr.ph965
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_59
# BB#34:                                # %.lr.ph965
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_59
# BB#35:                                # %.preheader882.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%edi, 80(%rsp)          # 4-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %edi
	subl	%ecx, %edi
	movq	240(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx), %eax
	movl	4(%rcx), %ebp
	subl	%esi, %eax
	movq	248(%rsp), %rcx         # 8-byte Reload
	subl	(%rcx), %ebp
	movl	56(%rsp), %ecx          # 4-byte Reload
	imull	%ecx, %ebp
	addl	%eax, %ebp
	imull	%r14d, %ebp
	movl	%r14d, %eax
	subl	%r13d, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
	subl	%edx, %ecx
	imull	%r14d, %ecx
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	leal	-1(%rdx), %ecx
	imull	%eax, %ecx
	addl	%r14d, %ecx
	subl	%r13d, %ecx
	movl	%ecx, 96(%rsp)          # 4-byte Spill
	movq	224(%rsp), %rcx         # 8-byte Reload
	movslq	%ecx, %rax
	movl	%r14d, %esi
	imull	%edx, %esi
	movl	%esi, 88(%rsp)          # 4-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	leal	(%rdi,%rbp), %edx
	notl	%ecx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	shlq	$3, %rax
	movq	160(%rsp), %rcx         # 8-byte Reload
	subq	%rax, %rcx
	addq	$8, %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_36:                               # %.preheader882.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_38 Depth 3
                                        #         Child Loop BB3_39 Depth 4
	testl	%r13d, %r13d
	movl	96(%rsp), %eax          # 4-byte Reload
	jle	.LBB3_42
# BB#37:                                # %.preheader878.us.us.preheader
                                        #   in Loop: Header=BB3_36 Depth=2
	movq	224(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rdx), %ebx
	xorl	%edi, %edi
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	%edx, %esi
	.p2align	4, 0x90
.LBB3_38:                               # %.preheader878.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_36 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_39 Depth 4
	movslq	%esi, %rax
	movq	184(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %r8
	movq	288(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %r9
	movq	176(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %r10
	movq	280(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,8), %r11
	leaq	(%rcx,%rax,8), %r15
	xorl	%edx, %edx
	movl	%ebx, %r12d
	movq	120(%rsp), %rax         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_39:                               #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_36 Depth=2
                                        #       Parent Loop BB3_38 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%r8,%rdx,8), %xmm0     # xmm0 = mem[0],zero
	movslq	%r12d, %r12
	addsd	(%rax,%r12,8), %xmm0
	addsd	(%r9,%rdx,8), %xmm0
	movsd	%xmm0, (%r9,%rdx,8)
	movsd	(%r10,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r15,%rdx,8), %xmm0
	addsd	(%r11,%rdx,8), %xmm0
	movsd	%xmm0, (%r11,%rdx,8)
	incl	%r12d
	incq	%rdx
	cmpl	%edx, %r13d
	jne	.LBB3_39
# BB#40:                                # %._crit_edge942.us.us
                                        #   in Loop: Header=BB3_38 Depth=3
	incl	%edi
	addl	%r14d, %ebx
	addl	%r14d, %esi
	cmpl	16(%rsp), %edi          # 4-byte Folded Reload
	jne	.LBB3_38
# BB#41:                                #   in Loop: Header=BB3_36 Depth=2
	movl	88(%rsp), %eax          # 4-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB3_42:                               # %._crit_edge946.us
                                        #   in Loop: Header=BB3_36 Depth=2
	addl	%edx, %eax
	addl	24(%rsp), %eax          # 4-byte Folded Reload
	incl	%ebp
	cmpl	48(%rsp), %ebp          # 4-byte Folded Reload
	movl	%eax, %edx
	jne	.LBB3_36
# BB#43:                                # %._crit_edge966
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	jle	.LBB3_59
# BB#44:                                # %.lr.ph993
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_59
# BB#45:                                # %.lr.ph993.split.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_59
# BB#46:                                # %.lr.ph993.split.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	testl	%r13d, %r13d
	jle	.LBB3_59
# BB#47:                                # %.preheader881.us.us.us.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	addl	72(%rsp), %eax          # 4-byte Folded Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leal	-1(%r13), %ecx
	movq	184(%rsp), %r11         # 8-byte Reload
	leaq	8(%r11,%rcx,8), %rax
	movq	176(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rdi,%rcx,8), %r10
	movq	160(%rsp), %rsi         # 8-byte Reload
	leaq	8(%rsi,%rcx,8), %r15
	movq	120(%rsp), %rbp         # 8-byte Reload
	leaq	8(%rbp,%rcx,8), %r9
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	1(%rcx), %r12
	movq	%r12, %rdx
	movabsq	$8589934588, %rcx       # imm = 0x1FFFFFFFC
	andq	%rcx, %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	leaq	-4(%rdx), %r8
	shrq	$2, %r8
	cmpq	%r10, %r11
	sbbb	%cl, %cl
	cmpq	%rax, %rdi
	sbbb	%dl, %dl
	andb	%cl, %dl
	cmpq	%r15, %r11
	sbbb	%cl, %cl
	cmpq	%rax, %rsi
	sbbb	%bl, %bl
	andb	%cl, %bl
	orb	%dl, %bl
	cmpq	%r9, %r11
	sbbb	%cl, %cl
	cmpq	%rax, %rbp
	sbbb	%al, %al
	andb	%cl, %al
	cmpq	%r15, %rdi
	sbbb	%cl, %cl
	cmpq	%r10, %rsi
	sbbb	%dl, %dl
	andb	%cl, %dl
	orb	%al, %dl
	orb	%bl, %dl
	cmpq	%r9, %rdi
	sbbb	%al, %al
	cmpq	%r10, %rbp
	sbbb	%cl, %cl
	andb	%al, %cl
	cmpq	%r9, %rsi
	sbbb	%al, %al
	cmpq	%r15, %rbp
	sbbb	%bl, %bl
	andb	%al, %bl
	orb	%cl, %bl
	orb	%dl, %bl
	andb	$1, %bl
	movb	%bl, 192(%rsp)          # 1-byte Spill
	movq	%r8, 168(%rsp)          # 8-byte Spill
                                        # kill: %R8D<def> %R8D<kill> %R8<kill> %R8<def>
	andl	$1, %r8d
	movq	%r8, 40(%rsp)           # 8-byte Spill
	leaq	48(%rbp), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	56(%rsp), %eax          # 4-byte Reload
	imull	%r14d, %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	48(%rsi), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	leaq	48(%rdi), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leaq	48(%r11), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	leaq	24(%rbp), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	leaq	24(%rsi), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	leaq	24(%rdi), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%r12, %rdi
	leaq	24(%r11), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_48:                               # %.preheader881.us.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_49 Depth 3
                                        #         Child Loop BB3_68 Depth 4
                                        #         Child Loop BB3_52 Depth 4
                                        #         Child Loop BB3_56 Depth 4
	movl	%ecx, 108(%rsp)         # 4-byte Spill
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%eax, %r15d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB3_49:                               # %.preheader.us.us.us.us
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_48 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB3_68 Depth 4
                                        #         Child Loop BB3_52 Depth 4
                                        #         Child Loop BB3_56 Depth 4
	movslq	%r15d, %r10
	xorl	%r11d, %r11d
	cmpq	$3, %rdi
	jbe	.LBB3_50
# BB#61:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_49 Depth=3
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB3_50
# BB#62:                                # %vector.memcheck
                                        #   in Loop: Header=BB3_49 Depth=3
	cmpb	$0, 192(%rsp)           # 1-byte Folded Reload
	jne	.LBB3_50
# BB#63:                                # %vector.body.preheader
                                        #   in Loop: Header=BB3_49 Depth=3
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	jne	.LBB3_64
# BB#65:                                # %vector.body.prol
                                        #   in Loop: Header=BB3_49 Depth=3
	movq	184(%rsp), %rax         # 8-byte Reload
	movups	%xmm1, (%rax,%r10,8)
	movups	%xmm1, 16(%rax,%r10,8)
	movq	176(%rsp), %rax         # 8-byte Reload
	movups	%xmm1, (%rax,%r10,8)
	movups	%xmm1, 16(%rax,%r10,8)
	movq	160(%rsp), %rax         # 8-byte Reload
	movups	%xmm1, (%rax,%r10,8)
	movups	%xmm1, 16(%rax,%r10,8)
	movq	120(%rsp), %rax         # 8-byte Reload
	movups	%xmm1, (%rax,%r10,8)
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movups	%xmm1, 16(%rax,%r10,8)
	movl	$4, %esi
	jmp	.LBB3_66
.LBB3_64:                               #   in Loop: Header=BB3_49 Depth=3
	xorl	%esi, %esi
.LBB3_66:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB3_49 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpq	$0, 168(%rsp)           # 8-byte Folded Reload
	je	.LBB3_69
# BB#67:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB3_49 Depth=3
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rdx
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rbx
	movq	144(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rbp
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r10,8), %rcx
	.p2align	4, 0x90
.LBB3_68:                               # %vector.body
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_48 Depth=2
                                        #       Parent Loop BB3_49 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movups	%xmm1, -48(%rcx,%rsi,8)
	movups	%xmm1, -32(%rcx,%rsi,8)
	movups	%xmm1, -48(%rbp,%rsi,8)
	movups	%xmm1, -32(%rbp,%rsi,8)
	movups	%xmm1, -48(%rbx,%rsi,8)
	movups	%xmm1, -32(%rbx,%rsi,8)
	movups	%xmm1, -48(%rdx,%rsi,8)
	movups	%xmm1, -32(%rdx,%rsi,8)
	movups	%xmm1, -16(%rcx,%rsi,8)
	movups	%xmm1, (%rcx,%rsi,8)
	movups	%xmm1, -16(%rbp,%rsi,8)
	movups	%xmm1, (%rbp,%rsi,8)
	movups	%xmm1, -16(%rbx,%rsi,8)
	movups	%xmm1, (%rbx,%rsi,8)
	movups	%xmm1, -16(%rdx,%rsi,8)
	movups	%xmm1, (%rdx,%rsi,8)
	addq	$8, %rsi
	cmpq	%rsi, %rax
	jne	.LBB3_68
.LBB3_69:                               # %middle.block
                                        #   in Loop: Header=BB3_49 Depth=3
	cmpq	%rax, %rdi
	je	.LBB3_57
# BB#70:                                #   in Loop: Header=BB3_49 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	%rax, %r10
	movl	%eax, %r11d
	.p2align	4, 0x90
.LBB3_50:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_49 Depth=3
	movl	%r13d, %edx
	subl	%r11d, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %esi
	subl	%r11d, %esi
	testb	$3, %dl
	je	.LBB3_54
# BB#51:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB3_49 Depth=3
	movq	%rdi, %r8
	movq	120(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %rcx
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %rbp
	movq	176(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %rbx
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r10,8), %rdi
	andl	$3, %edx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_52:                               # %scalar.ph.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_48 Depth=2
                                        #       Parent Loop BB3_49 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	$0, (%rdi,%rax,8)
	movq	$0, (%rbx,%rax,8)
	movq	$0, (%rbp,%rax,8)
	movq	$0, (%rcx,%rax,8)
	incq	%rax
	cmpl	%eax, %edx
	jne	.LBB3_52
# BB#53:                                # %scalar.ph.prol.loopexit.unr-lcssa
                                        #   in Loop: Header=BB3_49 Depth=3
	addq	%rax, %r10
	addl	%eax, %r11d
	movq	%r8, %rdi
.LBB3_54:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_49 Depth=3
	cmpl	$3, %esi
	jb	.LBB3_57
# BB#55:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB3_49 Depth=3
	movq	96(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,8), %rsi
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,8), %r8
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,8), %r12
	movq	72(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r10,8), %rbx
	movl	%r13d, %edx
	subl	%r11d, %edx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_56:                               # %scalar.ph
                                        #   Parent Loop BB3_3 Depth=1
                                        #     Parent Loop BB3_48 Depth=2
                                        #       Parent Loop BB3_49 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	$0, -24(%rbx,%rbp,8)
	movq	$0, -24(%r12,%rbp,8)
	movq	$0, -24(%r8,%rbp,8)
	movq	$0, -24(%rsi,%rbp,8)
	movq	$0, -16(%rbx,%rbp,8)
	movq	$0, -16(%r12,%rbp,8)
	movq	$0, -16(%r8,%rbp,8)
	movq	$0, -16(%rsi,%rbp,8)
	movq	$0, -8(%rbx,%rbp,8)
	movq	$0, -8(%r12,%rbp,8)
	movq	$0, -8(%r8,%rbp,8)
	movq	$0, -8(%rsi,%rbp,8)
	movq	$0, (%rbx,%rbp,8)
	movq	$0, (%r12,%rbp,8)
	movq	$0, (%r8,%rbp,8)
	movq	$0, (%rsi,%rbp,8)
	addq	$4, %rbp
	cmpl	%ebp, %edx
	jne	.LBB3_56
.LBB3_57:                               # %._crit_edge970.us.us.us.us
                                        #   in Loop: Header=BB3_49 Depth=3
	addl	%r14d, %r15d
	incl	%r9d
	cmpl	16(%rsp), %r9d          # 4-byte Folded Reload
	jne	.LBB3_49
# BB#58:                                # %._crit_edge974.us-lcssa.us.us.us.us
                                        #   in Loop: Header=BB3_48 Depth=2
	movl	108(%rsp), %ecx         # 4-byte Reload
	incl	%ecx
	movq	64(%rsp), %rax          # 8-byte Reload
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	cmpl	48(%rsp), %ecx          # 4-byte Folded Reload
	jne	.LBB3_48
	.p2align	4, 0x90
.LBB3_59:                               # %.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	304(%rsp), %rbx         # 8-byte Reload
	incq	%rbx
	movq	296(%rsp), %rbp         # 8-byte Reload
	movslq	8(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB3_3
.LBB3_60:                               # %.loopexit885
	xorl	%eax, %eax
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_SMG3RAPPeriodicSym, .Lfunc_end3-hypre_SMG3RAPPeriodicSym
	.cfi_endproc

	.globl	hypre_SMG3RAPPeriodicNoSym
	.p2align	4, 0x90
	.type	hypre_SMG3RAPPeriodicNoSym,@function
hypre_SMG3RAPPeriodicNoSym:             # @hypre_SMG3RAPPeriodicNoSym
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi52:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 56
	subq	$392, %rsp              # imm = 0x188
.Lcfi54:
	.cfi_def_cfa_offset 448
.Lcfi55:
	.cfi_offset %rbx, -56
.Lcfi56:
	.cfi_offset %r12, -48
.Lcfi57:
	.cfi_offset %r13, -40
.Lcfi58:
	.cfi_offset %r14, -32
.Lcfi59:
	.cfi_offset %r15, -24
.Lcfi60:
	.cfi_offset %rbp, -16
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movq	8(%rdi), %rax
	cmpl	$1, 64(%rax)
	jne	.LBB4_29
# BB#1:                                 # %.preheader622
	movq	8(%rax), %rcx
	cmpl	$0, 8(%rcx)
	jle	.LBB4_29
# BB#2:                                 # %.lr.ph679
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movl	8(%rax), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	$0, %r13d
                                        # implicit-def: %RAX
	movq	%rax, 248(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 240(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 232(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 224(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 216(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 208(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 200(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 192(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 184(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 176(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 168(%rsp)         # 8-byte Spill
                                        # implicit-def: %RAX
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%rcx, 256(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_8 Depth 2
                                        #       Child Loop BB4_11 Depth 3
                                        #         Child Loop BB4_12 Depth 4
                                        #     Child Loop BB4_21 Depth 2
                                        #       Child Loop BB4_23 Depth 3
                                        #         Child Loop BB4_24 Depth 4
	movq	(%rcx), %rcx
	leaq	(%r13,%r13,2), %rax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	(%rcx,%rax,8), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	40(%rbx), %rax
	movq	(%rax), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	$0, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rsp, %rbp
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$4294967295, %r14d      # imm = 0xFFFFFFFF
	movq	%r14, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 384(%rsp)         # 8-byte Spill
	movq	$1, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movabsq	$-4294967296, %r15      # imm = 0xFFFFFFFF00000000
	movq	%r15, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %r12       # imm = 0x100000000
	movq	%r12, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movq	$0, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	%r14, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movq	$1, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	%r15, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movq	%r12, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	$0, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	%r14, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	$1, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	%r15, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	%r12, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 280(%rsp)         # 8-byte Spill
	cmpl	$27, 48(%rsp)           # 4-byte Folded Reload
	jne	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	$-1, (%rsp)
	movl	$-1, 8(%rsp)
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rsp, %rbp
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movabsq	$-4294967295, %r14      # imm = 0xFFFFFFFF00000001
	movq	%r14, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movabsq	$8589934591, %r15       # imm = 0x1FFFFFFFF
	movq	%r15, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movabsq	$4294967297, %r12       # imm = 0x100000001
	movq	%r12, (%rsp)
	movl	$-1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	$-1, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movq	%r14, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	%r15, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%r12, (%rsp)
	movl	$0, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	$-1, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movq	%r14, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	%r15, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%r12, (%rsp)
	movl	$1, 8(%rsp)
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	callq	hypre_StructMatrixExtractPointerByIndex
	movq	%rax, 160(%rsp)         # 8-byte Spill
.LBB4_5:                                #   in Loop: Header=BB4_3 Depth=1
	movq	152(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	leaq	52(%rsp), %rsi
	callq	hypre_BoxGetSize
	movq	%r13, 272(%rsp)         # 8-byte Spill
	leaq	(,%r13,8), %rax
	leaq	(%rax,%rax,2), %rax
	movq	144(%rsp), %rbp         # 8-byte Reload
	movl	(%rbp,%rax), %r9d
	movl	4(%rbp,%rax), %r8d
	movl	12(%rbp,%rax), %ecx
	movl	%ecx, %edi
	subl	%r9d, %edi
	incl	%edi
	cmpl	%r9d, %ecx
	movl	16(%rbp,%rax), %edx
	movl	$0, %esi
	cmovsl	%esi, %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movl	%edx, %ecx
	subl	%r8d, %ecx
	incl	%ecx
	cmpl	%r8d, %edx
	cmovsl	%esi, %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rax), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rax, 264(%rsp)         # 8-byte Spill
	leaq	8(%rbp,%rax), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movl	52(%rsp), %edi
	movl	56(%rsp), %eax
	movl	60(%rsp), %esi
	cmpl	%edi, %eax
	movl	%edi, %edx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmovgel	%eax, %edx
	cmpl	%edx, %esi
	cmovgel	%esi, %edx
	testl	%edx, %edx
	jle	.LBB4_16
# BB#6:                                 # %.lr.ph635
                                        #   in Loop: Header=BB4_3 Depth=1
	testl	%esi, %esi
	jle	.LBB4_16
# BB#7:                                 # %.preheader621.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	12(%rsp), %r10d         # 4-byte Reload
	movl	%r10d, %r14d
	subl	%edi, %r14d
	movl	%ecx, %edx
	movq	24(%rsp), %r11          # 8-byte Reload
	subl	%r11d, %edx
	imull	%r10d, %edx
	leal	-1(%r11), %eax
	imull	%r14d, %eax
	addl	%r10d, %eax
	subl	%edi, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	%r10d, %eax
	imull	%r11d, %eax
	movl	%eax, 108(%rsp)         # 4-byte Spill
	movl	(%r15), %r11d
	subl	%r9d, %r11d
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %r9d
	subl	%r8d, %r9d
	movl	4(%rax), %ebx
	movq	136(%rsp), %rax         # 8-byte Reload
	subl	(%rax), %ebx
	imull	%ecx, %ebx
	addl	%r9d, %ebx
	imull	%r10d, %ebx
	addl	%r11d, %ebx
	xorl	%eax, %eax
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movl	%esi, 116(%rsp)         # 4-byte Spill
	movl	%edx, 112(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB4_8:                                # %.preheader621
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_11 Depth 3
                                        #         Child Loop BB4_12 Depth 4
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_15
# BB#9:                                 # %.preheader619.lr.ph
                                        #   in Loop: Header=BB4_8 Depth=2
	testl	%edi, %edi
	jle	.LBB4_30
# BB#10:                                # %.preheader619.us.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	movl	%eax, 120(%rsp)         # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebx, 124(%rsp)         # 4-byte Spill
	movl	%ebx, %ecx
	.p2align	4, 0x90
.LBB4_11:                               # %.preheader619.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_12 Depth 4
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movslq	%ecx, %r11
	movq	88(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	352(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	384(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	304(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %r13
	movq	344(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rbx
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rbp
	movq	296(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %r15
	movq	336(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %r9
	movq	368(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %r10
	movq	288(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rcx
	movq	328(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rsi
	movq	360(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %r8
	movq	280(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r11,8), %rax
	movq	320(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%r11,8), %rdx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB4_12:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_8 Depth=2
                                        #       Parent Loop BB4_11 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	40(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r11,8), %xmm0    # xmm0 = mem[0],zero
	movq	96(%rsp), %r14          # 8-byte Reload
	addsd	(%r14,%r11,8), %xmm0
	movq	32(%rsp), %r12          # 8-byte Reload
	addsd	(%r12,%r11,8), %xmm0
	movsd	%xmm0, (%r12,%r11,8)
	movq	$0, (%rdi,%r11,8)
	movq	$0, (%r14,%r11,8)
	movq	72(%rsp), %rdi          # 8-byte Reload
	movsd	(%rdi,%r11,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r13,%r11,8), %xmm0
	addsd	(%rbx,%r11,8), %xmm0
	movsd	%xmm0, (%rbx,%r11,8)
	movq	$0, (%rdi,%r11,8)
	movq	$0, (%r13,%r11,8)
	movsd	(%rbp,%r11,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r15,%r11,8), %xmm0
	addsd	(%r9,%r11,8), %xmm0
	movsd	%xmm0, (%r9,%r11,8)
	movq	$0, (%rbp,%r11,8)
	movq	$0, (%r15,%r11,8)
	movsd	(%r10,%r11,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rcx,%r11,8), %xmm0
	addsd	(%rsi,%r11,8), %xmm0
	movsd	%xmm0, (%rsi,%r11,8)
	movq	$0, (%r10,%r11,8)
	movq	$0, (%rcx,%r11,8)
	movsd	(%r8,%r11,8), %xmm0     # xmm0 = mem[0],zero
	addsd	(%rax,%r11,8), %xmm0
	addsd	(%rdx,%r11,8), %xmm0
	movsd	%xmm0, (%rdx,%r11,8)
	movq	$0, (%r8,%r11,8)
	movq	$0, (%rax,%r11,8)
	incq	%r11
	cmpl	%r11d, 64(%rsp)         # 4-byte Folded Reload
	jne	.LBB4_12
# BB#13:                                # %._crit_edge.us
                                        #   in Loop: Header=BB4_11 Depth=3
	movl	20(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	addl	12(%rsp), %ecx          # 4-byte Folded Reload
	cmpl	24(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB4_11
# BB#14:                                # %._crit_edge628.loopexit
                                        #   in Loop: Header=BB4_8 Depth=2
	movl	124(%rsp), %ebx         # 4-byte Reload
	addl	108(%rsp), %ebx         # 4-byte Folded Reload
	movq	152(%rsp), %r15         # 8-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movl	116(%rsp), %esi         # 4-byte Reload
	movl	112(%rsp), %edx         # 4-byte Reload
	movl	120(%rsp), %eax         # 4-byte Reload
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_30:                               # %.preheader619.preheader
                                        #   in Loop: Header=BB4_8 Depth=2
	addl	104(%rsp), %ebx         # 4-byte Folded Reload
.LBB4_15:                               # %._crit_edge628
                                        #   in Loop: Header=BB4_8 Depth=2
	addl	%edx, %ebx
	incl	%eax
	cmpl	%esi, %eax
	jne	.LBB4_8
.LBB4_16:                               # %._crit_edge636
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	$27, 48(%rsp)           # 4-byte Folded Reload
	jne	.LBB4_28
# BB#17:                                #   in Loop: Header=BB4_3 Depth=1
	movq	264(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %r14
	leaq	4(%rbp,%rax), %rbp
	movq	%r15, %rdi
	leaq	52(%rsp), %rsi
	callq	hypre_BoxGetSize
	movl	(%r14), %r9d
	movl	8(%rbp), %eax
	movl	%eax, %edi
	subl	%r9d, %edi
	incl	%edi
	cmpl	%r9d, %eax
	movl	(%rbp), %edx
	movl	12(%rbp), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %edi
	movl	%edi, 72(%rsp)          # 4-byte Spill
	movl	%eax, %edi
	subl	%edx, %edi
	incl	%edi
	cmpl	%edx, %eax
	cmovsl	%ecx, %edi
	movl	52(%rsp), %ecx
	movl	56(%rsp), %ebp
	movl	60(%rsp), %esi
	cmpl	%ecx, %ebp
	movl	%ecx, %eax
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	cmovgel	%ebp, %eax
	cmpl	%eax, %esi
	movl	%esi, 16(%rsp)          # 4-byte Spill
	cmovgel	%esi, %eax
	testl	%eax, %eax
	jle	.LBB4_28
# BB#18:                                # %.lr.ph663
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_28
# BB#19:                                # %.lr.ph663
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB4_28
# BB#20:                                # %.preheader620.us.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	72(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %eax
	subl	%ecx, %eax
	movl	%edi, %esi
	movq	32(%rsp), %r8           # 8-byte Reload
	subl	%r8d, %esi
	imull	%ebp, %esi
	movl	%esi, 24(%rsp)          # 4-byte Spill
	leal	-1(%r8), %esi
	imull	%eax, %esi
	addl	%ebp, %esi
	subl	%ecx, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%ebp, %eax
	imull	%r8d, %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	movl	(%r15), %eax
	subl	%r9d, %eax
	movq	128(%rsp), %rbx         # 8-byte Reload
	movl	(%rbx), %esi
	subl	%edx, %esi
	movl	4(%rbx), %ebx
	movq	136(%rsp), %rdx         # 8-byte Reload
	subl	(%rdx), %ebx
	imull	%edi, %ebx
	addl	%esi, %ebx
	imull	%ebp, %ebx
	addl	%eax, %ebx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_21:                               # %.preheader620.us
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_23 Depth 3
                                        #         Child Loop BB4_24 Depth 4
	testl	%ecx, %ecx
	movl	12(%rsp), %eax          # 4-byte Reload
	jle	.LBB4_27
# BB#22:                                # %.preheader.us.us.preheader
                                        #   in Loop: Header=BB4_21 Depth=2
	movl	%edx, 20(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movl	%ebx, 64(%rsp)          # 4-byte Spill
	movl	%ebx, %edx
	.p2align	4, 0x90
.LBB4_23:                               # %.preheader.us.us
                                        #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_21 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB4_24 Depth 4
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movslq	%edx, %rsi
	movq	248(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rbp
	movq	184(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rdi
	movq	216(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,8), %rax
	movq	240(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi,8), %rdx
	movq	176(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r8
	movq	208(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r9
	movq	232(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r10
	movq	168(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r11
	movq	200(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r14
	movq	224(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r15
	movq	160(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r12
	movq	192(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,8), %r13
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_24:                               #   Parent Loop BB4_3 Depth=1
                                        #     Parent Loop BB4_21 Depth=2
                                        #       Parent Loop BB4_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rbp,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%rdi,%rsi,8), %xmm0
	addsd	(%rax,%rsi,8), %xmm0
	movsd	%xmm0, (%rax,%rsi,8)
	movq	$0, (%rbp,%rsi,8)
	movq	$0, (%rdi,%rsi,8)
	movsd	(%rdx,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r8,%rsi,8), %xmm0
	addsd	(%r9,%rsi,8), %xmm0
	movsd	%xmm0, (%r9,%rsi,8)
	movq	$0, (%rdx,%rsi,8)
	movq	$0, (%r8,%rsi,8)
	movsd	(%r10,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r11,%rsi,8), %xmm0
	addsd	(%r14,%rsi,8), %xmm0
	movsd	%xmm0, (%r14,%rsi,8)
	movq	$0, (%r10,%rsi,8)
	movq	$0, (%r11,%rsi,8)
	movsd	(%r15,%rsi,8), %xmm0    # xmm0 = mem[0],zero
	addsd	(%r12,%rsi,8), %xmm0
	addsd	(%r13,%rsi,8), %xmm0
	movsd	%xmm0, (%r13,%rsi,8)
	movq	$0, (%r15,%rsi,8)
	movq	$0, (%r12,%rsi,8)
	incq	%rsi
	cmpl	%esi, %ecx
	jne	.LBB4_24
# BB#25:                                # %._crit_edge640.us.us
                                        #   in Loop: Header=BB4_23 Depth=3
	movl	40(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	96(%rsp), %edx          # 4-byte Reload
	addl	72(%rsp), %edx          # 4-byte Folded Reload
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB4_23
# BB#26:                                #   in Loop: Header=BB4_21 Depth=2
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	64(%rsp), %ebx          # 4-byte Reload
	movl	20(%rsp), %edx          # 4-byte Reload
.LBB4_27:                               # %._crit_edge644.us
                                        #   in Loop: Header=BB4_21 Depth=2
	addl	%ebx, %eax
	addl	24(%rsp), %eax          # 4-byte Folded Reload
	incl	%edx
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	movl	%eax, %ebx
	jne	.LBB4_21
	.p2align	4, 0x90
.LBB4_28:                               # %.loopexit
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	272(%rsp), %r13         # 8-byte Reload
	incq	%r13
	movq	256(%rsp), %rcx         # 8-byte Reload
	movslq	8(%rcx), %rax
	cmpq	%rax, %r13
	jl	.LBB4_3
.LBB4_29:                               # %.loopexit623
	xorl	%eax, %eax
	addq	$392, %rsp              # imm = 0x188
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	hypre_SMG3RAPPeriodicNoSym, .Lfunc_end4-hypre_SMG3RAPPeriodicNoSym
	.cfi_endproc

	.type	.Lhypre_SMG3CreateRAPOp.RAP_num_ghost,@object # @hypre_SMG3CreateRAPOp.RAP_num_ghost
	.section	.rodata,"a",@progbits
	.p2align	4
.Lhypre_SMG3CreateRAPOp.RAP_num_ghost:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	.Lhypre_SMG3CreateRAPOp.RAP_num_ghost, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
