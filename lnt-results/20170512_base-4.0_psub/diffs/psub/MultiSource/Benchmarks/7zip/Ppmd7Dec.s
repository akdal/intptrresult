	.text
	.file	"Ppmd7Dec.bc"
	.globl	Ppmd7z_RangeDec_Init
	.p2align	4, 0x90
	.type	Ppmd7z_RangeDec_Init,@function
Ppmd7z_RangeDec_Init:                   # @Ppmd7z_RangeDec_Init
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, 24(%rbx)
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movl	%eax, %ecx
	xorl	%eax, %eax
	testb	%cl, %cl
	jne	.LBB0_2
# BB#1:                                 # %.preheader
	movl	28(%rbx), %r14d
	shll	$8, %r14d
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %ebp
	orl	%r14d, %ebp
	movl	%ebp, 28(%rbx)
	shll	$8, %ebp
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %r14d
	orl	%ebp, %r14d
	movl	%r14d, 28(%rbx)
	shll	$8, %r14d
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %ebp
	orl	%r14d, %ebp
	movl	%ebp, 28(%rbx)
	shll	$8, %ebp
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %ecx
	orl	%ebp, %ecx
	movl	%ecx, 28(%rbx)
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	setne	%al
.LBB0_2:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	Ppmd7z_RangeDec_Init, .Lfunc_end0-Ppmd7z_RangeDec_Init
	.cfi_endproc

	.globl	Ppmd7z_RangeDec_CreateVTable
	.p2align	4, 0x90
	.type	Ppmd7z_RangeDec_CreateVTable,@function
Ppmd7z_RangeDec_CreateVTable:           # @Ppmd7z_RangeDec_CreateVTable
	.cfi_startproc
# BB#0:
	movq	$Range_GetThreshold, (%rdi)
	movq	$Range_Decode, 8(%rdi)
	movq	$Range_DecodeBit, 16(%rdi)
	retq
.Lfunc_end1:
	.size	Ppmd7z_RangeDec_CreateVTable, .Lfunc_end1-Ppmd7z_RangeDec_CreateVTable
	.cfi_endproc

	.p2align	4, 0x90
	.type	Range_GetThreshold,@function
Range_GetThreshold:                     # @Range_GetThreshold
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	movl	28(%rdi), %ecx
	xorl	%edx, %edx
	divl	%esi
	movl	%eax, %esi
	movl	%esi, 24(%rdi)
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	retq
.Lfunc_end2:
	.size	Range_GetThreshold, .Lfunc_end2-Range_GetThreshold
	.cfi_endproc

	.p2align	4, 0x90
	.type	Range_Decode,@function
Range_Decode:                           # @Range_Decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	24(%rbx), %eax
	movl	28(%rbx), %ebp
	imull	%eax, %esi
	subl	%esi, %ebp
	movl	%ebp, 28(%rbx)
	imull	%eax, %edx
	movl	%edx, 24(%rbx)
	cmpl	$16777215, %edx         # imm = 0xFFFFFF
	ja	.LBB3_3
# BB#1:
	shll	$8, %ebp
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %r14d
	orl	%ebp, %r14d
	movl	%r14d, 28(%rbx)
	movl	24(%rbx), %eax
	shll	$8, %eax
	movl	%eax, 24(%rbx)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB3_3
# BB#2:
	shll	$8, %r14d
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%r14d, %eax
	movl	%eax, 28(%rbx)
	shll	$8, 24(%rbx)
.LBB3_3:                                # %Range_Normalize.exit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	Range_Decode, .Lfunc_end3-Range_Decode
	.cfi_endproc

	.p2align	4, 0x90
	.type	Range_DecodeBit,@function
Range_DecodeBit:                        # @Range_DecodeBit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	24(%rbx), %ecx
	movl	28(%rbx), %edx
	movl	%ecx, %eax
	shrl	$14, %eax
	imull	%esi, %eax
	xorl	%r14d, %r14d
	movl	%edx, %ebp
	subl	%eax, %ebp
	jae	.LBB4_2
# BB#1:
	movl	%edx, %ebp
	jmp	.LBB4_3
.LBB4_2:
	movl	%ebp, 28(%rbx)
	subl	%eax, %ecx
	movl	$1, %r14d
	movl	%ecx, %eax
.LBB4_3:
	movl	%eax, 24(%rbx)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB4_6
# BB#4:
	shll	$8, %ebp
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %r15d
	orl	%ebp, %r15d
	movl	%r15d, 28(%rbx)
	movl	24(%rbx), %eax
	shll	$8, %eax
	movl	%eax, 24(%rbx)
	cmpl	$16777215, %eax         # imm = 0xFFFFFF
	ja	.LBB4_6
# BB#5:
	shll	$8, %r15d
	movq	32(%rbx), %rdi
	callq	*(%rdi)
	movzbl	%al, %eax
	orl	%r15d, %eax
	movl	%eax, 28(%rbx)
	shll	$8, 24(%rbx)
.LBB4_6:                                # %Range_Normalize.exit
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Range_DecodeBit, .Lfunc_end4-Range_DecodeBit
	.cfi_endproc

	.globl	Ppmd7_DecodeSymbol
	.p2align	4, 0x90
	.type	Ppmd7_DecodeSymbol,@function
Ppmd7_DecodeSymbol:                     # @Ppmd7_DecodeSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$2344, %rsp             # imm = 0x928
.Lcfi27:
	.cfi_def_cfa_offset 2400
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	(%r13), %rax
	movzwl	(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB5_1
# BB#14:
	movzbl	3(%rax), %ebx
	shlq	$7, %rbx
	addq	%r13, %rbx
	movq	16(%r13), %rcx
	movq	64(%r13), %rdx
	movl	8(%rax), %esi
	movzwl	(%rdx,%rsi), %edx
	movzbl	683(%r13,%rdx), %edx
	addl	32(%r13), %edx
	movzbl	(%rcx), %ecx
	movzbl	940(%r13,%rcx), %ecx
	movl	%ecx, 40(%r13)
	addl	%ecx, %edx
	movzbl	2(%rax), %eax
	movzbl	940(%r13,%rax), %eax
	leal	(%rdx,%rax,2), %eax
	movl	44(%r13), %ebp
	shrl	$26, %ebp
	andl	$32, %ebp
	addl	%eax, %ebp
	leaq	2672(%rbx,%rbp,2), %r14
	movzwl	2672(%rbx,%rbp,2), %esi
	movq	%r12, %rdi
	callq	*16(%r12)
	testl	%eax, %eax
	movzwl	2672(%rbx,%rbp,2), %eax
	je	.LBB5_40
# BB#15:                                # %.thread203
	leaq	64(%r13), %r15
	leal	32(%rax), %ecx
	shrl	$7, %ecx
	subl	%ecx, %eax
	movw	%ax, (%r14)
	shrl	$10, %eax
	andl	$63, %eax
	movzbl	PPMD7_kExpEscape(%rax), %eax
	movl	%eax, 28(%r13)
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	movq	(%r13), %rax
	movzbl	2(%rax), %ecx
	movb	$0, 32(%rsp,%rcx)
	movl	$0, 32(%r13)
	leaq	8(%r12), %rbx
.LBB5_16:                               # %.preheader
	movq	%r12, 8(%rsp)           # 8-byte Spill
	leaq	4(%rsp), %rdx
	jmp	.LBB5_17
.LBB5_1:
	movq	64(%r13), %rbx
	movl	4(%rax), %ebp
	movzwl	2(%rax), %esi
	movq	%r12, %rdi
	callq	*(%r12)
	movzbl	1(%rbx,%rbp), %edx
	cmpl	%edx, %eax
	jae	.LBB5_3
# BB#2:
	addq	%rbp, %rbx
	xorl	%esi, %esi
	movq	%r12, %rdi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	*8(%r12)
	movq	%rbx, 16(%r13)
	movzbl	(%rbx), %ebx
	movq	%r13, %rdi
	callq	Ppmd7_Update1_0
	jmp	.LBB5_41
	.p2align	4, 0x90
.LBB5_38:                               #   in Loop: Header=BB5_17 Depth=1
	movq	(%r13), %rax
.LBB5_17:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_18 Depth 2
                                        #     Child Loop BB5_21 Depth 2
                                        #     Child Loop BB5_34 Depth 2
                                        #     Child Loop BB5_37 Depth 2
	movzwl	(%rax), %ebp
	movl	24(%r13), %edi
	.p2align	4, 0x90
.LBB5_18:                               #   Parent Loop BB5_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rax), %esi
	incl	%edi
	testq	%rsi, %rsi
	je	.LBB5_39
# BB#19:                                #   in Loop: Header=BB5_18 Depth=2
	movq	(%r15), %rcx
	leaq	(%rcx,%rsi), %rax
	movq	%rax, (%r13)
	movzwl	(%rcx,%rsi), %r12d
	cmpw	%bp, %r12w
	je	.LBB5_18
# BB#20:                                #   in Loop: Header=BB5_17 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movl	%edi, 24(%r13)
	movl	4(%rax), %eax
	addq	%rax, %rcx
	movl	%r12d, %r14d
	subl	%ebp, %r14d
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_21:                               #   Parent Loop BB5_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %esi
	movsbl	32(%rsp,%rsi), %edi
	movzbl	1(%rcx), %esi
	andl	%edi, %esi
	addl	%esi, %ebx
	movl	%eax, %esi
	movq	%rcx, 288(%rsp,%rsi,8)
	subl	%edi, %eax
	addq	$6, %rcx
	cmpl	%r14d, %eax
	jne	.LBB5_21
# BB#22:                                #   in Loop: Header=BB5_17 Depth=1
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	Ppmd7_MakeEscFreq
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	4(%rsp), %esi
	addl	%ebx, %esi
	movl	%esi, 4(%rsp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	*(%rdi)
	cmpl	%ebx, %eax
	jb	.LBB5_23
# BB#29:                                #   in Loop: Header=BB5_17 Depth=1
	movl	4(%rsp), %edx
	cmpl	%edx, %eax
	jae	.LBB5_30
# BB#31:                                #   in Loop: Header=BB5_17 Depth=1
	subl	%ebx, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	%ebx, %esi
	movq	24(%rsp), %rbx          # 8-byte Reload
	callq	*(%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx), %eax
	addl	4(%rsp), %eax
	movw	%ax, (%rcx)
	decl	%r12d
	subl	%ebp, %r12d
	movl	%r14d, %eax
	andl	$3, %eax
	je	.LBB5_32
# BB#33:                                # %.prol.preheader
                                        #   in Loop: Header=BB5_17 Depth=1
	negl	%eax
	leaq	4(%rsp), %rdx
	.p2align	4, 0x90
.LBB5_34:                               #   Parent Loop BB5_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%r14d
	movq	288(%rsp,%r14,8), %rcx
	movzbl	(%rcx), %ecx
	movb	$0, 32(%rsp,%rcx)
	incl	%eax
	jne	.LBB5_34
	jmp	.LBB5_35
.LBB5_32:                               #   in Loop: Header=BB5_17 Depth=1
	leaq	4(%rsp), %rdx
.LBB5_35:                               # %.prol.loopexit
                                        #   in Loop: Header=BB5_17 Depth=1
	cmpl	$3, %r12d
	jb	.LBB5_38
# BB#36:                                # %.new
                                        #   in Loop: Header=BB5_17 Depth=1
	addl	$-2, %r14d
	.p2align	4, 0x90
.LBB5_37:                               #   Parent Loop BB5_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	1(%r14), %eax
	movq	288(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 32(%rsp,%rax)
	movl	%r14d, %eax
	movq	288(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 32(%rsp,%rax)
	leal	-1(%r14), %eax
	movq	288(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 32(%rsp,%rax)
	leal	-2(%r14), %eax
	movq	288(%rsp,%rax,8), %rax
	movzbl	(%rax), %eax
	movb	$0, 32(%rsp,%rax)
	addl	$-4, %r14d
	cmpl	$-2, %r14d
	jne	.LBB5_37
	jmp	.LBB5_38
.LBB5_39:                               # %.thread205.loopexit
	movl	%edi, 24(%r13)
	movl	$-1, %ebx
	jmp	.LBB5_41
.LBB5_3:
	leaq	64(%r13), %r15
	movl	$0, 32(%r13)
	movq	(%r13), %r8
	movzwl	(%r8), %ecx
	movl	$1, %edi
	subl	%ecx, %edi
	leaq	6(%rbx,%rbp), %rbp
	.p2align	4, 0x90
.LBB5_4:                                # =>This Inner Loop Header: Depth=1
	movzbl	1(%rbp), %ecx
	leal	(%rcx,%rdx), %esi
	cmpl	%eax, %esi
	ja	.LBB5_5
# BB#6:                                 #   in Loop: Header=BB5_4 Depth=1
	addq	$6, %rbp
	incl	%edi
	movl	%esi, %edx
	jne	.LBB5_4
# BB#7:
	movzwl	2(%r8), %edx
	movl	$-2, %ebx
	cmpl	%edx, %eax
	jae	.LBB5_41
# BB#8:
	movq	16(%r13), %rax
	movzbl	(%rax), %eax
	movzbl	940(%r13,%rax), %eax
	movl	%eax, 40(%r13)
	subl	%esi, %edx
	movq	%r12, %rdi
	callq	*8(%r12)
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	movzbl	-6(%rbp), %eax
	leaq	-6(%rbp), %rcx
	movb	$0, 32(%rsp,%rax)
	movq	(%r13), %rax
	movzwl	(%rax), %esi
	leal	-1(%rsi), %edx
	addl	$-2, %esi
	movl	%edx, %edi
	andl	$3, %edi
	je	.LBB5_11
# BB#9:                                 # %.prol.preheader318
	negl	%edi
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movzbl	-6(%rcx), %ebp
	addq	$-6, %rcx
	movb	$0, 32(%rsp,%rbp)
	decl	%edx
	incl	%edi
	jne	.LBB5_10
.LBB5_11:                               # %.prol.loopexit319
	leaq	8(%r12), %rbx
	cmpl	$3, %esi
	jb	.LBB5_16
# BB#12:                                # %.new320
	addq	$-6, %rcx
	.p2align	4, 0x90
.LBB5_13:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %esi
	movb	$0, 32(%rsp,%rsi)
	movzbl	-6(%rcx), %esi
	movb	$0, 32(%rsp,%rsi)
	movzbl	-12(%rcx), %esi
	movb	$0, 32(%rsp,%rsi)
	movzbl	-18(%rcx), %esi
	movb	$0, 32(%rsp,%rsi)
	addq	$-24, %rcx
	addl	$-4, %edx
	jne	.LBB5_13
	jmp	.LBB5_16
.LBB5_40:
	leal	32(%rax), %ecx
	shrl	$7, %ecx
	negl	%ecx
	leal	128(%rax,%rcx), %eax
	movw	%ax, (%r14)
	movq	(%r13), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, 16(%r13)
	movzbl	2(%rax), %ebx
	movq	%r13, %rdi
	callq	Ppmd7_UpdateBin
	jmp	.LBB5_41
.LBB5_5:
	movq	%r12, %rdi
	movl	%edx, %esi
	movl	%ecx, %edx
	callq	*8(%r12)
	movq	%rbp, 16(%r13)
	movzbl	(%rbp), %ebx
	movq	%r13, %rdi
	callq	Ppmd7_Update1
.LBB5_41:                               # %.thread
	movl	%ebx, %eax
	addq	$2344, %rsp             # imm = 0x928
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_23:
	xorl	%edi, %edi
	leaq	288(%rsp), %rcx
	.p2align	4, 0x90
.LBB5_24:                               # =>This Inner Loop Header: Depth=1
	movl	%edi, %esi
	movq	(%rcx), %rbx
	movzbl	1(%rbx), %edx
	leal	(%rdx,%rsi), %edi
	addq	$8, %rcx
	cmpl	%eax, %edi
	jbe	.LBB5_24
# BB#25:
	movq	8(%rsp), %rdi           # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	24(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movq	16(%rsp), %rdx          # 8-byte Reload
	movzbl	2(%rdx), %ecx
	cmpl	$6, %ecx
	ja	.LBB5_28
# BB#26:
	decb	3(%rdx)
	jne	.LBB5_28
# BB#27:
	shlw	(%rdx)
	movl	%ecx, %eax
	incb	%al
	movb	%al, 2(%rdx)
	movl	$3, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 3(%rdx)
.LBB5_28:
	movq	%rbx, 16(%r13)
	movzbl	(%rbx), %ebx
	movq	%r13, %rdi
	callq	Ppmd7_Update2
	jmp	.LBB5_41
.LBB5_30:
	movl	$-2, %ebx
	jmp	.LBB5_41
.Lfunc_end5:
	.size	Ppmd7_DecodeSymbol, .Lfunc_end5-Ppmd7_DecodeSymbol
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
