	.text
	.file	"opo.bc"
	.globl	phase_assignment
	.p2align	4, 0x90
	.type	phase_assignment,@function
phase_assignment:                       # @phase_assignment
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	movl	%ecx, %eax
	andl	$-2, %eax
	movl	%esi, %edx
	sarl	$31, %edx
	shrl	$30, %edx
	addl	%esi, %edx
	subl	%eax, %esi
	movl	%esi, skip_make_sparse(%rip)
	movl	%ecx, %eax
	sarl	%eax
	shrl	$31, %ecx
	addl	%eax, %ecx
	andl	$-2, %ecx
	subl	%ecx, %eax
	movl	%eax, opo_repeated(%rip)
	movl	%edx, %ecx
	sarl	$2, %ecx
	shrl	$31, %edx
	addl	%ecx, %edx
	andl	$-2, %edx
	subl	%edx, %ecx
	movl	%ecx, opo_exact(%rip)
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
# BB#1:
	callq	free
	movq	$0, 40(%rbx)
	movl	opo_repeated(%rip), %eax
.LBB0_2:
	testl	%eax, %eax
	je	.LBB0_7
# BB#3:
	movq	cube+88(%rip), %r14
	movl	(%r14), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB0_5
# BB#4:
	movl	$8, %edi
	jmp	.LBB0_6
.LBB0_7:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	find_phase
	movq	%rax, 40(%rbx)
	jmp	.LBB0_8
.LBB0_5:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB0_6:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r14, %rsi
	callq	set_copy
	movq	%rax, 40(%rbx)
	movq	%rbx, %rdi
	callq	repeated_phase_assignment
.LBB0_8:
	movl	$0, skip_make_sparse(%rip)
	movq	%rbx, %rdi
	callq	set_phase
	movl	opo_exact(%rip), %ebp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	cmpl	$0, %ebp
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	je	.LBB0_12
# BB#9:
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
	movq	%rax, %r15
	movq	%r15, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB0_14
# BB#10:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movl	$.L.str.9, %esi
	jmp	.LBB0_11
.LBB0_12:
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %r15
	movq	%r15, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB0_14
# BB#13:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movl	$.L.str.10, %esi
.LBB0_11:
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rcx, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	print_trace             # TAILCALL
.LBB0_14:                               # %minimize.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	phase_assignment, .Lfunc_end0-phase_assignment
	.cfi_endproc

	.globl	repeated_phase_assignment
	.p2align	4, 0x90
	.type	repeated_phase_assignment,@function
repeated_phase_assignment:              # @repeated_phase_assignment
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	cmpl	$0, (%rax,%rcx,4)
	jle	.LBB1_9
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rdx
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	find_phase
	movq	%rax, %rbx
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %ecx
	addl	%ebp, %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rbx,%rax,4), %esi
	movl	$1, %edx
	shll	%cl, %edx
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %esi
	jb	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	incq	%rax
	notl	%edx
	movq	40(%r14), %rcx
	andl	%edx, (%rcx,%rax,4)
.LBB1_4:                                #   in Loop: Header=BB1_2 Depth=1
	movl	summary(%rip), %eax
	orl	trace(%rip), %eax
	je	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movq	40(%r14), %rdi
	xorl	%eax, %eax
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=1
	testq	%rbx, %rbx
	je	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=1
	incl	%ebp
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	cmpl	(%rax,%rcx,4), %ebp
	jl	.LBB1_2
.LBB1_9:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	repeated_phase_assignment, .Lfunc_end1-repeated_phase_assignment
	.cfi_endproc

	.globl	find_phase
	.p2align	4, 0x90
	.type	find_phase,@function
find_phase:                             # @find_phase
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movl	%esi, %r15d
	movq	%rdi, %rbp
	movq	cube+88(%rip), %rbx
	movl	(%rbx), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB2_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB2_3
.LBB2_2:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB2_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	callq	set_copy
	movq	%rax, %r14
	xorl	%eax, %eax
	callq	new_PLA
	movq	%rax, %rbx
	movq	(%rbp), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, (%rbx)
	movq	16(%rbp), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 16(%rbx)
	movq	8(%rbp), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 8(%rbx)
	testq	%r12, %r12
	je	.LBB2_8
# BB#4:
	movl	(%r12), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB2_6
# BB#5:
	movl	$8, %edi
	jmp	.LBB2_7
.LBB2_6:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB2_7:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r12, %rsi
	callq	set_copy
	movq	%rax, 40(%rbx)
	movq	%rbx, %rdi
	callq	set_phase
.LBB2_8:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	output_phase_setup
	cmpl	$0, summary(%rip)
	je	.LBB2_10
# BB#9:
	movq	(%rbx), %r12
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB2_10:
	movl	opo_exact(%rip), %ebp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r12
	cmpl	$0, %ebp
	movq	(%rbx), %rdi
	movq	8(%rbx), %rsi
	movq	16(%rbx), %rdx
	je	.LBB2_13
# BB#11:
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB2_16
# BB#12:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r12, %rcx
	movl	$.L.str.9, %esi
	jmp	.LBB2_15
.LBB2_13:
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB2_16
# BB#14:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r12, %rcx
	movl	$.L.str.10, %esi
.LBB2_15:                               # %minimize.exit
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB2_16:                               # %minimize.exit
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r12
	movq	(%rbx), %rsi
	movq	8(%rbx), %rdx
	movq	%r14, %rdi
	movl	%r15d, %r8d
	callq	opo
	movq	%rax, %rbp
	movq	%rbp, (%rbx)
	cmpl	$0, summary(%rip)
	je	.LBB2_18
# BB#17:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r12, %rcx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB2_18:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	free_PLA
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %edx
	movl	%edx, %esi
	subl	%r15d, %esi
	movl	%esi, %edi
	shrl	$31, %edi
	addl	%esi, %edi
	sarl	%edi
	subl	%edi, %edx
	movl	%edx, (%rax,%rcx,4)
	xorl	%eax, %eax
	callq	cube_setup
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	find_phase, .Lfunc_end2-find_phase
	.cfi_endproc

	.globl	opo
	.p2align	4, 0x90
	.type	opo,@function
opo:                                    # @opo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 112
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	12(%rsi), %r15d
	cmpl	$33, %r15d
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	jge	.LBB3_2
# BB#1:
	movl	$8, %edi
	jmp	.LBB3_3
.LBB3_2:
	leal	-1(%r15), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB3_3:
	callq	malloc
	movq	%rax, %rcx
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r15d, %esi
	callq	set_fill
	movq	%rax, %r12
	testl	%r13d, %r13d
	movq	(%rsp), %rdi            # 8-byte Reload
	jle	.LBB3_11
# BB#4:                                 # %.lr.ph153
	movq	cube+16(%rip), %r8
	movq	24(%rdi), %r9
	movl	12(%rdi), %esi
	.p2align	4, 0x90
.LBB3_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	testl	%esi, %esi
	jle	.LBB3_10
# BB#6:                                 # %.lr.ph149
                                        #   in Loop: Header=BB3_5 Depth=1
	movslq	cube+124(%rip), %rcx
	movl	(%r8,%rcx,4), %ecx
	addl	%r14d, %ecx
	movl	%ecx, %edx
	sarl	$5, %edx
	incl	%edx
	movslq	%edx, %rbp
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	xorl	%ecx, %ecx
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB3_7:                                #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	(%rdx,%rbp,4), %ebx
	je	.LBB3_9
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=2
	movl	$-2, %esi
	roll	%cl, %esi
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	andl	%esi, 4(%r12,%rax,4)
	movl	12(%rdi), %esi
.LBB3_9:                                #   in Loop: Header=BB3_7 Depth=2
	movslq	(%rdi), %rax
	leaq	(%rdx,%rax,4), %rdx
	incl	%ecx
	cmpl	%esi, %ecx
	jl	.LBB3_7
.LBB3_10:                               # %._crit_edge150
                                        #   in Loop: Header=BB3_5 Depth=1
	incl	%r14d
	cmpl	%r13d, %r14d
	jne	.LBB3_5
.LBB3_11:                               # %._crit_edge154
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %eax
	subl	%r13d, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	movl	%eax, 20(%rsp)          # 4-byte Spill
	addl	%eax, %ecx
	sarl	%ecx
	leal	-1(%rcx,%r13), %r9d
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r12, %rdx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r13d, %r8d
	callq	opo_recur
	movq	%rax, %r14
	movq	24(%r14), %r15
	movq	(%rsp), %rax            # 8-byte Reload
	movl	12(%rax), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rax, %rsi
	movl	12(%rdx), %eax
	testl	%eax, %eax
	jle	.LBB3_16
# BB#12:                                # %.lr.ph144
	movq	24(%rdx), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_13:                               # =>This Inner Loop Header: Depth=1
	movl	%ebx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	movl	4(%r15,%rcx,4), %ecx
	btl	%ebx, %ecx
	jb	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_13 Depth=1
	xorl	%eax, %eax
	movq	%rsi, %rdi
	movq	%rbp, %rsi
	callq	sf_addset
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rax, %rsi
	movl	12(%rdx), %eax
.LBB3_15:                               #   in Loop: Header=BB3_13 Depth=1
	movslq	(%rdx), %rcx
	leaq	(%rbp,%rcx,4), %rbp
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB3_13
.LBB3_16:                               # %._crit_edge145
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	testq	%r12, %r12
	je	.LBB3_18
# BB#17:
	movq	%r12, %rdi
	callq	free
.LBB3_18:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	%rax, %r14
	movl	cube(%rip), %ebp
	cmpl	$33, %ebp
	jge	.LBB3_20
# BB#19:
	movl	$8, %edi
	jmp	.LBB3_21
.LBB3_20:
	leal	-1(%rbp), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB3_21:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%ebp, %esi
	callq	set_clear
	movq	%rax, %rbp
	movl	cube(%rip), %r15d
	cmpl	$33, %r15d
	movq	%r13, 48(%rsp)          # 8-byte Spill
	jge	.LBB3_23
# BB#22:
	movl	$8, %edi
	jmp	.LBB3_24
.LBB3_23:
	leal	-1(%r15), %eax
	sarl	$5, %eax
	cltq
	leaq	8(,%rax,4), %rdi
.LBB3_24:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r15d, %esi
	callq	set_clear
	movq	%rax, %r13
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	(%rdi), %eax
	movl	12(%rdi), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB3_33
# BB#25:                                # %.lr.ph138
	movq	24(%rdi), %r12
	movslq	%ecx, %rcx
	leaq	(%r12,%rcx,4), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	(%r14), %ecx
	.p2align	4, 0x90
.LBB3_26:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_28 Depth 2
	movslq	12(%r14), %rsi
	movslq	%ecx, %rdx
	imulq	%rsi, %rdx
	testl	%edx, %edx
	jle	.LBB3_32
# BB#27:                                # %.lr.ph134.preheader
                                        #   in Loop: Header=BB3_26 Depth=1
	movq	24(%r14), %r15
	leaq	(%r15,%rdx,4), %rbx
	.p2align	4, 0x90
.LBB3_28:                               # %.lr.ph134
                                        #   Parent Loop BB3_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	cdist0
	testl	%eax, %eax
	je	.LBB3_30
# BB#29:                                #   in Loop: Header=BB3_28 Depth=2
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	set_and
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	set_or
.LBB3_30:                               #   in Loop: Header=BB3_28 Depth=2
	movslq	(%r14), %rcx
	leaq	(%r15,%rcx,4), %r15
	cmpq	%rbx, %r15
	jb	.LBB3_28
# BB#31:                                # %._crit_edge135.loopexit
                                        #   in Loop: Header=BB3_26 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	(%rdi), %eax
.LBB3_32:                               # %._crit_edge135
                                        #   in Loop: Header=BB3_26 Depth=1
	movslq	%eax, %rdx
	leaq	(%r12,%rdx,4), %r12
	cmpq	8(%rsp), %r12           # 8-byte Folded Reload
	jb	.LBB3_26
.LBB3_33:                               # %._crit_edge139
	xorl	%eax, %eax
	callq	sf_free
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	testq	%r13, %r13
	je	.LBB3_35
# BB#34:
	movq	%r13, %rdi
	callq	free
.LBB3_35:                               # %.preheader
	cmpl	$2, 20(%rsp)            # 4-byte Folded Reload
	jl	.LBB3_42
# BB#36:                                # %.lr.ph.preheader
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	leal	(%r12,%rbx), %r14d
	movq	40(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_37:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %esi
	leal	(%rbx,%rsi), %ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rbp,%rax,4), %edi
	movl	$1, %edx
	shll	%cl, %edx
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %edi
	jae	.LBB3_41
# BB#38:                                #   in Loop: Header=BB3_37 Depth=1
	leal	(%r12,%rbx), %ecx
	addl	%esi, %ecx
	movl	%ecx, %esi
	sarl	$5, %esi
	movslq	%esi, %rsi
	movl	4(%rbp,%rsi,4), %esi
	btl	%ecx, %esi
	jae	.LBB3_40
# BB#39:                                #   in Loop: Header=BB3_37 Depth=1
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	fatal
	jmp	.LBB3_41
	.p2align	4, 0x90
.LBB3_40:                               #   in Loop: Header=BB3_37 Depth=1
	incq	%rax
	notl	%edx
	andl	%edx, (%r15,%rax,4)
.LBB3_41:                               #   in Loop: Header=BB3_37 Depth=1
	incl	%ebx
	cmpl	%r14d, %ebx
	jl	.LBB3_37
	jmp	.LBB3_43
.LBB3_42:                               # %._crit_edge
	testq	%rbp, %rbp
	je	.LBB3_44
.LBB3_43:                               # %._crit_edge.thread
	movq	%rbp, %rdi
	callq	free
.LBB3_44:
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	opo, .Lfunc_end3-opo
	.cfi_endproc

	.globl	opo_recur
	.p2align	4, 0x90
	.type	opo_recur,@function
opo_recur:                              # @opo_recur
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 64
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %ebx
	movq	%rdx, %r12
	movq	%rsi, %rbp
	movq	%rdi, %r13
	incl	opo_recur.level(%rip)
	cmpl	%r15d, %r8d
	jne	.LBB4_2
# BB#1:
	addl	%r8d, %ebx
	movq	%r13, %rdi
	movq	%r12, %rsi
	movl	%r8d, %edx
	movl	%ebx, %ecx
	callq	opo_leaf
	movq	%rax, %rbx
	jmp	.LBB4_5
.LBB4_2:
	leal	(%r15,%r8), %eax
	movl	%eax, %r14d
	shrl	$31, %r14d
	addl	%eax, %r14d
	sarl	%r14d
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movl	%ebx, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r14d, %r9d
	callq	opo_recur
	incl	%r14d
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movq	%rax, %rbp
	movl	%ebx, %ecx
	movl	%r14d, %r8d
	movl	%r15d, %r9d
	callq	opo_recur
	movq	%rax, %r15
	xorl	%edx, %edx
	cmpl	$1, opo_recur.level(%rip)
	sete	%dl
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	unate_intersect
	movq	%rax, %rbx
	cmpl	$0, trace(%rip)
	je	.LBB4_4
# BB#3:
	movl	opo_recur.level(%rip), %r14d
	decl	%r14d
	movl	12(%rbx), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	12(%rbp), %r12d
	movl	12(%r15), %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rdi
	callq	util_print_time
	movq	%rax, %r9
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	movl	%r12d, %ecx
	movl	%r13d, %r8d
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB4_4:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	sf_free
.LBB4_5:
	decl	opo_recur.level(%rip)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	opo_recur, .Lfunc_end4-opo_recur
	.cfi_endproc

	.globl	opo_leaf
	.p2align	4, 0x90
	.type	opo_leaf,@function
opo_leaf:                               # @opo_leaf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 80
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%edx, %r12d
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	12(%rbx), %esi
	xorl	%ebp, %ebp
	movl	$2, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r14
	movq	24(%r14), %rax
	movl	12(%r14), %ecx
	leal	1(%rcx), %edx
	imull	(%r14), %ecx
	movl	%edx, 12(%r14)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %r15
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	movl	12(%rsp), %r13d         # 4-byte Reload
	callq	set_copy
	movl	12(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB5_5
# BB#1:                                 # %.lr.ph51
	addl	%r13d, %r12d
	movq	24(%rbx), %rax
	movl	%r12d, %ecx
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rsi
	movl	$1, %edi
	movl	%r12d, %ecx
	shll	%cl, %edi
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	testl	(%rax,%rsi,4), %edi
	je	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	$-2, %edx
	movl	%ebp, %ecx
	roll	%cl, %edx
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%edx, 4(%r15,%rcx,4)
	movl	12(%rbx), %edx
.LBB5_4:                                #   in Loop: Header=BB5_2 Depth=1
	movslq	(%rbx), %rcx
	leaq	(%rax,%rcx,4), %rax
	incl	%ebp
	cmpl	%edx, %ebp
	jl	.LBB5_2
.LBB5_5:                                # %._crit_edge52
	movq	24(%r14), %rax
	movl	12(%r14), %ecx
	leal	1(%rcx), %edx
	imull	(%r14), %ecx
	movl	%edx, 12(%r14)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %r15
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	set_copy
	movl	12(%rbx), %edx
	testl	%edx, %edx
	jle	.LBB5_10
# BB#6:                                 # %.lr.ph
	addl	8(%rsp), %r13d          # 4-byte Folded Reload
	movq	24(%rbx), %rax
	movl	%r13d, %ecx
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rsi
	movl	$1, %edi
	movl	%r13d, %ecx
	shll	%cl, %edi
	.p2align	4, 0x90
.LBB5_7:                                # =>This Inner Loop Header: Depth=1
	testl	(%rax,%rsi,4), %edi
	je	.LBB5_9
# BB#8:                                 #   in Loop: Header=BB5_7 Depth=1
	movl	$-2, %edx
	movl	%ebp, %ecx
	roll	%cl, %edx
	movl	%ebp, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%edx, 4(%r15,%rcx,4)
	movl	12(%rbx), %edx
.LBB5_9:                                #   in Loop: Header=BB5_7 Depth=1
	movslq	(%rbx), %rcx
	leaq	(%rax,%rcx,4), %rax
	incl	%ebp
	cmpl	%edx, %ebp
	jl	.LBB5_7
.LBB5_10:                               # %._crit_edge
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	opo_leaf, .Lfunc_end5-opo_leaf
	.cfi_endproc

	.globl	output_phase_setup
	.p2align	4, 0x90
	.type	output_phase_setup,@function
output_phase_setup:                     # @output_phase_setup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 288
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	cube+124(%rip), %eax
	cmpl	$-1, %eax
	jne	.LBB6_2
# BB#1:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	cube+124(%rip), %eax
.LBB6_2:
	movq	(%rbx), %r12
	movq	8(%rbx), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 128(%rsp)         # 8-byte Spill
	movq	16(%rbx), %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	cube+16(%rip), %rcx
	cltq
	movl	(%rcx,%rax,4), %ecx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	leal	(%rcx,%rbp), %ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movq	cube+24(%rip), %rcx
	movl	(%rcx,%rax,4), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	cube+32(%rip), %rcx
	movl	(%rcx,%rax,4), %ecx
	movq	%rcx, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	%ecx, %ebx
	movq	%rbp, 200(%rsp)         # 8-byte Spill
	subl	%ebp, %ebx
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	addl	%ebx, (%rax,%rcx,4)
	xorl	%eax, %eax
	callq	cube_setup
	movq	cube+88(%rip), %rbx
	movl	(%rbx), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB6_4
# BB#3:
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB6_4:
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	callq	set_copy
	movq	%rax, %r13
	movl	60(%rsp), %eax          # 4-byte Reload
	cmpl	cube(%rip), %eax
	jge	.LBB6_7
# BB#5:                                 # %.lr.ph279.preheader
	movl	%eax, %ecx
	.p2align	4, 0x90
.LBB6_6:                                # %.lr.ph279
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2, %eax
	roll	%cl, %eax
	movl	%ecx, %edx
	sarl	$5, %edx
	movslq	%edx, %rdx
	andl	%eax, 4(%r13,%rdx,4)
	incl	%ecx
	cmpl	cube(%rip), %ecx
	jl	.LBB6_6
.LBB6_7:                                # %._crit_edge280
	movl	(%r13), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB6_9
# BB#8:
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB6_9:
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	set_copy
	movq	%rax, %rbp
	movq	cube+16(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %ecx
	movl	60(%rsp), %edi          # 4-byte Reload
	cmpl	%edi, %ecx
	jge	.LBB6_14
# BB#10:                                # %.lr.ph275.preheader
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	192(%rsp), %rsi         # 8-byte Reload
	leal	(%rsi,%rdx), %eax
	subl	%ecx, %eax
	leal	-1(%rsi,%rdx), %edx
	testb	$1, %al
	movl	%ecx, %eax
	je	.LBB6_12
# BB#11:                                # %.lr.ph275.prol
	movl	$-2, %eax
	roll	%cl, %eax
	movl	%ecx, %esi
	sarl	$5, %esi
	movslq	%esi, %rsi
	andl	%eax, 4(%rbp,%rsi,4)
	leal	1(%rcx), %eax
.LBB6_12:                               # %.lr.ph275.prol.loopexit
	cmpl	%ecx, %edx
	je	.LBB6_14
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph275
                                        # =>This Inner Loop Header: Depth=1
	movl	$-2, %edx
	movl	%eax, %ecx
	roll	%cl, %edx
	movl	%eax, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%edx, 4(%rbp,%rcx,4)
	leal	1(%rax), %ecx
	movl	$-2, %edx
	roll	%cl, %edx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%edx, 4(%rbp,%rcx,4)
	addl	$2, %eax
	cmpl	%edi, %eax
	jne	.LBB6_13
.LBB6_14:                               # %._crit_edge276
	movq	120(%rsp), %rbx         # 8-byte Reload
	movl	12(%rbx), %edi
	addl	12(%r12), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	callq	sf_new
	movq	128(%rsp), %rbp         # 8-byte Reload
	movq	%rax, (%rbp)
	movl	12(%rbx), %edi
	addl	12(%r12), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, 16(%rbp)
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	12(%rax), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movq	%rax, 8(%rbp)
	movslq	(%r12), %rcx
	movslq	12(%r12), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movl	8(%rsp), %r11d          # 4-byte Reload
	jle	.LBB6_96
# BB#15:                                # %.lr.ph271
	movq	24(%r12), %rdx
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rcx
	movq	16(%rax), %r15
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	24(%rcx), %rax
	movq	24(%r15), %rcx
	leaq	-4(%rcx), %rsi
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	4(%rcx), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	leaq	-4(%r8), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	leaq	4(%r8), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	leaq	-4(%rax), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	4(%rax), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	-4(%r13), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	4(%r13), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	cmpl	%r11d, 60(%rsp)         # 4-byte Folded Reload
	movq	%r15, 24(%rsp)          # 8-byte Spill
	leaq	-12(%r13), %rax
	jle	.LBB6_16
# BB#21:                                # %.lr.ph271.split.us.preheader
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-12(%r8), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$-1024, %ebx            # imm = 0xFC00
	.p2align	4, 0x90
.LBB6_22:                               # %.lr.ph271.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_32 Depth 2
                                        #     Child Loop BB6_36 Depth 2
                                        #     Child Loop BB6_50 Depth 2
                                        #     Child Loop BB6_54 Depth 2
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi), %eax
	leal	1(%rax), %ecx
	imull	(%rsi), %eax
	movl	%ecx, 12(%rsi)
	movslq	%eax, %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdi,4), %r14
	movl	12(%r15), %eax
	leal	1(%rax), %ecx
	imull	(%r15), %eax
	movl	%ecx, 12(%r15)
	movslq	%eax, %r11
	movl	(%r13), %ecx
	movl	(%rbp,%rdi,4), %eax
	andl	%ebx, %eax
	andl	$1023, %ecx             # imm = 0x3FF
	movq	%rcx, %r9
	movl	$1, %esi
	cmoveq	%rsi, %r9
	movl	%ecx, %esi
	orl	%eax, %esi
	movl	%esi, (%rbp,%rdi,4)
	cmpq	$8, %r9
	jb	.LBB6_35
# BB#23:                                # %min.iters.checked461
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%r9, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB6_35
# BB#24:                                # %vector.memcheck493
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%rcx, %rax
	notq	%rax
	testl	%ecx, %ecx
	movq	$-2, %rsi
	cmovneq	%rsi, %rax
	movq	%rdi, %rsi
	subq	%rax, %rsi
	movq	152(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,4), %r15
	addq	%rcx, %rdi
	movq	144(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdi,4), %rbp
	shlq	$2, %rax
	movq	136(%rsp), %rbx         # 8-byte Reload
	subq	%rax, %rbx
	movq	96(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %r10
	movq	%rdx, %rsi
	subq	%rax, %rsi
	addq	$-4, %rsi
	leaq	4(%rdx,%rcx,4), %r12
	cmpq	%r10, %r15
	sbbb	%r10b, %r10b
	cmpq	%rbp, %rbx
	sbbb	%al, %al
	andb	%r10b, %al
	cmpq	%r12, %r15
	sbbb	%bl, %bl
	cmpq	%rbp, %rsi
	sbbb	%sil, %sil
	testb	$1, %al
	jne	.LBB6_25
# BB#26:                                # %vector.memcheck493
                                        #   in Loop: Header=BB6_22 Depth=1
	andb	%sil, %bl
	andb	$1, %bl
	jne	.LBB6_25
# BB#27:                                # %vector.body456.preheader
                                        #   in Loop: Header=BB6_22 Depth=1
	leaq	-8(%r8), %rax
	movq	%rax, %rsi
	shrq	$3, %rsi
	btl	$3, %eax
	jb	.LBB6_28
# BB#29:                                # %vector.body456.prol
                                        #   in Loop: Header=BB6_22 Depth=1
	movups	-12(%r13,%rcx,4), %xmm0
	movups	-28(%r13,%rcx,4), %xmm1
	movups	-12(%rdx,%rcx,4), %xmm2
	movups	-28(%rdx,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r14,%rcx,4)
	movups	%xmm3, -28(%r14,%rcx,4)
	movl	$8, %r15d
	testq	%rsi, %rsi
	jne	.LBB6_31
	jmp	.LBB6_33
.LBB6_25:                               #   in Loop: Header=BB6_22 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	$-1024, %ebx            # imm = 0xFC00
	jmp	.LBB6_35
.LBB6_28:                               #   in Loop: Header=BB6_22 Depth=1
	xorl	%r15d, %r15d
	testq	%rsi, %rsi
	je	.LBB6_33
.LBB6_31:                               # %vector.body456.preheader.new
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%r9, %rsi
	andq	$-8, %rsi
	negq	%rsi
	negq	%r15
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx,4), %r10
	leaq	-12(%rdx,%rcx,4), %r12
	movq	48(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB6_32:                               # %vector.body456
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r10,%r15,4), %xmm0
	movups	-16(%r10,%r15,4), %xmm1
	movups	(%r12,%r15,4), %xmm2
	movups	-16(%r12,%r15,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%r15,4)
	movups	%xmm3, -28(%rdi,%r15,4)
	movups	-32(%r10,%r15,4), %xmm0
	movups	-48(%r10,%r15,4), %xmm1
	movups	-32(%r12,%r15,4), %xmm2
	movups	-48(%r12,%r15,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rdi,%r15,4)
	movups	%xmm3, -60(%rdi,%r15,4)
	addq	$-16, %r15
	cmpq	%r15, %rsi
	jne	.LBB6_32
.LBB6_33:                               # %middle.block457
                                        #   in Loop: Header=BB6_22 Depth=1
	cmpq	%r8, %r9
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	$-1024, %ebx            # imm = 0xFC00
	je	.LBB6_37
# BB#34:                                #   in Loop: Header=BB6_22 Depth=1
	subq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_35:                               # %scalar.ph458.preheader
                                        #   in Loop: Header=BB6_22 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB6_36:                               # %scalar.ph458
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rcx,4), %eax
	andl	-4(%r13,%rcx,4), %eax
	movl	%eax, -4(%r14,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB6_36
.LBB6_37:                               # %.loopexit871
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%r11,4), %rdi
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	(%r8), %ecx
	movl	(%rbp,%r11,4), %eax
	andl	%ebx, %eax
	andl	$1023, %ecx             # imm = 0x3FF
	movl	$1, %r9d
	cmovneq	%rcx, %r9
	movl	%ecx, %esi
	orl	%eax, %esi
	movl	%esi, (%rbp,%r11,4)
	cmpq	$8, %r9
	jae	.LBB6_39
# BB#38:                                #   in Loop: Header=BB6_22 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_53
	.p2align	4, 0x90
.LBB6_39:                               # %min.iters.checked402
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%r9, %r14
	andq	$1016, %r14             # imm = 0x3F8
	je	.LBB6_40
# BB#41:                                # %vector.memcheck434
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%rcx, %rax
	notq	%rax
	testl	%ecx, %ecx
	movq	$-2, %rsi
	cmovneq	%rsi, %rax
	movq	%r11, %rsi
	subq	%rax, %rsi
	movq	184(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,4), %r10
	addq	%rcx, %r11
	movq	176(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r11,4), %rbp
	shlq	$2, %rax
	movq	168(%rsp), %rbx         # 8-byte Reload
	subq	%rax, %rbx
	movq	160(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,4), %r8
	movq	%rdx, %rsi
	subq	%rax, %rsi
	addq	$-4, %rsi
	leaq	4(%rdx,%rcx,4), %r15
	cmpq	%r8, %r10
	sbbb	%al, %al
	cmpq	%rbp, %rbx
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r15, %r10
	sbbb	%al, %al
	cmpq	%rbp, %rsi
	sbbb	%sil, %sil
	testb	$1, %bl
	jne	.LBB6_42
# BB#43:                                # %vector.memcheck434
                                        #   in Loop: Header=BB6_22 Depth=1
	andb	%sil, %al
	andb	$1, %al
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB6_44
# BB#45:                                # %vector.body397.preheader
                                        #   in Loop: Header=BB6_22 Depth=1
	leaq	-8(%r14), %rax
	movq	%rax, %rsi
	shrq	$3, %rsi
	btl	$3, %eax
	jb	.LBB6_46
# BB#47:                                # %vector.body397.prol
                                        #   in Loop: Header=BB6_22 Depth=1
	movups	-12(%r8,%rcx,4), %xmm0
	movups	-28(%r8,%rcx,4), %xmm1
	movups	-12(%rdx,%rcx,4), %xmm2
	movups	-28(%rdx,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rcx,4)
	movups	%xmm3, -28(%rdi,%rcx,4)
	movl	$8, %eax
	jmp	.LBB6_48
.LBB6_40:                               #   in Loop: Header=BB6_22 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_53
.LBB6_42:                               #   in Loop: Header=BB6_22 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
.LBB6_44:                               #   in Loop: Header=BB6_22 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	$-1024, %ebx            # imm = 0xFC00
	jmp	.LBB6_53
.LBB6_46:                               #   in Loop: Header=BB6_22 Depth=1
	xorl	%eax, %eax
.LBB6_48:                               # %vector.body397.prol.loopexit
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB6_51
# BB#49:                                # %vector.body397.preheader.new
                                        #   in Loop: Header=BB6_22 Depth=1
	movq	%r9, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rax
	movq	104(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rsi
	leaq	-12(%rdx,%rcx,4), %rbp
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r11,4), %rbx
	.p2align	4, 0x90
.LBB6_50:                               # %vector.body397
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%rax,4), %xmm0
	movups	-16(%rsi,%rax,4), %xmm1
	movups	(%rbp,%rax,4), %xmm2
	movups	-16(%rbp,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rax,4)
	movups	%xmm3, -28(%rbx,%rax,4)
	movups	-32(%rsi,%rax,4), %xmm0
	movups	-48(%rsi,%rax,4), %xmm1
	movups	-32(%rbp,%rax,4), %xmm2
	movups	-48(%rbp,%rax,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rbx,%rax,4)
	movups	%xmm3, -60(%rbx,%rax,4)
	addq	$-16, %rax
	cmpq	%rax, %r10
	jne	.LBB6_50
.LBB6_51:                               # %middle.block398
                                        #   in Loop: Header=BB6_22 Depth=1
	cmpq	%r14, %r9
	movl	8(%rsp), %r11d          # 4-byte Reload
	movl	$-1024, %ebx            # imm = 0xFC00
	je	.LBB6_55
# BB#52:                                #   in Loop: Header=BB6_22 Depth=1
	subq	%r14, %rcx
	.p2align	4, 0x90
.LBB6_53:                               # %scalar.ph399.preheader
                                        #   in Loop: Header=BB6_22 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB6_54:                               # %scalar.ph399
                                        #   Parent Loop BB6_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rcx,4), %eax
	andl	-4(%r8,%rcx,4), %eax
	movl	%eax, -4(%rdi,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB6_54
.LBB6_55:                               # %.loopexit870
                                        #   in Loop: Header=BB6_22 Depth=1
	decl	12(%r15)
	movslq	(%r12), %rax
	leaq	(%rdx,%rax,4), %rdx
	cmpq	80(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB6_22
	jmp	.LBB6_96
.LBB6_16:                               # %.lr.ph271.split.preheader
	movq	%rax, 208(%rsp)         # 8-byte Spill
	leaq	-12(%r8), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	192(%rsp), %rcx         # 8-byte Reload
	leal	-1(%rcx,%rax), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB6_17:                               # %.lr.ph271.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_62 Depth 2
                                        #     Child Loop BB6_66 Depth 2
                                        #     Child Loop BB6_80 Depth 2
                                        #     Child Loop BB6_84 Depth 2
                                        #     Child Loop BB6_86 Depth 2
                                        #     Child Loop BB6_90 Depth 2
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi), %eax
	leal	1(%rax), %ecx
	imull	(%rsi), %eax
	movl	%ecx, 12(%rsi)
	movslq	%eax, %r9
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%r9,4), %rdi
	movl	12(%r15), %eax
	leal	1(%rax), %ecx
	imull	(%r15), %eax
	movl	%ecx, 12(%r15)
	movslq	%eax, %r11
	movl	(%r13), %esi
	movl	(%rbp,%r9,4), %eax
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	%ecx, %eax
	andl	$1023, %esi             # imm = 0x3FF
	movq	%rsi, %r15
	movl	$1, %ecx
	cmoveq	%rcx, %r15
	movl	%esi, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rbp,%r9,4)
	cmpq	$8, %r15
	jb	.LBB6_65
# BB#18:                                # %min.iters.checked343
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%r15, %r14
	andq	$1016, %r14             # imm = 0x3F8
	je	.LBB6_65
# BB#19:                                # %vector.memcheck375
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%rsi, %rax
	notq	%rax
	testl	%esi, %esi
	movq	$-2, %rcx
	cmovneq	%rcx, %rax
	movq	%r9, %rcx
	subq	%rax, %rcx
	movq	152(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rcx,4), %r10
	addq	%rsi, %r9
	movq	144(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9,4), %rbp
	shlq	$2, %rax
	movq	136(%rsp), %rbx         # 8-byte Reload
	subq	%rax, %rbx
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,4), %r8
	movq	%rdx, %rcx
	subq	%rax, %rcx
	addq	$-4, %rcx
	leaq	4(%rdx,%rsi,4), %r12
	cmpq	%r8, %r10
	sbbb	%al, %al
	cmpq	%rbp, %rbx
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r12, %r10
	sbbb	%al, %al
	cmpq	%rbp, %rcx
	sbbb	%cl, %cl
	testb	$1, %bl
	jne	.LBB6_20
# BB#56:                                # %vector.memcheck375
                                        #   in Loop: Header=BB6_17 Depth=1
	andb	%cl, %al
	andb	$1, %al
	jne	.LBB6_20
# BB#57:                                # %vector.body338.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	leaq	-8(%r14), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	btl	$3, %ecx
	movq	64(%rsp), %r12          # 8-byte Reload
	jb	.LBB6_58
# BB#59:                                # %vector.body338.prol
                                        #   in Loop: Header=BB6_17 Depth=1
	movups	-12(%r13,%rsi,4), %xmm0
	movups	-28(%r13,%rsi,4), %xmm1
	movups	-12(%rdx,%rsi,4), %xmm2
	movups	-28(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%rsi,4)
	movups	%xmm3, -28(%rdi,%rsi,4)
	movl	$8, %ecx
	jmp	.LBB6_60
.LBB6_20:                               #   in Loop: Header=BB6_17 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	jmp	.LBB6_65
.LBB6_58:                               #   in Loop: Header=BB6_17 Depth=1
	xorl	%ecx, %ecx
.LBB6_60:                               # %vector.body338.prol.loopexit
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
	testq	%rax, %rax
	je	.LBB6_63
# BB#61:                                # %vector.body338.preheader.new
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%r15, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rcx
	movq	208(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,4), %rax
	leaq	-12(%rdx,%rsi,4), %rbp
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r9,4), %rbx
	.p2align	4, 0x90
.LBB6_62:                               # %vector.body338
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rax,%rcx,4), %xmm0
	movups	-16(%rax,%rcx,4), %xmm1
	movups	(%rbp,%rcx,4), %xmm2
	movups	-16(%rbp,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rcx,4)
	movups	%xmm3, -28(%rbx,%rcx,4)
	movups	-32(%rax,%rcx,4), %xmm0
	movups	-48(%rax,%rcx,4), %xmm1
	movups	-32(%rbp,%rcx,4), %xmm2
	movups	-48(%rbp,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rbx,%rcx,4)
	movups	%xmm3, -60(%rbx,%rcx,4)
	addq	$-16, %rcx
	cmpq	%rcx, %r10
	jne	.LBB6_62
.LBB6_63:                               # %middle.block339
                                        #   in Loop: Header=BB6_17 Depth=1
	cmpq	%r14, %r15
	je	.LBB6_67
# BB#64:                                #   in Loop: Header=BB6_17 Depth=1
	subq	%r14, %rsi
	.p2align	4, 0x90
.LBB6_65:                               # %scalar.ph340.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB6_66:                               # %scalar.ph340
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rsi,4), %eax
	andl	-4(%r13,%rsi,4), %eax
	movl	%eax, -4(%rdi,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB6_66
.LBB6_67:                               # %.loopexit872
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%r11,4), %r15
	movl	(%r8), %r14d
	movl	(%rsi,%r11,4), %eax
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	%ecx, %eax
	andl	$1023, %r14d            # imm = 0x3FF
	movl	$1, %r10d
	cmovneq	%r14, %r10
	movl	%r14d, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rsi,%r11,4)
	cmpq	$8, %r10
	jae	.LBB6_69
# BB#68:                                #   in Loop: Header=BB6_17 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_83
	.p2align	4, 0x90
.LBB6_69:                               # %min.iters.checked
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%r10, %r9
	andq	$1016, %r9              # imm = 0x3F8
	je	.LBB6_70
# BB#71:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%r14, %rax
	notq	%rax
	testl	%r14d, %r14d
	movq	$-2, %rcx
	cmovneq	%rcx, %rax
	movq	%r11, %rcx
	subq	%rax, %rcx
	movq	184(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	addq	%r14, %r11
	movq	176(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%r11,4), %rsi
	shlq	$2, %rax
	movq	168(%rsp), %rbp         # 8-byte Reload
	subq	%rax, %rbp
	movq	160(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%r14,4), %r8
	movq	%rdx, %rbx
	subq	%rax, %rbx
	addq	$-4, %rbx
	leaq	4(%rdx,%r14,4), %r12
	cmpq	%r8, %rcx
	sbbb	%r8b, %r8b
	cmpq	%rsi, %rbp
	sbbb	%al, %al
	andb	%r8b, %al
	cmpq	%r12, %rcx
	sbbb	%cl, %cl
	cmpq	%rsi, %rbx
	sbbb	%bl, %bl
	testb	$1, %al
	jne	.LBB6_72
# BB#73:                                # %vector.memcheck
                                        #   in Loop: Header=BB6_17 Depth=1
	andb	%bl, %cl
	andb	$1, %cl
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB6_74
# BB#75:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	leaq	-8(%r9), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	btl	$3, %ecx
	movq	64(%rsp), %r12          # 8-byte Reload
	jb	.LBB6_76
# BB#77:                                # %vector.body.prol
                                        #   in Loop: Header=BB6_17 Depth=1
	movups	-12(%r8,%r14,4), %xmm0
	movups	-28(%r8,%r14,4), %xmm1
	movups	-12(%rdx,%r14,4), %xmm2
	movups	-28(%rdx,%r14,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r15,%r14,4)
	movups	%xmm3, -28(%r15,%r14,4)
	movl	$8, %esi
	testq	%rax, %rax
	jne	.LBB6_79
	jmp	.LBB6_81
.LBB6_70:                               #   in Loop: Header=BB6_17 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_83
.LBB6_74:                               #   in Loop: Header=BB6_17 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_83
.LBB6_72:                               #   in Loop: Header=BB6_17 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_83
.LBB6_76:                               #   in Loop: Header=BB6_17 Depth=1
	xorl	%esi, %esi
	testq	%rax, %rax
	je	.LBB6_81
.LBB6_79:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB6_17 Depth=1
	movq	%r10, %rcx
	andq	$-8, %rcx
	negq	%rcx
	negq	%rsi
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r14,4), %rax
	leaq	-12(%rdx,%r14,4), %rbp
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r11,4), %rbx
	.p2align	4, 0x90
.LBB6_80:                               # %vector.body
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rax,%rsi,4), %xmm0
	movups	-16(%rax,%rsi,4), %xmm1
	movups	(%rbp,%rsi,4), %xmm2
	movups	-16(%rbp,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rsi,4)
	movups	%xmm3, -28(%rbx,%rsi,4)
	movups	-32(%rax,%rsi,4), %xmm0
	movups	-48(%rax,%rsi,4), %xmm1
	movups	-32(%rbp,%rsi,4), %xmm2
	movups	-48(%rbp,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rbx,%rsi,4)
	movups	%xmm3, -60(%rbx,%rsi,4)
	addq	$-16, %rsi
	cmpq	%rsi, %rcx
	jne	.LBB6_80
.LBB6_81:                               # %middle.block
                                        #   in Loop: Header=BB6_17 Depth=1
	cmpq	%r9, %r10
	movl	8(%rsp), %r11d          # 4-byte Reload
	je	.LBB6_85
# BB#82:                                #   in Loop: Header=BB6_17 Depth=1
	subq	%r9, %r14
	.p2align	4, 0x90
.LBB6_83:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	incq	%r14
	.p2align	4, 0x90
.LBB6_84:                               # %scalar.ph
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%r14,4), %eax
	andl	-4(%r8,%r14,4), %eax
	movl	%eax, -4(%r15,%r14,4)
	decq	%r14
	cmpq	$1, %r14
	jg	.LBB6_84
.LBB6_85:                               # %.lr.ph263.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	movl	32(%rsp), %r9d          # 4-byte Reload
	movl	%r9d, %ecx
	.p2align	4, 0x90
.LBB6_86:                               # %.lr.ph263
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rdx,%rax,4), %ebp
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %ebx
	btl	%ebx, %ebp
	jae	.LBB6_88
# BB#87:                                #   in Loop: Header=BB6_86 Depth=2
	incq	%rax
	orl	%esi, (%rdi,%rax,4)
.LBB6_88:                               #   in Loop: Header=BB6_86 Depth=2
	cmpl	%r11d, %ecx
	jl	.LBB6_86
# BB#89:                                # %.lr.ph266.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	xorl	%eax, %eax
	movl	104(%rsp), %ecx         # 4-byte Reload
	movl	%r9d, %esi
	.p2align	4, 0x90
.LBB6_90:                               # %.lr.ph266
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movl	%esi, %edi
	sarl	$5, %edi
	movslq	%edi, %rdi
	movl	4(%rdx,%rdi,4), %edi
	btl	%esi, %edi
	jae	.LBB6_92
# BB#91:                                #   in Loop: Header=BB6_90 Depth=2
	movl	$1, %eax
	movl	$1, %edi
	shll	%cl, %edi
	movl	%ecx, %ebp
	sarl	$5, %ebp
	movslq	%ebp, %rbp
	orl	%edi, 4(%r15,%rbp,4)
.LBB6_92:                               #   in Loop: Header=BB6_90 Depth=2
	incl	%ecx
	cmpl	%r11d, %esi
	jl	.LBB6_90
# BB#93:                                # %._crit_edge267
                                        #   in Loop: Header=BB6_17 Depth=1
	testl	%eax, %eax
	movq	24(%rsp), %r15          # 8-byte Reload
	jne	.LBB6_95
# BB#94:                                #   in Loop: Header=BB6_17 Depth=1
	decl	12(%r15)
.LBB6_95:                               #   in Loop: Header=BB6_17 Depth=1
	movslq	(%r12), %rax
	leaq	(%rdx,%rax,4), %rdx
	cmpq	80(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB6_17
.LBB6_96:                               # %._crit_edge272
	movq	120(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB6_176
# BB#97:                                # %.lr.ph260
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	24(%rcx), %rdx
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %r9
	movq	16(%rax), %rax
	movq	24(%r9), %rcx
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	24(%rax), %rax
	leaq	-4(%rax), %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	4(%rax), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leaq	-4(%r13), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	4(%r13), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	-4(%rcx), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	4(%rcx), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	leaq	-4(%r8), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	4(%r8), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	cmpl	%r11d, 60(%rsp)         # 4-byte Folded Reload
	jle	.LBB6_98
# BB#103:                               # %.lr.ph260.split.us.preheader
	leaq	-12(%r8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	-12(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$-1024, %ebx            # imm = 0xFC00
	movq	%r9, 96(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB6_104:                              # %.lr.ph260.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_115 Depth 2
                                        #     Child Loop BB6_119 Depth 2
                                        #     Child Loop BB6_132 Depth 2
                                        #     Child Loop BB6_136 Depth 2
	movl	12(%r9), %eax
	leal	1(%rax), %ecx
	imull	(%r9), %eax
	movl	%ecx, 12(%r9)
	movslq	%eax, %rdi
	movq	48(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%rdi,4), %r14
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi), %eax
	leal	1(%rax), %ecx
	imull	(%rsi), %eax
	movl	%ecx, 12(%rsi)
	movslq	%eax, %r11
	movl	(%r8), %ecx
	movl	(%rbp,%rdi,4), %eax
	andl	%ebx, %eax
	andl	$1023, %ecx             # imm = 0x3FF
	movq	%rcx, %r12
	movl	$1, %esi
	cmoveq	%rsi, %r12
	movl	%ecx, %esi
	orl	%eax, %esi
	movl	%esi, (%rbp,%rdi,4)
	cmpq	$8, %r12
	jb	.LBB6_118
# BB#105:                               # %min.iters.checked697
                                        #   in Loop: Header=BB6_104 Depth=1
	movq	%r12, %r10
	andq	$1016, %r10             # imm = 0x3F8
	je	.LBB6_118
# BB#106:                               # %vector.memcheck729
                                        #   in Loop: Header=BB6_104 Depth=1
	movq	%rcx, %rax
	notq	%rax
	testl	%ecx, %ecx
	movq	$-2, %rsi
	cmovneq	%rsi, %rax
	movq	%rdi, %rsi
	subq	%rax, %rsi
	movq	160(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi,4), %r9
	addq	%rcx, %rdi
	movq	152(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,4), %rbx
	shlq	$2, %rax
	movq	144(%rsp), %rbp         # 8-byte Reload
	subq	%rax, %rbp
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx,4), %r15
	movq	%rdx, %r8
	subq	%rax, %r8
	addq	$-4, %r8
	leaq	4(%rdx,%rcx,4), %rax
	cmpq	%r15, %r9
	sbbb	%r15b, %r15b
	cmpq	%rbx, %rbp
	sbbb	%bpl, %bpl
	andb	%r15b, %bpl
	cmpq	%rax, %r9
	sbbb	%al, %al
	cmpq	%rbx, %r8
	sbbb	%bl, %bl
	testb	$1, %bpl
	jne	.LBB6_107
# BB#108:                               # %vector.memcheck729
                                        #   in Loop: Header=BB6_104 Depth=1
	andb	%bl, %al
	andb	$1, %al
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB6_109
# BB#110:                               # %vector.body692.preheader
                                        #   in Loop: Header=BB6_104 Depth=1
	leaq	-8(%r10), %rsi
	movq	%rsi, %rax
	shrq	$3, %rax
	btl	$3, %esi
	movq	96(%rsp), %r9           # 8-byte Reload
	jb	.LBB6_111
# BB#112:                               # %vector.body692.prol
                                        #   in Loop: Header=BB6_104 Depth=1
	movups	-12(%r8,%rcx,4), %xmm0
	movups	-28(%r8,%rcx,4), %xmm1
	movups	-12(%rdx,%rcx,4), %xmm2
	movups	-28(%rdx,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r14,%rcx,4)
	movups	%xmm3, -28(%r14,%rcx,4)
	movl	$8, %r15d
	testq	%rax, %rax
	jne	.LBB6_114
	jmp	.LBB6_116
.LBB6_107:                              #   in Loop: Header=BB6_104 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
.LBB6_109:                              #   in Loop: Header=BB6_104 Depth=1
	movq	96(%rsp), %r9           # 8-byte Reload
	movl	$-1024, %ebx            # imm = 0xFC00
	jmp	.LBB6_118
.LBB6_111:                              #   in Loop: Header=BB6_104 Depth=1
	xorl	%r15d, %r15d
	testq	%rax, %rax
	je	.LBB6_116
.LBB6_114:                              # %vector.body692.preheader.new
                                        #   in Loop: Header=BB6_104 Depth=1
	movq	%r12, %rax
	andq	$-8, %rax
	negq	%rax
	negq	%r15
	movq	24(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rsi
	leaq	-12(%rdx,%rcx,4), %rbp
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB6_115:                              # %vector.body692
                                        #   Parent Loop BB6_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rsi,%r15,4), %xmm0
	movups	-16(%rsi,%r15,4), %xmm1
	movups	(%rbp,%r15,4), %xmm2
	movups	-16(%rbp,%r15,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%r15,4)
	movups	%xmm3, -28(%rdi,%r15,4)
	movups	-32(%rsi,%r15,4), %xmm0
	movups	-48(%rsi,%r15,4), %xmm1
	movups	-32(%rbp,%r15,4), %xmm2
	movups	-48(%rbp,%r15,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rdi,%r15,4)
	movups	%xmm3, -60(%rdi,%r15,4)
	addq	$-16, %r15
	cmpq	%r15, %rax
	jne	.LBB6_115
.LBB6_116:                              # %middle.block693
                                        #   in Loop: Header=BB6_104 Depth=1
	cmpq	%r10, %r12
	movl	$-1024, %ebx            # imm = 0xFC00
	je	.LBB6_120
# BB#117:                               #   in Loop: Header=BB6_104 Depth=1
	subq	%r10, %rcx
	.p2align	4, 0x90
.LBB6_118:                              # %scalar.ph694.preheader
                                        #   in Loop: Header=BB6_104 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB6_119:                              # %scalar.ph694
                                        #   Parent Loop BB6_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rcx,4), %eax
	andl	-4(%r8,%rcx,4), %eax
	movl	%eax, -4(%r14,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB6_119
.LBB6_120:                              # %.loopexit
                                        #   in Loop: Header=BB6_104 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	leaq	(%rbp,%r11,4), %rcx
	movl	(%r13), %esi
	movl	(%rbp,%r11,4), %eax
	andl	%ebx, %eax
	andl	$1023, %esi             # imm = 0x3FF
	movl	$1, %r15d
	cmovneq	%rsi, %r15
	movl	%esi, %edi
	orl	%eax, %edi
	movl	%edi, (%rbp,%r11,4)
	cmpq	$8, %r15
	jae	.LBB6_122
# BB#121:                               #   in Loop: Header=BB6_104 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_135
	.p2align	4, 0x90
.LBB6_122:                              # %min.iters.checked638
                                        #   in Loop: Header=BB6_104 Depth=1
	movq	%r15, %r14
	andq	$1016, %r14             # imm = 0x3F8
	je	.LBB6_123
# BB#124:                               # %vector.memcheck670
                                        #   in Loop: Header=BB6_104 Depth=1
	movq	%rsi, %rax
	notq	%rax
	testl	%esi, %esi
	movq	$-2, %rdi
	cmovneq	%rdi, %rax
	movq	%r11, %rdi
	subq	%rax, %rdi
	movq	104(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rdi,4), %r10
	addq	%rsi, %r11
	movq	184(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r11,4), %rbp
	shlq	$2, %rax
	movq	176(%rsp), %rbx         # 8-byte Reload
	subq	%rax, %rbx
	movq	168(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rsi,4), %r8
	movq	%rdx, %rdi
	subq	%rax, %rdi
	addq	$-4, %rdi
	leaq	4(%rdx,%rsi,4), %r12
	cmpq	%r8, %r10
	sbbb	%al, %al
	cmpq	%rbp, %rbx
	sbbb	%bl, %bl
	andb	%al, %bl
	cmpq	%r12, %r10
	sbbb	%al, %al
	cmpq	%rbp, %rdi
	sbbb	%dil, %dil
	testb	$1, %bl
	jne	.LBB6_125
# BB#126:                               # %vector.memcheck670
                                        #   in Loop: Header=BB6_104 Depth=1
	andb	%dil, %al
	andb	$1, %al
	jne	.LBB6_125
# BB#127:                               # %vector.body633.preheader
                                        #   in Loop: Header=BB6_104 Depth=1
	leaq	-8(%r14), %rdi
	movq	%rdi, %rax
	shrq	$3, %rax
	btl	$3, %edi
	movq	16(%rsp), %r8           # 8-byte Reload
	jb	.LBB6_128
# BB#129:                               # %vector.body633.prol
                                        #   in Loop: Header=BB6_104 Depth=1
	movups	-12(%r13,%rsi,4), %xmm0
	movups	-28(%r13,%rsi,4), %xmm1
	movups	-12(%rdx,%rsi,4), %xmm2
	movups	-28(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rcx,%rsi,4)
	movups	%xmm3, -28(%rcx,%rsi,4)
	movl	$8, %edi
	testq	%rax, %rax
	jne	.LBB6_131
	jmp	.LBB6_133
.LBB6_123:                              #   in Loop: Header=BB6_104 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB6_135
.LBB6_125:                              #   in Loop: Header=BB6_104 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	8(%rsp), %r11d          # 4-byte Reload
	movl	$-1024, %ebx            # imm = 0xFC00
	jmp	.LBB6_135
.LBB6_128:                              #   in Loop: Header=BB6_104 Depth=1
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.LBB6_133
.LBB6_131:                              # %vector.body633.preheader.new
                                        #   in Loop: Header=BB6_104 Depth=1
	movq	%r15, %r10
	andq	$-8, %r10
	negq	%r10
	negq	%rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rsi,4), %rax
	leaq	-12(%rdx,%rsi,4), %rbp
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r11,4), %rbx
	.p2align	4, 0x90
.LBB6_132:                              # %vector.body633
                                        #   Parent Loop BB6_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rax,%rdi,4), %xmm0
	movups	-16(%rax,%rdi,4), %xmm1
	movups	(%rbp,%rdi,4), %xmm2
	movups	-16(%rbp,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rdi,4)
	movups	%xmm3, -28(%rbx,%rdi,4)
	movups	-32(%rax,%rdi,4), %xmm0
	movups	-48(%rax,%rdi,4), %xmm1
	movups	-32(%rbp,%rdi,4), %xmm2
	movups	-48(%rbp,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rbx,%rdi,4)
	movups	%xmm3, -60(%rbx,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %r10
	jne	.LBB6_132
.LBB6_133:                              # %middle.block634
                                        #   in Loop: Header=BB6_104 Depth=1
	cmpq	%r14, %r15
	movl	8(%rsp), %r11d          # 4-byte Reload
	movl	$-1024, %ebx            # imm = 0xFC00
	je	.LBB6_137
# BB#134:                               #   in Loop: Header=BB6_104 Depth=1
	subq	%r14, %rsi
	.p2align	4, 0x90
.LBB6_135:                              # %scalar.ph635.preheader
                                        #   in Loop: Header=BB6_104 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB6_136:                              # %scalar.ph635
                                        #   Parent Loop BB6_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rsi,4), %eax
	andl	-4(%r13,%rsi,4), %eax
	movl	%eax, -4(%rcx,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB6_136
.LBB6_137:                              # %.preheader242.us
                                        #   in Loop: Header=BB6_104 Depth=1
	decl	12(%r9)
	movq	120(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rdx,%rax,4), %rdx
	cmpq	80(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB6_104
	jmp	.LBB6_176
.LBB6_98:                               # %.lr.ph260.split.preheader
	leaq	-12(%r8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	-12(%r13), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	192(%rsp), %rcx         # 8-byte Reload
	leal	-1(%rcx,%rax), %r10d
	movq	88(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movl	%r10d, 32(%rsp)         # 4-byte Spill
	jmp	.LBB6_99
.LBB6_156:                              #   in Loop: Header=BB6_99 Depth=1
	xorl	%edi, %edi
	testq	%rax, %rax
	je	.LBB6_161
.LBB6_159:                              # %vector.body515.preheader.new
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	%r11, %rax
	andq	$-8, %rax
	negq	%rax
	negq	%rdi
	movq	208(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	leaq	-12(%rdx,%rsi,4), %rbp
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%r10,4), %rbx
	.p2align	4, 0x90
.LBB6_160:                              # %vector.body515
                                        #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%rdi,4), %xmm0
	movups	-16(%rcx,%rdi,4), %xmm1
	movups	(%rbp,%rdi,4), %xmm2
	movups	-16(%rbp,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rbx,%rdi,4)
	movups	%xmm3, -28(%rbx,%rdi,4)
	movups	-32(%rcx,%rdi,4), %xmm0
	movups	-48(%rcx,%rdi,4), %xmm1
	movups	-32(%rbp,%rdi,4), %xmm2
	movups	-48(%rbp,%rdi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rbx,%rdi,4)
	movups	%xmm3, -60(%rbx,%rdi,4)
	addq	$-16, %rdi
	cmpq	%rdi, %rax
	jne	.LBB6_160
.LBB6_161:                              # %middle.block516
                                        #   in Loop: Header=BB6_99 Depth=1
	cmpq	%r14, %r11
	movl	8(%rsp), %r11d          # 4-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
	je	.LBB6_165
# BB#162:                               #   in Loop: Header=BB6_99 Depth=1
	subq	%r14, %rsi
	jmp	.LBB6_163
	.p2align	4, 0x90
.LBB6_99:                               # %.lr.ph260.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_144 Depth 2
                                        #     Child Loop BB6_148 Depth 2
                                        #     Child Loop BB6_160 Depth 2
                                        #     Child Loop BB6_164 Depth 2
                                        #     Child Loop BB6_166 Depth 2
                                        #     Child Loop BB6_172 Depth 2
	movl	12(%r9), %eax
	leal	1(%rax), %ecx
	imull	(%r9), %eax
	movl	%ecx, 12(%r9)
	movslq	%eax, %rdi
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdi,4), %r12
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	12(%rsi), %eax
	leal	1(%rax), %ecx
	imull	(%rsi), %eax
	movl	%ecx, 12(%rsi)
	movslq	%eax, %r10
	movl	(%r8), %esi
	movl	(%rbx,%rdi,4), %eax
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	%ecx, %eax
	andl	$1023, %esi             # imm = 0x3FF
	movq	%rsi, %r11
	movl	$1, %ecx
	cmoveq	%rcx, %r11
	movl	%esi, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rbx,%rdi,4)
	cmpq	$8, %r11
	jb	.LBB6_147
# BB#100:                               # %min.iters.checked579
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	%r11, %r15
	andq	$1016, %r15             # imm = 0x3F8
	je	.LBB6_147
# BB#101:                               # %vector.memcheck611
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	%rsi, %rax
	notq	%rax
	testl	%esi, %esi
	movq	$-2, %rcx
	cmovneq	%rcx, %rax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	160(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rcx,4), %r14
	addq	%rsi, %rdi
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,4), %rbp
	shlq	$2, %rax
	movq	144(%rsp), %rbx         # 8-byte Reload
	subq	%rax, %rbx
	movq	136(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %r8
	movq	%rdx, %rcx
	subq	%rax, %rcx
	addq	$-4, %rcx
	leaq	4(%rdx,%rsi,4), %rax
	cmpq	%r8, %r14
	sbbb	%r8b, %r8b
	cmpq	%rbp, %rbx
	sbbb	%bl, %bl
	andb	%r8b, %bl
	cmpq	%rax, %r14
	sbbb	%al, %al
	cmpq	%rbp, %rcx
	sbbb	%cl, %cl
	testb	$1, %bl
	jne	.LBB6_102
# BB#138:                               # %vector.memcheck611
                                        #   in Loop: Header=BB6_99 Depth=1
	andb	%cl, %al
	andb	$1, %al
	movq	16(%rsp), %r8           # 8-byte Reload
	jne	.LBB6_147
# BB#139:                               # %vector.body574.preheader
                                        #   in Loop: Header=BB6_99 Depth=1
	leaq	-8(%r15), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	btl	$3, %ecx
	jb	.LBB6_140
# BB#141:                               # %vector.body574.prol
                                        #   in Loop: Header=BB6_99 Depth=1
	movups	-12(%r8,%rsi,4), %xmm0
	movups	-28(%r8,%rsi,4), %xmm1
	movups	-12(%rdx,%rsi,4), %xmm2
	movups	-28(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r12,%rsi,4)
	movups	%xmm3, -28(%r12,%rsi,4)
	movl	$8, %r14d
	testq	%rax, %rax
	jne	.LBB6_143
	jmp	.LBB6_145
.LBB6_102:                              #   in Loop: Header=BB6_99 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
	jmp	.LBB6_147
.LBB6_140:                              #   in Loop: Header=BB6_99 Depth=1
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.LBB6_145
.LBB6_143:                              # %vector.body574.preheader.new
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	%r11, %rax
	andq	$-8, %rax
	negq	%rax
	negq	%r14
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,4), %rcx
	leaq	-12(%rdx,%rsi,4), %rbp
	movq	48(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB6_144:                              # %vector.body574
                                        #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rcx,%r14,4), %xmm0
	movups	-16(%rcx,%r14,4), %xmm1
	movups	(%rbp,%r14,4), %xmm2
	movups	-16(%rbp,%r14,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rdi,%r14,4)
	movups	%xmm3, -28(%rdi,%r14,4)
	movups	-32(%rcx,%r14,4), %xmm0
	movups	-48(%rcx,%r14,4), %xmm1
	movups	-32(%rbp,%r14,4), %xmm2
	movups	-48(%rbp,%r14,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rdi,%r14,4)
	movups	%xmm3, -60(%rdi,%r14,4)
	addq	$-16, %r14
	cmpq	%r14, %rax
	jne	.LBB6_144
.LBB6_145:                              # %middle.block575
                                        #   in Loop: Header=BB6_99 Depth=1
	cmpq	%r15, %r11
	je	.LBB6_149
# BB#146:                               #   in Loop: Header=BB6_99 Depth=1
	subq	%r15, %rsi
	.p2align	4, 0x90
.LBB6_147:                              # %scalar.ph576.preheader
                                        #   in Loop: Header=BB6_99 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB6_148:                              # %scalar.ph576
                                        #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rsi,4), %eax
	andl	-4(%r8,%rsi,4), %eax
	movl	%eax, -4(%r12,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB6_148
.LBB6_149:                              # %.loopexit869
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%r10,4), %r15
	movl	(%r13), %esi
	movl	(%rdi,%r10,4), %eax
	movl	$-1024, %ecx            # imm = 0xFC00
	andl	%ecx, %eax
	andl	$1023, %esi             # imm = 0x3FF
	movl	$1, %r11d
	cmovneq	%rsi, %r11
	movl	%esi, %ecx
	orl	%eax, %ecx
	movl	%ecx, (%rdi,%r10,4)
	cmpq	$8, %r11
	jb	.LBB6_150
# BB#151:                               # %min.iters.checked520
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	%r11, %r14
	andq	$1016, %r14             # imm = 0x3F8
	je	.LBB6_150
# BB#152:                               # %vector.memcheck552
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	%rsi, %rax
	notq	%rax
	testl	%esi, %esi
	movq	$-2, %rcx
	cmovneq	%rcx, %rax
	movq	%r10, %rcx
	subq	%rax, %rcx
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rcx,4), %rcx
	addq	%rsi, %r10
	movq	184(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r10,4), %rdi
	shlq	$2, %rax
	movq	176(%rsp), %rbp         # 8-byte Reload
	subq	%rax, %rbp
	movq	168(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rsi,4), %r8
	movq	%rdx, %rbx
	subq	%rax, %rbx
	addq	$-4, %rbx
	leaq	4(%rdx,%rsi,4), %rax
	cmpq	%r8, %rcx
	sbbb	%r8b, %r8b
	cmpq	%rdi, %rbp
	sbbb	%bpl, %bpl
	andb	%r8b, %bpl
	cmpq	%rax, %rcx
	sbbb	%al, %al
	cmpq	%rdi, %rbx
	sbbb	%cl, %cl
	testb	$1, %bpl
	jne	.LBB6_153
# BB#154:                               # %vector.memcheck552
                                        #   in Loop: Header=BB6_99 Depth=1
	andb	%cl, %al
	andb	$1, %al
	jne	.LBB6_153
# BB#155:                               # %vector.body515.preheader
                                        #   in Loop: Header=BB6_99 Depth=1
	leaq	-8(%r14), %rcx
	movq	%rcx, %rax
	shrq	$3, %rax
	btl	$3, %ecx
	movq	16(%rsp), %r8           # 8-byte Reload
	jb	.LBB6_156
# BB#157:                               # %vector.body515.prol
                                        #   in Loop: Header=BB6_99 Depth=1
	movups	-12(%r13,%rsi,4), %xmm0
	movups	-28(%r13,%rsi,4), %xmm1
	movups	-12(%rdx,%rsi,4), %xmm2
	movups	-28(%rdx,%rsi,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r15,%rsi,4)
	movups	%xmm3, -28(%r15,%rsi,4)
	movl	$8, %edi
	testq	%rax, %rax
	jne	.LBB6_159
	jmp	.LBB6_161
.LBB6_153:                              #   in Loop: Header=BB6_99 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_150:                              #   in Loop: Header=BB6_99 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
.LBB6_163:                              # %scalar.ph517.preheader
                                        #   in Loop: Header=BB6_99 Depth=1
	incq	%rsi
	.p2align	4, 0x90
.LBB6_164:                              # %scalar.ph517
                                        #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdx,%rsi,4), %eax
	andl	-4(%r13,%rsi,4), %eax
	movl	%eax, -4(%r15,%rsi,4)
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB6_164
.LBB6_165:                              # %.lr.ph253.preheader
                                        #   in Loop: Header=BB6_99 Depth=1
	xorl	%eax, %eax
	movl	24(%rsp), %ecx          # 4-byte Reload
	movl	%r10d, %esi
	.p2align	4, 0x90
.LBB6_166:                              # %.lr.ph253
                                        #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movl	%esi, %edi
	sarl	$5, %edi
	movslq	%edi, %rdi
	movl	4(%rdx,%rdi,4), %edi
	btl	%esi, %edi
	jae	.LBB6_168
# BB#167:                               #   in Loop: Header=BB6_166 Depth=2
	movl	$1, %eax
	movl	$1, %edi
	shll	%cl, %edi
	movl	%ecx, %ebp
	sarl	$5, %ebp
	movslq	%ebp, %rbp
	orl	%edi, 4(%r12,%rbp,4)
.LBB6_168:                              #   in Loop: Header=BB6_166 Depth=2
	incl	%ecx
	cmpl	%r11d, %esi
	jl	.LBB6_166
# BB#169:                               # %._crit_edge254
                                        #   in Loop: Header=BB6_99 Depth=1
	testl	%eax, %eax
	jne	.LBB6_171
# BB#170:                               #   in Loop: Header=BB6_99 Depth=1
	decl	12(%r9)
.LBB6_171:                              # %.lr.ph256.preheader
                                        #   in Loop: Header=BB6_99 Depth=1
	movl	%r10d, %ecx
	.p2align	4, 0x90
.LBB6_172:                              # %.lr.ph256
                                        #   Parent Loop BB6_99 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%ecx
	movl	%ecx, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rdx,%rax,4), %edi
	movl	$1, %esi
	shll	%cl, %esi
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %ebp
	btl	%ebp, %edi
	jae	.LBB6_174
# BB#173:                               #   in Loop: Header=BB6_172 Depth=2
	incq	%rax
	orl	%esi, (%r15,%rax,4)
.LBB6_174:                              #   in Loop: Header=BB6_172 Depth=2
	cmpl	%r11d, %ecx
	jl	.LBB6_172
# BB#175:                               # %._crit_edge257
                                        #   in Loop: Header=BB6_99 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rdx,%rax,4), %rdx
	cmpq	80(%rsp), %rdx          # 8-byte Folded Reload
	jb	.LBB6_99
.LBB6_176:                              # %._crit_edge261
	movq	112(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	jle	.LBB6_216
# BB#177:                               # %.lr.ph249
	movq	112(%rsp), %rcx         # 8-byte Reload
	movq	24(%rcx), %rdi
	leaq	(%rdi,%rax,4), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	24(%rax), %r9
	leaq	-4(%r9), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	4(%r9), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	-4(%r13), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	4(%r13), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpl	%r11d, 60(%rsp)         # 4-byte Folded Reload
	jle	.LBB6_178
# BB#183:                               # %.lr.ph249.split.us.preheader
	leaq	-12(%r13), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$-1024, %r8d            # imm = 0xFC00
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB6_184:                              # %.lr.ph249.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_194 Depth 2
                                        #     Child Loop BB6_198 Depth 2
	movq	216(%rsp), %rdx         # 8-byte Reload
	movl	12(%rdx), %eax
	leal	1(%rax), %ecx
	imull	(%rdx), %eax
	movl	%ecx, 12(%rdx)
	movslq	%eax, %rsi
	leaq	(%r9,%rsi,4), %rcx
	movl	(%r13), %ebp
	movl	(%r9,%rsi,4), %eax
	andl	%r8d, %eax
	andl	$1023, %ebp             # imm = 0x3FF
	movq	%rbp, %r14
	cmoveq	%rbx, %r14
	movl	%ebp, %edx
	orl	%eax, %edx
	movl	%edx, (%r9,%rsi,4)
	cmpq	$8, %r14
	jb	.LBB6_197
# BB#185:                               # %min.iters.checked815
                                        #   in Loop: Header=BB6_184 Depth=1
	movq	%r14, %rax
	andq	$1016, %rax             # imm = 0x3F8
	je	.LBB6_197
# BB#186:                               # %vector.memcheck847
                                        #   in Loop: Header=BB6_184 Depth=1
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rax
	notq	%rax
	testl	%ebp, %ebp
	movq	$-2, %rdx
	cmovneq	%rdx, %rax
	movq	%rsi, %rdx
	subq	%rax, %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rdx,4), %r10
	addq	%rbp, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rsi,4), %r11
	shlq	$2, %rax
	movq	72(%rsp), %rdx          # 8-byte Reload
	subq	%rax, %rdx
	movq	24(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rbp,4), %r15
	movq	%rdi, %r12
	subq	%rax, %r12
	addq	$-4, %r12
	leaq	4(%rdi,%rbp,4), %rax
	cmpq	%r15, %r10
	sbbb	%r15b, %r15b
	cmpq	%r11, %rdx
	sbbb	%r8b, %r8b
	andb	%r15b, %r8b
	cmpq	%rax, %r10
	sbbb	%al, %al
	cmpq	%r11, %r12
	sbbb	%dl, %dl
	testb	$1, %r8b
	jne	.LBB6_187
# BB#188:                               # %vector.memcheck847
                                        #   in Loop: Header=BB6_184 Depth=1
	andb	%dl, %al
	andb	$1, %al
	jne	.LBB6_187
# BB#189:                               # %vector.body810.preheader
                                        #   in Loop: Header=BB6_184 Depth=1
	movq	8(%rsp), %r15           # 8-byte Reload
	leaq	-8(%r15), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	movl	$-1024, %r8d            # imm = 0xFC00
	movl	$1, %ebx
	jb	.LBB6_190
# BB#191:                               # %vector.body810.prol
                                        #   in Loop: Header=BB6_184 Depth=1
	movups	-12(%r13,%rbp,4), %xmm0
	movups	-28(%r13,%rbp,4), %xmm1
	movups	-12(%rdi,%rbp,4), %xmm2
	movups	-28(%rdi,%rbp,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rcx,%rbp,4)
	movups	%xmm3, -28(%rcx,%rbp,4)
	movl	$8, %r10d
	testq	%rax, %rax
	jne	.LBB6_193
	jmp	.LBB6_195
.LBB6_187:                              #   in Loop: Header=BB6_184 Depth=1
	movl	$-1024, %r8d            # imm = 0xFC00
	movl	$1, %ebx
	jmp	.LBB6_197
.LBB6_190:                              #   in Loop: Header=BB6_184 Depth=1
	xorl	%r10d, %r10d
	testq	%rax, %rax
	je	.LBB6_195
.LBB6_193:                              # %vector.body810.preheader.new
                                        #   in Loop: Header=BB6_184 Depth=1
	movq	%r14, %rax
	andq	$-8, %rax
	negq	%rax
	negq	%r10
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbp,4), %r11
	leaq	-12(%rdi,%rbp,4), %rdx
	leaq	(%r9,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB6_194:                              # %vector.body810
                                        #   Parent Loop BB6_184 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r11,%r10,4), %xmm0
	movups	-16(%r11,%r10,4), %xmm1
	movups	(%rdx,%r10,4), %xmm2
	movups	-16(%rdx,%r10,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rsi,%r10,4)
	movups	%xmm3, -28(%rsi,%r10,4)
	movups	-32(%r11,%r10,4), %xmm0
	movups	-48(%r11,%r10,4), %xmm1
	movups	-32(%rdx,%r10,4), %xmm2
	movups	-48(%rdx,%r10,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rsi,%r10,4)
	movups	%xmm3, -60(%rsi,%r10,4)
	addq	$-16, %r10
	cmpq	%r10, %rax
	jne	.LBB6_194
.LBB6_195:                              # %middle.block811
                                        #   in Loop: Header=BB6_184 Depth=1
	cmpq	%r15, %r14
	je	.LBB6_199
# BB#196:                               #   in Loop: Header=BB6_184 Depth=1
	subq	%r15, %rbp
	.p2align	4, 0x90
.LBB6_197:                              # %scalar.ph812.preheader
                                        #   in Loop: Header=BB6_184 Depth=1
	incq	%rbp
	.p2align	4, 0x90
.LBB6_198:                              # %scalar.ph812
                                        #   Parent Loop BB6_184 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdi,%rbp,4), %eax
	andl	-4(%r13,%rbp,4), %eax
	movl	%eax, -4(%rcx,%rbp,4)
	decq	%rbp
	cmpq	$1, %rbp
	jg	.LBB6_198
.LBB6_199:                              # %.preheader.us
                                        #   in Loop: Header=BB6_184 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rdi,%rax,4), %rdi
	cmpq	48(%rsp), %rdi          # 8-byte Folded Reload
	jb	.LBB6_184
	jmp	.LBB6_216
.LBB6_178:                              # %.lr.ph249.split.preheader
	leaq	-12(%r13), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	192(%rsp), %rcx         # 8-byte Reload
	leal	-1(%rcx,%rax), %r12d
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	%ecx, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	$-1024, %r14d           # imm = 0xFC00
	.p2align	4, 0x90
.LBB6_179:                              # %.lr.ph249.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_206 Depth 2
                                        #     Child Loop BB6_210 Depth 2
                                        #     Child Loop BB6_212 Depth 2
	movq	216(%rsp), %rdx         # 8-byte Reload
	movl	12(%rdx), %eax
	leal	1(%rax), %ecx
	imull	(%rdx), %eax
	movl	%ecx, 12(%rdx)
	movslq	%eax, %rbx
	leaq	(%r9,%rbx,4), %r15
	movl	(%r13), %ecx
	movl	(%r9,%rbx,4), %eax
	andl	%r14d, %eax
	andl	$1023, %ecx             # imm = 0x3FF
	movq	%rcx, %r10
	movl	$1, %edx
	cmoveq	%rdx, %r10
	movl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, (%r9,%rbx,4)
	cmpq	$8, %r10
	jb	.LBB6_209
# BB#180:                               # %min.iters.checked756
                                        #   in Loop: Header=BB6_179 Depth=1
	movq	%r10, %r8
	andq	$1016, %r8              # imm = 0x3F8
	je	.LBB6_209
# BB#181:                               # %vector.memcheck788
                                        #   in Loop: Header=BB6_179 Depth=1
	movq	%rcx, %rax
	notq	%rax
	testl	%ecx, %ecx
	movq	$-2, %rdx
	cmovneq	%rdx, %rax
	movq	%rbx, %rsi
	subq	%rax, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rsi,4), %r14
	addq	%rcx, %rbx
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rbx,4), %rsi
	shlq	$2, %rax
	movq	72(%rsp), %rbp          # 8-byte Reload
	subq	%rax, %rbp
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %r11
	movq	%rdi, %rdx
	subq	%rax, %rdx
	addq	$-4, %rdx
	leaq	4(%rdi,%rcx,4), %rax
	cmpq	%r11, %r14
	sbbb	%r11b, %r11b
	cmpq	%rsi, %rbp
	sbbb	%bpl, %bpl
	andb	%r11b, %bpl
	cmpq	%rax, %r14
	sbbb	%al, %al
	cmpq	%rsi, %rdx
	sbbb	%dl, %dl
	testb	$1, %bpl
	jne	.LBB6_182
# BB#200:                               # %vector.memcheck788
                                        #   in Loop: Header=BB6_179 Depth=1
	andb	%dl, %al
	andb	$1, %al
	jne	.LBB6_182
# BB#201:                               # %vector.body751.preheader
                                        #   in Loop: Header=BB6_179 Depth=1
	leaq	-8(%r8), %rdx
	movq	%rdx, %rax
	shrq	$3, %rax
	btl	$3, %edx
	jb	.LBB6_202
# BB#203:                               # %vector.body751.prol
                                        #   in Loop: Header=BB6_179 Depth=1
	movups	-12(%r13,%rcx,4), %xmm0
	movups	-28(%r13,%rcx,4), %xmm1
	movups	-12(%rdi,%rcx,4), %xmm2
	movups	-28(%rdi,%rcx,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%r15,%rcx,4)
	movups	%xmm3, -28(%r15,%rcx,4)
	movl	$8, %r14d
	testq	%rax, %rax
	jne	.LBB6_205
	jmp	.LBB6_207
.LBB6_182:                              #   in Loop: Header=BB6_179 Depth=1
	movl	8(%rsp), %r11d          # 4-byte Reload
	movl	$-1024, %r14d           # imm = 0xFC00
	jmp	.LBB6_209
.LBB6_202:                              #   in Loop: Header=BB6_179 Depth=1
	xorl	%r14d, %r14d
	testq	%rax, %rax
	je	.LBB6_207
.LBB6_205:                              # %vector.body751.preheader.new
                                        #   in Loop: Header=BB6_179 Depth=1
	movq	%r10, %rax
	andq	$-8, %rax
	negq	%rax
	negq	%r14
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,4), %r11
	leaq	-12(%rdi,%rcx,4), %rbp
	leaq	(%r9,%rbx,4), %rsi
	.p2align	4, 0x90
.LBB6_206:                              # %vector.body751
                                        #   Parent Loop BB6_179 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r11,%r14,4), %xmm0
	movups	-16(%r11,%r14,4), %xmm1
	movups	(%rbp,%r14,4), %xmm2
	movups	-16(%rbp,%r14,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -12(%rsi,%r14,4)
	movups	%xmm3, -28(%rsi,%r14,4)
	movups	-32(%r11,%r14,4), %xmm0
	movups	-48(%r11,%r14,4), %xmm1
	movups	-32(%rbp,%r14,4), %xmm2
	movups	-48(%rbp,%r14,4), %xmm3
	andps	%xmm0, %xmm2
	andps	%xmm1, %xmm3
	movups	%xmm2, -44(%rsi,%r14,4)
	movups	%xmm3, -60(%rsi,%r14,4)
	addq	$-16, %r14
	cmpq	%r14, %rax
	jne	.LBB6_206
.LBB6_207:                              # %middle.block752
                                        #   in Loop: Header=BB6_179 Depth=1
	cmpq	%r8, %r10
	movl	8(%rsp), %r11d          # 4-byte Reload
	movl	$-1024, %r14d           # imm = 0xFC00
	je	.LBB6_211
# BB#208:                               #   in Loop: Header=BB6_179 Depth=1
	subq	%r8, %rcx
	.p2align	4, 0x90
.LBB6_209:                              # %scalar.ph753.preheader
                                        #   in Loop: Header=BB6_179 Depth=1
	incq	%rcx
	.p2align	4, 0x90
.LBB6_210:                              # %scalar.ph753
                                        #   Parent Loop BB6_179 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-4(%rdi,%rcx,4), %eax
	andl	-4(%r13,%rcx,4), %eax
	movl	%eax, -4(%r15,%rcx,4)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB6_210
.LBB6_211:                              # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_179 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	movl	%r12d, %esi
	.p2align	4, 0x90
.LBB6_212:                              # %.lr.ph
                                        #   Parent Loop BB6_179 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%esi
	movl	%esi, %eax
	sarl	$5, %eax
	cltq
	movl	4(%rdi,%rax,4), %ebx
	movl	$1, %ebp
	movl	%esi, %ecx
	shll	%cl, %ebp
	andb	$31, %cl
	movzbl	%cl, %ecx
	btl	%ecx, %ebx
	jae	.LBB6_214
# BB#213:                               #   in Loop: Header=BB6_212 Depth=2
	incq	%rax
	orl	%ebp, (%r15,%rax,4)
	movl	$1, %eax
	movl	%edx, %ecx
	shll	%cl, %eax
	movl	%edx, %ecx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%eax, 4(%r15,%rcx,4)
.LBB6_214:                              #   in Loop: Header=BB6_212 Depth=2
	incl	%edx
	cmpl	%r11d, %esi
	jl	.LBB6_212
# BB#215:                               # %._crit_edge
                                        #   in Loop: Header=BB6_179 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rdi,%rax,4), %rdi
	cmpq	48(%rsp), %rdi          # 8-byte Folded Reload
	jb	.LBB6_179
.LBB6_216:                              # %._crit_edge250
	xorl	%eax, %eax
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	sf_free
	xorl	%eax, %eax
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	sf_free
	xorl	%eax, %eax
	movq	120(%rsp), %rdi         # 8-byte Reload
	callq	sf_free
	testq	%r13, %r13
	je	.LBB6_218
# BB#217:
	movq	%r13, %rdi
	callq	free
.LBB6_218:
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB6_220
# BB#219:
	callq	free
.LBB6_220:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	output_phase_setup, .Lfunc_end6-output_phase_setup
	.cfi_endproc

	.globl	set_phase
	.p2align	4, 0x90
	.type	set_phase,@function
set_phase:                              # @set_phase
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 80
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	cube+80(%rip), %rax
	movq	(%rax), %rbx
	movq	8(%rax), %rbp
	movq	40(%r15), %rdx
	movq	cube+72(%rip), %rax
	movslq	cube+4(%rip), %rcx
	movq	-8(%rax,%rcx,8), %r13
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	set_diff
	movq	cube+88(%rip), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	set_diff
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rsi
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdx
	callq	set_or
	movq	(%r15), %rax
	movq	16(%r15), %rcx
	movl	12(%rcx), %edi
	addl	12(%rax), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r14
	movq	(%r15), %rax
	movq	16(%r15), %rcx
	movl	12(%rcx), %edi
	addl	12(%rax), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%r15, %rsi
	movq	%rax, %r12
	movq	(%rsi), %rdi
	movslq	(%rdi), %rcx
	movslq	12(%rdi), %rax
	imulq	%rcx, %rax
	testl	%eax, %eax
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	jle	.LBB7_7
# BB#1:                                 # %.lr.ph85
	movq	24(%rdi), %r15
	leaq	(%r15,%rax,4), %rbp
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	set_and
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	setp_disjoint
	testl	%eax, %eax
	jne	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	24(%r14), %rax
	movl	12(%r14), %ecx
	leal	1(%rcx), %edx
	imull	(%r14), %ecx
	movl	%edx, 12(%r14)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	set_copy
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	set_and
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	setp_disjoint
	testl	%eax, %eax
	jne	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	24(%r12), %rax
	movl	12(%r12), %ecx
	leal	1(%rcx), %edx
	imull	(%r12), %ecx
	movl	%edx, 12(%r12)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	set_copy
.LBB7_6:                                #   in Loop: Header=BB7_2 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rdi
	movslq	(%rdi), %rax
	leaq	(%r15,%rax,4), %r15
	cmpq	%rbp, %r15
	jb	.LBB7_2
.LBB7_7:                                # %._crit_edge86
	movq	16(%rsi), %rax
	movslq	(%rax), %rdx
	movslq	12(%rax), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB7_15
# BB#8:                                 # %.lr.ph
	movq	24(%rax), %rbp
	leaq	(%rbp,%rcx,4), %r15
	.p2align	4, 0x90
.LBB7_9:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	set_and
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	setp_disjoint
	testl	%eax, %eax
	jne	.LBB7_11
# BB#10:                                #   in Loop: Header=BB7_9 Depth=1
	movq	24(%r12), %rax
	movl	12(%r12), %ecx
	leal	1(%rcx), %edx
	imull	(%r12), %ecx
	movl	%edx, 12(%r12)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	set_copy
.LBB7_11:                               #   in Loop: Header=BB7_9 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	set_and
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r13, %rsi
	callq	setp_disjoint
	testl	%eax, %eax
	jne	.LBB7_13
# BB#12:                                #   in Loop: Header=BB7_9 Depth=1
	movq	24(%r14), %rax
	movl	12(%r14), %ecx
	leal	1(%rcx), %edx
	imull	(%r14), %ecx
	movl	%edx, 12(%r14)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	set_copy
.LBB7_13:                               #   in Loop: Header=BB7_9 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rax
	movslq	(%rax), %rax
	leaq	(%rbp,%rax,4), %rbp
	cmpq	%r15, %rbp
	jb	.LBB7_9
# BB#14:                                # %._crit_edge.loopexit
	movq	(%rsi), %rdi
.LBB7_15:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%rsi, %rbx
	callq	sf_free
	movq	16(%rbx), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	%r14, (%rbx)
	movq	%r12, 16(%rbx)
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	set_phase, .Lfunc_end7-set_phase
	.cfi_endproc

	.globl	opoall
	.p2align	4, 0x90
	.type	opoall,@function
opoall:                                 # @opoall
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi96:
	.cfi_def_cfa_offset 128
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%esi, %ebp
	movq	%rdi, %r15
	movl	%ecx, opo_exact(%rip)
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_2
# BB#1:
	callq	free
	movq	$0, 40(%r15)
.LBB8_2:
	movq	cube+88(%rip), %r14
	movl	(%r14), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	cmpl	$33, %eax
	jae	.LBB8_4
# BB#3:
	movl	$8, %edi
	jmp	.LBB8_5
.LBB8_4:
	decl	%eax
	shrl	$5, %eax
	leal	8(,%rax,4), %edi
.LBB8_5:
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%r14, %rsi
	callq	set_copy
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %r12
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$1, %eax
	movl	$1, %ecx
	subl	%ebp, %ecx
	addl	12(%rsp), %ecx          # 4-byte Folded Reload
	shll	%cl, %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	cmpl	$31, %ecx
	jne	.LBB8_7
# BB#6:                                 # %.._crit_edge99_crit_edge
	movq	(%r15), %rbx
	jmp	.LBB8_27
.LBB8_7:                                # %.lr.ph98
	movq	(%r15), %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_12 Depth 2
	movq	%r12, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_save
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %r13
	movq	16(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	cube+88(%rip), %rbx
	movl	(%rbx), %eax
	shll	$5, %eax
	andl	$32736, %eax            # imm = 0x7FE0
	movl	$2, %ecx
	cmpl	$33, %eax
	jb	.LBB8_10
# BB#9:                                 #   in Loop: Header=BB8_8 Depth=1
	decl	%eax
	shrl	$5, %eax
	addl	$2, %eax
	movl	%eax, %ecx
.LBB8_10:                               #   in Loop: Header=BB8_8 Depth=1
	movl	%ecx, %edi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movq	%rbx, %rsi
	callq	set_copy
	cmpl	%ebp, 12(%rsp)          # 4-byte Folded Reload
	movq	%rax, 40(%r15)
	jl	.LBB8_15
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	cube+16(%rip), %rdx
	movl	%r14d, %edi
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %esi
	.p2align	4, 0x90
.LBB8_12:                               #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$1, %dil
	jne	.LBB8_14
# BB#13:                                #   in Loop: Header=BB8_12 Depth=2
	movslq	cube+124(%rip), %rcx
	movl	(%rdx,%rcx,4), %ecx
	addl	%esi, %ecx
	movl	$-2, %ebx
	roll	%cl, %ebx
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	andl	%ebx, 4(%rax,%rcx,4)
.LBB8_14:                               #   in Loop: Header=BB8_12 Depth=2
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	sarl	%ecx
	cmpl	%ebp, %esi
	leal	-1(%rsi), %esi
	movl	%ecx, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	jg	.LBB8_12
.LBB8_15:                               # %._crit_edge
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	%r15, %rdi
	callq	set_phase
	movq	40(%r15), %rdi
	xorl	%eax, %eax
	callq	pc1
	movq	%rax, %rcx
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	printf
	movl	$1, summary(%rip)
	movl	opo_exact(%rip), %ebx
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r12
	cmpl	$0, %ebx
	movq	(%r15), %rdi
	movq	8(%r15), %rsi
	movq	16(%r15), %rdx
	je	.LBB8_18
# BB#16:                                #   in Loop: Header=BB8_8 Depth=1
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	minimize_exact
	movq	%rax, %rbx
	movq	%rbx, (%r15)
	cmpl	$0, summary(%rip)
	je	.LBB8_21
# BB#17:                                #   in Loop: Header=BB8_8 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r12, %rcx
	movl	$.L.str.9, %esi
	jmp	.LBB8_20
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_8 Depth=1
	xorl	%eax, %eax
	callq	espresso
	movq	%rax, %rbx
	movq	%rbx, (%r15)
	cmpl	$0, summary(%rip)
	je	.LBB8_21
# BB#19:                                #   in Loop: Header=BB8_8 Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r12, %rcx
	movl	$.L.str.10, %esi
.LBB8_20:                               # %minimize.exit
                                        #   in Loop: Header=BB8_8 Depth=1
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB8_21:                               # %minimize.exit
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	(%r15), %rdi
	movl	12(%rdi), %eax
	movq	64(%rsp), %r12          # 8-byte Reload
	cmpl	12(%r12), %eax
	jge	.LBB8_23
# BB#22:                                #   in Loop: Header=BB8_8 Depth=1
	movq	40(%r15), %rsi
	xorl	%eax, %eax
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	set_copy
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	sf_free
	xorl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	sf_free
	movq	(%r15), %r12
	movq	8(%r15), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	16(%r15), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	jmp	.LBB8_24
	.p2align	4, 0x90
.LBB8_23:                               #   in Loop: Header=BB8_8 Depth=1
	xorl	%eax, %eax
	callq	sf_free
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	16(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_free
.LBB8_24:                               #   in Loop: Header=BB8_8 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	40(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB8_26
# BB#25:                                #   in Loop: Header=BB8_8 Depth=1
	callq	free
	movq	$0, 40(%r15)
.LBB8_26:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%rbx, (%r15)
	movq	%r13, 8(%r15)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r15)
	incl	%r14d
	cmpl	36(%rsp), %r14d         # 4-byte Folded Reload
	jl	.LBB8_8
.LBB8_27:                               # %._crit_edge99
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, 40(%r15)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sf_free
	movq	8(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	16(%r15), %rdi
	xorl	%eax, %eax
	callq	sf_free
	movq	%r12, (%r15)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%r15)
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%r15)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	opoall, .Lfunc_end8-opoall
	.cfi_endproc

	.type	opo_repeated,@object    # @opo_repeated
	.local	opo_repeated
	.comm	opo_repeated,4,4
	.type	opo_exact,@object       # @opo_exact
	.local	opo_exact
	.comm	opo_exact,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nOPO loop for output #%d\n"
	.size	.L.str, 26

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"PLA->phase is %s\n"
	.size	.L.str.1, 18

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"phase      is %s\n"
	.size	.L.str.2, 18

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"OPO-SETUP "
	.size	.L.str.3, 11

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"OPO       "
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"error in output phase assignment"
	.size	.L.str.5, 33

	.type	opo_recur.level,@object # @opo_recur.level
	.local	opo_recur.level
	.comm	opo_recur.level,4,4
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"# OPO[%d]: %4d = %4d x %4d, time = %s\n"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"output_phase_setup: must have an output"
	.size	.L.str.7, 40

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"# phase is %s\n"
	.size	.L.str.8, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"EXACT"
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"ESPRESSO  "
	.size	.L.str.10, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
