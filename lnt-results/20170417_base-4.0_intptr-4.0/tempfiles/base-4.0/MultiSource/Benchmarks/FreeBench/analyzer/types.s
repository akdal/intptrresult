	.text
	.file	"types.bc"
	.globl	hard_raw_mod
	.p2align	4, 0x90
	.type	hard_raw_mod,@function
hard_raw_mod:                           # @hard_raw_mod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, %r15d
	movq	%rcx, %r12
	movl	%edx, %r13d
	movl	%esi, %ebx
	movq	%rdi, %rbp
	movb	hard_raw_mod.hr_empty(%rip), %r14b
	movl	$56, %edi
	callq	malloc
	testb	%r14b, %r14b
	je	.LBB0_1
# BB#3:
	movq	hard_raw_mod.last(%rip), %rcx
	movq	%rax, (%rcx)
	movq	%rcx, 8(%rax)
	movq	%rax, hard_raw_mod.last(%rip)
	testq	%rax, %rax
	je	.LBB0_6
# BB#4:
	movq	$0, (%rax)
	movq	hard_raw_mod.last(%rip), %rax
	movq	%rbp, 16(%rax)
	movl	%ebx, 24(%rax)
	movl	%r13d, 28(%rax)
	movq	%r12, 32(%rax)
	movl	%r15d, 40(%rax)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 44(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 48(%rax)
	jmp	.LBB0_5
.LBB0_1:
	movq	%rax, hard_raw_list(%rip)
	testq	%rax, %rax
	je	.LBB0_6
# BB#2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rbp, 16(%rax)
	movl	%ebx, 24(%rax)
	movl	%r13d, 28(%rax)
	movq	%r12, 32(%rax)
	movl	%r15d, 40(%rax)
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%ecx, 44(%rax)
	movq	64(%rsp), %rcx
	movq	%rcx, 48(%rax)
	movb	$1, hard_raw_mod.hr_empty(%rip)
	movq	%rax, hard_raw_mod.last(%rip)
.LBB0_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_6:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	hard_raw_mod, .Lfunc_end0-hard_raw_mod
	.cfi_endproc

	.globl	def_hash
	.p2align	4, 0x90
	.type	def_hash,@function
def_hash:                               # @def_hash
	.cfi_startproc
# BB#0:
	movslq	def_table_size(%rip), %rcx
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	movq	%rdx, %rax
	retq
.Lfunc_end1:
	.size	def_hash, .Lfunc_end1-def_hash
	.cfi_endproc

	.globl	def_list_lookup
	.p2align	4, 0x90
	.type	def_list_lookup,@function
def_list_lookup:                        # @def_list_lookup
	.cfi_startproc
# BB#0:
	movslq	def_table_size(%rip), %rcx
	xorl	%edx, %edx
	movq	%rdi, %rax
	divq	%rcx
	movq	def_table(%rip), %rax
	movq	(%rax,%rdx,8), %rax
	testq	%rax, %rax
	jne	.LBB2_2
	jmp	.LBB2_4
	.p2align	4, 0x90
.LBB2_3:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB2_4
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rdi, 8(%rax)
	jne	.LBB2_3
	jmp	.LBB2_5
.LBB2_4:
	xorl	%eax, %eax
.LBB2_5:                                # %._crit_edge
	retq
.Lfunc_end2:
	.size	def_list_lookup, .Lfunc_end2-def_list_lookup
	.cfi_endproc

	.globl	def_list_mod
	.p2align	4, 0x90
	.type	def_list_mod,@function
def_list_mod:                           # @def_list_mod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movslq	def_table_size(%rip), %rcx
	xorl	%edx, %edx
	movq	%rbx, %rax
	divq	%rcx
	movq	%rdx, %r12
	movq	def_table(%rip), %rbp
	movq	(%rbp,%r12,8), %rax
	testq	%rax, %rax
	je	.LBB3_1
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbp
	cmpq	%rbx, 8(%rbp)
	je	.LBB3_4
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	(%rbp), %rax
	testq	%rax, %rax
	jne	.LBB3_3
# BB#6:                                 # %._crit_edge
	movl	$24, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB3_9
# BB#7:
	movq	$0, (%rax)
	movq	%rbx, 8(%rax)
	movl	%r15d, 16(%rax)
	movl	%r14d, 20(%rax)
	movq	%rax, (%rbp)
	jmp	.LBB3_8
.LBB3_1:
	movl	$24, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB3_9
# BB#2:
	movq	$0, (%rax)
	movq	%rbx, 8(%rax)
	movl	%r15d, 16(%rax)
	movl	%r14d, 20(%rax)
	movq	%rax, (%rbp,%r12,8)
	jmp	.LBB3_8
.LBB3_4:
	movl	%r15d, 16(%rbp)
	movl	%r14d, 20(%rbp)
.LBB3_8:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_9:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	def_list_mod, .Lfunc_end3-def_list_mod
	.cfi_endproc

	.globl	conflict_list
	.p2align	4, 0x90
	.type	conflict_list,@function
conflict_list:                          # @conflict_list
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpl	$0, first(%rip)
	je	.LBB4_1
# BB#3:
	movl	$24, %edi
	callq	malloc
	movq	%rax, list(%rip)
	testq	%rax, %rax
	je	.LBB4_10
# BB#4:
	movq	$0, (%rax)
	movq	%r14, 8(%rax)
	movl	$1, 16(%rax)
	movl	$0, first(%rip)
	movq	%rax, conflict_list.next(%rip)
	jmp	.LBB4_9
.LBB4_1:                                # %.preheader
	movq	list(%rip), %rax
	testq	%rax, %rax
	je	.LBB4_2
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	cmpq	%r14, 8(%rbx)
	je	.LBB4_11
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB4_5
	jmp	.LBB4_7
.LBB4_2:
	xorl	%ebx, %ebx
.LBB4_7:
	movl	$24, %edi
	callq	malloc
	movq	%rax, conflict_list.next(%rip)
	testq	%rax, %rax
	je	.LBB4_10
# BB#8:
	movq	$0, (%rax)
	movq	%r14, 8(%rax)
	movl	$1, 16(%rax)
	movq	%rax, (%rbx)
	jmp	.LBB4_9
.LBB4_11:
	incl	16(%rbx)
.LBB4_9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB4_10:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$35, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	conflict_list, .Lfunc_end4-conflict_list
	.cfi_endproc

	.type	hard_raw_mod.last,@object # @hard_raw_mod.last
	.local	hard_raw_mod.last
	.comm	hard_raw_mod.last,8,8
	.type	hard_raw_mod.hr_empty,@object # @hard_raw_mod.hr_empty
	.local	hard_raw_mod.hr_empty
	.comm	hard_raw_mod.hr_empty,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ALERT: \tOut of memory, aborting...\n"
	.size	.L.str, 36

	.type	conflict_list.next,@object # @conflict_list.next
	.local	conflict_list.next
	.comm	conflict_list.next,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
