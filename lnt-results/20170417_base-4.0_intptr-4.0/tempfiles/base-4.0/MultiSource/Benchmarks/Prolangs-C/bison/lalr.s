	.text
	.file	"lalr.bc"
	.globl	lalr
	.p2align	4, 0x90
	.type	lalr,@function
lalr:                                   # @lalr
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	ntokens(%rip), %eax
	leal	31(%rax), %ecx
	sarl	$31, %ecx
	shrl	$27, %ecx
	leal	31(%rax,%rcx), %eax
	sarl	$5, %eax
	movl	%eax, tokensetsize(%rip)
	movl	nstates(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, state_table(%rip)
	movq	first_state(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB0_4
# BB#1:                                 # %.lr.ph.preheader.i
	movswq	16(%rcx), %rdx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph..lr.ph_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	state_table(%rip), %rcx
	movswq	16(%rax), %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_2
.LBB0_4:                                # %set_state_table.exit
	movl	nstates(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, accessing_symbol(%rip)
	movq	first_state(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB0_7
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph.i4
                                        # =>This Inner Loop Header: Depth=1
	movzwl	18(%rcx), %edx
	movswq	16(%rcx), %rsi
	movw	%dx, (%rax,%rsi,2)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB0_5
.LBB0_7:                                # %set_accessing_symbol.exit
	movl	nstates(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, shift_table(%rip)
	movq	first_shift(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB0_11
# BB#8:                                 # %.lr.ph.preheader.i6
	movswq	8(%rcx), %rdx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB0_11
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph..lr.ph_crit_edge.i11
                                        # =>This Inner Loop Header: Depth=1
	movq	shift_table(%rip), %rcx
	movswq	8(%rax), %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_9
.LBB0_11:                               # %set_shift_table.exit
	movl	nstates(%rip), %edi
	shll	$3, %edi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, reduction_table(%rip)
	movq	first_reduction(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB0_15
# BB#12:                                # %.lr.ph.preheader.i13
	movswq	8(%rcx), %rdx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB0_15
	.p2align	4, 0x90
.LBB0_13:                               # %.lr.ph..lr.ph_crit_edge.i18
                                        # =>This Inner Loop Header: Depth=1
	movq	reduction_table(%rip), %rcx
	movswq	8(%rax), %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB0_13
.LBB0_15:                               # %set_reduction_table.exit
	movq	ritem(%rip), %rax
	movzwl	(%rax), %ecx
	testw	%cx, %cx
	je	.LBB0_18
# BB#16:                                # %.lr.ph.i19.preheader
	addq	$2, %rax
	xorl	%edx, %edx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph.i19
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rsi), %edi
	cmpl	%ebx, %esi
	cmovll	%ebx, %esi
	testw	%cx, %cx
	cmovlel	%edx, %edi
	cmovlel	%esi, %ebx
	movzwl	(%rax), %ecx
	addq	$2, %rax
	testw	%cx, %cx
	movl	%edi, %esi
	jne	.LBB0_17
.LBB0_18:                               # %set_maxrhs.exit
	movl	%ebx, maxrhs(%rip)
	callq	initialize_LA
	callq	set_goto_map
	callq	initialize_F
	callq	build_relations
	movq	includes(%rip), %rdi
	callq	digraph
	movl	ngotos(%rip), %ecx
	movq	includes(%rip), %rdi
	testl	%ecx, %ecx
	jle	.LBB0_23
# BB#19:                                # %.lr.ph.i20.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph.i20
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB0_22
# BB#21:                                #   in Loop: Header=BB0_20 Depth=1
	movq	%rax, %rdi
	callq	free
	movl	ngotos(%rip), %ecx
	movq	includes(%rip), %rdi
.LBB0_22:                               #   in Loop: Header=BB0_20 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB0_20
.LBB0_23:                               # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB0_25
# BB#24:
	callq	free
.LBB0_25:                               # %compute_FOLLOWS.exit
	popq	%rbx
	jmp	compute_lookaheads      # TAILCALL
.Lfunc_end0:
	.size	lalr, .Lfunc_end0-lalr
	.cfi_endproc

	.globl	set_state_table
	.p2align	4, 0x90
	.type	set_state_table,@function
set_state_table:                        # @set_state_table
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	nstates(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, state_table(%rip)
	movq	first_state(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB1_4
# BB#1:                                 # %.lr.ph.preheader
	movswq	16(%rcx), %rdx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	state_table(%rip), %rcx
	movswq	16(%rax), %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB1_2
.LBB1_4:                                # %._crit_edge
	popq	%rax
	retq
.Lfunc_end1:
	.size	set_state_table, .Lfunc_end1-set_state_table
	.cfi_endproc

	.globl	set_accessing_symbol
	.p2align	4, 0x90
	.type	set_accessing_symbol,@function
set_accessing_symbol:                   # @set_accessing_symbol
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movl	nstates(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, accessing_symbol(%rip)
	movq	first_state(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB2_3
	.p2align	4, 0x90
.LBB2_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	18(%rcx), %edx
	movswq	16(%rcx), %rsi
	movw	%dx, (%rax,%rsi,2)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB2_1
.LBB2_3:                                # %._crit_edge
	popq	%rax
	retq
.Lfunc_end2:
	.size	set_accessing_symbol, .Lfunc_end2-set_accessing_symbol
	.cfi_endproc

	.globl	set_shift_table
	.p2align	4, 0x90
	.type	set_shift_table,@function
set_shift_table:                        # @set_shift_table
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movl	nstates(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, shift_table(%rip)
	movq	first_shift(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB3_4
# BB#1:                                 # %.lr.ph.preheader
	movswq	8(%rcx), %rdx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB3_4
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	shift_table(%rip), %rcx
	movswq	8(%rax), %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB3_2
.LBB3_4:                                # %._crit_edge
	popq	%rax
	retq
.Lfunc_end3:
	.size	set_shift_table, .Lfunc_end3-set_shift_table
	.cfi_endproc

	.globl	set_reduction_table
	.p2align	4, 0x90
	.type	set_reduction_table,@function
set_reduction_table:                    # @set_reduction_table
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	nstates(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, reduction_table(%rip)
	movq	first_reduction(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB4_4
# BB#1:                                 # %.lr.ph.preheader
	movswq	8(%rcx), %rdx
	movq	%rcx, (%rax,%rdx,8)
	movq	(%rcx), %rax
	testq	%rax, %rax
	je	.LBB4_4
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	reduction_table(%rip), %rcx
	movswq	8(%rax), %rdx
	movq	%rax, (%rcx,%rdx,8)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_2
.LBB4_4:                                # %._crit_edge
	popq	%rax
	retq
.Lfunc_end4:
	.size	set_reduction_table, .Lfunc_end4-set_reduction_table
	.cfi_endproc

	.globl	set_maxrhs
	.p2align	4, 0x90
	.type	set_maxrhs,@function
set_maxrhs:                             # @set_maxrhs
	.cfi_startproc
# BB#0:
	movq	ritem(%rip), %rax
	movzwl	(%rax), %edx
	testw	%dx, %dx
	je	.LBB5_1
# BB#2:                                 # %.lr.ph.preheader
	addq	$2, %rax
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rdi), %esi
	cmpl	%ecx, %edi
	cmovll	%ecx, %edi
	testw	%dx, %dx
	cmovlel	%r8d, %esi
	cmovlel	%edi, %ecx
	movzwl	(%rax), %edx
	addq	$2, %rax
	testw	%dx, %dx
	movl	%esi, %edi
	jne	.LBB5_3
# BB#4:                                 # %._crit_edge
	movl	%ecx, maxrhs(%rip)
	retq
.LBB5_1:
	xorl	%ecx, %ecx
	movl	%ecx, maxrhs(%rip)
	retq
.Lfunc_end5:
	.size	set_maxrhs, .Lfunc_end5-set_maxrhs
	.cfi_endproc

	.globl	initialize_LA
	.p2align	4, 0x90
	.type	initialize_LA,@function
initialize_LA:                          # @initialize_LA
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movl	nstates(%rip), %edi
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, consistent(%rip)
	movl	nstates(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, lookaheads(%rip)
	movl	nstates(%rip), %esi
	testl	%esi, %esi
	jle	.LBB6_15
# BB#1:                                 # %.lr.ph66.preheader
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_12 Depth 2
	movw	%r14w, (%rax,%r8,2)
	movq	reduction_table(%rip), %rax
	movq	(%rax,%r8,8), %rsi
	movq	shift_table(%rip), %rax
	movq	(%rax,%r8,8), %rax
	testq	%rsi, %rsi
	je	.LBB6_7
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movswl	10(%rsi), %esi
	cmpl	$1, %esi
	jg	.LBB6_6
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	testq	%rax, %rax
	je	.LBB6_7
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	accessing_symbol(%rip), %rdi
	movswq	12(%rax), %rbx
	movswl	(%rdi,%rbx,2), %edi
	cmpl	ntokens(%rip), %edi
	jge	.LBB6_7
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	addl	%esi, %edx
	jmp	.LBB6_8
	.p2align	4, 0x90
.LBB6_7:                                #   in Loop: Header=BB6_2 Depth=1
	movq	consistent(%rip), %rsi
	movb	$1, (%rsi,%r8)
.LBB6_8:                                #   in Loop: Header=BB6_2 Depth=1
	movl	%edx, %r14d
	testq	%rax, %rax
	je	.LBB6_14
# BB#9:                                 # %.preheader52
                                        #   in Loop: Header=BB6_2 Depth=1
	movswq	10(%rax), %rdx
	testq	%rdx, %rdx
	jle	.LBB6_14
# BB#10:                                # %.lr.ph62
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	accessing_symbol(%rip), %rsi
	movl	error_token_number(%rip), %edi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_12:                               #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	12(%rax,%rbx,2), %rcx
	movswl	(%rsi,%rcx,2), %ecx
	cmpl	%edi, %ecx
	je	.LBB6_13
# BB#11:                                #   in Loop: Header=BB6_12 Depth=2
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB6_12
	jmp	.LBB6_14
.LBB6_13:                               #   in Loop: Header=BB6_2 Depth=1
	movq	consistent(%rip), %rax
	movb	$0, (%rax,%r8)
	.p2align	4, 0x90
.LBB6_14:                               # %.loopexit53
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%r8
	movslq	nstates(%rip), %rsi
	movq	lookaheads(%rip), %rax
	cmpq	%rsi, %r8
	movl	%r14d, %edx
	jl	.LBB6_2
.LBB6_15:                               # %._crit_edge67
	movslq	%esi, %rcx
	movw	%r14w, (%rax,%rcx,2)
	movl	tokensetsize(%rip), %edi
	imull	%r14d, %edi
	shll	$2, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, LA(%rip)
	leal	(%r14,%r14), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, LAruleno(%rip)
	shll	$3, %r14d
	xorl	%eax, %eax
	movl	%r14d, %edi
	callq	mallocate
	movq	%rax, lookback(%rip)
	movslq	nstates(%rip), %r8
	testq	%r8, %r8
	jle	.LBB6_23
# BB#16:                                # %.lr.ph60.preheader
	movq	LAruleno(%rip), %rcx
	movq	consistent(%rip), %rdx
	xorl	%esi, %esi
	jmp	.LBB6_17
	.p2align	4, 0x90
.LBB6_18:                               #   in Loop: Header=BB6_17 Depth=1
	movq	reduction_table(%rip), %rdi
	movq	(%rdi,%rsi,8), %rdi
	testq	%rdi, %rdi
	je	.LBB6_22
# BB#19:                                # %.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	cmpw	$0, 10(%rdi)
	jle	.LBB6_22
# BB#20:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_17 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_21:                               # %.lr.ph
                                        #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	12(%rdi,%rbx,2), %eax
	movw	%ax, (%rcx)
	addq	$2, %rcx
	incq	%rbx
	movswq	10(%rdi), %rax
	cmpq	%rax, %rbx
	jl	.LBB6_21
	jmp	.LBB6_22
	.p2align	4, 0x90
.LBB6_17:                               # %.lr.ph60
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_21 Depth 2
	cmpb	$0, (%rdx,%rsi)
	je	.LBB6_18
.LBB6_22:                               # %.loopexit
                                        #   in Loop: Header=BB6_17 Depth=1
	incq	%rsi
	cmpq	%r8, %rsi
	jl	.LBB6_17
.LBB6_23:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end6:
	.size	initialize_LA, .Lfunc_end6-initialize_LA
	.cfi_endproc

	.globl	set_goto_map
	.p2align	4, 0x90
	.type	set_goto_map,@function
set_goto_map:                           # @set_goto_map
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movl	nvars(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	mallocate
	movslq	ntokens(%rip), %rcx
	addq	%rcx, %rcx
	subq	%rcx, %rax
	movq	%rax, goto_map(%rip)
	movl	nvars(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r15
	movl	ntokens(%rip), %r8d
	movslq	%r8d, %r12
	addq	%r12, %r12
	movl	$0, ngotos(%rip)
	movq	first_shift(%rip), %r14
	testq	%r14, %r14
	je	.LBB7_10
# BB#1:                                 # %.lr.ph83.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph83
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
	movswq	10(%r14), %rbx
	testq	%rbx, %rbx
	jle	.LBB7_8
# BB#3:                                 # %.lr.ph76.preheader
                                        #   in Loop: Header=BB7_2 Depth=1
	addq	$5, %rbx
	.p2align	4, 0x90
.LBB7_4:                                # %.lr.ph76
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	accessing_symbol(%rip), %rax
	movswq	(%r14,%rbx,2), %rcx
	movswl	(%rax,%rcx,2), %eax
	cmpl	ntokens(%rip), %eax
	jl	.LBB7_8
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=2
	movslq	%eax, %rbp
	cmpl	$32767, %r13d           # imm = 0x7FFF
	jne	.LBB7_7
# BB#6:                                 #   in Loop: Header=BB7_4 Depth=2
	movl	$.L.str, %edi
	callq	toomany
	movl	ngotos(%rip), %r13d
.LBB7_7:                                #   in Loop: Header=BB7_4 Depth=2
	incl	%r13d
	movl	%r13d, ngotos(%rip)
	movq	goto_map(%rip), %rax
	incw	(%rax,%rbp,2)
	leaq	-1(%rbx), %rax
	addq	$-5, %rbx
	cmpq	$1, %rbx
	movq	%rax, %rbx
	jg	.LBB7_4
.LBB7_8:                                # %._crit_edge77
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB7_2
# BB#9:                                 # %._crit_edge84.loopexit
	movl	ntokens(%rip), %r8d
.LBB7_10:                               # %._crit_edge84
	movq	%r15, %r14
	subq	%r12, %r14
	movslq	nsyms(%rip), %r10
	cmpl	%r10d, %r8d
	jge	.LBB7_19
# BB#11:                                # %.lr.ph71
	movq	goto_map(%rip), %rsi
	movslq	%r8d, %rcx
	movl	%r10d, %eax
	subl	%r8d, %eax
	leaq	-1(%r10), %rbx
	subq	%rcx, %rbx
	andq	$3, %rax
	je	.LBB7_12
# BB#13:                                # %.prol.preheader
	negq	%rax
	xorl	%edx, %edx
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB7_14:                               # =>This Inner Loop Header: Depth=1
	movw	%dx, (%r14,%rdi,2)
	movswl	(%rsi,%rdi,2), %ebp
	addl	%ebp, %edx
	incq	%rdi
	incq	%rax
	jne	.LBB7_14
	jmp	.LBB7_15
.LBB7_12:
	xorl	%edx, %edx
	movq	%rcx, %rdi
.LBB7_15:                               # %.prol.loopexit
	cmpq	$3, %rbx
	jb	.LBB7_18
# BB#16:                                # %.lr.ph71.new
	movl	$6, %ebx
	subq	%r12, %rbx
	addq	%r15, %rbx
	.p2align	4, 0x90
.LBB7_17:                               # =>This Inner Loop Header: Depth=1
	movw	%dx, -6(%rbx,%rdi,2)
	movswl	(%rsi,%rdi,2), %eax
	addl	%edx, %eax
	movw	%ax, -4(%rbx,%rdi,2)
	movswl	2(%rsi,%rdi,2), %edx
	addl	%eax, %edx
	movw	%dx, -2(%rbx,%rdi,2)
	movswl	4(%rsi,%rdi,2), %eax
	addl	%edx, %eax
	movw	%ax, (%rbx,%rdi,2)
	movswl	6(%rsi,%rdi,2), %edx
	addl	%eax, %edx
	addq	$4, %rdi
	cmpq	%r10, %rdi
	jl	.LBB7_17
.LBB7_18:                               # %._crit_edge72
	cmpl	%r10d, %r8d
	jge	.LBB7_19
# BB#20:                                # %.lr.ph66
	movq	goto_map(%rip), %rdx
	movq	%r10, %r11
	subq	%rcx, %r11
	cmpq	$16, %r11
	jb	.LBB7_33
# BB#21:                                # %min.iters.checked
	movq	%r11, %r9
	andq	$-16, %r9
	je	.LBB7_33
# BB#22:                                # %vector.memcheck
	leaq	(%rdx,%rcx,2), %rax
	leaq	(%r10,%r10), %rsi
	subq	%r12, %rsi
	addq	%r15, %rsi
	cmpq	%rsi, %rax
	jae	.LBB7_24
# BB#23:                                # %vector.memcheck
	leaq	(%rdx,%r10,2), %rax
	leaq	(%rcx,%rcx), %rsi
	subq	%r12, %rsi
	addq	%r15, %rsi
	cmpq	%rax, %rsi
	jb	.LBB7_33
.LBB7_24:                               # %vector.body.preheader
	leaq	-16(%r9), %r8
	movl	%r8d, %edi
	shrl	$4, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB7_25
# BB#26:                                # %vector.body.prol.preheader
	leaq	16(%rdx,%rcx,2), %rbx
	leaq	16(%rcx,%rcx), %rsi
	subq	%r12, %rsi
	addq	%r15, %rsi
	negq	%rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_27:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi,%rax,2), %xmm0
	movups	(%rsi,%rax,2), %xmm1
	movups	%xmm0, -16(%rbx,%rax,2)
	movups	%xmm1, (%rbx,%rax,2)
	addq	$16, %rax
	incq	%rdi
	jne	.LBB7_27
	jmp	.LBB7_28
.LBB7_19:                               # %._crit_edge72.._crit_edge67_crit_edge
	movq	goto_map(%rip), %rdx
	jmp	.LBB7_34
.LBB7_25:
	xorl	%eax, %eax
.LBB7_28:                               # %vector.body.prol.loopexit
	cmpq	$48, %r8
	jb	.LBB7_31
# BB#29:                                # %vector.body.preheader.new
	movq	%r9, %rdi
	subq	%rax, %rdi
	addq	%rcx, %rax
	leaq	112(%rdx,%rax,2), %rbx
	leaq	112(%rax,%rax), %rax
	subq	%r12, %rax
	addq	%rax, %r15
	.p2align	4, 0x90
.LBB7_30:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%r15), %xmm0
	movups	-96(%r15), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%r15), %xmm0
	movups	-64(%r15), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%r15), %xmm0
	movups	-32(%r15), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%r15), %xmm0
	movups	(%r15), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %r15
	addq	$-64, %rdi
	jne	.LBB7_30
.LBB7_31:                               # %middle.block
	cmpq	%r9, %r11
	je	.LBB7_34
# BB#32:
	addq	%r9, %rcx
	.p2align	4, 0x90
.LBB7_33:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%r14,%rcx,2), %eax
	movw	%ax, (%rdx,%rcx,2)
	incq	%rcx
	cmpq	%r10, %rcx
	jl	.LBB7_33
.LBB7_34:                               # %._crit_edge67
	movw	%r13w, (%rdx,%r10,2)
	movw	%r13w, (%r14,%r10,2)
	addl	%r13d, %r13d
	xorl	%eax, %eax
	movl	%r13d, %edi
	callq	mallocate
	movq	%rax, from_state(%rip)
	movl	ngotos(%rip), %edi
	addl	%edi, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, to_state(%rip)
	movq	first_shift(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB7_41
# BB#35:                                # %.lr.ph62
	movq	accessing_symbol(%rip), %r9
	movl	ntokens(%rip), %r10d
	movq	from_state(%rip), %r8
	.p2align	4, 0x90
.LBB7_36:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_38 Depth 2
	movswq	10(%rcx), %rbp
	testq	%rbp, %rbp
	jle	.LBB7_40
# BB#37:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_36 Depth=1
	movzwl	8(%rcx), %r11d
	addq	$5, %rbp
	.p2align	4, 0x90
.LBB7_38:                               # %.lr.ph
                                        #   Parent Loop BB7_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	(%rcx,%rbp,2), %rdi
	movswl	(%r9,%rdi,2), %edx
	cmpl	%r10d, %edx
	jl	.LBB7_40
# BB#39:                                #   in Loop: Header=BB7_38 Depth=2
	movslq	%edx, %rdx
	movswq	(%r14,%rdx,2), %rsi
	leal	1(%rsi), %ebx
	movw	%bx, (%r14,%rdx,2)
	movw	%r11w, (%r8,%rsi,2)
	movw	%di, (%rax,%rsi,2)
	leaq	-1(%rbp), %rdx
	addq	$-5, %rbp
	cmpq	$1, %rbp
	movq	%rdx, %rbp
	jg	.LBB7_38
.LBB7_40:                               # %._crit_edge
                                        #   in Loop: Header=BB7_36 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_36
.LBB7_41:                               # %._crit_edge63
	movslq	ntokens(%rip), %rax
	addq	%rax, %rax
	addq	%rax, %r14
	je	.LBB7_42
# BB#43:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB7_42:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	set_goto_map, .Lfunc_end7-set_goto_map
	.cfi_endproc

	.globl	initialize_F
	.p2align	4, 0x90
	.type	initialize_F,@function
initialize_F:                           # @initialize_F
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 112
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	ngotos(%rip), %edi
	imull	tokensetsize(%rip), %edi
	shll	$2, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, F(%rip)
	movl	ngotos(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	ngotos(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %rbx
	cmpl	$0, ngotos(%rip)
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	jle	.LBB8_40
# BB#1:                                 # %.lr.ph105.preheader
	movq	F(%rip), %rbp
	leaq	16(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	14(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph105
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
                                        #     Child Loop BB8_10 Depth 2
                                        #       Child Loop BB8_13 Depth 3
                                        #     Child Loop BB8_30 Depth 2
                                        #     Child Loop BB8_34 Depth 2
                                        #     Child Loop BB8_37 Depth 2
	movq	to_state(%rip), %rax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movswq	(%rax,%rcx,2), %r13
	movq	shift_table(%rip), %rax
	movq	(%rax,%r13,8), %r14
	testq	%r14, %r14
	je	.LBB8_39
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	movswl	10(%r14), %r15d
	testl	%r15d, %r15d
	jle	.LBB8_7
# BB#4:                                 # %.lr.ph88
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	accessing_symbol(%rip), %rdx
	movslq	%r15d, %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_5:                                #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	12(%r14,%rax,2), %rcx
	movswl	(%rdx,%rcx,2), %ecx
	cmpl	ntokens(%rip), %ecx
	jge	.LBB8_8
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=2
	movl	$1, %edi
	shll	%cl, %edi
	sarl	$5, %ecx
	movslq	%ecx, %rcx
	orl	%edi, (%rbp,%rcx,4)
	incq	%rax
	cmpq	%rsi, %rax
	jl	.LBB8_5
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                #   in Loop: Header=BB8_2 Depth=1
	xorl	%eax, %eax
.LBB8_8:                                # %.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpl	%r15d, %eax
	jge	.LBB8_39
# BB#9:                                 # %.lr.ph93.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movslq	%eax, %rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB8_10:                               # %.lr.ph93
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_13 Depth 3
	movq	accessing_symbol(%rip), %rax
	movswq	12(%r14,%rbp,2), %rcx
	movq	nullable(%rip), %rdx
	movswq	(%rax,%rcx,2), %rcx
	cmpb	$0, (%rdx,%rcx)
	je	.LBB8_19
# BB#11:                                #   in Loop: Header=BB8_10 Depth=2
	movq	goto_map(%rip), %rdx
	movswl	(%rdx,%rcx,2), %eax
	movswl	2(%rdx,%rcx,2), %ecx
	cmpw	%cx, %ax
	jg	.LBB8_17
# BB#12:                                # %.lr.ph.lr.ph.i
                                        #   in Loop: Header=BB8_10 Depth=2
	movq	from_state(%rip), %rdx
	.p2align	4, 0x90
.LBB8_13:                               #   Parent Loop BB8_2 Depth=1
                                        #     Parent Loop BB8_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leal	(%rcx,%rax), %edi
	movl	%edi, %esi
	shrl	$31, %esi
	addl	%edi, %esi
	sarl	%esi
	movslq	%esi, %rdi
	cmpw	%r13w, (%rdx,%rdi,2)
	je	.LBB8_18
# BB#14:                                #   in Loop: Header=BB8_13 Depth=3
	jl	.LBB8_16
# BB#15:                                #   in Loop: Header=BB8_13 Depth=3
	leal	-1(%rsi), %ecx
	cmpl	%esi, %eax
	jl	.LBB8_13
	jmp	.LBB8_17
.LBB8_16:                               # %.outer.i
                                        #   in Loop: Header=BB8_13 Depth=3
	leal	1(%rsi), %eax
	cmpl	%ecx, %esi
	jl	.LBB8_13
	.p2align	4, 0x90
.LBB8_17:                               # %.outer._crit_edge.i
                                        #   in Loop: Header=BB8_10 Depth=2
	movl	$.L.str.1, %edi
	callq	berror
	movq	16(%rsp), %rbx          # 8-byte Reload
                                        # implicit-def: %ESI
.LBB8_18:                               # %map_goto.exit
                                        #   in Loop: Header=BB8_10 Depth=2
	movslq	%r12d, %rax
	incl	%r12d
	movw	%si, (%rbx,%rax,2)
.LBB8_19:                               #   in Loop: Header=BB8_10 Depth=2
	incq	%rbp
	cmpl	%r15d, %ebp
	jne	.LBB8_10
# BB#20:                                # %._crit_edge94
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%r12d, %r12d
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB8_39
# BB#21:                                #   in Loop: Header=BB8_2 Depth=1
	leal	2(%r12,%r12), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rax, (%rcx,%rdx,8)
	testl	%r12d, %r12d
	jle	.LBB8_38
# BB#22:                                # %.lr.ph98.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	%r12d, %ecx
	cmpl	$16, %r12d
	jae	.LBB8_24
# BB#23:                                #   in Loop: Header=BB8_2 Depth=1
	xorl	%edx, %edx
	jmp	.LBB8_32
.LBB8_24:                               # %min.iters.checked
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	%r12d, %r8d
	andl	$15, %r8d
	movq	%rcx, %rdx
	subq	%r8, %rdx
	je	.LBB8_28
# BB#25:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_2 Depth=1
	leaq	(%rbx,%rcx,2), %rsi
	cmpq	%rsi, %rax
	jae	.LBB8_29
# BB#26:                                # %vector.memcheck
                                        #   in Loop: Header=BB8_2 Depth=1
	leaq	(%rax,%rcx,2), %rsi
	cmpq	%rsi, %rbx
	jae	.LBB8_29
.LBB8_28:                               #   in Loop: Header=BB8_2 Depth=1
	xorl	%edx, %edx
	jmp	.LBB8_32
.LBB8_29:                               # %vector.body.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	leaq	16(%rax), %rdi
	movq	%rdx, %rbp
	movq	40(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB8_30:                               # %vector.body
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$-16, %rbp
	jne	.LBB8_30
# BB#31:                                # %middle.block
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%r8d, %r8d
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB8_38
.LBB8_32:                               # %.lr.ph98.preheader135
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	%ecx, %edi
	subl	%edx, %edi
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB8_35
# BB#33:                                # %.lr.ph98.prol.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB8_34:                               # %.lr.ph98.prol
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%rdx,2), %ebp
	movw	%bp, (%rax,%rdx,2)
	incq	%rdx
	incq	%rdi
	jne	.LBB8_34
.LBB8_35:                               # %.lr.ph98.prol.loopexit
                                        #   in Loop: Header=BB8_2 Depth=1
	cmpq	$7, %rsi
	movq	8(%rsp), %rbp           # 8-byte Reload
	jb	.LBB8_38
# BB#36:                                # %.lr.ph98.preheader135.new
                                        #   in Loop: Header=BB8_2 Depth=1
	subq	%rdx, %rcx
	leaq	14(%rax,%rdx,2), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB8_37:                               # %.lr.ph98
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-14(%rdx), %edi
	movw	%di, -14(%rsi)
	movzwl	-12(%rdx), %edi
	movw	%di, -12(%rsi)
	movzwl	-10(%rdx), %edi
	movw	%di, -10(%rsi)
	movzwl	-8(%rdx), %edi
	movw	%di, -8(%rsi)
	movzwl	-6(%rdx), %edi
	movw	%di, -6(%rsi)
	movzwl	-4(%rdx), %edi
	movw	%di, -4(%rsi)
	movzwl	-2(%rdx), %edi
	movw	%di, -2(%rsi)
	movzwl	(%rdx), %edi
	movw	%di, (%rsi)
	addq	$16, %rsi
	addq	$16, %rdx
	addq	$-8, %rcx
	jne	.LBB8_37
.LBB8_38:                               # %._crit_edge99
                                        #   in Loop: Header=BB8_2 Depth=1
	movslq	%r12d, %rcx
	movw	$-1, (%rax,%rcx,2)
	.p2align	4, 0x90
.LBB8_39:                               # %._crit_edge94.thread
                                        #   in Loop: Header=BB8_2 Depth=1
	movslq	tokensetsize(%rip), %rax
	leaq	(%rbp,%rax,4), %rbp
	movq	32(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movslq	ngotos(%rip), %rax
	cmpq	%rax, %rcx
	jl	.LBB8_2
.LBB8_40:                               # %._crit_edge106
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	callq	digraph
	movl	ngotos(%rip), %eax
	testl	%eax, %eax
	jle	.LBB8_45
# BB#41:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_42:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB8_44
# BB#43:                                #   in Loop: Header=BB8_42 Depth=1
	callq	free
	movl	ngotos(%rip), %eax
.LBB8_44:                               #   in Loop: Header=BB8_42 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB8_42
.LBB8_45:                               # %._crit_edge
	testq	%rbp, %rbp
	je	.LBB8_47
# BB#46:
	movq	%rbp, %rdi
	callq	free
.LBB8_47:
	movq	16(%rsp), %rdi          # 8-byte Reload
	addq	$56, %rsp
	testq	%rdi, %rdi
	je	.LBB8_49
# BB#48:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB8_49:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	initialize_F, .Lfunc_end8-initialize_F
	.cfi_endproc

	.globl	build_relations
	.p2align	4, 0x90
	.type	build_relations,@function
build_relations:                        # @build_relations
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 112
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	ngotos(%rip), %edi
	shll	$3, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, includes(%rip)
	movl	ngotos(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r14
	movl	maxrhs(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r9
	movl	ngotos(%rip), %esi
	testl	%esi, %esi
	movq	%r9, 16(%rsp)           # 8-byte Spill
	jle	.LBB9_52
# BB#1:                                 # %.lr.ph157.preheader
	leaq	16(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	14(%r14), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_2:                                # %.lr.ph157
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
                                        #       Child Loop BB9_6 Depth 3
                                        #         Child Loop BB9_8 Depth 4
                                        #       Child Loop BB9_15 Depth 3
                                        #       Child Loop BB9_21 Depth 3
                                        #         Child Loop BB9_25 Depth 4
                                        #     Child Loop BB9_42 Depth 2
                                        #     Child Loop BB9_46 Depth 2
                                        #     Child Loop BB9_49 Depth 2
	movq	accessing_symbol(%rip), %rax
	movq	to_state(%rip), %rcx
	movswq	(%rcx,%r12,2), %rcx
	movq	derives(%rip), %rdx
	movswq	(%rax,%rcx,2), %rax
	movq	(%rdx,%rax,8), %rbx
	cmpw	$0, (%rbx)
	jle	.LBB9_51
# BB#3:                                 # %.lr.ph148
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	from_state(%rip), %rax
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movzwl	(%rax,%r12,2), %eax
	movw	%ax, 10(%rsp)           # 2-byte Spill
	cwtl
	movl	%eax, 12(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB9_4:                                #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_6 Depth 3
                                        #         Child Loop BB9_8 Depth 4
                                        #       Child Loop BB9_15 Depth 3
                                        #       Child Loop BB9_21 Depth 3
                                        #         Child Loop BB9_25 Depth 4
	movzwl	10(%rsp), %eax          # 2-byte Folded Reload
	movw	%ax, (%r9)
	movq	ritem(%rip), %rax
	movq	rrhs(%rip), %rcx
	movswq	(%rbx), %rdx
	movswq	(%rcx,%rdx,2), %rcx
	leaq	(%rax,%rcx,2), %r12
	movzwl	(%rax,%rcx,2), %esi
	testw	%si, %si
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	jle	.LBB9_11
# BB#5:                                 # %.lr.ph132
                                        #   in Loop: Header=BB9_4 Depth=2
	movq	shift_table(%rip), %r8
	movq	accessing_symbol(%rip), %rcx
	movl	$1, %r13d
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edx
	.p2align	4, 0x90
.LBB9_6:                                #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB9_8 Depth 4
	movslq	%edx, %rax
	movq	(%r8,%rax,8), %rdi
	movswq	10(%rdi), %rbx
	testq	%rbx, %rbx
	jle	.LBB9_10
# BB#7:                                 # %.lr.ph123.preheader
                                        #   in Loop: Header=BB9_6 Depth=3
	movl	$6, %eax
	.p2align	4, 0x90
.LBB9_8:                                # %.lr.ph123
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_4 Depth=2
                                        #       Parent Loop BB9_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movswq	(%rdi,%rax,2), %rdx
	cmpw	%si, (%rcx,%rdx,2)
	je	.LBB9_10
# BB#9:                                 # %.lr.ph123
                                        #   in Loop: Header=BB9_8 Depth=4
	leaq	-5(%rax), %rbp
	incq	%rax
	cmpq	%rbx, %rbp
	jl	.LBB9_8
.LBB9_10:                               # %._crit_edge124
                                        #   in Loop: Header=BB9_6 Depth=3
	movw	%dx, (%r9,%r13,2)
	incq	%r13
	movzwl	2(%r12), %esi
	addq	$2, %r12
	testw	%si, %si
	jg	.LBB9_6
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_11:                               #   in Loop: Header=BB9_4 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edx
	movl	$1, %r13d
.LBB9_12:                               # %._crit_edge133
                                        #   in Loop: Header=BB9_4 Depth=2
	movq	consistent(%rip), %rcx
	movslq	%edx, %rax
	cmpb	$0, (%rcx,%rax)
	jne	.LBB9_20
# BB#13:                                #   in Loop: Header=BB9_4 Depth=2
	movq	%r14, %rbx
	movq	lookaheads(%rip), %rcx
	movswl	(%rcx,%rax,2), %r14d
	movswl	2(%rcx,%rax,2), %eax
	cmpw	%ax, %r14w
	jge	.LBB9_18
# BB#14:                                # %.lr.ph.i
                                        #   in Loop: Header=BB9_4 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx), %ecx
	movq	LAruleno(%rip), %rdx
	.p2align	4, 0x90
.LBB9_15:                               #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%r14d, %r14
	movzwl	(%rdx,%r14,2), %esi
	xorl	%edi, %edi
	cmpw	%cx, %si
	setne	%dil
	addl	%edi, %r14d
	cmpl	%eax, %r14d
	jge	.LBB9_17
# BB#16:                                #   in Loop: Header=BB9_15 Depth=3
	testb	%dil, %dil
	jne	.LBB9_15
.LBB9_17:                               # %.critedge.i
                                        #   in Loop: Header=BB9_4 Depth=2
	cmpw	%cx, %si
	je	.LBB9_19
.LBB9_18:                               # %.critedge.thread.i
                                        #   in Loop: Header=BB9_4 Depth=2
	movl	$.L.str.2, %edi
	callq	berror
.LBB9_19:                               # %add_lookback_edge.exit
                                        #   in Loop: Header=BB9_4 Depth=2
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	lookback(%rip), %rcx
	movslq	%r14d, %rdx
	movq	(%rcx,%rdx,8), %rsi
	movq	%rsi, (%rax)
	movq	24(%rsp), %rsi          # 8-byte Reload
	movw	%si, 8(%rax)
	movq	%rax, (%rcx,%rdx,8)
	movq	%rbx, %r14
	movq	16(%rsp), %r9           # 8-byte Reload
.LBB9_20:                               #   in Loop: Header=BB9_4 Depth=2
	shlq	$32, %r13
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	addq	%rax, %r13
	sarq	$32, %r13
	movslq	%r15d, %r15
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_21:                               # %.lr.ph138
                                        #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB9_25 Depth 4
	addq	$-2, %r12
	cmpq	ritem(%rip), %r12
	jb	.LBB9_31
# BB#22:                                #   in Loop: Header=BB9_21 Depth=3
	movswl	(%r12), %eax
	cmpl	ntokens(%rip), %eax
	jl	.LBB9_31
# BB#23:                                #   in Loop: Header=BB9_21 Depth=3
	movslq	%eax, %rcx
	movq	goto_map(%rip), %rdx
	movswl	(%rdx,%rcx,2), %eax
	movswl	2(%rdx,%rcx,2), %ecx
	cmpw	%cx, %ax
	jg	.LBB9_29
# BB#24:                                # %.lr.ph.lr.ph.i
                                        #   in Loop: Header=BB9_21 Depth=3
	movzwl	-2(%r9,%r13,2), %edx
	movq	from_state(%rip), %rsi
	.p2align	4, 0x90
.LBB9_25:                               #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_4 Depth=2
                                        #       Parent Loop BB9_21 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	(%rcx,%rax), %ebp
	movl	%ebp, %edi
	shrl	$31, %edi
	addl	%ebp, %edi
	sarl	%edi
	movslq	%edi, %rbp
	cmpw	%dx, (%rsi,%rbp,2)
	je	.LBB9_30
# BB#26:                                #   in Loop: Header=BB9_25 Depth=4
	jl	.LBB9_28
# BB#27:                                #   in Loop: Header=BB9_25 Depth=4
	leal	-1(%rdi), %ecx
	cmpl	%edi, %eax
	jl	.LBB9_25
	jmp	.LBB9_29
.LBB9_28:                               # %.outer.i
                                        #   in Loop: Header=BB9_25 Depth=4
	leal	1(%rdi), %eax
	cmpl	%ecx, %edi
	jl	.LBB9_25
	.p2align	4, 0x90
.LBB9_29:                               # %.outer._crit_edge.i
                                        #   in Loop: Header=BB9_21 Depth=3
	movl	$.L.str.1, %edi
	callq	berror
	movq	16(%rsp), %r9           # 8-byte Reload
                                        # implicit-def: %EDI
.LBB9_30:                               # %map_goto.exit
                                        #   in Loop: Header=BB9_21 Depth=3
	decq	%r13
	movw	%di, (%r14,%r15,2)
	incq	%r15
	movq	nullable(%rip), %rax
	movswq	(%r12), %rcx
	cmpb	$0, (%rax,%rcx)
	jne	.LBB9_21
.LBB9_31:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB9_4 Depth=2
	cmpw	$0, 2(%rbx)
	leaq	2(%rbx), %rbx
	jg	.LBB9_4
# BB#32:                                # %._crit_edge149
                                        #   in Loop: Header=BB9_2 Depth=1
	testl	%r15d, %r15d
	movq	24(%rsp), %r12          # 8-byte Reload
	je	.LBB9_51
# BB#33:                                #   in Loop: Header=BB9_2 Depth=1
	leal	2(%r15,%r15), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	includes(%rip), %rcx
	movq	%rax, (%rcx,%r12,8)
	testl	%r15d, %r15d
	jle	.LBB9_50
# BB#34:                                # %.lr.ph153.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%r15d, %ecx
	cmpl	$16, %r15d
	jae	.LBB9_36
# BB#35:                                #   in Loop: Header=BB9_2 Depth=1
	xorl	%edx, %edx
	jmp	.LBB9_44
.LBB9_36:                               # %min.iters.checked
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%r15d, %esi
	andl	$15, %esi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	je	.LBB9_40
# BB#37:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_2 Depth=1
	leaq	(%r14,%rcx,2), %rdi
	cmpq	%rdi, %rax
	jae	.LBB9_41
# BB#38:                                # %vector.memcheck
                                        #   in Loop: Header=BB9_2 Depth=1
	leaq	(%rax,%rcx,2), %rdi
	cmpq	%rdi, %r14
	jae	.LBB9_41
.LBB9_40:                               #   in Loop: Header=BB9_2 Depth=1
	xorl	%edx, %edx
	jmp	.LBB9_44
.LBB9_41:                               # %vector.body.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	leaq	16(%rax), %rdi
	movq	%rdx, %rbp
	movq	40(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_42:                               # %vector.body
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$32, %rbx
	addq	$32, %rdi
	addq	$-16, %rbp
	jne	.LBB9_42
# BB#43:                                # %middle.block
                                        #   in Loop: Header=BB9_2 Depth=1
	testl	%esi, %esi
	je	.LBB9_50
	.p2align	4, 0x90
.LBB9_44:                               # %.lr.ph153.preheader211
                                        #   in Loop: Header=BB9_2 Depth=1
	movl	%ecx, %edi
	subl	%edx, %edi
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB9_47
# BB#45:                                # %.lr.ph153.prol.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB9_46:                               # %.lr.ph153.prol
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r14,%rdx,2), %ebp
	movw	%bp, (%rax,%rdx,2)
	incq	%rdx
	incq	%rdi
	jne	.LBB9_46
.LBB9_47:                               # %.lr.ph153.prol.loopexit
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpq	$7, %rsi
	jb	.LBB9_50
# BB#48:                                # %.lr.ph153.preheader211.new
                                        #   in Loop: Header=BB9_2 Depth=1
	subq	%rdx, %rcx
	leaq	14(%rax,%rdx,2), %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rdx,2), %rdx
	.p2align	4, 0x90
.LBB9_49:                               # %.lr.ph153
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-14(%rdx), %edi
	movw	%di, -14(%rsi)
	movzwl	-12(%rdx), %edi
	movw	%di, -12(%rsi)
	movzwl	-10(%rdx), %edi
	movw	%di, -10(%rsi)
	movzwl	-8(%rdx), %edi
	movw	%di, -8(%rsi)
	movzwl	-6(%rdx), %edi
	movw	%di, -6(%rsi)
	movzwl	-4(%rdx), %edi
	movw	%di, -4(%rsi)
	movzwl	-2(%rdx), %edi
	movw	%di, -2(%rsi)
	movzwl	(%rdx), %edi
	movw	%di, (%rsi)
	addq	$16, %rsi
	addq	$16, %rdx
	addq	$-8, %rcx
	jne	.LBB9_49
.LBB9_50:                               # %._crit_edge154
                                        #   in Loop: Header=BB9_2 Depth=1
	movslq	%r15d, %rcx
	movw	$-1, (%rax,%rcx,2)
	movq	16(%rsp), %r9           # 8-byte Reload
.LBB9_51:                               # %._crit_edge149.thread
                                        #   in Loop: Header=BB9_2 Depth=1
	incq	%r12
	movslq	ngotos(%rip), %rsi
	cmpq	%rsi, %r12
	jl	.LBB9_2
.LBB9_52:                               # %._crit_edge158
	movq	includes(%rip), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	transpose
	movq	%rax, %r15
	movl	ngotos(%rip), %ecx
	movq	includes(%rip), %rdi
	testl	%ecx, %ecx
	jle	.LBB9_57
# BB#53:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_54:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB9_56
# BB#55:                                #   in Loop: Header=BB9_54 Depth=1
	movq	%rax, %rdi
	callq	free
	movl	ngotos(%rip), %ecx
	movq	includes(%rip), %rdi
.LBB9_56:                               #   in Loop: Header=BB9_54 Depth=1
	incq	%rbp
	movslq	%ecx, %rax
	cmpq	%rax, %rbp
	jl	.LBB9_54
.LBB9_57:                               # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB9_59
# BB#58:
	callq	free
.LBB9_59:
	movq	%r15, includes(%rip)
	testq	%r14, %r14
	je	.LBB9_61
# BB#60:
	movq	%r14, %rdi
	callq	free
.LBB9_61:
	movq	16(%rsp), %rdi          # 8-byte Reload
	addq	$56, %rsp
	testq	%rdi, %rdi
	je	.LBB9_63
# BB#62:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB9_63:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	build_relations, .Lfunc_end9-build_relations
	.cfi_endproc

	.globl	compute_FOLLOWS
	.p2align	4, 0x90
	.type	compute_FOLLOWS,@function
compute_FOLLOWS:                        # @compute_FOLLOWS
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 16
.Lcfi51:
	.cfi_offset %rbx, -16
	movq	includes(%rip), %rdi
	callq	digraph
	movl	ngotos(%rip), %ecx
	movq	includes(%rip), %rdi
	testl	%ecx, %ecx
	jle	.LBB10_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB10_4
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	movq	%rax, %rdi
	callq	free
	movl	ngotos(%rip), %ecx
	movq	includes(%rip), %rdi
.LBB10_4:                               #   in Loop: Header=BB10_2 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB10_2
.LBB10_5:                               # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB10_6
# BB#7:
	popq	%rbx
	jmp	free                    # TAILCALL
.LBB10_6:
	popq	%rbx
	retq
.Lfunc_end10:
	.size	compute_FOLLOWS, .Lfunc_end10-compute_FOLLOWS
	.cfi_endproc

	.globl	compute_lookaheads
	.p2align	4, 0x90
	.type	compute_lookaheads,@function
compute_lookaheads:                     # @compute_lookaheads
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 128
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	lookaheads(%rip), %rax
	movslq	nstates(%rip), %rcx
	movswl	(%rax,%rcx,2), %eax
	testl	%eax, %eax
	jle	.LBB11_34
# BB#1:                                 # %.lr.ph60
	movq	LA(%rip), %rdx
	movq	lookback(%rip), %rdi
	movq	F(%rip), %r9
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	4(%r9), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	48(%r9), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB11_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_26 Depth 2
                                        #     Child Loop BB11_6 Depth 2
                                        #       Child Loop BB11_17 Depth 3
                                        #       Child Loop BB11_22 Depth 3
                                        #       Child Loop BB11_24 Depth 3
	movq	%rdx, %r10
	movslq	tokensetsize(%rip), %rbx
	leaq	(%r10,%rbx,4), %rdx
	movq	(%rdi,%r13,8), %rax
	testq	%rax, %rax
	je	.LBB11_27
# BB#3:                                 # %.lr.ph55
                                        #   in Loop: Header=BB11_2 Depth=1
	testl	%ebx, %ebx
	jle	.LBB11_26
# BB#4:                                 # %.lr.ph55.split.us.preheader
                                        #   in Loop: Header=BB11_2 Depth=1
	leaq	4(%r10), %rcx
	cmpq	%rcx, %rdx
	cmovaq	%rdx, %rcx
	subq	%r10, %rcx
	decq	%rcx
	shrq	$2, %rcx
	leaq	4(%r10,%rcx,4), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %r12
	leaq	1(%rcx), %r15
	movq	%r15, %r8
	movabsq	$9223372036854775800, %rcx # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rcx, %r8
	leaq	-8(%r8), %rcx
	shrq	$3, %rcx
	leaq	(%r10,%r8,4), %rsi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leaq	48(%r10), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jmp	.LBB11_6
	.p2align	4, 0x90
.LBB11_5:                               # %._crit_edge51.us..lr.ph55.split.us_crit_edge
                                        #   in Loop: Header=BB11_6 Depth=2
	movl	tokensetsize(%rip), %ebx
.LBB11_6:                               # %.lr.ph55.split.us
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_17 Depth 3
                                        #       Child Loop BB11_22 Depth 3
                                        #       Child Loop BB11_24 Depth 3
	movswq	8(%rax), %rcx
	movslq	%ebx, %rbx
	imulq	%rcx, %rbx
	cmpq	$8, %r15
	leaq	(%r9,%rbx,4), %r14
	jae	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_6 Depth=2
	movq	%r10, %rbx
	jmp	.LBB11_20
	.p2align	4, 0x90
.LBB11_8:                               # %min.iters.checked
                                        #   in Loop: Header=BB11_6 Depth=2
	testq	%r8, %r8
	je	.LBB11_12
# BB#9:                                 # %vector.memcheck
                                        #   in Loop: Header=BB11_6 Depth=2
	leaq	(%r12,%rbx,4), %rcx
	cmpq	%rcx, %r10
	jae	.LBB11_13
# BB#10:                                # %vector.memcheck
                                        #   in Loop: Header=BB11_6 Depth=2
	cmpq	%rdi, %r14
	jae	.LBB11_13
.LBB11_12:                              #   in Loop: Header=BB11_6 Depth=2
	movq	%r10, %rbx
.LBB11_20:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB11_6 Depth=2
	leaq	4(%rbx), %r11
	cmpq	%r11, %rdx
	cmovaq	%rdx, %r11
	subq	%rbx, %r11
	decq	%r11
	movl	%r11d, %ecx
	shrl	$2, %ecx
	incl	%ecx
	andq	$3, %rcx
	je	.LBB11_23
# BB#21:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB11_6 Depth=2
	negq	%rcx
	.p2align	4, 0x90
.LBB11_22:                              # %scalar.ph.prol
                                        #   Parent Loop BB11_2 Depth=1
                                        #     Parent Loop BB11_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r14), %ebp
	addq	$4, %r14
	orl	%ebp, (%rbx)
	addq	$4, %rbx
	incq	%rcx
	jne	.LBB11_22
.LBB11_23:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB11_6 Depth=2
	cmpq	$12, %r11
	jb	.LBB11_25
	.p2align	4, 0x90
.LBB11_24:                              # %scalar.ph
                                        #   Parent Loop BB11_2 Depth=1
                                        #     Parent Loop BB11_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r14), %ecx
	orl	%ecx, (%rbx)
	movl	4(%r14), %ecx
	orl	%ecx, 4(%rbx)
	movl	8(%r14), %ecx
	orl	%ecx, 8(%rbx)
	movl	12(%r14), %ecx
	orl	%ecx, 12(%rbx)
	addq	$16, %rbx
	addq	$16, %r14
	cmpq	%rdx, %rbx
	jb	.LBB11_24
.LBB11_25:                              # %._crit_edge51.us
                                        #   in Loop: Header=BB11_6 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB11_5
	jmp	.LBB11_27
.LBB11_13:                              # %vector.body.preheader
                                        #   in Loop: Header=BB11_6 Depth=2
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB11_15
# BB#14:                                # %vector.body.prol
                                        #   in Loop: Header=BB11_6 Depth=2
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	(%r10), %xmm2
	movups	16(%r10), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%r10)
	movups	%xmm3, 16(%r10)
	movl	$8, %ecx
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB11_16
	jmp	.LBB11_18
.LBB11_15:                              #   in Loop: Header=BB11_6 Depth=2
	xorl	%ecx, %ecx
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB11_18
.LBB11_16:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB11_6 Depth=2
	movq	%r8, %rbp
	subq	%rcx, %rbp
	addq	%rcx, %rbx
	movq	%rdi, %rsi
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rbx,4), %rbx
	movq	%rsi, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB11_17:                              # %vector.body
                                        #   Parent Loop BB11_2 Depth=1
                                        #     Parent Loop BB11_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	-48(%rcx), %xmm2
	movups	-32(%rcx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -48(%rcx)
	movups	%xmm3, -32(%rcx)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	-16(%rcx), %xmm2
	movups	(%rcx), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -16(%rcx)
	movups	%xmm3, (%rcx)
	addq	$64, %rbx
	addq	$64, %rcx
	addq	$-16, %rbp
	jne	.LBB11_17
.LBB11_18:                              # %middle.block
                                        #   in Loop: Header=BB11_6 Depth=2
	cmpq	%r8, %r15
	je	.LBB11_25
# BB#19:                                #   in Loop: Header=BB11_6 Depth=2
	leaq	(%r14,%r8,4), %r14
	movq	40(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB11_20
	.p2align	4, 0x90
.LBB11_26:                              # %.lr.ph55.split
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB11_26
.LBB11_27:                              # %._crit_edge56
                                        #   in Loop: Header=BB11_2 Depth=1
	incq	%r13
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	jne	.LBB11_2
# BB#28:                                # %.preheader
	cmpw	$0, 4(%rsp)             # 2-byte Folded Reload
	jle	.LBB11_35
# BB#29:                                # %.lr.ph45.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_30:                              # %.lr.ph45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_31 Depth 2
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB11_33
	.p2align	4, 0x90
.LBB11_31:                              # %.lr.ph
                                        #   Parent Loop BB11_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbp
	movq	%rax, %rdi
	callq	free
	testq	%rbp, %rbp
	movq	%rbp, %rax
	jne	.LBB11_31
# BB#32:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB11_30 Depth=1
	movq	lookback(%rip), %rdi
.LBB11_33:                              # %._crit_edge
                                        #   in Loop: Header=BB11_30 Depth=1
	incq	%rbx
	cmpq	8(%rsp), %rbx           # 8-byte Folded Reload
	jne	.LBB11_30
	jmp	.LBB11_35
.LBB11_34:                              # %.preheader.thread
	movq	lookback(%rip), %rdi
.LBB11_35:                              # %._crit_edge46
	testq	%rdi, %rdi
	je	.LBB11_37
# BB#36:
	callq	free
.LBB11_37:
	movq	F(%rip), %rdi
	addq	$72, %rsp
	testq	%rdi, %rdi
	je	.LBB11_39
# BB#38:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB11_39:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	compute_lookaheads, .Lfunc_end11-compute_lookaheads
	.cfi_endproc

	.globl	map_goto
	.p2align	4, 0x90
	.type	map_goto,@function
map_goto:                               # @map_goto
	.cfi_startproc
# BB#0:
	movq	goto_map(%rip), %rax
	movslq	%esi, %rdx
	movswl	(%rax,%rdx,2), %ecx
	movswl	2(%rax,%rdx,2), %edx
	cmpw	%dx, %cx
	jg	.LBB12_6
# BB#1:                                 # %.lr.ph.lr.ph
	movq	from_state(%rip), %r8
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rcx), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	sarl	%eax
	movslq	%eax, %rsi
	movswl	(%r8,%rsi,2), %esi
	cmpl	%edi, %esi
	je	.LBB12_7
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	jl	.LBB12_4
# BB#5:                                 #   in Loop: Header=BB12_2 Depth=1
	leal	-1(%rax), %edx
	cmpl	%eax, %ecx
	jl	.LBB12_2
	jmp	.LBB12_6
.LBB12_4:                               # %.outer
                                        #   in Loop: Header=BB12_2 Depth=1
	leal	1(%rax), %ecx
	cmpl	%edx, %eax
	jl	.LBB12_2
.LBB12_6:                               # %.outer._crit_edge
	pushq	%rax
.Lcfi65:
	.cfi_def_cfa_offset 16
	movl	$.L.str.1, %edi
	callq	berror
                                        # implicit-def: %EAX
	addq	$8, %rsp
.LBB12_7:                               # %.loopexit
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end12:
	.size	map_goto, .Lfunc_end12-map_goto
	.cfi_endproc

	.globl	digraph
	.p2align	4, 0x90
	.type	digraph,@function
digraph:                                # @digraph
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 32
.Lcfi69:
	.cfi_offset %rbx, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	ngotos(%rip), %eax
	leal	2(%rax), %ecx
	movl	%ecx, infinity(%rip)
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, INDEX(%rip)
	movl	ngotos(%rip), %eax
	leal	2(%rax,%rax), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, VERTICES(%rip)
	movl	$0, top(%rip)
	movq	%rbx, R(%rip)
	movl	ngotos(%rip), %r15d
	movq	INDEX(%rip), %r14
	testl	%r15d, %r15d
	jle	.LBB13_6
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%r15), %eax
	leaq	2(%rax,%rax), %rdx
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
	.p2align	4, 0x90
.LBB13_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpw	$0, (%r14,%rbx,2)
	jne	.LBB13_5
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	R(%rip), %rax
	cmpq	$0, (%rax,%rbx,8)
	je	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	movl	%ebx, %edi
	callq	traverse
	movl	ngotos(%rip), %r15d
	movq	INDEX(%rip), %r14
.LBB13_5:                               #   in Loop: Header=BB13_2 Depth=1
	incq	%rbx
	movslq	%r15d, %rax
	cmpq	%rax, %rbx
	jl	.LBB13_2
.LBB13_6:                               # %._crit_edge
	testq	%r14, %r14
	je	.LBB13_8
# BB#7:
	movq	%r14, %rdi
	callq	free
.LBB13_8:
	movq	VERTICES(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB13_9
# BB#10:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	free                    # TAILCALL
.LBB13_9:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	digraph, .Lfunc_end13-digraph
	.cfi_endproc

	.globl	add_lookback_edge
	.p2align	4, 0x90
	.type	add_lookback_edge,@function
add_lookback_edge:                      # @add_lookback_edge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	lookaheads(%rip), %rax
	movslq	%edi, %rcx
	movswl	(%rax,%rcx,2), %ebx
	movswl	2(%rax,%rcx,2), %eax
	cmpw	%ax, %bx
	jge	.LBB14_5
# BB#1:                                 # %.lr.ph
	movq	LAruleno(%rip), %rcx
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rbx
	movswl	(%rcx,%rbx,2), %edx
	xorl	%edi, %edi
	cmpl	%esi, %edx
	setne	%dil
	addl	%edi, %ebx
	cmpl	%eax, %ebx
	jge	.LBB14_4
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	testb	%dil, %dil
	jne	.LBB14_2
.LBB14_4:                               # %.critedge
	cmpl	%esi, %edx
	je	.LBB14_6
.LBB14_5:                               # %.critedge.thread
	movl	$.L.str.2, %edi
	callq	berror
.LBB14_6:
	movl	$16, %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	lookback(%rip), %rcx
	movslq	%ebx, %rdx
	movq	(%rcx,%rdx,8), %rsi
	movq	%rsi, (%rax)
	movw	%bp, 8(%rax)
	movq	%rax, (%rcx,%rdx,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end14:
	.size	add_lookback_edge, .Lfunc_end14-add_lookback_edge
	.cfi_endproc

	.globl	transpose
	.p2align	4, 0x90
	.type	transpose,@function
transpose:                              # @transpose
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 96
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	leal	(%rbp,%rbp), %edi
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, %r12
	testl	%ebp, %ebp
	jle	.LBB15_7
# BB#1:                                 # %.lr.ph67.preheader
	movl	%ebp, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph67
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
	movq	(%rbx,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB15_6
# BB#3:                                 # %.preheader54
                                        #   in Loop: Header=BB15_2 Depth=1
	movzwl	(%rdx), %esi
	testw	%si, %si
	js	.LBB15_6
# BB#4:                                 # %.lr.ph64.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	addq	$2, %rdx
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph64
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	%si, %rsi
	incw	(%r12,%rsi,2)
	movzwl	(%rdx), %esi
	addq	$2, %rdx
	testw	%si, %si
	jns	.LBB15_5
.LBB15_6:                               # %.loopexit55
                                        #   in Loop: Header=BB15_2 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jne	.LBB15_2
.LBB15_7:                               # %._crit_edge68
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	leal	(,%rbp,8), %ebx
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	mallocate
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	mallocate
	movq	%rax, %rbx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	jle	.LBB15_12
# BB#8:                                 # %.lr.ph61.preheader
	movl	16(%rsp), %ebp          # 4-byte Reload
	movq	%rbx, %r15
	movq	8(%rsp), %r14           # 8-byte Reload
	movq	%r12, %r13
	.p2align	4, 0x90
.LBB15_9:                               # %.lr.ph61
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%r13), %eax
	testl	%eax, %eax
	jle	.LBB15_11
# BB#10:                                #   in Loop: Header=BB15_9 Depth=1
	leal	2(%rax,%rax), %edi
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	callq	mallocate
	movq	%rax, (%r14)
	movq	%rax, (%r15)
	movslq	32(%rsp), %rcx          # 4-byte Folded Reload
	movw	$-1, (%rax,%rcx,2)
.LBB15_11:                              #   in Loop: Header=BB15_9 Depth=1
	addq	$2, %r13
	addq	$8, %r14
	addq	$8, %r15
	decq	%rbp
	jne	.LBB15_9
.LBB15_12:                              # %._crit_edge62
	testq	%r12, %r12
	je	.LBB15_14
# BB#13:
	movq	%r12, %rdi
	callq	free
.LBB15_14:                              # %.preheader53
	movq	16(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	movq	24(%rsp), %r8           # 8-byte Reload
	jle	.LBB15_21
# BB#15:                                # %.lr.ph58.preheader
	movl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_16:                              # %.lr.ph58
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_19 Depth 2
	movq	(%r8,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB15_20
# BB#17:                                # %.preheader
                                        #   in Loop: Header=BB15_16 Depth=1
	movzwl	(%rdx), %esi
	testw	%si, %si
	js	.LBB15_20
# BB#18:                                # %.lr.ph
                                        #   in Loop: Header=BB15_16 Depth=1
	addq	$2, %rdx
	.p2align	4, 0x90
.LBB15_19:                              #   Parent Loop BB15_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	%si, %rsi
	movq	(%rbx,%rsi,8), %rdi
	leaq	2(%rdi), %rbp
	movq	%rbp, (%rbx,%rsi,8)
	movw	%cx, (%rdi)
	movzwl	(%rdx), %esi
	addq	$2, %rdx
	testw	%si, %si
	jns	.LBB15_19
.LBB15_20:                              # %.loopexit
                                        #   in Loop: Header=BB15_16 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jne	.LBB15_16
.LBB15_21:                              # %._crit_edge
	testq	%rbx, %rbx
	je	.LBB15_23
# BB#22:
	movq	%rbx, %rdi
	callq	free
.LBB15_23:
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	transpose, .Lfunc_end15-transpose
	.cfi_endproc

	.globl	traverse
	.p2align	4, 0x90
	.type	traverse,@function
traverse:                               # @traverse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi94:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi96:
	.cfi_def_cfa_offset 160
.Lcfi97:
	.cfi_offset %rbx, -56
.Lcfi98:
	.cfi_offset %r12, -48
.Lcfi99:
	.cfi_offset %r13, -40
.Lcfi100:
	.cfi_offset %r14, -32
.Lcfi101:
	.cfi_offset %r15, -24
.Lcfi102:
	.cfi_offset %rbp, -16
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movq	VERTICES(%rip), %rax
	movslq	top(%rip), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, top(%rip)
	movw	%di, 2(%rax,%rcx,2)
	movl	%edx, %ecx
	movq	INDEX(%rip), %r10
	movslq	%edi, %r12
	movw	%cx, (%r10,%r12,2)
	movq	F(%rip), %r8
	movslq	tokensetsize(%rip), %rbx
	movl	%ebx, %eax
	imull	%edi, %eax
	movslq	%eax, %rsi
	leaq	(%r8,%rsi,4), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	(%rax,%rbx,4), %r15
	movq	R(%rip), %rax
	movq	(%rax,%r12,8), %r14
	testq	%r14, %r14
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	je	.LBB16_33
# BB#1:                                 # %.preheader47
	movswl	(%r14), %ebp
	testl	%ebp, %ebp
	js	.LBB16_33
# BB#2:                                 # %.lr.ph55
	addq	$2, %r14
	testl	%ebx, %ebx
	jle	.LBB16_28
# BB#3:                                 # %.lr.ph55.split.us.preheader
	leaq	4(%r8,%rsi,4), %rax
	cmpq	%rax, %r15
	movq	%rax, 96(%rsp)          # 8-byte Spill
	cmovaq	%r15, %rax
	leaq	(,%rsi,4), %rcx
	subq	%rcx, %rax
	movq	%r8, %rcx
	notq	%rcx
	addq	%rax, %rcx
	shrq	$2, %rcx
	leaq	1(%rcx), %r13
	addq	%rsi, %rcx
	leaq	4(%r8,%rcx,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r13, %r8
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rax, %r8
	leaq	-8(%r8), %rcx
	shrq	$3, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%rax,%r8,4), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	jmp	.LBB16_4
	.p2align	4, 0x90
.LBB16_28:                              # %.lr.ph55.split
                                        # =>This Inner Loop Header: Depth=1
	movswq	%bp, %rbx
	movzwl	(%r10,%rbx,2), %edx
	testw	%dx, %dx
	jne	.LBB16_30
# BB#29:                                #   in Loop: Header=BB16_28 Depth=1
	movl	%ebp, %edi
	callq	traverse
	movq	INDEX(%rip), %r10
	movzwl	(%r10,%rbx,2), %edx
.LBB16_30:                              #   in Loop: Header=BB16_28 Depth=1
	movzwl	(%r10,%r12,2), %ecx
	cmpw	%dx, %cx
	jle	.LBB16_32
# BB#31:                                #   in Loop: Header=BB16_28 Depth=1
	movw	%dx, (%r10,%r12,2)
	movl	%edx, %ecx
.LBB16_32:                              # %.loopexit46
                                        #   in Loop: Header=BB16_28 Depth=1
	movswl	(%r14), %ebp
	addq	$2, %r14
	testl	%ebp, %ebp
	jns	.LBB16_28
	jmp	.LBB16_33
.LBB16_15:                              # %vector.body.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	cmpq	$0, 56(%rsp)            # 8-byte Folded Reload
	jne	.LBB16_17
# BB#16:                                # %vector.body.prol
                                        #   in Loop: Header=BB16_4 Depth=1
	movups	(%rdx), %xmm0
	movups	16(%rdx), %xmm1
	movq	8(%rsp), %rax           # 8-byte Reload
	movups	(%rax), %xmm2
	movups	16(%rax), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, (%rax)
	movups	%xmm3, 16(%rax)
	movl	$8, %eax
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	jne	.LBB16_18
	jmp	.LBB16_20
.LBB16_17:                              #   in Loop: Header=BB16_4 Depth=1
	xorl	%eax, %eax
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB16_20
.LBB16_18:                              # %vector.body.preheader.new
                                        #   in Loop: Header=BB16_4 Depth=1
	movq	%r8, %rbp
	subq	%rax, %rbp
	addq	%rax, %rsi
	leaq	48(%rdi,%rsi,4), %rsi
	movq	96(%rsp), %rdi          # 8-byte Reload
	leaq	44(%rdi,%rax,4), %rdi
	.p2align	4, 0x90
.LBB16_19:                              # %vector.body
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	-48(%rdi), %xmm2
	movups	-32(%rdi), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -48(%rdi)
	movups	%xmm3, -32(%rdi)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	-16(%rdi), %xmm2
	movups	(%rdi), %xmm3
	orps	%xmm0, %xmm2
	orps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdi)
	movups	%xmm3, (%rdi)
	addq	$64, %rsi
	addq	$64, %rdi
	addq	$-16, %rbp
	jne	.LBB16_19
.LBB16_20:                              # %middle.block
                                        #   in Loop: Header=BB16_4 Depth=1
	cmpq	%r8, %r13
	je	.LBB16_27
# BB#21:                                #   in Loop: Header=BB16_4 Depth=1
	leaq	(%rdx,%r8,4), %rdx
	movq	88(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB16_22
	.p2align	4, 0x90
.LBB16_4:                               # %.lr.ph55.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_19 Depth 2
                                        #     Child Loop BB16_24 Depth 2
                                        #     Child Loop BB16_26 Depth 2
	movswq	%bp, %rbx
	movzwl	(%r10,%rbx,2), %edx
	testw	%dx, %dx
	jne	.LBB16_6
# BB#5:                                 #   in Loop: Header=BB16_4 Depth=1
	movl	%ebp, %edi
	movq	%r8, %r12
	callq	traverse
	movq	%r12, %r8
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	INDEX(%rip), %r10
	movzwl	(%r10,%rbx,2), %edx
.LBB16_6:                               #   in Loop: Header=BB16_4 Depth=1
	movzwl	(%r10,%r12,2), %ecx
	cmpw	%dx, %cx
	jle	.LBB16_8
# BB#7:                                 #   in Loop: Header=BB16_4 Depth=1
	movw	%dx, (%r10,%r12,2)
	movl	%edx, %ecx
.LBB16_8:                               # %.lr.ph54.us
                                        #   in Loop: Header=BB16_4 Depth=1
	movslq	tokensetsize(%rip), %rax
	movslq	%ebp, %rsi
	imulq	%rax, %rsi
	cmpq	$8, %r13
	movq	F(%rip), %rdi
	leaq	(%rdi,%rsi,4), %rdx
	jae	.LBB16_10
# BB#9:                                 #   in Loop: Header=BB16_4 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB16_22
	.p2align	4, 0x90
.LBB16_10:                              # %min.iters.checked
                                        #   in Loop: Header=BB16_4 Depth=1
	testq	%r8, %r8
	je	.LBB16_14
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB16_4 Depth=1
	leaq	(%rdi,%r13,4), %rax
	leaq	(%rax,%rsi,4), %rax
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jae	.LBB16_15
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB16_4 Depth=1
	cmpq	80(%rsp), %rdx          # 8-byte Folded Reload
	jae	.LBB16_15
.LBB16_14:                              #   in Loop: Header=BB16_4 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB16_22:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	leaq	4(%rsi), %rbp
	cmpq	%rbp, %r15
	cmovaq	%r15, %rbp
	subq	%rsi, %rbp
	decq	%rbp
	movl	%ebp, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB16_25
# BB#23:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB16_4 Depth=1
	negq	%rdi
	.p2align	4, 0x90
.LBB16_24:                              # %scalar.ph.prol
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %eax
	addq	$4, %rdx
	orl	%eax, (%rsi)
	addq	$4, %rsi
	incq	%rdi
	jne	.LBB16_24
.LBB16_25:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB16_4 Depth=1
	cmpq	$12, %rbp
	jb	.LBB16_27
	.p2align	4, 0x90
.LBB16_26:                              # %scalar.ph
                                        #   Parent Loop BB16_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdx), %eax
	orl	%eax, (%rsi)
	movl	4(%rdx), %eax
	orl	%eax, 4(%rsi)
	movl	8(%rdx), %eax
	orl	%eax, 8(%rsi)
	movl	12(%rdx), %eax
	orl	%eax, 12(%rsi)
	addq	$16, %rsi
	addq	$16, %rdx
	cmpq	%r15, %rsi
	jb	.LBB16_26
.LBB16_27:                              # %..loopexit46_crit_edge.us
                                        #   in Loop: Header=BB16_4 Depth=1
	movswl	(%r14), %ebp
	addq	$2, %r14
	testl	%ebp, %ebp
	jns	.LBB16_4
.LBB16_33:                              # %.loopexit48
	movswl	%cx, %eax
	cmpl	72(%rsp), %eax          # 4-byte Folded Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	jne	.LBB16_61
# BB#34:                                # %.preheader
	movq	VERTICES(%rip), %r11
	movslq	top(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, top(%rip)
	movswq	(%r11,%rax,2), %rdi
	movzwl	infinity(%rip), %r8d
	movw	%r8w, (%r10,%rdi,2)
	cmpl	%edx, %edi
	je	.LBB16_61
# BB#35:                                # %.lr.ph51
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB16_59
# BB#36:                                # %.lr.ph51.split.us.preheader
	movq	F(%rip), %r9
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	4(%r14,%rsi,4), %rbp
	cmpq	%rbp, %r15
	movq	%rbp, %rax
	cmovaq	%r15, %rax
	leaq	(,%rsi,4), %rdx
	subq	%rdx, %rax
	movq	%r14, %rdx
	notq	%rdx
	addq	%rax, %rdx
	shrq	$2, %rdx
	leaq	4(%r9,%rdx,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	addq	%rdx, %rsi
	leaq	1(%rdx), %rbx
	leaq	4(%r14,%rsi,4), %r14
	movabsq	$9223372036854775800, %rax # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rbx, %rax
	leaq	-8(%rax), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	(%rdx,%rax,4), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	andl	$3, %esi
	leaq	16(%r9), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	negq	%rsi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	leaq	112(%r9), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB16_37
	.p2align	4, 0x90
.LBB16_59:                              # %.lr.ph51.split
                                        # =>This Inner Loop Header: Depth=1
	movswq	(%r11,%rcx,2), %rax
	decq	%rcx
	movw	%r8w, (%r10,%rax,2)
	cmpl	%edx, %eax
	jne	.LBB16_59
	jmp	.LBB16_60
.LBB16_44:                              # %vector.body100.preheader
                                        #   in Loop: Header=BB16_37 Depth=1
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB16_47
# BB#45:                                # %vector.body100.prol.preheader
                                        #   in Loop: Header=BB16_37 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rdx
	movq	48(%rsp), %rax          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB16_46:                              # %vector.body100.prol
                                        #   Parent Loop BB16_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-4(%rbp,%rsi,4), %xmm0
	movups	12(%rbp,%rsi,4), %xmm1
	movups	%xmm0, -16(%rdx,%rsi,4)
	movups	%xmm1, (%rdx,%rsi,4)
	addq	$8, %rsi
	incq	%rax
	jne	.LBB16_46
	jmp	.LBB16_48
.LBB16_47:                              #   in Loop: Header=BB16_37 Depth=1
	xorl	%esi, %esi
.LBB16_48:                              # %vector.body100.prol.loopexit
                                        #   in Loop: Header=BB16_37 Depth=1
	cmpq	$24, 72(%rsp)           # 8-byte Folded Reload
	jb	.LBB16_51
# BB#49:                                # %vector.body100.preheader.new
                                        #   in Loop: Header=BB16_37 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	subq	%rsi, %rdx
	addq	%rsi, %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rdi
	leaq	108(%rbp,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB16_50:                              # %vector.body100
                                        #   Parent Loop BB16_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdi)
	movups	%xmm1, -96(%rdi)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdi)
	movups	%xmm1, -64(%rdi)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdi)
	movups	%xmm1, -32(%rdi)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdi)
	movups	%xmm1, (%rdi)
	subq	$-128, %rdi
	subq	$-128, %rsi
	addq	$-32, %rdx
	jne	.LBB16_50
.LBB16_51:                              # %middle.block101
                                        #   in Loop: Header=BB16_37 Depth=1
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	je	.LBB16_58
# BB#52:                                #   in Loop: Header=BB16_37 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%r13,%rax,4), %r13
	movq	40(%rsp), %r12          # 8-byte Reload
	jmp	.LBB16_53
	.p2align	4, 0x90
.LBB16_37:                              # %.lr.ph51.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_46 Depth 2
                                        #     Child Loop BB16_50 Depth 2
                                        #     Child Loop BB16_55 Depth 2
                                        #     Child Loop BB16_57 Depth 2
	movslq	tokensetsize(%rip), %rax
	movslq	%edi, %rdi
	imulq	%rax, %rdi
	cmpq	$8, %rbx
	leaq	(%r9,%rdi,4), %r13
	jae	.LBB16_39
# BB#38:                                #   in Loop: Header=BB16_37 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB16_53
	.p2align	4, 0x90
.LBB16_39:                              # %min.iters.checked110
                                        #   in Loop: Header=BB16_37 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	je	.LBB16_43
# BB#40:                                # %vector.memcheck130
                                        #   in Loop: Header=BB16_37 Depth=1
	cmpq	%r14, %r13
	jae	.LBB16_44
# BB#41:                                # %vector.memcheck130
                                        #   in Loop: Header=BB16_37 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdi,4), %rax
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	jae	.LBB16_44
.LBB16_43:                              #   in Loop: Header=BB16_37 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB16_53:                              # %scalar.ph102.preheader
                                        #   in Loop: Header=BB16_37 Depth=1
	leaq	4(%r12), %rdi
	cmpq	%rdi, %r15
	cmovaq	%r15, %rdi
	subq	%r12, %rdi
	decq	%rdi
	movl	%edi, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB16_56
# BB#54:                                # %scalar.ph102.prol.preheader
                                        #   in Loop: Header=BB16_37 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB16_55:                              # %scalar.ph102.prol
                                        #   Parent Loop BB16_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12), %eax
	addq	$4, %r12
	movl	%eax, (%r13)
	addq	$4, %r13
	incq	%rdx
	jne	.LBB16_55
.LBB16_56:                              # %scalar.ph102.prol.loopexit
                                        #   in Loop: Header=BB16_37 Depth=1
	cmpq	$28, %rdi
	jb	.LBB16_58
	.p2align	4, 0x90
.LBB16_57:                              # %scalar.ph102
                                        #   Parent Loop BB16_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12), %eax
	movl	%eax, (%r13)
	movl	4(%r12), %eax
	movl	%eax, 4(%r13)
	movl	8(%r12), %eax
	movl	%eax, 8(%r13)
	movl	12(%r12), %eax
	movl	%eax, 12(%r13)
	movl	16(%r12), %eax
	movl	%eax, 16(%r13)
	movl	20(%r12), %eax
	movl	%eax, 20(%r13)
	movl	24(%r12), %eax
	movl	%eax, 24(%r13)
	movl	28(%r12), %eax
	movl	%eax, 28(%r13)
	addq	$32, %r12
	addq	$32, %r13
	cmpq	%r15, %r12
	jb	.LBB16_57
.LBB16_58:                              # %..loopexit_crit_edge.us
                                        #   in Loop: Header=BB16_37 Depth=1
	movswq	(%r11,%rcx,2), %rdi
	decq	%rcx
	movw	%r8w, (%r10,%rdi,2)
	cmpl	28(%rsp), %edi          # 4-byte Folded Reload
	jne	.LBB16_37
.LBB16_60:                              # %..loopexit45_crit_edge
	movl	%ecx, top(%rip)
.LBB16_61:                              # %.loopexit45
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	traverse, .Lfunc_end16-traverse
	.cfi_endproc

	.type	tokensetsize,@object    # @tokensetsize
	.comm	tokensetsize,4,4
	.type	state_table,@object     # @state_table
	.comm	state_table,8,8
	.type	accessing_symbol,@object # @accessing_symbol
	.comm	accessing_symbol,8,8
	.type	shift_table,@object     # @shift_table
	.comm	shift_table,8,8
	.type	reduction_table,@object # @reduction_table
	.comm	reduction_table,8,8
	.type	maxrhs,@object          # @maxrhs
	.local	maxrhs
	.comm	maxrhs,4,4
	.type	consistent,@object      # @consistent
	.comm	consistent,8,8
	.type	lookaheads,@object      # @lookaheads
	.comm	lookaheads,8,8
	.type	LA,@object              # @LA
	.comm	LA,8,8
	.type	LAruleno,@object        # @LAruleno
	.comm	LAruleno,8,8
	.type	lookback,@object        # @lookback
	.local	lookback
	.comm	lookback,8,8
	.type	goto_map,@object        # @goto_map
	.comm	goto_map,8,8
	.type	ngotos,@object          # @ngotos
	.local	ngotos
	.comm	ngotos,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"gotos"
	.size	.L.str, 6

	.type	from_state,@object      # @from_state
	.comm	from_state,8,8
	.type	to_state,@object        # @to_state
	.comm	to_state,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"map_goto"
	.size	.L.str.1, 9

	.type	F,@object               # @F
	.local	F
	.comm	F,8,8
	.type	includes,@object        # @includes
	.local	includes
	.comm	includes,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"add_lookback_edge"
	.size	.L.str.2, 18

	.type	infinity,@object        # @infinity
	.local	infinity
	.comm	infinity,4,4
	.type	INDEX,@object           # @INDEX
	.local	INDEX
	.comm	INDEX,8,8
	.type	VERTICES,@object        # @VERTICES
	.local	VERTICES
	.comm	VERTICES,8,8
	.type	top,@object             # @top
	.local	top
	.comm	top,4,4
	.type	R,@object               # @R
	.local	R
	.comm	R,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
