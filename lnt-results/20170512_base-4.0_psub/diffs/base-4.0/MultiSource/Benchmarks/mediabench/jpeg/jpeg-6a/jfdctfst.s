	.text
	.file	"jfdctfst.bc"
	.globl	jpeg_fdct_ifast
	.p2align	4, 0x90
	.type	jpeg_fdct_ifast,@function
jpeg_fdct_ifast:                        # @jpeg_fdct_ifast
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
.Lcfi4:
	.cfi_offset %rbx, -40
.Lcfi5:
	.cfi_offset %r14, -32
.Lcfi6:
	.cfi_offset %r15, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movl	$8, %r8d
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	28(%rcx), %edx
	movl	(%rcx), %r11d
	movl	4(%rcx), %r10d
	leal	(%rdx,%r11), %eax
	subl	%edx, %r11d
	movl	24(%rcx), %esi
	leal	(%rsi,%r10), %edx
	subl	%esi, %r10d
	movl	8(%rcx), %r9d
	movl	20(%rcx), %esi
	leal	(%rsi,%r9), %r14d
	subl	%esi, %r9d
	movl	12(%rcx), %esi
	movl	16(%rcx), %ebx
	leal	(%rbx,%rsi), %r15d
	subl	%ebx, %esi
	leal	(%r15,%rax), %ebx
	subl	%r15d, %eax
	leal	(%r14,%rdx), %r15d
	subl	%r14d, %edx
	leal	(%rbx,%r15), %ebp
	movl	%ebp, (%rcx)
	subl	%r15d, %ebx
	movl	%ebx, 16(%rcx)
	addl	%eax, %edx
	movslq	%edx, %rdx
	imulq	$181, %rdx, %rdx
	shrq	$8, %rdx
	leal	(%rdx,%rax), %ebx
	movl	%ebx, 8(%rcx)
	subl	%edx, %eax
	movl	%eax, 24(%rcx)
	addl	%r9d, %esi
	addl	%r10d, %r9d
	addl	%r11d, %r10d
	movslq	%esi, %rax
	subl	%r10d, %esi
	movslq	%esi, %rdx
	imulq	$98, %rdx, %rdx
	shrq	$8, %rdx
	imulq	$139, %rax, %rax
	shrq	$8, %rax
	addl	%edx, %eax
	movslq	%r10d, %rsi
	imulq	$334, %rsi, %rsi        # imm = 0x14E
	shrq	$8, %rsi
	addl	%edx, %esi
	movslq	%r9d, %rdx
	imulq	$181, %rdx, %rdx
	shrq	$8, %rdx
	leal	(%rdx,%r11), %ebp
	subl	%edx, %r11d
	leal	(%rax,%r11), %edx
	movl	%edx, 20(%rcx)
	subl	%eax, %r11d
	movl	%r11d, 12(%rcx)
	leal	(%rsi,%rbp), %eax
	movl	%eax, 4(%rcx)
	subl	%esi, %ebp
	movl	%ebp, 28(%rcx)
	addq	$32, %rcx
	decl	%r8d
	jg	.LBB0_1
# BB#2:                                 # %.preheader.preheader
	movl	$8, %r8d
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	224(%rdi), %edx
	movl	(%rdi), %ecx
	movl	32(%rdi), %r10d
	leal	(%rdx,%rcx), %eax
	subl	%edx, %ecx
	movl	192(%rdi), %esi
	leal	(%rsi,%r10), %edx
	subl	%esi, %r10d
	movl	64(%rdi), %r9d
	movl	160(%rdi), %esi
	leal	(%rsi,%r9), %r11d
	subl	%esi, %r9d
	movl	96(%rdi), %esi
	movl	128(%rdi), %ebx
	leal	(%rbx,%rsi), %ebp
	subl	%ebx, %esi
	leal	(%rbp,%rax), %ebx
	subl	%ebp, %eax
	leal	(%r11,%rdx), %r14d
	subl	%r11d, %edx
	leal	(%rbx,%r14), %ebp
	movl	%ebp, (%rdi)
	subl	%r14d, %ebx
	movl	%ebx, 128(%rdi)
	addl	%eax, %edx
	movslq	%edx, %rdx
	imulq	$181, %rdx, %rdx
	shrq	$8, %rdx
	leal	(%rdx,%rax), %ebp
	movl	%ebp, 64(%rdi)
	subl	%edx, %eax
	movl	%eax, 192(%rdi)
	addl	%r9d, %esi
	addl	%r10d, %r9d
	addl	%ecx, %r10d
	movslq	%esi, %rax
	subl	%r10d, %esi
	movslq	%esi, %rdx
	imulq	$98, %rdx, %rdx
	shrq	$8, %rdx
	imulq	$139, %rax, %rax
	shrq	$8, %rax
	addl	%edx, %eax
	movslq	%r10d, %rsi
	imulq	$334, %rsi, %rsi        # imm = 0x14E
	shrq	$8, %rsi
	addl	%edx, %esi
	movslq	%r9d, %rdx
	imulq	$181, %rdx, %rdx
	shrq	$8, %rdx
	leal	(%rdx,%rcx), %ebp
	subl	%edx, %ecx
	leal	(%rax,%rcx), %edx
	movl	%edx, 160(%rdi)
	subl	%eax, %ecx
	movl	%ecx, 96(%rdi)
	leal	(%rsi,%rbp), %eax
	movl	%eax, 32(%rdi)
	subl	%esi, %ebp
	movl	%ebp, 224(%rdi)
	addq	$4, %rdi
	decl	%r8d
	jg	.LBB0_3
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_fdct_ifast, .Lfunc_end0-jpeg_fdct_ifast
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
