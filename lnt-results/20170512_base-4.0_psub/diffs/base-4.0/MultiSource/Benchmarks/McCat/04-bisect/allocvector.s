	.text
	.file	"allocvector.bc"
	.globl	allocvector
	.p2align	4, 0x90
	.type	allocvector,@function
allocvector:                            # @allocvector
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	memset
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end0:
	.size	allocvector, .Lfunc_end0-allocvector
	.cfi_endproc

	.globl	dallocvector
	.p2align	4, 0x90
	.type	dallocvector,@function
dallocvector:                           # @dallocvector
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movslq	%edi, %rbx
	shlq	$3, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB1_2
# BB#1:                                 # %allocvector.exit
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	%r15, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end1:
	.size	dallocvector, .Lfunc_end1-dallocvector
	.cfi_endproc

	.globl	sallocvector
	.p2align	4, 0x90
	.type	sallocvector,@function
sallocvector:                           # @sallocvector
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movslq	%edi, %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_2
# BB#1:                                 # %allocvector.exit
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	%r15, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB2_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	sallocvector, .Lfunc_end2-sallocvector
	.cfi_endproc

	.globl	iallocvector
	.p2align	4, 0x90
	.type	iallocvector,@function
iallocvector:                           # @iallocvector
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movslq	%edi, %rbx
	shlq	$2, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_2
# BB#1:                                 # %allocvector.exit
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	%r15, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end3:
	.size	iallocvector, .Lfunc_end3-iallocvector
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error: couldn't allocate V in allocvector.\n"
	.size	.L.str, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
