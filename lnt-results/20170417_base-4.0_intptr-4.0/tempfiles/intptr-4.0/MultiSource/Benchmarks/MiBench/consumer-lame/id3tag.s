	.text
	.file	"id3tag.bc"
	.globl	id3_inittag
	.p2align	4, 0x90
	.type	id3_inittag,@function
id3_inittag:                            # @id3_inittag
	.cfi_startproc
# BB#0:
	movb	$0, 8(%rdi)
	movb	$0, 39(%rdi)
	movb	$0, 70(%rdi)
	movb	$0, 101(%rdi)
	movb	$0, 106(%rdi)
	movb	$-1, 265(%rdi)
	movb	$0, 266(%rdi)
	movl	$0, 4(%rdi)
	retq
.Lfunc_end0:
	.size	id3_inittag, .Lfunc_end0-id3_inittag
	.cfi_endproc

	.globl	id3_buildtag
	.p2align	4, 0x90
	.type	id3_buildtag,@function
id3_buildtag:                           # @id3_buildtag
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	leaq	137(%rbx), %r14
	movl	$4669780, 137(%rbx)     # imm = 0x474154
	leaq	8(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movslq	%eax, %rcx
	leaq	8(%rbx,%rcx), %rdi
	cmpl	$29, %ecx
	jg	.LBB1_2
# BB#1:                                 # %.lr.ph.preheader.i
	movl	$29, %edx
	subl	%eax, %edx
	leaq	1(%rdx,%rcx), %r12
	incq	%rdx
	movl	$32, %esi
	callq	memset
	leaq	8(%rbx,%r12), %rdi
.LBB1_2:                                # %id3_pad.exit
	movb	$0, (%rdi)
	movl	$30, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strncat
	leaq	39(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movslq	%eax, %rcx
	leaq	39(%rbx,%rcx), %rdi
	cmpl	$29, %ecx
	jg	.LBB1_4
# BB#3:                                 # %.lr.ph.preheader.i26
	movl	$29, %edx
	subl	%eax, %edx
	leaq	1(%rdx,%rcx), %r12
	incq	%rdx
	movl	$32, %esi
	callq	memset
	leaq	39(%rbx,%r12), %rdi
.LBB1_4:                                # %id3_pad.exit28
	movb	$0, (%rdi)
	movl	$30, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strncat
	leaq	70(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movslq	%eax, %rcx
	leaq	70(%rbx,%rcx), %rdi
	cmpl	$29, %ecx
	jg	.LBB1_6
# BB#5:                                 # %.lr.ph.preheader.i31
	movl	$29, %edx
	subl	%eax, %edx
	leaq	1(%rdx,%rcx), %r12
	incq	%rdx
	movl	$32, %esi
	callq	memset
	leaq	70(%rbx,%r12), %rdi
.LBB1_6:                                # %id3_pad.exit33
	movb	$0, (%rdi)
	movl	$30, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strncat
	leaq	101(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movslq	%eax, %rcx
	leaq	101(%rbx,%rcx), %rdi
	cmpl	$3, %ecx
	jg	.LBB1_8
# BB#7:                                 # %.lr.ph.preheader.i36
	movl	$3, %edx
	subl	%eax, %edx
	leaq	1(%rdx,%rcx), %r12
	incq	%rdx
	movl	$32, %esi
	callq	memset
	leaq	101(%rbx,%r12), %rdi
.LBB1_8:                                # %id3_pad.exit38
	movb	$0, (%rdi)
	movl	$4, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strncat
	leaq	106(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movslq	%eax, %rcx
	leaq	106(%rbx,%rcx), %rdi
	cmpl	$29, %ecx
	jg	.LBB1_10
# BB#9:                                 # %.lr.ph.preheader.i41
	movl	$29, %edx
	subl	%eax, %edx
	leaq	1(%rdx,%rcx), %r12
	incq	%rdx
	movl	$32, %esi
	callq	memset
	leaq	106(%rbx,%r12), %rdi
.LBB1_10:                               # %id3_pad.exit43
	movb	$0, (%rdi)
	movl	$30, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strncat
	leaq	265(%rbx), %r15
	movq	%r15, %rdi
	callq	strlen
	movslq	%eax, %rcx
	leaq	265(%rbx,%rcx), %rdi
	testl	%ecx, %ecx
	jg	.LBB1_12
# BB#11:                                # %.lr.ph.preheader.i46
	negl	%eax
	leaq	1(%rax,%rcx), %r12
	incq	%rax
	movl	$32, %esi
	movq	%rax, %rdx
	callq	memset
	leaq	265(%rbx,%r12), %rdi
.LBB1_12:                               # %id3_pad.exit48
	movb	$0, (%rdi)
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	strncat
	movb	266(%rbx), %al
	testb	%al, %al
	je	.LBB1_14
# BB#13:
	movb	$0, 262(%rbx)
	movb	%al, 263(%rbx)
.LBB1_14:
	movl	$1, 4(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	id3_buildtag, .Lfunc_end1-id3_buildtag
	.cfi_endproc

	.globl	id3_writetag
	.p2align	4, 0x90
	.type	id3_writetag,@function
id3_writetag:                           # @id3_writetag
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movl	$-1, %r14d
	cmpl	$0, 4(%rbx)
	je	.LBB2_3
# BB#1:
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_3
# BB#2:
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r15, %rdi
	callq	fseek
	addq	$137, %rbx
	movl	$1, %esi
	movl	$128, %edx
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	fwrite
	movq	%r15, %rdi
	callq	fclose
.LBB2_3:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	id3_writetag, .Lfunc_end2-id3_writetag
	.cfi_endproc

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"rb+"
	.size	.L.str.2, 4

	.type	genre_last,@object      # @genre_last
	.data
	.globl	genre_last
	.p2align	2
genre_last:
	.long	147                     # 0x93
	.size	genre_last, 4

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"Blues"
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Classic Rock"
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Country"
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Dance"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Disco"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Funk"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Grunge"
	.size	.L.str.9, 7

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Hip-Hop"
	.size	.L.str.10, 8

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Jazz"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"Metal"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"New Age"
	.size	.L.str.13, 8

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Oldies"
	.size	.L.str.14, 7

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Other"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Pop"
	.size	.L.str.16, 4

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"R&B"
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Rap"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Reggae"
	.size	.L.str.19, 7

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Rock"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"Techno"
	.size	.L.str.21, 7

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Industrial"
	.size	.L.str.22, 11

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Alternative"
	.size	.L.str.23, 12

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Ska"
	.size	.L.str.24, 4

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Death Metal"
	.size	.L.str.25, 12

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Pranks"
	.size	.L.str.26, 7

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Soundtrack"
	.size	.L.str.27, 11

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Euro-Techno"
	.size	.L.str.28, 12

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Ambient"
	.size	.L.str.29, 8

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Trip-Hop"
	.size	.L.str.30, 9

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Vocal"
	.size	.L.str.31, 6

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"Jazz+Funk"
	.size	.L.str.32, 10

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"Fusion"
	.size	.L.str.33, 7

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"Trance"
	.size	.L.str.34, 7

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"Classical"
	.size	.L.str.35, 10

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Instrumental"
	.size	.L.str.36, 13

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Acid"
	.size	.L.str.37, 5

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"House"
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Game"
	.size	.L.str.39, 5

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"Sound Clip"
	.size	.L.str.40, 11

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"Gospel"
	.size	.L.str.41, 7

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Noise"
	.size	.L.str.42, 6

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"AlternRock"
	.size	.L.str.43, 11

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"Bass"
	.size	.L.str.44, 5

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Soul"
	.size	.L.str.45, 5

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"Punk"
	.size	.L.str.46, 5

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"Space"
	.size	.L.str.47, 6

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"Meditative"
	.size	.L.str.48, 11

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"Instrumental Pop"
	.size	.L.str.49, 17

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"Instrumental Rock"
	.size	.L.str.50, 18

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"Ethnic"
	.size	.L.str.51, 7

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"Gothic"
	.size	.L.str.52, 7

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"Darkwave"
	.size	.L.str.53, 9

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"Techno-Industrial"
	.size	.L.str.54, 18

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"Electronic"
	.size	.L.str.55, 11

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"Pop-Folk"
	.size	.L.str.56, 9

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"Eurodance"
	.size	.L.str.57, 10

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"Dream"
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"Southern Rock"
	.size	.L.str.59, 14

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"Comedy"
	.size	.L.str.60, 7

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"Cult"
	.size	.L.str.61, 5

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"Gangsta"
	.size	.L.str.62, 8

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"Top 40"
	.size	.L.str.63, 7

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"Christian Rap"
	.size	.L.str.64, 14

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"Pop/Funk"
	.size	.L.str.65, 9

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"Jungle"
	.size	.L.str.66, 7

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"Native American"
	.size	.L.str.67, 16

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"Cabaret"
	.size	.L.str.68, 8

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"New Wave"
	.size	.L.str.69, 9

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"Psychadelic"
	.size	.L.str.70, 12

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Rave"
	.size	.L.str.71, 5

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"Showtunes"
	.size	.L.str.72, 10

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"Trailer"
	.size	.L.str.73, 8

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"Lo-Fi"
	.size	.L.str.74, 6

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"Tribal"
	.size	.L.str.75, 7

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"Acid Punk"
	.size	.L.str.76, 10

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"Acid Jazz"
	.size	.L.str.77, 10

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"Polka"
	.size	.L.str.78, 6

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"Retro"
	.size	.L.str.79, 6

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"Musical"
	.size	.L.str.80, 8

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"Rock & Roll"
	.size	.L.str.81, 12

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"Hard Rock"
	.size	.L.str.82, 10

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"Folk"
	.size	.L.str.83, 5

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"Folk/Rock"
	.size	.L.str.84, 10

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"National Folk"
	.size	.L.str.85, 14

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"Swing"
	.size	.L.str.86, 6

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"Fast-Fusion"
	.size	.L.str.87, 12

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"Bebob"
	.size	.L.str.88, 6

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"Latin"
	.size	.L.str.89, 6

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"Revival"
	.size	.L.str.90, 8

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"Celtic"
	.size	.L.str.91, 7

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"Bluegrass"
	.size	.L.str.92, 10

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"Avantgarde"
	.size	.L.str.93, 11

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"Gothic Rock"
	.size	.L.str.94, 12

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"Progressive Rock"
	.size	.L.str.95, 17

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"Psychedelic Rock"
	.size	.L.str.96, 17

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"Symphonic Rock"
	.size	.L.str.97, 15

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"Slow Rock"
	.size	.L.str.98, 10

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"Big Band"
	.size	.L.str.99, 9

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"Chorus"
	.size	.L.str.100, 7

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"Easy Listening"
	.size	.L.str.101, 15

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"Acoustic"
	.size	.L.str.102, 9

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"Humour"
	.size	.L.str.103, 7

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"Speech"
	.size	.L.str.104, 7

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"Chanson"
	.size	.L.str.105, 8

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"Opera"
	.size	.L.str.106, 6

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"Chamber Music"
	.size	.L.str.107, 14

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"Sonata"
	.size	.L.str.108, 7

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"Symphony"
	.size	.L.str.109, 9

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"Booty Bass"
	.size	.L.str.110, 11

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"Primus"
	.size	.L.str.111, 7

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"Porn Groove"
	.size	.L.str.112, 12

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"Satire"
	.size	.L.str.113, 7

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"Slow Jam"
	.size	.L.str.114, 9

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"Club"
	.size	.L.str.115, 5

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"Tango"
	.size	.L.str.116, 6

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"Samba"
	.size	.L.str.117, 6

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"Folklore"
	.size	.L.str.118, 9

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"Ballad"
	.size	.L.str.119, 7

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"Power Ballad"
	.size	.L.str.120, 13

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"Rhythmic Soul"
	.size	.L.str.121, 14

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"Freestyle"
	.size	.L.str.122, 10

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"Duet"
	.size	.L.str.123, 5

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"Punk Rock"
	.size	.L.str.124, 10

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"Drum Solo"
	.size	.L.str.125, 10

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"A capella"
	.size	.L.str.126, 10

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"Euro-House"
	.size	.L.str.127, 11

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"Dance Hall"
	.size	.L.str.128, 11

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"Goa"
	.size	.L.str.129, 4

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"Drum & Bass"
	.size	.L.str.130, 12

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"Club House"
	.size	.L.str.131, 11

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"Hardcore"
	.size	.L.str.132, 9

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"Terror"
	.size	.L.str.133, 7

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"Indie"
	.size	.L.str.134, 6

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"BritPop"
	.size	.L.str.135, 8

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"NegerPunk"
	.size	.L.str.136, 10

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"Polsk Punk"
	.size	.L.str.137, 11

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"Beat"
	.size	.L.str.138, 5

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"Christian Gangsta"
	.size	.L.str.139, 18

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"Heavy Metal"
	.size	.L.str.140, 12

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"Black Metal"
	.size	.L.str.141, 12

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"Crossover"
	.size	.L.str.142, 10

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"Contemporary C"
	.size	.L.str.143, 15

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"Christian Rock"
	.size	.L.str.144, 15

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"Merengue"
	.size	.L.str.145, 9

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"Salsa"
	.size	.L.str.146, 6

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"Thrash Metal"
	.size	.L.str.147, 13

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	"Anime"
	.size	.L.str.148, 6

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"JPop"
	.size	.L.str.149, 5

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	"SynthPop"
	.size	.L.str.150, 9

	.type	genre_list,@object      # @genre_list
	.data
	.globl	genre_list
	.p2align	4
genre_list:
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	.L.str.47
	.quad	.L.str.48
	.quad	.L.str.49
	.quad	.L.str.50
	.quad	.L.str.51
	.quad	.L.str.52
	.quad	.L.str.53
	.quad	.L.str.54
	.quad	.L.str.55
	.quad	.L.str.56
	.quad	.L.str.57
	.quad	.L.str.58
	.quad	.L.str.59
	.quad	.L.str.60
	.quad	.L.str.61
	.quad	.L.str.62
	.quad	.L.str.63
	.quad	.L.str.64
	.quad	.L.str.65
	.quad	.L.str.66
	.quad	.L.str.67
	.quad	.L.str.68
	.quad	.L.str.69
	.quad	.L.str.70
	.quad	.L.str.71
	.quad	.L.str.72
	.quad	.L.str.73
	.quad	.L.str.74
	.quad	.L.str.75
	.quad	.L.str.76
	.quad	.L.str.77
	.quad	.L.str.78
	.quad	.L.str.79
	.quad	.L.str.80
	.quad	.L.str.81
	.quad	.L.str.82
	.quad	.L.str.83
	.quad	.L.str.84
	.quad	.L.str.85
	.quad	.L.str.86
	.quad	.L.str.87
	.quad	.L.str.88
	.quad	.L.str.89
	.quad	.L.str.90
	.quad	.L.str.91
	.quad	.L.str.92
	.quad	.L.str.93
	.quad	.L.str.94
	.quad	.L.str.95
	.quad	.L.str.96
	.quad	.L.str.97
	.quad	.L.str.98
	.quad	.L.str.99
	.quad	.L.str.100
	.quad	.L.str.101
	.quad	.L.str.102
	.quad	.L.str.103
	.quad	.L.str.104
	.quad	.L.str.105
	.quad	.L.str.106
	.quad	.L.str.107
	.quad	.L.str.108
	.quad	.L.str.109
	.quad	.L.str.110
	.quad	.L.str.111
	.quad	.L.str.112
	.quad	.L.str.113
	.quad	.L.str.114
	.quad	.L.str.115
	.quad	.L.str.116
	.quad	.L.str.117
	.quad	.L.str.118
	.quad	.L.str.119
	.quad	.L.str.120
	.quad	.L.str.121
	.quad	.L.str.122
	.quad	.L.str.123
	.quad	.L.str.124
	.quad	.L.str.125
	.quad	.L.str.126
	.quad	.L.str.127
	.quad	.L.str.128
	.quad	.L.str.129
	.quad	.L.str.130
	.quad	.L.str.131
	.quad	.L.str.132
	.quad	.L.str.133
	.quad	.L.str.134
	.quad	.L.str.135
	.quad	.L.str.136
	.quad	.L.str.137
	.quad	.L.str.138
	.quad	.L.str.139
	.quad	.L.str.140
	.quad	.L.str.141
	.quad	.L.str.142
	.quad	.L.str.143
	.quad	.L.str.144
	.quad	.L.str.145
	.quad	.L.str.146
	.quad	.L.str.147
	.quad	.L.str.148
	.quad	.L.str.149
	.quad	.L.str.150
	.size	genre_list, 1184

	.type	id3tag,@object          # @id3tag
	.comm	id3tag,268,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
