	.text
	.file	"dominate.bc"
	.globl	sm_row_dominance
	.p2align	4, 0x90
	.type	sm_row_dominance,@function
sm_row_dominance:                       # @sm_row_dominance
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r14, -32
.Lcfi8:
	.cfi_offset %r15, -24
.Lcfi9:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	48(%r12), %r14d
	movq	32(%r12), %r15
	testq	%r15, %r15
	movl	%r14d, %eax
	je	.LBB0_22
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_16 Depth 2
	movq	16(%r15), %rcx
	movslq	4(%rcx), %rax
	testq	%rax, %rax
	js	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	cmpl	24(%r12), %eax
	jge	.LBB0_5
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movq	16(%r12), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_6:                                #   in Loop: Header=BB0_7 Depth=2
	movl	4(%rdx), %esi
	cmpl	4(%rax), %esi
	cmovlq	%rdx, %rax
.LBB0_7:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	24(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB0_13
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_7 Depth=2
	movslq	4(%rcx), %rdx
	testq	%rdx, %rdx
	js	.LBB0_12
# BB#9:                                 #   in Loop: Header=BB0_7 Depth=2
	cmpl	24(%r12), %edx
	jge	.LBB0_12
# BB#10:                                #   in Loop: Header=BB0_7 Depth=2
	movq	16(%r12), %rsi
	movq	(%rsi,%rdx,8), %rdx
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_7 Depth=2
	xorl	%edx, %edx
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_13:                               # %._crit_edge
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_16
	jmp	.LBB0_20
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_16 Depth=2
	movl	(%rbp), %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sm_delrow
.LBB0_15:                               # %._crit_edge
                                        #   in Loop: Header=BB0_16 Depth=2
	testq	%rbx, %rbx
	je	.LBB0_20
.LBB0_16:                               #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rbx), %rax
	movq	8(%rbx), %rbx
	movq	(%r12), %rcx
	movq	(%rcx,%rax,8), %rbp
	movl	4(%r15), %eax
	cmpl	%eax, 4(%rbp)
	jg	.LBB0_19
# BB#17:                                #   in Loop: Header=BB0_16 Depth=2
	jne	.LBB0_15
# BB#18:                                #   in Loop: Header=BB0_16 Depth=2
	movl	(%rbp), %eax
	cmpl	(%r15), %eax
	jle	.LBB0_15
	.p2align	4, 0x90
.LBB0_19:                               #   in Loop: Header=BB0_16 Depth=2
	xorl	%eax, %eax
	movq	%r15, %rdi
	movq	%rbp, %rsi
	callq	sm_row_contains
	testl	%eax, %eax
	jne	.LBB0_14
	jmp	.LBB0_15
	.p2align	4, 0x90
.LBB0_20:                               # %._crit_edge63
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	32(%r15), %r15
	testq	%r15, %r15
	jne	.LBB0_1
# BB#21:                                # %._crit_edge69.loopexit
	movl	48(%r12), %eax
.LBB0_22:                               # %._crit_edge69
	subl	%eax, %r14d
	movl	%r14d, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	sm_row_dominance, .Lfunc_end0-sm_row_dominance
	.cfi_endproc

	.globl	sm_col_dominance
	.p2align	4, 0x90
	.type	sm_col_dominance,@function
sm_col_dominance:                       # @sm_col_dominance
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	72(%rbx), %r14d
	movq	56(%rbx), %r12
	testq	%r12, %r12
	movl	%r14d, %eax
	je	.LBB1_34
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
                                        #     Child Loop BB1_15 Depth 2
                                        #     Child Loop BB1_21 Depth 2
	movq	%r12, %r13
	movq	16(%r13), %rcx
	movq	32(%r13), %r12
	movslq	(%rcx), %rax
	testq	%rax, %rax
	js	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpl	8(%rbx), %eax
	jge	.LBB1_5
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	(%rbx), %rdx
	movq	(%rdx,%rax,8), %rax
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_4:                                #   in Loop: Header=BB1_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_5:                                #   in Loop: Header=BB1_1 Depth=1
	xorl	%eax, %eax
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_7 Depth=2
	movl	4(%rdx), %esi
	cmpl	4(%rax), %esi
	cmovlq	%rdx, %rax
.LBB1_7:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_13
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_7 Depth=2
	movslq	(%rcx), %rdx
	testq	%rdx, %rdx
	js	.LBB1_12
# BB#9:                                 #   in Loop: Header=BB1_7 Depth=2
	cmpl	8(%rbx), %edx
	jge	.LBB1_12
# BB#10:                                #   in Loop: Header=BB1_7 Depth=2
	movq	(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rdx
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_12:                               #   in Loop: Header=BB1_7 Depth=2
	xorl	%edx, %edx
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_13:                               # %._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB1_32
# BB#14:                                # %.lr.ph68
                                        #   in Loop: Header=BB1_1 Depth=1
	testq	%r15, %r15
	je	.LBB1_21
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph68.split
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movslq	4(%rbp), %rcx
	movq	(%rax,%rcx,8), %rsi
	movslq	(%rsi), %rax
	movl	(%r15,%rax,4), %edx
	movslq	(%r13), %rcx
	cmpl	(%r15,%rcx,4), %edx
	jg	.LBB1_20
# BB#16:                                #   in Loop: Header=BB1_15 Depth=2
	movl	4(%r13), %edx
	cmpl	%edx, 4(%rsi)
	jg	.LBB1_19
# BB#17:                                #   in Loop: Header=BB1_15 Depth=2
	jne	.LBB1_20
# BB#18:                                #   in Loop: Header=BB1_15 Depth=2
	cmpl	%ecx, %eax
	jle	.LBB1_20
.LBB1_19:                               #   in Loop: Header=BB1_15 Depth=2
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sm_col_contains
	testl	%eax, %eax
	jne	.LBB1_31
.LBB1_20:                               #   in Loop: Header=BB1_15 Depth=2
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_15
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_21:                               # %.lr.ph68.split.us
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	4(%rbp), %rax
	testq	%rax, %rax
	js	.LBB1_25
# BB#22:                                #   in Loop: Header=BB1_21 Depth=2
	cmpl	24(%rbx), %eax
	jge	.LBB1_25
# BB#23:                                #   in Loop: Header=BB1_21 Depth=2
	movq	16(%rbx), %rcx
	movq	(%rcx,%rax,8), %rsi
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_25:                               #   in Loop: Header=BB1_21 Depth=2
	xorl	%esi, %esi
.LBB1_26:                               #   in Loop: Header=BB1_21 Depth=2
	movl	4(%r13), %eax
	cmpl	%eax, 4(%rsi)
	jg	.LBB1_29
# BB#27:                                #   in Loop: Header=BB1_21 Depth=2
	jne	.LBB1_30
# BB#28:                                #   in Loop: Header=BB1_21 Depth=2
	movl	(%rsi), %eax
	cmpl	(%r13), %eax
	jle	.LBB1_30
	.p2align	4, 0x90
.LBB1_29:                               #   in Loop: Header=BB1_21 Depth=2
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sm_col_contains
	testl	%eax, %eax
	jne	.LBB1_31
.LBB1_30:                               #   in Loop: Header=BB1_21 Depth=2
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_21
	jmp	.LBB1_32
.LBB1_31:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	(%r13), %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sm_delcol
	.p2align	4, 0x90
.LBB1_32:                               # %.backedge
                                        #   in Loop: Header=BB1_1 Depth=1
	testq	%r12, %r12
	jne	.LBB1_1
# BB#33:                                # %._crit_edge73.loopexit
	movl	72(%rbx), %eax
.LBB1_34:                               # %._crit_edge73
	subl	%eax, %r14d
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	sm_col_dominance, .Lfunc_end1-sm_col_dominance
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
