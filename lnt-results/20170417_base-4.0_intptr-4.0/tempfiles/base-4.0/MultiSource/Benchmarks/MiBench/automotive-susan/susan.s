	.text
	.file	"susan.bc"
	.globl	usage
	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$.Lstr.8, %edi
	callq	puts
	movl	$.Lstr.9, %edi
	callq	puts
	movl	$.Lstr.10, %edi
	callq	puts
	movl	$.Lstr.11, %edi
	callq	puts
	movl	$.Lstr.12, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.Lfunc_end0:
	.size	usage, .Lfunc_end0-usage
	.cfi_endproc

	.globl	getint
	.p2align	4, 0x90
	.type	getint,@function
getint:                                 # @getint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 32
	subq	$10000, %rsp            # imm = 0x2710
.Lcfi4:
	.cfi_def_cfa_offset 10032
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	%rsp, %r14
	jmp	.LBB1_1
	.p2align	4, 0x90
.LBB1_8:                                # %.thread
                                        #   in Loop: Header=BB1_1 Depth=1
	movl	$9000, %esi             # imm = 0x2328
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	fgets
.LBB1_1:                                # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	je	.LBB1_8
# BB#2:                                 # %.backedge
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpl	$-1, %eax
	je	.LBB1_9
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.LBB1_1
# BB#4:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rbx,4), %ecx
	leal	-48(%rax,%rcx,2), %ebx
	movq	%r15, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB1_7
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB1_5 Depth=1
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$10, %ecx
	jb	.LBB1_5
.LBB1_7:
	movl	%ebx, %eax
	addq	$10000, %rsp            # imm = 0x2710
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_9:
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	movl	$.L.str.14, %edx
	xorl	%eax, %eax
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end1:
	.size	getint, .Lfunc_end1-getint
	.cfi_endproc

	.globl	get_image
	.p2align	4, 0x90
	.type	get_image,@function
get_image:                              # @get_image
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$10008, %rsp            # imm = 0x2718
.Lcfi14:
	.cfi_def_cfa_offset 10064
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$.L.str.15, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_1
# BB#4:
	movq	%rbx, %rdi
	callq	fgetc
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	fgetc
	movzbl	%bpl, %ecx
	cmpl	$80, %ecx
	jne	.LBB2_6
# BB#5:
	movzbl	%al, %eax
	cmpl	$53, %eax
	jne	.LBB2_6
# BB#7:
	movq	%rsp, %rbp
	jmp	.LBB2_8
	.p2align	4, 0x90
.LBB2_31:                               # %.thread.i
                                        #   in Loop: Header=BB2_8 Depth=1
	movl	$9000, %esi             # imm = 0x2328
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	fgets
.LBB2_8:                                # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	je	.LBB2_31
# BB#9:                                 # %.backedge.i
                                        #   in Loop: Header=BB2_8 Depth=1
	cmpl	$-1, %eax
	je	.LBB2_10
# BB#11:                                #   in Loop: Header=BB2_8 Depth=1
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.LBB2_8
# BB#12:                                # %.preheader.i.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_13:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbp,4), %ecx
	leal	-48(%rax,%rcx,2), %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB2_15
# BB#14:                                # %.preheader.i
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$10, %ecx
	jb	.LBB2_13
.LBB2_15:                               # %getint.exit
	movl	%ebp, (%r12)
	movq	%rsp, %rbp
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_32:                               # %.thread.i26
                                        #   in Loop: Header=BB2_16 Depth=1
	movl	$9000, %esi             # imm = 0x2328
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	fgets
.LBB2_16:                               # %.backedge.i25
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$35, %eax
	je	.LBB2_32
# BB#17:                                # %.backedge.i25
                                        #   in Loop: Header=BB2_16 Depth=1
	cmpl	$-1, %eax
	je	.LBB2_10
# BB#18:                                #   in Loop: Header=BB2_16 Depth=1
	leal	-48(%rax), %ecx
	cmpl	$9, %ecx
	ja	.LBB2_16
# BB#19:                                # %.preheader.i32.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_20:                               # %.preheader.i32
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbp,%rbp,4), %ecx
	leal	-48(%rax,%rcx,2), %ebp
	movq	%rbx, %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB2_22
# BB#21:                                # %.preheader.i32
                                        #   in Loop: Header=BB2_20 Depth=1
	movl	%eax, %ecx
	addl	$-48, %ecx
	cmpl	$10, %ecx
	jb	.LBB2_20
.LBB2_22:                               # %getint.exit33
	movl	%ebp, (%r13)
	movq	%rsp, %rbp
	jmp	.LBB2_23
	.p2align	4, 0x90
.LBB2_33:                               # %.thread.i35
                                        #   in Loop: Header=BB2_23 Depth=1
	movl	$9000, %esi             # imm = 0x2328
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	fgets
.LBB2_23:                               # %.backedge.i34
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$35, %eax
	je	.LBB2_33
# BB#24:                                # %.backedge.i34
                                        #   in Loop: Header=BB2_23 Depth=1
	cmpl	$-1, %eax
	je	.LBB2_10
# BB#25:                                #   in Loop: Header=BB2_23 Depth=1
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB2_23
	.p2align	4, 0x90
.LBB2_26:                               # %.preheader.i41
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB2_28
# BB#27:                                # %.preheader.i41
                                        #   in Loop: Header=BB2_26 Depth=1
	addl	$-48, %eax
	cmpl	$10, %eax
	jb	.LBB2_26
.LBB2_28:                               # %getint.exit42
	movslq	(%r13), %rax
	movslq	(%r12), %rbp
	imulq	%rax, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, (%r15)
	movl	$1, %esi
	movq	%rax, %rdi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	callq	fread
	testq	%rax, %rax
	je	.LBB2_29
# BB#30:
	movq	%rbx, %rdi
	callq	fclose
	addq	$10008, %rsp            # imm = 0x2718
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_10:
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	movl	$.L.str.14, %edx
	xorl	%eax, %eax
.LBB2_3:
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.LBB2_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
	jmp	.LBB2_2
.LBB2_6:
	movq	stderr(%rip), %rdi
	movl	$.L.str.17, %esi
	jmp	.LBB2_2
.LBB2_29:
	movq	stderr(%rip), %rdi
	movl	$.L.str.18, %esi
.LBB2_2:
	xorl	%eax, %eax
	movq	%r14, %rdx
	jmp	.LBB2_3
.Lfunc_end2:
	.size	get_image, .Lfunc_end2-get_image
	.cfi_endproc

	.globl	put_image
	.p2align	4, 0x90
	.type	put_image,@function
put_image:                              # @put_image
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -48
.Lcfi27:
	.cfi_offset %r12, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$.L.str.19, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_1
# BB#3:
	movl	$.L.str.21, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.22, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r12d, %edx
	movl	%ebp, %ecx
	callq	fprintf
	movl	$.L.str.23, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	imull	%r12d, %ebp
	movslq	%ebp, %rsi
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	fwrite
	cmpq	$1, %rax
	jne	.LBB3_4
# BB#5:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fclose                  # TAILCALL
.LBB3_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str.20, %esi
	jmp	.LBB3_2
.LBB3_4:
	movq	stderr(%rip), %rdi
	movl	$.L.str.24, %esi
.LBB3_2:
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end3:
	.size	put_image, .Lfunc_end3-put_image
	.cfi_endproc

	.globl	int_to_uchar
	.p2align	4, 0x90
	.type	int_to_uchar,@function
int_to_uchar:                           # @int_to_uchar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
.Lcfi35:
	.cfi_offset %rbx, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%edx, %r8d
	testl	%r8d, %r8d
	jle	.LBB4_23
# BB#1:                                 # %.lr.ph40.preheader
	movl	(%rdi), %r15d
	movl	%r8d, %r11d
	cmpl	$1, %r8d
	movl	%r15d, %ecx
	movl	%r15d, %ebx
	je	.LBB4_15
# BB#2:                                 # %.lr.ph40..lr.ph40_crit_edge.lr.ph
	leaq	-1(%r11), %r9
	cmpq	$8, %r9
	jb	.LBB4_3
# BB#4:                                 # %min.iters.checked
	movq	%r9, %r14
	andq	$-8, %r14
	movq	%r9, %r10
	andq	$-8, %r10
	je	.LBB4_3
# BB#5:                                 # %vector.body.preheader
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm4        # xmm4 = xmm0[0,0,0,0]
	leaq	-8(%r10), %rax
	movq	%rax, %rcx
	shrq	$3, %rcx
	btl	$3, %eax
	jb	.LBB4_6
# BB#7:                                 # %vector.body.prol
	movdqu	4(%rdi), %xmm1
	movdqu	20(%rdi), %xmm6
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm6, %xmm3
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm1, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm4, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm6, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm4, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm4, %xmm5
	pcmpgtd	%xmm1, %xmm5
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	pand	%xmm5, %xmm1
	pandn	%xmm4, %xmm5
	por	%xmm1, %xmm5
	pand	%xmm0, %xmm6
	pandn	%xmm4, %xmm0
	por	%xmm6, %xmm0
	movl	$8, %ebx
	movdqa	%xmm5, %xmm4
	testq	%rcx, %rcx
	jne	.LBB4_9
	jmp	.LBB4_11
.LBB4_3:
	movl	$1, %r14d
	movl	%r15d, %ebx
	movl	%r15d, %ecx
.LBB4_13:                               # %.lr.ph40..lr.ph40_crit_edge.preheader
	movq	%r11, %rax
	subq	%r14, %rax
	leaq	(%rdi,%r14,4), %rdx
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph40..lr.ph40_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %ebp
	cmpl	%ecx, %ebp
	cmovgel	%ebp, %ecx
	cmpl	%ebx, %ebp
	cmovlel	%ebp, %ebx
	addq	$4, %rdx
	decq	%rax
	jne	.LBB4_14
.LBB4_15:                               # %._crit_edge41
	testl	%r8d, %r8d
	jle	.LBB4_23
# BB#16:                                # %.lr.ph.preheader
	subl	%ebx, %ecx
	subl	%ebx, %r15d
	movl	%r15d, %eax
	shll	$8, %eax
	subl	%r15d, %eax
	cltd
	idivl	%ecx
	movb	%al, (%rsi)
	cmpl	$1, %r8d
	je	.LBB4_23
# BB#17:                                # %.lr.ph..lr.ph_crit_edge.preheader
	testb	$1, %r11b
	jne	.LBB4_18
# BB#19:                                # %.lr.ph..lr.ph_crit_edge.prol
	movl	4(%rdi), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ecx
	movb	%al, 1(%rsi)
	movl	$2, %eax
	cmpl	$2, %r8d
	jne	.LBB4_21
	jmp	.LBB4_23
.LBB4_18:
	movl	$1, %eax
	cmpl	$2, %r8d
	je	.LBB4_23
.LBB4_21:                               # %.lr.ph..lr.ph_crit_edge.preheader.new
	subq	%rax, %r11
	leaq	1(%rsi,%rax), %rsi
	leaq	4(%rdi,%rax,4), %rdi
	.p2align	4, 0x90
.LBB4_22:                               # %.lr.ph..lr.ph_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ecx
	movb	%al, -1(%rsi)
	movl	(%rdi), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ecx
	movb	%al, (%rsi)
	addq	$2, %rsi
	addq	$8, %rdi
	addq	$-2, %r11
	jne	.LBB4_22
.LBB4_23:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_6:
	xorl	%ebx, %ebx
                                        # implicit-def: %XMM5
	movdqa	%xmm4, %xmm0
	movdqa	%xmm4, %xmm2
	movdqa	%xmm4, %xmm3
	testq	%rcx, %rcx
	je	.LBB4_11
.LBB4_9:                                # %vector.body.preheader.new
	movq	%r10, %rcx
	subq	%rbx, %rcx
	leaq	52(%rdi,%rbx,4), %rbx
	movdqa	%xmm4, %xmm5
	.p2align	4, 0x90
.LBB4_10:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rbx), %xmm11
	movdqu	-32(%rbx), %xmm10
	movdqu	-16(%rbx), %xmm9
	movdqu	(%rbx), %xmm8
	movdqa	%xmm11, %xmm4
	pcmpgtd	%xmm2, %xmm4
	movdqa	%xmm10, %xmm6
	pcmpgtd	%xmm3, %xmm6
	movdqa	%xmm11, %xmm1
	pand	%xmm4, %xmm1
	pandn	%xmm2, %xmm4
	por	%xmm1, %xmm4
	movdqa	%xmm10, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm3, %xmm6
	por	%xmm1, %xmm6
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm11, %xmm1
	movdqa	%xmm0, %xmm7
	pcmpgtd	%xmm10, %xmm7
	pand	%xmm1, %xmm11
	pandn	%xmm5, %xmm1
	por	%xmm11, %xmm1
	pand	%xmm7, %xmm10
	pandn	%xmm0, %xmm7
	por	%xmm10, %xmm7
	movdqa	%xmm9, %xmm2
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm8, %xmm3
	pcmpgtd	%xmm6, %xmm3
	movdqa	%xmm9, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm4, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm8, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm6, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm1, %xmm5
	pcmpgtd	%xmm9, %xmm5
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm8, %xmm0
	pand	%xmm5, %xmm9
	pandn	%xmm1, %xmm5
	por	%xmm9, %xmm5
	pand	%xmm0, %xmm8
	pandn	%xmm7, %xmm0
	por	%xmm8, %xmm0
	addq	$64, %rbx
	addq	$-16, %rcx
	jne	.LBB4_10
.LBB4_11:                               # %middle.block
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	por	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm1
	pandn	%xmm2, %xmm3
	por	%xmm1, %xmm3
	pshufd	$229, %xmm3, %xmm1      # xmm1 = xmm3[1,1,2,3]
	movd	%xmm3, %eax
	pcmpgtd	%xmm1, %xmm3
	movdqa	%xmm3, -40(%rsp)
	testb	$1, -40(%rsp)
	movd	%xmm1, %ecx
	cmovnel	%eax, %ecx
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm5, %xmm1
	pand	%xmm1, %xmm5
	pandn	%xmm0, %xmm1
	por	%xmm5, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movd	%xmm0, %eax
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm0, -24(%rsp)
	movd	%xmm2, %ebx
	testb	$1, -24(%rsp)
	cmovel	%eax, %ebx
	cmpq	%r10, %r9
	je	.LBB4_15
# BB#12:
	orq	$1, %r14
	jmp	.LBB4_13
.Lfunc_end4:
	.size	int_to_uchar, .Lfunc_end4-int_to_uchar
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_1:
	.quad	4636737291354636288     # double 100
	.text
	.globl	setup_brightness_lut
	.p2align	4, 0x90
	.type	setup_brightness_lut,@function
setup_brightness_lut:                   # @setup_brightness_lut
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 48
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$516, %edi              # imm = 0x204
	callq	malloc
	addq	$258, %rax              # imm = 0x102
	movq	%rax, (%rbx)
	cvtsi2ssl	%ebp, %xmm1
	cmpl	$6, %r14d
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movq	$-256, %rbp
	jne	.LBB5_1
	.p2align	4, 0x90
.LBB5_2:                                # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm1
	xorps	.LCPI5_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	callq	exp
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulsd	.LCPI5_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvttss2si	%xmm0, %eax
	movq	(%rbx), %rcx
	movb	%al, (%rcx,%rbp)
	incq	%rbp
	cmpq	$257, %rbp              # imm = 0x101
	jne	.LBB5_2
	jmp	.LBB5_3
	.p2align	4, 0x90
.LBB5_1:                                # %.split
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	xorps	.LCPI5_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	exp
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulsd	.LCPI5_1(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvttss2si	%xmm0, %eax
	movq	(%rbx), %rcx
	movb	%al, (%rcx,%rbp)
	incq	%rbp
	cmpq	$257, %rbp              # imm = 0x101
	jne	.LBB5_1
.LBB5_3:                                # %.us-lcssa.us
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	setup_brightness_lut, .Lfunc_end5-setup_brightness_lut
	.cfi_endproc

	.globl	susan_principle
	.p2align	4, 0x90
	.type	susan_principle,@function
susan_principle:                        # @susan_principle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 128
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %ebx
	movl	%ecx, %r13d
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %eax
	imull	%ebx, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
	addl	$-3, %ebp
	cmpl	$4, %ebp
	jl	.LBB6_8
# BB#1:                                 # %.preheader.lr.ph
	movslq	%ebx, %rcx
	leaq	-3(%rcx), %r11
	movl	%r11d, %r8d
	movl	%ebp, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	-8(%rcx,%rcx), %rdi
	leaq	14(%rcx,%rdi), %rbp
	leaq	-12(%rcx,%rcx), %rsi
	leaq	-12(%rdi,%rcx,2), %rdx
	leaq	-15(%rcx,%rcx,2), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	20(%rax,%rcx,2), %rax
	leaq	-6(%rsi,%rcx,2), %rsi
	leaq	20(%rsi,%rcx,2), %r14
	leaq	(%rcx,%rcx,2), %rbx
	leaq	12(%r12,%rbx,4), %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	leaq	(,%rcx,4), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	addq	$-3, %r8
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	$3, %esi
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_4 Depth 2
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	cmpl	$4, %r11d
	jl	.LBB6_7
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	(%rsp), %r12            # 8-byte Reload
	movq	8(%rsp), %r15           # 8-byte Reload
	.p2align	4, 0x90
.LBB6_4:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rsp), %rcx          # 8-byte Reload
	movzbl	18(%rcx,%r15), %r8d
	addq	64(%rsp), %r8           # 8-byte Folded Reload
	movzbl	2(%r15), %ebx
	movq	%r8, %r10
	subq	%rbx, %r10
	movzbl	(%r10), %ebx
	movzbl	3(%r15), %esi
	movq	%r8, %rcx
	subq	%rsi, %rcx
	movzbl	(%rcx), %ecx
	addl	%ebx, %ecx
	movzbl	4(%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	4(%r11,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	5(%r11,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	6(%r11,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	7(%r11,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	8(%r11,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	8(%rdi,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	9(%rdi,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	10(%rdi,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	11(%rdi,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	12(%rdi,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	13(%rdi,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	14(%rdi,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	-6(%rbp,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	-5(%rbp,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	-4(%rbp,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	-2(%rbp,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	-1(%rbp,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	(%rbp,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	20(%rdx,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	21(%rdx,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	22(%rdx,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	23(%rdx,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	24(%rdx,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	25(%rdx,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	26(%rdx,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	-4(%rax,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	-3(%rax,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	-2(%rax,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	-1(%rax,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	(%rax,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	(%r14,%r15), %ecx
	movq	%r8, %rbx
	subq	%rcx, %rbx
	movzbl	(%rbx), %ecx
	addl	%esi, %ecx
	movzbl	1(%r14,%r15), %esi
	movq	%r8, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%ecx, %esi
	movzbl	2(%r14,%r15), %ecx
	subq	%rcx, %r8
	movzbl	(%r8), %ecx
	leal	100(%rcx,%rsi), %ebx
	cmpl	%r13d, %ebx
	jg	.LBB6_6
# BB#5:                                 #   in Loop: Header=BB6_4 Depth=2
	movl	%r13d, %ecx
	subl	%ebx, %ecx
	movl	%ecx, (%r12)
.LBB6_6:                                #   in Loop: Header=BB6_4 Depth=2
	incq	%r15
	addq	$4, %r12
	decq	%r9
	jne	.LBB6_4
.LBB6_7:                                # %._crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	movq	48(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, 8(%rsp)           # 8-byte Folded Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, (%rsp)            # 8-byte Folded Spill
	cmpq	32(%rsp), %rsi          # 8-byte Folded Reload
	jne	.LBB6_2
.LBB6_8:                                # %._crit_edge159
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	susan_principle, .Lfunc_end6-susan_principle
	.cfi_endproc

	.globl	susan_principle_small
	.p2align	4, 0x90
	.type	susan_principle_small,@function
susan_principle_small:                  # @susan_principle_small
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 80
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r12d
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	%ebp, %eax
	imull	%r12d, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	memset
	decl	%ebp
	cmpl	$2, %ebp
	jl	.LBB7_8
# BB#1:                                 # %.preheader.lr.ph
	leal	-1(%r12), %eax
	cmpl	$2, %eax
	jl	.LBB7_8
# BB#2:                                 # %.preheader.us.preheader
	leal	-2(%r12), %ecx
	movslq	%ecx, %rcx
	movslq	%r12d, %rdx
	movl	%eax, %esi
	movl	%ebp, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	1(%r14,%rdx), %r12
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leaq	4(%r15,%rdx,4), %r15
	leaq	6(%r14,%rcx,2), %rbp
	leaq	2(%r14,%rcx), %r10
	leaq	2(%r14), %rax
	decq	%rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	$1, %r11d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB7_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_4 Depth 2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB7_4:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r12,%rdi), %r9d
	addq	%r13, %r9
	movzbl	-2(%rax,%rdi), %esi
	movq	%r9, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	movzbl	-1(%rax,%rdi), %ebx
	movq	%r9, %r8
	subq	%rbx, %r8
	movzbl	(%r8), %ebx
	addl	%esi, %ebx
	movzbl	(%rax,%rdi), %esi
	movq	%r9, %rdx
	subq	%rsi, %rdx
	movzbl	(%rdx), %edx
	addl	%ebx, %edx
	movzbl	(%r10,%rdi), %esi
	movq	%r9, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%edx, %esi
	movzbl	2(%r10,%rdi), %edx
	movq	%r9, %rbx
	subq	%rdx, %rbx
	movzbl	(%rbx), %edx
	addl	%esi, %edx
	movzbl	-2(%rbp,%rdi), %esi
	movq	%r9, %rbx
	subq	%rsi, %rbx
	movzbl	(%rbx), %esi
	addl	%edx, %esi
	movzbl	-1(%rbp,%rdi), %edx
	movq	%r9, %rbx
	subq	%rdx, %rbx
	movzbl	(%rbx), %edx
	addl	%esi, %edx
	movzbl	(%rbp,%rdi), %esi
	subq	%rsi, %r9
	movzbl	(%r9), %esi
	leal	100(%rsi,%rdx), %ebx
	cmpl	$730, %ebx              # imm = 0x2DA
	jg	.LBB7_6
# BB#5:                                 #   in Loop: Header=BB7_4 Depth=2
	movl	$730, %edx              # imm = 0x2DA
	subl	%ebx, %edx
	movl	%edx, (%r15,%rdi,4)
.LBB7_6:                                #   in Loop: Header=BB7_4 Depth=2
	incq	%rdi
	decq	%rcx
	jne	.LBB7_4
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB7_3 Depth=1
	incq	%r11
	addq	16(%rsp), %r14          # 8-byte Folded Reload
	cmpq	(%rsp), %r11            # 8-byte Folded Reload
	jne	.LBB7_3
.LBB7_8:                                # %._crit_edge67
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	susan_principle_small, .Lfunc_end7-susan_principle_small
	.cfi_endproc

	.globl	median
	.p2align	4, 0x90
	.type	median,@function
median:                                 # @median
	.cfi_startproc
# BB#0:                                 # %.lr.ph
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	-1(%rsi), %eax
	imull	%ecx, %eax
	addl	%edx, %eax
	movslq	%eax, %r8
	movzbl	-1(%rdi,%r8), %r9d
	movl	%r9d, -40(%rsp)
	movl	%ecx, %eax
	imull	%esi, %eax
	addl	%edx, %eax
	cltq
	movzbl	-1(%rdi,%rax), %r10d
	movzbl	1(%rdi,%rax), %r11d
	shll	$8, %r11d
	orl	%r10d, %r11d
	movzbl	(%rdi,%r8), %r10d
	movzbl	1(%rdi,%r8), %eax
	shll	$8, %eax
	orl	%r10d, %eax
	pinsrw	$0, %eax, %xmm0
	pinsrw	$1, %r11d, %xmm0
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	movdqu	%xmm0, -36(%rsp)
	incl	%esi
	imull	%ecx, %esi
	addl	%edx, %esi
	movslq	%esi, %rax
	movzbl	-1(%rdi,%rax), %ecx
	movl	%ecx, -20(%rsp)
	movzbl	(%rdi,%rax), %ecx
	movl	%ecx, -16(%rsp)
	movzbl	1(%rdi,%rax), %eax
	movl	%eax, -12(%rsp)
	movl	-36(%rsp), %ecx
	cmpl	%ecx, %r9d
	jle	.LBB8_1
# BB#4:
	movl	%ecx, -40(%rsp)
	movl	%r9d, -36(%rsp)
	jmp	.LBB8_2
.LBB8_1:
	movl	%ecx, %r9d
.LBB8_2:                                # %.backedge
	movl	-32(%rsp), %ecx
	cmpl	%ecx, %r9d
	jle	.LBB8_3
# BB#63:
	movl	%ecx, -36(%rsp)
	movl	%r9d, -32(%rsp)
	jmp	.LBB8_64
.LBB8_3:
	movl	%ecx, %r9d
.LBB8_64:                               # %.backedge.162
	movl	-28(%rsp), %ecx
	cmpl	%ecx, %r9d
	jle	.LBB8_65
# BB#66:
	movl	%ecx, -32(%rsp)
	movl	%r9d, -28(%rsp)
	jmp	.LBB8_67
.LBB8_65:
	movl	%ecx, %r9d
.LBB8_67:                               # %.backedge.264
	movl	-24(%rsp), %ecx
	cmpl	%ecx, %r9d
	jle	.LBB8_68
# BB#69:
	movl	%ecx, -28(%rsp)
	movl	%r9d, -24(%rsp)
	jmp	.LBB8_70
.LBB8_68:
	movl	%ecx, %r9d
.LBB8_70:                               # %.backedge.366
	movl	-20(%rsp), %ecx
	cmpl	%ecx, %r9d
	jle	.LBB8_71
# BB#72:
	movl	%ecx, -24(%rsp)
	movl	%r9d, -20(%rsp)
	jmp	.LBB8_73
.LBB8_71:
	movl	%ecx, %r9d
.LBB8_73:                               # %.backedge.468
	movl	-16(%rsp), %ecx
	cmpl	%ecx, %r9d
	jle	.LBB8_74
# BB#75:
	movl	%ecx, -20(%rsp)
	movl	%r9d, -16(%rsp)
	jmp	.LBB8_76
.LBB8_74:
	movl	%ecx, %r9d
.LBB8_76:                               # %.backedge.570
	movl	-12(%rsp), %ecx
	cmpl	%ecx, %r9d
	jle	.LBB8_5
# BB#77:
	movl	%ecx, -16(%rsp)
	movl	%r9d, -12(%rsp)
.LBB8_5:                                # %.lr.ph.1
	movl	-40(%rsp), %eax
	movl	-36(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_6
# BB#7:
	movl	%ecx, -40(%rsp)
	movl	%eax, -36(%rsp)
	jmp	.LBB8_8
.LBB8_6:
	movl	%ecx, %eax
.LBB8_8:                                # %.backedge.1
	movl	-32(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_9
# BB#51:
	movl	%ecx, -36(%rsp)
	movl	%eax, -32(%rsp)
	jmp	.LBB8_52
.LBB8_9:
	movl	%ecx, %eax
.LBB8_52:                               # %.backedge.1.1
	movl	-28(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_53
# BB#54:
	movl	%ecx, -32(%rsp)
	movl	%eax, -28(%rsp)
	jmp	.LBB8_55
.LBB8_53:
	movl	%ecx, %eax
.LBB8_55:                               # %.backedge.1.2
	movl	-24(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_56
# BB#57:
	movl	%ecx, -28(%rsp)
	movl	%eax, -24(%rsp)
	jmp	.LBB8_58
.LBB8_56:
	movl	%ecx, %eax
.LBB8_58:                               # %.backedge.1.3
	movl	-20(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_59
# BB#60:
	movl	%ecx, -24(%rsp)
	movl	%eax, -20(%rsp)
	jmp	.LBB8_61
.LBB8_59:
	movl	%ecx, %eax
.LBB8_61:                               # %.backedge.1.4
	movl	-16(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_10
# BB#62:
	movl	%ecx, -20(%rsp)
	movl	%eax, -16(%rsp)
.LBB8_10:                               # %.lr.ph.2
	movl	-40(%rsp), %eax
	movl	-36(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_11
# BB#12:
	movl	%ecx, -40(%rsp)
	movl	%eax, -36(%rsp)
	jmp	.LBB8_13
.LBB8_11:
	movl	%ecx, %eax
.LBB8_13:                               # %.backedge.2
	movl	-32(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_14
# BB#42:
	movl	%ecx, -36(%rsp)
	movl	%eax, -32(%rsp)
	jmp	.LBB8_43
.LBB8_14:
	movl	%ecx, %eax
.LBB8_43:                               # %.backedge.2.1
	movl	-28(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_44
# BB#45:
	movl	%ecx, -32(%rsp)
	movl	%eax, -28(%rsp)
	jmp	.LBB8_46
.LBB8_44:
	movl	%ecx, %eax
.LBB8_46:                               # %.backedge.2.2
	movl	-24(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_47
# BB#48:
	movl	%ecx, -28(%rsp)
	movl	%eax, -24(%rsp)
	jmp	.LBB8_49
.LBB8_47:
	movl	%ecx, %eax
.LBB8_49:                               # %.backedge.2.3
	movl	-20(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_15
# BB#50:
	movl	%ecx, -24(%rsp)
	movl	%eax, -20(%rsp)
.LBB8_15:                               # %.lr.ph.3
	movl	-40(%rsp), %eax
	movl	-36(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_16
# BB#17:
	movl	%ecx, -40(%rsp)
	movl	%eax, -36(%rsp)
	jmp	.LBB8_18
.LBB8_16:
	movl	%ecx, %eax
.LBB8_18:                               # %.backedge.3
	movl	-32(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_19
# BB#36:
	movl	%ecx, -36(%rsp)
	movl	%eax, -32(%rsp)
	jmp	.LBB8_37
.LBB8_19:
	movl	%ecx, %eax
.LBB8_37:                               # %.backedge.3.1
	movl	-28(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_38
# BB#39:
	movl	%ecx, -32(%rsp)
	movl	%eax, -28(%rsp)
	jmp	.LBB8_40
.LBB8_38:
	movl	%ecx, %eax
.LBB8_40:                               # %.backedge.3.2
	movl	-24(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_20
# BB#41:
	movl	%ecx, -28(%rsp)
	movl	%eax, -24(%rsp)
.LBB8_20:                               # %.lr.ph.4
	movl	-40(%rsp), %eax
	movl	-36(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_21
# BB#22:
	movl	%ecx, -40(%rsp)
	movl	%eax, -36(%rsp)
	jmp	.LBB8_23
.LBB8_21:
	movl	%ecx, %eax
.LBB8_23:                               # %.backedge.4
	movl	-32(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_24
# BB#33:
	movl	%ecx, -36(%rsp)
	movl	%eax, -32(%rsp)
	jmp	.LBB8_34
.LBB8_24:
	movl	%ecx, %eax
.LBB8_34:                               # %.backedge.4.1
	movl	-28(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_25
# BB#35:
	movl	%ecx, -32(%rsp)
	movl	%eax, -28(%rsp)
.LBB8_25:                               # %.lr.ph.5
	movl	-40(%rsp), %eax
	movl	-36(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_26
# BB#27:
	movl	%ecx, -40(%rsp)
	movl	%eax, -36(%rsp)
	jmp	.LBB8_28
.LBB8_26:
	movl	%ecx, %eax
.LBB8_28:                               # %.backedge.5
	movl	-32(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_30
# BB#29:
	movl	%ecx, -36(%rsp)
	movl	%eax, -32(%rsp)
.LBB8_30:                               # %.backedge.5.1
	movl	-40(%rsp), %eax
	movl	-36(%rsp), %ecx
	cmpl	%ecx, %eax
	jle	.LBB8_32
# BB#31:
	movl	%ecx, -40(%rsp)
	movl	%eax, -36(%rsp)
.LBB8_32:                               # %._crit_edge.6
	movl	-24(%rsp), %ecx
	addl	-28(%rsp), %ecx
	movl	%ecx, %eax
	shrl	$31, %eax
	addl	%ecx, %eax
	shrl	%eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end8:
	.size	median, .Lfunc_end8-median
	.cfi_endproc

	.globl	enlarge
	.p2align	4, 0x90
	.type	enlarge,@function
enlarge:                                # @enlarge
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 80
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movl	%r8d, %r15d
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r8
	cmpl	$0, (%rcx)
	movq	%r8, 8(%rsp)            # 8-byte Spill
	jle	.LBB9_3
# BB#1:                                 # %.lr.ph93
	leal	(%r15,%r15), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movslq	%r15d, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	leal	(%r15,%rbp), %eax
	movslq	(%r12), %rdx
	movq	(%rsp), %rsi            # 8-byte Reload
	leal	(%rdx,%rsi), %esi
	imull	%eax, %esi
	movslq	%esi, %rdi
	addq	%r14, %rdi
	addq	%rbx, %rdi
	movl	%edx, %eax
	imull	%ebp, %eax
	movslq	%eax, %rsi
	addq	(%r8), %rsi
	movq	%rcx, %r13
	callq	memcpy
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	%r13, %rcx
	incl	%ebp
	cmpl	(%rcx), %ebp
	jl	.LBB9_2
.LBB9_3:                                # %.preheader82
	testl	%r15d, %r15d
	jle	.LBB9_7
# BB#4:                                 # %.lr.ph90
	leal	(%r15,%r15), %r13d
	movslq	%r15d, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%ebp, %ebp
	movl	$-1, %ebx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB9_5:                                # =>This Inner Loop Header: Depth=1
	leal	(%r15,%rbx), %eax
	movslq	(%r12), %rdx
	leal	(%rdx,%r13), %ecx
	imull	%eax, %ecx
	movslq	%ecx, %rdi
	addq	%r14, %rdi
	addq	(%rsp), %rdi            # 8-byte Folded Reload
	movl	%edx, %eax
	imull	%ebp, %eax
	movslq	%eax, %rsi
	addq	(%r8), %rsi
	callq	memcpy
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	leal	(%r15,%rbp), %ecx
	addl	%eax, %ecx
	movslq	(%r12), %rdx
	leal	(%rdx,%r13), %esi
	imull	%ecx, %esi
	movslq	%esi, %rdi
	addq	%r14, %rdi
	addq	(%rsp), %rdi            # 8-byte Folded Reload
	addl	%ebx, %eax
	imull	%edx, %eax
	movslq	%eax, %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	(%rax), %rsi
	callq	memcpy
	movq	8(%rsp), %r8            # 8-byte Reload
	incl	%ebp
	decl	%ebx
	cmpl	%ebp, %r15d
	jne	.LBB9_5
# BB#6:                                 # %.preheader81
	testl	%r15d, %r15d
	movq	16(%rsp), %rcx          # 8-byte Reload
	jle	.LBB9_7
# BB#8:                                 # %.preheader.lr.ph
	movl	(%rcx), %esi
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_9:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_11 Depth 2
	movl	%esi, %edx
	addl	%r13d, %edx
	jle	.LBB9_12
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB9_9 Depth=1
	decl	%ebp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_11:                               #   Parent Loop BB9_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12), %esi
	addl	%r13d, %esi
	imull	%edx, %esi
	addl	%r15d, %esi
	leal	(%rsi,%rax), %edi
	movslq	%edi, %rdi
	movzbl	(%r14,%rdi), %ebx
	addl	%ebp, %esi
	movslq	%esi, %rsi
	movb	%bl, (%r14,%rsi)
	movl	(%r12), %esi
	leal	(%rsi,%r13), %edi
	imull	%edx, %edi
	addl	%r15d, %esi
	addl	%edi, %esi
	leal	(%rbp,%rsi), %edi
	movslq	%edi, %rdi
	movzbl	(%r14,%rdi), %ebx
	addl	%eax, %esi
	movslq	%esi, %rsi
	movb	%bl, (%r14,%rsi)
	incl	%edx
	movl	(%rcx), %esi
	leal	(%rsi,%r13), %edi
	cmpl	%edi, %edx
	jl	.LBB9_11
.LBB9_12:                               # %._crit_edge
                                        #   in Loop: Header=BB9_9 Depth=1
	leal	1(%rax), %edx
	notl	%eax
	cmpl	%r15d, %edx
	movl	%eax, %ebp
	movl	%edx, %eax
	jne	.LBB9_9
	jmp	.LBB9_13
.LBB9_7:                                # %.preheader81.._crit_edge87_crit_edge
	addl	%r15d, %r15d
	movl	%r15d, %r13d
.LBB9_13:                               # %._crit_edge87
	addl	%r13d, (%r12)
	addl	%r13d, (%rcx)
	movq	%r14, (%r8)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	enlarge, .Lfunc_end9-enlarge
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4609434218613702656     # double 1.5
.LCPI10_3:
	.quad	4636737291354636288     # double 100
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI10_1:
	.long	1097859072              # float 15
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	susan_smoothing
	.p2align	4, 0x90
	.type	susan_smoothing,@function
susan_smoothing:                        # @susan_smoothing
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 208
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%r8, 128(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	movl	%edi, %r14d
	cvtsd2ss	%xmm0, %xmm1
	movq	%rbx, 72(%rsp)
	movl	%edx, 8(%rsp)
	movl	%ecx, 12(%rsp)
	movl	$1, %r13d
	testl	%r14d, %r14d
	jne	.LBB10_2
# BB#1:
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	mulsd	.LCPI10_0(%rip), %xmm0
	cvttsd2si	%xmm0, %r13d
	incl	%r13d
.LBB10_2:
	ucomiss	.LCPI10_1(%rip), %xmm1
	ja	.LBB10_38
# BB#3:
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	leal	1(%r13,%r13), %ebp
	cmpl	%edx, %ebp
	jg	.LBB10_39
# BB#4:
	cmpl	%ecx, %ebp
	jg	.LBB10_39
# BB#5:
	leal	(%r13,%r13), %eax
	addl	%eax, %edx
	addl	%eax, %ecx
	imull	%edx, %ecx
	movslq	%ecx, %rdi
	callq	malloc
	leaq	72(%rsp), %rdi
	leaq	8(%rsp), %rdx
	leaq	12(%rsp), %rcx
	movq	%rax, %rsi
	movl	%r13d, %r8d
	callq	enlarge
	testl	%r14d, %r14d
	je	.LBB10_12
# BB#6:                                 # %.preheader233
	movl	12(%rsp), %eax
	leal	-1(%rax), %ecx
	cmpl	$2, %ecx
	jl	.LBB10_37
# BB#7:                                 # %.preheader232.preheader
	movl	$1, %r8d
	xorl	%ecx, %ecx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	8(%rsp), %ecx
	.p2align	4, 0x90
.LBB10_8:                               # %.preheader232
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_10 Depth 2
	leal	-1(%rcx), %edx
	cmpl	$2, %edx
	jl	.LBB10_36
# BB#9:                                 # %.lr.ph294
                                        #   in Loop: Header=BB10_8 Depth=1
	xorl	%ebp, %ebp
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movl	%r8d, 44(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB10_10:                              #   Parent Loop BB10_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rsp), %rdx
	movq	96(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	imull	%ecx, %eax
	movslq	%eax, %rdi
	addq	%rdx, %rdi
	movl	%r8d, %eax
	imull	%ecx, %eax
	leal	1(%rbp,%rax), %eax
	cltq
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movzbl	(%rdx,%rax), %edx
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leaq	(%rax,%rdx), %rbx
	movzbl	(%rbp,%rdi), %eax
	movq	%rbx, %rdx
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subq	%rax, %rdx
	movzbl	(%rdx), %r10d
	movzbl	1(%rbp,%rdi), %eax
	movq	%rbx, %rdx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subq	%rax, %rdx
	movzbl	(%rdx), %r13d
	movzbl	2(%rbp,%rdi), %eax
	movq	%rbx, %rdx
	movq	%rax, 56(%rsp)          # 8-byte Spill
	subq	%rax, %rdx
	movzbl	(%rdx), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movslq	%ecx, %r9
	leaq	-2(%r9,%rdi), %rax
	movzbl	2(%rbp,%rax), %esi
	movq	%rbx, %rdx
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	subq	%rsi, %rdx
	movzbl	(%rdx), %r8d
	movzbl	3(%rbp,%rax), %edx
	leaq	-4(%rdi,%r9,2), %rdi
	movq	%rbx, %rsi
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	subq	%rdx, %rsi
	movzbl	(%rsi), %r12d
	movzbl	4(%rbp,%rax), %eax
	movq	%rbx, %rsi
	movq	%rax, 112(%rsp)         # 8-byte Spill
	subq	%rax, %rsi
	movzbl	(%rsi), %r14d
	movzbl	4(%rbp,%rdi), %eax
	movq	%rbx, %rsi
	movq	%rax, 104(%rsp)         # 8-byte Spill
	subq	%rax, %rsi
	movzbl	(%rsi), %r15d
	movzbl	5(%rbp,%rdi), %edx
	movq	%rbx, %rax
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	subq	%rdx, %rax
	movzbl	(%rax), %r9d
	movzbl	6(%rbp,%rdi), %edi
	subq	%rdi, %rbx
	movzbl	(%rbx), %r11d
	movq	%r10, %rbx
	movq	%r13, %rax
	leal	(%rbx,%r13), %r10d
	movl	64(%rsp), %esi          # 4-byte Reload
	addl	%esi, %r10d
	addl	%r8d, %r10d
	addl	%r12d, %r10d
	addl	%r14d, %r10d
	addl	%r15d, %r10d
	addl	%r9d, %r10d
	addl	%r11d, %r10d
	addl	$-100, %r10d
	leaq	1(%rbp), %rdx
	je	.LBB10_11
# BB#33:                                #   in Loop: Header=BB10_10 Depth=2
	imull	32(%rsp), %ebx          # 4-byte Folded Reload
	movq	%rax, %rcx
	imull	24(%rsp), %ecx          # 4-byte Folded Reload
	imull	56(%rsp), %esi          # 4-byte Folded Reload
	imull	120(%rsp), %r8d         # 4-byte Folded Reload
	imull	48(%rsp), %r12d         # 4-byte Folded Reload
	imull	112(%rsp), %r14d        # 4-byte Folded Reload
	imull	104(%rsp), %r15d        # 4-byte Folded Reload
	imull	144(%rsp), %r9d         # 4-byte Folded Reload
	imull	%edi, %r11d
	movq	%rdx, %rdi
	imull	$-100, 16(%rsp), %eax   # 4-byte Folded Reload
	addl	%ebx, %eax
	addl	%ecx, %eax
	addl	%esi, %eax
	addl	%r8d, %eax
	addl	%r12d, %eax
	addl	%r14d, %eax
	addl	%r15d, %eax
	addl	%r9d, %eax
	addl	%r11d, %eax
	cltd
	idivl	%r10d
	movl	44(%rsp), %r8d          # 4-byte Reload
	movq	%rdi, %rdx
	jmp	.LBB10_34
	.p2align	4, 0x90
.LBB10_11:                              #   in Loop: Header=BB10_10 Depth=2
	movq	136(%rsp), %rdi         # 8-byte Reload
	movl	44(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rdx, %r14
	callq	median
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movl	%ebx, %r8d
	movq	%r14, %rdx
.LBB10_34:                              #   in Loop: Header=BB10_10 Depth=2
	movq	80(%rsp), %rbx          # 8-byte Reload
	movb	%al, (%rbx,%rbp)
	movslq	8(%rsp), %rcx
	movq	%rcx, %rax
	decq	%rax
	addq	$2, %rbp
	cmpq	%rax, %rbp
	movq	%rdx, %rbp
	jl	.LBB10_10
# BB#35:                                # %._crit_edge295.loopexit
                                        #   in Loop: Header=BB10_8 Depth=1
	movl	12(%rsp), %eax
	addq	%rdx, %rbx
.LBB10_36:                              # %._crit_edge295
                                        #   in Loop: Header=BB10_8 Depth=1
	incl	%r8d
	leal	-1(%rax), %edx
	movq	96(%rsp), %rsi          # 8-byte Reload
	incl	%esi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	cmpl	%edx, %r8d
	jl	.LBB10_8
	jmp	.LBB10_37
.LBB10_12:
	movslq	8(%rsp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	%ebp, 32(%rsp)          # 4-byte Spill
	movl	%ebp, %edi
	imull	%edi, %edi
	callq	malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%r13d, %eax
	negl	%eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	cmpl	%eax, %r13d
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	jl	.LBB10_17
# BB#13:                                # %.preheader231.preheader
	mulss	%xmm1, %xmm1
	xorps	.LCPI10_2(%rip), %xmm1
	movl	%r13d, %eax
	notl	%eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	24(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r12d
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB10_14:                              # %.preheader231
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_15 Depth 2
	movl	%r12d, %r14d
	imull	%r14d, %r14d
	movl	16(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB10_15:                              #   Parent Loop BB10_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r15d
	movl	%r15d, %eax
	imull	%eax, %eax
	addl	%r14d, %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	%xmm1, %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	exp
	movsd	.LCPI10_3(%rip), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	mulsd	%xmm2, %xmm0
	cvttsd2si	%xmm0, %eax
	movb	%al, (%rbp)
	incq	%rbp
	cmpl	%r13d, %r15d
	jl	.LBB10_15
# BB#16:                                # %._crit_edge288
                                        #   in Loop: Header=BB10_14 Depth=1
	cmpl	%r13d, %r12d
	leal	1(%r12), %eax
	movl	%eax, %r12d
	jl	.LBB10_14
.LBB10_17:                              # %.preheader230
	movl	12(%rsp), %eax
	subl	%r13d, %eax
	cmpl	%eax, %r13d
	jge	.LBB10_37
# BB#18:                                # %.preheader229.lr.ph
	movslq	32(%rsp), %rax          # 4-byte Folded Reload
	subq	%rax, 64(%rsp)          # 8-byte Folded Spill
	movslq	%r13d, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	negq	%rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	8(%rsp), %ecx
	movl	%r13d, %r14d
	notl	%r14d
	movl	%r13d, %ebp
	.p2align	4, 0x90
.LBB10_19:                              # %.preheader229
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_22 Depth 2
                                        #       Child Loop BB10_23 Depth 3
                                        #         Child Loop BB10_24 Depth 4
                                        #     Child Loop BB10_29 Depth 2
	movl	%ecx, %eax
	subl	%r13d, %eax
	cmpl	%eax, %r13d
	jge	.LBB10_32
# BB#20:                                # %.lr.ph253
                                        #   in Loop: Header=BB10_19 Depth=1
	cmpl	24(%rsp), %r13d         # 4-byte Folded Reload
	jge	.LBB10_21
# BB#28:                                # %.preheader312.preheader
                                        #   in Loop: Header=BB10_19 Depth=1
	movl	%r13d, %eax
	.p2align	4, 0x90
.LBB10_29:                              # %.preheader312
                                        #   Parent Loop BB10_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	72(%rsp), %rdx
	imull	%ebp, %ecx
	addl	%eax, %ecx
	movslq	%ecx, %rcx
	movzbl	(%rdx,%rcx), %ecx
	movb	%cl, (%rbx)
	incq	%rbx
	incl	%eax
	movl	8(%rsp), %ecx
	movl	%ecx, %edx
	subl	%r13d, %edx
	cmpl	%edx, %eax
	jl	.LBB10_29
	jmp	.LBB10_32
	.p2align	4, 0x90
.LBB10_21:                              #   in Loop: Header=BB10_19 Depth=1
	movl	%ebp, %eax
	subl	%r13d, %eax
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movq	104(%rsp), %rdx         # 8-byte Reload
	movl	%ebp, 56(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB10_22:                              # %.preheader.preheader
                                        #   Parent Loop BB10_19 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_23 Depth 3
                                        #         Child Loop BB10_24 Depth 4
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rdi
	movl	%ecx, %eax
	imull	%ebp, %eax
	addl	%edx, %eax
	cltq
	movzbl	(%rdi,%rax), %r15d
	movq	%r15, 32(%rsp)          # 8-byte Spill
	addq	128(%rsp), %r15         # 8-byte Folded Reload
	movl	%ecx, %eax
	imull	120(%rsp), %eax         # 4-byte Folded Reload
	movslq	%eax, %r12
	addq	%rdi, %r12
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	addq	%rdx, %r12
	addq	112(%rsp), %r12         # 8-byte Folded Reload
	xorl	%eax, %eax
	movq	48(%rsp), %rbx          # 8-byte Reload
	xorl	%r11d, %r11d
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	%edx, %r9d
	.p2align	4, 0x90
.LBB10_23:                              # %.preheader
                                        #   Parent Loop BB10_19 Depth=1
                                        #     Parent Loop BB10_22 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_24 Depth 4
	movl	%r14d, %esi
	.p2align	4, 0x90
.LBB10_24:                              #   Parent Loop BB10_19 Depth=1
                                        #     Parent Loop BB10_22 Depth=2
                                        #       Parent Loop BB10_23 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r12), %ebp
	incq	%r12
	movzbl	(%rbx), %r8d
	incq	%rbx
	movq	%r15, %r10
	subq	%rbp, %r10
	movzbl	(%r10), %edx
	imull	%r8d, %edx
	addl	%edx, %r11d
	imull	%ebp, %edx
	addl	%edx, %eax
	incl	%esi
	cmpl	%r13d, %esi
	jl	.LBB10_24
# BB#25:                                # %._crit_edge
                                        #   in Loop: Header=BB10_23 Depth=3
	addq	64(%rsp), %r12          # 8-byte Folded Reload
	cmpl	%r13d, %r9d
	leal	1(%r9), %edx
	movl	%edx, %r9d
	jl	.LBB10_23
# BB#26:                                # %._crit_edge248
                                        #   in Loop: Header=BB10_22 Depth=2
	addl	$-10000, %r11d          # imm = 0xD8F0
	je	.LBB10_27
# BB#30:                                #   in Loop: Header=BB10_22 Depth=2
	imull	$-10000, 32(%rsp), %ecx # 4-byte Folded Reload
                                        # imm = 0xD8F0
	addl	%ecx, %eax
	cltd
	idivl	%r11d
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	56(%rsp), %ebp          # 4-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB10_31
	.p2align	4, 0x90
.LBB10_27:                              #   in Loop: Header=BB10_22 Depth=2
	movl	56(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %edx
	callq	median
	movq	%rbx, %rdx
                                        # kill: %AL<def> %AL<kill> %EAX<def>
	movq	80(%rsp), %rbx          # 8-byte Reload
.LBB10_31:                              #   in Loop: Header=BB10_22 Depth=2
	movb	%al, (%rbx)
	incq	%rbx
	incq	%rdx
	movl	8(%rsp), %ecx
	movl	%ecx, %eax
	subl	%r13d, %eax
	cltq
	cmpq	%rax, %rdx
	jl	.LBB10_22
.LBB10_32:                              # %._crit_edge254
                                        #   in Loop: Header=BB10_19 Depth=1
	incl	%ebp
	movl	12(%rsp), %eax
	subl	%r13d, %eax
	cmpl	%eax, %ebp
	jl	.LBB10_19
.LBB10_37:                              # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_38:
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	movl	$.L.str.25, %edi
	movb	$1, %al
	callq	printf
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.Lstr.14, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.LBB10_39:
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	printf
	xorl	%edi, %edi
	callq	exit
.Lfunc_end10:
	.size	susan_smoothing, .Lfunc_end10-susan_smoothing
	.cfi_endproc

	.globl	edge_draw
	.p2align	4, 0x90
	.type	edge_draw,@function
edge_draw:                              # @edge_draw
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 16
.Lcfi99:
	.cfi_offset %rbx, -16
	imull	%edx, %ecx
	testl	%r8d, %r8d
	jne	.LBB11_6
# BB#1:                                 # %.preheader42
	testl	%ecx, %ecx
	jle	.LBB11_18
# BB#2:                                 # %.lr.ph47
	movslq	%edx, %r10
	movq	%r10, %r9
	negq	%r9
	leaq	-2(%r10), %r8
	movl	%ecx, %r11d
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	cmpb	$7, (%rdx)
	ja	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	movq	%rdx, %rax
	subq	%rsi, %rax
	addq	%rdi, %rax
	movb	$-1, -1(%r9,%rax)
	movb	$-1, (%r9,%rax)
	movb	$-1, 1(%r9,%rax)
	leaq	(%rax,%r9), %rax
	leaq	1(%r8,%rax), %rbx
	movb	$-1, -1(%r10,%rax)
	movb	$-1, 1(%r10,%rax)
	movb	$-1, (%r10,%rbx)
	movb	$-1, 1(%r10,%rbx)
	movb	$-1, 2(%r10,%rbx)
.LBB11_5:                               #   in Loop: Header=BB11_3 Depth=1
	incq	%rdx
	decl	%r11d
	jne	.LBB11_3
.LBB11_6:                               # %.preheader
	testl	%ecx, %ecx
	jle	.LBB11_18
# BB#7:                                 # %.lr.ph
	xorl	%edx, %edx
	testb	$1, %cl
	movq	%rsi, %rax
	je	.LBB11_11
# BB#8:
	cmpb	$7, (%rsi)
	ja	.LBB11_10
# BB#9:
	movb	$0, (%rdi)
.LBB11_10:
	leaq	1(%rsi), %rax
	movl	$1, %edx
.LBB11_11:                              # %.prol.loopexit
	cmpl	$1, %ecx
	je	.LBB11_18
# BB#12:                                # %.lr.ph.new
	incq	%rax
	subl	%edx, %ecx
	.p2align	4, 0x90
.LBB11_13:                              # =>This Inner Loop Header: Depth=1
	cmpb	$7, -1(%rax)
	ja	.LBB11_15
# BB#14:                                #   in Loop: Header=BB11_13 Depth=1
	leaq	-1(%rax), %rdx
	subq	%rsi, %rdx
	movb	$0, (%rdi,%rdx)
.LBB11_15:                              #   in Loop: Header=BB11_13 Depth=1
	cmpb	$8, (%rax)
	jae	.LBB11_17
# BB#16:                                #   in Loop: Header=BB11_13 Depth=1
	movq	%rax, %rdx
	subq	%rsi, %rdx
	movb	$0, (%rdi,%rdx)
.LBB11_17:                              #   in Loop: Header=BB11_13 Depth=1
	addq	$2, %rax
	addl	$-2, %ecx
	jne	.LBB11_13
.LBB11_18:                              # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end11:
	.size	edge_draw, .Lfunc_end11-edge_draw
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4604480259023595110     # double 0.69999999999999996
	.text
	.globl	susan_thin
	.p2align	4, 0x90
	.type	susan_thin,@function
susan_thin:                             # @susan_thin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi103:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi104:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 56
.Lcfi106:
	.cfi_offset %rbx, -56
.Lcfi107:
	.cfi_offset %r12, -48
.Lcfi108:
	.cfi_offset %r13, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	addl	$-4, %ecx
	movl	%ecx, -12(%rsp)         # 4-byte Spill
	cmpl	$5, %ecx
	jl	.LBB12_71
# BB#1:                                 # %.preheader459.lr.ph
	leal	-4(%rdx), %r12d
	movslq	%edx, %r15
	movsd	.LCPI12_0(%rip), %xmm0  # xmm0 = mem[0],zero
	movl	$4, %r14d
                                        # implicit-def: %EAX
	movq	%rax, -8(%rsp)          # 8-byte Spill
                                        # implicit-def: %EAX
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	movl	%r12d, -92(%rsp)        # 4-byte Spill
	movq	%r15, -32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB12_2:                               # %.preheader459
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_9 Depth 2
	cmpl	$5, %r12d
	jl	.LBB12_70
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB12_2 Depth=1
	movl	$4, %r13d
	jmp	.LBB12_9
.LBB12_4:                               #   in Loop: Header=BB12_9 Depth=2
	cmpb	$7, (%rsi,%rax)
	ja	.LBB12_6
# BB#5:                                 #   in Loop: Header=BB12_9 Depth=2
	addl	%r15d, %r15d
	addl	%ecx, %ecx
	leal	(%r14,%r14,2), %r14d
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax,2), %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	shll	$2, %edi
	xorl	%eax, %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	jmp	.LBB12_25
.LBB12_6:                               #   in Loop: Header=BB12_9 Depth=2
	movq	%r15, %r10
	cmpb	$7, 1(%rsi,%rax)
	movq	-80(%rsp), %rax         # 8-byte Reload
	movq	-32(%rsp), %r15         # 8-byte Reload
	ja	.LBB12_8
# BB#7:                                 #   in Loop: Header=BB12_9 Depth=2
	addl	%r12d, %r12d
	addl	%eax, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	leal	(%rdi,%rdi,2), %edi
	leal	(%r10,%r10,2), %r10d
	shll	$2, %r14d
	xorl	%ecx, %ecx
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	jmp	.LBB12_29
.LBB12_8:                               #   in Loop: Header=BB12_9 Depth=2
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	jmp	.LBB12_29
	.p2align	4, 0x90
.LBB12_9:                               # %.lr.ph
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r14d, %eax
	imull	%edx, %eax
	addl	%r13d, %eax
	movslq	%eax, %rbp
	movzbl	(%rsi,%rbp), %r8d
	cmpb	$7, %r8b
	ja	.LBB12_69
# BB#10:                                #   in Loop: Header=BB12_9 Depth=2
	leal	-1(%r14), %r11d
	movl	%r11d, %eax
	imull	%edx, %eax
	movslq	%eax, %rbx
	addq	%rsi, %rbx
	movslq	%r13d, %rcx
	cmpb	$8, (%rcx,%rbx)
	sbbl	%r9d, %r9d
	andl	$1, %r9d
	cmpb	$8, -1(%rcx,%rbx)
	adcl	$0, %r9d
	cmpb	$8, 1(%rcx,%rbx)
	leaq	(%rbx,%rcx), %rcx
	adcl	$0, %r9d
	cmpb	$8, -1(%r15,%rcx)
	movl	(%rdi,%rbp,4), %ebx
	adcl	$0, %r9d
	cmpb	$8, 1(%r15,%rcx)
	leaq	-1(%r15,%rcx), %rcx
	adcl	$0, %r9d
	cmpb	$8, (%r15,%rcx)
	adcl	$0, %r9d
	cmpb	$8, 1(%r15,%rcx)
	adcl	$0, %r9d
	cmpb	$8, 2(%r15,%rcx)
	adcl	$0, %r9d
	cmpl	$1, %r9d
	je	.LBB12_13
# BB#11:                                #   in Loop: Header=BB12_9 Depth=2
	testl	%r9d, %r9d
	jne	.LBB12_37
# BB#12:                                #   in Loop: Header=BB12_9 Depth=2
	movb	$100, (%rsi,%rbp)
	jmp	.LBB12_69
.LBB12_13:                              #   in Loop: Header=BB12_9 Depth=2
	cmpb	$5, %r8b
	ja	.LBB12_69
# BB#14:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%r13d, %eax
	movslq	%eax, %r10
	movl	4(%rdi,%r10,4), %r15d
	movl	4(%rdi,%rbp,4), %ecx
	leal	1(%r14), %eax
	imull	%edx, %eax
	addl	%r13d, %eax
	cltq
	movl	-4(%rdi,%rax,4), %r12d
	movl	(%rdi,%rax,4), %edx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	movl	4(%rdi,%rax,4), %edx
	cmpb	$7, -1(%rsi,%r10)
	movq	%r14, -72(%rsp)         # 8-byte Spill
	movl	%ebx, -36(%rsp)         # 4-byte Spill
	ja	.LBB12_16
# BB#15:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%r15d, %r15d
	movq	%r15, -80(%rsp)         # 8-byte Spill
	addl	%r12d, %r12d
	leal	(%rcx,%rcx,2), %ecx
	movq	-88(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax,2), %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	shll	$2, %edx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	xorl	%r10d, %r10d
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	movq	-32(%rsp), %r15         # 8-byte Reload
	jmp	.LBB12_30
.LBB12_16:                              #   in Loop: Header=BB12_9 Depth=2
	movq	%r15, -80(%rsp)         # 8-byte Spill
	movl	-4(%rdi,%rbp,4), %r15d
	cmpb	$7, (%rsi,%r10)
	ja	.LBB12_18
# BB#17:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%r15d, %r15d
	addl	%ecx, %ecx
	leal	(%r12,%r12,2), %r12d
	leal	(%rdx,%rdx,2), %edx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	movq	-88(%rsp), %rax         # 8-byte Reload
	shll	$2, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	jmp	.LBB12_20
.LBB12_18:                              #   in Loop: Header=BB12_9 Depth=2
	movl	-4(%rdi,%r10,4), %r14d
	cmpb	$7, 1(%rsi,%r10)
	ja	.LBB12_21
# BB#19:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%r14d, %r14d
	addl	%edx, %edx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	leal	(%r15,%r15,2), %r15d
	movq	-88(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax,2), %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	shll	$2, %r12d
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
.LBB12_20:                              # %.preheader.preheader
                                        #   in Loop: Header=BB12_9 Depth=2
	movq	%r15, %r10
	movq	-32(%rsp), %r15         # 8-byte Reload
	jmp	.LBB12_30
.LBB12_21:                              #   in Loop: Header=BB12_9 Depth=2
	movl	(%rdi,%r10,4), %edi
	cmpb	$7, -1(%rsi,%rbp)
	ja	.LBB12_23
# BB#22:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%edi, %edi
	movq	-88(%rsp), %rax         # 8-byte Reload
	addl	%eax, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax,2), %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	leal	(%rdx,%rdx,2), %edx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	shll	$2, %ecx
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
	xorl	%r14d, %r14d
	jmp	.LBB12_28
.LBB12_23:                              #   in Loop: Header=BB12_9 Depth=2
	cmpb	$7, 1(%rsi,%rbp)
	ja	.LBB12_26
# BB#24:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%edi, %edi
	movq	-88(%rsp), %rax         # 8-byte Reload
	addl	%eax, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leal	(%r14,%r14,2), %r14d
	leal	(%r12,%r12,2), %r12d
	shll	$2, %r15d
	xorl	%eax, %eax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
.LBB12_25:                              # %.preheader.preheader
                                        #   in Loop: Header=BB12_9 Depth=2
	movq	%r15, %r10
	jmp	.LBB12_28
.LBB12_26:                              #   in Loop: Header=BB12_9 Depth=2
	cmpb	$7, -1(%rsi,%rax)
	ja	.LBB12_4
# BB#27:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%r14d, %r14d
	addl	%edx, %edx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
	leal	(%rdi,%rdi,2), %edi
	leal	(%rcx,%rcx,2), %ecx
	movq	-80(%rsp), %rax         # 8-byte Reload
	shll	$2, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r10d, %r10d
.LBB12_28:                              # %.preheader.preheader
                                        #   in Loop: Header=BB12_9 Depth=2
	movq	-32(%rsp), %r15         # 8-byte Reload
.LBB12_29:                              # %.preheader.preheader
                                        #   in Loop: Header=BB12_9 Depth=2
	movq	%rdi, %rbp
.LBB12_30:                              # %.preheader.preheader
                                        #   in Loop: Header=BB12_9 Depth=2
	testl	%r14d, %r14d
	movl	$0, %eax
	cmovsl	%eax, %r14d
	movq	-8(%rsp), %rdi          # 8-byte Reload
	cmovgl	%eax, %edi
	movq	-24(%rsp), %rbx         # 8-byte Reload
	cmovgl	%eax, %ebx
	cmpl	%r14d, %ebp
	cmovgel	%ebp, %r14d
	movl	$1, %edx
	cmovgl	%edx, %edi
	cmovgl	%eax, %ebx
	movq	-80(%rsp), %rbp         # 8-byte Reload
	cmpl	%r14d, %ebp
	cmovgel	%ebp, %r14d
	movq	%rdi, %rbp
	cmovgl	%eax, %ebx
	movl	$2, %edi
	cmovgl	%edi, %ebp
	cmpl	%r14d, %r10d
	cmovgel	%r10d, %r14d
	cmovgl	%eax, %ebp
	cmovgl	%edx, %ebx
	cmpl	%r14d, %ecx
	cmovgel	%ecx, %r14d
	cmovgl	%edx, %ebx
	cmovgl	%edi, %ebp
	cmpl	%r14d, %r12d
	cmovgel	%r12d, %r14d
	cmovgl	%eax, %ebp
	cmovgl	%edi, %ebx
	movq	-88(%rsp), %rax         # 8-byte Reload
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	cmovgl	%edx, %ebp
	cmovgl	%edi, %ebx
	movq	-56(%rsp), %rax         # 8-byte Reload
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	cmovgl	%edi, %ebx
	movq	%rbx, -24(%rsp)         # 8-byte Spill
	cmovgl	%edi, %ebp
	testl	%r14d, %r14d
	movq	%rbp, -8(%rsp)          # 8-byte Spill
	jle	.LBB12_35
# BB#31:                                #   in Loop: Header=BB12_9 Depth=2
	cmpb	$4, %r8b
	movb	$4, %al
	jb	.LBB12_33
# BB#32:                                #   in Loop: Header=BB12_9 Depth=2
	incb	%r8b
	movl	%r8d, %eax
.LBB12_33:                              #   in Loop: Header=BB12_9 Depth=2
	movq	-24(%rsp), %rbx         # 8-byte Reload
	addl	%ebx, %r11d
	movl	%r11d, %ecx
	movq	-64(%rsp), %rdx         # 8-byte Reload
	imull	%edx, %ecx
	leal	(%r13,%rbp), %edi
	leal	-1(%rcx,%rdi), %ecx
	movslq	%ecx, %rcx
	movb	%al, (%rsi,%rcx)
	leal	(%rbp,%rbx,2), %eax
	cmpl	$2, %eax
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movl	-92(%rsp), %r12d        # 4-byte Reload
	movq	-72(%rsp), %r14         # 8-byte Reload
	jg	.LBB12_36
# BB#34:                                #   in Loop: Header=BB12_9 Depth=2
	leal	-2(%r13,%rbp), %r13d
	cmpl	$3, %r11d
	movl	$4, %eax
	cmovlel	%eax, %r11d
	cmpl	$3, %r13d
	cmovlel	%eax, %r13d
	movl	%r11d, %r14d
	jmp	.LBB12_36
.LBB12_35:                              #   in Loop: Header=BB12_9 Depth=2
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movl	-92(%rsp), %r12d        # 4-byte Reload
	movq	-72(%rsp), %r14         # 8-byte Reload
.LBB12_36:                              #   in Loop: Header=BB12_9 Depth=2
	movl	-36(%rsp), %ebx         # 4-byte Reload
.LBB12_37:                              #   in Loop: Header=BB12_9 Depth=2
	cmpl	$2, %r9d
	jne	.LBB12_47
# BB#38:                                #   in Loop: Header=BB12_9 Depth=2
	leal	-1(%r14), %r12d
	movl	%r12d, -88(%rsp)        # 4-byte Spill
	imull	%edx, %r12d
	addl	%r13d, %r12d
	movslq	%r12d, %r9
	movq	%r14, %rax
	movzbl	-1(%rsi,%r9), %r14d
	movzbl	1(%rsi,%r9), %r8d
	cmpb	$8, %r8b
	sbbl	%r10d, %r10d
	andl	$1, %r10d
	movq	%rax, %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	leal	1(%rax), %ebp
	imull	%edx, %ebp
	addl	%r13d, %ebp
	cmpb	$8, %r14b
	movslq	%ebp, %r11
	movzbl	-1(%rsi,%r11), %eax
	movzbl	1(%rsi,%r11), %ecx
	movl	%r10d, %edi
	adcl	$0, %edi
	cmpb	$8, %al
	adcl	$0, %edi
	cmpb	$8, %cl
	adcl	$0, %edi
	cmpl	$2, %edi
	jne	.LBB12_51
# BB#39:                                #   in Loop: Header=BB12_9 Depth=2
	cmpb	$8, %cl
	sbbb	%dil, %dil
	cmpb	$8, %al
	sbbb	%al, %al
	cmpb	$8, %r8b
	sbbb	%cl, %cl
	orb	%al, %cl
	cmpb	$8, %r14b
	sbbb	%al, %al
	andb	$1, %cl
	cmpb	$1, %cl
	jne	.LBB12_51
# BB#40:                                #   in Loop: Header=BB12_9 Depth=2
	orb	%dil, %al
	andb	$1, %al
	je	.LBB12_51
# BB#41:                                #   in Loop: Header=BB12_9 Depth=2
	xorl	%ecx, %ecx
	cmpb	$7, %r8b
	movl	$0, %eax
	movl	$-1, %edx
	cmoval	%edx, %eax
	seta	%cl
	cmpb	$8, %r8b
	sbbl	%edi, %edi
	cmpb	$8, %r14b
	cmovbl	%eax, %r10d
	cmovbl	%edi, %ecx
	movq	-72(%rsp), %r14         # 8-byte Reload
	leal	(%rcx,%r14), %eax
	movq	-64(%rsp), %rdx         # 8-byte Reload
	imull	%edx, %eax
	leal	(%r10,%r13), %edi
	addl	%eax, %edi
	movslq	%edi, %rax
	movq	-48(%rsp), %rdi         # 8-byte Reload
	cvtsi2ssl	(%rdi,%rax,4), %xmm1
	cvtsi2ssl	%ebx, %xmm2
	divss	%xmm2, %xmm1
	cvtss2sd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB12_68
# BB#42:                                #   in Loop: Header=BB12_9 Depth=2
	testl	%r10d, %r10d
	jne	.LBB12_61
# BB#43:                                #   in Loop: Header=BB12_9 Depth=2
	leal	(%r14,%rcx,2), %ebx
	imull	%edx, %ebx
	addl	%r13d, %ebx
	movslq	%ebx, %r8
	cmpb	$8, (%rsi,%r8)
	jb	.LBB12_61
# BB#44:                                #   in Loop: Header=BB12_9 Depth=2
	cmpb	$8, -1(%rsi,%r8)
	jb	.LBB12_61
# BB#45:                                #   in Loop: Header=BB12_9 Depth=2
	cmpb	$8, 1(%rsi,%r8)
	jb	.LBB12_61
# BB#46:                                # %._crit_edge486
                                        #   in Loop: Header=BB12_9 Depth=2
	movl	%r14d, %ecx
	imull	%edx, %ecx
	addl	%r13d, %ecx
	movl	-92(%rsp), %r12d        # 4-byte Reload
	jmp	.LBB12_65
.LBB12_47:                              #   in Loop: Header=BB12_9 Depth=2
	cmpl	$3, %r9d
	jl	.LBB12_69
# BB#48:                                #   in Loop: Header=BB12_9 Depth=2
	leal	-1(%r14), %ebx
	movl	%ebx, %eax
	imull	%edx, %eax
	addl	%r13d, %eax
	movq	%r14, %rbp
	movslq	%eax, %r14
	movzbl	(%rsi,%r14), %r11d
	movl	%ebp, %eax
	imull	%edx, %eax
	addl	%r13d, %eax
	movslq	%eax, %r9
	movzbl	-1(%rsi,%r9), %eax
	movzbl	1(%rsi,%r9), %r10d
	cmpb	$8, %r10b
	sbbl	%edi, %edi
	andl	$1, %edi
	movq	%rbp, %rcx
	movq	%rcx, -72(%rsp)         # 8-byte Spill
	leal	1(%rbp), %ecx
	imull	%edx, %ecx
	addl	%r13d, %ecx
	cmpb	$8, %r11b
	movslq	%ecx, %rbp
	movzbl	(%rsi,%rbp), %ecx
	adcl	$0, %edi
	cmpb	$8, %cl
	adcl	$0, %edi
	cmpb	$8, %al
	adcl	$0, %edi
	cmpl	$2, %edi
	jb	.LBB12_60
# BB#49:                                #   in Loop: Header=BB12_9 Depth=2
	cmpb	$8, %al
	sbbb	%dil, %dil
	cmpb	$8, %r11b
	sbbb	%r11b, %r11b
	cmpb	$8, -1(%rsi,%r14)
	sbbb	%al, %al
	orb	%r11b, %al
	andb	$1, %al
	movl	%edi, %r12d
	andb	%al, %r12b
	cmpb	$8, -1(%rsi,%rbp)
	sbbb	%r8b, %r8b
	orb	%dil, %r8b
	cmpb	$8, %cl
	sbbb	%cl, %cl
	andb	$1, %r8b
	movl	%ecx, %edi
	andb	%r8b, %dil
	cmpb	$8, 1(%rsi,%rbp)
	sbbb	%bpl, %bpl
	orb	%cl, %bpl
	cmpb	$8, %r10b
	sbbb	%r10b, %r10b
	movl	%r10d, %ecx
	andb	%bpl, %cl
	cmpb	$8, 1(%rsi,%r14)
	sbbb	%r14b, %r14b
	orb	%r10b, %r14b
	andb	%r14b, %r11b
	movzbl	%al, %r10d
	andb	$1, %r14b
	movzbl	%r14b, %eax
	andb	$1, %bpl
	movzbl	%bpl, %r14d
	movzbl	%r8b, %r8d
	andb	$1, %r11b
	andb	$1, %cl
	addl	%r10d, %eax
	movzbl	%r12b, %ebp
	subl	%ebp, %eax
	addl	%r8d, %eax
	movzbl	%r11b, %ebp
	subl	%ebp, %eax
	addl	%r14d, %eax
	movzbl	%dil, %edi
	subl	%edi, %eax
	movzbl	%cl, %ecx
	subl	%ecx, %eax
	cmpl	$1, %eax
	jg	.LBB12_58
# BB#50:                                #   in Loop: Header=BB12_9 Depth=2
	movb	$100, (%rsi,%r9)
	addl	$-2, %r13d
	cmpl	$5, -72(%rsp)           # 4-byte Folded Reload
	movl	$4, %eax
	cmovll	%eax, %ebx
	cmpl	$3, %r13d
	cmovlel	%eax, %r13d
	movl	%ebx, %r14d
	jmp	.LBB12_67
.LBB12_51:                              #   in Loop: Header=BB12_9 Depth=2
	movzbl	(%rsi,%r9), %ebp
	movq	-72(%rsp), %r14         # 8-byte Reload
	movl	%r14d, %eax
	movq	-64(%rsp), %rdx         # 8-byte Reload
	imull	%edx, %eax
	addl	%r13d, %eax
	movslq	%eax, %rdi
	movzbl	-1(%rsi,%rdi), %r12d
	movzbl	1(%rsi,%rdi), %r10d
	cmpb	$8, %r10b
	sbbl	%ecx, %ecx
	andl	$1, %ecx
	cmpb	$8, %bpl
	movzbl	(%rsi,%r11), %eax
	adcl	$0, %ecx
	cmpb	$8, %al
	adcl	$0, %ecx
	cmpb	$8, %r12b
	adcl	$0, %ecx
	cmpl	$2, %ecx
	jne	.LBB12_59
# BB#52:                                #   in Loop: Header=BB12_9 Depth=2
	cmpb	$8, %r12b
	sbbb	%dl, %dl
	cmpb	$8, %al
	sbbb	%r8b, %r8b
	cmpb	$8, %r10b
	sbbb	%cl, %cl
	cmpb	$8, %bpl
	sbbb	%r14b, %r14b
	orb	%r8b, %r14b
	andb	$1, %r14b
	cmpb	$1, %r14b
	jne	.LBB12_58
# BB#53:                                #   in Loop: Header=BB12_9 Depth=2
	orb	%dl, %cl
	andb	$1, %cl
	je	.LBB12_58
# BB#54:                                #   in Loop: Header=BB12_9 Depth=2
	cmpb	$8, %r12b
	sbbb	%r14b, %r14b
	cmpb	$8, -2(%rsi,%r9)
	sbbb	%r12b, %r12b
	cmpb	$8, -2(%rsi,%r11)
	sbbb	%cl, %cl
	movb	%cl, -80(%rsp)          # 1-byte Spill
	cmpb	$8, %al
	sbbb	%al, %al
	movb	%al, -56(%rsp)          # 1-byte Spill
	movq	-72(%rsp), %rdx         # 8-byte Reload
	leal	2(%rdx), %eax
	movq	-64(%rsp), %rcx         # 8-byte Reload
	imull	%ecx, %eax
	addl	%r13d, %eax
	cltq
	cmpb	$8, -1(%rsi,%rax)
	sbbb	%bl, %bl
	movb	%bl, -36(%rsp)          # 1-byte Spill
	cmpb	$8, 1(%rsi,%rax)
	sbbb	%bl, %bl
	cmpb	$8, %r10b
	sbbb	%al, %al
	cmpb	$8, 2(%rsi,%r9)
	sbbb	%r10b, %r10b
	cmpb	$8, 2(%rsi,%r11)
	sbbb	%r9b, %r9b
	cmpb	$8, %bpl
	sbbb	%r8b, %r8b
	leal	-2(%rdx), %ebp
	imull	%ecx, %ebp
	addl	%r13d, %ebp
	movslq	%ebp, %rbp
	cmpb	$8, -1(%rsi,%rbp)
	sbbb	%r11b, %r11b
	cmpb	$8, 1(%rsi,%rbp)
	sbbb	%cl, %cl
	orb	%r11b, %cl
	andb	%r8b, %cl
	testb	$1, %cl
	jne	.LBB12_66
# BB#55:                                #   in Loop: Header=BB12_9 Depth=2
	orb	-80(%rsp), %r12b        # 1-byte Folded Reload
	andb	%r12b, %r14b
	andb	$1, %r14b
	jne	.LBB12_66
# BB#56:                                #   in Loop: Header=BB12_9 Depth=2
	orb	%r9b, %r10b
	andb	%r10b, %al
	andb	$1, %al
	jne	.LBB12_66
# BB#57:                                #   in Loop: Header=BB12_9 Depth=2
	movzbl	-36(%rsp), %eax         # 1-byte Folded Reload
	orb	%bl, %al
	movzbl	-56(%rsp), %ecx         # 1-byte Folded Reload
	andb	%al, %cl
	andb	$1, %cl
	jne	.LBB12_66
.LBB12_58:                              #   in Loop: Header=BB12_9 Depth=2
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movl	-92(%rsp), %r12d        # 4-byte Reload
	movq	-72(%rsp), %r14         # 8-byte Reload
	jmp	.LBB12_69
.LBB12_59:                              #   in Loop: Header=BB12_9 Depth=2
	movq	-48(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB12_68
.LBB12_60:                              #   in Loop: Header=BB12_9 Depth=2
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movq	-72(%rsp), %r14         # 8-byte Reload
	jmp	.LBB12_69
.LBB12_61:                              #   in Loop: Header=BB12_9 Depth=2
	testl	%ecx, %ecx
	jne	.LBB12_68
# BB#62:                                #   in Loop: Header=BB12_9 Depth=2
	movl	%r14d, %ecx
	imull	%edx, %ecx
	addl	%r13d, %ecx
	leal	(%rcx,%r10,2), %ebx
	movslq	%ebx, %rbx
	cmpb	$8, (%rsi,%rbx)
	jb	.LBB12_68
# BB#63:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%r10d, %r10d
	addl	%r10d, %ebp
	movslq	%ebp, %rbp
	cmpb	$8, (%rsi,%rbp)
	jb	.LBB12_68
# BB#64:                                #   in Loop: Header=BB12_9 Depth=2
	addl	%r12d, %r10d
	movslq	%r10d, %rbp
	cmpb	$8, (%rsi,%rbp)
	movl	-92(%rsp), %r12d        # 4-byte Reload
	jb	.LBB12_69
.LBB12_65:                              #   in Loop: Header=BB12_9 Depth=2
	movslq	%ecx, %rcx
	movb	$100, (%rsi,%rcx)
	movb	$3, (%rsi,%rax)
	jmp	.LBB12_69
.LBB12_66:                              #   in Loop: Header=BB12_9 Depth=2
	movb	$100, (%rsi,%rdi)
	addl	$-2, %r13d
	cmpl	$5, -72(%rsp)           # 4-byte Folded Reload
	movl	$4, %eax
	movl	-88(%rsp), %ecx         # 4-byte Reload
	cmovll	%eax, %ecx
	cmpl	$3, %r13d
	cmovlel	%eax, %r13d
	movl	%ecx, %r14d
.LBB12_67:                              # %.thread450
                                        #   in Loop: Header=BB12_9 Depth=2
	movq	-48(%rsp), %rdi         # 8-byte Reload
	movq	-64(%rsp), %rdx         # 8-byte Reload
.LBB12_68:                              # %.thread450
                                        #   in Loop: Header=BB12_9 Depth=2
	movl	-92(%rsp), %r12d        # 4-byte Reload
	.p2align	4, 0x90
.LBB12_69:                              # %.thread450
                                        #   in Loop: Header=BB12_9 Depth=2
	incl	%r13d
	cmpl	%r12d, %r13d
	jl	.LBB12_9
.LBB12_70:                              # %._crit_edge
                                        #   in Loop: Header=BB12_2 Depth=1
	incl	%r14d
	cmpl	-12(%rsp), %r14d        # 4-byte Folded Reload
	jl	.LBB12_2
.LBB12_71:                              # %._crit_edge477
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	susan_thin, .Lfunc_end12-susan_thin
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4606281698874543309     # double 0.90000000000000002
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI13_1:
	.long	1056964608              # float 0.5
.LCPI13_2:
	.long	1073741824              # float 2
.LCPI13_3:
	.long	1232348160              # float 1.0E+6
.LCPI13_5:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	susan_edges
	.p2align	4, 0x90
	.type	susan_edges,@function
susan_edges:                            # @susan_edges
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi114:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi115:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 56
	subq	$792, %rsp              # imm = 0x318
.Lcfi118:
	.cfi_def_cfa_offset 848
.Lcfi119:
	.cfi_offset %rbx, -56
.Lcfi120:
	.cfi_offset %r12, -48
.Lcfi121:
	.cfi_offset %r13, -40
.Lcfi122:
	.cfi_offset %r14, -32
.Lcfi123:
	.cfi_offset %r15, -24
.Lcfi124:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movl	%r8d, 132(%rsp)         # 4-byte Spill
	movq	%rcx, 712(%rsp)         # 8-byte Spill
	movq	%rdx, 312(%rsp)         # 8-byte Spill
	movq	%rsi, %rcx
	movq	%rdi, 320(%rsp)         # 8-byte Spill
	movl	848(%rsp), %ebx
	movl	%ebx, %eax
	movq	%r9, 40(%rsp)           # 8-byte Spill
	imull	%r9d, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	callq	memset
	leal	-3(%rbx), %eax
	cmpl	$4, %eax
	jl	.LBB13_7
# BB#1:                                 # %.preheader848.lr.ph
	movslq	40(%rsp), %rcx          # 4-byte Folded Reload
	leaq	-3(%rcx), %r15
	movl	%r15d, %r8d
	movl	%eax, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	-8(%rcx,%rcx), %rdi
	leaq	14(%rcx,%rdi), %rbp
	leaq	-12(%rcx,%rcx), %rax
	leaq	-12(%rdi,%rcx,2), %rbx
	leaq	-15(%rcx,%rcx,2), %r10
	leaq	20(%r10,%rcx,2), %rdx
	leaq	-6(%rax,%rcx,2), %rax
	leaq	20(%rax,%rcx,2), %r12
	leaq	(%rcx,%rcx,2), %rsi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	12(%rax,%rsi,4), %r11
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	leaq	(,%rcx,4), %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	addq	$-3, %r8
	movq	%r8, 256(%rsp)          # 8-byte Spill
	movl	$3, %ecx
	movq	320(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB13_2:                               # %.preheader848
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_3 Depth 2
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	cmpl	$4, %r15d
	movq	256(%rsp), %r9          # 8-byte Reload
	movq	%r11, 16(%rsp)          # 8-byte Spill
	movq	%r13, 272(%rsp)         # 8-byte Spill
	jl	.LBB13_6
	.p2align	4, 0x90
.LBB13_3:                               #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	18(%r10,%r13), %r8d
	addq	712(%rsp), %r8          # 8-byte Folded Reload
	movzbl	2(%r13), %esi
	movq	%r8, %r14
	subq	%rsi, %r14
	movzbl	(%r14), %esi
	movzbl	3(%r13), %eax
	movq	%r8, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	addl	%esi, %eax
	movzbl	4(%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	4(%r15,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	5(%r15,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	6(%r15,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	7(%r15,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	8(%r15,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	8(%rdi,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	9(%rdi,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	10(%rdi,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	11(%rdi,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	12(%rdi,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	13(%rdi,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	14(%rdi,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	-6(%rbp,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	-5(%rbp,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	-4(%rbp,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	-2(%rbp,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	-1(%rbp,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	(%rbp,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	20(%rbx,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	21(%rbx,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	22(%rbx,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	23(%rbx,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	24(%rbx,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	25(%rbx,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	26(%rbx,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	-4(%rdx,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	-3(%rdx,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	-2(%rdx,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	-1(%rdx,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	(%rdx,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	(%r12,%r13), %eax
	movq	%r8, %rsi
	subq	%rax, %rsi
	movzbl	(%rsi), %eax
	addl	%ecx, %eax
	movzbl	1(%r12,%r13), %ecx
	movq	%r8, %rsi
	subq	%rcx, %rsi
	movzbl	(%rsi), %ecx
	addl	%eax, %ecx
	movzbl	2(%r12,%r13), %eax
	subq	%rax, %r8
	movzbl	(%r8), %eax
	leal	100(%rax,%rcx), %esi
	cmpl	132(%rsp), %esi         # 4-byte Folded Reload
	jg	.LBB13_5
# BB#4:                                 #   in Loop: Header=BB13_3 Depth=2
	movl	132(%rsp), %eax         # 4-byte Reload
	subl	%esi, %eax
	movl	%eax, (%r11)
.LBB13_5:                               #   in Loop: Header=BB13_3 Depth=2
	incq	%r13
	addq	$4, %r11
	decq	%r9
	jne	.LBB13_3
.LBB13_6:                               # %._crit_edge856
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	272(%rsp), %r13         # 8-byte Reload
	addq	264(%rsp), %r13         # 8-byte Folded Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	addq	104(%rsp), %r11         # 8-byte Folded Reload
	cmpq	112(%rsp), %rcx         # 8-byte Folded Reload
	jne	.LBB13_2
.LBB13_7:                               # %.preheader847
	movl	848(%rsp), %ecx
	addl	$-4, %ecx
	cmpl	$5, %ecx
	jl	.LBB13_43
# BB#8:                                 # %.preheader.lr.ph
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	-4(%rax), %edx
	movslq	%eax, %rsi
	movl	%ecx, %eax
	movq	%rax, 720(%rsp)         # 8-byte Spill
	leaq	-12(%rsi,%rsi), %rax
	leaq	29(%rax,%rsi,2), %rcx
	movq	%rcx, 544(%rsp)         # 8-byte Spill
	leaq	-12(%rsi,%rsi,2), %rdi
	movq	320(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %rdi
	movq	%rdi, 568(%rsp)         # 8-byte Spill
	leaq	28(%rax,%rsi,2), %rdi
	movq	%rdi, 536(%rsp)         # 8-byte Spill
	leaq	27(%rax,%rsi,2), %rdi
	movq	%rdi, 528(%rsp)         # 8-byte Spill
	leaq	21(%rax,%rsi,2), %rdi
	movq	%rdi, 520(%rsp)         # 8-byte Spill
	leaq	-3(%rsi,%rsi), %rdi
	addq	%rcx, %rdi
	movq	%rdi, 560(%rsp)         # 8-byte Spill
	leaq	20(%rax,%rsi,2), %rdi
	movq	%rdi, 512(%rsp)         # 8-byte Spill
	leaq	19(%rax,%rsi,2), %rdi
	movq	%rdi, 504(%rsp)         # 8-byte Spill
	leaq	18(%rax,%rsi,2), %rdi
	movq	%rdi, 496(%rsp)         # 8-byte Spill
	leaq	17(%rax,%rsi,2), %rax
	movq	%rax, 488(%rsp)         # 8-byte Spill
	movq	%rsi, %rax
	shlq	$4, %rax
	addq	32(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, 784(%rsp)         # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,4), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movsd	.LCPI13_0(%rip), %xmm4  # xmm4 = mem[0],zero
	movss	.LCPI13_1(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	leaq	-3(%rsi), %rax
	movq	%rax, 752(%rsp)         # 8-byte Spill
	leaq	-5(%rsi), %rax
	movq	%rax, 744(%rsp)         # 8-byte Spill
	leaq	-6(%rsi), %rax
	movq	%rax, 736(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	leaq	10(%rsi,%rsi,2), %rax
	movq	%rax, 480(%rsp)         # 8-byte Spill
	leaq	9(%rsi,%rsi,2), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	leaq	8(%rsi,%rsi,2), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	leaq	7(%rsi,%rsi,2), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	leaq	6(%rsi,%rsi,2), %rax
	movq	%rax, 448(%rsp)         # 8-byte Spill
	leaq	5(%rsi,%rsi,2), %rax
	movq	%rax, 440(%rsp)         # 8-byte Spill
	leaq	4(%rsi,%rsi,2), %rax
	movq	%rax, 432(%rsp)         # 8-byte Spill
	leaq	10(%rsi,%rsi), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	leaq	9(%rsi,%rsi), %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	8(%rsi,%rsi), %rax
	movq	%rax, 408(%rsp)         # 8-byte Spill
	leaq	6(%rsi,%rsi), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	leaq	5(%rsi,%rsi), %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	4(%rsi,%rsi), %rax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	leaq	4(%rsi), %rax
	movq	%rax, 376(%rsp)         # 8-byte Spill
	movq	%rsi, 552(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rsi,4), %rax
	movq	%rax, 768(%rsp)         # 8-byte Spill
	movq	%rdx, 728(%rsp)         # 8-byte Spill
	leaq	-4(%rdx), %rax
	movq	%rax, 776(%rsp)         # 8-byte Spill
	movl	$4, %ecx
	movl	$9, %eax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB13_9:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	$5, 728(%rsp)           # 4-byte Folded Reload
	jl	.LBB13_42
# BB#10:                                # %.lr.ph
                                        #   in Loop: Header=BB13_9 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	-3(%rax), %rax
	imulq	552(%rsp), %rax         # 8-byte Folded Reload
	addq	320(%rsp), %rax         # 8-byte Folded Reload
	movq	%rax, 760(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	movq	376(%rsp), %r14         # 8-byte Reload
	movq	384(%rsp), %r9          # 8-byte Reload
	movq	392(%rsp), %r12         # 8-byte Reload
	movq	400(%rsp), %r13         # 8-byte Reload
	movq	408(%rsp), %r8          # 8-byte Reload
	movq	416(%rsp), %r10         # 8-byte Reload
	movq	424(%rsp), %r11         # 8-byte Reload
	movq	432(%rsp), %rbp         # 8-byte Reload
	movq	440(%rsp), %rbx         # 8-byte Reload
	movq	448(%rsp), %rax         # 8-byte Reload
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rax         # 8-byte Reload
	movq	%rax, 704(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rax         # 8-byte Reload
	movq	%rax, 696(%rsp)         # 8-byte Spill
	movq	472(%rsp), %rax         # 8-byte Reload
	movq	%rax, 688(%rsp)         # 8-byte Spill
	movq	480(%rsp), %rax         # 8-byte Reload
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	488(%rsp), %rax         # 8-byte Reload
	movq	%rax, 680(%rsp)         # 8-byte Spill
	movq	496(%rsp), %rax         # 8-byte Reload
	movq	%rax, 672(%rsp)         # 8-byte Spill
	movq	504(%rsp), %rax         # 8-byte Reload
	movq	%rax, 664(%rsp)         # 8-byte Spill
	movq	512(%rsp), %rax         # 8-byte Reload
	movq	%rax, 656(%rsp)         # 8-byte Spill
	movq	520(%rsp), %rax         # 8-byte Reload
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rax         # 8-byte Reload
	movq	%rax, 648(%rsp)         # 8-byte Spill
	movq	536(%rsp), %rax         # 8-byte Reload
	movq	%rax, 640(%rsp)         # 8-byte Spill
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	368(%rsp), %rsi         # 8-byte Reload
	movl	$4, %eax
	movq	%rax, 632(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB13_11:                              #   Parent Loop BB13_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	784(%rsp), %rax         # 8-byte Reload
	movl	-20(%rax,%rsi,4), %ecx
	testl	%ecx, %ecx
	jle	.LBB13_41
# BB#12:                                #   in Loop: Header=BB13_11 Depth=2
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movl	132(%rsp), %edx         # 4-byte Reload
	subl	%ecx, %edx
	movq	768(%rsp), %rax         # 8-byte Reload
	movzbl	-5(%rax,%rsi), %r15d
	addq	712(%rsp), %r15         # 8-byte Folded Reload
	cmpl	$600, %edx              # imm = 0x258
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%ecx, 48(%rsp)          # 4-byte Spill
	movq	%rdi, 248(%rsp)         # 8-byte Spill
	movq	%r14, 240(%rsp)         # 8-byte Spill
	movq	%r9, 232(%rsp)          # 8-byte Spill
	movq	%r12, 224(%rsp)         # 8-byte Spill
	movq	%r13, 216(%rsp)         # 8-byte Spill
	movq	%r8, 208(%rsp)          # 8-byte Spill
	movq	%r10, 200(%rsp)         # 8-byte Spill
	movq	%r11, 192(%rsp)         # 8-byte Spill
	jle	.LBB13_13
# BB#16:                                #   in Loop: Header=BB13_11 Depth=2
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movq	344(%rsp), %rcx         # 8-byte Reload
	movzbl	-6(%rcx,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %edi
	movl	%edi, 24(%rsp)          # 4-byte Spill
	movzbl	-5(%rcx,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %edx
	addl	%edi, %edx
	movzbl	-4(%rcx,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	addl	%eax, %edx
	movl	%edx, 120(%rsp)         # 4-byte Spill
	movq	560(%rsp), %r9          # 8-byte Reload
	movzbl	-4(%r9,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r8d
	addl	%r8d, %r8d
	movzbl	-3(%r9,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r10d
	movzbl	-2(%r9,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r13d
	movzbl	-1(%r9,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %edx
	movzbl	(%r9,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %esi
	movzbl	(%r9,%r14), %eax
	negq	%rax
	movzbl	(%r15,%rax), %ebx
	movzbl	1(%r9,%r14), %eax
	negq	%rax
	movzbl	(%r15,%rax), %ebp
	leal	(%rbp,%rbp), %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movzbl	2(%r9,%r14), %eax
	negq	%rax
	movq	%r11, %r12
	movzbl	(%r15,%rax), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movzbl	3(%r9,%r14), %eax
	negq	%rax
	movzbl	(%r15,%rax), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	leal	(%r8,%r10,2), %eax
	leal	(%rax,%r13,2), %edi
	movzbl	4(%r9,%r14), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r11d
	leal	(%rdi,%rdx,2), %edi
	leal	(%rdi,%rsi,2), %edi
	addl	%ebx, %edi
	addl	%ebp, %edi
	movzbl	5(%r9,%r14), %ebp
	negq	%rbp
	movzbl	(%r15,%rbp), %eax
	addl	%ecx, %edi
	addl	72(%rsp), %edi          # 4-byte Folded Reload
	movzbl	6(%r9,%r14), %ebp
	negq	%rbp
	movzbl	(%r15,%rbp), %ebp
	addl	%r11d, %edi
	addl	%eax, %edi
	addl	%ebp, %edi
                                        # kill: %EBP<def> %EBP<kill> %RBP<def>
	subl	%ebx, %ebp
	movq	%r12, %r14
	movzbl	-6(%r9,%r14), %ebx
	negq	%rbx
	movzbl	(%r15,%rbx), %ebx
	subl	%ebx, %ebp
	movzbl	(%r9,%r14), %ebx
	negq	%rbx
	movzbl	(%r15,%rbx), %ebx
	addl	%ebx, %ebp
	movzbl	-1(%r9,%r14), %ebx
	negq	%rbx
	movzbl	(%r15,%rbx), %ebx
	addl	%eax, %ebx
	movzbl	-5(%r9,%r14), %ecx
	negq	%rcx
	movzbl	(%r15,%rcx), %r12d
	addl	%r12d, %r12d
	movq	88(%rsp), %rax          # 8-byte Reload
	subl	24(%rsp), %eax          # 4-byte Folded Reload
	movzbl	-4(%r9,%r14), %ecx
	negq	%rcx
	movzbl	(%r15,%rcx), %r13d
	subl	%r8d, %eax
	movzbl	-2(%r9,%r14), %ecx
	negq	%rcx
	movzbl	(%r15,%rcx), %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	subl	%r10d, %eax
	movq	272(%rsp), %r8          # 8-byte Reload
	movzbl	-6(%r9,%r8), %ecx
	negq	%rcx
	movzbl	(%r15,%rcx), %ecx
	addl	%edx, %eax
	movzbl	-5(%r9,%r8), %edx
	negq	%rdx
	movzbl	(%r15,%rdx), %edx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leal	(%rax,%rsi,2), %r14d
	leal	(%rdx,%rdx), %eax
	subl	80(%rsp), %r14d         # 4-byte Folded Reload
	movzbl	-4(%r9,%r8), %edx
	negq	%rdx
	movzbl	(%r15,%rdx), %edx
	movl	%edx, 24(%rsp)          # 4-byte Spill
	subl	64(%rsp), %r14d         # 4-byte Folded Reload
	movzbl	-2(%r9,%r8), %edx
	negq	%rdx
	movzbl	(%r15,%rdx), %r10d
	movl	%r10d, 64(%rsp)         # 4-byte Spill
	addl	%r11d, %r14d
	movzbl	-1(%r9,%r8), %edx
	negq	%rdx
	movzbl	(%r15,%rdx), %esi
	movl	%esi, 72(%rsp)          # 4-byte Spill
	subl	%r12d, %r14d
	movzbl	(%r9,%r8), %edx
	negq	%rdx
	movzbl	(%r15,%rdx), %r11d
	subl	%r13d, %r14d
	movq	264(%rsp), %r13         # 8-byte Reload
	movzbl	-4(%r9,%r13), %edx
	negq	%rdx
	movzbl	(%r15,%rdx), %edx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	addl	56(%rsp), %r14d         # 4-byte Folded Reload
	leal	(%rdx,%rdx), %edx
	movl	%edx, 56(%rsp)          # 4-byte Spill
	subl	%eax, %r14d
	movzbl	-3(%r9,%r13), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r12d
	addl	%esi, %ebx
	subl	24(%rsp), %r14d         # 4-byte Folded Reload
	addl	%r10d, %r14d
	leal	(%r14,%rbx,2), %ebx
	movzbl	-1(%r9,%r13), %edx
	negq	%rdx
	movzbl	(%r15,%rdx), %r14d
	subl	56(%rsp), %ebx          # 4-byte Folded Reload
	movzbl	(%r9,%r13), %esi
	negq	%rsi
	movzbl	(%r15,%rsi), %r10d
	subl	%ecx, %ebp
	addl	%r11d, %ebp
	leal	(%rbp,%rbp,2), %ebp
	subl	%r12d, %ebx
	addl	%ebp, %ebx
	movq	568(%rsp), %rdx         # 8-byte Reload
	movq	256(%rsp), %rax         # 8-byte Reload
	movzbl	-2(%rdx,%rax), %ebp
	negq	%rbp
	movzbl	(%r15,%rbp), %ebp
	addl	%r14d, %ebx
	leal	(%rbx,%r10,2), %ebx
	subl	%ebp, %ebx
	subl	120(%rsp), %ebp         # 4-byte Folded Reload
	movzbl	-1(%rdx,%rax), %esi
	negq	%rsi
	movzbl	(%r15,%rsi), %esi
	addl	%esi, %ebp
	movzbl	(%rdx,%rax), %esi
	negq	%rsi
	movzbl	(%r15,%rsi), %esi
	addl	%esi, %ebx
	addl	%esi, %ebp
	movzbl	-2(%r9,%r13), %esi
	negq	%rsi
	movzbl	(%r15,%rsi), %esi
	addl	%r12d, %esi
	addl	%r14d, %esi
	subl	%edi, %ecx
	addl	88(%rsp), %ecx          # 4-byte Folded Reload
	addl	24(%rsp), %ecx          # 4-byte Folded Reload
	movzbl	-3(%r9,%r8), %eax
	negq	%rax
	movzbl	(%r15,%rax), %eax
	addl	%eax, %ecx
	addl	64(%rsp), %ecx          # 4-byte Folded Reload
	addl	72(%rsp), %ecx          # 4-byte Folded Reload
	addl	%r11d, %ecx
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(%rcx,%rax,2), %eax
	leal	(%rax,%r10,2), %eax
	leal	(%rbp,%rbp,2), %ecx
	leal	(%rax,%rsi,2), %ebp
	addl	%ecx, %ebp
	movl	%ebx, %eax
	imull	%eax, %eax
	movl	%ebp, %ecx
	imull	%ecx, %ecx
	addl	%eax, %ecx
	cvtsi2ssl	%ecx, %xmm1
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB13_18
# BB#17:                                # %call.sqrt
                                        #   in Loop: Header=BB13_11 Depth=2
	movaps	%xmm1, %xmm0
	callq	sqrtf
	movss	.LCPI13_1(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movsd	.LCPI13_0(%rip), %xmm4  # xmm4 = mem[0],zero
.LBB13_18:                              # %.split
                                        #   in Loop: Header=BB13_11 Depth=2
	cvtss2sd	%xmm0, %xmm0
	movl	96(%rsp), %eax          # 4-byte Reload
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB13_19
# BB#20:                                #   in Loop: Header=BB13_11 Depth=2
	testl	%ebx, %ebx
	movss	.LCPI13_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	248(%rsp), %rdi         # 8-byte Reload
	movq	232(%rsp), %r9          # 8-byte Reload
	movq	200(%rsp), %r10         # 8-byte Reload
	movq	192(%rsp), %r11         # 8-byte Reload
	je	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_11 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebx, %xmm1
	divss	%xmm1, %xmm0
.LBB13_22:                              #   in Loop: Header=BB13_11 Depth=2
	movaps	%xmm0, %xmm2
	xorps	.LCPI13_4(%rip), %xmm2
	movaps	%xmm0, %xmm1
	cmpltss	.LCPI13_5, %xmm1
	movaps	%xmm1, %xmm3
	andnps	%xmm0, %xmm3
	andps	%xmm2, %xmm1
	orps	%xmm3, %xmm1
	ucomiss	%xmm1, %xmm5
	movq	240(%rsp), %r14         # 8-byte Reload
	movq	224(%rsp), %r12         # 8-byte Reload
	movq	216(%rsp), %r13         # 8-byte Reload
	movq	208(%rsp), %r8          # 8-byte Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	jbe	.LBB13_24
# BB#23:                                #   in Loop: Header=BB13_11 Depth=2
	movl	$1, %eax
	xorl	%ecx, %ecx
	jmp	.LBB13_27
	.p2align	4, 0x90
.LBB13_13:                              # %._crit_edge880
                                        #   in Loop: Header=BB13_11 Depth=2
	movq	344(%rsp), %rbx         # 8-byte Reload
	leaq	-4(%rbx,%rsi), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	560(%rsp), %rbp         # 8-byte Reload
	movq	%r10, %rax
	leaq	-4(%rbp,%rsi), %rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	leaq	-3(%rbp,%rsi), %rcx
	movq	%rcx, 280(%rsp)         # 8-byte Spill
	leaq	-2(%rbp,%rsi), %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movq	%r11, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	-1(%rbp,%rdx), %r11
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	(%rbp,%rdx), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r14), %r10
	leaq	1(%rbp,%r14), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	leaq	2(%rbp,%r14), %rdx
	leaq	3(%rbp,%r14), %rcx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	leaq	4(%rbp,%r14), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	5(%rbp,%r14), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	leaq	6(%rbp,%r14), %rcx
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r9), %rdi
	movq	%rdi, 592(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r12), %rdi
	movq	%rdi, 576(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r13), %rdi
	movq	%r10, %r13
	movq	328(%rsp), %r10         # 8-byte Reload
	movq	%rdi, 584(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r8), %rdi
	movq	%rdi, 600(%rsp)         # 8-byte Spill
	movq	%r11, %r9
	movq	336(%rsp), %rdi         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 608(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rsi), %rax
	movq	%rax, 616(%rsp)         # 8-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	312(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	704(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	696(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movq	688(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	272(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	680(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	%rax, 624(%rsp)         # 8-byte Spill
	movq	672(%rsp), %rax         # 8-byte Reload
	leaq	(%rbp,%rax), %rax
	movq	664(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbp,%rsi), %r8
	movq	656(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbp,%rsi), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	264(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbp,%rsi), %rcx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movq	568(%rsp), %rbp         # 8-byte Reload
	movq	648(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbp,%rsi), %r14
	movq	640(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbp,%rsi), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	256(%rsp), %rsi         # 8-byte Reload
	leaq	(%rbp,%rsi), %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	280(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB13_14
.LBB13_19:                              #   in Loop: Header=BB13_11 Depth=2
	movq	760(%rsp), %rax         # 8-byte Reload
	movq	632(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	752(%rsp), %rbp         # 8-byte Reload
	leaq	1(%rax,%rbp), %r10
	movq	744(%rsp), %rbx         # 8-byte Reload
	leaq	4(%rbx,%r10), %r13
	movq	736(%rsp), %rsi         # 8-byte Reload
	leaq	6(%rsi,%r13), %rdi
	leaq	6(%rsi,%rdi), %rdx
	leaq	6(%rbx,%rdx), %r12
	movq	%r12, 624(%rsp)         # 8-byte Spill
	leaq	2(%rax,%rbp), %r8
	leaq	3(%rax,%rbp), %r14
	leaq	4(%rax,%rbp), %r9
	leaq	5(%rax,%rbp), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movq	%r8, %rcx
	leaq	1(%rax), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	5(%rbx,%r10), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	6(%rbx,%r10), %r11
	leaq	7(%rbx,%r10), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	leaq	8(%rbx,%r10), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	9(%rbx,%r10), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	leaq	10(%rbx,%r10), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	7(%rsi,%r13), %rax
	movq	%rax, 576(%rsp)         # 8-byte Spill
	leaq	8(%rsi,%r13), %rax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	leaq	10(%rsi,%r13), %rax
	movq	%rax, 600(%rsp)         # 8-byte Spill
	leaq	11(%rsi,%r13), %rax
	movq	%rax, 608(%rsp)         # 8-byte Spill
	leaq	12(%rsi,%r13), %rax
	movq	%rax, 616(%rsp)         # 8-byte Spill
	leaq	7(%rsi,%rdi), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	leaq	8(%rsi,%rdi), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	leaq	9(%rsi,%rdi), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	10(%rsi,%rdi), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	11(%rsi,%rdi), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rdi, 592(%rsp)         # 8-byte Spill
	leaq	12(%rsi,%rdi), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	7(%rbx,%rdx), %rax
	leaq	8(%rbx,%rdx), %r8
	leaq	9(%rbx,%rdx), %rsi
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	leaq	10(%rbx,%rdx), %rdx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%r11, %rdx
	movq	%r14, %rdi
	leaq	4(%rbp,%r12), %r14
	leaq	5(%rbp,%r12), %rsi
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	leaq	6(%rbp,%r12), %rsi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	344(%rsp), %rbx         # 8-byte Reload
.LBB13_14:                              #   in Loop: Header=BB13_11 Depth=2
	movq	16(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rcx), %ecx
	negq	%rcx
	movzbl	(%r15,%rcx), %ebp
	movzbl	(%rdi), %ecx
	negq	%rcx
	movzbl	(%r15,%rcx), %r12d
	movzbl	(%r9), %ecx
	negq	%rcx
	movzbl	(%r15,%rcx), %ecx
	movzbl	(%rax), %eax
	negq	%rax
	movzbl	(%r15,%rax), %edi
	movzbl	(%r8), %eax
	negq	%rax
	movzbl	(%r15,%rax), %eax
	movl	%ebp, 336(%rsp)         # 4-byte Spill
	addl	%ebp, %r12d
	movl	%ecx, 328(%rsp)         # 4-byte Spill
	addl	%ecx, %r12d
	movl	%edi, 356(%rsp)         # 4-byte Spill
	addl	%edi, %r12d
	addl	%eax, %r12d
	movzbl	-6(%rbx,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %edi
	movzbl	-5(%rbx,%rsi), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r11d
	movq	160(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax), %eax
	negq	%rax
	movzbl	(%r15,%rax), %esi
	movzbl	(%r14), %eax
	negq	%rax
	movzbl	(%r15,%rax), %ebp
	movq	152(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax), %eax
	negq	%rax
	movzbl	(%r15,%rax), %eax
	movq	%rdi, %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	addl	%edi, %r11d
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	addl	%esi, %r11d
	movl	%ebp, 280(%rsp)         # 4-byte Spill
	addl	%ebp, %r11d
	addl	%eax, %r11d
	movzbl	(%r10), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r8d
	shll	$2, %r8d
	movq	144(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax), %eax
	negq	%rax
	movzbl	(%r15,%rax), %ecx
	movzbl	(%r13), %eax
	negq	%rax
	movzbl	(%r15,%rax), %esi
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %eax
	negq	%rax
	movzbl	(%r15,%rax), %ebp
	movzbl	(%rdx), %eax
	negq	%rax
	movzbl	(%r15,%rax), %r10d
	movq	288(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax), %eax
	negq	%rax
	movzbl	(%r15,%rax), %edi
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	leal	(%r8,%rcx,4), %eax
	movq	%rsi, %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	addl	%esi, %eax
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	addl	%ebp, %eax
	addl	%r10d, %eax
	addl	%edi, %eax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rsi), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %r14d
	movq	88(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rsi), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %r9d
	movq	176(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %r13d
	movq	120(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %ebp
	movq	296(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %esi
	movq	304(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %ecx
	movq	168(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %edi
	addl	%r14d, %eax
	movq	%r9, 168(%rsp)          # 8-byte Spill
	addl	%r9d, %eax
	movq	%r13, 120(%rsp)         # 8-byte Spill
	addl	%r13d, %eax
	movl	%ebp, 88(%rsp)          # 4-byte Spill
	addl	%ebp, %eax
	movl	%esi, 176(%rsp)         # 4-byte Spill
	addl	%esi, %eax
	movl	%ecx, 80(%rsp)          # 4-byte Spill
	addl	%ecx, %eax
	addl	%edi, %eax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %ebp
	movq	72(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %ecx
	movq	624(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %r13d
	movq	136(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %r9d
	movq	184(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %edx
	movl	%r9d, 56(%rsp)          # 4-byte Spill
	addl	%r9d, %r12d
	movl	%esi, 72(%rsp)          # 4-byte Spill
	addl	%esi, %eax
	movl	%ebp, 184(%rsp)         # 4-byte Spill
	addl	%ebp, %eax
	addl	%ecx, %eax
	leal	(%rax,%r13,4), %eax
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leal	(%rax,%rdx,4), %eax
	leal	(%rax,%r12,4), %r12d
	movq	96(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %eax
	addl	%eax, %r11d
	leal	(%r11,%r11,8), %edi
	addl	%edi, %r12d
	je	.LBB13_15
# BB#32:                                #   in Loop: Header=BB13_11 Depth=2
	movq	592(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movl	%r10d, %r9d
	movzbl	(%r15,%rdi), %r10d
	movq	576(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %edi
	negq	%rdi
	movzbl	(%r15,%rdi), %r11d
	movq	584(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %ebp
	negq	%rbp
	movzbl	(%r15,%rbp), %edx
	movl	%edx, 96(%rsp)          # 4-byte Spill
	movq	600(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %ebp
	negq	%rbp
	movzbl	(%r15,%rbp), %edx
	movl	%edx, 136(%rsp)         # 4-byte Spill
	movq	608(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %ebx
	negq	%rbx
	movzbl	(%r15,%rbx), %ebx
	movq	616(%rsp), %rdx         # 8-byte Reload
	movzbl	(%rdx), %ebp
	negq	%rbp
	movzbl	(%r15,%rbp), %esi
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	leal	(%rdx,%rdi), %ebp
	addl	176(%rsp), %ebp         # 4-byte Folded Reload
	addl	184(%rsp), %ebp         # 4-byte Folded Reload
	addl	%r11d, %ebp
	movq	288(%rsp), %r11         # 8-byte Reload
	shll	$2, %r11d
	shll	$2, %r13d
	addl	%ebx, %ebp
	movl	%eax, %ebx
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	120(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rax), %edi
	addl	88(%rsp), %edi          # 4-byte Folded Reload
	movl	%ecx, 304(%rsp)         # 4-byte Spill
	addl	%ecx, %edi
	movl	%ebx, %ecx
	addl	%r10d, %edi
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	shll	$2, %edx
	addl	%esi, %edi
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	152(%rsp), %rsi         # 8-byte Reload
	leal	(%rsi,%rax), %ebx
	addl	%r8d, %ebx
	movl	336(%rsp), %r10d        # 4-byte Reload
	addl	%r10d, %ebx
	movl	328(%rsp), %r15d        # 4-byte Reload
	addl	%r15d, %ebx
	addl	%r11d, %ebx
	movl	%r9d, 296(%rsp)         # 4-byte Spill
	addl	%r9d, %ebx
	movl	%r14d, %r9d
	addl	%r14d, %ebx
	addl	80(%rsp), %ebx          # 4-byte Folded Reload
	addl	72(%rsp), %ebx          # 4-byte Folded Reload
	addl	%r13d, %ebx
	movl	356(%rsp), %eax         # 4-byte Reload
	addl	%eax, %ebx
	addl	56(%rsp), %ebx          # 4-byte Folded Reload
	addl	%edx, %ebx
	addl	280(%rsp), %ebx         # 4-byte Folded Reload
	addl	%ecx, %ebx
	addl	96(%rsp), %ebx          # 4-byte Folded Reload
	addl	136(%rsp), %ebx         # 4-byte Folded Reload
	leal	(%rbx,%rbp,4), %edx
	leal	(%rdi,%rdi,8), %esi
	addl	%esi, %edx
	cvtsi2ssl	%edx, %xmm0
	cvtsi2ssl	%r12d, %xmm1
	divss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm5
	movl	%r10d, %esi
	movl	%r15d, %edx
	movq	24(%rsp), %r10          # 8-byte Reload
	jbe	.LBB13_34
# BB#33:                                #   in Loop: Header=BB13_11 Depth=2
	movl	$1, %r15d
	xorl	%ecx, %ecx
	jmp	.LBB13_36
.LBB13_34:                              #   in Loop: Header=BB13_11 Depth=2
	movl	%ecx, %r14d
	movl	304(%rsp), %ecx         # 4-byte Reload
	movq	%r13, 96(%rsp)          # 8-byte Spill
	movq	120(%rsp), %r13         # 8-byte Reload
	movl	88(%rsp), %r12d         # 4-byte Reload
	movq	152(%rsp), %r15         # 8-byte Reload
	movq	144(%rsp), %rdi         # 8-byte Reload
	movl	%r9d, 136(%rsp)         # 4-byte Spill
	movq	160(%rsp), %rbp         # 8-byte Reload
	movl	280(%rsp), %ebx         # 4-byte Reload
	ucomiss	.LCPI13_2(%rip), %xmm0
	jbe	.LBB13_35
.LBB13_15:                              #   in Loop: Header=BB13_11 Depth=2
	xorl	%r15d, %r15d
	movl	$1, %ecx
	jmp	.LBB13_36
.LBB13_24:                              #   in Loop: Header=BB13_11 Depth=2
	ucomiss	.LCPI13_2(%rip), %xmm1
	jbe	.LBB13_26
# BB#25:                                #   in Loop: Header=BB13_11 Depth=2
	xorl	%eax, %eax
	movl	$1, %ecx
	jmp	.LBB13_27
.LBB13_35:                              #   in Loop: Header=BB13_11 Depth=2
	subl	%r15d, %ebp
	addl	%ebp, %edi
	subl	%r13d, %edi
	subl	%r12d, %edi
	addl	%ecx, %edi
	subl	%ebx, %edi
	addl	%r14d, %edi
	addl	%esi, %r10d
	addl	184(%rsp), %r10d        # 4-byte Folded Reload
	addl	56(%rsp), %r10d         # 4-byte Folded Reload
	addl	%edx, %edx
	subl	%edx, %r8d
	subl	%r11d, %r8d
	addl	296(%rsp), %r8d         # 4-byte Folded Reload
	subl	136(%rsp), %r8d         # 4-byte Folded Reload
	movq	168(%rsp), %rbx         # 8-byte Reload
	addl	%ebx, %ebx
	subl	%ebx, %r8d
	movl	176(%rsp), %r15d        # 4-byte Reload
	addl	%r15d, %r15d
	subl	%r15d, %r8d
	subl	80(%rsp), %r8d          # 4-byte Folded Reload
	addl	72(%rsp), %r8d          # 4-byte Folded Reload
	subl	96(%rsp), %r8d          # 4-byte Folded Reload
	addl	%eax, %eax
	subl	%eax, %r8d
	movq	64(%rsp), %rax          # 8-byte Reload
	leal	(%r8,%rax,4), %eax
	leal	(%rax,%r10,2), %eax
	leal	(%rdi,%rdi,2), %ecx
	addl	%ecx, %eax
	testl	%eax, %eax
	movl	$1, %r15d
	movl	$1, %ecx
	movl	$-1, %edx
	cmovgl	%edx, %ecx
	.p2align	4, 0x90
.LBB13_36:                              # %.thread761
                                        #   in Loop: Header=BB13_11 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	248(%rsp), %rdi         # 8-byte Reload
	movq	240(%rsp), %r14         # 8-byte Reload
	movq	232(%rsp), %r9          # 8-byte Reload
	movq	224(%rsp), %r12         # 8-byte Reload
	movq	216(%rsp), %r13         # 8-byte Reload
	movq	208(%rsp), %r8          # 8-byte Reload
	movq	200(%rsp), %r10         # 8-byte Reload
	movq	192(%rsp), %r11         # 8-byte Reload
	movq	112(%rsp), %rbp         # 8-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	leal	(%rax,%rcx), %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	addl	%r15d, %edx
	leal	4(%rdi,%rdx), %edx
	movslq	%edx, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	(%rsi,%rdx,4), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	jle	.LBB13_41
# BB#37:                                #   in Loop: Header=BB13_11 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%ecx, %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	subl	%r15d, %edx
	leal	4(%rdi,%rdx), %edx
	movslq	%edx, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	(%rsi,%rdx,4), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	jl	.LBB13_41
# BB#38:                                #   in Loop: Header=BB13_11 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%rcx,2), %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	leal	(%rdx,%r15,2), %edx
	leal	4(%rdi,%rdx), %edx
	movslq	%edx, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	(%rsi,%rdx,4), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	jle	.LBB13_41
# BB#39:                                #   in Loop: Header=BB13_11 Depth=2
	addl	%ecx, %ecx
	addl	%r15d, %r15d
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%ecx, %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	subl	%r15d, %edx
	leal	4(%rdi,%rdx), %eax
	cltq
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	48(%rsp), %edx          # 4-byte Reload
	cmpl	(%rcx,%rax,4), %edx
	jl	.LBB13_41
# BB#40:                                #   in Loop: Header=BB13_11 Depth=2
	movq	360(%rsp), %rax         # 8-byte Reload
	movb	$2, -5(%rax,%rsi)
	jmp	.LBB13_41
.LBB13_26:                              #   in Loop: Header=BB13_11 Depth=2
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	movl	$1, %eax
	movl	$1, %ecx
	movl	$-1, %edx
	cmoval	%edx, %ecx
.LBB13_27:                              #   in Loop: Header=BB13_11 Depth=2
	leal	(%rsi,%rcx), %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	addl	%eax, %edx
	leal	4(%rdi,%rdx), %edx
	movslq	%edx, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %r15d         # 4-byte Reload
	cmpl	(%rsi,%rdx,4), %r15d
	movq	%rax, %r15
	movq	16(%rsp), %rsi          # 8-byte Reload
	jle	.LBB13_41
# BB#28:                                #   in Loop: Header=BB13_11 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%ecx, %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	subl	%r15d, %edx
	leal	4(%rdi,%rdx), %edx
	movslq	%edx, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	(%rsi,%rdx,4), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	jl	.LBB13_41
# BB#29:                                #   in Loop: Header=BB13_11 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	leal	(%rax,%rcx,2), %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	leal	(%rdx,%r15,2), %edx
	leal	4(%rdi,%rdx), %edx
	movslq	%edx, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	(%rsi,%rdx,4), %eax
	movl	48(%rsp), %edx          # 4-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	jle	.LBB13_41
# BB#30:                                #   in Loop: Header=BB13_11 Depth=2
	addl	%ecx, %ecx
	addl	%r15d, %r15d
	movq	%r15, %rax
	movl	%edx, %r15d
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%ecx, %edx
	imull	40(%rsp), %edx          # 4-byte Folded Reload
	subl	%eax, %edx
	leal	4(%rdi,%rdx), %eax
	cltq
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx,%rax,4), %r15d
	jl	.LBB13_41
# BB#31:                                # %.critedge
                                        #   in Loop: Header=BB13_11 Depth=2
	movq	360(%rsp), %rax         # 8-byte Reload
	movb	$1, -5(%rax,%rsi)
	.p2align	4, 0x90
.LBB13_41:                              # %.critedge755
                                        #   in Loop: Header=BB13_11 Depth=2
	incq	632(%rsp)               # 8-byte Folded Spill
	incq	%rsi
	incq	256(%rsp)               # 8-byte Folded Spill
	incq	640(%rsp)               # 8-byte Folded Spill
	incq	648(%rsp)               # 8-byte Folded Spill
	incq	264(%rsp)               # 8-byte Folded Spill
	incq	656(%rsp)               # 8-byte Folded Spill
	incq	664(%rsp)               # 8-byte Folded Spill
	incq	672(%rsp)               # 8-byte Folded Spill
	incq	680(%rsp)               # 8-byte Folded Spill
	incq	272(%rsp)               # 8-byte Folded Spill
	incq	688(%rsp)               # 8-byte Folded Spill
	incq	696(%rsp)               # 8-byte Folded Spill
	incq	704(%rsp)               # 8-byte Folded Spill
	incq	312(%rsp)               # 8-byte Folded Spill
	incq	%rbx
	incq	%rbp
	incq	%r11
	incq	%r10
	incq	%r8
	incq	%r13
	incq	%r12
	incq	%r9
	incq	%r14
	incq	%rdi
	cmpq	%rdi, 776(%rsp)         # 8-byte Folded Reload
	jne	.LBB13_11
.LBB13_42:                              # %._crit_edge
                                        #   in Loop: Header=BB13_9 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	incq	%rcx
	movq	552(%rsp), %rax         # 8-byte Reload
	addq	%rax, 368(%rsp)         # 8-byte Folded Spill
	addq	%rax, 544(%rsp)         # 8-byte Folded Spill
	addq	%rax, 536(%rsp)         # 8-byte Folded Spill
	addq	%rax, 528(%rsp)         # 8-byte Folded Spill
	addq	%rax, 520(%rsp)         # 8-byte Folded Spill
	addq	%rax, 512(%rsp)         # 8-byte Folded Spill
	addq	%rax, 504(%rsp)         # 8-byte Folded Spill
	addq	%rax, 496(%rsp)         # 8-byte Folded Spill
	addq	%rax, 488(%rsp)         # 8-byte Folded Spill
	addq	%rax, 480(%rsp)         # 8-byte Folded Spill
	addq	%rax, 472(%rsp)         # 8-byte Folded Spill
	addq	%rax, 464(%rsp)         # 8-byte Folded Spill
	addq	%rax, 456(%rsp)         # 8-byte Folded Spill
	addq	%rax, 448(%rsp)         # 8-byte Folded Spill
	addq	%rax, 440(%rsp)         # 8-byte Folded Spill
	addq	%rax, 432(%rsp)         # 8-byte Folded Spill
	addq	%rax, 424(%rsp)         # 8-byte Folded Spill
	addq	%rax, 416(%rsp)         # 8-byte Folded Spill
	addq	%rax, 408(%rsp)         # 8-byte Folded Spill
	addq	%rax, 400(%rsp)         # 8-byte Folded Spill
	addq	%rax, 392(%rsp)         # 8-byte Folded Spill
	addq	%rax, 384(%rsp)         # 8-byte Folded Spill
	addq	%rax, 376(%rsp)         # 8-byte Folded Spill
	cmpq	720(%rsp), %rcx         # 8-byte Folded Reload
	jne	.LBB13_9
.LBB13_43:                              # %._crit_edge853
	addq	$792, %rsp              # imm = 0x318
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	susan_edges, .Lfunc_end13-susan_edges
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4600877379321698714     # double 0.40000000000000002
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_1:
	.long	1056964608              # float 0.5
.LCPI14_2:
	.long	1073741824              # float 2
.LCPI14_3:
	.long	1232348160              # float 1.0E+6
.LCPI14_5:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	susan_edges_small
	.p2align	4, 0x90
	.type	susan_edges_small,@function
susan_edges_small:                      # @susan_edges_small
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi127:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi128:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi129:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi131:
	.cfi_def_cfa_offset 224
.Lcfi132:
	.cfi_offset %rbx, -56
.Lcfi133:
	.cfi_offset %r12, -48
.Lcfi134:
	.cfi_offset %r13, -40
.Lcfi135:
	.cfi_offset %r14, -32
.Lcfi136:
	.cfi_offset %r15, -24
.Lcfi137:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	224(%rsp), %ebx
	movl	%ebx, %eax
	movq	%r9, 8(%rsp)            # 8-byte Spill
	imull	%r9d, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	callq	memset
	leal	-1(%rbx), %eax
	cmpl	$2, %eax
	jl	.LBB14_8
# BB#1:                                 # %.preheader286.lr.ph
	movq	8(%rsp), %rcx           # 8-byte Reload
	leal	-1(%rcx), %ecx
	cmpl	$2, %ecx
	jl	.LBB14_8
# BB#2:                                 # %.preheader286.us.preheader
	movq	8(%rsp), %rsi           # 8-byte Reload
	leal	-2(%rsi), %edx
	movslq	%edx, %rdx
	movslq	%esi, %rsi
	movl	%ecx, %r10d
	movl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rsi), %r13
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	leaq	4(%rax,%rsi,4), %r12
	leaq	2(%rcx), %rbp
	leaq	6(%rcx,%rdx,2), %rbx
	leaq	2(%rcx,%rdx), %r9
	decq	%r10
	movl	$1, %r11d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB14_3:                               # %.preheader286.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_4 Depth 2
	movq	%r10, %rax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB14_4:                               #   Parent Loop BB14_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r13,%rcx), %r8d
	addq	120(%rsp), %r8          # 8-byte Folded Reload
	movzbl	-2(%rbp,%rcx), %esi
	movq	%r8, %rdi
	subq	%rsi, %rdi
	movzbl	(%rdi), %esi
	movzbl	-1(%rbp,%rcx), %edi
	movq	%r8, %r14
	subq	%rdi, %r14
	movzbl	(%r14), %edi
	addl	%esi, %edi
	movzbl	(%rbp,%rcx), %esi
	movq	%r8, %rdx
	subq	%rsi, %rdx
	movzbl	(%rdx), %edx
	addl	%edi, %edx
	movzbl	(%r9,%rcx), %esi
	movq	%r8, %rdi
	subq	%rsi, %rdi
	movzbl	(%rdi), %esi
	addl	%edx, %esi
	movzbl	2(%r9,%rcx), %edx
	movq	%r8, %rdi
	subq	%rdx, %rdi
	movzbl	(%rdi), %edx
	addl	%esi, %edx
	movzbl	-2(%rbx,%rcx), %esi
	movq	%r8, %rdi
	subq	%rsi, %rdi
	movzbl	(%rdi), %esi
	addl	%edx, %esi
	movzbl	-1(%rbx,%rcx), %edx
	movq	%r8, %rdi
	subq	%rdx, %rdi
	movzbl	(%rdi), %edx
	addl	%esi, %edx
	movzbl	(%rbx,%rcx), %esi
	subq	%rsi, %r8
	movzbl	(%r8), %esi
	leal	100(%rsi,%rdx), %edi
	cmpl	$730, %edi              # imm = 0x2DA
	jg	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=2
	movl	$730, %edx              # imm = 0x2DA
	subl	%edi, %edx
	movl	%edx, (%r12,%rcx,4)
.LBB14_6:                               #   in Loop: Header=BB14_4 Depth=2
	incq	%rcx
	decq	%rax
	jne	.LBB14_4
# BB#7:                                 # %._crit_edge294.us
                                        #   in Loop: Header=BB14_3 Depth=1
	incq	%r11
	addq	32(%rsp), %r15          # 8-byte Folded Reload
	cmpq	24(%rsp), %r11          # 8-byte Folded Reload
	jne	.LBB14_3
.LBB14_8:                               # %.preheader285
	movl	224(%rsp), %ecx
	addl	$-2, %ecx
	cmpl	$3, %ecx
	movq	8(%rsp), %r12           # 8-byte Reload
	jl	.LBB14_40
# BB#9:                                 # %.preheader.lr.ph
	movslq	%r12d, %r11
	leaq	-2(%r11), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%eax, %edx
	movl	%ecx, %eax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	-4(%r11,%r11,2), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leaq	-2(%r11,%r11), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	(%r11,%r11), %rax
	leaq	2(%r11,%r11), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rax,4), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	(,%r11,4), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	addq	$-2, %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movl	$2, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movsd	.LCPI14_0(%rip), %xmm4  # xmm4 = mem[0],zero
	movss	.LCPI14_1(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%r11, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB14_10:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_12 Depth 2
	cmpl	$3, 96(%rsp)            # 4-byte Folded Reload
	jl	.LBB14_39
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB14_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	decq	%rax
	imulq	%r11, %rax
	addq	56(%rsp), %rax          # 8-byte Folded Reload
	movq	%rax, 144(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	movl	$2, %r9d
	.p2align	4, 0x90
.LBB14_12:                              #   Parent Loop BB14_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %r10d
	testl	%r10d, %r10d
	jle	.LBB14_38
# BB#13:                                #   in Loop: Header=BB14_12 Depth=2
	movl	$730, %r14d             # imm = 0x2DA
	subl	%r10d, %r14d
	movzbl	2(%r13,%r11,2), %ebp
	addq	120(%rsp), %rbp         # 8-byte Folded Reload
	cmpl	$250, %r14d
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%r9, 24(%rsp)           # 8-byte Spill
	movl	%r10d, 4(%rsp)          # 4-byte Spill
	jle	.LBB14_14
# BB#17:                                #   in Loop: Header=BB14_12 Depth=2
	movzbl	1(%r11,%r13), %eax
	negq	%rax
	movzbl	(%rbp,%rax), %ecx
	movzbl	2(%r11,%r13), %eax
	negq	%rax
	movzbl	(%rbp,%rax), %eax
	addl	%ecx, %eax
	movzbl	3(%r11,%r13), %edx
	negq	%rdx
	movzbl	(%rbp,%rdx), %edx
	addl	%edx, %eax
	subl	%ecx, %edx
	movq	104(%rsp), %rsi         # 8-byte Reload
	movzbl	3(%rsi,%r13), %ecx
	negq	%rcx
	movzbl	(%rbp,%rcx), %ecx
	subl	%ecx, %edx
	movzbl	5(%rsi,%r13), %ecx
	negq	%rcx
	movzbl	(%rbp,%rcx), %ebx
	addl	%edx, %ebx
	movq	112(%rsp), %rdx         # 8-byte Reload
	movzbl	5(%rdx,%r13), %ecx
	negq	%rcx
	movzbl	(%rbp,%rcx), %ecx
	subl	%ecx, %ebx
	subl	%eax, %ecx
	movzbl	6(%rdx,%r13), %eax
	negq	%rax
	movzbl	(%rbp,%rax), %r12d
	addl	%ecx, %r12d
	movzbl	7(%rdx,%r13), %eax
	negq	%rax
	movzbl	(%rbp,%rax), %eax
	addl	%eax, %ebx
	addl	%eax, %r12d
	movl	%ebx, %eax
	imull	%eax, %eax
	movl	%r12d, %ecx
	imull	%ecx, %ecx
	addl	%eax, %ecx
	cvtsi2ssl	%ecx, %xmm1
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB14_19
# BB#18:                                # %call.sqrt
                                        #   in Loop: Header=BB14_12 Depth=2
	movaps	%xmm1, %xmm0
	movq	%r8, 152(%rsp)          # 8-byte Spill
	callq	sqrtf
	movl	4(%rsp), %r10d          # 4-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	152(%rsp), %r8          # 8-byte Reload
	movss	.LCPI14_1(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movsd	.LCPI14_0(%rip), %xmm4  # xmm4 = mem[0],zero
	movq	48(%rsp), %r11          # 8-byte Reload
.LBB14_19:                              # %.split
                                        #   in Loop: Header=BB14_12 Depth=2
	cvtss2sd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%r14d, %xmm1
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm4, %xmm1
	ucomisd	%xmm1, %xmm0
	jbe	.LBB14_20
# BB#21:                                #   in Loop: Header=BB14_12 Depth=2
	testl	%ebx, %ebx
	movss	.LCPI14_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	je	.LBB14_23
# BB#22:                                #   in Loop: Header=BB14_12 Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebx, %xmm1
	divss	%xmm1, %xmm0
.LBB14_23:                              #   in Loop: Header=BB14_12 Depth=2
	movaps	%xmm0, %xmm2
	xorps	.LCPI14_4(%rip), %xmm2
	movaps	%xmm0, %xmm1
	cmpltss	.LCPI14_5, %xmm1
	movaps	%xmm1, %xmm3
	andnps	%xmm0, %xmm3
	andps	%xmm2, %xmm1
	orps	%xmm3, %xmm1
	ucomiss	%xmm1, %xmm5
	jbe	.LBB14_25
# BB#24:                                #   in Loop: Header=BB14_12 Depth=2
	movl	$1, %eax
	xorl	%ecx, %ecx
	jmp	.LBB14_28
	.p2align	4, 0x90
.LBB14_14:                              # %._crit_edge311
                                        #   in Loop: Header=BB14_12 Depth=2
	movq	%r8, %r14
	leaq	3(%r13,%r11), %rsi
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	3(%r13,%rax), %r10
	leaq	5(%r13,%rax), %r8
	movq	112(%rsp), %rcx         # 8-byte Reload
	leaq	5(%r13,%rcx), %rbx
	leaq	6(%r13,%rcx), %rax
	leaq	7(%r13,%rcx), %r9
	jmp	.LBB14_15
.LBB14_20:                              #   in Loop: Header=BB14_12 Depth=2
	movq	%r8, %r14
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r9), %rsi
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rsi,%rcx), %r10
	leaq	3(%rsi,%rcx), %r8
	incq	%rsi
	leaq	2(%rcx,%r10), %rbx
	leaq	3(%rcx,%r10), %rax
	leaq	4(%rcx,%r10), %r9
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB14_15:                              #   in Loop: Header=BB14_12 Depth=2
	movzbl	1(%r11,%r13), %edx
	negq	%rdx
	movzbl	(%rbp,%rdx), %edx
	movq	%r11, %rdi
	movzbl	2(%rdi,%r13), %edi
	negq	%rdi
	movzbl	(%rbp,%rdi), %ecx
	addl	%edx, %ecx
	movzbl	(%rsi), %esi
	negq	%rsi
	movzbl	(%rbp,%rsi), %r11d
	addl	%r11d, %ecx
	movzbl	(%rbx), %edi
	negq	%rdi
	movzbl	(%rbp,%rdi), %ebx
	addl	%ebx, %ecx
	movzbl	(%rax), %eax
	negq	%rax
	movzbl	(%rbp,%rax), %edi
	addl	%ecx, %edi
	movzbl	(%r9), %eax
	negq	%rax
	movzbl	(%rbp,%rax), %eax
	addl	%eax, %edi
	je	.LBB14_16
# BB#31:                                #   in Loop: Header=BB14_12 Depth=2
	leal	(%r11,%rdx), %ecx
	movzbl	(%r10), %esi
	negq	%rsi
	movzbl	(%rbp,%rsi), %r9d
	movzbl	(%r8), %esi
	negq	%rsi
	movzbl	(%rbp,%rsi), %esi
	addl	%ebx, %ecx
	addl	%eax, %ecx
	addl	%r9d, %ecx
	addl	%esi, %ecx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ecx, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edi, %xmm1
	divss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm5
	jbe	.LBB14_33
# BB#32:                                #   in Loop: Header=BB14_12 Depth=2
	movl	$1, %eax
	xorl	%ecx, %ecx
	jmp	.LBB14_35
.LBB14_33:                              #   in Loop: Header=BB14_12 Depth=2
	ucomiss	.LCPI14_2(%rip), %xmm0
	jbe	.LBB14_34
.LBB14_16:                              #   in Loop: Header=BB14_12 Depth=2
	xorl	%eax, %eax
	movl	$1, %ecx
	jmp	.LBB14_35
.LBB14_25:                              #   in Loop: Header=BB14_12 Depth=2
	ucomiss	.LCPI14_2(%rip), %xmm1
	jbe	.LBB14_27
# BB#26:                                #   in Loop: Header=BB14_12 Depth=2
	xorl	%eax, %eax
	movl	$1, %ecx
	jmp	.LBB14_28
.LBB14_34:                              #   in Loop: Header=BB14_12 Depth=2
	subl	%r11d, %edx
	subl	%ebx, %edx
	addl	%eax, %edx
	testl	%edx, %edx
	movl	$1, %eax
	movl	$1, %ecx
	movl	$-1, %edx
	cmovgl	%edx, %ecx
	.p2align	4, 0x90
.LBB14_35:                              # %.thread279
                                        #   in Loop: Header=BB14_12 Depth=2
	movq	48(%rsp), %r11          # 8-byte Reload
	movq	%r14, %r8
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	4(%rsp), %ebp           # 4-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rcx), %edx
	imull	%r12d, %edx
	addl	%eax, %edx
	leal	2(%r15,%rdx), %edx
	movslq	%edx, %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpl	(%rsi,%rdx,4), %ebp
	jle	.LBB14_38
# BB#36:                                #   in Loop: Header=BB14_12 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%ecx, %edx
	imull	%r12d, %edx
	subl	%eax, %edx
	leal	2(%r15,%rdx), %eax
	cltq
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx,%rax,4), %ebp
	jl	.LBB14_38
# BB#37:                                #   in Loop: Header=BB14_12 Depth=2
	movq	88(%rsp), %rax          # 8-byte Reload
	movb	$2, (%rax,%r8)
	jmp	.LBB14_38
.LBB14_27:                              #   in Loop: Header=BB14_12 Depth=2
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	movl	$1, %eax
	movl	$1, %ecx
	movl	$-1, %edx
	cmoval	%edx, %ecx
.LBB14_28:                              #   in Loop: Header=BB14_12 Depth=2
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	(%rdx,%rcx), %edx
	imull	%r12d, %edx
	addl	%eax, %edx
	leal	2(%r15,%rdx), %edx
	movslq	%edx, %rdx
	movq	40(%rsp), %rsi          # 8-byte Reload
	cmpl	(%rsi,%rdx,4), %r10d
	jle	.LBB14_38
# BB#29:                                #   in Loop: Header=BB14_12 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%ecx, %edx
	imull	%r12d, %edx
	subl	%eax, %edx
	leal	2(%r15,%rdx), %eax
	cltq
	movq	40(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx,%rax,4), %r10d
	jl	.LBB14_38
# BB#30:                                # %.critedge
                                        #   in Loop: Header=BB14_12 Depth=2
	movq	88(%rsp), %rax          # 8-byte Reload
	movb	$1, (%rax,%r8)
	.p2align	4, 0x90
.LBB14_38:                              # %.critedge275
                                        #   in Loop: Header=BB14_12 Depth=2
	incq	%r9
	incq	%r13
	addq	$4, %rdi
	incq	%r8
	incq	%r15
	cmpq	%r15, 160(%rsp)         # 8-byte Folded Reload
	jne	.LBB14_12
.LBB14_39:                              # %._crit_edge
                                        #   in Loop: Header=BB14_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	addq	%r11, 64(%rsp)          # 8-byte Folded Spill
	movq	128(%rsp), %rax         # 8-byte Reload
	addq	%rax, 72(%rsp)          # 8-byte Folded Spill
	addq	%r11, 80(%rsp)          # 8-byte Folded Spill
	movq	%rcx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	136(%rsp), %rcx         # 8-byte Folded Reload
	jne	.LBB14_10
.LBB14_40:                              # %._crit_edge291
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	susan_edges_small, .Lfunc_end14-susan_edges_small
	.cfi_endproc

	.globl	corner_draw
	.p2align	4, 0x90
	.type	corner_draw,@function
corner_draw:                            # @corner_draw
	.cfi_startproc
# BB#0:
	cmpl	$7, 8(%rsi)
	je	.LBB15_5
# BB#1:                                 # %.lr.ph
	movslq	%edx, %r9
	testl	%ecx, %ecx
	je	.LBB15_2
	.p2align	4, 0x90
.LBB15_4:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movslq	4(%rsi), %rax
	imulq	%r9, %rax
	addq	%rdi, %rax
	movslq	(%rsi), %rcx
	movb	$0, (%rcx,%rax)
	cmpl	$7, 32(%rsi)
	leaq	24(%rsi), %rsi
	jne	.LBB15_4
	jmp	.LBB15_5
.LBB15_2:                               # %.lr.ph.split.us.preheader
	leaq	-2(%r9), %r8
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movslq	4(%rsi), %rdx
	decq	%rdx
	imulq	%r9, %rdx
	addq	%rdi, %rdx
	movslq	(%rsi), %rcx
	leaq	(%rdx,%rcx), %rax
	movb	$-1, -1(%rcx,%rdx)
	movb	$-1, (%rcx,%rdx)
	movb	$-1, 1(%rcx,%rdx)
	leaq	1(%r8,%rax), %rcx
	movb	$-1, -1(%r9,%rax)
	movb	$0, (%r9,%rax)
	movb	$-1, 1(%r9,%rax)
	movb	$-1, (%r9,%rcx)
	movb	$-1, 1(%r9,%rcx)
	movb	$-1, 2(%r9,%rcx)
	cmpl	$7, 32(%rsi)
	leaq	24(%rsi), %rsi
	jne	.LBB15_3
.LBB15_5:                               # %._crit_edge
	retq
.Lfunc_end15:
	.size	corner_draw, .Lfunc_end15-corner_draw
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_0:
	.quad	-4620693217682128896    # double -0.5
.LCPI16_1:
	.quad	4602678819172646912     # double 0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI16_2:
	.long	1077936128              # float 3
	.text
	.globl	susan_corners
	.p2align	4, 0x90
	.type	susan_corners,@function
susan_corners:                          # @susan_corners
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi141:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi142:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi144:
	.cfi_def_cfa_offset 512
.Lcfi145:
	.cfi_offset %rbx, -56
.Lcfi146:
	.cfi_offset %r12, -48
.Lcfi147:
	.cfi_offset %r13, -40
.Lcfi148:
	.cfi_offset %r14, -32
.Lcfi149:
	.cfi_offset %r15, -24
.Lcfi150:
	.cfi_offset %rbp, -16
	movl	%r9d, %r15d
	movq	%r8, 272(%rsp)          # 8-byte Spill
	movl	%ecx, %r13d
	movq	%rdx, 448(%rsp)         # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movl	512(%rsp), %r14d
	movl	%r14d, %eax
	imull	%r15d, %eax
	movslq	%eax, %rbx
	shlq	$2, %rbx
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memset
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 144(%rsp)         # 8-byte Spill
	addl	$-5, %r14d
	cmpl	$6, %r14d
	jl	.LBB16_102
# BB#1:                                 # %.preheader847.lr.ph
	leal	-5(%r15), %edx
	movq	%r15, 264(%rsp)         # 8-byte Spill
	movslq	%r15d, %rsi
	movl	%r14d, 228(%rsp)        # 4-byte Spill
	movl	%r14d, %eax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rsi), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rsi,2), %rdi
	movq	%rdi, 440(%rsp)         # 8-byte Spill
	leaq	-3(%rsi,%rsi,2), %r9
	addq	%rcx, %r9
	leaq	11(%rsi), %rdi
	movq	%rdi, 320(%rsp)         # 8-byte Spill
	leaq	11(%rsi,%rsi), %rdi
	leaq	11(%rsi,%rsi,2), %rbx
	movq	%rbx, 304(%rsp)         # 8-byte Spill
	movq	%rdi, 312(%rsp)         # 8-byte Spill
	leaq	-1(%rdi,%rsi,2), %rdi
	movq	%rdi, 296(%rsp)         # 8-byte Spill
	leaq	-6(%rax,%rsi,2), %rax
	addq	%rcx, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rsi,4), %rax
	leaq	5(%rcx,%rax), %rcx
	movq	%rcx, 432(%rsp)         # 8-byte Spill
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	leaq	20(%rbp,%rax,4), %rcx
	movq	%rcx, 376(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	20(%rcx,%rax,4), %rcx
	movq	%rcx, 368(%rsp)         # 8-byte Spill
	movq	144(%rsp), %rcx         # 8-byte Reload
	leaq	20(%rcx,%rax,4), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	leaq	-5(%rdx), %rax
	movq	%rax, 424(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	movsd	.LCPI16_0(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	.LCPI16_1(%rip), %xmm2  # xmm2 = mem[0],zero
	movss	.LCPI16_2(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movl	$5, %ecx
	xorl	%eax, %eax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movl	%r13d, 244(%rsp)        # 4-byte Spill
	.p2align	4, 0x90
.LBB16_2:                               # %.preheader847
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_4 Depth 2
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	cmpl	$6, 168(%rsp)           # 4-byte Folded Reload
	jl	.LBB16_46
# BB#3:                                 # %.lr.ph857
                                        #   in Loop: Header=BB16_2 Depth=1
	xorl	%ebx, %ebx
	movq	296(%rsp), %r12         # 8-byte Reload
	movq	304(%rsp), %r14         # 8-byte Reload
	movq	312(%rsp), %r15         # 8-byte Reload
	movq	320(%rsp), %rbp         # 8-byte Reload
	movq	288(%rsp), %rdx         # 8-byte Reload
	movl	$5, %eax
	.p2align	4, 0x90
.LBB16_4:                               #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%r14, 192(%rsp)         # 8-byte Spill
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 200(%rsp)         # 8-byte Spill
	movq	432(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax,%rdx), %r12d
	addq	448(%rsp), %r12         # 8-byte Folded Reload
	movq	440(%rsp), %rcx         # 8-byte Reload
	movzbl	4(%rcx,%rdx), %eax
	movq	%r12, %rsi
	subq	%rax, %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movzbl	5(%rcx,%rdx), %eax
	movq	%r12, %rsi
	subq	%rax, %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movzbl	6(%rcx,%rdx), %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movzbl	6(%r9,%rdx), %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movzbl	7(%r9,%rdx), %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movzbl	8(%r9,%rdx), %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movzbl	9(%r9,%rdx), %eax
	movq	%r12, %rcx
	subq	%rax, %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movzbl	10(%r9,%rdx), %ebx
	movq	%r12, %rax
	subq	%rbx, %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	movzbl	-6(%r9,%rbp), %r8d
	movq	%r12, %rbx
	subq	%r8, %rbx
	movzbl	-5(%r9,%rbp), %r8d
	movq	%r12, %rsi
	subq	%r8, %rsi
	movzbl	-4(%r9,%rbp), %r10d
	movq	%r12, %rax
	subq	%r10, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movzbl	-3(%r9,%rbp), %r11d
	movq	%r12, %r10
	subq	%r11, %r10
	movzbl	-2(%r9,%rbp), %r11d
	movq	%r12, %rdx
	subq	%r11, %rdx
	movzbl	-1(%r9,%rbp), %r14d
	movq	%r12, %r11
	subq	%r14, %r11
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movzbl	(%r9,%rbp), %eax
	movq	%r12, %r14
	subq	%rax, %r14
	movzbl	-6(%r9,%r15), %eax
	movq	%r12, %r13
	subq	%rax, %r13
	movzbl	-5(%r9,%r15), %eax
	movq	%r12, %r8
	subq	%rax, %r8
	movzbl	-4(%r9,%r15), %edi
	movq	%r12, %rcx
	subq	%rdi, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	movzbl	(%rax), %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	movzbl	(%rdi), %ebp
	movl	%eax, 240(%rsp)         # 4-byte Spill
	addl	%eax, %ebp
	movq	112(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax), %eax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	movq	104(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax), %eax
	movq	96(%rsp), %rdi          # 8-byte Reload
	movzbl	(%rdi), %edi
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 408(%rsp)         # 8-byte Spill
	leal	(%rbp,%rax), %ebp
	movq	%rdi, 328(%rsp)         # 8-byte Spill
	addl	%edi, %ebp
	movq	88(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %eax
	movq	%rax, %rdi
	movq	%rdi, 416(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	movq	80(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %edi
	movq	%rdi, 336(%rsp)         # 8-byte Spill
	addl	%edi, %ebp
	movq	%r12, %rdi
	movq	184(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax), %r12d
	movq	%r12, 80(%rsp)          # 8-byte Spill
	addl	%r12d, %ebp
	movzbl	(%rbx), %ebx
	movl	%ebx, 88(%rsp)          # 4-byte Spill
	addl	%ebx, %ebp
	movzbl	(%rsi), %esi
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	addl	%esi, %ebp
	movq	56(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax), %esi
	movl	%esi, 104(%rsp)         # 4-byte Spill
	addl	%esi, %ebp
	movzbl	(%r10), %esi
	movq	200(%rsp), %rbx         # 8-byte Reload
	movl	%esi, 184(%rsp)         # 4-byte Spill
	addl	%esi, %ebp
	movzbl	(%rdx), %edx
	movl	%edx, 112(%rsp)         # 4-byte Spill
	addl	%edx, %ebp
	movzbl	(%r11), %edx
	movq	32(%rsp), %r12          # 8-byte Reload
	movl	%edx, 16(%rsp)          # 4-byte Spill
	addl	%edx, %ebp
	movzbl	(%r14), %edx
	movq	192(%rsp), %r14         # 8-byte Reload
	movl	%edx, %r11d
	addl	%edx, %ebp
	movzbl	(%r13), %esi
	movl	244(%rsp), %r13d        # 4-byte Reload
	addl	%esi, %ebp
	movzbl	(%r8), %edx
	addl	%edx, %ebp
	movzbl	(%rcx), %r10d
	leal	100(%r10,%rbp), %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#5:                                 #   in Loop: Header=BB16_4 Depth=2
	movzbl	-2(%r9,%r15), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, %r8d
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#6:                                 #   in Loop: Header=BB16_4 Depth=2
	movzbl	-1(%r9,%r15), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#7:                                 #   in Loop: Header=BB16_4 Depth=2
	movzbl	(%r9,%r15), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 236(%rsp)         # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#8:                                 #   in Loop: Header=BB16_4 Depth=2
	movzbl	-6(%r9,%r14), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movq	%rax, 384(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#9:                                 #   in Loop: Header=BB16_4 Depth=2
	movzbl	-5(%r9,%r14), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#10:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-4(%r9,%r14), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#11:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-3(%r9,%r14), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 232(%rsp)         # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#12:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-2(%r9,%r14), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#13:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-1(%r9,%r14), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#14:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	(%r9,%r14), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#15:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-4(%r9,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#16:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-3(%r9,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 224(%rsp)         # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#17:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-2(%r9,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#18:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	-1(%r9,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 220(%rsp)         # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#19:                                #   in Loop: Header=BB16_4 Depth=2
	movzbl	(%r9,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#20:                                #   in Loop: Header=BB16_4 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	movzbl	(%rax,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#21:                                #   in Loop: Header=BB16_4 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	movzbl	1(%rax,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, 216(%rsp)         # 4-byte Spill
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#22:                                #   in Loop: Header=BB16_4 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	movzbl	2(%rax,%r12), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movzbl	(%rcx), %eax
	addl	%eax, %ebp
	cmpl	%r13d, %ebp
	jge	.LBB16_45
# BB#23:                                #   in Loop: Header=BB16_4 Depth=2
	movl	%r11d, 212(%rsp)        # 4-byte Spill
	movl	%eax, 208(%rsp)         # 4-byte Spill
	movl	%r11d, %eax
	subl	88(%rsp), %eax          # 4-byte Folded Reload
	subl	%esi, %eax
	addl	236(%rsp), %eax         # 4-byte Folded Reload
	movq	400(%rsp), %rcx         # 8-byte Reload
	subl	240(%rsp), %ecx         # 4-byte Folded Reload
	movq	384(%rsp), %r11         # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	%esi, %esi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	subl	%esi, %ecx
	subl	328(%rsp), %ecx         # 4-byte Folded Reload
	addl	336(%rsp), %ecx         # 4-byte Folded Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	leal	(%rcx,%rsi,2), %esi
	movq	96(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rcx), %ecx
	subl	%ecx, %esi
	addl	%edx, %edx
	subl	104(%rsp), %esi         # 4-byte Folded Reload
	addl	112(%rsp), %esi         # 4-byte Folded Reload
	subl	%edx, %esi
	subl	%r10d, %esi
	addl	%r8d, %esi
	movq	280(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rcx), %ecx
	subl	%ecx, %esi
	movq	56(%rsp), %r8           # 8-byte Reload
	addl	16(%rsp), %r8d          # 4-byte Folded Reload
	addl	68(%rsp), %r8d          # 4-byte Folded Reload
	subl	76(%rsp), %esi          # 4-byte Folded Reload
	addl	72(%rsp), %esi          # 4-byte Folded Reload
	leal	(%rsi,%r8,2), %ecx
	subl	%r11d, %eax
	addl	64(%rsp), %eax          # 4-byte Folded Reload
	leal	(%rax,%rax,2), %eax
	addl	%eax, %ecx
	movq	256(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax), %eax
	subl	%eax, %ecx
	movl	224(%rsp), %eax         # 4-byte Reload
	subl	%eax, %ecx
	movl	220(%rsp), %esi         # 4-byte Reload
	addl	%esi, %ecx
	movq	248(%rsp), %rdx         # 8-byte Reload
	leal	(%rcx,%rdx,2), %r8d
	movq	344(%rsp), %r10         # 8-byte Reload
	subl	%r10d, %r8d
	subl	408(%rsp), %r10d        # 4-byte Folded Reload
	addl	216(%rsp), %r10d        # 4-byte Folded Reload
	movl	208(%rsp), %ecx         # 4-byte Reload
	addl	%ecx, %r8d
	addl	%ecx, %r10d
	movq	352(%rsp), %rdx         # 8-byte Reload
	addl	%eax, %edx
	addl	%esi, %edx
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	328(%rsp), %rcx         # 8-byte Reload
	leal	(%rax,%rcx,2), %eax
	movq	416(%rsp), %rcx         # 8-byte Reload
	leal	(%rax,%rcx,2), %eax
	movq	336(%rsp), %rcx         # 8-byte Reload
	leal	(%rax,%rcx,2), %eax
	movq	80(%rsp), %rcx          # 8-byte Reload
	leal	(%rax,%rcx,2), %eax
	addl	88(%rsp), %eax          # 4-byte Folded Reload
	addl	96(%rsp), %eax          # 4-byte Folded Reload
	addl	104(%rsp), %eax         # 4-byte Folded Reload
	addl	184(%rsp), %eax         # 4-byte Folded Reload
	addl	112(%rsp), %eax         # 4-byte Folded Reload
	addl	16(%rsp), %eax          # 4-byte Folded Reload
	addl	212(%rsp), %eax         # 4-byte Folded Reload
	subl	%eax, %r11d
	addl	280(%rsp), %r11d        # 4-byte Folded Reload
	addl	76(%rsp), %r11d         # 4-byte Folded Reload
	addl	232(%rsp), %r11d        # 4-byte Folded Reload
	addl	72(%rsp), %r11d         # 4-byte Folded Reload
	addl	68(%rsp), %r11d         # 4-byte Folded Reload
	addl	64(%rsp), %r11d         # 4-byte Folded Reload
	leal	(%r10,%r10,2), %eax
	movq	256(%rsp), %rcx         # 8-byte Reload
	leal	(%r11,%rcx,2), %ecx
	leal	(%rcx,%rdx,2), %ecx
	movq	248(%rsp), %rdx         # 8-byte Reload
	leal	(%rcx,%rdx,2), %r11d
	addl	%eax, %r11d
	movl	%r8d, %eax
	imull	%eax, %eax
	movl	%r11d, %ecx
	imull	%ecx, %ecx
	leal	(%rcx,%rax), %edx
	movl	%ebp, %esi
	imull	%esi, %esi
	shrl	%esi
	cmpl	%esi, %edx
	jbe	.LBB16_45
# BB#24:                                #   in Loop: Header=BB16_4 Depth=2
	cmpl	%eax, %ecx
	movl	%r8d, 56(%rsp)          # 4-byte Spill
	jae	.LBB16_36
# BB#25:                                #   in Loop: Header=BB16_4 Depth=2
	cvtsi2ssl	%r11d, %xmm4
	movl	%r8d, %eax
	negl	%eax
	cmovll	%r8d, %eax
	cvtsi2ssl	%eax, %xmm5
	divss	%xmm5, %xmm4
	cltd
	idivl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	ucomiss	%xmm4, %xmm0
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm4, %xmm5
	movapd	%xmm1, %xmm6
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r10         # 8-byte Reload
	ja	.LBB16_27
# BB#26:                                #   in Loop: Header=BB16_4 Depth=2
	movapd	%xmm2, %xmm6
.LBB16_27:                              #   in Loop: Header=BB16_4 Depth=2
	addsd	%xmm6, %xmm5
	cvttsd2si	%xmm5, %ecx
	addl	%r10d, %ecx
	imull	%r8d, %ecx
	addl	%eax, %ecx
	leal	5(%rbx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	negq	%rcx
	movzbl	(%rdi,%rcx), %edx
	movaps	%xmm4, %xmm5
	addss	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm0
	cvtss2sd	%xmm5, %xmm5
	movapd	%xmm1, %xmm6
	ja	.LBB16_29
# BB#28:                                #   in Loop: Header=BB16_4 Depth=2
	movapd	%xmm2, %xmm6
.LBB16_29:                              #   in Loop: Header=BB16_4 Depth=2
	addsd	%xmm6, %xmm5
	cvttsd2si	%xmm5, %ecx
	addl	%r10d, %ecx
	imull	%r8d, %ecx
	leal	(%rcx,%rax,2), %ecx
	leal	5(%rbx,%rcx), %ecx
	movslq	%ecx, %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rsi,%rcx), %ecx
	negq	%rcx
	movzbl	(%rdi,%rcx), %ecx
	mulss	%xmm3, %xmm4
	ucomiss	%xmm4, %xmm0
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	ja	.LBB16_31
# BB#30:                                #   in Loop: Header=BB16_4 Depth=2
	movapd	%xmm2, %xmm5
.LBB16_31:                              #   in Loop: Header=BB16_4 Depth=2
	addl	%edx, %ecx
	addsd	%xmm5, %xmm4
	cvttsd2si	%xmm4, %edx
	addl	%r10d, %edx
	imull	%r8d, %edx
	addl	120(%rsp), %edx         # 4-byte Folded Reload
	leal	(%rax,%rax,2), %eax
	addl	%edx, %eax
	jmp	.LBB16_43
.LBB16_36:                              #   in Loop: Header=BB16_4 Depth=2
	cvtsi2ssl	%r8d, %xmm4
	movl	%r11d, %eax
	negl	%eax
	cmovll	%r11d, %eax
	cvtsi2ssl	%eax, %xmm5
	divss	%xmm5, %xmm4
	cltd
	idivl	%r11d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	ucomiss	%xmm4, %xmm0
	xorps	%xmm5, %xmm5
	cvtss2sd	%xmm4, %xmm5
	movapd	%xmm1, %xmm6
	movq	264(%rsp), %r8          # 8-byte Reload
	movq	176(%rsp), %r10         # 8-byte Reload
	ja	.LBB16_38
# BB#37:                                #   in Loop: Header=BB16_4 Depth=2
	movapd	%xmm2, %xmm6
.LBB16_38:                              #   in Loop: Header=BB16_4 Depth=2
	addsd	%xmm6, %xmm5
	cvttsd2si	%xmm5, %ecx
	leal	(%r10,%rax), %edx
	imull	%r8d, %edx
	addl	%ecx, %edx
	leal	5(%rbx,%rdx), %ecx
	movslq	%ecx, %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rcx), %ecx
	negq	%rcx
	movzbl	(%rdi,%rcx), %edx
	movaps	%xmm4, %xmm5
	addss	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm0
	cvtss2sd	%xmm5, %xmm5
	movapd	%xmm1, %xmm6
	ja	.LBB16_40
# BB#39:                                #   in Loop: Header=BB16_4 Depth=2
	movapd	%xmm2, %xmm6
.LBB16_40:                              #   in Loop: Header=BB16_4 Depth=2
	addsd	%xmm6, %xmm5
	cvttsd2si	%xmm5, %ecx
	leal	(%r10,%rax,2), %esi
	imull	%r8d, %esi
	addl	%ecx, %esi
	leal	5(%rbx,%rsi), %ecx
	movslq	%ecx, %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	movzbl	(%rsi,%rcx), %ecx
	negq	%rcx
	movzbl	(%rdi,%rcx), %ecx
	leal	(%rax,%rax,2), %eax
	mulss	%xmm3, %xmm4
	ucomiss	%xmm4, %xmm0
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	ja	.LBB16_42
# BB#41:                                #   in Loop: Header=BB16_4 Depth=2
	movapd	%xmm2, %xmm5
.LBB16_42:                              #   in Loop: Header=BB16_4 Depth=2
	addl	%edx, %ecx
	addsd	%xmm5, %xmm4
	cvttsd2si	%xmm4, %edx
	addl	%r10d, %eax
	imull	%r8d, %eax
	addl	%edx, %eax
	leal	5(%rbx,%rax), %eax
.LBB16_43:                              #   in Loop: Header=BB16_4 Depth=2
	cltq
	movq	40(%rsp), %rdx          # 8-byte Reload
	movzbl	(%rdx,%rax), %eax
	subq	%rax, %rdi
	movzbl	(%rdi), %eax
	addl	%ecx, %eax
	cmpl	$291, %eax              # imm = 0x123
	movl	56(%rsp), %edx          # 4-byte Reload
	jl	.LBB16_45
# BB#44:                                #   in Loop: Header=BB16_4 Depth=2
	movl	%r13d, %eax
	subl	%ebp, %eax
	movq	376(%rsp), %rcx         # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movl	%eax, (%rcx,%rsi,4)
	imull	$51, %edx, %eax
	cltd
	idivl	%ebp
	movq	368(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%rsi,4)
	imull	$51, %r11d, %eax
	cltd
	idivl	%ebp
	movq	360(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx,%rsi,4)
	.p2align	4, 0x90
.LBB16_45:                              #   in Loop: Header=BB16_4 Depth=2
	movq	120(%rsp), %rax         # 8-byte Reload
	incq	%rax
	movq	128(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	48(%rsp), %rbp          # 8-byte Reload
	incq	%rbp
	incq	%r15
	incq	%r14
	incq	%r12
	incq	%rbx
	cmpq	%rbx, 424(%rsp)         # 8-byte Folded Reload
	jne	.LBB16_4
.LBB16_46:                              # %._crit_edge858
                                        #   in Loop: Header=BB16_2 Depth=1
	movq	176(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	movq	160(%rsp), %rax         # 8-byte Reload
	addq	%rax, 288(%rsp)         # 8-byte Folded Spill
	addq	%rax, 320(%rsp)         # 8-byte Folded Spill
	addq	%rax, 312(%rsp)         # 8-byte Folded Spill
	addq	%rax, 304(%rsp)         # 8-byte Folded Spill
	addq	%rax, 296(%rsp)         # 8-byte Folded Spill
	cmpq	392(%rsp), %rcx         # 8-byte Folded Reload
	jne	.LBB16_2
# BB#32:                                # %.preheader846
	xorl	%eax, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	228(%rsp), %edx         # 4-byte Reload
	cmpl	$6, %edx
	movq	24(%rsp), %r12          # 8-byte Reload
	jl	.LBB16_102
# BB#33:                                # %.preheader.lr.ph
	movslq	168(%rsp), %r14         # 4-byte Folded Reload
	movslq	%edx, %rdi
	movq	160(%rsp), %rdx         # 8-byte Reload
	leaq	(,%rdx,4), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movl	$5, %r10d
	movq	%rdi, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB16_34:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_48 Depth 2
	cmpl	$5, 168(%rsp)           # 4-byte Folded Reload
	jle	.LBB16_35
# BB#47:                                # %.lr.ph
                                        #   in Loop: Header=BB16_34 Depth=1
	movq	%r10, %rsi
	imulq	160(%rsp), %rsi         # 8-byte Folded Reload
	leaq	-2(%r10), %r8
	leaq	-1(%r10), %r15
	leaq	1(%r10), %r11
	leaq	2(%r10), %rax
	leaq	3(%r10), %rcx
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %r9
	imulq	%r10, %r9
	leaq	-3(%r10), %rbx
	leaq	8(%r12,%r9), %rbp
	movq	152(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%r9), %rdi
	movq	%rdi, 192(%rsp)         # 8-byte Spill
	movq	%r15, %rdi
	movq	%r8, %r15
	addq	144(%rsp), %r9          # 8-byte Folded Reload
	addq	40(%rsp), %rsi          # 8-byte Folded Reload
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	imulq	%rdx, %rbx
	addq	%r12, %rbx
	imulq	%rdx, %r15
	addq	%r12, %r15
	imulq	%rdx, %rdi
	addq	%r12, %rdi
	movq	%rdx, %rsi
	imulq	%r11, %rsi
	addq	%r12, %rsi
	imulq	%rdx, %rax
	addq	%r12, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	imulq	%rdx, %rcx
	addq	%r12, %rcx
	movq	%rcx, %rdx
	movl	$5, %r13d
	.p2align	4, 0x90
.LBB16_48:                              #   Parent Loop BB16_34 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-8(%rbp,%r13,4), %r8d
	testl	%r8d, %r8d
	jle	.LBB16_99
# BB#49:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-12(%rbx,%r13,4), %r8d
	jle	.LBB16_99
# BB#50:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-8(%rbx,%r13,4), %r8d
	jle	.LBB16_99
# BB#51:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-4(%rbx,%r13,4), %r8d
	jle	.LBB16_99
# BB#52:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	(%rbx,%r13,4), %r8d
	jle	.LBB16_99
# BB#53:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	4(%rbx,%r13,4), %r8d
	jle	.LBB16_99
# BB#54:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	8(%rbx,%r13,4), %r8d
	jle	.LBB16_99
# BB#55:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	12(%rbx,%r13,4), %r8d
	jle	.LBB16_99
# BB#56:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-12(%r15,%r13,4), %r8d
	jle	.LBB16_99
# BB#57:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-8(%r15,%r13,4), %r8d
	jle	.LBB16_99
# BB#58:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-4(%r15,%r13,4), %r8d
	jle	.LBB16_99
# BB#59:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	(%r15,%r13,4), %r8d
	jle	.LBB16_99
# BB#60:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	4(%r15,%r13,4), %r8d
	jle	.LBB16_99
# BB#61:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	8(%r15,%r13,4), %r8d
	jle	.LBB16_99
# BB#62:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	12(%r15,%r13,4), %r8d
	jle	.LBB16_99
# BB#63:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-12(%rdi,%r13,4), %r8d
	jle	.LBB16_99
# BB#64:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-8(%rdi,%r13,4), %r8d
	jle	.LBB16_99
# BB#65:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-4(%rdi,%r13,4), %r8d
	jle	.LBB16_99
# BB#66:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	(%rdi,%r13,4), %r8d
	jle	.LBB16_99
# BB#67:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	4(%rdi,%r13,4), %r8d
	jle	.LBB16_99
# BB#68:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	8(%rdi,%r13,4), %r8d
	jle	.LBB16_99
# BB#69:                                #   in Loop: Header=BB16_48 Depth=2
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	cmpl	12(%rdi,%r13,4), %r8d
	jle	.LBB16_70
# BB#71:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-20(%rbp,%r13,4), %r8d
	jle	.LBB16_70
# BB#72:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-16(%rbp,%r13,4), %r8d
	jle	.LBB16_70
# BB#73:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-12(%rbp,%r13,4), %r8d
	jle	.LBB16_70
# BB#74:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-4(%rbp,%r13,4), %r8d
	jl	.LBB16_70
# BB#75:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	(%rbp,%r13,4), %r8d
	jl	.LBB16_70
# BB#76:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	4(%rbp,%r13,4), %r8d
	jl	.LBB16_70
# BB#77:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-12(%rsi,%r13,4), %r8d
	jl	.LBB16_70
# BB#78:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-8(%rsi,%r13,4), %r8d
	jl	.LBB16_70
# BB#79:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-4(%rsi,%r13,4), %r8d
	jl	.LBB16_70
# BB#80:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	(%rsi,%r13,4), %r8d
	jl	.LBB16_70
# BB#81:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	4(%rsi,%r13,4), %r8d
	jl	.LBB16_70
# BB#82:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	8(%rsi,%r13,4), %r8d
	jl	.LBB16_70
# BB#83:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	12(%rsi,%r13,4), %r8d
	jl	.LBB16_70
# BB#84:                                #   in Loop: Header=BB16_48 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-12(%rcx,%r13,4), %r8d
	jl	.LBB16_70
# BB#85:                                #   in Loop: Header=BB16_48 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-8(%rcx,%r13,4), %r8d
	jl	.LBB16_70
# BB#86:                                #   in Loop: Header=BB16_48 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-4(%rcx,%r13,4), %r8d
	jl	.LBB16_70
# BB#87:                                #   in Loop: Header=BB16_48 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx,%r13,4), %r8d
	jl	.LBB16_70
# BB#88:                                #   in Loop: Header=BB16_48 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	4(%rcx,%r13,4), %r8d
	jl	.LBB16_70
# BB#89:                                #   in Loop: Header=BB16_48 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	8(%rcx,%r13,4), %r8d
	jl	.LBB16_70
# BB#90:                                #   in Loop: Header=BB16_48 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	12(%rcx,%r13,4), %r8d
	jl	.LBB16_70
# BB#91:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-12(%rdx,%r13,4), %r8d
	jl	.LBB16_70
# BB#92:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-8(%rdx,%r13,4), %r8d
	jl	.LBB16_70
# BB#93:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	-4(%rdx,%r13,4), %r8d
	jl	.LBB16_70
# BB#94:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	(%rdx,%r13,4), %r8d
	jl	.LBB16_70
# BB#95:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	4(%rdx,%r13,4), %r8d
	jl	.LBB16_70
# BB#96:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	8(%rdx,%r13,4), %r8d
	jl	.LBB16_70
# BB#97:                                #   in Loop: Header=BB16_48 Depth=2
	cmpl	12(%rdx,%r13,4), %r8d
	jge	.LBB16_98
.LBB16_70:                              #   in Loop: Header=BB16_48 Depth=2
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB16_99:                              #   in Loop: Header=BB16_48 Depth=2
	incq	%r13
	cmpq	%r14, %r13
	jl	.LBB16_48
	jmp	.LBB16_100
.LBB16_98:                              #   in Loop: Header=BB16_48 Depth=2
	movq	48(%rsp), %r15          # 8-byte Reload
	movslq	%r15d, %r15
	leaq	(%r15,%r15,2), %r12
	movq	272(%rsp), %rdi         # 8-byte Reload
	movl	$0, 8(%rdi,%r12,8)
	movl	%r13d, (%rdi,%r12,8)
	movl	%r10d, 4(%rdi,%r12,8)
	movq	192(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r13,4), %r8d
	movl	%r8d, 12(%rdi,%r12,8)
	movl	(%r9,%r13,4), %ecx
	movl	%ecx, 16(%rdi,%r12,8)
	movq	200(%rsp), %rcx         # 8-byte Reload
	movzbl	(%rcx,%r13), %ecx
	movl	%ecx, 20(%rdi,%r12,8)
	incl	%r15d
	movq	%r15, 48(%rsp)          # 8-byte Spill
	cmpl	$15000, %r15d           # imm = 0x3A98
	movq	24(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	jne	.LBB16_99
	jmp	.LBB16_103
	.p2align	4, 0x90
.LBB16_35:                              # %.preheader.._crit_edge_crit_edge
                                        #   in Loop: Header=BB16_34 Depth=1
	incq	%r10
	movq	%r10, %r11
	jmp	.LBB16_101
	.p2align	4, 0x90
.LBB16_100:                             #   in Loop: Header=BB16_34 Depth=1
	movq	128(%rsp), %rdi         # 8-byte Reload
.LBB16_101:                             # %._crit_edge
                                        #   in Loop: Header=BB16_34 Depth=1
	cmpq	%rdi, %r11
	movq	%r11, %r10
	jl	.LBB16_34
.LBB16_102:                             # %._crit_edge854
	movslq	48(%rsp), %rax          # 4-byte Folded Reload
	leaq	(%rax,%rax,2), %rax
	movq	272(%rsp), %rcx         # 8-byte Reload
	movl	$7, 8(%rcx,%rax,8)
	movq	152(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	144(%rsp), %rdi         # 8-byte Reload
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.LBB16_103:
	movq	stderr(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end16:
	.size	susan_corners, .Lfunc_end16-susan_corners
	.cfi_endproc

	.globl	susan_corners_quick
	.p2align	4, 0x90
	.type	susan_corners_quick,@function
susan_corners_quick:                    # @susan_corners_quick
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi154:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi157:
	.cfi_def_cfa_offset 288
.Lcfi158:
	.cfi_offset %rbx, -56
.Lcfi159:
	.cfi_offset %r12, -48
.Lcfi160:
	.cfi_offset %r13, -40
.Lcfi161:
	.cfi_offset %r14, -32
.Lcfi162:
	.cfi_offset %r15, -24
.Lcfi163:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%r8, 136(%rsp)          # 8-byte Spill
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	288(%rsp), %ebp
	movl	%ebp, %eax
	imull	%r12d, %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%eax, %eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	memset
	addl	$-7, %ebp
	cmpl	$8, %ebp
	jl	.LBB17_86
# BB#1:                                 # %.preheader769.lr.ph
	movslq	%r12d, %r8
	addl	$-7, %r12d
	movl	%ebp, 48(%rsp)          # 4-byte Spill
	movl	%ebp, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	(,%r8,4), %rax
	leaq	(%r15,%r8,4), %r13
	leaq	-3(%r8,%r8,4), %rsi
	addq	%r15, %rsi
	leaq	13(%r8), %r10
	leaq	13(%r8,%r8), %rcx
	leaq	13(%r8,%r8,2), %rbx
	leaq	-1(%rcx,%r8,2), %rdi
	movq	%rax, 216(%rsp)         # 8-byte Spill
	leaq	-6(%rax,%r8,2), %rax
	addq	%r15, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	(,%r8,8), %r9
	movq	%r8, 184(%rsp)          # 8-byte Spill
	subq	%r8, %r9
	movq	%r15, 144(%rsp)         # 8-byte Spill
	leaq	(%r15,%r9), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	%r14, 224(%rsp)         # 8-byte Spill
	leaq	(%r14,%r9,4), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	-7(%r12), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	$7, %edx
	movl	$6, %r9d
	movq	%r12, 192(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB17_2:                               # %.preheader769
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_3 Depth 2
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	cmpl	$8, %r12d
	movq	96(%rsp), %r11          # 8-byte Reload
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %r12
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	%r9, 8(%rsp)            # 8-byte Spill
	movl	72(%rsp), %edx          # 4-byte Reload
	jl	.LBB17_24
	.p2align	4, 0x90
.LBB17_3:                               #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	160(%rsp), %rax         # 8-byte Reload
	movzbl	1(%rax,%r9), %r15d
	addq	24(%rsp), %r15          # 8-byte Folded Reload
	movzbl	(%r13,%r9), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	movzbl	1(%r13,%r9), %ebx
	movq	%r15, %rbp
	subq	%rbx, %rbp
	movzbl	(%rbp), %ebp
	addl	%eax, %ebp
	movzbl	2(%r13,%r9), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	2(%rsi,%r9), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	addl	%eax, %ebp
	movzbl	3(%rsi,%r9), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	4(%rsi,%r9), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	addl	%eax, %ebp
	movzbl	5(%rsi,%r9), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	6(%rsi,%r9), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	addl	%eax, %ebp
	movzbl	-6(%rsi,%r10), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	-5(%rsi,%r10), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	addl	%eax, %ebp
	movzbl	-4(%rsi,%r10), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	-3(%rsi,%r10), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	addl	%eax, %ebp
	movzbl	-2(%rsi,%r10), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	-1(%rsi,%r10), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	addl	%eax, %ebp
	movzbl	(%rsi,%r10), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	-6(%rsi,%rcx), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	addl	%eax, %ebp
	movzbl	-5(%rsi,%rcx), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %eax
	addl	%ebp, %eax
	movzbl	-4(%rsi,%rcx), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebp
	leal	100(%rbp,%rax), %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#4:                                 #   in Loop: Header=BB17_3 Depth=2
	movzbl	-2(%rsi,%rcx), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %r8d
	addl	%eax, %r8d
	cmpl	%edx, %r8d
	jge	.LBB17_23
# BB#5:                                 #   in Loop: Header=BB17_3 Depth=2
	movzbl	-1(%rsi,%rcx), %eax
	movq	%r15, %rbx
	subq	%rax, %rbx
	movzbl	(%rbx), %r14d
	addl	%r8d, %r14d
	cmpl	%edx, %r14d
	jge	.LBB17_23
# BB#6:                                 #   in Loop: Header=BB17_3 Depth=2
	movzbl	(%rsi,%rcx), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %ebx
	addl	%r14d, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#7:                                 #   in Loop: Header=BB17_3 Depth=2
	movzbl	-6(%rsi,%r12), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#8:                                 #   in Loop: Header=BB17_3 Depth=2
	movzbl	-5(%rsi,%r12), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebx
	addl	%eax, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#9:                                 #   in Loop: Header=BB17_3 Depth=2
	movzbl	-4(%rsi,%r12), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#10:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	-3(%rsi,%r12), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebx
	addl	%eax, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#11:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	-2(%rsi,%r12), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#12:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	-1(%rsi,%r12), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebx
	addl	%eax, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#13:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	(%rsi,%r12), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#14:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	-4(%rsi,%rdi), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebx
	addl	%eax, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#15:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	-3(%rsi,%rdi), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#16:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	-2(%rsi,%rdi), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebx
	addl	%eax, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#17:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	-1(%rsi,%rdi), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#18:                                #   in Loop: Header=BB17_3 Depth=2
	movzbl	(%rsi,%rdi), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebx
	addl	%eax, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#19:                                #   in Loop: Header=BB17_3 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax,%rdi), %eax
	movq	%r15, %rbp
	subq	%rax, %rbp
	movzbl	(%rbp), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#20:                                #   in Loop: Header=BB17_3 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	movzbl	1(%rbp,%rdi), %ebp
	movq	%r15, %rbx
	subq	%rbp, %rbx
	movzbl	(%rbx), %ebx
	addl	%eax, %ebx
	cmpl	%edx, %ebx
	jge	.LBB17_23
# BB#21:                                #   in Loop: Header=BB17_3 Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	movzbl	2(%rax,%rdi), %eax
	subq	%rax, %r15
	movzbl	(%r15), %eax
	addl	%ebx, %eax
	cmpl	%edx, %eax
	jge	.LBB17_23
# BB#22:                                #   in Loop: Header=BB17_3 Depth=2
	movl	%edx, %ebp
	subl	%eax, %ebp
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%ebp, 4(%rax,%r9,4)
	.p2align	4, 0x90
.LBB17_23:                              #   in Loop: Header=BB17_3 Depth=2
	incq	%r9
	incq	%r10
	incq	%rcx
	incq	%r12
	incq	%rdi
	decq	%r11
	jne	.LBB17_3
.LBB17_24:                              # %._crit_edge780
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	addq	%rax, %r9
	movq	32(%rsp), %r10          # 8-byte Reload
	addq	%rax, %r10
	movq	88(%rsp), %rcx          # 8-byte Reload
	addq	%rax, %rcx
	movq	80(%rsp), %rbx          # 8-byte Reload
	addq	%rax, %rbx
	movq	152(%rsp), %rdi         # 8-byte Reload
	addq	%rax, %rdi
	cmpq	104(%rsp), %rdx         # 8-byte Folded Reload
	movq	192(%rsp), %r12         # 8-byte Reload
	jne	.LBB17_2
# BB#25:                                # %.preheader768
	movl	48(%rsp), %eax          # 4-byte Reload
	cmpl	$8, %eax
	movq	144(%rsp), %r13         # 8-byte Reload
	jl	.LBB17_86
# BB#26:                                # %.preheader.lr.ph
	movslq	%r12d, %rsi
	movslq	%eax, %rdx
	addq	$9, %r13
	xorl	%ecx, %ecx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	$7, %edi
	movq	%r13, 144(%rsp)         # 8-byte Spill
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB17_27:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_30 Depth 2
	cmpl	$7, %r12d
	jle	.LBB17_28
# BB#29:                                # %.lr.ph
                                        #   in Loop: Header=BB17_27 Depth=1
	movq	%rdi, %rax
	movq	184(%rsp), %rdx         # 8-byte Reload
	imulq	%rdx, %rax
	leaq	-2(%rdi), %rcx
	movq	%rcx, %r11
	imulq	%rdx, %r11
	leaq	-1(%rdi), %r15
	movq	%r15, %rbx
	imulq	%rdx, %rbx
	leaq	1(%rdi), %r14
	movq	%r14, %r10
	imulq	%rdx, %r10
	leaq	2(%rdi), %rsi
	movq	%rsi, %r8
	imulq	%rdx, %r8
	leaq	3(%rdi), %r12
	movq	216(%rsp), %r9          # 8-byte Reload
	movq	%r9, %rbp
	imulq	%rdi, %rbp
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	leaq	-3(%rdi), %rdi
	movq	224(%rsp), %rdx         # 8-byte Reload
	leaq	40(%rdx,%rbp), %rbp
	addq	%r13, %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	imulq	%r9, %rdi
	leaq	40(%rdx,%rdi), %rax
	movq	%r14, %rdi
	imulq	%r9, %rcx
	leaq	40(%rdx,%rcx), %r14
	addq	%r13, %r11
	movq	%r11, 80(%rsp)          # 8-byte Spill
	imulq	%r9, %r15
	leaq	40(%rdx,%r15), %r11
	addq	%r13, %rbx
	movq	%r9, %rcx
	imulq	%rdi, %rcx
	leaq	40(%rdx,%rcx), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rax, %rcx
	addq	%r13, %r10
	movq	%r10, %r15
	imulq	%r9, %rsi
	leaq	40(%rdx,%rsi), %r10
	movq	24(%rsp), %rsi          # 8-byte Reload
	addq	%r13, %r8
	imulq	%r9, %r12
	leaq	40(%rdx,%r12), %rdx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB17_30:                              #   Parent Loop BB17_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-12(%rbp,%r12,4), %eax
	testl	%eax, %eax
	jle	.LBB17_83
# BB#31:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-24(%rcx,%r12,4), %eax
	jle	.LBB17_83
# BB#32:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-20(%rcx,%r12,4), %eax
	jle	.LBB17_83
# BB#33:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-16(%rcx,%r12,4), %eax
	jle	.LBB17_83
# BB#34:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-12(%rcx,%r12,4), %eax
	jle	.LBB17_83
# BB#35:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-8(%rcx,%r12,4), %eax
	jle	.LBB17_83
# BB#36:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-4(%rcx,%r12,4), %eax
	jle	.LBB17_83
# BB#37:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	(%rcx,%r12,4), %eax
	jle	.LBB17_83
# BB#38:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-24(%r14,%r12,4), %eax
	jle	.LBB17_83
# BB#39:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-20(%r14,%r12,4), %eax
	jle	.LBB17_83
# BB#40:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-16(%r14,%r12,4), %eax
	jle	.LBB17_83
# BB#41:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-12(%r14,%r12,4), %eax
	jle	.LBB17_83
# BB#42:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-8(%r14,%r12,4), %eax
	jle	.LBB17_83
# BB#43:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-4(%r14,%r12,4), %eax
	jle	.LBB17_83
# BB#44:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	(%r14,%r12,4), %eax
	jle	.LBB17_83
# BB#45:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-24(%r11,%r12,4), %eax
	jle	.LBB17_83
# BB#46:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-20(%r11,%r12,4), %eax
	jle	.LBB17_83
# BB#47:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-16(%r11,%r12,4), %eax
	jle	.LBB17_83
# BB#48:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-12(%r11,%r12,4), %eax
	jle	.LBB17_83
# BB#49:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-8(%r11,%r12,4), %eax
	jle	.LBB17_83
# BB#50:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-4(%r11,%r12,4), %eax
	jle	.LBB17_83
# BB#51:                                #   in Loop: Header=BB17_30 Depth=2
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	movq	%r14, %r9
	movq	%rcx, %r13
	movq	%rdi, %rdx
	movq	%r11, %r10
	cmpl	(%r11,%r12,4), %eax
	jle	.LBB17_52
# BB#54:                                #   in Loop: Header=BB17_30 Depth=2
	movq	%rbp, %r11
	cmpl	-24(%rbp,%r12,4), %eax
	jle	.LBB17_55
# BB#56:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-20(%r11,%r12,4), %eax
	jle	.LBB17_55
# BB#57:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-16(%r11,%r12,4), %eax
	jle	.LBB17_55
# BB#58:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-8(%r11,%r12,4), %eax
	jl	.LBB17_55
# BB#59:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	-4(%r11,%r12,4), %eax
	jl	.LBB17_55
# BB#60:                                #   in Loop: Header=BB17_30 Depth=2
	cmpl	(%r11,%r12,4), %eax
	jl	.LBB17_55
# BB#61:                                #   in Loop: Header=BB17_30 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-24(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#62:                                #   in Loop: Header=BB17_30 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-20(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#63:                                #   in Loop: Header=BB17_30 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-16(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#64:                                #   in Loop: Header=BB17_30 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-12(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#65:                                #   in Loop: Header=BB17_30 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-8(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#66:                                #   in Loop: Header=BB17_30 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	-4(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#67:                                #   in Loop: Header=BB17_30 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#68:                                #   in Loop: Header=BB17_30 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	-24(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#69:                                #   in Loop: Header=BB17_30 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	-20(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#70:                                #   in Loop: Header=BB17_30 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	-16(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#71:                                #   in Loop: Header=BB17_30 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	-12(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#72:                                #   in Loop: Header=BB17_30 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	-8(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#73:                                #   in Loop: Header=BB17_30 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	-4(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#74:                                #   in Loop: Header=BB17_30 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#75:                                #   in Loop: Header=BB17_30 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	-24(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#76:                                #   in Loop: Header=BB17_30 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	-20(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#77:                                #   in Loop: Header=BB17_30 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	-16(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#78:                                #   in Loop: Header=BB17_30 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	-12(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#79:                                #   in Loop: Header=BB17_30 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	-8(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#80:                                #   in Loop: Header=BB17_30 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	-4(%rcx,%r12,4), %eax
	jl	.LBB17_55
# BB#81:                                #   in Loop: Header=BB17_30 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	(%rcx,%r12,4), %eax
	jge	.LBB17_82
.LBB17_55:                              #   in Loop: Header=BB17_30 Depth=2
	movq	%rdx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r11, %rbp
	jmp	.LBB17_53
.LBB17_52:                              #   in Loop: Header=BB17_30 Depth=2
	movq	%rdx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
.LBB17_53:                              #   in Loop: Header=BB17_30 Depth=2
	movq	%r13, %rcx
	movq	%r9, %r14
	movq	%r10, %r11
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	.p2align	4, 0x90
.LBB17_83:                              #   in Loop: Header=BB17_30 Depth=2
	leaq	1(%r12), %rax
	addq	$8, %r12
	cmpq	%rsi, %r12
	movq	%rax, %r12
	jl	.LBB17_30
	jmp	.LBB17_84
.LBB17_82:                              #   in Loop: Header=BB17_30 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	cltq
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,2), %rcx
	movq	136(%rsp), %rsi         # 8-byte Reload
	movl	$0, 8(%rsi,%rcx,8)
	leal	7(%r12), %eax
	movl	%eax, (%rsi,%rcx,8)
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	%eax, 4(%rsi,%rcx,8)
	movq	%rcx, %rbp
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rsi          # 8-byte Reload
	movzbl	-4(%rsi,%r12), %eax
	movzbl	-3(%rsi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	-2(%rsi,%r12), %eax
	addl	%ecx, %eax
	movzbl	-1(%rsi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	(%rsi,%r12), %eax
	addl	%ecx, %eax
	movq	%rbx, %rsi
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movzbl	-4(%rsi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	-3(%rsi,%r12), %eax
	addl	%ecx, %eax
	movzbl	-2(%rsi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	-1(%rsi,%r12), %eax
	addl	%ecx, %eax
	movzbl	(%rsi,%r12), %ecx
	addl	%eax, %ecx
	movq	88(%rsp), %rsi          # 8-byte Reload
	movzbl	-4(%rsi,%r12), %eax
	addl	%ecx, %eax
	movzbl	-3(%rsi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	-2(%rsi,%r12), %eax
	addl	%ecx, %eax
	movzbl	-1(%rsi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	(%rsi,%r12), %eax
	addl	%ecx, %eax
	movq	%r15, %r14
	movzbl	-4(%r14,%r12), %ecx
	addl	%eax, %ecx
	movzbl	-3(%r14,%r12), %eax
	addl	%ecx, %eax
	movzbl	-2(%r14,%r12), %ecx
	addl	%eax, %ecx
	movzbl	-1(%r14,%r12), %eax
	addl	%ecx, %eax
	movzbl	(%r14,%r12), %ecx
	addl	%eax, %ecx
	movq	%r8, %rdi
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movzbl	-4(%rdi,%r12), %eax
	addl	%ecx, %eax
	movzbl	-3(%rdi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	-2(%rdi,%r12), %eax
	addl	%ecx, %eax
	movzbl	-1(%rdi,%r12), %ecx
	addl	%eax, %ecx
	movzbl	(%rdi,%r12), %eax
	addl	%ecx, %eax
	cltq
	imulq	$1374389535, %rax, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	sarl	$3, %eax
	addl	%ecx, %eax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movl	%eax, 20(%rcx,%rbp,8)
	movq	80(%rsp), %rdi          # 8-byte Reload
	movzbl	(%rdi,%r12), %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax,%r12), %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movzbl	(%rsi,%r12), %ecx
	movl	%ecx, 120(%rsp)         # 4-byte Spill
	movq	%r14, %rsi
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movzbl	(%rsi,%r12), %r14d
	movq	96(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx,%r12), %ecx
	movzbl	-4(%rdi,%r12), %ebp
	movl	%ebp, 116(%rsp)         # 4-byte Spill
	movzbl	-4(%rax,%r12), %edi
	movl	%edi, 180(%rsp)         # 4-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	movzbl	-4(%rax,%r12), %eax
	movl	%eax, 44(%rsp)          # 4-byte Spill
	movzbl	-4(%rsi,%r12), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movq	96(%rsp), %rax          # 8-byte Reload
	movzbl	-4(%rax,%r12), %ebp
	movl	%ebp, 176(%rsp)         # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	leal	(%rax,%rsi), %eax
	addl	120(%rsp), %eax         # 4-byte Folded Reload
	addl	%r14d, %eax
	addl	%ecx, %eax
	subl	116(%rsp), %eax         # 4-byte Folded Reload
	subl	%edi, %eax
	subl	44(%rsp), %eax          # 4-byte Folded Reload
	subl	48(%rsp), %eax          # 4-byte Folded Reload
	subl	%ebp, %eax
	movq	80(%rsp), %rsi          # 8-byte Reload
	movzbl	-1(%rsi,%r12), %edi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movq	72(%rsp), %rdi          # 8-byte Reload
	movzbl	-1(%rdi,%r12), %edi
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rbp          # 8-byte Reload
	movzbl	-1(%rbp,%r12), %ebp
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	movq	104(%rsp), %rbp         # 8-byte Reload
	movzbl	-1(%rbp,%r12), %ebp
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	120(%rsp), %rbp         # 8-byte Reload
	leal	(%rdi,%rbp), %ebp
	leal	(%rbp,%rax,2), %eax
	movq	96(%rsp), %rdi          # 8-byte Reload
	movzbl	-1(%rdi,%r12), %ebp
	addl	52(%rsp), %eax          # 4-byte Folded Reload
	movzbl	-3(%rsi,%r12), %esi
	movl	%esi, 172(%rsp)         # 4-byte Spill
	addl	44(%rsp), %eax          # 4-byte Folded Reload
	addl	%ebp, %eax
	subl	%esi, %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
	movzbl	-3(%rsi,%r12), %esi
	movl	%esi, 52(%rsp)          # 4-byte Spill
	subl	%esi, %eax
	movq	88(%rsp), %rsi          # 8-byte Reload
	movzbl	-3(%rsi,%r12), %esi
	subl	%esi, %eax
	subl	128(%rsp), %ecx         # 4-byte Folded Reload
	subl	116(%rsp), %ecx         # 4-byte Folded Reload
	addl	176(%rsp), %ecx         # 4-byte Folded Reload
	subl	120(%rsp), %ecx         # 4-byte Folded Reload
	addl	%ebp, %ecx
	movq	104(%rsp), %rsi         # 8-byte Reload
	movzbl	-3(%rsi,%r12), %ebp
	subl	172(%rsp), %ecx         # 4-byte Folded Reload
	movq	%rdi, %rsi
	movzbl	-3(%rsi,%r12), %edi
	subl	%ebp, %eax
	subl	%edi, %eax
	addl	%edi, %ecx
	movzbl	-2(%rsi,%r12), %edi
	addl	%edi, %ecx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movzbl	-2(%rsi,%r12), %edi
	subl	%edi, %ecx
	subl	56(%rsp), %r14d         # 4-byte Folded Reload
	subl	180(%rsp), %r14d        # 4-byte Folded Reload
	addl	48(%rsp), %r14d         # 4-byte Folded Reload
	subl	200(%rsp), %r14d        # 4-byte Folded Reload
	addl	44(%rsp), %r14d         # 4-byte Folded Reload
	subl	52(%rsp), %r14d         # 4-byte Folded Reload
	addl	%ebp, %r14d
	movq	104(%rsp), %rsi         # 8-byte Reload
	movzbl	-2(%rsi,%r12), %esi
	addl	%esi, %r14d
	movq	72(%rsp), %rsi          # 8-byte Reload
	movzbl	-2(%rsi,%r12), %esi
	subl	%esi, %r14d
	leal	(%r14,%rcx,2), %ecx
	cltq
	imulq	$-2004318071, %rax, %rsi # imm = 0x88888889
	shrq	$32, %rsi
	addl	%esi, %eax
	movl	%eax, %esi
	shrl	$31, %esi
	sarl	$3, %eax
	addl	%esi, %eax
	movq	208(%rsp), %rsi         # 8-byte Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
	movl	%eax, 12(%rdi,%rsi,8)
	movslq	%ecx, %rax
	imulq	$-2004318071, %rax, %rcx # imm = 0x88888889
	shrq	$32, %rcx
	addl	%ecx, %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	sarl	$3, %eax
	addl	%ecx, %eax
	movl	%eax, 16(%rdi,%rsi,8)
	movq	64(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpl	$15000, %eax            # imm = 0x3A98
	movq	%rdx, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r11, %rbp
	movq	%r13, %rcx
	movq	%r9, %r14
	movq	%r10, %r11
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
	jne	.LBB17_83
	jmp	.LBB17_87
	.p2align	4, 0x90
.LBB17_28:                              # %.preheader.._crit_edge_crit_edge
                                        #   in Loop: Header=BB17_27 Depth=1
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB17_27
	jmp	.LBB17_86
	.p2align	4, 0x90
.LBB17_84:                              #   in Loop: Header=BB17_27 Depth=1
	movq	192(%rsp), %r12         # 8-byte Reload
	movq	144(%rsp), %r13         # 8-byte Reload
	movq	160(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, %rdi
	jl	.LBB17_27
.LBB17_86:                              # %._crit_edge776
	movslq	64(%rsp), %rax          # 4-byte Folded Reload
	leaq	(%rax,%rax,2), %rax
	movq	136(%rsp), %rcx         # 8-byte Reload
	movl	$7, 8(%rcx,%rax,8)
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_87:
	movq	stderr(%rip), %rcx
	movl	$.L.str.29, %edi
	movl	$18, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end17:
	.size	susan_corners_quick, .Lfunc_end17-susan_corners_quick
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	1082130432              # float 4
.LCPI18_3:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI18_2:
	.quad	4636737291354636288     # double 100
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi167:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi168:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi169:
	.cfi_def_cfa_offset 56
	subq	$360152, %rsp           # imm = 0x57ED8
.Lcfi170:
	.cfi_def_cfa_offset 360208
.Lcfi171:
	.cfi_offset %rbx, -56
.Lcfi172:
	.cfi_offset %r12, -48
.Lcfi173:
	.cfi_offset %r13, -40
.Lcfi174:
	.cfi_offset %r14, -32
.Lcfi175:
	.cfi_offset %r15, -24
.Lcfi176:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movl	%edi, %ebp
	cmpl	$2, %ebp
	jle	.LBB18_123
# BB#1:
	movq	8(%r15), %rdi
	leaq	56(%rsp), %rsi
	leaq	52(%rsp), %rdx
	leaq	48(%rsp), %rcx
	callq	get_image
	movl	$1, %ebx
	cmpl	$3, %ebp
	jne	.LBB18_3
# BB#2:
	movd	.LCPI18_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	xorl	%r12d, %r12d
	movl	$20, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	xorl	%r14d, %r14d
	jmp	.LBB18_21
.LBB18_3:                               # %.lr.ph.preheader
	movl	$3, %r13d
	movl	$20, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
	movd	.LCPI18_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	xorl	%r14d, %r14d
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB18_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r13d, %rax
	movq	(%r15,%rax,8), %rax
	cmpb	$45, (%rax)
	jne	.LBB18_119
# BB#5:                                 #   in Loop: Header=BB18_4 Depth=1
	movsbl	1(%rax), %eax
	addl	$-51, %eax
	cmpl	$65, %eax
	ja	.LBB18_20
# BB#6:                                 #   in Loop: Header=BB18_4 Depth=1
	jmpq	*.LJTI18_0(,%rax,8)
.LBB18_7:                               #   in Loop: Header=BB18_4 Depth=1
	movl	$1, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB18_20
.LBB18_8:                               #   in Loop: Header=BB18_4 Depth=1
	movl	$1, 44(%rsp)            # 4-byte Folded Spill
	jmp	.LBB18_20
.LBB18_9:                               #   in Loop: Header=BB18_4 Depth=1
	movl	$2, %r14d
	jmp	.LBB18_20
.LBB18_10:                              #   in Loop: Header=BB18_4 Depth=1
	incl	%r13d
	cmpl	%ebp, %r13d
	jge	.LBB18_120
# BB#11:                                #   in Loop: Header=BB18_4 Depth=1
	movslq	%r13d, %rax
	movq	(%r15,%rax,8), %rdi
	xorl	%esi, %esi
	callq	strtod
	cvtsd2ss	%xmm0, %xmm0
	movd	%xmm0, 8(%rsp)          # 4-byte Folded Spill
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_20
# BB#12:                                #   in Loop: Header=BB18_4 Depth=1
	movl	$1, 16(%rsp)            # 4-byte Folded Spill
	jmp	.LBB18_20
.LBB18_13:                              #   in Loop: Header=BB18_4 Depth=1
	movl	$1, %r14d
	jmp	.LBB18_20
.LBB18_14:                              #   in Loop: Header=BB18_4 Depth=1
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	jmp	.LBB18_20
.LBB18_15:                              #   in Loop: Header=BB18_4 Depth=1
	movl	$1, %r12d
	jmp	.LBB18_20
.LBB18_16:                              #   in Loop: Header=BB18_4 Depth=1
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	jmp	.LBB18_20
.LBB18_17:                              #   in Loop: Header=BB18_4 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB18_20
.LBB18_18:                              #   in Loop: Header=BB18_4 Depth=1
	incl	%r13d
	cmpl	%ebp, %r13d
	jge	.LBB18_121
# BB#19:                                #   in Loop: Header=BB18_4 Depth=1
	movslq	%r13d, %rax
	movq	(%r15,%rax,8), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB18_20:                              #   in Loop: Header=BB18_4 Depth=1
	incl	%r13d
	cmpl	%ebp, %r13d
	jl	.LBB18_4
.LBB18_21:                              # %._crit_edge
	testl	%r14d, %r14d
	cmovnel	%r14d, %ebx
	cmpl	$1, %r12d
	cmovnel	%r14d, %ebx
	testl	%ebx, %ebx
	je	.LBB18_33
# BB#22:                                # %._crit_edge
	cmpl	$1, %ebx
	je	.LBB18_36
# BB#23:                                # %._crit_edge
	cmpl	$2, %ebx
	jne	.LBB18_41
# BB#24:
	movl	52(%rsp), %eax
	movslq	%eax, %r13
	movl	48(%rsp), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	imull	%eax, %ecx
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movslq	%ecx, %rdi
	shlq	$2, %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$516, %edi              # imm = 0x204
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, %r14
	addq	$258, %r14              # imm = 0x102
	movq	32(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_25:                              # %.split.us.i108
                                        # =>This Inner Loop Header: Depth=1
	leal	-256(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	32(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm1
	xorps	.LCPI18_1(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	callq	exp
	mulsd	.LCPI18_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvttss2si	%xmm0, %eax
	movb	%al, 2(%rbp,%rbx)
	incq	%rbx
	cmpq	$513, %rbx              # imm = 0x201
	jne	.LBB18_25
# BB#26:                                # %setup_brightness_lut.exit109
	testl	%r12d, %r12d
	je	.LBB18_42
# BB#27:
	movq	56(%rsp), %r12
	movl	$1850, %ecx             # imm = 0x73A
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r14, %rdx
	movl	12(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %r8d
	movl	8(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %r9d
	callq	susan_principle
	movl	40(%rsp), %r9d          # 4-byte Reload
	testl	%r9d, %r9d
	jle	.LBB18_97
# BB#28:                                # %.lr.ph40.preheader.i111
	cmpl	$1, %r9d
	je	.LBB18_97
# BB#29:                                # %.lr.ph40..lr.ph40_crit_edge.i122.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movl	%r9d, %ecx
	leaq	-1(%rcx), %rsi
	cmpq	$8, %rsi
	jb	.LBB18_68
# BB#30:                                # %min.iters.checked
	movq	%rsi, %rax
	andq	$-8, %rax
	movq	%rsi, %r8
	andq	$-8, %r8
	je	.LBB18_68
# BB#31:                                # %vector.body.preheader
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm4        # xmm4 = xmm0[0,0,0,0]
	leaq	-8(%r8), %rdi
	movq	%rdi, %rbx
	shrq	$3, %rbx
	btl	$3, %edi
	jb	.LBB18_109
# BB#32:                                # %vector.body.prol
	movq	16(%rsp), %rdi          # 8-byte Reload
	movdqu	4(%rdi), %xmm1
	movdqu	20(%rdi), %xmm6
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm6, %xmm3
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm1, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm4, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm6, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm4, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm4, %xmm5
	pcmpgtd	%xmm1, %xmm5
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	pand	%xmm5, %xmm1
	pandn	%xmm4, %xmm5
	por	%xmm1, %xmm5
	pand	%xmm0, %xmm6
	pandn	%xmm4, %xmm0
	por	%xmm6, %xmm0
	movl	$8, %ebp
	movdqa	%xmm5, %xmm4
	testq	%rbx, %rbx
	jne	.LBB18_110
	jmp	.LBB18_112
.LBB18_33:
	movl	$516, %edi              # imm = 0x204
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, %r14
	addq	$258, %r14              # imm = 0x102
	movq	32(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB18_34:                              # %.split.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-256(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	32(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	%xmm0, %xmm0
	xorps	.LCPI18_1(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	exp
	mulsd	.LCPI18_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvttss2si	%xmm0, %eax
	movb	%al, 2(%rbx,%rbp)
	incq	%rbp
	cmpq	$513, %rbp              # imm = 0x201
	jne	.LBB18_34
# BB#35:                                # %setup_brightness_lut.exit
	movq	56(%rsp), %r12
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movl	52(%rsp), %r13d
	movl	48(%rsp), %ebx
	movl	16(%rsp), %edi          # 4-byte Reload
	movq	%r12, %rsi
	movl	%r13d, %edx
	movl	%ebx, %ecx
	movq	%r14, %r8
	callq	susan_smoothing
	jmp	.LBB18_97
.LBB18_36:
	movl	52(%rsp), %eax
	movslq	%eax, %r13
	movl	48(%rsp), %r14d
	movl	%r14d, 8(%rsp)          # 4-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	imull	%eax, %r14d
	movslq	%r14d, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	(,%rax,4), %rdi
	callq	malloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$516, %edi              # imm = 0x204
	callq	malloc
	movq	%rax, %rbp
	addq	$258, %rax              # imm = 0x102
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB18_37:                              # %.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-256(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	32(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm1
	xorps	.LCPI18_1(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm0
	callq	exp
	mulsd	.LCPI18_2(%rip), %xmm0
	cvtsd2ss	%xmm0, %xmm0
	cvttss2si	%xmm0, %eax
	movb	%al, 2(%rbp,%rbx)
	incq	%rbx
	cmpq	$513, %rbx              # imm = 0x201
	jne	.LBB18_37
# BB#38:                                # %setup_brightness_lut.exit99
	testl	%r12d, %r12d
	je	.LBB18_44
# BB#39:
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %r12
	je	.LBB18_46
# BB#40:
	movq	%r12, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %r8d
	movl	8(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %r9d
	callq	susan_principle_small
	testl	%r14d, %r14d
	jg	.LBB18_47
	jmp	.LBB18_97
.LBB18_41:                              # %._crit_edge.int_to_uchar.exit_crit_edge
	movq	56(%rsp), %r12
	movl	52(%rsp), %r13d
	movl	48(%rsp), %ebx
	jmp	.LBB18_97
.LBB18_42:
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %r12
	movl	8(%rsp), %ebx           # 4-byte Reload
	je	.LBB18_52
# BB#43:
	movl	%ebx, (%rsp)
	leaq	144(%rsp), %r8
	movl	$1850, %ecx             # imm = 0x73A
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdi
	movq	%r14, %rdx
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	susan_corners_quick
	cmpl	$7, 152(%rsp)
	jne	.LBB18_53
	jmp	.LBB18_96
.LBB18_44:
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	$100, %esi
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	callq	memset
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %r12
	je	.LBB18_56
# BB#45:
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, (%rsp)
	movq	%r12, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	susan_edges_small
	jmp	.LBB18_57
.LBB18_46:
	movl	$2650, %ecx             # imm = 0xA5A
	movq	%r12, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movl	12(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %r8d
	movl	8(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %r9d
	callq	susan_principle
	testl	%r14d, %r14d
	jle	.LBB18_97
.LBB18_47:                              # %.lr.ph40.preheader.i
	cmpl	$1, %r14d
	je	.LBB18_97
# BB#48:                                # %.lr.ph40..lr.ph40_crit_edge.i.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movl	%r14d, %ecx
	leaq	-1(%rcx), %rsi
	cmpq	$8, %rsi
	jb	.LBB18_73
# BB#49:                                # %min.iters.checked248
	movq	%rsi, %rax
	andq	$-8, %rax
	movq	%rsi, %r8
	andq	$-8, %r8
	je	.LBB18_73
# BB#50:                                # %vector.body244.preheader
	movd	%edx, %xmm0
	pshufd	$0, %xmm0, %xmm4        # xmm4 = xmm0[0,0,0,0]
	leaq	-8(%r8), %rdi
	movq	%rdi, %rbx
	shrq	$3, %rbx
	btl	$3, %edi
	jb	.LBB18_114
# BB#51:                                # %vector.body244.prol
	movq	24(%rsp), %rdi          # 8-byte Reload
	movdqu	4(%rdi), %xmm1
	movdqu	20(%rdi), %xmm6
	movdqa	%xmm1, %xmm2
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm6, %xmm3
	pcmpgtd	%xmm4, %xmm3
	movdqa	%xmm1, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm4, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm6, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm4, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm4, %xmm5
	pcmpgtd	%xmm1, %xmm5
	movdqa	%xmm4, %xmm0
	pcmpgtd	%xmm6, %xmm0
	pand	%xmm5, %xmm1
	pandn	%xmm4, %xmm5
	por	%xmm1, %xmm5
	pand	%xmm0, %xmm6
	pandn	%xmm4, %xmm0
	por	%xmm6, %xmm0
	movl	$8, %ebp
	movdqa	%xmm5, %xmm4
	testq	%rbx, %rbx
	jne	.LBB18_115
	jmp	.LBB18_117
.LBB18_52:
	movl	%ebx, (%rsp)
	leaq	144(%rsp), %r8
	movl	$1850, %ecx             # imm = 0x73A
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdi
	movq	%r14, %rdx
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	susan_corners
	cmpl	$7, 152(%rsp)
	je	.LBB18_96
.LBB18_53:                              # %.lr.ph.i134
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	je	.LBB18_94
# BB#54:                                # %.lr.ph.split.i.preheader
	leaq	144(%rsp), %rax
	.p2align	4, 0x90
.LBB18_55:                              # %.lr.ph.split.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	4(%rax), %rcx
	imulq	%r13, %rcx
	addq	%r12, %rcx
	movslq	(%rax), %rdx
	movb	$0, (%rdx,%rcx)
	cmpl	$7, 32(%rax)
	leaq	24(%rax), %rax
	jne	.LBB18_55
	jmp	.LBB18_96
.LBB18_56:
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, (%rsp)
	movl	$2650, %r8d             # imm = 0xA5A
	movq	%r12, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	callq	susan_edges
.LBB18_57:
	movl	44(%rsp), %ebx          # 4-byte Reload
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	je	.LBB18_59
# BB#58:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	8(%rsp), %ecx           # 4-byte Reload
	callq	susan_thin
.LBB18_59:
	testl	%ebx, %ebx
	jne	.LBB18_65
# BB#60:                                # %.preheader42.i
	testl	%r14d, %r14d
	jle	.LBB18_98
# BB#61:                                # %.lr.ph47.i
	movq	%r13, %rax
	negq	%rax
	movq	%r13, %rcx
	addq	$-2, %rcx
	movl	%r14d, %edx
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB18_62:                              # =>This Inner Loop Header: Depth=1
	cmpb	$7, (%rsi)
	ja	.LBB18_64
# BB#63:                                #   in Loop: Header=BB18_62 Depth=1
	movq	%rsi, %rdi
	subq	%rbp, %rdi
	addq	%r12, %rdi
	movb	$-1, -1(%rax,%rdi)
	movb	$-1, (%rax,%rdi)
	movb	$-1, 1(%rax,%rdi)
	leaq	(%rdi,%rax), %rdi
	leaq	1(%rcx,%rdi), %rbx
	movb	$-1, -1(%r13,%rdi)
	movb	$-1, 1(%r13,%rdi)
	movb	$-1, (%r13,%rbx)
	movb	$-1, 1(%r13,%rbx)
	movb	$-1, 2(%r13,%rbx)
.LBB18_64:                              #   in Loop: Header=BB18_62 Depth=1
	incq	%rsi
	decl	%edx
	jne	.LBB18_62
.LBB18_65:                              # %.preheader.i
	testl	%r14d, %r14d
	movl	12(%rsp), %r13d         # 4-byte Reload
	jle	.LBB18_78
# BB#66:                                # %.lr.ph.i103
	xorl	%ecx, %ecx
	testb	$1, %r14b
	jne	.LBB18_79
# BB#67:
	movq	%rbp, %rax
	cmpl	$1, %r14d
	je	.LBB18_82
	jmp	.LBB18_83
.LBB18_68:
	movl	$1, %eax
	movl	%edx, %ebx
	movl	%edx, %ebp
.LBB18_69:                              # %.lr.ph40..lr.ph40_crit_edge.i122.preheader296
	movq	%rcx, %rsi
	subq	%rax, %rsi
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax,4), %rax
	.p2align	4, 0x90
.LBB18_70:                              # %.lr.ph40..lr.ph40_crit_edge.i122
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edi
	cmpl	%ebp, %edi
	cmovgel	%edi, %ebp
	cmpl	%ebx, %edi
	cmovlel	%edi, %ebx
	addq	$4, %rax
	decq	%rsi
	jne	.LBB18_70
.LBB18_71:                              # %.lr.ph.preheader.i125
	subl	%ebx, %ebp
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	cmpl	$1, %r9d
	movb	%al, (%r12)
	jne	.LBB18_90
# BB#72:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_73:
	movl	$1, %eax
	movl	%edx, %ebx
	movl	%edx, %ebp
.LBB18_74:                              # %.lr.ph40..lr.ph40_crit_edge.i.preheader292
	movq	%rcx, %rsi
	subq	%rax, %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax,4), %rax
	.p2align	4, 0x90
.LBB18_75:                              # %.lr.ph40..lr.ph40_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edi
	cmpl	%ebp, %edi
	cmovgel	%edi, %ebp
	cmpl	%ebx, %edi
	cmovlel	%edi, %ebx
	addq	$4, %rax
	decq	%rsi
	jne	.LBB18_75
.LBB18_76:                              # %.lr.ph.preheader.i
	subl	%ebx, %ebp
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	cmpl	$1, %r14d
	movb	%al, (%r12)
	jne	.LBB18_92
.LBB18_78:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_79:
	cmpb	$7, (%rbp)
	ja	.LBB18_81
# BB#80:
	movb	$0, (%r12)
.LBB18_81:
	leaq	1(%rbp), %rax
	movl	$1, %ecx
	cmpl	$1, %r14d
	jne	.LBB18_83
.LBB18_82:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_83:                              # %.lr.ph.i103.new
	incq	%rax
	subl	%ecx, %r14d
	.p2align	4, 0x90
.LBB18_84:                              # =>This Inner Loop Header: Depth=1
	cmpb	$7, -1(%rax)
	ja	.LBB18_86
# BB#85:                                #   in Loop: Header=BB18_84 Depth=1
	leaq	-1(%rax), %rcx
	subq	%rbp, %rcx
	movb	$0, (%r12,%rcx)
.LBB18_86:                              #   in Loop: Header=BB18_84 Depth=1
	cmpb	$8, (%rax)
	jae	.LBB18_88
# BB#87:                                #   in Loop: Header=BB18_84 Depth=1
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movb	$0, (%r12,%rcx)
.LBB18_88:                              #   in Loop: Header=BB18_84 Depth=1
	addq	$2, %rax
	addl	$-2, %r14d
	jne	.LBB18_84
# BB#89:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_90:                              # %.lr.ph..lr.ph_crit_edge.i132.preheader
	testb	$1, %cl
	jne	.LBB18_99
# BB#91:                                # %.lr.ph..lr.ph_crit_edge.i132.prol
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	movb	%al, 1(%r12)
	movl	$2, %eax
	cmpl	$2, %r9d
	je	.LBB18_100
	jmp	.LBB18_103
.LBB18_92:                              # %.lr.ph..lr.ph_crit_edge.i.preheader
	testb	$1, %cl
	jne	.LBB18_101
# BB#93:                                # %.lr.ph..lr.ph_crit_edge.i.prol
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	movb	%al, 1(%r12)
	movl	$2, %eax
	cmpl	$2, %r14d
	je	.LBB18_102
	jmp	.LBB18_106
.LBB18_94:                              # %.lr.ph.split.us.i.preheader
	leaq	-2(%r13), %rax
	leaq	144(%rsp), %rcx
	.p2align	4, 0x90
.LBB18_95:                              # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	4(%rcx), %rdx
	decq	%rdx
	imulq	%r13, %rdx
	addq	%r12, %rdx
	movslq	(%rcx), %rsi
	leaq	(%rdx,%rsi), %rdi
	movb	$-1, -1(%rsi,%rdx)
	movb	$-1, (%rsi,%rdx)
	movb	$-1, 1(%rsi,%rdx)
	leaq	1(%rax,%rdi), %rdx
	movb	$-1, -1(%r13,%rdi)
	movb	$0, (%r13,%rdi)
	movb	$-1, 1(%r13,%rdi)
	movb	$-1, (%r13,%rdx)
	movb	$-1, 1(%r13,%rdx)
	movb	$-1, 2(%r13,%rdx)
	cmpl	$7, 32(%rcx)
	leaq	24(%rcx), %rcx
	jne	.LBB18_95
.LBB18_96:
	movl	12(%rsp), %r13d         # 4-byte Reload
.LBB18_97:                              # %int_to_uchar.exit
	movq	16(%r15), %rdi
	movq	%r12, %rsi
	movl	%r13d, %edx
	movl	%ebx, %ecx
	callq	put_image
	xorl	%eax, %eax
	addq	$360152, %rsp           # imm = 0x57ED8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_98:
	movl	12(%rsp), %r13d         # 4-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_99:
	movl	$1, %eax
	cmpl	$2, %r9d
	jne	.LBB18_103
.LBB18_100:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_101:
	movl	$1, %eax
	cmpl	$2, %r14d
	jne	.LBB18_106
.LBB18_102:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_103:                             # %.lr.ph..lr.ph_crit_edge.i132.preheader.new
	subq	%rax, %rcx
	leaq	1(%r12,%rax), %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rax,4), %rdi
	.p2align	4, 0x90
.LBB18_104:                             # %.lr.ph..lr.ph_crit_edge.i132
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	movb	%al, -1(%rsi)
	movl	(%rdi), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	movb	%al, (%rsi)
	addq	$2, %rsi
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB18_104
# BB#105:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_106:                             # %.lr.ph..lr.ph_crit_edge.i.preheader.new
	subq	%rax, %rcx
	leaq	1(%r12,%rax), %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	leaq	4(%rdx,%rax,4), %rdi
	.p2align	4, 0x90
.LBB18_107:                             # %.lr.ph..lr.ph_crit_edge.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	movb	%al, -1(%rsi)
	movl	(%rdi), %edx
	subl	%ebx, %edx
	movl	%edx, %eax
	shll	$8, %eax
	subl	%edx, %eax
	cltd
	idivl	%ebp
	movb	%al, (%rsi)
	addq	$2, %rsi
	addq	$8, %rdi
	addq	$-2, %rcx
	jne	.LBB18_107
# BB#108:
	movl	8(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB18_97
.LBB18_109:
	xorl	%ebp, %ebp
                                        # implicit-def: %XMM5
	movdqa	%xmm4, %xmm0
	movdqa	%xmm4, %xmm2
	movdqa	%xmm4, %xmm3
	testq	%rbx, %rbx
	je	.LBB18_112
.LBB18_110:                             # %vector.body.preheader.new
	movq	%r8, %rdi
	subq	%rbp, %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	leaq	52(%rbx,%rbp,4), %rbp
	movdqa	%xmm4, %xmm5
	.p2align	4, 0x90
.LBB18_111:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rbp), %xmm11
	movdqu	-32(%rbp), %xmm10
	movdqu	-16(%rbp), %xmm9
	movdqu	(%rbp), %xmm8
	movdqa	%xmm11, %xmm4
	pcmpgtd	%xmm2, %xmm4
	movdqa	%xmm10, %xmm6
	pcmpgtd	%xmm3, %xmm6
	movdqa	%xmm11, %xmm1
	pand	%xmm4, %xmm1
	pandn	%xmm2, %xmm4
	por	%xmm1, %xmm4
	movdqa	%xmm10, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm3, %xmm6
	por	%xmm1, %xmm6
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm11, %xmm1
	movdqa	%xmm0, %xmm7
	pcmpgtd	%xmm10, %xmm7
	pand	%xmm1, %xmm11
	pandn	%xmm5, %xmm1
	por	%xmm11, %xmm1
	pand	%xmm7, %xmm10
	pandn	%xmm0, %xmm7
	por	%xmm10, %xmm7
	movdqa	%xmm9, %xmm2
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm8, %xmm3
	pcmpgtd	%xmm6, %xmm3
	movdqa	%xmm9, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm4, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm8, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm6, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm1, %xmm5
	pcmpgtd	%xmm9, %xmm5
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm8, %xmm0
	pand	%xmm5, %xmm9
	pandn	%xmm1, %xmm5
	por	%xmm9, %xmm5
	pand	%xmm0, %xmm8
	pandn	%xmm7, %xmm0
	por	%xmm8, %xmm0
	addq	$64, %rbp
	addq	$-16, %rdi
	jne	.LBB18_111
.LBB18_112:                             # %middle.block
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	por	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm1
	pandn	%xmm2, %xmm3
	por	%xmm1, %xmm3
	pshufd	$229, %xmm3, %xmm1      # xmm1 = xmm3[1,1,2,3]
	movd	%xmm3, %edi
	pcmpgtd	%xmm1, %xmm3
	movdqa	%xmm3, 112(%rsp)
	testb	$1, 112(%rsp)
	movd	%xmm1, %ebp
	cmovnel	%edi, %ebp
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm5, %xmm1
	pand	%xmm1, %xmm5
	pandn	%xmm0, %xmm1
	por	%xmm5, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movd	%xmm0, %edi
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm0, 128(%rsp)
	movd	%xmm2, %ebx
	testb	$1, 128(%rsp)
	cmovel	%edi, %ebx
	cmpq	%r8, %rsi
	je	.LBB18_71
# BB#113:
	orq	$1, %rax
	jmp	.LBB18_69
.LBB18_114:
	xorl	%ebp, %ebp
                                        # implicit-def: %XMM5
	movdqa	%xmm4, %xmm0
	movdqa	%xmm4, %xmm2
	movdqa	%xmm4, %xmm3
	testq	%rbx, %rbx
	je	.LBB18_117
.LBB18_115:                             # %vector.body244.preheader.new
	movq	%r8, %rdi
	subq	%rbp, %rdi
	movq	24(%rsp), %rbx          # 8-byte Reload
	leaq	52(%rbx,%rbp,4), %rbp
	movdqa	%xmm4, %xmm5
	.p2align	4, 0x90
.LBB18_116:                             # %vector.body244
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-48(%rbp), %xmm11
	movdqu	-32(%rbp), %xmm10
	movdqu	-16(%rbp), %xmm9
	movdqu	(%rbp), %xmm8
	movdqa	%xmm11, %xmm4
	pcmpgtd	%xmm2, %xmm4
	movdqa	%xmm10, %xmm6
	pcmpgtd	%xmm3, %xmm6
	movdqa	%xmm11, %xmm1
	pand	%xmm4, %xmm1
	pandn	%xmm2, %xmm4
	por	%xmm1, %xmm4
	movdqa	%xmm10, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm3, %xmm6
	por	%xmm1, %xmm6
	movdqa	%xmm5, %xmm1
	pcmpgtd	%xmm11, %xmm1
	movdqa	%xmm0, %xmm7
	pcmpgtd	%xmm10, %xmm7
	pand	%xmm1, %xmm11
	pandn	%xmm5, %xmm1
	por	%xmm11, %xmm1
	pand	%xmm7, %xmm10
	pandn	%xmm0, %xmm7
	por	%xmm10, %xmm7
	movdqa	%xmm9, %xmm2
	pcmpgtd	%xmm4, %xmm2
	movdqa	%xmm8, %xmm3
	pcmpgtd	%xmm6, %xmm3
	movdqa	%xmm9, %xmm0
	pand	%xmm2, %xmm0
	pandn	%xmm4, %xmm2
	por	%xmm0, %xmm2
	movdqa	%xmm8, %xmm0
	pand	%xmm3, %xmm0
	pandn	%xmm6, %xmm3
	por	%xmm0, %xmm3
	movdqa	%xmm1, %xmm5
	pcmpgtd	%xmm9, %xmm5
	movdqa	%xmm7, %xmm0
	pcmpgtd	%xmm8, %xmm0
	pand	%xmm5, %xmm9
	pandn	%xmm1, %xmm5
	por	%xmm9, %xmm5
	pand	%xmm0, %xmm8
	pandn	%xmm7, %xmm0
	por	%xmm8, %xmm0
	addq	$64, %rbp
	addq	$-16, %rdi
	jne	.LBB18_116
.LBB18_117:                             # %middle.block245
	movdqa	%xmm2, %xmm1
	pcmpgtd	%xmm3, %xmm1
	pand	%xmm1, %xmm2
	pandn	%xmm3, %xmm1
	por	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	movdqa	%xmm1, %xmm3
	pcmpgtd	%xmm2, %xmm3
	pand	%xmm3, %xmm1
	pandn	%xmm2, %xmm3
	por	%xmm1, %xmm3
	pshufd	$229, %xmm3, %xmm1      # xmm1 = xmm3[1,1,2,3]
	movd	%xmm3, %edi
	pcmpgtd	%xmm1, %xmm3
	movdqa	%xmm3, 80(%rsp)
	testb	$1, 80(%rsp)
	movd	%xmm1, %ebp
	cmovnel	%edi, %ebp
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm5, %xmm1
	pand	%xmm1, %xmm5
	pandn	%xmm0, %xmm1
	por	%xmm5, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	movdqa	%xmm0, %xmm2
	pcmpgtd	%xmm1, %xmm2
	pand	%xmm2, %xmm1
	pandn	%xmm0, %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movd	%xmm0, %edi
	pcmpgtd	%xmm2, %xmm0
	movdqa	%xmm0, 96(%rsp)
	movd	%xmm2, %ebx
	testb	$1, 96(%rsp)
	cmovel	%edi, %ebx
	cmpq	%r8, %rsi
	je	.LBB18_76
# BB#118:
	orq	$1, %rax
	jmp	.LBB18_74
.LBB18_119:
	callq	usage
.LBB18_120:
	movl	$.Lstr.16, %edi
	jmp	.LBB18_122
.LBB18_121:
	movl	$.Lstr.15, %edi
.LBB18_122:
	callq	puts
	xorl	%edi, %edi
	callq	exit
.LBB18_123:
	callq	usage
.Lfunc_end18:
	.size	main, .Lfunc_end18-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI18_0:
	.quad	.LBB18_7
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_8
	.quad	.LBB18_9
	.quad	.LBB18_10
	.quad	.LBB18_13
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_20
	.quad	.LBB18_14
	.quad	.LBB18_20
	.quad	.LBB18_15
	.quad	.LBB18_16
	.quad	.LBB18_20
	.quad	.LBB18_17
	.quad	.LBB18_18

	.type	.L.str.13,@object       # @.str.13
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.13:
	.asciz	"Image %s not binary PGM.\n"
	.size	.L.str.13, 26

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"is"
	.size	.L.str.14, 3

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"r"
	.size	.L.str.15, 2

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Can't input image %s.\n"
	.size	.L.str.16, 23

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Image %s does not have binary PGM header.\n"
	.size	.L.str.17, 43

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"Image %s is wrong size.\n"
	.size	.L.str.18, 25

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"w"
	.size	.L.str.19, 2

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Can't output image%s.\n"
	.size	.L.str.20, 23

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"P5\n"
	.size	.L.str.21, 4

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"%d %d\n"
	.size	.L.str.22, 7

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"255\n"
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Can't write image %s.\n"
	.size	.L.str.24, 23

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Distance_thresh (%f) too big for integer arithmetic.\n"
	.size	.L.str.25, 54

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Mask size (1.5*distance_thresh+1=%d) too big for image (%dx%d).\n"
	.size	.L.str.28, 65

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"Too many corners.\n"
	.size	.L.str.29, 19

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Usage: susan <in.pgm> <out.pgm> [options]\n"
	.size	.Lstr, 43

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"-s : Smoothing mode (default)"
	.size	.Lstr.1, 30

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.2:
	.asciz	"-e : Edges mode"
	.size	.Lstr.2, 16

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.3:
	.asciz	"-c : Corners mode\n"
	.size	.Lstr.3, 19

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"See source code for more information about setting the thresholds"
	.size	.Lstr.4, 66

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"-t <thresh> : Brightness threshold, all modes (default=20)"
	.size	.Lstr.5, 59

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"-d <thresh> : Distance threshold, smoothing mode, (default=4) (use next option instead for flat 3x3 mask)"
	.size	.Lstr.6, 106

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"-3 : Use flat 3x3 mask, edges or smoothing mode"
	.size	.Lstr.7, 48

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"-n : No post-processing on the binary edge map (runs much faster); edges mode"
	.size	.Lstr.8, 78

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"-q : Use faster (and usually stabler) corner mode; edge-like corner suppression not carried out; corners mode"
	.size	.Lstr.9, 110

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"-b : Mark corners/edges with single black points instead of black with white border; corners or edges mode"
	.size	.Lstr.10, 107

	.type	.Lstr.11,@object        # @str.11
	.p2align	4
.Lstr.11:
	.asciz	"-p : Output initial enhancement image only; corners or edges mode (default is edges mode)"
	.size	.Lstr.11, 90

	.type	.Lstr.12,@object        # @str.12
	.p2align	4
.Lstr.12:
	.asciz	"\nSUSAN Version 2l (C) 1995-1997 Stephen Smith, DRA UK. steve@fmrib.ox.ac.uk"
	.size	.Lstr.12, 76

	.type	.Lstr.13,@object        # @str.13
	.p2align	4
.Lstr.13:
	.asciz	"Either reduce it to <=15 or recompile with variable \"total\""
	.size	.Lstr.13, 60

	.type	.Lstr.14,@object        # @str.14
	.p2align	4
.Lstr.14:
	.asciz	"as a float: see top \"defines\" section."
	.size	.Lstr.14, 39

	.type	.Lstr.15,@object        # @str.15
	.p2align	4
.Lstr.15:
	.asciz	"No argument following -t"
	.size	.Lstr.15, 25

	.type	.Lstr.16,@object        # @str.16
	.p2align	4
.Lstr.16:
	.asciz	"No argument following -d"
	.size	.Lstr.16, 25


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
