	.text
	.file	"project.bc"
	.globl	hypre_ProjectBox
	.p2align	4, 0x90
	.type	hypre_ProjectBox,@function
hypre_ProjectBox:                       # @hypre_ProjectBox
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdx, %r8
	movl	(%rsi), %r11d
	movl	(%r8), %ebx
	movl	(%rdi), %eax
	movl	12(%rdi), %r9d
	leal	-1(%rbx), %edx
	xorl	%r10d, %r10d
	subl	%r11d, %eax
	movl	%edx, %ecx
	cmovlel	%r10d, %ecx
	addl	%eax, %ecx
	subl	%r11d, %r9d
	cmovnsl	%r10d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%r11d, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, (%rdi)
	movl	%r9d, %eax
	cltd
	idivl	%ebx
	subl	%edx, %r11d
	addl	%r9d, %r11d
	movl	%r11d, 12(%rdi)
	movl	4(%rsi), %r11d
	movl	4(%r8), %ebx
	movl	4(%rdi), %eax
	movl	16(%rdi), %r9d
	leal	-1(%rbx), %edx
	subl	%r11d, %eax
	movl	%edx, %ecx
	cmovlel	%r10d, %ecx
	addl	%eax, %ecx
	subl	%r11d, %r9d
	cmovnsl	%r10d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%r11d, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, 4(%rdi)
	movl	%r9d, %eax
	cltd
	idivl	%ebx
	subl	%edx, %r11d
	addl	%r9d, %r11d
	movl	%r11d, 16(%rdi)
	movl	8(%rsi), %ecx
	movl	8(%r8), %ebx
	movl	8(%rdi), %eax
	movl	20(%rdi), %r8d
	leal	-1(%rbx), %edx
	subl	%ecx, %eax
	movl	%edx, %esi
	cmovlel	%r10d, %esi
	addl	%eax, %esi
	subl	%ecx, %r8d
	cmovnsl	%r10d, %edx
	subl	%edx, %r8d
	movl	%esi, %eax
	cltd
	idivl	%ebx
	movl	%ecx, %eax
	subl	%edx, %eax
	addl	%esi, %eax
	movl	%eax, 8(%rdi)
	movl	%r8d, %eax
	cltd
	idivl	%ebx
	subl	%edx, %ecx
	addl	%r8d, %ecx
	movl	%ecx, 20(%rdi)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	hypre_ProjectBox, .Lfunc_end0-hypre_ProjectBox
	.cfi_endproc

	.globl	hypre_ProjectBoxArray
	.p2align	4, 0x90
	.type	hypre_ProjectBoxArray,@function
hypre_ProjectBoxArray:                  # @hypre_ProjectBoxArray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movq	%rdx, %r8
	cmpl	$0, 8(%rdi)
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph
	movq	(%rdi), %r10
	addq	$20, %r10
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %ebp
	movl	(%r8), %ebx
	movl	-20(%r10), %eax
	movl	-8(%r10), %r9d
	leal	-1(%rbx), %edx
	subl	%ebp, %eax
	movl	%edx, %ecx
	cmovlel	%r11d, %ecx
	addl	%eax, %ecx
	subl	%ebp, %r9d
	cmovnsl	%r11d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%ebp, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, -20(%r10)
	movl	%r9d, %eax
	cltd
	idivl	%ebx
	subl	%edx, %ebp
	addl	%r9d, %ebp
	movl	%ebp, -8(%r10)
	movl	4(%rsi), %ebp
	movl	4(%r8), %ebx
	movl	-16(%r10), %eax
	movl	-4(%r10), %r9d
	leal	-1(%rbx), %edx
	subl	%ebp, %eax
	movl	%edx, %ecx
	cmovlel	%r11d, %ecx
	addl	%eax, %ecx
	subl	%ebp, %r9d
	cmovnsl	%r11d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%ebp, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, -16(%r10)
	movl	%r9d, %eax
	cltd
	idivl	%ebx
	subl	%edx, %ebp
	addl	%r9d, %ebp
	movl	%ebp, -4(%r10)
	movl	8(%rsi), %ebp
	movl	8(%r8), %ebx
	movl	-12(%r10), %eax
	movl	(%r10), %r9d
	leal	-1(%rbx), %edx
	subl	%ebp, %eax
	movl	%edx, %ecx
	cmovlel	%r11d, %ecx
	addl	%eax, %ecx
	subl	%ebp, %r9d
	cmovnsl	%r11d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%ebp, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, -12(%r10)
	movl	%r9d, %eax
	cltd
	idivl	%ebx
	subl	%edx, %ebp
	addl	%r9d, %ebp
	movl	%ebp, (%r10)
	incq	%r14
	movslq	8(%rdi), %rax
	addq	$24, %r10
	cmpq	%rax, %r14
	jl	.LBB1_2
.LBB1_3:                                # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_ProjectBoxArray, .Lfunc_end1-hypre_ProjectBoxArray
	.cfi_endproc

	.globl	hypre_ProjectBoxArrayArray
	.p2align	4, 0x90
	.type	hypre_ProjectBoxArrayArray,@function
hypre_ProjectBoxArrayArray:             # @hypre_ProjectBoxArrayArray
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdx, %r8
	movl	8(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB2_7
# BB#1:                                 # %.lr.ph26
	movq	(%rdi), %r10
	xorl	%r11d, %r11d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	movq	(%r10,%r14,8), %r15
	cmpl	$0, 8(%r15)
	jle	.LBB2_6
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	(%r15), %rbx
	addq	$20, %rbx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi), %ebp
	movl	(%r8), %r13d
	movl	-20(%rbx), %eax
	movl	-8(%rbx), %r9d
	leal	-1(%r13), %edx
	subl	%ebp, %eax
	movl	%edx, %ecx
	cmovlel	%r11d, %ecx
	addl	%eax, %ecx
	subl	%ebp, %r9d
	cmovnsl	%r11d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%r13d
	movl	%ebp, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, -20(%rbx)
	movl	%r9d, %eax
	cltd
	idivl	%r13d
	subl	%edx, %ebp
	addl	%r9d, %ebp
	movl	%ebp, -8(%rbx)
	movl	4(%rsi), %ebp
	movl	4(%r8), %r13d
	movl	-16(%rbx), %eax
	movl	-4(%rbx), %r9d
	leal	-1(%r13), %edx
	subl	%ebp, %eax
	movl	%edx, %ecx
	cmovlel	%r11d, %ecx
	addl	%eax, %ecx
	subl	%ebp, %r9d
	cmovnsl	%r11d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%r13d
	movl	%ebp, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, -16(%rbx)
	movl	%r9d, %eax
	cltd
	idivl	%r13d
	subl	%edx, %ebp
	addl	%r9d, %ebp
	movl	%ebp, -4(%rbx)
	movl	8(%rsi), %ebp
	movl	8(%r8), %r13d
	movl	-12(%rbx), %eax
	movl	(%rbx), %r9d
	leal	-1(%r13), %edx
	subl	%ebp, %eax
	movl	%edx, %ecx
	cmovlel	%r11d, %ecx
	addl	%eax, %ecx
	subl	%ebp, %r9d
	cmovnsl	%r11d, %edx
	subl	%edx, %r9d
	movl	%ecx, %eax
	cltd
	idivl	%r13d
	movl	%ebp, %eax
	subl	%edx, %eax
	addl	%ecx, %eax
	movl	%eax, -12(%rbx)
	movl	%r9d, %eax
	cltd
	idivl	%r13d
	subl	%edx, %ebp
	addl	%r9d, %ebp
	movl	%ebp, (%rbx)
	incq	%r12
	movslq	8(%r15), %rax
	addq	$24, %rbx
	cmpq	%rax, %r12
	jl	.LBB2_4
# BB#5:                                 # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	8(%rdi), %eax
.LBB2_6:                                # %._crit_edge
                                        #   in Loop: Header=BB2_2 Depth=1
	incq	%r14
	movslq	%eax, %rcx
	cmpq	%rcx, %r14
	jl	.LBB2_2
.LBB2_7:                                # %._crit_edge27
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_ProjectBoxArrayArray, .Lfunc_end2-hypre_ProjectBoxArrayArray
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
