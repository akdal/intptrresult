	.text
	.file	"btBoxBoxDetector.bc"
	.globl	_ZN16btBoxBoxDetectorC2EP10btBoxShapeS1_
	.p2align	4, 0x90
	.type	_ZN16btBoxBoxDetectorC2EP10btBoxShapeS1_,@function
_ZN16btBoxBoxDetectorC2EP10btBoxShapeS1_: # @_ZN16btBoxBoxDetectorC2EP10btBoxShapeS1_
	.cfi_startproc
# BB#0:
	movq	$_ZTV16btBoxBoxDetector+16, (%rdi)
	movq	%rsi, 8(%rdi)
	movq	%rdx, 16(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN16btBoxBoxDetectorC2EP10btBoxShapeS1_, .Lfunc_end0-_ZN16btBoxBoxDetectorC2EP10btBoxShapeS1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_0:
	.long	1065353216              # float 1
.LCPI1_1:
	.long	953267991               # float 9.99999974E-5
	.text
	.globl	_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_
	.p2align	4, 0x90
	.type	_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_,@function
_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_: # @_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_
	.cfi_startproc
# BB#0:
	movss	(%rsi), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	mulss	%xmm10, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm1
	mulss	%xmm8, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm1, %xmm4
	mulss	%xmm4, %xmm4
	movss	.LCPI1_0(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	subss	%xmm4, %xmm2
	movss	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm4
	jae	.LBB1_1
# BB#2:
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	(%rdi), %xmm5
	subss	4(%rdi), %xmm3
	movss	8(%rdx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	8(%rdi), %xmm4
	mulss	%xmm5, %xmm7
	mulss	%xmm3, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm4, %xmm0
	addss	%xmm6, %xmm0
	mulss	%xmm10, %xmm5
	mulss	%xmm9, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	divss	%xmm2, %xmm11
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm0
	subss	%xmm4, %xmm0
	mulss	%xmm1, %xmm4
	subss	%xmm4, %xmm2
	mulss	%xmm11, %xmm2
	movss	%xmm2, (%r8)
	mulss	%xmm11, %xmm0
	movss	%xmm0, (%r9)
	retq
.LBB1_1:
	movl	$0, (%r8)
	xorps	%xmm0, %xmm0
	movss	%xmm0, (%r9)
	retq
.Lfunc_end1:
	.size	_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_, .Lfunc_end1-_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI2_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI2_6:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_2:
	.long	1566444395              # float 9.99999984E+17
.LCPI2_3:
	.long	872415232               # float 1.1920929E-7
.LCPI2_4:
	.long	1077936128              # float 3
.LCPI2_5:
	.long	1065353216              # float 1
.LCPI2_7:
	.long	1086918619              # float 6.28318548
.LCPI2_8:
	.long	3234402267              # float -6.28318548
.LCPI2_9:
	.long	1078530011              # float 3.14159274
.LCPI2_10:
	.long	1315859240              # float 1.0E+9
	.text
	.globl	_Z11cullPoints2iPfiiPi
	.p2align	4, 0x90
	.type	_Z11cullPoints2iPfiiPi,@function
_Z11cullPoints2iPfiiPi:                 # @_Z11cullPoints2iPfiiPi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movl	%edx, %r14d
	movq	%rsi, %rbp
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	cmpl	$1, %edi
	je	.LBB2_3
# BB#1:
	cmpl	$2, %edi
	jne	.LBB2_4
# BB#2:
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movsd	4(%rbp), %xmm0          # xmm0 = mem[0],zero
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	12(%rbp), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1]
	addps	%xmm0, %xmm7
	mulps	.LCPI2_0(%rip), %xmm7
	jmp	.LBB2_12
.LBB2_3:
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	jmp	.LBB2_12
.LBB2_4:                                # %.preheader
	jl	.LBB2_7
# BB#5:                                 # %.lr.ph143.preheader
	leal	-1(%rdi), %eax
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph143
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rdx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	12(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	mulss	%xmm3, %xmm4
	movq	4(%rdx), %xmm5          # xmm5 = mem[0],zero
	pshufd	$229, %xmm5, %xmm6      # xmm6 = xmm5[1,1,2,3]
	mulss	%xmm5, %xmm6
	subss	%xmm6, %xmm4
	addss	%xmm4, %xmm1
	unpcklps	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	addps	%xmm5, %xmm3
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm4, %xmm0
	addq	$8, %rdx
	decq	%rax
	jne	.LBB2_6
	jmp	.LBB2_8
.LBB2_7:
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
.LBB2_8:                                # %._crit_edge144
	leal	(%rdi,%rdi), %eax
	cltq
	movss	-8(%rbp,%rax,4), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rbp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	mulss	%xmm3, %xmm2
	movss	-4(%rbp,%rax,4), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	mulss	%xmm6, %xmm7
	subss	%xmm7, %xmm2
	addss	%xmm2, %xmm1
	movaps	.LCPI2_1(%rip), %xmm7   # xmm7 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm7
	ucomiss	.LCPI2_3(%rip), %xmm7
	jbe	.LBB2_10
# BB#9:
	mulss	.LCPI2_4(%rip), %xmm1
	movss	.LCPI2_5(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm7
	testl	%edi, %edi
	jg	.LBB2_11
	jmp	.LBB2_37
.LBB2_10:
	movss	.LCPI2_2(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	testl	%edi, %edi
	jle	.LBB2_37
.LBB2_11:
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	addps	%xmm6, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm2, %xmm0
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm0, %xmm7
.LBB2_12:                               # %.lr.ph137.preheader
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movl	%edi, %r12d
	movaps	%xmm7, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	addq	$4, %rbp
	leaq	96(%rsp), %r15
	movq	%r12, %rbx
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph137
                                        # =>This Inner Loop Header: Depth=1
	movss	-4(%rbp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	subss	64(%rsp), %xmm1         # 16-byte Folded Reload
	callq	atan2f
	movaps	80(%rsp), %xmm7         # 16-byte Reload
	movss	%xmm0, (%r15)
	addq	$4, %r15
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB2_13
# BB#14:                                # %._crit_edge138
	movq	56(%rsp), %rdi          # 8-byte Reload
	cmpl	$8, %edi
	jb	.LBB2_20
# BB#16:                                # %min.iters.checked
	movl	%edi, %ecx
	andl	$7, %ecx
	movq	%r12, %rax
	subq	%rcx, %rax
	je	.LBB2_20
# BB#17:                                # %vector.body.preheader
	leaq	32(%rsp), %rdx
	movaps	.LCPI2_6(%rip), %xmm0   # xmm0 = [1,1,1,1]
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB2_18:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, -16(%rdx)
	movaps	%xmm0, (%rdx)
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB2_18
# BB#19:                                # %middle.block
	testl	%ecx, %ecx
	jne	.LBB2_21
	jmp	.LBB2_23
.LBB2_20:
	xorl	%eax, %eax
.LBB2_21:                               # %.lr.ph133.preheader
	leaq	16(%rsp,%rax,4), %rcx
	subq	%rax, %r12
	.p2align	4, 0x90
.LBB2_22:                               # %.lr.ph133
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, (%rcx)
	addq	$4, %rcx
	decq	%r12
	jne	.LBB2_22
.LBB2_23:
	movb	$1, %dl
	movl	12(%rsp), %ecx          # 4-byte Reload
.LBB2_24:                               # %._crit_edge134
	movslq	%ecx, %rax
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, (%r13)
	cmpl	$2, %r14d
	jl	.LBB2_41
# BB#25:                                # %.lr.ph129
	addq	$4, %r13
	testb	%dl, %dl
	je	.LBB2_33
# BB#26:                                # %.lr.ph129.split.us.preheader
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%r14d, %xmm2
	movss	.LCPI2_7(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm11
	divss	%xmm2, %xmm11
	movss	96(%rsp,%rax,4), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movl	%edi, %eax
	movl	$1, %edi
	movss	.LCPI2_8(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI2_9(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	.LCPI2_10(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movaps	.LCPI2_1(%rip), %xmm6   # xmm6 = [nan,nan,nan,nan]
	.p2align	4, 0x90
.LBB2_27:                               # %.lr.ph129.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_28 Depth 2
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%edi, %xmm3
	mulss	%xmm11, %xmm3
	addss	%xmm10, %xmm3
	movaps	%xmm3, %xmm4
	addss	%xmm8, %xmm4
	movaps	%xmm5, %xmm7
	cmpltss	%xmm3, %xmm7
	andps	%xmm7, %xmm4
	andnps	%xmm3, %xmm7
	orps	%xmm4, %xmm7
	movl	%ecx, (%r13)
	movl	%ecx, %esi
	xorl	%edx, %edx
	movaps	%xmm9, %xmm3
	.p2align	4, 0x90
.LBB2_28:                               #   Parent Loop BB2_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$0, 16(%rsp,%rdx,4)
	je	.LBB2_31
# BB#29:                                #   in Loop: Header=BB2_28 Depth=2
	movss	96(%rsp,%rdx,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm2
	andps	%xmm6, %xmm2
	movaps	%xmm1, %xmm0
	subss	%xmm2, %xmm0
	movaps	%xmm5, %xmm4
	cmpltss	%xmm2, %xmm4
	andps	%xmm4, %xmm0
	andnps	%xmm2, %xmm4
	orps	%xmm0, %xmm4
	ucomiss	%xmm4, %xmm3
	jbe	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_28 Depth=2
	movl	%edx, (%r13)
	movl	%edx, %esi
	movaps	%xmm4, %xmm3
.LBB2_31:                               #   in Loop: Header=BB2_28 Depth=2
	incq	%rdx
	cmpq	%rdx, %rax
	jne	.LBB2_28
# BB#32:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_27 Depth=1
	movslq	%esi, %rdx
	movl	$0, 16(%rsp,%rdx,4)
	incl	%edi
	addq	$4, %r13
	cmpl	%r14d, %edi
	jne	.LBB2_27
	jmp	.LBB2_41
.LBB2_33:                               # %.lr.ph129.split.preheader
	leal	7(%r14), %esi
	leal	-2(%r14), %ecx
	andl	$7, %esi
	je	.LBB2_38
# BB#34:                                # %.lr.ph129.split.prol.preheader
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_35:                               # %.lr.ph129.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%r13)
	movl	$0, 16(%rsp,%rax,4)
	addq	$4, %r13
	incl	%edx
	cmpl	%edx, %esi
	jne	.LBB2_35
# BB#36:                                # %.lr.ph129.split.prol.loopexit.unr-lcssa
	incl	%edx
	cmpl	$7, %ecx
	jae	.LBB2_39
	jmp	.LBB2_41
.LBB2_37:                               # %._crit_edge138.thread
	xorl	%edx, %edx
	jmp	.LBB2_24
.LBB2_38:
	movl	$1, %edx
	cmpl	$7, %ecx
	jb	.LBB2_41
.LBB2_39:                               # %.lr.ph129.split.preheader.new
	subl	%edx, %r14d
	.p2align	4, 0x90
.LBB2_40:                               # %.lr.ph129.split
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, (%r13)
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, 4(%r13)
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, 8(%r13)
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, 12(%r13)
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, 16(%r13)
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, 20(%r13)
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, 24(%r13)
	movl	$0, 16(%rsp,%rax,4)
	movl	%eax, 28(%r13)
	movl	$0, 16(%rsp,%rax,4)
	addq	$32, %r13
	addl	$-8, %r14d
	jne	.LBB2_40
.LBB2_41:                               # %._crit_edge130
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z11cullPoints2iPfiiPi, .Lfunc_end2-_Z11cullPoints2iPfiiPi
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_0:
	.long	1056964608              # float 0.5
.LCPI3_2:
	.long	4286578687              # float -3.40282347E+38
.LCPI3_4:
	.long	1065772646              # float 1.04999995
.LCPI3_5:
	.long	1065353216              # float 1
.LCPI3_6:
	.long	3212836864              # float -1
.LCPI3_7:
	.long	953267991               # float 9.99999974E-5
.LCPI3_8:
	.long	0                       # float 0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI3_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE
	.p2align	4, 0x90
	.type	_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE,@function
_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE: # @_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$984, %rsp              # imm = 0x3D8
.Lcfi19:
	.cfi_def_cfa_offset 1040
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rcx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movss	(%r12), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	subss	(%r15), %xmm3
	subss	4(%r15), %xmm4
	movss	8(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	8(%r15), %xmm1
	movsd	(%r14), %xmm2           # xmm2 = mem[0],zero
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	movsd	32(%r14), %xmm12        # xmm12 = mem[0],zero
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	movaps	%xmm4, 112(%rsp)        # 16-byte Spill
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm0, %xmm4
	addps	%xmm3, %xmm4
	movaps	%xmm1, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm12, %xmm8
	addps	%xmm4, %xmm8
	movss	8(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	movss	24(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	40(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 256(%rsp)        # 16-byte Spill
	movss	.LCPI3_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm15          # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm15
	movss	%xmm15, 388(%rsp)
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	movss	%xmm6, 392(%rsp)
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movss	%xmm3, 396(%rsp)
	movss	(%r9), %xmm5            # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movss	%xmm5, 376(%rsp)
	movss	4(%r9), %xmm14          # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm14
	movss	%xmm14, 380(%rsp)
	mulss	8(%r9), %xmm4
	movss	%xmm4, 384(%rsp)
	movss	(%rbx), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm2, %xmm6
	movss	16(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	addps	%xmm6, %xmm3
	movss	32(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, 224(%rsp)       # 16-byte Spill
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm12, %xmm13
	addps	%xmm3, %xmm13
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm2, %xmm3
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm0, %xmm6
	addps	%xmm3, %xmm6
	movss	36(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, 48(%rsp)         # 16-byte Spill
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm12, %xmm9
	addps	%xmm6, %xmm9
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	unpcklps	%xmm6, %xmm6    # xmm6 = xmm6[0,0,1,1]
	pshufd	$225, %xmm2, %xmm2      # xmm2 = xmm2[1,0,2,3]
	mulps	%xmm6, %xmm2
	movss	24(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	unpcklps	%xmm6, %xmm6    # xmm6 = xmm6[0,0,1,1]
	pshufd	$225, %xmm0, %xmm0      # xmm0 = xmm0[1,0,2,3]
	mulps	%xmm6, %xmm0
	addps	%xmm2, %xmm0
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, 144(%rsp)        # 16-byte Spill
	unpcklps	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1]
	pshufd	$225, %xmm12, %xmm12    # xmm12 = xmm12[1,0,2,3]
	mulps	%xmm2, %xmm12
	addps	%xmm0, %xmm12
	movaps	.LCPI3_1(%rip), %xmm7   # xmm7 = [nan,nan,nan,nan]
	movaps	%xmm13, %xmm0
	andps	%xmm7, %xmm0
	movaps	%xmm0, 288(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm0
	addss	%xmm15, %xmm0
	movaps	%xmm9, %xmm3
	andps	%xmm7, %xmm3
	movaps	%xmm3, %xmm2
	movss	%xmm14, 192(%rsp)       # 4-byte Spill
	mulss	%xmm14, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm12, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm6, %xmm0
	andps	%xmm7, %xmm0
	movss	%xmm4, 128(%rsp)        # 4-byte Spill
	movaps	%xmm0, 336(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm8, %xmm0
	andps	%xmm7, %xmm0
	subss	%xmm4, %xmm0
	xorl	%r13d, %r13d
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	ja	.LBB3_152
# BB#1:
	movaps	%xmm6, 624(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm14
	movaps	%xmm3, 320(%rsp)        # 16-byte Spill
	movaps	%xmm13, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm9, 512(%rsp)        # 16-byte Spill
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	movaps	%xmm2, %xmm4
	andps	%xmm7, %xmm4
	movaps	%xmm4, 272(%rsp)        # 16-byte Spill
	movaps	%xmm9, 640(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm3
	andps	%xmm7, %xmm3
	movaps	%xmm12, %xmm4
	movaps	%xmm7, %xmm9
	andps	%xmm7, %xmm4
	movaps	%xmm4, 416(%rsp)        # 16-byte Spill
	movss	.LCPI3_2(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	xorl	%r13d, %r13d
	movaps	%xmm4, %xmm15
	ucomiss	%xmm4, %xmm0
	movaps	%xmm13, 528(%rsp)       # 16-byte Spill
	movaps	%xmm2, 752(%rsp)        # 16-byte Spill
	movaps	%xmm1, %xmm10
	jbe	.LBB3_3
# BB#2:
	xorps	%xmm2, %xmm2
	xorl	%ebp, %ebp
	ucomiss	%xmm8, %xmm2
	seta	%bpl
	movl	$1, %eax
	movaps	%xmm0, %xmm15
	movq	%r14, %rcx
	jmp	.LBB3_4
.LBB3_3:
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
.LBB3_4:
	movss	128(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm0
	movss	392(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm5, %xmm2
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	addss	%xmm4, %xmm2
	movaps	%xmm3, %xmm4
	mulss	192(%rsp), %xmm4        # 4-byte Folded Reload
	addss	%xmm2, %xmm4
	movaps	416(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm7, %xmm2
	addss	%xmm4, %xmm2
	subss	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	ja	.LBB3_152
# BB#5:
	movaps	%xmm1, 304(%rsp)        # 16-byte Spill
	movaps	%xmm12, 496(%rsp)       # 16-byte Spill
	movaps	%xmm8, 400(%rsp)        # 16-byte Spill
	movss	%xmm5, 4(%rsp)          # 4-byte Spill
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	unpcklps	16(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	movaps	160(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	unpcklps	112(%rsp), %xmm6 # 16-byte Folded Reload
                                        # xmm6 = xmm6[0],mem[0],xmm6[1],mem[1]
	movaps	%xmm11, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm5
	movaps	%xmm14, %xmm2
	mulss	%xmm1, %xmm2
	mulps	%xmm6, %xmm4
	movaps	176(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm6
	mulss	%xmm11, %xmm6
	addss	%xmm2, %xmm6
	mulss	96(%rsp), %xmm1         # 16-byte Folded Reload
	mulss	32(%rsp), %xmm11        # 16-byte Folded Reload
	addss	%xmm1, %xmm11
	movaps	144(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1]
	addps	%xmm5, %xmm4
	movaps	256(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm5, %xmm2
	mulss	48(%rsp), %xmm5         # 16-byte Folded Reload
	addss	%xmm6, %xmm2
	addss	%xmm11, %xmm5
	addps	%xmm4, %xmm1
	movaps	%xmm2, 160(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm6
	andps	%xmm9, %xmm6
	movaps	%xmm5, 256(%rsp)        # 16-byte Spill
	movaps	%xmm5, %xmm7
	andps	%xmm9, %xmm7
	movaps	%xmm1, %xmm5
	andps	%xmm9, %xmm5
	ucomiss	%xmm15, %xmm0
	jbe	.LBB3_7
# BB#6:
	leaq	4(%r14), %rcx
	xorps	%xmm2, %xmm2
	xorl	%ebp, %ebp
	ucomiss	304(%rsp), %xmm2        # 16-byte Folded Reload
	seta	%bpl
	movl	$2, %eax
	movaps	%xmm0, %xmm15
.LBB3_7:
	movaps	%xmm1, 608(%rsp)        # 16-byte Spill
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm0
	movaps	%xmm6, %xmm2
	movss	4(%rsp), %xmm11         # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm2
	movss	8(%rsp), %xmm13         # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	addss	%xmm13, %xmm2
	movaps	%xmm7, %xmm4
	mulss	192(%rsp), %xmm4        # 4-byte Folded Reload
	addss	%xmm2, %xmm4
	movaps	%xmm5, %xmm2
	mulss	128(%rsp), %xmm2        # 4-byte Folded Reload
	addss	%xmm4, %xmm2
	subss	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	ja	.LBB3_152
# BB#8:
	movaps	%xmm5, 480(%rsp)        # 16-byte Spill
	ucomiss	%xmm15, %xmm0
	jbe	.LBB3_10
# BB#9:
	leaq	8(%r14), %rcx
	xorps	%xmm2, %xmm2
	xorl	%ebp, %ebp
	ucomiss	%xmm1, %xmm2
	seta	%bpl
	movl	$3, %eax
	movaps	%xmm0, %xmm15
.LBB3_10:
	mulss	16(%rsp), %xmm14        # 16-byte Folded Reload
	mulss	112(%rsp), %xmm8        # 16-byte Folded Reload
	addss	%xmm14, %xmm8
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm10, %xmm2
	addss	%xmm8, %xmm2
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	movaps	%xmm2, %xmm8
	andps	%xmm2, %xmm0
	movss	388(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	288(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm12, %xmm2
	movaps	272(%rsp), %xmm4        # 16-byte Reload
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm6, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm4, %xmm2
	addss	%xmm11, %xmm2
	subss	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	ja	.LBB3_152
# BB#11:
	ucomiss	%xmm15, %xmm0
	jbe	.LBB3_13
# BB#12:
	xorps	%xmm2, %xmm2
	xorl	%ebp, %ebp
	ucomiss	%xmm8, %xmm2
	seta	%bpl
	movl	$4, %eax
	movaps	%xmm0, %xmm15
	movq	%rbx, %rcx
.LBB3_13:
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	mulss	16(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	mulss	112(%rsp), %xmm4        # 16-byte Folded Reload
	addss	%xmm0, %xmm4
	movaps	48(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm10, %xmm2
	addss	%xmm4, %xmm2
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	movaps	%xmm2, %xmm8
	andps	%xmm2, %xmm0
	movaps	320(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm12, %xmm2
	movaps	%xmm3, %xmm4
	mulss	%xmm5, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm7, %xmm2
	mulss	8(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm4, %xmm2
	addss	192(%rsp), %xmm2        # 4-byte Folded Reload
	subss	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm0
	movss	128(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	ja	.LBB3_152
# BB#14:
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	ucomiss	%xmm15, %xmm0
	jbe	.LBB3_16
# BB#15:
	leaq	4(%rbx), %rcx
	xorps	%xmm2, %xmm2
	xorl	%ebp, %ebp
	ucomiss	%xmm8, %xmm2
	seta	%bpl
	movl	$5, %eax
	movaps	%xmm0, %xmm15
.LBB3_16:
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	mulss	80(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	112(%rsp), %xmm1        # 16-byte Reload
	mulss	64(%rsp), %xmm1         # 16-byte Folded Reload
	addss	%xmm0, %xmm1
	mulss	144(%rsp), %xmm10       # 16-byte Folded Reload
	addss	%xmm1, %xmm10
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	movaps	%xmm10, %xmm5
	andps	%xmm10, %xmm0
	movaps	336(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm1
	mulss	%xmm12, %xmm1
	movaps	416(%rsp), %xmm2        # 16-byte Reload
	mulss	12(%rsp), %xmm2         # 4-byte Folded Reload
	addss	%xmm1, %xmm2
	movaps	480(%rsp), %xmm1        # 16-byte Reload
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	addss	%xmm2, %xmm1
	addss	%xmm11, %xmm1
	subss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	movaps	%xmm6, %xmm8
	ja	.LBB3_152
# BB#17:
	ucomiss	%xmm15, %xmm0
	jbe	.LBB3_19
# BB#18:
	leaq	8(%rbx), %rcx
	xorps	%xmm1, %xmm1
	xorl	%ebp, %ebp
	ucomiss	%xmm5, %xmm1
	seta	%bpl
	movl	$6, %eax
	movaps	%xmm0, %xmm15
.LBB3_19:
	movaps	224(%rsp), %xmm9        # 16-byte Reload
	movaps	752(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm6, %xmm9
	movaps	304(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm2, %xmm0
	subss	%xmm0, %xmm9
	movaps	.LCPI3_1(%rip), %xmm5   # xmm5 = [nan,nan,nan,nan]
	andps	%xmm9, %xmm5
	movaps	%xmm8, %xmm0
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	272(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	192(%rsp), %xmm0        # 4-byte Folded Reload
	addss	%xmm1, %xmm0
	movaps	320(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	subss	%xmm1, %xmm5
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm5
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	ja	.LBB3_152
# BB#20:
	movaps	%xmm2, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm0, %xmm4
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm1, 960(%rsp)        # 16-byte Spill
	movaps	%xmm4, 464(%rsp)        # 16-byte Spill
	addss	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_22
# BB#21:                                # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movaps	%xmm8, 144(%rsp)        # 16-byte Spill
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movaps	%xmm15, 16(%rsp)        # 16-byte Spill
	movaps	%xmm5, 80(%rsp)         # 16-byte Spill
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	64(%rsp), %xmm9         # 16-byte Reload
	movaps	80(%rsp), %xmm5         # 16-byte Reload
	movaps	16(%rsp), %xmm15        # 16-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	movss	128(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	336(%rsp), %xmm10       # 16-byte Reload
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB3_22:                               # %.split
	xorps	%xmm13, %xmm13
	ucomiss	%xmm13, %xmm0
	jbe	.LBB3_25
# BB#23:
	divss	%xmm0, %xmm5
	movss	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	ucomiss	%xmm15, %xmm2
	xorps	%xmm1, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	xorps	%xmm1, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	xorps	%xmm1, %xmm1
	movaps	640(%rsp), %xmm4        # 16-byte Reload
	jbe	.LBB3_26
# BB#24:
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	160(%rsp), %xmm1        # 16-byte Folded Reload
	xorps	%xmm2, %xmm2
	xorps	%xmm6, %xmm6
	divss	%xmm0, %xmm6
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	divss	%xmm0, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	movaps	752(%rsp), %xmm1        # 16-byte Reload
	divss	%xmm0, %xmm1
	xorl	%ebp, %ebp
	ucomiss	%xmm9, %xmm2
	seta	%bpl
	movl	$7, %eax
	xorl	%ecx, %ecx
	movaps	%xmm5, %xmm15
	jmp	.LBB3_26
.LBB3_25:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	xorps	%xmm1, %xmm1
	movaps	640(%rsp), %xmm4        # 16-byte Reload
.LBB3_26:
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	movaps	224(%rsp), %xmm9        # 16-byte Reload
	mulss	%xmm4, %xmm9
	movaps	304(%rsp), %xmm0        # 16-byte Reload
	movaps	256(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm9
	movaps	%xmm7, %xmm0
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	%xmm3, %xmm2
	mulss	8(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	movaps	%xmm10, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm2, %xmm0
	movaps	288(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm11, %xmm2
	addss	%xmm0, %xmm2
	movaps	.LCPI3_1(%rip), %xmm5   # xmm5 = [nan,nan,nan,nan]
	andps	%xmm9, %xmm5
	subss	%xmm2, %xmm5
	ucomiss	%xmm13, %xmm5
	ja	.LBB3_152
# BB#27:
	movaps	%xmm1, %xmm2
	mulss	%xmm2, %xmm2
	xorps	%xmm6, %xmm6
	addss	%xmm6, %xmm2
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm1, 928(%rsp)        # 16-byte Spill
	movaps	%xmm2, 944(%rsp)        # 16-byte Spill
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_29
# BB#28:                                # %call.sqrt1345
	movaps	%xmm1, %xmm0
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movaps	%xmm8, 144(%rsp)        # 16-byte Spill
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movaps	%xmm15, 16(%rsp)        # 16-byte Spill
	movaps	%xmm9, 736(%rsp)        # 16-byte Spill
	movaps	%xmm5, 448(%rsp)        # 16-byte Spill
	callq	sqrtf
	xorps	%xmm6, %xmm6
	movaps	448(%rsp), %xmm5        # 16-byte Reload
	movaps	736(%rsp), %xmm9        # 16-byte Reload
	movaps	16(%rsp), %xmm15        # 16-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	movaps	640(%rsp), %xmm4        # 16-byte Reload
	movss	128(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	336(%rsp), %xmm10       # 16-byte Reload
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB3_29:                               # %.split1344
	ucomiss	%xmm6, %xmm0
	jbe	.LBB3_32
# BB#30:
	divss	%xmm0, %xmm5
	movss	.LCPI3_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	ucomiss	%xmm15, %xmm1
	jbe	.LBB3_32
# BB#31:
	movaps	.LCPI3_3(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	256(%rsp), %xmm2        # 16-byte Folded Reload
	xorps	%xmm1, %xmm1
	xorps	%xmm6, %xmm6
	divss	%xmm0, %xmm6
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	divss	%xmm0, %xmm2
	movaps	%xmm2, 80(%rsp)         # 16-byte Spill
	divss	%xmm0, %xmm4
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	ucomiss	%xmm9, %xmm1
	seta	%bpl
	movl	$8, %eax
	xorl	%ecx, %ecx
	movaps	%xmm5, %xmm15
.LBB3_32:
	movaps	224(%rsp), %xmm9        # 16-byte Reload
	movaps	496(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm9
	movaps	608(%rsp), %xmm4        # 16-byte Reload
	movaps	304(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm9
	movaps	.LCPI3_1(%rip), %xmm5   # xmm5 = [nan,nan,nan,nan]
	andps	%xmm9, %xmm5
	movaps	480(%rsp), %xmm0        # 16-byte Reload
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	416(%rsp), %xmm1        # 16-byte Reload
	mulss	8(%rsp), %xmm1          # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movaps	320(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm14, %xmm0
	addss	%xmm1, %xmm0
	movaps	288(%rsp), %xmm1        # 16-byte Reload
	mulss	192(%rsp), %xmm1        # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	subss	%xmm1, %xmm5
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm5
	ja	.LBB3_152
# BB#33:
	mulss	%xmm4, %xmm4
	addss	%xmm0, %xmm4
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm1, 736(%rsp)        # 16-byte Spill
	movaps	%xmm4, 304(%rsp)        # 16-byte Spill
	addss	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_35
# BB#34:                                # %call.sqrt1347
	movaps	%xmm1, %xmm0
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movaps	%xmm8, 144(%rsp)        # 16-byte Spill
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movaps	%xmm15, 16(%rsp)        # 16-byte Spill
	movaps	%xmm5, 448(%rsp)        # 16-byte Spill
	movaps	%xmm9, 720(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	720(%rsp), %xmm9        # 16-byte Reload
	movaps	448(%rsp), %xmm5        # 16-byte Reload
	movaps	16(%rsp), %xmm15        # 16-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	movss	128(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	336(%rsp), %xmm10       # 16-byte Reload
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB3_35:                               # %.split1346
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB3_38
# BB#36:
	divss	%xmm0, %xmm5
	movss	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	ucomiss	%xmm15, %xmm2
	movaps	528(%rsp), %xmm4        # 16-byte Reload
	jbe	.LBB3_39
# BB#37:
	movaps	.LCPI3_3(%rip), %xmm6   # xmm6 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	608(%rsp), %xmm6        # 16-byte Folded Reload
	xorps	%xmm13, %xmm13
	xorps	%xmm2, %xmm2
	divss	%xmm0, %xmm2
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	divss	%xmm0, %xmm6
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	movaps	496(%rsp), %xmm6        # 16-byte Reload
	divss	%xmm0, %xmm6
	movaps	%xmm6, 176(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	ucomiss	%xmm9, %xmm13
	seta	%bpl
	movl	$9, %eax
	xorl	%ecx, %ecx
	movaps	%xmm5, %xmm15
	jmp	.LBB3_39
.LBB3_38:
	movaps	528(%rsp), %xmm4        # 16-byte Reload
.LBB3_39:
	movaps	400(%rsp), %xmm5        # 16-byte Reload
	mulss	160(%rsp), %xmm5        # 16-byte Folded Reload
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm8, %xmm0
	mulss	%xmm12, %xmm0
	movaps	288(%rsp), %xmm2        # 16-byte Reload
	mulss	8(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	movaps	416(%rsp), %xmm0        # 16-byte Reload
	mulss	192(%rsp), %xmm0        # 4-byte Folded Reload
	addss	%xmm2, %xmm0
	movaps	%xmm3, %xmm2
	mulss	%xmm11, %xmm2
	addss	%xmm0, %xmm2
	movaps	.LCPI3_1(%rip), %xmm6   # xmm6 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm6
	subss	%xmm2, %xmm6
	ucomiss	%xmm1, %xmm6
	ja	.LBB3_152
# BB#40:
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	movaps	%xmm8, 144(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	464(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm0, 448(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	movaps	%xmm15, 16(%rsp)        # 16-byte Spill
	jnp	.LBB3_42
# BB#41:                                # %call.sqrt1349
	movaps	%xmm1, %xmm0
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movaps	%xmm5, 464(%rsp)        # 16-byte Spill
	movaps	%xmm6, 720(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	720(%rsp), %xmm6        # 16-byte Reload
	movaps	464(%rsp), %xmm5        # 16-byte Reload
	movaps	16(%rsp), %xmm15        # 16-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	movaps	528(%rsp), %xmm4        # 16-byte Reload
	movaps	336(%rsp), %xmm10       # 16-byte Reload
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB3_42:                               # %.split1348
	xorps	%xmm8, %xmm8
	ucomiss	%xmm8, %xmm0
	jbe	.LBB3_45
# BB#43:
	divss	%xmm0, %xmm6
	movss	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	ucomiss	%xmm15, %xmm2
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	movaps	256(%rsp), %xmm9        # 16-byte Reload
	jbe	.LBB3_46
# BB#44:
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm4, %xmm1
	movaps	160(%rsp), %xmm13       # 16-byte Reload
	divss	%xmm0, %xmm13
	xorps	%xmm2, %xmm2
	movaps	%xmm6, %xmm3
	xorps	%xmm6, %xmm6
	divss	%xmm0, %xmm6
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	divss	%xmm0, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	ucomiss	%xmm5, %xmm2
	seta	%bpl
	movl	$10, %eax
	xorl	%ecx, %ecx
	movaps	%xmm13, 64(%rsp)        # 16-byte Spill
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	jmp	.LBB3_46
.LBB3_45:
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	movaps	256(%rsp), %xmm9        # 16-byte Reload
.LBB3_46:
	movaps	400(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm9, %xmm3
	movaps	512(%rsp), %xmm1        # 16-byte Reload
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm3
	movaps	.LCPI3_1(%rip), %xmm5   # xmm5 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm5
	movaps	%xmm7, %xmm0
	mulss	%xmm12, %xmm0
	movaps	%xmm11, %xmm2
	mulss	8(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	movaps	416(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm14, %xmm0
	addss	%xmm2, %xmm0
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	mulss	128(%rsp), %xmm2        # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	subss	%xmm2, %xmm5
	ucomiss	%xmm8, %xmm5
	movaps	624(%rsp), %xmm8        # 16-byte Reload
	ja	.LBB3_152
# BB#47:
	movaps	%xmm1, %xmm0
	mulss	%xmm0, %xmm0
	movaps	944(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_49
# BB#48:                                # %call.sqrt1351
	movaps	%xmm1, %xmm0
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movaps	%xmm5, 160(%rsp)        # 16-byte Spill
	movaps	%xmm3, 464(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	464(%rsp), %xmm3        # 16-byte Reload
	movaps	160(%rsp), %xmm5        # 16-byte Reload
	movaps	256(%rsp), %xmm9        # 16-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	movaps	336(%rsp), %xmm10       # 16-byte Reload
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	624(%rsp), %xmm8        # 16-byte Reload
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB3_49:                               # %.split1350
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB3_52
# BB#50:
	divss	%xmm0, %xmm5
	movss	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	ucomiss	16(%rsp), %xmm2         # 16-byte Folded Reload
	movaps	112(%rsp), %xmm13       # 16-byte Reload
	jbe	.LBB3_53
# BB#51:
	movaps	.LCPI3_3(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	512(%rsp), %xmm2        # 16-byte Folded Reload
	divss	%xmm0, %xmm9
	xorps	%xmm15, %xmm15
	xorps	%xmm6, %xmm6
	divss	%xmm0, %xmm6
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	divss	%xmm0, %xmm2
	movaps	%xmm2, 176(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	ucomiss	%xmm3, %xmm15
	seta	%bpl
	movl	$11, %eax
	xorl	%ecx, %ecx
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	jmp	.LBB3_53
.LBB3_52:
	movaps	112(%rsp), %xmm13       # 16-byte Reload
.LBB3_53:
	movaps	496(%rsp), %xmm3        # 16-byte Reload
	movaps	400(%rsp), %xmm0        # 16-byte Reload
	shufps	$1, %xmm0, %xmm3        # xmm3 = xmm3[1,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm3      # xmm3 = xmm3[2,0],xmm0[2,3]
	mulps	608(%rsp), %xmm3        # 16-byte Folded Reload
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm3
	movaps	.LCPI3_1(%rip), %xmm5   # xmm5 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm5
	movaps	480(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm12, %xmm0
	movaps	%xmm10, %xmm2
	mulss	8(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	movaps	%xmm13, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm2, %xmm0
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	mulss	192(%rsp), %xmm2        # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	subss	%xmm2, %xmm5
	ucomiss	%xmm1, %xmm5
	ja	.LBB3_152
# BB#54:
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	movaps	304(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm0, 256(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_56
# BB#55:                                # %call.sqrt1353
	movaps	%xmm1, %xmm0
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movaps	%xmm5, 160(%rsp)        # 16-byte Spill
	movaps	%xmm3, 304(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	304(%rsp), %xmm3        # 16-byte Reload
	movaps	160(%rsp), %xmm5        # 16-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	112(%rsp), %xmm13       # 16-byte Reload
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	624(%rsp), %xmm8        # 16-byte Reload
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB3_56:                               # %.split1352
	xorps	%xmm9, %xmm9
	ucomiss	%xmm9, %xmm0
	jbe	.LBB3_59
# BB#57:
	divss	%xmm0, %xmm5
	movss	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	ucomiss	16(%rsp), %xmm2         # 16-byte Folded Reload
	movaps	400(%rsp), %xmm4        # 16-byte Reload
	jbe	.LBB3_60
# BB#58:
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	movaps	608(%rsp), %xmm2        # 16-byte Reload
	divss	%xmm0, %xmm2
	xorps	%xmm10, %xmm10
	xorps	%xmm6, %xmm6
	divss	%xmm0, %xmm6
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	divss	%xmm0, %xmm1
	movaps	%xmm1, 176(%rsp)        # 16-byte Spill
	xorl	%ebp, %ebp
	ucomiss	%xmm3, %xmm10
	seta	%bpl
	movl	$12, %eax
	xorl	%ecx, %ecx
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	jmp	.LBB3_60
.LBB3_59:
	movaps	400(%rsp), %xmm4        # 16-byte Reload
.LBB3_60:
	movaps	%xmm4, %xmm3
	shufps	$225, %xmm3, %xmm3      # xmm3 = xmm3[1,0,2,3]
	movaps	528(%rsp), %xmm1        # 16-byte Reload
	mulps	%xmm3, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm1
	movaps	.LCPI3_1(%rip), %xmm5   # xmm5 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm5
	movaps	272(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm12, %xmm0
	movaps	288(%rsp), %xmm2        # 16-byte Reload
	mulss	12(%rsp), %xmm2         # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	movaps	480(%rsp), %xmm0        # 16-byte Reload
	mulss	192(%rsp), %xmm0        # 4-byte Folded Reload
	addss	%xmm2, %xmm0
	movaps	%xmm7, %xmm2
	movss	128(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	subss	%xmm2, %xmm5
	ucomiss	%xmm9, %xmm5
	ja	.LBB3_152
# BB#61:
	movaps	%xmm1, 272(%rsp)        # 16-byte Spill
	movaps	448(%rsp), %xmm1        # 16-byte Reload
	addss	960(%rsp), %xmm1        # 16-byte Folded Reload
	xorps	%xmm2, %xmm2
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	jnp	.LBB3_63
# BB#62:                                # %call.sqrt1355
	movaps	%xmm1, %xmm0
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	movaps	%xmm5, 288(%rsp)        # 16-byte Spill
	callq	sqrtf
	xorps	%xmm2, %xmm2
	movaps	288(%rsp), %xmm5        # 16-byte Reload
	movaps	160(%rsp), %xmm3        # 16-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	112(%rsp), %xmm13       # 16-byte Reload
	movaps	320(%rsp), %xmm11       # 16-byte Reload
	movss	128(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm14         # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB3_63:                               # %.split1354
	ucomiss	%xmm2, %xmm0
	jbe	.LBB3_66
# BB#64:
	divss	%xmm0, %xmm5
	movss	.LCPI3_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	mulss	%xmm5, %xmm1
	ucomiss	16(%rsp), %xmm1         # 16-byte Folded Reload
	movaps	512(%rsp), %xmm1        # 16-byte Reload
	movaps	144(%rsp), %xmm8        # 16-byte Reload
	jbe	.LBB3_67
# BB#65:
	movaps	752(%rsp), %xmm7        # 16-byte Reload
	xorps	.LCPI3_3(%rip), %xmm7
	divss	%xmm0, %xmm7
	movaps	528(%rsp), %xmm9        # 16-byte Reload
	divss	%xmm0, %xmm9
	xorps	%xmm2, %xmm2
	xorl	%ebp, %ebp
	ucomiss	272(%rsp), %xmm2        # 16-byte Folded Reload
	divss	%xmm0, %xmm2
	movaps	%xmm2, 176(%rsp)        # 16-byte Spill
	seta	%bpl
	movl	$13, %eax
	xorl	%ecx, %ecx
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	jmp	.LBB3_67
.LBB3_66:
	movaps	512(%rsp), %xmm1        # 16-byte Reload
	movaps	144(%rsp), %xmm8        # 16-byte Reload
.LBB3_67:
	mulps	%xmm1, %xmm3
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm3
	movaps	.LCPI3_1(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm1
	mulss	%xmm12, %xmm13
	mulss	12(%rsp), %xmm11        # 4-byte Folded Reload
	addss	%xmm13, %xmm11
	movaps	480(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm14, %xmm0
	addss	%xmm11, %xmm0
	mulss	%xmm8, %xmm4
	addss	%xmm0, %xmm4
	subss	%xmm4, %xmm1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	ja	.LBB3_152
# BB#68:
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movaps	%xmm3, 160(%rsp)        # 16-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movss	%xmm12, 96(%rsp)        # 4-byte Spill
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movaps	224(%rsp), %xmm2        # 16-byte Reload
	addss	928(%rsp), %xmm2        # 16-byte Folded Reload
	addss	%xmm0, %xmm2
	movaps	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_70
# BB#69:                                # %call.sqrt1357
	movaps	%xmm1, %xmm0
	callq	sqrtf
.LBB3_70:                               # %.split1356
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB3_73
# BB#71:
	movaps	128(%rsp), %xmm3        # 16-byte Reload
	divss	%xmm0, %xmm3
	movss	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm5
	mulss	%xmm3, %xmm2
	ucomiss	16(%rsp), %xmm2         # 16-byte Folded Reload
	jbe	.LBB3_73
# BB#72:
	movaps	640(%rsp), %xmm3        # 16-byte Reload
	xorps	.LCPI3_3(%rip), %xmm3
	divss	%xmm0, %xmm3
	movaps	512(%rsp), %xmm2        # 16-byte Reload
	divss	%xmm0, %xmm2
	xorps	%xmm4, %xmm4
	xorl	%ebp, %ebp
	ucomiss	160(%rsp), %xmm4        # 16-byte Folded Reload
	divss	%xmm0, %xmm4
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	seta	%bpl
	movl	$14, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movaps	%xmm3, 64(%rsp)         # 16-byte Spill
	movaps	%xmm2, 80(%rsp)         # 16-byte Spill
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
.LBB3_73:
	movaps	400(%rsp), %xmm0        # 16-byte Reload
	mulps	496(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm0, %xmm2
	movaps	.LCPI3_1(%rip), %xmm3   # xmm3 = [nan,nan,nan,nan]
	movaps	%xmm2, 128(%rsp)        # 16-byte Spill
	andps	%xmm2, %xmm3
	movaps	416(%rsp), %xmm2        # 16-byte Reload
	mulss	96(%rsp), %xmm2         # 4-byte Folded Reload
	movaps	336(%rsp), %xmm0        # 16-byte Reload
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
	addss	%xmm2, %xmm0
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	mulss	4(%rsp), %xmm2          # 4-byte Folded Reload
	addss	%xmm0, %xmm2
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	mulss	192(%rsp), %xmm0        # 4-byte Folded Reload
	addss	%xmm2, %xmm0
	subss	%xmm0, %xmm3
	movaps	%xmm3, 192(%rsp)        # 16-byte Spill
	ucomiss	%xmm1, %xmm3
	ja	.LBB3_152
# BB#74:
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	addss	736(%rsp), %xmm0        # 16-byte Folded Reload
	xorps	%xmm1, %xmm1
	addss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	sqrtss	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB3_76
# BB#75:                                # %call.sqrt1359
	movaps	%xmm1, %xmm0
	callq	sqrtf
.LBB3_76:                               # %.split1358
	ucomiss	.LCPI3_8, %xmm0
	jbe	.LBB3_79
# BB#77:
	movaps	192(%rsp), %xmm2        # 16-byte Reload
	divss	%xmm0, %xmm2
	movss	.LCPI3_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm2, %xmm1
	ucomiss	16(%rsp), %xmm1         # 16-byte Folded Reload
	jbe	.LBB3_79
# BB#78:                                # %.thread1169
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	496(%rsp), %xmm1        # 16-byte Folded Reload
	divss	%xmm0, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movaps	624(%rsp), %xmm1        # 16-byte Reload
	divss	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	xorl	%ebp, %ebp
	ucomiss	128(%rsp), %xmm2        # 16-byte Folded Reload
	divss	%xmm0, %xmm2
	movaps	%xmm2, 176(%rsp)        # 16-byte Spill
	seta	%bpl
	movl	$15, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	jmp	.LBB3_83
.LBB3_79:
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	je	.LBB3_152
# BB#80:
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB3_82
# BB#81:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rsi
	movl	(%rsi), %eax
	movq	1040(%rsp), %rcx
	movq	%rcx, %rdx
	movl	%eax, (%rdx)
	movl	16(%rsi), %ecx
	movl	%ecx, 4(%rdx)
	movss	32(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movd	%eax, %xmm0
	movd	%ecx, %xmm1
	jmp	.LBB3_84
.LBB3_82:
	movaps	16(%rsp), %xmm6         # 16-byte Reload
.LBB3_83:
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm5         # 16-byte Reload
	mulss	%xmm5, %xmm0
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	176(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	movq	1040(%rsp), %rax
	movss	%xmm0, (%rax)
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	movss	20(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm2
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, 4(%rax)
	mulss	32(%r14), %xmm5
	mulss	36(%r14), %xmm4
	addss	%xmm5, %xmm4
	mulss	40(%r14), %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
.LBB3_84:
	movq	1048(%rsp), %rax
	movq	1040(%rsp), %rcx
	movss	%xmm3, 8(%rcx)
	testl	%ebp, %ebp
	je	.LBB3_86
# BB#85:
	movdqa	.LCPI3_3(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm2, %xmm0
	movq	1040(%rsp), %rcx
	movd	%xmm0, (%rcx)
	pxor	%xmm2, %xmm1
	movd	%xmm1, 4(%rcx)
	xorps	%xmm2, %xmm3
	movss	%xmm3, 8(%rcx)
.LBB3_86:
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	xorps	.LCPI3_3(%rip), %xmm0
	movss	%xmm0, (%rax)
	cmpl	$7, 48(%rsp)            # 4-byte Folded Reload
	jl	.LBB3_90
# BB#87:                                # %.preheader1181
	movl	8(%r15), %ecx
	movl	%ecx, 792(%rsp)
	movq	(%r15), %rcx
	movq	%rcx, 784(%rsp)
	movq	1040(%rsp), %rcx
	movss	(%rcx), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	32(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r14), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm7, %xmm1
	movaps	%xmm12, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm11, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	cmpltss	%xmm1, %xmm4
	movss	.LCPI3_5(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	andps	%xmm8, %xmm1
	movss	.LCPI3_6(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	andnps	%xmm10, %xmm4
	orps	%xmm1, %xmm4
	mulss	96(%rsp), %xmm4         # 4-byte Folded Reload
	mulss	%xmm4, %xmm7
	mulss	%xmm4, %xmm6
	mulss	%xmm0, %xmm4
	movss	20(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%r14), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	mulss	%xmm5, %xmm3
	movaps	%xmm12, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm3, %xmm2
	movaps	%xmm11, %xmm1
	mulss	%xmm13, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm3, %xmm3
	cmpltss	%xmm1, %xmm3
	movaps	%xmm3, %xmm1
	andps	%xmm8, %xmm1
	andnps	%xmm10, %xmm3
	orps	%xmm1, %xmm3
	addss	784(%rsp), %xmm7
	addss	788(%rsp), %xmm6
	addss	792(%rsp), %xmm4
	mulss	12(%rsp), %xmm3         # 4-byte Folded Reload
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	mulss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	mulss	%xmm13, %xmm3
	addss	%xmm4, %xmm3
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	24(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm6
	mulss	%xmm2, %xmm6
	movaps	%xmm12, %xmm7
	mulss	%xmm4, %xmm7
	addss	%xmm6, %xmm7
	movaps	%xmm11, %xmm6
	mulss	%xmm1, %xmm6
	addss	%xmm7, %xmm6
	xorps	%xmm7, %xmm7
	cmpltss	%xmm6, %xmm7
	movaps	%xmm7, %xmm6
	andps	%xmm8, %xmm6
	andnps	%xmm10, %xmm7
	orps	%xmm6, %xmm7
	mulss	8(%rsp), %xmm7          # 4-byte Folded Reload
	mulss	%xmm7, %xmm2
	addss	%xmm5, %xmm2
	movss	%xmm2, 784(%rsp)
	mulss	%xmm7, %xmm4
	addss	%xmm0, %xmm4
	movss	%xmm4, 788(%rsp)
	mulss	%xmm1, %xmm7
	addss	%xmm3, %xmm7
	movss	%xmm7, 792(%rsp)
	movl	8(%r12), %ecx
	movl	%ecx, 552(%rsp)
	movq	(%r12), %rcx
	movq	%rcx, 544(%rsp)
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm12, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm11, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm4, %xmm4
	cmpltss	%xmm1, %xmm4
	movaps	%xmm4, %xmm1
	andps	%xmm10, %xmm1
	andnps	%xmm8, %xmm4
	orps	%xmm1, %xmm4
	mulss	4(%rsp), %xmm4          # 4-byte Folded Reload
	mulss	%xmm4, %xmm6
	addss	544(%rsp), %xmm6
	mulss	%xmm4, %xmm5
	addss	548(%rsp), %xmm5
	mulss	%xmm0, %xmm4
	addss	552(%rsp), %xmm4
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm12, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm11, %xmm2
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	xorps	%xmm3, %xmm3
	cmpltss	%xmm2, %xmm3
	movaps	%xmm3, %xmm2
	andps	%xmm10, %xmm2
	andnps	%xmm8, %xmm3
	orps	%xmm2, %xmm3
	mulss	380(%rsp), %xmm3
	mulss	%xmm3, %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm3, %xmm0
	addss	%xmm5, %xmm0
	mulss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	mulss	%xmm14, %xmm12
	addss	%xmm9, %xmm12
	mulss	%xmm2, %xmm11
	addss	%xmm12, %xmm11
	xorps	%xmm4, %xmm4
	cmpltss	%xmm11, %xmm4
	andps	%xmm4, %xmm10
	andnps	%xmm8, %xmm4
	orps	%xmm10, %xmm4
	mulss	384(%rsp), %xmm4
	mulss	%xmm4, %xmm1
	addss	%xmm7, %xmm1
	movss	%xmm1, 544(%rsp)
	mulss	%xmm4, %xmm14
	addss	%xmm0, %xmm14
	movss	%xmm14, 548(%rsp)
	mulss	%xmm2, %xmm4
	addss	%xmm3, %xmm4
	movss	%xmm4, 552(%rsp)
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	-7(%rcx), %ecx
	movslq	%ecx, %rcx
	imulq	$1431655766, %rcx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	movslq	%edx, %rsi
	movss	(%r14,%rsi,4), %xmm11   # xmm11 = mem[0],zero,zero,zero
	movss	16(%r14,%rsi,4), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movss	32(%r14,%rsi,4), %xmm9  # xmm9 = mem[0],zero,zero,zero
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %ecx
	movslq	%ecx, %rcx
	movss	(%rbx,%rcx,4), %xmm15   # xmm15 = mem[0],zero,zero,zero
	movss	16(%rbx,%rcx,4), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movss	32(%rbx,%rcx,4), %xmm12 # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm15, %xmm0
	movaps	%xmm10, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm9, %xmm7
	mulss	%xmm12, %xmm7
	addss	%xmm2, %xmm7
	movaps	%xmm7, %xmm2
	mulss	%xmm2, %xmm2
	movaps	%xmm8, %xmm0
	subss	%xmm2, %xmm0
	movss	.LCPI3_7(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	xorps	%xmm2, %xmm2
	jae	.LBB3_89
# BB#88:
	movaps	%xmm1, %xmm6
	subss	784(%rsp), %xmm6
	movaps	%xmm14, %xmm5
	subss	788(%rsp), %xmm5
	movaps	%xmm4, %xmm2
	subss	792(%rsp), %xmm2
	movaps	%xmm6, %xmm3
	mulss	%xmm15, %xmm3
	movaps	%xmm5, %xmm8
	mulss	%xmm13, %xmm8
	addss	%xmm3, %xmm8
	movaps	%xmm2, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm8, %xmm3
	mulss	%xmm11, %xmm6
	mulss	%xmm10, %xmm5
	addss	%xmm6, %xmm5
	mulss	%xmm9, %xmm2
	addss	%xmm5, %xmm2
	movss	.LCPI3_5(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm5
	mulss	%xmm7, %xmm2
	subss	%xmm3, %xmm2
	mulss	%xmm5, %xmm2
.LBB3_89:                               # %_Z20dLineClosestApproachRK9btVector3S1_S1_S1_PfS2_.exit
	mulss	%xmm2, %xmm15
	addss	%xmm15, %xmm1
	movss	%xmm1, 544(%rsp)
	mulss	%xmm2, %xmm13
	addss	%xmm13, %xmm14
	movss	%xmm14, 548(%rsp)
	mulss	%xmm2, %xmm12
	addss	%xmm12, %xmm4
	movss	%xmm4, 552(%rsp)
	movq	1088(%rsp), %rdi
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	movq	1040(%rsp), %rdx
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	8(%rdx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm2
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm0, 688(%rsp)
	movlps	%xmm3, 696(%rsp)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm0
	leaq	688(%rsp), %rsi
	leaq	544(%rsp), %rdx
	callq	*%rcx
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	1056(%rsp), %rcx
	movl	%eax, (%rcx)
	movl	$1, %r13d
	jmp	.LBB3_152
.LBB3_90:
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$4, %eax
	movq	%r12, %rcx
	cmovlq	%r15, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	cmovlq	%r12, %r15
	leaq	388(%rsp), %rbp
	leaq	376(%rsp), %rcx
	movq	%rcx, %rdx
	cmovlq	%rbp, %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	cmovlq	%rcx, %rbp
	movq	%rbx, %rcx
	cmovlq	%r14, %rcx
	cmovlq	%rbx, %r14
	movq	1040(%rsp), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	movss	4(%rdx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	cmpl	$3, %eax
	jg	.LBB3_92
# BB#91:
	movq	1040(%rsp), %rax
	movd	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movdqa	%xmm0, 224(%rsp)        # 16-byte Spill
	jmp	.LBB3_93
.LBB3_92:
	movdqa	.LCPI3_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movdqa	192(%rsp), %xmm1        # 16-byte Reload
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, 192(%rsp)        # 16-byte Spill
	movdqa	128(%rsp), %xmm1        # 16-byte Reload
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, 128(%rsp)        # 16-byte Spill
	movq	1040(%rsp), %rax
	movd	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, 224(%rsp)        # 16-byte Spill
.LBB3_93:
	movsd	(%r14), %xmm0           # xmm0 = mem[0],zero
	movsd	16(%r14), %xmm1         # xmm1 = mem[0],zero
	movsd	32(%r14), %xmm2         # xmm2 = mem[0],zero
	movaps	192(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm0, %xmm3
	movaps	128(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	addps	%xmm3, %xmm4
	movaps	224(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm4, %xmm0
	movss	%xmm0, 768(%rsp)
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movss	%xmm1, 772(%rsp)
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	movss	24(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm1, %xmm2
	movss	40(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, 776(%rsp)
	movaps	.LCPI3_1(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm0
	andps	%xmm2, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	xorl	%esi, %esi
	ucomiss	%xmm1, %xmm0
	seta	%bl
	setbe	%sil
	addl	%esi, %esi
	xorl	%edi, %edi
	ucomiss	%xmm1, %xmm2
	seta	%dl
	setbe	%dil
	incl	%edi
	ucomiss	%xmm0, %xmm2
	ja	.LBB3_95
# BB#94:
	movl	%ebx, %edx
.LBB3_95:
	cmoval	%edi, %esi
	setbe	%al
	andb	%al, %bl
	movzbl	%dl, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorps	%xmm0, %xmm0
	ucomiss	768(%rsp,%rsi,4), %xmm0
	movss	(%rbp,%rsi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	144(%rsp), %rax         # 8-byte Reload
	subss	(%rax), %xmm0
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	jbe	.LBB3_97
# BB#96:                                # %.preheader1186.preheader
	addss	%xmm1, %xmm0
	movss	%xmm0, 360(%rsp)
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movq	144(%rsp), %rdx         # 8-byte Reload
	subss	4(%rdx), %xmm3
	movl	%esi, %eax
	orl	$4, %eax
	movss	(%r14,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	%xmm3, %xmm1
	movss	%xmm1, 364(%rsp)
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	8(%rdx), %xmm3
	orl	$8, %esi
	mulss	(%r14,%rsi,4), %xmm2
	addss	%xmm3, %xmm2
	movaps	%xmm2, %xmm9
	jmp	.LBB3_98
.LBB3_97:                               # %.preheader1188.preheader
	subss	%xmm1, %xmm0
	movss	%xmm0, 360(%rsp)
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movq	144(%rsp), %rdx         # 8-byte Reload
	subss	4(%rdx), %xmm1
	movl	%esi, %eax
	orl	$4, %eax
	movss	(%r14,%rax,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm1
	movss	%xmm1, 364(%rsp)
	movss	8(%r15), %xmm9          # xmm9 = mem[0],zero,zero,zero
	subss	8(%rdx), %xmm9
	orl	$8, %esi
	mulss	(%r14,%rsi,4), %xmm2
	subss	%xmm2, %xmm9
.LBB3_98:                               # %.loopexit1187
	movzbl	%bl, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	176(%rsp), %rax         # 8-byte Reload
	incl	%eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	48(%rsp), %rdx          # 8-byte Reload
	cmpl	$4, %edx
	setl	%al
	movss	%xmm9, 368(%rsp)
	leal	(%rax,%rax,2), %eax
	addl	%edx, %eax
	addl	$-4, %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	je	.LBB3_101
# BB#99:                                # %.loopexit1187
	cmpl	$1, 64(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_102
# BB#100:
	movl	$2, %edx
	jmp	.LBB3_103
.LBB3_101:
	movl	$2, %edx
	movl	$1, %edi
	jmp	.LBB3_104
.LBB3_102:
	movl	$1, %edx
.LBB3_103:
	xorl	%edi, %edi
.LBB3_104:
	movss	(%rcx,%rdi,4), %xmm10   # xmm10 = mem[0],zero,zero,zero
	movss	16(%rcx,%rdi,4), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movss	32(%rcx,%rdi,4), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm10, %xmm2
	movaps	%xmm1, %xmm3
	mulss	%xmm4, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm9, %xmm11
	mulss	%xmm13, %xmm11
	addss	%xmm3, %xmm11
	movss	(%rcx,%rdx,4), %xmm12   # xmm12 = mem[0],zero,zero,zero
	movss	16(%rcx,%rdx,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movss	32(%rcx,%rdx,4), %xmm14 # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm14, %xmm9
	addss	%xmm1, %xmm9
	movq	160(%rsp), %rax         # 8-byte Reload
	movss	(%r14,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movss	16(%r14,%rax,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movss	32(%r14,%rax,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm6
	mulss	%xmm1, %xmm6
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	addss	%xmm6, %xmm7
	movaps	%xmm13, %xmm5
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movl	176(%rsp), %ecx         # 4-byte Reload
	movss	(%r14,%rcx,4), %xmm6    # xmm6 = mem[0],zero,zero,zero
	movss	16(%r14,%rcx,4), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movss	32(%r14,%rcx,4), %xmm8  # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm10
	mulss	%xmm7, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm8, %xmm13
	addss	%xmm4, %xmm13
	mulss	%xmm12, %xmm1
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm14, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm6, %xmm12
	mulss	%xmm7, %xmm2
	addss	%xmm12, %xmm2
	mulss	%xmm8, %xmm14
	addss	%xmm2, %xmm14
	movss	(%rbp,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	%xmm5, 112(%rsp)        # 4-byte Spill
	movaps	%xmm5, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm0
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movss	(%rbp,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movss	%xmm13, 256(%rsp)       # 4-byte Spill
	movaps	%xmm13, %xmm2
	mulss	%xmm3, %xmm2
	movss	%xmm14, 272(%rsp)       # 4-byte Spill
	mulss	%xmm14, %xmm3
	movaps	%xmm11, %xmm4
	subss	%xmm1, %xmm4
	movaps	%xmm4, %xmm5
	subss	%xmm2, %xmm5
	leaq	688(%rsp), %rsi
	movss	%xmm5, 688(%rsp)
	movaps	%xmm9, %xmm5
	subss	%xmm0, %xmm5
	movaps	%xmm5, %xmm6
	subss	%xmm3, %xmm6
	movss	%xmm6, 692(%rsp)
	movss	%xmm11, 96(%rsp)        # 4-byte Spill
	addss	%xmm11, %xmm1
	movss	%xmm9, 8(%rsp)          # 4-byte Spill
	addss	%xmm9, %xmm0
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	subss	%xmm3, %xmm0
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	subss	%xmm2, %xmm1
	unpcklps	%xmm2, %xmm2    # xmm2 = xmm2[0,0,1,1]
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	addps	%xmm4, %xmm2
	movups	%xmm2, 696(%rsp)
	movss	%xmm1, 712(%rsp)
	movss	%xmm0, 716(%rsp)
	movq	80(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%rdi,4), %eax
	movl	%eax, 216(%rsp)
	movl	(%rcx,%rdx,4), %eax
	movl	%eax, 220(%rsp)
	leaq	544(%rsp), %r8
	movl	$4, %r13d
	xorl	%r10d, %r10d
	movl	$1, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movaps	.LCPI3_3(%rip), %xmm5   # xmm5 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
.LBB3_105:                              # %.preheader152.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_107 Depth 2
                                        #     Child Loop BB3_116 Depth 2
	testl	%r13d, %r13d
	jle	.LBB3_123
# BB#106:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_105 Depth=1
	movl	$1, %r12d
	subq	%r10, %r12
	leaq	8(%rsi), %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%rsi,%rax,4), %rdx
	leaq	(%rsi,%r10,4), %rdi
	xorl	%ebx, %ebx
	movq	%r8, %r11
.LBB3_107:                              # %.lr.ph.i
                                        #   Parent Loop BB3_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	xorps	%xmm5, %xmm2
	movss	216(%rsp,%r10,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB3_110
# BB#108:                               #   in Loop: Header=BB3_107 Depth=2
	movl	-8(%rbp), %eax
	movl	%eax, (%r11)
	movl	-4(%rbp), %eax
	movl	%eax, 4(%r11)
	incl	%ebx
	testb	$8, %bl
	jne	.LBB3_126
# BB#109:                               # %._crit_edge180.i
                                        #   in Loop: Header=BB3_107 Depth=2
	addq	$8, %r11
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	216(%rsp,%r10,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
.LBB3_110:                              #   in Loop: Header=BB3_107 Depth=2
	cmpl	$1, %r13d
	movq	%rsi, %rax
	cmovgq	%rbp, %rax
	movaps	%xmm1, %xmm2
	xorps	%xmm5, %xmm2
	ucomiss	%xmm2, %xmm0
	seta	%r9b
	movss	(%rax,%r10,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	xorps	%xmm5, %xmm3
	ucomiss	%xmm3, %xmm0
	seta	%r15b
	cmpb	%r15b, %r9b
	je	.LBB3_113
# BB#111:                               #   in Loop: Header=BB3_107 Depth=2
	movss	(%rdx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	(%rax,%r12,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm4
	subss	%xmm1, %xmm2
	divss	%xmm2, %xmm4
	xorps	%xmm5, %xmm0
	subss	%xmm1, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, (%r11,%r12,4)
	movss	216(%rsp,%r10,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm0
	movss	%xmm0, (%r11,%r10,4)
	incl	%ebx
	testb	$8, %bl
	jne	.LBB3_126
# BB#112:                               #   in Loop: Header=BB3_107 Depth=2
	addq	$8, %r11
.LBB3_113:                              #   in Loop: Header=BB3_107 Depth=2
	addq	$8, %rbp
	addq	$8, %rdx
	addq	$8, %rdi
	cmpl	$1, %r13d
	leal	-1(%r13), %eax
	movl	%eax, %r13d
	jg	.LBB3_107
# BB#114:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_105 Depth=1
	leaq	544(%rsp), %rax
	cmpq	%rax, %r8
	movq	%rax, %rsi
	leaq	784(%rsp), %rax
	cmoveq	%rax, %rsi
	testl	%ebx, %ebx
	jle	.LBB3_124
# BB#115:                               # %.lr.ph.1.preheader.i
                                        #   in Loop: Header=BB3_105 Depth=1
	leaq	8(%r8), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r8,%rax,4), %r9
	leaq	(%r8,%r10,4), %rbp
	xorl	%r13d, %r13d
	movq	%rsi, %r11
.LBB3_116:                              # %.lr.ph.1.i
                                        #   Parent Loop BB3_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	216(%rsp,%r10,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB3_119
# BB#117:                               #   in Loop: Header=BB3_116 Depth=2
	movl	-8(%rdi), %eax
	movl	%eax, (%r11)
	movl	-4(%rdi), %eax
	movl	%eax, 4(%r11)
	incl	%r13d
	testb	$8, %r13b
	jne	.LBB3_127
# BB#118:                               # %._crit_edge182.i
                                        #   in Loop: Header=BB3_116 Depth=2
	addq	$8, %r11
	movss	(%rbp), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	216(%rsp,%r10,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
.LBB3_119:                              #   in Loop: Header=BB3_116 Depth=2
	cmpl	$1, %ebx
	movq	%r8, %rax
	cmovgq	%rdi, %rax
	ucomiss	%xmm1, %xmm0
	seta	%cl
	movss	(%rax,%r10,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	seta	%dl
	cmpb	%dl, %cl
	je	.LBB3_122
# BB#120:                               #   in Loop: Header=BB3_116 Depth=2
	movss	(%r9), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	(%rax,%r12,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm4
	subss	%xmm1, %xmm2
	divss	%xmm2, %xmm4
	subss	%xmm1, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, (%r11,%r12,4)
	movl	216(%rsp,%r10,4), %eax
	movl	%eax, (%r11,%r10,4)
	incl	%r13d
	testb	$8, %r13b
	jne	.LBB3_127
# BB#121:                               #   in Loop: Header=BB3_116 Depth=2
	addq	$8, %r11
.LBB3_122:                              #   in Loop: Header=BB3_116 Depth=2
	addq	$8, %rdi
	addq	$8, %r9
	addq	$8, %rbp
	cmpl	$1, %ebx
	leal	-1(%rbx), %eax
	movl	%eax, %ebx
	jg	.LBB3_116
	jmp	.LBB3_125
.LBB3_123:                              # %._crit_edge.thread.i
                                        #   in Loop: Header=BB3_105 Depth=1
	leaq	544(%rsp), %rax
	cmpq	%rax, %r8
	movq	%rax, %rsi
	leaq	784(%rsp), %rax
	cmoveq	%rax, %rsi
.LBB3_124:                              #   in Loop: Header=BB3_105 Depth=1
	xorl	%r13d, %r13d
.LBB3_125:                              # %._crit_edge.1.i
                                        #   in Loop: Header=BB3_105 Depth=1
	leaq	544(%rsp), %r8
	cmpq	%r8, %rsi
	leaq	784(%rsp), %rax
	cmoveq	%rax, %r8
	incq	%r10
	decq	32(%rsp)                # 8-byte Folded Spill
	cmpq	$2, %r10
	jl	.LBB3_105
	jmp	.LBB3_127
.LBB3_126:
	movq	%r8, %rsi
	movl	%ebx, %r13d
.LBB3_127:                              # %.thread.i
	leaq	544(%rsp), %rax
	cmpq	%rax, %rsi
	je	.LBB3_129
# BB#128:
	leal	(%r13,%r13), %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	leaq	544(%rsp), %rdi
	callq	memcpy
.LBB3_129:                              # %_ZL18intersectRectQuad2PfS_S_.exit
	testl	%r13d, %r13d
	jle	.LBB3_141
# BB#130:                               # %.lr.ph1218
	movss	112(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	movss	272(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	movss	256(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	subss	%xmm1, %xmm0
	movss	.LCPI3_5(%rip), %xmm10  # xmm10 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm10
	mulss	%xmm10, %xmm4
	movss	%xmm4, 112(%rsp)        # 4-byte Spill
	mulss	%xmm10, %xmm2
	movss	%xmm2, 256(%rsp)        # 4-byte Spill
	mulss	%xmm10, %xmm3
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	mulss	%xmm5, %xmm10
	movslq	64(%rsp), %rax          # 4-byte Folded Reload
	movq	80(%rsp), %rcx          # 8-byte Reload
	movss	(%rcx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movss	360(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movss	364(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	movq	160(%rsp), %rdx         # 8-byte Reload
	movss	(%r14,%rdx,4), %xmm12   # xmm12 = mem[0],zero,zero,zero
	movq	288(%rsp), %rax         # 8-byte Reload
	movss	(%r14,%rax,4), %xmm13   # xmm13 = mem[0],zero,zero,zero
	movss	368(%rsp), %xmm14       # xmm14 = mem[0],zero,zero,zero
	leal	4(%rdx), %eax
	movss	(%r14,%rax,4), %xmm15   # xmm15 = mem[0],zero,zero,zero
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, %eax
	orl	$4, %eax
	movss	(%r14,%rax,4), %xmm9    # xmm9 = mem[0],zero,zero,zero
	orl	$8, %edx
	movss	(%r14,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	orl	$8, %ecx
	movss	(%r14,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movl	%r13d, %eax
	leaq	548(%rsp), %rcx
	xorl	%r12d, %r12d
.LBB3_131:                              # =>This Inner Loop Header: Depth=1
	movss	-4(%rcx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm5
	subss	96(%rsp), %xmm5         # 4-byte Folded Reload
	movaps	%xmm10, %xmm0
	mulss	%xmm5, %xmm0
	movaps	%xmm6, %xmm4
	subss	8(%rsp), %xmm4          # 4-byte Folded Reload
	movss	256(%rsp), %xmm8        # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm8
	subss	%xmm8, %xmm0
	mulss	16(%rsp), %xmm5         # 4-byte Folded Reload
	mulss	112(%rsp), %xmm4        # 4-byte Folded Reload
	subss	%xmm5, %xmm4
	movaps	%xmm0, %xmm5
	mulss	%xmm12, %xmm5
	addss	80(%rsp), %xmm5         # 4-byte Folded Reload
	movaps	%xmm4, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm5, %xmm2
	movaps	%xmm0, %xmm5
	mulss	%xmm15, %xmm5
	addss	64(%rsp), %xmm5         # 4-byte Folded Reload
	movaps	%xmm4, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm5, %xmm7
	leal	(%r12,%r12,2), %edx
	movslq	%edx, %rdx
	movss	%xmm2, 784(%rsp,%rdx,4)
	movss	%xmm7, 788(%rsp,%rdx,4)
	mulss	%xmm1, %xmm0
	addss	%xmm14, %xmm0
	mulss	%xmm3, %xmm4
	addss	%xmm0, %xmm4
	movss	%xmm4, 792(%rsp,%rdx,4)
	mulss	192(%rsp), %xmm2        # 16-byte Folded Reload
	movss	788(%rsp,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	mulss	128(%rsp), %xmm0        # 16-byte Folded Reload
	addss	%xmm2, %xmm0
	mulss	224(%rsp), %xmm4        # 16-byte Folded Reload
	addss	%xmm0, %xmm4
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm0
	movslq	%r12d, %rdx
	movss	%xmm0, 656(%rsp,%rdx,4)
	ucomiss	.LCPI3_8, %xmm0
	jb	.LBB3_133
# BB#132:                               #   in Loop: Header=BB3_131 Depth=1
	leal	(%r12,%r12), %edx
	movslq	%edx, %rdx
	movss	%xmm11, 544(%rsp,%rdx,4)
	leal	1(%r12,%r12), %edx
	movslq	%edx, %rdx
	movss	%xmm6, 544(%rsp,%rdx,4)
	incl	%r12d
.LBB3_133:                              #   in Loop: Header=BB3_131 Depth=1
	addq	$8, %rcx
	decq	%rax
	jne	.LBB3_131
# BB#134:                               # %._crit_edge1219
	testl	%r12d, %r12d
	jle	.LBB3_141
# BB#135:
	movl	1064(%rsp), %eax
	cmpl	%eax, %r12d
	cmovlel	%r12d, %eax
	testl	%eax, %eax
	movl	$1, %r13d
	cmovgl	%eax, %r13d
	cmpl	%r13d, %r12d
	jle	.LBB3_142
# BB#136:
	xorl	%ecx, %ecx
	cmpl	$2, %r12d
	jl	.LBB3_148
# BB#137:                               # %.lr.ph1209.preheader
	movss	656(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movl	%r12d, %eax
	leal	3(%r12), %edi
	leaq	-2(%rax), %rsi
	andq	$3, %rdi
	je	.LBB3_145
# BB#138:                               # %.lr.ph1209.prol.preheader
	negq	%rdi
	xorl	%ecx, %ecx
	movl	$1, %edx
	movaps	%xmm0, %xmm1
.LBB3_139:                              # %.lr.ph1209.prol
                                        # =>This Inner Loop Header: Depth=1
	movss	656(%rsp,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	cmoval	%edx, %ecx
	maxss	%xmm1, %xmm0
	leaq	1(%rdi,%rdx), %rbp
	incq	%rdx
	cmpq	$1, %rbp
	movaps	%xmm0, %xmm1
	jne	.LBB3_139
	jmp	.LBB3_146
.LBB3_141:
	xorl	%r13d, %r13d
	jmp	.LBB3_152
.LBB3_142:                              # %.lr.ph1213
	movl	%r12d, %r13d
	leaq	792(%rsp), %rbx
	leaq	656(%rsp), %rbp
	movq	1088(%rsp), %r15
	movq	144(%rsp), %r14         # 8-byte Reload
.LBB3_143:                              # =>This Inner Loop Header: Depth=1
	movss	-8(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	(%r14), %xmm0
	movss	%xmm0, 880(%rsp)
	movss	-4(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	4(%r14), %xmm0
	movss	%xmm0, 884(%rsp)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	8(%r14), %xmm0
	movss	%xmm0, 888(%rsp)
	movq	(%r15), %rax
	movq	32(%rax), %rax
	movq	1040(%rsp), %rcx
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm3
	xorps	%xmm3, %xmm0
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 432(%rsp)
	movlps	%xmm2, 440(%rsp)
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm0
	movq	%r15, %rdi
	leaq	432(%rsp), %rsi
	leaq	880(%rsp), %rdx
	callq	*%rax
	addq	$12, %rbx
	addq	$4, %rbp
	decq	%r13
	jne	.LBB3_143
# BB#144:
	movl	%r12d, %r13d
	jmp	.LBB3_151
.LBB3_145:
	xorl	%ecx, %ecx
	movl	$1, %edx
.LBB3_146:                              # %.lr.ph1209.prol.loopexit
	cmpq	$3, %rsi
	jb	.LBB3_148
.LBB3_147:                              # %.lr.ph1209
                                        # =>This Inner Loop Header: Depth=1
	movss	656(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	cmoval	%edx, %ecx
	maxss	%xmm0, %xmm1
	movss	660(%rsp,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	leal	1(%rdx), %esi
	ucomiss	%xmm1, %xmm0
	cmovbel	%ecx, %esi
	maxss	%xmm1, %xmm0
	movss	664(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	leal	2(%rdx), %edi
	ucomiss	%xmm0, %xmm1
	cmovbel	%esi, %edi
	maxss	%xmm0, %xmm1
	movss	668(%rsp,%rdx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	leal	3(%rdx), %ecx
	ucomiss	%xmm1, %xmm0
	cmovbel	%edi, %ecx
	maxss	%xmm1, %xmm0
	addq	$4, %rdx
	cmpq	%rax, %rdx
	jne	.LBB3_147
.LBB3_148:                              # %._crit_edge1210
	leaq	544(%rsp), %rsi
	leaq	880(%rsp), %r8
	movl	%r12d, %edi
	movl	%r13d, %edx
	callq	_Z11cullPoints2iPfiiPi
	testl	%r13d, %r13d
	jle	.LBB3_151
# BB#149:                               # %.lr.ph
	movl	%r13d, %ebp
	xorl	%ebx, %ebx
	leaq	912(%rsp), %r14
	leaq	432(%rsp), %r15
.LBB3_150:                              # =>This Inner Loop Header: Depth=1
	movslq	880(%rsp,%rbx,4), %rax
	leaq	(%rax,%rax,2), %rcx
	movss	784(%rsp,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movq	144(%rsp), %rdx         # 8-byte Reload
	addss	(%rdx), %xmm0
	movss	%xmm0, 432(%rsp)
	movss	788(%rsp,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 436(%rsp)
	movss	792(%rsp,%rcx,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	movss	%xmm0, 440(%rsp)
	movq	1088(%rsp), %rdi
	movq	(%rdi), %rcx
	movq	32(%rcx), %rcx
	movq	1040(%rsp), %rdx
	movsd	(%rdx), %xmm0           # xmm0 = mem[0],zero
	movaps	.LCPI3_3(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm1, %xmm3
	xorps	%xmm3, %xmm0
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 912(%rsp)
	movlps	%xmm2, 920(%rsp)
	movss	656(%rsp,%rax,4), %xmm0 # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm0
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	*%rcx
	incq	%rbx
	cmpq	%rbp, %rbx
	jl	.LBB3_150
.LBB3_151:                              # %.loopexit
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	1056(%rsp), %rcx
	movl	%eax, (%rcx)
.LBB3_152:
	movl	%r13d, %eax
	addq	$984, %rsp              # imm = 0x3D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE, .Lfunc_end3-_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE
	.cfi_endproc

	.globl	_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.p2align	4, 0x90
	.type	_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb,@function
_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb: # @_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
	subq	$272, %rsp              # imm = 0x110
.Lcfi31:
	.cfi_def_cfa_offset 320
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r13, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r15
	movl	(%r13), %eax
	movl	%eax, 176(%rsp)
	movl	64(%r13), %eax
	movl	%eax, 128(%rsp)
	movl	4(%r13), %eax
	movl	%eax, 180(%rsp)
	movl	68(%r13), %eax
	movl	%eax, 132(%rsp)
	movl	8(%r13), %eax
	movl	%eax, 184(%rsp)
	movl	72(%r13), %eax
	movl	%eax, 136(%rsp)
	movl	16(%r13), %eax
	movl	%eax, 192(%rsp)
	movl	80(%r13), %eax
	movl	%eax, 144(%rsp)
	movl	20(%r13), %eax
	movl	%eax, 196(%rsp)
	movl	84(%r13), %eax
	movl	%eax, 148(%rsp)
	movl	24(%r13), %eax
	movl	%eax, 200(%rsp)
	movl	88(%r13), %eax
	movl	%eax, 152(%rsp)
	movl	32(%r13), %eax
	movl	%eax, 208(%rsp)
	movl	96(%r13), %eax
	movl	%eax, 160(%rsp)
	movl	36(%r13), %eax
	movl	%eax, 212(%rsp)
	movl	100(%r13), %eax
	movl	%eax, 164(%rsp)
	movl	40(%r13), %eax
	movl	%eax, 216(%rsp)
	movl	104(%r13), %eax
	movl	%eax, 168(%rsp)
	movq	8(%r15), %rbx
	movsd	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 60(%rsp)         # 4-byte Spill
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	unpcklps	64(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	addps	96(%rsp), %xmm1         # 16-byte Folded Reload
	addss	60(%rsp), %xmm0         # 4-byte Folded Reload
	addps	%xmm1, %xmm1
	movaps	%xmm1, %xmm2
	addss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 240(%rsp)
	movlps	%xmm1, 248(%rsp)
	leaq	48(%r13), %r12
	leaq	112(%r13), %r13
	movq	16(%r15), %rbx
	movsd	40(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movss	48(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 60(%rsp)         # 4-byte Spill
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*88(%rax)
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	unpcklps	64(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	addps	96(%rsp), %xmm1         # 16-byte Folded Reload
	addss	60(%rsp), %xmm0         # 4-byte Folded Reload
	addps	%xmm1, %xmm1
	movaps	%xmm1, %xmm2
	addss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 224(%rsp)
	movlps	%xmm1, 232(%rsp)
	movq	%r14, 48(%rsp)
	leaq	120(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	124(%rsp), %rax
	movq	%rax, 8(%rsp)
	leaq	256(%rsp), %rax
	movq	%rax, (%rsp)
	movl	$4, 24(%rsp)
	leaq	176(%rsp), %rsi
	leaq	240(%rsp), %rdx
	leaq	128(%rsp), %r8
	leaq	224(%rsp), %r9
	movq	%r12, %rdi
	movq	%r13, %rcx
	callq	_Z8dBoxBox2RK9btVector3PKfS1_S1_S3_S1_RS_PfPiiP12dContactGeomiRN36btDiscreteCollisionDetectorInterface6ResultE
	addq	$272, %rsp              # imm = 0x110
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb, .Lfunc_end4-_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.cfi_endproc

	.section	.text._ZN36btDiscreteCollisionDetectorInterfaceD2Ev,"axG",@progbits,_ZN36btDiscreteCollisionDetectorInterfaceD2Ev,comdat
	.weak	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.p2align	4, 0x90
	.type	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev,@function
_ZN36btDiscreteCollisionDetectorInterfaceD2Ev: # @_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end5:
	.size	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev, .Lfunc_end5-_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.cfi_endproc

	.section	.text._ZN16btBoxBoxDetectorD0Ev,"axG",@progbits,_ZN16btBoxBoxDetectorD0Ev,comdat
	.weak	_ZN16btBoxBoxDetectorD0Ev
	.p2align	4, 0x90
	.type	_ZN16btBoxBoxDetectorD0Ev,@function
_ZN16btBoxBoxDetectorD0Ev:              # @_ZN16btBoxBoxDetectorD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end6:
	.size	_ZN16btBoxBoxDetectorD0Ev, .Lfunc_end6-_ZN16btBoxBoxDetectorD0Ev
	.cfi_endproc

	.type	_ZTV16btBoxBoxDetector,@object # @_ZTV16btBoxBoxDetector
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btBoxBoxDetector
	.p2align	3
_ZTV16btBoxBoxDetector:
	.quad	0
	.quad	_ZTI16btBoxBoxDetector
	.quad	_ZN36btDiscreteCollisionDetectorInterfaceD2Ev
	.quad	_ZN16btBoxBoxDetectorD0Ev
	.quad	_ZN16btBoxBoxDetector16getClosestPointsERKN36btDiscreteCollisionDetectorInterface17ClosestPointInputERNS0_6ResultEP12btIDebugDrawb
	.size	_ZTV16btBoxBoxDetector, 40

	.type	_ZTS16btBoxBoxDetector,@object # @_ZTS16btBoxBoxDetector
	.globl	_ZTS16btBoxBoxDetector
	.p2align	4
_ZTS16btBoxBoxDetector:
	.asciz	"16btBoxBoxDetector"
	.size	_ZTS16btBoxBoxDetector, 19

	.type	_ZTS36btDiscreteCollisionDetectorInterface,@object # @_ZTS36btDiscreteCollisionDetectorInterface
	.section	.rodata._ZTS36btDiscreteCollisionDetectorInterface,"aG",@progbits,_ZTS36btDiscreteCollisionDetectorInterface,comdat
	.weak	_ZTS36btDiscreteCollisionDetectorInterface
	.p2align	4
_ZTS36btDiscreteCollisionDetectorInterface:
	.asciz	"36btDiscreteCollisionDetectorInterface"
	.size	_ZTS36btDiscreteCollisionDetectorInterface, 39

	.type	_ZTI36btDiscreteCollisionDetectorInterface,@object # @_ZTI36btDiscreteCollisionDetectorInterface
	.section	.rodata._ZTI36btDiscreteCollisionDetectorInterface,"aG",@progbits,_ZTI36btDiscreteCollisionDetectorInterface,comdat
	.weak	_ZTI36btDiscreteCollisionDetectorInterface
	.p2align	3
_ZTI36btDiscreteCollisionDetectorInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS36btDiscreteCollisionDetectorInterface
	.size	_ZTI36btDiscreteCollisionDetectorInterface, 16

	.type	_ZTI16btBoxBoxDetector,@object # @_ZTI16btBoxBoxDetector
	.section	.rodata,"a",@progbits
	.globl	_ZTI16btBoxBoxDetector
	.p2align	4
_ZTI16btBoxBoxDetector:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS16btBoxBoxDetector
	.quad	_ZTI36btDiscreteCollisionDetectorInterface
	.size	_ZTI16btBoxBoxDetector, 24


	.globl	_ZN16btBoxBoxDetectorC1EP10btBoxShapeS1_
	.type	_ZN16btBoxBoxDetectorC1EP10btBoxShapeS1_,@function
_ZN16btBoxBoxDetectorC1EP10btBoxShapeS1_ = _ZN16btBoxBoxDetectorC2EP10btBoxShapeS1_
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
