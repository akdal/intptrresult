	.text
	.file	"jfdctint.bc"
	.globl	jpeg_fdct_islow
	.p2align	4, 0x90
	.type	jpeg_fdct_islow,@function
jpeg_fdct_islow:                        # @jpeg_fdct_islow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
.Lcfi4:
	.cfi_offset %rbx, -40
.Lcfi5:
	.cfi_offset %r14, -32
.Lcfi6:
	.cfi_offset %r15, -24
.Lcfi7:
	.cfi_offset %rbp, -16
	movl	$8, %r8d
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movl	28(%rcx), %edx
	movl	(%rcx), %esi
	movl	4(%rcx), %eax
	leal	(%rdx,%rsi), %ebx
	movslq	%ebx, %r14
	subl	%edx, %esi
	movslq	%esi, %r9
	movl	24(%rcx), %edx
	leal	(%rdx,%rax), %esi
	movslq	%esi, %rsi
	subl	%edx, %eax
	movslq	%eax, %r10
	movl	8(%rcx), %eax
	movl	20(%rcx), %edx
	leal	(%rdx,%rax), %ebx
	movslq	%ebx, %rbx
	subl	%edx, %eax
	movslq	%eax, %r11
	movl	12(%rcx), %eax
	movl	16(%rcx), %edx
	leal	(%rdx,%rax), %ebp
	movslq	%ebp, %rbp
	subl	%edx, %eax
	movslq	%eax, %rdx
	leal	(%rbp,%r14), %eax
	subq	%rbp, %r14
	leal	(%rbx,%rsi), %ebp
	subq	%rbx, %rsi
	leal	(%rax,%rbp), %ebx
	shll	$2, %ebx
	movl	%ebx, (%rcx)
	subl	%ebp, %eax
	shll	$2, %eax
	movl	%eax, 16(%rcx)
	leaq	(%r14,%rsi), %rax
	imulq	$4433, %rax, %rax       # imm = 0x1151
	imulq	$6270, %r14, %rbx       # imm = 0x187E
	leaq	1024(%rbx,%rax), %rbx
	shrq	$11, %rbx
	movl	%ebx, 8(%rcx)
	imulq	$-15137, %rsi, %rsi     # imm = 0xC4DF
	leaq	1024(%rsi,%rax), %rax
	shrq	$11, %rax
	movl	%eax, 24(%rcx)
	leaq	(%rdx,%r10), %rax
	leaq	(%r11,%r9), %rsi
	leaq	(%rax,%rsi), %rbx
	imulq	$9633, %rbx, %r15       # imm = 0x25A1
	imulq	$2446, %rdx, %rbp       # imm = 0x98E
	leaq	(%rdx,%r9), %r14
	imulq	$16819, %r11, %rdx      # imm = 0x41B3
	leaq	(%r11,%r10), %r11
	imulq	$25172, %r10, %rbx      # imm = 0x6254
	imulq	$12299, %r9, %r9        # imm = 0x300B
	imulq	$-7373, %r14, %r10      # imm = 0xE333
	imulq	$-20995, %r11, %r11     # imm = 0xADFD
	imulq	$-16069, %rax, %rax     # imm = 0xC13B
	imulq	$-3196, %rsi, %rsi      # imm = 0xF384
	addq	%r15, %rax
	addq	%r15, %rsi
	addq	%r10, %rbp
	leaq	1024(%rbp,%rax), %rbp
	shrq	$11, %rbp
	movl	%ebp, 28(%rcx)
	addq	%r11, %rdx
	leaq	1024(%rdx,%rsi), %rdx
	shrq	$11, %rdx
	movl	%edx, 20(%rcx)
	addq	%r11, %rbx
	leaq	1024(%rbx,%rax), %rax
	shrq	$11, %rax
	movl	%eax, 12(%rcx)
	addq	%r10, %r9
	leaq	1024(%r9,%rsi), %rax
	shrq	$11, %rax
	movl	%eax, 4(%rcx)
	addq	$32, %rcx
	decl	%r8d
	jg	.LBB0_1
# BB#2:                                 # %.preheader.preheader
	movl	$8, %r8d
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	224(%rdi), %eax
	movl	(%rdi), %ecx
	movl	32(%rdi), %edx
	leal	(%rax,%rcx), %esi
	movslq	%esi, %rsi
	subl	%eax, %ecx
	movslq	%ecx, %r9
	movl	192(%rdi), %eax
	leal	(%rax,%rdx), %ecx
	movslq	%ecx, %rcx
	subl	%eax, %edx
	movslq	%edx, %r10
	movl	64(%rdi), %eax
	movl	160(%rdi), %edx
	leal	(%rdx,%rax), %ebp
	movslq	%ebp, %rbp
	subl	%edx, %eax
	movslq	%eax, %r11
	movl	96(%rdi), %edx
	movl	128(%rdi), %ebx
	leal	(%rbx,%rdx), %eax
	cltq
	subl	%ebx, %edx
	movslq	%edx, %rdx
	leaq	(%rax,%rsi), %rbx
	subq	%rax, %rsi
	leaq	(%rbp,%rcx), %rax
	subq	%rbp, %rcx
	leaq	2(%rax,%rbx), %rbp
	shrq	$2, %rbp
	movl	%ebp, (%rdi)
	movl	$2, %ebp
	subq	%rax, %rbp
	addq	%rbx, %rbp
	shrq	$2, %rbp
	movl	%ebp, 128(%rdi)
	leaq	(%rsi,%rcx), %rax
	imulq	$4433, %rax, %rax       # imm = 0x1151
	imulq	$6270, %rsi, %rsi       # imm = 0x187E
	leaq	16384(%rsi,%rax), %rsi
	shrq	$15, %rsi
	movl	%esi, 64(%rdi)
	imulq	$-15137, %rcx, %rcx     # imm = 0xC4DF
	leaq	16384(%rcx,%rax), %rax
	shrq	$15, %rax
	movl	%eax, 192(%rdi)
	leaq	(%rdx,%r10), %rax
	leaq	(%r11,%r9), %rcx
	leaq	(%rax,%rcx), %rsi
	imulq	$9633, %rsi, %rsi       # imm = 0x25A1
	imulq	$2446, %rdx, %rbx       # imm = 0x98E
	leaq	(%rdx,%r9), %r14
	imulq	$16819, %r11, %rbp      # imm = 0x41B3
	leaq	(%r11,%r10), %r11
	imulq	$25172, %r10, %rdx      # imm = 0x6254
	imulq	$12299, %r9, %r9        # imm = 0x300B
	imulq	$-7373, %r14, %r10      # imm = 0xE333
	imulq	$-20995, %r11, %r11     # imm = 0xADFD
	imulq	$-16069, %rax, %rax     # imm = 0xC13B
	imulq	$-3196, %rcx, %rcx      # imm = 0xF384
	addq	%rsi, %rax
	addq	%rsi, %rcx
	addq	%r10, %rbx
	leaq	16384(%rbx,%rax), %rsi
	shrq	$15, %rsi
	movl	%esi, 224(%rdi)
	addq	%r11, %rbp
	leaq	16384(%rbp,%rcx), %rsi
	shrq	$15, %rsi
	movl	%esi, 160(%rdi)
	addq	%r11, %rdx
	leaq	16384(%rdx,%rax), %rax
	shrq	$15, %rax
	movl	%eax, 96(%rdi)
	addq	%r10, %r9
	leaq	16384(%r9,%rcx), %rax
	shrq	$15, %rax
	movl	%eax, 32(%rdi)
	addq	$4, %rdi
	decl	%r8d
	jg	.LBB0_3
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_fdct_islow, .Lfunc_end0-jpeg_fdct_islow
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
