	.text
	.file	"life.bc"
	.globl	_ZN6living4sumsEPA40_PS_Pi
	.p2align	4, 0x90
	.type	_ZN6living4sumsEPA40_PS_Pi,@function
_ZN6living4sumsEPA40_PS_Pi:             # @_ZN6living4sumsEPA40_PS_Pi
	.cfi_startproc
# BB#0:                                 # %.preheader
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r14)
	movslq	8(%rbx), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movslq	12(%rbx), %rcx
	movq	-328(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movslq	12(%rbx), %rcx
	movq	-320(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movslq	12(%rbx), %rcx
	movq	-312(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	movslq	12(%rbx), %rcx
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movq	-8(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	movslq	12(%rbx), %rcx
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movq	(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	movslq	12(%rbx), %rcx
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movq	8(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movslq	12(%rbx), %rcx
	movq	312(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movslq	12(%rbx), %rcx
	movq	320(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	movslq	8(%rbx), %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$6, %rax
	addq	%r15, %rax
	movslq	12(%rbx), %rcx
	movq	328(%rax,%rcx,8), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %eax
	incl	(%r14,%rax,4)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN6living4sumsEPA40_PS_Pi, .Lfunc_end0-_ZN6living4sumsEPA40_PS_Pi
	.cfi_endproc

	.globl	_ZN5grass4nextEPA40_P6living
	.p2align	4, 0x90
	.type	_ZN5grass4nextEPA40_P6living,@function
_ZN5grass4nextEPA40_P6living:           # @_ZN5grass4nextEPA40_P6living
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 48
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	%rsp, %rdx
	callq	_ZN6living4sumsEPA40_PS_Pi
	movl	4(%rsp), %eax
	cmpl	8(%rsp), %eax
	movl	$_ZTV5grass+16, %eax
	movl	$_ZTV5empty+16, %ebx
	cmovgq	%rax, %rbx
	movl	$16, %edi
	callq	_Znwm
	movl	8(%r14), %ecx
	movl	12(%r14), %edx
	movl	%ecx, 8(%rax)
	movl	%edx, 12(%rax)
	movq	%rbx, (%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN5grass4nextEPA40_P6living, .Lfunc_end1-_ZN5grass4nextEPA40_P6living
	.cfi_endproc

	.globl	_ZN6rabbit4nextEPA40_P6living
	.p2align	4, 0x90
	.type	_ZN6rabbit4nextEPA40_P6living,@function
_ZN6rabbit4nextEPA40_P6living:          # @_ZN6rabbit4nextEPA40_P6living
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsp, %rdx
	callq	_ZN6living4sumsEPA40_PS_Pi
	movl	12(%rsp), %eax
	cmpl	8(%rsp), %eax
	jge	.LBB2_1
# BB#2:
	movl	16(%rbx), %ebp
	cmpl	$4, %ebp
	jl	.LBB2_3
.LBB2_1:
	movl	$16, %edi
	callq	_Znwm
	movl	8(%rbx), %ecx
	movl	12(%rbx), %edx
	movl	%ecx, 8(%rax)
	movl	%edx, 12(%rax)
	movq	$_ZTV5empty+16, (%rax)
	jmp	.LBB2_4
.LBB2_3:
	movl	$24, %edi
	callq	_Znwm
	movl	8(%rbx), %ecx
	movl	12(%rbx), %edx
	incl	%ebp
	movl	%ecx, 8(%rax)
	movl	%edx, 12(%rax)
	movq	$_ZTV6rabbit+16, (%rax)
	movl	%ebp, 16(%rax)
.LBB2_4:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_ZN6rabbit4nextEPA40_P6living, .Lfunc_end2-_ZN6rabbit4nextEPA40_P6living
	.cfi_endproc

	.globl	_ZN3fox4nextEPA40_P6living
	.p2align	4, 0x90
	.type	_ZN3fox4nextEPA40_P6living,@function
_ZN3fox4nextEPA40_P6living:             # @_ZN3fox4nextEPA40_P6living
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 48
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsp, %rdx
	callq	_ZN6living4sumsEPA40_PS_Pi
	cmpl	$6, 12(%rsp)
	jge	.LBB3_1
# BB#2:
	movl	16(%rbx), %ebp
	cmpl	$9, %ebp
	jl	.LBB3_3
.LBB3_1:
	movl	$16, %edi
	callq	_Znwm
	movl	8(%rbx), %ecx
	movl	12(%rbx), %edx
	movl	%ecx, 8(%rax)
	movl	%edx, 12(%rax)
	movq	$_ZTV5empty+16, (%rax)
	jmp	.LBB3_4
.LBB3_3:
	movl	$24, %edi
	callq	_Znwm
	movl	8(%rbx), %ecx
	movl	12(%rbx), %edx
	incl	%ebp
	movl	%ecx, 8(%rax)
	movl	%edx, 12(%rax)
	movq	$_ZTV3fox+16, (%rax)
	movl	%ebp, 16(%rax)
.LBB3_4:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN3fox4nextEPA40_P6living, .Lfunc_end3-_ZN3fox4nextEPA40_P6living
	.cfi_endproc

	.globl	_ZN5empty4nextEPA40_P6living
	.p2align	4, 0x90
	.type	_ZN5empty4nextEPA40_P6living,@function
_ZN5empty4nextEPA40_P6living:           # @_ZN5empty4nextEPA40_P6living
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsp, %rdx
	callq	_ZN6living4sumsEPA40_PS_Pi
	cmpl	$2, 12(%rsp)
	jl	.LBB4_2
# BB#1:
	movl	$24, %edi
	callq	_Znwm
	movl	8(%rbx), %ecx
	movl	12(%rbx), %edx
	movl	%ecx, 8(%rax)
	movl	%edx, 12(%rax)
	movq	$_ZTV3fox+16, (%rax)
	movl	$0, 16(%rax)
	jmp	.LBB4_7
.LBB4_2:
	cmpl	$2, 8(%rsp)
	jl	.LBB4_4
# BB#3:
	movl	$24, %edi
	callq	_Znwm
	movl	8(%rbx), %ecx
	movl	12(%rbx), %edx
	movl	%ecx, 8(%rax)
	movl	%edx, 12(%rax)
	movq	$_ZTV6rabbit+16, (%rax)
	movl	$0, 16(%rax)
	jmp	.LBB4_7
.LBB4_4:
	movl	4(%rsp), %ebp
	movl	$16, %edi
	callq	_Znwm
	cmpl	$0, %ebp
	movq	8(%rbx), %rcx
	movq	%rcx, 8(%rax)
	je	.LBB4_6
# BB#5:
	movq	$_ZTV5grass+16, (%rax)
	jmp	.LBB4_7
.LBB4_6:
	movq	$_ZTV5empty+16, (%rax)
.LBB4_7:
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN5empty4nextEPA40_P6living, .Lfunc_end4-_ZN5empty4nextEPA40_P6living
	.cfi_endproc

	.globl	_Z4initPA40_P6living
	.p2align	4, 0x90
	.type	_Z4initPA40_P6living,@function
_Z4initPA40_P6living:                   # @_Z4initPA40_P6living
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r12, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_2 Depth 2
	movq	%r14, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                #   Parent Loop BB5_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%ebx, 12(%rax)
	movq	$_ZTV5empty+16, (%rax)
	movq	%rax, (%r12)
	incq	%rbx
	addq	$8, %r12
	cmpq	$40, %rbx
	jne	.LBB5_2
# BB#3:                                 #   in Loop: Header=BB5_1 Depth=1
	incq	%r15
	addq	$320, %r14              # imm = 0x140
	cmpq	$40, %r15
	jne	.LBB5_1
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_Z4initPA40_P6living, .Lfunc_end5-_Z4initPA40_P6living
	.cfi_endproc

	.globl	_Z6updatePA40_P6livingS2_
	.p2align	4, 0x90
	.type	_Z6updatePA40_P6livingS2_,@function
_Z6updatePA40_P6livingS2_:              # @_Z6updatePA40_P6livingS2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 64
.Lcfi42:
	.cfi_offset %rbx, -56
.Lcfi43:
	.cfi_offset %r12, -48
.Lcfi44:
	.cfi_offset %r13, -40
.Lcfi45:
	.cfi_offset %r14, -32
.Lcfi46:
	.cfi_offset %r15, -24
.Lcfi47:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	$1, %r15d
	movl	$41, %r12d
	.p2align	4, 0x90
.LBB6_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_2 Depth 2
	movl	$38, %ebx
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB6_2:                                #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13,%rbp,8), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	callq	*8(%rax)
	movq	%rax, (%r14,%rbp,8)
	incq	%rbp
	decq	%rbx
	jne	.LBB6_2
# BB#3:                                 #   in Loop: Header=BB6_1 Depth=1
	incq	%r15
	addq	$40, %r12
	cmpq	$39, %r15
	jne	.LBB6_1
# BB#4:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_Z6updatePA40_P6livingS2_, .Lfunc_end6-_Z6updatePA40_P6livingS2_
	.cfi_endproc

	.globl	_Z4delePA40_P6living
	.p2align	4, 0x90
	.type	_Z4delePA40_P6living,@function
_Z4delePA40_P6living:                   # @_Z4delePA40_P6living
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 48
.Lcfi53:
	.cfi_offset %rbx, -40
.Lcfi54:
	.cfi_offset %r12, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	addq	$328, %r14              # imm = 0x148
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB7_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
	movl	$38, %r12d
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=2
	callq	_ZdlPv
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=2
	addq	$8, %rbx
	decq	%r12
	jne	.LBB7_2
# BB#5:                                 #   in Loop: Header=BB7_1 Depth=1
	incq	%r15
	addq	$320, %r14              # imm = 0x140
	cmpq	$39, %r15
	jne	.LBB7_1
# BB#6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_Z4delePA40_P6living, .Lfunc_end7-_Z4delePA40_P6living
	.cfi_endproc

	.globl	_Z4edenPA40_P6living
	.p2align	4, 0x90
	.type	_Z4edenPA40_P6living,@function
_Z4edenPA40_P6living:                   # @_Z4edenPA40_P6living
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 48
.Lcfi62:
	.cfi_offset %rbx, -40
.Lcfi63:
	.cfi_offset %r12, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB8_1:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_2 Depth 2
	movq	%r14, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                #   Parent Loop BB8_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, %r15
	jne	.LBB8_4
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=2
	movl	$24, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%r15d, 12(%rax)
	movq	$_ZTV3fox+16, (%rax)
	movl	$0, 16(%rax)
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_4:                                #   in Loop: Header=BB8_2 Depth=2
	jge	.LBB8_6
# BB#5:                                 #   in Loop: Header=BB8_2 Depth=2
	movl	$24, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%ebx, 12(%rax)
	movq	$_ZTV6rabbit+16, (%rax)
	movl	$0, 16(%rax)
	jmp	.LBB8_7
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_2 Depth=2
	movl	$16, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%ebx, 12(%rax)
	movq	$_ZTV5grass+16, (%rax)
.LBB8_7:                                #   in Loop: Header=BB8_2 Depth=2
	movq	%rax, (%r12)
	incq	%rbx
	addq	$8, %r12
	cmpq	$40, %rbx
	jne	.LBB8_2
# BB#8:                                 #   in Loop: Header=BB8_1 Depth=1
	incq	%r15
	addq	$320, %r14              # imm = 0x140
	cmpq	$40, %r15
	jne	.LBB8_1
# BB#9:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_Z4edenPA40_P6living, .Lfunc_end8-_Z4edenPA40_P6living
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$25624, %rsp            # imm = 0x6418
.Lcfi72:
	.cfi_def_cfa_offset 25680
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	leaq	12816(%rsp), %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB9_1:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_2 Depth 2
	movq	%r14, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_2:                                #   Parent Loop BB9_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%ebp, 12(%rax)
	movq	$_ZTV5empty+16, (%rax)
	movq	%rax, (%rbx)
	incq	%rbp
	addq	$8, %rbx
	cmpq	$40, %rbp
	jne	.LBB9_2
# BB#3:                                 #   in Loop: Header=BB9_1 Depth=1
	incq	%r15
	addq	$320, %r14              # imm = 0x140
	cmpq	$40, %r15
	jne	.LBB9_1
# BB#4:                                 # %_Z4initPA40_P6living.exit
	leaq	16(%rsp), %r14
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB9_5:                                # %.preheader.i5
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_6 Depth 2
	movq	%r14, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_6:                                #   Parent Loop BB9_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbp, %r15
	jne	.LBB9_8
# BB#7:                                 #   in Loop: Header=BB9_6 Depth=2
	movl	$24, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%r15d, 12(%rax)
	movq	$_ZTV3fox+16, (%rax)
	movl	$0, 16(%rax)
	jmp	.LBB9_11
	.p2align	4, 0x90
.LBB9_8:                                #   in Loop: Header=BB9_6 Depth=2
	jge	.LBB9_10
# BB#9:                                 #   in Loop: Header=BB9_6 Depth=2
	movl	$24, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%ebp, 12(%rax)
	movq	$_ZTV6rabbit+16, (%rax)
	movl	$0, 16(%rax)
	jmp	.LBB9_11
	.p2align	4, 0x90
.LBB9_10:                               #   in Loop: Header=BB9_6 Depth=2
	movl	$16, %edi
	callq	_Znwm
	movl	%r15d, 8(%rax)
	movl	%ebp, 12(%rax)
	movq	$_ZTV5grass+16, (%rax)
.LBB9_11:                               #   in Loop: Header=BB9_6 Depth=2
	movq	%rax, (%rbx)
	incq	%rbp
	addq	$8, %rbx
	cmpq	$40, %rbp
	jne	.LBB9_6
# BB#12:                                #   in Loop: Header=BB9_5 Depth=1
	incq	%r15
	addq	$320, %r14              # imm = 0x140
	cmpq	$40, %r15
	jne	.LBB9_5
# BB#13:                                # %_Z4edenPA40_P6living.exit.preheader.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB9_14:                               # %_Z4edenPA40_P6living.exit.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_16 Depth 2
                                        #       Child Loop BB9_17 Depth 3
                                        #     Child Loop BB9_20 Depth 2
                                        #       Child Loop BB9_21 Depth 3
                                        #     Child Loop BB9_26 Depth 2
                                        #       Child Loop BB9_27 Depth 3
                                        #     Child Loop BB9_30 Depth 2
                                        #       Child Loop BB9_31 Depth 3
	testb	$1, %r12b
	movl	%r12d, 4(%rsp)          # 4-byte Spill
	jne	.LBB9_15
# BB#25:                                # %.preheader.i18.preheader
                                        #   in Loop: Header=BB9_14 Depth=1
	leaq	344(%rsp), %r15
	movq	%r15, %rax
	leaq	13144(%rsp), %r13
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB9_26:                               # %.preheader.i18
                                        #   Parent Loop BB9_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_27 Depth 3
	movl	$38, %r12d
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, %rbp
	movq	%r13, %rbx
	leaq	16(%rsp), %r15
	.p2align	4, 0x90
.LBB9_27:                               #   Parent Loop BB9_14 Depth=1
                                        #     Parent Loop BB9_26 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*8(%rax)
	movq	%rax, (%rbx)
	addq	$8, %rbx
	addq	$8, %rbp
	decq	%r12
	jne	.LBB9_27
# BB#28:                                #   in Loop: Header=BB9_26 Depth=2
	incq	%r14
	addq	$320, %r13              # imm = 0x140
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$320, %rax              # imm = 0x140
	cmpq	$39, %r14
	jne	.LBB9_26
# BB#29:                                # %.preheader.i26.preheader
                                        #   in Loop: Header=BB9_14 Depth=1
	leaq	344(%rsp), %r14
	movl	$1, %r15d
	movl	4(%rsp), %r12d          # 4-byte Reload
	.p2align	4, 0x90
.LBB9_30:                               # %.preheader.i26
                                        #   Parent Loop BB9_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_31 Depth 3
	movl	$38, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB9_31:                               #   Parent Loop BB9_14 Depth=1
                                        #     Parent Loop BB9_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_33
# BB#32:                                #   in Loop: Header=BB9_31 Depth=3
	callq	_ZdlPv
.LBB9_33:                               #   in Loop: Header=BB9_31 Depth=3
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB9_31
# BB#34:                                #   in Loop: Header=BB9_30 Depth=2
	incq	%r15
	addq	$320, %r14              # imm = 0x140
	cmpq	$39, %r15
	jne	.LBB9_30
	jmp	.LBB9_35
	.p2align	4, 0x90
.LBB9_15:                               # %.preheader.i9.preheader
                                        #   in Loop: Header=BB9_14 Depth=1
	leaq	13144(%rsp), %rbp
	leaq	344(%rsp), %r12
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB9_16:                               # %.preheader.i9
                                        #   Parent Loop BB9_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_17 Depth 3
	movl	$38, %r13d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r12, %r15
	leaq	12816(%rsp), %r14
	.p2align	4, 0x90
.LBB9_17:                               #   Parent Loop BB9_14 Depth=1
                                        #     Parent Loop BB9_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*8(%rax)
	movq	%rax, (%r15)
	addq	$8, %r15
	addq	$8, %rbp
	decq	%r13
	jne	.LBB9_17
# BB#18:                                #   in Loop: Header=BB9_16 Depth=2
	incq	%rbx
	addq	$320, %r12              # imm = 0x140
	movq	8(%rsp), %rbp           # 8-byte Reload
	addq	$320, %rbp              # imm = 0x140
	cmpq	$39, %rbx
	jne	.LBB9_16
# BB#19:                                # %.preheader.i13.preheader
                                        #   in Loop: Header=BB9_14 Depth=1
	leaq	13144(%rsp), %r14
	movl	$1, %r15d
	movl	4(%rsp), %r12d          # 4-byte Reload
	.p2align	4, 0x90
.LBB9_20:                               # %.preheader.i13
                                        #   Parent Loop BB9_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_21 Depth 3
	movl	$38, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB9_21:                               #   Parent Loop BB9_14 Depth=1
                                        #     Parent Loop BB9_20 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_23
# BB#22:                                #   in Loop: Header=BB9_21 Depth=3
	callq	_ZdlPv
.LBB9_23:                               #   in Loop: Header=BB9_21 Depth=3
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB9_21
# BB#24:                                #   in Loop: Header=BB9_20 Depth=2
	incq	%r15
	addq	$320, %r14              # imm = 0x140
	cmpq	$39, %r15
	jne	.LBB9_20
.LBB9_35:                               # %_Z4delePA40_P6living.exit
                                        #   in Loop: Header=BB9_14 Depth=1
	incl	%r12d
	cmpl	$10000, %r12d           # imm = 0x2710
	jne	.LBB9_14
# BB#36:
	xorl	%eax, %eax
	addq	$25624, %rsp            # imm = 0x6418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	main, .Lfunc_end9-main
	.cfi_endproc

	.section	.text._ZN3fox3whoEv,"axG",@progbits,_ZN3fox3whoEv,comdat
	.weak	_ZN3fox3whoEv
	.p2align	4, 0x90
	.type	_ZN3fox3whoEv,@function
_ZN3fox3whoEv:                          # @_ZN3fox3whoEv
	.cfi_startproc
# BB#0:
	movl	$3, %eax
	retq
.Lfunc_end10:
	.size	_ZN3fox3whoEv, .Lfunc_end10-_ZN3fox3whoEv
	.cfi_endproc

	.section	.text._ZN6rabbit3whoEv,"axG",@progbits,_ZN6rabbit3whoEv,comdat
	.weak	_ZN6rabbit3whoEv
	.p2align	4, 0x90
	.type	_ZN6rabbit3whoEv,@function
_ZN6rabbit3whoEv:                       # @_ZN6rabbit3whoEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end11:
	.size	_ZN6rabbit3whoEv, .Lfunc_end11-_ZN6rabbit3whoEv
	.cfi_endproc

	.section	.text._ZN5grass3whoEv,"axG",@progbits,_ZN5grass3whoEv,comdat
	.weak	_ZN5grass3whoEv
	.p2align	4, 0x90
	.type	_ZN5grass3whoEv,@function
_ZN5grass3whoEv:                        # @_ZN5grass3whoEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end12:
	.size	_ZN5grass3whoEv, .Lfunc_end12-_ZN5grass3whoEv
	.cfi_endproc

	.section	.text._ZN5empty3whoEv,"axG",@progbits,_ZN5empty3whoEv,comdat
	.weak	_ZN5empty3whoEv
	.p2align	4, 0x90
	.type	_ZN5empty3whoEv,@function
_ZN5empty3whoEv:                        # @_ZN5empty3whoEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end13:
	.size	_ZN5empty3whoEv, .Lfunc_end13-_ZN5empty3whoEv
	.cfi_endproc

	.type	_ZTV3fox,@object        # @_ZTV3fox
	.section	.rodata,"a",@progbits
	.globl	_ZTV3fox
	.p2align	3
_ZTV3fox:
	.quad	0
	.quad	_ZTI3fox
	.quad	_ZN3fox3whoEv
	.quad	_ZN3fox4nextEPA40_P6living
	.size	_ZTV3fox, 32

	.type	_ZTS3fox,@object        # @_ZTS3fox
	.globl	_ZTS3fox
_ZTS3fox:
	.asciz	"3fox"
	.size	_ZTS3fox, 5

	.type	_ZTS6living,@object     # @_ZTS6living
	.section	.rodata._ZTS6living,"aG",@progbits,_ZTS6living,comdat
	.weak	_ZTS6living
_ZTS6living:
	.asciz	"6living"
	.size	_ZTS6living, 8

	.type	_ZTI6living,@object     # @_ZTI6living
	.section	.rodata._ZTI6living,"aG",@progbits,_ZTI6living,comdat
	.weak	_ZTI6living
	.p2align	3
_ZTI6living:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS6living
	.size	_ZTI6living, 16

	.type	_ZTI3fox,@object        # @_ZTI3fox
	.section	.rodata,"a",@progbits
	.globl	_ZTI3fox
	.p2align	4
_ZTI3fox:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS3fox
	.quad	_ZTI6living
	.size	_ZTI3fox, 24

	.type	_ZTV6rabbit,@object     # @_ZTV6rabbit
	.globl	_ZTV6rabbit
	.p2align	3
_ZTV6rabbit:
	.quad	0
	.quad	_ZTI6rabbit
	.quad	_ZN6rabbit3whoEv
	.quad	_ZN6rabbit4nextEPA40_P6living
	.size	_ZTV6rabbit, 32

	.type	_ZTS6rabbit,@object     # @_ZTS6rabbit
	.globl	_ZTS6rabbit
_ZTS6rabbit:
	.asciz	"6rabbit"
	.size	_ZTS6rabbit, 8

	.type	_ZTI6rabbit,@object     # @_ZTI6rabbit
	.globl	_ZTI6rabbit
	.p2align	4
_ZTI6rabbit:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS6rabbit
	.quad	_ZTI6living
	.size	_ZTI6rabbit, 24

	.type	_ZTV5grass,@object      # @_ZTV5grass
	.globl	_ZTV5grass
	.p2align	3
_ZTV5grass:
	.quad	0
	.quad	_ZTI5grass
	.quad	_ZN5grass3whoEv
	.quad	_ZN5grass4nextEPA40_P6living
	.size	_ZTV5grass, 32

	.type	_ZTS5grass,@object      # @_ZTS5grass
	.globl	_ZTS5grass
_ZTS5grass:
	.asciz	"5grass"
	.size	_ZTS5grass, 7

	.type	_ZTI5grass,@object      # @_ZTI5grass
	.globl	_ZTI5grass
	.p2align	4
_ZTI5grass:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS5grass
	.quad	_ZTI6living
	.size	_ZTI5grass, 24

	.type	_ZTV5empty,@object      # @_ZTV5empty
	.globl	_ZTV5empty
	.p2align	3
_ZTV5empty:
	.quad	0
	.quad	_ZTI5empty
	.quad	_ZN5empty3whoEv
	.quad	_ZN5empty4nextEPA40_P6living
	.size	_ZTV5empty, 32

	.type	_ZTS5empty,@object      # @_ZTS5empty
	.globl	_ZTS5empty
_ZTS5empty:
	.asciz	"5empty"
	.size	_ZTS5empty, 7

	.type	_ZTI5empty,@object      # @_ZTI5empty
	.globl	_ZTI5empty
	.p2align	4
_ZTI5empty:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS5empty
	.quad	_ZTI6living
	.size	_ZTI5empty, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
