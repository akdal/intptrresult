	.text
	.file	"bitstrng.bc"
	.globl	bitstring
	.p2align	4, 0x90
	.type	bitstring,@function
bitstring:                              # @bitstring
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	%ebx, %eax
	sarl	$2, %eax
	xorl	%edx, %edx
	testb	$3, %bl
	sete	%dl
	addl	%ebx, %eax
	movl	%ecx, %esi
	subl	%eax, %esi
	addl	%edx, %esi
	testl	%esi, %esi
	jle	.LBB0_2
# BB#1:                                 # %.lr.ph27.preheader
	leal	-1(%rdx,%rcx), %r12d
	subl	%eax, %r12d
	leaq	1(%r12), %rdx
	movl	$32, %esi
	movq	%r15, %rdi
	callq	memset
	leaq	1(%r15,%r12), %r15
.LBB0_2:                                # %.preheader
	testl	%ebx, %ebx
	jle	.LBB0_3
# BB#4:                                 # %.lr.ph.preheader
	movl	%ebx, %eax
	incq	%rax
	decl	%ebx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdx
	movl	%ebx, %ecx
	shrq	%cl, %rdx
	andb	$1, %dl
	orb	$48, %dl
	leaq	1(%r15), %rcx
	movb	%dl, (%r15)
	testl	%ebx, %ebx
	je	.LBB0_7
# BB#6:                                 # %.lr.ph
                                        #   in Loop: Header=BB0_5 Depth=1
	movl	%ebx, %edx
	andl	$3, %edx
	jne	.LBB0_7
# BB#9:                                 #   in Loop: Header=BB0_5 Depth=1
	movb	$32, 1(%r15)
	addq	$2, %r15
	movq	%r15, %rcx
.LBB0_7:                                # %.backedge
                                        #   in Loop: Header=BB0_5 Depth=1
	decq	%rax
	decl	%ebx
	cmpq	$1, %rax
	movq	%rcx, %r15
	jg	.LBB0_5
	jmp	.LBB0_8
.LBB0_3:
	movq	%r15, %rcx
.LBB0_8:                                # %._crit_edge
	movb	$0, (%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	bitstring, .Lfunc_end0-bitstring
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
