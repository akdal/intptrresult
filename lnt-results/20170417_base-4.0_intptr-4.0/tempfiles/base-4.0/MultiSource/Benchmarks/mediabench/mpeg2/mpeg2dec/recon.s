	.text
	.file	"recon.bc"
	.globl	form_predictions
	.p2align	4, 0x90
	.type	form_predictions,@function
form_predictions:                       # @form_predictions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 128
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movl	%ecx, %r8d
	movl	%edx, %r13d
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%edi, %r14d
	movslq	136(%rsp), %rbp
	imulq	$1431655766, %rbp, %rbx # imm = 0x55555556
	movq	%rbx, %rax
	shrq	$63, %rax
	shrq	$32, %rbx
	addl	%eax, %ebx
	leal	(%rbx,%rbx,2), %eax
	movl	%ebp, %r15d
	subl	%eax, %r15d
	movl	%r13d, %eax
	andl	$8, %eax
	movl	picture_coding_type(%rip), %edx
	jne	.LBB0_5
# BB#1:
	cmpl	$2, %edx
	je	.LBB0_5
# BB#2:
	movl	%r15d, %r12d
	movl	%ebx, %ebp
.LBB0_3:
	testb	$4, %r13b
	jne	.LBB0_44
.LBB0_4:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
	movq	128(%rsp), %rsi
	movl	picture_structure(%rip), %r12d
	cmpl	$3, %r12d
	jne	.LBB0_14
# BB#6:
	testl	%eax, %eax
	sete	%al
	cmpl	$2, %r8d
	je	.LBB0_18
# BB#7:
	testb	%al, %al
	jne	.LBB0_18
# BB#8:
	cmpl	$3, %r8d
	je	.LBB0_34
# BB#9:
	cmpl	$1, %r8d
	jne	.LBB0_38
# BB#10:
	cmpl	$1, %r15d
	jg	.LBB0_12
# BB#11:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	16(%rsp), %r11          # 8-byte Reload
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	sarl	%r11d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %r10d
	movl	4(%rax), %eax
	sarl	%eax
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$forward_reference_frame, %edi
	movl	$0, %edx
	movl	$8, %r9d
	movl	%r8d, %r12d
	movl	%ecx, %r8d
	pushq	%r15
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	movl	%r12d, %r8d
	addq	$48, %rsp
.Lcfi19:
	.cfi_adjust_cfa_offset -48
.LBB0_12:
	movl	$1, %r12d
	cmpl	$5, %ebp
	movl	$1, %ebp
	jg	.LBB0_3
# BB#13:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	8(%rax), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	sarl	%eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	16(%rdx), %r10d
	movl	20(%rdx), %ebp
	sarl	%ebp
	subq	$8, %rsp
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	movl	$1, %r12d
	movl	$forward_reference_frame, %edi
	movl	$1, %edx
	movl	$8, %r9d
	movl	%r8d, %r15d
	movl	%ecx, %r8d
	pushq	%rbx
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	movl	%r15d, %r8d
	addq	$48, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_41
.LBB0_14:
	movq	%r14, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	cmpl	$2, %r12d
	sete	%r14b
	movl	Second_Field(%rip), %ecx
	cmpl	$2, %edx
	jne	.LBB0_17
# BB#15:
	testl	%ecx, %ecx
	je	.LBB0_17
# BB#16:
	movq	32(%rsp), %rdx          # 8-byte Reload
	cmpl	(%rdx), %r14d
	jne	.LBB0_22
.LBB0_17:
	movl	$forward_reference_frame, %edi
	jmp	.LBB0_23
.LBB0_18:
	cmpl	$1, %r15d
	jg	.LBB0_20
# BB#19:
	movl	Coded_Picture_Width(%rip), %ecx
	movl	%r8d, %r12d
	leal	(%rcx,%rcx), %r8d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %r10d
	movl	4(%rax), %eax
	subq	$8, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	movl	$forward_reference_frame, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$8, %r9d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%r15
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	movl	%r12d, %r8d
	addq	$48, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -48
.LBB0_20:
	movl	$1, %r12d
	cmpl	$5, %ebp
	movl	$1, %ebp
	jg	.LBB0_3
# BB#21:
	movl	Coded_Picture_Width(%rip), %ecx
	movl	%r8d, %r15d
	leal	(%rcx,%rcx), %r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	movl	4(%rdx), %ebp
	subq	$8, %rsp
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	movl	$1, %r12d
	movl	$forward_reference_frame, %edi
	movl	$1, %esi
	movl	$1, %edx
	movl	$8, %r9d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%rbx
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi37:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	movl	%r15d, %r8d
	addq	$48, %rsp
.Lcfi40:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_41
.LBB0_22:
	movl	$backward_reference_frame, %edi
.LBB0_23:
	testl	%eax, %eax
	sete	%al
	cmpl	$1, %r8d
	je	.LBB0_32
# BB#24:
	testb	%al, %al
	jne	.LBB0_32
# BB#25:
	cmpl	$3, %r8d
	je	.LBB0_39
# BB#26:
	cmpl	$2, %r8d
	jne	.LBB0_42
# BB#27:
	movl	$1, %r12d
	cmpl	$1, %r15d
	jg	.LBB0_43
# BB#28:
	movl	%r8d, 28(%rsp)          # 4-byte Spill
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	movl	4(%rdx), %ebp
	subq	$8, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	movl	$0, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	%r15
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset -48
	movl	8(%rbx), %esi
	cmpl	$2, picture_coding_type(%rip)
	jne	.LBB0_59
# BB#29:
	movl	Second_Field(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_59
# BB#30:
	cmpl	%esi, %r14d
	je	.LBB0_60
# BB#31:
	movl	$backward_reference_frame, %edi
	jmp	.LBB0_61
.LBB0_32:
	movl	$1, %r12d
	cmpl	$1, %r15d
	jg	.LBB0_43
# BB#33:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	movl	4(%rdx), %ebp
	subq	$8, %rsp
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	movl	$0, %edx
	movl	$16, %r9d
	movl	%r8d, %ebx
	movl	%ecx, %r8d
	pushq	%r15
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	movq	80(%rsp), %r14          # 8-byte Reload
	pushq	%r14
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	movl	%ebx, %r8d
	addq	$48, %rsp
.Lcfi54:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_41
.LBB0_34:
	movl	%r8d, 28(%rsp)          # 4-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	sarl	%ecx
	leaq	48(%rsp), %rdi
	callq	Dual_Prime_Arithmetic
	cmpl	$1, %r15d
	jg	.LBB0_36
# BB#35:
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	16(%rsp), %rbx          # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	sarl	%ebx
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %r10d
	movl	4(%rax), %eax
	sarl	%eax
	subq	$8, %rsp
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	movl	$forward_reference_frame, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	$0
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi61:
	.cfi_adjust_cfa_offset -48
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movl	48(%rsp), %r10d
	movl	52(%rsp), %eax
	subq	$8, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	movl	$forward_reference_frame, %edi
	movl	$1, %esi
	xorl	%edx, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	$1
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi68:
	.cfi_adjust_cfa_offset -48
.LBB0_36:
	movl	$1, %r12d
	cmpl	$5, %ebp
	movl	$1, %ebp
	movl	28(%rsp), %r8d          # 4-byte Reload
	jg	.LBB0_3
# BB#37:
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	16(%rsp), %rbx          # 8-byte Reload
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	sarl	%ebx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	(%rdx), %eax
	movl	4(%rdx), %ebp
	sarl	%ebp
	subq	$8, %rsp
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	movl	$1, %r12d
	movl	$forward_reference_frame, %edi
	movl	$1, %esi
	movl	$1, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	$0
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi72:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi75:
	.cfi_adjust_cfa_offset -48
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movl	56(%rsp), %eax
	movl	60(%rsp), %ebp
	subq	$8, %rsp
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	movl	$forward_reference_frame, %edi
	xorl	%esi, %esi
	movl	$1, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	$1
.Lcfi77:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi78:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	movl	76(%rsp), %r8d          # 4-byte Reload
	addq	$48, %rsp
.Lcfi82:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_41
.LBB0_38:
	movl	$.Lstr.2, %edi
	movl	%r8d, %ebx
	callq	puts
	movl	%ebx, %r8d
	jmp	.LBB0_40
.LBB0_39:
	testl	%ecx, %ecx
	movl	$forward_reference_frame, %eax
	movl	$backward_reference_frame, %r15d
	cmoveq	%rax, %r15
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	(%rbx), %edx
	movl	4(%rbx), %ecx
	leaq	48(%rsp), %rdi
	movl	%r8d, 28(%rsp)          # 4-byte Spill
	callq	Dual_Prime_Arithmetic
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movl	(%rbx), %eax
	movl	4(%rbx), %ebp
	subq	$8, %rsp
.Lcfi83:
	.cfi_adjust_cfa_offset 8
	movl	$forward_reference_frame, %edi
	movl	$0, %edx
	movl	$16, %r9d
	movl	%r14d, %esi
	movl	%ecx, %r8d
	pushq	$0
.Lcfi84:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi85:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi86:
	.cfi_adjust_cfa_offset 8
	movq	48(%rsp), %rbx          # 8-byte Reload
	pushq	%rbx
.Lcfi87:
	.cfi_adjust_cfa_offset 8
	movq	80(%rsp), %r14          # 8-byte Reload
	pushq	%r14
.Lcfi88:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi89:
	.cfi_adjust_cfa_offset -48
	xorl	%esi, %esi
	cmpl	$2, %r12d
	setne	%sil
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movl	48(%rsp), %eax
	movl	52(%rsp), %ebp
	subq	$8, %rsp
.Lcfi90:
	.cfi_adjust_cfa_offset 8
	xorl	%edx, %edx
	movl	$16, %r9d
	movq	%r15, %rdi
	movl	%ecx, %r8d
	pushq	$1
.Lcfi91:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi92:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi93:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	movl	76(%rsp), %r8d          # 4-byte Reload
	addq	$48, %rsp
.Lcfi96:
	.cfi_adjust_cfa_offset -48
.LBB0_40:
	movl	$1, %r12d
.LBB0_41:
	movl	$1, %ebp
	testb	$4, %r13b
	je	.LBB0_4
	jmp	.LBB0_44
.LBB0_42:
	movl	$.Lstr.2, %edi
	movl	%r8d, %ebx
	callq	puts
	movl	%ebx, %r8d
	movl	$1, %r12d
.LBB0_43:
	movl	$1, %ebp
	movq	40(%rsp), %r14          # 8-byte Reload
	testb	$4, %r13b
	je	.LBB0_4
.LBB0_44:
	cmpl	$3, picture_structure(%rip)
	jne	.LBB0_50
# BB#45:
	cmpl	$2, %r8d
	jne	.LBB0_53
# BB#46:
	cmpl	$1, %r12d
	jg	.LBB0_48
# BB#47:
	movl	Coded_Picture_Width(%rip), %ecx
	leal	(%rcx,%rcx), %r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	8(%rdx), %eax
	movl	12(%rdx), %ebx
	subq	$8, %rsp
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	movl	$backward_reference_frame, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$8, %r9d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%r12
.Lcfi98:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi103:
	.cfi_adjust_cfa_offset -48
.LBB0_48:
	cmpl	$1, %ebp
	jg	.LBB0_4
# BB#49:
	movl	Coded_Picture_Width(%rip), %ecx
	leal	(%rcx,%rcx), %r8d
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	8(%rdx), %eax
	movl	12(%rdx), %ebx
	subq	$8, %rsp
.Lcfi104:
	.cfi_adjust_cfa_offset 8
	movl	$backward_reference_frame, %edi
	movl	$1, %esi
	movl	$1, %edx
	movl	$8, %r9d
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	pushq	%rbp
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi109:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi110:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_4
.LBB0_50:
	cmpl	$2, %r8d
	je	.LBB0_57
# BB#51:
	cmpl	$1, %r8d
	jne	.LBB0_58
# BB#52:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	8(%rdx), %eax
	movl	12(%rdx), %ebp
	subq	$8, %rsp
.Lcfi111:
	.cfi_adjust_cfa_offset 8
	movl	$backward_reference_frame, %edi
	movl	$0, %edx
	movl	$16, %r9d
	movl	%ecx, %r8d
	pushq	%r12
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi114:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi115:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi116:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi117:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_4
.LBB0_53:
	cmpl	$1, %r12d
	jg	.LBB0_55
# BB#54:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	sarl	%eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	8(%rdx), %r10d
	movl	12(%rdx), %ebx
	sarl	%ebx
	subq	$8, %rsp
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	movl	$backward_reference_frame, %edi
	movl	$0, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	%r12
.Lcfi119:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi124:
	.cfi_adjust_cfa_offset -48
.LBB0_55:
	cmpl	$1, %ebp
	jg	.LBB0_4
# BB#56:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	sarl	%eax
	movq	8(%rsp), %rdx           # 8-byte Reload
	movl	24(%rdx), %r10d
	movl	28(%rdx), %ebx
	sarl	%ebx
	subq	$8, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset 8
	movl	$backward_reference_frame, %edi
	movl	$1, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	%rbp
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi128:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi131:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_4
.LBB0_57:
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	4(%r15), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	8(%rbp), %eax
	movl	12(%rbp), %ebx
	subq	$8, %rsp
.Lcfi132:
	.cfi_adjust_cfa_offset 8
	movl	$backward_reference_frame, %edi
	movl	$0, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	%r12
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi134:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi135:
	.cfi_adjust_cfa_offset 8
	movq	48(%rsp), %rbx          # 8-byte Reload
	pushq	%rbx
.Lcfi136:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi137:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi138:
	.cfi_adjust_cfa_offset -48
	movl	12(%r15), %esi
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	addl	$8, %ebx
	movl	24(%rbp), %eax
	movl	28(%rbp), %ebp
	subq	$8, %rsp
.Lcfi139:
	.cfi_adjust_cfa_offset 8
	movl	$backward_reference_frame, %edi
	xorl	%edx, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	%r12
.Lcfi140:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi141:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi142:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi143:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi144:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi145:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB0_4
.LBB0_58:
	movl	$.Lstr.2, %edi
	callq	puts
	jmp	.LBB0_4
.LBB0_59:
	movl	%esi, %r14d
.LBB0_60:                               # %._crit_edge
	movl	$forward_reference_frame, %edi
	movl	%r14d, %esi
.LBB0_61:
	movl	Coded_Picture_Width(%rip), %ecx
	addl	%ecx, %ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	8(%rax), %r10d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	16(%rax), %ebp
	movl	20(%rax), %eax
	subq	$8, %rsp
.Lcfi146:
	.cfi_adjust_cfa_offset 8
	movl	$0, %edx
	movl	$8, %r9d
	movl	%ecx, %r8d
	pushq	%r15
.Lcfi147:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi148:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi149:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi150:
	.cfi_adjust_cfa_offset 8
	movq	80(%rsp), %r14          # 8-byte Reload
	pushq	%r14
.Lcfi151:
	.cfi_adjust_cfa_offset 8
	callq	form_prediction
	addq	$48, %rsp
.Lcfi152:
	.cfi_adjust_cfa_offset -48
	movl	$1, %ebp
	movl	28(%rsp), %r8d          # 4-byte Reload
	testb	$4, %r13b
	je	.LBB0_4
	jmp	.LBB0_44
.Lfunc_end0:
	.size	form_predictions, .Lfunc_end0-form_predictions
	.cfi_endproc

	.p2align	4, 0x90
	.type	form_prediction,@function
form_prediction:                        # @form_prediction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi156:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi157:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi158:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi159:
	.cfi_def_cfa_offset 112
.Lcfi160:
	.cfi_offset %rbx, -56
.Lcfi161:
	.cfi_offset %r12, -48
.Lcfi162:
	.cfi_offset %r13, -40
.Lcfi163:
	.cfi_offset %r14, -32
.Lcfi164:
	.cfi_offset %r15, -24
.Lcfi165:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movl	%ecx, %r14d
	movl	%esi, %ecx
	movq	%rdi, %r15
	movl	144(%rsp), %r10d
	movl	136(%rsp), %r12d
	movl	128(%rsp), %r11d
	movl	120(%rsp), %ebp
	movl	112(%rsp), %ebx
	movl	%r13d, %eax
	sarl	%eax
	xorl	%esi, %esi
	movl	%ecx, 44(%rsp)          # 4-byte Spill
	testl	%ecx, %ecx
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cltq
	movl	$0, %edi
	cmovneq	%rax, %rdi
	addq	(%r15), %rdi
	movl	%edx, 24(%rsp)          # 4-byte Spill
	testl	%edx, %edx
	cmovneq	%rax, %rsi
	addq	current_frame(%rip), %rsi
	subq	$8, %rsp
.Lcfi166:
	.cfi_adjust_cfa_offset 8
	movl	$16, 28(%rsp)           # 4-byte Folded Spill
	movl	$16, %r8d
	movl	%r14d, %edx
	movl	%r13d, %ecx
	movl	%r9d, 24(%rsp)          # 4-byte Spill
	pushq	%r10
.Lcfi167:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi168:
	.cfi_adjust_cfa_offset 8
	movq	%r11, 56(%rsp)          # 8-byte Spill
	pushq	%r11
.Lcfi169:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi170:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi171:
	.cfi_adjust_cfa_offset 8
	callq	form_component_prediction
	addq	$48, %rsp
.Lcfi172:
	.cfi_adjust_cfa_offset -48
	movl	chroma_format(%rip), %eax
	cmpl	$3, %eax
	jne	.LBB1_2
# BB#1:
	movq	%r12, %r10
	movq	%rbp, %r11
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	%r13d, %ecx
	jmp	.LBB1_6
.LBB1_2:
	sarl	%r14d
	sarl	%ebx
	movl	128(%rsp), %ecx
	movq	%rcx, %rdx
	shrl	$31, %ecx
	addl	%ecx, %edx
	sarl	%edx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movl	$8, 20(%rsp)            # 4-byte Folded Spill
	cmpl	$1, %eax
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	jne	.LBB1_3
# BB#4:
	sarl	16(%rsp)                # 4-byte Folded Spill
	sarl	%ebp
	movq	%rbp, %r11
	movl	%r12d, %eax
	shrl	$31, %eax
	addl	%r12d, %eax
	sarl	%eax
	movl	%eax, %r10d
	jmp	.LBB1_5
.LBB1_3:
	movq	%r12, %r10
	movq	%rbp, %r11
.LBB1_5:                                # %.thread
	movl	12(%rsp), %ecx          # 4-byte Reload
.LBB1_6:                                # %.thread
	movl	%ecx, %eax
	sarl	%eax
	xorl	%ebp, %ebp
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	cltq
	movl	$0, %ebx
	cmovneq	%rax, %rbx
	movq	8(%r15), %rdi
	addq	%rbx, %rdi
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	current_frame+8(%rip), %rsi
	addq	%rbp, %rsi
	subq	$8, %rsp
.Lcfi173:
	.cfi_adjust_cfa_offset 8
	movl	%r14d, %r12d
	movl	%r12d, %edx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movl	28(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	movl	152(%rsp), %eax
	pushq	%rax
.Lcfi174:
	.cfi_adjust_cfa_offset 8
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movq	%r10, %r15
	pushq	%r15
.Lcfi175:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	movq	%r11, %r14
	pushq	%r14
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	movq	88(%rsp), %r13          # 8-byte Reload
	pushq	%r13
.Lcfi178:
	.cfi_adjust_cfa_offset 8
	callq	form_component_prediction
	addq	$48, %rsp
.Lcfi179:
	.cfi_adjust_cfa_offset -48
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	16(%rax), %rbx
	addq	current_frame+16(%rip), %rbp
	subq	$8, %rsp
.Lcfi180:
	.cfi_adjust_cfa_offset 8
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%r12d, %edx
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	28(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	movl	152(%rsp), %eax
	pushq	%rax
.Lcfi181:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi182:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi183:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi184:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi185:
	.cfi_adjust_cfa_offset 8
	callq	form_component_prediction
	addq	$104, %rsp
.Lcfi186:
	.cfi_adjust_cfa_offset -48
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	form_prediction, .Lfunc_end1-form_prediction
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI2_1:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
.LCPI2_2:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.text
	.p2align	4, 0x90
	.type	form_component_prediction,@function
form_component_prediction:              # @form_component_prediction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi189:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi190:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi191:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi192:
	.cfi_def_cfa_offset 56
.Lcfi193:
	.cfi_offset %rbx, -56
.Lcfi194:
	.cfi_offset %r12, -48
.Lcfi195:
	.cfi_offset %r13, -40
.Lcfi196:
	.cfi_offset %r14, -32
.Lcfi197:
	.cfi_offset %r15, -24
.Lcfi198:
	.cfi_offset %rbp, -16
	movl	%r9d, -108(%rsp)        # 4-byte Spill
	movl	%r8d, -112(%rsp)        # 4-byte Spill
	movl	72(%rsp), %r11d
	movl	64(%rsp), %ebp
	movl	80(%rsp), %r9d
	movl	%r11d, %ebx
	sarl	%ebx
	movl	%r9d, %eax
	sarl	%eax
	addl	%ebp, %eax
	imull	%edx, %eax
	movslq	%eax, %r14
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	leaq	(%rdi,%r14), %rax
	movslq	56(%rsp), %r15
	addq	%r15, %rax
	movslq	%ebx, %rdi
	movq	%rdi, -104(%rsp)        # 8-byte Spill
	addq	%rdi, %rax
	imull	%edx, %ebp
	movslq	%ebp, %rdi
	movq	%rsi, -64(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	leaq	(%rsi,%rdi), %r8
	addq	%r15, %r8
	movl	%r9d, %esi
	orl	%r11d, %esi
	testb	$1, %sil
	movl	88(%rsp), %esi
	jne	.LBB2_20
# BB#1:
	testl	%esi, %esi
	je	.LBB2_57
# BB#2:                                 # %.preheader224
	movl	-108(%rsp), %r9d        # 4-byte Reload
	testl	%r9d, %r9d
	jle	.LBB2_146
# BB#3:                                 # %.preheader223.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#4:                                 # %.preheader223.us.preheader
	movslq	%ecx, %r12
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %edx
	addq	%r15, %rbp
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rdx), %rsi
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rsi        # 8-byte Reload
	addq	%r15, %rsi
	addq	%r14, %rsi
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	leaq	(%rsi,%rdx), %r15
	leaq	-1(%rdx), %r13
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$15, %ecx
	movq	%rdx, %rbp
	movq	%rcx, -80(%rsp)         # 8-byte Spill
	subq	%rcx, %rbp
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader223.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_13 Depth 2
                                        #     Child Loop BB2_18 Depth 2
	cmpl	$16, -112(%rsp)         # 4-byte Folded Reload
	jae	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB2_15
	.p2align	4, 0x90
.LBB2_7:                                # %min.iters.checked551
                                        #   in Loop: Header=BB2_5 Depth=1
	testq	%rbp, %rbp
	je	.LBB2_11
# BB#8:                                 # %vector.memcheck566
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	%r12, %rcx
	imulq	%r11, %rcx
	movq	-88(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx), %rsi
	movq	-64(%rsp), %rbx         # 8-byte Reload
	addq	%rbx, %rsi
	leaq	(%r15,%rcx), %rdi
	movq	-72(%rsp), %r10         # 8-byte Reload
	addq	%r10, %rdi
	cmpq	%rdi, %rsi
	jae	.LBB2_12
# BB#9:                                 # %vector.memcheck566
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	-96(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx), %rsi
	addq	%rbx, %rsi
	addq	-104(%rsp), %rcx        # 8-byte Folded Reload
	addq	%r10, %rcx
	cmpq	%rsi, %rcx
	jae	.LBB2_12
.LBB2_11:                               #   in Loop: Header=BB2_5 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB2_15
.LBB2_12:                               # %vector.body547.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_13:                               # %vector.body547
                                        #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r8,%rcx), %xmm0
	movdqu	(%rax,%rcx), %xmm1
	pavgb	%xmm0, %xmm1
	movdqu	%xmm1, (%r8,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rbp
	jne	.LBB2_13
# BB#14:                                # %middle.block548
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpl	$0, -80(%rsp)           # 4-byte Folded Reload
	movq	%rbp, %r14
	je	.LBB2_19
	.p2align	4, 0x90
.LBB2_15:                               # %scalar.ph549.preheader
                                        #   in Loop: Header=BB2_5 Depth=1
	movl	%edx, %ecx
	subl	%r14d, %ecx
	testb	$1, %cl
	movq	%r14, %r10
	je	.LBB2_17
# BB#16:                                # %scalar.ph549.prol
                                        #   in Loop: Header=BB2_5 Depth=1
	movzbl	(%r8,%r14), %ecx
	movzbl	(%rax,%r14), %esi
	leal	1(%rcx,%rsi), %ecx
	shrl	%ecx
	movb	%cl, (%r8,%r14)
	leaq	1(%r14), %r10
.LBB2_17:                               # %scalar.ph549.prol.loopexit
                                        #   in Loop: Header=BB2_5 Depth=1
	cmpq	%r14, %r13
	je	.LBB2_19
	.p2align	4, 0x90
.LBB2_18:                               # %scalar.ph549
                                        #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r8,%r10), %ecx
	movzbl	(%rax,%r10), %esi
	leal	1(%rcx,%rsi), %ecx
	shrl	%ecx
	movb	%cl, (%r8,%r10)
	movzbl	1(%r8,%r10), %ecx
	movzbl	1(%rax,%r10), %esi
	leal	1(%rcx,%rsi), %ecx
	shrl	%ecx
	movb	%cl, 1(%r8,%r10)
	addq	$2, %r10
	cmpq	%r10, %rdx
	jne	.LBB2_18
.LBB2_19:                               # %._crit_edge250.us
                                        #   in Loop: Header=BB2_5 Depth=1
	addq	%r12, %rax
	addq	%r12, %r8
	incq	%r11
	cmpl	%r9d, %r11d
	jne	.LBB2_5
	jmp	.LBB2_146
.LBB2_20:
	andl	$1, %r9d
	andl	$1, %r11d
	jne	.LBB2_39
# BB#21:
	testl	%r9d, %r9d
	je	.LBB2_39
# BB#22:
	testl	%esi, %esi
	movl	-108(%rsp), %esi        # 4-byte Reload
	je	.LBB2_93
# BB#23:                                # %.preheader230
	testl	%esi, %esi
	jle	.LBB2_146
# BB#24:                                # %.preheader229.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#25:                                # %.preheader229.us.preheader
	movslq	%ecx, %r10
	movslq	%edx, %r13
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %esi
	addq	%r15, %rbp
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rsi), %rdx
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rdx        # 8-byte Reload
	addq	%r15, %rdx
	leaq	(%rdx,%r13), %rdi
	addq	%r14, %rdi
	movq	%rdi, -32(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rsi), %rdi
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	addq	%r14, %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$15, %ecx
	movq	%rsi, %r14
	movq	%rcx, -56(%rsp)         # 8-byte Spill
	subq	%rcx, %r14
	negq	%rsi
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	pxor	%xmm10, %xmm10
	movdqa	.LCPI2_0(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movdqa	.LCPI2_1(%rip), %xmm8   # xmm8 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	.p2align	4, 0x90
.LBB2_26:                               # %.preheader229.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_32 Depth 2
                                        #     Child Loop BB2_37 Depth 2
	cmpl	$16, -112(%rsp)         # 4-byte Folded Reload
	jb	.LBB2_35
# BB#28:                                # %min.iters.checked478
                                        #   in Loop: Header=BB2_26 Depth=1
	testq	%r14, %r14
	je	.LBB2_35
# BB#29:                                # %vector.memcheck499
                                        #   in Loop: Header=BB2_26 Depth=1
	movq	%r10, %rcx
	imulq	%r12, %rcx
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %r11
	movq	-64(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %r11
	movq	-80(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx), %rdi
	addq	%rdx, %rdi
	movq	-32(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rbp
	movq	-72(%rsp), %rbx         # 8-byte Reload
	addq	%rbx, %rbp
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rdx
	addq	%rbx, %rdx
	movq	-104(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx), %rsi
	addq	%rbx, %rsi
	addq	-48(%rsp), %rcx         # 8-byte Folded Reload
	addq	%rbx, %rcx
	cmpq	%rdx, %r11
	sbbb	%dl, %dl
	cmpq	%rdi, %rbp
	sbbb	%bpl, %bpl
	andb	%dl, %bpl
	cmpq	%rcx, %r11
	sbbb	%cl, %cl
	cmpq	%rdi, %rsi
	sbbb	%sil, %sil
	testb	$1, %bpl
	jne	.LBB2_35
# BB#30:                                # %vector.memcheck499
                                        #   in Loop: Header=BB2_26 Depth=1
	andb	%sil, %cl
	andb	$1, %cl
	movl	$0, %ebp
	jne	.LBB2_36
# BB#31:                                # %vector.body474.preheader
                                        #   in Loop: Header=BB2_26 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_32:                               # %vector.body474
                                        #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r8,%rbp), %xmm3
	pshufd	$78, %xmm3, %xmm4       # xmm4 = xmm3[2,3,0,1]
	punpcklbw	%xmm10, %xmm3   # xmm3 = xmm3[0],xmm10[0],xmm3[1],xmm10[1],xmm3[2],xmm10[2],xmm3[3],xmm10[3],xmm3[4],xmm10[4],xmm3[5],xmm10[5],xmm3[6],xmm10[6],xmm3[7],xmm10[7]
	movdqa	%xmm3, %xmm11
	punpckhwd	%xmm10, %xmm11  # xmm11 = xmm11[4],xmm10[4],xmm11[5],xmm10[5],xmm11[6],xmm10[6],xmm11[7],xmm10[7]
	punpcklwd	%xmm10, %xmm3   # xmm3 = xmm3[0],xmm10[0],xmm3[1],xmm10[1],xmm3[2],xmm10[2],xmm3[3],xmm10[3]
	punpcklbw	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3],xmm4[4],xmm10[4],xmm4[5],xmm10[5],xmm4[6],xmm10[6],xmm4[7],xmm10[7]
	movdqa	%xmm4, %xmm12
	punpckhwd	%xmm10, %xmm12  # xmm12 = xmm12[4],xmm10[4],xmm12[5],xmm10[5],xmm12[6],xmm10[6],xmm12[7],xmm10[7]
	punpcklwd	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3]
	movdqu	(%rax,%rbp), %xmm1
	pshufd	$78, %xmm1, %xmm7       # xmm7 = xmm1[2,3,0,1]
	punpcklbw	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3],xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	movdqa	%xmm7, %xmm2
	punpcklwd	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3]
	punpckhwd	%xmm10, %xmm7   # xmm7 = xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	punpcklbw	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3],xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	movdqa	%xmm1, %xmm0
	punpcklwd	%xmm10, %xmm0   # xmm0 = xmm0[0],xmm10[0],xmm0[1],xmm10[1],xmm0[2],xmm10[2],xmm0[3],xmm10[3]
	punpckhwd	%xmm10, %xmm1   # xmm1 = xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	leaq	(%r13,%rbp), %rcx
	movdqu	(%rax,%rcx), %xmm5
	pshufd	$78, %xmm5, %xmm6       # xmm6 = xmm5[2,3,0,1]
	punpcklbw	%xmm10, %xmm6   # xmm6 = xmm6[0],xmm10[0],xmm6[1],xmm10[1],xmm6[2],xmm10[2],xmm6[3],xmm10[3],xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	movdqa	%xmm6, %xmm13
	punpcklwd	%xmm10, %xmm13  # xmm13 = xmm13[0],xmm10[0],xmm13[1],xmm10[1],xmm13[2],xmm10[2],xmm13[3],xmm10[3]
	punpckhwd	%xmm10, %xmm6   # xmm6 = xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	punpcklbw	%xmm10, %xmm5   # xmm5 = xmm5[0],xmm10[0],xmm5[1],xmm10[1],xmm5[2],xmm10[2],xmm5[3],xmm10[3],xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	movdqa	%xmm5, %xmm14
	punpcklwd	%xmm10, %xmm14  # xmm14 = xmm14[0],xmm10[0],xmm14[1],xmm10[1],xmm14[2],xmm10[2],xmm14[3],xmm10[3]
	punpckhwd	%xmm10, %xmm5   # xmm5 = xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	paddd	%xmm9, %xmm2
	paddd	%xmm13, %xmm2
	paddd	%xmm9, %xmm7
	paddd	%xmm6, %xmm7
	paddd	%xmm9, %xmm0
	paddd	%xmm14, %xmm0
	paddd	%xmm9, %xmm1
	paddd	%xmm5, %xmm1
	psrld	$1, %xmm1
	psrld	$1, %xmm0
	psrld	$1, %xmm7
	psrld	$1, %xmm2
	paddd	%xmm9, %xmm11
	paddd	%xmm1, %xmm11
	paddd	%xmm9, %xmm3
	paddd	%xmm0, %xmm3
	paddd	%xmm9, %xmm12
	paddd	%xmm7, %xmm12
	paddd	%xmm9, %xmm4
	paddd	%xmm2, %xmm4
	psrld	$1, %xmm4
	psrld	$1, %xmm12
	psrld	$1, %xmm3
	psrld	$1, %xmm11
	pand	%xmm8, %xmm11
	pand	%xmm8, %xmm3
	packuswb	%xmm11, %xmm3
	pand	%xmm8, %xmm12
	pand	%xmm8, %xmm4
	packuswb	%xmm12, %xmm4
	packuswb	%xmm4, %xmm3
	movdqu	%xmm3, (%r8,%rbp)
	addq	$16, %rbp
	cmpq	%rbp, %r14
	jne	.LBB2_32
# BB#33:                                # %middle.block475
                                        #   in Loop: Header=BB2_26 Depth=1
	cmpl	$0, -56(%rsp)           # 4-byte Folded Reload
	movq	%r14, %rbp
	jne	.LBB2_36
	jmp	.LBB2_38
	.p2align	4, 0x90
.LBB2_35:                               #   in Loop: Header=BB2_26 Depth=1
	xorl	%ebp, %ebp
.LBB2_36:                               # %scalar.ph476.preheader
                                        #   in Loop: Header=BB2_26 Depth=1
	leaq	(%r13,%rbp), %rsi
	movq	-96(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rbp), %rcx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_37:                               # %scalar.ph476
                                        #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rbp,%r11), %rdx
	movzbl	(%r8,%rdx), %edi
	movzbl	(%rax,%rdx), %r15d
	leaq	(%rsi,%r11), %r9
	movzbl	(%rax,%r9), %ebx
	leal	1(%r15,%rbx), %ebx
	shrl	%ebx
	leal	1(%rdi,%rbx), %edi
	shrl	%edi
	movb	%dil, (%r8,%rdx)
	incq	%r11
	movq	%rcx, %rdx
	addq	%r11, %rdx
	jne	.LBB2_37
.LBB2_38:                               # %._crit_edge262.us
                                        #   in Loop: Header=BB2_26 Depth=1
	addq	%r10, %rax
	addq	%r10, %r8
	incq	%r12
	cmpl	-108(%rsp), %r12d       # 4-byte Folded Reload
	jne	.LBB2_26
	jmp	.LBB2_146
.LBB2_39:
	testl	%r11d, %r11d
	movl	-108(%rsp), %ebx        # 4-byte Reload
	je	.LBB2_76
# BB#40:
	testl	%r9d, %r9d
	jne	.LBB2_76
# BB#41:
	testl	%esi, %esi
	je	.LBB2_112
# BB#42:                                # %.preheader239
	testl	%ebx, %ebx
	jle	.LBB2_146
# BB#43:                                # %.preheader238.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#44:                                # %.preheader238.us.preheader
	movslq	%ecx, %r13
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %edx
	addq	%r15, %rbp
	movq	%rbp, %r10
	leaq	(%rbp,%rdx), %rsi
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rsi        # 8-byte Reload
	addq	%r15, %rsi
	addq	%r14, %rsi
	movq	%rsi, %r14
	leaq	1(%rdx,%rsi), %r12
	movl	%ecx, %r15d
	andl	$15, %r15d
	movq	%rdx, %r11
	subq	%r15, %r11
	xorl	%ecx, %ecx
	pxor	%xmm10, %xmm10
	movdqa	.LCPI2_0(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movdqa	.LCPI2_1(%rip), %xmm8   # xmm8 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	movq	-72(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_45:                               # %.preheader238.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_53 Depth 2
                                        #     Child Loop BB2_55 Depth 2
	cmpl	$16, -112(%rsp)         # 4-byte Folded Reload
	jae	.LBB2_47
# BB#46:                                #   in Loop: Header=BB2_45 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_55
	.p2align	4, 0x90
.LBB2_47:                               # %min.iters.checked371
                                        #   in Loop: Header=BB2_45 Depth=1
	testq	%r11, %r11
	je	.LBB2_51
# BB#48:                                # %vector.memcheck386
                                        #   in Loop: Header=BB2_45 Depth=1
	movq	%r13, %rsi
	imulq	%rcx, %rsi
	leaq	(%r10,%rsi), %rdi
	movq	-64(%rsp), %rbx         # 8-byte Reload
	addq	%rbx, %rdi
	leaq	(%r12,%rsi), %rbp
	addq	%r9, %rbp
	cmpq	%rbp, %rdi
	jae	.LBB2_52
# BB#49:                                # %vector.memcheck386
                                        #   in Loop: Header=BB2_45 Depth=1
	movq	-88(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rsi), %rdi
	addq	%rbx, %rdi
	addq	%r14, %rsi
	addq	%r9, %rsi
	cmpq	%rdi, %rsi
	jae	.LBB2_52
# BB#50:                                #   in Loop: Header=BB2_45 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_55
.LBB2_51:                               #   in Loop: Header=BB2_45 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_55
.LBB2_52:                               # %vector.body367.preheader
                                        #   in Loop: Header=BB2_45 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_53:                               # %vector.body367
                                        #   Parent Loop BB2_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r8,%rbp), %xmm3
	pshufd	$78, %xmm3, %xmm4       # xmm4 = xmm3[2,3,0,1]
	punpcklbw	%xmm10, %xmm3   # xmm3 = xmm3[0],xmm10[0],xmm3[1],xmm10[1],xmm3[2],xmm10[2],xmm3[3],xmm10[3],xmm3[4],xmm10[4],xmm3[5],xmm10[5],xmm3[6],xmm10[6],xmm3[7],xmm10[7]
	movdqa	%xmm3, %xmm11
	punpckhwd	%xmm10, %xmm11  # xmm11 = xmm11[4],xmm10[4],xmm11[5],xmm10[5],xmm11[6],xmm10[6],xmm11[7],xmm10[7]
	punpcklwd	%xmm10, %xmm3   # xmm3 = xmm3[0],xmm10[0],xmm3[1],xmm10[1],xmm3[2],xmm10[2],xmm3[3],xmm10[3]
	punpcklbw	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3],xmm4[4],xmm10[4],xmm4[5],xmm10[5],xmm4[6],xmm10[6],xmm4[7],xmm10[7]
	movdqa	%xmm4, %xmm12
	punpckhwd	%xmm10, %xmm12  # xmm12 = xmm12[4],xmm10[4],xmm12[5],xmm10[5],xmm12[6],xmm10[6],xmm12[7],xmm10[7]
	punpcklwd	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3]
	movdqu	(%rax,%rbp), %xmm5
	movdqu	1(%rax,%rbp), %xmm7
	pshufd	$78, %xmm5, %xmm6       # xmm6 = xmm5[2,3,0,1]
	punpcklbw	%xmm10, %xmm6   # xmm6 = xmm6[0],xmm10[0],xmm6[1],xmm10[1],xmm6[2],xmm10[2],xmm6[3],xmm10[3],xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	movdqa	%xmm6, %xmm13
	punpcklwd	%xmm10, %xmm13  # xmm13 = xmm13[0],xmm10[0],xmm13[1],xmm10[1],xmm13[2],xmm10[2],xmm13[3],xmm10[3]
	punpckhwd	%xmm10, %xmm6   # xmm6 = xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	punpcklbw	%xmm10, %xmm5   # xmm5 = xmm5[0],xmm10[0],xmm5[1],xmm10[1],xmm5[2],xmm10[2],xmm5[3],xmm10[3],xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	movdqa	%xmm5, %xmm14
	punpcklwd	%xmm10, %xmm14  # xmm14 = xmm14[0],xmm10[0],xmm14[1],xmm10[1],xmm14[2],xmm10[2],xmm14[3],xmm10[3]
	punpckhwd	%xmm10, %xmm5   # xmm5 = xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	pshufd	$78, %xmm7, %xmm2       # xmm2 = xmm7[2,3,0,1]
	punpcklbw	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3],xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	movdqa	%xmm2, %xmm1
	punpcklwd	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3]
	punpckhwd	%xmm10, %xmm2   # xmm2 = xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	punpcklbw	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3],xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	movdqa	%xmm7, %xmm0
	punpcklwd	%xmm10, %xmm0   # xmm0 = xmm0[0],xmm10[0],xmm0[1],xmm10[1],xmm0[2],xmm10[2],xmm0[3],xmm10[3]
	punpckhwd	%xmm10, %xmm7   # xmm7 = xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	paddd	%xmm5, %xmm7
	paddd	%xmm14, %xmm0
	paddd	%xmm6, %xmm2
	paddd	%xmm13, %xmm1
	paddd	%xmm9, %xmm1
	paddd	%xmm9, %xmm2
	paddd	%xmm9, %xmm0
	paddd	%xmm9, %xmm7
	psrld	$1, %xmm7
	psrld	$1, %xmm0
	psrld	$1, %xmm2
	psrld	$1, %xmm1
	paddd	%xmm9, %xmm11
	paddd	%xmm7, %xmm11
	paddd	%xmm9, %xmm3
	paddd	%xmm0, %xmm3
	paddd	%xmm9, %xmm12
	paddd	%xmm2, %xmm12
	paddd	%xmm9, %xmm4
	paddd	%xmm1, %xmm4
	psrld	$1, %xmm4
	psrld	$1, %xmm12
	psrld	$1, %xmm3
	psrld	$1, %xmm11
	pand	%xmm8, %xmm11
	pand	%xmm8, %xmm3
	packuswb	%xmm11, %xmm3
	pand	%xmm8, %xmm12
	pand	%xmm8, %xmm4
	packuswb	%xmm12, %xmm4
	packuswb	%xmm4, %xmm3
	movdqu	%xmm3, (%r8,%rbp)
	addq	$16, %rbp
	cmpq	%rbp, %r11
	jne	.LBB2_53
# BB#54:                                # %middle.block368
                                        #   in Loop: Header=BB2_45 Depth=1
	testl	%r15d, %r15d
	movq	%r11, %rsi
	je	.LBB2_56
	.p2align	4, 0x90
.LBB2_55:                               # %scalar.ph369
                                        #   Parent Loop BB2_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r8,%rsi), %edi
	movzbl	(%rax,%rsi), %ebp
	movzbl	1(%rax,%rsi), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	leal	1(%rdi,%rbp), %edi
	shrl	%edi
	movb	%dil, (%r8,%rsi)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB2_55
.LBB2_56:                               # %._crit_edge280.us
                                        #   in Loop: Header=BB2_45 Depth=1
	addq	%r13, %rax
	addq	%r13, %r8
	incq	%rcx
	cmpl	-108(%rsp), %ecx        # 4-byte Folded Reload
	jne	.LBB2_45
	jmp	.LBB2_146
.LBB2_57:                               # %.preheader222
	movl	-108(%rsp), %ebx        # 4-byte Reload
	testl	%ebx, %ebx
	jle	.LBB2_146
# BB#58:                                # %.preheader.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#59:                                # %.preheader.us.preheader
	movslq	%ecx, %r11
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %r12d
	addq	%r15, %rbp
	movq	%rbp, %r9
	leaq	(%rbp,%r12), %rdx
	movq	%rdx, -88(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rdx        # 8-byte Reload
	addq	%r15, %rdx
	addq	%r14, %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	leaq	(%rdx,%r12), %r15
	leaq	-1(%r12), %r13
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$31, %ecx
	movq	%r12, %r10
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	subq	%rcx, %r10
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_60:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_68 Depth 2
                                        #     Child Loop BB2_72 Depth 2
                                        #     Child Loop BB2_74 Depth 2
	cmpl	$32, -112(%rsp)         # 4-byte Folded Reload
	jae	.LBB2_62
# BB#61:                                #   in Loop: Header=BB2_60 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB2_70
	.p2align	4, 0x90
.LBB2_62:                               # %min.iters.checked581
                                        #   in Loop: Header=BB2_60 Depth=1
	testq	%r10, %r10
	je	.LBB2_66
# BB#63:                                # %vector.memcheck596
                                        #   in Loop: Header=BB2_60 Depth=1
	movq	%r11, %rdx
	imulq	%r14, %rdx
	leaq	(%r9,%rdx), %rsi
	movq	-64(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %rsi
	leaq	(%r15,%rdx), %rdi
	movq	-72(%rsp), %rbp         # 8-byte Reload
	addq	%rbp, %rdi
	cmpq	%rdi, %rsi
	jae	.LBB2_67
# BB#64:                                # %vector.memcheck596
                                        #   in Loop: Header=BB2_60 Depth=1
	movq	-88(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdx), %rsi
	addq	%rcx, %rsi
	addq	-104(%rsp), %rdx        # 8-byte Folded Reload
	addq	%rbp, %rdx
	cmpq	%rsi, %rdx
	jae	.LBB2_67
.LBB2_66:                               #   in Loop: Header=BB2_60 Depth=1
	xorl	%ebp, %ebp
	jmp	.LBB2_70
.LBB2_67:                               # %vector.body577.preheader
                                        #   in Loop: Header=BB2_60 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_68:                               # %vector.body577
                                        #   Parent Loop BB2_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rax,%rdx), %xmm0
	movdqu	16(%rax,%rdx), %xmm1
	movdqu	%xmm0, (%r8,%rdx)
	movdqu	%xmm1, 16(%r8,%rdx)
	addq	$32, %rdx
	cmpq	%rdx, %r10
	jne	.LBB2_68
# BB#69:                                # %middle.block578
                                        #   in Loop: Header=BB2_60 Depth=1
	cmpl	$0, -96(%rsp)           # 4-byte Folded Reload
	movq	%r10, %rbp
	je	.LBB2_75
	.p2align	4, 0x90
.LBB2_70:                               # %scalar.ph579.preheader
                                        #   in Loop: Header=BB2_60 Depth=1
	movl	%r12d, %edx
	subl	%ebp, %edx
	movq	%r13, %rsi
	subq	%rbp, %rsi
	andq	$7, %rdx
	je	.LBB2_73
# BB#71:                                # %scalar.ph579.prol.preheader
                                        #   in Loop: Header=BB2_60 Depth=1
	negq	%rdx
	.p2align	4, 0x90
.LBB2_72:                               # %scalar.ph579.prol
                                        #   Parent Loop BB2_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax,%rbp), %ecx
	movb	%cl, (%r8,%rbp)
	incq	%rbp
	incq	%rdx
	jne	.LBB2_72
.LBB2_73:                               # %scalar.ph579.prol.loopexit
                                        #   in Loop: Header=BB2_60 Depth=1
	cmpq	$7, %rsi
	jb	.LBB2_75
	.p2align	4, 0x90
.LBB2_74:                               # %scalar.ph579
                                        #   Parent Loop BB2_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax,%rbp), %ecx
	movb	%cl, (%r8,%rbp)
	movzbl	1(%rax,%rbp), %ecx
	movb	%cl, 1(%r8,%rbp)
	movzbl	2(%rax,%rbp), %ecx
	movb	%cl, 2(%r8,%rbp)
	movzbl	3(%rax,%rbp), %ecx
	movb	%cl, 3(%r8,%rbp)
	movzbl	4(%rax,%rbp), %ecx
	movb	%cl, 4(%r8,%rbp)
	movzbl	5(%rax,%rbp), %ecx
	movb	%cl, 5(%r8,%rbp)
	movzbl	6(%rax,%rbp), %ecx
	movb	%cl, 6(%r8,%rbp)
	movzbl	7(%rax,%rbp), %ecx
	movb	%cl, 7(%r8,%rbp)
	addq	$8, %rbp
	cmpq	%rbp, %r12
	jne	.LBB2_74
.LBB2_75:                               # %._crit_edge.us
                                        #   in Loop: Header=BB2_60 Depth=1
	addq	%r11, %rax
	addq	%r11, %r8
	incq	%r14
	cmpl	%ebx, %r14d
	jne	.LBB2_60
	jmp	.LBB2_146
.LBB2_76:
	testl	%esi, %esi
	je	.LBB2_130
# BB#77:                                # %.preheader233
	testl	%ebx, %ebx
	jle	.LBB2_146
# BB#78:                                # %.preheader232.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#79:                                # %.preheader232.us.preheader
	movslq	%ecx, %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	movslq	%edx, %r11
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %esi
	addq	%r15, %rbp
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rsi), %rdx
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rdx        # 8-byte Reload
	addq	%r15, %rdx
	leaq	(%rdx,%r11), %rdi
	addq	%r14, %rdi
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	leaq	1(%rsi,%rdi), %rdi
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	addq	%r14, %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	leaq	1(%rsi,%rdx), %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$15, %ecx
	movq	%rsi, %r15
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	subq	%rcx, %r15
	incq	%r11
	negq	%rsi
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	pxor	%xmm11, %xmm11
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [2,2,2,2]
	movdqa	.LCPI2_0(%rip), %xmm9   # xmm9 = [1,1,1,1]
	movdqa	.LCPI2_1(%rip), %xmm10  # xmm10 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	.p2align	4, 0x90
.LBB2_80:                               # %.preheader232.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_86 Depth 2
                                        #     Child Loop BB2_91 Depth 2
	cmpl	$16, -112(%rsp)         # 4-byte Folded Reload
	jb	.LBB2_89
# BB#82:                                # %min.iters.checked439
                                        #   in Loop: Header=BB2_80 Depth=1
	testq	%r15, %r15
	je	.LBB2_89
# BB#83:                                # %vector.memcheck460
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	-96(%rsp), %rcx         # 8-byte Reload
	imulq	%r13, %rcx
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %r14
	movq	-64(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %r14
	movq	-32(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx), %rsi
	addq	%rdx, %rsi
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rdi
	movq	-72(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %rdi
	movq	-48(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rcx), %rbp
	addq	%rdx, %rbp
	movq	-104(%rsp), %rbx        # 8-byte Reload
	leaq	(%rbx,%rcx), %rbx
	addq	%rdx, %rbx
	addq	-56(%rsp), %rcx         # 8-byte Folded Reload
	addq	%rdx, %rcx
	cmpq	%rbp, %r14
	sbbb	%r9b, %r9b
	cmpq	%rsi, %rdi
	sbbb	%dl, %dl
	andb	%r9b, %dl
	cmpq	%rcx, %r14
	sbbb	%cl, %cl
	cmpq	%rsi, %rbx
	sbbb	%bl, %bl
	testb	$1, %dl
	jne	.LBB2_89
# BB#84:                                # %vector.memcheck460
                                        #   in Loop: Header=BB2_80 Depth=1
	andb	%bl, %cl
	andb	$1, %cl
	movl	$0, %r9d
	jne	.LBB2_90
# BB#85:                                # %vector.body435.preheader
                                        #   in Loop: Header=BB2_80 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_86:                               # %vector.body435
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rax,%r12), %xmm2
	movdqu	1(%rax,%r12), %xmm4
	pshufd	$78, %xmm2, %xmm6       # xmm6 = xmm2[2,3,0,1]
	punpcklbw	%xmm11, %xmm6   # xmm6 = xmm6[0],xmm11[0],xmm6[1],xmm11[1],xmm6[2],xmm11[2],xmm6[3],xmm11[3],xmm6[4],xmm11[4],xmm6[5],xmm11[5],xmm6[6],xmm11[6],xmm6[7],xmm11[7]
	movdqa	%xmm6, %xmm7
	punpcklwd	%xmm11, %xmm7   # xmm7 = xmm7[0],xmm11[0],xmm7[1],xmm11[1],xmm7[2],xmm11[2],xmm7[3],xmm11[3]
	punpckhwd	%xmm11, %xmm6   # xmm6 = xmm6[4],xmm11[4],xmm6[5],xmm11[5],xmm6[6],xmm11[6],xmm6[7],xmm11[7]
	punpcklbw	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1],xmm2[2],xmm11[2],xmm2[3],xmm11[3],xmm2[4],xmm11[4],xmm2[5],xmm11[5],xmm2[6],xmm11[6],xmm2[7],xmm11[7]
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm11, %xmm3   # xmm3 = xmm3[0],xmm11[0],xmm3[1],xmm11[1],xmm3[2],xmm11[2],xmm3[3],xmm11[3]
	punpckhwd	%xmm11, %xmm2   # xmm2 = xmm2[4],xmm11[4],xmm2[5],xmm11[5],xmm2[6],xmm11[6],xmm2[7],xmm11[7]
	pshufd	$78, %xmm4, %xmm5       # xmm5 = xmm4[2,3,0,1]
	punpcklbw	%xmm11, %xmm5   # xmm5 = xmm5[0],xmm11[0],xmm5[1],xmm11[1],xmm5[2],xmm11[2],xmm5[3],xmm11[3],xmm5[4],xmm11[4],xmm5[5],xmm11[5],xmm5[6],xmm11[6],xmm5[7],xmm11[7]
	punpcklbw	%xmm11, %xmm4   # xmm4 = xmm4[0],xmm11[0],xmm4[1],xmm11[1],xmm4[2],xmm11[2],xmm4[3],xmm11[3],xmm4[4],xmm11[4],xmm4[5],xmm11[5],xmm4[6],xmm11[6],xmm4[7],xmm11[7]
	movdqa	%xmm4, %xmm1
	punpckhwd	%xmm11, %xmm4   # xmm4 = xmm4[4],xmm11[4],xmm4[5],xmm11[5],xmm4[6],xmm11[6],xmm4[7],xmm11[7]
	paddd	%xmm2, %xmm4
	movdqa	%xmm5, %xmm2
	punpcklwd	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1],xmm2[2],xmm11[2],xmm2[3],xmm11[3]
	punpckhwd	%xmm11, %xmm5   # xmm5 = xmm5[4],xmm11[4],xmm5[5],xmm11[5],xmm5[6],xmm11[6],xmm5[7],xmm11[7]
	punpcklwd	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1],xmm1[2],xmm11[2],xmm1[3],xmm11[3]
	leaq	(%r11,%r12), %rcx
	paddd	%xmm3, %xmm1
	movdqu	-1(%rax,%rcx), %xmm3
	paddd	%xmm6, %xmm5
	pshufd	$78, %xmm3, %xmm6       # xmm6 = xmm3[2,3,0,1]
	punpcklbw	%xmm11, %xmm6   # xmm6 = xmm6[0],xmm11[0],xmm6[1],xmm11[1],xmm6[2],xmm11[2],xmm6[3],xmm11[3],xmm6[4],xmm11[4],xmm6[5],xmm11[5],xmm6[6],xmm11[6],xmm6[7],xmm11[7]
	paddd	%xmm7, %xmm2
	movdqa	%xmm6, %xmm7
	punpcklwd	%xmm11, %xmm6   # xmm6 = xmm6[0],xmm11[0],xmm6[1],xmm11[1],xmm6[2],xmm11[2],xmm6[3],xmm11[3]
	paddd	%xmm6, %xmm2
	movdqu	(%rax,%rcx), %xmm0
	punpcklbw	%xmm11, %xmm3   # xmm3 = xmm3[0],xmm11[0],xmm3[1],xmm11[1],xmm3[2],xmm11[2],xmm3[3],xmm11[3],xmm3[4],xmm11[4],xmm3[5],xmm11[5],xmm3[6],xmm11[6],xmm3[7],xmm11[7]
	punpckhwd	%xmm11, %xmm7   # xmm7 = xmm7[4],xmm11[4],xmm7[5],xmm11[5],xmm7[6],xmm11[6],xmm7[7],xmm11[7]
	paddd	%xmm7, %xmm5
	movdqa	%xmm3, %xmm6
	punpckhwd	%xmm11, %xmm6   # xmm6 = xmm6[4],xmm11[4],xmm6[5],xmm11[5],xmm6[6],xmm11[6],xmm6[7],xmm11[7]
	punpcklwd	%xmm11, %xmm3   # xmm3 = xmm3[0],xmm11[0],xmm3[1],xmm11[1],xmm3[2],xmm11[2],xmm3[3],xmm11[3]
	paddd	%xmm3, %xmm1
	pshufd	$78, %xmm0, %xmm7       # xmm7 = xmm0[2,3,0,1]
	punpcklbw	%xmm11, %xmm7   # xmm7 = xmm7[0],xmm11[0],xmm7[1],xmm11[1],xmm7[2],xmm11[2],xmm7[3],xmm11[3],xmm7[4],xmm11[4],xmm7[5],xmm11[5],xmm7[6],xmm11[6],xmm7[7],xmm11[7]
	paddd	%xmm6, %xmm4
	movdqa	%xmm7, %xmm6
	punpcklwd	%xmm11, %xmm6   # xmm6 = xmm6[0],xmm11[0],xmm6[1],xmm11[1],xmm6[2],xmm11[2],xmm6[3],xmm11[3]
	paddd	%xmm8, %xmm6
	paddd	%xmm2, %xmm6
	movdqu	(%r8,%r12), %xmm2
	punpckhwd	%xmm11, %xmm7   # xmm7 = xmm7[4],xmm11[4],xmm7[5],xmm11[5],xmm7[6],xmm11[6],xmm7[7],xmm11[7]
	punpcklbw	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0],xmm0[1],xmm11[1],xmm0[2],xmm11[2],xmm0[3],xmm11[3],xmm0[4],xmm11[4],xmm0[5],xmm11[5],xmm0[6],xmm11[6],xmm0[7],xmm11[7]
	paddd	%xmm8, %xmm7
	paddd	%xmm5, %xmm7
	movdqa	%xmm0, %xmm5
	punpcklwd	%xmm11, %xmm5   # xmm5 = xmm5[0],xmm11[0],xmm5[1],xmm11[1],xmm5[2],xmm11[2],xmm5[3],xmm11[3]
	paddd	%xmm8, %xmm5
	paddd	%xmm1, %xmm5
	pshufd	$78, %xmm2, %xmm1       # xmm1 = xmm2[2,3,0,1]
	punpcklbw	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1],xmm2[2],xmm11[2],xmm2[3],xmm11[3],xmm2[4],xmm11[4],xmm2[5],xmm11[5],xmm2[6],xmm11[6],xmm2[7],xmm11[7]
	punpckhwd	%xmm11, %xmm0   # xmm0 = xmm0[4],xmm11[4],xmm0[5],xmm11[5],xmm0[6],xmm11[6],xmm0[7],xmm11[7]
	paddd	%xmm8, %xmm0
	paddd	%xmm4, %xmm0
	movdqa	%xmm2, %xmm3
	punpckhwd	%xmm11, %xmm3   # xmm3 = xmm3[4],xmm11[4],xmm3[5],xmm11[5],xmm3[6],xmm11[6],xmm3[7],xmm11[7]
	psrld	$2, %xmm0
	paddd	%xmm9, %xmm3
	paddd	%xmm0, %xmm3
	punpcklwd	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1],xmm2[2],xmm11[2],xmm2[3],xmm11[3]
	punpcklbw	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1],xmm1[2],xmm11[2],xmm1[3],xmm11[3],xmm1[4],xmm11[4],xmm1[5],xmm11[5],xmm1[6],xmm11[6],xmm1[7],xmm11[7]
	psrld	$2, %xmm5
	paddd	%xmm9, %xmm2
	paddd	%xmm5, %xmm2
	movdqa	%xmm1, %xmm0
	punpckhwd	%xmm11, %xmm0   # xmm0 = xmm0[4],xmm11[4],xmm0[5],xmm11[5],xmm0[6],xmm11[6],xmm0[7],xmm11[7]
	psrld	$2, %xmm7
	paddd	%xmm9, %xmm0
	paddd	%xmm7, %xmm0
	punpcklwd	%xmm11, %xmm1   # xmm1 = xmm1[0],xmm11[0],xmm1[1],xmm11[1],xmm1[2],xmm11[2],xmm1[3],xmm11[3]
	psrld	$2, %xmm6
	paddd	%xmm9, %xmm1
	paddd	%xmm6, %xmm1
	psrld	$1, %xmm2
	psrld	$1, %xmm3
	pand	%xmm10, %xmm3
	pand	%xmm10, %xmm2
	packuswb	%xmm3, %xmm2
	psrld	$1, %xmm1
	psrld	$1, %xmm0
	pand	%xmm10, %xmm0
	pand	%xmm10, %xmm1
	packuswb	%xmm0, %xmm1
	packuswb	%xmm1, %xmm2
	movdqu	%xmm2, (%r8,%r12)
	addq	$16, %r12
	cmpq	%r12, %r15
	jne	.LBB2_86
# BB#87:                                # %middle.block436
                                        #   in Loop: Header=BB2_80 Depth=1
	cmpl	$0, -24(%rsp)           # 4-byte Folded Reload
	movq	%r15, %r9
	jne	.LBB2_90
	jmp	.LBB2_92
	.p2align	4, 0x90
.LBB2_89:                               #   in Loop: Header=BB2_80 Depth=1
	xorl	%r9d, %r9d
.LBB2_90:                               # %scalar.ph437.preheader
                                        #   in Loop: Header=BB2_80 Depth=1
	leaq	(%r11,%r9), %r14
	movq	-80(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %r12
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_91:                               # %scalar.ph437
                                        #   Parent Loop BB2_80 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r9,%rsi), %rdi
	movzbl	(%r8,%rdi), %ebx
	movzbl	(%rax,%rdi), %ebp
	movzbl	1(%rax,%rdi), %ecx
	leaq	(%r14,%rsi), %rdx
	movzbl	-1(%rax,%rdx), %r10d
	movzbl	(%rax,%rdx), %edx
	addl	%ebp, %ecx
	addl	%r10d, %ecx
	leal	2(%rdx,%rcx), %ecx
	shrl	$2, %ecx
	leal	1(%rbx,%rcx), %ecx
	shrl	%ecx
	movb	%cl, (%r8,%rdi)
	incq	%rsi
	movq	%r12, %rcx
	addq	%rsi, %rcx
	jne	.LBB2_91
.LBB2_92:                               # %._crit_edge268.us
                                        #   in Loop: Header=BB2_80 Depth=1
	movq	-96(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %rax
	addq	%rcx, %r8
	incq	%r13
	cmpl	-108(%rsp), %r13d       # 4-byte Folded Reload
	jne	.LBB2_80
	jmp	.LBB2_146
.LBB2_93:                               # %.preheader227
	testl	%esi, %esi
	jle	.LBB2_146
# BB#94:                                # %.preheader226.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#95:                                # %.preheader226.us.preheader
	movslq	%ecx, %r11
	movslq	%edx, %r10
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %esi
	addq	%r15, %rbp
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rsi), %rdx
	movq	%rdx, -48(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rdx        # 8-byte Reload
	addq	%r15, %rdx
	leaq	(%rdx,%r10), %rdi
	addq	%r14, %rdi
	movq	%rdi, -56(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rsi), %rdi
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	addq	%r14, %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	leaq	(%rdx,%rsi), %rdx
	movq	%rdx, -8(%rsp)          # 8-byte Spill
	leaq	-1(%rsi), %rdx
	movq	%rdx, -80(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$15, %ecx
	movq	%rsi, %r13
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	subq	%rcx, %r13
	movq	%rsi, -96(%rsp)         # 8-byte Spill
	negq	%rsi
	movq	%rsi, -32(%rsp)         # 8-byte Spill
	leaq	1(%r10), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_96:                               # %.preheader226.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_102 Depth 2
                                        #     Child Loop BB2_110 Depth 2
	cmpl	$16, -112(%rsp)         # 4-byte Folded Reload
	jb	.LBB2_105
# BB#98:                                # %min.iters.checked515
                                        #   in Loop: Header=BB2_96 Depth=1
	testq	%r13, %r13
	je	.LBB2_105
# BB#99:                                # %vector.memcheck536
                                        #   in Loop: Header=BB2_96 Depth=1
	movq	%r11, %rcx
	imulq	%r15, %rcx
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %r14
	movq	-64(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %r14
	movq	-48(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx), %rsi
	addq	%rdx, %rsi
	movq	-56(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rdi
	movq	-72(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %rdi
	movq	-24(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rcx), %rbp
	addq	%rdx, %rbp
	movq	-104(%rsp), %rbx        # 8-byte Reload
	leaq	(%rbx,%rcx), %rbx
	addq	%rdx, %rbx
	addq	-8(%rsp), %rcx          # 8-byte Folded Reload
	addq	%rdx, %rcx
	cmpq	%rbp, %r14
	sbbb	%r9b, %r9b
	cmpq	%rsi, %rdi
	sbbb	%dl, %dl
	andb	%r9b, %dl
	cmpq	%rcx, %r14
	sbbb	%cl, %cl
	cmpq	%rsi, %rbx
	sbbb	%bl, %bl
	testb	$1, %dl
	jne	.LBB2_105
# BB#100:                               # %vector.memcheck536
                                        #   in Loop: Header=BB2_96 Depth=1
	andb	%bl, %cl
	andb	$1, %cl
	movl	$0, %r12d
	jne	.LBB2_106
# BB#101:                               # %vector.body511.preheader
                                        #   in Loop: Header=BB2_96 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_102:                              # %vector.body511
                                        #   Parent Loop BB2_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rax,%rcx), %xmm0
	leaq	(%r10,%rcx), %rdx
	movdqu	(%rax,%rdx), %xmm1
	pavgb	%xmm0, %xmm1
	movdqu	%xmm1, (%r8,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %r13
	jne	.LBB2_102
# BB#103:                               # %middle.block512
                                        #   in Loop: Header=BB2_96 Depth=1
	cmpl	$0, -16(%rsp)           # 4-byte Folded Reload
	movq	%r13, %r12
	jne	.LBB2_106
	jmp	.LBB2_111
	.p2align	4, 0x90
.LBB2_105:                              #   in Loop: Header=BB2_96 Depth=1
	xorl	%r12d, %r12d
.LBB2_106:                              # %scalar.ph513.preheader
                                        #   in Loop: Header=BB2_96 Depth=1
	movq	-96(%rsp), %rcx         # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%r12d, %ecx
	testb	$1, %cl
	movq	%r12, %r9
	je	.LBB2_108
# BB#107:                               # %scalar.ph513.prol
                                        #   in Loop: Header=BB2_96 Depth=1
	movzbl	(%rax,%r12), %ecx
	leaq	(%r12,%r10), %rdx
	movzbl	(%rax,%rdx), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movb	%cl, (%r8,%r12)
	leaq	1(%r12), %r9
.LBB2_108:                              # %scalar.ph513.prol.loopexit
                                        #   in Loop: Header=BB2_96 Depth=1
	cmpq	%r12, -80(%rsp)         # 8-byte Folded Reload
	je	.LBB2_111
# BB#109:                               # %scalar.ph513.preheader.new
                                        #   in Loop: Header=BB2_96 Depth=1
	movq	-32(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %r14
	movq	-40(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r9), %rdx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_110:                              # %scalar.ph513
                                        #   Parent Loop BB2_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r9,%rbp), %rdi
	movzbl	(%rax,%rdi), %ebx
	leaq	(%rdx,%rbp), %rcx
	movzbl	-1(%rax,%rcx), %esi
	leal	1(%rbx,%rsi), %esi
	shrl	%esi
	movb	%sil, (%r8,%rdi)
	movzbl	1(%rax,%rdi), %esi
	movzbl	(%rax,%rcx), %ecx
	leal	1(%rsi,%rcx), %ecx
	shrl	%ecx
	movb	%cl, 1(%r8,%rdi)
	addq	$2, %rbp
	movq	%r14, %rcx
	addq	%rbp, %rcx
	jne	.LBB2_110
.LBB2_111:                              # %._crit_edge256.us
                                        #   in Loop: Header=BB2_96 Depth=1
	addq	%r11, %rax
	addq	%r11, %r8
	incq	%r15
	cmpl	-108(%rsp), %r15d       # 4-byte Folded Reload
	jne	.LBB2_96
	jmp	.LBB2_146
.LBB2_112:                              # %.preheader242
	testl	%ebx, %ebx
	jle	.LBB2_146
# BB#113:                               # %.preheader241.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#114:                               # %.preheader241.us.preheader
	movslq	%ecx, %r11
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %edx
	addq	%r15, %rbp
	movq	%rbp, %r12
	leaq	(%rbp,%rdx), %rsi
	movq	%rsi, -88(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rsi        # 8-byte Reload
	addq	%r15, %rsi
	addq	%r14, %rsi
	movq	%rsi, -104(%rsp)        # 8-byte Spill
	leaq	1(%rdx,%rsi), %r15
	leaq	-1(%rdx), %r13
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$15, %ecx
	movq	%rdx, %rbp
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	subq	%rcx, %rbp
	xorl	%ecx, %ecx
	movq	-64(%rsp), %r9          # 8-byte Reload
	movq	-72(%rsp), %r10         # 8-byte Reload
	.p2align	4, 0x90
.LBB2_115:                              # %.preheader241.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_123 Depth 2
                                        #     Child Loop BB2_128 Depth 2
	cmpl	$16, -112(%rsp)         # 4-byte Folded Reload
	jae	.LBB2_117
# BB#116:                               #   in Loop: Header=BB2_115 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_125
	.p2align	4, 0x90
.LBB2_117:                              # %min.iters.checked
                                        #   in Loop: Header=BB2_115 Depth=1
	testq	%rbp, %rbp
	je	.LBB2_121
# BB#118:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_115 Depth=1
	movq	%r11, %rsi
	imulq	%rcx, %rsi
	leaq	(%r12,%rsi), %rdi
	addq	%r9, %rdi
	leaq	(%r15,%rsi), %rbx
	addq	%r10, %rbx
	cmpq	%rbx, %rdi
	jae	.LBB2_122
# BB#119:                               # %vector.memcheck
                                        #   in Loop: Header=BB2_115 Depth=1
	movq	-88(%rsp), %rdi         # 8-byte Reload
	leaq	(%rdi,%rsi), %rdi
	addq	%r9, %rdi
	addq	-104(%rsp), %rsi        # 8-byte Folded Reload
	addq	%r10, %rsi
	cmpq	%rdi, %rsi
	jae	.LBB2_122
# BB#120:                               #   in Loop: Header=BB2_115 Depth=1
	xorl	%esi, %esi
	movl	-108(%rsp), %ebx        # 4-byte Reload
	jmp	.LBB2_125
.LBB2_121:                              #   in Loop: Header=BB2_115 Depth=1
	xorl	%esi, %esi
	jmp	.LBB2_125
.LBB2_122:                              # %vector.body.preheader
                                        #   in Loop: Header=BB2_115 Depth=1
	xorl	%esi, %esi
	movl	-108(%rsp), %ebx        # 4-byte Reload
	.p2align	4, 0x90
.LBB2_123:                              # %vector.body
                                        #   Parent Loop BB2_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rax,%rsi), %xmm0
	movdqu	1(%rax,%rsi), %xmm1
	pavgb	%xmm0, %xmm1
	movdqu	%xmm1, (%r8,%rsi)
	addq	$16, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB2_123
# BB#124:                               # %middle.block
                                        #   in Loop: Header=BB2_115 Depth=1
	cmpl	$0, -96(%rsp)           # 4-byte Folded Reload
	movq	%rbp, %rsi
	je	.LBB2_129
	.p2align	4, 0x90
.LBB2_125:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_115 Depth=1
	movl	%edx, %edi
	subl	%esi, %edi
	testb	$1, %dil
	movq	%rsi, %r14
	je	.LBB2_127
# BB#126:                               # %scalar.ph.prol
                                        #   in Loop: Header=BB2_115 Depth=1
	movzbl	(%rax,%rsi), %edi
	leaq	1(%rsi), %r14
	movzbl	1(%rax,%rsi), %ebx
	leal	1(%rdi,%rbx), %edi
	movl	-108(%rsp), %ebx        # 4-byte Reload
	shrl	%edi
	movb	%dil, (%r8,%rsi)
.LBB2_127:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB2_115 Depth=1
	cmpq	%rsi, %r13
	je	.LBB2_129
	.p2align	4, 0x90
.LBB2_128:                              # %scalar.ph
                                        #   Parent Loop BB2_115 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax,%r14), %esi
	movzbl	1(%rax,%r14), %edi
	leal	1(%rsi,%rdi), %esi
	shrl	%esi
	movb	%sil, (%r8,%r14)
	movzbl	1(%rax,%r14), %esi
	movzbl	2(%rax,%r14), %edi
	leal	1(%rsi,%rdi), %esi
	shrl	%esi
	movb	%sil, 1(%r8,%r14)
	leaq	2(%r14), %r14
	cmpq	%r14, %rdx
	jne	.LBB2_128
.LBB2_129:                              # %._crit_edge286.us
                                        #   in Loop: Header=BB2_115 Depth=1
	addq	%r11, %rax
	addq	%r11, %r8
	incq	%rcx
	cmpl	%ebx, %ecx
	jne	.LBB2_115
	jmp	.LBB2_146
.LBB2_130:                              # %.preheader236
	testl	%ebx, %ebx
	jle	.LBB2_146
# BB#131:                               # %.preheader235.lr.ph
	cmpl	$0, -112(%rsp)          # 4-byte Folded Reload
	jle	.LBB2_146
# BB#132:                               # %.preheader235.us.preheader
	movslq	%ecx, %rcx
	movq	%rcx, -96(%rsp)         # 8-byte Spill
	movslq	%edx, %r9
	movl	-112(%rsp), %ecx        # 4-byte Reload
	movl	%ecx, %esi
	addq	%r15, %rbp
	movq	%rbp, -88(%rsp)         # 8-byte Spill
	leaq	(%rbp,%rsi), %rdx
	movq	%rdx, -32(%rsp)         # 8-byte Spill
	movq	-104(%rsp), %rdx        # 8-byte Reload
	addq	%r15, %rdx
	leaq	(%rdx,%r9), %rdi
	addq	%r14, %rdi
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	leaq	1(%rsi,%rdi), %rdi
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	addq	%r14, %rdx
	movq	%rdx, -104(%rsp)        # 8-byte Spill
	leaq	1(%rsi,%rdx), %rdx
	movq	%rdx, -56(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	andl	$15, %ecx
	movq	%rsi, %r10
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	subq	%rcx, %r10
	incq	%r9
	negq	%rsi
	movq	%rsi, -80(%rsp)         # 8-byte Spill
	xorl	%r12d, %r12d
	pxor	%xmm10, %xmm10
	movdqa	.LCPI2_2(%rip), %xmm8   # xmm8 = [2,2,2,2]
	movdqa	.LCPI2_1(%rip), %xmm9   # xmm9 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	.p2align	4, 0x90
.LBB2_133:                              # %.preheader235.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_139 Depth 2
                                        #     Child Loop BB2_144 Depth 2
	cmpl	$16, -112(%rsp)         # 4-byte Folded Reload
	jb	.LBB2_142
# BB#135:                               # %min.iters.checked402
                                        #   in Loop: Header=BB2_133 Depth=1
	testq	%r10, %r10
	je	.LBB2_142
# BB#136:                               # %vector.memcheck422
                                        #   in Loop: Header=BB2_133 Depth=1
	movq	-96(%rsp), %rcx         # 8-byte Reload
	imulq	%r12, %rcx
	movq	-88(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %r15
	movq	-64(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %r15
	movq	-32(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx), %rdi
	addq	%rdx, %rdi
	movq	-40(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rbp
	movq	-72(%rsp), %rdx         # 8-byte Reload
	addq	%rdx, %rbp
	movq	-48(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rcx), %rbx
	addq	%rdx, %rbx
	movq	-104(%rsp), %rsi        # 8-byte Reload
	leaq	(%rsi,%rcx), %r14
	addq	%rdx, %r14
	addq	-56(%rsp), %rcx         # 8-byte Folded Reload
	addq	%rdx, %rcx
	cmpq	%rbx, %r15
	sbbb	%r11b, %r11b
	cmpq	%rdi, %rbp
	sbbb	%bl, %bl
	andb	%r11b, %bl
	cmpq	%rcx, %r15
	sbbb	%cl, %cl
	cmpq	%rdi, %r14
	sbbb	%sil, %sil
	testb	$1, %bl
	jne	.LBB2_142
# BB#137:                               # %vector.memcheck422
                                        #   in Loop: Header=BB2_133 Depth=1
	andb	%sil, %cl
	andb	$1, %cl
	movl	$0, %r14d
	jne	.LBB2_143
# BB#138:                               # %vector.body398.preheader
                                        #   in Loop: Header=BB2_133 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_139:                              # %vector.body398
                                        #   Parent Loop BB2_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rax,%rbp), %xmm5
	movdqu	1(%rax,%rbp), %xmm0
	pshufd	$78, %xmm5, %xmm1       # xmm1 = xmm5[2,3,0,1]
	punpcklbw	%xmm10, %xmm5   # xmm5 = xmm5[0],xmm10[0],xmm5[1],xmm10[1],xmm5[2],xmm10[2],xmm5[3],xmm10[3],xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	movdqa	%xmm5, %xmm6
	punpckhwd	%xmm10, %xmm6   # xmm6 = xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	punpcklwd	%xmm10, %xmm5   # xmm5 = xmm5[0],xmm10[0],xmm5[1],xmm10[1],xmm5[2],xmm10[2],xmm5[3],xmm10[3]
	punpcklbw	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3],xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	movdqa	%xmm1, %xmm2
	punpckhwd	%xmm10, %xmm2   # xmm2 = xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	punpcklwd	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3]
	pshufd	$78, %xmm0, %xmm4       # xmm4 = xmm0[2,3,0,1]
	punpcklbw	%xmm10, %xmm0   # xmm0 = xmm0[0],xmm10[0],xmm0[1],xmm10[1],xmm0[2],xmm10[2],xmm0[3],xmm10[3],xmm0[4],xmm10[4],xmm0[5],xmm10[5],xmm0[6],xmm10[6],xmm0[7],xmm10[7]
	punpcklbw	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3],xmm4[4],xmm10[4],xmm4[5],xmm10[5],xmm4[6],xmm10[6],xmm4[7],xmm10[7]
	movdqa	%xmm4, %xmm7
	punpcklwd	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3]
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm3
	punpckhwd	%xmm10, %xmm3   # xmm3 = xmm3[4],xmm10[4],xmm3[5],xmm10[5],xmm3[6],xmm10[6],xmm3[7],xmm10[7]
	punpcklwd	%xmm10, %xmm0   # xmm0 = xmm0[0],xmm10[0],xmm0[1],xmm10[1],xmm0[2],xmm10[2],xmm0[3],xmm10[3]
	punpckhwd	%xmm10, %xmm7   # xmm7 = xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	leaq	(%r9,%rbp), %rcx
	paddd	%xmm2, %xmm7
	movdqu	-1(%rax,%rcx), %xmm1
	paddd	%xmm5, %xmm0
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	punpcklbw	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3],xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	paddd	%xmm6, %xmm3
	movdqa	%xmm1, %xmm5
	punpckhwd	%xmm10, %xmm1   # xmm1 = xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	paddd	%xmm1, %xmm3
	movdqu	(%rax,%rcx), %xmm1
	punpcklbw	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3],xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	punpcklwd	%xmm10, %xmm5   # xmm5 = xmm5[0],xmm10[0],xmm5[1],xmm10[1],xmm5[2],xmm10[2],xmm5[3],xmm10[3]
	paddd	%xmm5, %xmm0
	movdqa	%xmm2, %xmm5
	punpcklwd	%xmm10, %xmm5   # xmm5 = xmm5[0],xmm10[0],xmm5[1],xmm10[1],xmm5[2],xmm10[2],xmm5[3],xmm10[3]
	punpckhwd	%xmm10, %xmm2   # xmm2 = xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	paddd	%xmm2, %xmm7
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	punpcklbw	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3],xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	paddd	%xmm5, %xmm4
	movdqa	%xmm1, %xmm5
	punpckhwd	%xmm10, %xmm5   # xmm5 = xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	paddd	%xmm8, %xmm5
	paddd	%xmm3, %xmm5
	punpcklwd	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3]
	punpcklbw	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3],xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	paddd	%xmm8, %xmm1
	paddd	%xmm0, %xmm1
	movdqa	%xmm2, %xmm0
	punpckhwd	%xmm10, %xmm0   # xmm0 = xmm0[4],xmm10[4],xmm0[5],xmm10[5],xmm0[6],xmm10[6],xmm0[7],xmm10[7]
	paddd	%xmm8, %xmm0
	paddd	%xmm7, %xmm0
	punpcklwd	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3]
	paddd	%xmm8, %xmm2
	paddd	%xmm4, %xmm2
	psrld	$2, %xmm1
	psrld	$2, %xmm5
	pand	%xmm9, %xmm5
	pand	%xmm9, %xmm1
	packuswb	%xmm5, %xmm1
	psrld	$2, %xmm2
	psrld	$2, %xmm0
	pand	%xmm9, %xmm0
	pand	%xmm9, %xmm2
	packuswb	%xmm0, %xmm2
	packuswb	%xmm2, %xmm1
	movdqu	%xmm1, (%r8,%rbp)
	addq	$16, %rbp
	cmpq	%rbp, %r10
	jne	.LBB2_139
# BB#140:                               # %middle.block399
                                        #   in Loop: Header=BB2_133 Depth=1
	cmpl	$0, -24(%rsp)           # 4-byte Folded Reload
	movq	%r10, %r14
	jne	.LBB2_143
	jmp	.LBB2_145
	.p2align	4, 0x90
.LBB2_142:                              #   in Loop: Header=BB2_133 Depth=1
	xorl	%r14d, %r14d
.LBB2_143:                              # %scalar.ph400.preheader
                                        #   in Loop: Header=BB2_133 Depth=1
	leaq	(%r9,%r14), %rsi
	movq	-80(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r14), %rcx
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_144:                              # %scalar.ph400
                                        #   Parent Loop BB2_133 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r14,%r11), %rdx
	movzbl	(%rax,%rdx), %edi
	movzbl	1(%rax,%rdx), %ebx
	leaq	(%rsi,%r11), %r15
	movzbl	-1(%rax,%r15), %r13d
	movzbl	(%rax,%r15), %ebp
	addl	%edi, %ebx
	addl	%r13d, %ebx
	leal	2(%rbp,%rbx), %edi
	shrl	$2, %edi
	movb	%dil, (%r8,%rdx)
	incq	%r11
	movq	%rcx, %rdx
	addq	%r11, %rdx
	jne	.LBB2_144
.LBB2_145:                              # %._crit_edge274.us
                                        #   in Loop: Header=BB2_133 Depth=1
	movq	-96(%rsp), %rcx         # 8-byte Reload
	addq	%rcx, %rax
	addq	%rcx, %r8
	incq	%r12
	cmpl	-108(%rsp), %r12d       # 4-byte Folded Reload
	jne	.LBB2_133
.LBB2_146:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	form_component_prediction, .Lfunc_end2-form_component_prediction
	.cfi_endproc

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"invalid motion_type"
	.size	.Lstr.2, 20


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
