	.text
	.file	"PercentPrinter.bc"
	.globl	_ZN15CPercentPrinter10ClosePrintEv
	.p2align	4, 0x90
	.type	_ZN15CPercentPrinter10ClosePrintEv,@function
_ZN15CPercentPrinter10ClosePrintEv:     # @_ZN15CPercentPrinter10ClosePrintEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 160
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	32(%r12), %ebx
	testl	%ebx, %ebx
	je	.LBB0_5
# BB#1:
	jle	.LBB0_2
# BB#3:                                 # %.lr.ph.preheader.i
	decl	%ebx
	leaq	1(%rbx), %r14
	movq	%rsp, %rdi
	movl	$8, %esi
	movq	%r14, %rdx
	callq	memset
	leaq	1(%rsp,%rbx), %r15
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	memset
	leaq	1(%rbx,%r15), %r15
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	memset
	leaq	1(%rbx,%r15), %rax
	jmp	.LBB0_4
.LBB0_2:
	movq	%rsp, %rax
.LBB0_4:                                # %_ZL9ClearPrevPci.exit
	movb	$0, (%rax)
	movq	40(%r12), %rdi
	movq	%rsp, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$0, 32(%r12)
.LBB0_5:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	_ZN15CPercentPrinter10ClosePrintEv, .Lfunc_end0-_ZN15CPercentPrinter10ClosePrintEv
	.cfi_endproc

	.globl	_ZN15CPercentPrinter11PrintStringEPKc
	.p2align	4, 0x90
	.type	_ZN15CPercentPrinter11PrintStringEPKc,@function
_ZN15CPercentPrinter11PrintStringEPKc:  # @_ZN15CPercentPrinter11PrintStringEPKc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 48
	subq	$128, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 176
.Lcfi15:
	.cfi_offset %rbx, -48
.Lcfi16:
	.cfi_offset %r12, -40
.Lcfi17:
	.cfi_offset %r13, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	32(%r13), %ebx
	testl	%ebx, %ebx
	je	.LBB1_1
# BB#2:
	jle	.LBB1_3
# BB#4:                                 # %.lr.ph.preheader.i.i
	decl	%ebx
	leaq	1(%rbx), %r15
	movq	%rsp, %rdi
	movl	$8, %esi
	movq	%r15, %rdx
	callq	memset
	leaq	1(%rsp,%rbx), %r12
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	memset
	leaq	1(%rbx,%r12), %r12
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	memset
	leaq	1(%rbx,%r12), %rax
	jmp	.LBB1_5
.LBB1_1:                                # %._ZN15CPercentPrinter10ClosePrintEv.exit_crit_edge
	addq	$40, %r13
	jmp	.LBB1_6
.LBB1_3:
	movq	%rsp, %rax
.LBB1_5:                                # %_ZL9ClearPrevPci.exit.i
	movb	$0, (%rax)
	movq	40(%r13), %rdi
	movq	%rsp, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$0, 32(%r13)
	leaq	40(%r13), %r13
.LBB1_6:                                # %_ZN15CPercentPrinter10ClosePrintEv.exit
	movq	(%r13), %rdi
	movq	%r14, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN15CPercentPrinter11PrintStringEPKc, .Lfunc_end1-_ZN15CPercentPrinter11PrintStringEPKc
	.cfi_endproc

	.globl	_ZN15CPercentPrinter11PrintStringEPKw
	.p2align	4, 0x90
	.type	_ZN15CPercentPrinter11PrintStringEPKw,@function
_ZN15CPercentPrinter11PrintStringEPKw:  # @_ZN15CPercentPrinter11PrintStringEPKw
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 48
	subq	$128, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 176
.Lcfi26:
	.cfi_offset %rbx, -48
.Lcfi27:
	.cfi_offset %r12, -40
.Lcfi28:
	.cfi_offset %r13, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	32(%r13), %ebx
	testl	%ebx, %ebx
	je	.LBB2_1
# BB#2:
	jle	.LBB2_3
# BB#4:                                 # %.lr.ph.preheader.i.i
	decl	%ebx
	leaq	1(%rbx), %r15
	movq	%rsp, %rdi
	movl	$8, %esi
	movq	%r15, %rdx
	callq	memset
	leaq	1(%rsp,%rbx), %r12
	movl	$32, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	memset
	leaq	1(%rbx,%r12), %r12
	movl	$8, %esi
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	memset
	leaq	1(%rbx,%r12), %rax
	jmp	.LBB2_5
.LBB2_1:                                # %._ZN15CPercentPrinter10ClosePrintEv.exit_crit_edge
	addq	$40, %r13
	jmp	.LBB2_6
.LBB2_3:
	movq	%rsp, %rax
.LBB2_5:                                # %_ZL9ClearPrevPci.exit.i
	movb	$0, (%rax)
	movq	40(%r13), %rdi
	movq	%rsp, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$0, 32(%r13)
	leaq	40(%r13), %r13
.LBB2_6:                                # %_ZN15CPercentPrinter10ClosePrintEv.exit
	movq	(%r13), %rdi
	movq	%r14, %rsi
	callq	_ZN13CStdOutStreamlsEPKw
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZN15CPercentPrinter11PrintStringEPKw, .Lfunc_end2-_ZN15CPercentPrinter11PrintStringEPKw
	.cfi_endproc

	.globl	_ZN15CPercentPrinter12PrintNewLineEv
	.p2align	4, 0x90
	.type	_ZN15CPercentPrinter12PrintNewLineEv,@function
_ZN15CPercentPrinter12PrintNewLineEv:   # @_ZN15CPercentPrinter12PrintNewLineEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 160
.Lcfi36:
	.cfi_offset %rbx, -40
.Lcfi37:
	.cfi_offset %r12, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	32(%r12), %ebx
	testl	%ebx, %ebx
	je	.LBB3_1
# BB#2:
	jle	.LBB3_3
# BB#4:                                 # %.lr.ph.preheader.i.i
	decl	%ebx
	leaq	1(%rbx), %r14
	movq	%rsp, %rdi
	movl	$8, %esi
	movq	%r14, %rdx
	callq	memset
	leaq	1(%rsp,%rbx), %r15
	movl	$32, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	memset
	leaq	1(%rbx,%r15), %r15
	movl	$8, %esi
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	memset
	leaq	1(%rbx,%r15), %rax
	jmp	.LBB3_5
.LBB3_1:                                # %._ZN15CPercentPrinter10ClosePrintEv.exit_crit_edge
	addq	$40, %r12
	jmp	.LBB3_6
.LBB3_3:
	movq	%rsp, %rax
.LBB3_5:                                # %_ZL9ClearPrevPci.exit.i
	movb	$0, (%rax)
	movq	40(%r12), %rdi
	movq	%rsp, %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	movl	$0, 32(%r12)
	leaq	40(%r12), %r12
.LBB3_6:                                # %_ZN15CPercentPrinter10ClosePrintEv.exit
	movq	(%r12), %rdi
	movl	$.L.str, %esi
	callq	_ZN13CStdOutStreamlsEPKc
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	_ZN15CPercentPrinter12PrintNewLineEv, .Lfunc_end3-_ZN15CPercentPrinter12PrintNewLineEv
	.cfi_endproc

	.globl	_ZN15CPercentPrinter12RePrintRatioEv
	.p2align	4, 0x90
	.type	_ZN15CPercentPrinter12RePrintRatioEv,@function
_ZN15CPercentPrinter12RePrintRatioEv:   # @_ZN15CPercentPrinter12RePrintRatioEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 48
	subq	$160, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 208
.Lcfi46:
	.cfi_offset %rbx, -48
.Lcfi47:
	.cfi_offset %r12, -40
.Lcfi48:
	.cfi_offset %r13, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	24(%r14), %rcx
	testq	%rcx, %rcx
	je	.LBB4_1
# BB#2:
	imulq	$100, 16(%r14), %rax
	xorl	%edx, %edx
	divq	%rcx
	movq	%rax, %rdi
	jmp	.LBB4_3
.LBB4_1:
	xorl	%edi, %edi
.LBB4_3:
	movq	%rsp, %rbx
	movl	$10, %edx
	movq	%rbx, %rsi
	callq	_Z21ConvertUInt64ToStringyPcj
	movq	%rbx, %rdi
	callq	strlen
	cltq
	leaq	1(%rax), %r15
	movb	$37, (%rsp,%rax)
	movb	$0, 1(%rsp,%rax)
	cmpl	$3, %r15d
	movl	$4, %r12d
	cmovgl	%r15d, %r12d
	addl	$2, %r12d
	movl	32(%r14), %ecx
	cmpl	%ecx, %r12d
	cmovll	%ecx, %r12d
	testl	%ecx, %ecx
	je	.LBB4_5
# BB#4:
	leaq	32(%rsp), %rax
	testl	%ecx, %ecx
	jg	.LBB4_10
	jmp	.LBB4_13
.LBB4_5:                                # %.preheader28
	testl	%r12d, %r12d
	jle	.LBB4_6
# BB#7:                                 # %.lr.ph37.preheader
	leal	-1(%r12), %r13d
	leaq	1(%r13), %rdx
	leaq	32(%rsp), %rdi
	movl	$32, %esi
	callq	memset
	leaq	33(%rsp,%r13), %rax
	jmp	.LBB4_8
.LBB4_6:
	leaq	32(%rsp), %rax
.LBB4_8:                                # %._crit_edge38
	movl	%r12d, 32(%r14)
	movl	%r12d, %ecx
	testl	%ecx, %ecx
	jle	.LBB4_13
.LBB4_10:                               # %.lr.ph33.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph33
                                        # =>This Inner Loop Header: Depth=1
	movb	$8, (%rax,%rcx)
	incq	%rcx
	cmpl	32(%r14), %ecx
	jl	.LBB4_11
# BB#12:                                # %._crit_edge.loopexit
	addq	%rcx, %rax
.LBB4_13:                               # %._crit_edge
	movl	%r12d, 32(%r14)
	cmpl	%r12d, %r15d
	jge	.LBB4_15
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movb	$32, (%rax)
	incq	%rax
	incl	%r15d
	cmpl	32(%r14), %r15d
	jl	.LBB4_14
	.p2align	4, 0x90
.LBB4_15:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %ecx
	incq	%rbx
	movb	%cl, (%rax)
	incq	%rax
	testb	%cl, %cl
	jne	.LBB4_15
# BB#16:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit
	movq	40(%r14), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZN13CStdOutStreamlsEPKc
	movq	40(%r14), %rdi
	callq	_ZN13CStdOutStream5FlushEv
	movq	16(%r14), %rax
	movq	%rax, 8(%r14)
	addq	$160, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	_ZN15CPercentPrinter12RePrintRatioEv, .Lfunc_end4-_ZN15CPercentPrinter12RePrintRatioEv
	.cfi_endproc

	.globl	_ZN15CPercentPrinter10PrintRatioEv
	.p2align	4, 0x90
	.type	_ZN15CPercentPrinter10PrintRatioEv,@function
_ZN15CPercentPrinter10PrintRatioEv:     # @_ZN15CPercentPrinter10PrintRatioEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	movq	(%rdi), %rax
	movq	8(%rdi), %rdx
	leaq	(%rax,%rdx), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB5_4
# BB#1:
	addq	%rcx, %rax
	cmpq	%rdx, %rax
	jbe	.LBB5_4
# BB#2:
	cmpl	$0, 32(%rdi)
	je	.LBB5_4
# BB#3:
	retq
.LBB5_4:
	jmp	_ZN15CPercentPrinter12RePrintRatioEv # TAILCALL
.Lfunc_end5:
	.size	_ZN15CPercentPrinter10PrintRatioEv, .Lfunc_end5-_ZN15CPercentPrinter10PrintRatioEv
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n"
	.size	.L.str, 2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
