	.text
	.file	"stepanov_container.bc"
	.section	.text._ZNSt6vectorIdSaIdEED2Ev,"axG",@progbits,_ZNSt6vectorIdSaIdEED2Ev,comdat
	.weak	_ZNSt6vectorIdSaIdEED2Ev
	.p2align	4, 0x90
	.type	_ZNSt6vectorIdSaIdEED2Ev,@function
_ZNSt6vectorIdSaIdEED2Ev:               # @_ZNSt6vectorIdSaIdEED2Ev
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB0_1
# BB#2:
	jmp	_ZdlPv                  # TAILCALL
.LBB0_1:                                # %_ZNSt12_Vector_baseIdSaIdEED2Ev.exit2
	retq
.Lfunc_end0:
	.size	_ZNSt6vectorIdSaIdEED2Ev, .Lfunc_end0-_ZNSt6vectorIdSaIdEED2Ev
	.cfi_endproc

	.text
	.globl	_Z3runPFvPdS_iES_S_i
	.p2align	4, 0x90
	.type	_Z3runPFvPdS_iES_S_i,@function
_Z3runPFvPdS_iES_S_i:                   # @_Z3runPFvPdS_iES_S_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movl	%ecx, %ebx
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	testl	%ebx, %ebx
	jle	.LBB1_3
# BB#1:                                 # %.lr.ph.preheader
	incl	%ebx
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	*%r12
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB1_2
.LBB1_3:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_Z3runPFvPdS_iES_S_i, .Lfunc_end1-_Z3runPFvPdS_iES_S_i
	.cfi_endproc

	.globl	_Z10array_testPdS_i
	.p2align	4, 0x90
	.type	_Z10array_testPdS_i,@function
_Z10array_testPdS_i:                    # @_Z10array_testPdS_i
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r12, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	subq	%r12, %rbx
	movq	%rbx, %r15
	sarq	$3, %r15
	movl	$8, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
	testq	%r15, %r15
	je	.LBB2_2
# BB#1:
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
.LBB2_2:                                # %_ZSt4copyIPdS0_ET0_T_S2_S1_.exit
	leaq	(%r14,%r15,8), %rbx
	leaq	(,%r15,8), %rax
	testq	%rax, %rax
	je	.LBB2_3
# BB#4:                                 # %_ZSt4sortIPdEvT_S1_.exit
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	bsrq	%rax, %rdx
	xorq	$63, %rdx
	addq	%rdx, %rdx
	xorq	$126, %rdx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	movq	%r14, %rdx
	addq	$-8, %rdx
	leaq	-8(,%r15,8), %rcx
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	je	.LBB2_13
# BB#6:                                 #   in Loop: Header=BB2_5 Depth=1
	leaq	8(%rdx), %rax
	movsd	8(%rdx), %xmm0          # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	16(%rdx), %xmm0
	movq	%rax, %rdx
	jne	.LBB2_5
	jp	.LBB2_5
	jmp	.LBB2_7
.LBB2_3:
	movq	%r14, %rax
.LBB2_7:                                # %_ZSt15__adjacent_findIPdN9__gnu_cxx5__ops19_Iter_equal_to_iterEET_S4_S4_T0_.exit.i.i
	cmpq	%rbx, %rax
	je	.LBB2_13
# BB#8:
	leaq	8(%rax), %rcx
	jmp	.LBB2_9
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_9 Depth=1
	movsd	%xmm0, 8(%rax)
	addq	$8, %rax
	addq	$-8, %rcx
.LBB2_9:                                # %.outer.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_10 Depth 2
	addq	$8, %rcx
	.p2align	4, 0x90
.LBB2_10:                               #   Parent Loop BB2_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %rbx
	je	.LBB2_13
# BB#11:                                #   in Loop: Header=BB2_10 Depth=2
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm0, %xmm1
	jne	.LBB2_12
	jnp	.LBB2_10
	jmp	.LBB2_12
.LBB2_13:                               # %_ZSt6uniqueIPdET_S1_S1_.exit
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.Lfunc_end2:
	.size	_Z10array_testPdS_i, .Lfunc_end2-_Z10array_testPdS_i
	.cfi_endproc

	.globl	_Z19vector_pointer_testPdS_i
	.p2align	4, 0x90
	.type	_Z19vector_pointer_testPdS_i,@function
_Z19vector_pointer_testPdS_i:           # @_Z19vector_pointer_testPdS_i
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 64
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	subq	%r12, %rbx
	movq	%rbx, %r13
	sarq	$3, %r13
	je	.LBB3_19
# BB#1:
	movq	%r13, %rax
	shrq	$61, %rax
	jne	.LBB3_2
# BB#4:
.Ltmp0:
	movq	%rbx, %rdi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp1:
# BB#5:                                 # %_ZNSt6vectorIdSaIdEEC2IPdEET_S4_RKS0_.exit
	leaq	(%r14,%r13,8), %r15
	leaq	(,%r13,8), %rbp
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	testq	%rbp, %rbp
	je	.LBB3_6
# BB#7:
	movq	%r15, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	bsrq	%rax, %rdx
	xorq	$63, %rdx
	addq	%rdx, %rdx
	xorq	$126, %rdx
.Ltmp2:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
.Ltmp3:
# BB#8:                                 # %.noexc
.Ltmp4:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
.Ltmp5:
# BB#9:                                 # %.preheader.i.i.i.preheader
	movq	%r14, %rdx
	addq	$-8, %rdx
	leaq	-8(,%r13,8), %rcx
	.p2align	4, 0x90
.LBB3_10:                               # %.preheader.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	testq	%rcx, %rcx
	je	.LBB3_18
# BB#11:                                #   in Loop: Header=BB3_10 Depth=1
	leaq	8(%rdx), %rax
	movsd	8(%rdx), %xmm0          # xmm0 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	16(%rdx), %xmm0
	movq	%rax, %rdx
	jne	.LBB3_10
	jp	.LBB3_10
	jmp	.LBB3_12
.LBB3_19:                               # %_ZNSt6vectorIdSaIdEED2Ev.exit4
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_6:
	movq	%r14, %rax
.LBB3_12:                               # %_ZSt15__adjacent_findIPdN9__gnu_cxx5__ops19_Iter_equal_to_iterEET_S4_S4_T0_.exit.i.i
	cmpq	%r15, %rax
	je	.LBB3_18
# BB#13:
	leaq	8(%rax), %rcx
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_17:                               #   in Loop: Header=BB3_14 Depth=1
	movsd	%xmm0, 8(%rax)
	addq	$8, %rax
	addq	$-8, %rcx
.LBB3_14:                               # %.outer.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
	addq	$8, %rcx
	.p2align	4, 0x90
.LBB3_15:                               #   Parent Loop BB3_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rcx, %r15
	je	.LBB3_18
# BB#16:                                #   in Loop: Header=BB3_15 Depth=2
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm0, %xmm1
	jne	.LBB3_17
	jnp	.LBB3_15
	jmp	.LBB3_17
.LBB3_18:                               # %_ZSt6uniqueIPdET_S1_S1_.exit.thread
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp7:
	callq	_ZSt17__throw_bad_allocv
.Ltmp8:
# BB#3:                                 # %.noexc.i
.LBB3_20:                               # %.body
.Ltmp9:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_21:
.Ltmp6:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_Z19vector_pointer_testPdS_i, .Lfunc_end3-_Z19vector_pointer_testPdS_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp2-.Ltmp1           #   Call between .Ltmp1 and .Ltmp2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp5-.Ltmp2           #   Call between .Ltmp2 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end4:
	.size	__clang_call_terminate, .Lfunc_end4-__clang_call_terminate

	.text
	.globl	_Z20vector_iterator_testPdS_i
	.p2align	4, 0x90
	.type	_Z20vector_iterator_testPdS_i,@function
_Z20vector_iterator_testPdS_i:          # @_Z20vector_iterator_testPdS_i
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	subq	%r15, %rbx
	movq	%rbx, %r12
	sarq	$3, %r12
	je	.LBB5_1
# BB#3:
	movq	%r12, %rax
	shrq	$61, %rax
	jne	.LBB5_4
# BB#6:
.Ltmp10:
	movq	%rbx, %rdi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp11:
# BB#7:                                 # %_ZNSt6vectorIdSaIdEEC2IPdEET_S4_RKS0_.exit
	leaq	(,%r12,8), %r13
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	testq	%r13, %r13
	je	.LBB5_2
# BB#8:
	leaq	(%r14,%r12,8), %rbx
	movq	%rbx, %rax
	subq	%r14, %rax
	sarq	$3, %rax
	bsrq	%rax, %rdx
	xorq	$63, %rdx
	addq	%rdx, %rdx
	xorq	$126, %rdx
.Ltmp12:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
.Ltmp13:
# BB#9:                                 # %.noexc
.Ltmp14:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
.Ltmp15:
# BB#10:
	movq	%rsp, %rcx
	leaq	8(%rsp), %rdx
	movq	%r14, (%rsp)
	movq	%rcx, %rsi
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB5_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	%rax, (%rdi)
	movq	8(%rsp), %rsi
	addq	$8, %rsi
	movq	%rsi, 8(%rsp)
	cmpq	%rbx, %rsi
	je	.LBB5_12
# BB#13:                                #   in Loop: Header=BB5_11 Depth=1
	movq	(%rsp), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	ucomisd	(%rsi), %xmm0
	movq	%rdx, %rsi
	movq	%rcx, %rdi
	jne	.LBB5_11
	jp	.LBB5_11
	jmp	.LBB5_14
.LBB5_1:
	xorl	%r14d, %r14d
.LBB5_2:                                # %_ZSt4sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEEvT_S7_.exit.thread
	movq	%r14, (%rsp)
	movq	%r14, %rbx
	movq	%r14, %rax
	cmpq	%rbx, %rax
	jne	.LBB5_15
	jmp	.LBB5_19
.LBB5_12:
	movq	%rbx, %rax
.LBB5_14:                               # %_ZSt15__adjacent_findIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops19_Iter_equal_to_iterEET_S9_S9_T0_.exit.i.i
	cmpq	%rbx, %rax
	je	.LBB5_19
.LBB5_15:
	leaq	8(%rax), %rcx
	jmp	.LBB5_16
.LBB5_18:                               #   in Loop: Header=BB5_16 Depth=1
	movsd	%xmm0, 8(%rax)
	addq	$8, %rax
	.p2align	4, 0x90
.LBB5_16:                               # =>This Inner Loop Header: Depth=1
	addq	$8, %rcx
	cmpq	%rbx, %rcx
	je	.LBB5_19
# BB#17:                                #   in Loop: Header=BB5_16 Depth=1
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB5_18
	jnp	.LBB5_16
	jmp	.LBB5_18
.LBB5_19:                               # %_ZSt6uniqueIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEET_S7_S7_.exit
	testq	%r14, %r14
	je	.LBB5_20
# BB#24:
	movq	%r14, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB5_20:                               # %_ZNSt6vectorIdSaIdEED2Ev.exit9
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_4:
.Ltmp17:
	callq	_ZSt17__throw_bad_allocv
.Ltmp18:
# BB#5:                                 # %.noexc.i
.LBB5_21:                               # %.body
.Ltmp19:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_22:
.Ltmp16:
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end5:
	.size	_Z20vector_iterator_testPdS_i, .Lfunc_end5-_Z20vector_iterator_testPdS_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp15-.Ltmp12         #   Call between .Ltmp12 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin1   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin1   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end5-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z10deque_testPdS_i
	.p2align	4, 0x90
	.type	_Z10deque_testPdS_i,@function
_Z10deque_testPdS_i:                    # @_Z10deque_testPdS_i
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 288
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	subq	%r14, %rbx
	movq	%rbx, %rsi
	sarq	$3, %rsi
	movq	$0, 104(%rsp)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 64(%rsp)
	movapd	%xmm0, 48(%rsp)
	movapd	%xmm0, 32(%rsp)
	movapd	%xmm0, 16(%rsp)
	movapd	%xmm0, (%rsp)
.Ltmp20:
	movq	%rsp, %rdi
	callq	_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm
.Ltmp21:
# BB#1:                                 # %.noexc
.Ltmp23:
	movq	%rsp, %rdi
	leaq	104(%rsp), %rsi
	callq	_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd
.Ltmp24:
# BB#2:
	movq	16(%rsp), %r12
	testq	%rbx, %rbx
	jle	.LBB6_3
# BB#8:                                 # %.lr.ph.i.i.i.i
	movq	32(%rsp), %r13
	movq	40(%rsp), %rax
	shrq	$3, %rbx
	incq	%rbx
	movq	%r13, %rcx
	.p2align	4, 0x90
.LBB6_9:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdx
	movq	%rdx, (%r12)
	addq	$8, %r12
	cmpq	%rcx, %r12
	jne	.LBB6_11
# BB#10:                                #   in Loop: Header=BB6_9 Depth=1
	movq	8(%rax), %r12
	addq	$8, %rax
	leaq	512(%r12), %rcx
.LBB6_11:                               # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit.i.i.i.i
                                        #   in Loop: Header=BB6_9 Depth=1
	addq	$8, %r14
	decq	%rbx
	cmpq	$1, %rbx
	jg	.LBB6_9
# BB#12:                                # %.loopexit.loopexit
	movq	16(%rsp), %r12
	jmp	.LBB6_13
.LBB6_3:                                # %..loopexit_crit_edge
	movq	32(%rsp), %r13
.LBB6_13:                               # %.loopexit
	movq	40(%rsp), %rbx
	movq	48(%rsp), %r15
	movq	64(%rsp), %rsi
	movq	72(%rsp), %r14
	cmpq	%r15, %r12
	je	.LBB6_17
# BB#14:
	movq	24(%rsp), %rax
	movq	56(%rsp), %rbp
	movq	%r12, 104(%rsp)
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rax, 112(%rsp)
	movq	%r13, 120(%rsp)
	movq	%rbx, 128(%rsp)
	movq	%r15, 200(%rsp)
	movq	%rbp, 208(%rsp)
	movq	%rsi, 216(%rsp)
	movq	%r14, 224(%rsp)
	movq	%r14, %rax
	subq	%rbx, %rax
	movq	%r15, %rcx
	subq	%rbp, %rcx
	sarq	$3, %rcx
	movq	%r13, %rdx
	subq	%r12, %rdx
	sarq	$3, %rdx
	addq	%rcx, %rdx
	leaq	-64(%rdx,%rax,8), %rax
	bsrq	%rax, %rdx
	xorq	$63, %rdx
	addq	%rdx, %rdx
	xorq	$126, %rdx
.Ltmp26:
	leaq	104(%rsp), %rdi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	leaq	200(%rsp), %rsi
	callq	_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_
.Ltmp27:
# BB#15:                                # %.noexc6
	movq	%r12, 168(%rsp)
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	%rax, 176(%rsp)
	movq	%r13, 184(%rsp)
	movq	%rbx, 192(%rsp)
	movq	%r15, 136(%rsp)
	movq	%rbp, 144(%rsp)
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rax, 152(%rsp)
	movq	%r14, 160(%rsp)
.Ltmp28:
	leaq	168(%rsp), %rdi
	leaq	136(%rsp), %rsi
	callq	_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
.Ltmp29:
# BB#16:                                # %.noexc6._crit_edge
	movq	16(%rsp), %r12
	movq	32(%rsp), %r13
	movq	40(%rsp), %rbx
	movq	48(%rsp), %r15
	movq	64(%rsp), %rsi
	movq	72(%rsp), %r14
.LBB6_17:
	cmpq	%r15, %r12
	je	.LBB6_22
# BB#18:                                # %.preheader.i.i.preheader
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB6_19:                               # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r13, %rbp
	movq	%r12, %rax
	movq	%rdx, %rcx
	leaq	8(%rax), %r12
	cmpq	%rbp, %r12
	jne	.LBB6_21
# BB#20:                                #   in Loop: Header=BB6_19 Depth=1
	leaq	8(%rcx), %rdx
	movq	8(%rcx), %r12
	leaq	512(%r12), %r13
.LBB6_21:                               # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit.i.i.i
                                        #   in Loop: Header=BB6_19 Depth=1
	cmpq	%r15, %r12
	je	.LBB6_22
# BB#23:                                #   in Loop: Header=BB6_19 Depth=1
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	ucomisd	(%r12), %xmm0
	jne	.LBB6_19
	jp	.LBB6_19
	jmp	.LBB6_24
.LBB6_22:
	movq	%r15, %rax
	movq	%rsi, %rbp
	movq	%r14, %rcx
.LBB6_24:                               # %_ZSt15__adjacent_findISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops19_Iter_equal_to_iterEET_S7_S7_T0_.exit.i.i
	cmpq	%r15, %rax
	je	.LBB6_35
# BB#25:
	leaq	8(%rax), %rsi
	cmpq	%rbp, %rsi
	je	.LBB6_27
# BB#26:
	movq	%rcx, %rdi
	movq	%rbp, %rdx
	jmp	.LBB6_28
.LBB6_27:
	leaq	8(%rcx), %rdi
	movq	8(%rcx), %rsi
	leaq	512(%rsi), %rdx
	jmp	.LBB6_28
.LBB6_34:                               # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit2.i.i
                                        #   in Loop: Header=BB6_28 Depth=1
	movsd	%xmm0, (%rax)
	.p2align	4, 0x90
.LBB6_28:                               # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit6.i.i
                                        # =>This Inner Loop Header: Depth=1
	addq	$8, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB6_30
# BB#29:                                #   in Loop: Header=BB6_28 Depth=1
	movq	8(%rdi), %rsi
	addq	$8, %rdi
	leaq	512(%rsi), %rdx
.LBB6_30:                               # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit4.i.i
                                        #   in Loop: Header=BB6_28 Depth=1
	cmpq	%r15, %rsi
	je	.LBB6_35
# BB#31:                                #   in Loop: Header=BB6_28 Depth=1
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jne	.LBB6_32
	jnp	.LBB6_28
.LBB6_32:                               #   in Loop: Header=BB6_28 Depth=1
	addq	$8, %rax
	cmpq	%rbp, %rax
	jne	.LBB6_34
# BB#33:                                #   in Loop: Header=BB6_28 Depth=1
	movq	8(%rcx), %rax
	addq	$8, %rcx
	leaq	512(%rax), %rbp
	jmp	.LBB6_34
.LBB6_35:                               # %_ZSt6uniqueISt15_Deque_iteratorIdRdPdEET_S4_S4_.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_41
# BB#36:
	leaq	8(%r14), %rax
	cmpq	%rax, %rbx
	jae	.LBB6_40
# BB#37:                                # %.lr.ph.i.i3.i9.preheader
	addq	$-8, %rbx
	.p2align	4, 0x90
.LBB6_38:                               # %.lr.ph.i.i3.i9
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	addq	$8, %rbx
	callq	_ZdlPv
	cmpq	%r14, %rbx
	jb	.LBB6_38
# BB#39:                                # %_ZNSt11_Deque_baseIdSaIdEE16_M_destroy_nodesEPPdS3_.exit.loopexit.i6.i12
	movq	(%rsp), %rdi
.LBB6_40:                               # %_ZNSt11_Deque_baseIdSaIdEE16_M_destroy_nodesEPPdS3_.exit.i7.i13
	callq	_ZdlPv
.LBB6_41:                               # %_ZNSt5dequeIdSaIdEED2Ev.exit14
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_4:
.Ltmp25:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_49
# BB#5:
	movq	40(%rsp), %rbp
	movq	72(%rsp), %rbx
	leaq	8(%rbx), %rax
	cmpq	%rax, %rbp
	jae	.LBB6_48
# BB#6:                                 # %.lr.ph.i.i.i.preheader
	addq	$-8, %rbp
	.p2align	4, 0x90
.LBB6_7:                                # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	addq	$8, %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	jb	.LBB6_7
	jmp	.LBB6_47
.LBB6_42:
.Ltmp22:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_43:
.Ltmp30:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_49
# BB#44:
	movq	40(%rsp), %rbp
	movq	72(%rsp), %rbx
	leaq	8(%rbx), %rax
	cmpq	%rax, %rbp
	jae	.LBB6_48
# BB#45:                                # %.lr.ph.i.i3.i.preheader
	addq	$-8, %rbp
	.p2align	4, 0x90
.LBB6_46:                               # %.lr.ph.i.i3.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	addq	$8, %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	jb	.LBB6_46
.LBB6_47:                               # %_ZNSt11_Deque_baseIdSaIdEE16_M_destroy_nodesEPPdS3_.exit.loopexit.i6.i
	movq	(%rsp), %rdi
.LBB6_48:                               # %_ZNSt11_Deque_baseIdSaIdEE16_M_destroy_nodesEPPdS3_.exit.i7.i
	callq	_ZdlPv
.LBB6_49:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_Z10deque_testPdS_i, .Lfunc_end6-_Z10deque_testPdS_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\266\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp29-.Ltmp26         #   Call between .Ltmp26 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin2   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end6-.Ltmp29     #   Call between .Ltmp29 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z9list_testPdS_i
	.p2align	4, 0x90
	.type	_Z9list_testPdS_i,@function
_Z9list_testPdS_i:                      # @_Z9list_testPdS_i
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 64
.Lcfi60:
	.cfi_offset %rbx, -40
.Lcfi61:
	.cfi_offset %r12, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%rsp, %rax
	movq	%rax, (%rsp)
	movq	%rax, 8(%rsp)
	movq	$0, 16(%rsp)
	cmpq	%r14, %r15
	je	.LBB7_4
# BB#1:                                 # %.lr.ph.i.i.preheader
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB7_2:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
.Ltmp31:
	movl	$24, %edi
	callq	_Znwm
.Ltmp32:
# BB#3:                                 # %.noexc.i
                                        #   in Loop: Header=BB7_2 Depth=1
	movq	(%r15), %rcx
	movq	%rcx, 16(%rax)
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	_ZNSt8__detail15_List_node_base7_M_hookEPS0_
	incq	16(%rsp)
	addq	$8, %r15
	cmpq	%r15, %r14
	jne	.LBB7_2
.LBB7_4:                                # %_ZNSt7__cxx114listIdSaIdEEC2IPdEET_S5_RKS1_.exit
.Ltmp34:
	movq	%rsp, %r15
	movq	%r15, %rdi
	callq	_ZNSt7__cxx114listIdSaIdEE4sortEv
.Ltmp35:
# BB#5:
	movq	(%rsp), %r14
	cmpq	%r15, %r14
	je	.LBB7_14
# BB#6:                                 # %.preheader.i
	movq	(%r14), %rdi
	cmpq	%r15, %rdi
	je	.LBB7_14
# BB#7:                                 # %.lr.ph.i.preheader
	movq	%r14, %rbx
.LBB7_11:                               # %.lr.ph.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_12 Depth 2
	movq	%rbx, %r12
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB7_12:                               #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	ucomisd	16(%rbx), %xmm0
	jne	.LBB7_10
	jp	.LBB7_10
# BB#13:                                #   in Loop: Header=BB7_12 Depth=2
	decq	16(%rsp)
	callq	_ZNSt8__detail15_List_node_base9_M_unhookEv
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	(%r14), %rdi
	cmpq	%r15, %rdi
	movq	%rdi, %rbx
	jne	.LBB7_12
	jmp	.LBB7_14
	.p2align	4, 0x90
.LBB7_10:                               # %.outer.loopexit.i
                                        #   in Loop: Header=BB7_11 Depth=1
	movq	(%rbx), %rdi
	cmpq	%r15, %rdi
	movq	%rbx, %r14
	jne	.LBB7_11
.LBB7_14:                               # %_ZNSt7__cxx114listIdSaIdEE6uniqueEv.exit
	movq	(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB7_16
	.p2align	4, 0x90
.LBB7_15:                               # %.lr.ph.i.i4
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r15, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_15
.LBB7_16:                               # %_ZNSt7__cxx1110_List_baseIdSaIdEED2Ev.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB7_17:
.Ltmp36:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	cmpq	%r15, %rdi
	je	.LBB7_19
	.p2align	4, 0x90
.LBB7_18:                               # %.lr.ph.i.i6
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r15, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_18
	jmp	.LBB7_19
.LBB7_8:
.Ltmp33:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	cmpq	%r12, %rdi
	je	.LBB7_19
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%r12, %rbx
	movq	%rbx, %rdi
	jne	.LBB7_9
.LBB7_19:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_Z9list_testPdS_i, .Lfunc_end7-_Z9list_testPdS_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp31-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin3   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin3   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Lfunc_end7-.Ltmp35     #   Call between .Ltmp35 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt7__cxx114listIdSaIdEE4sortEv,"axG",@progbits,_ZNSt7__cxx114listIdSaIdEE4sortEv,comdat
	.weak	_ZNSt7__cxx114listIdSaIdEE4sortEv
	.p2align	4, 0x90
	.type	_ZNSt7__cxx114listIdSaIdEE4sortEv,@function
_ZNSt7__cxx114listIdSaIdEE4sortEv:      # @_ZNSt7__cxx114listIdSaIdEE4sortEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi68:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 56
	subq	$1576, %rsp             # imm = 0x628
.Lcfi70:
	.cfi_def_cfa_offset 1632
.Lcfi71:
	.cfi_offset %rbx, -56
.Lcfi72:
	.cfi_offset %r12, -48
.Lcfi73:
	.cfi_offset %r13, -40
.Lcfi74:
	.cfi_offset %r14, -32
.Lcfi75:
	.cfi_offset %r15, -24
.Lcfi76:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	(%r13), %rsi
	cmpq	%r13, %rsi
	je	.LBB8_45
# BB#1:
	cmpq	%r13, (%rsi)
	je	.LBB8_45
# BB#2:
	movq	%rsp, %r15
	movq	%r15, (%rsp)
	movq	%r15, 8(%rsp)
	movq	$0, 16(%rsp)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_3:                                # =>This Inner Loop Header: Depth=1
	leaq	32(%rsp,%rax), %rcx
	movq	%rcx, 32(%rsp,%rax)
	movq	%rcx, 40(%rsp,%rax)
	movq	$0, 48(%rsp,%rax)
	leaq	56(%rsp,%rax), %rcx
	movq	%rcx, 56(%rsp,%rax)
	movq	%rcx, 64(%rsp,%rax)
	movq	$0, 72(%rsp,%rax)
	addq	$48, %rax
	cmpq	$1536, %rax             # imm = 0x600
	jne	.LBB8_3
# BB#4:                                 # %.preheader
	leaq	32(%rsp), %rbx
	leaq	1568(%rsp), %rbp
	movq	%r15, %rdi
	movq	%rbx, %r14
	movq	%r13, 24(%rsp)          # 8-byte Spill
	cmpq	%rsi, %rdi
	jne	.LBB8_6
	jmp	.LBB8_8
	.p2align	4, 0x90
.LBB8_23:                               #   in Loop: Header=BB8_8 Depth=1
	movq	%r14, %r12
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB8_24:                               # %._crit_edge69
                                        #   in Loop: Header=BB8_8 Depth=1
	movb	$1, %bl
	jmp	.LBB8_25
	.p2align	4, 0x90
.LBB8_10:                               #   in Loop: Header=BB8_8 Depth=1
	xorl	%ebx, %ebx
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB8_25:                               # %._crit_edge69
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZNSt8__detail15_List_node_base4swapERS0_S1_
	movq	16(%r12), %rax
	movq	16(%rsp), %rcx
	movq	%rcx, 16(%r12)
	movq	%rax, 16(%rsp)
	leaq	24(%r14), %rax
	testb	%bl, %bl
	cmovneq	%rax, %r14
	movq	(%r13), %rsi
	cmpq	%r13, %rsi
	je	.LBB8_27
# BB#26:                                # %._crit_edge69._crit_edge
                                        #   in Loop: Header=BB8_8 Depth=1
	movq	(%rsp), %rdi
	leaq	32(%rsp), %rbx
	cmpq	%rsi, %rdi
	je	.LBB8_8
.LBB8_6:
	movq	(%rsi), %rdx
	cmpq	%rdi, %rdx
	je	.LBB8_8
# BB#7:
	callq	_ZNSt8__detail15_List_node_base11_M_transferEPS0_S1_
	incq	16(%rsp)
	decq	16(%r13)
.LBB8_8:                                # %_ZNSt7__cxx114listIdSaIdEE6spliceESt14_List_iteratorIdERS2_S4_.exit.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_9 Depth 2
                                        #       Child Loop BB8_13 Depth 3
                                        #         Child Loop BB8_15 Depth 4
	movq	%rbx, %r12
	cmpq	%r14, %rbx
	je	.LBB8_24
	.p2align	4, 0x90
.LBB8_9:                                # %.lr.ph68
                                        #   Parent Loop BB8_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_13 Depth 3
                                        #         Child Loop BB8_15 Depth 4
	movq	(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB8_10
# BB#11:                                #   in Loop: Header=BB8_9 Depth=2
	cmpq	%r15, %r12
	je	.LBB8_22
# BB#12:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_9 Depth=2
	movq	(%rsp), %rsi
.LBB8_13:                               # %.lr.ph.i
                                        #   Parent Loop BB8_8 Depth=1
                                        #     Parent Loop BB8_9 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_15 Depth 4
	cmpq	%r15, %rsi
	je	.LBB8_18
# BB#14:                                # %.lr.ph.split.preheader.i
                                        #   in Loop: Header=BB8_13 Depth=3
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph.split.i
                                        #   Parent Loop BB8_8 Depth=1
                                        #     Parent Loop BB8_9 Depth=2
                                        #       Parent Loop BB8_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB8_16
# BB#17:                                #   in Loop: Header=BB8_15 Depth=4
	movq	(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB8_15
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_16:                               # %.outer.i
                                        #   in Loop: Header=BB8_13 Depth=3
	movq	(%rsi), %r13
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	_ZNSt8__detail15_List_node_base11_M_transferEPS0_S1_
	cmpq	%r12, %rbx
	movq	%r13, %rsi
	jne	.LBB8_13
	jmp	.LBB8_19
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_9 Depth=2
	movq	%rsi, %r13
.LBB8_19:                               # %.critedge.i
                                        #   in Loop: Header=BB8_9 Depth=2
	cmpq	%r15, %r13
	je	.LBB8_21
# BB#20:                                #   in Loop: Header=BB8_9 Depth=2
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	_ZNSt8__detail15_List_node_base11_M_transferEPS0_S1_
.LBB8_21:                               #   in Loop: Header=BB8_9 Depth=2
	movq	16(%rsp), %rax
	addq	%rax, 16(%r12)
	movq	$0, 16(%rsp)
.LBB8_22:                               # %_ZNSt7__cxx114listIdSaIdEE6spliceESt14_List_iteratorIdERS2_S4_.exit
                                        #   in Loop: Header=BB8_9 Depth=2
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZNSt8__detail15_List_node_base4swapERS0_S1_
	movq	16(%r12), %rax
	movq	16(%rsp), %rcx
	movq	%rcx, 16(%r12)
	movq	%rax, 16(%rsp)
	addq	$24, %r12
	cmpq	%r14, %r12
	jne	.LBB8_9
	jmp	.LBB8_23
.LBB8_27:
	leaq	56(%rsp), %r12
	cmpq	%r14, %r12
	jne	.LBB8_29
	jmp	.LBB8_39
	.p2align	4, 0x90
.LBB8_35:                               #   in Loop: Header=BB8_29 Depth=1
	movq	%rsi, %r13
.LBB8_36:                               # %.critedge.i51
                                        #   in Loop: Header=BB8_29 Depth=1
	cmpq	%r15, %r13
	je	.LBB8_38
# BB#37:                                #   in Loop: Header=BB8_29 Depth=1
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	_ZNSt8__detail15_List_node_base11_M_transferEPS0_S1_
.LBB8_38:                               #   in Loop: Header=BB8_29 Depth=1
	movq	-8(%r12), %rax
	addq	%rax, 16(%r12)
	movq	$0, -8(%r12)
	addq	$24, %r12
	cmpq	%r14, %r12
	je	.LBB8_39
.LBB8_29:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_30 Depth 2
                                        #       Child Loop BB8_32 Depth 3
	leaq	-24(%r12), %r15
	movq	-24(%r12), %rsi
	movq	(%r12), %rbx
	cmpq	%r12, %rbx
	je	.LBB8_35
.LBB8_30:                               # %.lr.ph.i44
                                        #   Parent Loop BB8_29 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_32 Depth 3
	cmpq	%r15, %rsi
	je	.LBB8_35
# BB#31:                                # %.lr.ph.split.preheader.i46
                                        #   in Loop: Header=BB8_30 Depth=2
	movsd	16(%rsi), %xmm0         # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB8_32:                               # %.lr.ph.split.i48
                                        #   Parent Loop BB8_29 Depth=1
                                        #     Parent Loop BB8_30 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB8_33
# BB#34:                                #   in Loop: Header=BB8_32 Depth=3
	movq	(%rbx), %rbx
	cmpq	%r12, %rbx
	jne	.LBB8_32
	jmp	.LBB8_35
	.p2align	4, 0x90
.LBB8_33:                               # %.outer.i49
                                        #   in Loop: Header=BB8_30 Depth=2
	movq	(%rsi), %r13
	movq	%rbx, %rdi
	movq	%r13, %rdx
	callq	_ZNSt8__detail15_List_node_base11_M_transferEPS0_S1_
	cmpq	%r12, %rbx
	movq	%r13, %rsi
	jne	.LBB8_30
	jmp	.LBB8_36
.LBB8_39:                               # %._crit_edge
	leaq	-24(%r14), %rsi
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZNSt8__detail15_List_node_base4swapERS0_S1_
	movq	-8(%r14), %rax
	movq	16(%rbx), %rcx
	movq	%rcx, -8(%r14)
	movq	%rax, 16(%rbx)
	leaq	32(%rsp), %r14
	.p2align	4, 0x90
.LBB8_40:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_41 Depth 2
	movq	-24(%rbp), %rdi
	addq	$-24, %rbp
	cmpq	%rbp, %rdi
	je	.LBB8_42
	.p2align	4, 0x90
.LBB8_41:                               # %.lr.ph.i.i54
                                        #   Parent Loop BB8_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rbx
	callq	_ZdlPv
	cmpq	%rbp, %rbx
	movq	%rbx, %rdi
	jne	.LBB8_41
.LBB8_42:                               # %_ZNSt7__cxx1110_List_baseIdSaIdEED2Ev.exit55
                                        #   in Loop: Header=BB8_40 Depth=1
	cmpq	%r14, %rbp
	jne	.LBB8_40
# BB#43:
	movq	(%rsp), %rdi
	movq	%rsp, %rbx
	cmpq	%rbx, %rdi
	je	.LBB8_45
	.p2align	4, 0x90
.LBB8_44:                               # %.lr.ph.i.i37
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rbp
	callq	_ZdlPv
	cmpq	%rbx, %rbp
	movq	%rbp, %rdi
	jne	.LBB8_44
.LBB8_45:
	addq	$1576, %rsp             # imm = 0x628
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZNSt7__cxx114listIdSaIdEE4sortEv, .Lfunc_end8-_ZNSt7__cxx114listIdSaIdEE4sortEv
	.cfi_endproc

	.text
	.globl	_Z8set_testPdS_i
	.p2align	4, 0x90
	.type	_Z8set_testPdS_i,@function
_Z8set_testPdS_i:                       # @_Z8set_testPdS_i
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi81:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi83:
	.cfi_def_cfa_offset 112
.Lcfi84:
	.cfi_offset %rbx, -56
.Lcfi85:
	.cfi_offset %r12, -48
.Lcfi86:
	.cfi_offset %r13, -40
.Lcfi87:
	.cfi_offset %r14, -32
.Lcfi88:
	.cfi_offset %r15, -24
.Lcfi89:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	leaq	16(%rsp), %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rsp)
	movups	%xmm0, 16(%rsp)
	movq	$0, 48(%rsp)
	movq	%r15, 32(%rsp)
	movq	%r15, 40(%rsp)
	cmpq	%r14, %r12
	je	.LBB9_1
# BB#2:                                 # %.lr.ph.i
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB9_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_8 Depth 2
	testq	%r13, %r13
	je	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	40(%rsp), %rbx
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	32(%rbx), %xmm0
	ja	.LBB9_13
.LBB9_5:                                #   in Loop: Header=BB9_3 Depth=1
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB9_6
# BB#7:                                 # %.lr.ph.i30.i
                                        #   in Loop: Header=BB9_3 Depth=1
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB9_8:                                # %.backedge.i35.i
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbx
	movsd	32(%rbx), %xmm1         # xmm1 = mem[0],zero
	leaq	16(%rbx), %rax
	leaq	24(%rbx), %rcx
	ucomisd	%xmm0, %xmm1
	cmovaq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB9_8
# BB#9:                                 # %._crit_edge.i36.i
                                        #   in Loop: Header=BB9_3 Depth=1
	ucomisd	%xmm0, %xmm1
	movq	%rbx, %rax
	ja	.LBB9_10
	jmp	.LBB9_12
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_3 Depth=1
	movq	%r15, %rbx
.LBB9_10:                               # %._crit_edge.thread.i29.i
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpq	32(%rsp), %rbx
	je	.LBB9_13
# BB#11:                                #   in Loop: Header=BB9_3 Depth=1
	movq	%rbx, %rdi
	callq	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
.LBB9_12:                               #   in Loop: Header=BB9_3 Depth=1
	ucomisd	32(%rax), %xmm0
	jbe	.LBB9_19
.LBB9_13:                               # %.noexc
                                        #   in Loop: Header=BB9_3 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_19
# BB#14:                                #   in Loop: Header=BB9_3 Depth=1
	cmpq	%rbx, %r15
	je	.LBB9_15
# BB#16:                                #   in Loop: Header=BB9_3 Depth=1
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%r12), %xmm0
	seta	%bpl
	jmp	.LBB9_17
.LBB9_15:                               #   in Loop: Header=BB9_3 Depth=1
	movb	$1, %bpl
.LBB9_17:                               # %_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE10_M_insert_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdEPSt18_Rb_tree_node_baseSB_RKdRT_.exit.i.i
                                        #   in Loop: Header=BB9_3 Depth=1
.Ltmp37:
	movl	$40, %edi
	callq	_Znwm
.Ltmp38:
# BB#18:                                # %.noexc2
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	(%r12), %rcx
	movq	%rcx, 32(%rax)
	movzbl	%bpl, %edi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_
	movq	48(%rsp), %r13
	incq	%r13
	movq	%r13, 48(%rsp)
.LBB9_19:                               # %_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE17_M_insert_unique_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_.exit.i
                                        #   in Loop: Header=BB9_3 Depth=1
	addq	$8, %r12
	cmpq	%r14, %r12
	jne	.LBB9_3
# BB#20:                                # %_ZNSt3setIdSt4lessIdESaIdEEC2IPdEET_S6_.exit.loopexit
	movq	24(%rsp), %rsi
	jmp	.LBB9_21
.LBB9_1:
	xorl	%esi, %esi
.LBB9_21:                               # %_ZNSt3setIdSt4lessIdESaIdEEC2IPdEET_S6_.exit
	leaq	8(%rsp), %rdi
	callq	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_22:
.Ltmp39:
	movq	%rax, %rbx
	movq	24(%rsp), %rsi
.Ltmp40:
	leaq	8(%rsp), %rdi
	callq	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
.Ltmp41:
# BB#23:                                # %_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEED2Ev.exit.i
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_24:
.Ltmp42:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_Z8set_testPdS_i, .Lfunc_end9-_Z8set_testPdS_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp37-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin4   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp40-.Ltmp38         #   Call between .Ltmp38 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin4   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp41     #   Call between .Ltmp41 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z13multiset_testPdS_i
	.p2align	4, 0x90
	.type	_Z13multiset_testPdS_i,@function
_Z13multiset_testPdS_i:                 # @_Z13multiset_testPdS_i
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 48
	subq	$64, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 112
.Lcfi96:
	.cfi_offset %rbx, -48
.Lcfi97:
	.cfi_offset %r12, -40
.Lcfi98:
	.cfi_offset %r13, -32
.Lcfi99:
	.cfi_offset %r14, -24
.Lcfi100:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %rbx
	leaq	16(%rsp), %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rsp)
	movups	%xmm0, 16(%rsp)
	movq	$0, 48(%rsp)
	movq	%r14, 32(%rsp)
	movq	%r14, 40(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 56(%rsp)
	cmpq	%r15, %rbx
	je	.LBB10_16
# BB#1:
	leaq	8(%rsp), %r12
	leaq	56(%rsp), %r13
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
.Ltmp43:
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_
.Ltmp44:
# BB#3:                                 # %.noexc.i
                                        #   in Loop: Header=BB10_2 Depth=1
	addq	$8, %rbx
	cmpq	%rbx, %r15
	jne	.LBB10_2
# BB#4:                                 # %.loopexit.loopexit
	movq	32(%rsp), %r15
	cmpq	%r14, %r15
	jne	.LBB10_6
	jmp	.LBB10_15
.LBB10_16:                              # %..loopexit_crit_edge
	movq	%r14, %r15
	cmpq	%r14, %r15
	je	.LBB10_15
.LBB10_6:                               # %.lr.ph.lr.ph
	movq	%r15, %rdi
	callq	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base
	cmpq	%r14, %rax
	je	.LBB10_15
# BB#7:                                 # %.lr.ph.lr.ph29
	movq	%r15, %rbx
.LBB10_12:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_13 Depth 2
	movq	%rbx, %r12
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB10_13:                              #   Parent Loop BB10_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	32(%r15), %xmm0         # xmm0 = mem[0],zero
	ucomisd	32(%rbx), %xmm0
	jne	.LBB10_11
	jp	.LBB10_11
# BB#14:                                # %.lr.ph.split
                                        #   in Loop: Header=BB10_13 Depth=2
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZSt28_Rb_tree_rebalance_for_erasePSt18_Rb_tree_node_baseRS_
	movq	%rax, %rdi
	callq	_ZdlPv
	decq	48(%rsp)
	movq	%r12, %rdi
	callq	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base
	movq	%rax, %rbx
	cmpq	%r14, %rbx
	jne	.LBB10_13
	jmp	.LBB10_15
	.p2align	4, 0x90
.LBB10_11:                              # %.outer.loopexit
                                        #   in Loop: Header=BB10_12 Depth=1
	movq	%rbx, %rdi
	callq	_ZSt18_Rb_tree_incrementPKSt18_Rb_tree_node_base
	cmpq	%r14, %rax
	movq	%rbx, %r15
	jne	.LBB10_12
.LBB10_15:                              # %.critedge
	movq	24(%rsp), %rsi
	leaq	8(%rsp), %rdi
	callq	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
	addq	$64, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB10_8:
.Ltmp45:
	movq	%rax, %rbx
	movq	24(%rsp), %rsi
.Ltmp46:
	leaq	8(%rsp), %rdi
	callq	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
.Ltmp47:
# BB#9:                                 # %_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEED2Ev.exit.i
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB10_10:
.Ltmp48:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_Z13multiset_testPdS_i, .Lfunc_end10-_Z13multiset_testPdS_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp43-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin5   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp46-.Ltmp44         #   Call between .Ltmp44 and .Ltmp46
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin5   #     jumps to .Ltmp48
	.byte	1                       #   On action: 1
	.long	.Ltmp47-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Lfunc_end10-.Ltmp47    #   Call between .Ltmp47 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_Z10initializePdS_
	.p2align	4, 0x90
	.type	_Z10initializePdS_,@function
_Z10initializePdS_:                     # @_Z10initializePdS_
	.cfi_startproc
# BB#0:
	cmpq	%rsi, %rdi
	je	.LBB11_8
# BB#1:                                 # %.lr.ph.preheader
	leaq	-8(%rsi), %rax
	subq	%rdi, %rax
	movl	%eax, %ecx
	shrl	$3, %ecx
	incl	%ecx
	andq	$7, %rcx
	je	.LBB11_2
# BB#3:                                 # %.lr.ph.prol.preheader
	negq	%rcx
	xorpd	%xmm0, %xmm0
	movsd	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, (%rdi)
	addq	$8, %rdi
	addsd	%xmm1, %xmm0
	incq	%rcx
	jne	.LBB11_4
	jmp	.LBB11_5
.LBB11_2:
	xorpd	%xmm0, %xmm0
.LBB11_5:                               # %.lr.ph.prol.loopexit
	cmpq	$56, %rax
	jb	.LBB11_8
# BB#6:                                 # %.lr.ph.preheader.new
	movsd	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB11_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, (%rdi)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rdi)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rdi)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 24(%rdi)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 32(%rdi)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 40(%rdi)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 48(%rdi)
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 56(%rdi)
	addsd	%xmm1, %xmm0
	addq	$64, %rdi
	cmpq	%rsi, %rdi
	jne	.LBB11_7
.LBB11_8:                               # %._crit_edge
	retq
.Lfunc_end11:
	.size	_Z10initializePdS_, .Lfunc_end11-_Z10initializePdS_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4604418534313441775     # double 0.69314718055994529
	.text
	.globl	_Z6logtwod
	.p2align	4, 0x90
	.type	_Z6logtwod,@function
_Z6logtwod:                             # @_Z6logtwod
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 16
	callq	log
	divsd	.LCPI12_0(%rip), %xmm0
	popq	%rax
	retq
.Lfunc_end12:
	.size	_Z6logtwod, .Lfunc_end12-_Z6logtwod
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4604418534313441775     # double 0.69314718055994529
.LCPI13_1:
	.quad	4716115549927240618     # double 19931568.569324173
	.text
	.globl	_Z15number_of_testsi
	.p2align	4, 0x90
	.type	_Z15number_of_testsi,@function
_Z15number_of_testsi:                   # @_Z15number_of_testsi
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 16
	cvtsi2sdl	%edi, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	callq	log
	divsd	.LCPI13_0(%rip), %xmm0
	mulsd	(%rsp), %xmm0           # 8-byte Folded Reload
	movsd	.LCPI13_1(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	popq	%rcx
	retq
.Lfunc_end13:
	.size	_Z15number_of_testsi, .Lfunc_end13-_Z15number_of_testsi
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4604418534313441775     # double 0.69314718055994529
.LCPI14_1:
	.quad	4716115549927240618     # double 19931568.569324173
.LCPI14_2:
	.quad	4607182418800017408     # double 1
	.text
	.globl	_Z9run_testsi
	.p2align	4, 0x90
	.type	_Z9run_testsi,@function
_Z9run_testsi:                          # @_Z9run_testsi
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r15
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi105:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi106:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi107:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi108:
	.cfi_def_cfa_offset 64
.Lcfi109:
	.cfi_offset %rbx, -48
.Lcfi110:
	.cfi_offset %r12, -40
.Lcfi111:
	.cfi_offset %r13, -32
.Lcfi112:
	.cfi_offset %r14, -24
.Lcfi113:
	.cfi_offset %r15, -16
	movl	%edi, %ebx
	cvtsi2sdl	%ebx, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	log
	divsd	.LCPI14_0(%rip), %xmm0
	mulsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	.LCPI14_1(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	callq	floor
	leal	(%rbx,%rbx), %eax
	movslq	%eax, %r12
	movq	result_times(%rip), %rax
	movq	%rax, result_times+8(%rip)
	testl	%ebx, %ebx
	je	.LBB14_1
# BB#2:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	js	.LBB14_3
# BB#5:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIdEE8allocateERS1_m.exit.i.i.i.i
	leaq	(,%r12,8), %r15
.Ltmp49:
	movq	%r15, %rdi
	callq	_Znwm
	movq	%rax, %r14
.Ltmp50:
# BB#6:                                 # %.lr.ph.i.preheader
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%r15, %rdx
	callq	memset
	movslq	%ebx, %rax
	leaq	-8(,%rax,8), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB14_7
# BB#8:                                 # %.lr.ph.i.prol.preheader
	negq	%rsi
	xorpd	%xmm2, %xmm2
	movsd	.LCPI14_2(%rip), %xmm1  # xmm1 = mem[0],zero
	movq	%r14, %rcx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB14_9:                               # %.lr.ph.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm2, (%rcx)
	addq	$8, %rcx
	addsd	%xmm1, %xmm2
	incq	%rsi
	jne	.LBB14_9
	jmp	.LBB14_10
.LBB14_1:                               # %_ZNSt6vectorIdSaIdEEC2EmRKdRKS0_.exit.thread
	movslq	%ebx, %rax
	shlq	$3, %rax
	xorl	%r14d, %r14d
	jmp	.LBB14_13
.LBB14_7:
	xorpd	%xmm2, %xmm2
	movq	%r14, %rcx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB14_10:                              # %.lr.ph.i.prol.loopexit
	leaq	(%r14,%rax,8), %rax
	cmpq	$56, %rdx
	jb	.LBB14_13
# BB#11:                                # %.lr.ph.i.preheader.new
	movsd	.LCPI14_2(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB14_12:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm2, (%rcx)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rcx)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 16(%rcx)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 24(%rcx)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 32(%rcx)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 40(%rcx)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 48(%rcx)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 56(%rcx)
	addsd	%xmm1, %xmm2
	addq	$64, %rcx
	cmpq	%rax, %rcx
	jne	.LBB14_12
.LBB14_13:                              # %_Z10initializePdS_.exit
	leaq	(%r14,%r12,8), %r15
	cmpq	%r15, %rax
	je	.LBB14_21
# BB#14:                                # %.lr.ph.i52.preheader
	leaq	-8(%r14,%r12,8), %rcx
	subq	%rax, %rcx
	movl	%ecx, %edx
	shrl	$3, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB14_15
# BB#16:                                # %.lr.ph.i52.prol.preheader
	negq	%rdx
	xorpd	%xmm2, %xmm2
	movsd	.LCPI14_2(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB14_17:                              # %.lr.ph.i52.prol
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm2, (%rax)
	addq	$8, %rax
	addsd	%xmm1, %xmm2
	incq	%rdx
	jne	.LBB14_17
	jmp	.LBB14_18
.LBB14_15:
	xorpd	%xmm2, %xmm2
.LBB14_18:                              # %.lr.ph.i52.prol.loopexit
	cmpq	$56, %rcx
	jb	.LBB14_21
# BB#19:                                # %.lr.ph.i52.preheader.new
	movsd	.LCPI14_2(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB14_20:                              # %.lr.ph.i52
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm2, (%rax)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rax)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 16(%rax)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 24(%rax)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 32(%rax)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 40(%rax)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 48(%rax)
	addsd	%xmm1, %xmm2
	movsd	%xmm2, 56(%rax)
	addsd	%xmm1, %xmm2
	addq	$64, %rax
	cmpq	%r15, %rax
	jne	.LBB14_20
.LBB14_21:                              # %_Z10initializePdS_.exit53
	cvttsd2si	%xmm0, %r13d
	cmpq	%r15, %r14
	je	.LBB14_26
# BB#22:                                # %.preheader.i
	leaq	8(%r14), %rbx
	cmpq	%r15, %rbx
	je	.LBB14_26
# BB#23:                                # %.lr.ph.i58
	leaq	-8(,%r12,8), %r12
	.p2align	4, 0x90
.LBB14_24:                              # =>This Inner Loop Header: Depth=1
	callq	rand
	cltq
	movq	%rbx, %rcx
	subq	%r14, %rcx
	sarq	$3, %rcx
	incq	%rcx
	cqto
	idivq	%rcx
	leaq	(%r14,%rdx,8), %rax
	cmpq	%rax, %rbx
	je	.LBB14_25
# BB#61:                                #   in Loop: Header=BB14_24 Depth=1
	movq	(%rbx), %rcx
	movq	(%rax), %rdx
	movq	%rdx, (%rbx)
	movq	%rcx, (%rax)
.LBB14_25:                              # %.backedge.i
                                        #   in Loop: Header=BB14_24 Depth=1
	addq	$8, %rbx
	addq	$-8, %r12
	jne	.LBB14_24
.LBB14_26:                              # %_ZSt14random_shuffleIPdEvT_S1_.exit
	testl	%r13d, %r13d
	jle	.LBB14_48
# BB#27:                                # %.lr.ph.i60.preheader
	leal	1(%r13), %ebx
	.p2align	4, 0x90
.LBB14_28:                              # %.lr.ph.i60
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %edx
.Ltmp54:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z10array_testPdS_i
.Ltmp55:
# BB#29:                                # %.noexc61
                                        #   in Loop: Header=BB14_28 Depth=1
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB14_28
# BB#30:                                # %.lr.ph.i64.preheader
	leal	1(%r13), %ebx
	.p2align	4, 0x90
.LBB14_31:                              # %.lr.ph.i64
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %edx
.Ltmp57:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z19vector_pointer_testPdS_i
.Ltmp58:
# BB#32:                                # %.noexc65
                                        #   in Loop: Header=BB14_31 Depth=1
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB14_31
# BB#33:                                # %.lr.ph.i68.preheader
	leal	1(%r13), %ebx
	.p2align	4, 0x90
.LBB14_34:                              # %.lr.ph.i68
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %edx
.Ltmp60:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z20vector_iterator_testPdS_i
.Ltmp61:
# BB#35:                                # %.noexc69
                                        #   in Loop: Header=BB14_34 Depth=1
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB14_34
# BB#36:                                # %.lr.ph.i72.preheader
	leal	1(%r13), %ebx
	.p2align	4, 0x90
.LBB14_37:                              # %.lr.ph.i72
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %edx
.Ltmp63:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z10deque_testPdS_i
.Ltmp64:
# BB#38:                                # %.noexc73
                                        #   in Loop: Header=BB14_37 Depth=1
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB14_37
# BB#39:                                # %.lr.ph.i76.preheader
	leal	1(%r13), %ebx
	.p2align	4, 0x90
.LBB14_40:                              # %.lr.ph.i76
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %edx
.Ltmp66:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z9list_testPdS_i
.Ltmp67:
# BB#41:                                # %.noexc77
                                        #   in Loop: Header=BB14_40 Depth=1
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB14_40
# BB#42:                                # %.lr.ph.i55.preheader
	leal	1(%r13), %ebx
	.p2align	4, 0x90
.LBB14_43:                              # %.lr.ph.i55
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%rbx), %edx
.Ltmp69:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z8set_testPdS_i
.Ltmp70:
# BB#44:                                # %.noexc56
                                        #   in Loop: Header=BB14_43 Depth=1
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB14_43
# BB#45:                                # %.lr.ph.i48.preheader
	incl	%r13d
	.p2align	4, 0x90
.LBB14_46:                              # %.lr.ph.i48
                                        # =>This Inner Loop Header: Depth=1
	leal	-2(%r13), %edx
.Ltmp72:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_Z13multiset_testPdS_i
.Ltmp73:
# BB#47:                                # %.noexc49
                                        #   in Loop: Header=BB14_46 Depth=1
	decl	%r13d
	cmpl	$1, %r13d
	jg	.LBB14_46
.LBB14_48:                              # %_Z3runPFvPdS_iES_S_i.exit
	testq	%r14, %r14
	je	.LBB14_49
# BB#62:
	movq	%r14, %rdi
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	_ZdlPv                  # TAILCALL
.LBB14_49:                              # %_ZNSt6vectorIdSaIdEED2Ev.exit47
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB14_3:                               # %.noexc.i.i
.Ltmp51:
	callq	_ZSt17__throw_bad_allocv
.Ltmp52:
# BB#4:                                 # %.noexc
.LBB14_50:
.Ltmp53:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_51:                              # %.loopexit
.Ltmp74:
	jmp	.LBB14_58
.LBB14_52:                              # %.loopexit.split-lp.loopexit
.Ltmp71:
	jmp	.LBB14_58
.LBB14_53:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp68:
	jmp	.LBB14_58
.LBB14_54:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp65:
	jmp	.LBB14_58
.LBB14_55:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp62:
	jmp	.LBB14_58
.LBB14_56:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp59:
	jmp	.LBB14_58
.LBB14_57:                              # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp56:
.LBB14_58:                              # %.loopexit.split-lp
	movq	%rax, %rbx
	testq	%r14, %r14
	je	.LBB14_60
# BB#59:
	movq	%r14, %rdi
	callq	_ZdlPv
.LBB14_60:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_Z9run_testsi, .Lfunc_end14-_Z9run_testsi
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\237\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp49-.Lfunc_begin6   #   Call between .Lfunc_begin6 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp53-.Lfunc_begin6   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin6   # >> Call Site 3 <<
	.long	.Ltmp54-.Ltmp50         #   Call between .Ltmp50 and .Ltmp54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin6   # >> Call Site 4 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin6   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin6   # >> Call Site 5 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin6   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin6   # >> Call Site 6 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin6   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin6   # >> Call Site 7 <<
	.long	.Ltmp64-.Ltmp63         #   Call between .Ltmp63 and .Ltmp64
	.long	.Ltmp65-.Lfunc_begin6   #     jumps to .Ltmp65
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin6   # >> Call Site 8 <<
	.long	.Ltmp67-.Ltmp66         #   Call between .Ltmp66 and .Ltmp67
	.long	.Ltmp68-.Lfunc_begin6   #     jumps to .Ltmp68
	.byte	0                       #   On action: cleanup
	.long	.Ltmp69-.Lfunc_begin6   # >> Call Site 9 <<
	.long	.Ltmp70-.Ltmp69         #   Call between .Ltmp69 and .Ltmp70
	.long	.Ltmp71-.Lfunc_begin6   #     jumps to .Ltmp71
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin6   # >> Call Site 10 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin6   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp51-.Lfunc_begin6   # >> Call Site 11 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin6   #     jumps to .Ltmp53
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin6   # >> Call Site 12 <<
	.long	.Lfunc_end14-.Ltmp52    #   Call between .Ltmp52 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 16
	movl	$100000, %edi           # imm = 0x186A0
	callq	_Z9run_testsi
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end15:
	.size	main, .Lfunc_end15-main
	.cfi_endproc

	.section	.text._ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE,"axG",@progbits,_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE,comdat
	.weak	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE,@function
_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE: # @_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB16_2
	.p2align	4, 0x90
.LBB16_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
	movq	16(%rbx), %r15
	movq	%rbx, %rdi
	callq	_ZdlPv
	testq	%r15, %r15
	movq	%r15, %rbx
	jne	.LBB16_1
.LBB16_2:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE, .Lfunc_end16-_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE8_M_eraseEPSt13_Rb_tree_nodeIdE
	.cfi_endproc

	.section	.text._ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_,@function
_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_: # @_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi123:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 48
.Lcfi126:
	.cfi_offset %rbx, -48
.Lcfi127:
	.cfi_offset %r12, -40
.Lcfi128:
	.cfi_offset %r13, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB17_31
# BB#1:                                 # %.lr.ph
	leaq	8(%r12), %r13
	.p2align	4, 0x90
.LBB17_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_11 Depth 2
                                        #       Child Loop BB17_12 Depth 3
                                        #       Child Loop BB17_14 Depth 3
	testq	%r15, %r15
	je	.LBB17_17
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	shrq	$4, %rax
	leaq	(%r12,%rax,8), %rdx
	leaq	-8(%r14), %rcx
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	movsd	(%r12,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB17_6
# BB#4:                                 #   in Loop: Header=BB17_2 Depth=1
	ucomisd	%xmm2, %xmm0
	movq	%rdx, %rax
	movapd	%xmm2, %xmm3
	ja	.LBB17_9
# BB#5:                                 #   in Loop: Header=BB17_2 Depth=1
	ucomisd	%xmm1, %xmm0
	cmovbeq	%r13, %rcx
	maxsd	%xmm1, %xmm0
	movq	%rcx, %rax
	jmp	.LBB17_8
	.p2align	4, 0x90
.LBB17_6:                               #   in Loop: Header=BB17_2 Depth=1
	ucomisd	%xmm1, %xmm0
	movq	%r13, %rax
	movapd	%xmm1, %xmm3
	ja	.LBB17_9
# BB#7:                                 #   in Loop: Header=BB17_2 Depth=1
	ucomisd	%xmm2, %xmm0
	cmovaq	%rcx, %rdx
	maxsd	%xmm2, %xmm0
	movq	%rdx, %rax
.LBB17_8:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_S4_T0_.exit.i
                                        #   in Loop: Header=BB17_2 Depth=1
	movapd	%xmm0, %xmm3
.LBB17_9:                               # %_ZSt22__move_median_to_firstIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_S4_T0_.exit.i
                                        #   in Loop: Header=BB17_2 Depth=1
	decq	%r15
	movq	(%r12), %rcx
	movsd	%xmm3, (%r12)
	movq	%rcx, (%rax)
	movq	%r14, %rax
	movq	%r13, %rcx
	jmp	.LBB17_11
	.p2align	4, 0x90
.LBB17_10:                              #   in Loop: Header=BB17_11 Depth=2
	movsd	%xmm2, (%rbx)
	movsd	%xmm0, (%rax)
.LBB17_11:                              #   Parent Loop BB17_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB17_12 Depth 3
                                        #       Child Loop BB17_14 Depth 3
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB17_12:                              #   Parent Loop BB17_2 Depth=1
                                        #     Parent Loop BB17_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rcx
	ucomisd	%xmm0, %xmm1
	ja	.LBB17_12
# BB#13:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB17_11 Depth=2
	leaq	-8(%rcx), %rbx
	.p2align	4, 0x90
.LBB17_14:                              # %.preheader.i.i
                                        #   Parent Loop BB17_2 Depth=1
                                        #     Parent Loop BB17_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rax), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rax
	ucomisd	%xmm1, %xmm2
	ja	.LBB17_14
# BB#15:                                #   in Loop: Header=BB17_11 Depth=2
	cmpq	%rax, %rbx
	jb	.LBB17_10
# BB#16:                                # %_ZSt27__unguarded_partition_pivotIPdN9__gnu_cxx5__ops15_Iter_less_iterEET_S4_S4_T0_.exit
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	movq	%rbx, %rax
	subq	%r12, %rax
	cmpq	$128, %rax
	movq	%rbx, %r14
	jg	.LBB17_2
	jmp	.LBB17_31
.LBB17_17:                              # %.lr.ph.i5.i
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.p2align	4, 0x90
.LBB17_18:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_20 Depth 2
                                        #     Child Loop BB17_26 Depth 2
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	movq	(%r12), %rax
	movq	%rax, -8(%r14)
	leaq	-8(%r14), %r14
	movq	%r14, %r8
	subq	%r12, %r8
	movq	%r8, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rcx
	cmpq	$2, %rcx
	jl	.LBB17_21
# BB#19:                                # %.lr.ph.i.i.i6.i.preheader
                                        #   in Loop: Header=BB17_18 Depth=1
	shrq	$63, %rcx
	leaq	-1(%rdx,%rcx), %rsi
	sarq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB17_20:                              # %.lr.ph.i.i.i6.i
                                        #   Parent Loop BB17_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%rdi), %rbx
	leaq	2(%rdi,%rdi), %rax
	leaq	1(%rdi,%rdi), %rcx
	movsd	8(%r12,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%r12,%rbx,8), %xmm1
	cmovbeq	%rax, %rcx
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r12,%rdi,8)
	cmpq	%rsi, %rcx
	movq	%rcx, %rdi
	jl	.LBB17_20
	jmp	.LBB17_22
	.p2align	4, 0x90
.LBB17_21:                              #   in Loop: Header=BB17_18 Depth=1
	xorl	%ecx, %ecx
.LBB17_22:                              # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB17_18 Depth=1
	testb	$1, %dl
	jne	.LBB17_25
# BB#23:                                #   in Loop: Header=BB17_18 Depth=1
	leaq	-2(%rdx), %rax
	shrq	$63, %rax
	leaq	-2(%rdx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %rcx
	jne	.LBB17_25
# BB#24:                                #   in Loop: Header=BB17_18 Depth=1
	leaq	(%rcx,%rcx), %rax
	movq	8(%r12,%rax,8), %rax
	movq	%rax, (%r12,%rcx,8)
	leaq	1(%rcx,%rcx), %rcx
.LBB17_25:                              #   in Loop: Header=BB17_18 Depth=1
	testq	%rcx, %rcx
	jle	.LBB17_29
	.p2align	4, 0x90
.LBB17_26:                              # %.lr.ph.i.i.i.i.i
                                        #   Parent Loop BB17_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rsi
	movq	%rsi, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rdx
	sarq	%rdx
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB17_29
# BB#27:                                #   in Loop: Header=BB17_26 Depth=2
	movsd	%xmm1, (%r12,%rcx,8)
	cmpq	$1, %rsi
	movq	%rdx, %rcx
	jg	.LBB17_26
	jmp	.LBB17_30
	.p2align	4, 0x90
.LBB17_29:                              #   in Loop: Header=BB17_18 Depth=1
	movq	%rcx, %rdx
.LBB17_30:                              # %_ZSt10__pop_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_T0_.exit.i.i
                                        #   in Loop: Header=BB17_18 Depth=1
	movsd	%xmm0, (%r12,%rdx,8)
	cmpq	$8, %r8
	jg	.LBB17_18
.LBB17_31:                              # %_ZSt14__partial_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_S4_T0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_, .Lfunc_end17-_ZSt16__introsort_loopIPdlN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_T1_
	.cfi_endproc

	.section	.text._ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,"axG",@progbits,_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,comdat
	.weak	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.p2align	4, 0x90
	.type	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,@function
_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_: # @_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi136:
	.cfi_def_cfa_offset 64
.Lcfi137:
	.cfi_offset %rbx, -48
.Lcfi138:
	.cfi_offset %r12, -40
.Lcfi139:
	.cfi_offset %r13, -32
.Lcfi140:
	.cfi_offset %r14, -24
.Lcfi141:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB18_20
# BB#1:
	movl	$1, %ebx
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB18_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_18 Depth 2
	leaq	(%r12,%rbx,8), %r13
	movsd	(%r12,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB18_17
# BB#3:                                 # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i
                                        #   in Loop: Header=BB18_2 Depth=1
	leaq	(,%rbx,8), %rdx
	subq	%rdx, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, (%r12)
	jmp	.LBB18_4
	.p2align	4, 0x90
.LBB18_17:                              #   in Loop: Header=BB18_2 Depth=1
	movq	%r13, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB18_18:                              # %._crit_edge.i
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB18_18
# BB#19:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rdx, 8(%rcx)
.LBB18_4:                               # %.backedge.i
                                        #   in Loop: Header=BB18_2 Depth=1
	incq	%rbx
	cmpq	$16, %rbx
	movq	%r13, %rdi
	jne	.LBB18_2
# BB#5:                                 # %_ZSt16__insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_.exit
	movq	%r12, %rax
	subq	$-128, %rax
	cmpq	%r14, %rax
	je	.LBB18_28
# BB#6:                                 # %.lr.ph.i28
	leaq	-136(%r14), %rdx
	subq	%r12, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB18_10
# BB#7:
	movq	%rsp, %rax
	leaq	136(%r12), %rdx
	.p2align	4, 0x90
.LBB18_8:                               # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %rsi
	movq	%rsi, (%rax)
	movq	-16(%rdx), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rsi
	movd	%rsi, %xmm1
	ucomisd	%xmm1, %xmm0
	movq	%rdx, %rax
	ja	.LBB18_8
# BB#9:                                 # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i34.prol
	movq	%rsi, (%rdx)
	addq	$136, %r12
	movq	%r12, %rax
.LBB18_10:                              # %.prol.loopexit
	testq	%rcx, %rcx
	je	.LBB18_28
# BB#11:                                # %.lr.ph.i28.new
	movq	%rsp, %rcx
	.p2align	4, 0x90
.LBB18_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_13 Depth 2
                                        #     Child Loop BB18_15 Depth 2
	movq	%rax, %rsi
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB18_13:                              #   Parent Loop BB18_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rdi
	movq	%rdi, (%rdx)
	movq	-8(%rsi), %xmm0         # xmm0 = mem[0],zero
	movq	%rsi, %rdx
	leaq	-8(%rsi), %rsi
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB18_13
# BB#14:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i34
                                        #   in Loop: Header=BB18_12 Depth=1
	movq	%rdi, 8(%rsi)
	leaq	8(%rax), %rdx
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB18_15:                              #   Parent Loop BB18_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx), %rdi
	movq	%rdi, (%rsi)
	movq	-8(%rdx), %xmm0         # xmm0 = mem[0],zero
	movq	%rdx, %rsi
	leaq	-8(%rdx), %rdx
	movq	(%rsp), %rdi
	movd	%rdi, %xmm1
	ucomisd	%xmm1, %xmm0
	ja	.LBB18_15
# BB#16:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i34.1
                                        #   in Loop: Header=BB18_12 Depth=1
	movq	%rdi, 8(%rdx)
	addq	$16, %rax
	cmpq	%r14, %rax
	jne	.LBB18_12
	jmp	.LBB18_28
.LBB18_20:
	cmpq	%r14, %r12
	je	.LBB18_28
# BB#21:                                # %.preheader.i
	leaq	8(%r12), %rax
	cmpq	%r14, %rax
	je	.LBB18_28
# BB#22:                                # %.lr.ph.i
	movq	%rsp, %r15
	movq	%r12, %rdi
	.p2align	4, 0x90
.LBB18_23:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_30 Depth 2
	movq	%rax, %rbx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movq	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB18_29
# BB#24:                                #   in Loop: Header=BB18_23 Depth=1
	movq	%rbx, %rdx
	subq	%r12, %rdx
	movq	%rdx, %rax
	sarq	$3, %rax
	je	.LBB18_26
# BB#25:                                #   in Loop: Header=BB18_23 Depth=1
	shlq	$3, %rax
	subq	%rax, %rdi
	addq	$16, %rdi
	movq	%r12, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
.LBB18_26:                              # %_ZSt13copy_backwardIPdS0_ET0_T_S2_S1_.exit.i15
                                        #   in Loop: Header=BB18_23 Depth=1
	movsd	%xmm1, (%r12)
	jmp	.LBB18_27
	.p2align	4, 0x90
.LBB18_29:                              #   in Loop: Header=BB18_23 Depth=1
	movq	%rbx, %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB18_30:                              # %._crit_edge.i23
                                        #   Parent Loop BB18_23 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm1, (%rax)
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	movq	%rcx, %rax
	leaq	-8(%rcx), %rcx
	movq	(%rsp), %rdx
	movd	%rdx, %xmm0
	ucomisd	%xmm0, %xmm1
	ja	.LBB18_30
# BB#31:                                # %_ZSt25__unguarded_linear_insertIPdN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i24
                                        #   in Loop: Header=BB18_23 Depth=1
	movq	%rdx, 8(%rcx)
.LBB18_27:                              # %.backedge.i17
                                        #   in Loop: Header=BB18_23 Depth=1
	leaq	8(%rbx), %rax
	cmpq	%r14, %rax
	movq	%rbx, %rdi
	jne	.LBB18_23
.LBB18_28:                              # %_ZSt26__unguarded_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_, .Lfunc_end18-_ZSt22__final_insertion_sortIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,"axG",@progbits,_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,comdat
	.weak	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_,@function
_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_: # @_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_startproc
# BB#0:
	subq	%rdi, %rsi
	cmpq	$16, %rsi
	jl	.LBB19_23
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r9
	sarq	%r9
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r11
	sarq	%r11
	testb	$1, %sil
	jne	.LBB19_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r9,%r9), %r8
	movq	%r9, %r10
	.p2align	4, 0x90
.LBB19_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_5 Depth 2
                                        #     Child Loop BB19_9 Depth 2
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %r11
	movq	%r10, %rdx
	jle	.LBB19_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB19_3 Depth=1
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB19_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB19_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rsi
	leaq	2(%rax,%rax), %rcx
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rsi,8), %xmm1
	cmovbeq	%rcx, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	movq	%rdx, %rax
	jl	.LBB19_5
.LBB19_6:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB19_3 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB19_8
# BB#7:                                 #   in Loop: Header=BB19_3 Depth=1
	movq	(%rdi,%r8,8), %rax
	movq	%rax, (%rdi,%r9,8)
	movq	%r8, %rdx
.LBB19_8:                               #   in Loop: Header=BB19_3 Depth=1
	cmpq	%r10, %rdx
	jle	.LBB19_12
	.p2align	4, 0x90
.LBB19_9:                               # %.lr.ph.i.i.us
                                        #   Parent Loop BB19_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB19_12
# BB#10:                                #   in Loop: Header=BB19_9 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jg	.LBB19_9
	jmp	.LBB19_13
	.p2align	4, 0x90
.LBB19_12:                              #   in Loop: Header=BB19_3 Depth=1
	movq	%rdx, %rax
.LBB19_13:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_.exit.us
                                        #   in Loop: Header=BB19_3 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	testq	%r10, %r10
	leaq	-1(%r10), %r10
	jne	.LBB19_3
	jmp	.LBB19_23
	.p2align	4, 0x90
.LBB19_14:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_16 Depth 2
                                        #     Child Loop BB19_18 Depth 2
	movsd	(%rdi,%r9,8), %xmm0     # xmm0 = mem[0],zero
	cmpq	%r9, %r11
	movq	%r9, %rsi
	jle	.LBB19_22
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB19_14 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB19_16:                              # %.lr.ph.i
                                        #   Parent Loop BB19_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	leaq	(%rax,%rax), %rcx
	leaq	2(%rax,%rax), %rsi
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rcx,8), %xmm1
	cmovbeq	%rsi, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	jl	.LBB19_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB19_14 Depth=1
	cmpq	%r9, %rdx
	jle	.LBB19_21
	.p2align	4, 0x90
.LBB19_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB19_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rsi
	sarq	%rsi
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB19_21
# BB#19:                                #   in Loop: Header=BB19_18 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r9, %rsi
	movq	%rsi, %rdx
	jg	.LBB19_18
	jmp	.LBB19_22
	.p2align	4, 0x90
.LBB19_21:                              #   in Loop: Header=BB19_14 Depth=1
	movq	%rdx, %rsi
.LBB19_22:                              # %_ZSt13__adjust_heapIPdldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S5_T1_T2_.exit
                                        #   in Loop: Header=BB19_14 Depth=1
	movsd	%xmm0, (%rdi,%rsi,8)
	testq	%r9, %r9
	leaq	-1(%r9), %r9
	jne	.LBB19_14
.LBB19_23:                              # %.loopexit
	retq
.Lfunc_end19:
	.size	_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_, .Lfunc_end19-_ZSt11__make_heapIPdN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S4_T0_
	.cfi_endproc

	.section	.text._ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_,@function
_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_: # @_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 48
.Lcfi147:
	.cfi_offset %rbx, -48
.Lcfi148:
	.cfi_offset %r12, -40
.Lcfi149:
	.cfi_offset %r13, -32
.Lcfi150:
	.cfi_offset %r14, -24
.Lcfi151:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	%r14, %rax
	subq	%r12, %rax
	cmpq	$129, %rax
	jl	.LBB20_31
# BB#1:                                 # %.lr.ph
	leaq	8(%r12), %r13
	.p2align	4, 0x90
.LBB20_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_11 Depth 2
                                        #       Child Loop BB20_12 Depth 3
                                        #       Child Loop BB20_14 Depth 3
	testq	%r15, %r15
	je	.LBB20_17
# BB#3:                                 #   in Loop: Header=BB20_2 Depth=1
	shrq	$4, %rax
	leaq	(%r12,%rax,8), %rdx
	leaq	-8(%r14), %rcx
	movsd	8(%r12), %xmm1          # xmm1 = mem[0],zero
	movsd	(%r12,%rax,8), %xmm2    # xmm2 = mem[0],zero
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB20_6
# BB#4:                                 #   in Loop: Header=BB20_2 Depth=1
	ucomisd	%xmm2, %xmm0
	movq	%rdx, %rax
	movapd	%xmm2, %xmm3
	ja	.LBB20_9
# BB#5:                                 #   in Loop: Header=BB20_2 Depth=1
	ucomisd	%xmm1, %xmm0
	cmovbeq	%r13, %rcx
	maxsd	%xmm1, %xmm0
	movq	%rcx, %rax
	jmp	.LBB20_8
	.p2align	4, 0x90
.LBB20_6:                               #   in Loop: Header=BB20_2 Depth=1
	ucomisd	%xmm1, %xmm0
	movq	%r13, %rax
	movapd	%xmm1, %xmm3
	ja	.LBB20_9
# BB#7:                                 #   in Loop: Header=BB20_2 Depth=1
	ucomisd	%xmm2, %xmm0
	cmovaq	%rcx, %rdx
	maxsd	%xmm2, %xmm0
	movq	%rdx, %rax
.LBB20_8:                               # %_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_S9_T0_.exit.i
                                        #   in Loop: Header=BB20_2 Depth=1
	movapd	%xmm0, %xmm3
.LBB20_9:                               # %_ZSt22__move_median_to_firstIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_S9_T0_.exit.i
                                        #   in Loop: Header=BB20_2 Depth=1
	decq	%r15
	movq	(%r12), %rcx
	movsd	%xmm3, (%r12)
	movq	%rcx, (%rax)
	movq	%r13, %rax
	movq	%r14, %rcx
	jmp	.LBB20_11
	.p2align	4, 0x90
.LBB20_10:                              #   in Loop: Header=BB20_11 Depth=2
	movsd	%xmm2, (%rbx)
	movsd	%xmm0, (%rcx)
.LBB20_11:                              #   Parent Loop BB20_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB20_12 Depth 3
                                        #       Child Loop BB20_14 Depth 3
	movsd	(%r12), %xmm1           # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB20_12:                              #   Parent Loop BB20_2 Depth=1
                                        #     Parent Loop BB20_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	addq	$8, %rax
	ucomisd	%xmm0, %xmm1
	ja	.LBB20_12
# BB#13:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB20_11 Depth=2
	leaq	-8(%rax), %rbx
	.p2align	4, 0x90
.LBB20_14:                              # %.preheader.i.i
                                        #   Parent Loop BB20_2 Depth=1
                                        #     Parent Loop BB20_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rcx), %xmm2         # xmm2 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	%xmm1, %xmm2
	ja	.LBB20_14
# BB#15:                                #   in Loop: Header=BB20_11 Depth=2
	cmpq	%rcx, %rbx
	jb	.LBB20_10
# BB#16:                                # %_ZSt27__unguarded_partition_pivotIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEET_S9_S9_T0_.exit
                                        #   in Loop: Header=BB20_2 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	movq	%rbx, %rax
	subq	%r12, %rax
	cmpq	$128, %rax
	movq	%rbx, %r14
	jg	.LBB20_2
	jmp	.LBB20_31
.LBB20_17:                              # %.lr.ph.i8.preheader.i
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.p2align	4, 0x90
.LBB20_18:                              # %.lr.ph.i8.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_20 Depth 2
                                        #     Child Loop BB20_26 Depth 2
	movsd	-8(%r14), %xmm0         # xmm0 = mem[0],zero
	movq	(%r12), %rax
	movq	%rax, -8(%r14)
	leaq	-8(%r14), %r14
	movq	%r14, %r8
	subq	%r12, %r8
	movq	%r8, %rdx
	sarq	$3, %rdx
	leaq	-1(%rdx), %rcx
	cmpq	$2, %rcx
	jl	.LBB20_21
# BB#19:                                # %.lr.ph.i.i11.i.preheader
                                        #   in Loop: Header=BB20_18 Depth=1
	shrq	$63, %rcx
	leaq	-1(%rdx,%rcx), %rsi
	sarq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB20_20:                              # %.lr.ph.i.i11.i
                                        #   Parent Loop BB20_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rdi,%rdi), %rbx
	leaq	2(%rdi,%rdi), %rax
	leaq	1(%rdi,%rdi), %rcx
	movsd	8(%r12,%rbx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%r12,%rbx,8), %xmm1
	cmovbeq	%rax, %rcx
	movq	(%r12,%rcx,8), %rax
	movq	%rax, (%r12,%rdi,8)
	cmpq	%rsi, %rcx
	movq	%rcx, %rdi
	jl	.LBB20_20
	jmp	.LBB20_22
	.p2align	4, 0x90
.LBB20_21:                              #   in Loop: Header=BB20_18 Depth=1
	xorl	%ecx, %ecx
.LBB20_22:                              # %._crit_edge.i.i13.i
                                        #   in Loop: Header=BB20_18 Depth=1
	testb	$1, %dl
	jne	.LBB20_25
# BB#23:                                #   in Loop: Header=BB20_18 Depth=1
	leaq	-2(%rdx), %rax
	shrq	$63, %rax
	leaq	-2(%rdx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %rcx
	jne	.LBB20_25
# BB#24:                                #   in Loop: Header=BB20_18 Depth=1
	leaq	(%rcx,%rcx), %rax
	movq	8(%r12,%rax,8), %rax
	movq	%rax, (%r12,%rcx,8)
	leaq	1(%rcx,%rcx), %rcx
.LBB20_25:                              #   in Loop: Header=BB20_18 Depth=1
	testq	%rcx, %rcx
	jle	.LBB20_29
	.p2align	4, 0x90
.LBB20_26:                              # %.lr.ph.i.i.i18.i
                                        #   Parent Loop BB20_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rcx), %rsi
	movq	%rsi, %rax
	shrq	$63, %rax
	leaq	-1(%rcx,%rax), %rdx
	sarq	%rdx
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB20_29
# BB#27:                                #   in Loop: Header=BB20_26 Depth=2
	movsd	%xmm1, (%r12,%rcx,8)
	cmpq	$1, %rsi
	movq	%rdx, %rcx
	jg	.LBB20_26
	jmp	.LBB20_30
	.p2align	4, 0x90
.LBB20_29:                              #   in Loop: Header=BB20_18 Depth=1
	movq	%rcx, %rdx
.LBB20_30:                              # %_ZSt10__pop_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_.exit20.i
                                        #   in Loop: Header=BB20_18 Depth=1
	movsd	%xmm0, (%r12,%rdx,8)
	cmpq	$8, %r8
	jg	.LBB20_18
.LBB20_31:                              # %_ZSt14__partial_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_S9_T0_.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_, .Lfunc_end20-_ZSt16__introsort_loopIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEElNS0_5__ops15_Iter_less_iterEEvT_S9_T0_T1_
	.cfi_endproc

	.section	.text._ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,"axG",@progbits,_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,comdat
	.weak	_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.p2align	4, 0x90
	.type	_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,@function
_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_: # @_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi157:
	.cfi_def_cfa_offset 64
.Lcfi158:
	.cfi_offset %rbx, -48
.Lcfi159:
	.cfi_offset %r12, -40
.Lcfi160:
	.cfi_offset %r13, -32
.Lcfi161:
	.cfi_offset %r14, -24
.Lcfi162:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%r14, %rax
	subq	%r15, %rax
	cmpq	$129, %rax
	jl	.LBB21_21
# BB#1:                                 # %.lr.ph.i11
	leaq	8(%r15), %r12
	movl	$1, %ebx
	movq	%r15, %r13
	.p2align	4, 0x90
.LBB21_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_6 Depth 2
	movq	%r13, %rdi
	leaq	(%r15,%rbx,8), %r13
	movsd	(%r15,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	movq	(%r15), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB21_4
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=1
	leaq	(,%rbx,8), %rdx
	subq	%rdx, %rdi
	addq	$16, %rdi
	movq	%r15, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%r15, %rax
	jmp	.LBB21_7
	.p2align	4, 0x90
.LBB21_4:                               #   in Loop: Header=BB21_2 Depth=1
	movq	(%rdi), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movq	%r13, %rax
	jbe	.LBB21_7
# BB#5:                                 # %.lr.ph.i.i16.preheader
                                        #   in Loop: Header=BB21_2 Depth=1
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB21_6:                               # %.lr.ph.i.i16
                                        #   Parent Loop BB21_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rax), %rcx
	movq	%rcx, (%rax)
	movq	-16(%rax), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rax), %rax
	ucomisd	%xmm1, %xmm0
	ja	.LBB21_6
.LBB21_7:                               # %_ZSt25__unguarded_linear_insertIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops14_Val_less_iterEEvT_T0_.exit.i18
                                        #   in Loop: Header=BB21_2 Depth=1
	movsd	%xmm1, (%rax)
	incq	%rbx
	addq	$8, %r12
	cmpq	$16, %rbx
	jne	.LBB21_2
# BB#8:                                 # %_ZSt16__insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_.exit19
	movq	%r15, %rax
	subq	$-128, %rax
	cmpq	%r14, %rax
	je	.LBB21_31
# BB#9:                                 # %.lr.ph.i6.preheader
	leaq	-136(%r14), %rdx
	subq	%r15, %rdx
	movq	%rdx, %rcx
	shrq	$3, %rcx
	btl	$3, %edx
	jb	.LBB21_14
# BB#10:                                # %.lr.ph.i6.prol
	movq	128(%r15), %rdx
	movd	%rdx, %xmm0
	movsd	120(%r15), %xmm1        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB21_13
	.p2align	4, 0x90
.LBB21_11:                              # %.lr.ph.i.i8.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rax), %rsi
	movq	%rsi, (%rax)
	movsd	-16(%rax), %xmm1        # xmm1 = mem[0],zero
	leaq	-8(%rax), %rax
	ucomisd	%xmm0, %xmm1
	ja	.LBB21_11
.LBB21_13:                              # %_ZSt25__unguarded_linear_insertIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops14_Val_less_iterEEvT_T0_.exit.i9.prol
	movq	%rdx, (%rax)
	addq	$136, %r15
	movq	%r15, %rax
.LBB21_14:                              # %.lr.ph.i6.prol.loopexit
	testq	%rcx, %rcx
	je	.LBB21_31
# BB#15:                                # %.lr.ph.i6.preheader.new
	leaq	8(%rax), %rcx
	.p2align	4, 0x90
.LBB21_16:                              # %.lr.ph.i6
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_18 Depth 2
                                        #     Child Loop BB21_33 Depth 2
	movq	(%rax), %rdx
	movd	%rdx, %xmm0
	movsd	-8(%rax), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movq	%rax, %rsi
	jbe	.LBB21_19
# BB#17:                                # %.lr.ph.i.i8.preheader
                                        #   in Loop: Header=BB21_16 Depth=1
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB21_18:                              # %.lr.ph.i.i8
                                        #   Parent Loop BB21_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi), %rdi
	movq	%rdi, (%rsi)
	movsd	-16(%rsi), %xmm1        # xmm1 = mem[0],zero
	leaq	-8(%rsi), %rsi
	ucomisd	%xmm0, %xmm1
	ja	.LBB21_18
.LBB21_19:                              # %_ZSt25__unguarded_linear_insertIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops14_Val_less_iterEEvT_T0_.exit.i9
                                        #   in Loop: Header=BB21_16 Depth=1
	movq	%rdx, (%rsi)
	movq	8(%rax), %rdx
	movd	%rdx, %xmm0
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB21_20
# BB#32:                                # %.lr.ph.i.i8.preheader.1
                                        #   in Loop: Header=BB21_16 Depth=1
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB21_33:                              # %.lr.ph.i.i8.1
                                        #   Parent Loop BB21_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi), %rdi
	movq	%rdi, (%rsi)
	movsd	-16(%rsi), %xmm1        # xmm1 = mem[0],zero
	leaq	-8(%rsi), %rsi
	ucomisd	%xmm0, %xmm1
	ja	.LBB21_33
	jmp	.LBB21_34
	.p2align	4, 0x90
.LBB21_20:                              #   in Loop: Header=BB21_16 Depth=1
	leaq	8(%rax), %rsi
.LBB21_34:                              # %_ZSt25__unguarded_linear_insertIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops14_Val_less_iterEEvT_T0_.exit.i9.1
                                        #   in Loop: Header=BB21_16 Depth=1
	movq	%rdx, (%rsi)
	addq	$16, %rax
	addq	$16, %rcx
	cmpq	%r14, %rax
	jne	.LBB21_16
	jmp	.LBB21_31
.LBB21_21:
	cmpq	%r14, %r15
	je	.LBB21_31
# BB#22:                                # %.preheader.i
	leaq	8(%r15), %rax
	cmpq	%r14, %rax
	je	.LBB21_31
# BB#23:                                # %.lr.ph.i
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB21_24:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_29 Depth 2
	movq	%rbx, %rdi
	movq	%rax, %rbx
	movsd	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movq	(%r15), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB21_27
# BB#25:                                #   in Loop: Header=BB21_24 Depth=1
	movq	%rbx, %rdx
	subq	%r15, %rdx
	movq	%rdx, %rcx
	sarq	$3, %rcx
	movq	%r15, %rax
	je	.LBB21_30
# BB#26:                                #   in Loop: Header=BB21_24 Depth=1
	shlq	$3, %rcx
	subq	%rcx, %rdi
	addq	$16, %rdi
	movq	%r15, %rsi
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	callq	memmove
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%r15, %rax
	jmp	.LBB21_30
	.p2align	4, 0x90
.LBB21_27:                              #   in Loop: Header=BB21_24 Depth=1
	movq	(%rdi), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movq	%rbx, %rax
	jbe	.LBB21_30
# BB#28:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB21_24 Depth=1
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB21_29:                              # %.lr.ph.i.i
                                        #   Parent Loop BB21_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rax), %rcx
	movq	%rcx, (%rax)
	movq	-16(%rax), %xmm0        # xmm0 = mem[0],zero
	leaq	-8(%rax), %rax
	ucomisd	%xmm1, %xmm0
	ja	.LBB21_29
.LBB21_30:                              # %_ZSt25__unguarded_linear_insertIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops14_Val_less_iterEEvT_T0_.exit.i
                                        #   in Loop: Header=BB21_24 Depth=1
	movsd	%xmm1, (%rax)
	leaq	8(%rbx), %rax
	cmpq	%r14, %rax
	jne	.LBB21_24
.LBB21_31:                              # %_ZSt26__unguarded_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_.exit
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_, .Lfunc_end21-_ZSt22__final_insertion_sortIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.cfi_endproc

	.section	.text._ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,"axG",@progbits,_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,comdat
	.weak	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.p2align	4, 0x90
	.type	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_,@function
_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_: # @_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.cfi_startproc
# BB#0:
	subq	%rdi, %rsi
	cmpq	$16, %rsi
	jl	.LBB22_23
# BB#1:
	sarq	$3, %rsi
	leaq	-2(%rsi), %rax
	shrq	$63, %rax
	leaq	-2(%rsi,%rax), %r9
	sarq	%r9
	leaq	-1(%rsi), %rax
	shrq	$63, %rax
	leaq	-1(%rsi,%rax), %r11
	sarq	%r11
	testb	$1, %sil
	jne	.LBB22_14
# BB#2:                                 # %.split.us.preheader
	leaq	1(%r9,%r9), %r8
	movq	%r9, %r10
	.p2align	4, 0x90
.LBB22_3:                               # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_5 Depth 2
                                        #     Child Loop BB22_9 Depth 2
	movsd	(%rdi,%r10,8), %xmm0    # xmm0 = mem[0],zero
	cmpq	%r10, %r11
	movq	%r10, %rdx
	jle	.LBB22_6
# BB#4:                                 # %.lr.ph.i.us.preheader
                                        #   in Loop: Header=BB22_3 Depth=1
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB22_5:                               # %.lr.ph.i.us
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax), %rsi
	leaq	2(%rax,%rax), %rcx
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rsi,8), %xmm1
	cmovbeq	%rcx, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	movq	%rdx, %rax
	jl	.LBB22_5
.LBB22_6:                               # %._crit_edge.i.us
                                        #   in Loop: Header=BB22_3 Depth=1
	cmpq	%r9, %rdx
	jne	.LBB22_8
# BB#7:                                 #   in Loop: Header=BB22_3 Depth=1
	movq	(%rdi,%r8,8), %rax
	movq	%rax, (%rdi,%r9,8)
	movq	%r8, %rdx
.LBB22_8:                               #   in Loop: Header=BB22_3 Depth=1
	cmpq	%r10, %rdx
	jle	.LBB22_12
	.p2align	4, 0x90
.LBB22_9:                               # %.lr.ph.i.i.us
                                        #   Parent Loop BB22_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rax
	sarq	%rax
	movsd	(%rdi,%rax,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB22_12
# BB#10:                                #   in Loop: Header=BB22_9 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r10, %rax
	movq	%rax, %rdx
	jg	.LBB22_9
	jmp	.LBB22_13
	.p2align	4, 0x90
.LBB22_12:                              #   in Loop: Header=BB22_3 Depth=1
	movq	%rdx, %rax
.LBB22_13:                              # %_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEldNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_.exit.us
                                        #   in Loop: Header=BB22_3 Depth=1
	movsd	%xmm0, (%rdi,%rax,8)
	testq	%r10, %r10
	leaq	-1(%r10), %r10
	jne	.LBB22_3
	jmp	.LBB22_23
	.p2align	4, 0x90
.LBB22_14:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_16 Depth 2
                                        #     Child Loop BB22_18 Depth 2
	movsd	(%rdi,%r9,8), %xmm0     # xmm0 = mem[0],zero
	cmpq	%r9, %r11
	movq	%r9, %rsi
	jle	.LBB22_22
# BB#15:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB22_14 Depth=1
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB22_16:                              # %.lr.ph.i
                                        #   Parent Loop BB22_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rax
	leaq	(%rax,%rax), %rcx
	leaq	2(%rax,%rax), %rsi
	leaq	1(%rax,%rax), %rdx
	movsd	8(%rdi,%rcx,8), %xmm1   # xmm1 = mem[0],zero
	ucomisd	16(%rdi,%rcx,8), %xmm1
	cmovbeq	%rsi, %rdx
	movq	(%rdi,%rdx,8), %rcx
	movq	%rcx, (%rdi,%rax,8)
	cmpq	%r11, %rdx
	jl	.LBB22_16
# BB#17:                                # %._crit_edge.i
                                        #   in Loop: Header=BB22_14 Depth=1
	cmpq	%r9, %rdx
	jle	.LBB22_21
	.p2align	4, 0x90
.LBB22_18:                              # %.lr.ph.i.i
                                        #   Parent Loop BB22_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %rsi
	sarq	%rsi
	movsd	(%rdi,%rsi,8), %xmm1    # xmm1 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB22_21
# BB#19:                                #   in Loop: Header=BB22_18 Depth=2
	movsd	%xmm1, (%rdi,%rdx,8)
	cmpq	%r9, %rsi
	movq	%rsi, %rdx
	jg	.LBB22_18
	jmp	.LBB22_22
	.p2align	4, 0x90
.LBB22_21:                              #   in Loop: Header=BB22_14 Depth=1
	movq	%rdx, %rsi
.LBB22_22:                              # %_ZSt13__adjust_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEEldNS0_5__ops15_Iter_less_iterEEvT_T0_SA_T1_T2_.exit
                                        #   in Loop: Header=BB22_14 Depth=1
	movsd	%xmm0, (%rdi,%rsi,8)
	testq	%r9, %r9
	leaq	-1(%r9), %r9
	jne	.LBB22_14
.LBB22_23:                              # %.loopexit
	retq
.Lfunc_end22:
	.size	_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_, .Lfunc_end22-_ZSt11__make_heapIN9__gnu_cxx17__normal_iteratorIPdSt6vectorIdSaIdEEEENS0_5__ops15_Iter_less_iterEEvT_S9_T0_
	.cfi_endproc

	.section	.text._ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd,"axG",@progbits,_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd,comdat
	.weak	_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd
	.p2align	4, 0x90
	.type	_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd,@function
_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd: # @_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd
	.cfi_startproc
# BB#0:
	movq	40(%rdi), %rax
	movq	72(%rdi), %r8
	cmpq	%r8, %rax
	jae	.LBB23_3
	.p2align	4, 0x90
.LBB23_1:                               # %.lr.ph.i.i.i.i.i14
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdx
	movq	(%rsi), %rcx
	movq	%rcx, (%rdx)
	movq	%rcx, 8(%rdx)
	movq	%rcx, 16(%rdx)
	movq	%rcx, 24(%rdx)
	movq	%rcx, 32(%rdx)
	movq	%rcx, 40(%rdx)
	movq	%rcx, 48(%rdx)
	movq	%rcx, 56(%rdx)
	movq	%rcx, 64(%rdx)
	movq	%rcx, 72(%rdx)
	movq	%rcx, 80(%rdx)
	movq	%rcx, 88(%rdx)
	movq	%rcx, 96(%rdx)
	movq	%rcx, 104(%rdx)
	movq	%rcx, 112(%rdx)
	movq	%rcx, 120(%rdx)
	movq	%rcx, 128(%rdx)
	movq	%rcx, 136(%rdx)
	movq	%rcx, 144(%rdx)
	movq	%rcx, 152(%rdx)
	movq	%rcx, 160(%rdx)
	movq	%rcx, 168(%rdx)
	movq	%rcx, 176(%rdx)
	movq	%rcx, 184(%rdx)
	movq	%rcx, 192(%rdx)
	movq	%rcx, 200(%rdx)
	movq	%rcx, 208(%rdx)
	movq	%rcx, 216(%rdx)
	movq	%rcx, 224(%rdx)
	movq	%rcx, 232(%rdx)
	movq	%rcx, 240(%rdx)
	movq	%rcx, 248(%rdx)
	movq	%rcx, 256(%rdx)
	movq	%rcx, 264(%rdx)
	movq	%rcx, 272(%rdx)
	movq	%rcx, 280(%rdx)
	movq	%rcx, 288(%rdx)
	movq	%rcx, 296(%rdx)
	movq	%rcx, 304(%rdx)
	movq	%rcx, 312(%rdx)
	movq	%rcx, 320(%rdx)
	movq	%rcx, 328(%rdx)
	movq	%rcx, 336(%rdx)
	movq	%rcx, 344(%rdx)
	movq	%rcx, 352(%rdx)
	movq	%rcx, 360(%rdx)
	movq	%rcx, 368(%rdx)
	movq	%rcx, 376(%rdx)
	movq	%rcx, 384(%rdx)
	movq	%rcx, 392(%rdx)
	movq	%rcx, 400(%rdx)
	movq	%rcx, 408(%rdx)
	movq	%rcx, 416(%rdx)
	movq	%rcx, 424(%rdx)
	movq	%rcx, 432(%rdx)
	movq	%rcx, 440(%rdx)
	movq	%rcx, 448(%rdx)
	movq	%rcx, 456(%rdx)
	movq	%rcx, 464(%rdx)
	movq	%rcx, 472(%rdx)
	movq	%rcx, 480(%rdx)
	movq	%rcx, 488(%rdx)
	movq	%rcx, 496(%rdx)
	movq	%rcx, 504(%rdx)
	addq	$8, %rax
	cmpq	%r8, %rax
	jb	.LBB23_1
.LBB23_3:                               # %._crit_edge
	movq	48(%rdi), %rax
	movq	56(%rdi), %rcx
	cmpq	%rax, %rcx
	je	.LBB23_16
# BB#4:                                 # %.lr.ph.i.i.i.i.i.preheader
	movq	(%rsi), %rdx
	leaq	-8(%rax), %r10
	subq	%rcx, %r10
	shrq	$3, %r10
	incq	%r10
	cmpq	$4, %r10
	jb	.LBB23_15
# BB#5:                                 # %min.iters.checked
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	andq	%r10, %r9
	je	.LBB23_15
# BB#6:                                 # %vector.ph
	movd	%rdx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %r8
	movl	%r8d, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB23_7
# BB#8:                                 # %vector.body.prol.preheader
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB23_9:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rcx,%rsi,8)
	movdqu	%xmm0, 16(%rcx,%rsi,8)
	addq	$4, %rsi
	incq	%rdi
	jne	.LBB23_9
	jmp	.LBB23_10
.LBB23_7:
	xorl	%esi, %esi
.LBB23_10:                              # %vector.body.prol.loopexit
	cmpq	$28, %r8
	jb	.LBB23_13
# BB#11:                                # %vector.ph.new
	movq	%r9, %rdi
	subq	%rsi, %rdi
	leaq	240(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB23_12:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-32, %rdi
	jne	.LBB23_12
.LBB23_13:                              # %middle.block
	cmpq	%r9, %r10
	je	.LBB23_16
# BB#14:
	leaq	(%rcx,%r9,8), %rcx
	.p2align	4, 0x90
.LBB23_15:                              # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB23_15
.LBB23_16:                              # %_ZSt22__uninitialized_fill_aIPdddEvT_S1_RKT0_RSaIT1_E.exit
	retq
.Lfunc_end23:
	.size	_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd, .Lfunc_end23-_ZNSt5dequeIdSaIdEE18_M_fill_initializeERKd
	.cfi_endproc

	.section	.text._ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm,"axG",@progbits,_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm,comdat
	.weak	_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm
	.p2align	4, 0x90
	.type	_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm,@function
_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm: # @_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi163:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi165:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi166:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 48
.Lcfi168:
	.cfi_offset %rbx, -48
.Lcfi169:
	.cfi_offset %r12, -40
.Lcfi170:
	.cfi_offset %r13, -32
.Lcfi171:
	.cfi_offset %r14, -24
.Lcfi172:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%r14, %r12
	shrq	$6, %r12
	leaq	3(%r12), %rax
	cmpq	$8, %rax
	movl	$8, %edi
	cmovaq	%rax, %rdi
	movq	%rdi, 8(%r15)
	movb	$1, %al
	testb	%al, %al
	je	.LBB24_15
# BB#1:                                 # %_ZNSt11_Deque_baseIdSaIdEE15_M_allocate_mapEm.exit
	incq	%r12
	shlq	$3, %rdi
	callq	_Znwm
	movq	%rax, (%r15)
	movq	8(%r15), %rcx
	subq	%r12, %rcx
	movabsq	$4611686018427387902, %rdx # imm = 0x3FFFFFFFFFFFFFFE
	andq	%rcx, %rdx
	leaq	(%rax,%rdx,4), %rbx
	leaq	(%rbx,%r12,8), %r13
	movq	%rbx, %r12
	.p2align	4, 0x90
.LBB24_2:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
.Ltmp75:
	movl	$512, %edi              # imm = 0x200
	callq	_Znwm
.Ltmp76:
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	movq	%rax, (%r12)
	addq	$8, %r12
	cmpq	%r13, %r12
	jb	.LBB24_2
# BB#4:                                 # %_ZNSt11_Deque_baseIdSaIdEE15_M_create_nodesEPPdS3_.exit
	movq	%rbx, 40(%r15)
	movq	(%rbx), %rax
	movq	%rax, 24(%r15)
	leaq	512(%rax), %rcx
	movq	%rcx, 32(%r15)
	leaq	-8(%r13), %rcx
	movq	%rcx, 72(%r15)
	movq	-8(%r13), %rcx
	movq	%rcx, 56(%r15)
	leaq	512(%rcx), %rdx
	movq	%rdx, 64(%r15)
	movq	%rax, 16(%r15)
	andl	$63, %r14d
	leaq	(%rcx,%r14,8), %rax
	movq	%rax, 48(%r15)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB24_15:                              # %.noexc.i
	callq	_ZSt17__throw_bad_allocv
.LBB24_5:
.Ltmp77:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	cmpq	%rbx, %r12
	jbe	.LBB24_7
	.p2align	4, 0x90
.LBB24_6:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	callq	_ZdlPv
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jb	.LBB24_6
.LBB24_7:                               # %_ZNSt11_Deque_baseIdSaIdEE16_M_destroy_nodesEPPdS3_.exit.i
.Ltmp78:
	callq	__cxa_rethrow
.Ltmp79:
# BB#8:
.LBB24_9:
.Ltmp80:
	movq	%rax, %r14
.Ltmp81:
	callq	__cxa_end_catch
.Ltmp82:
# BB#10:
	movq	%r14, %rdi
	callq	__cxa_begin_catch
	movq	(%r15), %rdi
	callq	_ZdlPv
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
.Ltmp84:
	callq	__cxa_rethrow
.Ltmp85:
# BB#14:
.LBB24_11:
.Ltmp86:
	movq	%rax, %rbx
.Ltmp87:
	callq	__cxa_end_catch
.Ltmp88:
# BB#12:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB24_13:
.Ltmp89:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB24_16:
.Ltmp83:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end24:
	.size	_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm, .Lfunc_end24-_ZNSt11_Deque_baseIdSaIdEE17_M_initialize_mapEm
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp75-.Lfunc_begin7   #   Call between .Lfunc_begin7 and .Ltmp75
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin7   #     jumps to .Ltmp77
	.byte	1                       #   On action: 1
	.long	.Ltmp76-.Lfunc_begin7   # >> Call Site 3 <<
	.long	.Ltmp78-.Ltmp76         #   Call between .Ltmp76 and .Ltmp78
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin7   # >> Call Site 4 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin7   #     jumps to .Ltmp80
	.byte	1                       #   On action: 1
	.long	.Ltmp81-.Lfunc_begin7   # >> Call Site 5 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin7   #     jumps to .Ltmp83
	.byte	1                       #   On action: 1
	.long	.Ltmp82-.Lfunc_begin7   # >> Call Site 6 <<
	.long	.Ltmp84-.Ltmp82         #   Call between .Ltmp82 and .Ltmp84
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin7   # >> Call Site 7 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin7   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin7   # >> Call Site 8 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin7   #     jumps to .Ltmp89
	.byte	1                       #   On action: 1
	.long	.Ltmp88-.Lfunc_begin7   # >> Call Site 9 <<
	.long	.Lfunc_end24-.Ltmp88    #   Call between .Ltmp88 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_,"axG",@progbits,_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_,comdat
	.weak	_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_
	.p2align	4, 0x90
	.type	_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_,@function
_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_: # @_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi177:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi178:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi179:
	.cfi_def_cfa_offset 368
.Lcfi180:
	.cfi_offset %rbx, -56
.Lcfi181:
	.cfi_offset %r12, -48
.Lcfi182:
	.cfi_offset %r13, -40
.Lcfi183:
	.cfi_offset %r14, -32
.Lcfi184:
	.cfi_offset %r15, -24
.Lcfi185:
	.cfi_offset %rbp, -16
	movq	%rdx, %r9
	movq	%rsi, %rbx
	movq	%rdi, %rcx
	jmp	.LBB25_1
	.p2align	4, 0x90
.LBB25_10:                              #   in Loop: Header=BB25_1 Depth=1
	movq	%r13, 112(%rsp)
	movq	8(%rcx), %rax
	movq	%rax, 120(%rsp)
	movq	%rdi, 128(%rsp)
	movq	%rdx, 136(%rsp)
	movq	%r12, 80(%rsp)
	movq	%rsi, 88(%rsp)
	movq	16(%rbx), %rax
	movq	%rax, 96(%rsp)
	movq	%r8, 104(%rsp)
	leaq	32(%rsp), %rdi
	leaq	112(%rsp), %rsi
	leaq	80(%rsp), %rdx
	movq	%rcx, %r14
	movq	%r9, %rbp
	callq	_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_
	movaps	32(%rsp), %xmm0
	movaps	%xmm0, 272(%rsp)
	movaps	48(%rsp), %xmm0
	movaps	%xmm0, 288(%rsp)
	movups	(%rbx), %xmm0
	movaps	%xmm0, 240(%rsp)
	movups	16(%rbx), %xmm0
	movaps	%xmm0, 256(%rsp)
	leaq	272(%rsp), %rdi
	leaq	240(%rsp), %rsi
	movq	%rbp, %rdx
	callq	_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_
	movq	%rbp, %r9
	movq	%r14, %rcx
	movaps	32(%rsp), %xmm0
	movaps	48(%rsp), %xmm1
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
.LBB25_1:                               # =>This Inner Loop Header: Depth=1
	decq	%r9
	movq	24(%rbx), %r8
	movq	24(%rcx), %rdx
	movq	%r8, %rbp
	subq	%rdx, %rbp
	movq	(%rbx), %r12
	movq	8(%rbx), %rsi
	movq	%r12, %r15
	subq	%rsi, %r15
	sarq	$3, %r15
	leaq	(%r15,%rbp,8), %rax
	movq	(%rcx), %r13
	movq	16(%rcx), %rdi
	movq	%rdi, %r14
	subq	%r13, %r14
	sarq	$3, %r14
	leaq	-64(%r14,%rax), %rax
	cmpq	$17, %rax
	jl	.LBB25_9
# BB#2:                                 #   in Loop: Header=BB25_1 Depth=1
	cmpq	$-1, %r9
	jne	.LBB25_10
# BB#3:
	shlq	$3, %rbp
	movq	8(%rcx), %rax
	movq	16(%rbx), %rbx
	movq	%r13, 208(%rsp)
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, 216(%rsp)
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 224(%rsp)
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 232(%rsp)
	movq	%r12, 176(%rsp)
	movq	%rsi, 184(%rsp)
	movq	%rbx, 192(%rsp)
	movq	%r8, 200(%rsp)
	movq	%r12, 144(%rsp)
	movq	%rsi, 152(%rsp)
	movq	%rbx, 160(%rsp)
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	%r8, 168(%rsp)
	leaq	208(%rsp), %rdi
	movq	%rsi, %rbx
	leaq	176(%rsp), %rsi
	leaq	144(%rsp), %rdx
	callq	_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_
	addq	%rbp, %r15
	leaq	-64(%r14,%r15), %rax
	cmpq	$2, %rax
	jl	.LBB25_9
# BB#4:                                 # %.lr.ph.i
	addq	$-64, %r14
	leaq	32(%rsp), %r15
	.p2align	4, 0x90
.LBB25_5:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, %r12
	je	.LBB25_7
# BB#6:                                 #   in Loop: Header=BB25_5 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	jmp	.LBB25_8
	.p2align	4, 0x90
.LBB25_7:                               #   in Loop: Header=BB25_5 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	-8(%rcx), %rbx
	addq	$-8, %rcx
	leaq	512(%rbx), %r12
.LBB25_8:                               # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit.i.i
                                        #   in Loop: Header=BB25_5 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	movsd	-8(%r12), %xmm0         # xmm0 = mem[0],zero
	movq	(%r13), %rax
	movq	%rax, -8(%r12)
	leaq	-8(%r12), %r12
	movq	%r13, 32(%rsp)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 40(%rsp)
	movq	%rsi, 48(%rsp)
	movq	%rdx, 56(%rsp)
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rax
	subq	%rdx, %rax
	movq	%r12, %rcx
	subq	%rbx, %rcx
	sarq	$3, %rcx
	leaq	(%r14,%rax,8), %rbp
	addq	%rcx, %rbp
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbp, %rdx
	callq	_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_
	cmpq	$1, %rbp
	jg	.LBB25_5
.LBB25_9:                               # %.loopexit
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_, .Lfunc_end25-_ZSt16__introsort_loopISt15_Deque_iteratorIdRdPdElN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_T1_
	.cfi_endproc

	.section	.text._ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_,"axG",@progbits,_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_,comdat
	.weak	_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_,@function
_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_: # @_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi186:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi188:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi189:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 48
	subq	$128, %rsp
.Lcfi191:
	.cfi_def_cfa_offset 176
.Lcfi192:
	.cfi_offset %rbx, -48
.Lcfi193:
	.cfi_offset %r12, -40
.Lcfi194:
	.cfi_offset %r13, -32
.Lcfi195:
	.cfi_offset %r14, -24
.Lcfi196:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	24(%r14), %r9
	movq	24(%r12), %r8
	movq	%r9, %rcx
	subq	%r8, %rcx
	movq	(%r14), %rdi
	movq	8(%r14), %rbx
	movq	%rdi, %rdx
	subq	%rbx, %rdx
	sarq	$3, %rdx
	leaq	(%rdx,%rcx,8), %rax
	movq	(%r12), %rdx
	movq	16(%r12), %rsi
	movq	%rsi, %rcx
	subq	%rdx, %rcx
	sarq	$3, %rcx
	leaq	-64(%rcx,%rax), %rax
	cmpq	$17, %rax
	jl	.LBB26_24
# BB#1:
	movabsq	$-288230376151711744, %r15 # imm = 0xFC00000000000000
	movq	%rdx, 96(%rsp)
	movq	8(%r12), %rax
	movq	%rax, 104(%rsp)
	movq	%rsi, 112(%rsp)
	movq	%r8, 120(%rsp)
	movq	%rdx, %rdi
	subq	%rax, %rdi
	sarq	$3, %rdi
	addq	$16, %rdi
	js	.LBB26_5
# BB#2:
	cmpq	$63, %rdi
	jg	.LBB26_4
# BB#3:
	subq	$-128, %rdx
	jmp	.LBB26_7
.LBB26_24:
	movq	%rdx, 32(%rsp)
	movq	8(%r12), %rax
	movq	%rax, 40(%rsp)
	movq	%rsi, 48(%rsp)
	movq	%r8, 56(%rsp)
	movq	%rdi, (%rsp)
	movq	%rbx, 8(%rsp)
	movq	16(%r14), %rax
	movq	%rax, 16(%rsp)
	movq	%r9, 24(%rsp)
	leaq	32(%rsp), %rdi
	movq	%rsp, %rsi
	callq	_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	jmp	.LBB26_25
.LBB26_5:
	movq	%rdi, %rdx
	shrq	$6, %rdx
	orq	%r15, %rdx
	jmp	.LBB26_6
.LBB26_4:
	movq	%rdi, %rdx
	shrq	$6, %rdx
.LBB26_6:
	movq	(%r8,%rdx,8), %rax
	leaq	(%r8,%rdx,8), %r8
	leaq	512(%rax), %rsi
	shlq	$6, %rdx
	subq	%rdx, %rdi
	leaq	(%rax,%rdi,8), %rdx
.LBB26_7:                               # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit5
	movq	%rdx, 64(%rsp)
	movq	%rax, 72(%rsp)
	movq	%rsi, 80(%rsp)
	movq	%r8, 88(%rsp)
	leaq	96(%rsp), %rdi
	leaq	64(%rsp), %rsi
	callq	_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	movq	(%r12), %rdx
	movq	8(%r12), %r13
	movq	24(%r12), %r8
	movq	%rdx, %rax
	subq	%r13, %rax
	sarq	$3, %rax
	addq	$16, %rax
	js	.LBB26_11
# BB#8:
	cmpq	$63, %rax
	jg	.LBB26_10
# BB#9:
	movq	16(%r12), %r9
	subq	$-128, %rdx
	jmp	.LBB26_13
.LBB26_11:
	movq	%rax, %rcx
	shrq	$6, %rcx
	orq	%r15, %rcx
	jmp	.LBB26_12
.LBB26_10:
	movq	%rax, %rcx
	shrq	$6, %rcx
.LBB26_12:
	movq	(%r8,%rcx,8), %r13
	leaq	(%r8,%rcx,8), %r8
	leaq	512(%r13), %r9
	shlq	$6, %rcx
	subq	%rcx, %rax
	leaq	(%r13,%rax,8), %rdx
.LBB26_13:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit
	movq	(%r14), %r11
	jmp	.LBB26_14
	.p2align	4, 0x90
.LBB26_23:                              #   in Loop: Header=BB26_14 Depth=1
	movq	8(%r8), %r13
	addq	$8, %r8
	leaq	512(%r13), %r9
	movq	%r13, %rdx
.LBB26_14:                              # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit.outer.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_15 Depth 2
                                        #       Child Loop BB26_19 Depth 3
	leaq	-8(%r8), %r10
	.p2align	4, 0x90
.LBB26_15:                              # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit.i
                                        #   Parent Loop BB26_14 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB26_19 Depth 3
	cmpq	%r11, %rdx
	je	.LBB26_25
# BB#16:                                #   in Loop: Header=BB26_15 Depth=2
	movq	(%rdx), %rax
	cmpq	%r13, %rdx
	movq	%rdx, %rsi
	movq	%r13, %rcx
	movq	%r8, %rdi
	jne	.LBB26_18
# BB#17:                                #   in Loop: Header=BB26_15 Depth=2
	movq	(%r10), %rcx
	leaq	512(%rcx), %rsi
	movq	%r10, %rdi
.LBB26_18:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit.preheader.i.i
                                        #   in Loop: Header=BB26_15 Depth=2
	movd	%rax, %xmm0
	movq	%rdx, %rbx
	jmp	.LBB26_19
.LBB26_21:                              #   in Loop: Header=BB26_19 Depth=3
	movq	-8(%rdi), %rcx
	addq	$-8, %rdi
	movq	%rsi, %rbx
	leaq	512(%rcx), %rsi
	.p2align	4, 0x90
.LBB26_19:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit.i.i
                                        #   Parent Loop BB26_14 Depth=1
                                        #     Parent Loop BB26_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rsi), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB26_22
# BB#20:                                #   in Loop: Header=BB26_19 Depth=3
	addq	$-8, %rsi
	movsd	%xmm1, (%rbx)
	cmpq	%rsi, %rcx
	movq	%rsi, %rbx
	jne	.LBB26_19
	jmp	.LBB26_21
	.p2align	4, 0x90
.LBB26_22:                              # %_ZSt25__unguarded_linear_insertISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit.i
                                        #   in Loop: Header=BB26_15 Depth=2
	movq	%rax, (%rbx)
	addq	$8, %rdx
	cmpq	%r9, %rdx
	jne	.LBB26_15
	jmp	.LBB26_23
.LBB26_25:                              # %_ZSt26__unguarded_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_.exit
	addq	$128, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end26:
	.size	_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_, .Lfunc_end26-_ZSt22__final_insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	.cfi_endproc

	.section	.text._ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_,"axG",@progbits,_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_,comdat
	.weak	_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_,@function
_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_: # @_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi200:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi201:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi202:
	.cfi_def_cfa_offset 56
.Lcfi203:
	.cfi_offset %rbx, -56
.Lcfi204:
	.cfi_offset %r12, -48
.Lcfi205:
	.cfi_offset %r13, -40
.Lcfi206:
	.cfi_offset %r14, -32
.Lcfi207:
	.cfi_offset %r15, -24
.Lcfi208:
	.cfi_offset %rbp, -16
	movq	%rdi, -8(%rsp)          # 8-byte Spill
	movabsq	$-288230376151711744, %r12 # imm = 0xFC00000000000000
	movq	(%rdx), %r13
	movq	24(%rdx), %r9
	movq	24(%rsi), %r11
	movq	%r9, %rax
	subq	%r11, %rax
	movq	%r13, -16(%rsp)         # 8-byte Spill
	subq	8(%rdx), %r13
	movq	%r13, %rbx
	sarq	$3, %rbx
	leaq	(%rbx,%rax,8), %rcx
	movq	16(%rsi), %r14
	movq	(%rsi), %r8
	movq	8(%rsi), %r10
	movq	%r14, %rsi
	subq	%r8, %rsi
	sarq	$3, %rsi
	leaq	(%rcx,%rsi), %rbp
	leaq	-64(%rsi,%rcx), %rcx
	shrq	$63, %rcx
	leaq	-64(%rcx,%rbp), %rsi
	sarq	%rsi
	movq	%r8, %rax
	subq	%r10, %rax
	movq	%rax, %r15
	sarq	$3, %r15
	movq	%rsi, %rcx
	addq	%r15, %rcx
	js	.LBB27_4
# BB#1:
	cmpq	$63, %rcx
	jg	.LBB27_3
# BB#2:
	leaq	(%r8,%rsi,8), %rcx
	jmp	.LBB27_6
.LBB27_4:
	movq	%rcx, %rbp
	shrq	$6, %rbp
	orq	%r12, %rbp
	jmp	.LBB27_5
.LBB27_3:
	movq	%rcx, %rbp
	shrq	$6, %rbp
.LBB27_5:
	movq	%rbp, %rsi
	shlq	$6, %rsi
	subq	%rsi, %rcx
	shlq	$3, %rcx
	addq	(%r11,%rbp,8), %rcx
.LBB27_6:                               # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit13
	incq	%r15
	cmpq	$-15, %rax
	jl	.LBB27_10
# BB#7:
	cmpq	$63, %r15
	jg	.LBB27_9
# BB#8:
	leaq	8(%r8), %rbp
	jmp	.LBB27_12
.LBB27_10:
	movq	%r15, %rsi
	shrq	$6, %rsi
	movq	%r12, %rdi
	orq	%r12, %rsi
	jmp	.LBB27_11
.LBB27_9:
	movq	%r12, %rdi
	movq	%r15, %rsi
	shrq	$6, %rsi
.LBB27_11:
	movq	%rsi, %r12
	shlq	$6, %r12
	movq	%r15, %rbp
	subq	%r12, %rbp
	shlq	$3, %rbp
	addq	(%r11,%rsi,8), %rbp
	movq	%rdi, %r12
.LBB27_12:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit8
	decq	%rbx
	testq	%r13, %r13
	jle	.LBB27_16
# BB#13:
	cmpq	$519, %r13              # imm = 0x207
	jg	.LBB27_15
# BB#14:
	movq	-16(%rsp), %rsi         # 8-byte Reload
	addq	$-8, %rsi
	movq	%rsi, %rbx
	jmp	.LBB27_18
.LBB27_16:
	movq	%rbx, %rsi
	shrq	$6, %rsi
	orq	%r12, %rsi
	jmp	.LBB27_17
.LBB27_15:
	movq	%rbx, %rsi
	shrq	$6, %rsi
.LBB27_17:
	movq	%rsi, %rdi
	shlq	$6, %rdi
	subq	%rdi, %rbx
	shlq	$3, %rbx
	addq	(%r9,%rsi,8), %rbx
.LBB27_18:                              # %_ZNKSt15_Deque_iteratorIdRdPdEmiEl.exit
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm2
	jbe	.LBB27_21
# BB#19:
	ucomisd	%xmm2, %xmm0
	movq	%rcx, %rsi
	ja	.LBB27_23
# BB#20:
	ucomisd	%xmm1, %xmm0
	cmovaq	%rbx, %rbp
	movq	%rbp, %rsi
	jmp	.LBB27_23
.LBB27_21:
	ucomisd	%xmm1, %xmm0
	movq	%rbp, %rsi
	ja	.LBB27_23
# BB#22:
	ucomisd	%xmm2, %xmm0
	cmovaq	%rbx, %rcx
	movq	%rcx, %rsi
.LBB27_23:                              # %_ZSt22__move_median_to_firstISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_S7_T0_.exit
	movq	(%r8), %rcx
	movq	(%rsi), %rdi
	movq	%rdi, (%r8)
	movq	%rcx, (%rsi)
	cmpq	$-15, %rax
	jl	.LBB27_27
# BB#24:
	cmpq	$63, %r15
	jg	.LBB27_26
# BB#25:
	leaq	8(%r8), %rsi
	jmp	.LBB27_29
.LBB27_27:
	movq	%r15, %rcx
	shrq	$6, %rcx
	orq	%r12, %rcx
	jmp	.LBB27_28
.LBB27_26:
	movq	%r15, %rcx
	shrq	$6, %rcx
.LBB27_28:
	movq	(%r11,%rcx,8), %r10
	leaq	(%r11,%rcx,8), %r11
	leaq	512(%r10), %r14
	shlq	$6, %rcx
	subq	%rcx, %r15
	leaq	(%r10,%r15,8), %rsi
.LBB27_29:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit
	movq	(%rdx), %rcx
	movq	8(%rdx), %rdx
	jmp	.LBB27_30
	.p2align	4, 0x90
.LBB27_37:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit._crit_edge.i
                                        #   in Loop: Header=BB27_30 Depth=1
	cmpq	%r9, %r11
	je	.LBB27_38
# BB#40:                                # %_ZStltIdRdPdEbRKSt15_Deque_iteratorIT_T0_T1_ES8_.exit.i
                                        #   in Loop: Header=BB27_30 Depth=1
	jb	.LBB27_41
	jmp	.LBB27_39
	.p2align	4, 0x90
.LBB27_38:                              #   in Loop: Header=BB27_30 Depth=1
	cmpq	%rcx, %rsi
	jae	.LBB27_39
.LBB27_41:                              #   in Loop: Header=BB27_30 Depth=1
	movq	(%rsi), %rdi
	movq	(%rcx), %rbx
	movq	%rbx, (%rsi)
	movq	%rdi, (%rcx)
	addq	$8, %rsi
	cmpq	%r14, %rsi
	jne	.LBB27_30
# BB#42:                                #   in Loop: Header=BB27_30 Depth=1
	movq	8(%r11), %rsi
	addq	$8, %r11
	movq	%rsi, %r10
	leaq	512(%rsi), %r14
.LBB27_30:                              # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_32 Depth 2
                                        #     Child Loop BB27_34 Depth 2
	movsd	(%r8), %xmm0            # xmm0 = mem[0],zero
	ucomisd	(%rsi), %xmm0
	ja	.LBB27_32
	jmp	.LBB27_34
	.p2align	4, 0x90
.LBB27_33:                              #   in Loop: Header=BB27_32 Depth=2
	movq	8(%r11), %rsi
	addq	$8, %r11
	leaq	512(%rsi), %r14
	movq	%rsi, %r10
.LBB27_31:                              # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit.i
                                        #   in Loop: Header=BB27_32 Depth=2
	ucomisd	(%rsi), %xmm0
	jbe	.LBB27_34
.LBB27_32:                              # %.lr.ph.i
                                        #   Parent Loop BB27_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$8, %rsi
	cmpq	%r14, %rsi
	jne	.LBB27_31
	jmp	.LBB27_33
	.p2align	4, 0x90
.LBB27_34:                              # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit5._crit_edge.i
                                        #   Parent Loop BB27_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rdx, %rcx
	jne	.LBB27_36
# BB#35:                                #   in Loop: Header=BB27_34 Depth=2
	movq	-8(%r9), %rdx
	addq	$-8, %r9
	leaq	512(%rdx), %rcx
.LBB27_36:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit.preheader.i
                                        #   in Loop: Header=BB27_34 Depth=2
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	addq	$-8, %rcx
	ucomisd	%xmm0, %xmm1
	ja	.LBB27_34
	jmp	.LBB27_37
.LBB27_39:                              # %_ZSt21__unguarded_partitionISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_S7_T0_.exit
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	%rsi, (%rax)
	movq	%r10, 8(%rax)
	movq	%r14, 16(%rax)
	movq	%r11, 24(%rax)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_, .Lfunc_end27-_ZSt27__unguarded_partition_pivotISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEET_S7_S7_T0_
	.cfi_endproc

	.section	.text._ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_,"axG",@progbits,_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_,comdat
	.weak	_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_,@function
_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_: # @_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi210:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi211:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi212:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi213:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi214:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi215:
	.cfi_def_cfa_offset 128
.Lcfi216:
	.cfi_offset %rbx, -56
.Lcfi217:
	.cfi_offset %r12, -48
.Lcfi218:
	.cfi_offset %r13, -40
.Lcfi219:
	.cfi_offset %r14, -32
.Lcfi220:
	.cfi_offset %r15, -24
.Lcfi221:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r13
	movq	(%r13), %rsi
	movq	16(%r13), %rdx
	movq	24(%r13), %r12
	movq	(%r15), %rbx
	movq	24(%r15), %rbp
	movq	%rbx, %rax
	subq	8(%r15), %rax
	movq	%rbp, %rcx
	subq	%r12, %rcx
	sarq	$3, %rax
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	subq	%rsi, %rdx
	sarq	$3, %rdx
	addq	%rax, %rdx
	leaq	-64(%rdx,%rcx,8), %rcx
	cmpq	$2, %rcx
	jl	.LBB28_10
# BB#1:
	movq	8(%r13), %rdx
	leaq	-2(%rcx), %rax
	shrq	$63, %rax
	movq	%rcx, (%rsp)            # 8-byte Spill
	leaq	-2(%rcx,%rax), %rbx
	sarq	%rbx
	movq	%rsi, %rax
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	subq	%rdx, %rax
	sarq	$3, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	(%rax,%rbx), %rbp
	shlq	$3, %rbp
	movq	%r14, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB28_2:                               # =>This Inner Loop Header: Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rbx, %rax
	js	.LBB28_6
# BB#3:                                 #   in Loop: Header=BB28_2 Depth=1
	cmpq	$63, %rax
	jg	.LBB28_5
# BB#4:                                 #   in Loop: Header=BB28_2 Depth=1
	leaq	(%rsi,%rbx,8), %rcx
	movq	(%rsp), %rdx            # 8-byte Reload
	jmp	.LBB28_8
	.p2align	4, 0x90
.LBB28_6:                               #   in Loop: Header=BB28_2 Depth=1
	shrq	$6, %rax
	movabsq	$-288230376151711744, %rcx # imm = 0xFC00000000000000
	orq	%rcx, %rax
	jmp	.LBB28_7
	.p2align	4, 0x90
.LBB28_5:                               #   in Loop: Header=BB28_2 Depth=1
	shrq	$6, %rax
.LBB28_7:                               #   in Loop: Header=BB28_2 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%r12,%rax,8), %rcx
	shlq	$9, %rax
	subq	%rax, %rcx
	addq	%rbp, %rcx
.LBB28_8:                               # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit.i
                                        #   in Loop: Header=BB28_2 Depth=1
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movq	%rsi, 8(%rsp)
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, 16(%rsp)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rsp)
	movq	%r12, 32(%rsp)
	leaq	8(%rsp), %rdi
	movq	%rsi, %r14
	movq	%rbx, %rsi
	callq	_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_
	movq	%r14, %rsi
	decq	%rbx
	addq	$-8, %rbp
	cmpq	$-1, %rbx
	movq	56(%rsp), %r14          # 8-byte Reload
	jne	.LBB28_2
# BB#9:                                 # %_ZSt11__make_heapISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_.exit.loopexit
	movq	(%r15), %rbx
	movq	24(%r15), %rbp
.LBB28_10:                              # %_ZSt11__make_heapISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_.exit
	movq	16(%r15), %r12
	cmpq	24(%r14), %rbp
	jne	.LBB28_14
	jmp	.LBB28_12
.LBB28_18:
	movq	8(%rbp), %rbx
	addq	$8, %rbp
	leaq	512(%rbx), %r12
	.p2align	4, 0x90
.LBB28_11:                              # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit
	cmpq	24(%r14), %rbp
	je	.LBB28_12
.LBB28_14:                              # %_ZStltIdRdPdEbRKSt15_Deque_iteratorIT_T0_T1_ES8_.exit
	jb	.LBB28_15
	jmp	.LBB28_13
	.p2align	4, 0x90
.LBB28_12:
	cmpq	(%r14), %rbx
	jae	.LBB28_13
.LBB28_15:
	movq	(%r13), %rax
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movq	(%rax), %xmm1           # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB28_17
# BB#16:
	movdqu	8(%r13), %xmm1
	movq	24(%r13), %rcx
	movq	(%r15), %rdx
	movq	24(%r15), %rsi
	subq	8(%r15), %rdx
	movq	(%rax), %rdi
	movq	%rdi, (%rbx)
	movq	%rax, 8(%rsp)
	movdqu	%xmm1, 16(%rsp)
	movq	%rcx, 32(%rsp)
	subq	%rcx, %rsi
	sarq	$3, %rdx
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %rcx
	subq	%rax, %rcx
	sarq	$3, %rcx
	addq	%rdx, %rcx
	leaq	-64(%rcx,%rsi,8), %rdx
	xorl	%esi, %esi
	leaq	8(%rsp), %rdi
	callq	_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_
.LBB28_17:
	addq	$8, %rbx
	cmpq	%r12, %rbx
	jne	.LBB28_11
	jmp	.LBB28_18
.LBB28_13:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_, .Lfunc_end28-_ZSt13__heap_selectISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_S7_T0_
	.cfi_endproc

	.section	.text._ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_,"axG",@progbits,_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_,comdat
	.weak	_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_
	.p2align	4, 0x90
	.type	_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_,@function
_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_: # @_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi222:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi223:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi224:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi225:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi226:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi227:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi228:
	.cfi_def_cfa_offset 96
.Lcfi229:
	.cfi_offset %rbx, -56
.Lcfi230:
	.cfi_offset %r12, -48
.Lcfi231:
	.cfi_offset %r13, -40
.Lcfi232:
	.cfi_offset %r14, -32
.Lcfi233:
	.cfi_offset %r15, -24
.Lcfi234:
	.cfi_offset %rbp, -16
	movq	%rsi, %r8
	movabsq	$-288230376151711744, %r9 # imm = 0xFC00000000000000
	leaq	-1(%rdx), %rax
	shrq	$63, %rax
	leaq	-1(%rdx,%rax), %r10
	sarq	%r10
	cmpq	%r8, %r10
	movq	%r8, %r12
	jle	.LBB29_27
# BB#1:                                 # %.lr.ph
	movq	(%rdi), %r11
	movq	24(%rdi), %r14
	movq	%r11, %r15
	subq	8(%rdi), %r15
	sarq	$3, %r15
	movq	%r8, %r13
	.p2align	4, 0x90
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	leaq	(%r13,%r13), %rsi
	leaq	2(%r13,%r13), %r12
	movq	%r15, %rcx
	addq	%r12, %rcx
	js	.LBB29_5
# BB#3:                                 #   in Loop: Header=BB29_2 Depth=1
	cmpq	$63, %rcx
	jg	.LBB29_6
# BB#4:                                 #   in Loop: Header=BB29_2 Depth=1
	leaq	(%r11,%r12,8), %rcx
	jmp	.LBB29_8
	.p2align	4, 0x90
.LBB29_5:                               #   in Loop: Header=BB29_2 Depth=1
	movq	%rcx, %rax
	shrq	$6, %rax
	orq	%r9, %rax
	jmp	.LBB29_7
	.p2align	4, 0x90
.LBB29_6:                               #   in Loop: Header=BB29_2 Depth=1
	movq	%rcx, %rax
	shrq	$6, %rax
.LBB29_7:                               #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rbx
	shlq	$6, %rbx
	subq	%rbx, %rcx
	shlq	$3, %rcx
	addq	(%r14,%rax,8), %rcx
.LBB29_8:                               # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit
                                        #   in Loop: Header=BB29_2 Depth=1
	orq	$1, %rsi
	movq	%r15, %rax
	addq	%rsi, %rax
	js	.LBB29_11
# BB#9:                                 #   in Loop: Header=BB29_2 Depth=1
	cmpq	$63, %rax
	jg	.LBB29_12
# BB#10:                                #   in Loop: Header=BB29_2 Depth=1
	leaq	(%r11,%rsi,8), %rax
	jmp	.LBB29_14
	.p2align	4, 0x90
.LBB29_11:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rbx
	shrq	$6, %rbx
	orq	%r9, %rbx
	jmp	.LBB29_13
	.p2align	4, 0x90
.LBB29_12:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rbx
	shrq	$6, %rbx
.LBB29_13:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rbx, %rbp
	shlq	$6, %rbp
	subq	%rbp, %rax
	shlq	$3, %rax
	addq	(%r14,%rbx,8), %rax
.LBB29_14:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit29
                                        #   in Loop: Header=BB29_2 Depth=1
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	ucomisd	(%rcx), %xmm1
	cmovaq	%rsi, %r12
	movq	%r15, %rax
	addq	%r12, %rax
	js	.LBB29_17
# BB#15:                                #   in Loop: Header=BB29_2 Depth=1
	cmpq	$63, %rax
	jg	.LBB29_18
# BB#16:                                #   in Loop: Header=BB29_2 Depth=1
	leaq	(%r11,%r12,8), %rax
	jmp	.LBB29_20
	.p2align	4, 0x90
.LBB29_17:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rcx
	shrq	$6, %rcx
	orq	%r9, %rcx
	jmp	.LBB29_19
	.p2align	4, 0x90
.LBB29_18:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rcx
	shrq	$6, %rcx
.LBB29_19:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rcx, %rsi
	shlq	$6, %rsi
	subq	%rsi, %rax
	shlq	$3, %rax
	addq	(%r14,%rcx,8), %rax
.LBB29_20:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit49
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	(%rax), %rcx
	movq	%r15, %rax
	addq	%r13, %rax
	js	.LBB29_23
# BB#21:                                #   in Loop: Header=BB29_2 Depth=1
	cmpq	$63, %rax
	jg	.LBB29_24
# BB#22:                                #   in Loop: Header=BB29_2 Depth=1
	leaq	(%r11,%r13,8), %rax
	jmp	.LBB29_26
	.p2align	4, 0x90
.LBB29_23:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rsi
	shrq	$6, %rsi
	orq	%r9, %rsi
	jmp	.LBB29_25
	.p2align	4, 0x90
.LBB29_24:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rax, %rsi
	shrq	$6, %rsi
.LBB29_25:                              #   in Loop: Header=BB29_2 Depth=1
	movq	%rsi, %rbx
	shlq	$6, %rbx
	subq	%rbx, %rax
	shlq	$3, %rax
	addq	(%r14,%rsi,8), %rax
.LBB29_26:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit44
                                        #   in Loop: Header=BB29_2 Depth=1
	movq	%rcx, (%rax)
	cmpq	%r10, %r12
	movq	%r12, %r13
	jl	.LBB29_2
.LBB29_27:                              # %._crit_edge
	testb	$1, %dl
	jne	.LBB29_33
# BB#28:
	leaq	-2(%rdx), %rax
	shrq	$63, %rax
	leaq	-2(%rdx,%rax), %rax
	sarq	%rax
	cmpq	%rax, %r12
	jne	.LBB29_33
# BB#29:
	leaq	1(%r12,%r12), %rsi
	movq	(%rdi), %rbx
	movq	24(%rdi), %r10
	movq	%rbx, %rcx
	subq	8(%rdi), %rcx
	sarq	$3, %rcx
	movq	%rcx, %rax
	addq	%rsi, %rax
	js	.LBB29_34
# BB#30:
	cmpq	$63, %rax
	jg	.LBB29_35
# BB#31:
	leaq	(%rbx,%rsi,8), %rax
	jmp	.LBB29_37
.LBB29_33:
	movq	%r12, %rsi
	jmp	.LBB29_44
.LBB29_34:
	movq	%rax, %rbp
	shrq	$6, %rbp
	orq	%r9, %rbp
	jmp	.LBB29_36
.LBB29_35:
	movq	%rax, %rbp
	shrq	$6, %rbp
.LBB29_36:
	movq	%rbp, %rdx
	shlq	$6, %rdx
	subq	%rdx, %rax
	shlq	$3, %rax
	addq	(%r10,%rbp,8), %rax
.LBB29_37:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit39
	movq	(%rax), %rdx
	addq	%r12, %rcx
	js	.LBB29_40
# BB#38:
	cmpq	$63, %rcx
	jg	.LBB29_41
# BB#39:
	leaq	(%rbx,%r12,8), %rcx
	jmp	.LBB29_43
.LBB29_40:
	movq	%rcx, %rbp
	shrq	$6, %rbp
	orq	%r9, %rbp
	jmp	.LBB29_42
.LBB29_41:
	movq	%rcx, %rbp
	shrq	$6, %rbp
.LBB29_42:
	movq	%rbp, %rax
	shlq	$6, %rax
	subq	%rax, %rcx
	shlq	$3, %rcx
	addq	(%r10,%rbp,8), %rcx
.LBB29_43:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit34
	movq	%rdx, (%rcx)
.LBB29_44:
	movups	(%rdi), %xmm1
	movaps	%xmm1, (%rsp)
	movups	16(%rdi), %xmm1
	movaps	%xmm1, 16(%rsp)
	movq	%rsp, %rdi
	movq	%r8, %rdx
	callq	_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_, .Lfunc_end29-_ZSt13__adjust_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops15_Iter_less_iterEEvT_T0_S8_T1_T2_
	.cfi_endproc

	.section	.text._ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_,"axG",@progbits,_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_,comdat
	.weak	_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_
	.p2align	4, 0x90
	.type	_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_,@function
_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_: # @_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi235:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi236:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi237:
	.cfi_def_cfa_offset 32
.Lcfi238:
	.cfi_offset %rbx, -32
.Lcfi239:
	.cfi_offset %r14, -24
.Lcfi240:
	.cfi_offset %r15, -16
	movabsq	$-288230376151711744, %r8 # imm = 0xFC00000000000000
	cmpq	%rdx, %rsi
	jle	.LBB30_1
# BB#2:                                 # %.lr.ph
	movq	(%rdi), %r11
	movq	8(%rdi), %r10
	movq	24(%rdi), %r9
	movq	%rsi, %r14
	.p2align	4, 0x90
.LBB30_3:                               # =>This Inner Loop Header: Depth=1
	leaq	-1(%r14), %rax
	shrq	$63, %rax
	leaq	-1(%r14,%rax), %rsi
	sarq	%rsi
	movq	%r11, %rcx
	subq	%r10, %rcx
	sarq	$3, %rcx
	addq	%rsi, %rcx
	js	.LBB30_7
# BB#4:                                 #   in Loop: Header=BB30_3 Depth=1
	cmpq	$63, %rcx
	jg	.LBB30_6
# BB#5:                                 #   in Loop: Header=BB30_3 Depth=1
	leaq	(%r11,%rsi,8), %rax
	ucomisd	(%rax), %xmm0
	ja	.LBB30_11
	jmp	.LBB30_10
	.p2align	4, 0x90
.LBB30_7:                               #   in Loop: Header=BB30_3 Depth=1
	movq	%rcx, %r15
	shrq	$6, %r15
	orq	%r8, %r15
	jmp	.LBB30_8
	.p2align	4, 0x90
.LBB30_6:                               #   in Loop: Header=BB30_3 Depth=1
	movq	%rcx, %r15
	shrq	$6, %r15
.LBB30_8:                               #   in Loop: Header=BB30_3 Depth=1
	movq	%r15, %rbx
	shlq	$6, %rbx
	movq	%rcx, %rax
	subq	%rbx, %rax
	shlq	$3, %rax
	addq	(%r9,%r15,8), %rax
	ucomisd	(%rax), %xmm0
	jbe	.LBB30_10
.LBB30_11:                              #   in Loop: Header=BB30_3 Depth=1
	testq	%rcx, %rcx
	js	.LBB30_15
# BB#12:                                #   in Loop: Header=BB30_3 Depth=1
	cmpq	$63, %rcx
	jg	.LBB30_14
# BB#13:                                #   in Loop: Header=BB30_3 Depth=1
	leaq	(%r11,%rsi,8), %rcx
	jmp	.LBB30_17
	.p2align	4, 0x90
.LBB30_15:                              #   in Loop: Header=BB30_3 Depth=1
	movq	%rcx, %rbx
	shrq	$6, %rbx
	orq	%r8, %rbx
	jmp	.LBB30_16
	.p2align	4, 0x90
.LBB30_14:                              #   in Loop: Header=BB30_3 Depth=1
	movq	%rcx, %rbx
	shrq	$6, %rbx
.LBB30_16:                              #   in Loop: Header=BB30_3 Depth=1
	movq	%rbx, %rax
	shlq	$6, %rax
	subq	%rax, %rcx
	shlq	$3, %rcx
	addq	(%r9,%rbx,8), %rcx
.LBB30_17:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit25
                                        #   in Loop: Header=BB30_3 Depth=1
	movq	(%rcx), %r15
	movq	(%rdi), %r11
	movq	8(%rdi), %r10
	movq	24(%rdi), %r9
	movq	%r11, %rbx
	subq	%r10, %rbx
	sarq	$3, %rbx
	addq	%r14, %rbx
	js	.LBB30_21
# BB#18:                                #   in Loop: Header=BB30_3 Depth=1
	cmpq	$63, %rbx
	jg	.LBB30_20
# BB#19:                                #   in Loop: Header=BB30_3 Depth=1
	leaq	(%r11,%r14,8), %rbx
	jmp	.LBB30_23
	.p2align	4, 0x90
.LBB30_21:                              #   in Loop: Header=BB30_3 Depth=1
	movq	%rbx, %rax
	shrq	$6, %rax
	orq	%r8, %rax
	jmp	.LBB30_22
	.p2align	4, 0x90
.LBB30_20:                              #   in Loop: Header=BB30_3 Depth=1
	movq	%rbx, %rax
	shrq	$6, %rax
.LBB30_22:                              #   in Loop: Header=BB30_3 Depth=1
	movq	%rax, %rcx
	shlq	$6, %rcx
	subq	%rcx, %rbx
	shlq	$3, %rbx
	addq	(%r9,%rax,8), %rbx
.LBB30_23:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit20
                                        #   in Loop: Header=BB30_3 Depth=1
	movq	%r15, (%rbx)
	cmpq	%rdx, %rsi
	movq	%rsi, %r14
	jg	.LBB30_3
	jmp	.LBB30_24
.LBB30_1:                               # %..critedge_crit_edge
	movq	(%rdi), %r11
	movq	8(%rdi), %r10
	movq	24(%rdi), %r9
	jmp	.LBB30_24
.LBB30_10:
	movq	%r14, %rsi
.LBB30_24:                              # %.critedge
	movq	%r11, %rax
	subq	%r10, %rax
	sarq	$3, %rax
	addq	%rsi, %rax
	js	.LBB30_28
# BB#25:
	cmpq	$63, %rax
	jg	.LBB30_27
# BB#26:
	leaq	(%r11,%rsi,8), %rax
	jmp	.LBB30_30
.LBB30_28:
	movq	%rax, %rcx
	shrq	$6, %rcx
	orq	%r8, %rcx
	jmp	.LBB30_29
.LBB30_27:
	movq	%rax, %rcx
	shrq	$6, %rcx
.LBB30_29:
	movq	%rcx, %rdx
	shlq	$6, %rdx
	subq	%rdx, %rax
	shlq	$3, %rax
	addq	(%r9,%rcx,8), %rax
.LBB30_30:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit15
	movsd	%xmm0, (%rax)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end30:
	.size	_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_, .Lfunc_end30-_ZSt11__push_heapISt15_Deque_iteratorIdRdPdEldN9__gnu_cxx5__ops14_Iter_less_valEEvT_T0_S8_T1_T2_
	.cfi_endproc

	.section	.text._ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_,"axG",@progbits,_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_,comdat
	.weak	_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	.p2align	4, 0x90
	.type	_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_,@function
_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_: # @_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi243:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi244:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi245:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 56
.Lcfi247:
	.cfi_offset %rbx, -56
.Lcfi248:
	.cfi_offset %r12, -48
.Lcfi249:
	.cfi_offset %r13, -40
.Lcfi250:
	.cfi_offset %r14, -32
.Lcfi251:
	.cfi_offset %r15, -24
.Lcfi252:
	.cfi_offset %rbp, -16
	movq	(%rdi), %r10
	movq	(%rsi), %rbp
	cmpq	%rbp, %r10
	je	.LBB31_33
# BB#1:
	movabsq	$-288230376151711744, %rdx # imm = 0xFC00000000000000
	movq	8(%rdi), %r11
	movq	24(%rdi), %r8
	movq	%r10, %rcx
	subq	%r11, %rcx
	movq	%rcx, %rax
	sarq	$3, %rax
	incq	%rax
	cmpq	$-15, %rcx
	jl	.LBB31_5
# BB#2:
	cmpq	$63, %rax
	jg	.LBB31_4
# BB#3:
	movq	16(%rdi), %r14
	addq	$8, %r10
	jmp	.LBB31_7
.LBB31_5:
	movq	%rax, %rcx
	shrq	$6, %rcx
	orq	%rdx, %rcx
	jmp	.LBB31_6
.LBB31_4:
	movq	%rax, %rcx
	shrq	$6, %rcx
.LBB31_6:
	movq	(%r8,%rcx,8), %r11
	leaq	(%r8,%rcx,8), %r8
	leaq	512(%r11), %r14
	shlq	$6, %rcx
	subq	%rcx, %rax
	leaq	(%r11,%rax,8), %r10
.LBB31_7:                               # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit7
	movq	%rbp, -24(%rsp)         # 8-byte Spill
	jmp	.LBB31_8
	.p2align	4, 0x90
.LBB31_20:                              #   in Loop: Header=BB31_8 Depth=1
	movq	8(%r8), %r10
	addq	$8, %r8
	movq	%r10, %r11
	leaq	512(%r10), %r14
.LBB31_8:                               # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_9 Depth 2
                                        #       Child Loop BB31_30 Depth 3
                                        #       Child Loop BB31_22 Depth 3
	leaq	-8(%r8), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	-63(,%r8,8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_9:                               # %_ZNSt15_Deque_iteratorIdRdPdEppEv.exit
                                        #   Parent Loop BB31_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_30 Depth 3
                                        #       Child Loop BB31_22 Depth 3
	cmpq	%rbp, %r10
	je	.LBB31_33
# BB#10:                                #   in Loop: Header=BB31_9 Depth=2
	movq	(%rdi), %rax
	movq	(%rax), %xmm0           # xmm0 = mem[0],zero
	movq	(%r10), %r13
	ucomisd	(%r10), %xmm0
	jbe	.LBB31_27
# BB#11:                                #   in Loop: Header=BB31_9 Depth=2
	movq	16(%rdi), %r12
	movq	24(%rdi), %r15
	movq	%r10, %rcx
	subq	%r11, %rcx
	movq	%rcx, %rsi
	sarq	$3, %rsi
	leaq	1(%rsi), %rbp
	cmpq	$-15, %rcx
	jl	.LBB31_15
# BB#12:                                #   in Loop: Header=BB31_9 Depth=2
	cmpq	$63, %rbp
	jg	.LBB31_14
# BB#13:                                #   in Loop: Header=BB31_9 Depth=2
	leaq	8(%r10), %rbp
	movq	%r8, %rdx
	movq	%r11, %rbx
	jmp	.LBB31_17
	.p2align	4, 0x90
.LBB31_27:                              #   in Loop: Header=BB31_9 Depth=2
	cmpq	%r11, %r10
	movq	%r10, %rcx
	movq	%r11, %rdx
	movq	%r8, %rsi
	jne	.LBB31_29
# BB#28:                                #   in Loop: Header=BB31_9 Depth=2
	movq	-16(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdx
	leaq	512(%rdx), %rcx
	movq	%rax, %rsi
.LBB31_29:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit.preheader.i
                                        #   in Loop: Header=BB31_9 Depth=2
	movd	%r13, %xmm0
	movq	%r10, %rax
	jmp	.LBB31_30
.LBB31_32:                              #   in Loop: Header=BB31_30 Depth=3
	movq	-8(%rsi), %rdx
	addq	$-8, %rsi
	movq	%rcx, %rax
	leaq	512(%rdx), %rcx
	.p2align	4, 0x90
.LBB31_30:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit.i
                                        #   Parent Loop BB31_8 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-8(%rcx), %xmm1         # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB31_19
# BB#31:                                #   in Loop: Header=BB31_30 Depth=3
	addq	$-8, %rcx
	movsd	%xmm1, (%rax)
	cmpq	%rcx, %rdx
	movq	%rcx, %rax
	jne	.LBB31_30
	jmp	.LBB31_32
	.p2align	4, 0x90
.LBB31_15:                              #   in Loop: Header=BB31_9 Depth=2
	movq	%rbp, %rcx
	shrq	$6, %rcx
	movabsq	$-288230376151711744, %rdx # imm = 0xFC00000000000000
	orq	%rdx, %rcx
	jmp	.LBB31_16
.LBB31_14:                              #   in Loop: Header=BB31_9 Depth=2
	movq	%rbp, %rcx
	shrq	$6, %rcx
.LBB31_16:                              #   in Loop: Header=BB31_9 Depth=2
	leaq	(%r8,%rcx,8), %rdx
	movq	(%r8,%rcx,8), %rbx
	shlq	$6, %rcx
	subq	%rcx, %rbp
	leaq	(%rbx,%rbp,8), %rbp
.LBB31_17:                              # %_ZNKSt15_Deque_iteratorIdRdPdEplEl.exit
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	%r8, %r9
	subq	%r15, %r9
	subq	%rax, %r12
	sarq	$3, %r12
	leaq	(%rsi,%r12), %rcx
	leaq	-64(%rcx,%r9,8), %rcx
	testq	%rcx, %rcx
	jle	.LBB31_18
# BB#21:                                # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB31_9 Depth=2
	addq	-8(%rsp), %rsi          # 8-byte Folded Reload
	addq	%r12, %rsi
	shlq	$3, %r15
	subq	%r15, %rsi
	movq	%r8, %r12
	movq	%r11, %r15
	movq	%r10, %rcx
	.p2align	4, 0x90
.LBB31_22:                              # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB31_8 Depth=1
                                        #     Parent Loop BB31_9 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r15, %rcx
	jne	.LBB31_24
# BB#23:                                #   in Loop: Header=BB31_22 Depth=3
	movq	-8(%r12), %r15
	addq	$-8, %r12
	leaq	512(%r15), %rcx
.LBB31_24:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit4.i.i.i.i
                                        #   in Loop: Header=BB31_22 Depth=3
	movq	-8(%rcx), %r9
	cmpq	%rbx, %rbp
	jne	.LBB31_26
# BB#25:                                #   in Loop: Header=BB31_22 Depth=3
	movq	-8(%rdx), %rbx
	addq	$-8, %rdx
	leaq	512(%rbx), %rbp
.LBB31_26:                              # %_ZNSt15_Deque_iteratorIdRdPdEmmEv.exit.i.i.i.i
                                        #   in Loop: Header=BB31_22 Depth=3
	addq	$-8, %rcx
	movq	%r9, -8(%rbp)
	addq	$-8, %rbp
	decq	%rsi
	cmpq	$1, %rsi
	jg	.LBB31_22
.LBB31_18:                              #   in Loop: Header=BB31_9 Depth=2
	movq	-24(%rsp), %rbp         # 8-byte Reload
.LBB31_19:                              # %_ZSt25__unguarded_linear_insertISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops14_Val_less_iterEEvT_T0_.exit
                                        #   in Loop: Header=BB31_9 Depth=2
	movq	%r13, (%rax)
	addq	$8, %r10
	cmpq	%r14, %r10
	jne	.LBB31_9
	jmp	.LBB31_20
.LBB31_33:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_, .Lfunc_end31-_ZSt16__insertion_sortISt15_Deque_iteratorIdRdPdEN9__gnu_cxx5__ops15_Iter_less_iterEEvT_S7_T0_
	.cfi_endproc

	.section	.text._ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_,"axG",@progbits,_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_,comdat
	.weak	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_,@function
_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_: # @_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi253:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi254:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi255:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi256:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi257:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi258:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi259:
	.cfi_def_cfa_offset 64
.Lcfi260:
	.cfi_offset %rbx, -56
.Lcfi261:
	.cfi_offset %r12, -48
.Lcfi262:
	.cfi_offset %r13, -40
.Lcfi263:
	.cfi_offset %r14, -32
.Lcfi264:
	.cfi_offset %r15, -24
.Lcfi265:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rdi, %r14
	callq	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd
	movq	%rdx, %rbx
	leaq	8(%r14), %r15
	testq	%rbx, %rbx
	je	.LBB32_4
# BB#1:
	testq	%rax, %rax
	movb	$1, %bpl
	jne	.LBB32_10
# BB#2:
	cmpq	%rbx, %r15
	je	.LBB32_10
# BB#3:
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%r12), %xmm0
	seta	%bpl
	jmp	.LBB32_10
.LBB32_4:
	movq	16(%r14), %rax
	testq	%rax, %rax
	movq	%r15, %rbx
	je	.LBB32_7
# BB#5:                                 # %.lr.ph.i
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB32_6:                               # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	leaq	16(%rbx), %rax
	leaq	24(%rbx), %rcx
	ucomisd	32(%rbx), %xmm0
	cmovaq	%rcx, %rax
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB32_6
.LBB32_7:                               # %._crit_edge.i
	cmpq	%rbx, %r15
	je	.LBB32_8
# BB#9:
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	ucomisd	32(%rbx), %xmm0
	setbe	%bpl
	jmp	.LBB32_10
.LBB32_8:
	movb	$1, %bpl
.LBB32_10:                              # %_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE21_M_insert_equal_lowerERKd.exit
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %r13
	movq	(%r12), %rax
	movq	%rax, 32(%r13)
	movzbl	%bpl, %edi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	_ZSt29_Rb_tree_insert_and_rebalancebPSt18_Rb_tree_node_baseS0_RS_
	incq	40(%r14)
	movq	%r13, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_, .Lfunc_end32-_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE16_M_insert_equal_INS5_11_Alloc_nodeEEESt17_Rb_tree_iteratorIdESt23_Rb_tree_const_iteratorIdERKdRT_
	.cfi_endproc

	.section	.text._ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd,"axG",@progbits,_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd,comdat
	.weak	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd
	.p2align	4, 0x90
	.type	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd,@function
_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd: # @_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi266:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi267:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi268:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi269:
	.cfi_def_cfa_offset 48
.Lcfi270:
	.cfi_offset %rbx, -32
.Lcfi271:
	.cfi_offset %r14, -24
.Lcfi272:
	.cfi_offset %r15, -16
	movq	%rdx, %rax
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	8(%rbx), %r15
	cmpq	%r14, %r15
	je	.LBB33_5
# BB#1:
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	ucomisd	32(%r14), %xmm0
	jbe	.LBB33_11
# BB#2:
	movq	32(%rbx), %rdx
	cmpq	%r14, %rdx
	je	.LBB33_22
# BB#3:
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	_ZSt18_Rb_tree_incrementPSt18_Rb_tree_node_base
	movq	%rax, %rdx
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	32(%rdx), %xmm0
	jbe	.LBB33_18
# BB#4:
	xorl	%eax, %eax
	xorl	%edx, %edx
	jmp	.LBB33_23
.LBB33_5:
	cmpq	$0, 40(%rbx)
	je	.LBB33_7
# BB#6:
	movq	32(%rbx), %rdx
	movsd	32(%rdx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	(%rax), %xmm0
	jbe	.LBB33_22
.LBB33_7:
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB33_21
# BB#8:                                 # %.lr.ph.i10
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB33_9:                               # %.backedge.i15
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movsd	32(%rdx), %xmm1         # xmm1 = mem[0],zero
	leaq	16(%rdx), %rax
	leaq	24(%rdx), %rcx
	ucomisd	%xmm0, %xmm1
	cmovaq	%rax, %rcx
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB33_9
	jmp	.LBB33_22
.LBB33_11:
	movq	24(%rbx), %rax
	cmpq	%r14, %rax
	movq	%rax, %rdx
	je	.LBB33_23
# BB#12:
	movq	%r14, %rdi
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	_ZSt18_Rb_tree_decrementPSt18_Rb_tree_node_base
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, %rcx
	movsd	32(%rcx), %xmm0         # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB33_20
# BB#13:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB33_21
	.p2align	4, 0x90
.LBB33_14:                              # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rdx
	movsd	32(%rdx), %xmm0         # xmm0 = mem[0],zero
	leaq	16(%rdx), %rax
	leaq	24(%rdx), %rcx
	ucomisd	%xmm1, %xmm0
	cmovaq	%rax, %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	jne	.LBB33_14
	jmp	.LBB33_22
.LBB33_21:
	movq	%r15, %rdx
.LBB33_22:
	xorl	%eax, %eax
.LBB33_23:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB33_18:
	movq	24(%r14), %rax
	testq	%rax, %rax
	cmovneq	%rdx, %rax
	cmoveq	%r14, %rdx
	jmp	.LBB33_23
.LBB33_20:
	movq	24(%rcx), %rax
	testq	%rax, %rax
	cmovneq	%r14, %rax
	cmoveq	%rcx, %r14
	movq	%r14, %rdx
	jmp	.LBB33_23
.Lfunc_end33:
	.size	_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd, .Lfunc_end33-_ZNSt8_Rb_treeIddSt9_IdentityIdESt4lessIdESaIdEE28_M_get_insert_hint_equal_posESt23_Rb_tree_const_iteratorIdERKd
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_stepanov_container.ii,@function
_GLOBAL__sub_I_stepanov_container.ii:   # @_GLOBAL__sub_I_stepanov_container.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi273:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	callq	__cxa_atexit
	xorps	%xmm0, %xmm0
	movups	%xmm0, result_times(%rip)
	movq	$0, result_times+16(%rip)
	movl	$_ZNSt6vectorIdSaIdEED2Ev, %edi
	movl	$result_times, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end34:
	.size	_GLOBAL__sub_I_stepanov_container.ii, .Lfunc_end34-_GLOBAL__sub_I_stepanov_container.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	result_times,@object    # @result_times
	.bss
	.globl	result_times
	.p2align	3
result_times:
	.zero	24
	.size	result_times, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_stepanov_container.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
