	.text
	.file	"btSoftBodyRigidBodyCollisionConfiguration.bc"
	.globl	_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	.p2align	4, 0x90
	.type	_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo,@function
_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo: # @_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	callq	_ZN31btDefaultCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	movq	$_ZTV41btSoftBodyRigidBodyCollisionConfiguration+16, (%r12)
.Ltmp0:
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp1:
# BB#1:
	movb	$0, 8(%rax)
	movq	$_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 176(%r12)
.Ltmp2:
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp3:
# BB#2:
	movb	$0, 8(%rax)
	movq	$_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 184(%r12)
.Ltmp4:
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp5:
# BB#3:
	movq	$_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 192(%r12)
	movb	$1, 8(%rax)
.Ltmp6:
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp7:
# BB#4:
	movb	$0, 8(%rax)
	movq	$_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE+16, (%rax)
	movq	%rax, 200(%r12)
.Ltmp8:
	movl	$16, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp9:
# BB#5:
	movq	$_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE+16, (%rax)
	movq	%rax, 208(%r12)
	movb	$1, 8(%rax)
	cmpb	$0, 56(%r12)
	je	.LBB0_22
# BB#6:
	movq	48(%r12), %rax
	testq	%rax, %rax
	je	.LBB0_22
# BB#7:
	cmpl	$247, (%rax)
	jg	.LBB0_22
# BB#8:
	movq	24(%rax), %rdi
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
# BB#9:                                 # %_ZN15btPoolAllocatorD2Ev.exit
	movq	48(%r12), %rdi
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
# BB#10:
.Ltmp15:
	movl	$32, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
.Ltmp16:
# BB#11:
	movl	28(%r14), %eax
	movl	$248, (%rbx)
	movl	%eax, 4(%rbx)
	imull	$248, %eax, %edi
.Ltmp18:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
.Ltmp19:
# BB#12:                                # %.noexc
	movq	%rax, 24(%rbx)
	movq	%rax, 16(%rbx)
	movl	4(%rbx), %edi
	movl	%edi, 8(%rbx)
	movl	%edi, %ebp
	decl	%ebp
	je	.LBB0_21
# BB#13:                                # %.lr.ph.i
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movslq	(%rbx), %rcx
	addl	$-2, %edi
	movl	%ebp, %edx
	andl	$7, %edx
	je	.LBB0_14
# BB#15:                                # %.prol.preheader
	negl	%edx
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB0_16:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rbx,%rcx), %rsi
	movq	%rsi, (%rbx)
	decl	%ebp
	incl	%edx
	movq	%rsi, %rbx
	jne	.LBB0_16
	jmp	.LBB0_17
.LBB0_14:
	movq	%rax, %rsi
.LBB0_17:                               # %.prol.loopexit
	leaq	1(%rdi), %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpl	$7, %edi
	jb	.LBB0_20
# BB#18:                                # %.lr.ph.i.new
	leaq	(%rcx,%rcx), %r9
	leaq	(%rcx,%rcx,2), %r11
	leaq	(,%rcx,4), %r10
	leaq	(%rcx,%rcx,4), %r13
	leaq	(%r9,%r9,2), %r15
	leaq	(,%rcx,8), %r14
	movq	%r14, %rbx
	subq	%rcx, %rbx
	.p2align	4, 0x90
.LBB0_19:                               # =>This Inner Loop Header: Depth=1
	leaq	(%rsi,%rcx), %rdx
	movq	%rdx, (%rsi)
	addq	%rcx, %rdx
	leaq	(%rsi,%r9), %r8
	movq	%r8, (%rsi,%rcx)
	addq	%rcx, %rdx
	leaq	(%rsi,%r11), %rdi
	movq	%rdi, (%rsi,%rcx,2)
	addq	%rcx, %rdx
	leaq	(%rsi,%r10), %rdi
	movq	%rdi, (%rsi,%r11)
	addq	%rcx, %rdx
	leaq	(%rsi,%r13), %rdi
	movq	%rdi, (%rsi,%rcx,4)
	addq	%rcx, %rdx
	leaq	(%rsi,%r15), %rdi
	movq	%rdi, (%rsi,%r13)
	addq	%rcx, %rdx
	leaq	(%rsi,%rbx), %rdi
	movq	%rdi, (%rsi,%r15)
	addq	%rcx, %rdx
	leaq	(%rsi,%r14), %rdi
	movq	%rdi, (%rsi,%rbx)
	addl	$-8, %ebp
	movq	%rdx, %rsi
	jne	.LBB0_19
.LBB0_20:                               # %._crit_edge.loopexit.i
	imulq	8(%rsp), %rcx           # 8-byte Folded Reload
	addq	%rcx, %rax
	movq	16(%rsp), %rbx          # 8-byte Reload
.LBB0_21:
	movq	$0, (%rax)
	movq	%rbx, 48(%r12)
.LBB0_22:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_24:
.Ltmp20:
	jmp	.LBB0_26
.LBB0_23:
.Ltmp17:
	jmp	.LBB0_26
.LBB0_25:
.Ltmp10:
.LBB0_26:
	movq	%rax, %r14
.Ltmp21:
	movq	%r12, %rdi
	callq	_ZN31btDefaultCollisionConfigurationD2Ev
.Ltmp22:
# BB#27:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_28:
.Ltmp23:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo, .Lfunc_end0-_ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp0           #   Call between .Ltmp0 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp11         #   Call between .Ltmp11 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin0   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin0   #     jumps to .Ltmp23
	.byte	1                       #   On action: 1
	.long	.Ltmp22-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end0-.Ltmp22     #   Call between .Ltmp22 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev
	.p2align	4, 0x90
	.type	_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev,@function
_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev: # @_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV41btSoftBodyRigidBodyCollisionConfiguration+16, (%rbx)
	movq	176(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp24:
	callq	*(%rax)
.Ltmp25:
# BB#1:
	movq	176(%rbx), %rdi
.Ltmp26:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp27:
# BB#2:
	movq	184(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp28:
	callq	*(%rax)
.Ltmp29:
# BB#3:
	movq	184(%rbx), %rdi
.Ltmp30:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp31:
# BB#4:
	movq	192(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp32:
	callq	*(%rax)
.Ltmp33:
# BB#5:
	movq	192(%rbx), %rdi
.Ltmp34:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp35:
# BB#6:
	movq	200(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp36:
	callq	*(%rax)
.Ltmp37:
# BB#7:
	movq	200(%rbx), %rdi
.Ltmp38:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp39:
# BB#8:
	movq	208(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp40:
	callq	*(%rax)
.Ltmp41:
# BB#9:
	movq	208(%rbx), %rdi
.Ltmp42:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp43:
# BB#10:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN31btDefaultCollisionConfigurationD2Ev # TAILCALL
.LBB2_11:
.Ltmp44:
	movq	%rax, %r14
.Ltmp45:
	movq	%rbx, %rdi
	callq	_ZN31btDefaultCollisionConfigurationD2Ev
.Ltmp46:
# BB#12:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_13:
.Ltmp47:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev, .Lfunc_end2-_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp43-.Ltmp24         #   Call between .Ltmp24 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp45-.Ltmp43         #   Call between .Ltmp43 and .Ltmp45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin1   #     jumps to .Ltmp47
	.byte	1                       #   On action: 1
	.long	.Ltmp46-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp46     #   Call between .Ltmp46 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev
	.p2align	4, 0x90
	.type	_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev,@function
_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev: # @_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp48:
	callq	_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev
.Ltmp49:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp50:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev, .Lfunc_end3-_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp48-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin2   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp49     #   Call between .Ltmp49 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.p2align	4, 0x90
	.type	_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii,@function
_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii: # @_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.cfi_startproc
# BB#0:
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$32, %esi
	jne	.LBB4_3
# BB#1:
	cmpl	$32, %edx
	jne	.LBB4_3
# BB#2:
	movq	176(%rdi), %rax
	retq
.LBB4_3:
	cmpl	$32, %esi
	jne	.LBB4_6
# BB#4:
	cmpl	$19, %edx
	jg	.LBB4_9
# BB#5:
	movq	184(%rdi), %rax
	retq
.LBB4_6:
	cmpl	$19, %esi
	jg	.LBB4_11
# BB#7:
	cmpl	$32, %edx
	jne	.LBB4_11
# BB#8:
	movq	192(%rdi), %rax
	retq
.LBB4_9:
	leal	-21(%rdx), %eax
	cmpl	$8, %eax
	ja	.LBB4_15
# BB#10:
	movq	200(%rdi), %rax
	retq
.LBB4_11:
	leal	-21(%rsi), %eax
	cmpl	$8, %eax
	ja	.LBB4_15
# BB#12:
	cmpl	$32, %edx
	jne	.LBB4_15
# BB#13:
	movq	208(%rdi), %rax
	retq
.LBB4_15:                               # %.thread
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	jmp	_ZN31btDefaultCollisionConfiguration31getCollisionAlgorithmCreateFuncEii # TAILCALL
.Lfunc_end4:
	.size	_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii, .Lfunc_end4-_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.cfi_endproc

	.section	.text._ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv,"axG",@progbits,_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv,comdat
	.weak	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv,@function
_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv: # @_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %rax
	retq
.Lfunc_end5:
	.size	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv, .Lfunc_end5-_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.cfi_endproc

	.section	.text._ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv,"axG",@progbits,_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv,comdat
	.weak	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv,@function
_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv: # @_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.cfi_startproc
# BB#0:
	movq	48(%rdi), %rax
	retq
.Lfunc_end6:
	.size	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv, .Lfunc_end6-_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.cfi_endproc

	.section	.text._ZN31btDefaultCollisionConfiguration17getStackAllocatorEv,"axG",@progbits,_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv,comdat
	.weak	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.p2align	4, 0x90
	.type	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv,@function
_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv: # @_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end7:
	.size	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv, .Lfunc_end7-_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.cfi_endproc

	.section	.text._ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev: # @_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end8:
	.size	_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end8-_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 48
.Lcfi28:
	.cfi_offset %rbx, -40
.Lcfi29:
	.cfi_offset %r12, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$48, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN28btSoftSoftCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end9-_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN30btCollisionAlgorithmCreateFuncD2Ev,"axG",@progbits,_ZN30btCollisionAlgorithmCreateFuncD2Ev,comdat
	.weak	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.p2align	4, 0x90
	.type	_ZN30btCollisionAlgorithmCreateFuncD2Ev,@function
_ZN30btCollisionAlgorithmCreateFuncD2Ev: # @_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN30btCollisionAlgorithmCreateFuncD2Ev, .Lfunc_end10-_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.cfi_endproc

	.section	.text._ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev: # @_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end11:
	.size	_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end11-_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -48
.Lcfi38:
	.cfi_offset %r12, -40
.Lcfi39:
	.cfi_offset %r13, -32
.Lcfi40:
	.cfi_offset %r14, -24
.Lcfi41:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$40, %esi
	callq	*96(%rax)
	movq	%rax, %r13
	cmpb	$0, 8(%rbx)
	je	.LBB12_1
# BB#2:
	xorl	%esi, %esi
	movl	$1, %r9d
	jmp	.LBB12_3
.LBB12_1:
	xorl	%esi, %esi
	xorl	%r9d, %r9d
.LBB12_3:
	movq	%r13, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	callq	_ZN29btSoftRigidCollisionAlgorithmC1EP20btPersistentManifoldRK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS6_b
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end12-_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev,"axG",@progbits,_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev,comdat
	.weak	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev: # @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end13:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev, .Lfunc_end13-_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -40
.Lcfi48:
	.cfi_offset %r12, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$248, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	xorl	%r8d, %r8d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end14-_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.section	.text._ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev,"axG",@progbits,_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev,comdat
	.weak	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev: # @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end15:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev, .Lfunc_end15-_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.cfi_endproc

	.section	.text._ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,"axG",@progbits,_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,comdat
	.weak	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.p2align	4, 0x90
	.type	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_,@function
_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_: # @_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 48
.Lcfi56:
	.cfi_offset %rbx, -40
.Lcfi57:
	.cfi_offset %r12, -32
.Lcfi58:
	.cfi_offset %r14, -24
.Lcfi59:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	movl	$248, %esi
	callq	*96(%rax)
	movq	%rax, %rbx
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	_ZN35btSoftBodyConcaveCollisionAlgorithmC1ERK36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_b
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_, .Lfunc_end16-_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.cfi_endproc

	.type	_ZTV41btSoftBodyRigidBodyCollisionConfiguration,@object # @_ZTV41btSoftBodyRigidBodyCollisionConfiguration
	.section	.rodata,"a",@progbits
	.globl	_ZTV41btSoftBodyRigidBodyCollisionConfiguration
	.p2align	3
_ZTV41btSoftBodyRigidBodyCollisionConfiguration:
	.quad	0
	.quad	_ZTI41btSoftBodyRigidBodyCollisionConfiguration
	.quad	_ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev
	.quad	_ZN41btSoftBodyRigidBodyCollisionConfigurationD0Ev
	.quad	_ZN31btDefaultCollisionConfiguration25getPersistentManifoldPoolEv
	.quad	_ZN31btDefaultCollisionConfiguration25getCollisionAlgorithmPoolEv
	.quad	_ZN31btDefaultCollisionConfiguration17getStackAllocatorEv
	.quad	_ZN41btSoftBodyRigidBodyCollisionConfiguration31getCollisionAlgorithmCreateFuncEii
	.size	_ZTV41btSoftBodyRigidBodyCollisionConfiguration, 64

	.type	_ZTS41btSoftBodyRigidBodyCollisionConfiguration,@object # @_ZTS41btSoftBodyRigidBodyCollisionConfiguration
	.globl	_ZTS41btSoftBodyRigidBodyCollisionConfiguration
	.p2align	4
_ZTS41btSoftBodyRigidBodyCollisionConfiguration:
	.asciz	"41btSoftBodyRigidBodyCollisionConfiguration"
	.size	_ZTS41btSoftBodyRigidBodyCollisionConfiguration, 44

	.type	_ZTI41btSoftBodyRigidBodyCollisionConfiguration,@object # @_ZTI41btSoftBodyRigidBodyCollisionConfiguration
	.globl	_ZTI41btSoftBodyRigidBodyCollisionConfiguration
	.p2align	4
_ZTI41btSoftBodyRigidBodyCollisionConfiguration:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS41btSoftBodyRigidBodyCollisionConfiguration
	.quad	_ZTI31btDefaultCollisionConfiguration
	.size	_ZTI41btSoftBodyRigidBodyCollisionConfiguration, 24

	.type	_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE,@object # @_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN28btSoftSoftCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN28btSoftSoftCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN28btSoftSoftCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE,@object # @_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE:
	.asciz	"N28btSoftSoftCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE, 45

	.type	_ZTS30btCollisionAlgorithmCreateFunc,@object # @_ZTS30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTS30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTS30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTS30btCollisionAlgorithmCreateFunc
	.p2align	4
_ZTS30btCollisionAlgorithmCreateFunc:
	.asciz	"30btCollisionAlgorithmCreateFunc"
	.size	_ZTS30btCollisionAlgorithmCreateFunc, 33

	.type	_ZTI30btCollisionAlgorithmCreateFunc,@object # @_ZTI30btCollisionAlgorithmCreateFunc
	.section	.rodata._ZTI30btCollisionAlgorithmCreateFunc,"aG",@progbits,_ZTI30btCollisionAlgorithmCreateFunc,comdat
	.weak	_ZTI30btCollisionAlgorithmCreateFunc
	.p2align	3
_ZTI30btCollisionAlgorithmCreateFunc:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS30btCollisionAlgorithmCreateFunc
	.size	_ZTI30btCollisionAlgorithmCreateFunc, 16

	.type	_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE,@object # @_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN28btSoftSoftCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN28btSoftSoftCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE,@object # @_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN29btSoftRigidCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN29btSoftRigidCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN29btSoftRigidCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE,@object # @_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE:
	.asciz	"N29btSoftRigidCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE, 46

	.type	_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE,@object # @_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN29btSoftRigidCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN29btSoftRigidCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,@object # @_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.p2align	3
_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE:
	.quad	0
	.quad	_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncD0Ev
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithm10CreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE, 40

	.type	_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,@object # @_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE:
	.asciz	"N35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE"
	.size	_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE, 52

	.type	_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,@object # @_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.section	.rodata._ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,"aG",@progbits,_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE,comdat
	.weak	_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.p2align	4
_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN35btSoftBodyConcaveCollisionAlgorithm10CreateFuncE, 24

	.type	_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.p2align	3
_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE:
	.quad	0
	.quad	_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.quad	_ZN30btCollisionAlgorithmCreateFuncD2Ev
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncD0Ev
	.quad	_ZN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFunc24CreateCollisionAlgorithmER36btCollisionAlgorithmConstructionInfoP17btCollisionObjectS4_
	.size	_ZTVN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE, 40

	.type	_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.p2align	4
_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE:
	.asciz	"N35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE"
	.size	_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE, 59

	.type	_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,@object # @_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.section	.rodata._ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,"aG",@progbits,_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE,comdat
	.weak	_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.p2align	4
_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE
	.quad	_ZTI30btCollisionAlgorithmCreateFunc
	.size	_ZTIN35btSoftBodyConcaveCollisionAlgorithm17SwappedCreateFuncE, 24


	.globl	_ZN41btSoftBodyRigidBodyCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo
	.type	_ZN41btSoftBodyRigidBodyCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo,@function
_ZN41btSoftBodyRigidBodyCollisionConfigurationC1ERK34btDefaultCollisionConstructionInfo = _ZN41btSoftBodyRigidBodyCollisionConfigurationC2ERK34btDefaultCollisionConstructionInfo
	.globl	_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev
	.type	_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev,@function
_ZN41btSoftBodyRigidBodyCollisionConfigurationD1Ev = _ZN41btSoftBodyRigidBodyCollisionConfigurationD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
