	.text
	.file	"sparse.bc"
	.globl	make_sparse
	.p2align	4, 0x90
	.type	make_sparse,@function
make_sparse:                            # @make_sparse
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	leaq	8(%rsp), %r12
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	cover_cost
	leaq	32(%rsp), %r13
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	mv_reduce
	movq	%rax, %rbx
	movl	$12, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	totals
	movl	48(%rsp), %eax
	cmpl	24(%rsp), %eax
	je	.LBB0_4
# BB#2:                                 #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	copy_cost
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movl	$1, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	expand
	movq	%rax, %rbx
	movl	$13, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	totals
	movl	48(%rsp), %eax
	cmpl	24(%rsp), %eax
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	xorl	%eax, %eax
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	copy_cost
	cmpl	$0, force_irredundant(%rip)
	jne	.LBB0_1
.LBB0_4:
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	make_sparse, .Lfunc_end0-make_sparse
	.cfi_endproc

	.globl	mv_reduce
	.p2align	4, 0x90
	.type	mv_reduce,@function
mv_reduce:                              # @mv_reduce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 128
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movl	cube+4(%rip), %eax
	testl	%eax, %eax
	movq	%r14, 8(%rsp)           # 8-byte Spill
	jle	.LBB1_27
# BB#1:                                 # %.lr.ph143
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_6 Depth 3
                                        #       Child Loop BB1_11 Depth 3
                                        #       Child Loop BB1_16 Depth 3
	movq	cube+112(%rip), %rcx
	cmpl	$0, (%rcx,%rdx,4)
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	je	.LBB1_26
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	cube+16(%rip), %rcx
	movl	(%rcx,%rdx,4), %esi
	movq	cube+24(%rip), %rcx
	cmpl	(%rcx,%rdx,4), %esi
	jg	.LBB1_26
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph140
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_6 Depth 3
                                        #       Child Loop BB1_11 Depth 3
                                        #       Child Loop BB1_16 Depth 3
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movslq	12(%r14), %rbx
	movq	%rbx, %rdi
	shlq	$3, %rdi
	callq	malloc
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	movl	%ebx, %edi
	callq	sf_new
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	(%r14), %eax
	movl	12(%r14), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB1_9
# BB#5:                                 # %.lr.ph127
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	24(%r14), %rbp
	movslq	%ecx, %rcx
	leaq	(%rbp,%rcx,4), %r15
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	sarl	$5, %edx
	incl	%edx
	movslq	%edx, %r12
	movl	$1, %r13d
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %r13d
	.p2align	4, 0x90
.LBB1_6:                                #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	(%rbp,%r12,4), %r13d
	je	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=3
	movq	16(%rsp), %rdi          # 8-byte Reload
	movslq	12(%rdi), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, (%rcx,%rax,8)
	movq	24(%rdi), %rcx
	leal	1(%rax), %edx
	movl	(%rdi), %esi
	imull	%eax, %esi
	movl	%edx, 12(%rdi)
	movslq	%esi, %rax
	leaq	(%rcx,%rax,4), %rbx
	movq	cube+72(%rip), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	set_diff
	orl	%r13d, (%rbx,%r12,4)
	movl	(%r14), %eax
.LBB1_8:                                #   in Loop: Header=BB1_6 Depth=3
	movslq	%eax, %rcx
	leaq	(%rbp,%rcx,4), %rbp
	cmpq	%r15, %rbp
	jb	.LBB1_6
.LBB1_9:                                # %._crit_edge128
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	12(%rbx), %edi
	movl	cube(%rip), %esi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r13
	movl	(%rbx), %eax
	movl	12(%rbx), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	jle	.LBB1_14
# BB#10:                                # %.lr.ph131
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	24(%rdx), %r15
	movslq	%ecx, %rcx
	leaq	(%r15,%rcx,4), %r12
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	sarl	$5, %edx
	incl	%edx
	movslq	%edx, %rbp
	movl	$1, %r14d
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %r14d
	.p2align	4, 0x90
.LBB1_11:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	(%r15,%rbp,4), %r14d
	je	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_11 Depth=3
	movq	24(%r13), %rax
	movl	12(%r13), %ecx
	leal	1(%rcx), %edx
	imull	(%r13), %ecx
	movl	%edx, 12(%r13)
	movslq	%ecx, %rcx
	leaq	(%rax,%rcx,4), %rbx
	movq	cube+72(%rip), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rdx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	set_diff
	orl	%r14d, (%rbx,%rbp,4)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
.LBB1_13:                               #   in Loop: Header=BB1_11 Depth=3
	movslq	%eax, %rcx
	leaq	(%r15,%rcx,4), %r15
	cmpq	%r12, %r15
	jb	.LBB1_11
.LBB1_14:                               # %._crit_edge132
                                        #   in Loop: Header=BB1_4 Depth=2
	xorl	%eax, %eax
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	mark_irredundant
	movl	(%rbx), %eax
	movl	12(%rbx), %ecx
	imull	%eax, %ecx
	testl	%ecx, %ecx
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB1_22
# BB#15:                                # %.lr.ph136
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rdx), %rbp
	movslq	%ecx, %rcx
	leaq	(%rbp,%rcx,4), %r15
	movl	$-2, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	roll	%cl, %edx
	movl	%edx, 60(%rsp)          # 4-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	sarl	$5, %ecx
	incl	%ecx
	movslq	%ecx, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	48(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_16:                               #   Parent Loop BB1_2 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testb	$32, 1(%rbp)
	jne	.LBB1_21
# BB#17:                                #   in Loop: Header=BB1_16 Depth=3
	movq	(%r12), %rbx
	movl	cube+4(%rip), %eax
	decl	%eax
	cmpq	%rax, %r14
	je	.LBB1_19
# BB#18:                                #   in Loop: Header=BB1_16 Depth=3
	movq	cube+72(%rip), %rax
	movq	(%rax,%r14,8), %rdi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	setp_implies
	testl	%eax, %eax
	jne	.LBB1_20
.LBB1_19:                               #   in Loop: Header=BB1_16 Depth=3
	movl	60(%rsp), %eax          # 4-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	andl	%eax, (%rbx,%rcx,4)
.LBB1_20:                               #   in Loop: Header=BB1_16 Depth=3
	andb	$127, 1(%rbx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
.LBB1_21:                               #   in Loop: Header=BB1_16 Depth=3
	movslq	%eax, %rcx
	leaq	(%rbp,%rcx,4), %rbp
	addq	$8, %r12
	cmpq	%r15, %rbp
	jb	.LBB1_16
.LBB1_22:                               # %._crit_edge137
                                        #   in Loop: Header=BB1_4 Depth=2
	xorl	%eax, %eax
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	sf_free
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	sf_free
	movq	48(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movq	8(%rsp), %r14           # 8-byte Reload
	je	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_4 Depth=2
	callq	free
.LBB1_24:                               #   in Loop: Header=BB1_4 Depth=2
	movq	cube+24(%rip), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	cmpl	(%rax,%rcx,4), %edx
	leal	1(%rdx), %eax
	movl	%eax, %esi
	jl	.LBB1_4
# BB#25:                                # %.loopexit121.loopexit
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	cube+4(%rip), %eax
.LBB1_26:                               # %.loopexit121
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB1_2
.LBB1_27:                               # %._crit_edge144
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_active
	movl	cube+4(%rip), %eax
	testl	%eax, %eax
	jle	.LBB1_38
# BB#28:                                # %.lr.ph124
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_29:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_32 Depth 2
	movq	cube+112(%rip), %rcx
	cmpl	$0, (%rcx,%r14,4)
	je	.LBB1_37
# BB#30:                                #   in Loop: Header=BB1_29 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	(%rcx), %rdx
	movslq	12(%rcx), %rcx
	imulq	%rdx, %rcx
	testl	%ecx, %ecx
	jle	.LBB1_37
# BB#31:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_29 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rax), %rbx
	leaq	(%rbx,%rcx,4), %rbp
	.p2align	4, 0x90
.LBB1_32:                               # %.lr.ph
                                        #   Parent Loop BB1_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testb	$32, 1(%rbx)
	je	.LBB1_35
# BB#33:                                #   in Loop: Header=BB1_32 Depth=2
	movq	cube+72(%rip), %rax
	movq	(%rax,%r14,8), %rsi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	setp_disjoint
	testl	%eax, %eax
	je	.LBB1_35
# BB#34:                                #   in Loop: Header=BB1_32 Depth=2
	andb	$-33, 1(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	decl	16(%rax)
.LBB1_35:                               #   in Loop: Header=BB1_32 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	(%rax), %rax
	leaq	(%rbx,%rax,4), %rbx
	cmpq	%rbp, %rbx
	jb	.LBB1_32
# BB#36:                                # %.loopexit.loopexit
                                        #   in Loop: Header=BB1_29 Depth=1
	movl	cube+4(%rip), %eax
.LBB1_37:                               # %.loopexit
                                        #   in Loop: Header=BB1_29 Depth=1
	incq	%r14
	movslq	%eax, %rcx
	cmpq	%rcx, %r14
	jl	.LBB1_29
.LBB1_38:                               # %._crit_edge
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	12(%rdi), %eax
	cmpl	16(%rdi), %eax
	jne	.LBB1_40
# BB#39:
	movq	%rdi, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_40:
	xorl	%eax, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	sf_inactive             # TAILCALL
.Lfunc_end1:
	.size	mv_reduce, .Lfunc_end1-mv_reduce
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
