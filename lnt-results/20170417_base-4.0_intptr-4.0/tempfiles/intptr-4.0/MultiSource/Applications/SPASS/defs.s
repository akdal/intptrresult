	.text
	.file	"defs.bc"
	.globl	def_CreateFromClauses
	.p2align	4, 0x90
	.type	def_CreateFromClauses,@function
def_CreateFromClauses:                  # @def_CreateFromClauses
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %r14d
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbp
	movl	$48, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, (%rbx)
	movq	%r13, 8(%rbx)
	movq	$0, 16(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 24(%rbx)
	movq	$0, 32(%rbx)
	movl	%r14d, 40(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	def_CreateFromClauses, .Lfunc_end0-def_CreateFromClauses
	.cfi_endproc

	.globl	def_CreateFromTerm
	.p2align	4, 0x90
	.type	def_CreateFromTerm,@function
def_CreateFromTerm:                     # @def_CreateFromTerm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r13, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	movl	$48, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, (%rbx)
	movq	%r12, 8(%rbx)
	movq	%r15, 16(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, 24(%rbx)
	movq	%r14, 32(%rbx)
	movl	$0, 40(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	def_CreateFromTerm, .Lfunc_end1-def_CreateFromTerm
	.cfi_endproc

	.globl	def_Delete
	.p2align	4, 0x90
	.type	def_Delete,@function
def_Delete:                             # @def_Delete
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movq	24(%rbx), %rax
	movq	8(%rax), %r14
	callq	term_Delete
	movq	8(%rbx), %rdi
	callq	term_Delete
	cmpq	$0, %r14
	je	.LBB2_6
# BB#1:
	movq	24(%rbx), %rax
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB2_4
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph.i12.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB2_2
# BB#3:                                 # %list_Delete.exit13.loopexit.i
	movq	24(%rbx), %rax
.LBB2_4:                                # %list_Delete.exit13.i
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB2_9
	.p2align	4, 0x90
.LBB2_5:                                # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rax
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rcx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rcx, (%rdx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB2_5
	jmp	.LBB2_8
.LBB2_6:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#7:
	callq	term_Delete
.LBB2_8:                                # %def_DeleteFromClauses.exit.sink.split
	movq	24(%rbx), %rax
.LBB2_9:                                # %def_DeleteFromClauses.exit
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	memory_ARRAY+384(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+384(%rip), %rax
	movq	%rbx, (%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	def_Delete, .Lfunc_end2-def_Delete
	.cfi_endproc

	.globl	def_PredicateOccurrences
	.p2align	4, 0x90
	.type	def_PredicateOccurrences,@function
def_PredicateOccurrences:               # @def_PredicateOccurrences
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movl	(%rdi), %ecx
	movl	fol_ALL(%rip), %eax
	cmpl	%ecx, %eax
	movl	fol_EXIST(%rip), %edx
	je	.LBB3_2
.LBB3_1:
	cmpl	%ecx, %edx
	jne	.LBB3_3
.LBB3_2:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rcx
	movq	(%rcx), %rcx
	movq	8(%rcx), %rdi
	movl	(%rdi), %ecx
	cmpl	%ecx, %eax
	jne	.LBB3_1
	jmp	.LBB3_2
.LBB3_3:                                # %tailrecurse._crit_edge
	cmpl	%ecx, fol_AND(%rip)
	je	.LBB3_10
# BB#4:                                 # %tailrecurse._crit_edge
	cmpl	%ecx, fol_OR(%rip)
	je	.LBB3_10
# BB#5:                                 # %tailrecurse._crit_edge
	cmpl	%ecx, fol_NOT(%rip)
	je	.LBB3_10
# BB#6:                                 # %tailrecurse._crit_edge
	cmpl	%ecx, fol_IMPLIED(%rip)
	je	.LBB3_10
# BB#7:                                 # %tailrecurse._crit_edge
	cmpl	%ecx, fol_VARLIST(%rip)
	je	.LBB3_10
# BB#8:                                 # %tailrecurse._crit_edge
	cmpl	%ecx, fol_IMPLIES(%rip)
	je	.LBB3_10
# BB#9:                                 # %tailrecurse._crit_edge
	cmpl	%ecx, fol_EQUIV(%rip)
	je	.LBB3_10
# BB#14:
	xorl	%eax, %eax
	cmpl	%r14d, %ecx
	sete	%al
	jmp	.LBB3_15
.LBB3_10:                               # %fol_IsJunctor.exit.thread
	movq	16(%rdi), %rbx
	xorl	%eax, %eax
	testq	%rbx, %rbx
	jne	.LBB3_13
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_13 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB3_15
.LBB3_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	movq	8(%rbx), %rdi
	movl	%r14d, %esi
	callq	def_PredicateOccurrences
	addl	%ebp, %eax
	cmpl	$1, %eax
	jle	.LBB3_11
.LBB3_15:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	def_PredicateOccurrences, .Lfunc_end3-def_PredicateOccurrences
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_0:
	.zero	16
	.text
	.globl	def_ExtractDefsFromTerm
	.p2align	4, 0x90
	.type	def_ExtractDefsFromTerm,@function
def_ExtractDefsFromTerm:                # @def_ExtractDefsFromTerm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 96
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	$1, %ebp
	xorl	%r12d, %r12d
	movq	%r15, %rbx
	cmpl	$1, %ebp
	je	.LBB4_6
	jmp	.LBB4_2
.LBB4_5:                                # %.us-lcssa131
	negl	%ebp
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
	cmpl	$1, %ebp
	je	.LBB4_6
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_13:                               # %list_Nconc.exit.us
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
.LBB4_6:                                # %.outer112.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
	movl	(%rbx), %eax
	cmpl	fol_ALL(%rip), %eax
	je	.LBB4_7
# BB#14:                                # %.outer112..outer112.split_crit_edge.us
                                        #   in Loop: Header=BB4_6 Depth=1
	cmpl	$-1, %ebp
	jne	.LBB4_16
# BB#15:                                # %.outer112..outer112.split_crit_edge.us
                                        #   in Loop: Header=BB4_6 Depth=1
	cmpl	fol_EXIST(%rip), %eax
	jne	.LBB4_16
.LBB4_7:                                # %.us-lcssa130.us.us
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	list_Copy
	testq	%r12, %r12
	je	.LBB4_8
# BB#9:                                 #   in Loop: Header=BB4_6 Depth=1
	testq	%rax, %rax
	je	.LBB4_13
# BB#10:                                # %.preheader.i.us.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB4_11:                               # %.preheader.i.us
                                        #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB4_11
# BB#12:                                #   in Loop: Header=BB4_6 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_8:                                #   in Loop: Header=BB4_6 Depth=1
	movq	%rax, %r12
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_30:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbx
.LBB4_2:                                # %.outer112.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_28 Depth 2
	movl	(%rbx), %eax
	cmpl	$-1, %ebp
	jne	.LBB4_4
# BB#3:                                 # %.outer112.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	cmpl	fol_EXIST(%rip), %eax
	jne	.LBB4_4
# BB#24:                                # %.us-lcssa130
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rdi
	callq	list_Copy
	testq	%r12, %r12
	je	.LBB4_25
# BB#26:                                #   in Loop: Header=BB4_2 Depth=1
	testq	%rax, %rax
	je	.LBB4_30
# BB#27:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB4_28:                               # %.preheader.i
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB4_28
# BB#29:                                #   in Loop: Header=BB4_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB4_30
	.p2align	4, 0x90
.LBB4_25:                               #   in Loop: Header=BB4_2 Depth=1
	movq	%rax, %r12
	jmp	.LBB4_30
.LBB4_16:                               # %.outer112.split..outer112.split.split_crit_edge.us
	cmpl	fol_NOT(%rip), %eax
	je	.LBB4_5
	jmp	.LBB4_17
.LBB4_4:                                # %.outer112.split.split
	cmpl	fol_NOT(%rip), %eax
	je	.LBB4_5
	jmp	.LBB4_18
.LBB4_17:
	movl	$1, %ebp
.LBB4_18:                               # %.us-lcssa.us
	cmpl	$1, %ebp
	jne	.LBB4_20
# BB#19:                                # %.us-lcssa.us
	cmpl	fol_AND(%rip), %eax
	je	.LBB4_22
.LBB4_20:
	cmpl	$-1, %ebp
	jne	.LBB4_36
# BB#21:
	cmpl	fol_OR(%rip), %eax
	jne	.LBB4_36
.LBB4_22:
	movq	%rbx, %rdi
	callq	term_Copy
	movl	(%rbx), %esi
	movq	%rax, %rdi
	callq	cnf_Flatten
	movq	%rax, %rbx
	movq	16(%rbx), %rbp
	testq	%rbp, %rbp
	je	.LBB4_23
# BB#31:                                # %.lr.ph128.preheader
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_32:                               # %.lr.ph128
                                        # =>This Inner Loop Header: Depth=1
	movl	fol_ALL(%rip), %r15d
	movl	$term_Copy, %esi
	movq	%r12, %rdi
	callq	list_CopyWithElement
	movq	%rax, %rbx
	movq	8(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	$0, (%rax)
	movl	%r15d, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifierAddFather
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rax, %r13
	jne	.LBB4_32
# BB#33:                                # %._crit_edge
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_35
	.p2align	4, 0x90
.LBB4_34:                               # %.lr.ph.i104
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rdi)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rdi, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rdi
	jne	.LBB4_34
	jmp	.LBB4_35
.LBB4_36:
	movq	%r15, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%r12, %r12
	jne	.LBB4_38
	jmp	.LBB4_39
.LBB4_23:
	xorl	%eax, %eax
.LBB4_35:                               # %list_Delete.exit105
	movq	memory_ARRAY+256(%rip), %rdx
	movslq	32(%rdx), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+256(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%r12, %r12
	je	.LBB4_39
	.p2align	4, 0x90
.LBB4_38:                               # %.lr.ph.i99
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %r12
	jne	.LBB4_38
.LBB4_39:                               # %list_Delete.exit100.preheader
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB4_56
# BB#40:                                # %.lr.ph
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB4_41:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %rbp
	movq	%rbp, %rdi
	callq	term_AddFatherLinks
	movq	%rbp, %rdi
	movq	%rsp, %rsi
	callq	cnf_ContainsDefinition
	testl	%eax, %eax
	je	.LBB4_53
# BB#42:                                #   in Loop: Header=BB4_41 Depth=1
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	(%rsp), %rsi
	movq	%rbp, %rdi
	leaq	32(%rsp), %rdx
	callq	cnf_DefConvert
	movq	%rax, %rbp
	movq	(%rsp), %rax
	movq	8(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r13
	movq	(%rsp), %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movq	32(%rsp), %r12
	movl	$48, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%r13, (%r14)
	movq	%rbx, 8(%r14)
	movq	%r12, 16(%r14)
	movl	$16, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, 24(%r14)
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 32(%r14)
	movl	$0, 40(%r14)
	movq	(%rsp), %rbx
	movl	(%rbx), %r13d
	movq	%rbp, %rdi
	movl	%r13d, %esi
	callq	def_PredicateOccurrences
	movl	44(%r14), %ecx
	cmpl	$2, %eax
	jl	.LBB4_45
# BB#43:                                #   in Loop: Header=BB4_41 Depth=1
	testb	$1, %cl
	je	.LBB4_47
# BB#44:                                #   in Loop: Header=BB4_41 Depth=1
	decl	%ecx
	jmp	.LBB4_46
	.p2align	4, 0x90
.LBB4_45:                               #   in Loop: Header=BB4_41 Depth=1
	orl	$1, %ecx
.LBB4_46:                               # %def_RemoveAttribute.exit95.sink.split
                                        #   in Loop: Header=BB4_41 Depth=1
	movl	%ecx, 44(%r14)
	movl	(%rbx), %r13d
.LBB4_47:                               # %def_RemoveAttribute.exit95
                                        #   in Loop: Header=BB4_41 Depth=1
	cmpl	fol_EQUALITY(%rip), %r13d
	movq	8(%rsp), %rbx           # 8-byte Reload
	jne	.LBB4_49
# BB#48:                                #   in Loop: Header=BB4_41 Depth=1
	orl	$2, %ecx
	jmp	.LBB4_51
	.p2align	4, 0x90
.LBB4_49:                               #   in Loop: Header=BB4_41 Depth=1
	testb	$2, %cl
	je	.LBB4_52
# BB#50:                                #   in Loop: Header=BB4_41 Depth=1
	addl	$-2, %ecx
.LBB4_51:                               # %def_RemoveAttribute.exit.sink.split
                                        #   in Loop: Header=BB4_41 Depth=1
	movl	%ecx, 44(%r14)
.LBB4_52:                               # %def_RemoveAttribute.exit
                                        #   in Loop: Header=BB4_41 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
.LBB4_53:                               # %list_Delete.exit100
                                        #   in Loop: Header=BB4_41 Depth=1
	movq	%rbp, %rdi
	callq	term_Delete
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB4_41
# BB#54:                                # %list_Delete.exit100._crit_edge
	movq	16(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB4_56
	.p2align	4, 0x90
.LBB4_55:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB4_55
.LBB4_56:                               # %list_Delete.exit
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	def_ExtractDefsFromTerm, .Lfunc_end4-def_ExtractDefsFromTerm
	.cfi_endproc

	.globl	def_ExtractDefsFromClauselist
	.p2align	4, 0x90
	.type	def_ExtractDefsFromClauselist,@function
def_ExtractDefsFromClauselist:          # @def_ExtractDefsFromClauselist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 176
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movb	$1, %r14b
	testq	%rbp, %rbp
	je	.LBB5_1
# BB#2:                                 # %.lr.ph201
	movq	104(%rdi), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	112(%rdi), %r14
	xorl	%r12d, %r12d
	movq	%rbp, %rbx
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB5_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_5 Depth 2
                                        #     Child Loop BB5_15 Depth 2
                                        #     Child Loop BB5_19 Depth 2
                                        #     Child Loop BB5_26 Depth 2
                                        #     Child Loop BB5_28 Depth 2
                                        #     Child Loop BB5_31 Depth 2
                                        #     Child Loop BB5_33 Depth 2
	movq	8(%rbx), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%r14, %rsi
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	12(%rsp), %rcx
	leaq	16(%rsp), %r8
	callq	clause_ContainsPotPredDef
	testl	%eax, %eax
	je	.LBB5_35
# BB#4:                                 # %.lr.ph178
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_5:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rsi
	movq	16(%rsp), %rcx
	leaq	60(%rsp), %rdx
	callq	clause_IsPartOfDefinition
	xorl	%r14d, %r14d
	testl	%eax, %eax
	je	.LBB5_9
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=2
	movq	8(%rbp), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	%r15, (%rbx)
	movslq	60(%rsp), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r12, (%rax)
	movq	16(%rsp), %rcx
	cmpq	$0, 8(%rcx)
	jne	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_5 Depth=2
	xorl	%r14d, %r14d
	cmpq	$0, (%rcx)
	sete	%r14b
.LBB5_8:                                #   in Loop: Header=BB5_5 Depth=2
	movq	%rbx, %r15
	movq	%rax, %r12
.LBB5_9:                                #   in Loop: Header=BB5_5 Depth=2
	testl	%r14d, %r14d
	jne	.LBB5_11
# BB#10:                                #   in Loop: Header=BB5_5 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_5
.LBB5_11:                               # %.critedge
                                        #   in Loop: Header=BB5_3 Depth=1
	testl	%r14d, %r14d
	je	.LBB5_25
# BB#12:                                #   in Loop: Header=BB5_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%r15, %rcx
	movl	48(%rax), %r15d
	shrl	$3, %r15d
	andl	$1, %r15d
	testq	%rcx, %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	je	.LBB5_13
# BB#14:                                # %.lr.ph185.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	xorl	%r13d, %r13d
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB5_15:                               # %.lr.ph185
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rax
	movslq	(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r14, 8(%rbx)
	movq	%r13, (%rbx)
	movq	8(%rbp), %rax
	movq	(%rbp), %rbp
	testb	$8, 48(%rax)
	movl	$1, %eax
	cmovnel	%eax, %r15d
	testq	%rbp, %rbp
	movq	%rbx, %r13
	jne	.LBB5_15
	jmp	.LBB5_16
	.p2align	4, 0x90
.LBB5_25:                               #   in Loop: Header=BB5_3 Depth=1
	testq	%r12, %r12
	je	.LBB5_27
	.p2align	4, 0x90
.LBB5_26:                               # %.lr.ph.i151
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB5_26
.LBB5_27:                               # %list_Delete.exit152
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	16(%rsp), %rcx
	movq	(%rcx), %rax
	testq	%rax, %rax
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
	je	.LBB5_30
	.p2align	4, 0x90
.LBB5_28:                               # %.lr.ph.i146
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB5_28
# BB#29:                                # %list_Delete.exit147.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	16(%rsp), %rcx
.LBB5_30:                               # %list_Delete.exit147
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.LBB5_32
	.p2align	4, 0x90
.LBB5_31:                               # %.lr.ph.i141
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB5_31
	jmp	.LBB5_32
.LBB5_13:                               #   in Loop: Header=BB5_3 Depth=1
	xorl	%ebx, %ebx
.LBB5_16:                               # %._crit_edge186
                                        #   in Loop: Header=BB5_3 Depth=1
	movl	%r15d, 56(%rsp)         # 4-byte Spill
	movq	24(%rsp), %rbp          # 8-byte Reload
	movslq	(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rbx, (%rax)
	movl	12(%rsp), %ebx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%r12, (%rax)
	movq	56(%rbp), %rax
	movslq	12(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movl	64(%rbp), %ecx
	movl	68(%rbp), %edx
	movl	72(%rbp), %esi
	leal	(%rdx,%rcx), %eax
	addl	%esi, %eax
	jne	.LBB5_18
# BB#17:                                #   in Loop: Header=BB5_3 Depth=1
	xorl	%r13d, %r13d
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
	jmp	.LBB5_22
.LBB5_18:                               # %.lr.ph195.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	xorl	%ebp, %ebp
	xorl	%r13d, %r13d
	movq	48(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB5_19:                               # %.lr.ph195
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	12(%rsp), %ebp
	je	.LBB5_21
# BB#20:                                #   in Loop: Header=BB5_19 Depth=2
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	56(%rbx), %rax
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r13, (%rax)
	movl	64(%rbx), %ecx
	movl	68(%rbx), %edx
	movl	72(%rbx), %esi
	movq	%rax, %r13
.LBB5_21:                               #   in Loop: Header=BB5_19 Depth=2
	incl	%ebp
	leal	(%rdx,%rcx), %eax
	addl	%esi, %eax
	cmpl	%eax, %ebp
	jb	.LBB5_19
.LBB5_22:                               # %._crit_edge196
                                        #   in Loop: Header=BB5_3 Depth=1
	movl	fol_OR(%rip), %edi
	movq	%r13, %rsi
	callq	term_CreateAddFather
	movq	%rax, %rbx
	movl	fol_NOT(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rdi
	callq	cnf_NegationNormalFormula
	movq	%rax, %rbp
	movl	$48, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, (%rbx)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbx)
	movq	$0, 16(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 24(%rbx)
	movq	$0, 32(%rbx)
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, 40(%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	%r14, (%rbp)
	movq	40(%rsp), %r14          # 8-byte Reload
	cmpl	$0, 148(%r14)
	je	.LBB5_23
# BB#24:                                #   in Loop: Header=BB5_3 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	callq	def_Print
.LBB5_23:                               #   in Loop: Header=BB5_3 Depth=1
	movq	%rbp, %r12
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB5_32:                               # %list_Delete.exit142
                                        #   in Loop: Header=BB5_3 Depth=1
	testq	%r15, %r15
	je	.LBB5_34
	.p2align	4, 0x90
.LBB5_33:                               # %.lr.ph.i
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB5_33
.LBB5_34:                               # %list_Delete.exit
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	16(%rsp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	112(%rsp), %rbx         # 8-byte Reload
.LBB5_35:                               #   in Loop: Header=BB5_3 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB5_3
# BB#36:                                # %._crit_edge202
	testq	%r12, %r12
	sete	%r14b
	je	.LBB5_40
# BB#37:                                # %._crit_edge202
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	148(%rax), %eax
	testl	%eax, %eax
	je	.LBB5_40
# BB#38:                                # %.lr.ph172.preheader
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB5_39:                               # %.lr.ph172
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	callq	def_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_39
.LBB5_40:                               # %.preheader
	testq	%r12, %r12
	je	.LBB5_41
# BB#42:                                # %.lr.ph
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	symbol_SIGNATURE(%rip), %rax
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB5_43:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rdx), %rsi
	movq	8(%rsi), %rsi
	xorl	%edi, %edi
	subl	(%rsi), %edi
	sarl	%cl, %edi
	movslq	%edi, %rsi
	movq	(%rax,%rsi,8), %rsi
	orl	$128, 20(%rsi)
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_43
# BB#44:
	movq	64(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB5_45
.LBB5_1:
	xorl	%r12d, %r12d
	jmp	.LBB5_45
.LBB5_41:
	movq	64(%rsp), %rdi          # 8-byte Reload
	movb	$1, %r14b
.LBB5_45:                               # %._crit_edge
	movq	(%rdi), %rax
	testq	%rax, %rax
	movq	%rax, %rcx
	cmoveq	%r12, %rcx
	je	.LBB5_50
# BB#46:                                # %._crit_edge
	testb	%r14b, %r14b
	jne	.LBB5_50
# BB#47:                                # %.preheader.i.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB5_48:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB5_48
# BB#49:
	movq	%r12, (%rcx)
	movq	%rax, %rcx
.LBB5_50:                               # %list_Nconc.exit
	movq	%rcx, (%rdi)
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	def_ExtractDefsFromClauselist, .Lfunc_end5-def_ExtractDefsFromClauselist
	.cfi_endproc

	.globl	def_Print
	.p2align	4, 0x90
	.type	def_Print,@function
def_Print:                              # @def_Print
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 32
.Lcfi63:
	.cfi_offset %rbx, -32
.Lcfi64:
	.cfi_offset %r14, -24
.Lcfi65:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%r14), %rdi
	callq	fol_PrettyPrint
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r14), %rdi
	callq	fol_PrettyPrint
	movq	24(%r14), %rax
	cmpq	$0, 8(%rax)
	movq	stdout(%rip), %rcx
	je	.LBB6_7
# BB#1:
	movl	$.L.str.5, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	24(%r14), %r15
	movq	8(%r15), %rbx
	testq	%rbx, %rbx
	je	.LBB6_4
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %r15
	movl	8(%rbx), %esi
	movl	8(%r15), %edx
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_2
.LBB6_4:                                # %._crit_edge
	cmpl	$0, 40(%r14)
	movq	stdout(%rip), %rcx
	je	.LBB6_6
# BB#5:
	movl	$.L.str.7, %edi
	movl	$33, %esi
	jmp	.LBB6_10
.LBB6_7:
	movl	$.L.str.9, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	movq	32(%r14), %rdi
	movq	stdout(%rip), %rsi
	callq	fputs
	movl	$.L.str.10, %edi
	callq	puts
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB6_9
# BB#8:
	callq	fol_PrettyPrint
	jmp	.LBB6_11
.LBB6_6:
	movl	$.L.str.8, %edi
	movl	$37, %esi
	jmp	.LBB6_10
.LBB6_9:
	movq	stdout(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$8, %esi
.LBB6_10:
	movl	$1, %edx
	callq	fwrite
.LBB6_11:
	movq	stdout(%rip), %rcx
	movl	$.L.str.12, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movl	44(%r14), %eax
	testb	$3, %al
	je	.LBB6_17
# BB#12:
	testb	$2, %al
	je	.LBB6_14
# BB#13:
	movq	stdout(%rip), %rcx
	movl	$.L.str.13, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movl	44(%r14), %eax
.LBB6_14:
	testb	$1, %al
	jne	.LBB6_15
# BB#18:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB6_17:
	movq	stdout(%rip), %rcx
	movl	$.L.str.15, %edi
	movl	$6, %esi
	jmp	.LBB6_16
.LBB6_15:
	movq	stdout(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$25, %esi
.LBB6_16:
	movl	$1, %edx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	fwrite                  # TAILCALL
.Lfunc_end6:
	.size	def_Print, .Lfunc_end6-def_Print
	.cfi_endproc

	.globl	def_ApplyDefToTermOnce
	.p2align	4, 0x90
	.type	def_ApplyDefToTermOnce,@function
def_ApplyDefToTermOnce:                 # @def_ApplyDefToTermOnce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi70:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi72:
	.cfi_def_cfa_offset 128
.Lcfi73:
	.cfi_offset %rbx, -56
.Lcfi74:
	.cfi_offset %r12, -48
.Lcfi75:
	.cfi_offset %r13, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movl	$1, (%r8)
	movq	%r15, 16(%rsp)          # 8-byte Spill
	jmp	.LBB7_1
.LBB7_15:                               #   in Loop: Header=BB7_1 Depth=1
	movq	%r12, %rdi
	callq	term_Delete
	.p2align	4, 0x90
.LBB7_1:                                # %list_Delete.exit66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_6 Depth 2
                                        #     Child Loop BB7_8 Depth 2
                                        #     Child Loop BB7_10 Depth 2
                                        #     Child Loop BB7_12 Depth 2
	movq	%r15, %r12
	movq	%r12, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movq	$0, 24(%rsp)
	movq	$0, 32(%rsp)
	movq	8(%r14), %rax
	movl	(%rax), %esi
	movq	%rbx, %rdi
	leaq	40(%rsp), %rdx
	leaq	64(%rsp), %rcx
	leaq	32(%rsp), %r8
	leaq	24(%rsp), %r9
	callq	cnf_ContainsPredicate
	testl	%eax, %eax
	je	.LBB7_18
# BB#2:                                 #   in Loop: Header=BB7_1 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
	movl	$0, 12(%rsp)
	movq	16(%r14), %rdi
	xorl	%esi, %esi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB7_9
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	movl	$1, 12(%rsp)
	movq	(%r14), %rdi
	movq	8(%r14), %rbp
	callq	term_Copy
	movq	40(%rsp), %rcx
	movq	%rbp, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	movq	%r13, %r8
	callq	cnf_ApplyDefinitionOnce
	movq	%rax, %r15
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	je	.LBB7_5
# BB#4:                                 #   in Loop: Header=BB7_1 Depth=1
	movq	%r12, %rdi
	callq	term_Delete
.LBB7_5:                                #   in Loop: Header=BB7_1 Depth=1
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.LBB7_7
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph.i70
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_6
.LBB7_7:                                # %list_Delete.exit71
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB7_1
	.p2align	4, 0x90
.LBB7_8:                                # %.lr.ph.i65
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_8
	jmp	.LBB7_1
	.p2align	4, 0x90
.LBB7_9:                                #   in Loop: Header=BB7_1 Depth=1
	movq	16(%r14), %rdi
	callq	term_Copy
	movq	64(%rsp), %rsi
	movq	8(%r14), %rcx
	movq	16(%rcx), %rcx
	movq	40(%rsp), %rdx
	movq	16(%rdx), %r8
	movq	32(%rsp), %r9
	movq	%rbx, %rdi
	movq	%rax, %rdx
	leaq	12(%rsp), %rax
	pushq	%rax
.Lcfi79:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi80:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi81:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi82:
	.cfi_adjust_cfa_offset 8
	callq	cnf_DefTargetConvert
	addq	$32, %rsp
.Lcfi83:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	32(%rsp), %rax
	testq	%rax, %rax
	je	.LBB7_11
	.p2align	4, 0x90
.LBB7_10:                               # %.lr.ph.i60
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_10
.LBB7_11:                               # %list_Delete.exit61
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB7_13
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph.i
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB7_12
.LBB7_13:                               # %list_Delete.exit
                                        #   in Loop: Header=BB7_1 Depth=1
	cmpl	$0, 12(%rsp)
	je	.LBB7_16
# BB#14:                                #   in Loop: Header=BB7_1 Depth=1
	movq	(%r14), %rdi
	movq	8(%r14), %rbp
	callq	term_Copy
	movq	40(%rsp), %rcx
	movq	%rbp, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	movq	%r13, %r8
	callq	cnf_ApplyDefinitionOnce
	movq	%rax, %r15
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	je	.LBB7_1
	jmp	.LBB7_15
.LBB7_18:
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	$1, (%rax)
	movq	%rbx, %rdi
	callq	term_Delete
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB7_20
# BB#19:
	xorl	%eax, %eax
	jmp	.LBB7_21
.LBB7_16:
	movq	%rbx, %rdi
	callq	term_Delete
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	je	.LBB7_17
.LBB7_20:
	movq	%r12, %rdi
	callq	cnf_ObviousSimplifications
.LBB7_21:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_17:
	xorl	%eax, %eax
	jmp	.LBB7_21
.Lfunc_end7:
	.size	def_ApplyDefToTermOnce, .Lfunc_end7-def_ApplyDefToTermOnce
	.cfi_endproc

	.globl	def_ApplyDefToTermExhaustive
	.p2align	4, 0x90
	.type	def_ApplyDefToTermExhaustive,@function
def_ApplyDefToTermExhaustive:           # @def_ApplyDefToTermExhaustive
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi87:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi88:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi89:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi90:
	.cfi_def_cfa_offset 80
.Lcfi91:
	.cfi_offset %rbx, -56
.Lcfi92:
	.cfi_offset %r12, -48
.Lcfi93:
	.cfi_offset %r13, -40
.Lcfi94:
	.cfi_offset %r14, -32
.Lcfi95:
	.cfi_offset %r15, -24
.Lcfi96:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	104(%rdi), %r12
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	112(%rdi), %r13
	movq	%rbx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB8_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_4 Depth 2
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB8_9
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB8_2 Depth=1
	movl	$1, %r15d
	.p2align	4, 0x90
.LBB8_4:                                # %.lr.ph
                                        #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r12, %rcx
	leaq	20(%rsp), %r8
	callq	def_ApplyDefToTermOnce
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_8
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=2
	xorl	%r15d, %r15d
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	je	.LBB8_7
# BB#6:                                 #   in Loop: Header=BB8_4 Depth=2
	movq	%rbx, %rdi
	callq	term_Delete
.LBB8_7:                                #   in Loop: Header=BB8_4 Depth=2
	movq	%r14, %rbx
.LBB8_8:                                #   in Loop: Header=BB8_4 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_4
# BB#1:                                 # %.loopexit
                                        #   in Loop: Header=BB8_2 Depth=1
	testl	%r15d, %r15d
	je	.LBB8_2
.LBB8_9:                                # %.loopexit.thread
	xorl	%eax, %eax
	cmpq	(%rsp), %rbx            # 8-byte Folded Reload
	cmovneq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	def_ApplyDefToTermExhaustive, .Lfunc_end8-def_ApplyDefToTermExhaustive
	.cfi_endproc

	.globl	def_ApplyDefToClauseOnce
	.p2align	4, 0x90
	.type	def_ApplyDefToClauseOnce,@function
def_ApplyDefToClauseOnce:               # @def_ApplyDefToClauseOnce
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi100:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 64
.Lcfi104:
	.cfi_offset %rbx, -56
.Lcfi105:
	.cfi_offset %r12, -48
.Lcfi106:
	.cfi_offset %r13, -40
.Lcfi107:
	.cfi_offset %r14, -32
.Lcfi108:
	.cfi_offset %r15, -24
.Lcfi109:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbp
	movq	%rdx, %r13
	movq	%rsi, %r14
	movq	%rdi, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r14, 8(%r15)
	movq	$0, (%r15)
	testq	%r15, %r15
	je	.LBB9_13
# BB#1:                                 # %.lr.ph64
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_7 Depth 2
	movq	8(%rbx), %rdi
	movq	8(%r12), %rax
	movl	(%rax), %esi
	callq	clause_ContainsSymbol
	testl	%eax, %eax
	je	.LBB9_12
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	8(%rbx), %rdi
	movq	(%r12), %rdx
	movq	8(%r12), %rsi
	movq	%r13, %rcx
	movq	%rbp, %r8
	callq	cnf_ApplyDefinitionToClause
	testq	%r15, %r15
	je	.LBB9_4
# BB#5:                                 #   in Loop: Header=BB9_2 Depth=1
	testq	%rax, %rax
	je	.LBB9_9
# BB#6:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB9_7:                                # %.preheader.i
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB9_7
# BB#8:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB9_9
	.p2align	4, 0x90
.LBB9_4:                                #   in Loop: Header=BB9_2 Depth=1
	movq	%rax, %r15
.LBB9_9:                                # %list_Nconc.exit
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	8(%rbx), %rdi
	cmpq	%r14, %rdi
	je	.LBB9_11
# BB#10:                                #   in Loop: Header=BB9_2 Depth=1
	callq	clause_Delete
.LBB9_11:                               #   in Loop: Header=BB9_2 Depth=1
	movq	$0, 8(%rbx)
.LBB9_12:                               #   in Loop: Header=BB9_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_2
.LBB9_13:                               # %._crit_edge65
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	list_PointerDeleteElement
	testq	%rax, %rax
	je	.LBB9_22
# BB#14:
	cmpq	%r14, 8(%rax)
	jne	.LBB9_17
# BB#15:
	movq	(%rax), %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	testq	%rsi, %rsi
	movq	%rsi, %rax
	je	.LBB9_16
.LBB9_17:                               # %.lr.ph
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB9_18:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	cmpl	$0, 40(%r12)
	je	.LBB9_20
# BB#19:                                #   in Loop: Header=BB9_18 Depth=1
	orb	$8, 48(%rbx)
.LBB9_20:                               #   in Loop: Header=BB9_18 Depth=1
	movl	$25, 76(%rbx)
	movslq	(%r14), %r15
	movq	24(%r12), %rax
	movq	8(%rax), %rdi
	callq	list_Copy
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 32(%rbx)
	movq	24(%r12), %rax
	movq	(%rax), %rdi
	callq	list_Copy
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	$0, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 40(%rbx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_18
# BB#21:
	movq	(%rsp), %rbx            # 8-byte Reload
.LBB9_22:                               # %._crit_edge
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_16:
	xorl	%ebx, %ebx
	jmp	.LBB9_22
.Lfunc_end9:
	.size	def_ApplyDefToClauseOnce, .Lfunc_end9-def_ApplyDefToClauseOnce
	.cfi_endproc

	.globl	def_ApplyDefToClauseExhaustive
	.p2align	4, 0x90
	.type	def_ApplyDefToClauseExhaustive,@function
def_ApplyDefToClauseExhaustive:         # @def_ApplyDefToClauseExhaustive
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi116:
	.cfi_def_cfa_offset 96
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %rdi
	callq	clause_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%rax, %rax
	je	.LBB10_33
# BB#1:                                 # %.preheader.lr.ph
	movq	104(%r15), %rbx
	movq	112(%r15), %rbp
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%r15, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB10_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_4 Depth 2
                                        #       Child Loop BB10_6 Depth 3
                                        #         Child Loop BB10_9 Depth 4
                                        #       Child Loop BB10_24 Depth 3
                                        #     Child Loop BB10_31 Depth 2
	testq	%rax, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB10_29
# BB#3:                                 # %.lr.ph72.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	xorl	%ecx, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB10_4:                               # %.lr.ph72
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_6 Depth 3
                                        #         Child Loop BB10_9 Depth 4
                                        #       Child Loop BB10_24 Depth 3
	movq	8(%r13), %r12
	movq	(%r15), %r14
	testq	%r14, %r14
	je	.LBB10_17
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB10_4 Depth=2
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB10_6:                               # %.lr.ph
                                        #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_4 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB10_9 Depth 4
	movq	8(%r14), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	callq	def_ApplyDefToClauseOnce
	testq	%r15, %r15
	je	.LBB10_11
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=3
	testq	%rax, %rax
	je	.LBB10_12
# BB#8:                                 # %.preheader.i62.preheader
                                        #   in Loop: Header=BB10_6 Depth=3
	movq	%r15, %rdx
	.p2align	4, 0x90
.LBB10_9:                               # %.preheader.i62
                                        #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_4 Depth=2
                                        #       Parent Loop BB10_6 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB10_9
# BB#10:                                #   in Loop: Header=BB10_6 Depth=3
	movq	%rax, (%rcx)
	jmp	.LBB10_12
	.p2align	4, 0x90
.LBB10_11:                              #   in Loop: Header=BB10_6 Depth=3
	movq	%rax, %r15
.LBB10_12:                              # %list_Nconc.exit64
                                        #   in Loop: Header=BB10_6 Depth=3
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB10_6
# BB#13:                                # %._crit_edge
                                        #   in Loop: Header=BB10_4 Depth=2
	testq	%r15, %r15
	movq	(%rsp), %rax            # 8-byte Reload
	je	.LBB10_18
# BB#14:                                #   in Loop: Header=BB10_4 Depth=2
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	je	.LBB10_22
# BB#15:                                #   in Loop: Header=BB10_4 Depth=2
	cmpl	$0, 36(%rbp)
	je	.LBB10_20
# BB#16:                                #   in Loop: Header=BB10_4 Depth=2
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	prfs_InsertDocProofClause
	jmp	.LBB10_21
	.p2align	4, 0x90
.LBB10_17:                              # %._crit_edge.thread
                                        #   in Loop: Header=BB10_4 Depth=2
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB10_19
	jmp	.LBB10_28
	.p2align	4, 0x90
.LBB10_18:                              #   in Loop: Header=BB10_4 Depth=2
	cmpq	24(%rsp), %r12          # 8-byte Folded Reload
	movq	16(%rsp), %r15          # 8-byte Reload
	je	.LBB10_28
.LBB10_19:                              #   in Loop: Header=BB10_4 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB10_28
.LBB10_20:                              #   in Loop: Header=BB10_4 Depth=2
	movq	%r12, %rdi
	callq	clause_Delete
.LBB10_21:                              #   in Loop: Header=BB10_4 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
.LBB10_22:                              #   in Loop: Header=BB10_4 Depth=2
	testq	%rax, %rax
	je	.LBB10_26
# BB#23:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB10_4 Depth=2
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB10_24:                              # %.preheader.i
                                        #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB10_24
# BB#25:                                #   in Loop: Header=BB10_4 Depth=2
	movq	%r15, (%rax)
	jmp	.LBB10_27
.LBB10_26:                              #   in Loop: Header=BB10_4 Depth=2
	movq	%r15, (%rsp)            # 8-byte Spill
.LBB10_27:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB10_4 Depth=2
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB10_28:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB10_4 Depth=2
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB10_4
	jmp	.LBB10_30
	.p2align	4, 0x90
.LBB10_29:                              #   in Loop: Header=BB10_2 Depth=1
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB10_30:                              # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	32(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB10_31:                              # %.lr.ph.i
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB10_31
# BB#32:                                # %list_Delete.exit.backedge
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	testq	%rax, %rax
	jne	.LBB10_2
	jmp	.LBB10_34
.LBB10_33:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB10_34:                              # %list_Delete.exit._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	def_ApplyDefToClauseExhaustive, .Lfunc_end10-def_ApplyDefToClauseExhaustive
	.cfi_endproc

	.globl	def_ApplyDefToClauselist
	.p2align	4, 0x90
	.type	def_ApplyDefToClauselist,@function
def_ApplyDefToClauselist:               # @def_ApplyDefToClauselist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi127:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi129:
	.cfi_def_cfa_offset 96
.Lcfi130:
	.cfi_offset %rbx, -56
.Lcfi131:
	.cfi_offset %r12, -48
.Lcfi132:
	.cfi_offset %r13, -40
.Lcfi133:
	.cfi_offset %r14, -32
.Lcfi134:
	.cfi_offset %r15, -24
.Lcfi135:
	.cfi_offset %rbp, -16
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	112(%rdi), %rbx
	testq	%rdx, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	je	.LBB11_1
# BB#2:                                 # %.lr.ph
	movq	104(%rdi), %r13
	testl	%ecx, %ecx
	je	.LBB11_7
# BB#3:                                 # %.lr.ph.split.preheader
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%rdx, %r15
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_20 Depth 2
	movq	8(%r15), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	def_ApplyDefToClauseOnce
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB11_17
# BB#5:                                 #   in Loop: Header=BB11_4 Depth=1
	cmpl	$0, 36(%rbx)
	movq	8(%r15), %rsi
	je	.LBB11_15
# BB#6:                                 #   in Loop: Header=BB11_4 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	prfs_InsertDocProofClause
	jmp	.LBB11_16
	.p2align	4, 0x90
.LBB11_15:                              #   in Loop: Header=BB11_4 Depth=1
	movq	%rsi, %rdi
	callq	clause_Delete
.LBB11_16:                              #   in Loop: Header=BB11_4 Depth=1
	movq	$0, 8(%r15)
.LBB11_17:                              #   in Loop: Header=BB11_4 Depth=1
	testq	%rbp, %rbp
	movq	%rbp, %r12
	cmoveq	%r14, %r12
	je	.LBB11_22
# BB#18:                                #   in Loop: Header=BB11_4 Depth=1
	testq	%r14, %r14
	je	.LBB11_22
# BB#19:                                # %.preheader.i42.preheader
                                        #   in Loop: Header=BB11_4 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB11_20:                              # %.preheader.i42
                                        #   Parent Loop BB11_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB11_20
# BB#21:                                #   in Loop: Header=BB11_4 Depth=1
	movq	%r14, (%rax)
	movq	%rbp, %r12
.LBB11_22:                              # %list_Nconc.exit44
                                        #   in Loop: Header=BB11_4 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	movq	%r12, %rbp
	jne	.LBB11_4
	jmp	.LBB11_23
.LBB11_1:
	xorl	%r12d, %r12d
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB11_25
	jmp	.LBB11_24
.LBB11_7:                               # %.lr.ph.split.us.preheader
	xorl	%r12d, %r12d
	movq	%rdx, %rbp
	.p2align	4, 0x90
.LBB11_8:                               # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_12 Depth 2
	movq	8(%rbp), %rsi
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r13, %rcx
	callq	def_ApplyDefToClauseOnce
	testq	%r12, %r12
	je	.LBB11_9
# BB#10:                                #   in Loop: Header=BB11_8 Depth=1
	testq	%rax, %rax
	je	.LBB11_14
# BB#11:                                # %.preheader.i42.us.preheader
                                        #   in Loop: Header=BB11_8 Depth=1
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB11_12:                              # %.preheader.i42.us
                                        #   Parent Loop BB11_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB11_12
# BB#13:                                #   in Loop: Header=BB11_8 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB11_14
	.p2align	4, 0x90
.LBB11_9:                               #   in Loop: Header=BB11_8 Depth=1
	movq	%rax, %r12
.LBB11_14:                              # %list_Nconc.exit44.us
                                        #   in Loop: Header=BB11_8 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB11_8
.LBB11_23:                              # %._crit_edge
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB11_24
.LBB11_25:
	xorl	%esi, %esi
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	list_PointerDeleteElement
	movq	%rax, %rbp
	testq	%r12, %r12
	jne	.LBB11_27
	jmp	.LBB11_29
.LBB11_24:
	movq	16(%rsp), %rbp          # 8-byte Reload
	testq	%r12, %r12
	je	.LBB11_29
.LBB11_27:
	movl	148(%rbx), %eax
	testl	%eax, %eax
	je	.LBB11_29
# BB#28:
	movq	stdout(%rip), %rcx
	movl	$.L.str.16, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_ListPrint
.LBB11_29:
	testq	%rbp, %rbp
	movq	%rbp, %rax
	cmoveq	%r12, %rax
	je	.LBB11_34
# BB#30:
	testq	%r12, %r12
	je	.LBB11_34
# BB#31:                                # %.preheader.i.preheader
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB11_32:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB11_32
# BB#33:
	movq	%r12, (%rax)
	movq	%rbp, %rax
.LBB11_34:                              # %list_Nconc.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	def_ApplyDefToClauselist, .Lfunc_end11-def_ApplyDefToClauselist
	.cfi_endproc

	.globl	def_ApplyDefToTermlist
	.p2align	4, 0x90
	.type	def_ApplyDefToTermlist,@function
def_ApplyDefToTermlist:                 # @def_ApplyDefToTermlist
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi136:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi137:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi139:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi140:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi142:
	.cfi_def_cfa_offset 96
.Lcfi143:
	.cfi_offset %rbx, -56
.Lcfi144:
	.cfi_offset %r12, -48
.Lcfi145:
	.cfi_offset %r13, -40
.Lcfi146:
	.cfi_offset %r14, -32
.Lcfi147:
	.cfi_offset %r15, -24
.Lcfi148:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rdi, %r15
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	$1, (%r8)
	testq	%rsi, %rsi
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	je	.LBB12_1
# BB#2:                                 # %.lr.ph64
	xorl	%r12d, %r12d
	testl	%r9d, %r9d
	je	.LBB12_11
# BB#3:                                 # %.lr.ph64.split.preheader
	movq	%rsi, %r13
	.p2align	4, 0x90
.LBB12_4:                               # %.lr.ph64.split
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rsp), %r8
	callq	def_ApplyDefToTermOnce
	movq	%rax, %rbp
	cmpl	$0, 4(%rsp)
	jne	.LBB12_6
# BB#5:                                 #   in Loop: Header=BB12_4 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
.LBB12_6:                               #   in Loop: Header=BB12_4 Depth=1
	testq	%rbp, %rbp
	je	.LBB12_10
# BB#7:                                 #   in Loop: Header=BB12_4 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	%rbp, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	%r12, (%r14)
	movq	8(%r13), %rax
	movq	(%rax), %rdi
	callq	term_Delete
	movq	8(%r13), %rax
	movq	8(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB12_9
# BB#8:                                 #   in Loop: Header=BB12_4 Depth=1
	callq	string_StringFree
	movq	8(%r13), %rax
.LBB12_9:                               #   in Loop: Header=BB12_4 Depth=1
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	$0, 8(%r13)
	movq	%r14, %r12
.LBB12_10:                              #   in Loop: Header=BB12_4 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB12_4
	jmp	.LBB12_17
.LBB12_1:
	xorl	%r12d, %r12d
	jmp	.LBB12_17
.LBB12_11:                              # %.lr.ph64.split.us.preheader
	leaq	4(%rsp), %r14
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB12_12:                              # %.lr.ph64.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	callq	def_ApplyDefToTermOnce
	movq	%rax, %r13
	cmpl	$0, 4(%rsp)
	jne	.LBB12_14
# BB#13:                                #   in Loop: Header=BB12_12 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
.LBB12_14:                              #   in Loop: Header=BB12_12 Depth=1
	testq	%r13, %r13
	je	.LBB12_16
# BB#15:                                #   in Loop: Header=BB12_12 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	$0, 8(%rbx)
	movq	%r13, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB12_16:                              #   in Loop: Header=BB12_12 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB12_12
.LBB12_17:                              # %._crit_edge
	xorl	%esi, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	list_PointerDeleteElement
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB12_21
# BB#18:                                # %._crit_edge
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	148(%rax), %eax
	testl	%eax, %eax
	je	.LBB12_21
# BB#19:                                # %.lr.ph.preheader
	movq	stdout(%rip), %rcx
	movl	$.L.str.17, %edi
	movl	$42, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB12_20:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	8(%rbp), %rax
	movq	(%rax), %rdi
	callq	fol_PrettyPrint
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB12_20
.LBB12_21:                              # %.loopexit
	testq	%rbx, %rbx
	movq	%rbx, %rax
	cmoveq	%r12, %rax
	je	.LBB12_26
# BB#22:                                # %.loopexit
	testq	%r12, %r12
	je	.LBB12_26
# BB#23:                                # %.preheader.i.preheader
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB12_24:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB12_24
# BB#25:
	movq	%r12, (%rax)
	movq	%rbx, %rax
.LBB12_26:                              # %list_Nconc.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	def_ApplyDefToTermlist, .Lfunc_end12-def_ApplyDefToTermlist
	.cfi_endproc

	.globl	def_ExtractDefsFromTermlist
	.p2align	4, 0x90
	.type	def_ExtractDefsFromTermlist,@function
def_ExtractDefsFromTermlist:            # @def_ExtractDefsFromTermlist
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi150:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi151:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi152:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 48
.Lcfi154:
	.cfi_offset %rbx, -48
.Lcfi155:
	.cfi_offset %r12, -40
.Lcfi156:
	.cfi_offset %r13, -32
.Lcfi157:
	.cfi_offset %r14, -24
.Lcfi158:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	112(%r14), %r12
	xorl	%r13d, %r13d
	testq	%rbx, %rbx
	jne	.LBB13_17
	jmp	.LBB13_2
	.p2align	4, 0x90
.LBB13_23:                              # %list_Nconc.exit64
                                        #   in Loop: Header=BB13_17 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB13_2
.LBB13_17:                              # %.lr.ph92
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_21 Depth 2
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	fol_NormalizeVars
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	callq	def_ExtractDefsFromTerm
	testq	%r13, %r13
	je	.LBB13_18
# BB#19:                                #   in Loop: Header=BB13_17 Depth=1
	testq	%rax, %rax
	je	.LBB13_23
# BB#20:                                # %.preheader.i62.preheader
                                        #   in Loop: Header=BB13_17 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB13_21:                              # %.preheader.i62
                                        #   Parent Loop BB13_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB13_21
# BB#22:                                #   in Loop: Header=BB13_17 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB13_23
	.p2align	4, 0x90
.LBB13_18:                              #   in Loop: Header=BB13_17 Depth=1
	movq	%rax, %r13
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_17
	jmp	.LBB13_2
	.p2align	4, 0x90
.LBB13_30:                              # %list_Nconc.exit58
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	(%r15), %r15
.LBB13_2:                               # %.preheader81
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_24 Depth 2
                                        #     Child Loop BB13_28 Depth 2
	testq	%r15, %r15
	je	.LBB13_3
.LBB13_24:                              # %.lr.ph88
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%r15), %rax
	movq	(%rax), %rdi
	callq	fol_NormalizeVars
	movq	8(%r15), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	callq	def_ExtractDefsFromTerm
	testq	%r13, %r13
	je	.LBB13_25
# BB#26:                                #   in Loop: Header=BB13_2 Depth=1
	testq	%rax, %rax
	je	.LBB13_30
# BB#27:                                # %.preheader.i56.preheader
                                        #   in Loop: Header=BB13_2 Depth=1
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB13_28:                              # %.preheader.i56
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB13_28
# BB#29:                                #   in Loop: Header=BB13_2 Depth=1
	movq	%rax, (%rcx)
	jmp	.LBB13_30
	.p2align	4, 0x90
.LBB13_25:                              #   in Loop: Header=BB13_24 Depth=2
	movq	%rax, %r13
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB13_24
.LBB13_3:                               # %.preheader
	testq	%r13, %r13
	je	.LBB13_6
# BB#4:                                 # %.lr.ph85
	movl	symbol_TYPESTATBITS(%rip), %ecx
	movq	symbol_SIGNATURE(%rip), %rax
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB13_5:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rdx), %rsi
	movq	8(%rsi), %rsi
	xorl	%edi, %edi
	subl	(%rsi), %edi
	sarl	%cl, %edi
	movslq	%edi, %rsi
	movq	(%rax,%rsi,8), %rsi
	orl	$128, 20(%rsi)
	movq	(%rdx), %rdx
	testq	%rdx, %rdx
	jne	.LBB13_5
.LBB13_6:                               # %._crit_edge
	movq	(%r14), %rax
	testq	%rax, %rax
	movq	%r13, %rcx
	je	.LBB13_11
# BB#7:
	testq	%r13, %r13
	je	.LBB13_16
# BB#8:                                 # %.preheader.i.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB13_9:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB13_9
# BB#10:
	movq	%r13, (%rcx)
	movq	%rax, %rcx
.LBB13_11:                              # %list_Nconc.exit
	testq	%r13, %r13
	movq	%rcx, (%r14)
	je	.LBB13_16
# BB#12:                                # %list_Nconc.exit
	movl	148(%r12), %eax
	testl	%eax, %eax
	je	.LBB13_16
# BB#13:
	movq	stdout(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB13_16
	.p2align	4, 0x90
.LBB13_14:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	callq	def_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_14
.LBB13_16:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	def_ExtractDefsFromTermlist, .Lfunc_end13-def_ExtractDefsFromTermlist
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.zero	16
	.text
	.globl	def_FlattenWithOneDefinition
	.p2align	4, 0x90
	.type	def_FlattenWithOneDefinition,@function
def_FlattenWithOneDefinition:           # @def_FlattenWithOneDefinition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi162:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi163:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi165:
	.cfi_def_cfa_offset 96
.Lcfi166:
	.cfi_offset %rbx, -56
.Lcfi167:
	.cfi_offset %r12, -48
.Lcfi168:
	.cfi_offset %r13, -40
.Lcfi169:
	.cfi_offset %r14, -32
.Lcfi170:
	.cfi_offset %r15, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	cmpq	$0, 16(%rbx)
	je	.LBB14_2
.LBB14_9:
	xorl	%r15d, %r15d
	jmp	.LBB14_10
.LBB14_2:
	movq	(%rdi), %r14
	testq	%r14, %r14
	je	.LBB14_9
# BB#3:                                 # %.lr.ph
	movq	104(%rdi), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	112(%rdi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB14_4:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rbp
	cmpq	%rbx, %rbp
	je	.LBB14_8
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=1
	movq	(%rbp), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	je	.LBB14_8
# BB#6:                                 #   in Loop: Header=BB14_4 Depth=1
	movq	(%rbx), %rdi
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	jne	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_4 Depth=1
	movq	(%rbp), %rsi
	movq	%rbx, %rdi
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	36(%rsp), %r8
	callq	def_ApplyDefToTermOnce
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	8(%rbp), %rdi
	callq	term_Copy
	movq	%rax, %r13
	movq	16(%rbp), %rdi
	callq	term_Copy
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%rax, %r15
	movq	32(%rbp), %rbp
	movl	$48, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r12)
	movq	%r13, 8(%r12)
	movq	%r15, 16(%r12)
	movl	$16, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%rax, 24(%r12)
	movq	%rbp, 32(%r12)
	movl	$0, 40(%r12)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%r12, 8(%r15)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%r15)
	.p2align	4, 0x90
.LBB14_8:                               #   in Loop: Header=BB14_4 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB14_4
.LBB14_10:                              # %.loopexit
	movq	%r15, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	def_FlattenWithOneDefinition, .Lfunc_end14-def_FlattenWithOneDefinition
	.cfi_endproc

	.globl	def_FlattenWithOneDefinitionDestructive
	.p2align	4, 0x90
	.type	def_FlattenWithOneDefinitionDestructive,@function
def_FlattenWithOneDefinitionDestructive: # @def_FlattenWithOneDefinitionDestructive
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi172:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi173:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi174:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi175:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi176:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi178:
	.cfi_def_cfa_offset 96
.Lcfi179:
	.cfi_offset %rbx, -56
.Lcfi180:
	.cfi_offset %r12, -48
.Lcfi181:
	.cfi_offset %r13, -40
.Lcfi182:
	.cfi_offset %r14, -32
.Lcfi183:
	.cfi_offset %r15, -24
.Lcfi184:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	cmpq	$0, 16(%rbx)
	jne	.LBB15_17
# BB#1:
	movq	(%rbp), %r15
	testq	%r15, %r15
	je	.LBB15_2
# BB#3:                                 # %.lr.ph
	movq	104(%rbp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	112(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	8(%rbx), %r13
	movl	symbol_TYPESTATBITS(%rip), %ebp
	movq	%r15, (%rsp)            # 8-byte Spill
	jmp	.LBB15_4
.LBB15_7:                               #   in Loop: Header=BB15_4 Depth=1
	movq	(%r14), %rsi
	movq	%rbx, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	36(%rsp), %r8
	callq	def_ApplyDefToTermOnce
	movq	%rax, %r12
	movq	(%r14), %rdi
	callq	term_Delete
	movq	%r12, (%r14)
	jmp	.LBB15_12
	.p2align	4, 0x90
.LBB15_4:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r15), %r14
	cmpq	%rbx, %r14
	je	.LBB15_11
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=1
	movq	(%r14), %rdi
	movq	(%r13), %rax
	movl	(%rax), %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	je	.LBB15_12
# BB#6:                                 #   in Loop: Header=BB15_4 Depth=1
	movq	(%rbx), %rdi
	movq	8(%r14), %rax
	movl	(%rax), %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	je	.LBB15_7
# BB#8:                                 #   in Loop: Header=BB15_4 Depth=1
	movq	8(%r14), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movl	%ebp, %ecx
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	20(%rax), %ecx
	testb	%cl, %cl
	jns	.LBB15_10
# BB#9:                                 #   in Loop: Header=BB15_4 Depth=1
	addl	$-128, %ecx
	movl	%ecx, 20(%rax)
.LBB15_10:                              # %symbol_RemoveProperty.exit57
                                        #   in Loop: Header=BB15_4 Depth=1
	movq	%r14, %rdi
	callq	def_Delete
.LBB15_11:                              #   in Loop: Header=BB15_4 Depth=1
	movq	$0, 8(%r15)
.LBB15_12:                              #   in Loop: Header=BB15_4 Depth=1
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB15_4
# BB#13:
	movl	%ebp, %ecx
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
	jmp	.LBB15_14
.LBB15_2:                               # %.._crit_edge_crit_edge
	movl	symbol_TYPESTATBITS(%rip), %ecx
	leaq	8(%rbx), %r13
.LBB15_14:                              # %._crit_edge
	movq	(%r13), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	20(%rax), %ecx
	testb	%cl, %cl
	jns	.LBB15_16
# BB#15:
	addl	$-128, %ecx
	movl	%ecx, 20(%rax)
.LBB15_16:                              # %symbol_RemoveProperty.exit
	movq	%rbx, %rdi
	callq	def_Delete
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	list_PointerDeleteElement
	movq	%rax, (%rbp)
.LBB15_17:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	def_FlattenWithOneDefinitionDestructive, .Lfunc_end15-def_FlattenWithOneDefinitionDestructive
	.cfi_endproc

	.globl	def_FlattenWithOneDefinitionSemiDestructive
	.p2align	4, 0x90
	.type	def_FlattenWithOneDefinitionSemiDestructive,@function
def_FlattenWithOneDefinitionSemiDestructive: # @def_FlattenWithOneDefinitionSemiDestructive
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi185:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi186:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi188:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi189:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi190:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi191:
	.cfi_def_cfa_offset 64
.Lcfi192:
	.cfi_offset %rbx, -56
.Lcfi193:
	.cfi_offset %r12, -48
.Lcfi194:
	.cfi_offset %r13, -40
.Lcfi195:
	.cfi_offset %r14, -32
.Lcfi196:
	.cfi_offset %r15, -24
.Lcfi197:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	cmpq	$0, 16(%rbx)
	jne	.LBB16_11
# BB#1:
	movq	(%rdi), %r13
	testq	%r13, %r13
	je	.LBB16_11
# BB#2:                                 # %.lr.ph
	movq	104(%rdi), %r14
	movq	112(%rdi), %r15
	.p2align	4, 0x90
.LBB16_3:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rbp
	cmpq	%rbx, %rbp
	je	.LBB16_10
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	(%rbp), %rdi
	movq	8(%rbx), %rax
	movl	(%rax), %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	je	.LBB16_10
# BB#5:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	(%rbx), %rdi
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	callq	term_ContainsSymbol
	testl	%eax, %eax
	je	.LBB16_6
# BB#7:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	8(%rbp), %rax
	xorl	%edx, %edx
	subl	(%rax), %edx
	movl	symbol_TYPESTATBITS(%rip), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edx
	movq	symbol_SIGNATURE(%rip), %rax
	movslq	%edx, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	20(%rax), %ecx
	testb	%cl, %cl
	jns	.LBB16_9
# BB#8:                                 #   in Loop: Header=BB16_3 Depth=1
	addl	$-128, %ecx
	movl	%ecx, 20(%rax)
.LBB16_9:                               # %symbol_RemoveProperty.exit
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	%rbp, %rdi
	callq	def_Delete
	movq	$0, 8(%r13)
	jmp	.LBB16_10
.LBB16_6:                               #   in Loop: Header=BB16_3 Depth=1
	movq	(%rbp), %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	leaq	4(%rsp), %r8
	callq	def_ApplyDefToTermOnce
	movq	%rax, %r12
	movq	(%rbp), %rdi
	callq	term_Delete
	movq	%r12, (%rbp)
	.p2align	4, 0x90
.LBB16_10:                              #   in Loop: Header=BB16_3 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB16_3
.LBB16_11:                              # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	def_FlattenWithOneDefinitionSemiDestructive, .Lfunc_end16-def_FlattenWithOneDefinitionSemiDestructive
	.cfi_endproc

	.globl	def_FlattenDefinitionsDestructive
	.p2align	4, 0x90
	.type	def_FlattenDefinitionsDestructive,@function
def_FlattenDefinitionsDestructive:      # @def_FlattenDefinitionsDestructive
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 32
.Lcfi201:
	.cfi_offset %rbx, -32
.Lcfi202:
	.cfi_offset %r14, -24
.Lcfi203:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB17_1
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %r15
	movq	8(%r15), %rdi
	callq	fol_PrettyPrintDFG
	testq	%r15, %r15
	je	.LBB17_4
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	def_FlattenWithOneDefinitionSemiDestructive
.LBB17_4:                               #   in Loop: Header=BB17_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB17_2
# BB#5:                                 # %._crit_edge.loopexit
	movq	(%r14), %rdi
	jmp	.LBB17_6
.LBB17_1:
	xorl	%edi, %edi
.LBB17_6:                               # %._crit_edge
	xorl	%esi, %esi
	callq	list_PointerDeleteElement
	movq	%rax, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	def_FlattenDefinitionsDestructive, .Lfunc_end17-def_FlattenDefinitionsDestructive
	.cfi_endproc

	.globl	def_GetTermsForProof
	.p2align	4, 0x90
	.type	def_GetTermsForProof,@function
def_GetTermsForProof:                   # @def_GetTermsForProof
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi204:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi205:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi206:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi207:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi208:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi209:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi210:
	.cfi_def_cfa_offset 64
.Lcfi211:
	.cfi_offset %rbx, -56
.Lcfi212:
	.cfi_offset %r12, -48
.Lcfi213:
	.cfi_offset %r13, -40
.Lcfi214:
	.cfi_offset %r14, -32
.Lcfi215:
	.cfi_offset %r15, -24
.Lcfi216:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpl	$1, %r12d
	jne	.LBB18_20
	jmp	.LBB18_2
	.p2align	4, 0x90
.LBB18_4:                               #   in Loop: Header=BB18_2 Depth=1
	cmpl	fol_OR(%rip), %ebp
	je	.LBB18_23
# BB#5:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	$-1, %r12d
	jne	.LBB18_8
# BB#6:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	fol_AND(%rip), %ebp
	je	.LBB18_7
.LBB18_8:                               #   in Loop: Header=BB18_2 Depth=1
	cmpl	fol_IMPLIES(%rip), %ebp
	jne	.LBB18_13
# BB#9:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	16(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	movq	16(%r15), %rcx
	movq	8(%rcx), %rdi
	testl	%eax, %eax
	jne	.LBB18_63
# BB#10:                                #   in Loop: Header=BB18_2 Depth=1
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	cmpl	$-1, %r12d
	jne	.LBB18_13
# BB#11:                                #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_12
.LBB18_13:                              #   in Loop: Header=BB18_2 Depth=1
	cmpl	fol_IMPLIED(%rip), %ebp
	jne	.LBB18_18
# BB#14:                                #   in Loop: Header=BB18_2 Depth=1
	movq	16(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	movq	16(%r15), %rcx
	movq	8(%rcx), %rdi
	cmpl	$-1, %r12d
	jne	.LBB18_17
# BB#15:                                #   in Loop: Header=BB18_2 Depth=1
	testl	%eax, %eax
	jne	.LBB18_16
.LBB18_17:                              #   in Loop: Header=BB18_2 Depth=1
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	jne	.LBB18_62
.LBB18_18:                              #   in Loop: Header=BB18_2 Depth=1
	cmpl	%ebp, fol_ALL(%rip)
	movq	%r15, %rbx
	je	.LBB18_2
# BB#19:                                #   in Loop: Header=BB18_2 Depth=1
	cmpl	%ebp, fol_EXIST(%rip)
	movq	%r15, %rbx
	je	.LBB18_2
	jmp	.LBB18_67
	.p2align	4, 0x90
.LBB18_20:                              # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	cmpq	%r14, %rbx
	je	.LBB18_67
# BB#21:                                #   in Loop: Header=BB18_20 Depth=1
	movq	8(%rbx), %r15
	movl	(%r15), %ebp
	cmpl	fol_NOT(%rip), %ebp
	je	.LBB18_22
# BB#35:                                #   in Loop: Header=BB18_20 Depth=1
	cmpl	$-1, %r12d
	jne	.LBB18_52
# BB#36:                                #   in Loop: Header=BB18_20 Depth=1
	cmpl	fol_AND(%rip), %ebp
	je	.LBB18_37
.LBB18_52:                              #   in Loop: Header=BB18_20 Depth=1
	cmpl	fol_IMPLIES(%rip), %ebp
	jne	.LBB18_57
# BB#53:                                #   in Loop: Header=BB18_20 Depth=1
	movq	16(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	movq	16(%r15), %rax
	movq	8(%rax), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	cmpl	$-1, %r12d
	jne	.LBB18_57
# BB#54:                                #   in Loop: Header=BB18_20 Depth=1
	testl	%eax, %eax
	jne	.LBB18_55
.LBB18_57:                              #   in Loop: Header=BB18_20 Depth=1
	cmpl	fol_IMPLIED(%rip), %ebp
	jne	.LBB18_65
# BB#58:                                #   in Loop: Header=BB18_20 Depth=1
	movq	16(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	movq	16(%r15), %rcx
	movq	8(%rcx), %rdi
	cmpl	$-1, %r12d
	jne	.LBB18_64
# BB#59:                                #   in Loop: Header=BB18_20 Depth=1
	testl	%eax, %eax
	jne	.LBB18_60
.LBB18_64:                              #   in Loop: Header=BB18_20 Depth=1
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
.LBB18_65:                              #   in Loop: Header=BB18_20 Depth=1
	cmpl	%ebp, fol_ALL(%rip)
	movq	%r15, %rbx
	je	.LBB18_20
# BB#66:                                #   in Loop: Header=BB18_20 Depth=1
	cmpl	%ebp, fol_EXIST(%rip)
	movq	%r15, %rbx
	je	.LBB18_20
	jmp	.LBB18_67
.LBB18_22:                              # %.us-lcssa.us
	negl	%r12d
	movq	%r15, %rbx
	cmpl	$1, %r12d
	jne	.LBB18_20
	.p2align	4, 0x90
.LBB18_2:                               # %tailrecurse.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	term_AddFatherLinks
	cmpq	%r14, %rbx
	je	.LBB18_67
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	8(%rbx), %r15
	movl	(%r15), %ebp
	cmpl	fol_NOT(%rip), %ebp
	jne	.LBB18_4
	jmp	.LBB18_22
.LBB18_67:                              # %.us-lcssa179.us
	xorl	%eax, %eax
	jmp	.LBB18_68
.LBB18_37:
	movl	$-1, %r12d
	jmp	.LBB18_38
.LBB18_23:                              # %.us-lcssa173.us
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	16(%r15), %r14
	xorl	%ebp, %ebp
	testq	%r14, %r14
	jne	.LBB18_25
	jmp	.LBB18_28
	.p2align	4, 0x90
.LBB18_27:                              #   in Loop: Header=BB18_25 Depth=1
	movq	(%r14), %r14
	testq	%r14, %r14
	je	.LBB18_28
.LBB18_25:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	jne	.LBB18_27
# BB#26:                                #   in Loop: Header=BB18_25 Depth=1
	movl	fol_NOT(%rip), %r12d
	movq	8(%r14), %rdi
	callq	term_Copy
	movq	%rax, %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	$0, (%rax)
	movl	%r12d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, %rbp
	jmp	.LBB18_27
.LBB18_28:                              # %._crit_edge
	movq	%rbp, %rdi
	callq	list_Length
	cmpl	$1, %eax
	jne	.LBB18_31
# BB#29:
	movq	8(%rbp), %rbx
	testq	%rbp, %rbp
	movq	(%rsp), %rdi            # 8-byte Reload
	je	.LBB18_32
	.p2align	4, 0x90
.LBB18_30:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB18_30
	jmp	.LBB18_32
.LBB18_7:
	movl	$1, %r12d
.LBB18_38:                              # %.us-lcssa174.us
	movq	16(%r15), %rdi
	callq	list_Length
	movq	16(%r15), %rdi
	cmpl	$2, %eax
	jne	.LBB18_42
# BB#39:
	movq	8(%rdi), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	movq	16(%r15), %rcx
	testl	%eax, %eax
	je	.LBB18_40
# BB#41:
	movq	(%rcx), %rax
	movq	8(%rax), %rdi
	jmp	.LBB18_50
.LBB18_42:
	callq	list_Length
	movq	16(%r15), %rbp
	cmpl	$3, %eax
	jb	.LBB18_49
# BB#43:                                # %.preheader
	movl	%r12d, (%rsp)           # 4-byte Spill
	xorl	%r13d, %r13d
	testq	%rbp, %rbp
	jne	.LBB18_45
	jmp	.LBB18_48
	.p2align	4, 0x90
.LBB18_47:                              #   in Loop: Header=BB18_45 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB18_48
.LBB18_45:                              # %.lr.ph170
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rdi
	movq	%rbx, %rsi
	callq	term_HasPointerSubterm
	testl	%eax, %eax
	jne	.LBB18_47
# BB#46:                                #   in Loop: Header=BB18_45 Depth=1
	movq	8(%rbp), %rdi
	callq	term_Copy
	movq	%r14, %r12
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r12, %r14
	movq	%r13, (%rax)
	movq	%rax, %r13
	jmp	.LBB18_47
.LBB18_55:
	leaq	16(%r15), %rax
	movl	$-1, %ebx
	jmp	.LBB18_56
.LBB18_60:
	movl	$-1, %ebx
	jmp	.LBB18_61
.LBB18_49:
	movq	8(%rbp), %rdi
	jmp	.LBB18_50
.LBB18_48:                              # %._crit_edge171
	movl	fol_AND(%rip), %edi
	movq	%r13, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movl	(%rsp), %r12d           # 4-byte Reload
	jmp	.LBB18_51
.LBB18_31:
	movl	fol_AND(%rip), %edi
	movq	%rbp, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB18_32:                              # %list_Delete.exit
	movl	$1, %edx
	jmp	.LBB18_33
.LBB18_12:
	leaq	16(%r15), %rax
	movl	$1, %ebx
.LBB18_56:                              # %.us-lcssa176.us
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	jmp	.LBB18_61
.LBB18_16:
	movl	$1, %ebx
.LBB18_61:                              # %.us-lcssa177.us
	callq	term_Copy
	movq	%rax, %rbp
	movl	fol_NOT(%rip), %r12d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movl	%r12d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbp
	negl	%ebx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	def_GetTermsForProof
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	jmp	.LBB18_68
.LBB18_62:                              # %.us-lcssa178.us
	movq	16(%r15), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
.LBB18_63:                              # %.us-lcssa175.us
	callq	term_Copy
	movq	%rax, %rbx
	movl	$1, %edx
	movq	%r14, %rdi
.LBB18_33:                              # %list_Delete.exit
	movq	%r15, %rsi
	jmp	.LBB18_34
.LBB18_40:
	movq	8(%rcx), %rdi
.LBB18_50:
	callq	term_Copy
	movq	%rax, %rbx
.LBB18_51:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movl	%r12d, %edx
.LBB18_34:                              # %list_Delete.exit
	callq	def_GetTermsForProof
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
.LBB18_68:                              # %.us-lcssa179.us
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	def_GetTermsForProof, .Lfunc_end18-def_GetTermsForProof
	.cfi_endproc

	.globl	def_FindProofForGuard
	.p2align	4, 0x90
	.type	def_FindProofForGuard,@function
def_FindProofForGuard:                  # @def_FindProofForGuard
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi217:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi218:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi219:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi220:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi221:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi223:
	.cfi_def_cfa_offset 80
.Lcfi224:
	.cfi_offset %rbx, -56
.Lcfi225:
	.cfi_offset %r12, -48
.Lcfi226:
	.cfi_offset %r13, -40
.Lcfi227:
	.cfi_offset %r14, -32
.Lcfi228:
	.cfi_offset %r15, -24
.Lcfi229:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	callq	term_Copy
	movq	%rax, %r12
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	def_GetTermsForProof
	testq	%rax, %rax
	je	.LBB19_8
# BB#1:
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movl	fol_AND(%rip), %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	fol_FreeVariables
	movq	%rax, %rbp
	movq	%r14, %rdi
	callq	fol_FreeVariables
	testq	%rbp, %rbp
	movq	%r15, 16(%rsp)          # 8-byte Spill
	je	.LBB19_2
# BB#3:
	testq	%rax, %rax
	je	.LBB19_7
# BB#4:                                 # %.preheader.i.preheader
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB19_5:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB19_5
# BB#6:
	movq	%rax, (%rcx)
	jmp	.LBB19_7
.LBB19_8:
	xorl	%edi, %edi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	%r12, %rdi
	callq	term_Delete
	jmp	.LBB19_9
.LBB19_2:
	movq	%rax, %rbp
.LBB19_7:                               # %list_Nconc.exit
	movl	$term_Equal, %esi
	movq	%rbp, %rdi
	callq	list_DeleteDuplicates
	movq	%rax, %r15
	movl	$term_Copy, %esi
	movq	%r15, %rdi
	callq	list_NMapCar
	movq	%r14, %rdi
	callq	term_Copy
	movq	%rax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbp, (%rax)
	movl	fol_IMPLIES(%rip), %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movl	fol_ALL(%rip), %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movl	%ebp, %edi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	fol_CreateQuantifier
	movq	%rax, %rbx
	xorl	%edi, %edi
	movq	%rbx, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	cnf_HaveProof
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	term_Delete
	movq	%r12, %rdi
	callq	term_Delete
	movl	$1, %eax
	testl	%ebp, %ebp
	jne	.LBB19_10
.LBB19_9:
	xorl	%eax, %eax
.LBB19_10:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	def_FindProofForGuard, .Lfunc_end19-def_FindProofForGuard
	.cfi_endproc

	.globl	def_ApplyDefinitionToTermList
	.p2align	4, 0x90
	.type	def_ApplyDefinitionToTermList,@function
def_ApplyDefinitionToTermList:          # @def_ApplyDefinitionToTermList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi230:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi231:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi233:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi234:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi236:
	.cfi_def_cfa_offset 144
.Lcfi237:
	.cfi_offset %rbx, -56
.Lcfi238:
	.cfi_offset %r12, -48
.Lcfi239:
	.cfi_offset %r13, -40
.Lcfi240:
	.cfi_offset %r14, -32
.Lcfi241:
	.cfi_offset %r15, -24
.Lcfi242:
	.cfi_offset %rbp, -16
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movl	204(%rdx), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	testl	%eax, %eax
	je	.LBB20_53
# BB#1:
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB20_4:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_7 Depth 2
                                        #       Child Loop BB20_10 Depth 3
                                        #         Child Loop BB20_15 Depth 4
                                        #           Child Loop BB20_18 Depth 5
                                        #           Child Loop BB20_29 Depth 5
                                        #           Child Loop BB20_41 Depth 5
                                        #         Child Loop BB20_48 Depth 4
	testq	%rdi, %rdi
	je	.LBB20_53
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB20_4 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB20_53
# BB#6:                                 # %.lr.ph160.preheader
                                        #   in Loop: Header=BB20_4 Depth=1
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB20_7:                               # %.lr.ph160
                                        #   Parent Loop BB20_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB20_10 Depth 3
                                        #         Child Loop BB20_15 Depth 4
                                        #           Child Loop BB20_18 Depth 5
                                        #           Child Loop BB20_29 Depth 5
                                        #           Child Loop BB20_41 Depth 5
                                        #         Child Loop BB20_48 Depth 4
	movq	8(%rbx), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r13
	cmpq	$0, 24(%rsp)            # 8-byte Folded Reload
	je	.LBB20_51
# BB#8:                                 # %.lr.ph160
                                        #   in Loop: Header=BB20_7 Depth=2
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB20_51
# BB#9:                                 # %.lr.ph152
                                        #   in Loop: Header=BB20_7 Depth=2
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	%r13, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB20_10:                              #   Parent Loop BB20_4 Depth=1
                                        #     Parent Loop BB20_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB20_15 Depth 4
                                        #           Child Loop BB20_18 Depth 5
                                        #           Child Loop BB20_29 Depth 5
                                        #           Child Loop BB20_41 Depth 5
                                        #         Child Loop BB20_48 Depth 4
	movq	%rbx, %r14
	movq	8(%rbp), %rax
	movq	(%rax), %rbx
	movl	(%r13), %esi
	movq	%rbx, %rdi
	callq	term_FindAllAtoms
	movq	%rax, %r12
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	testq	%r12, %r12
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	je	.LBB20_11
# BB#12:                                #   in Loop: Header=BB20_10 Depth=3
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB20_13
# BB#14:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB20_10 Depth=3
	movl	cont_BINDINGS(%rip), %eax
	movl	cont_STACKPOINTER(%rip), %ecx
	movq	%r12, %rbp
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB20_15:                              # %.lr.ph
                                        #   Parent Loop BB20_4 Depth=1
                                        #     Parent Loop BB20_7 Depth=2
                                        #       Parent Loop BB20_10 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB20_18 Depth 5
                                        #           Child Loop BB20_29 Depth 5
                                        #           Child Loop BB20_41 Depth 5
	movq	8(%rbp), %r15
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movslq	%ecx, %rcx
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	unify_Match
	testl	%eax, %eax
	je	.LBB20_39
# BB#16:                                #   in Loop: Header=BB20_15 Depth=4
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB20_19
# BB#17:                                # %.lr.ph.i127.preheader
                                        #   in Loop: Header=BB20_15 Depth=4
	incl	%eax
	.p2align	4, 0x90
.LBB20_18:                              # %.lr.ph.i127
                                        #   Parent Loop BB20_4 Depth=1
                                        #     Parent Loop BB20_7 Depth=2
                                        #       Parent Loop BB20_10 Depth=3
                                        #         Parent Loop BB20_15 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB20_18
.LBB20_19:                              # %._crit_edge.i128
                                        #   in Loop: Header=BB20_15 Depth=4
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB20_21
# BB#20:                                #   in Loop: Header=BB20_15 Depth=4
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB20_21:                              # %cont_BackTrack.exit129
                                        #   in Loop: Header=BB20_15 Depth=4
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	term_Copy
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	term_MaxVar
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	%eax, %esi
	callq	fol_NormalizeVarsStartingAt
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	%r15, %rdx
	callq	unify_Match
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r14, %rsi
	callq	fol_ApplyContextToTerm
	testl	%eax, %eax
	je	.LBB20_38
# BB#22:                                #   in Loop: Header=BB20_15 Depth=4
	movq	8(%rbx), %rax
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB20_23
# BB#26:                                #   in Loop: Header=BB20_15 Depth=4
	callq	term_Copy
	movq	%rax, %rbx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rbx, %rsi
	callq	fol_ApplyContextToTerm
	testl	%eax, %eax
	je	.LBB20_37
# BB#27:                                #   in Loop: Header=BB20_15 Depth=4
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB20_30
# BB#28:                                # %.lr.ph.i121.preheader
                                        #   in Loop: Header=BB20_15 Depth=4
	incl	%eax
	.p2align	4, 0x90
.LBB20_29:                              # %.lr.ph.i121
                                        #   Parent Loop BB20_4 Depth=1
                                        #     Parent Loop BB20_7 Depth=2
                                        #       Parent Loop BB20_10 Depth=3
                                        #         Parent Loop BB20_15 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB20_29
.LBB20_30:                              # %._crit_edge.i122
                                        #   in Loop: Header=BB20_15 Depth=4
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	movq	40(%rsp), %r13          # 8-byte Reload
	je	.LBB20_32
# BB#31:                                #   in Loop: Header=BB20_15 Depth=4
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB20_32:                              # %cont_BackTrack.exit123
                                        #   in Loop: Header=BB20_15 Depth=4
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	72(%rsp), %r8           # 8-byte Reload
	callq	def_FindProofForGuard
	testl	%eax, %eax
	je	.LBB20_36
# BB#33:                                #   in Loop: Header=BB20_15 Depth=4
	decl	8(%rsp)                 # 4-byte Folded Spill
	movl	(%r14), %eax
	movl	%eax, (%r15)
	movq	16(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%r14), %rax
	movq	%rax, 16(%r15)
	movq	$0, 16(%r14)
	movq	%r13, %rdi
	callq	term_AddFatherLinks
	cmpl	$0, 148(%rbx)
	je	.LBB20_34
# BB#35:                                #   in Loop: Header=BB20_15 Depth=4
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	movl	$.L.str.19, %edi
	callq	puts
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	8(%rax), %rdi
	callq	fol_PrettyPrintDFG
	movl	$.L.str.20, %edi
	callq	puts
	movq	%r13, %rdi
	callq	fol_PrettyPrintDFG
	jmp	.LBB20_36
.LBB20_23:                              #   in Loop: Header=BB20_15 Depth=4
	decl	8(%rsp)                 # 4-byte Folded Spill
	movl	(%r14), %eax
	movl	%eax, (%r15)
	movq	16(%r15), %rdi
	movl	$term_Delete, %esi
	callq	list_DeleteWithElement
	movq	16(%r14), %rax
	movq	%rax, 16(%r15)
	movq	$0, 16(%r14)
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	term_AddFatherLinks
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 148(%rax)
	je	.LBB20_24
# BB#25:                                #   in Loop: Header=BB20_15 Depth=4
	movl	$.L.str.19, %edi
	callq	puts
	movq	16(%rsp), %r15          # 8-byte Reload
	movq	8(%r15), %rax
	movq	8(%rax), %rdi
	callq	fol_PrettyPrintDFG
	movl	$.L.str.20, %edi
	callq	puts
	movq	%rbx, %rdi
	movq	%r15, %rbx
	callq	fol_PrettyPrintDFG
	jmp	.LBB20_38
.LBB20_24:                              #   in Loop: Header=BB20_15 Depth=4
	movq	16(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB20_38
.LBB20_34:                              #   in Loop: Header=BB20_15 Depth=4
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
.LBB20_36:                              #   in Loop: Header=BB20_15 Depth=4
	movq	32(%rsp), %rbx          # 8-byte Reload
.LBB20_37:                              #   in Loop: Header=BB20_15 Depth=4
	movq	%rbx, %rdi
	callq	term_Delete
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	80(%rsp), %r13          # 8-byte Reload
.LBB20_38:                              #   in Loop: Header=BB20_15 Depth=4
	movq	%r14, %rdi
	callq	term_Delete
.LBB20_39:                              #   in Loop: Header=BB20_15 Depth=4
	xorps	%xmm0, %xmm0
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	jle	.LBB20_40
	.p2align	4, 0x90
.LBB20_41:                              # %.lr.ph.i117
                                        #   Parent Loop BB20_4 Depth=1
                                        #     Parent Loop BB20_7 Depth=2
                                        #       Parent Loop BB20_10 Depth=3
                                        #         Parent Loop BB20_15 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	cont_LASTBINDING(%rip), %rax
	movq	%rax, cont_CURRENTBINDING(%rip)
	movq	24(%rax), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rax)
	movl	$0, 20(%rax)
	movq	cont_CURRENTBINDING(%rip), %rax
	movq	$0, 24(%rax)
	leal	-1(%rcx), %eax
	movl	%eax, cont_BINDINGS(%rip)
	cmpl	$1, %ecx
	movl	%eax, %ecx
	jg	.LBB20_41
	jmp	.LBB20_42
	.p2align	4, 0x90
.LBB20_40:                              #   in Loop: Header=BB20_15 Depth=4
	movl	%ecx, %eax
.LBB20_42:                              # %._crit_edge.i
                                        #   in Loop: Header=BB20_15 Depth=4
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB20_43
# BB#44:                                #   in Loop: Header=BB20_15 Depth=4
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
	jmp	.LBB20_45
	.p2align	4, 0x90
.LBB20_43:                              #   in Loop: Header=BB20_15 Depth=4
	xorl	%ecx, %ecx
.LBB20_45:                              # %cont_BackTrack.exit
                                        #   in Loop: Header=BB20_15 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB20_47
# BB#46:                                # %cont_BackTrack.exit
                                        #   in Loop: Header=BB20_15 Depth=4
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB20_15
	jmp	.LBB20_47
	.p2align	4, 0x90
.LBB20_11:                              #   in Loop: Header=BB20_10 Depth=3
	movq	%r14, %rbx
	testq	%r12, %r12
	jne	.LBB20_48
	jmp	.LBB20_49
	.p2align	4, 0x90
.LBB20_13:                              #   in Loop: Header=BB20_10 Depth=3
	movq	%r14, %rbx
.LBB20_47:                              # %._crit_edge
                                        #   in Loop: Header=BB20_10 Depth=3
	testq	%r12, %r12
	je	.LBB20_49
	.p2align	4, 0x90
.LBB20_48:                              # %.lr.ph.i
                                        #   Parent Loop BB20_4 Depth=1
                                        #     Parent Loop BB20_7 Depth=2
                                        #       Parent Loop BB20_10 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB20_48
.LBB20_49:                              # %list_Delete.exit
                                        #   in Loop: Header=BB20_10 Depth=3
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB20_51
# BB#50:                                # %list_Delete.exit
                                        #   in Loop: Header=BB20_10 Depth=3
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB20_10
.LBB20_51:                              # %._crit_edge153
                                        #   in Loop: Header=BB20_7 Depth=2
	movq	%r13, %rdi
	callq	term_Delete
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB20_2
# BB#52:                                # %._crit_edge153
                                        #   in Loop: Header=BB20_7 Depth=2
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB20_7
.LBB20_2:                               # %.loopexit
                                        #   in Loop: Header=BB20_4 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	56(%rsp), %rdi          # 8-byte Reload
	je	.LBB20_53
# BB#3:                                 # %.loopexit
                                        #   in Loop: Header=BB20_4 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jne	.LBB20_4
.LBB20_53:                              # %._crit_edge164
	movq	24(%rsp), %rax          # 8-byte Reload
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	def_ApplyDefinitionToTermList, .Lfunc_end20-def_ApplyDefinitionToTermList
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\nNew definition found :"
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\nFound definitions :\n"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n---\n"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\n\nAtom: "
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\nExpansion: \n"
	.size	.L.str.4, 14

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nParent clauses: "
	.size	.L.str.5, 18

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%d.%d "
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\nDerived from conjecture clauses."
	.size	.L.str.7, 34

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\nNot derived from conjecture clauses."
	.size	.L.str.8, 38

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\nLabel: "
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\nGuard:"
	.size	.L.str.10, 8

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Nothing."
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nAttributes: "
	.size	.L.str.12, 14

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	" Equality "
	.size	.L.str.13, 11

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	" No Multiple Occurrences "
	.size	.L.str.14, 26

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	" None "
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"\nNew clauses after applying definitions : \n"
	.size	.L.str.16, 44

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n\nNew terms after applying definitions : \n"
	.size	.L.str.17, 43

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\nApplied definition for"
	.size	.L.str.19, 24

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\nNew formula:"
	.size	.L.str.20, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
