	.text
	.file	"getNextCommandCode.bc"
	.globl	getNextCommandCode
	.p2align	4, 0x90
	.type	getNextCommandCode,@function
getNextCommandCode:                     # @getNextCommandCode
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	leaq	8(%rsp), %rsi
	callq	getInt
	movq	%rax, %rcx
	cmpq	$3, %rcx
	ja	.LBB0_1
# BB#2:
	movl	$4, %edx
	xorl	%eax, %eax
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_3:
	movq	8(%rsp), %rdx
	cmpq	$4, %rdx
	jae	.LBB0_4
# BB#7:                                 # %switch.lookup
	xorl	%eax, %eax
	jmp	.LBB0_8
.LBB0_4:
	movl	$.L.str, %edi
	jmp	.LBB0_5
.LBB0_1:
	movq	%rcx, %rax
	jmp	.LBB0_9
.LBB0_6:
	movl	$.L.str.1, %edi
.LBB0_5:                                # %.sink.split
	xorl	%esi, %esi
	callq	errorMessage
	movl	$getNextCommandCode.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$5, %edx
	movl	$2, %eax
.LBB0_8:                                # %.sink.split
	movl	%edx, (%rbx)
.LBB0_9:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end0:
	.size	getNextCommandCode, .Lfunc_end0-getNextCommandCode
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_3
	.quad	.LBB0_8
	.quad	.LBB0_4
	.quad	.LBB0_6

	.type	getNextCommandCode.name,@object # @getNextCommandCode.name
	.data
	.p2align	4
getNextCommandCode.name:
	.asciz	"getNextCommandCode"
	.size	getNextCommandCode.name, 19

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"unknown command code"
	.size	.L.str, 21

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"improper format - code must be an integer"
	.size	.L.str.1, 42


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
