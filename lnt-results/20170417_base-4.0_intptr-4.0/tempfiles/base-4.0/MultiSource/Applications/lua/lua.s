	.text
	.file	"lua.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 64
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebp
	callq	luaL_newstate
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#4:
	movq	$.L.str.1, (%r14)
	movl	%ebp, 8(%rsp)
	movq	%r14, 16(%rsp)
	leaq	8(%rsp), %rdx
	movl	$pmain, %esi
	movq	%rbx, %rdi
	callq	lua_cpcall
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	report
	movq	%rbx, %rdi
	callq	lua_close
	xorl	%eax, %eax
	orl	24(%rsp), %ebp
	setne	%al
.LBB0_5:
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB0_1:
	movq	(%r14), %rdx
	testq	%rdx, %rdx
	je	.LBB0_3
# BB#2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB0_3:                                # %l_message.exit
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	movl	$.L.str, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %eax
	jmp	.LBB0_5
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	pmain,@function
pmain:                                  # @pmain
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 96
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, %esi
	callq	lua_touserdata
	movq	%rax, %rbx
	movq	8(%rbx), %r12
	movq	%r14, globalL(%rip)
	movq	(%r12), %rax
	testq	%rax, %rax
	je	.LBB1_3
# BB#1:
	cmpb	$0, (%rax)
	je	.LBB1_3
# BB#2:
	movq	%rax, progname(%rip)
.LBB1_3:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	lua_gc
	movq	%r14, %rdi
	callq	luaL_openlibs
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	lua_gc
	movl	$.L.str.4, %edi
	callq	getenv
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB1_6
# BB#4:
	cmpb	$64, (%rbp)
	jne	.LBB1_7
# BB#5:
	incq	%rbp
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	luaL_loadfile
	jmp	.LBB1_8
.LBB1_6:                                # %handle_luainit.exit.thread
	movq	%rbx, %r15
	addq	$16, %r15
	movl	$0, 16(%rbx)
	jmp	.LBB1_12
.LBB1_7:
	movq	%rbp, %rdi
	callq	strlen
	movl	$.L.str.5, %ecx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	luaL_loadbuffer
.LBB1_8:
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB1_10
# BB#9:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	docall
	testl	%eax, %eax
	setne	%cl
.LBB1_10:                               # %handle_luainit.exit
	movzbl	%cl, %esi
	movq	%r14, %rdi
	callq	report
	movl	%eax, 16(%rbx)
	testl	%eax, %eax
	jne	.LBB1_79
# BB#11:
	movq	%rbx, %r15
	addq	$16, %r15
.LBB1_12:
	movq	8(%r12), %rax
	testq	%rax, %rax
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	movl	$0, 28(%rsp)            # 4-byte Folded Spill
	je	.LBB1_34
# BB#13:                                # %.lr.ph.i.preheader
	movl	$1, %ebp
	xorl	%edi, %edi
	xorl	%r13d, %r13d
	xorl	%esi, %esi
.LBB1_14:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$45, (%rax)
	jne	.LBB1_29
# BB#15:                                #   in Loop: Header=BB1_14 Depth=1
	movsbl	1(%rax), %ecx
	leal	-101(%rcx), %edx
	cmpl	$17, %edx
	ja	.LBB1_24
# BB#16:                                #   in Loop: Header=BB1_14 Depth=1
	jmpq	*.LJTI1_0(,%rdx,8)
.LBB1_17:                               #   in Loop: Header=BB1_14 Depth=1
	movl	$1, %esi
.LBB1_18:                               #   in Loop: Header=BB1_14 Depth=1
	cmpb	$0, 2(%rax)
	jne	.LBB1_23
# BB#19:                                #   in Loop: Header=BB1_14 Depth=1
	movslq	%ebp, %rax
	cmpq	$0, 8(%r12,%rax,8)
	je	.LBB1_30
# BB#20:                                #   in Loop: Header=BB1_14 Depth=1
	incl	%ebp
	jmp	.LBB1_23
.LBB1_21:                               #   in Loop: Header=BB1_14 Depth=1
	movl	$1, %edi
	cmpb	$0, 2(%rax)
	movl	$1, %r13d
	je	.LBB1_23
	jmp	.LBB1_30
.LBB1_22:                               #   in Loop: Header=BB1_14 Depth=1
	movl	$1, %r13d
	cmpb	$0, 2(%rax)
	jne	.LBB1_30
.LBB1_23:                               # %.thread118
                                        #   in Loop: Header=BB1_14 Depth=1
	movslq	%ebp, %rax
	incl	%ebp
	movq	8(%r12,%rax,8), %rax
	testq	%rax, %rax
	jne	.LBB1_14
	jmp	.LBB1_31
.LBB1_24:
	testl	%ecx, %ecx
	je	.LBB1_29
# BB#25:
	cmpl	$45, %ecx
	jne	.LBB1_30
# BB#26:
	cmpb	$0, 2(%rax)
	jne	.LBB1_30
# BB#27:
	movslq	%ebp, %rax
	cmpq	$0, 8(%r12,%rax,8)
	je	.LBB1_31
# BB#28:
	incl	%ebp
.LBB1_29:                               # %collectargs.exit
	testl	%ebp, %ebp
	jns	.LBB1_32
.LBB1_30:                               # %collectargs.exit.thread
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	jmp	.LBB1_78
.LBB1_31:
	xorl	%ebp, %ebp
.LBB1_32:                               # %collectargs.exit.thread62
	movl	%edi, 28(%rsp)          # 4-byte Spill
	movl	%esi, 24(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	testl	%r13d, %r13d
	jne	.LBB1_80
.LBB1_33:
	movl	%eax, 20(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	movq	%rbp, (%rsp)            # 8-byte Spill
	movl	%ebp, %r13d
	jg	.LBB1_35
.LBB1_34:                               # %.thread73
	movl	(%rbx), %r13d
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB1_35:
	cmpl	$2, %r13d
	jl	.LBB1_52
# BB#36:                                # %.lr.ph.i40.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_37:                               # %.lr.ph.i40
                                        # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_51
# BB#38:                                #   in Loop: Header=BB1_37 Depth=1
	movsbl	1(%rbp), %ecx
	cmpl	$108, %ecx
	je	.LBB1_42
# BB#39:                                #   in Loop: Header=BB1_37 Depth=1
	cmpl	$101, %ecx
	jne	.LBB1_51
# BB#40:                                #   in Loop: Header=BB1_37 Depth=1
	cmpb	$0, 2(%rbp)
	je	.LBB1_44
# BB#41:                                #   in Loop: Header=BB1_37 Depth=1
	addq	$2, %rbp
	jmp	.LBB1_45
	.p2align	4, 0x90
.LBB1_42:                               #   in Loop: Header=BB1_37 Depth=1
	cmpb	$0, 2(%rbp)
	je	.LBB1_48
# BB#43:                                #   in Loop: Header=BB1_37 Depth=1
	addq	$2, %rbp
	jmp	.LBB1_49
.LBB1_44:                               #   in Loop: Header=BB1_37 Depth=1
	leal	1(%rax), %ebx
	movq	8(%r12,%rax,8), %rbp
.LBB1_45:                               #   in Loop: Header=BB1_37 Depth=1
	movq	%rbp, %rdi
	callq	strlen
	movl	$.L.str.11, %ecx
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	luaL_loadbuffer
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB1_47
# BB#46:                                #   in Loop: Header=BB1_37 Depth=1
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	docall
	testl	%eax, %eax
	setne	%cl
.LBB1_47:                               # %dostring.exit.i
                                        #   in Loop: Header=BB1_37 Depth=1
	movzbl	%cl, %esi
	movq	%r14, %rdi
	jmp	.LBB1_50
.LBB1_48:                               #   in Loop: Header=BB1_37 Depth=1
	leal	1(%rax), %ebx
	movq	8(%r12,%rax,8), %rbp
.LBB1_49:                               #   in Loop: Header=BB1_37 Depth=1
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.12, %edx
	movq	%r14, %rdi
	callq	lua_getfield
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	lua_pushstring
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	docall
	movq	%r14, %rdi
	movl	%eax, %esi
.LBB1_50:                               #   in Loop: Header=BB1_37 Depth=1
	callq	report
	testl	%eax, %eax
	jne	.LBB1_78
.LBB1_51:                               #   in Loop: Header=BB1_37 Depth=1
	incl	%ebx
	cmpl	%r13d, %ebx
	jl	.LBB1_37
.LBB1_52:                               # %.loopexit
	movl	$0, (%r15)
	movq	(%rsp), %rbx            # 8-byte Reload
	testl	%ebx, %ebx
	je	.LBB1_71
# BB#53:                                # %.preheader.preheader
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movl	%ebx, %r13d
	negl	%r13d
	movl	$-1, %ebx
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB1_54:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	incl	%r13d
	incl	%ebx
	cmpq	$0, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB1_54
# BB#55:
	movq	(%rsp), %rax            # 8-byte Reload
	leal	1(%rax), %ebp
	leal	-2(%r13), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	leal	1(%r13), %esi
	movl	$.L.str.16, %edx
	movq	%r14, %rdi
	callq	luaL_checkstack
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	cmpl	%ebp, %ebx
	jle	.LBB1_58
# BB#56:                                # %.lr.ph33.preheader.i.i
	movslq	16(%rsp), %rax          # 4-byte Folded Reload
	leaq	(%r12,%rax,8), %rbp
	movl	12(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB1_57:                               # %.lr.ph33.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rsi
	movq	%r14, %rdi
	callq	lua_pushstring
	addq	$8, %rbp
	decl	%r15d
	jne	.LBB1_57
.LBB1_58:                               # %._crit_edge34.i.i
	movq	%r14, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	16(%rsp), %edx          # 4-byte Reload
	callq	lua_createtable
	testl	%ebx, %ebx
	jle	.LBB1_61
# BB#59:                                # %.lr.ph.preheader.i.i
	movl	%ebx, %ebx
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %ebp
	negl	%ebp
	movq	%r12, %r15
	.p2align	4, 0x90
.LBB1_60:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rsi
	movq	%r14, %rdi
	callq	lua_pushstring
	movl	$-2, %esi
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	lua_rawseti
	incl	%ebp
	addq	$8, %r15
	decq	%rbx
	jne	.LBB1_60
.LBB1_61:                               # %getargs.exit.i
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.13, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	(%rsp), %rax            # 4-byte Folded Reload
	movq	(%r12,%rax,8), %rsi
	cmpb	$45, (%rsi)
	jne	.LBB1_67
# BB#62:
	cmpb	$0, 1(%rsi)
	jne	.LBB1_67
# BB#63:
	movq	-8(%r12,%rax,8), %rax
	movzbl	(%rax), %edi
	movl	$45, %edx
	movl	$45, %ecx
	subl	%edi, %ecx
	jne	.LBB1_66
# BB#64:
	movzbl	1(%rax), %ecx
	subl	%ecx, %edx
	movl	%edx, %ecx
	jne	.LBB1_66
# BB#65:
	movzbl	2(%rax), %ecx
	negl	%ecx
.LBB1_66:
	xorl	%eax, %eax
	testl	%ecx, %ecx
	cmovneq	%rax, %rsi
.LBB1_67:                               # %.thread.i
	movq	%r14, %rdi
	callq	luaL_loadfile
	movl	%eax, %r15d
	movl	$1, %ebp
	subl	%r13d, %ebp
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	lua_insert
	testl	%r15d, %r15d
	je	.LBB1_69
# BB#68:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	lua_settop
	jmp	.LBB1_70
.LBB1_78:                               # %runargs.exit
	movl	$1, (%r15)
	jmp	.LBB1_79
.LBB1_69:
	xorl	%edx, %edx
	movq	%r14, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	callq	docall
	movl	%eax, %r15d
.LBB1_70:
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	report
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	testl	%eax, %eax
	jne	.LBB1_79
.LBB1_71:                               # %.thread
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB1_73
.LBB1_72:
	movq	%r14, %rdi
	callq	dotty
	jmp	.LBB1_79
.LBB1_73:
	movl	24(%rsp), %eax          # 4-byte Reload
	orl	%ebx, %eax
	orl	20(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB1_79
# BB#74:
	xorl	%edi, %edi
	callq	isatty
	testl	%eax, %eax
	jne	.LBB1_81
# BB#75:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	luaL_loadfile
	movb	$1, %cl
	testl	%eax, %eax
	jne	.LBB1_77
# BB#76:
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	docall
	testl	%eax, %eax
	setne	%cl
.LBB1_77:                               # %dofile.exit
	movzbl	%cl, %esi
	movq	%r14, %rdi
	callq	report
.LBB1_79:
	xorl	%eax, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_80:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	%r13d, %eax
	jmp	.LBB1_33
.LBB1_81:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	jmp	.LBB1_72
.Lfunc_end1:
	.size	pmain, .Lfunc_end1-pmain
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_17
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_21
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_18
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_30
	.quad	.LBB1_22

	.text
	.p2align	4, 0x90
	.type	report,@function
report:                                 # @report
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	testl	%ebx, %ebx
	je	.LBB2_5
# BB#1:
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_type
	testl	%eax, %eax
	jne	.LBB2_2
.LBB2_5:
	movl	%ebx, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB2_2:
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	lua_tolstring
	testq	%rax, %rax
	movl	$.L.str.27, %r15d
	cmovneq	%rax, %r15
	movq	progname(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB2_4
# BB#3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB2_4:                                # %l_message.exit
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$-2, %esi
	movq	%r14, %rdi
	callq	lua_settop
	jmp	.LBB2_5
.Lfunc_end2:
	.size	report, .Lfunc_end2-report
	.cfi_endproc

	.p2align	4, 0x90
	.type	dotty,@function
dotty:                                  # @dotty
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -48
.Lcfi33:
	.cfi_offset %r12, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	progname(%rip), %r12
	movq	$0, progname(%rip)
	xorl	%esi, %esi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	pushline
	testl	%eax, %eax
	je	.LBB3_17
# BB#1:                                 # %.preheader.i.lr.ph
	leaq	8(%rsp), %r14
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$.L.str.19, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	lua_concat
.LBB3_2:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rax, %rbp
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_objlen
	movl	$.L.str.20, %ecx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rdx
	callq	luaL_loadbuffer
	movl	%eax, %ebp
	cmpl	$3, %ebp
	jne	.LBB3_7
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$-1, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_tolstring
	movq	8(%rsp), %rcx
	leaq	-7(%rax,%rcx), %rbp
	movl	$.L.str.26, %esi
	movq	%rax, %rdi
	callq	strstr
	cmpq	%rbp, %rax
	jne	.LBB3_4
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	pushline
	testl	%eax, %eax
	jne	.LBB3_6
	jmp	.LBB3_17
.LBB3_7:                                # %loadline.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_remove
	testl	%ebp, %ebp
	je	.LBB3_10
# BB#8:                                 # %loadline.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	$-1, %ebp
	jne	.LBB3_9
	jmp	.LBB3_17
.LBB3_10:                               #   in Loop: Header=BB3_2 Depth=1
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	docall
	movl	%eax, %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	report
	testl	%ebp, %ebp
	jne	.LBB3_16
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%rbx, %rdi
	callq	lua_gettop
	testl	%eax, %eax
	jle	.LBB3_16
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.17, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movq	%rbx, %rdi
	callq	lua_gettop
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	lua_pcall
	testl	%eax, %eax
	je	.LBB3_16
# BB#13:                                #   in Loop: Header=BB3_2 Depth=1
	movq	progname(%rip), %rbp
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rax, %rcx
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	lua_pushfstring
	movq	%rax, %r15
	testq	%rbp, %rbp
	je	.LBB3_15
# BB#14:                                #   in Loop: Header=BB3_2 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
.LBB3_15:                               # %l_message.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	fprintf
	movq	stderr(%rip), %rdi
	callq	fflush
	jmp	.LBB3_16
.LBB3_4:                                # %loadline.exit.thread17
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_remove
	movl	$3, %ebp
.LBB3_9:                                # %.thread
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	report
.LBB3_16:                               # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	pushline
	testl	%eax, %eax
	jne	.LBB3_2
.LBB3_17:                               # %loadline.exit.thread
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	%r12, progname(%rip)
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	dotty, .Lfunc_end3-dotty
	.cfi_endproc

	.p2align	4, 0x90
	.type	docall,@function
docall:                                 # @docall
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 48
.Lcfi42:
	.cfi_offset %rbx, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movl	%esi, %r14d
	movq	%rdi, %rbx
	callq	lua_gettop
	movl	%eax, %ebp
	subl	%r14d, %ebp
	movl	$traceback, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_insert
	movl	$2, %edi
	movl	$laction, %esi
	callq	signal
	cmpl	$1, %r15d
	sbbl	%edx, %edx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	movl	%ebp, %ecx
	callq	lua_pcall
	movl	%eax, %r14d
	movl	$2, %edi
	xorl	%esi, %esi
	callq	signal
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	lua_remove
	testl	%r14d, %r14d
	je	.LBB4_2
# BB#1:
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_gc
.LBB4_2:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	docall, .Lfunc_end4-docall
	.cfi_endproc

	.p2align	4, 0x90
	.type	traceback,@function
traceback:                              # @traceback
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 16
.Lcfi47:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_isstring
	testl	%eax, %eax
	je	.LBB5_7
# BB#1:
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.6, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$5, %eax
	jne	.LBB5_2
# BB#4:
	movl	$-1, %esi
	movl	$.L.str.7, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$6, %eax
	jne	.LBB5_5
# BB#6:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	jmp	.LBB5_7
.LBB5_2:
	movl	$-2, %esi
	jmp	.LBB5_3
.LBB5_5:
	movl	$-3, %esi
.LBB5_3:
	movq	%rbx, %rdi
	callq	lua_settop
.LBB5_7:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	traceback, .Lfunc_end5-traceback
	.cfi_endproc

	.p2align	4, 0x90
	.type	laction,@function
laction:                                # @laction
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 16
	xorl	%esi, %esi
	callq	signal
	movq	globalL(%rip), %rdi
	movl	$lstop, %esi
	movl	$11, %edx
	movl	$1, %ecx
	popq	%rax
	jmp	lua_sethook             # TAILCALL
.Lfunc_end6:
	.size	laction, .Lfunc_end6-laction
	.cfi_endproc

	.p2align	4, 0x90
	.type	lstop,@function
lstop:                                  # @lstop
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 16
.Lcfi50:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	lua_sethook
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	popq	%rbx
	jmp	luaL_error              # TAILCALL
.Lfunc_end7:
	.size	lstop, .Lfunc_end7-lstop
	.cfi_endproc

	.p2align	4, 0x90
	.type	pushline,@function
pushline:                               # @pushline
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 40
	subq	$520, %rsp              # imm = 0x208
.Lcfi55:
	.cfi_def_cfa_offset 560
.Lcfi56:
	.cfi_offset %rbx, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	testl	%r14d, %r14d
	movl	$.L.str.22, %eax
	movl	$.L.str.23, %edx
	cmovneq	%rax, %rdx
	movl	$.L.str.24, %eax
	movl	$.L.str.25, %ebp
	cmovneq	%rax, %rbp
	movl	$-10002, %esi           # imm = 0xD8EE
	callq	lua_getfield
	xorl	%r15d, %r15d
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	testq	%rax, %rax
	cmovneq	%rax, %rbp
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movq	stdout(%rip), %rsi
	movq	%rbp, %rdi
	callq	fputs
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stdin(%rip), %rdx
	movq	%rsp, %rdi
	movl	$512, %esi              # imm = 0x200
	callq	fgets
	testq	%rax, %rax
	je	.LBB8_9
# BB#1:
	movq	%rsp, %rdi
	callq	strlen
	testq	%rax, %rax
	je	.LBB8_4
# BB#2:
	cmpb	$10, -1(%rsp,%rax)
	jne	.LBB8_4
# BB#3:
	movb	$0, -1(%rsp,%rax)
.LBB8_4:
	testl	%r14d, %r14d
	je	.LBB8_7
# BB#5:
	cmpb	$61, (%rsp)
	jne	.LBB8_7
# BB#6:
	leaq	1(%rsp), %rdx
	movl	$.L.str.21, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	lua_pushfstring
	jmp	.LBB8_8
.LBB8_7:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
.LBB8_8:
	movl	$1, %r15d
.LBB8_9:
	movl	%r15d, %eax
	addq	$520, %rsp              # imm = 0x208
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	pushline, .Lfunc_end8-pushline
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"cannot create state: not enough memory"
	.size	.L.str, 39

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"lua"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: "
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s\n"
	.size	.L.str.3, 4

	.type	globalL,@object         # @globalL
	.local	globalL
	.comm	globalL,8,8
	.type	progname,@object        # @progname
	.data
	.p2align	3
progname:
	.quad	.L.str.1
	.size	progname, 8

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"LUA_INIT"
	.size	.L.str.4, 9

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"=LUA_INIT"
	.size	.L.str.5, 10

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"debug"
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"traceback"
	.size	.L.str.7, 10

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"interrupted!"
	.size	.L.str.8, 13

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"usage: %s [options] [script [args]].\nAvailable options are:\n  -e stat  execute string 'stat'\n  -l name  require library 'name'\n  -i       enter interactive mode after executing 'script'\n  -v       show version information\n  --       stop handling options\n  -        execute stdin and stop handling options\n"
	.size	.L.str.9, 307

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Lua 5.1.4  Copyright (C) 1994-2008 Lua.org, PUC-Rio"
	.size	.L.str.10, 52

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"=(command line)"
	.size	.L.str.11, 16

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"require"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"arg"
	.size	.L.str.13, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"too many arguments to script"
	.size	.L.str.16, 29

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"print"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"error calling 'print' (%s)"
	.size	.L.str.18, 27

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\n"
	.size	.L.str.19, 2

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"=stdin"
	.size	.L.str.20, 7

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"return %s"
	.size	.L.str.21, 10

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"_PROMPT"
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"_PROMPT2"
	.size	.L.str.23, 9

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"> "
	.size	.L.str.24, 3

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	">> "
	.size	.L.str.25, 4

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"'<eof>'"
	.size	.L.str.26, 8

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"(error object is not a string)"
	.size	.L.str.27, 31


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
