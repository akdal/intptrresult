	.text
	.file	"motion.bc"
	.globl	motion_vectors
	.p2align	4, 0x90
	.type	motion_vectors,@function
motion_vectors:                         # @motion_vectors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%ecx, %r12d
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	88(%rsp), %eax
	movl	80(%rsp), %r15d
	movl	72(%rsp), %ecx
	movl	64(%rsp), %edx
	cmpl	$1, %r8d
	jne	.LBB0_3
# BB#1:
	orl	%r15d, %r9d
	je	.LBB0_4
# BB#2:                                 # %._crit_edge
	movslq	%r12d, %rbp
	jmp	.LBB0_5
.LBB0_3:
	movl	$1, %edi
	movl	%edx, %r15d
	callq	Get_Bits
	movslq	%r12d, %rbp
	movl	%eax, (%r14,%rbp,4)
	leaq	(%rbx,%rbp,8), %rdi
	movl	$0, (%rsp)
	movq	%r13, %rsi
	movl	%r15d, %edx
	movl	72(%rsp), %ecx
	movl	80(%rsp), %r8d
	movl	88(%rsp), %r9d
	callq	motion_vector
	movl	$1, %edi
	callq	Get_Bits
	movl	%eax, 8(%r14,%rbp,4)
	leaq	16(%rbx,%rbp,8), %rdi
	movl	$0, (%rsp)
	movq	%r13, %rsi
	movl	%r15d, %edx
	movl	72(%rsp), %ecx
	movl	80(%rsp), %r8d
	movl	88(%rsp), %r9d
	callq	motion_vector
	jmp	.LBB0_6
.LBB0_4:
	movl	$1, %edi
	movl	%ecx, %ebp
	callq	Get_Bits
	movl	64(%rsp), %edx
	movl	%ebp, %ecx
	movslq	%r12d, %rbp
	movl	%eax, (%r14,%rbp,4)
	movl	%eax, 8(%r14,%rbp,4)
	movl	88(%rsp), %eax
.LBB0_5:
	leaq	(%rbx,%rbp,8), %rdi
	movl	$0, (%rsp)
	movq	%r13, %rsi
	movl	%r15d, %r8d
	movl	%eax, %r9d
	callq	motion_vector
	movl	(%rbx,%rbp,8), %eax
	movl	%eax, 16(%rbx,%rbp,8)
	movl	4(%rbx,%rbp,8), %eax
	movl	%eax, 20(%rbx,%rbp,8)
.LBB0_6:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	motion_vectors, .Lfunc_end0-motion_vectors
	.cfi_endproc

	.globl	motion_vector
	.p2align	4, 0x90
	.type	motion_vector,@function
motion_vector:                          # @motion_vector
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, (%rsp)            # 4-byte Spill
	movl	%ecx, %r13d
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	64(%rsp), %r12d
	callq	Get_motion_code
	movl	%eax, %ebp
	xorl	%eax, %eax
	testl	%ebx, %ebx
	je	.LBB1_3
# BB#1:
	testl	%ebp, %ebp
	je	.LBB1_3
# BB#2:
	movl	%ebx, %edi
	callq	Get_Bits
.LBB1_3:
	movl	$16, %esi
	movl	%ebx, %ecx
	shll	%cl, %esi
	testl	%r12d, %r12d
	setne	%r12b
	movl	(%r15), %edx
	movl	%r12d, %ecx
	sarl	%cl, %edx
	testl	%ebp, %ebp
	jle	.LBB1_6
# BB#4:
	decl	%ebp
	movl	%ebx, %ecx
	shll	%cl, %ebp
	addl	%eax, %ebp
	leal	1(%rdx,%rbp), %edx
	cmpl	%esi, %edx
	jl	.LBB1_7
# BB#5:
	addl	%esi, %esi
	subl	%esi, %edx
	jmp	.LBB1_7
.LBB1_6:
	js	.LBB1_8
.LBB1_7:
	movq	%r14, %rbx
	movl	(%rsp), %ebp            # 4-byte Reload
.LBB1_10:                               # %decode_motion_vector.exit
	movl	%r12d, %ecx
	shll	%cl, %edx
	movl	%edx, (%r15)
	testl	%ebp, %ebp
	je	.LBB1_12
# BB#11:
	callq	Get_dmvector
	movl	%eax, (%rbx)
.LBB1_12:
	callq	Get_motion_code
	movl	%eax, %r14d
	xorl	%eax, %eax
	testl	%r13d, %r13d
	je	.LBB1_15
# BB#13:
	testl	%r14d, %r14d
	movl	4(%rsp), %edi           # 4-byte Reload
	je	.LBB1_16
# BB#14:
	movl	%r13d, %edi
	callq	Get_Bits
.LBB1_15:
	movl	4(%rsp), %edi           # 4-byte Reload
.LBB1_16:
	movl	4(%r15), %edx
	testl	%edi, %edi
	je	.LBB1_18
# BB#17:
	sarl	%edx
	movl	%edx, 4(%r15)
.LBB1_18:                               # %._crit_edge
	movl	$16, %esi
	movl	%r13d, %ecx
	shll	%cl, %esi
	movl	%r12d, %ecx
	sarl	%cl, %edx
	testl	%r14d, %r14d
	jle	.LBB1_21
# BB#19:
	decl	%r14d
	movl	%r13d, %ecx
	shll	%cl, %r14d
	addl	%eax, %r14d
	leal	1(%rdx,%r14), %edx
	cmpl	%esi, %edx
	jl	.LBB1_24
# BB#20:
	addl	%esi, %esi
	subl	%esi, %edx
	jmp	.LBB1_24
.LBB1_21:
	jns	.LBB1_24
# BB#22:
	notl	%r14d
	movl	%r13d, %ecx
	shll	%cl, %r14d
	notl	%eax
	subl	%r14d, %eax
	addl	%eax, %edx
	movl	%esi, %eax
	negl	%eax
	cmpl	%eax, %edx
	jge	.LBB1_24
# BB#23:
	leal	(%rdx,%rsi,2), %edx
.LBB1_24:                               # %decode_motion_vector.exit32
	testl	%edi, %edi
	setne	%al
	movl	%r12d, %ecx
	shll	%cl, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movl	%edx, 4(%r15)
	testl	%ebp, %ebp
	je	.LBB1_26
# BB#25:
	callq	Get_dmvector
	movl	%eax, 4(%rbx)
.LBB1_26:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_8:
	notl	%ebp
	movl	%ebx, %ecx
	shll	%cl, %ebp
	notl	%eax
	subl	%ebp, %eax
	addl	%eax, %edx
	movl	%esi, %eax
	negl	%eax
	cmpl	%eax, %edx
	movq	%r14, %rbx
	movl	(%rsp), %ebp            # 4-byte Reload
	jge	.LBB1_10
# BB#9:
	leal	(%rdx,%rsi,2), %edx
	jmp	.LBB1_10
.Lfunc_end1:
	.size	motion_vector, .Lfunc_end1-motion_vector
	.cfi_endproc

	.globl	Dual_Prime_Arithmetic
	.p2align	4, 0x90
	.type	Dual_Prime_Arithmetic,@function
Dual_Prime_Arithmetic:                  # @Dual_Prime_Arithmetic
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	$3, picture_structure(%rip)
	jne	.LBB2_5
# BB#1:
	cmpl	$0, top_field_first(%rip)
	je	.LBB2_3
# BB#2:
	xorl	%r9d, %r9d
	testl	%edx, %edx
	setg	%r9b
	leal	(%r9,%rdx), %eax
	sarl	%eax
	addl	(%rsi), %eax
	movl	%eax, (%rdi)
	xorl	%r10d, %r10d
	testl	%ecx, %ecx
	setg	%r10b
	leal	(%r10,%rcx), %r11d
	sarl	%r11d
	leaq	4(%rsi), %r8
	movl	4(%rsi), %eax
	leal	-1(%r11,%rax), %eax
	movl	%eax, 4(%rdi)
	leal	(%rdx,%rdx,2), %r11d
	addl	%r9d, %r11d
	sarl	%r11d
	addl	(%rsi), %r11d
	leal	(%rcx,%rcx,2), %r9d
	addl	%r10d, %r9d
	jmp	.LBB2_4
.LBB2_5:
	xorl	%eax, %eax
	testl	%edx, %edx
	setg	%al
	addl	%edx, %eax
	sarl	%eax
	addl	(%rsi), %eax
	movl	%eax, (%rdi)
	xorl	%eax, %eax
	testl	%ecx, %ecx
	setg	%al
	addl	%ecx, %eax
	sarl	%eax
	addl	4(%rsi), %eax
	cmpl	$1, picture_structure(%rip)
	movl	$-1, %ecx
	movl	$1, %edx
	cmovel	%ecx, %edx
	addl	%eax, %edx
	movl	%edx, 4(%rdi)
	retq
.LBB2_3:
	leal	(%rdx,%rdx,2), %r8d
	xorl	%r11d, %r11d
	testl	%edx, %edx
	setg	%r11b
	addl	%r11d, %r8d
	sarl	%r8d
	addl	(%rsi), %r8d
	movl	%r8d, (%rdi)
	leal	(%rcx,%rcx,2), %r10d
	xorl	%r9d, %r9d
	testl	%ecx, %ecx
	setg	%r9b
	addl	%r9d, %r10d
	sarl	%r10d
	leaq	4(%rsi), %r8
	movl	4(%rsi), %eax
	leal	-1(%r10,%rax), %eax
	movl	%eax, 4(%rdi)
	addl	%edx, %r11d
	sarl	%r11d
	addl	(%rsi), %r11d
	addl	%ecx, %r9d
.LBB2_4:
	movl	%r11d, 8(%rdi)
	sarl	%r9d
	movl	(%r8), %eax
	leal	1(%r9,%rax), %eax
	movl	%eax, 12(%rdi)
	retq
.Lfunc_end2:
	.size	Dual_Prime_Arithmetic, .Lfunc_end2-Dual_Prime_Arithmetic
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
