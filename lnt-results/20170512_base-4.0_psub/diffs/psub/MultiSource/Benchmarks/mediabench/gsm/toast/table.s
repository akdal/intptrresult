	.text
	.file	"table.bc"
	.type	gsm_A,@object           # @gsm_A
	.data
	.globl	gsm_A
	.p2align	4
gsm_A:
	.short	20480                   # 0x5000
	.short	20480                   # 0x5000
	.short	20480                   # 0x5000
	.short	20480                   # 0x5000
	.short	13964                   # 0x368c
	.short	15360                   # 0x3c00
	.short	8534                    # 0x2156
	.short	9036                    # 0x234c
	.size	gsm_A, 16

	.type	gsm_B,@object           # @gsm_B
	.globl	gsm_B
	.p2align	4
gsm_B:
	.short	0                       # 0x0
	.short	0                       # 0x0
	.short	2048                    # 0x800
	.short	62976                   # 0xf600
	.short	94                      # 0x5e
	.short	63744                   # 0xf900
	.short	65195                   # 0xfeab
	.short	64392                   # 0xfb88
	.size	gsm_B, 16

	.type	gsm_MIC,@object         # @gsm_MIC
	.globl	gsm_MIC
	.p2align	4
gsm_MIC:
	.short	65504                   # 0xffe0
	.short	65504                   # 0xffe0
	.short	65520                   # 0xfff0
	.short	65520                   # 0xfff0
	.short	65528                   # 0xfff8
	.short	65528                   # 0xfff8
	.short	65532                   # 0xfffc
	.short	65532                   # 0xfffc
	.size	gsm_MIC, 16

	.type	gsm_MAC,@object         # @gsm_MAC
	.globl	gsm_MAC
	.p2align	4
gsm_MAC:
	.short	31                      # 0x1f
	.short	31                      # 0x1f
	.short	15                      # 0xf
	.short	15                      # 0xf
	.short	7                       # 0x7
	.short	7                       # 0x7
	.short	3                       # 0x3
	.short	3                       # 0x3
	.size	gsm_MAC, 16

	.type	gsm_INVA,@object        # @gsm_INVA
	.globl	gsm_INVA
	.p2align	4
gsm_INVA:
	.short	13107                   # 0x3333
	.short	13107                   # 0x3333
	.short	13107                   # 0x3333
	.short	13107                   # 0x3333
	.short	19223                   # 0x4b17
	.short	17476                   # 0x4444
	.short	31454                   # 0x7ade
	.short	29708                   # 0x740c
	.size	gsm_INVA, 16

	.type	gsm_DLB,@object         # @gsm_DLB
	.globl	gsm_DLB
	.p2align	1
gsm_DLB:
	.short	6554                    # 0x199a
	.short	16384                   # 0x4000
	.short	26214                   # 0x6666
	.short	32767                   # 0x7fff
	.size	gsm_DLB, 8

	.type	gsm_QLB,@object         # @gsm_QLB
	.globl	gsm_QLB
	.p2align	1
gsm_QLB:
	.short	3277                    # 0xccd
	.short	11469                   # 0x2ccd
	.short	21299                   # 0x5333
	.short	32767                   # 0x7fff
	.size	gsm_QLB, 8

	.type	gsm_H,@object           # @gsm_H
	.globl	gsm_H
	.p2align	4
gsm_H:
	.short	65402                   # 0xff7a
	.short	65162                   # 0xfe8a
	.short	0                       # 0x0
	.short	2054                    # 0x806
	.short	5741                    # 0x166d
	.short	8192                    # 0x2000
	.short	5741                    # 0x166d
	.short	2054                    # 0x806
	.short	0                       # 0x0
	.short	65162                   # 0xfe8a
	.short	65402                   # 0xff7a
	.size	gsm_H, 22

	.type	gsm_NRFAC,@object       # @gsm_NRFAC
	.globl	gsm_NRFAC
	.p2align	4
gsm_NRFAC:
	.short	29128                   # 0x71c8
	.short	26215                   # 0x6667
	.short	23832                   # 0x5d18
	.short	21846                   # 0x5556
	.short	20165                   # 0x4ec5
	.short	18725                   # 0x4925
	.short	17476                   # 0x4444
	.short	16384                   # 0x4000
	.size	gsm_NRFAC, 16

	.type	gsm_FAC,@object         # @gsm_FAC
	.globl	gsm_FAC
	.p2align	4
gsm_FAC:
	.short	18431                   # 0x47ff
	.short	20479                   # 0x4fff
	.short	22527                   # 0x57ff
	.short	24575                   # 0x5fff
	.short	26623                   # 0x67ff
	.short	28671                   # 0x6fff
	.short	30719                   # 0x77ff
	.short	32767                   # 0x7fff
	.size	gsm_FAC, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
