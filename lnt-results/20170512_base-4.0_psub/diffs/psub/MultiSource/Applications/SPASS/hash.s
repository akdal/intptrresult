	.text
	.file	"hash.bc"
	.globl	hash_Init
	.p2align	4, 0x90
	.type	hash_Init,@function
hash_Init:                              # @hash_Init
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, hash_TABLE+208(%rip)
	movaps	%xmm0, hash_TABLE+192(%rip)
	movaps	%xmm0, hash_TABLE+176(%rip)
	movaps	%xmm0, hash_TABLE+160(%rip)
	movaps	%xmm0, hash_TABLE+144(%rip)
	movaps	%xmm0, hash_TABLE+128(%rip)
	movaps	%xmm0, hash_TABLE+112(%rip)
	movaps	%xmm0, hash_TABLE+96(%rip)
	movaps	%xmm0, hash_TABLE+80(%rip)
	movaps	%xmm0, hash_TABLE+64(%rip)
	movaps	%xmm0, hash_TABLE+48(%rip)
	movaps	%xmm0, hash_TABLE+32(%rip)
	movaps	%xmm0, hash_TABLE+16(%rip)
	movaps	%xmm0, hash_TABLE(%rip)
	movq	$0, hash_TABLE+224(%rip)
	retq
.Lfunc_end0:
	.size	hash_Init, .Lfunc_end0-hash_Init
	.cfi_endproc

	.globl	hash_Reset
	.p2align	4, 0x90
	.type	hash_Reset,@function
hash_Reset:                             # @hash_Reset
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
                                        #     Child Loop BB1_4 Depth 2
	movq	hash_TABLE(,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB1_5
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rdx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rdx, (%rsi)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_2
# BB#3:                                 # %._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	hash_TABLE(,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB1_5
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.i
                                        #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB1_4
.LBB1_5:                                # %list_Delete.exit
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	$0, hash_TABLE(,%rax,8)
	incq	%rax
	cmpq	$29, %rax
	jne	.LBB1_1
# BB#6:
	retq
.Lfunc_end1:
	.size	hash_Reset, .Lfunc_end1-hash_Reset
	.cfi_endproc

	.globl	hash_ResetWithValue
	.p2align	4, 0x90
	.type	hash_ResetWithValue,@function
hash_ResetWithValue:                    # @hash_ResetWithValue
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB2_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_2 Depth 2
                                        #     Child Loop BB2_4 Depth 2
	movq	hash_TABLE(,%r14,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_5
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rax
	movq	(%rax), %rdi
	callq	*%r15
	movq	8(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB2_2
# BB#3:                                 # %._crit_edge
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	hash_TABLE(,%r14,8), %rax
	testq	%rax, %rax
	je	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph.i
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB2_4
.LBB2_5:                                # %list_Delete.exit
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	$0, hash_TABLE(,%r14,8)
	incq	%r14
	cmpq	$29, %r14
	jne	.LBB2_1
# BB#6:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	hash_ResetWithValue, .Lfunc_end2-hash_ResetWithValue
	.cfi_endproc

	.globl	hash_Get
	.p2align	4, 0x90
	.type	hash_Get,@function
hash_Get:                               # @hash_Get
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movl	$2369637129, %ecx       # imm = 0x8D3DCB09
	imulq	%rax, %rcx
	shrq	$36, %rcx
	imull	$29, %ecx, %eax
	movl	%edi, %ecx
	subl	%eax, %ecx
	movq	hash_TABLE(,%rcx,8), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jne	.LBB3_2
	jmp	.LBB3_4
	.p2align	4, 0x90
.LBB3_5:                                #   in Loop: Header=BB3_2 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB3_4
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rcx), %rdx
	cmpq	%rdi, 8(%rdx)
	jne	.LBB3_5
# BB#3:
	movq	(%rdx), %rax
.LBB3_4:                                # %.loopexit
	retq
.Lfunc_end3:
	.size	hash_Get, .Lfunc_end3-hash_Get
	.cfi_endproc

	.type	hash_TABLE,@object      # @hash_TABLE
	.comm	hash_TABLE,232,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
