	.text
	.file	"btGhostObject.bc"
	.globl	_ZN13btGhostObjectC2Ev
	.p2align	4, 0x90
	.type	_ZN13btGhostObjectC2Ev,@function
_ZN13btGhostObjectC2Ev:                 # @_ZN13btGhostObjectC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	_ZN17btCollisionObjectC2Ev
	movq	$_ZTV13btGhostObject+16, (%rbx)
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movl	$0, 284(%rbx)
	movl	$0, 288(%rbx)
	movl	$3, 256(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN13btGhostObjectC2Ev, .Lfunc_end0-_ZN13btGhostObjectC2Ev
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN13btGhostObjectD2Ev
	.p2align	4, 0x90
	.type	_ZN13btGhostObjectD2Ev,@function
_ZN13btGhostObjectD2Ev:                 # @_ZN13btGhostObjectD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13btGhostObject+16, (%rbx)
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#1:
	cmpb	$0, 304(%rbx)
	je	.LBB2_3
# BB#2:
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB2_3:                                # %.noexc
	movq	$0, 296(%rbx)
.LBB2_4:
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17btCollisionObjectD2Ev # TAILCALL
.LBB2_5:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp4:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_7:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN13btGhostObjectD2Ev, .Lfunc_end2-_ZN13btGhostObjectD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN13btGhostObjectD0Ev
	.p2align	4, 0x90
	.type	_ZN13btGhostObjectD0Ev,@function
_ZN13btGhostObjectD0Ev:                 # @_ZN13btGhostObjectD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13btGhostObject+16, (%rbx)
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#1:
	cmpb	$0, 304(%rbx)
	je	.LBB3_3
# BB#2:
.Ltmp6:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp7:
.LBB3_3:                                # %.noexc.i
	movq	$0, 296(%rbx)
.LBB3_4:
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
.Ltmp12:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp13:
# BB#5:                                 # %_ZN13btGhostObjectD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB3_6:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp10:
	jmp	.LBB3_9
.LBB3_7:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB3_8:
.Ltmp14:
	movq	%rax, %r14
.LBB3_9:                                # %.body
.Ltmp15:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
# BB#10:                                # %_ZN17btCollisionObjectdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_11:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN13btGhostObjectD0Ev, .Lfunc_end3-_ZN13btGhostObjectD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp13          #   Call between .Ltmp13 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Lfunc_end3-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_,@function
_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_: # @_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	(%rsi), %r15
	movslq	284(%r14), %rax
	testq	%rax, %rax
	jle	.LBB4_5
# BB#1:                                 # %.lr.ph.i
	movl	%eax, %ecx
	movq	296(%r14), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r15, (%rsi,%rdx,8)
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB4_2
	jmp	.LBB4_5
.LBB4_4:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit
	cmpl	%edx, %ecx
	jne	.LBB4_21
.LBB4_5:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit.thread
	cmpl	288(%r14), %eax
	jne	.LBB4_20
# BB#6:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB4_20
# BB#7:
	testl	%ebp, %ebp
	je	.LBB4_8
# BB#9:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	284(%r14), %eax
	testl	%eax, %eax
	jg	.LBB4_11
	jmp	.LBB4_15
.LBB4_8:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB4_15
.LBB4_11:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB4_13
	.p2align	4, 0x90
.LBB4_12:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r14), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB4_12
.LBB4_13:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB4_15
	.p2align	4, 0x90
.LBB4_14:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r14), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	296(%r14), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	296(%r14), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	296(%r14), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB4_14
.LBB4_15:                               # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_.exit.i.i
	movq	296(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB4_19
# BB#16:
	cmpb	$0, 304(%r14)
	je	.LBB4_18
# BB#17:
	callq	_Z21btAlignedFreeInternalPv
	movl	284(%r14), %eax
.LBB4_18:
	movq	$0, 296(%r14)
.LBB4_19:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv.exit.i.i
	movb	$1, 304(%r14)
	movq	%rbx, 296(%r14)
	movl	%ebp, 288(%r14)
.LBB4_20:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_.exit
	movq	296(%r14), %rcx
	cltq
	movq	%r15, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 284(%r14)
.LBB4_21:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_, .Lfunc_end4-_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.cfi_endproc

	.globl	_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.p2align	4, 0x90
	.type	_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_,@function
_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_: # @_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.cfi_startproc
# BB#0:
	movslq	284(%rdi), %rax
	testq	%rax, %rax
	jle	.LBB5_6
# BB#1:                                 # %.lr.ph.i
	movq	(%rsi), %rdx
	movq	296(%rdi), %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, (%rsi,%rcx,8)
	je	.LBB5_4
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB5_2
	jmp	.LBB5_6
.LBB5_4:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit
	cmpl	%eax, %ecx
	jge	.LBB5_6
# BB#5:
	movq	296(%rdi), %rdx
	movq	-8(%rdx,%rax,8), %rsi
	decq	%rax
	movq	%rsi, (%rdx,%rcx,8)
	movl	%eax, 284(%rdi)
.LBB5_6:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit.thread
	retq
.Lfunc_end5:
	.size	_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_, .Lfunc_end5-_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.cfi_endproc

	.globl	_ZN24btPairCachingGhostObjectC2Ev
	.p2align	4, 0x90
	.type	_ZN24btPairCachingGhostObjectC2Ev,@function
_ZN24btPairCachingGhostObjectC2Ev:      # @_ZN24btPairCachingGhostObjectC2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	_ZN17btCollisionObjectC2Ev
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movl	$0, 284(%rbx)
	movl	$0, 288(%rbx)
	movl	$3, 256(%rbx)
	movq	$_ZTV24btPairCachingGhostObject+16, (%rbx)
.Ltmp18:
	movl	$128, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
.Ltmp19:
# BB#1:
.Ltmp20:
	movq	%r14, %rdi
	callq	_ZN28btHashedOverlappingPairCacheC1Ev
.Ltmp21:
# BB#2:
	movq	%r14, 312(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB6_3:
.Ltmp22:
	movq	%rax, %r14
	movq	$_ZTV13btGhostObject+16, (%rbx)
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_7
# BB#4:
	cmpb	$0, 304(%rbx)
	je	.LBB6_6
# BB#5:
.Ltmp23:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
.LBB6_6:                                # %.noexc.i
	movq	$0, 296(%rbx)
.LBB6_7:
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
.Ltmp29:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp30:
# BB#8:                                 # %_ZN13btGhostObjectD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_9:
.Ltmp25:
	movq	%rax, %r14
.Ltmp26:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp27:
	jmp	.LBB6_12
.LBB6_10:
.Ltmp28:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_11:
.Ltmp31:
	movq	%rax, %r14
.LBB6_12:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN24btPairCachingGhostObjectC2Ev, .Lfunc_end6-_ZN24btPairCachingGhostObjectC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp18         #   Call between .Ltmp18 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin2   #     jumps to .Ltmp31
	.byte	1                       #   On action: 1
	.long	.Ltmp30-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp26-.Ltmp30         #   Call between .Ltmp30 and .Ltmp26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin2   #     jumps to .Ltmp28
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btPairCachingGhostObjectD2Ev
	.p2align	4, 0x90
	.type	_ZN24btPairCachingGhostObjectD2Ev,@function
_ZN24btPairCachingGhostObjectD2Ev:      # @_ZN24btPairCachingGhostObjectD2Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV24btPairCachingGhostObject+16, (%rbx)
	movq	312(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp32:
	callq	*(%rax)
.Ltmp33:
# BB#1:
	movq	312(%rbx), %rdi
.Ltmp34:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp35:
# BB#2:
	movq	$_ZTV13btGhostObject+16, (%rbx)
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_6
# BB#3:
	cmpb	$0, 304(%rbx)
	je	.LBB7_5
# BB#4:
.Ltmp46:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp47:
.LBB7_5:                                # %.noexc.i
	movq	$0, 296(%rbx)
.LBB7_6:                                # %_ZN13btGhostObjectD2Ev.exit
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17btCollisionObjectD2Ev # TAILCALL
.LBB7_7:
.Ltmp48:
	movq	%rax, %r14
.Ltmp49:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp50:
	jmp	.LBB7_8
.LBB7_9:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_10:
.Ltmp36:
	movq	%rax, %r14
	movq	$_ZTV13btGhostObject+16, (%rbx)
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_14
# BB#11:
	cmpb	$0, 304(%rbx)
	je	.LBB7_13
# BB#12:
.Ltmp37:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp38:
.LBB7_13:                               # %.noexc.i3
	movq	$0, 296(%rbx)
.LBB7_14:
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
.Ltmp43:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp44:
.LBB7_8:                                # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB7_15:
.Ltmp39:
	movq	%rax, %r14
.Ltmp40:
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObjectD2Ev
.Ltmp41:
	jmp	.LBB7_18
.LBB7_16:
.Ltmp42:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_17:
.Ltmp45:
	movq	%rax, %r14
.LBB7_18:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN24btPairCachingGhostObjectD2Ev, .Lfunc_end7-_ZN24btPairCachingGhostObjectD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp32-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp35-.Ltmp32         #   Call between .Ltmp32 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin3   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin3   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp49-.Ltmp47         #   Call between .Ltmp47 and .Ltmp49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp50-.Ltmp49         #   Call between .Ltmp49 and .Ltmp50
	.long	.Ltmp51-.Lfunc_begin3   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp37-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin3   #     jumps to .Ltmp39
	.byte	1                       #   On action: 1
	.long	.Ltmp43-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp44-.Ltmp43         #   Call between .Ltmp43 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin3   #     jumps to .Ltmp45
	.byte	1                       #   On action: 1
	.long	.Ltmp44-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp40-.Ltmp44         #   Call between .Ltmp44 and .Ltmp40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp41-.Ltmp40         #   Call between .Ltmp40 and .Ltmp41
	.long	.Ltmp42-.Lfunc_begin3   #     jumps to .Ltmp42
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btPairCachingGhostObjectD0Ev
	.p2align	4, 0x90
	.type	_ZN24btPairCachingGhostObjectD0Ev,@function
_ZN24btPairCachingGhostObjectD0Ev:      # @_ZN24btPairCachingGhostObjectD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp52:
	callq	_ZN24btPairCachingGhostObjectD2Ev
.Ltmp53:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB8_2:
.Ltmp54:
	movq	%rax, %r14
.Ltmp55:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp56:
# BB#3:                                 # %_ZN17btCollisionObjectdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB8_4:
.Ltmp57:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end8:
	.size	_ZN24btPairCachingGhostObjectD0Ev, .Lfunc_end8-_ZN24btPairCachingGhostObjectD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp52-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp53-.Ltmp52         #   Call between .Ltmp52 and .Ltmp53
	.long	.Ltmp54-.Lfunc_begin4   #     jumps to .Ltmp54
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp55-.Ltmp53         #   Call between .Ltmp53 and .Ltmp55
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp56-.Ltmp55         #   Call between .Ltmp55 and .Ltmp56
	.long	.Ltmp57-.Lfunc_begin4   #     jumps to .Ltmp57
	.byte	1                       #   On action: 1
	.long	.Ltmp56-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end8-.Ltmp56     #   Call between .Ltmp56 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_,@function
_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_: # @_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	testq	%r15, %r15
	jne	.LBB9_2
# BB#1:
	movq	192(%r12), %r15
.LBB9_2:
	movq	(%r14), %r13
	movslq	284(%r12), %rax
	testq	%rax, %rax
	jle	.LBB9_7
# BB#3:                                 # %.lr.ph.i
	movl	%eax, %ecx
	movq	296(%r12), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_4:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r13, (%rsi,%rdx,8)
	je	.LBB9_6
# BB#5:                                 #   in Loop: Header=BB9_4 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB9_4
	jmp	.LBB9_7
.LBB9_6:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit
	cmpl	%edx, %ecx
	jne	.LBB9_23
.LBB9_7:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit.thread
	cmpl	288(%r12), %eax
	jne	.LBB9_22
# BB#8:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB9_22
# BB#9:
	testl	%ebp, %ebp
	je	.LBB9_10
# BB#11:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	284(%r12), %eax
	testl	%eax, %eax
	jg	.LBB9_13
	jmp	.LBB9_17
.LBB9_23:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_10:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB9_17
.LBB9_13:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB9_15
	.p2align	4, 0x90
.LBB9_14:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r12), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB9_14
.LBB9_15:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB9_17
	.p2align	4, 0x90
.LBB9_16:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r12), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	296(%r12), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	296(%r12), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	296(%r12), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB9_16
.LBB9_17:                               # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_.exit.i.i
	movq	296(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB9_21
# BB#18:
	cmpb	$0, 304(%r12)
	je	.LBB9_20
# BB#19:
	callq	_Z21btAlignedFreeInternalPv
	movl	284(%r12), %eax
.LBB9_20:
	movq	$0, 296(%r12)
.LBB9_21:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv.exit.i.i
	movb	$1, 304(%r12)
	movq	%rbx, 296(%r12)
	movl	%ebp, 288(%r12)
.LBB9_22:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_.exit
	movq	296(%r12), %rcx
	cltq
	movq	%r13, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 284(%r12)
	movq	312(%r12), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	%r15, %rsi
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end9:
	.size	_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_, .Lfunc_end9-_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.cfi_endproc

	.globl	_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.p2align	4, 0x90
	.type	_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_,@function
_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_: # @_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.cfi_startproc
# BB#0:
	movq	%rdx, %r8
	movq	%rsi, %r9
	movq	(%r9), %r10
	testq	%rcx, %rcx
	jne	.LBB10_2
# BB#1:
	movq	192(%rdi), %rcx
.LBB10_2:
	movslq	284(%rdi), %rsi
	testq	%rsi, %rsi
	jle	.LBB10_7
# BB#3:                                 # %.lr.ph.i
	movq	296(%rdi), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB10_4:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r10, (%rax,%rdx,8)
	je	.LBB10_6
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=1
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB10_4
	jmp	.LBB10_7
.LBB10_6:                               # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit
	cmpl	%esi, %edx
	jge	.LBB10_7
# BB#8:
	movq	296(%rdi), %rax
	movq	-8(%rax,%rsi,8), %r10
	decq	%rsi
	movq	%r10, (%rax,%rdx,8)
	movl	%esi, 284(%rdi)
	movq	312(%rdi), %rdi
	movq	(%rdi), %rax
	movq	24(%rax), %rax
	movq	%rcx, %rsi
	movq	%r9, %rdx
	movq	%r8, %rcx
	jmpq	*%rax                   # TAILCALL
.LBB10_7:                               # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit.thread
	retq
.Lfunc_end10:
	.size	_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_, .Lfunc_end10-_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1073741824              # float 2
.LCPI11_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_2:
	.zero	16
	.text
	.globl	_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf
	.p2align	4, 0x90
	.type	_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf,@function
_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf: # @_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$312, %rsp              # imm = 0x138
.Lcfi55:
	.cfi_def_cfa_offset 368
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movq	%r8, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movups	(%rdx), %xmm0
	movaps	%xmm0, 240(%rsp)
	movups	16(%rdx), %xmm0
	movaps	%xmm0, 256(%rsp)
	movups	32(%rdx), %xmm0
	movaps	%xmm0, 272(%rsp)
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movups	48(%rdx), %xmm0
	movaps	%xmm0, 288(%rsp)
	movups	(%rcx), %xmm0
	movaps	%xmm0, 176(%rsp)
	movups	16(%rcx), %xmm0
	movaps	%xmm0, 192(%rsp)
	movups	32(%rcx), %xmm0
	movaps	%xmm0, 208(%rsp)
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movups	48(%rcx), %xmm0
	movaps	%xmm0, 224(%rsp)
	movaps	224(%rsp), %xmm0
	subps	288(%rsp), %xmm0
	movss	232(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	296(%rsp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 8(%rsp)
	movlps	%xmm2, 16(%rsp)
	leaq	240(%rsp), %rbp
	leaq	176(%rsp), %rsi
	leaq	64(%rsp), %rdx
	leaq	24(%rsp), %rcx
	movq	%rbp, %rdi
	callq	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	64(%rsp), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	72(%rsp), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 144(%rsp)
	movlps	%xmm1, 152(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 112(%rsp)
	leaq	24(%rsp), %rsi
	movq	%rbp, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	24(%rsp), %xmm1         # xmm1 = mem[0],zero
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm1, %xmm3
	mulss	%xmm3, %xmm3
	pshufd	$229, %xmm1, %xmm10     # xmm10 = xmm1[1,1,2,3]
	movdqa	%xmm10, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm0, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm4, %xmm3
	pshufd	$229, %xmm0, %xmm5      # xmm5 = xmm0[1,1,2,3]
	movdqa	%xmm5, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	movss	.LCPI11_0(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	divss	%xmm4, %xmm6
	movaps	%xmm1, %xmm3
	mulss	%xmm6, %xmm3
	movdqa	%xmm10, %xmm4
	mulss	%xmm6, %xmm4
	mulss	%xmm0, %xmm6
	movdqa	%xmm5, %xmm8
	mulss	%xmm3, %xmm8
	movdqa	%xmm5, %xmm9
	mulss	%xmm4, %xmm9
	mulss	%xmm6, %xmm5
	mulss	%xmm1, %xmm3
	movaps	%xmm1, %xmm7
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm1
	mulss	%xmm10, %xmm4
	mulss	%xmm6, %xmm10
	mulss	%xmm0, %xmm6
	movaps	%xmm4, %xmm2
	addss	%xmm6, %xmm2
	movss	.LCPI11_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm11
	subss	%xmm2, %xmm11
	movaps	%xmm7, %xmm12
	subss	%xmm5, %xmm12
	movaps	%xmm1, %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm5, %xmm7
	addss	%xmm3, %xmm6
	movaps	%xmm0, %xmm5
	subss	%xmm6, %xmm5
	movaps	%xmm10, %xmm6
	subss	%xmm8, %xmm6
	subss	%xmm9, %xmm1
	addss	%xmm8, %xmm10
	addss	%xmm3, %xmm4
	movaps	%xmm0, %xmm3
	subss	%xmm4, %xmm3
	movss	%xmm11, 64(%rsp)
	movss	%xmm12, 68(%rsp)
	movss	%xmm2, 72(%rsp)
	movl	$0, 76(%rsp)
	movss	%xmm7, 80(%rsp)
	movss	%xmm5, 84(%rsp)
	movss	%xmm6, 88(%rsp)
	movl	$0, 92(%rsp)
	movss	%xmm1, 96(%rsp)
	movss	%xmm10, 100(%rsp)
	movss	%xmm3, 104(%rsp)
	movl	$0, 108(%rsp)
	leaq	64(%rsp), %rsi
	leaq	8(%rsp), %rdx
	leaq	144(%rsp), %rcx
	leaq	24(%rsp), %r8
	leaq	160(%rsp), %r9
	movq	%r14, 128(%rsp)         # 8-byte Spill
	movq	%r14, %rdi
	callq	_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_
	cmpl	$0, 284(%rbx)
	jle	.LBB11_7
# BB#1:                                 # %.lr.ph
	addq	$48, 48(%rsp)           # 8-byte Folded Spill
	addq	$48, 56(%rsp)           # 8-byte Folded Spill
	xorl	%ebp, %ebp
	movq	%rbx, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movq	296(%rbx), %rax
	movq	(%rax,%rbp,8), %r14
	movq	(%r15), %rax
	movq	192(%r14), %rsi
	movq	%r15, %rdi
	callq	*16(%rax)
	testb	%al, %al
	je	.LBB11_6
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	200(%r14), %rdi
	movq	(%rdi), %rax
	leaq	8(%r14), %rbx
	movq	%rbx, %rsi
	leaq	64(%rsp), %r12
	movq	%r12, %rdx
	leaq	8(%rsp), %r13
	movq	%r13, %rcx
	callq	*16(%rax)
	movsd	64(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rsp), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	72(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	32(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 64(%rsp)
	movlps	%xmm2, 72(%rsp)
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movsd	160(%rsp), %xmm1        # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	168(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 8(%rsp)
	movlps	%xmm2, 16(%rsp)
	movl	$1065353216, 44(%rsp)   # imm = 0x3F800000
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	movq	%r13, %rcx
	leaq	44(%rsp), %r8
	leaq	144(%rsp), %r9
	callq	_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_
	testb	%al, %al
	je	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	movq	200(%r14), %r8
	movq	%r15, (%rsp)
	movq	128(%rsp), %rdi         # 8-byte Reload
	leaq	240(%rsp), %rsi
	leaq	176(%rsp), %rdx
	movq	%r14, %rcx
	movq	%rbx, %r9
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf
.LBB11_5:                               #   in Loop: Header=BB11_2 Depth=1
	movq	136(%rsp), %rbx         # 8-byte Reload
.LBB11_6:                               #   in Loop: Header=BB11_2 Depth=1
	incq	%rbp
	movslq	284(%rbx), %rax
	cmpq	%rax, %rbp
	jl	.LBB11_2
.LBB11_7:                               # %._crit_edge
	addq	$312, %rsp              # imm = 0x138
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf, .Lfunc_end11-_ZNK13btGhostObject15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RN16btCollisionWorld20ConvexResultCallbackEf
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI12_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
.LCPI12_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_1:
	.long	1056964608              # float 0.5
.LCPI12_3:
	.long	1065353216              # float 1
.LCPI12_4:
	.long	3212836864              # float -1
	.section	.text._Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_,"axG",@progbits,_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_,comdat
	.weak	_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_
	.p2align	4, 0x90
	.type	_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_,@function
_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_: # @_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_
	.cfi_startproc
# BB#0:
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm7
	subps	%xmm1, %xmm7
	movss	8(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	subss	%xmm3, %xmm4
	movaps	.LCPI12_0(%rip), %xmm9  # xmm9 = <0.5,0.5,u,u>
	mulps	%xmm9, %xmm7
	movss	.LCPI12_1(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	movaps	%xmm7, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movlps	%xmm7, -32(%rsp)
	xorps	%xmm8, %xmm8
	xorps	%xmm5, %xmm5
	movss	%xmm4, %xmm5            # xmm5 = xmm4[0],xmm5[1,2,3]
	movlps	%xmm5, -24(%rsp)
	addps	%xmm1, %xmm0
	addss	%xmm3, %xmm2
	mulps	%xmm9, %xmm0
	mulss	%xmm10, %xmm2
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	subps	%xmm0, %xmm1
	movss	8(%rdi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm3
	movaps	%xmm1, %xmm13
	shufps	$229, %xmm13, %xmm13    # xmm13 = xmm13[1,1,2,3]
	xorps	%xmm5, %xmm5
	movss	%xmm3, %xmm5            # xmm5 = xmm3[0],xmm5[1,2,3]
	movlps	%xmm1, -16(%rsp)
	movlps	%xmm5, -8(%rsp)
	movss	(%rsi), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm10
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm12
	movss	8(%rsi), %xmm14         # xmm14 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm14
	movaps	.LCPI12_2(%rip), %xmm11 # xmm11 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm7, %xmm2
	xorps	%xmm11, %xmm2
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	xorl	%ecx, %ecx
	ucomiss	%xmm7, %xmm1
	seta	%cl
	leal	(%rax,%rcx,8), %eax
	movaps	%xmm6, %xmm5
	xorps	%xmm11, %xmm5
	xorl	%ecx, %ecx
	ucomiss	%xmm13, %xmm5
	seta	%cl
	leal	(%rax,%rcx,2), %eax
	xorl	%ecx, %ecx
	ucomiss	%xmm6, %xmm13
	seta	%cl
	shll	$4, %ecx
	orl	%eax, %ecx
	movaps	%xmm4, %xmm0
	xorps	%xmm11, %xmm0
	xorl	%eax, %eax
	ucomiss	%xmm3, %xmm0
	seta	%al
	leal	(%rcx,%rax,4), %ecx
	xorl	%eax, %eax
	ucomiss	%xmm4, %xmm3
	seta	%al
	shll	$5, %eax
	orl	%ecx, %eax
	xorl	%ecx, %ecx
	ucomiss	%xmm10, %xmm2
	seta	%cl
	xorl	%edx, %edx
	ucomiss	%xmm7, %xmm10
	seta	%dl
	leal	(%rcx,%rdx,8), %ecx
	xorl	%edx, %edx
	ucomiss	%xmm12, %xmm5
	seta	%dl
	leal	(%rcx,%rdx,2), %ecx
	xorl	%edx, %edx
	ucomiss	%xmm6, %xmm12
	seta	%dl
	shll	$4, %edx
	orl	%ecx, %edx
	xorl	%ecx, %ecx
	ucomiss	%xmm14, %xmm0
	seta	%cl
	leal	(%rdx,%rcx,4), %edx
	xorl	%ecx, %ecx
	ucomiss	%xmm4, %xmm14
	seta	%cl
	shll	$5, %ecx
	orl	%edx, %ecx
	testl	%ecx, %eax
	jne	.LBB12_12
# BB#1:
	movl	(%r8), %r11d
	movd	%r11d, %xmm9
	subss	%xmm1, %xmm10
	subss	%xmm13, %xmm12
	subss	%xmm3, %xmm14
	movaps	%xmm8, -56(%rsp)
	xorps	%xmm11, %xmm1
	movss	-32(%rsp), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	-28(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm11, %xmm13
	movss	-8(%rsp), %xmm6         # xmm6 = mem[0],zero,zero,zero
	xorps	%xmm11, %xmm6
	movss	-24(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm7, %xmm7
	movss	.LCPI12_3(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movl	$1, %esi
	movl	$2, %edi
	leaq	-52(%rsp), %r10
	movss	.LCPI12_4(%rip), %xmm11 # xmm11 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB12_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	testl	%eax, %esi
	je	.LBB12_5
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm4, %xmm2
	mulss	%xmm15, %xmm2
	movaps	%xmm1, %xmm5
	subss	%xmm2, %xmm5
	divss	%xmm10, %xmm5
	ucomiss	%xmm7, %xmm5
	jb	.LBB12_8
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	$0, 8(%r10)
	movq	$0, (%r10)
	movss	%xmm4, -56(%rsp)
	jmp	.LBB12_9
	.p2align	4, 0x90
.LBB12_5:                               #   in Loop: Header=BB12_2 Depth=1
	testl	%ecx, %esi
	je	.LBB12_8
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm4, %xmm5
	mulss	%xmm15, %xmm5
	movaps	%xmm1, %xmm2
	subss	%xmm5, %xmm2
	divss	%xmm10, %xmm2
	movd	%r11d, %xmm5
	ucomiss	%xmm2, %xmm5
	jbe	.LBB12_8
# BB#7:                                 #   in Loop: Header=BB12_2 Depth=1
	movd	%xmm2, %r11d
	movdqa	%xmm2, %xmm9
	.p2align	4, 0x90
.LBB12_8:                               # %_Z8btSetMinIfEvRT_RKS0_.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm7, %xmm5
.LBB12_9:                               # %_Z8btSetMinIfEvRT_RKS0_.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	leal	(%rsi,%rsi), %edx
	testl	%eax, %edx
	je	.LBB12_14
# BB#10:                                #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movaps	%xmm13, %xmm2
	subss	%xmm7, %xmm2
	divss	%xmm12, %xmm2
	ucomiss	%xmm5, %xmm2
	jb	.LBB12_17
# BB#11:                                #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm8, -56(%rsp)
	movss	%xmm4, -52(%rsp)
	jmp	.LBB12_18
	.p2align	4, 0x90
.LBB12_14:                              #   in Loop: Header=BB12_2 Depth=1
	testl	%ecx, %edx
	je	.LBB12_17
# BB#15:                                #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm7
	movaps	%xmm13, %xmm2
	subss	%xmm7, %xmm2
	divss	%xmm12, %xmm2
	movd	%r11d, %xmm7
	ucomiss	%xmm2, %xmm7
	jbe	.LBB12_17
# BB#16:                                #   in Loop: Header=BB12_2 Depth=1
	movd	%xmm2, %r11d
	movdqa	%xmm2, %xmm9
	.p2align	4, 0x90
.LBB12_17:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.1
                                        #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm5, %xmm2
.LBB12_18:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.1
                                        #   in Loop: Header=BB12_2 Depth=1
	leal	(,%rsi,4), %edx
	testl	%eax, %edx
	je	.LBB12_21
# BB#19:                                #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm4, %xmm5
	mulss	%xmm3, %xmm5
	movaps	%xmm6, %xmm7
	subss	%xmm5, %xmm7
	divss	%xmm14, %xmm7
	ucomiss	%xmm2, %xmm7
	jb	.LBB12_24
# BB#20:                                #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm8, -56(%rsp)
	movss	%xmm4, -48(%rsp)
	jmp	.LBB12_25
	.p2align	4, 0x90
.LBB12_21:                              #   in Loop: Header=BB12_2 Depth=1
	testl	%ecx, %edx
	je	.LBB12_24
# BB#22:                                #   in Loop: Header=BB12_2 Depth=1
	mulss	%xmm3, %xmm4
	movaps	%xmm6, %xmm5
	subss	%xmm4, %xmm5
	divss	%xmm14, %xmm5
	movd	%r11d, %xmm4
	ucomiss	%xmm5, %xmm4
	jbe	.LBB12_24
# BB#23:                                #   in Loop: Header=BB12_2 Depth=1
	movd	%xmm5, %r11d
	movdqa	%xmm5, %xmm9
	.p2align	4, 0x90
.LBB12_24:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.2
                                        #   in Loop: Header=BB12_2 Depth=1
	movaps	%xmm2, %xmm7
.LBB12_25:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.2
                                        #   in Loop: Header=BB12_2 Depth=1
	shll	$3, %esi
	decl	%edi
	movaps	%xmm11, %xmm4
	jne	.LBB12_2
# BB#26:
	ucomiss	%xmm7, %xmm9
	jae	.LBB12_27
.LBB12_12:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB12_27:                              # %.critedge
	movss	%xmm7, (%r8)
	movaps	-56(%rsp), %xmm0
	movups	%xmm0, (%r9)
	movb	$1, %al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end12:
	.size	_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_, .Lfunc_end12-_Z9btRayAabbRK9btVector3S1_S1_S1_RfRS_
	.cfi_endproc

	.text
	.globl	_ZNK13btGhostObject7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.p2align	4, 0x90
	.type	_ZNK13btGhostObject7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE,@function
_ZNK13btGhostObject7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE: # @_ZNK13btGhostObject7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 192
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdi, %r14
	movl	$1065353216, 72(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 76(%rsp)
	movl	$1065353216, 92(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 96(%rsp)
	movq	$1065353216, 112(%rsp)  # imm = 0x3F800000
	movups	(%rsi), %xmm1
	movups	%xmm1, 120(%rsp)
	movl	$1065353216, 8(%rsp)    # imm = 0x3F800000
	movups	%xmm0, 12(%rsp)
	movl	$1065353216, 28(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 32(%rsp)
	movq	$1065353216, 48(%rsp)   # imm = 0x3F800000
	movups	(%rdx), %xmm0
	movups	%xmm0, 56(%rsp)
	cmpl	$0, 284(%r14)
	jle	.LBB13_5
# BB#1:                                 # %.lr.ph
	xorl	%ebp, %ebp
	leaq	72(%rsp), %r15
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r14), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	(%r13), %rax
	movq	192(%rbx), %rsi
	movq	%r13, %rdi
	callq	*16(%rax)
	testb	%al, %al
	je	.LBB13_4
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	movq	200(%rbx), %rcx
	leaq	8(%rbx), %r8
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r13, %r9
	callq	_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE
.LBB13_4:                               #   in Loop: Header=BB13_2 Depth=1
	incq	%rbp
	movslq	284(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB13_2
.LBB13_5:                               # %._crit_edge
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_ZNK13btGhostObject7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE, .Lfunc_end13-_ZNK13btGhostObject7rayTestERK9btVector3S2_RN16btCollisionWorld17RayResultCallbackE
	.cfi_endproc

	.section	.text._ZN17btCollisionObject24checkCollideWithOverrideEPS_,"axG",@progbits,_ZN17btCollisionObject24checkCollideWithOverrideEPS_,comdat
	.weak	_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject24checkCollideWithOverrideEPS_,@function
_ZN17btCollisionObject24checkCollideWithOverrideEPS_: # @_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end14:
	.size	_ZN17btCollisionObject24checkCollideWithOverrideEPS_, .Lfunc_end14-_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.cfi_endproc

	.section	.text._ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,"axG",@progbits,_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,comdat
	.weak	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.p2align	4, 0x90
	.type	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape,@function
_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape: # @_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.cfi_startproc
# BB#0:
	movq	%rsi, 200(%rdi)
	movq	%rsi, 208(%rdi)
	retq
.Lfunc_end15:
	.size	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape, .Lfunc_end15-_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI16_0:
	.long	1065353216              # float 1
.LCPI16_1:
	.long	679477248               # float 1.42108547E-14
	.section	.text._ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,"axG",@progbits,_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,comdat
	.weak	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,@function
_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf: # @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Lcfi77:
	.cfi_def_cfa_offset 112
.Lcfi78:
	.cfi_offset %rbx, -24
.Lcfi79:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movss	20(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	40(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm4, %xmm0
	movss	24(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm14, %xmm1
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm5
	mulss	%xmm1, %xmm5
	movss	16(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm5
	movaps	%xmm14, %xmm15
	mulss	%xmm11, %xmm15
	movaps	%xmm8, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm15
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	4(%rdi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movss	8(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm14, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm12, %xmm10
	mulss	%xmm13, %xmm10
	movaps	%xmm8, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm1, %xmm9
	mulss	%xmm6, %xmm9
	mulss	%xmm11, %xmm6
	mulss	%xmm13, %xmm1
	mulss	%xmm13, %xmm11
	mulss	%xmm5, %xmm13
	addss	36(%rsp), %xmm13        # 4-byte Folded Reload
	addss	%xmm13, %xmm2
	movss	.LCPI16_0(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm13
	subss	%xmm3, %xmm7
	subss	%xmm0, %xmm10
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	mulss	%xmm13, %xmm7
	mulss	%xmm13, %xmm10
	mulss	%xmm13, %xmm5
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	subss	%xmm9, %xmm4
	mulss	%xmm13, %xmm4
	mulss	%xmm0, %xmm12
	subss	%xmm12, %xmm6
	mulss	%xmm13, %xmm6
	mulss	%xmm13, %xmm15
	mulss	%xmm0, %xmm14
	subss	%xmm14, %xmm1
	mulss	%xmm13, %xmm1
	mulss	%xmm0, %xmm8
	subss	%xmm11, %xmm8
	mulss	%xmm13, %xmm8
	movss	(%rsi), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm9
	mulss	%xmm3, %xmm9
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm9, %xmm2
	movss	8(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm12, %xmm11
	mulss	%xmm7, %xmm11
	movaps	%xmm13, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm11, %xmm2
	movaps	%xmm9, %xmm11
	mulss	%xmm1, %xmm11
	addss	%xmm2, %xmm11
	mulss	%xmm10, %xmm12
	mulss	%xmm6, %xmm13
	addss	%xmm12, %xmm13
	mulss	%xmm8, %xmm9
	addss	%xmm13, %xmm9
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm12
	mulss	%xmm2, %xmm12
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm13
	mulss	%xmm0, %xmm13
	addss	%xmm12, %xmm13
	movss	24(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm12
	mulss	%xmm14, %xmm12
	addss	%xmm13, %xmm12
	movaps	%xmm7, %xmm13
	mulss	%xmm2, %xmm13
	movaps	%xmm4, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm13, %xmm3
	movaps	%xmm1, %xmm13
	mulss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	mulss	%xmm10, %xmm2
	mulss	%xmm6, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm8, %xmm14
	addss	%xmm0, %xmm14
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm0, %xmm10
	mulss	%xmm2, %xmm6
	addss	%xmm10, %xmm6
	mulss	%xmm3, %xmm8
	addss	%xmm6, %xmm8
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rsp)
	movss	%xmm11, 44(%rsp)
	movss	%xmm9, 48(%rsp)
	movl	$0, 52(%rsp)
	movss	%xmm12, 56(%rsp)
	movss	%xmm13, 60(%rsp)
	movss	%xmm14, 64(%rsp)
	movl	$0, 68(%rsp)
	movss	%xmm15, 72(%rsp)
	movss	%xmm1, 76(%rsp)
	movss	%xmm8, 80(%rsp)
	movl	$0, 84(%rsp)
	leaq	40(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB16_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB16_2:                               # %.split
	movss	.LCPI16_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	mulps	16(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	shufps	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	movl	16(%rsp), %eax
	movl	20(%rsp), %ecx
	movl	24(%rsp), %edx
	movl	%eax, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	%edx, 8(%rbx)
	movl	$0, 12(%rbx)
	movd	%eax, %xmm0
	mulss	%xmm0, %xmm0
	movd	%ecx, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%edx, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI16_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB16_4
# BB#3:
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	movq	$0, 8(%rbx)
	jmp	.LBB16_7
.LBB16_4:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB16_6
# BB#5:                                 # %call.sqrt52
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB16_6:                               # %.split51
	movss	.LCPI16_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 4(%rbx)
	mulss	8(%rbx), %xmm2
	movss	%xmm2, 8(%rbx)
.LBB16_7:
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf, .Lfunc_end16-_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_0:
	.long	1065353216              # float 1
.LCPI17_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi83:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi84:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi86:
	.cfi_def_cfa_offset 80
.Lcfi87:
	.cfi_offset %rbx, -56
.Lcfi88:
	.cfi_offset %r12, -48
.Lcfi89:
	.cfi_offset %r13, -40
.Lcfi90:
	.cfi_offset %r14, -32
.Lcfi91:
	.cfi_offset %r15, -24
.Lcfi92:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB17_4
# BB#1:
	addss	.LCPI17_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB17_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB17_3:                               # %.split
	movss	.LCPI17_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB17_7
.LBB17_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI17_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB17_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB17_6:                               # %.split71
	movss	.LCPI17_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB17_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end17-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.type	_ZTV13btGhostObject,@object # @_ZTV13btGhostObject
	.section	.rodata,"a",@progbits
	.globl	_ZTV13btGhostObject
	.p2align	3
_ZTV13btGhostObject:
	.quad	0
	.quad	_ZTI13btGhostObject
	.quad	_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.quad	_ZN13btGhostObjectD2Ev
	.quad	_ZN13btGhostObjectD0Ev
	.quad	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.quad	_ZN13btGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.quad	_ZN13btGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.size	_ZTV13btGhostObject, 64

	.type	_ZTV24btPairCachingGhostObject,@object # @_ZTV24btPairCachingGhostObject
	.globl	_ZTV24btPairCachingGhostObject
	.p2align	3
_ZTV24btPairCachingGhostObject:
	.quad	0
	.quad	_ZTI24btPairCachingGhostObject
	.quad	_ZN17btCollisionObject24checkCollideWithOverrideEPS_
	.quad	_ZN24btPairCachingGhostObjectD2Ev
	.quad	_ZN24btPairCachingGhostObjectD0Ev
	.quad	_ZN17btCollisionObject17setCollisionShapeEP16btCollisionShape
	.quad	_ZN24btPairCachingGhostObject28addOverlappingObjectInternalEP17btBroadphaseProxyS1_
	.quad	_ZN24btPairCachingGhostObject31removeOverlappingObjectInternalEP17btBroadphaseProxyP12btDispatcherS1_
	.size	_ZTV24btPairCachingGhostObject, 64

	.type	_ZTS13btGhostObject,@object # @_ZTS13btGhostObject
	.globl	_ZTS13btGhostObject
_ZTS13btGhostObject:
	.asciz	"13btGhostObject"
	.size	_ZTS13btGhostObject, 16

	.type	_ZTI13btGhostObject,@object # @_ZTI13btGhostObject
	.globl	_ZTI13btGhostObject
	.p2align	4
_ZTI13btGhostObject:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13btGhostObject
	.quad	_ZTI17btCollisionObject
	.size	_ZTI13btGhostObject, 24

	.type	_ZTS24btPairCachingGhostObject,@object # @_ZTS24btPairCachingGhostObject
	.globl	_ZTS24btPairCachingGhostObject
	.p2align	4
_ZTS24btPairCachingGhostObject:
	.asciz	"24btPairCachingGhostObject"
	.size	_ZTS24btPairCachingGhostObject, 27

	.type	_ZTI24btPairCachingGhostObject,@object # @_ZTI24btPairCachingGhostObject
	.globl	_ZTI24btPairCachingGhostObject
	.p2align	4
_ZTI24btPairCachingGhostObject:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS24btPairCachingGhostObject
	.quad	_ZTI13btGhostObject
	.size	_ZTI24btPairCachingGhostObject, 24


	.globl	_ZN13btGhostObjectC1Ev
	.type	_ZN13btGhostObjectC1Ev,@function
_ZN13btGhostObjectC1Ev = _ZN13btGhostObjectC2Ev
	.globl	_ZN13btGhostObjectD1Ev
	.type	_ZN13btGhostObjectD1Ev,@function
_ZN13btGhostObjectD1Ev = _ZN13btGhostObjectD2Ev
	.globl	_ZN24btPairCachingGhostObjectC1Ev
	.type	_ZN24btPairCachingGhostObjectC1Ev,@function
_ZN24btPairCachingGhostObjectC1Ev = _ZN24btPairCachingGhostObjectC2Ev
	.globl	_ZN24btPairCachingGhostObjectD1Ev
	.type	_ZN24btPairCachingGhostObjectD1Ev,@function
_ZN24btPairCachingGhostObjectD1Ev = _ZN24btPairCachingGhostObjectD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
