	.text
	.file	"box_algebra.bc"
	.globl	hypre_IntersectBoxes
	.p2align	4, 0x90
	.type	hypre_IntersectBoxes,@function
hypre_IntersectBoxes:                   # @hypre_IntersectBoxes
	.cfi_startproc
# BB#0:
	leaq	12(%rdi), %r8
	leaq	12(%rsi), %rcx
	movl	(%rdi), %eax
	cmpl	(%rsi), %eax
	movq	%rdi, %rax
	cmovlq	%rsi, %rax
	movl	(%rax), %eax
	movl	%eax, (%rdx)
	movl	12(%rdi), %eax
	cmpl	12(%rsi), %eax
	movq	%rcx, %rax
	cmovlq	%r8, %rax
	movl	(%rax), %eax
	movl	%eax, 12(%rdx)
	movl	4(%rdi), %eax
	cmpl	4(%rsi), %eax
	movq	%rdi, %rax
	cmovlq	%rsi, %rax
	movl	4(%rax), %eax
	movl	%eax, 4(%rdx)
	movl	16(%rdi), %eax
	cmpl	16(%rsi), %eax
	movq	%rcx, %rax
	cmovlq	%r8, %rax
	movl	4(%rax), %eax
	movl	%eax, 16(%rdx)
	movl	8(%rdi), %eax
	cmpl	8(%rsi), %eax
	movq	%rdi, %rax
	cmovlq	%rsi, %rax
	movl	8(%rax), %eax
	movl	%eax, 8(%rdx)
	movl	20(%rdi), %eax
	cmpl	20(%rsi), %eax
	cmovlq	%r8, %rcx
	movl	8(%rcx), %eax
	movl	%eax, 20(%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	hypre_IntersectBoxes, .Lfunc_end0-hypre_IntersectBoxes
	.cfi_endproc

	.globl	hypre_SubtractBoxes
	.p2align	4, 0x90
	.type	hypre_SubtractBoxes,@function
hypre_SubtractBoxes:                    # @hypre_SubtractBoxes
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$7, %esi
	movq	%r14, %rdi
	callq	hypre_BoxArraySetSize
	movq	(%r14), %rax
	movl	(%r15), %ecx
	movl	%ecx, 144(%rax)
	movl	4(%r15), %ecx
	movl	%ecx, 148(%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 152(%rax)
	movl	12(%r15), %r8d
	movl	%r8d, 156(%rax)
	movl	16(%r15), %ecx
	movl	%ecx, 160(%rax)
	movl	20(%r15), %ecx
	movl	%ecx, 164(%rax)
	movl	(%r12), %edi
	cmpl	%r8d, %edi
	jg	.LBB1_3
# BB#1:                                 # %.lr.ph
	xorl	%esi, %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movl	12(%r12,%rdx,4), %ebx
	movl	144(%rax,%rdx,4), %ecx
	cmpl	%ecx, %ebx
	jl	.LBB1_3
# BB#5:                                 #   in Loop: Header=BB1_2 Depth=1
	cmpl	%ecx, %edi
	jle	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	movslq	%esi, %rsi
	movl	144(%rax), %ecx
	leaq	(%rsi,%rsi,2), %rdi
	leaq	(%rax,%rdi,8), %rbx
	movl	%ecx, (%rax,%rdi,8)
	movl	148(%rax), %ecx
	movl	%ecx, 4(%rax,%rdi,8)
	movl	152(%rax), %ecx
	movl	%ecx, 8(%rax,%rdi,8)
	movl	156(%rax), %ecx
	movl	%ecx, 12(%rax,%rdi,8)
	movl	160(%rax), %ecx
	movl	%ecx, 16(%rax,%rdi,8)
	movl	164(%rax), %ecx
	movl	%ecx, 20(%rax,%rdi,8)
	movl	(%r12,%rdx,4), %ecx
	decl	%ecx
	movl	%ecx, 12(%rbx,%rdx,4)
	movl	(%r12,%rdx,4), %ecx
	movl	%ecx, 144(%rax,%rdx,4)
	incl	%esi
	movl	12(%r12,%rdx,4), %ebx
	movl	156(%rax,%rdx,4), %r8d
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=1
	cmpl	%r8d, %ebx
	jge	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_2 Depth=1
	movslq	%esi, %rsi
	movl	144(%rax), %ecx
	leaq	(%rsi,%rsi,2), %rdi
	leaq	(%rax,%rdi,8), %rbx
	movl	%ecx, (%rax,%rdi,8)
	movl	148(%rax), %ecx
	movl	%ecx, 4(%rax,%rdi,8)
	movl	152(%rax), %ecx
	movl	%ecx, 8(%rax,%rdi,8)
	movl	156(%rax), %ecx
	movl	%ecx, 12(%rax,%rdi,8)
	movl	160(%rax), %ecx
	movl	%ecx, 16(%rax,%rdi,8)
	movl	164(%rax), %ecx
	movl	%ecx, 20(%rax,%rdi,8)
	movl	12(%r12,%rdx,4), %ecx
	incl	%ecx
	movl	%ecx, (%rbx,%rdx,4)
	movl	12(%r12,%rdx,4), %ecx
	movl	%ecx, 156(%rax,%rdx,4)
	incl	%esi
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=1
	leaq	1(%rdx), %rcx
	cmpq	$2, %rcx
	jg	.LBB1_4
# BB#10:                                # %._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	160(%rax,%rdx,4), %r8d
	movl	4(%r12,%rdx,4), %edi
	cmpl	%r8d, %edi
	movq	%rcx, %rdx
	jle	.LBB1_2
.LBB1_3:                                # %._crit_edge110
	movl	(%r15), %ecx
	movl	%ecx, (%rax)
	movl	4(%r15), %ecx
	movl	%ecx, 4(%rax)
	movl	8(%r15), %ecx
	movl	%ecx, 8(%rax)
	movl	12(%r15), %ecx
	movl	%ecx, 12(%rax)
	movl	16(%r15), %ecx
	movl	%ecx, 16(%rax)
	movl	20(%r15), %ecx
	movl	%ecx, 20(%rax)
	movl	$1, %esi
.LBB1_4:                                # %.loopexit
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	hypre_BoxArraySetSize
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	hypre_SubtractBoxes, .Lfunc_end1-hypre_SubtractBoxes
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	hypre_UnionBoxes
	.p2align	4, 0x90
	.type	hypre_UnionBoxes,@function
hypre_UnionBoxes:                       # @hypre_UnionBoxes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$344, %rsp              # imm = 0x158
.Lcfi15:
	.cfi_def_cfa_offset 400
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movslq	8(%r15), %r13
	cmpq	$2, %r13
	jl	.LBB2_90
# BB#1:                                 # %.preheader240399
	leal	(,%r13,8), %eax
	leal	(%rax,%rax,2), %edi
	callq	hypre_MAlloc
	movq	%rax, 160(%rsp)
	movq	%rax, 64(%rsp)          # 8-byte Spill
	leaq	(%rax,%r13,8), %rax
	movl	$0, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	%rax, 168(%rsp)
	leaq	(%rax,%r13,8), %rax
	movq	%rax, 176(%rsp)
	cmpl	$0, 8(%r15)
	movq	%r15, 56(%rsp)          # 8-byte Spill
	jle	.LBB2_2
# BB#3:                                 # %.lr.ph331
	movq	(%r15), %r8
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB2_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_5 Depth 2
                                        #       Child Loop BB2_8 Depth 3
                                        #       Child Loop BB2_16 Depth 3
                                        #       Child Loop BB2_18 Depth 3
                                        #       Child Loop BB2_98 Depth 3
                                        #       Child Loop BB2_105 Depth 3
                                        #       Child Loop BB2_107 Depth 3
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader239
                                        #   Parent Loop BB2_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_8 Depth 3
                                        #       Child Loop BB2_16 Depth 3
                                        #       Child Loop BB2_18 Depth 3
                                        #       Child Loop BB2_98 Depth 3
                                        #       Child Loop BB2_105 Depth 3
                                        #       Child Loop BB2_107 Depth 3
	leaq	(%r9,%r9,2), %rax
	leaq	(%r8,%rax,8), %rax
	movl	(%rax,%r11,4), %r10d
	movl	12(%rax,%r11,4), %ebx
	movslq	24(%rsp,%r11,4), %rsi
	testq	%rsi, %rsi
	jle	.LBB2_6
# BB#7:                                 # %.lr.ph322
                                        #   in Loop: Header=BB2_5 Depth=2
	movq	160(%rsp,%r11,8), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	(%rdi,%rbp,4), %r10d
	jle	.LBB2_9
# BB#10:                                #   in Loop: Header=BB2_8 Depth=3
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB2_8
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_6:                                #   in Loop: Header=BB2_5 Depth=2
	xorl	%ebp, %ebp
	jmp	.LBB2_11
	.p2align	4, 0x90
.LBB2_9:                                #   in Loop: Header=BB2_5 Depth=2
	je	.LBB2_20
	.p2align	4, 0x90
.LBB2_11:                               # %.critedge
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpl	%ebp, %esi
	movq	160(%rsp,%r11,8), %rdi
	jle	.LBB2_12
# BB#13:                                # %.lr.ph326
                                        #   in Loop: Header=BB2_5 Depth=2
	movslq	%ebp, %rax
	movl	%esi, %edx
	subl	%ebp, %edx
	leaq	-1(%rsi), %r14
	subq	%rax, %r14
	andq	$7, %rdx
	je	.LBB2_14
# BB#15:                                # %.prol.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	negq	%rdx
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB2_16:                               #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi,%rbp,4), %ecx
	movl	%ecx, (%rdi,%rbp,4)
	decq	%rbp
	incq	%rdx
	jne	.LBB2_16
	jmp	.LBB2_17
	.p2align	4, 0x90
.LBB2_12:                               # %.critedge.._crit_edge327_crit_edge
                                        #   in Loop: Header=BB2_5 Depth=2
	movslq	%ebp, %rax
	jmp	.LBB2_19
.LBB2_14:                               #   in Loop: Header=BB2_5 Depth=2
	movq	%rsi, %rbp
.LBB2_17:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpq	$7, %r14
	jb	.LBB2_19
	.p2align	4, 0x90
.LBB2_18:                               #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi,%rbp,4), %ecx
	movl	%ecx, (%rdi,%rbp,4)
	movl	-8(%rdi,%rbp,4), %ecx
	movl	%ecx, -4(%rdi,%rbp,4)
	movl	-12(%rdi,%rbp,4), %ecx
	movl	%ecx, -8(%rdi,%rbp,4)
	movl	-16(%rdi,%rbp,4), %ecx
	movl	%ecx, -12(%rdi,%rbp,4)
	movl	-20(%rdi,%rbp,4), %ecx
	movl	%ecx, -16(%rdi,%rbp,4)
	movl	-24(%rdi,%rbp,4), %ecx
	movl	%ecx, -20(%rdi,%rbp,4)
	movl	-28(%rdi,%rbp,4), %ecx
	movl	%ecx, -24(%rdi,%rbp,4)
	movl	-32(%rdi,%rbp,4), %ecx
	movl	%ecx, -28(%rdi,%rbp,4)
	leaq	-8(%rbp), %rbp
	cmpq	%rax, %rbp
	jg	.LBB2_18
.LBB2_19:                               # %._crit_edge327
                                        #   in Loop: Header=BB2_5 Depth=2
	movl	%r10d, (%rdi,%rax,4)
	leal	1(%rsi), %esi
	movl	%esi, 24(%rsp,%r11,4)
.LBB2_20:                               # %.preheader239.1389
                                        #   in Loop: Header=BB2_5 Depth=2
	leal	1(%rbx), %eax
	testl	%esi, %esi
	jle	.LBB2_21
# BB#97:                                # %.lr.ph322.1.preheader
                                        #   in Loop: Header=BB2_5 Depth=2
	movslq	%esi, %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_98:                               # %.lr.ph322.1
                                        #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rbp,4), %edx
	cmpl	%edx, %ebx
	jl	.LBB2_99
# BB#100:                               #   in Loop: Header=BB2_98 Depth=3
	incq	%rbp
	cmpq	%rcx, %rbp
	jl	.LBB2_98
	jmp	.LBB2_101
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_5 Depth=2
	xorl	%ebp, %ebp
	jmp	.LBB2_101
	.p2align	4, 0x90
.LBB2_99:                               #   in Loop: Header=BB2_5 Depth=2
	cmpl	%edx, %eax
	je	.LBB2_109
	.p2align	4, 0x90
.LBB2_101:                              # %.critedge.1
                                        #   in Loop: Header=BB2_5 Depth=2
	movl	%esi, %ecx
	subl	%ebp, %ecx
	jle	.LBB2_102
# BB#103:                               # %.lr.ph326.1
                                        #   in Loop: Header=BB2_5 Depth=2
	movslq	%esi, %rbx
	movslq	%ebp, %rbp
	leaq	-1(%rbx), %rdx
	subq	%rbp, %rdx
	andq	$7, %rcx
	je	.LBB2_106
# BB#104:                               # %.prol.preheader471
                                        #   in Loop: Header=BB2_5 Depth=2
	negq	%rcx
	.p2align	4, 0x90
.LBB2_105:                              #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi,%rbx,4), %esi
	movl	%esi, (%rdi,%rbx,4)
	decq	%rbx
	incq	%rcx
	jne	.LBB2_105
.LBB2_106:                              # %.prol.loopexit472
                                        #   in Loop: Header=BB2_5 Depth=2
	cmpq	$7, %rdx
	jb	.LBB2_108
	.p2align	4, 0x90
.LBB2_107:                              #   Parent Loop BB2_4 Depth=1
                                        #     Parent Loop BB2_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	-4(%rdi,%rbx,4), %ecx
	movl	%ecx, (%rdi,%rbx,4)
	movl	-8(%rdi,%rbx,4), %ecx
	movl	%ecx, -4(%rdi,%rbx,4)
	movl	-12(%rdi,%rbx,4), %ecx
	movl	%ecx, -8(%rdi,%rbx,4)
	movl	-16(%rdi,%rbx,4), %ecx
	movl	%ecx, -12(%rdi,%rbx,4)
	movl	-20(%rdi,%rbx,4), %ecx
	movl	%ecx, -16(%rdi,%rbx,4)
	movl	-24(%rdi,%rbx,4), %ecx
	movl	%ecx, -20(%rdi,%rbx,4)
	movl	-28(%rdi,%rbx,4), %ecx
	movl	%ecx, -24(%rdi,%rbx,4)
	movl	-32(%rdi,%rbx,4), %ecx
	movl	%ecx, -28(%rdi,%rbx,4)
	leaq	-8(%rbx), %rbx
	cmpq	%rbp, %rbx
	jg	.LBB2_107
	jmp	.LBB2_108
	.p2align	4, 0x90
.LBB2_102:                              # %.critedge.1.._crit_edge327.1_crit_edge
                                        #   in Loop: Header=BB2_5 Depth=2
	movslq	%ebp, %rbp
.LBB2_108:                              # %._crit_edge327.1
                                        #   in Loop: Header=BB2_5 Depth=2
	movl	%eax, (%rdi,%rbp,4)
	incl	24(%rsp,%r11,4)
.LBB2_109:                              #   in Loop: Header=BB2_5 Depth=2
	incq	%r11
	cmpq	$3, %r11
	jne	.LBB2_5
# BB#22:                                #   in Loop: Header=BB2_4 Depth=1
	incq	%r9
	movslq	8(%r15), %rcx
	cmpq	%rcx, %r9
	jl	.LBB2_4
# BB#23:                                # %.preheader238.preheader.loopexit
	movl	%r10d, 336(%rsp)
	movl	%eax, 340(%rsp)
	movl	24(%rsp), %eax
	movl	28(%rsp), %ecx
	movl	32(%rsp), %edx
	jmp	.LBB2_24
.LBB2_2:                                # %.preheader240399..preheader238.preheader_crit_edge
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
.LBB2_24:                               # %.preheader238.preheader
	addq	%r13, %r13
	leal	-1(%rax), %r12d
	movl	%r12d, 24(%rsp)
	leal	-1(%rcx), %r14d
	movl	%r14d, 28(%rsp)
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	leal	-1(%rdx), %edx
	movl	%edx, 32(%rsp)
	movl	%r14d, %edi
	imull	%r12d, %edi
	movl	%edx, 152(%rsp)         # 4-byte Spill
	imull	%edx, %edi
	movl	$1, 252(%rsp)
	movl	%eax, 256(%rsp)
	movq	%rcx, 184(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	movq	%rax, 240(%rsp)         # 8-byte Spill
	imull	%eax, %ecx
	movq	%rcx, %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movl	%ecx, 260(%rsp)
	movl	$4, %esi
	movq	%rdi, 264(%rsp)         # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	hypre_CAlloc
	movq	%rax, %r15
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 8(%rax)
	jle	.LBB2_44
# BB#25:                                # %.lr.ph319
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	232(%rsp), %rcx         # 8-byte Reload
	leal	1(%rax,%rcx), %ecx
	movslq	%r12d, %r8
	movslq	%r14d, %rdx
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r13,4), %rdi
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	leaq	(%rax,%r13,8), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leaq	16(%r15), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	imulq	%r8, %rdx
	movq	%rdx, 304(%rsp)         # 8-byte Spill
	leaq	(,%rdx,4), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	leaq	(,%r8,4), %rdi
	xorl	%edx, %edx
	movabsq	$4294967296, %r11       # imm = 0x100000000
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_26:                               # %.preheader237
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_27 Depth 2
                                        #     Child Loop BB2_29 Depth 2
                                        #     Child Loop BB2_31 Depth 2
                                        #     Child Loop BB2_33 Depth 2
                                        #     Child Loop BB2_35 Depth 2
                                        #     Child Loop BB2_37 Depth 2
                                        #     Child Loop BB2_41 Depth 2
                                        #       Child Loop BB2_53 Depth 3
                                        #         Child Loop BB2_60 Depth 4
                                        #         Child Loop BB2_63 Depth 4
                                        #         Child Loop BB2_55 Depth 4
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rdx,2), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	(%rsi,%rax,8), %r10d
	movl	$-1, %r9d
	xorl	%esi, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB2_27:                               #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r9d, %eax
	movl	%esi, %ebx
	addq	%r11, %rbp
	leal	1(%rax), %r9d
	leal	1(%rbx), %esi
	cmpl	(%rdx), %r10d
	leaq	4(%rdx), %rdx
	jne	.LBB2_27
# BB#28:                                #   in Loop: Header=BB2_26 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	12(%rdx,%rsi,8), %r10d
	incl	%r10d
	sarq	$32, %rbp
	movslq	%ebx, %rsi
	movq	%rbx, %rdi
	movq	%rsi, %rbx
	shlq	$32, %rbx
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rdx, %rbx
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rsi,4), %rdx
	movq	%rdi, %rsi
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_29:                               #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edi
	addq	%r11, %rbx
	incl	%eax
	leal	1(%rdi), %esi
	cmpl	(%rdx), %r10d
	leaq	4(%rdx), %rdx
	jne	.LBB2_29
# BB#30:                                # %.preheader237.1363
                                        #   in Loop: Header=BB2_26 Depth=1
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movl	%r9d, 156(%rsp)         # 4-byte Spill
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	4(%rax,%rdx,8), %r9d
	movl	$-1, %eax
	xorl	%edx, %edx
	movq	200(%rsp), %rbp         # 8-byte Reload
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB2_31:                               #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %edi
	movl	%edx, %ebx
	addq	%r11, %rsi
	leal	1(%rdi), %eax
	leal	1(%rbx), %edx
	cmpl	(%rbp), %r9d
	leaq	4(%rbp), %rbp
	jne	.LBB2_31
# BB#32:                                #   in Loop: Header=BB2_26 Depth=1
	movl	%eax, 80(%rsp)          # 4-byte Spill
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	(%rsp), %rax            # 8-byte Reload
	movl	16(%rbp,%rax,8), %eax
	incl	%eax
	sarq	$32, %rsi
	movq	%rsi, 312(%rsp)         # 8-byte Spill
	movslq	%ebx, %rsi
	movq	%r11, %rbx
	movq	%rsi, %r9
	shlq	$32, %r9
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	%rdx, %r9
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	200(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB2_33:                               #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rbx, %r9
	incl	%edi
	cmpl	(%rdx), %eax
	leaq	4(%rdx), %rdx
	jne	.LBB2_33
# BB#34:                                # %.preheader237.2364
                                        #   in Loop: Header=BB2_26 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	8(%rbp,%rax,8), %eax
	movl	%eax, 48(%rsp)          # 4-byte Spill
	movl	$-1, %r11d
	xorl	%eax, %eax
	movq	192(%rsp), %rsi         # 8-byte Reload
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB2_35:                               #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r11d, %ebp
	movl	%eax, %r8d
	addq	%rbx, %rdx
	leal	1(%rbp), %r11d
	leal	1(%r8), %eax
	movq	%rbx, %r10
	movl	48(%rsp), %ebx          # 4-byte Reload
	cmpl	(%rsi), %ebx
	movq	%r10, %rbx
	leaq	4(%rsi), %rsi
	jne	.LBB2_35
# BB#36:                                #   in Loop: Header=BB2_26 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	20(%rax,%rsi,8), %r10d
	incl	%r10d
	sarq	$32, %rdx
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	movslq	%r8d, %rdx
	movq	%rdx, %rsi
	shlq	$32, %rsi
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rsi
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	leaq	(%rax,%rdx,4), %rdx
	movl	%r10d, %eax
	.p2align	4, 0x90
.LBB2_37:                               #   Parent Loop BB2_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rbx, %rsi
	incl	%ebp
	cmpl	(%rdx), %eax
	leaq	4(%rdx), %rdx
	jne	.LBB2_37
# BB#38:                                #   in Loop: Header=BB2_26 Depth=1
	movq	%rsi, 208(%rsp)         # 8-byte Spill
	cmpl	%ebp, %r11d
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	jge	.LBB2_43
# BB#39:                                # %.lr.ph309
                                        #   in Loop: Header=BB2_26 Depth=1
	cmpl	%edi, 80(%rsp)          # 4-byte Folded Reload
	jge	.LBB2_43
# BB#40:                                # %.lr.ph309.split.us.preheader
                                        #   in Loop: Header=BB2_26 Depth=1
	sarq	$32, 208(%rsp)          # 8-byte Folded Spill
	movq	8(%rsp), %rdx           # 8-byte Reload
	sarq	$32, %rdx
	sarq	$32, %r9
	movq	16(%rsp), %rax          # 8-byte Reload
	subq	%rax, %rdx
	leaq	-8(%rdx), %rdi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	shrl	$3, %edi
	incl	%edi
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	andq	$-8, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	leaq	(%rax,%rdx), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	andl	$3, %edi
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movq	48(%rsp), %rax          # 8-byte Reload
	imulq	280(%rsp), %rax         # 8-byte Folded Reload
	addq	%rax, %rsi
	imulq	%r8, %rsi
	movq	104(%rsp), %rbp         # 8-byte Reload
	leaq	(%rbp,%rsi), %rax
	movq	224(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax,4), %r11
	movl	$-8, %edx
	subl	128(%rsp), %edx         # 4-byte Folded Reload
	movq	120(%rsp), %rax         # 8-byte Reload
	movslq	%eax, %rbx
	addl	%eax, %edx
	shrl	$3, %edx
	incl	%edx
	andl	$3, %edx
	negq	%rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	subq	%rbp, %rbx
	andq	$-8, %rbx
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	leaq	16(%rbp,%rsi), %rbx
	.p2align	4, 0x90
.LBB2_41:                               # %.lr.ph309.split.us
                                        #   Parent Loop BB2_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_53 Depth 3
                                        #         Child Loop BB2_60 Depth 4
                                        #         Child Loop BB2_63 Depth 4
                                        #         Child Loop BB2_55 Depth 4
	movq	320(%rsp), %rax         # 8-byte Reload
	cmpl	%eax, 156(%rsp)         # 4-byte Folded Reload
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	%rsi, %rbp
	movq	%rbx, 328(%rsp)         # 8-byte Spill
	movq	%r11, 104(%rsp)         # 8-byte Spill
	movq	312(%rsp), %rdi         # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	jge	.LBB2_42
	.p2align	4, 0x90
.LBB2_53:                               # %.lr.ph301.us.us
                                        #   Parent Loop BB2_26 Depth=1
                                        #     Parent Loop BB2_41 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_60 Depth 4
                                        #         Child Loop BB2_63 Depth 4
                                        #         Child Loop BB2_55 Depth 4
	cmpq	$7, 8(%rsp)             # 8-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	jbe	.LBB2_54
# BB#56:                                # %min.iters.checked
                                        #   in Loop: Header=BB2_53 Depth=3
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	je	.LBB2_54
# BB#57:                                # %vector.ph
                                        #   in Loop: Header=BB2_53 Depth=3
	cmpq	$0, 144(%rsp)           # 8-byte Folded Reload
	je	.LBB2_58
# BB#59:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB2_53 Depth=3
	movq	128(%rsp), %rsi         # 8-byte Reload
	movq	%r11, %rdx
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_60:                               # %vector.body.prol
                                        #   Parent Loop BB2_26 Depth=1
                                        #     Parent Loop BB2_41 Depth=2
                                        #       Parent Loop BB2_53 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$8, %r8
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB2_60
	jmp	.LBB2_61
.LBB2_58:                               #   in Loop: Header=BB2_53 Depth=3
	xorl	%r8d, %r8d
.LBB2_61:                               # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_53 Depth=3
	cmpq	$24, 80(%rsp)           # 8-byte Folded Reload
	jb	.LBB2_64
# BB#62:                                # %vector.ph.new
                                        #   in Loop: Header=BB2_53 Depth=3
	movq	120(%rsp), %r10         # 8-byte Reload
	subq	%r8, %r10
	addq	%rbx, %r8
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r8,4), %rsi
	.p2align	4, 0x90
.LBB2_63:                               # %vector.body
                                        #   Parent Loop BB2_26 Depth=1
                                        #     Parent Loop BB2_41 Depth=2
                                        #       Parent Loop BB2_53 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	movdqu	%xmm0, 16(%rsi)
	movdqu	%xmm0, 32(%rsi)
	subq	$-128, %rsi
	addq	$-32, %r10
	jne	.LBB2_63
.LBB2_64:                               # %middle.block
                                        #   in Loop: Header=BB2_53 Depth=3
	movq	(%rsp), %rax            # 8-byte Reload
	cmpq	%rax, 8(%rsp)           # 8-byte Folded Reload
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	96(%rsp), %r10          # 8-byte Reload
	je	.LBB2_65
	.p2align	4, 0x90
.LBB2_54:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_53 Depth=3
	leaq	(%rax,%rbp), %rdx
	leaq	(%r15,%rdx,4), %rdx
	movq	48(%rsp), %rsi          # 8-byte Reload
	subq	%rax, %rsi
	.p2align	4, 0x90
.LBB2_55:                               # %scalar.ph
                                        #   Parent Loop BB2_26 Depth=1
                                        #     Parent Loop BB2_41 Depth=2
                                        #       Parent Loop BB2_53 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	%ecx, (%rdx)
	addq	$4, %rdx
	decq	%rsi
	jne	.LBB2_55
.LBB2_65:                               # %._crit_edge302.us.us
                                        #   in Loop: Header=BB2_53 Depth=3
	incq	%rdi
	addq	%r10, %r11
	addq	%r8, %rbx
	addq	%r8, %rbp
	cmpq	%r9, %rdi
	jne	.LBB2_53
.LBB2_42:                               # %._crit_edge306.us
                                        #   in Loop: Header=BB2_41 Depth=2
	movq	216(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	104(%rsp), %r11         # 8-byte Reload
	addq	296(%rsp), %r11         # 8-byte Folded Reload
	movq	304(%rsp), %rax         # 8-byte Reload
	movq	328(%rsp), %rbx         # 8-byte Reload
	addq	%rax, %rbx
	movq	112(%rsp), %rsi         # 8-byte Reload
	addq	%rax, %rsi
	movq	%rdx, 216(%rsp)         # 8-byte Spill
	cmpq	208(%rsp), %rdx         # 8-byte Folded Reload
	jl	.LBB2_41
.LBB2_43:                               # %._crit_edge310
                                        #   in Loop: Header=BB2_26 Depth=1
	movq	288(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	56(%rsp), %rax          # 8-byte Reload
	movslq	8(%rax), %rax
	cmpq	%rax, %rdx
	movq	72(%rsp), %rsi          # 8-byte Reload
	movabsq	$4294967296, %r11       # imm = 0x100000000
	jl	.LBB2_26
.LBB2_44:                               # %.preheader236
	xorl	%eax, %eax
                                        # implicit-def: %ECX
	.p2align	4, 0x90
.LBB2_45:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_47 Depth 2
                                        #       Child Loop BB2_94 Depth 3
                                        #       Child Loop BB2_49 Depth 3
                                        #         Child Loop BB2_50 Depth 4
	movslq	.Lswitch.table.1(,%rax,4), %r10
	movl	$0, 36(%rsp,%r10,4)
	movl	24(%rsp,%r10,4), %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	testl	%edx, %edx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jle	.LBB2_96
# BB#46:                                # %.lr.ph288
                                        #   in Loop: Header=BB2_45 Depth=1
	movslq	.Lswitch.table(,%rax,4), %r9
	movl	24(%rsp,%r9,4), %r11d
	cltq
	.p2align	4, 0x90
.LBB2_47:                               #   Parent Loop BB2_45 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_94 Depth 3
                                        #       Child Loop BB2_49 Depth 3
                                        #         Child Loop BB2_50 Depth 4
	movl	$0, 36(%rsp,%r9,4)
	testl	%r11d, %r11d
	jle	.LBB2_95
# BB#48:                                # %.lr.ph281
                                        #   in Loop: Header=BB2_47 Depth=2
	movl	24(%rsp,%rax,4), %esi
	testl	%esi, %esi
	jle	.LBB2_94
	.p2align	4, 0x90
.LBB2_49:                               # %.lr.ph281.split.us
                                        #   Parent Loop BB2_45 Depth=1
                                        #     Parent Loop BB2_47 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_50 Depth 4
	movl	$0, 36(%rsp,%rax,4)
	xorl	%edx, %edx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_50:                               #   Parent Loop BB2_45 Depth=1
                                        #     Parent Loop BB2_47 Depth=2
                                        #       Parent Loop BB2_49 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	44(%rsp), %edi
	imull	%r14d, %edi
	addl	40(%rsp), %edi
	imull	%r12d, %edi
	movslq	36(%rsp), %rbx
	movslq	%edi, %rdi
	addq	%rbx, %rdi
	movl	(%r15,%rdi,4), %r8d
	testl	%ebp, %ebp
	je	.LBB2_91
# BB#51:                                #   in Loop: Header=BB2_50 Depth=4
	cmpl	%ecx, %r8d
	jne	.LBB2_91
# BB#52:                                #   in Loop: Header=BB2_50 Depth=4
	movl	$0, (%r15,%rdi,4)
	movl	252(%rsp,%rax,4), %edi
	movslq	%r13d, %rbx
	addl	%edi, (%r15,%rbx,4)
	jmp	.LBB2_92
	.p2align	4, 0x90
.LBB2_91:                               # %._crit_edge404
                                        #   in Loop: Header=BB2_50 Depth=4
	xorl	%ebp, %ebp
	testl	%r8d, %r8d
	setne	%bpl
	cmovel	%r13d, %edi
	cmovnel	%r8d, %ecx
	movl	%edi, %r13d
.LBB2_92:                               #   in Loop: Header=BB2_50 Depth=4
	incl	%edx
	movl	%edx, 36(%rsp,%rax,4)
	cmpl	%esi, %edx
	jl	.LBB2_50
# BB#93:                                # %._crit_edge275.us
                                        #   in Loop: Header=BB2_49 Depth=3
	movl	36(%rsp,%r9,4), %edx
	incl	%edx
	movl	%edx, 36(%rsp,%r9,4)
	cmpl	%r11d, %edx
	jl	.LBB2_49
	jmp	.LBB2_95
	.p2align	4, 0x90
.LBB2_94:                               # %.lr.ph281.split
                                        #   Parent Loop BB2_45 Depth=1
                                        #     Parent Loop BB2_47 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	$0, 36(%rsp,%rax,4)
	movl	36(%rsp,%r9,4), %edx
	incl	%edx
	movl	%edx, 36(%rsp,%r9,4)
	cmpl	%r11d, %edx
	jl	.LBB2_94
.LBB2_95:                               # %._crit_edge282
                                        #   in Loop: Header=BB2_47 Depth=2
	movl	36(%rsp,%r10,4), %edx
	incl	%edx
	movl	%edx, 36(%rsp,%r10,4)
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	jl	.LBB2_47
.LBB2_96:                               # %._crit_edge289
                                        #   in Loop: Header=BB2_45 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	incq	%rax
	cmpq	$3, %rax
	jne	.LBB2_45
# BB#66:                                # %.preheader235
	movq	264(%rsp), %rbp         # 8-byte Reload
	testl	%ebp, %ebp
	jle	.LBB2_67
# BB#68:                                # %.lr.ph267.preheader
	movl	%ebp, %eax
	cmpl	$7, %ebp
	ja	.LBB2_72
# BB#69:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB2_70
.LBB2_67:
	xorl	%esi, %esi
	movq	56(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB2_77
.LBB2_72:                               # %min.iters.checked439
	andl	$7, %ebp
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movq	56(%rsp), %rdi          # 8-byte Reload
	je	.LBB2_73
# BB#74:                                # %vector.body435.preheader
	leaq	16(%r15), %rdx
	pxor	%xmm1, %xmm1
	movdqa	.LCPI2_0(%rip), %xmm2   # xmm2 = [1,1,1,1]
	movq	%rcx, %rsi
	pxor	%xmm3, %xmm3
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_75:                               # %vector.body435
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rdx), %xmm4
	movdqu	(%rdx), %xmm5
	pcmpeqd	%xmm1, %xmm4
	pandn	%xmm2, %xmm4
	pcmpeqd	%xmm1, %xmm5
	pandn	%xmm2, %xmm5
	paddd	%xmm4, %xmm3
	paddd	%xmm5, %xmm0
	addq	$32, %rdx
	addq	$-8, %rsi
	jne	.LBB2_75
# BB#76:                                # %middle.block436
	paddd	%xmm3, %xmm0
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	paddd	%xmm0, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	paddd	%xmm1, %xmm0
	movd	%xmm0, %esi
	testl	%ebp, %ebp
	jne	.LBB2_70
	jmp	.LBB2_77
.LBB2_73:
	xorl	%ecx, %ecx
	xorl	%esi, %esi
.LBB2_70:                               # %.lr.ph267.preheader457
	leaq	(%r15,%rcx,4), %rdx
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB2_71:                               # %.lr.ph267
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$1, (%rdx)
	sbbl	$-1, %esi
	addq	$4, %rdx
	decq	%rax
	jne	.LBB2_71
.LBB2_77:                               # %._crit_edge268
	callq	hypre_BoxArraySetSize
	cmpl	$2, 272(%rsp)           # 4-byte Folded Reload
	jl	.LBB2_89
# BB#78:                                # %.preheader234.lr.ph
	movl	24(%rsp), %ecx
	movq	160(%rsp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	168(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	176(%rsp), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	imull	%r14d, %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movslq	152(%rsp), %rcx         # 4-byte Folded Reload
	movl	%r14d, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r12d, %r12d
	xorl	%edi, %edi
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	48(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_79:                               # %.preheader234
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_82 Depth 2
                                        #       Child Loop BB2_83 Depth 3
	cmpl	$2, %eax
	jl	.LBB2_88
# BB#80:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB2_79 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_88
# BB#81:                                # %.preheader.us.preheader
                                        #   in Loop: Header=BB2_79 Depth=1
	movl	%edi, 80(%rsp)          # 4-byte Spill
	movl	%edi, %eax
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB2_82:                               # %.preheader.us
                                        #   Parent Loop BB2_79 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_83 Depth 3
	movl	%eax, 96(%rsp)          # 4-byte Spill
	cltq
	leaq	(%r15,%rax,4), %r14
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB2_83:                               #   Parent Loop BB2_79 Depth=1
                                        #     Parent Loop BB2_82 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r14,%r10,4), %ecx
	testl	%ecx, %ecx
	je	.LBB2_85
# BB#84:                                #   in Loop: Header=BB2_83 Depth=3
	movl	%ecx, %eax
	cltd
	movq	240(%rsp), %rbx         # 8-byte Reload
	idivl	%ebx
	movl	%edx, %esi
	movl	%ecx, %eax
	cltd
	idivl	232(%rsp)               # 4-byte Folded Reload
	movl	%eax, %ecx
	movl	%edx, %eax
	cltd
	idivl	%ebx
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rdx
	movslq	%r12d, %r12
	movl	(%r8,%r10,4), %r11d
	leaq	(%r12,%r12,2), %r13
	movl	%r11d, (%rdx,%r13,8)
	movl	(%rbp,%r9,4), %ebx
	movl	%ebx, 4(%rdx,%r13,8)
	movq	(%rsp), %r11            # 8-byte Reload
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	(%r11,%rdi,4), %ebx
	movl	%ebx, 8(%rdx,%r13,8)
	addl	%r10d, %esi
	movslq	%esi, %rsi
	movl	(%r8,%rsi,4), %esi
	decl	%esi
	movl	%esi, 12(%rdx,%r13,8)
	addl	%r9d, %eax
	cltq
	movl	(%rbp,%rax,4), %eax
	decl	%eax
	movl	%eax, 16(%rdx,%r13,8)
	addl	%edi, %ecx
	movslq	%ecx, %rax
	movl	(%r11,%rax,4), %eax
	decl	%eax
	movl	%eax, 20(%rdx,%r13,8)
	incl	%r12d
.LBB2_85:                               #   in Loop: Header=BB2_83 Depth=3
	incq	%r10
	cmpq	%r10, 16(%rsp)          # 8-byte Folded Reload
	jne	.LBB2_83
# BB#86:                                # %._crit_edge.us
                                        #   in Loop: Header=BB2_82 Depth=2
	incq	%r9
	movl	96(%rsp), %eax          # 4-byte Reload
	addl	16(%rsp), %eax          # 4-byte Folded Reload
	cmpq	88(%rsp), %r9           # 8-byte Folded Reload
	jne	.LBB2_82
# BB#87:                                # %._crit_edge256.loopexit
                                        #   in Loop: Header=BB2_79 Depth=1
	movl	80(%rsp), %edi          # 4-byte Reload
	addl	144(%rsp), %edi         # 4-byte Folded Reload
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	136(%rsp), %rcx         # 8-byte Reload
.LBB2_88:                               # %._crit_edge256
                                        #   in Loop: Header=BB2_79 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, %rsi
	incq	%rsi
	movq	%rsi, %rdx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	cmpq	%rcx, %rsi
	jl	.LBB2_79
.LBB2_89:                               # %._crit_edge264
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	hypre_Free
	movq	$0, 160(%rsp)
	movq	%r15, %rdi
	callq	hypre_Free
.LBB2_90:
	xorl	%eax, %eax
	addq	$344, %rsp              # imm = 0x158
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	hypre_UnionBoxes, .Lfunc_end2-hypre_UnionBoxes
	.cfi_endproc

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	2
.Lswitch.table:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.size	.Lswitch.table, 12

	.type	.Lswitch.table.1,@object # @switch.table.1
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.Lswitch.table.1:
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.size	.Lswitch.table.1, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
