	.text
	.file	"util.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4670349086937841664     # double 16807
.LCPI0_1:
	.quad	4607182418800017408     # double 1
.LCPI0_2:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	my_rand
	.p2align	4, 0x90
	.type	my_rand,@function
my_rand:                                # @my_rand
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	divsd	.LCPI0_2(%rip), %xmm0
	callq	floor
	mulsd	.LCPI0_2(%rip), %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm0
	popq	%rax
	retq
.Lfunc_end0:
	.size	my_rand, .Lfunc_end0-my_rand
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4746794007244308480     # double 2147483647
	.text
	.globl	xrand
	.p2align	4, 0x90
	.type	xrand,@function
xrand:                                  # @xrand
	.cfi_startproc
# BB#0:
	subsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	divsd	.LCPI1_0(%rip), %xmm1
	addsd	%xmm1, %xmm0
	retq
.Lfunc_end1:
	.size	xrand, .Lfunc_end1-xrand
	.cfi_endproc

	.globl	error
	.p2align	4, 0x90
	.type	error,@function
error:                                  # @error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	stderr(%rip), %rdi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	callq	fprintf
	callq	__errno_location
	cmpl	$0, (%rax)
	je	.LBB2_2
# BB#1:
	movl	$.L.str, %edi
	callq	perror
.LBB2_2:
	xorl	%edi, %edi
	callq	exit
.Lfunc_end2:
	.size	error, .Lfunc_end2-error
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error"
	.size	.L.str, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
