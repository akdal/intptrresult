	.text
	.file	"main.bc"
	.globl	printit
	.p2align	4, 0x90
	.type	printit,@function
printit:                                # @printit
	.cfi_startproc
# BB#0:
	movq	%rsi, %rcx
	movl	%edi, %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%edx, %esi
	movq	%rcx, %rdx
	jmp	printf                  # TAILCALL
.Lfunc_end0:
	.size	printit, .Lfunc_end0-printit
	.cfi_endproc

	.globl	addfile
	.p2align	4, 0x90
	.type	addfile,@function
addfile:                                # @addfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 80
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movq	$0, 16(%rsp)
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB1_13
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	movq	%rsp, %r15
.LBB1_2:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_7 Depth 2
	testl	%ebp, %ebp
	je	.LBB1_3
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph.split
                                        #   Parent Loop BB1_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %ebp
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=2
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	movl	(%rax,%rcx,4), %ebp
.LBB1_9:                                # %tolower.exit
                                        #   in Loop: Header=BB1_7 Depth=2
	movl	%ebp, %eax
	shll	$24, %eax
	addl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	cmpl	$452984830, %eax        # imm = 0x1AFFFFFE
	ja	.LBB1_11
# BB#10:                                #   in Loop: Header=BB1_7 Depth=2
	movsbl	%bpl, %esi
	movq	%r15, %rdi
	callq	charsequence_push
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB1_7
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.split.us
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	subl	$-128, %eax
	cmpl	$383, %eax              # imm = 0x17F
	ja	.LBB1_5
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=1
	callq	__ctype_tolower_loc
	movq	(%rax), %rax
	movslq	%r12d, %rcx
	movl	(%rax,%rcx,4), %r12d
.LBB1_5:                                # %tolower.exit.us
                                        #   in Loop: Header=BB1_2 Depth=1
	movl	%r12d, %eax
	shll	$24, %eax
	addl	$-1610612737, %eax      # imm = 0x9FFFFFFF
	xorl	%ebp, %ebp
	cmpl	$452984831, %eax        # imm = 0x1AFFFFFF
	jae	.LBB1_12
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	%r15, %rdi
	callq	charsequence_reset
	movsbl	%r12b, %esi
	movq	%r15, %rdi
	callq	charsequence_push
	movl	$1, %ebp
	jmp	.LBB1_12
	.p2align	4, 0x90
.LBB1_11:                               # %.us-lcssa21
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%r15, %rdi
	callq	charsequence_val
	movq	%rax, %rbp
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	trie_insert
	movq	%rbp, %rdi
	callq	free
	xorl	%ebp, %ebp
.LBB1_12:                               # %.outer.backedge
                                        #   in Loop: Header=BB1_2 Depth=1
	movq	%rbx, %rdi
	callq	feof
	testl	%eax, %eax
	je	.LBB1_2
.LBB1_13:                               # %.outer._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	addfile, .Lfunc_end1-addfile
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	callq	trie_init
	movq	%rax, t(%rip)
	cmpl	$1, %ebp
	jne	.LBB2_1
# BB#8:
	movq	stdin(%rip), %rsi
	movq	%rax, %rdi
	callq	addfile
	jmp	.LBB2_7
.LBB2_1:                                # %.preheader
	cmpl	$2, %ebp
	jl	.LBB2_7
# BB#2:                                 # %.lr.ph.preheader
	addq	$8, %rbx
	jmp	.LBB2_3
.LBB2_4:                                #   in Loop: Header=BB2_3 Depth=1
	movq	stderr(%rip), %rdi
	movq	(%rbx), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	jmp	.LBB2_6
	.p2align	4, 0x90
.LBB2_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movl	$.L.str.1, %esi
	callq	fopen
	testq	%rax, %rax
	je	.LBB2_4
# BB#5:                                 #   in Loop: Header=BB2_3 Depth=1
	movq	t(%rip), %rdi
	movq	%rax, %rsi
	callq	addfile
.LBB2_6:                                #   in Loop: Header=BB2_3 Depth=1
	decl	%ebp
	addq	$8, %rbx
	cmpl	$1, %ebp
	jg	.LBB2_3
.LBB2_7:                                # %.loopexit
	movq	t(%rip), %rdi
	movl	$printit, %esi
	callq	trie_scan
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%5d : '%s'\n"
	.size	.L.str, 12

	.type	t,@object               # @t
	.comm	t,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"rb"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"unable to open file '%s'\n"
	.size	.L.str.2, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
