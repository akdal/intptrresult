	.text
	.file	"mpglib_main.bc"
	.globl	is_syncword
	.p2align	4, 0x90
	.type	is_syncword,@function
is_syncword:                            # @is_syncword
	.cfi_startproc
# BB#0:
	cmpb	$-1, (%rdi)
	je	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB0_2:
	cmpb	$-17, 1(%rdi)
	seta	%al
	movzbl	%al, %eax
	retq
.Lfunc_end0:
	.size	is_syncword, .Lfunc_end0-is_syncword
	.cfi_endproc

	.globl	lame_decode_initfile
	.p2align	4, 0x90
	.type	lame_decode_initfile,@function
lame_decode_initfile:                   # @lame_decode_initfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 208
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %r14
	movl	$mp, %edi
	callq	InitMP3
	xorl	%ebp, %ebp
	movl	$buf, %edi
	xorl	%esi, %esi
	movl	$16384, %edx            # imm = 0x4000
	callq	memset
	.p2align	4, 0x90
.LBB1_1:                                # %is_syncword.exit.thread
                                        # =>This Inner Loop Header: Depth=1
	movb	%bpl, buf(%rip)
	movl	$buf+1, %edi
	movl	$1, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fread
	movl	$-1, %ebx
	testq	%rax, %rax
	je	.LBB1_12
# BB#2:                                 # %is_syncword.exit.thread._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	movb	buf+1(%rip), %bpl
	cmpb	$-1, buf(%rip)
	jne	.LBB1_1
# BB#3:                                 # %is_syncword.exit.thread._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	cmpb	$-16, %bpl
	jb	.LBB1_1
# BB#4:
	movl	$buf+2, %edi
	movl	$1, %esi
	movl	$46, %edx
	movq	%r14, %rcx
	callq	fread
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB1_12
# BB#5:
	movq	%r15, 8(%rsp)           # 8-byte Spill
	addl	$2, %r14d
	leaq	24(%rsp), %rdi
	movl	$buf, %esi
	callq	GetVbrTag
	movl	%eax, %ebp
	movslq	36(%rsp), %r15
	movl	$0, 4(%rsp)
	leaq	4(%rsp), %r9
	movl	$mp, %edi
	movl	$buf, %esi
	movl	$out, %ecx
	movl	$8192, %r8d             # imm = 0x2000
	movl	%r14d, %edx
	callq	decodeMP3
	xorl	%ebx, %ebx
	testl	%ebp, %ebp
	cmovneq	%r15, %rbx
	jne	.LBB1_8
# BB#6:
	movl	4(%rsp), %eax
	testl	%eax, %eax
	jg	.LBB1_7
.LBB1_8:
	movl	mp+28(%rip), %eax
	movl	%eax, (%r13)
	movslq	mp+64(%rip), %rax
	movl	freqs(,%rax,8), %eax
	movl	%eax, (%r12)
	movslq	mp+40(%rip), %rax
	movslq	mp+52(%rip), %rcx
	shlq	$6, %rcx
	movslq	mp+60(%rip), %rdx
	leaq	(%rax,%rax,2), %rax
	shlq	$6, %rax
	addq	%rcx, %rax
	movl	tabsel_123-64(%rax,%rdx,4), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	testl	%ebp, %ebp
	movq	8(%rsp), %rdx           # 8-byte Reload
	je	.LBB1_11
# BB#9:
	testq	%rbx, %rbx
	je	.LBB1_11
# BB#10:
	cmpl	$0, mp+40(%rip)
	movl	$1152, %ecx             # imm = 0x480
	movl	$576, %eax              # imm = 0x240
	cmoveq	%rcx, %rax
	imulq	%rbx, %rax
.LBB1_11:
	movq	%rax, (%rdx)
	xorl	%ebx, %ebx
.LBB1_12:                               # %.loopexit
	movl	%ebx, %eax
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_7:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$49, %esi
	movl	$1, %edx
	callq	fwrite
	jmp	.LBB1_8
.Lfunc_end1:
	.size	lame_decode_initfile, .Lfunc_end1-lame_decode_initfile
	.cfi_endproc

	.globl	lame_decode_init
	.p2align	4, 0x90
	.type	lame_decode_init,@function
lame_decode_init:                       # @lame_decode_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$mp, %edi
	callq	InitMP3
	movl	$buf, %edi
	xorl	%esi, %esi
	movl	$16384, %edx            # imm = 0x4000
	callq	memset
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	lame_decode_init, .Lfunc_end2-lame_decode_init
	.cfi_endproc

	.globl	lame_decode_fromfile
	.p2align	4, 0x90
	.type	lame_decode_fromfile,@function
lame_decode_fromfile:                   # @lame_decode_fromfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 128
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	$0, 8(%rsp)
	movl	$buf, %edi
	movl	$1, %esi
	movl	$64, %edx
	movq	%rbx, %rcx
	callq	fread
	testq	%rax, %rax
	je	.LBB3_1
# BB#2:                                 # %.preheader45.preheader
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader45
                                        # =>This Inner Loop Header: Depth=1
	movl	$mp, %edi
	movl	$buf, %esi
	movl	$out, %ecx
	movl	$8192, %r8d             # imm = 0x2000
	movl	%eax, %edx
	movq	%r12, %r9
	callq	decodeMP3
	movl	%eax, %r13d
	cmpl	$1, %r13d
	je	.LBB3_5
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	8(%rsp), %edx
	testl	%edx, %edx
	jne	.LBB3_7
.LBB3_5:                                # %.critedge
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	$buf, %edi
	movl	$1, %esi
	movl	$100, %edx
	movq	%rbx, %rcx
	callq	fread
	testq	%rax, %rax
	jne	.LBB3_3
# BB#6:
	movl	$-1, %eax
	jmp	.LBB3_22
.LBB3_1:
	xorl	%eax, %eax
	jmp	.LBB3_22
.LBB3_7:
	xorl	%eax, %eax
	testl	%r13d, %r13d
	jne	.LBB3_21
# BB#8:
	movslq	mp+28(%rip), %rdi
	leal	(%rdi,%rdi), %ecx
	movl	%edx, %eax
	cltd
	idivl	%ecx
	cmpl	$1152, %eax             # imm = 0x480
	je	.LBB3_10
# BB#9:
	cmpl	$576, %eax              # imm = 0x240
	jne	.LBB3_23
.LBB3_10:                               # %.preheader44
	testl	%edi, %edi
	jle	.LBB3_21
# BB#11:                                # %.preheader.us.preheader
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%eax, %edx
	movl	%edi, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	-1(%rdx), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$3, %edx
	leaq	(%rdi,%rdi), %rsi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	leaq	(,%rdi,8), %rdi
	movl	$out, %eax
	xorl	%r12d, %r12d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB3_12:                               # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_15 Depth 2
                                        #     Child Loop BB3_18 Depth 2
	testq	%r11, %r11
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmoveq	48(%rsp), %rbx          # 8-byte Folded Reload
	testq	%rdx, %rdx
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB3_13
# BB#14:                                # %.prol.preheader
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	%rax, %rcx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_15:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rcx), %eax
	movw	%ax, (%rbx,%rbp,2)
	incq	%rbp
	addq	%rsi, %rcx
	cmpq	%rbp, %rdx
	jne	.LBB3_15
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_12 Depth=1
	xorl	%ebp, %ebp
.LBB3_16:                               # %.prol.loopexit
                                        #   in Loop: Header=BB3_12 Depth=1
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB3_19
# BB#17:                                # %.preheader.us.new
                                        #   in Loop: Header=BB3_12 Depth=1
	movq	16(%rsp), %r8           # 8-byte Reload
	subq	%rbp, %r8
	leaq	6(%rbx,%rbp,2), %r9
	leaq	3(%rbp), %r14
	imulq	%rsi, %r14
	addq	%r12, %r14
	leaq	2(%rbp), %r15
	imulq	%rsi, %r15
	addq	%r12, %r15
	movq	24(%rsp), %r10          # 8-byte Reload
	imulq	%rbp, %r10
	addq	%r11, %r10
	incq	%rbp
	imulq	%rsi, %rbp
	addq	%r12, %rbp
	movl	$out, %ebx
	.p2align	4, 0x90
.LBB3_18:                               #   Parent Loop BB3_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%r10,2), %eax
	movw	%ax, -6(%r9)
	movzwl	(%rbp,%rbx), %eax
	movw	%ax, -4(%r9)
	movzwl	(%r15,%rbx), %eax
	movw	%ax, -2(%r9)
	movzwl	(%r14,%rbx), %eax
	movw	%ax, (%r9)
	addq	$8, %r9
	addq	%rdi, %rbx
	addq	$-4, %r8
	jne	.LBB3_18
.LBB3_19:                               # %._crit_edge.us
                                        #   in Loop: Header=BB3_12 Depth=1
	incq	%r11
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	$2, %rax
	addq	$2, %r12
	cmpq	40(%rsp), %r11          # 8-byte Folded Reload
	jne	.LBB3_12
# BB#20:
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB3_21:                               # %.loopexit
	cmpl	$-1, %r13d
	cmovel	%r13d, %eax
.LBB3_22:                               # %.loopexit46
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_23:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-50, %edi
	callq	exit
.Lfunc_end3:
	.size	lame_decode_fromfile, .Lfunc_end3-lame_decode_fromfile
	.cfi_endproc

	.globl	lame_decode
	.p2align	4, 0x90
	.type	lame_decode,@function
lame_decode:                            # @lame_decode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 144
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbp
	movl	%esi, %eax
	movq	%rdi, %rdx
	leaq	36(%rsp), %r9
	movl	$mp, %edi
	movl	$out, %ecx
	movl	$8192, %r8d             # imm = 0x2000
	movq	%rdx, %rsi
	movl	%eax, %edx
	callq	decodeMP3
	movl	%eax, %esi
	xorl	%eax, %eax
	testl	%esi, %esi
	jne	.LBB4_23
# BB#1:
	movl	36(%rsp), %eax
	movslq	mp+28(%rip), %rdi
	leal	(%rdi,%rdi), %ecx
	cltd
	idivl	%ecx
	cmpl	$1152, %eax             # imm = 0x480
	jg	.LBB4_24
# BB#2:                                 # %.preheader29
	testl	%edi, %edi
	jle	.LBB4_23
# BB#3:                                 # %.preheader29
	testl	%eax, %eax
	jle	.LBB4_23
# BB#4:                                 # %.preheader.us.preheader
	movl	%esi, 28(%rsp)          # 4-byte Spill
	movl	%eax, %ecx
	leaq	(%rcx,%rcx), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	-1(%rcx), %rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movl	%eax, %edx
	andl	$15, %edx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	subq	%rdx, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	setne	%cl
	cmpl	$1, %edi
	sete	%dl
	andb	%cl, %dl
	movb	%dl, 7(%rsp)            # 1-byte Spill
	movq	%rdi, %r15
	shlq	$5, %r15
	leaq	(%rdi,%rdi), %r12
	leaq	(,%rdi,8), %r10
	movl	$out+16, %r13d
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	jmp	.LBB4_5
.LBB4_12:                               # %vector.body.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	leaq	16(%r14), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB4_13:                               # %vector.body
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rax)
	movups	%xmm1, (%rax)
	addq	%r15, %rdx
	addq	$32, %rax
	addq	$-16, %rcx
	jne	.LBB4_13
# BB#14:                                # %middle.block
                                        #   in Loop: Header=BB4_5 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	jne	.LBB4_15
	jmp	.LBB4_21
	.p2align	4, 0x90
.LBB4_5:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_13 Depth 2
                                        #     Child Loop BB4_17 Depth 2
                                        #     Child Loop BB4_20 Depth 2
	testq	%r8, %r8
	cmoveq	%rbp, %r14
	cmpl	$16, %eax
	jae	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_5 Depth=1
	xorl	%r11d, %r11d
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_7:                                # %min.iters.checked
                                        #   in Loop: Header=BB4_5 Depth=1
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB4_11
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	leaq	out(%rax,%r8,2), %rax
	cmpq	%rax, %r14
	jae	.LBB4_12
# BB#9:                                 # %vector.memcheck
                                        #   in Loop: Header=BB4_5 Depth=1
	leaq	out(%r8,%r8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	(%r14,%rcx,2), %rcx
	cmpq	%rcx, %rax
	jae	.LBB4_12
.LBB4_11:                               #   in Loop: Header=BB4_5 Depth=1
	xorl	%r11d, %r11d
.LBB4_15:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	subl	%r11d, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	subq	%r11, %rcx
	andq	$3, %rdx
	je	.LBB4_18
# BB#16:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	imulq	%r11, %rax
	addq	%r8, %rax
	leaq	out(%rax,%rax), %rax
	negq	%rdx
	.p2align	4, 0x90
.LBB4_17:                               # %scalar.ph.prol
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax), %esi
	movw	%si, (%r14,%r11,2)
	incq	%r11
	addq	%r12, %rax
	incq	%rdx
	jne	.LBB4_17
.LBB4_18:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB4_5 Depth=1
	cmpq	$3, %rcx
	jb	.LBB4_21
# BB#19:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB4_5 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	subq	%r11, %rdx
	leaq	6(%r14,%r11,2), %rcx
	leaq	3(%r11), %rsi
	imulq	%r12, %rsi
	addq	%r9, %rsi
	leaq	2(%r11), %rdi
	imulq	%r12, %rdi
	addq	%r9, %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	imulq	%r11, %rbx
	addq	%r8, %rbx
	incq	%r11
	imulq	%r12, %r11
	addq	%r9, %r11
	movl	$out, %eax
	.p2align	4, 0x90
.LBB4_20:                               # %scalar.ph
                                        #   Parent Loop BB4_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax,%rbx,2), %ebp
	movw	%bp, -6(%rcx)
	movzwl	(%r11,%rax), %ebp
	movw	%bp, -4(%rcx)
	movzwl	(%rdi,%rax), %ebp
	movw	%bp, -2(%rcx)
	movzwl	(%rsi,%rax), %ebp
	movw	%bp, (%rcx)
	addq	$8, %rcx
	addq	%r10, %rax
	addq	$-4, %rdx
	jne	.LBB4_20
.LBB4_21:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_5 Depth=1
	incq	%r8
	addq	$2, %r13
	addq	$2, %r9
	cmpq	16(%rsp), %r8           # 8-byte Folded Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	32(%rsp), %eax          # 4-byte Reload
	jl	.LBB4_5
# BB#22:
	movl	28(%rsp), %esi          # 4-byte Reload
.LBB4_23:                               # %.loopexit
	cmpl	$-1, %esi
	cmovel	%esi, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_24:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$65, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-50, %edi
	callq	exit
.Lfunc_end4:
	.size	lame_decode, .Lfunc_end4-lame_decode
	.cfi_endproc

	.type	mp,@object              # @mp
	.comm	mp,31880,8
	.type	buf,@object             # @buf
	.local	buf
	.comm	buf,16384,16
	.type	out,@object             # @out
	.local	out
	.comm	out,8192,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Opps: first frame of mpglib output will be lost \n"
	.size	.L.str, 50

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Opps: mpg123 returned more than one frame!  Cant handle this... \n"
	.size	.L.str.1, 66

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Opps: mpg123 returned more than one frame!  shouldn't happen... \n"
	.size	.L.str.2, 66


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
