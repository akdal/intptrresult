	.text
	.file	"BZip2Encoder.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.quad	900000                  # 0xdbba0
	.quad	2700002                 # 0x2932e2
	.text
	.globl	_ZN9NCompress6NBZip211CThreadInfo5AllocEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo5AllocEv,@function
_ZN9NCompress6NBZip211CThreadInfo5AllocEv: # @_ZN9NCompress6NBZip211CThreadInfo5AllocEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpq	$0, 24(%rbx)
	jne	.LBB0_2
# BB#1:
	movl	$7462144, %edi          # imm = 0x71DD00
	callq	BigAlloc
	movq	%rax, 24(%rbx)
	testq	%rax, %rax
	je	.LBB0_6
.LBB0_2:
	movb	$1, %bpl
	cmpq	$0, (%rbx)
	jne	.LBB0_7
# BB#3:
	movl	$4610480, %edi          # imm = 0x4659B0
	callq	MidAlloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB0_6
# BB#4:
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	paddq	.LCPI0_0(%rip), %xmm0
	movdqu	%xmm0, 8(%rbx)
	jmp	.LBB0_7
.LBB0_6:
	xorl	%ebp, %ebp
.LBB0_7:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN9NCompress6NBZip211CThreadInfo5AllocEv, .Lfunc_end0-_ZN9NCompress6NBZip211CThreadInfo5AllocEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo4FreeEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo4FreeEv,@function
_ZN9NCompress6NBZip211CThreadInfo4FreeEv: # @_ZN9NCompress6NBZip211CThreadInfo4FreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 16
.Lcfi6:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	callq	BigFree
	movq	$0, 24(%rbx)
	movq	(%rbx), %rdi
	callq	MidFree
	movq	$0, (%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN9NCompress6NBZip211CThreadInfo4FreeEv, .Lfunc_end1-_ZN9NCompress6NBZip211CThreadInfo4FreeEv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo6CreateEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo6CreateEv,@function
_ZN9NCompress6NBZip211CThreadInfo6CreateEv: # @_ZN9NCompress6NBZip211CThreadInfo6CreateEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	36112(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB2_3
# BB#1:
	leaq	36216(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	jne	.LBB2_3
# BB#2:
	leaq	36320(%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	testl	%eax, %eax
	je	.LBB2_4
.LBB2_3:
	popq	%rbx
	retq
.LBB2_4:
	leaq	36096(%rbx), %rdi
	movl	$_ZN9NCompress6NBZip2L8MFThreadEPv, %esi
	movq	%rbx, %rdx
	popq	%rbx
	jmp	Thread_Create           # TAILCALL
.Lfunc_end2:
	.size	_ZN9NCompress6NBZip211CThreadInfo6CreateEv, .Lfunc_end2-_ZN9NCompress6NBZip211CThreadInfo6CreateEv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip2L8MFThreadEPv,@function
_ZN9NCompress6NBZip2L8MFThreadEPv:      # @_ZN9NCompress6NBZip2L8MFThreadEPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	callq	_ZN9NCompress6NBZip211CThreadInfo10ThreadFuncEv
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_ZN9NCompress6NBZip2L8MFThreadEPv, .Lfunc_end3-_ZN9NCompress6NBZip2L8MFThreadEPv
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb,@function
_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb: # @_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	36088(%rbx), %rax
	movb	$1, 589(%rax)
	leaq	36112(%rbx), %rdi
	callq	Event_Set
	testb	%bpl, %bpl
	je	.LBB4_2
# BB#1:
	movq	36088(%rbx), %rdi
	addq	$536, %rdi              # imm = 0x218
	callq	pthread_mutex_unlock
.LBB4_2:
	movq	36088(%rbx), %rdi
	addq	$592, %rdi              # imm = 0x250
	callq	Event_Wait
	addq	$36216, %rbx            # imm = 0x8D78
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	Event_Set               # TAILCALL
.Lfunc_end4:
	.size	_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb, .Lfunc_end4-_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo10ThreadFuncEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo10ThreadFuncEv,@function
_ZN9NCompress6NBZip211CThreadInfo10ThreadFuncEv: # @_ZN9NCompress6NBZip211CThreadInfo10ThreadFuncEv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 64
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	36088(%rbx), %rdi
	addq	$432, %rdi              # imm = 0x1B0
	callq	Event_Wait
	movq	36088(%rbx), %rdi
	addq	$536, %rdi              # imm = 0x218
	callq	pthread_mutex_lock
	movq	36088(%rbx), %rdi
	cmpb	$0, 588(%rdi)
	jne	.LBB5_7
# BB#1:                                 # %.lr.ph
	leaq	36112(%rbx), %r14
	leaq	36216(%rbx), %r15
	xorl	%r12d, %r12d
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_2 Depth=1
	movq	(%rbx), %rsi
.Ltmp0:
	callq	_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh
	movl	%eax, %ebp
.Ltmp1:
# BB#9:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	36088(%rbx), %rdi
	movq	48(%rdi), %rax
	addq	80(%rdi), %rax
	subq	64(%rdi), %rax
	movq	%rax, 36424(%rbx)
	movl	584(%rdi), %eax
	movl	%eax, 36076(%rbx)
	incl	%eax
	movl	%eax, 584(%rdi)
	cmpl	576(%rdi), %eax
	cmovel	%r12d, %eax
	movl	%eax, 584(%rdi)
	testl	%ebp, %ebp
	je	.LBB5_10
# BB#22:                                #   in Loop: Header=BB5_2 Depth=1
	addq	$536, %rdi              # imm = 0x218
	callq	pthread_mutex_unlock
	xorl	%r13d, %r13d
.Ltmp3:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej
	movl	%eax, %ebp
.Ltmp4:
# BB#18:                                # %_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb.exit28.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB5_6
.LBB5_19:                               #   in Loop: Header=BB5_2 Depth=1
	movq	36088(%rbx), %rax
	movl	%ebp, 696(%rax)
	movb	$1, 589(%rax)
	movq	%r14, %rdi
	callq	Event_Set
	testb	%r13b, %r13b
	jne	.LBB5_4
	jmp	.LBB5_5
.LBB5_10:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$1, 589(%rdi)
	movb	$1, %r13b
.Ltmp5:
	movq	%r14, %rdi
	callq	Event_Set
.Ltmp6:
# BB#11:                                # %.noexc
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	36088(%rbx), %rdi
	addq	$536, %rdi              # imm = 0x218
	callq	pthread_mutex_unlock
	movq	36088(%rbx), %rdi
	movl	$592, %eax              # imm = 0x250
	addq	%rax, %rdi
.Ltmp7:
	callq	Event_Wait
.Ltmp8:
# BB#12:                                # %.noexc26
                                        #   in Loop: Header=BB5_2 Depth=1
.Ltmp9:
	movq	%r15, %rdi
	callq	Event_Set
.Ltmp10:
	jmp	.LBB5_6
.LBB5_13:                               #   in Loop: Header=BB5_2 Depth=1
.Ltmp2:
	movq	%rdx, %rbp
	movb	$1, %r13b
	jmp	.LBB5_15
.LBB5_14:                               #   in Loop: Header=BB5_2 Depth=1
.Ltmp11:
	movq	%rdx, %rbp
.LBB5_15:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rax, %rdi
	cmpl	$3, %ebp
	jne	.LBB5_20
# BB#16:                                #   in Loop: Header=BB5_2 Depth=1
	callq	__cxa_begin_catch
	jmp	.LBB5_17
.LBB5_20:                               #   in Loop: Header=BB5_2 Depth=1
	callq	__cxa_begin_catch
	cmpl	$2, %ebp
	jne	.LBB5_21
.LBB5_17:                               # %_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb.exit28.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	movl	(%rax), %ebp
	callq	__cxa_end_catch
	testl	%ebp, %ebp
	jne	.LBB5_19
	jmp	.LBB5_6
.LBB5_21:                               # %.thread
                                        #   in Loop: Header=BB5_2 Depth=1
	callq	__cxa_end_catch
	movl	$-2147467259, %ebp      # imm = 0x80004005
	jmp	.LBB5_19
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	cmpb	$0, 589(%rdi)
	je	.LBB5_8
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	movb	$1, 589(%rdi)
	movq	%r14, %rdi
	callq	Event_Set
.LBB5_4:                                # %_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb.exit28.backedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	36088(%rbx), %rdi
	addq	$536, %rdi              # imm = 0x218
	callq	pthread_mutex_unlock
.LBB5_5:                                # %_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb.exit28.backedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	36088(%rbx), %rdi
	addq	$592, %rdi              # imm = 0x250
	callq	Event_Wait
	movq	%r15, %rdi
	callq	Event_Set
.LBB5_6:                                # %_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb.exit28.backedge
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	36088(%rbx), %rdi
	addq	$432, %rdi              # imm = 0x1B0
	callq	Event_Wait
	movq	36088(%rbx), %rdi
	addq	$536, %rdi              # imm = 0x218
	callq	pthread_mutex_lock
	movq	36088(%rbx), %rdi
	cmpb	$0, 588(%rdi)
	je	.LBB5_2
.LBB5_7:                                # %_ZN9NCompress6NBZip211CThreadInfo12FinishStreamEb.exit28._crit_edge
	addq	$536, %rdi              # imm = 0x218
	callq	pthread_mutex_unlock
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN9NCompress6NBZip211CThreadInfo10ThreadFuncEv, .Lfunc_end5-_ZN9NCompress6NBZip211CThreadInfo10ThreadFuncEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\342\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	5                       #   On action: 3
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	5                       #   On action: 3
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp5-.Ltmp4           #   Call between .Ltmp4 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp10-.Ltmp5          #   Call between .Ltmp5 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin0   #     jumps to .Ltmp11
	.byte	5                       #   On action: 3
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Lfunc_end5-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh,@function
_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh: # @_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi34:
	.cfi_def_cfa_offset 80
.Lcfi35:
	.cfi_offset %rbx, -56
.Lcfi36:
	.cfi_offset %r12, -48
.Lcfi37:
	.cfi_offset %r13, -40
.Lcfi38:
	.cfi_offset %r14, -32
.Lcfi39:
	.cfi_offset %r15, -24
.Lcfi40:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r12
	leaq	48(%r12), %rbx
	movq	48(%r12), %rax
	cmpq	56(%r12), %rax
	jb	.LBB6_3
# BB#1:
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	xorl	%r15d, %r15d
	testb	%al, %al
	je	.LBB6_23
# BB#2:                                 # %._crit_edge.i
	movq	(%rbx), %rax
.LBB6_3:                                # %_ZN9CInBuffer8ReadByteERh.exit38.thread.us.preheader.lr.ph.lr.ph
	leaq	1(%rax), %rcx
	movq	%rcx, 48(%r12)
	movb	(%rax), %al
	imull	$100000, 28(%r12), %ecx # imm = 0x186A0
	decl	%ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rbp, (%rsp)            # 8-byte Spill
	movb	%al, (%rbp)
	movl	$1, %r15d
.LBB6_4:                                # %_ZN9CInBuffer8ReadByteERh.exit38.thread.us.preheader.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
                                        #       Child Loop BB6_6 Depth 3
	movl	%eax, %r14d
	movl	$1, %esi
.LBB6_5:                                # %_ZN9CInBuffer8ReadByteERh.exit38.thread.us.preheader
                                        #   Parent Loop BB6_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_6 Depth 3
	movq	(%rbx), %rcx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	leal	252(%rsi), %ebp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB6_6:                                # %_ZN9CInBuffer8ReadByteERh.exit38.thread.us
                                        #   Parent Loop BB6_4 Depth=1
                                        #     Parent Loop BB6_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	56(%r12), %rcx
	jb	.LBB6_9
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=3
	movq	%rbx, %rdi
	callq	_ZN9CInBuffer9ReadBlockEv
	testb	%al, %al
	je	.LBB6_20
# BB#8:                                 # %._crit_edge.i36.us
                                        #   in Loop: Header=BB6_6 Depth=3
	movq	(%rbx), %rcx
.LBB6_9:                                #   in Loop: Header=BB6_6 Depth=3
	movq	%rcx, %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movzbl	(%rax), %eax
	cmpb	%r14b, %al
	jne	.LBB6_13
# BB#10:                                #   in Loop: Header=BB6_6 Depth=3
	leal	-251(%rbp,%r13), %eax
	cmpl	$5, %eax
	jl	.LBB6_18
# BB#11:                                #   in Loop: Header=BB6_6 Depth=3
	leal	1(%rbp,%r13), %eax
	leal	1(%r13), %edx
	cmpl	$511, %eax              # imm = 0x1FF
	movl	%edx, %r13d
	jne	.LBB6_6
# BB#12:                                #   in Loop: Header=BB6_5 Depth=2
	xorl	%esi, %esi
	movb	$-1, %al
	jmp	.LBB6_19
	.p2align	4, 0x90
.LBB6_18:                               # %..sink.split_crit_edge
                                        #   in Loop: Header=BB6_5 Depth=2
	movq	8(%rsp), %rsi           # 8-byte Reload
	leal	1(%rsi,%r13), %esi
	movl	%r14d, %eax
.LBB6_19:                               # %.sink.split
                                        #   in Loop: Header=BB6_5 Depth=2
	movq	(%rsp), %rcx            # 8-byte Reload
	movb	%al, (%rcx,%r15)
	incq	%r15
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	jb	.LBB6_5
	jmp	.LBB6_21
	.p2align	4, 0x90
.LBB6_13:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB6_4 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	(%rdx,%r13), %ecx
	cmpl	$4, %ecx
	jl	.LBB6_14
# BB#15:                                #   in Loop: Header=BB6_4 Depth=1
	leal	252(%rdx,%r13), %ecx
	movl	%r15d, %edx
	leal	1(%r15), %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movb	%cl, (%rdi,%rdx)
	movl	%esi, %r15d
	jmp	.LBB6_16
	.p2align	4, 0x90
.LBB6_14:                               #   in Loop: Header=BB6_4 Depth=1
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB6_16:                               # %_ZN9CInBuffer8ReadByteERh.exit38.thread.outer
                                        #   in Loop: Header=BB6_4 Depth=1
	movl	%r15d, %ecx
	incl	%r15d
	movb	%al, (%rdi,%rcx)
	cmpl	16(%rsp), %r15d         # 4-byte Folded Reload
	jb	.LBB6_4
# BB#17:
	movl	$1, %esi
	cmpl	$4, %esi
	jge	.LBB6_22
	jmp	.LBB6_23
.LBB6_20:                               # %_ZN9CInBuffer8ReadByteERh.exit38.loopexit
	movq	8(%rsp), %rsi           # 8-byte Reload
	addl	%r13d, %esi
.LBB6_21:                               # %_ZN9CInBuffer8ReadByteERh.exit38
	cmpl	$4, %esi
	jl	.LBB6_23
.LBB6_22:
	addl	$252, %esi
	movl	%r15d, %eax
	leal	1(%r15), %ecx
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	%sil, (%rdx,%rax)
	movl	%ecx, %r15d
.LBB6_23:                               # %_ZN9CInBuffer8ReadByteERh.exit
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh, .Lfunc_end6-_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej,@function
_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej: # @_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 80
.Lcfi47:
	.cfi_offset %rbx, -48
.Lcfi48:
	.cfi_offset %r12, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%esi, %eax
	movq	%rdi, %rbx
	movq	16(%rbx), %rcx
	movq	%rcx, 16(%rsp)
	movl	$0, (%rsp)
	movl	$8, 4(%rsp)
	movb	$0, 8(%rsp)
	movq	%rsp, %rcx
	movq	%rcx, 32(%rbx)
	movl	$0, 36072(%rbx)
	movq	(%rbx), %rsi
	movq	36088(%rbx), %rcx
	movl	416(%rcx), %ecx
	movl	%eax, %edx
	callq	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj
	movq	36088(%rbx), %rdi
	cmpb	$0, 580(%rdi)
	je	.LBB7_2
# BB#1:
	movq	424(%rdi), %rax
	movl	36076(%rbx), %ecx
	imulq	$36688, %rcx, %rcx      # imm = 0x8F50
	leaq	36320(%rax,%rcx), %rdi
	callq	Event_Wait
	movq	36088(%rbx), %rdi
.LBB7_2:                                # %.preheader
	movl	36072(%rbx), %eax
	testl	%eax, %eax
	je	.LBB7_6
# BB#3:                                 # %.lr.ph.preheader
	movl	420(%rdi), %ecx
	xorl	%edx, %edx
	testb	$1, %al
	je	.LBB7_5
# BB#4:                                 # %.lr.ph.prol
	roll	%ecx
	xorl	31976(%rbx), %ecx
	movl	%ecx, 420(%rdi)
	movl	$1, %edx
.LBB7_5:                                # %.lr.ph.prol.loopexit
	cmpl	$1, %eax
	je	.LBB7_6
	.p2align	4, 0x90
.LBB7_8:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	roll	%ecx
	xorl	31976(%rbx,%rsi,4), %ecx
	movl	%ecx, 420(%rdi)
	leal	1(%rdx), %esi
	roll	%ecx
	xorl	31976(%rbx,%rsi,4), %ecx
	movl	%ecx, 420(%rdi)
	addl	$2, %edx
	cmpl	%eax, %edx
	jb	.LBB7_8
.LBB7_6:                                # %._crit_edge
	movq	16(%rbx), %rsi
	movl	(%rsp), %eax
	movl	$8, %ecx
	subl	4(%rsp), %ecx
	leal	(%rcx,%rax,8), %edx
	movzbl	8(%rsp), %ecx
	callq	_ZN9NCompress6NBZip28CEncoder10WriteBytesEPKhjh
	movq	36088(%rbx), %rbp
	cmpb	$0, 580(%rbp)
	je	.LBB7_7
# BB#9:
	movl	36076(%rbx), %r12d
	incl	%r12d
	movl	576(%rbp), %r15d
	cmpq	$0, 704(%rbp)
	je	.LBB7_10
# BB#11:
	leaq	352(%rbp), %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
	movl	$15, %ecx
	subl	408(%rbp), %ecx
	shrl	$3, %ecx
	addq	%rax, %rcx
	movq	%rcx, 24(%rsp)
	movq	36088(%rbx), %rax
	movq	704(%rax), %rdi
	movq	(%rdi), %rax
	leaq	36424(%rbx), %rsi
	leaq	24(%rsp), %rdx
	callq	*40(%rax)
	movl	%eax, %r14d
	movq	36088(%rbx), %rbp
	jmp	.LBB7_12
.LBB7_7:
	xorl	%r14d, %r14d
	jmp	.LBB7_13
.LBB7_10:
	xorl	%r14d, %r14d
.LBB7_12:
	xorl	%eax, %eax
	cmpl	%r15d, %r12d
	movq	424(%rbp), %rcx
	movl	%r12d, %edx
	cmovneq	%rdx, %rax
	imulq	$36688, %rax, %rax      # imm = 0x8F50
	leaq	36320(%rcx,%rax), %rdi
	callq	Event_Set
.LBB7_13:
	movl	%r14d, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej, .Lfunc_end7-_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoderC2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoderC2Ev,@function
_ZN9NCompress6NBZip28CEncoderC2Ev:      # @_ZN9NCompress6NBZip28CEncoderC2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 48
.Lcfi57:
	.cfi_offset %rbx, -48
.Lcfi58:
	.cfi_offset %r12, -40
.Lcfi59:
	.cfi_offset %r13, -32
.Lcfi60:
	.cfi_offset %r14, -24
.Lcfi61:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$0, 24(%rbx)
	movl	$_ZTVN9NCompress6NBZip28CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NBZip28CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress6NBZip28CEncoderE+160, 16(%rbx)
	movl	$9, 28(%rbx)
	movb	$0, 32(%rbx)
	leaq	48(%rbx), %r15
.Ltmp12:
	movq	%r15, %rdi
	callq	_ZN9CInBufferC1Ev
.Ltmp13:
# BB#1:
	leaq	352(%rbx), %r12
	movq	$0, 352(%rbx)
	movl	$0, 360(%rbx)
	movq	$0, 376(%rbx)
	movq	$0, 392(%rbx)
	movq	$1, 416(%rbx)
	leaq	432(%rbx), %r13
	movl	$0, 432(%rbx)
	leaq	536(%rbx), %rdi
.Ltmp15:
	callq	CriticalSection_Init
.Ltmp16:
# BB#2:
	movl	$0, 592(%rbx)
	movq	$0, 424(%rbx)
	movl	$0, 40(%rbx)
	movl	$1, 576(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB8_3:
.Ltmp17:
	movq	%rax, %r14
.Ltmp18:
	movq	%r13, %rdi
	callq	Event_Close
.Ltmp19:
# BB#4:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
.Ltmp20:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer4FreeEv
.Ltmp21:
# BB#5:
	movq	376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_7
# BB#6:
	movq	(%rdi), %rax
.Ltmp26:
	callq	*16(%rax)
.Ltmp27:
.LBB8_7:                                # %_ZN12CBitmEncoderI10COutBufferED2Ev.exit
.Ltmp28:
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4FreeEv
.Ltmp29:
# BB#8:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_17
# BB#9:
	movq	(%rdi), %rax
.Ltmp34:
	callq	*16(%rax)
.Ltmp35:
	jmp	.LBB8_17
.LBB8_13:
.Ltmp30:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_19
# BB#14:
	movq	(%rdi), %rax
.Ltmp31:
	callq	*16(%rax)
.Ltmp32:
	jmp	.LBB8_19
.LBB8_15:
.Ltmp33:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_10:
.Ltmp22:
	movq	%rax, %r14
	movq	376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_19
# BB#11:
	movq	(%rdi), %rax
.Ltmp23:
	callq	*16(%rax)
.Ltmp24:
	jmp	.LBB8_19
.LBB8_12:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_18:
.Ltmp36:
	movq	%rax, %r14
.LBB8_19:                               # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB8_16:
.Ltmp14:
	movq	%rax, %r14
.LBB8_17:                               # %_ZN9CInBufferD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN9NCompress6NBZip28CEncoderC2Ev, .Lfunc_end8-_ZN9NCompress6NBZip28CEncoderC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin1   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin1   #     jumps to .Ltmp22
	.byte	1                       #   On action: 1
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp27-.Ltmp26         #   Call between .Ltmp26 and .Ltmp27
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	1                       #   On action: 1
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	1                       #   On action: 1
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Lfunc_end8-.Ltmp24     #   Call between .Ltmp24 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end9:
	.size	__clang_call_terminate, .Lfunc_end9-__clang_call_terminate

	.text
	.globl	_ZN9NCompress6NBZip28CEncoderD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoderD2Ev,@function
_ZN9NCompress6NBZip28CEncoderD2Ev:      # @_ZN9NCompress6NBZip28CEncoderD2Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 32
.Lcfi65:
	.cfi_offset %rbx, -24
.Lcfi66:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN9NCompress6NBZip28CEncoderE+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress6NBZip28CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN9NCompress6NBZip28CEncoderE+160, 16(%rbx)
.Ltmp37:
	callq	_ZN9NCompress6NBZip28CEncoder4FreeEv
.Ltmp38:
# BB#1:
	leaq	592(%rbx), %rdi
.Ltmp42:
	callq	Event_Close
.Ltmp43:
# BB#2:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
	leaq	536(%rbx), %rdi
	callq	pthread_mutex_destroy
	leaq	432(%rbx), %rdi
.Ltmp47:
	callq	Event_Close
.Ltmp48:
# BB#3:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit8
	leaq	352(%rbx), %rdi
.Ltmp58:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp59:
# BB#4:
	movq	376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp64:
	callq	*16(%rax)
.Ltmp65:
.LBB10_6:                               # %_ZN12CBitmEncoderI10COutBufferED2Ev.exit
	leaq	48(%rbx), %rdi
.Ltmp76:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp77:
# BB#7:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_9
# BB#8:
	movq	(%rdi), %rax
.Ltmp82:
	callq	*16(%rax)
.Ltmp83:
.LBB10_9:                               # %_ZN9CInBufferD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB10_31:
.Ltmp84:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_19:
.Ltmp66:
	movq	%rax, %r14
	jmp	.LBB10_24
.LBB10_13:
.Ltmp78:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_27
# BB#14:
	movq	(%rdi), %rax
.Ltmp79:
	callq	*16(%rax)
.Ltmp80:
	jmp	.LBB10_27
.LBB10_15:
.Ltmp81:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_10:
.Ltmp60:
	movq	%rax, %r14
	movq	376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_24
# BB#11:
	movq	(%rdi), %rax
.Ltmp61:
	callq	*16(%rax)
.Ltmp62:
	jmp	.LBB10_24
.LBB10_12:
.Ltmp63:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_20:
.Ltmp49:
	movq	%rax, %r14
	jmp	.LBB10_21
.LBB10_17:
.Ltmp44:
	movq	%rax, %r14
	jmp	.LBB10_18
.LBB10_16:
.Ltmp39:
	movq	%rax, %r14
	leaq	592(%rbx), %rdi
.Ltmp40:
	callq	Event_Close
.Ltmp41:
.LBB10_18:                              # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit13
	leaq	536(%rbx), %rdi
	callq	pthread_mutex_destroy
	leaq	432(%rbx), %rdi
.Ltmp45:
	callq	Event_Close
.Ltmp46:
.LBB10_21:                              # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit15
	leaq	352(%rbx), %rdi
.Ltmp50:
	callq	_ZN10COutBuffer4FreeEv
.Ltmp51:
# BB#22:
	movq	376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_24
# BB#23:
	movq	(%rdi), %rax
.Ltmp56:
	callq	*16(%rax)
.Ltmp57:
.LBB10_24:                              # %_ZN12CBitmEncoderI10COutBufferED2Ev.exit20
	leaq	48(%rbx), %rdi
.Ltmp67:
	callq	_ZN9CInBuffer4FreeEv
.Ltmp68:
# BB#25:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_27
# BB#26:
	movq	(%rdi), %rax
.Ltmp73:
	callq	*16(%rax)
.Ltmp74:
.LBB10_27:                              # %_ZN9CInBufferD2Ev.exit25
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB10_28:
.Ltmp52:
	movq	%rax, %r14
	movq	376(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_36
# BB#29:
	movq	(%rdi), %rax
.Ltmp53:
	callq	*16(%rax)
.Ltmp54:
	jmp	.LBB10_36
.LBB10_30:
.Ltmp55:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_32:
.Ltmp69:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB10_36
# BB#33:
	movq	(%rdi), %rax
.Ltmp70:
	callq	*16(%rax)
.Ltmp71:
	jmp	.LBB10_36
.LBB10_34:
.Ltmp72:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB10_35:
.Ltmp75:
	movq	%rax, %r14
.LBB10_36:                              # %.body18
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end10:
	.size	_ZN9NCompress6NBZip28CEncoderD2Ev, .Lfunc_end10-_ZN9NCompress6NBZip28CEncoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp39-.Lfunc_begin2   #     jumps to .Ltmp39
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin2   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin2   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp58-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp59-.Ltmp58         #   Call between .Ltmp58 and .Ltmp59
	.long	.Ltmp60-.Lfunc_begin2   #     jumps to .Ltmp60
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp65-.Ltmp64         #   Call between .Ltmp64 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin2   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin2   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin2   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp83-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp79-.Ltmp83         #   Call between .Ltmp83 and .Ltmp79
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin2   #     jumps to .Ltmp81
	.byte	1                       #   On action: 1
	.long	.Ltmp61-.Lfunc_begin2   # >> Call Site 10 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin2   #     jumps to .Ltmp63
	.byte	1                       #   On action: 1
	.long	.Ltmp40-.Lfunc_begin2   # >> Call Site 11 <<
	.long	.Ltmp46-.Ltmp40         #   Call between .Ltmp40 and .Ltmp46
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp50-.Lfunc_begin2   # >> Call Site 12 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin2   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp56-.Lfunc_begin2   # >> Call Site 13 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp67-.Lfunc_begin2   # >> Call Site 14 <<
	.long	.Ltmp68-.Ltmp67         #   Call between .Ltmp67 and .Ltmp68
	.long	.Ltmp69-.Lfunc_begin2   #     jumps to .Ltmp69
	.byte	1                       #   On action: 1
	.long	.Ltmp73-.Lfunc_begin2   # >> Call Site 15 <<
	.long	.Ltmp74-.Ltmp73         #   Call between .Ltmp73 and .Ltmp74
	.long	.Ltmp75-.Lfunc_begin2   #     jumps to .Ltmp75
	.byte	1                       #   On action: 1
	.long	.Ltmp74-.Lfunc_begin2   # >> Call Site 16 <<
	.long	.Ltmp53-.Ltmp74         #   Call between .Ltmp74 and .Ltmp53
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin2   # >> Call Site 17 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin2   #     jumps to .Ltmp55
	.byte	1                       #   On action: 1
	.long	.Ltmp70-.Lfunc_begin2   # >> Call Site 18 <<
	.long	.Ltmp71-.Ltmp70         #   Call between .Ltmp70 and .Ltmp71
	.long	.Ltmp72-.Lfunc_begin2   #     jumps to .Ltmp72
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CEncoder4FreeEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder4FreeEv,@function
_ZN9NCompress6NBZip28CEncoder4FreeEv:   # @_ZN9NCompress6NBZip28CEncoder4FreeEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi71:
	.cfi_def_cfa_offset 48
.Lcfi72:
	.cfi_offset %rbx, -40
.Lcfi73:
	.cfi_offset %r12, -32
.Lcfi74:
	.cfi_offset %r14, -24
.Lcfi75:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	cmpq	$0, 424(%r15)
	je	.LBB11_13
# BB#1:
	movb	$1, 588(%r15)
	leaq	432(%r15), %rdi
	callq	Event_Set
	cmpl	$0, 576(%r15)
	movq	424(%r15), %r12
	je	.LBB11_6
# BB#2:                                 # %.lr.ph
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB11_3:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %r14d
	cmpb	$0, 580(%r15)
	je	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=1
	imulq	$36688, %r14, %rax      # imm = 0x8F50
	leaq	36096(%r12,%rax), %rdi
	callq	Thread_Wait
.LBB11_5:                               #   in Loop: Header=BB11_3 Depth=1
	imulq	$36688, %r14, %rbx      # imm = 0x8F50
	movq	24(%r12,%rbx), %rdi
	callq	BigFree
	movq	$0, 24(%r12,%rbx)
	movq	(%r12,%rbx), %rdi
	callq	MidFree
	movq	$0, (%r12,%rbx)
	leal	1(%r14), %eax
	movq	424(%r15), %r12
	cmpl	576(%r15), %eax
	jb	.LBB11_3
.LBB11_6:                               # %._crit_edge
	testq	%r12, %r12
	je	.LBB11_12
# BB#7:
	leaq	-8(%r12), %r14
	movq	-8(%r12), %rax
	testq	%rax, %rax
	je	.LBB11_11
# BB#8:                                 # %.preheader10.preheader
	imulq	$36688, %rax, %rbx      # imm = 0x8F50
	.p2align	4, 0x90
.LBB11_9:                               # %.preheader10
                                        # =>This Inner Loop Header: Depth=1
	leaq	-36688(%r12,%rbx), %rdi
.Ltmp85:
	callq	_ZN9NCompress6NBZip211CThreadInfoD2Ev
.Ltmp86:
# BB#10:                                #   in Loop: Header=BB11_9 Depth=1
	addq	$-36688, %rbx           # imm = 0xFFFF70B0
	jne	.LBB11_9
.LBB11_11:                              # %.loopexit11
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB11_12:
	movq	$0, 424(%r15)
.LBB11_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB11_14:
.Ltmp87:
	movq	%rax, %r15
	cmpq	$36688, %rbx            # imm = 0x8F50
	jne	.LBB11_16
	jmp	.LBB11_18
	.p2align	4, 0x90
.LBB11_17:                              #   in Loop: Header=BB11_16 Depth=1
	addq	$-36688, %rbx           # imm = 0xFFFF70B0
	cmpq	$36688, %rbx            # imm = 0x8F50
	je	.LBB11_18
.LBB11_16:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	-73376(%r12,%rbx), %rdi
.Ltmp88:
	callq	_ZN9NCompress6NBZip211CThreadInfoD2Ev
.Ltmp89:
	jmp	.LBB11_17
.LBB11_18:                              # %.loopexit
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB11_19:
.Ltmp90:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN9NCompress6NBZip28CEncoder4FreeEv, .Lfunc_end11-_ZN9NCompress6NBZip28CEncoder4FreeEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp85-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp85
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp89-.Ltmp88         #   Call between .Ltmp88 and .Ltmp89
	.long	.Ltmp90-.Lfunc_begin3   #     jumps to .Ltmp90
	.byte	1                       #   On action: 1
	.long	.Ltmp89-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp89    #   Call between .Ltmp89 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress6NBZip28CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CEncoderD1Ev,@function
_ZThn8_N9NCompress6NBZip28CEncoderD1Ev: # @_ZThn8_N9NCompress6NBZip28CEncoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NBZip28CEncoderD2Ev # TAILCALL
.Lfunc_end12:
	.size	_ZThn8_N9NCompress6NBZip28CEncoderD1Ev, .Lfunc_end12-_ZThn8_N9NCompress6NBZip28CEncoderD1Ev
	.cfi_endproc

	.globl	_ZThn16_N9NCompress6NBZip28CEncoderD1Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip28CEncoderD1Ev,@function
_ZThn16_N9NCompress6NBZip28CEncoderD1Ev: # @_ZThn16_N9NCompress6NBZip28CEncoderD1Ev
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress6NBZip28CEncoderD2Ev # TAILCALL
.Lfunc_end13:
	.size	_ZThn16_N9NCompress6NBZip28CEncoderD1Ev, .Lfunc_end13-_ZThn16_N9NCompress6NBZip28CEncoderD1Ev
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoderD0Ev,@function
_ZN9NCompress6NBZip28CEncoderD0Ev:      # @_ZN9NCompress6NBZip28CEncoderD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp91:
	callq	_ZN9NCompress6NBZip28CEncoderD2Ev
.Ltmp92:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB14_2:
.Ltmp93:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end14:
	.size	_ZN9NCompress6NBZip28CEncoderD0Ev, .Lfunc_end14-_ZN9NCompress6NBZip28CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp91-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp92-.Ltmp91         #   Call between .Ltmp91 and .Ltmp92
	.long	.Ltmp93-.Lfunc_begin4   #     jumps to .Ltmp93
	.byte	0                       #   On action: cleanup
	.long	.Ltmp92-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Lfunc_end14-.Ltmp92    #   Call between .Ltmp92 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn8_N9NCompress6NBZip28CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CEncoderD0Ev,@function
_ZThn8_N9NCompress6NBZip28CEncoderD0Ev: # @_ZThn8_N9NCompress6NBZip28CEncoderD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -24
.Lcfi85:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp94:
	movq	%rbx, %rdi
	callq	_ZN9NCompress6NBZip28CEncoderD2Ev
.Ltmp95:
# BB#1:                                 # %_ZN9NCompress6NBZip28CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB15_2:
.Ltmp96:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end15:
	.size	_ZThn8_N9NCompress6NBZip28CEncoderD0Ev, .Lfunc_end15-_ZThn8_N9NCompress6NBZip28CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp94-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin5   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp95-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end15-.Ltmp95    #   Call between .Ltmp95 and .Lfunc_end15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZThn16_N9NCompress6NBZip28CEncoderD0Ev
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip28CEncoderD0Ev,@function
_ZThn16_N9NCompress6NBZip28CEncoderD0Ev: # @_ZThn16_N9NCompress6NBZip28CEncoderD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 32
.Lcfi89:
	.cfi_offset %rbx, -24
.Lcfi90:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-16, %rbx
.Ltmp97:
	movq	%rbx, %rdi
	callq	_ZN9NCompress6NBZip28CEncoderD2Ev
.Ltmp98:
# BB#1:                                 # %_ZN9NCompress6NBZip28CEncoderD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB16_2:
.Ltmp99:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end16:
	.size	_ZThn16_N9NCompress6NBZip28CEncoderD0Ev, .Lfunc_end16-_ZThn16_N9NCompress6NBZip28CEncoderD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp97-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin6   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end16-.Ltmp98    #   Call between .Ltmp98 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CEncoder6CreateEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder6CreateEv,@function
_ZN9NCompress6NBZip28CEncoder6CreateEv: # @_ZN9NCompress6NBZip28CEncoder6CreateEv
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 64
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 432(%r14)
	je	.LBB17_20
.LBB17_1:                               # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit.thread
	cmpl	$0, 592(%r14)
	je	.LBB17_21
.LBB17_2:                               # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit41.thread
	cmpq	$0, 424(%r14)
	je	.LBB17_4
# BB#3:
	movl	40(%r14), %eax
	xorl	%ebp, %ebp
	cmpl	576(%r14), %eax
	je	.LBB17_25
.LBB17_4:
.Ltmp100:
	movq	%r14, %rdi
	callq	_ZN9NCompress6NBZip28CEncoder4FreeEv
.Ltmp101:
# BB#5:
	movl	576(%r14), %ebp
	cmpq	$1, %rbp
	seta	580(%r14)
	seta	%r12b
	movl	%ebp, 40(%r14)
	imulq	$36688, %rbp, %rbx      # imm = 0x8F50
	leaq	8(%rbx), %rdi
.Ltmp102:
	callq	_Znam
	movq	%rax, %r15
.Ltmp103:
# BB#6:
	movq	%rbp, (%r15)
	addq	$8, %r15
	testl	%ebp, %ebp
	je	.LBB17_22
# BB#7:
	addq	%r15, %rbx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB17_8:                               # =>This Inner Loop Header: Depth=1
	movq	$0, (%rax)
	movq	$0, 24(%rax)
	movl	$0, 36104(%rax)
	movl	$0, 36112(%rax)
	movl	$0, 36216(%rax)
	movl	$0, 36320(%rax)
	addq	$36688, %rax            # imm = 0x8F50
	cmpq	%rbx, %rax
	jne	.LBB17_8
# BB#9:                                 # %.loopexit51
	testl	%ebp, %ebp
	movq	%r15, 424(%r14)
	je	.LBB17_24
# BB#10:                                # %.lr.ph.preheader
	xorl	%r13d, %r13d
	jmp	.LBB17_12
	.p2align	4, 0x90
.LBB17_11:                              # %.thread..lr.ph_crit_edge
                                        #   in Loop: Header=BB17_12 Depth=1
	movq	424(%r14), %r15
	movzbl	580(%r14), %r12d
.LBB17_12:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, %eax
	imulq	$36688, %rax, %rbx      # imm = 0x8F50
	movq	%r14, 36088(%r15,%rbx)
	testb	%r12b, %r12b
	je	.LBB17_18
# BB#13:                                #   in Loop: Header=BB17_12 Depth=1
	leaq	36112(%r15,%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB17_23
# BB#14:                                #   in Loop: Header=BB17_12 Depth=1
	leaq	36216(%r15,%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB17_23
# BB#15:                                #   in Loop: Header=BB17_12 Depth=1
	leaq	36320(%r15,%rbx), %rdi
	callq	AutoResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB17_23
# BB#16:                                # %_ZN9NCompress6NBZip211CThreadInfo6CreateEv.exit
                                        #   in Loop: Header=BB17_12 Depth=1
	leaq	(%r15,%rbx), %rdx
	leaq	36096(%r15,%rbx), %rdi
	movl	$_ZN9NCompress6NBZip2L8MFThreadEPv, %esi
	callq	Thread_Create
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB17_23
# BB#17:                                # %_ZN9NCompress6NBZip211CThreadInfo6CreateEv.exit..thread_crit_edge
                                        #   in Loop: Header=BB17_12 Depth=1
	movl	576(%r14), %ebp
.LBB17_18:                              # %.thread
                                        #   in Loop: Header=BB17_12 Depth=1
	incl	%r13d
	cmpl	%ebp, %r13d
	jb	.LBB17_11
	jmp	.LBB17_24
.LBB17_20:                              # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit
	leaq	432(%r14), %rdi
	callq	ManualResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB17_25
	jmp	.LBB17_1
.LBB17_21:                              # %_ZN8NWindows16NSynchronization17CManualResetEvent18CreateIfNotCreatedEv.exit41
	leaq	592(%r14), %rdi
	callq	ManualResetEvent_CreateNotSignaled
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB17_25
	jmp	.LBB17_2
.LBB17_22:                              # %.loopexit51.thread
	movq	%r15, 424(%r14)
.LBB17_24:
	xorl	%ebp, %ebp
	jmp	.LBB17_25
.LBB17_23:                              # %_ZN9NCompress6NBZip211CThreadInfo6CreateEv.exit.thread
	movl	%r13d, 576(%r14)
	movq	%r14, %rdi
	callq	_ZN9NCompress6NBZip28CEncoder4FreeEv
.LBB17_25:                              # %.loopexit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_26:
.Ltmp104:
	movq	%rax, %rdi
	callq	__cxa_begin_catch
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB17_25
.Lfunc_end17:
	.size	_ZN9NCompress6NBZip28CEncoder6CreateEv, .Lfunc_end17-_ZN9NCompress6NBZip28CEncoder6CreateEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\242\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp100-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp103-.Ltmp100       #   Call between .Ltmp100 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin7  #     jumps to .Ltmp104
	.byte	1                       #   On action: 1
	.long	.Ltmp103-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end17-.Ltmp103   #   Call between .Ltmp103 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9NCompress6NBZip211CThreadInfoD2Ev,"axG",@progbits,_ZN9NCompress6NBZip211CThreadInfoD2Ev,comdat
	.weak	_ZN9NCompress6NBZip211CThreadInfoD2Ev
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfoD2Ev,@function
_ZN9NCompress6NBZip211CThreadInfoD2Ev:  # @_ZN9NCompress6NBZip211CThreadInfoD2Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
.Ltmp105:
	callq	BigFree
.Ltmp106:
# BB#1:                                 # %.noexc
	movq	$0, 24(%rbx)
	movq	(%rbx), %rdi
.Ltmp107:
	callq	MidFree
.Ltmp108:
# BB#2:
	movq	$0, (%rbx)
	leaq	36320(%rbx), %rdi
.Ltmp112:
	callq	Event_Close
.Ltmp113:
# BB#3:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit
	leaq	36216(%rbx), %rdi
.Ltmp117:
	callq	Event_Close
.Ltmp118:
# BB#4:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit8
	leaq	36112(%rbx), %rdi
.Ltmp122:
	callq	Event_Close
.Ltmp123:
# BB#5:                                 # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit10
	addq	$36096, %rbx            # imm = 0x8D00
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	Thread_Close            # TAILCALL
.LBB18_9:
.Ltmp124:
	movq	%rax, %r14
	jmp	.LBB18_12
.LBB18_10:
.Ltmp119:
	movq	%rax, %r14
	jmp	.LBB18_11
.LBB18_7:
.Ltmp114:
	movq	%rax, %r14
	jmp	.LBB18_8
.LBB18_6:
.Ltmp109:
	movq	%rax, %r14
	leaq	36320(%rbx), %rdi
.Ltmp110:
	callq	Event_Close
.Ltmp111:
.LBB18_8:                               # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit12
	leaq	36216(%rbx), %rdi
.Ltmp115:
	callq	Event_Close
.Ltmp116:
.LBB18_11:                              # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit14
	leaq	36112(%rbx), %rdi
.Ltmp120:
	callq	Event_Close
.Ltmp121:
.LBB18_12:                              # %_ZN8NWindows16NSynchronization10CBaseEventD2Ev.exit16
	addq	$36096, %rbx            # imm = 0x8D00
.Ltmp125:
	movq	%rbx, %rdi
	callq	Thread_Close
.Ltmp126:
# BB#13:                                # %_ZN8NWindows7CThreadD2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_14:
.Ltmp127:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN9NCompress6NBZip211CThreadInfoD2Ev, .Lfunc_end18-_ZN9NCompress6NBZip211CThreadInfoD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp105-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp108-.Ltmp105       #   Call between .Ltmp105 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin8  #     jumps to .Ltmp109
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin8  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp118-.Ltmp117       #   Call between .Ltmp117 and .Ltmp118
	.long	.Ltmp119-.Lfunc_begin8  #     jumps to .Ltmp119
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin8  #     jumps to .Ltmp124
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp110-.Ltmp123       #   Call between .Ltmp123 and .Ltmp110
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp110-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp126-.Ltmp110       #   Call between .Ltmp110 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin8  #     jumps to .Ltmp127
	.byte	1                       #   On action: 1
	.long	.Ltmp126-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Lfunc_end18-.Ltmp126   #   Call between .Ltmp126 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj,@function
_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj: # @_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj
	.cfi_startproc
# BB#0:
	movl	%edx, %r10d
	testl	%r10d, %r10d
	jle	.LBB19_5
# BB#1:                                 # %.lr.ph.i
	movq	32(%rdi), %r8
	movl	4(%r8), %r9d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %r10d
	movl	%r9d, %edx
	cmovlel	%r10d, %edx
	subl	%edx, %r10d
	movzbl	8(%r8), %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	movl	%esi, %eax
	movl	%r10d, %ecx
	shrl	%cl, %eax
	orl	%eax, %edi
	movb	%dil, 8(%r8)
	shll	%cl, %eax
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB19_4
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	16(%r8), %r9
	movl	(%r8), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r8)
	movb	%dil, (%r9,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB19_4:                               # %.backedge.i
                                        #   in Loop: Header=BB19_2 Depth=1
	subl	%eax, %esi
	testl	%r10d, %r10d
	jg	.LBB19_2
.LBB19_5:                               # %_ZN9NCompress6NBZip216CMsbfEncoderTemp9WriteBitsEji.exit
	retq
.Lfunc_end19:
	.size	_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj, .Lfunc_end19-_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh,@function
_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh: # @_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh
	.cfi_startproc
# BB#0:
	movq	32(%rdi), %r8
	movl	4(%r8), %r9d
	movl	$8, %r10d
	.p2align	4, 0x90
.LBB20_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %r10d
	movl	%r9d, %edx
	cmovlel	%r10d, %edx
	subl	%edx, %r10d
	movzbl	8(%r8), %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	movl	%esi, %eax
	movl	%r10d, %ecx
	shrl	%cl, %eax
	orl	%eax, %edi
	movb	%dil, 8(%r8)
	shll	%cl, %eax
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB20_3
# BB#2:                                 #   in Loop: Header=BB20_1 Depth=1
	movq	16(%r8), %r9
	movl	(%r8), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r8)
	movb	%dil, (%r9,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB20_3:                               # %.backedge.i.i
                                        #   in Loop: Header=BB20_1 Depth=1
	subl	%eax, %esi
	testl	%r10d, %r10d
	jg	.LBB20_1
# BB#4:                                 # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit
	retq
.Lfunc_end20:
	.size	_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh, .Lfunc_end20-_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb,@function
_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb: # @_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb
	.cfi_startproc
# BB#0:
	movzbl	%sil, %r10d
	movq	32(%rdi), %r8
	movl	4(%r8), %r9d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB21_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	movl	%r10d, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %edi
	movb	%dil, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB21_3
# BB#2:                                 #   in Loop: Header=BB21_1 Depth=1
	movq	16(%r8), %r9
	movl	(%r8), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r8)
	movb	%dil, (%r9,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB21_3:                               # %.backedge.i.i
                                        #   in Loop: Header=BB21_1 Depth=1
	subl	%esi, %r10d
	testl	%eax, %eax
	jg	.LBB21_1
# BB#4:                                 # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit
	retq
.Lfunc_end21:
	.size	_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb, .Lfunc_end21-_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo9WriteCrc2Ej
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo9WriteCrc2Ej,@function
_ZN9NCompress6NBZip211CThreadInfo9WriteCrc2Ej: # @_ZN9NCompress6NBZip211CThreadInfo9WriteCrc2Ej
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 24
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %rbp, -16
	movl	%esi, %r11d
	movl	%esi, %r10d
	shrl	$24, %r10d
	movq	32(%rdi), %r8
	movl	4(%r8), %r9d
	movl	$8, %eax
	.p2align	4, 0x90
.LBB22_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%r10d, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB22_3
# BB#2:                                 #   in Loop: Header=BB22_1 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB22_3:                               # %.backedge.i.i.i
                                        #   in Loop: Header=BB22_1 Depth=1
	subl	%esi, %r10d
	testl	%eax, %eax
	jg	.LBB22_1
# BB#4:                                 # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit
	movl	%r11d, %eax
	shrl	$16, %eax
	movzbl	%al, %r10d
	movq	32(%rdi), %r8
	movl	4(%r8), %r9d
	movl	$8, %eax
	.p2align	4, 0x90
.LBB22_5:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%r10d, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB22_7
# BB#6:                                 #   in Loop: Header=BB22_5 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB22_7:                               # %.backedge.i.i.i.1
                                        #   in Loop: Header=BB22_5 Depth=1
	subl	%esi, %r10d
	testl	%eax, %eax
	jg	.LBB22_5
# BB#8:                                 # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit.1
	movl	%r11d, %eax
	movzbl	%ah, %ebp  # NOREX
	movq	32(%rdi), %r8
	movl	4(%r8), %r9d
	movl	$8, %eax
	.p2align	4, 0x90
.LBB22_9:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%ebp, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB22_11
# BB#10:                                #   in Loop: Header=BB22_9 Depth=1
	movq	16(%r8), %r9
	movl	(%r8), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r8)
	movb	%bl, (%r9,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB22_11:                              # %.backedge.i.i.i.2
                                        #   in Loop: Header=BB22_9 Depth=1
	subl	%esi, %ebp
	testl	%eax, %eax
	jg	.LBB22_9
# BB#12:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit.2
	movzbl	%r11b, %ebx
	movq	32(%rdi), %r8
	movl	4(%r8), %edi
	movl	$8, %eax
	.p2align	4, 0x90
.LBB22_13:                              # =>This Inner Loop Header: Depth=1
	cmpl	%edi, %eax
	movl	%edi, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebp
	movl	%edx, %ecx
	shll	%cl, %ebp
	movl	%ebx, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %ebp
	movb	%bpl, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %edi
	movl	%edi, 4(%r8)
	jne	.LBB22_15
# BB#14:                                #   in Loop: Header=BB22_13 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %edi
	movl	%edi, (%r8)
	movb	%bpl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %edi
.LBB22_15:                              # %.backedge.i.i.i.3
                                        #   in Loop: Header=BB22_13 Depth=1
	subl	%esi, %ebx
	testl	%eax, %eax
	jg	.LBB22_13
# BB#16:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit.3
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end22:
	.size	_ZN9NCompress6NBZip211CThreadInfo9WriteCrc2Ej, .Lfunc_end22-_ZN9NCompress6NBZip211CThreadInfo9WriteCrc2Ej
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj,@function
_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj: # @_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 48
.Lcfi118:
	.cfi_offset %rbx, -48
.Lcfi119:
	.cfi_offset %r12, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	testl	%edx, %edx
	je	.LBB23_7
# BB#1:                                 # %.lr.ph.i
	leaq	352(%rbx), %r14
	movl	408(%rbx), %ecx
	.p2align	4, 0x90
.LBB23_2:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	subl	%ecx, %ebp
	jb	.LBB23_3
# BB#4:                                 #   in Loop: Header=BB23_2 Depth=1
	movl	%r15d, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r12d
	shll	%cl, %r12d
	orl	412(%rbx), %eax
	movq	352(%rbx), %rcx
	movl	360(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	360(%rbx), %eax
	cmpl	364(%rbx), %eax
	jne	.LBB23_6
# BB#5:                                 #   in Loop: Header=BB23_2 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB23_6:                               # %_ZN10COutBuffer9WriteByteEh.exit.i
                                        #   in Loop: Header=BB23_2 Depth=1
	subl	%r12d, %r15d
	movl	$8, 408(%rbx)
	movb	$0, 412(%rbx)
	movl	$8, %ecx
	testl	%ebp, %ebp
	movl	%ebp, %edx
	jne	.LBB23_2
	jmp	.LBB23_7
.LBB23_3:
	movzbl	%r15b, %eax
	subl	%edx, %ecx
	movl	%ecx, 408(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	412(%rbx), %eax
	movb	%al, 412(%rbx)
.LBB23_7:                               # %_ZN12CBitmEncoderI10COutBufferE9WriteBitsEjj.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj, .Lfunc_end23-_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoder9WriteByteEh
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder9WriteByteEh,@function
_ZN9NCompress6NBZip28CEncoder9WriteByteEh: # @_ZN9NCompress6NBZip28CEncoder9WriteByteEh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 48
.Lcfi128:
	.cfi_offset %rbx, -48
.Lcfi129:
	.cfi_offset %r12, -40
.Lcfi130:
	.cfi_offset %r14, -32
.Lcfi131:
	.cfi_offset %r15, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %rbx
	leaq	352(%rbx), %r14
	movl	408(%rbx), %ecx
	movl	$8, %eax
	.p2align	4, 0x90
.LBB24_1:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	subl	%ecx, %ebp
	jb	.LBB24_2
# BB#3:                                 #   in Loop: Header=BB24_1 Depth=1
	movl	%r15d, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r12d
	shll	%cl, %r12d
	orl	412(%rbx), %eax
	movq	352(%rbx), %rcx
	movl	360(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	360(%rbx), %eax
	cmpl	364(%rbx), %eax
	jne	.LBB24_5
# BB#4:                                 #   in Loop: Header=BB24_1 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB24_5:                               # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB24_1 Depth=1
	subl	%r12d, %r15d
	movl	$8, 408(%rbx)
	movb	$0, 412(%rbx)
	movl	$8, %ecx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB24_1
	jmp	.LBB24_6
.LBB24_2:
	movzbl	%r15b, %edx
	subl	%eax, %ecx
	movl	%ecx, 408(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	412(%rbx), %edx
	movb	%dl, 412(%rbx)
.LBB24_6:                               # %_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	_ZN9NCompress6NBZip28CEncoder9WriteByteEh, .Lfunc_end24-_ZN9NCompress6NBZip28CEncoder9WriteByteEh
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoder8WriteBitEb
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder8WriteBitEb,@function
_ZN9NCompress6NBZip28CEncoder8WriteBitEb: # @_ZN9NCompress6NBZip28CEncoder8WriteBitEb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 48
.Lcfi138:
	.cfi_offset %rbx, -48
.Lcfi139:
	.cfi_offset %r12, -40
.Lcfi140:
	.cfi_offset %r14, -32
.Lcfi141:
	.cfi_offset %r15, -24
.Lcfi142:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzbl	%sil, %r15d
	leaq	352(%rbx), %r14
	movl	408(%rbx), %ecx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB25_1:                               # =>This Inner Loop Header: Depth=1
	movl	%eax, %ebp
	subl	%ecx, %ebp
	jb	.LBB25_2
# BB#3:                                 #   in Loop: Header=BB25_1 Depth=1
	movl	%r15d, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r12d
	shll	%cl, %r12d
	orl	412(%rbx), %eax
	movq	352(%rbx), %rcx
	movl	360(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	360(%rbx), %eax
	cmpl	364(%rbx), %eax
	jne	.LBB25_5
# BB#4:                                 #   in Loop: Header=BB25_1 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB25_5:                               # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB25_1 Depth=1
	subl	%r12d, %r15d
	movl	$8, 408(%rbx)
	movb	$0, 412(%rbx)
	movl	$8, %ecx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB25_1
	jmp	.LBB25_6
.LBB25_2:
	movzbl	%r15b, %edx
	subl	%eax, %ecx
	movl	%ecx, 408(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	412(%rbx), %edx
	movb	%dl, 412(%rbx)
.LBB25_6:                               # %_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZN9NCompress6NBZip28CEncoder8WriteBitEb, .Lfunc_end25-_ZN9NCompress6NBZip28CEncoder8WriteBitEb
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoder8WriteCrcEj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder8WriteCrcEj,@function
_ZN9NCompress6NBZip28CEncoder8WriteCrcEj: # @_ZN9NCompress6NBZip28CEncoder8WriteCrcEj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi149:
	.cfi_def_cfa_offset 64
.Lcfi150:
	.cfi_offset %rbx, -56
.Lcfi151:
	.cfi_offset %r12, -48
.Lcfi152:
	.cfi_offset %r13, -40
.Lcfi153:
	.cfi_offset %r14, -32
.Lcfi154:
	.cfi_offset %r15, -24
.Lcfi155:
	.cfi_offset %rbp, -16
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movq	%rdi, %r13
	leaq	352(%r13), %r15
	movl	408(%r13), %eax
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB26_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_2 Depth 2
	leal	(,%r12,8), %edx
	movl	$24, %ecx
	subl	%edx, %ecx
	movl	4(%rsp), %edx           # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %edx
	movzbl	%dl, %ebx
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB26_2:                               #   Parent Loop BB26_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB26_3
# BB#4:                                 #   in Loop: Header=BB26_2 Depth=2
	movl	%ebx, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r14d
	shll	%cl, %r14d
	orl	412(%r13), %eax
	movq	352(%r13), %rcx
	movl	360(%r13), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r13)
	movb	%al, (%rcx,%rdx)
	movl	360(%r13), %eax
	cmpl	364(%r13), %eax
	jne	.LBB26_6
# BB#5:                                 #   in Loop: Header=BB26_2 Depth=2
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB26_6:                               # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i
                                        #   in Loop: Header=BB26_2 Depth=2
	subl	%r14d, %ebx
	movl	$8, 408(%r13)
	movb	$0, 412(%r13)
	movl	$8, %eax
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB26_2
	jmp	.LBB26_7
	.p2align	4, 0x90
.LBB26_3:                               #   in Loop: Header=BB26_1 Depth=1
	movzbl	%bl, %edx
	subl	%ecx, %eax
	movl	%eax, 408(%r13)
	movl	%eax, %ecx
	shll	%cl, %edx
	orl	412(%r13), %edx
	movb	%dl, 412(%r13)
.LBB26_7:                               # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit
                                        #   in Loop: Header=BB26_1 Depth=1
	incl	%r12d
	cmpl	$4, %r12d
	jne	.LBB26_1
# BB#8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN9NCompress6NBZip28CEncoder8WriteCrcEj, .Lfunc_end26-_ZN9NCompress6NBZip28CEncoder8WriteCrcEj
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_0:
	.byte	0                       # 0x0
	.byte	1                       # 0x1
	.byte	2                       # 0x2
	.byte	3                       # 0x3
	.byte	4                       # 0x4
	.byte	5                       # 0x5
	.byte	6                       # 0x6
	.byte	7                       # 0x7
	.byte	8                       # 0x8
	.byte	9                       # 0x9
	.byte	10                      # 0xa
	.byte	11                      # 0xb
	.byte	12                      # 0xc
	.byte	13                      # 0xd
	.byte	14                      # 0xe
	.byte	15                      # 0xf
.LCPI27_1:
	.zero	16,16
.LCPI27_2:
	.zero	16,32
.LCPI27_3:
	.zero	16,48
.LCPI27_4:
	.zero	16,64
.LCPI27_5:
	.zero	16,80
.LCPI27_6:
	.zero	16,96
.LCPI27_7:
	.zero	16,112
.LCPI27_8:
	.zero	16,128
	.text
	.globl	_ZN9NCompress6NBZip211CThreadInfo11EncodeBlockEPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo11EncodeBlockEPKhj,@function
_ZN9NCompress6NBZip211CThreadInfo11EncodeBlockEPKhj: # @_ZN9NCompress6NBZip211CThreadInfo11EncodeBlockEPKhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi158:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi159:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi160:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 56
	subq	$1768, %rsp             # imm = 0x6E8
.Lcfi162:
	.cfi_def_cfa_offset 1824
.Lcfi163:
	.cfi_offset %rbx, -56
.Lcfi164:
	.cfi_offset %r12, -48
.Lcfi165:
	.cfi_offset %r13, -40
.Lcfi166:
	.cfi_offset %r14, -32
.Lcfi167:
	.cfi_offset %r15, -24
.Lcfi168:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movq	32(%r14), %r8
	movl	4(%r8), %r9d
	movl	$1, %eax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	movl	%ebp, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %esi
	movb	%sil, 8(%r8)
	shll	%cl, %edi
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB27_3
# BB#2:                                 #   in Loop: Header=BB27_1 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r8)
	movb	%sil, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB27_3:                               # %.backedge.i.i.i
                                        #   in Loop: Header=BB27_1 Depth=1
	subl	%edi, %ebp
	testl	%eax, %eax
	jg	.LBB27_1
# BB#4:                                 # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit
	movq	24(%r14), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%r15d, %edx
	callq	BlockSort
	movq	24(%r14), %rcx
	movl	%eax, %edx
	movl	%r15d, (%rcx,%rdx,4)
	movq	32(%r14), %r8
	movl	4(%r8), %r9d
	movl	$24, %edx
	.p2align	4, 0x90
.LBB27_5:                               # =>This Inner Loop Header: Depth=1
	cmpl	%r9d, %edx
	movl	%r9d, %esi
	cmovlel	%edx, %esi
	subl	%esi, %edx
	movzbl	8(%r8), %edi
	movl	%esi, %ecx
	shll	%cl, %edi
	movl	%eax, %ebp
	movl	%edx, %ecx
	shrl	%cl, %ebp
	orl	%ebp, %edi
	movb	%dil, 8(%r8)
	shll	%cl, %ebp
	subl	%esi, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB27_7
# BB#6:                                 #   in Loop: Header=BB27_5 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %esi
	leal	1(%rsi), %ebx
	movl	%ebx, (%r8)
	movb	%dil, (%rcx,%rsi)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB27_7:                               # %.backedge.i.i472
                                        #   in Loop: Header=BB27_5 Depth=1
	subl	%ebp, %eax
	testl	%edx, %edx
	jg	.LBB27_5
# BB#8:                                 # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit473
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 720(%rsp)
	movdqa	%xmm0, 704(%rsp)
	movdqa	%xmm0, 688(%rsp)
	movdqa	%xmm0, 672(%rsp)
	movdqa	%xmm0, 656(%rsp)
	movdqa	%xmm0, 640(%rsp)
	movdqa	%xmm0, 624(%rsp)
	movdqa	%xmm0, 608(%rsp)
	movdqa	%xmm0, 592(%rsp)
	movdqa	%xmm0, 576(%rsp)
	movdqa	%xmm0, 560(%rsp)
	movdqa	%xmm0, 544(%rsp)
	movdqa	%xmm0, 528(%rsp)
	movdqa	%xmm0, 512(%rsp)
	movdqa	%xmm0, 496(%rsp)
	movdqa	%xmm0, 480(%rsp)
	movdqa	%xmm0, 272(%rsp)
	testl	%r15d, %r15d
	je	.LBB27_15
# BB#9:                                 # %.lr.ph560.preheader
	movl	%r15d, %eax
	leaq	-1(%rax), %rcx
	movq	%rax, %rsi
	xorl	%edx, %edx
	andq	$3, %rsi
	je	.LBB27_12
# BB#10:                                # %.lr.ph560.prol.preheader
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_11:                              # %.lr.ph560.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp,%rdx), %edi
	movb	$1, 480(%rsp,%rdi)
	incq	%rdx
	cmpq	%rdx, %rsi
	jne	.LBB27_11
.LBB27_12:                              # %.lr.ph560.prol.loopexit
	cmpq	$3, %rcx
	jb	.LBB27_15
# BB#13:                                # %.lr.ph560.preheader.new
	subq	%rdx, %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	3(%rcx,%rdx), %rcx
	.p2align	4, 0x90
.LBB27_14:                              # %.lr.ph560
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-3(%rcx), %edx
	movb	$1, 480(%rsp,%rdx)
	movzbl	-2(%rcx), %edx
	movb	$1, 480(%rsp,%rdx)
	movzbl	-1(%rcx), %edx
	movb	$1, 480(%rsp,%rdx)
	movzbl	(%rcx), %edx
	movb	$1, 480(%rsp,%rdx)
	addq	$4, %rcx
	addq	$-4, %rax
	jne	.LBB27_14
.LBB27_15:                              # %.preheader502.preheader
	xorl	%eax, %eax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB27_16:                              # %.preheader502
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, 480(%rsp,%rax)
	je	.LBB27_18
# BB#17:                                #   in Loop: Header=BB27_16 Depth=1
	movl	%eax, %ecx
	shrl	$4, %ecx
	movb	$1, 272(%rsp,%rcx)
	movslq	%r13d, %rcx
	incl	%r13d
	movb	%al, 1512(%rsp,%rcx)
.LBB27_18:                              # %.preheader502.1759
                                        #   in Loop: Header=BB27_16 Depth=1
	cmpb	$0, 481(%rsp,%rax)
	je	.LBB27_20
# BB#19:                                #   in Loop: Header=BB27_16 Depth=1
	movl	%eax, %ecx
	shrl	$4, %ecx
	movb	$1, 272(%rsp,%rcx)
	leal	1(%rax), %ecx
	movslq	%r13d, %rdx
	incl	%r13d
	movb	%cl, 1512(%rsp,%rdx)
.LBB27_20:                              #   in Loop: Header=BB27_16 Depth=1
	addq	$2, %rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB27_16
# BB#21:                                # %.preheader501.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB27_22:                              # %.preheader501
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_23 Depth 2
	movzbl	272(%rsp,%r8), %esi
	movq	32(%r14), %r9
	movl	4(%r9), %r10d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB27_23:                              #   Parent Loop BB27_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r10d, %eax
	movl	%r10d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r9), %ebp
	movl	%edx, %ecx
	shll	%cl, %ebp
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebp
	movb	%bpl, 8(%r9)
	shll	%cl, %edi
	subl	%edx, %r10d
	movl	%r10d, 4(%r9)
	jne	.LBB27_25
# BB#24:                                #   in Loop: Header=BB27_23 Depth=2
	movq	16(%r9), %rcx
	movl	(%r9), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r9)
	movb	%bpl, (%rcx,%rdx)
	movl	$8, 4(%r9)
	movl	$8, %r10d
.LBB27_25:                              # %.backedge.i.i.i482
                                        #   in Loop: Header=BB27_23 Depth=2
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB27_23
# BB#26:                                # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit483
                                        #   in Loop: Header=BB27_22 Depth=1
	incq	%r8
	cmpq	$16, %r8
	jne	.LBB27_22
# BB#27:                                # %.preheader500.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB27_28:                              # %.preheader500
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_30 Depth 2
	movl	%r8d, %eax
	shrl	$4, %eax
	cmpb	$0, 272(%rsp,%rax)
	je	.LBB27_33
# BB#29:                                #   in Loop: Header=BB27_28 Depth=1
	movzbl	480(%rsp,%r8), %esi
	movq	32(%r14), %r9
	movl	4(%r9), %r10d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB27_30:                              #   Parent Loop BB27_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r10d, %eax
	movl	%r10d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r9), %ebp
	movl	%edx, %ecx
	shll	%cl, %ebp
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebp
	movb	%bpl, 8(%r9)
	shll	%cl, %edi
	subl	%edx, %r10d
	movl	%r10d, 4(%r9)
	jne	.LBB27_32
# BB#31:                                #   in Loop: Header=BB27_30 Depth=2
	movq	16(%r9), %rcx
	movl	(%r9), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r9)
	movb	%bpl, (%rcx,%rdx)
	movl	$8, 4(%r9)
	movl	$8, %r10d
.LBB27_32:                              # %.backedge.i.i.i477
                                        #   in Loop: Header=BB27_30 Depth=2
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB27_30
.LBB27_33:                              # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit478
                                        #   in Loop: Header=BB27_28 Depth=1
	incq	%r8
	cmpq	$256, %r8               # imm = 0x100
	jne	.LBB27_28
# BB#34:
	movq	8(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	480(%rsp), %rdi
	xorl	%r12d, %r12d
	xorl	%esi, %esi
	movl	$1032, %edx             # imm = 0x408
	callq	memset
	leal	2(%r13), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	24(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_35:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_37 Depth 2
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r12,4), %eax
	movzbl	-1(%r15,%rax), %esi
	leaq	1512(%rsp), %rdi
	callq	_ZN9NCompress12CMtf8Encoder11FindAndMoveEh
	testl	%eax, %eax
	je	.LBB27_40
# BB#36:                                # %.preheader499
                                        #   in Loop: Header=BB27_35 Depth=1
	testl	%ebp, %ebp
	movq	24(%rsp), %rsi          # 8-byte Reload
	je	.LBB27_38
	.p2align	4, 0x90
.LBB27_37:                              # %.lr.ph550
                                        #   Parent Loop BB27_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ebp
	movl	%ebp, %ecx
	andl	$1, %ecx
	movl	%ebx, %edx
	incl	%ebx
	movb	%cl, (%rsi,%rdx)
	incl	480(%rsp,%rcx,4)
	shrl	%ebp
	jne	.LBB27_37
.LBB27_38:                              # %._crit_edge551
                                        #   in Loop: Header=BB27_35 Depth=1
	movl	$1, %ecx
	cmpl	$254, %eax
	jl	.LBB27_41
# BB#39:                                #   in Loop: Header=BB27_35 Depth=1
	movl	%ebx, %ecx
	incl	%ebx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movb	$-1, (%rsi,%rcx)
	movl	$-254, %ecx
	jmp	.LBB27_42
	.p2align	4, 0x90
.LBB27_40:                              #   in Loop: Header=BB27_35 Depth=1
	incl	%ebp
	jmp	.LBB27_43
	.p2align	4, 0x90
.LBB27_41:                              #   in Loop: Header=BB27_35 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
.LBB27_42:                              #   in Loop: Header=BB27_35 Depth=1
	addl	%eax, %ecx
	movl	%ebx, %edx
	incl	%ebx
	movb	%cl, (%rsi,%rdx)
	cltq
	incl	484(%rsp,%rax,4)
	xorl	%ebp, %ebp
.LBB27_43:                              #   in Loop: Header=BB27_35 Depth=1
	incq	%r12
	cmpq	48(%rsp), %r12          # 8-byte Folded Reload
	jb	.LBB27_35
# BB#44:                                # %.preheader498
	testl	%ebp, %ebp
	je	.LBB27_47
# BB#45:                                # %.lr.ph545.preheader
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	32(%rsp), %r10d         # 4-byte Reload
	.p2align	4, 0x90
.LBB27_46:                              # %.lr.ph545
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebp
	movl	%ebp, %eax
	andl	$1, %eax
	movl	%ebx, %ecx
	incl	%ebx
	movb	%al, (%rdx,%rcx)
	incl	480(%rsp,%rax,4)
	shrl	%ebp
	jne	.LBB27_46
	jmp	.LBB27_48
.LBB27_47:
	movl	32(%rsp), %r10d         # 4-byte Reload
.LBB27_48:                              # %._crit_edge546
	movl	$1, %esi
	cmpl	$256, %r10d             # imm = 0x100
	movl	%ebx, %eax
	jl	.LBB27_50
# BB#49:
	leal	1(%rbx), %eax
	movl	%ebx, %ecx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movb	$-1, (%rdx,%rcx)
	movl	$2, %esi
.LBB27_50:                              # %min.iters.checked
	leal	(%rsi,%r13), %ecx
	movl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
	movb	%cl, (%rdx,%rax)
	movslq	%r13d, %rax
	incl	484(%rsp,%rax,4)
	pxor	%xmm0, %xmm0
	xorl	%eax, %eax
	pxor	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB27_51:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	paddd	480(%rsp,%rax,4), %xmm0
	paddd	496(%rsp,%rax,4), %xmm1
	paddd	512(%rsp,%rax,4), %xmm0
	paddd	528(%rsp,%rax,4), %xmm1
	paddd	544(%rsp,%rax,4), %xmm0
	paddd	560(%rsp,%rax,4), %xmm1
	paddd	576(%rsp,%rax,4), %xmm0
	paddd	592(%rsp,%rax,4), %xmm1
	addq	$32, %rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB27_51
# BB#52:                                # %.loopexit739758
	paddd	%xmm0, %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm0
	movabsq	$9223372036854775776, %r8 # imm = 0x7FFFFFFFFFFFFFE0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %ecx
	addl	1504(%rsp), %ecx
	movl	1508(%rsp), %edx
	leal	(%rdx,%rcx), %ebp
	addl	%ebx, %esi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	32(%r14), %r9
	movl	(%r9), %eax
	movl	4(%r9), %ebx
	movl	$8, %edi
	movl	$8, %esi
	subl	%ebx, %esi
	leal	(%rsi,%rax,8), %esi
	movb	8(%r9), %al
	movb	%al, 15(%rsp)           # 1-byte Spill
	xorl	%eax, %eax
	cmpl	$2399, %ebp             # imm = 0x95F
	seta	%al
	addl	$5, %eax
	leal	49(%rdx,%rcx), %ecx
	imulq	$1374389535, %rcx, %rcx # imm = 0x51EB851F
	shrq	$36, %rcx
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	$8, %ecx
	subl	%esi, %ecx
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	movl	%esi, %ecx
	shrl	$3, %ecx
	movl	%ecx, 76(%rsp)          # 4-byte Spill
	andl	$7, %esi
	subl	%esi, %edi
	movl	%edi, 80(%rsp)          # 4-byte Spill
	xorl	%ecx, %ecx
	cmpl	$199, %ebp
	seta	%cl
	orl	$2, %ecx
	cmpl	$1200, %ebp             # imm = 0x4B0
	movl	$4, %edx
	cmovael	%eax, %edx
	movl	%ebp, 88(%rsp)          # 4-byte Spill
	cmpl	$600, %ebp              # imm = 0x258
	movslq	%r10d, %rax
	cmovbl	%ecx, %edx
	movl	%edx, 72(%rsp)          # 4-byte Spill
	testq	%rax, %rax
	movl	$1, %r15d
	movq	%rax, 48(%rsp)          # 8-byte Spill
	cmovgq	%rax, %r15
	cmpb	$0, 36080(%r14)
	leaq	1588(%r14), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	sete	%al
	leaq	24(%r8), %rcx
	andq	%r15, %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	leaq	-218(%r14), %rcx
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	movl	$-1, 60(%rsp)           # 4-byte Folded Spill
	movl	$2, 56(%rsp)            # 4-byte Folded Spill
	movl	$2, %edx
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r15, 136(%rsp)         # 8-byte Spill
	jmp	.LBB27_53
.LBB27_55:                              #   in Loop: Header=BB27_53 Depth=1
	xorl	%edx, %edx
	movdqa	%xmm0, %xmm3
	movdqa	.LCPI27_0(%rip), %xmm0  # xmm0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movdqa	.LCPI27_2(%rip), %xmm4  # xmm4 = [32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32]
.LBB27_56:                              # %vector.body690.prol.loopexit
                                        #   in Loop: Header=BB27_53 Depth=1
	cmpq	$96, %rsi
	movdqa	.LCPI27_3(%rip), %xmm5  # xmm5 = [48,48,48,48,48,48,48,48,48,48,48,48,48,48,48,48]
	movdqa	%xmm5, %xmm8
	movdqa	.LCPI27_4(%rip), %xmm6  # xmm6 = [64,64,64,64,64,64,64,64,64,64,64,64,64,64,64,64]
	movdqa	%xmm6, %xmm9
	movdqa	.LCPI27_5(%rip), %xmm7  # xmm7 = [80,80,80,80,80,80,80,80,80,80,80,80,80,80,80,80]
	movdqa	%xmm7, %xmm10
	movdqa	.LCPI27_6(%rip), %xmm5  # xmm5 = [96,96,96,96,96,96,96,96,96,96,96,96,96,96,96,96]
	movdqa	.LCPI27_7(%rip), %xmm6  # xmm6 = [112,112,112,112,112,112,112,112,112,112,112,112,112,112,112,112]
	movdqa	.LCPI27_8(%rip), %xmm7  # xmm7 = [128,128,128,128,128,128,128,128,128,128,128,128,128,128,128,128]
	jb	.LBB27_59
# BB#57:                                # %vector.body690.preheader.new
                                        #   in Loop: Header=BB27_53 Depth=1
	movq	%rax, %rsi
	subq	%rdx, %rsi
	leaq	384(%rsp), %rdi
	addq	%rdi, %rdx
	.p2align	4, 0x90
.LBB27_58:                              # %vector.body690
                                        #   Parent Loop BB27_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm0, %xmm1
	paddb	%xmm3, %xmm1
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm1, -96(%rdx)
	movdqa	%xmm0, %xmm1
	paddb	%xmm4, %xmm1
	movdqa	%xmm0, %xmm2
	paddb	%xmm8, %xmm2
	movdqu	%xmm1, -80(%rdx)
	movdqu	%xmm2, -64(%rdx)
	movdqa	%xmm0, %xmm1
	paddb	%xmm9, %xmm1
	movdqa	%xmm0, %xmm2
	paddb	%xmm10, %xmm2
	movdqu	%xmm1, -48(%rdx)
	movdqu	%xmm2, -32(%rdx)
	movdqa	%xmm0, %xmm1
	paddb	%xmm5, %xmm1
	movdqa	%xmm0, %xmm2
	paddb	%xmm6, %xmm2
	movdqu	%xmm1, -16(%rdx)
	movdqu	%xmm2, (%rdx)
	pxor	%xmm7, %xmm0
	subq	$-128, %rdx
	addq	$-128, %rsi
	jne	.LBB27_58
.LBB27_59:                              # %middle.block691
                                        #   in Loop: Header=BB27_53 Depth=1
	cmpq	%rax, %rcx
	jne	.LBB27_135
	jmp	.LBB27_136
	.p2align	4, 0x90
.LBB27_53:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_62 Depth 2
                                        #     Child Loop BB27_66 Depth 2
                                        #     Child Loop BB27_70 Depth 2
                                        #       Child Loop BB27_72 Depth 3
                                        #       Child Loop BB27_81 Depth 3
                                        #     Child Loop BB27_84 Depth 2
                                        #       Child Loop BB27_85 Depth 3
                                        #         Child Loop BB27_86 Depth 4
                                        #         Child Loop BB27_92 Depth 4
                                        #           Child Loop BB27_93 Depth 5
                                        #         Child Loop BB27_98 Depth 4
                                        #       Child Loop BB27_101 Depth 3
                                        #         Child Loop BB27_105 Depth 4
                                        #     Child Loop BB27_133 Depth 2
                                        #     Child Loop BB27_58 Depth 2
                                        #     Child Loop BB27_135 Depth 2
                                        #     Child Loop BB27_137 Depth 2
                                        #       Child Loop BB27_140 Depth 3
                                        #         Child Loop BB27_141 Depth 4
                                        #       Child Loop BB27_147 Depth 3
                                        #       Child Loop BB27_152 Depth 3
                                        #     Child Loop BB27_155 Depth 2
                                        #       Child Loop BB27_156 Depth 3
                                        #       Child Loop BB27_160 Depth 3
                                        #         Child Loop BB27_161 Depth 4
                                        #           Child Loop BB27_162 Depth 5
                                        #           Child Loop BB27_172 Depth 5
                                        #           Child Loop BB27_167 Depth 5
                                        #         Child Loop BB27_179 Depth 4
                                        #     Child Loop BB27_185 Depth 2
                                        #       Child Loop BB27_191 Depth 3
	testb	$1, %al
	movl	%edx, 92(%rsp)          # 4-byte Spill
	je	.LBB27_60
# BB#54:                                #   in Loop: Header=BB27_53 Depth=1
	leaq	4(%r9), %r10
	leaq	8(%r9), %r11
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r8d
	jmp	.LBB27_61
	.p2align	4, 0x90
.LBB27_60:                              #   in Loop: Header=BB27_53 Depth=1
	movl	76(%rsp), %eax          # 4-byte Reload
	movl	%eax, (%r9)
	leaq	4(%r9), %r10
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, 4(%r9)
	leaq	8(%r9), %r11
	movb	15(%rsp), %cl           # 1-byte Reload
	movb	%cl, 8(%r9)
	cmpl	$7, %edx
	movl	56(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %r8d
	cmovll	%edx, %r8d
	movl	%eax, %ebx
.LBB27_61:                              #   in Loop: Header=BB27_53 Depth=1
	movl	$3, %eax
	movl	%r8d, %esi
	.p2align	4, 0x90
.LBB27_62:                              #   Parent Loop BB27_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ebx, %eax
	movl	%ebx, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	(%r11), %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	movl	%esi, %ebp
	movl	%eax, %ecx
	shrl	%cl, %ebp
	orl	%ebp, %edi
	movb	%dil, (%r11)
	shll	%cl, %ebp
	subl	%edx, %ebx
	movl	%ebx, (%r10)
	jne	.LBB27_64
# BB#63:                                #   in Loop: Header=BB27_62 Depth=2
	movq	16(%r9), %rcx
	movl	(%r9), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r9)
	movb	%dil, (%rcx,%rdx)
	movl	$8, 4(%r9)
	movl	$8, %ebx
.LBB27_64:                              # %.backedge.i.i467
                                        #   in Loop: Header=BB27_62 Depth=2
	subl	%ebp, %esi
	testl	%eax, %eax
	jg	.LBB27_62
# BB#65:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit468
                                        #   in Loop: Header=BB27_53 Depth=1
	movq	32(%r14), %r9
	movl	4(%r9), %r10d
	movl	$15, %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %ebp
	.p2align	4, 0x90
.LBB27_66:                              #   Parent Loop BB27_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%r10d, %eax
	movl	%r10d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r9), %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	movl	%ebp, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %esi
	movb	%sil, 8(%r9)
	shll	%cl, %edi
	subl	%edx, %r10d
	movl	%r10d, 4(%r9)
	jne	.LBB27_68
# BB#67:                                #   in Loop: Header=BB27_66 Depth=2
	movq	16(%r9), %rcx
	movl	(%r9), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r9)
	movb	%sil, (%rcx,%rdx)
	movl	$8, 4(%r9)
	movl	$8, %r10d
.LBB27_68:                              # %.backedge.i.i462
                                        #   in Loop: Header=BB27_66 Depth=2
	subl	%edi, %ebp
	testl	%eax, %eax
	jg	.LBB27_66
# BB#69:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit463.preheader
                                        #   in Loop: Header=BB27_53 Depth=1
	movslq	%r8d, %r9
	movq	%r9, 40(%rsp)           # 8-byte Spill
	imulq	$258, %r9, %r10         # imm = 0x102
	addq	128(%rsp), %r10         # 8-byte Folded Reload
	xorl	%ebp, %ebp
	movl	88(%rsp), %r15d         # 4-byte Reload
	jmp	.LBB27_70
.LBB27_79:                              #   in Loop: Header=BB27_70 Depth=2
	movslq	%r11d, %rsi
	subl	480(%rsp,%rsi,4), %edx
	jmp	.LBB27_80
	.p2align	4, 0x90
.LBB27_70:                              # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit463
                                        #   Parent Loop BB27_53 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_72 Depth 3
                                        #       Child Loop BB27_81 Depth 3
	xorl	%edx, %edx
	movl	%r15d, %eax
	divl	%r9d
	cmpl	%r15d, %r9d
	movl	%ebp, %edi
	movl	$0, %edx
	ja	.LBB27_73
# BB#71:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB27_70 Depth=2
	movslq	%ebp, %rdx
	leaq	480(%rsp,%rdx,4), %rbx
	xorl	%edx, %edx
	movl	%ebp, %edi
	.p2align	4, 0x90
.LBB27_72:                              # %.lr.ph
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addl	(%rbx), %edx
	addq	$4, %rbx
	incl	%edi
	cmpl	%eax, %edx
	jb	.LBB27_72
.LBB27_73:                              # %._crit_edge
                                        #   in Loop: Header=BB27_70 Depth=2
	leal	-1(%rdi), %r11d
	cmpl	%ebp, %r11d
	jle	.LBB27_78
# BB#74:                                #   in Loop: Header=BB27_70 Depth=2
	cmpl	%r9d, %r8d
	je	.LBB27_78
# BB#75:                                #   in Loop: Header=BB27_70 Depth=2
	cmpl	$1, %r9d
	je	.LBB27_78
# BB#76:                                #   in Loop: Header=BB27_70 Depth=2
	movl	%r8d, %esi
	subl	%r9d, %esi
	testb	$1, %sil
	jne	.LBB27_79
	.p2align	4, 0x90
.LBB27_78:                              #   in Loop: Header=BB27_70 Depth=2
	movl	%edi, %r11d
.LBB27_80:                              #   in Loop: Header=BB27_70 Depth=2
	decq	%r9
	movslq	%ebp, %r12
	movslq	%r11d, %rbp
	movq	%r10, %rsi
	xorl	%ebx, %ebx
	movq	48(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_81:                              #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_70 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, %rbx
	setge	%al
	cmpq	%r12, %rbx
	setl	%cl
	orb	%al, %cl
	movb	%cl, (%rsi)
	incq	%rbx
	incq	%rsi
	cmpq	%rdi, %rbx
	jl	.LBB27_81
# BB#82:                                #   in Loop: Header=BB27_70 Depth=2
	subl	%edx, %r15d
	addq	$-258, %r10             # imm = 0xFEFE
	testl	%r9d, %r9d
	movl	%r11d, %ebp
	movq	40(%rsp), %r12          # 8-byte Reload
	jne	.LBB27_70
# BB#83:                                # %.preheader494.preheader
                                        #   in Loop: Header=BB27_53 Depth=1
	testl	%r8d, %r8d
	movl	$1, %eax
	cmovlel	%eax, %r8d
	decl	%r8d
	imulq	$1032, %r8, %rax        # imm = 0x408
	addq	$1032, %rax             # imm = 0x408
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	136(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB27_84:                              # %.preheader494
                                        #   Parent Loop BB27_53 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_85 Depth 3
                                        #         Child Loop BB27_86 Depth 4
                                        #         Child Loop BB27_92 Depth 4
                                        #           Child Loop BB27_93 Depth 5
                                        #         Child Loop BB27_98 Depth 4
                                        #       Child Loop BB27_101 Depth 3
                                        #         Child Loop BB27_105 Depth 4
	movl	%eax, 16(%rsp)          # 4-byte Spill
	xorl	%esi, %esi
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	callq	memset
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	112(%rsp), %r11         # 8-byte Reload
	.p2align	4, 0x90
.LBB27_85:                              # %.preheader493
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_84 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB27_86 Depth 4
                                        #         Child Loop BB27_92 Depth 4
                                        #           Child Loop BB27_93 Depth 5
                                        #         Child Loop BB27_98 Depth 4
	movl	$1, %edx
	.p2align	4, 0x90
.LBB27_86:                              #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_84 Depth=2
                                        #       Parent Loop BB27_85 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leal	1(%rcx), %esi
	movl	%ecx, %eax
	movzbl	(%r13,%rax), %eax
	cmpl	$255, %eax
	je	.LBB27_88
# BB#87:                                #   in Loop: Header=BB27_86 Depth=4
	movl	%esi, %ecx
	jmp	.LBB27_89
	.p2align	4, 0x90
.LBB27_88:                              #   in Loop: Header=BB27_86 Depth=4
	addl	$2, %ecx
	movl	%esi, %esi
	movzbl	(%r13,%rsi), %esi
	addl	%esi, %eax
.LBB27_89:                              #   in Loop: Header=BB27_86 Depth=4
	movl	%eax, 268(%rsp,%rdx,4)
	cmpq	$49, %rdx
	leaq	1(%rdx), %rdx
	jg	.LBB27_91
# BB#90:                                #   in Loop: Header=BB27_86 Depth=4
	cmpl	%r11d, %ecx
	jb	.LBB27_86
.LBB27_91:                              # %.critedge.preheader
                                        #   in Loop: Header=BB27_85 Depth=3
	movl	%r8d, %r9d
	addq	$-2, %rdx
	movl	$-1, %r10d
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB27_92:                              # %.critedge
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_84 Depth=2
                                        #       Parent Loop BB27_85 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB27_93 Depth 5
	xorl	%ebp, %ebp
	movq	$-1, %rax
	.p2align	4, 0x90
.LBB27_93:                              #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_84 Depth=2
                                        #       Parent Loop BB27_85 Depth=3
                                        #         Parent Loop BB27_92 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	276(%rsp,%rax,4), %esi
	imulq	$258, %rdi, %rbx        # imm = 0x102
	addq	%r14, %rbx
	movzbl	40(%rsi,%rbx), %esi
	addl	%esi, %ebp
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB27_93
# BB#94:                                #   in Loop: Header=BB27_92 Depth=4
	cmpl	%r10d, %ebp
	jae	.LBB27_96
# BB#95:                                #   in Loop: Header=BB27_92 Depth=4
	movb	%dil, 13972(%r14,%r9)
	movl	%ebp, %r10d
.LBB27_96:                              #   in Loop: Header=BB27_92 Depth=4
	incq	%rdi
	cmpq	%r12, %rdi
	jl	.LBB27_92
# BB#97:                                #   in Loop: Header=BB27_85 Depth=3
	movzbl	13972(%r14,%r9), %eax
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB27_98:                              #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_84 Depth=2
                                        #       Parent Loop BB27_85 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	276(%rsp,%rsi,4), %edi
	imulq	$1032, %rax, %rbp       # imm = 0x408
	addq	%r14, %rbp
	incl	1588(%rbp,%rdi,4)
	incq	%rsi
	cmpq	%rdx, %rsi
	jl	.LBB27_98
# BB#99:                                #   in Loop: Header=BB27_85 Depth=3
	incl	%r8d
	cmpl	%r11d, %ecx
	jb	.LBB27_85
# BB#100:                               # %.preheader492.preheader
                                        #   in Loop: Header=BB27_84 Depth=2
	movq	104(%rsp), %rbp         # 8-byte Reload
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB27_101:                             # %.preheader492
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_84 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB27_105 Depth 4
	cmpq	$8, %r15
	movq	48(%rsp), %rdx          # 8-byte Reload
	jae	.LBB27_103
# BB#102:                               #   in Loop: Header=BB27_101 Depth=3
	xorl	%eax, %eax
	cmpl	$0, (%rbp,%rax,4)
	jne	.LBB27_126
	jmp	.LBB27_125
	.p2align	4, 0x90
.LBB27_103:                             # %min.iters.checked710
                                        #   in Loop: Header=BB27_101 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	testq	%rax, %rax
	pxor	%xmm2, %xmm2
	je	.LBB27_123
# BB#104:                               # %vector.body705.preheader
                                        #   in Loop: Header=BB27_101 Depth=3
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB27_105:                             # %vector.body705
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_84 Depth=2
                                        #       Parent Loop BB27_101 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movdqu	(%rcx), %xmm1
	movdqu	16(%rcx), %xmm0
	pcmpeqd	%xmm2, %xmm1
	movdqa	%xmm1, 256(%rsp)
	testb	$1, 256(%rsp)
	je	.LBB27_107
# BB#106:                               # %pred.store.if
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, (%rcx)
.LBB27_107:                             # %pred.store.continue
                                        #   in Loop: Header=BB27_105 Depth=4
	movdqa	%xmm1, 240(%rsp)
	testb	$1, 244(%rsp)
	je	.LBB27_109
# BB#108:                               # %pred.store.if725
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, 4(%rcx)
.LBB27_109:                             # %pred.store.continue726
                                        #   in Loop: Header=BB27_105 Depth=4
	movdqa	%xmm1, 224(%rsp)
	testb	$1, 232(%rsp)
	je	.LBB27_111
# BB#110:                               # %pred.store.if727
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, 8(%rcx)
.LBB27_111:                             # %pred.store.continue728
                                        #   in Loop: Header=BB27_105 Depth=4
	movdqa	%xmm1, 208(%rsp)
	testb	$1, 220(%rsp)
	je	.LBB27_113
# BB#112:                               # %pred.store.if729
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, 12(%rcx)
.LBB27_113:                             # %pred.store.continue730
                                        #   in Loop: Header=BB27_105 Depth=4
	pcmpeqd	%xmm2, %xmm0
	movdqa	%xmm0, 192(%rsp)
	testb	$1, 192(%rsp)
	je	.LBB27_115
# BB#114:                               # %pred.store.if731
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, 16(%rcx)
.LBB27_115:                             # %pred.store.continue732
                                        #   in Loop: Header=BB27_105 Depth=4
	movdqa	%xmm0, 176(%rsp)
	testb	$1, 180(%rsp)
	je	.LBB27_117
# BB#116:                               # %pred.store.if733
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, 20(%rcx)
.LBB27_117:                             # %pred.store.continue734
                                        #   in Loop: Header=BB27_105 Depth=4
	movdqa	%xmm0, 160(%rsp)
	testb	$1, 168(%rsp)
	je	.LBB27_119
# BB#118:                               # %pred.store.if735
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, 24(%rcx)
.LBB27_119:                             # %pred.store.continue736
                                        #   in Loop: Header=BB27_105 Depth=4
	movdqa	%xmm0, 144(%rsp)
	testb	$1, 156(%rsp)
	je	.LBB27_121
# BB#120:                               # %pred.store.if737
                                        #   in Loop: Header=BB27_105 Depth=4
	movl	$1, 28(%rcx)
.LBB27_121:                             # %pred.store.continue738
                                        #   in Loop: Header=BB27_105 Depth=4
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB27_105
# BB#122:                               # %middle.block706
                                        #   in Loop: Header=BB27_101 Depth=3
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpq	%rax, %r15
	jne	.LBB27_124
	jmp	.LBB27_127
.LBB27_123:                             #   in Loop: Header=BB27_101 Depth=3
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB27_124:                             # %scalar.ph707
                                        #   in Loop: Header=BB27_101 Depth=3
	cmpl	$0, (%rbp,%rax,4)
	jne	.LBB27_126
.LBB27_125:                             #   in Loop: Header=BB27_101 Depth=3
	movl	$1, (%rbp,%rax,4)
.LBB27_126:                             #   in Loop: Header=BB27_101 Depth=3
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB27_124
.LBB27_127:                             # %.loopexit
                                        #   in Loop: Header=BB27_101 Depth=3
	imulq	$1032, %r13, %rax       # imm = 0x408
	leaq	1588(%r14,%rax), %rdi
	leaq	7780(%r14,%rax), %rsi
	imulq	$258, %r13, %rax        # imm = 0x102
	leaq	40(%r14,%rax), %rdx
	movl	$258, %ecx              # imm = 0x102
	movl	$16, %r8d
	callq	Huffman_Generate
	incq	%r13
	addq	$1032, %rbp             # imm = 0x408
	cmpq	%r12, %r13
	jl	.LBB27_101
# BB#128:                               #   in Loop: Header=BB27_84 Depth=2
	movl	16(%rsp), %eax          # 4-byte Reload
	incl	%eax
	cmpl	$4, %eax
	movq	24(%rsp), %r13          # 8-byte Reload
	jne	.LBB27_84
# BB#129:                               #   in Loop: Header=BB27_53 Depth=1
	testq	%r12, %r12
	movl	$1, %ecx
	cmovgq	%r12, %rcx
	cmpq	$31, %rcx
	jbe	.LBB27_134
# BB#130:                               # %min.iters.checked694
                                        #   in Loop: Header=BB27_53 Depth=1
	movq	%rcx, %rax
	movabsq	$9223372036854775776, %rdx # imm = 0x7FFFFFFFFFFFFFE0
	andq	%rdx, %rax
	movdqa	.LCPI27_1(%rip), %xmm0  # xmm0 = [16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16]
	je	.LBB27_134
# BB#131:                               # %vector.body690.preheader
                                        #   in Loop: Header=BB27_53 Depth=1
	leaq	-32(%rax), %rsi
	movl	%esi, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB27_55
# BB#132:                               # %vector.body690.prol.preheader
                                        #   in Loop: Header=BB27_53 Depth=1
	negq	%rdi
	xorl	%edx, %edx
	movdqa	%xmm0, %xmm3
	movdqa	.LCPI27_0(%rip), %xmm0  # xmm0 = [0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15]
	movdqa	.LCPI27_2(%rip), %xmm4  # xmm4 = [32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32]
	.p2align	4, 0x90
.LBB27_133:                             # %vector.body690.prol
                                        #   Parent Loop BB27_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	%xmm0, %xmm1
	paddb	%xmm3, %xmm1
	movdqu	%xmm0, 272(%rsp,%rdx)
	movdqu	%xmm1, 288(%rsp,%rdx)
	addq	$32, %rdx
	paddb	%xmm4, %xmm0
	incq	%rdi
	jne	.LBB27_133
	jmp	.LBB27_56
	.p2align	4, 0x90
.LBB27_134:                             #   in Loop: Header=BB27_53 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB27_135:                             # %scalar.ph692
                                        #   Parent Loop BB27_53 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	%al, 272(%rsp,%rax)
	incq	%rax
	cmpq	%r12, %rax
	jl	.LBB27_135
.LBB27_136:                             # %.preheader496.preheader
                                        #   in Loop: Header=BB27_53 Depth=1
	movb	272(%rsp), %r15b
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB27_137:                             # %.preheader496
                                        #   Parent Loop BB27_53 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_140 Depth 3
                                        #         Child Loop BB27_141 Depth 4
                                        #       Child Loop BB27_147 Depth 3
                                        #       Child Loop BB27_152 Depth 3
	movl	%r15d, %eax
	movb	13972(%r14,%r8), %r15b
	movq	32(%r14), %r13
	leaq	4(%r13), %r12
	leaq	8(%r13), %rbp
	leaq	16(%r13), %r11
	xorl	%r10d, %r10d
	cmpb	%r15b, %al
	movl	4(%r13), %r9d
	jne	.LBB27_139
# BB#138:                               #   in Loop: Header=BB27_137 Depth=2
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB27_146
	.p2align	4, 0x90
.LBB27_139:                             # %.preheader490.preheader
                                        #   in Loop: Header=BB27_137 Depth=2
	movb	%r15b, 16(%rsp)         # 1-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB27_140:                             # %.preheader490
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_137 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB27_141 Depth 4
	movl	$1, %r8d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB27_141:                             #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_137 Depth=2
                                        #       Parent Loop BB27_140 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	(%rbp), %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	movl	%r8d, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %esi
	movb	%sil, (%rbp)
	shll	%cl, %edi
	subl	%edx, %r9d
	movl	%r9d, (%r12)
	jne	.LBB27_143
# BB#142:                               #   in Loop: Header=BB27_141 Depth=4
	movq	(%r11), %rcx
	movl	(%r13), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r13)
	movb	%sil, (%rcx,%rdx)
	movl	$8, (%r12)
	movl	$8, %r9d
.LBB27_143:                             # %.backedge.i.i.i457
                                        #   in Loop: Header=BB27_141 Depth=4
	subl	%edi, %r8d
	testl	%eax, %eax
	jg	.LBB27_141
# BB#144:                               # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit458
                                        #   in Loop: Header=BB27_140 Depth=3
	movq	32(%r14), %r13
	leaq	4(%r13), %r12
	leaq	8(%r13), %rbp
	leaq	16(%r13), %r11
	movb	16(%rsp), %al           # 1-byte Reload
	cmpb	%al, 273(%rsp,%r15)
	leaq	1(%r15), %r15
	movl	4(%r13), %r9d
	jne	.LBB27_140
# BB#145:                               # %.preheader491.loopexit
                                        #   in Loop: Header=BB27_137 Depth=2
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	64(%rsp), %r8           # 8-byte Reload
	movb	16(%rsp), %r15b         # 1-byte Reload
.LBB27_146:                             # %.preheader491
                                        #   in Loop: Header=BB27_137 Depth=2
	movl	$1, %eax
	.p2align	4, 0x90
.LBB27_147:                             #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_137 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	(%rbp), %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	movl	%r10d, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %esi
	movb	%sil, (%rbp)
	shll	%cl, %edi
	subl	%edx, %r9d
	movl	%r9d, (%r12)
	jne	.LBB27_149
# BB#148:                               #   in Loop: Header=BB27_147 Depth=3
	movq	(%r11), %rcx
	movl	(%r13), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r13)
	movb	%sil, (%rcx,%rdx)
	movl	$8, (%r12)
	movl	$8, %r9d
.LBB27_149:                             # %.backedge.i.i.i452
                                        #   in Loop: Header=BB27_147 Depth=3
	subl	%edi, %r10d
	testl	%eax, %eax
	jg	.LBB27_147
# BB#150:                               # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit453.preheader
                                        #   in Loop: Header=BB27_137 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB27_153
# BB#151:                               # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit453.preheader565
                                        #   in Loop: Header=BB27_137 Depth=2
	cltq
	incq	%rax
	.p2align	4, 0x90
.LBB27_152:                             # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit453
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_137 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	270(%rsp,%rax), %ecx
	movb	%cl, 271(%rsp,%rax)
	decq	%rax
	cmpq	$1, %rax
	jg	.LBB27_152
.LBB27_153:                             # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit453._crit_edge
                                        #   in Loop: Header=BB27_137 Depth=2
	movb	%r15b, 272(%rsp)
	incq	%r8
	cmpq	96(%rsp), %r8           # 8-byte Folded Reload
	jb	.LBB27_137
# BB#154:                               #   in Loop: Header=BB27_53 Depth=1
	xorl	%eax, %eax
	movq	40(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_155:                             #   Parent Loop BB27_53 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_156 Depth 3
                                        #       Child Loop BB27_160 Depth 3
                                        #         Child Loop BB27_161 Depth 4
                                        #           Child Loop BB27_162 Depth 5
                                        #           Child Loop BB27_172 Depth 5
                                        #           Child Loop BB27_167 Depth 5
                                        #         Child Loop BB27_179 Depth 4
	movq	%rax, 64(%rsp)          # 8-byte Spill
	imulq	$258, %rax, %rax        # imm = 0x102
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movzbl	40(%r14,%rax), %r11d
	movq	32(%r14), %r8
	movl	4(%r8), %r9d
	movl	$5, %eax
	movl	%r11d, %ebp
	.p2align	4, 0x90
.LBB27_156:                             #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_155 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%r9d, %eax
	movl	%r9d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	movl	%ebp, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %edi
	movb	%dil, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %r9d
	movl	%r9d, 4(%r8)
	jne	.LBB27_158
# BB#157:                               #   in Loop: Header=BB27_156 Depth=3
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r8)
	movb	%dil, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %r9d
.LBB27_158:                             # %.backedge.i.i
                                        #   in Loop: Header=BB27_156 Depth=3
	subl	%esi, %ebp
	testl	%eax, %eax
	jg	.LBB27_156
# BB#159:                               # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit.preheader
                                        #   in Loop: Header=BB27_155 Depth=2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB27_160:                             # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_155 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB27_161 Depth 4
                                        #           Child Loop BB27_162 Depth 5
                                        #           Child Loop BB27_172 Depth 5
                                        #           Child Loop BB27_167 Depth 5
                                        #         Child Loop BB27_179 Depth 4
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax), %rax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movzbl	40(%rcx,%rax), %ebp
	movq	32(%r14), %r10
	leaq	4(%r10), %rdi
	leaq	8(%r10), %r8
	leaq	16(%r10), %r9
	cmpl	%ebp, %r11d
	movl	4(%r10), %r13d
	je	.LBB27_178
	.p2align	4, 0x90
.LBB27_161:                             # %.preheader488
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_155 Depth=2
                                        #       Parent Loop BB27_160 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB27_162 Depth 5
                                        #           Child Loop BB27_172 Depth 5
                                        #           Child Loop BB27_167 Depth 5
	movl	$1, %r15d
	movl	$1, %eax
	.p2align	4, 0x90
.LBB27_162:                             #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_155 Depth=2
                                        #       Parent Loop BB27_160 Depth=3
                                        #         Parent Loop BB27_161 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpl	%r13d, %eax
	movl	%r13d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	(%r8), %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	movl	%r15d, %r12d
	movl	%eax, %ecx
	shrl	%cl, %r12d
	orl	%r12d, %esi
	movb	%sil, (%r8)
	shll	%cl, %r12d
	subl	%edx, %r13d
	movl	%r13d, (%rdi)
	jne	.LBB27_164
# BB#163:                               #   in Loop: Header=BB27_162 Depth=5
	movq	(%r9), %rcx
	movl	(%r10), %edx
	leal	1(%rdx), %ebx
	movl	%ebx, (%r10)
	movb	%sil, (%rcx,%rdx)
	movl	$8, (%rdi)
	movl	$8, %r13d
.LBB27_164:                             # %.backedge.i.i.i447
                                        #   in Loop: Header=BB27_162 Depth=5
	subl	%r12d, %r15d
	testl	%eax, %eax
	jg	.LBB27_162
# BB#165:                               # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit448
                                        #   in Loop: Header=BB27_161 Depth=4
	movl	%ebp, %r15d
	cmpl	%ebp, %r11d
	movq	32(%r14), %r8
	movl	4(%r8), %ebp
	jae	.LBB27_171
# BB#166:                               # %.preheader.preheader
                                        #   in Loop: Header=BB27_161 Depth=4
	movl	$1, %eax
	xorl	%ebx, %ebx
	movq	40(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_167:                             # %.preheader
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_155 Depth=2
                                        #       Parent Loop BB27_160 Depth=3
                                        #         Parent Loop BB27_161 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	movl	%ebx, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %edi
	movb	%dil, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB27_169
# BB#168:                               #   in Loop: Header=BB27_167 Depth=5
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%dil, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB27_169:                             # %.backedge.i.i.i442
                                        #   in Loop: Header=BB27_167 Depth=5
	subl	%esi, %ebx
	testl	%eax, %eax
	jg	.LBB27_167
# BB#170:                               #   in Loop: Header=BB27_161 Depth=4
	movl	$1, %eax
	jmp	.LBB27_176
	.p2align	4, 0x90
.LBB27_171:                             # %.preheader487.preheader
                                        #   in Loop: Header=BB27_161 Depth=4
	movl	$1, %edi
	movl	$1, %eax
	movq	40(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_172:                             # %.preheader487
                                        #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_155 Depth=2
                                        #       Parent Loop BB27_160 Depth=3
                                        #         Parent Loop BB27_161 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%edi, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %esi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB27_174
# BB#173:                               #   in Loop: Header=BB27_172 Depth=5
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB27_174:                             # %.backedge.i.i.i437
                                        #   in Loop: Header=BB27_172 Depth=5
	subl	%esi, %edi
	testl	%eax, %eax
	jg	.LBB27_172
# BB#175:                               #   in Loop: Header=BB27_161 Depth=4
	movl	$-1, %eax
.LBB27_176:                             # %.backedge
                                        #   in Loop: Header=BB27_161 Depth=4
	addl	%eax, %r11d
	movq	32(%r14), %r10
	leaq	4(%r10), %rdi
	leaq	8(%r10), %r8
	leaq	16(%r10), %r9
	movl	%r15d, %ebp
	cmpl	%ebp, %r11d
	movl	4(%r10), %r13d
	jne	.LBB27_161
# BB#177:                               #   in Loop: Header=BB27_160 Depth=3
	movl	%ebp, %r11d
.LBB27_178:                             # %.preheader489
                                        #   in Loop: Header=BB27_160 Depth=3
	movl	$1, %eax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB27_179:                             #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_155 Depth=2
                                        #       Parent Loop BB27_160 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpl	%r13d, %eax
	movl	%r13d, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%ebp, %esi
	movl	%eax, %ecx
	shrl	%cl, %esi
	orl	%esi, %ebx
	movb	%bl, (%r8)
	shll	%cl, %esi
	subl	%edx, %r13d
	movl	%r13d, (%rdi)
	jne	.LBB27_181
# BB#180:                               #   in Loop: Header=BB27_179 Depth=4
	movq	(%r9), %r15
	movl	(%r10), %edx
	leal	1(%rdx), %ecx
	movl	%ecx, (%r10)
	movb	%bl, (%r15,%rdx)
	movl	$8, (%rdi)
	movl	$8, %r13d
.LBB27_181:                             # %.backedge.i.i.i432
                                        #   in Loop: Header=BB27_179 Depth=4
	subl	%esi, %ebp
	testl	%eax, %eax
	jg	.LBB27_179
# BB#182:                               # %_ZN9NCompress6NBZip211CThreadInfo9WriteBit2Eb.exit433
                                        #   in Loop: Header=BB27_160 Depth=3
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	cmpq	48(%rsp), %rcx          # 8-byte Folded Reload
	jl	.LBB27_160
# BB#183:                               #   in Loop: Header=BB27_155 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	incq	%rax
	cmpq	%r12, %rax
	jl	.LBB27_155
# BB#184:                               # %.preheader495.preheader
                                        #   in Loop: Header=BB27_53 Depth=1
	xorl	%r15d, %r15d
	xorl	%r8d, %r8d
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	movq	24(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB27_185:                             # %.preheader495
                                        #   Parent Loop BB27_53 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_191 Depth 3
	leal	1(%r11), %ecx
	movl	%r11d, %eax
	movzbl	(%r13,%rax), %eax
	cmpl	$255, %eax
	je	.LBB27_187
# BB#186:                               #   in Loop: Header=BB27_185 Depth=2
	movl	%ecx, %r11d
	testl	%r15d, %r15d
	jne	.LBB27_189
	jmp	.LBB27_188
	.p2align	4, 0x90
.LBB27_187:                             #   in Loop: Header=BB27_185 Depth=2
	addl	$2, %r11d
	movl	%ecx, %ecx
	movzbl	(%r13,%rcx), %ecx
	addl	%ecx, %eax
	testl	%r15d, %r15d
	jne	.LBB27_189
.LBB27_188:                             #   in Loop: Header=BB27_185 Depth=2
	movl	%r8d, %ecx
	incl	%r8d
	movzbl	13972(%r14,%rcx), %ecx
	imulq	$258, %rcx, %rdx        # imm = 0x102
	leaq	40(%r14,%rdx), %r10
	imulq	$1032, %rcx, %rcx       # imm = 0x408
	leaq	7780(%r14,%rcx), %r9
	movl	$50, %r15d
.LBB27_189:                             #   in Loop: Header=BB27_185 Depth=2
	movl	%eax, %ecx
	movzbl	(%r10,%rcx), %eax
	testl	%eax, %eax
	je	.LBB27_194
# BB#190:                               # %.lr.ph.i
                                        #   in Loop: Header=BB27_185 Depth=2
	movq	32(%r14), %r12
	movl	(%r9,%rcx,4), %esi
	movl	4(%r12), %ebp
	.p2align	4, 0x90
.LBB27_191:                             #   Parent Loop BB27_53 Depth=1
                                        #     Parent Loop BB27_185 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r12), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebx
	movb	%bl, 8(%r12)
	shll	%cl, %edi
	subl	%edx, %ebp
	movl	%ebp, 4(%r12)
	jne	.LBB27_193
# BB#192:                               #   in Loop: Header=BB27_191 Depth=3
	movq	16(%r12), %rcx
	movl	(%r12), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r12)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r12)
	movl	$8, %ebp
.LBB27_193:                             # %.backedge.i
                                        #   in Loop: Header=BB27_191 Depth=3
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB27_191
.LBB27_194:                             # %_ZN9NCompress6NBZip216CMsbfEncoderTemp9WriteBitsEji.exit
                                        #   in Loop: Header=BB27_185 Depth=2
	decl	%r15d
	cmpl	112(%rsp), %r11d        # 4-byte Folded Reload
	jb	.LBB27_185
# BB#195:                               #   in Loop: Header=BB27_53 Depth=1
	cmpb	$0, 36080(%r14)
	movl	92(%rsp), %edx          # 4-byte Reload
	je	.LBB27_199
# BB#196:                               #   in Loop: Header=BB27_53 Depth=1
	movq	32(%r14), %r9
	movl	(%r9), %eax
	movl	4(%r9), %ebx
	movl	84(%rsp), %ecx          # 4-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	subl	%ebx, %ecx
	leal	(%rcx,%rax,8), %eax
	cmpl	60(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB27_198
# BB#197:                               #   in Loop: Header=BB27_53 Depth=1
	cmpl	$6, %edx
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movl	%edx, 56(%rsp)          # 4-byte Spill
	je	.LBB27_199
.LBB27_198:                             #   in Loop: Header=BB27_53 Depth=1
	incl	%edx
	xorl	%eax, %eax
	cmpl	$8, %edx
	jl	.LBB27_53
.LBB27_199:                             # %.thread
	addq	$1768, %rsp             # imm = 0x6E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	_ZN9NCompress6NBZip211CThreadInfo11EncodeBlockEPKhj, .Lfunc_end27-_ZN9NCompress6NBZip211CThreadInfo11EncodeBlockEPKhj
	.cfi_endproc

	.section	.text._ZN9NCompress12CMtf8Encoder11FindAndMoveEh,"axG",@progbits,_ZN9NCompress12CMtf8Encoder11FindAndMoveEh,comdat
	.weak	_ZN9NCompress12CMtf8Encoder11FindAndMoveEh
	.p2align	4, 0x90
	.type	_ZN9NCompress12CMtf8Encoder11FindAndMoveEh,@function
_ZN9NCompress12CMtf8Encoder11FindAndMoveEh: # @_ZN9NCompress12CMtf8Encoder11FindAndMoveEh
	.cfi_startproc
# BB#0:
	movabsq	$-4294967296, %r9       # imm = 0xFFFFFFFF00000000
	movl	$-1, %eax
	movl	$4294967287, %r8d       # imm = 0xFFFFFFF7
	movq	%rdi, %rcx
	movl	$4294967287, %r10d      # imm = 0xFFFFFFF7
	.p2align	4, 0x90
.LBB28_1:                               # =>This Inner Loop Header: Depth=1
	incq	%r10
	leaq	9(%r9,%r8), %r9
	incl	%eax
	cmpb	%sil, (%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB28_1
# BB#2:                                 # %.preheader31
	cmpl	$8, %eax
	movl	%eax, %ecx
	jl	.LBB28_6
# BB#3:                                 # %.lr.ph34.preheader
	sarq	$32, %r9
	andl	$-8, %r10d
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB28_4:                               # %.lr.ph34
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rdi,%rcx), %edx
	movb	%dl, (%rdi,%rcx)
	movzbl	-2(%rdi,%rcx), %edx
	movb	%dl, -1(%rdi,%rcx)
	movzbl	-3(%rdi,%rcx), %edx
	movb	%dl, -2(%rdi,%rcx)
	movzbl	-4(%rdi,%rcx), %edx
	movb	%dl, -3(%rdi,%rcx)
	movzbl	-5(%rdi,%rcx), %edx
	movb	%dl, -4(%rdi,%rcx)
	movzbl	-6(%rdi,%rcx), %edx
	movb	%dl, -5(%rdi,%rcx)
	movzbl	-7(%rdi,%rcx), %edx
	movb	%dl, -6(%rdi,%rcx)
	movzbl	-8(%rdi,%rcx), %edx
	movb	%dl, -7(%rdi,%rcx)
	leaq	-8(%rcx), %rcx
	cmpq	$7, %rcx
	jg	.LBB28_4
# BB#5:                                 # %.preheader.loopexit
	leal	1(%r9,%r8), %ecx
	subl	%r10d, %ecx
.LBB28_6:                               # %.preheader
	testl	%ecx, %ecx
	jle	.LBB28_9
# BB#7:                                 # %.lr.ph.preheader
	movslq	%ecx, %rcx
	incq	%rcx
	.p2align	4, 0x90
.LBB28_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-2(%rdi,%rcx), %edx
	movb	%dl, -1(%rdi,%rcx)
	decq	%rcx
	cmpq	$1, %rcx
	jg	.LBB28_8
.LBB28_9:                               # %._crit_edge
	movb	%sil, (%rdi)
	retq
.Lfunc_end28:
	.size	_ZN9NCompress12CMtf8Encoder11FindAndMoveEh, .Lfunc_end28-_ZN9NCompress12CMtf8Encoder11FindAndMoveEh
	.cfi_endproc

	.text
	.globl	_ZN9NCompress6NBZip211CThreadInfo22EncodeBlockWithHeadersEPKhj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo22EncodeBlockWithHeadersEPKhj,@function
_ZN9NCompress6NBZip211CThreadInfo22EncodeBlockWithHeadersEPKhj: # @_ZN9NCompress6NBZip211CThreadInfo22EncodeBlockWithHeadersEPKhj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 48
.Lcfi174:
	.cfi_offset %rbx, -48
.Lcfi175:
	.cfi_offset %r12, -40
.Lcfi176:
	.cfi_offset %r14, -32
.Lcfi177:
	.cfi_offset %r15, -24
.Lcfi178:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	32(%r15), %r8
	movl	4(%r8), %ebp
	movl	$8, %eax
	movl	$49, %esi
	.p2align	4, 0x90
.LBB29_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %edi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB29_3
# BB#2:                                 #   in Loop: Header=BB29_1 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB29_3:                               # %.backedge.i.i.i
                                        #   in Loop: Header=BB29_1 Depth=1
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB29_1
# BB#4:                                 # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit
	movq	32(%r15), %r8
	movl	4(%r8), %ebp
	movl	$8, %eax
	movl	$65, %esi
	.p2align	4, 0x90
.LBB29_5:                               # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %edi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB29_7
# BB#6:                                 #   in Loop: Header=BB29_5 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB29_7:                               # %.backedge.i.i.i33
                                        #   in Loop: Header=BB29_5 Depth=1
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB29_5
# BB#8:                                 # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit34
	movq	32(%r15), %r8
	movl	4(%r8), %ebp
	movl	$8, %eax
	movl	$89, %esi
	.p2align	4, 0x90
.LBB29_9:                               # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %edi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB29_11
# BB#10:                                #   in Loop: Header=BB29_9 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB29_11:                              # %.backedge.i.i.i38
                                        #   in Loop: Header=BB29_9 Depth=1
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB29_9
# BB#12:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit39
	movq	32(%r15), %r8
	movl	4(%r8), %ebp
	movl	$8, %eax
	movl	$38, %esi
	.p2align	4, 0x90
.LBB29_13:                              # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %edi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB29_15
# BB#14:                                #   in Loop: Header=BB29_13 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB29_15:                              # %.backedge.i.i.i53
                                        #   in Loop: Header=BB29_13 Depth=1
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB29_13
# BB#16:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit54
	movq	32(%r15), %r8
	movl	4(%r8), %ebp
	movl	$8, %eax
	movl	$83, %esi
	.p2align	4, 0x90
.LBB29_17:                              # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %edi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB29_19
# BB#18:                                #   in Loop: Header=BB29_17 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB29_19:                              # %.backedge.i.i.i48
                                        #   in Loop: Header=BB29_17 Depth=1
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB29_17
# BB#20:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit49
	movq	32(%r15), %r8
	movl	4(%r8), %ebp
	movl	$8, %eax
	movl	$89, %esi
	.p2align	4, 0x90
.LBB29_21:                              # =>This Inner Loop Header: Depth=1
	cmpl	%ebp, %eax
	movl	%ebp, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	8(%r8), %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%esi, %edi
	movl	%eax, %ecx
	shrl	%cl, %edi
	orl	%edi, %ebx
	movb	%bl, 8(%r8)
	shll	%cl, %edi
	subl	%edx, %ebp
	movl	%ebp, 4(%r8)
	jne	.LBB29_23
# BB#22:                                #   in Loop: Header=BB29_21 Depth=1
	movq	16(%r8), %rcx
	movl	(%r8), %edx
	leal	1(%rdx), %ebp
	movl	%ebp, (%r8)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%r8)
	movl	$8, %ebp
.LBB29_23:                              # %.backedge.i.i.i43
                                        #   in Loop: Header=BB29_21 Depth=1
	subl	%edi, %esi
	testl	%eax, %eax
	jg	.LBB29_21
# BB#24:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteByte2Eh.exit44
	movb	(%r12), %bl
	movl	%r14d, %r9d
	xorl	%ecx, %ecx
	movl	$-1, %ebp
	movl	$1, %r8d
	movl	%ebx, %r10d
	xorl	%esi, %esi
	cmpl	$4, %esi
	jne	.LBB29_32
	jmp	.LBB29_26
	.p2align	4, 0x90
.LBB29_34:                              # %.loopexit._crit_edge
	movb	(%r12,%rcx), %bl
	cmpl	$4, %esi
	jne	.LBB29_32
.LBB29_26:                              # %.preheader
	xorl	%esi, %esi
	testb	%bl, %bl
	je	.LBB29_33
# BB#27:                                # %.lr.ph
	movzbl	%r10b, %edi
	movl	%ebx, %r11d
	decb	%r11b
	movl	%ebx, %edx
	andb	$3, %dl
	je	.LBB29_30
# BB#28:                                # %.prol.preheader
	negb	%dl
	.p2align	4, 0x90
.LBB29_29:                              # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	shrl	$24, %eax
	xorl	%edi, %eax
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebp
	decb	%bl
	incb	%dl
	jne	.LBB29_29
.LBB29_30:                              # %.prol.loopexit
	cmpb	$3, %r11b
	jb	.LBB29_33
	.p2align	4, 0x90
.LBB29_31:                              # =>This Inner Loop Header: Depth=1
	movl	%ebp, %eax
	shrl	$24, %eax
	xorl	%edi, %eax
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebp
	movl	%ebp, %eax
	shrl	$24, %eax
	xorl	%edi, %eax
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebp
	movl	%ebp, %eax
	shrl	$24, %eax
	xorl	%edi, %eax
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebp
	movl	%ebp, %eax
	shrl	$24, %eax
	xorl	%edi, %eax
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rax,4), %ebp
	addb	$-4, %bl
	jne	.LBB29_31
	jmp	.LBB29_33
	.p2align	4, 0x90
.LBB29_32:
	incl	%esi
	cmpb	%bl, %r10b
	cmovnel	%r8d, %esi
	movl	%ebp, %eax
	shrl	$24, %eax
	movzbl	%bl, %edx
	xorl	%eax, %edx
	shll	$8, %ebp
	xorl	_ZN9CBZip2Crc5TableE(,%rdx,4), %ebp
	movl	%ebx, %r10d
.LBB29_33:                              # %.loopexit
	incq	%rcx
	cmpq	%r9, %rcx
	jb	.LBB29_34
# BB#35:
	notl	%ebp
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	_ZN9NCompress6NBZip211CThreadInfo9WriteCrc2Ej
	movq	%r15, %rdi
	movq	%r12, %rsi
	movl	%r14d, %edx
	callq	_ZN9NCompress6NBZip211CThreadInfo11EncodeBlockEPKhj
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end29:
	.size	_ZN9NCompress6NBZip211CThreadInfo22EncodeBlockWithHeadersEPKhj, .Lfunc_end29-_ZN9NCompress6NBZip211CThreadInfo22EncodeBlockWithHeadersEPKhj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj,@function
_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj: # @_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi179:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi180:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi181:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi182:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi183:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi184:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi185:
	.cfi_def_cfa_offset 96
.Lcfi186:
	.cfi_offset %rbx, -56
.Lcfi187:
	.cfi_offset %r12, -48
.Lcfi188:
	.cfi_offset %r13, -40
.Lcfi189:
	.cfi_offset %r14, -32
.Lcfi190:
	.cfi_offset %r15, -24
.Lcfi191:
	.cfi_offset %rbp, -16
	movl	%ecx, %r15d
	movl	%edx, %r12d
	movq	%rsi, %r13
	movl	36072(%rdi), %edx
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	32(%rdi), %rax
	movl	(%rax), %ecx
	movl	4(%rax), %r9d
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, %esi
	movl	$8, %ecx
	subl	%r9d, %ecx
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	leal	(%rcx,%rsi,8), %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movb	$1, %r14b
	xorl	%r10d, %r10d
	cmpl	$1024, %r12d            # imm = 0x400
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	jb	.LBB30_19
# BB#1:
	cmpl	$2, %r15d
	jb	.LBB30_19
# BB#2:
	movb	8(%rax), %sil
	movl	%r12d, %ebp
	shrl	%ebp
	jmp	.LBB30_4
	.p2align	4, 0x90
.LBB30_3:                               #   in Loop: Header=BB30_4 Depth=1
	incl	%ebp
.LBB30_4:                               # =>This Inner Loop Header: Depth=1
	movl	%ebp, %ebx
	leal	-1(%rbp), %eax
	movzbl	(%r13,%rax), %eax
	cmpb	%al, (%r13,%rbx)
	jne	.LBB30_6
# BB#5:                                 #   in Loop: Header=BB30_4 Depth=1
	cmpl	%r12d, %ebp
	jb	.LBB30_3
	jmp	.LBB30_8
	.p2align	4, 0x90
.LBB30_6:                               #   in Loop: Header=BB30_4 Depth=1
	cmpl	%r12d, %ebp
	jae	.LBB30_8
# BB#7:                                 #   in Loop: Header=BB30_4 Depth=1
	leal	-2(%rbp), %ecx
	cmpb	(%r13,%rcx), %al
	je	.LBB30_3
.LBB30_8:                               # %.critedge
	xorl	%r10d, %r10d
	cmpl	%r12d, %ebp
	movl	$0, %r11d
	jae	.LBB30_20
# BB#9:
	movb	%sil, (%rsp)            # 1-byte Spill
	addq	%r13, %rbx
	decl	%r15d
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%r13, %rsi
	movl	%ebp, %edx
	movl	%r15d, %ecx
	callq	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj
	movl	%r12d, %edx
	subl	%ebp, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %ecx
	callq	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj
	movq	32(%r14), %rsi
	movl	(%rsi), %ecx
	movl	4(%rsi), %edi
	movl	$8, %r9d
	movl	$8, %eax
	subl	%edi, %eax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	leal	(%rax,%rcx,8), %r10d
	movb	8(%rsi), %r11b
	movl	%r10d, %ecx
	andl	$7, %ecx
	je	.LBB30_17
# BB#10:
	movl	$8, %eax
	subl	%ecx, %eax
	je	.LBB30_17
# BB#11:                                # %.lr.ph.i.i
	xorl	%r8d, %r8d
	movl	%r11d, %ecx
	jmp	.LBB30_13
	.p2align	4, 0x90
.LBB30_12:                              # %.backedge.i.i._crit_edge
                                        #   in Loop: Header=BB30_13 Depth=1
	subl	%ebp, %r8d
	movzbl	8(%rsi), %ecx
.LBB30_13:                              # =>This Inner Loop Header: Depth=1
	cmpl	%edi, %eax
	movl	%edi, %edx
	cmovlel	%eax, %edx
	subl	%edx, %eax
	movzbl	%cl, %ebx
	movl	%edx, %ecx
	shll	%cl, %ebx
	movl	%r8d, %ebp
	movl	%eax, %ecx
	shrl	%cl, %ebp
	orl	%ebp, %ebx
	movb	%bl, 8(%rsi)
	shll	%cl, %ebp
	subl	%edx, %edi
	movl	%edi, 4(%rsi)
	jne	.LBB30_15
# BB#14:                                #   in Loop: Header=BB30_13 Depth=1
	movq	16(%rsi), %rcx
	movl	(%rsi), %edx
	leal	1(%rdx), %edi
	movl	%edi, (%rsi)
	movb	%bl, (%rcx,%rdx)
	movl	$8, 4(%rsi)
	movl	$8, %edi
.LBB30_15:                              # %.backedge.i.i
                                        #   in Loop: Header=BB30_13 Depth=1
	testl	%eax, %eax
	jg	.LBB30_12
# BB#16:                                # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit.loopexit
	movq	32(%r14), %rsi
	movl	(%rsi), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB30_17:                              # %_ZN9NCompress6NBZip211CThreadInfo10WriteBits2Ejj.exit
	movl	4(%rsp), %eax           # 4-byte Reload
	movb	(%rsp), %cl             # 1-byte Reload
	andl	$7, %eax
	subl	%eax, %r9d
	movl	%r9d, 4(%rsi)
	movb	%cl, 8(%rsi)
	xorl	%r14d, %r14d
	jmp	.LBB30_20
.LBB30_19:
	xorl	%r11d, %r11d
.LBB30_20:
	movl	%r11d, (%rsp)           # 4-byte Spill
	movl	%r10d, %ebp
	movl	%r9d, %ebx
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movq	%r13, %rsi
	movl	%r12d, %edx
	callq	_ZN9NCompress6NBZip211CThreadInfo22EncodeBlockWithHeadersEPKhj
	testb	%r14b, %r14b
	je	.LBB30_22
# BB#21:
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	1(%rdx), %ecx
	movl	%ecx, 36072(%r15)
	movl	%eax, 31976(%r15,%rdx,4)
	jmp	.LBB30_34
.LBB30_22:
	movl	$8, %ecx
	subl	%ebx, %ecx
	movq	16(%rsp), %r13          # 8-byte Reload
	leal	(%rcx,%r13,8), %r8d
	movq	32(%r15), %rcx
	movl	(%rcx), %edx
	movl	$8, %esi
	subl	4(%rcx), %esi
	leal	(%rsi,%rdx,8), %r9d
	movl	%r9d, %esi
	subl	%r8d, %esi
	movl	%ebp, %edi
	movl	4(%rsp), %r14d          # 4-byte Reload
	subl	%r14d, %edi
	cmpl	%edi, %esi
	jae	.LBB30_27
# BB#23:
	subl	%r13d, %edx
	je	.LBB30_33
# BB#24:                                # %.lr.ph.preheader
	movq	16(%rcx), %rsi
	movl	%edx, %r11d
	leaq	-1(%r11), %r10
	movq	%r11, %rdi
	andq	$3, %rdi
	je	.LBB30_28
# BB#25:                                # %.lr.ph.prol.preheader
	movl	%r13d, %ebx
	xorl	%ecx, %ecx
	movq	24(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB30_26:                              # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	(%rbx,%rcx), %ebp
	movzbl	(%rsi,%rbp), %edx
	leal	(%r12,%rcx), %ebp
	movb	%dl, (%rsi,%rbp)
	incq	%rcx
	cmpq	%rcx, %rdi
	jne	.LBB30_26
	jmp	.LBB30_29
.LBB30_27:
	movl	%ebp, %eax
	shrl	$3, %eax
	movl	%eax, (%rcx)
	andl	$7, %ebp
	movl	$8, %eax
	subl	%ebp, %eax
	movl	%eax, 4(%rcx)
	movl	(%rsp), %eax            # 4-byte Reload
	movb	%al, 8(%rcx)
	jmp	.LBB30_34
.LBB30_28:
	xorl	%ecx, %ecx
	movq	24(%rsp), %r12          # 8-byte Reload
.LBB30_29:                              # %.lr.ph.prol.loopexit
	cmpq	$3, %r10
	jb	.LBB30_32
# BB#30:                                # %.lr.ph.preheader.new
	subq	%rcx, %r11
	leaq	3(%r12,%rcx), %rbp
	movl	%r13d, %edx
	leaq	3(%rdx,%rcx), %rbx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB30_31:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	-3(%rbx,%rcx), %edx
	movzbl	(%rsi,%rdx), %edx
	leal	-3(%rbp,%rcx), %edi
	movb	%dl, (%rsi,%rdi)
	leal	-2(%rbx,%rcx), %edx
	movzbl	(%rsi,%rdx), %edx
	leal	-2(%rbp,%rcx), %edi
	movb	%dl, (%rsi,%rdi)
	leal	-1(%rbx,%rcx), %edx
	movzbl	(%rsi,%rdx), %edx
	leal	-1(%rbp,%rcx), %edi
	movb	%dl, (%rsi,%rdi)
	leal	(%rbx,%rcx), %edx
	movzbl	(%rsi,%rdx), %edx
	leal	(%rbp,%rcx), %edi
	movb	%dl, (%rsi,%rdi)
	addq	$4, %rcx
	cmpq	%rcx, %r11
	jne	.LBB30_31
.LBB30_32:                              # %._crit_edge.loopexit
	movq	32(%r15), %rcx
.LBB30_33:                              # %._crit_edge
	movq	8(%rsp), %rsi           # 8-byte Reload
	subl	%r8d, %r14d
	addl	%r9d, %r14d
	movl	%r14d, %edx
	shrl	$3, %edx
	movl	%edx, (%rcx)
	andl	$7, %r14d
	movl	$8, %edx
	subl	%r14d, %edx
	movl	%edx, 4(%rcx)
	movl	%esi, %ecx
	incl	%ecx
	movl	%ecx, 36072(%r15)
	movl	%eax, 31976(%r15,%rsi,4)
.LBB30_34:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end30:
	.size	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj, .Lfunc_end30-_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock2EPKhjj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoder10WriteBytesEPKhjh
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder10WriteBytesEPKhjh,@function
_ZN9NCompress6NBZip28CEncoder10WriteBytesEPKhjh: # @_ZN9NCompress6NBZip28CEncoder10WriteBytesEPKhjh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi192:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi193:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi195:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi196:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi198:
	.cfi_def_cfa_offset 80
.Lcfi199:
	.cfi_offset %rbx, -56
.Lcfi200:
	.cfi_offset %r12, -48
.Lcfi201:
	.cfi_offset %r13, -40
.Lcfi202:
	.cfi_offset %r14, -32
.Lcfi203:
	.cfi_offset %r15, -24
.Lcfi204:
	.cfi_offset %rbp, -16
	movl	%ecx, (%rsp)            # 4-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %eax
	shrl	$3, %eax
	je	.LBB31_5
# BB#1:                                 # %.lr.ph
	leaq	352(%rbx), %r13
	movl	408(%rbx), %ecx
	movl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB31_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_3 Depth 2
	movq	16(%rsp), %rax          # 8-byte Reload
	movzbl	(%rax,%r15), %r14d
	movl	$8, %eax
	.p2align	4, 0x90
.LBB31_3:                               #   Parent Loop BB31_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebp
	subl	%ecx, %ebp
	jb	.LBB31_4
# BB#13:                                #   in Loop: Header=BB31_3 Depth=2
	movl	%r14d, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r12d
	shll	%cl, %r12d
	orl	412(%rbx), %eax
	movq	352(%rbx), %rcx
	movl	360(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	360(%rbx), %eax
	cmpl	364(%rbx), %eax
	jne	.LBB31_15
# BB#14:                                #   in Loop: Header=BB31_3 Depth=2
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB31_15:                              # %_ZN10COutBuffer9WriteByteEh.exit.i
                                        #   in Loop: Header=BB31_3 Depth=2
	subl	%r12d, %r14d
	movl	$8, 408(%rbx)
	movb	$0, 412(%rbx)
	movl	$8, %ecx
	testl	%ebp, %ebp
	movl	%ebp, %eax
	jne	.LBB31_3
	jmp	.LBB31_16
	.p2align	4, 0x90
.LBB31_4:                               #   in Loop: Header=BB31_2 Depth=1
	movzbl	%r14b, %edx
	subl	%eax, %ecx
	movl	%ecx, 408(%rbx)
	shll	%cl, %edx
	orl	412(%rbx), %edx
	movb	%dl, 412(%rbx)
.LBB31_16:                              # %_ZN12CBitmEncoderI10COutBufferE9WriteBitsEjj.exit
                                        #   in Loop: Header=BB31_2 Depth=1
	incq	%r15
	cmpq	8(%rsp), %r15           # 8-byte Folded Reload
	jne	.LBB31_2
.LBB31_5:                               # %._crit_edge
	movl	4(%rsp), %edx           # 4-byte Reload
	andl	$7, %edx
	je	.LBB31_12
# BB#6:                                 # %.lr.ph.i.i
	movzbl	(%rsp), %r15d           # 1-byte Folded Reload
	leaq	352(%rbx), %r14
	movl	408(%rbx), %ecx
	.p2align	4, 0x90
.LBB31_7:                               # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	subl	%ecx, %ebp
	jb	.LBB31_8
# BB#9:                                 #   in Loop: Header=BB31_7 Depth=1
	movl	%r15d, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r12d
	shll	%cl, %r12d
	orl	412(%rbx), %eax
	movq	352(%rbx), %rcx
	movl	360(%rbx), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%rbx)
	movb	%al, (%rcx,%rdx)
	movl	360(%rbx), %eax
	cmpl	364(%rbx), %eax
	jne	.LBB31_11
# BB#10:                                #   in Loop: Header=BB31_7 Depth=1
	movq	%r14, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.LBB31_11:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i
                                        #   in Loop: Header=BB31_7 Depth=1
	subl	%r12d, %r15d
	movl	$8, 408(%rbx)
	movb	$0, 412(%rbx)
	movl	$8, %ecx
	testl	%ebp, %ebp
	movl	%ebp, %edx
	jne	.LBB31_7
	jmp	.LBB31_12
.LBB31_8:
	movzbl	%r15b, %eax
	subl	%edx, %ecx
	movl	%ecx, 408(%rbx)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	412(%rbx), %eax
	movb	%al, 412(%rbx)
.LBB31_12:                              # %_ZN9NCompress6NBZip28CEncoder9WriteBitsEjj.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZN9NCompress6NBZip28CEncoder10WriteBytesEPKhjh, .Lfunc_end31-_ZN9NCompress6NBZip28CEncoder10WriteBytesEPKhjh
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI32_0:
	.quad	900000                  # 0xdbba0
	.quad	2700002                 # 0x2932e2
	.text
	.globl	_ZN9NCompress6NBZip28CEncoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress6NBZip28CEncoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress6NBZip28CEncoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%rbp
.Lcfi205:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi206:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi207:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi208:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi209:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi211:
	.cfi_def_cfa_offset 96
.Lcfi212:
	.cfi_offset %rbx, -56
.Lcfi213:
	.cfi_offset %r12, -48
.Lcfi214:
	.cfi_offset %r13, -40
.Lcfi215:
	.cfi_offset %r14, -32
.Lcfi216:
	.cfi_offset %r15, -24
.Lcfi217:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%rdx, %rbp
	movq	%rsi, %r13
	movq	%rdi, %r14
	movq	%rbx, 704(%r14)
	callq	_ZN9NCompress6NBZip28CEncoder6CreateEv
	movl	%eax, %r12d
	testl	%r12d, %r12d
	je	.LBB32_2
.LBB32_1:                               # %_ZN9NCompress6NBZip28CEncoder8CFlusherD2Ev.exit193
	movl	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB32_2:                               # %.preheader
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 576(%r14)
	je	.LBB32_14
# BB#3:                                 # %.lr.ph358
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB32_4:                               # =>This Inner Loop Header: Depth=1
	movq	424(%r14), %r15
	movl	%ebx, %ebx
	cmpb	$0, 580(%r14)
	je	.LBB32_8
# BB#5:                                 #   in Loop: Header=BB32_4 Depth=1
	imulq	$36688, %rbx, %rbp      # imm = 0x8F50
	leaq	36112(%r15,%rbp), %rdi
	callq	Event_Reset
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB32_1
# BB#6:                                 #   in Loop: Header=BB32_4 Depth=1
	leaq	36216(%r15,%rbp), %rdi
	callq	Event_Reset
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB32_1
# BB#7:                                 #   in Loop: Header=BB32_4 Depth=1
	leaq	36320(%r15,%rbp), %rdi
	callq	Event_Reset
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB32_1
.LBB32_8:                               #   in Loop: Header=BB32_4 Depth=1
	movzbl	32(%r14), %eax
	imulq	$36688, %rbx, %rbp      # imm = 0x8F50
	movb	%al, 36080(%r15,%rbp)
	cmpq	$0, 24(%r15,%rbp)
	jne	.LBB32_10
# BB#9:                                 #   in Loop: Header=BB32_4 Depth=1
	leaq	24(%r15,%rbp), %r12
	movl	$7462144, %edi          # imm = 0x71DD00
	callq	BigAlloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB32_58
.LBB32_10:                              #   in Loop: Header=BB32_4 Depth=1
	cmpq	$0, (%r15,%rbp)
	jne	.LBB32_13
# BB#11:                                #   in Loop: Header=BB32_4 Depth=1
	leaq	(%r15,%rbp), %r12
	movl	$4610480, %edi          # imm = 0x4659B0
	callq	MidAlloc
	movq	%rax, (%r12)
	testq	%rax, %rax
	je	.LBB32_58
# BB#12:                                #   in Loop: Header=BB32_4 Depth=1
	movd	%rax, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	paddq	.LCPI32_0(%rip), %xmm0
	movdqu	%xmm0, 8(%r15,%rbp)
.LBB32_13:                              # %.thread
                                        #   in Loop: Header=BB32_4 Depth=1
	incl	%ebx
	cmpl	576(%r14), %ebx
	jb	.LBB32_4
.LBB32_14:                              # %._crit_edge
	leaq	48(%r14), %r15
	movl	$131072, %esi           # imm = 0x20000
	movq	%r15, %rdi
	callq	_ZN9CInBuffer6CreateEj
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	testb	%al, %al
	je	.LBB32_1
# BB#15:
	leaq	352(%r14), %rbx
	movl	$131072, %esi           # imm = 0x20000
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer6CreateEj
	testb	%al, %al
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB32_1
# BB#16:
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_ZN9CInBuffer9SetStreamEP19ISequentialInStream
	movq	%r15, %rdi
	callq	_ZN9CInBuffer4InitEv
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN10COutBuffer9SetStreamEP20ISequentialOutStream
	movq	%rbx, %rdi
	callq	_ZN10COutBuffer4InitEv
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$0, 420(%r14)
	movl	$0, 584(%r14)
	movw	$0, 588(%r14)
	leaq	592(%r14), %r12
.Ltmp128:
	movq	%r12, %rdi
	callq	Event_Reset
.Ltmp129:
	movq	%rbx, %r13
# BB#17:                                # %_ZN8NWindows16NSynchronization10CBaseEvent5ResetEv.exit
	movl	408(%r14), %ecx
	movl	$8, %edx
	movl	$66, %ebx
	.p2align	4, 0x90
.LBB32_18:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	subl	%ecx, %ebp
	jb	.LBB32_23
# BB#19:                                #   in Loop: Header=BB32_18 Depth=1
	movl	%ebx, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r15d
	shll	%cl, %r15d
	orl	412(%r14), %eax
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_21
# BB#20:                                #   in Loop: Header=BB32_18 Depth=1
.Ltmp130:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp131:
.LBB32_21:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i
                                        #   in Loop: Header=BB32_18 Depth=1
	subl	%r15d, %ebx
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %ecx
	testl	%ebp, %ebp
	movl	%ebp, %edx
	jne	.LBB32_18
# BB#22:
	xorl	%eax, %eax
	jmp	.LBB32_24
.LBB32_58:
	movl	$-2147024882, %r12d     # imm = 0x8007000E
	jmp	.LBB32_1
.LBB32_23:
	movzbl	%bl, %eax
	subl	%edx, %ecx
	movl	%ecx, 408(%r14)
	shll	%cl, %eax
	orl	412(%r14), %eax
	movb	%al, 412(%r14)
.LBB32_24:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit.preheader
	movl	$8, %edx
	movl	$90, %r15d
	.p2align	4, 0x90
.LBB32_25:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	subl	%ecx, %ebp
	jb	.LBB32_29
# BB#26:                                #   in Loop: Header=BB32_25 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%al, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_28
# BB#27:                                #   in Loop: Header=BB32_25 Depth=1
.Ltmp133:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp134:
.LBB32_28:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i120
                                        #   in Loop: Header=BB32_25 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %ecx
	xorl	%eax, %eax
	testl	%ebp, %ebp
	movl	%ebp, %edx
	jne	.LBB32_25
	jmp	.LBB32_30
.LBB32_29:
	movzbl	%r15b, %esi
	subl	%edx, %ecx
	movl	%ecx, 408(%r14)
	shll	%cl, %esi
	movzbl	%al, %eax
	orl	%esi, %eax
	movb	%al, 412(%r14)
.LBB32_30:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit122.preheader
	movl	$8, %edx
	movl	$104, %r15d
	.p2align	4, 0x90
.LBB32_31:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit122
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	subl	%ecx, %ebp
	jb	.LBB32_35
# BB#32:                                #   in Loop: Header=BB32_31 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%al, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_34
# BB#33:                                #   in Loop: Header=BB32_31 Depth=1
.Ltmp136:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp137:
.LBB32_34:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i126
                                        #   in Loop: Header=BB32_31 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %ecx
	xorl	%eax, %eax
	testl	%ebp, %ebp
	movl	%ebp, %edx
	jne	.LBB32_31
	jmp	.LBB32_36
.LBB32_35:
	movzbl	%r15b, %esi
	subl	%edx, %ecx
	movl	%ecx, 408(%r14)
	shll	%cl, %esi
	movzbl	%al, %eax
	orl	%esi, %eax
	movb	%al, 412(%r14)
.LBB32_36:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit128
	movl	28(%r14), %edx
	addl	$48, %edx
	movzbl	%dl, %r15d
	movl	$8, %edx
	.p2align	4, 0x90
.LBB32_37:                              # =>This Inner Loop Header: Depth=1
	movl	%edx, %ebp
	subl	%ecx, %ebp
	jb	.LBB32_41
# BB#38:                                #   in Loop: Header=BB32_37 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%al, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_40
# BB#39:                                #   in Loop: Header=BB32_37 Depth=1
.Ltmp139:
	movq	%r13, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp140:
.LBB32_40:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i132
                                        #   in Loop: Header=BB32_37 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %ecx
	xorl	%eax, %eax
	testl	%ebp, %ebp
	movl	%ebp, %edx
	jne	.LBB32_37
	jmp	.LBB32_42
.LBB32_41:
	movzbl	%r15b, %esi
	subl	%edx, %ecx
	movl	%ecx, 408(%r14)
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movzbl	%al, %eax
	orl	%esi, %eax
	movb	%al, 412(%r14)
.LBB32_42:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit134
	cmpb	$0, 580(%r14)
	je	.LBB32_59
# BB#43:
	movq	424(%r14), %rdi
	addq	$36320, %rdi            # imm = 0x8DE0
.Ltmp142:
	callq	Event_Set
.Ltmp143:
# BB#44:                                # %_ZN8NWindows16NSynchronization10CBaseEvent3SetEv.exit
	movl	$0, 696(%r14)
	leaq	432(%r14), %rbp
.Ltmp144:
	movq	%rbp, %rdi
	callq	Event_Set
.Ltmp145:
# BB#45:                                # %_ZN8NWindows16NSynchronization10CBaseEvent3SetEv.exit137.preheader
	cmpl	$0, 576(%r14)
	je	.LBB32_49
# BB#46:                                # %.lr.ph356.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB32_47:                              # %.lr.ph356
                                        # =>This Inner Loop Header: Depth=1
	movq	424(%r14), %rax
	movl	%ebx, %ecx
	imulq	$36688, %rcx, %rcx      # imm = 0x8F50
	leaq	36112(%rax,%rcx), %rdi
.Ltmp146:
	callq	Event_Wait
.Ltmp147:
# BB#48:                                # %_ZN8NWindows16NSynchronization10CBaseEvent4LockEv.exit
                                        #   in Loop: Header=BB32_47 Depth=1
	incl	%ebx
	cmpl	576(%r14), %ebx
	jb	.LBB32_47
.LBB32_49:                              # %_ZN8NWindows16NSynchronization10CBaseEvent3SetEv.exit137._crit_edge
.Ltmp149:
	movq	%rbp, %rdi
	callq	Event_Reset
.Ltmp150:
# BB#50:                                # %_ZN8NWindows16NSynchronization10CBaseEvent5ResetEv.exit140
.Ltmp151:
	movq	%r12, %rdi
	callq	Event_Set
.Ltmp152:
# BB#51:                                # %_ZN8NWindows16NSynchronization10CBaseEvent3SetEv.exit142.preheader
	cmpl	$0, 576(%r14)
	je	.LBB32_55
# BB#52:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB32_53:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	424(%r14), %rax
	movl	%ebx, %ecx
	imulq	$36688, %rcx, %rcx      # imm = 0x8F50
	leaq	36216(%rax,%rcx), %rdi
.Ltmp153:
	callq	Event_Wait
.Ltmp154:
# BB#54:                                # %_ZN8NWindows16NSynchronization10CBaseEvent4LockEv.exit144
                                        #   in Loop: Header=BB32_53 Depth=1
	incl	%ebx
	cmpl	576(%r14), %ebx
	jb	.LBB32_53
.LBB32_55:                              # %_ZN8NWindows16NSynchronization10CBaseEvent3SetEv.exit142._crit_edge
.Ltmp156:
	movq	%r12, %rdi
	callq	Event_Reset
.Ltmp157:
# BB#56:                                # %_ZN8NWindows16NSynchronization10CBaseEvent5ResetEv.exit146
	movq	%r13, %r15
	movl	696(%r14), %r12d
	testl	%r12d, %r12d
	jne	.LBB32_125
	jmp	.LBB32_72
.LBB32_59:                              # %.thread203.preheader
	movq	%r13, %r15
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB32_68
# BB#60:
	leaq	32(%rsp), %rbx
	leaq	24(%rsp), %r13
.LBB32_61:                              # %.thread203.outer
                                        # =>This Inner Loop Header: Depth=1
	movq	424(%r14), %rbp
	movq	(%rbp), %rsi
.Ltmp159:
	movq	%r14, %rdi
	callq	_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh
.Ltmp160:
# BB#62:                                #   in Loop: Header=BB32_61 Depth=1
	testl	%eax, %eax
	je	.LBB32_72
# BB#63:                                #   in Loop: Header=BB32_61 Depth=1
.Ltmp162:
	movq	%rbp, %rdi
	movl	%eax, %esi
	callq	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej
	movl	%eax, %r12d
.Ltmp163:
# BB#64:                                #   in Loop: Header=BB32_61 Depth=1
	testl	%r12d, %r12d
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB32_125
# BB#65:                                # %.us-lcssa339
                                        #   in Loop: Header=BB32_61 Depth=1
	movq	48(%r14), %rax
	addq	80(%r14), %rax
	subq	64(%r14), %rax
	movq	%rax, 32(%rsp)
.Ltmp165:
	movq	%r15, %rdi
	callq	_ZNK10COutBuffer16GetProcessedSizeEv
.Ltmp166:
# BB#66:                                #   in Loop: Header=BB32_61 Depth=1
	movl	$15, %ecx
	subl	408(%r14), %ecx
	shrl	$3, %ecx
	addq	%rax, %rcx
	movq	%rcx, 24(%rsp)
	movq	(%rbp), %rax
.Ltmp168:
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp169:
# BB#67:                                #   in Loop: Header=BB32_61 Depth=1
	testl	%r12d, %r12d
	je	.LBB32_61
	jmp	.LBB32_125
.LBB32_68:                              # %.thread203.us.us
                                        # =>This Inner Loop Header: Depth=1
	movq	424(%r14), %rbp
	movq	(%rbp), %rsi
.Ltmp171:
	movq	%r14, %rdi
	callq	_ZN9NCompress6NBZip28CEncoder12ReadRleBlockEPh
.Ltmp172:
# BB#69:                                #   in Loop: Header=BB32_68 Depth=1
	testl	%eax, %eax
	je	.LBB32_72
# BB#70:                                #   in Loop: Header=BB32_68 Depth=1
.Ltmp174:
	movq	%rbp, %rdi
	movl	%eax, %esi
	callq	_ZN9NCompress6NBZip211CThreadInfo12EncodeBlock3Ej
	movl	%eax, %r12d
.Ltmp175:
# BB#71:                                #   in Loop: Header=BB32_68 Depth=1
	testl	%r12d, %r12d
	je	.LBB32_68
	jmp	.LBB32_125
.LBB32_72:                              # %.thread204
	movl	408(%r14), %eax
	movl	$8, %ecx
	movl	$23, %ebx
	movq	%r15, %r12
	.p2align	4, 0x90
.LBB32_73:                              # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_78
# BB#74:                                #   in Loop: Header=BB32_73 Depth=1
	movl	%ebx, %eax
	movl	%ebp, %ecx
	shrl	%cl, %eax
	movl	%eax, %r15d
	shll	%cl, %r15d
	orl	412(%r14), %eax
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_76
# BB#75:                                #   in Loop: Header=BB32_73 Depth=1
.Ltmp177:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp178:
.LBB32_76:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i153
                                        #   in Loop: Header=BB32_73 Depth=1
	subl	%r15d, %ebx
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_73
# BB#77:
	xorl	%edx, %edx
	jmp	.LBB32_79
.LBB32_78:
	movzbl	%bl, %edx
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %edx
	orl	412(%r14), %edx
	movb	%dl, 412(%r14)
.LBB32_79:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit155.preheader
	movl	$8, %ecx
	movl	$114, %r15d
	.p2align	4, 0x90
.LBB32_80:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit155
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_84
# BB#81:                                #   in Loop: Header=BB32_80 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%dl, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_83
# BB#82:                                #   in Loop: Header=BB32_80 Depth=1
.Ltmp180:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp181:
.LBB32_83:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i159
                                        #   in Loop: Header=BB32_80 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	xorl	%edx, %edx
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_80
	jmp	.LBB32_85
.LBB32_84:
	movzbl	%r15b, %esi
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movb	%dl, 412(%r14)
.LBB32_85:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit161.preheader
	movl	$8, %ecx
	movl	$69, %r15d
	.p2align	4, 0x90
.LBB32_86:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit161
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_90
# BB#87:                                #   in Loop: Header=BB32_86 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%dl, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_89
# BB#88:                                #   in Loop: Header=BB32_86 Depth=1
.Ltmp183:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp184:
.LBB32_89:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i165
                                        #   in Loop: Header=BB32_86 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	xorl	%edx, %edx
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_86
	jmp	.LBB32_91
.LBB32_90:
	movzbl	%r15b, %esi
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movb	%dl, 412(%r14)
.LBB32_91:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit167.preheader
	movl	$8, %ecx
	movl	$56, %r15d
	.p2align	4, 0x90
.LBB32_92:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit167
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_96
# BB#93:                                #   in Loop: Header=BB32_92 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%dl, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_95
# BB#94:                                #   in Loop: Header=BB32_92 Depth=1
.Ltmp186:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp187:
.LBB32_95:                              # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i171
                                        #   in Loop: Header=BB32_92 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	xorl	%edx, %edx
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_92
	jmp	.LBB32_97
.LBB32_96:
	movzbl	%r15b, %esi
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movb	%dl, 412(%r14)
.LBB32_97:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit173.preheader
	movl	$8, %ecx
	movl	$80, %r15d
	.p2align	4, 0x90
.LBB32_98:                              # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit173
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_102
# BB#99:                                #   in Loop: Header=BB32_98 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%dl, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_101
# BB#100:                               #   in Loop: Header=BB32_98 Depth=1
.Ltmp189:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp190:
.LBB32_101:                             # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i177
                                        #   in Loop: Header=BB32_98 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	xorl	%edx, %edx
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_98
	jmp	.LBB32_103
.LBB32_102:
	movzbl	%r15b, %esi
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movb	%dl, 412(%r14)
.LBB32_103:                             # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit179.preheader
	movl	$8, %ecx
	movl	$144, %r15d
	.p2align	4, 0x90
.LBB32_104:                             # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit179
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_108
# BB#105:                               #   in Loop: Header=BB32_104 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%dl, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_107
# BB#106:                               #   in Loop: Header=BB32_104 Depth=1
.Ltmp192:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp193:
.LBB32_107:                             # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i183
                                        #   in Loop: Header=BB32_104 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	xorl	%edx, %edx
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_104
	jmp	.LBB32_109
.LBB32_108:
	movzbl	%r15b, %esi
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movb	%dl, 412(%r14)
.LBB32_109:                             # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit185
	movq	%r12, %r15
	movl	420(%r14), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB32_110:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB32_111 Depth 2
	leal	(,%r12,8), %esi
	movl	$24, %ecx
	subl	%esi, %ecx
	movl	8(%rsp), %esi           # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movzbl	%sil, %r13d
	movl	$8, %ecx
	.p2align	4, 0x90
.LBB32_111:                             #   Parent Loop BB32_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_115
# BB#112:                               #   in Loop: Header=BB32_111 Depth=2
	movl	%r13d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%dl, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_114
# BB#113:                               #   in Loop: Header=BB32_111 Depth=2
.Ltmp195:
	movq	%r15, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp196:
.LBB32_114:                             # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i.i
                                        #   in Loop: Header=BB32_111 Depth=2
	subl	%ebx, %r13d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	xorl	%edx, %edx
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_111
	jmp	.LBB32_116
	.p2align	4, 0x90
.LBB32_115:                             #   in Loop: Header=BB32_110 Depth=1
	movzbl	%r13b, %esi
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %esi
	movzbl	%dl, %edx
	orl	%esi, %edx
	movb	%dl, 412(%r14)
.LBB32_116:                             # %_ZN9NCompress6NBZip28CEncoder9WriteByteEh.exit.i
                                        #   in Loop: Header=BB32_110 Depth=1
	incl	%r12d
	cmpl	$4, %r12d
	jne	.LBB32_110
# BB#117:                               # %_ZN9NCompress6NBZip28CEncoder8WriteCrcEj.exit
	leal	-1(%rax), %ecx
	cmpl	$6, %ecx
	movq	%r15, %r12
	ja	.LBB32_124
# BB#118:                               # %.lr.ph.i.i.i.preheader
	xorl	%r15d, %r15d
	movl	%eax, %ecx
.LBB32_119:                             # %.lr.ph.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %ebp
	subl	%eax, %ebp
	jb	.LBB32_123
# BB#120:                               #   in Loop: Header=BB32_119 Depth=1
	movl	%r15d, %ebx
	movl	%ebp, %ecx
	shrl	%cl, %ebx
	movzbl	%dl, %eax
	orl	%ebx, %eax
	shll	%cl, %ebx
	movq	352(%r14), %rcx
	movl	360(%r14), %edx
	leal	1(%rdx), %esi
	movl	%esi, 360(%r14)
	movb	%al, (%rcx,%rdx)
	movl	360(%r14), %eax
	cmpl	364(%r14), %eax
	jne	.LBB32_122
# BB#121:                               #   in Loop: Header=BB32_119 Depth=1
.Ltmp198:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer14FlushWithCheckEv
.Ltmp199:
.LBB32_122:                             # %_ZN10COutBuffer9WriteByteEh.exit.i.i.i189
                                        #   in Loop: Header=BB32_119 Depth=1
	subl	%ebx, %r15d
	movl	$8, 408(%r14)
	movb	$0, 412(%r14)
	movl	$8, %eax
	xorl	%edx, %edx
	testl	%ebp, %ebp
	movl	%ebp, %ecx
	jne	.LBB32_119
	jmp	.LBB32_124
.LBB32_123:
	movzbl	%r15b, %esi
	subl	%ecx, %eax
	movl	%eax, 408(%r14)
	movl	%eax, %ecx
	shll	%cl, %esi
	movzbl	%dl, %eax
	orl	%esi, %eax
	movb	%al, 412(%r14)
.LBB32_124:                             # %_ZN12CBitmEncoderI10COutBufferE5FlushEv.exit.i
.Ltmp201:
	movq	%r12, %rdi
	callq	_ZN10COutBuffer5FlushEv
	movl	%eax, %r12d
.Ltmp202:
.LBB32_125:                             # %_ZN9NCompress6NBZip28CEncoder5FlushEv.exit
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB32_127
# BB#126:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 72(%r14)
.LBB32_127:                             # %_ZN9CInBuffer13ReleaseStreamEv.exit.i.i192
	movq	376(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB32_1
# BB#128:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 376(%r14)
	jmp	.LBB32_1
.LBB32_129:                             # %.loopexit
.Ltmp200:
	jmp	.LBB32_151
.LBB32_130:                             # %.loopexit.split-lp227.loopexit.split-lp
.Ltmp158:
	jmp	.LBB32_151
.LBB32_131:                             # %.us-lcssa337.us.us-lcssa.us
.Ltmp176:
	jmp	.LBB32_151
.LBB32_132:                             # %.us-lcssa.us.us-lcssa.us
.Ltmp173:
	jmp	.LBB32_151
.LBB32_133:                             # %.us-lcssa343
.Ltmp170:
	jmp	.LBB32_151
.LBB32_134:                             # %.us-lcssa341
.Ltmp167:
	jmp	.LBB32_151
.LBB32_135:                             # %.us-lcssa337.us-lcssa
.Ltmp164:
	jmp	.LBB32_151
.LBB32_136:                             # %.us-lcssa.us-lcssa
.Ltmp161:
	jmp	.LBB32_151
.LBB32_137:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp
.Ltmp203:
	jmp	.LBB32_151
.LBB32_138:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp194:
	jmp	.LBB32_151
.LBB32_139:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp191:
	jmp	.LBB32_151
.LBB32_140:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp188:
	jmp	.LBB32_151
.LBB32_141:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp185:
	jmp	.LBB32_151
.LBB32_142:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp182:
	jmp	.LBB32_151
.LBB32_143:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp179:
	jmp	.LBB32_151
.LBB32_144:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp141:
	jmp	.LBB32_151
.LBB32_145:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp138:
	jmp	.LBB32_151
.LBB32_146:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp135:
	jmp	.LBB32_151
.LBB32_147:                             # %.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit.split-lp.loopexit
.Ltmp132:
	jmp	.LBB32_151
.LBB32_148:                             # %.loopexit226
.Ltmp155:
	jmp	.LBB32_151
.LBB32_149:                             # %.loopexit.split-lp227.loopexit
.Ltmp148:
	jmp	.LBB32_151
.LBB32_150:                             # %.loopexit.split-lp.loopexit
.Ltmp197:
.LBB32_151:
	movq	%rax, %rbx
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB32_154
# BB#152:
	movq	(%rdi), %rax
.Ltmp204:
	callq	*16(%rax)
.Ltmp205:
# BB#153:                               # %.noexc147
	movq	$0, 72(%r14)
.LBB32_154:                             # %_ZN9CInBuffer13ReleaseStreamEv.exit.i.i
	movq	376(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB32_157
# BB#155:
	movq	(%rdi), %rax
.Ltmp206:
	callq	*16(%rax)
.Ltmp207:
# BB#156:                               # %.noexc148
	movq	$0, 376(%r14)
.LBB32_157:                             # %_ZN9NCompress6NBZip28CEncoder8CFlusherD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB32_158:
.Ltmp208:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end32:
	.size	_ZN9NCompress6NBZip28CEncoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end32-_ZN9NCompress6NBZip28CEncoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\202\203\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\371\002"              # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp128-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp128
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp203-.Lfunc_begin9  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin9  #     jumps to .Ltmp132
	.byte	0                       #   On action: cleanup
	.long	.Ltmp133-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin9  #     jumps to .Ltmp135
	.byte	0                       #   On action: cleanup
	.long	.Ltmp136-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin9  #     jumps to .Ltmp138
	.byte	0                       #   On action: cleanup
	.long	.Ltmp139-.Lfunc_begin9  # >> Call Site 6 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin9  #     jumps to .Ltmp141
	.byte	0                       #   On action: cleanup
	.long	.Ltmp142-.Lfunc_begin9  # >> Call Site 7 <<
	.long	.Ltmp145-.Ltmp142       #   Call between .Ltmp142 and .Ltmp145
	.long	.Ltmp203-.Lfunc_begin9  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin9  # >> Call Site 8 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin9  #     jumps to .Ltmp148
	.byte	0                       #   On action: cleanup
	.long	.Ltmp149-.Lfunc_begin9  # >> Call Site 9 <<
	.long	.Ltmp152-.Ltmp149       #   Call between .Ltmp149 and .Ltmp152
	.long	.Ltmp158-.Lfunc_begin9  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin9  # >> Call Site 10 <<
	.long	.Ltmp154-.Ltmp153       #   Call between .Ltmp153 and .Ltmp154
	.long	.Ltmp155-.Lfunc_begin9  #     jumps to .Ltmp155
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin9  # >> Call Site 11 <<
	.long	.Ltmp157-.Ltmp156       #   Call between .Ltmp156 and .Ltmp157
	.long	.Ltmp158-.Lfunc_begin9  #     jumps to .Ltmp158
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin9  # >> Call Site 12 <<
	.long	.Ltmp160-.Ltmp159       #   Call between .Ltmp159 and .Ltmp160
	.long	.Ltmp161-.Lfunc_begin9  #     jumps to .Ltmp161
	.byte	0                       #   On action: cleanup
	.long	.Ltmp162-.Lfunc_begin9  # >> Call Site 13 <<
	.long	.Ltmp163-.Ltmp162       #   Call between .Ltmp162 and .Ltmp163
	.long	.Ltmp164-.Lfunc_begin9  #     jumps to .Ltmp164
	.byte	0                       #   On action: cleanup
	.long	.Ltmp165-.Lfunc_begin9  # >> Call Site 14 <<
	.long	.Ltmp166-.Ltmp165       #   Call between .Ltmp165 and .Ltmp166
	.long	.Ltmp167-.Lfunc_begin9  #     jumps to .Ltmp167
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin9  # >> Call Site 15 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin9  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin9  # >> Call Site 16 <<
	.long	.Ltmp172-.Ltmp171       #   Call between .Ltmp171 and .Ltmp172
	.long	.Ltmp173-.Lfunc_begin9  #     jumps to .Ltmp173
	.byte	0                       #   On action: cleanup
	.long	.Ltmp174-.Lfunc_begin9  # >> Call Site 17 <<
	.long	.Ltmp175-.Ltmp174       #   Call between .Ltmp174 and .Ltmp175
	.long	.Ltmp176-.Lfunc_begin9  #     jumps to .Ltmp176
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin9  # >> Call Site 18 <<
	.long	.Ltmp178-.Ltmp177       #   Call between .Ltmp177 and .Ltmp178
	.long	.Ltmp179-.Lfunc_begin9  #     jumps to .Ltmp179
	.byte	0                       #   On action: cleanup
	.long	.Ltmp180-.Lfunc_begin9  # >> Call Site 19 <<
	.long	.Ltmp181-.Ltmp180       #   Call between .Ltmp180 and .Ltmp181
	.long	.Ltmp182-.Lfunc_begin9  #     jumps to .Ltmp182
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin9  # >> Call Site 20 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin9  #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin9  # >> Call Site 21 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin9  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin9  # >> Call Site 22 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin9  #     jumps to .Ltmp191
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin9  # >> Call Site 23 <<
	.long	.Ltmp193-.Ltmp192       #   Call between .Ltmp192 and .Ltmp193
	.long	.Ltmp194-.Lfunc_begin9  #     jumps to .Ltmp194
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin9  # >> Call Site 24 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin9  #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin9  # >> Call Site 25 <<
	.long	.Ltmp199-.Ltmp198       #   Call between .Ltmp198 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin9  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp201-.Lfunc_begin9  # >> Call Site 26 <<
	.long	.Ltmp202-.Ltmp201       #   Call between .Ltmp201 and .Ltmp202
	.long	.Ltmp203-.Lfunc_begin9  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin9  # >> Call Site 27 <<
	.long	.Ltmp204-.Ltmp202       #   Call between .Ltmp202 and .Ltmp204
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp204-.Lfunc_begin9  # >> Call Site 28 <<
	.long	.Ltmp207-.Ltmp204       #   Call between .Ltmp204 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin9  #     jumps to .Ltmp208
	.byte	1                       #   On action: 1
	.long	.Ltmp207-.Lfunc_begin9  # >> Call Site 29 <<
	.long	.Lfunc_end32-.Ltmp207   #   Call between .Ltmp207 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo,@function
_ZN9NCompress6NBZip28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo: # @_ZN9NCompress6NBZip28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbx
.Lcfi218:
	.cfi_def_cfa_offset 16
.Lcfi219:
	.cfi_offset %rbx, -16
.Ltmp209:
	callq	_ZN9NCompress6NBZip28CEncoder8CodeRealEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp210:
.LBB33_6:
	movl	%ebx, %eax
	popq	%rbx
	retq
.LBB33_1:
.Ltmp211:
	movq	%rdx, %rbx
	movq	%rax, %rdi
	cmpl	$3, %ebx
	jne	.LBB33_3
# BB#2:
	callq	__cxa_begin_catch
	jmp	.LBB33_4
.LBB33_3:
	callq	__cxa_begin_catch
	cmpl	$2, %ebx
	jne	.LBB33_5
.LBB33_4:
	movl	(%rax), %ebx
	callq	__cxa_end_catch
	jmp	.LBB33_6
.LBB33_5:
	callq	__cxa_end_catch
	movl	$1, %ebx
	jmp	.LBB33_6
.Lfunc_end33:
	.size	_ZN9NCompress6NBZip28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo, .Lfunc_end33-_ZN9NCompress6NBZip28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\256\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp209-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin10 #     jumps to .Ltmp211
	.byte	5                       #   On action: 3
	.long	.Ltmp210-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp210   #   Call between .Ltmp210 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
	.byte	3                       # >> Action Record 3 <<
                                        #   Catch TypeInfo 3
	.byte	125                     #   Continue to action 2
                                        # >> Catch TypeInfos <<
	.long	_ZTI18CInBufferException # TypeInfo 3
	.long	_ZTI19COutBufferException # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 16
.Lcfi221:
	.cfi_offset %rbx, -16
	testl	%ecx, %ecx
	je	.LBB34_15
# BB#1:                                 # %.lr.ph
	movl	%ecx, %r8d
	addq	$8, %rdx
	xorl	%ebx, %ebx
	movl	$1, %r9d
	movl	$10, %r10d
	.p2align	4, 0x90
.LBB34_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rbx,4), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %ecx
	je	.LBB34_7
# BB#3:                                 #   in Loop: Header=BB34_2 Depth=1
	cmpl	$13, %ecx
	je	.LBB34_12
# BB#4:                                 #   in Loop: Header=BB34_2 Depth=1
	cmpl	$11, %ecx
	jne	.LBB34_16
# BB#5:                                 #   in Loop: Header=BB34_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB34_16
# BB#6:                                 #   in Loop: Header=BB34_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	cmpl	$10, %eax
	cmovael	%r10d, %eax
	movl	%eax, 416(%rdi)
	cmpl	$1, %eax
	seta	32(%rdi)
	jmp	.LBB34_14
	.p2align	4, 0x90
.LBB34_7:                               #   in Loop: Header=BB34_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB34_16
# BB#8:                                 #   in Loop: Header=BB34_2 Depth=1
	movl	(%rdx), %eax
	movl	$1, %r11d
	cmpl	$100000, %eax           # imm = 0x186A0
	jb	.LBB34_11
# BB#9:                                 #   in Loop: Header=BB34_2 Depth=1
	movl	$9, %r11d
	cmpl	$999999, %eax           # imm = 0xF423F
	ja	.LBB34_11
# BB#10:                                # %select.false.sink
                                        #   in Loop: Header=BB34_2 Depth=1
	shrl	$5, %eax
	imulq	$175921861, %rax, %r11  # imm = 0xA7C5AC5
	shrq	$39, %r11
.LBB34_11:                              #   in Loop: Header=BB34_2 Depth=1
	movl	%r11d, 28(%rdi)
	jmp	.LBB34_14
	.p2align	4, 0x90
.LBB34_12:                              #   in Loop: Header=BB34_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB34_16
# BB#13:                                #   in Loop: Header=BB34_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	movl	%eax, 576(%rdi)
.LBB34_14:                              #   in Loop: Header=BB34_2 Depth=1
	incq	%rbx
	addq	$16, %rdx
	cmpq	%r8, %rbx
	jb	.LBB34_2
.LBB34_15:
	xorl	%eax, %eax
.LBB34_16:                              # %.thread
	popq	%rbx
	retq
.Lfunc_end34:
	.size	_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end34-_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZThn8_N9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj,@function
_ZThn8_N9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj: # @_ZThn8_N9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 16
.Lcfi223:
	.cfi_offset %rbx, -16
	testl	%ecx, %ecx
	je	.LBB35_15
# BB#1:                                 # %.lr.ph.i
	movl	%ecx, %r8d
	addq	$8, %rdx
	xorl	%ebx, %ebx
	movl	$1, %r9d
	movl	$10, %r10d
	.p2align	4, 0x90
.LBB35_2:                               # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rbx,4), %ecx
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %ecx
	je	.LBB35_7
# BB#3:                                 #   in Loop: Header=BB35_2 Depth=1
	cmpl	$13, %ecx
	je	.LBB35_12
# BB#4:                                 #   in Loop: Header=BB35_2 Depth=1
	cmpl	$11, %ecx
	jne	.LBB35_16
# BB#5:                                 #   in Loop: Header=BB35_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB35_16
# BB#6:                                 #   in Loop: Header=BB35_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	cmpl	$10, %eax
	cmovael	%r10d, %eax
	movl	%eax, 408(%rdi)
	cmpl	$1, %eax
	seta	24(%rdi)
	jmp	.LBB35_14
	.p2align	4, 0x90
.LBB35_7:                               #   in Loop: Header=BB35_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB35_16
# BB#8:                                 #   in Loop: Header=BB35_2 Depth=1
	movl	(%rdx), %eax
	movl	$1, %r11d
	cmpl	$100000, %eax           # imm = 0x186A0
	jb	.LBB35_11
# BB#9:                                 #   in Loop: Header=BB35_2 Depth=1
	movl	$9, %r11d
	cmpl	$999999, %eax           # imm = 0xF423F
	ja	.LBB35_11
# BB#10:                                # %select.false.sink
                                        #   in Loop: Header=BB35_2 Depth=1
	shrl	$5, %eax
	imulq	$175921861, %rax, %r11  # imm = 0xA7C5AC5
	shrq	$39, %r11
.LBB35_11:                              #   in Loop: Header=BB35_2 Depth=1
	movl	%r11d, 20(%rdi)
	jmp	.LBB35_14
	.p2align	4, 0x90
.LBB35_12:                              #   in Loop: Header=BB35_2 Depth=1
	movzwl	-8(%rdx), %ecx
	cmpl	$19, %ecx
	jne	.LBB35_16
# BB#13:                                #   in Loop: Header=BB35_2 Depth=1
	movl	(%rdx), %eax
	testl	%eax, %eax
	cmovel	%r9d, %eax
	movl	%eax, 568(%rdi)
.LBB35_14:                              #   in Loop: Header=BB35_2 Depth=1
	incq	%rbx
	addq	$16, %rdx
	cmpq	%r8, %rbx
	jb	.LBB35_2
.LBB35_15:
	xorl	%eax, %eax
.LBB35_16:                              # %_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj.exit
	popq	%rbx
	retq
.Lfunc_end35:
	.size	_ZThn8_N9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj, .Lfunc_end35-_ZThn8_N9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.cfi_endproc

	.globl	_ZN9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj,@function
_ZN9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj: # @_ZN9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	movl	$1, %eax
	cmovnel	%esi, %eax
	movl	%eax, 576(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end36:
	.size	_ZN9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj, .Lfunc_end36-_ZN9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.cfi_endproc

	.globl	_ZThn16_N9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj,@function
_ZThn16_N9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj: # @_ZThn16_N9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	movl	$1, %eax
	cmovnel	%esi, %eax
	movl	%eax, 560(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end37:
	.size	_ZThn16_N9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj, .Lfunc_end37-_ZThn16_N9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi224:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB38_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB38_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB38_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB38_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB38_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB38_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB38_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB38_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB38_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB38_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB38_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB38_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB38_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB38_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB38_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB38_16
.LBB38_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_ICompressSetCoderMt(%rip), %cl
	jne	.LBB38_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+1(%rip), %al
	jne	.LBB38_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+2(%rip), %al
	jne	.LBB38_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+3(%rip), %al
	jne	.LBB38_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+4(%rip), %al
	jne	.LBB38_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+5(%rip), %al
	jne	.LBB38_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+6(%rip), %al
	jne	.LBB38_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+7(%rip), %al
	jne	.LBB38_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+8(%rip), %al
	jne	.LBB38_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+9(%rip), %al
	jne	.LBB38_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+10(%rip), %al
	jne	.LBB38_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+11(%rip), %al
	jne	.LBB38_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+12(%rip), %al
	jne	.LBB38_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+13(%rip), %al
	jne	.LBB38_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+14(%rip), %al
	jne	.LBB38_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit8
	movb	15(%rsi), %al
	cmpb	IID_ICompressSetCoderMt+15(%rip), %al
	jne	.LBB38_33
.LBB38_16:
	leaq	16(%rdi), %rax
	jmp	.LBB38_50
.LBB38_33:                              # %_ZeqRK4GUIDS1_.exit8.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_ICompressSetCoderProperties(%rip), %cl
	jne	.LBB38_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+1(%rip), %cl
	jne	.LBB38_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+2(%rip), %cl
	jne	.LBB38_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+3(%rip), %cl
	jne	.LBB38_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+4(%rip), %cl
	jne	.LBB38_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+5(%rip), %cl
	jne	.LBB38_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+6(%rip), %cl
	jne	.LBB38_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+7(%rip), %cl
	jne	.LBB38_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+8(%rip), %cl
	jne	.LBB38_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+9(%rip), %cl
	jne	.LBB38_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+10(%rip), %cl
	jne	.LBB38_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+11(%rip), %cl
	jne	.LBB38_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+12(%rip), %cl
	jne	.LBB38_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+13(%rip), %cl
	jne	.LBB38_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+14(%rip), %cl
	jne	.LBB38_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit10
	movb	15(%rsi), %cl
	cmpb	IID_ICompressSetCoderProperties+15(%rip), %cl
	jne	.LBB38_51
# BB#49:
	leaq	8(%rdi), %rax
.LBB38_50:                              # %_ZeqRK4GUIDS1_.exit10.thread
	movq	%rax, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB38_51:                              # %_ZeqRK4GUIDS1_.exit10.thread
	popq	%rcx
	retq
.Lfunc_end38:
	.size	_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end38-_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip28CEncoder6AddRefEv,"axG",@progbits,_ZN9NCompress6NBZip28CEncoder6AddRefEv,comdat
	.weak	_ZN9NCompress6NBZip28CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder6AddRefEv,@function
_ZN9NCompress6NBZip28CEncoder6AddRefEv: # @_ZN9NCompress6NBZip28CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	24(%rdi), %eax
	incl	%eax
	movl	%eax, 24(%rdi)
	retq
.Lfunc_end39:
	.size	_ZN9NCompress6NBZip28CEncoder6AddRefEv, .Lfunc_end39-_ZN9NCompress6NBZip28CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZN9NCompress6NBZip28CEncoder7ReleaseEv,"axG",@progbits,_ZN9NCompress6NBZip28CEncoder7ReleaseEv,comdat
	.weak	_ZN9NCompress6NBZip28CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN9NCompress6NBZip28CEncoder7ReleaseEv,@function
_ZN9NCompress6NBZip28CEncoder7ReleaseEv: # @_ZN9NCompress6NBZip28CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi225:
	.cfi_def_cfa_offset 16
	movl	24(%rdi), %eax
	decl	%eax
	movl	%eax, 24(%rdi)
	jne	.LBB40_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB40_2:
	popq	%rcx
	retq
.Lfunc_end40:
	.size	_ZN9NCompress6NBZip28CEncoder7ReleaseEv, .Lfunc_end40-_ZN9NCompress6NBZip28CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end41:
	.size	_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end41-_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv,"axG",@progbits,_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv,comdat
	.weak	_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv,@function
_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv: # @_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end42:
	.size	_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv, .Lfunc_end42-_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv,"axG",@progbits,_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv,comdat
	.weak	_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv,@function
_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv: # @_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi226:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB43_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB43_2:                               # %_ZN9NCompress6NBZip28CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end43:
	.size	_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv, .Lfunc_end43-_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv,@function
_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv: # @_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	addq	$-16, %rdi
	jmp	_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv # TAILCALL
.Lfunc_end44:
	.size	_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv, .Lfunc_end44-_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv,"axG",@progbits,_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv,comdat
	.weak	_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv,@function
_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv: # @_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end45:
	.size	_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv, .Lfunc_end45-_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv
	.cfi_endproc

	.section	.text._ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv,"axG",@progbits,_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv,comdat
	.weak	_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv,@function
_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv: # @_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi227:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB46_2
# BB#1:
	addq	$-16, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB46_2:                               # %_ZN9NCompress6NBZip28CEncoder7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end46:
	.size	_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv, .Lfunc_end46-_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv
	.cfi_endproc

	.type	_ZTS18CInBufferException,@object # @_ZTS18CInBufferException
	.section	.rodata._ZTS18CInBufferException,"aG",@progbits,_ZTS18CInBufferException,comdat
	.weak	_ZTS18CInBufferException
	.p2align	4
_ZTS18CInBufferException:
	.asciz	"18CInBufferException"
	.size	_ZTS18CInBufferException, 21

	.type	_ZTS16CSystemException,@object # @_ZTS16CSystemException
	.section	.rodata._ZTS16CSystemException,"aG",@progbits,_ZTS16CSystemException,comdat
	.weak	_ZTS16CSystemException
	.p2align	4
_ZTS16CSystemException:
	.asciz	"16CSystemException"
	.size	_ZTS16CSystemException, 19

	.type	_ZTI16CSystemException,@object # @_ZTI16CSystemException
	.section	.rodata._ZTI16CSystemException,"aG",@progbits,_ZTI16CSystemException,comdat
	.weak	_ZTI16CSystemException
	.p2align	3
_ZTI16CSystemException:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16CSystemException
	.size	_ZTI16CSystemException, 16

	.type	_ZTI18CInBufferException,@object # @_ZTI18CInBufferException
	.section	.rodata._ZTI18CInBufferException,"aG",@progbits,_ZTI18CInBufferException,comdat
	.weak	_ZTI18CInBufferException
	.p2align	4
_ZTI18CInBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CInBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI18CInBufferException, 24

	.type	_ZTS19COutBufferException,@object # @_ZTS19COutBufferException
	.section	.rodata._ZTS19COutBufferException,"aG",@progbits,_ZTS19COutBufferException,comdat
	.weak	_ZTS19COutBufferException
	.p2align	4
_ZTS19COutBufferException:
	.asciz	"19COutBufferException"
	.size	_ZTS19COutBufferException, 22

	.type	_ZTI19COutBufferException,@object # @_ZTI19COutBufferException
	.section	.rodata._ZTI19COutBufferException,"aG",@progbits,_ZTI19COutBufferException,comdat
	.weak	_ZTI19COutBufferException
	.p2align	4
_ZTI19COutBufferException:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19COutBufferException
	.quad	_ZTI16CSystemException
	.size	_ZTI19COutBufferException, 24

	.type	_ZTVN9NCompress6NBZip28CEncoderE,@object # @_ZTVN9NCompress6NBZip28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN9NCompress6NBZip28CEncoderE
	.p2align	3
_ZTVN9NCompress6NBZip28CEncoderE:
	.quad	0
	.quad	_ZTIN9NCompress6NBZip28CEncoderE
	.quad	_ZN9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZN9NCompress6NBZip28CEncoder6AddRefEv
	.quad	_ZN9NCompress6NBZip28CEncoder7ReleaseEv
	.quad	_ZN9NCompress6NBZip28CEncoderD2Ev
	.quad	_ZN9NCompress6NBZip28CEncoderD0Ev
	.quad	_ZN9NCompress6NBZip28CEncoder4CodeEP19ISequentialInStreamP20ISequentialOutStreamPKyS7_P21ICompressProgressInfo
	.quad	_ZN9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	_ZN9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.quad	-8
	.quad	_ZTIN9NCompress6NBZip28CEncoderE
	.quad	_ZThn8_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N9NCompress6NBZip28CEncoder6AddRefEv
	.quad	_ZThn8_N9NCompress6NBZip28CEncoder7ReleaseEv
	.quad	_ZThn8_N9NCompress6NBZip28CEncoderD1Ev
	.quad	_ZThn8_N9NCompress6NBZip28CEncoderD0Ev
	.quad	_ZThn8_N9NCompress6NBZip28CEncoder18SetCoderPropertiesEPKjPK14tagPROPVARIANTj
	.quad	-16
	.quad	_ZTIN9NCompress6NBZip28CEncoderE
	.quad	_ZThn16_N9NCompress6NBZip28CEncoder14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn16_N9NCompress6NBZip28CEncoder6AddRefEv
	.quad	_ZThn16_N9NCompress6NBZip28CEncoder7ReleaseEv
	.quad	_ZThn16_N9NCompress6NBZip28CEncoderD1Ev
	.quad	_ZThn16_N9NCompress6NBZip28CEncoderD0Ev
	.quad	_ZThn16_N9NCompress6NBZip28CEncoder18SetNumberOfThreadsEj
	.size	_ZTVN9NCompress6NBZip28CEncoderE, 208

	.type	_ZTSN9NCompress6NBZip28CEncoderE,@object # @_ZTSN9NCompress6NBZip28CEncoderE
	.globl	_ZTSN9NCompress6NBZip28CEncoderE
	.p2align	4
_ZTSN9NCompress6NBZip28CEncoderE:
	.asciz	"N9NCompress6NBZip28CEncoderE"
	.size	_ZTSN9NCompress6NBZip28CEncoderE, 29

	.type	_ZTS14ICompressCoder,@object # @_ZTS14ICompressCoder
	.section	.rodata._ZTS14ICompressCoder,"aG",@progbits,_ZTS14ICompressCoder,comdat
	.weak	_ZTS14ICompressCoder
	.p2align	4
_ZTS14ICompressCoder:
	.asciz	"14ICompressCoder"
	.size	_ZTS14ICompressCoder, 17

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI14ICompressCoder,@object # @_ZTI14ICompressCoder
	.section	.rodata._ZTI14ICompressCoder,"aG",@progbits,_ZTI14ICompressCoder,comdat
	.weak	_ZTI14ICompressCoder
	.p2align	4
_ZTI14ICompressCoder:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS14ICompressCoder
	.quad	_ZTI8IUnknown
	.size	_ZTI14ICompressCoder, 24

	.type	_ZTS27ICompressSetCoderProperties,@object # @_ZTS27ICompressSetCoderProperties
	.section	.rodata._ZTS27ICompressSetCoderProperties,"aG",@progbits,_ZTS27ICompressSetCoderProperties,comdat
	.weak	_ZTS27ICompressSetCoderProperties
	.p2align	4
_ZTS27ICompressSetCoderProperties:
	.asciz	"27ICompressSetCoderProperties"
	.size	_ZTS27ICompressSetCoderProperties, 30

	.type	_ZTI27ICompressSetCoderProperties,@object # @_ZTI27ICompressSetCoderProperties
	.section	.rodata._ZTI27ICompressSetCoderProperties,"aG",@progbits,_ZTI27ICompressSetCoderProperties,comdat
	.weak	_ZTI27ICompressSetCoderProperties
	.p2align	4
_ZTI27ICompressSetCoderProperties:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS27ICompressSetCoderProperties
	.quad	_ZTI8IUnknown
	.size	_ZTI27ICompressSetCoderProperties, 24

	.type	_ZTS19ICompressSetCoderMt,@object # @_ZTS19ICompressSetCoderMt
	.section	.rodata._ZTS19ICompressSetCoderMt,"aG",@progbits,_ZTS19ICompressSetCoderMt,comdat
	.weak	_ZTS19ICompressSetCoderMt
	.p2align	4
_ZTS19ICompressSetCoderMt:
	.asciz	"19ICompressSetCoderMt"
	.size	_ZTS19ICompressSetCoderMt, 22

	.type	_ZTI19ICompressSetCoderMt,@object # @_ZTI19ICompressSetCoderMt
	.section	.rodata._ZTI19ICompressSetCoderMt,"aG",@progbits,_ZTI19ICompressSetCoderMt,comdat
	.weak	_ZTI19ICompressSetCoderMt
	.p2align	4
_ZTI19ICompressSetCoderMt:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ICompressSetCoderMt
	.quad	_ZTI8IUnknown
	.size	_ZTI19ICompressSetCoderMt, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN9NCompress6NBZip28CEncoderE,@object # @_ZTIN9NCompress6NBZip28CEncoderE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN9NCompress6NBZip28CEncoderE
	.p2align	4
_ZTIN9NCompress6NBZip28CEncoderE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN9NCompress6NBZip28CEncoderE
	.long	1                       # 0x1
	.long	4                       # 0x4
	.quad	_ZTI14ICompressCoder
	.quad	2                       # 0x2
	.quad	_ZTI27ICompressSetCoderProperties
	.quad	2050                    # 0x802
	.quad	_ZTI19ICompressSetCoderMt
	.quad	4098                    # 0x1002
	.quad	_ZTI13CMyUnknownImp
	.quad	6146                    # 0x1802
	.size	_ZTIN9NCompress6NBZip28CEncoderE, 88


	.globl	_ZN9NCompress6NBZip28CEncoderC1Ev
	.type	_ZN9NCompress6NBZip28CEncoderC1Ev,@function
_ZN9NCompress6NBZip28CEncoderC1Ev = _ZN9NCompress6NBZip28CEncoderC2Ev
	.globl	_ZN9NCompress6NBZip28CEncoderD1Ev
	.type	_ZN9NCompress6NBZip28CEncoderD1Ev,@function
_ZN9NCompress6NBZip28CEncoderD1Ev = _ZN9NCompress6NBZip28CEncoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
