	.text
	.file	"CreateCoder.bc"
	.globl	_Z13RegisterCodecPK10CCodecInfo
	.p2align	4, 0x90
	.type	_Z13RegisterCodecPK10CCodecInfo,@function
_Z13RegisterCodecPK10CCodecInfo:        # @_Z13RegisterCodecPK10CCodecInfo
	.cfi_startproc
# BB#0:
	movl	g_NumCodecs(%rip), %eax
	cmpq	$63, %rax
	ja	.LBB0_2
# BB#1:
	leal	1(%rax), %ecx
	movl	%ecx, g_NumCodecs(%rip)
	movq	%rdi, g_Codecs(,%rax,8)
.LBB0_2:
	retq
.Lfunc_end0:
	.size	_Z13RegisterCodecPK10CCodecInfo, .Lfunc_end0-_Z13RegisterCodecPK10CCodecInfo
	.cfi_endproc

	.globl	_Z10FindMethodRK11CStringBaseIwERyRjS4_
	.p2align	4, 0x90
	.type	_Z10FindMethodRK11CStringBaseIwERyRjS4_,@function
_Z10FindMethodRK11CStringBaseIwERyRjS4_: # @_Z10FindMethodRK11CStringBaseIwERyRjS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r13
	xorl	%ebx, %ebx
	cmpl	$0, g_NumCodecs(%rip)
	je	.LBB1_5
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movl	%ebx, %eax
	movq	g_Codecs(,%rax,8), %rbp
	movq	24(%rbp), %rsi
	movq	(%r13), %rdi
	callq	_Z21MyStringCompareNoCasePKwS0_
	testl	%eax, %eax
	je	.LBB1_4
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	incl	%ebx
	cmpl	g_NumCodecs(%rip), %ebx
	jb	.LBB1_1
# BB#3:
	xorl	%ebx, %ebx
	jmp	.LBB1_5
.LBB1_4:                                # %.critedge
	movq	16(%rbp), %rax
	movq	%rax, (%r12)
	movl	32(%rbp), %eax
	movl	%eax, (%r15)
	movl	$1, (%r14)
	movb	$1, %bl
.LBB1_5:                                # %.loopexit
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z10FindMethodRK11CStringBaseIwERyRjS4_, .Lfunc_end1-_Z10FindMethodRK11CStringBaseIwERyRjS4_
	.cfi_endproc

	.globl	_Z10FindMethodyR11CStringBaseIwE
	.p2align	4, 0x90
	.type	_Z10FindMethodyR11CStringBaseIwE,@function
_Z10FindMethodyR11CStringBaseIwE:       # @_Z10FindMethodyR11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	g_NumCodecs(%rip), %eax
	testl	%eax, %eax
	je	.LBB2_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	g_Codecs(,%rcx,8), %rdx
	cmpq	%rdi, 16(%rdx)
	je	.LBB2_6
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	incq	%rcx
	cmpl	%eax, %ecx
	jb	.LBB2_2
.LBB2_5:
	xorl	%eax, %eax
	jmp	.LBB2_16
.LBB2_6:                                # %.critedge
	movq	24(%rdx), %r12
	movl	$0, 8(%r14)
	movq	(%r14), %rbx
	movl	$0, (%rbx)
	xorl	%ebp, %ebp
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB2_7
# BB#8:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	12(%r14), %r13d
	cmpl	%ebp, %r13d
	je	.LBB2_13
# BB#9:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB2_12
# BB#10:
	testl	%r13d, %r13d
	jle	.LBB2_12
# BB#11:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
.LBB2_12:                               # %._crit_edge16.i.i
	movq	%r15, (%r14)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 12(%r14)
	movq	%r15, %rbx
.LBB2_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	decl	%ebp
	.p2align	4, 0x90
.LBB2_14:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %eax
	addq	$4, %r12
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB2_14
# BB#15:                                # %_ZN11CStringBaseIwEaSEPKw.exit
	movl	%ebp, 8(%r14)
	movb	$1, %al
.LBB2_16:                               # %.loopexit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	_Z10FindMethodyR11CStringBaseIwE, .Lfunc_end2-_Z10FindMethodyR11CStringBaseIwE
	.cfi_endproc

	.globl	_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb
	.p2align	4, 0x90
	.type	_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb,@function
_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb: # @_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r13
	movl	g_NumCodecs(%rip), %eax
	testq	%rax, %rax
	je	.LBB3_36
# BB#1:                                 # %.lr.ph
	xorl	%ecx, %ecx
	testb	%r8b, %r8b
	je	.LBB3_5
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	g_Codecs(,%rcx,8), %rbx
	cmpq	%rdi, 16(%rbx)
	jne	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	8(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_8
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB3_2
	jmp	.LBB3_36
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	g_Codecs(,%rcx,8), %rbx
	cmpq	%rdi, 16(%rbx)
	jne	.LBB3_7
# BB#6:                                 #   in Loop: Header=BB3_5 Depth=1
	movq	(%rbx), %rdx
	testq	%rdx, %rdx
	jne	.LBB3_11
.LBB3_7:                                #   in Loop: Header=BB3_5 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB3_5
.LBB3_36:                               # %.loopexit
	testb	%bpl, %bpl
	je	.LBB3_46
.LBB3_37:
	cmpq	$0, (%r13)
	je	.LBB3_46
# BB#38:
	movl	$200, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp0:
	movq	%rbp, %rdi
	callq	_ZN12CFilterCoderC1Ev
.Ltmp1:
# BB#39:
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_41
# BB#40:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_41:                               # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit52
	movq	%rbp, (%r14)
	movq	(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB3_43
# BB#42:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB3_43:
	movq	192(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_45
# BB#44:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_45:                               # %_ZN9CMyComPtrI15ICompressFilterEaSERKS1_.exit
	movq	%rbx, 192(%rbp)
.LBB3_46:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_8:                                # %.us-lcssa57.us
	callq	*%rdx
	movq	%rax, %r12
	cmpb	$0, 36(%rbx)
	je	.LBB3_17
# BB#9:                                 # %.critedge45
	testq	%r12, %r12
	jne	.LBB3_13
	jmp	.LBB3_14
.LBB3_11:                               # %.us-lcssa
	callq	*%rdx
	movq	%rax, %r12
	cmpb	$0, 36(%rbx)
	je	.LBB3_23
# BB#12:                                # %.critedge48
	testq	%r12, %r12
	je	.LBB3_14
.LBB3_13:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*8(%rax)
.LBB3_14:
	movq	(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB3_16
# BB#15:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_16:                               # %_ZN9CMyComPtrI15ICompressFilterEaSEPS0_.exit49
	movq	%r12, (%r13)
	testb	%bpl, %bpl
	jne	.LBB3_37
	jmp	.LBB3_46
.LBB3_17:
	cmpl	$1, 32(%rbx)
	jne	.LBB3_26
# BB#18:                                # %.critedge
	testq	%r12, %r12
	jne	.LBB3_19
	jmp	.LBB3_20
.LBB3_23:
	cmpl	$1, 32(%rbx)
	jne	.LBB3_31
# BB#24:                                # %.critedge46
	testq	%r12, %r12
	je	.LBB3_20
.LBB3_19:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*8(%rax)
.LBB3_20:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB3_22
# BB#21:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_22:                               # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit
	movq	%r12, (%r14)
	testb	%bpl, %bpl
	jne	.LBB3_37
	jmp	.LBB3_46
.LBB3_26:                               # %.critedge44
	testq	%r12, %r12
	je	.LBB3_28
# BB#27:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*8(%rax)
.LBB3_28:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_30
# BB#29:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_30:                               # %_ZN9CMyComPtrI15ICompressCoder2EaSEPS0_.exit
	movq	%r12, (%r15)
	testb	%bpl, %bpl
	jne	.LBB3_37
	jmp	.LBB3_46
.LBB3_31:                               # %.critedge47
	testq	%r12, %r12
	je	.LBB3_33
# BB#32:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*8(%rax)
.LBB3_33:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB3_35
# BB#34:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB3_35:                               # %_ZN9CMyComPtrI15ICompressCoder2EaSEPS0_.exit51
	movq	%r12, (%r15)
	testb	%bpl, %bpl
	je	.LBB3_46
	jmp	.LBB3_37
.LBB3_47:
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb, .Lfunc_end3-_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb
	.p2align	4, 0x90
	.type	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb,@function
_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb: # @_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdx, %rax
	movq	%rsi, %rdx
	movq	$0, 8(%rsp)
.Ltmp3:
	movzbl	%cl, %r8d
	leaq	8(%rsp), %rsi
	movl	$1, %r9d
	movq	%rax, %rcx
	callq	_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb
.Ltmp4:
# BB#1:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_3
# BB#2:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB4_3:                                # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB4_4:
.Ltmp5:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
# BB#5:
	movq	(%rdi), %rax
.Ltmp6:
	callq	*16(%rax)
.Ltmp7:
.LBB4_6:                                # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit5
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_7:
.Ltmp8:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb, .Lfunc_end4-_Z11CreateCoderyR9CMyComPtrI14ICompressCoderERS_I15ICompressCoder2Eb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	1                       #   On action: 1
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end5:
	.size	__clang_call_terminate, .Lfunc_end5-__clang_call_terminate

	.text
	.globl	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
	.p2align	4, 0x90
	.type	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb,@function
_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb: # @_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 32
.Lcfi44:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	$0, (%rsp)
	movq	$0, 8(%rsp)
.Ltmp9:
	movzbl	%dl, %r8d
	leaq	8(%rsp), %rsi
	movq	%rsp, %rcx
	movl	$1, %r9d
	movq	%rax, %rdx
	callq	_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb
.Ltmp10:
# BB#1:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp15:
	callq	*16(%rax)
.Ltmp16:
.LBB6_3:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_5
# BB#4:
	movq	(%rdi), %rax
.Ltmp21:
	callq	*16(%rax)
.Ltmp22:
.LBB6_5:                                # %_ZN9CMyComPtrI15ICompressFilterED2Ev.exit9
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB6_12:
.Ltmp23:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_9:
.Ltmp17:
	movq	%rax, %rbx
	jmp	.LBB6_10
.LBB6_6:
.Ltmp11:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_10
# BB#7:
	movq	(%rdi), %rax
.Ltmp12:
	callq	*16(%rax)
.Ltmp13:
.LBB6_10:                               # %.body
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_13
# BB#11:
	movq	(%rdi), %rax
.Ltmp18:
	callq	*16(%rax)
.Ltmp19:
.LBB6_13:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_8:
.Ltmp14:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_14:
.Ltmp20:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb, .Lfunc_end6-_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp9-.Lfunc_begin2    # >> Call Site 1 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin2   #     jumps to .Ltmp11
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin2   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp12-.Ltmp22         #   Call between .Ltmp22 and .Ltmp12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin2   #     jumps to .Ltmp20
	.byte	1                       #   On action: 1
	.long	.Ltmp19-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Lfunc_end6-.Ltmp19     #   Call between .Ltmp19 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_Z12CreateFilteryR9CMyComPtrI15ICompressFilterEb
	.p2align	4, 0x90
	.type	_Z12CreateFilteryR9CMyComPtrI15ICompressFilterEb,@function
_Z12CreateFilteryR9CMyComPtrI15ICompressFilterEb: # @_Z12CreateFilteryR9CMyComPtrI15ICompressFilterEb
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -16
	movq	$0, 8(%rsp)
	movq	$0, (%rsp)
.Ltmp24:
	movzbl	%dl, %r8d
	leaq	8(%rsp), %rdx
	movq	%rsp, %rcx
	xorl	%r9d, %r9d
	callq	_Z11CreateCoderyR9CMyComPtrI15ICompressFilterERS_I14ICompressCoderERS_I15ICompressCoder2Ebb
.Ltmp25:
# BB#1:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_3
# BB#2:
	movq	(%rdi), %rax
.Ltmp29:
	callq	*16(%rax)
.Ltmp30:
.LBB7_3:                                # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_5
# BB#4:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB7_5:                                # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit8
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB7_8:
.Ltmp31:
	movq	%rax, %rbx
	jmp	.LBB7_9
.LBB7_6:
.Ltmp26:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_9
# BB#7:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
.LBB7_9:                                # %_ZN9CMyComPtrI15ICompressCoder2ED2Ev.exit7
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp32:
	callq	*16(%rax)
.Ltmp33:
.LBB7_11:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_12:
.Ltmp34:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_Z12CreateFilteryR9CMyComPtrI15ICompressFilterEb, .Lfunc_end7-_Z12CreateFilteryR9CMyComPtrI15ICompressFilterEb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin3   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp27-.Ltmp30         #   Call between .Ltmp30 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp33-.Ltmp27         #   Call between .Ltmp27 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin3   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end7-.Ltmp33     #   Call between .Ltmp33 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.type	g_NumCodecs,@object     # @g_NumCodecs
	.bss
	.globl	g_NumCodecs
	.p2align	2
g_NumCodecs:
	.long	0                       # 0x0
	.size	g_NumCodecs, 4

	.type	g_Codecs,@object        # @g_Codecs
	.globl	g_Codecs
	.p2align	4
g_Codecs:
	.zero	512
	.size	g_Codecs, 512


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
