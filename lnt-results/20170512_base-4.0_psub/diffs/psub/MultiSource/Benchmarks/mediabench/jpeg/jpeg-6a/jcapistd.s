	.text
	.file	"jcapistd.bc"
	.globl	jpeg_start_compress
	.p2align	4, 0x90
	.type	jpeg_start_compress,@function
jpeg_start_compress:                    # @jpeg_start_compress
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$100, %eax
	je	.LBB0_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB0_2:
	testl	%ebp, %ebp
	je	.LBB0_4
# BB#3:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	jpeg_suppress_tables
.LBB0_4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	32(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	movq	%rbx, %rdi
	callq	jinit_compress_master
	movq	424(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	$0, 296(%rbx)
	cmpl	$1, 248(%rbx)
	movl	$101, %eax
	sbbl	$-1, %eax
	movl	%eax, 28(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_start_compress, .Lfunc_end0-jpeg_start_compress
	.cfi_endproc

	.globl	jpeg_write_scanlines
	.p2align	4, 0x90
	.type	jpeg_write_scanlines,@function
jpeg_write_scanlines:                   # @jpeg_write_scanlines
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 48
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$101, %eax
	je	.LBB1_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB1_2:
	movl	296(%rbx), %eax
	cmpl	44(%rbx), %eax
	jb	.LBB1_4
# BB#3:
	movq	(%rbx), %rax
	movl	$119, 40(%rax)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB1_4:
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB1_6
# BB#5:
	movl	296(%rbx), %ecx
	movq	%rcx, 8(%rax)
	movl	44(%rbx), %ecx
	movq	%rcx, 16(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB1_6:
	movq	424(%rbx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB1_8
# BB#7:
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB1_8:
	movl	44(%rbx), %ecx
	subl	296(%rbx), %ecx
	cmpl	%ebp, %ecx
	cmovael	%ebp, %ecx
	movl	$0, 12(%rsp)
	movq	432(%rbx), %rax
	leaq	12(%rsp), %rdx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*8(%rax)
	movl	12(%rsp), %eax
	addl	%eax, 296(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	jpeg_write_scanlines, .Lfunc_end1-jpeg_write_scanlines
	.cfi_endproc

	.globl	jpeg_write_raw_data
	.p2align	4, 0x90
	.type	jpeg_write_raw_data,@function
jpeg_write_raw_data:                    # @jpeg_write_raw_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	cmpl	$102, %eax
	je	.LBB2_2
# BB#1:
	movq	(%rbx), %rcx
	movl	$18, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB2_2:
	movl	44(%rbx), %eax
	movl	296(%rbx), %ecx
	cmpl	%eax, %ecx
	jae	.LBB2_11
# BB#3:
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB2_5
# BB#4:
	movq	%rcx, 8(%rdx)
	movq	%rax, 16(%rdx)
	movq	%rbx, %rdi
	callq	*(%rdx)
.LBB2_5:
	movq	424(%rbx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB2_7
# BB#6:
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB2_7:
	movl	308(%rbx), %ebp
	shll	$3, %ebp
	cmpl	%r15d, %ebp
	jbe	.LBB2_9
# BB#8:
	movq	(%rbx), %rax
	movl	$21, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB2_9:
	movq	448(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*8(%rax)
	testl	%eax, %eax
	je	.LBB2_12
# BB#10:
	addl	%ebp, 296(%rbx)
	jmp	.LBB2_13
.LBB2_11:
	movq	(%rbx), %rax
	movl	$119, 40(%rax)
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	*8(%rax)
.LBB2_12:
	xorl	%ebp, %ebp
.LBB2_13:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	jpeg_write_raw_data, .Lfunc_end2-jpeg_write_raw_data
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
