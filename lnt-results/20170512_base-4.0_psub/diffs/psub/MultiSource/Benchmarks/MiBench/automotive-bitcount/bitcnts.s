	.text
	.file	"bitcnts.bc"
	.globl	rand
	.p2align	4, 0x90
	.type	rand,@function
rand:                                   # @rand
	.cfi_startproc
# BB#0:
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	shrq	$16, %rax
	movl	%eax, %ecx
	imulq	$131077, %rcx, %rcx     # imm = 0x20005
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$14, %edx
	movl	%edx, %ecx
	shll	$15, %ecx
	subl	%edx, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	retq
.Lfunc_end0:
	.size	rand, .Lfunc_end0-rand
	.cfi_endproc

	.globl	srand
	.p2align	4, 0x90
	.type	srand,@function
srand:                                  # @srand
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	movq	%rax, next(%rip)
	retq
.Lfunc_end1:
	.size	srand, .Lfunc_end1-srand
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	cmpl	$1, %edi
	jle	.LBB2_7
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
	movq	$1, next(%rip)
	movl	$.L.str.8, %edi
	callq	puts
	movq	%rbx, %rax
	shlq	$32, %rax
	testq	%rax, %rax
	jle	.LBB2_8
# BB#2:                                 # %.split.us.preheader
	movslq	%ebx, %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB2_3:                                # %.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	shrq	$16, %rax
	movl	%eax, %ecx
	imulq	$131077, %rcx, %rcx     # imm = 0x20005
	shrq	$32, %rcx
	movl	%eax, %edx
	subl	%ecx, %edx
	shrl	%edx
	addl	%ecx, %edx
	shrl	$14, %edx
	movl	%edx, %ecx
	shll	$15, %ecx
	subl	%edx, %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %ebx
	movq	main.pBitCntFunc(,%r12,8), %r13
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_4:                                #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	*%r13
	cltq
	addq	%rax, %r14
	incq	%rbp
	addq	$13, %rbx
	cmpq	%r15, %rbp
	jl	.LBB2_4
# BB#5:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	main.text(,%r12,8), %rsi
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	printf
	incq	%r12
	cmpq	$7, %r12
	jne	.LBB2_3
	jmp	.LBB2_6
.LBB2_8:                                # %.split.preheader
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	movl	$.L.str.9, %edi
	movl	$.L.str, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	movl	$.L.str.9, %edi
	movl	$.L.str.1, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	movl	$.L.str.9, %edi
	movl	$.L.str.2, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	movl	$.L.str.9, %edi
	movl	$.L.str.3, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	movl	$.L.str.9, %edi
	movl	$.L.str.4, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	movl	$.L.str.9, %edi
	movl	$.L.str.5, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
	imulq	$1103515245, next(%rip), %rax # imm = 0x41C64E6D
	addq	$12345, %rax            # imm = 0x3039
	movq	%rax, next(%rip)
	movl	$.L.str.9, %edi
	movl	$.L.str.6, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
.LBB2_6:                                # %.us-lcssa.us
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_7:
	movq	stderr(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$28, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$-1, %edi
	callq	exit
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.p2align	4, 0x90
	.type	bit_shifter,@function
bit_shifter:                            # @bit_shifter
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#1:                                 # %.lr.ph.preheader
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edx
	movl	%edi, %eax
	andl	$1, %eax
	addl	%edx, %eax
	sarq	%rdi
	je	.LBB3_4
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	$64, %ecx
	leal	1(%rcx), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jb	.LBB3_2
.LBB3_4:                                # %.critedge
	retq
.Lfunc_end3:
	.size	bit_shifter, .Lfunc_end3-bit_shifter
	.cfi_endproc

	.type	next,@object            # @next
	.data
	.p2align	3
next:
	.quad	1                       # 0x1
	.size	next, 8

	.type	main.pBitCntFunc,@object # @main.pBitCntFunc
	.section	.rodata,"a",@progbits
	.p2align	4
main.pBitCntFunc:
	.quad	bit_count
	.quad	bitcount
	.quad	ntbl_bitcnt
	.quad	ntbl_bitcount
	.quad	BW_btbl_bitcount
	.quad	AR_btbl_bitcount
	.quad	bit_shifter
	.size	main.pBitCntFunc, 56

	.type	main.text,@object       # @main.text
	.p2align	4
main.text:
	.quad	.L.str
	.quad	.L.str.1
	.quad	.L.str.2
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	.L.str.5
	.quad	.L.str.6
	.size	main.text, 56

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Optimized 1 bit/loop counter"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Ratko's mystery algorithm"
	.size	.L.str.1, 26

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Recursive bit count by nybbles"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Non-recursive bit count by nybbles"
	.size	.L.str.3, 35

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Non-recursive bit count by bytes (BW)"
	.size	.L.str.4, 38

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Non-recursive bit count by bytes (AR)"
	.size	.L.str.5, 38

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Shift and count bits"
	.size	.L.str.6, 21

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Usage: bitcnts <iterations>\n"
	.size	.L.str.7, 29

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Bit counter algorithm benchmark\n"
	.size	.L.str.8, 33

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%-38s> Bits: %ld\n"
	.size	.L.str.9, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
