	.text
	.file	"pidiv.bc"
	.globl	pidiv
	.p2align	4, 0x90
	.type	pidiv,@function
pidiv:                                  # @pidiv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movq	%rdi, %r13
	testq	%r13, %r13
	je	.LBB0_2
# BB#1:
	incw	(%r13)
.LBB0_2:
	movl	%r12d, %ebx
	negl	%ebx
	cmovll	%r12d, %ebx
	andl	$65535, %ebx            # imm = 0xFFFF
	je	.LBB0_3
# BB#4:
	movzwl	4(%r13), %r15d
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movl	%r15d, %edi
	callq	palloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB0_5
# BB#6:
	leaq	8(%r13), %rcx
	leaq	(%r15,%r15), %rsi
	.p2align	4, 0x90
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	shll	$16, %ebp
	movzwl	6(%r13,%rsi), %eax
	orl	%ebp, %eax
	xorl	%edx, %edx
	divl	%ebx
	movw	%ax, 6(%r14,%rsi)
	leaq	6(%r13,%rsi), %rax
	addq	$-2, %rsi
	cmpq	%rcx, %rax
	movl	%edx, %ebp
	ja	.LBB0_7
# BB#8:
	cmpl	$2, %r15d
	jb	.LBB0_11
# BB#9:
	cmpw	$0, 6(%r14,%r15,2)
	jne	.LBB0_11
# BB#10:
	decw	4(%r14)
.LBB0_11:
	movzbl	6(%r13), %eax
	shrl	$31, %r12d
	cmpl	%r12d, %eax
	setne	6(%r14)
	movzwl	4(%r14), %eax
	cmpl	$1, %eax
	jne	.LBB0_14
# BB#12:
	cmpw	$0, 8(%r14)
	jne	.LBB0_14
# BB#13:
	movb	$0, 6(%r14)
	testq	%r13, %r13
	jne	.LBB0_15
	jmp	.LBB0_17
.LBB0_3:
	movl	$4, %edi
	movl	$.L.str, %esi
	movl	$.L.str.2, %edx
	callq	errorp
	movq	%rax, %rdi
	callq	pnew
	movq	%rax, %r14
	testq	%r13, %r13
	jne	.LBB0_15
	jmp	.LBB0_17
.LBB0_5:
	xorl	%r14d, %r14d
.LBB0_14:
	testq	%r13, %r13
	je	.LBB0_17
.LBB0_15:
	decw	(%r13)
	jne	.LBB0_17
# BB#16:
	xorl	%eax, %eax
	movq	%r13, %rdi
	callq	pfree
.LBB0_17:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	presult                 # TAILCALL
.Lfunc_end0:
	.size	pidiv, .Lfunc_end0-pidiv
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"pidiv"
	.size	.L.str, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"divide by zero"
	.size	.L.str.2, 15


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
