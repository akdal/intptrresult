	.text
	.file	"shared_output.bc"
	.globl	mdprintf
	.p2align	4, 0x90
	.type	mdprintf,@function
mdprintf:                               # @mdprintf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$728, %rsp              # imm = 0x2D8
.Lcfi2:
	.cfi_def_cfa_offset 752
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	testb	%al, %al
	je	.LBB0_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB0_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	752(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$16, (%rsp)
	leaq	208(%rsp), %rdi
	movq	%rsp, %rcx
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdx
	callq	vsnprintf
	cmpl	$-1, %eax
	je	.LBB0_3
# BB#4:
	cmpl	$512, %eax              # imm = 0x200
	movl	$511, %ecx              # imm = 0x1FF
	cmovll	%eax, %ecx
	movslq	%ecx, %rdx
	leaq	208(%rsp), %rsi
	xorl	%ecx, %ecx
	movl	%ebp, %edi
	callq	send
	jmp	.LBB0_5
.LBB0_3:
	movl	$-1, %eax
.LBB0_5:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$728, %rsp              # imm = 0x2D8
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	mdprintf, .Lfunc_end0-mdprintf
	.cfi_endproc

	.globl	logg_close
	.p2align	4, 0x90
	.type	logg_close,@function
logg_close:                             # @logg_close
	.cfi_startproc
# BB#0:
	movq	logg_fd(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	callq	fclose
	movq	$0, logg_fd(%rip)
	addq	$8, %rsp
.LBB1_2:
	retq
.Lfunc_end1:
	.size	logg_close, .Lfunc_end1-logg_close
	.cfi_endproc

	.globl	logg
	.p2align	4, 0x90
	.type	logg,@function
logg:                                   # @logg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
	subq	$1504, %rsp             # imm = 0x5E0
.Lcfi9:
	.cfi_def_cfa_offset 1536
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testb	%al, %al
	je	.LBB2_2
# BB#1:
	movaps	%xmm0, 192(%rsp)
	movaps	%xmm1, 208(%rsp)
	movaps	%xmm2, 224(%rsp)
	movaps	%xmm3, 240(%rsp)
	movaps	%xmm4, 256(%rsp)
	movaps	%xmm5, 272(%rsp)
	movaps	%xmm6, 288(%rsp)
	movaps	%xmm7, 304(%rsp)
.LBB2_2:
	movq	%r9, 184(%rsp)
	movq	%r8, 176(%rsp)
	movq	%rcx, 168(%rsp)
	movq	%rdx, 160(%rsp)
	movq	%rsi, 152(%rsp)
	leaq	144(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	1536(%rsp), %rcx
	movq	%rcx, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	movq	%rax, 80(%rsp)
	movq	%rcx, 72(%rsp)
	movl	$48, 68(%rsp)
	movl	$8, 64(%rsp)
	movq	%rax, 48(%rsp)
	movq	%rcx, 40(%rsp)
	movl	$48, 36(%rsp)
	movl	$8, 32(%rsp)
	cmpq	$0, logg_file(%rip)
	je	.LBB2_31
# BB#3:
	cmpq	$0, logg_fd(%rip)
	je	.LBB2_4
.LBB2_9:
	cmpl	$0, logg_size(%rip)
	je	.LBB2_13
# BB#10:
	movq	logg_file(%rip), %rsi
	leaq	320(%rsp), %rdx
	movl	$1, %edi
	callq	__xstat
	cmpl	$-1, %eax
	je	.LBB2_13
# BB#11:
	movl	368(%rsp), %edx
	movl	logg_size(%rip), %ecx
	cmpl	%ecx, %edx
	jbe	.LBB2_13
# BB#12:                                # %.thread
	movq	$0, logg_file(%rip)
	movq	logg_fd(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	logg_fd(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	logg_fd(%rip), %rdi
	callq	fclose
	movq	$0, logg_fd(%rip)
	cmpw	$0, logg_foreground(%rip)
	jne	.LBB2_32
	jmp	.LBB2_34
.LBB2_13:
	cmpq	$0, logg_fd(%rip)
	je	.LBB2_31
# BB#14:
	cmpw	$0, logg_time(%rip)
	je	.LBB2_18
# BB#15:
	cmpb	$42, (%r14)
	jne	.LBB2_17
# BB#16:
	movzwl	logg_verbose(%rip), %eax
	testw	%ax, %ax
	je	.LBB2_18
.LBB2_17:
	leaq	136(%rsp), %rbx
	movq	%rbx, %rdi
	callq	time
	movq	%rbx, %rdi
	callq	ctime
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	$1, %esi
	movq	%rax, %rdi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	strlen
	leaq	-1(%rax), %rdx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	strncpy
	movq	logg_fd(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdx
	callq	fprintf
	movq	%rbp, %rdi
	callq	free
.LBB2_18:
	xorl	%edi, %edi
	movl	$5, %edx
	movq	%r14, %rsi
	callq	dcgettext
	movb	(%r14), %al
	cmpb	$94, %al
	je	.LBB2_23
# BB#19:
	cmpb	$42, %al
	je	.LBB2_24
# BB#20:
	cmpb	$33, %al
	jne	.LBB2_26
# BB#21:
	movq	logg_fd(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$7, %esi
	jmp	.LBB2_22
.LBB2_4:
	movl	$31, %edi
	callq	umask
	movl	%eax, %ebp
	movq	logg_file(%rip), %rdi
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %rbx
	movq	%rbx, logg_fd(%rip)
	movl	%ebp, %edi
	callq	umask
	testq	%rbx, %rbx
	je	.LBB2_5
# BB#6:
	cmpw	$0, logg_lock(%rip)
	je	.LBB2_9
# BB#7:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 112(%rsp)
	movaps	%xmm0, 96(%rsp)
	movw	$1, 96(%rsp)
	movq	logg_fd(%rip), %rdi
	callq	fileno
	movl	%eax, %ecx
	leaq	96(%rsp), %rdx
	movl	$6, %esi
	xorl	%eax, %eax
	movl	%ecx, %edi
	callq	fcntl
	cmpl	$-1, %eax
	jne	.LBB2_9
# BB#8:
	movl	$-1, %eax
	jmp	.LBB2_35
.LBB2_5:
	movq	logg_file(%rip), %rsi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-1, %eax
	jmp	.LBB2_35
.LBB2_23:
	movq	logg_fd(%rip), %rcx
	movl	$.L.str.6, %edi
	movl	$9, %esi
.LBB2_22:
	movl	$1, %edx
	callq	fwrite
	movq	logg_fd(%rip), %rdi
	jmp	.LBB2_27
.LBB2_24:
	cmpw	$0, logg_verbose(%rip)
	je	.LBB2_30
# BB#25:
	movq	logg_fd(%rip), %rdi
	jmp	.LBB2_27
.LBB2_26:
	movq	logg_fd(%rip), %rdi
	cmpb	$35, %al
	jne	.LBB2_28
.LBB2_27:
	leaq	1(%r14), %rsi
	movq	%rsp, %rdx
.LBB2_29:
	callq	vfprintf
.LBB2_30:
	movq	logg_fd(%rip), %rdi
	callq	fflush
.LBB2_31:
	cmpw	$0, logg_foreground(%rip)
	je	.LBB2_34
.LBB2_32:
	xorl	%edi, %edi
	movl	$5, %edx
	movq	%r14, %rsi
	callq	dcgettext
	leaq	464(%rsp), %rdi
	leaq	32(%rsp), %rcx
	movl	$1024, %esi             # imm = 0x400
	movq	%r14, %rdx
	callq	vsnprintf
	movb	$0, 1488(%rsp)
	cmpb	$35, 464(%rsp)
	je	.LBB2_34
# BB#33:
	leaq	464(%rsp), %rsi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	mprintf
.LBB2_34:
	xorl	%eax, %eax
.LBB2_35:
	addq	$1504, %rsp             # imm = 0x5E0
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB2_28:
	movq	%rsp, %rdx
	movq	%r14, %rsi
	jmp	.LBB2_29
.Lfunc_end2:
	.size	logg, .Lfunc_end2-logg
	.cfi_endproc

	.globl	mprintf
	.p2align	4, 0x90
	.type	mprintf,@function
mprintf:                                # @mprintf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	subq	$728, %rsp              # imm = 0x2D8
.Lcfi15:
	.cfi_def_cfa_offset 752
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	testb	%al, %al
	je	.LBB3_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB3_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	cmpw	$0, mprintf_disabled(%rip)
	jne	.LBB3_17
# BB#3:
	movq	stdout(%rip), %r14
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	752(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	leaq	208(%rsp), %rdi
	movq	%rsp, %rcx
	movl	$512, %esi              # imm = 0x200
	movq	%rbx, %rdx
	callq	vsnprintf
	movb	208(%rsp), %al
	cmpb	$64, %al
	je	.LBB3_5
# BB#4:
	cmpb	$33, %al
	jne	.LBB3_7
.LBB3_5:
	cmpw	$0, mprintf_stdout(%rip)
	cmoveq	stderr(%rip), %r14
	leaq	209(%rsp), %rdx
	movl	$.L.str.8, %esi
.LBB3_6:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	cmpq	stdout(%rip), %r14
	jne	.LBB3_17
	jmp	.LBB3_16
.LBB3_7:
	cmpw	$0, mprintf_quiet(%rip)
	jne	.LBB3_15
# BB#8:
	cmpb	$42, %al
	je	.LBB3_11
# BB#9:
	cmpb	$94, %al
	jne	.LBB3_13
# BB#10:
	cmpw	$0, mprintf_stdout(%rip)
	cmoveq	stderr(%rip), %r14
	leaq	209(%rsp), %rdx
	movl	$.L.str.9, %esi
	jmp	.LBB3_6
.LBB3_11:
	cmpw	$0, mprintf_verbose(%rip)
	je	.LBB3_15
# BB#12:
	leaq	209(%rsp), %rdi
	jmp	.LBB3_14
.LBB3_13:
	leaq	208(%rsp), %rdi
.LBB3_14:
	movq	%r14, %rsi
	callq	fputs
.LBB3_15:
	cmpq	stdout(%rip), %r14
	jne	.LBB3_17
.LBB3_16:
	movq	%r14, %rdi
	callq	fflush
.LBB3_17:
	addq	$728, %rsp              # imm = 0x2D8
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	mprintf, .Lfunc_end3-mprintf
	.cfi_endproc

	.type	logg_fd,@object         # @logg_fd
	.bss
	.globl	logg_fd
	.p2align	3
logg_fd:
	.quad	0
	.size	logg_fd, 8

	.type	logg_verbose,@object    # @logg_verbose
	.globl	logg_verbose
	.p2align	1
logg_verbose:
	.short	0                       # 0x0
	.size	logg_verbose, 2

	.type	logg_lock,@object       # @logg_lock
	.data
	.globl	logg_lock
	.p2align	1
logg_lock:
	.short	1                       # 0x1
	.size	logg_lock, 2

	.type	logg_time,@object       # @logg_time
	.bss
	.globl	logg_time
	.p2align	1
logg_time:
	.short	0                       # 0x0
	.size	logg_time, 2

	.type	logg_foreground,@object # @logg_foreground
	.data
	.globl	logg_foreground
	.p2align	1
logg_foreground:
	.short	1                       # 0x1
	.size	logg_foreground, 2

	.type	logg_size,@object       # @logg_size
	.bss
	.globl	logg_size
	.p2align	2
logg_size:
	.long	0                       # 0x0
	.size	logg_size, 4

	.type	logg_file,@object       # @logg_file
	.globl	logg_file
	.p2align	3
logg_file:
	.quad	0
	.size	logg_file, 8

	.type	mprintf_disabled,@object # @mprintf_disabled
	.globl	mprintf_disabled
	.p2align	1
mprintf_disabled:
	.short	0                       # 0x0
	.size	mprintf_disabled, 2

	.type	mprintf_verbose,@object # @mprintf_verbose
	.globl	mprintf_verbose
	.p2align	1
mprintf_verbose:
	.short	0                       # 0x0
	.size	mprintf_verbose, 2

	.type	mprintf_quiet,@object   # @mprintf_quiet
	.globl	mprintf_quiet
	.p2align	1
mprintf_quiet:
	.short	0                       # 0x0
	.size	mprintf_quiet, 2

	.type	mprintf_stdout,@object  # @mprintf_stdout
	.globl	mprintf_stdout
	.p2align	1
mprintf_stdout:
	.short	0                       # 0x0
	.size	mprintf_stdout, 2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"at"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ERROR: Can't open %s in append mode (check permissions!).\n"
	.size	.L.str.1, 59

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Log size = %u, max = %u\n"
	.size	.L.str.2, 25

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"LOGGING DISABLED (Maximal log file size exceeded).\n"
	.size	.L.str.3, 52

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s -> "
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ERROR: "
	.size	.L.str.5, 8

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"WARNING: "
	.size	.L.str.6, 10

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%s"
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ERROR: %s"
	.size	.L.str.8, 10

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"WARNING: %s"
	.size	.L.str.9, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
