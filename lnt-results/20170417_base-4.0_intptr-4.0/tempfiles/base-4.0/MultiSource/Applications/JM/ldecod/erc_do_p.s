	.text
	.file	"erc_do_p.bc"
	.globl	ercConcealInterFrame
	.p2align	4, 0x90
	.type	ercConcealInterFrame,@function
ercConcealInterFrame:                   # @ercConcealInterFrame
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 224
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r12
	movl	%ecx, %r14d
	movl	%edx, %r13d
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.LBB0_47
# BB#1:
	cmpl	$0, 64(%r12)
	je	.LBB0_47
# BB#2:
	movl	$1, %eax
	cmpl	$0, 60(%r12)
	je	.LBB0_47
# BB#3:
	testl	%r9d, %r9d
	je	.LBB0_4
# BB#5:
	movq	img(%rip), %rax
	movl	5932(%rax), %ecx
	imull	5936(%rax), %ecx
	addl	%ecx, %ecx
	movslq	%ecx, %rax
	leaq	512(%rax,%rax), %rdi
	jmp	.LBB0_6
.LBB0_4:
	movl	$512, %edi              # imm = 0x200
.LBB0_6:
	callq	malloc
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	jne	.LBB0_8
# BB#7:
	movl	$.L.str, %edi
	callq	no_mem_exit
.LBB0_8:
	movl	%r13d, %eax
	sarl	$4, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB0_46
# BB#9:                                 # %.lr.ph175
	sarl	$4, %r14d
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %ecx
	movl	%ecx, 92(%rsp)          # 4-byte Spill
	movl	%r13d, %edi
	sarl	$3, %edi
	addl	%edi, %edi
	leal	(%r14,%r14), %ecx
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	leal	(%rax,%rax), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movslq	%r14d, %rbx
	leal	-1(%rbx), %eax
	movl	%eax, 100(%rsp)         # 4-byte Spill
	xorl	%edx, %edx
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movl	%r13d, 8(%rsp)          # 4-byte Spill
	movq	%r12, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_10:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
                                        #       Child Loop BB0_15 Depth 3
                                        #       Child Loop BB0_21 Depth 3
                                        #       Child Loop BB0_37 Depth 3
                                        #       Child Loop BB0_30 Depth 3
	movl	%edx, %eax
	shrl	$31, %eax
	addl	%edx, %eax
	sarl	%eax
	movl	92(%rsp), %ecx          # 4-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	subl	%eax, %ecx
	movl	%edx, 96(%rsp)          # 4-byte Spill
	testb	$1, %dl
	cmovel	%eax, %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB0_45
# BB#11:                                # %.lr.ph171
                                        #   in Loop: Header=BB0_10 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%rax), %edx
	xorl	%esi, %esi
	movl	%edx, 12(%rsp)          # 4-byte Spill
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_48:                               # %split
                                        #   in Loop: Header=BB0_12 Depth=2
	incl	%r13d
	cmpl	%r14d, %r13d
	jge	.LBB0_19
.LBB0_27:                               #   in Loop: Header=BB0_12 Depth=2
	movq	%r8, %rax
	testl	%eax, %eax
	je	.LBB0_28
# BB#35:                                #   in Loop: Header=BB0_12 Depth=2
	leal	1(%r13), %esi
	movl	%r13d, %ebp
	subl	%eax, %ebp
	jl	.LBB0_44
# BB#36:                                # %.lr.ph166.preheader
                                        #   in Loop: Header=BB0_12 Depth=2
	movl	%ebp, 108(%rsp)         # 4-byte Spill
	movl	%esi, 104(%rsp)         # 4-byte Spill
	xorl	%esi, %esi
	leaq	128(%rsp), %r15
	jmp	.LBB0_37
	.p2align	4, 0x90
.LBB0_42:                               # %..lr.ph166_crit_edge
                                        #   in Loop: Header=BB0_37 Depth=3
	subl	%r14d, %r13d
	xorl	$1, %r14d
	movq	120(%rsp), %rax         # 8-byte Reload
	addl	%r14d, %eax
	incl	%esi
	movq	8(%r12), %rcx
	leaq	128(%rsp), %r15
	movl	12(%rsp), %edx          # 4-byte Reload
.LBB0_37:                               # %.lr.ph166
                                        #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	%esi, %ebp
	movl	%esi, %r14d
	andl	$1, %r14d
	movl	%r13d, %ebx
	movq	%rax, 120(%rsp)         # 8-byte Spill
	cmovel	%eax, %ebx
	leal	(%rbx,%rbx), %esi
	movq	%r15, %rdi
	movl	28(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	pushq	$0
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	imull	16(%rsp), %ebx          # 4-byte Folded Reload
	addl	48(%rsp), %ebx          # 4-byte Folded Reload
	cmpl	$8, erc_mvperMB(%rip)
	jl	.LBB0_39
# BB#38:                                #   in Loop: Header=BB0_37 Depth=3
	subq	$8, %rsp
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%ebx, %edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r15, %r8
	movl	16(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %r9d
	movq	120(%rsp), %r15         # 8-byte Reload
	pushq	8(%r15)
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	concealByTrial
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_40
	.p2align	4, 0x90
.LBB0_39:                               #   in Loop: Header=BB0_37 Depth=3
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebx, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	8(%rsp), %r12d          # 4-byte Reload
	movl	%r12d, %ecx
	callq	concealByCopy
	movq	112(%rsp), %r15         # 8-byte Reload
.LBB0_40:                               #   in Loop: Header=BB0_37 Depth=3
	movl	$-1, %esi
	movl	%ebx, %edi
	movl	%r12d, %edx
	movq	%r15, %rcx
	callq	ercMarkCurrMBConcealed
	movl	%ebp, %esi
	cmpl	%esi, 108(%rsp)         # 4-byte Folded Reload
	movq	%r15, %r12
	jne	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_12 Depth=2
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	movl	104(%rsp), %esi         # 4-byte Reload
	jmp	.LBB0_44
	.p2align	4, 0x90
.LBB0_19:                               # %.preheader159
                                        #   in Loop: Header=BB0_12 Depth=2
	cmpl	%r14d, %r8d
	movl	%r14d, %esi
	movl	8(%rsp), %r13d          # 4-byte Reload
	jge	.LBB0_44
# BB#20:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_12 Depth=2
	leal	(%r8,%r8), %r15d
	movl	100(%rsp), %ebx         # 4-byte Reload
	subl	%r8d, %ebx
	imull	16(%rsp), %r8d          # 4-byte Folded Reload
	addl	48(%rsp), %r8d          # 4-byte Folded Reload
	movq	%r8, %rbp
	jmp	.LBB0_21
	.p2align	4, 0x90
.LBB0_26:                               # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_21 Depth=3
	movq	8(%r12), %rcx
	addl	$2, %r15d
	decl	%ebx
	addl	16(%rsp), %ebp          # 4-byte Folded Reload
	movl	12(%rsp), %edx          # 4-byte Reload
.LBB0_21:                               # %.lr.ph
                                        #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	128(%rsp), %r14
	movq	%r14, %rdi
	movl	%r15d, %esi
	movl	28(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	pushq	$0
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	cmpl	$8, erc_mvperMB(%rip)
	jl	.LBB0_23
# BB#22:                                #   in Loop: Header=BB0_21 Depth=3
	subq	$8, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%r14, %r8
	movl	%r13d, %r9d
	pushq	8(%r12)
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	concealByTrial
	addq	$16, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_23:                               #   in Loop: Header=BB0_21 Depth=3
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %ecx
	callq	concealByCopy
.LBB0_24:                               #   in Loop: Header=BB0_21 Depth=3
	movl	$-1, %esi
	movl	%ebp, %edi
	movl	%r13d, %edx
	movq	%r12, %rcx
	callq	ercMarkCurrMBConcealed
	testl	%ebx, %ebx
	jne	.LBB0_26
# BB#25:                                #   in Loop: Header=BB0_12 Depth=2
	movq	80(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %esi
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	12(%rsp), %edx          # 4-byte Reload
	jmp	.LBB0_44
.LBB0_28:                               # %.preheader
                                        #   in Loop: Header=BB0_12 Depth=2
	testl	%r13d, %r13d
	js	.LBB0_43
# BB#29:                                # %.lr.ph168.preheader
                                        #   in Loop: Header=BB0_12 Depth=2
	leal	(%r13,%r13), %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ebp
	imull	%r13d, %ebp
	addl	48(%rsp), %ebp          # 4-byte Folded Reload
	movl	%r13d, %r14d
	jmp	.LBB0_30
	.p2align	4, 0x90
.LBB0_34:                               # %..lr.ph168_crit_edge
                                        #   in Loop: Header=BB0_30 Depth=3
	decl	%r14d
	movq	8(%r12), %rcx
	addl	$-2, %r15d
	subl	16(%rsp), %ebp          # 4-byte Folded Reload
.LBB0_30:                               # %.lr.ph168
                                        #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	128(%rsp), %rbx
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	28(%rsp), %r8d          # 4-byte Reload
	movl	24(%rsp), %r9d          # 4-byte Reload
	pushq	$0
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	ercCollect8PredBlocks
	addq	$16, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -16
	cmpl	$8, erc_mvperMB(%rip)
	jl	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_30 Depth=3
	subq	$8, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
	movl	%ebp, %edx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rbx, %r8
	movl	16(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r9d
	pushq	8(%r12)
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	callq	concealByTrial
	addq	$16, %rsp
.Lcfi30:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_32:                               #   in Loop: Header=BB0_30 Depth=3
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	%ebp, %esi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	8(%rsp), %ebx           # 4-byte Reload
	movl	%ebx, %ecx
	callq	concealByCopy
.LBB0_33:                               #   in Loop: Header=BB0_30 Depth=3
	movl	$-1, %esi
	movl	%ebp, %edi
	movl	%ebx, %edx
	movq	%r12, %rcx
	callq	ercMarkCurrMBConcealed
	testl	%r14d, %r14d
	movl	12(%rsp), %edx          # 4-byte Reload
	jg	.LBB0_34
.LBB0_43:                               # %._crit_edge
                                        #   in Loop: Header=BB0_12 Depth=2
	incl	%r13d
	movl	%r13d, %esi
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB0_44
	.p2align	4, 0x90
.LBB0_12:                               #   Parent Loop BB0_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_15 Depth 3
                                        #       Child Loop BB0_21 Depth 3
                                        #       Child Loop BB0_37 Depth 3
                                        #       Child Loop BB0_30 Depth 3
	movq	8(%r12), %rcx
	movl	%edi, %eax
	imull	%esi, %eax
	addl	%edx, %eax
	cltq
	cmpl	$1, (%rcx,%rax,4)
	jg	.LBB0_13
# BB#14:                                # %.preheader161.preheader
                                        #   in Loop: Header=BB0_12 Depth=2
	movq	%rsi, %r8
	movslq	%esi, %r13
	leal	1(%r13), %eax
	imull	%edi, %eax
	addl	%edx, %eax
	.p2align	4, 0x90
.LBB0_15:                               # %.preheader161
                                        #   Parent Loop BB0_10 Depth=1
                                        #     Parent Loop BB0_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	1(%r13), %rbp
	cmpq	%rbx, %rbp
	jge	.LBB0_48
# BB#16:                                #   in Loop: Header=BB0_15 Depth=3
	movslq	%eax, %rsi
	leal	(%rsi,%rdi), %eax
	cmpl	$1, (%rcx,%rsi,4)
	movq	%rbp, %r13
	jle	.LBB0_15
# BB#17:                                # %._crit_edge185
                                        #   in Loop: Header=BB0_12 Depth=2
	decl	%ebp
	movl	%ebp, %r13d
	cmpl	%r14d, %r13d
	jl	.LBB0_27
	jmp	.LBB0_19
	.p2align	4, 0x90
.LBB0_13:                               #   in Loop: Header=BB0_12 Depth=2
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
.LBB0_44:                               # %.loopexit
                                        #   in Loop: Header=BB0_12 Depth=2
	incl	%esi
	cmpl	%r14d, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	jl	.LBB0_12
.LBB0_45:                               # %._crit_edge172
                                        #   in Loop: Header=BB0_10 Depth=1
	movl	96(%rsp), %edx          # 4-byte Reload
	incl	%edx
	cmpl	16(%rsp), %edx          # 4-byte Folded Reload
	jne	.LBB0_10
.LBB0_46:                               # %._crit_edge176
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	free
	movl	$1, %eax
.LBB0_47:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ercConcealInterFrame, .Lfunc_end0-ercConcealInterFrame
	.cfi_endproc

	.p2align	4, 0x90
	.type	concealByTrial,@function
concealByTrial:                         # @concealByTrial
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 208
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%rcx, %r15
	movl	%edx, %ebx
	movq	%rsi, %r14
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movl	%r9d, %edi
	sarl	$4, %edi
	leal	(,%rbx,4), %eax
	movslq	%eax, %rcx
	movl	%r9d, 16(%rsp)          # 4-byte Spill
	movl	%r9d, %esi
	sarl	$3, %esi
	movl	%ebx, %eax
	cltd
	idivl	%edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	imull	%esi, %eax
	addl	%edx, %edx
	leal	(%rdx,%rax,2), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	cltd
	idivl	%esi
	shll	$3, %edx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	4(%r15,%rcx,8), %rsi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	%edx, 4(%r15,%rcx,8)
	shll	$3, %eax
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movl	%eax, 8(%r15,%rcx,8)
	movl	%ebx, %eax
	subl	%edi, %eax
	movl	%eax, 88(%rsp)          # 4-byte Spill
	leal	-1(%rbx), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	addl	%ebx, %edi
	movl	%edi, 92(%rsp)          # 4-byte Spill
	incl	%ebx
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	movl	$3, %edx
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	movq	%r15, 128(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
	movl	$4, %ebp
	movl	$0, 32(%rsp)            # 4-byte Folded Spill
	movl	$0, 36(%rsp)            # 4-byte Folded Spill
	xorl	%r12d, %r12d
	movl	%edx, 96(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB1_2:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpl	%edx, (%rax,%rbp,4)
	jl	.LBB1_37
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=2
	leal	-4(%rbp), %eax
	cmpl	$3, %eax
	ja	.LBB1_9
# BB#4:                                 #   in Loop: Header=BB1_2 Depth=2
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_5:                                #   in Loop: Header=BB1_2 Depth=2
	movl	$2, %r8d
	movl	$3, %esi
	movl	88(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edi
	jmp	.LBB1_9
.LBB1_6:                                #   in Loop: Header=BB1_2 Depth=2
	movl	$1, %r8d
	movl	$3, %esi
	movl	84(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edi
	jmp	.LBB1_9
.LBB1_7:                                #   in Loop: Header=BB1_2 Depth=2
	xorl	%r8d, %r8d
	movl	$1, %esi
	movl	92(%rsp), %eax          # 4-byte Reload
	movl	%eax, %edi
	jmp	.LBB1_9
.LBB1_8:                                #   in Loop: Header=BB1_2 Depth=2
	xorl	%r8d, %r8d
	movl	$2, %esi
	movq	120(%rsp), %rax         # 8-byte Reload
	movl	%eax, %edi
	.p2align	4, 0x90
.LBB1_9:                                #   in Loop: Header=BB1_2 Depth=2
	leal	(,%rdi,4), %eax
	cltq
	leaq	(%rax,%rax,2), %rax
	leaq	(%r15,%rax,8), %rbx
	movzbl	(%r15,%rax,8), %eax
	cmpb	$3, %al
	jb	.LBB1_12
# BB#10:                                #   in Loop: Header=BB1_2 Depth=2
	movslq	%r8d, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	cmpb	$6, (%rbx,%rcx,8)
	je	.LBB1_37
# BB#11:                                #   in Loop: Header=BB1_2 Depth=2
	movslq	%esi, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	cmpb	$6, (%rbx,%rcx,8)
	jne	.LBB1_13
	jmp	.LBB1_37
	.p2align	4, 0x90
.LBB1_12:                               #   in Loop: Header=BB1_2 Depth=2
	cmpb	$2, %al
	je	.LBB1_37
.LBB1_13:                               #   in Loop: Header=BB1_2 Depth=2
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movl	%r8d, %ebp
	xorl	%r13d, %r13d
	cmpb	$2, %al
	seta	%r13b
	xorl	%edx, %edx
	cmpl	%esi, %ebp
	sete	%dl
	incl	%edx
	subl	%edx, %r13d
	movl	%ebp, 100(%rsp)         # 4-byte Spill
	movl	%esi, 52(%rsp)          # 4-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movl	%edx, 48(%rsp)          # 4-byte Spill
	cmpb	$3, %al
	jae	.LBB1_15
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_2 Depth=2
	cmpb	$2, %al
	je	.LBB1_17
# BB#19:                                #   in Loop: Header=BB1_2 Depth=2
	testb	%al, %al
	je	.LBB1_20
# BB#23:                                # %._crit_edge26
                                        #   in Loop: Header=BB1_2 Depth=2
	movslq	%ebp, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_2 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_17
# BB#25:                                #   in Loop: Header=BB1_2 Depth=2
	movq	$0, 20(%rsp)
	movl	$0, 28(%rsp)
	movq	erc_img(%rip), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	leaq	20(%rsp), %rsi
	movq	%r14, %r8
	callq	buildPredRegionYUV
	movl	$1, 36(%rsp)            # 4-byte Folded Spill
	jmp	.LBB1_26
	.p2align	4, 0x90
.LBB1_14:                               # %._crit_edge
                                        #   in Loop: Header=BB1_2 Depth=2
	movzbl	(%rbx), %eax
	subl	%edx, %r13d
	movl	%r15d, %r12d
	movl	%esi, %ebp
	cmpb	$3, %al
	jb	.LBB1_18
.LBB1_15:                               #   in Loop: Header=BB1_2 Depth=2
	movslq	%ebp, %rcx
	leaq	(%rcx,%rcx,2), %rcx
	cmpb	$4, (%rbx,%rcx,8)
	je	.LBB1_20
# BB#16:                                #   in Loop: Header=BB1_2 Depth=2
	cmpb	$6, (%rbx,%rcx,8)
	jne	.LBB1_24
.LBB1_17:                               #   in Loop: Header=BB1_2 Depth=2
	movl	%r12d, %r15d
	testl	%r13d, %r13d
	jns	.LBB1_14
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_24:                               #   in Loop: Header=BB1_2 Depth=2
	leaq	(%rbx,%rcx,8), %rcx
	cmpb	$2, %al
	cmovbeq	%rbx, %rcx
	movl	12(%rcx), %eax
	movl	%eax, 20(%rsp)
	movl	16(%rcx), %eax
	movl	%eax, 24(%rsp)
	movl	20(%rcx), %eax
	movl	%eax, 28(%rsp)
	movq	erc_img(%rip), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	leaq	20(%rsp), %rsi
	movq	%r14, %r8
	callq	buildPredRegionYUV
.LBB1_26:                               #   in Loop: Header=BB1_2 Depth=2
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rcx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	%r14, %rbx
	movq	%r14, %rdx
	movl	16(%rsp), %r8d          # 4-byte Reload
	callq	edgeDistortion
	movl	%eax, %r14d
	movl	$1, %r15d
	testl	%r12d, %r12d
	je	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_2 Depth=2
	cmpl	32(%rsp), %r14d         # 4-byte Folded Reload
	jge	.LBB1_35
.LBB1_28:                               # %.preheader2.preheader
                                        #   in Loop: Header=BB1_2 Depth=2
	movl	28(%rsp), %eax
	movl	%eax, 80(%rsp)
	movq	20(%rsp), %rax
	movq	%rax, 72(%rsp)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movzbl	(%rcx), %eax
	cmpb	$3, %al
	jb	.LBB1_31
# BB#29:                                #   in Loop: Header=BB1_2 Depth=2
	movslq	%ebp, %rax
	leaq	(%rax,%rax,2), %rax
	cmpb	$4, (%rcx,%rax,8)
	jne	.LBB1_32
	jmp	.LBB1_33
	.p2align	4, 0x90
.LBB1_31:                               #   in Loop: Header=BB1_2 Depth=2
	testb	%al, %al
	je	.LBB1_33
.LBB1_32:                               #   in Loop: Header=BB1_2 Depth=2
	movb	$1, %al
	jmp	.LBB1_34
.LBB1_33:                               #   in Loop: Header=BB1_2 Depth=2
	xorl	%eax, %eax
.LBB1_34:                               #   in Loop: Header=BB1_2 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	movb	%al, -4(%rcx)
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%rbx, %rsi
	movl	16(%rsp), %edx          # 4-byte Reload
	callq	copyPredMB
	movl	%r14d, 32(%rsp)         # 4-byte Spill
.LBB1_35:                               #   in Loop: Header=BB1_2 Depth=2
	movq	%rbx, %r14
	movl	52(%rsp), %esi          # 4-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	48(%rsp), %edx          # 4-byte Reload
	testl	%r13d, %r13d
	jns	.LBB1_14
.LBB1_36:                               #   in Loop: Header=BB1_2 Depth=2
	movl	%r15d, %r12d
	movq	128(%rsp), %r15         # 8-byte Reload
	movl	96(%rsp), %edx          # 4-byte Reload
	movl	100(%rsp), %r8d         # 4-byte Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_37:                               # %.loopexit
                                        #   in Loop: Header=BB1_2 Depth=2
	incq	%rbp
	cmpq	$8, %rbp
	jne	.LBB1_2
# BB#38:                                #   in Loop: Header=BB1_1 Depth=1
	decl	%edx
	cmpl	$2, %edx
	jl	.LBB1_40
# BB#39:                                #   in Loop: Header=BB1_1 Depth=1
	testl	%r12d, %r12d
	je	.LBB1_1
.LBB1_40:
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_44
# BB#41:
	movq	$0, 20(%rsp)
	movl	$0, 28(%rsp)
	movq	erc_img(%rip), %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %edx
	movl	4(%rax), %ecx
	leaq	20(%rsp), %rsi
	movq	%r14, %r8
	callq	buildPredRegionYUV
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rcx
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	12(%rsp), %esi          # 4-byte Reload
	movq	%r14, %rdx
	movl	16(%rsp), %r8d          # 4-byte Reload
	callq	edgeDistortion
	testl	%r12d, %r12d
	je	.LBB1_43
# BB#42:
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jge	.LBB1_44
.LBB1_43:                               # %.preheader1.preheader
	movl	28(%rsp), %eax
	movl	%eax, 80(%rsp)
	movq	20(%rsp), %rax
	movq	%rax, 72(%rsp)
	movq	40(%rsp), %rax          # 8-byte Reload
	movb	$0, -4(%rax)
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	%r14, %rsi
	movl	16(%rsp), %edx          # 4-byte Reload
	callq	copyPredMB
.LBB1_44:                               # %.preheader.preheader
	movl	80(%rsp), %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	movl	%eax, 20(%r15,%rcx,8)
	movq	72(%rsp), %rax
	movq	%rax, 12(%r15,%rcx,8)
	movslq	12(%rsp), %rax          # 4-byte Folded Reload
	movq	208(%rsp), %rcx
	movl	$2, (%rcx,%rax,4)
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	concealByTrial, .Lfunc_end1-concealByTrial
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_5
	.quad	.LBB1_6
	.quad	.LBB1_7
	.quad	.LBB1_8

	.text
	.p2align	4, 0x90
	.type	concealByCopy,@function
concealByCopy:                          # @concealByCopy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdx, %r8
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	leal	(,%rsi,4), %eax
	cltq
	leaq	(%rax,%rax,2), %rbx
	movb	$0, (%r8,%rbx,8)
	movl	%ecx, %ebp
	sarl	$4, %ebp
	movl	%esi, %eax
	cltd
	idivl	%ebp
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%edx, %esi
	shll	$4, %esi
	movl	%esi, 4(%r8,%rbx,8)
	movl	%eax, %esi
	shll	$4, %esi
	movl	%esi, 8(%r8,%rbx,8)
	movl	%ecx, %esi
	sarl	$3, %esi
	imull	%esi, %eax
	addl	%edx, %edx
	leal	(%rdx,%rax,2), %eax
	movq	listX(%rip), %rdx
	movq	(%rdx), %rbp
	cltd
	idivl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	shll	$3, %edx
	leal	(,%rax,8), %esi
	leal	16(,%rax,8), %eax
	movq	%rbp, -32(%rsp)         # 8-byte Spill
	movq	316920(%rbp), %r10
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movq	(%rdi), %r12
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	movslq	%edx, %r14
	movl	%esi, -40(%rsp)         # 4-byte Spill
	movslq	%esi, %rbx
	movl	%ecx, -36(%rsp)         # 4-byte Spill
	movslq	%ecx, %rcx
	movl	%eax, -44(%rsp)         # 4-byte Spill
	movslq	%eax, %r15
	leaq	15(%r14), %rsi
	leaq	16(%r14), %r13
	movq	%rcx, %rbp
	imulq	%rbx, %rbp
	leaq	16(%r14,%rbp), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	leaq	16(%r12), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	(%rbp,%r14), %r11
	movq	%r11, -56(%rsp)         # 8-byte Spill
	xorl	%r8d, %r8d
	movq	%r10, -72(%rsp)         # 8-byte Spill
	movq	%r13, -8(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_10 Depth 2
                                        #     Child Loop BB2_13 Depth 2
	movl	$16, %eax
	cmpq	$16, %rax
	movq	(%r10,%rbx,8), %r9
	movq	%r14, %rax
	jb	.LBB2_12
# BB#2:                                 # %min.iters.checked
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	$16, %eax
	testq	%rax, %rax
	movq	%r14, %rax
	je	.LBB2_12
# BB#3:                                 # %vector.memcheck
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	%rcx, %rax
	imulq	%r8, %rax
	movq	-56(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax), %rdi
	leaq	(%r12,%rdi,2), %rdi
	leaq	(%r9,%r13,2), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_5
# BB#4:                                 # %vector.memcheck
                                        #   in Loop: Header=BB2_1 Depth=1
	addq	-64(%rsp), %rax         # 8-byte Folded Reload
	leaq	(%r12,%rax,2), %rax
	leaq	(%r9,%r14,2), %rdx
	cmpq	%rax, %rdx
	movq	%r14, %rax
	jb	.LBB2_12
.LBB2_5:                                # %vector.body.preheader
                                        #   in Loop: Header=BB2_1 Depth=1
	xorl	%eax, %eax
	testb	%al, %al
	jne	.LBB2_6
# BB#7:                                 # %vector.body.prol
                                        #   in Loop: Header=BB2_1 Depth=1
	movq	%rbx, %rax
	imulq	%rcx, %rax
	addq	%r14, %rax
	movups	(%r9,%r14,2), %xmm0
	movups	16(%r9,%r14,2), %xmm1
	movups	%xmm0, (%r12,%rax,2)
	movups	%xmm1, 16(%r12,%rax,2)
	movl	$16, %edi
	jmp	.LBB2_8
.LBB2_6:                                #   in Loop: Header=BB2_1 Depth=1
	xorl	%edi, %edi
.LBB2_8:                                # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB2_1 Depth=1
	movb	$1, %al
	testb	%al, %al
	jne	.LBB2_11
# BB#9:                                 # %vector.body.preheader.new
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	$16, %eax
	subq	%rdi, %rax
	leaq	(%r14,%rdi), %rdx
	leaq	48(%r9,%rdx,2), %r10
	addq	%r11, %rdi
	movq	-16(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdi,2), %r13
	.p2align	4, 0x90
.LBB2_10:                               # %vector.body
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%r10), %xmm0
	movups	-32(%r10), %xmm1
	movups	%xmm0, -16(%r13)
	movups	%xmm1, (%r13)
	movups	-16(%r10), %xmm0
	movups	(%r10), %xmm1
	movups	%xmm0, 16(%r13)
	movups	%xmm1, 32(%r13)
	addq	$64, %r10
	addq	$64, %r13
	addq	$-32, %rax
	jne	.LBB2_10
.LBB2_11:                               # %middle.block
                                        #   in Loop: Header=BB2_1 Depth=1
	movl	$16, %eax
	cmpq	%rax, %rax
	movq	-8(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rax
	movq	-72(%rsp), %r10         # 8-byte Reload
	je	.LBB2_14
	.p2align	4, 0x90
.LBB2_12:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB2_1 Depth=1
	leaq	(%rax,%rbp), %rdx
	leaq	(%r12,%rdx,2), %rdi
	decq	%rax
	.p2align	4, 0x90
.LBB2_13:                               # %scalar.ph
                                        #   Parent Loop BB2_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	2(%r9,%rax,2), %edx
	movw	%dx, (%rdi)
	addq	$2, %rdi
	incq	%rax
	cmpq	%rsi, %rax
	jl	.LBB2_13
.LBB2_14:                               # %.loopexit
                                        #   in Loop: Header=BB2_1 Depth=1
	incq	%rbx
	incq	%r8
	addq	%rcx, %r11
	addq	%rcx, %rbp
	cmpq	%r15, %rbx
	jl	.LBB2_1
# BB#15:
	movq	dec_picture(%rip), %rax
	movslq	317044(%rax), %rax
	movb	uv_div.1(,%rax,4), %cl
	movl	-40(%rsp), %edi         # 4-byte Reload
	sarl	%cl, %edi
	movl	-44(%rsp), %esi         # 4-byte Reload
	sarl	%cl, %esi
	cmpl	%esi, %edi
	jge	.LBB2_24
# BB#16:                                # %.lr.ph4.i
	movq	-80(%rsp), %rdx         # 8-byte Reload
	leal	16(%rdx), %r15d
	movl	uv_div.0(,%rax,4), %ecx
	sarl	%cl, %r15d
	sarl	%cl, %edx
	movq	%rdx, -80(%rsp)         # 8-byte Spill
	cmpl	%r15d, %edx
	jge	.LBB2_24
# BB#17:                                # %.lr.ph4.i.split.us.preheader
	movslq	%esi, %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	movq	-80(%rsp), %rsi         # 8-byte Reload
	movslq	%esi, %r12
	movslq	%r15d, %r8
	movslq	%edi, %r14
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	316928(%rax), %rax
	movq	(%rax), %rdx
	movq	%rdx, -64(%rsp)         # 8-byte Spill
	movq	8(%rax), %r10
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	8(%rax), %r9
	movq	16(%rax), %rdx
	subl	%esi, %r15d
	leaq	-1(%r8), %r11
	andl	$1, %r15d
	leaq	1(%r12), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_18:                               # %.lr.ph4.i.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_22 Depth 2
	movl	%r14d, %r13d
	imull	-36(%rsp), %r13d        # 4-byte Folded Reload
	sarl	%cl, %r13d
	testq	%r15, %r15
	movq	-64(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movq	(%r10,%r14,8), %rsi
	movq	%r12, %rbp
	je	.LBB2_20
# BB#19:                                #   in Loop: Header=BB2_18 Depth=1
	movq	-80(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%r13), %edi
	movzwl	(%rax,%r12,2), %ebp
	movslq	%edi, %rdi
	movw	%bp, (%r9,%rdi,2)
	movzwl	(%rsi,%r12,2), %ebp
	movw	%bp, (%rdx,%rdi,2)
	movq	-72(%rsp), %rbp         # 8-byte Reload
.LBB2_20:                               # %.prol.loopexit
                                        #   in Loop: Header=BB2_18 Depth=1
	cmpq	%r12, %r11
	je	.LBB2_23
# BB#21:                                # %.lr.ph4.i.split.us.new
                                        #   in Loop: Header=BB2_18 Depth=1
	movl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_22:                               #   Parent Loop BB2_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%r13,%rbp), %rbx
	movzwl	(%rax,%rbp,2), %edi
	movslq	%ebx, %rbx
	movw	%di, (%r9,%rbx,2)
	movzwl	(%rsi,%rbp,2), %edi
	movw	%di, (%rdx,%rbx,2)
	incl	%ebx
	movzwl	2(%rax,%rbp,2), %edi
	movslq	%ebx, %rbx
	movw	%di, (%r9,%rbx,2)
	movzwl	2(%rsi,%rbp,2), %edi
	movw	%di, (%rdx,%rbx,2)
	addq	$2, %rbp
	cmpq	%rbp, %r8
	jne	.LBB2_22
.LBB2_23:                               # %._crit_edge.i.loopexit.us
                                        #   in Loop: Header=BB2_18 Depth=1
	incq	%r14
	cmpq	-56(%rsp), %r14         # 8-byte Folded Reload
	jne	.LBB2_18
.LBB2_24:                               # %copyBetweenFrames.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	concealByCopy, .Lfunc_end2-concealByCopy
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.zero	16
	.text
	.globl	conceal_lost_frames
	.p2align	4, 0x90
	.type	conceal_lost_frames,@function
conceal_lost_frames:                    # @conceal_lost_frames
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi56:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi59:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi60:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi62:
	.cfi_def_cfa_offset 64
.Lcfi63:
	.cfi_offset %rbx, -56
.Lcfi64:
	.cfi_offset %r12, -48
.Lcfi65:
	.cfi_offset %r13, -40
.Lcfi66:
	.cfi_offset %r14, -32
.Lcfi67:
	.cfi_offset %r15, -24
.Lcfi68:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	5696(%r14), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	5700(%r14), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	$0, 5696(%r14)
	cmpl	$1, 6080(%r14)
	jne	.LBB3_2
# BB#1:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	subl	6064(%r14), %eax
	movl	%eax, 6056(%r14)
	movl	$0, 6072(%r14)
	jmp	.LBB3_3
.LBB3_2:
	movl	5660(%r14), %eax
	incl	%eax
	xorl	%edx, %edx
	divl	5816(%r14)
	movl	%edx, %ebp
.LBB3_3:
	movl	5676(%r14), %r13d
	cmpl	%ebp, %r13d
	je	.LBB3_15
# BB#4:                                 # %.lr.ph
	movabsq	$4294967298, %r15       # imm = 0x100000002
	.p2align	4, 0x90
.LBB3_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	movl	48(%r14), %esi
	movl	52(%r14), %edx
	movl	56(%r14), %ecx
	movl	64(%r14), %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movq	%rax, %rbx
	movl	$1, 316900(%rbx)
	movl	%ebp, 316832(%rbx)
	movl	%ebp, 316824(%rbx)
	movl	$0, 316856(%rbx)
	movl	$0, 316852(%rbx)
	movl	$1, 316848(%rbx)
	movl	$1, 317096(%rbx)
	movl	$0, 317040(%rbx)
	movl	%ebp, 5676(%r14)
	movl	6060(%r14), %eax
	addl	6056(%r14), %eax
	movd	%eax, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4(%rbx)
	movl	%eax, 6056(%r14)
	movl	dpb+28(%rip), %ecx
	decl	%ecx
	js	.LBB3_11
# BB#6:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB3_5 Depth=1
	movq	dpb(%rip), %rax
	movslq	%ecx, %rcx
	.p2align	4, 0x90
.LBB3_7:                                #   Parent Loop BB3_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rcx,8), %rdx
	cmpl	$3, (%rdx)
	jne	.LBB3_10
# BB#8:                                 #   in Loop: Header=BB3_7 Depth=2
	movq	48(%rdx), %rdi
	cmpl	$0, 316848(%rdi)
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_7 Depth=2
	cmpl	$0, 316844(%rdi)
	je	.LBB3_12
.LBB3_10:                               # %.backedge.i.i
                                        #   in Loop: Header=BB3_7 Depth=2
	decq	%rcx
	testl	%ecx, %ecx
	jns	.LBB3_7
.LBB3_11:                               #   in Loop: Header=BB3_5 Depth=1
	xorl	%edi, %edi
.LBB3_12:                               # %copy_prev_pic_to_concealed_pic.exit
                                        #   in Loop: Header=BB3_5 Depth=1
	movl	$0, 6084(%r14)
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	copy_to_conceal
	cmpl	$1, 6080(%r14)
	jne	.LBB3_14
# BB#13:                                #   in Loop: Header=BB3_5 Depth=1
	movq	%rbx, %r12
	addq	$4, %r12
	movq	%r15, 317024(%rbx)
	callq	flush_dpb
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%r12)
	movl	$0, 6056(%r14)
.LBB3_14:                               #   in Loop: Header=BB3_5 Depth=1
	movq	%rbx, %rdi
	callq	store_picture_in_dpb
	movl	%ebp, 5660(%r14)
	movaps	ref_flag+48(%rip), %xmm0
	incl	%ebp
	movl	%ebp, %eax
	cltd
	idivl	5816(%r14)
	movups	%xmm0, ref_flag+52(%rip)
	movaps	ref_flag+32(%rip), %xmm0
	movups	%xmm0, ref_flag+36(%rip)
	movaps	ref_flag+16(%rip), %xmm0
	movups	%xmm0, ref_flag+20(%rip)
	movaps	ref_flag(%rip), %xmm0
	movups	%xmm0, ref_flag+4(%rip)
	movl	$0, ref_flag(%rip)
	movl	%edx, %ebp
	cmpl	%ebp, %r13d
	jne	.LBB3_5
.LBB3_15:                               # %._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 5696(%r14)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 5700(%r14)
	movl	%r13d, 5676(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	conceal_lost_frames, .Lfunc_end3-conceal_lost_frames
	.cfi_endproc

	.globl	update_ref_list_for_concealment
	.p2align	4, 0x90
	.type	update_ref_list_for_concealment,@function
update_ref_list_for_concealment:        # @update_ref_list_for_concealment
	.cfi_startproc
# BB#0:
	movl	dpb+28(%rip), %r8d
	testq	%r8, %r8
	je	.LBB4_12
# BB#1:                                 # %.lr.ph
	xorl	%ecx, %ecx
	testb	$1, %r8b
	jne	.LBB4_3
# BB#2:
	xorl	%edx, %edx
	cmpl	$1, %r8d
	jne	.LBB4_7
	jmp	.LBB4_12
.LBB4_3:
	movq	dpb(%rip), %rdx
	movq	(%rdx), %rdx
	cmpl	$0, 44(%rdx)
	je	.LBB4_5
# BB#4:
	movq	dpb+8(%rip), %rcx
	movq	%rdx, (%rcx)
	movl	$1, %ecx
.LBB4_5:                                # %.prol.loopexit
	movl	$1, %edx
	cmpl	$1, %r8d
	je	.LBB4_12
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rsi
	movq	(%rsi,%rdx,8), %rsi
	cmpl	$0, 44(%rsi)
	je	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	movq	dpb+8(%rip), %rdi
	movl	%ecx, %eax
	incl	%ecx
	movq	%rsi, (%rdi,%rax,8)
.LBB4_9:                                #   in Loop: Header=BB4_7 Depth=1
	movq	dpb(%rip), %rax
	movq	8(%rax,%rdx,8), %rsi
	cmpl	$0, 44(%rsi)
	je	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_7 Depth=1
	movq	dpb+8(%rip), %rax
	movl	%ecx, %edi
	incl	%ecx
	movq	%rsi, (%rax,%rdi,8)
.LBB4_11:                               #   in Loop: Header=BB4_7 Depth=1
	addq	$2, %rdx
	cmpq	%r8, %rdx
	jb	.LBB4_7
.LBB4_12:                               # %._crit_edge
	movq	active_pps(%rip), %rax
	movl	1112(%rax), %eax
	movl	%eax, dpb+32(%rip)
	retq
.Lfunc_end4:
	.size	update_ref_list_for_concealment, .Lfunc_end4-update_ref_list_for_concealment
	.cfi_endproc

	.globl	init_lists_for_non_reference_loss
	.p2align	4, 0x90
	.type	init_lists_for_non_reference_loss,@function
init_lists_for_non_reference_loss:      # @init_lists_for_non_reference_loss
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 48
.Lcfi74:
	.cfi_offset %rbx, -48
.Lcfi75:
	.cfi_offset %r12, -40
.Lcfi76:
	.cfi_offset %r14, -32
.Lcfi77:
	.cfi_offset %r15, -24
.Lcfi78:
	.cfi_offset %rbp, -16
	movl	%esi, %r12d
	movl	%edi, %r14d
	movq	active_sps(%rip), %rax
	movl	1008(%rax), %ecx
	addl	$4, %ecx
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	testl	%r12d, %r12d
	jne	.LBB5_6
# BB#1:
	movl	dpb+32(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB5_6
# BB#2:                                 # %.lr.ph139
	movq	dpb(%rip), %rdx
	movq	img(%rip), %r8
	movq	dpb+8(%rip), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx,%rbp,8), %rbx
	cmpl	$1, 44(%rbx)
	jne	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movl	20(%rbx), %ebx
	cmpl	6076(%r8), %ebx
	movl	$0, %esi
	cmoval	%eax, %esi
	subl	%esi, %ebx
	movq	(%rdi,%rbp,8), %rsi
	movl	%ebx, 28(%rsi)
	movq	48(%rsi), %rsi
	movl	%ebx, 316832(%rsi)
.LBB5_5:                                #   in Loop: Header=BB5_3 Depth=1
	incq	%rbp
	cmpq	%rcx, %rbp
	jb	.LBB5_3
.LBB5_6:                                # %.loopexit
	xorl	%r15d, %r15d
	movl	%r12d, %eax
	orl	%r14d, %eax
	jne	.LBB5_10
# BB#7:                                 # %.preheader105
	movl	dpb+28(%rip), %eax
	testq	%rax, %rax
	je	.LBB5_8
# BB#13:                                # %.lr.ph135
	testb	$1, %al
	jne	.LBB5_15
# BB#14:
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	cmpl	$1, %eax
	jne	.LBB5_20
	jmp	.LBB5_9
.LBB5_8:
	xorl	%r15d, %r15d
	jmp	.LBB5_9
.LBB5_15:
	movq	dpb(%rip), %rcx
	movq	(%rcx), %rcx
	cmpl	$1, 44(%rcx)
	jne	.LBB5_16
# BB#17:
	movq	48(%rcx), %rcx
	movq	listX(%rip), %rdx
	movq	%rcx, (%rdx)
	movl	$1, %r15d
	jmp	.LBB5_18
.LBB5_16:
	xorl	%r15d, %r15d
.LBB5_18:                               # %.prol.loopexit171
	movl	$1, %ecx
	cmpl	$1, %eax
	je	.LBB5_9
	.p2align	4, 0x90
.LBB5_20:                               # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$1, 44(%rdx)
	jne	.LBB5_22
# BB#21:                                #   in Loop: Header=BB5_20 Depth=1
	movq	48(%rdx), %rdx
	movq	listX(%rip), %rsi
	movslq	%r15d, %rdi
	incl	%r15d
	movq	%rdx, (%rsi,%rdi,8)
.LBB5_22:                               #   in Loop: Header=BB5_20 Depth=1
	movq	dpb(%rip), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	cmpl	$1, 44(%rdx)
	jne	.LBB5_24
# BB#23:                                #   in Loop: Header=BB5_20 Depth=1
	movq	48(%rdx), %rdx
	movq	listX(%rip), %rsi
	movslq	%r15d, %rdi
	incl	%r15d
	movq	%rdx, (%rsi,%rdi,8)
.LBB5_24:                               #   in Loop: Header=BB5_20 Depth=1
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jb	.LBB5_20
.LBB5_9:                                # %._crit_edge136
	movq	listX(%rip), %rdi
	movslq	%r15d, %rbp
	movl	$8, %edx
	movl	$compare_pic_by_pic_num_desc, %ecx
	movq	%rbp, %rsi
	callq	qsort
	movl	%ebp, listXsize(%rip)
.LBB5_10:
	cmpl	$1, %r14d
	jne	.LBB5_12
# BB#11:
	testl	%r12d, %r12d
	jne	.LBB5_12
# BB#25:                                # %.preheader104
	movl	dpb+28(%rip), %eax
	testq	%rax, %rax
	je	.LBB5_31
# BB#26:                                # %.lr.ph130
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_27:                               # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$1, 44(%rdx)
	jne	.LBB5_30
# BB#28:                                #   in Loop: Header=BB5_27 Depth=1
	movq	img(%rip), %rsi
	movl	6072(%rsi), %esi
	movq	48(%rdx), %rdx
	cmpl	4(%rdx), %esi
	jle	.LBB5_30
# BB#29:                                #   in Loop: Header=BB5_27 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r15d, %rdi
	incl	%r15d
	movq	%rdx, (%rsi,%rdi,8)
.LBB5_30:                               #   in Loop: Header=BB5_27 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB5_27
.LBB5_31:                               # %._crit_edge131
	movq	listX(%rip), %rdi
	movslq	%r15d, %rbx
	movl	$8, %edx
	movl	$compare_pic_by_poc_desc, %ecx
	movq	%rbx, %rsi
	callq	qsort
	movl	dpb+28(%rip), %eax
	testq	%rax, %rax
	movl	%r15d, %r14d
	je	.LBB5_37
# BB#32:                                # %.lr.ph125
	xorl	%ecx, %ecx
	movl	%r15d, %r14d
	.p2align	4, 0x90
.LBB5_33:                               # =>This Inner Loop Header: Depth=1
	movq	dpb(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	cmpl	$1, 44(%rdx)
	jne	.LBB5_36
# BB#34:                                #   in Loop: Header=BB5_33 Depth=1
	movq	img(%rip), %rsi
	movl	6072(%rsi), %esi
	movq	48(%rdx), %rdx
	cmpl	4(%rdx), %esi
	jge	.LBB5_36
# BB#35:                                #   in Loop: Header=BB5_33 Depth=1
	movq	listX(%rip), %rsi
	movslq	%r14d, %rdi
	incl	%r14d
	movq	%rdx, (%rsi,%rdi,8)
.LBB5_36:                               #   in Loop: Header=BB5_33 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB5_33
.LBB5_37:                               # %._crit_edge126
	movq	listX(%rip), %rax
	leaq	(%rax,%rbx,8), %rdi
	movl	%r14d, %eax
	subl	%ebx, %eax
	movslq	%eax, %rbp
	movl	$8, %edx
	movl	$compare_pic_by_poc_asc, %ecx
	movq	%rbp, %rsi
	callq	qsort
	testl	%ebx, %ebx
	jle	.LBB5_45
# BB#38:                                # %.lr.ph121.preheader
	movl	%r15d, %r9d
	leaq	-1(%r9), %r8
	movq	%r9, %rsi
	andq	$3, %rsi
	je	.LBB5_39
# BB#40:                                # %.lr.ph121.prol.preheader
	leaq	(,%rbp,8), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB5_41:                               # %.lr.ph121.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	listX+8(%rip), %rax
	addq	%rdi, %rax
	movq	%rdx, (%rax,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB5_41
	jmp	.LBB5_42
.LBB5_12:                               # %._crit_edge161
	movl	listXsize(%rip), %r8d
	movl	listXsize+4(%rip), %r14d
	cmpl	$2, %r8d
	jge	.LBB5_55
	jmp	.LBB5_66
.LBB5_39:
	xorl	%ecx, %ecx
.LBB5_42:                               # %.lr.ph121.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB5_45
# BB#43:                                # %.lr.ph121.preheader.new
	shlq	$3, %rbp
	.p2align	4, 0x90
.LBB5_44:                               # %.lr.ph121
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, (%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	8(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 8(%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	16(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 16(%rdx,%rcx,8)
	movq	listX(%rip), %rax
	movq	24(%rax,%rcx,8), %rax
	movq	listX+8(%rip), %rdx
	addq	%rbp, %rdx
	movq	%rax, 24(%rdx,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %r9
	jne	.LBB5_44
.LBB5_45:                               # %.preheader103
	movslq	%r14d, %rbp
	movl	%r14d, %edx
	subl	%r15d, %edx
	jle	.LBB5_53
# BB#46:                                # %.lr.ph117.preheader
	leaq	-1(%rbp), %r8
	subq	%rbx, %r8
	andq	$3, %rdx
	movq	%rbx, %rax
	je	.LBB5_50
# BB#47:                                # %.lr.ph117.prol.preheader
	leaq	(,%rbx,8), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_48:                               # %.lr.ph117.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rdi
	addq	%rsi, %rdi
	movq	(%rdi,%rax,8), %rdi
	movq	listX+8(%rip), %rcx
	movq	%rdi, (%rcx,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB5_48
# BB#49:                                # %.lr.ph117.prol.loopexit.unr-lcssa
	addq	%rbx, %rax
.LBB5_50:                               # %.lr.ph117.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB5_53
# BB#51:                                # %.lr.ph117.preheader.new
	shlq	$3, %rbx
	negq	%rbx
	.p2align	4, 0x90
.LBB5_52:                               # %.lr.ph117
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	listX+8(%rip), %rdx
	addq	%rbx, %rdx
	movq	%rcx, (%rdx,%rax,8)
	movq	listX(%rip), %rcx
	movq	8(%rcx,%rax,8), %rcx
	movq	listX+8(%rip), %rdx
	addq	%rbx, %rdx
	movq	%rcx, 8(%rdx,%rax,8)
	movq	listX(%rip), %rcx
	movq	16(%rcx,%rax,8), %rcx
	movq	listX+8(%rip), %rdx
	addq	%rbx, %rdx
	movq	%rcx, 16(%rdx,%rax,8)
	movq	listX(%rip), %rcx
	movq	24(%rcx,%rax,8), %rcx
	movq	listX+8(%rip), %rdx
	addq	%rbx, %rdx
	movq	%rcx, 24(%rdx,%rax,8)
	addq	$4, %rax
	cmpq	%rax, %rbp
	jne	.LBB5_52
.LBB5_53:                               # %._crit_edge118
	movl	%r14d, listXsize+4(%rip)
	movl	%r14d, listXsize(%rip)
	movq	listX(%rip), %rax
	leaq	(%rax,%rbp,8), %rdi
	xorl	%esi, %esi
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	callq	qsort
	movslq	listXsize(%rip), %rax
	leaq	(,%rax,8), %rdi
	addq	listX+8(%rip), %rdi
	subq	%rax, %rbp
	movl	$8, %edx
	movl	$compare_pic_by_lt_pic_num_asc, %ecx
	movq	%rbp, %rsi
	callq	qsort
	movl	%r14d, listXsize+4(%rip)
	movl	%r14d, listXsize(%rip)
	movl	%r14d, %r8d
	cmpl	$2, %r8d
	jl	.LBB5_66
.LBB5_55:
	cmpl	%r14d, %r8d
	jne	.LBB5_66
# BB#56:                                # %.preheader
	testl	%r14d, %r14d
	jle	.LBB5_65
# BB#57:                                # %.lr.ph114
	movq	listX(%rip), %rcx
	movq	listX+8(%rip), %rdx
	movslq	%r14d, %r10
	leaq	-1(%r10), %r9
	movq	%r10, %rsi
	xorl	%edi, %edi
	andq	$3, %rsi
	je	.LBB5_58
# BB#59:                                # %.prol.preheader
	movl	$1, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_60:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rax
	cmpq	(%rdx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	incq	%rdi
	cmpq	%rdi, %rsi
	jne	.LBB5_60
	jmp	.LBB5_61
.LBB5_58:
	xorl	%ebp, %ebp
.LBB5_61:                               # %.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB5_64
# BB#62:                                # %.lr.ph114.new
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB5_63:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rax
	cmpq	(%rdx,%rdi,8), %rax
	movq	8(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	8(%rdx,%rdi,8), %rax
	movq	16(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	16(%rdx,%rdi,8), %rax
	movq	24(%rcx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	cmpq	24(%rdx,%rdi,8), %rax
	cmovnel	%ebx, %ebp
	addq	$4, %rdi
	cmpq	%r10, %rdi
	jl	.LBB5_63
.LBB5_64:                               # %._crit_edge115
	testl	%ebp, %ebp
	jne	.LBB5_66
.LBB5_65:                               # %._crit_edge115.thread
	movq	listX+8(%rip), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	%rdx, (%rax)
	movq	listX+8(%rip), %rax
	movq	%rcx, 8(%rax)
.LBB5_66:
	movq	active_sps(%rip), %rax
	movl	2060(%rax), %eax
	cmpl	%eax, %r8d
	cmovlel	%r8d, %eax
	movl	%eax, listXsize(%rip)
	movl	$0, listXsize+4(%rip)
	cmpl	$32, %eax
	ja	.LBB5_69
# BB#67:                                # %.lr.ph110.preheader
	movl	%eax, %eax
	.p2align	4, 0x90
.LBB5_68:                               # %.lr.ph110
                                        # =>This Inner Loop Header: Depth=1
	movq	listX(%rip), %rcx
	movq	$0, (%rcx,%rax,8)
	incq	%rax
	cmpl	$33, %eax
	jb	.LBB5_68
.LBB5_69:                               # %.lr.ph
	movq	listX+8(%rip), %rax
	movq	$0, (%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 8(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 16(%rax)
	movq	$0, 24(%rax)
	movq	$0, 32(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 40(%rax)
	movq	$0, 48(%rax)
	movq	$0, 56(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 64(%rax)
	movq	$0, 72(%rax)
	movq	$0, 80(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 88(%rax)
	movq	$0, 96(%rax)
	movq	$0, 104(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 112(%rax)
	movq	$0, 120(%rax)
	movq	$0, 128(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 136(%rax)
	movq	$0, 144(%rax)
	movq	$0, 152(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 160(%rax)
	movq	$0, 168(%rax)
	movq	$0, 176(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 184(%rax)
	movq	$0, 192(%rax)
	movq	$0, 200(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 208(%rax)
	movq	$0, 216(%rax)
	movq	$0, 224(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 232(%rax)
	movq	$0, 240(%rax)
	movq	$0, 248(%rax)
	movq	listX+8(%rip), %rax
	movq	$0, 256(%rax)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	init_lists_for_non_reference_loss, .Lfunc_end5-init_lists_for_non_reference_loss
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_pic_num_desc,@function
compare_pic_by_pic_num_desc:            # @compare_pic_by_pic_num_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	316832(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	316832(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end6:
	.size	compare_pic_by_pic_num_desc, .Lfunc_end6-compare_pic_by_pic_num_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_poc_desc,@function
compare_pic_by_poc_desc:                # @compare_pic_by_poc_desc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	4(%rcx), %eax
	movl	$-1, %ecx
	cmovlel	%edx, %ecx
	movl	$1, %eax
	cmovgel	%ecx, %eax
	retq
.Lfunc_end7:
	.size	compare_pic_by_poc_desc, .Lfunc_end7-compare_pic_by_poc_desc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_poc_asc,@function
compare_pic_by_poc_asc:                 # @compare_pic_by_poc_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	4(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	4(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end8:
	.size	compare_pic_by_poc_asc, .Lfunc_end8-compare_pic_by_poc_asc
	.cfi_endproc

	.p2align	4, 0x90
	.type	compare_pic_by_lt_pic_num_asc,@function
compare_pic_by_lt_pic_num_asc:          # @compare_pic_by_lt_pic_num_asc
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	316836(%rax), %eax
	movq	(%rsi), %rcx
	xorl	%edx, %edx
	cmpl	316836(%rcx), %eax
	setg	%dl
	movl	$-1, %eax
	cmovgel	%edx, %eax
	retq
.Lfunc_end9:
	.size	compare_pic_by_lt_pic_num_asc, .Lfunc_end9-compare_pic_by_lt_pic_num_asc
	.cfi_endproc

	.globl	get_pic_from_dpb
	.p2align	4, 0x90
	.type	get_pic_from_dpb,@function
get_pic_from_dpb:                       # @get_pic_from_dpb
	.cfi_startproc
# BB#0:
	movl	dpb+28(%rip), %ecx
	movq	img(%rip), %rax
	movl	6068(%rax), %edx
	cmpl	$2, %edx
	je	.LBB10_3
# BB#1:
	xorl	%r8d, %r8d
	cmpl	$1, %edx
	jne	.LBB10_5
# BB#2:
	subl	6064(%rax), %edi
	jmp	.LBB10_4
.LBB10_3:
	addl	6064(%rax), %edi
.LBB10_4:
	movl	%edi, %r8d
.LBB10_5:
	movq	dpb(%rip), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB10_6:                               # =>This Inner Loop Header: Depth=1
	decl	%ecx
	js	.LBB10_9
# BB#7:                                 #   in Loop: Header=BB10_6 Depth=1
	movslq	%ecx, %rdx
	movq	(%rdi,%rdx,8), %rdx
	cmpl	%r8d, 40(%rdx)
	jne	.LBB10_6
# BB#8:
	movl	%ecx, (%rsi)
	movq	48(%rdx), %rax
.LBB10_9:                               # %.loopexit
	retq
.Lfunc_end10:
	.size	get_pic_from_dpb, .Lfunc_end10-get_pic_from_dpb
	.cfi_endproc

	.globl	comp
	.p2align	4, 0x90
	.type	comp,@function
comp:                                   # @comp
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	subl	(%rsi), %eax
	retq
.Lfunc_end11:
	.size	comp, .Lfunc_end11-comp
	.cfi_endproc

	.globl	init_node
	.p2align	4, 0x90
	.type	init_node,@function
init_node:                              # @init_node
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi79:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 32
.Lcfi82:
	.cfi_offset %rbx, -24
.Lcfi83:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$1, %edi
	movl	$24, %esi
	callq	calloc
	testq	%rax, %rax
	je	.LBB12_1
# BB#2:
	movq	%rbx, (%rax)
	movl	%ebp, 8(%rax)
	jmp	.LBB12_3
.LBB12_1:
	xorl	%eax, %eax
.LBB12_3:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end12:
	.size	init_node, .Lfunc_end12-init_node
	.cfi_endproc

	.globl	print_node
	.p2align	4, 0x90
	.type	print_node,@function
print_node:                             # @print_node
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.Lfunc_end13:
	.size	print_node, .Lfunc_end13-print_node
	.cfi_endproc

	.globl	print_list
	.p2align	4, 0x90
	.type	print_list,@function
print_list:                             # @print_list
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 16
.Lcfi85:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB14_3
	.p2align	4, 0x90
.LBB14_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %esi
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB14_1
.LBB14_3:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end14:
	.size	print_list, .Lfunc_end14-print_list
	.cfi_endproc

	.globl	add_node
	.p2align	4, 0x90
	.type	add_node,@function
add_node:                               # @add_node
	.cfi_startproc
# BB#0:
	movq	concealment_end(%rip), %rax
	addq	$16, %rax
	cmpq	$0, concealment_head(%rip)
	movl	$concealment_head, %ecx
	cmoveq	%rcx, %rax
	movq	%rdi, (%rax)
	movq	%rdi, concealment_end(%rip)
	retq
.Lfunc_end15:
	.size	add_node, .Lfunc_end15-add_node
	.cfi_endproc

	.globl	delete_node
	.p2align	4, 0x90
	.type	delete_node,@function
delete_node:                            # @delete_node
	.cfi_startproc
# BB#0:
	cmpq	%rdi, concealment_head(%rip)
	je	.LBB16_1
# BB#4:
	retq
.LBB16_1:
	movq	16(%rdi), %rax
	movq	%rax, concealment_head(%rip)
	cmpq	%rdi, concealment_end(%rip)
	jne	.LBB16_3
# BB#2:
	movq	%rax, concealment_end(%rip)
.LBB16_3:
	jmp	free                    # TAILCALL
.Lfunc_end16:
	.size	delete_node, .Lfunc_end16-delete_node
	.cfi_endproc

	.globl	delete_list
	.p2align	4, 0x90
	.type	delete_list,@function
delete_list:                            # @delete_list
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 16
.Lcfi87:
	.cfi_offset %rbx, -16
	movq	concealment_head(%rip), %rax
	testq	%rax, %rax
	je	.LBB17_6
# BB#1:
	cmpq	%rdi, %rax
	je	.LBB17_2
	.p2align	4, 0x90
.LBB17_3:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rcx
	movq	16(%rcx), %rax
	cmpq	%rdi, %rax
	jne	.LBB17_3
	jmp	.LBB17_4
.LBB17_2:
	movq	$0, concealment_head(%rip)
	xorl	%ecx, %ecx
.LBB17_4:                               # %.loopexit10
	movq	%rcx, concealment_end(%rip)
	testq	%rdi, %rdi
	je	.LBB17_6
	.p2align	4, 0x90
.LBB17_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rdi), %rbx
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB17_5
.LBB17_6:                               # %.loopexit
	popq	%rbx
	retq
.Lfunc_end17:
	.size	delete_list, .Lfunc_end17-delete_list
	.cfi_endproc

	.globl	conceal_non_ref_pics
	.p2align	4, 0x90
	.type	conceal_non_ref_pics,@function
conceal_non_ref_pics:                   # @conceal_non_ref_pics
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 64
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movl	%edi, %r13d
	movl	dpb+28(%rip), %eax
	testl	%eax, %eax
	je	.LBB18_29
# BB#1:
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	dpb+24(%rip), %esi
	movl	$pocs_in_dpb, %edi
	movl	$4, %edx
	movl	$comp, %ecx
	callq	qsort
	movl	dpb+24(%rip), %eax
	cmpl	%r13d, %eax
	je	.LBB18_28
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
                                        # implicit-def: %EBX
.LBB18_3:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_4 Depth 2
                                        #     Child Loop BB18_13 Depth 2
                                        #     Child Loop BB18_19 Depth 2
	movq	img(%rip), %rdx
	movl	%ebp, %r12d
	.p2align	4, 0x90
.LBB18_4:                               #   Parent Loop BB18_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	movl	%eax, dpb+28(%rip)
	movl	%r12d, %ebp
	leal	1(%r12), %r12d
	movl	pocs_in_dpb(,%r12,4), %ecx
	subl	pocs_in_dpb(,%rbp,4), %ecx
	cmpl	6064(%rdx), %ecx
	jle	.LBB18_5
# BB#6:                                 #   in Loop: Header=BB18_4 Depth=2
	movl	48(%rdx), %esi
	movl	52(%rdx), %eax
	movl	56(%rdx), %ecx
	movl	64(%rdx), %r8d
	xorl	%edi, %edi
	movl	%eax, %edx
	callq	alloc_storable_picture
	movq	%rax, %r14
	movl	pocs_in_dpb(,%rbp,4), %eax
	movq	img(%rip), %rdx
	movl	6064(%rdx), %ecx
	leal	(%rcx,%rax), %r15d
	cmpl	6072(%rdx), %r15d
	jg	.LBB18_8
# BB#7:                                 # %..backedge_crit_edge
                                        #   in Loop: Header=BB18_4 Depth=2
	movl	dpb+24(%rip), %eax
.LBB18_5:                               # %.backedge
                                        #   in Loop: Header=BB18_4 Depth=2
	movl	%eax, %ecx
	subl	%r13d, %ecx
	cmpl	%ecx, %r12d
	jb	.LBB18_4
	jmp	.LBB18_28
.LBB18_8:                               #   in Loop: Header=BB18_3 Depth=1
	movl	%r15d, 6072(%rdx)
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, 4(%r14)
	movl	dpb+28(%rip), %esi
	movl	6068(%rdx), %edi
	cmpl	$2, %edi
	je	.LBB18_11
# BB#9:                                 #   in Loop: Header=BB18_3 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %edi
	movq	%rbx, %r12
	jne	.LBB18_12
# BB#10:                                #   in Loop: Header=BB18_3 Depth=1
	movl	%eax, %ecx
	jmp	.LBB18_12
.LBB18_11:                              #   in Loop: Header=BB18_3 Depth=1
	addl	%r15d, %ecx
	movq	%rbx, %r12
.LBB18_12:                              #   in Loop: Header=BB18_3 Depth=1
	incl	%ebp
	movq	dpb(%rip), %rax
	.p2align	4, 0x90
.LBB18_13:                              #   Parent Loop BB18_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%esi
	js	.LBB18_14
# BB#15:                                #   in Loop: Header=BB18_13 Depth=2
	movslq	%esi, %rdi
	movq	(%rax,%rdi,8), %rdi
	cmpl	%ecx, 40(%rdi)
	jne	.LBB18_13
# BB#16:                                #   in Loop: Header=BB18_3 Depth=1
	movq	48(%rdi), %rdi
	movl	%esi, %r12d
	jmp	.LBB18_17
.LBB18_14:                              #   in Loop: Header=BB18_3 Depth=1
	xorl	%edi, %edi
.LBB18_17:                              # %get_pic_from_dpb.exit
                                        #   in Loop: Header=BB18_3 Depth=1
	leal	1(%r12), %ecx
	movl	%ecx, dpb+28(%rip)
	movl	316824(%rdi), %esi
	incl	%esi
	movl	%r12d, %ecx
	incl	%ecx
	movl	%esi, 6076(%rdx)
	je	.LBB18_24
# BB#18:                                # %.lr.ph.i
                                        #   in Loop: Header=BB18_3 Depth=1
	movl	%ecx, %r8d
	decq	%r8
	xorl	%edx, %edx
	xorl	%esi, %esi
	jmp	.LBB18_19
	.p2align	4, 0x90
.LBB18_22:                              # %._crit_edge
                                        #   in Loop: Header=BB18_19 Depth=2
	incq	%rdx
	movq	dpb(%rip), %rax
.LBB18_19:                              #   Parent Loop BB18_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rdx,8), %rax
	cmpl	$0, 44(%rax)
	je	.LBB18_21
# BB#20:                                #   in Loop: Header=BB18_19 Depth=2
	movq	dpb+8(%rip), %rcx
	movl	%esi, %ebx
	incl	%esi
	movq	%rax, (%rcx,%rbx,8)
.LBB18_21:                              #   in Loop: Header=BB18_19 Depth=2
	cmpq	%rdx, %r8
	jne	.LBB18_22
# BB#23:                                # %update_ref_list_for_concealment.exit.loopexit
                                        #   in Loop: Header=BB18_3 Depth=1
	movq	img(%rip), %rdx
.LBB18_24:                              # %update_ref_list_for_concealment.exit
                                        #   in Loop: Header=BB18_3 Depth=1
	movq	active_pps(%rip), %rax
	movl	1112(%rax), %eax
	movl	%eax, dpb+32(%rip)
	movl	$1, 6084(%rdx)
	movq	%r14, %rsi
	callq	copy_to_conceal
	movl	$1, %edi
	movl	$24, %esi
	callq	calloc
	movq	%r12, %rbx
	testq	%rax, %rax
	je	.LBB18_25
# BB#26:                                #   in Loop: Header=BB18_3 Depth=1
	movq	%r14, (%rax)
	movl	%r15d, 8(%rax)
	jmp	.LBB18_27
.LBB18_25:                              #   in Loop: Header=BB18_3 Depth=1
	xorl	%eax, %eax
.LBB18_27:                              # %init_node.exit
                                        #   in Loop: Header=BB18_3 Depth=1
	movq	concealment_end(%rip), %rcx
	addq	$16, %rcx
	cmpq	$0, concealment_head(%rip)
	movl	$concealment_head, %edx
	cmoveq	%rdx, %rcx
	movq	%rax, (%rcx)
	movq	%rax, concealment_end(%rip)
	movl	dpb+24(%rip), %eax
	movl	%eax, %ecx
	subl	%r13d, %ecx
	cmpl	%ecx, %ebp
	jb	.LBB18_3
.LBB18_28:                              # %.outer._crit_edge
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, dpb+28(%rip)
.LBB18_29:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	conceal_non_ref_pics, .Lfunc_end18-conceal_non_ref_pics
	.cfi_endproc

	.p2align	4, 0x90
	.type	copy_to_conceal,@function
copy_to_conceal:                        # @copy_to_conceal
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi107:
	.cfi_def_cfa_offset 432
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movl	$0, 4(%rdx)
	movl	316912(%rdi), %eax
	movl	%eax, 316912(%rsi)
	movl	6084(%rdx), %ebp
	movl	%ebp, 317024(%rdi)
	movl	%ebp, 317024(%rsi)
	movl	$0, 317028(%rsi)
	movl	317032(%rdi), %eax
	movl	%eax, 317032(%rsi)
	movl	317036(%rdi), %eax
	movl	%eax, 317036(%rsi)
	movl	$0, 317040(%rdi)
	movl	$0, 317040(%rsi)
	movups	317044(%rdi), %xmm0
	movups	%xmm0, 317044(%rsi)
	movups	317060(%rdi), %xmm0
	movups	%xmm0, 317060(%rsi)
	movl	317084(%rdi), %eax
	movl	%eax, 317084(%rsi)
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, dec_picture(%rip)
	movl	6068(%rdx), %edi
	cmpl	$2, %edi
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	je	.LBB19_46
# BB#1:
	cmpl	$1, %edi
	jne	.LBB19_68
# BB#2:
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	316908(%rcx), %eax
	movl	%eax, 316908(%rsi)
	movl	316912(%rcx), %eax
	movl	%eax, 316912(%rsi)
	movl	48(%rdx), %r14d
	testl	%r14d, %r14d
	jle	.LBB19_68
# BB#3:
	movl	52(%rdx), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	jle	.LBB19_68
# BB#4:                                 # %.preheader35.us.preheader.i
	movl	%edi, 88(%rsp)          # 4-byte Spill
	movl	%ebp, 96(%rsp)          # 4-byte Spill
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	316920(%rax), %r13
	movq	316928(%rax), %r10
	movq	316920(%rsi), %r8
	movq	316928(%rsi), %r11
	leaq	-1(%r14), %r12
	movl	%r14d, %r9d
	andl	$15, %r9d
	movq	%r14, %r15
	subq	%r9, %r15
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB19_5:                               # %.preheader35.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_13 Depth 2
                                        #     Child Loop BB19_17 Depth 2
                                        #     Child Loop BB19_20 Depth 2
	cmpl	$16, %r14d
	movq	(%r13,%rax,8), %rdx
	movq	(%r8,%rax,8), %rbp
	jae	.LBB19_7
# BB#6:                                 #   in Loop: Header=BB19_5 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB19_15
	.p2align	4, 0x90
.LBB19_7:                               # %min.iters.checked
                                        #   in Loop: Header=BB19_5 Depth=1
	testq	%r15, %r15
	je	.LBB19_11
# BB#8:                                 # %vector.memcheck
                                        #   in Loop: Header=BB19_5 Depth=1
	leaq	(%rdx,%r14,2), %rcx
	cmpq	%rcx, %rbp
	jae	.LBB19_12
# BB#9:                                 # %vector.memcheck
                                        #   in Loop: Header=BB19_5 Depth=1
	leaq	(%rbp,%r14,2), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB19_12
.LBB19_11:                              #   in Loop: Header=BB19_5 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB19_15
.LBB19_12:                              # %vector.body.preheader
                                        #   in Loop: Header=BB19_5 Depth=1
	leaq	16(%rdx), %rcx
	leaq	16(%rbp), %rsi
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB19_13:                              # %vector.body
                                        #   Parent Loop BB19_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	addq	$32, %rcx
	addq	$32, %rsi
	addq	$-16, %rbx
	jne	.LBB19_13
# BB#14:                                # %middle.block
                                        #   in Loop: Header=BB19_5 Depth=1
	testl	%r9d, %r9d
	movq	%r15, %rbx
	je	.LBB19_21
	.p2align	4, 0x90
.LBB19_15:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB19_5 Depth=1
	movl	%r14d, %esi
	subl	%ebx, %esi
	movq	%r12, %rcx
	subq	%rbx, %rcx
	andq	$7, %rsi
	je	.LBB19_18
# BB#16:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB19_5 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB19_17:                              # %scalar.ph.prol
                                        #   Parent Loop BB19_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdx,%rbx,2), %edi
	movw	%di, (%rbp,%rbx,2)
	incq	%rbx
	incq	%rsi
	jne	.LBB19_17
.LBB19_18:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB19_5 Depth=1
	cmpq	$7, %rcx
	jb	.LBB19_21
# BB#19:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB19_5 Depth=1
	movq	%r14, %rcx
	subq	%rbx, %rcx
	leaq	14(%rbp,%rbx,2), %rbp
	leaq	14(%rdx,%rbx,2), %rdx
	.p2align	4, 0x90
.LBB19_20:                              # %scalar.ph
                                        #   Parent Loop BB19_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-14(%rdx), %esi
	movw	%si, -14(%rbp)
	movzwl	-12(%rdx), %esi
	movw	%si, -12(%rbp)
	movzwl	-10(%rdx), %esi
	movw	%si, -10(%rbp)
	movzwl	-8(%rdx), %esi
	movw	%si, -8(%rbp)
	movzwl	-6(%rdx), %esi
	movw	%si, -6(%rbp)
	movzwl	-4(%rdx), %esi
	movw	%si, -4(%rbp)
	movzwl	-2(%rdx), %esi
	movw	%si, -2(%rbp)
	movzwl	(%rdx), %esi
	movw	%si, (%rbp)
	addq	$16, %rbp
	addq	$16, %rdx
	addq	$-8, %rcx
	jne	.LBB19_20
.LBB19_21:                              # %._crit_edge41.us.i
                                        #   in Loop: Header=BB19_5 Depth=1
	incq	%rax
	cmpq	8(%rsp), %rax           # 8-byte Folded Reload
	jne	.LBB19_5
# BB#22:                                # %.preheader34.i
	cmpl	$2, %r14d
	jl	.LBB19_45
# BB#23:                                # %.preheader34.i
	cmpl	$2, 8(%rsp)             # 4-byte Folded Reload
	jl	.LBB19_45
# BB#24:                                # %.preheader.us.preheader.i
	movq	8(%rsp), %rax           # 8-byte Reload
	shrl	%eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	shrl	%r14d
	movq	(%r10), %r8
	movq	8(%r10), %r9
	movq	(%r11), %rbp
	movq	8(%r11), %r15
	cmpl	$1, %r14d
	movl	$1, %r12d
	cmovaq	%r14, %r12
	movl	%r12d, %esi
	andl	$2147483632, %esi       # imm = 0x7FFFFFF0
	leaq	-16(%rsi), %rax
	shrq	$4, %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%r12, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB19_25:                              # %.preheader.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_37 Depth 2
                                        #     Child Loop BB19_43 Depth 2
	cmpq	$16, %r12
	movq	(%r8,%rdi,8), %rbx
	movq	(%rbp,%rdi,8), %rax
	movq	(%r9,%rdi,8), %r11
	movq	(%r15,%rdi,8), %r10
	jae	.LBB19_27
# BB#26:                                #   in Loop: Header=BB19_25 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB19_43
	.p2align	4, 0x90
.LBB19_27:                              # %min.iters.checked229
                                        #   in Loop: Header=BB19_25 Depth=1
	testq	%rsi, %rsi
	je	.LBB19_35
# BB#28:                                # %vector.memcheck262
                                        #   in Loop: Header=BB19_25 Depth=1
	leaq	(%rax,%r12,2), %rcx
	leaq	(%r10,%r12,2), %rbp
	leaq	(%rbx,%r12,2), %rdx
	leaq	(%r11,%r12,2), %r13
	cmpq	%rbp, %rax
	sbbb	%r8b, %r8b
	cmpq	%rcx, %r10
	sbbb	%r15b, %r15b
	andb	%r8b, %r15b
	cmpq	%rdx, %rax
	sbbb	%r12b, %r12b
	cmpq	%rcx, %rbx
	sbbb	%r8b, %r8b
	cmpq	%r13, %rax
	sbbb	%r9b, %r9b
	cmpq	%rcx, %r11
	sbbb	%cl, %cl
	movb	%cl, 28(%rsp)           # 1-byte Spill
	cmpq	%rdx, %r10
	sbbb	%dl, %dl
	cmpq	%rbp, %rbx
	sbbb	%cl, %cl
	movb	%cl, 24(%rsp)           # 1-byte Spill
	cmpq	%r13, %r10
	sbbb	%cl, %cl
	cmpq	%rbp, %r11
	sbbb	%r13b, %r13b
	testb	$1, %r15b
	jne	.LBB19_41
# BB#29:                                # %vector.memcheck262
                                        #   in Loop: Header=BB19_25 Depth=1
	andb	%r8b, %r12b
	andb	$1, %r12b
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB19_42
# BB#30:                                # %vector.memcheck262
                                        #   in Loop: Header=BB19_25 Depth=1
	andb	28(%rsp), %r9b          # 1-byte Folded Reload
	andb	$1, %r9b
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	jne	.LBB19_40
# BB#31:                                # %vector.memcheck262
                                        #   in Loop: Header=BB19_25 Depth=1
	andb	24(%rsp), %dl           # 1-byte Folded Reload
	andb	$1, %dl
	movq	40(%rsp), %rbp          # 8-byte Reload
	jne	.LBB19_39
# BB#32:                                # %vector.memcheck262
                                        #   in Loop: Header=BB19_25 Depth=1
	andb	%r13b, %cl
	andb	$1, %cl
	movl	$0, %ecx
	jne	.LBB19_43
# BB#33:                                # %vector.body225.preheader
                                        #   in Loop: Header=BB19_25 Depth=1
	cmpq	$0, 104(%rsp)           # 8-byte Folded Reload
	jne	.LBB19_36
# BB#34:                                # %vector.body225.prol
                                        #   in Loop: Header=BB19_25 Depth=1
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm0, (%rax)
	movups	%xmm1, 16(%rax)
	movups	(%r11), %xmm0
	movups	16(%r11), %xmm1
	movups	%xmm0, (%r10)
	movups	%xmm1, 16(%r10)
	movl	$16, %ecx
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	jne	.LBB19_37
	jmp	.LBB19_38
.LBB19_35:                              #   in Loop: Header=BB19_25 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB19_43
.LBB19_36:                              #   in Loop: Header=BB19_25 Depth=1
	xorl	%ecx, %ecx
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	je	.LBB19_38
	.p2align	4, 0x90
.LBB19_37:                              # %vector.body225
                                        #   Parent Loop BB19_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%rbx,%rcx,2), %xmm0
	movups	16(%rbx,%rcx,2), %xmm1
	movups	%xmm0, (%rax,%rcx,2)
	movups	%xmm1, 16(%rax,%rcx,2)
	movups	(%r11,%rcx,2), %xmm0
	movups	16(%r11,%rcx,2), %xmm1
	movups	%xmm0, (%r10,%rcx,2)
	movups	%xmm1, 16(%r10,%rcx,2)
	movups	32(%rbx,%rcx,2), %xmm0
	movups	48(%rbx,%rcx,2), %xmm1
	movups	%xmm0, 32(%rax,%rcx,2)
	movups	%xmm1, 48(%rax,%rcx,2)
	movups	32(%r11,%rcx,2), %xmm0
	movups	48(%r11,%rcx,2), %xmm1
	movups	%xmm0, 32(%r10,%rcx,2)
	movups	%xmm1, 48(%r10,%rcx,2)
	addq	$32, %rcx
	cmpq	%rcx, %rsi
	jne	.LBB19_37
.LBB19_38:                              # %middle.block226
                                        #   in Loop: Header=BB19_25 Depth=1
	cmpq	%rsi, %r12
	movq	%rsi, %rcx
	jne	.LBB19_43
	jmp	.LBB19_44
.LBB19_39:                              #   in Loop: Header=BB19_25 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB19_43
.LBB19_40:                              #   in Loop: Header=BB19_25 Depth=1
	xorl	%ecx, %ecx
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB19_43
.LBB19_41:                              #   in Loop: Header=BB19_25 Depth=1
	xorl	%ecx, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	jmp	.LBB19_43
.LBB19_42:                              #   in Loop: Header=BB19_25 Depth=1
	xorl	%ecx, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB19_43:                              # %scalar.ph227
                                        #   Parent Loop BB19_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%rcx,2), %edx
	movw	%dx, (%rax,%rcx,2)
	movzwl	(%r11,%rcx,2), %edx
	movw	%dx, (%r10,%rcx,2)
	incq	%rcx
	cmpq	%r14, %rcx
	jl	.LBB19_43
.LBB19_44:                              # %._crit_edge.us.i
                                        #   in Loop: Header=BB19_25 Depth=1
	incq	%rdi
	cmpq	8(%rsp), %rdi           # 8-byte Folded Reload
	jl	.LBB19_25
.LBB19_45:                              # %CopyImgData.exit
	cmpl	$2, 88(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	96(%rsp), %ebp          # 4-byte Reload
	jne	.LBB19_68
.LBB19_46:
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 317044(%rax)
	je	.LBB19_48
# BB#47:
	movl	%ebp, %r14d
	movl	5936(%rdx), %eax
	imull	5932(%rdx), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	sarl	$3, %ecx
	movslq	%ecx, %rax
	leaq	32(%rax,%rax), %rdi
	jmp	.LBB19_49
.LBB19_48:
	movl	%ebp, %r14d
	movl	$32, %edi
.LBB19_49:
	movq	%rdx, %rbp
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, erc_img(%rip)
	movq	64(%rsp), %rax          # 8-byte Reload
	movslq	316908(%rax), %rbx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%ebx, 316908(%rsi)
	movl	316912(%rax), %ecx
	xorl	%edx, %edx
	movl	%ecx, %eax
	movq	%rbx, %rdi
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	divl	%ebx
	movl	%eax, %ebx
	movl	%r14d, %edi
	cmpl	$1, %edi
	sete	%r14b
	cmpl	$1, %edi
	movl	%ecx, 316912(%rsi)
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	5592(%rbp), %rax
	movl	16(%rax), %esi
	jne	.LBB19_51
# BB#50:
	movl	$1, %edi
	callq	init_lists_for_non_reference_loss
	jmp	.LBB19_52
.LBB19_51:
	callq	init_lists
.LBB19_52:                              # %.preheader194
	testl	%ebx, %ebx
	movq	80(%rsp), %r8           # 8-byte Reload
	jle	.LBB19_67
# BB#53:                                # %.lr.ph204
	xorl	%eax, %eax
	movb	%r14b, %al
	incl	%eax
	movl	%eax, 124(%rsp)         # 4-byte Spill
	shll	$2, %ebx
	movq	136(%rsp), %rax         # 8-byte Reload
	leaq	(,%rax,4), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movslq	%ebx, %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB19_54:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_56 Depth 2
                                        #       Child Loop BB19_62 Depth 3
	cmpl	$0, 136(%rsp)           # 4-byte Folded Reload
	jle	.LBB19_66
# BB#55:                                # %.lr.ph
                                        #   in Loop: Header=BB19_54 Depth=1
	leal	(,%rcx,4), %esi
	movl	%ecx, %eax
	shll	$4, %eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movslq	%esi, %rdx
	leal	(%rcx,%rcx), %eax
	movslq	%eax, %rdi
	movq	%rdx, %rax
	orq	$1, %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	%rdx, %rax
	orq	$2, %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	%rdx, %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	orq	$3, %rdx
	movq	%rdx, 248(%rsp)         # 8-byte Spill
	movq	%rdi, 232(%rsp)         # 8-byte Spill
	leaq	1(%rdi), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movl	%esi, 128(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB19_56:                              #   Parent Loop BB19_54 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_62 Depth 3
	leal	(,%rbx,4), %r9d
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	316952(%rax), %r10
	movq	316976(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rbx,8), %rdi
	movswl	(%rdi), %eax
	cltd
	movl	124(%rsp), %ebp         # 4-byte Reload
	idivl	%ebp
	movl	%eax, %r14d
	movswl	2(%rdi), %eax
	cltd
	idivl	%ebp
	movl	%eax, %r12d
	movq	(%r10), %rax
	movq	(%rax,%rcx,8), %rax
	movb	(%rax,%rbx), %al
	testb	%al, %al
	jns	.LBB19_58
# BB#57:                                #   in Loop: Header=BB19_56 Depth=2
	xorl	%eax, %eax
.LBB19_58:                              #   in Loop: Header=BB19_56 Depth=2
	movzbl	%al, %r15d
	movq	316976(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rbx,8), %rax
	movw	%r14w, (%rax)
	movw	%r12w, 2(%rax)
	movq	316952(%r8), %rax
	movq	(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movb	%r15b, (%rax,%rbx)
	movl	%r9d, %eax
	orl	%esi, %eax
	testb	$12, %al
	jne	.LBB19_60
# BB#59:                                #   in Loop: Header=BB19_56 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	incl	4(%rax)
.LBB19_60:                              #   in Loop: Header=BB19_56 Depth=2
	movslq	%r9d, %rdx
	movq	erc_img(%rip), %rbp
	movq	dec_picture(%rip), %rax
	movslq	317044(%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ebx, 72(%rbp)
	movl	%ecx, 68(%rbp)
	movl	%esi, 76(%rbp)
	movl	5936(%rbp), %eax
	imull	%ecx, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movl	%ecx, 88(%rbp)
	movq	%rdx, 296(%rsp)         # 8-byte Spill
	movl	%edx, 92(%rbp)
	movl	5932(%rbp), %eax
	imull	%ebx, %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$30, %ecx
	addl	%eax, %ecx
	sarl	$2, %ecx
	movl	%ecx, 96(%rbp)
	movq	%rbx, 304(%rsp)         # 8-byte Spill
	movl	%ebx, %edx
	shll	$4, %edx
	addl	%r14d, %edx
	movq	280(%rsp), %rax         # 8-byte Reload
	leal	(%r12,%rax), %ecx
	movq	listX(%rip), %rsi
	movl	%r15d, %edi
	movq	%rbp, %r8
	leaq	144(%rsp), %r9
	callq	get_block
	movzwl	144(%rsp), %eax
	movw	%ax, 104(%rbp)
	movzwl	148(%rsp), %ecx
	movw	%cx, 106(%rbp)
	movzwl	152(%rsp), %ecx
	movw	%cx, 108(%rbp)
	movzwl	156(%rsp), %ecx
	movw	%cx, 110(%rbp)
	movzwl	160(%rsp), %ecx
	movw	%cx, 136(%rbp)
	movzwl	164(%rsp), %ecx
	movw	%cx, 138(%rbp)
	movzwl	168(%rsp), %ecx
	movw	%cx, 140(%rbp)
	movzwl	172(%rsp), %ecx
	movw	%cx, 142(%rbp)
	movzwl	176(%rsp), %ecx
	movw	%cx, 168(%rbp)
	movzwl	180(%rsp), %ecx
	movw	%cx, 170(%rbp)
	movzwl	184(%rsp), %ecx
	movw	%cx, 172(%rbp)
	movzwl	188(%rsp), %ecx
	movw	%cx, 174(%rbp)
	movzwl	192(%rsp), %ecx
	movw	%cx, 200(%rbp)
	movzwl	196(%rsp), %ecx
	movw	%cx, 202(%rbp)
	movzwl	200(%rsp), %ecx
	movw	%cx, 204(%rbp)
	movzwl	204(%rsp), %ecx
	movw	%cx, 206(%rbp)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movw	%ax, (%rcx)
	movzwl	106(%rbp), %eax
	movw	%ax, 2(%rcx)
	movzwl	108(%rbp), %eax
	movw	%ax, 4(%rcx)
	movzwl	110(%rbp), %eax
	movw	%ax, 6(%rcx)
	movzwl	136(%rbp), %eax
	movw	%ax, 8(%rcx)
	movzwl	138(%rbp), %eax
	movw	%ax, 10(%rcx)
	movzwl	140(%rbp), %eax
	movw	%ax, 12(%rcx)
	movzwl	142(%rbp), %eax
	movw	%ax, 14(%rcx)
	movzwl	168(%rbp), %eax
	movw	%ax, 16(%rcx)
	movzwl	170(%rbp), %eax
	movw	%ax, 18(%rcx)
	movzwl	172(%rbp), %eax
	movw	%ax, 20(%rcx)
	movzwl	174(%rbp), %eax
	movw	%ax, 22(%rcx)
	movzwl	200(%rbp), %eax
	movw	%ax, 24(%rcx)
	movzwl	202(%rbp), %eax
	movw	%ax, 26(%rcx)
	movzwl	204(%rbp), %eax
	movw	%ax, 28(%rcx)
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movzwl	206(%rbp), %eax
	movw	%ax, 30(%rcx)
	movq	dec_picture(%rip), %r10
	cmpl	$0, 317044(%r10)
	je	.LBB19_63
# BB#61:                                #   in Loop: Header=BB19_56 Depth=2
	movq	8(%rsp), %r11           # 8-byte Reload
	shlq	$5, %r11
	movl	$64, %eax
	xorl	%edx, %edx
	movq	48(%rsp), %rsi          # 8-byte Reload
	idivl	5932(%rsi)
	movl	%eax, %edi
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	5936(%rsi)
	movl	%eax, %ebx
	imull	%edi, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	sarl	%eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movzbl	subblk_offset_x-32(%r11), %ecx
	movl	88(%rsi), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	96(%rsi), %esi
	leal	(%rsi,%rcx), %ebp
	movq	listX(%rip), %rax
	movq	(%rax,%r15,8), %rax
	movq	316928(%rax), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	imull	%edi, %ebp
	addl	%r14d, %ebp
	movl	%ebp, %eax
	cltd
	idivl	%edi
	movl	%eax, %r13d
	testl	%r13d, %r13d
	movl	$0, %r15d
	cmovsl	%r15d, %r13d
	leal	-1(%rdi,%rbp), %eax
	leal	1(%rsi,%rcx), %esi
	cltd
	leal	-1(%rdi), %ecx
	idivl	%edi
	testl	%eax, %eax
	cmovsl	%r15d, %eax
	movq	%r12, %r8
	movl	%eax, %r12d
	andl	%ecx, %ebp
	imull	%edi, %esi
	addl	%r14d, %esi
	movl	%edi, %eax
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	subl	%ebp, %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	%esi, %eax
	cltd
	idivl	%edi
	movl	%eax, %r14d
	testl	%r14d, %r14d
	cmovsl	%r15d, %r14d
	leal	-1(%rdi,%rsi), %eax
	cltd
	idivl	%edi
	testl	%eax, %eax
	cmovsl	%r15d, %eax
	andl	%ecx, %esi
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	subl	%esi, %edi
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	316872(%r10), %r9d
	movl	316876(%r10), %r10d
	decl	%r9d
	decl	%r10d
	cmpl	%r9d, %r13d
	cmovgl	%r9d, %r13d
	cmpl	%r9d, %r12d
	cmovgl	%r9d, %r12d
	cmpl	%r9d, %r14d
	cmovgl	%r9d, %r14d
	cmpl	%r9d, %eax
	cmovlel	%eax, %r9d
	movzbl	subblk_offset_y-32(%r11), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
	leal	(%rdx,%rax), %ecx
	leal	1(%rdx,%rax), %edi
	imull	%ebx, %ecx
	addl	%r8d, %ecx
	movl	%ecx, %eax
	cltd
	idivl	%ebx
	movl	%eax, %r11d
	testl	%r11d, %r11d
	cmovsl	%r15d, %r11d
	cmpl	%r10d, %r11d
	cmovgl	%r10d, %r11d
	leal	-1(%rbx,%rcx), %eax
	cltd
	idivl	%ebx
	movl	%eax, %esi
	testl	%esi, %esi
	cmovsl	%r15d, %esi
	cmpl	%r10d, %esi
	cmovgl	%r10d, %esi
	imull	%ebx, %edi
	addl	%r8d, %edi
	leal	-1(%rbx), %r8d
	andl	%r8d, %ecx
	movl	%ebx, %eax
	movq	%rcx, 368(%rsp)         # 8-byte Spill
	subl	%ecx, %eax
	movl	%eax, 132(%rsp)         # 4-byte Spill
	movl	%edi, %eax
	cltd
	idivl	%ebx
	movl	%eax, %ebp
	testl	%ebp, %ebp
	cmovsl	%r15d, %ebp
	cmpl	%r10d, %ebp
	cmovgl	%r10d, %ebp
	leal	-1(%rbx,%rdi), %eax
	cltd
	idivl	%ebx
	testl	%eax, %eax
	cmovsl	%r15d, %eax
	cmpl	%r10d, %eax
	cmovgl	%r10d, %eax
	andl	%r8d, %edi
	movslq	%r13d, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	%r12d, %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	movslq	%r14d, %rcx
	movq	%rcx, 344(%rsp)         # 8-byte Spill
	movslq	%r9d, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movslq	%r11d, %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movslq	%esi, %rcx
	movq	%rcx, 328(%rsp)         # 8-byte Spill
	movq	%rdi, 360(%rsp)         # 8-byte Spill
	subl	%edi, %ebx
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movslq	%ebp, %rcx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, 312(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB19_62:                              # %.preheader.preheader.i206
                                        #   Parent Loop BB19_54 Depth=1
                                        #     Parent Loop BB19_56 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r11,8), %r14
	movq	336(%rsp), %rax         # 8-byte Reload
	movq	(%r14,%rax,8), %rdi
	movq	328(%rsp), %rax         # 8-byte Reload
	movq	(%r14,%rax,8), %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	(%rdi,%rax,2), %edx
	movl	40(%rsp), %esi          # 4-byte Reload
	imull	%esi, %edx
	movzwl	(%rcx,%rax,2), %eax
	imull	%esi, %eax
	movq	352(%rsp), %rbp         # 8-byte Reload
	movzwl	(%rcx,%rbp,2), %esi
	movq	104(%rsp), %r8          # 8-byte Reload
	imull	%r8d, %esi
	addl	%eax, %esi
	movzwl	(%rdi,%rbp,2), %eax
	imull	%r8d, %eax
	addl	%edx, %eax
	movq	368(%rsp), %r13         # 8-byte Reload
	imull	%r13d, %esi
	movl	132(%rsp), %r9d         # 4-byte Reload
	imull	%r9d, %eax
	addl	24(%rsp), %eax          # 4-byte Folded Reload
	addl	%esi, %eax
	cltd
	idivl	28(%rsp)                # 4-byte Folded Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	movw	%ax, 104(%r10)
	movq	344(%rsp), %r12         # 8-byte Reload
	movzwl	(%rdi,%r12,2), %edx
	movq	8(%rsp), %rbx           # 8-byte Reload
	imull	%ebx, %edx
	movq	56(%rsp), %rsi          # 8-byte Reload
	movzwl	(%rdi,%rsi,2), %eax
	movq	%rsi, %rdi
	movq	88(%rsp), %r15          # 8-byte Reload
	imull	%r15d, %eax
	movzwl	(%rcx,%r12,2), %esi
	imull	%ebx, %esi
	movzwl	(%rcx,%rdi,2), %ecx
	imull	%r15d, %ecx
	addl	%esi, %ecx
	imull	%r13d, %ecx
	addl	%edx, %eax
	imull	%r9d, %eax
	movl	24(%rsp), %edi          # 4-byte Reload
	addl	%edi, %eax
	addl	%ecx, %eax
	cltd
	movl	28(%rsp), %r9d          # 4-byte Reload
	idivl	%r9d
	movw	%ax, 106(%r10)
	movq	320(%rsp), %rax         # 8-byte Reload
	movq	(%r14,%rax,8), %r13
	movq	312(%rsp), %rax         # 8-byte Reload
	movq	(%r14,%rax,8), %rcx
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	(%r13,%rax,2), %edx
	movl	40(%rsp), %esi          # 4-byte Reload
	imull	%esi, %edx
	movzwl	(%rcx,%rax,2), %eax
	imull	%esi, %eax
	movzwl	(%rcx,%rbp,2), %esi
	imull	%r8d, %esi
	addl	%eax, %esi
	movzwl	(%r13,%rbp,2), %eax
	imull	%r8d, %eax
	addl	%edx, %eax
	movq	360(%rsp), %r14         # 8-byte Reload
	imull	%r14d, %esi
	movq	112(%rsp), %r8          # 8-byte Reload
	imull	%r8d, %eax
	addl	%edi, %eax
	addl	%esi, %eax
	cltd
	idivl	%r9d
	movw	%ax, 136(%r10)
	movzwl	(%rcx,%r12,2), %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	imull	%esi, %eax
	movq	56(%rsp), %rbp          # 8-byte Reload
	movzwl	(%rcx,%rbp,2), %ecx
	imull	%r15d, %ecx
	addl	%eax, %ecx
	movzwl	(%r13,%r12,2), %edx
	imull	%esi, %edx
	movzwl	(%r13,%rbp,2), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	imull	%r15d, %eax
	addl	%edx, %eax
	imull	%r14d, %ecx
	imull	%r8d, %eax
	addl	%edi, %eax
	addl	%ecx, %eax
	cltd
	idivl	%r9d
	movw	%ax, 138(%r10)
	movzwl	104(%r10), %eax
	movw	%ax, 32(%rsi,%r11,8)
	movzwl	106(%r10), %eax
	movw	%ax, 34(%rsi,%r11,8)
	movzwl	136(%r10), %eax
	movw	%ax, 36(%rsi,%r11,8)
	movzwl	138(%r10), %eax
	movw	%ax, 38(%rsi,%r11,8)
	incq	%r11
	cmpq	$2, %r11
	jne	.LBB19_62
.LBB19_63:                              # %buildPredblockRegionYUV.exit
                                        #   in Loop: Header=BB19_56 Depth=2
	movq	80(%rsp), %r8           # 8-byte Reload
	movq	316920(%r8), %rax
	movq	272(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rdx
	movq	296(%rsp), %rsi         # 8-byte Reload
	movq	%rdx, (%rcx,%rsi,2)
	movq	264(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movq	8(%rdi), %rdx
	movq	%rdx, (%rcx,%rsi,2)
	movq	256(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movq	16(%rdi), %rdx
	movq	%rdx, (%rcx,%rsi,2)
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	24(%rdi), %rcx
	movq	%rcx, (%rax,%rsi,2)
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	movq	304(%rsp), %rbx         # 8-byte Reload
	je	.LBB19_65
# BB#64:                                # %.preheader192
                                        #   in Loop: Header=BB19_56 Depth=2
	movq	316928(%r8), %rax
	leal	(%rbx,%rbx), %ecx
	movslq	%ecx, %rcx
	movq	(%rax), %rdx
	movq	232(%rsp), %r9          # 8-byte Reload
	movq	(%rdx,%r9,8), %rsi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movzwl	32(%rbp), %edi
	movw	%di, (%rsi,%rcx,2)
	movzwl	34(%rbp), %edi
	movw	%di, 2(%rsi,%rcx,2)
	movq	224(%rsp), %rdi         # 8-byte Reload
	movq	(%rdx,%rdi,8), %rdx
	movzwl	36(%rbp), %esi
	movw	%si, (%rdx,%rcx,2)
	movzwl	38(%rbp), %esi
	movw	%si, 2(%rdx,%rcx,2)
	movq	8(%rax), %rax
	movq	(%rax,%r9,8), %rdx
	movzwl	40(%rbp), %esi
	movw	%si, (%rdx,%rcx,2)
	movzwl	42(%rbp), %esi
	movw	%si, 2(%rdx,%rcx,2)
	movq	(%rax,%rdi,8), %rax
	movzwl	44(%rbp), %edx
	movw	%dx, (%rax,%rcx,2)
	movzwl	46(%rbp), %edx
	movw	%dx, 2(%rax,%rcx,2)
.LBB19_65:                              # %.loopexit
                                        #   in Loop: Header=BB19_56 Depth=2
	incq	%rbx
	cmpq	240(%rsp), %rbx         # 8-byte Folded Reload
	movq	288(%rsp), %rcx         # 8-byte Reload
	movl	128(%rsp), %esi         # 4-byte Reload
	jl	.LBB19_56
.LBB19_66:                              # %._crit_edge
                                        #   in Loop: Header=BB19_54 Depth=1
	incq	%rcx
	cmpq	216(%rsp), %rcx         # 8-byte Folded Reload
	jl	.LBB19_54
.LBB19_67:                              # %._crit_edge205
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB19_68:                              # %CopyImgData.exit.thread
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	copy_to_conceal, .Lfunc_end19-copy_to_conceal
	.cfi_endproc

	.globl	sliding_window_poc_management
	.p2align	4, 0x90
	.type	sliding_window_poc_management,@function
sliding_window_poc_management:          # @sliding_window_poc_management
	.cfi_startproc
# BB#0:
	movl	dpb+28(%rip), %eax
	cmpl	dpb+24(%rip), %eax
	jne	.LBB20_14
# BB#1:
	cmpl	$1, %eax
	je	.LBB20_14
# BB#2:                                 # %.lr.ph
	decl	%eax
	cmpl	$1, %eax
	movl	$1, %edx
	cmovaq	%rax, %rdx
	cmpq	$8, %rdx
	jae	.LBB20_4
# BB#3:
	xorl	%ecx, %ecx
	jmp	.LBB20_13
.LBB20_4:                               # %min.iters.checked
	movl	$4294967288, %ecx       # imm = 0xFFFFFFF8
	andq	%rdx, %rcx
	je	.LBB20_5
# BB#6:                                 # %vector.body.preheader
	leaq	-8(%rcx), %r8
	movl	%r8d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB20_7
# BB#8:                                 # %vector.body.prol.preheader
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB20_9:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	pocs_in_dpb+4(,%rsi,4), %xmm0
	movups	pocs_in_dpb+20(,%rsi,4), %xmm1
	movaps	%xmm0, pocs_in_dpb(,%rsi,4)
	movaps	%xmm1, pocs_in_dpb+16(,%rsi,4)
	addq	$8, %rsi
	incq	%rdi
	jne	.LBB20_9
	jmp	.LBB20_10
.LBB20_5:
	xorl	%ecx, %ecx
	jmp	.LBB20_13
.LBB20_7:
	xorl	%esi, %esi
.LBB20_10:                              # %vector.body.prol.loopexit
	cmpq	$24, %r8
	jb	.LBB20_12
	.p2align	4, 0x90
.LBB20_11:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	leaq	(,%rsi,4), %rdi
	orq	$4, %rdi
	movups	pocs_in_dpb(%rdi), %xmm0
	movups	pocs_in_dpb+16(%rdi), %xmm1
	movaps	%xmm0, pocs_in_dpb(,%rsi,4)
	movaps	%xmm1, pocs_in_dpb+16(,%rsi,4)
	leaq	8(%rsi), %rdi
	orq	$1, %rdi
	movups	pocs_in_dpb(,%rdi,4), %xmm0
	movups	pocs_in_dpb+16(,%rdi,4), %xmm1
	movaps	%xmm0, pocs_in_dpb+32(,%rsi,4)
	movaps	%xmm1, pocs_in_dpb+48(,%rsi,4)
	leaq	16(%rsi), %rdi
	orq	$1, %rdi
	movups	pocs_in_dpb(,%rdi,4), %xmm0
	movups	pocs_in_dpb+16(,%rdi,4), %xmm1
	movaps	%xmm0, pocs_in_dpb+64(,%rsi,4)
	movaps	%xmm1, pocs_in_dpb+80(,%rsi,4)
	leaq	24(%rsi), %rdi
	orq	$1, %rdi
	movups	pocs_in_dpb(,%rdi,4), %xmm0
	movups	pocs_in_dpb+16(,%rdi,4), %xmm1
	movaps	%xmm0, pocs_in_dpb+96(,%rsi,4)
	movaps	%xmm1, pocs_in_dpb+112(,%rsi,4)
	addq	$32, %rsi
	cmpq	%rcx, %rsi
	jne	.LBB20_11
.LBB20_12:                              # %middle.block
	cmpq	%rcx, %rdx
	je	.LBB20_14
	.p2align	4, 0x90
.LBB20_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	pocs_in_dpb+4(,%rcx,4), %edx
	movl	%edx, pocs_in_dpb(,%rcx,4)
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB20_13
.LBB20_14:                              # %.loopexit
	retq
.Lfunc_end20:
	.size	sliding_window_poc_management, .Lfunc_end20-sliding_window_poc_management
	.cfi_endproc

	.globl	write_lost_non_ref_pic
	.p2align	4, 0x90
	.type	write_lost_non_ref_pic,@function
write_lost_non_ref_pic:                 # @write_lost_non_ref_pic
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	jle	.LBB21_5
# BB#1:
	subl	dpb+40(%rip), %edi
	movq	img(%rip), %rax
	cmpl	6064(%rax), %edi
	jle	.LBB21_5
# BB#2:
	subq	$72, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 80
	movq	concealment_head(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, 48(%rsp)
	movl	$0, 36(%rsp)
	movq	$3, (%rsp)
	movq	%rsp, %rdi
	callq	write_stored_frame
	movq	concealment_head(%rip), %rdi
	movq	16(%rdi), %rax
	movq	%rax, concealment_head(%rip)
	cmpq	%rdi, concealment_end(%rip)
	jne	.LBB21_4
# BB#3:
	movq	%rax, concealment_end(%rip)
.LBB21_4:                               # %delete_node.exit
	callq	free
	addq	$72, %rsp
.LBB21_5:
	retq
.Lfunc_end21:
	.size	write_lost_non_ref_pic, .Lfunc_end21-write_lost_non_ref_pic
	.cfi_endproc

	.globl	write_lost_ref_after_idr
	.p2align	4, 0x90
	.type	write_lost_ref_after_idr,@function
write_lost_ref_after_idr:               # @write_lost_ref_after_idr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -24
.Lcfi119:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	movq	last_out_fs(%rip), %rax
	movq	48(%rax), %rsi
	testq	%rsi, %rsi
	jne	.LBB22_2
# BB#1:
	movq	img(%rip), %rax
	movl	48(%rax), %esi
	movl	52(%rax), %edx
	movl	56(%rax), %ecx
	movl	64(%rax), %r8d
	xorl	%edi, %edi
	callq	alloc_storable_picture
	movq	%rax, %rsi
	movq	last_out_fs(%rip), %rax
	movq	%rsi, 48(%rax)
	movl	$3, (%rax)
.LBB22_2:
	movq	img(%rip), %rdx
	movl	$1, %ebp
	cmpl	$2, 6068(%rdx)
	jne	.LBB22_4
# BB#3:
	movl	$1, 6068(%rdx)
	movl	$2, %ebp
.LBB22_4:
	movq	dpb(%rip), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	48(%rax), %rdi
	callq	copy_to_conceal
	movq	img(%rip), %rax
	movl	%ebp, 6068(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end22:
	.size	write_lost_ref_after_idr, .Lfunc_end22-write_lost_ref_after_idr
	.cfi_endproc

	.p2align	4, 0x90
	.type	buildPredRegionYUV,@function
buildPredRegionYUV:                     # @buildPredRegionYUV
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi120:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi121:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi123:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi124:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi125:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi126:
	.cfi_def_cfa_offset 320
.Lcfi127:
	.cfi_offset %rbx, -56
.Lcfi128:
	.cfi_offset %r12, -48
.Lcfi129:
	.cfi_offset %r13, -40
.Lcfi130:
	.cfi_offset %r14, -32
.Lcfi131:
	.cfi_offset %r15, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	dec_picture(%rip), %rax
	movslq	317044(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	8(%rsi), %r13d
	xorl	%r15d, %r15d
	testl	%r13d, %r13d
	cmovsl	%r15d, %r13d
	movl	%edx, %ebp
	sarl	$31, %ebp
	shrl	$28, %ebp
	addl	%edx, %ebp
	sarl	$4, %ebp
	movl	%ebp, 72(%rdi)
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	movl	%edx, 68(%rdi)
	leal	(,%rdx,4), %eax
	movl	%eax, 76(%rdi)
	imull	5936(%rdi), %edx
	movl	%edx, 88(%rdi)
	leal	(,%rbp,4), %ecx
	movl	%ecx, 92(%rdi)
	imull	5932(%rdi), %ebp
	movl	%ebp, 96(%rdi)
	leaq	206(%rdi), %rcx
	leaq	96(%rsp), %r12
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	jmp	.LBB23_1
	.p2align	4, 0x90
.LBB23_4:                               # %._crit_edge
                                        #   in Loop: Header=BB23_1 Depth=1
	movl	76(%rdi), %eax
	movq	48(%rsp), %rcx          # 8-byte Reload
	subq	$-128, %rcx
.LBB23_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_2 Depth 2
	leal	(%r15,%rax), %ebx
	shll	$4, %ebx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rcx, %rbp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_2:                               # %.preheader247
                                        #   Parent Loop BB23_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	92(%rdi), %edx
	addl	%r14d, %edx
	shll	$4, %edx
	addl	(%rsi), %edx
	movl	4(%rsi), %ecx
	addl	%ebx, %ecx
	movq	listX(%rip), %rsi
	movl	%r13d, %edi
	movq	40(%rsp), %r8           # 8-byte Reload
	movq	%r12, %r9
	callq	get_block
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movzwl	96(%rsp), %eax
	movw	%ax, -102(%rbp)
	movzwl	112(%rsp), %eax
	movw	%ax, -70(%rbp)
	movzwl	128(%rsp), %eax
	movw	%ax, -38(%rbp)
	movzwl	144(%rsp), %eax
	movw	%ax, -6(%rbp)
	movzwl	100(%rsp), %eax
	movw	%ax, -100(%rbp)
	movzwl	116(%rsp), %eax
	movw	%ax, -68(%rbp)
	movzwl	132(%rsp), %eax
	movw	%ax, -36(%rbp)
	movzwl	148(%rsp), %eax
	movw	%ax, -4(%rbp)
	movzwl	104(%rsp), %eax
	movw	%ax, -98(%rbp)
	movzwl	120(%rsp), %eax
	movw	%ax, -66(%rbp)
	movzwl	136(%rsp), %eax
	movw	%ax, -34(%rbp)
	movzwl	152(%rsp), %eax
	movw	%ax, -2(%rbp)
	movzwl	108(%rsp), %eax
	movw	%ax, -96(%rbp)
	movzwl	124(%rsp), %eax
	movw	%ax, -64(%rbp)
	movzwl	140(%rsp), %eax
	movw	%ax, -32(%rbp)
	movzwl	156(%rsp), %eax
	movw	%ax, (%rbp)
	incq	%r14
	addq	$8, %rbp
	cmpq	$4, %r14
	jne	.LBB23_2
# BB#3:                                 #   in Loop: Header=BB23_1 Depth=1
	incq	%r15
	cmpq	$4, %r15
	jne	.LBB23_4
# BB#5:                                 # %.preheader245.preheader
	decq	24(%rsp)                # 8-byte Folded Spill
	xorl	%eax, %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	.p2align	4, 0x90
.LBB23_6:                               # %.preheader245
                                        # =>This Inner Loop Header: Depth=1
	movzwl	104(%rdi,%rax), %ecx
	movw	%cx, (%rsi,%rax)
	movzwl	106(%rdi,%rax), %ecx
	movw	%cx, 2(%rsi,%rax)
	movzwl	108(%rdi,%rax), %ecx
	movw	%cx, 4(%rsi,%rax)
	movzwl	110(%rdi,%rax), %ecx
	movw	%cx, 6(%rsi,%rax)
	movzwl	112(%rdi,%rax), %ecx
	movw	%cx, 8(%rsi,%rax)
	movzwl	114(%rdi,%rax), %ecx
	movw	%cx, 10(%rsi,%rax)
	movzwl	116(%rdi,%rax), %ecx
	movw	%cx, 12(%rsi,%rax)
	movzwl	118(%rdi,%rax), %ecx
	movw	%cx, 14(%rsi,%rax)
	movzwl	120(%rdi,%rax), %ecx
	movw	%cx, 16(%rsi,%rax)
	movzwl	122(%rdi,%rax), %ecx
	movw	%cx, 18(%rsi,%rax)
	movzwl	124(%rdi,%rax), %ecx
	movw	%cx, 20(%rsi,%rax)
	movzwl	126(%rdi,%rax), %ecx
	movw	%cx, 22(%rsi,%rax)
	movzwl	128(%rdi,%rax), %ecx
	movw	%cx, 24(%rsi,%rax)
	movzwl	130(%rdi,%rax), %ecx
	movw	%cx, 26(%rsi,%rax)
	movzwl	132(%rdi,%rax), %ecx
	movw	%cx, 28(%rsi,%rax)
	movzwl	134(%rdi,%rax), %ecx
	movw	%cx, 30(%rsi,%rax)
	addq	$32, %rax
	cmpq	$512, %rax              # imm = 0x200
	jne	.LBB23_6
# BB#7:
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB23_19
# BB#8:
	addq	$512, %rsi              # imm = 0x200
	xorl	%r9d, %r9d
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	5932(%rdi)
	movl	%eax, %r8d
	leal	-1(%r8), %r10d
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	5936(%rdi)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %edx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movl	%eax, %ebx
	imull	%r8d, %ebx
	movl	%ebx, %r14d
	sarl	%r14d
	movl	%r13d, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	104(%rdi), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	shlq	$5, 24(%rsp)            # 8-byte Folded Spill
	movl	%ebx, 72(%rsp)          # 4-byte Spill
	movl	%r10d, 68(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB23_9:                               # %.preheader244
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_11 Depth 2
                                        #       Child Loop BB23_12 Depth 3
                                        #         Child Loop BB23_13 Depth 4
                                        #           Child Loop BB23_14 Depth 5
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	cmpl	$2, 5924(%rdi)
	jl	.LBB23_18
# BB#10:                                # %.preheader242.lr.ph
                                        #   in Loop: Header=BB23_9 Depth=1
	movq	80(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	dec_picture(%rip), %rax
	movl	316872(%rax), %r15d
	movl	316876(%rax), %ebp
	movq	listX(%rip), %rax
	movq	168(%rsp), %rsi         # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	316928(%rax), %rax
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	decl	%r15d
	decl	%ebp
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	movslq	4(%rdx), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB23_11:                              # %.preheader242
                                        #   Parent Loop BB23_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB23_12 Depth 3
                                        #         Child Loop BB23_13 Depth 4
                                        #           Child Loop BB23_14 Depth 5
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB23_12:                              #   Parent Loop BB23_9 Depth=1
                                        #     Parent Loop BB23_11 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB23_13 Depth 4
                                        #           Child Loop BB23_14 Depth 5
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	(%rax,%rdx,4), %rax
	movzbl	subblk_offset_y(%rsi,%rax), %edx
	movslq	88(%rdi), %rbp
	addq	%rdx, %rbp
	movq	%rbp, 216(%rsp)         # 8-byte Spill
	movq	%rsi, 184(%rsp)         # 8-byte Spill
	movzbl	subblk_offset_x(%rsi,%rax), %eax
	movl	96(%rdi), %esi
	leal	(%rsi,%rax), %ecx
	leal	1(%rsi,%rax), %r13d
	imull	%r8d, %r13d
	decl	%r13d
	imull	%r8d, %ecx
	shlq	$4, %rdx
	addq	%rax, %rdx
	movq	176(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,2), %rsi
	xorl	%edx, %edx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB23_13:                              #   Parent Loop BB23_9 Depth=1
                                        #     Parent Loop BB23_11 Depth=2
                                        #       Parent Loop BB23_12 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB23_14 Depth 5
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	leal	(%rax,%rdx), %edi
	movq	192(%rsp), %r11         # 8-byte Reload
	imull	%r11d, %edi
	addl	200(%rsp), %edi         # 4-byte Folded Reload
	movl	%edi, %eax
	cltd
	idivl	%r11d
	movl	%eax, %ebp
	testl	%ebp, %ebp
	cmovsl	%r9d, %ebp
	movl	60(%rsp), %r12d         # 4-byte Reload
	cmpl	%r12d, %ebp
	cmovgl	%r12d, %ebp
	movq	88(%rsp), %rax          # 8-byte Reload
	leal	(%rdi,%rax), %eax
	cltd
	idivl	%r11d
	testl	%eax, %eax
	cmovsl	%r9d, %eax
	cmpl	%r12d, %eax
	cmovgl	%r12d, %eax
	andl	88(%rsp), %edi          # 4-byte Folded Reload
	movl	%r11d, %edx
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	subl	%edi, %edx
	movl	%edx, 76(%rsp)          # 4-byte Spill
	movslq	%ebp, %rdx
	movq	208(%rsp), %rbp         # 8-byte Reload
	movq	(%rbp,%rdx,8), %rdx
	movq	%rdx, 256(%rsp)         # 8-byte Spill
	cltq
	movq	(%rbp,%rax,8), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movl	$4, %edi
	movq	%rsi, 232(%rsp)         # 8-byte Spill
	movq	%rsi, %r12
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, %esi
	.p2align	4, 0x90
.LBB23_14:                              #   Parent Loop BB23_9 Depth=1
                                        #     Parent Loop BB23_11 Depth=2
                                        #       Parent Loop BB23_12 Depth=3
                                        #         Parent Loop BB23_13 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	leal	(%rcx,%rsi), %ebp
	movl	%ebp, %eax
	cltd
	idivl	%r8d
	movl	%eax, %ebx
	testl	%ebx, %ebx
	cmovsl	%r9d, %ebx
	cmpl	%r15d, %ebx
	cmovgl	%r15d, %ebx
	leal	(%r13,%rsi), %eax
	cltd
	idivl	%r8d
	testl	%eax, %eax
	cmovsl	%r9d, %eax
	cmpl	%r15d, %eax
	cmovgl	%r15d, %eax
	andl	%r10d, %ebp
	movl	%r8d, %edx
	subl	%ebp, %edx
	movslq	%ebx, %rbx
	movq	256(%rsp), %r11         # 8-byte Reload
	movzwl	(%r11,%rbx,2), %r10d
	imull	%edx, %r10d
	movq	%r13, %rcx
	movl	%r15d, %r13d
	movq	%r8, %r15
	movl	%r14d, %r8d
	movslq	%eax, %r14
	movq	248(%rsp), %r9          # 8-byte Reload
	movzwl	(%r9,%rbx,2), %ebx
	imull	%edx, %ebx
	movzwl	(%r11,%r14,2), %eax
	imull	%ebp, %eax
	movzwl	(%r9,%r14,2), %edx
	movl	%r8d, %r14d
	movq	%r15, %r8
	movl	%r13d, %r15d
	movq	%rcx, %r13
	movq	240(%rsp), %rcx         # 8-byte Reload
	imull	%ebp, %edx
	addl	%ebx, %edx
	movl	72(%rsp), %ebx          # 4-byte Reload
	addl	%r10d, %eax
	movl	68(%rsp), %r10d         # 4-byte Reload
	xorl	%r9d, %r9d
	imull	48(%rsp), %edx          # 4-byte Folded Reload
	imull	76(%rsp), %eax          # 4-byte Folded Reload
	addl	%r14d, %eax
	addl	%edx, %eax
	cltd
	idivl	%ebx
	movw	%ax, (%r12)
	addl	%r8d, %esi
	addq	$2, %r12
	decq	%rdi
	jne	.LBB23_14
# BB#15:                                #   in Loop: Header=BB23_13 Depth=4
	movq	224(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	movq	232(%rsp), %rsi         # 8-byte Reload
	addq	$32, %rsi
	cmpq	$4, %rdx
	jne	.LBB23_13
# BB#16:                                #   in Loop: Header=BB23_12 Depth=3
	movq	184(%rsp), %rsi         # 8-byte Reload
	incq	%rsi
	cmpq	$4, %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	jne	.LBB23_12
# BB#17:                                #   in Loop: Header=BB23_11 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rsi
	incq	%rsi
	movl	5924(%rdi), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%eax, %edx
	sarl	%edx
	movslq	%edx, %rax
	movq	%rsi, %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	cmpq	%rax, %rsi
	jl	.LBB23_11
.LBB23_18:                              # %.preheader.preheader
                                        #   in Loop: Header=BB23_9 Depth=1
	movzwl	104(%rdi), %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movw	%ax, (%rsi)
	movzwl	106(%rdi), %eax
	movw	%ax, 2(%rsi)
	movzwl	108(%rdi), %eax
	movw	%ax, 4(%rsi)
	movzwl	110(%rdi), %eax
	movw	%ax, 6(%rsi)
	movzwl	112(%rdi), %eax
	movw	%ax, 8(%rsi)
	movzwl	114(%rdi), %eax
	movw	%ax, 10(%rsi)
	movzwl	116(%rdi), %eax
	movw	%ax, 12(%rsi)
	movzwl	118(%rdi), %eax
	movw	%ax, 14(%rsi)
	movzwl	136(%rdi), %eax
	movw	%ax, 16(%rsi)
	movzwl	138(%rdi), %eax
	movw	%ax, 18(%rsi)
	movzwl	140(%rdi), %eax
	movw	%ax, 20(%rsi)
	movzwl	142(%rdi), %eax
	movw	%ax, 22(%rsi)
	movzwl	144(%rdi), %eax
	movw	%ax, 24(%rsi)
	movzwl	146(%rdi), %eax
	movw	%ax, 26(%rsi)
	movzwl	148(%rdi), %eax
	movw	%ax, 28(%rsi)
	movzwl	150(%rdi), %eax
	movw	%ax, 30(%rsi)
	movzwl	168(%rdi), %eax
	movw	%ax, 32(%rsi)
	movzwl	170(%rdi), %eax
	movw	%ax, 34(%rsi)
	movzwl	172(%rdi), %eax
	movw	%ax, 36(%rsi)
	movzwl	174(%rdi), %eax
	movw	%ax, 38(%rsi)
	movzwl	176(%rdi), %eax
	movw	%ax, 40(%rsi)
	movzwl	178(%rdi), %eax
	movw	%ax, 42(%rsi)
	movzwl	180(%rdi), %eax
	movw	%ax, 44(%rsi)
	movzwl	182(%rdi), %eax
	movw	%ax, 46(%rsi)
	movzwl	200(%rdi), %eax
	movw	%ax, 48(%rsi)
	movzwl	202(%rdi), %eax
	movw	%ax, 50(%rsi)
	movzwl	204(%rdi), %eax
	movw	%ax, 52(%rsi)
	movzwl	206(%rdi), %eax
	movw	%ax, 54(%rsi)
	movzwl	208(%rdi), %eax
	movw	%ax, 56(%rsi)
	movzwl	210(%rdi), %eax
	movw	%ax, 58(%rsi)
	movzwl	212(%rdi), %eax
	movw	%ax, 60(%rsi)
	movzwl	214(%rdi), %eax
	movw	%ax, 62(%rsi)
	movzwl	232(%rdi), %eax
	movw	%ax, 64(%rsi)
	movzwl	234(%rdi), %eax
	movw	%ax, 66(%rsi)
	movzwl	236(%rdi), %eax
	movw	%ax, 68(%rsi)
	movzwl	238(%rdi), %eax
	movw	%ax, 70(%rsi)
	movzwl	240(%rdi), %eax
	movw	%ax, 72(%rsi)
	movzwl	242(%rdi), %eax
	movw	%ax, 74(%rsi)
	movzwl	244(%rdi), %eax
	movw	%ax, 76(%rsi)
	movzwl	246(%rdi), %eax
	movw	%ax, 78(%rsi)
	movzwl	264(%rdi), %eax
	movw	%ax, 80(%rsi)
	movzwl	266(%rdi), %eax
	movw	%ax, 82(%rsi)
	movzwl	268(%rdi), %eax
	movw	%ax, 84(%rsi)
	movzwl	270(%rdi), %eax
	movw	%ax, 86(%rsi)
	movzwl	272(%rdi), %eax
	movw	%ax, 88(%rsi)
	movzwl	274(%rdi), %eax
	movw	%ax, 90(%rsi)
	movzwl	276(%rdi), %eax
	movw	%ax, 92(%rsi)
	movzwl	278(%rdi), %eax
	movw	%ax, 94(%rsi)
	movzwl	296(%rdi), %eax
	movw	%ax, 96(%rsi)
	movzwl	298(%rdi), %eax
	movw	%ax, 98(%rsi)
	movzwl	300(%rdi), %eax
	movw	%ax, 100(%rsi)
	movzwl	302(%rdi), %eax
	movw	%ax, 102(%rsi)
	movzwl	304(%rdi), %eax
	movw	%ax, 104(%rsi)
	movzwl	306(%rdi), %eax
	movw	%ax, 106(%rsi)
	movzwl	308(%rdi), %eax
	movw	%ax, 108(%rsi)
	movzwl	310(%rdi), %eax
	movw	%ax, 110(%rsi)
	movzwl	328(%rdi), %eax
	movw	%ax, 112(%rsi)
	movzwl	330(%rdi), %eax
	movw	%ax, 114(%rsi)
	movzwl	332(%rdi), %eax
	movw	%ax, 116(%rsi)
	movzwl	334(%rdi), %eax
	movw	%ax, 118(%rsi)
	movzwl	336(%rdi), %eax
	movw	%ax, 120(%rsi)
	movzwl	338(%rdi), %eax
	movw	%ax, 122(%rsi)
	movzwl	340(%rdi), %eax
	movw	%ax, 124(%rsi)
	movzwl	342(%rdi), %eax
	movw	%ax, 126(%rsi)
	subq	$-128, %rsi
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movq	%rdx, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpq	$2, %rdx
	jne	.LBB23_9
.LBB23_19:                              # %.loopexit
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	buildPredRegionYUV, .Lfunc_end23-buildPredRegionYUV
	.cfi_endproc

	.p2align	4, 0x90
	.type	edgeDistortion,@function
edgeDistortion:                         # @edgeDistortion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi136:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi137:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 56
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movq	%rdx, %r9
	movl	%r8d, %ebp
	sarl	$3, %ebp
	movl	%esi, %eax
	cltd
	idivl	%ebp
	imull	%r8d, %eax
	shll	$3, %eax
	cltq
	leaq	(%rcx,%rax,2), %rax
	shll	$3, %edx
	movslq	%edx, %rcx
	leaq	(%rax,%rcx,2), %r11
	movslq	%r8d, %r12
	movq	%r12, %r8
	negq	%r8
	movl	%r12d, %eax
	shll	$4, %eax
	movslq	%eax, %r10
	leaq	(,%r12,4), %r13
	movl	$3, %r15d
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB24_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_2 Depth 2
                                        #       Child Loop BB24_11 Depth 3
                                        #       Child Loop BB24_9 Depth 3
	movl	$4, %ebx
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB24_2:                               #   Parent Loop BB24_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_11 Depth 3
                                        #       Child Loop BB24_9 Depth 3
	cmpl	%r15d, (%rdi,%rbx,4)
	jl	.LBB24_13
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=2
	leal	-4(%rbx), %ecx
	cmpl	$3, %ecx
	ja	.LBB24_12
# BB#4:                                 #   in Loop: Header=BB24_2 Depth=2
	jmpq	*.LJTI24_0(,%rcx,8)
.LBB24_7:                               # %vector.body
                                        #   in Loop: Header=BB24_2 Depth=2
	movd	%eax, %xmm2
	movq	(%r9), %xmm1            # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	(%r11,%r8,2), %xmm3     # xmm3 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	psubd	%xmm3, %xmm1
	movdqa	%xmm1, %xmm3
	psrad	$31, %xmm3
	paddd	%xmm3, %xmm1
	pxor	%xmm3, %xmm1
	paddd	%xmm2, %xmm1
	movq	8(%r9), %xmm3           # xmm3 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	movq	8(%r11,%r8,2), %xmm2    # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm3
	movdqa	%xmm3, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm3
	pxor	%xmm2, %xmm3
	movq	16(%r9), %xmm2          # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movq	16(%r11,%r8,2), %xmm4   # xmm4 = mem[0],zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	psubd	%xmm4, %xmm2
	movdqa	%xmm2, %xmm4
	psrad	$31, %xmm4
	paddd	%xmm4, %xmm2
	pxor	%xmm4, %xmm2
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	movq	24(%r9), %xmm1          # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	24(%r11,%r8,2), %xmm3   # xmm3 = mem[0],zero
	jmp	.LBB24_6
.LBB24_8:                               # %.preheader15.preheader
                                        #   in Loop: Header=BB24_2 Depth=2
	movq	%r11, %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_9:                               # %.preheader15
                                        #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	(%r9,%rcx), %edx
	movzwl	-2(%rbp), %esi
	subl	%esi, %edx
	movl	%edx, %esi
	negl	%esi
	cmovll	%edx, %esi
	addl	%eax, %esi
	movzwl	32(%r9,%rcx), %edx
	movzwl	-2(%rbp,%r12,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	addl	%esi, %eax
	addq	$64, %rcx
	addq	%r13, %rbp
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB24_9
	jmp	.LBB24_12
.LBB24_5:                               # %vector.body54
                                        #   in Loop: Header=BB24_2 Depth=2
	movd	%eax, %xmm2
	movq	480(%r9), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	(%r11,%r10,2), %xmm3    # xmm3 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	psubd	%xmm3, %xmm1
	movdqa	%xmm1, %xmm3
	psrad	$31, %xmm3
	paddd	%xmm3, %xmm1
	pxor	%xmm3, %xmm1
	paddd	%xmm2, %xmm1
	movq	488(%r9), %xmm3         # xmm3 = mem[0],zero
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	movq	8(%r11,%r10,2), %xmm2   # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	psubd	%xmm2, %xmm3
	movdqa	%xmm3, %xmm2
	psrad	$31, %xmm2
	paddd	%xmm2, %xmm3
	pxor	%xmm2, %xmm3
	movq	496(%r9), %xmm2         # xmm2 = mem[0],zero
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	movq	16(%r11,%r10,2), %xmm4  # xmm4 = mem[0],zero
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	psubd	%xmm4, %xmm2
	movdqa	%xmm2, %xmm4
	psrad	$31, %xmm4
	paddd	%xmm4, %xmm2
	pxor	%xmm4, %xmm2
	paddd	%xmm3, %xmm2
	paddd	%xmm1, %xmm2
	movq	504(%r9), %xmm1         # xmm1 = mem[0],zero
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	movq	24(%r11,%r10,2), %xmm3  # xmm3 = mem[0],zero
.LBB24_6:                               # %.loopexit
                                        #   in Loop: Header=BB24_2 Depth=2
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	psubd	%xmm3, %xmm1
	movdqa	%xmm1, %xmm3
	psrad	$31, %xmm3
	paddd	%xmm3, %xmm1
	pxor	%xmm3, %xmm1
	paddd	%xmm2, %xmm1
	pshufd	$78, %xmm1, %xmm2       # xmm2 = xmm1[2,3,0,1]
	paddd	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm1      # xmm1 = xmm2[1,1,2,3]
	paddd	%xmm2, %xmm1
	movd	%xmm1, %eax
	jmp	.LBB24_12
.LBB24_10:                              # %.preheader19.preheader
                                        #   in Loop: Header=BB24_2 Depth=2
	movq	%r11, %rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_11:                              # %.preheader19
                                        #   Parent Loop BB24_1 Depth=1
                                        #     Parent Loop BB24_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	30(%r9,%rcx), %edx
	movzwl	32(%rbp), %esi
	subl	%esi, %edx
	movl	%edx, %esi
	negl	%esi
	cmovll	%edx, %esi
	addl	%eax, %esi
	movzwl	62(%r9,%rcx), %edx
	movzwl	32(%rbp,%r12,2), %eax
	subl	%eax, %edx
	movl	%edx, %eax
	negl	%eax
	cmovll	%edx, %eax
	addl	%esi, %eax
	addq	$64, %rcx
	addq	%r13, %rbp
	cmpq	$512, %rcx              # imm = 0x200
	jne	.LBB24_11
	.p2align	4, 0x90
.LBB24_12:                              # %.loopexit
                                        #   in Loop: Header=BB24_2 Depth=2
	incl	%r14d
.LBB24_13:                              #   in Loop: Header=BB24_2 Depth=2
	incq	%rbx
	cmpq	$8, %rbx
	jne	.LBB24_2
# BB#14:                                #   in Loop: Header=BB24_1 Depth=1
	decl	%r15d
	cmpl	$2, %r15d
	jl	.LBB24_16
# BB#15:                                #   in Loop: Header=BB24_1 Depth=1
	testl	%r14d, %r14d
	je	.LBB24_1
.LBB24_16:
	testl	%r14d, %r14d
	je	.LBB24_17
# BB#18:
	cltd
	idivl	%r14d
	jmp	.LBB24_19
.LBB24_17:
	xorl	%eax, %eax
.LBB24_19:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	edgeDistortion, .Lfunc_end24-edgeDistortion
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI24_0:
	.quad	.LBB24_7
	.quad	.LBB24_8
	.quad	.LBB24_5
	.quad	.LBB24_10

	.text
	.p2align	4, 0x90
	.type	copyPredMB,@function
copyPredMB:                             # @copyPredMB
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
.Lcfi151:
	.cfi_offset %rbx, -56
.Lcfi152:
	.cfi_offset %r12, -48
.Lcfi153:
	.cfi_offset %r13, -40
.Lcfi154:
	.cfi_offset %r14, -32
.Lcfi155:
	.cfi_offset %r15, -24
.Lcfi156:
	.cfi_offset %rbp, -16
	movl	%edx, %r8d
	movq	dec_picture(%rip), %rbp
	movslq	317044(%rbp), %r15
	movl	uv_div.0(,%r15,4), %eax
	movl	%eax, -120(%rsp)        # 4-byte Spill
	movl	uv_div.1(,%r15,4), %ecx
	sarl	$3, %r8d
	movl	%edi, %eax
	cltd
	idivl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	leal	(,%rdx,8), %edi
	leal	(,%rax,8), %r11d
	leal	15(,%rdx,8), %edx
	leal	15(,%rax,8), %r14d
	movq	316920(%rbp), %r8
	movl	%edi, -124(%rsp)        # 4-byte Spill
	movslq	%edi, %r12
	movl	%edx, -112(%rsp)        # 4-byte Spill
	movslq	%edx, %rdi
	movslq	%r11d, %rbx
	movslq	%r14d, %r13
	decq	%r12
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB25_1:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_2 Depth 2
	movq	(%r8,%rbx,8), %rbp
	movl	%eax, %r9d
	movq	%r12, %rdx
	.p2align	4, 0x90
.LBB25_2:                               #   Parent Loop BB25_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%r9d, %r9
	movzwl	(%rsi,%r9,2), %r10d
	movw	%r10w, 2(%rbp,%rdx,2)
	incq	%rdx
	incl	%r9d
	cmpq	%rdi, %rdx
	jl	.LBB25_2
# BB#3:                                 #   in Loop: Header=BB25_1 Depth=1
	addl	$16, %eax
	cmpq	%r13, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB25_1
# BB#4:
	testl	%r15d, %r15d
	je	.LBB25_19
# BB#5:
	sarl	%cl, %r11d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %r14d
	cmpl	%r14d, %r11d
	jg	.LBB25_19
# BB#6:                                 # %.lr.ph4
	movl	-124(%rsp), %eax        # 4-byte Reload
	movl	-120(%rsp), %ecx        # 4-byte Reload
	sarl	%cl, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	movl	-112(%rsp), %edi        # 4-byte Reload
	sarl	%cl, %edi
	movl	%edi, %edx
	cmpl	%edi, %eax
	jg	.LBB25_19
# BB#7:                                 # %.lr.ph4.split.preheader
	movq	dec_picture(%rip), %rdi
	movl	-124(%rsp), %ecx        # 4-byte Reload
	sarl	%ecx
	movq	img(%rip), %rbp
	movslq	%eax, %r8
	movslq	%ecx, %r9
	movslq	%edx, %rcx
	movslq	%r11d, %rdx
	movslq	%r14d, %r10
	movl	5932(%rbp), %eax
	movl	%eax, -124(%rsp)        # 4-byte Spill
	movq	316928(%rdi), %rdi
	movq	(%rdi), %rax
	movq	%rax, -112(%rsp)        # 8-byte Spill
	movq	8(%rdi), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	cmpq	%rcx, %r8
	movq	%rcx, %rax
	cmovgeq	%r8, %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leaq	1(%rax), %r11
	movl	$256, %edi              # imm = 0x100
	subq	%r9, %rdi
	movl	$321, %ebp              # imm = 0x141
	subq	%r9, %rbp
	leaq	328(%r8), %rbx
	subq	%r9, %rbx
	movl	$320, %eax              # imm = 0x140
	subq	%r9, %rax
	movq	%r11, -64(%rsp)         # 8-byte Spill
	subq	%r8, %r11
	leaq	(%rsi,%rdi,2), %rdi
	movq	%rdi, -72(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rbp,2), %rdi
	movq	%rdi, -80(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rbx,2), %rdi
	movq	%rdi, -88(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rax,2), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movq	%r11, -120(%rsp)        # 8-byte Spill
	movq	%r11, %rbp
	andq	$-16, %rbp
	leaq	(%r8,%rbp), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	leaq	8(%r8), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	movq	%r8, -40(%rsp)          # 8-byte Spill
	movq	%r10, -48(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB25_8:                               # %.lr.ph4.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_14 Depth 2
                                        #     Child Loop BB25_17 Depth 2
	cmpq	$16, -120(%rsp)         # 8-byte Folded Reload
	movslq	%r15d, %rdi
	movq	-112(%rsp), %rax        # 8-byte Reload
	movq	(%rax,%rdx,8), %r13
	movq	-8(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %r11
	movq	%r8, %rax
	jb	.LBB25_16
# BB#9:                                 # %min.iters.checked
                                        #   in Loop: Header=BB25_8 Depth=1
	testq	%rbp, %rbp
	movq	%r8, %rax
	je	.LBB25_16
# BB#10:                                # %vector.memcheck
                                        #   in Loop: Header=BB25_8 Depth=1
	movl	-124(%rsp), %eax        # 4-byte Reload
	imull	%r12d, %eax
	cltq
	leaq	(%r8,%rax), %rsi
	movq	%rsi, -32(%rsp)         # 8-byte Spill
	addq	-56(%rsp), %rax         # 8-byte Folded Reload
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	leaq	(%r13,%r8,2), %rdi
	movq	-64(%rsp), %r14         # 8-byte Reload
	leaq	(%r13,%r14,2), %rsi
	leaq	(%r11,%r8,2), %r9
	leaq	(%r11,%r14,2), %r10
	cmpq	%r10, %rdi
	sbbb	%r14b, %r14b
	cmpq	%rsi, %r9
	sbbb	%r8b, %r8b
	andb	%r14b, %r8b
	movq	-80(%rsp), %rbx         # 8-byte Reload
	leaq	(%rbx,%rax,2), %rax
	cmpq	%rax, %rdi
	movq	-72(%rsp), %rdi         # 8-byte Reload
	movq	-32(%rsp), %rbx         # 8-byte Reload
	leaq	(%rdi,%rbx,2), %rdi
	sbbb	%bl, %bl
	cmpq	%rsi, %rdi
	sbbb	%r14b, %r14b
	cmpq	%rax, %r9
	sbbb	%sil, %sil
	cmpq	%r10, %rdi
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	-48(%rsp), %r10         # 8-byte Reload
	sbbb	%r9b, %r9b
	testb	$1, %r8b
	movq	-40(%rsp), %r8          # 8-byte Reload
	movq	%r8, %rax
	jne	.LBB25_16
# BB#11:                                # %vector.memcheck
                                        #   in Loop: Header=BB25_8 Depth=1
	andb	%r14b, %bl
	andb	$1, %bl
	movq	%r8, %rax
	jne	.LBB25_16
# BB#12:                                # %vector.memcheck
                                        #   in Loop: Header=BB25_8 Depth=1
	andb	%r9b, %sil
	andb	$1, %sil
	movq	%r8, %rax
	jne	.LBB25_16
# BB#13:                                # %vector.body.preheader
                                        #   in Loop: Header=BB25_8 Depth=1
	movq	-88(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,2), %rax
	movq	-104(%rsp), %rsi        # 8-byte Reload
	leaq	(%r11,%rsi,2), %r9
	leaq	(%r13,%rsi,2), %rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_14:                              # %vector.body
                                        #   Parent Loop BB25_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-144(%rax,%rsi,2), %xmm0
	movups	-128(%rax,%rsi,2), %xmm1
	movups	%xmm0, -16(%rbx,%rsi,2)
	movups	%xmm1, (%rbx,%rsi,2)
	movups	-16(%rax,%rsi,2), %xmm0
	movups	(%rax,%rsi,2), %xmm1
	movups	%xmm0, -16(%r9,%rsi,2)
	movups	%xmm1, (%r9,%rsi,2)
	addq	$16, %rsi
	cmpq	%rsi, %rbp
	jne	.LBB25_14
# BB#15:                                # %middle.block
                                        #   in Loop: Header=BB25_8 Depth=1
	cmpq	%rbp, -120(%rsp)        # 8-byte Folded Reload
	movq	-96(%rsp), %rax         # 8-byte Reload
	je	.LBB25_18
	.p2align	4, 0x90
.LBB25_16:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB25_8 Depth=1
	movq	-16(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rdi,2), %rsi
	decq	%rax
	.p2align	4, 0x90
.LBB25_17:                              # %scalar.ph
                                        #   Parent Loop BB25_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-126(%rsi,%rax,2), %edi
	movw	%di, 2(%r13,%rax,2)
	movzwl	2(%rsi,%rax,2), %edi
	movw	%di, 2(%r11,%rax,2)
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB25_17
.LBB25_18:                              # %._crit_edge
                                        #   in Loop: Header=BB25_8 Depth=1
	incl	%r12d
	addl	-124(%rsp), %r15d       # 4-byte Folded Reload
	cmpq	%r10, %rdx
	leaq	1(%rdx), %rdx
	jl	.LBB25_8
.LBB25_19:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	copyPredMB, .Lfunc_end25-copyPredMB
	.cfi_endproc

	.type	concealment_head,@object # @concealment_head
	.bss
	.globl	concealment_head
	.p2align	3
concealment_head:
	.quad	0
	.size	concealment_head, 8

	.type	concealment_end,@object # @concealment_end
	.globl	concealment_end
	.p2align	3
concealment_end:
	.quad	0
	.size	concealment_end, 8

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ercConcealInterFrame: predMB"
	.size	.L.str, 29

	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Missing POC=%d\n"
	.size	.L.str.1, 16

	.type	pocs_in_dpb,@object     # @pocs_in_dpb
	.comm	pocs_in_dpb,400,16
	.type	last_out_fs,@object     # @last_out_fs
	.comm	last_out_fs,8,8
	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	erc_img,@object         # @erc_img
	.comm	erc_img,8,8
	.type	uv_div.0,@object        # @uv_div.0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
uv_div.0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.size	uv_div.0, 16

	.type	uv_div.1,@object        # @uv_div.1
	.p2align	4
uv_div.1:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	uv_div.1, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
