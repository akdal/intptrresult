	.text
	.file	"zrelbit.bc"
	.globl	zeq
	.p2align	4, 0x90
	.type	zeq,@function
zeq:                                    # @zeq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	cmpb	$13, %dl
	ja	.LBB0_4
# BB#1:
	andl	$63, %ecx
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_2:
	movl	$-7, %ebp
	testb	$2, %ah
	jne	.LBB0_4
	jmp	.LBB0_9
.LBB0_3:
	movq	%rbx, %rdi
	callq	dict_access_ref
	movl	$-7, %ebp
	testb	$2, 9(%rax)
	je	.LBB0_9
.LBB0_4:
	leaq	-16(%rbx), %r14
	movzwl	-8(%rbx), %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	cmpb	$13, %dl
	ja	.LBB0_8
# BB#5:
	andl	$63, %ecx
	jmpq	*.LJTI0_1(,%rcx,8)
.LBB0_6:
	movl	$-7, %ebp
	testb	$2, %ah
	jne	.LBB0_8
	jmp	.LBB0_9
.LBB0_7:
	movq	%r14, %rdi
	callq	dict_access_ref
	movl	$-7, %ebp
	testb	$2, 9(%rax)
	je	.LBB0_9
.LBB0_8:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	obj_eq
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	movw	%cx, -16(%rbx)
	movw	$4, -8(%rbx)
	addq	$-16, osp(%rip)
.LBB0_9:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	zeq, .Lfunc_end0-zeq
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_2
	.quad	.LBB0_4
	.quad	.LBB0_3
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_2
	.quad	.LBB0_4
	.quad	.LBB0_4
	.quad	.LBB0_2
.LJTI0_1:
	.quad	.LBB0_6
	.quad	.LBB0_8
	.quad	.LBB0_7
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_6
	.quad	.LBB0_8
	.quad	.LBB0_8
	.quad	.LBB0_6

	.text
	.globl	zne
	.p2align	4, 0x90
	.type	zne,@function
zne:                                    # @zne
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movzwl	8(%rbx), %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	cmpb	$13, %dl
	ja	.LBB1_4
# BB#1:
	andl	$63, %ecx
	jmpq	*.LJTI1_0(,%rcx,8)
.LBB1_2:
	movl	$-7, %ebp
	testb	$2, %ah
	jne	.LBB1_4
	jmp	.LBB1_9
.LBB1_3:
	movq	%rbx, %rdi
	callq	dict_access_ref
	movl	$-7, %ebp
	testb	$2, 9(%rax)
	je	.LBB1_9
.LBB1_4:
	leaq	-16(%rbx), %r14
	movzwl	-8(%rbx), %eax
	movl	%eax, %ecx
	shrl	$2, %ecx
	movl	%ecx, %edx
	andb	$63, %dl
	cmpb	$13, %dl
	ja	.LBB1_8
# BB#5:
	andl	$63, %ecx
	jmpq	*.LJTI1_1(,%rcx,8)
.LBB1_6:
	movl	$-7, %ebp
	testb	$2, %ah
	jne	.LBB1_8
	jmp	.LBB1_9
.LBB1_7:
	movq	%r14, %rdi
	callq	dict_access_ref
	movl	$-7, %ebp
	testb	$2, 9(%rax)
	je	.LBB1_9
.LBB1_8:
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	obj_eq
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	movw	$4, -8(%rbx)
	addq	$-16, osp(%rip)
	movw	%cx, -16(%rbx)
.LBB1_9:                                # %zeq.exit.thread
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	zne, .Lfunc_end1-zne
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_2
	.quad	.LBB1_4
	.quad	.LBB1_3
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_2
	.quad	.LBB1_4
	.quad	.LBB1_4
	.quad	.LBB1_2
.LJTI1_1:
	.quad	.LBB1_6
	.quad	.LBB1_8
	.quad	.LBB1_7
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_6
	.quad	.LBB1_8
	.quad	.LBB1_8
	.quad	.LBB1_6

	.text
	.globl	zge
	.p2align	4, 0x90
	.type	zge,@function
zge:                                    # @zge
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$6, %esi
	callq	obj_compare
	testl	%eax, %eax
	js	.LBB2_2
# BB#1:
	movw	%ax, -16(%rbx)
	movw	$4, -8(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB2_2:
	popq	%rbx
	retq
.Lfunc_end2:
	.size	zge, .Lfunc_end2-zge
	.cfi_endproc

	.globl	zgt
	.p2align	4, 0x90
	.type	zgt,@function
zgt:                                    # @zgt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$4, %esi
	callq	obj_compare
	testl	%eax, %eax
	js	.LBB3_2
# BB#1:
	movw	%ax, -16(%rbx)
	movw	$4, -8(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB3_2:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	zgt, .Lfunc_end3-zgt
	.cfi_endproc

	.globl	zle
	.p2align	4, 0x90
	.type	zle,@function
zle:                                    # @zle
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$3, %esi
	callq	obj_compare
	testl	%eax, %eax
	js	.LBB4_2
# BB#1:
	movw	%ax, -16(%rbx)
	movw	$4, -8(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB4_2:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	zle, .Lfunc_end4-zle
	.cfi_endproc

	.globl	zlt
	.p2align	4, 0x90
	.type	zlt,@function
zlt:                                    # @zlt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	obj_compare
	testl	%eax, %eax
	js	.LBB5_2
# BB#1:
	movw	%ax, -16(%rbx)
	movw	$4, -8(%rbx)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB5_2:
	popq	%rbx
	retq
.Lfunc_end5:
	.size	zlt, .Lfunc_end5-zlt
	.cfi_endproc

	.globl	zmax
	.p2align	4, 0x90
	.type	zmax,@function
zmax:                                   # @zmax
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 16
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	obj_compare
	testl	%eax, %eax
	js	.LBB6_4
# BB#1:
	je	.LBB6_3
# BB#2:
	movups	(%rbx), %xmm0
	movups	%xmm0, -16(%rbx)
.LBB6_3:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB6_4:
	popq	%rbx
	retq
.Lfunc_end6:
	.size	zmax, .Lfunc_end6-zmax
	.cfi_endproc

	.globl	zmin
	.p2align	4, 0x90
	.type	zmin,@function
zmin:                                   # @zmin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$4, %esi
	callq	obj_compare
	testl	%eax, %eax
	js	.LBB7_4
# BB#1:
	je	.LBB7_3
# BB#2:
	movups	(%rbx), %xmm0
	movups	%xmm0, -16(%rbx)
.LBB7_3:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB7_4:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	zmin, .Lfunc_end7-zmin
	.cfi_endproc

	.globl	zand
	.p2align	4, 0x90
	.type	zand,@function
zand:                                   # @zand
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	movw	-8(%rdi), %dx
	xorw	%cx, %dx
	movl	$-20, %eax
	testb	$-4, %dl
	jne	.LBB8_6
# BB#1:
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB8_4
# BB#2:
	cmpb	$1, %cl
	jne	.LBB8_6
# BB#3:
	movzwl	(%rdi), %eax
	andw	%ax, -16(%rdi)
	jmp	.LBB8_5
.LBB8_4:
	movq	(%rdi), %rax
	andq	%rax, -16(%rdi)
.LBB8_5:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB8_6:
	retq
.Lfunc_end8:
	.size	zand, .Lfunc_end8-zand
	.cfi_endproc

	.globl	znot
	.p2align	4, 0x90
	.type	znot,@function
znot:                                   # @znot
	.cfi_startproc
# BB#0:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB9_3
# BB#1:
	movl	$-20, %eax
	cmpb	$1, %cl
	jne	.LBB9_5
# BB#2:
	xorl	%eax, %eax
	cmpw	$0, (%rdi)
	sete	%al
	movw	%ax, (%rdi)
	jmp	.LBB9_4
.LBB9_3:
	notq	(%rdi)
.LBB9_4:
	xorl	%eax, %eax
.LBB9_5:
	retq
.Lfunc_end9:
	.size	znot, .Lfunc_end9-znot
	.cfi_endproc

	.globl	zor
	.p2align	4, 0x90
	.type	zor,@function
zor:                                    # @zor
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	movw	-8(%rdi), %dx
	xorw	%cx, %dx
	movl	$-20, %eax
	testb	$-4, %dl
	jne	.LBB10_6
# BB#1:
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB10_4
# BB#2:
	cmpb	$1, %cl
	jne	.LBB10_6
# BB#3:
	movzwl	(%rdi), %eax
	orw	%ax, -16(%rdi)
	jmp	.LBB10_5
.LBB10_4:
	movq	(%rdi), %rax
	orq	%rax, -16(%rdi)
.LBB10_5:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB10_6:
	retq
.Lfunc_end10:
	.size	zor, .Lfunc_end10-zor
	.cfi_endproc

	.globl	zxor
	.p2align	4, 0x90
	.type	zxor,@function
zxor:                                   # @zxor
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	movw	-8(%rdi), %dx
	xorw	%cx, %dx
	movl	$-20, %eax
	testb	$-4, %dl
	jne	.LBB11_6
# BB#1:
	shrb	$2, %cl
	cmpb	$5, %cl
	je	.LBB11_4
# BB#2:
	cmpb	$1, %cl
	jne	.LBB11_6
# BB#3:
	movzwl	(%rdi), %eax
	xorw	%ax, -16(%rdi)
	jmp	.LBB11_5
.LBB11_4:
	movq	(%rdi), %rax
	xorq	%rax, -16(%rdi)
.LBB11_5:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB11_6:
	retq
.Lfunc_end11:
	.size	zxor, .Lfunc_end11-zxor
	.cfi_endproc

	.globl	zbitshift
	.p2align	4, 0x90
	.type	zbitshift,@function
zbitshift:                              # @zbitshift
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB12_9
# BB#1:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB12_9
# BB#2:
	movq	(%rdi), %rcx
	leaq	31(%rcx), %rax
	cmpq	$63, %rax
	jb	.LBB12_4
# BB#3:
	movq	$0, -16(%rdi)
	jmp	.LBB12_8
.LBB12_4:
	movq	-16(%rdi), %rax
	testl	%ecx, %ecx
	js	.LBB12_5
# BB#6:
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rax
	jmp	.LBB12_7
.LBB12_5:
	negl	%ecx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrq	%cl, %rax
.LBB12_7:
	movq	%rax, -16(%rdi)
.LBB12_8:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB12_9:
	retq
.Lfunc_end12:
	.size	zbitshift, .Lfunc_end12-zbitshift
	.cfi_endproc

	.globl	zrelbit_op_init
	.p2align	4, 0x90
	.type	zrelbit_op_init,@function
zrelbit_op_init:                        # @zrelbit_op_init
	.cfi_startproc
# BB#0:
	movl	$zrelbit_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end13:
	.size	zrelbit_op_init, .Lfunc_end13-zrelbit_op_init
	.cfi_endproc

	.globl	obj_compare
	.p2align	4, 0x90
	.type	obj_compare,@function
obj_compare:                            # @obj_compare
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movl	%esi, %ebx
	movw	-8(%rdi), %cx
	movl	%ecx, %edx
	shrb	$2, %dl
	movl	$-20, %eax
	cmpb	$13, %dl
	je	.LBB14_11
# BB#1:
	cmpb	$11, %dl
	je	.LBB14_8
# BB#2:
	cmpb	$5, %dl
	jne	.LBB14_23
# BB#3:
	movb	8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$11, %cl
	je	.LBB14_16
# BB#4:
	cmpb	$5, %cl
	jne	.LBB14_23
# BB#5:
	movq	-16(%rdi), %rax
	cmpq	(%rdi), %rax
	sete	%al
	movb	$2, %cl
	jg	.LBB14_7
# BB#6:
	movl	%eax, %ecx
.LBB14_7:
	shrl	%cl, %ebx
	jmp	.LBB14_22
.LBB14_8:
	movss	-16(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movb	8(%rdi), %cl
	shrb	$2, %cl
	cmpb	$11, %cl
	je	.LBB14_17
# BB#9:
	cmpb	$5, %cl
	jne	.LBB14_23
# BB#10:
	cvtsi2ssq	(%rdi), %xmm1
	ucomiss	%xmm1, %xmm0
	ja	.LBB14_19
	jmp	.LBB14_20
.LBB14_11:
	testb	$2, %ch
	jne	.LBB14_13
# BB#12:
	movl	$-7, %eax
	popq	%rbx
	retq
.LBB14_13:
	movzwl	8(%rdi), %ecx
	movl	%ecx, %edx
	andl	$252, %edx
	cmpl	$52, %edx
	jne	.LBB14_23
# BB#14:
	testb	$2, %ch
	movl	$-7, %eax
	je	.LBB14_23
# BB#15:
	movzwl	-6(%rdi), %esi
	movq	-16(%rdi), %r8
	movq	(%rdi), %rdx
	movzwl	10(%rdi), %ecx
	xorl	%eax, %eax
	movq	%r8, %rdi
	callq	bytes_compare
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rax), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	jmp	.LBB14_22
.LBB14_16:
	cvtsi2ssq	-16(%rdi), %xmm0
.LBB14_17:
	movss	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB14_20
.LBB14_19:
	sarl	$2, %ebx
	jmp	.LBB14_22
.LBB14_20:
	jne	.LBB14_22
	jp	.LBB14_22
# BB#21:
	sarl	%ebx
.LBB14_22:
	andl	$1, %ebx
	movl	%ebx, %eax
.LBB14_23:
	popq	%rbx
	retq
.Lfunc_end14:
	.size	obj_compare, .Lfunc_end14-obj_compare
	.cfi_endproc

	.type	zrelbit_op_init.my_defs,@object # @zrelbit_op_init.my_defs
	.data
	.p2align	4
zrelbit_op_init.my_defs:
	.quad	.L.str
	.quad	zand
	.quad	.L.str.1
	.quad	zbitshift
	.quad	.L.str.2
	.quad	zeq
	.quad	.L.str.3
	.quad	zge
	.quad	.L.str.4
	.quad	zgt
	.quad	.L.str.5
	.quad	zle
	.quad	.L.str.6
	.quad	zlt
	.quad	.L.str.7
	.quad	zmax
	.quad	.L.str.8
	.quad	zmin
	.quad	.L.str.9
	.quad	zne
	.quad	.L.str.10
	.quad	znot
	.quad	.L.str.11
	.quad	zor
	.quad	.L.str.12
	.quad	zxor
	.zero	16
	.size	zrelbit_op_init.my_defs, 224

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"2and"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"2bitshift"
	.size	.L.str.1, 10

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"2eq"
	.size	.L.str.2, 4

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"2ge"
	.size	.L.str.3, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"2gt"
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"2le"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"2lt"
	.size	.L.str.6, 4

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"2max"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"2min"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"2ne"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"1not"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"2or"
	.size	.L.str.11, 4

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"2xor"
	.size	.L.str.12, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
