	.text
	.file	"cubic.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	3225419776              # float -3
.LCPI0_1:
	.long	1091567616              # float 9
.LCPI0_2:
	.long	1104674816              # float 27
.LCPI0_3:
	.long	1113063424              # float 54
.LCPI0_6:
	.long	1077936128              # float 3
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_4:
	.quad	-4611686018427387904    # double -2
.LCPI0_5:
	.quad	4613937818241073152     # double 3
.LCPI0_7:
	.quad	4607182418800017408     # double 1
.LCPI0_8:
	.quad	4618760256179416344     # double 6.2831853071795862
.LCPI0_9:
	.quad	4623263855806786840     # double 12.566370614359172
.LCPI0_11:
	.quad	4599676419421066581     # double 0.33333333333333331
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_10:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI0_12:
	.quad	-4616189618054758400    # double -1
	.quad	4607182418800017408     # double 1
	.text
	.globl	SolveCubic
	.p2align	4, 0x90
	.type	SolveCubic,@function
SolveCubic:                             # @SolveCubic
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$184, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 208
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 176(%rsp)
	fldl	176(%rsp)
	divsd	%xmm0, %xmm2
	movsd	%xmm2, 168(%rsp)
	fldl	168(%rsp)
	divsd	%xmm0, %xmm3
	movsd	%xmm3, 160(%rsp)
	fldl	160(%rsp)
	fld	%st(2)
	fmul	%st(3)
	fld	%st(2)
	fmuls	.LCPI0_0(%rip)
	faddp	%st(1)
	flds	.LCPI0_1(%rip)
	fdivr	%st(0), %st(1)
	fld	%st(4)
	fadd	%st(5)
	fmul	%st(5)
	fmul	%st(5)
	fld	%st(5)
	fstpt	36(%rsp)                # 10-byte Folded Spill
	fxch	%st(5)
	fmulp	%st(1)
	fmulp	%st(3)
	fxch	%st(3)
	fsubp	%st(2)
	fmuls	.LCPI0_2(%rip)
	faddp	%st(1)
	fdivs	.LCPI0_3(%rip)
	fld	%st(0)
	fmul	%st(1)
	fld	%st(2)
	fmul	%st(3)
	fld	%st(3)
	fstpt	24(%rsp)                # 10-byte Folded Spill
	fmulp	%st(3)
	fsub	%st(2)
	fstpl	152(%rsp)
	movsd	152(%rsp), %xmm1        # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm1, %xmm0
	jae	.LBB0_1
# BB#10:
	fstp	%st(1)
	movl	$1, (%rdi)
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_12
# BB#11:                                # %call.sqrt71
	movapd	%xmm1, %xmm0
	fstpt	8(%rsp)                 # 10-byte Folded Spill
	callq	sqrt
	fldt	8(%rsp)                 # 10-byte Folded Reload
.LBB0_12:                               # %.split70
	fstl	88(%rsp)
	fldz
	xorl	%ebx, %ebx
	fucompi	%st(1)
	fstp	%st(0)
	seta	%bl
	movsd	88(%rsp), %xmm2         # xmm2 = mem[0],zero
	andpd	.LCPI0_10(%rip), %xmm2
	addsd	%xmm0, %xmm2
	movsd	.LCPI0_11(%rip), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm2, %xmm0
	callq	pow
	movsd	%xmm0, 80(%rsp)
	fldl	80(%rsp)
	fldt	24(%rsp)                # 10-byte Folded Reload
	fdiv	%st(1)
	faddp	%st(1)
	fstpl	72(%rsp)
	movsd	.LCPI0_12(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	72(%rsp), %xmm0
	fldt	36(%rsp)                # 10-byte Folded Reload
	fdivs	.LCPI0_0(%rip)
	movsd	%xmm0, 64(%rsp)
	faddl	64(%rsp)
	fstpl	(%r14)
	jmp	.LBB0_13
.LBB0_1:
	movl	$3, (%rdi)
	fxch	%st(1)
	fstpl	144(%rsp)
	movsd	144(%rsp), %xmm1        # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_3
# BB#2:                                 # %call.sqrt
	movapd	%xmm1, %xmm0
	fstpt	8(%rsp)                 # 10-byte Folded Spill
	callq	sqrt
	fldt	8(%rsp)                 # 10-byte Folded Reload
.LBB0_3:                                # %.split
	movsd	%xmm0, 136(%rsp)
	fdivl	136(%rsp)
	fstpl	120(%rsp)
	movsd	120(%rsp), %xmm0        # xmm0 = mem[0],zero
	callq	acos
	fldt	24(%rsp)                # 10-byte Folded Reload
	fstpl	128(%rsp)
	movsd	128(%rsp), %xmm1        # xmm1 = mem[0],zero
	movsd	%xmm1, 56(%rsp)         # 8-byte Spill
	sqrtsd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm1
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	jnp	.LBB0_5
# BB#4:                                 # %call.sqrt67
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sqrt
	movapd	%xmm0, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB0_5:                                # %.split.split
	mulsd	.LCPI0_4(%rip), %xmm1
	movsd	%xmm1, 48(%rsp)         # 8-byte Spill
	divsd	.LCPI0_5(%rip), %xmm0
	callq	cos
	mulsd	48(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 112(%rsp)
	fldt	36(%rsp)                # 10-byte Folded Reload
	fdivs	.LCPI0_6(%rip)
	fld	%st(0)
	fstpt	36(%rsp)                # 10-byte Folded Spill
	fsubrl	112(%rsp)
	fstpl	(%r14)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_7
# BB#6:                                 # %call.sqrt68
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sqrt
.LBB0_7:                                # %.split.split.split
	mulsd	.LCPI0_4(%rip), %xmm0
	movsd	%xmm0, 48(%rsp)         # 8-byte Spill
	movsd	.LCPI0_7(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	.LCPI0_8(%rip), %xmm0   # xmm0 = mem[0],zero
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	divsd	.LCPI0_5(%rip), %xmm0
	callq	cos
	mulsd	48(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 104(%rsp)
	fldt	36(%rsp)                # 10-byte Folded Reload
	fsubrl	104(%rsp)
	fstpl	8(%r14)
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_9
# BB#8:                                 # %call.sqrt69
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	sqrt
.LBB0_9:                                # %.split.split.split.split
	mulsd	.LCPI0_4(%rip), %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movsd	.LCPI0_7(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	atan
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI0_9(%rip), %xmm0
	divsd	.LCPI0_5(%rip), %xmm0
	callq	cos
	mulsd	24(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	%xmm0, 96(%rsp)
	fldt	36(%rsp)                # 10-byte Folded Reload
	fsubrl	96(%rsp)
	fstpl	16(%r14)
.LBB0_13:
	addq	$184, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	SolveCubic, .Lfunc_end0-SolveCubic
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
