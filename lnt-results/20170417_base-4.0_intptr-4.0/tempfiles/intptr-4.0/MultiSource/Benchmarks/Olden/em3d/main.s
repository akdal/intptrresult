	.text
	.file	"main.bc"
	.globl	print_graph
	.p2align	4, 0x90
	.type	print_graph,@function
print_graph:                            # @print_graph
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	%esi, %r15
	movq	(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_3
	.p2align	4, 0x90
.LBB0_1:                                # %.lr.ph17
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movl	40(%rbx), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_1
.LBB0_3:                                # %._crit_edge18
	movq	8(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_6
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movl	40(%rbx), %esi
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB0_4
.LBB0_6:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	print_graph, .Lfunc_end0-print_graph
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %r15, -16
	callq	dealwithargs
	movl	n_nodes(%rip), %esi
	movl	d_nodes(%rip), %edx
	movl	local_p(%rip), %ecx
	movl	NumNodes(%rip), %r8d
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	xorl	%eax, %eax
	callq	initialize_graph
	movq	%rax, %r14
	cmpl	$0, DebugFlag(%rip)
	je	.LBB1_10
# BB#1:
	movl	NumNodes(%rip), %eax
	testl	%eax, %eax
	jle	.LBB1_10
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #     Child Loop BB1_7 Depth 2
	movq	(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_6
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph17.i
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movl	40(%rbx), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_4
.LBB1_6:                                # %._crit_edge18.i
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	8(%r14,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_9
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph.i
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movl	40(%rbx), %esi
	movl	$.L.str.1, %edi
	movb	$1, %al
	callq	printf
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_7
.LBB1_9:                                # %print_graph.exit
                                        #   in Loop: Header=BB1_3 Depth=1
	incq	%r15
	movslq	NumNodes(%rip), %rax
	cmpq	%rax, %r15
	jl	.LBB1_3
.LBB1_10:                               # %.loopexit
	movq	(%r14), %rdi
	callq	compute_nodes
	movq	8(%r14), %rdi
	callq	compute_nodes
	movl	nonlocals(%rip), %esi
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	printf
	callq	printstats
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"E: value %f, from_count %d\n"
	.size	.L.str, 28

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"H: value %f, from_count %d\n"
	.size	.L.str.1, 28

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Hello world--Doing em3d with args %d %d %d %d\n"
	.size	.L.str.2, 47

	.type	DebugFlag,@object       # @DebugFlag
	.comm	DebugFlag,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"nonlocals = %d\n"
	.size	.L.str.3, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
