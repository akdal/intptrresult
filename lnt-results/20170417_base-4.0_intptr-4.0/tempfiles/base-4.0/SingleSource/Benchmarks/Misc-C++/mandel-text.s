	.text
	.file	"mandel-text.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4587366580439587226     # double 0.050000000000000003
.LCPI0_1:
	.quad	-4616189618054758400    # double -1
.LCPI0_2:
	.quad	-4611010478483282330    # double -2.2999999999999998
.LCPI0_3:
	.quad	4616189618054758400     # double 4
.LCPI0_4:
	.quad	4607182418800017408     # double 1
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 48
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	xorl	%ebx, %ebx
	xorpd	%xmm0, %xmm0
	movsd	.LCPI0_3(%rip), %xmm6   # xmm6 = mem[0],zero
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_2 Depth 2
                                        #       Child Loop BB0_3 Depth 3
                                        #         Child Loop BB0_5 Depth 4
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movapd	%xmm0, %xmm7
	mulsd	.LCPI0_0(%rip), %xmm7
	addsd	.LCPI0_1(%rip), %xmm7
	xorpd	%xmm8, %xmm8
	xorl	%ebp, %ebp
	movsd	%xmm7, (%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_3 Depth 3
                                        #         Child Loop BB0_5 Depth 4
	movapd	%xmm8, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_2(%rip), %xmm0
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_5 Depth 4
	movl	$1, %edx
	movapd	%xmm7, %xmm3
	movapd	%xmm0, %xmm2
	xorl	%ecx, %ecx
	jmp	.LBB0_5
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_5 Depth=4
	addsd	%xmm1, %xmm1
	mulsd	%xmm2, %xmm1
	addsd	%xmm7, %xmm1
	subsd	%xmm3, %xmm4
	addsd	%xmm0, %xmm4
	addl	$2, %ecx
	addl	$2, %edx
	movapd	%xmm1, %xmm3
	movapd	%xmm4, %xmm2
.LBB0_5:                                #   Parent Loop BB0_1 Depth=1
                                        #     Parent Loop BB0_2 Depth=2
                                        #       Parent Loop BB0_3 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movapd	%xmm2, %xmm1
	mulsd	%xmm1, %xmm1
	movapd	%xmm3, %xmm4
	mulsd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	addsd	%xmm4, %xmm5
	ucomisd	%xmm6, %xmm5
	ja	.LBB0_9
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=4
	cmpl	$255, %edx
	jge	.LBB0_10
# BB#7:                                 #   in Loop: Header=BB0_5 Depth=4
	addsd	%xmm2, %xmm2
	mulsd	%xmm3, %xmm2
	addsd	%xmm7, %xmm2
	subsd	%xmm4, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm1, %xmm4
	mulsd	%xmm4, %xmm4
	movapd	%xmm2, %xmm3
	mulsd	%xmm3, %xmm3
	movapd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	ucomisd	%xmm6, %xmm5
	jbe	.LBB0_4
# BB#8:                                 #   in Loop: Header=BB0_3 Depth=3
	orl	$1, %ecx
.LBB0_9:                                #   in Loop: Header=BB0_3 Depth=3
	xorl	%esi, %esi
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=3
	movb	$1, %sil
	movl	%edx, %ecx
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=3
	incl	%eax
	cmpl	$2000, %eax             # imm = 0x7D0
	jne	.LBB0_3
# BB#12:                                #   in Loop: Header=BB0_2 Depth=2
	cmpl	$100000, %ecx           # imm = 0x186A0
	movsd	%xmm8, 8(%rsp)          # 8-byte Spill
	jge	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_2 Depth=2
	movq	stdout(%rip), %rsi
	movl	$88, %edi
	jmp	.LBB0_17
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=2
	testb	%sil, %sil
	movq	stdout(%rip), %rsi
	je	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_2 Depth=2
	movl	$32, %edi
	jmp	.LBB0_17
.LBB0_16:                               #   in Loop: Header=BB0_2 Depth=2
	movl	$46, %edi
	.p2align	4, 0x90
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=2
	callq	_IO_putc
	movsd	8(%rsp), %xmm8          # 8-byte Reload
                                        # xmm8 = mem[0],zero
	movsd	(%rsp), %xmm7           # 8-byte Reload
                                        # xmm7 = mem[0],zero
	movsd	.LCPI0_3(%rip), %xmm6   # xmm6 = mem[0],zero
	addsd	.LCPI0_4(%rip), %xmm8
	incl	%ebp
	cmpl	$78, %ebp
	jne	.LBB0_2
# BB#18:                                #   in Loop: Header=BB0_1 Depth=1
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movsd	.LCPI0_3(%rip), %xmm6   # xmm6 = mem[0],zero
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI0_4(%rip), %xmm0
	incl	%ebx
	cmpl	$40, %ebx
	jne	.LBB0_1
# BB#19:
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
