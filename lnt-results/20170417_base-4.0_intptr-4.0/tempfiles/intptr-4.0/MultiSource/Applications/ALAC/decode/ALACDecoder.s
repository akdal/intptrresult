	.text
	.file	"ALACDecoder.bc"
	.globl	_ZN11ALACDecoderC2Ev
	.p2align	4, 0x90
	.type	_ZN11ALACDecoderC2Ev,@function
_ZN11ALACDecoderC2Ev:                   # @_ZN11ALACDecoderC2Ev
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	movq	$0, 16(%rdi)
	movups	%xmm0, 48(%rdi)
	movups	%xmm0, 32(%rdi)
	retq
.Lfunc_end0:
	.size	_ZN11ALACDecoderC2Ev, .Lfunc_end0-_ZN11ALACDecoderC2Ev
	.cfi_endproc

	.globl	_ZN11ALACDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN11ALACDecoderD2Ev,@function
_ZN11ALACDecoderD2Ev:                   # @_ZN11ALACDecoderD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	free
	movq	$0, 32(%rbx)
.LBB1_2:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	free
	movq	$0, 40(%rbx)
.LBB1_4:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	free
	movq	$0, 48(%rbx)
.LBB1_6:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN11ALACDecoderD2Ev, .Lfunc_end1-_ZN11ALACDecoderD2Ev
	.cfi_endproc

	.globl	_ZN11ALACDecoder4InitEPvj
	.p2align	4, 0x90
	.type	_ZN11ALACDecoder4InitEPvj,@function
_ZN11ALACDecoder4InitEPvj:              # @_ZN11ALACDecoder4InitEPvj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 80
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movb	4(%rbx), %al
	cmpb	$102, %al
	jne	.LBB2_5
# BB#1:
	cmpb	$114, 5(%rbx)
	jne	.LBB2_10
# BB#2:
	cmpb	$109, 6(%rbx)
	jne	.LBB2_10
# BB#3:
	cmpb	$97, 7(%rbx)
	jne	.LBB2_10
# BB#4:
	addl	$-12, %edx
	movb	16(%rbx), %al
	addq	$12, %rbx
.LBB2_5:
	cmpb	$97, %al
	jne	.LBB2_10
# BB#6:
	cmpb	$108, 5(%rbx)
	jne	.LBB2_10
# BB#7:
	cmpb	$97, 6(%rbx)
	jne	.LBB2_10
# BB#8:
	cmpb	$99, 7(%rbx)
	jne	.LBB2_10
# BB#9:
	addq	$12, %rbx
	addl	$-12, %edx
.LBB2_10:                               # %.thread
	cmpl	$24, %edx
	jb	.LBB2_13
# BB#11:
	movl	(%rbx), %edi
	callq	Swap32BtoN
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movb	4(%rbx), %r15b
	movb	5(%rbx), %al
	movb	%al, 19(%rsp)           # 1-byte Spill
	movb	6(%rbx), %al
	movb	%al, 18(%rsp)           # 1-byte Spill
	movb	7(%rbx), %al
	movb	%al, 17(%rsp)           # 1-byte Spill
	movb	8(%rbx), %al
	movb	%al, 16(%rsp)           # 1-byte Spill
	movb	9(%rbx), %al
	movb	%al, 15(%rsp)           # 1-byte Spill
	movzwl	10(%rbx), %edi
	callq	Swap16BtoN
	movl	%eax, %r12d
	movl	12(%rbx), %edi
	callq	Swap32BtoN
	movl	%eax, %r13d
	movl	16(%rbx), %edi
	callq	Swap32BtoN
	movl	%eax, %ebp
	movl	20(%rbx), %edi
	callq	Swap32BtoN
	movl	20(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, (%r14)
	movb	%r15b, 4(%r14)
	movb	19(%rsp), %dl           # 1-byte Reload
	movb	%dl, 5(%r14)
	movb	18(%rsp), %dl           # 1-byte Reload
	movb	%dl, 6(%r14)
	movb	17(%rsp), %dl           # 1-byte Reload
	movb	%dl, 7(%r14)
	movb	16(%rsp), %dl           # 1-byte Reload
	movb	%dl, 8(%r14)
	movb	15(%rsp), %dl           # 1-byte Reload
	movb	%dl, 9(%r14)
	movw	%r12w, 10(%r14)
	movl	%r13d, 12(%r14)
	movl	%ebp, 16(%r14)
	movl	%eax, 20(%r14)
	testb	%r15b, %r15b
	je	.LBB2_12
.LBB2_13:
	movl	$-50, %eax
.LBB2_14:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_12:
	movl	%ecx, %ebx
	shlq	$2, %rbx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %r15
	movq	%r15, 32(%r14)
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, %rbp
	movq	%rbp, 40(%r14)
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	calloc
	movq	%rax, 48(%r14)
	movq	%rax, 56(%r14)
	xorl	%ecx, %ecx
	testq	%rax, %rax
	movl	$-108, %eax
	cmovel	%eax, %ecx
	testq	%rbp, %rbp
	cmovel	%eax, %ecx
	testq	%r15, %r15
	cmovel	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB2_14
.Lfunc_end2:
	.size	_ZN11ALACDecoder4InitEPvj, .Lfunc_end2-_ZN11ALACDecoder4InitEPvj
	.cfi_endproc

	.globl	_ZN11ALACDecoder6DecodeEP9BitBufferPhjjPj
	.p2align	4, 0x90
	.type	_ZN11ALACDecoder6DecodeEP9BitBufferPhjjPj,@function
_ZN11ALACDecoder6DecodeEP9BitBufferPhjjPj: # @_ZN11ALACDecoder6DecodeEP9BitBufferPhjjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
	subq	$376, %rsp              # imm = 0x178
.Lcfi21:
	.cfi_def_cfa_offset 432
.Lcfi22:
	.cfi_offset %rbx, -56
.Lcfi23:
	.cfi_offset %r12, -48
.Lcfi24:
	.cfi_offset %r13, -40
.Lcfi25:
	.cfi_offset %r14, -32
.Lcfi26:
	.cfi_offset %r15, -24
.Lcfi27:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rsi, %r15
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	testq	%r15, %r15
	sete	%al
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	testq	%rdx, %rdx
	sete	%bl
	movq	%r9, 128(%rsp)          # 8-byte Spill
	testq	%r9, %r9
	sete	%dl
	movq	%r8, 32(%rsp)           # 8-byte Spill
	testl	%r8d, %r8d
	movl	$-50, %r8d
	je	.LBB3_157
# BB#1:
	orb	%bl, %al
	orb	%dl, %al
	jne	.LBB3_157
# BB#2:
	movq	24(%rsp), %rax          # 8-byte Reload
	movw	$0, 24(%rax)
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	%rcx, %r12
	movl	%ecx, (%rax)
	movq	32(%rsp), %rcx          # 8-byte Reload
	leal	(,%rcx,8), %eax
	movl	%eax, 140(%rsp)         # 4-byte Spill
	leal	(%rcx,%rcx,2), %eax
	movl	%eax, 152(%rsp)         # 4-byte Spill
	leal	(%rcx,%rcx), %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r15, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_76 Depth 2
                                        #     Child Loop BB3_52 Depth 2
                                        #     Child Loop BB3_27 Depth 2
                                        #     Child Loop BB3_30 Depth 2
                                        #     Child Loop BB3_96 Depth 2
                                        #     Child Loop BB3_47 Depth 2
                                        #     Child Loop BB3_44 Depth 2
                                        #     Child Loop BB3_13 Depth 2
                                        #     Child Loop BB3_57 Depth 2
                                        #     Child Loop BB3_67 Depth 2
                                        #     Child Loop BB3_81 Depth 2
                                        #     Child Loop BB3_84 Depth 2
	movq	(%r15), %rax
	cmpq	8(%r15), %rax
	jae	.LBB3_108
# BB#4:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movzbl	6(%rax), %ebx
	movl	$3, %esi
	movq	%r15, %rdi
	callq	BitBufferReadSmall
	xorl	%r8d, %r8d
	cmpb	$7, %al
	ja	.LBB3_40
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	$-50, %r8d
	movzbl	%al, %eax
	jmpq	*.LJTI3_0(,%rax,8)
.LBB3_6:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$4, %esi
	movq	%r15, %rdi
	callq	BitBufferReadSmall
	movl	$1, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	orl	24(%rax), %edx
	movw	%dx, 24(%rax)
	movl	$12, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	$-50, %r8d
	testw	%ax, %ax
	jne	.LBB3_157
# BB#7:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movl	$4, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	$-50, %r8d
	movl	%eax, %ebx
	movl	%ebx, %r13d
	shrb	%r13b
	andb	$3, %r13b
	cmpb	$3, %r13b
	movq	%r12, %r14
	je	.LBB3_157
# BB#8:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	%ebx, %eax
	andl	$1, %ebx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	5(%rcx), %edx
	movl	%r13d, %ecx
	shlb	$3, %cl
	movzbl	%cl, %r12d
	testb	$-8, %al
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	movl	$16, %esi
	movq	%r15, %rdi
	movq	%rdx, %r14
	callq	BitBufferRead
	movl	%eax, %ebp
	shll	$16, %ebp
	movl	$16, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movq	%r14, %rdx
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	orl	%ebp, %eax
	movq	%rax, %r14
.LBB3_10:                               #   in Loop: Header=BB3_3 Depth=1
	subl	%r12d, %edx
	testl	%ebx, %ebx
	movq	%r14, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	jne	.LBB3_41
# BB#11:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%r12d, 100(%rsp)        # 4-byte Spill
	movl	$8, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	$8, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	$8, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movl	$8, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	%eax, %r14d
	shrl	$5, %r14d
	andl	$31, %eax
	movl	%eax, 80(%rsp)          # 4-byte Spill
	je	.LBB3_14
# BB#12:                                # %.lr.ph455.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	80(%rsp), %ebp          # 4-byte Reload
	leaq	192(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_13:                               # %.lr.ph455
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movw	%ax, (%rbx)
	addq	$2, %rbx
	decq	%rbp
	jne	.LBB3_13
.LBB3_14:                               # %._crit_edge456
                                        #   in Loop: Header=BB3_3 Depth=1
	andl	$7, %r14d
	testb	%r13b, %r13b
	movq	64(%rsp), %r12          # 8-byte Reload
	je	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_3 Depth=1
	movq	16(%r15), %rax
	movq	%rax, 176(%rsp)
	movdqu	(%r15), %xmm0
	movdqa	%xmm0, 160(%rsp)
	movl	%r12d, %esi
	imull	100(%rsp), %esi         # 4-byte Folded Reload
	movq	%r15, %rdi
	callq	BitBufferAdvance
.LBB3_16:                               #   in Loop: Header=BB3_3 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movzbl	7(%rbx), %esi
	imull	52(%rsp), %r14d         # 4-byte Folded Reload
	shrl	$2, %r14d
	movzbl	8(%rbx), %ecx
	movzwl	10(%rbx), %eax
	movl	%eax, (%rsp)
	leaq	264(%rsp), %rbp
	movq	%rbp, %rdi
	movl	%r14d, %edx
	movl	%r12d, %r8d
	movl	%r12d, %r9d
	callq	set_ag_params
	movq	48(%rbx), %rdx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movl	%r12d, %ecx
	movq	56(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	leaq	108(%rsp), %r9
	callq	dyn_decomp
	movl	%eax, %r8d
	testl	%r8d, %r8d
	jne	.LBB3_157
# BB#17:                                #   in Loop: Header=BB3_3 Depth=1
	movl	76(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %eax
	andl	$240, %eax
	andl	$15, %ebp
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	je	.LBB3_53
# BB#18:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$0, (%rsp)
	xorl	%ecx, %ecx
	movl	$31, %r8d
	movq	%rdi, %rsi
	movl	%r12d, %edx
	movq	56(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %r9d
	callq	unpc_block
	movq	32(%rbx), %rsi
	movq	48(%rbx), %rdi
	movl	%ebp, (%rsp)
	movl	%r12d, %edx
	leaq	192(%rsp), %rcx
	movl	80(%rsp), %r8d          # 4-byte Reload
	movl	%r14d, %r9d
	jmp	.LBB3_54
.LBB3_19:                               #   in Loop: Header=BB3_3 Depth=1
	leal	2(%rbp), %r13d
	cmpl	32(%rsp), %r13d         # 4-byte Folded Reload
	ja	.LBB3_111
# BB#20:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$4, %esi
	movq	%r15, %rdi
	callq	BitBufferReadSmall
	movl	$1, %edx
	movl	%eax, %ecx
	shll	%cl, %edx
	movq	24(%rsp), %rax          # 8-byte Reload
	orl	24(%rax), %edx
	movw	%dx, 24(%rax)
	movl	$12, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	$-50, %r8d
	testw	%ax, %ax
	jne	.LBB3_157
# BB#21:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%ebx, 52(%rsp)          # 4-byte Spill
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movl	$4, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	$-50, %r8d
	movl	%eax, %ebx
	shrb	%al
	andb	$3, %al
	movb	%al, 56(%rsp)           # 1-byte Spill
	cmpb	$3, %al
	je	.LBB3_157
# BB#22:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%ebx, %eax
	andl	$1, %ebx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movzbl	5(%rcx), %edx
	movl	%edx, 80(%rsp)          # 4-byte Spill
	testb	$-8, %al
	je	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$16, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movl	%eax, %ebp
	shll	$16, %ebp
	movl	$16, %esi
	movq	%r15, %rdi
	callq	BitBufferRead
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, %r12d
	orl	%ebp, %r12d
.LBB3_24:                               #   in Loop: Header=BB3_3 Depth=1
	testl	%ebx, %ebx
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movl	%r13d, 100(%rsp)        # 4-byte Spill
	jne	.LBB3_49
# BB#25:                                #   in Loop: Header=BB3_3 Depth=1
	movb	56(%rsp), %al           # 1-byte Reload
	shlb	$3, %al
	movq	%r15, %r13
	movzbl	%al, %r15d
	movl	$8, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movl	%eax, 124(%rsp)         # 4-byte Spill
	movl	$8, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movl	%eax, 120(%rsp)         # 4-byte Spill
	movl	$8, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movl	%eax, 148(%rsp)         # 4-byte Spill
	movl	$8, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movl	%eax, %r14d
	andl	$31, %eax
	movl	%eax, 76(%rsp)          # 4-byte Spill
	je	.LBB3_28
# BB#26:                                # %.lr.ph440.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	76(%rsp), %ebp          # 4-byte Reload
	leaq	192(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_27:                               # %.lr.ph440
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movw	%ax, (%rbx)
	addq	$2, %rbx
	decq	%rbp
	jne	.LBB3_27
.LBB3_28:                               # %._crit_edge
                                        #   in Loop: Header=BB3_3 Depth=1
	subl	%r15d, 80(%rsp)         # 4-byte Folded Spill
	shrl	$5, %r14d
	movl	$8, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movl	%eax, 136(%rsp)         # 4-byte Spill
	movl	$8, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movl	%eax, 144(%rsp)         # 4-byte Spill
	andl	$31, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	je	.LBB3_31
# BB#29:                                # %.lr.ph443.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	104(%rsp), %ebp         # 4-byte Reload
	leaq	304(%rsp), %rbx
	.p2align	4, 0x90
.LBB3_30:                               # %.lr.ph443
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %esi
	movq	%r13, %rdi
	callq	BitBufferRead
	movw	%ax, (%rbx)
	addq	$2, %rbx
	decq	%rbp
	jne	.LBB3_30
.LBB3_31:                               # %._crit_edge444
                                        #   in Loop: Header=BB3_3 Depth=1
	incl	80(%rsp)                # 4-byte Folded Spill
	andl	$7, %r14d
	movb	56(%rsp), %cl           # 1-byte Reload
	testb	%cl, %cl
	movq	64(%rsp), %r12          # 8-byte Reload
	je	.LBB3_33
# BB#32:                                #   in Loop: Header=BB3_3 Depth=1
	movq	16(%r13), %rax
	movq	%rax, 176(%rsp)
	movdqu	(%r13), %xmm0
	movdqa	%xmm0, 160(%rsp)
	movl	%ecx, %eax
	shlb	$4, %al
	movzbl	%al, %esi
	imull	%r12d, %esi
	movq	%r13, %rdi
	callq	BitBufferAdvance
.LBB3_33:                               #   in Loop: Header=BB3_3 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
	movzbl	7(%rbx), %esi
	imull	52(%rsp), %r14d         # 4-byte Folded Reload
	shrl	$2, %r14d
	movzbl	8(%rbx), %ecx
	movzwl	10(%rbx), %eax
	movl	%eax, (%rsp)
	leaq	264(%rsp), %rbp
	movq	%rbp, %rdi
	movl	%r14d, %edx
	movl	%r12d, %r8d
	movl	%r12d, %r9d
	callq	set_ag_params
	movq	48(%rbx), %rdx
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%r12d, %ecx
	movl	80(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %r8d
	leaq	108(%rsp), %r9
	callq	dyn_decomp
	movl	%eax, %r8d
	testl	%r8d, %r8d
	jne	.LBB3_157
# BB#34:                                #   in Loop: Header=BB3_3 Depth=1
	movl	144(%rsp), %edx         # 4-byte Reload
	shrl	$5, %edx
	movl	148(%rsp), %r14d        # 4-byte Reload
	movl	%r14d, %eax
	andl	$240, %eax
	andl	$15, %r14d
	andl	$7, %edx
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	je	.LBB3_88
# BB#35:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$0, (%rsp)
	xorl	%ecx, %ecx
	movl	$31, %r8d
	movq	%rdi, %rsi
	movl	%edx, %ebx
	movl	%r12d, %edx
	movl	%r13d, %r9d
	callq	unpc_block
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rsi
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	48(%rax), %rdi
	movl	%r14d, (%rsp)
	jmp	.LBB3_89
.LBB3_36:                               #   in Loop: Header=BB3_3 Depth=1
	movq	%r15, %rsi
	callq	_ZN11ALACDecoder17DataStreamElementEP9BitBuffer
	movl	%eax, %r8d
	jmp	.LBB3_40
.LBB3_37:                               #   in Loop: Header=BB3_3 Depth=1
	movl	$4, %esi
	movq	%r15, %rdi
	callq	BitBufferReadSmall
	movzbl	%al, %ebx
	cmpb	$15, %bl
	jne	.LBB3_39
# BB#38:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$8, %esi
	movq	%r15, %rdi
	callq	BitBufferReadSmall
	movzbl	%al, %eax
	leal	-1(%rbx,%rax), %ebx
.LBB3_39:                               # %_ZN11ALACDecoder11FillElementEP9BitBuffer.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	shll	$3, %ebx
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	BitBufferAdvance
	movq	(%r15), %rax
	cmpq	8(%r15), %rax
	movl	$0, %r8d
	movl	$-50, %eax
	cmoval	%eax, %r8d
.LBB3_40:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%ebp, %eax
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jb	.LBB3_107
	jmp	.LBB3_109
.LBB3_41:                               #   in Loop: Header=BB3_3 Depth=1
	movl	$32, %r12d
	subl	%edx, %r12d
	cmpl	$16, %edx
	ja	.LBB3_45
# BB#42:                                # %.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	testl	%r14d, %r14d
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	je	.LBB3_48
# BB#43:                                # %.lr.ph452
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	64(%rsp), %ebx          # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_44:                               #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	56(%rsp), %esi          # 1-byte Folded Reload
	movq	%r14, %rdi
	callq	BitBufferRead
	movl	%r12d, %ecx
	shll	%cl, %eax
	sarl	%cl, %eax
	movq	32(%r13), %rcx
	movl	%eax, (%rcx,%rbp,4)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_44
	jmp	.LBB3_48
.LBB3_45:                               #   in Loop: Header=BB3_3 Depth=1
	testl	%r14d, %r14d
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	je	.LBB3_48
# BB#46:                                # %.lr.ph450
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	240(%rax), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	64(%rsp), %r15d         # 4-byte Reload
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_47:                               #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %esi
	movq	%r14, %rdi
	callq	BitBufferRead
	movl	%eax, %ebp
	shll	$16, %ebp
	movl	%r12d, %ecx
	sarl	%cl, %ebp
	movzbl	52(%rsp), %esi          # 1-byte Folded Reload
	movq	%r14, %rdi
	callq	BitBufferRead
	orl	%ebp, %eax
	movq	32(%r13), %rcx
	movl	%eax, (%rcx,%rbx,4)
	incq	%rbx
	cmpq	%rbx, %r15
	jne	.LBB3_47
.LBB3_48:                               # %.thread
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	imull	%r12d, %eax
	movl	%eax, 108(%rsp)
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	jmp	.LBB3_60
.LBB3_49:                               #   in Loop: Header=BB3_3 Depth=1
	movzbl	5(%rcx), %eax
	movl	$32, %r13d
	subl	%eax, %r13d
	movl	%eax, 56(%rsp)          # 4-byte Spill
	cmpl	$16, %eax
	ja	.LBB3_74
# BB#50:                                # %.preheader415
                                        #   in Loop: Header=BB3_3 Depth=1
	testl	%r12d, %r12d
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	je	.LBB3_77
# BB#51:                                # %.lr.ph438.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	64(%rsp), %r12d         # 4-byte Reload
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_52:                               # %.lr.ph438
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	movl	56(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	callq	BitBufferRead
	movl	%r13d, %ecx
	shll	%cl, %eax
	sarl	%cl, %eax
	movq	32(%r15), %rcx
	movl	%eax, (%rcx,%rbp,4)
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	BitBufferRead
	movl	%r13d, %ecx
	shll	%cl, %eax
	sarl	%cl, %eax
	movq	40(%r15), %rcx
	movl	%eax, (%rcx,%rbp,4)
	incq	%rbp
	cmpq	%rbp, %r12
	jne	.LBB3_52
	jmp	.LBB3_77
.LBB3_53:                               #   in Loop: Header=BB3_3 Depth=1
	movq	32(%rbx), %rsi
	movl	%ebp, (%rsp)
	movl	%r12d, %edx
	leaq	192(%rsp), %rcx
	movl	80(%rsp), %r8d          # 4-byte Reload
	movq	56(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
.LBB3_54:                               #   in Loop: Header=BB3_3 Depth=1
	callq	unpc_block
	movzbl	%r13b, %r9d
	testb	%r13b, %r13b
	movq	24(%rsp), %r13          # 8-byte Reload
	je	.LBB3_59
# BB#55:                                #   in Loop: Header=BB3_3 Depth=1
	movb	$1, %dl
	testl	%r12d, %r12d
	je	.LBB3_60
# BB#56:                                # %.lr.ph459.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%r9d, 56(%rsp)          # 4-byte Spill
	movl	%r12d, %ebp
	xorl	%ebx, %ebx
	leaq	160(%rsp), %r14
	movl	100(%rsp), %r15d        # 4-byte Reload
	.p2align	4, 0x90
.LBB3_57:                               # %.lr.ph459
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	BitBufferRead
	movq	56(%r13), %rcx
	movw	%ax, (%rcx,%rbx,2)
	incq	%rbx
	cmpq	%rbx, %rbp
	jne	.LBB3_57
# BB#58:                                #   in Loop: Header=BB3_3 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movl	56(%rsp), %r9d          # 4-byte Reload
	movb	$1, %dl
	jmp	.LBB3_60
.LBB3_59:                               #   in Loop: Header=BB3_3 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_60:                               # %.loopexit412
                                        #   in Loop: Header=BB3_3 Depth=1
	movzbl	5(%r13), %eax
	addl	$-16, %eax
	roll	$30, %eax
	cmpl	$4, %eax
	movq	112(%rsp), %r14         # 8-byte Reload
	ja	.LBB3_87
# BB#61:                                # %.loopexit412
                                        #   in Loop: Header=BB3_3 Depth=1
	jmpq	*.LJTI3_2(,%rax,8)
.LBB3_62:                               #   in Loop: Header=BB3_3 Depth=1
	testl	%r12d, %r12d
	je	.LBB3_87
# BB#63:                                # %.lr.ph463
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%r14d, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %r15
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rcx), %r9
	movl	%r12d, %ebx
	cmpl	$8, %r12d
	jb	.LBB3_78
# BB#64:                                # %min.iters.checked
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%r12d, %r8d
	andl	$7, %r8d
	movq	%rbx, %rdi
	subq	%r8, %rdi
	je	.LBB3_78
# BB#65:                                # %vector.scevcheck
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpl	$1, 32(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_78
# BB#66:                                # %vector.body.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%edi, %edx
	imull	32(%rsp), %edx          # 4-byte Folded Reload
	leaq	16(%r9), %rcx
	xorl	%ebp, %ebp
	movq	%rdi, %rsi
	movl	140(%rsp), %r11d        # 4-byte Reload
	.p2align	4, 0x90
.LBB3_67:                               # %vector.body
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%rcx), %xmm0
	movdqu	(%rcx), %xmm1
	movl	%ebp, %r10d
	pshuflw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, (%r15,%r10,2)
	pshuflw	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	movq	%xmm0, 8(%r15,%r10,2)
	addq	$32, %rcx
	addl	%r11d, %ebp
	addq	$-8, %rsi
	jne	.LBB3_67
# BB#68:                                # %middle.block
                                        #   in Loop: Header=BB3_3 Depth=1
	testl	%r8d, %r8d
	movq	64(%rsp), %r12          # 8-byte Reload
	jne	.LBB3_79
	jmp	.LBB3_87
.LBB3_69:                               #   in Loop: Header=BB3_3 Depth=1
	leal	(%r14,%r14,2), %esi
	addq	40(%rsp), %rsi          # 8-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	copyPredictorTo20
	jmp	.LBB3_87
.LBB3_70:                               #   in Loop: Header=BB3_3 Depth=1
	leal	(%r14,%r14,2), %eax
	addq	40(%rsp), %rax          # 8-byte Folded Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rcx), %rdi
	testb	%dl, %dl
	je	.LBB3_85
# BB#71:                                #   in Loop: Header=BB3_3 Depth=1
	movq	56(%rcx), %rsi
	movq	%rax, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r12d, %r8d
	callq	copyPredictorTo24Shift
	jmp	.LBB3_87
.LBB3_72:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%r14d, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rcx), %rdi
	testb	%dl, %dl
	je	.LBB3_86
# BB#73:                                #   in Loop: Header=BB3_3 Depth=1
	movq	56(%rcx), %rsi
	movq	%rax, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r12d, %r8d
	callq	copyPredictorTo32Shift
	jmp	.LBB3_87
.LBB3_74:                               #   in Loop: Header=BB3_3 Depth=1
	testl	%r12d, %r12d
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	je	.LBB3_77
# BB#75:                                # %.lr.ph436.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	56(%rsp), %eax          # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addb	$-16, %al
	movb	%al, 52(%rsp)           # 1-byte Spill
	movl	64(%rsp), %eax          # 4-byte Reload
	movq	%rax, 80(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_76:                               # %.lr.ph436
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %esi
	movq	%r14, %rdi
	callq	BitBufferRead
	movl	%eax, %ebx
	shll	$16, %ebx
	movl	%r13d, %ecx
	sarl	%cl, %ebx
	movzbl	52(%rsp), %r12d         # 1-byte Folded Reload
	movq	%r14, %rdi
	movl	%r12d, %esi
	callq	BitBufferRead
	orl	%ebx, %eax
	movq	32(%r15), %rcx
	movl	%eax, (%rcx,%rbp,4)
	movl	$16, %esi
	movq	%r14, %rdi
	callq	BitBufferRead
	movl	%eax, %ebx
	shll	$16, %ebx
	movl	%r13d, %ecx
	sarl	%cl, %ebx
	movq	%r14, %rdi
	movl	%r12d, %esi
	callq	BitBufferRead
	orl	%ebx, %eax
	movq	40(%r15), %rcx
	movl	%eax, (%rcx,%rbp,4)
	incq	%rbp
	cmpq	%rbp, 80(%rsp)          # 8-byte Folded Reload
	jne	.LBB3_76
.LBB3_77:                               # %.thread403
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movl	56(%rsp), %eax          # 4-byte Reload
	imull	%r12d, %eax
	movl	%eax, 108(%rsp)
	movl	%eax, 156(%rsp)
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	xorl	%r8d, %r8d
	movq	%r15, %rcx
	movq	%r14, %r15
	jmp	.LBB3_100
.LBB3_78:                               #   in Loop: Header=BB3_3 Depth=1
	xorl	%edi, %edi
	xorl	%edx, %edx
.LBB3_79:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%ebx, %ecx
	subl	%edi, %ecx
	leaq	-1(%rbx), %r8
	subq	%rdi, %r8
	andq	$3, %rcx
	je	.LBB3_82
# BB#80:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	negq	%rcx
	.p2align	4, 0x90
.LBB3_81:                               # %scalar.ph.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%r9,%rdi,4), %esi
	movl	%edx, %ebp
	movw	%si, (%r15,%rbp,2)
	incq	%rdi
	addl	32(%rsp), %edx          # 4-byte Folded Reload
	incq	%rcx
	jne	.LBB3_81
.LBB3_82:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$3, %r8
	movq	64(%rsp), %r12          # 8-byte Reload
	jb	.LBB3_87
# BB#83:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB3_3 Depth=1
	subq	%rdi, %rbx
	leaq	12(%r9,%rdi,4), %rsi
	.p2align	4, 0x90
.LBB3_84:                               # %scalar.ph
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	-12(%rsi), %ecx
	movl	%edx, %edi
	movw	%cx, (%r15,%rdi,2)
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	movzwl	-8(%rsi), %edi
	movw	%di, (%r15,%rcx,2)
	addl	%eax, %ecx
	movq	184(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rdx), %edi
	movzwl	-4(%rsi), %ebp
	movw	%bp, (%r15,%rdi,2)
	movq	64(%rsp), %r12          # 8-byte Reload
	addl	%eax, %ecx
	addl	152(%rsp), %edx         # 4-byte Folded Reload
	movzwl	(%rsi), %edi
	movw	%di, (%r15,%rdx,2)
	addl	%eax, %ecx
	addq	$16, %rsi
	addq	$-4, %rbx
	movl	%ecx, %edx
	jne	.LBB3_84
	jmp	.LBB3_87
.LBB3_85:                               #   in Loop: Header=BB3_3 Depth=1
	movq	%rax, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	copyPredictorTo24
	jmp	.LBB3_87
.LBB3_86:                               #   in Loop: Header=BB3_3 Depth=1
	movq	%rax, %rsi
	movq	32(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r12d, %ecx
	callq	copyPredictorTo32
	.p2align	4, 0x90
.LBB3_87:                               # %.loopexit411
                                        #   in Loop: Header=BB3_3 Depth=1
	incl	%r14d
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax)
	movl	%r14d, %eax
	movq	88(%rsp), %r15          # 8-byte Reload
	xorl	%r8d, %r8d
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jb	.LBB3_107
	jmp	.LBB3_109
.LBB3_88:                               #   in Loop: Header=BB3_3 Depth=1
	movq	32(%rbx), %rsi
	movl	%r14d, (%rsp)
	movl	%edx, %ebx
.LBB3_89:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%r12d, %edx
	leaq	192(%rsp), %rcx
	movl	76(%rsp), %r8d          # 4-byte Reload
	movl	%r13d, %r9d
	callq	unpc_block
	movl	%ebx, %edx
	movq	24(%rsp), %rbx          # 8-byte Reload
	movzbl	7(%rbx), %esi
	imull	52(%rsp), %edx          # 4-byte Folded Reload
	shrl	$2, %edx
	movzbl	8(%rbx), %ecx
	movzwl	10(%rbx), %eax
	movl	%eax, (%rsp)
	leaq	264(%rsp), %r14
	movq	%r14, %rdi
	movl	%r12d, %r8d
	movl	%r12d, %r9d
	callq	set_ag_params
	movq	48(%rbx), %rdx
	movq	%r14, %rdi
	movq	88(%rsp), %rsi          # 8-byte Reload
	movl	%r12d, %ecx
	movl	%r13d, %r8d
	leaq	156(%rsp), %r9
	callq	dyn_decomp
	movl	%eax, %r8d
	testl	%r8d, %r8d
	jne	.LBB3_157
# BB#90:                                #   in Loop: Header=BB3_3 Depth=1
	movl	136(%rsp), %r14d        # 4-byte Reload
	movl	%r14d, %eax
	andl	$240, %eax
	andl	$15, %r14d
	movq	48(%rbx), %rdi
	testl	%eax, %eax
	je	.LBB3_92
# BB#91:                                #   in Loop: Header=BB3_3 Depth=1
	movl	$0, (%rsp)
	xorl	%ecx, %ecx
	movl	$31, %r8d
	movq	%rdi, %rsi
	movl	%r12d, %edx
	movl	%r13d, %r9d
	callq	unpc_block
	movq	40(%rbx), %rsi
	movq	48(%rbx), %rdi
	jmp	.LBB3_93
.LBB3_92:                               #   in Loop: Header=BB3_3 Depth=1
	movq	40(%rbx), %rsi
.LBB3_93:                               #   in Loop: Header=BB3_3 Depth=1
	movl	%r14d, (%rsp)
	movl	%r12d, %edx
	leaq	304(%rsp), %rcx
	movl	104(%rsp), %r8d         # 4-byte Reload
	movl	%r13d, %r9d
	callq	unpc_block
	movb	56(%rsp), %al           # 1-byte Reload
	testb	%al, %al
	movzbl	%al, %r13d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	124(%rsp), %ebx         # 4-byte Reload
	movl	120(%rsp), %r8d         # 4-byte Reload
	je	.LBB3_99
# BB#94:                                #   in Loop: Header=BB3_3 Depth=1
	movl	%r12d, %eax
	addl	%eax, %eax
	je	.LBB3_99
# BB#95:                                # %.lr.ph447.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movl	%eax, %r12d
	xorl	%ebp, %ebp
	movq	%rcx, %rbx
	leaq	160(%rsp), %r14
	.p2align	4, 0x90
.LBB3_96:                               # %.lr.ph447
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	BitBufferRead
	movq	56(%rbx), %rcx
	movw	%ax, (%rcx,%rbp,2)
	movq	%r14, %rdi
	movl	%r15d, %esi
	callq	BitBufferRead
	movq	56(%rbx), %rcx
	movw	%ax, 2(%rcx,%rbp,2)
	addq	$2, %rbp
	cmpq	%r12, %rbp
	jb	.LBB3_96
# BB#97:                                #   in Loop: Header=BB3_3 Depth=1
	movq	%rbx, %rcx
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movl	124(%rsp), %ebx         # 4-byte Reload
	movl	120(%rsp), %r8d         # 4-byte Reload
	jmp	.LBB3_100
.LBB3_99:                               #   in Loop: Header=BB3_3 Depth=1
	movq	88(%rsp), %r15          # 8-byte Reload
.LBB3_100:                              # %.loopexit414
                                        #   in Loop: Header=BB3_3 Depth=1
	movzbl	5(%rcx), %eax
	addl	$-16, %eax
	roll	$30, %eax
	cmpl	$4, %eax
	ja	.LBB3_106
# BB#101:                               # %.loopexit414
                                        #   in Loop: Header=BB3_3 Depth=1
	jmpq	*.LJTI3_1(,%rax,8)
.LBB3_102:                              #   in Loop: Header=BB3_3 Depth=1
	movl	112(%rsp), %eax         # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,2), %rdx
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rdi
	movq	40(%rax), %rsi
	movzbl	%bl, %r9d
	movsbl	%r8b, %eax
	movl	%eax, (%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r12d, %r8d
	callq	unmix16
	jmp	.LBB3_106
.LBB3_103:                              #   in Loop: Header=BB3_3 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax,2), %edx
	addq	40(%rsp), %rdx          # 8-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rdi
	movq	40(%rax), %rsi
	movzbl	%bl, %r9d
	movsbl	%r8b, %eax
	movl	%eax, (%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r12d, %r8d
	callq	unmix20
	jmp	.LBB3_106
.LBB3_104:                              #   in Loop: Header=BB3_3 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rax,2), %edx
	addq	40(%rsp), %rdx          # 8-byte Folded Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rcx), %rdi
	movq	40(%rcx), %rsi
	movzbl	%bl, %r9d
	movsbl	%r8b, %eax
	movq	56(%rcx), %rcx
	movl	%r13d, 16(%rsp)
	movq	%rcx, 8(%rsp)
	movl	%eax, (%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r12d, %r8d
	callq	unmix24
	jmp	.LBB3_106
.LBB3_105:                              #   in Loop: Header=BB3_3 Depth=1
	movl	112(%rsp), %eax         # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,4), %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rcx), %rdi
	movq	40(%rcx), %rsi
	movzbl	%bl, %r9d
	movsbl	%r8b, %eax
	movq	56(%rcx), %rcx
	movl	%r13d, 16(%rsp)
	movq	%rcx, 8(%rsp)
	movl	%eax, (%rsp)
	movq	32(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r12d, %r8d
	callq	unmix32
.LBB3_106:                              #   in Loop: Header=BB3_3 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%r12d, (%rax)
	xorl	%r8d, %r8d
	movl	100(%rsp), %eax         # 4-byte Reload
	cmpl	32(%rsp), %eax          # 4-byte Folded Reload
	jae	.LBB3_109
	.p2align	4, 0x90
.LBB3_107:                              #   in Loop: Header=BB3_3 Depth=1
	testl	%r8d, %r8d
	movl	%eax, %ebp
	je	.LBB3_3
	jmp	.LBB3_112
.LBB3_108:
	movl	$-50, %r8d
	jmp	.LBB3_157
.LBB3_109:
	movl	%eax, %ebp
	cmpl	32(%rsp), %ebp          # 4-byte Folded Reload
	jb	.LBB3_113
	jmp	.LBB3_157
.LBB3_110:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	BitBufferByteAlign
	xorl	%r8d, %r8d
	jmp	.LBB3_157
.LBB3_111:
	xorl	%r8d, %r8d
.LBB3_112:                              # %.thread407.preheader
	cmpl	32(%rsp), %ebp          # 4-byte Folded Reload
	jae	.LBB3_157
.LBB3_113:                              # %.lr.ph
	leal	(%r12,%r12,2), %r13d
	movq	%rbp, %rbx
	movl	%ebp, %r15d
	cmpl	$1, 32(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_119
# BB#114:                               # %.lr.ph.split.us.preheader
	movl	%r12d, %eax
	leaq	(%rax,%rax), %r12
	leaq	(,%rax,4), %rbp
	movl	$1, %eax
	subl	%r15d, %eax
	testb	$1, %al
	je	.LBB3_140
# BB#115:                               # %.lr.ph.split.us.prol
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	5(%rax), %al
	cmpb	$16, %al
	je	.LBB3_136
# BB#116:                               # %.lr.ph.split.us.prol
	cmpb	$24, %al
	je	.LBB3_137
# BB#117:                               # %.lr.ph.split.us.prol
	cmpb	$32, %al
	jne	.LBB3_139
# BB#118:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,4), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	jmp	.LBB3_138
.LBB3_119:                              # %.lr.ph.split
	movq	32(%rsp), %rcx          # 8-byte Reload
	imull	%ecx, %r12d
	imull	%ecx, %r13d
	leal	(%rcx,%rcx,2), %eax
	testl	%r12d, %r12d
	movl	%ecx, %ecx
	je	.LBB3_131
	.p2align	4, 0x90
.LBB3_120:                              # %.lr.ph.split.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_124 Depth 2
                                        #     Child Loop BB3_129 Depth 2
                                        #     Child Loop BB3_126 Depth 2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movb	5(%rdx), %dl
	cmpb	$32, %dl
	je	.LBB3_125
# BB#121:                               # %.lr.ph.split.split
                                        #   in Loop: Header=BB3_120 Depth=1
	cmpb	$24, %dl
	je	.LBB3_127
# BB#122:                               # %.lr.ph.split.split
                                        #   in Loop: Header=BB3_120 Depth=1
	cmpb	$16, %dl
	jne	.LBB3_130
# BB#123:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB3_120 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r15,2), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_124:                              # %.lr.ph.i
                                        #   Parent Loop BB3_120 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edi
	movw	$0, (%rdx,%rdi,2)
	addl	32(%rsp), %esi          # 4-byte Folded Reload
	cmpl	%r12d, %esi
	jb	.LBB3_124
	jmp	.LBB3_130
	.p2align	4, 0x90
.LBB3_125:                              # %.preheader.i399
                                        #   in Loop: Header=BB3_120 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r15,4), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_126:                              # %.lr.ph.i401
                                        #   Parent Loop BB3_120 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edi
	movl	$0, (%rdx,%rdi,4)
	addl	32(%rsp), %esi          # 4-byte Folded Reload
	cmpl	%r12d, %esi
	jb	.LBB3_126
	jmp	.LBB3_130
	.p2align	4, 0x90
.LBB3_127:                              # %.preheader.i397
                                        #   in Loop: Header=BB3_120 Depth=1
	testl	%r13d, %r13d
	je	.LBB3_130
# BB#128:                               # %.lr.ph.i398.preheader
                                        #   in Loop: Header=BB3_120 Depth=1
	leal	(%r15,%r15,2), %edx
	addq	40(%rsp), %rdx          # 8-byte Folded Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_129:                              # %.lr.ph.i398
                                        #   Parent Loop BB3_120 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edi
	movb	$0, (%rdx,%rdi)
	leal	1(%rsi), %edi
	movb	$0, (%rdx,%rdi)
	leal	2(%rsi), %edi
	movb	$0, (%rdx,%rdi)
	addl	%eax, %esi
	cmpl	%r13d, %esi
	jb	.LBB3_129
	.p2align	4, 0x90
.LBB3_130:                              # %_ZL6Zero16Psjj.exit
                                        #   in Loop: Header=BB3_120 Depth=1
	incq	%r15
	cmpq	%rcx, %r15
	jne	.LBB3_120
	jmp	.LBB3_157
	.p2align	4, 0x90
.LBB3_131:                              # %.lr.ph.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_134 Depth 2
	movq	24(%rsp), %rdx          # 8-byte Reload
	cmpb	$24, 5(%rdx)
	jne	.LBB3_135
# BB#132:                               # %.preheader.i397.us423
                                        #   in Loop: Header=BB3_131 Depth=1
	testl	%r13d, %r13d
	je	.LBB3_135
# BB#133:                               # %.lr.ph.i398.us424.preheader
                                        #   in Loop: Header=BB3_131 Depth=1
	leal	(%r15,%r15,2), %edx
	addq	40(%rsp), %rdx          # 8-byte Folded Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_134:                              # %.lr.ph.i398.us424
                                        #   Parent Loop BB3_131 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%esi, %edi
	movb	$0, (%rdx,%rdi)
	leal	1(%rsi), %edi
	movb	$0, (%rdx,%rdi)
	leal	2(%rsi), %edi
	movb	$0, (%rdx,%rdi)
	addl	%eax, %esi
	cmpl	%r13d, %esi
	jb	.LBB3_134
.LBB3_135:                              # %_ZL6Zero16Psjj.exit.us429
                                        #   in Loop: Header=BB3_131 Depth=1
	incq	%r15
	cmpq	%rcx, %r15
	jne	.LBB3_131
	jmp	.LBB3_157
.LBB3_136:
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15,2), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	jmp	.LBB3_138
.LBB3_137:
	leal	(%r15,%r15,2), %edi
	addq	40(%rsp), %rdi          # 8-byte Folded Reload
	xorl	%esi, %esi
	movq	%r13, %rdx
.LBB3_138:                              # %_ZL6Zero16Psjj.exit.us.prol
	movl	%r8d, %r14d
	callq	memset
	movl	%r14d, %r8d
.LBB3_139:                              # %_ZL6Zero16Psjj.exit.us.prol
	incq	%r15
.LBB3_140:                              # %.lr.ph.split.us.prol.loopexit
	testl	%ebx, %ebx
	je	.LBB3_157
# BB#141:                               # %.lr.ph.split.us.preheader.new
	leaq	(%r15,%r15,2), %rbx
	leaq	(%r15,%r15), %r14
	leaq	(,%r15,4), %r15
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB3_143
	.p2align	4, 0x90
.LBB3_142:                              # %_ZL6Zero16Psjj.exit.us.1
                                        #   in Loop: Header=BB3_143 Depth=1
	addq	$6, %rbx
	addq	$4, %r14
	addq	$8, %r15
.LBB3_143:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzbl	5(%rcx), %eax
	cmpb	$16, %al
	je	.LBB3_147
# BB#144:                               # %.lr.ph.split.us
                                        #   in Loop: Header=BB3_143 Depth=1
	cmpb	$24, %al
	je	.LBB3_148
# BB#145:                               # %.lr.ph.split.us
                                        #   in Loop: Header=BB3_143 Depth=1
	cmpb	$32, %al
	jne	.LBB3_150
# BB#146:                               #   in Loop: Header=BB3_143 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r15), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	jmp	.LBB3_149
	.p2align	4, 0x90
.LBB3_147:                              #   in Loop: Header=BB3_143 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	jmp	.LBB3_149
	.p2align	4, 0x90
.LBB3_148:                              #   in Loop: Header=BB3_143 Depth=1
	movl	%ebx, %edi
	addq	40(%rsp), %rdi          # 8-byte Folded Reload
	xorl	%esi, %esi
	movq	%r13, %rdx
.LBB3_149:                              # %_ZL6Zero16Psjj.exit.us
                                        #   in Loop: Header=BB3_143 Depth=1
	callq	memset
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB3_150:                              # %_ZL6Zero16Psjj.exit.us
                                        #   in Loop: Header=BB3_143 Depth=1
	movzbl	5(%rcx), %eax
	cmpb	$16, %al
	je	.LBB3_154
# BB#151:                               # %_ZL6Zero16Psjj.exit.us
                                        #   in Loop: Header=BB3_143 Depth=1
	cmpb	$24, %al
	je	.LBB3_155
# BB#152:                               # %_ZL6Zero16Psjj.exit.us
                                        #   in Loop: Header=BB3_143 Depth=1
	cmpb	$32, %al
	jne	.LBB3_142
# BB#153:                               #   in Loop: Header=BB3_143 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	4(%rax,%r15), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	jmp	.LBB3_156
	.p2align	4, 0x90
.LBB3_154:                              #   in Loop: Header=BB3_143 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	2(%rax,%r14), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	jmp	.LBB3_156
	.p2align	4, 0x90
.LBB3_155:                              #   in Loop: Header=BB3_143 Depth=1
	leal	3(%rbx), %edi
	addq	40(%rsp), %rdi          # 8-byte Folded Reload
	xorl	%esi, %esi
	movq	%r13, %rdx
.LBB3_156:                              # %_ZL6Zero16Psjj.exit.us.1
                                        #   in Loop: Header=BB3_143 Depth=1
	callq	memset
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB3_142
.LBB3_157:                              # %.loopexit
	movl	%r8d, %eax
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN11ALACDecoder6DecodeEP9BitBufferPhjjPj, .Lfunc_end3-_ZN11ALACDecoder6DecodeEP9BitBufferPhjjPj
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI3_0:
	.quad	.LBB3_6
	.quad	.LBB3_19
	.quad	.LBB3_112
	.quad	.LBB3_6
	.quad	.LBB3_36
	.quad	.LBB3_112
	.quad	.LBB3_37
	.quad	.LBB3_110
.LJTI3_1:
	.quad	.LBB3_102
	.quad	.LBB3_103
	.quad	.LBB3_104
	.quad	.LBB3_106
	.quad	.LBB3_105
.LJTI3_2:
	.quad	.LBB3_62
	.quad	.LBB3_69
	.quad	.LBB3_70
	.quad	.LBB3_87
	.quad	.LBB3_72

	.text
	.globl	_ZN11ALACDecoder17DataStreamElementEP9BitBuffer
	.p2align	4, 0x90
	.type	_ZN11ALACDecoder17DataStreamElementEP9BitBuffer,@function
_ZN11ALACDecoder17DataStreamElementEP9BitBuffer: # @_ZN11ALACDecoder17DataStreamElementEP9BitBuffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi28:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 32
.Lcfi31:
	.cfi_offset %rbx, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	$4, %esi
	movq	%rbx, %rdi
	callq	BitBufferReadSmall
	movq	%rbx, %rdi
	callq	BitBufferReadOne
	movl	%eax, %r14d
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	BitBufferReadSmall
	movzbl	%al, %ebp
	cmpb	$-1, %bpl
	jne	.LBB4_2
# BB#1:
	movl	$8, %esi
	movq	%rbx, %rdi
	callq	BitBufferReadSmall
	movzbl	%al, %eax
	addl	%eax, %ebp
.LBB4_2:
	testb	%r14b, %r14b
	je	.LBB4_4
# BB#3:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	BitBufferByteAlign
.LBB4_4:
	shll	$3, %ebp
	andl	$524280, %ebp           # imm = 0x7FFF8
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	BitBufferAdvance
	movq	(%rbx), %rax
	xorl	%ecx, %ecx
	cmpq	8(%rbx), %rax
	movl	$-50, %eax
	cmovbel	%ecx, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN11ALACDecoder17DataStreamElementEP9BitBuffer, .Lfunc_end4-_ZN11ALACDecoder17DataStreamElementEP9BitBuffer
	.cfi_endproc

	.globl	_ZN11ALACDecoder11FillElementEP9BitBuffer
	.p2align	4, 0x90
	.type	_ZN11ALACDecoder11FillElementEP9BitBuffer,@function
_ZN11ALACDecoder11FillElementEP9BitBuffer: # @_ZN11ALACDecoder11FillElementEP9BitBuffer
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movl	$4, %esi
	movq	%r14, %rdi
	callq	BitBufferReadSmall
	movzbl	%al, %ebx
	cmpb	$15, %bl
	jne	.LBB5_2
# BB#1:
	movl	$8, %esi
	movq	%r14, %rdi
	callq	BitBufferReadSmall
	movzbl	%al, %eax
	leal	-1(%rbx,%rax), %ebx
.LBB5_2:
	shll	$3, %ebx
	movq	%r14, %rdi
	movl	%ebx, %esi
	callq	BitBufferAdvance
	movq	(%r14), %rax
	xorl	%ecx, %ecx
	cmpq	8(%r14), %rax
	movl	$-50, %eax
	cmovbel	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	_ZN11ALACDecoder11FillElementEP9BitBuffer, .Lfunc_end5-_ZN11ALACDecoder11FillElementEP9BitBuffer
	.cfi_endproc


	.globl	_ZN11ALACDecoderC1Ev
	.type	_ZN11ALACDecoderC1Ev,@function
_ZN11ALACDecoderC1Ev = _ZN11ALACDecoderC2Ev
	.globl	_ZN11ALACDecoderD1Ev
	.type	_ZN11ALACDecoderD1Ev,@function
_ZN11ALACDecoderD1Ev = _ZN11ALACDecoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
