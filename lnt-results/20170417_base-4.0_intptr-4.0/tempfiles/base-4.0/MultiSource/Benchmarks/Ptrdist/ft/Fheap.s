	.text
	.file	"Fheap.bc"
	.globl	InitFHeap
	.p2align	4, 0x90
	.type	InitFHeap,@function
InitFHeap:                              # @InitFHeap
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$hTable, %edi
	xorl	%esi, %esi
	movl	$80000, %edx            # imm = 0x13880
	callq	memset
	popq	%rax
	retq
.Lfunc_end0:
	.size	InitFHeap, .Lfunc_end0-InitFHeap
	.cfi_endproc

	.globl	MakeHeap
	.p2align	4, 0x90
	.type	MakeHeap,@function
MakeHeap:                               # @MakeHeap
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	MakeHeap, .Lfunc_end1-MakeHeap
	.cfi_endproc

	.globl	FindMin
	.p2align	4, 0x90
	.type	FindMin,@function
FindMin:                                # @FindMin
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB2_1
# BB#2:
	movq	(%rdi), %rax
	retq
.LBB2_1:
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	FindMin, .Lfunc_end2-FindMin
	.cfi_endproc

	.globl	Insert
	.p2align	4, 0x90
	.type	Insert,@function
Insert:                                 # @Insert
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 48
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r12, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_5
# BB#1:                                 # %NewHeap.exit
	movq	%r15, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 32(%rbx)
	movl	$0, 40(%rbx)
	movw	$0, 44(%rbx)
	movq	(%r14), %r12
	testq	%r12, %r12
	je	.LBB3_2
# BB#3:
	movq	24(%r12), %rax
	movq	%rbx, 32(%rax)
	movq	%r12, 32(%rbx)
	movq	%rbx, 24(%r12)
	movq	%rax, 24(%rbx)
	movq	(%r12), %rdi
	movq	%r15, %rsi
	callq	LessThan
	testl	%eax, %eax
	movq	%rbx, %rax
	cmovneq	%r12, %rax
	jmp	.LBB3_4
.LBB3_2:
	movq	%rbx, %rax
.LBB3_4:                                # %Meld.exit
	movq	%rax, (%r14)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB3_5:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	Insert, .Lfunc_end3-Insert
	.cfi_endproc

	.globl	NewHeap
	.p2align	4, 0x90
	.type	NewHeap,@function
NewHeap:                                # @NewHeap
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$48, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB4_2
# BB#1:
	movq	%rbx, (%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 32(%rax)
	movl	$0, 40(%rax)
	movw	$0, 44(%rax)
	popq	%rbx
	retq
.LBB4_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$23, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end4:
	.size	NewHeap, .Lfunc_end4-NewHeap
	.cfi_endproc

	.globl	Meld
	.p2align	4, 0x90
	.type	Meld,@function
Meld:                                   # @Meld
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -24
.Lcfi16:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB5_4
# BB#1:
	testq	%rbx, %rbx
	je	.LBB5_2
# BB#3:
	movq	24(%rbx), %rax
	movq	%r14, 32(%rax)
	movq	24(%r14), %rcx
	movq	%rbx, 32(%rcx)
	movq	%rcx, 24(%rbx)
	movq	%rax, 24(%r14)
	movq	(%rbx), %rdi
	movq	(%r14), %rsi
	callq	LessThan
	testl	%eax, %eax
	cmoveq	%r14, %rbx
	jmp	.LBB5_4
.LBB5_2:
	movq	%r14, %rbx
.LBB5_4:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end5:
	.size	Meld, .Lfunc_end5-Meld
	.cfi_endproc

	.globl	CombineLists
	.p2align	4, 0x90
	.type	CombineLists,@function
CombineLists:                           # @CombineLists
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	movq	%rsi, 32(%rax)
	movq	24(%rsi), %rcx
	movq	%rdi, 32(%rcx)
	movq	%rcx, 24(%rdi)
	movq	%rax, 24(%rsi)
	retq
.Lfunc_end6:
	.size	CombineLists, .Lfunc_end6-CombineLists
	.cfi_endproc

	.globl	DeleteMin
	.p2align	4, 0x90
	.type	DeleteMin,@function
DeleteMin:                              # @DeleteMin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	testq	%rdi, %rdi
	je	.LBB7_1
# BB#2:
	movq	24(%rdi), %rcx
	cmpq	%rdi, %rcx
	je	.LBB7_3
# BB#4:
	leaq	24(%rdi), %rax
	movq	32(%rdi), %rdx
	movq	%rdx, 32(%rcx)
	movq	32(%rdi), %rdx
	movq	%rcx, 24(%rdx)
	jmp	.LBB7_5
.LBB7_1:
	xorl	%r14d, %r14d
	jmp	.LBB7_50
.LBB7_3:
	leaq	16(%rdi), %rax
.LBB7_5:                                # %RemoveEntry.exit
	movq	(%rax), %r14
	testq	%r14, %r14
	je	.LBB7_6
# BB#7:
	cmpq	16(%rdi), %r14
	jne	.LBB7_9
# BB#8:
	movq	$0, 16(%rdi)
.LBB7_9:                                # %.preheader112.preheader
	movq	%rdi, (%rsp)            # 8-byte Spill
	xorl	%r13d, %r13d
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB7_10:                               # %.preheader112
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_12 Depth 2
	movq	%r12, %rbx
	movq	24(%rbx), %r12
	movq	%rbx, 24(%rbx)
	movq	%rbx, 32(%rbx)
	movq	$0, 8(%rbx)
	movslq	40(%rbx), %rax
	leaq	hTable(,%rax,8), %rbp
	movq	hTable(,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB7_22
# BB#11:                                # %.lr.ph145.preheader
                                        #   in Loop: Header=BB7_10 Depth=1
	leaq	40(%rbx), %r15
	.p2align	4, 0x90
.LBB7_12:                               #   Parent Loop BB7_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	movq	(%rbx), %rsi
	callq	LessThan
	testl	%eax, %eax
	movq	(%rbp), %rax
	jne	.LBB7_13
# BB#17:                                #   in Loop: Header=BB7_12 Depth=2
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB7_18
# BB#19:                                #   in Loop: Header=BB7_12 Depth=2
	movq	24(%rcx), %rdx
	movq	%rax, 32(%rdx)
	movq	24(%rax), %rsi
	movq	%rcx, 32(%rsi)
	movq	%rsi, 24(%rcx)
	movq	%rdx, 24(%rax)
	jmp	.LBB7_20
	.p2align	4, 0x90
.LBB7_18:                               #   in Loop: Header=BB7_12 Depth=2
	movq	%rax, 16(%rbx)
.LBB7_20:                               # %AddEntry.exit109
                                        #   in Loop: Header=BB7_12 Depth=2
	movq	%rbx, 8(%rax)
	movl	(%r15), %ecx
	movl	40(%rax), %eax
	leal	1(%rcx,%rax), %eax
	movl	%eax, (%r15)
	movq	$0, (%rbp)
	movslq	%eax, %rcx
	leaq	hTable(,%rcx,8), %rbp
	movq	hTable(,%rcx,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_12
	jmp	.LBB7_22
.LBB7_13:                               #   in Loop: Header=BB7_12 Depth=2
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB7_14
# BB#15:                                #   in Loop: Header=BB7_12 Depth=2
	movq	24(%rcx), %rdx
	movq	%rbx, 32(%rdx)
	movq	24(%rbx), %rsi
	movq	%rcx, 32(%rsi)
	movq	%rsi, 24(%rcx)
	movq	%rdx, 24(%rbx)
	jmp	.LBB7_16
.LBB7_14:                               #   in Loop: Header=BB7_12 Depth=2
	movq	%rbx, 16(%rax)
.LBB7_16:                               # %AddEntry.exit110
                                        #   in Loop: Header=BB7_12 Depth=2
	movq	%rax, 8(%rbx)
	movl	40(%rax), %ecx
	movl	(%r15), %edx
	leal	1(%rcx,%rdx), %ecx
	movl	%ecx, 40(%rax)
	movq	(%rbp), %rbx
	movq	$0, (%rbp)
	leaq	40(%rbx), %r15
	movslq	40(%rbx), %rax
	leaq	hTable(,%rax,8), %rbp
	movq	hTable(,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_12
	.p2align	4, 0x90
.LBB7_22:                               # %.outer111._crit_edge
                                        #   in Loop: Header=BB7_10 Depth=1
	movq	%rbx, (%rbp)
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	cmpq	%r14, %r12
	jne	.LBB7_10
# BB#23:
	movq	(%rsp), %r12            # 8-byte Reload
	movq	16(%r12), %r14
	testq	%r14, %r14
	je	.LBB7_37
	.p2align	4, 0x90
.LBB7_24:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_26 Depth 2
	movq	%r14, %rbx
	movq	24(%rbx), %r14
	movq	%rbx, 24(%rbx)
	movq	%rbx, 32(%rbx)
	movq	$0, 8(%rbx)
	movslq	40(%rbx), %rax
	leaq	hTable(,%rax,8), %rbp
	movq	hTable(,%rax,8), %rcx
	testq	%rcx, %rcx
	je	.LBB7_36
# BB#25:                                # %.lr.ph129.preheader
                                        #   in Loop: Header=BB7_24 Depth=1
	leaq	40(%rbx), %r15
	.p2align	4, 0x90
.LBB7_26:                               #   Parent Loop BB7_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdi
	movq	(%rbx), %rsi
	callq	LessThan
	testl	%eax, %eax
	movq	(%rbp), %rax
	jne	.LBB7_27
# BB#31:                                #   in Loop: Header=BB7_26 Depth=2
	movq	16(%rbx), %rcx
	testq	%rcx, %rcx
	je	.LBB7_32
# BB#33:                                #   in Loop: Header=BB7_26 Depth=2
	movq	24(%rcx), %rdx
	movq	%rax, 32(%rdx)
	movq	24(%rax), %rsi
	movq	%rcx, 32(%rsi)
	movq	%rsi, 24(%rcx)
	movq	%rdx, 24(%rax)
	jmp	.LBB7_34
	.p2align	4, 0x90
.LBB7_32:                               #   in Loop: Header=BB7_26 Depth=2
	movq	%rax, 16(%rbx)
.LBB7_34:                               # %AddEntry.exit
                                        #   in Loop: Header=BB7_26 Depth=2
	movq	%rbx, 8(%rax)
	movl	(%r15), %ecx
	movl	40(%rax), %eax
	leal	1(%rcx,%rax), %eax
	movl	%eax, (%r15)
	movq	$0, (%rbp)
	movslq	%eax, %rcx
	leaq	hTable(,%rcx,8), %rbp
	movq	hTable(,%rcx,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_26
	jmp	.LBB7_36
.LBB7_27:                               #   in Loop: Header=BB7_26 Depth=2
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB7_28
# BB#29:                                #   in Loop: Header=BB7_26 Depth=2
	movq	24(%rcx), %rdx
	movq	%rbx, 32(%rdx)
	movq	24(%rbx), %rsi
	movq	%rcx, 32(%rsi)
	movq	%rsi, 24(%rcx)
	movq	%rdx, 24(%rbx)
	jmp	.LBB7_30
.LBB7_28:                               #   in Loop: Header=BB7_26 Depth=2
	movq	%rbx, 16(%rax)
.LBB7_30:                               # %AddEntry.exit108
                                        #   in Loop: Header=BB7_26 Depth=2
	movq	%rax, 8(%rbx)
	movl	40(%rax), %ecx
	movl	(%r15), %edx
	leal	1(%rcx,%rdx), %ecx
	movl	%ecx, 40(%rax)
	movq	(%rbp), %rbx
	movq	$0, (%rbp)
	leaq	40(%rbx), %r15
	movslq	40(%rbx), %rax
	leaq	hTable(,%rax,8), %rbp
	movq	hTable(,%rax,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB7_26
	.p2align	4, 0x90
.LBB7_36:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB7_24 Depth=1
	movq	%rbx, (%rbp)
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	cmpq	16(%r12), %r14
	jne	.LBB7_24
.LBB7_37:                               # %.loopexit
	testl	%r13d, %r13d
	movslq	%r13d, %r12
	js	.LBB7_38
# BB#39:                                # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_40:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, hTable(,%rax,8)
	jne	.LBB7_42
# BB#41:                                #   in Loop: Header=BB7_40 Depth=1
	leaq	1(%rax), %rbp
	cmpq	%r12, %rax
	movq	%rbp, %rax
	jl	.LBB7_40
	jmp	.LBB7_43
.LBB7_6:
	callq	free
	xorl	%r14d, %r14d
	jmp	.LBB7_50
.LBB7_38:
	xorl	%ebp, %ebp
	jmp	.LBB7_43
.LBB7_42:                               # %.lr.ph.._crit_edge.loopexit_crit_edge
	movl	%eax, %ebp
.LBB7_43:                               # %._crit_edge
	movslq	%ebp, %rax
	leaq	hTable(,%rax,8), %rbx
	movq	hTable(,%rax,8), %r15
	movq	%r15, %r14
	jmp	.LBB7_44
	.p2align	4, 0x90
.LBB7_47:                               #   in Loop: Header=BB7_44 Depth=1
	movq	24(%r15), %rcx
	movq	%rax, 32(%rcx)
	movq	24(%rax), %rdx
	movq	%r15, 32(%rdx)
	movq	%rdx, 24(%r15)
	movq	%rcx, 24(%rax)
	movq	(%rbx), %rax
	movq	(%rax), %rdi
	movq	(%r14), %rsi
	callq	LessThan
	testl	%eax, %eax
	je	.LBB7_44
# BB#48:                                #   in Loop: Header=BB7_44 Depth=1
	movq	(%rbx), %r14
	.p2align	4, 0x90
.LBB7_44:                               # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_45 Depth 2
	movq	$0, (%rbx)
	movslq	%ebp, %rbp
	leaq	hTable(,%rbp,8), %rbx
	.p2align	4, 0x90
.LBB7_45:                               #   Parent Loop BB7_44 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r12, %rbp
	jge	.LBB7_49
# BB#46:                                #   in Loop: Header=BB7_45 Depth=2
	incq	%rbp
	movq	8(%rbx), %rax
	addq	$8, %rbx
	testq	%rax, %rax
	je	.LBB7_45
	jmp	.LBB7_47
.LBB7_49:
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	free
.LBB7_50:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	DeleteMin, .Lfunc_end7-DeleteMin
	.cfi_endproc

	.globl	RemoveEntry
	.p2align	4, 0x90
	.type	RemoveEntry,@function
RemoveEntry:                            # @RemoveEntry
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	cmpq	%rdi, %rax
	je	.LBB8_1
# BB#2:
	movq	32(%rdi), %rcx
	movq	%rcx, 32(%rax)
	movq	32(%rdi), %rcx
	leaq	24(%rdi), %rdi
	movq	%rax, 24(%rcx)
	movq	(%rdi), %rax
	retq
.LBB8_1:
	addq	$16, %rdi
	movq	(%rdi), %rax
	retq
.Lfunc_end8:
	.size	RemoveEntry, .Lfunc_end8-RemoveEntry
	.cfi_endproc

	.globl	AddEntry
	.p2align	4, 0x90
	.type	AddEntry,@function
AddEntry:                               # @AddEntry
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB9_1
# BB#2:
	movq	24(%rax), %rcx
	movq	%rsi, 32(%rcx)
	movq	24(%rsi), %rdx
	movq	%rax, 32(%rdx)
	movq	%rdx, 24(%rax)
	movq	%rcx, 24(%rsi)
	jmp	.LBB9_3
.LBB9_1:
	movq	%rsi, 16(%rdi)
.LBB9_3:
	movq	%rdi, 8(%rsi)
	movl	40(%rdi), %eax
	movl	40(%rsi), %ecx
	leal	1(%rax,%rcx), %eax
	movl	%eax, 40(%rdi)
	retq
.Lfunc_end9:
	.size	AddEntry, .Lfunc_end9-AddEntry
	.cfi_endproc

	.globl	DecreaseKey
	.p2align	4, 0x90
	.type	DecreaseKey,@function
DecreaseKey:                            # @DecreaseKey
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 32
.Lcfi33:
	.cfi_offset %rbx, -24
.Lcfi34:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB10_8
# BB#1:
	movq	24(%rbx), %rcx
	cmpq	%rbx, 16(%rax)
	jne	.LBB10_3
# BB#2:
	xorl	%esi, %esi
	cmpq	%rbx, %rcx
	cmovneq	%rcx, %rsi
	movq	%rsi, 16(%rax)
.LBB10_3:                               # %._crit_edge.i
	cmpq	%rbx, %rcx
	je	.LBB10_5
# BB#4:
	movq	32(%rbx), %rsi
	movq	%rsi, 32(%rcx)
	movq	32(%rbx), %rsi
	movq	%rcx, 24(%rsi)
.LBB10_5:                               # %RemoveEntry.exit.i
	movl	40(%rbx), %ecx
	notl	%ecx
	.p2align	4, 0x90
.LBB10_6:                               # =>This Inner Loop Header: Depth=1
	addl	%ecx, 40(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB10_6
# BB#7:                                 # %RemoveChild.exit
	movq	%rbx, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	24(%r14), %rax
	movq	%rbx, 32(%rax)
	movq	%r14, 32(%rbx)
	movq	%rbx, 24(%r14)
	movq	%rax, 24(%rbx)
.LBB10_8:
	movq	(%rbx), %rdi
	movl	%edx, %esi
	callq	Subtract
	movq	%rax, (%rbx)
	movq	(%r14), %rsi
	movq	%rax, %rdi
	callq	LessThan
	testl	%eax, %eax
	cmoveq	%r14, %rbx
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	DecreaseKey, .Lfunc_end10-DecreaseKey
	.cfi_endproc

	.globl	RemoveChild
	.p2align	4, 0x90
	.type	RemoveChild,@function
RemoveChild:                            # @RemoveChild
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	movq	24(%rdi), %rcx
	cmpq	%rdi, 16(%rax)
	jne	.LBB11_2
# BB#1:
	xorl	%edx, %edx
	cmpq	%rdi, %rcx
	cmovneq	%rcx, %rdx
	movq	%rdx, 16(%rax)
.LBB11_2:                               # %._crit_edge
	cmpq	%rdi, %rcx
	je	.LBB11_4
# BB#3:
	movq	32(%rdi), %rdx
	movq	%rdx, 32(%rcx)
	movq	32(%rdi), %rdx
	movq	%rcx, 24(%rdx)
.LBB11_4:                               # %RemoveEntry.exit
	movl	40(%rdi), %ecx
	notl	%ecx
	.p2align	4, 0x90
.LBB11_5:                               # =>This Inner Loop Header: Depth=1
	addl	%ecx, 40(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB11_5
# BB#6:                                 # %FixRank.exit
	movq	%rdi, 24(%rdi)
	movq	%rdi, 32(%rdi)
	movq	$0, 8(%rdi)
	retq
.Lfunc_end11:
	.size	RemoveChild, .Lfunc_end11-RemoveChild
	.cfi_endproc

	.globl	FixRank
	.p2align	4, 0x90
	.type	FixRank,@function
FixRank:                                # @FixRank
	.cfi_startproc
# BB#0:
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	subl	%esi, 40(%rdi)
	movq	8(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB12_1
# BB#2:
	retq
.Lfunc_end12:
	.size	FixRank, .Lfunc_end12-FixRank
	.cfi_endproc

	.globl	Delete
	.p2align	4, 0x90
	.type	Delete,@function
Delete:                                 # @Delete
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r12, -32
.Lcfi42:
	.cfi_offset %r14, -24
.Lcfi43:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	%r14, %r15
	je	.LBB13_14
# BB#1:
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB13_2
# BB#4:
	movq	24(%r14), %rcx
	cmpq	%r14, 16(%rax)
	jne	.LBB13_6
# BB#5:
	xorl	%edx, %edx
	cmpq	%r14, %rcx
	cmovneq	%rcx, %rdx
	movq	%rdx, 16(%rax)
.LBB13_6:                               # %._crit_edge.i
	cmpq	%r14, %rcx
	je	.LBB13_8
# BB#7:
	movq	32(%r14), %rdx
	movq	%rdx, 32(%rcx)
	movq	32(%r14), %rdx
	movq	%rcx, 24(%rdx)
.LBB13_8:                               # %RemoveEntry.exit.i
	movl	40(%r14), %ecx
	notl	%ecx
	.p2align	4, 0x90
.LBB13_9:                               # =>This Inner Loop Header: Depth=1
	addl	%ecx, 40(%rax)
	movq	8(%rax), %rax
	testq	%rax, %rax
	jne	.LBB13_9
# BB#10:                                # %RemoveChild.exit
	movq	%r14, 24(%r14)
	movq	%r14, 32(%r14)
	movq	$0, 8(%r14)
	jmp	.LBB13_11
.LBB13_14:
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	DeleteMin               # TAILCALL
.LBB13_2:
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB13_11
# BB#3:
	movq	32(%r14), %rcx
	movq	%rcx, 32(%rax)
	movq	32(%r14), %rcx
	movq	%rax, 24(%rcx)
.LBB13_11:                              # %RemoveEntry.exit
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB13_13
	.p2align	4, 0x90
.LBB13_12:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %r12
	movq	%rbx, 24(%rbx)
	movq	$0, 8(%rbx)
	movq	24(%r15), %rax
	movq	%rbx, 32(%rax)
	movq	%r15, 32(%rbx)
	movq	%rbx, 24(%r15)
	movq	%rax, 24(%rbx)
	movq	(%rbx), %rdi
	movq	(%r15), %rsi
	callq	LessThan
	testl	%eax, %eax
	cmovneq	%rbx, %r15
	cmpq	16(%r14), %r12
	movq	%r12, %rbx
	jne	.LBB13_12
.LBB13_13:
	movq	%r14, %rdi
	callq	free
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	Delete, .Lfunc_end13-Delete
	.cfi_endproc

	.globl	ItemOf
	.p2align	4, 0x90
	.type	ItemOf,@function
ItemOf:                                 # @ItemOf
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	retq
.Lfunc_end14:
	.size	ItemOf, .Lfunc_end14-ItemOf
	.cfi_endproc

	.globl	Find
	.p2align	4, 0x90
	.type	Find,@function
Find:                                   # @Find
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB15_7
# BB#1:                                 # %.preheader.preheader
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB15_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	callq	Equal
	testl	%eax, %eax
	jne	.LBB15_8
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	callq	LessThan
	testl	%eax, %eax
	je	.LBB15_6
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	16(%rbx), %rdi
	movq	%r14, %rsi
	callq	Find
	testq	%rax, %rax
	jne	.LBB15_5
.LBB15_6:                               #   in Loop: Header=BB15_2 Depth=1
	movq	24(%rbx), %rbx
	cmpq	%r15, %rbx
	jne	.LBB15_2
.LBB15_7:
	xorl	%ebx, %ebx
	jmp	.LBB15_8
.LBB15_5:
	movq	%rax, %rbx
.LBB15_8:                               # %.loopexit
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	Find, .Lfunc_end15-Find
	.cfi_endproc

	.type	hTable,@object          # @hTable
	.local	hTable
	.comm	hTable,80000,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Oops, could not malloc\n"
	.size	.L.str, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
