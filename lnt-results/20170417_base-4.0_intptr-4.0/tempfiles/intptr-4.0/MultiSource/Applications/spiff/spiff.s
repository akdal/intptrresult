	.text
	.file	"spiff.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi6:
	.cfi_def_cfa_offset 1104
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r12d
	movl	$-1, %ebp
	cmpl	$2, %r12d
	jl	.LBB0_52
# BB#1:                                 # %.lr.ph54.i
	movl	$-1, %ebp
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_45 Depth 2
                                        #       Child Loop BB0_48 Depth 3
                                        #     Child Loop BB0_38 Depth 2
	movq	8(%r14), %r15
	cmpb	$45, (%r15)
	jne	.LBB0_51
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movsbl	1(%r15), %eax
	addl	$-48, %eax
	cmpl	$71, %eax
	ja	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	leaq	8(%r14), %r13
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	incq	%r15
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movq	%rax, %rbp
	jmp	.LBB0_50
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	jmp	.LBB0_50
.LBB0_7:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.12, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.16, %edi
	jmp	.LBB0_13
.LBB0_8:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.20, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.21, %edi
	jmp	.LBB0_12
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.22, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.23, %edi
	jmp	.LBB0_12
.LBB0_10:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.28, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.30, %edi
	jmp	.LBB0_12
.LBB0_11:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.18, %edi
.LBB0_12:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	callq	C_addcmd
	movl	$.L.str.19, %edi
.LBB0_13:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	callq	C_addcmd
	jmp	.LBB0_50
.LBB0_14:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rdi
	callq	strlen
	cmpq	$2, %rax
	jne	.LBB0_32
# BB#15:                                #   in Loop: Header=BB0_2 Depth=1
	decl	%r12d
	movq	16(%r14), %r15
	jmp	.LBB0_33
.LBB0_16:                               #   in Loop: Header=BB0_2 Depth=1
	orb	$2, _Y_flags(%rip)
	jmp	.LBB0_50
.LBB0_17:                               #   in Loop: Header=BB0_2 Depth=1
	orb	$4, _Y_flags(%rip)
	jmp	.LBB0_50
.LBB0_18:                               #   in Loop: Header=BB0_2 Depth=1
	orb	$32, _Y_flags(%rip)
	jmp	.LBB0_50
.LBB0_19:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$1, _Y_eflag(%rip)
	jmp	.LBB0_50
.LBB0_20:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rdi
	callq	strlen
	cmpq	$2, %rax
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jne	.LBB0_34
# BB#21:                                #   in Loop: Header=BB0_2 Depth=1
	decl	%r12d
	movq	16(%r14), %r15
	movq	%r13, %r14
	jmp	.LBB0_35
.LBB0_22:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$_T_gtol, %edi
	movl	$2, %esi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	_T_addtol
	jmp	.LBB0_50
.LBB0_23:                               #   in Loop: Header=BB0_2 Depth=1
	orb	$16, _Y_flags(%rip)
	jmp	.LBB0_50
.LBB0_24:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	callq	Z_setquiet
	jmp	.LBB0_50
.LBB0_25:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rdi
	callq	strlen
	cmpq	$2, %rax
	jne	.LBB0_40
# BB#26:                                #   in Loop: Header=BB0_2 Depth=1
	decl	%r12d
	movq	16(%r14), %r15
	jmp	.LBB0_41
.LBB0_27:                               #   in Loop: Header=BB0_2 Depth=1
	movq	%r15, %rdi
	callq	strlen
	cmpq	$2, %rax
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jne	.LBB0_43
# BB#28:                                #   in Loop: Header=BB0_2 Depth=1
	decl	%r12d
	movq	16(%r14), %r15
	movq	%r13, %r14
	jmp	.LBB0_45
.LBB0_29:                               #   in Loop: Header=BB0_2 Depth=1
	orb	$8, _Y_flags(%rip)
	jmp	.LBB0_50
.LBB0_30:                               #   in Loop: Header=BB0_2 Depth=1
	movb	$1, _Y_vflag(%rip)
	jmp	.LBB0_50
.LBB0_31:                               #   in Loop: Header=BB0_2 Depth=1
	orb	$1, _Y_flags(%rip)
	jmp	.LBB0_50
.LBB0_32:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %r15
	movq	%r14, %r13
.LBB0_33:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$_T_gtol, %edi
	xorl	%esi, %esi
	jmp	.LBB0_42
.LBB0_34:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %r15
.LBB0_35:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str, %esi
	movq	%r15, %rdi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB0_38
# BB#36:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	Z_fatal
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_37:                               # %.lr.ph.i
                                        #   in Loop: Header=BB0_38 Depth=2
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	C_addcmd
.LBB0_38:                               # %.lr.ph.i
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1024, %esi             # imm = 0x400
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	testq	%rax, %rax
	jne	.LBB0_37
# BB#39:                                # %._crit_edge.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	%rbp, %rdi
	callq	fclose
	jmp	.LBB0_49
.LBB0_40:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %r15
	movq	%r14, %r13
.LBB0_41:                               #   in Loop: Header=BB0_2 Depth=1
	movl	$_T_gtol, %edi
	movl	$1, %esi
.LBB0_42:                               #   in Loop: Header=BB0_2 Depth=1
	xorl	%eax, %eax
	movq	%r15, %rdx
	callq	_T_addtol
	movq	%r13, %r14
	jmp	.LBB0_50
.LBB0_43:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$2, %r15
	jmp	.LBB0_45
	.p2align	4, 0x90
.LBB0_44:                               # %.critedge.i.i
                                        #   in Loop: Header=BB0_45 Depth=2
	movb	$0, (%rcx)
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	C_addcmd
	cmpb	$10, -1(%rbp)
	leaq	-1(%rbp), %r15
	cmoveq	%rbp, %r15
.LBB0_45:                               #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_48 Depth 3
	movb	(%r15), %al
	testb	%al, %al
	je	.LBB0_49
# BB#46:                                # %.preheader.i.i
                                        #   in Loop: Header=BB0_45 Depth=2
	incq	%r15
	movq	%r15, %rbp
	movq	%rbx, %rcx
	testb	%al, %al
	je	.LBB0_44
	.p2align	4, 0x90
.LBB0_48:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_45 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpb	$10, %al
	je	.LBB0_44
# BB#47:                                #   in Loop: Header=BB0_48 Depth=3
	movb	%al, (%rcx)
	incq	%rcx
	movzbl	(%rbp), %eax
	incq	%rbp
	testb	%al, %al
	jne	.LBB0_48
	jmp	.LBB0_44
.LBB0_49:                               # %_Y_cmdlines.exit.i
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB0_50:                               #   in Loop: Header=BB0_2 Depth=1
	addq	$8, %r14
	decl	%r12d
	cmpl	$1, %r12d
	jg	.LBB0_2
	jmp	.LBB0_52
.LBB0_51:                               # %.critedge.i
	cmpl	$3, %r12d
	je	.LBB0_53
.LBB0_52:                               # %.critedge.thread.i
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	Z_fatal
.LBB0_53:                               # %_Y_doargs.exit
	movq	%rbp, %r15
	movq	8(%r14), %rbp
	movq	16(%r14), %rbx
	xorl	%eax, %eax
	callq	T_initdefault
	xorl	%edi, %edi
	movq	%rbp, %rsi
	callq	L_init_file
	movl	$0, _K_atm(%rip)
	movl	_L_arlm(%rip), %edx
	movl	_Y_flags(%rip), %ecx
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	P_file_parse
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	L_init_file
	movl	$0, _K_btm(%rip)
	movl	_L_brlm(%rip), %edx
	movl	_Y_flags(%rip), %ecx
	movl	$1, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	P_file_parse
	cmpb	$1, _Y_vflag(%rip)
	jne	.LBB0_55
# BB#54:
	movl	_Y_flags(%rip), %edi
	callq	V_visual
	jmp	.LBB0_65
.LBB0_55:
	movq	%r15, %rbp
	cmpl	$-1, %ebp
	jne	.LBB0_57
# BB#56:
	movl	_K_btm(%rip), %ebp
	addl	_K_atm(%rip), %ebp
.LBB0_57:                               # %.preheader
	movl	$30001, %ebx            # imm = 0x7531
	.p2align	4, 0x90
.LBB0_58:                               # =>This Inner Loop Header: Depth=1
	movl	_K_atm(%rip), %edi
	movl	_K_btm(%rip), %esi
	movl	_Y_flags(%rip), %ecx
	cmpb	$1, _Y_eflag(%rip)
	jne	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_58 Depth=1
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	Q_do_exact
	jmp	.LBB0_61
	.p2align	4, 0x90
.LBB0_60:                               #   in Loop: Header=BB0_58 Depth=1
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	G_do_miller
.LBB0_61:                               #   in Loop: Header=BB0_58 Depth=1
	movq	%rax, %rcx
	decl	%ebx
	jne	.LBB0_58
# BB#62:
	testq	%rcx, %rcx
	je	.LBB0_64
# BB#63:
	movl	_Y_flags(%rip), %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	O_output
	movl	$1, %eax
	jmp	.LBB0_65
.LBB0_64:
	xorl	%eax, %eax
.LBB0_65:
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_5
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_7
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_8
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_9
	.quad	.LBB0_10
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_11
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_14
	.quad	.LBB0_16
	.quad	.LBB0_17
	.quad	.LBB0_18
	.quad	.LBB0_19
	.quad	.LBB0_20
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_22
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_23
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_6
	.quad	.LBB0_24
	.quad	.LBB0_25
	.quad	.LBB0_27
	.quad	.LBB0_29
	.quad	.LBB0_6
	.quad	.LBB0_30
	.quad	.LBB0_31

	.type	_Y_flags,@object        # @_Y_flags
	.local	_Y_flags
	.comm	_Y_flags,4,4
	.type	_Y_vflag,@object        # @_Y_vflag
	.local	_Y_vflag
	.comm	_Y_vflag,1,4
	.type	_Y_eflag,@object        # @_Y_eflag
	.local	_Y_eflag
	.comm	_Y_eflag,1,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"can't open command file\n"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"literal  \"   \"    \\ "
	.size	.L.str.2, 21

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"comment  /*  */\t "
	.size	.L.str.3, 18

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"literal  &&\t\t "
	.size	.L.str.4, 15

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"literal  ||\t\t "
	.size	.L.str.5, 15

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"literal  <=\t\t "
	.size	.L.str.6, 15

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"literal  >=\t\t "
	.size	.L.str.7, 15

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"literal  !=\t\t "
	.size	.L.str.8, 15

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"literal  ==\t\t "
	.size	.L.str.9, 15

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"literal  --\t\t "
	.size	.L.str.10, 15

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"literal  ++\t\t "
	.size	.L.str.11, 15

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"literal  <<\t\t "
	.size	.L.str.12, 15

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"literal  >>\t\t "
	.size	.L.str.13, 15

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"literal  ->\t\t "
	.size	.L.str.14, 15

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"addalpha _\t\t "
	.size	.L.str.15, 14

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"tol      a0 \t\t "
	.size	.L.str.16, 16

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"literal  '    '    \\\t"
	.size	.L.str.17, 22

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"comment  #    $\t"
	.size	.L.str.18, 17

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"tol      a0 \t\t"
	.size	.L.str.19, 15

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"literal  '\t'     ' "
	.size	.L.str.20, 20

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"comment  ^C   $\t"
	.size	.L.str.21, 17

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"literal  \" \t\"\t"
	.size	.L.str.22, 15

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"comment  ; \t$\t"
	.size	.L.str.23, 15

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"literal ' \t'\t"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"literal \"\t\"\t"
	.size	.L.str.25, 13

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"comment (*\t*)\t"
	.size	.L.str.26, 15

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"literal :=\t\t"
	.size	.L.str.27, 13

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"literal <>\t\t"
	.size	.L.str.28, 13

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"literal <=\t\t"
	.size	.L.str.29, 13

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"literal >=\t\t"
	.size	.L.str.30, 13

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"don't understand arguments\n"
	.size	.L.str.31, 28

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"spiff requires two file names.\n"
	.size	.L.str.32, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
