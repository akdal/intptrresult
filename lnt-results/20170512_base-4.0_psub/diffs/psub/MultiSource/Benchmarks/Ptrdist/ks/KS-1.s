	.text
	.file	"KS-1.bc"
	.globl	ReadNetList
	.p2align	4, 0x90
	.type	ReadNetList,@function
ReadNetList:                            # @ReadNetList
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1048, %rsp             # imm = 0x418
.Lcfi6:
	.cfi_def_cfa_offset 1104
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$.L.str, %esi
	callq	fopen
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_1
# BB#4:
	leaq	16(%rsp), %rbx
	movl	$1024, %esi             # imm = 0x400
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movl	$.L.str.6, %esi
	movl	$numNets, %edx
	movl	$numModules, %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sscanf
	cmpl	$2, %eax
	jne	.LBB0_17
# BB#5:                                 # %.preheader
	cmpq	$0, numNets(%rip)
	je	.LBB0_16
# BB#6:                                 # %.lr.ph44.preheader
	xorl	%r14d, %r14d
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph44
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
	movl	$1024, %esi             # imm = 0x400
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	fgets
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	strtok
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	%rax, %r12
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB0_8
# BB#10:                                #   in Loop: Header=BB0_7 Depth=1
	xorl	%edi, %edi
	movl	$.L.str.8, %esi
	callq	strtok
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	decq	%rax
	movq	%rax, 8(%r13)
	movq	$0, (%r13)
	xorl	%edi, %edi
	movl	$.L.str.8, %esi
	callq	strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_15
# BB#11:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	%r13, %r15
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph
                                        #   Parent Loop BB0_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB0_13
# BB#14:                                #   in Loop: Header=BB0_12 Depth=2
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	decq	%rax
	movq	%rax, 8(%rbp)
	movq	$0, (%rbp)
	movq	%rbp, (%r15)
	xorl	%edi, %edi
	movl	$.L.str.8, %esi
	callq	strtok
	movq	%rax, %rbx
	testq	%rbx, %rbx
	movq	%rbp, %r15
	jne	.LBB0_12
.LBB0_15:                               # %._crit_edge
                                        #   in Loop: Header=BB0_7 Depth=1
	movq	%r13, nets-8(,%r12,8)
	incq	%r14
	cmpq	numNets(%rip), %r14
	movq	8(%rsp), %rbp           # 8-byte Reload
	jb	.LBB0_7
.LBB0_16:                               # %._crit_edge45
	addq	$1048, %rsp             # imm = 0x418
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_13:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.3, %ecx
	movl	$70, %r8d
.LBB0_9:
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.9, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	callq	fprintf
.LBB0_3:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$1, %edi
	callq	exit
.LBB0_8:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.3, %ecx
	movl	$63, %r8d
	jmp	.LBB0_9
.LBB0_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.3, %ecx
	movl	$46, %r8d
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	jmp	.LBB0_2
.LBB0_17:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.3, %ecx
	movl	$51, %r8d
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.7, %esi
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	movq	%rbp, %rdx
.LBB0_2:
	callq	fprintf
	jmp	.LBB0_3
.Lfunc_end0:
	.size	ReadNetList, .Lfunc_end0-ReadNetList
	.cfi_endproc

	.globl	NetsToModules
	.p2align	4, 0x90
	.type	NetsToModules,@function
NetsToModules:                          # @NetsToModules
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	numModules(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB1_2
# BB#1:                                 # %.lr.ph30
	shlq	$3, %rdx
	movl	$modules, %edi
	xorl	%esi, %esi
	callq	memset
.LBB1_2:                                # %.preheader
	movq	numNets(%rip), %r14
	testq	%r14, %r14
	je	.LBB1_9
# BB#3:                                 # %.lr.ph26.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph26
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	movq	nets(,%r15,8), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_6
	jmp	.LBB1_8
	.p2align	4, 0x90
.LBB1_7:                                #   in Loop: Header=BB1_6 Depth=2
	movq	%r15, 8(%rax)
	movq	8(%rbx), %rcx
	movq	modules(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	%rax, modules(,%rcx,8)
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB1_8
.LBB1_6:                                # %.lr.ph
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$16, %edi
	callq	malloc
	testq	%rax, %rax
	jne	.LBB1_7
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_8:                                # %._crit_edge
                                        #   in Loop: Header=BB1_4 Depth=1
	incq	%r15
	cmpq	%r14, %r15
	jb	.LBB1_4
.LBB1_9:                                # %._crit_edge27
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB1_10:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.10, %ecx
	movl	$96, %r8d
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	NetsToModules, .Lfunc_end1-NetsToModules
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.text
	.globl	ComputeNetCosts
	.p2align	4, 0x90
	.type	ComputeNetCosts,@function
ComputeNetCosts:                        # @ComputeNetCosts
	.cfi_startproc
# BB#0:
	movq	numNets(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_14
# BB#1:                                 # %.lr.ph.preheader
	cmpq	$8, %rax
	jae	.LBB2_3
# BB#2:
	xorl	%ecx, %ecx
	jmp	.LBB2_13
.LBB2_3:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB2_4
# BB#5:                                 # %vector.body.preheader
	leaq	-8(%rcx), %rsi
	movl	%esi, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB2_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rdi
	xorl	%edx, %edx
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB2_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, cost(,%rdx,4)
	movaps	%xmm0, cost+16(,%rdx,4)
	addq	$8, %rdx
	incq	%rdi
	jne	.LBB2_8
	jmp	.LBB2_9
.LBB2_4:
	xorl	%ecx, %ecx
	jmp	.LBB2_13
.LBB2_6:
	xorl	%edx, %edx
.LBB2_9:                                # %vector.body.prol.loopexit
	cmpq	$56, %rsi
	jb	.LBB2_12
# BB#10:                                # %vector.body.preheader.new
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00,1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB2_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, cost(,%rdx,4)
	movaps	%xmm0, cost+16(,%rdx,4)
	movaps	%xmm0, cost+32(,%rdx,4)
	movaps	%xmm0, cost+48(,%rdx,4)
	movaps	%xmm0, cost+64(,%rdx,4)
	movaps	%xmm0, cost+80(,%rdx,4)
	movaps	%xmm0, cost+96(,%rdx,4)
	movaps	%xmm0, cost+112(,%rdx,4)
	movaps	%xmm0, cost+128(,%rdx,4)
	movaps	%xmm0, cost+144(,%rdx,4)
	movaps	%xmm0, cost+160(,%rdx,4)
	movaps	%xmm0, cost+176(,%rdx,4)
	movaps	%xmm0, cost+192(,%rdx,4)
	movaps	%xmm0, cost+208(,%rdx,4)
	movaps	%xmm0, cost+224(,%rdx,4)
	movaps	%xmm0, cost+240(,%rdx,4)
	addq	$64, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB2_11
.LBB2_12:                               # %middle.block
	cmpq	%rcx, %rax
	je	.LBB2_14
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$1065353216, cost(,%rcx,4) # imm = 0x3F800000
	incq	%rcx
	cmpq	%rax, %rcx
	jb	.LBB2_13
.LBB2_14:                               # %._crit_edge
	retq
.Lfunc_end2:
	.size	ComputeNetCosts, .Lfunc_end2-ComputeNetCosts
	.cfi_endproc

	.globl	InitLists
	.p2align	4, 0x90
	.type	InitLists,@function
InitLists:                              # @InitLists
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 80
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	xorps	%xmm0, %xmm0
	movups	%xmm0, groupA(%rip)
	movups	%xmm0, groupB(%rip)
	movq	numModules(%rip), %r14
	cmpq	$2, %r14
	jb	.LBB3_14
# BB#1:                                 # %.lr.ph
	shrq	%r14
	leaq	moduleToGroup(,%r14,4), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB3_3
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	%r12, 8(%rbx)
	testq	%rbp, %rbp
	je	.LBB3_6
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=1
	movq	$0, (%rbx)
	movq	%rbx, (%r13)
	movq	%rbx, groupA+8(%rip)
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movq	%rbx, groupA+8(%rip)
	movq	%rbx, groupA(%rip)
	movq	$0, (%rbx)
	movq	%rbx, %rbp
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=1
	movl	$0, moduleToGroup(,%r12,4)
	movl	$16, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB3_9
# BB#10:                                #   in Loop: Header=BB3_2 Depth=1
	leaq	(%r14,%r12), %rcx
	movq	%rcx, 8(%rax)
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	je	.LBB3_11
# BB#12:                                #   in Loop: Header=BB3_2 Depth=1
	movq	$0, (%rax)
	movq	%rax, (%r15)
	movq	%rax, groupB+8(%rip)
	jmp	.LBB3_13
	.p2align	4, 0x90
.LBB3_11:                               #   in Loop: Header=BB3_2 Depth=1
	movq	%rax, groupB+8(%rip)
	movq	%rax, groupB(%rip)
	movq	$0, (%rax)
	movq	%rax, %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
.LBB3_13:                               #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	$1, (%rcx,%r12,4)
	incq	%r12
	cmpq	%r14, %r12
	movq	%rax, %r15
	movq	%rbx, %r13
	jb	.LBB3_2
.LBB3_14:                               # %._crit_edge
	xorps	%xmm0, %xmm0
	movups	%xmm0, swapToA(%rip)
	movups	%xmm0, swapToB(%rip)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_3:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.12, %ecx
	movl	$145, %r8d
	jmp	.LBB3_4
.LBB3_9:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$.L.str.2, %edx
	movl	$.L.str.12, %ecx
	movl	$164, %r8d
.LBB3_4:
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	InitLists, .Lfunc_end3-InitLists
	.cfi_endproc

	.globl	ComputeDs
	.p2align	4, 0x90
	.type	ComputeDs,@function
ComputeDs:                              # @ComputeDs
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	testq	%r8, %r8
	jne	.LBB4_2
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_3:                                #   in Loop: Header=BB4_2 Depth=1
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
.LBB4_14:                               # %._crit_edge46
                                        #   in Loop: Header=BB4_2 Depth=1
	subss	%xmm1, %xmm0
	movss	%xmm0, D(,%rcx,4)
	movq	(%r8), %r8
	testq	%r8, %r8
	je	.LBB4_15
.LBB4_2:                                # %.lr.ph52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #       Child Loop BB4_7 Depth 3
	movq	8(%r8), %rcx
	movq	modules(,%rcx,8), %r9
	testq	%r9, %r9
	je	.LBB4_3
# BB#4:                                 # %.lr.ph45.preheader
                                        #   in Loop: Header=BB4_2 Depth=1
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph45
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_7 Depth 3
	movq	8(%r9), %rdi
	movq	nets(,%rdi,8), %rax
	testq	%rax, %rax
	jne	.LBB4_7
	jmp	.LBB4_13
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_7 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB4_13
.LBB4_7:                                # %.lr.ph
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %rdx
	cmpq	%rcx, %rdx
	je	.LBB4_12
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=3
	movl	moduleToGroup(,%rdx,4), %edx
	cmpl	$1, %edx
	ja	.LBB4_12
# BB#9:                                 #   in Loop: Header=BB4_7 Depth=3
	movss	cost(,%rdi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cmpl	%esi, %edx
	jne	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_7 Depth=3
	addss	%xmm2, %xmm1
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB4_7
	jmp	.LBB4_13
.LBB4_11:                               #   in Loop: Header=BB4_7 Depth=3
	addss	%xmm2, %xmm0
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_13:                               # %._crit_edge
                                        #   in Loop: Header=BB4_5 Depth=2
	movq	(%r9), %r9
	testq	%r9, %r9
	jne	.LBB4_5
	jmp	.LBB4_14
.LBB4_15:                               # %._crit_edge53
	retq
.Lfunc_end4:
	.size	ComputeDs, .Lfunc_end4-ComputeDs
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"r"
	.size	.L.str, 2

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"(%s:%s():%d): "
	.size	.L.str.1, 15

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Benchmarks/Ptrdist/ks/KS-1.c"
	.size	.L.str.2, 79

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"ReadData"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"unable to open input file [%s]"
	.size	.L.str.4, 31

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%lu %lu"
	.size	.L.str.6, 8

	.type	numNets,@object         # @numNets
	.comm	numNets,8,8
	.type	numModules,@object      # @numModules
	.comm	numModules,8,8
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"unable to parse header in file [%s]"
	.size	.L.str.7, 36

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" \t\n"
	.size	.L.str.8, 4

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"unable to allocate a module list node"
	.size	.L.str.9, 38

	.type	nets,@object            # @nets
	.comm	nets,8192,16
	.type	modules,@object         # @modules
	.comm	modules,8192,16
	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"NetsToModules"
	.size	.L.str.10, 14

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"unable to allocate net list node"
	.size	.L.str.11, 33

	.type	cost,@object            # @cost
	.comm	cost,4096,16
	.type	groupA,@object          # @groupA
	.comm	groupA,16,8
	.type	groupB,@object          # @groupB
	.comm	groupB,16,8
	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"main"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"unable to allocate ModuleRec"
	.size	.L.str.13, 29

	.type	moduleToGroup,@object   # @moduleToGroup
	.comm	moduleToGroup,4096,16
	.type	swapToA,@object         # @swapToA
	.comm	swapToA,16,8
	.type	swapToB,@object         # @swapToB
	.comm	swapToB,16,8
	.type	D,@object               # @D
	.comm	D,4096,16
	.type	GP,@object              # @GP
	.comm	GP,4096,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
