	.text
	.file	"TarUpdate.bc"
	.globl	_ZN8NArchive4NTar13UpdateArchiveEP9IInStreamP20ISequentialOutStreamRK13CObjectVectorINS0_7CItemExEERKS5_INS0_11CUpdateItemEEP22IArchiveUpdateCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar13UpdateArchiveEP9IInStreamP20ISequentialOutStreamRK13CObjectVectorINS0_7CItemExEERKS5_INS0_11CUpdateItemEEP22IArchiveUpdateCallback,@function
_ZN8NArchive4NTar13UpdateArchiveEP9IInStreamP20ISequentialOutStreamRK13CObjectVectorINS0_7CItemExEERKS5_INS0_11CUpdateItemEEP22IArchiveUpdateCallback: # @_ZN8NArchive4NTar13UpdateArchiveEP9IInStreamP20ISequentialOutStreamRK13CObjectVectorINS0_7CItemExEERKS5_INS0_11CUpdateItemEEP22IArchiveUpdateCallback
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 272
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r12
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	$0, 8(%rsp)
.Ltmp0:
	leaq	8(%rsp), %rdi
	callq	_ZN8NArchive4NTar11COutArchive6CreateEP20ISequentialOutStream
.Ltmp1:
# BB#1:                                 # %.preheader
	movslq	12(%r12), %rax
	testq	%rax, %rax
	jle	.LBB0_7
# BB#2:                                 # %.lr.ph347
	movq	16(%r12), %rcx
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rdx
	xorl	%edi, %edi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdi,8), %rbp
	cmpb	$0, 72(%rbp)
	je	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	movq	16(%rbp), %rbp
	jmp	.LBB0_6
	.p2align	4, 0x90
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	movslq	(%rbp), %rbp
	movq	(%rdx,%rbp,8), %rbx
	movl	120(%rbx), %ebp
	addq	16(%rbx), %rbp
.LBB0_6:                                #   in Loop: Header=BB0_3 Depth=1
	addq	%rbp, %rsi
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB0_3
	jmp	.LBB0_8
.LBB0_7:
	xorl	%esi, %esi
.LBB0_8:                                # %._crit_edge348
	movq	(%r14), %rax
.Ltmp3:
	movq	%r14, %rdi
	callq	*40(%rax)
	movl	%eax, %r13d
.Ltmp4:
# BB#9:
	testl	%r13d, %r13d
	jne	.LBB0_184
# BB#10:
.Ltmp5:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp6:
# BB#11:
	movl	$0, 16(%rbx)
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
.Ltmp8:
	movq	%rbx, %rdi
	callq	*_ZTVN9NCompress10CCopyCoderE+24(%rip)
.Ltmp9:
# BB#12:                                # %_ZN9CMyComPtrI14ICompressCoderEC2EPS0_.exit
.Ltmp11:
	movl	$72, %edi
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp12:
# BB#13:
.Ltmp14:
	movq	%rbp, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp15:
# BB#14:
	movq	(%rbp), %rax
.Ltmp17:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp18:
# BB#15:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp20:
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp21:
# BB#16:
.Ltmp22:
	movl	$48, %edi
	callq	_Znwm
.Ltmp23:
# BB#17:
	movl	$0, 8(%rax)
	movq	$_ZTV26CLimitedSequentialInStream+16, (%rax)
	movq	$0, 16(%rax)
.Ltmp25:
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	callq	*_ZTV26CLimitedSequentialInStream+24(%rip)
.Ltmp26:
# BB#18:                                # %_ZN9CMyComPtrI26CLimitedSequentialInStreamEC2EPS0_.exit
	movq	%r15, 192(%rsp)         # 8-byte Spill
	movq	%r14, 200(%rsp)         # 8-byte Spill
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB0_20
# BB#19:
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp28:
	callq	*8(%rax)
.Ltmp29:
.LBB0_20:                               # %.noexc259
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB0_22
# BB#21:
	movq	(%rdi), %rax
.Ltmp30:
	callq	*16(%rax)
.Ltmp31:
.LBB0_22:                               # %_ZN26CLimitedSequentialInStream9SetStreamEP19ISequentialInStream.exit
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rax, 16(%rcx)
	cmpl	$0, 12(%r12)
	jle	.LBB0_180
# BB#23:                                # %.lr.ph
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	%r12, (%rsp)            # 8-byte Spill
	jmp	.LBB0_177
	.p2align	4, 0x90
.LBB0_24:                               #   in Loop: Header=BB0_177 Depth=1
	movq	16(%r12), %rax
	movq	(%rax,%r15,8), %r12
.Ltmp35:
	leaq	64(%rsp), %rdi
	callq	_ZN8NArchive4NTar5CItemC2Ev
.Ltmp36:
# BB#25:                                #   in Loop: Header=BB0_177 Depth=1
	cmpb	$0, 73(%r12)
	je	.LBB0_29
# BB#26:                                #   in Loop: Header=BB0_177 Depth=1
	movl	12(%r12), %eax
	movl	%eax, 88(%rsp)
	leaq	24(%r12), %rcx
	leaq	64(%rsp), %rax
	cmpq	%rax, %rcx
	je	.LBB0_50
# BB#27:                                #   in Loop: Header=BB0_177 Depth=1
	movl	$0, 72(%rsp)
	movq	64(%rsp), %rax
	movb	$0, (%rax)
	movslq	32(%r12), %rax
	leaq	1(%rax), %rdx
	movl	76(%rsp), %ebp
	cmpl	%ebp, %edx
	jne	.LBB0_30
# BB#28:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	64(%rsp), %r14
	jmp	.LBB0_47
	.p2align	4, 0x90
.LBB0_29:                               #   in Loop: Header=BB0_177 Depth=1
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	16(%rax), %rax
	movslq	(%r12), %rcx
	movq	(%rax,%rcx,8), %rsi
.Ltmp44:
	leaq	64(%rsp), %rdi
	callq	_ZN8NArchive4NTar5CItemaSERKS1_
.Ltmp45:
	jmp	.LBB0_100
.LBB0_30:                               #   in Loop: Header=BB0_177 Depth=1
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpl	$-1, %eax
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movq	%rdx, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp38:
	callq	_Znam
	movq	%rax, %r14
.Ltmp39:
# BB#31:                                # %.noexc263
                                        #   in Loop: Header=BB0_177 Depth=1
	testl	%ebp, %ebp
	jle	.LBB0_46
# BB#32:                                # %.preheader.i.i
                                        #   in Loop: Header=BB0_177 Depth=1
	movslq	72(%rsp), %rax
	testq	%rax, %rax
	movq	64(%rsp), %rdi
	jle	.LBB0_44
# BB#33:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpl	$31, %eax
	jbe	.LBB0_37
# BB#34:                                # %min.iters.checked387
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%rax, %rcx
	andq	$-32, %rcx
	je	.LBB0_37
# BB#35:                                # %vector.memcheck398
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	(%rdi,%rax), %rdx
	cmpq	%rdx, %r14
	jae	.LBB0_153
# BB#36:                                # %vector.memcheck398
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	(%r14,%rax), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_153
.LBB0_37:                               #   in Loop: Header=BB0_177 Depth=1
	xorl	%ecx, %ecx
.LBB0_38:                               # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_41
# BB#39:                                # %.lr.ph.i.i.prol.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_40:                               # %.lr.ph.i.i.prol
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%r14,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_40
.LBB0_41:                               # %.lr.ph.i.i.prol.loopexit
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	$7, %rbp
	jb	.LBB0_45
# BB#42:                                # %.lr.ph.i.i.preheader.new
                                        #   in Loop: Header=BB0_177 Depth=1
	subq	%rcx, %rax
	leaq	7(%r14,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_43:                               # %.lr.ph.i.i
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %ebx
	movb	%bl, -7(%rdx)
	movzbl	-6(%rcx), %ebx
	movb	%bl, -6(%rdx)
	movzbl	-5(%rcx), %ebx
	movb	%bl, -5(%rdx)
	movzbl	-4(%rcx), %ebx
	movb	%bl, -4(%rdx)
	movzbl	-3(%rcx), %ebx
	movb	%bl, -3(%rdx)
	movzbl	-2(%rcx), %ebx
	movb	%bl, -2(%rdx)
	movzbl	-1(%rcx), %ebx
	movb	%bl, -1(%rdx)
	movzbl	(%rcx), %ebx
	movb	%bl, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rax
	jne	.LBB0_43
	jmp	.LBB0_45
.LBB0_44:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB0_177 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_46
.LBB0_45:                               # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB0_177 Depth=1
	callq	_ZdaPv
.LBB0_46:                               # %._crit_edge17.i.i
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%r14, 64(%rsp)
	movslq	72(%rsp), %rax
	movb	$0, (%r14,%rax)
	movq	208(%rsp), %rax         # 8-byte Reload
	movl	%eax, 76(%rsp)
	movq	40(%rsp), %rcx          # 8-byte Reload
.LBB0_47:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	(%rcx), %rax
	.p2align	4, 0x90
.LBB0_48:                               #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%r14)
	incq	%r14
	testb	%cl, %cl
	jne	.LBB0_48
# BB#49:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i
                                        #   in Loop: Header=BB0_177 Depth=1
	movl	32(%r12), %eax
	movl	%eax, 72(%rsp)
.LBB0_50:                               # %_ZN11CStringBaseIcEaSERKS0_.exit
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	40(%r12), %r14
	leaq	128(%rsp), %rax
	cmpq	%rax, %r14
	je	.LBB0_73
# BB#51:                                #   in Loop: Header=BB0_177 Depth=1
	movl	$0, 136(%rsp)
	movq	128(%rsp), %rax
	movb	$0, (%rax)
	movslq	48(%r12), %rax
	leaq	1(%rax), %rcx
	movl	140(%rsp), %ebp
	cmpl	%ebp, %ecx
	jne	.LBB0_53
# BB#52:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i265
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	128(%rsp), %rbx
	jmp	.LBB0_70
.LBB0_53:                               #   in Loop: Header=BB0_177 Depth=1
	cmpl	$-1, %eax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp40:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp41:
# BB#54:                                # %.noexc279
                                        #   in Loop: Header=BB0_177 Depth=1
	testl	%ebp, %ebp
	jle	.LBB0_69
# BB#55:                                # %.preheader.i.i266
                                        #   in Loop: Header=BB0_177 Depth=1
	movslq	136(%rsp), %r8
	testq	%r8, %r8
	movq	128(%rsp), %rdi
	jle	.LBB0_67
# BB#56:                                # %.lr.ph.preheader.i.i267
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpl	$31, %r8d
	jbe	.LBB0_60
# BB#57:                                # %min.iters.checked360
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB0_60
# BB#58:                                # %vector.memcheck371
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_156
# BB#59:                                # %vector.memcheck371
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_156
.LBB0_60:                               #   in Loop: Header=BB0_177 Depth=1
	xorl	%ecx, %ecx
.LBB0_61:                               # %.lr.ph.i.i272.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_64
# BB#62:                                # %.lr.ph.i.i272.prol.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_63:                               # %.lr.ph.i.i272.prol
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_63
.LBB0_64:                               # %.lr.ph.i.i272.prol.loopexit
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	$7, %rbp
	jb	.LBB0_68
# BB#65:                                # %.lr.ph.i.i272.preheader.new
                                        #   in Loop: Header=BB0_177 Depth=1
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_66:                               # %.lr.ph.i.i272
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB0_66
	jmp	.LBB0_68
.LBB0_67:                               # %._crit_edge.i.i268
                                        #   in Loop: Header=BB0_177 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_69
.LBB0_68:                               # %._crit_edge.thread.i.i273
                                        #   in Loop: Header=BB0_177 Depth=1
	callq	_ZdaPv
.LBB0_69:                               # %._crit_edge17.i.i274
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%rbx, 128(%rsp)
	movslq	136(%rsp), %rax
	movb	$0, (%rbx,%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, 140(%rsp)
.LBB0_70:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i275
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB0_71:                               #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB0_71
# BB#72:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i278
                                        #   in Loop: Header=BB0_177 Depth=1
	movl	48(%r12), %eax
	movl	%eax, 136(%rsp)
.LBB0_73:                               # %_ZN11CStringBaseIcEaSERKS0_.exit280
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	56(%r12), %r14
	leaq	144(%rsp), %rax
	cmpq	%rax, %r14
	je	.LBB0_96
# BB#74:                                #   in Loop: Header=BB0_177 Depth=1
	movl	$0, 152(%rsp)
	movq	144(%rsp), %rax
	movb	$0, (%rax)
	movslq	64(%r12), %rax
	leaq	1(%rax), %rcx
	movl	156(%rsp), %ebp
	cmpl	%ebp, %ecx
	jne	.LBB0_76
# BB#75:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i282
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	144(%rsp), %rbx
	jmp	.LBB0_93
.LBB0_76:                               #   in Loop: Header=BB0_177 Depth=1
	cmpl	$-1, %eax
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, %rdi
	movq	$-1, %rax
	cmovlq	%rax, %rdi
.Ltmp42:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp43:
# BB#77:                                # %.noexc296
                                        #   in Loop: Header=BB0_177 Depth=1
	testl	%ebp, %ebp
	jle	.LBB0_92
# BB#78:                                # %.preheader.i.i283
                                        #   in Loop: Header=BB0_177 Depth=1
	movslq	152(%rsp), %r8
	testq	%r8, %r8
	movq	144(%rsp), %rdi
	jle	.LBB0_90
# BB#79:                                # %.lr.ph.preheader.i.i284
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpl	$31, %r8d
	jbe	.LBB0_83
# BB#80:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%r8, %rcx
	andq	$-32, %rcx
	je	.LBB0_83
# BB#81:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	(%rdi,%r8), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB0_159
# BB#82:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	(%rbx,%r8), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB0_159
.LBB0_83:                               #   in Loop: Header=BB0_177 Depth=1
	xorl	%ecx, %ecx
.LBB0_84:                               # %.lr.ph.i.i289.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	movl	%r8d, %esi
	subl	%ecx, %esi
	leaq	-1(%r8), %rbp
	subq	%rcx, %rbp
	andq	$7, %rsi
	je	.LBB0_87
# BB#85:                                # %.lr.ph.i.i289.prol.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	negq	%rsi
	.p2align	4, 0x90
.LBB0_86:                               # %.lr.ph.i.i289.prol
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB0_86
.LBB0_87:                               # %.lr.ph.i.i289.prol.loopexit
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	$7, %rbp
	jb	.LBB0_91
# BB#88:                                # %.lr.ph.i.i289.preheader.new
                                        #   in Loop: Header=BB0_177 Depth=1
	subq	%rcx, %r8
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB0_89:                               # %.lr.ph.i.i289
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %r8
	jne	.LBB0_89
	jmp	.LBB0_91
.LBB0_90:                               # %._crit_edge.i.i285
                                        #   in Loop: Header=BB0_177 Depth=1
	testq	%rdi, %rdi
	je	.LBB0_92
.LBB0_91:                               # %._crit_edge.thread.i.i290
                                        #   in Loop: Header=BB0_177 Depth=1
	callq	_ZdaPv
.LBB0_92:                               # %._crit_edge17.i.i291
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%rbx, 144(%rsp)
	movslq	152(%rsp), %rax
	movb	$0, (%rbx,%rax)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, 156(%rsp)
.LBB0_93:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i292
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB0_94:                               #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB0_94
# BB#95:                                # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i295
                                        #   in Loop: Header=BB0_177 Depth=1
	movl	64(%r12), %eax
	movl	%eax, 152(%rsp)
.LBB0_96:                               # %_ZN11CStringBaseIcEaSERKS0_.exit297
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpb	$0, 74(%r12)
	je	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_177 Depth=1
	movb	$53, 168(%rsp)
	xorl	%eax, %eax
	jmp	.LBB0_99
	.p2align	4, 0x90
.LBB0_98:                               #   in Loop: Header=BB0_177 Depth=1
	movb	$48, 168(%rsp)
	movq	16(%r12), %rax
.LBB0_99:                               #   in Loop: Header=BB0_177 Depth=1
	movq	%rax, 80(%rsp)
	movl	8(%r12), %eax
	movl	%eax, 100(%rsp)
	movb	$0, 169(%rsp)
	movb	$0, 170(%rsp)
	movq	$0, 92(%rsp)
	movq	_ZN8NArchive4NTar11NFileHeader6NMagic6kEmptyE(%rip), %rax
	movq	(%rax), %rax
	movq	%rax, 160(%rsp)
.LBB0_100:                              #   in Loop: Header=BB0_177 Depth=1
	cmpb	$0, 72(%r12)
	je	.LBB0_107
# BB#101:                               #   in Loop: Header=BB0_177 Depth=1
	movq	16(%r12), %rax
	movq	%rax, 80(%rsp)
	cmpq	$-1, %rax
	je	.LBB0_111
# BB#102:                               #   in Loop: Header=BB0_177 Depth=1
	movq	$0, 56(%rsp)
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
	movl	4(%r12), %esi
.Ltmp47:
	leaq	56(%rsp), %rdx
	callq	*72(%rax)
.Ltmp48:
# BB#103:                               #   in Loop: Header=BB0_177 Depth=1
	movl	$1, %r14d
	testl	%eax, %eax
	je	.LBB0_115
# BB#104:                               #   in Loop: Header=BB0_177 Depth=1
	cmpl	$1, %eax
	jne	.LBB0_118
.LBB0_105:                              #   in Loop: Header=BB0_177 Depth=1
	movq	16(%r12), %rbx
	movq	200(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp56:
	xorl	%esi, %esi
	callq	*80(%rax)
.Ltmp57:
# BB#106:                               #   in Loop: Header=BB0_177 Depth=1
	addq	%rbx, 176(%rsp)         # 8-byte Folded Spill
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
	cmovnel	%eax, %r13d
	jmp	.LBB0_119
	.p2align	4, 0x90
.LBB0_107:                              #   in Loop: Header=BB0_177 Depth=1
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	(%r12), %rcx
	movq	(%rax,%rcx,8), %rax
	movq	16(%rax), %rax
	movq	%rax, 80(%rsp)
	movq	16(%rdx), %rax
	movslq	(%r12), %rcx
	movq	(%rax,%rcx,8), %r14
	cmpb	$0, 73(%r12)
	je	.LBB0_112
# BB#108:                               #   in Loop: Header=BB0_177 Depth=1
.Ltmp64:
	leaq	8(%rsp), %rdi
	leaq	64(%rsp), %rsi
	movq	32(%rsp), %rbp          # 8-byte Reload
	callq	_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE
.Ltmp65:
# BB#109:                               #   in Loop: Header=BB0_177 Depth=1
	movl	$1, %ebx
	testl	%eax, %eax
	movq	(%rsp), %r12            # 8-byte Reload
	je	.LBB0_123
# BB#110:                               #   in Loop: Header=BB0_177 Depth=1
	movl	%eax, %r13d
	jmp	.LBB0_131
	.p2align	4, 0x90
.LBB0_111:                              #   in Loop: Header=BB0_177 Depth=1
	movl	$-2147024809, %r13d     # imm = 0x80070057
	movl	$1, %ebx
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB0_130
.LBB0_112:                              #   in Loop: Header=BB0_177 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	112(%r14), %rsi
.Ltmp68:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	32(%rsp), %rbp          # 8-byte Reload
	callq	*48(%rax)
.Ltmp69:
# BB#113:                               #   in Loop: Header=BB0_177 Depth=1
	testl	%eax, %eax
	movq	(%rsp), %r12            # 8-byte Reload
	je	.LBB0_126
# BB#114:                               #   in Loop: Header=BB0_177 Depth=1
	movl	$1, %ebx
	movl	%eax, %r13d
	jmp	.LBB0_131
.LBB0_115:                              #   in Loop: Header=BB0_177 Depth=1
.Ltmp50:
	leaq	8(%rsp), %rdi
	leaq	64(%rsp), %rsi
	callq	_ZN8NArchive4NTar11COutArchive11WriteHeaderERKNS0_5CItemE
.Ltmp51:
# BB#116:                               #   in Loop: Header=BB0_177 Depth=1
	testl	%eax, %eax
	movl	%eax, %ebx
	cmovel	%r13d, %ebx
	je	.LBB0_141
.LBB0_118:                              #   in Loop: Header=BB0_177 Depth=1
	movl	%eax, %r13d
.LBB0_119:                              #   in Loop: Header=BB0_177 Depth=1
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	(%rsp), %r12            # 8-byte Reload
	je	.LBB0_121
# BB#120:                               #   in Loop: Header=BB0_177 Depth=1
	movq	(%rdi), %rax
.Ltmp61:
	callq	*16(%rax)
.Ltmp62:
.LBB0_121:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
                                        #   in Loop: Header=BB0_177 Depth=1
	movl	$1, %ebx
	testl	%r14d, %r14d
	jne	.LBB0_131
.LBB0_122:                              # %.thread
                                        #   in Loop: Header=BB0_177 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB0_131
.LBB0_123:                              #   in Loop: Header=BB0_177 Depth=1
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	120(%r14), %esi
	addq	112(%r14), %rsi
.Ltmp66:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
.Ltmp67:
# BB#124:                               #   in Loop: Header=BB0_177 Depth=1
	testl	%eax, %eax
	jne	.LBB0_129
# BB#125:                               #   in Loop: Header=BB0_177 Depth=1
	movq	16(%r14), %r12
	jmp	.LBB0_127
.LBB0_126:                              #   in Loop: Header=BB0_177 Depth=1
	movl	120(%r14), %r12d
	addq	16(%r14), %r12
.LBB0_127:                              #   in Loop: Header=BB0_177 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r12, 24(%rsi)
	movq	$0, 32(%rsi)
	movb	$0, 40(%rsi)
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp70:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	%rbp, %r9
	callq	*40(%rax)
.Ltmp71:
# BB#128:                               #   in Loop: Header=BB0_177 Depth=1
	movl	$1, %ebx
	testl	%eax, %eax
	je	.LBB0_148
.LBB0_129:                              #   in Loop: Header=BB0_177 Depth=1
	movl	%eax, %r13d
.LBB0_130:                              # %.thread326
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	(%rsp), %r12            # 8-byte Reload
.LBB0_131:                              # %.thread326
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_133
# BB#132:                               #   in Loop: Header=BB0_177 Depth=1
	callq	_ZdaPv
.LBB0_133:                              # %_ZN11CStringBaseIcED2Ev.exit.i303
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_135
# BB#134:                               #   in Loop: Header=BB0_177 Depth=1
	callq	_ZdaPv
.LBB0_135:                              # %_ZN11CStringBaseIcED2Ev.exit1.i304
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_137
# BB#136:                               #   in Loop: Header=BB0_177 Depth=1
	callq	_ZdaPv
.LBB0_137:                              # %_ZN11CStringBaseIcED2Ev.exit2.i305
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_139
# BB#138:                               #   in Loop: Header=BB0_177 Depth=1
	callq	_ZdaPv
.LBB0_139:                              # %_ZN8NArchive4NTar5CItemD2Ev.exit306
                                        #   in Loop: Header=BB0_177 Depth=1
	testl	%ebx, %ebx
	jne	.LBB0_181
# BB#140:                               #   in Loop: Header=BB0_177 Depth=1
	incq	%r15
	movslq	12(%r12), %rax
	cmpq	%rax, %r15
	jl	.LBB0_177
	jmp	.LBB0_180
.LBB0_141:                              #   in Loop: Header=BB0_177 Depth=1
	cmpb	$0, 74(%r12)
	jne	.LBB0_105
# BB#142:                               #   in Loop: Header=BB0_177 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movq	56(%rsp), %rsi
.Ltmp52:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %r13d
.Ltmp53:
# BB#143:                               #   in Loop: Header=BB0_177 Depth=1
	testl	%r13d, %r13d
	jne	.LBB0_119
# BB#144:                               #   in Loop: Header=BB0_177 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	32(%rax), %rsi
	movl	$-2147467259, %r13d     # imm = 0x80004005
	cmpq	80(%rsp), %rsi
	jne	.LBB0_119
# BB#145:                               #   in Loop: Header=BB0_177 Depth=1
.Ltmp54:
	leaq	8(%rsp), %rdi
	callq	_ZN8NArchive4NTar11COutArchive16FillDataResidualEy
.Ltmp55:
# BB#146:                               #   in Loop: Header=BB0_177 Depth=1
	testl	%eax, %eax
	movl	%ebx, %r13d
	jne	.LBB0_118
	jmp	.LBB0_105
.LBB0_148:                              #   in Loop: Header=BB0_177 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpq	%r12, 32(%rax)
	jne	.LBB0_152
# BB#149:                               #   in Loop: Header=BB0_177 Depth=1
	movq	16(%r14), %rsi
.Ltmp72:
	leaq	8(%rsp), %rdi
	callq	_ZN8NArchive4NTar11COutArchive16FillDataResidualEy
.Ltmp73:
# BB#150:                               #   in Loop: Header=BB0_177 Depth=1
	testl	%eax, %eax
	jne	.LBB0_129
# BB#151:                               #   in Loop: Header=BB0_177 Depth=1
	addq	%r12, 176(%rsp)         # 8-byte Folded Spill
	movq	(%rsp), %r12            # 8-byte Reload
	jmp	.LBB0_122
.LBB0_152:                              #   in Loop: Header=BB0_177 Depth=1
	movl	$-2147467259, %r13d     # imm = 0x80004005
	jmp	.LBB0_130
.LBB0_153:                              # %vector.body383.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_162
# BB#154:                               # %vector.body383.prol.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_155:                              # %vector.body383.prol
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movdqu	%xmm0, (%r14,%rbp)
	movdqu	%xmm1, 16(%r14,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_155
	jmp	.LBB0_163
.LBB0_156:                              # %vector.body356.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_167
# BB#157:                               # %vector.body356.prol.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_158:                              # %vector.body356.prol
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movdqu	%xmm0, (%rbx,%rbp)
	movdqu	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_158
	jmp	.LBB0_168
.LBB0_159:                              # %vector.body.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	leaq	-32(%rcx), %rdx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB0_172
# BB#160:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_177 Depth=1
	negq	%rsi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_161:                              # %vector.body.prol
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%rdi,%rbp), %xmm0
	movdqu	16(%rdi,%rbp), %xmm1
	movdqu	%xmm0, (%rbx,%rbp)
	movdqu	%xmm1, 16(%rbx,%rbp)
	addq	$32, %rbp
	incq	%rsi
	jne	.LBB0_161
	jmp	.LBB0_173
.LBB0_162:                              #   in Loop: Header=BB0_177 Depth=1
	xorl	%ebp, %ebp
.LBB0_163:                              # %vector.body383.prol.loopexit
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_166
# BB#164:                               # %vector.body383.preheader.new
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%r14,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB0_165:                              # %vector.body383
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_165
.LBB0_166:                              # %middle.block384
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	%rcx, %rax
	jne	.LBB0_38
	jmp	.LBB0_45
.LBB0_167:                              #   in Loop: Header=BB0_177 Depth=1
	xorl	%ebp, %ebp
.LBB0_168:                              # %vector.body356.prol.loopexit
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_171
# BB#169:                               # %vector.body356.preheader.new
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB0_170:                              # %vector.body356
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_170
.LBB0_171:                              # %middle.block357
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	%rcx, %r8
	jne	.LBB0_61
	jmp	.LBB0_68
.LBB0_172:                              #   in Loop: Header=BB0_177 Depth=1
	xorl	%ebp, %ebp
.LBB0_173:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	$96, %rdx
	jb	.LBB0_176
# BB#174:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB0_177 Depth=1
	movq	%rcx, %rdx
	subq	%rbp, %rdx
	leaq	112(%rbx,%rbp), %rsi
	leaq	112(%rdi,%rbp), %rbp
	.p2align	4, 0x90
.LBB0_175:                              # %vector.body
                                        #   Parent Loop BB0_177 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movdqu	-16(%rbp), %xmm0
	movdqu	(%rbp), %xmm1
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbp
	addq	$-128, %rdx
	jne	.LBB0_175
.LBB0_176:                              # %middle.block
                                        #   in Loop: Header=BB0_177 Depth=1
	cmpq	%rcx, %r8
	jne	.LBB0_84
	jmp	.LBB0_91
	.p2align	4, 0x90
.LBB0_177:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_155 Depth 2
                                        #     Child Loop BB0_165 Depth 2
                                        #     Child Loop BB0_40 Depth 2
                                        #     Child Loop BB0_43 Depth 2
                                        #     Child Loop BB0_48 Depth 2
                                        #     Child Loop BB0_158 Depth 2
                                        #     Child Loop BB0_170 Depth 2
                                        #     Child Loop BB0_63 Depth 2
                                        #     Child Loop BB0_66 Depth 2
                                        #     Child Loop BB0_71 Depth 2
                                        #     Child Loop BB0_161 Depth 2
                                        #     Child Loop BB0_175 Depth 2
                                        #     Child Loop BB0_86 Depth 2
                                        #     Child Loop BB0_89 Depth 2
                                        #     Child Loop BB0_94 Depth 2
	movq	176(%rsp), %rax         # 8-byte Reload
	movq	%rax, 56(%rbp)
	movq	%rax, 48(%rbp)
.Ltmp32:
	movq	%rbp, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
.Ltmp33:
# BB#178:                               #   in Loop: Header=BB0_177 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	je	.LBB0_24
# BB#179:
	movl	%eax, %r13d
	jmp	.LBB0_181
.LBB0_180:                              # %._crit_edge
.Ltmp75:
	leaq	8(%rsp), %rdi
	callq	_ZN8NArchive4NTar11COutArchive17WriteFinishHeaderEv
	movl	%eax, %r13d
.Ltmp76:
.LBB0_181:                              # %.loopexit329
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp80:
	callq	*16(%rax)
.Ltmp81:
# BB#182:                               # %_ZN9CMyComPtrI26CLimitedSequentialInStreamED2Ev.exit299
	movq	(%rbp), %rax
.Ltmp85:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp86:
# BB#183:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit262
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp90:
	callq	*16(%rax)
.Ltmp91:
.LBB0_184:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit254
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_186
# BB#185:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB0_186:                              # %_ZN8NArchive4NTar11COutArchiveD2Ev.exit250
	movl	%r13d, %eax
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_187:
.Ltmp92:
	jmp	.LBB0_206
.LBB0_188:
.Ltmp87:
	jmp	.LBB0_194
.LBB0_189:
.Ltmp82:
	jmp	.LBB0_199
.LBB0_190:
.Ltmp27:
	jmp	.LBB0_199
.LBB0_191:
.Ltmp19:
	jmp	.LBB0_194
.LBB0_192:
.Ltmp16:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB0_223
.LBB0_193:
.Ltmp13:
.LBB0_194:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rax, %rbx
	jmp	.LBB0_223
.LBB0_195:
.Ltmp10:
	jmp	.LBB0_206
.LBB0_196:
.Ltmp63:
	jmp	.LBB0_209
.LBB0_197:                              # %.loopexit.split-lp
.Ltmp77:
	jmp	.LBB0_220
.LBB0_198:
.Ltmp24:
.LBB0_199:                              # %_ZN9CMyComPtrI26CLimitedSequentialInStreamED2Ev.exit
	movq	%rax, %rbx
	jmp	.LBB0_222
.LBB0_200:
.Ltmp58:
	jmp	.LBB0_202
.LBB0_201:
.Ltmp49:
.LBB0_202:
	movq	%rax, %rbx
	movq	56(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_210
# BB#203:
	movq	(%rdi), %rax
.Ltmp59:
	callq	*16(%rax)
.Ltmp60:
	jmp	.LBB0_210
.LBB0_204:
.Ltmp2:
	jmp	.LBB0_206
.LBB0_205:
.Ltmp7:
.LBB0_206:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%rax, %rbx
	jmp	.LBB0_224
.LBB0_207:
.Ltmp74:
	jmp	.LBB0_209
.LBB0_208:
.Ltmp46:
.LBB0_209:
	movq	%rax, %rbx
.LBB0_210:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_212
# BB#211:
	callq	_ZdaPv
.LBB0_212:                              # %_ZN11CStringBaseIcED2Ev.exit.i
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_214
# BB#213:
	callq	_ZdaPv
.LBB0_214:                              # %_ZN11CStringBaseIcED2Ev.exit1.i
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_216
# BB#215:
	callq	_ZdaPv
.LBB0_216:                              # %_ZN11CStringBaseIcED2Ev.exit2.i
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_221
# BB#217:
	callq	_ZdaPv
	jmp	.LBB0_221
.LBB0_218:
.Ltmp37:
	jmp	.LBB0_220
.LBB0_219:                              # %.loopexit
.Ltmp34:
.LBB0_220:
	movq	%rax, %rbx
.LBB0_221:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp78:
	callq	*16(%rax)
.Ltmp79:
.LBB0_222:                              # %_ZN9CMyComPtrI26CLimitedSequentialInStreamED2Ev.exit
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp83:
	callq	*16(%rax)
.Ltmp84:
.LBB0_223:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp88:
	callq	*16(%rax)
.Ltmp89:
.LBB0_224:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_226
# BB#225:
	movq	(%rdi), %rax
.Ltmp93:
	callq	*16(%rax)
.Ltmp94:
.LBB0_226:                              # %_ZN8NArchive4NTar11COutArchiveD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB0_227:
.Ltmp95:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN8NArchive4NTar13UpdateArchiveEP9IInStreamP20ISequentialOutStreamRK13CObjectVectorINS0_7CItemExEERKS5_INS0_11CUpdateItemEEP22IArchiveUpdateCallback, .Lfunc_end0-_ZN8NArchive4NTar13UpdateArchiveEP9IInStreamP20ISequentialOutStreamRK13CObjectVectorINS0_7CItemExEERKS5_INS0_11CUpdateItemEEP22IArchiveUpdateCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\350\002"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\337\002"              # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp23-.Ltmp20         #   Call between .Ltmp20 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp31-.Ltmp28         #   Call between .Ltmp28 and .Ltmp31
	.long	.Ltmp77-.Lfunc_begin0   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin0   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp43-.Ltmp44         #   Call between .Ltmp44 and .Ltmp43
	.long	.Ltmp46-.Lfunc_begin0   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp64-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp69-.Ltmp64         #   Call between .Ltmp64 and .Ltmp69
	.long	.Ltmp74-.Lfunc_begin0   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp61-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp62-.Ltmp61         #   Call between .Ltmp61 and .Ltmp62
	.long	.Ltmp63-.Lfunc_begin0   #     jumps to .Ltmp63
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp71-.Ltmp66         #   Call between .Ltmp66 and .Ltmp71
	.long	.Ltmp74-.Lfunc_begin0   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp52-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp55-.Ltmp52         #   Call between .Ltmp52 and .Ltmp55
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp73-.Ltmp72         #   Call between .Ltmp72 and .Ltmp73
	.long	.Ltmp74-.Lfunc_begin0   #     jumps to .Ltmp74
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp33-.Ltmp32         #   Call between .Ltmp32 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin0   #     jumps to .Ltmp34
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin0   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp80-.Lfunc_begin0   # >> Call Site 22 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp82-.Lfunc_begin0   #     jumps to .Ltmp82
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin0   # >> Call Site 23 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin0   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin0   # >> Call Site 24 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin0   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin0   # >> Call Site 25 <<
	.long	.Ltmp59-.Ltmp91         #   Call between .Ltmp91 and .Ltmp59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin0   # >> Call Site 26 <<
	.long	.Ltmp94-.Ltmp59         #   Call between .Ltmp59 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin0   #     jumps to .Ltmp95
	.byte	1                       #   On action: 1
	.long	.Ltmp94-.Lfunc_begin0   # >> Call Site 27 <<
	.long	.Lfunc_end0-.Ltmp94     #   Call between .Ltmp94 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.section	.text._ZN8NArchive4NTar5CItemC2Ev,"axG",@progbits,_ZN8NArchive4NTar5CItemC2Ev,comdat
	.weak	_ZN8NArchive4NTar5CItemC2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar5CItemC2Ev,@function
_ZN8NArchive4NTar5CItemC2Ev:            # @_ZN8NArchive4NTar5CItemC2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -40
.Lcfi19:
	.cfi_offset %r12, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$4, %edi
	callq	_Znam
	movq	%rax, (%rbx)
	movb	$0, (%rax)
	movl	$4, 12(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%rbx)
.Ltmp96:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp97:
# BB#1:
	movq	%r15, 48(%rbx)
	movb	$0, (%r15)
	movl	$4, 60(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 64(%rbx)
.Ltmp99:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp100:
# BB#2:
	leaq	48(%rbx), %r15
	movq	%r12, 64(%rbx)
	movb	$0, (%r12)
	movl	$4, 76(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 80(%rbx)
.Ltmp102:
	movl	$4, %edi
	callq	_Znam
.Ltmp103:
# BB#3:
	movq	%rax, 80(%rbx)
	movb	$0, (%rax)
	movl	$4, 92(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB1_6:                                # %_ZN11CStringBaseIcED2Ev.exit
.Ltmp104:
	movq	%rax, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB1_7
	jmp	.LBB1_8
.LBB1_5:                                # %_ZN11CStringBaseIcED2Ev.exit.thread
.Ltmp101:
	movq	%rax, %r14
.LBB1_7:
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB1_8
.LBB1_4:
.Ltmp98:
	movq	%rax, %r14
.LBB1_8:                                # %_ZN11CStringBaseIcED2Ev.exit6
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_10
# BB#9:
	callq	_ZdaPv
.LBB1_10:                               # %_ZN11CStringBaseIcED2Ev.exit7
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN8NArchive4NTar5CItemC2Ev, .Lfunc_end1-_ZN8NArchive4NTar5CItemC2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp96-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp96
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin1   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin1  #     jumps to .Ltmp101
	.byte	0                       #   On action: cleanup
	.long	.Ltmp102-.Lfunc_begin1  # >> Call Site 4 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin1  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp103-.Lfunc_begin1  # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp103    #   Call between .Ltmp103 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive4NTar5CItemaSERKS1_,"axG",@progbits,_ZN8NArchive4NTar5CItemaSERKS1_,comdat
	.weak	_ZN8NArchive4NTar5CItemaSERKS1_
	.p2align	4, 0x90
	.type	_ZN8NArchive4NTar5CItemaSERKS1_,@function
_ZN8NArchive4NTar5CItemaSERKS1_:        # @_ZN8NArchive4NTar5CItemaSERKS1_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 48
.Lcfi27:
	.cfi_offset %rbx, -48
.Lcfi28:
	.cfi_offset %r12, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	%r15, %r14
	je	.LBB2_1
# BB#2:
	movl	$0, 8(%r15)
	movq	(%r15), %rax
	movb	$0, (%rax)
	movslq	8(%r14), %rax
	leaq	1(%rax), %r12
	movl	12(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB2_4
# BB#3:                                 # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i
	movq	(%r15), %rbx
	jmp	.LBB2_28
.LBB2_1:                                # %_ZN11CStringBaseIcEaSERKS0_.exit.thread
	movups	16(%r14), %xmm0
	movups	32(%r14), %xmm1
	movups	%xmm1, 32(%r15)
	movups	%xmm0, 16(%r15)
	jmp	.LBB2_118
.LBB2_4:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB2_27
# BB#5:                                 # %.preheader.i.i
	movslq	8(%r15), %rbp
	testq	%rbp, %rbp
	movq	(%r15), %rdi
	jle	.LBB2_25
# BB#6:                                 # %.lr.ph.preheader.i.i
	cmpl	$31, %ebp
	jbe	.LBB2_7
# BB#14:                                # %min.iters.checked
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB2_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB2_17
# BB#16:                                # %vector.memcheck
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_17
.LBB2_7:
	xorl	%ecx, %ecx
.LBB2_8:                                # %.lr.ph.i.i.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB2_11
# BB#9:                                 # %.lr.ph.i.i.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB2_10:                               # %.lr.ph.i.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_10
.LBB2_11:                               # %.lr.ph.i.i.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB2_26
# BB#12:                                # %.lr.ph.i.i.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB2_13
	jmp	.LBB2_26
.LBB2_25:                               # %._crit_edge.i.i
	testq	%rdi, %rdi
	je	.LBB2_27
.LBB2_26:                               # %._crit_edge.thread.i.i
	callq	_ZdaPv
.LBB2_27:                               # %._crit_edge17.i.i
	movq	%rbx, (%r15)
	movslq	8(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 12(%r15)
.LBB2_28:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB2_29:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB2_29
# BB#30:                                # %_ZN11CStringBaseIcEaSERKS0_.exit
	cmpq	%r15, %r14
	movl	8(%r14), %eax
	movl	%eax, 8(%r15)
	movups	16(%r14), %xmm0
	movups	32(%r14), %xmm1
	movups	%xmm1, 32(%r15)
	movups	%xmm0, 16(%r15)
	je	.LBB2_118
# BB#31:
	movl	$0, 56(%r15)
	movq	48(%r15), %rax
	movb	$0, (%rax)
	movslq	56(%r14), %rax
	leaq	1(%rax), %r12
	movl	60(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB2_33
# BB#32:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i8
	movq	48(%r15), %rbx
	jmp	.LBB2_57
.LBB2_33:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB2_56
# BB#34:                                # %.preheader.i.i9
	movslq	56(%r15), %rbp
	testq	%rbp, %rbp
	movq	48(%r15), %rdi
	jle	.LBB2_54
# BB#35:                                # %.lr.ph.preheader.i.i10
	cmpl	$31, %ebp
	jbe	.LBB2_36
# BB#43:                                # %min.iters.checked62
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB2_36
# BB#44:                                # %vector.memcheck73
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB2_46
# BB#45:                                # %vector.memcheck73
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_46
.LBB2_36:
	xorl	%ecx, %ecx
.LBB2_37:                               # %.lr.ph.i.i15.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB2_40
# BB#38:                                # %.lr.ph.i.i15.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB2_39:                               # %.lr.ph.i.i15.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_39
.LBB2_40:                               # %.lr.ph.i.i15.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB2_55
# BB#41:                                # %.lr.ph.i.i15.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB2_42:                               # %.lr.ph.i.i15
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB2_42
	jmp	.LBB2_55
.LBB2_54:                               # %._crit_edge.i.i11
	testq	%rdi, %rdi
	je	.LBB2_56
.LBB2_55:                               # %._crit_edge.thread.i.i16
	callq	_ZdaPv
.LBB2_56:                               # %._crit_edge17.i.i17
	movq	%rbx, 48(%r15)
	movslq	56(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 60(%r15)
.LBB2_57:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i18
	movq	48(%r14), %rax
	.p2align	4, 0x90
.LBB2_58:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB2_58
# BB#59:                                # %_ZN11CStringBaseIcEaSERKS0_.exit22
	cmpq	%r15, %r14
	movl	56(%r14), %eax
	movl	%eax, 56(%r15)
	je	.LBB2_118
# BB#60:
	movl	$0, 72(%r15)
	movq	64(%r15), %rax
	movb	$0, (%rax)
	movslq	72(%r14), %rax
	leaq	1(%rax), %r12
	movl	76(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB2_62
# BB#61:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i24
	movq	64(%r15), %rbx
	jmp	.LBB2_86
.LBB2_62:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB2_85
# BB#63:                                # %.preheader.i.i25
	movslq	72(%r15), %rbp
	testq	%rbp, %rbp
	movq	64(%r15), %rdi
	jle	.LBB2_83
# BB#64:                                # %.lr.ph.preheader.i.i26
	cmpl	$31, %ebp
	jbe	.LBB2_65
# BB#72:                                # %min.iters.checked89
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB2_65
# BB#73:                                # %vector.memcheck100
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB2_75
# BB#74:                                # %vector.memcheck100
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_75
.LBB2_65:
	xorl	%ecx, %ecx
.LBB2_66:                               # %.lr.ph.i.i31.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB2_69
# BB#67:                                # %.lr.ph.i.i31.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB2_68:                               # %.lr.ph.i.i31.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_68
.LBB2_69:                               # %.lr.ph.i.i31.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB2_84
# BB#70:                                # %.lr.ph.i.i31.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB2_71:                               # %.lr.ph.i.i31
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB2_71
	jmp	.LBB2_84
.LBB2_83:                               # %._crit_edge.i.i27
	testq	%rdi, %rdi
	je	.LBB2_85
.LBB2_84:                               # %._crit_edge.thread.i.i32
	callq	_ZdaPv
.LBB2_85:                               # %._crit_edge17.i.i33
	movq	%rbx, 64(%r15)
	movslq	72(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 76(%r15)
.LBB2_86:                               # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i34
	movq	64(%r14), %rax
	.p2align	4, 0x90
.LBB2_87:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB2_87
# BB#88:                                # %_ZN11CStringBaseIcEaSERKS0_.exit38
	cmpq	%r15, %r14
	movl	72(%r14), %eax
	movl	%eax, 72(%r15)
	je	.LBB2_118
# BB#89:
	movl	$0, 88(%r15)
	movq	80(%r15), %rax
	movb	$0, (%rax)
	movslq	88(%r14), %rax
	leaq	1(%rax), %r12
	movl	92(%r15), %ebp
	cmpl	%ebp, %r12d
	jne	.LBB2_91
# BB#90:                                # %._ZN11CStringBaseIcE11SetCapacityEi.exit_crit_edge.i40
	movq	80(%r15), %rbx
	jmp	.LBB2_115
.LBB2_91:
	cmpl	$-1, %eax
	movq	$-1, %rax
	movq	%r12, %rdi
	cmovlq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB2_114
# BB#92:                                # %.preheader.i.i41
	movslq	88(%r15), %rbp
	testq	%rbp, %rbp
	movq	80(%r15), %rdi
	jle	.LBB2_112
# BB#93:                                # %.lr.ph.preheader.i.i42
	cmpl	$31, %ebp
	jbe	.LBB2_94
# BB#101:                               # %min.iters.checked116
	movq	%rbp, %rcx
	andq	$-32, %rcx
	je	.LBB2_94
# BB#102:                               # %vector.memcheck127
	leaq	(%rdi,%rbp), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB2_104
# BB#103:                               # %vector.memcheck127
	leaq	(%rbx,%rbp), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_104
.LBB2_94:
	xorl	%ecx, %ecx
.LBB2_95:                               # %.lr.ph.i.i47.preheader
	movl	%ebp, %esi
	subl	%ecx, %esi
	leaq	-1(%rbp), %rax
	subq	%rcx, %rax
	andq	$7, %rsi
	je	.LBB2_98
# BB#96:                                # %.lr.ph.i.i47.prol.preheader
	negq	%rsi
	.p2align	4, 0x90
.LBB2_97:                               # %.lr.ph.i.i47.prol
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdi,%rcx), %edx
	movb	%dl, (%rbx,%rcx)
	incq	%rcx
	incq	%rsi
	jne	.LBB2_97
.LBB2_98:                               # %.lr.ph.i.i47.prol.loopexit
	cmpq	$7, %rax
	jb	.LBB2_113
# BB#99:                                # %.lr.ph.i.i47.preheader.new
	subq	%rcx, %rbp
	leaq	7(%rbx,%rcx), %rdx
	leaq	7(%rdi,%rcx), %rcx
	.p2align	4, 0x90
.LBB2_100:                              # %.lr.ph.i.i47
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movb	%al, -7(%rdx)
	movzbl	-6(%rcx), %eax
	movb	%al, -6(%rdx)
	movzbl	-5(%rcx), %eax
	movb	%al, -5(%rdx)
	movzbl	-4(%rcx), %eax
	movb	%al, -4(%rdx)
	movzbl	-3(%rcx), %eax
	movb	%al, -3(%rdx)
	movzbl	-2(%rcx), %eax
	movb	%al, -2(%rdx)
	movzbl	-1(%rcx), %eax
	movb	%al, -1(%rdx)
	movzbl	(%rcx), %eax
	movb	%al, (%rdx)
	addq	$8, %rdx
	addq	$8, %rcx
	addq	$-8, %rbp
	jne	.LBB2_100
	jmp	.LBB2_113
.LBB2_17:                               # %vector.body.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB2_20
	jmp	.LBB2_21
.LBB2_112:                              # %._crit_edge.i.i43
	testq	%rdi, %rdi
	je	.LBB2_114
.LBB2_113:                              # %._crit_edge.thread.i.i48
	callq	_ZdaPv
.LBB2_114:                              # %._crit_edge17.i.i49
	movq	%rbx, 80(%r15)
	movslq	88(%r15), %rax
	movb	$0, (%rbx,%rax)
	movl	%r12d, 92(%r15)
.LBB2_115:                              # %_ZN11CStringBaseIcE11SetCapacityEi.exit.i50
	movq	80(%r14), %rax
	.p2align	4, 0x90
.LBB2_116:                              # =>This Inner Loop Header: Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	movb	%cl, (%rbx)
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB2_116
# BB#117:                               # %_Z12MyStringCopyIcEPT_S1_PKS0_.exit.i53
	movl	88(%r14), %eax
	movl	%eax, 88(%r15)
.LBB2_118:                              # %_ZN11CStringBaseIcEaSERKS0_.exit54
	movb	106(%r14), %al
	movb	%al, 106(%r15)
	movzwl	104(%r14), %eax
	movw	%ax, 104(%r15)
	movq	96(%r14), %rax
	movq	%rax, 96(%r15)
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_46:                               # %vector.body58.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_47
# BB#48:                                # %vector.body58.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_49:                               # %vector.body58.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB2_49
	jmp	.LBB2_50
.LBB2_75:                               # %vector.body85.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_76
# BB#77:                                # %vector.body85.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB2_78:                               # %vector.body85.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB2_78
	jmp	.LBB2_79
.LBB2_18:
	xorl	%edx, %edx
.LBB2_21:                               # %vector.body.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB2_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
	.p2align	4, 0x90
.LBB2_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB2_23
.LBB2_24:                               # %middle.block
	cmpq	%rcx, %rbp
	jne	.LBB2_8
	jmp	.LBB2_26
.LBB2_104:                              # %vector.body112.preheader
	leaq	-32(%rcx), %r8
	movl	%r8d, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_105
# BB#106:                               # %vector.body112.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
.LBB2_107:                              # %vector.body112.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rdx), %xmm0
	movups	16(%rdi,%rdx), %xmm1
	movups	%xmm0, (%rbx,%rdx)
	movups	%xmm1, 16(%rbx,%rdx)
	addq	$32, %rdx
	incq	%rsi
	jne	.LBB2_107
	jmp	.LBB2_108
.LBB2_47:
	xorl	%edx, %edx
.LBB2_50:                               # %vector.body58.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB2_53
# BB#51:                                # %vector.body58.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB2_52:                               # %vector.body58
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB2_52
.LBB2_53:                               # %middle.block59
	cmpq	%rcx, %rbp
	jne	.LBB2_37
	jmp	.LBB2_55
.LBB2_76:
	xorl	%edx, %edx
.LBB2_79:                               # %vector.body85.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB2_82
# BB#80:                                # %vector.body85.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB2_81:                               # %vector.body85
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB2_81
.LBB2_82:                               # %middle.block86
	cmpq	%rcx, %rbp
	jne	.LBB2_66
	jmp	.LBB2_84
.LBB2_105:
	xorl	%edx, %edx
.LBB2_108:                              # %vector.body112.prol.loopexit
	cmpq	$96, %r8
	jb	.LBB2_111
# BB#109:                               # %vector.body112.preheader.new
	movq	%rcx, %r8
	subq	%rdx, %r8
	leaq	112(%rbx,%rdx), %rsi
	leaq	112(%rdi,%rdx), %rdx
.LBB2_110:                              # %vector.body112
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rdx
	addq	$-128, %r8
	jne	.LBB2_110
.LBB2_111:                              # %middle.block113
	cmpq	%rcx, %rbp
	jne	.LBB2_95
	jmp	.LBB2_113
.Lfunc_end2:
	.size	_ZN8NArchive4NTar5CItemaSERKS1_, .Lfunc_end2-_ZN8NArchive4NTar5CItemaSERKS1_
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
