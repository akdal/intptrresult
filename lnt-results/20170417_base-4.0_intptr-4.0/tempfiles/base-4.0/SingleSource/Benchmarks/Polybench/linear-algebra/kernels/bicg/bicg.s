	.text
	.file	"bicg.bc"
	.globl	polybench_flush_cache
	.p2align	4, 0x90
	.type	polybench_flush_cache,@function
polybench_flush_cache:                  # @polybench_flush_cache
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	polybench_flush_cache, .Lfunc_end0-polybench_flush_cache
	.cfi_endproc

	.globl	polybench_prepare_instruments
	.p2align	4, 0x90
	.type	polybench_prepare_instruments,@function
polybench_prepare_instruments:          # @polybench_prepare_instruments
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end1:
	.size	polybench_prepare_instruments, .Lfunc_end1-polybench_prepare_instruments
	.cfi_endproc

	.globl	polybench_timer_start
	.p2align	4, 0x90
	.type	polybench_timer_start,@function
polybench_timer_start:                  # @polybench_timer_start
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_start(%rip)
	retq
.Lfunc_end2:
	.size	polybench_timer_start, .Lfunc_end2-polybench_timer_start
	.cfi_endproc

	.globl	polybench_timer_stop
	.p2align	4, 0x90
	.type	polybench_timer_stop,@function
polybench_timer_stop:                   # @polybench_timer_stop
	.cfi_startproc
# BB#0:
	movq	$0, polybench_t_end(%rip)
	retq
.Lfunc_end3:
	.size	polybench_timer_stop, .Lfunc_end3-polybench_timer_stop
	.cfi_endproc

	.globl	polybench_timer_print
	.p2align	4, 0x90
	.type	polybench_timer_print,@function
polybench_timer_print:                  # @polybench_timer_print
	.cfi_startproc
# BB#0:
	movsd	polybench_t_end(%rip), %xmm0 # xmm0 = mem[0],zero
	subsd	polybench_t_start(%rip), %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	polybench_timer_print, .Lfunc_end4-polybench_timer_print
	.cfi_endproc

	.globl	polybench_alloc_data
	.p2align	4, 0x90
	.type	polybench_alloc_data,@function
polybench_alloc_data:                   # @polybench_alloc_data
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movslq	%esi, %rdx
	imulq	%rdi, %rdx
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB5_3
# BB#2:                                 # %xmalloc.exit
	popq	%rcx
	retq
.LBB5_3:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end5:
	.size	polybench_alloc_data, .Lfunc_end5-polybench_alloc_data
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI6_1:
	.quad	4661014508095930368     # double 4000
.LCPI6_3:
	.quad	4532020583610935537     # double 1.0000000000000001E-5
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 96
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$128000000, %edx        # imm = 0x7A12000
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#1:
	movq	(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_44
# BB#2:                                 # %polybench_alloc_data.exit
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#3:                                 # %polybench_alloc_data.exit
	movq	(%rsp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_44
# BB#4:                                 # %polybench_alloc_data.exit54
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#5:                                 # %polybench_alloc_data.exit54
	movq	(%rsp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB6_44
# BB#6:                                 # %polybench_alloc_data.exit56
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#7:                                 # %polybench_alloc_data.exit56
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_44
# BB#8:                                 # %polybench_alloc_data.exit58
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#9:                                 # %polybench_alloc_data.exit58
	movq	(%rsp), %r13
	testq	%r13, %r13
	je	.LBB6_44
# BB#10:                                # %polybench_alloc_data.exit60
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#11:                                # %polybench_alloc_data.exit60
	movq	(%rsp), %r12
	testq	%r12, %r12
	je	.LBB6_44
# BB#12:                                # %polybench_alloc_data.exit62
	movq	$0, (%rsp)
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$32000, %edx            # imm = 0x7D00
	callq	posix_memalign
	testl	%eax, %eax
	jne	.LBB6_44
# BB#13:                                # %polybench_alloc_data.exit62
	movq	(%rsp), %r14
	testq	%r14, %r14
	je	.LBB6_44
# BB#14:                                # %polybench_alloc_data.exit64
	xorl	%eax, %eax
	movsd	.LCPI6_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_15:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rbx,%rax,8)
	leal	1(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rbx,%rax,8)
	leal	2(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rbx,%rax,8)
	leal	3(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 24(%rbx,%rax,8)
	leal	4(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 32(%rbx,%rax,8)
	addq	$5, %rax
	cmpq	$4000, %rax             # imm = 0xFA0
	jne	.LBB6_15
# BB#16:                                # %.preheader.i.preheader
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	leaq	8(%rbp), %rax
	xorl	%ecx, %ecx
	movsd	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movq	32(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_17:                               # %.preheader.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm2, %xmm3
	mulsd	%xmm0, %xmm3
	movsd	%xmm3, (%r13,%rcx,8)
	movq	$-4000, %rdx            # imm = 0xF060
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	4001(%rdx), %edi
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edi, %xmm3
	mulsd	%xmm2, %xmm3
	divsd	%xmm1, %xmm3
	movsd	%xmm3, -8(%rsi)
	leal	4002(%rdx), %edi
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edi, %xmm3
	mulsd	%xmm2, %xmm3
	divsd	%xmm1, %xmm3
	movsd	%xmm3, (%rsi)
	addq	$16, %rsi
	addq	$2, %rdx
	jne	.LBB6_18
# BB#19:                                #   in Loop: Header=BB6_17 Depth=1
	incq	%rcx
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rcx             # imm = 0xFA0
	jne	.LBB6_17
# BB#20:                                # %init_array.exit
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movl	$32000, %edx            # imm = 0x7D00
	movq	%rbp, %rdi
	callq	memset
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_21:                               # %.preheader.i66
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_22 Depth 2
	movq	$0, (%rsi,%r15,8)
	movq	%rax, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_22:                               #   Parent Loop BB6_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r13,%r15,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rcx), %xmm0
	addsd	(%rbp,%rdx,8), %xmm0
	movsd	%xmm0, (%rbp,%rdx,8)
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx,%rdx,8), %xmm0
	addsd	(%rsi,%r15,8), %xmm0
	movsd	%xmm0, (%rsi,%r15,8)
	incq	%rdx
	addq	$8, %rcx
	cmpq	$4000, %rdx             # imm = 0xFA0
	jne	.LBB6_22
# BB#23:                                #   in Loop: Header=BB6_21 Depth=1
	incq	%r15
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %r15             # imm = 0xFA0
	jne	.LBB6_21
# BB#24:                                # %kernel_bicg.exit
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$32000, %edx            # imm = 0x7D00
	movq	%r12, %rdi
	callq	memset
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB6_25:                               # %.preheader.i73
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_26 Depth 2
	movq	$0, (%r14,%rbp,8)
	movq	%rax, %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_26:                               #   Parent Loop BB6_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%r13,%rbp,8), %xmm0    # xmm0 = mem[0],zero
	mulsd	(%rcx), %xmm0
	addsd	(%r12,%rdx,8), %xmm0
	movsd	%xmm0, (%r12,%rdx,8)
	movsd	(%rcx), %xmm0           # xmm0 = mem[0],zero
	mulsd	(%rbx,%rdx,8), %xmm0
	addsd	(%r14,%rbp,8), %xmm0
	movsd	%xmm0, (%r14,%rbp,8)
	incq	%rdx
	addq	$8, %rcx
	cmpq	$4000, %rdx             # imm = 0xFA0
	jne	.LBB6_26
# BB#27:                                #   in Loop: Header=BB6_25 Depth=1
	incq	%rbp
	addq	$32000, %rax            # imm = 0x7D00
	cmpq	$4000, %rbp             # imm = 0xFA0
	jne	.LBB6_25
# BB#28:                                # %kernel_bicg_StrictFP.exit.preheader
	movl	$1, %edx
	movapd	.LCPI6_2(%rip), %xmm2   # xmm2 = [nan,nan]
	movsd	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_29:                               # %kernel_bicg_StrictFP.exit
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%rbp,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r12,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_30
# BB#32:                                # %kernel_bicg_StrictFP.exit.1135
                                        #   in Loop: Header=BB6_29 Depth=1
	movsd	(%rbp,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r12,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_31
# BB#33:                                #   in Loop: Header=BB6_29 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$4000, %rdx             # imm = 0xFA0
	movq	%rax, %rdx
	jl	.LBB6_29
# BB#34:                                # %check_FP.exit.preheader
	movl	$1, %edx
	.p2align	4, 0x90
.LBB6_35:                               # %check_FP.exit
                                        # =>This Inner Loop Header: Depth=1
	movsd	-8(%rcx,%rdx,8), %xmm0  # xmm0 = mem[0],zero
	movsd	-8(%r14,%rdx,8), %xmm1  # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_30
# BB#36:                                # %check_FP.exit.1134
                                        #   in Loop: Header=BB6_35 Depth=1
	movsd	(%rcx,%rdx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rdx,8), %xmm1    # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm4
	subsd	%xmm1, %xmm4
	andpd	%xmm2, %xmm4
	ucomisd	%xmm3, %xmm4
	ja	.LBB6_31
# BB#37:                                #   in Loop: Header=BB6_35 Depth=1
	leaq	2(%rdx), %rax
	incq	%rdx
	cmpq	$4000, %rdx             # imm = 0xFA0
	movq	%rax, %rdx
	jl	.LBB6_35
# BB#38:                                # %check_FP.exit85
	movl	$64001, %edi            # imm = 0xFA01
	callq	malloc
	movb	$0, 64000(%rax)
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_39:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rsi), %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, (%rax,%rsi,2)
	movb	%dl, 1(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$8, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 2(%rax,%rsi,2)
	movb	%dl, 3(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 4(%rax,%rsi,2)
	movb	%dl, 5(%rax,%rsi,2)
	movl	%ecx, %edx
	shrl	$24, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 6(%rax,%rsi,2)
	movb	%dl, 7(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 8(%rax,%rsi,2)
	movb	%dl, 9(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 10(%rax,%rsi,2)
	movb	%dl, 11(%rax,%rsi,2)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 12(%rax,%rsi,2)
	movb	%dl, 13(%rax,%rsi,2)
	shrq	$56, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, 14(%rax,%rsi,2)
	movb	%cl, 15(%rax,%rsi,2)
	addq	$8, %rsi
	cmpq	$32000, %rsi            # imm = 0x7D00
	jne	.LBB6_39
# BB#40:
	movb	$0, 4000(%rax)
	movq	stderr(%rip), %rsi
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	fputs
	movq	8(%rsp), %rdi           # 8-byte Reload
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_41:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rax), %rcx
	movl	%ecx, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, (%rdi,%rax,2)
	movb	%dl, 1(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$8, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 2(%rdi,%rax,2)
	movb	%dl, 3(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$16, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 4(%rdi,%rax,2)
	movb	%dl, 5(%rdi,%rax,2)
	movl	%ecx, %edx
	shrl	$24, %edx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 6(%rdi,%rax,2)
	movb	%dl, 7(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 8(%rdi,%rax,2)
	movb	%dl, 9(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$40, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 10(%rdi,%rax,2)
	movb	%dl, 11(%rdi,%rax,2)
	movq	%rcx, %rdx
	shrq	$48, %rdx
	andb	$15, %dl
	orb	$48, %dl
	movb	%dl, 12(%rdi,%rax,2)
	movb	%dl, 13(%rdi,%rax,2)
	shrq	$56, %rcx
	andb	$15, %cl
	orb	$48, %cl
	movb	%cl, 14(%rdi,%rax,2)
	movb	%cl, 15(%rdi,%rax,2)
	addq	$8, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jne	.LBB6_41
# BB#42:                                # %print_array.exit
	movb	$0, 4000(%rdi)
	movq	stderr(%rip), %rsi
	callq	fputs
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	movq	%rbp, %rdi
	callq	free
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	%r13, %rdi
	callq	free
	movq	%r12, %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	jmp	.LBB6_43
.LBB6_30:                               # %check_FP.exit.threadsplit
	decq	%rdx
.LBB6_31:                               # %check_FP.exit.thread
	movq	stderr(%rip), %rdi
	movsd	.LCPI6_3(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$.L.str.2, %esi
	movb	$3, %al
	movl	%edx, %ecx
	callq	fprintf
	movl	$1, %eax
.LBB6_43:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_44:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$50, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	main, .Lfunc_end6-main
	.cfi_endproc

	.type	polybench_papi_counters_threadid,@object # @polybench_papi_counters_threadid
	.bss
	.globl	polybench_papi_counters_threadid
	.p2align	2
polybench_papi_counters_threadid:
	.long	0                       # 0x0
	.size	polybench_papi_counters_threadid, 4

	.type	polybench_program_total_flops,@object # @polybench_program_total_flops
	.globl	polybench_program_total_flops
	.p2align	3
polybench_program_total_flops:
	.quad	0                       # double 0
	.size	polybench_program_total_flops, 8

	.type	polybench_t_start,@object # @polybench_t_start
	.comm	polybench_t_start,8,8
	.type	polybench_t_end,@object # @polybench_t_end
	.comm	polybench_t_end,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%0.6f\n"
	.size	.L.str, 7

	.type	polybench_c_start,@object # @polybench_c_start
	.comm	polybench_c_start,8,8
	.type	polybench_c_end,@object # @polybench_c_end
	.comm	polybench_c_end,8,8
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"[PolyBench] posix_memalign: cannot allocate memory"
	.size	.L.str.1, 51

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"A[%d] = %lf and B[%d] = %lf differ more than FP_ABSTOLERANCE = %lf\n"
	.size	.L.str.2, 68


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
