	.text
	.file	"btAxisSweep3.bc"
	.globl	_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb
	.p2align	4, 0x90
	.type	_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb,@function
_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb: # @_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%r8, %r10
	movl	%ecx, %r11d
	movq	%rdi, %rbx
	movzbl	%r9b, %eax
	movl	$65534, %ecx            # imm = 0xFFFE
	movl	$65535, %r8d            # imm = 0xFFFF
	movl	%r11d, %r9d
	pushq	%rax
.Lcfi2:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi3:
	.cfi_adjust_cfa_offset 8
	callq	_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb
	addq	$16, %rsp
.Lcfi4:
	.cfi_adjust_cfa_offset -16
	movq	$_ZTV12btAxisSweep3+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb, .Lfunc_end0-_ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb,"axG",@progbits,_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb,comdat
	.weak	_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb,@function
_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb: # @_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 64
.Lcfi12:
	.cfi_offset %rbx, -56
.Lcfi13:
	.cfi_offset %r12, -48
.Lcfi14:
	.cfi_offset %r13, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movb	72(%rsp), %al
	movq	64(%rsp), %rsi
	movq	$_ZTV20btAxisSweep3InternalItE+16, (%rbx)
	movw	%cx, 8(%rbx)
	movw	%r8w, 10(%rbx)
	movq	%rsi, 128(%rbx)
	movq	$0, 136(%rbx)
	movb	$0, 144(%rbx)
	movl	$0, 148(%rbx)
	movq	$0, 152(%rbx)
	incl	%r12d
	testq	%rsi, %rsi
	jne	.LBB1_2
# BB#1:
	movl	$128, %edi
	movl	%eax, %r13d
	movl	$16, %esi
	movq	%rdx, %r14
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	_ZN28btHashedOverlappingPairCacheC1Ev
	movl	%r13d, %eax
	movq	%r14, %rdx
	movq	%rbp, 128(%rbx)
	movb	$1, 144(%rbx)
.LBB1_2:
	leaq	12(%rbx), %r14
	leaq	28(%rbx), %r13
	testb	%al, %al
	jne	.LBB1_4
# BB#3:
	movl	$40, %edi
	movl	$16, %esi
	movq	%r15, (%rsp)            # 8-byte Spill
	movq	%rdx, %r15
	callq	_Z22btAlignedAllocInternalmi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	movups	%xmm0, 8(%rax)
	movq	$_ZTV15btNullPairCache+16, (%rax)
	movb	$1, 32(%rax)
	movq	$0, 24(%rax)
	movl	$0, 12(%rax)
	movl	$0, 16(%rax)
	movq	%rax, 160(%rbx)
	movl	$224, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movq	160(%rbx), %rsi
	movq	%rbp, %rdi
	callq	_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache
	movq	%r15, %rdx
	movq	(%rsp), %r15            # 8-byte Reload
	movq	%rbp, 152(%rbx)
	movb	$1, 221(%rbp)
.LBB1_4:                                # %_ZN20btAxisSweep3InternalItE6HandlenaEm.exit
	movups	(%r15), %xmm0
	movups	%xmm0, (%r14)
	movups	(%rdx), %xmm0
	movups	%xmm0, (%r13)
	movsd	28(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	12(%rbx), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	20(%rbx), %xmm1
	movzwl	10(%rbx), %eax
	cvtsi2ssl	%eax, %xmm2
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	divps	%xmm0, %xmm3
	divss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, 44(%rbx)
	movlps	%xmm0, 52(%rbx)
	movzwl	%r12w, %r14d
	movq	%r14, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,4), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	testw	%r14w, %r14w
	je	.LBB1_7
# BB#5:
	leaq	(%r14,%r14,4), %rcx
	shlq	$4, %rcx
	addq	%rax, %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB1_6:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rdx)
	movq	$0, 16(%rdx)
	addq	$80, %rdx
	cmpq	%rcx, %rdx
	jne	.LBB1_6
.LBB1_7:                                # %.loopexit
	movq	%rax, 64(%rbx)
	movw	%r14w, 62(%rbx)
	movw	$0, 60(%rbx)
	movw	$1, 72(%rbx)
	cmpl	$2, %r14d
	jb	.LBB1_16
# BB#8:                                 # %.lr.ph.preheader
	leal	7(%r14), %esi
	leaq	-2(%r14), %r8
	andq	$7, %rsi
	je	.LBB1_9
# BB#10:                                # %.lr.ph.prol.preheader
	leaq	140(%rax), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	2(%rcx), %edx
	movw	%dx, (%rdi)
	addq	$80, %rdi
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB1_11
# BB#12:                                # %.lr.ph.prol.loopexit.unr-lcssa
	incq	%rcx
	cmpq	$7, %r8
	jae	.LBB1_14
	jmp	.LBB1_16
.LBB1_9:
	movl	$1, %ecx
	cmpq	$7, %r8
	jb	.LBB1_16
.LBB1_14:                               # %.lr.ph.preheader.new
	movq	%rcx, %r8
	subq	%r14, %r8
	leaq	(%rcx,%rcx,4), %rsi
	shlq	$4, %rsi
	leaq	620(%rax,%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%rdi), %ebp
	movw	%bp, -560(%rsi)
	leal	2(%rcx,%rdi), %ebp
	movw	%bp, -480(%rsi)
	leal	3(%rcx,%rdi), %ebp
	movw	%bp, -400(%rsi)
	leal	4(%rcx,%rdi), %ebp
	movw	%bp, -320(%rsi)
	leal	5(%rcx,%rdi), %ebp
	movw	%bp, -240(%rsi)
	leal	6(%rcx,%rdi), %ebp
	movw	%bp, -160(%rsi)
	leal	7(%rcx,%rdi), %ebp
	movw	%bp, -80(%rsi)
	leal	8(%rcx,%rdi), %ebp
	movw	%bp, (%rsi)
	addq	$8, %rdi
	addq	$640, %rsi              # imm = 0x280
	movq	%r8, %rdx
	addq	%rdi, %rdx
	jne	.LBB1_15
.LBB1_16:                               # %._crit_edge
	leaq	(%r14,%r14,4), %rcx
	shlq	$4, %rcx
	movw	$0, -20(%rax,%rcx)
	leaq	(,%r14,8), %rbp
	movl	$16, %esi
	movq	%rbp, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 104(%rbx)
	movq	%rax, 80(%rbx)
	movl	$16, %esi
	movq	%rbp, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 112(%rbx)
	movq	%rax, 88(%rbx)
	movl	$16, %esi
	movq	%rbp, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 120(%rbx)
	movq	%rax, 96(%rbx)
	movq	64(%rbx), %rax
	movq	$0, (%rax)
	movw	$0, 60(%rax)
	movw	$1, 66(%rax)
	movq	80(%rbx), %rcx
	movw	$0, (%rcx)
	movw	$0, 2(%rcx)
	movzwl	10(%rbx), %edx
	movw	%dx, 4(%rcx)
	movw	$0, 6(%rcx)
	movw	$0, 62(%rax)
	movw	$1, 68(%rax)
	movq	88(%rbx), %rcx
	movw	$0, (%rcx)
	movw	$0, 2(%rcx)
	movzwl	10(%rbx), %edx
	movw	%dx, 4(%rcx)
	movw	$0, 6(%rcx)
	movw	$0, 64(%rax)
	movw	$1, 70(%rax)
	movq	96(%rbx), %rax
	movw	$0, (%rax)
	movw	$0, 2(%rax)
	movzwl	10(%rbx), %ecx
	movw	%cx, 4(%rax)
	movw	$0, 6(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb, .Lfunc_end1-_ZN20btAxisSweep3InternalItEC2ERK9btVector3S3_tttP22btOverlappingPairCacheb
	.cfi_endproc

	.text
	.globl	_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb
	.p2align	4, 0x90
	.type	_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb,@function
_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb: # @_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%r8, %r10
	movl	%ecx, %r11d
	movq	%rdi, %rbx
	movzbl	%r9b, %eax
	movl	$-2, %ecx
	movl	$2147483647, %r8d       # imm = 0x7FFFFFFF
	movl	%r11d, %r9d
	pushq	%rax
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb
	addq	$16, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -16
	movq	$_ZTV17bt32BitAxisSweep3+16, (%rbx)
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb, .Lfunc_end2-_ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	65535                   # 0xffff
	.long	65535                   # 0xffff
	.long	65535                   # 0xffff
	.long	65535                   # 0xffff
.LCPI3_1:
	.long	1258291200              # 0x4b000000
	.long	1258291200              # 0x4b000000
	.long	1258291200              # 0x4b000000
	.long	1258291200              # 0x4b000000
.LCPI3_2:
	.long	1392508928              # 0x53000000
	.long	1392508928              # 0x53000000
	.long	1392508928              # 0x53000000
	.long	1392508928              # 0x53000000
.LCPI3_3:
	.long	3539992704              # float -5.49764202E+11
	.long	3539992704              # float -5.49764202E+11
	.long	3539992704              # float -5.49764202E+11
	.long	3539992704              # float -5.49764202E+11
	.section	.text._ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb,"axG",@progbits,_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb,comdat
	.weak	_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb,@function
_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb: # @_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 80
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movq	%rdi, %r12
	movb	88(%rsp), %bl
	movq	80(%rsp), %rax
	movq	$_ZTV20btAxisSweep3InternalIjE+16, (%r12)
	movl	%ecx, 8(%r12)
	movl	%r8d, 12(%r12)
	movq	%rax, 136(%r12)
	movq	$0, 144(%r12)
	movb	$0, 152(%r12)
	movl	$0, 156(%r12)
	movq	$0, 160(%r12)
	testq	%rax, %rax
	jne	.LBB3_2
# BB#1:
	movl	$128, %edi
	movq	%rsi, %r14
	movl	$16, %esi
	movq	%rdx, %rbp
	callq	_Z22btAlignedAllocInternalmi
	movq	%r13, %r15
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	_ZN28btHashedOverlappingPairCacheC1Ev
	movq	%r14, %rsi
	movq	%rbp, %rdx
	movq	%r13, 136(%r12)
	movq	%r15, %r13
	movb	$1, 152(%r12)
.LBB3_2:
	leaq	16(%r12), %r15
	leaq	32(%r12), %r14
	leal	1(%r13), %ebp
	testb	%bl, %bl
	jne	.LBB3_4
# BB#3:
	movl	$40, %edi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	$16, %esi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	_Z22btAlignedAllocInternalmi
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rax)
	movups	%xmm0, 8(%rax)
	movq	$_ZTV15btNullPairCache+16, (%rax)
	movb	$1, 32(%rax)
	movq	$0, 24(%rax)
	movl	$0, 12(%rax)
	movl	$0, 16(%rax)
	movq	%rax, 168(%r12)
	movl	$224, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%r13, %rbx
	movq	%rax, %r13
	movq	168(%r12), %rsi
	movq	%r13, %rdi
	callq	_ZN16btDbvtBroadphaseC1EP22btOverlappingPairCache
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r13, 160(%r12)
	movb	$1, 221(%r13)
	movq	%rbx, %r13
.LBB3_4:                                # %_ZN20btAxisSweep3InternalIjE6HandlenaEm.exit
	movups	(%rsi), %xmm0
	movups	%xmm0, (%r15)
	movups	(%rdx), %xmm0
	movups	%xmm0, (%r14)
	movsd	32(%r12), %xmm0         # xmm0 = mem[0],zero
	movsd	16(%r12), %xmm1         # xmm1 = mem[0],zero
	subps	%xmm1, %xmm0
	movss	40(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%r12), %xmm1
	movl	12(%r12), %eax
	cvtsi2ssq	%rax, %xmm2
	movd	%eax, %xmm3
	pshufd	$80, %xmm3, %xmm3       # xmm3 = xmm3[0,0,1,1]
	movdqa	.LCPI3_0(%rip), %xmm4   # xmm4 = [65535,65535,65535,65535]
	pand	%xmm3, %xmm4
	por	.LCPI3_1(%rip), %xmm4
	psrld	$16, %xmm3
	por	.LCPI3_2(%rip), %xmm3
	addps	.LCPI3_3(%rip), %xmm3
	addps	%xmm4, %xmm3
	divps	%xmm0, %xmm3
	divss	%xmm1, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, 48(%r12)
	movlps	%xmm0, 56(%r12)
	movl	%ebp, %r15d
	movq	%r15, %rax
	shlq	$5, %rax
	leaq	(%rax,%rax,2), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	testl	%ebp, %ebp
	je	.LBB3_7
# BB#5:
	leaq	(%r15,%r15,2), %rcx
	shlq	$5, %rcx
	addq	%rax, %rcx
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movq	$0, (%rdx)
	movq	$0, 16(%rdx)
	addq	$96, %rdx
	cmpq	%rcx, %rdx
	jne	.LBB3_6
.LBB3_7:                                # %.loopexit
	movq	%rax, 72(%r12)
	movl	%ebp, 68(%r12)
	movl	$0, 64(%r12)
	movl	$1, 80(%r12)
	cmpl	$2, %ebp
	jb	.LBB3_16
# BB#8:                                 # %.lr.ph.preheader
	leal	7(%r15), %esi
	leaq	-2(%r15), %rdx
	andq	$7, %rsi
	je	.LBB3_9
# BB#10:                                # %.lr.ph.prol.preheader
	leaq	156(%rax), %rdi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_11:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	leal	2(%rcx), %ebp
	movl	%ebp, (%rdi)
	addq	$96, %rdi
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB3_11
# BB#12:                                # %.lr.ph.prol.loopexit.unr-lcssa
	incq	%rcx
	cmpq	$7, %rdx
	jae	.LBB3_14
	jmp	.LBB3_16
.LBB3_9:
	movl	$1, %ecx
	cmpq	$7, %rdx
	jb	.LBB3_16
.LBB3_14:                               # %.lr.ph.preheader.new
	movq	%rcx, %rbx
	subq	%r15, %rbx
	leaq	(%rcx,%rcx,2), %rsi
	shlq	$5, %rsi
	leaq	732(%rax,%rsi), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rcx,%rdi), %ebp
	movl	%ebp, -672(%rsi)
	leal	2(%rcx,%rdi), %ebp
	movl	%ebp, -576(%rsi)
	leal	3(%rcx,%rdi), %ebp
	movl	%ebp, -480(%rsi)
	leal	4(%rcx,%rdi), %ebp
	movl	%ebp, -384(%rsi)
	leal	5(%rcx,%rdi), %ebp
	movl	%ebp, -288(%rsi)
	leal	6(%rcx,%rdi), %ebp
	movl	%ebp, -192(%rsi)
	leal	7(%rcx,%rdi), %ebp
	movl	%ebp, -96(%rsi)
	leal	8(%rcx,%rdi), %ebp
	movl	%ebp, (%rsi)
	addq	$8, %rdi
	addq	$768, %rsi              # imm = 0x300
	movq	%rbx, %rdx
	addq	%rdi, %rdx
	jne	.LBB3_15
.LBB3_16:                               # %._crit_edge
	movl	%r13d, %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	movl	$0, 60(%rax,%rcx)
	shlq	$4, %r15
	movl	$16, %esi
	movq	%r15, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 112(%r12)
	movq	%rax, 88(%r12)
	movl	$16, %esi
	movq	%r15, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 120(%r12)
	movq	%rax, 96(%r12)
	movl	$16, %esi
	movq	%r15, %rdi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, 128(%r12)
	movq	%rax, 104(%r12)
	movq	72(%r12), %rax
	movq	$0, (%rax)
	movl	$0, 60(%rax)
	movl	$1, 72(%rax)
	movq	88(%r12), %rcx
	movl	$0, (%rcx)
	movl	$0, 4(%rcx)
	movl	12(%r12), %edx
	movl	%edx, 8(%rcx)
	movl	$0, 12(%rcx)
	movl	$0, 64(%rax)
	movl	$1, 76(%rax)
	movq	96(%r12), %rcx
	movl	$0, (%rcx)
	movl	$0, 4(%rcx)
	movl	12(%r12), %edx
	movl	%edx, 8(%rcx)
	movl	$0, 12(%rcx)
	movl	$0, 68(%rax)
	movl	$1, 80(%rax)
	movq	104(%r12), %rax
	movl	$0, (%rax)
	movl	$0, 4(%rax)
	movl	12(%r12), %ecx
	movl	%ecx, 8(%rax)
	movl	$0, 12(%rax)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb, .Lfunc_end3-_ZN20btAxisSweep3InternalIjEC2ERK9btVector3S3_jjjP22btOverlappingPairCacheb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItED2Ev,"axG",@progbits,_ZN20btAxisSweep3InternalItED2Ev,comdat
	.weak	_ZN20btAxisSweep3InternalItED2Ev
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItED2Ev,@function
_ZN20btAxisSweep3InternalItED2Ev:       # @_ZN20btAxisSweep3InternalItED2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV20btAxisSweep3InternalItE+16, (%rbx)
	cmpq	$0, 152(%rbx)
	je	.LBB4_2
# BB#1:
	movq	160(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	160(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	152(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	152(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB4_2:                                # %.preheader.preheader
	movq	120(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	112(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	104(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_4
# BB#3:
	callq	_Z21btAlignedFreeInternalPv
.LBB4_4:                                # %_ZN20btAxisSweep3InternalItE6HandledaEPv.exit
	cmpb	$0, 144(%rbx)
	je	.LBB4_6
# BB#5:
	movq	128(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	128(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB4_6:
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZN20btAxisSweep3InternalItED2Ev, .Lfunc_end4-_ZN20btAxisSweep3InternalItED2Ev
	.cfi_endproc

	.section	.text._ZN12btAxisSweep3D0Ev,"axG",@progbits,_ZN12btAxisSweep3D0Ev,comdat
	.weak	_ZN12btAxisSweep3D0Ev
	.p2align	4, 0x90
	.type	_ZN12btAxisSweep3D0Ev,@function
_ZN12btAxisSweep3D0Ev:                  # @_ZN12btAxisSweep3D0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi40:
	.cfi_def_cfa_offset 32
.Lcfi41:
	.cfi_offset %rbx, -24
.Lcfi42:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN20btAxisSweep3InternalItED2Ev
.Ltmp1:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB5_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN20btAxisSweep3InternalItEdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN12btAxisSweep3D0Ev, .Lfunc_end5-_ZN12btAxisSweep3D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_,"axG",@progbits,_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_,comdat
	.weak	_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_,@function
_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_: # @_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi49:
	.cfi_def_cfa_offset 80
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movzwl	80(%rsp), %eax
	movswl	%ax, %r12d
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r8, %rcx
	movl	%r13d, %r8d
	movl	%r12d, %r9d
	pushq	96(%rsp)
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	callq	_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	addq	$16, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset -16
	movq	64(%rbp), %r14
	movq	152(%rbp), %rdi
	movzwl	%ax, %eax
	leaq	(%rax,%rax,4), %rbp
	shlq	$4, %rbp
	leaq	(%r14,%rbp), %rax
	testq	%rdi, %rdi
	je	.LBB6_2
# BB#1:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%rdi), %rax
	subq	$8, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	movswl	%r13w, %r9d
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	pushq	$0
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	callq	*16(%rax)
	addq	$32, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 72(%r14,%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB6_2:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_, .Lfunc_end6-_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -32
.Lcfi68:
	.cfi_offset %r14, -24
.Lcfi69:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	152(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_2
# BB#1:
	movq	(%rdi), %rax
	movq	72(%r15), %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
.LBB7_2:
	movzwl	24(%r15), %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher # TAILCALL
.Lfunc_end7:
	.size	_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end7-_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher,@function
_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher: # @_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -48
.Lcfi76:
	.cfi_offset %r12, -40
.Lcfi77:
	.cfi_offset %r13, -32
.Lcfi78:
	.cfi_offset %r14, -24
.Lcfi79:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movups	(%r12), %xmm0
	movups	%xmm0, 28(%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 44(%rbx)
	movzwl	24(%rbx), %esi
	callq	_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher
	movq	152(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB8_1
# BB#2:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movq	72(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB8_1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher, .Lfunc_end8-_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.cfi_endproc

	.section	.text._ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_,"axG",@progbits,_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_,comdat
	.weak	_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_,@function
_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_: # @_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.cfi_startproc
# BB#0:
	movups	28(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movups	44(%rsi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.Lfunc_end9:
	.size	_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_, .Lfunc_end9-_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_,"axG",@progbits,_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_,comdat
	.weak	_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_,@function
_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_: # @_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -32
.Lcfi84:
	.cfi_offset %r14, -24
.Lcfi85:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	movq	152(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB10_1
# BB#7:
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	movq	%r14, %rcx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB10_1:                               # %.preheader
	movzwl	60(%r15), %eax
	testw	%ax, %ax
	je	.LBB10_6
# BB#2:                                 # %.lr.ph
	movw	$1, %bx
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movq	80(%r15), %rcx
	movzwl	%bx, %ebx
	testb	$1, (%rcx,%rbx,4)
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	movq	(%r14), %rax
	movzwl	2(%rcx,%rbx,4), %ecx
	leaq	(%rcx,%rcx,4), %rsi
	shlq	$4, %rsi
	addq	64(%r15), %rsi
	movq	%r14, %rdi
	callq	*16(%rax)
	movzwl	60(%r15), %eax
.LBB10_5:                               #   in Loop: Header=BB10_3 Depth=1
	incl	%ebx
	movzwl	%bx, %ecx
	movzwl	%ax, %edx
	leal	1(%rdx,%rdx), %edx
	cmpl	%edx, %ecx
	jb	.LBB10_3
.LBB10_6:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_, .Lfunc_end10-_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.zero	16
	.section	.text._ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher,@function
_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher: # @_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 112
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	128(%r12), %rdi
	movq	(%rdi), %rax
	callq	*112(%rax)
	testb	%al, %al
	je	.LBB11_24
# BB#1:
	movq	128(%r12), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	4(%rax), %eax
	cmpl	$2, %eax
	jl	.LBB11_3
# BB#2:
	decl	%eax
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	movq	(%rsp), %rax            # 8-byte Reload
	movl	4(%rax), %eax
.LBB11_3:                               # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit
	subl	148(%r12), %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	leaq	16(%rsp), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 148(%r12)
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB11_4
# BB#5:                                 # %.lr.ph
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_6:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rsi
	movq	16(%rdi), %r15
	movq	(%r15,%rbx), %rbp
	movq	8(%r15,%rbx), %rdx
	cmpq	%rsi, %rbp
	jne	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_6 Depth=1
	cmpq	%r14, %rdx
	je	.LBB11_19
.LBB11_8:                               # %_ZeqRK16btBroadphasePairS1_.exit.thread
                                        #   in Loop: Header=BB11_6 Depth=1
	movzwl	66(%rbp), %esi
	cmpw	60(%rdx), %si
	jae	.LBB11_12
# BB#9:                                 #   in Loop: Header=BB11_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB11_19
	.p2align	4, 0x90
.LBB11_12:                              #   in Loop: Header=BB11_6 Depth=1
	movzwl	66(%rdx), %esi
	cmpw	60(%rbp), %si
	jae	.LBB11_10
# BB#13:                                #   in Loop: Header=BB11_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB11_19
.LBB11_10:                              #   in Loop: Header=BB11_6 Depth=1
	movzwl	68(%rbp), %esi
	cmpw	62(%rdx), %si
	jae	.LBB11_14
# BB#11:                                #   in Loop: Header=BB11_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB11_19
.LBB11_14:                              #   in Loop: Header=BB11_6 Depth=1
	movzwl	68(%rdx), %esi
	cmpw	62(%rbp), %si
	jae	.LBB11_16
# BB#15:                                #   in Loop: Header=BB11_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB11_19
.LBB11_16:                              #   in Loop: Header=BB11_6 Depth=1
	movzwl	70(%rbp), %esi
	cmpw	64(%rdx), %si
	jae	.LBB11_18
# BB#17:                                #   in Loop: Header=BB11_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB11_19
.LBB11_18:                              # %_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_.exit
                                        #   in Loop: Header=BB11_6 Depth=1
	movzwl	70(%rdx), %esi
	cmpw	64(%rbp), %si
	movq	%rdx, %r14
	jae	.LBB11_20
	.p2align	4, 0x90
.LBB11_19:                              # %_ZN20btAxisSweep3InternalItE15testAabbOverlapEP17btBroadphaseProxyS2_.exit.thread
                                        #   in Loop: Header=BB11_6 Depth=1
	leaq	(%r15,%rbx), %rsi
	movq	128(%r12), %rdi
	movq	(%rdi), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	*64(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15,%rbx)
	movl	148(%r12), %ecx
	incl	%ecx
	movl	%ecx, 148(%r12)
	decl	gOverlappingPairs(%rip)
	movl	4(%rdi), %eax
	movq	%r14, %rdx
.LBB11_20:                              # %.thread52
                                        #   in Loop: Header=BB11_6 Depth=1
	incq	%r13
	movslq	%eax, %rsi
	addq	$32, %rbx
	cmpq	%rsi, %r13
	movq	%rdx, %r14
	jl	.LBB11_6
# BB#21:                                # %._crit_edge
	cmpl	$2, %eax
	jl	.LBB11_23
# BB#22:
	decl	%eax
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	4(%rdi), %eax
	movl	148(%r12), %ecx
	jmp	.LBB11_23
.LBB11_4:
	xorl	%ecx, %ecx
.LBB11_23:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit28
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	leaq	16(%rsp), %rdx
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movl	$0, 148(%r12)
.LBB11_24:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher, .Lfunc_end11-_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv,"axG",@progbits,_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv,comdat
	.weak	_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv,@function
_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv: # @_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	128(%rdi), %rax
	retq
.Lfunc_end12:
	.size	_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv, .Lfunc_end12-_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv,"axG",@progbits,_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv,comdat
	.weak	_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv,@function
_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv: # @_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	128(%rdi), %rax
	retq
.Lfunc_end13:
	.size	_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv, .Lfunc_end13-_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_,"axG",@progbits,_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_,comdat
	.weak	_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_
	.p2align	4, 0x90
	.type	_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_,@function
_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_: # @_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_
	.cfi_startproc
# BB#0:
	movups	12(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	movups	28(%rdi), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end14:
	.size	_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_, .Lfunc_end14-_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher,@function
_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher: # @_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher
	.cfi_startproc
# BB#0:
	cmpw	$0, 60(%rdi)
	jne	.LBB15_5
# BB#1:
	movw	$1, 72(%rdi)
	movzwl	62(%rdi), %edx
	movq	64(%rdi), %rax
	cmpl	$2, %edx
	jb	.LBB15_4
# BB#2:                                 # %.lr.ph.preheader
	movw	$1, %cx
	.p2align	4, 0x90
.LBB15_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	%cx, %edx
	leal	1(%rcx), %ecx
	leaq	(%rdx,%rdx,4), %rdx
	shlq	$4, %rdx
	movw	%cx, 60(%rax,%rdx)
	movw	62(%rdi), %dx
	cmpw	%dx, %cx
	jb	.LBB15_3
.LBB15_4:                               # %._crit_edge
	movzwl	%dx, %ecx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movw	$0, -20(%rax,%rcx)
.LBB15_5:
	retq
.Lfunc_end15:
	.size	_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher, .Lfunc_end15-_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE10printStatsEv,"axG",@progbits,_ZN20btAxisSweep3InternalItE10printStatsEv,comdat
	.weak	_ZN20btAxisSweep3InternalItE10printStatsEv
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE10printStatsEv,@function
_ZN20btAxisSweep3InternalItE10printStatsEv: # @_ZN20btAxisSweep3InternalItE10printStatsEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end16:
	.size	_ZN20btAxisSweep3InternalItE10printStatsEv, .Lfunc_end16-_ZN20btAxisSweep3InternalItE10printStatsEv
	.cfi_endproc

	.section	.text._ZN17bt32BitAxisSweep3D0Ev,"axG",@progbits,_ZN17bt32BitAxisSweep3D0Ev,comdat
	.weak	_ZN17bt32BitAxisSweep3D0Ev
	.p2align	4, 0x90
	.type	_ZN17bt32BitAxisSweep3D0Ev,@function
_ZN17bt32BitAxisSweep3D0Ev:             # @_ZN17bt32BitAxisSweep3D0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi101:
	.cfi_def_cfa_offset 32
.Lcfi102:
	.cfi_offset %rbx, -24
.Lcfi103:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp6:
	callq	_ZN20btAxisSweep3InternalIjED2Ev
.Ltmp7:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB17_2:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp10:
# BB#3:                                 # %_ZN20btAxisSweep3InternalIjEdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_4:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN17bt32BitAxisSweep3D0Ev, .Lfunc_end17-_ZN17bt32BitAxisSweep3D0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end17-.Ltmp10    #   Call between .Ltmp10 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_,"axG",@progbits,_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_,comdat
	.weak	_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_,@function
_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_: # @_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi110:
	.cfi_def_cfa_offset 80
.Lcfi111:
	.cfi_offset %rbx, -56
.Lcfi112:
	.cfi_offset %r12, -48
.Lcfi113:
	.cfi_offset %r13, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movzwl	80(%rsp), %eax
	movswl	%ax, %r12d
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r8, %rcx
	movl	%r13d, %r8d
	movl	%r12d, %r9d
	pushq	96(%rsp)
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	callq	_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	addq	$16, %rsp
.Lcfi119:
	.cfi_adjust_cfa_offset -16
	movq	72(%rbp), %r14
	movq	160(%rbp), %rdi
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rbp
	shlq	$5, %rbp
	leaq	(%r14,%rbp), %rax
	testq	%rdi, %rdi
	je	.LBB18_2
# BB#1:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	(%rdi), %rax
	subq	$8, %rsp
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	movswl	%r13w, %r9d
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movl	12(%rsp), %ecx          # 4-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	pushq	$0
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
.Lcfi122:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	callq	*16(%rax)
	addq	$32, %rsp
.Lcfi124:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 88(%r14,%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB18_2:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_, .Lfunc_end18-_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 32
.Lcfi128:
	.cfi_offset %rbx, -32
.Lcfi129:
	.cfi_offset %r14, -24
.Lcfi130:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	160(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB19_2
# BB#1:
	movq	(%rdi), %rax
	movq	88(%r15), %rsi
	movq	%r14, %rdx
	callq	*24(%rax)
.LBB19_2:
	movl	24(%r15), %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher # TAILCALL
.Lfunc_end19:
	.size	_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end19-_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher,@function
_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher: # @_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi133:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi134:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 48
.Lcfi136:
	.cfi_offset %rbx, -48
.Lcfi137:
	.cfi_offset %r12, -40
.Lcfi138:
	.cfi_offset %r13, -32
.Lcfi139:
	.cfi_offset %r14, -24
.Lcfi140:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movups	(%r12), %xmm0
	movups	%xmm0, 28(%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 44(%rbx)
	movl	24(%rbx), %esi
	callq	_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher
	movq	160(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB20_1
# BB#2:
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movq	88(%rbx), %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movq	%r14, %r8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB20_1:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end20:
	.size	_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher, .Lfunc_end20-_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.cfi_endproc

	.section	.text._ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_,"axG",@progbits,_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_,comdat
	.weak	_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_,@function
_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_: # @_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.cfi_startproc
# BB#0:
	movups	28(%rsi), %xmm0
	movups	%xmm0, (%rdx)
	movups	44(%rsi), %xmm0
	movups	%xmm0, (%rcx)
	retq
.Lfunc_end21:
	.size	_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_, .Lfunc_end21-_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_,"axG",@progbits,_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_,comdat
	.weak	_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_,@function
_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_: # @_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 32
.Lcfi144:
	.cfi_offset %rbx, -32
.Lcfi145:
	.cfi_offset %r14, -24
.Lcfi146:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdi, %r15
	movq	160(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB22_1
# BB#7:
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	movq	%r14, %rcx
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB22_1:                               # %.preheader
	movl	64(%r15), %eax
	leal	(%rax,%rax), %ecx
	cmpl	$2, %ecx
	jb	.LBB22_6
# BB#2:                                 # %.lr.ph
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB22_3:                               # =>This Inner Loop Header: Depth=1
	movq	88(%r15), %rcx
	movl	%ebx, %ebx
	testb	$1, (%rcx,%rbx,8)
	je	.LBB22_5
# BB#4:                                 #   in Loop: Header=BB22_3 Depth=1
	movq	(%r14), %rax
	movl	4(%rcx,%rbx,8), %ecx
	leaq	(%rcx,%rcx,2), %rsi
	shlq	$5, %rsi
	addq	72(%r15), %rsi
	movq	%r14, %rdi
	callq	*16(%rax)
	movl	64(%r15), %eax
.LBB22_5:                               #   in Loop: Header=BB22_3 Depth=1
	incl	%ebx
	leal	1(%rax,%rax), %ecx
	cmpl	%ecx, %ebx
	jb	.LBB22_3
.LBB22_6:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_, .Lfunc_end22-_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI23_0:
	.zero	16
	.section	.text._ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher,@function
_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher: # @_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi147:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi150:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi153:
	.cfi_def_cfa_offset 112
.Lcfi154:
	.cfi_offset %rbx, -56
.Lcfi155:
	.cfi_offset %r12, -48
.Lcfi156:
	.cfi_offset %r13, -40
.Lcfi157:
	.cfi_offset %r14, -32
.Lcfi158:
	.cfi_offset %r15, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movq	136(%r12), %rdi
	movq	(%rdi), %rax
	callq	*112(%rax)
	testb	%al, %al
	je	.LBB23_24
# BB#1:
	movq	136(%r12), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	4(%rax), %eax
	cmpl	$2, %eax
	jl	.LBB23_3
# BB#2:
	decl	%eax
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	movq	(%rsp), %rax            # 8-byte Reload
	movl	4(%rax), %eax
.LBB23_3:                               # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit
	subl	156(%r12), %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	leaq	16(%rsp), %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	$0, 156(%r12)
	movl	4(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB23_4
# BB#5:                                 # %.lr.ph
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB23_6:                               # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rsi
	movq	16(%rdi), %r15
	movq	(%r15,%rbx), %rbp
	movq	8(%r15,%rbx), %rdx
	cmpq	%rsi, %rbp
	jne	.LBB23_8
# BB#7:                                 #   in Loop: Header=BB23_6 Depth=1
	cmpq	%r14, %rdx
	je	.LBB23_19
.LBB23_8:                               # %_ZeqRK16btBroadphasePairS1_.exit.thread
                                        #   in Loop: Header=BB23_6 Depth=1
	movl	72(%rbp), %esi
	cmpl	60(%rdx), %esi
	jae	.LBB23_12
# BB#9:                                 #   in Loop: Header=BB23_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB23_19
	.p2align	4, 0x90
.LBB23_12:                              #   in Loop: Header=BB23_6 Depth=1
	movl	72(%rdx), %esi
	cmpl	60(%rbp), %esi
	jae	.LBB23_10
# BB#13:                                #   in Loop: Header=BB23_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB23_19
.LBB23_10:                              #   in Loop: Header=BB23_6 Depth=1
	movl	76(%rbp), %esi
	cmpl	64(%rdx), %esi
	jae	.LBB23_14
# BB#11:                                #   in Loop: Header=BB23_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB23_19
.LBB23_14:                              #   in Loop: Header=BB23_6 Depth=1
	movl	76(%rdx), %esi
	cmpl	64(%rbp), %esi
	jae	.LBB23_16
# BB#15:                                #   in Loop: Header=BB23_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB23_19
.LBB23_16:                              #   in Loop: Header=BB23_6 Depth=1
	movl	80(%rbp), %esi
	cmpl	68(%rdx), %esi
	jae	.LBB23_18
# BB#17:                                #   in Loop: Header=BB23_6 Depth=1
	movq	%rdx, %r14
	jmp	.LBB23_19
.LBB23_18:                              # %_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_.exit
                                        #   in Loop: Header=BB23_6 Depth=1
	movl	80(%rdx), %esi
	cmpl	68(%rbp), %esi
	movq	%rdx, %r14
	jae	.LBB23_20
	.p2align	4, 0x90
.LBB23_19:                              # %_ZN20btAxisSweep3InternalIjE15testAabbOverlapEP17btBroadphaseProxyS2_.exit.thread
                                        #   in Loop: Header=BB23_6 Depth=1
	leaq	(%r15,%rbx), %rsi
	movq	136(%r12), %rdi
	movq	(%rdi), %rax
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	*64(%rax)
	movq	(%rsp), %rdi            # 8-byte Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15,%rbx)
	movl	156(%r12), %ecx
	incl	%ecx
	movl	%ecx, 156(%r12)
	decl	gOverlappingPairs(%rip)
	movl	4(%rdi), %eax
	movq	%r14, %rdx
.LBB23_20:                              # %.thread52
                                        #   in Loop: Header=BB23_6 Depth=1
	incq	%r13
	movslq	%eax, %rsi
	addq	$32, %rbx
	cmpq	%rsi, %r13
	movq	%rdx, %r14
	jl	.LBB23_6
# BB#21:                                # %._crit_edge
	cmpl	$2, %eax
	jl	.LBB23_23
# BB#22:
	decl	%eax
	xorl	%esi, %esi
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%eax, %edx
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	4(%rdi), %eax
	movl	156(%r12), %ecx
	jmp	.LBB23_23
.LBB23_4:
	xorl	%ecx, %ecx
.LBB23_23:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE9quickSortI29btBroadphasePairSortPredicateEEvT_.exit28
	subl	%ecx, %eax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movaps	%xmm0, 16(%rsp)
	leaq	16(%rsp), %rdx
	movl	%eax, %esi
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	movl	$0, 156(%r12)
.LBB23_24:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher, .Lfunc_end23-_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv,"axG",@progbits,_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv,comdat
	.weak	_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv,@function
_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv: # @_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	136(%rdi), %rax
	retq
.Lfunc_end24:
	.size	_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv, .Lfunc_end24-_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv,"axG",@progbits,_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv,comdat
	.weak	_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.p2align	4, 0x90
	.type	_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv,@function
_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv: # @_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.cfi_startproc
# BB#0:
	movq	136(%rdi), %rax
	retq
.Lfunc_end25:
	.size	_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv, .Lfunc_end25-_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.cfi_endproc

	.section	.text._ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_,"axG",@progbits,_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_,comdat
	.weak	_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_
	.p2align	4, 0x90
	.type	_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_,@function
_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_: # @_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_
	.cfi_startproc
# BB#0:
	movups	16(%rdi), %xmm0
	movups	%xmm0, (%rsi)
	movups	32(%rdi), %xmm0
	movups	%xmm0, (%rdx)
	retq
.Lfunc_end26:
	.size	_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_, .Lfunc_end26-_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher,@function
_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher: # @_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher
	.cfi_startproc
# BB#0:
	cmpl	$0, 64(%rdi)
	jne	.LBB27_5
# BB#1:
	movl	$1, 80(%rdi)
	movl	68(%rdi), %edx
	movq	72(%rdi), %rax
	cmpl	$2, %edx
	jb	.LBB27_4
# BB#2:                                 # %.lr.ph.preheader
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB27_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	incl	%ecx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	movl	%ecx, 60(%rax,%rdx)
	movl	68(%rdi), %edx
	cmpl	%edx, %ecx
	jb	.LBB27_3
.LBB27_4:                               # %._crit_edge
	decl	%edx
	leaq	(%rdx,%rdx,2), %rcx
	shlq	$5, %rcx
	movl	$0, 60(%rax,%rcx)
.LBB27_5:
	retq
.Lfunc_end27:
	.size	_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher, .Lfunc_end27-_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE10printStatsEv,"axG",@progbits,_ZN20btAxisSweep3InternalIjE10printStatsEv,comdat
	.weak	_ZN20btAxisSweep3InternalIjE10printStatsEv
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE10printStatsEv,@function
_ZN20btAxisSweep3InternalIjE10printStatsEv: # @_ZN20btAxisSweep3InternalIjE10printStatsEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end28:
	.size	_ZN20btAxisSweep3InternalIjE10printStatsEv, .Lfunc_end28-_ZN20btAxisSweep3InternalIjE10printStatsEv
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end29:
	.size	__clang_call_terminate, .Lfunc_end29-__clang_call_terminate

	.section	.text._ZN20btAxisSweep3InternalItED0Ev,"axG",@progbits,_ZN20btAxisSweep3InternalItED0Ev,comdat
	.weak	_ZN20btAxisSweep3InternalItED0Ev
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItED0Ev,@function
_ZN20btAxisSweep3InternalItED0Ev:       # @_ZN20btAxisSweep3InternalItED0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi162:
	.cfi_def_cfa_offset 32
.Lcfi163:
	.cfi_offset %rbx, -24
.Lcfi164:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp12:
	callq	_ZN20btAxisSweep3InternalItED2Ev
.Ltmp13:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB30_2:
.Ltmp14:
	movq	%rax, %r14
.Ltmp15:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
# BB#3:                                 # %_ZN20btAxisSweep3InternalItEdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_4:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN20btAxisSweep3InternalItED0Ev, .Lfunc_end30-_ZN20btAxisSweep3InternalItED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end30-.Ltmp16    #   Call between .Ltmp16 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN15btNullPairCacheD2Ev,"axG",@progbits,_ZN15btNullPairCacheD2Ev,comdat
	.weak	_ZN15btNullPairCacheD2Ev
	.p2align	4, 0x90
	.type	_ZN15btNullPairCacheD2Ev,@function
_ZN15btNullPairCacheD2Ev:               # @_ZN15btNullPairCacheD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 16
.Lcfi166:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV15btNullPairCache+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB31_4
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB31_3
# BB#2:
	callq	_Z21btAlignedFreeInternalPv
.LBB31_3:                               # %.noexc
	movq	$0, 24(%rbx)
.LBB31_4:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	popq	%rbx
	retq
.Lfunc_end31:
	.size	_ZN15btNullPairCacheD2Ev, .Lfunc_end31-_ZN15btNullPairCacheD2Ev
	.cfi_endproc

	.section	.text._ZN15btNullPairCacheD0Ev,"axG",@progbits,_ZN15btNullPairCacheD0Ev,comdat
	.weak	_ZN15btNullPairCacheD0Ev
	.p2align	4, 0x90
	.type	_ZN15btNullPairCacheD0Ev,@function
_ZN15btNullPairCacheD0Ev:               # @_ZN15btNullPairCacheD0Ev
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi167:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi168:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi169:
	.cfi_def_cfa_offset 32
.Lcfi170:
	.cfi_offset %rbx, -24
.Lcfi171:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV15btNullPairCache+16, (%rbx)
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB32_3
# BB#1:
	cmpb	$0, 32(%rbx)
	je	.LBB32_3
# BB#2:
.Ltmp18:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp19:
.LBB32_3:                               # %.noexc.i
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB32_4:
.Ltmp20:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN15btNullPairCacheD0Ev, .Lfunc_end32-_ZN15btNullPairCacheD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp18-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin3   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp19    #   Call between .Ltmp19 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_,"axG",@progbits,_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_,comdat
	.weak	_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_,@function
_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_: # @_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end33:
	.size	_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_, .Lfunc_end33-_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.cfi_endproc

	.section	.text._ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher,"axG",@progbits,_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher,comdat
	.weak	_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher,@function
_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher: # @_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end34:
	.size	_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher, .Lfunc_end34-_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.cfi_endproc

	.section	.text._ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher,"axG",@progbits,_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher,comdat
	.weak	_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher,@function
_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher: # @_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end35:
	.size	_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end35-_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc

	.section	.text._ZN15btNullPairCache26getOverlappingPairArrayPtrEv,"axG",@progbits,_ZN15btNullPairCache26getOverlappingPairArrayPtrEv,comdat
	.weak	_ZN15btNullPairCache26getOverlappingPairArrayPtrEv
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache26getOverlappingPairArrayPtrEv,@function
_ZN15btNullPairCache26getOverlappingPairArrayPtrEv: # @_ZN15btNullPairCache26getOverlappingPairArrayPtrEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end36:
	.size	_ZN15btNullPairCache26getOverlappingPairArrayPtrEv, .Lfunc_end36-_ZN15btNullPairCache26getOverlappingPairArrayPtrEv
	.cfi_endproc

	.section	.text._ZNK15btNullPairCache26getOverlappingPairArrayPtrEv,"axG",@progbits,_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv,comdat
	.weak	_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv
	.p2align	4, 0x90
	.type	_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv,@function
_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv: # @_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rax
	retq
.Lfunc_end37:
	.size	_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv, .Lfunc_end37-_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv
	.cfi_endproc

	.section	.text._ZN15btNullPairCache23getOverlappingPairArrayEv,"axG",@progbits,_ZN15btNullPairCache23getOverlappingPairArrayEv,comdat
	.weak	_ZN15btNullPairCache23getOverlappingPairArrayEv
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache23getOverlappingPairArrayEv,@function
_ZN15btNullPairCache23getOverlappingPairArrayEv: # @_ZN15btNullPairCache23getOverlappingPairArrayEv
	.cfi_startproc
# BB#0:
	leaq	8(%rdi), %rax
	retq
.Lfunc_end38:
	.size	_ZN15btNullPairCache23getOverlappingPairArrayEv, .Lfunc_end38-_ZN15btNullPairCache23getOverlappingPairArrayEv
	.cfi_endproc

	.section	.text._ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher,"axG",@progbits,_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher,comdat
	.weak	_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher,@function
_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher: # @_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end39:
	.size	_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher, .Lfunc_end39-_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.cfi_endproc

	.section	.text._ZNK15btNullPairCache22getNumOverlappingPairsEv,"axG",@progbits,_ZNK15btNullPairCache22getNumOverlappingPairsEv,comdat
	.weak	_ZNK15btNullPairCache22getNumOverlappingPairsEv
	.p2align	4, 0x90
	.type	_ZNK15btNullPairCache22getNumOverlappingPairsEv,@function
_ZNK15btNullPairCache22getNumOverlappingPairsEv: # @_ZNK15btNullPairCache22getNumOverlappingPairsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end40:
	.size	_ZNK15btNullPairCache22getNumOverlappingPairsEv, .Lfunc_end40-_ZNK15btNullPairCache22getNumOverlappingPairsEv
	.cfi_endproc

	.section	.text._ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher,"axG",@progbits,_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher,comdat
	.weak	_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher,@function
_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher: # @_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end41:
	.size	_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher, .Lfunc_end41-_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.cfi_endproc

	.section	.text._ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,"axG",@progbits,_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,comdat
	.weak	_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback,@function
_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback: # @_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end42:
	.size	_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback, .Lfunc_end42-_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.cfi_endproc

	.section	.text._ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher,"axG",@progbits,_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher,comdat
	.weak	_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher,@function
_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher: # @_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end43:
	.size	_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher, .Lfunc_end43-_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.cfi_endproc

	.section	.text._ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_,"axG",@progbits,_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_,comdat
	.weak	_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_,@function
_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_: # @_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end44:
	.size	_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_, .Lfunc_end44-_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_
	.cfi_endproc

	.section	.text._ZN15btNullPairCache18hasDeferredRemovalEv,"axG",@progbits,_ZN15btNullPairCache18hasDeferredRemovalEv,comdat
	.weak	_ZN15btNullPairCache18hasDeferredRemovalEv
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache18hasDeferredRemovalEv,@function
_ZN15btNullPairCache18hasDeferredRemovalEv: # @_ZN15btNullPairCache18hasDeferredRemovalEv
	.cfi_startproc
# BB#0:
	movb	$1, %al
	retq
.Lfunc_end45:
	.size	_ZN15btNullPairCache18hasDeferredRemovalEv, .Lfunc_end45-_ZN15btNullPairCache18hasDeferredRemovalEv
	.cfi_endproc

	.section	.text._ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,"axG",@progbits,_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,comdat
	.weak	_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback,@function
_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback: # @_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end46:
	.size	_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback, .Lfunc_end46-_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.cfi_endproc

	.section	.text._ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher,"axG",@progbits,_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher,comdat
	.weak	_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher,@function
_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher: # @_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end47:
	.size	_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher, .Lfunc_end47-_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjED2Ev,"axG",@progbits,_ZN20btAxisSweep3InternalIjED2Ev,comdat
	.weak	_ZN20btAxisSweep3InternalIjED2Ev
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjED2Ev,@function
_ZN20btAxisSweep3InternalIjED2Ev:       # @_ZN20btAxisSweep3InternalIjED2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 16
.Lcfi173:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV20btAxisSweep3InternalIjE+16, (%rbx)
	cmpq	$0, 160(%rbx)
	je	.LBB48_2
# BB#1:
	movq	168(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	168(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	160(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	160(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB48_2:                               # %.preheader.preheader
	movq	128(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	120(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	112(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB48_4
# BB#3:
	callq	_Z21btAlignedFreeInternalPv
.LBB48_4:                               # %_ZN20btAxisSweep3InternalIjE6HandledaEPv.exit
	cmpb	$0, 152(%rbx)
	je	.LBB48_6
# BB#5:
	movq	136(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movq	136(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB48_6:
	popq	%rbx
	retq
.Lfunc_end48:
	.size	_ZN20btAxisSweep3InternalIjED2Ev, .Lfunc_end48-_ZN20btAxisSweep3InternalIjED2Ev
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjED0Ev,"axG",@progbits,_ZN20btAxisSweep3InternalIjED0Ev,comdat
	.weak	_ZN20btAxisSweep3InternalIjED0Ev
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjED0Ev,@function
_ZN20btAxisSweep3InternalIjED0Ev:       # @_ZN20btAxisSweep3InternalIjED0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi174:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi175:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi176:
	.cfi_def_cfa_offset 32
.Lcfi177:
	.cfi_offset %rbx, -24
.Lcfi178:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp21:
	callq	_ZN20btAxisSweep3InternalIjED2Ev
.Ltmp22:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB49_2:
.Ltmp23:
	movq	%rax, %r14
.Ltmp24:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp25:
# BB#3:                                 # %_ZN20btAxisSweep3InternalIjEdlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB49_4:
.Ltmp26:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end49:
	.size	_ZN20btAxisSweep3InternalIjED0Ev, .Lfunc_end49-_ZN20btAxisSweep3InternalIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table49:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp21-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin4   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp24-.Ltmp22         #   Call between .Ltmp22 and .Ltmp24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin4   #     jumps to .Ltmp26
	.byte	1                       #   On action: 1
	.long	.Ltmp25-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end49-.Ltmp25    #   Call between .Ltmp25 and .Lfunc_end49
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_,"axG",@progbits,_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_,comdat
	.weak	_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_,@function
_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_: # @_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi179:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi180:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi181:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi182:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi183:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi184:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi185:
	.cfi_def_cfa_offset 64
.Lcfi186:
	.cfi_offset %rbx, -56
.Lcfi187:
	.cfi_offset %r12, -48
.Lcfi188:
	.cfi_offset %r13, -40
.Lcfi189:
	.cfi_offset %r14, -32
.Lcfi190:
	.cfi_offset %r15, -24
.Lcfi191:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%rdi, %rbx
	movss	12(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm7
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movss	44(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	48(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	movss	52(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jae	.LBB50_1
# BB#2:                                 # %.sink.split.i
	movzwl	10(%rbx), %eax
	cvtsi2ssl	%eax, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %r9d
	cmovaew	%ax, %r9w
	andw	8(%rbx), %r9w
	jmp	.LBB50_3
.LBB50_1:
	xorl	%r9d, %r9d
.LBB50_3:
	mulss	%xmm8, %xmm6
	ucomiss	%xmm7, %xmm1
	jae	.LBB50_4
# BB#5:                                 # %.sink.split19.i
	movzwl	10(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	ucomiss	%xmm0, %xmm7
	cvttss2si	%xmm7, %r11d
	cmovaew	%ax, %r11w
	andw	8(%rbx), %r11w
	jmp	.LBB50_6
.LBB50_4:
	xorl	%r11d, %r11d
.LBB50_6:
	xorps	%xmm7, %xmm7
	ucomiss	%xmm6, %xmm7
	jae	.LBB50_7
# BB#8:                                 # %.sink.split25.i
	movzwl	10(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	ucomiss	%xmm0, %xmm6
	cvttss2si	%xmm6, %r12d
	cmovaew	%ax, %r12w
	andw	8(%rbx), %r12w
	jmp	.LBB50_9
.LBB50_7:
	xorl	%r12d, %r12d
.LBB50_9:                               # %_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i.exit
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm6
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	mulss	%xmm4, %xmm0
	mulss	%xmm5, %xmm6
	movw	$1, %r15w
	ucomiss	%xmm0, %xmm7
	movw	$1, %r10w
	jae	.LBB50_11
# BB#10:                                # %.sink.split.i59
	movzwl	10(%rbx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %r10d
	cmovaew	%ax, %r10w
	andw	8(%rbx), %r10w
	orl	$1, %r10d
.LBB50_11:
	mulss	%xmm8, %xmm1
	movw	%r10w, 2(%rsp)
	xorps	%xmm0, %xmm0
	ucomiss	%xmm6, %xmm0
	jae	.LBB50_13
# BB#12:                                # %.sink.split19.i62
	movzwl	10(%rbx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	ucomiss	%xmm2, %xmm6
	cvttss2si	%xmm6, %r15d
	cmovaew	%ax, %r15w
	andw	8(%rbx), %r15w
	orl	$1, %r15d
.LBB50_13:
	movq	72(%rsp), %rdi
	movw	%r15w, 4(%rsp)
	movw	$1, %ax
	ucomiss	%xmm1, %xmm0
	jae	.LBB50_15
# BB#14:                                # %.sink.split25.i65
	movzwl	10(%rbx), %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	ucomiss	%xmm0, %xmm1
	cvttss2si	%xmm1, %eax
	cmovaew	%dx, %ax
	andw	8(%rbx), %ax
	orl	$1, %eax
.LBB50_15:                              # %_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i.exit67
	movw	%ax, 6(%rsp)
	movzwl	72(%rbx), %r14d
	movq	64(%rbx), %r13
	leaq	(%r14,%r14,4), %rdx
	shlq	$4, %rdx
	movzwl	60(%r13,%rdx), %eax
	movw	%ax, 72(%rbx)
	movzwl	60(%rbx), %eax
	leal	1(%rax), %esi
	movw	%si, 60(%rbx)
	movl	%r14d, 24(%r13,%rdx)
	movq	%rcx, (%r13,%rdx)
	movw	%r8w, 8(%r13,%rdx)
	movw	%bp, 10(%r13,%rdx)
	movq	%rdi, 16(%r13,%rdx)
	leal	2(%rax,%rax), %eax
	movzwl	%ax, %ecx
	leal	-1(%rcx), %esi
	addw	$2, 66(%r13)
	leal	1(%rcx), %eax
	movq	80(%rbx), %rdi
	movl	-4(%rdi,%rcx,4), %ebp
	movl	%ebp, (%rdi,%rax,4)
	movq	80(%rbx), %rdi
	movw	%r9w, -4(%rdi,%rcx,4)
	movw	%r14w, -2(%rdi,%rcx,4)
	movw	%r10w, (%rdi,%rcx,4)
	movw	%r14w, 2(%rdi,%rcx,4)
	movw	%si, 60(%r13,%rdx)
	leaq	66(%r13,%rdx), %r8
	movw	%cx, 66(%r13,%rdx)
	movq	64(%rbx), %rdi
	addw	$2, 68(%rdi)
	movq	88(%rbx), %rdi
	movl	-4(%rdi,%rcx,4), %ebp
	movl	%ebp, (%rdi,%rax,4)
	movq	88(%rbx), %rdi
	movw	%r11w, -4(%rdi,%rcx,4)
	movw	%r14w, -2(%rdi,%rcx,4)
	movw	%r15w, (%rdi,%rcx,4)
	movw	%r14w, 2(%rdi,%rcx,4)
	movw	%si, 62(%r13,%rdx)
	movw	%cx, 68(%r13,%rdx)
	movq	64(%rbx), %rdi
	addw	$2, 70(%rdi)
	movq	96(%rbx), %rdi
	movl	-4(%rdi,%rcx,4), %ebp
	movl	%ebp, (%rdi,%rax,4)
	movq	96(%rbx), %rax
	movw	%r12w, -4(%rax,%rcx,4)
	movw	%r14w, -2(%rax,%rcx,4)
	movzwl	6(%rsp), %edi
	movw	%di, (%rax,%rcx,4)
	movw	%r14w, 2(%rax,%rcx,4)
	movw	%si, 64(%r13,%rdx)
	movw	%cx, 70(%r13,%rdx)
	movq	80(%rbx), %rsi
	movzwl	60(%r13,%rdx), %eax
	movw	-4(%rsi,%rax,4), %di
	cmpw	%di, (%rsi,%rax,4)
	jae	.LBB50_20
# BB#16:                                # %.lr.ph.i84
	movq	64(%rbx), %rbp
	leaq	(%rsi,%rax,4), %rcx
	movzwl	2(%rsi,%rax,4), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	leaq	60(%rbp,%rax), %rsi
	jmp	.LBB50_17
	.p2align	4, 0x90
.LBB50_18:                              # %.lr.ph.split..lr.ph.split_crit_edge.i91
                                        #   in Loop: Header=BB50_17 Depth=1
	addq	$-4, %rcx
	movq	64(%rbx), %rbp
.LBB50_17:                              # %.lr.ph.split.i89
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rcx), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	testb	$1, %dil
	leaq	60(%rbp,%rax), %rdi
	leaq	66(%rbp,%rax), %rax
	cmoveq	%rdi, %rax
	incw	(%rax)
	decw	(%rsi)
	movl	-4(%rcx), %eax
	movl	(%rcx), %edi
	movl	%eax, (%rcx)
	movl	%edi, -4(%rcx)
	movw	-8(%rcx), %di
	cmpw	%di, -4(%rcx)
	jb	.LBB50_18
# BB#19:                                # %_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb.exit92.loopexit
	movq	80(%rbx), %rsi
.LBB50_20:                              # %_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb.exit92
	leaq	62(%r13,%rdx), %r9
	movzwl	(%r8), %eax
	movw	-4(%rsi,%rax,4), %di
	cmpw	%di, (%rsi,%rax,4)
	jae	.LBB50_24
# BB#21:                                # %.lr.ph.i74
	leaq	(%rsi,%rax,4), %rcx
	movzwl	2(%rsi,%rax,4), %eax
	movq	64(%rbx), %rsi
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	leaq	66(%rsi,%rax), %rbp
	jmp	.LBB50_22
	.p2align	4, 0x90
.LBB50_23:                              # %..lr.ph.split_crit_edge.i81
                                        #   in Loop: Header=BB50_22 Depth=1
	addq	$-4, %rcx
	movq	64(%rbx), %rsi
.LBB50_22:                              # %.lr.ph.split.i79
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rcx), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	testb	$1, %dil
	leaq	66(%rsi,%rax), %rdi
	leaq	60(%rsi,%rax), %rax
	cmovneq	%rdi, %rax
	incw	(%rax)
	decw	(%rbp)
	movl	-4(%rcx), %eax
	movl	(%rcx), %esi
	movl	%eax, (%rcx)
	movl	%esi, -4(%rcx)
	movw	-8(%rcx), %di
	cmpw	%di, -4(%rcx)
	jb	.LBB50_23
.LBB50_24:                              # %_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb.exit82
	leaq	68(%r13,%rdx), %r8
	movq	88(%rbx), %rdi
	movzwl	(%r9), %eax
	movw	-4(%rdi,%rax,4), %bp
	cmpw	%bp, (%rdi,%rax,4)
	jae	.LBB50_29
# BB#25:                                # %.lr.ph.i69
	movq	64(%rbx), %rcx
	leaq	(%rdi,%rax,4), %rsi
	movzwl	2(%rdi,%rax,4), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	leaq	62(%rcx,%rax), %rdi
	jmp	.LBB50_26
	.p2align	4, 0x90
.LBB50_27:                              # %.lr.ph.split..lr.ph.split_crit_edge.i
                                        #   in Loop: Header=BB50_26 Depth=1
	addq	$-4, %rsi
	movq	64(%rbx), %rcx
.LBB50_26:                              # %.lr.ph.split.i71
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rsi), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	testb	$1, %bpl
	leaq	62(%rcx,%rax), %rbp
	leaq	68(%rcx,%rax), %rax
	cmoveq	%rbp, %rax
	incw	(%rax)
	decw	(%rdi)
	movl	-4(%rsi), %eax
	movl	(%rsi), %ecx
	movl	%eax, (%rsi)
	movl	%ecx, -4(%rsi)
	movw	-8(%rsi), %bp
	cmpw	%bp, -4(%rsi)
	jb	.LBB50_27
# BB#28:                                # %_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb.exit.loopexit
	movq	88(%rbx), %rdi
.LBB50_29:                              # %_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb.exit
	movq	64(%rsp), %r15
	leaq	64(%r13,%rdx), %rbp
	movzwl	(%r8), %edx
	movw	-4(%rdi,%rdx,4), %cx
	cmpw	%cx, (%rdi,%rdx,4)
	jae	.LBB50_33
# BB#30:                                # %.lr.ph.i
	leaq	(%rdi,%rdx,4), %rax
	movzwl	2(%rdi,%rdx,4), %edx
	movq	64(%rbx), %rsi
	leaq	(%rdx,%rdx,4), %rdx
	shlq	$4, %rdx
	leaq	68(%rsi,%rdx), %rdx
	jmp	.LBB50_31
	.p2align	4, 0x90
.LBB50_32:                              # %..lr.ph.split_crit_edge.i
                                        #   in Loop: Header=BB50_31 Depth=1
	addq	$-4, %rax
	movq	64(%rbx), %rsi
.LBB50_31:                              # %.lr.ph.split.i
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rax), %edi
	leaq	(%rdi,%rdi,4), %rdi
	shlq	$4, %rdi
	testb	$1, %cl
	leaq	68(%rsi,%rdi), %rcx
	leaq	62(%rsi,%rdi), %rsi
	cmovneq	%rcx, %rsi
	incw	(%rsi)
	decw	(%rdx)
	movl	-4(%rax), %ecx
	movl	(%rax), %esi
	movl	%ecx, (%rax)
	movl	%esi, -4(%rax)
	movw	-8(%rax), %cx
	cmpw	%cx, -4(%rax)
	jb	.LBB50_32
.LBB50_33:                              # %_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb.exit
	movzwl	(%rbp), %edx
	movl	$2, %esi
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb
	movzwl	6(%rbp), %edx
	movl	$2, %esi
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end50:
	.size	_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_, .Lfunc_end50-_ZN20btAxisSweep3InternalItE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb,@function
_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb: # @_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi192:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi193:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi194:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi195:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi196:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi198:
	.cfi_def_cfa_offset 96
.Lcfi199:
	.cfi_offset %rbx, -56
.Lcfi200:
	.cfi_offset %r12, -48
.Lcfi201:
	.cfi_offset %r13, -40
.Lcfi202:
	.cfi_offset %r14, -32
.Lcfi203:
	.cfi_offset %r15, -24
.Lcfi204:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r14
	movslq	%esi, %r13
	movq	80(%r14,%r13,8), %rcx
	movw	-4(%rcx,%rdx,4), %ax
	cmpw	%ax, (%rcx,%rdx,4)
	jae	.LBB51_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rdx,4), %rbp
	movq	64(%r14), %rdi
	movzwl	2(%rcx,%rdx,4), %ecx
	leaq	(%rcx,%rcx,4), %rsi
	shlq	$4, %rsi
	addq	%rdi, %rsi
	leaq	60(%rsi,%r13,2), %r15
	movl	$1, %edx
	movl	$1, %r12d
	movl	%r13d, %ecx
	shll	%cl, %r12d
	andl	$3, %r12d
	movl	%r12d, %ecx
	shll	%cl, %edx
	testb	%r8b, %r8b
	je	.LBB51_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %edx
	leaq	66(%rsi,%r12,2), %r8
	leaq	66(%rsi,%rdx,2), %r9
	jmp	.LBB51_3
	.p2align	4, 0x90
.LBB51_15:                              # %.lr.ph.split..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB51_14 Depth=1
	addq	$-4, %rbp
	movq	64(%r14), %rdi
.LBB51_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rbp), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	addq	%rdi, %rcx
	testb	$1, %al
	leaq	60(%rcx,%r13,2), %rax
	leaq	66(%rcx,%r13,2), %rcx
	cmoveq	%rax, %rcx
	incw	(%rcx)
	decw	(%r15)
	movl	-4(%rbp), %eax
	movl	(%rbp), %ecx
	movl	%eax, (%rbp)
	movl	%ecx, -4(%rbp)
	movw	-8(%rbp), %ax
	cmpw	%ax, -4(%rbp)
	jb	.LBB51_15
	jmp	.LBB51_16
	.p2align	4, 0x90
.LBB51_13:                              # %..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB51_3 Depth=1
	addq	$-4, %rbp
	movq	64(%r14), %rdi
.LBB51_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rbp), %ecx
	leaq	(%rcx,%rcx,4), %rbx
	shlq	$4, %rbx
	addq	%rdi, %rbx
	testb	$1, %al
	jne	.LBB51_4
# BB#11:                                #   in Loop: Header=BB51_3 Depth=1
	leaq	60(%rbx,%r13,2), %rax
	jmp	.LBB51_12
	.p2align	4, 0x90
.LBB51_4:                               #   in Loop: Header=BB51_3 Depth=1
	movzwl	(%r8), %eax
	cmpw	60(%rbx,%r12,2), %ax
	jb	.LBB51_10
# BB#5:                                 #   in Loop: Header=BB51_3 Depth=1
	movzwl	66(%rbx,%r12,2), %eax
	cmpw	-6(%r8), %ax
	jb	.LBB51_10
# BB#6:                                 #   in Loop: Header=BB51_3 Depth=1
	movzwl	(%r9), %eax
	cmpw	60(%rbx,%rdx,2), %ax
	jb	.LBB51_10
# BB#7:                                 # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB51_3 Depth=1
	movzwl	66(%rbx,%rdx,2), %eax
	cmpw	-6(%r9), %ax
	jb	.LBB51_10
# BB#8:                                 #   in Loop: Header=BB51_3 Depth=1
	movq	128(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r9, 8(%rsp)            # 8-byte Spill
	callq	*16(%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	136(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB51_10
# BB#9:                                 #   in Loop: Header=BB51_3 Depth=1
	movq	(%rdi), %rax
	movq	%rbx, %rdx
	callq	*16(%rax)
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB51_10:                              # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB51_3 Depth=1
	leaq	66(%rbx,%r13,2), %rax
.LBB51_12:                              #   in Loop: Header=BB51_3 Depth=1
	incw	(%rax)
	decw	(%r15)
	movl	-4(%rbp), %eax
	movl	(%rbp), %ecx
	movl	%eax, (%rbp)
	movl	%ecx, -4(%rbp)
	movw	-8(%rbp), %ax
	cmpw	%ax, -4(%rbp)
	jb	.LBB51_13
.LBB51_16:                              # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end51:
	.size	_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb, .Lfunc_end51-_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb,@function
_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb: # @_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi205:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi206:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi207:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi208:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi209:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi211:
	.cfi_def_cfa_offset 80
.Lcfi212:
	.cfi_offset %rbx, -56
.Lcfi213:
	.cfi_offset %r12, -48
.Lcfi214:
	.cfi_offset %r13, -40
.Lcfi215:
	.cfi_offset %r14, -32
.Lcfi216:
	.cfi_offset %r15, -24
.Lcfi217:
	.cfi_offset %rbp, -16
	movq	%rcx, %r10
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r9
	movslq	%esi, %r13
	movq	80(%r9,%r13,8), %rcx
	movw	-4(%rcx,%rdx,4), %ax
	cmpw	%ax, (%rcx,%rdx,4)
	jae	.LBB52_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rdx,4), %r15
	movzwl	2(%rcx,%rdx,4), %edx
	movq	64(%r9), %rbx
	movl	$1, %esi
	movl	$1, %r12d
	movl	%r13d, %ecx
	shll	%cl, %r12d
	andl	$3, %r12d
	movl	%r12d, %ecx
	shll	%cl, %esi
	leaq	(%rdx,%rdx,4), %rcx
	shlq	$4, %rcx
	addq	%rbx, %rcx
	leaq	66(%rcx,%r13,2), %r14
	testb	%r8b, %r8b
	je	.LBB52_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %esi
	jmp	.LBB52_3
	.p2align	4, 0x90
.LBB52_15:                              # %..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB52_14 Depth=1
	addq	$-4, %r15
	movq	64(%r9), %rbx
.LBB52_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%r15), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	addq	%rbx, %rcx
	testb	$1, %al
	leaq	66(%rcx,%r13,2), %rax
	leaq	60(%rcx,%r13,2), %rcx
	cmovneq	%rax, %rcx
	incw	(%rcx)
	decw	(%r14)
	movl	-4(%r15), %eax
	movl	(%r15), %ecx
	movl	%eax, (%r15)
	movl	%ecx, -4(%r15)
	movw	-8(%r15), %ax
	cmpw	%ax, -4(%r15)
	jb	.LBB52_15
	jmp	.LBB52_16
	.p2align	4, 0x90
.LBB52_13:                              # %..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB52_3 Depth=1
	addq	$-4, %r15
	movq	64(%r9), %rbx
.LBB52_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%r15), %ecx
	leaq	(%rcx,%rcx,4), %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	testb	$1, %al
	jne	.LBB52_4
# BB#5:                                 #   in Loop: Header=BB52_3 Depth=1
	movzwl	2(%r15), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	addq	%rax, %rbx
	movzwl	66(%rbx,%r12,2), %eax
	cmpw	60(%rbp,%r12,2), %ax
	jb	.LBB52_11
# BB#6:                                 #   in Loop: Header=BB52_3 Depth=1
	movzwl	66(%rbp,%r12,2), %eax
	cmpw	60(%rbx,%r12,2), %ax
	jb	.LBB52_11
# BB#7:                                 #   in Loop: Header=BB52_3 Depth=1
	movzwl	66(%rbx,%rsi,2), %eax
	cmpw	60(%rbp,%rsi,2), %ax
	jb	.LBB52_11
# BB#8:                                 # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB52_3 Depth=1
	movzwl	66(%rbp,%rsi,2), %eax
	cmpw	60(%rbx,%rsi,2), %ax
	jb	.LBB52_11
# BB#9:                                 #   in Loop: Header=BB52_3 Depth=1
	movq	128(%r9), %rdi
	movq	(%rdi), %rax
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r10, %rcx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	136(%r9), %rdi
	testq	%rdi, %rdi
	je	.LBB52_11
# BB#10:                                #   in Loop: Header=BB52_3 Depth=1
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r10, %rcx
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB52_11:                              # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB52_3 Depth=1
	leaq	60(%rbp,%r13,2), %rax
	jmp	.LBB52_12
	.p2align	4, 0x90
.LBB52_4:                               #   in Loop: Header=BB52_3 Depth=1
	leaq	66(%rbp,%r13,2), %rax
.LBB52_12:                              #   in Loop: Header=BB52_3 Depth=1
	incw	(%rax)
	decw	(%r14)
	movl	-4(%r15), %eax
	movl	(%r15), %ecx
	movl	%eax, (%r15)
	movl	%ecx, -4(%r15)
	movw	-8(%r15), %ax
	cmpw	%ax, -4(%r15)
	jb	.LBB52_13
.LBB52_16:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end52:
	.size	_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb, .Lfunc_end52-_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher,@function
_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher: # @_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi218:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi219:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi220:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi221:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi222:
	.cfi_def_cfa_offset 48
.Lcfi223:
	.cfi_offset %rbx, -48
.Lcfi224:
	.cfi_offset %r12, -40
.Lcfi225:
	.cfi_offset %r13, -32
.Lcfi226:
	.cfi_offset %r14, -24
.Lcfi227:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movl	%esi, %r14d
	movq	%rdi, %r13
	movq	64(%r13), %r12
	movq	128(%r13), %rdi
	movq	(%rdi), %rax
	callq	*112(%rax)
	testb	%al, %al
	jne	.LBB53_2
# BB#1:
	leaq	(%r14,%r14,4), %rsi
	shlq	$4, %rsi
	addq	%r12, %rsi
	movq	128(%r13), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rdx
	callq	*32(%rax)
.LBB53_2:                               # %.preheader47
	movzwl	60(%r13), %r9d
	movq	64(%r13), %rax
	addw	$-2, 66(%rax)
	addw	$-2, 68(%rax)
	addw	$-2, 70(%rax)
	addq	%r9, %r9
	movw	10(%r13), %di
	xorl	%edx, %edx
	leaq	(%r14,%r14,4), %r8
	shlq	$4, %r8
	addq	%r8, %r12
	.p2align	4, 0x90
.LBB53_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB53_6 Depth 2
                                        #     Child Loop BB53_11 Depth 2
	movq	80(%r13,%rdx,8), %r10
	movzwl	66(%r12,%rdx,2), %eax
	movw	%di, (%r10,%rax,4)
	movzwl	6(%r10,%rax,4), %esi
	testw	%si, %si
	je	.LBB53_4
# BB#5:                                 # %.lr.ph.i39
                                        #   in Loop: Header=BB53_3 Depth=1
	leaq	(%r10,%rax,4), %rcx
	movzwl	2(%r10,%rax,4), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	addq	64(%r13), %rax
	leaq	66(%rax,%rdx,2), %rax
	.p2align	4, 0x90
.LBB53_6:                               # %.lr.ph.split.i41
                                        #   Parent Loop BB53_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	4(%rcx), %bx
	cmpw	%bx, %di
	jb	.LBB53_8
# BB#7:                                 #   in Loop: Header=BB53_6 Depth=2
	movq	64(%r13), %rdi
	movzwl	%si, %esi
	leaq	(%rsi,%rsi,4), %rsi
	shlq	$4, %rsi
	testb	$1, %bl
	leaq	66(%rdi,%rsi), %rbx
	leaq	60(%rdi,%rsi), %rsi
	cmovneq	%rbx, %rsi
	decw	(%rsi,%rdx,2)
	incw	(%rax)
	movl	(%rcx), %edi
	movl	4(%rcx), %esi
	movl	%esi, (%rcx)
	movl	%edi, 4(%rcx)
	movzwl	10(%rcx), %esi
	leaq	4(%rcx), %rcx
	testw	%si, %si
	jne	.LBB53_6
.LBB53_8:                               # %_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb.exit.loopexit
                                        #   in Loop: Header=BB53_3 Depth=1
	movw	10(%r13), %di
	movq	80(%r13,%rdx,8), %r11
	jmp	.LBB53_9
	.p2align	4, 0x90
.LBB53_4:                               #   in Loop: Header=BB53_3 Depth=1
	movq	%r10, %r11
.LBB53_9:                               # %_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb.exit
                                        #   in Loop: Header=BB53_3 Depth=1
	movzwl	60(%r12,%rdx,2), %esi
	movw	%di, (%r10,%rsi,4)
	movzwl	6(%r11,%rsi,4), %eax
	testw	%ax, %ax
	je	.LBB53_14
# BB#10:                                # %.lr.ph.i
                                        #   in Loop: Header=BB53_3 Depth=1
	leaq	(%r11,%rsi,4), %rdi
	movzwl	2(%r11,%rsi,4), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	addq	64(%r13), %rcx
	leaq	60(%rcx,%rdx,2), %rcx
	movw	(%r11,%rsi,4), %si
	.p2align	4, 0x90
.LBB53_11:                              # %.lr.ph.split.i
                                        #   Parent Loop BB53_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	4(%rdi), %bx
	cmpw	%bx, %si
	jb	.LBB53_13
# BB#12:                                #   in Loop: Header=BB53_11 Depth=2
	movzwl	%ax, %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	addq	64(%r13), %rax
	testb	$1, %bl
	leaq	60(%rax,%rdx,2), %rsi
	leaq	66(%rax,%rdx,2), %rax
	cmoveq	%rsi, %rax
	decw	(%rax)
	incw	(%rcx)
	movl	(%rdi), %esi
	movl	4(%rdi), %eax
	movl	%eax, (%rdi)
	movl	%esi, 4(%rdi)
	movzwl	10(%rdi), %eax
	leaq	4(%rdi), %rdi
	testw	%ax, %ax
	jne	.LBB53_11
.LBB53_13:                              # %_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb.exit.loopexit
                                        #   in Loop: Header=BB53_3 Depth=1
	movw	10(%r13), %di
.LBB53_14:                              # %_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb.exit
                                        #   in Loop: Header=BB53_3 Depth=1
	movw	$0, -2(%r10,%r9,4)
	movw	%di, -4(%r10,%r9,4)
	incq	%rdx
	cmpq	$3, %rdx
	jne	.LBB53_3
# BB#15:
	movq	64(%r13), %rax
	movzwl	72(%r13), %ecx
	movw	%cx, 60(%rax,%r8)
	movw	%r14w, 72(%r13)
	decw	60(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end53:
	.size	_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher, .Lfunc_end53-_ZN20btAxisSweep3InternalItE12removeHandleEtP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb,@function
_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb: # @_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi228:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi229:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi230:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi231:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi232:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi233:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi234:
	.cfi_def_cfa_offset 112
.Lcfi235:
	.cfi_offset %rbx, -56
.Lcfi236:
	.cfi_offset %r12, -48
.Lcfi237:
	.cfi_offset %r13, -40
.Lcfi238:
	.cfi_offset %r14, -32
.Lcfi239:
	.cfi_offset %r15, -24
.Lcfi240:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r14
	movslq	%esi, %r12
	movq	80(%r14,%r12,8), %rcx
	movzwl	6(%rcx,%rdx,4), %eax
	testw	%ax, %ax
	je	.LBB54_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rdx,4), %rbp
	movzwl	2(%rcx,%rdx,4), %edx
	movl	$1, %r9d
	movl	$1, %r10d
	movl	%r12d, %ecx
	shll	%cl, %r10d
	andl	$3, %r10d
	movl	%r10d, %ecx
	shll	%cl, %r9d
	leaq	(%rdx,%rdx,4), %rcx
	shlq	$4, %rcx
	addq	64(%r14), %rcx
	leaq	66(%rcx,%r12,2), %r11
	testb	%r8b, %r8b
	je	.LBB54_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %r9d
	leaq	66(%rcx,%r10,2), %rdx
	leaq	66(%rcx,%r9,2), %r8
	.p2align	4, 0x90
.LBB54_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movw	4(%rbp), %cx
	cmpw	%cx, (%rbp)
	jb	.LBB54_16
# BB#4:                                 #   in Loop: Header=BB54_3 Depth=1
	movq	64(%r14), %r15
	movzwl	%ax, %eax
	testb	$1, %cl
	jne	.LBB54_5
# BB#6:                                 #   in Loop: Header=BB54_3 Depth=1
	movzwl	(%rdx), %ecx
	leaq	(%rax,%rax,4), %r13
	shlq	$4, %r13
	leaq	(%r15,%r13), %rbx
	cmpw	60(%rbx,%r10,2), %cx
	jb	.LBB54_12
# BB#7:                                 #   in Loop: Header=BB54_3 Depth=1
	movzwl	66(%rbx,%r10,2), %eax
	cmpw	-6(%rdx), %ax
	jb	.LBB54_12
# BB#8:                                 #   in Loop: Header=BB54_3 Depth=1
	movzwl	(%r8), %eax
	cmpw	60(%rbx,%r9,2), %ax
	jb	.LBB54_12
# BB#9:                                 # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB54_3 Depth=1
	movzwl	66(%rbx,%r9,2), %eax
	cmpw	-6(%r8), %ax
	jb	.LBB54_12
# BB#10:                                #   in Loop: Header=BB54_3 Depth=1
	movzwl	2(%rbp), %eax
	leaq	(%rax,%rax,4), %rsi
	shlq	$4, %rsi
	addq	%r15, %rsi
	movq	128(%r14), %rdi
	movq	(%rdi), %rax
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	callq	*16(%rax)
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	136(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB54_12
# BB#11:                                #   in Loop: Header=BB54_3 Depth=1
	movq	(%rdi), %rax
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	*16(%rax)
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB54_12:                              # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB54_3 Depth=1
	leaq	60(%r15,%r13), %rax
	jmp	.LBB54_13
	.p2align	4, 0x90
.LBB54_5:                               #   in Loop: Header=BB54_3 Depth=1
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	leaq	66(%r15,%rax), %rax
.LBB54_13:                              #   in Loop: Header=BB54_3 Depth=1
	decw	(%rax,%r12,2)
	incw	(%r11)
	movl	(%rbp), %eax
	movl	4(%rbp), %ecx
	movl	%ecx, (%rbp)
	movl	%eax, 4(%rbp)
	movzwl	10(%rbp), %eax
	leaq	4(%rbp), %rbp
	testw	%ax, %ax
	jne	.LBB54_3
	jmp	.LBB54_16
	.p2align	4, 0x90
.LBB54_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movw	4(%rbp), %cx
	cmpw	%cx, (%rbp)
	jb	.LBB54_16
# BB#15:                                #   in Loop: Header=BB54_14 Depth=1
	movq	64(%r14), %rdx
	movzwl	%ax, %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	testb	$1, %cl
	leaq	66(%rdx,%rax), %rcx
	leaq	60(%rdx,%rax), %rax
	cmovneq	%rcx, %rax
	decw	(%rax,%r12,2)
	incw	(%r11)
	movl	(%rbp), %eax
	movl	4(%rbp), %ecx
	movl	%ecx, (%rbp)
	movl	%eax, 4(%rbp)
	movzwl	10(%rbp), %eax
	leaq	4(%rbp), %rbp
	testw	%ax, %ax
	jne	.LBB54_14
.LBB54_16:                              # %.critedge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end54:
	.size	_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb, .Lfunc_end54-_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb,@function
_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb: # @_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi241:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi242:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi243:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi244:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi245:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi246:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi247:
	.cfi_def_cfa_offset 80
.Lcfi248:
	.cfi_offset %rbx, -56
.Lcfi249:
	.cfi_offset %r12, -48
.Lcfi250:
	.cfi_offset %r13, -40
.Lcfi251:
	.cfi_offset %r14, -32
.Lcfi252:
	.cfi_offset %r15, -24
.Lcfi253:
	.cfi_offset %rbp, -16
	movq	%rcx, %r9
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r14
	movslq	%esi, %r13
	movq	80(%r14,%r13,8), %rcx
	movzwl	6(%rcx,%rdx,4), %eax
	testw	%ax, %ax
	je	.LBB55_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rdx,4), %r15
	movzwl	2(%rcx,%rdx,4), %ecx
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	addq	64(%r14), %rcx
	leaq	60(%rcx,%r13,2), %rdx
	movl	$1, %esi
	movl	$1, %r12d
	movl	%r13d, %ecx
	shll	%cl, %r12d
	andl	$3, %r12d
	movl	%r12d, %ecx
	shll	%cl, %esi
	testb	%r8b, %r8b
	je	.LBB55_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %esi
	.p2align	4, 0x90
.LBB55_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movw	4(%r15), %cx
	cmpw	%cx, (%r15)
	jb	.LBB55_16
# BB#4:                                 #   in Loop: Header=BB55_3 Depth=1
	movq	64(%r14), %rbp
	movzwl	%ax, %eax
	leaq	(%rax,%rax,4), %rbx
	shlq	$4, %rbx
	addq	%rbp, %rbx
	testb	$1, %cl
	jne	.LBB55_5
# BB#12:                                #   in Loop: Header=BB55_3 Depth=1
	leaq	60(%rbx,%r13,2), %rax
	jmp	.LBB55_13
	.p2align	4, 0x90
.LBB55_5:                               #   in Loop: Header=BB55_3 Depth=1
	movzwl	2(%r15), %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	addq	%rax, %rbp
	movzwl	66(%rbp,%r12,2), %eax
	cmpw	60(%rbx,%r12,2), %ax
	jb	.LBB55_11
# BB#6:                                 #   in Loop: Header=BB55_3 Depth=1
	movzwl	66(%rbx,%r12,2), %eax
	cmpw	60(%rbp,%r12,2), %ax
	jb	.LBB55_11
# BB#7:                                 #   in Loop: Header=BB55_3 Depth=1
	movzwl	66(%rbp,%rsi,2), %eax
	cmpw	60(%rbx,%rsi,2), %ax
	jb	.LBB55_11
# BB#8:                                 # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB55_3 Depth=1
	movzwl	66(%rbx,%rsi,2), %eax
	cmpw	60(%rbp,%rsi,2), %ax
	jb	.LBB55_11
# BB#9:                                 #   in Loop: Header=BB55_3 Depth=1
	movq	128(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rbp, %rsi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	movq	%r9, %rcx
	movq	%r9, 8(%rsp)            # 8-byte Spill
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	136(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB55_11
# BB#10:                                #   in Loop: Header=BB55_3 Depth=1
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r9, %rcx
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB55_11:                              # %_ZN20btAxisSweep3InternalItE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB55_3 Depth=1
	leaq	66(%rbx,%r13,2), %rax
.LBB55_13:                              #   in Loop: Header=BB55_3 Depth=1
	decw	(%rax)
	incw	(%rdx)
	movl	(%r15), %eax
	movl	4(%r15), %ecx
	movl	%ecx, (%r15)
	movl	%eax, 4(%r15)
	movzwl	10(%r15), %eax
	leaq	4(%r15), %r15
	testw	%ax, %ax
	jne	.LBB55_3
	jmp	.LBB55_16
	.p2align	4, 0x90
.LBB55_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movw	4(%r15), %cx
	cmpw	%cx, (%r15)
	jb	.LBB55_16
# BB#15:                                #   in Loop: Header=BB55_14 Depth=1
	movzwl	%ax, %eax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	addq	64(%r14), %rax
	testb	$1, %cl
	leaq	60(%rax,%r13,2), %rcx
	leaq	66(%rax,%r13,2), %rax
	cmoveq	%rcx, %rax
	decw	(%rax)
	incw	(%rdx)
	movl	(%r15), %eax
	movl	4(%r15), %ecx
	movl	%ecx, (%r15)
	movl	%eax, 4(%r15)
	movzwl	10(%r15), %eax
	leaq	4(%r15), %r15
	testw	%ax, %ax
	jne	.LBB55_14
.LBB55_16:                              # %.critedge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end55:
	.size	_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb, .Lfunc_end55-_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher,@function
_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher: # @_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi254:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi255:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi256:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi257:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi258:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi259:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi260:
	.cfi_def_cfa_offset 96
.Lcfi261:
	.cfi_offset %rbx, -56
.Lcfi262:
	.cfi_offset %r12, -48
.Lcfi263:
	.cfi_offset %r13, -40
.Lcfi264:
	.cfi_offset %r14, -32
.Lcfi265:
	.cfi_offset %r15, -24
.Lcfi266:
	.cfi_offset %rbp, -16
	movq	%r8, 24(%rsp)           # 8-byte Spill
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	64(%rdi), %rax
	movss	12(%rdi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm7
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	20(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movss	44(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	48(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	movss	52(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jae	.LBB56_1
# BB#2:                                 # %.sink.split.i
	movzwl	10(%rdi), %ebp
	cvtsi2ssl	%ebp, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %edx
	cmovaew	%bp, %dx
	andw	8(%rdi), %dx
	jmp	.LBB56_3
.LBB56_1:
	xorl	%edx, %edx
.LBB56_3:
	mulss	%xmm8, %xmm6
	movw	%dx, 18(%rsp)
	ucomiss	%xmm7, %xmm1
	jae	.LBB56_4
# BB#5:                                 # %.sink.split19.i
	movzwl	10(%rdi), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	ucomiss	%xmm0, %xmm7
	cvttss2si	%xmm7, %ebx
	cmovaew	%bp, %bx
	andw	8(%rdi), %bx
	jmp	.LBB56_6
.LBB56_4:
	xorl	%ebx, %ebx
.LBB56_6:
	movw	%bx, 20(%rsp)
	xorps	%xmm7, %xmm7
	ucomiss	%xmm6, %xmm7
	jae	.LBB56_7
# BB#8:                                 # %.sink.split25.i
	movzwl	10(%rdi), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	ucomiss	%xmm0, %xmm6
	cvttss2si	%xmm6, %ebx
	cmovaew	%bp, %bx
	andw	8(%rdi), %bx
	jmp	.LBB56_9
.LBB56_7:
	xorl	%ebx, %ebx
.LBB56_9:                               # %_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i.exit
	movw	%bx, 22(%rsp)
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm6
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	mulss	%xmm4, %xmm0
	mulss	%xmm5, %xmm6
	movw	$1, %bx
	ucomiss	%xmm0, %xmm7
	movw	$1, %cx
	jae	.LBB56_11
# BB#10:                                # %.sink.split.i49
	movzwl	10(%rdi), %ebp
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebp, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %ecx
	cmovaew	%bp, %cx
	andw	8(%rdi), %cx
	orl	$1, %ecx
.LBB56_11:
	mulss	%xmm8, %xmm1
	movw	%cx, 12(%rsp)
	xorps	%xmm0, %xmm0
	ucomiss	%xmm6, %xmm0
	jae	.LBB56_13
# BB#12:                                # %.sink.split19.i52
	movzwl	10(%rdi), %ebp
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebp, %xmm2
	ucomiss	%xmm2, %xmm6
	cvttss2si	%xmm6, %ebx
	cmovaew	%bp, %bx
	andw	8(%rdi), %bx
	orl	$1, %ebx
.LBB56_13:
	movw	%bx, 14(%rsp)
	movw	$1, %bx
	ucomiss	%xmm1, %xmm0
	jae	.LBB56_15
# BB#14:                                # %.sink.split25.i55
	movzwl	10(%rdi), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	ucomiss	%xmm0, %xmm1
	cvttss2si	%xmm1, %ebx
	cmovaew	%bp, %bx
	andw	8(%rdi), %bx
	orl	$1, %ebx
.LBB56_15:                              # %_ZNK20btAxisSweep3InternalItE8quantizeEPtRK9btVector3i.exit57
	movw	%bx, 16(%rsp)
	leaq	(%rsi,%rsi,4), %rsi
	shlq	$4, %rsi
	leaq	66(%rax,%rsi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	movq	%rdi, %r12
	jmp	.LBB56_16
	.p2align	4, 0x90
.LBB56_25:                              # %._crit_edge
                                        #   in Loop: Header=BB56_16 Depth=1
	movw	20(%rsp,%rbx,2), %dx
	movw	14(%rsp,%rbx,2), %cx
	incq	%rbx
.LBB56_16:                              # =>This Inner Loop Header: Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	-6(%rax,%rbx,2), %ebp
	movzwl	(%rax,%rbx,2), %r13d
	movzwl	%dx, %r15d
	movq	80(%rdi,%rbx,8), %rax
	movzwl	(%rax,%rbp,4), %edx
	movzwl	%cx, %r14d
	movzwl	(%rax,%r13,4), %ecx
	movw	%r15w, (%rax,%rbp,4)
	movw	%r14w, (%rax,%r13,4)
	subl	%ecx, %r14d
	subl	%edx, %r15d
	jns	.LBB56_18
# BB#17:                                #   in Loop: Header=BB56_16 Depth=1
	movzwl	%bp, %edx
	movl	$1, %r8d
	movl	%ebx, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalItE11sortMinDownEitP12btDispatcherb
	movq	%r12, %rdi
.LBB56_18:                              #   in Loop: Header=BB56_16 Depth=1
	testl	%r14d, %r14d
	jle	.LBB56_20
# BB#19:                                #   in Loop: Header=BB56_16 Depth=1
	movzwl	%r13w, %edx
	movl	$1, %r8d
	movl	%ebx, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalItE9sortMaxUpEitP12btDispatcherb
	movq	%r12, %rdi
.LBB56_20:                              #   in Loop: Header=BB56_16 Depth=1
	testl	%r15d, %r15d
	jle	.LBB56_22
# BB#21:                                #   in Loop: Header=BB56_16 Depth=1
	movzwl	%bp, %edx
	movl	$1, %r8d
	movl	%ebx, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalItE9sortMinUpEitP12btDispatcherb
	movq	%r12, %rdi
.LBB56_22:                              #   in Loop: Header=BB56_16 Depth=1
	testl	%r14d, %r14d
	jns	.LBB56_24
# BB#23:                                #   in Loop: Header=BB56_16 Depth=1
	movzwl	%r13w, %edx
	movl	$1, %r8d
	movl	%ebx, %esi
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalItE11sortMaxDownEitP12btDispatcherb
	movq	%r12, %rdi
.LBB56_24:                              #   in Loop: Header=BB56_16 Depth=1
	cmpq	$2, %rbx
	jne	.LBB56_25
# BB#26:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end56:
	.size	_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher, .Lfunc_end56-_ZN20btAxisSweep3InternalItE12updateHandleEtRK9btVector3S3_P12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi267:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi268:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi269:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi270:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi271:
	.cfi_def_cfa_offset 48
.Lcfi272:
	.cfi_offset %rbx, -48
.Lcfi273:
	.cfi_offset %r12, -40
.Lcfi274:
	.cfi_offset %r14, -32
.Lcfi275:
	.cfi_offset %r15, -24
.Lcfi276:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	4(%r15), %r12d
	cmpl	%r14d, %r12d
	jg	.LBB57_19
# BB#1:
	jge	.LBB57_19
# BB#2:
	cmpl	%r14d, 8(%r15)
	jge	.LBB57_14
# BB#3:
	testl	%r14d, %r14d
	je	.LBB57_4
# BB#5:
	movslq	%r14d, %rdi
	shlq	$5, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	4(%r15), %eax
	testl	%eax, %eax
	jg	.LBB57_7
	jmp	.LBB57_9
.LBB57_4:
	xorl	%ebp, %ebp
	movl	%r12d, %eax
	testl	%eax, %eax
	jle	.LBB57_9
.LBB57_7:                               # %.lr.ph.i.i
	cltq
	movl	$24, %ecx
	.p2align	4, 0x90
.LBB57_8:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	-24(%rdx,%rcx), %xmm0
	movups	%xmm0, -24(%rbp,%rcx)
	movq	-8(%rdx,%rcx), %rsi
	movq	%rsi, -8(%rbp,%rcx)
	movq	(%rdx,%rcx), %rdx
	movq	%rdx, (%rbp,%rcx)
	addq	$32, %rcx
	decq	%rax
	jne	.LBB57_8
.LBB57_9:                               # %_ZNK20btAlignedObjectArrayI16btBroadphasePairE4copyEiiPS0_.exit.i
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB57_13
# BB#10:
	cmpb	$0, 24(%r15)
	je	.LBB57_12
# BB#11:
	callq	_Z21btAlignedFreeInternalPv
.LBB57_12:
	movq	$0, 16(%r15)
.LBB57_13:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.preheader
	movb	$1, 24(%r15)
	movq	%rbp, 16(%r15)
	movl	%r14d, 8(%r15)
	cmpl	%r14d, %r12d
	jge	.LBB57_19
.LBB57_14:                              # %.lr.ph
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	movl	%r14d, %ecx
	subl	%r12d, %ecx
	leaq	-1(%rax), %rsi
	testb	$1, %cl
	movq	%rdx, %rcx
	je	.LBB57_16
# BB#15:                                # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol
	movq	16(%r15), %rcx
	movq	%rdx, %rdi
	shlq	$5, %rdi
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rcx,%rdi)
	movq	16(%rbx), %rbp
	movq	%rbp, 16(%rcx,%rdi)
	movq	24(%rbx), %rbp
	movq	%rbp, 24(%rcx,%rdi)
	leaq	1(%rdx), %rcx
.LBB57_16:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit.prol.loopexit
	cmpq	%rdx, %rsi
	je	.LBB57_19
# BB#17:                                # %.lr.ph.new
	subq	%rcx, %rax
	shlq	$5, %rcx
	.p2align	4, 0x90
.LBB57_18:                              # %_ZN20btAlignedObjectArrayI16btBroadphasePairE7reserveEi.exit
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 16(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 24(%rdx,%rcx)
	movq	16(%r15), %rdx
	movups	(%rbx), %xmm0
	movups	%xmm0, 32(%rdx,%rcx)
	movq	16(%rbx), %rsi
	movq	%rsi, 48(%rdx,%rcx)
	movq	24(%rbx), %rsi
	movq	%rsi, 56(%rdx,%rcx)
	addq	$64, %rcx
	addq	$-2, %rax
	jne	.LBB57_18
.LBB57_19:                              # %.loopexit
	movl	%r14d, 4(%r15)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end57:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_, .Lfunc_end57-_ZN20btAlignedObjectArrayI16btBroadphasePairE6resizeEiRKS0_
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi277:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi278:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi279:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi280:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi281:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi282:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi283:
	.cfi_def_cfa_offset 64
.Lcfi284:
	.cfi_offset %rbx, -56
.Lcfi285:
	.cfi_offset %r12, -48
.Lcfi286:
	.cfi_offset %r13, -40
.Lcfi287:
	.cfi_offset %r14, -32
.Lcfi288:
	.cfi_offset %r15, -24
.Lcfi289:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	%esi, %r12d
	movq	%rdi, %r15
	movq	%rdx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB58_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB58_2 Depth 2
                                        #       Child Loop BB58_20 Depth 3
                                        #       Child Loop BB58_4 Depth 3
                                        #       Child Loop BB58_57 Depth 3
                                        #       Child Loop BB58_33 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r8
	leal	(%rsi,%rdx), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	shlq	$5, %rax
	movq	(%r8,%rax), %r10
	movq	8(%r8,%rax), %r13
	movq	16(%r8,%rax), %r9
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	jmp	.LBB58_2
	.p2align	4, 0x90
.LBB58_48:                              # %._crit_edge
                                        #   in Loop: Header=BB58_2 Depth=2
	movq	16(%r15), %r8
.LBB58_2:                               #   Parent Loop BB58_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB58_20 Depth 3
                                        #       Child Loop BB58_4 Depth 3
                                        #       Child Loop BB58_57 Depth 3
                                        #       Child Loop BB58_33 Depth 3
	movslq	%r12d, %r12
	testq	%r10, %r10
	je	.LBB58_3
# BB#19:                                # %.split.preheader
                                        #   in Loop: Header=BB58_2 Depth=2
	movl	24(%r10), %r11d
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB58_20
	.p2align	4, 0x90
.LBB58_55:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread
                                        #   in Loop: Header=BB58_20 Depth=3
	incq	%r12
	addq	$32, %rax
.LBB58_20:                              # %.split
                                        #   Parent Loop BB58_1 Depth=1
                                        #     Parent Loop BB58_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %ebx
	testq	%rdi, %rdi
	movl	$-1, %ebp
	je	.LBB58_22
# BB#21:                                #   in Loop: Header=BB58_20 Depth=3
	movl	24(%rdi), %ebp
.LBB58_22:                              #   in Loop: Header=BB58_20 Depth=3
	movq	-8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB58_24
# BB#23:                                #   in Loop: Header=BB58_20 Depth=3
	movl	24(%rcx), %ebx
.LBB58_24:                              #   in Loop: Header=BB58_20 Depth=3
	testq	%r13, %r13
	je	.LBB58_25
# BB#26:                                #   in Loop: Header=BB58_20 Depth=3
	movl	24(%r13), %r14d
	cmpl	%r11d, %ebp
	jg	.LBB58_55
	jmp	.LBB58_28
	.p2align	4, 0x90
.LBB58_25:                              #   in Loop: Header=BB58_20 Depth=3
	movl	$-1, %r14d
	cmpl	%r11d, %ebp
	jg	.LBB58_55
.LBB58_28:                              #   in Loop: Header=BB58_20 Depth=3
	cmpq	%r10, %rdi
	setne	%bpl
	cmpl	%r14d, %ebx
	jg	.LBB58_53
# BB#29:                                #   in Loop: Header=BB58_20 Depth=3
	testb	%bpl, %bpl
	jne	.LBB58_53
# BB#30:                                #   in Loop: Header=BB58_20 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB58_31
# BB#52:                                #   in Loop: Header=BB58_20 Depth=3
	cmpq	%r9, (%rax)
	ja	.LBB58_55
	jmp	.LBB58_31
	.p2align	4, 0x90
.LBB58_53:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB58_20 Depth=3
	cmpq	%r10, %rdi
	jne	.LBB58_31
# BB#54:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit
                                        #   in Loop: Header=BB58_20 Depth=3
	cmpl	%r14d, %ebx
	jg	.LBB58_55
	jmp	.LBB58_31
	.p2align	4, 0x90
.LBB58_3:                               # %.split.us.preheader
                                        #   in Loop: Header=BB58_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rdi
	jmp	.LBB58_4
	.p2align	4, 0x90
.LBB58_18:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread.us
                                        #   in Loop: Header=BB58_4 Depth=3
	incq	%r12
	addq	$32, %rdi
.LBB58_4:                               # %.split.us
                                        #   Parent Loop BB58_1 Depth=1
                                        #     Parent Loop BB58_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rdi), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %eax
	je	.LBB58_6
# BB#5:                                 #   in Loop: Header=BB58_4 Depth=3
	movl	24(%rbx), %eax
.LBB58_6:                               #   in Loop: Header=BB58_4 Depth=3
	movq	-8(%rdi), %rcx
	testq	%rcx, %rcx
	je	.LBB58_8
# BB#7:                                 #   in Loop: Header=BB58_4 Depth=3
	movl	24(%rcx), %r11d
.LBB58_8:                               #   in Loop: Header=BB58_4 Depth=3
	testq	%r13, %r13
	je	.LBB58_9
# BB#10:                                #   in Loop: Header=BB58_4 Depth=3
	movl	24(%r13), %ebp
	testl	%eax, %eax
	jns	.LBB58_18
	jmp	.LBB58_12
	.p2align	4, 0x90
.LBB58_9:                               #   in Loop: Header=BB58_4 Depth=3
	movl	$-1, %ebp
	testl	%eax, %eax
	jns	.LBB58_18
.LBB58_12:                              #   in Loop: Header=BB58_4 Depth=3
	testq	%rbx, %rbx
	setne	%al
	cmpl	%ebp, %r11d
	jg	.LBB58_16
# BB#13:                                #   in Loop: Header=BB58_4 Depth=3
	testb	%al, %al
	jne	.LBB58_16
# BB#14:                                #   in Loop: Header=BB58_4 Depth=3
	cmpq	%r13, %rcx
	jne	.LBB58_31
# BB#15:                                #   in Loop: Header=BB58_4 Depth=3
	cmpq	%r9, (%rdi)
	ja	.LBB58_18
	jmp	.LBB58_31
.LBB58_16:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB58_4 Depth=3
	testq	%rbx, %rbx
	jne	.LBB58_31
# BB#17:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.us
                                        #   in Loop: Header=BB58_4 Depth=3
	cmpl	%ebp, %r11d
	jg	.LBB58_18
	.p2align	4, 0x90
.LBB58_31:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader
                                        #   in Loop: Header=BB58_2 Depth=2
	movslq	%edx, %rdx
	testq	%r10, %r10
	je	.LBB58_32
# BB#56:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.preheader47
                                        #   in Loop: Header=BB58_2 Depth=2
	movl	24(%r10), %r11d
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB58_57
	.p2align	4, 0x90
.LBB58_71:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread
                                        #   in Loop: Header=BB58_57 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB58_57:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40
                                        #   Parent Loop BB58_1 Depth=1
                                        #     Parent Loop BB58_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rdi
	movl	$-1, %r14d
	testq	%rdi, %rdi
	movl	$-1, %ecx
	je	.LBB58_59
# BB#58:                                #   in Loop: Header=BB58_57 Depth=3
	movl	24(%rdi), %ecx
.LBB58_59:                              #   in Loop: Header=BB58_57 Depth=3
	testq	%r13, %r13
	je	.LBB58_61
# BB#60:                                #   in Loop: Header=BB58_57 Depth=3
	movl	24(%r13), %r14d
.LBB58_61:                              #   in Loop: Header=BB58_57 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB58_62
# BB#63:                                #   in Loop: Header=BB58_57 Depth=3
	movl	24(%rbp), %ebx
	cmpl	%ecx, %r11d
	jg	.LBB58_71
	jmp	.LBB58_65
	.p2align	4, 0x90
.LBB58_62:                              #   in Loop: Header=BB58_57 Depth=3
	movl	$-1, %ebx
	cmpl	%ecx, %r11d
	jg	.LBB58_71
.LBB58_65:                              #   in Loop: Header=BB58_57 Depth=3
	cmpq	%rdi, %r10
	setne	%cl
	cmpl	%ebx, %r14d
	jg	.LBB58_69
# BB#66:                                #   in Loop: Header=BB58_57 Depth=3
	testb	%cl, %cl
	jne	.LBB58_69
# BB#67:                                #   in Loop: Header=BB58_57 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB58_45
# BB#68:                                #   in Loop: Header=BB58_57 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB58_71
	jmp	.LBB58_45
	.p2align	4, 0x90
.LBB58_69:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB58_57 Depth=3
	cmpq	%rdi, %r10
	jne	.LBB58_45
# BB#70:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32
                                        #   in Loop: Header=BB58_57 Depth=3
	cmpl	%ebx, %r14d
	jg	.LBB58_71
	jmp	.LBB58_45
	.p2align	4, 0x90
.LBB58_32:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us.preheader
                                        #   in Loop: Header=BB58_2 Depth=2
	movq	%rdx, %rax
	shlq	$5, %rax
	leaq	16(%r8,%rax), %rax
	jmp	.LBB58_33
	.p2align	4, 0x90
.LBB58_51:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread.us
                                        #   in Loop: Header=BB58_33 Depth=3
	decq	%rdx
	addq	$-32, %rax
.LBB58_33:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit.thread40.us
                                        #   Parent Loop BB58_1 Depth=1
                                        #     Parent Loop BB58_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-16(%rax), %rbx
	movl	$-1, %r11d
	testq	%rbx, %rbx
	movl	$-1, %ecx
	je	.LBB58_35
# BB#34:                                #   in Loop: Header=BB58_33 Depth=3
	movl	24(%rbx), %ecx
.LBB58_35:                              #   in Loop: Header=BB58_33 Depth=3
	testq	%r13, %r13
	je	.LBB58_37
# BB#36:                                #   in Loop: Header=BB58_33 Depth=3
	movl	24(%r13), %r11d
.LBB58_37:                              #   in Loop: Header=BB58_33 Depth=3
	movq	-8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB58_38
# BB#39:                                #   in Loop: Header=BB58_33 Depth=3
	movl	24(%rbp), %edi
	cmpl	$-1, %ecx
	jl	.LBB58_51
	jmp	.LBB58_41
	.p2align	4, 0x90
.LBB58_38:                              #   in Loop: Header=BB58_33 Depth=3
	movl	$-1, %edi
	cmpl	$-1, %ecx
	jl	.LBB58_51
.LBB58_41:                              #   in Loop: Header=BB58_33 Depth=3
	testq	%rbx, %rbx
	setne	%cl
	cmpl	%edi, %r11d
	jg	.LBB58_49
# BB#42:                                #   in Loop: Header=BB58_33 Depth=3
	testb	%cl, %cl
	jne	.LBB58_49
# BB#43:                                #   in Loop: Header=BB58_33 Depth=3
	cmpq	%rbp, %r13
	jne	.LBB58_45
# BB#44:                                #   in Loop: Header=BB58_33 Depth=3
	cmpq	(%rax), %r9
	ja	.LBB58_51
	jmp	.LBB58_45
	.p2align	4, 0x90
.LBB58_49:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB58_33 Depth=3
	testq	%rbx, %rbx
	jne	.LBB58_45
# BB#50:                                # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.us
                                        #   in Loop: Header=BB58_33 Depth=3
	cmpl	%edi, %r11d
	jg	.LBB58_51
	.p2align	4, 0x90
.LBB58_45:                              # %_ZN29btBroadphasePairSortPredicateclERK16btBroadphasePairS2_.exit32.thread41
                                        #   in Loop: Header=BB58_2 Depth=2
	cmpl	%edx, %r12d
	jg	.LBB58_47
# BB#46:                                #   in Loop: Header=BB58_2 Depth=2
	movq	%r12, %rax
	shlq	$5, %rax
	movups	(%r8,%rax), %xmm0
	movups	16(%r8,%rax), %xmm1
	movq	%rdx, %rcx
	shlq	$5, %rcx
	movups	(%r8,%rcx), %xmm2
	movups	16(%r8,%rcx), %xmm3
	movups	%xmm3, 16(%r8,%rax)
	movups	%xmm2, (%r8,%rax)
	movq	16(%r15), %rax
	movups	%xmm0, (%rax,%rcx)
	movups	%xmm1, 16(%rax,%rcx)
	leal	1(%r12), %r12d
	leal	-1(%rdx), %edx
.LBB58_47:                              #   in Loop: Header=BB58_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB58_48
# BB#72:                                #   in Loop: Header=BB58_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB58_74
# BB#73:                                #   in Loop: Header=BB58_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
.LBB58_74:                              #   in Loop: Header=BB58_1 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	cmpl	%edx, %r12d
	jl	.LBB58_1
# BB#75:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end58:
	.size	_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii, .Lfunc_end58-_ZN20btAlignedObjectArrayI16btBroadphasePairE17quickSortInternalI29btBroadphasePairSortPredicateEEvT_ii
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_,"axG",@progbits,_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_,comdat
	.weak	_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_,@function
_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_: # @_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi293:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi294:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi295:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi296:
	.cfi_def_cfa_offset 64
.Lcfi297:
	.cfi_offset %rbx, -56
.Lcfi298:
	.cfi_offset %r12, -48
.Lcfi299:
	.cfi_offset %r13, -40
.Lcfi300:
	.cfi_offset %r14, -32
.Lcfi301:
	.cfi_offset %r15, -24
.Lcfi302:
	.cfi_offset %rbp, -16
	movl	%r9d, 4(%rsp)           # 4-byte Spill
	movl	%r8d, (%rsp)            # 4-byte Spill
	movq	%rdi, %rbx
	movss	16(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm7
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movss	48(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	52(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	movss	56(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	xorl	%r8d, %r8d
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	movl	$0, %r11d
	jae	.LBB59_2
# BB#1:                                 # %.sink.split.i
	movl	12(%rbx), %eax
	cvtsi2ssq	%rax, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %r11
	cmovael	%eax, %r11d
	andl	8(%rbx), %r11d
.LBB59_2:
	mulss	%xmm8, %xmm6
	ucomiss	%xmm7, %xmm1
	jae	.LBB59_4
# BB#3:                                 # %.sink.split16.i
	movl	12(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	ucomiss	%xmm0, %xmm7
	cvttss2si	%xmm7, %r8
	cmovael	%eax, %r8d
	andl	8(%rbx), %r8d
.LBB59_4:
	xorl	%r15d, %r15d
	xorps	%xmm7, %xmm7
	ucomiss	%xmm6, %xmm7
	jae	.LBB59_6
# BB#5:                                 # %.sink.split19.i
	movl	12(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rax, %xmm0
	ucomiss	%xmm0, %xmm6
	cvttss2si	%xmm6, %r15
	cmovael	%eax, %r15d
	andl	8(%rbx), %r15d
.LBB59_6:                               # %_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i.exit
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm6
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	mulss	%xmm4, %xmm0
	mulss	%xmm5, %xmm6
	movl	$1, %r12d
	ucomiss	%xmm0, %xmm7
	movl	$1, %r9d
	jae	.LBB59_8
# BB#7:                                 # %.sink.split.i59
	movl	12(%rbx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rax, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %r9
	cmovael	%eax, %r9d
	andl	8(%rbx), %r9d
	orl	$1, %r9d
.LBB59_8:
	mulss	%xmm8, %xmm1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm6, %xmm0
	jae	.LBB59_10
# BB#9:                                 # %.sink.split16.i61
	movl	12(%rbx), %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rax, %xmm2
	ucomiss	%xmm2, %xmm6
	cvttss2si	%xmm6, %r12
	cmovael	%eax, %r12d
	andl	8(%rbx), %r12d
	orl	$1, %r12d
.LBB59_10:
	movq	72(%rsp), %rsi
	movl	$1, %r10d
	ucomiss	%xmm1, %xmm0
	jae	.LBB59_12
# BB#11:                                # %.sink.split19.i63
	movl	12(%rbx), %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rdx, %xmm0
	ucomiss	%xmm0, %xmm1
	cvttss2si	%xmm1, %r10
	cmovael	%edx, %r10d
	andl	8(%rbx), %r10d
	orl	$1, %r10d
.LBB59_12:                              # %_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i.exit64
	movl	80(%rbx), %r14d
	movq	72(%rbx), %r13
	leaq	(%r14,%r14,2), %rdx
	shlq	$5, %rdx
	movl	60(%r13,%rdx), %ebp
	movl	%ebp, 80(%rbx)
	movl	64(%rbx), %ebp
	leal	1(%rbp), %eax
	movl	%eax, 64(%rbx)
	movl	%r14d, 24(%r13,%rdx)
	movq	%rcx, (%r13,%rdx)
	movl	(%rsp), %eax            # 4-byte Reload
	movw	%ax, 8(%r13,%rdx)
	movl	4(%rsp), %eax           # 4-byte Reload
	movw	%ax, 10(%r13,%rdx)
	movq	%rsi, 16(%r13,%rdx)
	leal	1(%rbp,%rbp), %ecx
	addl	$2, 72(%r13)
	leal	3(%rbp,%rbp), %eax
	movq	88(%rbx), %rsi
	movq	(%rsi,%rcx,8), %rdi
	movq	%rdi, (%rsi,%rax,8)
	movq	88(%rbx), %rsi
	movl	%r11d, (%rsi,%rcx,8)
	leal	2(%rbp,%rbp), %edi
	movl	%r14d, 4(%rsi,%rcx,8)
	movl	%r9d, (%rsi,%rdi,8)
	movl	%r14d, 4(%rsi,%rdi,8)
	movl	%ecx, 60(%r13,%rdx)
	movl	%edi, 72(%r13,%rdx)
	movq	72(%rbx), %rsi
	addl	$2, 76(%rsi)
	movq	96(%rbx), %rsi
	movq	(%rsi,%rcx,8), %rbp
	movq	%rbp, (%rsi,%rax,8)
	movq	96(%rbx), %rsi
	movl	%r8d, (%rsi,%rcx,8)
	movl	%r14d, 4(%rsi,%rcx,8)
	movl	%r12d, (%rsi,%rdi,8)
	movl	%r14d, 4(%rsi,%rdi,8)
	movl	%ecx, 64(%r13,%rdx)
	movl	%edi, 76(%r13,%rdx)
	movq	72(%rbx), %rsi
	addl	$2, 80(%rsi)
	movq	104(%rbx), %rsi
	movq	(%rsi,%rcx,8), %rbp
	movq	%rbp, (%rsi,%rax,8)
	movq	104(%rbx), %rax
	movl	%r15d, (%rax,%rcx,8)
	movl	%r14d, 4(%rax,%rcx,8)
	movl	%r10d, (%rax,%rdi,8)
	leaq	72(%r13,%rdx), %r9
	movl	%r14d, 4(%rax,%rdi,8)
	movl	%ecx, 68(%r13,%rdx)
	movl	%edi, 80(%r13,%rdx)
	movq	88(%rbx), %rbp
	movl	60(%r13,%rdx), %ecx
	movl	-8(%rbp,%rcx,8), %edi
	cmpl	%edi, (%rbp,%rcx,8)
	jae	.LBB59_17
# BB#13:                                # %.lr.ph.i80
	movq	72(%rbx), %rax
	leaq	(%rbp,%rcx,8), %rsi
	movl	4(%rbp,%rcx,8), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	leaq	60(%rax,%rcx), %rcx
	jmp	.LBB59_14
	.p2align	4, 0x90
.LBB59_15:                              # %.lr.ph.split..lr.ph.split_crit_edge.i87
                                        #   in Loop: Header=BB59_14 Depth=1
	addq	$-8, %rsi
	movq	72(%rbx), %rax
.LBB59_14:                              # %.lr.ph.split.i85
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi), %ebp
	leaq	(%rbp,%rbp,2), %rbp
	shlq	$5, %rbp
	testb	$1, %dil
	leaq	60(%rax,%rbp), %rdi
	leaq	72(%rax,%rbp), %rax
	cmoveq	%rdi, %rax
	incl	(%rax)
	decl	(%rcx)
	movq	-8(%rsi), %rax
	movq	(%rsi), %rdi
	movq	%rax, (%rsi)
	movq	%rdi, -8(%rsi)
	movl	-16(%rsi), %edi
	cmpl	%edi, -8(%rsi)
	jb	.LBB59_15
# BB#16:                                # %_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb.exit88.loopexit
	movq	88(%rbx), %rbp
.LBB59_17:                              # %_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb.exit88
	leaq	64(%r13,%rdx), %r8
	movl	(%r9), %eax
	movl	-8(%rbp,%rax,8), %edi
	cmpl	%edi, (%rbp,%rax,8)
	jae	.LBB59_21
# BB#18:                                # %.lr.ph.i70
	leaq	(%rbp,%rax,8), %rcx
	movl	4(%rbp,%rax,8), %esi
	movq	72(%rbx), %rax
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	leaq	72(%rax,%rsi), %rsi
	jmp	.LBB59_19
	.p2align	4, 0x90
.LBB59_20:                              # %..lr.ph.split_crit_edge.i77
                                        #   in Loop: Header=BB59_19 Depth=1
	addq	$-8, %rcx
	movq	72(%rbx), %rax
.LBB59_19:                              # %.lr.ph.split.i75
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rcx), %ebp
	leaq	(%rbp,%rbp,2), %rbp
	shlq	$5, %rbp
	testb	$1, %dil
	leaq	72(%rax,%rbp), %rdi
	leaq	60(%rax,%rbp), %rax
	cmovneq	%rdi, %rax
	incl	(%rax)
	decl	(%rsi)
	movq	-8(%rcx), %rax
	movq	(%rcx), %rdi
	movq	%rax, (%rcx)
	movq	%rdi, -8(%rcx)
	movl	-16(%rcx), %edi
	cmpl	%edi, -8(%rcx)
	jb	.LBB59_20
.LBB59_21:                              # %_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb.exit78
	leaq	76(%r13,%rdx), %r9
	movq	96(%rbx), %rdi
	movl	(%r8), %ebp
	movl	-8(%rdi,%rbp,8), %ecx
	cmpl	%ecx, (%rdi,%rbp,8)
	jae	.LBB59_26
# BB#22:                                # %.lr.ph.i65
	movq	72(%rbx), %rax
	leaq	(%rdi,%rbp,8), %rsi
	movl	4(%rdi,%rbp,8), %edi
	leaq	(%rdi,%rdi,2), %rdi
	shlq	$5, %rdi
	leaq	64(%rax,%rdi), %rdi
	jmp	.LBB59_23
	.p2align	4, 0x90
.LBB59_24:                              # %.lr.ph.split..lr.ph.split_crit_edge.i
                                        #   in Loop: Header=BB59_23 Depth=1
	addq	$-8, %rsi
	movq	72(%rbx), %rax
.LBB59_23:                              # %.lr.ph.split.i67
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rsi), %ebp
	leaq	(%rbp,%rbp,2), %rbp
	shlq	$5, %rbp
	testb	$1, %cl
	leaq	64(%rax,%rbp), %rcx
	leaq	76(%rax,%rbp), %rax
	cmoveq	%rcx, %rax
	incl	(%rax)
	decl	(%rdi)
	movq	-8(%rsi), %rax
	movq	(%rsi), %rcx
	movq	%rax, (%rsi)
	movq	%rcx, -8(%rsi)
	movl	-16(%rsi), %ecx
	cmpl	%ecx, -8(%rsi)
	jb	.LBB59_24
# BB#25:                                # %_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb.exit.loopexit
	movq	96(%rbx), %rdi
.LBB59_26:                              # %_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb.exit
	movq	64(%rsp), %r15
	leaq	68(%r13,%rdx), %r12
	movl	(%r9), %edx
	movl	-8(%rdi,%rdx,8), %ecx
	cmpl	%ecx, (%rdi,%rdx,8)
	jae	.LBB59_30
# BB#27:                                # %.lr.ph.i
	leaq	(%rdi,%rdx,8), %rax
	movl	4(%rdi,%rdx,8), %edx
	movq	72(%rbx), %rsi
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$5, %rdx
	leaq	76(%rsi,%rdx), %rdx
	jmp	.LBB59_28
	.p2align	4, 0x90
.LBB59_29:                              # %..lr.ph.split_crit_edge.i
                                        #   in Loop: Header=BB59_28 Depth=1
	addq	$-8, %rax
	movq	72(%rbx), %rsi
.LBB59_28:                              # %.lr.ph.split.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rax), %edi
	leaq	(%rdi,%rdi,2), %rdi
	shlq	$5, %rdi
	testb	$1, %cl
	leaq	76(%rsi,%rdi), %rcx
	leaq	64(%rsi,%rdi), %rsi
	cmovneq	%rcx, %rsi
	incl	(%rsi)
	decl	(%rdx)
	movq	-8(%rax), %rcx
	movq	(%rax), %rsi
	movq	%rcx, (%rax)
	movq	%rsi, -8(%rax)
	movl	-16(%rax), %ecx
	cmpl	%ecx, -8(%rax)
	jb	.LBB59_29
.LBB59_30:                              # %_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb.exit
	movl	(%r12), %edx
	movl	$2, %esi
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb
	movl	12(%r12), %edx
	movl	$2, %esi
	movl	$1, %r8d
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end59:
	.size	_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_, .Lfunc_end59-_ZN20btAxisSweep3InternalIjE9addHandleERK9btVector3S3_PvssP12btDispatcherS4_
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb,@function
_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb: # @_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi303:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi304:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi305:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi306:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi307:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi308:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi309:
	.cfi_def_cfa_offset 96
.Lcfi310:
	.cfi_offset %rbx, -56
.Lcfi311:
	.cfi_offset %r12, -48
.Lcfi312:
	.cfi_offset %r13, -40
.Lcfi313:
	.cfi_offset %r14, -32
.Lcfi314:
	.cfi_offset %r15, -24
.Lcfi315:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	%esi, %r13
	movq	88(%r14,%r13,8), %rcx
	movl	%edx, %esi
	movl	-8(%rcx,%rsi,8), %eax
	cmpl	%eax, (%rcx,%rsi,8)
	jae	.LBB60_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rsi,8), %rbp
	movq	72(%r14), %rdx
	movl	4(%rcx,%rsi,8), %ecx
	leaq	(%rcx,%rcx,2), %rsi
	shlq	$5, %rsi
	addq	%rdx, %rsi
	leaq	60(%rsi,%r13,4), %r15
	movl	$1, %r9d
	movl	$1, %r12d
	movl	%r13d, %ecx
	shll	%cl, %r12d
	andl	$3, %r12d
	movl	%r12d, %ecx
	shll	%cl, %r9d
	testb	%r8b, %r8b
	je	.LBB60_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %r9d
	leaq	72(%rsi,%r12,4), %r8
	leaq	72(%rsi,%r9,4), %r10
	jmp	.LBB60_3
	.p2align	4, 0x90
.LBB60_15:                              # %.lr.ph.split..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB60_14 Depth=1
	addq	$-8, %rbp
	movq	72(%r14), %rdx
.LBB60_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	addq	%rdx, %rcx
	testb	$1, %al
	leaq	60(%rcx,%r13,4), %rax
	leaq	72(%rcx,%r13,4), %rcx
	cmoveq	%rax, %rcx
	incl	(%rcx)
	decl	(%r15)
	movq	-8(%rbp), %rax
	movq	(%rbp), %rcx
	movq	%rax, (%rbp)
	movq	%rcx, -8(%rbp)
	movl	-16(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jb	.LBB60_15
	jmp	.LBB60_16
	.p2align	4, 0x90
.LBB60_13:                              # %..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB60_3 Depth=1
	addq	$-8, %rbp
	movq	72(%r14), %rdx
.LBB60_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %ecx
	leaq	(%rcx,%rcx,2), %rbx
	shlq	$5, %rbx
	addq	%rdx, %rbx
	testb	$1, %al
	jne	.LBB60_4
# BB#11:                                #   in Loop: Header=BB60_3 Depth=1
	leaq	60(%rbx,%r13,4), %rax
	jmp	.LBB60_12
	.p2align	4, 0x90
.LBB60_4:                               #   in Loop: Header=BB60_3 Depth=1
	movl	(%r8), %eax
	cmpl	60(%rbx,%r12,4), %eax
	jb	.LBB60_10
# BB#5:                                 #   in Loop: Header=BB60_3 Depth=1
	movl	72(%rbx,%r12,4), %eax
	cmpl	-12(%r8), %eax
	jb	.LBB60_10
# BB#6:                                 #   in Loop: Header=BB60_3 Depth=1
	movl	(%r10), %eax
	cmpl	60(%rbx,%r9,4), %eax
	jb	.LBB60_10
# BB#7:                                 # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB60_3 Depth=1
	movl	72(%rbx,%r9,4), %eax
	cmpl	-12(%r10), %eax
	jb	.LBB60_10
# BB#8:                                 #   in Loop: Header=BB60_3 Depth=1
	movq	136(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%r10, 16(%rsp)          # 8-byte Spill
	callq	*16(%rax)
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB60_10
# BB#9:                                 #   in Loop: Header=BB60_3 Depth=1
	movq	(%rdi), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbx, %rdx
	callq	*16(%rax)
	movq	16(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	.p2align	4, 0x90
.LBB60_10:                              # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB60_3 Depth=1
	leaq	72(%rbx,%r13,4), %rax
.LBB60_12:                              #   in Loop: Header=BB60_3 Depth=1
	incl	(%rax)
	decl	(%r15)
	movq	-8(%rbp), %rax
	movq	(%rbp), %rcx
	movq	%rax, (%rbp)
	movq	%rcx, -8(%rbp)
	movl	-16(%rbp), %eax
	cmpl	%eax, -8(%rbp)
	jb	.LBB60_13
.LBB60_16:                              # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end60:
	.size	_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb, .Lfunc_end60-_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb,@function
_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb: # @_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi316:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi317:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi318:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi319:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi320:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi321:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi322:
	.cfi_def_cfa_offset 80
.Lcfi323:
	.cfi_offset %rbx, -56
.Lcfi324:
	.cfi_offset %r12, -48
.Lcfi325:
	.cfi_offset %r13, -40
.Lcfi326:
	.cfi_offset %r14, -32
.Lcfi327:
	.cfi_offset %r15, -24
.Lcfi328:
	.cfi_offset %rbp, -16
	movq	%rcx, %r10
	movq	%rdi, %r9
	movslq	%esi, %r13
	movq	88(%r9,%r13,8), %rcx
	movl	%edx, %edx
	movl	-8(%rcx,%rdx,8), %eax
	cmpl	%eax, (%rcx,%rdx,8)
	jae	.LBB61_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rdx,8), %r15
	movl	4(%rcx,%rdx,8), %edx
	movq	72(%r9), %rbx
	movl	$1, %esi
	movl	$1, %r12d
	movl	%r13d, %ecx
	shll	%cl, %r12d
	andl	$3, %r12d
	movl	%r12d, %ecx
	shll	%cl, %esi
	leaq	(%rdx,%rdx,2), %rcx
	shlq	$5, %rcx
	addq	%rbx, %rcx
	leaq	72(%rcx,%r13,4), %r14
	testb	%r8b, %r8b
	je	.LBB61_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %esi
	jmp	.LBB61_3
	.p2align	4, 0x90
.LBB61_15:                              # %..lr.ph.split_crit_edge
                                        #   in Loop: Header=BB61_14 Depth=1
	addq	$-8, %r15
	movq	72(%r9), %rbx
.LBB61_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	addq	%rbx, %rcx
	testb	$1, %al
	leaq	72(%rcx,%r13,4), %rax
	leaq	60(%rcx,%r13,4), %rcx
	cmovneq	%rax, %rcx
	incl	(%rcx)
	decl	(%r14)
	movq	-8(%r15), %rax
	movq	(%r15), %rcx
	movq	%rax, (%r15)
	movq	%rcx, -8(%r15)
	movl	-16(%r15), %eax
	cmpl	%eax, -8(%r15)
	jb	.LBB61_15
	jmp	.LBB61_16
	.p2align	4, 0x90
.LBB61_13:                              # %..lr.ph.split.us_crit_edge
                                        #   in Loop: Header=BB61_3 Depth=1
	addq	$-8, %r15
	movq	72(%r9), %rbx
.LBB61_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r15), %ecx
	leaq	(%rcx,%rcx,2), %rbp
	shlq	$5, %rbp
	addq	%rbx, %rbp
	testb	$1, %al
	jne	.LBB61_4
# BB#5:                                 #   in Loop: Header=BB61_3 Depth=1
	movl	4(%r15), %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	addq	%rax, %rbx
	movl	72(%rbx,%r12,4), %eax
	cmpl	60(%rbp,%r12,4), %eax
	jb	.LBB61_11
# BB#6:                                 #   in Loop: Header=BB61_3 Depth=1
	movl	72(%rbp,%r12,4), %eax
	cmpl	60(%rbx,%r12,4), %eax
	jb	.LBB61_11
# BB#7:                                 #   in Loop: Header=BB61_3 Depth=1
	movl	72(%rbx,%rsi,4), %eax
	cmpl	60(%rbp,%rsi,4), %eax
	jb	.LBB61_11
# BB#8:                                 # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB61_3 Depth=1
	movl	72(%rbp,%rsi,4), %eax
	cmpl	60(%rbx,%rsi,4), %eax
	jb	.LBB61_11
# BB#9:                                 #   in Loop: Header=BB61_3 Depth=1
	movq	136(%r9), %rdi
	movq	(%rdi), %rax
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r10, %rcx
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	movq	144(%r9), %rdi
	testq	%rdi, %rdi
	je	.LBB61_11
# BB#10:                                #   in Loop: Header=BB61_3 Depth=1
	movq	(%rdi), %rax
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%r10, %rcx
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	.p2align	4, 0x90
.LBB61_11:                              # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB61_3 Depth=1
	leaq	60(%rbp,%r13,4), %rax
	jmp	.LBB61_12
	.p2align	4, 0x90
.LBB61_4:                               #   in Loop: Header=BB61_3 Depth=1
	leaq	72(%rbp,%r13,4), %rax
.LBB61_12:                              #   in Loop: Header=BB61_3 Depth=1
	incl	(%rax)
	decl	(%r14)
	movq	-8(%r15), %rax
	movq	(%r15), %rcx
	movq	%rax, (%r15)
	movq	%rcx, -8(%r15)
	movl	-16(%r15), %eax
	cmpl	%eax, -8(%r15)
	jb	.LBB61_13
.LBB61_16:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end61:
	.size	_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb, .Lfunc_end61-_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher,@function
_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher: # @_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi329:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi330:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi331:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi332:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi333:
	.cfi_def_cfa_offset 48
.Lcfi334:
	.cfi_offset %rbx, -48
.Lcfi335:
	.cfi_offset %r12, -40
.Lcfi336:
	.cfi_offset %r14, -32
.Lcfi337:
	.cfi_offset %r15, -24
.Lcfi338:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	72(%rbx), %r12
	movq	136(%rbx), %rdi
	movl	%r14d, %ebp
	movq	(%rdi), %rax
	callq	*112(%rax)
	testb	%al, %al
	jne	.LBB62_2
# BB#1:
	leaq	(%rbp,%rbp,2), %rsi
	shlq	$5, %rsi
	addq	%r12, %rsi
	movq	136(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rdx
	callq	*32(%rax)
.LBB62_2:                               # %.preheader47
	movl	64(%rbx), %eax
	movq	72(%rbx), %rcx
	addl	$-2, 72(%rcx)
	addl	$-2, 76(%rcx)
	addl	$-2, 80(%rcx)
	leal	-1(%rax,%rax), %eax
	movslq	%eax, %r9
	movl	12(%rbx), %edi
	xorl	%edx, %edx
	leaq	(%rbp,%rbp,2), %r8
	shlq	$5, %r8
	addq	%r8, %r12
	.p2align	4, 0x90
.LBB62_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB62_6 Depth 2
                                        #     Child Loop BB62_11 Depth 2
	movq	88(%rbx,%rdx,8), %r10
	movl	72(%r12,%rdx,4), %esi
	movl	%edi, (%r10,%rsi,8)
	movl	12(%r10,%rsi,8), %eax
	testl	%eax, %eax
	je	.LBB62_4
# BB#5:                                 # %.lr.ph.i39
                                        #   in Loop: Header=BB62_3 Depth=1
	leaq	(%r10,%rsi,8), %rcx
	movl	4(%r10,%rsi,8), %esi
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	addq	72(%rbx), %rsi
	leaq	72(%rsi,%rdx,4), %rbp
	.p2align	4, 0x90
.LBB62_6:                               # %.lr.ph.split.i41
                                        #   Parent Loop BB62_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rcx), %esi
	cmpl	%esi, %edi
	jb	.LBB62_8
# BB#7:                                 #   in Loop: Header=BB62_6 Depth=2
	movq	72(%rbx), %rdi
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	testb	$1, %sil
	leaq	72(%rdi,%rax), %rsi
	leaq	60(%rdi,%rax), %rax
	cmovneq	%rsi, %rax
	decl	(%rax,%rdx,4)
	incl	(%rbp)
	movq	(%rcx), %rdi
	movq	8(%rcx), %rax
	movq	%rax, (%rcx)
	movq	%rdi, 8(%rcx)
	movl	20(%rcx), %eax
	leaq	8(%rcx), %rcx
	testl	%eax, %eax
	jne	.LBB62_6
.LBB62_8:                               # %_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb.exit.loopexit
                                        #   in Loop: Header=BB62_3 Depth=1
	movl	12(%rbx), %edi
	movq	88(%rbx,%rdx,8), %rax
	jmp	.LBB62_9
	.p2align	4, 0x90
.LBB62_4:                               #   in Loop: Header=BB62_3 Depth=1
	movq	%r10, %rax
.LBB62_9:                               # %_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb.exit
                                        #   in Loop: Header=BB62_3 Depth=1
	movl	60(%r12,%rdx,4), %esi
	movl	%edi, (%r10,%rsi,8)
	movl	12(%rax,%rsi,8), %ebp
	testl	%ebp, %ebp
	je	.LBB62_14
# BB#10:                                # %.lr.ph.i
                                        #   in Loop: Header=BB62_3 Depth=1
	leaq	(%rax,%rsi,8), %rdi
	movl	4(%rax,%rsi,8), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	addq	72(%rbx), %rcx
	leaq	60(%rcx,%rdx,4), %rcx
	movl	(%rax,%rsi,8), %eax
	.p2align	4, 0x90
.LBB62_11:                              # %.lr.ph.split.i
                                        #   Parent Loop BB62_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rdi), %esi
	cmpl	%esi, %eax
	jb	.LBB62_13
# BB#12:                                #   in Loop: Header=BB62_11 Depth=2
	movl	%ebp, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	addq	72(%rbx), %rax
	testb	$1, %sil
	leaq	60(%rax,%rdx,4), %rsi
	leaq	72(%rax,%rdx,4), %rax
	cmoveq	%rsi, %rax
	decl	(%rax)
	incl	(%rcx)
	movq	(%rdi), %rax
	movq	8(%rdi), %rsi
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	movl	20(%rdi), %ebp
	leaq	8(%rdi), %rdi
	testl	%ebp, %ebp
	jne	.LBB62_11
.LBB62_13:                              # %_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb.exit.loopexit
                                        #   in Loop: Header=BB62_3 Depth=1
	movl	12(%rbx), %edi
.LBB62_14:                              # %_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb.exit
                                        #   in Loop: Header=BB62_3 Depth=1
	movl	$0, 4(%r10,%r9,8)
	movl	%edi, (%r10,%r9,8)
	incq	%rdx
	cmpq	$3, %rdx
	jne	.LBB62_3
# BB#15:
	movq	72(%rbx), %rax
	movl	80(%rbx), %ecx
	movl	%ecx, 60(%rax,%r8)
	movl	%r14d, 80(%rbx)
	decl	64(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end62:
	.size	_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher, .Lfunc_end62-_ZN20btAxisSweep3InternalIjE12removeHandleEjP12btDispatcher
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb,@function
_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb: # @_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi339:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi340:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi341:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi342:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi343:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi344:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi345:
	.cfi_def_cfa_offset 112
.Lcfi346:
	.cfi_offset %rbx, -56
.Lcfi347:
	.cfi_offset %r12, -48
.Lcfi348:
	.cfi_offset %r13, -40
.Lcfi349:
	.cfi_offset %r14, -32
.Lcfi350:
	.cfi_offset %r15, -24
.Lcfi351:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	%esi, %r12
	movq	88(%r14,%r12,8), %rcx
	movl	%edx, %edx
	movl	12(%rcx,%rdx,8), %eax
	testl	%eax, %eax
	je	.LBB63_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rdx,8), %rbp
	movl	4(%rcx,%rdx,8), %edx
	movl	$1, %r9d
	movl	$1, %r10d
	movl	%r12d, %ecx
	shll	%cl, %r10d
	andl	$3, %r10d
	movl	%r10d, %ecx
	shll	%cl, %r9d
	leaq	(%rdx,%rdx,2), %rcx
	shlq	$5, %rcx
	addq	72(%r14), %rcx
	leaq	72(%rcx,%r12,4), %r11
	testb	%r8b, %r8b
	je	.LBB63_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %r9d
	leaq	72(%rcx,%r10,4), %rdx
	leaq	72(%rcx,%r9,4), %r8
	.p2align	4, 0x90
.LBB63_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbp), %ecx
	cmpl	%ecx, (%rbp)
	jb	.LBB63_16
# BB#4:                                 #   in Loop: Header=BB63_3 Depth=1
	movq	72(%r14), %r15
	movl	%eax, %eax
	testb	$1, %cl
	jne	.LBB63_5
# BB#6:                                 #   in Loop: Header=BB63_3 Depth=1
	movl	(%rdx), %ecx
	leaq	(%rax,%rax,2), %r13
	shlq	$5, %r13
	leaq	(%r15,%r13), %rbx
	cmpl	60(%rbx,%r10,4), %ecx
	jb	.LBB63_12
# BB#7:                                 #   in Loop: Header=BB63_3 Depth=1
	movl	72(%rbx,%r10,4), %eax
	cmpl	-12(%rdx), %eax
	jb	.LBB63_12
# BB#8:                                 #   in Loop: Header=BB63_3 Depth=1
	movl	(%r8), %eax
	cmpl	60(%rbx,%r9,4), %eax
	jb	.LBB63_12
# BB#9:                                 # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB63_3 Depth=1
	movl	72(%rbx,%r9,4), %eax
	cmpl	-12(%r8), %eax
	jb	.LBB63_12
# BB#10:                                #   in Loop: Header=BB63_3 Depth=1
	movl	4(%rbp), %eax
	leaq	(%rax,%rax,2), %rsi
	shlq	$5, %rsi
	addq	%r15, %rsi
	movq	136(%r14), %rdi
	movq	(%rdi), %rax
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	callq	*16(%rax)
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB63_12
# BB#11:                                #   in Loop: Header=BB63_3 Depth=1
	movq	(%rdi), %rax
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	*16(%rax)
	movq	8(%rsp), %r8            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB63_12:                              # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB63_3 Depth=1
	leaq	60(%r15,%r13), %rax
	jmp	.LBB63_13
	.p2align	4, 0x90
.LBB63_5:                               #   in Loop: Header=BB63_3 Depth=1
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	leaq	72(%r15,%rax), %rax
.LBB63_13:                              #   in Loop: Header=BB63_3 Depth=1
	decl	(%rax,%r12,4)
	incl	(%r11)
	movq	(%rbp), %rax
	movq	8(%rbp), %rcx
	movq	%rcx, (%rbp)
	movq	%rax, 8(%rbp)
	movl	20(%rbp), %eax
	leaq	8(%rbp), %rbp
	testl	%eax, %eax
	jne	.LBB63_3
	jmp	.LBB63_16
	.p2align	4, 0x90
.LBB63_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbp), %ecx
	cmpl	%ecx, (%rbp)
	jb	.LBB63_16
# BB#15:                                #   in Loop: Header=BB63_14 Depth=1
	movq	72(%r14), %rdx
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	testb	$1, %cl
	leaq	72(%rdx,%rax), %rcx
	leaq	60(%rdx,%rax), %rax
	cmovneq	%rcx, %rax
	decl	(%rax,%r12,4)
	incl	(%r11)
	movq	(%rbp), %rax
	movq	8(%rbp), %rcx
	movq	%rcx, (%rbp)
	movq	%rax, 8(%rbp)
	movl	20(%rbp), %eax
	leaq	8(%rbp), %rbp
	testl	%eax, %eax
	jne	.LBB63_14
.LBB63_16:                              # %.critedge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end63:
	.size	_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb, .Lfunc_end63-_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb,"axG",@progbits,_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb,comdat
	.weak	_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb,@function
_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb: # @_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi352:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi353:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi354:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi355:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi356:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi357:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi358:
	.cfi_def_cfa_offset 80
.Lcfi359:
	.cfi_offset %rbx, -56
.Lcfi360:
	.cfi_offset %r12, -48
.Lcfi361:
	.cfi_offset %r13, -40
.Lcfi362:
	.cfi_offset %r14, -32
.Lcfi363:
	.cfi_offset %r15, -24
.Lcfi364:
	.cfi_offset %rbp, -16
	movq	%rcx, %r9
	movq	%rdi, %r14
	movslq	%esi, %r13
	movq	88(%r14,%r13,8), %rcx
	movl	%edx, %edx
	movl	12(%rcx,%rdx,8), %eax
	testl	%eax, %eax
	je	.LBB64_16
# BB#1:                                 # %.lr.ph
	leaq	(%rcx,%rdx,8), %r15
	movl	4(%rcx,%rdx,8), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	addq	72(%r14), %rcx
	leaq	60(%rcx,%r13,4), %rdx
	movl	$1, %esi
	movl	$1, %r12d
	movl	%r13d, %ecx
	shll	%cl, %r12d
	andl	$3, %r12d
	movl	%r12d, %ecx
	shll	%cl, %esi
	testb	%r8b, %r8b
	je	.LBB64_14
# BB#2:                                 # %.lr.ph.split.us.preheader
	andl	$3, %esi
	.p2align	4, 0x90
.LBB64_3:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%r15), %ecx
	cmpl	%ecx, (%r15)
	jb	.LBB64_16
# BB#4:                                 #   in Loop: Header=BB64_3 Depth=1
	movq	72(%r14), %rbp
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rbx
	shlq	$5, %rbx
	addq	%rbp, %rbx
	testb	$1, %cl
	jne	.LBB64_5
# BB#12:                                #   in Loop: Header=BB64_3 Depth=1
	leaq	60(%rbx,%r13,4), %rax
	jmp	.LBB64_13
	.p2align	4, 0x90
.LBB64_5:                               #   in Loop: Header=BB64_3 Depth=1
	movl	4(%r15), %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	addq	%rax, %rbp
	movl	72(%rbp,%r12,4), %eax
	cmpl	60(%rbx,%r12,4), %eax
	jb	.LBB64_11
# BB#6:                                 #   in Loop: Header=BB64_3 Depth=1
	movl	72(%rbx,%r12,4), %eax
	cmpl	60(%rbp,%r12,4), %eax
	jb	.LBB64_11
# BB#7:                                 #   in Loop: Header=BB64_3 Depth=1
	movl	72(%rbp,%rsi,4), %eax
	cmpl	60(%rbx,%rsi,4), %eax
	jb	.LBB64_11
# BB#8:                                 # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.us
                                        #   in Loop: Header=BB64_3 Depth=1
	movl	72(%rbx,%rsi,4), %eax
	cmpl	60(%rbp,%rsi,4), %eax
	jb	.LBB64_11
# BB#9:                                 #   in Loop: Header=BB64_3 Depth=1
	movq	136(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rbp, %rsi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdx
	movq	%r9, %rcx
	movq	%r9, 8(%rsp)            # 8-byte Spill
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	144(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB64_11
# BB#10:                                #   in Loop: Header=BB64_3 Depth=1
	movq	(%rdi), %rax
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%r9, %rcx
	callq	*24(%rax)
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	.p2align	4, 0x90
.LBB64_11:                              # %_ZN20btAxisSweep3InternalIjE13testOverlap2DEPKNS0_6HandleES3_ii.exit.thread.us
                                        #   in Loop: Header=BB64_3 Depth=1
	leaq	72(%rbx,%r13,4), %rax
.LBB64_13:                              #   in Loop: Header=BB64_3 Depth=1
	decl	(%rax)
	incl	(%rdx)
	movq	(%r15), %rax
	movq	8(%r15), %rcx
	movq	%rcx, (%r15)
	movq	%rax, 8(%r15)
	movl	20(%r15), %eax
	leaq	8(%r15), %r15
	testl	%eax, %eax
	jne	.LBB64_3
	jmp	.LBB64_16
	.p2align	4, 0x90
.LBB64_14:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%r15), %ecx
	cmpl	%ecx, (%r15)
	jb	.LBB64_16
# BB#15:                                #   in Loop: Header=BB64_14 Depth=1
	movl	%eax, %eax
	leaq	(%rax,%rax,2), %rax
	shlq	$5, %rax
	addq	72(%r14), %rax
	testb	$1, %cl
	leaq	60(%rax,%r13,4), %rcx
	leaq	72(%rax,%r13,4), %rax
	cmoveq	%rcx, %rax
	decl	(%rax)
	incl	(%rdx)
	movq	(%r15), %rax
	movq	8(%r15), %rcx
	movq	%rcx, (%r15)
	movq	%rax, 8(%r15)
	movl	20(%r15), %eax
	leaq	8(%r15), %r15
	testl	%eax, %eax
	jne	.LBB64_14
.LBB64_16:                              # %.critedge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end64:
	.size	_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb, .Lfunc_end64-_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb
	.cfi_endproc

	.section	.text._ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher,"axG",@progbits,_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher,comdat
	.weak	_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher
	.p2align	4, 0x90
	.type	_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher,@function
_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher: # @_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi365:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi366:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi367:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi368:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi369:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi370:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi371:
	.cfi_def_cfa_offset 96
.Lcfi372:
	.cfi_offset %rbx, -56
.Lcfi373:
	.cfi_offset %r12, -48
.Lcfi374:
	.cfi_offset %r13, -40
.Lcfi375:
	.cfi_offset %r14, -32
.Lcfi376:
	.cfi_offset %r15, -24
.Lcfi377:
	.cfi_offset %rbp, -16
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movq	72(%rdi), %rax
	movss	16(%rdi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movss	20(%rdi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm7
	movss	8(%rdx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	24(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm6
	movss	48(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	52(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	movq	%rdi, (%rsp)            # 8-byte Spill
	movss	56(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	xorl	%edi, %edi
	xorps	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	movl	$0, %edx
	jae	.LBB65_2
# BB#1:                                 # %.sink.split.i
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	12(%rbx), %ebp
	cvtsi2ssq	%rbp, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %rdx
	cmovael	%ebp, %edx
	andl	8(%rbx), %edx
.LBB65_2:
	mulss	%xmm8, %xmm6
	movl	%edx, 28(%rsp)
	ucomiss	%xmm7, %xmm1
	jae	.LBB65_4
# BB#3:                                 # %.sink.split16.i
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	12(%rbx), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rbp, %xmm0
	ucomiss	%xmm0, %xmm7
	cvttss2si	%xmm7, %rdi
	cmovael	%ebp, %edi
	andl	8(%rbx), %edi
.LBB65_4:
	movl	%edi, 32(%rsp)
	xorl	%edi, %edi
	xorps	%xmm7, %xmm7
	ucomiss	%xmm6, %xmm7
	jae	.LBB65_6
# BB#5:                                 # %.sink.split19.i
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	12(%rbx), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rbp, %xmm0
	ucomiss	%xmm0, %xmm6
	cvttss2si	%xmm6, %rdi
	cmovael	%ebp, %edi
	andl	8(%rbx), %edi
.LBB65_6:                               # %_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i.exit
	movl	%edi, 36(%rsp)
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm10, %xmm0
	subss	%xmm9, %xmm6
	movss	8(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	mulss	%xmm4, %xmm0
	mulss	%xmm5, %xmm6
	movl	$1, %edi
	ucomiss	%xmm0, %xmm7
	movl	$1, %ecx
	jae	.LBB65_8
# BB#7:                                 # %.sink.split.i49
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	12(%rbx), %ebp
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rbp, %xmm2
	ucomiss	%xmm2, %xmm0
	cvttss2si	%xmm0, %rcx
	cmovael	%ebp, %ecx
	andl	8(%rbx), %ecx
	orl	$1, %ecx
.LBB65_8:
	movl	%esi, %esi
	mulss	%xmm8, %xmm1
	movl	%ecx, 16(%rsp)
	xorps	%xmm0, %xmm0
	ucomiss	%xmm6, %xmm0
	jae	.LBB65_10
# BB#9:                                 # %.sink.split16.i51
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	12(%rbx), %ebp
	xorps	%xmm2, %xmm2
	cvtsi2ssq	%rbp, %xmm2
	ucomiss	%xmm2, %xmm6
	cvttss2si	%xmm6, %rdi
	cmovael	%ebp, %edi
	andl	8(%rbx), %edi
	orl	$1, %edi
.LBB65_10:
	movl	%edi, 20(%rsp)
	movl	$1, %edi
	ucomiss	%xmm1, %xmm0
	jae	.LBB65_12
# BB#11:                                # %.sink.split19.i53
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	12(%rbx), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2ssq	%rbp, %xmm0
	ucomiss	%xmm0, %xmm1
	cvttss2si	%xmm1, %rdi
	cmovael	%ebp, %edi
	andl	8(%rbx), %edi
	orl	$1, %edi
.LBB65_12:                              # %_ZNK20btAxisSweep3InternalIjE8quantizeEPjRK9btVector3i.exit54
	movl	%edi, 24(%rsp)
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	leaq	72(%rax,%rsi), %rbp
	xorl	%ebx, %ebx
	jmp	.LBB65_13
	.p2align	4, 0x90
.LBB65_22:                              # %._crit_edge
                                        #   in Loop: Header=BB65_13 Depth=1
	movl	32(%rsp,%rbx,4), %edx
	movl	20(%rsp,%rbx,4), %ecx
	incq	%rbx
.LBB65_13:                              # =>This Inner Loop Header: Depth=1
	movl	-12(%rbp,%rbx,4), %r13d
	movl	(%rbp,%rbx,4), %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	movq	88(%rax,%rbx,8), %rax
	movl	%ecx, %r14d
	subl	(%rax,%r12,8), %r14d
	movl	%edx, %r15d
	subl	(%rax,%r13,8), %r15d
	movl	%edx, (%rax,%r13,8)
	movl	%ecx, (%rax,%r12,8)
	jns	.LBB65_15
# BB#14:                                #   in Loop: Header=BB65_13 Depth=1
	movl	$1, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	movl	%r13d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalIjE11sortMinDownEijP12btDispatcherb
.LBB65_15:                              #   in Loop: Header=BB65_13 Depth=1
	testl	%r14d, %r14d
	jle	.LBB65_17
# BB#16:                                #   in Loop: Header=BB65_13 Depth=1
	movl	$1, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	movl	%r12d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalIjE9sortMaxUpEijP12btDispatcherb
.LBB65_17:                              #   in Loop: Header=BB65_13 Depth=1
	testl	%r15d, %r15d
	jle	.LBB65_19
# BB#18:                                #   in Loop: Header=BB65_13 Depth=1
	movl	$1, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	movl	%r13d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalIjE9sortMinUpEijP12btDispatcherb
.LBB65_19:                              #   in Loop: Header=BB65_13 Depth=1
	testl	%r14d, %r14d
	jns	.LBB65_21
# BB#20:                                #   in Loop: Header=BB65_13 Depth=1
	movl	$1, %r8d
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%ebx, %esi
	movl	%r12d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	_ZN20btAxisSweep3InternalIjE11sortMaxDownEijP12btDispatcherb
.LBB65_21:                              #   in Loop: Header=BB65_13 Depth=1
	cmpq	$2, %rbx
	jne	.LBB65_22
# BB#23:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end65:
	.size	_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher, .Lfunc_end65-_ZN20btAxisSweep3InternalIjE12updateHandleEjRK9btVector3S3_P12btDispatcher
	.cfi_endproc

	.type	_ZTV12btAxisSweep3,@object # @_ZTV12btAxisSweep3
	.section	.rodata._ZTV12btAxisSweep3,"aG",@progbits,_ZTV12btAxisSweep3,comdat
	.weak	_ZTV12btAxisSweep3
	.p2align	3
_ZTV12btAxisSweep3:
	.quad	0
	.quad	_ZTI12btAxisSweep3
	.quad	_ZN20btAxisSweep3InternalItED2Ev
	.quad	_ZN12btAxisSweep3D0Ev
	.quad	_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.quad	_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.quad	_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.quad	_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.quad	_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_
	.quad	_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalItE10printStatsEv
	.size	_ZTV12btAxisSweep3, 120

	.type	_ZTV17bt32BitAxisSweep3,@object # @_ZTV17bt32BitAxisSweep3
	.section	.rodata._ZTV17bt32BitAxisSweep3,"aG",@progbits,_ZTV17bt32BitAxisSweep3,comdat
	.weak	_ZTV17bt32BitAxisSweep3
	.p2align	3
_ZTV17bt32BitAxisSweep3:
	.quad	0
	.quad	_ZTI17bt32BitAxisSweep3
	.quad	_ZN20btAxisSweep3InternalIjED2Ev
	.quad	_ZN17bt32BitAxisSweep3D0Ev
	.quad	_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.quad	_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.quad	_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.quad	_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.quad	_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_
	.quad	_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalIjE10printStatsEv
	.size	_ZTV17bt32BitAxisSweep3, 120

	.type	_ZTS12btAxisSweep3,@object # @_ZTS12btAxisSweep3
	.section	.rodata._ZTS12btAxisSweep3,"aG",@progbits,_ZTS12btAxisSweep3,comdat
	.weak	_ZTS12btAxisSweep3
_ZTS12btAxisSweep3:
	.asciz	"12btAxisSweep3"
	.size	_ZTS12btAxisSweep3, 15

	.type	_ZTS20btAxisSweep3InternalItE,@object # @_ZTS20btAxisSweep3InternalItE
	.section	.rodata._ZTS20btAxisSweep3InternalItE,"aG",@progbits,_ZTS20btAxisSweep3InternalItE,comdat
	.weak	_ZTS20btAxisSweep3InternalItE
	.p2align	4
_ZTS20btAxisSweep3InternalItE:
	.asciz	"20btAxisSweep3InternalItE"
	.size	_ZTS20btAxisSweep3InternalItE, 26

	.type	_ZTS21btBroadphaseInterface,@object # @_ZTS21btBroadphaseInterface
	.section	.rodata._ZTS21btBroadphaseInterface,"aG",@progbits,_ZTS21btBroadphaseInterface,comdat
	.weak	_ZTS21btBroadphaseInterface
	.p2align	4
_ZTS21btBroadphaseInterface:
	.asciz	"21btBroadphaseInterface"
	.size	_ZTS21btBroadphaseInterface, 24

	.type	_ZTI21btBroadphaseInterface,@object # @_ZTI21btBroadphaseInterface
	.section	.rodata._ZTI21btBroadphaseInterface,"aG",@progbits,_ZTI21btBroadphaseInterface,comdat
	.weak	_ZTI21btBroadphaseInterface
	.p2align	3
_ZTI21btBroadphaseInterface:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS21btBroadphaseInterface
	.size	_ZTI21btBroadphaseInterface, 16

	.type	_ZTI20btAxisSweep3InternalItE,@object # @_ZTI20btAxisSweep3InternalItE
	.section	.rodata._ZTI20btAxisSweep3InternalItE,"aG",@progbits,_ZTI20btAxisSweep3InternalItE,comdat
	.weak	_ZTI20btAxisSweep3InternalItE
	.p2align	4
_ZTI20btAxisSweep3InternalItE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20btAxisSweep3InternalItE
	.quad	_ZTI21btBroadphaseInterface
	.size	_ZTI20btAxisSweep3InternalItE, 24

	.type	_ZTI12btAxisSweep3,@object # @_ZTI12btAxisSweep3
	.section	.rodata._ZTI12btAxisSweep3,"aG",@progbits,_ZTI12btAxisSweep3,comdat
	.weak	_ZTI12btAxisSweep3
	.p2align	4
_ZTI12btAxisSweep3:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS12btAxisSweep3
	.quad	_ZTI20btAxisSweep3InternalItE
	.size	_ZTI12btAxisSweep3, 24

	.type	_ZTS17bt32BitAxisSweep3,@object # @_ZTS17bt32BitAxisSweep3
	.section	.rodata._ZTS17bt32BitAxisSweep3,"aG",@progbits,_ZTS17bt32BitAxisSweep3,comdat
	.weak	_ZTS17bt32BitAxisSweep3
	.p2align	4
_ZTS17bt32BitAxisSweep3:
	.asciz	"17bt32BitAxisSweep3"
	.size	_ZTS17bt32BitAxisSweep3, 20

	.type	_ZTS20btAxisSweep3InternalIjE,@object # @_ZTS20btAxisSweep3InternalIjE
	.section	.rodata._ZTS20btAxisSweep3InternalIjE,"aG",@progbits,_ZTS20btAxisSweep3InternalIjE,comdat
	.weak	_ZTS20btAxisSweep3InternalIjE
	.p2align	4
_ZTS20btAxisSweep3InternalIjE:
	.asciz	"20btAxisSweep3InternalIjE"
	.size	_ZTS20btAxisSweep3InternalIjE, 26

	.type	_ZTI20btAxisSweep3InternalIjE,@object # @_ZTI20btAxisSweep3InternalIjE
	.section	.rodata._ZTI20btAxisSweep3InternalIjE,"aG",@progbits,_ZTI20btAxisSweep3InternalIjE,comdat
	.weak	_ZTI20btAxisSweep3InternalIjE
	.p2align	4
_ZTI20btAxisSweep3InternalIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20btAxisSweep3InternalIjE
	.quad	_ZTI21btBroadphaseInterface
	.size	_ZTI20btAxisSweep3InternalIjE, 24

	.type	_ZTI17bt32BitAxisSweep3,@object # @_ZTI17bt32BitAxisSweep3
	.section	.rodata._ZTI17bt32BitAxisSweep3,"aG",@progbits,_ZTI17bt32BitAxisSweep3,comdat
	.weak	_ZTI17bt32BitAxisSweep3
	.p2align	4
_ZTI17bt32BitAxisSweep3:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS17bt32BitAxisSweep3
	.quad	_ZTI20btAxisSweep3InternalIjE
	.size	_ZTI17bt32BitAxisSweep3, 24

	.type	_ZTV20btAxisSweep3InternalItE,@object # @_ZTV20btAxisSweep3InternalItE
	.section	.rodata._ZTV20btAxisSweep3InternalItE,"aG",@progbits,_ZTV20btAxisSweep3InternalItE,comdat
	.weak	_ZTV20btAxisSweep3InternalItE
	.p2align	3
_ZTV20btAxisSweep3InternalItE:
	.quad	0
	.quad	_ZTI20btAxisSweep3InternalItE
	.quad	_ZN20btAxisSweep3InternalItED2Ev
	.quad	_ZN20btAxisSweep3InternalItED0Ev
	.quad	_ZN20btAxisSweep3InternalItE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.quad	_ZN20btAxisSweep3InternalItE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalItE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.quad	_ZNK20btAxisSweep3InternalItE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.quad	_ZN20btAxisSweep3InternalItE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.quad	_ZN20btAxisSweep3InternalItE25calculateOverlappingPairsEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalItE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalItE17getBroadphaseAabbER9btVector3S2_
	.quad	_ZN20btAxisSweep3InternalItE9resetPoolEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalItE10printStatsEv
	.size	_ZTV20btAxisSweep3InternalItE, 120

	.type	_ZTV15btNullPairCache,@object # @_ZTV15btNullPairCache
	.section	.rodata._ZTV15btNullPairCache,"aG",@progbits,_ZTV15btNullPairCache,comdat
	.weak	_ZTV15btNullPairCache
	.p2align	3
_ZTV15btNullPairCache:
	.quad	0
	.quad	_ZTI15btNullPairCache
	.quad	_ZN15btNullPairCacheD2Ev
	.quad	_ZN15btNullPairCacheD0Ev
	.quad	_ZN15btNullPairCache18addOverlappingPairEP17btBroadphaseProxyS1_
	.quad	_ZN15btNullPairCache21removeOverlappingPairEP17btBroadphaseProxyS1_P12btDispatcher
	.quad	_ZN15btNullPairCache37removeOverlappingPairsContainingProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN15btNullPairCache26getOverlappingPairArrayPtrEv
	.quad	_ZNK15btNullPairCache26getOverlappingPairArrayPtrEv
	.quad	_ZN15btNullPairCache23getOverlappingPairArrayEv
	.quad	_ZN15btNullPairCache20cleanOverlappingPairER16btBroadphasePairP12btDispatcher
	.quad	_ZNK15btNullPairCache22getNumOverlappingPairsEv
	.quad	_ZN15btNullPairCache19cleanProxyFromPairsEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN15btNullPairCache24setOverlapFilterCallbackEP23btOverlapFilterCallback
	.quad	_ZN15btNullPairCache26processAllOverlappingPairsEP17btOverlapCallbackP12btDispatcher
	.quad	_ZN15btNullPairCache8findPairEP17btBroadphaseProxyS1_
	.quad	_ZN15btNullPairCache18hasDeferredRemovalEv
	.quad	_ZN15btNullPairCache28setInternalGhostPairCallbackEP25btOverlappingPairCallback
	.quad	_ZN15btNullPairCache20sortOverlappingPairsEP12btDispatcher
	.size	_ZTV15btNullPairCache, 152

	.type	_ZTS15btNullPairCache,@object # @_ZTS15btNullPairCache
	.section	.rodata._ZTS15btNullPairCache,"aG",@progbits,_ZTS15btNullPairCache,comdat
	.weak	_ZTS15btNullPairCache
	.p2align	4
_ZTS15btNullPairCache:
	.asciz	"15btNullPairCache"
	.size	_ZTS15btNullPairCache, 18

	.type	_ZTS22btOverlappingPairCache,@object # @_ZTS22btOverlappingPairCache
	.section	.rodata._ZTS22btOverlappingPairCache,"aG",@progbits,_ZTS22btOverlappingPairCache,comdat
	.weak	_ZTS22btOverlappingPairCache
	.p2align	4
_ZTS22btOverlappingPairCache:
	.asciz	"22btOverlappingPairCache"
	.size	_ZTS22btOverlappingPairCache, 25

	.type	_ZTS25btOverlappingPairCallback,@object # @_ZTS25btOverlappingPairCallback
	.section	.rodata._ZTS25btOverlappingPairCallback,"aG",@progbits,_ZTS25btOverlappingPairCallback,comdat
	.weak	_ZTS25btOverlappingPairCallback
	.p2align	4
_ZTS25btOverlappingPairCallback:
	.asciz	"25btOverlappingPairCallback"
	.size	_ZTS25btOverlappingPairCallback, 28

	.type	_ZTI25btOverlappingPairCallback,@object # @_ZTI25btOverlappingPairCallback
	.section	.rodata._ZTI25btOverlappingPairCallback,"aG",@progbits,_ZTI25btOverlappingPairCallback,comdat
	.weak	_ZTI25btOverlappingPairCallback
	.p2align	3
_ZTI25btOverlappingPairCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS25btOverlappingPairCallback
	.size	_ZTI25btOverlappingPairCallback, 16

	.type	_ZTI22btOverlappingPairCache,@object # @_ZTI22btOverlappingPairCache
	.section	.rodata._ZTI22btOverlappingPairCache,"aG",@progbits,_ZTI22btOverlappingPairCache,comdat
	.weak	_ZTI22btOverlappingPairCache
	.p2align	4
_ZTI22btOverlappingPairCache:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22btOverlappingPairCache
	.quad	_ZTI25btOverlappingPairCallback
	.size	_ZTI22btOverlappingPairCache, 24

	.type	_ZTI15btNullPairCache,@object # @_ZTI15btNullPairCache
	.section	.rodata._ZTI15btNullPairCache,"aG",@progbits,_ZTI15btNullPairCache,comdat
	.weak	_ZTI15btNullPairCache
	.p2align	4
_ZTI15btNullPairCache:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btNullPairCache
	.quad	_ZTI22btOverlappingPairCache
	.size	_ZTI15btNullPairCache, 24

	.type	_ZTV20btAxisSweep3InternalIjE,@object # @_ZTV20btAxisSweep3InternalIjE
	.section	.rodata._ZTV20btAxisSweep3InternalIjE,"aG",@progbits,_ZTV20btAxisSweep3InternalIjE,comdat
	.weak	_ZTV20btAxisSweep3InternalIjE
	.p2align	3
_ZTV20btAxisSweep3InternalIjE:
	.quad	0
	.quad	_ZTI20btAxisSweep3InternalIjE
	.quad	_ZN20btAxisSweep3InternalIjED2Ev
	.quad	_ZN20btAxisSweep3InternalIjED0Ev
	.quad	_ZN20btAxisSweep3InternalIjE11createProxyERK9btVector3S3_iPvssP12btDispatcherS4_
	.quad	_ZN20btAxisSweep3InternalIjE12destroyProxyEP17btBroadphaseProxyP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalIjE7setAabbEP17btBroadphaseProxyRK9btVector3S5_P12btDispatcher
	.quad	_ZNK20btAxisSweep3InternalIjE7getAabbEP17btBroadphaseProxyR9btVector3S4_
	.quad	_ZN20btAxisSweep3InternalIjE7rayTestERK9btVector3S3_R23btBroadphaseRayCallbackS3_S3_
	.quad	_ZN20btAxisSweep3InternalIjE25calculateOverlappingPairsEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalIjE23getOverlappingPairCacheEv
	.quad	_ZNK20btAxisSweep3InternalIjE17getBroadphaseAabbER9btVector3S2_
	.quad	_ZN20btAxisSweep3InternalIjE9resetPoolEP12btDispatcher
	.quad	_ZN20btAxisSweep3InternalIjE10printStatsEv
	.size	_ZTV20btAxisSweep3InternalIjE, 120


	.globl	_ZN12btAxisSweep3C1ERK9btVector3S2_tP22btOverlappingPairCacheb
	.type	_ZN12btAxisSweep3C1ERK9btVector3S2_tP22btOverlappingPairCacheb,@function
_ZN12btAxisSweep3C1ERK9btVector3S2_tP22btOverlappingPairCacheb = _ZN12btAxisSweep3C2ERK9btVector3S2_tP22btOverlappingPairCacheb
	.globl	_ZN17bt32BitAxisSweep3C1ERK9btVector3S2_jP22btOverlappingPairCacheb
	.type	_ZN17bt32BitAxisSweep3C1ERK9btVector3S2_jP22btOverlappingPairCacheb,@function
_ZN17bt32BitAxisSweep3C1ERK9btVector3S2_jP22btOverlappingPairCacheb = _ZN17bt32BitAxisSweep3C2ERK9btVector3S2_jP22btOverlappingPairCacheb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
