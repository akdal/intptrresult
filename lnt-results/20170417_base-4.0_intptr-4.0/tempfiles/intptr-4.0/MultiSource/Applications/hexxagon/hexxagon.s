	.text
	.file	"hexxagon.bc"
	.globl	_Z17stripFromDblSpacePc
	.p2align	4, 0x90
	.type	_Z17stripFromDblSpacePc,@function
_Z17stripFromDblSpacePc:                # @_Z17stripFromDblSpacePc
	.cfi_startproc
# BB#0:
	movb	(%rdi), %cl
	testb	%cl, %cl
	je	.LBB0_10
# BB#1:                                 # %.lr.ph.preheader
	leaq	1(%rdi), %rax
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%edx, %edx
	je	.LBB0_5
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	cmpb	$9, %cl
	je	.LBB0_6
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	cmpb	$32, %cl
	je	.LBB0_6
.LBB0_5:                                #   in Loop: Header=BB0_2 Depth=1
	movb	%cl, (%rdi)
	incq	%rdi
	movzbl	-1(%rax), %ecx
.LBB0_6:                                #   in Loop: Header=BB0_2 Depth=1
	movl	$1, %edx
	cmpb	$9, %cl
	je	.LBB0_9
# BB#7:                                 #   in Loop: Header=BB0_2 Depth=1
	cmpb	$32, %cl
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_2 Depth=1
	xorl	%edx, %edx
.LBB0_9:                                #   in Loop: Header=BB0_2 Depth=1
	movzbl	(%rax), %ecx
	incq	%rax
	testb	%cl, %cl
	jne	.LBB0_2
.LBB0_10:                               # %._crit_edge
	movb	$0, (%rdi)
	retq
.Lfunc_end0:
	.size	_Z17stripFromDblSpacePc, .Lfunc_end0-_Z17stripFromDblSpacePc
	.cfi_endproc

	.globl	_Z9printCopyv
	.p2align	4, 0x90
	.type	_Z9printCopyv,@function
_Z9printCopyv:                          # @_Z9printCopyv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.51, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$.Lstr.8, %edi
	callq	puts
	movl	$.Lstr.9, %edi
	callq	puts
	movl	$.Lstr.10, %edi
	callq	puts
	movl	$.Lstr.11, %edi
	callq	puts
	movl	$.Lstr.12, %edi
	callq	puts
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.Lstr.14, %edi
	popq	%rax
	jmp	puts                    # TAILCALL
.Lfunc_end1:
	.size	_Z9printCopyv, .Lfunc_end1-_Z9printCopyv
	.cfi_endproc

	.globl	_Z9printHelpv
	.p2align	4, 0x90
	.type	_Z9printHelpv,@function
_Z9printHelpv:                          # @_Z9printHelpv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1:
	.cfi_def_cfa_offset 16
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.Lstr.16, %edi
	callq	puts
	movl	$.Lstr.17, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.19, %edi
	callq	puts
	movl	$.Lstr.20, %edi
	callq	puts
	movl	$.Lstr.21, %edi
	callq	puts
	movl	$.Lstr.22, %edi
	callq	puts
	movl	$.Lstr.23, %edi
	callq	puts
	movl	$.Lstr.24, %edi
	callq	puts
	movl	$.Lstr.25, %edi
	callq	puts
	movl	$10, %edi
	callq	putchar
	movl	$.Lstr.26, %edi
	callq	puts
	movl	$.Lstr.27, %edi
	callq	puts
	movl	$.Lstr.28, %edi
	callq	puts
	movl	$.Lstr.29, %edi
	callq	puts
	movl	$.Lstr.30, %edi
	callq	puts
	movl	$.Lstr.31, %edi
	callq	puts
	movl	$.Lstr.32, %edi
	callq	puts
	movl	$.Lstr.33, %edi
	callq	puts
	movl	$.Lstr.34, %edi
	callq	puts
	movl	$.Lstr.35, %edi
	callq	puts
	movl	$.Lstr.36, %edi
	callq	puts
	movl	$.Lstr.37, %edi
	callq	puts
	movl	$.Lstr.38, %edi
	callq	puts
	movl	$.Lstr.39, %edi
	callq	puts
	movl	$.Lstr.40, %edi
	callq	puts
	movl	$.Lstr.41, %edi
	callq	puts
	movl	$.Lstr.42, %edi
	callq	puts
	movl	$.Lstr.43, %edi
	callq	puts
	movl	$.Lstr.44, %edi
	callq	puts
	movl	$.Lstr.45, %edi
	callq	puts
	movl	$.Lstr.46, %edi
	callq	puts
	movl	$.Lstr.47, %edi
	callq	puts
	movl	$.Lstr.48, %edi
	callq	puts
	movl	$.Lstr.49, %edi
	callq	puts
	movl	$.Lstr.50, %edi
	callq	puts
	movl	$10, %edi
	popq	%rax
	jmp	putchar                 # TAILCALL
.Lfunc_end2:
	.size	_Z9printHelpv, .Lfunc_end2-_Z9printHelpv
	.cfi_endproc

	.globl	_Z10parseCordsPc
	.p2align	4, 0x90
	.type	_Z10parseCordsPc,@function
_Z10parseCordsPc:                       # @_Z10parseCordsPc
	.cfi_startproc
# BB#0:
	movsbl	(%rdi), %eax
	movl	%eax, %ecx
	andb	$-33, %cl
	addb	$-65, %cl
	cmpb	$8, %cl
	ja	.LBB3_4
# BB#1:
	movsbl	1(%rdi), %esi
	movl	%esi, %ecx
	addb	$-49, %cl
	cmpb	$8, %cl
	ja	.LBB3_4
# BB#2:
	movl	%eax, %edx
	addb	$-97, %dl
	movl	$-97, %ecx
	cmpb	$9, %dl
	jb	.LBB3_5
# BB#3:
	movl	%eax, %edx
	addb	$-65, %dl
	movl	$-65, %ecx
	cmpb	$8, %dl
	ja	.LBB3_4
.LBB3_5:
	leal	1(%rax,%rcx), %edi
	addl	$-48, %esi
	jmp	_Z16getHexxagonIndexii  # TAILCALL
.LBB3_4:
	movl	$-1, %eax
	retq
.Lfunc_end3:
	.size	_Z10parseCordsPc, .Lfunc_end3-_Z10parseCordsPc
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movl	$.Lstr.51, %edi
	callq	puts
	movl	$.Lstr.52, %edi
	callq	puts
	movl	$.Lstr.53, %edi
	callq	puts
	movl	$.Lstr.54, %edi
	callq	puts
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGameC1Ev
.Ltmp1:
# BB#1:
	movq	16(%rbx), %rdi
	movl	(%rbx), %esi
	callq	_ZN13HexxagonBoard16displayBoardTextEi
	movabsq	$2334392246976603502, %rax # imm = 0x20656D616777656E
	movq	%rax, 8(%rsp)
	movb	$0, 18(%rsp)
	movw	$17219, 16(%rsp)        # imm = 0x4343
	leaq	8(%rsp), %rdi
	callq	strlen
	testl	%eax, %eax
	je	.LBB4_4
# BB#2:
	shlq	$32, %rax
	movabsq	$-4294967296, %rcx      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rcx
	sarq	$32, %rcx
	cmpb	$32, 8(%rsp,%rcx)
	jne	.LBB4_4
# BB#3:
	movb	$0, 8(%rsp,%rcx)
	cmpb	$0, 8(%rsp)
	je	.LBB4_140
.LBB4_4:                                # %.thread
	leaq	8(%rsp), %rsi
	movl	$.L.str.56, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_140
# BB#5:
	leaq	8(%rsp), %rsi
	movl	$.L.str.57, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_140
# BB#6:
	leaq	8(%rsp), %rsi
	movl	$.L.str.58, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_7
# BB#10:
	leaq	8(%rsp), %rsi
	movl	$.L.str.59, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_11
# BB#12:
	leaq	8(%rsp), %rsi
	movl	$.L.str.60, %edi
	movl	$6, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_13
# BB#16:
	leaq	8(%rsp), %rsi
	movl	$.L.str.62, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_17
# BB#19:
	leaq	8(%rsp), %rsi
	movl	$.L.str.64, %edi
	movl	$5, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_20
# BB#28:
	leaq	8(%rsp), %rsi
	movl	$.L.str.66, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_29
# BB#30:
	leaq	8(%rsp), %rsi
	movl	$.L.str.68, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_31
# BB#32:
	leaq	8(%rsp), %rsi
	movl	$.L.str.69, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_33
# BB#35:
	leaq	8(%rsp), %rsi
	movl	$.L.str.71, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_36
# BB#38:
	leaq	8(%rsp), %rsi
	movl	$.L.str.73, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_39
# BB#40:
	leaq	8(%rsp), %rsi
	movl	$.L.str.74, %edi
	movl	$5, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_41
# BB#48:
	leaq	8(%rsp), %rsi
	movl	$.L.str.79, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_49
# BB#50:
	leaq	8(%rsp), %rsi
	movl	$.L.str.81, %edi
	movl	$5, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_51
# BB#56:
	leaq	8(%rsp), %rsi
	movl	$.L.str.85, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_57
# BB#58:
	leaq	8(%rsp), %rsi
	movl	$.L.str.87, %edi
	movl	$8, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_59
# BB#87:
	leaq	8(%rsp), %rsi
	movl	$.L.str.90, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_88
# BB#89:
	leaq	8(%rsp), %r15
	movl	$.L.str.91, %edi
	movl	$5, %edx
	movq	%r15, %rsi
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_91
# BB#90:
	leaq	8(%rsp), %rsi
	movl	$.L.str.92, %edi
	movl	$2, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_91
# BB#121:
	leaq	8(%rsp), %rsi
	movl	$.L.str.96, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_123
# BB#122:
	leaq	8(%rsp), %rsi
	movl	$.L.str.97, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_123
# BB#124:
	leaq	8(%rsp), %rsi
	movl	$.L.str.98, %edi
	movl	$6, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_126
# BB#125:
	leaq	8(%rsp), %rsi
	movl	$.L.str.99, %edi
	movl	$2, %edx
	callq	strncasecmp
	testl	%eax, %eax
	je	.LBB4_126
# BB#135:
	leaq	8(%rsp), %rsi
	movl	$.L.str.102, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_137
# BB#136:
	leaq	8(%rsp), %rsi
	movl	$.L.str.103, %edi
	callq	strcasecmp
	testl	%eax, %eax
	je	.LBB4_137
# BB#138:
	movl	$.Lstr.81, %edi
	jmp	.LBB4_139
.LBB4_7:
	callq	_Z9printHelpv
	jmp	.LBB4_140
.LBB4_11:
	callq	_Z9printCopyv
	jmp	.LBB4_140
.LBB4_13:
	movsbl	14(%rsp), %eax
	addl	$-48, %eax
	cmpl	$9, %eax
	ja	.LBB4_15
# BB#14:
	cmpb	$0, 15(%rsp)
	je	.LBB4_140
.LBB4_15:
	movl	$.Lstr.55, %edi
	jmp	.LBB4_139
.LBB4_17:
	movl	$.L.str.63, %edi
	movl	$4, %esi
	jmp	.LBB4_18
.LBB4_20:                               # %.preheader292.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_21:                               # %.preheader292
                                        # =>This Inner Loop Header: Depth=1
	movsbl	13(%rsp,%rax), %ecx
	leal	-48(%rcx), %edx
	incq	%rax
	cmpl	$10, %edx
	jb	.LBB4_21
# BB#22:
	cmpq	$1, %rax
	je	.LBB4_26
# BB#23:
	testb	%cl, %cl
	je	.LBB4_27
# BB#24:
	cmpb	$32, %cl
	jne	.LBB4_26
# BB#25:
	cmpb	$0, 13(%rsp,%rax)
	je	.LBB4_27
.LBB4_26:
	movl	$.Lstr.56, %edi
.LBB4_139:                              # %.loopexit
	callq	puts
	jmp	.LBB4_140
.LBB4_29:
	movl	$.L.str.67, %edi
	movl	$12, %esi
.LBB4_18:                               # %.loopexit
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB4_140
.LBB4_33:
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame4prevEv
	testl	%eax, %eax
	je	.LBB4_31
# BB#34:
	movl	$.Lstr.57, %edi
	jmp	.LBB4_139
.LBB4_27:
	leaq	13(%rsp), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	jmp	.LBB4_140
.LBB4_36:
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame4nextEv
	testl	%eax, %eax
	je	.LBB4_31
# BB#37:
	movl	$.Lstr.58, %edi
	jmp	.LBB4_139
.LBB4_39:
	movl	$4, %esi
	xorl	%edx, %edx
	movl	$12000, %ecx            # imm = 0x2EE0
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame12computerMoveEiPFvvEi
.LBB4_107:                              # %.loopexit
	movq	16(%rbx), %rdi
	movl	(%rbx), %esi
	callq	_ZN13HexxagonBoard16displayBoardTextEi
.LBB4_78:
	movl	$4, %esi
	xorl	%edx, %edx
	movl	$12000, %ecx            # imm = 0x2EE0
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame12computerMoveEiPFvvEi
.LBB4_31:
	movq	16(%rbx), %rdi
	movl	(%rbx), %esi
	callq	_ZN13HexxagonBoard16displayBoardTextEi
.LBB4_140:                              # %.loopexit
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB4_41:
	leaq	13(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame8loadGameEPc
	cmpl	$-3, %eax
	je	.LBB4_46
# BB#42:
	cmpl	$-1, %eax
	je	.LBB4_45
# BB#43:
	testl	%eax, %eax
	jne	.LBB4_47
# BB#44:
	movl	$.Lstr.62, %edi
	callq	puts
	jmp	.LBB4_31
.LBB4_49:
	movl	$.Lstr.63, %edi
	jmp	.LBB4_139
.LBB4_51:
	leaq	13(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame8saveGameEPc
	cmpl	$-1, %eax
	je	.LBB4_54
# BB#52:
	testl	%eax, %eax
	jne	.LBB4_55
# BB#53:
	movl	$.Lstr.66, %edi
	jmp	.LBB4_139
.LBB4_46:
	movl	$.Lstr.60, %edi
	jmp	.LBB4_139
.LBB4_45:
	movl	$.Lstr.61, %edi
	jmp	.LBB4_139
.LBB4_47:
	movl	$.Lstr.59, %edi
	jmp	.LBB4_139
.LBB4_57:
	movl	$.Lstr.67, %edi
	jmp	.LBB4_139
.LBB4_59:
	movb	16(%rsp), %al
	addb	$-67, %al
	cmpb	$37, %al
	ja	.LBB4_88
# BB#60:
	movzbl	%al, %eax
	movabsq	$141733920801, %rcx     # imm = 0x2100000021
	btq	%rax, %rcx
	jae	.LBB4_88
# BB#61:
	movb	17(%rsp), %al
	addb	$-67, %al
	cmpb	$37, %al
	ja	.LBB4_88
# BB#62:
	movzbl	%al, %eax
	movabsq	$141733920769, %rcx     # imm = 0x2100000001
	btq	%rax, %rcx
	jae	.LBB4_88
# BB#63:
	movb	18(%rsp), %al
	testb	%al, %al
	je	.LBB4_66
# BB#64:
	cmpb	$32, %al
	jne	.LBB4_88
# BB#65:
	cmpb	$0, 19(%rsp)
	je	.LBB4_66
.LBB4_88:
	movl	$.Lstr.70, %edi
	jmp	.LBB4_139
.LBB4_54:
	movl	$.Lstr.65, %edi
	jmp	.LBB4_139
.LBB4_55:
	movl	$.Lstr.64, %edi
	jmp	.LBB4_139
.LBB4_91:
	movl	$.L.str.92, %edi
	movl	$2, %edx
	movq	%r15, %rsi
	callq	strncasecmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leaq	2(%rcx,%rcx,2), %r14
	orq	%r15, %r14
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r15
	cmpl	$5, %r15d
	jne	.LBB4_108
# BB#92:
	movsbl	(%r14), %ecx
	movl	%ecx, %edx
	andb	$-33, %dl
	addb	$-65, %dl
	movb	$-1, %al
	cmpb	$8, %dl
	ja	.LBB4_97
# BB#93:
	movsbl	1(%r14), %esi
	movl	%esi, %edx
	addb	$-49, %dl
	cmpb	$8, %dl
	ja	.LBB4_97
# BB#94:
	movl	%ecx, %edx
	addb	$-97, %dl
	movl	$-97, %edi
	cmpb	$9, %dl
	jb	.LBB4_96
# BB#95:
	movl	%ecx, %edx
	addb	$-65, %dl
	movl	$-65, %edi
	cmpb	$8, %dl
	ja	.LBB4_97
.LBB4_96:
	leal	1(%rcx,%rdi), %edi
	addl	$-48, %esi
	callq	_Z16getHexxagonIndexii
.LBB4_97:                               # %_Z10parseCordsPc.exit
	movb	%al, 24(%rsp)
	movsbl	3(%r14), %ecx
	movl	%ecx, %edx
	andb	$-33, %dl
	addb	$-65, %dl
	movl	$-1, %eax
	cmpb	$8, %dl
	ja	.LBB4_103
# BB#98:
	movsbl	4(%r14), %esi
	jmp	.LBB4_99
.LBB4_108:
	cmpl	$4, %r15d
	jne	.LBB4_120
# BB#109:
	movsbl	(%r14), %ecx
	movl	%ecx, %edx
	andb	$-33, %dl
	addb	$-65, %dl
	movb	$-1, %al
	cmpb	$8, %dl
	ja	.LBB4_114
# BB#110:
	movsbl	1(%r14), %esi
	movl	%esi, %edx
	addb	$-49, %dl
	cmpb	$8, %dl
	ja	.LBB4_114
# BB#111:
	movl	%ecx, %edx
	addb	$-97, %dl
	movl	$-97, %edi
	cmpb	$9, %dl
	jb	.LBB4_113
# BB#112:
	movl	%ecx, %edx
	addb	$-65, %dl
	movl	$-65, %edi
	cmpb	$8, %dl
	ja	.LBB4_114
.LBB4_113:
	leal	1(%rcx,%rdi), %edi
	addl	$-48, %esi
	callq	_Z16getHexxagonIndexii
.LBB4_114:                              # %_Z10parseCordsPc.exit245
	movb	%al, 24(%rsp)
	movsbl	2(%r14), %ecx
	movl	%ecx, %edx
	andb	$-33, %dl
	addb	$-65, %dl
	movl	$-1, %eax
	cmpb	$8, %dl
	ja	.LBB4_103
# BB#115:
	movsbl	3(%r14), %esi
.LBB4_99:
	movl	%esi, %edx
	addb	$-49, %dl
	cmpb	$8, %dl
	ja	.LBB4_103
# BB#100:
	movl	%ecx, %edx
	addb	$-97, %dl
	movl	$-97, %edi
	cmpb	$9, %dl
	jb	.LBB4_102
# BB#101:
	movl	%ecx, %edx
	addb	$-65, %dl
	movl	$-65, %edi
	cmpb	$8, %dl
	ja	.LBB4_103
.LBB4_102:
	leal	1(%rcx,%rdi), %edi
	addl	$-48, %esi
	callq	_Z16getHexxagonIndexii
.LBB4_103:                              # %_Z10parseCordsPc.exit251
	movb	%al, 25(%rsp)
	cmpb	$-1, 24(%rsp)
	je	.LBB4_116
# BB#104:                               # %_Z10parseCordsPc.exit251
	cmpb	$-1, %al
	je	.LBB4_116
.LBB4_105:
	movq	16(%rbx), %rdi
	leaq	24(%rsp), %rsi
	callq	_ZN13HexxagonBoard11isMoveValidER12HexxagonMove
	testl	%eax, %eax
	je	.LBB4_134
# BB#106:
	leaq	24(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame9applyMoveER12HexxagonMove
	jmp	.LBB4_107
.LBB4_66:
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame11destroyRestEv
.Ltmp4:
# BB#67:                                # %.noexc
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_69
# BB#68:
	callq	_ZdlPv
.LBB4_69:
	movq	%rbx, %rdi
	callq	_ZdlPv
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp6:
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGameC1Ev
.Ltmp7:
# BB#70:
	movq	16(%rbx), %rdi
	movl	(%rbx), %esi
	callq	_ZN13HexxagonBoard16displayBoardTextEi
	movb	16(%rsp), %sil
	movl	%esi, %ecx
	addb	$-67, %cl
	cmpb	$37, %cl
	ja	.LBB4_86
# BB#71:
	movzbl	%cl, %edx
	jmpq	*.LJTI4_0(,%rdx,8)
.LBB4_77:
	movb	17(%rsp), %al
	orb	$32, %al
	cmpb	$104, %al
	je	.LBB4_78
# BB#79:
	cmpb	$37, %cl
	ja	.LBB4_86
# BB#80:
	jmpq	*.LJTI4_2(,%rdx,8)
.LBB4_116:
	cmpl	$5, %r15d
	jne	.LBB4_119
# BB#117:
	cmpb	$32, 2(%r14)
	je	.LBB4_118
	jmp	.LBB4_123
.LBB4_120:                              # %.thread265
	movb	$-1, 24(%rsp)
	movl	$.Lstr.76, %edi
	jmp	.LBB4_139
.LBB4_119:
	cmpl	$4, %r15d
	jne	.LBB4_123
.LBB4_118:
	movl	$.Lstr.74, %edi
	jmp	.LBB4_139
.LBB4_123:
	movl	$.Lstr.76, %edi
	jmp	.LBB4_139
.LBB4_74:
	movb	17(%rsp), %al
	orb	$32, %al
	cmpb	$99, %al
	je	.LBB4_140
# BB#75:                                # %thread-pre-split
	cmpb	$37, %cl
	ja	.LBB4_86
# BB#76:                                # %thread-pre-split
	jmpq	*.LJTI4_1(,%rdx,8)
.LBB4_81:
	movb	17(%rsp), %al
	orb	$32, %al
	cmpb	$104, %al
	je	.LBB4_140
# BB#82:                                # %thread-pre-split260
	orb	$32, %sil
	cmpb	$99, %sil
	jne	.LBB4_86
.LBB4_83:
	movb	17(%rsp), %al
	orb	$32, %al
	cmpb	$99, %al
	je	.LBB4_84
.LBB4_86:
	movl	$.Lstr.69, %edi
	jmp	.LBB4_139
.LBB4_85:                               # %.lr.ph
                                        #   in Loop: Header=BB4_84 Depth=1
	movl	$4, %esi
	xorl	%edx, %edx
	movl	$12000, %ecx            # imm = 0x2EE0
	movq	%rbx, %rdi
	callq	_ZN12HexxagonGame12computerMoveEiPFvvEi
	movq	16(%rbx), %rdi
	movl	(%rbx), %esi
	callq	_ZN13HexxagonBoard16displayBoardTextEi
.LBB4_84:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rdi
	callq	_ZN13HexxagonBoard9endOfGameEv
	testl	%eax, %eax
	je	.LBB4_85
	jmp	.LBB4_140
.LBB4_126:
	leaq	8(%rsp), %rsi
	movl	$.L.str.99, %edi
	movl	$2, %edx
	callq	strncasecmp
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	leaq	10(%rsp,%rcx,4), %r14
	movq	%r14, %rdi
	callq	strlen
	cmpq	$2, %rax
	jne	.LBB4_137
# BB#127:
	movsbl	(%r14), %eax
	movl	%eax, %ecx
	andb	$-33, %cl
	addb	$-65, %cl
	cmpb	$8, %cl
	ja	.LBB4_131
# BB#128:
	movsbl	1(%r14), %esi
	movl	%esi, %ecx
	addb	$-49, %cl
	cmpb	$8, %cl
	ja	.LBB4_131
# BB#129:
	movl	%eax, %edx
	addb	$-97, %dl
	movl	$-97, %ecx
	cmpb	$9, %dl
	jb	.LBB4_133
# BB#130:
	movl	%eax, %edx
	addb	$-65, %dl
	movl	$-65, %ecx
	cmpb	$9, %dl
	jae	.LBB4_131
.LBB4_133:                              # %_Z10parseCordsPc.exit257
	leal	1(%rax,%rcx), %edi
	addl	$-48, %esi
	callq	_Z16getHexxagonIndexii
	movb	%al, 25(%rsp)
	movb	%al, 24(%rsp)
	cmpb	$-1, %al
	jne	.LBB4_105
	jmp	.LBB4_132
.LBB4_134:
	movl	$.Lstr.79, %edi
	jmp	.LBB4_139
.LBB4_137:
	movl	$.Lstr.80, %edi
	jmp	.LBB4_139
.LBB4_131:                              # %_Z10parseCordsPc.exit257.thread
	movw	$-1, 24(%rsp)
.LBB4_132:
	movl	$.Lstr.78, %edi
	jmp	.LBB4_139
.LBB4_73:
.Ltmp8:
	jmp	.LBB4_9
.LBB4_72:
.Ltmp5:
	jmp	.LBB4_9
.LBB4_8:
.Ltmp2:
.LBB4_9:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	main, .Lfunc_end4-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_77
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_74
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_77
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_74
.LJTI4_1:
	.quad	.LBB4_77
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_81
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_77
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_81
.LJTI4_2:
	.quad	.LBB4_83
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_81
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_83
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_86
	.quad	.LBB4_81
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp6-.Ltmp4           #   Call between .Ltmp4 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Lfunc_end4-.Ltmp7      #   Call between .Ltmp7 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L_ZZ4mainE3tmp,@object # @_ZZ4mainE3tmp
	.section	.rodata.str1.1,"aMS",@progbits,1
.L_ZZ4mainE3tmp:
	.asciz	"newgame CC"
	.size	.L_ZZ4mainE3tmp, 11

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"quit"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"exit"
	.size	.L.str.57, 5

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"help"
	.size	.L.str.58, 5

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"copyright"
	.size	.L.str.59, 10

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"level "
	.size	.L.str.60, 7

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"level"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"The search level is set to: %i.\n"
	.size	.L.str.63, 33

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"time "
	.size	.L.str.64, 6

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"time"
	.size	.L.str.66, 5

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"The search time is set to %i sec.\n"
	.size	.L.str.67, 35

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"board"
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"undo"
	.size	.L.str.69, 5

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"redo"
	.size	.L.str.71, 5

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"hint"
	.size	.L.str.73, 5

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"load "
	.size	.L.str.74, 6

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"load"
	.size	.L.str.79, 5

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"save "
	.size	.L.str.81, 6

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"save"
	.size	.L.str.85, 5

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"newgame "
	.size	.L.str.87, 9

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"newgame"
	.size	.L.str.90, 8

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"jump "
	.size	.L.str.91, 6

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"j "
	.size	.L.str.92, 3

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"jump"
	.size	.L.str.96, 5

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"j"
	.size	.L.str.97, 2

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"clone "
	.size	.L.str.98, 7

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"c "
	.size	.L.str.99, 3

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"clone"
	.size	.L.str.102, 6

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"c"
	.size	.L.str.103, 2

	.type	.Lstr.1,@object         # @str.1
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.1:
	.asciz	"Copyright (C) 2001 Erik Jonsson.\n"
	.size	.Lstr.1, 34

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"The pieces was drawn by Stefan P\345hlson.\n"
	.size	.Lstr.2, 41

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"This program is free software; you can redistribute it and/or"
	.size	.Lstr.3, 62

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"modify it under the terms of the GNU General Public License"
	.size	.Lstr.4, 60

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"as published by the Free Software Foundation; either version 2"
	.size	.Lstr.5, 63

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"of the License, or (at your option) any later version.\n"
	.size	.Lstr.6, 56

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"This program is distributed in the hope that it will be useful,"
	.size	.Lstr.7, 64

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"but WITHOUT ANY WARRANTY; without even the implied warranty of"
	.size	.Lstr.8, 63

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the"
	.size	.Lstr.9, 62

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"GNU General Public License for more details.\n"
	.size	.Lstr.10, 46

	.type	.Lstr.11,@object        # @str.11
	.p2align	4
.Lstr.11:
	.asciz	"You should have received a copy of the GNU General Public License"
	.size	.Lstr.11, 66

	.type	.Lstr.12,@object        # @str.12
	.p2align	4
.Lstr.12:
	.asciz	"along with this program; if not, write to the Free Software"
	.size	.Lstr.12, 60

	.type	.Lstr.13,@object        # @str.13
	.p2align	4
.Lstr.13:
	.asciz	"Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.\n"
	.size	.Lstr.13, 77

	.type	.Lstr.14,@object        # @str.14
	.p2align	4
.Lstr.14:
	.asciz	"Email erik@nesqi.homeip.net\n"
	.size	.Lstr.14, 29

	.type	.Lstr.15,@object        # @str.15
	.p2align	4
.Lstr.15:
	.asciz	"        __                              "
	.size	.Lstr.15, 41

	.type	.Lstr.16,@object        # @str.16
	.p2align	4
.Lstr.16:
	.asciz	"     __/  \\__                          "
	.size	.Lstr.16, 40

	.type	.Lstr.17,@object        # @str.17
	.p2align	4
.Lstr.17:
	.asciz	"  __/  \\__/  \\__      Hexxagon v0.3.1 "
	.size	.Lstr.17, 39

	.type	.Lstr.18,@object        # @str.18
	.p2align	4
.Lstr.18:
	.asciz	" /  \\__/  \\__/  \\     Copyright 2001 "
	.size	.Lstr.18, 38

	.type	.Lstr.19,@object        # @str.19
	.p2align	4
.Lstr.19:
	.asciz	" \\__/  \\__/  \\__/     Erik Jonsson   "
	.size	.Lstr.19, 38

	.type	.Lstr.20,@object        # @str.20
	.p2align	4
.Lstr.20:
	.asciz	" /  \\__/  \\__/  \\                    "
	.size	.Lstr.20, 38

	.type	.Lstr.21,@object        # @str.21
	.p2align	4
.Lstr.21:
	.asciz	" \\__/  \\__/  \\__/                    "
	.size	.Lstr.21, 38

	.type	.Lstr.22,@object        # @str.22
	.p2align	4
.Lstr.22:
	.asciz	"    \\__/  \\__/                        "
	.size	.Lstr.22, 39

	.type	.Lstr.23,@object        # @str.23
	.p2align	4
.Lstr.23:
	.asciz	"       \\__/                            \n"
	.size	.Lstr.23, 41

	.type	.Lstr.24,@object        # @str.24
	.p2align	4
.Lstr.24:
	.asciz	" erik@nesqi.homeip.net"
	.size	.Lstr.24, 23

	.type	.Lstr.25,@object        # @str.25
	.p2align	4
.Lstr.25:
	.asciz	" http://nesqi.homeip.net/hexxagon"
	.size	.Lstr.25, 34

	.type	.Lstr.26,@object        # @str.26
	.p2align	4
.Lstr.26:
	.asciz	" Common commands:"
	.size	.Lstr.26, 18

	.type	.Lstr.27,@object        # @str.27
	.p2align	4
.Lstr.27:
	.asciz	"   quit                  : Quit the program."
	.size	.Lstr.27, 45

	.type	.Lstr.28,@object        # @str.28
	.p2align	4
.Lstr.28:
	.asciz	"   help                  : Print this help."
	.size	.Lstr.28, 44

	.type	.Lstr.29,@object        # @str.29
	.p2align	4
.Lstr.29:
	.asciz	"   copyright             : Prints copyright information.\n"
	.size	.Lstr.29, 58

	.type	.Lstr.30,@object        # @str.30
	.p2align	4
.Lstr.30:
	.asciz	" Play related commands:"
	.size	.Lstr.30, 24

	.type	.Lstr.31,@object        # @str.31
	.p2align	4
.Lstr.31:
	.asciz	"   jump  [FROM][TO]      : Jump from eg. A4 to B4."
	.size	.Lstr.31, 51

	.type	.Lstr.32,@object        # @str.32
	.p2align	4
.Lstr.32:
	.asciz	"   j     [FROM][TO]      : Same as jump."
	.size	.Lstr.32, 41

	.type	.Lstr.33,@object        # @str.33
	.p2align	4
.Lstr.33:
	.asciz	"   clone [TO]            : Clone to a position."
	.size	.Lstr.33, 48

	.type	.Lstr.34,@object        # @str.34
	.p2align	4
.Lstr.34:
	.asciz	"   c     [TO]            : Same as clone."
	.size	.Lstr.34, 42

	.type	.Lstr.35,@object        # @str.35
	.p2align	4
.Lstr.35:
	.asciz	"   hint                  : Let the computer move for you."
	.size	.Lstr.35, 58

	.type	.Lstr.36,@object        # @str.36
	.p2align	4
.Lstr.36:
	.asciz	"   board                 : Display the current playboard."
	.size	.Lstr.36, 58

	.type	.Lstr.37,@object        # @str.37
	.p2align	4
.Lstr.37:
	.asciz	"   undo                  : Go gack one half move. (Undo)"
	.size	.Lstr.37, 57

	.type	.Lstr.38,@object        # @str.38
	.p2align	4
.Lstr.38:
	.asciz	"   redo                  : Goto the next half move. (If you have used \"undo\".)\n"
	.size	.Lstr.38, 80

	.type	.Lstr.39,@object        # @str.39
	.p2align	4
.Lstr.39:
	.asciz	" Game related commands:"
	.size	.Lstr.39, 24

	.type	.Lstr.40,@object        # @str.40
	.p2align	4
.Lstr.40:
	.asciz	"   time [seconds]        : Max time for the computer to think."
	.size	.Lstr.40, 63

	.type	.Lstr.41,@object        # @str.41
	.p2align	4
.Lstr.41:
	.asciz	"   level [level]         : Max search depth for the computer the use."
	.size	.Lstr.41, 70

	.type	.Lstr.42,@object        # @str.42
	.p2align	4
.Lstr.42:
	.asciz	"   load [filename]       : Load a game from a save-file."
	.size	.Lstr.42, 57

	.type	.Lstr.43,@object        # @str.43
	.p2align	4
.Lstr.43:
	.asciz	"   save [filename]       : Save a game (with history) to a file."
	.size	.Lstr.43, 65

	.type	.Lstr.44,@object        # @str.44
	.p2align	4
.Lstr.44:
	.asciz	"   newgame [CC/HC/CH/HH] : Start a new game:"
	.size	.Lstr.44, 45

	.type	.Lstr.45,@object        # @str.45
	.p2align	4
.Lstr.45:
	.asciz	"                           HC for human against computer."
	.size	.Lstr.45, 58

	.type	.Lstr.46,@object        # @str.46
	.p2align	4
.Lstr.46:
	.asciz	"                           CH for computer against human."
	.size	.Lstr.46, 58

	.type	.Lstr.47,@object        # @str.47
	.p2align	4
.Lstr.47:
	.asciz	"                              Same as HC but computer starts."
	.size	.Lstr.47, 62

	.type	.Lstr.48,@object        # @str.48
	.p2align	4
.Lstr.48:
	.asciz	"                           CC for computer against computer."
	.size	.Lstr.48, 61

	.type	.Lstr.49,@object        # @str.49
	.p2align	4
.Lstr.49:
	.asciz	"                           HH for human against human.\n"
	.size	.Lstr.49, 56

	.type	.Lstr.50,@object        # @str.50
	.p2align	4
.Lstr.50:
	.asciz	" If you just hit enter your last command will be repeated."
	.size	.Lstr.50, 59

	.type	.Lstr.51,@object        # @str.51
	.p2align	4
.Lstr.51:
	.asciz	"Hexxagon board game."
	.size	.Lstr.51, 21

	.type	.Lstr.52,@object        # @str.52
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.52:
	.asciz	"Copyright 2001."
	.size	.Lstr.52, 16

	.type	.Lstr.53,@object        # @str.53
.Lstr.53:
	.asciz	"Erik Jonsson."
	.size	.Lstr.53, 14

	.type	.Lstr.54,@object        # @str.54
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.54:
	.asciz	"Type \"copyright\" to see the copyright notice.\n"
	.size	.Lstr.54, 47

	.type	.Lstr.55,@object        # @str.55
	.p2align	4
.Lstr.55:
	.asciz	"Invalid input to command \"level\".\nValid values are 0-9."
	.size	.Lstr.55, 56

	.type	.Lstr.56,@object        # @str.56
	.p2align	4
.Lstr.56:
	.asciz	"Syntax: time [number]"
	.size	.Lstr.56, 22

	.type	.Lstr.57,@object        # @str.57
	.p2align	4
.Lstr.57:
	.asciz	"No more moves left to undo!"
	.size	.Lstr.57, 28

	.type	.Lstr.58,@object        # @str.58
	.p2align	4
.Lstr.58:
	.asciz	"There is no move to redo!"
	.size	.Lstr.58, 26

	.type	.Lstr.59,@object        # @str.59
	.p2align	4
.Lstr.59:
	.asciz	"Error reading from load-file!"
	.size	.Lstr.59, 30

	.type	.Lstr.60,@object        # @str.60
	.p2align	4
.Lstr.60:
	.asciz	"Unknown file format."
	.size	.Lstr.60, 21

	.type	.Lstr.61,@object        # @str.61
	.p2align	4
.Lstr.61:
	.asciz	"Error opening load-file!"
	.size	.Lstr.61, 25

	.type	.Lstr.62,@object        # @str.62
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.62:
	.asciz	"Game loaded."
	.size	.Lstr.62, 13

	.type	.Lstr.63,@object        # @str.63
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.63:
	.asciz	"The load command needs a filename as argument."
	.size	.Lstr.63, 47

	.type	.Lstr.64,@object        # @str.64
	.p2align	4
.Lstr.64:
	.asciz	"Error writing to save-file!"
	.size	.Lstr.64, 28

	.type	.Lstr.65,@object        # @str.65
	.p2align	4
.Lstr.65:
	.asciz	"Error opening save-file!"
	.size	.Lstr.65, 25

	.type	.Lstr.66,@object        # @str.66
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.66:
	.asciz	"Game saved."
	.size	.Lstr.66, 12

	.type	.Lstr.67,@object        # @str.67
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.67:
	.asciz	"The save command needs a filename as argument."
	.size	.Lstr.67, 47

	.type	.Lstr.69,@object        # @str.69
	.p2align	4
.Lstr.69:
	.asciz	"You have found a bug in Hexxagon! This code should never be called!!"
	.size	.Lstr.69, 69

	.type	.Lstr.70,@object        # @str.70
	.p2align	4
.Lstr.70:
	.asciz	"Syntax: newgame HC/CH/CC/HH"
	.size	.Lstr.70, 28

	.type	.Lstr.74,@object        # @str.74
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.74:
	.asciz	"Bad cordinate."
	.size	.Lstr.74, 15

	.type	.Lstr.76,@object        # @str.76
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.76:
	.asciz	"Syntax: jump [FROM][TO]\nEg. \"jump a4b4\""
	.size	.Lstr.76, 40

	.type	.Lstr.78,@object        # @str.78
	.p2align	4
.Lstr.78:
	.asciz	"Invalid cordinate."
	.size	.Lstr.78, 19

	.type	.Lstr.79,@object        # @str.79
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.79:
	.asciz	"Illegal move!"
	.size	.Lstr.79, 14

	.type	.Lstr.80,@object        # @str.80
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.80:
	.asciz	"Syntax: clone [TO]\nEg. \"clone b4\""
	.size	.Lstr.80, 34

	.type	.Lstr.81,@object        # @str.81
	.p2align	4
.Lstr.81:
	.asciz	"Unknown command, try \"help\" for a list of commands."
	.size	.Lstr.81, 52


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
