	.text
	.file	"fft.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1060439283              # float 0.707106769
	.text
	.globl	fft_short
	.p2align	4, 0x90
	.type	fft_short,@function
fft_short:                              # @fft_short
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%esi, %r12d
	movq	%rdi, %r15
	movslq	%r12d, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	1020(%r15), %r13
	xorl	%r14d, %r14d
	movss	.LCPI0_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #     Child Loop BB0_6 Depth 2
                                        #     Child Loop BB0_3 Depth 2
	leaq	(%r14,%r14,2), %r8
	shlq	$6, %r8
	addq	$192, %r8
	cmpl	$1, %r12d
	jg	.LBB0_4
# BB#2:                                 # %.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	movq	%r13, %rdx
	movl	$32, %esi
	.p2align	4, 0x90
.LBB0_3:                                #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	rv_tbl-8(,%rsi,8), %rax
	movslq	%r8d, %r9
	addq	%rax, %r9
	movslq	%r9d, %rbp
	movswl	(%rcx,%rbp,2), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebx, %xmm0
	mulss	window_s(,%rax,4), %xmm0
	movl	$127, %ebx
	subq	%rax, %rbx
	movswl	256(%rcx,%rbp,2), %edi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edi, %xmm1
	mulss	window_s(,%rbx,4), %xmm1
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	addss	%xmm0, %xmm1
	movswl	128(%rcx,%rbp,2), %edi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edi, %xmm0
	mulss	window_s+256(,%rax,4), %xmm0
	movl	$63, %edi
	subq	%rax, %rdi
	movswl	384(%rcx,%rbp,2), %ebx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebx, %xmm3
	mulss	window_s(,%rdi,4), %xmm3
	movaps	%xmm0, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm0, %xmm3
	movaps	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, -524(%rdx)
	subss	%xmm3, %xmm1
	movss	%xmm1, -516(%rdx)
	movaps	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, -520(%rdx)
	subss	%xmm4, %xmm2
	movss	%xmm2, -512(%rdx)
	movswl	2(%rcx,%rbp,2), %edi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edi, %xmm0
	mulss	window_s+4(,%rax,4), %xmm0
	movl	$126, %edi
	subq	%rax, %rdi
	movswl	258(%rcx,%rbp,2), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebx, %xmm1
	mulss	window_s(,%rdi,4), %xmm1
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	addss	%xmm0, %xmm1
	movswl	130(%rcx,%rbp,2), %edi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edi, %xmm0
	mulss	window_s+260(,%rax,4), %xmm0
	movl	$62, %edi
	subq	%rax, %rdi
	movswl	386(%rcx,%r9,2), %eax
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%eax, %xmm3
	mulss	window_s(,%rdi,4), %xmm3
	movaps	%xmm0, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm0, %xmm3
	movaps	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, -12(%rdx)
	subss	%xmm3, %xmm1
	movss	%xmm1, -4(%rdx)
	movaps	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, -8(%rdx)
	subss	%xmm4, %xmm2
	movss	%xmm2, (%rdx)
	addq	$-16, %rdx
	decq	%rsi
	jg	.LBB0_3
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	cmpl	$2, %r12d
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	(%rax), %r11
	movq	8(%rax), %rdx
	movq	%r13, %rsi
	movl	$32, %r9d
	jne	.LBB0_5
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader331
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	rv_tbl-8(,%r9,8), %rbp
	movslq	%r8d, %r10
	addq	%rbp, %r10
	movslq	%r10d, %rax
	movswl	(%r11,%rax,2), %edi
	movswl	(%rdx,%rax,2), %ebx
	addl	%edi, %ebx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ebx, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s(,%rbp,4), %xmm2
	movl	$127, %edi
	subq	%rbp, %rdi
	movswl	256(%r11,%rax,2), %ebx
	movswl	256(%rdx,%rax,2), %ecx
	addl	%ebx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ecx, %xmm1
	mulss	%xmm5, %xmm1
	mulss	window_s(,%rdi,4), %xmm1
	movaps	%xmm2, %xmm0
	subss	%xmm1, %xmm0
	addss	%xmm2, %xmm1
	movswl	128(%r11,%rax,2), %ecx
	movswl	128(%rdx,%rax,2), %edi
	addl	%ecx, %edi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%edi, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s+256(,%rbp,4), %xmm2
	movl	$63, %ecx
	subq	%rbp, %rcx
	movswl	384(%r11,%rax,2), %edi
	movswl	384(%rdx,%rax,2), %ebx
	addl	%edi, %ebx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ebx, %xmm3
	mulss	%xmm5, %xmm3
	mulss	window_s(,%rcx,4), %xmm3
	movaps	%xmm2, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm2, %xmm3
	movaps	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, -524(%rsi)
	subss	%xmm3, %xmm1
	movss	%xmm1, -516(%rsi)
	movaps	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, -520(%rsi)
	subss	%xmm4, %xmm0
	movss	%xmm0, -512(%rsi)
	movswl	2(%r11,%rax,2), %ecx
	movswl	2(%rdx,%rax,2), %edi
	addl	%ecx, %edi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%edi, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s+4(,%rbp,4), %xmm2
	movl	$126, %ecx
	subq	%rbp, %rcx
	movswl	258(%r11,%rax,2), %edi
	movswl	258(%rdx,%rax,2), %ebx
	addl	%edi, %ebx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%ebx, %xmm1
	mulss	%xmm5, %xmm1
	mulss	window_s(,%rcx,4), %xmm1
	movaps	%xmm2, %xmm0
	subss	%xmm1, %xmm0
	addss	%xmm2, %xmm1
	movswl	130(%r11,%rax,2), %ecx
	movswl	130(%rdx,%rax,2), %eax
	addl	%ecx, %eax
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%eax, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s+260(,%rbp,4), %xmm2
	movl	$62, %eax
	subq	%rbp, %rax
	movswl	386(%r11,%r10,2), %ecx
	movswl	386(%rdx,%r10,2), %edi
	addl	%ecx, %edi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%edi, %xmm3
	mulss	%xmm5, %xmm3
	mulss	window_s(,%rax,4), %xmm3
	movaps	%xmm2, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm2, %xmm3
	movaps	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, -12(%rsi)
	subss	%xmm3, %xmm1
	movss	%xmm1, -4(%rsi)
	movaps	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, -8(%rsi)
	subss	%xmm4, %xmm0
	movss	%xmm0, (%rsi)
	addq	$-16, %rsi
	decq	%r9
	jg	.LBB0_6
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader333
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	rv_tbl-8(,%r9,8), %rbp
	movslq	%r8d, %r10
	addq	%rbp, %r10
	movslq	%r10d, %rax
	movswl	(%r11,%rax,2), %ecx
	movswl	(%rdx,%rax,2), %edi
	subl	%edi, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s(,%rbp,4), %xmm2
	movl	$127, %ecx
	subq	%rbp, %rcx
	movswl	256(%r11,%rax,2), %edi
	movswl	256(%rdx,%rax,2), %ebx
	subl	%ebx, %edi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edi, %xmm1
	mulss	%xmm5, %xmm1
	mulss	window_s(,%rcx,4), %xmm1
	movaps	%xmm2, %xmm0
	subss	%xmm1, %xmm0
	addss	%xmm2, %xmm1
	movswl	128(%r11,%rax,2), %ecx
	movswl	128(%rdx,%rax,2), %edi
	subl	%edi, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s+256(,%rbp,4), %xmm2
	movl	$63, %ecx
	subq	%rbp, %rcx
	movswl	384(%r11,%rax,2), %edi
	movswl	384(%rdx,%rax,2), %ebx
	subl	%ebx, %edi
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%edi, %xmm3
	mulss	%xmm5, %xmm3
	mulss	window_s(,%rcx,4), %xmm3
	movaps	%xmm2, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm2, %xmm3
	movaps	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, -524(%rsi)
	subss	%xmm3, %xmm1
	movss	%xmm1, -516(%rsi)
	movaps	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, -520(%rsi)
	subss	%xmm4, %xmm0
	movss	%xmm0, -512(%rsi)
	movswl	2(%r11,%rax,2), %ecx
	movswl	2(%rdx,%rax,2), %edi
	subl	%edi, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s+4(,%rbp,4), %xmm2
	movl	$126, %ecx
	subq	%rbp, %rcx
	movswl	258(%r11,%rax,2), %edi
	movswl	258(%rdx,%rax,2), %ebx
	subl	%ebx, %edi
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edi, %xmm1
	mulss	%xmm5, %xmm1
	mulss	window_s(,%rcx,4), %xmm1
	movaps	%xmm2, %xmm0
	subss	%xmm1, %xmm0
	addss	%xmm2, %xmm1
	movswl	130(%r11,%rax,2), %ecx
	movswl	130(%rdx,%rax,2), %eax
	subl	%eax, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm5, %xmm2
	mulss	window_s+260(,%rbp,4), %xmm2
	movl	$62, %eax
	subq	%rbp, %rax
	movswl	386(%r11,%r10,2), %ecx
	movswl	386(%rdx,%r10,2), %edi
	subl	%edi, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	mulss	%xmm5, %xmm3
	mulss	window_s(,%rax,4), %xmm3
	movaps	%xmm2, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm2, %xmm3
	movaps	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, -12(%rsi)
	subss	%xmm3, %xmm1
	movss	%xmm1, -4(%rsi)
	movaps	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, -8(%rsi)
	subss	%xmm4, %xmm0
	movss	%xmm0, (%rsi)
	addq	$-16, %rsi
	decq	%r9
	jg	.LBB0_5
.LBB0_7:                                # %.loopexit
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	$256, %esi              # imm = 0x100
	movq	%r15, %rdi
	callq	fht
	movss	.LCPI0_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	incq	%r14
	addq	$1024, %r15             # imm = 0x400
	addq	$1024, %r13             # imm = 0x400
	cmpq	$3, %r14
	jne	.LBB0_1
# BB#8:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	fft_short, .Lfunc_end0-fft_short
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4609047870845172685     # double 1.4142135623730951
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_1:
	.long	1065353216              # float 1
	.text
	.p2align	4, 0x90
	.type	fht,@function
fht:                                    # @fht
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
.Lcfi19:
	.cfi_offset %rbx, -56
.Lcfi20:
	.cfi_offset %r12, -48
.Lcfi21:
	.cfi_offset %r13, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%esi, -56(%rsp)         # 4-byte Spill
	movslq	%esi, %rax
	movq	%rdi, -48(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rax,4), %rax
	movl	$costab, %ecx
	movq	%rcx, -64(%rsp)         # 8-byte Spill
	movw	$4, %bp
	movsd	.LCPI1_0(%rip), %xmm9   # xmm9 = mem[0],zero
	movss	.LCPI1_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB1_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_2 Depth 2
                                        #     Child Loop BB1_5 Depth 2
                                        #       Child Loop BB1_6 Depth 3
	movswl	%bp, %ecx
	leal	(%rbp,%rbp), %edx
	leal	(%rbp,%rbp,2), %ebx
	leal	(,%rbp,4), %ebp
	movswq	%cx, %r8
	sarl	%ecx
	movl	%ecx, -52(%rsp)         # 4-byte Spill
	movslq	%ecx, %rdi
	movswq	%dx, %rsi
	movswq	%bx, %r13
	movq	%rbp, -40(%rsp)         # 8-byte Spill
	movswq	%bp, %rbx
	shlq	$2, %rbx
	shlq	$2, %rsi
	leaq	(%rsi,%rdi,4), %r10
	shlq	$2, %r13
	leaq	(%r13,%rdi,4), %rdx
	movq	%r8, %r9
	leaq	(,%r8,4), %r8
	leaq	(%r8,%rdi,4), %rcx
	leaq	(,%rdi,4), %rdi
	movq	-48(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_2:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	(%r8,%rbp), %xmm1       # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	addss	%xmm1, %xmm0
	movss	(%rsi,%rbp), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movss	(%r13,%rbp), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm3, %xmm1
	movaps	%xmm0, %xmm3
	subss	%xmm1, %xmm3
	movss	%xmm3, (%rsi,%rbp)
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rbp)
	movaps	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movss	%xmm0, (%r13,%rbp)
	addss	%xmm2, %xmm4
	movss	%xmm4, (%r8,%rbp)
	movss	(%rdi,%rbp), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movss	(%rcx,%rbp), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	addss	%xmm1, %xmm0
	movss	(%rdx,%rbp), %xmm1      # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm9, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	movss	(%r10,%rbp), %xmm3      # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm9, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movaps	%xmm0, %xmm4
	subss	%xmm3, %xmm4
	movss	%xmm4, (%r10,%rbp)
	addss	%xmm3, %xmm0
	movss	%xmm0, (%rdi,%rbp)
	movaps	%xmm2, %xmm0
	subss	%xmm1, %xmm0
	movss	%xmm0, (%rdx,%rbp)
	addss	%xmm1, %xmm2
	movss	%xmm2, (%rcx,%rbp)
	addq	%rbx, %rbp
	cmpq	%rax, %rbp
	jb	.LBB1_2
# BB#3:                                 #   in Loop: Header=BB1_1 Depth=1
	cmpl	$3, %r9d
	jl	.LBB1_8
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm10         # xmm10 = mem[0],zero,zero,zero
	leaq	(%rsi,%r9,4), %rcx
	movq	%rcx, -24(%rsp)         # 8-byte Spill
	leaq	(%r13,%r9,4), %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	shlq	$3, %r9
	movq	%r9, -8(%rsp)           # 8-byte Spill
	movw	$1, %r14w
	movq	%rsi, -16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_5:                                #   Parent Loop BB1_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_6 Depth 3
	movswq	%r14w, %rbp
	leaq	(,%rbp,4), %rcx
	movaps	%xmm10, %xmm12
	addss	%xmm12, %xmm12
	movaps	%xmm10, %xmm1
	mulss	%xmm12, %xmm1
	movaps	%xmm8, %xmm13
	subss	%xmm1, %xmm13
	mulss	%xmm11, %xmm12
	movq	%r8, %rdx
	subq	%rcx, %rdx
	movq	-24(%rsp), %r15         # 8-byte Reload
	subq	%rcx, %r15
	movq	-32(%rsp), %r12         # 8-byte Reload
	subq	%rcx, %r12
	movq	-8(%rsp), %r10          # 8-byte Reload
	subq	%rcx, %r10
	leaq	(%rsi,%rbp,4), %rdi
	leaq	(%r13,%rbp,4), %rsi
	leaq	(%r8,%rbp,4), %r9
	movq	-48(%rsp), %r11         # 8-byte Reload
	.p2align	4, 0x90
.LBB1_6:                                #   Parent Loop BB1_1 Depth=1
                                        #     Parent Loop BB1_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movss	(%r9,%r11), %xmm2       # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm6
	mulss	%xmm2, %xmm6
	movss	(%r10,%r11), %xmm1      # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm3
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm6
	mulss	%xmm13, %xmm2
	mulss	%xmm12, %xmm1
	addss	%xmm2, %xmm1
	movss	(%rcx,%r11), %xmm2      # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm14
	subss	%xmm1, %xmm14
	addss	%xmm2, %xmm1
	movss	(%rdx,%r11), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm15
	subss	%xmm6, %xmm15
	addss	%xmm3, %xmm6
	movss	(%rsi,%r11), %xmm3      # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm4
	mulss	%xmm3, %xmm4
	movss	(%r12,%r11), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm5
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm4
	mulss	%xmm13, %xmm3
	mulss	%xmm12, %xmm0
	addss	%xmm3, %xmm0
	movss	(%rdi,%r11), %xmm5      # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	subss	%xmm0, %xmm3
	addss	%xmm5, %xmm0
	movss	(%r15,%r11), %xmm7      # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm5
	subss	%xmm4, %xmm5
	addss	%xmm7, %xmm4
	movaps	%xmm10, %xmm7
	mulss	%xmm0, %xmm7
	movaps	%xmm11, %xmm2
	mulss	%xmm5, %xmm2
	subss	%xmm2, %xmm7
	mulss	%xmm11, %xmm0
	mulss	%xmm10, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm1, %xmm0
	subss	%xmm5, %xmm0
	movss	%xmm0, (%rdi,%r11)
	addss	%xmm1, %xmm5
	movss	%xmm5, (%rcx,%r11)
	movaps	%xmm15, %xmm0
	subss	%xmm7, %xmm0
	movss	%xmm0, (%r12,%r11)
	addss	%xmm15, %xmm7
	movss	%xmm7, (%r10,%r11)
	movaps	%xmm11, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm10, %xmm1
	mulss	%xmm3, %xmm1
	subss	%xmm1, %xmm0
	mulss	%xmm10, %xmm4
	mulss	%xmm11, %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm6, %xmm1
	subss	%xmm3, %xmm1
	movss	%xmm1, (%r15,%r11)
	addss	%xmm6, %xmm3
	movss	%xmm3, (%rdx,%r11)
	movaps	%xmm14, %xmm1
	subss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%r11)
	addss	%xmm14, %xmm0
	movss	%xmm0, (%r9,%r11)
	addq	%rbx, %r11
	leaq	(%r11,%rcx), %rbp
	cmpq	%rax, %rbp
	jb	.LBB1_6
# BB#7:                                 #   in Loop: Header=BB1_5 Depth=2
	movq	-64(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm10, %xmm3
	mulss	%xmm1, %xmm3
	subss	%xmm3, %xmm2
	mulss	%xmm1, %xmm11
	mulss	%xmm0, %xmm10
	addss	%xmm11, %xmm10
	incl	%r14d
	movswl	%r14w, %ecx
	cmpl	-52(%rsp), %ecx         # 4-byte Folded Reload
	movaps	%xmm2, %xmm11
	movq	-16(%rsp), %rsi         # 8-byte Reload
	jl	.LBB1_5
.LBB1_8:                                # %._crit_edge
                                        #   in Loop: Header=BB1_1 Depth=1
	addq	$8, -64(%rsp)           # 8-byte Folded Spill
	movq	-40(%rsp), %rbp         # 8-byte Reload
	cmpw	-56(%rsp), %bp          # 2-byte Folded Reload
	jl	.LBB1_1
# BB#9:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	fht, .Lfunc_end1-fht
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	1060439283              # float 0.707106769
	.text
	.globl	fft_long
	.p2align	4, 0x90
	.type	fft_long,@function
fft_long:                               # @fft_long
	.cfi_startproc
# BB#0:
	cmpl	$1, %esi
	jg	.LBB2_3
# BB#1:                                 # %.preheader
	movslq	%esi, %rax
	movq	(%rdx,%rax,8), %r9
	leaq	4092(%rdi), %rcx
	movl	$128, %r8d
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movswq	rv_tbl-2(%r8,%r8), %rsi
	movswl	(%r9,%rsi,2), %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	mulss	window(,%rsi,4), %xmm0
	movl	$511, %edx              # imm = 0x1FF
	subq	%rsi, %rdx
	movswl	1024(%r9,%rsi,2), %eax
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%eax, %xmm1
	mulss	window(,%rdx,4), %xmm1
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	addss	%xmm0, %xmm1
	movswl	512(%r9,%rsi,2), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	window+1024(,%rsi,4), %xmm0
	movl	$255, %eax
	subq	%rsi, %rax
	movswl	1536(%r9,%rsi,2), %edx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%edx, %xmm3
	mulss	window(,%rax,4), %xmm3
	movaps	%xmm0, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm0, %xmm3
	movaps	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, -2060(%rcx)
	subss	%xmm3, %xmm1
	movss	%xmm1, -2052(%rcx)
	movaps	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, -2056(%rcx)
	subss	%xmm4, %xmm2
	movss	%xmm2, -2048(%rcx)
	movswl	2(%r9,%rsi,2), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	window+4(,%rsi,4), %xmm0
	movl	$510, %eax              # imm = 0x1FE
	subq	%rsi, %rax
	movswl	1026(%r9,%rsi,2), %edx
	xorps	%xmm1, %xmm1
	cvtsi2ssl	%edx, %xmm1
	mulss	window(,%rax,4), %xmm1
	movaps	%xmm0, %xmm2
	subss	%xmm1, %xmm2
	addss	%xmm0, %xmm1
	movswl	514(%r9,%rsi,2), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	mulss	window+1028(,%rsi,4), %xmm0
	movl	$254, %eax
	subq	%rsi, %rax
	movswl	1538(%r9,%rsi,2), %edx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%edx, %xmm3
	mulss	window(,%rax,4), %xmm3
	movaps	%xmm0, %xmm4
	subss	%xmm3, %xmm4
	addss	%xmm0, %xmm3
	movaps	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, -12(%rcx)
	subss	%xmm3, %xmm1
	movss	%xmm1, -4(%rcx)
	movaps	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, -8(%rcx)
	subss	%xmm4, %xmm2
	movss	%xmm2, (%rcx)
	addq	$-16, %rcx
	decq	%r8
	jg	.LBB2_2
	jmp	.LBB2_8
.LBB2_3:
	cmpl	$2, %esi
	movq	(%rdx), %r9
	movq	8(%rdx), %r10
	jne	.LBB2_4
# BB#6:                                 # %.preheader280.preheader
	leaq	4092(%rdi), %rdx
	movl	$128, %r8d
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB2_7:                                # %.preheader280
                                        # =>This Inner Loop Header: Depth=1
	movswq	rv_tbl-2(%r8,%r8), %rsi
	movswl	(%r9,%rsi,2), %eax
	movswl	(%r10,%rsi,2), %ecx
	addl	%eax, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window(,%rsi,4), %xmm3
	movl	$511, %eax              # imm = 0x1FF
	subq	%rsi, %rax
	movswl	1024(%r9,%rsi,2), %r11d
	movswl	1024(%r10,%rsi,2), %ecx
	addl	%r11d, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm0, %xmm2
	mulss	window(,%rax,4), %xmm2
	movaps	%xmm3, %xmm1
	subss	%xmm2, %xmm1
	addss	%xmm3, %xmm2
	movswl	512(%r9,%rsi,2), %eax
	movswl	512(%r10,%rsi,2), %ecx
	addl	%eax, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window+1024(,%rsi,4), %xmm3
	movl	$255, %eax
	subq	%rsi, %rax
	movswl	1536(%r9,%rsi,2), %r11d
	movswl	1536(%r10,%rsi,2), %ecx
	addl	%r11d, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ecx, %xmm4
	mulss	%xmm0, %xmm4
	mulss	window(,%rax,4), %xmm4
	movaps	%xmm3, %xmm5
	subss	%xmm4, %xmm5
	addss	%xmm3, %xmm4
	movaps	%xmm2, %xmm3
	addss	%xmm4, %xmm3
	movss	%xmm3, -2060(%rdx)
	subss	%xmm4, %xmm2
	movss	%xmm2, -2052(%rdx)
	movaps	%xmm1, %xmm2
	addss	%xmm5, %xmm2
	movss	%xmm2, -2056(%rdx)
	subss	%xmm5, %xmm1
	movss	%xmm1, -2048(%rdx)
	movswl	2(%r9,%rsi,2), %eax
	movswl	2(%r10,%rsi,2), %ecx
	addl	%eax, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window+4(,%rsi,4), %xmm3
	movl	$510, %eax              # imm = 0x1FE
	subq	%rsi, %rax
	movswl	1026(%r9,%rsi,2), %r11d
	movswl	1026(%r10,%rsi,2), %ecx
	addl	%r11d, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm0, %xmm2
	mulss	window(,%rax,4), %xmm2
	movaps	%xmm3, %xmm1
	subss	%xmm2, %xmm1
	addss	%xmm3, %xmm2
	movswl	514(%r9,%rsi,2), %eax
	movswl	514(%r10,%rsi,2), %ecx
	addl	%eax, %ecx
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%ecx, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window+1028(,%rsi,4), %xmm3
	movl	$254, %eax
	subq	%rsi, %rax
	movswl	1538(%r9,%rsi,2), %ecx
	movswl	1538(%r10,%rsi,2), %esi
	addl	%ecx, %esi
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%esi, %xmm4
	mulss	%xmm0, %xmm4
	mulss	window(,%rax,4), %xmm4
	movaps	%xmm3, %xmm5
	subss	%xmm4, %xmm5
	addss	%xmm3, %xmm4
	movaps	%xmm2, %xmm3
	addss	%xmm4, %xmm3
	movss	%xmm3, -12(%rdx)
	subss	%xmm4, %xmm2
	movss	%xmm2, -4(%rdx)
	movaps	%xmm1, %xmm2
	addss	%xmm5, %xmm2
	movss	%xmm2, -8(%rdx)
	subss	%xmm5, %xmm1
	movss	%xmm1, (%rdx)
	addq	$-16, %rdx
	decq	%r8
	jg	.LBB2_7
	jmp	.LBB2_8
.LBB2_4:                                # %.preheader282.preheader
	leaq	4092(%rdi), %r11
	movl	$128, %r8d
	movss	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader282
                                        # =>This Inner Loop Header: Depth=1
	movswq	rv_tbl-2(%r8,%r8), %rsi
	movswl	(%r9,%rsi,2), %eax
	movswl	(%r10,%rsi,2), %ecx
	subl	%ecx, %eax
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%eax, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window(,%rsi,4), %xmm3
	movl	$511, %eax              # imm = 0x1FF
	subq	%rsi, %rax
	movswl	1024(%r9,%rsi,2), %ecx
	movswl	1024(%r10,%rsi,2), %edx
	subl	%edx, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm0, %xmm2
	mulss	window(,%rax,4), %xmm2
	movaps	%xmm3, %xmm1
	subss	%xmm2, %xmm1
	addss	%xmm3, %xmm2
	movswl	512(%r9,%rsi,2), %eax
	movswl	512(%r10,%rsi,2), %ecx
	subl	%ecx, %eax
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%eax, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window+1024(,%rsi,4), %xmm3
	movl	$255, %eax
	subq	%rsi, %rax
	movswl	1536(%r9,%rsi,2), %ecx
	movswl	1536(%r10,%rsi,2), %edx
	subl	%edx, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ecx, %xmm4
	mulss	%xmm0, %xmm4
	mulss	window(,%rax,4), %xmm4
	movaps	%xmm3, %xmm5
	subss	%xmm4, %xmm5
	addss	%xmm3, %xmm4
	movaps	%xmm2, %xmm3
	addss	%xmm4, %xmm3
	movss	%xmm3, -2060(%r11)
	subss	%xmm4, %xmm2
	movss	%xmm2, -2052(%r11)
	movaps	%xmm1, %xmm2
	addss	%xmm5, %xmm2
	movss	%xmm2, -2056(%r11)
	subss	%xmm5, %xmm1
	movss	%xmm1, -2048(%r11)
	movswl	2(%r9,%rsi,2), %eax
	movswl	2(%r10,%rsi,2), %ecx
	subl	%ecx, %eax
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%eax, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window+4(,%rsi,4), %xmm3
	movl	$510, %eax              # imm = 0x1FE
	subq	%rsi, %rax
	movswl	1026(%r9,%rsi,2), %ecx
	movswl	1026(%r10,%rsi,2), %edx
	subl	%edx, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2ssl	%ecx, %xmm2
	mulss	%xmm0, %xmm2
	mulss	window(,%rax,4), %xmm2
	movaps	%xmm3, %xmm1
	subss	%xmm2, %xmm1
	addss	%xmm3, %xmm2
	movswl	514(%r9,%rsi,2), %eax
	movswl	514(%r10,%rsi,2), %ecx
	subl	%ecx, %eax
	xorps	%xmm3, %xmm3
	cvtsi2ssl	%eax, %xmm3
	mulss	%xmm0, %xmm3
	mulss	window+1028(,%rsi,4), %xmm3
	movl	$254, %eax
	subq	%rsi, %rax
	movswl	1538(%r9,%rsi,2), %ecx
	movswl	1538(%r10,%rsi,2), %edx
	subl	%edx, %ecx
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%ecx, %xmm4
	mulss	%xmm0, %xmm4
	mulss	window(,%rax,4), %xmm4
	movaps	%xmm3, %xmm5
	subss	%xmm4, %xmm5
	addss	%xmm3, %xmm4
	movaps	%xmm2, %xmm3
	addss	%xmm4, %xmm3
	movss	%xmm3, -12(%r11)
	subss	%xmm4, %xmm2
	movss	%xmm2, -4(%r11)
	movaps	%xmm1, %xmm2
	addss	%xmm5, %xmm2
	movss	%xmm2, -8(%r11)
	subss	%xmm5, %xmm1
	movss	%xmm1, (%r11)
	addq	$-16, %r11
	decq	%r8
	jg	.LBB2_5
.LBB2_8:                                # %.loopexit
	movl	$1024, %esi             # imm = 0x400
	jmp	fht                     # TAILCALL
.Lfunc_end2:
	.size	fft_long, .Lfunc_end2-fft_long
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1064076126              # float 0.923879504
	.long	1053028118              # float 0.382683456
	.long	1065272429              # float 0.995184719
	.long	1036565814              # float 0.0980171412
.LCPI3_1:
	.long	1065348163              # float 0.999698817
	.long	1019808432              # float 0.024541229
	.long	1065352900              # float 0.999981164
	.long	1003032456              # float 0.00613588467
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_2:
	.quad	4602678819172646912     # double 0.5
.LCPI3_3:
	.quad	4618760256179416344     # double 6.2831853071795862
.LCPI3_4:
	.quad	4562146422526312448     # double 9.765625E-4
.LCPI3_5:
	.quad	4607182418800017408     # double 1
.LCPI3_6:
	.quad	4571153621781053440     # double 0.00390625
	.text
	.globl	init_fft
	.p2align	4, 0x90
	.type	init_fft,@function
init_fft:                               # @init_fft
	.cfi_startproc
# BB#0:                                 # %.preheader18.preheader29
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
.Lcfi26:
	.cfi_offset %rbx, -16
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [9.238795e-01,3.826835e-01,9.951847e-01,9.801714e-02]
	movaps	%xmm0, costab(%rip)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [9.996988e-01,2.454123e-02,9.999812e-01,6.135885e-03]
	movaps	%xmm0, costab+16(%rip)
	xorl	%ebx, %ebx
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI3_3(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB3_1:                                # %.preheader18
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	.LCPI3_4(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	movsd	.LCPI3_5(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	.LCPI3_2(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, window(,%rbx,4)
	leal	1(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	.LCPI3_2(%rip), %xmm0
	mulsd	.LCPI3_3(%rip), %xmm0
	mulsd	.LCPI3_4(%rip), %xmm0
	callq	cos
	movsd	.LCPI3_5(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movsd	.LCPI3_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movss	%xmm0, window+4(,%rbx,4)
	addq	$2, %rbx
	cmpq	$512, %rbx              # imm = 0x200
	jne	.LBB3_1
# BB#2:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addsd	%xmm2, %xmm0
	mulsd	%xmm1, %xmm0
	movsd	.LCPI3_6(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	movsd	.LCPI3_5(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	.LCPI3_2(%rip), %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, window_s(,%rbx,4)
	leal	1(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	.LCPI3_2(%rip), %xmm0
	mulsd	.LCPI3_3(%rip), %xmm0
	mulsd	.LCPI3_6(%rip), %xmm0
	callq	cos
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movsd	.LCPI3_5(%rip), %xmm1   # xmm1 = mem[0],zero
	subsd	%xmm0, %xmm1
	mulsd	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movsd	.LCPI3_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movss	%xmm0, window_s+4(,%rbx,4)
	addq	$2, %rbx
	cmpq	$128, %rbx
	jne	.LBB3_3
# BB#4:
	popq	%rbx
	retq
.Lfunc_end3:
	.size	init_fft, .Lfunc_end3-init_fft
	.cfi_endproc

	.type	rv_tbl,@object          # @rv_tbl
	.section	.rodata,"a",@progbits
	.p2align	4
rv_tbl:
	.short	0                       # 0x0
	.short	128                     # 0x80
	.short	64                      # 0x40
	.short	192                     # 0xc0
	.short	32                      # 0x20
	.short	160                     # 0xa0
	.short	96                      # 0x60
	.short	224                     # 0xe0
	.short	16                      # 0x10
	.short	144                     # 0x90
	.short	80                      # 0x50
	.short	208                     # 0xd0
	.short	48                      # 0x30
	.short	176                     # 0xb0
	.short	112                     # 0x70
	.short	240                     # 0xf0
	.short	8                       # 0x8
	.short	136                     # 0x88
	.short	72                      # 0x48
	.short	200                     # 0xc8
	.short	40                      # 0x28
	.short	168                     # 0xa8
	.short	104                     # 0x68
	.short	232                     # 0xe8
	.short	24                      # 0x18
	.short	152                     # 0x98
	.short	88                      # 0x58
	.short	216                     # 0xd8
	.short	56                      # 0x38
	.short	184                     # 0xb8
	.short	120                     # 0x78
	.short	248                     # 0xf8
	.short	4                       # 0x4
	.short	132                     # 0x84
	.short	68                      # 0x44
	.short	196                     # 0xc4
	.short	36                      # 0x24
	.short	164                     # 0xa4
	.short	100                     # 0x64
	.short	228                     # 0xe4
	.short	20                      # 0x14
	.short	148                     # 0x94
	.short	84                      # 0x54
	.short	212                     # 0xd4
	.short	52                      # 0x34
	.short	180                     # 0xb4
	.short	116                     # 0x74
	.short	244                     # 0xf4
	.short	12                      # 0xc
	.short	140                     # 0x8c
	.short	76                      # 0x4c
	.short	204                     # 0xcc
	.short	44                      # 0x2c
	.short	172                     # 0xac
	.short	108                     # 0x6c
	.short	236                     # 0xec
	.short	28                      # 0x1c
	.short	156                     # 0x9c
	.short	92                      # 0x5c
	.short	220                     # 0xdc
	.short	60                      # 0x3c
	.short	188                     # 0xbc
	.short	124                     # 0x7c
	.short	252                     # 0xfc
	.short	2                       # 0x2
	.short	130                     # 0x82
	.short	66                      # 0x42
	.short	194                     # 0xc2
	.short	34                      # 0x22
	.short	162                     # 0xa2
	.short	98                      # 0x62
	.short	226                     # 0xe2
	.short	18                      # 0x12
	.short	146                     # 0x92
	.short	82                      # 0x52
	.short	210                     # 0xd2
	.short	50                      # 0x32
	.short	178                     # 0xb2
	.short	114                     # 0x72
	.short	242                     # 0xf2
	.short	10                      # 0xa
	.short	138                     # 0x8a
	.short	74                      # 0x4a
	.short	202                     # 0xca
	.short	42                      # 0x2a
	.short	170                     # 0xaa
	.short	106                     # 0x6a
	.short	234                     # 0xea
	.short	26                      # 0x1a
	.short	154                     # 0x9a
	.short	90                      # 0x5a
	.short	218                     # 0xda
	.short	58                      # 0x3a
	.short	186                     # 0xba
	.short	122                     # 0x7a
	.short	250                     # 0xfa
	.short	6                       # 0x6
	.short	134                     # 0x86
	.short	70                      # 0x46
	.short	198                     # 0xc6
	.short	38                      # 0x26
	.short	166                     # 0xa6
	.short	102                     # 0x66
	.short	230                     # 0xe6
	.short	22                      # 0x16
	.short	150                     # 0x96
	.short	86                      # 0x56
	.short	214                     # 0xd6
	.short	54                      # 0x36
	.short	182                     # 0xb6
	.short	118                     # 0x76
	.short	246                     # 0xf6
	.short	14                      # 0xe
	.short	142                     # 0x8e
	.short	78                      # 0x4e
	.short	206                     # 0xce
	.short	46                      # 0x2e
	.short	174                     # 0xae
	.short	110                     # 0x6e
	.short	238                     # 0xee
	.short	30                      # 0x1e
	.short	158                     # 0x9e
	.short	94                      # 0x5e
	.short	222                     # 0xde
	.short	62                      # 0x3e
	.short	190                     # 0xbe
	.short	126                     # 0x7e
	.short	254                     # 0xfe
	.size	rv_tbl, 256

	.type	window_s,@object        # @window_s
	.local	window_s
	.comm	window_s,512,16
	.type	window,@object          # @window
	.local	window
	.comm	window,2048,16
	.type	costab,@object          # @costab
	.local	costab
	.comm	costab,32,16

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
