	.text
	.file	"InStreamWithCRC.bc"
	.globl	_ZN26CSequentialInStreamWithCRC4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN26CSequentialInStreamWithCRC4ReadEPvjPj,@function
_ZN26CSequentialInStreamWithCRC4ReadEPvjPj: # @_ZN26CSequentialInStreamWithCRC4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 64
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%edx, %ebp
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	12(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %r15d
	movl	12(%rsp), %edx
	addq	%rdx, 24(%rbx)
	testl	%ebp, %ebp
	je	.LBB0_3
# BB#1:
	testl	%edx, %edx
	jne	.LBB0_3
# BB#2:
	movb	$1, 36(%rbx)
.LBB0_3:
	movl	32(%rbx), %edi
	movq	%r12, %rsi
	callq	CrcUpdate
	movl	%eax, 32(%rbx)
	testq	%r14, %r14
	je	.LBB0_5
# BB#4:
	movl	12(%rsp), %eax
	movl	%eax, (%r14)
.LBB0_5:
	movl	%r15d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN26CSequentialInStreamWithCRC4ReadEPvjPj, .Lfunc_end0-_ZN26CSequentialInStreamWithCRC4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN16CInStreamWithCRC4ReadEPvjPj
	.p2align	4, 0x90
	.type	_ZN16CInStreamWithCRC4ReadEPvjPj,@function
_ZN16CInStreamWithCRC4ReadEPvjPj:       # @_ZN16CInStreamWithCRC4ReadEPvjPj
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	4(%rsp), %rcx
	callq	*40(%rax)
	movl	%eax, %r14d
	movl	4(%rsp), %edx
	addq	%rdx, 24(%rbx)
	movl	32(%rbx), %edi
	movq	%rbp, %rsi
	callq	CrcUpdate
	movl	%eax, 32(%rbx)
	testq	%r15, %r15
	je	.LBB1_2
# BB#1:
	movl	4(%rsp), %eax
	movl	%eax, (%r15)
.LBB1_2:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN16CInStreamWithCRC4ReadEPvjPj, .Lfunc_end1-_ZN16CInStreamWithCRC4ReadEPvjPj
	.cfi_endproc

	.globl	_ZN16CInStreamWithCRC4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN16CInStreamWithCRC4SeekExjPy,@function
_ZN16CInStreamWithCRC4SeekExjPy:        # @_ZN16CInStreamWithCRC4SeekExjPy
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	jne	.LBB2_2
# BB#1:
	testl	%edx, %edx
	jne	.LBB2_2
# BB#3:
	movq	$0, 24(%rdi)
	movl	$-1, 32(%rdi)
	movq	16(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	xorl	%esi, %esi
	xorl	%edx, %edx
	jmpq	*%rax                   # TAILCALL
.LBB2_2:
	movl	$-2147467259, %eax      # imm = 0x80004005
	retq
.Lfunc_end2:
	.size	_ZN16CInStreamWithCRC4SeekExjPy, .Lfunc_end2-_ZN16CInStreamWithCRC4SeekExjPy
	.cfi_endproc

	.section	.text._ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv,@function
_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv: # @_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB3_17
# BB#1:
	movb	1(%rsi), %cl
	cmpb	IID_IUnknown+1(%rip), %cl
	jne	.LBB3_17
# BB#2:
	movb	2(%rsi), %cl
	cmpb	IID_IUnknown+2(%rip), %cl
	jne	.LBB3_17
# BB#3:
	movb	3(%rsi), %cl
	cmpb	IID_IUnknown+3(%rip), %cl
	jne	.LBB3_17
# BB#4:
	movb	4(%rsi), %cl
	cmpb	IID_IUnknown+4(%rip), %cl
	jne	.LBB3_17
# BB#5:
	movb	5(%rsi), %cl
	cmpb	IID_IUnknown+5(%rip), %cl
	jne	.LBB3_17
# BB#6:
	movb	6(%rsi), %cl
	cmpb	IID_IUnknown+6(%rip), %cl
	jne	.LBB3_17
# BB#7:
	movb	7(%rsi), %cl
	cmpb	IID_IUnknown+7(%rip), %cl
	jne	.LBB3_17
# BB#8:
	movb	8(%rsi), %cl
	cmpb	IID_IUnknown+8(%rip), %cl
	jne	.LBB3_17
# BB#9:
	movb	9(%rsi), %cl
	cmpb	IID_IUnknown+9(%rip), %cl
	jne	.LBB3_17
# BB#10:
	movb	10(%rsi), %cl
	cmpb	IID_IUnknown+10(%rip), %cl
	jne	.LBB3_17
# BB#11:
	movb	11(%rsi), %cl
	cmpb	IID_IUnknown+11(%rip), %cl
	jne	.LBB3_17
# BB#12:
	movb	12(%rsi), %cl
	cmpb	IID_IUnknown+12(%rip), %cl
	jne	.LBB3_17
# BB#13:
	movb	13(%rsi), %cl
	cmpb	IID_IUnknown+13(%rip), %cl
	jne	.LBB3_17
# BB#14:
	movb	14(%rsi), %cl
	cmpb	IID_IUnknown+14(%rip), %cl
	jne	.LBB3_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %cl
	cmpb	IID_IUnknown+15(%rip), %cl
	jne	.LBB3_17
# BB#16:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB3_17:                               # %_ZeqRK4GUIDS1_.exit.thread
	popq	%rcx
	retq
.Lfunc_end3:
	.size	_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv, .Lfunc_end3-_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN26CSequentialInStreamWithCRC6AddRefEv,"axG",@progbits,_ZN26CSequentialInStreamWithCRC6AddRefEv,comdat
	.weak	_ZN26CSequentialInStreamWithCRC6AddRefEv
	.p2align	4, 0x90
	.type	_ZN26CSequentialInStreamWithCRC6AddRefEv,@function
_ZN26CSequentialInStreamWithCRC6AddRefEv: # @_ZN26CSequentialInStreamWithCRC6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end4:
	.size	_ZN26CSequentialInStreamWithCRC6AddRefEv, .Lfunc_end4-_ZN26CSequentialInStreamWithCRC6AddRefEv
	.cfi_endproc

	.section	.text._ZN26CSequentialInStreamWithCRC7ReleaseEv,"axG",@progbits,_ZN26CSequentialInStreamWithCRC7ReleaseEv,comdat
	.weak	_ZN26CSequentialInStreamWithCRC7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN26CSequentialInStreamWithCRC7ReleaseEv,@function
_ZN26CSequentialInStreamWithCRC7ReleaseEv: # @_ZN26CSequentialInStreamWithCRC7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi21:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB5_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB5_2:
	popq	%rcx
	retq
.Lfunc_end5:
	.size	_ZN26CSequentialInStreamWithCRC7ReleaseEv, .Lfunc_end5-_ZN26CSequentialInStreamWithCRC7ReleaseEv
	.cfi_endproc

	.section	.text._ZN26CSequentialInStreamWithCRCD2Ev,"axG",@progbits,_ZN26CSequentialInStreamWithCRCD2Ev,comdat
	.weak	_ZN26CSequentialInStreamWithCRCD2Ev
	.p2align	4, 0x90
	.type	_ZN26CSequentialInStreamWithCRCD2Ev,@function
_ZN26CSequentialInStreamWithCRCD2Ev:    # @_ZN26CSequentialInStreamWithCRCD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV26CSequentialInStreamWithCRC+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB6_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB6_1:                                # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	retq
.Lfunc_end6:
	.size	_ZN26CSequentialInStreamWithCRCD2Ev, .Lfunc_end6-_ZN26CSequentialInStreamWithCRCD2Ev
	.cfi_endproc

	.section	.text._ZN26CSequentialInStreamWithCRCD0Ev,"axG",@progbits,_ZN26CSequentialInStreamWithCRCD0Ev,comdat
	.weak	_ZN26CSequentialInStreamWithCRCD0Ev
	.p2align	4, 0x90
	.type	_ZN26CSequentialInStreamWithCRCD0Ev,@function
_ZN26CSequentialInStreamWithCRCD0Ev:    # @_ZN26CSequentialInStreamWithCRCD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV26CSequentialInStreamWithCRC+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp0:
	callq	*16(%rax)
.Ltmp1:
.LBB7_2:                                # %_ZN26CSequentialInStreamWithCRCD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB7_3:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN26CSequentialInStreamWithCRCD0Ev, .Lfunc_end7-_ZN26CSequentialInStreamWithCRCD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Lfunc_end7-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv,@function
_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv: # @_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB8_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB8_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB8_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB8_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB8_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB8_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB8_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB8_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB8_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB8_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB8_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB8_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB8_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB8_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB8_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB8_32
.LBB8_16:                               # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInStream(%rip), %cl
	jne	.LBB8_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IInStream+1(%rip), %cl
	jne	.LBB8_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IInStream+2(%rip), %cl
	jne	.LBB8_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IInStream+3(%rip), %cl
	jne	.LBB8_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IInStream+4(%rip), %cl
	jne	.LBB8_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IInStream+5(%rip), %cl
	jne	.LBB8_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IInStream+6(%rip), %cl
	jne	.LBB8_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IInStream+7(%rip), %cl
	jne	.LBB8_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IInStream+8(%rip), %cl
	jne	.LBB8_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IInStream+9(%rip), %cl
	jne	.LBB8_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IInStream+10(%rip), %cl
	jne	.LBB8_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IInStream+11(%rip), %cl
	jne	.LBB8_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IInStream+12(%rip), %cl
	jne	.LBB8_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IInStream+13(%rip), %cl
	jne	.LBB8_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IInStream+14(%rip), %cl
	jne	.LBB8_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IInStream+15(%rip), %cl
	jne	.LBB8_33
.LBB8_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB8_33:                               # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end8:
	.size	_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv, .Lfunc_end8-_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN16CInStreamWithCRC6AddRefEv,"axG",@progbits,_ZN16CInStreamWithCRC6AddRefEv,comdat
	.weak	_ZN16CInStreamWithCRC6AddRefEv
	.p2align	4, 0x90
	.type	_ZN16CInStreamWithCRC6AddRefEv,@function
_ZN16CInStreamWithCRC6AddRefEv:         # @_ZN16CInStreamWithCRC6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end9:
	.size	_ZN16CInStreamWithCRC6AddRefEv, .Lfunc_end9-_ZN16CInStreamWithCRC6AddRefEv
	.cfi_endproc

	.section	.text._ZN16CInStreamWithCRC7ReleaseEv,"axG",@progbits,_ZN16CInStreamWithCRC7ReleaseEv,comdat
	.weak	_ZN16CInStreamWithCRC7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN16CInStreamWithCRC7ReleaseEv,@function
_ZN16CInStreamWithCRC7ReleaseEv:        # @_ZN16CInStreamWithCRC7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi28:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB10_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB10_2:
	popq	%rcx
	retq
.Lfunc_end10:
	.size	_ZN16CInStreamWithCRC7ReleaseEv, .Lfunc_end10-_ZN16CInStreamWithCRC7ReleaseEv
	.cfi_endproc

	.section	.text._ZN16CInStreamWithCRCD2Ev,"axG",@progbits,_ZN16CInStreamWithCRCD2Ev,comdat
	.weak	_ZN16CInStreamWithCRCD2Ev
	.p2align	4, 0x90
	.type	_ZN16CInStreamWithCRCD2Ev,@function
_ZN16CInStreamWithCRCD2Ev:              # @_ZN16CInStreamWithCRCD2Ev
	.cfi_startproc
# BB#0:
	movq	$_ZTV16CInStreamWithCRC+16, (%rdi)
	movq	16(%rdi), %rdi
	testq	%rdi, %rdi
	je	.LBB11_1
# BB#2:
	movq	(%rdi), %rax
	jmpq	*16(%rax)               # TAILCALL
.LBB11_1:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	retq
.Lfunc_end11:
	.size	_ZN16CInStreamWithCRCD2Ev, .Lfunc_end11-_ZN16CInStreamWithCRCD2Ev
	.cfi_endproc

	.section	.text._ZN16CInStreamWithCRCD0Ev,"axG",@progbits,_ZN16CInStreamWithCRCD0Ev,comdat
	.weak	_ZN16CInStreamWithCRCD0Ev
	.p2align	4, 0x90
	.type	_ZN16CInStreamWithCRCD0Ev,@function
_ZN16CInStreamWithCRCD0Ev:              # @_ZN16CInStreamWithCRCD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16CInStreamWithCRC+16, (%rbx)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB12_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp3:
	callq	*16(%rax)
.Ltmp4:
.LBB12_2:                               # %_ZN16CInStreamWithCRCD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB12_3:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZN16CInStreamWithCRCD0Ev, .Lfunc_end12-_ZN16CInStreamWithCRCD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp4     #   Call between .Ltmp4 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	_ZTV26CSequentialInStreamWithCRC,@object # @_ZTV26CSequentialInStreamWithCRC
	.section	.rodata,"a",@progbits
	.globl	_ZTV26CSequentialInStreamWithCRC
	.p2align	3
_ZTV26CSequentialInStreamWithCRC:
	.quad	0
	.quad	_ZTI26CSequentialInStreamWithCRC
	.quad	_ZN26CSequentialInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.quad	_ZN26CSequentialInStreamWithCRC6AddRefEv
	.quad	_ZN26CSequentialInStreamWithCRC7ReleaseEv
	.quad	_ZN26CSequentialInStreamWithCRCD2Ev
	.quad	_ZN26CSequentialInStreamWithCRCD0Ev
	.quad	_ZN26CSequentialInStreamWithCRC4ReadEPvjPj
	.size	_ZTV26CSequentialInStreamWithCRC, 64

	.type	_ZTS26CSequentialInStreamWithCRC,@object # @_ZTS26CSequentialInStreamWithCRC
	.globl	_ZTS26CSequentialInStreamWithCRC
	.p2align	4
_ZTS26CSequentialInStreamWithCRC:
	.asciz	"26CSequentialInStreamWithCRC"
	.size	_ZTS26CSequentialInStreamWithCRC, 29

	.type	_ZTS19ISequentialInStream,@object # @_ZTS19ISequentialInStream
	.section	.rodata._ZTS19ISequentialInStream,"aG",@progbits,_ZTS19ISequentialInStream,comdat
	.weak	_ZTS19ISequentialInStream
	.p2align	4
_ZTS19ISequentialInStream:
	.asciz	"19ISequentialInStream"
	.size	_ZTS19ISequentialInStream, 22

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI19ISequentialInStream,@object # @_ZTI19ISequentialInStream
	.section	.rodata._ZTI19ISequentialInStream,"aG",@progbits,_ZTI19ISequentialInStream,comdat
	.weak	_ZTI19ISequentialInStream
	.p2align	4
_ZTI19ISequentialInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19ISequentialInStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19ISequentialInStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI26CSequentialInStreamWithCRC,@object # @_ZTI26CSequentialInStreamWithCRC
	.section	.rodata,"a",@progbits
	.globl	_ZTI26CSequentialInStreamWithCRC
	.p2align	4
_ZTI26CSequentialInStreamWithCRC:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS26CSequentialInStreamWithCRC
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI19ISequentialInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI26CSequentialInStreamWithCRC, 56

	.type	_ZTV16CInStreamWithCRC,@object # @_ZTV16CInStreamWithCRC
	.globl	_ZTV16CInStreamWithCRC
	.p2align	3
_ZTV16CInStreamWithCRC:
	.quad	0
	.quad	_ZTI16CInStreamWithCRC
	.quad	_ZN16CInStreamWithCRC14QueryInterfaceERK4GUIDPPv
	.quad	_ZN16CInStreamWithCRC6AddRefEv
	.quad	_ZN16CInStreamWithCRC7ReleaseEv
	.quad	_ZN16CInStreamWithCRCD2Ev
	.quad	_ZN16CInStreamWithCRCD0Ev
	.quad	_ZN16CInStreamWithCRC4ReadEPvjPj
	.quad	_ZN16CInStreamWithCRC4SeekExjPy
	.size	_ZTV16CInStreamWithCRC, 72

	.type	_ZTS16CInStreamWithCRC,@object # @_ZTS16CInStreamWithCRC
	.globl	_ZTS16CInStreamWithCRC
	.p2align	4
_ZTS16CInStreamWithCRC:
	.asciz	"16CInStreamWithCRC"
	.size	_ZTS16CInStreamWithCRC, 19

	.type	_ZTS9IInStream,@object  # @_ZTS9IInStream
	.section	.rodata._ZTS9IInStream,"aG",@progbits,_ZTS9IInStream,comdat
	.weak	_ZTS9IInStream
_ZTS9IInStream:
	.asciz	"9IInStream"
	.size	_ZTS9IInStream, 11

	.type	_ZTI9IInStream,@object  # @_ZTI9IInStream
	.section	.rodata._ZTI9IInStream,"aG",@progbits,_ZTI9IInStream,comdat
	.weak	_ZTI9IInStream
	.p2align	4
_ZTI9IInStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS9IInStream
	.quad	_ZTI19ISequentialInStream
	.size	_ZTI9IInStream, 24

	.type	_ZTI16CInStreamWithCRC,@object # @_ZTI16CInStreamWithCRC
	.section	.rodata,"a",@progbits
	.globl	_ZTI16CInStreamWithCRC
	.p2align	4
_ZTI16CInStreamWithCRC:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS16CInStreamWithCRC
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI9IInStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI16CInStreamWithCRC, 56


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
