	.text
	.file	"z44.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16
	.text
	.globl	VerticalHyphenate
	.p2align	4, 0x90
	.type	VerticalHyphenate,@function
VerticalHyphenate:                      # @VerticalHyphenate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r9
	cmpb	$18, 32(%r9)
	je	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %rbx
	movl	$.L.str.1, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%rbx, %r9
.LBB0_2:                                # %.outer460.preheader
	xorl	%r15d, %r15d
                                        # implicit-def: %R10
	xorl	%r13d, %r13d
	xorl	%esi, %esi
	movq	%r9, %rax
	xorl	%r11d, %r11d
	xorl	%ecx, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_3:                                #   in Loop: Header=BB0_4 Depth=1
	movl	60(%rdi), %ecx
	cmpl	%ecx, %r15d
	cmovll	%ecx, %r15d
.LBB0_4:                                # %.outer460
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
                                        #       Child Loop BB0_7 Depth 3
                                        #     Child Loop BB0_11 Depth 2
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_16 Depth 2
                                        #     Child Loop BB0_20 Depth 2
	movq	%rsi, %r8
.LBB0_5:                                #   Parent Loop BB0_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_7 Depth 3
	movq	8(%rax), %rax
	cmpq	%r9, %rax
	je	.LBB0_24
# BB#6:                                 # %.preheader456.preheader
                                        #   in Loop: Header=BB0_5 Depth=2
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader456
                                        #   Parent Loop BB0_4 Depth=1
                                        #     Parent Loop BB0_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdx), %rdx
	movzbl	32(%rdx), %esi
	cmpq	$26, %rsi
	ja	.LBB0_12
# BB#8:                                 # %.preheader456
                                        #   in Loop: Header=BB0_7 Depth=3
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_9:                                #   in Loop: Header=BB0_5 Depth=2
	testb	$2, 45(%rdx)
	jne	.LBB0_5
	jmp	.LBB0_124
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_4 Depth=1
	movq	8(%rdx), %rdi
	addq	$16, %rdi
	.p2align	4, 0x90
.LBB0_11:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rdx
	movb	32(%rdx), %sil
	leaq	16(%rdx), %rdi
	testb	%sil, %sil
	je	.LBB0_11
.LBB0_12:                               # %.loopexit455
                                        #   in Loop: Header=BB0_4 Depth=1
	cmpb	$9, %sil
	jne	.LBB0_125
# BB#13:                                #   in Loop: Header=BB0_4 Depth=1
	movq	8(%rdx), %rbp
	addq	$16, %rbp
	.p2align	4, 0x90
.LBB0_14:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdi
	movzbl	32(%rdi), %ebx
	leaq	16(%rdi), %rbp
	testb	%bl, %bl
	je	.LBB0_14
# BB#15:                                #   in Loop: Header=BB0_4 Depth=1
	movq	(%rdx), %rsi
	.p2align	4, 0x90
.LBB0_16:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$16, %rsi
	movq	(%rsi), %rsi
	movzbl	32(%rsi), %ecx
	testb	%cl, %cl
	je	.LBB0_16
# BB#17:                                #   in Loop: Header=BB0_4 Depth=1
	cmpb	$15, %cl
	jne	.LBB0_125
# BB#18:                                #   in Loop: Header=BB0_4 Depth=1
	testq	%r8, %r8
	cmoveq	%rsi, %r13
	cmoveq	%rsi, %r8
	cmpq	%r8, %rsi
	jne	.LBB0_125
# BB#19:                                #   in Loop: Header=BB0_4 Depth=1
	movq	(%rbp), %rcx
	.p2align	4, 0x90
.LBB0_20:                               #   Parent Loop BB0_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	cmpb	$0, 32(%rcx)
	je	.LBB0_20
# BB#21:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	%rsi, %rcx
	jne	.LBB0_125
# BB#22:                                #   in Loop: Header=BB0_4 Depth=1
	cmpb	$19, %bl
	jne	.LBB0_3
# BB#23:                                #   in Loop: Header=BB0_4 Depth=1
	cmpq	$0, 16(%rsp)            # 8-byte Folded Reload
	movq	%rdx, %r10
	movq	%rdi, %r11
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	je	.LBB0_4
	jmp	.LBB0_125
.LBB0_24:
	xorl	%r14d, %r14d
	movq	16(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB0_125
# BB#25:
	movq	8(%r11), %rbp
	cmpq	%rax, %rbp
	je	.LBB0_125
# BB#26:                                # %.preheader453.preheader
	movq	%r10, 96(%rsp)          # 8-byte Spill
	movq	%r11, 64(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_27:                               # %.preheader453
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_28 Depth 2
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_28:                               #   Parent Loop BB0_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_28
# BB#29:                                #   in Loop: Header=BB0_27 Depth=1
	cmpb	$1, %al
	je	.LBB0_33
# BB#30:                                #   in Loop: Header=BB0_27 Depth=1
	cmpb	$9, %al
	jne	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_27 Depth=1
	movq	%rbx, %rdi
	movq	%r9, %r12
	callq	SplitIsDefinite
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	64(%rsp), %r11          # 8-byte Reload
	movq	%r12, %r9
	testl	%eax, %eax
	je	.LBB0_33
	jmp	.LBB0_35
.LBB0_32:                               #   in Loop: Header=BB0_27 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_35
.LBB0_33:                               # %.critedge
                                        #   in Loop: Header=BB0_27 Depth=1
	movq	8(%rbp), %rbp
	cmpq	%rcx, %rbp
	jne	.LBB0_27
	jmp	.LBB0_125
.LBB0_35:                               # %.preheader449
	cmpq	%rcx, %rbp
	je	.LBB0_125
.LBB0_36:                               # %.lr.ph680
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_51 Depth 2
                                        #       Child Loop BB0_52 Depth 3
	cmpb	$19, 32(%rbx)
	jne	.LBB0_58
# BB#37:                                #   in Loop: Header=BB0_36 Depth=1
	movq	8(%rbx), %r14
	cmpq	%rbx, %r14
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB0_42
# BB#38:                                #   in Loop: Header=BB0_36 Depth=1
	cmpb	$0, 32(%r14)
	je	.LBB0_40
# BB#39:                                #   in Loop: Header=BB0_36 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %r12
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %r11          # 8-byte Reload
	movq	%r12, %r9
.LBB0_40:                               #   in Loop: Header=BB0_36 Depth=1
	movq	%r14, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r14), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%r14, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_42
# BB#41:                                #   in Loop: Header=BB0_36 Depth=1
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%r14), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_42:                               #   in Loop: Header=BB0_36 Depth=1
	movq	%rbp, xx_link(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	24(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB0_44
# BB#43:                                #   in Loop: Header=BB0_36 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rbp), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%rbp), %rcx
	movq	%rax, 24(%rcx)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	jmp	.LBB0_45
.LBB0_44:                               #   in Loop: Header=BB0_36 Depth=1
	xorl	%eax, %eax
.LBB0_45:                               #   in Loop: Header=BB0_36 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%rbp, zz_hold(%rip)
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB0_47
# BB#46:                                #   in Loop: Header=BB0_36 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rbp), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rbp
.LBB0_47:                               #   in Loop: Header=BB0_36 Depth=1
	movq	%rbp, zz_hold(%rip)
	movzbl	32(%rbp), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbp), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbp)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_36 Depth=1
	movq	%r9, %rbx
	callq	DisposeObject
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %r11          # 8-byte Reload
	movq	%rbx, %r9
.LBB0_49:                               # %.preheader446
                                        #   in Loop: Header=BB0_36 Depth=1
	movq	8(%r11), %rbp
	xorl	%r14d, %r14d
	cmpq	%rsi, %rbp
	je	.LBB0_125
	.p2align	4, 0x90
.LBB0_51:                               # %.preheader445
                                        #   Parent Loop BB0_36 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_52 Depth 3
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB0_52:                               #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_51 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_52
# BB#53:                                #   in Loop: Header=BB0_51 Depth=2
	cmpb	$1, %al
	je	.LBB0_50
# BB#54:                                #   in Loop: Header=BB0_51 Depth=2
	cmpb	$9, %al
	jne	.LBB0_56
# BB#55:                                #   in Loop: Header=BB0_51 Depth=2
	movq	%rbx, %rdi
	movq	%r9, %r12
	callq	SplitIsDefinite
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %r11          # 8-byte Reload
	movq	%r12, %r9
	testl	%eax, %eax
	je	.LBB0_50
	jmp	.LBB0_57
.LBB0_56:                               #   in Loop: Header=BB0_51 Depth=2
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_57
.LBB0_50:                               # %.critedge3
                                        #   in Loop: Header=BB0_51 Depth=2
	movq	8(%rbp), %rbp
	cmpq	%rsi, %rbp
	jne	.LBB0_51
	jmp	.LBB0_125
.LBB0_57:                               # %.loopexit447
                                        #   in Loop: Header=BB0_36 Depth=1
	cmpq	%rsi, %rbp
	jne	.LBB0_36
	jmp	.LBB0_125
.LBB0_58:
	movb	32(%rbx), %al
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_60
# BB#59:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movq	%r9, %r14
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	%r14, %r9
.LBB0_60:
	movq	8(%rbp), %rdx
	cmpq	16(%rsp), %rdx          # 8-byte Folded Reload
	je	.LBB0_124
# BB#61:                                # %.preheader442.lr.ph.preheader
	movq	(%rbp), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB0_62:                               # %.preheader442
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_63 Depth 2
	movq	%rdx, %r12
.LBB0_63:                               #   Parent Loop BB0_62 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB0_63
# BB#64:                                #   in Loop: Header=BB0_62 Depth=1
	cmpb	$9, %al
	movq	16(%rsp), %rcx          # 8-byte Reload
	je	.LBB0_67
# BB#65:                                #   in Loop: Header=BB0_62 Depth=1
	cmpb	$1, %al
	jne	.LBB0_68
# BB#66:                                # %.critedge4.outer
                                        #   in Loop: Header=BB0_62 Depth=1
	movq	8(%rdx), %rdx
	xorl	%r14d, %r14d
	cmpq	%rcx, %rdx
	movq	%r12, 32(%rsp)          # 8-byte Spill
	jne	.LBB0_62
	jmp	.LBB0_125
.LBB0_67:                               #   in Loop: Header=BB0_62 Depth=1
	movq	%r12, %rdi
	movq	%r9, %rbp
	movq	%rdx, %r14
	callq	SplitIsDefinite
	movq	%r14, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rbp, %r9
	testl	%eax, %eax
	je	.LBB0_69
	jmp	.LBB0_71
.LBB0_68:                               #   in Loop: Header=BB0_62 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_71
.LBB0_69:                               # %.critedge4.backedge
                                        #   in Loop: Header=BB0_62 Depth=1
	movq	8(%rdx), %rdx
	xorl	%r14d, %r14d
	cmpq	%rcx, %rdx
	jne	.LBB0_62
	jmp	.LBB0_125
.LBB0_71:
	movq	%rdx, %rbp
	movq	%r9, 56(%rsp)           # 8-byte Spill
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB0_73
# BB#72:
	movq	no_fpos(%rip), %r8
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_73:                               # %.preheader439
	movq	%rbp, %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB0_74
.LBB0_124:
	xorl	%r14d, %r14d
.LBB0_125:                              # %.critedge367
	movl	%r14d, %eax
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_75:                               #   in Loop: Header=BB0_74 Depth=1
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	je	.LBB0_99
.LBB0_77:                               # %.preheader435
                                        #   Parent Loop BB0_74 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_78 Depth 3
	movq	%rbp, %rdi
.LBB0_78:                               #   Parent Loop BB0_74 Depth=1
                                        #     Parent Loop BB0_77 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rdi), %rdi
	movzbl	32(%rdi), %eax
	testb	%al, %al
	je	.LBB0_78
# BB#79:                                #   in Loop: Header=BB0_77 Depth=2
	cmpb	$1, %al
	je	.LBB0_76
# BB#80:                                #   in Loop: Header=BB0_77 Depth=2
	cmpb	$9, %al
	jne	.LBB0_82
# BB#81:                                #   in Loop: Header=BB0_77 Depth=2
	callq	SplitIsDefinite
	testl	%eax, %eax
	je	.LBB0_76
	jmp	.LBB0_83
.LBB0_82:                               #   in Loop: Header=BB0_77 Depth=2
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_83
.LBB0_76:                               # %.critedge6
                                        #   in Loop: Header=BB0_77 Depth=2
	movq	8(%rbp), %rbp
	cmpq	%r12, %rbp
	jne	.LBB0_77
.LBB0_99:                               # %.critedge372
                                        #   in Loop: Header=BB0_74 Depth=1
	movq	8(%r12), %rbp
	cmpq	%r12, %rbp
	je	.LBB0_104
# BB#100:                               #   in Loop: Header=BB0_74 Depth=1
	cmpb	$0, 32(%rbp)
	je	.LBB0_102
# BB#101:                               #   in Loop: Header=BB0_74 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_102:                              #   in Loop: Header=BB0_74 Depth=1
	movq	%rbp, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbp, zz_res(%rip)
	movq	%r14, %rax
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_104
# BB#103:                               #   in Loop: Header=BB0_74 Depth=1
	movq	%r14, %rcx
	movq	(%rcx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_104:                              #   in Loop: Header=BB0_74 Depth=1
	movq	%r14, xx_link(%rip)
	movq	%r14, zz_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB0_106
# BB#105:                               #   in Loop: Header=BB0_74 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%r14), %rcx
	movq	%rcx, 16(%rax)
	movq	16(%r14), %rcx
	movq	%rax, 24(%rcx)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	jmp	.LBB0_107
.LBB0_106:                              #   in Loop: Header=BB0_74 Depth=1
	xorl	%eax, %eax
.LBB0_107:                              #   in Loop: Header=BB0_74 Depth=1
	movq	%rax, xx_tmp(%rip)
	movq	%r14, zz_hold(%rip)
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB0_109
# BB#108:                               #   in Loop: Header=BB0_74 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%r14), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %r14
.LBB0_109:                              #   in Loop: Header=BB0_74 Depth=1
	movq	%r14, zz_hold(%rip)
	movzbl	32(%r14), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r14), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r14)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB0_111
# BB#110:                               #   in Loop: Header=BB0_74 Depth=1
	callq	DisposeObject
.LBB0_111:                              #   in Loop: Header=BB0_74 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rax
	movq	8(%rax), %rsi
	xorl	%r14d, %r14d
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, %rsi
	je	.LBB0_125
# BB#112:                               # %.preheader434.preheader
                                        #   in Loop: Header=BB0_74 Depth=1
	xorl	%edx, %edx
.LBB0_113:                              # %.preheader434
                                        #   Parent Loop BB0_74 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_114 Depth 3
	movq	%rsi, %r12
.LBB0_114:                              #   Parent Loop BB0_74 Depth=1
                                        #     Parent Loop BB0_113 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12), %r12
	movzbl	32(%r12), %eax
	testb	%al, %al
	je	.LBB0_114
# BB#115:                               #   in Loop: Header=BB0_113 Depth=2
	cmpb	$1, %al
	je	.LBB0_120
# BB#116:                               #   in Loop: Header=BB0_113 Depth=2
	cmpb	$9, %al
	jne	.LBB0_118
# BB#117:                               #   in Loop: Header=BB0_113 Depth=2
	movq	%r12, %rdi
	movq	%rdx, %rbp
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	callq	SplitIsDefinite
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB0_119
	jmp	.LBB0_121
.LBB0_118:                              #   in Loop: Header=BB0_113 Depth=2
	addb	$-9, %al
	cmpb	$90, %al
	jbe	.LBB0_121
.LBB0_119:                              #   in Loop: Header=BB0_113 Depth=2
	movq	%rdx, %r12
.LBB0_120:                              # %.critedge8
                                        #   in Loop: Header=BB0_113 Depth=2
	movq	8(%rsi), %rsi
	cmpq	%rcx, %rsi
	movq	%r12, %rdx
	jne	.LBB0_113
	jmp	.LBB0_125
.LBB0_121:                              #   in Loop: Header=BB0_74 Depth=1
	movq	%rsi, %rbp
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	testq	%rdx, %rdx
	jne	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_74 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB0_123:                              # %.backedge441
                                        #   in Loop: Header=BB0_74 Depth=1
	movq	%rbp, %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB0_74
	jmp	.LBB0_125
.LBB0_83:                               #   in Loop: Header=BB0_74 Depth=1
	movq	(%rbp), %rax
	cmpq	%r12, %rax
	je	.LBB0_99
# BB#84:                                # %.preheader437.preheader
                                        #   in Loop: Header=BB0_74 Depth=1
	movq	%rax, %rcx
.LBB0_85:                               # %.preheader437
                                        #   Parent Loop BB0_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB0_85
# BB#86:                                # %.preheader437
                                        #   in Loop: Header=BB0_74 Depth=1
	cmpb	$1, %dl
	jne	.LBB0_88
# BB#87:                                #   in Loop: Header=BB0_74 Depth=1
	movzwl	44(%rcx), %ecx
	cmpl	$8191, %ecx             # imm = 0x1FFF
	ja	.LBB0_89
.LBB0_88:                               # %.loopexit438
                                        #   in Loop: Header=BB0_74 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%rbp), %rax
.LBB0_89:                               #   in Loop: Header=BB0_74 Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_91
# BB#90:                                #   in Loop: Header=BB0_74 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB0_91:                               #   in Loop: Header=BB0_74 Depth=1
	movq	%rax, zz_res(%rip)
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	24(%rcx), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_94
# BB#92:                                #   in Loop: Header=BB0_74 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_74 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rcx
.LBB0_94:                               #   in Loop: Header=BB0_74 Depth=1
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_96
# BB#95:                                #   in Loop: Header=BB0_74 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB0_96:                               #   in Loop: Header=BB0_74 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_99
# BB#97:                                #   in Loop: Header=BB0_74 Depth=1
	testq	%rcx, %rcx
	je	.LBB0_99
# BB#98:                                #   in Loop: Header=BB0_74 Depth=1
	movq	(%rbp), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rcx), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	jmp	.LBB0_99
.LBB0_74:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_77 Depth 2
                                        #       Child Loop BB0_78 Depth 3
                                        #     Child Loop BB0_85 Depth 2
                                        #     Child Loop BB0_113 Depth 2
                                        #       Child Loop BB0_114 Depth 3
	movq	%rax, %r14
	cmpb	$19, 32(%r12)
	je	.LBB0_75
# BB#126:
	movb	32(%r12), %al
	addb	$-9, %al
	cmpb	$91, %al
	jb	.LBB0_128
# BB#127:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_128:
	movq	32(%rsp), %rax          # 8-byte Reload
	movzwl	44(%rax), %eax
	cmpl	$8191, %eax             # imm = 0x1FFF
	ja	.LBB0_130
# BB#129:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_130:                              # %.critedge385
	movl	60(%rbx), %eax
	cmpl	%eax, %r15d
	cmovll	%eax, %r15d
	movl	52(%r12), %esi
	movl	60(%r12), %edx
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	44(%rax), %rbp
	movl	%r15d, %edi
	movq	%rbp, %rcx
	callq	MinGap
	movl	%eax, %r15d
	movl	60(%rbx), %edi
	movl	52(%r12), %esi
	movl	60(%r12), %edx
	movq	%rbp, %rcx
	callq	MinGap
	xorl	%r14d, %r14d
	cmpl	%eax, %r15d
	jne	.LBB0_125
# BB#131:                               # %.preheader432
	movq	64(%rsp), %rax          # 8-byte Reload
.LBB0_132:                              # %.preheader432
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_134 Depth 2
	movq	8(%rax), %rax
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	je	.LBB0_138
# BB#133:                               # %.preheader431
                                        #   in Loop: Header=BB0_132 Depth=1
	movq	%rax, %rcx
.LBB0_134:                              #   Parent Loop BB0_132 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB0_134
# BB#135:                               #   in Loop: Header=BB0_132 Depth=1
	cmpb	$1, %dl
	jne	.LBB0_132
# BB#136:                               #   in Loop: Header=BB0_132 Depth=1
	movzwl	44(%rcx), %ecx
	cmpl	$8192, %ecx             # imm = 0x2000
	jb	.LBB0_132
# BB#137:                               #   in Loop: Header=BB0_132 Depth=1
	andl	$512, %ecx              # imm = 0x200
	testw	%cx, %cx
	je	.LBB0_132
	jmp	.LBB0_125
.LBB0_138:                              # %._crit_edge617
	movq	24(%rbx), %rax
	jmp	.LBB0_140
.LBB0_139:                              # %EncloseInHcat.exit
                                        #   in Loop: Header=BB0_140 Depth=1
	movq	80(%rsp), %rax          # 8-byte Reload
.LBB0_140:                              # %._crit_edge617
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_142 Depth 2
                                        #     Child Loop BB0_161 Depth 2
                                        #       Child Loop BB0_162 Depth 3
                                        #       Child Loop BB0_165 Depth 3
                                        #       Child Loop BB0_172 Depth 3
	movq	8(%rax), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	cmpq	16(%rsp), %rax          # 8-byte Folded Reload
	je	.LBB0_258
# BB#141:                               # %.preheader429
                                        #   in Loop: Header=BB0_140 Depth=1
	movq	80(%rsp), %rcx          # 8-byte Reload
.LBB0_142:                              #   Parent Loop BB0_140 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rcx), %rcx
	movzbl	32(%rcx), %eax
	testb	%al, %al
	je	.LBB0_142
# BB#143:                               #   in Loop: Header=BB0_140 Depth=1
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	cmpb	$1, %al
	je	.LBB0_139
# BB#144:                               #   in Loop: Header=BB0_140 Depth=1
	addb	$-9, %al
	cmpb	$90, %al
	ja	.LBB0_139
# BB#145:                               #   in Loop: Header=BB0_140 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 24(%rax)
	jne	.LBB0_147
# BB#146:                               #   in Loop: Header=BB0_140 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.18, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_147:                              #   in Loop: Header=BB0_140 Depth=1
	movzbl	zz_lengths+18(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_149
# BB#148:                               #   in Loop: Header=BB0_140 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_150
.LBB0_149:                              #   in Loop: Header=BB0_140 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rax, zz_hold(%rip)
.LBB0_150:                              #   in Loop: Header=BB0_140 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$18, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	andb	$-9, 43(%rax)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_152
# BB#151:                               #   in Loop: Header=BB0_140 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB0_152:                              #   in Loop: Header=BB0_140 Depth=1
	movq	%rax, zz_res(%rip)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_155
# BB#153:                               #   in Loop: Header=BB0_140 Depth=1
	testq	%rax, %rax
	je	.LBB0_155
# BB#154:                               #   in Loop: Header=BB0_140 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_155:                              #   in Loop: Header=BB0_140 Depth=1
	movq	72(%rsp), %rax          # 8-byte Reload
	cmpq	%rax, 24(%rax)
	je	.LBB0_157
# BB#156:                               #   in Loop: Header=BB0_140 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.19, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_157:                              #   in Loop: Header=BB0_140 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movzwl	34(%rdx), %eax
	movq	24(%rsp), %rdi          # 8-byte Reload
	movw	%ax, 34(%rdi)
	movl	36(%rdx), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rdi), %ecx
	movl	$-1048576, %esi         # imm = 0xFFF00000
	andl	%esi, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rdi)
	movl	36(%rdx), %ecx
	andl	%esi, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rdi)
	movl	48(%rdx), %eax
	movl	%eax, 48(%rdi)
	movl	56(%rdx), %eax
	movl	%eax, 56(%rdi)
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	52(%rcx), %eax
	movl	%eax, 52(%rdi)
	movl	60(%rcx), %ecx
	movl	%ecx, 60(%rdi)
	movzbl	zz_lengths+15(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	testq	%rcx, %rcx
	je	.LBB0_159
# BB#158:                               #   in Loop: Header=BB0_140 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_160
.LBB0_159:                              #   in Loop: Header=BB0_140 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	%rax, zz_hold(%rip)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	52(%rax), %eax
.LBB0_160:                              #   in Loop: Header=BB0_140 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movb	$15, 32(%rcx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movl	%eax, 52(%rcx)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	60(%rax), %eax
	movl	%eax, 60(%rcx)
	movb	$1, 41(%rcx)
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB0_139
.LBB0_161:                              # %.preheader.i
                                        #   Parent Loop BB0_140 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_162 Depth 3
                                        #       Child Loop BB0_165 Depth 3
                                        #       Child Loop BB0_172 Depth 3
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rcx, %rbp
.LBB0_162:                              #   Parent Loop BB0_140 Depth=1
                                        #     Parent Loop BB0_161 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%rbp), %rbp
	movzbl	32(%rbp), %eax
	cmpq	$26, %rax
	ja	.LBB0_168
# BB#163:                               #   in Loop: Header=BB0_162 Depth=3
	jmpq	*.LJTI0_1(,%rax,8)
.LBB0_164:                              #   in Loop: Header=BB0_161 Depth=2
	movq	8(%rbp), %rcx
	addq	$16, %rcx
.LBB0_165:                              #   Parent Loop BB0_140 Depth=1
                                        #     Parent Loop BB0_161 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %r12
	movb	32(%r12), %al
	leaq	16(%r12), %rcx
	testb	%al, %al
	je	.LBB0_165
	jmp	.LBB0_169
.LBB0_166:                              #   in Loop: Header=BB0_161 Depth=2
	movzbl	zz_lengths+1(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB0_246
# BB#167:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_247
.LBB0_168:                              # %.loopexit.i.loopexit1108
                                        #   in Loop: Header=BB0_161 Depth=2
	movq	%rbp, %r12
.LBB0_169:                              # %.loopexit.i
                                        #   in Loop: Header=BB0_161 Depth=2
	cmpb	$9, %al
	je	.LBB0_171
# BB#170:                               #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_171:                              #   in Loop: Header=BB0_161 Depth=2
	movq	8(%r12), %rax
	addq	$16, %rax
.LBB0_172:                              #   Parent Loop BB0_140 Depth=1
                                        #     Parent Loop BB0_161 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %r15
	leaq	16(%r15), %rax
	cmpb	$0, 32(%r15)
	je	.LBB0_172
# BB#173:                               #   in Loop: Header=BB0_161 Depth=2
	movzbl	zz_lengths+9(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_175
# BB#174:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_176
.LBB0_175:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_176:                              #   in Loop: Header=BB0_161 Depth=2
	movb	$9, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movzwl	34(%r12), %ecx
	movw	%cx, 34(%rax)
	movl	36(%r12), %esi
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %esi
	movl	36(%rax), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%esi, %ecx
	movl	%ecx, 36(%rax)
	movl	36(%r12), %ecx
	andl	%edx, %ecx
	orl	%esi, %ecx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 36(%rax)
	cmpq	%rbp, %r12
	je	.LBB0_179
# BB#177:                               #   in Loop: Header=BB0_161 Depth=2
	movzbl	32(%rbp), %eax
	movzbl	zz_lengths(%rax), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB0_181
# BB#178:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_182
.LBB0_179:                              #   in Loop: Header=BB0_161 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_197
# BB#180:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_198
.LBB0_181:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
	movb	32(%rbp), %al
.LBB0_182:                              #   in Loop: Header=BB0_161 Depth=2
	movb	%al, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movl	48(%rbp), %eax
	movl	%eax, 48(%r14)
	movl	56(%rbp), %eax
	movl	%eax, 56(%r14)
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	48(%rcx), %eax
	movl	%eax, 52(%r14)
	movl	56(%rcx), %eax
	movl	%eax, 60(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_184
# BB#183:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_185
.LBB0_184:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_185:                              #   in Loop: Header=BB0_161 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_188
# BB#186:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_188
# BB#187:                               #   in Loop: Header=BB0_161 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_188:                              #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB0_191
# BB#189:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_191
# BB#190:                               #   in Loop: Header=BB0_161 Depth=2
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_191:                              #   in Loop: Header=BB0_161 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_193
# BB#192:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_194
.LBB0_193:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_194:                              #   in Loop: Header=BB0_161 Depth=2
	testq	%r14, %r14
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	je	.LBB0_202
# BB#195:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_202
# BB#196:                               #   in Loop: Header=BB0_161 Depth=2
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	jmp	.LBB0_201
.LBB0_197:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_198:                              #   in Loop: Header=BB0_161 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_202
# BB#199:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_202
# BB#200:                               #   in Loop: Header=BB0_161 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
.LBB0_201:                              #   in Loop: Header=BB0_161 Depth=2
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_202:                              #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_res(%rip)
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, zz_hold(%rip)
	testq	%rsi, %rsi
	je	.LBB0_205
# BB#203:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_205
# BB#204:                               #   in Loop: Header=BB0_161 Depth=2
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_205:                              #   in Loop: Header=BB0_161 Depth=2
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	je	.LBB0_208
# BB#206:                               #   in Loop: Header=BB0_161 Depth=2
	leaq	32(%r15), %r14
	movzbl	zz_lengths+26(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB0_209
# BB#207:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_210
.LBB0_208:                              #   in Loop: Header=BB0_161 Depth=2
	movl	48(%r12), %eax
	movq	72(%rsp), %rbp          # 8-byte Reload
	movl	%eax, 48(%rbp)
	movl	56(%r12), %eax
	movl	%eax, 56(%rbp)
	jmp	.LBB0_219
.LBB0_209:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB0_210:                              #   in Loop: Header=BB0_161 Depth=2
	movb	$26, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzwl	34(%r15), %eax
	movw	%ax, 34(%rbp)
	movl	36(%r15), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%rbp), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movl	36(%r15), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movl	48(%r15), %eax
	movl	%eax, 64(%rbp)
	movl	56(%r15), %eax
	addl	48(%r15), %eax
	movl	%eax, 68(%rbp)
	movl	56(%r15), %eax
	movl	%eax, 72(%rbp)
	movl	48(%r15), %eax
	movl	%eax, 48(%rbp)
	movl	56(%r15), %eax
	movl	%eax, 56(%rbp)
	movl	$0, 60(%rbp)
	movl	$0, 52(%rbp)
	movl	$11, %edi
	movl	$.L.str.21, %esi
	movq	%r14, %rdx
	callq	MakeWord
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r15)
	movl	40(%r15), %eax
	movl	$-1610612737, %ecx      # imm = 0x9FFFFFFF
	andl	%ecx, %eax
	orl	$536870912, %eax        # imm = 0x20000000
	movl	%eax, 40(%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_212
# BB#211:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_213
.LBB0_212:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_213:                              #   in Loop: Header=BB0_161 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_216
# BB#214:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_216
# BB#215:                               #   in Loop: Header=BB0_161 Depth=2
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_216:                              #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB0_219
# BB#217:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_219
# BB#218:                               #   in Loop: Header=BB0_161 Depth=2
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_219:                              #   in Loop: Header=BB0_161 Depth=2
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_221
# BB#220:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_222
.LBB0_221:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_222:                              #   in Loop: Header=BB0_161 Depth=2
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_225
# BB#223:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_225
# BB#224:                               #   in Loop: Header=BB0_161 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_225:                              #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB0_228
# BB#226:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_228
# BB#227:                               #   in Loop: Header=BB0_161 Depth=2
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_228:                              #   in Loop: Header=BB0_161 Depth=2
	movl	48(%rbp), %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 48(%rcx)
	movl	56(%rbp), %eax
	movl	%eax, 56(%rcx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_230
# BB#229:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_231
.LBB0_230:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_231:                              #   in Loop: Header=BB0_161 Depth=2
	movq	40(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_234
# BB#232:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_234
# BB#233:                               #   in Loop: Header=BB0_161 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_234:                              #   in Loop: Header=BB0_161 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_237
# BB#235:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_237
# BB#236:                               #   in Loop: Header=BB0_161 Depth=2
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsi), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rsi)
	movq	16(%rax), %rdx
	movq	%rsi, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_237:                              #   in Loop: Header=BB0_161 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	52(%rcx), %eax
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	%eax, 52(%rdx)
	movl	60(%rcx), %eax
	movl	%eax, 60(%rdx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_239
# BB#238:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_240
.LBB0_239:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_240:                              #   in Loop: Header=BB0_161 Depth=2
	movq	48(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_243
# BB#241:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_243
# BB#242:                               #   in Loop: Header=BB0_161 Depth=2
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_243:                              #   in Loop: Header=BB0_161 Depth=2
	testq	%rbp, %rbp
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB0_257
# BB#244:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_257
# BB#245:                               #   in Loop: Header=BB0_161 Depth=2
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	jmp	.LBB0_256
.LBB0_246:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB0_247:                              #   in Loop: Header=BB0_161 Depth=2
	movb	$1, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzwl	34(%rbp), %eax
	movw	%ax, 34(%r14)
	movl	36(%rbp), %eax
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	%ecx, %eax
	movl	36(%r14), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r14)
	movl	36(%rbp), %ecx
	andl	%edx, %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r14)
	movzwl	44(%rbp), %ecx
	andl	$128, %ecx
	movzwl	44(%r14), %eax
	andl	$65407, %eax            # imm = 0xFF7F
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	44(%rbp), %ecx
	andl	$256, %ecx              # imm = 0x100
	andl	$-257, %eax             # imm = 0xFEFF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	44(%rbp), %ecx
	andl	$512, %ecx              # imm = 0x200
	andl	$-513, %eax             # imm = 0xFDFF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	44(%rbp), %ecx
	andl	$7168, %ecx             # imm = 0x1C00
	andl	$-7169, %eax            # imm = 0xE3FF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	44(%rbp), %ecx
	andl	$57344, %ecx            # imm = 0xE000
	andl	$8191, %eax             # imm = 0x1FFF
	orl	%ecx, %eax
	movw	%ax, 44(%r14)
	movzwl	46(%rbp), %eax
	movw	%ax, 46(%r14)
	movb	41(%rbp), %al
	movb	%al, 41(%r14)
	movb	42(%rbp), %al
	movb	%al, 42(%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_249
# BB#248:                               #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_250
.LBB0_249:                              #   in Loop: Header=BB0_161 Depth=2
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_250:                              #   in Loop: Header=BB0_161 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	je	.LBB0_253
# BB#251:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_253
# BB#252:                               #   in Loop: Header=BB0_161 Depth=2
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_253:                              #   in Loop: Header=BB0_161 Depth=2
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB0_257
# BB#254:                               #   in Loop: Header=BB0_161 Depth=2
	testq	%rax, %rax
	je	.LBB0_257
# BB#255:                               #   in Loop: Header=BB0_161 Depth=2
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
.LBB0_256:                              # %.backedge.i
                                        #   in Loop: Header=BB0_161 Depth=2
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_257:                              # %.backedge.i
                                        #   in Loop: Header=BB0_161 Depth=2
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	8(%rcx), %rcx
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	jne	.LBB0_161
	jmp	.LBB0_139
.LBB0_258:                              # %._crit_edge
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rbp
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	je	.LBB0_263
# BB#259:
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	8(%rax), %r14
	cmpb	$0, 32(%rbp)
	je	.LBB0_261
# BB#260:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_261:
	movq	%rbp, zz_res(%rip)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, zz_hold(%rip)
	movq	(%rcx), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	%rbp, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB0_263
# BB#262:
	movq	(%r14), %rax
	movq	%rax, zz_tmp(%rip)
	movq	(%rbp), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_263:
	movl	60(%rbx), %eax
	movq	96(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 60(%rcx)
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 60(%rcx)
	movl	%eax, 60(%r13)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 60(%rcx)
	movq	24(%rcx), %rax
	movq	(%rax), %r12
	cmpb	$0, 32(%r12)
	jne	.LBB0_268
.LBB0_265:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_266 Depth 2
	leaq	16(%r12), %rcx
.LBB0_266:                              #   Parent Loop BB0_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %edx
	addq	$16, %rcx
	testb	%dl, %dl
	je	.LBB0_266
# BB#267:                               #   in Loop: Header=BB0_265 Depth=1
	addb	$-119, %dl
	cmpb	$19, %dl
	ja	.LBB0_268
# BB#264:                               #   in Loop: Header=BB0_265 Depth=1
	movq	(%r12), %r12
	cmpb	$0, 32(%r12)
	je	.LBB0_265
.LBB0_268:                              # %.preheader426
	movq	8(%r12), %rbp
	cmpq	%rax, %rbp
	je	.LBB0_298
# BB#269:
	addq	$8, %r12
	movl	$28, %r15d
	movl	$38, %r13d
.LBB0_270:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_282 Depth 2
                                        #       Child Loop BB0_284 Depth 3
	movq	16(%rbp), %rbp
	movb	32(%rbp), %al
	testb	%al, %al
	je	.LBB0_270
# BB#271:                               #   in Loop: Header=BB0_270 Depth=1
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$20, %cl
	jb	.LBB0_273
# BB#272:                               #   in Loop: Header=BB0_270 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	callq	Error
	movb	32(%rbp), %al
.LBB0_273:                              #   in Loop: Header=BB0_270 Depth=1
	movl	%eax, %ecx
	addb	$-128, %cl
	cmpb	$10, %cl
	ja	.LBB0_276
# BB#274:                               #   in Loop: Header=BB0_270 Depth=1
	movzbl	%cl, %eax
	btq	%rax, %r13
	jb	.LBB0_280
# BB#275:                               #   in Loop: Header=BB0_270 Depth=1
	movl	$1792, %ecx             # imm = 0x700
	btq	%rax, %rcx
	jb	.LBB0_278
	jmp	.LBB0_296
.LBB0_276:                              #   in Loop: Header=BB0_270 Depth=1
	movl	%eax, %ecx
	addb	$-119, %cl
	cmpb	$8, %cl
	ja	.LBB0_295
# BB#277:                               #   in Loop: Header=BB0_270 Depth=1
	movzbl	%cl, %ecx
	btq	%rcx, %r15
	jae	.LBB0_279
.LBB0_278:                              #   in Loop: Header=BB0_270 Depth=1
	addq	$80, %rbp
	jmp	.LBB0_281
.LBB0_279:                              #   in Loop: Header=BB0_270 Depth=1
	cmpq	$1, %rcx
	jne	.LBB0_294
.LBB0_280:                              #   in Loop: Header=BB0_270 Depth=1
	addq	$88, %rbp
.LBB0_281:                              # %FindTarget.exit
                                        #   in Loop: Header=BB0_270 Depth=1
	movq	(%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB0_296
.LBB0_282:                              # %.loopexit.i402
                                        #   Parent Loop BB0_270 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_284 Depth 3
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB0_296
# BB#283:                               # %.preheader.i403
                                        #   in Loop: Header=BB0_282 Depth=2
	movq	%rcx, %rdx
	movq	%rax, %rcx
.LBB0_284:                              #   Parent Loop BB0_270 Depth=1
                                        #     Parent Loop BB0_282 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %ebx
	testb	%bl, %bl
	je	.LBB0_284
# BB#285:                               #   in Loop: Header=BB0_282 Depth=2
	cmpb	$8, %bl
	jne	.LBB0_282
# BB#286:                               # %WhichComponent.exit
                                        #   in Loop: Header=BB0_270 Depth=1
	testq	%rdx, %rdx
	movq	(%r12), %rcx
	je	.LBB0_297
# BB#287:                               # %WhichComponent.exit
                                        #   in Loop: Header=BB0_270 Depth=1
	cmpq	56(%rsp), %rdx          # 8-byte Folded Reload
	je	.LBB0_297
# BB#288:                               #   in Loop: Header=BB0_270 Depth=1
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rsi
	cmpq	%rcx, %rsi
	je	.LBB0_290
# BB#289:                               #   in Loop: Header=BB0_270 Depth=1
	movq	%rsi, zz_res(%rip)
	movq	(%rcx), %rax
	movq	%rax, (%rsi)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rsi
	movq	%rax, 8(%rsi)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
	movq	24(%rdx), %rax
.LBB0_290:                              #   in Loop: Header=BB0_270 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB0_293
# BB#291:                               #   in Loop: Header=BB0_270 Depth=1
	testq	%rax, %rax
	je	.LBB0_293
# BB#292:                               #   in Loop: Header=BB0_270 Depth=1
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB0_293:                              # %.backedge
                                        #   in Loop: Header=BB0_270 Depth=1
	movq	(%r12), %rbp
	movl	$1, %r14d
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	24(%rax), %rbp
	jne	.LBB0_270
	jmp	.LBB0_125
.LBB0_294:                              #   in Loop: Header=BB0_270 Depth=1
	movl	$353, %edx              # imm = 0x161
	btq	%rcx, %rdx
	jb	.LBB0_296
.LBB0_295:                              # %FindTarget.exit.thread408
                                        #   in Loop: Header=BB0_270 Depth=1
	movzbl	%al, %edi
	movq	no_fpos(%rip), %rbp
	callq	Image
	movq	%rax, (%rsp)
	movl	$1, %edi
	movl	$3, %esi
	movl	$.L.str.22, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.23, %r9d
	xorl	%eax, %eax
	movq	%rbp, %r8
	callq	Error
                                        # implicit-def: %RCX
	jmp	.LBB0_282
.LBB0_296:                              # %.outer.backedge.sink.split
                                        #   in Loop: Header=BB0_270 Depth=1
	movq	(%r12), %rcx
.LBB0_297:                              # %.outer.backedge
                                        #   in Loop: Header=BB0_270 Depth=1
	movq	8(%rcx), %rbp
	addq	$8, %rcx
	movl	$1, %r14d
	movq	56(%rsp), %rax          # 8-byte Reload
	cmpq	24(%rax), %rbp
	movq	%rcx, %r12
	jne	.LBB0_270
	jmp	.LBB0_125
.LBB0_298:
	movl	$1, %r14d
	jmp	.LBB0_125
.Lfunc_end0:
	.size	VerticalHyphenate, .Lfunc_end0-VerticalHyphenate
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_7
	.quad	.LBB0_9
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_12
	.quad	.LBB0_10
	.quad	.LBB0_12
	.quad	.LBB0_10
.LJTI0_1:
	.quad	.LBB0_162
	.quad	.LBB0_166
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_168
	.quad	.LBB0_164
	.quad	.LBB0_168
	.quad	.LBB0_164

	.text
	.globl	ConvertGalleyList
	.p2align	4, 0x90
	.type	ConvertGalleyList,@function
ConvertGalleyList:                      # @ConvertGalleyList
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 48
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	8(%r14), %rax
	leaq	16(%rax), %rcx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbx
	leaq	16(%rbx), %rcx
	cmpb	$0, 32(%rbx)
	je	.LBB1_1
# BB#2:
	movq	8(%rbx), %rdx
	addq	$16, %rdx
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rcx
	leaq	16(%rcx), %rdx
	cmpb	$0, 32(%rcx)
	je	.LBB1_3
# BB#4:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rdx
	cmpq	%rax, %rdx
	je	.LBB1_6
# BB#5:
	movq	%rdx, zz_res(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rdx)
	movq	16(%rax), %rsi
	movq	%rdx, 24(%rsi)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_6:
	movq	%rax, zz_res(%rip)
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_8
# BB#7:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB1_8:
	movq	8(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_10
# BB#9:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_10:
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_12
# BB#11:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_12:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	24(%r14), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_14
# BB#13:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB1_14:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_17
# BB#15:
	testq	%rax, %rax
	je	.LBB1_17
# BB#16:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_17:                               # %.preheader
	movq	8(%r14), %rax
	xorl	%edi, %edi
	cmpq	%r14, %rax
	je	.LBB1_20
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%edi
	movq	8(%rax), %rax
	cmpq	%r14, %rax
	jne	.LBB1_18
.LBB1_20:                               # %._crit_edge
	leaq	8(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%r14, %rsi
	callq	BuildMergeTree
	movq	%rax, %r15
	cmpq	%r14, 8(%r14)
	jne	.LBB1_22
# BB#21:
	cmpq	%r14, 24(%r14)
	je	.LBB1_23
.LBB1_22:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.8, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_23:
	movq	%r14, zz_hold(%rip)
	movzbl	32(%r14), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r14), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r14)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	8(%r15), %rcx
	leaq	16(%rcx), %rdx
	.p2align	4, 0x90
.LBB1_24:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rax
	leaq	16(%rax), %rdx
	cmpb	$0, 32(%rax)
	je	.LBB1_24
# BB#25:
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_27
# BB#26:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB1_27:
	testq	%rbx, %rbx
	movq	%rcx, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB1_30
# BB#28:
	testq	%rcx, %rcx
	je	.LBB1_30
# BB#29:
	movq	(%rbx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	zz_res(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	zz_tmp(%rip), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_tmp(%rip), %rdx
	movq	%rcx, 8(%rdx)
.LBB1_30:
	movq	(%r15), %rcx
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB1_32
# BB#31:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB1_32:
	movq	%rcx, zz_res(%rip)
	movq	%rax, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_35
# BB#33:
	testq	%rcx, %rcx
	je	.LBB1_35
# BB#34:
	movq	(%rax), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB1_35:
	cmpq	%r15, 8(%r15)
	jne	.LBB1_37
# BB#36:
	cmpq	%r15, 24(%r15)
	je	.LBB1_38
.LBB1_37:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.9, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_38:
	movq	%r15, zz_hold(%rip)
	movzbl	32(%r15), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	movq	%r15, %rdx
	addq	$33, %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r15)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	ConvertGalleyList, .Lfunc_end1-ConvertGalleyList
	.cfi_endproc

	.p2align	4, 0x90
	.type	BuildMergeTree,@function
BuildMergeTree:                         # @BuildMergeTree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 80
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %r13
	movl	%edi, %ebp
	cmpl	$1, %ebp
	jne	.LBB2_15
# BB#1:
	movzbl	zz_lengths+81(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB2_2
# BB#3:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_4
.LBB2_15:
	movl	%ebp, %ebx
	shrl	$31, %ebx
	addl	%ebp, %ebx
	sarl	%ebx
	movl	%ebx, %edi
	movq	%r13, %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	BuildMergeTree
	movq	%rax, %r12
	subl	%ebx, %ebp
	leaq	16(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	BuildMergeTree
	movq	%rax, %r14
	movq	8(%rsp), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_16
	.p2align	4, 0x90
.LBB2_18:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_19 Depth 2
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB2_19:                               #   Parent Loop BB2_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB2_19
# BB#20:                                #   in Loop: Header=BB2_18 Depth=1
	testb	$4, 126(%rbx)
	jne	.LBB2_21
# BB#17:                                #   in Loop: Header=BB2_18 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.LBB2_18
.LBB2_21:                               # %._crit_edge
	cmpq	%rax, %rbx
	jne	.LBB2_23
	jmp	.LBB2_22
.LBB2_2:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB2_4:
	movb	$81, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movq	8(%r13), %rcx
	leaq	16(%rcx), %rdx
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movzbl	32(%rbx), %eax
	leaq	16(%rbx), %rdx
	testb	%al, %al
	je	.LBB2_5
# BB#6:
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rdx
	cmpq	%rcx, %rdx
	je	.LBB2_8
# BB#7:
	movq	%rdx, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_hold(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	%rdx, 8(%rdx)
	movq	%rdx, (%rdx)
	movq	xx_link(%rip), %rcx
.LBB2_8:
	movq	%rcx, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB2_11
# BB#9:
	testq	%rcx, %rcx
	je	.LBB2_11
# BB#10:
	movq	(%r12), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	zz_hold(%rip), %rcx
	movq	zz_res(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rcx, 8(%rsi)
	movq	zz_tmp(%rip), %rcx
	movq	%rcx, (%rdx)
	movq	zz_res(%rip), %rcx
	movq	zz_tmp(%rip), %rdx
	movq	%rcx, 8(%rdx)
.LBB2_11:
	cmpb	$2, %al
	jne	.LBB2_13
# BB#12:
	movq	80(%rbx), %rax
	testb	$8, 126(%rax)
	jne	.LBB2_14
.LBB2_13:                               # %._crit_edge148
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.24, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	80(%rbx), %rax
.LBB2_14:
	movq	%rax, (%r15)
	movq	%rbx, %rdi
	callq	DetachEnv
	movq	%rax, (%r14)
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	AttachEnv
	jmp	.LBB2_89
.LBB2_16:
                                        # implicit-def: %RBX
.LBB2_22:                               # %._crit_edge.thread
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.25, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_23:
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB2_24
# BB#25:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_26
.LBB2_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB2_26:
	movb	$2, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movq	%rbx, 80(%r15)
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r13
	testq	%r13, %r13
	je	.LBB2_27
# BB#28:
	movq	%r13, zz_hold(%rip)
	movq	(%r13), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_29
.LBB2_27:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r13
	movq	%r13, zz_hold(%rip)
.LBB2_29:
	movb	$10, 32(%r13)
	movq	%r13, 24(%r13)
	movq	%r13, 16(%r13)
	movq	%r13, 8(%r13)
	movq	%r13, (%r13)
	movl	$144, %esi
	movq	%rbx, %rdi
	callq	ChildSym
	movq	%rax, 80(%r13)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_30
# BB#31:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_32
.LBB2_30:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_32:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB2_35
# BB#33:
	testq	%rax, %rax
	je	.LBB2_35
# BB#34:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_35:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB2_38
# BB#36:
	testq	%rax, %rax
	je	.LBB2_38
# BB#37:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_38:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_39
# BB#40:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_41
.LBB2_39:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_41:
	testq	%r13, %r13
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	je	.LBB2_44
# BB#42:
	testq	%rax, %rax
	je	.LBB2_44
# BB#43:
	movq	(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r13)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_44:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB2_47
# BB#45:
	testq	%rax, %rax
	je	.LBB2_47
# BB#46:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_47:
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB2_48
# BB#49:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_50
.LBB2_48:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB2_50:
	movb	$10, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movl	$146, %esi
	movq	%rbx, %rdi
	callq	ChildSym
	movq	%rax, 80(%r12)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_51
# BB#52:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_53
.LBB2_51:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_53:
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB2_56
# BB#54:
	testq	%rax, %rax
	je	.LBB2_56
# BB#55:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_56:
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB2_59
# BB#57:
	testq	%rax, %rax
	je	.LBB2_59
# BB#58:
	movq	16(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r12)
	movq	16(%rax), %rdx
	movq	%r12, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_59:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_60
# BB#61:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_62
.LBB2_60:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_62:
	testq	%r12, %r12
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	je	.LBB2_65
# BB#63:
	testq	%rax, %rax
	je	.LBB2_65
# BB#64:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_65:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB2_68
# BB#66:
	testq	%rax, %rax
	je	.LBB2_68
# BB#67:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_68:
	movzbl	zz_lengths+81(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB2_69
# BB#70:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_71
.LBB2_69:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB2_71:
	movb	$81, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_72
# BB#73:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_74
.LBB2_72:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_74:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB2_77
# BB#75:
	testq	%rax, %rax
	je	.LBB2_77
# BB#76:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_77:
	testq	%r15, %r15
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB2_80
# BB#78:
	testq	%rax, %rax
	je	.LBB2_80
# BB#79:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_80:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_81
# BB#82:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_83
.LBB2_81:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_83:
	testq	%r12, %r12
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	je	.LBB2_86
# BB#84:
	testq	%rax, %rax
	je	.LBB2_86
# BB#85:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_86:
	movq	%rax, zz_res(%rip)
	movq	16(%rsp), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_89
# BB#87:
	testq	%rcx, %rcx
	je	.LBB2_89
# BB#88:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB2_89:
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	BuildMergeTree, .Lfunc_end2-BuildMergeTree
	.cfi_endproc

	.globl	BuildEnclose
	.p2align	4, 0x90
	.type	BuildEnclose,@function
BuildEnclose:                           # @BuildEnclose
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 48
.Lcfi38:
	.cfi_offset %rbx, -48
.Lcfi39:
	.cfi_offset %r12, -40
.Lcfi40:
	.cfi_offset %r13, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	80(%r14), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_1
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader123
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	movq	%rcx, %r13
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%r13), %r13
	cmpb	$0, 32(%r13)
	je	.LBB3_4
# BB#5:                                 #   in Loop: Header=BB3_3 Depth=1
	testb	$16, 126(%r13)
	jne	.LBB3_2
# BB#6:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	8(%rcx), %rcx
	cmpq	%rax, %rcx
	jne	.LBB3_3
	jmp	.LBB3_7
.LBB3_1:
                                        # implicit-def: %R13
.LBB3_2:
	cmpq	%rax, %rcx
	jne	.LBB3_8
.LBB3_7:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.10, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_8:
	movq	8(%r13), %r15
	cmpq	%r13, %r15
	je	.LBB3_19
# BB#9:                                 # %.preheader.lr.ph
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB3_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_11 Depth 2
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB3_11:                               #   Parent Loop BB3_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_11
# BB#12:                                #   in Loop: Header=BB3_10 Depth=1
	leaq	32(%rbx), %r8
	movl	%eax, %ecx
	addb	$112, %cl
	cmpb	$2, %cl
	jae	.LBB3_13
# BB#70:                                #   in Loop: Header=BB3_10 Depth=1
	movl	$44, %edi
	movl	$1, %esi
	movl	$.L.str.11, %edx
	movl	$1, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB3_17
	.p2align	4, 0x90
.LBB3_13:                               #   in Loop: Header=BB3_10 Depth=1
	cmpb	$-110, %al
	jne	.LBB3_17
# BB#14:                                #   in Loop: Header=BB3_10 Depth=1
	movzwl	41(%r13), %eax
	testb	$1, %ah
	je	.LBB3_16
# BB#15:                                #   in Loop: Header=BB3_10 Depth=1
	movl	$44, %edi
	movl	$2, %esi
	movl	$.L.str.13, %edx
	movl	$1, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_16:                               # %.loopexit122
                                        #   in Loop: Header=BB3_10 Depth=1
	movq	%rbx, %r12
.LBB3_17:                               # %.loopexit122
                                        #   in Loop: Header=BB3_10 Depth=1
	movq	8(%r15), %r15
	cmpq	%r13, %r15
	jne	.LBB3_10
# BB#18:                                # %._crit_edge
	testq	%r12, %r12
	jne	.LBB3_20
.LBB3_19:                               # %._crit_edge.thread
	leaq	32(%r13), %r8
	xorl	%r12d, %r12d
	movl	$44, %edi
	movl	$3, %esi
	movl	$.L.str.14, %edx
	movl	$1, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_20:
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB3_21
# BB#22:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_23
.LBB3_21:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB3_23:
	movb	$2, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%r15)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r15), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r15)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r15)
	movq	%r13, 80(%r15)
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_24
# BB#25:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_26
.LBB3_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB3_26:
	movb	$10, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	34(%r14), %eax
	movw	%ax, 34(%rbx)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r14), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbx), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbx)
	andl	36(%r14), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbx)
	movq	%r12, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_27
# BB#28:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_29
.LBB3_27:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_29:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB3_32
# BB#30:
	testq	%rax, %rax
	je	.LBB3_32
# BB#31:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_32:
	leaq	32(%r14), %r12
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_35
# BB#33:
	testq	%rax, %rax
	je	.LBB3_35
# BB#34:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_35:
	movl	$11, %edi
	movl	$.L.str.16, %esi
	movq	%r12, %rdx
	callq	MakeWord
	movq	%rax, %r13
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_36
# BB#37:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_38
.LBB3_36:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_38:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB3_41
# BB#39:
	testq	%rax, %rax
	je	.LBB3_41
# BB#40:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_41:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%r13, %r13
	je	.LBB3_44
# BB#42:
	testq	%rax, %rax
	je	.LBB3_44
# BB#43:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_44:
	movq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB3_45:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_45
# BB#46:
	cmpb	$2, %al
	je	.LBB3_48
# BB#47:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.17, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_48:                               # %.loopexit
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	CopyObject
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	SetEnv
	movq	%rax, %r14
	movzbl	zz_lengths+81(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB3_49
# BB#50:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_51
.LBB3_49:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB3_51:
	movb	$81, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_52
# BB#53:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_54
.LBB3_52:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_54:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_57
# BB#55:
	testq	%rax, %rax
	je	.LBB3_57
# BB#56:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_57:
	testq	%r15, %r15
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB3_60
# BB#58:
	testq	%rax, %rax
	je	.LBB3_60
# BB#59:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_60:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_61
# BB#62:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_63
.LBB3_61:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_63:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB3_66
# BB#64:
	testq	%rax, %rax
	je	.LBB3_66
# BB#65:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_66:
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB3_69
# BB#67:
	testq	%rax, %rax
	je	.LBB3_69
# BB#68:
	movq	16(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r14)
	movq	16(%rax), %rdx
	movq	%r14, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_69:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	BuildEnclose, .Lfunc_end3-BuildEnclose
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"assert failed in %s"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"VerticalHyphenate: type(y) != HCAT!"
	.size	.L.str.1, 36

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"TransferLinks: start_link!"
	.size	.L.str.2, 27

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"FirstDefiniteCompressed!"
	.size	.L.str.3, 25

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"NextDefiniteWithGap: g == nilobj!"
	.size	.L.str.4, 34

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"NDWGC!"
	.size	.L.str.5, 7

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"FirstDefiniteWithGapCompressed: mode(gap(g))!"
	.size	.L.str.6, 46

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"MoveIndexes: is_index!"
	.size	.L.str.7, 23

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ConvertGalleyList: x!"
	.size	.L.str.8, 22

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"ConvertGalleyList: y!"
	.size	.L.str.9, 22

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"BuildEnclose: no enclose!"
	.size	.L.str.10, 26

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%s may not have a left or named parameter"
	.size	.L.str.11, 42

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"@Enclose"
	.size	.L.str.12, 9

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%s may not have a body parameter"
	.size	.L.str.13, 33

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%s must have a right parameter"
	.size	.L.str.14, 31

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"??"
	.size	.L.str.16, 3

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"BuildEnclose:  hd child!"
	.size	.L.str.17, 25

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"EncloseInHCat: Up(nxt) == nxt!"
	.size	.L.str.18, 31

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"EncloseInHCat: Up(nxt) != nxt!"
	.size	.L.str.19, 31

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"EncloseInHcat: type(s2) != SPLIT!"
	.size	.L.str.20, 34

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.zero	1
	.size	.L.str.21, 1

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"assert failed in %s %s"
	.size	.L.str.22, 23

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"FindTarget: unknown index"
	.size	.L.str.23, 26

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"BuildMergeTree: has_m!"
	.size	.L.str.24, 23

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"BuildMergeTree: y!"
	.size	.L.str.25, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
