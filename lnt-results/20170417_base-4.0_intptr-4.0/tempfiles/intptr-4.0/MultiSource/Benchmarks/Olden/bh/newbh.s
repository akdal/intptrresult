	.text
	.file	"newbh.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	xorl	%eax, %eax
	callq	dealwithargs
	movl	nbody(%rip), %esi
	movl	NumNodes(%rip), %edx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	callq	old_main
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.quad	-4611686018427387904    # double -2
	.quad	-4611686018427387904    # double -2
.LCPI1_1:
	.quad	-4611686018427387904    # double -2
	.quad	4616189618054758400     # double 4
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_2:
	.quad	4607182418800017408     # double 1
.LCPI1_3:
	.quad	4742290407621132288     # double 1073741824
.LCPI1_4:
	.quad	4578359381184846234     # double 0.012500000000000001
.LCPI1_5:
	.quad	4611688833177155011     # double 2.0012500000000002
	.text
	.globl	old_main
	.p2align	4, 0x90
	.type	old_main,@function
old_main:                               # @old_main
	.cfi_startproc
# BB#0:                                 # %.preheader157.preheader
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$888, %rsp              # imm = 0x378
.Lcfi7:
	.cfi_def_cfa_offset 944
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	NumNodes(%rip)
	movl	%eax, %r14d
	movl	$123, %edi
	callq	srand
	movl	$1064, %edi             # imm = 0x428
	callq	malloc
	movq	%rax, %r12
	movq	$0, 32(%r12)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [-2.000000e+00,-2.000000e+00]
	movups	%xmm0, (%r12)
	movapd	.LCPI1_1(%rip), %xmm0   # xmm0 = [-2.000000e+00,4.000000e+00]
	movupd	%xmm0, 16(%r12)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	leaq	368(%rsp), %r15
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rbx
	movl	$32, %eax
	xorl	%edx, %edx
	idivl	NumNodes(%rip)
	movl	%eax, %ecx
	movl	%ebx, %eax
	cltd
	idivl	%ecx
	movl	nbody(%rip), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$27, %edx
	addl	%ecx, %edx
	sarl	$5, %edx
	leaq	1(%rbx), %rbp
	movq	%r15, %rdi
	movl	%eax, %esi
	movl	%ebp, %ecx
	callq	uniform_testdata
	movq	416(%rsp), %rax
	movq	%rax, 40(%r12,%rbx,8)
	testq	%r13, %r13
	je	.LBB1_3
# BB#2:                                 #   in Loop: Header=BB1_1 Depth=1
	movq	%rax, 128(%r13)
.LBB1_3:                                # %.preheader155.preheader189
                                        #   in Loop: Header=BB1_1 Depth=1
	movq	424(%rsp), %r13
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	addpd	368(%rsp), %xmm0
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	384(%rsp), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movupd	392(%rsp), %xmm0
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	addpd	%xmm0, %xmm1
	movapd	%xmm1, 48(%rsp)         # 16-byte Spill
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	408(%rsp), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	cmpq	$32, %rbp
	jne	.LBB1_1
# BB#4:                                 # %.preheader154186
	movl	$.Lstr, %edi
	callq	puts
	movl	nbody(%rip), %ebp
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 352(%rsp)
	movapd	%xmm0, 336(%rsp)
	movapd	%xmm0, 320(%rsp)
	movapd	%xmm0, 304(%rsp)
	movapd	%xmm0, 288(%rsp)
	movapd	%xmm0, 272(%rsp)
	movapd	%xmm0, 256(%rsp)
	movapd	%xmm0, 240(%rsp)
	movapd	%xmm0, 224(%rsp)
	movapd	%xmm0, 208(%rsp)
	movapd	%xmm0, 192(%rsp)
	movapd	%xmm0, 176(%rsp)
	movapd	%xmm0, 160(%rsp)
	movapd	%xmm0, 144(%rsp)
	movapd	%xmm0, 128(%rsp)
	movapd	%xmm0, 112(%rsp)
	leaq	368(%rsp), %rdi
	xorl	%esi, %esi
	movl	$512, %edx              # imm = 0x200
	callq	memset
	movq	40(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB1_11
# BB#5:                                 # %.preheader152.lr.ph
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	divpd	%xmm0, %xmm1
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movapd	48(%rsp), %xmm1         # 16-byte Reload
	divpd	%xmm0, %xmm1
	movapd	%xmm1, 48(%rsp)         # 16-byte Spill
	movsd	24(%r12), %xmm1         # xmm1 = mem[0],zero
	movsd	(%r12), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	movsd	8(%r12), %xmm0          # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movsd	16(%r12), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	xorps	%xmm2, %xmm2
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader152
                                        # =>This Inner Loop Header: Depth=1
	movupd	16(%rbx), %xmm5
	subpd	16(%rsp), %xmm5         # 16-byte Folded Reload
	movupd	%xmm5, 16(%rbx)
	movsd	32(%rbx), %xmm4         # xmm4 = mem[0],zero
	subsd	(%rsp), %xmm4           # 8-byte Folded Reload
	movsd	%xmm4, 32(%rbx)
	movupd	48(%rbx), %xmm0
	subpd	48(%rsp), %xmm0         # 16-byte Folded Reload
	movupd	%xmm0, 48(%rbx)
	movsd	64(%rbx), %xmm0         # xmm0 = mem[0],zero
	subsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movsd	%xmm0, 64(%rbx)
	movapd	%xmm5, %xmm0
	subsd	88(%rsp), %xmm0         # 8-byte Folded Reload
	divsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	movl	$0, %r15d
	movsd	%xmm4, 64(%rsp)         # 8-byte Spill
	jb	.LBB1_9
# BB#7:                                 # %.preheader152
                                        #   in Loop: Header=BB1_6 Depth=1
	movsd	.LCPI1_2(%rip), %xmm3   # xmm3 = mem[0],zero
	ucomisd	%xmm0, %xmm3
	movl	$0, %r15d
	jbe	.LBB1_9
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	mulsd	.LCPI1_3(%rip), %xmm0
	movapd	%xmm5, 96(%rsp)         # 16-byte Spill
	callq	floor
	movapd	96(%rsp), %xmm5         # 16-byte Reload
	movsd	64(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	xorps	%xmm2, %xmm2
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvttsd2si	%xmm0, %r15d
.LBB1_9:                                #   in Loop: Header=BB1_6 Depth=1
	movhlps	%xmm5, %xmm5            # xmm5 = xmm5[1,1]
	subsd	80(%rsp), %xmm5         # 8-byte Folded Reload
	divsd	%xmm1, %xmm5
	ucomisd	%xmm2, %xmm5
	jb	.LBB1_10
# BB#18:                                #   in Loop: Header=BB1_6 Depth=1
	movsd	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm5, %xmm0
	movl	$0, %ebp
	jbe	.LBB1_20
# BB#19:                                #   in Loop: Header=BB1_6 Depth=1
	mulsd	.LCPI1_3(%rip), %xmm5
	movapd	%xmm5, %xmm0
	callq	floor
	movsd	64(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	xorps	%xmm2, %xmm2
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvttsd2si	%xmm0, %ebp
	shlq	$32, %rbp
	jmp	.LBB1_20
	.p2align	4, 0x90
.LBB1_10:                               #   in Loop: Header=BB1_6 Depth=1
	xorl	%ebp, %ebp
.LBB1_20:                               #   in Loop: Header=BB1_6 Depth=1
	subsd	72(%rsp), %xmm4         # 8-byte Folded Reload
	divsd	%xmm1, %xmm4
	xorl	%eax, %eax
	ucomisd	%xmm2, %xmm4
	jb	.LBB1_23
# BB#21:                                #   in Loop: Header=BB1_6 Depth=1
	movsd	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm4, %xmm0
	jbe	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_6 Depth=1
	mulsd	.LCPI1_3(%rip), %xmm4
	movapd	%xmm4, %xmm0
	callq	floor
	xorps	%xmm2, %xmm2
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvttsd2si	%xmm0, %eax
.LBB1_23:                               # %intcoord.exit
                                        #   in Loop: Header=BB1_6 Depth=1
	movl	%r15d, %ecx
	shrl	$27, %ecx
	andl	$4, %ecx
	movq	%rbp, %rdx
	shrq	$60, %rdx
	andl	$2, %edx
	orl	%ecx, %edx
	movl	%eax, %ecx
	shrl	$29, %ecx
	andl	$1, %ecx
	orl	%edx, %ecx
	shrl	$26, %r15d
	andl	$4, %r15d
	shrq	$59, %rbp
	andl	$2, %ebp
	orl	%r15d, %ebp
	shrl	$28, %eax
	andl	$1, %eax
	orl	%ebp, %eax
	leal	(%rax,%rcx,8), %eax
	xorl	%edx, %edx
	idivl	%r14d
	cltq
	incl	112(%rsp,%rax,4)
	movq	368(%rsp,%rax,8), %rcx
	movq	%rcx, 136(%rbx)
	movq	%rbx, 368(%rsp,%rax,8)
	movl	%eax, 40(%rbx)
	movq	128(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_6
.LBB1_11:                               # %.preheader150
	cmpl	$0, NumNodes(%rip)
	jle	.LBB1_14
# BB#12:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	movl	112(%rsp,%rbx,4), %edx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	368(%rsp,%rbx,8), %rax
	movq	%rax, 552(%r12,%rbx,8)
	incq	%rbx
	movslq	NumNodes(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_13
.LBB1_14:                               # %.preheader.preheader
	xorl	%ebx, %ebx
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_15:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r12, %rdi
	movl	%ebx, %esi
	callq	stepsystem
	incl	%ebx
	cmpl	$9, %ebx
	jg	.LBB1_17
# BB#16:                                # %.preheader
                                        #   in Loop: Header=BB1_15 Depth=1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	addsd	.LCPI1_4(%rip), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movsd	.LCPI1_5(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB1_15
.LBB1_17:
	movq	%r12, %rax
	addq	$888, %rsp              # imm = 0x378
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	old_main, .Lfunc_end1-old_main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4638355772470722560     # double 123
.LCPI2_1:
	.quad	4607182418800017408     # double 1
.LCPI2_2:
	.quad	4607173411600762667     # double 0.99899999999999999
.LCPI2_3:
	.quad	-4619192017806338731    # double -0.66666666666666663
.LCPI2_4:
	.quad	-4616189618054758400    # double -1
.LCPI2_5:
	.quad	4616189618054758400     # double 4
.LCPI2_6:
	.quad	4591870180066957722     # double 0.10000000000000001
.LCPI2_7:
	.quad	4615063718147915776     # double 3.5
.LCPI2_8:
	.quad	4609047870845172685     # double 1.4142135623730951
.LCPI2_9:
	.quad	4598175219545276416     # double 0.25
.LCPI2_10:
	.quad	4608546739414082322     # double 1.3029400317411199
.LCPI2_11:
	.quad	0                       # double 0
	.text
	.globl	uniform_testdata
	.p2align	4, 0x90
	.type	uniform_testdata,@function
uniform_testdata:                       # @uniform_testdata
	.cfi_startproc
# BB#0:                                 # %.preheader146.preheader
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 256
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movl	%edx, %r12d
	movl	%esi, %r13d
	movq	%rdi, %rbx
	xorpd	%xmm1, %xmm1
	movapd	%xmm1, 144(%rsp)
	movapd	%xmm1, 128(%rsp)
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 48(%rsp)         # 16-byte Spill
	movapd	%xmm1, 112(%rsp)
	movl	$144, %edi
	callq	malloc
	movq	%rax, %r15
	movw	$1, (%r15)
	movl	%r13d, 40(%r15)
	movq	$0, 136(%r15)
	movl	%r13d, 44(%r15)
	testl	%r12d, %r12d
	jle	.LBB2_1
# BB#2:                                 # %.lr.ph
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	cvtsi2sdl	%ebp, %xmm3
	mulsd	.LCPI2_0(%rip), %xmm3
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 72(%rsp)         # 8-byte Spill
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 40(%rsp)         # 8-byte Spill
	movq	%r15, %r14
	xorl	%ebx, %ebx
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB2_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
                                        #     Child Loop BB2_10 Depth 2
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	movapd	%xmm2, 176(%rsp)        # 16-byte Spill
	movsd	%xmm1, 88(%rsp)         # 8-byte Spill
	movl	$144, %edi
	callq	malloc
	movq	%rax, %rbp
	movw	$1, (%rbp)
	movl	%r13d, 40(%rbp)
	movq	$0, 136(%rbp)
	movl	%r13d, 44(%rbp)
	testq	%rbp, %rbp
	jne	.LBB2_5
# BB#4:                                 #   in Loop: Header=BB2_3 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	error
.LBB2_5:                                # %.preheader145168
                                        #   in Loop: Header=BB2_3 Depth=1
	movq	%rbp, 128(%r14)
	movw	$1, (%rbp)
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rbp)
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	xorpd	%xmm0, %xmm0
	movb	$3, %al
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	xrand
	movsd	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	addsd	.LCPI2_4(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB2_7
# BB#6:                                 # %call.sqrt
                                        #   in Loop: Header=BB2_3 Depth=1
	callq	sqrt
.LBB2_7:                                # %.preheader145168.split
                                        #   in Loop: Header=BB2_3 Depth=1
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movb	$3, %al
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	xrand
	movsd	.LCPI2_5(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, 16(%rbp)
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movb	$3, %al
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	xrand
	mulsd	.LCPI2_5(%rip), %xmm0
	movsd	%xmm0, 24(%rbp)
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	xorpd	%xmm0, %xmm0
	movb	$3, %al
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	xrand
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	mulsd	.LCPI2_5(%rip), %xmm0
	movsd	%xmm0, 32(%rbp)
	movupd	16(%rbp), %xmm1
	movapd	48(%rsp), %xmm2         # 16-byte Reload
	addpd	%xmm1, %xmm2
	movapd	%xmm2, 48(%rsp)         # 16-byte Spill
	movsd	40(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 40(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB2_8:                                # %.preheader144
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movb	$3, %al
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	xrand
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	xorpd	%xmm0, %xmm0
	movb	$3, %al
	movsd	.LCPI2_6(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	xrand
	movsd	%xmm0, 104(%rsp)        # 8-byte Spill
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	movapd	%xmm0, %xmm1
	mulsd	%xmm1, %xmm1
	movsd	%xmm1, 96(%rsp)         # 8-byte Spill
	movsd	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	.LCPI2_7(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	mulsd	96(%rsp), %xmm0         # 8-byte Folded Reload
	movsd	104(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	ja	.LBB2_8
# BB#9:                                 #   in Loop: Header=BB2_3 Depth=1
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	mulsd	.LCPI2_8(%rip), %xmm0
	movapd	%xmm0, 16(%rsp)         # 16-byte Spill
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	.LCPI2_1(%rip), %xmm0
	movsd	.LCPI2_9(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movapd	16(%rsp), %xmm1         # 16-byte Reload
	divsd	%xmm0, %xmm1
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	.p2align	4, 0x90
.LBB2_10:                               # %.preheader170
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movb	$3, %al
	movsd	.LCPI2_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	xrand
	movsd	%xmm0, 48(%rbp)
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movb	$3, %al
	movsd	.LCPI2_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	xrand
	movsd	%xmm0, 56(%rbp)
	movb	$1, %al
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	my_rand
	movapd	%xmm0, %xmm2
	movb	$3, %al
	movsd	.LCPI2_4(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI2_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	xrand
	movsd	%xmm0, 64(%rbp)
	movsd	48(%rbp), %xmm1         # xmm1 = mem[0],zero
	movsd	56(%rbp), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm1
	addsd	.LCPI2_11, %xmm1
	mulsd	%xmm2, %xmm2
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	ucomisd	.LCPI2_1(%rip), %xmm0
	ja	.LBB2_10
# BB#11:                                # %.preheader143.preheader171
                                        #   in Loop: Header=BB2_3 Depth=1
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	mulsd	.LCPI2_10(%rip), %xmm4
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	jnp	.LBB2_13
# BB#12:                                # %call.sqrt213
                                        #   in Loop: Header=BB2_3 Depth=1
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	%xmm0, %xmm1
.LBB2_13:                               # %.preheader143.preheader171.split
                                        #   in Loop: Header=BB2_3 Depth=1
	divsd	%xmm1, %xmm4
	movupd	48(%rbp), %xmm0
	movapd	%xmm4, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	movupd	%xmm1, 48(%rbp)
	mulsd	64(%rbp), %xmm4
	movsd	%xmm4, 64(%rbp)
	movapd	176(%rsp), %xmm2        # 16-byte Reload
	addpd	%xmm1, %xmm2
	movsd	88(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm4, %xmm1
	incl	%ebx
	cmpl	%r12d, %ebx
	movq	%rbp, %r14
	jne	.LBB2_3
# BB#14:                                # %._crit_edge.loopexit
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	%xmm0, 112(%rsp)
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 128(%rsp)
	movupd	%xmm2, 136(%rsp)
	movsd	%xmm1, 152(%rsp)
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB2_15
.LBB2_1:
	movq	%r15, %rbp
.LBB2_15:                               # %._crit_edge
	movq	$0, 128(%rbp)
	movq	128(%r15), %rax
	movq	%rax, 160(%rsp)
	movq	%rbp, 168(%rsp)
	movaps	112(%rsp), %xmm0
	movaps	128(%rsp), %xmm1
	movaps	144(%rsp), %xmm2
	movaps	160(%rsp), %xmm3
	movups	%xmm3, 48(%rbx)
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rbx, %rax
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	uniform_testdata, .Lfunc_end2-uniform_testdata
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4607182418800017408     # double 1
.LCPI3_1:
	.quad	4742290407621132288     # double 1073741824
	.text
	.globl	intcoord
	.p2align	4, 0x90
	.type	intcoord,@function
intcoord:                               # @intcoord
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -48
.Lcfi34:
	.cfi_offset %r12, -40
.Lcfi35:
	.cfi_offset %r13, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movsd	24(%r14), %xmm3         # xmm3 = mem[0],zero
	movsd	16(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rdi), %xmm4         # xmm4 = mem[0],zero
	movsd	32(%rdi), %xmm2         # xmm2 = mem[0],zero
	subsd	(%r14), %xmm0
	divsd	%xmm3, %xmm0
	xorl	%r12d, %r12d
	xorps	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm0
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	jb	.LBB3_1
# BB#2:
	movsd	.LCPI3_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movl	$0, %r15d
	jbe	.LBB3_4
# BB#3:
	mulsd	.LCPI3_1(%rip), %xmm0
	movsd	%xmm4, 24(%rsp)         # 8-byte Spill
	callq	floor
	xorps	%xmm5, %xmm5
	movsd	24(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvttsd2si	%xmm0, %r15d
	movabsq	$4294967296, %r12       # imm = 0x100000000
	jmp	.LBB3_4
.LBB3_1:
	xorl	%r15d, %r15d
.LBB3_4:
	subsd	8(%r14), %xmm4
	divsd	%xmm3, %xmm4
	xorl	%r13d, %r13d
	ucomisd	%xmm5, %xmm4
	jb	.LBB3_5
# BB#6:
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm4, %xmm0
	movl	$0, %ebx
	jbe	.LBB3_8
# BB#7:
	mulsd	.LCPI3_1(%rip), %xmm4
	movapd	%xmm4, %xmm0
	callq	floor
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvttsd2si	%xmm0, %ebx
	shlq	$32, %rbx
	movq	%r12, %r13
	jmp	.LBB3_8
.LBB3_5:
	xorl	%ebx, %ebx
.LBB3_8:
	subsd	16(%r14), %xmm2
	divsd	%xmm3, %xmm2
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jb	.LBB3_9
# BB#10:
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	movl	$0, %ecx
	jbe	.LBB3_12
# BB#11:
	mulsd	.LCPI3_1(%rip), %xmm2
	movapd	%xmm2, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %ecx
	movq	%r13, %rax
	jmp	.LBB3_12
.LBB3_9:
	xorl	%ecx, %ecx
.LBB3_12:
	orq	%r15, %rbx
	movabsq	$281470681743360, %rdx  # imm = 0xFFFF00000000
	andq	%rax, %rdx
	orq	%rcx, %rdx
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	intcoord, .Lfunc_end3-intcoord
	.cfi_endproc

	.globl	old_subindex
	.p2align	4, 0x90
	.type	old_subindex,@function
old_subindex:                           # @old_subindex
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	shrq	$32, %rax
	xorl	%ecx, %ecx
	testl	%edx, %edi
	setne	%cl
	leal	(,%rcx,4), %edi
	testl	%edx, %eax
	leal	2(,%rcx,4), %ecx
	cmovel	%edi, %ecx
	xorl	%eax, %eax
	testl	%edx, %esi
	setne	%al
	orl	%ecx, %eax
	retq
.Lfunc_end4:
	.size	old_subindex, .Lfunc_end4-old_subindex
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	stepsystem
	.p2align	4, 0x90
	.type	stepsystem,@function
stepsystem:                             # @stepsystem
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 272
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r15
	movq	32(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB5_2
# BB#1:
	callq	freetree
	movq	$0, 32(%r15)
.LBB5_2:
	movl	nbody(%rip), %esi
	xorl	%r8d, %r8d
	movq	%r15, %rdx
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	movl	%ebx, %ecx
	callq	maketree
	movq	%rax, %rbp
	movq	%rbp, 32(%r15)
	movslq	NumNodes(%rip), %r12
	testq	%r12, %r12
	jle	.LBB5_9
# BB#3:                                 # %.lr.ph.i
	movsd	24(%r15), %xmm0         # xmm0 = mem[0],zero
	leaq	88(%rsp), %rbx
	mulsd	%xmm0, %xmm0
	xorps	%xmm2, %xmm2
	leaq	152(%rsp), %r13
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	jmp	.LBB5_4
	.p2align	4, 0x90
.LBB5_7:                                # %grav.exit.backedge.i._crit_edge
                                        #   in Loop: Header=BB5_4 Depth=1
	decq	%r12
	movq	32(%r15), %rbp
.LBB5_4:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_8 Depth 2
	movq	544(%r15,%r12,8), %r14
	testq	%r14, %r14
	je	.LBB5_6
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph.i.i
                                        #   Parent Loop BB5_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, 80(%rsp)
	movq	32(%r14), %rax
	movq	%rax, 16(%rbx)
	movups	16(%r14), %xmm3
	movups	%xmm3, (%rbx)
	movups	%xmm2, 40(%rbx)
	movups	%xmm2, 24(%rbx)
	movaps	80(%rsp), %xmm4
	movaps	96(%rsp), %xmm5
	movaps	112(%rsp), %xmm2
	movaps	128(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm5, 16(%rsp)
	movups	%xmm4, (%rsp)
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	walksub
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movups	152(%rsp), %xmm4
	movups	168(%rsp), %xmm5
	movups	184(%rsp), %xmm2
	movups	200(%rsp), %xmm3
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm2, 112(%rsp)
	xorps	%xmm2, %xmm2
	movaps	%xmm5, 96(%rsp)
	movaps	%xmm4, 80(%rsp)
	movq	112(%rsp), %rax
	movq	%rax, 120(%r14)
	movq	48(%rbx), %rax
	movq	%rax, 112(%r14)
	movups	32(%rbx), %xmm3
	movups	%xmm3, 96(%r14)
	movq	136(%r14), %r14
	testq	%r14, %r14
	jne	.LBB5_8
.LBB5_6:                                # %grav.exit.backedge.i
                                        #   in Loop: Header=BB5_4 Depth=1
	cmpq	$2, %r12
	jge	.LBB5_7
.LBB5_9:                                # %computegrav.exit
	movq	552(%r15), %rdi
	movl	68(%rsp), %esi          # 4-byte Reload
	callq	vp
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	stepsystem, .Lfunc_end5-stepsystem
	.cfi_endproc

	.globl	testdata
	.p2align	4, 0x90
	.type	testdata,@function
testdata:                               # @testdata
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movl	$.L.str.3, %edi
	movl	$99, %esi
	xorl	%eax, %eax
	callq	printf
	callq	abort
.Lfunc_end6:
	.size	testdata, .Lfunc_end6-testdata
	.cfi_endproc

	.globl	freetree1
	.p2align	4, 0x90
	.type	freetree1,@function
freetree1:                              # @freetree1
	.cfi_startproc
# BB#0:
	jmp	freetree                # TAILCALL
.Lfunc_end7:
	.size	freetree1, .Lfunc_end7-freetree1
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4607182418800017408     # double 1
.LCPI8_1:
	.quad	4742290407621132288     # double 1073741824
.LCPI8_2:
	.quad	0                       # double 0
	.text
	.globl	maketree
	.p2align	4, 0x90
	.type	maketree,@function
maketree:                               # @maketree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 96
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	$0, 32(%r14)
	movl	%esi, nbody(%rip)
	movslq	NumNodes(%rip), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	jle	.LBB8_1
	.p2align	4, 0x90
.LBB8_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_19 Depth 2
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	544(%r14,%rcx,8), %rbx
	jmp	.LBB8_19
	.p2align	4, 0x90
.LBB8_18:                               #   in Loop: Header=BB8_19 Depth=2
	movq	136(%rbx), %rbx
.LBB8_19:                               #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rbx, %rbx
	je	.LBB8_2
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB8_19 Depth=2
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	ucomisd	.LCPI8_2, %xmm0
	jne	.LBB8_5
	jnp	.LBB8_18
.LBB8_5:                                #   in Loop: Header=BB8_19 Depth=2
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	expandbox
	movsd	24(%r14), %xmm3         # xmm3 = mem[0],zero
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rbx), %xmm4         # xmm4 = mem[0],zero
	movsd	32(%rbx), %xmm2         # xmm2 = mem[0],zero
	subsd	(%r14), %xmm0
	divsd	%xmm3, %xmm0
	ucomisd	.LCPI8_2, %xmm0
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	jb	.LBB8_6
# BB#7:                                 #   in Loop: Header=BB8_19 Depth=2
	movsd	.LCPI8_0(%rip), %xmm1   # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	movl	$0, %r12d
	movl	$0, %r13d
	jbe	.LBB8_9
# BB#8:                                 #   in Loop: Header=BB8_19 Depth=2
	mulsd	.LCPI8_1(%rip), %xmm0
	movsd	%xmm4, 32(%rsp)         # 8-byte Spill
	callq	floor
	movsd	32(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvttsd2si	%xmm0, %r13d
	movabsq	$4294967296, %r12       # imm = 0x100000000
	jmp	.LBB8_9
.LBB8_6:                                #   in Loop: Header=BB8_19 Depth=2
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.LBB8_9:                                #   in Loop: Header=BB8_19 Depth=2
	subsd	8(%r14), %xmm4
	divsd	%xmm3, %xmm4
	ucomisd	.LCPI8_2, %xmm4
	jb	.LBB8_10
# BB#11:                                #   in Loop: Header=BB8_19 Depth=2
	movsd	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm4, %xmm0
	movl	$0, %ebp
	movl	$0, %r15d
	jbe	.LBB8_13
# BB#12:                                #   in Loop: Header=BB8_19 Depth=2
	mulsd	.LCPI8_1(%rip), %xmm4
	movapd	%xmm4, %xmm0
	callq	floor
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvttsd2si	%xmm0, %r15d
	shlq	$32, %r15
	movq	%r12, %rbp
	jmp	.LBB8_13
.LBB8_10:                               #   in Loop: Header=BB8_19 Depth=2
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
.LBB8_13:                               #   in Loop: Header=BB8_19 Depth=2
	subsd	16(%r14), %xmm2
	divsd	%xmm3, %xmm2
	ucomisd	.LCPI8_2, %xmm2
	jb	.LBB8_14
# BB#15:                                #   in Loop: Header=BB8_19 Depth=2
	movsd	.LCPI8_0(%rip), %xmm0   # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	movl	$0, %edx
	movl	$0, %eax
	jbe	.LBB8_17
# BB#16:                                #   in Loop: Header=BB8_19 Depth=2
	mulsd	.LCPI8_1(%rip), %xmm2
	movapd	%xmm2, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	movq	%rbp, %rdx
	jmp	.LBB8_17
.LBB8_14:                               #   in Loop: Header=BB8_19 Depth=2
	xorl	%edx, %edx
	xorl	%eax, %eax
.LBB8_17:                               # %intcoord.exit
                                        #   in Loop: Header=BB8_19 Depth=2
	orq	%r13, %r15
	movabsq	$281470681743360, %rcx  # imm = 0xFFFF00000000
	andq	%rcx, %rdx
	orq	%rax, %rdx
	movq	32(%r14), %rcx
	movl	$536870912, %r8d        # imm = 0x20000000
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r14, %r9
	callq	loadtree
	movq	%rax, 32(%r14)
	jmp	.LBB8_18
	.p2align	4, 0x90
.LBB8_2:                                # %.loopexit
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpq	$2, %rcx
	leaq	-1(%rcx), %rcx
	jge	.LBB8_3
.LBB8_1:                                # %._crit_edge
	movq	%rax, %rdi
	callq	hackcofm
	movq	32(%r14), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	maketree, .Lfunc_end8-maketree
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	computegrav
	.p2align	4, 0x90
	.type	computegrav,@function
computegrav:                            # @computegrav
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 272
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movslq	NumNodes(%rip), %r12
	testq	%r12, %r12
	jle	.LBB9_6
# BB#1:                                 # %.lr.ph
	movsd	24(%r14), %xmm0         # xmm0 = mem[0],zero
	leaq	88(%rsp), %rbx
	mulsd	%xmm0, %xmm0
	xorps	%xmm2, %xmm2
	leaq	152(%rsp), %r15
	movsd	.LCPI9_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_4 Depth 2
	movq	544(%r14,%r12,8), %rbp
	testq	%rbp, %rbp
	je	.LBB9_5
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	32(%r14), %r13
	.p2align	4, 0x90
.LBB9_4:                                # %.lr.ph.i
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, 80(%rsp)
	movq	32(%rbp), %rax
	movq	%rax, 16(%rbx)
	movups	16(%rbp), %xmm3
	movups	%xmm3, (%rbx)
	movups	%xmm2, 40(%rbx)
	movups	%xmm2, 24(%rbx)
	movaps	80(%rsp), %xmm4
	movaps	96(%rsp), %xmm5
	movaps	112(%rsp), %xmm2
	movaps	128(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm5, 16(%rsp)
	movups	%xmm4, (%rsp)
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	walksub
	movsd	.LCPI9_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movups	152(%rsp), %xmm4
	movups	168(%rsp), %xmm5
	movups	184(%rsp), %xmm2
	movups	200(%rsp), %xmm3
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm2, 112(%rsp)
	xorps	%xmm2, %xmm2
	movaps	%xmm5, 96(%rsp)
	movaps	%xmm4, 80(%rsp)
	movq	112(%rsp), %rax
	movq	%rax, 120(%rbp)
	movq	48(%rbx), %rax
	movq	%rax, 112(%rbp)
	movups	32(%rbx), %xmm3
	movups	%xmm3, 96(%rbp)
	movq	136(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB9_4
.LBB9_5:                                # %grav.exit.backedge
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpq	$1, %r12
	leaq	-1(%r12), %r12
	jg	.LBB9_2
.LBB9_6:                                # %grav.exit._crit_edge
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	computegrav, .Lfunc_end9-computegrav
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI10_0:
	.quad	4573855781557475738     # double 0.0062500000000000003
	.quad	4573855781557475738     # double 0.0062500000000000003
.LCPI10_2:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
.LCPI10_5:
	.quad	4578359381184846234     # double 0.012500000000000001
	.quad	4578359381184846234     # double 0.012500000000000001
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_1:
	.quad	4573855781557475738     # double 0.0062500000000000003
.LCPI10_3:
	.quad	4621819117588971520     # double 10
.LCPI10_4:
	.quad	4666723172467343360     # double 1.0E+4
.LCPI10_6:
	.quad	4578359381184846234     # double 0.012500000000000001
	.text
	.globl	vp
	.p2align	4, 0x90
	.type	vp,@function
vp:                                     # @vp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 24
	subq	$216, %rsp
.Lcfi80:
	.cfi_def_cfa_offset 240
.Lcfi81:
	.cfi_offset %rbx, -24
.Lcfi82:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB10_2
	jmp	.LBB10_51
	.p2align	4, 0x90
.LBB10_50:                              #   in Loop: Header=BB10_2 Depth=1
	movq	136(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB10_51
.LBB10_2:                               # %.preheader178
                                        # =>This Inner Loop Header: Depth=1
	movq	112(%rbx), %rax
	movq	%rax, 128(%rsp)
	movups	96(%rbx), %xmm0
	movaps	%xmm0, 112(%rsp)
	testl	%ebp, %ebp
	jle	.LBB10_4
# BB#3:                                 # %.preheader177.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	movapd	112(%rsp), %xmm0
	movupd	48(%rbx), %xmm1
	movupd	72(%rbx), %xmm2
	subpd	%xmm2, %xmm0
	movsd	128(%rsp), %xmm2        # xmm2 = mem[0],zero
	subsd	88(%rbx), %xmm2
	mulpd	.LCPI10_0(%rip), %xmm0
	mulsd	.LCPI10_1(%rip), %xmm2
	addpd	%xmm1, %xmm0
	movapd	%xmm0, 80(%rsp)
	addsd	64(%rbx), %xmm2
	movsd	%xmm2, 96(%rsp)
	movq	96(%rsp), %rax
	movq	%rax, 64(%rbx)
	movaps	80(%rsp), %xmm0
	movups	%xmm0, 48(%rbx)
.LBB10_4:                               # %.loopexit
                                        #   in Loop: Header=BB10_2 Depth=1
	movupd	16(%rbx), %xmm1
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_5
# BB#7:                                 #   in Loop: Header=BB10_2 Depth=1
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_8
# BB#9:                                 #   in Loop: Header=BB10_2 Depth=1
	movapd	64(%rsp), %xmm0         # 16-byte Reload
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_10
# BB#11:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	movapd	.LCPI10_2(%rip), %xmm1  # xmm1 = [nan,nan]
	andpd	%xmm1, %xmm0
	movsd	.LCPI10_3(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB10_12
# BB#13:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	(%rsp), %xmm0           # 16-byte Reload
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm2
	jbe	.LBB10_14
# BB#15:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	64(%rsp), %xmm0         # 16-byte Reload
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm2
	jbe	.LBB10_52
# BB#16:                                # %.preheader173.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	movq	128(%rsp), %rax
	movq	%rax, 88(%rbx)
	movaps	112(%rsp), %xmm0
	movups	%xmm0, 72(%rbx)
	movupd	72(%rbx), %xmm1
	movsd	88(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movapd	%xmm1, (%rsp)           # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_17
# BB#18:                                #   in Loop: Header=BB10_2 Depth=1
	movaps	(%rsp), %xmm0           # 16-byte Reload
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_19
# BB#20:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_21
# BB#22:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	(%rsp), %xmm4           # 16-byte Reload
	movapd	%xmm4, %xmm0
	movapd	.LCPI10_2(%rip), %xmm1  # xmm1 = [nan,nan]
	andpd	%xmm1, %xmm0
	movsd	.LCPI10_4(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB10_23
# BB#24:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	48(%rsp), %xmm0         # 16-byte Reload
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm2
	movapd	16(%rsp), %xmm3         # 16-byte Reload
	jbe	.LBB10_25
# BB#26:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	%xmm3, %xmm0
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm2
	jbe	.LBB10_53
# BB#27:                                # %.preheader172.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	mulpd	.LCPI10_0(%rip), %xmm4
	movapd	%xmm4, (%rsp)           # 16-byte Spill
	movapd	%xmm4, 80(%rsp)
	mulsd	.LCPI10_1(%rip), %xmm3
	movapd	%xmm3, 16(%rsp)         # 16-byte Spill
	movsd	%xmm3, 96(%rsp)
	movupd	48(%rbx), %xmm1
	movsd	64(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movapd	%xmm1, 48(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_28
# BB#29:                                #   in Loop: Header=BB10_2 Depth=1
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_30
# BB#31:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	144(%rsp), %xmm0        # 16-byte Reload
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_32
# BB#33:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	48(%rsp), %xmm6         # 16-byte Reload
	movapd	%xmm6, %xmm0
	movapd	.LCPI10_2(%rip), %xmm1  # xmm1 = [nan,nan]
	andpd	%xmm1, %xmm0
	movsd	.LCPI10_4(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movapd	(%rsp), %xmm4           # 16-byte Reload
	jbe	.LBB10_34
# BB#35:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	192(%rsp), %xmm0        # 16-byte Reload
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm2
	movapd	144(%rsp), %xmm5        # 16-byte Reload
	jbe	.LBB10_36
# BB#37:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	%xmm5, %xmm0
	andpd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm2
	jbe	.LBB10_54
# BB#38:                                # %.preheader171.preheader
                                        #   in Loop: Header=BB10_2 Depth=1
	leaq	16(%rbx), %rax
	addpd	%xmm4, %xmm6
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	addsd	%xmm2, %xmm5
	movapd	%xmm6, %xmm0
	mulpd	.LCPI10_5(%rip), %xmm0
	movapd	%xmm5, %xmm1
	mulsd	.LCPI10_6(%rip), %xmm1
	addpd	%xmm0, %xmm3
	movapd	%xmm3, 160(%rsp)
	movapd	64(%rsp), %xmm0         # 16-byte Reload
	addsd	%xmm1, %xmm0
	movsd	%xmm0, 176(%rsp)
	movq	176(%rsp), %rcx
	movq	%rcx, 16(%rax)
	movaps	160(%rsp), %xmm0
	movups	%xmm0, (%rax)
	addpd	%xmm4, %xmm6
	movupd	%xmm6, 48(%rbx)
	addsd	%xmm2, %xmm5
	movsd	%xmm5, 64(%rbx)
	movsd	16(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	24(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movsd	32(%rbx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	movapd	%xmm1, %xmm0
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_39
# BB#40:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	(%rsp), %xmm0           # 16-byte Reload
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_41
# BB#42:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	callq	__isnan
	testl	%eax, %eax
	jne	.LBB10_43
# BB#44:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	.LCPI10_2(%rip), %xmm0  # xmm0 = [nan,nan]
	movapd	32(%rsp), %xmm2         # 16-byte Reload
	andpd	%xmm0, %xmm2
	movsd	.LCPI10_4(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm2, %xmm1
	movapd	(%rsp), %xmm2           # 16-byte Reload
	jbe	.LBB10_45
# BB#46:                                #   in Loop: Header=BB10_2 Depth=1
	andpd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm1
	jbe	.LBB10_47
# BB#48:                                #   in Loop: Header=BB10_2 Depth=1
	movapd	16(%rsp), %xmm2         # 16-byte Reload
	andpd	%xmm0, %xmm2
	ucomisd	%xmm2, %xmm1
	ja	.LBB10_50
# BB#49:
	movl	$.L.str.3, %edi
	movl	$64, %esi
	jmp	.LBB10_6
.LBB10_51:                              # %._crit_edge
	addq	$216, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB10_5:
	movl	$.L.str.3, %edi
	movl	$99, %esi
	jmp	.LBB10_6
.LBB10_8:
	movl	$.L.str.3, %edi
	movl	$98, %esi
	jmp	.LBB10_6
.LBB10_10:
	movl	$.L.str.3, %edi
	movl	$97, %esi
	jmp	.LBB10_6
.LBB10_12:
	movl	$.L.str.3, %edi
	movl	$96, %esi
	jmp	.LBB10_6
.LBB10_14:
	movl	$.L.str.3, %edi
	movl	$95, %esi
	jmp	.LBB10_6
.LBB10_52:
	movl	$.L.str.3, %edi
	movl	$94, %esi
	jmp	.LBB10_6
.LBB10_17:
	movl	$.L.str.3, %edi
	movl	$89, %esi
	jmp	.LBB10_6
.LBB10_19:
	movl	$.L.str.3, %edi
	movl	$88, %esi
	jmp	.LBB10_6
.LBB10_21:
	movl	$.L.str.3, %edi
	movl	$87, %esi
	jmp	.LBB10_6
.LBB10_23:
	movl	$.L.str.3, %edi
	movl	$86, %esi
	jmp	.LBB10_6
.LBB10_25:
	movl	$.L.str.3, %edi
	movl	$85, %esi
	jmp	.LBB10_6
.LBB10_53:
	movl	$.L.str.3, %edi
	movl	$84, %esi
	jmp	.LBB10_6
.LBB10_28:
	movl	$.L.str.3, %edi
	movl	$79, %esi
	jmp	.LBB10_6
.LBB10_30:
	movl	$.L.str.3, %edi
	movl	$78, %esi
	jmp	.LBB10_6
.LBB10_32:
	movl	$.L.str.3, %edi
	movl	$77, %esi
	jmp	.LBB10_6
.LBB10_34:
	movl	$.L.str.3, %edi
	movl	$76, %esi
	jmp	.LBB10_6
.LBB10_36:
	movl	$.L.str.3, %edi
	movl	$75, %esi
	jmp	.LBB10_6
.LBB10_54:
	movl	$.L.str.3, %edi
	movl	$74, %esi
	jmp	.LBB10_6
.LBB10_39:
	movl	$.L.str.3, %edi
	movl	$69, %esi
	jmp	.LBB10_6
.LBB10_41:
	movl	$.L.str.3, %edi
	movl	$68, %esi
	jmp	.LBB10_6
.LBB10_43:
	movl	$.L.str.3, %edi
	movl	$67, %esi
	jmp	.LBB10_6
.LBB10_45:
	movl	$.L.str.3, %edi
	movl	$66, %esi
	jmp	.LBB10_6
.LBB10_47:
	movl	$.L.str.3, %edi
	movl	$65, %esi
.LBB10_6:
	xorl	%eax, %eax
	callq	printf
	callq	abort
.Lfunc_end10:
	.size	vp, .Lfunc_end10-vp
	.cfi_endproc

	.globl	freetree
	.p2align	4, 0x90
	.type	freetree,@function
freetree:                               # @freetree
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 16
.Lcfi84:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB11_21
# BB#1:
	movzwl	(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB11_21
# BB#2:                                 # %.preheader
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_4
# BB#3:
	callq	freetree
.LBB11_4:
	movq	96(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_6
# BB#5:
	callq	freetree
.LBB11_6:
	movq	88(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_8
# BB#7:
	callq	freetree
.LBB11_8:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_10
# BB#9:
	callq	freetree
.LBB11_10:
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_12
# BB#11:
	callq	freetree
.LBB11_12:
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_14
# BB#13:
	callq	freetree
.LBB11_14:
	movq	56(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_16
# BB#15:
	callq	freetree
.LBB11_16:
	movq	48(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_18
# BB#17:
	callq	freetree
.LBB11_18:
	movzwl	(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB11_19
# BB#22:
	movl	$bp_free_list, %eax
	movq	bp_free_list(%rip), %rcx
	movq	%rcx, 128(%rbx)
	jmp	.LBB11_20
.LBB11_19:
	movl	$cp_free_list, %eax
	movq	cp_free_list(%rip), %rcx
	movq	%rcx, 112(%rbx)
.LBB11_20:                              # %my_free.exit
	movq	%rbx, (%rax)
.LBB11_21:
	popq	%rbx
	retq
.Lfunc_end11:
	.size	freetree, .Lfunc_end11-freetree
	.cfi_endproc

	.globl	my_free
	.p2align	4, 0x90
	.type	my_free,@function
my_free:                                # @my_free
	.cfi_startproc
# BB#0:
	movzwl	(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB12_2
# BB#1:
	movl	$bp_free_list, %eax
	movq	bp_free_list(%rip), %rcx
	movq	%rcx, 128(%rdi)
	movq	%rdi, (%rax)
	retq
.LBB12_2:
	movl	$cp_free_list, %eax
	movq	cp_free_list(%rip), %rcx
	movq	%rcx, 112(%rdi)
	movq	%rdi, (%rax)
	retq
.Lfunc_end12:
	.size	my_free, .Lfunc_end12-my_free
	.cfi_endproc

	.globl	ubody_alloc
	.p2align	4, 0x90
	.type	ubody_alloc,@function
ubody_alloc:                            # @ubody_alloc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 16
.Lcfi86:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$144, %edi
	callq	malloc
	movw	$1, (%rax)
	movl	%ebx, 40(%rax)
	movq	$0, 136(%rax)
	movl	%ebx, 44(%rax)
	popq	%rbx
	retq
.Lfunc_end13:
	.size	ubody_alloc, .Lfunc_end13-ubody_alloc
	.cfi_endproc

	.globl	cell_alloc
	.p2align	4, 0x90
	.type	cell_alloc,@function
cell_alloc:                             # @cell_alloc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 16
.Lcfi88:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	cp_free_list(%rip), %rax
	testq	%rax, %rax
	je	.LBB14_2
# BB#1:
	movq	112(%rax), %rcx
	movq	%rcx, cp_free_list(%rip)
	jmp	.LBB14_3
.LBB14_2:
	movl	$120, %edi
	callq	malloc
.LBB14_3:
	movw	$2, (%rax)
	movl	%ebx, 40(%rax)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 96(%rax)
	movups	%xmm0, 80(%rax)
	movups	%xmm0, 64(%rax)
	movups	%xmm0, 48(%rax)
	popq	%rbx
	retq
.Lfunc_end14:
	.size	cell_alloc, .Lfunc_end14-cell_alloc
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI15_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	grav
	.p2align	4, 0x90
	.type	grav,@function
grav:                                   # @grav
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 40
	subq	$216, %rsp
.Lcfi93:
	.cfi_def_cfa_offset 256
.Lcfi94:
	.cfi_offset %rbx, -40
.Lcfi95:
	.cfi_offset %r12, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	testq	%rbx, %rbx
	je	.LBB15_3
# BB#1:                                 # %.lr.ph
	leaq	88(%rsp), %r12
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	leaq	152(%rsp), %r15
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	%rbx, 80(%rsp)
	movq	32(%rbx), %rax
	movq	%rax, 16(%r12)
	movups	16(%rbx), %xmm0
	movups	%xmm0, (%r12)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%r12)
	movups	%xmm0, 24(%r12)
	movaps	80(%rsp), %xmm0
	movaps	96(%rsp), %xmm1
	movaps	112(%rsp), %xmm2
	movaps	128(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	.LCPI15_0(%rip), %xmm1  # xmm1 = mem[0],zero
	callq	walksub
	movups	152(%rsp), %xmm0
	movups	168(%rsp), %xmm1
	movups	184(%rsp), %xmm2
	movups	200(%rsp), %xmm3
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm0, 80(%rsp)
	movq	112(%rsp), %rax
	movq	%rax, 120(%rbx)
	movq	48(%r12), %rax
	movq	%rax, 112(%rbx)
	movupd	32(%r12), %xmm0
	movupd	%xmm0, 96(%rbx)
	movq	136(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_2
.LBB15_3:                               # %._crit_edge
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	grav, .Lfunc_end15-grav
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	gravstep
	.p2align	4, 0x90
	.type	gravstep,@function
gravstep:                               # @gravstep
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 16
	subq	$192, %rsp
.Lcfi99:
	.cfi_def_cfa_offset 208
.Lcfi100:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	%rdi, %rax
	movq	%rbx, 64(%rsp)
	movq	32(%rbx), %rcx
	movq	%rcx, 88(%rsp)
	movups	16(%rbx), %xmm1
	movups	%xmm1, 72(%rsp)
	mulsd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	movaps	%xmm1, 112(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	64(%rsp), %xmm1
	movaps	80(%rsp), %xmm2
	movaps	96(%rsp), %xmm3
	movaps	112(%rsp), %xmm4
	movups	%xmm4, 48(%rsp)
	movups	%xmm3, 32(%rsp)
	movups	%xmm2, 16(%rsp)
	movups	%xmm1, (%rsp)
	leaq	128(%rsp), %rdi
	movsd	.LCPI16_0(%rip), %xmm1  # xmm1 = mem[0],zero
	xorl	%edx, %edx
	movq	%rax, %rsi
	callq	walksub
	movups	128(%rsp), %xmm0
	movups	144(%rsp), %xmm1
	movups	160(%rsp), %xmm2
	movups	176(%rsp), %xmm3
	movaps	%xmm3, 112(%rsp)
	movaps	%xmm2, 96(%rsp)
	movaps	%xmm1, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	96(%rsp), %rax
	movq	%rax, 120(%rbx)
	movq	120(%rsp), %rax
	movq	%rax, 112(%rbx)
	movups	104(%rsp), %xmm0
	movups	%xmm0, 96(%rbx)
	addq	$192, %rsp
	popq	%rbx
	retq
.Lfunc_end16:
	.size	gravstep, .Lfunc_end16-gravstep
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI17_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	hackgrav
	.p2align	4, 0x90
	.type	hackgrav,@function
hackgrav:                               # @hackgrav
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 16
	subq	$192, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 208
.Lcfi103:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	%rbx, 64(%rsp)
	movq	32(%rbx), %rax
	movq	%rax, 88(%rsp)
	movups	16(%rbx), %xmm1
	movups	%xmm1, 72(%rsp)
	mulsd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	movaps	%xmm1, 112(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	64(%rsp), %xmm1
	movaps	80(%rsp), %xmm2
	movaps	96(%rsp), %xmm3
	movaps	112(%rsp), %xmm4
	movups	%xmm4, 48(%rsp)
	movups	%xmm3, 32(%rsp)
	movups	%xmm2, 16(%rsp)
	movups	%xmm1, (%rsp)
	leaq	128(%rsp), %rdi
	movsd	.LCPI17_0(%rip), %xmm1  # xmm1 = mem[0],zero
	xorl	%edx, %edx
	callq	walksub
	movups	128(%rsp), %xmm0
	movups	144(%rsp), %xmm1
	movups	160(%rsp), %xmm2
	movups	176(%rsp), %xmm3
	movaps	%xmm3, 112(%rsp)
	movaps	%xmm2, 96(%rsp)
	movaps	%xmm1, 80(%rsp)
	movaps	%xmm0, 64(%rsp)
	movq	96(%rsp), %rax
	movq	%rax, 120(%rbx)
	movq	120(%rsp), %rax
	movq	%rax, 112(%rbx)
	movups	104(%rsp), %xmm0
	movups	%xmm0, 96(%rbx)
	addq	$192, %rsp
	popq	%rbx
	retq
.Lfunc_end17:
	.size	hackgrav, .Lfunc_end17-hackgrav
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI18_0:
	.quad	4567911030049346684     # double 0.0025000000000000005
	.text
	.globl	gravsub
	.p2align	4, 0x90
	.type	gravsub,@function
gravsub:                                # @gravsub
	.cfi_startproc
# BB#0:                                 # %.preheader55.preheader62
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 64
.Lcfi108:
	.cfi_offset %rbx, -32
.Lcfi109:
	.cfi_offset %r14, -24
.Lcfi110:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	leaq	64(%rsp), %r15
	movupd	16(%r14), %xmm2
	movupd	72(%rsp), %xmm0
	subpd	%xmm0, %xmm2
	movsd	32(%r14), %xmm3         # xmm3 = mem[0],zero
	subsd	88(%rsp), %xmm3
	movapd	%xmm2, %xmm0
	mulsd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm2, %xmm0
	movhlps	%xmm0, %xmm0            # xmm0 = xmm0[1,1]
	mulsd	%xmm0, %xmm0
	addsd	%xmm1, %xmm0
	movapd	%xmm3, %xmm4
	mulsd	%xmm4, %xmm4
	addsd	%xmm0, %xmm4
	addsd	.LCPI18_0(%rip), %xmm4
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm4, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB18_2
# BB#1:                                 # %call.sqrt
	movapd	%xmm4, %xmm0
	movapd	%xmm2, 16(%rsp)         # 16-byte Spill
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	movsd	%xmm4, (%rsp)           # 8-byte Spill
	callq	sqrt
	movsd	(%rsp), %xmm4           # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movapd	16(%rsp), %xmm2         # 16-byte Reload
.LBB18_2:                               # %.preheader55.preheader62.split
	movsd	8(%r14), %xmm1          # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	32(%r15), %xmm0         # xmm0 = mem[0],zero
	subsd	%xmm1, %xmm0
	movsd	%xmm0, 32(%r15)
	divsd	%xmm4, %xmm1
	movapd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm2, %xmm0
	mulsd	%xmm3, %xmm1
	movupd	40(%r15), %xmm2
	addpd	%xmm0, %xmm2
	movupd	%xmm2, 40(%r15)
	addsd	56(%r15), %xmm1
	movsd	%xmm1, 56(%r15)
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	32(%r15), %xmm2
	movups	48(%r15), %xmm3
	movups	%xmm3, 48(%rbx)
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	gravsub, .Lfunc_end18-gravsub
	.cfi_endproc

	.globl	subdivp
	.p2align	4, 0x90
	.type	subdivp,@function
subdivp:                                # @subdivp
	.cfi_startproc
# BB#0:
	movzwl	(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB19_2
# BB#1:
	xorl	%eax, %eax
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.LBB19_2:                               # %.preheader28.preheader
	leaq	8(%rsp), %rax
	movsd	16(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	24(%rdi), %xmm3         # xmm3 = mem[0],zero
	subsd	8(%rax), %xmm2
	subsd	16(%rax), %xmm3
	movsd	32(%rdi), %xmm4         # xmm4 = mem[0],zero
	subsd	24(%rax), %xmm4
	mulsd	%xmm2, %xmm2
	xorpd	%xmm5, %xmm5
	addsd	%xmm2, %xmm5
	mulsd	%xmm3, %xmm3
	addsd	%xmm5, %xmm3
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	mulsd	%xmm1, %xmm4
	xorl	%eax, %eax
	ucomisd	%xmm4, %xmm0
	seta	%al
                                        # kill: %AX<def> %AX<kill> %EAX<kill>
	retq
.Lfunc_end19:
	.size	subdivp, .Lfunc_end19-subdivp
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI20_0:
	.quad	4607182418800017408     # double 1
.LCPI20_1:
	.quad	4652007308841189376     # double 1000
.LCPI20_2:
	.quad	4602678819172646912     # double 0.5
.LCPI20_3:
	.quad	4742290407621132288     # double 1073741824
	.text
	.globl	expandbox
	.p2align	4, 0x90
	.type	expandbox,@function
expandbox:                              # @expandbox
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi112:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi113:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi114:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi115:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi117:
	.cfi_def_cfa_offset 96
.Lcfi118:
	.cfi_offset %rbx, -56
.Lcfi119:
	.cfi_offset %r12, -48
.Lcfi120:
	.cfi_offset %r13, -40
.Lcfi121:
	.cfi_offset %r14, -32
.Lcfi122:
	.cfi_offset %r15, -24
.Lcfi123:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movsd	24(%rbx), %xmm4         # xmm4 = mem[0],zero
	movsd	(%rbx), %xmm5           # xmm5 = mem[0],zero
	movsd	8(%rbx), %xmm6          # xmm6 = mem[0],zero
	movsd	16(%rbx), %xmm7         # xmm7 = mem[0],zero
	xorps	%xmm8, %xmm8
	movsd	.LCPI20_2(%rip), %xmm9  # xmm9 = mem[0],zero
	jmp	.LBB20_1
	.p2align	4, 0x90
.LBB20_16:                              #   in Loop: Header=BB20_1 Depth=1
	movq	cp_free_list(%rip), %r15
	testq	%r15, %r15
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	je	.LBB20_20
# BB#17:                                #   in Loop: Header=BB20_1 Depth=1
	movq	112(%r15), %rax
	movq	%rax, cp_free_list(%rip)
	jmp	.LBB20_21
	.p2align	4, 0x90
.LBB20_20:                              #   in Loop: Header=BB20_1 Depth=1
	movl	$120, %edi
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	malloc
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	xorps	%xmm8, %xmm8
	movq	%rax, %r15
.LBB20_21:                              # %cell_alloc.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	movw	$2, (%r15)
	movl	$0, 40(%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 96(%r15)
	movups	%xmm0, 80(%r15)
	movups	%xmm0, 64(%r15)
	movups	%xmm0, 48(%r15)
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	movsd	24(%rbx), %xmm4         # xmm4 = mem[0],zero
	subsd	%xmm0, %xmm3
	divsd	%xmm4, %xmm3
	xorl	%r13d, %r13d
	ucomisd	%xmm8, %xmm3
	movsd	%xmm4, 8(%rsp)          # 8-byte Spill
	jb	.LBB20_22
# BB#23:                                # %cell_alloc.exit
                                        #   in Loop: Header=BB20_1 Depth=1
	movsd	.LCPI20_0(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm3, %xmm0
	movl	$0, %ebp
	jbe	.LBB20_25
# BB#24:                                #   in Loop: Header=BB20_1 Depth=1
	mulsd	.LCPI20_3(%rip), %xmm3
	movapd	%xmm3, %xmm0
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	floor
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	xorps	%xmm8, %xmm8
	cvttsd2si	%xmm0, %ebp
	movabsq	$4294967296, %r13       # imm = 0x100000000
	jmp	.LBB20_25
	.p2align	4, 0x90
.LBB20_22:                              #   in Loop: Header=BB20_1 Depth=1
	xorl	%ebp, %ebp
.LBB20_25:                              #   in Loop: Header=BB20_1 Depth=1
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	subsd	%xmm0, %xmm2
	divsd	%xmm4, %xmm2
	ucomisd	%xmm8, %xmm2
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	jb	.LBB20_26
# BB#27:                                #   in Loop: Header=BB20_1 Depth=1
	movsd	.LCPI20_0(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	movl	$0, %eax
	movl	$0, %r12d
	jbe	.LBB20_29
# BB#28:                                #   in Loop: Header=BB20_1 Depth=1
	mulsd	.LCPI20_3(%rip), %xmm2
	movapd	%xmm2, %xmm0
	callq	floor
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	xorps	%xmm8, %xmm8
	cvttsd2si	%xmm0, %r12d
	shlq	$32, %r12
	movq	%r13, %rax
	jmp	.LBB20_29
	.p2align	4, 0x90
.LBB20_26:                              #   in Loop: Header=BB20_1 Depth=1
	xorl	%eax, %eax
	xorl	%r12d, %r12d
.LBB20_29:                              #   in Loop: Header=BB20_1 Depth=1
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	subsd	%xmm0, %xmm1
	divsd	%xmm4, %xmm1
	movsd	.LCPI20_0(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	jbe	.LBB20_32
# BB#30:                                #   in Loop: Header=BB20_1 Depth=1
	ucomisd	%xmm8, %xmm1
	jb	.LBB20_32
# BB#31:                                #   in Loop: Header=BB20_1 Depth=1
	movabsq	$281470681743360, %rcx  # imm = 0xFFFF00000000
	andq	%rcx, %rax
	je	.LBB20_32
# BB#33:                                #   in Loop: Header=BB20_1 Depth=1
	mulsd	.LCPI20_3(%rip), %xmm1
	movapd	%xmm1, %xmm0
	callq	floor
	xorps	%xmm8, %xmm8
	cvttsd2si	%xmm0, %eax
	shrl	$27, %ebp
	andl	$4, %ebp
	shrq	$60, %r12
	andl	$2, %r12d
	orl	%ebp, %r12d
	shrl	$29, %eax
	andl	$1, %eax
	orl	%r12d, %eax
	movq	32(%rbx), %rcx
	movq	%rcx, 48(%r15,%rax,8)
	movq	%r15, 32(%rbx)
	movsd	.LCPI20_2(%rip), %xmm3  # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm9
	movsd	8(%rsp), %xmm4          # 8-byte Reload
                                        # xmm4 = mem[0],zero
	movsd	32(%rsp), %xmm5         # 8-byte Reload
                                        # xmm5 = mem[0],zero
	movsd	(%rsp), %xmm6           # 8-byte Reload
                                        # xmm6 = mem[0],zero
	movsd	24(%rsp), %xmm7         # 8-byte Reload
                                        # xmm7 = mem[0],zero
.LBB20_1:                               # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB20_8 Depth 2
	movsd	32(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	%xmm7, %xmm0
	divsd	%xmm4, %xmm0
	ucomisd	%xmm8, %xmm0
	jb	.LBB20_7
# BB#2:                                 # %.sink.split
                                        #   in Loop: Header=BB20_1 Depth=1
	movsd	.LCPI20_0(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB20_7
# BB#3:                                 # %.sink.split
                                        #   in Loop: Header=BB20_1 Depth=1
	movsd	16(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	%xmm5, %xmm0
	divsd	%xmm4, %xmm0
	ucomisd	%xmm8, %xmm0
	jb	.LBB20_7
# BB#4:                                 # %.sink.split
                                        #   in Loop: Header=BB20_1 Depth=1
	movsd	.LCPI20_0(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB20_7
# BB#5:                                 # %.sink.split
                                        #   in Loop: Header=BB20_1 Depth=1
	movsd	24(%r14), %xmm0         # xmm0 = mem[0],zero
	subsd	%xmm6, %xmm0
	divsd	%xmm4, %xmm0
	ucomisd	%xmm8, %xmm0
	jb	.LBB20_7
# BB#6:                                 # %.sink.split
                                        #   in Loop: Header=BB20_1 Depth=1
	movsd	.LCPI20_0(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	ja	.LBB20_34
	.p2align	4, 0x90
.LBB20_7:                               # %.sink.split.split.preheader
                                        #   in Loop: Header=BB20_1 Depth=1
	movsd	.LCPI20_1(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB20_8:                               # %.sink.split.split
                                        #   Parent Loop BB20_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	ucomisd	%xmm4, %xmm0
	jbe	.LBB20_18
# BB#9:                                 # %.preheader57
                                        #   in Loop: Header=BB20_8 Depth=2
	movapd	%xmm4, %xmm1
	mulsd	%xmm9, %xmm1
	movapd	%xmm1, %xmm3
	addsd	%xmm5, %xmm3
	movapd	%xmm1, %xmm2
	addsd	%xmm6, %xmm2
	ucomisd	16(%r14), %xmm3
	jbe	.LBB20_11
# BB#10:                                #   in Loop: Header=BB20_8 Depth=2
	subsd	%xmm4, %xmm5
	movsd	%xmm5, (%rbx)
.LBB20_11:                              # %.preheader.161
                                        #   in Loop: Header=BB20_8 Depth=2
	addsd	%xmm7, %xmm1
	ucomisd	24(%r14), %xmm2
	jbe	.LBB20_13
# BB#12:                                #   in Loop: Header=BB20_8 Depth=2
	subsd	%xmm4, %xmm6
	movsd	%xmm6, 8(%rbx)
.LBB20_13:                              # %.preheader.262
                                        #   in Loop: Header=BB20_8 Depth=2
	ucomisd	32(%r14), %xmm1
	jbe	.LBB20_15
# BB#14:                                #   in Loop: Header=BB20_8 Depth=2
	subsd	%xmm4, %xmm7
	movsd	%xmm7, 16(%rbx)
.LBB20_15:                              #   in Loop: Header=BB20_8 Depth=2
	addsd	%xmm4, %xmm4
	movsd	%xmm4, 24(%rbx)
	cmpq	$0, 32(%rbx)
	je	.LBB20_8
	jmp	.LBB20_16
.LBB20_34:                              # %.split
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_18:
	movl	$.L.str.3, %edi
	movl	$999, %esi              # imm = 0x3E7
.LBB20_19:
	xorl	%eax, %eax
	callq	printf
	callq	abort
.LBB20_32:                              # %intcoord1.exit.thread
	movl	$.L.str.3, %edi
	movl	$1, %esi
	jmp	.LBB20_19
.Lfunc_end20:
	.size	expandbox, .Lfunc_end20-expandbox
	.cfi_endproc

	.globl	loadtree
	.p2align	4, 0x90
	.type	loadtree,@function
loadtree:                               # @loadtree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi126:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi127:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi128:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 64
.Lcfi131:
	.cfi_offset %rbx, -56
.Lcfi132:
	.cfi_offset %r12, -48
.Lcfi133:
	.cfi_offset %r13, -40
.Lcfi134:
	.cfi_offset %r14, -32
.Lcfi135:
	.cfi_offset %r15, -24
.Lcfi136:
	.cfi_offset %rbp, -16
	movl	%r8d, %ebp
	movq	%rcx, %r13
	movq	%rdx, %r15
	movq	%rsi, %r12
	testq	%r13, %r13
	je	.LBB21_1
# BB#3:
	testl	%ebp, %ebp
	je	.LBB21_10
# BB#4:
	movzwl	(%r13), %eax
	cmpl	$1, %eax
	jne	.LBB21_9
# BB#5:
	movq	%rdi, (%rsp)            # 8-byte Spill
	movq	cp_free_list(%rip), %r14
	testq	%r14, %r14
	je	.LBB21_7
# BB#6:
	movq	112(%r14), %rax
	movq	%rax, cp_free_list(%rip)
	jmp	.LBB21_8
.LBB21_1:
	movq	%rdi, %rax
	jmp	.LBB21_2
.LBB21_7:
	movl	$120, %edi
	movq	%r9, %rbx
	callq	malloc
	movq	%rbx, %r9
	movq	%rax, %r14
.LBB21_8:                               # %cell_alloc.exit
	movw	$2, (%r14)
	movl	$0, 40(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 48(%r14)
	movq	%r13, %rdi
	movq	%r9, %rsi
	movl	%ebp, %edx
	movq	%r9, %rbx
	callq	subindex
	movq	%rbx, %r9
	cltq
	movq	%r13, 48(%r14,%rax,8)
	movq	%r14, %r13
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB21_9:
	movq	%r12, %rax
	shrq	$32, %rax
	xorl	%ecx, %ecx
	testl	%ebp, %r12d
	setne	%cl
	leal	(,%rcx,4), %edx
	testl	%ebp, %eax
	leal	2(,%rcx,4), %eax
	cmovel	%edx, %eax
	xorl	%ebx, %ebx
	testl	%ebp, %r15d
	setne	%bl
	orl	%eax, %ebx
	movq	48(%r13,%rbx,8), %rcx
	sarl	%ebp
	movq	%r12, %rsi
	movq	%r15, %rdx
	movl	%ebp, %r8d
	callq	loadtree
	movq	%rax, 48(%r13,%rbx,8)
	movq	%r13, %rax
.LBB21_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_10:
	movl	$.L.str.3, %edi
	movl	$2, %esi
	xorl	%eax, %eax
	callq	printf
	callq	abort
.Lfunc_end21:
	.size	loadtree, .Lfunc_end21-loadtree
	.cfi_endproc

	.globl	hackcofm
	.p2align	4, 0x90
	.type	hackcofm,@function
hackcofm:                               # @hackcofm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 24
	subq	$56, %rsp
.Lcfi139:
	.cfi_def_cfa_offset 80
.Lcfi140:
	.cfi_offset %rbx, -24
.Lcfi141:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movzwl	(%r14), %eax
	cmpl	$2, %eax
	jne	.LBB22_19
# BB#1:
	movq	48(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_2
# BB#3:                                 # %.preheader.preheader69
	movq	%rbx, %rdi
	callq	hackcofm
	movaps	%xmm0, %xmm3
	movupd	16(%rbx), %xmm0
	movaps	%xmm3, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm0, %xmm1
	movsd	32(%rbx), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	xorpd	%xmm4, %xmm4
	addpd	%xmm1, %xmm4
	xorpd	%xmm0, %xmm0
	addsd	%xmm0, %xmm2
	addsd	%xmm0, %xmm3
	jmp	.LBB22_4
.LBB22_19:
	movsd	8(%r14), %xmm3          # xmm3 = mem[0],zero
	jmp	.LBB22_20
.LBB22_2:
	xorpd	%xmm4, %xmm4
	xorpd	%xmm2, %xmm2
	xorpd	%xmm3, %xmm3
.LBB22_4:                               # %.loopexit59
	movq	56(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_6
# BB#5:                                 # %.preheader.preheader69.1
	movq	%rbx, %rdi
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	hackcofm
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	32(%rbx), %xmm0
	addpd	%xmm2, %xmm4
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
.LBB22_6:                               # %.loopexit59.1
	movq	64(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_8
# BB#7:                                 # %.preheader.preheader69.2
	movq	%rbx, %rdi
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	hackcofm
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	32(%rbx), %xmm0
	addpd	%xmm2, %xmm4
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
.LBB22_8:                               # %.loopexit59.2
	movq	72(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_10
# BB#9:                                 # %.preheader.preheader69.3
	movq	%rbx, %rdi
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	hackcofm
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	32(%rbx), %xmm0
	addpd	%xmm2, %xmm4
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
.LBB22_10:                              # %.loopexit59.3
	movq	80(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_12
# BB#11:                                # %.preheader.preheader69.4
	movq	%rbx, %rdi
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	hackcofm
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	32(%rbx), %xmm0
	addpd	%xmm2, %xmm4
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
.LBB22_12:                              # %.loopexit59.4
	movq	88(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_14
# BB#13:                                # %.preheader.preheader69.5
	movq	%rbx, %rdi
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	hackcofm
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	32(%rbx), %xmm0
	addpd	%xmm2, %xmm4
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
.LBB22_14:                              # %.loopexit59.5
	movq	96(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_16
# BB#15:                                # %.preheader.preheader69.6
	movq	%rbx, %rdi
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	hackcofm
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	32(%rbx), %xmm0
	addpd	%xmm2, %xmm4
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
.LBB22_16:                              # %.loopexit59.6
	movq	104(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB22_18
# BB#17:                                # %.preheader.preheader69.7
	movq	%rbx, %rdi
	movapd	%xmm3, 32(%rsp)         # 16-byte Spill
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	movapd	%xmm4, 16(%rsp)         # 16-byte Spill
	callq	hackcofm
	movapd	16(%rsp), %xmm4         # 16-byte Reload
	movapd	32(%rsp), %xmm3         # 16-byte Reload
	movupd	16(%rbx), %xmm1
	movapd	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	mulpd	%xmm1, %xmm2
	addsd	%xmm0, %xmm3
	mulsd	32(%rbx), %xmm0
	addpd	%xmm2, %xmm4
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	addsd	%xmm0, %xmm2
.LBB22_18:                              # %.loopexit59.7
	movsd	%xmm3, 8(%r14)
	movapd	%xmm3, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	divpd	%xmm0, %xmm4
	movupd	%xmm4, 16(%r14)
	divsd	%xmm3, %xmm2
	movsd	%xmm2, 32(%r14)
.LBB22_20:                              # %.loopexit
	movapd	%xmm3, %xmm0
	addq	$56, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end22:
	.size	hackcofm, .Lfunc_end22-hackcofm
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI23_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	ic_test
	.p2align	4, 0x90
	.type	ic_test,@function
ic_test:                                # @ic_test
	.cfi_startproc
# BB#0:
	movsd	16(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	32(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	24(%rsi), %xmm3         # xmm3 = mem[0],zero
	subsd	(%rsi), %xmm0
	divsd	%xmm3, %xmm0
	xorps	%xmm4, %xmm4
	ucomisd	%xmm4, %xmm0
	setae	%al
	movsd	.LCPI23_0(%rip), %xmm5  # xmm5 = mem[0],zero
	ucomisd	%xmm0, %xmm5
	seta	%cl
	andb	%al, %cl
	subsd	8(%rsi), %xmm1
	divsd	%xmm3, %xmm1
	ucomisd	%xmm4, %xmm1
	setae	%al
	ucomisd	%xmm1, %xmm5
	seta	%dl
	andb	%al, %dl
	andb	%cl, %dl
	subsd	16(%rsi), %xmm2
	divsd	%xmm3, %xmm2
	ucomisd	%xmm4, %xmm2
	setae	%al
	ucomisd	%xmm2, %xmm5
	seta	%cl
	andb	%al, %cl
	andb	%dl, %cl
	movzbl	%cl, %eax
	retq
.Lfunc_end23:
	.size	ic_test, .Lfunc_end23-ic_test
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI24_0:
	.quad	4607182418800017408     # double 1
.LCPI24_1:
	.quad	4742290407621132288     # double 1073741824
	.text
	.globl	intcoord1
	.p2align	4, 0x90
	.type	intcoord1,@function
intcoord1:                              # @intcoord1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi147:
	.cfi_def_cfa_offset 80
.Lcfi148:
	.cfi_offset %rbx, -48
.Lcfi149:
	.cfi_offset %r12, -40
.Lcfi150:
	.cfi_offset %r13, -32
.Lcfi151:
	.cfi_offset %r14, -24
.Lcfi152:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	subsd	(%r14), %xmm0
	movsd	24(%r14), %xmm3         # xmm3 = mem[0],zero
	divsd	%xmm3, %xmm0
	xorl	%r12d, %r12d
	xorps	%xmm5, %xmm5
	ucomisd	%xmm5, %xmm0
	movsd	%xmm2, 16(%rsp)         # 8-byte Spill
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	jb	.LBB24_1
# BB#2:
	movsd	.LCPI24_0(%rip), %xmm4  # xmm4 = mem[0],zero
	ucomisd	%xmm0, %xmm4
	movl	$0, %r15d
	jbe	.LBB24_4
# BB#3:
	mulsd	.LCPI24_1(%rip), %xmm0
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	callq	floor
	xorps	%xmm5, %xmm5
	movsd	24(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvttsd2si	%xmm0, %r15d
	movabsq	$4294967296, %r12       # imm = 0x100000000
	jmp	.LBB24_4
.LBB24_1:
	xorl	%r15d, %r15d
.LBB24_4:
	subsd	8(%r14), %xmm1
	divsd	%xmm3, %xmm1
	xorl	%r13d, %r13d
	ucomisd	%xmm5, %xmm1
	jb	.LBB24_5
# BB#6:
	movsd	.LCPI24_0(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm1, %xmm0
	movl	$0, %ebx
	jbe	.LBB24_8
# BB#7:
	mulsd	.LCPI24_1(%rip), %xmm1
	movapd	%xmm1, %xmm0
	callq	floor
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	cvttsd2si	%xmm0, %ebx
	shlq	$32, %rbx
	movq	%r12, %r13
	jmp	.LBB24_8
.LBB24_5:
	xorl	%ebx, %ebx
.LBB24_8:
	subsd	16(%r14), %xmm2
	divsd	%xmm3, %xmm2
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm2
	jb	.LBB24_9
# BB#10:
	movsd	.LCPI24_0(%rip), %xmm0  # xmm0 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	movl	$0, %ecx
	jbe	.LBB24_12
# BB#11:
	mulsd	.LCPI24_1(%rip), %xmm2
	movapd	%xmm2, %xmm0
	callq	floor
	cvttsd2si	%xmm0, %ecx
	movq	%r13, %rax
	jmp	.LBB24_12
.LBB24_9:
	xorl	%ecx, %ecx
.LBB24_12:
	orq	%r15, %rbx
	movabsq	$281470681743360, %rdx  # imm = 0xFFFF00000000
	andq	%rax, %rdx
	orq	%rcx, %rdx
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	intcoord1, .Lfunc_end24-intcoord1
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI25_0:
	.quad	4607182418800017408     # double 1
.LCPI25_1:
	.quad	4742290407621132288     # double 1073741824
	.text
	.globl	subindex
	.p2align	4, 0x90
	.type	subindex,@function
subindex:                               # @subindex
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi156:
	.cfi_def_cfa_offset 64
.Lcfi157:
	.cfi_offset %rbx, -32
.Lcfi158:
	.cfi_offset %r14, -24
.Lcfi159:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbx
	movsd	16(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rbx), %xmm1         # xmm1 = mem[0],zero
	subsd	(%rbx), %xmm0
	movsd	%xmm1, 24(%rsp)         # 8-byte Spill
	divsd	%xmm1, %xmm0
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jb	.LBB25_2
# BB#1:
	movsd	.LCPI25_0(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jbe	.LBB25_2
# BB#4:
	movsd	32(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	mulsd	.LCPI25_1(%rip), %xmm0
	callq	floor
	movsd	16(%rsp), %xmm4         # 8-byte Reload
                                        # xmm4 = mem[0],zero
	subsd	8(%rbx), %xmm4
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	divsd	%xmm3, %xmm4
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm4
	movsd	.LCPI25_0(%rip), %xmm5  # xmm5 = mem[0],zero
	jb	.LBB25_6
# BB#5:
	ucomisd	%xmm4, %xmm5
	jbe	.LBB25_6
# BB#7:
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	subsd	16(%rbx), %xmm2
	divsd	%xmm3, %xmm2
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm2
	jb	.LBB25_9
# BB#8:
	ucomisd	%xmm2, %xmm5
	jbe	.LBB25_9
# BB#10:
	cvttsd2si	%xmm0, %ebx
	mulsd	.LCPI25_1(%rip), %xmm4
	movapd	%xmm4, %xmm0
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	floor
	cvttsd2si	%xmm0, %ebp
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	mulsd	.LCPI25_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %ecx
	xorl	%eax, %eax
	testl	%r14d, %ebx
	setne	%al
	leal	(,%rax,4), %edx
	testl	%r14d, %ebp
	leal	2(,%rax,4), %esi
	cmovel	%edx, %esi
	xorl	%eax, %eax
	testl	%r14d, %ecx
	setne	%al
	orl	%esi, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB25_2:
	movl	$.L.str.3, %edi
	movl	$5, %esi
	jmp	.LBB25_3
.LBB25_6:
	movl	$.L.str.3, %edi
	movl	$6, %esi
	jmp	.LBB25_3
.LBB25_9:
	movl	$.L.str.3, %edi
	movl	$7, %esi
.LBB25_3:
	xorl	%eax, %eax
	callq	printf
	callq	abort
.Lfunc_end25:
	.size	subindex, .Lfunc_end25-subindex
	.cfi_endproc

	.globl	printtree
	.p2align	4, 0x90
	.type	printtree,@function
printtree:                              # @printtree
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	ptree                   # TAILCALL
.Lfunc_end26:
	.size	printtree, .Lfunc_end26-printtree
	.cfi_endproc

	.globl	ptree
	.p2align	4, 0x90
	.type	ptree,@function
ptree:                                  # @ptree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi160:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi161:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi162:
	.cfi_def_cfa_offset 32
.Lcfi163:
	.cfi_offset %rbx, -24
.Lcfi164:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB27_3
# BB#1:
	movsd	16(%rbx), %xmm0         # xmm0 = mem[0],zero
	movsd	24(%rbx), %xmm1         # xmm1 = mem[0],zero
	movsd	32(%rbx), %xmm2         # xmm2 = mem[0],zero
	movzwl	(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB27_2
# BB#4:
	movl	$.L.str.5, %edi
	movb	$3, %al
	movl	%ebp, %esi
	movq	%rbx, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB27_3:
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	printf                  # TAILCALL
.LBB27_2:                               # %.loopexit.loopexit22
	movl	$.L.str.6, %edi
	movb	$3, %al
	movl	%ebp, %esi
	movq	%rbx, %rdx
	callq	printf
	incl	%ebp
	movq	48(%rbx), %rdi
	movl	%ebp, %esi
	callq	ptree
	movq	56(%rbx), %rdi
	movl	%ebp, %esi
	callq	ptree
	movq	64(%rbx), %rdi
	movl	%ebp, %esi
	callq	ptree
	movq	72(%rbx), %rdi
	movl	%ebp, %esi
	callq	ptree
	movq	80(%rbx), %rdi
	movl	%ebp, %esi
	callq	ptree
	movq	88(%rbx), %rdi
	movl	%ebp, %esi
	callq	ptree
	movq	96(%rbx), %rdi
	movl	%ebp, %esi
	callq	ptree
	movq	104(%rbx), %rdi
	movl	%ebp, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	ptree                   # TAILCALL
.Lfunc_end27:
	.size	ptree, .Lfunc_end27-ptree
	.cfi_endproc

	.globl	dis_number
	.p2align	4, 0x90
	.type	dis_number,@function
dis_number:                             # @dis_number
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 16
.Lcfi166:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cvtsi2sdl	nbody(%rip), %xmm0
	cvtsi2sdl	NumNodes(%rip), %xmm1
	divsd	%xmm1, %xmm0
	callq	ceil
	cvttsd2si	%xmm0, %edx
	movl	$-1, %esi
	movq	%rbx, %rdi
	popq	%rbx
	jmp	dis2_number             # TAILCALL
.Lfunc_end28:
	.size	dis_number, .Lfunc_end28-dis_number
	.cfi_endproc

	.globl	dis2_number
	.p2align	4, 0x90
	.type	dis2_number,@function
dis2_number:                            # @dis2_number
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi167:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi168:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi169:
	.cfi_def_cfa_offset 32
.Lcfi170:
	.cfi_offset %rbx, -24
.Lcfi171:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB29_3
# BB#1:
	movzwl	(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB29_4
# BB#2:
	incl	%esi
	movl	%esi, %eax
	cltd
	idivl	%ebp
	movl	%eax, 44(%rbx)
.LBB29_3:                               # %.loopexit
	movl	%esi, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB29_4:                               # %.preheader
	movq	48(%rbx), %rdi
	movl	%ebp, %edx
	callq	dis2_number
	movq	56(%rbx), %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	callq	dis2_number
	movq	64(%rbx), %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	callq	dis2_number
	movq	72(%rbx), %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	callq	dis2_number
	movq	80(%rbx), %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	callq	dis2_number
	movq	88(%rbx), %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	callq	dis2_number
	movq	96(%rbx), %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	callq	dis2_number
	movq	104(%rbx), %rdi
	movl	%eax, %esi
	movl	%ebp, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	dis2_number             # TAILCALL
.Lfunc_end29:
	.size	dis2_number, .Lfunc_end29-dis2_number
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"nbody = %d, numnodes = %d\n"
	.size	.L.str, 27

	.type	nbody,@object           # @nbody
	.comm	nbody,4,4
	.type	NumNodes,@object        # @NumNodes
	.comm	NumNodes,4,4
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Bodies per %d = %d\n"
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Assertion Failure #%d"
	.size	.L.str.3, 22

	.type	cp_free_list,@object    # @cp_free_list
	.bss
	.globl	cp_free_list
	.p2align	3
cp_free_list:
	.quad	0
	.size	cp_free_list, 8

	.type	bp_free_list,@object    # @bp_free_list
	.globl	bp_free_list
	.p2align	3
bp_free_list:
	.quad	0
	.size	bp_free_list, 8

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"testdata: not enough memory\n"
	.size	.L.str.4, 29

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%2d BODY@%x %f, %f, %f\n"
	.size	.L.str.5, 24

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"%2d CELL@%x %f, %f, %f\n"
	.size	.L.str.6, 24

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%2d NULL TREE\n"
	.size	.L.str.7, 15

	.type	root,@object            # @root
	.comm	root,8,8
	.type	rmin,@object            # @rmin
	.comm	rmin,24,16
	.type	xxxrsize,@object        # @xxxrsize
	.comm	xxxrsize,8,8
	.type	arg1,@object            # @arg1
	.comm	arg1,4,4
	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"bodies created "
	.size	.Lstr, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
