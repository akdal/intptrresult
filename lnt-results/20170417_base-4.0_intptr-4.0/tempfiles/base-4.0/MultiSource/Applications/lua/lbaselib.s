	.text
	.file	"lbaselib.bc"
	.globl	luaopen_base
	.p2align	4, 0x90
	.type	luaopen_base,@function
luaopen_base:                           # @luaopen_base
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$-10002, %esi           # imm = 0xD8EE
	callq	lua_pushvalue
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.1, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$.L.str.1, %esi
	movl	$base_funcs, %edx
	movq	%rbx, %rdi
	callq	luaL_register
	movl	$.L.str.2, %esi
	movl	$7, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.3, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$ipairsaux, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$luaB_ipairs, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$.L.str.4, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$luaB_next, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$luaB_pairs, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-2, %esi
	movl	$.L.str.5, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movl	$.L.str.6, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	movl	$-2, %esi
	movl	$.L.str.7, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$luaB_newproxy, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.8, %edx
	movq	%rbx, %rdi
	callq	lua_setfield
	movl	$.L.str, %esi
	movl	$co_funcs, %edx
	movq	%rbx, %rdi
	callq	luaL_register
	movl	$2, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	luaopen_base, .Lfunc_end0-luaopen_base
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_ipairs,@function
luaB_ipairs:                            # @luaB_ipairs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$-10003, %esi           # imm = 0xD8ED
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$3, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	luaB_ipairs, .Lfunc_end1-luaB_ipairs
	.cfi_endproc

	.p2align	4, 0x90
	.type	ipairsaux,@function
ipairsaux:                              # @ipairsaux
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 32
.Lcfi7:
	.cfi_offset %rbx, -24
.Lcfi8:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$2, %esi
	callq	luaL_checkinteger
	movq	%rax, %rbx
	movl	$1, %esi
	movl	$5, %edx
	movq	%r14, %rdi
	callq	luaL_checktype
	incl	%ebx
	movslq	%ebx, %rbx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lua_pushinteger
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	lua_rawgeti
	movl	$-1, %esi
	movq	%r14, %rdi
	callq	lua_type
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	addl	%ecx, %ecx
	movl	%ecx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	ipairsaux, .Lfunc_end2-ipairsaux
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_pairs,@function
luaB_pairs:                             # @luaB_pairs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$-10003, %esi           # imm = 0xD8ED
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$3, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	luaB_pairs, .Lfunc_end3-luaB_pairs
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_next,@function
luaB_next:                              # @luaB_next
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$2, %ebp
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_next
	testl	%eax, %eax
	jne	.LBB4_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$1, %ebp
.LBB4_2:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	luaB_next, .Lfunc_end4-luaB_next
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_newproxy,@function
luaB_newproxy:                          # @luaB_newproxy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_settop
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_newuserdata
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_toboolean
	testl	%eax, %eax
	je	.LBB5_8
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$1, %eax
	jne	.LBB5_3
# BB#2:
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_createtable
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushboolean
	movl	$-10003, %esi           # imm = 0xD8ED
	movq	%rbx, %rdi
	callq	lua_rawset
	jmp	.LBB5_7
.LBB5_3:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getmetatable
	testl	%eax, %eax
	je	.LBB5_5
# BB#4:
	movl	$-10003, %esi           # imm = 0xD8ED
	movq	%rbx, %rdi
	callq	lua_rawget
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_toboolean
	movl	%eax, %ebp
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	testl	%ebp, %ebp
	jne	.LBB5_6
.LBB5_5:                                # %.critedge
	movl	$1, %esi
	movl	$.L.str.64, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB5_6:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getmetatable
.LBB5_7:
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
.LBB5_8:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end5:
	.size	luaB_newproxy, .Lfunc_end5-luaB_newproxy
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_assert,@function
luaB_assert:                            # @luaB_assert
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_toboolean
	testl	%eax, %eax
	je	.LBB6_1
# BB#2:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	lua_gettop              # TAILCALL
.LBB6_1:
	movl	$2, %esi
	movl	$.L.str.34, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaL_optlstring
	movq	%rax, %rcx
	movl	$.L.str.33, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	popq	%rbx
	jmp	luaL_error              # TAILCALL
.Lfunc_end6:
	.size	luaB_assert, .Lfunc_end6-luaB_assert
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4562146422526312448     # double 9.765625E-4
	.text
	.p2align	4, 0x90
	.type	luaB_collectgarbage,@function
luaB_collectgarbage:                    # @luaB_collectgarbage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$.L.str.37, %edx
	movl	$luaB_collectgarbage.opts, %ecx
	callq	luaL_checkoption
	movl	%eax, %ebp
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaL_optinteger
	movslq	%ebp, %rcx
	movl	luaB_collectgarbage.optsnum(,%rcx,4), %ebp
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movl	%eax, %edx
	callq	lua_gc
	movl	%eax, %r14d
	cmpl	$5, %ebp
	je	.LBB7_3
# BB#1:
	cmpl	$3, %ebp
	jne	.LBB7_4
# BB#2:
	movl	$4, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_gc
	cvtsi2sdl	%r14d, %xmm1
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI7_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
	jmp	.LBB7_5
.LBB7_3:
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	lua_pushboolean
	jmp	.LBB7_6
.LBB7_4:
	cvtsi2sdl	%r14d, %xmm0
.LBB7_5:
	movq	%rbx, %rdi
	callq	lua_pushnumber
.LBB7_6:
	movl	$1, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	luaB_collectgarbage, .Lfunc_end7-luaB_collectgarbage
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_dofile,@function
luaB_dofile:                            # @luaB_dofile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	luaL_optlstring
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	lua_gettop
	movl	%eax, %r14d
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	luaL_loadfile
	testl	%eax, %eax
	je	.LBB8_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_error
.LBB8_2:
	xorl	%esi, %esi
	movl	$-1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movq	%rbx, %rdi
	callq	lua_gettop
	subl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end8:
	.size	luaB_dofile, .Lfunc_end8-luaB_dofile
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_error,@function
luaB_error:                             # @luaB_error
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 32
.Lcfi38:
	.cfi_offset %rbx, -24
.Lcfi39:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	movl	$1, %edx
	callq	luaL_optinteger
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_isstring
	testl	%r14d, %r14d
	jle	.LBB9_3
# BB#1:
	testl	%eax, %eax
	je	.LBB9_3
# BB#2:
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	luaL_where
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_concat
.LBB9_3:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	lua_error               # TAILCALL
.Lfunc_end9:
	.size	luaB_error, .Lfunc_end9-luaB_error
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_gcinfo,@function
luaB_gcinfo:                            # @luaB_gcinfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$3, %esi
	xorl	%edx, %edx
	callq	lua_gc
	movslq	%eax, %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	luaB_gcinfo, .Lfunc_end10-luaB_gcinfo
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_getfenv,@function
luaB_getfenv:                           # @luaB_getfenv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	getfunc
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_iscfunction
	testl	%eax, %eax
	je	.LBB11_2
# BB#1:
	movl	$-10002, %esi           # imm = 0xD8EE
	movq	%rbx, %rdi
	callq	lua_pushvalue
	jmp	.LBB11_3
.LBB11_2:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_getfenv
.LBB11_3:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	luaB_getfenv, .Lfunc_end11-luaB_getfenv
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_getmetatable,@function
luaB_getmetatable:                      # @luaB_getmetatable
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 16
.Lcfi45:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_getmetatable
	testl	%eax, %eax
	je	.LBB12_1
# BB#2:
	movl	$1, %esi
	movl	$.L.str.46, %edx
	movq	%rbx, %rdi
	callq	luaL_getmetafield
	jmp	.LBB12_3
.LBB12_1:
	movq	%rbx, %rdi
	callq	lua_pushnil
.LBB12_3:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	luaB_getmetatable, .Lfunc_end12-luaB_getmetatable
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_loadfile,@function
luaB_loadfile:                          # @luaB_loadfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %ebp
	movl	$1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	luaL_optlstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	luaL_loadfile
	testl	%eax, %eax
	je	.LBB13_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$2, %ebp
.LBB13_2:                               # %load_aux.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	luaB_loadfile, .Lfunc_end13-luaB_loadfile
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_load,@function
luaB_load:                              # @luaB_load
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	movl	$.L.str.47, %edx
	xorl	%ecx, %ecx
	callq	luaL_optlstring
	movq	%rax, %r14
	movl	$1, %ebp
	movl	$1, %esi
	movl	$6, %edx
	movq	%rbx, %rdi
	callq	luaL_checktype
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$generic_reader, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rcx
	callq	lua_load
	testl	%eax, %eax
	je	.LBB14_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$2, %ebp
.LBB14_2:                               # %load_aux.exit
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end14:
	.size	luaB_load, .Lfunc_end14-luaB_load
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_loadstring,@function
luaB_loadstring:                        # @luaB_loadstring
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %r14d
	leaq	8(%rsp), %rdx
	movl	$1, %esi
	callq	luaL_checklstring
	movq	%rax, %rbp
	movl	$2, %esi
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	luaL_optlstring
	movq	8(%rsp), %rdx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%rax, %rcx
	callq	luaL_loadbuffer
	testl	%eax, %eax
	je	.LBB15_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$2, %r14d
.LBB15_2:                               # %load_aux.exit
	movl	%r14d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end15:
	.size	luaB_loadstring, .Lfunc_end15-luaB_loadstring
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_pcall,@function
luaB_pcall:                             # @luaB_pcall
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 16
.Lcfi65:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movq	%rbx, %rdi
	callq	lua_gettop
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %esi
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	lua_pcall
	xorl	%esi, %esi
	testl	%eax, %eax
	sete	%sil
	movq	%rbx, %rdi
	callq	lua_pushboolean
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movq	%rbx, %rdi
	popq	%rbx
	jmp	lua_gettop              # TAILCALL
.Lfunc_end16:
	.size	luaB_pcall, .Lfunc_end16-luaB_pcall
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_print,@function
luaB_print:                             # @luaB_print
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi70:
	.cfi_def_cfa_offset 48
.Lcfi71:
	.cfi_offset %rbx, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	callq	lua_gettop
	movl	%eax, %r14d
	movl	$-10002, %esi           # imm = 0xD8EE
	movl	$.L.str.29, %edx
	movq	%r15, %rdi
	callq	lua_getfield
	testl	%r14d, %r14d
	jle	.LBB17_6
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB17_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movl	$-1, %esi
	movq	%r15, %rdi
	callq	lua_pushvalue
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	lua_pushvalue
	movl	$1, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	lua_call
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	lua_tolstring
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB17_7
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	cmpl	$2, %ebp
	jl	.LBB17_5
# BB#4:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	stdout(%rip), %rsi
	movl	$9, %edi
	callq	fputc
.LBB17_5:                               #   in Loop: Header=BB17_2 Depth=1
	movq	stdout(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_settop
	cmpl	%r14d, %ebp
	jl	.LBB17_2
.LBB17_6:
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_7:
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaL_error              # TAILCALL
.Lfunc_end17:
	.size	luaB_print, .Lfunc_end17-luaB_print
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_rawequal,@function
luaB_rawequal:                          # @luaB_rawequal
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 16
.Lcfi76:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checkany
	movl	$1, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	lua_rawequal
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_pushboolean
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end18:
	.size	luaB_rawequal, .Lfunc_end18-luaB_rawequal
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_rawget,@function
luaB_rawget:                            # @luaB_rawget
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 16
.Lcfi78:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checkany
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_rawget
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end19:
	.size	luaB_rawget, .Lfunc_end19-luaB_rawget
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_rawset,@function
luaB_rawset:                            # @luaB_rawset
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 16
.Lcfi80:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	luaL_checkany
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	luaL_checkany
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_rawset
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end20:
	.size	luaB_rawset, .Lfunc_end20-luaB_rawset
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_select,@function
luaB_select:                            # @luaB_select
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 32
.Lcfi84:
	.cfi_offset %rbx, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	callq	lua_gettop
	movl	%eax, %ebx
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_type
	cmpl	$4, %eax
	jne	.LBB21_3
# BB#1:
	movl	$1, %ebp
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	lua_tolstring
	cmpb	$35, (%rax)
	jne	.LBB21_3
# BB#2:
	decl	%ebx
	movslq	%ebx, %rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	jmp	.LBB21_6
.LBB21_3:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	luaL_checkinteger
	leal	(%rax,%rbx), %ecx
	cmpl	%ebx, %eax
	movl	%eax, %ebp
	cmovgl	%ebx, %ebp
	testl	%eax, %eax
	cmovsl	%ecx, %ebp
	testl	%ebp, %ebp
	jg	.LBB21_5
# BB#4:
	movl	$1, %esi
	movl	$.L.str.53, %edx
	movq	%r14, %rdi
	callq	luaL_argerror
.LBB21_5:
	subl	%ebp, %ebx
	movl	%ebx, %ebp
.LBB21_6:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end21:
	.size	luaB_select, .Lfunc_end21-luaB_select
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_setfenv,@function
luaB_setfenv:                           # @luaB_setfenv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 16
.Lcfi88:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	movl	$5, %edx
	callq	luaL_checktype
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	getfunc
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB22_3
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_tonumber
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB22_3
	jp	.LBB22_3
# BB#2:
	movq	%rbx, %rdi
	callq	lua_pushthread
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB22_3:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_iscfunction
	testl	%eax, %eax
	je	.LBB22_4
.LBB22_5:
	movl	$.L.str.54, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
	movl	$1, %eax
	popq	%rbx
	retq
.LBB22_4:
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_setfenv
	movl	%eax, %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	je	.LBB22_5
# BB#6:
	popq	%rbx
	retq
.Lfunc_end22:
	.size	luaB_setfenv, .Lfunc_end22-luaB_setfenv
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_setmetatable,@function
luaB_setmetatable:                      # @luaB_setmetatable
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	callq	lua_type
	movl	%eax, %ebp
	movl	$1, %esi
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	luaL_checktype
	testl	%ebp, %ebp
	je	.LBB23_3
# BB#1:
	cmpl	$5, %ebp
	je	.LBB23_3
# BB#2:
	movl	$2, %esi
	movl	$.L.str.55, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB23_3:
	movl	$1, %esi
	movl	$.L.str.46, %edx
	movq	%rbx, %rdi
	callq	luaL_getmetafield
	testl	%eax, %eax
	je	.LBB23_5
# BB#4:
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB23_5:
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_setmetatable
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end23:
	.size	luaB_setmetatable, .Lfunc_end23-luaB_setmetatable
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI24_0:
	.long	1127219200              # 0x43300000
	.long	1160773632              # 0x45300000
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI24_1:
	.quad	4841369599423283200     # double 4503599627370496
	.quad	4985484787499139072     # double 1.9342813113834067E+25
	.text
	.p2align	4, 0x90
	.type	luaB_tonumber,@function
luaB_tonumber:                          # @luaB_tonumber
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi96:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi97:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi98:
	.cfi_def_cfa_offset 48
.Lcfi99:
	.cfi_offset %rbx, -40
.Lcfi100:
	.cfi_offset %r12, -32
.Lcfi101:
	.cfi_offset %r14, -24
.Lcfi102:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$2, %esi
	movl	$10, %edx
	callq	luaL_optinteger
	movq	%rax, %rbx
	movl	$1, %esi
	cmpl	$10, %ebx
	jne	.LBB24_3
# BB#1:
	movq	%r15, %rdi
	callq	luaL_checkany
	movl	$1, %esi
	movq	%r15, %rdi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB24_12
# BB#2:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	lua_tonumber
	jmp	.LBB24_11
.LBB24_3:
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	luaL_checklstring
	movq	%rax, %r12
	leal	-2(%rbx), %eax
	cmpl	$35, %eax
	jb	.LBB24_5
# BB#4:
	movl	$2, %esi
	movl	$.L.str.57, %edx
	movq	%r15, %rdi
	callq	luaL_argerror
.LBB24_5:
	movq	%rsp, %rsi
	movq	%r12, %rdi
	movl	%ebx, %edx
	callq	strtoul
	movq	%rax, %r14
	movq	(%rsp), %rbx
	cmpq	%rbx, %r12
	je	.LBB24_12
# BB#6:                                 # %.preheader
	callq	__ctype_b_loc
	movq	(%rax), %rcx
	movzbl	(%rbx), %eax
	testb	$32, 1(%rcx,%rax,2)
	je	.LBB24_9
	.p2align	4, 0x90
.LBB24_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	1(%rbx), %eax
	incq	%rbx
	testb	$32, 1(%rcx,%rax,2)
	jne	.LBB24_7
# BB#8:                                 # %._crit_edge
	movq	%rbx, (%rsp)
.LBB24_9:
	testb	%al, %al
	je	.LBB24_10
.LBB24_12:
	movq	%r15, %rdi
	callq	lua_pushnil
	jmp	.LBB24_13
.LBB24_10:                              # %.critedge
	movd	%r14, %xmm1
	punpckldq	.LCPI24_0(%rip), %xmm1 # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	subpd	.LCPI24_1(%rip), %xmm1
	pshufd	$78, %xmm1, %xmm0       # xmm0 = xmm1[2,3,0,1]
	addpd	%xmm1, %xmm0
.LBB24_11:
	movq	%r15, %rdi
	callq	lua_pushnumber
.LBB24_13:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	luaB_tonumber, .Lfunc_end24-luaB_tonumber
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_tostring,@function
luaB_tostring:                          # @luaB_tostring
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movl	$1, %esi
	movl	$.L.str.58, %edx
	movq	%rbx, %rdi
	callq	luaL_callmeta
	testl	%eax, %eax
	je	.LBB25_1
.LBB25_8:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB25_1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$4, %eax
	ja	.LBB25_7
# BB#2:
	movl	%eax, %eax
	jmpq	*.LJTI25_0(,%rax,8)
.LBB25_6:
	movl	$.L.str.61, %esi
	movl	$3, %edx
	movq	%rbx, %rdi
	callq	lua_pushlstring
	jmp	.LBB25_8
.LBB25_5:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_toboolean
	testl	%eax, %eax
	movl	$.L.str.59, %eax
	movl	$.L.str.60, %esi
	cmovneq	%rax, %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	jmp	.LBB25_8
.LBB25_7:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_topointer
	movq	%rax, %rcx
	movl	$.L.str.62, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	jmp	.LBB25_8
.LBB25_3:
	movl	$1, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	lua_tolstring
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lua_pushstring
	jmp	.LBB25_8
.LBB25_4:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	jmp	.LBB25_8
.Lfunc_end25:
	.size	luaB_tostring, .Lfunc_end25-luaB_tostring
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI25_0:
	.quad	.LBB25_6
	.quad	.LBB25_5
	.quad	.LBB25_7
	.quad	.LBB25_3
	.quad	.LBB25_4

	.text
	.p2align	4, 0x90
	.type	luaB_type,@function
luaB_type:                              # @luaB_type
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 16
.Lcfi109:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checkany
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	movq	%rbx, %rdi
	movl	%eax, %esi
	callq	lua_typename
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lua_pushstring
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end26:
	.size	luaB_type, .Lfunc_end26-luaB_type
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_unpack,@function
luaB_unpack:                            # @luaB_unpack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 48
.Lcfi115:
	.cfi_offset %rbx, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, %esi
	movl	$5, %edx
	callq	luaL_checktype
	movl	$2, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	luaL_optinteger
	movq	%rax, %rbx
	movl	$3, %esi
	movq	%r14, %rdi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB27_1
# BB#2:
	movl	$3, %esi
	movq	%r14, %rdi
	callq	luaL_checkinteger
	jmp	.LBB27_3
.LBB27_1:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	lua_objlen
.LBB27_3:
	movq	%rax, %r15
	xorl	%eax, %eax
	movl	%r15d, %ebp
	subl	%ebx, %ebp
	jl	.LBB27_9
# BB#4:
	cmpl	%ebx, %r15d
	js	.LBB27_10
# BB#5:
	incl	%ebp
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	lua_checkstack
	testl	%eax, %eax
	je	.LBB27_10
# BB#6:
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	lua_rawgeti
	cmpl	%ebx, %r15d
	jle	.LBB27_8
	.p2align	4, 0x90
.LBB27_7:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incl	%ebx
	movl	$1, %esi
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	lua_rawgeti
	cmpl	%ebx, %r15d
	jne	.LBB27_7
.LBB27_8:
	movl	%ebp, %eax
.LBB27_9:                               # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_10:
	movl	$.L.str.63, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	luaL_error              # TAILCALL
.Lfunc_end27:
	.size	luaB_unpack, .Lfunc_end27-luaB_unpack
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_xpcall,@function
luaB_xpcall:                            # @luaB_xpcall
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 16
.Lcfi120:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$2, %esi
	callq	luaL_checkany
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	xorl	%esi, %esi
	movl	$-1, %edx
	movl	$1, %ecx
	movq	%rbx, %rdi
	callq	lua_pcall
	xorl	%esi, %esi
	testl	%eax, %eax
	sete	%sil
	movq	%rbx, %rdi
	callq	lua_pushboolean
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_replace
	movq	%rbx, %rdi
	popq	%rbx
	jmp	lua_gettop              # TAILCALL
.Lfunc_end28:
	.size	luaB_xpcall, .Lfunc_end28-luaB_xpcall
	.cfi_endproc

	.p2align	4, 0x90
	.type	getfunc,@function
getfunc:                                # @getfunc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 32
	subq	$128, %rsp
.Lcfi124:
	.cfi_def_cfa_offset 160
.Lcfi125:
	.cfi_offset %rbx, -32
.Lcfi126:
	.cfi_offset %r14, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	movl	$1, %esi
	cmpl	$6, %eax
	jne	.LBB29_1
# BB#11:
	movq	%rbx, %rdi
	addq	$128, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	lua_pushvalue           # TAILCALL
.LBB29_1:
	testl	%ebp, %ebp
	je	.LBB29_3
# BB#2:
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	luaL_optinteger
	jmp	.LBB29_4
.LBB29_3:
	movq	%rbx, %rdi
	callq	luaL_checkinteger
.LBB29_4:
	movq	%rax, %r14
	testl	%r14d, %r14d
	jns	.LBB29_6
# BB#5:
	movl	$1, %esi
	movl	$.L.str.42, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB29_6:
	leaq	8(%rsp), %rdx
	movq	%rbx, %rdi
	movl	%r14d, %esi
	callq	lua_getstack
	testl	%eax, %eax
	jne	.LBB29_8
# BB#7:
	movl	$1, %esi
	movl	$.L.str.43, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB29_8:
	leaq	8(%rsp), %rdx
	movl	$.L.str.44, %esi
	movq	%rbx, %rdi
	callq	lua_getinfo
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	jne	.LBB29_10
# BB#9:
	movl	$.L.str.45, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%r14d, %edx
	callq	luaL_error
.LBB29_10:
	addq	$128, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end29:
	.size	getfunc, .Lfunc_end29-getfunc
	.cfi_endproc

	.p2align	4, 0x90
	.type	generic_reader,@function
generic_reader:                         # @generic_reader
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -24
.Lcfi132:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movl	$2, %esi
	movl	$.L.str.48, %edx
	callq	luaL_checkstack
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_call
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB30_1
# BB#2:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isstring
	testl	%eax, %eax
	je	.LBB30_3
# BB#5:
	movl	$3, %esi
	movq	%rbx, %rdi
	callq	lua_replace
	movl	$3, %esi
	movq	%rbx, %rdi
	movq	%r14, %rdx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	lua_tolstring           # TAILCALL
.LBB30_1:
	movq	$0, (%r14)
	jmp	.LBB30_4
.LBB30_3:
	movl	$.L.str.49, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB30_4:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end30:
	.size	generic_reader, .Lfunc_end30-generic_reader
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_cocreate,@function
luaB_cocreate:                          # @luaB_cocreate
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 32
.Lcfi136:
	.cfi_offset %rbx, -24
.Lcfi137:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	lua_newthread
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$6, %eax
	jne	.LBB31_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_iscfunction
	testl	%eax, %eax
	je	.LBB31_3
.LBB31_2:
	movl	$1, %esi
	movl	$.L.str.71, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB31_3:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_xmove
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end31:
	.size	luaB_cocreate, .Lfunc_end31-luaB_cocreate
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_coresume,@function
luaB_coresume:                          # @luaB_coresume
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 48
.Lcfi143:
	.cfi_offset %rbx, -40
.Lcfi144:
	.cfi_offset %r14, -32
.Lcfi145:
	.cfi_offset %r15, -24
.Lcfi146:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$1, %esi
	callq	lua_tothread
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB32_2
# BB#1:
	movl	$1, %esi
	movl	$.L.str.72, %edx
	movq	%r15, %rdi
	callq	luaL_argerror
.LBB32_2:
	movq	%r15, %rdi
	callq	lua_gettop
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	auxresume
	movl	%eax, %ebx
	testl	%ebx, %ebx
	js	.LBB32_3
# BB#4:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	lua_pushboolean
	leal	1(%rbx), %ebp
	notl	%ebx
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	lua_insert
	jmp	.LBB32_5
.LBB32_3:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	lua_pushboolean
	movl	$-2, %esi
	movq	%r15, %rdi
	callq	lua_insert
	movl	$2, %ebp
.LBB32_5:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	luaB_coresume, .Lfunc_end32-luaB_coresume
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_corunning,@function
luaB_corunning:                         # @luaB_corunning
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 16
.Lcfi148:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	lua_pushthread
	testl	%eax, %eax
	je	.LBB33_2
# BB#1:
	movq	%rbx, %rdi
	callq	lua_pushnil
.LBB33_2:
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end33:
	.size	luaB_corunning, .Lfunc_end33-luaB_corunning
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_costatus,@function
luaB_costatus:                          # @luaB_costatus
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 24
	subq	$120, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 144
.Lcfi152:
	.cfi_offset %rbx, -24
.Lcfi153:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$1, %esi
	callq	lua_tothread
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB34_2
# BB#1:
	movl	$1, %esi
	movl	$.L.str.72, %edx
	movq	%r14, %rdi
	callq	luaL_argerror
.LBB34_2:
	cmpq	%r14, %rbx
	je	.LBB34_3
# BB#4:
	movq	%rbx, %rdi
	callq	lua_status
	cmpl	$1, %eax
	je	.LBB34_5
# BB#6:
	testl	%eax, %eax
	jne	.LBB34_10
# BB#7:
	movq	%rsp, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_getstack
	testl	%eax, %eax
	jle	.LBB34_9
# BB#8:
	movl	$2, %eax
	jmp	.LBB34_11
.LBB34_3:
	xorl	%eax, %eax
	jmp	.LBB34_11
.LBB34_5:
	movl	$1, %eax
	jmp	.LBB34_11
.LBB34_10:
	movl	$3, %eax
	jmp	.LBB34_11
.LBB34_9:
	movq	%rbx, %rdi
	callq	lua_gettop
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	leaq	1(%rcx,%rcx), %rax
.LBB34_11:                              # %costatus.exit
	movq	statnames(,%rax,8), %rsi
	movq	%r14, %rdi
	callq	lua_pushstring
	movl	$1, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end34:
	.size	luaB_costatus, .Lfunc_end34-luaB_costatus
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_cowrap,@function
luaB_cowrap:                            # @luaB_cowrap
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi155:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi156:
	.cfi_def_cfa_offset 32
.Lcfi157:
	.cfi_offset %rbx, -24
.Lcfi158:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	lua_newthread
	movq	%rax, %r14
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	cmpl	$6, %eax
	jne	.LBB35_2
# BB#1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_iscfunction
	testl	%eax, %eax
	je	.LBB35_3
.LBB35_2:
	movl	$1, %esi
	movl	$.L.str.71, %edx
	movq	%rbx, %rdi
	callq	luaL_argerror
.LBB35_3:                               # %luaB_cocreate.exit
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushvalue
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_xmove
	movl	$luaB_auxwrap, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	lua_pushcclosure
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end35:
	.size	luaB_cowrap, .Lfunc_end35-luaB_cowrap
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_yield,@function
luaB_yield:                             # @luaB_yield
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi159:
	.cfi_def_cfa_offset 16
.Lcfi160:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	lua_gettop
	movq	%rbx, %rdi
	movl	%eax, %esi
	popq	%rbx
	jmp	lua_yield               # TAILCALL
.Lfunc_end36:
	.size	luaB_yield, .Lfunc_end36-luaB_yield
	.cfi_endproc

	.p2align	4, 0x90
	.type	auxresume,@function
auxresume:                              # @auxresume
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi164:
	.cfi_def_cfa_offset 40
	subq	$120, %rsp
.Lcfi165:
	.cfi_def_cfa_offset 160
.Lcfi166:
	.cfi_offset %rbx, -40
.Lcfi167:
	.cfi_offset %r14, -32
.Lcfi168:
	.cfi_offset %r15, -24
.Lcfi169:
	.cfi_offset %rbp, -16
	movl	%edx, %r15d
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpq	%rbx, %r14
	je	.LBB37_1
# BB#2:
	movq	%rbx, %rdi
	callq	lua_status
	cmpl	$1, %eax
	je	.LBB37_3
# BB#4:
	testl	%eax, %eax
	jne	.LBB37_7
# BB#5:
	movq	%rsp, %rdx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	lua_getstack
	movl	$2, %ebp
	testl	%eax, %eax
	jg	.LBB37_8
# BB#6:
	movq	%rbx, %rdi
	callq	lua_gettop
	xorl	%ecx, %ecx
	testl	%eax, %eax
	sete	%cl
	leal	1(%rcx,%rcx), %ebp
	jmp	.LBB37_8
.LBB37_1:
	xorl	%ebp, %ebp
	jmp	.LBB37_8
.LBB37_3:
	movl	$1, %ebp
	jmp	.LBB37_8
.LBB37_7:
	movl	$3, %ebp
.LBB37_8:                               # %costatus.exit
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	lua_checkstack
	testl	%eax, %eax
	jne	.LBB37_10
# BB#9:
	movl	$.L.str.73, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	luaL_error
.LBB37_10:
	cmpl	$1, %ebp
	jne	.LBB37_11
# BB#12:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	lua_xmove
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	lua_setlevel
	movq	%rbx, %rdi
	movl	%r15d, %esi
	callq	lua_resume
	orl	$1, %eax
	cmpl	$1, %eax
	jne	.LBB37_16
# BB#13:
	movq	%rbx, %rdi
	callq	lua_gettop
	movl	%eax, %ebp
	leal	1(%rbp), %esi
	movq	%r14, %rdi
	callq	lua_checkstack
	testl	%eax, %eax
	jne	.LBB37_15
# BB#14:
	movl	$.L.str.75, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	luaL_error
.LBB37_15:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	lua_xmove
	jmp	.LBB37_18
.LBB37_11:
	movslq	%ebp, %rax
	movq	statnames(,%rax,8), %rdx
	movl	$.L.str.74, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	lua_pushfstring
	jmp	.LBB37_17
.LBB37_16:
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lua_xmove
.LBB37_17:
	movl	$-1, %ebp
.LBB37_18:
	movl	%ebp, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end37:
	.size	auxresume, .Lfunc_end37-auxresume
	.cfi_endproc

	.p2align	4, 0x90
	.type	luaB_auxwrap,@function
luaB_auxwrap:                           # @luaB_auxwrap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi170:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi171:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi172:
	.cfi_def_cfa_offset 32
.Lcfi173:
	.cfi_offset %rbx, -24
.Lcfi174:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$-10003, %esi           # imm = 0xD8ED
	callq	lua_tothread
	movq	%rax, %rbp
	movq	%rbx, %rdi
	callq	lua_gettop
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movl	%eax, %edx
	callq	auxresume
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jns	.LBB38_4
# BB#1:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isstring
	testl	%eax, %eax
	je	.LBB38_3
# BB#2:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaL_where
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_insert
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	lua_concat
.LBB38_3:
	movq	%rbx, %rdi
	callq	lua_error
.LBB38_4:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end38:
	.size	luaB_auxwrap, .Lfunc_end38-luaB_auxwrap
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"coroutine"
	.size	.L.str, 10

	.type	co_funcs,@object        # @co_funcs
	.section	.rodata,"a",@progbits
	.p2align	4
co_funcs:
	.quad	.L.str.65
	.quad	luaB_cocreate
	.quad	.L.str.66
	.quad	luaB_coresume
	.quad	.L.str.67
	.quad	luaB_corunning
	.quad	.L.str.68
	.quad	luaB_costatus
	.quad	.L.str.69
	.quad	luaB_cowrap
	.quad	.L.str.70
	.quad	luaB_yield
	.zero	16
	.size	co_funcs, 112

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"_G"
	.size	.L.str.1, 3

	.type	base_funcs,@object      # @base_funcs
	.section	.rodata,"a",@progbits
	.p2align	4
base_funcs:
	.quad	.L.str.9
	.quad	luaB_assert
	.quad	.L.str.10
	.quad	luaB_collectgarbage
	.quad	.L.str.11
	.quad	luaB_dofile
	.quad	.L.str.12
	.quad	luaB_error
	.quad	.L.str.13
	.quad	luaB_gcinfo
	.quad	.L.str.14
	.quad	luaB_getfenv
	.quad	.L.str.15
	.quad	luaB_getmetatable
	.quad	.L.str.16
	.quad	luaB_loadfile
	.quad	.L.str.17
	.quad	luaB_load
	.quad	.L.str.18
	.quad	luaB_loadstring
	.quad	.L.str.19
	.quad	luaB_next
	.quad	.L.str.20
	.quad	luaB_pcall
	.quad	.L.str.21
	.quad	luaB_print
	.quad	.L.str.22
	.quad	luaB_rawequal
	.quad	.L.str.23
	.quad	luaB_rawget
	.quad	.L.str.24
	.quad	luaB_rawset
	.quad	.L.str.25
	.quad	luaB_select
	.quad	.L.str.26
	.quad	luaB_setfenv
	.quad	.L.str.27
	.quad	luaB_setmetatable
	.quad	.L.str.28
	.quad	luaB_tonumber
	.quad	.L.str.29
	.quad	luaB_tostring
	.quad	.L.str.30
	.quad	luaB_type
	.quad	.L.str.31
	.quad	luaB_unpack
	.quad	.L.str.32
	.quad	luaB_xpcall
	.zero	16
	.size	base_funcs, 400

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"Lua 5.1"
	.size	.L.str.2, 8

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"_VERSION"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ipairs"
	.size	.L.str.4, 7

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"pairs"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"kv"
	.size	.L.str.6, 3

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"__mode"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"newproxy"
	.size	.L.str.8, 9

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"assert"
	.size	.L.str.9, 7

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"collectgarbage"
	.size	.L.str.10, 15

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"dofile"
	.size	.L.str.11, 7

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"error"
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"gcinfo"
	.size	.L.str.13, 7

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"getfenv"
	.size	.L.str.14, 8

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"getmetatable"
	.size	.L.str.15, 13

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"loadfile"
	.size	.L.str.16, 9

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"load"
	.size	.L.str.17, 5

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"loadstring"
	.size	.L.str.18, 11

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"next"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"pcall"
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"print"
	.size	.L.str.21, 6

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"rawequal"
	.size	.L.str.22, 9

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"rawget"
	.size	.L.str.23, 7

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"rawset"
	.size	.L.str.24, 7

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"select"
	.size	.L.str.25, 7

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"setfenv"
	.size	.L.str.26, 8

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"setmetatable"
	.size	.L.str.27, 13

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"tonumber"
	.size	.L.str.28, 9

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"tostring"
	.size	.L.str.29, 9

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"type"
	.size	.L.str.30, 5

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"unpack"
	.size	.L.str.31, 7

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"xpcall"
	.size	.L.str.32, 7

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%s"
	.size	.L.str.33, 3

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"assertion failed!"
	.size	.L.str.34, 18

	.type	luaB_collectgarbage.opts,@object # @luaB_collectgarbage.opts
	.section	.rodata,"a",@progbits
	.p2align	4
luaB_collectgarbage.opts:
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	0
	.size	luaB_collectgarbage.opts, 64

	.type	.L.str.35,@object       # @.str.35
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.35:
	.asciz	"stop"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"restart"
	.size	.L.str.36, 8

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"collect"
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"count"
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"step"
	.size	.L.str.39, 5

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"setpause"
	.size	.L.str.40, 9

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"setstepmul"
	.size	.L.str.41, 11

	.type	luaB_collectgarbage.optsnum,@object # @luaB_collectgarbage.optsnum
	.section	.rodata,"a",@progbits
	.p2align	4
luaB_collectgarbage.optsnum:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.size	luaB_collectgarbage.optsnum, 28

	.type	.L.str.42,@object       # @.str.42
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.42:
	.asciz	"level must be non-negative"
	.size	.L.str.42, 27

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"invalid level"
	.size	.L.str.43, 14

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"f"
	.size	.L.str.44, 2

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"no function environment for tail call at level %d"
	.size	.L.str.45, 50

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"__metatable"
	.size	.L.str.46, 12

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"=(load)"
	.size	.L.str.47, 8

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"too many nested functions"
	.size	.L.str.48, 26

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"reader function must return a string"
	.size	.L.str.49, 37

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"'tostring' must return a string to 'print'"
	.size	.L.str.50, 43

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"index out of range"
	.size	.L.str.53, 19

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"'setfenv' cannot change environment of given object"
	.size	.L.str.54, 52

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"nil or table expected"
	.size	.L.str.55, 22

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"cannot change a protected metatable"
	.size	.L.str.56, 36

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"base out of range"
	.size	.L.str.57, 18

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"__tostring"
	.size	.L.str.58, 11

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"true"
	.size	.L.str.59, 5

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"false"
	.size	.L.str.60, 6

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"nil"
	.size	.L.str.61, 4

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"%s: %p"
	.size	.L.str.62, 7

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"too many results to unpack"
	.size	.L.str.63, 27

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"boolean or proxy expected"
	.size	.L.str.64, 26

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"create"
	.size	.L.str.65, 7

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"resume"
	.size	.L.str.66, 7

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"running"
	.size	.L.str.67, 8

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"status"
	.size	.L.str.68, 7

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"wrap"
	.size	.L.str.69, 5

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"yield"
	.size	.L.str.70, 6

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"Lua function expected"
	.size	.L.str.71, 22

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"coroutine expected"
	.size	.L.str.72, 19

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"too many arguments to resume"
	.size	.L.str.73, 29

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"cannot resume %s coroutine"
	.size	.L.str.74, 27

	.type	statnames,@object       # @statnames
	.section	.rodata,"a",@progbits
	.p2align	4
statnames:
	.quad	.L.str.67
	.quad	.L.str.76
	.quad	.L.str.77
	.quad	.L.str.78
	.size	statnames, 32

	.type	.L.str.75,@object       # @.str.75
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.75:
	.asciz	"too many results to resume"
	.size	.L.str.75, 27

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"suspended"
	.size	.L.str.76, 10

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"normal"
	.size	.L.str.77, 7

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"dead"
	.size	.L.str.78, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
