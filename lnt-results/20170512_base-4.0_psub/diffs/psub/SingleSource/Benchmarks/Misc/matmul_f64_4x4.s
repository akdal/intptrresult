	.text
	.file	"matmul_f64_4x4.bc"
	.globl	wrap_mul4
	.p2align	4, 0x90
	.type	wrap_mul4,@function
wrap_mul4:                              # @wrap_mul4
	.cfi_startproc
# BB#0:
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	movsd	8(%rsi), %xmm0          # xmm0 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movupd	(%rdx), %xmm13
	movupd	16(%rdx), %xmm5
	movupd	32(%rdx), %xmm15
	movupd	48(%rdx), %xmm6
	movaps	%xmm2, %xmm1
	mulpd	%xmm13, %xmm1
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movaps	%xmm0, %xmm3
	mulpd	%xmm15, %xmm3
	addpd	%xmm1, %xmm3
	movupd	64(%rdx), %xmm10
	movsd	16(%rsi), %xmm4         # xmm4 = mem[0],zero
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	movaps	%xmm4, %xmm7
	mulpd	%xmm10, %xmm7
	addpd	%xmm3, %xmm7
	movupd	96(%rdx), %xmm14
	movsd	24(%rsi), %xmm12        # xmm12 = mem[0],zero
	movlhps	%xmm12, %xmm12          # xmm12 = xmm12[0,0]
	movaps	%xmm12, %xmm1
	mulpd	%xmm14, %xmm1
	addpd	%xmm7, %xmm1
	movapd	%xmm1, -24(%rsp)        # 16-byte Spill
	mulpd	%xmm5, %xmm2
	movapd	%xmm5, %xmm8
	movapd	%xmm6, %xmm5
	movapd	%xmm5, -72(%rsp)        # 16-byte Spill
	mulpd	%xmm5, %xmm0
	addpd	%xmm2, %xmm0
	movupd	80(%rdx), %xmm1
	mulpd	%xmm1, %xmm4
	movapd	%xmm1, %xmm9
	addpd	%xmm0, %xmm4
	movupd	112(%rdx), %xmm0
	movapd	%xmm0, -56(%rsp)        # 16-byte Spill
	mulpd	%xmm0, %xmm12
	addpd	%xmm4, %xmm12
	movsd	32(%rsi), %xmm2         # xmm2 = mem[0],zero
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movapd	%xmm13, %xmm0
	mulpd	%xmm2, %xmm0
	movsd	40(%rsi), %xmm4         # xmm4 = mem[0],zero
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	movapd	%xmm15, %xmm7
	mulpd	%xmm4, %xmm7
	addpd	%xmm0, %xmm7
	movsd	48(%rsi), %xmm1         # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	%xmm10, %xmm6
	mulpd	%xmm1, %xmm6
	addpd	%xmm7, %xmm6
	movsd	56(%rsi), %xmm3         # xmm3 = mem[0],zero
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	movapd	%xmm14, %xmm11
	mulpd	%xmm3, %xmm11
	addpd	%xmm6, %xmm11
	movapd	%xmm8, %xmm7
	movapd	%xmm7, -40(%rsp)        # 16-byte Spill
	mulpd	%xmm7, %xmm2
	mulpd	%xmm5, %xmm4
	addpd	%xmm2, %xmm4
	movapd	%xmm9, %xmm8
	mulpd	%xmm8, %xmm1
	addpd	%xmm4, %xmm1
	movapd	-56(%rsp), %xmm9        # 16-byte Reload
	mulpd	%xmm9, %xmm3
	addpd	%xmm1, %xmm3
	movsd	64(%rsi), %xmm1         # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movapd	%xmm13, %xmm2
	mulpd	%xmm1, %xmm2
	movsd	72(%rsi), %xmm6         # xmm6 = mem[0],zero
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	movapd	%xmm15, %xmm4
	mulpd	%xmm6, %xmm4
	addpd	%xmm2, %xmm4
	movsd	80(%rsi), %xmm0         # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movapd	%xmm10, %xmm5
	mulpd	%xmm0, %xmm5
	addpd	%xmm4, %xmm5
	movsd	88(%rsi), %xmm4         # xmm4 = mem[0],zero
	movlhps	%xmm4, %xmm4            # xmm4 = xmm4[0,0]
	movapd	%xmm14, %xmm2
	mulpd	%xmm4, %xmm2
	addpd	%xmm5, %xmm2
	mulpd	%xmm7, %xmm1
	movapd	-72(%rsp), %xmm7        # 16-byte Reload
	mulpd	%xmm7, %xmm6
	addpd	%xmm1, %xmm6
	mulpd	%xmm8, %xmm0
	addpd	%xmm6, %xmm0
	mulpd	%xmm9, %xmm4
	addpd	%xmm0, %xmm4
	movsd	96(%rsi), %xmm0         # xmm0 = mem[0],zero
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	mulpd	%xmm0, %xmm13
	movsd	104(%rsi), %xmm1        # xmm1 = mem[0],zero
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	mulpd	%xmm1, %xmm15
	addpd	%xmm13, %xmm15
	movsd	112(%rsi), %xmm5        # xmm5 = mem[0],zero
	movlhps	%xmm5, %xmm5            # xmm5 = xmm5[0,0]
	mulpd	%xmm5, %xmm10
	addpd	%xmm15, %xmm10
	movsd	120(%rsi), %xmm6        # xmm6 = mem[0],zero
	movlhps	%xmm6, %xmm6            # xmm6 = xmm6[0,0]
	mulpd	%xmm6, %xmm14
	addpd	%xmm10, %xmm14
	mulpd	-40(%rsp), %xmm0        # 16-byte Folded Reload
	mulpd	%xmm7, %xmm1
	addpd	%xmm0, %xmm1
	mulpd	%xmm8, %xmm5
	addpd	%xmm1, %xmm5
	mulpd	%xmm9, %xmm6
	addpd	%xmm5, %xmm6
	movaps	-24(%rsp), %xmm0        # 16-byte Reload
	movups	%xmm0, (%rdi)
	movupd	%xmm12, 16(%rdi)
	movupd	%xmm11, 32(%rdi)
	movupd	%xmm3, 48(%rdi)
	movupd	%xmm2, 64(%rdi)
	movupd	%xmm4, 80(%rdi)
	movupd	%xmm14, 96(%rdi)
	movupd	%xmm6, 112(%rdi)
	retq
.Lfunc_end0:
	.size	wrap_mul4, .Lfunc_end0-wrap_mul4
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$136, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 160
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movl	$50000000, %ebp         # imm = 0x2FAF080
	movq	%rsp, %rbx
	.p2align	4, 0x90
.LBB1_1:                                # =>This Inner Loop Header: Depth=1
	movl	$main.A, %esi
	movl	$main.B, %edx
	movq	%rbx, %rdi
	callq	wrap_mul4
	decl	%ebp
	jne	.LBB1_1
# BB#2:                                 # %.preheader.preheader
	movsd	(%rsp), %xmm0           # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	8(%rsp), %xmm0          # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	24(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movl	$10, %edi
	callq	putchar
	movsd	32(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	40(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	48(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movl	$10, %edi
	callq	putchar
	movsd	64(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	72(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	80(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	88(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movl	$10, %edi
	callq	putchar
	movsd	96(%rsp), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	104(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	112(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movsd	120(%rsp), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	movl	$10, %edi
	callq	putchar
	xorl	%eax, %eax
	addq	$136, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.type	main.A,@object          # @main.A
	.section	.rodata,"a",@progbits
	.p2align	4
main.A:
	.quad	4616752568008179712     # double 4.5
	.quad	4608533498688228557     # double 1.3
	.quad	4618441417868443648     # double 6
	.quad	4616302208045442662     # double 4.0999999999999996
	.quad	4612811918334230528     # double 2.5
	.quad	4619792497756654797     # double 7.2000000000000002
	.quad	4620355447710076109     # double 7.7000000000000001
	.quad	4610334938539176755     # double 1.7
	.quad	4619229547803233485     # double 6.7000000000000002
	.quad	4608533498688228557     # double 1.3
	.quad	4621481347616918733     # double 9.4000000000000003
	.quad	4608533498688228557     # double 1.3
	.quad	4607632778762754458     # double 1.1000000000000001
	.quad	4612136378390124954     # double 2.2000000000000002
	.quad	4613937818241073152     # double 3
	.quad	4611911198408756429     # double 2.1000000000000001
	.size	main.A, 128

	.type	main.B,@object          # @main.B
	.p2align	4
main.B:
	.quad	4607182418800017408     # double 1
	.quad	4620580627691444634     # double 7.9000000000000004
	.quad	4617428107952285286     # double 5.0999999999999996
	.quad	4614838538166547251     # double 3.3999999999999999
	.quad	4619116957812549222     # double 6.5999999999999996
	.quad	4613487458278336102     # double 2.7999999999999998
	.quad	4617765877924338074     # double 5.4000000000000004
	.quad	4621368757626234470     # double 9.1999999999999993
	.quad	4617315517961601024     # double 5
	.quad	4616302208045442662     # double 4.0999999999999996
	.quad	4616302208045442662     # double 4.0999999999999996
	.quad	4621762822593629389     # double 9.9000000000000003
	.quad	4620918397663497421     # double 8.4000000000000004
	.quad	4615514078110652826     # double 3.7000000000000002
	.quad	4621537642612260864     # double 9.5
	.quad	4618891777831180698     # double 6.4000000000000004
	.size	main.B, 128

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%8.2f"
	.size	.L.str, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
