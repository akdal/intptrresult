	.text
	.file	"number.bc"
	.globl	free_num
	.p2align	4, 0x90
	.type	free_num,@function
free_num:                               # @free_num
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#1:
	decl	12(%rdi)
	jne	.LBB0_3
# BB#2:
	callq	free
.LBB0_3:
	movq	$0, (%rbx)
.LBB0_4:
	popq	%rbx
	retq
.Lfunc_end0:
	.size	free_num, .Lfunc_end0-free_num
	.cfi_endproc

	.globl	new_num
	.p2align	4, 0x90
	.type	new_num,@function
new_num:                                # @new_num
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movslq	%edi, %rbx
	movslq	%esi, %r14
	leaq	1040(%rbx,%r14), %rdi
	callq	malloc
	movl	$0, (%rax)
	movl	%ebx, 4(%rax)
	movl	%r14d, 8(%rax)
	movl	$1, 12(%rax)
	movb	$0, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	new_num, .Lfunc_end1-new_num
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	init_numbers
	.p2align	4, 0x90
	.type	init_numbers,@function
init_numbers:                           # @init_numbers
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movl	$1041, %edi             # imm = 0x411
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,0,1]
	movups	%xmm0, (%rax)
	movb	$0, 16(%rax)
	movq	%rax, _zero_(%rip)
	movl	$1041, %edi             # imm = 0x411
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,0,1]
	movups	%xmm0, (%rax)
	movq	%rax, _one_(%rip)
	movb	$1, 16(%rax)
	movl	$1041, %edi             # imm = 0x411
	callq	malloc
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [0,1,0,1]
	movups	%xmm0, (%rax)
	movq	%rax, _two_(%rip)
	movb	$2, 16(%rax)
	popq	%rax
	retq
.Lfunc_end2:
	.size	init_numbers, .Lfunc_end2-init_numbers
	.cfi_endproc

	.globl	copy_num
	.p2align	4, 0x90
	.type	copy_num,@function
copy_num:                               # @copy_num
	.cfi_startproc
# BB#0:
	incl	12(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end3:
	.size	copy_num, .Lfunc_end3-copy_num
	.cfi_endproc

	.globl	init_num
	.p2align	4, 0x90
	.type	init_num,@function
init_num:                               # @init_num
	.cfi_startproc
# BB#0:
	movq	_zero_(%rip), %rax
	incl	12(%rax)
	movq	%rax, (%rdi)
	retq
.Lfunc_end4:
	.size	init_num, .Lfunc_end4-init_num
	.cfi_endproc

	.globl	int2num
	.p2align	4, 0x90
	.type	int2num,@function
int2num:                                # @int2num
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 80
.Lcfi14:
	.cfi_offset %rbx, -48
.Lcfi15:
	.cfi_offset %r12, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%esi, %r15d
	movq	%rdi, %r14
	movl	%r15d, %eax
	negl	%eax
	cmovll	%r15d, %eax
	movslq	%eax, %rcx
	imulq	$1717986919, %rcx, %rdx # imm = 0x66666667
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	movb	%sil, (%rsp)
	addl	$9, %ecx
	cmpl	$19, %ecx
	jb	.LBB5_4
# BB#1:                                 # %.lr.ph39.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	leal	(%rcx,%rdx), %eax
	movslq	%eax, %rsi
	imulq	$1717986919, %rsi, %rdi # imm = 0x66666667
	movq	%rdi, %rbp
	shrq	$63, %rbp
	sarq	$34, %rdi
	addl	%ebp, %edi
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %edi
	subl	%edi, %esi
	movb	%sil, (%rsp,%rbx)
	leal	9(%rcx,%rdx), %ecx
	incq	%rbx
	cmpl	$18, %ecx
	ja	.LBB5_2
# BB#3:                                 # %._crit_edge40.loopexit
	leaq	(%rsp,%rbx), %rbp
	jmp	.LBB5_5
.LBB5_4:
	leaq	1(%rsp), %rbp
	movl	$1, %ebx
.LBB5_5:                                # %._crit_edge40
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_9
# BB#6:
	decl	12(%rdi)
	jne	.LBB5_8
# BB#7:
	callq	free
.LBB5_8:
	movq	$0, (%r14)
.LBB5_9:                                # %free_num.exit
	movslq	%ebx, %r12
	leaq	1040(%r12), %rdi
	callq	malloc
	movl	$0, (%rax)
	movl	%r12d, 4(%rax)
	movl	$0, 8(%rax)
	movl	$1, 12(%rax)
	movb	$0, 16(%rax)
	movq	%rax, (%r14)
	testl	%r15d, %r15d
	jns	.LBB5_11
# BB#10:
	movl	$1, (%rax)
.LBB5_11:
	testl	%ebx, %ebx
	jle	.LBB5_24
# BB#12:                                # %.lr.ph.preheader
	leaq	16(%rax), %rcx
	movl	%ebx, %edx
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %esi
	cmovgl	%edx, %esi
	leal	1(%rbx,%rsi), %r9d
	leaq	1(%r9), %rsi
	cmpq	$32, %rsi
	jae	.LBB5_14
# BB#13:
	movq	%rbp, %rdx
	jmp	.LBB5_22
.LBB5_14:                               # %min.iters.checked
	movl	%esi, %r8d
	andl	$31, %r8d
	subq	%r8, %rsi
	je	.LBB5_18
# BB#15:                                # %vector.memcheck
	cmpl	$-3, %edx
	movl	$-2, %edi
	cmovgl	%edx, %edi
	cmpq	%rbp, %rcx
	jae	.LBB5_19
# BB#16:                                # %vector.memcheck
	leal	1(%rbx,%rdi), %edx
	leaq	17(%rax,%rdx), %rdi
	notq	%rdx
	addq	%rbp, %rdx
	cmpq	%rdi, %rdx
	jae	.LBB5_19
.LBB5_18:
	movq	%rbp, %rdx
	jmp	.LBB5_22
.LBB5_19:                               # %vector.body.preheader
	subl	%esi, %ebx
	addq	%rsi, %rcx
	leaq	-1(%r8), %rdx
	subq	%r9, %rdx
	addq	%rbp, %rdx
	addq	$-16, %rbp
	addq	$32, %rax
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB5_20:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbp), %xmm1
	movdqu	(%rbp), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rax)
	movdqu	%xmm1, (%rax)
	addq	$-32, %rbp
	addq	$32, %rax
	addq	$-32, %rsi
	jne	.LBB5_20
# BB#21:                                # %middle.block
	testq	%r8, %r8
	je	.LBB5_24
.LBB5_22:                               # %.lr.ph.preheader57
	incl	%ebx
	decq	%rdx
	.p2align	4, 0x90
.LBB5_23:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %eax
	movb	%al, (%rcx)
	incq	%rcx
	decl	%ebx
	decq	%rdx
	cmpl	$1, %ebx
	jg	.LBB5_23
.LBB5_24:                               # %._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	int2num, .Lfunc_end5-int2num
	.cfi_endproc

	.globl	num2long
	.p2align	4, 0x90
	.type	num2long,@function
num2long:                               # @num2long
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %esi
	testl	%esi, %esi
	jle	.LBB6_1
# BB#2:                                 # %.lr.ph.preheader
	leaq	16(%rdi), %rcx
	xorl	%eax, %eax
	movabsq	$922337203685477580, %r8 # imm = 0xCCCCCCCCCCCCCCC
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, %edx
	leaq	(%rax,%rax,4), %rax
	movsbq	(%rcx), %rsi
	leaq	(%rsi,%rax,2), %rax
	cmpq	%r8, %rax
	jg	.LBB6_5
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB6_3 Depth=1
	incq	%rcx
	leal	-1(%rdx), %esi
	cmpl	$1, %edx
	jg	.LBB6_3
.LBB6_5:                                # %._crit_edge.loopexit
	xorl	%ecx, %ecx
	cmpl	$1, %edx
	cmovgq	%rcx, %rax
	jmp	.LBB6_6
.LBB6_1:
	xorl	%eax, %eax
.LBB6_6:                                # %._crit_edge
	xorl	%ecx, %ecx
	testq	%rax, %rax
	cmovnsq	%rax, %rcx
	movq	%rcx, %rax
	negq	%rax
	cmpl	$0, (%rdi)
	cmoveq	%rcx, %rax
	retq
.Lfunc_end6:
	.size	num2long, .Lfunc_end6-num2long
	.cfi_endproc

	.globl	bc_compare
	.p2align	4, 0x90
	.type	bc_compare,@function
bc_compare:                             # @bc_compare
	.cfi_startproc
# BB#0:
	movl	$1, %edx
	xorl	%ecx, %ecx
	jmp	_do_compare             # TAILCALL
.Lfunc_end7:
	.size	bc_compare, .Lfunc_end7-bc_compare
	.cfi_endproc

	.p2align	4, 0x90
	.type	_do_compare,@function
_do_compare:                            # @_do_compare
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	testl	%edx, %edx
	je	.LBB8_2
# BB#1:
	movl	(%rdi), %eax
	cmpl	(%rsi), %eax
	jne	.LBB8_17
.LBB8_2:
	movl	4(%rdi), %r10d
	cmpl	4(%rsi), %r10d
	jne	.LBB8_14
# BB#3:
	movl	8(%rdi), %r8d
	movl	8(%rsi), %r9d
	cmpl	%r9d, %r8d
	movq	%rdi, %rax
	cmovgq	%rsi, %rax
	addl	8(%rax), %r10d
	jle	.LBB8_20
# BB#4:                                 # %.lr.ph84.preheader
	movl	%r10d, %r14d
	xorl	%r15d, %r15d
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph84
                                        # =>This Inner Loop Header: Depth=1
	movzbl	16(%rdi,%r15), %r11d
	cmpb	16(%rsi,%r15), %r11b
	jne	.LBB8_26
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=1
	incq	%r15
	decq	%rbx
	leal	(%r14,%rbx), %eax
	cmpl	$1, %eax
	jg	.LBB8_5
# BB#7:                                 # %._crit_edge.loopexit
	subl	%r15d, %r10d
	leaq	16(%rsi,%r15), %rsi
	leaq	16(%rdi,%r15), %rcx
	testl	%r10d, %r10d
	jne	.LBB8_21
	jmp	.LBB8_8
.LBB8_14:
	jle	.LBB8_18
# BB#15:
	testl	%edx, %edx
	jne	.LBB8_24
	jmp	.LBB8_16
.LBB8_17:
	cmpl	$1, %eax
	jmp	.LBB8_25
.LBB8_18:
	testl	%edx, %edx
	jne	.LBB8_30
	jmp	.LBB8_19
.LBB8_20:
	leaq	16(%rdi), %rcx
	addq	$16, %rsi
	testl	%r10d, %r10d
	je	.LBB8_8
.LBB8_21:                               # %._crit_edge..thread_crit_edge
	movb	(%rcx), %r11b
.LBB8_22:                               # %.thread
	cmpb	(%rsi), %r11b
	jle	.LBB8_29
# BB#23:
	testl	%edx, %edx
	je	.LBB8_16
.LBB8_24:
	cmpl	$1, (%rdi)
.LBB8_25:                               # %.loopexit
	sbbl	%eax, %eax
	notl	%eax
	jmp	.LBB8_31
.LBB8_26:                               # %.critedge
	leaq	16(%rsi,%r15), %rsi
	testl	%ecx, %ecx
	je	.LBB8_22
# BB#27:                                # %.critedge
	decl	%r10d
	cmpl	%r15d, %r10d
	jne	.LBB8_22
# BB#28:                                # %.critedge
	xorl	%eax, %eax
	cmpl	%r9d, %r8d
	je	.LBB8_32
	jmp	.LBB8_22
.LBB8_29:
	testl	%edx, %edx
	je	.LBB8_19
.LBB8_30:
	cmpl	$1, (%rdi)
	sbbl	%eax, %eax
.LBB8_31:                               # %.loopexit
	orl	$1, %eax
.LBB8_32:                               # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB8_8:
	xorl	%eax, %eax
	cmpl	%r9d, %r8d
	je	.LBB8_32
# BB#9:
	jle	.LBB8_33
# BB#10:
	cmpl	%r9d, %r8d
	jle	.LBB8_32
# BB#11:                                # %.lr.ph.preheader
	incl	%r8d
	subl	%r9d, %r8d
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rcx)
	jne	.LBB8_37
# BB#13:                                #   in Loop: Header=BB8_12 Depth=1
	incq	%rcx
	decl	%r8d
	cmpl	$1, %r8d
	jg	.LBB8_12
	jmp	.LBB8_32
.LBB8_33:
	cmpl	%r8d, %r9d
	jle	.LBB8_32
# BB#34:                                # %.lr.ph80.preheader
	incl	%r9d
	subl	%r8d, %r9d
	.p2align	4, 0x90
.LBB8_35:                               # %.lr.ph80
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB8_38
# BB#36:                                #   in Loop: Header=BB8_35 Depth=1
	incq	%rsi
	decl	%r9d
	cmpl	$1, %r9d
	jg	.LBB8_35
	jmp	.LBB8_32
.LBB8_37:
	testl	%edx, %edx
	jne	.LBB8_24
.LBB8_16:
	movl	$1, %eax
	jmp	.LBB8_32
.LBB8_38:
	testl	%edx, %edx
	jne	.LBB8_30
.LBB8_19:
	movl	$-1, %eax
	jmp	.LBB8_32
.Lfunc_end8:
	.size	_do_compare, .Lfunc_end8-_do_compare
	.cfi_endproc

	.globl	is_zero
	.p2align	4, 0x90
	.type	is_zero,@function
is_zero:                                # @is_zero
	.cfi_startproc
# BB#0:
	cmpq	%rdi, _zero_(%rip)
	je	.LBB9_5
# BB#1:
	movl	8(%rdi), %eax
	addl	4(%rdi), %eax
	jle	.LBB9_7
# BB#2:                                 # %.lr.ph.preheader
	addq	$16, %rdi
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdi)
	jne	.LBB9_7
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	incq	%rdi
	leal	-1(%rax), %ecx
	cmpl	$1, %eax
	movl	%ecx, %eax
	jg	.LBB9_3
	jmp	.LBB9_8
.LBB9_7:
	movl	%eax, %ecx
.LBB9_8:                                # %.thread
	testl	%ecx, %ecx
	sete	%al
	retq
.LBB9_5:
	movb	$1, %al
	retq
.Lfunc_end9:
	.size	is_zero, .Lfunc_end9-is_zero
	.cfi_endproc

	.globl	is_neg
	.p2align	4, 0x90
	.type	is_neg,@function
is_neg:                                 # @is_neg
	.cfi_startproc
# BB#0:
	cmpl	$1, (%rdi)
	sete	%al
	retq
.Lfunc_end10:
	.size	is_neg, .Lfunc_end10-is_neg
	.cfi_endproc

	.globl	bc_add
	.p2align	4, 0x90
	.type	bc_add,@function
bc_add:                                 # @bc_add
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 48
.Lcfi30:
	.cfi_offset %rbx, -40
.Lcfi31:
	.cfi_offset %r12, -32
.Lcfi32:
	.cfi_offset %r14, -24
.Lcfi33:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	(%r12), %eax
	cmpl	(%r15), %eax
	jne	.LBB11_2
# BB#1:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_do_add
	jmp	.LBB11_13
.LBB11_2:
	movl	4(%r12), %edx
	cmpl	4(%r15), %edx
	jne	.LBB11_8
# BB#3:
	movl	8(%r12), %r8d
	movl	8(%r15), %ecx
	cmpl	%ecx, %r8d
	movq	%r12, %rax
	cmovgq	%r15, %rax
	movl	8(%rax), %eax
	movl	%eax, %edi
	addl	%edx, %edi
	jle	.LBB11_9
# BB#4:                                 # %.lr.ph84.i.preheader
	leal	1(%rax,%rdx), %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB11_5:                               # %.lr.ph84.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	16(%r12,%rsi), %ebx
	movzbl	16(%r15,%rsi), %eax
	cmpb	%al, %bl
	jne	.LBB11_11
# BB#6:                                 #   in Loop: Header=BB11_5 Depth=1
	decl	%edx
	incq	%rsi
	cmpl	$1, %edx
	jg	.LBB11_5
# BB#7:                                 # %._crit_edge.i.loopexit
	subl	%esi, %edi
	leaq	16(%r15,%rsi), %rdx
	leaq	16(%r12,%rsi), %rsi
	testl	%edi, %edi
	jne	.LBB11_10
	jmp	.LBB11_14
.LBB11_8:
	jg	.LBB11_12
	jmp	.LBB11_25
.LBB11_9:
	leaq	16(%r12), %rsi
	leaq	16(%r15), %rdx
	testl	%edi, %edi
	je	.LBB11_14
.LBB11_10:                              # %._crit_edge..thread_crit_edge.i
	movb	(%rsi), %bl
	movb	(%rdx), %al
.LBB11_11:                              # %.thread.i
	cmpb	%al, %bl
	jle	.LBB11_25
.LBB11_12:                              # %_do_compare.exit
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_do_sub
.LBB11_13:
	movq	%rax, %rbx
	movl	(%r12), %eax
	jmp	.LBB11_26
.LBB11_14:
	cmpl	%ecx, %r8d
	jne	.LBB11_16
.LBB11_15:                              # %.loopexit
	movq	_zero_(%rip), %rbx
	incl	12(%rbx)
	jmp	.LBB11_27
.LBB11_16:
	jle	.LBB11_21
# BB#17:
	cmpl	%ecx, %r8d
	jle	.LBB11_15
# BB#18:                                # %.lr.ph.i.preheader
	incl	%r8d
	subl	%ecx, %r8d
.LBB11_19:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB11_12
# BB#20:                                #   in Loop: Header=BB11_19 Depth=1
	incq	%rsi
	decl	%r8d
	cmpl	$1, %r8d
	jg	.LBB11_19
	jmp	.LBB11_15
.LBB11_21:
	cmpl	%r8d, %ecx
	jle	.LBB11_15
# BB#22:                                # %.lr.ph80.i.preheader
	incl	%ecx
	subl	%r8d, %ecx
.LBB11_23:                              # %.lr.ph80.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdx)
	jne	.LBB11_25
# BB#24:                                #   in Loop: Header=BB11_23 Depth=1
	incq	%rdx
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB11_23
	jmp	.LBB11_15
.LBB11_25:                              # %.loopexit22
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_do_sub
	movq	%rax, %rbx
	movl	(%r15), %eax
.LBB11_26:
	movl	%eax, (%rbx)
.LBB11_27:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB11_31
# BB#28:
	decl	12(%rdi)
	jne	.LBB11_30
# BB#29:
	callq	free
.LBB11_30:
	movq	$0, (%r14)
.LBB11_31:                              # %free_num.exit
	movq	%rbx, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end11:
	.size	bc_add, .Lfunc_end11-bc_add
	.cfi_endproc

	.p2align	4, 0x90
	.type	_do_add,@function
_do_add:                                # @_do_add
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 96
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	8(%rbx), %ebp
	movl	8(%r14), %r12d
	cmpl	%r12d, %ebp
	movq	%r14, %rax
	cmovgq	%rbx, %rax
	movslq	4(%rbx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movslq	4(%r14), %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	cmpl	%edx, %ecx
	movq	%r14, %rcx
	cmovgq	%rbx, %rcx
	movslq	4(%rcx), %r15
	movslq	8(%rax), %r13
	leaq	1041(%r13,%r15), %rdi
	callq	malloc
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rbx, %r9
	movq	24(%rsp), %rbx          # 8-byte Reload
	leaq	(%r9,%rbx), %rdx
	leaq	(%r14,%rdi), %rsi
	leaq	16(%rax,%r13), %rcx
	leaq	1(%r15,%rcx), %rcx
	leaq	1(%r15), %r10
	decq	%rcx
	cmpl	%r12d, %ebp
	movl	$0, (%rax)
	movl	%r10d, 4(%rax)
	movl	%r13d, 8(%rax)
	movl	$1, 12(%rax)
	movb	$0, 16(%rax)
	movslq	%ebp, %r8
	leaq	15(%r8,%rdx), %r11
	movslq	%r12d, %rdx
	leaq	15(%rdx,%rsi), %r15
	jne	.LBB12_2
# BB#1:
	movl	%ebp, %r12d
	jmp	.LBB12_21
.LBB12_2:
	cmpl	%r12d, %ebp
	jle	.LBB12_5
# BB#3:                                 # %.lr.ph126.preheader
	movq	%r9, 8(%rsp)            # 8-byte Spill
	leaq	14(%r8,%rbx), %rsi
	leal	-1(%rbp), %edx
	subl	%r12d, %edx
	subq	%rdx, %rsi
	leaq	14(%r13,%r10), %rdi
	subq	%rdx, %rdi
	leaq	1(%rdx), %r9
	cmpq	$15, %r9
	ja	.LBB12_11
# BB#4:
	movq	8(%rsp), %r9            # 8-byte Reload
	jmp	.LBB12_16
.LBB12_5:                               # %.preheader102
	cmpl	%ebp, %r12d
	jle	.LBB12_21
# BB#6:                                 # %.lr.ph133.preheader
	movq	%r9, 8(%rsp)            # 8-byte Spill
	leaq	14(%rdx,%rdi), %rsi
	leal	-1(%r12), %ebx
	subl	%ebp, %ebx
	subq	%rbx, %rsi
	leaq	14(%r13,%r10), %r9
	subq	%rbx, %r9
	leaq	1(%rbx), %r8
	cmpq	$15, %r8
	jbe	.LBB12_19
# BB#7:                                 # %min.iters.checked
	movq	%r14, (%rsp)            # 8-byte Spill
	movl	%r8d, %r14d
	andl	$15, %r14d
	movq	%r8, %rdi
	subq	%r14, %rdi
	subq	%r14, %r8
	je	.LBB12_18
# BB#8:                                 # %vector.body.preheader
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	subl	%edi, %r12d
	negq	%rbx
	leaq	-1(%r14,%rbx), %rsi
	addq	%rsi, %rcx
	addq	%rsi, %r15
	addq	16(%rsp), %rdx          # 8-byte Folded Reload
	movq	(%rsp), %rsi            # 8-byte Reload
	leaq	8(%rsi,%rdx), %rbx
	addq	%r10, %r13
	leaq	8(%rax,%r13), %rdi
	.p2align	4, 0x90
.LBB12_9:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %r10
	movq	(%rbx), %rsi
	movq	%rsi, (%rdi)
	movq	%r10, -8(%rdi)
	addq	$-16, %rbx
	addq	$-16, %rdi
	addq	$-16, %r8
	jne	.LBB12_9
# BB#10:                                # %middle.block
	testq	%r14, %r14
	movq	(%rsp), %r14            # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	jne	.LBB12_19
	jmp	.LBB12_20
.LBB12_11:                              # %min.iters.checked182
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	%r14, (%rsp)            # 8-byte Spill
	movl	%r9d, %edi
	andl	$15, %edi
	movq	%r9, %r14
	subq	%rdi, %r14
	subq	%rdi, %r9
	je	.LBB12_15
# BB#12:                                # %vector.body178.preheader
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	subl	%r14d, %ebp
	negq	%rdx
	movq	%rdi, %r14
	leaq	-1(%rdi,%rdx), %rdi
	addq	%rdi, %rcx
	addq	%rdi, %r11
	addq	%rbx, %r8
	movq	8(%rsp), %rdx           # 8-byte Reload
	leaq	8(%rdx,%r8), %rbx
	addq	%r10, %r13
	leaq	8(%rax,%r13), %rdi
	.p2align	4, 0x90
.LBB12_13:                              # %vector.body178
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rbx), %r8
	movq	(%rbx), %rsi
	movq	%rsi, (%rdi)
	movq	%r8, -8(%rdi)
	addq	$-16, %rbx
	addq	$-16, %rdi
	addq	$-16, %r9
	jne	.LBB12_13
# BB#14:                                # %middle.block179
	testq	%r14, %r14
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%rsp), %r14            # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	jne	.LBB12_16
	jmp	.LBB12_17
.LBB12_15:
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%rsp), %r14            # 8-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_16:                              # %.lr.ph126
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r11), %ebx
	decq	%r11
	movb	%bl, (%rcx)
	decq	%rcx
	decl	%ebp
	cmpl	%r12d, %ebp
	jg	.LBB12_16
.LBB12_17:                              # %.loopexit.loopexit
	addq	%r9, %rsi
	addq	%rax, %rdi
	movq	%rsi, %r11
	movq	%rdi, %rcx
	movl	%r12d, %ebp
	jmp	.LBB12_21
.LBB12_18:
	movq	(%rsp), %r14            # 8-byte Reload
	.p2align	4, 0x90
.LBB12_19:                              # %.lr.ph133
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r15), %ebx
	decq	%r15
	movb	%bl, (%rcx)
	decq	%rcx
	decl	%r12d
	cmpl	%ebp, %r12d
	jg	.LBB12_19
.LBB12_20:                              # %.loopexit.loopexit137
	addq	%r14, %rsi
	addq	%rax, %r9
	movq	%rsi, %r15
	movq	%r9, %rcx
	movl	%ebp, %r12d
	movq	8(%rsp), %r9            # 8-byte Reload
.LBB12_21:                              # %.loopexit
	addl	4(%r14), %r12d
	xorl	%edi, %edi
	addl	4(%r9), %ebp
	jle	.LBB12_27
# BB#22:                                # %.loopexit
	testl	%r12d, %r12d
	jle	.LBB12_27
# BB#23:                                # %.lr.ph115.preheader
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB12_24:                              # %.lr.ph115
                                        # =>This Inner Loop Header: Depth=1
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill> %RBP<def>
	movzbl	(%r11), %esi
	decq	%r11
	movzbl	(%r15), %ebx
	decq	%r15
	addl	%edi, %esi
	leal	(%rsi,%rbx), %edx
	xorl	%edi, %edi
	cmpb	$9, %dl
	setg	%dil
	leal	246(%rbx,%rsi), %esi
	cmovlel	%edx, %esi
	movb	%sil, (%rcx)
	decq	%rcx
	cmpl	$2, %ebp
	leal	-1(%rbp), %ebp
	leal	-1(%r12), %ebx
	jl	.LBB12_28
# BB#25:                                # %.lr.ph115
                                        #   in Loop: Header=BB12_24 Depth=1
	cmpl	$1, %r12d
	movl	%ebx, %r12d
	jg	.LBB12_24
	jmp	.LBB12_28
.LBB12_27:
	movl	%r12d, %ebx
.LBB12_28:                              # %._crit_edge116
	testl	%ebp, %ebp
	cmovnel	%ebp, %ebx
	testl	%ebx, %ebx
	jle	.LBB12_31
# BB#29:                                # %.lr.ph.preheader
	testl	%ebp, %ebp
	cmoveq	%r15, %r11
	incl	%ebx
	.p2align	4, 0x90
.LBB12_30:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r11), %edx
	decq	%r11
	leal	(%rdx,%rdi), %ebp
	xorl	%esi, %esi
	cmpb	$9, %bpl
	setg	%sil
	leal	246(%rdx,%rdi), %edx
	cmovlel	%ebp, %edx
	movb	%dl, (%rcx)
	decq	%rcx
	decl	%ebx
	cmpl	$1, %ebx
	movl	%esi, %edi
	jg	.LBB12_30
	jmp	.LBB12_32
.LBB12_31:
	movl	%edi, %esi
.LBB12_32:                              # %._crit_edge
	testl	%esi, %esi
	je	.LBB12_34
# BB#33:
	incb	(%rcx)
.LBB12_34:
	leaq	16(%rax), %rcx
	movl	4(%rax), %edx
	movq	%rcx, %rsi
	cmpl	$2, %edx
	jl	.LBB12_37
	.p2align	4, 0x90
.LBB12_35:                              # %.lr.ph27.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB12_37
# BB#36:                                #   in Loop: Header=BB12_35 Depth=1
	incq	%rsi
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB12_35
.LBB12_37:                              # %.critedge.i
	movl	%edx, 4(%rax)
	addl	8(%rax), %edx
	jle	.LBB12_53
# BB#38:                                # %.lr.ph.i.preheader
	movl	%edx, %edi
	notl	%edi
	cmpl	$-3, %edi
	movl	$-2, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rbp,%rdx), %r9d
	incq	%r9
	cmpq	$32, %r9
	jb	.LBB12_51
# BB#39:                                # %min.iters.checked216
	movabsq	$8589934560, %r8        # imm = 0x1FFFFFFE0
	andq	%r9, %r8
	je	.LBB12_51
# BB#40:                                # %vector.memcheck
	cmpl	$-3, %edi
	movl	$-2, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rbp,%rdx), %edi
	leaq	1(%rsi,%rdi), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB12_42
# BB#41:                                # %vector.memcheck
	leaq	17(%rax,%rdi), %rdi
	cmpq	%rdi, %rsi
	jb	.LBB12_51
.LBB12_42:                              # %vector.body212.preheader
	leaq	-32(%r8), %rbp
	movl	%ebp, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB12_45
# BB#43:                                # %vector.body212.prol.preheader
	negq	%rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_44:                              # %vector.body212.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rbx), %xmm0
	movups	16(%rsi,%rbx), %xmm1
	movups	%xmm0, 16(%rax,%rbx)
	movups	%xmm1, 32(%rax,%rbx)
	addq	$32, %rbx
	incq	%rdi
	jne	.LBB12_44
	jmp	.LBB12_46
.LBB12_45:
	xorl	%ebx, %ebx
.LBB12_46:                              # %vector.body212.prol.loopexit
	cmpq	$96, %rbp
	jb	.LBB12_49
# BB#47:                                # %vector.body212.preheader.new
	movq	%r8, %rdi
	subq	%rbx, %rdi
	leaq	128(%rax,%rbx), %rbp
	leaq	112(%rsi,%rbx), %rbx
	.p2align	4, 0x90
.LBB12_48:                              # %vector.body212
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-128, %rdi
	jne	.LBB12_48
.LBB12_49:                              # %middle.block213
	cmpq	%r8, %r9
	je	.LBB12_53
# BB#50:
	addq	%r8, %rsi
	addq	%r8, %rcx
	subl	%r8d, %edx
.LBB12_51:                              # %.lr.ph.i.preheader250
	incl	%edx
	.p2align	4, 0x90
.LBB12_52:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ebx
	incq	%rsi
	movb	%bl, (%rcx)
	incq	%rcx
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB12_52
.LBB12_53:                              # %_rm_leading_zeros.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_do_add, .Lfunc_end12-_do_add
	.cfi_endproc

	.p2align	4, 0x90
	.type	_do_sub,@function
_do_sub:                                # @_do_sub
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 96
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movslq	4(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movslq	4(%rbp), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	%ecx, %eax
	movq	%rbp, %rax
	cmovgq	%r14, %rax
	movslq	4(%rax), %r12
	movslq	8(%r14), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movslq	8(%rbp), %r15
	movq	%r14, %rax
	cmovgq	%rbp, %rax
	cmpl	%ecx, %r15d
	movq	%rbp, %rcx
	cmovlq	%r14, %rcx
	movslq	8(%rcx), %rbx
	movl	4(%rax), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%r14, %rax
	cmovlq	%rbp, %rax
	movl	8(%rax), %r13d
	leaq	1040(%r12,%rbx), %rdi
	callq	malloc
	movq	%rbp, %r11
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbx, %rsi
	movl	$0, (%rax)
	movl	%r12d, 4(%rax)
	movl	%esi, 8(%rax)
	movl	$1, 12(%rax)
	movb	$0, 16(%rax)
	movq	%r14, 24(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	(%r14,%rcx), %rcx
	leaq	15(%rbp,%rcx), %r8
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, %rdx
	leaq	(%r11,%rcx), %rcx
	leaq	15(%r15,%rcx), %r9
	leaq	16(%rax,%r12), %rcx
	leaq	-1(%rsi,%rcx), %r14
	cmpl	%r13d, %ebp
	jne	.LBB13_1
# BB#34:
	cmpl	%ebp, %r15d
	jle	.LBB13_35
# BB#36:                                # %.lr.ph133.preheader
	leal	1(%r15), %r9d
	subl	%ebp, %r9d
	addq	%r12, %rsi
	leaq	15(%rax,%rsi), %r10
	movq	%rdx, %r14
	addq	%r15, %r14
	leaq	15(%r11,%r14), %rbx
	xorl	%edx, %edx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_37:                              # %.lr.ph133
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx,%rdx), %ebp
	addl	%r15d, %ebp
	movl	%ebp, %ecx
	negl	%ecx
	movl	$10, %edi
	subl	%ebp, %edi
	movl	%ecx, %r15d
	shrl	$31, %r15d
	testl	%ebp, %ebp
	cmovlel	%ecx, %edi
	movb	%dil, (%r10,%rdx)
	decq	%rdx
	leal	(%r9,%rdx), %ecx
	cmpl	$1, %ecx
	jg	.LBB13_37
# BB#10:                                # %.preheader.loopexit
	addq	%r14, %r11
	addq	%rax, %rsi
	leaq	15(%rdx,%rsi), %r14
	leaq	15(%rdx,%r11), %r9
	jmp	.LBB13_11
.LBB13_1:
	xorl	%r15d, %r15d
	movl	%ebp, %ebx
	subl	%r13d, %ebx
	jle	.LBB13_11
# BB#2:                                 # %.lr.ph141.preheader
	leal	-1(%r13), %ecx
	subl	%ebp, %ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%rbp,%rdx), %edx
	subl	%r13d, %edx
	leaq	1(%rdx), %r10
	cmpq	$15, %r10
	jbe	.LBB13_3
# BB#6:                                 # %min.iters.checked
	movl	%r10d, %edi
	andl	$15, %edi
	movq	%r10, %rcx
	subq	%rdi, %rcx
	subq	%rdi, %r10
	je	.LBB13_3
# BB#7:                                 # %vector.body.preheader
	subl	%ecx, %ebx
	negq	%rdx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	leaq	-1(%rdi,%rdx), %rcx
	addq	%rcx, %r14
	addq	%rcx, %r8
	addq	16(%rsp), %rbp          # 8-byte Folded Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	8(%rcx,%rbp), %rdx
	addq	%r12, %rsi
	leaq	8(%rax,%rsi), %rcx
	.p2align	4, 0x90
.LBB13_8:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movq	-8(%rdx), %r11
	movq	(%rdx), %rbp
	movq	%rbp, (%rcx)
	movq	%r11, -8(%rcx)
	addq	$-16, %rdx
	addq	$-16, %rcx
	addq	$-16, %r10
	jne	.LBB13_8
# BB#9:                                 # %middle.block
	cmpq	$0, 8(%rsp)             # 8-byte Folded Reload
	movl	4(%rsp), %edx           # 4-byte Reload
	jne	.LBB13_4
	jmp	.LBB13_12
.LBB13_3:
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB13_4:                               # %.lr.ph141.preheader212
	incl	%ebx
	.p2align	4, 0x90
.LBB13_5:                               # %.lr.ph141
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r8), %ecx
	decq	%r8
	movb	%cl, (%r14)
	decq	%r14
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB13_5
	jmp	.LBB13_12
.LBB13_35:
	xorl	%r15d, %r15d
.LBB13_11:                              # %.preheader
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB13_12:                              # %.preheader
	addl	%edx, %r13d
	testl	%r13d, %r13d
	jle	.LBB13_20
# BB#13:                                # %.lr.ph125.preheader
	leal	-1(%r13), %r11d
	testb	$1, %r13b
	jne	.LBB13_15
# BB#14:
	xorl	%r10d, %r10d
	movq	%r14, %rdx
	movq	%r8, %rcx
	jmp	.LBB13_16
.LBB13_15:                              # %.lr.ph125.prol
	movsbl	(%r8), %ebx
	movsbl	(%r9), %ecx
	subl	%ecx, %ebx
	movl	%ebx, %ebp
	subl	%r15d, %ebp
	addl	$10, %ebp
	subl	%r15d, %ebx
	cmovnsl	%ebx, %ebp
	leaq	-1(%r8), %rcx
	decq	%r9
	shrl	$31, %ebx
	leaq	-1(%r14), %rdx
	movb	%bpl, (%r14)
	movl	$1, %r10d
	movl	%ebx, %r15d
.LBB13_16:                              # %.lr.ph125.prol.loopexit
	notq	%r11
	cmpl	$1, %r13d
	je	.LBB13_19
# BB#17:                                # %.lr.ph125.preheader.new
	subl	%r10d, %r13d
	.p2align	4, 0x90
.LBB13_18:                              # %.lr.ph125
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rcx), %edi
	movsbl	(%r9), %ebx
	subl	%ebx, %edi
	movl	%edi, %ebp
	subl	%r15d, %ebp
	addl	$10, %ebp
	subl	%r15d, %edi
	cmovnsl	%edi, %ebp
	shrl	$31, %edi
	movb	%bpl, (%rdx)
	movsbl	-1(%rcx), %r15d
	movsbl	-1(%r9), %ebp
	subl	%ebp, %r15d
	movl	%r15d, %esi
	subl	%edi, %esi
	addl	$10, %esi
	subl	%edi, %r15d
	cmovnsl	%r15d, %esi
	shrl	$31, %r15d
	movb	%sil, -1(%rdx)
	addq	$-2, %rcx
	addq	$-2, %r9
	addq	$-2, %rdx
	addl	$-2, %r13d
	jne	.LBB13_18
.LBB13_19:                              # %._crit_edge.loopexit
	addq	%r11, %r8
	addq	%r11, %r14
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB13_20:                              # %._crit_edge
	cmpl	%edx, %r12d
	je	.LBB13_24
# BB#21:
	cmpl	%edx, %r12d
	jle	.LBB13_24
# BB#22:                                # %.lr.ph.preheader
	incl	%r12d
	subl	%edx, %r12d
	.p2align	4, 0x90
.LBB13_23:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%r8), %ecx
	movl	%ecx, %edx
	subl	%r15d, %edx
	addl	$10, %edx
	subl	%r15d, %ecx
	cmovnsl	%ecx, %edx
	decq	%r8
	shrl	$31, %ecx
	movb	%dl, (%r14)
	decq	%r14
	decl	%r12d
	cmpl	$1, %r12d
	movl	%ecx, %r15d
	jg	.LBB13_23
.LBB13_24:                              # %.loopexit
	leaq	16(%rax), %rcx
	movl	4(%rax), %edx
	movq	%rcx, %rsi
	cmpl	$2, %edx
	jl	.LBB13_27
	.p2align	4, 0x90
.LBB13_25:                              # %.lr.ph27.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB13_27
# BB#26:                                #   in Loop: Header=BB13_25 Depth=1
	incq	%rsi
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB13_25
.LBB13_27:                              # %.critedge.i
	movl	%edx, 4(%rax)
	addl	8(%rax), %edx
	jle	.LBB13_47
# BB#28:                                # %.lr.ph.i.preheader
	movl	%edx, %edi
	notl	%edi
	cmpl	$-3, %edi
	movl	$-2, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rbp,%rdx), %r9d
	incq	%r9
	cmpq	$32, %r9
	jb	.LBB13_45
# BB#29:                                # %min.iters.checked179
	movabsq	$8589934560, %r8        # imm = 0x1FFFFFFE0
	andq	%r9, %r8
	je	.LBB13_45
# BB#30:                                # %vector.memcheck
	cmpl	$-3, %edi
	movl	$-2, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rbp,%rdx), %edi
	leaq	1(%rsi,%rdi), %rbp
	cmpq	%rbp, %rcx
	jae	.LBB13_32
# BB#31:                                # %vector.memcheck
	leaq	17(%rax,%rdi), %rdi
	cmpq	%rdi, %rsi
	jb	.LBB13_45
.LBB13_32:                              # %vector.body174.preheader
	leaq	-32(%r8), %rbx
	movl	%ebx, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB13_33
# BB#38:                                # %vector.body174.prol.preheader
	negq	%rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_39:                              # %vector.body174.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi,%rbp), %xmm0
	movups	16(%rsi,%rbp), %xmm1
	movups	%xmm0, 16(%rax,%rbp)
	movups	%xmm1, 32(%rax,%rbp)
	addq	$32, %rbp
	incq	%rdi
	jne	.LBB13_39
	jmp	.LBB13_40
.LBB13_33:
	xorl	%ebp, %ebp
.LBB13_40:                              # %vector.body174.prol.loopexit
	cmpq	$96, %rbx
	jb	.LBB13_43
# BB#41:                                # %vector.body174.preheader.new
	movq	%r8, %rdi
	subq	%rbp, %rdi
	leaq	128(%rax,%rbp), %rbx
	leaq	112(%rsi,%rbp), %rbp
	.p2align	4, 0x90
.LBB13_42:                              # %vector.body174
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbp), %xmm0
	movups	-96(%rbp), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rbp), %xmm0
	movups	-64(%rbp), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rbp), %xmm0
	movups	-32(%rbp), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rbp), %xmm0
	movups	(%rbp), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rbp
	addq	$-128, %rdi
	jne	.LBB13_42
.LBB13_43:                              # %middle.block175
	cmpq	%r8, %r9
	je	.LBB13_47
# BB#44:
	addq	%r8, %rsi
	addq	%r8, %rcx
	subl	%r8d, %edx
.LBB13_45:                              # %.lr.ph.i.preheader211
	incl	%edx
	.p2align	4, 0x90
.LBB13_46:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rsi), %ebx
	incq	%rsi
	movb	%bl, (%rcx)
	incq	%rcx
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB13_46
.LBB13_47:                              # %_rm_leading_zeros.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_do_sub, .Lfunc_end13-_do_sub
	.cfi_endproc

	.globl	bc_sub
	.p2align	4, 0x90
	.type	bc_sub,@function
bc_sub:                                 # @bc_sub
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 48
.Lcfi65:
	.cfi_offset %rbx, -40
.Lcfi66:
	.cfi_offset %r12, -32
.Lcfi67:
	.cfi_offset %r14, -24
.Lcfi68:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	(%r12), %eax
	cmpl	(%r15), %eax
	jne	.LBB14_7
# BB#1:
	movl	4(%r12), %edx
	cmpl	4(%r15), %edx
	jne	.LBB14_8
# BB#2:
	movl	8(%r12), %r8d
	movl	8(%r15), %ecx
	cmpl	%ecx, %r8d
	movq	%r12, %rax
	cmovgq	%r15, %rax
	movl	8(%rax), %eax
	movl	%eax, %edi
	addl	%edx, %edi
	jle	.LBB14_9
# BB#3:                                 # %.lr.ph84.i.preheader
	leal	1(%rax,%rdx), %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB14_4:                               # %.lr.ph84.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	16(%r12,%rsi), %ebx
	movzbl	16(%r15,%rsi), %eax
	cmpb	%al, %bl
	jne	.LBB14_11
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=1
	decl	%edx
	incq	%rsi
	cmpl	$1, %edx
	jg	.LBB14_4
# BB#6:                                 # %._crit_edge.i.loopexit
	subl	%esi, %edi
	leaq	16(%r15,%rsi), %rdx
	leaq	16(%r12,%rsi), %rsi
	testl	%edi, %edi
	jne	.LBB14_10
	jmp	.LBB14_14
.LBB14_7:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_do_add
	jmp	.LBB14_13
.LBB14_8:
	jg	.LBB14_12
	jmp	.LBB14_25
.LBB14_9:
	leaq	16(%r12), %rsi
	leaq	16(%r15), %rdx
	testl	%edi, %edi
	je	.LBB14_14
.LBB14_10:                              # %._crit_edge..thread_crit_edge.i
	movb	(%rsi), %bl
	movb	(%rdx), %al
.LBB14_11:                              # %.thread.i
	cmpb	%al, %bl
	jle	.LBB14_25
.LBB14_12:                              # %_do_compare.exit
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	_do_sub
.LBB14_13:
	movq	%rax, %rbx
	movl	(%r12), %eax
	jmp	.LBB14_26
.LBB14_14:
	cmpl	%ecx, %r8d
	jne	.LBB14_16
.LBB14_15:                              # %.loopexit
	movq	_zero_(%rip), %rbx
	incl	12(%rbx)
	jmp	.LBB14_27
.LBB14_16:
	jle	.LBB14_21
# BB#17:
	cmpl	%ecx, %r8d
	jle	.LBB14_15
# BB#18:                                # %.lr.ph.i.preheader
	incl	%r8d
	subl	%ecx, %r8d
.LBB14_19:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB14_12
# BB#20:                                #   in Loop: Header=BB14_19 Depth=1
	incq	%rsi
	decl	%r8d
	cmpl	$1, %r8d
	jg	.LBB14_19
	jmp	.LBB14_15
.LBB14_21:
	cmpl	%r8d, %ecx
	jle	.LBB14_15
# BB#22:                                # %.lr.ph80.i.preheader
	incl	%ecx
	subl	%r8d, %ecx
.LBB14_23:                              # %.lr.ph80.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdx)
	jne	.LBB14_25
# BB#24:                                #   in Loop: Header=BB14_23 Depth=1
	incq	%rdx
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB14_23
	jmp	.LBB14_15
.LBB14_25:                              # %.loopexit22
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_do_sub
	movq	%rax, %rbx
	xorl	%eax, %eax
	cmpl	$0, (%r15)
	sete	%al
.LBB14_26:
	movl	%eax, (%rbx)
.LBB14_27:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB14_31
# BB#28:
	decl	12(%rdi)
	jne	.LBB14_30
# BB#29:
	callq	free
.LBB14_30:
	movq	$0, (%r14)
.LBB14_31:                              # %free_num.exit
	movq	%rbx, (%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	bc_sub, .Lfunc_end14-bc_sub
	.cfi_endproc

	.globl	bc_multiply
	.p2align	4, 0x90
	.type	bc_multiply,@function
bc_multiply:                            # @bc_multiply
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi69:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi72:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi73:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 144
.Lcfi76:
	.cfi_offset %rbx, -56
.Lcfi77:
	.cfi_offset %r12, -48
.Lcfi78:
	.cfi_offset %r13, -40
.Lcfi79:
	.cfi_offset %r14, -32
.Lcfi80:
	.cfi_offset %r15, -24
.Lcfi81:
	.cfi_offset %rbp, -16
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r13
	movl	8(%r13), %eax
	movl	4(%r13), %r14d
	addl	%eax, %r14d
	movl	4(%r15), %edx
	movl	8(%r15), %esi
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	leal	(%rsi,%rdx), %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leal	(%rdx,%r14), %edx
	leal	(%rsi,%rax), %ebp
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	cmpl	%esi, %eax
	movq	%r15, %rax
	cmovgq	%r13, %rax
	movl	8(%rax), %eax
	cmpl	%ecx, %eax
	cmovll	%ecx, %eax
	cmpl	%eax, %ebp
	cmovlel	%ebp, %eax
	subl	%ebp, %edx
	movslq	%edx, %r12
	movslq	%eax, %rbx
	leaq	1040(%r12,%rbx), %rdi
	callq	malloc
	movl	%r12d, 4(%rax)
	movl	%ebx, 8(%rax)
	movl	$1, 12(%rax)
	movb	$0, 16(%rax)
	movl	(%r13), %edx
	xorl	%ecx, %ecx
	cmpl	(%r15), %edx
	setne	%cl
	leaq	16(%r13), %rsi
	movq	8(%rsp), %r8            # 8-byte Reload
	leal	-1(%r8,%r14), %r9d
	movl	%ebp, %edi
	subl	%ebx, %edi
	movl	%r9d, %edx
	subl	%edi, %edx
	subl	%ebx, %ebp
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%ecx, (%rax)
	movslq	%r14d, %rbx
	leaq	15(%r13,%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movslq	%r8d, %rdi
	movslq	%edx, %rdx
	movq	%r15, 80(%rsp)          # 8-byte Spill
	jle	.LBB15_9
# BB#1:                                 # %.lr.ph169
	movl	%r9d, 36(%rsp)          # 4-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	leal	-1(%rdi), %eax
	movslq	%ebp, %r8
	movslq	%eax, %r11
	leaq	-1(%rdi), %r14
	movq	%r13, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	leaq	(%r13,%rbx), %r12
	leaq	15(%r15,%rdi), %rbp
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_5 Depth 2
	testq	%r14, %r14
	movl	$0, %eax
	cmovnsq	%r14, %rax
	movq	%r10, %rdx
	subq	%rdi, %rdx
	cmpq	$-1, %rdx
	notq	%rdx
	movl	$0, %ebx
	cmovlq	%rbx, %rdx
	cmpq	%r11, %r10
	movl	%r11d, %ebx
	cmovlel	%r10d, %ebx
	testl	%ebx, %ebx
	js	.LBB15_7
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	addq	48(%rsp), %rdx          # 8-byte Folded Reload
	cmpq	%rsi, %rdx
	jb	.LBB15_7
# BB#4:                                 # %.lr.ph164.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	negq	%rax
	movslq	%ebx, %rdx
	notq	%rdx
	leaq	(%rdi,%rax), %r15
	movq	%r12, %r13
	.p2align	4, 0x90
.LBB15_5:                               # %.lr.ph164
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rbx
	movsbl	1(%rbp,%rdx), %ecx
	movsbl	14(%r15,%r13), %r9d
	imull	%ecx, %r9d
	movslq	%r9d, %rcx
	addq	%rbx, %rcx
	cmpq	$-2, %rdx
	jg	.LBB15_7
# BB#6:                                 # %.lr.ph164
                                        #   in Loop: Header=BB15_5 Depth=2
	leaq	(%r13,%rdi), %rbx
	leaq	13(%rax,%rbx), %rbx
	incq	%rdx
	decq	%r13
	cmpq	%rsi, %rbx
	jae	.LBB15_5
.LBB15_7:                               # %.critedge
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%rcx, %rax
	movabsq	$7378697629483820647, %rcx # imm = 0x6666666666666667
	imulq	%rcx
	movq	%rdx, %rcx
	movq	%rcx, %rax
	shrq	$63, %rax
	sarq	$2, %rcx
	addq	%rax, %rcx
	incq	%r10
	decq	%r14
	decq	%r12
	cmpq	%r8, %r10
	jl	.LBB15_2
# BB#8:                                 # %.preheader.loopexit
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	36(%rsp), %r9d          # 4-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB15_10
.LBB15_9:
	xorl	%ecx, %ecx
	xorl	%r10d, %r10d
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	%r13, %rbp
.LBB15_10:                              # %.preheader
	leaq	16(%r14,%rdx), %r11
	cmpl	%r9d, %r10d
	jge	.LBB15_21
# BB#11:                                # %.lr.ph156
	leal	-1(%rdi), %eax
	movslq	%r10d, %r13
	movslq	%eax, %r8
	addq	$15, %rdx
	movq	64(%rsp), %r14          # 8-byte Reload
	addl	%ebx, %r14d
	movq	72(%rsp), %rax          # 8-byte Reload
	leal	-2(%rax,%r14), %eax
	subl	%r10d, %eax
	subq	%rax, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movslq	%r9d, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	-1(%rdi), %r15
	subq	%r13, %r15
	subq	%r13, %rbx
	addq	%rbx, %rbp
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	15(%rax,%rdi), %r9
	.p2align	4, 0x90
.LBB15_12:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_15 Depth 2
	testq	%r15, %r15
	movl	$0, %eax
	cmovnsq	%r15, %rax
	movq	%r13, %rdx
	subq	%rdi, %rdx
	cmpq	$-1, %rdx
	notq	%rdx
	movl	$0, %ebx
	cmovlq	%rbx, %rdx
	cmpq	%r8, %r13
	movl	%r8d, %ebx
	cmovlel	%r13d, %ebx
	testl	%ebx, %ebx
	js	.LBB15_18
# BB#13:                                #   in Loop: Header=BB15_12 Depth=1
	addq	48(%rsp), %rdx          # 8-byte Folded Reload
	cmpq	%rsi, %rdx
	jb	.LBB15_18
# BB#14:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_12 Depth=1
	negq	%rax
	movslq	%ebx, %rdx
	notq	%rdx
	leaq	(%rdi,%rax), %r12
	movq	%rbp, %r10
	movq	%rbp, %r14
	.p2align	4, 0x90
.LBB15_15:                              # %.lr.ph
                                        #   Parent Loop BB15_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rbx
	movsbl	1(%r9,%rdx), %ecx
	movsbl	14(%r12,%r14), %ebp
	imull	%ecx, %ebp
	movslq	%ebp, %rcx
	addq	%rbx, %rcx
	cmpq	$-2, %rdx
	jg	.LBB15_19
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB15_15 Depth=2
	leaq	(%r14,%rdi), %rbx
	leaq	13(%rax,%rbx), %rbx
	incq	%rdx
	decq	%r14
	cmpq	%rsi, %rbx
	jae	.LBB15_15
	jmp	.LBB15_19
	.p2align	4, 0x90
.LBB15_18:                              #   in Loop: Header=BB15_12 Depth=1
	movq	%rbp, %r10
.LBB15_19:                              # %.critedge5
                                        #   in Loop: Header=BB15_12 Depth=1
	movq	%rcx, %rax
	movabsq	$7378697629483820647, %rdx # imm = 0x6666666666666667
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$2, %rdx
	leal	(%rdx,%rax), %ebx
	addl	%ebx, %ebx
	leal	(%rbx,%rbx,4), %ebx
	subl	%ebx, %ecx
	movb	%cl, (%r11)
	decq	%r11
	movq	%rdx, %rcx
	addq	%rax, %rcx
	incq	%r13
	decq	%r15
	movq	%r10, %rbp
	decq	%rbp
	cmpq	8(%rsp), %r13           # 8-byte Folded Reload
	jne	.LBB15_12
# BB#20:                                # %._crit_edge.loopexit
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	addq	%r14, %r11
.LBB15_21:                              # %._crit_edge
	movb	%cl, (%r11)
	movq	40(%rsp), %r9           # 8-byte Reload
	movq	(%r9), %rdi
	testq	%rdi, %rdi
	je	.LBB15_25
# BB#22:
	decl	12(%rdi)
	jne	.LBB15_24
# BB#23:
	callq	free
	movq	40(%rsp), %r9           # 8-byte Reload
.LBB15_24:
	movq	$0, (%r9)
.LBB15_25:                              # %free_num.exit
	leaq	16(%r14), %rax
	movq	%r14, (%r9)
	movl	4(%r14), %ecx
	movq	%rax, %rdx
	cmpl	$2, %ecx
	jl	.LBB15_28
	.p2align	4, 0x90
.LBB15_26:                              # %.lr.ph27.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdx)
	jne	.LBB15_28
# BB#27:                                #   in Loop: Header=BB15_26 Depth=1
	incq	%rdx
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB15_26
.LBB15_28:                              # %.critedge.i
	movl	%ecx, 4(%r14)
	addl	8(%r14), %ecx
	jle	.LBB15_45
# BB#29:                                # %.lr.ph.i.preheader
	movl	%ecx, %esi
	notl	%esi
	cmpl	$-3, %esi
	movl	$-2, %edi
	cmovgl	%esi, %edi
	leal	1(%rdi,%rcx), %edi
	incq	%rdi
	cmpq	$32, %rdi
	jb	.LBB15_42
# BB#30:                                # %min.iters.checked
	movabsq	$8589934560, %r8        # imm = 0x1FFFFFFE0
	andq	%rdi, %r8
	je	.LBB15_42
# BB#31:                                # %vector.memcheck
	cmpl	$-3, %esi
	movl	$-2, %ebp
	cmovgl	%esi, %ebp
	leal	1(%rbp,%rcx), %esi
	leaq	1(%rdx,%rsi), %rbp
	cmpq	%rbp, %rax
	jae	.LBB15_33
# BB#32:                                # %vector.memcheck
	leaq	17(%r14,%rsi), %rsi
	cmpq	%rsi, %rdx
	jb	.LBB15_42
.LBB15_33:                              # %vector.body.preheader
	leaq	-32(%r8), %rbp
	movl	%ebp, %esi
	shrl	$5, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB15_36
# BB#34:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_35:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdx,%rbx), %xmm0
	movups	16(%rdx,%rbx), %xmm1
	movups	%xmm0, 16(%r14,%rbx)
	movups	%xmm1, 32(%r14,%rbx)
	addq	$32, %rbx
	incq	%rsi
	jne	.LBB15_35
	jmp	.LBB15_37
.LBB15_36:
	xorl	%ebx, %ebx
.LBB15_37:                              # %vector.body.prol.loopexit
	cmpq	$96, %rbp
	jb	.LBB15_40
# BB#38:                                # %vector.body.preheader.new
	movq	%r8, %rsi
	subq	%rbx, %rsi
	leaq	128(%r14,%rbx), %rbp
	leaq	112(%rdx,%rbx), %rbx
	.p2align	4, 0x90
.LBB15_39:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rbx
	addq	$-128, %rsi
	jne	.LBB15_39
.LBB15_40:                              # %middle.block
	cmpq	%r8, %rdi
	je	.LBB15_44
# BB#41:
	addq	%r8, %rdx
	addq	%r8, %rax
	subl	%r8d, %ecx
.LBB15_42:                              # %.lr.ph.i.preheader197
	incl	%ecx
	.p2align	4, 0x90
.LBB15_43:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %ebx
	incq	%rdx
	movb	%bl, (%rax)
	incq	%rax
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB15_43
.LBB15_44:                              # %_rm_leading_zeros.exit.loopexit
	movq	(%r9), %r14
.LBB15_45:                              # %_rm_leading_zeros.exit
	cmpq	%r14, _zero_(%rip)
	je	.LBB15_53
# BB#46:
	movl	8(%r14), %eax
	addl	4(%r14), %eax
	jle	.LBB15_50
# BB#47:                                # %.lr.ph.preheader.i
	leaq	16(%r14), %rcx
	.p2align	4, 0x90
.LBB15_48:                              # %.lr.ph.i147
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rcx)
	jne	.LBB15_51
# BB#49:                                #   in Loop: Header=BB15_48 Depth=1
	incq	%rcx
	leal	-1(%rax), %edx
	cmpl	$1, %eax
	movl	%edx, %eax
	jg	.LBB15_48
	jmp	.LBB15_52
.LBB15_50:
	movl	%eax, %edx
	testl	%edx, %edx
	jne	.LBB15_54
	jmp	.LBB15_53
.LBB15_51:
	movl	%eax, %edx
.LBB15_52:                              # %is_zero.exit
	testl	%edx, %edx
	jne	.LBB15_54
.LBB15_53:                              # %is_zero.exit.thread
	movl	$0, (%r14)
.LBB15_54:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	bc_multiply, .Lfunc_end15-bc_multiply
	.cfi_endproc

	.globl	bc_divide
	.p2align	4, 0x90
	.type	bc_divide,@function
bc_divide:                              # @bc_divide
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi85:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi86:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 256
.Lcfi89:
	.cfi_offset %rbx, -56
.Lcfi90:
	.cfi_offset %r12, -48
.Lcfi91:
	.cfi_offset %r13, -40
.Lcfi92:
	.cfi_offset %r14, -32
.Lcfi93:
	.cfi_offset %r15, -24
.Lcfi94:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movl	$-1, %eax
	cmpq	%rbx, _zero_(%rip)
	je	.LBB16_113
# BB#1:
	movl	4(%rbx), %r10d
	movl	8(%rbx), %r9d
	movl	%r9d, %ebp
	addl	%r10d, %ebp
	jle	.LBB16_2
# BB#3:                                 # %.lr.ph.preheader.i
	leaq	16(%rbx), %rsi
	.p2align	4, 0x90
.LBB16_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB16_5
# BB#6:                                 #   in Loop: Header=BB16_4 Depth=1
	incq	%rsi
	leal	-1(%rbp), %edi
	cmpl	$1, %ebp
	movl	%edi, %ebp
	jg	.LBB16_4
	jmp	.LBB16_7
.LBB16_2:
	movl	%ebp, %edi
	testl	%edi, %edi
	jne	.LBB16_8
	jmp	.LBB16_113
.LBB16_5:
	movl	%ebp, %edi
.LBB16_7:                               # %is_zero.exit
	testl	%edi, %edi
	je	.LBB16_113
.LBB16_8:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	testl	%r9d, %r9d
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	je	.LBB16_9
.LBB16_19:                              # %.lr.ph326.preheader
	movslq	%r10d, %rax
	movl	%r9d, %r15d
	leaq	15(%rbx,%rax), %rax
	movq	%r10, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB16_20:                              # %.lr.ph326
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rax,%r15)
	jne	.LBB16_23
# BB#21:                                #   in Loop: Header=BB16_20 Depth=1
	decq	%r15
	testl	%r15d, %r15d
	jne	.LBB16_20
# BB#22:
	xorl	%r15d, %r15d
	jmp	.LBB16_23
.LBB16_9:
	xorl	%r15d, %r15d
	cmpl	$1, %r10d
	jne	.LBB16_10
# BB#11:
	movl	$1, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpb	$1, 16(%rbx)
	jne	.LBB16_23
# BB#12:
	movslq	4(%r12), %r13
	movslq	8(%rsp), %r14           # 4-byte Folded Reload
	leaq	1040(%r14,%r13), %rdi
	callq	malloc
	movq	%rax, %rbp
	movl	%r13d, 4(%rbp)
	movl	%r14d, 8(%rbp)
	movl	$1, 12(%rbp)
	movb	$0, 16(%rbp)
	movl	(%r12), %eax
	xorl	%ecx, %ecx
	cmpl	(%rbx), %eax
	setne	%cl
	movl	%ecx, (%rbp)
	leaq	16(%rbp,%r13), %rdi
	leaq	16(%rbp), %rbx
	xorl	%r15d, %r15d
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memset
	leaq	16(%r12), %rsi
	movl	8(%r12), %eax
	cmpl	%r14d, %eax
	cmovgl	8(%rsp), %eax           # 4-byte Folded Reload
	addl	%r13d, %eax
	movslq	%eax, %rdx
	movq	%rbx, %rdi
	callq	memcpy
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB16_13
# BB#14:
	decl	12(%rdi)
	je	.LBB16_17
# BB#15:                                # %.thread364
	movq	%rbp, (%r14)
.LBB16_16:                              # %.critedge
	xorl	%r15d, %r15d
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB16_23
.LBB16_10:
	movq	%r10, (%rsp)            # 8-byte Spill
.LBB16_23:                              # %.critedge
	movl	4(%r12), %r14d
	movl	8(%r12), %eax
	leal	(%r14,%r15), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%eax, %edx
	subl	%r15d, %edx
	xorl	%ecx, %ecx
	movq	8(%rsp), %r13           # 8-byte Reload
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	subl	%edx, %r13d
	cmovlel	%ecx, %r13d
	addl	%eax, %r14d
	movq	%r13, 80(%rsp)          # 8-byte Spill
	leal	2(%r13,%r14), %ebp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memset
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	leaq	1(%rbx), %rdi
	leaq	16(%r12), %rsi
	movslq	%r14d, %rdx
	callq	memcpy
	movq	(%rsp), %rax            # 8-byte Reload
	leal	(%rax,%r15), %ebp
	leal	1(%rax,%r15), %edi
	callq	malloc
	movq	%rax, %rbx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %rsi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	callq	memcpy
	movq	%rbp, %rcx
	movb	$0, (%rbx,%rcx)
	cmpb	$0, (%rbx)
	je	.LBB16_25
# BB#24:
	movq	%rbx, %r12
	jmp	.LBB16_27
.LBB16_25:                              # %.lr.ph318.preheader
	movq	%rbx, %r12
	.p2align	4, 0x90
.LBB16_26:                              # %.lr.ph318
                                        # =>This Inner Loop Header: Depth=1
	decl	%ecx
	cmpb	$0, 1(%r12)
	leaq	1(%r12), %r12
	je	.LBB16_26
.LBB16_27:                              # %._crit_edge319
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r15), %eax
	movb	$1, %r13b
	subl	%ecx, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	%r15d, %ebp
	jb	.LBB16_29
# BB#28:
	cmpl	%ecx, 16(%rsp)          # 4-byte Folded Reload
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, %ebp
	cmovbl	%r15d, %ebp
	xorl	%r13d, %r13d
.LBB16_29:
	incl	%ebp
	movl	%ebp, %eax
	subl	%r15d, %eax
	movslq	%eax, %rbx
	movslq	%r15d, %rax
	leaq	1040(%rax,%rbx), %rdi
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	callq	malloc
	movl	$0, (%rax)
	movl	%ebx, 4(%rax)
	movl	%r15d, 8(%rax)
	movl	$1, 12(%rax)
	leaq	16(%rax), %rdi
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movb	$0, 16(%rax)
	xorl	%esi, %esi
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movq	%rbp, %rdx
	callq	memset
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	1(%rax), %edi
	movq	%rdi, (%rsp)            # 8-byte Spill
	callq	malloc
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	%rax, %r11
	testb	%r13b, %r13b
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	%r11, 56(%rsp)          # 8-byte Spill
	jne	.LBB16_80
# BB#30:
	movsbl	(%r12), %ecx
	incl	%ecx
	xorl	%r8d, %r8d
	movl	$10, %eax
	xorl	%edx, %edx
	idivl	%ecx
	cmpl	$1, %eax
	je	.LBB16_46
# BB#31:
	addl	80(%rsp), %r14d         # 4-byte Folded Reload
	incl	%r14d
	movslq	%r14d, %rdx
	testl	%eax, %eax
	je	.LBB16_32
# BB#33:
	testl	%r14d, %r14d
	jle	.LBB16_38
# BB#34:                                # %.lr.ph.preheader.i252
	leaq	-1(%r10,%rdx), %rcx
	movq	72(%rsp), %rsi          # 8-byte Reload
	cmpl	%esi, %r15d
	cmovgel	%r15d, %esi
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	2(%rsi,%rdx), %esi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_35:                              # %.lr.ph.i253
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rcx), %edi
	imull	%eax, %edi
	addl	%edx, %edi
	movslq	%edi, %rbp
	imulq	$1717986919, %rbp, %rdx # imm = 0x66666667
	movq	%rdx, %rbx
	shrq	$63, %rbx
	sarq	$34, %rdx
	addl	%ebx, %edx
	leal	(%rdx,%rdx), %ebx
	leal	(%rbx,%rbx,4), %ebx
	subl	%ebx, %ebp
	movb	%bpl, (%rcx)
	decq	%rcx
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB16_35
# BB#36:                                # %._crit_edge.i
	addl	$9, %edi
	cmpl	$19, %edi
	jb	.LBB16_38
# BB#37:
	movb	%dl, (%rcx)
.LBB16_38:                              # %_one_mult.exit
	cmpl	$1, %eax
	je	.LBB16_46
# BB#39:                                # %_one_mult.exit
	testl	%eax, %eax
	je	.LBB16_40
# BB#41:
	testl	%r9d, %r9d
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	movl	$0, %r8d
	jle	.LBB16_46
# BB#42:                                # %.lr.ph.preheader.i278
	movslq	%r9d, %rcx
	leaq	-1(%r12,%rcx), %rcx
	xorl	%edx, %edx
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	.p2align	4, 0x90
.LBB16_43:                              # %.lr.ph.i285
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rcx), %edi
	imull	%eax, %edi
	addl	%edx, %edi
	movslq	%edi, %rbp
	imulq	$1717986919, %rbp, %rdx # imm = 0x66666667
	movq	%rdx, %rbx
	shrq	$63, %rbx
	sarq	$34, %rdx
	addl	%ebx, %edx
	leal	(%rdx,%rdx), %ebx
	leal	(%rbx,%rbx,4), %ebx
	subl	%ebx, %ebp
	movb	%bpl, (%rcx)
	decq	%rcx
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB16_43
# BB#44:                                # %._crit_edge.i287
	addl	$9, %edi
	cmpl	$19, %edi
	jb	.LBB16_46
# BB#45:
	movb	%dl, (%rcx)
	jmp	.LBB16_46
.LBB16_32:                              # %_one_mult.exit.thread
	xorl	%esi, %esi
	movq	%r10, %rdi
	callq	memset
	movq	24(%rsp), %r9           # 8-byte Reload
.LBB16_40:
	movslq	%r9d, %rdx
	xorl	%esi, %esi
	movq	%r12, %rdi
	callq	memset
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	xorl	%r8d, %r8d
.LBB16_46:                              # %_one_mult.exit288
	movl	%r9d, %ecx
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	16(%rax,%rcx), %r13
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	movl	%edx, %eax
	subq	%rax, %r13
	cmpl	%r9d, %edx
	cmovaeq	104(%rsp), %r13         # 8-byte Folded Reload
	movslq	%r9d, %rdx
	leaq	(%r12,%rcx), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	leaq	1(%r11,%rdx), %rax
	decq	%rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	leaq	-1(%rcx), %rax
	leal	-1(%r9), %esi
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	subq	%rsi, %rax
	addq	%r10, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	andl	$1, %eax
	movl	%eax, 72(%rsp)          # 4-byte Spill
	leaq	-1(%r11,%rcx), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	1(%r11), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leaq	(%r11,%rcx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r9d, %eax
	andl	$1, %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leaq	-1(%r12,%rcx), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	leaq	-1(%r12,%rdx), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB16_47:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_58 Depth 2
                                        #     Child Loop BB16_66 Depth 2
                                        #     Child Loop BB16_75 Depth 2
	movl	%r8d, %r14d
	movsbl	(%r12), %ecx
	movsbl	(%r10,%r14), %eax
	cmpb	%al, %cl
	jne	.LBB16_49
# BB#48:                                # %._crit_edge350
                                        #   in Loop: Header=BB16_47 Depth=1
	addl	%eax, %eax
	leal	(%rax,%rax,4), %esi
	leal	1(%r14), %r8d
	leaq	(%r10,%r8), %rdi
	movl	$9, %r15d
	jmp	.LBB16_50
	.p2align	4, 0x90
.LBB16_49:                              #   in Loop: Header=BB16_47 Depth=1
	addl	%eax, %eax
	leal	(%rax,%rax,4), %esi
	leal	1(%r14), %r8d
	leaq	(%r10,%r8), %rdi
	movsbl	(%r10,%r8), %eax
	addl	%esi, %eax
	cltd
	idivl	%ecx
	movl	%eax, %r15d
.LBB16_50:                              #   in Loop: Header=BB16_47 Depth=1
	movsbl	1(%r12), %ebx
	movl	%ebx, %eax
	imull	%r15d, %eax
	movsbl	(%rdi), %edx
	addl	%esi, %edx
	movl	%ecx, %esi
	imull	%r15d, %esi
	movl	%edx, %edi
	subl	%esi, %edi
	leal	(%rdi,%rdi,4), %edi
	leal	2(%r14), %esi
	movsbl	(%r10,%rsi), %esi
	leal	(%rsi,%rdi,2), %edi
	cmpl	%edi, %eax
	jbe	.LBB16_52
# BB#51:                                #   in Loop: Header=BB16_47 Depth=1
	leal	-1(%r15), %eax
	imull	%eax, %ebx
	imull	%eax, %ecx
	subl	%ecx, %edx
	leal	(%rdx,%rdx,4), %ecx
	leal	(%rsi,%rcx,2), %ecx
	addl	$-2, %r15d
	cmpl	%ecx, %ebx
	cmovbel	%eax, %r15d
.LBB16_52:                              #   in Loop: Header=BB16_47 Depth=1
	testl	%r15d, %r15d
	je	.LBB16_53
# BB#54:                                #   in Loop: Header=BB16_47 Depth=1
	movb	$0, (%r11)
	cmpl	$1, %r15d
	movq	%r8, 8(%rsp)            # 8-byte Spill
	jne	.LBB16_56
# BB#55:                                #   in Loop: Header=BB16_47 Depth=1
	movq	184(%rsp), %rdi         # 8-byte Reload
	movq	%r12, %rsi
	movq	192(%rsp), %rdx         # 8-byte Reload
	callq	memcpy
	movq	24(%rsp), %r9           # 8-byte Reload
	movq	56(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	jne	.LBB16_62
	jmp	.LBB16_78
	.p2align	4, 0x90
.LBB16_53:                              #   in Loop: Header=BB16_47 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB16_79
	.p2align	4, 0x90
.LBB16_56:                              #   in Loop: Header=BB16_47 Depth=1
	testl	%r9d, %r9d
	jle	.LBB16_61
# BB#57:                                # %.lr.ph.i273.preheader
                                        #   in Loop: Header=BB16_47 Depth=1
	xorl	%eax, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, %edx
	movq	160(%rsp), %rsi         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB16_58:                              # %.lr.ph.i273
                                        #   Parent Loop BB16_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rsi), %edi
	imull	%r15d, %edi
	addl	%eax, %edi
	movslq	%edi, %rbx
	imulq	$1717986919, %rbx, %rax # imm = 0x66666667
	movq	%rax, %rbp
	shrq	$63, %rbp
	sarq	$34, %rax
	addl	%ebp, %eax
	leal	(%rax,%rax), %ebp
	leal	(%rbp,%rbp,4), %ebp
	subl	%ebp, %ebx
	movb	%bl, (%rcx)
	decq	%rcx
	decq	%rsi
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB16_58
# BB#59:                                # %._crit_edge.i275
                                        #   in Loop: Header=BB16_47 Depth=1
	addl	$9, %edi
	cmpl	$19, %edi
	jb	.LBB16_61
# BB#60:                                #   in Loop: Header=BB16_47 Depth=1
	movb	%al, (%rcx)
	.p2align	4, 0x90
.LBB16_61:                              # %_one_mult.exit276
                                        #   in Loop: Header=BB16_47 Depth=1
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	je	.LBB16_78
.LBB16_62:                              # %.lr.ph.preheader
                                        #   in Loop: Header=BB16_47 Depth=1
	leaq	(%r10,%r14), %r8
	addq	80(%rsp), %r8           # 8-byte Folded Reload
	xorl	%edi, %edi
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movl	$0, %ebx
	movq	%r8, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	je	.LBB16_64
# BB#63:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB16_47 Depth=1
	movsbl	(%r8), %edi
	movq	16(%rsp), %rax          # 8-byte Reload
	movsbl	(%rax), %edx
	movl	%edi, %esi
	subl	%edx, %esi
	addl	$10, %esi
	subl	%edx, %edi
	cmovnsl	%edi, %esi
	shrl	$31, %edi
	leaq	-1(%r8), %rdx
	movb	%sil, (%r8)
	movl	$1, %ebx
	movq	168(%rsp), %rsi         # 8-byte Reload
.LBB16_64:                              # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB16_47 Depth=1
	testl	%r9d, %r9d
	je	.LBB16_67
# BB#65:                                # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB16_47 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	%eax, %r9d
	subl	%ebx, %r9d
	.p2align	4, 0x90
.LBB16_66:                              # %.lr.ph
                                        #   Parent Loop BB16_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rdx), %eax
	movsbl	(%rsi), %ebx
	subl	%ebx, %eax
	movl	%eax, %ebp
	subl	%edi, %ebp
	addl	$10, %ebp
	subl	%edi, %eax
	cmovnsl	%eax, %ebp
	shrl	$31, %eax
	movb	%bpl, (%rdx)
	movsbl	-1(%rdx), %edi
	movsbl	-1(%rsi), %ebp
	subl	%ebp, %edi
	movl	%edi, %ecx
	subl	%eax, %ecx
	addl	$10, %ecx
	subl	%eax, %edi
	cmovnsl	%edi, %ecx
	shrl	$31, %edi
	movb	%cl, -1(%rdx)
	addq	$-2, %rdx
	addq	$-2, %rsi
	addl	$-2, %r9d
	jne	.LBB16_66
.LBB16_67:                              # %._crit_edge
                                        #   in Loop: Header=BB16_47 Depth=1
	cmpl	$1, %edi
	jne	.LBB16_68
# BB#69:                                #   in Loop: Header=BB16_47 Depth=1
	decl	%r15d
	movq	24(%rsp), %r9           # 8-byte Reload
	testl	%r9d, %r9d
	je	.LBB16_78
# BB#70:                                # %.lr.ph310.preheader
                                        #   in Loop: Header=BB16_47 Depth=1
	xorl	%ecx, %ecx
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	jne	.LBB16_72
# BB#71:                                #   in Loop: Header=BB16_47 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
	xorl	%edi, %edi
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jne	.LBB16_74
	jmp	.LBB16_76
.LBB16_68:                              #   in Loop: Header=BB16_47 Depth=1
	movq	24(%rsp), %r9           # 8-byte Reload
	jmp	.LBB16_78
.LBB16_72:                              # %.lr.ph310.prol
                                        #   in Loop: Header=BB16_47 Depth=1
	movsbl	(%r8), %eax
	movq	120(%rsp), %rbx         # 8-byte Reload
	movsbl	(%rbx), %edx
	leal	(%rax,%rdx), %esi
	xorl	%ecx, %ecx
	cmpl	$9, %esi
	setg	%cl
	leal	246(%rax,%rdx), %eax
	cmovlel	%esi, %eax
	movb	%al, (%r8)
	decq	%r8
	movl	$1, %edi
	movq	%rbx, %rdx
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	je	.LBB16_76
.LBB16_74:                              # %.lr.ph310.preheader.new
                                        #   in Loop: Header=BB16_47 Depth=1
	decq	%rdx
	movl	%r9d, %esi
	subl	%edi, %esi
	.p2align	4, 0x90
.LBB16_75:                              # %.lr.ph310
                                        #   Parent Loop BB16_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%r8), %eax
	movsbl	(%rdx), %edi
	addl	%ecx, %eax
	leal	(%rax,%rdi), %ecx
	xorl	%ebp, %ebp
	cmpl	$9, %ecx
	setg	%bpl
	leal	246(%rdi,%rax), %eax
	cmovlel	%ecx, %eax
	movb	%al, (%r8)
	movsbl	-1(%r8), %eax
	movsbl	-1(%rdx), %edi
	addl	%ebp, %eax
	leal	(%rax,%rdi), %ebp
	xorl	%ecx, %ecx
	cmpl	$9, %ebp
	setg	%cl
	leal	246(%rdi,%rax), %eax
	cmovlel	%ebp, %eax
	movb	%al, -1(%r8)
	addq	$-2, %rdx
	addq	$-2, %r8
	addl	$-2, %esi
	jne	.LBB16_75
.LBB16_76:                              # %._crit_edge311
                                        #   in Loop: Header=BB16_47 Depth=1
	testb	$1, %cl
	je	.LBB16_78
# BB#77:                                #   in Loop: Header=BB16_47 Depth=1
	movq	128(%rsp), %rsi         # 8-byte Reload
	movsbl	(%rsi,%r14), %eax
	leal	1(%rax), %ecx
	movslq	%ecx, %rcx
	imulq	$1717986919, %rcx, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$34, %rcx
	addl	%edx, %ecx
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,4), %ecx
	negl	%ecx
	leal	1(%rax,%rcx), %eax
	movb	%al, (%rsi,%r14)
	.p2align	4, 0x90
.LBB16_78:                              # %.thread
                                        #   in Loop: Header=BB16_47 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
.LBB16_79:                              # %.thread
                                        #   in Loop: Header=BB16_47 Depth=1
	movb	%r15b, (%r13)
	incq	%r13
	cmpl	52(%rsp), %r8d          # 4-byte Folded Reload
	jbe	.LBB16_47
.LBB16_80:                              # %.loopexit
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	xorl	%ecx, %ecx
	movq	64(%rsp), %rdx          # 8-byte Reload
	cmpl	(%rdx), %eax
	setne	%cl
	movq	112(%rsp), %r12         # 8-byte Reload
	movl	%ecx, (%r12)
	cmpq	%r12, _zero_(%rip)
	movl	4(%r12), %eax
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	144(%rsp), %r15         # 8-byte Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	je	.LBB16_88
# BB#81:
	movl	8(%r12), %ecx
	addl	%eax, %ecx
	jle	.LBB16_82
# BB#83:                                # %.lr.ph.i259.preheader
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB16_84:                              # %.lr.ph.i259
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rdx)
	jne	.LBB16_85
# BB#86:                                #   in Loop: Header=BB16_84 Depth=1
	incq	%rdx
	leal	-1(%rcx), %esi
	cmpl	$1, %ecx
	movl	%esi, %ecx
	jg	.LBB16_84
	jmp	.LBB16_87
.LBB16_82:
	movl	%ecx, %esi
	testl	%esi, %esi
	jne	.LBB16_89
	jmp	.LBB16_88
.LBB16_85:
	movl	%ecx, %esi
.LBB16_87:                              # %is_zero.exit264
	testl	%esi, %esi
	jne	.LBB16_89
.LBB16_88:                              # %is_zero.exit264.thread
	movl	$0, (%r12)
.LBB16_89:
	movq	%rbx, %rcx
	cmpl	$2, %eax
	jl	.LBB16_92
	.p2align	4, 0x90
.LBB16_90:                              # %.lr.ph27.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rcx)
	jne	.LBB16_92
# BB#91:                                #   in Loop: Header=BB16_90 Depth=1
	incq	%rcx
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB16_90
.LBB16_92:                              # %.critedge.i
	movl	%eax, 4(%r12)
	addl	8(%r12), %eax
	jle	.LBB16_108
# BB#93:                                # %.lr.ph.i254.preheader
	movl	%eax, %edi
	notl	%edi
	cmpl	$-3, %edi
	movl	$-2, %edx
	cmovgl	%edi, %edx
	leal	1(%rdx,%rax), %esi
	incq	%rsi
	cmpq	$32, %rsi
	jb	.LBB16_106
# BB#94:                                # %min.iters.checked
	movabsq	$8589934560, %r8        # imm = 0x1FFFFFFE0
	andq	%rsi, %r8
	je	.LBB16_106
# BB#95:                                # %vector.memcheck
	cmpl	$-3, %edi
	movl	$-2, %ebp
	cmovgl	%edi, %ebp
	leal	1(%rbp,%rax), %edi
	leaq	1(%rcx,%rdi), %rbp
	cmpq	%rbp, %rbx
	jae	.LBB16_97
# BB#96:                                # %vector.memcheck
	leaq	17(%r12,%rdi), %rdi
	cmpq	%rdi, %rcx
	jb	.LBB16_106
.LBB16_97:                              # %vector.body.preheader
	leaq	-32(%r8), %rbp
	movl	%ebp, %edi
	shrl	$5, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB16_98
# BB#99:                                # %vector.body.prol.preheader
	negq	%rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB16_100:                             # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rcx,%rdx), %xmm0
	movups	16(%rcx,%rdx), %xmm1
	movups	%xmm0, 16(%r12,%rdx)
	movups	%xmm1, 32(%r12,%rdx)
	addq	$32, %rdx
	incq	%rdi
	jne	.LBB16_100
	jmp	.LBB16_101
.LBB16_98:
	xorl	%edx, %edx
.LBB16_101:                             # %vector.body.prol.loopexit
	cmpq	$96, %rbp
	jb	.LBB16_104
# BB#102:                               # %vector.body.preheader.new
	movq	%r8, %rdi
	subq	%rdx, %rdi
	leaq	128(%r12,%rdx), %rbp
	leaq	112(%rcx,%rdx), %rdx
	.p2align	4, 0x90
.LBB16_103:                             # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rdx), %xmm0
	movups	-96(%rdx), %xmm1
	movups	%xmm0, -112(%rbp)
	movups	%xmm1, -96(%rbp)
	movups	-80(%rdx), %xmm0
	movups	-64(%rdx), %xmm1
	movups	%xmm0, -80(%rbp)
	movups	%xmm1, -64(%rbp)
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -48(%rbp)
	movups	%xmm1, -32(%rbp)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -16(%rbp)
	movups	%xmm1, (%rbp)
	subq	$-128, %rbp
	subq	$-128, %rdx
	addq	$-128, %rdi
	jne	.LBB16_103
.LBB16_104:                             # %middle.block
	cmpq	%r8, %rsi
	je	.LBB16_108
# BB#105:
	addq	%r8, %rcx
	addq	%r8, %rbx
	subl	%r8d, %eax
.LBB16_106:                             # %.lr.ph.i254.preheader393
	incl	%eax
	.p2align	4, 0x90
.LBB16_107:                             # %.lr.ph.i254
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	incq	%rcx
	movb	%dl, (%rbx)
	incq	%rbx
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB16_107
.LBB16_108:                             # %_rm_leading_zeros.exit
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB16_112
# BB#109:
	decl	12(%rdi)
	jne	.LBB16_111
# BB#110:
	callq	free
	movq	56(%rsp), %r11          # 8-byte Reload
.LBB16_111:
	movq	$0, (%r14)
.LBB16_112:                             # %free_num.exit
	movq	%r12, (%r14)
	movq	%r11, %rdi
	callq	free
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	%r15, %rdi
	callq	free
	xorl	%eax, %eax
.LBB16_113:                             # %is_zero.exit.thread
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_13:                              # %.thread363
	movq	%rbp, (%r14)
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB16_23
.LBB16_17:
	callq	free
	movq	64(%rsp), %rbx          # 8-byte Reload
	movl	4(%rbx), %r10d
	movl	8(%rbx), %r9d
	movq	%rbp, (%r14)
	testl	%r9d, %r9d
	je	.LBB16_114
# BB#18:
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB16_19
.LBB16_114:
	movq	%r10, (%rsp)            # 8-byte Spill
	jmp	.LBB16_16
.Lfunc_end16:
	.size	bc_divide, .Lfunc_end16-bc_divide
	.cfi_endproc

	.globl	bc_modulo
	.p2align	4, 0x90
	.type	bc_modulo,@function
bc_modulo:                              # @bc_modulo
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi100:
	.cfi_def_cfa_offset 64
.Lcfi101:
	.cfi_offset %rbx, -48
.Lcfi102:
	.cfi_offset %r12, -40
.Lcfi103:
	.cfi_offset %r14, -32
.Lcfi104:
	.cfi_offset %r15, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	_zero_(%rip), %rax
	movl	$-1, %r12d
	cmpq	%rbx, %rax
	je	.LBB17_12
# BB#1:
	movl	8(%rbx), %ebp
	movl	4(%rbx), %edx
	addl	%ebp, %edx
	jle	.LBB17_2
# BB#3:                                 # %.lr.ph.preheader.i
	leaq	16(%rbx), %rsi
	.p2align	4, 0x90
.LBB17_4:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB17_5
# BB#6:                                 #   in Loop: Header=BB17_4 Depth=1
	incq	%rsi
	leal	-1(%rdx), %edi
	cmpl	$1, %edx
	movl	%edi, %edx
	jg	.LBB17_4
	jmp	.LBB17_7
.LBB17_2:
	movl	%edx, %edi
	testl	%edi, %edi
	jne	.LBB17_8
	jmp	.LBB17_12
.LBB17_5:
	movl	%edx, %edi
.LBB17_7:                               # %is_zero.exit
	testl	%edi, %edi
	je	.LBB17_12
.LBB17_8:
	movl	8(%r15), %edx
	addl	%ecx, %ebp
	cmpl	%ebp, %edx
	cmovgel	%edx, %ebp
	incl	12(%rax)
	movq	%rax, 8(%rsp)
	leaq	8(%rsp), %r12
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	bc_divide
	movq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movl	%ebp, %ecx
	callq	bc_multiply
	movq	8(%rsp), %rbx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	bc_sub
	xorl	%r12d, %r12d
	testq	%rbx, %rbx
	je	.LBB17_12
# BB#9:
	decl	12(%rbx)
	jne	.LBB17_11
# BB#10:
	movq	%rbx, %rdi
	callq	free
.LBB17_11:
	movq	$0, 8(%rsp)
.LBB17_12:                              # %free_num.exit
	movl	%r12d, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end17:
	.size	bc_modulo, .Lfunc_end17-bc_modulo
	.cfi_endproc

	.globl	bc_raise
	.p2align	4, 0x90
	.type	bc_raise,@function
bc_raise:                               # @bc_raise
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi112:
	.cfi_def_cfa_offset 80
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movl	%ecx, %r13d
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	cmpl	$0, 8(%r12)
	je	.LBB18_2
# BB#1:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	rt_warn
.LBB18_2:
	movl	4(%r12), %ebx
	testl	%ebx, %ebx
	jle	.LBB18_3
# BB#4:                                 # %.lr.ph.preheader.i
	leaq	16(%r12), %rdx
	xorl	%ecx, %ecx
	movabsq	$922337203685477581, %rsi # imm = 0xCCCCCCCCCCCCCCD
	movl	%ebx, %eax
	.p2align	4, 0x90
.LBB18_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, %edi
	leaq	(%rcx,%rcx,4), %rax
	movsbq	(%rdx), %rcx
	leaq	(%rcx,%rax,2), %rcx
	cmpl	$2, %edi
	jl	.LBB18_7
# BB#6:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB18_5 Depth=1
	incq	%rdx
	leal	-1(%rdi), %eax
	cmpq	%rsi, %rcx
	jl	.LBB18_5
.LBB18_7:                               # %._crit_edge.loopexit.i
	xorl	%eax, %eax
	cmpl	$1, %edi
	cmovgq	%rax, %rcx
	jmp	.LBB18_8
.LBB18_3:
	xorl	%ecx, %ecx
.LBB18_8:                               # %num2long.exit
	xorl	%eax, %eax
	testq	%rcx, %rcx
	cmovnsq	%rcx, %rax
	movq	%rax, %rbp
	negq	%rbp
	cmpl	$0, (%r12)
	cmoveq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB18_9
# BB#17:                                # %.critedge
	movq	%r14, 16(%rsp)          # 8-byte Spill
	js	.LBB18_18
# BB#19:
	movslq	8(%r15), %rax
	movq	%rax, %rcx
	imulq	%rbp, %rcx
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	movslq	%r13d, %r13
	cmpq	%r13, %rcx
	cmovlel	%ecx, %r13d
	xorl	%r14d, %r14d
	jmp	.LBB18_20
.LBB18_9:
	cmpl	$1, %ebx
	jg	.LBB18_11
# BB#10:
	cmpb	$0, 16(%r12)
	je	.LBB18_12
.LBB18_11:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	rt_error
.LBB18_12:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB18_16
# BB#13:
	decl	12(%rdi)
	jne	.LBB18_15
# BB#14:
	callq	free
.LBB18_15:
	movq	$0, (%r14)
.LBB18_16:                              # %free_num.exit39
	movq	_one_(%rip), %rax
	incl	12(%rax)
	movq	%rax, (%r14)
	jmp	.LBB18_40
.LBB18_18:
	negq	%rbp
	movb	$1, %r14b
.LBB18_20:
	movq	_one_(%rip), %rax
	incl	12(%rax)
	movq	%rax, 8(%rsp)
	incl	12(%r15)
	movq	%r15, (%rsp)
	testq	%rbp, %rbp
	je	.LBB18_26
# BB#21:                                # %.lr.ph.preheader
	movq	%rsp, %r15
	leaq	8(%rsp), %r12
	.p2align	4, 0x90
.LBB18_22:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testb	$1, %bpl
	jne	.LBB18_24
# BB#23:                                # %.lr.ph._crit_edge
                                        #   in Loop: Header=BB18_22 Depth=1
	movq	(%rsp), %rbx
	jmp	.LBB18_25
	.p2align	4, 0x90
.LBB18_24:                              #   in Loop: Header=BB18_22 Depth=1
	movq	8(%rsp), %rdi
	movq	(%rsp), %rbx
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movl	%r13d, %ecx
	callq	bc_multiply
.LBB18_25:                              #   in Loop: Header=BB18_22 Depth=1
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movl	%r13d, %ecx
	callq	bc_multiply
	sarq	%rbp
	jne	.LBB18_22
.LBB18_26:                              # %._crit_edge
	testb	%r14b, %r14b
	je	.LBB18_31
# BB#27:
	movq	_one_(%rip), %rdi
	movq	8(%rsp), %r15
	movq	%r15, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	%r13d, %ecx
	callq	bc_divide
	testq	%r15, %r15
	je	.LBB18_36
# BB#28:
	decl	12(%r15)
	jne	.LBB18_30
# BB#29:
	movq	%r15, %rdi
	callq	free
.LBB18_30:
	movq	$0, 8(%rsp)
	jmp	.LBB18_36
.LBB18_31:
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB18_35
# BB#32:
	decl	12(%rdi)
	jne	.LBB18_34
# BB#33:
	callq	free
.LBB18_34:
	movq	$0, (%rbx)
.LBB18_35:                              # %free_num.exit40
	movq	8(%rsp), %rax
	movq	%rax, (%rbx)
.LBB18_36:                              # %free_num.exit41
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB18_40
# BB#37:
	decl	12(%rdi)
	jne	.LBB18_39
# BB#38:
	callq	free
.LBB18_39:
	movq	$0, (%rsp)
.LBB18_40:                              # %free_num.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end18:
	.size	bc_raise, .Lfunc_end18-bc_raise
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI19_1:
	.long	0                       # 0x0
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.text
	.globl	bc_sqrt
	.p2align	4, 0x90
	.type	bc_sqrt,@function
bc_sqrt:                                # @bc_sqrt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi125:
	.cfi_def_cfa_offset 128
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %r12
	movq	(%r12), %r13
	movq	_zero_(%rip), %rbx
	xorl	%ebp, %ebp
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	_do_compare
	testl	%eax, %eax
	js	.LBB19_86
# BB#1:
	je	.LBB19_2
# BB#7:
	movq	_one_(%rip), %r15
	movl	$1, %edx
	xorl	%ecx, %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	_do_compare
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB19_8
# BB#13:
	movl	8(%r13), %eax
	cmpl	%r14d, %eax
	cmovll	%r14d, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	12(%rbx), %r13d
	movq	%rbx, 8(%rsp)
	leal	2(%r13), %eax
	movl	%eax, 12(%rbx)
	movq	%rbx, (%rsp)
	movl	$1042, %edi             # imm = 0x412
	callq	malloc
	movdqa	.LCPI19_0(%rip), %xmm0  # xmm0 = [0,1,1,1]
	movdqu	%xmm0, (%rax)
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movw	$1280, 16(%rax)         # imm = 0x500
	testl	%ebp, %ebp
	js	.LBB19_14
# BB#15:
	incl	%r13d
	movb	$0, 32(%rsp)
	movb	$1, 33(%rsp)
	movl	%r13d, 12(%rbx)
	jne	.LBB19_17
# BB#16:
	movq	%rbx, %rdi
	callq	free
.LBB19_17:                              # %.lr.ph.i
	movl	$1042, %edi             # imm = 0x412
	callq	malloc
	movdqa	.LCPI19_1(%rip), %xmm0  # xmm0 = [0,2,0,1]
	movdqu	%xmm0, (%rax)
	movb	$0, 16(%rax)
	movq	%rax, 8(%rsp)
	movb	33(%rsp), %cl
	movb	%cl, 16(%rax)
	movb	32(%rsp), %cl
	movb	%cl, 17(%rax)
	movq	(%r12), %rax
	movl	4(%rax), %r15d
	movl	%r15d, %eax
	negl	%eax
	cmovll	%r15d, %eax
	movslq	%eax, %rcx
	imulq	$1717986919, %rcx, %rdx # imm = 0x66666667
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	movb	%sil, 32(%rsp)
	addl	$9, %ecx
	cmpl	$19, %ecx
	jb	.LBB19_18
# BB#19:                                # %.lr.ph39.i43.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB19_20:                              # %.lr.ph39.i43
                                        # =>This Inner Loop Header: Depth=1
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	leal	(%rcx,%rdx), %eax
	movslq	%eax, %rsi
	imulq	$1717986919, %rsi, %rdi # imm = 0x66666667
	movq	%rdi, %rbp
	shrq	$63, %rbp
	sarq	$34, %rdi
	addl	%ebp, %edi
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %edi
	subl	%edi, %esi
	movb	%sil, 32(%rsp,%rbx)
	leal	9(%rcx,%rdx), %ecx
	incq	%rbx
	cmpl	$18, %ecx
	ja	.LBB19_20
# BB#21:                                # %._crit_edge40.i44.loopexit
	leaq	32(%rsp,%rbx), %rbp
	jmp	.LBB19_22
.LBB19_2:
	testq	%r13, %r13
	je	.LBB19_6
# BB#3:
	decl	12(%r13)
	jne	.LBB19_5
# BB#4:
	movq	%r13, %rdi
	callq	free
.LBB19_5:
	movq	$0, (%r12)
	movq	_zero_(%rip), %rbx
.LBB19_6:                               # %free_num.exit
	incl	12(%rbx)
	movq	%rbx, (%r12)
	movl	$1, %ebp
	jmp	.LBB19_86
.LBB19_8:
	testq	%r13, %r13
	je	.LBB19_12
# BB#9:
	decl	12(%r13)
	jne	.LBB19_11
# BB#10:
	movq	%r13, %rdi
	callq	free
.LBB19_11:
	movq	$0, (%r12)
	movq	_one_(%rip), %r15
.LBB19_12:                              # %free_num.exit34
	incl	12(%r15)
	movq	%r15, (%r12)
	movl	$1, %ebp
	jmp	.LBB19_86
.LBB19_14:
	incl	12(%r15)
	movq	%r15, 8(%rsp)
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB19_46
.LBB19_18:
	leaq	33(%rsp), %rbp
	movl	$1, %ebx
.LBB19_22:                              # %._crit_edge40.i44
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB19_26
# BB#23:
	decl	12(%rdi)
	jne	.LBB19_25
# BB#24:
	callq	free
.LBB19_25:
	movq	$0, (%rsp)
.LBB19_26:                              # %free_num.exit.i45
	movslq	%ebx, %r14
	leaq	1040(%r14), %rdi
	callq	malloc
	movl	$0, (%rax)
	movl	%r14d, 4(%rax)
	movl	$0, 8(%rax)
	movl	$1, 12(%rax)
	movb	$0, 16(%rax)
	movq	%rax, (%rsp)
	testl	%r15d, %r15d
	jns	.LBB19_28
# BB#27:
	movl	$1, (%rax)
.LBB19_28:
	testl	%ebx, %ebx
	jle	.LBB19_29
# BB#30:                                # %.lr.ph.i50.preheader
	leaq	16(%rax), %rcx
	movl	%ebx, %edx
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %esi
	cmovgl	%edx, %esi
	leal	1(%rbx,%rsi), %r9d
	leaq	1(%r9), %rsi
	cmpq	$32, %rsi
	jb	.LBB19_31
# BB#32:                                # %min.iters.checked
	movl	%esi, %r8d
	andl	$31, %r8d
	subq	%r8, %rsi
	je	.LBB19_31
# BB#33:                                # %vector.memcheck
	cmpl	$-3, %edx
	movl	$-2, %edi
	cmovgl	%edx, %edi
	cmpq	%rbp, %rcx
	jae	.LBB19_35
# BB#34:                                # %vector.memcheck
	leal	1(%rbx,%rdi), %edx
	leaq	17(%rax,%rdx), %rdi
	notq	%rdx
	addq	%rbp, %rdx
	cmpq	%rdi, %rdx
	jae	.LBB19_35
.LBB19_31:
	movq	%rbp, %rdx
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB19_38:                              # %.lr.ph.i50.preheader129
	incl	%ebx
	decq	%rdx
	.p2align	4, 0x90
.LBB19_39:                              # %.lr.ph.i50
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rdx), %eax
	movb	%al, (%rcx)
	incq	%rcx
	decl	%ebx
	decq	%rdx
	cmpl	$1, %ebx
	jg	.LBB19_39
.LBB19_40:                              # %int2num.exit51.loopexit
	movq	(%rsp), %rax
	jmp	.LBB19_41
.LBB19_29:
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB19_41:                              # %int2num.exit51
	movq	%rsp, %rdx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %ecx
	callq	bc_multiply
	movq	(%rsp), %rbx
	movl	$0, 8(%rbx)
	movq	8(%rsp), %rdi
	leaq	8(%rsp), %rdx
	movq	%rbx, %rsi
	movl	%r14d, %ecx
	callq	bc_raise
	testq	%rbx, %rbx
	je	.LBB19_45
# BB#42:
	decl	12(%rbx)
	jne	.LBB19_44
# BB#43:
	movq	%rbx, %rdi
	callq	free
.LBB19_44:
	movq	$0, (%rsp)
.LBB19_45:                              # %free_num.exit52.preheader.preheader
	xorl	%ebx, %ebx
.LBB19_46:                              # %free_num.exit52.preheader.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	2(%rax), %r14d
	leaq	8(%rsp), %r13
	.p2align	4, 0x90
.LBB19_47:                              # %free_num.exit52.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_55 Depth 2
                                        #     Child Loop BB19_69 Depth 2
                                        #     Child Loop BB19_63 Depth 2
	testq	%rbx, %rbx
	je	.LBB19_51
# BB#48:                                #   in Loop: Header=BB19_47 Depth=1
	decl	12(%rbx)
	jne	.LBB19_50
# BB#49:                                #   in Loop: Header=BB19_47 Depth=1
	movq	%rbx, %rdi
	callq	free
.LBB19_50:                              #   in Loop: Header=BB19_47 Depth=1
	movq	$0, (%rsp)
.LBB19_51:                              # %free_num.exit53
                                        #   in Loop: Header=BB19_47 Depth=1
	movq	8(%rsp), %rbx
	incl	12(%rbx)
	movq	%rbx, (%rsp)
	movq	(%r12), %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movl	%r14d, %ecx
	callq	bc_divide
	movq	8(%rsp), %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	bc_add
	movq	8(%rsp), %rdi
	movq	%rbp, %rsi
	movq	%r13, %rdx
	movl	%r14d, %ecx
	callq	bc_multiply
	movq	8(%rsp), %r15
	movl	4(%r15), %r8d
	cmpl	4(%rbx), %r8d
	jne	.LBB19_47
# BB#52:                                #   in Loop: Header=BB19_47 Depth=1
	movl	8(%r15), %eax
	movl	8(%rbx), %ecx
	cmpl	%ecx, %eax
	movq	%r15, %rsi
	cmovgq	%rbx, %rsi
	addl	8(%rsi), %r8d
	jle	.LBB19_53
# BB#54:                                # %.lr.ph84.i.preheader
                                        #   in Loop: Header=BB19_47 Depth=1
	movl	%r8d, %edi
	movl	$1, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB19_55:                              # %.lr.ph84.i
                                        #   Parent Loop BB19_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	16(%r15,%rsi), %edx
	cmpb	16(%rbx,%rsi), %dl
	jne	.LBB19_65
# BB#56:                                #   in Loop: Header=BB19_55 Depth=2
	incq	%rsi
	decq	%rbp
	leal	(%rbp,%rdi), %edx
	cmpl	$1, %edx
	jg	.LBB19_55
# BB#57:                                # %._crit_edge.i.loopexit
                                        #   in Loop: Header=BB19_47 Depth=1
	subl	%esi, %r8d
	leaq	16(%rbx,%rsi), %rdi
	leaq	16(%r15,%rsi), %rsi
	movq	16(%rsp), %rbp          # 8-byte Reload
	testl	%r8d, %r8d
	jne	.LBB19_47
	jmp	.LBB19_59
.LBB19_53:                              #   in Loop: Header=BB19_47 Depth=1
	leaq	16(%r15), %rsi
	leaq	16(%rbx), %rdi
	testl	%r8d, %r8d
	jne	.LBB19_47
.LBB19_59:                              #   in Loop: Header=BB19_47 Depth=1
	cmpl	%ecx, %eax
	je	.LBB19_71
# BB#60:                                #   in Loop: Header=BB19_47 Depth=1
	jle	.LBB19_67
# BB#61:                                #   in Loop: Header=BB19_47 Depth=1
	cmpl	%ecx, %eax
	jle	.LBB19_71
# BB#62:                                # %.lr.ph.i54.preheader
                                        #   in Loop: Header=BB19_47 Depth=1
	incl	%eax
	subl	%ecx, %eax
	.p2align	4, 0x90
.LBB19_63:                              # %.lr.ph.i54
                                        #   Parent Loop BB19_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rsi)
	jne	.LBB19_47
# BB#64:                                #   in Loop: Header=BB19_63 Depth=2
	incq	%rsi
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB19_63
	jmp	.LBB19_71
.LBB19_65:                              # %.critedge.i
                                        #   in Loop: Header=BB19_47 Depth=1
	cmpl	%ecx, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB19_47
# BB#66:                                # %.critedge.i
                                        #   in Loop: Header=BB19_47 Depth=1
	decl	%r8d
	cmpl	%esi, %r8d
	jne	.LBB19_47
	jmp	.LBB19_71
.LBB19_67:                              #   in Loop: Header=BB19_47 Depth=1
	cmpl	%eax, %ecx
	jle	.LBB19_71
# BB#68:                                # %.lr.ph80.i.preheader
                                        #   in Loop: Header=BB19_47 Depth=1
	incl	%ecx
	subl	%eax, %ecx
	.p2align	4, 0x90
.LBB19_69:                              # %.lr.ph80.i
                                        #   Parent Loop BB19_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rdi)
	jne	.LBB19_47
# BB#70:                                #   in Loop: Header=BB19_69 Depth=2
	incq	%rdi
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB19_69
.LBB19_71:                              # %.loopexit
	movq	(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB19_75
# BB#72:
	decl	12(%rdi)
	jne	.LBB19_74
# BB#73:
	callq	free
.LBB19_74:
	movq	$0, (%r12)
.LBB19_75:                              # %free_num.exit37
	movq	_one_(%rip), %rsi
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	bc_divide
	testq	%r15, %r15
	je	.LBB19_79
# BB#76:
	decl	12(%r15)
	jne	.LBB19_78
# BB#77:
	movq	%r15, %rdi
	callq	free
.LBB19_78:
	movq	$0, 8(%rsp)
.LBB19_79:                              # %free_num.exit36
	testq	%rbx, %rbx
	je	.LBB19_83
# BB#80:
	decl	12(%rbx)
	jne	.LBB19_82
# BB#81:
	movq	%rbx, %rdi
	callq	free
.LBB19_82:
	movq	$0, (%rsp)
.LBB19_83:                              # %free_num.exit35
	movl	$1, %ebp
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB19_86
# BB#84:
	decl	12(%rdi)
	jne	.LBB19_86
# BB#85:
	callq	free
.LBB19_86:                              # %free_num.exit33
	movl	%ebp, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_35:                              # %vector.body.preheader
	subl	%esi, %ebx
	addq	%rsi, %rcx
	leaq	-1(%r8), %rdx
	subq	%r9, %rdx
	addq	%rbp, %rdx
	addq	$-16, %rbp
	addq	$32, %rax
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB19_36:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbp), %xmm1
	movdqu	(%rbp), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rax)
	movdqu	%xmm1, (%rax)
	addq	$-32, %rbp
	addq	$32, %rax
	addq	$-32, %rsi
	jne	.LBB19_36
# BB#37:                                # %middle.block
	testq	%r8, %r8
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB19_38
	jmp	.LBB19_40
.Lfunc_end19:
	.size	bc_sqrt, .Lfunc_end19-bc_sqrt
	.cfi_endproc

	.globl	out_long
	.p2align	4, 0x90
	.type	out_long,@function
out_long:                               # @out_long
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi136:
	.cfi_def_cfa_offset 80
.Lcfi137:
	.cfi_offset %rbx, -40
.Lcfi138:
	.cfi_offset %r14, -32
.Lcfi139:
	.cfi_offset %r15, -24
.Lcfi140:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movl	%esi, %ebp
	movq	%rdi, %r15
	testl	%edx, %edx
	je	.LBB20_2
# BB#1:
	movl	$32, %edi
	callq	*%r14
.LBB20_2:
	movq	%rsp, %rbx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r15
	cmpl	%ebp, %r15d
	jge	.LBB20_3
	.p2align	4, 0x90
.LBB20_7:                               # %.lr.ph18
                                        # =>This Inner Loop Header: Depth=1
	movl	$48, %edi
	callq	*%r14
	decl	%ebp
	cmpl	%r15d, %ebp
	jg	.LBB20_7
.LBB20_3:                               # %.preheader
	testl	%r15d, %r15d
	jle	.LBB20_6
# BB#4:                                 # %.lr.ph.preheader
	movl	%r15d, %ebp
	.p2align	4, 0x90
.LBB20_5:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx), %edi
	callq	*%r14
	incq	%rbx
	decq	%rbp
	jne	.LBB20_5
.LBB20_6:                               # %._crit_edge
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	out_long, .Lfunc_end20-out_long
	.cfi_endproc

	.globl	out_num
	.p2align	4, 0x90
	.type	out_num,@function
out_num:                                # @out_num
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi141:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi142:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi143:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi144:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi145:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi146:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi147:
	.cfi_def_cfa_offset 176
.Lcfi148:
	.cfi_offset %rbx, -56
.Lcfi149:
	.cfi_offset %r12, -48
.Lcfi150:
	.cfi_offset %r13, -40
.Lcfi151:
	.cfi_offset %r14, -32
.Lcfi152:
	.cfi_offset %r15, -24
.Lcfi153:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movl	%esi, %r13d
	movq	%rdi, %r12
	cmpl	$1, (%r12)
	jne	.LBB21_2
# BB#1:
	movl	$45, %edi
	callq	*%r15
.LBB21_2:
	movq	_zero_(%rip), %rcx
	cmpq	%r12, %rcx
	je	.LBB21_17
# BB#3:
	movl	4(%r12), %ebp
	movl	8(%r12), %eax
	movl	%eax, %edx
	addl	%ebp, %edx
	jle	.LBB21_7
# BB#4:                                 # %.lr.ph.preheader.i
	leaq	16(%r12), %rsi
	.p2align	4, 0x90
.LBB21_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpb	$0, (%rsi)
	jne	.LBB21_8
# BB#6:                                 #   in Loop: Header=BB21_5 Depth=1
	incq	%rsi
	leal	-1(%rdx), %edi
	cmpl	$1, %edx
	movl	%edi, %edx
	jg	.LBB21_5
	jmp	.LBB21_9
.LBB21_7:
	movl	%edx, %edi
	testl	%edi, %edi
	jne	.LBB21_10
	jmp	.LBB21_17
.LBB21_8:
	movl	%edx, %edi
.LBB21_9:                               # %is_zero.exit
	testl	%edi, %edi
	je	.LBB21_17
.LBB21_10:
	cmpl	$10, %r13d
	jne	.LBB21_19
# BB#11:
	leaq	16(%r12), %rbx
	cmpl	$1, %ebp
	jg	.LBB21_14
# BB#12:
	cmpb	$0, (%rbx)
	je	.LBB21_35
# BB#13:
	testl	%ebp, %ebp
	movl	$1, %ebp
	jle	.LBB21_36
.LBB21_14:                              # %.lr.ph171.preheader
	incl	%ebp
	.p2align	4, 0x90
.LBB21_15:                              # %.lr.ph171
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx), %edi
	incq	%rbx
	addl	$48, %edi
	callq	*%r15
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB21_15
# BB#16:                                # %.loopexit.loopexit
	movl	8(%r12), %eax
	testl	%eax, %eax
	jg	.LBB21_37
	jmp	.LBB21_18
.LBB21_17:                              # %is_zero.exit.thread
	movl	$48, %edi
	callq	*%r15
.LBB21_18:                              # %free_num.exit
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_19:
	incl	12(%rcx)
	movq	%rcx, (%rsp)
	movq	_one_(%rip), %rsi
	movq	%rsp, %rdx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	callq	bc_divide
	movq	_zero_(%rip), %r14
	movl	12(%r14), %eax
	movq	%r14, 16(%rsp)
	movq	%r14, 48(%rsp)
	addl	$3, %eax
	movl	%eax, 12(%r14)
	movq	(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%r12, %rdi
	callq	bc_sub
	movl	%r13d, %eax
	negl	%eax
	cmovll	%r13d, %eax
	movslq	%eax, %rcx
	imulq	$1717986919, %rcx, %rdx # imm = 0x66666667
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	leaq	65(%rsp), %rbx
	movb	%sil, 64(%rsp)
	addl	$9, %ecx
	movl	$1, %ebp
	cmpl	$19, %ecx
	jb	.LBB21_23
# BB#20:                                # %.lr.ph39.i.preheader
	movl	$1, %ebp
	.p2align	4, 0x90
.LBB21_21:                              # %.lr.ph39.i
                                        # =>This Inner Loop Header: Depth=1
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	leal	(%rcx,%rdx), %eax
	movslq	%eax, %rsi
	imulq	$1717986919, %rsi, %rdi # imm = 0x66666667
	movq	%rdi, %rbx
	shrq	$63, %rbx
	sarq	$34, %rdi
	addl	%ebx, %edi
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %edi
	subl	%edi, %esi
	movb	%sil, 64(%rsp,%rbp)
	leal	9(%rcx,%rdx), %ecx
	incq	%rbp
	cmpl	$18, %ecx
	ja	.LBB21_21
# BB#22:                                # %._crit_edge40.i.loopexit
	leaq	64(%rsp,%rbp), %rbx
.LBB21_23:                              # %._crit_edge40.i
	testq	%r14, %r14
	je	.LBB21_26
# BB#24:
	decl	12(%r14)
	jne	.LBB21_26
# BB#25:
	movq	%r14, %rdi
	callq	free
.LBB21_26:                              # %free_num.exit.i
	movslq	%ebp, %r14
	leaq	1040(%r14), %rdi
	callq	malloc
	movl	$0, (%rax)
	movl	%r14d, 4(%rax)
	movl	$0, 8(%rax)
	movl	$1, 12(%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movb	$0, 16(%rax)
	testl	%r13d, %r13d
	jns	.LBB21_28
# BB#27:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$1, (%rax)
.LBB21_28:
	testl	%ebp, %ebp
	jle	.LBB21_46
# BB#29:                                # %.lr.ph.i74.preheader
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	16(%rax), %rax
	movl	%ebp, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%rbp,%rdx), %edi
	leaq	1(%rdi), %rdx
	cmpq	$32, %rdx
	jae	.LBB21_31
# BB#30:
	movq	%rbx, %rcx
	jmp	.LBB21_44
.LBB21_31:                              # %min.iters.checked
	movl	%edx, %r8d
	andl	$31, %r8d
	subq	%r8, %rdx
	je	.LBB21_40
# BB#32:                                # %vector.memcheck
	cmpl	$-3, %ecx
	movl	$-2, %esi
	cmovgl	%ecx, %esi
	cmpq	%rbx, %rax
	jae	.LBB21_41
# BB#33:                                # %vector.memcheck
	leal	1(%rbp,%rsi), %ecx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	17(%rsi,%rcx), %rsi
	notq	%rcx
	addq	%rbx, %rcx
	cmpq	%rsi, %rcx
	jae	.LBB21_41
# BB#34:
	movq	%rbx, %rcx
	jmp	.LBB21_44
.LBB21_35:
	leaq	17(%r12), %rbx
.LBB21_36:                              # %.loopexit
	testl	%eax, %eax
	jle	.LBB21_18
.LBB21_37:
	movl	$46, %edi
	callq	*%r15
	cmpl	$0, 8(%r12)
	jle	.LBB21_18
# BB#38:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB21_39:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	(%rbx,%rbp), %edi
	addl	$48, %edi
	callq	*%r15
	incq	%rbp
	cmpl	8(%r12), %ebp
	jl	.LBB21_39
	jmp	.LBB21_18
.LBB21_40:
	movq	%rbx, %rcx
	jmp	.LBB21_44
.LBB21_41:                              # %vector.body.preheader
	subl	%edx, %ebp
	addq	%rdx, %rax
	leaq	-1(%r8), %rcx
	subq	%rdi, %rcx
	addq	%rbx, %rcx
	addq	$-16, %rbx
	movq	8(%rsp), %rsi           # 8-byte Reload
	leaq	32(%rsi), %rdi
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB21_42:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbx), %xmm1
	movdqu	(%rbx), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$-32, %rbx
	addq	$32, %rdi
	addq	$-32, %rdx
	jne	.LBB21_42
# BB#43:                                # %middle.block
	testq	%r8, %r8
	je	.LBB21_46
.LBB21_44:                              # %.lr.ph.i74.preheader328
	incl	%ebp
	decq	%rcx
	.p2align	4, 0x90
.LBB21_45:                              # %.lr.ph.i74
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	movb	%dl, (%rax)
	incq	%rax
	decl	%ebp
	decq	%rcx
	cmpl	$1, %ebp
	jg	.LBB21_45
.LBB21_46:                              # %int2num.exit
	movq	_zero_(%rip), %rdi
	incl	12(%rdi)
	leal	-1(%r13), %ecx
	movl	$1, %r14d
	movl	$1, %eax
	subl	%r13d, %eax
	testl	%r13d, %r13d
	cmovgl	%ecx, %eax
	movslq	%eax, %rcx
	imulq	$1717986919, %rcx, %rdx # imm = 0x66666667
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	movb	%sil, 64(%rsp)
	addl	$9, %ecx
	cmpl	$19, %ecx
	leaq	65(%rsp), %rbp
	jb	.LBB21_50
# BB#47:                                # %.lr.ph39.i82.preheader
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB21_48:                              # %.lr.ph39.i82
                                        # =>This Inner Loop Header: Depth=1
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	leal	(%rcx,%rdx), %eax
	movslq	%eax, %rsi
	imulq	$1717986919, %rsi, %rbp # imm = 0x66666667
	movq	%rbp, %rbx
	shrq	$63, %rbx
	sarq	$34, %rbp
	addl	%ebx, %ebp
	addl	%ebp, %ebp
	leal	(%rbp,%rbp,4), %ebp
	subl	%ebp, %esi
	movb	%sil, 64(%rsp,%r14)
	leal	9(%rcx,%rdx), %ecx
	incq	%r14
	cmpl	$18, %ecx
	ja	.LBB21_48
# BB#49:                                # %._crit_edge40.i85.loopexit
	leaq	64(%rsp,%r14), %rbp
.LBB21_50:                              # %._crit_edge40.i85
	testq	%rdi, %rdi
	je	.LBB21_53
# BB#51:
	decl	12(%rdi)
	jne	.LBB21_53
# BB#52:
	callq	free
.LBB21_53:                              # %free_num.exit.i86
	movslq	%r14d, %rbx
	leaq	1040(%rbx), %rdi
	callq	malloc
	movl	$0, (%rax)
	movl	%ebx, 4(%rax)
	movl	$0, 8(%rax)
	movl	$1, 12(%rax)
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movb	$0, 16(%rax)
	testl	%r13d, %r13d
	jg	.LBB21_55
# BB#54:
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	$1, (%rax)
.LBB21_55:
	movq	%r12, 24(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB21_68
# BB#56:                                # %.lr.ph.i91.preheader
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax), %rax
	movl	%r14d, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%r14,%rdx), %edi
	leaq	1(%rdi), %rdx
	cmpq	$32, %rdx
	jae	.LBB21_58
# BB#57:
	movq	%rbp, %rcx
	jmp	.LBB21_66
.LBB21_58:                              # %min.iters.checked244
	movl	%edx, %r8d
	andl	$31, %r8d
	subq	%r8, %rdx
	je	.LBB21_62
# BB#59:                                # %vector.memcheck256
	cmpl	$-3, %ecx
	movl	$-2, %esi
	cmovgl	%ecx, %esi
	cmpq	%rbp, %rax
	jae	.LBB21_63
# BB#60:                                # %vector.memcheck256
	leal	1(%r14,%rsi), %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	17(%rsi,%rcx), %rsi
	notq	%rcx
	addq	%rbp, %rcx
	cmpq	%rsi, %rcx
	jae	.LBB21_63
.LBB21_62:
	movq	%rbp, %rcx
	jmp	.LBB21_66
.LBB21_63:                              # %vector.body239.preheader
	subl	%edx, %r14d
	addq	%rdx, %rax
	leaq	-1(%r8), %rcx
	subq	%rdi, %rcx
	addq	%rbp, %rcx
	addq	$-16, %rbp
	movq	32(%rsp), %rdi          # 8-byte Reload
	addq	$32, %rdi
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB21_64:                              # %vector.body239
                                        # =>This Inner Loop Header: Depth=1
	movdqu	-16(%rbp), %xmm1
	movdqu	(%rbp), %xmm2
	movdqa	%xmm2, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm2    # xmm2 = xmm2[8],xmm0[8],xmm2[9],xmm0[9],xmm2[10],xmm0[10],xmm2[11],xmm0[11],xmm2[12],xmm0[12],xmm2[13],xmm0[13],xmm2[14],xmm0[14],xmm2[15],xmm0[15]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm2
	movdqa	%xmm1, %xmm3
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	pshuflw	$27, %xmm3, %xmm3       # xmm3 = xmm3[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm3, %xmm3       # xmm3 = xmm3[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm0, %xmm1    # xmm1 = xmm1[8],xmm0[8],xmm1[9],xmm0[9],xmm1[10],xmm0[10],xmm1[11],xmm0[11],xmm1[12],xmm0[12],xmm1[13],xmm0[13],xmm1[14],xmm0[14],xmm1[15],xmm0[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm3, %xmm1
	movdqu	%xmm2, -16(%rdi)
	movdqu	%xmm1, (%rdi)
	addq	$-32, %rbp
	addq	$32, %rdi
	addq	$-32, %rdx
	jne	.LBB21_64
# BB#65:                                # %middle.block240
	testq	%r8, %r8
	je	.LBB21_68
.LBB21_66:                              # %.lr.ph.i91.preheader327
	incl	%r14d
	decq	%rcx
	.p2align	4, 0x90
.LBB21_67:                              # %.lr.ph.i91
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	movb	%dl, (%rax)
	incq	%rax
	decl	%r14d
	decq	%rcx
	cmpl	$1, %r14d
	jg	.LBB21_67
.LBB21_68:                              # %int2num.exit92
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	(%rsp), %r12
	cmpq	%r12, _zero_(%rip)
	je	.LBB21_93
# BB#69:                                # %.lr.ph175.preheader
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movq	8(%rsp), %r14           # 8-byte Reload
	.p2align	4, 0x90
.LBB21_70:                              # %.lr.ph175
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_72 Depth 2
                                        #     Child Loop BB21_79 Depth 2
	movl	8(%r12), %eax
	addl	4(%r12), %eax
	jle	.LBB21_74
# BB#71:                                # %.lr.ph.preheader.i93
                                        #   in Loop: Header=BB21_70 Depth=1
	leaq	16(%r12), %rcx
	.p2align	4, 0x90
.LBB21_72:                              # %.lr.ph.i96
                                        #   Parent Loop BB21_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	$0, (%rcx)
	jne	.LBB21_75
# BB#73:                                #   in Loop: Header=BB21_72 Depth=2
	incq	%rcx
	leal	-1(%rax), %edx
	cmpl	$1, %eax
	movl	%edx, %eax
	jg	.LBB21_72
	jmp	.LBB21_76
	.p2align	4, 0x90
.LBB21_74:                              #   in Loop: Header=BB21_70 Depth=1
	movl	%eax, %edx
	testl	%edx, %edx
	jne	.LBB21_77
	jmp	.LBB21_84
	.p2align	4, 0x90
.LBB21_75:                              #   in Loop: Header=BB21_70 Depth=1
	movl	%eax, %edx
.LBB21_76:                              # %is_zero.exit101
                                        #   in Loop: Header=BB21_70 Depth=1
	testl	%edx, %edx
	je	.LBB21_84
.LBB21_77:                              #   in Loop: Header=BB21_70 Depth=1
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	leaq	48(%rsp), %rdx
	callq	bc_modulo
	movl	$16, %edi
	callq	malloc
	movq	%rax, %r13
	movq	48(%rsp), %rax
	movl	4(%rax), %edi
	testl	%edi, %edi
	jle	.LBB21_82
# BB#78:                                # %.lr.ph.preheader.i102
                                        #   in Loop: Header=BB21_70 Depth=1
	leaq	16(%rax), %rdx
	xorl	%ecx, %ecx
	movabsq	$922337203685477581, %rbp # imm = 0xCCCCCCCCCCCCCCD
	.p2align	4, 0x90
.LBB21_79:                              # %.lr.ph.i103
                                        #   Parent Loop BB21_70 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %esi
	leaq	(%rcx,%rcx,4), %rcx
	movsbq	(%rdx), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	cmpl	$2, %esi
	jl	.LBB21_81
# BB#80:                                # %.lr.ph.i103
                                        #   in Loop: Header=BB21_79 Depth=2
	incq	%rdx
	leal	-1(%rsi), %edi
	cmpq	%rbp, %rcx
	jl	.LBB21_79
.LBB21_81:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB21_70 Depth=1
	cmpl	$1, %esi
	movl	$0, %ebp
	cmovgq	%rbp, %rcx
	jmp	.LBB21_83
	.p2align	4, 0x90
.LBB21_82:                              #   in Loop: Header=BB21_70 Depth=1
	xorl	%ecx, %ecx
.LBB21_83:                              # %num2long.exit
                                        #   in Loop: Header=BB21_70 Depth=1
	testq	%rcx, %rcx
	cmovsq	%rbp, %rcx
	movq	%rcx, %rdx
	negq	%rdx
	cmpl	$0, (%rax)
	cmoveq	%rcx, %rdx
	movq	%rdx, (%r13)
	movq	%rbx, 8(%r13)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rsp, %rdx
	callq	bc_divide
	movq	(%rsp), %r12
	cmpq	%r12, _zero_(%rip)
	movq	%r13, %rbx
	jne	.LBB21_70
	jmp	.LBB21_85
.LBB21_84:                              # %is_zero.exit101.thread
	testq	%rbx, %rbx
	movq	%rbx, %r13
	je	.LBB21_94
.LBB21_85:                              # %.preheader
	cmpl	$17, 112(%rsp)          # 4-byte Folded Reload
	jge	.LBB21_87
	.p2align	4, 0x90
.LBB21_86:                              # %.preheader.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %rbx
	movslq	(%r13), %rax
	movsbl	ref_str(%rax), %edi
	callq	*%r15
	movq	%r13, %rdi
	callq	free
	testq	%rbx, %rbx
	movq	%rbx, %r13
	jne	.LBB21_86
	jmp	.LBB21_94
	.p2align	4, 0x90
.LBB21_87:                              # %.preheader.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_88 Depth 2
                                        #     Child Loop BB21_91 Depth 2
	movq	(%r13), %r14
	movq	8(%r13), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %ebp
	movl	$32, %edi
	callq	*%r15
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	leaq	64(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	sprintf
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %r14
	cmpl	%ebp, %r14d
	jge	.LBB21_89
	.p2align	4, 0x90
.LBB21_88:                              # %.lr.ph18.i110
                                        #   Parent Loop BB21_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$48, %edi
	callq	*%r15
	decl	%ebp
	cmpl	%r14d, %ebp
	jg	.LBB21_88
.LBB21_89:                              # %.preheader.i106
                                        #   in Loop: Header=BB21_87 Depth=1
	testl	%r14d, %r14d
	jle	.LBB21_92
# BB#90:                                # %.lr.ph.preheader.i108
                                        #   in Loop: Header=BB21_87 Depth=1
	movl	%r14d, %ebp
	leaq	64(%rsp), %rbx
	.p2align	4, 0x90
.LBB21_91:                              # %.lr.ph.i114
                                        #   Parent Loop BB21_87 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rbx), %edi
	callq	*%r15
	incq	%rbx
	decq	%rbp
	jne	.LBB21_91
.LBB21_92:                              # %out_long.exit115
                                        #   in Loop: Header=BB21_87 Depth=1
	movq	%r13, %rdi
	callq	free
	movq	40(%rsp), %r13          # 8-byte Reload
	testq	%r13, %r13
	jne	.LBB21_87
.LBB21_93:
	movq	8(%rsp), %r14           # 8-byte Reload
.LBB21_94:                              # %.loopexit157
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 8(%rbx)
	jle	.LBB21_137
# BB#95:
	movl	$46, %edi
	callq	*%r15
	movq	_one_(%rip), %rax
	incl	12(%rax)
	movq	%rax, 56(%rsp)
	movl	8(%rbx), %ecx
	cmpl	%ecx, 4(%rax)
	jg	.LBB21_137
# BB#96:                                # %.lr.ph173
	xorl	%ebx, %ebx
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	jmp	.LBB21_100
.LBB21_97:                              # %vector.body281.preheader
                                        #   in Loop: Header=BB21_100 Depth=1
	subl	%edx, %r14d
	addq	%rdx, %rax
	leaq	-1(%rsi), %rcx
	subq	%rdi, %rcx
	addq	%r13, %rcx
	addq	$-16, %r13
	addq	$32, %r12
	.p2align	4, 0x90
.LBB21_98:                              # %vector.body281
                                        #   Parent Loop BB21_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	-16(%r13), %xmm0
	movdqu	(%r13), %xmm1
	movdqa	%xmm1, %xmm2
	punpcklbw	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3],xmm2[4],xmm3[4],xmm2[5],xmm3[5],xmm2[6],xmm3[6],xmm2[7],xmm3[7]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm3, %xmm1    # xmm1 = xmm1[8],xmm3[8],xmm1[9],xmm3[9],xmm1[10],xmm3[10],xmm1[11],xmm3[11],xmm1[12],xmm3[12],xmm1[13],xmm3[13],xmm1[14],xmm3[14],xmm1[15],xmm3[15]
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	pshuflw	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm1, %xmm1       # xmm1 = xmm1[0,1,2,3,7,6,5,4]
	packuswb	%xmm2, %xmm1
	movdqa	%xmm0, %xmm2
	punpcklbw	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1],xmm2[2],xmm3[2],xmm2[3],xmm3[3],xmm2[4],xmm3[4],xmm2[5],xmm3[5],xmm2[6],xmm3[6],xmm2[7],xmm3[7]
	pshufd	$78, %xmm2, %xmm2       # xmm2 = xmm2[2,3,0,1]
	pshuflw	$27, %xmm2, %xmm2       # xmm2 = xmm2[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm2, %xmm2       # xmm2 = xmm2[0,1,2,3,7,6,5,4]
	punpckhbw	%xmm3, %xmm0    # xmm0 = xmm0[8],xmm3[8],xmm0[9],xmm3[9],xmm0[10],xmm3[10],xmm0[11],xmm3[11],xmm0[12],xmm3[12],xmm0[13],xmm3[13],xmm0[14],xmm3[14],xmm0[15],xmm3[15]
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	pshuflw	$27, %xmm0, %xmm0       # xmm0 = xmm0[3,2,1,0,4,5,6,7]
	pshufhw	$27, %xmm0, %xmm0       # xmm0 = xmm0[0,1,2,3,7,6,5,4]
	packuswb	%xmm2, %xmm0
	movdqu	%xmm1, -16(%r12)
	movdqu	%xmm0, (%r12)
	addq	$-32, %r13
	addq	$32, %r12
	addq	$-32, %rdx
	jne	.LBB21_98
# BB#99:                                # %middle.block282
                                        #   in Loop: Header=BB21_100 Depth=1
	testq	%rsi, %rsi
	movq	24(%rsp), %r13          # 8-byte Reload
	jne	.LBB21_122
	jmp	.LBB21_124
	.p2align	4, 0x90
.LBB21_100:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_102 Depth 2
                                        #     Child Loop BB21_108 Depth 2
                                        #     Child Loop BB21_98 Depth 2
                                        #     Child Loop BB21_123 Depth 2
                                        #     Child Loop BB21_131 Depth 2
                                        #     Child Loop BB21_134 Depth 2
	movq	16(%rsp), %rdi
	movq	%r14, %rsi
	leaq	16(%rsp), %rdx
	callq	bc_multiply
	movq	16(%rsp), %rax
	movl	4(%rax), %edi
	testl	%edi, %edi
	jle	.LBB21_105
# BB#101:                               # %.lr.ph.preheader.i116
                                        #   in Loop: Header=BB21_100 Depth=1
	leaq	16(%rax), %rdx
	xorl	%ecx, %ecx
	movabsq	$922337203685477581, %rbp # imm = 0xCCCCCCCCCCCCCCD
	.p2align	4, 0x90
.LBB21_102:                             # %.lr.ph.i120
                                        #   Parent Loop BB21_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edi, %esi
	leaq	(%rcx,%rcx,4), %rcx
	movsbq	(%rdx), %rdi
	leaq	(%rdi,%rcx,2), %rcx
	cmpl	$2, %esi
	jl	.LBB21_104
# BB#103:                               # %.lr.ph.i120
                                        #   in Loop: Header=BB21_102 Depth=2
	incq	%rdx
	leal	-1(%rsi), %edi
	cmpq	%rbp, %rcx
	jl	.LBB21_102
.LBB21_104:                             # %._crit_edge.loopexit.i122
                                        #   in Loop: Header=BB21_100 Depth=1
	cmpl	$1, %esi
	cmovgq	%rbx, %rcx
	jmp	.LBB21_106
	.p2align	4, 0x90
.LBB21_105:                             #   in Loop: Header=BB21_100 Depth=1
	xorl	%ecx, %ecx
.LBB21_106:                             # %num2long.exit126
                                        #   in Loop: Header=BB21_100 Depth=1
	testq	%rcx, %rcx
	cmovsq	%rbx, %rcx
	movq	%rcx, %rbx
	negq	%rbx
	cmpl	$0, (%rax)
	cmoveq	%rcx, %rbx
	movl	%ebx, %eax
	negl	%eax
	cmovll	%ebx, %eax
	movslq	%eax, %rcx
	imulq	$1717986919, %rcx, %rdx # imm = 0x66666667
	movq	%rdx, %rsi
	shrq	$63, %rsi
	sarq	$34, %rdx
	addl	%esi, %edx
	addl	%edx, %edx
	leal	(%rdx,%rdx,4), %edx
	movl	%ecx, %esi
	subl	%edx, %esi
	movb	%sil, 64(%rsp)
	addl	$9, %ecx
	movl	$1, %r14d
	cmpl	$19, %ecx
	leaq	65(%rsp), %r13
	jb	.LBB21_110
# BB#107:                               # %.lr.ph39.i134.preheader
                                        #   in Loop: Header=BB21_100 Depth=1
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB21_108:                             # %.lr.ph39.i134
                                        #   Parent Loop BB21_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cltq
	imulq	$1717986919, %rax, %rcx # imm = 0x66666667
	movq	%rcx, %rdx
	shrq	$63, %rdx
	sarq	$34, %rcx
	leal	(%rcx,%rdx), %eax
	movslq	%eax, %rsi
	imulq	$1717986919, %rsi, %rdi # imm = 0x66666667
	movq	%rdi, %rbp
	shrq	$63, %rbp
	sarq	$34, %rdi
	addl	%ebp, %edi
	addl	%edi, %edi
	leal	(%rdi,%rdi,4), %edi
	subl	%edi, %esi
	movb	%sil, 64(%rsp,%r14)
	leal	9(%rcx,%rdx), %ecx
	incq	%r14
	cmpl	$18, %ecx
	ja	.LBB21_108
# BB#109:                               # %._crit_edge40.i137.loopexit
                                        #   in Loop: Header=BB21_100 Depth=1
	movq	(%rsp), %r12
	leaq	64(%rsp,%r14), %r13
.LBB21_110:                             # %._crit_edge40.i137
                                        #   in Loop: Header=BB21_100 Depth=1
	testq	%r12, %r12
	je	.LBB21_114
# BB#111:                               #   in Loop: Header=BB21_100 Depth=1
	decl	12(%r12)
	jne	.LBB21_113
# BB#112:                               #   in Loop: Header=BB21_100 Depth=1
	movq	%r12, %rdi
	callq	free
.LBB21_113:                             #   in Loop: Header=BB21_100 Depth=1
	movq	$0, (%rsp)
.LBB21_114:                             # %free_num.exit.i138
                                        #   in Loop: Header=BB21_100 Depth=1
	movslq	%r14d, %rbp
	leaq	1040(%rbp), %rdi
	callq	malloc
	movq	%rax, %r12
	movl	$0, (%r12)
	movl	%ebp, 4(%r12)
	movl	$0, 8(%r12)
	movl	$1, 12(%r12)
	movb	$0, 16(%r12)
	movq	%r12, (%rsp)
	testl	%ebx, %ebx
	jns	.LBB21_116
# BB#115:                               #   in Loop: Header=BB21_100 Depth=1
	movl	$1, (%r12)
.LBB21_116:                             #   in Loop: Header=BB21_100 Depth=1
	testl	%r14d, %r14d
	pxor	%xmm3, %xmm3
	jle	.LBB21_125
# BB#117:                               # %.lr.ph.i143.preheader
                                        #   in Loop: Header=BB21_100 Depth=1
	leaq	16(%r12), %rax
	movl	%r14d, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %edx
	cmovgl	%ecx, %edx
	leal	1(%r14,%rdx), %edi
	leaq	1(%rdi), %rdx
	cmpq	$32, %rdx
	jb	.LBB21_121
# BB#118:                               # %min.iters.checked286
                                        #   in Loop: Header=BB21_100 Depth=1
	movl	%edx, %esi
	andl	$31, %esi
	subq	%rsi, %rdx
	je	.LBB21_121
# BB#119:                               # %vector.memcheck300
                                        #   in Loop: Header=BB21_100 Depth=1
	cmpl	$-3, %ecx
	movl	$-2, %ebp
	cmovlel	%ebp, %ecx
	cmpq	%r13, %rax
	jae	.LBB21_97
# BB#120:                               # %vector.memcheck300
                                        #   in Loop: Header=BB21_100 Depth=1
	leal	1(%r14,%rcx), %ecx
	leaq	17(%r12,%rcx), %r8
	movq	%r13, %rbp
	subq	%rcx, %rbp
	decq	%rbp
	cmpq	%r8, %rbp
	jae	.LBB21_97
	.p2align	4, 0x90
.LBB21_121:                             #   in Loop: Header=BB21_100 Depth=1
	movq	%r13, %rcx
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB21_122:                             # %.lr.ph.i143.preheader325
                                        #   in Loop: Header=BB21_100 Depth=1
	incl	%r14d
	decq	%rcx
	.p2align	4, 0x90
.LBB21_123:                             # %.lr.ph.i143
                                        #   Parent Loop BB21_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %edx
	movb	%dl, (%rax)
	incq	%rax
	decl	%r14d
	decq	%rcx
	cmpl	$1, %r14d
	jg	.LBB21_123
.LBB21_124:                             # %int2num.exit144.loopexit
                                        #   in Loop: Header=BB21_100 Depth=1
	movq	(%rsp), %r12
	jmp	.LBB21_126
	.p2align	4, 0x90
.LBB21_125:                             #   in Loop: Header=BB21_100 Depth=1
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB21_126:                             # %int2num.exit144
                                        #   in Loop: Header=BB21_100 Depth=1
	movq	16(%rsp), %rdi
	movq	%r12, %rsi
	leaq	16(%rsp), %rdx
	callq	bc_sub
	movslq	%ebx, %rbx
	cmpl	$16, 112(%rsp)          # 4-byte Folded Reload
	jg	.LBB21_128
# BB#127:                               #   in Loop: Header=BB21_100 Depth=1
	movsbl	ref_str(%rbx), %edi
	callq	*%r15
	jmp	.LBB21_136
	.p2align	4, 0x90
.LBB21_128:                             #   in Loop: Header=BB21_100 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %ebp
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	je	.LBB21_130
# BB#129:                               #   in Loop: Header=BB21_100 Depth=1
	movl	$32, %edi
	callq	*%r15
.LBB21_130:                             #   in Loop: Header=BB21_100 Depth=1
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	leaq	64(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	sprintf
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	cmpl	%ebp, %ebx
	jge	.LBB21_132
	.p2align	4, 0x90
.LBB21_131:                             # %.lr.ph18.i
                                        #   Parent Loop BB21_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$48, %edi
	callq	*%r15
	decl	%ebp
	cmpl	%ebx, %ebp
	jg	.LBB21_131
.LBB21_132:                             # %.preheader.i
                                        #   in Loop: Header=BB21_100 Depth=1
	testl	%ebx, %ebx
	jle	.LBB21_135
# BB#133:                               # %.lr.ph.preheader.i104
                                        #   in Loop: Header=BB21_100 Depth=1
	movl	%ebx, %ebx
	leaq	64(%rsp), %rbp
	.p2align	4, 0x90
.LBB21_134:                             # %.lr.ph.i105
                                        #   Parent Loop BB21_100 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	(%rbp), %edi
	callq	*%r15
	incq	%rbp
	decq	%rbx
	jne	.LBB21_134
.LBB21_135:                             # %out_long.exit
                                        #   in Loop: Header=BB21_100 Depth=1
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
.LBB21_136:                             #   in Loop: Header=BB21_100 Depth=1
	movq	8(%rsp), %r14           # 8-byte Reload
	xorl	%ebx, %ebx
	movq	56(%rsp), %rdi
	xorl	%ecx, %ecx
	movq	%r14, %rsi
	leaq	56(%rsp), %rdx
	callq	bc_multiply
	movq	56(%rsp), %rax
	movl	8(%r13), %ecx
	cmpl	%ecx, 4(%rax)
	jle	.LBB21_100
.LBB21_137:                             # %.loopexit156
	testq	%r12, %r12
	je	.LBB21_141
# BB#138:
	decl	12(%r12)
	jne	.LBB21_140
# BB#139:
	movq	%r12, %rdi
	callq	free
.LBB21_140:
	movq	$0, (%rsp)
.LBB21_141:                             # %free_num.exit71
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_145
# BB#142:
	decl	12(%rdi)
	jne	.LBB21_144
# BB#143:
	callq	free
.LBB21_144:
	movq	$0, 16(%rsp)
.LBB21_145:                             # %free_num.exit70
	testq	%r14, %r14
	je	.LBB21_148
# BB#146:
	decl	12(%r14)
	jne	.LBB21_148
# BB#147:
	movq	%r14, %rdi
	callq	free
.LBB21_148:                             # %free_num.exit69
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB21_18
# BB#149:
	decl	12(%rdi)
	jne	.LBB21_151
# BB#150:
	callq	free
.LBB21_151:
	movq	$0, 48(%rsp)
	jmp	.LBB21_18
.Lfunc_end21:
	.size	out_num, .Lfunc_end21-out_num
	.cfi_endproc

	.type	_zero_,@object          # @_zero_
	.comm	_zero_,8,8
	.type	_one_,@object           # @_one_
	.comm	_one_,8,8
	.type	_two_,@object           # @_two_
	.comm	_two_,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"non-zero scale in exponent"
	.size	.L.str, 27

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"exponent too large in raise"
	.size	.L.str.1, 28

	.type	ref_str,@object         # @ref_str
	.data
	.globl	ref_str
	.p2align	4
ref_str:
	.asciz	"0123456789ABCDEF"
	.size	ref_str, 17

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"%ld"
	.size	.L.str.2, 4


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
