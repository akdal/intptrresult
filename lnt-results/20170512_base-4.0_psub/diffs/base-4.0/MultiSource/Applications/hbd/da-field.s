	.text
	.file	"da-field.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	39                      # 0x27
	.text
	.globl	_Z5dogetP9Classfile
	.p2align	4, 0x90
	.type	_Z5dogetP9Classfile,@function
_Z5dogetP9Classfile:                    # @_Z5dogetP9Classfile
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	currpc(%rip), %r13d
	leal	2(%r13), %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shll	$8, %ecx
	movzbl	1(%rax), %r14d
	orl	%ecx, %r14d
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	40(%rdi), %rbp
	movzwl	%r14w, %eax
	shlq	$4, %rax
	movq	8(%rbp,%rax), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movzwl	2(%rax), %eax
	shlq	$4, %rax
	movq	8(%rbp,%rax), %rbx
	movzwl	(%rbx), %eax
	shlq	$4, %rax
	movq	8(%rbp,%rax), %r15
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, (%rsp)            # 8-byte Spill
	movzwl	2(%rbx), %eax
	shlq	$4, %rax
	movq	8(%rbp,%rax), %rdi
.Ltmp0:
	callq	_Z8sig2typePc
	movl	%eax, %r12d
.Ltmp1:
# BB#1:
.Ltmp2:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp3:
# BB#2:                                 # %.noexc66
	decl	%r13d
	movswl	%r14w, %eax
	movq	%r15, (%rbp)
	movl	%r12d, 8(%rbp)
	movl	$2, 12(%rbp)
	movl	%eax, 16(%rbp)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, 8(%rax)
	movl	%r13d, 16(%rax)
	movl	%r13d, 12(%rax)
.Ltmp4:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp5:
# BB#3:
	movl	$0, (%rbx)
	movl	$1, 4(%rbx)
	movl	%r12d, 8(%rbx)
	movl	$39, 12(%rbx)
	movq	%rbp, 16(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rbx, (%rax)
	cmpl	$178, ch(%rip)
	jne	.LBB0_14
# BB#4:
	movq	8(%rsp), %rax           # 8-byte Reload
	movzwl	(%rax), %eax
	shlq	$4, %rax
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	40(%rdx), %rcx
	movq	64(%rdx), %rsi
	movq	8(%rcx,%rax), %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %r12
	movq	%r12, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_5
# BB#6:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp10:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp11:
# BB#7:                                 # %.noexc
	movq	%r12, (%rbp)
	movl	$0, 8(%rbp)
	movl	$0, 12(%rbp)
	movl	$0, 16(%rbp)
	movl	$1, 8(%r15)
	movl	%r13d, 16(%r15)
	movl	%r13d, 12(%r15)
.Ltmp12:
	movl	$24, %edi
	callq	_Znwm
.Ltmp13:
# BB#8:
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbp, 16(%rax)
	movq	%rax, (%r15)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	8(%rbx), %r12d
	movl	$1, 8(%rbp)
	movl	%r13d, 16(%rbp)
	movl	%r13d, 12(%rbp)
.Ltmp15:
	movl	$24, %edi
	callq	_Znwm
.Ltmp16:
# BB#9:
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	%r12d, 8(%rax)
	movl	$5, 12(%rax)
	movq	%rax, (%rbp)
	movq	%r15, 24(%rbp)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 32(%rbp)
	movl	$0, 8(%rbx)
	jmp	.LBB0_10
.LBB0_14:
	movq	stkptr(%rip), %r15
	movq	-8(%r15), %rbp
	movq	(%rbp), %rax
	cmpl	$1, 4(%rax)
	jne	.LBB0_17
# BB#15:
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movl	$.L.str, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_16
.LBB0_17:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movq	-8(%r15), %r12
	movl	16(%r12), %eax
	cmpl	%eax, %r13d
	cmovbl	%r13d, %eax
	movl	8(%rbx), %ebx
	movl	$1, 8(%rbp)
	movl	%r13d, 12(%rbp)
	movl	%eax, 16(%rbp)
.Ltmp7:
	movl	$24, %edi
	callq	_Znwm
.Ltmp8:
# BB#18:
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	%ebx, 8(%rax)
	movl	$5, 12(%rax)
	movq	%rax, (%rbp)
	movq	%r12, 24(%rbp)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 32(%rbp)
	movq	%rbp, -8(%r15)
	movq	(%rax), %rax
	movl	$0, 8(%rax)
	jmp	.LBB0_19
.LBB0_5:
	movq	(%rsp), %rbp            # 8-byte Reload
.LBB0_10:
	movq	stkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	%rbp, (%rax)
.LBB0_19:
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_16:
	movl	16(%rbp), %eax
	cmpl	%eax, %r13d
	cmovbl	%r13d, %eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, 16(%rcx)
	movq	%rcx, -8(%r15)
	jmp	.LBB0_19
.LBB0_12:
.Ltmp17:
	jmp	.LBB0_13
.LBB0_22:
.Ltmp9:
.LBB0_13:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	jmp	.LBB0_21
.LBB0_11:
.Ltmp14:
	movq	%rax, %rbx
	movq	%r15, %rdi
	jmp	.LBB0_21
.LBB0_20:
.Ltmp6:
	movq	%rax, %rbx
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB0_21:
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_Z5dogetP9Classfile, .Lfunc_end0-_Z5dogetP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp5-.Ltmp0           #   Call between .Ltmp0 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp5          #   Call between .Ltmp5 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 4 <<
	.long	.Ltmp13-.Ltmp10         #   Call between .Ltmp10 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin0   #     jumps to .Ltmp17
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp7-.Ltmp16          #   Call between .Ltmp16 and .Ltmp7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 8 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 9 <<
	.long	.Lfunc_end0-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	39                      # 0x27
	.text
	.globl	_Z5doputP9Classfile
	.p2align	4, 0x90
	.type	_Z5doputP9Classfile,@function
_Z5doputP9Classfile:                    # @_Z5doputP9Classfile
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	currpc(%rip), %r14d
	leal	2(%r14), %eax
	movl	%eax, currpc(%rip)
	addl	$-2, bufflength(%rip)
	movq	inbuff(%rip), %rax
	leaq	2(%rax), %rcx
	movq	%rcx, inbuff(%rip)
	movzbl	(%rax), %ecx
	shlq	$8, %rcx
	movzbl	1(%rax), %eax
	orq	%rcx, %rax
	movq	40(%r12), %rbx
	shlq	$4, %rax
	movq	8(%rbx,%rax), %r13
	movzwl	2(%r13), %eax
	shlq	$4, %rax
	movq	8(%rbx,%rax), %rbp
	movzwl	(%rbp), %eax
	shlq	$4, %rax
	movq	8(%rbx,%rax), %r15
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, (%rsp)            # 8-byte Spill
	movzwl	2(%rbp), %eax
	shlq	$4, %rax
	movq	8(%rbx,%rax), %rdi
.Ltmp18:
	callq	_Z8sig2typePc
	movl	%eax, %ebp
.Ltmp19:
# BB#1:
.Ltmp20:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp21:
# BB#2:                                 # %.noexc
	decl	%r14d
	movq	%r15, (%rbx)
	movl	%ebp, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movl	$1, 8(%rax)
	movl	%r14d, 16(%rax)
	movl	%r14d, 12(%rax)
.Ltmp22:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp23:
# BB#3:
	movl	$0, (%r15)
	movl	$1, 4(%r15)
	movl	%ebp, 8(%r15)
	movl	$39, 12(%r15)
	movq	%rbx, 16(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%r15, (%rax)
	cmpl	$179, ch(%rip)
	jne	.LBB1_21
# BB#4:
	movzwl	(%r13), %eax
	shlq	$4, %rax
	movq	40(%r12), %rcx
	movq	64(%r12), %rsi
	movq	8(%rcx,%rax), %rax
	shlq	$4, %rax
	movq	8(%rcx,%rax), %rbp
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_5
# BB#6:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r12
.Ltmp34:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp35:
# BB#7:                                 # %.noexc128
	movq	%rbp, (%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movl	$1, 8(%r12)
	movl	%r14d, 16(%r12)
	movl	%r14d, 12(%r12)
.Ltmp36:
	movl	$24, %edi
	callq	_Znwm
.Ltmp37:
# BB#8:
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [0,1,0,39]
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%rax, (%r12)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movl	8(%r15), %ebx
	movl	$1, 8(%rbp)
	movl	%r14d, 16(%rbp)
	movl	%r14d, 12(%rbp)
.Ltmp39:
	movl	$24, %edi
	callq	_Znwm
.Ltmp40:
# BB#9:
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	%ebx, 8(%rax)
	movl	$5, 12(%rax)
	movq	%rax, (%rbp)
	movq	%r12, 24(%rbp)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 32(%rbp)
	movl	$0, 8(%r15)
	movq	%rbp, %r12
	jmp	.LBB1_10
.LBB1_21:
	movq	stkptr(%rip), %rbx
	movq	-16(%rbx), %rbp
	movq	(%rbp), %rax
	cmpl	$1, 4(%rax)
	jne	.LBB1_32
# BB#22:
	movq	16(%rax), %rax
	movq	(%rax), %rdi
	movl	$.L.str, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_23
.LBB1_32:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %r12
	movq	-8(%rbx), %rax
	movq	%r14, %rcx
	movl	16(%rax), %r14d
	cmpl	%ecx, %r14d
	cmovael	%ecx, %r14d
	movl	8(%r15), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	$1, 8(%r12)
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, 12(%r12)
	movl	%r14d, 16(%r12)
.Ltmp25:
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp26:
# BB#33:
	addq	$-8, %rbx
	movl	$0, (%r13)
	movl	$4, 4(%r13)
	movl	12(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%r13)
	movl	$5, 12(%r13)
	movq	%r13, (%r12)
	movq	%rbp, 24(%r12)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 32(%r12)
	movl	$0, 8(%r15)
	movq	%rbx, stkptr(%rip)
	movq	(%rbx), %rbp
	movq	(%rbp), %rax
	cmpl	$4, 8(%rax)
	jne	.LBB1_38
# BB#34:
	cmpl	$10, 8(%r13)
	jne	.LBB1_38
# BB#35:
	movl	$std_exps+48, %ecx
	cmpq	%rcx, %rax
	je	.LBB1_37
# BB#36:
	movl	$std_exps+72, %ecx
	cmpq	%rcx, %rax
	jne	.LBB1_38
.LBB1_37:
	addq	$312, %rax              # imm = 0x138
	movq	%rax, (%rbp)
	movq	(%r12), %r13
.LBB1_38:                               # %._crit_edge
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	cmpl	%r14d, 16(%rbp)
	movq	%r12, %rax
	cmovbq	%rbp, %rax
	movl	16(%rax), %eax
	movl	8(%r13), %r14d
	movl	$1, 8(%rbx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp28:
	movl	$24, %edi
	callq	_Znwm
.Ltmp29:
	jmp	.LBB1_39
.LBB1_5:
	movq	(%rsp), %r12            # 8-byte Reload
.LBB1_10:
	movq	stkptr(%rip), %rax
	leaq	-8(%rax), %rcx
	movq	%rcx, stkptr(%rip)
	movq	-8(%rax), %rbp
	movq	(%rbp), %rax
	cmpl	$4, 8(%rax)
	jne	.LBB1_15
# BB#11:
	movq	(%r12), %rcx
	cmpl	$10, 8(%rcx)
	jne	.LBB1_15
# BB#12:
	movl	$std_exps+48, %ecx
	cmpq	%rcx, %rax
	je	.LBB1_14
# BB#13:
	movl	$std_exps+72, %ecx
	cmpq	%rcx, %rax
	jne	.LBB1_15
.LBB1_14:
	addq	$312, %rax              # imm = 0x138
	movq	%rax, (%rbp)
.LBB1_15:                               # %._crit_edge155
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	16(%rbp), %eax
	cmpl	%eax, %r14d
	cmovbl	%r14d, %eax
	movq	(%r12), %rcx
	movq	%r14, %rdx
	movl	8(%rcx), %r14d
	movl	$1, 8(%rbx)
	movl	%edx, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp42:
	movl	$24, %edi
	callq	_Znwm
.Ltmp43:
.LBB1_39:                               # %_ZN3ExpC2Eji7Exptype4Type2OpPS_S3_.exit125
	movl	$0, (%rax)
	movl	$4, 4(%rax)
	movl	%r14d, 8(%rax)
	movl	$6, 12(%rax)
	movq	%rax, (%rbx)
	movq	%r12, 24(%rbx)
	movq	%rbp, 32(%rbx)
	movq	donestkptr(%rip), %rax
	leaq	8(%rax), %rcx
	movq	%rcx, donestkptr(%rip)
	movq	%rbx, (%rax)
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_23:
	leaq	-8(%rbx), %rbp
	movq	%rbp, stkptr(%rip)
	movq	-8(%rbx), %rax
	movq	%rax, %r13
	movq	(%rax), %rax
	cmpl	$4, 8(%rax)
	jne	.LBB1_24
# BB#25:
	cmpl	$10, 8(%r15)
	movq	(%rsp), %r12            # 8-byte Reload
	jne	.LBB1_29
# BB#26:
	movl	$std_exps+48, %ecx
	cmpq	%rcx, %rax
	je	.LBB1_28
# BB#27:
	movl	$std_exps+72, %ecx
	cmpq	%rcx, %rax
	jne	.LBB1_29
.LBB1_28:
	addq	$312, %rax              # imm = 0x138
	movq	%rax, (%r13)
	movq	stkptr(%rip), %rbp
	movq	(%r12), %r15
	jmp	.LBB1_29
.LBB1_24:
	movq	(%rsp), %r12            # 8-byte Reload
.LBB1_29:                               # %._crit_edge154
	leaq	-8(%rbp), %rax
	movq	%rax, stkptr(%rip)
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movq	-8(%rbp), %rax
	movl	16(%rax), %eax
	cmpl	%eax, %r14d
	cmovbl	%r14d, %eax
	movq	%r14, %rcx
	movl	8(%r15), %r14d
	movl	$1, 8(%rbx)
	movl	%ecx, 12(%rbx)
	movl	%eax, 16(%rbx)
.Ltmp31:
	movl	$24, %edi
	callq	_Znwm
.Ltmp32:
# BB#30:
	movq	%r13, %rbp
	jmp	.LBB1_39
.LBB1_31:
.Ltmp33:
	jmp	.LBB1_20
.LBB1_18:
.Ltmp41:
	movq	%rax, %r15
	movq	%rbp, %rdi
	jmp	.LBB1_41
.LBB1_43:
.Ltmp30:
	jmp	.LBB1_20
.LBB1_42:
.Ltmp27:
	jmp	.LBB1_17
.LBB1_19:
.Ltmp44:
.LBB1_20:
	movq	%rax, %r15
	movq	%rbx, %rdi
	jmp	.LBB1_41
.LBB1_16:
.Ltmp38:
.LBB1_17:
	movq	%rax, %r15
	movq	%r12, %rdi
	jmp	.LBB1_41
.LBB1_40:
.Ltmp24:
	movq	%rax, %r15
	movq	(%rsp), %rdi            # 8-byte Reload
.LBB1_41:
	callq	_ZdlPv
	movq	%r15, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_Z5doputP9Classfile, .Lfunc_end1-_Z5doputP9Classfile
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\306\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp18-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp23-.Ltmp18         #   Call between .Ltmp18 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin1   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp34-.Ltmp23         #   Call between .Ltmp23 and .Ltmp34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp37-.Ltmp34         #   Call between .Ltmp34 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin1   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp39-.Ltmp37         #   Call between .Ltmp37 and .Ltmp39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp39-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin1   #     jumps to .Ltmp41
	.byte	0                       #   On action: cleanup
	.long	.Ltmp40-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp25-.Ltmp40         #   Call between .Ltmp40 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin1   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp26-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp28-.Ltmp26         #   Call between .Ltmp26 and .Ltmp28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp42-.Ltmp29         #   Call between .Ltmp29 and .Ltmp42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	0                       #   On action: cleanup
	.long	.Ltmp43-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp31-.Ltmp43         #   Call between .Ltmp43 and .Ltmp31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp32-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Lfunc_end1-.Ltmp32     #   Call between .Ltmp32 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"this"
	.size	.L.str, 5


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
