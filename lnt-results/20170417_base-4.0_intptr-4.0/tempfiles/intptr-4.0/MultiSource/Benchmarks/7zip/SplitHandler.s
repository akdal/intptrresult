	.text
	.file	"SplitHandler.bc"
	.globl	_ZN8NArchive6NSplit8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive6NSplit8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive6NSplit8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$2, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end0:
	.size	_ZN8NArchive6NSplit8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end0-_ZN8NArchive6NSplit8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive6NSplit8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive6NSplit8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive6NSplit8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$1, %esi
	ja	.LBB1_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive6NSplit6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive6NSplit6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	_ZN8NArchive6NSplit8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end1-_ZN8NArchive6NSplit8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive6NSplit8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive6NSplit8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive6NSplit8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end2:
	.size	_ZN8NArchive6NSplit8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end2-_ZN8NArchive6NSplit8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive6NSplit8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive6NSplit8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive6NSplit8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	testl	%esi, %esi
	jne	.LBB3_2
# BB#1:
	movl	_ZN8NArchive6NSplit9kArcPropsE+8(%rip), %eax
	movl	%eax, (%rcx)
	movzwl	_ZN8NArchive6NSplit9kArcPropsE+12(%rip), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB3_2:
	retq
.Lfunc_end3:
	.size	_ZN8NArchive6NSplit8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end3-_ZN8NArchive6NSplit8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive6NSplit8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive6NSplit8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive6NSplit8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 32
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movl	$0, (%rsp)
	cmpl	$39, %esi
	je	.LBB4_3
# BB#1:
	cmpl	$1, %esi
	jne	.LBB4_4
# BB#2:
.Ltmp2:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp3:
	jmp	.LBB4_4
.LBB4_3:
	movl	52(%rdi), %esi
.Ltmp0:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEj
.Ltmp1:
.LBB4_4:
.Ltmp4:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp5:
# BB#5:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB4_6:
.Ltmp6:
	movq	%rax, %rbx
.Ltmp7:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp8:
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_8:
.Ltmp9:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive6NSplit8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end4-_ZN8NArchive6NSplit8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp5-.Ltmp2           #   Call between .Ltmp2 and .Ltmp5
	.long	.Ltmp6-.Lfunc_begin0    #     jumps to .Ltmp6
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp7-.Ltmp5           #   Call between .Ltmp5 and .Ltmp7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp8-.Ltmp7           #   Call between .Ltmp7 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	1                       #   On action: 1
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp8      #   Call between .Ltmp8 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end5:
	.size	__clang_call_terminate, .Lfunc_end5-__clang_call_terminate

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	108                     # 0x6c
	.long	101                     # 0x65
.LCPI6_1:
	.zero	16
	.text
	.globl	_ZN8NArchive6NSplit8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive6NSplit8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive6NSplit8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi9:
	.cfi_def_cfa_offset 320
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rsi, %rbp
	movq	%rdi, %r14
	movq	(%r14), %rax
.Ltmp10:
	callq	*48(%rax)
.Ltmp11:
# BB#1:
	testq	%r15, %r15
	je	.LBB6_23
# BB#2:
	movq	$0, 72(%rsp)
	movq	(%r15), %rax
.Ltmp13:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp14:
# BB#3:                                 # %_ZN9CMyComPtrI20IArchiveOpenCallbackEC2EPS0_.exit
	movq	(%r15), %rax
.Ltmp16:
	leaq	72(%rsp), %rdx
	movl	$IID_IArchiveOpenVolumeCallback, %esi
	movq	%r15, %rdi
	callq	*(%rax)
.Ltmp17:
# BB#4:                                 # %_ZNK9CMyComPtrI20IArchiveOpenCallbackE14QueryInterfaceI26IArchiveOpenVolumeCallbackEEiRK4GUIDPPT_.exit
	movl	$1, %r13d
	testl	%eax, %eax
	movl	$1, %ebx
	jne	.LBB6_197
# BB#5:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 48(%rsp)
.Ltmp18:
	movl	$16, %edi
	callq	_Znam
.Ltmp19:
# BB#6:
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movq	%rax, 48(%rsp)
	movl	$0, (%rax)
	movl	$4, 60(%rsp)
	movl	$0, 8(%rsp)
	movq	72(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp21:
	leaq	8(%rsp), %rdx
	movl	$4, %esi
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp22:
# BB#7:
	movl	$1, %ebx
	testl	%ebp, %ebp
	jne	.LBB6_20
# BB#8:
	movzwl	8(%rsp), %eax
	cmpl	$8, %eax
	movl	$1, %ebp
	jne	.LBB6_20
# BB#9:
	movq	16(%rsp), %rbp
	movl	$0, 56(%rsp)
	movq	48(%rsp), %rbx
	movl	$0, (%rbx)
	xorl	%r13d, %r13d
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB6_10:                               # =>This Inner Loop Header: Depth=1
	incl	%r13d
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB6_10
# BB#11:                                # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	60(%rsp), %r12d
	cmpl	%r13d, %r12d
	je	.LBB6_17
# BB#12:
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp23:
	callq	_Znam
.Ltmp24:
# BB#13:                                # %.noexc165
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB6_16
# BB#14:                                # %.noexc165
	testl	%r12d, %r12d
	jle	.LBB6_16
# BB#15:                                # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	56(%rsp), %rcx
.LBB6_16:                               # %._crit_edge16.i.i
	movq	%rax, 48(%rsp)
	movl	$0, (%rax,%rcx,4)
	movl	%r13d, 60(%rsp)
	movq	%rax, %rbx
.LBB6_17:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
	decl	%r13d
	.p2align	4, 0x90
.LBB6_18:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %eax
	addq	$4, %rbp
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB6_18
# BB#19:                                # %_ZN11CStringBaseIwEaSEPKw.exit
	movl	%r13d, 56(%rsp)
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
.LBB6_20:
.Ltmp28:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp29:
# BB#21:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit167
	movl	$1, %r13d
	testl	%ebx, %ebx
	je	.LBB6_24
# BB#22:
	movl	%ebp, %ebx
	jmp	.LBB6_195
.LBB6_23:
	movl	$1, %eax
	jmp	.LBB6_201
.LBB6_24:
	movslq	56(%rsp), %r13
	testq	%r13, %r13
	movl	$-1, %r12d
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	je	.LBB6_29
# BB#25:
	movq	48(%rsp), %rax
	leaq	(,%r13,4), %rcx
	.p2align	4, 0x90
.LBB6_26:                               # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rax,%rcx)
	je	.LBB6_28
# BB#27:                                #   in Loop: Header=BB6_26 Depth=1
	addq	$-4, %rcx
	jne	.LBB6_26
	jmp	.LBB6_29
.LBB6_28:
	leaq	-4(%rax,%rcx), %r12
	subq	%rax, %r12
	shrq	$2, %r12
.LBB6_29:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 80(%rsp)
.Ltmp31:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp32:
# BB#30:
	movq	%rbp, 80(%rsp)
	movl	$0, (%rbp)
	movl	$4, 92(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
.Ltmp34:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp35:
# BB#31:
	movq	%rbx, 32(%rsp)
	movl	$0, (%rbx)
	movl	$4, 44(%rsp)
	testl	%r12d, %r12d
	movq	%r14, 24(%rsp)          # 8-byte Spill
	js	.LBB6_46
# BB#32:
	incl	%r12d
.Ltmp39:
	leaq	112(%rsp), %rdi
	leaq	48(%rsp), %rsi
	xorl	%edx, %edx
	movl	%r12d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp40:
# BB#33:                                # %_ZNK11CStringBaseIwE4LeftEi.exit
	movl	$0, 88(%rsp)
	movq	80(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	120(%rsp), %rbp
	incq	%rbp
	movl	92(%rsp), %r14d
	cmpl	%r14d, %ebp
	je	.LBB6_39
# BB#34:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp42:
	callq	_Znam
	movq	%rax, %r13
.Ltmp43:
# BB#35:                                # %.noexc183
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_38
# BB#36:                                # %.noexc183
	testl	%r14d, %r14d
	jle	.LBB6_38
# BB#37:                                # %._crit_edge.thread.i.i178
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	88(%rsp), %rax
.LBB6_38:                               # %._crit_edge16.i.i179
	movq	%r13, 80(%rsp)
	movl	$0, (%r13,%rax,4)
	movl	%ebp, 92(%rsp)
	movq	%r13, %rbx
.LBB6_39:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i180
	movq	112(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_40:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_40
# BB#41:
	movl	120(%rsp), %eax
	movl	%eax, 88(%rsp)
	testq	%rdi, %rdi
	je	.LBB6_43
# BB#42:
	callq	_ZdaPv
.LBB6_43:                               # %_ZN11CStringBaseIwED2Ev.exit184
	movl	56(%rsp), %ecx
	subl	%r12d, %ecx
.Ltmp45:
	leaq	112(%rsp), %rdi
	leaq	48(%rsp), %rsi
	movl	%r12d, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp46:
# BB#44:                                # %_ZNK11CStringBaseIwE3MidEi.exit
	movl	$0, 40(%rsp)
	movq	32(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	120(%rsp), %rbp
	incq	%rbp
	movl	44(%rsp), %r14d
	cmpl	%r14d, %ebp
	jne	.LBB6_52
# BB#45:
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB6_58
.LBB6_46:
	movl	$0, 40(%rsp)
	movl	$0, (%rbx)
	incl	%r13d
	cmpl	$4, %r13d
	movq	176(%rsp), %r12         # 8-byte Reload
	je	.LBB6_49
# BB#47:
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp37:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp38:
# BB#48:                                # %._crit_edge16.i.i203
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%rsp), %rax
	movq	%rbp, 32(%rsp)
	movl	$0, (%rbp,%rax,4)
	movl	%r13d, 44(%rsp)
	movq	%rbp, %rbx
.LBB6_49:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i204
	movq	48(%rsp), %rax
	.p2align	4, 0x90
.LBB6_50:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB6_50
# BB#51:                                # %_ZN11CStringBaseIwEaSERKS0_.exit208
	movl	56(%rsp), %ebx
	movl	%ebx, 40(%rsp)
	jmp	.LBB6_62
.LBB6_52:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp48:
	callq	_Znam
	movq	%rax, %r12
.Ltmp49:
# BB#53:                                # %.noexc194
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_56
# BB#54:                                # %.noexc194
	testl	%r14d, %r14d
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB6_57
# BB#55:                                # %._crit_edge.thread.i.i189
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%rsp), %rax
	jmp	.LBB6_57
.LBB6_56:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB6_57:                               # %._crit_edge16.i.i190
	movq	%r12, 32(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 44(%rsp)
	movq	%r12, %rbx
.LBB6_58:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i191
	movq	176(%rsp), %r12         # 8-byte Reload
	movq	112(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_59:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_59
# BB#60:
	movl	120(%rsp), %ebx
	movl	%ebx, 40(%rsp)
	testq	%rdi, %rdi
	je	.LBB6_62
# BB#61:
	callq	_ZdaPv
	movl	40(%rsp), %ebx
.LBB6_62:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 160(%rsp)
	movl	%ebx, %ebp
	incl	%ebp
	je	.LBB6_65
# BB#63:                                # %._crit_edge16.i.i209
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp51:
	callq	_Znam
	movq	%rax, %rdi
.Ltmp52:
# BB#64:                                # %.noexc213
	movq	%rdi, 160(%rsp)
	movl	$0, (%rdi)
	movl	%ebp, 172(%rsp)
	jmp	.LBB6_66
.LBB6_65:
	xorl	%edi, %edi
.LBB6_66:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i210
	movq	32(%rsp), %rax
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB6_67:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB6_67
# BB#68:
	movl	%ebx, 168(%rsp)
.Ltmp54:
	callq	_Z13MyStringUpperPw
.Ltmp55:
# BB#69:                                # %_ZN11CStringBaseIwE9MakeUpperEv.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 112(%rsp)
.Ltmp56:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp57:
# BB#70:                                # %.noexc215
	movq	%rbx, 112(%rsp)
	movl	$0, (%rbx)
	movl	$4, 124(%rsp)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 128(%rsp)
.Ltmp59:
	movl	$16, %edi
	callq	_Znam
.Ltmp60:
# BB#71:
	movq	%rax, 128(%rsp)
	movl	$0, (%rax)
	movl	$4, 140(%rsp)
	movl	168(%rsp), %edx
	cmpl	$3, %edx
	movl	$2, %ecx
	cmovll	%edx, %ecx
	subl	%ecx, %edx
.Ltmp62:
	leaq	8(%rsp), %rdi
	leaq	160(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp63:
# BB#72:                                # %_ZNK11CStringBaseIwE5RightEi.exit
	movq	8(%rsp), %rdi
.Ltmp65:
	movl	$.L.str, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp66:
# BB#73:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_75
# BB#74:
	callq	_ZdaPv
.LBB6_75:                               # %_ZN11CStringBaseIwED2Ev.exit218
	testl	%ebx, %ebx
	je	.LBB6_82
# BB#76:
	movl	40(%rsp), %edx
	cmpl	$3, %edx
	movl	$2, %ecx
	cmovll	%edx, %ecx
	subl	%ecx, %edx
.Ltmp68:
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp69:
# BB#77:                                # %_ZNK11CStringBaseIwE5RightEi.exit222
	movq	8(%rsp), %rdi
.Ltmp71:
	movl	$.L.str.1, %esi
	callq	_Z15MyStringComparePKwS0_
	movl	%eax, %ebx
.Ltmp72:
# BB#78:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_80
# BB#79:
	callq	_ZdaPv
.LBB6_80:                               # %_ZN11CStringBaseIwED2Ev.exit225
	movl	$1, %r13d
	testl	%ebx, %ebx
	je	.LBB6_86
# BB#81:
	movl	$1, %ebx
	jmp	.LBB6_185
.LBB6_82:                               # %.preheader366
	movl	168(%rsp), %eax
	movl	$2, %ebp
	movb	$1, %r13b
	cmpl	$3, %eax
	jl	.LBB6_92
# BB#83:                                # %.lr.ph
	movq	160(%rsp), %rcx
	leal	-3(%rax), %edx
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB6_84:                               # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rsi
	cmpl	$65, (%rcx,%rsi,4)
	jne	.LBB6_92
# BB#85:                                #   in Loop: Header=BB6_84 Depth=1
	incl	%ebp
	decl	%edx
	cmpl	%ebp, %eax
	jg	.LBB6_84
	jmp	.LBB6_92
.LBB6_86:                               # %.preheader368
	movl	168(%rsp), %eax
	movl	$2, %ebp
	cmpl	$3, %eax
	jl	.LBB6_90
# BB#87:                                # %.lr.ph437
	movq	160(%rsp), %rcx
	leal	-3(%rax), %edx
	movl	$2, %ebp
.LBB6_88:                               # =>This Inner Loop Header: Depth=1
	movslq	%edx, %rsi
	cmpl	$48, (%rcx,%rsi,4)
	jne	.LBB6_90
# BB#89:                                #   in Loop: Header=BB6_88 Depth=1
	incl	%ebp
	decl	%edx
	cmpl	%ebp, %eax
	jg	.LBB6_88
.LBB6_90:                               # %._crit_edge
	cmpl	40(%rsp), %ebp
	movl	$1, %ebx
	jne	.LBB6_185
# BB#91:
	xorl	%r13d, %r13d
.LBB6_92:                               # %.loopexit367
	testq	%r12, %r12
	je	.LBB6_94
# BB#93:
	movq	(%r12), %rax
.Ltmp74:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp75:
.LBB6_94:                               # %_ZN9CMyComPtrI9IInStreamEC2EPS0_.exit
.Ltmp76:
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp77:
# BB#95:                                # %.noexc230
	testq	%r12, %r12
	movq	%r12, (%rbx)
	je	.LBB6_97
# BB#96:
	movq	(%r12), %rax
.Ltmp78:
	movq	%r12, %rdi
	callq	*8(%rax)
.Ltmp79:
.LBB6_97:                               # %_ZN9CMyComPtrI9IInStreamEC2ERKS1_.exit.i
	leaq	40(%r14), %rdi
.Ltmp81:
	movq	%rdi, 256(%rsp)         # 8-byte Spill
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp82:
# BB#98:
	movslq	52(%r14), %rax
	leal	1(%rax), %ecx
	testq	%r12, %r12
	movq	56(%r14), %rdx
	movq	%rbx, (%rdx,%rax,8)
	movl	%ecx, 52(%r14)
	je	.LBB6_100
# BB#99:
	movq	(%r12), %rax
.Ltmp86:
	movq	%r12, %rdi
	callq	*16(%rax)
.Ltmp87:
.LBB6_100:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	movl	168(%rsp), %ecx
	subl	%ebp, %ecx
.Ltmp89:
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rsi
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp90:
# BB#101:                               # %_ZNK11CStringBaseIwE4LeftEi.exit236
	movslq	88(%rsp), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	movl	%r13d, 204(%rsp)        # 4-byte Spill
	je	.LBB6_104
# BB#102:                               # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp92:
	callq	_Znam
.Ltmp93:
# BB#103:                               # %.noexc238
	movl	$0, (%rax)
	movl	%ebx, %r12d
	movq	%rax, %r13
	jmp	.LBB6_105
.LBB6_104:
	xorl	%eax, %eax
	xorl	%r13d, %r13d
.LBB6_105:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	80(%rsp), %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB6_106:                              # =>This Inner Loop Header: Depth=1
	movl	(%rsi,%rcx), %edx
	movl	%edx, (%r13,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB6_106
# BB#107:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	16(%rsp), %ebx
	movl	%r12d, %ecx
	subl	%r14d, %ecx
	cmpl	%ebx, %ecx
	jle	.LBB6_109
# BB#108:
	movq	%rax, %rdi
	jmp	.LBB6_120
.LBB6_109:
	decl	%ecx
	cmpl	$8, %r12d
	movl	$16, %esi
	movl	$4, %edx
	cmovgl	%esi, %edx
	cmpl	$65, %r12d
	jl	.LBB6_111
# BB#110:                               # %select.true.sink
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
.LBB6_111:                              # %select.end
	leal	(%rdx,%rcx), %esi
	movl	%ebx, %edi
	subl	%ecx, %edi
	cmpl	%ebx, %esi
	cmovgel	%edx, %edi
	leal	1(%r12,%rdi), %ecx
	cmpl	%r12d, %ecx
	jne	.LBB6_113
# BB#112:
	movq	%rax, %rdi
	jmp	.LBB6_120
.LBB6_113:
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp95:
	callq	_Znam
	movq	%rax, %rdi
.Ltmp96:
# BB#114:                               # %.noexc309
	testl	%r12d, %r12d
	jle	.LBB6_119
# BB#115:                               # %.preheader.i.i
	testl	%r14d, %r14d
	movq	96(%rsp), %rsi          # 8-byte Reload
	jle	.LBB6_117
# BB#116:                               # %.lr.ph.i.i
	leaq	(,%r14,4), %rdx
	movq	%rdi, %rbx
	callq	memcpy
	movq	%rbx, %rdi
	jmp	.LBB6_118
.LBB6_117:                              # %._crit_edge.i.i
	testq	%r13, %r13
	je	.LBB6_119
.LBB6_118:                              # %._crit_edge.thread.i.i306
	movq	%rdi, %rbx
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	%rbx, %rdi
.LBB6_119:                              # %._crit_edge16.i.i307
	movl	$0, (%rdi,%r14,4)
	movq	%rdi, %r13
.LBB6_120:                              # %.noexc.i
	leaq	(%r13,%r14,4), %rcx
	movq	8(%rsp), %rsi
	.p2align	4, 0x90
.LBB6_121:                              # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %eax
	addq	$4, %rsi
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB6_121
# BB#122:
	movslq	16(%rsp), %r12
	movl	$0, 120(%rsp)
	movq	112(%rsp), %rbx
	movl	$0, (%rbx)
	movq	%r14, 96(%rsp)          # 8-byte Spill
	leaq	1(%r12,%r14), %rax
	movl	124(%rsp), %r14d
	cmpl	%r14d, %eax
	jne	.LBB6_124
# BB#123:
	movq	96(%rsp), %r14          # 8-byte Reload
	jmp	.LBB6_130
.LBB6_124:
	movq	%rdi, 208(%rsp)         # 8-byte Spill
	movl	$4, %ecx
	movq	%rax, 248(%rsp)         # 8-byte Spill
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp98:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp99:
# BB#125:                               # %.noexc249
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_128
# BB#126:                               # %.noexc249
	testl	%r14d, %r14d
	movq	96(%rsp), %r14          # 8-byte Reload
	jle	.LBB6_129
# BB#127:                               # %._crit_edge.thread.i.i244
	movq	%rbx, %rdi
	movq	%rcx, %rbx
	callq	_ZdaPv
	movq	%rbx, %rcx
	movslq	120(%rsp), %rax
	jmp	.LBB6_129
.LBB6_128:
	movq	96(%rsp), %r14          # 8-byte Reload
.LBB6_129:                              # %._crit_edge16.i.i245
	movq	%rcx, 112(%rsp)
	movl	$0, (%rcx,%rax,4)
	movq	248(%rsp), %rax         # 8-byte Reload
	movl	%eax, 124(%rsp)
	movq	%rcx, %rbx
	movq	208(%rsp), %rdi         # 8-byte Reload
.LBB6_130:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i246.preheader
	addq	%r14, %r12
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_131:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i246
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_131
# BB#132:
	movl	%r12d, 120(%rsp)
	testq	%r13, %r13
	je	.LBB6_134
# BB#133:
	callq	_ZdaPv
.LBB6_134:                              # %_ZN11CStringBaseIwED2Ev.exit251
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	movl	204(%rsp), %r13d        # 4-byte Reload
	je	.LBB6_136
# BB#135:
	callq	_ZdaPv
.LBB6_136:                              # %_ZN11CStringBaseIwED2Ev.exit252
	movl	40(%rsp), %edx
	cmpl	%ebp, %edx
	cmovlel	%edx, %ebp
	subl	%ebp, %edx
.Ltmp101:
	leaq	8(%rsp), %rdi
	leaq	32(%rsp), %rsi
	movl	%ebp, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp102:
# BB#137:                               # %_ZNK11CStringBaseIwE5RightEi.exit255
	movl	$0, 136(%rsp)
	movq	128(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %rbp
	incq	%rbp
	movl	140(%rsp), %r14d
	cmpl	%r14d, %ebp
	jne	.LBB6_139
# BB#138:
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB6_145
.LBB6_139:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp104:
	callq	_Znam
	movq	%rax, %r12
.Ltmp105:
# BB#140:                               # %.noexc264
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_143
# BB#141:                               # %.noexc264
	testl	%r14d, %r14d
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB6_144
# BB#142:                               # %._crit_edge.thread.i.i259
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	136(%rsp), %rax
	jmp	.LBB6_144
.LBB6_143:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB6_144:                              # %._crit_edge16.i.i260
	movq	%r12, 128(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 140(%rsp)
	movq	%r12, %rbx
.LBB6_145:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i261
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_146:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_146
# BB#147:
	movl	16(%rsp), %eax
	movl	%eax, 136(%rsp)
	testq	%rdi, %rdi
	je	.LBB6_149
# BB#148:
	callq	_ZdaPv
.LBB6_149:                              # %_ZN11CStringBaseIwED2Ev.exit266
	movb	%r13b, 144(%rsp)
	movl	88(%rsp), %ecx
	testl	%ecx, %ecx
	jle	.LBB6_154
# BB#150:
	decl	%ecx
.Ltmp107:
	leaq	8(%rsp), %rbx
	leaq	80(%rsp), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp108:
# BB#151:                               # %_ZNK11CStringBaseIwE4LeftEi.exit286
	leaq	24(%r14), %rax
	cmpq	%rax, %rbx
	je	.LBB6_161
# BB#152:
	movl	$0, 32(%r14)
	movq	24(%r14), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %rbp
	incq	%rbp
	movl	36(%r14), %r14d
	cmpl	%r14d, %ebp
	jne	.LBB6_162
# BB#153:
	movq	24(%rsp), %r14          # 8-byte Reload
	jmp	.LBB6_168
.LBB6_154:                              # %_Z11MyStringLenIwEiPKT_.exit.i269
	movl	$0, 32(%r14)
	movq	24(%r14), %rbx
	movl	$0, (%rbx)
	movl	36(%r14), %ebp
	cmpl	$5, %ebp
	je	.LBB6_160
# BB#155:
.Ltmp113:
	movl	$20, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp114:
# BB#156:                               # %.noexc278
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_159
# BB#157:                               # %.noexc278
	testl	%ebp, %ebp
	jle	.LBB6_159
# BB#158:                               # %._crit_edge.thread.i.i273
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	24(%rsp), %rax          # 8-byte Reload
	movslq	32(%rax), %rax
.LBB6_159:                              # %._crit_edge16.i.i274
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%r14, 24(%rcx)
	movl	$0, (%r14,%rax,4)
	movl	$5, 36(%rcx)
	movq	%r14, %rbx
	movq	%rcx, %r14
.LBB6_160:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i277.preheader
	movaps	.LCPI6_0(%rip), %xmm0   # xmm0 = [102,105,108,101]
	movups	%xmm0, (%rbx)
	movl	$0, 16(%rbx)
	movl	$4, 32(%r14)
	jmp	.LBB6_172
.LBB6_161:                              # %_ZNK11CStringBaseIwE4LeftEi.exit286._ZN11CStringBaseIwEaSERKS0_.exit296_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_171
	jmp	.LBB6_172
.LBB6_162:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp110:
	callq	_Znam
	movq	%rax, %r12
.Ltmp111:
# BB#163:                               # %.noexc295
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_166
# BB#164:                               # %.noexc295
	testl	%r14d, %r14d
	movq	24(%rsp), %r14          # 8-byte Reload
	jle	.LBB6_167
# BB#165:                               # %._crit_edge.thread.i.i290
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	32(%r14), %rax
	jmp	.LBB6_167
.LBB6_166:
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB6_167:                              # %._crit_edge16.i.i291
	movq	%r12, 24(%r14)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 36(%r14)
	movq	%r12, %rbx
.LBB6_168:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i292
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_169:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_169
# BB#170:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	16(%rsp), %eax
	movl	%eax, 32(%r14)
	testq	%rdi, %rdi
	je	.LBB6_172
.LBB6_171:
	callq	_ZdaPv
.LBB6_172:
	movq	$0, 104(%r14)
	movl	$0, 216(%rsp)
	movq	72(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp116:
	leaq	216(%rsp), %rdx
	movl	$7, %esi
	callq	*40(%rax)
.Ltmp117:
# BB#173:
	movl	$1, %ebx
	testl	%eax, %eax
	je	.LBB6_175
# BB#174:
                                        # implicit-def: %R12
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB6_178
.LBB6_175:
	movzwl	216(%rsp), %eax
	cmpl	$21, %eax
	jne	.LBB6_177
# BB#176:
	xorl	%ebx, %ebx
	movq	224(%rsp), %r12
	jmp	.LBB6_178
.LBB6_177:
	movl	$-2147024809, 4(%rsp)   # 4-byte Folded Spill
                                        # imm = 0x80070057
                                        # implicit-def: %R12
.LBB6_178:
.Ltmp121:
	leaq	216(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp122:
# BB#179:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit302
	movl	$1, %r13d
	testl	%ebx, %ebx
	je	.LBB6_181
.LBB6_180:                              # %.us-lcssa432.us.loopexit507
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB6_185
.LBB6_181:
	addq	%r12, 104(%r14)
	leaq	72(%r14), %rbx
.Ltmp124:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp125:
# BB#182:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	88(%r14), %rax
	movslq	84(%r14), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 84(%r14)
	movslq	52(%r14), %rax
	movq	%rax, 8(%rsp)
	movq	(%r15), %rax
.Ltmp127:
	leaq	8(%rsp), %rsi
	xorl	%edx, %edx
	movq	%r15, %rdi
	callq	*48(%rax)
	movl	%eax, %ebx
.Ltmp128:
# BB#183:
	xorl	%r13d, %r13d
	testl	%ebx, %ebx
	setne	%al
	je	.LBB6_202
# BB#184:
	movb	%al, %r13b
.LBB6_185:                              # %.us-lcssa432.us
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_187
# BB#186:
	callq	_ZdaPv
.LBB6_187:                              # %_ZN11CStringBaseIwED2Ev.exit.i330
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_189
# BB#188:
	callq	_ZdaPv
.LBB6_189:                              # %_ZN8NArchive6NSplit8CSeqNameD2Ev.exit331
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_191
# BB#190:
	callq	_ZdaPv
.LBB6_191:                              # %_ZN11CStringBaseIwED2Ev.exit332
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_193
# BB#192:
	callq	_ZdaPv
.LBB6_193:                              # %_ZN11CStringBaseIwED2Ev.exit333
	movq	80(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_195
# BB#194:
	callq	_ZdaPv
.LBB6_195:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_197
# BB#196:
	callq	_ZdaPv
.LBB6_197:
	movq	(%r15), %rax
.Ltmp173:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp174:
# BB#198:                               # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit337
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_200
# BB#199:
	movq	(%rdi), %rax
.Ltmp179:
	callq	*16(%rax)
.Ltmp180:
.LBB6_200:                              # %_ZN9CMyComPtrI26IArchiveOpenVolumeCallbackED2Ev.exit339
	testl	%r13d, %r13d
	cmovel	%r13d, %ebx
	movl	%ebx, %eax
.LBB6_201:
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_202:                              # %.preheader
	cmpq	$0, 176(%rsp)           # 8-byte Folded Reload
	je	.LBB6_228
# BB#203:
	leaq	184(%rsp), %rbx
.LBB6_204:                              # %.preheader.split
                                        # =>This Inner Loop Header: Depth=1
.Ltmp130:
	movq	%rbx, %rdi
	leaq	112(%rsp), %rsi
	callq	_ZN8NArchive6NSplit8CSeqName11GetNextNameEv
.Ltmp131:
# BB#205:                               #   in Loop: Header=BB6_204 Depth=1
	movq	$0, 104(%rsp)
	movq	72(%rsp), %rdi
	movq	(%rdi), %rax
	movq	184(%rsp), %rsi
.Ltmp133:
	leaq	104(%rsp), %rdx
	callq	*48(%rax)
.Ltmp134:
# BB#206:                               #   in Loop: Header=BB6_204 Depth=1
	testl	%eax, %eax
	je	.LBB6_209
# BB#207:                               #   in Loop: Header=BB6_204 Depth=1
	movl	$6, %r13d
	cmpl	$1, %eax
	je	.LBB6_223
# BB#208:                               #   in Loop: Header=BB6_204 Depth=1
	movl	$1, %r13d
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jmp	.LBB6_223
.LBB6_209:                              #   in Loop: Header=BB6_204 Depth=1
	movl	$0, 232(%rsp)
	movq	72(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp136:
	movl	$7, %esi
	leaq	232(%rsp), %rbp
	movq	%rbp, %rdx
	callq	*40(%rax)
.Ltmp137:
# BB#210:                               #   in Loop: Header=BB6_204 Depth=1
	testl	%eax, %eax
	je	.LBB6_212
# BB#211:                               #   in Loop: Header=BB6_204 Depth=1
	movl	$1, %ebx
	jmp	.LBB6_213
.LBB6_212:                              #   in Loop: Header=BB6_204 Depth=1
	movzwl	232(%rsp), %eax
	xorl	%ebx, %ebx
	cmpl	$21, %eax
	cmoveq	240(%rsp), %r12
	setne	%bl
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	$-2147024809, %ecx      # imm = 0x80070057
	cmovnel	%ecx, %eax
.LBB6_213:                              #   in Loop: Header=BB6_204 Depth=1
	movl	%eax, 4(%rsp)           # 4-byte Spill
.Ltmp141:
	movq	%rbp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp142:
# BB#214:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit315
                                        #   in Loop: Header=BB6_204 Depth=1
	movl	$1, %r13d
	testl	%ebx, %ebx
	jne	.LBB6_222
# BB#215:                               #   in Loop: Header=BB6_204 Depth=1
	addq	%r12, 104(%r14)
.Ltmp144:
	movq	24(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp145:
# BB#216:                               #   in Loop: Header=BB6_204 Depth=1
	movq	88(%r14), %rax
	movslq	84(%r14), %rcx
	movq	%r12, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 84(%r14)
.Ltmp146:
	movl	$8, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp147:
# BB#217:                               # %.noexc319
                                        #   in Loop: Header=BB6_204 Depth=1
	movq	104(%rsp), %rdi
	movq	%rdi, (%rbx)
	testq	%rdi, %rdi
	je	.LBB6_219
# BB#218:                               #   in Loop: Header=BB6_204 Depth=1
	movq	(%rdi), %rax
.Ltmp148:
	callq	*8(%rax)
.Ltmp149:
.LBB6_219:                              # %_ZN9CMyComPtrI9IInStreamEC2ERKS1_.exit.i318
                                        #   in Loop: Header=BB6_204 Depth=1
.Ltmp151:
	movq	256(%rsp), %rdi         # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp152:
# BB#220:                               #   in Loop: Header=BB6_204 Depth=1
	movq	56(%r14), %rax
	movslq	52(%r14), %rcx
	movq	%rbx, (%rax,%rcx,8)
	incq	%rcx
	movl	%ecx, 52(%r14)
	movq	%rcx, 8(%rsp)
	movq	(%r15), %rax
.Ltmp154:
	xorl	%edx, %edx
	movq	%r15, %rdi
	leaq	8(%rsp), %rsi
	callq	*48(%rax)
.Ltmp155:
# BB#221:                               #   in Loop: Header=BB6_204 Depth=1
	xorl	%r13d, %r13d
	testl	%eax, %eax
	setne	%r13b
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmovnel	%eax, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
.LBB6_222:                              #   in Loop: Header=BB6_204 Depth=1
	leaq	184(%rsp), %rbx
.LBB6_223:                              #   in Loop: Header=BB6_204 Depth=1
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_225
# BB#224:                               #   in Loop: Header=BB6_204 Depth=1
	movq	(%rdi), %rax
.Ltmp157:
	callq	*16(%rax)
.Ltmp158:
.LBB6_225:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit325
                                        #   in Loop: Header=BB6_204 Depth=1
	movq	184(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_227
# BB#226:                               #   in Loop: Header=BB6_204 Depth=1
	callq	_ZdaPv
.LBB6_227:                              # %_ZN11CStringBaseIwED2Ev.exit326
                                        #   in Loop: Header=BB6_204 Depth=1
	movl	%r13d, %eax
	andb	$7, %al
	je	.LBB6_204
	jmp	.LBB6_236
.LBB6_228:                              # %.preheader.split.us.preheader
	leaq	184(%rsp), %r12
	leaq	112(%rsp), %rbp
	leaq	104(%rsp), %r14
.LBB6_229:                              # %.preheader.split.us
                                        # =>This Inner Loop Header: Depth=1
.Ltmp160:
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	_ZN8NArchive6NSplit8CSeqName11GetNextNameEv
.Ltmp161:
# BB#230:                               #   in Loop: Header=BB6_229 Depth=1
	movq	$0, 104(%rsp)
	movq	72(%rsp), %rdi
	movq	(%rdi), %rax
	movq	184(%rsp), %rsi
.Ltmp163:
	movq	%r14, %rdx
	callq	*48(%rax)
.Ltmp164:
# BB#231:                               #   in Loop: Header=BB6_229 Depth=1
	cmpl	$2, %eax
	sbbl	%ebx, %ebx
	cmpl	$2, %eax
	movl	4(%rsp), %ecx           # 4-byte Reload
	cmovael	%eax, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_233
# BB#232:                               #   in Loop: Header=BB6_229 Depth=1
	movq	(%rdi), %rax
.Ltmp168:
	callq	*16(%rax)
.Ltmp169:
.LBB6_233:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit325.us
                                        #   in Loop: Header=BB6_229 Depth=1
	andl	$1, %ebx
	leal	1(%rbx,%rbx,4), %r13d
	movq	184(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_235
# BB#234:                               #   in Loop: Header=BB6_229 Depth=1
	callq	_ZdaPv
.LBB6_235:                              # %_ZN11CStringBaseIwED2Ev.exit326.us
                                        #   in Loop: Header=BB6_229 Depth=1
	movl	%r13d, %eax
	andb	$7, %al
	je	.LBB6_229
.LBB6_236:                              # %_ZN11CStringBaseIwED2Ev.exit326
	cmpb	$6, %al
	jne	.LBB6_180
# BB#237:                               # %.us-lcssa432.us.loopexit494
	xorl	%r13d, %r13d
	jmp	.LBB6_180
.LBB6_238:
.Ltmp115:
	jmp	.LBB6_303
.LBB6_239:
.Ltmp112:
	jmp	.LBB6_270
.LBB6_240:
.Ltmp97:
	movq	%rdx, %r12
	movq	%rax, %r14
	testq	%r13, %r13
	je	.LBB6_248
# BB#241:
	movq	96(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB6_247
.LBB6_242:
.Ltmp129:
	jmp	.LBB6_303
.LBB6_243:                              # %.loopexit.split-lp
.Ltmp126:
	jmp	.LBB6_303
.LBB6_244:
.Ltmp106:
	jmp	.LBB6_270
.LBB6_245:
.Ltmp100:
	movq	%rdx, %r12
	movq	%rax, %r14
	testq	%r13, %r13
	je	.LBB6_248
# BB#246:
	movq	208(%rsp), %rdi         # 8-byte Reload
.LBB6_247:                              # %_ZN11CStringBaseIwED2Ev.exit282
	callq	_ZdaPv
	jmp	.LBB6_248
.LBB6_249:
.Ltmp109:
	jmp	.LBB6_303
.LBB6_250:
.Ltmp94:
	jmp	.LBB6_270
.LBB6_251:                              # %.body232.thread
.Ltmp80:
	movq	%rdx, %rbp
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB6_267
.LBB6_252:
.Ltmp50:
	jmp	.LBB6_254
.LBB6_253:
.Ltmp44:
.LBB6_254:
	movq	%rdx, %r12
	movq	%rax, %r14
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_309
	jmp	.LBB6_310
.LBB6_255:
.Ltmp123:
	jmp	.LBB6_303
.LBB6_256:
.Ltmp118:
	movq	%rdx, %r12
	movq	%rax, %r14
.Ltmp119:
	leaq	216(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp120:
	jmp	.LBB6_304
.LBB6_257:
.Ltmp103:
	jmp	.LBB6_303
.LBB6_258:
.Ltmp91:
	jmp	.LBB6_303
.LBB6_259:
.Ltmp88:
	jmp	.LBB6_303
.LBB6_260:
.Ltmp73:
	jmp	.LBB6_270
.LBB6_261:
.Ltmp70:
	jmp	.LBB6_303
.LBB6_262:
.Ltmp47:
	jmp	.LBB6_265
.LBB6_263:
.Ltmp41:
	jmp	.LBB6_265
.LBB6_264:
.Ltmp53:
.LBB6_265:
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_310
.LBB6_266:                              # %.body232
.Ltmp83:
	movq	%rdx, %rbp
	movq	%rax, %r14
	testq	%r12, %r12
	je	.LBB6_268
.LBB6_267:
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp84:
	callq	*16(%rax)
.Ltmp85:
	movq	%rbp, %r12
	jmp	.LBB6_304
.LBB6_268:
	movq	%rbp, %r12
	jmp	.LBB6_304
.LBB6_269:
.Ltmp67:
.LBB6_270:
	movq	%rdx, %r12
	movq	%rax, %r14
.LBB6_248:                              # %_ZN11CStringBaseIwED2Ev.exit282
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_301
	jmp	.LBB6_304
.LBB6_271:
.Ltmp64:
	jmp	.LBB6_303
.LBB6_272:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp61:
	movq	%rdx, %r12
	movq	%rax, %r14
	movq	%rbx, %rdi
	jmp	.LBB6_307
.LBB6_273:                              # %.thread
.Ltmp36:
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_313
.LBB6_274:
.Ltmp33:
	jmp	.LBB6_277
.LBB6_275:
.Ltmp58:
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_308
.LBB6_276:
.Ltmp30:
.LBB6_277:
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_314
.LBB6_278:
.Ltmp25:
	movq	%rdx, %r12
	movq	%rax, %r14
.Ltmp26:
	leaq	8(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp27:
	jmp	.LBB6_314
.LBB6_279:
.Ltmp181:
	jmp	.LBB6_285
.LBB6_280:
.Ltmp175:
	jmp	.LBB6_282
.LBB6_281:
.Ltmp15:
.LBB6_282:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_317
.LBB6_283:
.Ltmp20:
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_316
.LBB6_284:
.Ltmp12:
.LBB6_285:
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_319
.LBB6_286:                              # %.us-lcssa424
.Ltmp150:
	movq	%rdx, %r12
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB6_298
.LBB6_287:                              # %.us-lcssa428
.Ltmp156:
	jmp	.LBB6_297
.LBB6_288:                              # %.us-lcssa422
.Ltmp143:
	jmp	.LBB6_297
.LBB6_289:                              # %.us-lcssa420
.Ltmp138:
	movq	%rdx, %r12
	movq	%rax, %r14
.Ltmp139:
	leaq	232(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp140:
	jmp	.LBB6_298
.LBB6_290:                              # %.us-lcssa426
.Ltmp153:
	jmp	.LBB6_297
.LBB6_291:                              # %.us-lcssa430.us
.Ltmp170:
	jmp	.LBB6_293
.LBB6_292:                              # %.us-lcssa430
.Ltmp159:
.LBB6_293:
	movq	%rdx, %r12
	movq	%rax, %r14
	jmp	.LBB6_300
.LBB6_294:                              # %.us-lcssa.us
.Ltmp165:
	jmp	.LBB6_297
.LBB6_295:                              # %.loopexit.us-lcssa.us
.Ltmp162:
	jmp	.LBB6_303
.LBB6_296:                              # %.us-lcssa
.Ltmp135:
.LBB6_297:
	movq	%rdx, %r12
	movq	%rax, %r14
.LBB6_298:
	movq	104(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_300
# BB#299:
	movq	(%rdi), %rax
.Ltmp166:
	callq	*16(%rax)
.Ltmp167:
.LBB6_300:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit328
	movq	184(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_304
.LBB6_301:
	callq	_ZdaPv
	jmp	.LBB6_304
.LBB6_302:                              # %.loopexit.us-lcssa
.Ltmp132:
.LBB6_303:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit281
	movq	%rdx, %r12
	movq	%rax, %r14
.LBB6_304:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit281
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_306
# BB#305:
	callq	_ZdaPv
.LBB6_306:                              # %_ZN11CStringBaseIwED2Ev.exit.i311
	movq	112(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_308
.LBB6_307:
	callq	_ZdaPv
.LBB6_308:
	movq	160(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_310
.LBB6_309:
	callq	_ZdaPv
.LBB6_310:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_312
# BB#311:
	callq	_ZdaPv
.LBB6_312:
	movq	80(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB6_314
.LBB6_313:
	movq	%rbp, %rdi
	callq	_ZdaPv
.LBB6_314:
	movq	48(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_316
# BB#315:
	callq	_ZdaPv
.LBB6_316:
	movq	(%r15), %rax
.Ltmp171:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp172:
.LBB6_317:                              # %_ZN9CMyComPtrI20IArchiveOpenCallbackED2Ev.exit
	movq	72(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_319
# BB#318:
	movq	(%rdi), %rax
.Ltmp176:
	callq	*16(%rax)
.Ltmp177:
.LBB6_319:
	movq	%r14, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r12d
	je	.LBB6_321
# BB#320:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
	jmp	.LBB6_201
.LBB6_321:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp182:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp183:
# BB#322:
.LBB6_323:
.Ltmp184:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_324:
.Ltmp178:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN8NArchive6NSplit8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end6-_ZN8NArchive6NSplit8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\347\205"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\330\005"              # Call site table length
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp10         #   Call between .Ltmp10 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	3                       #   On action: 2
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	3                       #   On action: 2
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp19-.Ltmp16         #   Call between .Ltmp16 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin1   #     jumps to .Ltmp20
	.byte	3                       #   On action: 2
	.long	.Ltmp21-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp24-.Ltmp21         #   Call between .Ltmp21 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin1   #     jumps to .Ltmp25
	.byte	3                       #   On action: 2
	.long	.Ltmp28-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp29-.Ltmp28         #   Call between .Ltmp28 and .Ltmp29
	.long	.Ltmp30-.Lfunc_begin1   #     jumps to .Ltmp30
	.byte	3                       #   On action: 2
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	3                       #   On action: 2
	.long	.Ltmp34-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin1   #     jumps to .Ltmp36
	.byte	3                       #   On action: 2
	.long	.Ltmp39-.Lfunc_begin1   # >> Call Site 8 <<
	.long	.Ltmp40-.Ltmp39         #   Call between .Ltmp39 and .Ltmp40
	.long	.Ltmp41-.Lfunc_begin1   #     jumps to .Ltmp41
	.byte	3                       #   On action: 2
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 9 <<
	.long	.Ltmp43-.Ltmp42         #   Call between .Ltmp42 and .Ltmp43
	.long	.Ltmp44-.Lfunc_begin1   #     jumps to .Ltmp44
	.byte	3                       #   On action: 2
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 10 <<
	.long	.Ltmp46-.Ltmp45         #   Call between .Ltmp45 and .Ltmp46
	.long	.Ltmp47-.Lfunc_begin1   #     jumps to .Ltmp47
	.byte	3                       #   On action: 2
	.long	.Ltmp37-.Lfunc_begin1   # >> Call Site 11 <<
	.long	.Ltmp38-.Ltmp37         #   Call between .Ltmp37 and .Ltmp38
	.long	.Ltmp53-.Lfunc_begin1   #     jumps to .Ltmp53
	.byte	3                       #   On action: 2
	.long	.Ltmp48-.Lfunc_begin1   # >> Call Site 12 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin1   #     jumps to .Ltmp50
	.byte	3                       #   On action: 2
	.long	.Ltmp51-.Lfunc_begin1   # >> Call Site 13 <<
	.long	.Ltmp52-.Ltmp51         #   Call between .Ltmp51 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin1   #     jumps to .Ltmp53
	.byte	3                       #   On action: 2
	.long	.Ltmp54-.Lfunc_begin1   # >> Call Site 14 <<
	.long	.Ltmp57-.Ltmp54         #   Call between .Ltmp54 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin1   #     jumps to .Ltmp58
	.byte	3                       #   On action: 2
	.long	.Ltmp59-.Lfunc_begin1   # >> Call Site 15 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin1   #     jumps to .Ltmp61
	.byte	3                       #   On action: 2
	.long	.Ltmp62-.Lfunc_begin1   # >> Call Site 16 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin1   #     jumps to .Ltmp64
	.byte	3                       #   On action: 2
	.long	.Ltmp65-.Lfunc_begin1   # >> Call Site 17 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin1   #     jumps to .Ltmp67
	.byte	3                       #   On action: 2
	.long	.Ltmp68-.Lfunc_begin1   # >> Call Site 18 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin1   #     jumps to .Ltmp70
	.byte	3                       #   On action: 2
	.long	.Ltmp71-.Lfunc_begin1   # >> Call Site 19 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin1   #     jumps to .Ltmp73
	.byte	3                       #   On action: 2
	.long	.Ltmp74-.Lfunc_begin1   # >> Call Site 20 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp88-.Lfunc_begin1   #     jumps to .Ltmp88
	.byte	3                       #   On action: 2
	.long	.Ltmp76-.Lfunc_begin1   # >> Call Site 21 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp83-.Lfunc_begin1   #     jumps to .Ltmp83
	.byte	3                       #   On action: 2
	.long	.Ltmp78-.Lfunc_begin1   # >> Call Site 22 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin1   #     jumps to .Ltmp80
	.byte	3                       #   On action: 2
	.long	.Ltmp81-.Lfunc_begin1   # >> Call Site 23 <<
	.long	.Ltmp82-.Ltmp81         #   Call between .Ltmp81 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin1   #     jumps to .Ltmp83
	.byte	3                       #   On action: 2
	.long	.Ltmp86-.Lfunc_begin1   # >> Call Site 24 <<
	.long	.Ltmp87-.Ltmp86         #   Call between .Ltmp86 and .Ltmp87
	.long	.Ltmp88-.Lfunc_begin1   #     jumps to .Ltmp88
	.byte	3                       #   On action: 2
	.long	.Ltmp89-.Lfunc_begin1   # >> Call Site 25 <<
	.long	.Ltmp90-.Ltmp89         #   Call between .Ltmp89 and .Ltmp90
	.long	.Ltmp91-.Lfunc_begin1   #     jumps to .Ltmp91
	.byte	3                       #   On action: 2
	.long	.Ltmp92-.Lfunc_begin1   # >> Call Site 26 <<
	.long	.Ltmp93-.Ltmp92         #   Call between .Ltmp92 and .Ltmp93
	.long	.Ltmp94-.Lfunc_begin1   #     jumps to .Ltmp94
	.byte	3                       #   On action: 2
	.long	.Ltmp95-.Lfunc_begin1   # >> Call Site 27 <<
	.long	.Ltmp96-.Ltmp95         #   Call between .Ltmp95 and .Ltmp96
	.long	.Ltmp97-.Lfunc_begin1   #     jumps to .Ltmp97
	.byte	3                       #   On action: 2
	.long	.Ltmp96-.Lfunc_begin1   # >> Call Site 28 <<
	.long	.Ltmp98-.Ltmp96         #   Call between .Ltmp96 and .Ltmp98
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp98-.Lfunc_begin1   # >> Call Site 29 <<
	.long	.Ltmp99-.Ltmp98         #   Call between .Ltmp98 and .Ltmp99
	.long	.Ltmp100-.Lfunc_begin1  #     jumps to .Ltmp100
	.byte	3                       #   On action: 2
	.long	.Ltmp101-.Lfunc_begin1  # >> Call Site 30 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin1  #     jumps to .Ltmp103
	.byte	3                       #   On action: 2
	.long	.Ltmp104-.Lfunc_begin1  # >> Call Site 31 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin1  #     jumps to .Ltmp106
	.byte	3                       #   On action: 2
	.long	.Ltmp107-.Lfunc_begin1  # >> Call Site 32 <<
	.long	.Ltmp108-.Ltmp107       #   Call between .Ltmp107 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin1  #     jumps to .Ltmp109
	.byte	3                       #   On action: 2
	.long	.Ltmp113-.Lfunc_begin1  # >> Call Site 33 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin1  #     jumps to .Ltmp115
	.byte	3                       #   On action: 2
	.long	.Ltmp110-.Lfunc_begin1  # >> Call Site 34 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin1  #     jumps to .Ltmp112
	.byte	3                       #   On action: 2
	.long	.Ltmp116-.Lfunc_begin1  # >> Call Site 35 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin1  #     jumps to .Ltmp118
	.byte	3                       #   On action: 2
	.long	.Ltmp121-.Lfunc_begin1  # >> Call Site 36 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin1  #     jumps to .Ltmp123
	.byte	3                       #   On action: 2
	.long	.Ltmp124-.Lfunc_begin1  # >> Call Site 37 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin1  #     jumps to .Ltmp126
	.byte	3                       #   On action: 2
	.long	.Ltmp127-.Lfunc_begin1  # >> Call Site 38 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp129-.Lfunc_begin1  #     jumps to .Ltmp129
	.byte	3                       #   On action: 2
	.long	.Ltmp173-.Lfunc_begin1  # >> Call Site 39 <<
	.long	.Ltmp174-.Ltmp173       #   Call between .Ltmp173 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin1  #     jumps to .Ltmp175
	.byte	3                       #   On action: 2
	.long	.Ltmp179-.Lfunc_begin1  # >> Call Site 40 <<
	.long	.Ltmp180-.Ltmp179       #   Call between .Ltmp179 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin1  #     jumps to .Ltmp181
	.byte	3                       #   On action: 2
	.long	.Ltmp130-.Lfunc_begin1  # >> Call Site 41 <<
	.long	.Ltmp131-.Ltmp130       #   Call between .Ltmp130 and .Ltmp131
	.long	.Ltmp132-.Lfunc_begin1  #     jumps to .Ltmp132
	.byte	3                       #   On action: 2
	.long	.Ltmp133-.Lfunc_begin1  # >> Call Site 42 <<
	.long	.Ltmp134-.Ltmp133       #   Call between .Ltmp133 and .Ltmp134
	.long	.Ltmp135-.Lfunc_begin1  #     jumps to .Ltmp135
	.byte	3                       #   On action: 2
	.long	.Ltmp136-.Lfunc_begin1  # >> Call Site 43 <<
	.long	.Ltmp137-.Ltmp136       #   Call between .Ltmp136 and .Ltmp137
	.long	.Ltmp138-.Lfunc_begin1  #     jumps to .Ltmp138
	.byte	3                       #   On action: 2
	.long	.Ltmp141-.Lfunc_begin1  # >> Call Site 44 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin1  #     jumps to .Ltmp143
	.byte	3                       #   On action: 2
	.long	.Ltmp144-.Lfunc_begin1  # >> Call Site 45 <<
	.long	.Ltmp147-.Ltmp144       #   Call between .Ltmp144 and .Ltmp147
	.long	.Ltmp153-.Lfunc_begin1  #     jumps to .Ltmp153
	.byte	3                       #   On action: 2
	.long	.Ltmp148-.Lfunc_begin1  # >> Call Site 46 <<
	.long	.Ltmp149-.Ltmp148       #   Call between .Ltmp148 and .Ltmp149
	.long	.Ltmp150-.Lfunc_begin1  #     jumps to .Ltmp150
	.byte	3                       #   On action: 2
	.long	.Ltmp151-.Lfunc_begin1  # >> Call Site 47 <<
	.long	.Ltmp152-.Ltmp151       #   Call between .Ltmp151 and .Ltmp152
	.long	.Ltmp153-.Lfunc_begin1  #     jumps to .Ltmp153
	.byte	3                       #   On action: 2
	.long	.Ltmp154-.Lfunc_begin1  # >> Call Site 48 <<
	.long	.Ltmp155-.Ltmp154       #   Call between .Ltmp154 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin1  #     jumps to .Ltmp156
	.byte	3                       #   On action: 2
	.long	.Ltmp157-.Lfunc_begin1  # >> Call Site 49 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin1  #     jumps to .Ltmp159
	.byte	3                       #   On action: 2
	.long	.Ltmp160-.Lfunc_begin1  # >> Call Site 50 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin1  #     jumps to .Ltmp162
	.byte	3                       #   On action: 2
	.long	.Ltmp163-.Lfunc_begin1  # >> Call Site 51 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin1  #     jumps to .Ltmp165
	.byte	3                       #   On action: 2
	.long	.Ltmp168-.Lfunc_begin1  # >> Call Site 52 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin1  #     jumps to .Ltmp170
	.byte	3                       #   On action: 2
	.long	.Ltmp119-.Lfunc_begin1  # >> Call Site 53 <<
	.long	.Ltmp177-.Ltmp119       #   Call between .Ltmp119 and .Ltmp177
	.long	.Ltmp178-.Lfunc_begin1  #     jumps to .Ltmp178
	.byte	1                       #   On action: 1
	.long	.Ltmp177-.Lfunc_begin1  # >> Call Site 54 <<
	.long	.Ltmp182-.Ltmp177       #   Call between .Ltmp177 and .Ltmp182
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin1  # >> Call Site 55 <<
	.long	.Ltmp183-.Ltmp182       #   Call between .Ltmp182 and .Ltmp183
	.long	.Ltmp184-.Lfunc_begin1  #     jumps to .Ltmp184
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin1  # >> Call Site 56 <<
	.long	.Lfunc_end6-.Ltmp183    #   Call between .Ltmp183 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_S4_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_S4_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_S4_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_S4_,@function
_ZplIwE11CStringBaseIT_ERKS2_S4_:       # @_ZplIwE11CStringBaseIT_ERKS2_S4_
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 48
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r13, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r15), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB7_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r12d, 12(%rbx)
	jmp	.LBB7_3
.LBB7_1:
	xorl	%eax, %eax
.LBB7_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB7_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r13d, 8(%rbx)
	movl	8(%r14), %esi
.Ltmp185:
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp186:
# BB#6:                                 # %.noexc
	movslq	8(%rbx), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%rbx), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB7_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB7_7
# BB#8:
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB7_9:
.Ltmp187:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB7_11
# BB#10:
	callq	_ZdaPv
.LBB7_11:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZplIwE11CStringBaseIT_ERKS2_S4_, .Lfunc_end7-_ZplIwE11CStringBaseIT_ERKS2_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp185-.Lfunc_begin2  #   Call between .Lfunc_begin2 and .Ltmp185
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp185-.Lfunc_begin2  # >> Call Site 2 <<
	.long	.Ltmp186-.Ltmp185       #   Call between .Ltmp185 and .Ltmp186
	.long	.Ltmp187-.Lfunc_begin2  #     jumps to .Ltmp187
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin2  # >> Call Site 3 <<
	.long	.Lfunc_end7-.Ltmp186    #   Call between .Ltmp186 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NArchive6NSplit8CSeqName11GetNextNameEv,"axG",@progbits,_ZN8NArchive6NSplit8CSeqName11GetNextNameEv,comdat
	.weak	_ZN8NArchive6NSplit8CSeqName11GetNextNameEv
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CSeqName11GetNextNameEv,@function
_ZN8NArchive6NSplit8CSeqName11GetNextNameEv: # @_ZN8NArchive6NSplit8CSeqName11GetNextNameEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 144
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbp
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %r14
	movq	$0, 24(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, 16(%rsp)
	movl	$0, (%rbx)
	movl	$4, 28(%rsp)
	leaq	16(%rbp), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpb	$0, 32(%rbp)
	movslq	24(%rbp), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r14, 80(%rsp)          # 8-byte Spill
	je	.LBB8_39
# BB#1:                                 # %.thread.preheader
	testl	%eax, %eax
	jle	.LBB8_88
# BB#2:                                 # %.lr.ph617
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$4, 52(%rsp)            # 4-byte Folded Spill
	movl	$4, 56(%rsp)            # 4-byte Folded Spill
	movq	%rax, %r14
	movl	$4, %r13d
	.p2align	4, 0x90
.LBB8_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_15 Depth 2
                                        #     Child Loop BB8_36 Depth 2
                                        #     Child Loop BB8_23 Depth 2
                                        #     Child Loop BB8_31 Depth 2
	leaq	-1(%r14), %r15
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	-4(%rax,%r14,4), %ebp
	cmpl	$90, %ebp
	movq	%r15, 40(%rsp)          # 8-byte Spill
	je	.LBB8_9
# BB#4:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpl	$122, %ebp
	jne	.LBB8_83
# BB#5:                                 #   in Loop: Header=BB8_3 Depth=1
.Ltmp197:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp198:
# BB#6:                                 # %.noexc
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	$97, (%r14)
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB8_13
# BB#7:                                 #   in Loop: Header=BB8_3 Depth=1
	cmpl	$3, %eax
	movl	$4, %ecx
	cmovlel	%ecx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp200:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp201:
# BB#8:                                 # %.lr.ph.i.i261
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	$97, (%rbp)
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbp)
	movq	%rbp, %r14
	jmp	.LBB8_14
	.p2align	4, 0x90
.LBB8_9:                                #   in Loop: Header=BB8_3 Depth=1
.Ltmp188:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp189:
# BB#10:                                # %.noexc100
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	$65, (%r14)
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB8_21
# BB#11:                                #   in Loop: Header=BB8_3 Depth=1
	cmpl	$3, %esi
	movl	$4, %eax
	cmovgl	%esi, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp191:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp192:
# BB#12:                                # %.lr.ph.i.i276
                                        #   in Loop: Header=BB8_3 Depth=1
	movl	$65, (%rbp)
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbp)
	movq	%rbp, %r14
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB8_22
.LBB8_13:                               #   in Loop: Header=BB8_3 Depth=1
	movq	%r14, %rbp
.LBB8_14:                               # %.noexc.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	16(%rsp), %rcx
	leaq	4(%rbp), %rsi
	.p2align	4, 0x90
.LBB8_15:                               #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB8_15
# BB#16:                                #   in Loop: Header=BB8_3 Depth=1
	movslq	24(%rsp), %rdx
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	leaq	2(%rdx), %r15
	cmpl	56(%rsp), %r15d         # 4-byte Folded Reload
	je	.LBB8_35
# BB#17:                                #   in Loop: Header=BB8_3 Depth=1
	movq	%rdx, %r13
	movq	%r15, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp203:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp204:
# BB#18:                                # %.noexc93
                                        #   in Loop: Header=BB8_3 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_33
# BB#19:                                # %.noexc93
                                        #   in Loop: Header=BB8_3 Depth=1
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	jle	.LBB8_34
# BB#20:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB8_34
.LBB8_21:                               #   in Loop: Header=BB8_3 Depth=1
	movq	%r14, %rbp
.LBB8_22:                               # %.noexc.i96
                                        #   in Loop: Header=BB8_3 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB8_23:                               #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rcx), %eax
	movl	%eax, 4(%rbp,%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB8_23
# BB#24:                                #   in Loop: Header=BB8_3 Depth=1
	movl	$0, 24(%rsp)
	movl	$0, (%rbx)
	leal	2(%rsi), %r12d
	cmpl	%r13d, %r12d
	jne	.LBB8_26
# BB#25:                                #   in Loop: Header=BB8_3 Depth=1
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r15d
	jmp	.LBB8_30
	.p2align	4, 0x90
.LBB8_26:                               #   in Loop: Header=BB8_3 Depth=1
	movslq	%r12d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp194:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp195:
# BB#27:                                # %.noexc112
                                        #   in Loop: Header=BB8_3 Depth=1
	testl	%r13d, %r13d
	jle	.LBB8_29
# BB#28:                                # %._crit_edge.thread.i.i107
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB8_29:                               # %._crit_edge16.i.i108
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%r15, 16(%rsp)
	movl	$0, (%r15)
	movl	%r12d, 28(%rsp)
	movq	%r15, %rbx
	movl	%r12d, 52(%rsp)         # 4-byte Spill
	movl	%r12d, %r15d
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB8_30:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i109.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	incl	%esi
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_31:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i109
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_31
# BB#32:                                # %_ZN11CStringBaseIwED2Ev.exit114
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%esi, 24(%rsp)
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB8_38
.LBB8_33:                               #   in Loop: Header=BB8_3 Depth=1
	xorl	%eax, %eax
.LBB8_34:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%r12, 16(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 28(%rsp)
	movq	%r12, %rbx
	movl	%r15d, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	%r13, %rdx
.LBB8_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	incl	%edx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_36:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_36
# BB#37:                                # %_ZN11CStringBaseIwED2Ev.exit94
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 24(%rsp)
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	%r15d, %r12d
.LBB8_38:                               # %.thread.backedge
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	40(%rsp), %r14          # 8-byte Reload
	testq	%r14, %r14
	movl	%r15d, %ecx
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movl	%r12d, %r13d
	jg	.LBB8_3
	jmp	.LBB8_89
.LBB8_39:                               # %.thread520.preheader
	testl	%eax, %eax
	jle	.LBB8_88
# BB#40:                                # %.lr.ph609
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	$4, %r12d
	.p2align	4, 0x90
.LBB8_41:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_48 Depth 2
                                        #     Child Loop BB8_58 Depth 2
                                        #     Child Loop BB8_64 Depth 2
                                        #     Child Loop BB8_70 Depth 2
                                        #     Child Loop BB8_80 Depth 2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	32(%rsp), %r13          # 8-byte Reload
	movl	-4(%rax,%r13,4), %ebp
	cmpl	$57, %ebp
	jne	.LBB8_102
# BB#42:                                #   in Loop: Header=BB8_41 Depth=1
.Ltmp248:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp249:
# BB#43:                                # %.noexc168
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	$48, (%r14)
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB8_46
# BB#44:                                #   in Loop: Header=BB8_41 Depth=1
	cmpl	$3, %eax
	movl	$4, %ecx
	cmovlel	%ecx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp251:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp252:
# BB#45:                                # %.lr.ph.i.i321
                                        #   in Loop: Header=BB8_41 Depth=1
	movl	$48, (%rbp)
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbp)
	movq	%rbp, %r14
	jmp	.LBB8_47
	.p2align	4, 0x90
.LBB8_46:                               #   in Loop: Header=BB8_41 Depth=1
	movq	%r14, %rbp
.LBB8_47:                               # %.noexc.i164
                                        #   in Loop: Header=BB8_41 Depth=1
	decq	%r13
	movq	16(%rsp), %rcx
	leaq	4(%rbp), %rsi
	.p2align	4, 0x90
.LBB8_48:                               #   Parent Loop BB8_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB8_48
# BB#49:                                #   in Loop: Header=BB8_41 Depth=1
	movslq	24(%rsp), %rdx
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	leaq	2(%rdx), %rax
	cmpl	%r12d, %eax
	movq	%r13, 32(%rsp)          # 8-byte Spill
	jne	.LBB8_51
# BB#50:                                #   in Loop: Header=BB8_41 Depth=1
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	jmp	.LBB8_57
	.p2align	4, 0x90
.LBB8_51:                               #   in Loop: Header=BB8_41 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rax, %r13
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp254:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp255:
# BB#52:                                # %.noexc180
                                        #   in Loop: Header=BB8_41 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_55
# BB#53:                                # %.noexc180
                                        #   in Loop: Header=BB8_41 Depth=1
	testl	%r12d, %r12d
	movl	$0, %eax
	jle	.LBB8_56
# BB#54:                                # %._crit_edge.thread.i.i175
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB8_56
.LBB8_55:                               #   in Loop: Header=BB8_41 Depth=1
	xorl	%eax, %eax
.LBB8_56:                               # %._crit_edge16.i.i176
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	%r15, 16(%rsp)
	movl	$0, (%r15,%rax,4)
	movq	%r13, %r12
	movl	%r12d, 28(%rsp)
	movq	%r15, %rbx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
.LBB8_57:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i177.preheader
                                        #   in Loop: Header=BB8_41 Depth=1
	incl	%edx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_58:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i177
                                        #   Parent Loop BB8_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_58
# BB#59:                                # %_ZN11CStringBaseIwED2Ev.exit182
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 24(%rsp)
	movq	%r14, %rdi
	callq	_ZdaPv
	testl	%r13d, %r13d
	je	.LBB8_61
# BB#60:                                #   in Loop: Header=BB8_41 Depth=1
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	jmp	.LBB8_82
	.p2align	4, 0x90
.LBB8_61:                               #   in Loop: Header=BB8_41 Depth=1
.Ltmp257:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp258:
# BB#62:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	$49, (%r14)
.Ltmp260:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp261:
# BB#63:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB8_41 Depth=1
	movl	$49, (%r15)
	movl	$4, %eax
	.p2align	4, 0x90
.LBB8_64:                               # %._crit_edge
                                        #   Parent Loop BB8_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_64
# BB#65:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
                                        #   in Loop: Header=BB8_41 Depth=1
	movl	24(%rsp), %eax
	testl	%eax, %eax
	jle	.LBB8_68
# BB#66:                                #   in Loop: Header=BB8_41 Depth=1
	cmpl	$3, %eax
	movl	$4, %ecx
	cmovlel	%ecx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp263:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp264:
# BB#67:                                # %.lr.ph.i.i336
                                        #   in Loop: Header=BB8_41 Depth=1
	movl	(%r15), %eax
	movl	%eax, (%rbp)
	movq	%r15, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbp)
	movq	%rbp, %r15
	jmp	.LBB8_69
.LBB8_68:                               #   in Loop: Header=BB8_41 Depth=1
	movq	%r15, %rbp
.LBB8_69:                               # %.noexc.i187
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	16(%rsp), %rcx
	leaq	4(%rbp), %rsi
	.p2align	4, 0x90
.LBB8_70:                               #   Parent Loop BB8_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB8_70
# BB#71:                                #   in Loop: Header=BB8_41 Depth=1
	movslq	24(%rsp), %rdx
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	leaq	2(%rdx), %rax
	cmpl	%r12d, %eax
	jne	.LBB8_73
# BB#72:                                #   in Loop: Header=BB8_41 Depth=1
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	jmp	.LBB8_79
.LBB8_73:                               #   in Loop: Header=BB8_41 Depth=1
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp266:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp267:
# BB#74:                                # %.noexc200
                                        #   in Loop: Header=BB8_41 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_77
# BB#75:                                # %.noexc200
                                        #   in Loop: Header=BB8_41 Depth=1
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	jle	.LBB8_78
# BB#76:                                # %._crit_edge.thread.i.i195
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB8_78
.LBB8_77:                               #   in Loop: Header=BB8_41 Depth=1
	xorl	%eax, %eax
.LBB8_78:                               # %._crit_edge16.i.i196
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	%r12, 16(%rsp)
	movl	$0, (%r12,%rax,4)
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, 28(%rsp)
	movq	%r12, %rbx
	movq	%rax, %r12
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB8_79:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i197.preheader
                                        #   in Loop: Header=BB8_41 Depth=1
	incl	%edx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_80:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i197
                                        #   Parent Loop BB8_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_80
# BB#81:                                # %_ZN11CStringBaseIwED2Ev.exit203
                                        #   in Loop: Header=BB8_41 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 24(%rsp)
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r14, %rdi
	callq	_ZdaPv
.LBB8_82:                               # %.thread520.backedge
                                        #   in Loop: Header=BB8_41 Depth=1
	testq	%r13, %r13
	jg	.LBB8_41
	jmp	.LBB8_89
.LBB8_83:
	incl	%ebp
	testl	%r15d, %r15d
	movq	32(%rsp), %r15          # 8-byte Reload
	jne	.LBB8_106
# BB#84:
	movl	%ebp, %eax
	orl	$32, %eax
	cmpl	$122, %eax
	jne	.LBB8_106
# BB#85:
.Ltmp224:
	movl	$1, %esi
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp225:
# BB#86:
	movslq	8(%rbx), %rax
	leaq	1(%rax), %rcx
	cmpl	$122, %ebp
	movq	(%rbx), %rdx
	movl	%ebp, (%rdx,%rax,4)
	movl	%ecx, 8(%rbx)
	movl	$0, 4(%rdx,%rax,4)
	movl	$97, %eax
	movl	$65, %edi
	cmovel	%eax, %edi
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	testl	%r15d, %r15d
	js	.LBB8_88
# BB#87:                                # %.lr.ph611.preheader
	movq	$-8, %r15
	movl	28(%rsp), %ebp
	xorl	%r12d, %r12d
	movq	%rbx, %r13
	movl	%edi, 40(%rsp)          # 4-byte Spill
	jmp	.LBB8_180
.LBB8_88:
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB8_89:                               # %.loopexit
	leaq	16(%rsp), %rax
	cmpq	64(%rsp), %rax          # 8-byte Folded Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	je	.LBB8_98
# BB#90:
	movl	$0, 24(%rsi)
	movq	16(%rsi), %rbp
	movl	$0, (%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
	incl	%eax
	movl	28(%rsi), %r15d
	cmpl	%r15d, %eax
	je	.LBB8_96
# BB#91:
	movq	%rax, %r13
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp269:
	callq	_Znam
	movq	%rax, %r14
.Ltmp270:
# BB#92:                                # %.noexc255
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB8_95
# BB#93:                                # %.noexc255
	testl	%r15d, %r15d
	jle	.LBB8_95
# BB#94:                                # %._crit_edge.thread.i.i250
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	72(%rsp), %rax          # 8-byte Reload
	movslq	24(%rax), %rax
	movq	16(%rsp), %rbx
.LBB8_95:                               # %._crit_edge16.i.i251
	movq	72(%rsp), %rcx          # 8-byte Reload
	movq	%r14, 16(%rcx)
	movl	$0, (%r14,%rax,4)
	movl	%r13d, 28(%rcx)
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB8_96:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i252
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rbp)
	addq	$4, %rbp
	testl	%eax, %eax
	jne	.LBB8_96
# BB#97:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	24(%rsp), %eax
	movq	72(%rsp), %rsi          # 8-byte Reload
	movl	%eax, 24(%rsi)
.LBB8_98:                               # %_ZN11CStringBaseIwEaSERKS0_.exit256
.Ltmp271:
	movq	%r12, %rdi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
.Ltmp272:
# BB#99:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_101
# BB#100:
	callq	_ZdaPv
.LBB8_101:                              # %_ZN11CStringBaseIwED2Ev.exit92
	movq	%r12, %rax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_102:
.Ltmp230:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp231:
# BB#103:                               # %.noexc211
	movq	%r12, 40(%rsp)          # 8-byte Spill
	incl	%ebp
	movl	%ebp, (%r14)
	movl	$0, 4(%r14)
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB8_110
# BB#104:
	cmpl	$3, %esi
	movl	$4, %eax
	cmovgl	%esi, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp233:
	callq	_Znam
	movq	%rax, %r12
.Ltmp234:
# BB#105:                               # %.lr.ph.i.i351
	movl	%ebp, (%r12)
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, 4(%r12)
	movq	%r12, %r14
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB8_111
.LBB8_106:
.Ltmp206:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp207:
# BB#107:                               # %.noexc128
	movl	%ebp, (%r15)
	movl	$0, 4(%r15)
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB8_141
# BB#108:
	cmpl	$3, %esi
	movl	$4, %eax
	cmovgl	%esi, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp209:
	callq	_Znam
	movq	%rax, %r12
.Ltmp210:
# BB#109:                               # %.lr.ph.i.i291
	movl	%ebp, (%r12)
	movq	%r15, %rdi
	callq	_ZdaPv
	movl	$0, 4(%r12)
	movq	%r12, %r15
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB8_142
.LBB8_110:
	movq	%r14, %r12
.LBB8_111:                              # %.noexc.i207
	leaq	4(%r12), %rcx
	.p2align	4, 0x90
.LBB8_112:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB8_112
# BB#113:
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	leal	2(%rsi), %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	%eax, %ecx
	jne	.LBB8_115
# BB#114:
	movl	%eax, %ebp
	jmp	.LBB8_120
.LBB8_115:
	movl	%ecx, %ebp
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp236:
	callq	_Znam
	movq	%rax, %r15
.Ltmp237:
# BB#116:                               # %.noexc223
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_119
# BB#117:                               # %.noexc223
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	jle	.LBB8_119
# BB#118:                               # %._crit_edge.thread.i.i218
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
.LBB8_119:                              # %._crit_edge16.i.i219
	movq	%r15, 16(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 28(%rsp)
	movq	%r15, %rbx
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB8_120:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i220.preheader
	incl	%esi
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_121:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i220
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %ecx
	addq	$4, %r12
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_121
# BB#122:                               # %_ZN11CStringBaseIwED2Ev.exit225
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%esi, 24(%rsp)
	movq	%r14, %rdi
	callq	_ZdaPv
	addl	$-2, %r13d
	js	.LBB8_89
# BB#123:                               # %.lr.ph.preheader
	movslq	%r13d, %r13
	movl	%ebp, %r12d
	.p2align	4, 0x90
.LBB8_124:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_130 Depth 2
                                        #     Child Loop BB8_139 Depth 2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%r13,4), %ebx
.Ltmp239:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp240:
# BB#125:                               # %.noexc230
                                        #   in Loop: Header=BB8_124 Depth=1
	movl	%ebx, (%r14)
	movl	$0, 4(%r14)
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB8_128
# BB#126:                               #   in Loop: Header=BB8_124 Depth=1
	cmpl	$3, %eax
	movl	$4, %ecx
	cmovlel	%ecx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp242:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp243:
# BB#127:                               # %.lr.ph.i.i366
                                        #   in Loop: Header=BB8_124 Depth=1
	movl	%ebx, (%rbp)
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbp)
	movq	%rbp, %r14
	jmp	.LBB8_129
	.p2align	4, 0x90
.LBB8_128:                              #   in Loop: Header=BB8_124 Depth=1
	movq	%r14, %rbp
.LBB8_129:                              # %.noexc.i226
                                        #   in Loop: Header=BB8_124 Depth=1
	movq	16(%rsp), %rcx
	leaq	4(%rbp), %rsi
	.p2align	4, 0x90
.LBB8_130:                              #   Parent Loop BB8_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB8_130
# BB#131:                               #   in Loop: Header=BB8_124 Depth=1
	movslq	24(%rsp), %rdx
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	leaq	2(%rdx), %r15
	cmpl	%r12d, %r15d
	je	.LBB8_138
# BB#132:                               #   in Loop: Header=BB8_124 Depth=1
	movq	%r15, %rax
	movl	%r12d, %r15d
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp245:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp246:
# BB#133:                               # %.noexc242
                                        #   in Loop: Header=BB8_124 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_136
# BB#134:                               # %.noexc242
                                        #   in Loop: Header=BB8_124 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB8_137
# BB#135:                               # %._crit_edge.thread.i.i237
                                        #   in Loop: Header=BB8_124 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB8_137
.LBB8_136:                              #   in Loop: Header=BB8_124 Depth=1
	xorl	%eax, %eax
.LBB8_137:                              # %._crit_edge16.i.i238
                                        #   in Loop: Header=BB8_124 Depth=1
	movq	%r12, 16(%rsp)
	movl	$0, (%r12,%rax,4)
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	%r15d, 28(%rsp)
	movq	%r12, %rbx
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB8_138:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i239.preheader
                                        #   in Loop: Header=BB8_124 Depth=1
	incl	%edx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_139:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i239
                                        #   Parent Loop BB8_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_139
# BB#140:                               # %_ZN11CStringBaseIwED2Ev.exit244
                                        #   in Loop: Header=BB8_124 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 24(%rsp)
	movq	%r14, %rdi
	callq	_ZdaPv
	testq	%r13, %r13
	leaq	-1(%r13), %r13
	movl	%r15d, %r12d
	jg	.LBB8_124
	jmp	.LBB8_89
.LBB8_141:
	movq	%r15, %r12
.LBB8_142:                              # %.noexc.i124
	leaq	4(%r12), %rcx
	movl	52(%rsp), %ebp          # 4-byte Reload
	.p2align	4, 0x90
.LBB8_143:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB8_143
# BB#144:
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	leal	2(%rsi), %r13d
	cmpl	%ebp, %r13d
	jne	.LBB8_146
# BB#145:
	movl	%ebp, %r13d
	jmp	.LBB8_151
.LBB8_146:
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movslq	%r13d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp212:
	callq	_Znam
	movq	%rax, %r15
.Ltmp213:
# BB#147:                               # %.noexc140
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB8_150
# BB#148:                               # %.noexc140
	testl	%ebp, %ebp
	jle	.LBB8_150
# BB#149:                               # %._crit_edge.thread.i.i135
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
.LBB8_150:                              # %._crit_edge16.i.i136
	movq	%r15, 16(%rsp)
	movl	$0, (%r15,%rax,4)
	movl	%r13d, 28(%rsp)
	movq	%r15, %rbx
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB8_151:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i137.preheader
	incl	%esi
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_152:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i137
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %ecx
	addq	$4, %r12
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_152
# BB#153:                               # %_ZN11CStringBaseIwED2Ev.exit142
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%esi, 24(%rsp)
	movq	%r15, %rdi
	callq	_ZdaPv
	addl	$-2, %r14d
	js	.LBB8_89
# BB#154:                               # %.lr.ph613.preheader
	movslq	%r14d, %r12
	.p2align	4, 0x90
.LBB8_155:                              # %.lr.ph613
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_161 Depth 2
                                        #     Child Loop BB8_170 Depth 2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movl	(%rax,%r12,4), %ebx
.Ltmp215:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp216:
# BB#156:                               # %.noexc147
                                        #   in Loop: Header=BB8_155 Depth=1
	movl	%ebx, (%r14)
	movl	$0, 4(%r14)
	movq	8(%rsp), %rax           # 8-byte Reload
	testl	%eax, %eax
	jle	.LBB8_159
# BB#157:                               #   in Loop: Header=BB8_155 Depth=1
	cmpl	$3, %eax
	movl	$4, %ecx
	cmovlel	%ecx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp218:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp219:
# BB#158:                               # %.lr.ph.i.i306
                                        #   in Loop: Header=BB8_155 Depth=1
	movl	%ebx, (%rbp)
	movq	%r14, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbp)
	movq	%rbp, %r14
	jmp	.LBB8_160
	.p2align	4, 0x90
.LBB8_159:                              #   in Loop: Header=BB8_155 Depth=1
	movq	%r14, %rbp
.LBB8_160:                              # %.noexc.i143
                                        #   in Loop: Header=BB8_155 Depth=1
	movq	16(%rsp), %rcx
	leaq	4(%rbp), %rsi
	.p2align	4, 0x90
.LBB8_161:                              #   Parent Loop BB8_155 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB8_161
# BB#162:                               #   in Loop: Header=BB8_155 Depth=1
	movslq	24(%rsp), %rdx
	movl	$0, 24(%rsp)
	movq	16(%rsp), %rbx
	movl	$0, (%rbx)
	leaq	2(%rdx), %r15
	cmpl	%r13d, %r15d
	je	.LBB8_169
# BB#163:                               #   in Loop: Header=BB8_155 Depth=1
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%r15, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp221:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp222:
# BB#164:                               # %.noexc159
                                        #   in Loop: Header=BB8_155 Depth=1
	testq	%rbx, %rbx
	je	.LBB8_167
# BB#165:                               # %.noexc159
                                        #   in Loop: Header=BB8_155 Depth=1
	testl	%r13d, %r13d
	movl	$0, %eax
	jle	.LBB8_168
# BB#166:                               # %._crit_edge.thread.i.i154
                                        #   in Loop: Header=BB8_155 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%rsp), %rax
	jmp	.LBB8_168
.LBB8_167:                              #   in Loop: Header=BB8_155 Depth=1
	xorl	%eax, %eax
.LBB8_168:                              # %._crit_edge16.i.i155
                                        #   in Loop: Header=BB8_155 Depth=1
	movq	%r12, 16(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 28(%rsp)
	movq	%r12, %rbx
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB8_169:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i156.preheader
                                        #   in Loop: Header=BB8_155 Depth=1
	incl	%edx
	movq	%rbx, %rax
	.p2align	4, 0x90
.LBB8_170:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i156
                                        #   Parent Loop BB8_155 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp), %ecx
	addq	$4, %rbp
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB8_170
# BB#171:                               # %_ZN11CStringBaseIwED2Ev.exit161
                                        #   in Loop: Header=BB8_155 Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	%edx, 24(%rsp)
	movq	%r14, %rdi
	callq	_ZdaPv
	testq	%r12, %r12
	leaq	-1(%r12), %r12
	movl	%r15d, %r13d
	jg	.LBB8_155
	jmp	.LBB8_89
.LBB8_172:                              # %vector.body.preheader
                                        #   in Loop: Header=BB8_180 Depth=1
	leaq	-8(%r12), %rcx
	movq	%rcx, %rdx
	shrq	$3, %rdx
	incq	%rdx
	testb	$3, %dl
	je	.LBB8_175
# BB#173:                               # %vector.body.prol.preheader
                                        #   in Loop: Header=BB8_180 Depth=1
	movl	%r15d, %edx
	shrl	$3, %edx
	incl	%edx
	andl	$3, %edx
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB8_174:                              # %vector.body.prol
                                        #   Parent Loop BB8_180 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	(%r13,%rsi,4), %xmm0
	movups	16(%r13,%rsi,4), %xmm1
	movups	%xmm0, (%rbx,%rsi,4)
	movups	%xmm1, 16(%rbx,%rsi,4)
	addq	$8, %rsi
	incq	%rdx
	jne	.LBB8_174
	jmp	.LBB8_176
.LBB8_175:                              #   in Loop: Header=BB8_180 Depth=1
	xorl	%esi, %esi
.LBB8_176:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB8_180 Depth=1
	cmpq	$24, %rcx
	jb	.LBB8_179
# BB#177:                               # %vector.body.preheader.new
                                        #   in Loop: Header=BB8_180 Depth=1
	movq	%r12, %rcx
	andq	$-8, %rcx
	subq	%rsi, %rcx
	leaq	112(%rbx,%rsi,4), %rdx
	leaq	112(%r13,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB8_178:                              # %vector.body
                                        #   Parent Loop BB8_180 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-112(%rsi), %xmm0
	movups	-96(%rsi), %xmm1
	movups	%xmm0, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	-80(%rsi), %xmm0
	movups	-64(%rsi), %xmm1
	movups	%xmm0, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rsi
	addq	$-32, %rcx
	jne	.LBB8_178
.LBB8_179:                              # %middle.block
                                        #   in Loop: Header=BB8_180 Depth=1
	cmpq	%rax, %r12
	jne	.LBB8_194
	jmp	.LBB8_200
	.p2align	4, 0x90
.LBB8_180:                              # %.lr.ph611
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_174 Depth 2
                                        #     Child Loop BB8_178 Depth 2
                                        #     Child Loop BB8_196 Depth 2
                                        #     Child Loop BB8_198 Depth 2
	movl	%ebp, %eax
	subl	%r12d, %eax
	cmpl	$1, %eax
	jle	.LBB8_182
# BB#181:                               #   in Loop: Header=BB8_180 Depth=1
	movl	%ebp, %r14d
	jmp	.LBB8_202
	.p2align	4, 0x90
.LBB8_182:                              #   in Loop: Header=BB8_180 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %ebp
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %ebp
	jl	.LBB8_184
# BB#183:                               # %select.true.sink
                                        #   in Loop: Header=BB8_180 Depth=1
	movl	%ebp, %edx
	shrl	$31, %edx
	addl	%ebp, %edx
	sarl	%edx
.LBB8_184:                              # %select.end
                                        #   in Loop: Header=BB8_180 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%rbp,%rsi), %r14d
	cmpl	%ebp, %r14d
	jne	.LBB8_186
# BB#185:                               #   in Loop: Header=BB8_180 Depth=1
	movl	%ebp, %r14d
	jmp	.LBB8_202
	.p2align	4, 0x90
.LBB8_186:                              #   in Loop: Header=BB8_180 Depth=1
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp227:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp228:
# BB#187:                               # %.noexc123
                                        #   in Loop: Header=BB8_180 Depth=1
	testl	%ebp, %ebp
	movl	40(%rsp), %edi          # 4-byte Reload
	jle	.LBB8_201
# BB#188:                               # %.preheader.i.i
                                        #   in Loop: Header=BB8_180 Depth=1
	testl	%r12d, %r12d
	jle	.LBB8_199
# BB#189:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB8_180 Depth=1
	cmpq	$7, %r12
	jbe	.LBB8_193
# BB#190:                               # %min.iters.checked
                                        #   in Loop: Header=BB8_180 Depth=1
	movq	%r12, %rax
	andq	$-8, %rax
	je	.LBB8_193
# BB#191:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_180 Depth=1
	leaq	(%r13,%r12,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB8_172
# BB#192:                               # %vector.memcheck
                                        #   in Loop: Header=BB8_180 Depth=1
	leaq	(,%r12,4), %rcx
	addq	%rbx, %rcx
	cmpq	%rcx, %r13
	jae	.LBB8_172
.LBB8_193:                              #   in Loop: Header=BB8_180 Depth=1
	xorl	%eax, %eax
.LBB8_194:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB8_180 Depth=1
	leaq	-1(%r12), %rcx
	movl	%r12d, %edx
	subl	%eax, %edx
	subq	%rax, %rcx
	testb	$7, %dl
	je	.LBB8_197
# BB#195:                               # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB8_180 Depth=1
	movl	%r12d, %edx
	subl	%eax, %edx
	andl	$7, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB8_196:                              # %scalar.ph.prol
                                        #   Parent Loop BB8_180 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rax,4), %esi
	movl	%esi, (%rbx,%rax,4)
	incq	%rax
	incq	%rdx
	jne	.LBB8_196
.LBB8_197:                              # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB8_180 Depth=1
	cmpq	$7, %rcx
	jb	.LBB8_200
	.p2align	4, 0x90
.LBB8_198:                              # %scalar.ph
                                        #   Parent Loop BB8_180 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rax,4), %ecx
	movl	%ecx, (%rbx,%rax,4)
	movl	4(%r13,%rax,4), %ecx
	movl	%ecx, 4(%rbx,%rax,4)
	movl	8(%r13,%rax,4), %ecx
	movl	%ecx, 8(%rbx,%rax,4)
	movl	12(%r13,%rax,4), %ecx
	movl	%ecx, 12(%rbx,%rax,4)
	movl	16(%r13,%rax,4), %ecx
	movl	%ecx, 16(%rbx,%rax,4)
	movl	20(%r13,%rax,4), %ecx
	movl	%ecx, 20(%rbx,%rax,4)
	movl	24(%r13,%rax,4), %ecx
	movl	%ecx, 24(%rbx,%rax,4)
	movl	28(%r13,%rax,4), %ecx
	movl	%ecx, 28(%rbx,%rax,4)
	addq	$8, %rax
	cmpq	%rax, %r12
	jne	.LBB8_198
	jmp	.LBB8_200
.LBB8_199:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB8_180 Depth=1
	testq	%r13, %r13
	je	.LBB8_201
.LBB8_200:                              # %._crit_edge.thread.i.i120
                                        #   in Loop: Header=BB8_180 Depth=1
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	40(%rsp), %edi          # 4-byte Reload
.LBB8_201:                              # %._crit_edge16.i.i121
                                        #   in Loop: Header=BB8_180 Depth=1
	movq	%rbx, 16(%rsp)
	movslq	%r12d, %rax
	movl	$0, (%rbx,%rax,4)
	movl	%r14d, 28(%rsp)
	movq	%rbx, %r13
.LBB8_202:                              #   in Loop: Header=BB8_180 Depth=1
	movslq	%r12d, %rax
	movl	%edi, (%r13,%rax,4)
	movq	%r12, %rcx
	incq	%rcx
	movl	%ecx, 24(%rsp)
	movslq	%ecx, %rax
	movl	$0, (%r13,%rax,4)
	incq	%r15
	cmpl	32(%rsp), %r12d         # 4-byte Folded Reload
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %r12
	movl	%r14d, %ebp
	jl	.LBB8_180
	jmp	.LBB8_89
.LBB8_203:                              # %_ZN11CStringBaseIwED2Ev.exit162
.Ltmp214:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	jmp	.LBB8_235
.LBB8_204:                              # %_ZN11CStringBaseIwED2Ev.exit245
.Ltmp238:
	jmp	.LBB8_228
.LBB8_205:                              # %_ZN11CStringBaseIwED2Ev.exit.i127
.Ltmp211:
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB8_235
.LBB8_206:                              # %_ZN11CStringBaseIwED2Ev.exit.i210
.Ltmp235:
	jmp	.LBB8_228
.LBB8_207:
.Ltmp208:
	jmp	.LBB8_234
.LBB8_208:
.Ltmp226:
	jmp	.LBB8_234
.LBB8_209:
.Ltmp232:
	jmp	.LBB8_234
.LBB8_210:
.Ltmp229:
	jmp	.LBB8_234
.LBB8_211:
.Ltmp268:
	jmp	.LBB8_213
.LBB8_212:                              # %_ZN11CStringBaseIwED2Ev.exit.i188
.Ltmp265:
.LBB8_213:                              # %_ZN11CStringBaseIwED2Ev.exit206
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB8_229
.LBB8_214:                              # %_ZN11CStringBaseIwED2Ev.exit163
.Ltmp223:
	jmp	.LBB8_228
.LBB8_215:                              # %_ZN11CStringBaseIwED2Ev.exit246
.Ltmp247:
	jmp	.LBB8_228
.LBB8_216:                              # %_ZN11CStringBaseIwED2Ev.exit.i146
.Ltmp220:
	jmp	.LBB8_228
.LBB8_217:
.Ltmp273:
	jmp	.LBB8_234
.LBB8_218:
.Ltmp262:
	jmp	.LBB8_228
.LBB8_219:
.Ltmp259:
	jmp	.LBB8_234
.LBB8_220:                              # %_ZN11CStringBaseIwED2Ev.exit.i229
.Ltmp244:
	jmp	.LBB8_228
.LBB8_221:                              # %_ZN11CStringBaseIwED2Ev.exit95
.Ltmp205:
	jmp	.LBB8_228
.LBB8_222:                              # %_ZN11CStringBaseIwED2Ev.exit115
.Ltmp196:
	jmp	.LBB8_228
.LBB8_223:                              # %_ZN11CStringBaseIwED2Ev.exit204
.Ltmp256:
	jmp	.LBB8_228
.LBB8_224:                              # %_ZN11CStringBaseIwED2Ev.exit.i99
.Ltmp193:
	jmp	.LBB8_228
.LBB8_225:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp202:
	jmp	.LBB8_228
.LBB8_226:
.Ltmp217:
	jmp	.LBB8_234
.LBB8_227:                              # %_ZN11CStringBaseIwED2Ev.exit.i167
.Ltmp253:
.LBB8_228:                              # %_ZN11CStringBaseIwED2Ev.exit206
	movq	%rax, %rbx
.LBB8_229:                              # %_ZN11CStringBaseIwED2Ev.exit206
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB8_235
.LBB8_230:
.Ltmp241:
	jmp	.LBB8_234
.LBB8_231:
.Ltmp190:
	jmp	.LBB8_234
.LBB8_232:
.Ltmp199:
	jmp	.LBB8_234
.LBB8_233:
.Ltmp250:
.LBB8_234:
	movq	%rax, %rbx
.LBB8_235:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_237
# BB#236:
	callq	_ZdaPv
.LBB8_237:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_ZN8NArchive6NSplit8CSeqName11GetNextNameEv, .Lfunc_end8-_ZN8NArchive6NSplit8CSeqName11GetNextNameEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\211\203\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\206\003"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp197-.Lfunc_begin3  #   Call between .Lfunc_begin3 and .Ltmp197
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin3  # >> Call Site 2 <<
	.long	.Ltmp198-.Ltmp197       #   Call between .Ltmp197 and .Ltmp198
	.long	.Ltmp199-.Lfunc_begin3  #     jumps to .Ltmp199
	.byte	0                       #   On action: cleanup
	.long	.Ltmp200-.Lfunc_begin3  # >> Call Site 3 <<
	.long	.Ltmp201-.Ltmp200       #   Call between .Ltmp200 and .Ltmp201
	.long	.Ltmp202-.Lfunc_begin3  #     jumps to .Ltmp202
	.byte	0                       #   On action: cleanup
	.long	.Ltmp188-.Lfunc_begin3  # >> Call Site 4 <<
	.long	.Ltmp189-.Ltmp188       #   Call between .Ltmp188 and .Ltmp189
	.long	.Ltmp190-.Lfunc_begin3  #     jumps to .Ltmp190
	.byte	0                       #   On action: cleanup
	.long	.Ltmp191-.Lfunc_begin3  # >> Call Site 5 <<
	.long	.Ltmp192-.Ltmp191       #   Call between .Ltmp191 and .Ltmp192
	.long	.Ltmp193-.Lfunc_begin3  #     jumps to .Ltmp193
	.byte	0                       #   On action: cleanup
	.long	.Ltmp203-.Lfunc_begin3  # >> Call Site 6 <<
	.long	.Ltmp204-.Ltmp203       #   Call between .Ltmp203 and .Ltmp204
	.long	.Ltmp205-.Lfunc_begin3  #     jumps to .Ltmp205
	.byte	0                       #   On action: cleanup
	.long	.Ltmp194-.Lfunc_begin3  # >> Call Site 7 <<
	.long	.Ltmp195-.Ltmp194       #   Call between .Ltmp194 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin3  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin3  # >> Call Site 8 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin3  #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin3  # >> Call Site 9 <<
	.long	.Ltmp252-.Ltmp251       #   Call between .Ltmp251 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin3  #     jumps to .Ltmp253
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin3  # >> Call Site 10 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp256-.Lfunc_begin3  #     jumps to .Ltmp256
	.byte	0                       #   On action: cleanup
	.long	.Ltmp257-.Lfunc_begin3  # >> Call Site 11 <<
	.long	.Ltmp258-.Ltmp257       #   Call between .Ltmp257 and .Ltmp258
	.long	.Ltmp259-.Lfunc_begin3  #     jumps to .Ltmp259
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin3  #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp265-.Lfunc_begin3  #     jumps to .Ltmp265
	.byte	0                       #   On action: cleanup
	.long	.Ltmp266-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp267-.Ltmp266       #   Call between .Ltmp266 and .Ltmp267
	.long	.Ltmp268-.Lfunc_begin3  #     jumps to .Ltmp268
	.byte	0                       #   On action: cleanup
	.long	.Ltmp224-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp225-.Ltmp224       #   Call between .Ltmp224 and .Ltmp225
	.long	.Ltmp226-.Lfunc_begin3  #     jumps to .Ltmp226
	.byte	0                       #   On action: cleanup
	.long	.Ltmp269-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp272-.Ltmp269       #   Call between .Ltmp269 and .Ltmp272
	.long	.Ltmp273-.Lfunc_begin3  #     jumps to .Ltmp273
	.byte	0                       #   On action: cleanup
	.long	.Ltmp230-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Ltmp231-.Ltmp230       #   Call between .Ltmp230 and .Ltmp231
	.long	.Ltmp232-.Lfunc_begin3  #     jumps to .Ltmp232
	.byte	0                       #   On action: cleanup
	.long	.Ltmp233-.Lfunc_begin3  # >> Call Site 18 <<
	.long	.Ltmp234-.Ltmp233       #   Call between .Ltmp233 and .Ltmp234
	.long	.Ltmp235-.Lfunc_begin3  #     jumps to .Ltmp235
	.byte	0                       #   On action: cleanup
	.long	.Ltmp206-.Lfunc_begin3  # >> Call Site 19 <<
	.long	.Ltmp207-.Ltmp206       #   Call between .Ltmp206 and .Ltmp207
	.long	.Ltmp208-.Lfunc_begin3  #     jumps to .Ltmp208
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin3  # >> Call Site 20 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin3  #     jumps to .Ltmp211
	.byte	0                       #   On action: cleanup
	.long	.Ltmp236-.Lfunc_begin3  # >> Call Site 21 <<
	.long	.Ltmp237-.Ltmp236       #   Call between .Ltmp236 and .Ltmp237
	.long	.Ltmp238-.Lfunc_begin3  #     jumps to .Ltmp238
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin3  # >> Call Site 22 <<
	.long	.Ltmp240-.Ltmp239       #   Call between .Ltmp239 and .Ltmp240
	.long	.Ltmp241-.Lfunc_begin3  #     jumps to .Ltmp241
	.byte	0                       #   On action: cleanup
	.long	.Ltmp242-.Lfunc_begin3  # >> Call Site 23 <<
	.long	.Ltmp243-.Ltmp242       #   Call between .Ltmp242 and .Ltmp243
	.long	.Ltmp244-.Lfunc_begin3  #     jumps to .Ltmp244
	.byte	0                       #   On action: cleanup
	.long	.Ltmp245-.Lfunc_begin3  # >> Call Site 24 <<
	.long	.Ltmp246-.Ltmp245       #   Call between .Ltmp245 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin3  #     jumps to .Ltmp247
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin3  # >> Call Site 25 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin3  #     jumps to .Ltmp214
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin3  # >> Call Site 26 <<
	.long	.Ltmp216-.Ltmp215       #   Call between .Ltmp215 and .Ltmp216
	.long	.Ltmp217-.Lfunc_begin3  #     jumps to .Ltmp217
	.byte	0                       #   On action: cleanup
	.long	.Ltmp218-.Lfunc_begin3  # >> Call Site 27 <<
	.long	.Ltmp219-.Ltmp218       #   Call between .Ltmp218 and .Ltmp219
	.long	.Ltmp220-.Lfunc_begin3  #     jumps to .Ltmp220
	.byte	0                       #   On action: cleanup
	.long	.Ltmp221-.Lfunc_begin3  # >> Call Site 28 <<
	.long	.Ltmp222-.Ltmp221       #   Call between .Ltmp221 and .Ltmp222
	.long	.Ltmp223-.Lfunc_begin3  #     jumps to .Ltmp223
	.byte	0                       #   On action: cleanup
	.long	.Ltmp227-.Lfunc_begin3  # >> Call Site 29 <<
	.long	.Ltmp228-.Ltmp227       #   Call between .Ltmp227 and .Ltmp228
	.long	.Ltmp229-.Lfunc_begin3  #     jumps to .Ltmp229
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin3  # >> Call Site 30 <<
	.long	.Lfunc_end8-.Ltmp228    #   Call between .Ltmp228 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN8NArchive6NSplit8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler5CloseEv,@function
_ZN8NArchive6NSplit8CHandler5CloseEv:   # @_ZN8NArchive6NSplit8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 16
.Lcfi40:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	72(%rbx), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	addq	$40, %rbx
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	_ZN8NArchive6NSplit8CHandler5CloseEv, .Lfunc_end9-_ZN8NArchive6NSplit8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive6NSplit8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive6NSplit8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive6NSplit8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$0, 52(%rdi)
	setne	%al
	movl	%eax, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_ZN8NArchive6NSplit8CHandler16GetNumberOfItemsEPj, .Lfunc_end10-_ZN8NArchive6NSplit8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive6NSplit8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive6NSplit8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive6NSplit8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -16
	movq	%rcx, %rbx
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movl	$0, (%rsp)
	leal	-7(%rdx), %eax
	cmpl	$2, %eax
	jae	.LBB11_1
# BB#3:
	movq	104(%rdi), %rsi
.Ltmp274:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp275:
	jmp	.LBB11_4
.LBB11_1:
	cmpl	$3, %edx
	jne	.LBB11_4
# BB#2:
	movq	24(%rdi), %rsi
.Ltmp276:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKw
.Ltmp277:
.LBB11_4:
.Ltmp278:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp279:
# BB#5:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB11_6:
.Ltmp280:
	movq	%rax, %rbx
.Ltmp281:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp282:
# BB#7:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB11_8:
.Ltmp283:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN8NArchive6NSplit8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end11-_ZN8NArchive6NSplit8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp274-.Lfunc_begin4  # >> Call Site 1 <<
	.long	.Ltmp279-.Ltmp274       #   Call between .Ltmp274 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin4  #     jumps to .Ltmp280
	.byte	0                       #   On action: cleanup
	.long	.Ltmp279-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp281-.Ltmp279       #   Call between .Ltmp279 and .Ltmp281
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin4  #     jumps to .Ltmp283
	.byte	1                       #   On action: 1
	.long	.Ltmp282-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp282   #   Call between .Ltmp282 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive6NSplit8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive6NSplit8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive6NSplit8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi47:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi48:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi50:
	.cfi_def_cfa_offset 80
.Lcfi51:
	.cfi_offset %rbx, -56
.Lcfi52:
	.cfi_offset %r12, -48
.Lcfi53:
	.cfi_offset %r13, -40
.Lcfi54:
	.cfi_offset %r14, -32
.Lcfi55:
	.cfi_offset %r15, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movl	%ecx, %ebx
	movq	%rdi, %r15
	cmpl	$-1, %edx
	je	.LBB12_5
# BB#1:
	testl	%edx, %edx
	je	.LBB12_11
# BB#2:
	cmpl	$1, %edx
	jne	.LBB12_4
# BB#3:
	cmpl	$0, (%rsi)
	je	.LBB12_5
.LBB12_4:
	movl	$-2147024809, %ebp      # imm = 0x80070057
	jmp	.LBB12_12
.LBB12_5:
	movq	(%r13), %rax
	movq	104(%r15), %rsi
.Ltmp284:
	movq	%r13, %rdi
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp285:
# BB#6:
	testl	%ebp, %ebp
	jne	.LBB12_12
# BB#7:
	movq	$0, (%rsp)
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	setne	%r14b
	movq	(%r13), %rax
.Ltmp287:
	movq	%rsp, %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%r14d, %ecx
	callq	*56(%rax)
	movl	%eax, %ebp
.Ltmp288:
# BB#8:
	testl	%ebp, %ebp
	je	.LBB12_13
.LBB12_9:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit138
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_12
# BB#10:
	movq	(%rdi), %rax
.Ltmp334:
	callq	*16(%rax)
.Ltmp335:
	jmp	.LBB12_12
.LBB12_11:
	xorl	%ebp, %ebp
.LBB12_12:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_13:
	testl	%ebx, %ebx
	sete	%al
	cmpq	$0, (%rsp)
	jne	.LBB12_15
# BB#14:
	xorl	%ebp, %ebp
	testb	%al, %al
	jne	.LBB12_12
.LBB12_15:
	movq	(%r13), %rax
.Ltmp289:
	movq	%r13, %rdi
	movl	%r14d, %esi
	callq	*64(%rax)
	movl	%eax, %ebp
.Ltmp290:
# BB#16:
	testl	%ebp, %ebp
	jne	.LBB12_9
# BB#17:
.Ltmp291:
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp292:
# BB#18:
	movl	$0, 16(%rbx)
	movl	$_ZTVN9NCompress10CCopyCoderE+88, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN9NCompress10CCopyCoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
.Ltmp294:
	movq	%rbx, %rdi
	callq	*_ZTVN9NCompress10CCopyCoderE+24(%rip)
.Ltmp295:
# BB#19:                                # %_ZN9CMyComPtrI14ICompressCoderEC2EPS0_.exit
.Ltmp297:
	movl	$72, %edi
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp298:
# BB#20:
.Ltmp300:
	movq	%rbp, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp301:
# BB#21:
	movq	(%rbp), %rax
.Ltmp303:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp304:
# BB#22:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp306:
	xorl	%edx, %edx
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp307:
# BB#23:                                # %.preheader
	cmpl	$0, 52(%r15)
	jle	.LBB12_32
# BB#24:                                # %.lr.ph
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
.LBB12_25:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, 56(%rdi)
	movq	%r12, 48(%rdi)
.Ltmp308:
	callq	_ZN14CLocalProgress6SetCurEv
	movl	%eax, %ebp
.Ltmp309:
# BB#26:                                #   in Loop: Header=BB12_25 Depth=1
	testl	%ebp, %ebp
	jne	.LBB12_36
# BB#27:                                #   in Loop: Header=BB12_25 Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rbx
	movq	(%rbx), %rax
.Ltmp311:
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp312:
# BB#28:                                #   in Loop: Header=BB12_25 Depth=1
	testl	%ebp, %ebp
	jne	.LBB12_39
# BB#29:                                #   in Loop: Header=BB12_25 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
	movq	(%rsp), %rdx
.Ltmp313:
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rbx, %rsi
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp314:
# BB#30:                                #   in Loop: Header=BB12_25 Depth=1
	testl	%ebp, %ebp
	jne	.LBB12_39
# BB#31:                                # %.thread
                                        #   in Loop: Header=BB12_25 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	addq	32(%rbx), %r12
	incq	%r14
	movslq	52(%r15), %rax
	cmpq	%rax, %r14
	jl	.LBB12_25
.LBB12_32:                              # %._crit_edge
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_35
# BB#33:
	movq	(%rdi), %rax
.Ltmp316:
	callq	*16(%rax)
.Ltmp317:
# BB#34:                                # %.noexc141
	movq	$0, (%rsp)
.LBB12_35:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
	movq	(%r13), %rax
.Ltmp318:
	xorl	%esi, %esi
	movq	%r13, %rdi
	callq	*72(%rax)
	movl	%eax, %ebp
.Ltmp319:
	jmp	.LBB12_36
.LBB12_39:
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB12_36:                              # %.thread154
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp323:
	callq	*16(%rax)
.Ltmp324:
# BB#37:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit140
	movq	(%rbx), %rax
.Ltmp328:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp329:
	jmp	.LBB12_9
.LBB12_40:
.Ltmp330:
	jmp	.LBB12_50
.LBB12_41:
.Ltmp325:
	jmp	.LBB12_45
.LBB12_42:
.Ltmp305:
	jmp	.LBB12_45
.LBB12_43:
.Ltmp302:
	movq	%rdx, %r15
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB12_56
.LBB12_44:
.Ltmp299:
.LBB12_45:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB12_56
.LBB12_46:
.Ltmp296:
	jmp	.LBB12_50
.LBB12_47:
.Ltmp320:
	jmp	.LBB12_55
.LBB12_48:
.Ltmp336:
	jmp	.LBB12_52
.LBB12_49:
.Ltmp293:
.LBB12_50:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB12_57
.LBB12_51:
.Ltmp286:
.LBB12_52:
	movq	%rdx, %r15
	movq	%rax, %rbx
	jmp	.LBB12_59
.LBB12_53:
.Ltmp310:
	jmp	.LBB12_55
.LBB12_54:
.Ltmp315:
.LBB12_55:
	movq	%rdx, %r15
	movq	%rax, %rbx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp321:
	callq	*16(%rax)
.Ltmp322:
.LBB12_56:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp326:
	callq	*16(%rax)
.Ltmp327:
.LBB12_57:                              # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB12_59
# BB#58:
	movq	(%rdi), %rax
.Ltmp331:
	callq	*16(%rax)
.Ltmp332:
.LBB12_59:
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r15d
	je	.LBB12_61
# BB#60:
	callq	__cxa_end_catch
	movl	$-2147024882, %ebp      # imm = 0x8007000E
	jmp	.LBB12_12
.LBB12_61:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp337:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp338:
# BB#62:
.LBB12_63:
.Ltmp339:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB12_64:
.Ltmp333:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end12:
	.size	_ZN8NArchive6NSplit8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end12-_ZN8NArchive6NSplit8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\371\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp284-.Lfunc_begin5  # >> Call Site 1 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp286-.Lfunc_begin5  #     jumps to .Ltmp286
	.byte	3                       #   On action: 2
	.long	.Ltmp287-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp288-.Ltmp287       #   Call between .Ltmp287 and .Ltmp288
	.long	.Ltmp293-.Lfunc_begin5  #     jumps to .Ltmp293
	.byte	3                       #   On action: 2
	.long	.Ltmp334-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp335-.Ltmp334       #   Call between .Ltmp334 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin5  #     jumps to .Ltmp336
	.byte	3                       #   On action: 2
	.long	.Ltmp289-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp292-.Ltmp289       #   Call between .Ltmp289 and .Ltmp292
	.long	.Ltmp293-.Lfunc_begin5  #     jumps to .Ltmp293
	.byte	3                       #   On action: 2
	.long	.Ltmp294-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp295-.Ltmp294       #   Call between .Ltmp294 and .Ltmp295
	.long	.Ltmp296-.Lfunc_begin5  #     jumps to .Ltmp296
	.byte	3                       #   On action: 2
	.long	.Ltmp297-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp298-.Ltmp297       #   Call between .Ltmp297 and .Ltmp298
	.long	.Ltmp299-.Lfunc_begin5  #     jumps to .Ltmp299
	.byte	3                       #   On action: 2
	.long	.Ltmp300-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp301-.Ltmp300       #   Call between .Ltmp300 and .Ltmp301
	.long	.Ltmp302-.Lfunc_begin5  #     jumps to .Ltmp302
	.byte	3                       #   On action: 2
	.long	.Ltmp303-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp304-.Ltmp303       #   Call between .Ltmp303 and .Ltmp304
	.long	.Ltmp305-.Lfunc_begin5  #     jumps to .Ltmp305
	.byte	3                       #   On action: 2
	.long	.Ltmp306-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp307-.Ltmp306       #   Call between .Ltmp306 and .Ltmp307
	.long	.Ltmp320-.Lfunc_begin5  #     jumps to .Ltmp320
	.byte	3                       #   On action: 2
	.long	.Ltmp308-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp309-.Ltmp308       #   Call between .Ltmp308 and .Ltmp309
	.long	.Ltmp310-.Lfunc_begin5  #     jumps to .Ltmp310
	.byte	3                       #   On action: 2
	.long	.Ltmp311-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp314-.Ltmp311       #   Call between .Ltmp311 and .Ltmp314
	.long	.Ltmp315-.Lfunc_begin5  #     jumps to .Ltmp315
	.byte	3                       #   On action: 2
	.long	.Ltmp316-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp319-.Ltmp316       #   Call between .Ltmp316 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin5  #     jumps to .Ltmp320
	.byte	3                       #   On action: 2
	.long	.Ltmp323-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin5  #     jumps to .Ltmp325
	.byte	3                       #   On action: 2
	.long	.Ltmp328-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp329-.Ltmp328       #   Call between .Ltmp328 and .Ltmp329
	.long	.Ltmp330-.Lfunc_begin5  #     jumps to .Ltmp330
	.byte	3                       #   On action: 2
	.long	.Ltmp321-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp332-.Ltmp321       #   Call between .Ltmp321 and .Ltmp332
	.long	.Ltmp333-.Lfunc_begin5  #     jumps to .Ltmp333
	.byte	1                       #   On action: 1
	.long	.Ltmp332-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp337-.Ltmp332       #   Call between .Ltmp332 and .Ltmp337
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp337-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp338-.Ltmp337       #   Call between .Ltmp337 and .Ltmp338
	.long	.Ltmp339-.Lfunc_begin5  #     jumps to .Ltmp339
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Lfunc_end12-.Ltmp338   #   Call between .Ltmp338 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream,@function
_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream: # @_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi60:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi61:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 96
.Lcfi64:
	.cfi_offset %rbx, -56
.Lcfi65:
	.cfi_offset %r12, -48
.Lcfi66:
	.cfi_offset %r13, -40
.Lcfi67:
	.cfi_offset %r14, -32
.Lcfi68:
	.cfi_offset %r15, -24
.Lcfi69:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rdi, %r12
	movl	$-2147024809, %eax      # imm = 0x80070057
	testl	%esi, %esi
	jne	.LBB13_37
# BB#1:
	movq	$0, (%rbx)
.Ltmp340:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp341:
# BB#2:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	$0, 8(%r15)
	movq	$_ZTV12CMultiStream+16, (%r15)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 48(%r15)
	movq	$8, 64(%r15)
	movq	$_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE+16, 40(%r15)
.Ltmp343:
	movq	%r15, %rdi
	callq	*_ZTV12CMultiStream+24(%rip)
.Ltmp344:
# BB#3:                                 # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit.preheader
	cmpl	$0, 52(%r12)
	jle	.LBB13_14
# BB#4:                                 # %.lr.ph
	movq	%r15, %rax
	addq	$40, %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB13_5:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%r14,8), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB13_7
# BB#6:                                 #   in Loop: Header=BB13_5 Depth=1
	movq	(%rbx), %rax
.Ltmp346:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp347:
.LBB13_7:                               # %_ZN9CMyComPtrI9IInStreamEaSERKS1_.exit
                                        #   in Loop: Header=BB13_5 Depth=1
	movq	88(%r12), %rax
	movq	(%rax,%r14,8), %r13
.Ltmp349:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp350:
# BB#8:                                 # %.noexc38
                                        #   in Loop: Header=BB13_5 Depth=1
	testq	%rbx, %rbx
	movq	%rbx, (%rbp)
	je	.LBB13_10
# BB#9:                                 #   in Loop: Header=BB13_5 Depth=1
	movq	(%rbx), %rax
.Ltmp351:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp352:
.LBB13_10:                              #   in Loop: Header=BB13_5 Depth=1
	movq	%r13, 8(%rbp)
	movups	24(%rsp), %xmm0
	movups	%xmm0, 16(%rbp)
.Ltmp354:
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp355:
# BB#11:                                #   in Loop: Header=BB13_5 Depth=1
	movslq	52(%r15), %rax
	leal	1(%rax), %ecx
	testq	%rbx, %rbx
	movq	56(%r15), %rdx
	movq	%rbp, (%rdx,%rax,8)
	movl	%ecx, 52(%r15)
	je	.LBB13_13
# BB#12:                                #   in Loop: Header=BB13_5 Depth=1
	movq	(%rbx), %rax
.Ltmp359:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp360:
.LBB13_13:                              # %_ZN12CMultiStream14CSubStreamInfoD2Ev.exit41
                                        #   in Loop: Header=BB13_5 Depth=1
	incq	%r14
	movslq	52(%r12), %rax
	cmpq	%rax, %r14
	jl	.LBB13_5
.LBB13_14:                              # %_ZN9CMyComPtrI19ISequentialInStreamEC2EPS0_.exit._crit_edge
	cmpl	$0, 52(%r15)
	jle	.LBB13_15
# BB#16:                                # %.lr.ph.i
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_17:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r15), %rax
	movq	(%rax,%rbp,8), %rcx
	movq	%rbx, 16(%rcx)
	movq	(%rcx), %rdi
	movq	8(%rcx), %r14
	movq	(%rdi), %rax
	addq	$24, %rcx
.Ltmp362:
	xorl	%esi, %esi
	movl	$1, %edx
	callq	*48(%rax)
.Ltmp363:
# BB#18:                                # %.noexc32
                                        #   in Loop: Header=BB13_17 Depth=1
	testl	%eax, %eax
	jne	.LBB13_21
# BB#19:                                #   in Loop: Header=BB13_17 Depth=1
	addq	%r14, %rbx
	incq	%rbp
	movslq	52(%r15), %rax
	cmpq	%rax, %rbp
	jl	.LBB13_17
	jmp	.LBB13_20
.LBB13_15:
	xorl	%ebx, %ebx
.LBB13_20:                              # %._crit_edge.i
	movq	%rbx, 24(%r15)
	movq	$0, 16(%r15)
	movl	$0, 32(%r15)
.LBB13_21:                              # %.loopexit
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%r15, (%rax)
	xorl	%eax, %eax
	jmp	.LBB13_37
.LBB13_24:
.Ltmp345:
	jmp	.LBB13_23
.LBB13_22:
.Ltmp342:
.LBB13_23:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%rdx, %r14
	movq	%rax, %r12
	jmp	.LBB13_33
.LBB13_29:
.Ltmp361:
	jmp	.LBB13_31
.LBB13_26:                              # %.body.thread
.Ltmp353:
	movq	%rdx, %r14
	movq	%rax, %r12
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB13_28
.LBB13_25:                              # %.body.thread60
.Ltmp348:
	jmp	.LBB13_31
.LBB13_30:
.Ltmp364:
.LBB13_31:
	movq	%rdx, %r14
	movq	%rax, %r12
	jmp	.LBB13_32
.LBB13_27:                              # %.body
.Ltmp356:
	movq	%rdx, %r14
	movq	%rax, %r12
	testq	%rbx, %rbx
	je	.LBB13_32
.LBB13_28:
	movq	(%rbx), %rax
.Ltmp357:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp358:
.LBB13_32:
	movq	(%r15), %rax
.Ltmp365:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp366:
.LBB13_33:                              # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	%r12, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r14d
	je	.LBB13_34
# BB#36:
	callq	__cxa_end_catch
	movl	$-2147024882, %eax      # imm = 0x8007000E
.LBB13_37:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_34:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp368:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp369:
# BB#39:
.LBB13_35:
.Ltmp370:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_38:
.Ltmp367:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream, .Lfunc_end13-_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\253\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Ltmp340-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp341-.Ltmp340       #   Call between .Ltmp340 and .Ltmp341
	.long	.Ltmp342-.Lfunc_begin6  #     jumps to .Ltmp342
	.byte	3                       #   On action: 2
	.long	.Ltmp343-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp344-.Ltmp343       #   Call between .Ltmp343 and .Ltmp344
	.long	.Ltmp345-.Lfunc_begin6  #     jumps to .Ltmp345
	.byte	3                       #   On action: 2
	.long	.Ltmp346-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp347-.Ltmp346       #   Call between .Ltmp346 and .Ltmp347
	.long	.Ltmp348-.Lfunc_begin6  #     jumps to .Ltmp348
	.byte	3                       #   On action: 2
	.long	.Ltmp349-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp350-.Ltmp349       #   Call between .Ltmp349 and .Ltmp350
	.long	.Ltmp356-.Lfunc_begin6  #     jumps to .Ltmp356
	.byte	3                       #   On action: 2
	.long	.Ltmp351-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp352-.Ltmp351       #   Call between .Ltmp351 and .Ltmp352
	.long	.Ltmp353-.Lfunc_begin6  #     jumps to .Ltmp353
	.byte	3                       #   On action: 2
	.long	.Ltmp354-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp355-.Ltmp354       #   Call between .Ltmp354 and .Ltmp355
	.long	.Ltmp356-.Lfunc_begin6  #     jumps to .Ltmp356
	.byte	3                       #   On action: 2
	.long	.Ltmp359-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin6  #     jumps to .Ltmp361
	.byte	3                       #   On action: 2
	.long	.Ltmp362-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin6  #     jumps to .Ltmp364
	.byte	3                       #   On action: 2
	.long	.Ltmp357-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Ltmp366-.Ltmp357       #   Call between .Ltmp357 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin6  #     jumps to .Ltmp367
	.byte	1                       #   On action: 1
	.long	.Ltmp366-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp368-.Ltmp366       #   Call between .Ltmp366 and .Ltmp368
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp368-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp369-.Ltmp368       #   Call between .Ltmp368 and .Ltmp369
	.long	.Ltmp370-.Lfunc_begin6  #     jumps to .Ltmp370
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Lfunc_end13-.Ltmp369   #   Call between .Ltmp369 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZThn8_N8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream,@function
_ZThn8_N8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream: # @_ZThn8_N8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream # TAILCALL
.Lfunc_end14:
	.size	_ZThn8_N8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream, .Lfunc_end14-_ZThn8_N8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
	.cfi_endproc

	.section	.text._ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi70:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 32
.Lcfi73:
	.cfi_offset %rbx, -32
.Lcfi74:
	.cfi_offset %r14, -24
.Lcfi75:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB15_1
# BB#2:
	movl	$IID_IInArchive, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB15_3
.LBB15_1:
	movq	%rbx, (%r14)
.LBB15_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB15_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB15_3:
	movl	$IID_IInArchiveGetStream, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB15_4
# BB#5:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB15_6
.LBB15_4:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB15_7
.Lfunc_end15:
	.size	_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end15-_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive6NSplit8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive6NSplit8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive6NSplit8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler6AddRefEv,@function
_ZN8NArchive6NSplit8CHandler6AddRefEv:  # @_ZN8NArchive6NSplit8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end16:
	.size	_ZN8NArchive6NSplit8CHandler6AddRefEv, .Lfunc_end16-_ZN8NArchive6NSplit8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive6NSplit8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive6NSplit8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive6NSplit8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandler7ReleaseEv,@function
_ZN8NArchive6NSplit8CHandler7ReleaseEv: # @_ZN8NArchive6NSplit8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi76:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB17_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB17_2:
	popq	%rcx
	retq
.Lfunc_end17:
	.size	_ZN8NArchive6NSplit8CHandler7ReleaseEv, .Lfunc_end17-_ZN8NArchive6NSplit8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive6NSplit8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive6NSplit8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive6NSplit8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandlerD2Ev,@function
_ZN8NArchive6NSplit8CHandlerD2Ev:       # @_ZN8NArchive6NSplit8CHandlerD2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi79:
	.cfi_def_cfa_offset 32
.Lcfi80:
	.cfi_offset %rbx, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$_ZTVN8NArchive6NSplit8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive6NSplit8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	leaq	72(%r15), %rdi
.Ltmp371:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp372:
# BB#1:
	leaq	40(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE+16, 40(%r15)
.Ltmp383:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp384:
# BB#2:
.Ltmp389:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp390:
# BB#3:                                 # %_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev.exit
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB18_7
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZdaPv                  # TAILCALL
.LBB18_7:                               # %_ZN11CStringBaseIwED2Ev.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB18_12:
.Ltmp391:
	movq	%rax, %r14
	jmp	.LBB18_13
.LBB18_5:
.Ltmp385:
	movq	%rax, %r14
.Ltmp386:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp387:
	jmp	.LBB18_13
.LBB18_6:
.Ltmp388:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB18_8:
.Ltmp373:
	movq	%rax, %r14
	leaq	40(%r15), %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE+16, 40(%r15)
.Ltmp374:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp375:
# BB#9:
.Ltmp380:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp381:
.LBB18_13:                              # %_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev.exit6
	movq	24(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB18_15
# BB#14:
	callq	_ZdaPv
.LBB18_15:                              # %_ZN11CStringBaseIwED2Ev.exit7
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB18_16:
.Ltmp382:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB18_10:
.Ltmp376:
	movq	%rax, %r14
.Ltmp377:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp378:
# BB#17:                                # %.body4
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB18_11:
.Ltmp379:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN8NArchive6NSplit8CHandlerD2Ev, .Lfunc_end18-_ZN8NArchive6NSplit8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp371-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp372-.Ltmp371       #   Call between .Ltmp371 and .Ltmp372
	.long	.Ltmp373-.Lfunc_begin7  #     jumps to .Ltmp373
	.byte	0                       #   On action: cleanup
	.long	.Ltmp383-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp384-.Ltmp383       #   Call between .Ltmp383 and .Ltmp384
	.long	.Ltmp385-.Lfunc_begin7  #     jumps to .Ltmp385
	.byte	0                       #   On action: cleanup
	.long	.Ltmp389-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp390-.Ltmp389       #   Call between .Ltmp389 and .Ltmp390
	.long	.Ltmp391-.Lfunc_begin7  #     jumps to .Ltmp391
	.byte	0                       #   On action: cleanup
	.long	.Ltmp386-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp387-.Ltmp386       #   Call between .Ltmp386 and .Ltmp387
	.long	.Ltmp388-.Lfunc_begin7  #     jumps to .Ltmp388
	.byte	1                       #   On action: 1
	.long	.Ltmp374-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp375-.Ltmp374       #   Call between .Ltmp374 and .Ltmp375
	.long	.Ltmp376-.Lfunc_begin7  #     jumps to .Ltmp376
	.byte	1                       #   On action: 1
	.long	.Ltmp380-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp381-.Ltmp380       #   Call between .Ltmp380 and .Ltmp381
	.long	.Ltmp382-.Lfunc_begin7  #     jumps to .Ltmp382
	.byte	1                       #   On action: 1
	.long	.Ltmp381-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp377-.Ltmp381       #   Call between .Ltmp381 and .Ltmp377
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin7  #     jumps to .Ltmp379
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive6NSplit8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive6NSplit8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive6NSplit8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplit8CHandlerD0Ev,@function
_ZN8NArchive6NSplit8CHandlerD0Ev:       # @_ZN8NArchive6NSplit8CHandlerD0Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp392:
	callq	_ZN8NArchive6NSplit8CHandlerD2Ev
.Ltmp393:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB19_2:
.Ltmp394:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end19:
	.size	_ZN8NArchive6NSplit8CHandlerD0Ev, .Lfunc_end19-_ZN8NArchive6NSplit8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table19:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp392-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp393-.Ltmp392       #   Call between .Ltmp392 and .Ltmp393
	.long	.Ltmp394-.Lfunc_begin8  #     jumps to .Ltmp394
	.byte	0                       #   On action: cleanup
	.long	.Ltmp393-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Lfunc_end19-.Ltmp393   #   Call between .Ltmp393 and .Lfunc_end19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB20_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB20_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB20_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB20_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB20_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB20_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB20_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB20_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB20_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB20_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB20_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB20_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB20_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB20_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB20_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB20_16
.LBB20_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB20_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB20_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB20_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB20_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB20_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB20_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB20_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB20_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB20_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB20_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB20_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB20_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB20_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB20_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB20_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB20_33
.LBB20_16:
	movq	%rdi, (%rdx)
	jmp	.LBB20_50
.LBB20_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IInArchiveGetStream(%rip), %cl
	jne	.LBB20_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+1(%rip), %cl
	jne	.LBB20_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+2(%rip), %cl
	jne	.LBB20_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+3(%rip), %cl
	jne	.LBB20_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+4(%rip), %cl
	jne	.LBB20_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+5(%rip), %cl
	jne	.LBB20_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+6(%rip), %cl
	jne	.LBB20_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+7(%rip), %cl
	jne	.LBB20_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+8(%rip), %cl
	jne	.LBB20_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+9(%rip), %cl
	jne	.LBB20_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+10(%rip), %cl
	jne	.LBB20_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+11(%rip), %cl
	jne	.LBB20_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+12(%rip), %cl
	jne	.LBB20_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+13(%rip), %cl
	jne	.LBB20_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+14(%rip), %cl
	jne	.LBB20_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_IInArchiveGetStream+15(%rip), %cl
	jne	.LBB20_51
# BB#49:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
.LBB20_50:                              # %_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv.exit
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB20_51:                              # %_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end20:
	.size	_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end20-_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive6NSplit8CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv,@function
_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv: # @_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end21:
	.size	_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv, .Lfunc_end21-_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv,@function
_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv: # @_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB22_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB22_2:                               # %_ZN8NArchive6NSplit8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end22:
	.size	_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv, .Lfunc_end22-_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive6NSplit8CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive6NSplit8CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive6NSplit8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive6NSplit8CHandlerD1Ev,@function
_ZThn8_N8NArchive6NSplit8CHandlerD1Ev:  # @_ZThn8_N8NArchive6NSplit8CHandlerD1Ev
	.cfi_startproc
# BB#0:
	addq	$-8, %rdi
	jmp	_ZN8NArchive6NSplit8CHandlerD2Ev # TAILCALL
.Lfunc_end23:
	.size	_ZThn8_N8NArchive6NSplit8CHandlerD1Ev, .Lfunc_end23-_ZThn8_N8NArchive6NSplit8CHandlerD1Ev
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive6NSplit8CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive6NSplit8CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive6NSplit8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive6NSplit8CHandlerD0Ev,@function
_ZThn8_N8NArchive6NSplit8CHandlerD0Ev:  # @_ZThn8_N8NArchive6NSplit8CHandlerD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 32
.Lcfi93:
	.cfi_offset %rbx, -24
.Lcfi94:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	addq	$-8, %rbx
.Ltmp395:
	movq	%rbx, %rdi
	callq	_ZN8NArchive6NSplit8CHandlerD2Ev
.Ltmp396:
# BB#1:                                 # %_ZN8NArchive6NSplit8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB24_2:
.Ltmp397:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZThn8_N8NArchive6NSplit8CHandlerD0Ev, .Lfunc_end24-_ZThn8_N8NArchive6NSplit8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp395-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp396-.Ltmp395       #   Call between .Ltmp395 and .Ltmp396
	.long	.Ltmp397-.Lfunc_begin9  #     jumps to .Ltmp397
	.byte	0                       #   On action: cleanup
	.long	.Ltmp396-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end24-.Ltmp396   #   Call between .Ltmp396 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi95:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi96:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi98:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi99:
	.cfi_def_cfa_offset 48
.Lcfi100:
	.cfi_offset %rbx, -48
.Lcfi101:
	.cfi_offset %r12, -40
.Lcfi102:
	.cfi_offset %r14, -32
.Lcfi103:
	.cfi_offset %r15, -24
.Lcfi104:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB25_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB25_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB25_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB25_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB25_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB25_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB25_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB25_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB25_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB25_17
.LBB25_7:
	xorl	%ecx, %ecx
.LBB25_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB25_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB25_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB25_10
.LBB25_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB25_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB25_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB25_13
	jmp	.LBB25_26
.LBB25_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB25_27
.LBB25_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB25_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB25_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB25_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB25_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB25_20
	jmp	.LBB25_21
.LBB25_18:
	xorl	%ebx, %ebx
.LBB25_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB25_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB25_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB25_23
.LBB25_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB25_8
	jmp	.LBB25_26
.Lfunc_end25:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end25-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev,@function
_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev: # @_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 32
.Lcfi108:
	.cfi_offset %rbx, -24
.Lcfi109:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE+16, (%rbx)
.Ltmp398:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp399:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB26_2:
.Ltmp400:
	movq	%rax, %r14
.Ltmp401:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp402:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB26_4:
.Ltmp403:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev, .Lfunc_end26-_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp398-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp399-.Ltmp398       #   Call between .Ltmp398 and .Ltmp399
	.long	.Ltmp400-.Lfunc_begin10 #     jumps to .Ltmp400
	.byte	0                       #   On action: cleanup
	.long	.Ltmp399-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp401-.Ltmp399       #   Call between .Ltmp399 and .Ltmp401
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp401-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp402-.Ltmp401       #   Call between .Ltmp401 and .Ltmp402
	.long	.Ltmp403-.Lfunc_begin10 #     jumps to .Ltmp403
	.byte	1                       #   On action: 1
	.long	.Ltmp402-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Lfunc_end26-.Ltmp402   #   Call between .Ltmp402 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev,@function
_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev: # @_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 32
.Lcfi113:
	.cfi_offset %rbx, -24
.Lcfi114:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE+16, (%rbx)
.Ltmp404:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp405:
# BB#1:
.Ltmp410:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp411:
# BB#2:                                 # %_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB27_5:
.Ltmp412:
	movq	%rax, %r14
	jmp	.LBB27_6
.LBB27_3:
.Ltmp406:
	movq	%rax, %r14
.Ltmp407:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp408:
.LBB27_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB27_4:
.Ltmp409:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end27:
	.size	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev, .Lfunc_end27-_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp404-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp405-.Ltmp404       #   Call between .Ltmp404 and .Ltmp405
	.long	.Ltmp406-.Lfunc_begin11 #     jumps to .Ltmp406
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp412-.Lfunc_begin11 #     jumps to .Ltmp412
	.byte	0                       #   On action: cleanup
	.long	.Ltmp407-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp408-.Ltmp407       #   Call between .Ltmp407 and .Ltmp408
	.long	.Ltmp409-.Lfunc_begin11 #     jumps to .Ltmp409
	.byte	1                       #   On action: 1
	.long	.Ltmp408-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Lfunc_end27-.Ltmp408   #   Call between .Ltmp408 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii: # @_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 64
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB28_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB28_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB28_6
# BB#3:                                 #   in Loop: Header=BB28_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB28_5
# BB#4:                                 #   in Loop: Header=BB28_2 Depth=1
	movq	(%rdi), %rax
.Ltmp413:
	callq	*16(%rax)
.Ltmp414:
.LBB28_5:                               # %_ZN12CMultiStream14CSubStreamInfoD2Ev.exit
                                        #   in Loop: Header=BB28_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB28_6:                               #   in Loop: Header=BB28_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB28_2
.LBB28_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB28_8:
.Ltmp415:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end28:
	.size	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii, .Lfunc_end28-_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table28:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp413-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp414-.Ltmp413       #   Call between .Ltmp413 and .Ltmp414
	.long	.Ltmp415-.Lfunc_begin12 #     jumps to .Ltmp415
	.byte	0                       #   On action: cleanup
	.long	.Ltmp414-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end28-.Ltmp414   #   Call between .Ltmp414 and .Lfunc_end28
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive6NSplitL9CreateArcEv,@function
_ZN8NArchive6NSplitL9CreateArcEv:       # @_ZN8NArchive6NSplitL9CreateArcEv
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -24
.Lcfi132:
	.cfi_offset %r14, -16
	movl	$112, %edi
	callq	_Znwm
	movq	%rax, %rbx
	movl	$0, 16(%rbx)
	movl	$_ZTVN8NArchive6NSplit8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive6NSplit8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbx)
.Ltmp416:
	movl	$16, %edi
	callq	_Znam
.Ltmp417:
# BB#1:
	movq	%rax, 24(%rbx)
	movl	$0, (%rax)
	movl	$4, 36(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbx)
	movq	$8, 64(%rbx)
	movq	$_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE+16, 40(%rbx)
	movdqu	%xmm0, 80(%rbx)
	movq	$8, 96(%rbx)
	movq	$_ZTV13CRecordVectorIyE+16, 72(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB29_2:
.Ltmp418:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end29:
	.size	_ZN8NArchive6NSplitL9CreateArcEv, .Lfunc_end29-_ZN8NArchive6NSplitL9CreateArcEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp416-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp416
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp416-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp417-.Ltmp416       #   Call between .Ltmp416 and .Ltmp417
	.long	.Ltmp418-.Lfunc_begin13 #     jumps to .Ltmp418
	.byte	0                       #   On action: cleanup
	.long	.Ltmp417-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Lfunc_end29-.Ltmp417   #   Call between .Ltmp417 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev,@function
_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev: # @_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi135:
	.cfi_def_cfa_offset 32
.Lcfi136:
	.cfi_offset %rbx, -24
.Lcfi137:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE+16, (%rbx)
.Ltmp419:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp420:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB30_2:
.Ltmp421:
	movq	%rax, %r14
.Ltmp422:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp423:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_4:
.Ltmp424:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev, .Lfunc_end30-_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp419-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp420-.Ltmp419       #   Call between .Ltmp419 and .Ltmp420
	.long	.Ltmp421-.Lfunc_begin14 #     jumps to .Ltmp421
	.byte	0                       #   On action: cleanup
	.long	.Ltmp420-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp422-.Ltmp420       #   Call between .Ltmp420 and .Ltmp422
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp424-.Lfunc_begin14 #     jumps to .Ltmp424
	.byte	1                       #   On action: 1
	.long	.Ltmp423-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end30-.Ltmp423   #   Call between .Ltmp423 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev,@function
_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev: # @_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi140:
	.cfi_def_cfa_offset 32
.Lcfi141:
	.cfi_offset %rbx, -24
.Lcfi142:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE+16, (%rbx)
.Ltmp425:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp426:
# BB#1:
.Ltmp431:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp432:
# BB#2:                                 # %_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB31_5:
.Ltmp433:
	movq	%rax, %r14
	jmp	.LBB31_6
.LBB31_3:
.Ltmp427:
	movq	%rax, %r14
.Ltmp428:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp429:
.LBB31_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB31_4:
.Ltmp430:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end31:
	.size	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev, .Lfunc_end31-_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table31:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp425-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp426-.Ltmp425       #   Call between .Ltmp425 and .Ltmp426
	.long	.Ltmp427-.Lfunc_begin15 #     jumps to .Ltmp427
	.byte	0                       #   On action: cleanup
	.long	.Ltmp431-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp432-.Ltmp431       #   Call between .Ltmp431 and .Ltmp432
	.long	.Ltmp433-.Lfunc_begin15 #     jumps to .Ltmp433
	.byte	0                       #   On action: cleanup
	.long	.Ltmp428-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp429-.Ltmp428       #   Call between .Ltmp428 and .Ltmp429
	.long	.Ltmp430-.Lfunc_begin15 #     jumps to .Ltmp430
	.byte	1                       #   On action: 1
	.long	.Ltmp429-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Lfunc_end31-.Ltmp429   #   Call between .Ltmp429 and .Lfunc_end31
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii,@function
_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii: # @_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi143:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi144:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi145:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi146:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi147:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi148:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi149:
	.cfi_def_cfa_offset 64
.Lcfi150:
	.cfi_offset %rbx, -56
.Lcfi151:
	.cfi_offset %r12, -48
.Lcfi152:
	.cfi_offset %r13, -40
.Lcfi153:
	.cfi_offset %r14, -32
.Lcfi154:
	.cfi_offset %r15, -24
.Lcfi155:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB32_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB32_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB32_6
# BB#3:                                 #   in Loop: Header=BB32_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB32_5
# BB#4:                                 #   in Loop: Header=BB32_2 Depth=1
	movq	(%rdi), %rax
.Ltmp434:
	callq	*16(%rax)
.Ltmp435:
.LBB32_5:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
                                        #   in Loop: Header=BB32_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB32_6:                               #   in Loop: Header=BB32_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB32_2
.LBB32_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB32_8:
.Ltmp436:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end32:
	.size	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii, .Lfunc_end32-_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table32:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp434-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp435-.Ltmp434       #   Call between .Ltmp434 and .Ltmp435
	.long	.Ltmp436-.Lfunc_begin16 #     jumps to .Ltmp436
	.byte	0                       #   On action: cleanup
	.long	.Ltmp435-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Lfunc_end32-.Ltmp435   #   Call between .Ltmp435 and .Lfunc_end32
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 32
.Lcfi159:
	.cfi_offset %rbx, -24
.Lcfi160:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp437:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp438:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_2:
.Ltmp439:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end33-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp437-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp438-.Ltmp437       #   Call between .Ltmp437 and .Ltmp438
	.long	.Ltmp439-.Lfunc_begin17 #     jumps to .Ltmp439
	.byte	0                       #   On action: cleanup
	.long	.Ltmp438-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp438   #   Call between .Ltmp438 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB34_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB34_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB34_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB34_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB34_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB34_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB34_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB34_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB34_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB34_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB34_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB34_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB34_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB34_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB34_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB34_16:
	xorl	%eax, %eax
	retq
.Lfunc_end34:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end34-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%rbp
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi163:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi164:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi165:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi166:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi167:
	.cfi_def_cfa_offset 64
.Lcfi168:
	.cfi_offset %rbx, -56
.Lcfi169:
	.cfi_offset %r12, -48
.Lcfi170:
	.cfi_offset %r13, -40
.Lcfi171:
	.cfi_offset %r14, -32
.Lcfi172:
	.cfi_offset %r15, -24
.Lcfi173:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB35_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB35_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB35_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB35_5
.LBB35_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB35_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp440:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp441:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB35_35
# BB#12:
	movq	%rbx, %r13
.LBB35_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB35_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB35_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB35_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB35_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB35_15
.LBB35_14:
	xorl	%esi, %esi
.LBB35_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB35_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB35_16
.LBB35_29:
	movq	%r13, %rbx
.LBB35_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp442:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp443:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB35_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB35_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB35_8
.LBB35_3:
	xorl	%eax, %eax
.LBB35_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB35_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB35_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB35_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB35_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB35_30
.LBB35_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB35_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB35_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB35_24
	jmp	.LBB35_25
.LBB35_22:
	xorl	%ecx, %ecx
.LBB35_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB35_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB35_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB35_27
.LBB35_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB35_15
	jmp	.LBB35_29
.LBB35_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp444:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end35:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end35-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin18-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp440-.Lfunc_begin18 #   Call between .Lfunc_begin18 and .Ltmp440
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp440-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp443-.Ltmp440       #   Call between .Ltmp440 and .Ltmp443
	.long	.Ltmp444-.Lfunc_begin18 #     jumps to .Ltmp444
	.byte	0                       #   On action: cleanup
	.long	.Ltmp443-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Lfunc_end35-.Ltmp443   #   Call between .Ltmp443 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_SplitHandler.ii,@function
_GLOBAL__sub_I_SplitHandler.ii:         # @_GLOBAL__sub_I_SplitHandler.ii
	.cfi_startproc
# BB#0:
	movl	$_ZN8NArchive6NSplitL9g_ArcInfoE, %edi
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end36:
	.size	_GLOBAL__sub_I_SplitHandler.ii, .Lfunc_end36-_GLOBAL__sub_I_SplitHandler.ii
	.cfi_endproc

	.type	_ZN8NArchive6NSplit6kPropsE,@object # @_ZN8NArchive6NSplit6kPropsE
	.data
	.globl	_ZN8NArchive6NSplit6kPropsE
	.p2align	4
_ZN8NArchive6NSplit6kPropsE:
	.quad	0
	.long	3                       # 0x3
	.short	8                       # 0x8
	.zero	2
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.size	_ZN8NArchive6NSplit6kPropsE, 32

	.type	_ZN8NArchive6NSplit9kArcPropsE,@object # @_ZN8NArchive6NSplit9kArcPropsE
	.globl	_ZN8NArchive6NSplit9kArcPropsE
	.p2align	4
_ZN8NArchive6NSplit9kArcPropsE:
	.quad	0
	.long	39                      # 0x27
	.short	19                      # 0x13
	.zero	2
	.size	_ZN8NArchive6NSplit9kArcPropsE, 16

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	65                      # 0x41
	.long	65                      # 0x41
	.long	0                       # 0x0
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
	.p2align	2
.L.str.1:
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	0                       # 0x0
	.size	.L.str.1, 12

	.type	_ZTVN8NArchive6NSplit8CHandlerE,@object # @_ZTVN8NArchive6NSplit8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive6NSplit8CHandlerE
	.p2align	3
_ZTVN8NArchive6NSplit8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive6NSplit8CHandlerE
	.quad	_ZN8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive6NSplit8CHandler6AddRefEv
	.quad	_ZN8NArchive6NSplit8CHandler7ReleaseEv
	.quad	_ZN8NArchive6NSplit8CHandlerD2Ev
	.quad	_ZN8NArchive6NSplit8CHandlerD0Ev
	.quad	_ZN8NArchive6NSplit8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive6NSplit8CHandler5CloseEv
	.quad	_ZN8NArchive6NSplit8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive6NSplit8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive6NSplit8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive6NSplit8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive6NSplit8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive6NSplit8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive6NSplit8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive6NSplit8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
	.quad	-8
	.quad	_ZTIN8NArchive6NSplit8CHandlerE
	.quad	_ZThn8_N8NArchive6NSplit8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive6NSplit8CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive6NSplit8CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive6NSplit8CHandlerD1Ev
	.quad	_ZThn8_N8NArchive6NSplit8CHandlerD0Ev
	.quad	_ZThn8_N8NArchive6NSplit8CHandler9GetStreamEjPP19ISequentialInStream
	.size	_ZTVN8NArchive6NSplit8CHandlerE, 208

	.type	_ZTSN8NArchive6NSplit8CHandlerE,@object # @_ZTSN8NArchive6NSplit8CHandlerE
	.globl	_ZTSN8NArchive6NSplit8CHandlerE
	.p2align	4
_ZTSN8NArchive6NSplit8CHandlerE:
	.asciz	"N8NArchive6NSplit8CHandlerE"
	.size	_ZTSN8NArchive6NSplit8CHandlerE, 28

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS19IInArchiveGetStream,@object # @_ZTS19IInArchiveGetStream
	.section	.rodata._ZTS19IInArchiveGetStream,"aG",@progbits,_ZTS19IInArchiveGetStream,comdat
	.weak	_ZTS19IInArchiveGetStream
	.p2align	4
_ZTS19IInArchiveGetStream:
	.asciz	"19IInArchiveGetStream"
	.size	_ZTS19IInArchiveGetStream, 22

	.type	_ZTI19IInArchiveGetStream,@object # @_ZTI19IInArchiveGetStream
	.section	.rodata._ZTI19IInArchiveGetStream,"aG",@progbits,_ZTI19IInArchiveGetStream,comdat
	.weak	_ZTI19IInArchiveGetStream
	.p2align	4
_ZTI19IInArchiveGetStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19IInArchiveGetStream
	.quad	_ZTI8IUnknown
	.size	_ZTI19IInArchiveGetStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive6NSplit8CHandlerE,@object # @_ZTIN8NArchive6NSplit8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive6NSplit8CHandlerE
	.p2align	4
_ZTIN8NArchive6NSplit8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive6NSplit8CHandlerE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI19IInArchiveGetStream
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN8NArchive6NSplit8CHandlerE, 72

	.type	_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,@object # @_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.section	.rodata._ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.p2align	3
_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.quad	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED2Ev
	.quad	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEED0Ev
	.quad	_ZN13CObjectVectorIN12CMultiStream14CSubStreamInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN12CMultiStream14CSubStreamInfoEE, 40

	.type	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,@object # @_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.section	.rodata._ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.p2align	4
_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE:
	.asciz	"13CObjectVectorIN12CMultiStream14CSubStreamInfoEE"
	.size	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE, 50

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,@object # @_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.section	.rodata._ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.p2align	4
_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN12CMultiStream14CSubStreamInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN12CMultiStream14CSubStreamInfoEE, 24

	.type	_ZN8NArchive6NSplitL9g_ArcInfoE,@object # @_ZN8NArchive6NSplitL9g_ArcInfoE
	.data
	.p2align	3
_ZN8NArchive6NSplitL9g_ArcInfoE:
	.quad	.L.str.3
	.quad	.L.str.4
	.quad	0
	.byte	234                     # 0xea
	.zero	28
	.zero	3
	.long	0                       # 0x0
	.byte	0                       # 0x0
	.zero	3
	.quad	_ZN8NArchive6NSplitL9CreateArcEv
	.quad	0
	.size	_ZN8NArchive6NSplitL9g_ArcInfoE, 80

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.3:
	.long	83                      # 0x53
	.long	112                     # 0x70
	.long	108                     # 0x6c
	.long	105                     # 0x69
	.long	116                     # 0x74
	.long	0                       # 0x0
	.size	.L.str.3, 24

	.type	.L.str.4,@object        # @.str.4
	.p2align	2
.L.str.4:
	.long	48                      # 0x30
	.long	48                      # 0x30
	.long	49                      # 0x31
	.long	0                       # 0x0
	.size	.L.str.4, 16

	.type	_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE,@object # @_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE
	.section	.rodata._ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE,"aG",@progbits,_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE,comdat
	.weak	_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE
	.p2align	3
_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE
	.quad	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED2Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEED0Ev
	.quad	_ZN13CObjectVectorI9CMyComPtrI9IInStreamEE6DeleteEii
	.size	_ZTV13CObjectVectorI9CMyComPtrI9IInStreamEE, 40

	.type	_ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE,@object # @_ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE
	.section	.rodata._ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE,"aG",@progbits,_ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE,comdat
	.weak	_ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE
	.p2align	4
_ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE:
	.asciz	"13CObjectVectorI9CMyComPtrI9IInStreamEE"
	.size	_ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE, 40

	.type	_ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE,@object # @_ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE
	.section	.rodata._ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE,"aG",@progbits,_ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE,comdat
	.weak	_ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE
	.p2align	4
_ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI9CMyComPtrI9IInStreamEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI9CMyComPtrI9IInStreamEE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_SplitHandler.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
