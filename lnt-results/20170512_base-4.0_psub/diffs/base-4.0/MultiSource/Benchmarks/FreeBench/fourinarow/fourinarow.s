	.text
	.file	"fourinarow.bc"
	.globl	init_patterns
	.p2align	4, 0x90
	.type	init_patterns,@function
init_patterns:                          # @init_patterns
	.cfi_startproc
# BB#0:
	movq	C4VERT(%rip), %rax
	shlq	$14, %rax
	orq	$16513, %rax            # imm = 0x4081
	movq	%rax, %rcx
	shlq	$7, %rcx
	orq	$1, %rcx
	movq	%rcx, C4VERT(%rip)
	movabsq	$144115188075839617, %rcx # imm = 0x1FFFFFFFFFFC081
	andq	%rax, %rcx
	movq	%rcx, C3VERT(%rip)
	shrq	$7, %rax
	movabsq	$1125899906842497, %rcx # imm = 0x3FFFFFFFFFF81
	andq	%rax, %rcx
	movq	%rcx, C2VERT(%rip)
	movq	$15, C4HORIZ(%rip)
	movq	$7, C3HORIZ(%rip)
	movq	$3, C2HORIZ(%rip)
	movq	C4UP_R(%rip), %rax
	shlq	$16, %rax
	orq	$65793, %rax            # imm = 0x10101
	movq	%rax, %rcx
	shlq	$8, %rcx
	orq	$1, %rcx
	movq	%rcx, C4UP_R(%rip)
	movabsq	$72057594037862657, %rcx # imm = 0xFFFFFFFFFF0101
	andq	%rax, %rcx
	movq	%rcx, C3UP_R(%rip)
	shrq	$8, %rax
	movabsq	$281474976710401, %rcx  # imm = 0xFFFFFFFFFF01
	andq	%rax, %rcx
	movq	%rcx, C2UP_R(%rip)
	movq	C4UP_L(%rip), %rax
	shlq	$12, %rax
	orq	$33288, %rax            # imm = 0x8208
	movq	%rax, %rcx
	shlq	$6, %rcx
	orq	$8, %rcx
	movq	%rcx, C4UP_L(%rip)
	movabsq	$288230376151708168, %rcx # imm = 0x3FFFFFFFFFFF208
	andq	%rax, %rcx
	movq	%rcx, C3UP_L(%rip)
	shrq	$6, %rax
	movabsq	$4503599627370440, %rcx # imm = 0xFFFFFFFFFFFC8
	andq	%rax, %rcx
	movq	%rcx, C2UP_L(%rip)
	retq
.Lfunc_end0:
	.size	init_patterns, .Lfunc_end0-init_patterns
	.cfi_endproc

	.globl	init_board
	.p2align	4, 0x90
	.type	init_board,@function
init_board:                             # @init_board
	.cfi_startproc
# BB#0:                                 # %.preheader14
	movw	$11822, 4(%rdi)         # imm = 0x2E2E
	movl	$774778414, (%rdi)      # imm = 0x2E2E2E2E
	movw	$11822, 11(%rdi)        # imm = 0x2E2E
	movl	$774778414, 7(%rdi)     # imm = 0x2E2E2E2E
	movw	$11822, 18(%rdi)        # imm = 0x2E2E
	movl	$774778414, 14(%rdi)    # imm = 0x2E2E2E2E
	movw	$11822, 25(%rdi)        # imm = 0x2E2E
	movl	$774778414, 21(%rdi)    # imm = 0x2E2E2E2E
	movw	$11822, 32(%rdi)        # imm = 0x2E2E
	movl	$774778414, 28(%rdi)    # imm = 0x2E2E2E2E
	movw	$11822, 39(%rdi)        # imm = 0x2E2E
	movl	$774778414, 35(%rdi)    # imm = 0x2E2E2E2E
	movw	$11822, 46(%rdi)        # imm = 0x2E2E
	movl	$774778414, 42(%rdi)    # imm = 0x2E2E2E2E
	movb	$0, 6(%rdi)
	movb	$0, 13(%rdi)
	movb	$0, 20(%rdi)
	movb	$0, 27(%rdi)
	movb	$0, 34(%rdi)
	movb	$0, 41(%rdi)
	movb	$0, 48(%rdi)
	retq
.Lfunc_end1:
	.size	init_board, .Lfunc_end1-init_board
	.cfi_endproc

	.globl	print_board
	.p2align	4, 0x90
	.type	print_board,@function
print_board:                            # @print_board
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movsbl	6(%r15), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	13(%r15), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	20(%r15), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	27(%r15), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	34(%r15), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	41(%r15), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	48(%r15), %esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$47, %ebx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	leaq	-42(%rbx), %r14
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movsbl	-42(%r15,%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	-35(%r15,%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	-28(%r15,%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	-21(%r15,%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	-14(%r15,%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	-7(%r15,%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movsbl	(%r15,%rbx), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	decq	%rbx
	testq	%r14, %r14
	jg	.LBB2_1
# BB#2:
	movq	stdout(%rip), %rsi
	movl	$32, %edi
	callq	_IO_putc
	movl	$.L.str, %edi
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str, %edi
	movl	$1, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str, %edi
	movl	$2, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str, %edi
	movl	$3, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str, %edi
	movl	$4, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str, %edi
	movl	$5, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str, %edi
	movl	$6, %esi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movl	$.Lstr, %edi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	puts                    # TAILCALL
.Lfunc_end2:
	.size	print_board, .Lfunc_end2-print_board
	.cfi_endproc

	.globl	place_piece
	.p2align	4, 0x90
	.type	place_piece,@function
place_piece:                            # @place_piece
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movl	%edi, %ecx
	cmpl	$7, %ecx
	jb	.LBB3_2
# BB#1:
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movl	$1, %ebx
	jmp	.LBB3_8
.LBB3_2:
	movslq	%ecx, %rcx
	leaq	(,%rcx,8), %rax
	subq	%rcx, %rax
	movsbq	6(%rdx,%rax), %rcx
	cmpq	$5, %rcx
	movl	$1, %ebx
	jg	.LBB3_8
# BB#3:
	cmpl	$1, %esi
	je	.LBB3_4
# BB#5:
	cmpl	$2, %esi
	jne	.LBB3_9
# BB#6:
	movb	$120, %bl
	jmp	.LBB3_7
.LBB3_4:
	movb	$111, %bl
.LBB3_7:
	leaq	6(%rdx,%rax), %rsi
	addq	%rax, %rdx
	movb	%bl, (%rcx,%rdx)
	incb	(%rsi)
	xorl	%ebx, %ebx
	jmp	.LBB3_8
.LBB3_9:
	movl	$.Lstr.1, %edi
	callq	puts
.LBB3_8:
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	place_piece, .Lfunc_end3-place_piece
	.cfi_endproc

	.globl	board_full
	.p2align	4, 0x90
	.type	board_full,@function
board_full:                             # @board_full
	.cfi_startproc
# BB#0:
	movsbl	6(%rdi), %eax
	movsbl	13(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	20(%rdi), %eax
	addl	%ecx, %eax
	movsbl	27(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	34(%rdi), %eax
	addl	%ecx, %eax
	movsbl	41(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	48(%rdi), %edx
	addl	%ecx, %edx
	xorl	%eax, %eax
	cmpl	$42, %edx
	sete	%al
	retq
.Lfunc_end4:
	.size	board_full, .Lfunc_end4-board_full
	.cfi_endproc

	.globl	find_winner_p
	.p2align	4, 0x90
	.type	find_winner_p,@function
find_winner_p:                          # @find_winner_p
	.cfi_startproc
# BB#0:
	movsbl	6(%rdi), %eax
	movsbl	13(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	20(%rdi), %eax
	addl	%ecx, %eax
	movsbl	27(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	34(%rdi), %eax
	addl	%ecx, %eax
	movsbl	41(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	48(%rdi), %edx
	addl	%ecx, %edx
	movl	$2, %eax
	cmpl	$42, %edx
	je	.LBB5_70
# BB#1:                                 # %.preheader70.preheader
	xorl	%eax, %eax
	cmpb	$111, (%rdi)
	sete	%al
	leaq	2(%rax), %rcx
	cmpb	$111, 7(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$4, %rax
	cmpb	$111, 14(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$8, %rcx
	cmpb	$111, 21(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$16, %rax
	cmpb	$111, 28(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$32, %rcx
	cmpb	$111, 35(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$64, %rax
	cmpb	$111, 42(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$128, %rcx
	cmpb	$111, 1(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$256, %rax              # imm = 0x100
	cmpb	$111, 8(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$512, %rcx              # imm = 0x200
	cmpb	$111, 15(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1024, %rax             # imm = 0x400
	cmpb	$111, 22(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$2048, %rcx             # imm = 0x800
	cmpb	$111, 29(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$4096, %rax             # imm = 0x1000
	cmpb	$111, 36(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$8192, %rcx             # imm = 0x2000
	cmpb	$111, 43(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$16384, %rax            # imm = 0x4000
	cmpb	$111, 2(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$32768, %rcx            # imm = 0x8000
	cmpb	$111, 9(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$65536, %rax            # imm = 0x10000
	cmpb	$111, 16(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$131072, %rcx           # imm = 0x20000
	cmpb	$111, 23(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$262144, %rax           # imm = 0x40000
	cmpb	$111, 30(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$524288, %rcx           # imm = 0x80000
	cmpb	$111, 37(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1048576, %rax          # imm = 0x100000
	cmpb	$111, 44(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$2097152, %rcx          # imm = 0x200000
	cmpb	$111, 3(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$4194304, %rax          # imm = 0x400000
	cmpb	$111, 10(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$8388608, %rcx          # imm = 0x800000
	cmpb	$111, 17(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$16777216, %rax         # imm = 0x1000000
	cmpb	$111, 24(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$33554432, %rcx         # imm = 0x2000000
	cmpb	$111, 31(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$67108864, %rax         # imm = 0x4000000
	cmpb	$111, 38(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$134217728, %rcx        # imm = 0x8000000
	cmpb	$111, 45(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$268435456, %rax        # imm = 0x10000000
	cmpb	$111, 4(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$536870912, %rcx        # imm = 0x20000000
	cmpb	$111, 11(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1073741824, %rax       # imm = 0x40000000
	cmpb	$111, 18(%rdi)
	cmovneq	%rcx, %rax
	movl	$2147483648, %ecx       # imm = 0x80000000
	orq	%rax, %rcx
	cmpb	$111, 25(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$4294967296, %rax       # imm = 0x100000000
	orq	%rcx, %rax
	cmpb	$111, 32(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$8589934592, %rcx       # imm = 0x200000000
	orq	%rax, %rcx
	cmpb	$111, 39(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$17179869184, %rax      # imm = 0x400000000
	orq	%rcx, %rax
	cmpb	$111, 46(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$34359738368, %rcx      # imm = 0x800000000
	orq	%rax, %rcx
	cmpb	$111, 5(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$68719476736, %rax      # imm = 0x1000000000
	orq	%rcx, %rax
	cmpb	$111, 12(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$137438953472, %rcx     # imm = 0x2000000000
	orq	%rax, %rcx
	cmpb	$111, 19(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$274877906944, %rax     # imm = 0x4000000000
	orq	%rcx, %rax
	cmpb	$111, 26(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$549755813888, %rcx     # imm = 0x8000000000
	orq	%rax, %rcx
	cmpb	$111, 33(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$1099511627776, %rax    # imm = 0x10000000000
	orq	%rcx, %rax
	cmpb	$111, 40(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$2199023255552, %rcx    # imm = 0x20000000000
	orq	%rax, %rcx
	cmpb	$111, 47(%rdi)
	cmovneq	%rax, %rcx
	movq	C4VERT(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	movl	$1, %eax
	cmpq	%rdx, %rsi
	je	.LBB5_70
# BB#2:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#3:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#4:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#5:
	movq	%rdx, %rsi
	shlq	$4, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#6:
	movq	%rdx, %rsi
	shlq	$5, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#7:
	movq	%rdx, %rsi
	shlq	$6, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#8:
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#9:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#10:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#11:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#12:
	movq	%rdx, %rsi
	shlq	$11, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#13:
	movq	%rdx, %rsi
	shlq	$12, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#14:
	movq	%rdx, %rsi
	shlq	$13, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#15:
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#16:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#17:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#18:
	movq	%rdx, %rsi
	shlq	$17, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#19:
	movq	%rdx, %rsi
	shlq	$18, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#20:
	movq	%rdx, %rsi
	shlq	$19, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#21:
	shlq	$20, %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB5_70
# BB#22:                                # %.preheader6799
	movq	C4HORIZ(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB5_70
# BB#23:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#24:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#25:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#26:                                # %.preheader65.193
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#27:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#28:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#29:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#30:                                # %.preheader65.294
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#31:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#32:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#33:
	movq	%rdx, %rsi
	shlq	$17, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#34:                                # %.preheader65.395
	movq	%rdx, %rsi
	shlq	$21, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#35:
	movq	%rdx, %rsi
	shlq	$22, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#36:
	movq	%rdx, %rsi
	shlq	$23, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#37:
	movq	%rdx, %rsi
	shlq	$24, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#38:                                # %.preheader65.496
	movq	%rdx, %rsi
	shlq	$28, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#39:
	movq	%rdx, %rsi
	shlq	$29, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#40:
	movq	%rdx, %rsi
	shlq	$30, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#41:
	movq	%rdx, %rsi
	shlq	$31, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#42:                                # %.preheader65.597
	movq	%rdx, %rsi
	shlq	$35, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#43:
	movq	%rdx, %rsi
	shlq	$36, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#44:
	movq	%rdx, %rsi
	shlq	$37, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#45:
	shlq	$38, %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB5_70
# BB#46:                                # %.preheader6498
	movq	C4UP_R(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB5_70
# BB#47:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#48:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#49:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#50:                                # %.preheader62.190
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#51:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#52:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#53:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#54:                                # %.preheader62.291
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#55:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#56:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#57:
	shlq	$17, %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB5_70
# BB#58:                                # %.preheader6092
	movq	C4UP_L(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB5_70
# BB#59:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#60:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#61:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#62:                                # %.preheader.187
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#63:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#64:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#65:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#66:                                # %.preheader.288
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#67:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#68:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB5_70
# BB#69:
	shlq	$17, %rdx
	andq	%rdx, %rcx
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	sete	%al
.LBB5_70:                               # %.loopexit
	retq
.Lfunc_end5:
	.size	find_winner_p, .Lfunc_end5-find_winner_p
	.cfi_endproc

	.globl	find_winner_c
	.p2align	4, 0x90
	.type	find_winner_c,@function
find_winner_c:                          # @find_winner_c
	.cfi_startproc
# BB#0:
	movsbl	6(%rdi), %eax
	movsbl	13(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	20(%rdi), %eax
	addl	%ecx, %eax
	movsbl	27(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	34(%rdi), %eax
	addl	%ecx, %eax
	movsbl	41(%rdi), %ecx
	addl	%eax, %ecx
	movsbl	48(%rdi), %edx
	addl	%ecx, %edx
	movl	$2, %eax
	cmpl	$42, %edx
	je	.LBB6_70
# BB#1:                                 # %.preheader70.preheader
	xorl	%eax, %eax
	cmpb	$120, (%rdi)
	sete	%al
	leaq	2(%rax), %rcx
	cmpb	$120, 7(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$4, %rax
	cmpb	$120, 14(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$8, %rcx
	cmpb	$120, 21(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$16, %rax
	cmpb	$120, 28(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$32, %rcx
	cmpb	$120, 35(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$64, %rax
	cmpb	$120, 42(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$128, %rcx
	cmpb	$120, 1(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$256, %rax              # imm = 0x100
	cmpb	$120, 8(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$512, %rcx              # imm = 0x200
	cmpb	$120, 15(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1024, %rax             # imm = 0x400
	cmpb	$120, 22(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$2048, %rcx             # imm = 0x800
	cmpb	$120, 29(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$4096, %rax             # imm = 0x1000
	cmpb	$120, 36(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$8192, %rcx             # imm = 0x2000
	cmpb	$120, 43(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$16384, %rax            # imm = 0x4000
	cmpb	$120, 2(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$32768, %rcx            # imm = 0x8000
	cmpb	$120, 9(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$65536, %rax            # imm = 0x10000
	cmpb	$120, 16(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$131072, %rcx           # imm = 0x20000
	cmpb	$120, 23(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$262144, %rax           # imm = 0x40000
	cmpb	$120, 30(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$524288, %rcx           # imm = 0x80000
	cmpb	$120, 37(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1048576, %rax          # imm = 0x100000
	cmpb	$120, 44(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$2097152, %rcx          # imm = 0x200000
	cmpb	$120, 3(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$4194304, %rax          # imm = 0x400000
	cmpb	$120, 10(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$8388608, %rcx          # imm = 0x800000
	cmpb	$120, 17(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$16777216, %rax         # imm = 0x1000000
	cmpb	$120, 24(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$33554432, %rcx         # imm = 0x2000000
	cmpb	$120, 31(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$67108864, %rax         # imm = 0x4000000
	cmpb	$120, 38(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$134217728, %rcx        # imm = 0x8000000
	cmpb	$120, 45(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$268435456, %rax        # imm = 0x10000000
	cmpb	$120, 4(%rdi)
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$536870912, %rcx        # imm = 0x20000000
	cmpb	$120, 11(%rdi)
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1073741824, %rax       # imm = 0x40000000
	cmpb	$120, 18(%rdi)
	cmovneq	%rcx, %rax
	movl	$2147483648, %ecx       # imm = 0x80000000
	orq	%rax, %rcx
	cmpb	$120, 25(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$4294967296, %rax       # imm = 0x100000000
	orq	%rcx, %rax
	cmpb	$120, 32(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$8589934592, %rcx       # imm = 0x200000000
	orq	%rax, %rcx
	cmpb	$120, 39(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$17179869184, %rax      # imm = 0x400000000
	orq	%rcx, %rax
	cmpb	$120, 46(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$34359738368, %rcx      # imm = 0x800000000
	orq	%rax, %rcx
	cmpb	$120, 5(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$68719476736, %rax      # imm = 0x1000000000
	orq	%rcx, %rax
	cmpb	$120, 12(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$137438953472, %rcx     # imm = 0x2000000000
	orq	%rax, %rcx
	cmpb	$120, 19(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$274877906944, %rax     # imm = 0x4000000000
	orq	%rcx, %rax
	cmpb	$120, 26(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$549755813888, %rcx     # imm = 0x8000000000
	orq	%rax, %rcx
	cmpb	$120, 33(%rdi)
	cmovneq	%rax, %rcx
	movabsq	$1099511627776, %rax    # imm = 0x10000000000
	orq	%rcx, %rax
	cmpb	$120, 40(%rdi)
	cmovneq	%rcx, %rax
	movabsq	$2199023255552, %rcx    # imm = 0x20000000000
	orq	%rax, %rcx
	cmpb	$120, 47(%rdi)
	cmovneq	%rax, %rcx
	movq	C4VERT(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	movl	$1, %eax
	cmpq	%rdx, %rsi
	je	.LBB6_70
# BB#2:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#3:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#4:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#5:
	movq	%rdx, %rsi
	shlq	$4, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#6:
	movq	%rdx, %rsi
	shlq	$5, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#7:
	movq	%rdx, %rsi
	shlq	$6, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#8:
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#9:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#10:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#11:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#12:
	movq	%rdx, %rsi
	shlq	$11, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#13:
	movq	%rdx, %rsi
	shlq	$12, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#14:
	movq	%rdx, %rsi
	shlq	$13, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#15:
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#16:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#17:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#18:
	movq	%rdx, %rsi
	shlq	$17, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#19:
	movq	%rdx, %rsi
	shlq	$18, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#20:
	movq	%rdx, %rsi
	shlq	$19, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#21:
	shlq	$20, %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB6_70
# BB#22:                                # %.preheader6799
	movq	C4HORIZ(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB6_70
# BB#23:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#24:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#25:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#26:                                # %.preheader65.193
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#27:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#28:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#29:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#30:                                # %.preheader65.294
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#31:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#32:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#33:
	movq	%rdx, %rsi
	shlq	$17, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#34:                                # %.preheader65.395
	movq	%rdx, %rsi
	shlq	$21, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#35:
	movq	%rdx, %rsi
	shlq	$22, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#36:
	movq	%rdx, %rsi
	shlq	$23, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#37:
	movq	%rdx, %rsi
	shlq	$24, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#38:                                # %.preheader65.496
	movq	%rdx, %rsi
	shlq	$28, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#39:
	movq	%rdx, %rsi
	shlq	$29, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#40:
	movq	%rdx, %rsi
	shlq	$30, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#41:
	movq	%rdx, %rsi
	shlq	$31, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#42:                                # %.preheader65.597
	movq	%rdx, %rsi
	shlq	$35, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#43:
	movq	%rdx, %rsi
	shlq	$36, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#44:
	movq	%rdx, %rsi
	shlq	$37, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#45:
	shlq	$38, %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB6_70
# BB#46:                                # %.preheader6498
	movq	C4UP_R(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB6_70
# BB#47:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#48:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#49:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#50:                                # %.preheader62.190
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#51:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#52:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#53:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#54:                                # %.preheader62.291
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#55:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#56:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#57:
	shlq	$17, %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB6_70
# BB#58:                                # %.preheader6092
	movq	C4UP_L(%rip), %rdx
	movq	%rdx, %rsi
	andq	%rcx, %rsi
	cmpq	%rdx, %rsi
	je	.LBB6_70
# BB#59:
	leaq	(%rdx,%rdx), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#60:
	leaq	(,%rdx,4), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#61:
	leaq	(,%rdx,8), %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#62:                                # %.preheader.187
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#63:
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#64:
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#65:
	movq	%rdx, %rsi
	shlq	$10, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#66:                                # %.preheader.288
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#67:
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#68:
	movq	%rdx, %rsi
	shlq	$16, %rsi
	movq	%rsi, %rdi
	andq	%rcx, %rdi
	cmpq	%rsi, %rdi
	je	.LBB6_70
# BB#69:
	shlq	$17, %rdx
	andq	%rdx, %rcx
	xorl	%eax, %eax
	cmpq	%rdx, %rcx
	sete	%al
.LBB6_70:                               # %.loopexit
	retq
.Lfunc_end6:
	.size	find_winner_c, .Lfunc_end6-find_winner_c
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_0:
	.long	1092616192              # float 10
.LCPI7_2:
	.long	1148846080              # float 1000
.LCPI7_3:
	.long	1101004800              # float 20
.LCPI7_4:
	.long	1084227584              # float 5
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	2
.LCPI7_1:
	.long	1065353216              # float 1
	.long	3212836864              # float -1
	.text
	.globl	value
	.p2align	4, 0x90
	.type	value,@function
value:                                  # @value
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$472, %rsp              # imm = 0x1D8
.Lcfi14:
	.cfi_def_cfa_offset 528
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%rsi, -112(%rsp)        # 8-byte Spill
	movq	%rdi, -120(%rsp)        # 8-byte Spill
	cvtsi2ssl	off(%rip), %xmm0
	divss	.LCPI7_0(%rip), %xmm0
	movq	C4UP_R(%rip), %rax
	movq	C4UP_L(%rip), %r9
	movq	%rax, %rdx
	shlq	$6, %rdx
	movq	%rdx, 448(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	shlq	$7, %rdx
	movq	%rdx, 440(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	shlq	$8, %rdx
	movq	%rdx, 432(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	shlq	$9, %rdx
	movq	%rdx, 424(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	shlq	$12, %rdx
	movq	%rdx, 416(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	shlq	$13, %rdx
	movq	%rdx, 408(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	shlq	$14, %rdx
	movq	%rdx, 400(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	shlq	$15, %rdx
	movq	%rdx, 392(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$6, %rdx
	movq	%rdx, 384(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$7, %rdx
	movq	%rdx, 376(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$8, %rdx
	movq	%rdx, 368(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$9, %rdx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$12, %rdx
	movq	%rdx, 352(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$13, %rdx
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$14, %rdx
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	movq	%r9, %rdx
	shlq	$15, %rdx
	movq	%rdx, 328(%rsp)         # 8-byte Spill
	movq	C3UP_R(%rip), %rdx
	movq	%rdx, %rsi
	shlq	$6, %rsi
	movq	%rsi, 312(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$7, %rsi
	movq	%rsi, 304(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$8, %rsi
	movq	%rsi, 296(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$9, %rsi
	movq	%rsi, 288(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$12, %rsi
	movq	%rsi, 280(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$13, %rsi
	movq	%rsi, 272(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$14, %rsi
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	movq	%rdx, %rsi
	shlq	$15, %rsi
	movq	%rsi, 256(%rsp)         # 8-byte Spill
	movq	C3UP_L(%rip), %rsi
	movq	%rsi, %rdi
	shlq	$6, %rdi
	movq	%rdi, 240(%rsp)         # 8-byte Spill
	movq	%rsi, %rdi
	shlq	$7, %rdi
	movq	%rdi, 232(%rsp)         # 8-byte Spill
	movq	%rsi, %rdi
	shlq	$8, %rdi
	movq	%rdi, 224(%rsp)         # 8-byte Spill
	movq	%rsi, %rdi
	shlq	$9, %rdi
	movq	%rdi, 216(%rsp)         # 8-byte Spill
	movq	%rsi, %rdi
	shlq	$12, %rdi
	movq	%rdi, 208(%rsp)         # 8-byte Spill
	movq	%rsi, %rdi
	shlq	$13, %rdi
	movq	%rdi, 200(%rsp)         # 8-byte Spill
	movq	%rsi, %rdi
	shlq	$14, %rdi
	movq	%rdi, 192(%rsp)         # 8-byte Spill
	movq	%rsi, %rdi
	shlq	$15, %rdi
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	movq	C2UP_R(%rip), %rdi
	movq	%rdi, %rbp
	shlq	$6, %rbp
	movq	%rbp, 168(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	shlq	$7, %rbp
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	shlq	$8, %rbp
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	shlq	$9, %rbp
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	shlq	$12, %rbp
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	shlq	$13, %rbp
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	shlq	$14, %rbp
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	shlq	$15, %rbp
	movq	%rbp, 112(%rsp)         # 8-byte Spill
	movq	C2UP_L(%rip), %rbx
	movq	%rbx, %rbp
	shlq	$6, %rbp
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	shlq	$7, %rbp
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	shlq	$8, %rbp
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	shlq	$9, %rbp
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	shlq	$12, %rbp
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	shlq	$13, %rbp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	shlq	$14, %rbp
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%rbx, %rbp
	shlq	$15, %rbp
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movss	.LCPI7_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI7_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI7_4(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movq	C4VERT(%rip), %r14
	movq	C4HORIZ(%rip), %r15
	movq	C3VERT(%rip), %rbp
	movq	C3HORIZ(%rip), %r10
	movq	C2VERT(%rip), %r8
	movq	C2HORIZ(%rip), %r11
	leaq	(%rax,%rax), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	(,%rax,4), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 464(%rsp)         # 8-byte Spill
	leaq	(,%rax,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	(%r9,%r9), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	(,%r9,4), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r9, 456(%rsp)          # 8-byte Spill
	leaq	(,%r9,8), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rdx), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	leaq	(,%rdx,4), %rax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	%rdx, 320(%rsp)         # 8-byte Spill
	leaq	(,%rdx,8), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	leaq	(%rsi,%rsi), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	(,%rsi,4), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	movq	%rsi, 248(%rsp)         # 8-byte Spill
	leaq	(,%rsi,8), %rax
	movq	%rax, -56(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rdi), %rax
	movq	%rax, -64(%rsp)         # 8-byte Spill
	leaq	(,%rdi,4), %rax
	movq	%rax, -72(%rsp)         # 8-byte Spill
	movq	%rdi, 176(%rsp)         # 8-byte Spill
	leaq	(,%rdi,8), %rax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	leaq	(%rbx,%rbx), %rax
	movq	%rax, -88(%rsp)         # 8-byte Spill
	leaq	(,%rbx,4), %rax
	movq	%rax, -96(%rsp)         # 8-byte Spill
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	leaq	(,%rbx,8), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	xorl	%eax, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_2 Depth 2
                                        #     Child Loop BB7_6 Depth 2
                                        #     Child Loop BB7_64 Depth 2
                                        #     Child Loop BB7_69 Depth 2
                                        #     Child Loop BB7_129 Depth 2
                                        #     Child Loop BB7_134 Depth 2
	xorl	%ecx, %ecx
	movl	%edx, -124(%rsp)        # 4-byte Spill
	testl	%edx, %edx
	sete	%cl
	movq	-112(%rsp), %r9         # 8-byte Reload
	cmoveq	-120(%rsp), %r9         # 8-byte Folded Reload
	movss	.LCPI7_1(,%rcx,4), %xmm4 # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	movaps	%xmm4, %xmm5
	mulss	%xmm1, %xmm5
	xorl	%ecx, %ecx
	jmp	.LBB7_2
	.p2align	4, 0x90
.LBB7_199:                              #   in Loop: Header=BB7_2 Depth=2
	incq	%rcx
.LBB7_2:                                #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdx
	shlq	%cl, %rdx
	movq	%rdx, %rbx
	andq	%r9, %rbx
	cmpq	%rdx, %rbx
	jne	.LBB7_4
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=2
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_4:                                #   in Loop: Header=BB7_2 Depth=2
	incq	%rcx
	cmpq	$21, %rcx
	je	.LBB7_5
# BB#197:                               #   in Loop: Header=BB7_2 Depth=2
	movq	%r14, %rdx
	shlq	%cl, %rdx
	movq	%rdx, %rsi
	andq	%r9, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB7_199
# BB#198:                               #   in Loop: Header=BB7_2 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
	jmp	.LBB7_199
	.p2align	4, 0x90
.LBB7_5:                                # %.preheader176.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	$3, %edx
	.p2align	4, 0x90
.LBB7_6:                                # %.preheader176
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-3(%rdx), %ecx
	movq	%r15, %rbx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbx
	movq	%rbx, %rcx
	andq	%r9, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB7_8
# BB#7:                                 #   in Loop: Header=BB7_6 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_8:                                #   in Loop: Header=BB7_6 Depth=2
	leal	-2(%rdx), %ecx
	movq	%r15, %rbx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbx
	movq	%rbx, %rcx
	andq	%r9, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB7_10
# BB#9:                                 #   in Loop: Header=BB7_6 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_10:                               #   in Loop: Header=BB7_6 Depth=2
	leal	-1(%rdx), %ecx
	movq	%r15, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_12
# BB#11:                                #   in Loop: Header=BB7_6 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_12:                               #   in Loop: Header=BB7_6 Depth=2
	movq	%r15, %rsi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_14
# BB#13:                                #   in Loop: Header=BB7_6 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_14:                               #   in Loop: Header=BB7_6 Depth=2
	addq	$6, %rdx
	cmpq	$39, %rdx
	jne	.LBB7_6
# BB#15:                                # %.preheader186
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	464(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_17
# BB#16:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_17:                               #   in Loop: Header=BB7_1 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_19
# BB#18:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_19:                               #   in Loop: Header=BB7_1 Depth=1
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_21
# BB#20:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_21:                               #   in Loop: Header=BB7_1 Depth=1
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_23
# BB#22:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_23:                               # %.preheader175.1246
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	448(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_25
# BB#24:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_25:                               #   in Loop: Header=BB7_1 Depth=1
	movq	440(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_27
# BB#26:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_27:                               #   in Loop: Header=BB7_1 Depth=1
	movq	432(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_29
# BB#28:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_29:                               #   in Loop: Header=BB7_1 Depth=1
	movq	424(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_31
# BB#30:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_31:                               # %.preheader175.2247
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	416(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_33
# BB#32:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_33:                               #   in Loop: Header=BB7_1 Depth=1
	movq	408(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_35
# BB#34:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_35:                               #   in Loop: Header=BB7_1 Depth=1
	movq	400(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_37
# BB#36:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_37:                               #   in Loop: Header=BB7_1 Depth=1
	movq	392(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_39
# BB#38:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_39:                               # %.preheader185248
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	456(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_41
# BB#40:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_41:                               #   in Loop: Header=BB7_1 Depth=1
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_43
# BB#42:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_43:                               #   in Loop: Header=BB7_1 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_45
# BB#44:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_45:                               #   in Loop: Header=BB7_1 Depth=1
	movq	-8(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_47
# BB#46:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_47:                               # %.preheader174.1257
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	384(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_49
# BB#48:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_49:                               #   in Loop: Header=BB7_1 Depth=1
	movq	376(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_51
# BB#50:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_51:                               #   in Loop: Header=BB7_1 Depth=1
	movq	368(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_53
# BB#52:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_53:                               #   in Loop: Header=BB7_1 Depth=1
	movq	360(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_55
# BB#54:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_55:                               # %.preheader174.2258
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	352(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_57
# BB#56:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_57:                               #   in Loop: Header=BB7_1 Depth=1
	movq	344(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_59
# BB#58:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_59:                               #   in Loop: Header=BB7_1 Depth=1
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_61
# BB#60:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_61:                               #   in Loop: Header=BB7_1 Depth=1
	movq	328(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_63
# BB#62:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm6, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_63:                               # %.preheader184259
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	-124(%rsp), %ecx        # 4-byte Reload
	testl	%ecx, %ecx
	movq	-120(%rsp), %rbx        # 8-byte Reload
	cmoveq	-112(%rsp), %rbx        # 8-byte Folded Reload
	movaps	%xmm4, %xmm5
	mulss	%xmm2, %xmm5
	movl	$1, %edx
	jmp	.LBB7_64
	.p2align	4, 0x90
.LBB7_203:                              #   in Loop: Header=BB7_64 Depth=2
	addq	$2, %rdx
.LBB7_64:                               #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rcx
	movq	%rbp, %r13
	shlq	%cl, %r13
	movq	%r13, %r12
	andq	%r9, %r12
	cmpq	%r13, %r12
	jne	.LBB7_67
# BB#65:                                #   in Loop: Header=BB7_64 Depth=2
	movq	%r14, %rsi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	testq	%rbx, %rsi
	jne	.LBB7_67
# BB#66:                                #   in Loop: Header=BB7_64 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
	.p2align	4, 0x90
.LBB7_67:                               #   in Loop: Header=BB7_64 Depth=2
	cmpq	$21, %rdx
	je	.LBB7_68
# BB#200:                               #   in Loop: Header=BB7_64 Depth=2
	movq	%rbp, %rsi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_203
# BB#201:                               #   in Loop: Header=BB7_64 Depth=2
	movq	%r14, %rsi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	testq	%rbx, %rsi
	jne	.LBB7_203
# BB#202:                               #   in Loop: Header=BB7_64 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
	jmp	.LBB7_203
	.p2align	4, 0x90
.LBB7_68:                               # %.preheader173.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	$4, %edx
	.p2align	4, 0x90
.LBB7_69:                               # %.preheader173
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-4(%rdx), %ecx
	movq	%r10, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_71
# BB#70:                                #   in Loop: Header=BB7_69 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_71:                               #   in Loop: Header=BB7_69 Depth=2
	leal	-3(%rdx), %ecx
	movq	%r10, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_73
# BB#72:                                #   in Loop: Header=BB7_69 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_73:                               #   in Loop: Header=BB7_69 Depth=2
	leal	-2(%rdx), %ecx
	movq	%r10, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_75
# BB#74:                                #   in Loop: Header=BB7_69 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_75:                               #   in Loop: Header=BB7_69 Depth=2
	leal	-1(%rdx), %ecx
	movq	%r10, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_77
# BB#76:                                #   in Loop: Header=BB7_69 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_77:                               #   in Loop: Header=BB7_69 Depth=2
	movq	%r10, %rsi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_79
# BB#78:                                #   in Loop: Header=BB7_69 Depth=2
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_79:                               #   in Loop: Header=BB7_69 Depth=2
	addq	$6, %rdx
	cmpq	$40, %rdx
	jne	.LBB7_69
# BB#80:                                # %.preheader182
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	320(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_82
# BB#81:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_82:                               #   in Loop: Header=BB7_1 Depth=1
	movq	-16(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_84
# BB#83:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_84:                               #   in Loop: Header=BB7_1 Depth=1
	movq	-24(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_86
# BB#85:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_86:                               #   in Loop: Header=BB7_1 Depth=1
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_88
# BB#87:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_88:                               # %.preheader172.1277
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	312(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_90
# BB#89:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_90:                               #   in Loop: Header=BB7_1 Depth=1
	movq	304(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_92
# BB#91:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_92:                               #   in Loop: Header=BB7_1 Depth=1
	movq	296(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_94
# BB#93:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_94:                               #   in Loop: Header=BB7_1 Depth=1
	movq	288(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_96
# BB#95:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_96:                               # %.preheader172.2278
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_98
# BB#97:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_98:                               #   in Loop: Header=BB7_1 Depth=1
	movq	272(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_100
# BB#99:                                #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_100:                              #   in Loop: Header=BB7_1 Depth=1
	movq	264(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_102
# BB#101:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_102:                              #   in Loop: Header=BB7_1 Depth=1
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_104
# BB#103:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_104:                              # %.preheader181279
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	248(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_106
# BB#105:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_106:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_108
# BB#107:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_108:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-48(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_110
# BB#109:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_110:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-56(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_112
# BB#111:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_112:                              # %.preheader171.1288
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	240(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_114
# BB#113:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_114:                              #   in Loop: Header=BB7_1 Depth=1
	movq	232(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_116
# BB#115:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_116:                              #   in Loop: Header=BB7_1 Depth=1
	movq	224(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_118
# BB#117:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_118:                              #   in Loop: Header=BB7_1 Depth=1
	movq	216(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_120
# BB#119:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_120:                              # %.preheader171.2289
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	208(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_122
# BB#121:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_122:                              #   in Loop: Header=BB7_1 Depth=1
	movq	200(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_124
# BB#123:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_124:                              #   in Loop: Header=BB7_1 Depth=1
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_126
# BB#125:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm5, %xmm6
	cvttss2si	%xmm6, %eax
.LBB7_126:                              #   in Loop: Header=BB7_1 Depth=1
	movq	184(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_128
# BB#127:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm6, %xmm6
	cvtsi2ssl	%eax, %xmm6
	addss	%xmm6, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_128:                              # %.preheader180290
                                        #   in Loop: Header=BB7_1 Depth=1
	mulss	%xmm3, %xmm4
	movl	$1, %edx
	jmp	.LBB7_129
	.p2align	4, 0x90
.LBB7_207:                              #   in Loop: Header=BB7_129 Depth=2
	addq	$2, %rdx
.LBB7_129:                              #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	-1(%rdx), %rcx
	movq	%r8, %rdi
	shlq	%cl, %rdi
	movq	%rdi, %rsi
	andq	%r9, %rsi
	cmpq	%rdi, %rsi
	jne	.LBB7_132
# BB#130:                               #   in Loop: Header=BB7_129 Depth=2
	movq	%r14, %rsi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rsi
	testq	%rbx, %rsi
	jne	.LBB7_132
# BB#131:                               #   in Loop: Header=BB7_129 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
	.p2align	4, 0x90
.LBB7_132:                              #   in Loop: Header=BB7_129 Depth=2
	cmpq	$21, %rdx
	je	.LBB7_133
# BB#204:                               #   in Loop: Header=BB7_129 Depth=2
	movq	%r8, %rsi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_207
# BB#205:                               #   in Loop: Header=BB7_129 Depth=2
	movq	%r14, %rsi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	testq	%rbx, %rsi
	jne	.LBB7_207
# BB#206:                               #   in Loop: Header=BB7_129 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
	jmp	.LBB7_207
	.p2align	4, 0x90
.LBB7_133:                              # %.preheader170.preheader
                                        #   in Loop: Header=BB7_1 Depth=1
	movl	$5, %edx
	.p2align	4, 0x90
.LBB7_134:                              # %.preheader170
                                        #   Parent Loop BB7_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-5(%rdx), %ecx
	movq	%r11, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_136
# BB#135:                               #   in Loop: Header=BB7_134 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_136:                              #   in Loop: Header=BB7_134 Depth=2
	leal	-4(%rdx), %ecx
	movq	%r11, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_138
# BB#137:                               #   in Loop: Header=BB7_134 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_138:                              #   in Loop: Header=BB7_134 Depth=2
	leal	-3(%rdx), %ecx
	movq	%r11, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_140
# BB#139:                               #   in Loop: Header=BB7_134 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_140:                              #   in Loop: Header=BB7_134 Depth=2
	leal	-2(%rdx), %ecx
	movq	%r11, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_142
# BB#141:                               #   in Loop: Header=BB7_134 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_142:                              #   in Loop: Header=BB7_134 Depth=2
	leal	-1(%rdx), %ecx
	movq	%r11, %rsi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_144
# BB#143:                               #   in Loop: Header=BB7_134 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_144:                              #   in Loop: Header=BB7_134 Depth=2
	movq	%r11, %rsi
	movl	%edx, %ecx
	shlq	%cl, %rsi
	movq	%rsi, %rcx
	andq	%r9, %rcx
	cmpq	%rsi, %rcx
	jne	.LBB7_146
# BB#145:                               #   in Loop: Header=BB7_134 Depth=2
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_146:                              #   in Loop: Header=BB7_134 Depth=2
	addq	$6, %rdx
	cmpq	$41, %rdx
	jne	.LBB7_134
# BB#147:                               # %.preheader178
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_149
# BB#148:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_149:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-64(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_151
# BB#150:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_151:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-72(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_153
# BB#152:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_153:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-80(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_155
# BB#154:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_155:                              # %.preheader169.1308
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_157
# BB#156:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_157:                              #   in Loop: Header=BB7_1 Depth=1
	movq	160(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_159
# BB#158:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_159:                              #   in Loop: Header=BB7_1 Depth=1
	movq	152(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_161
# BB#160:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_161:                              #   in Loop: Header=BB7_1 Depth=1
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_163
# BB#162:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_163:                              # %.preheader169.2309
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_165
# BB#164:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_165:                              #   in Loop: Header=BB7_1 Depth=1
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_167
# BB#166:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_167:                              #   in Loop: Header=BB7_1 Depth=1
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_169
# BB#168:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_169:                              #   in Loop: Header=BB7_1 Depth=1
	movq	112(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_171
# BB#170:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_171:                              # %.preheader177310
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_173
# BB#172:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_173:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-88(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_175
# BB#174:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_175:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-96(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_177
# BB#176:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_177:                              #   in Loop: Header=BB7_1 Depth=1
	movq	-104(%rsp), %rdx        # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_179
# BB#178:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_179:                              # %.preheader.1319
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	96(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_181
# BB#180:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_181:                              #   in Loop: Header=BB7_1 Depth=1
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_183
# BB#182:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_183:                              #   in Loop: Header=BB7_1 Depth=1
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_185
# BB#184:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_185:                              #   in Loop: Header=BB7_1 Depth=1
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_187
# BB#186:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_187:                              # %.preheader.2320
                                        #   in Loop: Header=BB7_1 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_189
# BB#188:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_189:                              #   in Loop: Header=BB7_1 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_191
# BB#190:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_191:                              #   in Loop: Header=BB7_1 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, %rcx
	andq	%r9, %rcx
	cmpq	%rdx, %rcx
	jne	.LBB7_193
# BB#192:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm4, %xmm5
	cvttss2si	%xmm5, %eax
.LBB7_193:                              #   in Loop: Header=BB7_1 Depth=1
	movl	-124(%rsp), %edx        # 4-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	andq	%rcx, %r9
	cmpq	%rcx, %r9
	jne	.LBB7_195
# BB#194:                               #   in Loop: Header=BB7_1 Depth=1
	xorps	%xmm5, %xmm5
	cvtsi2ssl	%eax, %xmm5
	addss	%xmm5, %xmm4
	cvttss2si	%xmm4, %eax
.LBB7_195:                              #   in Loop: Header=BB7_1 Depth=1
	incl	%edx
	cmpl	$2, %edx
	jne	.LBB7_1
# BB#196:
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	addq	$472, %rsp              # imm = 0x1D8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	value, .Lfunc_end7-value
	.cfi_endproc

	.globl	think
	.p2align	4, 0x90
	.type	think,@function
think:                                  # @think
	.cfi_startproc
# BB#0:                                 # %.preheader100
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi27:
	.cfi_def_cfa_offset 144
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movl	%edx, 60(%rsp)          # 4-byte Spill
	movl	%esi, 44(%rsp)          # 4-byte Spill
	movb	(%rdi), %cl
	movb	%cl, 48(%rsp)           # 1-byte Spill
	movb	1(%rdi), %dl
	movb	%dl, 40(%rsp)           # 1-byte Spill
	xorl	%eax, %eax
	cmpb	$111, %cl
	sete	%al
	movb	7(%rdi), %bl
	movb	%bl, 72(%rsp)           # 1-byte Spill
	leaq	2(%rax), %rcx
	cmpb	$111, %bl
	cmovneq	%rax, %rcx
	movb	14(%rdi), %bl
	movb	%bl, 80(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$4, %rax
	cmpb	$111, %bl
	cmovneq	%rcx, %rax
	movb	21(%rdi), %bl
	movb	%bl, 43(%rsp)           # 1-byte Spill
	movq	%rax, %rcx
	orq	$8, %rcx
	cmpb	$111, %bl
	cmovneq	%rax, %rcx
	movb	28(%rdi), %bl
	movb	%bl, 42(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$16, %rax
	cmpb	$111, %bl
	cmovneq	%rcx, %rax
	movb	35(%rdi), %bl
	movb	%bl, 41(%rsp)           # 1-byte Spill
	movq	%rax, %rcx
	orq	$32, %rcx
	cmpb	$111, %bl
	cmovneq	%rax, %rcx
	movb	42(%rdi), %bl
	movb	%bl, 39(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$64, %rax
	cmpb	$111, %bl
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$128, %rcx
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movb	8(%rdi), %dl
	movb	%dl, 38(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$256, %rax              # imm = 0x100
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movb	15(%rdi), %dl
	movb	%dl, 37(%rsp)           # 1-byte Spill
	movq	%rax, %rcx
	orq	$512, %rcx              # imm = 0x200
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movb	22(%rdi), %dl
	movb	%dl, 36(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$1024, %rax             # imm = 0x400
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movb	29(%rdi), %dl
	movb	%dl, 35(%rsp)           # 1-byte Spill
	movq	%rax, %rcx
	orq	$2048, %rcx             # imm = 0x800
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movb	36(%rdi), %dl
	movb	%dl, 34(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$4096, %rax             # imm = 0x1000
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movb	43(%rdi), %dl
	movb	%dl, 33(%rsp)           # 1-byte Spill
	movq	%rax, %rcx
	orq	$8192, %rcx             # imm = 0x2000
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movb	2(%rdi), %dl
	movb	%dl, 32(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$16384, %rax            # imm = 0x4000
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movb	9(%rdi), %dl
	movb	%dl, 31(%rsp)           # 1-byte Spill
	movq	%rax, %rcx
	orq	$32768, %rcx            # imm = 0x8000
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movb	16(%rdi), %dl
	movb	%dl, 30(%rsp)           # 1-byte Spill
	movq	%rcx, %rax
	orq	$65536, %rax            # imm = 0x10000
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movb	23(%rdi), %dl
	movb	%dl, 29(%rsp)           # 1-byte Spill
	movq	%rax, %rcx
	orq	$131072, %rcx           # imm = 0x20000
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$262144, %rax           # imm = 0x40000
	movb	30(%rdi), %dl
	movb	%dl, 28(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$524288, %rcx           # imm = 0x80000
	movb	37(%rdi), %dl
	movb	%dl, 27(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1048576, %rax          # imm = 0x100000
	movb	44(%rdi), %dl
	movb	%dl, 26(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$2097152, %rcx          # imm = 0x200000
	movb	3(%rdi), %dl
	movb	%dl, 25(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$4194304, %rax          # imm = 0x400000
	movb	10(%rdi), %dl
	movb	%dl, 24(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$8388608, %rcx          # imm = 0x800000
	movb	17(%rdi), %dl
	movb	%dl, 23(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$16777216, %rax         # imm = 0x1000000
	movb	24(%rdi), %dl
	movb	%dl, 22(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$33554432, %rcx         # imm = 0x2000000
	movb	31(%rdi), %dl
	movb	%dl, 21(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$67108864, %rax         # imm = 0x4000000
	movb	38(%rdi), %dl
	movb	%dl, 20(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$134217728, %rcx        # imm = 0x8000000
	movb	45(%rdi), %dl
	movb	%dl, 19(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$268435456, %rax        # imm = 0x10000000
	movb	4(%rdi), %dl
	movb	%dl, 18(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movq	%rax, %rcx
	orq	$536870912, %rcx        # imm = 0x20000000
	movb	11(%rdi), %dl
	movb	%dl, 17(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movq	%rcx, %rax
	orq	$1073741824, %rax       # imm = 0x40000000
	movb	18(%rdi), %dl
	movb	%dl, 16(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movl	$2147483648, %r15d      # imm = 0x80000000
	movq	%rax, %rcx
	orq	%r15, %rcx
	movb	25(%rdi), %dl
	movb	%dl, 15(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movabsq	$4294967296, %r14       # imm = 0x100000000
	movq	%rcx, %rax
	orq	%r14, %rax
	movb	32(%rdi), %dl
	movb	%dl, 14(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movabsq	$8589934592, %rbx       # imm = 0x200000000
	movq	%rax, %rcx
	orq	%rbx, %rcx
	movb	39(%rdi), %dl
	movb	%dl, 13(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movabsq	$17179869184, %r11      # imm = 0x400000000
	movq	%rcx, %rax
	orq	%r11, %rax
	movb	46(%rdi), %dl
	movb	%dl, 12(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movabsq	$34359738368, %r10      # imm = 0x800000000
	movq	%rax, %rcx
	orq	%r10, %rcx
	movb	5(%rdi), %dl
	movb	%dl, 11(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movabsq	$68719476736, %r9       # imm = 0x1000000000
	movq	%rcx, %rax
	orq	%r9, %rax
	movb	12(%rdi), %dl
	movb	%dl, 10(%rsp)           # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movabsq	$137438953472, %r8      # imm = 0x2000000000
	movq	%rax, %rcx
	orq	%r8, %rcx
	movb	19(%rdi), %dl
	movb	%dl, 9(%rsp)            # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rax, %rcx
	movabsq	$274877906944, %rsi     # imm = 0x4000000000
	movq	%rcx, %rax
	orq	%rsi, %rax
	movb	26(%rdi), %dl
	movb	%dl, 8(%rsp)            # 1-byte Spill
	cmpb	$111, %dl
	cmovneq	%rcx, %rax
	movabsq	$549755813888, %rdx     # imm = 0x8000000000
	movq	%rax, %rbp
	orq	%rdx, %rbp
	movb	33(%rdi), %cl
	movb	%cl, 7(%rsp)            # 1-byte Spill
	cmpb	$111, %cl
	cmovneq	%rax, %rbp
	movabsq	$1099511627776, %rcx    # imm = 0x10000000000
	movq	%rbp, %rax
	orq	%rcx, %rax
	movb	40(%rdi), %r13b
	cmpb	$111, %r13b
	cmovneq	%rbp, %rax
	movabsq	$2199023255552, %r12    # imm = 0x20000000000
	movq	%rax, %rbp
	orq	%r12, %rbp
	movb	47(%rdi), %dil
	cmpb	$111, %dil
	cmovneq	%rax, %rbp
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	cmpb	$120, 48(%rsp)          # 1-byte Folded Reload
	sete	%al
	cmpb	$120, 72(%rsp)          # 1-byte Folded Reload
	leaq	2(%rax), %rbp
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$4, %rax
	cmpb	$120, 80(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$8, %rbp
	cmpb	$120, 43(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$16, %rax
	cmpb	$120, 42(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$32, %rbp
	cmpb	$120, 41(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$64, %rax
	cmpb	$120, 39(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$128, %rbp
	cmpb	$120, 40(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$256, %rax              # imm = 0x100
	cmpb	$120, 38(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$512, %rbp              # imm = 0x200
	cmpb	$120, 37(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$1024, %rax             # imm = 0x400
	cmpb	$120, 36(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$2048, %rbp             # imm = 0x800
	cmpb	$120, 35(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$4096, %rax             # imm = 0x1000
	cmpb	$120, 34(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$8192, %rbp             # imm = 0x2000
	cmpb	$120, 33(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$16384, %rax            # imm = 0x4000
	cmpb	$120, 32(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$32768, %rbp            # imm = 0x8000
	cmpb	$120, 31(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$65536, %rax            # imm = 0x10000
	cmpb	$120, 30(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$131072, %rbp           # imm = 0x20000
	cmpb	$120, 29(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$262144, %rax           # imm = 0x40000
	cmpb	$120, 28(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$524288, %rbp           # imm = 0x80000
	cmpb	$120, 27(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$1048576, %rax          # imm = 0x100000
	cmpb	$120, 26(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$2097152, %rbp          # imm = 0x200000
	cmpb	$120, 25(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$4194304, %rax          # imm = 0x400000
	cmpb	$120, 24(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$8388608, %rbp          # imm = 0x800000
	cmpb	$120, 23(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$16777216, %rax         # imm = 0x1000000
	cmpb	$120, 22(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$33554432, %rbp         # imm = 0x2000000
	cmpb	$120, 21(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$67108864, %rax         # imm = 0x4000000
	cmpb	$120, 20(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$134217728, %rbp        # imm = 0x8000000
	cmpb	$120, 19(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$268435456, %rax        # imm = 0x10000000
	cmpb	$120, 18(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	movq	%rax, %rbp
	orq	$536870912, %rbp        # imm = 0x20000000
	cmpb	$120, 17(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %rbp
	movq	%rbp, %rax
	orq	$1073741824, %rax       # imm = 0x40000000
	cmpb	$120, 16(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbp, %rax
	orq	%rax, %r15
	cmpb	$120, 15(%rsp)          # 1-byte Folded Reload
	cmovneq	%rax, %r15
	orq	%r15, %r14
	cmpb	$120, 14(%rsp)          # 1-byte Folded Reload
	cmovneq	%r15, %r14
	orq	%r14, %rbx
	cmpb	$120, 13(%rsp)          # 1-byte Folded Reload
	cmovneq	%r14, %rbx
	orq	%rbx, %r11
	cmpb	$120, 12(%rsp)          # 1-byte Folded Reload
	cmovneq	%rbx, %r11
	orq	%r11, %r10
	cmpb	$120, 11(%rsp)          # 1-byte Folded Reload
	cmovneq	%r11, %r10
	orq	%r10, %r9
	cmpb	$120, 10(%rsp)          # 1-byte Folded Reload
	cmovneq	%r10, %r9
	orq	%r9, %r8
	cmpb	$120, 9(%rsp)           # 1-byte Folded Reload
	cmovneq	%r9, %r8
	orq	%r8, %rsi
	cmpb	$120, 8(%rsp)           # 1-byte Folded Reload
	cmovneq	%r8, %rsi
	orq	%rsi, %rdx
	cmpb	$120, 7(%rsp)           # 1-byte Folded Reload
	cmovneq	%rsi, %rdx
	orq	%rdx, %rcx
	cmpb	$120, %r13b
	cmovneq	%rdx, %rcx
	movl	60(%rsp), %eax          # 4-byte Reload
	orq	%rcx, %r12
	cmpb	$120, %dil
	cmovneq	%rcx, %r12
	movq	%r12, 48(%rsp)          # 8-byte Spill
	cmpl	$1, %eax
	jne	.LBB8_5
# BB#1:
	movl	44(%rsp), %eax          # 4-byte Reload
	cmpl	$1, %eax
	je	.LBB8_4
# BB#2:
	cmpl	$2, %eax
	jne	.LBB8_56
# BB#3:
	movq	%rsp, %rcx
	movl	$1, %edi
	movl	$-100000, %r8d          # imm = 0xFFFE7960
	movl	$100000, %r9d           # imm = 0x186A0
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	minimax_comp_ab
	jmp	.LBB8_56
.LBB8_5:
	cmpl	$2, %eax
	jne	.LBB8_10
# BB#6:
	movl	44(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	jne	.LBB8_8
# BB#7:                                 # %.thread
	movq	%rsp, %rcx
	movl	$1, %edi
	movl	$100000, %r8d           # imm = 0x186A0
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	minimax_comp_ab2
	jmp	.LBB8_56
.LBB8_10:
	movl	44(%rsp), %eax          # 4-byte Reload
	cmpl	$2, %eax
	jne	.LBB8_33
# BB#11:
	cmpl	$2, DEPTH(%rip)
	jl	.LBB8_56
# BB#12:                                # %.preheader.i
	movq	48(%rsp), %rbx          # 8-byte Reload
	orq	64(%rsp), %rbx          # 8-byte Folded Reload
	movl	$-100000, %ebp          # imm = 0xFFFE7960
	xorl	%r13d, %r13d
	xorl	%r14d, %r14d
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_13:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_21 Depth 2
	movl	$1, %r12d
	movl	%r13d, %ecx
	shlq	%cl, %r12
	btq	%r13, %rbx
	jae	.LBB8_19
# BB#14:                                #   in Loop: Header=BB8_13 Depth=1
	leal	7(%r13), %ecx
	movl	$1, %r12d
	shlq	%cl, %r12
	btq	%rcx, %rbx
	jae	.LBB8_19
# BB#15:                                #   in Loop: Header=BB8_13 Depth=1
	leal	14(%r13), %ecx
	movl	$1, %r12d
	shlq	%cl, %r12
	btq	%rcx, %rbx
	jae	.LBB8_19
# BB#16:                                #   in Loop: Header=BB8_13 Depth=1
	leal	21(%r13), %ecx
	movl	$1, %r12d
	shlq	%cl, %r12
	btq	%rcx, %rbx
	jae	.LBB8_19
# BB#17:                                #   in Loop: Header=BB8_13 Depth=1
	leal	28(%r13), %ecx
	movl	$1, %r12d
	shlq	%cl, %r12
	btq	%rcx, %rbx
	jae	.LBB8_19
# BB#18:                                #   in Loop: Header=BB8_13 Depth=1
	leal	35(%r13), %ecx
	movl	$1, %r12d
	shlq	%cl, %r12
	btq	%rcx, %rbx
	jb	.LBB8_31
	.p2align	4, 0x90
.LBB8_19:                               #   in Loop: Header=BB8_13 Depth=1
	orq	48(%rsp), %r12          # 8-byte Folded Reload
	cmpl	$3, DEPTH(%rip)
	jl	.LBB8_57
# BB#20:                                # %.preheader.i35
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	%r14d, 72(%rsp)         # 4-byte Spill
	movl	%ebp, 60(%rsp)          # 4-byte Spill
	movq	%r12, %rbx
	orq	64(%rsp), %rbx          # 8-byte Folded Reload
	movl	$100000, %r15d          # imm = 0x186A0
	xorl	%ebp, %ebp
	movl	$35, %r14d
	.p2align	4, 0x90
.LBB8_21:                               #   Parent Loop BB8_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-35(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %rbx
	jae	.LBB8_27
# BB#22:                                #   in Loop: Header=BB8_21 Depth=2
	leal	-28(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %rbx
	jae	.LBB8_27
# BB#23:                                #   in Loop: Header=BB8_21 Depth=2
	leal	-21(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %rbx
	jae	.LBB8_27
# BB#24:                                #   in Loop: Header=BB8_21 Depth=2
	leal	-14(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %rbx
	jae	.LBB8_27
# BB#25:                                #   in Loop: Header=BB8_21 Depth=2
	leal	-7(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %rbx
	jae	.LBB8_27
# BB#26:                                #   in Loop: Header=BB8_21 Depth=2
	movl	$1, %esi
	movl	%r14d, %ecx
	shlq	%cl, %rsi
	btq	%r14, %rbx
	jb	.LBB8_28
	.p2align	4, 0x90
.LBB8_27:                               #   in Loop: Header=BB8_21 Depth=2
	orq	64(%rsp), %rsi          # 8-byte Folded Reload
	movl	$3, %edi
	movq	%r12, %rdx
	movq	%rsp, %rcx
	callq	minimax_comp
	leal	-35(%r14), %ecx
	cmpl	%r15d, %eax
	cmovlel	%eax, %r15d
	cmovlel	%ecx, %ebp
.LBB8_28:                               # %bit_place_piece.exit.i44
                                        #   in Loop: Header=BB8_21 Depth=2
	incq	%r14
	cmpq	$42, %r14
	jne	.LBB8_21
# BB#29:                                #   in Loop: Header=BB8_13 Depth=1
	movl	%ebp, (%rsp)
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	60(%rsp), %ebp          # 4-byte Reload
	movl	72(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB8_30
	.p2align	4, 0x90
.LBB8_57:                               #   in Loop: Header=BB8_13 Depth=1
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	value
	movl	%eax, %r15d
.LBB8_30:                               # %minimax_player.exit
                                        #   in Loop: Header=BB8_13 Depth=1
	cmpl	%ebp, %r15d
	cmovgel	%r15d, %ebp
	cmovgl	%r13d, %r14d
.LBB8_31:                               # %bit_place_piece.exit.i
                                        #   in Loop: Header=BB8_13 Depth=1
	incq	%r13
	cmpq	$7, %r13
	jne	.LBB8_13
# BB#32:
	movl	%r14d, (%rsp)
	movl	44(%rsp), %eax          # 4-byte Reload
.LBB8_33:                               # %minimax_comp.exit
	cmpl	$1, %eax
	jne	.LBB8_56
# BB#34:                                # %minimax_comp.exit
	cmpl	$2, DEPTH(%rip)
	jl	.LBB8_56
# BB#35:                                # %.preheader.i46
	movq	48(%rsp), %r14          # 8-byte Reload
	orq	64(%rsp), %r14          # 8-byte Folded Reload
	movl	$100000, %ebx           # imm = 0x186A0
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	movq	%r14, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_36:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_44 Depth 2
	movl	$1, %r13d
	movl	%r12d, %ecx
	shlq	%cl, %r13
	btq	%r12, %r14
	jae	.LBB8_42
# BB#37:                                #   in Loop: Header=BB8_36 Depth=1
	leal	7(%r12), %ecx
	movl	$1, %r13d
	shlq	%cl, %r13
	btq	%rcx, %r14
	jae	.LBB8_42
# BB#38:                                #   in Loop: Header=BB8_36 Depth=1
	leal	14(%r12), %ecx
	movl	$1, %r13d
	shlq	%cl, %r13
	btq	%rcx, %r14
	jae	.LBB8_42
# BB#39:                                #   in Loop: Header=BB8_36 Depth=1
	leal	21(%r12), %ecx
	movl	$1, %r13d
	shlq	%cl, %r13
	btq	%rcx, %r14
	jae	.LBB8_42
# BB#40:                                #   in Loop: Header=BB8_36 Depth=1
	leal	28(%r12), %ecx
	movl	$1, %r13d
	shlq	%cl, %r13
	btq	%rcx, %r14
	jae	.LBB8_42
# BB#41:                                #   in Loop: Header=BB8_36 Depth=1
	leal	35(%r12), %ecx
	movl	$1, %r13d
	shlq	%cl, %r13
	btq	%rcx, %r14
	jb	.LBB8_54
	.p2align	4, 0x90
.LBB8_42:                               #   in Loop: Header=BB8_36 Depth=1
	orq	64(%rsp), %r13          # 8-byte Folded Reload
	cmpl	$3, DEPTH(%rip)
	jl	.LBB8_58
# BB#43:                                # %.preheader.i60
                                        #   in Loop: Header=BB8_36 Depth=1
	movl	%r15d, 60(%rsp)         # 4-byte Spill
	movl	%ebx, 44(%rsp)          # 4-byte Spill
	movq	%r13, %r14
	orq	48(%rsp), %r14          # 8-byte Folded Reload
	movl	$-100000, %ebp          # imm = 0xFFFE7960
	xorl	%r15d, %r15d
	movl	$35, %ebx
	.p2align	4, 0x90
.LBB8_44:                               #   Parent Loop BB8_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	-35(%rbx), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r14
	jae	.LBB8_50
# BB#45:                                #   in Loop: Header=BB8_44 Depth=2
	leal	-28(%rbx), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r14
	jae	.LBB8_50
# BB#46:                                #   in Loop: Header=BB8_44 Depth=2
	leal	-21(%rbx), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r14
	jae	.LBB8_50
# BB#47:                                #   in Loop: Header=BB8_44 Depth=2
	leal	-14(%rbx), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r14
	jae	.LBB8_50
# BB#48:                                #   in Loop: Header=BB8_44 Depth=2
	leal	-7(%rbx), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r14
	jae	.LBB8_50
# BB#49:                                #   in Loop: Header=BB8_44 Depth=2
	movl	$1, %edx
	movl	%ebx, %ecx
	shlq	%cl, %rdx
	btq	%rbx, %r14
	jb	.LBB8_51
	.p2align	4, 0x90
.LBB8_50:                               #   in Loop: Header=BB8_44 Depth=2
	orq	48(%rsp), %rdx          # 8-byte Folded Reload
	movl	$3, %edi
	movq	%r13, %rsi
	movq	%rsp, %rcx
	callq	minimax_player
	leal	-35(%rbx), %ecx
	cmpl	%ebp, %eax
	cmovgel	%eax, %ebp
	cmovgl	%ecx, %r15d
.LBB8_51:                               # %bit_place_piece.exit.i71
                                        #   in Loop: Header=BB8_44 Depth=2
	incq	%rbx
	cmpq	$42, %rbx
	jne	.LBB8_44
# BB#52:                                #   in Loop: Header=BB8_36 Depth=1
	movl	%r15d, (%rsp)
	movq	72(%rsp), %r14          # 8-byte Reload
	movl	44(%rsp), %ebx          # 4-byte Reload
	movl	60(%rsp), %r15d         # 4-byte Reload
	jmp	.LBB8_53
	.p2align	4, 0x90
.LBB8_58:                               #   in Loop: Header=BB8_36 Depth=1
	movq	%r13, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	value
	movl	%eax, %ebp
.LBB8_53:                               # %minimax_comp.exit73
                                        #   in Loop: Header=BB8_36 Depth=1
	cmpl	%ebx, %ebp
	cmovlel	%ebp, %ebx
	cmovlel	%r12d, %r15d
.LBB8_54:                               # %bit_place_piece.exit.i57
                                        #   in Loop: Header=BB8_36 Depth=1
	incq	%r12
	cmpq	$7, %r12
	jne	.LBB8_36
# BB#55:
	movl	%r15d, (%rsp)
	jmp	.LBB8_56
.LBB8_4:
	movq	%rsp, %rcx
	movl	$1, %edi
	movl	$-100000, %r8d          # imm = 0xFFFE7960
	movl	$100000, %r9d           # imm = 0x186A0
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	minimax_player_ab
	jmp	.LBB8_56
.LBB8_8:
	cmpl	$1, %eax
	jne	.LBB8_56
# BB#9:
	movq	%rsp, %rcx
	movl	$1, %edi
	movl	$-100000, %r8d          # imm = 0xFFFE7960
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	minimax_player_ab2
.LBB8_56:                               # %minimax_player.exit59
	movl	(%rsp), %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	think, .Lfunc_end8-think
	.cfi_endproc

	.globl	minimax_comp_ab
	.p2align	4, 0x90
	.type	minimax_comp_ab,@function
minimax_comp_ab:                        # @minimax_comp_ab
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi37:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi38:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 80
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %ebx
	movq	%rcx, %rax
	movl	%edi, %r13d
	cmpl	%r13d, DEPTH(%rip)
	jle	.LBB9_14
# BB#1:                                 # %.preheader
	xorl	%r15d, %r15d
	cmpl	%ebp, %ebx
	jge	.LBB9_13
# BB#2:                                 # %.lr.ph
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	orq	%rsi, %r12
	incl	%r13d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movl	%r14d, %ecx
	shlq	%cl, %rdx
	btq	%r14, %r12
	jae	.LBB9_9
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	leal	7(%r14), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB9_9
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	leal	14(%r14), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB9_9
# BB#6:                                 #   in Loop: Header=BB9_3 Depth=1
	leal	21(%r14), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB9_9
# BB#7:                                 #   in Loop: Header=BB9_3 Depth=1
	leal	28(%r14), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB9_9
# BB#8:                                 #   in Loop: Header=BB9_3 Depth=1
	leal	35(%r14), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jb	.LBB9_10
	.p2align	4, 0x90
.LBB9_9:                                #   in Loop: Header=BB9_3 Depth=1
	orq	16(%rsp), %rdx          # 8-byte Folded Reload
	movl	%r13d, %edi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rax, %rcx
	movl	%ebx, %r8d
	movl	%ebp, %r9d
	callq	minimax_player_ab
	cmpl	%ebx, %eax
	cmovgel	%eax, %ebx
	cmovgl	%r14d, %r15d
.LBB9_10:                               # %bit_place_piece.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	incq	%r14
	cmpq	$6, %r14
	jg	.LBB9_11
# BB#12:                                # %bit_place_piece.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpl	%ebp, %ebx
	movq	(%rsp), %rax            # 8-byte Reload
	jl	.LBB9_3
	jmp	.LBB9_13
.LBB9_14:
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	value                   # TAILCALL
.LBB9_11:
	movq	(%rsp), %rax            # 8-byte Reload
.LBB9_13:
	movl	%r15d, (%rax)
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	minimax_comp_ab, .Lfunc_end9-minimax_comp_ab
	.cfi_endproc

	.globl	minimax_player_ab
	.p2align	4, 0x90
	.type	minimax_player_ab,@function
minimax_player_ab:                      # @minimax_player_ab
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 80
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movl	%r8d, %ebp
	movq	%rcx, %rax
	movl	%edi, %r13d
	cmpl	%r13d, DEPTH(%rip)
	jle	.LBB10_14
# BB#1:                                 # %.preheader
	xorl	%r15d, %r15d
	cmpl	%ebp, %ebx
	jle	.LBB10_13
# BB#2:                                 # %.lr.ph
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	orq	%rsi, %r12
	incl	%r13d
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB10_3:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	%r14d, %ecx
	shlq	%cl, %rsi
	btq	%r14, %r12
	jae	.LBB10_9
# BB#4:                                 #   in Loop: Header=BB10_3 Depth=1
	leal	7(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB10_9
# BB#5:                                 #   in Loop: Header=BB10_3 Depth=1
	leal	14(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB10_9
# BB#6:                                 #   in Loop: Header=BB10_3 Depth=1
	leal	21(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB10_9
# BB#7:                                 #   in Loop: Header=BB10_3 Depth=1
	leal	28(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB10_9
# BB#8:                                 #   in Loop: Header=BB10_3 Depth=1
	leal	35(%r14), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jb	.LBB10_10
	.p2align	4, 0x90
.LBB10_9:                               #   in Loop: Header=BB10_3 Depth=1
	orq	8(%rsp), %rsi           # 8-byte Folded Reload
	movl	%r13d, %edi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rax, %rcx
	movl	%ebp, %r8d
	movl	%ebx, %r9d
	callq	minimax_comp_ab
	cmpl	%ebx, %eax
	cmovlel	%eax, %ebx
	cmovlel	%r14d, %r15d
.LBB10_10:                              # %bit_place_piece.exit
                                        #   in Loop: Header=BB10_3 Depth=1
	incq	%r14
	cmpq	$6, %r14
	jg	.LBB10_11
# BB#12:                                # %bit_place_piece.exit
                                        #   in Loop: Header=BB10_3 Depth=1
	cmpl	%ebp, %ebx
	movq	(%rsp), %rax            # 8-byte Reload
	jg	.LBB10_3
	jmp	.LBB10_13
.LBB10_14:
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	value                   # TAILCALL
.LBB10_11:
	movq	(%rsp), %rax            # 8-byte Reload
.LBB10_13:
	movl	%r15d, (%rax)
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	minimax_player_ab, .Lfunc_end10-minimax_player_ab
	.cfi_endproc

	.globl	minimax_comp_ab2
	.p2align	4, 0x90
	.type	minimax_comp_ab2,@function
minimax_comp_ab2:                       # @minimax_comp_ab2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi66:
	.cfi_def_cfa_offset 80
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, %rax
	movq	%rsi, %r14
	movl	%edi, %ebp
	cmpl	%ebp, DEPTH(%rip)
	jle	.LBB11_12
# BB#1:                                 # %.preheader
	movq	%rax, %r12
	orq	%r14, %r12
	incl	%ebp
	movl	$-100000, %r13d         # imm = 0xFFFE7960
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB11_2:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movl	%r15d, %ecx
	shlq	%cl, %rdx
	btq	%r15, %r12
	jae	.LBB11_8
# BB#3:                                 #   in Loop: Header=BB11_2 Depth=1
	leal	7(%r15), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB11_8
# BB#4:                                 #   in Loop: Header=BB11_2 Depth=1
	leal	14(%r15), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB11_8
# BB#5:                                 #   in Loop: Header=BB11_2 Depth=1
	leal	21(%r15), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB11_8
# BB#6:                                 #   in Loop: Header=BB11_2 Depth=1
	leal	28(%r15), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jae	.LBB11_8
# BB#7:                                 #   in Loop: Header=BB11_2 Depth=1
	leal	35(%r15), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r12
	jb	.LBB11_9
	.p2align	4, 0x90
.LBB11_8:                               #   in Loop: Header=BB11_2 Depth=1
	orq	%rax, %rdx
	movl	%ebp, %edi
	movq	%r14, %rsi
	movq	%rbx, %rcx
	movl	%r13d, %r8d
	movl	%r11d, %ebx
	callq	minimax_player_ab2
	movl	%ebx, %r11d
	movl	4(%rsp), %r8d           # 4-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	cmovgl	%r15d, %r11d
	cmpl	%r8d, %r13d
	jg	.LBB11_11
.LBB11_9:                               # %bit_place_piece.exit
                                        #   in Loop: Header=BB11_2 Depth=1
	incq	%r15
	cmpq	$7, %r15
	jl	.LBB11_2
# BB#10:
	movl	%r11d, (%rbx)
.LBB11_11:                              # %.loopexit
	movl	%r13d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_12:
	movq	%r14, %rdi
	movq	%rax, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	value                   # TAILCALL
.Lfunc_end11:
	.size	minimax_comp_ab2, .Lfunc_end11-minimax_comp_ab2
	.cfi_endproc

	.globl	minimax_player_ab2
	.p2align	4, 0x90
	.type	minimax_player_ab2,@function
minimax_player_ab2:                     # @minimax_player_ab2
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 80
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%rcx, %rax
	movq	%rdx, %r13
	movq	%rsi, %r14
	movl	%edi, %ebp
	cmpl	%ebp, DEPTH(%rip)
	jle	.LBB12_12
# BB#1:                                 # %.preheader
	movq	%r13, %r12
	orq	%r14, %r12
	incl	%ebp
	movl	$100000, %ebx           # imm = 0x186A0
	xorl	%r15d, %r15d
	xorl	%r11d, %r11d
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB12_2:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	%r15d, %ecx
	shlq	%cl, %rsi
	btq	%r15, %r12
	jae	.LBB12_8
# BB#3:                                 #   in Loop: Header=BB12_2 Depth=1
	leal	7(%r15), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB12_8
# BB#4:                                 #   in Loop: Header=BB12_2 Depth=1
	leal	14(%r15), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB12_8
# BB#5:                                 #   in Loop: Header=BB12_2 Depth=1
	leal	21(%r15), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB12_8
# BB#6:                                 #   in Loop: Header=BB12_2 Depth=1
	leal	28(%r15), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jae	.LBB12_8
# BB#7:                                 #   in Loop: Header=BB12_2 Depth=1
	leal	35(%r15), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r12
	jb	.LBB12_9
	.p2align	4, 0x90
.LBB12_8:                               #   in Loop: Header=BB12_2 Depth=1
	orq	%r14, %rsi
	movl	%ebp, %edi
	movq	%r13, %rdx
	movq	%rax, %rcx
	movl	%ebx, %r8d
	movl	%r11d, 12(%rsp)         # 4-byte Spill
	callq	minimax_comp_ab2
	movl	12(%rsp), %r11d         # 4-byte Reload
	movl	8(%rsp), %r8d           # 4-byte Reload
	cmpl	%ebx, %eax
	cmovlel	%eax, %ebx
	movq	16(%rsp), %rax          # 8-byte Reload
	cmovlel	%r15d, %r11d
	cmpl	%r8d, %ebx
	jl	.LBB12_11
.LBB12_9:                               # %bit_place_piece.exit
                                        #   in Loop: Header=BB12_2 Depth=1
	incq	%r15
	cmpq	$7, %r15
	jl	.LBB12_2
# BB#10:
	movl	%r11d, (%rax)
.LBB12_11:                              # %.loopexit
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_12:
	movq	%r14, %rdi
	movq	%r13, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	value                   # TAILCALL
.Lfunc_end12:
	.size	minimax_player_ab2, .Lfunc_end12-minimax_player_ab2
	.cfi_endproc

	.globl	minimax_comp
	.p2align	4, 0x90
	.type	minimax_comp,@function
minimax_comp:                           # @minimax_comp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 80
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %rax
	movq	%rsi, %r13
	movl	%edi, %r14d
	cmpl	%r14d, DEPTH(%rip)
	jle	.LBB13_11
# BB#1:                                 # %.preheader
	movq	%rax, %r15
	orq	%r13, %r15
	incl	%r14d
	movl	$-100000, %r12d         # imm = 0xFFFE7960
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %edx
	movl	%ebp, %ecx
	shlq	%cl, %rdx
	btq	%rbp, %r15
	jae	.LBB13_8
# BB#3:                                 #   in Loop: Header=BB13_2 Depth=1
	leal	7(%rbp), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r15
	jae	.LBB13_8
# BB#4:                                 #   in Loop: Header=BB13_2 Depth=1
	leal	14(%rbp), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r15
	jae	.LBB13_8
# BB#5:                                 #   in Loop: Header=BB13_2 Depth=1
	leal	21(%rbp), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r15
	jae	.LBB13_8
# BB#6:                                 #   in Loop: Header=BB13_2 Depth=1
	leal	28(%rbp), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r15
	jae	.LBB13_8
# BB#7:                                 #   in Loop: Header=BB13_2 Depth=1
	leal	35(%rbp), %ecx
	movl	$1, %edx
	shlq	%cl, %rdx
	btq	%rcx, %r15
	jb	.LBB13_9
	.p2align	4, 0x90
.LBB13_8:                               #   in Loop: Header=BB13_2 Depth=1
	orq	%rax, %rdx
	movl	%r14d, %edi
	movq	%r13, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	minimax_player
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	movq	16(%rsp), %rax          # 8-byte Reload
	cmovgl	%ebp, %ebx
.LBB13_9:                               # %bit_place_piece.exit
                                        #   in Loop: Header=BB13_2 Depth=1
	incq	%rbp
	cmpq	$7, %rbp
	jne	.LBB13_2
# BB#10:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%ebx, (%rax)
	movl	%r12d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_11:
	movq	%r13, %rdi
	movq	%rax, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	value                   # TAILCALL
.Lfunc_end13:
	.size	minimax_comp, .Lfunc_end13-minimax_comp
	.cfi_endproc

	.globl	minimax_player
	.p2align	4, 0x90
	.type	minimax_player,@function
minimax_player:                         # @minimax_player
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi103:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi105:
	.cfi_def_cfa_offset 80
.Lcfi106:
	.cfi_offset %rbx, -56
.Lcfi107:
	.cfi_offset %r12, -48
.Lcfi108:
	.cfi_offset %r13, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %rax
	movl	%edi, %r14d
	cmpl	%r14d, DEPTH(%rip)
	jle	.LBB14_11
# BB#1:                                 # %.preheader
	movq	%r12, %r15
	orq	%rax, %r15
	incl	%r14d
	movl	$100000, %r13d          # imm = 0x186A0
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movl	$1, %esi
	movl	%ebp, %ecx
	shlq	%cl, %rsi
	btq	%rbp, %r15
	jae	.LBB14_8
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	leal	7(%rbp), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r15
	jae	.LBB14_8
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	leal	14(%rbp), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r15
	jae	.LBB14_8
# BB#5:                                 #   in Loop: Header=BB14_2 Depth=1
	leal	21(%rbp), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r15
	jae	.LBB14_8
# BB#6:                                 #   in Loop: Header=BB14_2 Depth=1
	leal	28(%rbp), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r15
	jae	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_2 Depth=1
	leal	35(%rbp), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	btq	%rcx, %r15
	jb	.LBB14_9
	.p2align	4, 0x90
.LBB14_8:                               #   in Loop: Header=BB14_2 Depth=1
	orq	%rax, %rsi
	movl	%r14d, %edi
	movq	%r12, %rdx
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	minimax_comp
	cmpl	%r13d, %eax
	cmovlel	%eax, %r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	cmovlel	%ebp, %ebx
.LBB14_9:                               # %bit_place_piece.exit
                                        #   in Loop: Header=BB14_2 Depth=1
	incq	%rbp
	cmpq	$7, %rbp
	jne	.LBB14_2
# BB#10:
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%ebx, (%rax)
	movl	%r13d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_11:
	movq	%rax, %rdi
	movq	%r12, %rsi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	value                   # TAILCALL
.Lfunc_end14:
	.size	minimax_player, .Lfunc_end14-minimax_player
	.cfi_endproc

	.globl	bit_place_piece
	.p2align	4, 0x90
	.type	bit_place_piece,@function
bit_place_piece:                        # @bit_place_piece
	.cfi_startproc
# BB#0:
	movq	%rcx, %r8
	movl	%edi, %r9d
	movq	(%r8), %r10
	orq	(%rdx), %r10
	movl	$1, %edi
	movl	%r9d, %ecx
	shlq	%cl, %rdi
	btq	%r9, %r10
	jae	.LBB15_2
# BB#1:
	leal	7(%r9), %ecx
	movl	$1, %edi
	shlq	%cl, %rdi
	btq	%rcx, %r10
	jae	.LBB15_2
# BB#4:
	leal	14(%r9), %ecx
	movl	$1, %edi
	shlq	%cl, %rdi
	btq	%rcx, %r10
	jae	.LBB15_2
# BB#5:
	leal	21(%r9), %ecx
	movl	$1, %edi
	shlq	%cl, %rdi
	btq	%rcx, %r10
	jae	.LBB15_2
# BB#6:
	leal	28(%r9), %ecx
	movl	$1, %edi
	shlq	%cl, %rdi
	btq	%rcx, %r10
	jae	.LBB15_2
# BB#7:
	addl	$35, %r9d
	movl	$1, %edi
	movl	%r9d, %ecx
	shlq	%cl, %rdi
	movl	$1, %eax
	btq	%r9, %r10
	jb	.LBB15_3
.LBB15_2:
	cmpl	$1, %esi
	cmoveq	%rdx, %r8
	orq	%rdi, (%r8)
	xorl	%eax, %eax
.LBB15_3:                               # %.loopexit
	retq
.Lfunc_end15:
	.size	bit_place_piece, .Lfunc_end15-bit_place_piece
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 16
	subq	$80, %rsp
.Lcfi113:
	.cfi_def_cfa_offset 96
.Lcfi114:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movl	$0, 12(%rsp)
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.8, %esi
	movl	$.L.str.9, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	8(%rbx), %rdi
	movl	$.L.str.10, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB16_2
# BB#1:
	movl	$.L.str.11, %edi
	movl	$.L.str.10, %esi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB16_29
.LBB16_2:
	movl	$.L.str.13, %esi
	movl	$DEPTH, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	leaq	12(%rsp), %rdx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fscanf
	movq	%rbx, %rdi
	callq	fclose
	movl	DEPTH(%rip), %esi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 12(%rsp)
	movl	$.L.str.16, %eax
	movl	$.L.str.17, %esi
	cmovneq	%rax, %rsi
	movl	$.L.str.15, %edi
	xorl	%eax, %eax
	callq	printf
	movl	12(%rsp), %eax
	cmpl	$1, %eax
	jne	.LBB16_4
# BB#3:
	movl	$.Lstr.7, %edi
	callq	puts
	movl	12(%rsp), %eax
.LBB16_4:
	cmpl	$2, %eax
	jne	.LBB16_6
# BB#5:
	movl	$.Lstr.6, %edi
	callq	puts
.LBB16_6:
	movq	C4VERT(%rip), %rax
	shlq	$14, %rax
	orq	$16513, %rax            # imm = 0x4081
	movq	%rax, %rcx
	shlq	$7, %rcx
	orq	$1, %rcx
	movq	%rcx, C4VERT(%rip)
	movabsq	$144115188075839617, %rcx # imm = 0x1FFFFFFFFFFC081
	andq	%rax, %rcx
	movq	%rcx, C3VERT(%rip)
	shrq	$7, %rax
	movabsq	$1125899906842497, %rcx # imm = 0x3FFFFFFFFFF81
	andq	%rax, %rcx
	movq	%rcx, C2VERT(%rip)
	movq	$15, C4HORIZ(%rip)
	movq	$7, C3HORIZ(%rip)
	movq	$3, C2HORIZ(%rip)
	movq	C4UP_R(%rip), %rax
	shlq	$16, %rax
	orq	$65793, %rax            # imm = 0x10101
	movq	%rax, %rcx
	shlq	$8, %rcx
	orq	$1, %rcx
	movq	%rcx, C4UP_R(%rip)
	movabsq	$72057594037862657, %rcx # imm = 0xFFFFFFFFFF0101
	andq	%rax, %rcx
	movq	%rcx, C3UP_R(%rip)
	shrq	$8, %rax
	movabsq	$281474976710401, %rcx  # imm = 0xFFFFFFFFFF01
	andq	%rax, %rcx
	movq	%rcx, C2UP_R(%rip)
	movq	C4UP_L(%rip), %rax
	shlq	$12, %rax
	orq	$33288, %rax            # imm = 0x8208
	movq	%rax, %rcx
	shlq	$6, %rcx
	orq	$8, %rcx
	movq	%rcx, C4UP_L(%rip)
	movabsq	$288230376151708168, %rcx # imm = 0x3FFFFFFFFFFF208
	andq	%rax, %rcx
	movq	%rcx, C3UP_L(%rip)
	shrq	$6, %rax
	movabsq	$4503599627370440, %rcx # imm = 0xFFFFFFFFFFFC8
	andq	%rax, %rcx
	movq	%rcx, C2UP_L(%rip)
	movw	$11822, 20(%rsp)        # imm = 0x2E2E
	movl	$774778414, 16(%rsp)    # imm = 0x2E2E2E2E
	movw	$11822, 27(%rsp)        # imm = 0x2E2E
	movl	$774778414, 23(%rsp)    # imm = 0x2E2E2E2E
	movw	$11822, 34(%rsp)        # imm = 0x2E2E
	movl	$774778414, 30(%rsp)    # imm = 0x2E2E2E2E
	movw	$11822, 41(%rsp)        # imm = 0x2E2E
	movl	$774778414, 37(%rsp)    # imm = 0x2E2E2E2E
	movw	$11822, 48(%rsp)        # imm = 0x2E2E
	movl	$774778414, 44(%rsp)    # imm = 0x2E2E2E2E
	movw	$11822, 55(%rsp)        # imm = 0x2E2E
	movl	$774778414, 51(%rsp)    # imm = 0x2E2E2E2E
	movw	$11822, 62(%rsp)        # imm = 0x2E2E
	movl	$774778414, 58(%rsp)    # imm = 0x2E2E2E2E
	movb	$0, 22(%rsp)
	movb	$0, 29(%rsp)
	movb	$0, 36(%rsp)
	movb	$0, 43(%rsp)
	movb	$0, 50(%rsp)
	movb	$0, 57(%rsp)
	movb	$0, 64(%rsp)
	leaq	16(%rsp), %rbx
	movq	%rbx, %rdi
	callq	print_board
	movq	%rbx, %rdi
	callq	find_winner_p
	testl	%eax, %eax
	jne	.LBB16_18
# BB#7:                                 # %.lr.ph.preheader
	leaq	16(%rsp), %rbx
	.p2align	4, 0x90
.LBB16_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rdi
	callq	find_winner_c
	testl	%eax, %eax
	jne	.LBB16_18
# BB#9:                                 #   in Loop: Header=BB16_8 Depth=1
	movl	12(%rsp), %edx
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	think
	movl	%eax, %ecx
	cmpl	$7, %ecx
	jb	.LBB16_11
# BB#10:                                #   in Loop: Header=BB16_8 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	jmp	.LBB16_13
	.p2align	4, 0x90
.LBB16_11:                              #   in Loop: Header=BB16_8 Depth=1
	movslq	%ecx, %rcx
	leaq	(,%rcx,8), %rax
	subq	%rcx, %rax
	movsbq	22(%rsp,%rax), %rcx
	cmpq	$5, %rcx
	jg	.LBB16_13
# BB#12:                                #   in Loop: Header=BB16_8 Depth=1
	leaq	22(%rsp,%rax), %rdx
	leaq	16(%rsp,%rax), %rax
	movb	$111, (%rcx,%rax)
	incb	(%rdx)
.LBB16_13:                              # %place_piece.exit
                                        #   in Loop: Header=BB16_8 Depth=1
	movl	12(%rsp), %edx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	think
	movl	%eax, %ecx
	cmpl	$7, %ecx
	jb	.LBB16_15
# BB#14:                                #   in Loop: Header=BB16_8 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	jmp	.LBB16_17
	.p2align	4, 0x90
.LBB16_15:                              #   in Loop: Header=BB16_8 Depth=1
	movslq	%ecx, %rcx
	leaq	(,%rcx,8), %rax
	subq	%rcx, %rax
	movsbq	22(%rsp,%rax), %rcx
	cmpq	$5, %rcx
	jg	.LBB16_17
# BB#16:                                #   in Loop: Header=BB16_8 Depth=1
	leaq	22(%rsp,%rax), %rdx
	leaq	16(%rsp,%rax), %rax
	movb	$120, (%rcx,%rax)
	incb	(%rdx)
.LBB16_17:                              # %place_piece.exit15
                                        #   in Loop: Header=BB16_8 Depth=1
	movq	%rbx, %rdi
	callq	print_board
	movq	%rbx, %rdi
	callq	find_winner_p
	testl	%eax, %eax
	je	.LBB16_8
.LBB16_18:                              # %.critedge
	leaq	16(%rsp), %rdi
	callq	find_winner_p
	cmpl	$1, %eax
	jne	.LBB16_21
# BB#19:
	leaq	16(%rsp), %rdi
	callq	find_winner_c
	testl	%eax, %eax
	jne	.LBB16_21
# BB#20:
	movl	$.Lstr.5, %edi
	callq	puts
.LBB16_21:
	leaq	16(%rsp), %rdi
	callq	find_winner_c
	cmpl	$1, %eax
	jne	.LBB16_24
# BB#22:
	leaq	16(%rsp), %rdi
	callq	find_winner_p
	testl	%eax, %eax
	jne	.LBB16_24
# BB#23:
	movl	$.Lstr.4, %edi
	callq	puts
.LBB16_24:
	leaq	16(%rsp), %rdi
	callq	find_winner_p
	movl	%eax, %ebx
	cmpl	$2, %ebx
	je	.LBB16_27
# BB#25:
	leaq	16(%rsp), %rdi
	callq	find_winner_c
	testl	%ebx, %ebx
	je	.LBB16_28
# BB#26:
	cmpl	$1, %eax
	jne	.LBB16_28
.LBB16_27:
	movl	$.Lstr.3, %edi
	callq	puts
.LBB16_28:
	xorl	%eax, %eax
	addq	$80, %rsp
	popq	%rbx
	retq
.LBB16_29:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end16:
	.size	main, .Lfunc_end16-main
	.cfi_endproc

	.type	DEPTH,@object           # @DEPTH
	.data
	.globl	DEPTH
	.p2align	2
DEPTH:
	.long	3                       # 0x3
	.size	DEPTH, 4

	.type	off,@object             # @off
	.bss
	.globl	off
	.p2align	2
off:
	.long	0                       # 0x0
	.size	off, 4

	.type	C4VERT,@object          # @C4VERT
	.comm	C4VERT,8,8
	.type	C3VERT,@object          # @C3VERT
	.comm	C3VERT,8,8
	.type	C2VERT,@object          # @C2VERT
	.comm	C2VERT,8,8
	.type	C4HORIZ,@object         # @C4HORIZ
	.comm	C4HORIZ,8,8
	.type	C3HORIZ,@object         # @C3HORIZ
	.comm	C3HORIZ,8,8
	.type	C2HORIZ,@object         # @C2HORIZ
	.comm	C2HORIZ,8,8
	.type	C4UP_R,@object          # @C4UP_R
	.comm	C4UP_R,8,8
	.type	C3UP_R,@object          # @C3UP_R
	.comm	C3UP_R,8,8
	.type	C2UP_R,@object          # @C2UP_R
	.comm	C2UP_R,8,8
	.type	C4UP_L,@object          # @C4UP_L
	.comm	C4UP_L,8,8
	.type	C3UP_L,@object          # @C3UP_L
	.comm	C3UP_L,8,8
	.type	C2UP_L,@object          # @C2UP_L
	.comm	C2UP_L,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" %d"
	.size	.L.str, 4

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d "
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%c "
	.size	.L.str.2, 4

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ERROR: Faulty column: %d.\n"
	.size	.L.str.4, 27

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Compile date: %s\n"
	.size	.L.str.6, 18

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"today"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Compiler switches: %s\n"
	.size	.L.str.8, 23

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.zero	1
	.size	.L.str.9, 1

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"r"
	.size	.L.str.10, 2

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"test.in"
	.size	.L.str.11, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%d"
	.size	.L.str.13, 3

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Recursion depth: %d\n"
	.size	.L.str.14, 21

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Alpha-Beta pruning: %s\n"
	.size	.L.str.15, 24

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"on"
	.size	.L.str.16, 3

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"off"
	.size	.L.str.17, 4

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"----------------"
	.size	.Lstr, 17

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"ERROR: Unknown player."
	.size	.Lstr.1, 23

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"ERROR: Could not open indata file"
	.size	.Lstr.2, 34

	.type	.Lstr.3,@object         # @str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.3:
	.asciz	"It's a tie."
	.size	.Lstr.3, 12

	.type	.Lstr.4,@object         # @str.4
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.4:
	.asciz	"The computer is the winner."
	.size	.Lstr.4, 28

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"The player is the winner."
	.size	.Lstr.5, 26

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"Using pruning method 2"
	.size	.Lstr.6, 23

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"Using pruning method 1"
	.size	.Lstr.7, 23


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
