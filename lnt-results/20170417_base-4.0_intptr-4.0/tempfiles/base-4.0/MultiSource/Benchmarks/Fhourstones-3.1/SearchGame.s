	.text
	.file	"SearchGame.bc"
	.globl	reset
	.p2align	4, 0x90
	.type	reset,@function
reset:                                  # @reset
	.cfi_startproc
# BB#0:
	movl	$0, nplies(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, color(%rip)
	movl	$353240832, height(%rip) # imm = 0x150E0700
	movw	$8988, height+4(%rip)   # imm = 0x231C
	movb	$42, height+6(%rip)
	retq
.Lfunc_end0:
	.size	reset, .Lfunc_end0-reset
	.cfi_endproc

	.globl	positioncode
	.p2align	4, 0x90
	.type	positioncode,@function
positioncode:                           # @positioncode
	.cfi_startproc
# BB#0:
	movl	nplies(%rip), %eax
	andl	$1, %eax
	movq	color(,%rax,8), %rcx
	addq	color(%rip), %rcx
	addq	color+8(%rip), %rcx
	movabsq	$4432676798593, %rax    # imm = 0x40810204081
	addq	%rcx, %rax
	retq
.Lfunc_end1:
	.size	positioncode, .Lfunc_end1-positioncode
	.cfi_endproc

	.globl	printMoves
	.p2align	4, 0x90
	.type	printMoves,@function
printMoves:                             # @printMoves
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	cmpl	$0, nplies(%rip)
	jle	.LBB2_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	moves(,%rbx,4), %esi
	incl	%esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	nplies(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB2_2
.LBB2_3:                                # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end2:
	.size	printMoves, .Lfunc_end2-printMoves
	.cfi_endproc

	.globl	islegal
	.p2align	4, 0x90
	.type	islegal,@function
islegal:                                # @islegal
	.cfi_startproc
# BB#0:
	movabsq	$283691315109952, %rcx  # imm = 0x1020408102040
	xorl	%eax, %eax
	testq	%rcx, %rdi
	sete	%al
	retq
.Lfunc_end3:
	.size	islegal, .Lfunc_end3-islegal
	.cfi_endproc

	.globl	isplayable
	.p2align	4, 0x90
	.type	isplayable,@function
isplayable:                             # @isplayable
	.cfi_startproc
# BB#0:
	movl	nplies(%rip), %eax
	andl	$1, %eax
	movslq	%edi, %rcx
	movb	height(%rcx), %cl
	movl	$1, %edx
	shlq	%cl, %rdx
	orq	color(,%rax,8), %rdx
	movabsq	$283691315109952, %rcx  # imm = 0x1020408102040
	xorl	%eax, %eax
	testq	%rcx, %rdx
	sete	%al
	retq
.Lfunc_end4:
	.size	isplayable, .Lfunc_end4-isplayable
	.cfi_endproc

	.globl	haswon
	.p2align	4, 0x90
	.type	haswon,@function
haswon:                                 # @haswon
	.cfi_startproc
# BB#0:
	movq	%rdi, %rcx
	shrq	$6, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$12, %rdx
	movl	$1, %eax
	testq	%rcx, %rdx
	jne	.LBB5_4
# BB#1:
	movq	%rdi, %rcx
	shrq	$7, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$14, %rdx
	testq	%rcx, %rdx
	jne	.LBB5_4
# BB#2:
	movq	%rdi, %rcx
	shrq	$8, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$16, %rdx
	testq	%rcx, %rdx
	jne	.LBB5_4
# BB#3:
	movq	%rdi, %rcx
	shrq	%rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$2, %rdx
	xorl	%eax, %eax
	testq	%rcx, %rdx
	setne	%al
.LBB5_4:
	retq
.Lfunc_end5:
	.size	haswon, .Lfunc_end5-haswon
	.cfi_endproc

	.globl	islegalhaswon
	.p2align	4, 0x90
	.type	islegalhaswon,@function
islegalhaswon:                          # @islegalhaswon
	.cfi_startproc
# BB#0:
	movabsq	$283691315109952, %rax  # imm = 0x1020408102040
	testq	%rax, %rdi
	je	.LBB6_2
# BB#1:
	xorl	%eax, %eax
	movzbl	%al, %eax
	retq
.LBB6_2:
	movq	%rdi, %rcx
	shrq	$6, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$12, %rdx
	movb	$1, %al
	testq	%rcx, %rdx
	jne	.LBB6_6
# BB#3:
	movq	%rdi, %rcx
	shrq	$7, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$14, %rdx
	testq	%rcx, %rdx
	jne	.LBB6_6
# BB#4:
	movq	%rdi, %rcx
	shrq	$8, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$16, %rdx
	testq	%rcx, %rdx
	jne	.LBB6_6
# BB#5:
	movq	%rdi, %rax
	shrq	%rax
	andq	%rdi, %rax
	movq	%rax, %rcx
	shrq	$2, %rcx
	testq	%rax, %rcx
	setne	%al
.LBB6_6:                                # %haswon.exit
	movzbl	%al, %eax
	retq
.Lfunc_end6:
	.size	islegalhaswon, .Lfunc_end6-islegalhaswon
	.cfi_endproc

	.globl	backmove
	.p2align	4, 0x90
	.type	backmove,@function
backmove:                               # @backmove
	.cfi_startproc
# BB#0:
	movslq	nplies(%rip), %rax
	leaq	-1(%rax), %rdx
	movl	%edx, nplies(%rip)
	movslq	moves-4(,%rax,4), %rax
	movb	height(%rax), %cl
	decb	%cl
	movb	%cl, height(%rax)
	movl	$1, %eax
	shlq	%cl, %rax
	andl	$1, %edx
	xorq	%rax, color(,%rdx,8)
	retq
.Lfunc_end7:
	.size	backmove, .Lfunc_end7-backmove
	.cfi_endproc

	.globl	makemove
	.p2align	4, 0x90
	.type	makemove,@function
makemove:                               # @makemove
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	movb	height(%rax), %cl
	movl	%ecx, %edx
	incb	%dl
	movb	%dl, height(%rax)
	movl	$1, %edx
	shlq	%cl, %rdx
	movslq	nplies(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%eax, moves(,%rcx,4)
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	xorq	%rdx, color(,%rcx,8)
	movl	%esi, nplies(%rip)
	retq
.Lfunc_end8:
	.size	makemove, .Lfunc_end8-makemove
	.cfi_endproc

	.globl	trans_init
	.p2align	4, 0x90
	.type	trans_init,@function
trans_init:                             # @trans_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 16
	movl	$8306069, %edi          # imm = 0x7EBD95
	movl	$8, %esi
	callq	calloc
	movq	%rax, ht(%rip)
	popq	%rax
	retq
.Lfunc_end9:
	.size	trans_init, .Lfunc_end9-trans_init
	.cfi_endproc

	.globl	emptyTT
	.p2align	4, 0x90
	.type	emptyTT,@function
emptyTT:                                # @emptyTT
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	jmp	.LBB10_1
	.p2align	4, 0x90
.LBB10_3:                               #   in Loop: Header=BB10_1 Depth=1
	movq	ht(%rip), %rcx
	movq	$0, 40(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 48(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 56(%rcx,%rax)
	addq	$64, %rax
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	movq	ht(%rip), %rcx
	movq	$0, (%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 8(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 16(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 24(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 32(%rcx,%rax)
	cmpq	$66448512, %rax         # imm = 0x3F5EC80
	jne	.LBB10_3
# BB#2:
	movq	$0, posed(%rip)
	retq
.Lfunc_end10:
	.size	emptyTT, .Lfunc_end10-emptyTT
	.cfi_endproc

	.globl	hash
	.p2align	4, 0x90
	.type	hash,@function
hash:                                   # @hash
	.cfi_startproc
# BB#0:
	movl	nplies(%rip), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	movq	color(,%rcx,8), %rdx
	addq	color(%rip), %rdx
	addq	color+8(%rip), %rdx
	movabsq	$4432676798593, %rcx    # imm = 0x40810204081
	addq	%rdx, %rcx
	cmpl	$9, %eax
	jg	.LBB11_5
# BB#1:                                 # %.preheader
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB11_4
# BB#2:                                 # %.lr.ph.preheader
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	shlq	$7, %rax
	movl	%edx, %esi
	andl	$127, %esi
	orq	%rsi, %rax
	shrq	$7, %rdx
	jne	.LBB11_3
.LBB11_4:                               # %._crit_edge
	cmpq	%rcx, %rax
	cmovbq	%rax, %rcx
.LBB11_5:
	movq	%rcx, %rax
	shrq	$23, %rax
	movl	%eax, lock(%rip)
	movabsq	$-9131717620722432537, %rdx # imm = 0x81459F34B3AA05E7
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$22, %rdx
	imull	$8306069, %edx, %eax    # imm = 0x7EBD95
	subl	%eax, %ecx
	movl	%ecx, htindex(%rip)
	retq
.Lfunc_end11:
	.size	hash, .Lfunc_end11-hash
	.cfi_endproc

	.globl	transpose
	.p2align	4, 0x90
	.type	transpose,@function
transpose:                              # @transpose
	.cfi_startproc
# BB#0:
	movl	nplies(%rip), %eax
	movl	%eax, %ecx
	andl	$1, %ecx
	movq	color(,%rcx,8), %rdx
	addq	color(%rip), %rdx
	addq	color+8(%rip), %rdx
	movabsq	$4432676798593, %rcx    # imm = 0x40810204081
	addq	%rdx, %rcx
	cmpl	$9, %eax
	jg	.LBB12_5
# BB#1:                                 # %.preheader.i
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB12_4
# BB#2:                                 # %.lr.ph.i.preheader
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	shlq	$7, %rax
	movl	%edx, %esi
	andl	$127, %esi
	orq	%rsi, %rax
	shrq	$7, %rdx
	jne	.LBB12_3
.LBB12_4:                               # %._crit_edge.i
	cmpq	%rcx, %rax
	cmovbq	%rax, %rcx
.LBB12_5:                               # %hash.exit
	movq	%rcx, %rsi
	shrq	$23, %rsi
	movl	%esi, lock(%rip)
	movabsq	$-9131717620722432537, %rdx # imm = 0x81459F34B3AA05E7
	movq	%rcx, %rax
	mulq	%rdx
	shrq	$22, %rdx
	imulq	$8306069, %rdx, %rax    # imm = 0x7EBD95
	subq	%rax, %rcx
	movl	%ecx, htindex(%rip)
	movq	ht(%rip), %rax
	movq	(%rax,%rcx,8), %rcx
	movl	%ecx, %eax
	andl	$67108863, %eax         # imm = 0x3FFFFFF
	cmpl	%esi, %eax
	jne	.LBB12_7
# BB#6:
	shrq	$61, %rcx
	jmp	.LBB12_9
.LBB12_7:
	movq	%rcx, %rdx
	shrq	$32, %rdx
	andl	$67108863, %edx         # imm = 0x3FFFFFF
	xorl	%eax, %eax
	cmpl	%esi, %edx
	jne	.LBB12_10
# BB#8:
	shrq	$58, %rcx
	andl	$7, %ecx
.LBB12_9:
	movl	%ecx, %eax
.LBB12_10:
	retq
.Lfunc_end12:
	.size	transpose, .Lfunc_end12-transpose
	.cfi_endproc

	.globl	transtore
	.p2align	4, 0x90
	.type	transtore,@function
transtore:                              # @transtore
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	incq	posed(%rip)
	movq	ht(%rip), %r8
	movslq	%edi, %r9
	movq	(%r8,%r9,8), %rax
	movl	%eax, %edi
	andl	$67108863, %edi         # imm = 0x3FFFFFF
	cmpl	%esi, %edi
	je	.LBB13_2
# BB#1:
	movl	%eax, %edi
	shrl	$26, %edi
	cmpl	%ecx, %edi
	jle	.LBB13_2
# BB#3:
	andl	$67108863, %esi         # imm = 0x3FFFFFF
	shlq	$32, %rsi
	movabsq	$-2305843004918726657, %rcx # imm = 0xE0000000FFFFFFFF
	andq	%rcx, %rax
	andl	$7, %edx
	shlq	$58, %rdx
	orq	%rax, %rsi
	movq	%rdx, %rcx
	jmp	.LBB13_4
.LBB13_2:
	movabsq	$2305843004918726656, %rdi # imm = 0x1FFFFFFF00000000
	andq	%rdi, %rax
	shlq	$61, %rdx
	shll	$26, %ecx
	andl	$67108863, %esi         # imm = 0x3FFFFFF
	orq	%rdx, %rsi
	orq	%rax, %rsi
.LBB13_4:
	orq	%rsi, %rcx
	movq	%rcx, (%r8,%r9,8)
	retq
.Lfunc_end13:
	.size	transtore, .Lfunc_end13-transtore
	.cfi_endproc

	.globl	htstat
	.p2align	4, 0x90
	.type	htstat,@function
htstat:                                 # @htstat
	.cfi_startproc
# BB#0:                                 # %.preheader24
	subq	$40, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 16(%rsp)
	movapd	%xmm0, (%rsp)
	xorl	%eax, %eax
	movq	ht(%rip), %rcx
	movabsq	$288230371856744448, %rdx # imm = 0x3FFFFFF00000000
	.p2align	4, 0x90
.LBB14_1:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rax,8), %rsi
	testl	$67108863, %esi         # imm = 0x3FFFFFF
	je	.LBB14_3
# BB#2:                                 #   in Loop: Header=BB14_1 Depth=1
	movq	%rsi, %rdi
	shrq	$61, %rdi
	incl	(%rsp,%rdi,4)
.LBB14_3:                               #   in Loop: Header=BB14_1 Depth=1
	testq	%rdx, %rsi
	je	.LBB14_5
# BB#4:                                 #   in Loop: Header=BB14_1 Depth=1
	shrq	$56, %rsi
	andl	$28, %esi
	incl	(%rsp,%rsi)
.LBB14_5:                               #   in Loop: Header=BB14_1 Depth=1
	incq	%rax
	cmpq	$8306069, %rax          # imm = 0x7EBD95
	jne	.LBB14_1
# BB#6:                                 # %.preheader.preheader
	movl	4(%rsp), %edi
	movl	8(%rsp), %r8d
	leal	(%r8,%rdi), %eax
	movl	12(%rsp), %ecx
	addl	%ecx, %eax
	movl	16(%rsp), %edx
	addl	%edx, %eax
	movl	20(%rsp), %esi
	addl	%esi, %eax
	jle	.LBB14_8
# BB#7:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	cvtsi2sdl	%eax, %xmm5
	divsd	%xmm5, %xmm0
	cvtsi2sdl	%r8d, %xmm1
	divsd	%xmm5, %xmm1
	cvtsi2sdl	%ecx, %xmm2
	divsd	%xmm5, %xmm2
	cvtsi2sdl	%edx, %xmm3
	divsd	%xmm5, %xmm3
	cvtsi2sdl	%esi, %xmm4
	divsd	%xmm5, %xmm4
	movl	$.L.str.1, %edi
	movb	$5, %al
	callq	printf
.LBB14_8:
	addq	$40, %rsp
	retq
.Lfunc_end14:
	.size	htstat, .Lfunc_end14-htstat
	.cfi_endproc

	.globl	millisecs
	.p2align	4, 0x90
	.type	millisecs,@function
millisecs:                              # @millisecs
	.cfi_startproc
# BB#0:
	movq	millisecs.Time(%rip), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, millisecs.Time(%rip)
	retq
.Lfunc_end15:
	.size	millisecs, .Lfunc_end15-millisecs
	.cfi_endproc

	.globl	min
	.p2align	4, 0x90
	.type	min,@function
min:                                    # @min
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	cmovlel	%edi, %esi
	movl	%esi, %eax
	retq
.Lfunc_end16:
	.size	min, .Lfunc_end16-min
	.cfi_endproc

	.globl	max
	.p2align	4, 0x90
	.type	max,@function
max:                                    # @max
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	cmovgel	%edi, %esi
	movl	%esi, %eax
	retq
.Lfunc_end17:
	.size	max, .Lfunc_end17-max
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	3                       # 0x3
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI18_1:
	.long	6                       # 0x6
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	6                       # 0x6
.LCPI18_2:
	.long	7                       # 0x7
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	13                      # 0xd
	.text
	.globl	inithistory
	.p2align	4, 0x90
	.type	inithistory,@function
inithistory:                            # @inithistory
	.cfi_startproc
# BB#0:
	movl	$3, history+20(%rip)
	movl	$3, history+188(%rip)
	movl	$4, history+16(%rip)
	movl	$4, history+184(%rip)
	movaps	.LCPI18_0(%rip), %xmm0  # xmm0 = [3,4,5,5]
	movups	%xmm0, history+168(%rip)
	movaps	%xmm0, history(%rip)
	movl	$4, history+140(%rip)
	movl	$4, history+48(%rip)
	movl	$4, history+160(%rip)
	movl	$4, history+28(%rip)
	movaps	.LCPI18_1(%rip), %xmm1  # xmm1 = [6,8,8,6]
	movaps	%xmm1, history+144(%rip)
	movaps	%xmm1, history+32(%rip)
	movl	$5, history+112(%rip)
	movl	$5, history+76(%rip)
	movl	$5, history+132(%rip)
	movl	$5, history+56(%rip)
	movl	$8, history+116(%rip)
	movl	$8, history+72(%rip)
	movl	$8, history+128(%rip)
	movl	$8, history+60(%rip)
	movl	$11, history+120(%rip)
	movl	$11, history+68(%rip)
	movl	$11, history+124(%rip)
	movl	$11, history+64(%rip)
	movl	$7, history+104(%rip)
	movl	$10, history+100(%rip)
	movaps	.LCPI18_2(%rip), %xmm2  # xmm2 = [7,10,13,13]
	movups	%xmm2, history+84(%rip)
	movl	$3, history+216(%rip)
	movl	$3, history+384(%rip)
	movl	$4, history+212(%rip)
	movl	$4, history+380(%rip)
	movups	%xmm0, history+364(%rip)
	movups	%xmm0, history+196(%rip)
	movl	$4, history+336(%rip)
	movl	$4, history+244(%rip)
	movl	$4, history+356(%rip)
	movl	$4, history+224(%rip)
	movups	%xmm1, history+340(%rip)
	movups	%xmm1, history+228(%rip)
	movl	$5, history+308(%rip)
	movl	$5, history+272(%rip)
	movl	$5, history+328(%rip)
	movl	$5, history+252(%rip)
	movl	$8, history+312(%rip)
	movl	$8, history+268(%rip)
	movl	$8, history+324(%rip)
	movl	$8, history+256(%rip)
	movl	$11, history+316(%rip)
	movl	$11, history+264(%rip)
	movl	$11, history+320(%rip)
	movl	$11, history+260(%rip)
	movl	$7, history+300(%rip)
	movl	$10, history+296(%rip)
	movups	%xmm2, history+280(%rip)
	retq
.Lfunc_end18:
	.size	inithistory, .Lfunc_end18-inithistory
	.cfi_endproc

	.globl	ab
	.p2align	4, 0x90
	.type	ab,@function
ab:                                     # @ab
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi10:
	.cfi_def_cfa_offset 224
.Lcfi11:
	.cfi_offset %rbx, -56
.Lcfi12:
	.cfi_offset %r12, -48
.Lcfi13:
	.cfi_offset %r13, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	incq	nodes(%rip)
	movl	%esi, %r14d
	movl	%edi, %r11d
	movslq	nplies(%rip), %r8
	cmpq	$41, %r8
	movl	$3, %ebx
	je	.LBB19_81
# BB#1:
	movl	%r8d, %r10d
	andl	$1, %r10d
	movl	%r10d, %eax
	xorl	$1, %eax
	movq	color(,%rax,8), %rax
	xorl	%edx, %edx
	movabsq	$283691315109952, %r9   # imm = 0x1020408102040
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movsbq	height(%rdx), %rcx
	movl	%ecx, %ecx
	movl	$1, %ebp
	shlq	%cl, %rbp
	orq	%rax, %rbp
	testq	%r9, %rbp
	jne	.LBB19_27
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	movl	$2, %ebx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shlq	%cl, %rbx
	orq	%rax, %rbx
	testq	%r9, %rbx
	je	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB19_9
.LBB19_5:                               #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rsi
	shrq	$6, %rsi
	andq	%rbx, %rsi
	movq	%rsi, %rdi
	shrq	$12, %rdi
	movb	$1, %cl
	testq	%rsi, %rdi
	jne	.LBB19_9
# BB#6:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rsi
	shrq	$7, %rsi
	andq	%rbx, %rsi
	movq	%rsi, %rdi
	shrq	$14, %rdi
	testq	%rsi, %rdi
	jne	.LBB19_9
# BB#7:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rsi
	shrq	$8, %rsi
	andq	%rbx, %rsi
	movq	%rsi, %rdi
	shrq	$16, %rdi
	testq	%rsi, %rdi
	jne	.LBB19_9
# BB#8:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	%rbx, %rcx
	shrq	%rcx
	andq	%rbx, %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	testq	%rcx, %rsi
	setne	%cl
	.p2align	4, 0x90
.LBB19_9:                               # %islegalhaswon.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rsi
	shrq	$6, %rsi
	andq	%rbp, %rsi
	movq	%rsi, %rdi
	shrq	$12, %rdi
	testq	%rsi, %rdi
	jne	.LBB19_13
# BB#10:                                #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rsi
	shrq	$7, %rsi
	andq	%rbp, %rsi
	movq	%rsi, %rdi
	shrq	$14, %rdi
	testq	%rsi, %rdi
	jne	.LBB19_13
# BB#11:                                #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rsi
	shrq	$8, %rsi
	andq	%rbp, %rsi
	movq	%rsi, %rdi
	shrq	$16, %rdi
	testq	%rsi, %rdi
	jne	.LBB19_13
# BB#12:                                # %haswon.exit
                                        #   in Loop: Header=BB19_2 Depth=1
	movq	%rbp, %rsi
	shrq	%rsi
	andq	%rbp, %rsi
	movq	%rsi, %rdi
	shrq	$2, %rdi
	testq	%rsi, %rdi
	jne	.LBB19_13
# BB#25:                                #   in Loop: Header=BB19_2 Depth=1
	testb	%cl, %cl
	jne	.LBB19_27
# BB#26:                                #   in Loop: Header=BB19_2 Depth=1
	movslq	%r15d, %rcx
	incl	%r15d
	movl	%edx, 16(%rsp,%rcx,4)
	.p2align	4, 0x90
.LBB19_27:                              #   in Loop: Header=BB19_2 Depth=1
	incq	%rdx
	cmpq	$7, %rdx
	jl	.LBB19_2
# BB#28:
	testl	%r15d, %r15d
	jne	.LBB19_18
# BB#29:
	movl	$1, %ebx
	jmp	.LBB19_81
.LBB19_13:                              # %haswon.exit.thread
	movl	$1, %ebx
	testb	%cl, %cl
	jne	.LBB19_81
# BB#14:
	movl	%edx, 16(%rsp)
	leal	1(%rdx), %ecx
	movl	$1, %r15d
	cmpl	$6, %ecx
	jg	.LBB19_18
# BB#15:                                # %.lr.ph174.preheader
	incq	%rdx
	.p2align	4, 0x90
.LBB19_16:                              # %.lr.ph174
                                        # =>This Inner Loop Header: Depth=1
	movzbl	height(%rdx), %ecx
	movl	$1, %ebp
	shlq	%cl, %rbp
	orq	%rax, %rbp
	testq	%r9, %rbp
	jne	.LBB19_17
# BB#21:                                #   in Loop: Header=BB19_16 Depth=1
	movq	%rbp, %rcx
	shrq	$6, %rcx
	andq	%rbp, %rcx
	movq	%rcx, %rsi
	shrq	$12, %rsi
	movl	$1, %ebx
	testq	%rcx, %rsi
	jne	.LBB19_81
# BB#22:                                #   in Loop: Header=BB19_16 Depth=1
	movq	%rbp, %rcx
	shrq	$7, %rcx
	andq	%rbp, %rcx
	movq	%rcx, %rsi
	shrq	$14, %rsi
	testq	%rcx, %rsi
	jne	.LBB19_81
# BB#23:                                #   in Loop: Header=BB19_16 Depth=1
	movq	%rbp, %rcx
	shrq	$8, %rcx
	andq	%rbp, %rcx
	movq	%rcx, %rsi
	shrq	$16, %rsi
	testq	%rcx, %rsi
	jne	.LBB19_81
# BB#24:                                # %islegalhaswon.exit148
                                        #   in Loop: Header=BB19_16 Depth=1
	movq	%rbp, %rcx
	shrq	%rcx
	andq	%rbp, %rcx
	movq	%rcx, %rsi
	shrq	$2, %rsi
	testq	%rcx, %rsi
	jne	.LBB19_81
.LBB19_17:                              # %islegalhaswon.exit148.thread149.backedge
                                        #   in Loop: Header=BB19_16 Depth=1
	incq	%rdx
	cmpq	$7, %rdx
	jl	.LBB19_16
.LBB19_18:                              # %.thread
	cmpl	$40, %r8d
	movl	$3, %ebx
	je	.LBB19_81
# BB#19:
	cmpl	$1, %r15d
	jne	.LBB19_30
# BB#20:
	movslq	16(%rsp), %rax
	movb	height(%rax), %cl
	movl	%ecx, %edx
	incb	%dl
	movb	%dl, height(%rax)
	movl	$1, %ebp
	movl	$1, %edx
	shlq	%cl, %rdx
	xorq	%rdx, color(,%r10,8)
	leal	1(%r8), %ecx
	movl	%ecx, nplies(%rip)
	movl	%eax, moves(,%r8,4)
	movl	$6, %ebx
	movl	$6, %edi
	subl	%r14d, %edi
	movl	$6, %esi
	subl	%r11d, %esi
	callq	ab
	subl	%eax, %ebx
	movslq	nplies(%rip), %rax
	leaq	-1(%rax), %rdx
	movl	%edx, nplies(%rip)
	movslq	moves-4(,%rax,4), %rax
	movb	height(%rax), %cl
	decb	%cl
	movb	%cl, height(%rax)
	shlq	%cl, %rbp
	andl	$1, %edx
	xorq	%rbp, color(,%rdx,8)
	jmp	.LBB19_81
.LBB19_30:
	movq	color(,%r10,8), %rax
	addq	color(%rip), %rax
	addq	color+8(%rip), %rax
	movabsq	$4432676798593, %rsi    # imm = 0x40810204081
	addq	%rax, %rsi
	cmpl	$9, %r8d
	jg	.LBB19_35
# BB#31:                                # %.preheader.i.i
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.LBB19_34
# BB#32:                                # %.lr.ph.i.i.preheader
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB19_33:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	shlq	$7, %rax
	movl	%ecx, %edx
	andl	$127, %edx
	orq	%rdx, %rax
	shrq	$7, %rcx
	jne	.LBB19_33
.LBB19_34:                              # %._crit_edge.i.i
	cmpq	%rsi, %rax
	cmovbq	%rax, %rsi
.LBB19_35:                              # %hash.exit.i
	movq	%rsi, %rdi
	shrq	$23, %rdi
	movl	%edi, lock(%rip)
	movabsq	$-9131717620722432537, %rcx # imm = 0x81459F34B3AA05E7
	movq	%rsi, %rax
	mulq	%rcx
	shrq	$22, %rdx
	imulq	$8306069, %rdx, %rax    # imm = 0x7EBD95
	subq	%rax, %rsi
	movl	%esi, htindex(%rip)
	movq	ht(%rip), %rax
	movq	(%rax,%rsi,8), %rbx
	movl	%ebx, %eax
	andl	$67108863, %eax         # imm = 0x3FFFFFF
	cmpl	%edi, %eax
	jne	.LBB19_37
# BB#36:
	shrq	$61, %rbx
	testl	%ebx, %ebx
	jne	.LBB19_41
	jmp	.LBB19_40
.LBB19_37:
	movq	%rbx, %rax
	shrq	$32, %rax
	andl	$67108863, %eax         # imm = 0x3FFFFFF
	xorl	%ecx, %ecx
	cmpl	%edi, %eax
	jne	.LBB19_45
# BB#38:
	shrq	$58, %rbx
	andl	$7, %ebx
	testl	%ebx, %ebx
	je	.LBB19_40
.LBB19_41:                              # %transpose.exit
	cmpl	$4, %ebx
	je	.LBB19_44
# BB#42:                                # %transpose.exit
	cmpl	$2, %ebx
	jne	.LBB19_81
# BB#43:
	movl	$3, %r14d
	movl	$2, %ecx
	cmpl	$2, %r11d
	movl	$2, %ebx
	jle	.LBB19_45
	jmp	.LBB19_81
.LBB19_40:
	xorl	%ecx, %ecx
	jmp	.LBB19_45
.LBB19_44:
	movl	$3, %r11d
	movl	$4, %ecx
	cmpl	$4, %r14d
	movl	$4, %ebx
	jl	.LBB19_81
.LBB19_45:                              # %transpose.exit.thread
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	movq	%r10, 56(%rsp)          # 8-byte Spill
	movq	posed(%rip), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	$6, %eax
	movl	%r14d, 76(%rsp)         # 4-byte Spill
	subl	%r14d, %eax
	movl	%eax, 100(%rsp)         # 4-byte Spill
	movslq	%r15d, %rcx
	leal	1(%r15), %esi
	movq	%r15, 112(%rsp)         # 8-byte Spill
	leal	-2(%r15), %edi
	xorl	%ebp, %ebp
	movl	$1, %edx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movl	%esi, 84(%rsp)          # 4-byte Spill
	movl	%edi, 80(%rsp)          # 4-byte Spill
.LBB19_46:                              # %.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_47 Depth 2
                                        #       Child Loop BB19_48 Depth 3
                                        #         Child Loop BB19_54 Depth 4
                                        #         Child Loop BB19_58 Depth 4
                                        #         Child Loop BB19_60 Depth 4
	movl	$6, %eax
	subl	%r11d, %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	%r11d, 88(%rsp)         # 4-byte Spill
.LBB19_47:                              # %.outer
                                        #   Parent Loop BB19_46 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB19_48 Depth 3
                                        #         Child Loop BB19_54 Depth 4
                                        #         Child Loop BB19_58 Depth 4
                                        #         Child Loop BB19_60 Depth 4
	movl	%ebp, %ebx
	movslq	%ebx, %rbp
	subl	%ebp, %esi
	movl	%esi, 96(%rsp)          # 4-byte Spill
	subl	%ebp, %edi
	movl	%edi, 92(%rsp)          # 4-byte Spill
	movq	%rbp, %rax
	notq	%rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movl	%ebp, %r11d
	negb	%r11b
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB19_48:                              #   Parent Loop BB19_46 Depth=1
                                        #     Parent Loop BB19_47 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB19_54 Depth 4
                                        #         Child Loop BB19_58 Depth 4
                                        #         Child Loop BB19_60 Depth 4
	movl	%ebx, %eax
	movq	%rbp, %r12
	cmpq	%rcx, %r12
	jge	.LBB19_73
# BB#49:                                #   in Loop: Header=BB19_48 Depth=3
	movq	%rax, %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	leal	1(%rax), %r10d
	leaq	1(%r12), %r9
	cmpq	%rcx, %r9
	movl	%r12d, %edi
	movq	112(%rsp), %rbp         # 8-byte Reload
	jge	.LBB19_55
# BB#50:                                # %.lr.ph167.preheader
                                        #   in Loop: Header=BB19_48 Depth=3
	movl	96(%rsp), %esi          # 4-byte Reload
	subl	%r15d, %esi
	movslq	%r10d, %rax
	movslq	16(%rsp,%r12,4), %rcx
	movsbq	height(%rcx), %rdx
	imulq	$196, 56(%rsp), %rcx    # 8-byte Folded Reload
	movl	history(%rcx,%rdx,4), %edx
	testb	$1, %sil
	jne	.LBB19_52
# BB#51:                                #   in Loop: Header=BB19_48 Depth=3
	movl	%r12d, %edi
	cmpl	%r15d, 92(%rsp)         # 4-byte Folded Reload
	jne	.LBB19_54
	jmp	.LBB19_55
	.p2align	4, 0x90
.LBB19_52:                              # %.lr.ph167.prol
                                        #   in Loop: Header=BB19_48 Depth=3
	movslq	16(%rsp,%rax,4), %rsi
	movsbq	height(%rsi), %rsi
	movl	history(%rcx,%rsi,4), %esi
	cmpl	%edx, %esi
	cmovgel	%esi, %edx
	movl	%r12d, %edi
	cmovgl	%eax, %edi
	incq	%rax
	cmpl	%r15d, 92(%rsp)         # 4-byte Folded Reload
	je	.LBB19_55
	.p2align	4, 0x90
.LBB19_54:                              # %.lr.ph167
                                        #   Parent Loop BB19_46 Depth=1
                                        #     Parent Loop BB19_47 Depth=2
                                        #       Parent Loop BB19_48 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movslq	16(%rsp,%rax,4), %rsi
	movsbq	height(%rsi), %rsi
	movl	history(%rcx,%rsi,4), %esi
	cmpl	%edx, %esi
	cmovgel	%esi, %edx
	cmovgl	%eax, %edi
	movslq	20(%rsp,%rax,4), %rsi
	movsbq	height(%rsi), %rsi
	movl	history(%rcx,%rsi,4), %esi
	leal	1(%rax), %ebx
	cmpl	%edx, %esi
	cmovgel	%esi, %edx
	cmovgl	%ebx, %edi
	addq	$2, %rax
	cmpl	%ebp, %eax
	jne	.LBB19_54
.LBB19_55:                              # %._crit_edge168
                                        #   in Loop: Header=BB19_48 Depth=3
	movl	%r10d, 108(%rsp)        # 4-byte Spill
	movq	%r9, %rbp
	movslq	%edi, %rax
	movl	16(%rsp,%rax,4), %ecx
	movslq	%ecx, %r13
	cmpq	%r12, %rax
	jle	.LBB19_61
# BB#56:                                # %.lr.ph171.preheader
                                        #   in Loop: Header=BB19_48 Depth=3
	movq	144(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%r14), %rdx
	movq	136(%rsp), %rcx         # 8-byte Reload
	subq	%r14, %rcx
	movl	%eax, %esi
	subl	%edx, %esi
	addq	%rax, %rcx
	testb	$7, %sil
	je	.LBB19_59
# BB#57:                                # %.lr.ph171.prol.preheader
                                        #   in Loop: Header=BB19_48 Depth=3
	movl	%r11d, %edx
	addb	%al, %dl
	movzbl	%dl, %edx
	andl	$7, %edx
	negq	%rdx
	.p2align	4, 0x90
.LBB19_58:                              # %.lr.ph171.prol
                                        #   Parent Loop BB19_46 Depth=1
                                        #     Parent Loop BB19_47 Depth=2
                                        #       Parent Loop BB19_48 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	12(%rsp,%rax,4), %esi
	movl	%esi, 16(%rsp,%rax,4)
	decq	%rax
	incq	%rdx
	jne	.LBB19_58
.LBB19_59:                              # %.lr.ph171.prol.loopexit
                                        #   in Loop: Header=BB19_48 Depth=3
	cmpq	$7, %rcx
	jb	.LBB19_61
	.p2align	4, 0x90
.LBB19_60:                              # %.lr.ph171
                                        #   Parent Loop BB19_46 Depth=1
                                        #     Parent Loop BB19_47 Depth=2
                                        #       Parent Loop BB19_48 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	12(%rsp,%rax,4), %ecx
	movl	%ecx, 16(%rsp,%rax,4)
	movl	8(%rsp,%rax,4), %ecx
	movl	%ecx, 12(%rsp,%rax,4)
	movl	4(%rsp,%rax,4), %ecx
	movl	%ecx, 8(%rsp,%rax,4)
	movl	(%rsp,%rax,4), %ecx
	movl	%ecx, 4(%rsp,%rax,4)
	movl	-4(%rsp,%rax,4), %ecx
	movl	%ecx, (%rsp,%rax,4)
	movl	-8(%rsp,%rax,4), %ecx
	movl	%ecx, -4(%rsp,%rax,4)
	movl	-12(%rsp,%rax,4), %ecx
	movl	%ecx, -8(%rsp,%rax,4)
	movl	-16(%rsp,%rax,4), %ecx
	movl	%ecx, -12(%rsp,%rax,4)
	leaq	-8(%rax), %rax
	cmpq	%r12, %rax
	jg	.LBB19_60
.LBB19_61:                              # %._crit_edge172
                                        #   in Loop: Header=BB19_48 Depth=3
	movl	%r13d, 16(%rsp,%r12,4)
	movb	height(%r13), %cl
	movl	%ecx, %eax
	incb	%al
	movb	%al, height(%r13)
	movl	$1, %eax
	shlq	%cl, %rax
	movl	%r8d, %ecx
	andl	$1, %ecx
	xorq	%rax, color(,%rcx,8)
	leal	1(%r8), %eax
	movl	%eax, nplies(%rip)
	movslq	%r8d, %rax
	movl	%r13d, moves(,%rax,4)
	movl	100(%rsp), %edi         # 4-byte Reload
	movl	104(%rsp), %esi         # 4-byte Reload
	movl	%r11d, %ebx
	callq	ab
	movl	%ebx, %r11d
	movl	$6, %edx
	subl	%eax, %edx
	movslq	nplies(%rip), %rax
	leaq	-1(%rax), %r8
	movl	%r8d, nplies(%rip)
	movslq	moves-4(,%rax,4), %rsi
	movb	height(%rsi), %cl
	decb	%cl
	movb	%cl, height(%rsi)
	movl	$1, %esi
	shlq	%cl, %rsi
	movl	%r8d, %ecx
	andl	$1, %ecx
	xorq	%rsi, color(,%rcx,8)
	incl	%r15d
	incq	%r14
	addb	$7, %r11b
	cmpl	12(%rsp), %edx          # 4-byte Folded Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	movl	108(%rsp), %ebx         # 4-byte Reload
	jle	.LBB19_48
# BB#62:                                #   in Loop: Header=BB19_47 Depth=2
	movl	88(%rsp), %r11d         # 4-byte Reload
	cmpl	%r11d, %edx
	movl	%edx, 12(%rsp)          # 4-byte Spill
	movl	84(%rsp), %esi          # 4-byte Reload
	movl	80(%rsp), %edi          # 4-byte Reload
	jle	.LBB19_47
# BB#63:                                #   in Loop: Header=BB19_47 Depth=2
	testl	%eax, %eax
	movl	%edx, 12(%rsp)          # 4-byte Spill
	jle	.LBB19_47
# BB#64:                                #   in Loop: Header=BB19_46 Depth=1
	cmpl	76(%rsp), %edx          # 4-byte Folded Reload
	movl	%edx, %r11d
	jl	.LBB19_46
# BB#65:
	movq	112(%rsp), %rax         # 8-byte Reload
	decl	%eax
	cmpl	$3, %edx
	movl	$4, %ecx
	cmovnel	%edx, %ecx
	cmpl	%eax, %r12d
	cmovgel	%edx, %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	testl	%r12d, %r12d
	jle	.LBB19_73
# BB#66:                                # %.lr.ph.preheader
	movl	%r12d, %eax
	testb	$1, %r12b
	jne	.LBB19_68
# BB#67:
	xorl	%ecx, %ecx
	cmpq	$1, %rax
	jne	.LBB19_70
	jmp	.LBB19_72
.LBB19_68:                              # %.lr.ph.prol
	movslq	16(%rsp), %rcx
	movsbq	height(%rcx), %rcx
	imulq	$196, 56(%rsp), %rdx    # 8-byte Folded Reload
	decl	history(%rdx,%rcx,4)
	movl	$1, %ecx
	cmpq	$1, %rax
	je	.LBB19_72
.LBB19_70:                              # %.lr.ph.preheader.new
	movl	152(%rsp), %eax         # 4-byte Reload
	subq	%rcx, %rax
	leaq	20(%rsp,%rcx,4), %rcx
	imulq	$196, 56(%rsp), %rdx    # 8-byte Folded Reload
.LBB19_71:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	-4(%rcx), %rsi
	movsbq	height(%rsi), %rsi
	decl	history(%rdx,%rsi,4)
	movslq	(%rcx), %rsi
	movsbq	height(%rsi), %rsi
	decl	history(%rdx,%rsi,4)
	addq	$8, %rcx
	addq	$-2, %rax
	jne	.LBB19_71
.LBB19_72:                              # %._crit_edge
	movsbq	height(%r13), %rax
	imulq	$196, 56(%rsp), %rcx    # 8-byte Folded Reload
	addl	%r12d, history(%rcx,%rax,4)
.LBB19_73:                              # %.loopexit
	movl	52(%rsp), %eax          # 4-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	$6, %esi
	subl	%eax, %esi
	movq	posed(%rip), %rax
	movq	%rax, %rdi
	subq	%rcx, %rdi
	movl	$-1, %edx
	movl	$-67108864, %ecx        # imm = 0xFC000000
	movq	128(%rsp), %r8          # 8-byte Reload
	movq	120(%rsp), %rbp         # 8-byte Reload
	.p2align	4, 0x90
.LBB19_74:                              # =>This Inner Loop Header: Depth=1
	shrq	%rdi
	incl	%edx
	addl	$67108864, %ecx         # imm = 0x4000000
	testq	%rdi, %rdi
	jne	.LBB19_74
# BB#75:
	movl	12(%rsp), %edi          # 4-byte Reload
	cmpl	%esi, %edi
	movl	$3, %ebx
	cmovnel	%edi, %ebx
	incq	%rax
	movq	%rax, posed(%rip)
	movq	ht(%rip), %rax
	movq	(%rax,%r8,8), %rsi
	movl	%esi, %edi
	andl	$67108863, %edi         # imm = 0x3FFFFFF
	cmpl	%ebp, %edi
	je	.LBB19_77
# BB#76:
	movl	%esi, %edi
	shrl	$26, %edi
	cmpl	%edx, %edi
	jle	.LBB19_77
# BB#78:
	movabsq	$-2305843004918726657, %rcx # imm = 0xE0000000FFFFFFFF
	andq	%rcx, %rsi
	movl	%ebx, %ecx
	andl	$7, %ecx
	shlq	$58, %rcx
	andq	$67108863, %rbp         # imm = 0x3FFFFFF
	shlq	$32, %rbp
	jmp	.LBB19_79
.LBB19_77:
	movabsq	$2305843004918726656, %rdi # imm = 0x1FFFFFFF00000000
	andq	%rdi, %rsi
	movq	%rbx, %rdi
	shlq	$61, %rdi
	movl	%ecx, %ecx
	andl	$67108863, %ebp         # imm = 0x3FFFFFF
	orq	%rdi, %rbp
.LBB19_79:                              # %transtore.exit
	orq	%rsi, %rbp
	orq	%rbp, %rcx
	movq	%rcx, (%rax,%r8,8)
	cmpl	$0, nplies(%rip)
	jns	.LBB19_81
# BB#80:                                # %printMoves.exit
	movslq	%ebx, %rax
	movsbl	.L.str.3(%rax), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
.LBB19_81:                              # %islegalhaswon.exit148.thread
	movl	%ebx, %eax
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	ab, .Lfunc_end19-ab
	.cfi_endproc

	.globl	solve
	.p2align	4, 0x90
	.type	solve,@function
solve:                                  # @solve
	.cfi_startproc
# BB#0:
	movl	nplies(%rip), %ecx
	andl	$1, %ecx
	movl	%ecx, %eax
	xorl	$1, %eax
	movq	$0, nodes(%rip)
	movq	$1, msecs(%rip)
	movq	color(,%rax,8), %rdx
	movq	%rdx, %rsi
	shrq	$6, %rsi
	andq	%rdx, %rsi
	movq	%rsi, %rdi
	shrq	$12, %rdi
	movl	$1, %eax
	testq	%rsi, %rdi
	jne	.LBB20_12
# BB#1:
	movq	%rdx, %rsi
	shrq	$7, %rsi
	andq	%rdx, %rsi
	movq	%rsi, %rdi
	shrq	$14, %rdi
	testq	%rsi, %rdi
	jne	.LBB20_12
# BB#2:
	movq	%rdx, %rsi
	shrq	$8, %rsi
	andq	%rdx, %rsi
	movq	%rsi, %rdi
	shrq	$16, %rdi
	testq	%rsi, %rdi
	jne	.LBB20_12
# BB#3:                                 # %haswon.exit
	movq	%rdx, %rsi
	shrq	%rsi
	andq	%rdx, %rsi
	movq	%rsi, %rdx
	shrq	$2, %rdx
	testq	%rsi, %rdx
	jne	.LBB20_12
# BB#4:                                 # %.preheader
	movq	color(,%rcx,8), %r9
	xorl	%esi, %esi
	movabsq	$283691315109952, %r8   # imm = 0x1020408102040
	.p2align	4, 0x90
.LBB20_5:                               # =>This Inner Loop Header: Depth=1
	movzbl	height(%rsi), %ecx
	movl	$1, %edi
	shlq	%cl, %rdi
	orq	%r9, %rdi
	testq	%r8, %rdi
	jne	.LBB20_10
# BB#6:                                 #   in Loop: Header=BB20_5 Depth=1
	movq	%rdi, %rcx
	shrq	$6, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$12, %rdx
	movl	$5, %eax
	testq	%rcx, %rdx
	jne	.LBB20_12
# BB#7:                                 #   in Loop: Header=BB20_5 Depth=1
	movq	%rdi, %rcx
	shrq	$7, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$14, %rdx
	testq	%rcx, %rdx
	jne	.LBB20_12
# BB#8:                                 #   in Loop: Header=BB20_5 Depth=1
	movq	%rdi, %rcx
	shrq	$8, %rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$16, %rdx
	testq	%rcx, %rdx
	jne	.LBB20_12
# BB#9:                                 # %islegalhaswon.exit
                                        #   in Loop: Header=BB20_5 Depth=1
	movq	%rdi, %rcx
	shrq	%rcx
	andq	%rdi, %rcx
	movq	%rcx, %rdx
	shrq	$2, %rdx
	testq	%rcx, %rdx
	jne	.LBB20_12
.LBB20_10:                              # %islegalhaswon.exit.thread16
                                        #   in Loop: Header=BB20_5 Depth=1
	incq	%rsi
	cmpq	$7, %rsi
	jl	.LBB20_5
# BB#11:
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 16
	callq	inithistory
	movq	millisecs.Time(%rip), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, millisecs.Time(%rip)
	movq	%rax, msecs(%rip)
	movl	$1, %edi
	movl	$5, %esi
	callq	ab
	movq	millisecs.Time(%rip), %rcx
	incq	%rcx
	movq	%rcx, millisecs.Time(%rip)
	subq	msecs(%rip), %rcx
	movq	%rcx, msecs(%rip)
	popq	%rcx
.LBB20_12:                              # %haswon.exit.thread
	retq
.Lfunc_end20:
	.size	solve, .Lfunc_end20-solve
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI21_0:
	.zero	16
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movl	$8306069, %edi          # imm = 0x7EBD95
	movl	$8, %esi
	callq	calloc
	movq	%rax, ht(%rip)
	movl	$.L.str.4, %edi
	callq	puts
	movl	$.L.str.5, %edi
	movl	$7, %esi
	movl	$6, %edx
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.6, %edi
	movl	$8306069, %esi          # imm = 0x7EBD95
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB21_1
	.p2align	4, 0x90
.LBB21_13:                              #   in Loop: Header=BB21_1 Depth=1
	movslq	%esi, %rax
	movsbl	.L.str.3(%rax), %edx
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	callq	printf
	callq	htstat
.LBB21_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_2 Depth 2
                                        #     Child Loop BB21_8 Depth 2
                                        #     Child Loop BB21_10 Depth 2
                                        #     Child Loop BB21_12 Depth 2
	movl	$0, nplies(%rip)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, color(%rip)
	movl	$353240832, height(%rip) # imm = 0x150E0700
	movw	$8988, height+4(%rip)   # imm = 0x231C
	movb	$42, height+6(%rip)
	jmp	.LBB21_2
	.p2align	4, 0x90
.LBB21_4:                               #   in Loop: Header=BB21_2 Depth=2
	movslq	%edx, %rax
	movzbl	height(%rax), %ecx
	movl	%ecx, %ebx
	incb	%bl
	movb	%bl, height(%rax)
	movl	$1, %eax
	shlq	%cl, %rax
	movslq	nplies(%rip), %rcx
	leal	1(%rcx), %esi
	movl	%edx, moves(,%rcx,4)
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1, %ecx
	xorq	%rax, color(,%rcx,8)
	movl	%esi, nplies(%rip)
.LBB21_2:                               #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	stdin(%rip), %rdi
	callq	_IO_getc
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	cmpl	$-1, %eax
	je	.LBB21_14
# BB#3:                                 #   in Loop: Header=BB21_2 Depth=2
	leal	-49(%rax), %edx
	cmpl	$6, %edx
	jbe	.LBB21_4
# BB#5:                                 #   in Loop: Header=BB21_2 Depth=2
	cmpl	$10, %eax
	jne	.LBB21_2
# BB#6:                                 #   in Loop: Header=BB21_1 Depth=1
	movl	nplies(%rip), %esi
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, nplies(%rip)
	jle	.LBB21_9
# BB#7:                                 # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB21_1 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB21_8:                               # %.lr.ph.i
                                        #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	moves(,%rbx,4), %esi
	incl	%esi
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	movslq	nplies(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB21_8
.LBB21_9:                               # %printMoves.exit
                                        #   in Loop: Header=BB21_1 Depth=1
	movl	$.L.str.8, %edi
	callq	puts
	xorl	%eax, %eax
	jmp	.LBB21_10
	.p2align	4, 0x90
.LBB21_15:                              #   in Loop: Header=BB21_10 Depth=2
	movq	ht(%rip), %rcx
	movq	$0, 40(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 48(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 56(%rcx,%rax)
	addq	$64, %rax
.LBB21_10:                              #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	ht(%rip), %rcx
	movq	$0, (%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 8(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 16(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 24(%rcx,%rax)
	movq	ht(%rip), %rcx
	movq	$0, 32(%rcx,%rax)
	cmpq	$66448512, %rax         # imm = 0x3F5EC80
	jne	.LBB21_15
# BB#11:                                # %emptyTT.exit
                                        #   in Loop: Header=BB21_1 Depth=1
	movq	$0, posed(%rip)
	callq	solve
	movl	%eax, %esi
	movl	$-1, %ecx
	movq	posed(%rip), %rax
	.p2align	4, 0x90
.LBB21_12:                              #   Parent Loop BB21_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	shrq	%rax
	incl	%ecx
	testq	%rax, %rax
	jne	.LBB21_12
	jmp	.LBB21_13
.LBB21_14:                              # %._crit_edge
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end21:
	.size	main, .Lfunc_end21-main
	.cfi_endproc

	.type	nplies,@object          # @nplies
	.comm	nplies,4,4
	.type	color,@object           # @color
	.comm	color,16,16
	.type	height,@object          # @height
	.comm	height,7,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d"
	.size	.L.str, 3

	.type	moves,@object           # @moves
	.comm	moves,168,16
	.type	ht,@object              # @ht
	.comm	ht,8,8
	.type	posed,@object           # @posed
	.comm	posed,8,8
	.type	lock,@object            # @lock
	.comm	lock,4,4
	.type	htindex,@object         # @htindex
	.comm	htindex,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"- %5.3f  < %5.3f  = %5.3f  > %5.3f  + %5.3f\n"
	.size	.L.str.1, 45

	.type	millisecs.Time,@object  # @millisecs.Time
	.local	millisecs.Time
	.comm	millisecs.Time,8,8
	.type	history,@object         # @history
	.comm	history,392,16
	.type	nodes,@object           # @nodes
	.comm	nodes,8,8
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%c%d\n"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"#-<=>+"
	.size	.L.str.3, 7

	.type	msecs,@object           # @msecs
	.comm	msecs,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Fhourstones 3.1 (C)"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Boardsize = %dx%d\n"
	.size	.L.str.5, 19

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Using %d transposition table entries.\n"
	.size	.L.str.6, 39

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"\nSolving %d-ply position after "
	.size	.L.str.7, 32

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	" . . ."
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"score = %d (%c)  work = %d\n"
	.size	.L.str.9, 28


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
