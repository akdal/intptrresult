	.text
	.file	"primes.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 64
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movq	$0, 16(%rsp)
	movq	$_ZTV7counter+16, 8(%rsp)
	movl	$2, 24(%rsp)
	leaq	8(%rsp), %rbx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	%eax, %r14d
	movl	$24, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$_ZTV6filter+16, (%rbp)
	movl	%r14d, 16(%rbp)
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	cmpl	$100001, %r14d          # imm = 0x186A1
	movq	%rbp, %rbx
	jl	.LBB0_1
# BB#2:
	movl	$10, %edi
	callq	putchar
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.globl	_ZN5sieve3outEv
	.p2align	4, 0x90
	.type	_ZN5sieve3outEv,@function
_ZN5sieve3outEv:                        # @_ZN5sieve3outEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %ebp
	movl	$24, %edi
	callq	_Znwm
	movq	8(%rbx), %rcx
	movq	%rcx, 8(%rax)
	movq	$_ZTV6filter+16, (%rax)
	movl	%ebp, 16(%rax)
	movq	%rax, 8(%rbx)
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN5sieve3outEv, .Lfunc_end1-_ZN5sieve3outEv
	.cfi_endproc

	.globl	_ZN6filter3outEv
	.p2align	4, 0x90
	.type	_ZN6filter3outEv,@function
_ZN6filter3outEv:                       # @_ZN6filter3outEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*(%rax)
	movl	%eax, %ecx
	cltd
	idivl	16(%rbx)
	testl	%edx, %edx
	je	.LBB2_1
# BB#2:
	movl	%ecx, %eax
	popq	%rbx
	retq
.Lfunc_end2:
	.size	_ZN6filter3outEv, .Lfunc_end2-_ZN6filter3outEv
	.cfi_endproc

	.section	.text._ZN7counter3outEv,"axG",@progbits,_ZN7counter3outEv,comdat
	.weak	_ZN7counter3outEv
	.p2align	4, 0x90
	.type	_ZN7counter3outEv,@function
_ZN7counter3outEv:                      # @_ZN7counter3outEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rdi)
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	retq
.Lfunc_end3:
	.size	_ZN7counter3outEv, .Lfunc_end3-_ZN7counter3outEv
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%d "
	.size	.L.str, 4

	.type	_ZTV5sieve,@object      # @_ZTV5sieve
	.section	.rodata,"a",@progbits
	.globl	_ZTV5sieve
	.p2align	3
_ZTV5sieve:
	.quad	0
	.quad	_ZTI5sieve
	.quad	_ZN5sieve3outEv
	.size	_ZTV5sieve, 24

	.type	_ZTS5sieve,@object      # @_ZTS5sieve
	.globl	_ZTS5sieve
_ZTS5sieve:
	.asciz	"5sieve"
	.size	_ZTS5sieve, 7

	.type	_ZTS4item,@object       # @_ZTS4item
	.section	.rodata._ZTS4item,"aG",@progbits,_ZTS4item,comdat
	.weak	_ZTS4item
_ZTS4item:
	.asciz	"4item"
	.size	_ZTS4item, 6

	.type	_ZTI4item,@object       # @_ZTI4item
	.section	.rodata._ZTI4item,"aG",@progbits,_ZTI4item,comdat
	.weak	_ZTI4item
	.p2align	3
_ZTI4item:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS4item
	.size	_ZTI4item, 16

	.type	_ZTI5sieve,@object      # @_ZTI5sieve
	.section	.rodata,"a",@progbits
	.globl	_ZTI5sieve
	.p2align	4
_ZTI5sieve:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS5sieve
	.quad	_ZTI4item
	.size	_ZTI5sieve, 24

	.type	_ZTV6filter,@object     # @_ZTV6filter
	.globl	_ZTV6filter
	.p2align	3
_ZTV6filter:
	.quad	0
	.quad	_ZTI6filter
	.quad	_ZN6filter3outEv
	.size	_ZTV6filter, 24

	.type	_ZTS6filter,@object     # @_ZTS6filter
	.globl	_ZTS6filter
_ZTS6filter:
	.asciz	"6filter"
	.size	_ZTS6filter, 8

	.type	_ZTI6filter,@object     # @_ZTI6filter
	.globl	_ZTI6filter
	.p2align	4
_ZTI6filter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS6filter
	.quad	_ZTI4item
	.size	_ZTI6filter, 24

	.type	_ZTV7counter,@object    # @_ZTV7counter
	.section	.rodata._ZTV7counter,"aG",@progbits,_ZTV7counter,comdat
	.weak	_ZTV7counter
	.p2align	3
_ZTV7counter:
	.quad	0
	.quad	_ZTI7counter
	.quad	_ZN7counter3outEv
	.size	_ZTV7counter, 24

	.type	_ZTS7counter,@object    # @_ZTS7counter
	.section	.rodata._ZTS7counter,"aG",@progbits,_ZTS7counter,comdat
	.weak	_ZTS7counter
_ZTS7counter:
	.asciz	"7counter"
	.size	_ZTS7counter, 9

	.type	_ZTI7counter,@object    # @_ZTI7counter
	.section	.rodata._ZTI7counter,"aG",@progbits,_ZTI7counter,comdat
	.weak	_ZTI7counter
	.p2align	4
_ZTI7counter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS7counter
	.quad	_ZTI4item
	.size	_ZTI7counter, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
