	.text
	.file	"gim_memory.bc"
	.globl	_Z21gim_set_alloc_handlerPFPvmE
	.p2align	4, 0x90
	.type	_Z21gim_set_alloc_handlerPFPvmE,@function
_Z21gim_set_alloc_handlerPFPvmE:        # @_Z21gim_set_alloc_handlerPFPvmE
	.cfi_startproc
# BB#0:
	movq	%rdi, _ZL9g_allocfn(%rip)
	retq
.Lfunc_end0:
	.size	_Z21gim_set_alloc_handlerPFPvmE, .Lfunc_end0-_Z21gim_set_alloc_handlerPFPvmE
	.cfi_endproc

	.globl	_Z22gim_set_alloca_handlerPFPvmE
	.p2align	4, 0x90
	.type	_Z22gim_set_alloca_handlerPFPvmE,@function
_Z22gim_set_alloca_handlerPFPvmE:       # @_Z22gim_set_alloca_handlerPFPvmE
	.cfi_startproc
# BB#0:
	movq	%rdi, _ZL10g_allocafn(%rip)
	retq
.Lfunc_end1:
	.size	_Z22gim_set_alloca_handlerPFPvmE, .Lfunc_end1-_Z22gim_set_alloca_handlerPFPvmE
	.cfi_endproc

	.globl	_Z23gim_set_realloc_handlerPFPvS_mmE
	.p2align	4, 0x90
	.type	_Z23gim_set_realloc_handlerPFPvS_mmE,@function
_Z23gim_set_realloc_handlerPFPvS_mmE:   # @_Z23gim_set_realloc_handlerPFPvS_mmE
	.cfi_startproc
# BB#0:
	movq	%rdi, _ZL11g_reallocfn(%rip)
	retq
.Lfunc_end2:
	.size	_Z23gim_set_realloc_handlerPFPvS_mmE, .Lfunc_end2-_Z23gim_set_realloc_handlerPFPvS_mmE
	.cfi_endproc

	.globl	_Z20gim_set_free_handlerPFvPvE
	.p2align	4, 0x90
	.type	_Z20gim_set_free_handlerPFvPvE,@function
_Z20gim_set_free_handlerPFvPvE:         # @_Z20gim_set_free_handlerPFvPvE
	.cfi_startproc
# BB#0:
	movq	%rdi, _ZL8g_freefn(%rip)
	retq
.Lfunc_end3:
	.size	_Z20gim_set_free_handlerPFvPvE, .Lfunc_end3-_Z20gim_set_free_handlerPFvPvE
	.cfi_endproc

	.globl	_Z21gim_get_alloc_handlerv
	.p2align	4, 0x90
	.type	_Z21gim_get_alloc_handlerv,@function
_Z21gim_get_alloc_handlerv:             # @_Z21gim_get_alloc_handlerv
	.cfi_startproc
# BB#0:
	movq	_ZL9g_allocfn(%rip), %rax
	retq
.Lfunc_end4:
	.size	_Z21gim_get_alloc_handlerv, .Lfunc_end4-_Z21gim_get_alloc_handlerv
	.cfi_endproc

	.globl	_Z22gim_get_alloca_handlerv
	.p2align	4, 0x90
	.type	_Z22gim_get_alloca_handlerv,@function
_Z22gim_get_alloca_handlerv:            # @_Z22gim_get_alloca_handlerv
	.cfi_startproc
# BB#0:
	movq	_ZL10g_allocafn(%rip), %rax
	retq
.Lfunc_end5:
	.size	_Z22gim_get_alloca_handlerv, .Lfunc_end5-_Z22gim_get_alloca_handlerv
	.cfi_endproc

	.globl	_Z23gim_get_realloc_handlerv
	.p2align	4, 0x90
	.type	_Z23gim_get_realloc_handlerv,@function
_Z23gim_get_realloc_handlerv:           # @_Z23gim_get_realloc_handlerv
	.cfi_startproc
# BB#0:
	movq	_ZL11g_reallocfn(%rip), %rax
	retq
.Lfunc_end6:
	.size	_Z23gim_get_realloc_handlerv, .Lfunc_end6-_Z23gim_get_realloc_handlerv
	.cfi_endproc

	.globl	_Z20gim_get_free_handlerv
	.p2align	4, 0x90
	.type	_Z20gim_get_free_handlerv,@function
_Z20gim_get_free_handlerv:              # @_Z20gim_get_free_handlerv
	.cfi_startproc
# BB#0:
	movq	_ZL8g_freefn(%rip), %rax
	retq
.Lfunc_end7:
	.size	_Z20gim_get_free_handlerv, .Lfunc_end7-_Z20gim_get_free_handlerv
	.cfi_endproc

	.globl	_Z9gim_allocm
	.p2align	4, 0x90
	.type	_Z9gim_allocm,@function
_Z9gim_allocm:                          # @_Z9gim_allocm
	.cfi_startproc
# BB#0:
	movq	_ZL9g_allocfn(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_2
# BB#1:
	jmpq	*%rax                   # TAILCALL
.LBB8_2:
	jmp	malloc                  # TAILCALL
.Lfunc_end8:
	.size	_Z9gim_allocm, .Lfunc_end8-_Z9gim_allocm
	.cfi_endproc

	.globl	_Z10gim_allocam
	.p2align	4, 0x90
	.type	_Z10gim_allocam,@function
_Z10gim_allocam:                        # @_Z10gim_allocam
	.cfi_startproc
# BB#0:
	movq	_ZL10g_allocafn(%rip), %rax
	testq	%rax, %rax
	jne	.LBB9_3
# BB#1:
	movq	_ZL9g_allocfn(%rip), %rax
	testq	%rax, %rax
	je	.LBB9_2
.LBB9_3:
	jmpq	*%rax                   # TAILCALL
.LBB9_2:
	jmp	malloc                  # TAILCALL
.Lfunc_end9:
	.size	_Z10gim_allocam, .Lfunc_end9-_Z10gim_allocam
	.cfi_endproc

	.globl	_Z11gim_reallocPvmm
	.p2align	4, 0x90
	.type	_Z11gim_reallocPvmm,@function
_Z11gim_reallocPvmm:                    # @_Z11gim_reallocPvmm
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	_ZL9g_allocfn(%rip), %rax
	movq	%rbx, %rdi
	testq	%rax, %rax
	je	.LBB10_2
# BB#1:
	callq	*%rax
	jmp	.LBB10_3
.LBB10_2:
	callq	malloc
.LBB10_3:                               # %_Z9gim_allocm.exit
	movq	%rax, %r14
	cmpq	%rbx, %r12
	cmovbq	%r12, %rbx
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	testq	%r15, %r15
	je	.LBB10_7
# BB#4:
	movq	_ZL8g_freefn(%rip), %rax
	movq	%r15, %rdi
	testq	%rax, %rax
	je	.LBB10_6
# BB#5:
	callq	*%rax
	jmp	.LBB10_7
.LBB10_6:
	callq	free
.LBB10_7:                               # %_Z8gim_freePv.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end10:
	.size	_Z11gim_reallocPvmm, .Lfunc_end10-_Z11gim_reallocPvmm
	.cfi_endproc

	.globl	_Z8gim_freePv
	.p2align	4, 0x90
	.type	_Z8gim_freePv,@function
_Z8gim_freePv:                          # @_Z8gim_freePv
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB11_3
# BB#1:
	movq	_ZL8g_freefn(%rip), %rax
	testq	%rax, %rax
	je	.LBB11_2
# BB#4:
	jmpq	*%rax                   # TAILCALL
.LBB11_3:
	retq
.LBB11_2:
	jmp	free                    # TAILCALL
.Lfunc_end11:
	.size	_Z8gim_freePv, .Lfunc_end11-_Z8gim_freePv
	.cfi_endproc

	.type	_ZL9g_allocfn,@object   # @_ZL9g_allocfn
	.local	_ZL9g_allocfn
	.comm	_ZL9g_allocfn,8,8
	.type	_ZL10g_allocafn,@object # @_ZL10g_allocafn
	.local	_ZL10g_allocafn
	.comm	_ZL10g_allocafn,8,8
	.type	_ZL11g_reallocfn,@object # @_ZL11g_reallocfn
	.local	_ZL11g_reallocfn
	.comm	_ZL11g_reallocfn,8,8
	.type	_ZL8g_freefn,@object    # @_ZL8g_freefn
	.local	_ZL8g_freefn
	.comm	_ZL8g_freefn,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
