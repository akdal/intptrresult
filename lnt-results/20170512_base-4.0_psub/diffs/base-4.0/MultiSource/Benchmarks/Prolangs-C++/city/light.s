	.text
	.file	"light.bc"
	.globl	_ZN5light4tickEv
	.p2align	4, 0x90
	.type	_ZN5light4tickEv,@function
_ZN5light4tickEv:                       # @_ZN5light4tickEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	28(%rbx), %eax
	leal	-1(%rax), %ecx
	movl	%ecx, 28(%rbx)
	cmpl	$1, %eax
	jg	.LBB0_2
# BB#1:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	%eax, 8(%rbx)
	cltq
	movl	12(%rbx,%rax,4), %eax
	movl	%eax, 28(%rbx)
.LBB0_2:
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN5light4tickEv, .Lfunc_end0-_ZN5light4tickEv
	.cfi_endproc

	.globl	_ZN5light4initEiiii
	.p2align	4, 0x90
	.type	_ZN5light4initEiiii,@function
_ZN5light4initEiiii:                    # @_ZN5light4initEiiii
	.cfi_startproc
# BB#0:
	movl	$0, 32(%rdi)
	movl	$0, 8(%rdi)
	movl	%esi, 12(%rdi)
	movl	%edx, 16(%rdi)
	movl	%ecx, 20(%rdi)
	movl	%r8d, 24(%rdi)
	movl	%esi, 28(%rdi)
	retq
.Lfunc_end1:
	.size	_ZN5light4initEiiii, .Lfunc_end1-_ZN5light4initEiiii
	.cfi_endproc

	.globl	_ZlsRSo5light
	.p2align	4, 0x90
	.type	_ZlsRSo5light,@function
_ZlsRSo5light:                          # @_ZlsRSo5light
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 48
.Lcfi7:
	.cfi_offset %rbx, -40
.Lcfi8:
	.cfi_offset %r12, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	8(%r12), %esi
	callq	_ZNSolsEi
	movq	%rax, %r15
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%r15, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	8(%r12), %eax
	orl	$1, %eax
	xorl	%esi, %esi
	cmpl	$1, %eax
	sete	%sil
	movq	%r15, %rdi
	callq	_ZNSolsEi
	xorl	%esi, %esi
	cmpl	$3, 8(%r12)
	sete	%sil
	movq	%rax, %rdi
	callq	_ZNSolsEi
	xorl	%esi, %esi
	cmpl	$2, 8(%r12)
	sete	%sil
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%rax, %rbx
	movl	$.L.str, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movl	8(%r12), %eax
	orl	$1, %eax
	xorl	%esi, %esi
	cmpl	$3, %eax
	sete	%sil
	movq	%rbx, %rdi
	callq	_ZNSolsEi
	xorl	%esi, %esi
	cmpl	$1, 8(%r12)
	sete	%sil
	movq	%rax, %rdi
	callq	_ZNSolsEi
	xorl	%esi, %esi
	cmpl	$0, 8(%r12)
	sete	%sil
	movq	%rax, %rdi
	callq	_ZNSolsEi
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	_ZlsRSo5light, .Lfunc_end2-_ZlsRSo5light
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_light.ii,@function
_GLOBAL__sub_I_light.ii:                # @_GLOBAL__sub_I_light.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end3:
	.size	_GLOBAL__sub_I_light.ii, .Lfunc_end3-_GLOBAL__sub_I_light.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" "
	.size	.L.str, 2

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_light.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
