	.text
	.file	"stepanov_v1p2.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4656510908468559872     # double 2000
.LCPI0_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI0_2:
	.quad	4502148214488346440     # double 9.9999999999999995E-8
	.text
	.globl	_Z9summarizev
	.p2align	4, 0x90
	.type	_Z9summarizev,@function
_Z9summarizev:                          # @_Z9summarizev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi1:
	.cfi_def_cfa_offset 64
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	$.Lstr, %edi
	callq	puts
	movl	$.Lstr.1, %edi
	callq	puts
	movl	current_test(%rip), %eax
	xorpd	%xmm0, %xmm0
	testl	%eax, %eax
	jle	.LBB0_1
# BB#7:                                 # %.lr.ph49.preheader
	cvtsi2sdl	iterations(%rip), %xmm3
	mulsd	.LCPI0_0(%rip), %xmm3
	divsd	.LCPI0_1(%rip), %xmm3
	xorl	%ebx, %ebx
	movsd	%xmm3, 24(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph49
                                        # =>This Inner Loop Header: Depth=1
	movsd	result_times(,%rbx,8), %xmm2 # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm0
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm4
	mulsd	%xmm4, %xmm0
	movapd	%xmm3, %xmm1
	divsd	%xmm2, %xmm1
	mulsd	%xmm4, %xmm1
	divsd	result_times(%rip), %xmm2
	mulsd	%xmm4, %xmm2
	movl	$.L.str.2, %edi
	movb	$3, %al
	movl	%ebx, %esi
	callq	printf
	movsd	24(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	incq	%rbx
	movslq	current_test(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_8
# BB#2:                                 # %.preheader
	testl	%eax, %eax
	xorpd	%xmm2, %xmm2
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm1, %xmm1
	xorpd	%xmm0, %xmm0
	jle	.LBB0_6
# BB#3:                                 # %.lr.ph.preheader
	xorpd	%xmm1, %xmm1
	xorl	%ebx, %ebx
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm2, %xmm2
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_4:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	result_times(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm2
	movsd	%xmm2, 40(%rsp)         # 8-byte Spill
	callq	log
	movsd	32(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 32(%rsp)         # 8-byte Spill
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	result_times(,%rbx,8), %xmm0
	callq	log
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movsd	result_times(,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	divsd	result_times(%rip), %xmm0
	callq	log
	movsd	40(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	movsd	16(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	incq	%rbx
	movslq	current_test(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_4
# BB#5:                                 # %._crit_edge.loopexit
	mulsd	.LCPI0_2(%rip), %xmm2
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	jmp	.LBB0_6
.LBB0_1:
	xorpd	%xmm2, %xmm2
	xorpd	%xmm1, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	xorpd	%xmm1, %xmm1
.LBB0_6:                                # %._crit_edge
	movsd	%xmm2, 40(%rsp)         # 8-byte Spill
	movsd	%xmm1, 16(%rsp)         # 8-byte Spill
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	divsd	%xmm1, %xmm0
	callq	exp
	mulsd	.LCPI0_2(%rip), %xmm0
	movsd	%xmm0, 24(%rsp)         # 8-byte Spill
	movl	current_test(%rip), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	callq	exp
	mulsd	.LCPI0_2(%rip), %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	movl	current_test(%rip), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	callq	exp
	movapd	%xmm0, %xmm2
	mulsd	.LCPI0_2(%rip), %xmm2
	movl	$.L.str.3, %edi
	movb	$3, %al
	movsd	24(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	8(%rsp), %xmm1          # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	printf
	movl	$.L.str.4, %edi
	movb	$1, %al
	movsd	40(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	printf
	movl	current_test(%rip), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	callq	exp
	mulsd	.LCPI0_2(%rip), %xmm0
	movl	$.L.str.5, %edi
	movb	$1, %al
	addq	$48, %rsp
	popq	%rbx
	jmp	printf                  # TAILCALL
.Lfunc_end0:
	.size	_Z9summarizev, .Lfunc_end0-_Z9summarizev
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4663319084467748864     # double 6000
.LCPI1_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI1_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.text
	.globl	_Z5test0PdS_
	.p2align	4, 0x90
	.type	_Z5test0PdS_,@function
_Z5test0PdS_:                           # @_Z5test0PdS_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 32
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	clock
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB1_10
# BB#1:                                 # %.preheader.lr.ph
	subq	%rbx, %r14
	testq	%r14, %r14
	jle	.LBB1_2
# BB#4:                                 # %.preheader.us.preheader
	sarq	$3, %r14
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	xorpd	%xmm0, %xmm0
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_6:                                #   Parent Loop BB1_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rbx,%rcx,8), %xmm0
	incq	%rcx
	cmpq	%r14, %rcx
	jl	.LBB1_6
# BB#7:                                 # %._crit_edge.us
                                        #   in Loop: Header=BB1_5 Depth=1
	ucomisd	.LCPI1_0(%rip), %xmm0
	jne	.LBB1_8
	jnp	.LBB1_9
.LBB1_8:                                #   in Loop: Header=BB1_5 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movl	iterations(%rip), %eax
.LBB1_9:                                # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB1_5 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB1_5
	jmp	.LBB1_10
.LBB1_2:                                # %_Z5checkd.exit.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_3:                                # %_Z5checkd.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	incl	%ebx
	cmpl	iterations(%rip), %ebx
	jl	.LBB1_3
.LBB1_10:                               # %._crit_edge19
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI1_1(%rip), %xmm0
	addsd	.LCPI1_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_Z5test0PdS_, .Lfunc_end1-_Z5test0PdS_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	4613937818241073152     # double 3
	.quad	4613937818241073152     # double 3
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	cmpl	$2, %edi
	jl	.LBB2_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, iterations(%rip)
.LBB2_2:
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	dpb(%rip), %rdi
	movq	dpe(%rip), %rsi
	cmpq	%rsi, %rdi
	je	.LBB2_16
# BB#3:                                 # %.lr.ph.i.preheader
	leaq	-8(%rsi), %rcx
	subq	%rdi, %rcx
	shrq	$3, %rcx
	incq	%rcx
	cmpq	$4, %rcx
	movq	%rdi, %rax
	jb	.LBB2_14
# BB#4:                                 # %min.iters.checked
	movq	%rcx, %r10
	andq	%r9, %r10
	movq	%rdi, %rax
	je	.LBB2_14
# BB#5:                                 # %vector.body.preheader
	leaq	-4(%r10), %r8
	movl	%r8d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB2_6
# BB#7:                                 # %vector.body.prol.preheader
	negq	%rdx
	xorl	%eax, %eax
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [3.000000e+00,3.000000e+00]
	.p2align	4, 0x90
.LBB2_8:                                # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rdi,%rax,8)
	movups	%xmm0, 16(%rdi,%rax,8)
	addq	$4, %rax
	incq	%rdx
	jne	.LBB2_8
	jmp	.LBB2_9
.LBB2_6:
	xorl	%eax, %eax
.LBB2_9:                                # %vector.body.prol.loopexit
	cmpq	$28, %r8
	jb	.LBB2_12
# BB#10:                                # %vector.body.preheader.new
	movq	%r10, %rdx
	subq	%rax, %rdx
	leaq	240(%rdi,%rax,8), %rax
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [3.000000e+00,3.000000e+00]
	.p2align	4, 0x90
.LBB2_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rax)
	movups	%xmm0, -224(%rax)
	movups	%xmm0, -208(%rax)
	movups	%xmm0, -192(%rax)
	movups	%xmm0, -176(%rax)
	movups	%xmm0, -160(%rax)
	movups	%xmm0, -144(%rax)
	movups	%xmm0, -128(%rax)
	movups	%xmm0, -112(%rax)
	movups	%xmm0, -96(%rax)
	movups	%xmm0, -80(%rax)
	movups	%xmm0, -64(%rax)
	movups	%xmm0, -48(%rax)
	movups	%xmm0, -32(%rax)
	movups	%xmm0, -16(%rax)
	movups	%xmm0, (%rax)
	addq	$256, %rax              # imm = 0x100
	addq	$-32, %rdx
	jne	.LBB2_11
.LBB2_12:                               # %middle.block
	cmpq	%r10, %rcx
	je	.LBB2_16
# BB#13:
	leaq	(%rdi,%r10,8), %rax
.LBB2_14:                               # %.lr.ph.i.preheader52
	movabsq	$4613937818241073152, %rcx # imm = 0x4008000000000000
	.p2align	4, 0x90
.LBB2_15:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
	cmpq	%rax, %rsi
	jne	.LBB2_15
.LBB2_16:                               # %_Z4fillIPddEvT_S1_T0_.exit
	movq	Dpb(%rip), %rcx
	movq	Dpe(%rip), %rdx
	cmpq	%rdx, %rcx
	je	.LBB2_31
# BB#17:                                # %.lr.ph.i26.preheader
	leaq	-8(%rdx), %rsi
	subq	%rcx, %rsi
	shrq	$3, %rsi
	incq	%rsi
	cmpq	$4, %rsi
	jb	.LBB2_28
# BB#18:                                # %min.iters.checked39
	andq	%rsi, %r9
	je	.LBB2_28
# BB#19:                                # %vector.body31.preheader
	leaq	-4(%r9), %r8
	movl	%r8d, %eax
	shrl	$2, %eax
	incl	%eax
	andq	$7, %rax
	je	.LBB2_20
# BB#21:                                # %vector.body31.prol.preheader
	negq	%rax
	xorl	%edi, %edi
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [3.000000e+00,3.000000e+00]
	.p2align	4, 0x90
.LBB2_22:                               # %vector.body31.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, (%rcx,%rdi,8)
	movups	%xmm0, 16(%rcx,%rdi,8)
	addq	$4, %rdi
	incq	%rax
	jne	.LBB2_22
	jmp	.LBB2_23
.LBB2_20:
	xorl	%edi, %edi
.LBB2_23:                               # %vector.body31.prol.loopexit
	cmpq	$28, %r8
	jb	.LBB2_26
# BB#24:                                # %vector.body31.preheader.new
	movq	%r9, %rax
	subq	%rdi, %rax
	leaq	240(%rcx,%rdi,8), %rdi
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [3.000000e+00,3.000000e+00]
	.p2align	4, 0x90
.LBB2_25:                               # %vector.body31
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, -240(%rdi)
	movups	%xmm0, -224(%rdi)
	movups	%xmm0, -208(%rdi)
	movups	%xmm0, -192(%rdi)
	movups	%xmm0, -176(%rdi)
	movups	%xmm0, -160(%rdi)
	movups	%xmm0, -144(%rdi)
	movups	%xmm0, -128(%rdi)
	movups	%xmm0, -112(%rdi)
	movups	%xmm0, -96(%rdi)
	movups	%xmm0, -80(%rdi)
	movups	%xmm0, -64(%rdi)
	movups	%xmm0, -48(%rdi)
	movups	%xmm0, -32(%rdi)
	movups	%xmm0, -16(%rdi)
	movups	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rax
	jne	.LBB2_25
.LBB2_26:                               # %middle.block32
	cmpq	%r9, %rsi
	je	.LBB2_30
# BB#27:
	leaq	(%rcx,%r9,8), %rcx
.LBB2_28:                               # %.lr.ph.i26.preheader51
	movabsq	$4613937818241073152, %rax # imm = 0x4008000000000000
	.p2align	4, 0x90
.LBB2_29:                               # %.lr.ph.i26
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB2_29
.LBB2_30:                               # %_Z4fillIP6DoubleS0_EvT_S2_T0_.exit.loopexit
	movq	dpb(%rip), %rdi
	movq	dpe(%rip), %rsi
.LBB2_31:                               # %_Z4fillIP6DoubleS0_EvT_S2_T0_.exit
	callq	_Z5test0PdS_
	movq	dpb(%rip), %rdi
	movq	dpe(%rip), %rsi
	movsd	d(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testIPddEvT_S1_T0_
	movq	Dpb(%rip), %rdi
	movq	Dpe(%rip), %rsi
	movsd	D(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testIP6DoubleS0_EvT_S2_T0_
	movq	dPb(%rip), %rdi
	movq	dPe(%rip), %rsi
	movsd	d(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI14double_pointerdEvT_S1_T0_
	movq	DPb(%rip), %rdi
	movq	DPe(%rip), %rsi
	movsd	D(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI14Double_pointer6DoubleEvT_S2_T0_
	movq	rdpb(%rip), %rdi
	movq	rdpe(%rip), %rsi
	movsd	d(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_
	movq	rDpb(%rip), %rdi
	movq	rDpe(%rip), %rsi
	movsd	D(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_
	movq	rdPb(%rip), %rdi
	movq	rdPe(%rip), %rsi
	movsd	d(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_
	movq	rDPb(%rip), %rdi
	movq	rDPe(%rip), %rsi
	movsd	D(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_
	movq	rrdpb(%rip), %rdi
	movq	rrdpe(%rip), %rsi
	movsd	d(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_
	movq	rrDpb(%rip), %rdi
	movq	rrDpe(%rip), %rsi
	movsd	D(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_
	movq	rrdPb(%rip), %rdi
	movq	rrdPe(%rip), %rsi
	movsd	d(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_
	movq	rrDPb(%rip), %rdi
	movq	rrDPe(%rip), %rsi
	movsd	D(%rip), %xmm0          # xmm0 = mem[0],zero
	callq	_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_
	callq	_Z9summarizev
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	main, .Lfunc_end2-main
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4663319084467748864     # double 6000
.LCPI3_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI3_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testIPddEvT_S1_T0_,"axG",@progbits,_Z4testIPddEvT_S1_T0_,comdat
	.weak	_Z4testIPddEvT_S1_T0_
	.p2align	4, 0x90
	.type	_Z4testIPddEvT_S1_T0_,@function
_Z4testIPddEvT_S1_T0_:                  # @_Z4testIPddEvT_S1_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB3_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB3_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_5 Depth 2
                                        #     Child Loop BB3_7 Depth 2
	testq	%r12, %r12
	movq	%r14, %rcx
	movapd	%xmm1, %xmm0
	je	.LBB3_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r13, %rdx
	movq	%r14, %rcx
	movapd	%xmm1, %xmm0
	.p2align	4, 0x90
.LBB3_5:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB3_5
.LBB3_6:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB3_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB3_8
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph.i
                                        #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB3_7
.LBB3_8:                                # %_Z10accumulateIPddET0_T_S2_S1_.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	ucomisd	.LCPI3_0(%rip), %xmm0
	jne	.LBB3_9
	jnp	.LBB3_10
.LBB3_9:                                #   in Loop: Header=BB3_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB3_10:                               # %_Z5checkd.exit
                                        #   in Loop: Header=BB3_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB3_3
	jmp	.LBB3_15
.LBB3_11:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI3_0(%rip), %xmm1
	jne	.LBB3_13
	jnp	.LBB3_14
.LBB3_13:                               #   in Loop: Header=BB3_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB3_14:                               # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB3_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB3_12
.LBB3_15:                               # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI3_1(%rip), %xmm0
	addsd	.LCPI3_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_Z4testIPddEvT_S1_T0_, .Lfunc_end3-_Z4testIPddEvT_S1_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_0:
	.quad	4663319084467748864     # double 6000
.LCPI4_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI4_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testIP6DoubleS0_EvT_S2_T0_,"axG",@progbits,_Z4testIP6DoubleS0_EvT_S2_T0_,comdat
	.weak	_Z4testIP6DoubleS0_EvT_S2_T0_
	.p2align	4, 0x90
	.type	_Z4testIP6DoubleS0_EvT_S2_T0_,@function
_Z4testIP6DoubleS0_EvT_S2_T0_:          # @_Z4testIP6DoubleS0_EvT_S2_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB4_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB4_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_5 Depth 2
                                        #     Child Loop BB4_7 Depth 2
	testq	%r12, %r12
	movq	%r14, %rcx
	movapd	%xmm1, %xmm0
	je	.LBB4_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	movq	%r13, %rdx
	movq	%r14, %rcx
	movapd	%xmm1, %xmm0
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB4_5
.LBB4_6:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB4_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB4_8
	.p2align	4, 0x90
.LBB4_7:                                # %.lr.ph.i
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB4_7
.LBB4_8:                                # %_Z10accumulateIP6DoubleS0_ET0_T_S3_S2_.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	ucomisd	.LCPI4_0(%rip), %xmm0
	jne	.LBB4_9
	jnp	.LBB4_10
.LBB4_9:                                #   in Loop: Header=BB4_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB4_10:                               # %_Z5checkd.exit
                                        #   in Loop: Header=BB4_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB4_3
	jmp	.LBB4_15
.LBB4_11:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI4_0(%rip), %xmm1
	jne	.LBB4_13
	jnp	.LBB4_14
.LBB4_13:                               #   in Loop: Header=BB4_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB4_14:                               # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB4_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB4_12
.LBB4_15:                               # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI4_1(%rip), %xmm0
	addsd	.LCPI4_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_Z4testIP6DoubleS0_EvT_S2_T0_, .Lfunc_end4-_Z4testIP6DoubleS0_EvT_S2_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4663319084467748864     # double 6000
.LCPI5_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI5_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI14double_pointerdEvT_S1_T0_,"axG",@progbits,_Z4testI14double_pointerdEvT_S1_T0_,comdat
	.weak	_Z4testI14double_pointerdEvT_S1_T0_
	.p2align	4, 0x90
	.type	_Z4testI14double_pointerdEvT_S1_T0_,@function
_Z4testI14double_pointerdEvT_S1_T0_:    # @_Z4testI14double_pointerdEvT_S1_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi42:
	.cfi_def_cfa_offset 64
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB5_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB5_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_5 Depth 2
                                        #     Child Loop BB5_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB5_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB5_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB5_5
.LBB5_6:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB5_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                # %.lr.ph.i
                                        #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB5_7
.LBB5_8:                                # %_Z10accumulateI14double_pointerdET0_T_S2_S1_.exit
                                        #   in Loop: Header=BB5_3 Depth=1
	ucomisd	.LCPI5_0(%rip), %xmm0
	jne	.LBB5_9
	jnp	.LBB5_10
.LBB5_9:                                #   in Loop: Header=BB5_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB5_10:                               # %_Z5checkd.exit
                                        #   in Loop: Header=BB5_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB5_3
	jmp	.LBB5_15
.LBB5_11:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI5_0(%rip), %xmm1
	jne	.LBB5_13
	jnp	.LBB5_14
.LBB5_13:                               #   in Loop: Header=BB5_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB5_14:                               # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB5_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB5_12
.LBB5_15:                               # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI5_1(%rip), %xmm0
	addsd	.LCPI5_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_Z4testI14double_pointerdEvT_S1_T0_, .Lfunc_end5-_Z4testI14double_pointerdEvT_S1_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4663319084467748864     # double 6000
.LCPI6_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI6_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI14Double_pointer6DoubleEvT_S2_T0_,"axG",@progbits,_Z4testI14Double_pointer6DoubleEvT_S2_T0_,comdat
	.weak	_Z4testI14Double_pointer6DoubleEvT_S2_T0_
	.p2align	4, 0x90
	.type	_Z4testI14Double_pointer6DoubleEvT_S2_T0_,@function
_Z4testI14Double_pointer6DoubleEvT_S2_T0_: # @_Z4testI14Double_pointer6DoubleEvT_S2_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 64
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB6_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB6_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_5 Depth 2
                                        #     Child Loop BB6_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB6_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB6_5
.LBB6_6:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB6_8
	.p2align	4, 0x90
.LBB6_7:                                # %.lr.ph.i
                                        #   Parent Loop BB6_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB6_7
.LBB6_8:                                # %_Z10accumulateI14Double_pointer6DoubleET0_T_S3_S2_.exit
                                        #   in Loop: Header=BB6_3 Depth=1
	ucomisd	.LCPI6_0(%rip), %xmm0
	jne	.LBB6_9
	jnp	.LBB6_10
.LBB6_9:                                #   in Loop: Header=BB6_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB6_10:                               # %_Z5checkd.exit
                                        #   in Loop: Header=BB6_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB6_3
	jmp	.LBB6_15
.LBB6_11:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI6_0(%rip), %xmm1
	jne	.LBB6_13
	jnp	.LBB6_14
.LBB6_13:                               #   in Loop: Header=BB6_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB6_14:                               # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB6_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB6_12
.LBB6_15:                               # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI6_1(%rip), %xmm0
	addsd	.LCPI6_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_Z4testI14Double_pointer6DoubleEvT_S2_T0_, .Lfunc_end6-_Z4testI14Double_pointer6DoubleEvT_S2_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4663319084467748864     # double 6000
.LCPI7_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI7_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorIPddEdEvT_S3_T0_,"axG",@progbits,_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_,comdat
	.weak	_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_,@function
_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_: # @_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi62:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi63:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi65:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 64
.Lcfi69:
	.cfi_offset %rbx, -56
.Lcfi70:
	.cfi_offset %r12, -48
.Lcfi71:
	.cfi_offset %r13, -40
.Lcfi72:
	.cfi_offset %r14, -32
.Lcfi73:
	.cfi_offset %r15, -24
.Lcfi74:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB7_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB7_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%r14), %r15
	subq	%rbx, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
                                        #     Child Loop BB7_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB7_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB7_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB7_5:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addq	$-8, %rcx
	incq	%rdx
	jne	.LBB7_5
.LBB7_6:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB7_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB7_8
	.p2align	4, 0x90
.LBB7_7:                                # %.lr.ph.i
                                        #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-56(%rcx), %xmm0
	addsd	-64(%rcx), %xmm0
	addq	$-64, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB7_7
.LBB7_8:                                # %_Z10accumulateI16reverse_iteratorIPddEdET0_T_S4_S3_.exit
                                        #   in Loop: Header=BB7_3 Depth=1
	ucomisd	.LCPI7_0(%rip), %xmm0
	jne	.LBB7_9
	jnp	.LBB7_10
.LBB7_9:                                #   in Loop: Header=BB7_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB7_10:                               # %_Z5checkd.exit
                                        #   in Loop: Header=BB7_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB7_3
	jmp	.LBB7_15
.LBB7_11:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI7_0(%rip), %xmm1
	jne	.LBB7_13
	jnp	.LBB7_14
.LBB7_13:                               #   in Loop: Header=BB7_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB7_14:                               # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB7_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB7_12
.LBB7_15:                               # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI7_1(%rip), %xmm0
	addsd	.LCPI7_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_, .Lfunc_end7-_Z4testI16reverse_iteratorIPddEdEvT_S3_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_0:
	.quad	4663319084467748864     # double 6000
.LCPI8_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI8_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_,"axG",@progbits,_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_,comdat
	.weak	_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_,@function
_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_: # @_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi75:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi76:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi78:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi79:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi81:
	.cfi_def_cfa_offset 64
.Lcfi82:
	.cfi_offset %rbx, -56
.Lcfi83:
	.cfi_offset %r12, -48
.Lcfi84:
	.cfi_offset %r13, -40
.Lcfi85:
	.cfi_offset %r14, -32
.Lcfi86:
	.cfi_offset %r15, -24
.Lcfi87:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB8_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB8_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%r14), %r15
	subq	%rbx, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
                                        #     Child Loop BB8_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB8_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addq	$-8, %rcx
	incq	%rdx
	jne	.LBB8_5
.LBB8_6:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB8_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                # %.lr.ph.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-56(%rcx), %xmm0
	addsd	-64(%rcx), %xmm0
	addq	$-64, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB8_7
.LBB8_8:                                # %_Z10accumulateI16reverse_iteratorIP6DoubleS1_ES1_ET0_T_S5_S4_.exit
                                        #   in Loop: Header=BB8_3 Depth=1
	ucomisd	.LCPI8_0(%rip), %xmm0
	jne	.LBB8_9
	jnp	.LBB8_10
.LBB8_9:                                #   in Loop: Header=BB8_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB8_10:                               # %_Z5checkd.exit
                                        #   in Loop: Header=BB8_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB8_3
	jmp	.LBB8_15
.LBB8_11:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI8_0(%rip), %xmm1
	jne	.LBB8_13
	jnp	.LBB8_14
.LBB8_13:                               #   in Loop: Header=BB8_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB8_14:                               # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB8_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB8_12
.LBB8_15:                               # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI8_1(%rip), %xmm0
	addsd	.LCPI8_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_, .Lfunc_end8-_Z4testI16reverse_iteratorIP6DoubleS1_ES1_EvT_S4_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI9_0:
	.quad	4663319084467748864     # double 6000
.LCPI9_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI9_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_,"axG",@progbits,_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_,comdat
	.weak	_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_,@function
_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_: # @_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 64
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB9_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB9_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%r14), %r15
	subq	%rbx, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB9_3:                                # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_5 Depth 2
                                        #     Child Loop BB9_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB9_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB9_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB9_5:                                # %.lr.ph.i.prol
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addq	$-8, %rcx
	incq	%rdx
	jne	.LBB9_5
.LBB9_6:                                # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB9_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB9_8
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph.i
                                        #   Parent Loop BB9_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-56(%rcx), %xmm0
	addsd	-64(%rcx), %xmm0
	addq	$-64, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB9_7
.LBB9_8:                                # %_Z10accumulateI16reverse_iteratorI14double_pointerdEdET0_T_S4_S3_.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	ucomisd	.LCPI9_0(%rip), %xmm0
	jne	.LBB9_9
	jnp	.LBB9_10
.LBB9_9:                                #   in Loop: Header=BB9_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB9_10:                               # %_Z5checkd.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB9_3
	jmp	.LBB9_15
.LBB9_11:                               # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_12:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI9_0(%rip), %xmm1
	jne	.LBB9_13
	jnp	.LBB9_14
.LBB9_13:                               #   in Loop: Header=BB9_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB9_14:                               # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB9_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB9_12
.LBB9_15:                               # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI9_1(%rip), %xmm0
	addsd	.LCPI9_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_, .Lfunc_end9-_Z4testI16reverse_iteratorI14double_pointerdEdEvT_S3_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4663319084467748864     # double 6000
.LCPI10_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI10_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_,"axG",@progbits,_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_,comdat
	.weak	_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_,@function
_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_: # @_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi104:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi105:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi107:
	.cfi_def_cfa_offset 64
.Lcfi108:
	.cfi_offset %rbx, -56
.Lcfi109:
	.cfi_offset %r12, -48
.Lcfi110:
	.cfi_offset %r13, -40
.Lcfi111:
	.cfi_offset %r14, -32
.Lcfi112:
	.cfi_offset %r15, -24
.Lcfi113:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB10_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB10_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%r14), %r15
	subq	%rbx, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_5 Depth 2
                                        #     Child Loop BB10_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB10_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB10_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addq	$-8, %rcx
	incq	%rdx
	jne	.LBB10_5
.LBB10_6:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB10_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB10_8
	.p2align	4, 0x90
.LBB10_7:                               # %.lr.ph.i
                                        #   Parent Loop BB10_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-8(%rcx), %xmm0
	addsd	-16(%rcx), %xmm0
	addsd	-24(%rcx), %xmm0
	addsd	-32(%rcx), %xmm0
	addsd	-40(%rcx), %xmm0
	addsd	-48(%rcx), %xmm0
	addsd	-56(%rcx), %xmm0
	addsd	-64(%rcx), %xmm0
	addq	$-64, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB10_7
.LBB10_8:                               # %_Z10accumulateI16reverse_iteratorI14Double_pointer6DoubleES2_ET0_T_S5_S4_.exit
                                        #   in Loop: Header=BB10_3 Depth=1
	ucomisd	.LCPI10_0(%rip), %xmm0
	jne	.LBB10_9
	jnp	.LBB10_10
.LBB10_9:                               #   in Loop: Header=BB10_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB10_10:                              # %_Z5checkd.exit
                                        #   in Loop: Header=BB10_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB10_3
	jmp	.LBB10_15
.LBB10_11:                              # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI10_0(%rip), %xmm1
	jne	.LBB10_13
	jnp	.LBB10_14
.LBB10_13:                              #   in Loop: Header=BB10_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB10_14:                              # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB10_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB10_12
.LBB10_15:                              # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI10_1(%rip), %xmm0
	addsd	.LCPI10_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_, .Lfunc_end10-_Z4testI16reverse_iteratorI14Double_pointer6DoubleES2_EvT_S4_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4663319084467748864     # double 6000
.LCPI11_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI11_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_,"axG",@progbits,_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_,comdat
	.weak	_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_,@function
_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_: # @_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi114:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi117:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi118:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi120:
	.cfi_def_cfa_offset 64
.Lcfi121:
	.cfi_offset %rbx, -56
.Lcfi122:
	.cfi_offset %r12, -48
.Lcfi123:
	.cfi_offset %r13, -40
.Lcfi124:
	.cfi_offset %r14, -32
.Lcfi125:
	.cfi_offset %r15, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB11_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB11_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_5 Depth 2
                                        #     Child Loop BB11_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB11_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB11_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB11_5:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB11_5
.LBB11_6:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB11_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB11_8
	.p2align	4, 0x90
.LBB11_7:                               # %.lr.ph.i
                                        #   Parent Loop BB11_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB11_7
.LBB11_8:                               # %_Z10accumulateI16reverse_iteratorIS0_IPddEdEdET0_T_S5_S4_.exit
                                        #   in Loop: Header=BB11_3 Depth=1
	ucomisd	.LCPI11_0(%rip), %xmm0
	jne	.LBB11_9
	jnp	.LBB11_10
.LBB11_9:                               #   in Loop: Header=BB11_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB11_10:                              # %_Z5checkd.exit
                                        #   in Loop: Header=BB11_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB11_3
	jmp	.LBB11_15
.LBB11_11:                              # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI11_0(%rip), %xmm1
	jne	.LBB11_13
	jnp	.LBB11_14
.LBB11_13:                              #   in Loop: Header=BB11_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB11_14:                              # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB11_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB11_12
.LBB11_15:                              # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI11_1(%rip), %xmm0
	addsd	.LCPI11_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_, .Lfunc_end11-_Z4testI16reverse_iteratorIS0_IPddEdEdEvT_S4_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI12_0:
	.quad	4663319084467748864     # double 6000
.LCPI12_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI12_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_,"axG",@progbits,_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_,comdat
	.weak	_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_,@function
_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_: # @_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi130:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi131:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi133:
	.cfi_def_cfa_offset 64
.Lcfi134:
	.cfi_offset %rbx, -56
.Lcfi135:
	.cfi_offset %r12, -48
.Lcfi136:
	.cfi_offset %r13, -40
.Lcfi137:
	.cfi_offset %r14, -32
.Lcfi138:
	.cfi_offset %r15, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB12_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB12_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_3:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_5 Depth 2
                                        #     Child Loop BB12_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB12_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB12_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB12_5:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB12_5
.LBB12_6:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB12_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB12_8
	.p2align	4, 0x90
.LBB12_7:                               # %.lr.ph.i
                                        #   Parent Loop BB12_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB12_7
.LBB12_8:                               # %_Z10accumulateI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_ET0_T_S6_S5_.exit
                                        #   in Loop: Header=BB12_3 Depth=1
	ucomisd	.LCPI12_0(%rip), %xmm0
	jne	.LBB12_9
	jnp	.LBB12_10
.LBB12_9:                               #   in Loop: Header=BB12_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB12_10:                              # %_Z5checkd.exit
                                        #   in Loop: Header=BB12_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB12_3
	jmp	.LBB12_15
.LBB12_11:                              # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI12_0(%rip), %xmm1
	jne	.LBB12_13
	jnp	.LBB12_14
.LBB12_13:                              #   in Loop: Header=BB12_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB12_14:                              # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB12_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB12_12
.LBB12_15:                              # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI12_1(%rip), %xmm0
	addsd	.LCPI12_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_, .Lfunc_end12-_Z4testI16reverse_iteratorIS0_IP6DoubleS1_ES1_ES1_EvT_S5_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4663319084467748864     # double 6000
.LCPI13_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI13_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_,"axG",@progbits,_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_,comdat
	.weak	_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_,@function
_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_: # @_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi146:
	.cfi_def_cfa_offset 64
.Lcfi147:
	.cfi_offset %rbx, -56
.Lcfi148:
	.cfi_offset %r12, -48
.Lcfi149:
	.cfi_offset %r13, -40
.Lcfi150:
	.cfi_offset %r14, -32
.Lcfi151:
	.cfi_offset %r15, -24
.Lcfi152:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB13_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB13_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_5 Depth 2
                                        #     Child Loop BB13_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB13_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB13_5:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB13_5
.LBB13_6:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB13_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB13_8
	.p2align	4, 0x90
.LBB13_7:                               # %.lr.ph.i
                                        #   Parent Loop BB13_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB13_7
.LBB13_8:                               # %_Z10accumulateI16reverse_iteratorIS0_I14double_pointerdEdEdET0_T_S5_S4_.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	ucomisd	.LCPI13_0(%rip), %xmm0
	jne	.LBB13_9
	jnp	.LBB13_10
.LBB13_9:                               #   in Loop: Header=BB13_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB13_10:                              # %_Z5checkd.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB13_3
	jmp	.LBB13_15
.LBB13_11:                              # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB13_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI13_0(%rip), %xmm1
	jne	.LBB13_13
	jnp	.LBB13_14
.LBB13_13:                              #   in Loop: Header=BB13_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB13_14:                              # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB13_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB13_12
.LBB13_15:                              # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI13_1(%rip), %xmm0
	addsd	.LCPI13_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_, .Lfunc_end13-_Z4testI16reverse_iteratorIS0_I14double_pointerdEdEdEvT_S4_T0_
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI14_0:
	.quad	4663319084467748864     # double 6000
.LCPI14_1:
	.quad	4696837146684686336     # double 1.0E+6
.LCPI14_2:
	.quad	4503599627370496000     # double 1.1920928955078125E-7
	.section	.text._Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_,"axG",@progbits,_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_,comdat
	.weak	_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_
	.p2align	4, 0x90
	.type	_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_,@function
_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_: # @_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi153:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi154:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi155:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi156:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi157:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi158:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi159:
	.cfi_def_cfa_offset 64
.Lcfi160:
	.cfi_offset %rbx, -56
.Lcfi161:
	.cfi_offset %r12, -48
.Lcfi162:
	.cfi_offset %r13, -40
.Lcfi163:
	.cfi_offset %r14, -32
.Lcfi164:
	.cfi_offset %r15, -24
.Lcfi165:
	.cfi_offset %rbp, -16
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	clock
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movq	%rax, start_time(%rip)
	movl	iterations(%rip), %eax
	testl	%eax, %eax
	jle	.LBB14_15
# BB#1:                                 # %.lr.ph
	cmpq	%rbx, %r14
	je	.LBB14_11
# BB#2:                                 # %.lr.ph.split.preheader
	leaq	-8(%rbx), %r15
	subq	%r14, %r15
	movl	%r15d, %r12d
	shrl	$3, %r12d
	incl	%r12d
	andl	$7, %r12d
	movq	%r12, %r13
	negq	%r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_3:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_5 Depth 2
                                        #     Child Loop BB14_7 Depth 2
	testq	%r12, %r12
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	je	.LBB14_6
# BB#4:                                 # %.lr.ph.i.prol.preheader
                                        #   in Loop: Header=BB14_3 Depth=1
	movq	%r13, %rdx
	movapd	%xmm1, %xmm0
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB14_5:                               # %.lr.ph.i.prol
                                        #   Parent Loop BB14_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	incq	%rdx
	jne	.LBB14_5
.LBB14_6:                               # %.lr.ph.i.prol.loopexit
                                        #   in Loop: Header=BB14_3 Depth=1
	cmpq	$56, %r15
	jb	.LBB14_8
	.p2align	4, 0x90
.LBB14_7:                               # %.lr.ph.i
                                        #   Parent Loop BB14_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	(%rcx), %xmm0
	addsd	8(%rcx), %xmm0
	addsd	16(%rcx), %xmm0
	addsd	24(%rcx), %xmm0
	addsd	32(%rcx), %xmm0
	addsd	40(%rcx), %xmm0
	addsd	48(%rcx), %xmm0
	addsd	56(%rcx), %xmm0
	addq	$64, %rcx
	cmpq	%rbx, %rcx
	jne	.LBB14_7
.LBB14_8:                               # %_Z10accumulateI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_ET0_T_S6_S5_.exit
                                        #   in Loop: Header=BB14_3 Depth=1
	ucomisd	.LCPI14_0(%rip), %xmm0
	jne	.LBB14_9
	jnp	.LBB14_10
.LBB14_9:                               #   in Loop: Header=BB14_3 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB14_10:                              # %_Z5checkd.exit
                                        #   in Loop: Header=BB14_3 Depth=1
	incl	%ebp
	cmpl	%eax, %ebp
	jl	.LBB14_3
	jmp	.LBB14_15
.LBB14_11:                              # %.lr.ph.split.us.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_12:                              # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	ucomisd	.LCPI14_0(%rip), %xmm1
	jne	.LBB14_13
	jnp	.LBB14_14
.LBB14_13:                              #   in Loop: Header=BB14_12 Depth=1
	movl	current_test(%rip), %esi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	printf
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movl	iterations(%rip), %eax
.LBB14_14:                              # %_Z5checkd.exit.us
                                        #   in Loop: Header=BB14_12 Depth=1
	incl	%ebx
	cmpl	%eax, %ebx
	jl	.LBB14_12
.LBB14_15:                              # %._crit_edge
	callq	clock
	movq	%rax, end_time(%rip)
	subq	start_time(%rip), %rax
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI14_1(%rip), %xmm0
	addsd	.LCPI14_2(%rip), %xmm0
	movslq	current_test(%rip), %rax
	leal	1(%rax), %ecx
	movl	%ecx, current_test(%rip)
	movsd	%xmm0, result_times(,%rax,8)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_, .Lfunc_end14-_Z4testI16reverse_iteratorIS0_I14Double_pointer6DoubleES2_ES2_EvT_S5_T0_
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_stepanov_v1p2.ii,@function
_GLOBAL__sub_I_stepanov_v1p2.ii:        # @_GLOBAL__sub_I_stepanov_v1p2.ii
	.cfi_startproc
# BB#0:
	movq	$0, D(%rip)
	movq	dpb(%rip), %rax
	movq	%rax, dPb(%rip)
	movq	dpe(%rip), %rcx
	movq	%rcx, dPe(%rip)
	movq	Dpb(%rip), %rdx
	movq	%rdx, DPb(%rip)
	movq	Dpe(%rip), %rsi
	movq	%rsi, DPe(%rip)
	movq	%rcx, rdpb(%rip)
	movq	%rax, rdpe(%rip)
	movq	%rsi, rDpb(%rip)
	movq	%rdx, rDpe(%rip)
	movq	%rcx, rdPb(%rip)
	movq	%rax, rdPe(%rip)
	movq	%rsi, rDPb(%rip)
	movq	%rdx, rDPe(%rip)
	movq	%rax, rrdpb(%rip)
	movq	%rcx, rrdpe(%rip)
	movq	%rdx, rrDpb(%rip)
	movq	%rsi, rrDpe(%rip)
	movq	%rax, rrdPb(%rip)
	movq	%rcx, rrdPe(%rip)
	movq	%rdx, rrDPb(%rip)
	movq	%rsi, rrDPe(%rip)
	retq
.Lfunc_end15:
	.size	_GLOBAL__sub_I_stepanov_v1p2.ii, .Lfunc_end15-_GLOBAL__sub_I_stepanov_v1p2.ii
	.cfi_endproc

	.type	iterations,@object      # @iterations
	.data
	.globl	iterations
	.p2align	2
iterations:
	.long	250000                  # 0x3d090
	.size	iterations, 4

	.type	current_test,@object    # @current_test
	.bss
	.globl	current_test
	.p2align	2
current_test:
	.long	0                       # 0x0
	.size	current_test, 4

	.type	result_times,@object    # @result_times
	.globl	result_times
	.p2align	4
result_times:
	.zero	160
	.size	result_times, 160

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"%2i       %5.2fsec    %5.2fM         %.2f\n"
	.size	.L.str.2, 43

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"mean:    %5.2fsec    %5.2fM         %.2f\n"
	.size	.L.str.3, 42

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\nTotal absolute time: %.2f sec\n"
	.size	.L.str.4, 32

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nAbstraction Penalty: %.2f\n\n"
	.size	.L.str.5, 29

	.type	start_time,@object      # @start_time
	.bss
	.globl	start_time
	.p2align	3
start_time:
	.quad	0                       # 0x0
	.size	start_time, 8

	.type	end_time,@object        # @end_time
	.globl	end_time
	.p2align	3
end_time:
	.quad	0                       # 0x0
	.size	end_time, 8

	.type	data,@object            # @data
	.globl	data
	.p2align	4
data:
	.zero	16000
	.size	data, 16000

	.type	Data,@object            # @Data
	.globl	Data
	.p2align	4
Data:
	.zero	16000
	.size	Data, 16000

	.type	d,@object               # @d
	.globl	d
	.p2align	3
d:
	.quad	0                       # double 0
	.size	d, 8

	.type	D,@object               # @D
	.globl	D
	.p2align	3
D:
	.zero	8
	.size	D, 8

	.type	dpb,@object             # @dpb
	.data
	.globl	dpb
	.p2align	3
dpb:
	.quad	data
	.size	dpb, 8

	.type	dpe,@object             # @dpe
	.globl	dpe
	.p2align	3
dpe:
	.quad	data+16000
	.size	dpe, 8

	.type	Dpb,@object             # @Dpb
	.globl	Dpb
	.p2align	3
Dpb:
	.quad	Data
	.size	Dpb, 8

	.type	Dpe,@object             # @Dpe
	.globl	Dpe
	.p2align	3
Dpe:
	.quad	Data+16000
	.size	Dpe, 8

	.type	dPb,@object             # @dPb
	.bss
	.globl	dPb
	.p2align	3
dPb:
	.zero	8
	.size	dPb, 8

	.type	dPe,@object             # @dPe
	.globl	dPe
	.p2align	3
dPe:
	.zero	8
	.size	dPe, 8

	.type	DPb,@object             # @DPb
	.globl	DPb
	.p2align	3
DPb:
	.zero	8
	.size	DPb, 8

	.type	DPe,@object             # @DPe
	.globl	DPe
	.p2align	3
DPe:
	.zero	8
	.size	DPe, 8

	.type	rdpb,@object            # @rdpb
	.globl	rdpb
	.p2align	3
rdpb:
	.zero	8
	.size	rdpb, 8

	.type	rdpe,@object            # @rdpe
	.globl	rdpe
	.p2align	3
rdpe:
	.zero	8
	.size	rdpe, 8

	.type	rDpb,@object            # @rDpb
	.globl	rDpb
	.p2align	3
rDpb:
	.zero	8
	.size	rDpb, 8

	.type	rDpe,@object            # @rDpe
	.globl	rDpe
	.p2align	3
rDpe:
	.zero	8
	.size	rDpe, 8

	.type	rdPb,@object            # @rdPb
	.globl	rdPb
	.p2align	3
rdPb:
	.zero	8
	.size	rdPb, 8

	.type	rdPe,@object            # @rdPe
	.globl	rdPe
	.p2align	3
rdPe:
	.zero	8
	.size	rdPe, 8

	.type	rDPb,@object            # @rDPb
	.globl	rDPb
	.p2align	3
rDPb:
	.zero	8
	.size	rDPb, 8

	.type	rDPe,@object            # @rDPe
	.globl	rDPe
	.p2align	3
rDPe:
	.zero	8
	.size	rDPe, 8

	.type	rrdpb,@object           # @rrdpb
	.globl	rrdpb
	.p2align	3
rrdpb:
	.zero	8
	.size	rrdpb, 8

	.type	rrdpe,@object           # @rrdpe
	.globl	rrdpe
	.p2align	3
rrdpe:
	.zero	8
	.size	rrdpe, 8

	.type	rrDpb,@object           # @rrDpb
	.globl	rrDpb
	.p2align	3
rrDpb:
	.zero	8
	.size	rrDpb, 8

	.type	rrDpe,@object           # @rrDpe
	.globl	rrDpe
	.p2align	3
rrDpe:
	.zero	8
	.size	rrDpe, 8

	.type	rrdPb,@object           # @rrdPb
	.globl	rrdPb
	.p2align	3
rrdPb:
	.zero	8
	.size	rrdPb, 8

	.type	rrdPe,@object           # @rrdPe
	.globl	rrdPe
	.p2align	3
rrdPe:
	.zero	8
	.size	rrdPe, 8

	.type	rrDPb,@object           # @rrDPb
	.globl	rrDPb
	.p2align	3
rrDPb:
	.zero	8
	.size	rrDPb, 8

	.type	rrDPe,@object           # @rrDPe
	.globl	rrDPe
	.p2align	3
rrDPe:
	.zero	8
	.size	rrDPe, 8

	.type	.L.str.27,@object       # @.str.27
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.27:
	.asciz	"test %i failed\n"
	.size	.L.str.27, 16

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_stepanov_v1p2.ii
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"\ntest      absolute   additions      ratio with"
	.size	.Lstr, 48

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"number    time       per second     test0\n"
	.size	.Lstr.1, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
