	.text
	.file	"getbits.bc"
	.globl	Initialize_Buffer
	.p2align	4, 0x90
	.type	Initialize_Buffer,@function
Initialize_Buffer:                      # @Initialize_Buffer
	.cfi_startproc
# BB#0:
	movq	ld(%rip), %rax
	movl	$0, 2096(%rax)
	leaq	2052(%rax), %rcx
	movq	%rcx, 2056(%rax)
	movq	%rcx, 2088(%rax)
	movl	$0, 2080(%rax)
	xorl	%edi, %edi
	jmp	Flush_Buffer            # TAILCALL
.Lfunc_end0:
	.size	Initialize_Buffer, .Lfunc_end0-Initialize_Buffer
	.cfi_endproc

	.globl	Flush_Buffer
	.p2align	4, 0x90
	.type	Flush_Buffer,@function
Flush_Buffer:                           # @Flush_Buffer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edi, %r13d
	movq	ld(%rip), %rsi
	movl	2080(%rsi), %edi
	movl	%r13d, %ecx
	shll	%cl, %edi
	movl	%edi, 2080(%rsi)
	movl	2096(%rsi), %r14d
	movl	%r14d, %ebp
	subl	%r13d, %ebp
	movl	%ebp, 2096(%rsi)
	cmpl	$24, %ebp
	jg	.LBB1_34
# BB#1:
	cmpl	$0, System_Stream_Flag(%rip)
	movq	2056(%rsi), %rdx
	je	.LBB1_10
# BB#2:
	movq	2088(%rsi), %rax
	leaq	-4(%rax), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB1_3
.LBB1_10:                               # %._crit_edge
	leaq	2048(%rsi), %r8
	leal	8(%r14), %eax
	subl	%r13d, %eax
	cmpl	$24, %eax
	movl	$25, %ecx
	movl	$25, %ebx
	cmovgl	%eax, %ebx
	cmpq	%r8, %rdx
	leal	-1(%rbx,%r13), %r15d
	jae	.LBB1_11
# BB#24:                                # %.preheader.preheader
	cmpl	$24, %eax
	cmovgl	%eax, %ecx
	leal	-1(%rcx,%r13), %ecx
	subl	%r14d, %ecx
	movl	%ecx, %eax
	shrl	$3, %eax
	btl	$3, %ecx
	jb	.LBB1_26
# BB#25:                                # %.preheader.prol
	leaq	1(%rdx), %rbx
	movq	%rbx, 2056(%rsi)
	movzbl	(%rdx), %edx
	movl	$24, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	orl	%edx, %edi
	movl	%edi, 2080(%rsi)
	addl	$8, %ebp
	movq	%rbx, %rdx
.LBB1_26:                               # %.preheader.prol.loopexit
	testl	%eax, %eax
	je	.LBB1_33
# BB#27:                                # %.preheader.preheader.new
	incq	%rdx
	movl	$16, %eax
	subl	%ebp, %eax
	.p2align	4, 0x90
.LBB1_28:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, 2056(%rsi)
	movzbl	-1(%rdx), %ebx
	leal	8(%rax), %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	orl	%edi, %ebx
	movl	%ebx, 2080(%rsi)
	leaq	1(%rdx), %rcx
	movq	%rcx, 2056(%rsi)
	movzbl	(%rdx), %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	orl	%ebx, %edi
	movl	%edi, 2080(%rsi)
	addl	$16, %ebp
	addq	$2, %rdx
	addl	$-16, %eax
	cmpl	$25, %ebp
	jl	.LBB1_28
	jmp	.LBB1_33
.LBB1_11:
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_12:                               # %.preheader15
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #     Child Loop BB1_31 Depth 2
	leaq	2052(%rsi), %rax
	cmpq	%rax, %rdx
	jb	.LBB1_32
# BB#13:                                #   in Loop: Header=BB1_12 Depth=1
	movl	(%rsi), %edi
	addq	$4, %rsi
	movl	$2048, %edx             # imm = 0x800
	callq	read
	movq	ld(%rip), %rdx
	leaq	4(%rdx), %rcx
	movq	%rcx, 2056(%rdx)
	cmpl	$0, System_Stream_Flag(%rip)
	je	.LBB1_15
# BB#14:                                #   in Loop: Header=BB1_12 Depth=1
	addq	$-2048, 2088(%rdx)      # imm = 0xF800
.LBB1_15:                               #   in Loop: Header=BB1_12 Depth=1
	cmpl	$2047, %eax             # imm = 0x7FF
	jg	.LBB1_32
# BB#16:                                #   in Loop: Header=BB1_12 Depth=1
	testl	%eax, %eax
	cmovsl	%r12d, %eax
	testb	$3, %al
	je	.LBB1_22
# BB#17:                                # %.lr.ph12.preheader.i
                                        #   in Loop: Header=BB1_12 Depth=1
	movl	%eax, %ecx
	incl	%eax
	movb	$0, 4(%rdx,%rcx)
	testb	$3, %al
	je	.LBB1_21
# BB#18:                                # %.lr.ph12..lr.ph12_crit_edge.i.preheader
                                        #   in Loop: Header=BB1_12 Depth=1
	addq	$5, %rcx
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph12..lr.ph12_crit_edge.i
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	ld(%rip), %rax
	movb	$0, (%rax,%rcx)
	leal	-3(%rcx), %eax
	incq	%rcx
	testb	$3, %al
	jne	.LBB1_19
# BB#20:                                # %.preheader.i.loopexit
                                        #   in Loop: Header=BB1_12 Depth=1
	addl	$-4, %ecx
	movl	%ecx, %eax
.LBB1_21:                               # %.preheader.i
                                        #   in Loop: Header=BB1_12 Depth=1
	cmpl	$2047, %eax             # imm = 0x7FF
	jg	.LBB1_32
.LBB1_22:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB1_12 Depth=1
	movslq	%eax, %rcx
	leaq	4(%rcx), %rax
	cmpq	$2047, %rax             # imm = 0x7FF
	movl	$2048, %esi             # imm = 0x800
	cmovgq	%rax, %rsi
	decq	%rsi
	subq	%rcx, %rsi
	movq	%rsi, %rdx
	shrq	$2, %rdx
	btl	$2, %esi
	jb	.LBB1_23
# BB#29:                                # %.lr.ph.i14.prol
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	ld(%rip), %rsi
	movb	$0, 4(%rsi,%rcx)
	movq	ld(%rip), %rsi
	movb	$0, 5(%rsi,%rcx)
	movq	ld(%rip), %rsi
	movb	$1, 6(%rsi,%rcx)
	movq	ld(%rip), %rsi
	movb	$-73, 7(%rsi,%rcx)
	testq	%rdx, %rdx
	jne	.LBB1_31
	jmp	.LBB1_32
.LBB1_23:                               #   in Loop: Header=BB1_12 Depth=1
	movq	%rcx, %rax
	testq	%rdx, %rdx
	je	.LBB1_32
	.p2align	4, 0x90
.LBB1_31:                               # %.lr.ph.i14
                                        #   Parent Loop BB1_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	ld(%rip), %rcx
	movb	$0, 4(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 5(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$1, 6(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$-73, 7(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 8(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 9(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$1, 10(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$-73, 11(%rcx,%rax)
	addq	$8, %rax
	cmpq	$2048, %rax             # imm = 0x800
	jl	.LBB1_31
	.p2align	4, 0x90
.LBB1_32:                               # %Fill_Buffer.exit
                                        #   in Loop: Header=BB1_12 Depth=1
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rax
	leaq	1(%rax), %rdx
	movq	%rdx, 2056(%rsi)
	movzbl	(%rax), %eax
	movl	$24, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	%eax, 2080(%rsi)
	addl	$8, %ebp
	cmpl	$25, %ebp
	jl	.LBB1_12
	jmp	.LBB1_33
.LBB1_3:                                # %.preheader17.preheader
	leal	24(%r13), %r15d
	movq	$-2048, %rbx            # imm = 0xF800
	cmpq	%rax, %rdx
	jb	.LBB1_7
	jmp	.LBB1_5
	.p2align	4, 0x90
.LBB1_9:                                # %Get_Byte.exit..preheader17_crit_edge
                                        #   in Loop: Header=BB1_7 Depth=1
	movq	2056(%rsi), %rdx
	movq	2088(%rsi), %rax
	cmpq	%rax, %rdx
	jb	.LBB1_7
.LBB1_5:
	callq	Next_Packet
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rdx
	jmp	.LBB1_7
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        #   in Loop: Header=BB1_7 Depth=1
	movl	(%rsi), %edi
	addq	$4, %rsi
	movl	$2048, %edx             # imm = 0x800
	callq	read
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rdx
	addq	%rbx, %rdx
	movq	%rdx, 2056(%rsi)
	addq	$-2048, 2088(%rsi)      # imm = 0xF800
.LBB1_7:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	2052(%rsi), %rax
	cmpq	%rax, %rdx
	jae	.LBB1_6
# BB#8:                                 #   in Loop: Header=BB1_7 Depth=1
	leaq	2056(%rsi), %rax
	leaq	1(%rdx), %rcx
	movq	%rcx, (%rax)
	movzbl	(%rdx), %eax
	movl	$24, %ecx
	subl	%ebp, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	orl	%eax, 2080(%rsi)
	addl	$8, %ebp
	cmpl	$24, %ebp
	jle	.LBB1_9
.LBB1_33:                               # %.loopexit
	subl	%r14d, %r15d
	andl	$-8, %r15d
	leal	8(%r14,%r15), %eax
	subl	%r13d, %eax
	movl	%eax, 2096(%rsi)
.LBB1_34:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Flush_Buffer, .Lfunc_end1-Flush_Buffer
	.cfi_endproc

	.globl	Fill_Buffer
	.p2align	4, 0x90
	.type	Fill_Buffer,@function
Fill_Buffer:                            # @Fill_Buffer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	ld(%rip), %rsi
	movl	(%rsi), %edi
	addq	$4, %rsi
	movl	$2048, %edx             # imm = 0x800
	callq	read
	movq	ld(%rip), %rdx
	leaq	4(%rdx), %rcx
	movq	%rcx, 2056(%rdx)
	cmpl	$0, System_Stream_Flag(%rip)
	je	.LBB2_2
# BB#1:
	addq	$-2048, 2088(%rdx)      # imm = 0xF800
.LBB2_2:
	cmpl	$2047, %eax             # imm = 0x7FF
	jg	.LBB2_14
# BB#3:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovnsl	%eax, %ecx
	testb	$3, %cl
	je	.LBB2_9
# BB#4:                                 # %.lr.ph12.preheader
	movl	%ecx, %eax
	incl	%ecx
	movb	$0, 4(%rdx,%rax)
	testb	$3, %cl
	je	.LBB2_8
# BB#5:                                 # %.lr.ph12..lr.ph12_crit_edge.preheader
	addq	$5, %rax
	.p2align	4, 0x90
.LBB2_6:                                # %.lr.ph12..lr.ph12_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	ld(%rip), %rcx
	movb	$0, (%rcx,%rax)
	leal	-3(%rax), %ecx
	incq	%rax
	testb	$3, %cl
	jne	.LBB2_6
# BB#7:                                 # %.preheader.loopexit
	addl	$-4, %eax
	movl	%eax, %ecx
.LBB2_8:                                # %.preheader
	cmpl	$2047, %ecx             # imm = 0x7FF
	jg	.LBB2_14
.LBB2_9:                                # %.lr.ph.preheader
	movslq	%ecx, %rcx
	leaq	4(%rcx), %rax
	cmpq	$2047, %rax             # imm = 0x7FF
	movl	$2048, %esi             # imm = 0x800
	cmovgq	%rax, %rsi
	decq	%rsi
	subq	%rcx, %rsi
	movq	%rsi, %rdx
	shrq	$2, %rdx
	btl	$2, %esi
	jb	.LBB2_10
# BB#11:                                # %.lr.ph.prol
	movq	ld(%rip), %rsi
	movb	$0, 4(%rsi,%rcx)
	movq	ld(%rip), %rsi
	movb	$0, 5(%rsi,%rcx)
	movq	ld(%rip), %rsi
	movb	$1, 6(%rsi,%rcx)
	movq	ld(%rip), %rsi
	movb	$-73, 7(%rsi,%rcx)
	testq	%rdx, %rdx
	jne	.LBB2_13
	jmp	.LBB2_14
.LBB2_10:
	movq	%rcx, %rax
	testq	%rdx, %rdx
	je	.LBB2_14
	.p2align	4, 0x90
.LBB2_13:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	ld(%rip), %rcx
	movb	$0, 4(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 5(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$1, 6(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$-73, 7(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 8(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$0, 9(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$1, 10(%rcx,%rax)
	movq	ld(%rip), %rcx
	movb	$-73, 11(%rcx,%rax)
	addq	$8, %rax
	cmpq	$2048, %rax             # imm = 0x800
	jl	.LBB2_13
.LBB2_14:                               # %.loopexit
	popq	%rax
	retq
.Lfunc_end2:
	.size	Fill_Buffer, .Lfunc_end2-Fill_Buffer
	.cfi_endproc

	.globl	Get_Byte
	.p2align	4, 0x90
	.type	Get_Byte,@function
Get_Byte:                               # @Get_Byte
	.cfi_startproc
# BB#0:
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rax
	leaq	2052(%rsi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB3_2
# BB#1:
	addq	$2056, %rsi             # imm = 0x808
	jmp	.LBB3_5
.LBB3_2:                                # %.lr.ph.preheader
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movq	$-2048, %rbx            # imm = 0xF800
	.p2align	4, 0x90
.LBB3_3:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edi
	addq	$4, %rsi
	movl	$2048, %edx             # imm = 0x800
	callq	read
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rax
	addq	%rbx, %rax
	movq	%rax, 2056(%rsi)
	addq	$-2048, 2088(%rsi)      # imm = 0xF800
	leaq	2052(%rsi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB3_3
# BB#4:
	addq	$2056, %rsi             # imm = 0x808
	popq	%rbx
.LBB3_5:                                # %._crit_edge
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsi)
	movzbl	(%rax), %eax
	retq
.Lfunc_end3:
	.size	Get_Byte, .Lfunc_end3-Get_Byte
	.cfi_endproc

	.globl	Get_Word
	.p2align	4, 0x90
	.type	Get_Word,@function
Get_Word:                               # @Get_Word
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rax
	leaq	2052(%rsi), %rcx
	cmpq	%rcx, %rax
	jb	.LBB4_3
# BB#1:                                 # %.lr.ph.i.preheader
	movq	$-2048, %rbx            # imm = 0xF800
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edi
	addq	$4, %rsi
	movl	$2048, %edx             # imm = 0x800
	callq	read
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rax
	addq	%rbx, %rax
	movq	%rax, 2056(%rsi)
	addq	$-2048, 2088(%rsi)      # imm = 0xF800
	leaq	2052(%rsi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB4_2
.LBB4_3:
	leaq	2056(%rsi), %rcx
	leaq	1(%rax), %rdx
	movq	%rdx, (%rcx)
	movzbl	(%rax), %ebp
	shll	$8, %ebp
	movq	2056(%rsi), %rax
	leaq	2052(%rsi), %rcx
	cmpq	%rcx, %rax
	jb	.LBB4_6
# BB#4:                                 # %.lr.ph.i2.preheader
	movq	$-2048, %rbx            # imm = 0xF800
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph.i2
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edi
	addq	$4, %rsi
	movl	$2048, %edx             # imm = 0x800
	callq	read
	movq	ld(%rip), %rsi
	movq	2056(%rsi), %rax
	addq	%rbx, %rax
	movq	%rax, 2056(%rsi)
	addq	$-2048, 2088(%rsi)      # imm = 0xF800
	leaq	2052(%rsi), %rcx
	cmpq	%rcx, %rax
	jae	.LBB4_5
.LBB4_6:
	addq	$2056, %rsi             # imm = 0x808
	leaq	1(%rax), %rcx
	movq	%rcx, (%rsi)
	movzbl	(%rax), %eax
	orl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	Get_Word, .Lfunc_end4-Get_Word
	.cfi_endproc

	.globl	Show_Bits
	.p2align	4, 0x90
	.type	Show_Bits,@function
Show_Bits:                              # @Show_Bits
	.cfi_startproc
# BB#0:
	movq	ld(%rip), %rax
	movl	2080(%rax), %eax
	movl	$32, %ecx
	subl	%edi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	retq
.Lfunc_end5:
	.size	Show_Bits, .Lfunc_end5-Show_Bits
	.cfi_endproc

	.globl	Get_Bits1
	.p2align	4, 0x90
	.type	Get_Bits1,@function
Get_Bits1:                              # @Get_Bits1
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	ld(%rip), %rax
	movl	2080(%rax), %ebx
	shrl	$31, %ebx
	movl	$1, %edi
	callq	Flush_Buffer
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	Get_Bits1, .Lfunc_end6-Get_Bits1
	.cfi_endproc

	.globl	Get_Bits
	.p2align	4, 0x90
	.type	Get_Bits,@function
Get_Bits:                               # @Get_Bits
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	movq	ld(%rip), %rax
	movl	2080(%rax), %ebx
	movl	$32, %ecx
	subl	%edi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %ebx
	callq	Flush_Buffer
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	Get_Bits, .Lfunc_end7-Get_Bits
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
