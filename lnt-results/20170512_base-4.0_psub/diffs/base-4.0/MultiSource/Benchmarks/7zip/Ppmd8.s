	.text
	.file	"Ppmd8.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.zero	16,6
	.text
	.globl	Ppmd8_Construct
	.p2align	4, 0x90
	.type	Ppmd8_Construct,@function
Ppmd8_Construct:                        # @Ppmd8_Construct
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
.Lcfi2:
	.cfi_offset %rbx, -24
.Lcfi3:
	.cfi_offset %r14, -16
	movq	$0, 56(%rdi)
	xorl	%r14d, %r14d
	movl	$4, %r8d
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_1:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_14 Depth 2
                                        #     Child Loop BB0_17 Depth 2
	movl	%r14d, %r10d
	shrl	$2, %r10d
	incl	%r10d
	cmpq	$11, %r14
	cmoval	%r8d, %r10d
	cmpl	$32, %r10d
	movl	%r11d, %edx
	movl	%r10d, %ecx
	jb	.LBB0_12
# BB#2:                                 # %min.iters.checked
                                        #   in Loop: Header=BB0_1 Depth=1
	movl	%r10d, %r9d
	andl	$2147483616, %r9d       # imm = 0x7FFFFFE0
	movl	%r11d, %edx
	movl	%r10d, %ecx
	je	.LBB0_12
# BB#3:                                 # %vector.scevcheck
                                        #   in Loop: Header=BB0_1 Depth=1
	leal	-1(%r10), %ecx
	addl	%r11d, %ecx
	movl	%r11d, %edx
	movl	%r10d, %ecx
	jb	.LBB0_12
# BB#4:                                 # %vector.ph
                                        #   in Loop: Header=BB0_1 Depth=1
	movzbl	%r14b, %ecx
	movd	%ecx, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leal	-32(%r9), %edx
	movl	%edx, %esi
	shrl	$5, %esi
	incl	%esi
	andl	$3, %esi
	je	.LBB0_5
# BB#6:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	negl	%esi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_7:                                # %vector.body.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%rcx), %ebx
	movdqu	%xmm0, 166(%rdi,%rbx)
	movdqu	%xmm0, 182(%rdi,%rbx)
	addl	$32, %ecx
	incl	%esi
	jne	.LBB0_7
	jmp	.LBB0_8
.LBB0_5:                                #   in Loop: Header=BB0_1 Depth=1
	xorl	%ecx, %ecx
.LBB0_8:                                # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$96, %edx
	jb	.LBB0_10
	.p2align	4, 0x90
.LBB0_9:                                # %vector.body
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%r11,%rcx), %edx
	movdqu	%xmm0, 166(%rdi,%rdx)
	movdqu	%xmm0, 182(%rdi,%rdx)
	leal	32(%r11,%rcx), %edx
	movdqu	%xmm0, 166(%rdi,%rdx)
	movdqu	%xmm0, 182(%rdi,%rdx)
	leal	64(%r11,%rcx), %edx
	movdqu	%xmm0, 166(%rdi,%rdx)
	movdqu	%xmm0, 182(%rdi,%rdx)
	leal	96(%r11,%rcx), %edx
	movdqu	%xmm0, 166(%rdi,%rdx)
	movdqu	%xmm0, 182(%rdi,%rdx)
	subl	$-128, %ecx
	cmpl	%ecx, %r9d
	jne	.LBB0_9
.LBB0_10:                               # %middle.block
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	%r9d, %r10d
	je	.LBB0_18
# BB#11:                                #   in Loop: Header=BB0_1 Depth=1
	leal	(%r11,%r9), %edx
	movl	%r10d, %ecx
	subl	%r9d, %ecx
	.p2align	4, 0x90
.LBB0_12:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	leal	-1(%rcx), %r9d
	movl	%ecx, %esi
	andl	$7, %esi
	je	.LBB0_15
# BB#13:                                # %scalar.ph.prol.preheader
                                        #   in Loop: Header=BB0_1 Depth=1
	negl	%esi
	.p2align	4, 0x90
.LBB0_14:                               # %scalar.ph.prol
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, %ebx
	incl	%edx
	movb	%r14b, 166(%rdi,%rbx)
	decl	%ecx
	incl	%esi
	jne	.LBB0_14
.LBB0_15:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_1 Depth=1
	cmpl	$7, %r9d
	jb	.LBB0_18
# BB#16:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_1 Depth=1
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_17:                               # %scalar.ph
                                        #   Parent Loop BB0_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rdx,%rsi), %ebx
	leal	1(%rdx,%rsi), %eax
	movb	%r14b, 166(%rdi,%rbx)
	leal	2(%rdx,%rsi), %ebx
	movb	%r14b, 166(%rdi,%rax)
	leal	3(%rdx,%rsi), %eax
	movb	%r14b, 166(%rdi,%rbx)
	leal	4(%rdx,%rsi), %ebx
	movb	%r14b, 166(%rdi,%rax)
	leal	5(%rdx,%rsi), %eax
	movb	%r14b, 166(%rdi,%rbx)
	leal	6(%rdx,%rsi), %ebx
	movb	%r14b, 166(%rdi,%rax)
	leal	7(%rdx,%rsi), %eax
	movb	%r14b, 166(%rdi,%rbx)
	movb	%r14b, 166(%rdi,%rax)
	addl	$8, %esi
	cmpl	%esi, %ecx
	jne	.LBB0_17
.LBB0_18:                               # %.loopexit
                                        #   in Loop: Header=BB0_1 Depth=1
	addl	%r11d, %r10d
	movb	%r10b, 128(%rdi,%r14)
	incq	%r14
	cmpq	$38, %r14
	movl	%r10d, %r11d
	jne	.LBB0_1
# BB#19:                                # %.lr.ph.preheader
	movw	$512, 600(%rdi)         # imm = 0x200
	movabsq	$289360691352306692, %rax # imm = 0x404040404040404
	movq	%rax, 602(%rdi)
	movb	$4, 610(%rdi)
	movabsq	$434041037028460038, %rax # imm = 0x606060606060606
	movq	%rax, 848(%rdi)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [6,6,6,6,6,6,6,6,6,6,6,6,6,6,6,6]
	movups	%xmm0, 835(%rdi)
	movups	%xmm0, 819(%rdi)
	movups	%xmm0, 803(%rdi)
	movups	%xmm0, 787(%rdi)
	movups	%xmm0, 771(%rdi)
	movups	%xmm0, 755(%rdi)
	movups	%xmm0, 739(%rdi)
	movups	%xmm0, 723(%rdi)
	movups	%xmm0, 707(%rdi)
	movups	%xmm0, 691(%rdi)
	movups	%xmm0, 675(%rdi)
	movups	%xmm0, 659(%rdi)
	movups	%xmm0, 643(%rdi)
	movups	%xmm0, 627(%rdi)
	movups	%xmm0, 611(%rdi)
	movl	$50462976, 856(%rdi)    # imm = 0x3020100
	movb	$4, 860(%rdi)
	movl	$1, %edx
	movl	$5, %ecx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_20:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, 861(%rdi,%rax)
	decl	%edx
	leal	-3(%rcx), %esi
	xorl	%ebx, %ebx
	testl	%edx, %edx
	sete	%bl
	cmovnel	%edx, %esi
	leal	(%rbx,%rcx), %r8d
	movb	%r8b, 862(%rdi,%rax)
	xorl	%edx, %edx
	decl	%esi
	sete	%dl
	leal	-3(%rbx,%rcx), %ebx
	cmovnel	%esi, %ebx
	leal	(%rdx,%r8), %esi
	movb	%sil, 863(%rdi,%rax)
	xorl	%ecx, %ecx
	decl	%ebx
	sete	%cl
	leal	-3(%rdx,%r8), %edx
	cmovnel	%ebx, %edx
	addl	%esi, %ecx
	addq	$3, %rax
	cmpq	$255, %rax
	jne	.LBB0_20
# BB#21:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	Ppmd8_Construct, .Lfunc_end0-Ppmd8_Construct
	.cfi_endproc

	.globl	Ppmd8_Free
	.p2align	4, 0x90
	.type	Ppmd8_Free,@function
Ppmd8_Free:                             # @Ppmd8_Free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movq	56(%rbx), %rsi
	movq	%rax, %rdi
	callq	*8(%rax)
	movl	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Ppmd8_Free, .Lfunc_end1-Ppmd8_Free
	.cfi_endproc

	.globl	Ppmd8_Alloc
	.p2align	4, 0x90
	.type	Ppmd8_Alloc,@function
Ppmd8_Alloc:                            # @Ppmd8_Alloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -32
.Lcfi10:
	.cfi_offset %r14, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	56(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB2_2
# BB#1:
	movl	$1, %eax
	cmpl	%r14d, 48(%rbx)
	je	.LBB2_5
.LBB2_2:                                # %._crit_edge
	movq	%rbp, %rdi
	callq	*8(%rbp)
	movl	$0, 48(%rbx)
	movq	$0, 56(%rbx)
	movl	%r14d, %eax
	andl	$3, %eax
	movl	$4, %esi
	subl	%eax, %esi
	movl	%esi, 96(%rbx)
	addl	%r14d, %esi
	movq	%rbp, %rdi
	callq	*(%rbp)
	movq	%rax, 56(%rbx)
	testq	%rax, %rax
	je	.LBB2_3
# BB#4:
	movl	%r14d, 48(%rbx)
	movl	$1, %eax
	jmp	.LBB2_5
.LBB2_3:
	xorl	%eax, %eax
.LBB2_5:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	Ppmd8_Alloc, .Lfunc_end2-Ppmd8_Alloc
	.cfi_endproc

	.globl	Ppmd8_Init
	.p2align	4, 0x90
	.type	Ppmd8_Init,@function
Ppmd8_Init:                             # @Ppmd8_Init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	%esi, 36(%rbx)
	movl	%edx, 100(%rbx)
	callq	RestartModel
	movb	$7, 1118(%rbx)
	movw	$0, 1116(%rbx)
	movb	$64, 1119(%rbx)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	Ppmd8_Init, .Lfunc_end3-Ppmd8_Init
	.cfi_endproc

	.p2align	4, 0x90
	.type	RestartModel,@function
RestartModel:                           # @RestartModel
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	296(%rbx), %rdi
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movl	$304, %edx              # imm = 0x130
	callq	memset
	movq	56(%rbx), %r10
	movl	96(%rbx), %r8d
	leaq	(%r10,%r8), %rdx
	movq	%rdx, 80(%rbx)
	movl	48(%rbx), %esi
	leaq	(%rdx,%rsi), %rdi
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rsi, %rcx
	shrq	$38, %rcx
	imull	$84, %ecx, %r9d
	subq	%r9, %rdi
	movq	%rdi, 88(%rbx)
	movl	$0, 52(%rbx)
	movl	36(%rbx), %ecx
	movl	%ecx, 24(%rbx)
	cmpl	$12, %ecx
	notl	%ecx
	movl	$-13, %eax
	cmovbl	%ecx, %eax
	movl	%eax, 44(%rbx)
	movl	%eax, 40(%rbx)
	movl	$0, 32(%rbx)
	leaq	-12(%rsi,%rdx), %rax
	movq	%rax, 72(%rbx)
	movq	%rax, 8(%rbx)
	movq	%rax, (%rbx)
	movl	$0, -4(%rsi,%rdx)
	movb	$-1, -12(%rsi,%rdx)
	movb	$0, -11(%rsi,%rdx)
	movw	$257, -10(%rsi,%rdx)    # imm = 0x101
	movq	%rdi, 16(%rbx)
	leaq	1536(%rdi), %rax
	movq	%rax, 64(%rbx)
	subl	%r10d, %edi
	movl	%edi, -8(%rsi,%rdx)
	leaq	10(%rsi,%r8), %rdx
	subq	%r9, %rdx
	addq	%r10, %rdx
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movb	%r14b, -10(%rdx)
	movb	$1, -9(%rdx)
	movw	$0, -8(%rdx)
	movw	$0, -6(%rdx)
	leal	1(%r14), %eax
	movb	%al, -4(%rdx)
	movb	$1, -3(%rdx)
	movw	$0, -2(%rdx)
	movw	$0, (%rdx)
	addq	$2, %r14
	addq	$12, %rdx
	cmpq	$256, %r14              # imm = 0x100
	jne	.LBB4_1
# BB#2:                                 # %.preheader96.preheader
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_3:                                # =>This Inner Loop Header: Depth=1
	movl	%esi, %eax
	movzbl	856(%rbx,%rax), %eax
	incl	%esi
	cmpq	%rcx, %rax
	je	.LBB4_3
# BB#4:                                 # %.preheader95
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	$15581, %eax            # imm = 0x3CDD
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movq	%rcx, %rdi
	shlq	$7, %rdi
	movw	%dx, 4192(%rbx,%rdi)
	movw	%dx, 4208(%rbx,%rdi)
	movw	%dx, 4224(%rbx,%rdi)
	movw	%dx, 4240(%rbx,%rdi)
	movw	%dx, 4256(%rbx,%rdi)
	movw	%dx, 4272(%rbx,%rdi)
	movw	%dx, 4288(%rbx,%rdi)
	movw	%dx, 4304(%rbx,%rdi)
	movl	$7999, %eax             # imm = 0x1F3F
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, 4194(%rbx,%rdi)
	movw	%dx, 4210(%rbx,%rdi)
	movw	%dx, 4226(%rbx,%rdi)
	movw	%dx, 4242(%rbx,%rdi)
	movw	%dx, 4258(%rbx,%rdi)
	movw	%dx, 4274(%rbx,%rdi)
	movw	%dx, 4290(%rbx,%rdi)
	movw	%dx, 4306(%rbx,%rdi)
	movl	$22975, %eax            # imm = 0x59BF
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, 4196(%rbx,%rdi)
	movw	%dx, 4212(%rbx,%rdi)
	movw	%dx, 4228(%rbx,%rdi)
	movw	%dx, 4244(%rbx,%rdi)
	movw	%dx, 4260(%rbx,%rdi)
	movw	%dx, 4276(%rbx,%rdi)
	movw	%dx, 4292(%rbx,%rdi)
	movw	%dx, 4308(%rbx,%rdi)
	movl	$18675, %eax            # imm = 0x48F3
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, 4198(%rbx,%rdi)
	movw	%dx, 4214(%rbx,%rdi)
	movw	%dx, 4230(%rbx,%rdi)
	movw	%dx, 4246(%rbx,%rdi)
	movw	%dx, 4262(%rbx,%rdi)
	movw	%dx, 4278(%rbx,%rdi)
	movw	%dx, 4294(%rbx,%rdi)
	movw	%dx, 4310(%rbx,%rdi)
	movl	$25761, %eax            # imm = 0x64A1
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, 4200(%rbx,%rdi)
	movw	%dx, 4216(%rbx,%rdi)
	movw	%dx, 4232(%rbx,%rdi)
	movw	%dx, 4248(%rbx,%rdi)
	movw	%dx, 4264(%rbx,%rdi)
	movw	%dx, 4280(%rbx,%rdi)
	movw	%dx, 4296(%rbx,%rdi)
	movw	%dx, 4312(%rbx,%rdi)
	movl	$23228, %eax            # imm = 0x5ABC
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, 4202(%rbx,%rdi)
	movw	%dx, 4218(%rbx,%rdi)
	movw	%dx, 4234(%rbx,%rdi)
	movw	%dx, 4250(%rbx,%rdi)
	movw	%dx, 4266(%rbx,%rdi)
	movw	%dx, 4282(%rbx,%rdi)
	movw	%dx, 4298(%rbx,%rdi)
	movw	%dx, 4314(%rbx,%rdi)
	movl	$26162, %eax            # imm = 0x6632
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, 4204(%rbx,%rdi)
	movw	%dx, 4220(%rbx,%rdi)
	movw	%dx, 4236(%rbx,%rdi)
	movw	%dx, 4252(%rbx,%rdi)
	movw	%dx, 4268(%rbx,%rdi)
	movw	%dx, 4284(%rbx,%rdi)
	movw	%dx, 4300(%rbx,%rdi)
	movw	%dx, 4316(%rbx,%rdi)
	movl	$24657, %eax            # imm = 0x6051
	xorl	%edx, %edx
	divl	%esi
	movl	$16384, %edx            # imm = 0x4000
	subl	%eax, %edx
	movw	%dx, 4206(%rbx,%rdi)
	movw	%dx, 4222(%rbx,%rdi)
	movw	%dx, 4238(%rbx,%rdi)
	movw	%dx, 4254(%rbx,%rdi)
	movw	%dx, 4270(%rbx,%rdi)
	movw	%dx, 4286(%rbx,%rdi)
	movw	%dx, 4302(%rbx,%rdi)
	movw	%dx, 4318(%rbx,%rdi)
	incq	%rcx
	decl	%esi
	cmpq	$25, %rcx
	jne	.LBB4_3
# BB#5:                                 # %.preheader93.preheader
	leaq	1127(%rbx), %r8
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB4_6:                                # %.preheader93
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_7 Depth 2
                                        #     Child Loop BB4_9 Depth 2
	leaq	3(%rcx), %rdi
	leal	-1(%rdx), %eax
	shll	$4, %edx
	addl	$24, %edx
	movw	%dx, %si
	movl	%eax, %edx
	.p2align	4, 0x90
.LBB4_7:                                #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	4(%rdx), %eax
	movzbl	856(%rbx,%rax), %eax
	incl	%edx
	addl	$16, %esi
	cmpq	%rdi, %rax
	je	.LBB4_7
# BB#8:                                 # %.preheader
                                        #   in Loop: Header=BB4_6 Depth=1
	movl	$32, %edi
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB4_9:                                #   Parent Loop BB4_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$3, -5(%rax)
	movw	%si, -7(%rax)
	movb	$7, -4(%rax)
	movb	$3, -1(%rax)
	movw	%si, -3(%rax)
	movb	$7, (%rax)
	addq	$8, %rax
	addq	$-2, %rdi
	jne	.LBB4_9
# BB#10:                                #   in Loop: Header=BB4_6 Depth=1
	incq	%rcx
	subq	$-128, %r8
	cmpq	$24, %rcx
	jne	.LBB4_6
# BB#11:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	RestartModel, .Lfunc_end4-RestartModel
	.cfi_endproc

	.globl	Ppmd8_MakeEscFreq
	.p2align	4, 0x90
	.type	Ppmd8_MakeEscFreq,@function
Ppmd8_MakeEscFreq:                      # @Ppmd8_MakeEscFreq
	.cfi_startproc
# BB#0:
	movq	(%rdi), %r8
	movzbl	(%r8), %ecx
	cmpl	$255, %ecx
	je	.LBB5_2
# BB#1:
	movzbl	858(%rdi,%rcx), %r10d
	shlq	$7, %r10
	addq	%rdi, %r10
	movzwl	2(%r8), %r9d
	imull	$11, %ecx, %eax
	addl	$11, %eax
	cmpl	%r9d, %eax
	sbbq	%rax, %rax
	andl	$4, %eax
	addq	%r10, %rax
	addl	%ecx, %ecx
	movq	56(%rdi), %r9
	movl	8(%r8), %edi
	movzbl	(%r9,%rdi), %edi
	addl	%esi, %edi
	cmpl	%edi, %ecx
	sbbq	%rsi, %rsi
	andl	$8, %esi
	addq	%rax, %rsi
	movzbl	1(%r8), %r8d
	leaq	736(%rsi,%r8,4), %rdi
	movzwl	736(%rsi,%r8,4), %r9d
	movb	738(%rsi,%r8,4), %cl
	movl	%r9d, %eax
	shrl	%cl, %eax
	subl	%eax, %r9d
	movw	%r9w, 736(%rsi,%r8,4)
	cmpl	$1, %eax
	adcl	$0, %eax
	jmp	.LBB5_3
.LBB5_2:
	addq	$1116, %rdi             # imm = 0x45C
	movl	$1, %eax
.LBB5_3:
	movl	%eax, (%rdx)
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	Ppmd8_MakeEscFreq, .Lfunc_end5-Ppmd8_MakeEscFreq
	.cfi_endproc

	.globl	Ppmd8_Update1
	.p2align	4, 0x90
	.type	Ppmd8_Update1,@function
Ppmd8_Update1:                          # @Ppmd8_Update1
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rax
	movb	1(%rax), %cl
	addb	$4, %cl
	movb	%cl, 1(%rax)
	movq	(%rbx), %rdx
	addw	$4, 2(%rdx)
	cmpb	-5(%rax), %cl
	jbe	.LBB6_3
# BB#1:
	leaq	-6(%rax), %rcx
	movzwl	4(%rax), %edx
	movw	%dx, 12(%rsp)
	movl	(%rax), %edx
	movl	%edx, 8(%rsp)
	movzwl	-2(%rax), %edx
	movw	%dx, 4(%rax)
	movl	-6(%rax), %edx
	movl	%edx, (%rax)
	movzwl	12(%rsp), %edx
	movw	%dx, -2(%rax)
	movl	8(%rsp), %edx
	movl	%edx, -6(%rax)
	movq	%rcx, 16(%rbx)
	cmpb	$125, -5(%rax)
	movq	%rcx, %rax
	jb	.LBB6_3
# BB#2:
	movq	%rbx, %rdi
	callq	Rescale
	movq	16(%rbx), %rax
.LBB6_3:
	cmpl	$0, 24(%rbx)
	jne	.LBB6_6
# BB#4:
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	addq	56(%rbx), %rax
	cmpq	88(%rbx), %rax
	jae	.LBB6_5
.LBB6_6:
	movq	%rbx, %rdi
	callq	UpdateModel
	movq	8(%rbx), %rax
	jmp	.LBB6_7
.LBB6_5:
	movq	%rax, 8(%rbx)
.LBB6_7:                                # %NextContext.exit
	movq	%rax, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	Ppmd8_Update1, .Lfunc_end6-Ppmd8_Update1
	.cfi_endproc

	.p2align	4, 0x90
	.type	Rescale,@function
Rescale:                                # @Rescale
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi24:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi25:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi26:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 56
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	56(%rdi), %rsi
	movq	(%rdi), %rcx
	movq	16(%rdi), %rax
	movl	4(%rcx), %ebp
	leaq	(%rsi,%rbp), %rcx
	movzwl	4(%rax), %edx
	movw	%dx, -28(%rsp)
	movl	(%rax), %edx
	movl	%edx, -32(%rsp)
	cmpq	%rcx, %rax
	movq	%rsi, -24(%rsp)         # 8-byte Spill
	movq	%rbp, -8(%rsp)          # 8-byte Spill
	je	.LBB7_2
	.p2align	4, 0x90
.LBB7_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	-2(%rax), %edx
	movw	%dx, 4(%rax)
	movl	-6(%rax), %edx
	movl	%edx, (%rax)
	leaq	-6(%rax), %rax
	cmpq	%rax, %rcx
	jne	.LBB7_1
.LBB7_2:                                # %._crit_edge
	movzwl	-28(%rsp), %edx
	movw	%dx, 4(%rax)
	movl	-32(%rsp), %edx
	movl	%edx, (%rax)
	movq	(%rdi), %rax
	movzwl	2(%rax), %r12d
	movzbl	1(%rcx), %edx
	subl	%edx, %r12d
	addb	$4, %dl
	xorl	%r9d, %r9d
	movq	%rdi, -40(%rsp)         # 8-byte Spill
	cmpl	$0, 24(%rdi)
	setne	%r9b
	movzbl	%dl, %esi
	addl	%r9d, %esi
	shrl	%esi
	movb	%sil, 1(%rcx)
	movzbl	(%rax), %ebp
	leal	-1(%rbp), %r14d
	leaq	1(%r14), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rcx, %rbx
	movl	%esi, %r15d
	jmp	.LBB7_3
	.p2align	4, 0x90
.LBB7_8:                                # %..critedge_crit_edge
                                        #   in Loop: Header=BB7_3 Depth=1
	leaq	6(%rcx,%r8), %rdi
	jmp	.LBB7_9
	.p2align	4, 0x90
.LBB7_6:                                #   in Loop: Header=BB7_3 Depth=1
	movq	%rcx, %rdi
.LBB7_9:                                # %.critedge
                                        #   in Loop: Header=BB7_3 Depth=1
	movb	%r13b, (%rdi)
	movb	%r10b, 1(%rcx,%r11)
	movl	%esi, 2(%rcx,%r11)
.LBB7_10:                               #   in Loop: Header=BB7_3 Depth=1
	movl	%r12d, %r13d
	subl	%eax, %r13d
	addl	%r10d, %r15d
	decl	%ebp
	je	.LBB7_12
# BB#11:                                # %._crit_edge182
                                        #   in Loop: Header=BB7_3 Depth=1
	movb	7(%rbx), %sil
	addq	$6, %rbx
	addq	$6, %rdx
	movl	%r13d, %r12d
.LBB7_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_5 Depth 2
	movzbl	7(%rbx), %eax
	leal	(%rax,%r9), %r10d
	shrl	%r10d
	movb	%r10b, 7(%rbx)
	movzbl	%sil, %esi
	cmpl	%esi, %r10d
	jbe	.LBB7_10
# BB#4:                                 #   in Loop: Header=BB7_3 Depth=1
	movb	6(%rbx), %r13b
	movl	8(%rbx), %esi
	movq	%rdx, %r8
	.p2align	4, 0x90
.LBB7_5:                                #   Parent Loop BB7_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r8, %r11
	movzwl	4(%rcx,%r11), %edi
	movw	%di, 10(%rcx,%r11)
	movl	(%rcx,%r11), %edi
	movl	%edi, 6(%rcx,%r11)
	testq	%r11, %r11
	je	.LBB7_6
# BB#7:                                 #   in Loop: Header=BB7_5 Depth=2
	leaq	-6(%r11), %r8
	cmpb	-5(%rcx,%r11), %r10b
	ja	.LBB7_5
	jmp	.LBB7_8
.LBB7_12:
	movq	-16(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rdx,2), %rdx
	cmpb	$0, 1(%rcx,%rdx,2)
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx), %r8
	je	.LBB7_14
# BB#13:                                # %._crit_edge189
	movb	1(%r8), %bl
	movq	56(%rdx), %r10
	movl	4(%r8), %ecx
	jmp	.LBB7_44
.LBB7_14:
	movzbl	(%r8), %r9d
	leaq	(%r14,%r14,2), %rdx
	movq	-8(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,2), %rdx
	movq	-24(%rsp), %rsi         # 8-byte Reload
	leaq	1(%rsi,%rdx), %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_15:                               # =>This Inner Loop Header: Depth=1
	incl	%edx
	cmpb	$0, (%rsi)
	leaq	-6(%rsi), %rsi
	je	.LBB7_15
# BB#16:
	subl	%eax, %r12d
	leal	(%r12,%rdx), %r13d
	movl	%r9d, %r14d
	subl	%edx, %r14d
	movb	%r14b, (%r8)
	cmpb	%dl, %r9b
	jne	.LBB7_20
# BB#17:                                # %.critedge137
	movb	(%rcx), %bl
	movl	2(%rcx), %ebp
	movzbl	1(%rcx), %eax
	leal	(%r12,%rax,2), %eax
	leal	-1(%rdx,%rax), %eax
	xorl	%edx, %edx
	divl	%r13d
	movl	%eax, %edx
	andl	$254, %edx
	cmpl	$41, %edx
	movb	$41, %dl
	ja	.LBB7_19
# BB#18:                                # %.critedge137
	movl	%eax, %edx
.LBB7_19:                               # %.critedge137
	addl	$2, %r9d
	shrl	%r9d
	decl	%r9d
	movq	-40(%rsp), %rdi         # 8-byte Reload
	movzbl	166(%rdi,%r9), %eax
	movl	$-1, (%rcx)
	movl	296(%rdi,%rax,4), %esi
	movl	%esi, 4(%rcx)
	movzbl	128(%rdi,%rax), %esi
	movl	%esi, 8(%rcx)
	subl	56(%rdi), %ecx
	movl	%ecx, 296(%rdi,%rax,4)
	incl	448(%rdi,%rax,4)
	movb	1(%r8), %al
	andb	$16, %al
	cmpb	$63, %bl
	seta	%cl
	shlb	$3, %cl
	orb	%al, %cl
	movb	%cl, 1(%r8)
	leaq	2(%r8), %rax
	movq	%rax, 16(%rdi)
	movb	%bl, 2(%r8)
	movb	%dl, 3(%r8)
	movl	%ebp, 4(%r8)
	jmp	.LBB7_45
.LBB7_20:
	movzbl	%r14b, %r11d
	leal	2(%r9), %eax
	shrl	%eax
	leal	2(%r11), %ebp
	shrl	%ebp
	cmpl	%ebp, %eax
	jne	.LBB7_22
# BB#21:                                # %._crit_edge185
	movq	-40(%rsp), %rax         # 8-byte Reload
	movq	56(%rax), %r10
	movl	4(%r8), %ecx
	jmp	.LBB7_37
.LBB7_22:
	decl	%eax
	movq	-40(%rsp), %rbx         # 8-byte Reload
	movzbl	166(%rbx,%rax), %edi
	leal	-1(%rbp), %eax
	movzbl	166(%rbx,%rax), %esi
	cmpb	%sil, %dil
	jne	.LBB7_24
# BB#23:                                # %.ShrinkUnits.exit_crit_edge
	movq	56(%rbx), %r10
	jmp	.LBB7_36
.LBB7_24:
	movl	296(%rbx,%rsi,4), %r12d
	testq	%r12, %r12
	je	.LBB7_31
# BB#25:
	movq	%rdi, -16(%rsp)         # 8-byte Spill
	movq	56(%rbx), %r10
	leaq	(%r10,%r12), %rdi
	movq	%rdi, -24(%rsp)         # 8-byte Spill
	movl	4(%r10,%r12), %edi
	movl	%edi, 296(%rbx,%rsi,4)
	decl	448(%rbx,%rsi,4)
	testb	$1, %bpl
	jne	.LBB7_27
# BB#26:
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movq	%rdi, %rsi
	movq	%rcx, %r12
	movl	%ebp, %eax
	jmp	.LBB7_28
.LBB7_31:
	movzbl	128(%rbx,%rdi), %edi
	movzbl	128(%rbx,%rsi), %eax
	subl	%eax, %edi
	leaq	(%rax,%rax,2), %rax
	leaq	(%rcx,%rax,4), %rax
	leal	-1(%rdi), %r12d
	movzbl	166(%rbx,%r12), %esi
	movzbl	128(%rbx,%rsi), %ebp
	cmpl	%edi, %ebp
	movq	%rbx, %rdi
	jne	.LBB7_33
# BB#32:                                # %._crit_edge.i
	movq	56(%rdi), %r10
	jmp	.LBB7_34
.LBB7_27:
	movl	(%rcx), %esi
	movq	-24(%rsp), %rdi         # 8-byte Reload
	movl	%esi, (%rdi)
	movl	4(%rcx), %esi
	movl	%esi, 4(%rdi)
	movl	8(%rcx), %esi
	movl	%esi, 8(%rdi)
	leaq	12(%rcx), %r12
	movq	%rdi, %rsi
	addq	$12, %rsi
.LBB7_28:                               # %.prol.loopexit211
	movq	%rdi, %rbx
	cmpl	$1, %ebp
	je	.LBB7_30
	.p2align	4, 0x90
.LBB7_29:                               # =>This Inner Loop Header: Depth=1
	movl	(%r12), %edi
	movl	%edi, (%rsi)
	movl	4(%r12), %edi
	movl	%edi, 4(%rsi)
	movl	8(%r12), %edi
	movl	%edi, 8(%rsi)
	movl	12(%r12), %edi
	movl	%edi, 12(%rsi)
	movl	16(%r12), %edi
	movl	%edi, 16(%rsi)
	movl	20(%r12), %edi
	movl	%edi, 20(%rsi)
	addq	$24, %r12
	addq	$24, %rsi
	addl	$-2, %eax
	jne	.LBB7_29
.LBB7_30:
	movl	$-1, (%rcx)
	movq	%rcx, %rax
	movq	-40(%rsp), %rdi         # 8-byte Reload
	movq	-16(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB7_35
.LBB7_33:
	leal	-1(%rsi), %esi
	movzbl	128(%rdi,%rsi), %r10d
	leaq	(%r10,%r10,2), %rbp
	leaq	(%rax,%rbp,4), %rbx
	subl	%r10d, %r12d
	movl	$-1, (%rax,%rbp,4)
	movq	-40(%rsp), %rdi         # 8-byte Reload
	movl	296(%rdi,%r12,4), %edi
	movl	%edi, 4(%rax,%rbp,4)
	movq	-40(%rsp), %rdi         # 8-byte Reload
	movzbl	128(%rdi,%r12), %edi
	movl	%edi, 8(%rax,%rbp,4)
	movq	-40(%rsp), %rdi         # 8-byte Reload
	movq	56(%rdi), %r10
	subl	%r10d, %ebx
	movl	%ebx, 296(%rdi,%r12,4)
	incl	448(%rdi,%r12,4)
.LBB7_34:                               # %SplitBlock.exit
	movl	$-1, (%rax)
	movl	%esi, %esi
	movq	%rcx, %rbx
.LBB7_35:                               # %ShrinkUnits.exit.sink.split
	movl	296(%rdi,%rsi,4), %ecx
	movl	%ecx, 4(%rax)
	movzbl	128(%rdi,%rsi), %ecx
	movl	%ecx, 8(%rax)
	subl	%r10d, %eax
	movl	%eax, 296(%rdi,%rsi,4)
	incl	448(%rdi,%rsi,4)
	movq	%rbx, %rcx
.LBB7_36:                               # %ShrinkUnits.exit
	subl	%r10d, %ecx
	movl	%ecx, 4(%r8)
.LBB7_37:
	movb	1(%r8), %al
	andb	$-9, %al
	movl	%ecx, %esi
	leaq	(%r10,%rsi), %rbp
	cmpb	$63, (%r10,%rsi)
	seta	%bl
	shlb	$3, %bl
	orb	%al, %bl
	movb	%bl, 1(%r8)
	leal	-1(%r11), %esi
	testb	$3, %r14b
	je	.LBB7_40
# BB#38:                                # %.prol.preheader
	decl	%edx
	addb	$3, %r9b
	subb	%dl, %r9b
	movzbl	%r9b, %edx
	andl	$3, %edx
	negl	%edx
	.p2align	4, 0x90
.LBB7_39:                               # =>This Inner Loop Header: Depth=1
	cmpb	$63, 6(%rbp)
	leaq	6(%rbp), %rbp
	seta	%al
	shlb	$3, %al
	orb	%al, %bl
	decl	%r11d
	incl	%edx
	jne	.LBB7_39
.LBB7_40:                               # %.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB7_43
# BB#41:                                # %.new
	addq	$24, %rbp
	.p2align	4, 0x90
.LBB7_42:                               # =>This Inner Loop Header: Depth=1
	cmpb	$63, -18(%rbp)
	seta	%al
	shlb	$3, %al
	orb	%bl, %al
	cmpb	$63, -12(%rbp)
	seta	%bl
	shlb	$3, %bl
	cmpb	$63, -6(%rbp)
	seta	%dl
	shlb	$3, %dl
	orb	%bl, %dl
	orb	%al, %dl
	cmpb	$63, (%rbp)
	seta	%bl
	shlb	$3, %bl
	orb	%dl, %bl
	addq	$24, %rbp
	addl	$-4, %r11d
	jne	.LBB7_42
.LBB7_43:                               # %.loopexit
	movb	%bl, 1(%r8)
	movq	-40(%rsp), %rdx         # 8-byte Reload
.LBB7_44:
	addl	%r13d, %r15d
	shrl	%r13d
	subl	%r13d, %r15d
	movw	%r15w, 2(%r8)
	orb	$4, %bl
	movb	%bl, 1(%r8)
	movl	%ecx, %eax
	addq	%r10, %rax
	movq	%rax, 16(%rdx)
.LBB7_45:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	Rescale, .Lfunc_end7-Rescale
	.cfi_endproc

	.globl	Ppmd8_Update1_0
	.p2align	4, 0x90
	.type	Ppmd8_Update1_0,@function
Ppmd8_Update1_0:                        # @Ppmd8_Update1_0
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 16
.Lcfi35:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %r8
	movq	16(%rbx), %rax
	movzbl	1(%rax), %edx
	movl	%edx, %esi
	addl	%esi, %esi
	movzwl	2(%r8), %edi
	xorl	%ecx, %ecx
	cmpl	%edi, %esi
	setae	%cl
	movl	%ecx, 32(%rbx)
	addl	%ecx, 40(%rbx)
	addl	$4, %edi
	movw	%di, 2(%r8)
	addb	$4, %dl
	movb	%dl, 1(%rax)
	cmpb	$125, %dl
	jb	.LBB8_2
# BB#1:
	movq	%rbx, %rdi
	callq	Rescale
	movq	16(%rbx), %rax
.LBB8_2:
	cmpl	$0, 24(%rbx)
	jne	.LBB8_5
# BB#3:
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	addq	56(%rbx), %rax
	cmpq	88(%rbx), %rax
	jae	.LBB8_4
.LBB8_5:
	movq	%rbx, %rdi
	callq	UpdateModel
	movq	8(%rbx), %rax
	jmp	.LBB8_6
.LBB8_4:
	movq	%rax, 8(%rbx)
.LBB8_6:                                # %NextContext.exit
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end8:
	.size	Ppmd8_Update1_0, .Lfunc_end8-Ppmd8_Update1_0
	.cfi_endproc

	.globl	Ppmd8_UpdateBin
	.p2align	4, 0x90
	.type	Ppmd8_UpdateBin,@function
Ppmd8_UpdateBin:                        # @Ppmd8_UpdateBin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 16
.Lcfi37:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	16(%rbx), %rax
	movb	1(%rax), %cl
	cmpb	$-60, %cl
	adcb	$0, %cl
	movb	%cl, 1(%rax)
	movl	$1, 32(%rbx)
	incl	40(%rbx)
	cmpl	$0, 24(%rbx)
	jne	.LBB9_3
# BB#1:
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %eax
	shlq	$16, %rax
	orq	%rcx, %rax
	addq	56(%rbx), %rax
	cmpq	88(%rbx), %rax
	jae	.LBB9_2
.LBB9_3:
	movq	%rbx, %rdi
	callq	UpdateModel
	movq	8(%rbx), %rax
	jmp	.LBB9_4
.LBB9_2:
	movq	%rax, 8(%rbx)
.LBB9_4:                                # %NextContext.exit
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end9:
	.size	Ppmd8_UpdateBin, .Lfunc_end9-Ppmd8_UpdateBin
	.cfi_endproc

	.globl	Ppmd8_Update2
	.p2align	4, 0x90
	.type	Ppmd8_Update2,@function
Ppmd8_Update2:                          # @Ppmd8_Update2
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	addw	$4, 2(%rax)
	movq	16(%rbx), %rax
	movb	1(%rax), %cl
	addb	$4, %cl
	movb	%cl, 1(%rax)
	cmpb	$125, %cl
	jb	.LBB10_2
# BB#1:
	movq	%rbx, %rdi
	callq	Rescale
.LBB10_2:
	movl	44(%rbx), %eax
	movl	%eax, 40(%rbx)
	movq	%rbx, %rdi
	callq	UpdateModel
	movq	8(%rbx), %rax
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end10:
	.size	Ppmd8_Update2, .Lfunc_end10-Ppmd8_Update2
	.cfi_endproc

	.p2align	4, 0x90
	.type	UpdateModel,@function
UpdateModel:                            # @UpdateModel
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 128
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %rax
	movzwl	2(%rax), %ecx
	movzwl	4(%rax), %r13d
	shll	$16, %r13d
	orl	%ecx, %r13d
	movzbl	1(%rax), %r11d
	cmpl	$30, %r11d
	movb	(%rax), %r9b
	ja	.LBB11_10
# BB#1:
	movq	(%r14), %rax
	movl	8(%rax), %edx
	testq	%rdx, %rdx
	je	.LBB11_10
# BB#2:
	movq	56(%r14), %rcx
	leaq	(%rcx,%rdx), %rax
	cmpb	$0, (%rcx,%rdx)
	je	.LBB11_11
# BB#3:
	movl	4(%rax), %edx
	leaq	(%rcx,%rdx), %rbx
	cmpb	%r9b, (%rcx,%rdx)
	je	.LBB11_7
	.p2align	4, 0x90
.LBB11_4:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpb	%r9b, 6(%rbx)
	leaq	6(%rbx), %rbx
	jne	.LBB11_4
# BB#5:
	movb	1(%rbx), %cl
	cmpb	-5(%rbx), %cl
	jb	.LBB11_7
# BB#6:
	movzwl	4(%rbx), %ecx
	movw	%cx, 20(%rsp)
	movl	(%rbx), %ecx
	movl	%ecx, 16(%rsp)
	movzwl	-2(%rbx), %ecx
	movw	%cx, 4(%rbx)
	movl	-6(%rbx), %ecx
	movl	%ecx, (%rbx)
	movzwl	20(%rsp), %ecx
	movw	%cx, -2(%rbx)
	movl	16(%rsp), %ecx
	movl	%ecx, -6(%rbx)
	leaq	-6(%rbx), %rbx
.LBB11_7:
	movb	1(%rbx), %cl
	cmpb	$114, %cl
	ja	.LBB11_14
# BB#8:
	addb	$2, %cl
	movb	%cl, 1(%rbx)
	addw	$2, 2(%rax)
	jmp	.LBB11_14
.LBB11_10:
	xorl	%ebx, %ebx
.LBB11_14:
	movq	8(%r14), %r12
	testl	%r13d, %r13d
	je	.LBB11_18
# BB#15:
	movl	24(%r14), %eax
	testl	%eax, %eax
	jne	.LBB11_18
# BB#16:
	movq	(%r14), %rcx
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	CreateSuccessors
	movq	16(%r14), %rcx
	testq	%rax, %rax
	je	.LBB11_23
# BB#17:
	movq	%rax, %rdx
	subq	56(%r14), %rdx
	movw	%dx, 2(%rcx)
	shrl	$16, %edx
	movw	%dx, 4(%rcx)
	jmp	.LBB11_84
.LBB11_18:
	movq	16(%r14), %rax
	movq	80(%r14), %rcx
	movb	(%rax), %al
	leaq	1(%rcx), %rdx
	movq	%rdx, 80(%r14)
	movb	%al, (%rcx)
	movq	80(%r14), %r15
	movq	88(%r14), %rax
	cmpq	%rax, %r15
	jae	.LBB11_24
# BB#19:
	movq	56(%r14), %r10
	subq	%r10, %r15
	testl	%r13d, %r13d
	movb	%r9b, 14(%rsp)          # 1-byte Spill
	je	.LBB11_25
# BB#20:
	movl	%r13d, %edx
	addq	%r10, %rdx
	cmpq	%rax, %rdx
	jae	.LBB11_50
# BB#21:
	movl	%r11d, %ebp
	movq	(%r14), %rcx
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	CreateSuccessors
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB11_24
# BB#22:                                # %.thread238
	movq	56(%r14), %r10
	movl	%ebp, %r11d
	movb	14(%rsp), %r9b          # 1-byte Reload
	jmp	.LBB11_49
.LBB11_23:
	movw	$0, 2(%rcx)
	movw	$0, 4(%rcx)
.LBB11_24:
	movq	%r14, %rdi
	movq	%r12, %rsi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	RestoreModel            # TAILCALL
.LBB11_11:
	movb	3(%rax), %cl
	addq	$2, %rax
	cmpb	$31, %cl
	ja	.LBB11_13
# BB#12:
	incb	%cl
	movb	%cl, 1(%rax)
.LBB11_13:
	movq	%rax, %rbx
	jmp	.LBB11_14
.LBB11_25:
	movq	(%r14), %r13
	movq	16(%r14), %rcx
	movw	%r15w, 2(%rcx)
	movl	%r15d, %r8d
	shrl	$16, %r8d
	leaq	4(%rcx), %rbp
	movl	24(%r14), %edx
	movq	%r13, 40(%rsp)          # 8-byte Spill
	jmp	.LBB11_27
	.p2align	4, 0x90
.LBB11_26:                              #   in Loop: Header=BB11_27 Depth=1
	movw	%r15w, 2(%rbx)
	xorl	%ebx, %ebx
.LBB11_27:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_33 Depth 2
	movw	%r8w, (%rbp)
	incl	%edx
	testq	%rbx, %rbx
	movl	8(%r13), %eax
	je	.LBB11_30
# BB#28:                                #   in Loop: Header=BB11_27 Depth=1
	addq	%r10, %rax
	movq	%rax, %r13
.LBB11_29:                              #   in Loop: Header=BB11_27 Depth=1
	movzwl	2(%rbx), %eax
	leaq	4(%rbx), %rbp
	movzwl	4(%rbx), %esi
	movl	%esi, %edi
	shll	$16, %edi
	orl	%eax, %edi
	je	.LBB11_26
	jmp	.LBB11_37
	.p2align	4, 0x90
.LBB11_30:                              #   in Loop: Header=BB11_27 Depth=1
	testl	%eax, %eax
	je	.LBB11_40
# BB#31:                                #   in Loop: Header=BB11_27 Depth=1
	leaq	(%r10,%rax), %r13
	cmpb	$0, (%r10,%rax)
	je	.LBB11_36
# BB#32:                                #   in Loop: Header=BB11_27 Depth=1
	movl	4(%r13), %esi
	leaq	(%r10,%rsi), %rbx
	movb	(%rcx), %al
	cmpb	%al, (%r10,%rsi)
	je	.LBB11_34
	.p2align	4, 0x90
.LBB11_33:                              # %.preheader.i
                                        #   Parent Loop BB11_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	%al, 6(%rbx)
	leaq	6(%rbx), %rbx
	jne	.LBB11_33
.LBB11_34:                              # %.loopexit.i
                                        #   in Loop: Header=BB11_27 Depth=1
	movb	1(%rbx), %al
	cmpb	$114, %al
	ja	.LBB11_29
# BB#35:                                #   in Loop: Header=BB11_27 Depth=1
	addb	$2, %al
	movb	%al, 1(%rbx)
	addw	$2, 2(%r13)
	jmp	.LBB11_29
.LBB11_36:                              #   in Loop: Header=BB11_27 Depth=1
	leaq	2(%r13), %rbx
	movb	3(%r13), %al
	cmpb	$32, %al
	adcb	$0, %al
	movb	%al, 3(%r13)
	jmp	.LBB11_29
.LBB11_37:
	movl	%edx, 24(%r14)
	cmpl	%r15d, %edi
	ja	.LBB11_43
# BB#38:
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movl	%r11d, %r15d
	movq	%rbx, 16(%r14)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	movq	%r13, %rcx
	callq	CreateSuccessors
	testq	%rax, %rax
	je	.LBB11_41
# BB#39:
	subq	56(%r14), %rax
	movw	%ax, 2(%rbx)
	movl	%eax, %esi
	shrl	$16, %esi
	jmp	.LBB11_42
.LBB11_40:                              # %.loopexit75.i
	movl	%edx, 24(%r14)
	testq	%r13, %r13
	jne	.LBB11_49
	jmp	.LBB11_24
.LBB11_41:
	movw	$0, 2(%rbx)
	xorl	%eax, %eax
	xorl	%esi, %esi
.LBB11_42:
	movq	32(%rsp), %rcx          # 8-byte Reload
	movw	%si, (%rbp)
	movq	%rcx, 16(%r14)
	movl	24(%r14), %edx
	movl	%r15d, %r11d
	movb	14(%rsp), %r9b          # 1-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB11_43:
	cmpl	$1, %edx
	jne	.LBB11_46
# BB#44:
	movq	40(%rsp), %rdx          # 8-byte Reload
	cmpq	%rdx, 8(%r14)
	jne	.LBB11_46
# BB#45:
	movw	%ax, 2(%rcx)
	movw	%si, 4(%rcx)
	decq	80(%r14)
	movw	2(%rbx), %ax
	movw	4(%rbx), %si
.LBB11_46:
	movzwl	%ax, %eax
	shll	$16, %esi
	orl	%eax, %esi
	je	.LBB11_24
# BB#47:
	movq	56(%r14), %r10
	movl	%esi, %r13d
	addq	%r10, %r13
	testq	%r13, %r13
	je	.LBB11_24
.LBB11_49:                              # %.sink.split
	subl	%r10d, %r13d
.LBB11_50:
	decl	24(%r14)
	je	.LBB11_52
# BB#51:                                # %._crit_edge279
	movq	(%r14), %rax
	jmp	.LBB11_53
.LBB11_52:
	movq	(%r14), %rax
	xorl	%edx, %edx
	cmpq	%rax, 8(%r14)
	movq	$-1, %rsi
	cmoveq	%rdx, %rsi
	addq	%rsi, 80(%r14)
	movl	%r13d, %r15d
.LBB11_53:
	cmpb	$63, %r9b
	seta	%r8b
	cmpq	%rax, %r12
	je	.LBB11_83
# BB#54:                                # %.lr.ph
	movzbl	(%rax), %ecx
	shlb	$3, %r8b
	movzwl	2(%rax), %eax
	xorl	%edx, %edx
	cmpb	$2, %cl
	seta	%dl
	movl	%edx, 56(%rsp)          # 4-byte Spill
	subl	%r11d, %eax
	addl	%r11d, %r11d
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	subl	%ecx, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r15d, %eax
	shrl	$16, %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movb	%r8b, 15(%rsp)          # 1-byte Spill
	movq	%r13, 64(%rsp)          # 8-byte Spill
	movq	%r15, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB11_55:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_76 Depth 2
	movzbl	(%r12), %ebx
	testl	%ebx, %ebx
	je	.LBB11_59
# BB#56:                                #   in Loop: Header=BB11_55 Depth=1
	testb	$1, %bl
	je	.LBB11_78
# BB#57:                                #   in Loop: Header=BB11_55 Depth=1
	leal	1(%rbx), %r15d
	shrl	%r15d
	leal	-1(%r15), %ebp
	movzbl	166(%r14,%rbp), %r13d
	cmpb	166(%r14,%r15), %r13b
	jne	.LBB11_61
# BB#58:                                #   in Loop: Header=BB11_55 Depth=1
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	jmp	.LBB11_78
	.p2align	4, 0x90
.LBB11_59:                              #   in Loop: Header=BB11_55 Depth=1
	movl	296(%r14), %edx
	testq	%rdx, %rdx
	je	.LBB11_63
# BB#60:                                #   in Loop: Header=BB11_55 Depth=1
	leaq	(%r10,%rdx), %rax
	movl	4(%r10,%rdx), %ecx
	movl	%ecx, 296(%r14)
	decl	448(%r14)
	testq	%rax, %rax
	jne	.LBB11_66
	jmp	.LBB11_24
.LBB11_61:                              #   in Loop: Header=BB11_55 Depth=1
	movl	300(%r14,%r13,4), %edx
	testq	%rdx, %rdx
	je	.LBB11_70
# BB#62:                                #   in Loop: Header=BB11_55 Depth=1
	leaq	(%r10,%rdx), %rax
	movl	4(%r10,%rdx), %ecx
	movl	%ecx, 300(%r14,%r13,4)
	decl	452(%r14,%r13,4)
	testq	%rax, %rax
	jne	.LBB11_73
	jmp	.LBB11_24
.LBB11_63:                              #   in Loop: Header=BB11_55 Depth=1
	movzbl	128(%r14), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rcx
	movl	72(%r14), %edx
	movq	64(%r14), %rax
	subl	%eax, %edx
	cmpl	%edx, %ecx
	jbe	.LBB11_65
# BB#64:                                #   in Loop: Header=BB11_55 Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	movl	%r11d, %ebp
	callq	AllocUnitsRare
	movb	15(%rsp), %r8b          # 1-byte Reload
	movb	14(%rsp), %r9b          # 1-byte Reload
	movl	%ebp, %r11d
	testq	%rax, %rax
	jne	.LBB11_66
	jmp	.LBB11_24
.LBB11_65:                              #   in Loop: Header=BB11_55 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 64(%r14)
	testq	%rax, %rax
	je	.LBB11_24
	.p2align	4, 0x90
.LBB11_66:                              # %.thread243
                                        #   in Loop: Header=BB11_55 Depth=1
	movzwl	6(%r12), %ecx
	movw	%cx, 4(%rax)
	movl	2(%r12), %ecx
	movl	%ecx, (%rax)
	movq	56(%r14), %r10
	movl	%eax, %edx
	subl	%r10d, %edx
	movl	%edx, 4(%r12)
	movb	1(%rax), %dl
	cmpb	$30, %dl
	jb	.LBB11_68
# BB#67:                                # %.thread243
                                        #   in Loop: Header=BB11_55 Depth=1
	movb	$120, %dl
	jmp	.LBB11_69
.LBB11_68:                              #   in Loop: Header=BB11_55 Depth=1
	addb	%dl, %dl
.LBB11_69:                              # %.thread243
                                        #   in Loop: Header=BB11_55 Depth=1
	movb	%dl, 1(%rax)
	movzbl	%dl, %edx
	movl	28(%r14), %eax
	addl	56(%rsp), %eax          # 4-byte Folded Reload
	addl	%edx, %eax
	jmp	.LBB11_79
.LBB11_70:                              #   in Loop: Header=BB11_55 Depth=1
	leaq	1(%r13), %rsi
	movzbl	128(%r14,%rsi), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rcx
	movl	72(%r14), %edx
	movq	64(%r14), %rax
	subl	%eax, %edx
	cmpl	%edx, %ecx
	jbe	.LBB11_72
# BB#71:                                #   in Loop: Header=BB11_55 Depth=1
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r11d, 52(%rsp)         # 4-byte Spill
	callq	AllocUnitsRare
	movl	52(%rsp), %r11d         # 4-byte Reload
	testq	%rax, %rax
	jne	.LBB11_73
	jmp	.LBB11_24
.LBB11_72:                              #   in Loop: Header=BB11_55 Depth=1
	addq	%rax, %rcx
	movq	%rcx, 64(%r14)
	testq	%rax, %rax
	je	.LBB11_24
.LBB11_73:                              #   in Loop: Header=BB11_55 Depth=1
	movq	56(%r14), %r10
	movl	4(%r12), %r8d
	leaq	(%r10,%r8), %r9
	testb	$1, %r15b
	jne	.LBB11_75
# BB#74:                                #   in Loop: Header=BB11_55 Depth=1
	movq	%rax, %rdx
	movq	%r9, %rdi
	movl	%r15d, %ebp
	cmpl	$1, %r15d
	jne	.LBB11_76
	jmp	.LBB11_77
.LBB11_75:                              #   in Loop: Header=BB11_55 Depth=1
	movl	(%r9), %edx
	movl	%edx, (%rax)
	movl	4(%r9), %edx
	movl	%edx, 4(%rax)
	movl	8(%r9), %edx
	movl	%edx, 8(%rax)
	movq	%r9, %rdi
	addq	$12, %rdi
	leaq	12(%rax), %rdx
	cmpl	$1, %r15d
	je	.LBB11_77
	.p2align	4, 0x90
.LBB11_76:                              #   Parent Loop BB11_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rdi), %esi
	movl	%esi, (%rdx)
	movl	4(%rdi), %esi
	movl	%esi, 4(%rdx)
	movl	8(%rdi), %esi
	movl	%esi, 8(%rdx)
	movl	12(%rdi), %esi
	movl	%esi, 12(%rdx)
	movl	16(%rdi), %esi
	movl	%esi, 16(%rdx)
	movl	20(%rdi), %esi
	movl	%esi, 20(%rdx)
	addq	$24, %rdi
	addq	$24, %rdx
	addl	$-2, %ebp
	jne	.LBB11_76
.LBB11_77:                              # %.thread239
                                        #   in Loop: Header=BB11_55 Depth=1
	movl	$-1, (%r9)
	movl	296(%r14,%r13,4), %edx
	movl	%edx, 4(%r9)
	movzbl	128(%r14,%r13), %edx
	movl	%edx, 8(%r9)
	movl	%r8d, 296(%r14,%r13,4)
	incl	448(%r14,%r13,4)
	subl	%r10d, %eax
	movl	%eax, 4(%r12)
	movb	14(%rsp), %r9b          # 1-byte Reload
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	24(%rsp), %r15          # 8-byte Reload
	movb	15(%rsp), %r8b          # 1-byte Reload
.LBB11_78:                              # %.thread241
                                        #   in Loop: Header=BB11_55 Depth=1
	movw	2(%r12), %ax
	leal	1(%rbx,%rbx,2), %edx
	cmpl	60(%rsp), %edx          # 4-byte Folded Reload
	sbbw	%dx, %dx
	subw	%dx, %ax
.LBB11_79:                              #   in Loop: Header=BB11_55 Depth=1
	movw	%ax, 2(%r12)
	movzwl	%ax, %eax
	leal	6(%rax), %edx
	imull	%r11d, %edx
	movq	40(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %ecx
	leal	(%rcx,%rcx), %edi
	leal	(%rdi,%rdi,2), %edi
	cmpl	%edi, %edx
	jae	.LBB11_81
# BB#80:                                #   in Loop: Header=BB11_55 Depth=1
	cmpl	%edx, %ecx
	sbbl	%edi, %edi
	andl	$1, %edi
	shll	$2, %ecx
	xorl	%ebp, %ebp
	cmpl	%ecx, %edx
	setae	%bpl
	leal	1(%rdi,%rbp), %edx
	movl	$4, %edi
	jmp	.LBB11_82
	.p2align	4, 0x90
.LBB11_81:                              #   in Loop: Header=BB11_55 Depth=1
	leal	(%rcx,%rcx,8), %edi
	cmpl	%edx, %edi
	sbbl	%edi, %edi
	andl	$1, %edi
	movl	%ecx, %esi
	shll	$2, %esi
	leal	(%rsi,%rsi,2), %esi
	cmpl	%edx, %esi
	adcl	$0, %edi
	leal	(%rcx,%rcx,4), %ecx
	leal	(%rcx,%rcx,2), %ecx
	cmpl	%edx, %ecx
	adcl	$0, %edi
	addl	$4, %edi
	movl	%edi, %edx
.LBB11_82:                              #   in Loop: Header=BB11_55 Depth=1
	addl	%eax, %edi
	movw	%di, 2(%r12)
	movl	4(%r12), %eax
	addq	%r10, %rax
	leaq	(%rbx,%rbx,2), %rcx
	movw	%r15w, 8(%rax,%rcx,2)
	movl	32(%rsp), %esi          # 4-byte Reload
	movw	%si, 10(%rax,%rcx,2)
	movb	%r9b, 6(%rax,%rcx,2)
	movb	%dl, 7(%rax,%rcx,2)
	orb	%r8b, 1(%r12)
	incb	%bl
	movb	%bl, (%r12)
	movl	8(%r12), %r12d
	addq	%r10, %r12
	cmpq	(%r14), %r12
	jne	.LBB11_55
.LBB11_83:                              # %._crit_edge
	movl	%r13d, %eax
	addq	%r10, %rax
	movq	%rax, (%r14)
.LBB11_84:                              # %.thread246
	movq	%rax, 8(%r14)
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	UpdateModel, .Lfunc_end11-UpdateModel
	.cfi_endproc

	.p2align	4, 0x90
	.type	CreateSuccessors,@function
CreateSuccessors:                       # @CreateSuccessors
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$152, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 208
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdi, %r14
	movq	16(%r14), %rdi
	movzwl	2(%rdi), %eax
	movzwl	4(%rdi), %r12d
	shll	$16, %r12d
	xorl	%ebp, %ebp
	testl	%esi, %esi
	jne	.LBB12_2
# BB#1:
	movq	%rdi, 16(%rsp)
	movl	$1, %ebp
.LBB12_2:                               # %.preheader
	orl	%eax, %r12d
	movl	8(%rbx), %esi
	testl	%esi, %esi
	movq	56(%r14), %rcx
	je	.LBB12_13
# BB#3:                                 # %.lr.ph
	leaq	-6(%rcx), %r8
	jmp	.LBB12_4
	.p2align	4, 0x90
.LBB12_12:                              # %.thread
                                        #   in Loop: Header=BB12_4 Depth=1
	movl	%ebp, %eax
	incl	%ebp
	movq	%rdx, 16(%rsp,%rax,8)
	movl	8(%rbx), %esi
	testl	%esi, %esi
	movl	$0, %edx
	jne	.LBB12_4
	jmp	.LBB12_13
	.p2align	4, 0x90
.LBB12_5:                               #   in Loop: Header=BB12_4 Depth=1
	cmpb	$0, (%rbx)
	je	.LBB12_10
# BB#6:                                 #   in Loop: Header=BB12_4 Depth=1
	movl	4(%rbx), %edx
	movb	(%rdi), %al
	addq	%r8, %rdx
	.p2align	4, 0x90
.LBB12_7:                               #   Parent Loop BB12_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpb	%al, 6(%rdx)
	leaq	6(%rdx), %rdx
	jne	.LBB12_7
# BB#8:                                 #   in Loop: Header=BB12_4 Depth=1
	movb	1(%rdx), %al
	cmpb	$114, %al
	ja	.LBB12_11
# BB#9:                                 #   in Loop: Header=BB12_4 Depth=1
	incb	%al
	movb	%al, 1(%rdx)
	incw	2(%rbx)
	jmp	.LBB12_11
.LBB12_10:                              #   in Loop: Header=BB12_4 Depth=1
	movl	8(%rbx), %eax
	cmpb	$0, (%rcx,%rax)
	leaq	2(%rbx), %rdx
	sete	%sil
	movb	3(%rbx), %r9b
	cmpb	$24, %r9b
	sbbb	%al, %al
	andb	%sil, %al
	addb	%r9b, %al
	movb	%al, 3(%rbx)
	jmp	.LBB12_11
	.p2align	4, 0x90
.LBB12_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_7 Depth 2
	movl	%esi, %ebx
	addq	%rcx, %rbx
	testq	%rdx, %rdx
	je	.LBB12_5
.LBB12_11:                              #   in Loop: Header=BB12_4 Depth=1
	movzwl	2(%rdx), %esi
	movzwl	4(%rdx), %eax
	shll	$16, %eax
	orl	%esi, %eax
	cmpl	%r12d, %eax
	je	.LBB12_12
# BB#30:
	movl	%eax, %ebx
	addq	%rcx, %rbx
	testl	%ebp, %ebp
	movq	%rbx, %rax
	je	.LBB12_29
.LBB12_13:                              # %.loopexit113
	movl	%r12d, %eax
	incl	%r12d
	movl	%r12d, %r8d
	shrl	$16, %r8d
	cmpb	$63, (%rdi)
	movb	(%rcx,%rax), %r15b
	seta	%r11b
	shlb	$4, %r11b
	cmpb	$63, %r15b
	seta	%sil
	shlb	$3, %sil
	movzbl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB12_14
# BB#15:
	movl	4(%rbx), %edx
	addq	%rcx, %rdx
	.p2align	4, 0x90
.LBB12_16:                              # =>This Inner Loop Header: Depth=1
	cmpb	%r15b, (%rdx)
	leaq	6(%rdx), %rdx
	jne	.LBB12_16
# BB#17:
	movzbl	-5(%rdx), %edx
	movzwl	2(%rbx), %edi
	incl	%edi
	addl	%edx, %eax
	subl	%eax, %edi
	leal	-2(%rdx,%rdx), %eax
	cmpl	%edi, %eax
	jbe	.LBB12_18
# BB#19:
	leal	-4(%rdx,%rdi,2), %eax
	xorl	%edx, %edx
	divl	%edi
	movl	%eax, %edi
	jmp	.LBB12_20
.LBB12_14:
	movb	3(%rbx), %dil
	jmp	.LBB12_21
.LBB12_18:
	decl	%edx
	leal	(%rdx,%rdx,4), %eax
	cmpl	%eax, %edi
	sbbl	%edi, %edi
	andl	$1, %edi
.LBB12_20:
	incl	%edi
.LBB12_21:                              # %.sink.split
	orb	%sil, %r11b
	leal	-1(%rbp), %eax
	leaq	16(%rsp,%rax,8), %r13
	movl	%r8d, 12(%rsp)          # 4-byte Spill
	movb	%r11b, 7(%rsp)          # 1-byte Spill
	movl	%edi, 8(%rsp)           # 4-byte Spill
	.p2align	4, 0x90
.LBB12_22:                              # =>This Inner Loop Header: Depth=1
	movq	72(%r14), %rax
	cmpq	64(%r14), %rax
	je	.LBB12_25
# BB#23:                                #   in Loop: Header=BB12_22 Depth=1
	addq	$-12, %rax
	movq	%rax, 72(%r14)
	jmp	.LBB12_24
	.p2align	4, 0x90
.LBB12_25:                              #   in Loop: Header=BB12_22 Depth=1
	movl	296(%r14), %edx
	testq	%rdx, %rdx
	je	.LBB12_27
# BB#26:                                #   in Loop: Header=BB12_22 Depth=1
	leaq	(%rcx,%rdx), %rax
	movl	4(%rcx,%rdx), %ecx
	movl	%ecx, 296(%r14)
	decl	448(%r14)
	jmp	.LBB12_24
.LBB12_27:                              #   in Loop: Header=BB12_22 Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	AllocUnitsRare
	movl	8(%rsp), %edi           # 4-byte Reload
	movzbl	7(%rsp), %r11d          # 1-byte Folded Reload
	movl	12(%rsp), %r8d          # 4-byte Reload
	testq	%rax, %rax
	je	.LBB12_28
	.p2align	4, 0x90
.LBB12_24:                              #   in Loop: Header=BB12_22 Depth=1
	movb	$0, (%rax)
	movb	%r11b, 1(%rax)
	movb	%r15b, 2(%rax)
	movb	%dil, 3(%rax)
	movw	%r12w, 4(%rax)
	movw	%r8w, 6(%rax)
	movq	56(%r14), %rcx
	subl	%ecx, %ebx
	movl	%ebx, 8(%rax)
	movq	%rax, %rdx
	subq	%rcx, %rdx
	movq	(%r13), %rsi
	movw	%dx, 2(%rsi)
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	shrl	$16, %edx
	decl	%ebp
	leaq	-8(%r13), %r13
	movw	%dx, 4(%rsi)
	movq	%rax, %rbx
	jne	.LBB12_22
.LBB12_29:                              # %.loopexit
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB12_28:
	xorl	%eax, %eax
	jmp	.LBB12_29
.Lfunc_end12:
	.size	CreateSuccessors, .Lfunc_end12-CreateSuccessors
	.cfi_endproc

	.p2align	4, 0x90
	.type	RestoreModel,@function
RestoreModel:                           # @RestoreModel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 32
	subq	$160, %rsp
.Lcfi69:
	.cfi_def_cfa_offset 192
.Lcfi70:
	.cfi_offset %rbx, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	56(%rbx), %rdi
	movl	96(%rbx), %ecx
	addq	%rdi, %rcx
	movq	%rcx, 80(%rbx)
	movq	8(%rbx), %r15
	cmpq	%r14, %r15
	jne	.LBB13_5
	jmp	.LBB13_2
	.p2align	4, 0x90
.LBB13_11:                              #   in Loop: Header=BB13_5 Depth=1
	movq	56(%rbx), %rdi
	movl	8(%r15), %r15d
	addq	%rdi, %r15
	cmpq	%r14, %r15
	je	.LBB13_2
.LBB13_5:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%r15), %ecx
	decb	%cl
	movb	%cl, (%r15)
	je	.LBB13_6
# BB#10:                                #   in Loop: Header=BB13_5 Depth=1
	movzbl	%cl, %edx
	addl	$3, %edx
	shrl	%edx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	Refresh
	jmp	.LBB13_11
	.p2align	4, 0x90
.LBB13_6:                               #   in Loop: Header=BB13_5 Depth=1
	movl	4(%r15), %esi
	movzbl	1(%r15), %ecx
	andb	$16, %cl
	cmpb	$63, (%rdi,%rsi)
	leaq	(%rdi,%rsi), %rdx
	seta	%al
	shlb	$3, %al
	orb	%cl, %al
	movb	%al, 1(%r15)
	leaq	2(%r15), %rcx
	movzwl	4(%rdi,%rsi), %eax
	movw	%ax, 6(%r15)
	movl	(%rdi,%rsi), %eax
	movl	%eax, 2(%r15)
	cmpq	%rdx, 88(%rbx)
	je	.LBB13_8
# BB#7:                                 #   in Loop: Header=BB13_5 Depth=1
	movl	$-1, (%rdx)
	movl	296(%rbx), %eax
	movl	%eax, 4(%rdx)
	movzbl	128(%rbx), %eax
	movl	%eax, 8(%rdx)
	subl	56(%rbx), %edx
	movl	%edx, 296(%rbx)
	incl	448(%rbx)
	jmp	.LBB13_9
.LBB13_8:                               #   in Loop: Header=BB13_5 Depth=1
	addq	$12, %rdx
	movq	%rdx, 88(%rbx)
.LBB13_9:                               # %SpecialFreeUnit.exit
                                        #   in Loop: Header=BB13_5 Depth=1
	movzbl	1(%rcx), %eax
	addl	$11, %eax
	shrl	$3, %eax
	movb	%al, 1(%rcx)
	jmp	.LBB13_11
.LBB13_2:                               # %.preheader65
	movq	(%rbx), %rcx
	cmpq	%r14, %rcx
	je	.LBB13_15
	.p2align	4, 0x90
.LBB13_3:                               # %.lr.ph74
                                        # =>This Inner Loop Header: Depth=1
	movzbl	(%r14), %edx
	testl	%edx, %edx
	je	.LBB13_4
# BB#12:                                #   in Loop: Header=BB13_3 Depth=1
	movzwl	2(%r14), %eax
	addl	$4, %eax
	movw	%ax, 2(%r14)
	movzwl	%ax, %eax
	leal	128(,%rdx,4), %esi
	cmpl	%esi, %eax
	jbe	.LBB13_14
# BB#13:                                #   in Loop: Header=BB13_3 Depth=1
	addl	$2, %edx
	shrl	%edx
	movl	$1, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	Refresh
	movq	(%rbx), %rcx
	movq	56(%rbx), %rdi
	jmp	.LBB13_14
	.p2align	4, 0x90
.LBB13_4:                               #   in Loop: Header=BB13_3 Depth=1
	movzbl	3(%r14), %eax
	movl	%eax, %edx
	shrb	%dl
	subb	%dl, %al
	movb	%al, 3(%r14)
.LBB13_14:                              #   in Loop: Header=BB13_3 Depth=1
	movl	8(%r14), %r14d
	addq	%rdi, %r14
	cmpq	%rcx, %r14
	jne	.LBB13_3
.LBB13_15:                              # %._crit_edge
	cmpl	$0, 100(%rbx)
	je	.LBB13_38
# BB#16:                                # %.preheader64
	movdqu	448(%rbx), %xmm1
	movd	128(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	pxor	%xmm0, %xmm0
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm1, %xmm2
	pshufd	$232, %xmm2, %xmm4      # xmm4 = xmm2[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	movdqu	464(%rbx), %xmm1
	movd	132(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm1, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	paddd	%xmm4, %xmm2
	movdqu	480(%rbx), %xmm1
	movd	136(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm4, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movdqu	496(%rbx), %xmm4
	movd	140(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3],xmm1[4],xmm0[4],xmm1[5],xmm0[5],xmm1[6],xmm0[6],xmm1[7],xmm0[7]
	punpcklwd	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1],xmm1[2],xmm0[2],xmm1[3],xmm0[3]
	pshufd	$245, %xmm1, %xmm5      # xmm5 = xmm1[1,1,3,3]
	pmuludq	%xmm4, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	paddd	%xmm3, %xmm1
	paddd	%xmm2, %xmm1
	movdqu	512(%rbx), %xmm2
	movd	144(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm2, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqu	528(%rbx), %xmm2
	movd	148(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	pshufd	$245, %xmm4, %xmm5      # xmm5 = xmm4[1,1,3,3]
	pmuludq	%xmm2, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm5, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	paddd	%xmm3, %xmm4
	movdqu	544(%rbx), %xmm3
	movd	152(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3],xmm2[4],xmm0[4],xmm2[5],xmm0[5],xmm2[6],xmm0[6],xmm2[7],xmm0[7]
	punpcklwd	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1],xmm2[2],xmm0[2],xmm2[3],xmm0[3]
	pshufd	$245, %xmm2, %xmm5      # xmm5 = xmm2[1,1,3,3]
	pmuludq	%xmm3, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm5, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	paddd	%xmm4, %xmm2
	paddd	%xmm1, %xmm2
	movdqu	560(%rbx), %xmm1
	movd	156(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3],xmm3[4],xmm0[4],xmm3[5],xmm0[5],xmm3[6],xmm0[6],xmm3[7],xmm0[7]
	punpcklwd	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1],xmm3[2],xmm0[2],xmm3[3],xmm0[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm4, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	movdqu	576(%rbx), %xmm1
	movd	160(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	punpcklbw	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3],xmm4[4],xmm0[4],xmm4[5],xmm0[5],xmm4[6],xmm0[6],xmm4[7],xmm0[7]
	punpcklwd	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1],xmm4[2],xmm0[2],xmm4[3],xmm0[3]
	pshufd	$245, %xmm4, %xmm0      # xmm0 = xmm4[1,1,3,3]
	pmuludq	%xmm1, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3]
	punpckldq	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	paddd	%xmm3, %xmm4
	paddd	%xmm2, %xmm4
	pshufd	$78, %xmm4, %xmm0       # xmm0 = xmm4[2,3,0,1]
	paddd	%xmm4, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	movzbl	164(%rbx), %ecx
	imull	592(%rbx), %ecx
	addl	%eax, %ecx
	movzbl	165(%rbx), %eax
	imull	596(%rbx), %eax
	addl	%ecx, %eax
	movl	48(%rbx), %ecx
	movl	72(%rbx), %edx
	subl	64(%rbx), %edx
	movl	88(%rbx), %esi
	subl	80(%rbx), %esi
	imull	$-12, %eax, %eax
	addl	%ecx, %eax
	subl	%edx, %eax
	subl	%esi, %eax
	shrl	%ecx
	cmpl	%ecx, %eax
	jae	.LBB13_17
.LBB13_38:
	movq	%rbx, %rdi
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	RestartModel            # TAILCALL
.LBB13_17:                              # %.preheader63
	movq	8(%rbx), %rsi
	movl	8(%rsi), %ecx
	testl	%ecx, %ecx
	je	.LBB13_20
	.p2align	4, 0x90
.LBB13_18:                              # =>This Inner Loop Header: Depth=1
	movl	%ecx, %edx
	movl	8(%rdi,%rdx), %ecx
	testl	%ecx, %ecx
	jne	.LBB13_18
# BB#19:                                # %.preheader.loopexit
	addq	%rdx, %rdi
	movq	%rdi, 8(%rbx)
	movq	%rdi, %rsi
	jmp	.LBB13_20
	.p2align	4, 0x90
.LBB13_35:                              # %GetUsedMemory.exit141
                                        #   in Loop: Header=BB13_20 Depth=1
	movdqu	448(%rbx), %xmm0
	movd	128(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	punpcklwd	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm3      # xmm3 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	movdqu	464(%rbx), %xmm0
	movd	132(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	punpcklwd	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3]
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	paddd	%xmm3, %xmm1
	movdqu	480(%rbx), %xmm0
	movd	136(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3],xmm2[4],xmm5[4],xmm2[5],xmm5[5],xmm2[6],xmm5[6],xmm2[7],xmm5[7]
	punpcklwd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqu	496(%rbx), %xmm3
	movd	140(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1],xmm0[2],xmm5[2],xmm0[3],xmm5[3],xmm0[4],xmm5[4],xmm0[5],xmm5[5],xmm0[6],xmm5[6],xmm0[7],xmm5[7]
	punpcklwd	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1],xmm0[2],xmm5[2],xmm0[3],xmm5[3]
	pshufd	$245, %xmm0, %xmm4      # xmm4 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm0    # xmm0 = xmm0[0],xmm3[0],xmm0[1],xmm3[1]
	paddd	%xmm2, %xmm0
	paddd	%xmm1, %xmm0
	movdqu	512(%rbx), %xmm1
	movd	144(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3],xmm2[4],xmm5[4],xmm2[5],xmm5[5],xmm2[6],xmm5[6],xmm2[7],xmm5[7]
	punpcklwd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm1, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm3, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movdqu	528(%rbx), %xmm1
	movd	148(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1],xmm3[2],xmm5[2],xmm3[3],xmm5[3],xmm3[4],xmm5[4],xmm3[5],xmm5[5],xmm3[6],xmm5[6],xmm3[7],xmm5[7]
	punpcklwd	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1],xmm3[2],xmm5[2],xmm3[3],xmm5[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm1, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	pmuludq	%xmm4, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	paddd	%xmm2, %xmm3
	movdqu	544(%rbx), %xmm2
	movd	152(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3],xmm1[4],xmm5[4],xmm1[5],xmm5[5],xmm1[6],xmm5[6],xmm1[7],xmm5[7]
	punpcklwd	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1],xmm1[2],xmm5[2],xmm1[3],xmm5[3]
	pshufd	$245, %xmm1, %xmm4      # xmm4 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshufd	$245, %xmm2, %xmm2      # xmm2 = xmm2[1,1,3,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	paddd	%xmm3, %xmm1
	paddd	%xmm0, %xmm1
	movdqu	560(%rbx), %xmm0
	movd	156(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3],xmm2[4],xmm5[4],xmm2[5],xmm5[5],xmm2[6],xmm5[6],xmm2[7],xmm5[7]
	punpcklwd	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1],xmm2[2],xmm5[2],xmm2[3],xmm5[3]
	pshufd	$245, %xmm2, %xmm3      # xmm3 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movdqu	576(%rbx), %xmm0
	movd	160(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	punpcklbw	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1],xmm3[2],xmm5[2],xmm3[3],xmm5[3],xmm3[4],xmm5[4],xmm3[5],xmm5[5],xmm3[6],xmm5[6],xmm3[7],xmm5[7]
	punpcklwd	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1],xmm3[2],xmm5[2],xmm3[3],xmm5[3]
	pshufd	$245, %xmm3, %xmm4      # xmm4 = xmm3[1,1,3,3]
	pmuludq	%xmm0, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm4, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	paddd	%xmm2, %xmm3
	paddd	%xmm1, %xmm3
	pshufd	$78, %xmm3, %xmm0       # xmm0 = xmm3[2,3,0,1]
	paddd	%xmm3, %xmm0
	pshufd	$229, %xmm0, %xmm1      # xmm1 = xmm0[1,1,2,3]
	paddd	%xmm0, %xmm1
	movd	%xmm1, %eax
	movzbl	164(%rbx), %ecx
	imull	592(%rbx), %ecx
	addl	%eax, %ecx
	movzbl	165(%rbx), %eax
	imull	596(%rbx), %eax
	addl	%ecx, %eax
	movl	48(%rbx), %ecx
	movl	72(%rbx), %edx
	subl	64(%rbx), %edx
	movl	88(%rbx), %esi
	subl	80(%rbx), %esi
	imull	$-12, %eax, %eax
	addl	%ecx, %eax
	subl	%edx, %eax
	subl	%esi, %eax
	shrl	$2, %ecx
	leal	(%rcx,%rcx,2), %ecx
	cmpl	%ecx, %eax
	jbe	.LBB13_37
# BB#36:                                # %GetUsedMemory.exit._crit_edge
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	8(%rbx), %rsi
.LBB13_20:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_25 Depth 2
                                        #     Child Loop BB13_27 Depth 2
                                        #       Child Loop BB13_29 Depth 3
                                        #         Child Loop BB13_30 Depth 4
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	CutOff
	pxor	%xmm5, %xmm5
	movdqa	%xmm5, 128(%rsp)
	movdqa	%xmm5, 112(%rsp)
	movdqa	%xmm5, 96(%rsp)
	movdqa	%xmm5, 80(%rsp)
	movdqa	%xmm5, 64(%rsp)
	movdqa	%xmm5, 48(%rsp)
	movdqa	%xmm5, 32(%rsp)
	movdqa	%xmm5, 16(%rsp)
	movdqa	%xmm5, (%rsp)
	movq	$0, 144(%rsp)
	movq	64(%rbx), %rax
	cmpq	72(%rbx), %rax
	je	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_20 Depth=1
	movl	$0, (%rax)
.LBB13_22:                              #   in Loop: Header=BB13_20 Depth=1
	movq	88(%rbx), %rax
	cmpl	$-1, (%rax)
	je	.LBB13_24
# BB#23:                                #   in Loop: Header=BB13_20 Depth=1
	movq	%rax, %rcx
	jmp	.LBB13_26
	.p2align	4, 0x90
.LBB13_24:                              # %.lr.ph40.i.preheader
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB13_25:                              # %.lr.ph40.i
                                        #   Parent Loop BB13_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$0, (%rcx)
	movl	8(%rax), %ecx
	decl	%ecx
	movzbl	166(%rbx,%rcx), %ecx
	incl	(%rsp,%rcx,4)
	movl	8(%rax), %ecx
	leaq	(%rcx,%rcx,2), %rdx
	leaq	(%rax,%rdx,4), %rcx
	cmpl	$-1, (%rax,%rdx,4)
	movq	%rcx, %rax
	je	.LBB13_25
.LBB13_26:                              # %._crit_edge41.i
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	%rcx, 88(%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB13_27:                              #   Parent Loop BB13_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_29 Depth 3
                                        #         Child Loop BB13_30 Depth 4
	movl	(%rsp,%rax,4), %ecx
	testl	%ecx, %ecx
	je	.LBB13_34
# BB#28:                                # %.lr.ph.i
                                        #   in Loop: Header=BB13_27 Depth=2
	leaq	296(%rbx,%rax,4), %rdx
	jmp	.LBB13_29
	.p2align	4, 0x90
.LBB13_31:                              #   in Loop: Header=BB13_29 Depth=3
	movq	%rdi, %rdx
.LBB13_29:                              # %.loopexit
                                        #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_27 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_30 Depth 4
	movl	(%rdx), %esi
	addq	56(%rbx), %rsi
	.p2align	4, 0x90
.LBB13_30:                              #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_27 Depth=2
                                        #       Parent Loop BB13_29 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	leaq	4(%rsi), %rdi
	cmpl	$0, (%rsi)
	jne	.LBB13_31
# BB#32:                                #   in Loop: Header=BB13_30 Depth=4
	movl	(%rdi), %esi
	movl	%esi, (%rdx)
	addq	56(%rbx), %rsi
	decl	448(%rbx,%rax,4)
	decl	%ecx
	jne	.LBB13_30
# BB#33:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB13_27 Depth=2
	movl	$0, (%rsp,%rax,4)
.LBB13_34:                              # %._crit_edge.i
                                        #   in Loop: Header=BB13_27 Depth=2
	incq	%rax
	cmpq	$38, %rax
	jne	.LBB13_27
	jmp	.LBB13_35
.LBB13_37:
	movl	$0, 52(%rbx)
	movl	36(%rbx), %eax
	movl	%eax, 24(%rbx)
	addq	$160, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	RestoreModel, .Lfunc_end13-RestoreModel
	.cfi_endproc

	.p2align	4, 0x90
	.type	AllocUnitsRare,@function
AllocUnitsRare:                         # @AllocUnitsRare
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi76:
	.cfi_def_cfa_offset 40
.Lcfi77:
	.cfi_offset %rbx, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	cmpl	$0, 52(%rdi)
	je	.LBB14_5
.LBB14_1:                               # %.preheader.preheader
	leal	1(%rsi), %eax
	.p2align	4, 0x90
.LBB14_2:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$38, %eax
	je	.LBB14_3
# BB#33:                                #   in Loop: Header=BB14_2 Depth=1
	movl	%eax, %edx
	movl	296(%rdi,%rdx,4), %ebx
	incl	%eax
	testq	%rbx, %rbx
	je	.LBB14_2
# BB#34:
	movq	56(%rdi), %r8
	leaq	(%r8,%rbx), %rax
	movl	4(%r8,%rbx), %ebp
	movl	%ebp, 296(%rdi,%rdx,4)
	decl	448(%rdi,%rdx,4)
	movzbl	128(%rdi,%rdx), %ebp
	movl	%esi, %edx
	movzbl	128(%rdi,%rdx), %edx
	subl	%edx, %ebp
	leaq	(%rdx,%rdx,2), %rdx
	leaq	(%rax,%rdx,4), %rdx
	leal	-1(%rbp), %ebx
	movzbl	166(%rdi,%rbx), %esi
	movzbl	128(%rdi,%rsi), %ecx
	cmpl	%ebp, %ecx
	je	.LBB14_36
# BB#35:
	leal	-1(%rsi), %esi
	movzbl	128(%rdi,%rsi), %r9d
	leaq	(%r9,%r9,2), %rbp
	leaq	(%rdx,%rbp,4), %r10
	subl	%r9d, %ebx
	movl	$-1, (%rdx,%rbp,4)
	movl	296(%rdi,%rbx,4), %ecx
	movl	%ecx, 4(%rdx,%rbp,4)
	movzbl	128(%rdi,%rbx), %ecx
	movl	%ecx, 8(%rdx,%rbp,4)
	subl	%r8d, %r10d
	movl	%r10d, 296(%rdi,%rbx,4)
	incl	448(%rdi,%rbx,4)
.LBB14_36:                              # %SplitBlock.exit
	movl	$-1, (%rdx)
	movl	%esi, %ecx
	movl	296(%rdi,%rcx,4), %esi
	movl	%esi, 4(%rdx)
	movzbl	128(%rdi,%rcx), %esi
	movl	%esi, 8(%rdx)
	subl	%r8d, %edx
	movl	%edx, 296(%rdi,%rcx,4)
	incl	448(%rdi,%rcx,4)
	jmp	.LBB14_37
.LBB14_3:
	movl	%esi, %eax
	movzbl	128(%rdi,%rax), %eax
	shlq	$2, %rax
	leaq	(%rax,%rax,2), %rcx
	decl	52(%rdi)
	movq	88(%rdi), %rax
	movl	80(%rdi), %edx
	movl	%eax, %esi
	subl	%edx, %esi
	cmpl	%ecx, %esi
	jbe	.LBB14_4
# BB#32:
	subq	%rcx, %rax
	movq	%rax, 88(%rdi)
	jmp	.LBB14_37
.LBB14_5:
	movl	$0, -4(%rsp)
	movl	$8192, 52(%rdi)         # imm = 0x2000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 576(%rdi)
	movups	%xmm0, 560(%rdi)
	movups	%xmm0, 544(%rdi)
	movups	%xmm0, 528(%rdi)
	movups	%xmm0, 512(%rdi)
	movups	%xmm0, 496(%rdi)
	movups	%xmm0, 480(%rdi)
	movups	%xmm0, 464(%rdi)
	movups	%xmm0, 448(%rdi)
	movq	$0, 592(%rdi)
	movq	64(%rdi), %rax
	cmpq	72(%rdi), %rax
	je	.LBB14_7
# BB#6:
	movl	$0, (%rax)
.LBB14_7:                               # %.preheader70.i
	leaq	-4(%rsp), %r9
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB14_8:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_10 Depth 2
                                        #       Child Loop BB14_14 Depth 3
	movl	296(%rdi,%r8,4), %ebx
	movl	$0, 296(%rdi,%r8,4)
	testl	%ebx, %ebx
	je	.LBB14_17
# BB#9:                                 # %.lr.ph84.i
                                        #   in Loop: Header=BB14_8 Depth=1
	movq	56(%rdi), %r10
	.p2align	4, 0x90
.LBB14_10:                              #   Parent Loop BB14_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_14 Depth 3
	movl	%ebx, %eax
	leaq	(%r10,%rax), %rdx
	cmpl	$0, 8(%r10,%rax)
	je	.LBB14_11
# BB#12:                                #   in Loop: Header=BB14_10 Depth=2
	movl	%ebx, (%r9)
	leaq	4(%rdx), %rbx
	jmp	.LBB14_14
	.p2align	4, 0x90
.LBB14_13:                              # %.lr.ph80.i
                                        #   in Loop: Header=BB14_14 Depth=3
	leaq	(%rax,%rax,2), %rcx
	addl	8(%rdx,%rcx,4), %eax
	movl	%eax, 8(%rdx)
	movl	$0, 8(%rdx,%rcx,4)
.LBB14_14:                              # %.lr.ph80.i
                                        #   Parent Loop BB14_8 Depth=1
                                        #     Parent Loop BB14_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	8(%rdx), %eax
	leaq	(%rax,%rax,2), %rcx
	cmpl	$-1, (%rdx,%rcx,4)
	je	.LBB14_13
# BB#15:                                #   in Loop: Header=BB14_10 Depth=2
	movq	%rbx, %r9
	jmp	.LBB14_16
	.p2align	4, 0x90
.LBB14_11:                              # %..loopexit_crit_edge.i
                                        #   in Loop: Header=BB14_10 Depth=2
	addq	$4, %rdx
	movq	%rdx, %rbx
.LBB14_16:                              # %.loopexit.i
                                        #   in Loop: Header=BB14_10 Depth=2
	movl	(%rbx), %ebx
	testl	%ebx, %ebx
	jne	.LBB14_10
.LBB14_17:                              # %._crit_edge85.i
                                        #   in Loop: Header=BB14_8 Depth=1
	incq	%r8
	cmpq	$38, %r8
	jne	.LBB14_8
# BB#18:
	movl	$0, (%r9)
	movl	-4(%rsp), %r8d
	jmp	.LBB14_19
.LBB14_29:                              # %._crit_edge._crit_edge.i
                                        #   in Loop: Header=BB14_19 Depth=1
	movl	$-1, (%r10)
	movl	%edx, %eax
	movl	296(%rdi,%rax,4), %edx
	movl	%edx, 4(%r10)
	movzbl	128(%rdi,%rax), %edx
	movl	%edx, 8(%r10)
	subl	%ecx, %r10d
	movl	%r10d, 296(%rdi,%rax,4)
	incl	448(%rdi,%rax,4)
	.p2align	4, 0x90
.LBB14_19:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_25 Depth 2
	movl	%r8d, %ebx
	testl	%ebx, %ebx
	je	.LBB14_30
# BB#20:                                #   in Loop: Header=BB14_19 Depth=1
	movq	56(%rdi), %rcx
	movl	%ebx, %r10d
	movl	4(%rcx,%r10), %r8d
	movl	8(%rcx,%r10), %eax
	testl	%eax, %eax
	je	.LBB14_19
# BB#21:                                # %.preheader.i
                                        #   in Loop: Header=BB14_19 Depth=1
	addq	%rcx, %r10
	cmpl	$129, %eax
	jb	.LBB14_27
# BB#22:                                # %.lr.ph.i
                                        #   in Loop: Header=BB14_19 Depth=1
	leal	-129(%rax), %edx
	movl	%edx, %r14d
	shrl	$7, %r14d
	movq	%r14, %r15
	shlq	$9, %r15
	btl	$7, %edx
	movl	%eax, %r9d
	movq	%r10, %rdx
	jb	.LBB14_24
# BB#23:                                #   in Loop: Header=BB14_19 Depth=1
	movl	$-1, (%r10)
	movl	444(%rdi), %edx
	movl	%edx, 4(%r10)
	movzbl	165(%rdi), %edx
	movl	%edx, 8(%r10)
	movl	%ebx, 444(%rdi)
	incl	596(%rdi)
	leal	-128(%rax), %r9d
	leaq	1536(%r10), %rdx
.LBB14_24:                              # %.prol.loopexit
                                        #   in Loop: Header=BB14_19 Depth=1
	movl	%r14d, %r11d
	shll	$7, %r11d
	leaq	(%r15,%r15,2), %r15
	testl	%r14d, %r14d
	je	.LBB14_26
	.p2align	4, 0x90
.LBB14_25:                              #   Parent Loop BB14_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$-1, (%rdx)
	movl	444(%rdi), %ebp
	movl	%ebp, 4(%rdx)
	movzbl	165(%rdi), %ebp
	movl	%ebp, 8(%rdx)
	movl	%edx, %ebp
	subl	%ecx, %ebp
	movl	%ebp, 444(%rdi)
	incl	596(%rdi)
	leaq	1536(%rdx), %rbp
	movl	$-1, 1536(%rdx)
	movl	444(%rdi), %ebx
	movl	%ebx, 1540(%rdx)
	movzbl	165(%rdi), %ebx
	movl	%ebx, 1544(%rdx)
	subl	%ecx, %ebp
	movl	%ebp, 444(%rdi)
	incl	596(%rdi)
	addl	$-256, %r9d
	addq	$3072, %rdx             # imm = 0xC00
	cmpl	$128, %r9d
	ja	.LBB14_25
.LBB14_26:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB14_19 Depth=1
	leaq	1536(%r10,%r15), %r10
	addl	$-128, %eax
	subl	%r11d, %eax
.LBB14_27:                              # %._crit_edge.i
                                        #   in Loop: Header=BB14_19 Depth=1
	leal	-1(%rax), %ebx
	movzbl	166(%rdi,%rbx), %edx
	movzbl	128(%rdi,%rdx), %ebp
	cmpl	%eax, %ebp
	je	.LBB14_29
# BB#28:                                #   in Loop: Header=BB14_19 Depth=1
	leal	-1(%rdx), %edx
	movzbl	128(%rdi,%rdx), %r9d
	leaq	(%r9,%r9,2), %rbp
	leaq	(%r10,%rbp,4), %r11
	subl	%r9d, %ebx
	movl	$-1, (%r10,%rbp,4)
	movl	296(%rdi,%rbx,4), %eax
	movl	%eax, 4(%r10,%rbp,4)
	movzbl	128(%rdi,%rbx), %eax
	movl	%eax, 8(%r10,%rbp,4)
	subl	%ecx, %r11d
	movl	%r11d, 296(%rdi,%rbx,4)
	incl	448(%rdi,%rbx,4)
	jmp	.LBB14_29
.LBB14_30:                              # %GlueFreeBlocks.exit
	movl	%esi, %ecx
	movl	296(%rdi,%rcx,4), %edx
	testq	%rdx, %rdx
	je	.LBB14_1
# BB#31:
	movq	56(%rdi), %rsi
	leaq	(%rsi,%rdx), %rax
	movl	4(%rsi,%rdx), %edx
	movl	%edx, 296(%rdi,%rcx,4)
	decl	448(%rdi,%rcx,4)
	jmp	.LBB14_37
.LBB14_4:
	xorl	%eax, %eax
.LBB14_37:
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	AllocUnitsRare, .Lfunc_end14-AllocUnitsRare
	.cfi_endproc

	.p2align	4, 0x90
	.type	Refresh,@function
Refresh:                                # @Refresh
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 40
.Lcfi85:
	.cfi_offset %rbx, -40
.Lcfi86:
	.cfi_offset %r14, -32
.Lcfi87:
	.cfi_offset %r15, -24
.Lcfi88:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movzbl	(%rsi), %r8d
	movq	56(%rdi), %r9
	movl	4(%rsi), %r10d
	addq	%r9, %r10
	leal	2(%r8), %r15d
	shrl	%r15d
	decl	%edx
	movzbl	166(%rdi,%rdx), %r14d
	leal	-1(%r15), %eax
	movzbl	166(%rdi,%rax), %edx
	cmpb	%dl, %r14b
	je	.LBB15_12
# BB#1:
	movl	296(%rdi,%rdx,4), %ebx
	testq	%rbx, %rbx
	je	.LBB15_8
# BB#2:
	leaq	(%r9,%rbx), %r11
	movl	4(%r9,%rbx), %ebx
	movl	%ebx, 296(%rdi,%rdx,4)
	decl	448(%rdi,%rdx,4)
	testb	$1, %r15b
	jne	.LBB15_4
# BB#3:
	movq	%r11, %rdx
	movq	%r10, %rbx
	movl	%r15d, %eax
	cmpl	$1, %r15d
	jne	.LBB15_6
	jmp	.LBB15_7
.LBB15_8:
	movzbl	128(%rdi,%r14), %ebp
	movzbl	128(%rdi,%rdx), %eax
	subl	%eax, %ebp
	leaq	(%rax,%rax,2), %rax
	leaq	(%r10,%rax,4), %rax
	leal	-1(%rbp), %ebx
	movzbl	166(%rdi,%rbx), %r11d
	movzbl	128(%rdi,%r11), %edx
	cmpl	%ebp, %edx
	je	.LBB15_10
# BB#9:
	leal	-1(%r11), %r11d
	movzbl	128(%rdi,%r11), %r14d
	leaq	(%r14,%r14,2), %rbp
	leaq	(%rax,%rbp,4), %r15
	subl	%r14d, %ebx
	movl	$-1, (%rax,%rbp,4)
	movl	296(%rdi,%rbx,4), %edx
	movl	%edx, 4(%rax,%rbp,4)
	movzbl	128(%rdi,%rbx), %edx
	movl	%edx, 8(%rax,%rbp,4)
	subl	%r9d, %r15d
	movl	%r15d, 296(%rdi,%rbx,4)
	incl	448(%rdi,%rbx,4)
.LBB15_10:                              # %SplitBlock.exit
	movl	$-1, (%rax)
	movl	%r11d, %r14d
	movq	%r10, %r11
	jmp	.LBB15_11
.LBB15_4:
	movl	(%r10), %edx
	movl	%edx, (%r11)
	movl	4(%r10), %edx
	movl	%edx, 4(%r11)
	movl	8(%r10), %edx
	movl	%edx, 8(%r11)
	leaq	12(%r10), %rbx
	movq	%r11, %rdx
	addq	$12, %rdx
	cmpl	$1, %r15d
	je	.LBB15_7
	.p2align	4, 0x90
.LBB15_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ebp
	movl	%ebp, (%rdx)
	movl	4(%rbx), %ebp
	movl	%ebp, 4(%rdx)
	movl	8(%rbx), %ebp
	movl	%ebp, 8(%rdx)
	movl	12(%rbx), %ebp
	movl	%ebp, 12(%rdx)
	movl	16(%rbx), %ebp
	movl	%ebp, 16(%rdx)
	movl	20(%rbx), %ebp
	movl	%ebp, 20(%rdx)
	addq	$24, %rbx
	addq	$24, %rdx
	addl	$-2, %eax
	jne	.LBB15_6
.LBB15_7:
	movl	$-1, (%r10)
	movq	%r10, %rax
.LBB15_11:                              # %ShrinkUnits.exit.sink.split
	movl	296(%rdi,%r14,4), %edx
	movl	%edx, 4(%rax)
	movzbl	128(%rdi,%r14), %edx
	movl	%edx, 8(%rax)
	subl	%r9d, %eax
	movl	%eax, 296(%rdi,%r14,4)
	incl	448(%rdi,%r14,4)
	movq	%r11, %r10
.LBB15_12:                              # %ShrinkUnits.exit
	movl	%r10d, %eax
	subl	%r9d, %eax
	movl	%eax, 4(%rsi)
	movzbl	1(%rsi), %eax
	leal	16(,%rcx,4), %edx
	andl	%eax, %edx
	xorl	%eax, %eax
	cmpb	$63, (%r10)
	seta	%al
	leal	(%rdx,%rax,8), %r11d
	movzwl	2(%rsi), %edx
	movzbl	1(%r10), %eax
	subl	%eax, %edx
	addl	%ecx, %eax
	shrl	%cl, %eax
	movb	%al, 1(%r10)
	movzbl	%al, %ebx
	testb	$1, %r8b
	jne	.LBB15_14
# BB#13:
                                        # implicit-def: %EAX
                                        # implicit-def: %EDI
	movl	%r8d, %r9d
	cmpb	$1, %r8b
	jne	.LBB15_16
	jmp	.LBB15_18
.LBB15_14:
	movzbl	7(%r10), %eax
	subl	%eax, %edx
	addl	%ecx, %eax
	shrl	%cl, %eax
	movb	%al, 7(%r10)
	movzbl	%al, %eax
	addl	%ebx, %eax
	xorl	%edi, %edi
	cmpb	$63, 6(%r10)
	leaq	6(%r10), %r10
	seta	%dil
	shll	$3, %edi
	orl	%r11d, %edi
	leal	-1(%r8), %r9d
	movl	%eax, %ebx
	movl	%edi, %r11d
	cmpb	$1, %r8b
	je	.LBB15_18
.LBB15_16:                              # %ShrinkUnits.exit.new
	addq	$13, %r10
	movl	%ebx, %eax
	movl	%r11d, %edi
	.p2align	4, 0x90
.LBB15_17:                              # =>This Inner Loop Header: Depth=1
	movzbl	-6(%r10), %ebp
	subl	%ebp, %edx
	addl	%ecx, %ebp
	shrl	%cl, %ebp
	movb	%bpl, -6(%r10)
	movzbl	%bpl, %ebp
	addl	%eax, %ebp
	xorl	%ebx, %ebx
	cmpb	$63, -7(%r10)
	seta	%bl
	shll	$3, %ebx
	orl	%edi, %ebx
	movzbl	(%r10), %eax
	subl	%eax, %edx
	addl	%ecx, %eax
	shrl	%cl, %eax
	movb	%al, (%r10)
	movzbl	%al, %eax
	addl	%ebp, %eax
	xorl	%edi, %edi
	cmpb	$63, -1(%r10)
	seta	%dil
	shll	$3, %edi
	orl	%ebx, %edi
	addq	$12, %r10
	addl	$-2, %r9d
	jne	.LBB15_17
.LBB15_18:
	addl	%ecx, %edx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %edx
	addl	%eax, %edx
	movw	%dx, 2(%rsi)
	movb	%dil, 1(%rsi)
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	Refresh, .Lfunc_end15-Refresh
	.cfi_endproc

	.p2align	4, 0x90
	.type	CutOff,@function
CutOff:                                 # @CutOff
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi92:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 80
.Lcfi96:
	.cfi_offset %rbx, -56
.Lcfi97:
	.cfi_offset %r12, -48
.Lcfi98:
	.cfi_offset %r13, -40
.Lcfi99:
	.cfi_offset %r14, -32
.Lcfi100:
	.cfi_offset %r15, -24
.Lcfi101:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r13
	movzbl	(%r14), %r15d
	testl	%r15d, %r15d
	je	.LBB16_1
# BB#10:
	movq	56(%r13), %rax
	movq	88(%r13), %rcx
	movl	4(%r14), %r9d
	leaq	(%rax,%r9), %r10
	leal	2(%r15), %ebx
	shrl	%ebx
	leaq	16384(%rcx), %rdx
	cmpq	%r10, %rdx
	jae	.LBB16_12
# BB#11:
	movq	%r10, %rsi
	jmp	.LBB16_23
.LBB16_1:
	movzwl	4(%r14), %eax
	movzwl	6(%r14), %esi
	shlq	$16, %rsi
	orq	%rax, %rsi
	addq	56(%r13), %rsi
	movq	88(%r13), %rax
	cmpq	%rax, %rsi
	jb	.LBB16_8
# BB#2:
	cmpl	%ebp, 36(%r13)
	jbe	.LBB16_3
# BB#4:
	leal	1(%rbp), %edx
	movq	%r13, %rdi
	callq	CutOff
	movl	%eax, %ecx
	shrl	$16, %ecx
	jmp	.LBB16_5
.LBB16_12:
	leal	-1(%rbx), %edi
	movzbl	166(%r13,%rdi), %r8d
	movl	296(%r13,%r8,4), %edx
	cmpl	%edx, %r9d
	jbe	.LBB16_14
# BB#13:
	movq	%r10, %rsi
	jmp	.LBB16_23
.LBB16_14:
	movq	%rbp, %r11
	leaq	(%rax,%rdx), %rsi
	movl	4(%rax,%rdx), %edx
	movl	%edx, 296(%r13,%r8,4)
	decl	448(%r13,%r8,4)
	testb	$1, %bl
	jne	.LBB16_16
# BB#15:
	movq	%rsi, %rdx
	movq	%r10, %rbp
	movl	%ebx, %edi
	jmp	.LBB16_17
.LBB16_3:
	xorl	%eax, %eax
	xorl	%ecx, %ecx
.LBB16_5:
	movw	%ax, 4(%r14)
	movw	%cx, 6(%r14)
	cmpl	$10, %ebp
	jb	.LBB16_46
# BB#6:
	movzwl	%ax, %eax
	shll	$16, %ecx
	orl	%eax, %ecx
	testl	%ecx, %ecx
	jne	.LBB16_46
# BB#7:                                 # %._crit_edge104
	movq	88(%r13), %rax
.LBB16_8:
	cmpq	%r14, %rax
	je	.LBB16_40
# BB#9:
	movl	$-1, (%r14)
	movl	296(%r13), %eax
	movl	%eax, 4(%r14)
	movzbl	128(%r13), %eax
	movl	%eax, 8(%r14)
	subl	56(%r13), %r14d
	jmp	.LBB16_39
.LBB16_46:
	subl	56(%r13), %r14d
	jmp	.LBB16_45
.LBB16_16:
	movl	(%r10), %edx
	movl	%edx, (%rsi)
	movl	4(%r10), %edx
	movl	%edx, 4(%rsi)
	movl	8(%r10), %edx
	movl	%edx, 8(%rsi)
	leaq	12(%r10), %rbp
	movq	%rsi, %rdx
	addq	$12, %rdx
.LBB16_17:                              # %.prol.loopexit
	movq	%rbx, %r12
	cmpl	$1, %ebx
	je	.LBB16_19
	.p2align	4, 0x90
.LBB16_18:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbp), %ebx
	movl	%ebx, (%rdx)
	movl	4(%rbp), %ebx
	movl	%ebx, 4(%rdx)
	movl	8(%rbp), %ebx
	movl	%ebx, 8(%rdx)
	movl	12(%rbp), %ebx
	movl	%ebx, 12(%rdx)
	movl	16(%rbp), %ebx
	movl	%ebx, 16(%rdx)
	movl	20(%rbp), %ebx
	movl	%ebx, 20(%rdx)
	addq	$24, %rbp
	addq	$24, %rdx
	addl	$-2, %edi
	jne	.LBB16_18
.LBB16_19:
	cmpq	%r10, %rcx
	je	.LBB16_21
# BB#20:
	movl	$-1, (%r10)
	movl	296(%r13,%r8,4), %edx
	movl	%edx, 4(%r10)
	movzbl	128(%r13,%r8), %edx
	movl	%edx, 8(%r10)
	movl	%r9d, 296(%r13,%r8,4)
	incl	448(%r13,%r8,4)
	jmp	.LBB16_22
.LBB16_21:
	movzbl	128(%r13,%r8), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%r10,%rcx,4), %rcx
	movq	%rcx, 88(%r13)
.LBB16_22:                              # %MoveUnitsUp.exit
	movq	%r11, %rbp
	movq	%r12, %rbx
.LBB16_23:                              # %MoveUnitsUp.exit
	subq	%rax, %rsi
	movl	%esi, 4(%r14)
	movl	%esi, %edi
	addq	%rax, %rdi
	leaq	(%r15,%r15,2), %rdx
	leaq	(%rdi,%rdx,2), %r12
	cmpq	%rdi, %r12
	jae	.LBB16_25
# BB#24:
	movq	%rax, %rcx
	movl	%r15d, %edx
	testl	%ebp, %ebp
	jne	.LBB16_35
	jmp	.LBB16_44
.LBB16_25:                              # %.lr.ph
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	leal	1(%rbp), %ebx
	jmp	.LBB16_26
	.p2align	4, 0x90
.LBB16_32:                              # %._crit_edge101
                                        #   in Loop: Header=BB16_26 Depth=1
	movq	88(%r13), %rcx
.LBB16_26:                              # =>This Inner Loop Header: Depth=1
	movzwl	2(%r12), %edx
	movzwl	4(%r12), %esi
	shlq	$16, %rsi
	orq	%rdx, %rsi
	addq	%rax, %rsi
	cmpq	%rcx, %rsi
	jae	.LBB16_28
# BB#27:                                #   in Loop: Header=BB16_26 Depth=1
	movslq	%r15d, %rax
	decl	%r15d
	movw	$0, 2(%r12)
	movw	$0, 4(%r12)
	movzwl	4(%r12), %ecx
	movw	%cx, 12(%rsp)
	movl	(%r12), %ecx
	movl	%ecx, 8(%rsp)
	leaq	(%rax,%rax,2), %rax
	movzwl	4(%rdi,%rax,2), %ecx
	movw	%cx, 4(%r12)
	movl	(%rdi,%rax,2), %ecx
	movl	%ecx, (%r12)
	movzwl	12(%rsp), %ecx
	movw	%cx, 4(%rdi,%rax,2)
	movl	8(%rsp), %ecx
	movl	%ecx, (%rdi,%rax,2)
	jmp	.LBB16_31
	.p2align	4, 0x90
.LBB16_28:                              #   in Loop: Header=BB16_26 Depth=1
	cmpl	%ebp, 36(%r13)
	jbe	.LBB16_30
# BB#29:                                #   in Loop: Header=BB16_26 Depth=1
	movq	%r13, %rdi
	movl	%ebx, %edx
	callq	CutOff
	movw	%ax, 2(%r12)
	shrl	$16, %eax
	movw	%ax, 4(%r12)
	jmp	.LBB16_31
	.p2align	4, 0x90
.LBB16_30:                              #   in Loop: Header=BB16_26 Depth=1
	movw	$0, 2(%r12)
	movw	$0, 4(%r12)
.LBB16_31:                              #   in Loop: Header=BB16_26 Depth=1
	addq	$-6, %r12
	movq	56(%r13), %rax
	movl	4(%r14), %esi
	leaq	(%rax,%rsi), %rdi
	cmpq	%rdi, %r12
	jae	.LBB16_32
# BB#33:                                # %._crit_edge.loopexit
	movb	(%r14), %dl
	movq	%rax, %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
	testl	%ebp, %ebp
	je	.LBB16_44
.LBB16_35:                              # %._crit_edge
	movzbl	%dl, %edx
	cmpl	%edx, %r15d
	je	.LBB16_44
# BB#36:
	movb	%r15b, (%r14)
	movl	%esi, %edx
	addq	%rdx, %rax
	testl	%r15d, %r15d
	js	.LBB16_37
# BB#41:
	je	.LBB16_42
# BB#43:
	movzwl	2(%r14), %eax
	shll	$4, %r15d
	xorl	%ecx, %ecx
	cmpl	%r15d, %eax
	setg	%cl
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	Refresh
	movq	56(%r13), %rcx
	jmp	.LBB16_44
.LBB16_37:
	decl	%ebx
	movzbl	166(%r13,%rbx), %edx
	movl	$-1, (%rax)
	movl	296(%r13,%rdx,4), %esi
	movl	%esi, 4(%rax)
	movzbl	128(%r13,%rdx), %esi
	movl	%esi, 8(%rax)
	subl	%ecx, %eax
	movl	%eax, 296(%r13,%rdx,4)
	incl	448(%r13,%rdx,4)
	cmpq	%r14, 88(%r13)
	je	.LBB16_40
# BB#38:
	movl	$-1, (%r14)
	movl	296(%r13), %eax
	movl	%eax, 4(%r14)
	movzbl	128(%r13), %eax
	movl	%eax, 8(%r14)
	subl	%ecx, %r14d
.LBB16_39:                              # %SpecialFreeUnit.exit
	movl	%r14d, 296(%r13)
	incl	448(%r13)
	xorl	%r14d, %r14d
	jmp	.LBB16_45
.LBB16_40:
	addq	$12, %r14
	movq	%r14, 88(%r13)
	xorl	%r14d, %r14d
	jmp	.LBB16_45
.LBB16_42:
	movb	1(%r14), %cl
	andb	$16, %cl
	cmpb	$63, (%rax)
	seta	%dl
	shlb	$3, %dl
	orb	%cl, %dl
	movb	%dl, 1(%r14)
	movzwl	4(%rax), %ecx
	movw	%cx, 6(%r14)
	movl	(%rax), %ecx
	movl	%ecx, 2(%r14)
	decl	%ebx
	movzbl	166(%r13,%rbx), %edx
	movl	$-1, (%rax)
	movl	296(%r13,%rdx,4), %ecx
	movl	%ecx, 4(%rax)
	movzbl	128(%r13,%rdx), %ecx
	movl	%ecx, 8(%rax)
	movq	56(%r13), %rcx
	subl	%ecx, %eax
	movl	%eax, 296(%r13,%rdx,4)
	incl	448(%r13,%rdx,4)
	movb	3(%r14), %al
	addb	$11, %al
	shrb	$3, %al
	movb	%al, 3(%r14)
.LBB16_44:
	subl	%ecx, %r14d
.LBB16_45:                              # %SpecialFreeUnit.exit
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	CutOff, .Lfunc_end16-CutOff
	.cfi_endproc

	.type	PPMD8_kExpEscape,@object # @PPMD8_kExpEscape
	.section	.rodata,"a",@progbits
	.globl	PPMD8_kExpEscape
	.p2align	4
PPMD8_kExpEscape:
	.ascii	"\031\016\t\007\005\005\004\004\004\003\003\003\002\002\002\002"
	.size	PPMD8_kExpEscape, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
