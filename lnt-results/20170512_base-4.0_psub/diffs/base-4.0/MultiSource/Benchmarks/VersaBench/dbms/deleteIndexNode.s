	.text
	.file	"deleteIndexNode.bc"
	.globl	deleteIndexNode
	.p2align	4, 0x90
	.type	deleteIndexNode,@function
deleteIndexNode:                        # @deleteIndexNode
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movq	40(%rdi), %rbx
	movq	(%r14), %rsi
	callq	deleteIndexEntry
	testq	%rbx, %rbx
	movq	%rbx, %rdi
	jne	.LBB0_1
.LBB0_2:                                # %._crit_edge
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.Lfunc_end0:
	.size	deleteIndexNode, .Lfunc_end0-deleteIndexNode
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
