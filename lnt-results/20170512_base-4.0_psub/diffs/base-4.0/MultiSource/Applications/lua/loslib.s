	.text
	.file	"loslib.bc"
	.globl	luaopen_os
	.p2align	4, 0x90
	.type	luaopen_os,@function
luaopen_os:                             # @luaopen_os
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$.L.str, %esi
	movl	$syslib, %edx
	callq	luaL_register
	movl	$1, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	luaopen_os, .Lfunc_end0-luaopen_os
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4696837146684686336     # double 1.0E+6
	.text
	.p2align	4, 0x90
	.type	os_clock,@function
os_clock:                               # @os_clock
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	clock
	cvtsi2sdq	%rax, %xmm0
	divsd	.LCPI1_0(%rip), %xmm0
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	os_clock, .Lfunc_end1-os_clock
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_date,@function
os_date:                                # @os_date
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi3:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi4:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi6:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 56
	subq	$8440, %rsp             # imm = 0x20F8
.Lcfi9:
	.cfi_def_cfa_offset 8496
.Lcfi10:
	.cfi_offset %rbx, -56
.Lcfi11:
	.cfi_offset %r12, -48
.Lcfi12:
	.cfi_offset %r13, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$1, %esi
	movl	$.L.str.12, %edx
	xorl	%ecx, %ecx
	callq	luaL_optlstring
	movq	%rax, %rbx
	movl	$2, %esi
	movq	%r14, %rdi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB2_1
# BB#2:
	movl	$2, %esi
	movq	%r14, %rdi
	callq	luaL_checknumber
	cvttsd2si	%xmm0, %rax
	jmp	.LBB2_3
.LBB2_1:
	xorl	%edi, %edi
	callq	time
.LBB2_3:
	movq	%rax, 8(%rsp)
	cmpb	$33, (%rbx)
	jne	.LBB2_5
# BB#4:
	leaq	8(%rsp), %rdi
	callq	gmtime
	movq	%rax, %r15
	incq	%rbx
	testq	%r15, %r15
	jne	.LBB2_8
	jmp	.LBB2_7
.LBB2_5:
	leaq	8(%rsp), %rdi
	callq	localtime
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB2_7
.LBB2_8:
	cmpb	$42, (%rbx)
	jne	.LBB2_13
# BB#9:
	cmpb	$116, 1(%rbx)
	jne	.LBB2_13
# BB#10:
	cmpb	$0, 2(%rbx)
	je	.LBB2_11
.LBB2_13:                               # %.thread
	movb	$37, 5(%rsp)
	movb	$0, 7(%rsp)
	leaq	224(%rsp), %r12
	movq	%r14, %rdi
	movq	%r12, %rsi
	callq	luaL_buffinit
	leaq	8440(%rsp), %rbp
	leaq	16(%rsp), %r14
	leaq	5(%rsp), %r13
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_21:                               #   in Loop: Header=BB2_14 Depth=1
	leaq	1(%rcx), %rdx
	movq	%rdx, 224(%rsp)
	movb	%al, (%rcx)
	incq	%rbx
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rbx), %eax
	cmpb	$37, %al
	je	.LBB2_18
# BB#15:                                #   in Loop: Header=BB2_14 Depth=1
	testb	%al, %al
	jne	.LBB2_19
	jmp	.LBB2_16
	.p2align	4, 0x90
.LBB2_18:                               #   in Loop: Header=BB2_14 Depth=1
	movzbl	1(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB2_19
# BB#22:                                #   in Loop: Header=BB2_14 Depth=1
	incq	%rbx
	movb	%cl, 6(%rsp)
	movl	$200, %esi
	movq	%r14, %rdi
	movq	%r13, %rdx
	movq	%r15, %rcx
	callq	strftime
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	luaL_addlstring
	incq	%rbx
	jmp	.LBB2_14
	.p2align	4, 0x90
.LBB2_19:                               #   in Loop: Header=BB2_14 Depth=1
	movq	224(%rsp), %rcx
	cmpq	%rbp, %rcx
	jb	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_14 Depth=1
	movq	%r12, %rdi
	callq	luaL_prepbuffer
	movzbl	(%rbx), %eax
	movq	224(%rsp), %rcx
	jmp	.LBB2_21
.LBB2_16:
	leaq	224(%rsp), %rdi
	callq	luaL_pushresult
	jmp	.LBB2_17
.LBB2_7:
	movq	%r14, %rdi
	callq	lua_pushnil
.LBB2_17:                               # %setboolfield.exit
	movl	$1, %eax
	addq	$8440, %rsp             # imm = 0x20F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_11:
	xorl	%esi, %esi
	movl	$9, %edx
	movq	%r14, %rdi
	callq	lua_createtable
	movslq	(%r15), %rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.14, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	4(%r15), %rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.15, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	8(%r15), %rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.16, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	12(%r15), %rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.17, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	16(%r15), %rsi
	incq	%rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.18, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	20(%r15), %rsi
	addq	$1900, %rsi             # imm = 0x76C
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.19, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	24(%r15), %rsi
	incq	%rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.20, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movslq	28(%r15), %rsi
	incq	%rsi
	movq	%r14, %rdi
	callq	lua_pushinteger
	movl	$-2, %esi
	movl	$.L.str.21, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	movl	32(%r15), %esi
	testl	%esi, %esi
	js	.LBB2_17
# BB#12:
	movq	%r14, %rdi
	callq	lua_pushboolean
	movl	$-2, %esi
	movl	$.L.str.22, %edx
	movq	%r14, %rdi
	callq	lua_setfield
	jmp	.LBB2_17
.Lfunc_end2:
	.size	os_date, .Lfunc_end2-os_date
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_difftime,@function
os_difftime:                            # @os_difftime
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	luaL_checknumber
	cvttsd2si	%xmm0, %r14
	movl	$2, %esi
	xorps	%xmm0, %xmm0
	movq	%rbx, %rdi
	callq	luaL_optnumber
	cvttsd2si	%xmm0, %rsi
	movq	%r14, %rdi
	callq	difftime
	movq	%rbx, %rdi
	callq	lua_pushnumber
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	os_difftime, .Lfunc_end3-os_difftime
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_execute,@function
os_execute:                             # @os_execute
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 16
.Lcfi22:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	luaL_optlstring
	movq	%rax, %rdi
	callq	system
	movslq	%eax, %rsi
	movq	%rbx, %rdi
	callq	lua_pushinteger
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end4:
	.size	os_execute, .Lfunc_end4-os_execute
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_exit,@function
os_exit:                                # @os_exit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_optinteger
	movl	%eax, %edi
	callq	exit
.Lfunc_end5:
	.size	os_exit, .Lfunc_end5-os_exit
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_getenv,@function
os_getenv:                              # @os_getenv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %rdi
	callq	getenv
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lua_pushstring
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	os_getenv, .Lfunc_end6-os_getenv
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_remove,@function
os_remove:                              # @os_remove
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r14, -32
.Lcfi33:
	.cfi_offset %r15, -24
.Lcfi34:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %r15d
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	remove
	movl	%eax, %ebp
	callq	__errno_location
	testl	%ebp, %ebp
	je	.LBB7_1
# BB#2:
	movslq	(%rax), %rbp
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	%ebp, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	lua_pushinteger
	movl	$3, %r15d
	jmp	.LBB7_3
.LBB7_1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushboolean
.LBB7_3:                                # %os_pushresult.exit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	os_remove, .Lfunc_end7-os_remove
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_rename,@function
os_rename:                              # @os_rename
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %r15d
	movl	$1, %esi
	xorl	%edx, %edx
	callq	luaL_checklstring
	movq	%rax, %r14
	movl	$2, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaL_checklstring
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	rename
	movl	%eax, %ebp
	callq	__errno_location
	testl	%ebp, %ebp
	je	.LBB8_1
# BB#2:
	movslq	(%rax), %rbp
	movq	%rbx, %rdi
	callq	lua_pushnil
	movl	%ebp, %edi
	callq	strerror
	movq	%rax, %rcx
	movl	$.L.str.23, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rdx
	callq	lua_pushfstring
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	lua_pushinteger
	movl	$3, %r15d
	jmp	.LBB8_3
.LBB8_1:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_pushboolean
.LBB8_3:                                # %os_pushresult.exit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	os_rename, .Lfunc_end8-os_rename
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_setlocale,@function
os_setlocale:                           # @os_setlocale
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi46:
	.cfi_def_cfa_offset 32
.Lcfi47:
	.cfi_offset %rbx, -24
.Lcfi48:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	luaL_optlstring
	movq	%rax, %r14
	movl	$2, %esi
	movl	$.L.str.24, %edx
	movl	$os_setlocale.catnames, %ecx
	movq	%rbx, %rdi
	callq	luaL_checkoption
	cltq
	movl	os_setlocale.cat(,%rax,4), %edi
	movq	%r14, %rsi
	callq	setlocale
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	lua_pushstring
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	os_setlocale, .Lfunc_end9-os_setlocale
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_time,@function
os_time:                                # @os_time
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi52:
	.cfi_def_cfa_offset 96
.Lcfi53:
	.cfi_offset %rbx, -32
.Lcfi54:
	.cfi_offset %r14, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	$1, %esi
	callq	lua_type
	testl	%eax, %eax
	jle	.LBB10_1
# BB#2:
	movl	$1, %esi
	movl	$5, %edx
	movq	%rbx, %rdi
	callq	luaL_checktype
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	$-1, %esi
	movl	$.L.str.14, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isnumber
	xorl	%r14d, %r14d
	testl	%eax, %eax
	movl	$0, %ebp
	je	.LBB10_4
# BB#3:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tointeger
	movq	%rax, %rbp
.LBB10_4:                               # %getfield.exit
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	%ebp, 8(%rsp)
	movl	$-1, %esi
	movl	$.L.str.15, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB10_6
# BB#5:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tointeger
	movq	%rax, %r14
.LBB10_6:                               # %getfield.exit14
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	%r14d, 12(%rsp)
	movl	$-1, %esi
	movl	$.L.str.16, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB10_7
# BB#8:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tointeger
	movq	%rax, %rbp
	jmp	.LBB10_9
.LBB10_1:
	xorl	%edi, %edi
	callq	time
	cmpq	$-1, %rax
	jne	.LBB10_23
	jmp	.LBB10_22
.LBB10_7:
	movl	$12, %ebp
.LBB10_9:                               # %getfield.exit16
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	%ebp, 16(%rsp)
	movl	$-1, %esi
	movl	$.L.str.17, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB10_11
# BB#10:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tointeger
	movq	%rax, %rbp
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	jmp	.LBB10_12
.LBB10_11:
	movl	$.L.str.29, %esi
	movl	$.L.str.17, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
	movl	%eax, %ebp
.LBB10_12:                              # %getfield.exit18
	movl	%ebp, 20(%rsp)
	movl	$-1, %esi
	movl	$.L.str.18, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB10_14
# BB#13:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tointeger
	movq	%rax, %rbp
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	jmp	.LBB10_15
.LBB10_14:
	movl	$.L.str.29, %esi
	movl	$.L.str.18, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
	movl	%eax, %ebp
.LBB10_15:                              # %getfield.exit21
	decl	%ebp
	movl	%ebp, 24(%rsp)
	movl	$-1, %esi
	movl	$.L.str.19, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_isnumber
	testl	%eax, %eax
	je	.LBB10_17
# BB#16:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_tointeger
	movq	%rax, %rbp
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	jmp	.LBB10_18
.LBB10_17:
	movl	$.L.str.29, %esi
	movl	$.L.str.19, %edx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
	movl	%eax, %ebp
.LBB10_18:                              # %getfield.exit24
	addl	$-1900, %ebp            # imm = 0xF894
	movl	%ebp, 28(%rsp)
	movl	$-1, %ebp
	movl	$-1, %esi
	movl	$.L.str.22, %edx
	movq	%rbx, %rdi
	callq	lua_getfield
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_type
	testl	%eax, %eax
	je	.LBB10_20
# BB#19:
	movl	$-1, %esi
	movq	%rbx, %rdi
	callq	lua_toboolean
	movl	%eax, %ebp
.LBB10_20:                              # %getboolfield.exit
	movl	$-2, %esi
	movq	%rbx, %rdi
	callq	lua_settop
	movl	%ebp, 40(%rsp)
	leaq	8(%rsp), %rdi
	callq	mktime
	cmpq	$-1, %rax
	je	.LBB10_22
.LBB10_23:
	cvtsi2sdq	%rax, %xmm0
	movq	%rbx, %rdi
	callq	lua_pushnumber
	jmp	.LBB10_24
.LBB10_22:
	movq	%rbx, %rdi
	callq	lua_pushnil
.LBB10_24:
	movl	$1, %eax
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	os_time, .Lfunc_end10-os_time
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_tmpname,@function
os_tmpname:                             # @os_tmpname
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 48
.Lcfi58:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movups	.L.str.30(%rip), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdi
	callq	mkstemp
	cmpl	$-1, %eax
	je	.LBB11_2
# BB#1:
	movl	%eax, %edi
	callq	close
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	lua_pushstring
	movl	$1, %eax
	jmp	.LBB11_3
.LBB11_2:
	movl	$.L.str.31, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	luaL_error
.LBB11_3:
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end11:
	.size	os_tmpname, .Lfunc_end11-os_tmpname
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"os"
	.size	.L.str, 3

	.type	syslib,@object          # @syslib
	.section	.rodata,"a",@progbits
	.p2align	4
syslib:
	.quad	.L.str.1
	.quad	os_clock
	.quad	.L.str.2
	.quad	os_date
	.quad	.L.str.3
	.quad	os_difftime
	.quad	.L.str.4
	.quad	os_execute
	.quad	.L.str.5
	.quad	os_exit
	.quad	.L.str.6
	.quad	os_getenv
	.quad	.L.str.7
	.quad	os_remove
	.quad	.L.str.8
	.quad	os_rename
	.quad	.L.str.9
	.quad	os_setlocale
	.quad	.L.str.10
	.quad	os_time
	.quad	.L.str.11
	.quad	os_tmpname
	.zero	16
	.size	syslib, 192

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"clock"
	.size	.L.str.1, 6

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"date"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"difftime"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"execute"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"exit"
	.size	.L.str.5, 5

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"getenv"
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"remove"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"rename"
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"setlocale"
	.size	.L.str.9, 10

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"time"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"tmpname"
	.size	.L.str.11, 8

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%c"
	.size	.L.str.12, 3

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"sec"
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"min"
	.size	.L.str.15, 4

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"hour"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"day"
	.size	.L.str.17, 4

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"month"
	.size	.L.str.18, 6

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"year"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"wday"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"yday"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"isdst"
	.size	.L.str.22, 6

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"%s: %s"
	.size	.L.str.23, 7

	.type	os_setlocale.cat,@object # @os_setlocale.cat
	.section	.rodata,"a",@progbits
	.p2align	4
os_setlocale.cat:
	.long	6                       # 0x6
	.long	3                       # 0x3
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	2                       # 0x2
	.size	os_setlocale.cat, 24

	.type	os_setlocale.catnames,@object # @os_setlocale.catnames
	.p2align	4
os_setlocale.catnames:
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.10
	.quad	0
	.size	os_setlocale.catnames, 56

	.type	.L.str.24,@object       # @.str.24
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.24:
	.asciz	"all"
	.size	.L.str.24, 4

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"collate"
	.size	.L.str.25, 8

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ctype"
	.size	.L.str.26, 6

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"monetary"
	.size	.L.str.27, 9

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"numeric"
	.size	.L.str.28, 8

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"field '%s' missing in date table"
	.size	.L.str.29, 33

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"/tmp/lua_XXXXXX"
	.size	.L.str.30, 16

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"unable to generate a unique filename"
	.size	.L.str.31, 37


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
