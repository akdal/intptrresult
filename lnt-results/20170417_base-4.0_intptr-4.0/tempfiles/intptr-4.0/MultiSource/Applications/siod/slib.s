	.text
	.file	"slib.bc"
	.globl	siod_version
	.p2align	4, 0x90
	.type	siod_version,@function
siod_version:                           # @siod_version
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end0:
	.size	siod_version, .Lfunc_end0-siod_version
	.cfi_endproc

	.globl	process_cla
	.p2align	4, 0x90
	.type	process_cla,@function
process_cla:                            # @process_cla
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movq	%rsi, %r14
	movl	%edi, %ebp
	movb	process_cla.siod_lib_set(%rip), %al
	testb	%al, %al
	jne	.LBB1_3
# BB#1:
	movl	$.L.str.3, %edi
	callq	getenv
	testq	%rax, %rax
	je	.LBB1_3
# BB#2:
	movq	%rax, siod_lib(%rip)
	movb	$1, process_cla.siod_lib_set(%rip)
.LBB1_3:                                # %.preheader
	cmpl	$2, %ebp
	jl	.LBB1_36
# BB#4:                                 # %.lr.ph
	testl	%ebx, %ebx
	movl	%ebp, %ebp
	je	.LBB1_10
# BB#5:                                 # %.lr.ph.split.preheader
	addq	$8, %r14
	decq	%rbp
	jmp	.LBB1_6
.LBB1_26:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, heap_size(%rip)
	movq	(%r14), %rdi
	addq	$2, %rdi
	movl	$58, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_35
# BB#27:                                #   in Loop: Header=BB1_6 Depth=1
	incq	%rax
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	%rax, nheaps(%rip)
	jmp	.LBB1_35
.LBB1_29:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	movq	%rbx, init_file(%rip)
	jmp	.LBB1_35
.LBB1_25:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	movq	%rbx, siod_lib(%rip)
	jmp	.LBB1_35
.LBB1_30:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, inums_dim(%rip)
	jmp	.LBB1_35
.LBB1_28:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, obarray_dim(%rip)
	jmp	.LBB1_35
.LBB1_32:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, stack_size(%rip)
	jmp	.LBB1_35
.LBB1_33:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, siod_verbose_level(%rip)
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$2, %rax
	jb	.LBB1_35
# BB#7:                                 #   in Loop: Header=BB1_6 Depth=1
	cmpb	$45, (%rbx)
	jne	.LBB1_34
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	movsbl	1(%rbx), %eax
	addl	$-103, %eax
	cmpl	$15, %eax
	ja	.LBB1_34
# BB#9:                                 #   in Loop: Header=BB1_6 Depth=1
	jmpq	*.LJTI1_0(,%rax,8)
.LBB1_31:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, gc_kind_copying(%rip)
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_34:                               #   in Loop: Header=BB1_6 Depth=1
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
.LBB1_35:                               #   in Loop: Header=BB1_6 Depth=1
	addq	$8, %r14
	decq	%rbp
	jne	.LBB1_6
	jmp	.LBB1_36
.LBB1_10:                               # %.lr.ph.split.us.preheader
	addq	$8, %r14
	decq	%rbp
	.p2align	4, 0x90
.LBB1_11:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	cmpq	$2, %rax
	jb	.LBB1_24
# BB#12:                                #   in Loop: Header=BB1_11 Depth=1
	cmpb	$45, (%rbx)
	jne	.LBB1_24
# BB#13:                                #   in Loop: Header=BB1_11 Depth=1
	movsbl	1(%rbx), %eax
	addl	$-103, %eax
	cmpl	$15, %eax
	ja	.LBB1_24
# BB#14:                                #   in Loop: Header=BB1_11 Depth=1
	jmpq	*.LJTI1_1(,%rax,8)
.LBB1_17:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, gc_kind_copying(%rip)
	jmp	.LBB1_24
.LBB1_21:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, heap_size(%rip)
	movq	(%r14), %rdi
	addq	$2, %rdi
	movl	$58, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB1_24
# BB#22:                                #   in Loop: Header=BB1_11 Depth=1
	incq	%rax
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rax, %rdi
	callq	strtol
	movq	%rax, nheaps(%rip)
	jmp	.LBB1_24
.LBB1_19:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	movq	%rbx, init_file(%rip)
	jmp	.LBB1_24
.LBB1_23:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	movq	%rbx, siod_lib(%rip)
	jmp	.LBB1_24
.LBB1_18:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, inums_dim(%rip)
	jmp	.LBB1_24
.LBB1_20:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, obarray_dim(%rip)
	jmp	.LBB1_24
.LBB1_16:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, stack_size(%rip)
	jmp	.LBB1_24
.LBB1_15:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$2, %rbx
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%rbx, %rdi
	callq	strtol
	movq	%rax, siod_verbose_level(%rip)
	.p2align	4, 0x90
.LBB1_24:                               #   in Loop: Header=BB1_11 Depth=1
	addq	$8, %r14
	decq	%rbp
	jne	.LBB1_11
.LBB1_36:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	process_cla, .Lfunc_end1-process_cla
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI1_0:
	.quad	.LBB1_31
	.quad	.LBB1_26
	.quad	.LBB1_29
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_25
	.quad	.LBB1_34
	.quad	.LBB1_30
	.quad	.LBB1_28
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_32
	.quad	.LBB1_34
	.quad	.LBB1_34
	.quad	.LBB1_33
.LJTI1_1:
	.quad	.LBB1_17
	.quad	.LBB1_21
	.quad	.LBB1_19
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_23
	.quad	.LBB1_24
	.quad	.LBB1_18
	.quad	.LBB1_20
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_16
	.quad	.LBB1_24
	.quad	.LBB1_24
	.quad	.LBB1_15

	.text
	.globl	print_welcome
	.p2align	4, 0x90
	.type	print_welcome,@function
print_welcome:                          # @print_welcome
	.cfi_startproc
# BB#0:
	cmpq	$2, siod_verbose_level(%rip)
	jl	.LBB2_1
# BB#2:
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 16
	movl	$.L.str.5, %edi
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr, %edi
	popq	%rax
	jmp	puts                    # TAILCALL
.LBB2_1:
	retq
.Lfunc_end2:
	.size	print_welcome, .Lfunc_end2-print_welcome
	.cfi_endproc

	.globl	print_hs_1
	.p2align	4, 0x90
	.type	print_hs_1,@function
print_hs_1:                             # @print_hs_1
	.cfi_startproc
# BB#0:
	cmpq	$2, siod_verbose_level(%rip)
	jl	.LBB3_1
# BB#2:
	movq	nheaps(%rip), %rsi
	movq	heap_size(%rip), %rdx
	leaq	(,%rdx,8), %rax
	leaq	(%rax,%rax,2), %rcx
	movq	inums_dim(%rip), %r8
	cmpq	$1, gc_kind_copying(%rip)
	movl	$.L.str.8, %eax
	movl	$.L.str.9, %r9d
	cmoveq	%rax, %r9
	movl	$.L.str.7, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.LBB3_1:
	retq
.Lfunc_end3:
	.size	print_hs_1, .Lfunc_end3-print_hs_1
	.cfi_endproc

	.globl	print_hs_2
	.p2align	4, 0x90
	.type	print_hs_2,@function
print_hs_2:                             # @print_hs_2
	.cfi_startproc
# BB#0:
	cmpq	$2, siod_verbose_level(%rip)
	jl	.LBB4_3
# BB#1:
	movq	heaps(%rip), %rax
	movq	(%rax), %rsi
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB4_2
# BB#4:
	movq	8(%rax), %rdx
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.LBB4_3:
	retq
.LBB4_2:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	print_hs_2, .Lfunc_end4-print_hs_2
	.cfi_endproc

	.globl	no_interrupt
	.p2align	4, 0x90
	.type	no_interrupt,@function
no_interrupt:                           # @no_interrupt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 16
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	nointerrupt(%rip), %rbx
	movq	%rdi, nointerrupt(%rip)
	testq	%rdi, %rdi
	jne	.LBB5_3
# BB#1:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB5_3
# BB#2:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB5_3:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end5:
	.size	no_interrupt, .Lfunc_end5-no_interrupt
	.cfi_endproc

	.globl	err_ctrl_c
	.p2align	4, 0x90
	.type	err_ctrl_c,@function
err_ctrl_c:                             # @err_ctrl_c
	.cfi_startproc
# BB#0:
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	jmp	err                     # TAILCALL
.Lfunc_end6:
	.size	err_ctrl_c, .Lfunc_end6-err_ctrl_c
	.cfi_endproc

	.globl	handle_sigfpe
	.p2align	4, 0x90
	.type	handle_sigfpe,@function
handle_sigfpe:                          # @handle_sigfpe
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 16
	movl	$8, %edi
	movl	$handle_sigfpe, %esi
	callq	signal
	movl	$.L.str.12, %edi
	xorl	%esi, %esi
	popq	%rax
	jmp	err                     # TAILCALL
.Lfunc_end7:
	.size	handle_sigfpe, .Lfunc_end7-handle_sigfpe
	.cfi_endproc

	.globl	err
	.p2align	4, 0x90
	.type	err,@function
err:                                    # @err
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 64
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	inside_err(%rip), %r12b
	movq	$1, nointerrupt(%rip)
	testq	%r15, %r15
	je	.LBB8_3
# BB#1:
	xorl	%ecx, %ecx
	testq	%r14, %r14
	je	.LBB8_8
.LBB8_2:                                # %.critedge..critedge.thread_crit_edge
	movw	2(%r14), %ax
	jmp	.LBB8_11
.LBB8_3:
	testq	%r14, %r14
	je	.LBB8_9
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$1, %eax
	jne	.LBB8_10
# BB#5:
	movq	8(%r14), %rax
	testq	%rax, %rax
	je	.LBB8_25
# BB#6:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	jne	.LBB8_25
# BB#7:                                 # %get_c_string.exit
	movq	16(%rax), %r15
	movq	%r14, %rcx
	movq	16(%r14), %r14
	testq	%r14, %r14
	jne	.LBB8_2
.LBB8_8:
	movq	%rcx, (%rsp)            # 8-byte Spill
	movb	$1, %bpl
	xorl	%r14d, %r14d
	jmp	.LBB8_18
.LBB8_9:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.LBB8_26
.LBB8_10:
	xorl	%r15d, %r15d
	xorl	%ecx, %ecx
.LBB8_11:                               # %.critedge.thread
	movzwl	%ax, %eax
	cmpl	$13, %eax
	movq	%rcx, (%rsp)            # 8-byte Spill
	je	.LBB8_14
# BB#12:                                # %.critedge.thread
	cmpl	$3, %eax
	jne	.LBB8_17
# BB#13:
	leaq	8(%r14), %rax
	jmp	.LBB8_15
.LBB8_14:
	leaq	16(%r14), %rax
.LBB8_15:                               # %try_get_c_string.exit
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB8_17
# BB#16:
	xorl	%ebp, %ebp
	xorl	%esi, %esi
	movl	$80, %edx
	movq	%rbx, %rdi
	callq	memchr
	testq	%rax, %rax
	cmoveq	%rax, %rbx
	jmp	.LBB8_19
.LBB8_17:
	xorl	%ebp, %ebp
.LBB8_18:                               # %try_get_c_string.exit.thread
	xorl	%ebx, %ebx
.LBB8_19:                               # %try_get_c_string.exit.thread
	testq	%r15, %r15
	setne	%r13b
	je	.LBB8_30
# BB#20:                                # %try_get_c_string.exit.thread
	movq	siod_verbose_level(%rip), %rax
	testq	%rax, %rax
	jle	.LBB8_30
# BB#21:
	testb	%bpl, %bpl
	je	.LBB8_23
# BB#22:
	movl	$.L.str.17, %edi
	jmp	.LBB8_28
.LBB8_23:
	testq	%rbx, %rbx
	je	.LBB8_27
# BB#24:
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	printf
	jmp	.LBB8_29
.LBB8_25:
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
.LBB8_26:                               # %try_get_c_string.exit.thread.thread
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	cmpq	$1, errjmp_ok(%rip)
	je	.LBB8_31
	jmp	.LBB8_40
.LBB8_27:
	movl	$.L.str.19, %edi
.LBB8_28:                               # %try_get_c_string.exit.thread.thread
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
.LBB8_29:                               # %try_get_c_string.exit.thread.thread
	movb	$1, %r13b
.LBB8_30:                               # %try_get_c_string.exit.thread.thread
	cmpq	$1, errjmp_ok(%rip)
	jne	.LBB8_40
.LBB8_31:
	movb	$1, inside_err(%rip)
	movq	sym_errobj(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB8_33
# BB#32:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB8_34
.LBB8_33:                               # %.critedge.i52
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB8_34:                               # %setvar.exit
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	envlookup
	addq	$16, %rbx
	testq	%rax, %rax
	leaq	8(%rax), %rax
	cmoveq	%rbx, %rax
	movq	%r14, (%rax)
	movq	catch_framep(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB8_39
# BB#35:                                # %.lr.ph
	movq	sym_errobj(%rip), %rax
	movq	sym_catchall(%rip), %rcx
	.p2align	4, 0x90
.LBB8_36:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdx
	cmpq	%rax, %rdx
	je	.LBB8_44
# BB#37:                                #   in Loop: Header=BB8_36 Depth=1
	cmpq	%rcx, %rdx
	je	.LBB8_44
# BB#38:                                #   in Loop: Header=BB8_36 Depth=1
	movq	216(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB8_36
.LBB8_39:                               # %._crit_edge
	movb	$0, inside_err(%rip)
	cmpb	$1, %r13b
	movl	$1, %esi
	adcl	$0, %esi
	movl	$errjmp, %edi
	callq	longjmp
.LBB8_40:
	cmpq	$0, siod_verbose_level(%rip)
	jle	.LBB8_42
# BB#41:
	movl	$.Lstr.1, %edi
	callq	puts
.LBB8_42:
	movq	fatal_exit_hook(%rip), %rax
	testq	%rax, %rax
	je	.LBB8_54
# BB#43:
	callq	*%rax
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_44:
	movl	%r12d, %ecx
	movl	$.L.str.20, %r12d
	testb	%r13b, %r13b
	cmovneq	%r15, %r12
	movq	(%rsp), %rbp            # 8-byte Reload
	testq	%rbp, %rbp
	setne	%al
	orb	%al, %cl
	testb	$1, %cl
	jne	.LBB8_53
# BB#45:
	movq	%r12, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%r12, %rsi
	callq	strcons
	movq	%rax, %r15
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB8_49
# BB#46:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB8_48
# BB#47:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB8_48:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB8_52
.LBB8_49:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB8_51
# BB#50:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB8_51:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB8_52:                               # %cons.exit
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r15, 8(%rbp)
	movq	%r14, 16(%rbp)
.LBB8_53:
	movq	%rbp, 8(%rbx)
	movq	$0, nointerrupt(%rip)
	movb	$0, inside_err(%rip)
	addq	$16, %rbx
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	longjmp
.LBB8_54:
	movl	$10, %edi
	callq	exit
.Lfunc_end8:
	.size	err, .Lfunc_end8-err
	.cfi_endproc

	.globl	handle_sigint
	.p2align	4, 0x90
	.type	handle_sigint,@function
handle_sigint:                          # @handle_sigint
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 16
	movl	$2, %edi
	movl	$handle_sigint, %esi
	callq	signal
	cmpq	$1, nointerrupt(%rip)
	jne	.LBB9_2
# BB#1:
	movq	$1, interrupt_differed(%rip)
	popq	%rax
	retq
.LBB9_2:
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	popq	%rax
	jmp	err                     # TAILCALL
.Lfunc_end9:
	.size	handle_sigint, .Lfunc_end9-handle_sigint
	.cfi_endproc

	.globl	get_eof_val
	.p2align	4, 0x90
	.type	get_eof_val,@function
get_eof_val:                            # @get_eof_val
	.cfi_startproc
# BB#0:
	movq	eof_val(%rip), %rax
	retq
.Lfunc_end10:
	.size	get_eof_val, .Lfunc_end10-get_eof_val
	.cfi_endproc

	.globl	repl_driver
	.p2align	4, 0x90
	.type	repl_driver,@function
repl_driver:                            # @repl_driver
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 80
.Lcfi29:
	.cfi_offset %rbx, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	leaq	32(%rsp), %rax
	movq	%rax, stack_start_ptr(%rip)
	subq	stack_size(%rip), %rax
	movq	%rax, stack_limit_ptr(%rip)
	movl	$errjmp, %edi
	callq	_setjmp
	movl	%eax, %ebp
	cmpl	$2, %ebp
	jne	.LBB11_4
# BB#1:
	testq	%rbx, %rbx
	je	.LBB11_3
# BB#2:
	movq	repl_driver.osigint(%rip), %rsi
	movl	$2, %edi
	callq	signal
.LBB11_3:
	movq	repl_driver.osigfpe(%rip), %rsi
	movl	$8, %edi
	callq	signal
	movl	$2, %eax
	jmp	.LBB11_16
.LBB11_4:
	testq	%rbx, %rbx
	je	.LBB11_6
# BB#5:
	movl	$2, %edi
	movl	$handle_sigint, %esi
	callq	signal
	movq	%rax, repl_driver.osigint(%rip)
.LBB11_6:
	movl	$8, %edi
	movl	$handle_sigfpe, %esi
	callq	signal
	movq	%rax, repl_driver.osigfpe(%rip)
	movq	$0, catch_framep(%rip)
	movq	$1, errjmp_ok(%rip)
	movq	$0, interrupt_differed(%rip)
	movq	$0, nointerrupt(%rip)
	testl	%ebp, %ebp
	jne	.LBB11_10
# BB#7:
	testq	%r15, %r15
	je	.LBB11_10
# BB#8:
	movq	init_file(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB11_10
# BB#9:
	xorl	%esi, %esi
	movl	$1, %edx
	callq	vload
.LBB11_10:
	testq	%r14, %r14
	je	.LBB11_11
# BB#12:
	movq	%r14, %rdi
	jmp	.LBB11_13
.LBB11_11:
	movq	repl_puts(%rip), %rax
	movq	%rax, (%rsp)
	movq	repl_read(%rip), %rax
	movq	%rax, 8(%rsp)
	movq	repl_eval(%rip), %rax
	movq	%rax, 16(%rsp)
	movq	repl_print(%rip), %rax
	movq	%rax, 24(%rsp)
	movq	%rsp, %rdi
.LBB11_13:
	callq	repl
	testq	%rbx, %rbx
	je	.LBB11_15
# BB#14:
	movq	repl_driver.osigint(%rip), %rsi
	movl	$2, %edi
	callq	signal
.LBB11_15:
	movq	repl_driver.osigfpe(%rip), %rsi
	movl	$8, %edi
	callq	signal
	xorl	%eax, %eax
.LBB11_16:
	movq	$0, stack_start_ptr(%rip)
	movq	$0, stack_limit_ptr(%rip)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	repl_driver, .Lfunc_end11-repl_driver
	.cfi_endproc

	.globl	vload
	.p2align	4, 0x90
	.type	vload,@function
vload:                                  # @vload
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$552, %rsp              # imm = 0x228
.Lcfi39:
	.cfi_def_cfa_offset 608
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$124, %esi
	callq	strchr
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB12_1
# BB#2:
	xorl	%esi, %esi
	movl	$10, %edx
	movq	%r15, %rdi
	callq	strtol
	movq	%rax, %rbx
	incq	%rbp
	movq	%rbp, %r15
	testq	%r12, %r12
	jne	.LBB12_4
	jmp	.LBB12_13
.LBB12_1:
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.LBB12_13
.LBB12_4:
	movq	nointerrupt(%rip), %r12
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.133, %esi
	movq	%r15, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB12_6
# BB#5:
	movq	%rax, %rdi
	callq	fclose
	jmp	.LBB12_10
.LBB12_6:
	cmpb	$47, (%r15)
	je	.LBB12_10
# BB#7:
	movq	siod_lib(%rip), %r13
	movq	%r13, %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rbp,%rax), %rax
	cmpq	$511, %rax              # imm = 0x1FF
	ja	.LBB12_10
# BB#8:
	leaq	32(%rsp), %rbp
	movq	%rbp, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	%rbp, %rdi
	callq	strlen
	movw	$47, 32(%rsp,%rax)
	movq	%rbp, %rdi
	movq	%r15, %rsi
	callq	strcat
	movl	$.L.str.133, %esi
	movq	%rbp, %rdi
	callq	fopen
	testq	%rax, %rax
	je	.LBB12_10
# BB#9:
	movq	%rax, %rdi
	callq	fclose
	movq	%rbp, %r15
.LBB12_10:
	movslq	%r12d, %rax
	movq	%rax, nointerrupt(%rip)
	testl	%eax, %eax
	jne	.LBB12_13
# BB#11:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_13
# BB#12:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB12_13:                              # %no_interrupt.exit
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB12_23
# BB#14:
	movq	stdout(%rip), %rcx
	movq	nointerrupt(%rip), %rbp
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.138, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbp, nointerrupt(%rip)
	testq	%rbp, %rbp
	jne	.LBB12_17
# BB#15:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_17
# BB#16:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	movq	nointerrupt(%rip), %rbp
.LBB12_17:                              # %put_st.exit106
	movq	stdout(%rip), %rsi
	movq	$1, nointerrupt(%rip)
	movq	%r15, %rdi
	callq	fputs
	movq	%rbp, nointerrupt(%rip)
	testq	%rbp, %rbp
	jne	.LBB12_20
# BB#18:                                # %put_st.exit106
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_20
# BB#19:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	movq	nointerrupt(%rip), %rbp
.LBB12_20:                              # %put_st.exit109
	movq	stdout(%rip), %rsi
	movq	$1, nointerrupt(%rip)
	movl	$10, %edi
	callq	fputc
	movq	%rbp, nointerrupt(%rip)
	testq	%rbp, %rbp
	jne	.LBB12_23
# BB#21:                                # %put_st.exit109
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_23
# BB#22:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB12_23:                              # %put_st.exit112
	movl	$.L.str.139, %eax
	movl	$.L.str.133, %edx
	testq	%rbx, %rbx
	cmovneq	%rax, %rdx
	movl	$fopen, %edi
	movq	%r15, %rsi
	callq	fopen_cg
	movq	%rax, %r13
	testq	%rbx, %rbx
	movq	8(%r13), %rbp
	jle	.LBB12_25
	.p2align	4, 0x90
.LBB12_24:                              # %.lr.ph148
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, %rdi
	callq	_IO_getc
	decq	%rbx
	jne	.LBB12_24
.LBB12_25:                              # %._crit_edge
	movb	$0, 32(%rsp)
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_26:                              # %.sink.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_34 Depth 2
                                        #       Child Loop BB12_35 Depth 3
                                        #       Child Loop BB12_38 Depth 3
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$35, %eax
	jne	.LBB12_29
	jmp	.LBB12_34
	.p2align	4, 0x90
.LBB12_27:                              # %.loopexit.loopexit200
                                        #   in Loop: Header=BB12_34 Depth=2
	movl	$-1, %eax
	cmpl	$35, %eax
	je	.LBB12_34
.LBB12_29:                              # %.loopexit
                                        #   in Loop: Header=BB12_26 Depth=1
	cmpl	$59, %eax
	jne	.LBB12_30
.LBB12_34:                              # %.outer.preheader
                                        #   Parent Loop BB12_26 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_35 Depth 3
                                        #       Child Loop BB12_38 Depth 3
	leaq	1(%rbx), %rax
	cmpq	$511, %rax              # imm = 0x1FF
	ja	.LBB12_38
	.p2align	4, 0x90
.LBB12_35:                              # %.outer.split.us
                                        #   Parent Loop BB12_26 Depth=1
                                        #     Parent Loop BB12_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB12_27
# BB#36:                                # %.outer.split.us
                                        #   in Loop: Header=BB12_35 Depth=3
	cmpl	$10, %eax
	je	.LBB12_26
# BB#37:                                # %.us-lcssa146.us
                                        #   in Loop: Header=BB12_35 Depth=3
	movb	%al, 32(%rsp,%rbx)
	movb	$0, 33(%rsp,%rbx)
	leaq	1(%rbx), %rax
	addq	$2, %rbx
	cmpq	$512, %rbx              # imm = 0x200
	movq	%rax, %rbx
	jb	.LBB12_35
	.p2align	4, 0x90
.LBB12_38:                              # %.outer.split
                                        #   Parent Loop BB12_26 Depth=1
                                        #     Parent Loop BB12_34 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rbp, %rdi
	callq	_IO_getc
	cmpl	$-1, %eax
	je	.LBB12_27
# BB#39:                                # %.outer.split
                                        #   in Loop: Header=BB12_38 Depth=3
	cmpl	$10, %eax
	jne	.LBB12_38
	jmp	.LBB12_26
.LBB12_30:                              # %.loopexit
	cmpl	$-1, %eax
	je	.LBB12_32
# BB#31:
	movl	%eax, %edi
	movq	%rbp, %rsi
	callq	ungetc
.LBB12_32:                              # %.loopexit127
	leaq	32(%rsp), %rdi
	movl	$.L.str.135, %esi
	callq	strstr
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB12_33
# BB#40:
	leaq	7(%r12), %rbp
	movb	7(%r12), %bl
	testb	%bl, %bl
	je	.LBB12_44
# BB#41:                                # %.lr.ph
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB12_42:                              # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rcx
	testb	$8, (%rax,%rcx,2)
	je	.LBB12_44
# BB#43:                                #   in Loop: Header=BB12_42 Depth=1
	movzbl	1(%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	jne	.LBB12_42
.LBB12_44:                              # %.critedge
	subq	%r12, %rbp
	leaq	32(%rsp), %r15
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memmove
	movb	$95, 38(%rsp)
	movb	$0, 32(%rsp,%rbp)
	movq	%r15, %rdi
	callq	strlen
	movb	$0, 36(%rsp,%rax)
	movl	$1835234094, 32(%rsp,%rax) # imm = 0x6D63732E
	movq	$-1, %rdi
	movq	%r15, %rsi
	callq	strcons
	movq	%rax, %rdi
	callq	require
	movb	$0, 32(%rsp,%rbp)
	movl	$1, %esi
	movq	%r15, %rdi
	callq	gen_intern
	movq	%rax, %rbx
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	leval
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	funcall1
	movq	%rax, %r15
	cmpq	$5, siod_verbose_level(%rip)
	jl	.LBB12_54
# BB#45:
	movq	stdout(%rip), %rcx
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.135, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB12_48
# BB#46:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_48
# BB#47:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB12_48:                              # %put_st.exit115
	movq	stdout(%rip), %rax
	testq	%rax, %rax
	jne	.LBB12_51
# BB#49:
	movl	$.L.str.134, %edi
	xorl	%esi, %esi
	callq	err
	movq	8, %rax
	testq	%rax, %rax
	jne	.LBB12_51
# BB#50:
	movl	$.L.str.151, %edi
	xorl	%esi, %esi
	callq	err
	movq	8, %rax
.LBB12_51:                              # %lprin1.exit
	movq	$0, 8(%rsp)
	movq	$fputs_fcn, 16(%rsp)
	movq	%rax, 24(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r15, %rdi
	callq	lprin1g
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movl	$10, %edi
	callq	fputc
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB12_54
# BB#52:                                # %lprin1.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_54
# BB#53:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	jmp	.LBB12_54
.LBB12_33:
	xorl	%r15d, %r15d
.LBB12_54:                              # %put_st.exit118.preheader
	xorl	%r12d, %r12d
	jmp	.LBB12_55
.LBB12_100:                             #   in Loop: Header=BB12_55 Depth=1
	movq	%rbx, %r12
	.p2align	4, 0x90
.LBB12_55:                              # %put_st.exit118.outer.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_57 Depth 2
	movq	%r12, (%rsp)            # 8-byte Spill
	testq	%r15, %r15
	jne	.LBB12_69
	jmp	.LBB12_57
	.p2align	4, 0x90
.LBB12_104:                             # %setcdr.exit
                                        #   in Loop: Header=BB12_55 Depth=1
	movq	%rbx, 16(%r12)
	movq	%rbx, %r12
	testq	%r15, %r15
	je	.LBB12_57
.LBB12_69:                              # %put_st.exit118.outer.split
                                        #   in Loop: Header=BB12_55 Depth=1
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	funcall1
	movq	%rax, %rbp
	movq	eof_val(%rip), %rax
	testq	%r14, %r14
	je	.LBB12_70
# BB#89:                                # %put_st.exit118
                                        #   in Loop: Header=BB12_55 Depth=1
	cmpq	%rax, %rbp
	je	.LBB12_74
# BB#90:                                #   in Loop: Header=BB12_55 Depth=1
	cmpq	$5, siod_verbose_level(%rip)
	jl	.LBB12_92
# BB#91:                                #   in Loop: Header=BB12_55 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	lprint
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB12_93
	jmp	.LBB12_96
	.p2align	4, 0x90
.LBB12_68:                              #   in Loop: Header=BB12_57 Depth=2
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	leval
.LBB12_57:                              # %put_st.exit118.us
                                        #   Parent Loop BB12_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%r13, %r13
	jne	.LBB12_59
# BB#58:                                # %put_st.exit118.us
                                        #   in Loop: Header=BB12_57 Depth=2
	movq	stdin(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB12_64
.LBB12_59:                              #   in Loop: Header=BB12_57 Depth=2
	testq	%r13, %r13
	je	.LBB12_61
# BB#60:                                #   in Loop: Header=BB12_57 Depth=2
	movzwl	2(%r13), %eax
	cmpl	$17, %eax
	je	.LBB12_62
.LBB12_61:                              # %.critedge.i.i.us
                                        #   in Loop: Header=BB12_57 Depth=2
	movl	$.L.str.134, %edi
	movq	%r13, %rsi
	callq	err
.LBB12_62:                              #   in Loop: Header=BB12_57 Depth=2
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	jne	.LBB12_64
# BB#63:                                #   in Loop: Header=BB12_57 Depth=2
	movl	$.L.str.151, %edi
	movq	%r13, %rsi
	callq	err
	movq	8(%r13), %rdi
	.p2align	4, 0x90
.LBB12_64:                              # %lread.exit.us
                                        #   in Loop: Header=BB12_57 Depth=2
	callq	lreadf
	movq	%rax, %rbp
	cmpq	eof_val(%rip), %rbp
	je	.LBB12_74
# BB#65:                                #   in Loop: Header=BB12_57 Depth=2
	cmpq	$5, siod_verbose_level(%rip)
	jl	.LBB12_67
# BB#66:                                #   in Loop: Header=BB12_57 Depth=2
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	lprint
.LBB12_67:                              #   in Loop: Header=BB12_57 Depth=2
	testq	%r14, %r14
	je	.LBB12_68
.LBB12_92:                              # %.us-lcssa135.us
                                        #   in Loop: Header=BB12_55 Depth=1
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB12_96
.LBB12_93:                              #   in Loop: Header=BB12_55 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB12_95
# BB#94:                                #   in Loop: Header=BB12_55 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB12_95:                              #   in Loop: Header=BB12_55 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB12_99
	.p2align	4, 0x90
.LBB12_96:                              #   in Loop: Header=BB12_55 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB12_98
# BB#97:                                #   in Loop: Header=BB12_55 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB12_98:                              #   in Loop: Header=BB12_55 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB12_99:                              # %cons.exit
                                        #   in Loop: Header=BB12_55 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%rbp, 8(%rbx)
	movq	$0, 16(%rbx)
	je	.LBB12_100
# BB#101:                               #   in Loop: Header=BB12_55 Depth=1
	testq	%r12, %r12
	je	.LBB12_103
# BB#102:                               #   in Loop: Header=BB12_55 Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	je	.LBB12_104
.LBB12_103:                             # %.critedge.i123
                                        #   in Loop: Header=BB12_55 Depth=1
	movl	$.L.str.30, %edi
	movq	%r12, %rsi
	callq	err
	jmp	.LBB12_104
.LBB12_70:                              # %put_st.exit118.us136.preheader
	cmpq	%rax, %rbp
	je	.LBB12_74
	.p2align	4, 0x90
.LBB12_71:                              # %.lr.ph195
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$5, siod_verbose_level(%rip)
	jl	.LBB12_73
# BB#72:                                #   in Loop: Header=BB12_71 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	lprint
.LBB12_73:                              # %put_st.exit118.us136
                                        #   in Loop: Header=BB12_71 Depth=1
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	leval
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	funcall1
	movq	%rax, %rbp
	cmpq	eof_val(%rip), %rbp
	jne	.LBB12_71
.LBB12_74:                              # %.us-lcssa.us
	testq	%r13, %r13
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	je	.LBB12_76
# BB#75:
	movzwl	2(%r13), %eax
	cmpl	$17, %eax
	je	.LBB12_77
.LBB12_76:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%r13, %rsi
	callq	err
.LBB12_77:
	movq	8(%r13), %rdi
	testq	%rdi, %rdi
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB12_79
# BB#78:
	callq	fclose
	movq	$0, 8(%r13)
.LBB12_79:
	movq	16(%r13), %rdi
	testq	%rdi, %rdi
	je	.LBB12_81
# BB#80:
	callq	free
	movq	$0, 16(%r13)
.LBB12_81:                              # %file_gc_free.exit.i
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB12_84
# BB#82:                                # %file_gc_free.exit.i
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_84
# BB#83:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB12_84:                              # %fclose_l.exit
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB12_88
# BB#85:
	movq	stdout(%rip), %rcx
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.140, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB12_88
# BB#86:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB12_88
# BB#87:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB12_88:                              # %put_st.exit
	movq	%rbp, %rax
	addq	$552, %rsp              # imm = 0x228
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	vload, .Lfunc_end12-vload
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI13_0:
	.quad	4633641066610819072     # double 60
	.text
	.globl	repl
	.p2align	4, 0x90
	.type	repl,@function
repl:                                   # @repl
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 96
.Lcfi52:
	.cfi_offset %rbx, -48
.Lcfi53:
	.cfi_offset %r12, -40
.Lcfi54:
	.cfi_offset %r13, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	16(%rsp), %r14
	movabsq	$-6148914691236517205, %r12 # imm = 0xAAAAAAAAAAAAAAAB
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB13_11
	jmp	.LBB13_2
	.p2align	4, 0x90
.LBB13_33:
	movq	%rax, %rdi
	callq	*%rcx
.LBB13_1:                               # %.backedge
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB13_11
.LBB13_2:
	cmpq	$0, gc_status_flag(%rip)
	jne	.LBB13_4
# BB#3:
	movq	heap(%rip), %rax
	cmpq	heap_end(%rip), %rax
	jb	.LBB13_11
.LBB13_4:
	movq	%r14, %rdi
	callq	times
	movq	16(%rsp), %rbx
	movq	24(%rsp), %r13
	callq	gc_stop_and_copy
	cmpq	$2, siod_verbose_level(%rip)
	jl	.LBB13_18
# BB#5:
	cvtsi2sdq	%rbx, %xmm0
	cvtsi2sdq	%r13, %xmm1
	addsd	%xmm0, %xmm1
	movsd	.LCPI13_0(%rip), %xmm0  # xmm0 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%rsp)          # 8-byte Spill
	movq	tkbuffer(%rip), %rbx
	movq	%r14, %rdi
	callq	times
	xorps	%xmm1, %xmm1
	cvtsi2sdq	16(%rsp), %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdq	24(%rsp), %xmm0
	addsd	%xmm1, %xmm0
	divsd	.LCPI13_0(%rip), %xmm0
	subsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	movq	old_heap_used(%rip), %rdx
	movq	heap(%rip), %rcx
	movq	heap_end(%rip), %r8
	subq	%rcx, %r8
	subq	heap_org(%rip), %rcx
	sarq	$3, %rcx
	imulq	%r12, %rcx
	sarq	$3, %r8
	imulq	%r12, %r8
	movl	$.L.str.15, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	sprintf
	movq	tkbuffer(%rip), %rdi
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB13_6
# BB#10:
	callq	*%rax
	cmpq	$2, siod_verbose_level(%rip)
	jge	.LBB13_12
	jmp	.LBB13_18
.LBB13_6:
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB13_9
# BB#7:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB13_9
# BB#8:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB13_9:                               # %put_st.exit.i
	movq	stdout(%rip), %rdi
	callq	fflush
	.p2align	4, 0x90
.LBB13_11:                              # %grepl_puts.exit
	cmpq	$2, siod_verbose_level(%rip)
	jl	.LBB13_18
.LBB13_12:
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB13_13
# BB#17:
	movl	$.L.str.16, %edi
	callq	*%rax
	jmp	.LBB13_18
.LBB13_13:
	movq	stdout(%rip), %rcx
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.16, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB13_16
# BB#14:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB13_16
# BB#15:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB13_16:                              # %put_st.exit.i23
	movq	stdout(%rip), %rdi
	callq	fflush
	.p2align	4, 0x90
.LBB13_18:                              # %grepl_puts.exit24
	movq	8(%r15), %rax
	testq	%rax, %rax
	je	.LBB13_19
# BB#23:
	callq	*%rax
	jmp	.LBB13_24
	.p2align	4, 0x90
.LBB13_19:
	movq	stdin(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB13_22
# BB#20:
	movl	$.L.str.134, %edi
	xorl	%esi, %esi
	callq	err
	movq	8, %rdi
	testq	%rdi, %rdi
	jne	.LBB13_22
# BB#21:
	movl	$.L.str.151, %edi
	xorl	%esi, %esi
	callq	err
	movq	8, %rdi
	.p2align	4, 0x90
.LBB13_22:                              # %lread.exit
	callq	lreadf
.LBB13_24:
	movq	%rax, %rbx
	cmpq	eof_val(%rip), %rbx
	je	.LBB13_34
# BB#25:                                # %myrealtime.exit
	movq	%r14, %rdi
	callq	times
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	gettimeofday
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB13_27
# BB#26:
	movq	$0, gc_cells_allocated(%rip)
	movq	$0, gc_time_taken(%rip)
.LBB13_27:
	movq	16(%r15), %rax
	testq	%rax, %rax
	je	.LBB13_28
# BB#29:
	movq	%rbx, %rdi
	callq	*%rax
	jmp	.LBB13_30
	.p2align	4, 0x90
.LBB13_28:
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	leval
.LBB13_30:
	movq	24(%r15), %rcx
	testq	%rcx, %rcx
	jne	.LBB13_33
# BB#31:
	cmpq	$2, siod_verbose_level(%rip)
	jl	.LBB13_1
# BB#32:
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	lprint
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB13_2
	jmp	.LBB13_11
.LBB13_34:
	xorl	%eax, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	repl, .Lfunc_end13-repl
	.cfi_endproc

	.globl	repl_c_string
	.p2align	4, 0x90
	.type	repl_c_string,@function
repl_c_string:                          # @repl_c_string
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 48
	movq	$repl_c_string_read, 16(%rsp)
	movq	$0, 24(%rsp)
	cmpq	$1, %rcx
	jle	.LBB14_2
# BB#1:
	movl	$repl_c_string_print, %eax
	movl	$ignore_puts, %r8d
	movq	%rdi, %r9
	jmp	.LBB14_3
.LBB14_2:
	testq	%rcx, %rcx
	movl	$noprompt_puts, %eax
	movl	$ignore_puts, %r8d
	cmovneq	%rax, %r8
	movl	$not_ignore_print, %ecx
	movl	$ignore_print, %eax
	cmovneq	%rcx, %rax
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
.LBB14_3:
	movq	%r8, 8(%rsp)
	movq	%rax, 32(%rsp)
	movq	%rcx, repl_c_string_print_len(%rip)
	movq	%r9, repl_c_string_out(%rip)
	movq	%rdi, repl_c_string_arg(%rip)
	movb	$0, repl_c_string_flag(%rip)
	leaq	8(%rsp), %rax
	movq	%rsi, %rdi
	movq	%rdx, %rsi
	movq	%rax, %rdx
	callq	repl_driver
	movb	repl_c_string_flag(%rip), %cl
	xorl	%edx, %edx
	testb	%cl, %cl
	sete	%dl
	addq	%rdx, %rdx
	testq	%rax, %rax
	cmoveq	%rdx, %rax
	addq	$40, %rsp
	retq
.Lfunc_end14:
	.size	repl_c_string, .Lfunc_end14-repl_c_string
	.cfi_endproc

	.p2align	4, 0x90
	.type	repl_c_string_read,@function
repl_c_string_read:                     # @repl_c_string_read
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 16
.Lcfi59:
	.cfi_offset %rbx, -16
	movq	repl_c_string_arg(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB15_4
# BB#1:
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcons
	movq	$0, repl_c_string_arg(%rip)
	movq	repl_c_string_out(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB15_3
# BB#2:
	movb	$0, (%rcx)
.LBB15_3:
	movq	%rax, %rdi
	popq	%rbx
	jmp	read_from_string        # TAILCALL
.LBB15_4:
	movq	eof_val(%rip), %rax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	repl_c_string_read, .Lfunc_end15-repl_c_string_read
	.cfi_endproc

	.p2align	4, 0x90
	.type	ignore_puts,@function
ignore_puts:                            # @ignore_puts
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end16:
	.size	ignore_puts, .Lfunc_end16-ignore_puts
	.cfi_endproc

	.p2align	4, 0x90
	.type	repl_c_string_print,@function
repl_c_string_print:                    # @repl_c_string_print
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 48
	movq	$0, 16(%rsp)
	movq	$rcsp_puts, 24(%rsp)
	movq	repl_c_string_out(%rip), %rax
	movq	%rax, (%rsp)
	movq	repl_c_string_print_len(%rip), %rcx
	leaq	-1(%rax,%rcx), %rax
	movq	%rax, 8(%rsp)
	movq	%rsp, %rax
	movq	%rax, 32(%rsp)
	leaq	16(%rsp), %rsi
	callq	lprin1g
	movb	$1, repl_c_string_flag(%rip)
	addq	$40, %rsp
	retq
.Lfunc_end17:
	.size	repl_c_string_print, .Lfunc_end17-repl_c_string_print
	.cfi_endproc

	.p2align	4, 0x90
	.type	noprompt_puts,@function
noprompt_puts:                          # @noprompt_puts
	.cfi_startproc
# BB#0:
	cmpb	$62, (%rdi)
	jne	.LBB18_3
# BB#1:
	cmpb	$32, 1(%rdi)
	jne	.LBB18_3
# BB#2:
	cmpb	$0, 2(%rdi)
	je	.LBB18_5
.LBB18_3:                               # %.thread
	pushq	%rbx
.Lcfi61:
	.cfi_def_cfa_offset 16
.Lcfi62:
	.cfi_offset %rbx, -16
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	popq	%rbx
	jne	.LBB18_5
# BB#4:                                 # %.thread
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB18_5
# BB#6:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	jmp	err                     # TAILCALL
.LBB18_5:                               # %put_st.exit
	retq
.Lfunc_end18:
	.size	noprompt_puts, .Lfunc_end18-noprompt_puts
	.cfi_endproc

	.p2align	4, 0x90
	.type	not_ignore_print,@function
not_ignore_print:                       # @not_ignore_print
	.cfi_startproc
# BB#0:
	movb	$1, repl_c_string_flag(%rip)
	xorl	%esi, %esi
	jmp	lprint                  # TAILCALL
.Lfunc_end19:
	.size	not_ignore_print, .Lfunc_end19-not_ignore_print
	.cfi_endproc

	.p2align	4, 0x90
	.type	ignore_print,@function
ignore_print:                           # @ignore_print
	.cfi_startproc
# BB#0:
	movb	$1, repl_c_string_flag(%rip)
	retq
.Lfunc_end20:
	.size	ignore_print, .Lfunc_end20-ignore_print
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI21_0:
	.quad	4633641066610819072     # double 60
	.text
	.globl	myruntime
	.p2align	4, 0x90
	.type	myruntime,@function
myruntime:                              # @myruntime
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi63:
	.cfi_def_cfa_offset 48
	leaq	8(%rsp), %rdi
	callq	times
	cvtsi2sdq	8(%rsp), %xmm1
	cvtsi2sdq	16(%rsp), %xmm0
	addsd	%xmm1, %xmm0
	divsd	.LCPI21_0(%rip), %xmm0
	addq	$40, %rsp
	retq
.Lfunc_end21:
	.size	myruntime, .Lfunc_end21-myruntime
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI22_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	myrealtime
	.p2align	4, 0x90
	.type	myrealtime,@function
myrealtime:                             # @myrealtime
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi64:
	.cfi_def_cfa_offset 32
	leaq	8(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	xorpd	%xmm0, %xmm0
	testl	%eax, %eax
	jne	.LBB22_2
# BB#1:
	cvtsi2sdq	8(%rsp), %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdq	16(%rsp), %xmm0
	mulsd	.LCPI22_0(%rip), %xmm0
	addsd	%xmm1, %xmm0
.LBB22_2:
	addq	$24, %rsp
	retq
.Lfunc_end22:
	.size	myrealtime, .Lfunc_end22-myrealtime
	.cfi_endproc

	.globl	set_repl_hooks
	.p2align	4, 0x90
	.type	set_repl_hooks,@function
set_repl_hooks:                         # @set_repl_hooks
	.cfi_startproc
# BB#0:
	movq	%rdi, repl_puts(%rip)
	movq	%rsi, repl_read(%rip)
	movq	%rdx, repl_eval(%rip)
	movq	%rcx, repl_print(%rip)
	retq
.Lfunc_end23:
	.size	set_repl_hooks, .Lfunc_end23-set_repl_hooks
	.cfi_endproc

	.globl	gput_st
	.p2align	4, 0x90
	.type	gput_st,@function
gput_st:                                # @gput_st
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rcx
	movq	16(%rdi), %rax
	movq	%rsi, %rdi
	movq	%rax, %rsi
	jmpq	*%rcx                   # TAILCALL
.Lfunc_end24:
	.size	gput_st, .Lfunc_end24-gput_st
	.cfi_endproc

	.globl	fput_st
	.p2align	4, 0x90
	.type	fput_st,@function
fput_st:                                # @fput_st
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 16
.Lcfi66:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movq	%rsi, %rdi
	movq	%rax, %rsi
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB25_2
# BB#1:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB25_2
# BB#3:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	popq	%rbx
	jmp	err                     # TAILCALL
.LBB25_2:                               # %no_interrupt.exit
	popq	%rbx
	retq
.Lfunc_end25:
	.size	fput_st, .Lfunc_end25-fput_st
	.cfi_endproc

	.globl	fputs_fcn
	.p2align	4, 0x90
	.type	fputs_fcn,@function
fputs_fcn:                              # @fputs_fcn
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 16
.Lcfi68:
	.cfi_offset %rbx, -16
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB26_3
# BB#1:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB26_3
# BB#2:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB26_3:                               # %fput_st.exit
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end26:
	.size	fputs_fcn, .Lfunc_end26-fputs_fcn
	.cfi_endproc

	.globl	put_st
	.p2align	4, 0x90
	.type	put_st,@function
put_st:                                 # @put_st
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 16
.Lcfi70:
	.cfi_offset %rbx, -16
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB27_2
# BB#1:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB27_2
# BB#3:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	popq	%rbx
	jmp	err                     # TAILCALL
.LBB27_2:                               # %fput_st.exit
	popq	%rbx
	retq
.Lfunc_end27:
	.size	put_st, .Lfunc_end27-put_st
	.cfi_endproc

	.globl	grepl_puts
	.p2align	4, 0x90
	.type	grepl_puts,@function
grepl_puts:                             # @grepl_puts
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB28_1
# BB#5:
	jmpq	*%rsi                   # TAILCALL
.LBB28_1:
	pushq	%rbx
.Lcfi71:
	.cfi_def_cfa_offset 16
.Lcfi72:
	.cfi_offset %rbx, -16
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB28_4
# BB#2:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB28_4
# BB#3:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB28_4:                               # %put_st.exit
	movq	stdout(%rip), %rdi
	popq	%rbx
	jmp	fflush                  # TAILCALL
.Lfunc_end28:
	.size	grepl_puts, .Lfunc_end28-grepl_puts
	.cfi_endproc

	.globl	gc_stop_and_copy
	.p2align	4, 0x90
	.type	gc_stop_and_copy,@function
gc_stop_and_copy:                       # @gc_stop_and_copy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 64
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	nointerrupt(%rip), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	$1, nointerrupt(%rip)
	movq	$0, errjmp_ok(%rip)
	movq	heap_org(%rip), %r15
	movq	heap(%rip), %r12
	movq	%r12, %rax
	subq	%r15, %rax
	sarq	$3, %rax
	movabsq	$-6148914691236517205, %rcx # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rax, %rcx
	movq	%rcx, old_heap_used(%rip)
	movq	heaps(%rip), %rax
	movq	(%rax), %r14
	cmpq	%r14, %r15
	jne	.LBB29_2
# BB#1:
	movq	8(%rax), %r14
.LBB29_2:                               # %get_newspace.exit
	movq	%r14, heap(%rip)
	movq	%r14, heap_org(%rip)
	movq	heap_size(%rip), %rax
	leaq	(%rax,%rax,2), %rax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, heap_end(%rip)
	movq	protected_registers(%rip), %r13
	testq	%r13, %r13
	jne	.LBB29_4
	jmp	.LBB29_8
	.p2align	4, 0x90
.LBB29_7:                               # %._crit_edge.i
                                        #   in Loop: Header=BB29_4 Depth=1
	movq	16(%r13), %r13
	testq	%r13, %r13
	je	.LBB29_8
.LBB29_4:                               # %.lr.ph22.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_6 Depth 2
	movq	8(%r13), %rbx
	testq	%rbx, %rbx
	jle	.LBB29_7
# BB#5:                                 # %.lr.ph.i12.preheader
                                        #   in Loop: Header=BB29_4 Depth=1
	movq	(%r13), %rbp
	.p2align	4, 0x90
.LBB29_6:                               # %.lr.ph.i12
                                        #   Parent Loop BB29_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdi
	callq	gc_relocate
	movq	%rax, (%rbp)
	addq	$8, %rbp
	decq	%rbx
	jne	.LBB29_6
	jmp	.LBB29_7
.LBB29_8:                               # %scan_registers.exit
	movq	%r14, %rdi
	callq	scan_newspace
	cmpq	%r12, %r15
	jae	.LBB29_21
# BB#9:                                 # %.lr.ph.i.preheader
	movl	$3674110, %r13d         # imm = 0x380FFE
	jmp	.LBB29_10
.LBB29_13:                              #   in Loop: Header=BB29_10 Depth=1
	movq	user_types(%rip), %r14
	testq	%r14, %r14
	jne	.LBB29_17
# BB#14:                                #   in Loop: Header=BB29_10 Depth=1
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB29_16
# BB#15:                                #   in Loop: Header=BB29_10 Depth=1
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB29_16:                              # %must_malloc.exit.i.i
                                        #   in Loop: Header=BB29_10 Depth=1
	movq	%r14, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%r14, %rdi
	callq	memset
.LBB29_17:                              #   in Loop: Header=BB29_10 Depth=1
	movzwl	%bp, %eax
	cmpl	$99, %eax
	ja	.LBB29_24
# BB#18:                                # %get_user_type_hooks.exit.i
                                        #   in Loop: Header=BB29_10 Depth=1
	movslq	%ebp, %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	24(%r14,%rax), %rax
	testq	%rax, %rax
	je	.LBB29_20
# BB#19:                                #   in Loop: Header=BB29_10 Depth=1
	movq	%r15, %rdi
	callq	*%rax
	jmp	.LBB29_20
	.p2align	4, 0x90
.LBB29_10:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	cmpw	$0, (%r15)
	jne	.LBB29_20
# BB#11:                                #   in Loop: Header=BB29_10 Depth=1
	movswl	2(%r15), %ebp
	cmpl	$21, %ebp
	ja	.LBB29_13
# BB#12:                                #   in Loop: Header=BB29_10 Depth=1
	btl	%ebp, %r13d
	jae	.LBB29_13
.LBB29_20:                              #   in Loop: Header=BB29_10 Depth=1
	addq	$24, %r15
	cmpq	%r12, %r15
	jb	.LBB29_10
.LBB29_21:                              # %free_oldspace.exit
	movq	$1, errjmp_ok(%rip)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, nointerrupt(%rip)
	testq	%rax, %rax
	jne	.LBB29_23
# BB#22:                                # %free_oldspace.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB29_23
# BB#25:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	err                     # TAILCALL
.LBB29_23:                              # %no_interrupt.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB29_24:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end29:
	.size	gc_stop_and_copy, .Lfunc_end29-gc_stop_and_copy
	.cfi_endproc

	.globl	lread
	.p2align	4, 0x90
	.type	lread,@function
lread:                                  # @lread
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 16
.Lcfi87:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB30_2
# BB#1:
	movq	stdin(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB30_7
.LBB30_2:
	testq	%rbx, %rbx
	je	.LBB30_4
# BB#3:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB30_5
.LBB30_4:                               # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB30_5:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB30_7
# BB#6:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %rdi
.LBB30_7:                               # %get_c_file.exit
	popq	%rbx
	jmp	lreadf                  # TAILCALL
.Lfunc_end30:
	.size	lread, .Lfunc_end30-lread
	.cfi_endproc

	.globl	leval
	.p2align	4, 0x90
	.type	leval,@function
leval:                                  # @leval
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi88:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi91:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi92:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi94:
	.cfi_def_cfa_offset 80
.Lcfi95:
	.cfi_offset %rbx, -56
.Lcfi96:
	.cfi_offset %r12, -48
.Lcfi97:
	.cfi_offset %r13, -40
.Lcfi98:
	.cfi_offset %r14, -32
.Lcfi99:
	.cfi_offset %r15, -24
.Lcfi100:
	.cfi_offset %rbp, -16
	movq	%rdi, (%rsp)
	movq	%rsi, 8(%rsp)
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB31_2
# BB#1:
	movl	$.L.str.23, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_2:                               # %.preheader
	movq	%rsp, %r15
	leaq	8(%rsp), %r12
	jmp	.LBB31_3
.LBB31_372:                             # %cons.exit161
                                        #   in Loop: Header=BB31_3 Depth=1
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r13, 8(%rbx)
	movq	%rbp, 16(%rbx)
	movq	%rbx, (%rsp)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	leval
	movq	%rax, (%rsp)
	.p2align	4, 0x90
.LBB31_3:                               # %.backedge
                                        # =>This Inner Loop Header: Depth=1
	movq	sym_eval_history_ptr(%rip), %rax
	movq	16(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB31_6
# BB#4:                                 #   in Loop: Header=BB31_3 Depth=1
	movzwl	2(%rcx), %edx
	cmpl	$1, %edx
	jne	.LBB31_6
# BB#5:                                 #   in Loop: Header=BB31_3 Depth=1
	movq	(%rsp), %rdx
	movq	%rdx, 8(%rcx)
	movq	16(%rcx), %rcx
	movq	%rcx, 16(%rax)
.LBB31_6:                               # %.thread
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB31_383
# BB#7:                                 #   in Loop: Header=BB31_3 Depth=1
	movswl	2(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB31_8
# BB#13:                                #   in Loop: Header=BB31_3 Depth=1
	movq	8(%rdi), %r13
	testq	%r13, %r13
	je	.LBB31_14
# BB#15:                                #   in Loop: Header=BB31_3 Depth=1
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	cmpl	$1, %ecx
	je	.LBB31_21
# BB#16:                                #   in Loop: Header=BB31_3 Depth=1
	cmpl	$3, %ecx
	jne	.LBB31_24
# BB#17:                                #   in Loop: Header=BB31_3 Depth=1
	movq	8(%rsp), %rsi
	movq	%r13, %rdi
	callq	envlookup
	testq	%rax, %rax
	je	.LBB31_19
# BB#18:                                #   in Loop: Header=BB31_3 Depth=1
	movq	8(%rax), %r13
	testq	%r13, %r13
	jne	.LBB31_23
	jmp	.LBB31_14
.LBB31_21:                              #   in Loop: Header=BB31_3 Depth=1
	movq	8(%rsp), %rsi
	movq	%r13, %rdi
	callq	leval
	movq	%rax, %r13
.LBB31_22:                              #   in Loop: Header=BB31_3 Depth=1
	testq	%r13, %r13
	je	.LBB31_14
.LBB31_23:                              # %..thread177_crit_edge
                                        #   in Loop: Header=BB31_3 Depth=1
	movzwl	2(%r13), %eax
.LBB31_24:                              # %.thread177
                                        #   in Loop: Header=BB31_3 Depth=1
	movswl	%ax, %ecx
	addl	$-3, %ecx
	cmpl	$18, %ecx
	ja	.LBB31_373
# BB#25:                                # %.thread177
                                        #   in Loop: Header=BB31_3 Depth=1
	jmpq	*.LJTI31_0(,%rcx,8)
.LBB31_344:                             #   in Loop: Header=BB31_3 Depth=1
	movq	sym_quote(%rip), %r14
	movq	(%rsp), %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_348
# BB#345:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB31_347
# BB#346:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_347:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_351
.LBB31_199:                             #   in Loop: Header=BB31_3 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	*16(%r13)
	jmp	.LBB31_200
.LBB31_201:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%r13), %rax
	testq	%rax, %rax
	je	.LBB31_343
# BB#202:                               #   in Loop: Header=BB31_3 Depth=1
	movswl	2(%rax), %ecx
	decl	%ecx
	cmpl	$19, %ecx
	ja	.LBB31_343
# BB#203:                               #   in Loop: Header=BB31_3 Depth=1
	jmpq	*.LJTI31_1(,%rcx,8)
.LBB31_204:                             #   in Loop: Header=BB31_3 Depth=1
	movq	%r12, %rbx
	movq	(%rsp), %rax
	movq	16(%rax), %rdi
	movq	8(%rsp), %rsi
	callq	leval_args
	movq	%rax, %r12
	movq	8(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%r13), %rax
	movq	8(%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB31_227
# BB#205:                               #   in Loop: Header=BB31_3 Depth=1
	movzwl	2(%rbp), %eax
	cmpl	$3, %eax
	jne	.LBB31_227
# BB#206:                               #   in Loop: Header=BB31_3 Depth=1
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_210
# BB#207:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %r15
	cmpq	heap_end(%rip), %r15
	jb	.LBB31_209
# BB#208:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_209:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%r15), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_213
.LBB31_373:                             #   in Loop: Header=BB31_3 Depth=1
	movswq	%ax, %rbp
	jmp	.LBB31_374
.LBB31_19:                              #   in Loop: Header=BB31_3 Depth=1
	movq	(%rsp), %rax
	movq	8(%rax), %rsi
	movq	16(%rsi), %r13
	cmpq	unbound_marker(%rip), %r13
	jne	.LBB31_22
# BB#20:                                #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.49, %edi
	callq	err
	testq	%r13, %r13
	jne	.LBB31_23
	.p2align	4, 0x90
.LBB31_14:                              #   in Loop: Header=BB31_3 Depth=1
	xorl	%r13d, %r13d
	xorl	%ebp, %ebp
.LBB31_374:                             # %.thread179
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	user_types(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_378
# BB#375:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB31_377
# BB#376:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_377:                             # %must_malloc.exit
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
.LBB31_378:                             #   in Loop: Header=BB31_3 Depth=1
	cmpq	$99, %rbp
	ja	.LBB31_385
# BB#379:                               # %get_user_type_hooks.exit
                                        #   in Loop: Header=BB31_3 Depth=1
	leaq	(%rbp,%rbp,4), %rax
	shlq	$4, %rax
	movq	40(%rbx,%rax), %rax
	testq	%rax, %rax
	je	.LBB31_381
# BB#380:                               #   in Loop: Header=BB31_3 Depth=1
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	*%rax
.LBB31_200:                             #   in Loop: Header=BB31_3 Depth=1
	testq	%rax, %rax
	jne	.LBB31_3
	jmp	.LBB31_383
.LBB31_227:                             # %.critedge.i
                                        #   in Loop: Header=BB31_3 Depth=1
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_231
# BB#228:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	jb	.LBB31_230
# BB#229:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_230:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_234
.LBB31_348:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB31_350
# BB#349:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB31_350:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_351:                             # %cons.exit155
                                        #   in Loop: Header=BB31_3 Depth=1
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%rbx, 8(%rbp)
	movq	$0, 16(%rbp)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_355
# BB#352:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB31_354
# BB#353:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_354:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_358
.LBB31_355:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_357
# BB#356:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB31_357:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_358:                             # %cons.exit157
                                        #   in Loop: Header=BB31_3 Depth=1
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r14, 8(%rbx)
	movq	%rbp, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_362
# BB#359:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB31_361
# BB#360:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_361:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_365
.LBB31_362:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB31_364
# BB#363:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB31_364:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_365:                             # %cons.exit159
                                        #   in Loop: Header=BB31_3 Depth=1
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%rbx, 8(%rbp)
	movq	$0, 16(%rbp)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_369
# BB#366:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB31_368
# BB#367:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_368:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_372
.LBB31_369:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_371
# BB#370:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB31_371:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
	jmp	.LBB31_372
.LBB31_231:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	jne	.LBB31_233
# BB#232:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %r14
.LBB31_233:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_234:                             # %extend_env.exit
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	%rbp, %r15
	movq	%r12, %rbp
	movq	%rbx, %r12
.LBB31_235:                             # %extend_env.exit
                                        #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movw	$0, (%r14)
	movw	$1, 2(%r14)
	movq	%r15, 8(%r14)
	movq	%rbp, 16(%r14)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_239
# BB#236:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	movq	%rsp, %r15
	jb	.LBB31_238
# BB#237:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_238:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_242
.LBB31_239:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	movq	%rsp, %r15
	jne	.LBB31_241
# BB#240:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB31_241:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_242:                             # %cons.exit172
                                        #   in Loop: Header=BB31_3 Depth=1
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r14, 8(%rbp)
	movq	%rbx, 16(%rbp)
	movq	%rbp, 8(%rsp)
	movq	16(%r13), %rax
	movq	16(%rax), %rax
	movq	%rax, (%rsp)
	jmp	.LBB31_3
.LBB31_210:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %r15
	testq	%r15, %r15
	jne	.LBB31_212
# BB#211:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %r15
.LBB31_212:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%r15), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_213:                             # %cons.exit164
                                        #   in Loop: Header=BB31_3 Depth=1
	movw	$0, (%r15)
	movw	$1, 2(%r15)
	movq	%rbp, 8(%r15)
	movq	$0, 16(%r15)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_217
# BB#214:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB31_216
# BB#215:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_216:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_220
.LBB31_217:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB31_219
# BB#218:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB31_219:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_220:                             # %cons.exit166
                                        #   in Loop: Header=BB31_3 Depth=1
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r12, 8(%rbp)
	movq	$0, 16(%rbp)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_224
# BB#221:                               #   in Loop: Header=BB31_3 Depth=1
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	movq	%rbx, %r12
	jb	.LBB31_223
# BB#222:                               #   in Loop: Header=BB31_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_223:                             #   in Loop: Header=BB31_3 Depth=1
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_235
.LBB31_224:                             #   in Loop: Header=BB31_3 Depth=1
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	movq	%rbx, %r12
	jne	.LBB31_226
# BB#225:                               #   in Loop: Header=BB31_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %r14
.LBB31_226:                             #   in Loop: Header=BB31_3 Depth=1
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
	jmp	.LBB31_235
.LBB31_8:
	cmpl	$3, %eax
	jne	.LBB31_383
# BB#9:
	movq	8(%rsp), %rsi
	callq	envlookup
	testq	%rax, %rax
	je	.LBB31_11
# BB#10:
	movq	8(%rax), %r14
	jmp	.LBB31_384
.LBB31_381:
	movl	$.L.str.89, %edi
.LBB31_382:                             # %.thread173
	movq	%r13, %rsi
	callq	err
.LBB31_383:                             # %.thread173
	movq	(%rsp), %r14
.LBB31_384:                             # %.thread196
	movq	%r14, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB31_11:
	movq	(%rsp), %rsi
	movq	16(%rsi), %r14
	cmpq	unbound_marker(%rip), %r14
	jne	.LBB31_384
# BB#12:
	movl	$.L.str.49, %edi
	callq	err
	jmp	.LBB31_384
.LBB31_343:                             # %.thread251
	movl	$.L.str.88, %edi
	jmp	.LBB31_382
.LBB31_26:
	callq	*16(%r13)
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_27:
	movq	16(%r13), %rbx
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB31_32
# BB#28:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_32
# BB#29:
	cmpl	$1, %eax
	jne	.LBB31_31
# BB#30:
	movq	8(%rsi), %rdi
	jmp	.LBB31_33
.LBB31_35:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_40
# BB#36:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_40
# BB#37:
	cmpl	$1, %eax
	jne	.LBB31_39
# BB#38:
	movq	8(%rsi), %rdi
	jmp	.LBB31_41
.LBB31_73:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_78
# BB#74:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_78
# BB#75:
	cmpl	$1, %eax
	jne	.LBB31_77
# BB#76:
	movq	8(%rsi), %rdi
	jmp	.LBB31_79
.LBB31_197:
	movq	16(%r13), %rbx
	movq	(%rsp), %rax
	movq	16(%rax), %rdi
	movq	8(%rsp), %rsi
	callq	leval_args
	jmp	.LBB31_34
.LBB31_198:
	movq	(%rsp), %rax
	movq	16(%rax), %rdi
	movq	8(%rsp), %rsi
	callq	*16(%r13)
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_99:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_104
# BB#100:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_104
# BB#101:
	cmpl	$1, %eax
	jne	.LBB31_103
# BB#102:
	movq	8(%rsi), %rdi
	jmp	.LBB31_105
.LBB31_139:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_144
# BB#140:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_144
# BB#141:
	cmpl	$1, %eax
	jne	.LBB31_143
# BB#142:
	movq	8(%rsi), %rdi
	jmp	.LBB31_145
.LBB31_50:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_55
# BB#51:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_55
# BB#52:
	cmpl	$1, %eax
	jne	.LBB31_54
# BB#53:
	movq	8(%rsi), %rdi
	jmp	.LBB31_56
.LBB31_243:
	movq	8(%r13), %rdi
	callq	*16(%rax)
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_244:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_249
# BB#245:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_249
# BB#246:
	cmpl	$1, %eax
	jne	.LBB31_248
# BB#247:
	movq	8(%rsi), %rdi
	jmp	.LBB31_250
.LBB31_251:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_256
# BB#252:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_256
# BB#253:
	cmpl	$1, %eax
	jne	.LBB31_255
# BB#254:
	movq	8(%rsi), %rdi
	jmp	.LBB31_257
.LBB31_335:
	movq	16(%rax), %r15
	movq	8(%r13), %rbp
	movq	(%rsp), %rax
	movq	16(%rax), %rdi
	movq	8(%rsp), %rsi
	callq	leval_args
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB31_339
# BB#336:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB31_338
# BB#337:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB31_338:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB31_342
.LBB31_267:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_272
# BB#268:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_272
# BB#269:
	cmpl	$1, %eax
	jne	.LBB31_271
# BB#270:
	movq	8(%rsi), %rdi
	jmp	.LBB31_273
.LBB31_294:
	movq	(%rsp), %rax
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	testq	%rsi, %rsi
	je	.LBB31_299
# BB#295:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_299
# BB#296:
	cmpl	$1, %eax
	jne	.LBB31_298
# BB#297:
	movq	8(%rsi), %rdi
	jmp	.LBB31_300
.LBB31_31:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_32:                              # %car.exit79
	xorl	%edi, %edi
.LBB31_33:                              # %car.exit79
	movq	8(%rsp), %rsi
	callq	leval
.LBB31_34:                              # %.thread196
	movq	%rax, %rdi
	callq	*%rbx
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_39:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_40:                              # %car.exit
	xorl	%edi, %edi
.LBB31_41:                              # %car.exit
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_42
# BB#43:
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	movq	16(%r13), %rbx
	testq	%rsi, %rsi
	je	.LBB31_48
# BB#44:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_48
# BB#45:
	cmpl	$1, %eax
	jne	.LBB31_47
# BB#46:
	movq	8(%rsi), %rdi
	jmp	.LBB31_49
.LBB31_42:                              # %.thread183
	movq	$0, (%rsp)
	movq	16(%r13), %rbx
	jmp	.LBB31_48
.LBB31_77:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_78:                              # %car.exit82
	xorl	%edi, %edi
.LBB31_79:                              # %car.exit82
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_80
# BB#81:
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	movq	16(%r13), %r15
	testq	%rsi, %rsi
	je	.LBB31_86
# BB#82:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_86
# BB#83:
	cmpl	$1, %eax
	jne	.LBB31_85
# BB#84:
	movq	8(%rsi), %rdi
	jmp	.LBB31_87
.LBB31_80:                              # %.thread198
	movq	$0, (%rsp)
	movq	16(%r13), %r15
	jmp	.LBB31_86
.LBB31_103:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_104:                             # %car.exit90
	xorl	%edi, %edi
.LBB31_105:                             # %car.exit90
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_106
# BB#107:
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	movq	16(%r13), %r12
	testq	%rsi, %rsi
	je	.LBB31_112
# BB#108:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_112
# BB#109:
	cmpl	$1, %eax
	jne	.LBB31_111
# BB#110:
	movq	8(%rsi), %rdi
	jmp	.LBB31_113
.LBB31_106:                             # %.thread208
	movq	$0, (%rsp)
	movq	16(%r13), %r12
	jmp	.LBB31_112
.LBB31_143:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_144:                             # %car.exit104
	xorl	%edi, %edi
.LBB31_145:                             # %car.exit104
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_146
# BB#147:
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	movq	16(%r13), %r13
	testq	%rsi, %rsi
	je	.LBB31_152
# BB#148:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_152
# BB#149:
	cmpl	$1, %eax
	jne	.LBB31_151
# BB#150:
	movq	8(%rsi), %rdi
	jmp	.LBB31_153
.LBB31_146:                             # %.thread225
	movq	$0, (%rsp)
	movq	16(%r13), %r13
	jmp	.LBB31_152
.LBB31_54:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_55:                              # %car.exit75
	xorl	%edi, %edi
.LBB31_56:                              # %car.exit75
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_57
# BB#58:
	movq	16(%rax), %rsi
	movq	%rsi, (%rsp)
	movq	16(%r13), %rbx
	addq	$16, %r13
	testq	%rsi, %rsi
	je	.LBB31_63
# BB#59:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_63
# BB#60:
	cmpl	$1, %eax
	jne	.LBB31_62
# BB#61:
	movq	8(%rsi), %rdi
	jmp	.LBB31_64
.LBB31_57:                              # %.thread189
	movq	$0, (%rsp)
	movq	16(%r13), %rbx
	addq	$16, %r13
	jmp	.LBB31_63
.LBB31_339:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_341
# BB#340:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB31_341:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB31_342:                             # %cons.exit
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%rbp, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbx, %rdi
	callq	*%r15
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_47:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_48:                              # %car.exit73
	xorl	%edi, %edi
.LBB31_49:                              # %car.exit73
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	*%rbx
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_85:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_86:                              # %car.exit84
	xorl	%edi, %edi
.LBB31_87:                              # %car.exit84
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %rbx
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_97
# BB#88:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_97
# BB#89:
	cmpl	$1, %eax
	jne	.LBB31_90
# BB#91:                                # %cdr.exit86
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_97
# BB#92:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_97
# BB#93:
	cmpl	$1, %eax
	jne	.LBB31_95
# BB#94:
	movq	8(%rsi), %rdi
	jmp	.LBB31_98
.LBB31_90:
	movl	$.L.str.28, %edi
.LBB31_96:                              # %car.exit88
	callq	err
.LBB31_97:                              # %car.exit88
	xorl	%edi, %edi
.LBB31_98:                              # %car.exit88
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	*%r15
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_111:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_112:                             # %car.exit92
	xorl	%edi, %edi
.LBB31_113:                             # %car.exit92
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r15
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_123
# BB#114:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_123
# BB#115:
	cmpl	$1, %eax
	jne	.LBB31_116
# BB#117:                               # %cdr.exit94
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_123
# BB#118:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_123
# BB#119:
	cmpl	$1, %eax
	jne	.LBB31_121
# BB#120:
	movq	8(%rsi), %rdi
	jmp	.LBB31_124
.LBB31_116:
	movl	$.L.str.28, %edi
.LBB31_122:                             # %car.exit96
	callq	err
.LBB31_123:                             # %car.exit96
	xorl	%edi, %edi
.LBB31_124:                             # %car.exit96
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %rbx
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_137
# BB#125:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_137
# BB#126:
	cmpl	$1, %eax
	jne	.LBB31_127
# BB#128:                               # %cdr.exit98
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_137
# BB#129:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_137
# BB#130:
	cmpl	$1, %eax
	jne	.LBB31_127
# BB#131:                               # %cdr.exit100
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_137
# BB#132:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_137
# BB#133:
	cmpl	$1, %eax
	jne	.LBB31_135
# BB#134:
	movq	8(%rsi), %rdi
	jmp	.LBB31_138
.LBB31_127:
	movl	$.L.str.28, %edi
.LBB31_136:                             # %car.exit102
	callq	err
.LBB31_137:                             # %car.exit102
	xorl	%edi, %edi
.LBB31_138:                             # %car.exit102
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rcx
	callq	*%r12
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_151:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_152:                             # %car.exit106
	xorl	%edi, %edi
.LBB31_153:                             # %car.exit106
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r15
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_163
# BB#154:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_163
# BB#155:
	cmpl	$1, %eax
	jne	.LBB31_156
# BB#157:                               # %cdr.exit108
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_163
# BB#158:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_163
# BB#159:
	cmpl	$1, %eax
	jne	.LBB31_161
# BB#160:
	movq	8(%rsi), %rdi
	jmp	.LBB31_164
.LBB31_156:
	movl	$.L.str.28, %edi
.LBB31_162:                             # %car.exit110
	callq	err
.LBB31_163:                             # %car.exit110
	xorl	%edi, %edi
.LBB31_164:                             # %car.exit110
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r12
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_177
# BB#165:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_177
# BB#166:
	cmpl	$1, %eax
	jne	.LBB31_167
# BB#168:                               # %cdr.exit112
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_177
# BB#169:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_177
# BB#170:
	cmpl	$1, %eax
	jne	.LBB31_167
# BB#171:                               # %cdr.exit114
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_177
# BB#172:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_177
# BB#173:
	cmpl	$1, %eax
	jne	.LBB31_175
# BB#174:
	movq	8(%rsi), %rdi
	jmp	.LBB31_178
.LBB31_167:
	movl	$.L.str.28, %edi
.LBB31_176:                             # %car.exit116
	callq	err
.LBB31_177:                             # %car.exit116
	xorl	%edi, %edi
.LBB31_178:                             # %car.exit116
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %rbx
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_194
# BB#179:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_194
# BB#180:
	cmpl	$1, %eax
	jne	.LBB31_181
# BB#182:                               # %cdr.exit118
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_194
# BB#183:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_194
# BB#184:
	cmpl	$1, %eax
	jne	.LBB31_181
# BB#185:                               # %cdr.exit120
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_194
# BB#186:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_194
# BB#187:
	cmpl	$1, %eax
	jne	.LBB31_181
# BB#188:                               # %cdr.exit122
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_194
# BB#189:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_194
# BB#190:
	cmpl	$1, %eax
	jne	.LBB31_192
# BB#191:
	movq	8(%rsi), %rdi
	jmp	.LBB31_195
.LBB31_181:
	movl	$.L.str.28, %edi
.LBB31_193:                             # %car.exit124
	callq	err
.LBB31_194:                             # %car.exit124
	xorl	%edi, %edi
.LBB31_195:                             # %car.exit124
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r14, %rdi
	movq	%r15, %rsi
	jmp	.LBB31_196
.LBB31_62:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_63:                              # %car.exit77
	xorl	%edi, %edi
.LBB31_64:                              # %car.exit77
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	*%rbx
	movq	%rax, %r14
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_68
# BB#65:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_68
# BB#66:
	cmpl	$1, %eax
	jne	.LBB31_67
# BB#69:                                # %cdr.exit.preheader
	movq	16(%rsi), %rax
	jmp	.LBB31_70
.LBB31_72:                              # %cdr.exit
                                        #   in Loop: Header=BB31_70 Depth=1
	movq	(%r13), %rbx
	movq	8(%rax), %rdi
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	*%rbx
	movq	%rax, %r14
	movq	(%rsp), %rax
	movq	16(%rax), %rax
.LBB31_70:                              # %cdr.exit.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rsp)
	testq	%rax, %rax
	je	.LBB31_384
# BB#71:                                # %.lr.ph
                                        #   in Loop: Header=BB31_70 Depth=1
	movzwl	2(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB31_72
	jmp	.LBB31_384
.LBB31_67:
	movl	$.L.str.28, %edi
	callq	err
.LBB31_68:                              # %cdr.exit.preheader.thread
	movq	$0, (%rsp)
	jmp	.LBB31_384
.LBB31_248:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_249:                             # %car.exit126
	xorl	%edi, %edi
.LBB31_250:                             # %car.exit126
	movq	8(%rsp), %rsi
	callq	leval
	movq	8(%r13), %rdi
	movq	16(%r13), %rcx
	movq	%rax, %rsi
	callq	*16(%rcx)
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_255:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_256:                             # %car.exit128
	xorl	%edi, %edi
.LBB31_257:                             # %car.exit128
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_258
# BB#259:
	movq	16(%rax), %rsi
	jmp	.LBB31_260
.LBB31_258:
	xorl	%esi, %esi
.LBB31_260:
	movq	%rsi, (%rsp)
	movq	8(%r13), %r15
	movq	16(%r13), %rax
	movq	16(%rax), %rbx
	testq	%rsi, %rsi
	je	.LBB31_265
# BB#261:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_265
# BB#262:
	cmpl	$1, %eax
	jne	.LBB31_264
# BB#263:
	movq	8(%rsi), %rdi
	jmp	.LBB31_266
.LBB31_271:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_272:                             # %car.exit132
	xorl	%edi, %edi
.LBB31_273:                             # %car.exit132
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_274
# BB#275:
	movq	16(%rax), %rsi
	jmp	.LBB31_276
.LBB31_274:
	xorl	%esi, %esi
.LBB31_276:
	movq	%rsi, (%rsp)
	movq	8(%r13), %r15
	movq	16(%r13), %rax
	movq	16(%rax), %rbp
	testq	%rsi, %rsi
	je	.LBB31_281
# BB#277:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_281
# BB#278:
	cmpl	$1, %eax
	jne	.LBB31_280
# BB#279:
	movq	8(%rsi), %rdi
	jmp	.LBB31_282
.LBB31_298:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_299:                             # %car.exit140
	xorl	%edi, %edi
.LBB31_300:                             # %car.exit140
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r14
	movq	(%rsp), %rax
	testq	%rax, %rax
	je	.LBB31_301
# BB#302:
	movq	16(%rax), %rsi
	jmp	.LBB31_303
.LBB31_301:
	xorl	%esi, %esi
.LBB31_303:
	movq	%rsi, (%rsp)
	movq	8(%r13), %r15
	movq	16(%r13), %rax
	movq	16(%rax), %r13
	testq	%rsi, %rsi
	je	.LBB31_308
# BB#304:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_308
# BB#305:
	cmpl	$1, %eax
	jne	.LBB31_307
# BB#306:
	movq	8(%rsi), %rdi
	jmp	.LBB31_309
.LBB31_264:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_265:                             # %car.exit130
	xorl	%edi, %edi
.LBB31_266:                             # %car.exit130
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	*%rbx
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_280:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_281:                             # %car.exit134
	xorl	%edi, %edi
.LBB31_282:                             # %car.exit134
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %rbx
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_292
# BB#283:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_292
# BB#284:
	cmpl	$1, %eax
	jne	.LBB31_285
# BB#286:                               # %cdr.exit136
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_292
# BB#287:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_292
# BB#288:
	cmpl	$1, %eax
	jne	.LBB31_290
# BB#289:
	movq	8(%rsi), %rdi
	jmp	.LBB31_293
.LBB31_307:
	movl	$.L.str.27, %edi
	callq	err
.LBB31_308:                             # %car.exit142
	xorl	%edi, %edi
.LBB31_309:                             # %car.exit142
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %r12
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_319
# BB#310:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_319
# BB#311:
	cmpl	$1, %eax
	jne	.LBB31_312
# BB#313:                               # %cdr.exit144
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_319
# BB#314:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_319
# BB#315:
	cmpl	$1, %eax
	jne	.LBB31_317
# BB#316:
	movq	8(%rsi), %rdi
	jmp	.LBB31_320
.LBB31_285:
	movl	$.L.str.28, %edi
.LBB31_291:                             # %car.exit138
	callq	err
.LBB31_292:                             # %car.exit138
	xorl	%edi, %edi
.LBB31_293:                             # %car.exit138
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rcx
	callq	*%rbp
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_312:
	movl	$.L.str.28, %edi
.LBB31_318:                             # %car.exit146
	callq	err
.LBB31_319:                             # %car.exit146
	xorl	%edi, %edi
.LBB31_320:                             # %car.exit146
	movq	8(%rsp), %rsi
	callq	leval
	movq	%rax, %rbx
	movq	(%rsp), %rsi
	testq	%rsi, %rsi
	je	.LBB31_333
# BB#321:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_333
# BB#322:
	cmpl	$1, %eax
	jne	.LBB31_323
# BB#324:                               # %cdr.exit148
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_333
# BB#325:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_333
# BB#326:
	cmpl	$1, %eax
	jne	.LBB31_323
# BB#327:                               # %cdr.exit150
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB31_333
# BB#328:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB31_333
# BB#329:
	cmpl	$1, %eax
	jne	.LBB31_331
# BB#330:
	movq	8(%rsi), %rdi
	jmp	.LBB31_334
.LBB31_323:
	movl	$.L.str.28, %edi
.LBB31_332:                             # %car.exit152
	callq	err
.LBB31_333:                             # %car.exit152
	xorl	%edi, %edi
.LBB31_334:                             # %car.exit152
	movq	8(%rsp), %rsi
	callq	leval
	movq	%r15, %rdi
	movq	%r14, %rsi
.LBB31_196:                             # %.thread196
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movq	%rax, %r8
	callq	*%r13
	movq	%rax, %r14
	jmp	.LBB31_384
.LBB31_95:
	movl	$.L.str.27, %edi
	jmp	.LBB31_96
.LBB31_121:
	movl	$.L.str.27, %edi
	jmp	.LBB31_122
.LBB31_161:
	movl	$.L.str.27, %edi
	jmp	.LBB31_162
.LBB31_290:
	movl	$.L.str.27, %edi
	jmp	.LBB31_291
.LBB31_317:
	movl	$.L.str.27, %edi
	jmp	.LBB31_318
.LBB31_135:
	movl	$.L.str.27, %edi
	jmp	.LBB31_136
.LBB31_175:
	movl	$.L.str.27, %edi
	jmp	.LBB31_176
.LBB31_331:
	movl	$.L.str.27, %edi
	jmp	.LBB31_332
.LBB31_192:
	movl	$.L.str.27, %edi
	jmp	.LBB31_193
.LBB31_385:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end31:
	.size	leval, .Lfunc_end31-leval
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI31_0:
	.quad	.LBB31_344
	.quad	.LBB31_26
	.quad	.LBB31_27
	.quad	.LBB31_35
	.quad	.LBB31_73
	.quad	.LBB31_197
	.quad	.LBB31_198
	.quad	.LBB31_199
	.quad	.LBB31_201
	.quad	.LBB31_373
	.quad	.LBB31_373
	.quad	.LBB31_373
	.quad	.LBB31_373
	.quad	.LBB31_373
	.quad	.LBB31_373
	.quad	.LBB31_373
	.quad	.LBB31_99
	.quad	.LBB31_139
	.quad	.LBB31_50
.LJTI31_1:
	.quad	.LBB31_204
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_243
	.quad	.LBB31_244
	.quad	.LBB31_251
	.quad	.LBB31_335
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_343
	.quad	.LBB31_267
	.quad	.LBB31_294

	.text
	.globl	lprint
	.p2align	4, 0x90
	.type	lprint,@function
lprint:                                 # @lprint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi101:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi102:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi103:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi104:
	.cfi_def_cfa_offset 64
.Lcfi105:
	.cfi_offset %rbx, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r15, %r15
	jne	.LBB32_2
# BB#1:
	movq	stdout(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB32_7
.LBB32_2:
	testq	%r15, %r15
	je	.LBB32_4
# BB#3:
	movzwl	2(%r15), %eax
	cmpl	$17, %eax
	je	.LBB32_5
.LBB32_4:                               # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%r15, %rsi
	callq	err
.LBB32_5:
	movq	8(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB32_7
# BB#6:
	movl	$.L.str.151, %edi
	movq	%r15, %rsi
	callq	err
	movq	8(%r15), %rbx
.LBB32_7:                               # %get_c_file.exit
	movq	$0, 8(%rsp)
	movq	$fputs_fcn, 16(%rsp)
	movq	%rbx, 24(%rsp)
	leaq	8(%rsp), %rsi
	movq	%r14, %rdi
	callq	lprin1g
	movq	nointerrupt(%rip), %r14
	movq	$1, nointerrupt(%rip)
	movl	$10, %edi
	movq	%rbx, %rsi
	callq	fputc
	movq	%r14, nointerrupt(%rip)
	testq	%r14, %r14
	jne	.LBB32_10
# BB#8:                                 # %get_c_file.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB32_10
# BB#9:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB32_10:                              # %fput_st.exit
	xorl	%eax, %eax
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end32:
	.size	lprint, .Lfunc_end32-lprint
	.cfi_endproc

	.globl	set_fatal_exit_hook
	.p2align	4, 0x90
	.type	set_fatal_exit_hook,@function
set_fatal_exit_hook:                    # @set_fatal_exit_hook
	.cfi_startproc
# BB#0:
	movq	%rdi, fatal_exit_hook(%rip)
	retq
.Lfunc_end33:
	.size	set_fatal_exit_hook, .Lfunc_end33-set_fatal_exit_hook
	.cfi_endproc

	.globl	get_c_string
	.p2align	4, 0x90
	.type	get_c_string,@function
get_c_string:                           # @get_c_string
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB34_5
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB34_4
# BB#2:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	jne	.LBB34_5
# BB#3:
	movq	8(%rax), %rax
	retq
.LBB34_5:                               # %.critedge8
	pushq	%rax
.Lcfi108:
	.cfi_def_cfa_offset 16
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%eax, %eax
	addq	$8, %rsp
	retq
.LBB34_4:
	movq	16(%rax), %rax
	retq
.Lfunc_end34:
	.size	get_c_string, .Lfunc_end34-get_c_string
	.cfi_endproc

	.globl	try_get_c_string
	.p2align	4, 0x90
	.type	try_get_c_string,@function
try_get_c_string:                       # @try_get_c_string
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB35_1
# BB#2:
	movzwl	2(%rdi), %eax
	cmpl	$13, %eax
	je	.LBB35_6
# BB#3:
	movzwl	%ax, %eax
	cmpl	$3, %eax
	jne	.LBB35_4
# BB#5:
	movq	8(%rdi), %rax
	retq
.LBB35_1:
	xorl	%eax, %eax
	retq
.LBB35_6:
	movq	16(%rdi), %rax
	retq
.LBB35_4:
	xorl	%eax, %eax
	retq
.Lfunc_end35:
	.size	try_get_c_string, .Lfunc_end35-try_get_c_string
	.cfi_endproc

	.globl	setvar
	.p2align	4, 0x90
	.type	setvar,@function
setvar:                                 # @setvar
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -32
.Lcfi113:
	.cfi_offset %r14, -24
.Lcfi114:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB36_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB36_3
.LBB36_2:                               # %.critedge
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB36_3:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	envlookup
	addq	$16, %rbx
	testq	%rax, %rax
	leaq	8(%rax), %rax
	cmoveq	%rbx, %rax
	movq	%r14, (%rax)
	movq	%r14, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end36:
	.size	setvar, .Lfunc_end36-setvar
	.cfi_endproc

	.globl	cons
	.p2align	4, 0x90
	.type	cons,@function
cons:                                   # @cons
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -32
.Lcfi119:
	.cfi_offset %r14, -24
.Lcfi120:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB37_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB37_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB37_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB37_7
.LBB37_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB37_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB37_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB37_7:
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end37:
	.size	cons, .Lfunc_end37-cons
	.cfi_endproc

	.globl	errswitch
	.p2align	4, 0x90
	.type	errswitch,@function
errswitch:                              # @errswitch
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi121:
	.cfi_def_cfa_offset 16
	movl	$.L.str.22, %edi
	xorl	%esi, %esi
	callq	err
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end38:
	.size	errswitch, .Lfunc_end38-errswitch
	.cfi_endproc

	.globl	err_stack
	.p2align	4, 0x90
	.type	err_stack,@function
err_stack:                              # @err_stack
	.cfi_startproc
# BB#0:
	movl	$.L.str.23, %edi
	xorl	%esi, %esi
	jmp	err                     # TAILCALL
.Lfunc_end39:
	.size	err_stack, .Lfunc_end39-err_stack
	.cfi_endproc

	.globl	stack_limit
	.p2align	4, 0x90
	.type	stack_limit,@function
stack_limit:                            # @stack_limit
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.LBB40_2
# BB#1:
	callq	get_c_long
	movq	%rax, stack_size(%rip)
	movq	stack_start_ptr(%rip), %rcx
	subq	%rax, %rcx
	movq	%rcx, stack_limit_ptr(%rip)
.LBB40_2:
	testq	%rbx, %rbx
	je	.LBB40_3
# BB#6:
	movq	stack_size(%rip), %rcx
	cvtsi2sdq	%rcx, %xmm2
	movq	inums_dim(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB40_11
# BB#7:
	cvttsd2si	%xmm2, %rax
	cmpq	%rdx, %rax
	jge	.LBB40_11
# BB#8:
	testq	%rcx, %rcx
	js	.LBB40_11
# BB#9:
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB40_11
	jp	.LBB40_11
# BB#10:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB40_19
.LBB40_11:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB40_15
# BB#12:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB40_14
# BB#13:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB40_14:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB40_18
.LBB40_3:
	movq	tkbuffer(%rip), %rdi
	movq	stack_size(%rip), %rdx
	movq	stack_start_ptr(%rip), %rcx
	movq	stack_limit_ptr(%rip), %r8
	xorl	%ebx, %ebx
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	tkbuffer(%rip), %rdi
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %r14
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%r14, nointerrupt(%rip)
	testq	%r14, %r14
	jne	.LBB40_19
# BB#4:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB40_19
# BB#5:
	movq	$0, interrupt_differed(%rip)
	xorl	%ebx, %ebx
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	jmp	.LBB40_19
.LBB40_15:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB40_17
# BB#16:
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	gc_for_newcell
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB40_17:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB40_18:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB40_19:                              # %put_st.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end40:
	.size	stack_limit, .Lfunc_end40-stack_limit
	.cfi_endproc

	.globl	flocons
	.p2align	4, 0x90
	.type	flocons,@function
flocons:                                # @flocons
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi128:
	.cfi_def_cfa_offset 32
.Lcfi129:
	.cfi_offset %rbx, -16
	movq	inums_dim(%rip), %rax
	testq	%rax, %rax
	jle	.LBB41_5
# BB#1:
	xorps	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm0
	jb	.LBB41_5
# BB#2:
	cvttsd2si	%xmm0, %rcx
	cvtsi2sdq	%rcx, %xmm1
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm3, %xmm2
	jne	.LBB41_5
	jp	.LBB41_5
# BB#3:
	cmpq	%rax, %rcx
	jge	.LBB41_5
# BB#4:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB41_13
.LBB41_5:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB41_9
# BB#6:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB41_8
# BB#7:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB41_8:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB41_12
.LBB41_9:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB41_11
# BB#10:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB41_11:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB41_12:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm0, 8(%rbx)
.LBB41_13:
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end41:
	.size	flocons, .Lfunc_end41-flocons
	.cfi_endproc

	.globl	get_c_string_dim
	.p2align	4, 0x90
	.type	get_c_string_dim,@function
get_c_string_dim:                       # @get_c_string_dim
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi132:
	.cfi_def_cfa_offset 32
.Lcfi133:
	.cfi_offset %rbx, -24
.Lcfi134:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB42_7
# BB#1:
	movswl	2(%rax), %ecx
	addl	$-3, %ecx
	cmpl	$15, %ecx
	ja	.LBB42_7
# BB#2:
	jmpq	*.LJTI42_0(,%rcx,8)
.LBB42_4:
	movq	8(%rax), %rcx
	jmp	.LBB42_5
.LBB42_7:                               # %.thread
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%ebx, %ebx
	jmp	.LBB42_8
.LBB42_3:
	movq	8(%rax), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, (%r14)
	jmp	.LBB42_8
.LBB42_6:
	movq	8(%rax), %rcx
	shlq	$3, %rcx
.LBB42_5:
	movq	%rcx, (%r14)
	movq	16(%rax), %rbx
.LBB42_8:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end42:
	.size	get_c_string_dim, .Lfunc_end42-get_c_string_dim
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI42_0:
	.quad	.LBB42_3
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_4
	.quad	.LBB42_7
	.quad	.LBB42_6
	.quad	.LBB42_7
	.quad	.LBB42_7
	.quad	.LBB42_4

	.text
	.globl	lerr
	.p2align	4, 0x90
	.type	lerr,@function
lerr:                                   # @lerr
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi135:
	.cfi_def_cfa_offset 16
.Lcfi136:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB43_9
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB43_8
# BB#2:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	je	.LBB43_7
# BB#3:
	cmpl	$1, %ecx
	jne	.LBB43_9
# BB#4:
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB43_9
# BB#5:
	movzwl	2(%rcx), %ecx
	cmpl	$13, %ecx
	jne	.LBB43_9
# BB#6:
	xorl	%edi, %edi
	movq	%rax, %rsi
	jmp	.LBB43_11
.LBB43_9:                               # %.critedge8.i
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%edi, %edi
.LBB43_10:                              # %get_c_string.exit
	movq	%rbx, %rsi
.LBB43_11:
	callq	err
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB43_8:
	movq	16(%rax), %rdi
	jmp	.LBB43_10
.LBB43_7:
	movq	8(%rax), %rdi
	jmp	.LBB43_10
.Lfunc_end43:
	.size	lerr, .Lfunc_end43-lerr
	.cfi_endproc

	.globl	gc_fatal_error
	.p2align	4, 0x90
	.type	gc_fatal_error,@function
gc_fatal_error:                         # @gc_fatal_error
	.cfi_startproc
# BB#0:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	jmp	err                     # TAILCALL
.Lfunc_end44:
	.size	gc_fatal_error, .Lfunc_end44-gc_fatal_error
	.cfi_endproc

	.globl	newcell
	.p2align	4, 0x90
	.type	newcell,@function
newcell:                                # @newcell
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi139:
	.cfi_def_cfa_offset 32
.Lcfi140:
	.cfi_offset %rbx, -24
.Lcfi141:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB45_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB45_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB45_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB45_7
.LBB45_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB45_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB45_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB45_7:
	movw	$0, (%rbx)
	movw	%r14w, 2(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end45:
	.size	newcell, .Lfunc_end45-newcell
	.cfi_endproc

	.globl	gc_for_newcell
	.p2align	4, 0x90
	.type	gc_for_newcell,@function
gc_for_newcell:                         # @gc_for_newcell
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi142:
	.cfi_def_cfa_offset 16
.Lcfi143:
	.cfi_offset %rbx, -16
	movq	heap(%rip), %rax
	cmpq	heap_end(%rip), %rax
	jae	.LBB46_3
# BB#1:
	movq	%rax, freelist(%rip)
	movq	$0, 16(%rax)
	addq	$24, %rax
	movq	%rax, heap(%rip)
.LBB46_2:
	popq	%rbx
	retq
.LBB46_3:
	cmpq	$0, errjmp_ok(%rip)
	jne	.LBB46_5
# BB#4:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB46_5:
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movq	$0, errjmp_ok(%rip)
	callq	gc_mark_and_sweep
	movq	$1, errjmp_ok(%rip)
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB46_8
# BB#6:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB46_8
# BB#7:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB46_8:                               # %no_interrupt.exit.preheader
	movq	freelist(%rip), %rax
	testq	%rax, %rax
	je	.LBB46_15
# BB#9:                                 # %no_interrupt.exit.preheader21
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB46_10:                              # %no_interrupt.exit
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$99, %rcx
	leaq	1(%rcx), %rcx
	jg	.LBB46_12
# BB#11:                                # %no_interrupt.exit
                                        #   in Loop: Header=BB46_10 Depth=1
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.LBB46_10
.LBB46_12:
	cmpq	$101, %rcx
	jne	.LBB46_17
# BB#13:
	movq	sym_after_gc(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB46_17
# BB#14:
	xorl	%esi, %esi
	callq	leval
	xorl	%esi, %esi
	movq	%rax, %rdi
	popq	%rbx
	jmp	leval                   # TAILCALL
.LBB46_15:
	callq	allocate_aheap
	testq	%rax, %rax
	jne	.LBB46_2
# BB#16:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	popq	%rbx
	jmp	err                     # TAILCALL
.LBB46_17:
	popq	%rbx
	jmp	allocate_aheap          # TAILCALL
.Lfunc_end46:
	.size	gc_for_newcell, .Lfunc_end46-gc_for_newcell
	.cfi_endproc

	.globl	consp
	.p2align	4, 0x90
	.type	consp,@function
consp:                                  # @consp
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB47_2
# BB#1:
	movzwl	2(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	$1, %ecx
	cmoveq	sym_t(%rip), %rax
	retq
.LBB47_2:                               # %.critedge
	xorl	%eax, %eax
	retq
.Lfunc_end47:
	.size	consp, .Lfunc_end47-consp
	.cfi_endproc

	.globl	car
	.p2align	4, 0x90
	.type	car,@function
car:                                    # @car
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB48_1
# BB#2:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB48_3
# BB#4:
	cmpl	$1, %ecx
	jne	.LBB48_6
# BB#5:
	movq	8(%rax), %rax
	retq
.LBB48_1:
	xorl	%eax, %eax
	retq
.LBB48_3:
	xorl	%eax, %eax
	retq
.LBB48_6:
	pushq	%rax
.Lcfi144:
	.cfi_def_cfa_offset 16
	movl	$.L.str.27, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%eax, %eax
	addq	$8, %rsp
	retq
.Lfunc_end48:
	.size	car, .Lfunc_end48-car
	.cfi_endproc

	.globl	cdr
	.p2align	4, 0x90
	.type	cdr,@function
cdr:                                    # @cdr
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB49_1
# BB#2:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB49_3
# BB#4:
	cmpl	$1, %ecx
	jne	.LBB49_6
# BB#5:
	movq	16(%rax), %rax
	retq
.LBB49_1:
	xorl	%eax, %eax
	retq
.LBB49_3:
	xorl	%eax, %eax
	retq
.LBB49_6:
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 16
	movl	$.L.str.28, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%eax, %eax
	addq	$8, %rsp
	retq
.Lfunc_end49:
	.size	cdr, .Lfunc_end49-cdr
	.cfi_endproc

	.globl	setcar
	.p2align	4, 0x90
	.type	setcar,@function
setcar:                                 # @setcar
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi148:
	.cfi_def_cfa_offset 32
.Lcfi149:
	.cfi_offset %rbx, -24
.Lcfi150:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB50_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB50_3
.LBB50_2:                               # %.critedge
	movl	$.L.str.29, %edi
	movq	%rbx, %rsi
	callq	err
.LBB50_3:
	movq	%r14, 8(%rbx)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end50:
	.size	setcar, .Lfunc_end50-setcar
	.cfi_endproc

	.globl	setcdr
	.p2align	4, 0x90
	.type	setcdr,@function
setcdr:                                 # @setcdr
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi153:
	.cfi_def_cfa_offset 32
.Lcfi154:
	.cfi_offset %rbx, -24
.Lcfi155:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB51_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB51_3
.LBB51_2:                               # %.critedge
	movl	$.L.str.30, %edi
	movq	%rbx, %rsi
	callq	err
.LBB51_3:
	movq	%r14, 16(%rbx)
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end51:
	.size	setcdr, .Lfunc_end51-setcdr
	.cfi_endproc

	.globl	numberp
	.p2align	4, 0x90
	.type	numberp,@function
numberp:                                # @numberp
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB52_2
# BB#1:
	movzwl	2(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	$2, %ecx
	cmoveq	sym_t(%rip), %rax
	retq
.LBB52_2:                               # %.critedge
	xorl	%eax, %eax
	retq
.Lfunc_end52:
	.size	numberp, .Lfunc_end52-numberp
	.cfi_endproc

	.globl	plus
	.p2align	4, 0x90
	.type	plus,@function
plus:                                   # @plus
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi156:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi157:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 32
.Lcfi159:
	.cfi_offset %rbx, -24
.Lcfi160:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB53_1
# BB#3:
	testq	%rbx, %rbx
	je	.LBB53_5
# BB#4:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB53_6
.LBB53_5:                               # %.critedge
	movl	$.L.str.31, %edi
	movq	%rbx, %rsi
	callq	err
.LBB53_6:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB53_8
# BB#7:
	movl	$.L.str.32, %edi
	movq	%r14, %rsi
	callq	err
.LBB53_8:
	movsd	8(%rbx), %xmm3          # xmm3 = mem[0],zero
	addsd	8(%r14), %xmm3
.LBB53_9:                               # %.sink.split
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB53_14
# BB#10:
	cvttsd2si	%xmm3, %rax
	cmpq	%rcx, %rax
	jge	.LBB53_14
# BB#11:
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB53_14
# BB#12:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB53_14
	jp	.LBB53_14
# BB#13:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB53_22
.LBB53_14:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB53_18
# BB#15:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB53_17
# BB#16:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB53_17:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB53_21
.LBB53_1:
	testq	%rbx, %rbx
	jne	.LBB53_22
# BB#2:
	xorpd	%xmm3, %xmm3
	jmp	.LBB53_9
.LBB53_18:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB53_20
# BB#19:
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	gc_for_newcell
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB53_20:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB53_21:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
.LBB53_22:                              # %flocons.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end53:
	.size	plus, .Lfunc_end53-plus
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI54_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	ltimes
	.p2align	4, 0x90
	.type	ltimes,@function
ltimes:                                 # @ltimes
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi161:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi163:
	.cfi_def_cfa_offset 32
.Lcfi164:
	.cfi_offset %rbx, -24
.Lcfi165:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB54_1
# BB#3:
	testq	%rbx, %rbx
	je	.LBB54_5
# BB#4:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB54_6
.LBB54_5:                               # %.critedge
	movl	$.L.str.33, %edi
	movq	%rbx, %rsi
	callq	err
.LBB54_6:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB54_8
# BB#7:
	movl	$.L.str.34, %edi
	movq	%r14, %rsi
	callq	err
.LBB54_8:
	movsd	8(%rbx), %xmm3          # xmm3 = mem[0],zero
	mulsd	8(%r14), %xmm3
.LBB54_9:                               # %.sink.split
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB54_14
# BB#10:
	cvttsd2si	%xmm3, %rax
	cmpq	%rcx, %rax
	jge	.LBB54_14
# BB#11:
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB54_14
# BB#12:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB54_14
	jp	.LBB54_14
# BB#13:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB54_22
.LBB54_14:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB54_18
# BB#15:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB54_17
# BB#16:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB54_17:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB54_21
.LBB54_1:
	testq	%rbx, %rbx
	jne	.LBB54_22
# BB#2:
	movsd	.LCPI54_0(%rip), %xmm3  # xmm3 = mem[0],zero
	jmp	.LBB54_9
.LBB54_18:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB54_20
# BB#19:
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	gc_for_newcell
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB54_20:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB54_21:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
.LBB54_22:                              # %flocons.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end54:
	.size	ltimes, .Lfunc_end54-ltimes
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI55_0:
	.quad	-9223372036854775808    # double -0
	.text
	.globl	difference
	.p2align	4, 0x90
	.type	difference,@function
difference:                             # @difference
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 32
.Lcfi169:
	.cfi_offset %rbx, -24
.Lcfi170:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB55_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB55_3
.LBB55_2:                               # %.critedge
	movl	$.L.str.35, %edi
	movq	%rbx, %rsi
	callq	err
.LBB55_3:
	testq	%r14, %r14
	je	.LBB55_4
# BB#5:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB55_7
# BB#6:
	movl	$.L.str.36, %edi
	movq	%r14, %rsi
	callq	err
.LBB55_7:
	movsd	8(%rbx), %xmm3          # xmm3 = mem[0],zero
	jmp	.LBB55_8
.LBB55_4:
	movsd	.LCPI55_0(%rip), %xmm3  # xmm3 = mem[0],zero
	movq	%rbx, %r14
.LBB55_8:
	subsd	8(%r14), %xmm3
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB55_13
# BB#9:
	cvttsd2si	%xmm3, %rax
	cmpq	%rcx, %rax
	jge	.LBB55_13
# BB#10:
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB55_13
# BB#11:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB55_13
	jp	.LBB55_13
# BB#12:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB55_21
.LBB55_13:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB55_17
# BB#14:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB55_16
# BB#15:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB55_16:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB55_20
.LBB55_17:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB55_19
# BB#18:
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	gc_for_newcell
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB55_19:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB55_20:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
.LBB55_21:                              # %flocons.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end55:
	.size	difference, .Lfunc_end55-difference
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI56_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	Quotient
	.p2align	4, 0x90
	.type	Quotient,@function
Quotient:                               # @Quotient
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi173:
	.cfi_def_cfa_offset 32
.Lcfi174:
	.cfi_offset %rbx, -24
.Lcfi175:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB56_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB56_3
.LBB56_2:                               # %.critedge
	movl	$.L.str.37, %edi
	movq	%rbx, %rsi
	callq	err
.LBB56_3:
	testq	%r14, %r14
	je	.LBB56_4
# BB#5:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB56_7
# BB#6:
	movl	$.L.str.38, %edi
	movq	%r14, %rsi
	callq	err
.LBB56_7:
	movsd	8(%rbx), %xmm3          # xmm3 = mem[0],zero
	jmp	.LBB56_8
.LBB56_4:
	movsd	.LCPI56_0(%rip), %xmm3  # xmm3 = mem[0],zero
	movq	%rbx, %r14
.LBB56_8:
	divsd	8(%r14), %xmm3
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB56_13
# BB#9:
	cvttsd2si	%xmm3, %rax
	cmpq	%rcx, %rax
	jge	.LBB56_13
# BB#10:
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB56_13
# BB#11:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB56_13
	jp	.LBB56_13
# BB#12:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB56_21
.LBB56_13:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB56_17
# BB#14:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB56_16
# BB#15:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB56_16:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB56_20
.LBB56_17:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB56_19
# BB#18:
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	gc_for_newcell
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB56_19:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB56_20:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
.LBB56_21:                              # %flocons.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end56:
	.size	Quotient, .Lfunc_end56-Quotient
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI57_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI57_1:
	.quad	-9223372036854775808    # double -0
	.text
	.globl	lllabs
	.p2align	4, 0x90
	.type	lllabs,@function
lllabs:                                 # @lllabs
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 32
.Lcfi178:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB57_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB57_3
.LBB57_2:                               # %.critedge
	movl	$.L.str.39, %edi
	movq	%rbx, %rsi
	callq	err
.LBB57_3:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	xorpd	%xmm1, %xmm1
	ucomisd	%xmm0, %xmm1
	jbe	.LBB57_17
# BB#4:
	movapd	.LCPI57_0(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm2
	movq	inums_dim(%rip), %rax
	testq	%rax, %rax
	jle	.LBB57_9
# BB#5:
	movsd	.LCPI57_1(%rip), %xmm1  # xmm1 = mem[0],zero
	ucomisd	%xmm0, %xmm1
	jb	.LBB57_9
# BB#6:
	cvttsd2si	%xmm2, %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdq	%rcx, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB57_9
	jp	.LBB57_9
# BB#7:
	cmpq	%rax, %rcx
	jge	.LBB57_9
# BB#8:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB57_17
.LBB57_9:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB57_13
# BB#10:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB57_12
# BB#11:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movapd	%xmm2, (%rsp)           # 16-byte Spill
	callq	err
	movapd	(%rsp), %xmm2           # 16-byte Reload
.LBB57_12:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB57_16
.LBB57_13:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB57_15
# BB#14:
	movapd	%xmm2, (%rsp)           # 16-byte Spill
	callq	gc_for_newcell
	movapd	(%rsp), %xmm2           # 16-byte Reload
	movq	freelist(%rip), %rbx
.LBB57_15:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB57_16:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB57_17:                              # %flocons.exit
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end57:
	.size	lllabs, .Lfunc_end57-lllabs
	.cfi_endproc

	.globl	lsqrt
	.p2align	4, 0x90
	.type	lsqrt,@function
lsqrt:                                  # @lsqrt
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi180:
	.cfi_def_cfa_offset 32
.Lcfi181:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB58_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB58_3
.LBB58_2:                               # %.critedge
	movl	$.L.str.40, %edi
	movq	%rbx, %rsi
	callq	err
.LBB58_3:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	sqrtsd	%xmm0, %xmm3
	ucomisd	%xmm3, %xmm3
	jnp	.LBB58_5
# BB#4:                                 # %call.sqrt
	callq	sqrt
	movapd	%xmm0, %xmm3
.LBB58_5:                               # %.split
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB58_10
# BB#6:
	cvttsd2si	%xmm3, %rax
	cmpq	%rcx, %rax
	jge	.LBB58_10
# BB#7:
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB58_10
# BB#8:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB58_10
	jp	.LBB58_10
# BB#9:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB58_18
.LBB58_10:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB58_14
# BB#11:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB58_13
# BB#12:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB58_13:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB58_17
.LBB58_14:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB58_16
# BB#15:
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB58_16:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB58_17:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
.LBB58_18:                              # %flocons.exit
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end58:
	.size	lsqrt, .Lfunc_end58-lsqrt
	.cfi_endproc

	.globl	greaterp
	.p2align	4, 0x90
	.type	greaterp,@function
greaterp:                               # @greaterp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi184:
	.cfi_def_cfa_offset 32
.Lcfi185:
	.cfi_offset %rbx, -24
.Lcfi186:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB59_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB59_3
.LBB59_2:                               # %.critedge
	movl	$.L.str.41, %edi
	movq	%rbx, %rsi
	callq	err
.LBB59_3:
	testq	%r14, %r14
	je	.LBB59_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB59_6
.LBB59_5:                               # %.critedge10
	movl	$.L.str.42, %edi
	movq	%r14, %rsi
	callq	err
.LBB59_6:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	8(%r14), %xmm0
	cmovaq	sym_t(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end59:
	.size	greaterp, .Lfunc_end59-greaterp
	.cfi_endproc

	.globl	lessp
	.p2align	4, 0x90
	.type	lessp,@function
lessp:                                  # @lessp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi189:
	.cfi_def_cfa_offset 32
.Lcfi190:
	.cfi_offset %rbx, -24
.Lcfi191:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB60_2
# BB#1:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB60_3
.LBB60_2:                               # %.critedge
	movl	$.L.str.43, %edi
	movq	%r14, %rsi
	callq	err
.LBB60_3:
	testq	%rbx, %rbx
	je	.LBB60_5
# BB#4:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB60_6
.LBB60_5:                               # %.critedge10
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB60_6:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	8(%r14), %xmm0
	cmovaq	sym_t(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end60:
	.size	lessp, .Lfunc_end60-lessp
	.cfi_endproc

	.globl	greaterEp
	.p2align	4, 0x90
	.type	greaterEp,@function
greaterEp:                              # @greaterEp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi194:
	.cfi_def_cfa_offset 32
.Lcfi195:
	.cfi_offset %rbx, -24
.Lcfi196:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB61_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB61_3
.LBB61_2:                               # %.critedge
	movl	$.L.str.41, %edi
	movq	%rbx, %rsi
	callq	err
.LBB61_3:
	testq	%r14, %r14
	je	.LBB61_5
# BB#4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB61_6
.LBB61_5:                               # %.critedge10
	movl	$.L.str.42, %edi
	movq	%r14, %rsi
	callq	err
.LBB61_6:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	8(%r14), %xmm0
	cmovaeq	sym_t(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end61:
	.size	greaterEp, .Lfunc_end61-greaterEp
	.cfi_endproc

	.globl	lessEp
	.p2align	4, 0x90
	.type	lessEp,@function
lessEp:                                 # @lessEp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi198:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi199:
	.cfi_def_cfa_offset 32
.Lcfi200:
	.cfi_offset %rbx, -24
.Lcfi201:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB62_2
# BB#1:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB62_3
.LBB62_2:                               # %.critedge
	movl	$.L.str.43, %edi
	movq	%r14, %rsi
	callq	err
.LBB62_3:
	testq	%rbx, %rbx
	je	.LBB62_5
# BB#4:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB62_6
.LBB62_5:                               # %.critedge10
	movl	$.L.str.44, %edi
	movq	%rbx, %rsi
	callq	err
.LBB62_6:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	xorl	%eax, %eax
	ucomisd	8(%r14), %xmm0
	cmovaeq	sym_t(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end62:
	.size	lessEp, .Lfunc_end62-lessEp
	.cfi_endproc

	.globl	lmax
	.p2align	4, 0x90
	.type	lmax,@function
lmax:                                   # @lmax
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi204:
	.cfi_def_cfa_offset 32
.Lcfi205:
	.cfi_offset %rbx, -24
.Lcfi206:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB63_7
# BB#1:
	testq	%rbx, %rbx
	je	.LBB63_3
# BB#2:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB63_4
.LBB63_3:                               # %.critedge
	movl	$.L.str.45, %edi
	movq	%rbx, %rsi
	callq	err
.LBB63_4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB63_6
# BB#5:
	movl	$.L.str.46, %edi
	movq	%r14, %rsi
	callq	err
.LBB63_6:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	ucomisd	8(%r14), %xmm0
	cmovbeq	%r14, %rbx
.LBB63_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end63:
	.size	lmax, .Lfunc_end63-lmax
	.cfi_endproc

	.globl	lmin
	.p2align	4, 0x90
	.type	lmin,@function
lmin:                                   # @lmin
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi209:
	.cfi_def_cfa_offset 32
.Lcfi210:
	.cfi_offset %rbx, -24
.Lcfi211:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%r14, %r14
	je	.LBB64_7
# BB#1:
	testq	%rbx, %rbx
	je	.LBB64_3
# BB#2:
	movzwl	2(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB64_4
.LBB64_3:                               # %.critedge
	movl	$.L.str.47, %edi
	movq	%rbx, %rsi
	callq	err
.LBB64_4:
	movzwl	2(%r14), %eax
	cmpl	$2, %eax
	je	.LBB64_6
# BB#5:
	movl	$.L.str.48, %edi
	movq	%r14, %rsi
	callq	err
.LBB64_6:
	movsd	8(%r14), %xmm0          # xmm0 = mem[0],zero
	ucomisd	8(%rbx), %xmm0
	cmovbeq	%r14, %rbx
.LBB64_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end64:
	.size	lmin, .Lfunc_end64-lmin
	.cfi_endproc

	.globl	eq
	.p2align	4, 0x90
	.type	eq,@function
eq:                                     # @eq
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpq	%rsi, %rdi
	cmoveq	sym_t(%rip), %rax
	retq
.Lfunc_end65:
	.size	eq, .Lfunc_end65-eq
	.cfi_endproc

	.globl	eql
	.p2align	4, 0x90
	.type	eql,@function
eql:                                    # @eql
	.cfi_startproc
# BB#0:
	cmpq	%rsi, %rdi
	je	.LBB66_8
# BB#1:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	je	.LBB66_9
# BB#2:
	testq	%rsi, %rsi
	je	.LBB66_9
# BB#3:
	movzwl	2(%rdi), %ecx
	cmpl	$2, %ecx
	jne	.LBB66_9
# BB#4:
	movzwl	2(%rsi), %eax
	cmpl	$2, %eax
	jne	.LBB66_5
# BB#6:
	movsd	8(%rdi), %xmm0          # xmm0 = mem[0],zero
	ucomisd	8(%rsi), %xmm0
	jne	.LBB66_7
	jp	.LBB66_7
.LBB66_8:
	movq	sym_t(%rip), %rax
.LBB66_9:                               # %.critedge
	retq
.LBB66_5:
	xorl	%eax, %eax
	retq
.LBB66_7:
	xorl	%eax, %eax
	retq
.Lfunc_end66:
	.size	eql, .Lfunc_end66-eql
	.cfi_endproc

	.globl	symcons
	.p2align	4, 0x90
	.type	symcons,@function
symcons:                                # @symcons
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi212:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi213:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi214:
	.cfi_def_cfa_offset 32
.Lcfi215:
	.cfi_offset %rbx, -32
.Lcfi216:
	.cfi_offset %r14, -24
.Lcfi217:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB67_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB67_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB67_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB67_7
.LBB67_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB67_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB67_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB67_7:
	movw	$0, (%rbx)
	movw	$3, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end67:
	.size	symcons, .Lfunc_end67-symcons
	.cfi_endproc

	.globl	symbolp
	.p2align	4, 0x90
	.type	symbolp,@function
symbolp:                                # @symbolp
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB68_2
# BB#1:
	movzwl	2(%rdi), %ecx
	xorl	%eax, %eax
	cmpl	$3, %ecx
	cmoveq	sym_t(%rip), %rax
	retq
.LBB68_2:                               # %.critedge
	xorl	%eax, %eax
	retq
.Lfunc_end68:
	.size	symbolp, .Lfunc_end68-symbolp
	.cfi_endproc

	.globl	err_ubv
	.p2align	4, 0x90
	.type	err_ubv,@function
err_ubv:                                # @err_ubv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi218:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	movl	$.L.str.49, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end69:
	.size	err_ubv, .Lfunc_end69-err_ubv
	.cfi_endproc

	.globl	symbol_boundp
	.p2align	4, 0x90
	.type	symbol_boundp,@function
symbol_boundp:                          # @symbol_boundp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi219:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi220:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi221:
	.cfi_def_cfa_offset 32
.Lcfi222:
	.cfi_offset %rbx, -24
.Lcfi223:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB70_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB70_3
.LBB70_2:                               # %.critedge
	movl	$.L.str.50, %edi
	movq	%rbx, %rsi
	callq	err
.LBB70_3:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	envlookup
	testq	%rax, %rax
	je	.LBB70_6
# BB#4:
	movq	sym_t(%rip), %rax
	jmp	.LBB70_5
.LBB70_6:
	movq	16(%rbx), %rcx
	xorl	%eax, %eax
	cmpq	unbound_marker(%rip), %rcx
	cmovneq	sym_t(%rip), %rax
.LBB70_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end70:
	.size	symbol_boundp, .Lfunc_end70-symbol_boundp
	.cfi_endproc

	.globl	envlookup
	.p2align	4, 0x90
	.type	envlookup,@function
envlookup:                              # @envlookup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi224:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi227:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi228:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi229:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi230:
	.cfi_def_cfa_offset 64
.Lcfi231:
	.cfi_offset %rbx, -56
.Lcfi232:
	.cfi_offset %r12, -48
.Lcfi233:
	.cfi_offset %r13, -40
.Lcfi234:
	.cfi_offset %r14, -32
.Lcfi235:
	.cfi_offset %r15, -24
.Lcfi236:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB71_26
# BB#1:                                 # %.lr.ph55.preheader
	movq	%r14, %r13
.LBB71_2:                               # %.lr.ph55
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB71_8 Depth 2
	movzwl	2(%r13), %eax
	cmpl	$1, %eax
	jne	.LBB71_25
# BB#3:                                 #   in Loop: Header=BB71_2 Depth=1
	movq	8(%r13), %r12
	testq	%r12, %r12
	je	.LBB71_5
# BB#4:                                 #   in Loop: Header=BB71_2 Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	je	.LBB71_6
.LBB71_5:                               # %.thread37
                                        #   in Loop: Header=BB71_2 Depth=1
	movl	$.L.str.85, %edi
	movq	%r12, %rsi
	callq	err
.LBB71_6:                               #   in Loop: Header=BB71_2 Depth=1
	movq	8(%r12), %rbp
	testq	%rbp, %rbp
	je	.LBB71_14
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB71_2 Depth=1
	movq	%r12, %rbx
	.p2align	4, 0x90
.LBB71_8:                               # %.lr.ph
                                        #   Parent Loop BB71_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzwl	2(%rbp), %eax
	cmpl	$1, %eax
	jne	.LBB71_15
# BB#9:                                 #   in Loop: Header=BB71_8 Depth=2
	testq	%rbx, %rbx
	je	.LBB71_11
# BB#10:                                #   in Loop: Header=BB71_8 Depth=2
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB71_12
.LBB71_11:                              # %.thread39
                                        #   in Loop: Header=BB71_8 Depth=2
	movl	$.L.str.86, %edi
	movq	%r12, %rsi
	callq	err
.LBB71_12:                              #   in Loop: Header=BB71_8 Depth=2
	cmpq	%r15, 8(%rbp)
	je	.LBB71_27
# BB#13:                                #   in Loop: Header=BB71_8 Depth=2
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB71_8
	jmp	.LBB71_14
	.p2align	4, 0x90
.LBB71_15:                              #   in Loop: Header=BB71_2 Depth=1
	cmpq	%r15, %rbp
	jne	.LBB71_14
# BB#16:                                #   in Loop: Header=BB71_2 Depth=1
	movzwl	%ax, %eax
	cmpl	$3, %eax
	je	.LBB71_17
	.p2align	4, 0x90
.LBB71_14:                              # %.thread41
                                        #   in Loop: Header=BB71_2 Depth=1
	movq	16(%r13), %r13
	testq	%r13, %r13
	jne	.LBB71_2
	jmp	.LBB71_26
.LBB71_25:
	movl	$.L.str.87, %edi
	movq	%r14, %rsi
	callq	err
.LBB71_26:                              # %.thread43
	xorl	%ebx, %ebx
.LBB71_27:                              # %.thread43
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB71_17:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB71_21
# BB#18:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB71_20
# BB#19:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB71_20:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB71_24
.LBB71_21:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB71_23
# BB#22:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB71_23:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB71_24:                              # %cons.exit
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%rbx, 8(%rbp)
	movq	$0, 16(%rbp)
	movq	%rbp, %rbx
	jmp	.LBB71_27
.Lfunc_end71:
	.size	envlookup, .Lfunc_end71-envlookup
	.cfi_endproc

	.globl	symbol_value
	.p2align	4, 0x90
	.type	symbol_value,@function
symbol_value:                           # @symbol_value
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi237:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi239:
	.cfi_def_cfa_offset 32
.Lcfi240:
	.cfi_offset %rbx, -24
.Lcfi241:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB72_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB72_3
.LBB72_2:                               # %.critedge
	movl	$.L.str.50, %edi
	movq	%rbx, %rsi
	callq	err
.LBB72_3:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	envlookup
	testq	%rax, %rax
	je	.LBB72_5
# BB#4:
	movq	8(%rax), %r14
	jmp	.LBB72_7
.LBB72_5:
	movq	16(%rbx), %r14
	cmpq	unbound_marker(%rip), %r14
	jne	.LBB72_7
# BB#6:
	movl	$.L.str.49, %edi
	movq	%rbx, %rsi
	callq	err
.LBB72_7:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end72:
	.size	symbol_value, .Lfunc_end72-symbol_value
	.cfi_endproc

	.globl	must_malloc
	.p2align	4, 0x90
	.type	must_malloc,@function
must_malloc:                            # @must_malloc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi242:
	.cfi_def_cfa_offset 16
.Lcfi243:
	.cfi_offset %rbx, -16
	testq	%rdi, %rdi
	movl	$1, %eax
	cmoveq	%rax, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB73_2
# BB#1:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB73_2:
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end73:
	.size	must_malloc, .Lfunc_end73-must_malloc
	.cfi_endproc

	.globl	gen_intern
	.p2align	4, 0x90
	.type	gen_intern,@function
gen_intern:                             # @gen_intern
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi244:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi245:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi246:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi247:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi248:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi250:
	.cfi_def_cfa_offset 64
.Lcfi251:
	.cfi_offset %rbx, -56
.Lcfi252:
	.cfi_offset %r12, -48
.Lcfi253:
	.cfi_offset %r13, -40
.Lcfi254:
	.cfi_offset %r14, -32
.Lcfi255:
	.cfi_offset %r15, -24
.Lcfi256:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	nointerrupt(%rip), %rax
	movq	$1, nointerrupt(%rip)
	movq	obarray_dim(%rip), %rcx
	cmpq	$2, %rcx
	movq	%rax, (%rsp)            # 8-byte Spill
	jl	.LBB74_1
# BB#2:                                 # %.preheader
	movb	(%r15), %al
	testb	%al, %al
	je	.LBB74_3
# BB#4:                                 # %.lr.ph59.preheader
	leaq	1(%r15), %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB74_5:                               # %.lr.ph59
                                        # =>This Inner Loop Header: Depth=1
	movsbq	%al, %rdx
	movq	%rbx, %rax
	shlq	$4, %rax
	addq	%rbx, %rax
	xorq	%rdx, %rax
	cqto
	idivq	%rcx
	movzbl	(%rsi), %eax
	incq	%rsi
	testb	%al, %al
	movq	%rdx, %rbx
	jne	.LBB74_5
	jmp	.LBB74_6
.LBB74_1:
	movl	$oblistvar, %eax
	xorl	%ebx, %ebx
	jmp	.LBB74_7
.LBB74_3:
	xorl	%ebx, %ebx
.LBB74_6:                               # %._crit_edge60
	leaq	(,%rbx,8), %rax
	addq	obarray(%rip), %rax
.LBB74_7:
	movq	(%rax), %r13
	testq	%r13, %r13
	je	.LBB74_14
# BB#8:                                 # %.lr.ph.preheader
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB74_9:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %r14
	movq	8(%r14), %rsi
	movq	%r15, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB74_10
# BB#13:                                #   in Loop: Header=BB74_9 Depth=1
	movq	16(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB74_9
.LBB74_14:                              # %._crit_edge
	cmpq	$1, %r12
	jne	.LBB74_15
# BB#16:
	movq	%r15, %rdi
	callq	strlen
	incq	%rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %r12
	testq	%r12, %r12
	jne	.LBB74_18
# BB#17:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB74_18:                              # %must_malloc.exit
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	strcpy
	jmp	.LBB74_19
.LBB74_15:
	movq	%r15, %r12
.LBB74_19:
	movq	unbound_marker(%rip), %rbp
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB74_23
# BB#20:
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	jb	.LBB74_22
# BB#21:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB74_22:
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB74_26
.LBB74_23:
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	jne	.LBB74_25
# BB#24:
	callq	gc_for_newcell
	movq	freelist(%rip), %r14
.LBB74_25:
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB74_26:                              # %symcons.exit
	movw	$0, (%r14)
	movw	$3, 2(%r14)
	movq	%r12, 8(%r14)
	movq	%rbp, 16(%r14)
	cmpq	$2, obarray_dim(%rip)
	jl	.LBB74_35
# BB#27:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB74_31
# BB#28:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB74_30
# BB#29:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB74_30:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB74_34
.LBB74_10:
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, nointerrupt(%rip)
	testq	%rax, %rax
	jne	.LBB74_45
# BB#11:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB74_45
# BB#12:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	movq	8(%rbp), %r14
	jmp	.LBB74_45
.LBB74_31:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB74_33
# BB#32:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB74_33:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB74_34:                              # %cons.exit
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r14, 8(%rbp)
	movq	%r13, 16(%rbp)
	movq	obarray(%rip), %rax
	movq	%rbp, (%rax,%rbx,8)
.LBB74_35:
	movq	oblistvar(%rip), %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB74_39
# BB#36:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB74_38
# BB#37:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB74_38:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB74_42
.LBB74_39:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB74_41
# BB#40:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB74_41:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB74_42:                              # %cons.exit55
	movq	(%rsp), %rax            # 8-byte Reload
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r14, 8(%rbp)
	movq	%rbx, 16(%rbp)
	movq	%rbp, oblistvar(%rip)
	movq	%rax, nointerrupt(%rip)
	testq	%rax, %rax
	jne	.LBB74_45
# BB#43:                                # %cons.exit55
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB74_45
# BB#44:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB74_45:                              # %no_interrupt.exit
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end74:
	.size	gen_intern, .Lfunc_end74-gen_intern
	.cfi_endproc

	.globl	cintern
	.p2align	4, 0x90
	.type	cintern,@function
cintern:                                # @cintern
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	gen_intern              # TAILCALL
.Lfunc_end75:
	.size	cintern, .Lfunc_end75-cintern
	.cfi_endproc

	.globl	rintern
	.p2align	4, 0x90
	.type	rintern,@function
rintern:                                # @rintern
	.cfi_startproc
# BB#0:
	movl	$1, %esi
	jmp	gen_intern              # TAILCALL
.Lfunc_end76:
	.size	rintern, .Lfunc_end76-rintern
	.cfi_endproc

	.globl	intern
	.p2align	4, 0x90
	.type	intern,@function
intern:                                 # @intern
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB77_5
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB77_4
# BB#2:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	jne	.LBB77_5
# BB#3:
	movq	8(%rax), %rdi
	movl	$1, %esi
	jmp	gen_intern              # TAILCALL
.LBB77_5:                               # %.critedge8.i
	pushq	%rax
.Lcfi257:
	.cfi_def_cfa_offset 16
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%edi, %edi
	addq	$8, %rsp
	movl	$1, %esi
	jmp	gen_intern              # TAILCALL
.LBB77_4:
	movq	16(%rax), %rdi
	movl	$1, %esi
	jmp	gen_intern              # TAILCALL
.Lfunc_end77:
	.size	intern, .Lfunc_end77-intern
	.cfi_endproc

	.globl	subrcons
	.p2align	4, 0x90
	.type	subrcons,@function
subrcons:                               # @subrcons
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi260:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi261:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi262:
	.cfi_def_cfa_offset 48
.Lcfi263:
	.cfi_offset %rbx, -40
.Lcfi264:
	.cfi_offset %r12, -32
.Lcfi265:
	.cfi_offset %r14, -24
.Lcfi266:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB78_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB78_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB78_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB78_7
.LBB78_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB78_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB78_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB78_7:
	movw	$0, (%rbx)
	movw	%r12w, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end78:
	.size	subrcons, .Lfunc_end78-subrcons
	.cfi_endproc

	.globl	closure
	.p2align	4, 0x90
	.type	closure,@function
closure:                                # @closure
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi267:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi268:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 32
.Lcfi270:
	.cfi_offset %rbx, -32
.Lcfi271:
	.cfi_offset %r14, -24
.Lcfi272:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB79_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB79_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB79_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB79_7
.LBB79_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB79_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB79_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB79_7:
	movw	$0, (%rbx)
	movw	$11, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end79:
	.size	closure, .Lfunc_end79-closure
	.cfi_endproc

	.globl	gc_protect
	.p2align	4, 0x90
	.type	gc_protect,@function
gc_protect:                             # @gc_protect
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi273:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi274:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi275:
	.cfi_def_cfa_offset 32
.Lcfi276:
	.cfi_offset %rbx, -24
.Lcfi277:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB80_2
# BB#1:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB80_2:                               # %gc_protect_n.exit
	movq	%r14, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end80:
	.size	gc_protect, .Lfunc_end80-gc_protect
	.cfi_endproc

	.globl	gc_protect_n
	.p2align	4, 0x90
	.type	gc_protect_n,@function
gc_protect_n:                           # @gc_protect_n
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi278:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi279:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi280:
	.cfi_def_cfa_offset 32
.Lcfi281:
	.cfi_offset %rbx, -32
.Lcfi282:
	.cfi_offset %r14, -24
.Lcfi283:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB81_2
# BB#1:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB81_2:                               # %must_malloc.exit
	movq	%r15, (%rbx)
	movq	%r14, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end81:
	.size	gc_protect_n, .Lfunc_end81-gc_protect_n
	.cfi_endproc

	.globl	gc_protect_sym
	.p2align	4, 0x90
	.type	gc_protect_sym,@function
gc_protect_sym:                         # @gc_protect_sym
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi284:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi285:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi286:
	.cfi_def_cfa_offset 32
.Lcfi287:
	.cfi_offset %rbx, -24
.Lcfi288:
	.cfi_offset %r14, -16
	movq	%rsi, %rax
	movq	%rdi, %r14
	xorl	%esi, %esi
	movq	%rax, %rdi
	callq	gen_intern
	movq	%rax, (%r14)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB82_2
# BB#1:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB82_2:                               # %gc_protect.exit
	movq	%r14, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end82:
	.size	gc_protect_sym, .Lfunc_end82-gc_protect_sym
	.cfi_endproc

	.globl	scan_registers
	.p2align	4, 0x90
	.type	scan_registers,@function
scan_registers:                         # @scan_registers
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi289:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi290:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi291:
	.cfi_def_cfa_offset 32
.Lcfi292:
	.cfi_offset %rbx, -32
.Lcfi293:
	.cfi_offset %r14, -24
.Lcfi294:
	.cfi_offset %r15, -16
	movq	protected_registers(%rip), %r14
	testq	%r14, %r14
	jne	.LBB83_2
	jmp	.LBB83_6
	.p2align	4, 0x90
.LBB83_5:                               # %._crit_edge
                                        #   in Loop: Header=BB83_2 Depth=1
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.LBB83_6
.LBB83_2:                               # %.lr.ph22
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB83_4 Depth 2
	movq	8(%r14), %r15
	testq	%r15, %r15
	jle	.LBB83_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB83_2 Depth=1
	movq	(%r14), %rbx
	.p2align	4, 0x90
.LBB83_4:                               # %.lr.ph
                                        #   Parent Loop BB83_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	callq	gc_relocate
	movq	%rax, (%rbx)
	addq	$8, %rbx
	decq	%r15
	jne	.LBB83_4
	jmp	.LBB83_5
.LBB83_6:                               # %._crit_edge23
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end83:
	.size	scan_registers, .Lfunc_end83-scan_registers
	.cfi_endproc

	.globl	gc_relocate
	.p2align	4, 0x90
	.type	gc_relocate,@function
gc_relocate:                            # @gc_relocate
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi295:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi296:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi297:
	.cfi_def_cfa_offset 32
.Lcfi298:
	.cfi_offset %rbx, -32
.Lcfi299:
	.cfi_offset %r14, -24
.Lcfi300:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB84_1
# BB#2:
	movzwl	(%r14), %eax
	cmpl	$1, %eax
	jne	.LBB84_4
# BB#3:
	movq	8(%r14), %rbx
	jmp	.LBB84_17
.LBB84_1:
	xorl	%ebx, %ebx
	jmp	.LBB84_17
.LBB84_4:
	movswl	2(%r14), %ebp
	cmpl	$21, %ebp
	ja	.LBB84_6
# BB#5:
	movl	$3674110, %eax          # imm = 0x380FFE
	btl	%ebp, %eax
	jae	.LBB84_6
.LBB84_13:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB84_15
# BB#14:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB84_15:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	movq	16(%r14), %rax
	movq	%rax, 16(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, (%rbx)
.LBB84_16:
	movw	$1, (%r14)
	movq	%rbx, 8(%r14)
.LBB84_17:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB84_6:
	movq	user_types(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB84_10
# BB#7:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB84_9
# BB#8:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB84_9:                               # %must_malloc.exit.i
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
.LBB84_10:
	movzwl	%bp, %eax
	cmpl	$99, %eax
	ja	.LBB84_18
# BB#11:                                # %get_user_type_hooks.exit
	movslq	%ebp, %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	(%rbx,%rax), %rax
	testq	%rax, %rax
	je	.LBB84_13
# BB#12:
	movq	%r14, %rdi
	callq	*%rax
	movq	%rax, %rbx
	jmp	.LBB84_16
.LBB84_18:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end84:
	.size	gc_relocate, .Lfunc_end84-gc_relocate
	.cfi_endproc

	.globl	init_storage
	.p2align	4, 0x90
	.type	init_storage,@function
init_storage:                           # @init_storage
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi301:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi302:
	.cfi_def_cfa_offset 32
.Lcfi303:
	.cfi_offset %rbx, -16
	cmpq	$0, stack_start_ptr(%rip)
	jne	.LBB85_2
# BB#1:
	leaq	8(%rsp), %rax
	movq	%rax, stack_start_ptr(%rip)
.LBB85_2:
	callq	init_storage_1
	callq	init_storage_a
	movq	user_types(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB85_4
# BB#3:                                 # %set_gc_hooks.exit.thread
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1360(%rbx)
	movq	$0, 1376(%rbx)
	movq	$file_gc_free, 1384(%rbx)
	jmp	.LBB85_10
.LBB85_4:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB85_6
# BB#5:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB85_6:                               # %set_gc_hooks.exit
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
	xorps	%xmm0, %xmm0
	movups	%xmm0, 1360(%rbx)
	movq	$0, 1376(%rbx)
	movq	$file_gc_free, 1384(%rbx)
	testq	%rbx, %rbx
	jne	.LBB85_10
# BB#7:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB85_9
# BB#8:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB85_9:                               # %must_malloc.exit.i.i1
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
.LBB85_10:                              # %set_print_hooks.exit
	movq	$file_prin1, 1392(%rbx)
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end85:
	.size	init_storage, .Lfunc_end85-init_storage
	.cfi_endproc

	.globl	init_storage_1
	.p2align	4, 0x90
	.type	init_storage_1,@function
init_storage_1:                         # @init_storage_1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi304:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi305:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi306:
	.cfi_def_cfa_offset 32
.Lcfi307:
	.cfi_offset %rbx, -32
.Lcfi308:
	.cfi_offset %r14, -24
.Lcfi309:
	.cfi_offset %r15, -16
	movl	$5121, %edi             # imm = 0x1401
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_2
# BB#1:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_2:                               # %must_malloc.exit
	movq	%rbx, tkbuffer(%rip)
	cmpq	$1, gc_kind_copying(%rip)
	sete	%al
	movq	nheaps(%rip), %rbx
	cmpq	$2, %rbx
	setne	%cl
	testq	%rbx, %rbx
	jle	.LBB86_4
# BB#3:                                 # %must_malloc.exit
	andb	%cl, %al
	je	.LBB86_5
.LBB86_4:
	movl	$.L.str.52, %edi
	xorl	%esi, %esi
	callq	err
	movq	nheaps(%rip), %rbx
.LBB86_5:
	leaq	(,%rbx,8), %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB86_7
# BB#6:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	nheaps(%rip), %rbx
.LBB86_7:                               # %must_malloc.exit21
	movq	%r14, heaps(%rip)
	testq	%rbx, %rbx
	jle	.LBB86_11
# BB#8:                                 # %.lr.ph85
	movq	$0, (%r14)
	cmpq	$1, %rbx
	je	.LBB86_11
# BB#9:                                 # %._crit_edge91.preheader
	movl	$1, %eax
	.p2align	4, 0x90
.LBB86_10:                              # %._crit_edge91
                                        # =>This Inner Loop Header: Depth=1
	movq	heaps(%rip), %rcx
	movq	$0, (%rcx,%rax,8)
	incq	%rax
	cmpq	%rbx, %rax
	jl	.LBB86_10
.LBB86_11:                              # %._crit_edge86
	movq	heap_size(%rip), %rbx
	leaq	(,%rbx,8), %rax
	leaq	(%rax,%rax,2), %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB86_13
# BB#12:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	heap_size(%rip), %rbx
.LBB86_13:                              # %must_malloc.exit22
	movq	heaps(%rip), %rax
	movq	%r14, (%rax)
	movq	heaps(%rip), %r14
	movq	(%r14), %rax
	movq	%rax, heap(%rip)
	movq	%rax, heap_org(%rip)
	leaq	(%rbx,%rbx,2), %rcx
	leaq	(%rax,%rcx,8), %rax
	movq	%rax, heap_end(%rip)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB86_17
# BB#14:
	shlq	$3, %rbx
	leaq	(%rbx,%rbx,2), %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_16
# BB#15:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	heaps(%rip), %r14
.LBB86_16:                              # %must_malloc.exit23
	movq	%rbx, 8(%r14)
	jmp	.LBB86_18
.LBB86_17:
	movq	$0, freelist(%rip)
.LBB86_18:
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_20
# BB#19:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_20:                              # %gc_protect.exit
	movq	$oblistvar, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movq	obarray_dim(%rip), %r15
	cmpq	$2, %r15
	jl	.LBB86_29
# BB#21:
	leaq	(,%r15,8), %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB86_23
# BB#22:                                # %must_malloc.exit24.thread
	movq	%rbx, obarray(%rip)
	jmp	.LBB86_24
.LBB86_23:                              # %must_malloc.exit24
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	obarray_dim(%rip), %r15
	movq	%rbx, obarray(%rip)
	testq	%r15, %r15
	jle	.LBB86_26
.LBB86_24:                              # %.lr.ph80
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB86_25:                              # =>This Inner Loop Header: Depth=1
	movq	$0, (%rbx,%rax,8)
	incq	%rax
	movq	obarray(%rip), %rbx
	cmpq	%r15, %rax
	jl	.LBB86_25
.LBB86_26:                              # %._crit_edge81
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB86_28
# BB#27:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_28:                              # %gc_protect_n.exit
	movq	%rbx, (%r14)
	movq	%r15, 8(%r14)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%r14)
	movq	%r14, protected_registers(%rip)
.LBB86_29:
	movl	$.L.str.53, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB86_33
# BB#30:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB86_32
# BB#31:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_32:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB86_36
.LBB86_33:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB86_35
# BB#34:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB86_35:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB86_36:                              # %cons.exit
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r14, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, unbound_marker(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_38
# BB#37:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_38:                              # %gc_protect.exit25
	movq	$unbound_marker, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movl	$.L.str.54, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB86_42
# BB#39:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB86_41
# BB#40:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_41:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB86_45
.LBB86_42:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB86_44
# BB#43:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB86_44:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB86_45:                              # %cons.exit28
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r14, 8(%rbx)
	movq	$0, 16(%rbx)
	movq	%rbx, eof_val(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_47
# BB#46:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_47:                              # %gc_protect.exit29
	movq	$eof_val, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movl	$.L.str.55, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	movq	%r14, sym_t(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_49
# BB#48:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	sym_t(%rip), %r14
.LBB86_49:                              # %gc_protect_sym.exit
	movq	$sym_t, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	testq	%r14, %r14
	je	.LBB86_51
# BB#50:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB86_52
.LBB86_51:                              # %.critedge.i
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB86_52:                              # %setvar.exit
	movq	%r14, 16(%r14)
	movl	$.L.str.56, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB86_54
# BB#53:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB86_55
.LBB86_54:                              # %.critedge.i32
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB86_55:                              # %setvar.exit34
	movq	$0, 16(%rbx)
	movl	$.L.str.57, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	movl	$.L.str.58, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB86_57
# BB#56:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB86_58
.LBB86_57:                              # %.critedge.i37
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB86_58:                              # %setvar.exit39
	movq	%r14, 16(%rbx)
	movl	$.L.str.59, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	movl	$.L.str.60, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB86_60
# BB#59:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB86_61
.LBB86_60:                              # %.critedge.i42
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB86_61:                              # %setvar.exit44
	movq	%r14, 16(%rbx)
	movl	$.L.str.61, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	movl	$.L.str.62, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB86_63
# BB#62:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB86_64
.LBB86_63:                              # %.critedge.i47
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB86_64:                              # %setvar.exit49
	movq	%r14, 16(%rbx)
	movl	$.L.str.63, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	movq	%r14, sym_errobj(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_66
# BB#65:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	sym_errobj(%rip), %r14
.LBB86_66:                              # %gc_protect_sym.exit51
	movq	$sym_errobj, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	testq	%r14, %r14
	je	.LBB86_68
# BB#67:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB86_69
.LBB86_68:                              # %.critedge.i53
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB86_69:                              # %setvar.exit55
	movq	$0, 16(%r14)
	movl	$.L.str.64, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, sym_catchall(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_71
# BB#70:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_71:                              # %gc_protect_sym.exit57
	movq	$sym_catchall, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movl	$.L.str.65, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, sym_progn(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_73
# BB#72:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_73:                              # %gc_protect_sym.exit58
	movq	$sym_progn, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movl	$.L.str.66, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, sym_lambda(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_75
# BB#74:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_75:                              # %gc_protect_sym.exit59
	movq	$sym_lambda, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movl	$.L.str.67, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, sym_quote(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_77
# BB#76:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_77:                              # %gc_protect_sym.exit60
	movq	$sym_quote, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movl	$.L.str.68, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, sym_dot(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_79
# BB#78:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_79:                              # %gc_protect_sym.exit61
	movq	$sym_dot, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	movl	$.L.str.69, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	movq	%r14, sym_after_gc(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_81
# BB#80:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	sym_after_gc(%rip), %r14
.LBB86_81:                              # %gc_protect_sym.exit62
	movq	$sym_after_gc, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	testq	%r14, %r14
	je	.LBB86_83
# BB#82:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB86_84
.LBB86_83:                              # %.critedge.i64
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB86_84:                              # %setvar.exit66
	movq	$0, 16(%r14)
	movl	$.L.str.70, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	movq	%r14, sym_eval_history_ptr(%rip)
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_86
# BB#85:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	sym_eval_history_ptr(%rip), %r14
.LBB86_86:                              # %gc_protect_sym.exit68
	movq	$sym_eval_history_ptr, (%rbx)
	movq	$1, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
	testq	%r14, %r14
	je	.LBB86_88
# BB#87:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB86_89
.LBB86_88:                              # %.critedge.i70
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB86_89:                              # %setvar.exit72
	movq	$0, 16(%r14)
	movq	inums_dim(%rip), %rax
	testq	%rax, %rax
	jle	.LBB86_106
# BB#90:
	shlq	$3, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB86_92
# BB#91:                                # %must_malloc.exit74.thread
	movq	%r14, inums(%rip)
	jmp	.LBB86_93
.LBB86_92:                              # %must_malloc.exit74
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	inums_dim(%rip), %r15
	movq	%r14, inums(%rip)
	testq	%r15, %r15
	jle	.LBB86_103
.LBB86_93:                              # %.lr.ph.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB86_94:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB86_98
# BB#95:                                #   in Loop: Header=BB86_94 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB86_97
# BB#96:                                #   in Loop: Header=BB86_94 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_97:                              #   in Loop: Header=BB86_94 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB86_101
	.p2align	4, 0x90
.LBB86_98:                              #   in Loop: Header=BB86_94 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB86_100
# BB#99:                                #   in Loop: Header=BB86_94 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB86_100:                             #   in Loop: Header=BB86_94 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB86_101:                             #   in Loop: Header=BB86_94 Depth=1
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	cvtsi2sdq	%r14, %xmm0
	movsd	%xmm0, 8(%rbx)
	movq	inums(%rip), %rax
	movq	%rbx, (%rax,%r14,8)
	incq	%r14
	movq	inums_dim(%rip), %r15
	cmpq	%r15, %r14
	jl	.LBB86_94
# BB#102:                               # %._crit_edge.loopexit
	movq	inums(%rip), %r14
.LBB86_103:                             # %._crit_edge
	movl	$24, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB86_105
# BB#104:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB86_105:                             # %gc_protect_n.exit75
	movq	%r14, (%rbx)
	movq	%r15, 8(%rbx)
	movq	protected_registers(%rip), %rax
	movq	%rax, 16(%rbx)
	movq	%rbx, protected_registers(%rip)
.LBB86_106:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end86:
	.size	init_storage_1, .Lfunc_end86-init_storage_1
	.cfi_endproc

	.globl	set_gc_hooks
	.p2align	4, 0x90
	.type	set_gc_hooks,@function
set_gc_hooks:                           # @set_gc_hooks
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi310:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi311:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi312:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi313:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi314:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi315:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi316:
	.cfi_def_cfa_offset 64
.Lcfi317:
	.cfi_offset %rbx, -56
.Lcfi318:
	.cfi_offset %r12, -48
.Lcfi319:
	.cfi_offset %r13, -40
.Lcfi320:
	.cfi_offset %r14, -32
.Lcfi321:
	.cfi_offset %r15, -24
.Lcfi322:
	.cfi_offset %rbp, -16
	movq	%r8, %rbx
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, %r14
	movq	user_types(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB87_4
# BB#1:
	movq	%r9, (%rsp)             # 8-byte Spill
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB87_3
# BB#2:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB87_3:                               # %must_malloc.exit.i
	movq	%rbp, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbp, %rdi
	callq	memset
	movq	(%rsp), %r9             # 8-byte Reload
.LBB87_4:
	cmpq	$99, %r14
	ja	.LBB87_6
# BB#5:
	leaq	(%r14,%r14,4), %rax
	shlq	$4, %rax
	addq	%rax, %rbp
	jmp	.LBB87_7
.LBB87_6:
	xorl	%ebp, %ebp
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	movq	%rbx, %r14
	movq	%r9, %rbx
	callq	err
	movq	%rbx, %r9
	movq	%r14, %rbx
.LBB87_7:                               # %get_user_type_hooks.exit
	movq	%r15, (%rbp)
	movq	%r12, 8(%rbp)
	movq	%r13, 16(%rbp)
	movq	%rbx, 24(%rbp)
	movq	gc_kind_copying(%rip), %rax
	movq	%rax, (%r9)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end87:
	.size	set_gc_hooks, .Lfunc_end87-set_gc_hooks
	.cfi_endproc

	.globl	file_gc_free
	.p2align	4, 0x90
	.type	file_gc_free,@function
file_gc_free:                           # @file_gc_free
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi323:
	.cfi_def_cfa_offset 16
.Lcfi324:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB88_2
# BB#1:
	callq	fclose
	movq	$0, 8(%rbx)
.LBB88_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB88_4
# BB#3:
	callq	free
	movq	$0, 16(%rbx)
.LBB88_4:
	popq	%rbx
	retq
.Lfunc_end88:
	.size	file_gc_free, .Lfunc_end88-file_gc_free
	.cfi_endproc

	.globl	set_print_hooks
	.p2align	4, 0x90
	.type	set_print_hooks,@function
set_print_hooks:                        # @set_print_hooks
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi325:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi326:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi327:
	.cfi_def_cfa_offset 32
.Lcfi328:
	.cfi_offset %rbx, -32
.Lcfi329:
	.cfi_offset %r14, -24
.Lcfi330:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	user_types(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB89_4
# BB#1:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB89_3
# BB#2:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB89_3:                               # %must_malloc.exit.i
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
.LBB89_4:
	cmpq	$99, %r15
	ja	.LBB89_6
# BB#5:                                 # %get_user_type_hooks.exit
	leaq	(%r15,%r15,4), %rax
	shlq	$4, %rax
	movq	%r14, 32(%rbx,%rax)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB89_6:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end89:
	.size	set_print_hooks, .Lfunc_end89-set_print_hooks
	.cfi_endproc

	.globl	file_prin1
	.p2align	4, 0x90
	.type	file_prin1,@function
file_prin1:                             # @file_prin1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi331:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi332:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi333:
	.cfi_def_cfa_offset 32
.Lcfi334:
	.cfi_offset %rbx, -32
.Lcfi335:
	.cfi_offset %r14, -24
.Lcfi336:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	16(%r15), %r14
	movq	16(%rbx), %rsi
	movl	$.L.str.149, %edi
	callq	*8(%rbx)
	movq	tkbuffer(%rip), %rdi
	movq	8(%r15), %rdx
	movl	$.L.str.150, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	tkbuffer(%rip), %rdi
	movq	16(%rbx), %rsi
	callq	*8(%rbx)
	testq	%r14, %r14
	je	.LBB90_2
# BB#1:
	movq	16(%rbx), %rsi
	movl	$.L.str.112, %edi
	callq	*8(%rbx)
	movq	16(%rbx), %rsi
	movq	%r14, %rdi
	callq	*8(%rbx)
.LBB90_2:
	movq	8(%rbx), %rax
	movq	16(%rbx), %rsi
	movl	$.L.str.118, %edi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.Lfunc_end90:
	.size	file_prin1, .Lfunc_end90-file_prin1
	.cfi_endproc

	.globl	init_subr
	.p2align	4, 0x90
	.type	init_subr,@function
init_subr:                              # @init_subr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi337:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi338:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi339:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi340:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi341:
	.cfi_def_cfa_offset 48
.Lcfi342:
	.cfi_offset %rbx, -48
.Lcfi343:
	.cfi_offset %r12, -40
.Lcfi344:
	.cfi_offset %r13, -32
.Lcfi345:
	.cfi_offset %r14, -24
.Lcfi346:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r13
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB91_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB91_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB91_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB91_7
.LBB91_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB91_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB91_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB91_7:                               # %subrcons.exit
	movw	$0, (%rbx)
	movw	%r12w, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r13, %r13
	je	.LBB91_9
# BB#8:
	movzwl	2(%r13), %eax
	cmpl	$3, %eax
	je	.LBB91_10
.LBB91_9:                               # %.critedge.i
	movl	$.L.str.92, %edi
	movq	%r13, %rsi
	callq	err
.LBB91_10:                              # %setvar.exit
	movq	%rbx, 16(%r13)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end91:
	.size	init_subr, .Lfunc_end91-init_subr
	.cfi_endproc

	.globl	init_subr_0
	.p2align	4, 0x90
	.type	init_subr_0,@function
init_subr_0:                            # @init_subr_0
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi347:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi348:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi349:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi350:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi351:
	.cfi_def_cfa_offset 48
.Lcfi352:
	.cfi_offset %rbx, -40
.Lcfi353:
	.cfi_offset %r12, -32
.Lcfi354:
	.cfi_offset %r14, -24
.Lcfi355:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB92_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB92_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB92_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB92_7
.LBB92_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB92_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB92_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB92_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB92_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB92_10
.LBB92_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB92_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end92:
	.size	init_subr_0, .Lfunc_end92-init_subr_0
	.cfi_endproc

	.globl	init_subr_1
	.p2align	4, 0x90
	.type	init_subr_1,@function
init_subr_1:                            # @init_subr_1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi356:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi357:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi358:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi359:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi360:
	.cfi_def_cfa_offset 48
.Lcfi361:
	.cfi_offset %rbx, -40
.Lcfi362:
	.cfi_offset %r12, -32
.Lcfi363:
	.cfi_offset %r14, -24
.Lcfi364:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB93_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB93_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB93_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB93_7
.LBB93_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB93_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB93_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB93_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB93_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB93_10
.LBB93_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB93_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end93:
	.size	init_subr_1, .Lfunc_end93-init_subr_1
	.cfi_endproc

	.globl	init_subr_2
	.p2align	4, 0x90
	.type	init_subr_2,@function
init_subr_2:                            # @init_subr_2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi365:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi366:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi367:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi368:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi369:
	.cfi_def_cfa_offset 48
.Lcfi370:
	.cfi_offset %rbx, -40
.Lcfi371:
	.cfi_offset %r12, -32
.Lcfi372:
	.cfi_offset %r14, -24
.Lcfi373:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB94_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB94_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB94_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB94_7
.LBB94_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB94_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB94_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB94_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB94_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB94_10
.LBB94_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB94_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end94:
	.size	init_subr_2, .Lfunc_end94-init_subr_2
	.cfi_endproc

	.globl	init_subr_2n
	.p2align	4, 0x90
	.type	init_subr_2n,@function
init_subr_2n:                           # @init_subr_2n
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi374:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi375:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi376:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi377:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi378:
	.cfi_def_cfa_offset 48
.Lcfi379:
	.cfi_offset %rbx, -40
.Lcfi380:
	.cfi_offset %r12, -32
.Lcfi381:
	.cfi_offset %r14, -24
.Lcfi382:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB95_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB95_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB95_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB95_7
.LBB95_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB95_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB95_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB95_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$21, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB95_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB95_10
.LBB95_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB95_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end95:
	.size	init_subr_2n, .Lfunc_end95-init_subr_2n
	.cfi_endproc

	.globl	init_subr_3
	.p2align	4, 0x90
	.type	init_subr_3,@function
init_subr_3:                            # @init_subr_3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi383:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi384:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi385:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi386:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi387:
	.cfi_def_cfa_offset 48
.Lcfi388:
	.cfi_offset %rbx, -40
.Lcfi389:
	.cfi_offset %r12, -32
.Lcfi390:
	.cfi_offset %r14, -24
.Lcfi391:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB96_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB96_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB96_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB96_7
.LBB96_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB96_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB96_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB96_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$7, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB96_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB96_10
.LBB96_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB96_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end96:
	.size	init_subr_3, .Lfunc_end96-init_subr_3
	.cfi_endproc

	.globl	init_subr_4
	.p2align	4, 0x90
	.type	init_subr_4,@function
init_subr_4:                            # @init_subr_4
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi392:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi393:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi394:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi395:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi396:
	.cfi_def_cfa_offset 48
.Lcfi397:
	.cfi_offset %rbx, -40
.Lcfi398:
	.cfi_offset %r12, -32
.Lcfi399:
	.cfi_offset %r14, -24
.Lcfi400:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB97_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB97_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB97_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB97_7
.LBB97_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB97_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB97_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB97_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$19, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB97_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB97_10
.LBB97_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB97_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end97:
	.size	init_subr_4, .Lfunc_end97-init_subr_4
	.cfi_endproc

	.globl	init_subr_5
	.p2align	4, 0x90
	.type	init_subr_5,@function
init_subr_5:                            # @init_subr_5
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi401:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi402:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi403:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi404:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi405:
	.cfi_def_cfa_offset 48
.Lcfi406:
	.cfi_offset %rbx, -40
.Lcfi407:
	.cfi_offset %r12, -32
.Lcfi408:
	.cfi_offset %r14, -24
.Lcfi409:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB98_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB98_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB98_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB98_7
.LBB98_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB98_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB98_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB98_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$20, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB98_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB98_10
.LBB98_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB98_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end98:
	.size	init_subr_5, .Lfunc_end98-init_subr_5
	.cfi_endproc

	.globl	init_lsubr
	.p2align	4, 0x90
	.type	init_lsubr,@function
init_lsubr:                             # @init_lsubr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi410:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi411:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi412:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi413:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi414:
	.cfi_def_cfa_offset 48
.Lcfi415:
	.cfi_offset %rbx, -40
.Lcfi416:
	.cfi_offset %r12, -32
.Lcfi417:
	.cfi_offset %r14, -24
.Lcfi418:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB99_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB99_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB99_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB99_7
.LBB99_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB99_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB99_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB99_7:                               # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$8, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB99_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB99_10
.LBB99_9:                               # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB99_10:                              # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end99:
	.size	init_lsubr, .Lfunc_end99-init_lsubr
	.cfi_endproc

	.globl	init_fsubr
	.p2align	4, 0x90
	.type	init_fsubr,@function
init_fsubr:                             # @init_fsubr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi419:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi420:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi421:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi422:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi423:
	.cfi_def_cfa_offset 48
.Lcfi424:
	.cfi_offset %rbx, -40
.Lcfi425:
	.cfi_offset %r12, -32
.Lcfi426:
	.cfi_offset %r14, -24
.Lcfi427:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB100_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB100_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB100_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB100_7
.LBB100_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB100_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB100_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB100_7:                              # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB100_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB100_10
.LBB100_9:                              # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB100_10:                             # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end100:
	.size	init_fsubr, .Lfunc_end100-init_fsubr
	.cfi_endproc

	.globl	init_msubr
	.p2align	4, 0x90
	.type	init_msubr,@function
init_msubr:                             # @init_msubr
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi428:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi429:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi430:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi431:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi432:
	.cfi_def_cfa_offset 48
.Lcfi433:
	.cfi_offset %rbx, -40
.Lcfi434:
	.cfi_offset %r12, -32
.Lcfi435:
	.cfi_offset %r14, -24
.Lcfi436:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB101_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB101_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB101_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB101_7
.LBB101_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB101_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB101_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB101_7:                              # %subrcons.exit.i
	movw	$0, (%rbx)
	movw	$10, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
	testq	%r12, %r12
	je	.LBB101_9
# BB#8:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB101_10
.LBB101_9:                              # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB101_10:                             # %init_subr.exit
	movq	%rbx, 16(%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end101:
	.size	init_msubr, .Lfunc_end101-init_msubr
	.cfi_endproc

	.globl	assq
	.p2align	4, 0x90
	.type	assq,@function
assq:                                   # @assq
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB102_1
# BB#2:                                 # %.lr.ph.preheader
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB102_3:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rcx), %eax
	cmpl	$1, %eax
	jne	.LBB102_9
# BB#4:                                 #   in Loop: Header=BB102_3 Depth=1
	movq	8(%rcx), %rax
	testq	%rax, %rax
	je	.LBB102_7
# BB#5:                                 #   in Loop: Header=BB102_3 Depth=1
	movzwl	2(%rax), %edx
	cmpl	$1, %edx
	jne	.LBB102_7
# BB#6:                                 #   in Loop: Header=BB102_3 Depth=1
	cmpq	%rdi, 8(%rax)
	je	.LBB102_10
.LBB102_7:                              # %.thread16
                                        #   in Loop: Header=BB102_3 Depth=1
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB102_3
# BB#8:
	xorl	%eax, %eax
	retq
.LBB102_1:
	xorl	%eax, %eax
	retq
.LBB102_9:
	pushq	%rax
.Lcfi437:
	.cfi_def_cfa_offset 16
	movl	$.L.str.71, %edi
	callq	err
	xorl	%eax, %eax
	addq	$8, %rsp
.LBB102_10:                             # %.thread17
	retq
.Lfunc_end102:
	.size	assq, .Lfunc_end102-assq
	.cfi_endproc

	.globl	get_user_type_hooks
	.p2align	4, 0x90
	.type	get_user_type_hooks,@function
get_user_type_hooks:                    # @get_user_type_hooks
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi438:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi439:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi440:
	.cfi_def_cfa_offset 32
.Lcfi441:
	.cfi_offset %rbx, -24
.Lcfi442:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	user_types(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB103_4
# BB#1:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB103_3
# BB#2:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB103_3:                              # %must_malloc.exit
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
.LBB103_4:
	cmpq	$99, %r14
	ja	.LBB103_6
# BB#5:
	leaq	(%r14,%r14,4), %rax
	shlq	$4, %rax
	addq	%rax, %rbx
	jmp	.LBB103_7
.LBB103_6:
	xorl	%ebx, %ebx
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.LBB103_7:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end103:
	.size	get_user_type_hooks, .Lfunc_end103-get_user_type_hooks
	.cfi_endproc

	.globl	allocate_user_tc
	.p2align	4, 0x90
	.type	allocate_user_tc,@function
allocate_user_tc:                       # @allocate_user_tc
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi443:
	.cfi_def_cfa_offset 16
.Lcfi444:
	.cfi_offset %rbx, -16
	movq	user_tc_next(%rip), %rbx
	cmpq	$101, %rbx
	movq	%rbx, %rax
	jl	.LBB104_2
# BB#1:
	movl	$.L.str.73, %edi
	xorl	%esi, %esi
	callq	err
	movq	user_tc_next(%rip), %rax
.LBB104_2:
	incq	%rax
	movq	%rax, user_tc_next(%rip)
	movq	%rbx, %rax
	popq	%rbx
	retq
.Lfunc_end104:
	.size	allocate_user_tc, .Lfunc_end104-allocate_user_tc
	.cfi_endproc

	.globl	get_newspace
	.p2align	4, 0x90
	.type	get_newspace,@function
get_newspace:                           # @get_newspace
	.cfi_startproc
# BB#0:
	movq	heaps(%rip), %rcx
	movq	(%rcx), %rax
	cmpq	%rax, heap_org(%rip)
	jne	.LBB105_2
# BB#1:
	movq	8(%rcx), %rax
.LBB105_2:
	movq	%rax, heap(%rip)
	movq	%rax, heap_org(%rip)
	movq	heap_size(%rip), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	movq	%rcx, heap_end(%rip)
	retq
.Lfunc_end105:
	.size	get_newspace, .Lfunc_end105-get_newspace
	.cfi_endproc

	.globl	scan_newspace
	.p2align	4, 0x90
	.type	scan_newspace,@function
scan_newspace:                          # @scan_newspace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi445:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi446:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi447:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi448:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi449:
	.cfi_def_cfa_offset 48
.Lcfi450:
	.cfi_offset %rbx, -48
.Lcfi451:
	.cfi_offset %r12, -40
.Lcfi452:
	.cfi_offset %r14, -32
.Lcfi453:
	.cfi_offset %r15, -24
.Lcfi454:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	cmpq	%rbx, heap(%rip)
	jbe	.LBB106_19
# BB#1:                                 # %.lr.ph.preheader
	movl	$3672052, %r12d         # imm = 0x3807F4
	movl	$2050, %r15d            # imm = 0x802
	jmp	.LBB106_2
.LBB106_8:                              #   in Loop: Header=BB106_2 Depth=1
	movslq	%eax, %rbp
	jmp	.LBB106_9
	.p2align	4, 0x90
.LBB106_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testq	%rbx, %rbx
	je	.LBB106_3
# BB#4:                                 #   in Loop: Header=BB106_2 Depth=1
	movswl	2(%rbx), %eax
	cmpl	$21, %eax
	ja	.LBB106_8
# BB#5:                                 #   in Loop: Header=BB106_2 Depth=1
	btl	%eax, %r12d
	jb	.LBB106_18
# BB#6:                                 #   in Loop: Header=BB106_2 Depth=1
	btl	%eax, %r15d
	jae	.LBB106_7
# BB#16:                                #   in Loop: Header=BB106_2 Depth=1
	movq	8(%rbx), %rdi
	callq	gc_relocate
	movq	%rax, 8(%rbx)
	jmp	.LBB106_17
	.p2align	4, 0x90
.LBB106_3:                              #   in Loop: Header=BB106_2 Depth=1
	xorl	%ebp, %ebp
.LBB106_9:                              # %.thread19
                                        #   in Loop: Header=BB106_2 Depth=1
	movq	user_types(%rip), %r14
	testq	%r14, %r14
	jne	.LBB106_13
# BB#10:                                #   in Loop: Header=BB106_2 Depth=1
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB106_12
# BB#11:                                #   in Loop: Header=BB106_2 Depth=1
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB106_12:                             # %must_malloc.exit.i
                                        #   in Loop: Header=BB106_2 Depth=1
	movq	%r14, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%r14, %rdi
	callq	memset
.LBB106_13:                             #   in Loop: Header=BB106_2 Depth=1
	cmpq	$99, %rbp
	ja	.LBB106_20
# BB#14:                                # %get_user_type_hooks.exit
                                        #   in Loop: Header=BB106_2 Depth=1
	leaq	(%rbp,%rbp,4), %rax
	shlq	$4, %rax
	movq	8(%r14,%rax), %rax
	testq	%rax, %rax
	je	.LBB106_18
# BB#15:                                #   in Loop: Header=BB106_2 Depth=1
	movq	%rbx, %rdi
	callq	*%rax
	jmp	.LBB106_18
.LBB106_7:                              #   in Loop: Header=BB106_2 Depth=1
	cmpl	$3, %eax
	jne	.LBB106_8
.LBB106_17:                             #   in Loop: Header=BB106_2 Depth=1
	movq	16(%rbx), %rdi
	callq	gc_relocate
	movq	%rax, 16(%rbx)
	.p2align	4, 0x90
.LBB106_18:                             #   in Loop: Header=BB106_2 Depth=1
	addq	$24, %rbx
	cmpq	heap(%rip), %rbx
	jb	.LBB106_2
.LBB106_19:                             # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB106_20:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end106:
	.size	scan_newspace, .Lfunc_end106-scan_newspace
	.cfi_endproc

	.globl	free_oldspace
	.p2align	4, 0x90
	.type	free_oldspace,@function
free_oldspace:                          # @free_oldspace
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi455:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi456:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi457:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi458:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi459:
	.cfi_def_cfa_offset 48
.Lcfi460:
	.cfi_offset %rbx, -48
.Lcfi461:
	.cfi_offset %r12, -40
.Lcfi462:
	.cfi_offset %r14, -32
.Lcfi463:
	.cfi_offset %r15, -24
.Lcfi464:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpq	%r14, %rbx
	jae	.LBB107_13
# BB#1:                                 # %.lr.ph.preheader
	movl	$3674110, %r12d         # imm = 0x380FFE
	jmp	.LBB107_2
.LBB107_5:                              #   in Loop: Header=BB107_2 Depth=1
	movq	user_types(%rip), %r15
	testq	%r15, %r15
	jne	.LBB107_9
# BB#6:                                 #   in Loop: Header=BB107_2 Depth=1
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB107_8
# BB#7:                                 #   in Loop: Header=BB107_2 Depth=1
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB107_8:                              # %must_malloc.exit.i
                                        #   in Loop: Header=BB107_2 Depth=1
	movq	%r15, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%r15, %rdi
	callq	memset
.LBB107_9:                              #   in Loop: Header=BB107_2 Depth=1
	movzwl	%bp, %eax
	cmpl	$99, %eax
	ja	.LBB107_14
# BB#10:                                # %get_user_type_hooks.exit
                                        #   in Loop: Header=BB107_2 Depth=1
	movslq	%ebp, %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	24(%r15,%rax), %rax
	testq	%rax, %rax
	je	.LBB107_12
# BB#11:                                #   in Loop: Header=BB107_2 Depth=1
	movq	%rbx, %rdi
	callq	*%rax
	jmp	.LBB107_12
	.p2align	4, 0x90
.LBB107_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpw	$0, (%rbx)
	jne	.LBB107_12
# BB#3:                                 #   in Loop: Header=BB107_2 Depth=1
	movswl	2(%rbx), %ebp
	cmpl	$21, %ebp
	ja	.LBB107_5
# BB#4:                                 #   in Loop: Header=BB107_2 Depth=1
	btl	%ebp, %r12d
	jae	.LBB107_5
.LBB107_12:                             #   in Loop: Header=BB107_2 Depth=1
	addq	$24, %rbx
	cmpq	%r14, %rbx
	jb	.LBB107_2
.LBB107_13:                             # %._crit_edge
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB107_14:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end107:
	.size	free_oldspace, .Lfunc_end107-free_oldspace
	.cfi_endproc

	.globl	allocate_aheap
	.p2align	4, 0x90
	.type	allocate_aheap,@function
allocate_aheap:                         # @allocate_aheap
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi465:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi466:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi467:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi468:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi469:
	.cfi_def_cfa_offset 48
.Lcfi470:
	.cfi_offset %rbx, -40
.Lcfi471:
	.cfi_offset %r12, -32
.Lcfi472:
	.cfi_offset %r14, -24
.Lcfi473:
	.cfi_offset %r15, -16
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB108_2
# BB#1:
	movl	$.L.str.77, %edi
	xorl	%esi, %esi
	callq	err
.LBB108_2:                              # %gc_kind_check.exit.preheader
	movq	nheaps(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB108_3
# BB#4:                                 # %.lr.ph32
	movq	heaps(%rip), %rdx
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB108_5:                              # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rdx,%rbx,8)
	je	.LBB108_6
# BB#19:                                # %gc_kind_check.exit
                                        #   in Loop: Header=BB108_5 Depth=1
	incq	%rbx
	cmpq	%rcx, %rbx
	jl	.LBB108_5
	jmp	.LBB108_20
.LBB108_3:
	xorl	%eax, %eax
	jmp	.LBB108_20
.LBB108_6:
	movq	nointerrupt(%rip), %r15
	movq	$1, nointerrupt(%rip)
	cmpq	$0, gc_status_flag(%rip)
	je	.LBB108_9
# BB#7:
	cmpq	$4, siod_verbose_level(%rip)
	jl	.LBB108_9
# BB#8:
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	printf
.LBB108_9:
	movq	heap_size(%rip), %r12
	leaq	(,%r12,8), %rax
	leaq	(%rax,%rax,2), %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB108_11
# BB#10:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	heap_size(%rip), %r12
.LBB108_11:                             # %must_malloc.exit
	movq	heaps(%rip), %rax
	movq	%r14, (%rax,%rbx,8)
	movq	heaps(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
	movw	$12, 2(%rcx)
	cmpq	$2, %r12
	jl	.LBB108_12
# BB#13:                                # %.lr.ph.preheader
	leaq	(%r12,%r12,2), %rdx
	leaq	(%rcx,%rdx,8), %rsi
	.p2align	4, 0x90
.LBB108_14:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	leaq	24(%rcx), %rdx
	movq	%rdx, 16(%rcx)
	movw	$12, 26(%rcx)
	addq	$48, %rcx
	cmpq	%rsi, %rcx
	movq	%rdx, %rcx
	jb	.LBB108_14
	jmp	.LBB108_15
.LBB108_12:
	movq	%rcx, %rdx
.LBB108_15:                             # %._crit_edge
	movq	freelist(%rip), %rcx
	movq	%rcx, 16(%rdx)
	movq	(%rax,%rbx,8), %rax
	movq	%rax, freelist(%rip)
	movq	%r15, nointerrupt(%rip)
	testq	%r15, %r15
	jne	.LBB108_18
# BB#16:                                # %._crit_edge
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB108_18
# BB#17:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB108_18:                             # %no_interrupt.exit
	movq	sym_t(%rip), %rax
.LBB108_20:                             # %.loopexit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end108:
	.size	allocate_aheap, .Lfunc_end108-allocate_aheap
	.cfi_endproc

	.globl	gc_kind_check
	.p2align	4, 0x90
	.type	gc_kind_check,@function
gc_kind_check:                          # @gc_kind_check
	.cfi_startproc
# BB#0:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB109_1
# BB#2:
	movl	$.L.str.77, %edi
	xorl	%esi, %esi
	jmp	err                     # TAILCALL
.LBB109_1:
	retq
.Lfunc_end109:
	.size	gc_kind_check, .Lfunc_end109-gc_kind_check
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI110_0:
	.quad	4633641066610819072     # double 60
	.text
	.globl	gc_mark_and_sweep
	.p2align	4, 0x90
	.type	gc_mark_and_sweep,@function
gc_mark_and_sweep:                      # @gc_mark_and_sweep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi474:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi475:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi476:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi477:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi478:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi479:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi480:
	.cfi_def_cfa_offset 96
.Lcfi481:
	.cfi_offset %rbx, -56
.Lcfi482:
	.cfi_offset %r12, -48
.Lcfi483:
	.cfi_offset %r13, -40
.Lcfi484:
	.cfi_offset %r14, -32
.Lcfi485:
	.cfi_offset %r15, -24
.Lcfi486:
	.cfi_offset %rbp, -16
	movq	%rsp, %rdi
	callq	times
	cvtsi2sdq	(%rsp), %xmm0
	cvtsi2sdq	8(%rsp), %xmm1
	addsd	%xmm0, %xmm1
	divsd	.LCPI110_0(%rip), %xmm1
	movsd	%xmm1, gc_rt(%rip)
	movq	$0, gc_cells_collected(%rip)
	cmpq	$0, gc_status_flag(%rip)
	je	.LBB110_3
# BB#1:
	cmpq	$4, siod_verbose_level(%rip)
	jl	.LBB110_3
# BB#2:
	movl	$.Lstr.2, %edi
	callq	puts
.LBB110_3:                              # %gc_ms_stats_start.exit.preheader
	movq	heap(%rip), %rax
	movq	heap_end(%rip), %rcx
	cmpq	%rcx, %rax
	jae	.LBB110_6
	.p2align	4, 0x90
.LBB110_4:                              # %gc_ms_stats_start.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	$786432, (%rax)         # imm = 0xC0000
	addq	$24, %rax
	cmpq	%rcx, %rax
	jb	.LBB110_4
# BB#5:                                 # %gc_ms_stats_start.exit._crit_edge
	movq	%rax, heap(%rip)
.LBB110_6:
	movl	$save_regs_gc_mark, %edi
	callq	_setjmp
	xorl	%r15d, %r15d
	movabsq	$-6148914691236517205, %r14 # imm = 0xAAAAAAAAAAAAAAAB
	movabsq	$9223372036854775800, %r13 # imm = 0x7FFFFFFFFFFFFFF8
	jmp	.LBB110_7
.LBB110_33:                             # %looks_pointerp.exit.i.i
                                        #   in Loop: Header=BB110_7 Depth=1
	callq	gc_mark
	jmp	.LBB110_34
	.p2align	4, 0x90
.LBB110_7:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB110_10 Depth 2
	movq	nheaps(%rip), %rsi
	testq	%rsi, %rsi
	jle	.LBB110_34
# BB#8:                                 # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB110_7 Depth=1
	movq	save_regs_gc_mark(,%r15,8), %rdi
	testq	%rdi, %rdi
	je	.LBB110_34
# BB#9:                                 # %.lr.ph.split.i.i.i.preheader
                                        #   in Loop: Header=BB110_7 Depth=1
	movq	heaps(%rip), %rbx
	movq	heap_size(%rip), %r8
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB110_10:                             # %.lr.ph.split.i.i.i
                                        #   Parent Loop BB110_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	cmpq	%rdi, %rax
	ja	.LBB110_15
# BB#11:                                # %.lr.ph.split.i.i.i
                                        #   in Loop: Header=BB110_10 Depth=2
	testq	%rax, %rax
	je	.LBB110_15
# BB#12:                                # %.lr.ph.split.i.i.i
                                        #   in Loop: Header=BB110_10 Depth=2
	leaq	(%r8,%r8,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB110_15
# BB#13:                                #   in Loop: Header=BB110_10 Depth=2
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%r14
	shrq	%rdx
	andq	%r13, %rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rcx
	jne	.LBB110_15
# BB#14:                                #   in Loop: Header=BB110_10 Depth=2
	movzwl	2(%rdi), %eax
	cmpl	$12, %eax
	jne	.LBB110_33
	.p2align	4, 0x90
.LBB110_15:                             #   in Loop: Header=BB110_10 Depth=2
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB110_10
.LBB110_34:                             # %looks_pointerp.exit.i.i.thread
                                        #   in Loop: Header=BB110_7 Depth=1
	incq	%r15
	cmpq	$25, %r15
	jne	.LBB110_7
# BB#16:                                # %mark_locations.exit.preheader
	movq	protected_registers(%rip), %r15
	testq	%r15, %r15
	jne	.LBB110_18
	jmp	.LBB110_22
	.p2align	4, 0x90
.LBB110_21:                             # %mark_locations.exit
                                        #   in Loop: Header=BB110_18 Depth=1
	movq	16(%r15), %r15
	testq	%r15, %r15
	je	.LBB110_22
.LBB110_18:                             # %.lr.ph22
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB110_20 Depth 2
	movq	8(%r15), %rbp
	testq	%rbp, %rbp
	jle	.LBB110_21
# BB#19:                                # %.lr.ph19.preheader
                                        #   in Loop: Header=BB110_18 Depth=1
	movq	(%r15), %rbx
	.p2align	4, 0x90
.LBB110_20:                             # %.lr.ph19
                                        #   Parent Loop BB110_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	callq	gc_mark
	addq	$8, %rbx
	decq	%rbp
	jne	.LBB110_20
	jmp	.LBB110_21
.LBB110_22:                             # %mark_protected_registers.exit
	movq	stack_start_ptr(%rip), %r9
	leaq	32(%rsp), %rax
	cmpq	%rax, %r9
	movq	%rax, %r12
	cmovaq	%r9, %r12
	cmovaq	%rax, %r9
	subq	%r9, %r12
	testq	%r12, %r12
	jle	.LBB110_37
# BB#23:                                # %.lr.ph.preheader
	sarq	$3, %r12
	xorl	%r15d, %r15d
	jmp	.LBB110_24
.LBB110_35:                             # %looks_pointerp.exit.i.i13
                                        #   in Loop: Header=BB110_24 Depth=1
	movq	%r9, %rbx
	callq	gc_mark
	movq	%rbx, %r9
	jmp	.LBB110_36
	.p2align	4, 0x90
.LBB110_24:                             # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB110_27 Depth 2
	movq	nheaps(%rip), %rsi
	testq	%rsi, %rsi
	jle	.LBB110_36
# BB#25:                                # %.lr.ph.i.i.i3
                                        #   in Loop: Header=BB110_24 Depth=1
	movq	(%r9,%r15,8), %rdi
	testq	%rdi, %rdi
	je	.LBB110_36
# BB#26:                                # %.lr.ph.split.i.i.i11.preheader
                                        #   in Loop: Header=BB110_24 Depth=1
	movq	heaps(%rip), %rbx
	movq	heap_size(%rip), %r8
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB110_27:                             # %.lr.ph.split.i.i.i11
                                        #   Parent Loop BB110_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	cmpq	%rdi, %rax
	ja	.LBB110_32
# BB#28:                                # %.lr.ph.split.i.i.i11
                                        #   in Loop: Header=BB110_27 Depth=2
	testq	%rax, %rax
	je	.LBB110_32
# BB#29:                                # %.lr.ph.split.i.i.i11
                                        #   in Loop: Header=BB110_27 Depth=2
	leaq	(%r8,%r8,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB110_32
# BB#30:                                #   in Loop: Header=BB110_27 Depth=2
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%r14
	shrq	%rdx
	andq	%r13, %rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rcx
	jne	.LBB110_32
# BB#31:                                #   in Loop: Header=BB110_27 Depth=2
	movzwl	2(%rdi), %eax
	cmpl	$12, %eax
	jne	.LBB110_35
	.p2align	4, 0x90
.LBB110_32:                             #   in Loop: Header=BB110_27 Depth=2
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB110_27
.LBB110_36:                             # %looks_pointerp.exit.i.i13.thread
                                        #   in Loop: Header=BB110_24 Depth=1
	incq	%r15
	cmpq	%r12, %r15
	jl	.LBB110_24
.LBB110_37:                             # %mark_locations.exit14
	callq	gc_sweep
	movq	%rsp, %rdi
	callq	times
	xorps	%xmm1, %xmm1
	cvtsi2sdq	(%rsp), %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2sdq	8(%rsp), %xmm0
	addsd	%xmm1, %xmm0
	divsd	.LCPI110_0(%rip), %xmm0
	subsd	gc_rt(%rip), %xmm0
	movsd	%xmm0, gc_rt(%rip)
	movsd	gc_time_taken(%rip), %xmm1 # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, gc_time_taken(%rip)
	cmpq	$0, gc_status_flag(%rip)
	je	.LBB110_40
# BB#38:                                # %mark_locations.exit14
	cmpq	$4, siod_verbose_level(%rip)
	jl	.LBB110_40
# BB#39:
	movq	gc_cells_collected(%rip), %rsi
	movl	$.L.str.76, %edi
	movb	$1, %al
	callq	printf
.LBB110_40:                             # %gc_ms_stats_end.exit
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end110:
	.size	gc_mark_and_sweep, .Lfunc_end110-gc_mark_and_sweep
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI111_0:
	.quad	4633641066610819072     # double 60
	.text
	.globl	gc_ms_stats_start
	.p2align	4, 0x90
	.type	gc_ms_stats_start,@function
gc_ms_stats_start:                      # @gc_ms_stats_start
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi487:
	.cfi_def_cfa_offset 48
	leaq	8(%rsp), %rdi
	callq	times
	cvtsi2sdq	8(%rsp), %xmm0
	cvtsi2sdq	16(%rsp), %xmm1
	addsd	%xmm0, %xmm1
	divsd	.LCPI111_0(%rip), %xmm1
	movsd	%xmm1, gc_rt(%rip)
	movq	$0, gc_cells_collected(%rip)
	cmpq	$0, gc_status_flag(%rip)
	je	.LBB111_2
# BB#1:
	cmpq	$4, siod_verbose_level(%rip)
	jl	.LBB111_2
# BB#3:
	movl	$.Lstr.2, %edi
	addq	$40, %rsp
	jmp	puts                    # TAILCALL
.LBB111_2:
	addq	$40, %rsp
	retq
.Lfunc_end111:
	.size	gc_ms_stats_start, .Lfunc_end111-gc_ms_stats_start
	.cfi_endproc

	.globl	mark_locations
	.p2align	4, 0x90
	.type	mark_locations,@function
mark_locations:                         # @mark_locations
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi488:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi489:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi490:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi491:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi492:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi493:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi494:
	.cfi_def_cfa_offset 64
.Lcfi495:
	.cfi_offset %rbx, -56
.Lcfi496:
	.cfi_offset %r12, -48
.Lcfi497:
	.cfi_offset %r13, -40
.Lcfi498:
	.cfi_offset %r14, -32
.Lcfi499:
	.cfi_offset %r15, -24
.Lcfi500:
	.cfi_offset %rbp, -16
	movq	%rdi, %r9
	cmpq	%rsi, %r9
	movq	%rsi, %r15
	cmovaq	%r9, %r15
	cmovaq	%rsi, %r9
	subq	%r9, %r15
	testq	%r15, %r15
	jle	.LBB112_13
# BB#1:                                 # %.lr.ph.preheader
	sarq	$3, %r15
	xorl	%r14d, %r14d
	movabsq	$-6148914691236517205, %r12 # imm = 0xAAAAAAAAAAAAAAAB
	movabsq	$9223372036854775800, %r13 # imm = 0x7FFFFFFFFFFFFFF8
	jmp	.LBB112_2
.LBB112_11:                             # %looks_pointerp.exit.i
                                        #   in Loop: Header=BB112_2 Depth=1
	movq	%r9, %rbx
	callq	gc_mark
	movq	%rbx, %r9
	jmp	.LBB112_12
	.p2align	4, 0x90
.LBB112_2:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB112_5 Depth 2
	movq	nheaps(%rip), %rsi
	testq	%rsi, %rsi
	jle	.LBB112_12
# BB#3:                                 # %.lr.ph.i.i
                                        #   in Loop: Header=BB112_2 Depth=1
	movq	(%r9,%r14,8), %rdi
	testq	%rdi, %rdi
	je	.LBB112_12
# BB#4:                                 # %.lr.ph.split.i.i.preheader
                                        #   in Loop: Header=BB112_2 Depth=1
	movq	heaps(%rip), %rbx
	movq	heap_size(%rip), %r8
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB112_5:                              # %.lr.ph.split.i.i
                                        #   Parent Loop BB112_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	cmpq	%rdi, %rax
	ja	.LBB112_10
# BB#6:                                 # %.lr.ph.split.i.i
                                        #   in Loop: Header=BB112_5 Depth=2
	testq	%rax, %rax
	je	.LBB112_10
# BB#7:                                 # %.lr.ph.split.i.i
                                        #   in Loop: Header=BB112_5 Depth=2
	leaq	(%r8,%r8,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB112_10
# BB#8:                                 #   in Loop: Header=BB112_5 Depth=2
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%r12
	shrq	%rdx
	andq	%r13, %rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rcx
	jne	.LBB112_10
# BB#9:                                 #   in Loop: Header=BB112_5 Depth=2
	movzwl	2(%rdi), %eax
	cmpl	$12, %eax
	jne	.LBB112_11
	.p2align	4, 0x90
.LBB112_10:                             #   in Loop: Header=BB112_5 Depth=2
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB112_5
.LBB112_12:                             # %looks_pointerp.exit.i.thread
                                        #   in Loop: Header=BB112_2 Depth=1
	incq	%r14
	cmpq	%r15, %r14
	jl	.LBB112_2
.LBB112_13:                             # %mark_locations_array.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end112:
	.size	mark_locations, .Lfunc_end112-mark_locations
	.cfi_endproc

	.globl	mark_protected_registers
	.p2align	4, 0x90
	.type	mark_protected_registers,@function
mark_protected_registers:               # @mark_protected_registers
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi501:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi502:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi503:
	.cfi_def_cfa_offset 32
.Lcfi504:
	.cfi_offset %rbx, -32
.Lcfi505:
	.cfi_offset %r14, -24
.Lcfi506:
	.cfi_offset %r15, -16
	movq	protected_registers(%rip), %r14
	testq	%r14, %r14
	jne	.LBB113_2
	jmp	.LBB113_6
	.p2align	4, 0x90
.LBB113_5:                              # %._crit_edge
                                        #   in Loop: Header=BB113_2 Depth=1
	movq	16(%r14), %r14
	testq	%r14, %r14
	je	.LBB113_6
.LBB113_2:                              # %.lr.ph20
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB113_4 Depth 2
	movq	8(%r14), %r15
	testq	%r15, %r15
	jle	.LBB113_5
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB113_2 Depth=1
	movq	(%r14), %rbx
	.p2align	4, 0x90
.LBB113_4:                              # %.lr.ph
                                        #   Parent Loop BB113_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rdi
	callq	gc_mark
	addq	$8, %rbx
	decq	%r15
	jne	.LBB113_4
	jmp	.LBB113_5
.LBB113_6:                              # %._crit_edge21
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end113:
	.size	mark_protected_registers, .Lfunc_end113-mark_protected_registers
	.cfi_endproc

	.globl	gc_sweep
	.p2align	4, 0x90
	.type	gc_sweep,@function
gc_sweep:                               # @gc_sweep
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi507:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi508:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi509:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi510:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi511:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi512:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi513:
	.cfi_def_cfa_offset 64
.Lcfi514:
	.cfi_offset %rbx, -56
.Lcfi515:
	.cfi_offset %r12, -48
.Lcfi516:
	.cfi_offset %r13, -40
.Lcfi517:
	.cfi_offset %r14, -32
.Lcfi518:
	.cfi_offset %r15, -24
.Lcfi519:
	.cfi_offset %rbp, -16
	cmpq	$0, nheaps(%rip)
	jle	.LBB114_1
# BB#2:                                 # %.lr.ph53.preheader
	xorl	%r15d, %r15d
	movl	$3678206, %ecx          # imm = 0x381FFE
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB114_3:                              # %.lr.ph53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB114_6 Depth 2
	movq	heaps(%rip), %rax
	movq	(%rax,%r15,8), %rbx
	testq	%rbx, %rbx
	je	.LBB114_19
# BB#4:                                 #   in Loop: Header=BB114_3 Depth=1
	movq	heap_size(%rip), %rax
	testq	%rax, %rax
	jle	.LBB114_19
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB114_3 Depth=1
	leaq	(%rax,%rax,2), %rax
	leaq	(%rbx,%rax,8), %rbp
	jmp	.LBB114_6
.LBB114_9:                              #   in Loop: Header=BB114_6 Depth=2
	movq	user_types(%rip), %rdx
	testq	%rdx, %rdx
	jne	.LBB114_13
# BB#10:                                #   in Loop: Header=BB114_6 Depth=2
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	testq	%rax, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	jne	.LBB114_12
# BB#11:                                #   in Loop: Header=BB114_6 Depth=2
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
	movq	(%rsp), %rax            # 8-byte Reload
.LBB114_12:                             # %must_malloc.exit
                                        #   in Loop: Header=BB114_6 Depth=2
	movq	%rax, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	memset
	movq	(%rsp), %rdx            # 8-byte Reload
	movl	$3678206, %ecx          # imm = 0x381FFE
.LBB114_13:                             #   in Loop: Header=BB114_6 Depth=2
	movzwl	%r14w, %eax
	cmpl	$99, %eax
	ja	.LBB114_21
# BB#14:                                # %get_user_type_hooks.exit
                                        #   in Loop: Header=BB114_6 Depth=2
	movslq	%r14d, %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	24(%rdx,%rax), %rax
	testq	%rax, %rax
	je	.LBB114_16
# BB#15:                                #   in Loop: Header=BB114_6 Depth=2
	movq	%rbx, %rdi
	callq	*%rax
	movl	$3678206, %ecx          # imm = 0x381FFE
	jmp	.LBB114_16
	.p2align	4, 0x90
.LBB114_6:                              # %.lr.ph
                                        #   Parent Loop BB114_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpw	$0, (%rbx)
	je	.LBB114_7
# BB#17:                                #   in Loop: Header=BB114_6 Depth=2
	movw	$0, (%rbx)
	jmp	.LBB114_18
	.p2align	4, 0x90
.LBB114_7:                              #   in Loop: Header=BB114_6 Depth=2
	movswl	2(%rbx), %r14d
	cmpl	$21, %r14d
	ja	.LBB114_9
# BB#8:                                 #   in Loop: Header=BB114_6 Depth=2
	btl	%r14d, %ecx
	jae	.LBB114_9
.LBB114_16:                             #   in Loop: Header=BB114_6 Depth=2
	incq	%r13
	movw	$12, 2(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rbx, %r12
.LBB114_18:                             #   in Loop: Header=BB114_6 Depth=2
	addq	$24, %rbx
	cmpq	%rbp, %rbx
	jb	.LBB114_6
.LBB114_19:                             # %.loopexit
                                        #   in Loop: Header=BB114_3 Depth=1
	incq	%r15
	cmpq	nheaps(%rip), %r15
	jl	.LBB114_3
	jmp	.LBB114_20
.LBB114_1:
	xorl	%r12d, %r12d
	xorl	%r13d, %r13d
.LBB114_20:                             # %._crit_edge
	movq	%r13, gc_cells_collected(%rip)
	movq	%r12, freelist(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB114_21:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end114:
	.size	gc_sweep, .Lfunc_end114-gc_sweep
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI115_0:
	.quad	4633641066610819072     # double 60
	.text
	.globl	gc_ms_stats_end
	.p2align	4, 0x90
	.type	gc_ms_stats_end,@function
gc_ms_stats_end:                        # @gc_ms_stats_end
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi520:
	.cfi_def_cfa_offset 48
	leaq	8(%rsp), %rdi
	callq	times
	cvtsi2sdq	8(%rsp), %xmm1
	cvtsi2sdq	16(%rsp), %xmm0
	addsd	%xmm1, %xmm0
	divsd	.LCPI115_0(%rip), %xmm0
	subsd	gc_rt(%rip), %xmm0
	movsd	%xmm0, gc_rt(%rip)
	movsd	gc_time_taken(%rip), %xmm1 # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, gc_time_taken(%rip)
	cmpq	$0, gc_status_flag(%rip)
	je	.LBB115_2
# BB#1:
	cmpq	$4, siod_verbose_level(%rip)
	jl	.LBB115_2
# BB#3:
	movq	gc_cells_collected(%rip), %rsi
	movl	$.L.str.76, %edi
	movb	$1, %al
	addq	$40, %rsp
	jmp	printf                  # TAILCALL
.LBB115_2:
	addq	$40, %rsp
	retq
.Lfunc_end115:
	.size	gc_ms_stats_end, .Lfunc_end115-gc_ms_stats_end
	.cfi_endproc

	.globl	gc_mark
	.p2align	4, 0x90
	.type	gc_mark,@function
gc_mark:                                # @gc_mark
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi521:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi522:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi523:
	.cfi_def_cfa_offset 32
.Lcfi524:
	.cfi_offset %rbx, -32
.Lcfi525:
	.cfi_offset %r14, -24
.Lcfi526:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB116_2
	jmp	.LBB116_15
	.p2align	4, 0x90
.LBB116_6:                              # %.backedge
                                        #   in Loop: Header=BB116_2 Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB116_15
.LBB116_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpw	$0, (%rbx)
	jne	.LBB116_15
# BB#3:                                 #   in Loop: Header=BB116_2 Depth=1
	movw	$1, (%rbx)
	movswl	2(%rbx), %r15d
	leal	-1(%r15), %eax
	cmpl	$20, %eax
	ja	.LBB116_9
# BB#4:                                 #   in Loop: Header=BB116_2 Depth=1
	jmpq	*.LJTI116_0(,%rax,8)
.LBB116_5:                              #   in Loop: Header=BB116_2 Depth=1
	movq	8(%rbx), %rdi
	callq	gc_mark
	jmp	.LBB116_6
	.p2align	4, 0x90
.LBB116_8:                              #   in Loop: Header=BB116_2 Depth=1
	movq	16(%rbx), %rdi
	addq	$8, %rbx
	callq	gc_mark
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB116_2
.LBB116_15:                             # %.loopexit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB116_9:
	movq	user_types(%rip), %r14
	testq	%r14, %r14
	jne	.LBB116_13
# BB#10:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB116_12
# BB#11:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB116_12:                             # %must_malloc.exit.i
	movq	%r14, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%r14, %rdi
	callq	memset
.LBB116_13:
	movzwl	%r15w, %eax
	cmpl	$99, %eax
	ja	.LBB116_16
# BB#14:                                # %get_user_type_hooks.exit
	movslq	%r15d, %rax
	leaq	(%rax,%rax,4), %rax
	shlq	$4, %rax
	movq	16(%r14,%rax), %rax
	testq	%rax, %rax
	je	.LBB116_15
# BB#17:
	movq	%rbx, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmpq	*%rax                   # TAILCALL
.LBB116_16:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end116:
	.size	gc_mark, .Lfunc_end116-gc_mark
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI116_0:
	.quad	.LBB116_5
	.quad	.LBB116_15
	.quad	.LBB116_6
	.quad	.LBB116_15
	.quad	.LBB116_15
	.quad	.LBB116_15
	.quad	.LBB116_15
	.quad	.LBB116_15
	.quad	.LBB116_15
	.quad	.LBB116_15
	.quad	.LBB116_8
	.quad	.LBB116_9
	.quad	.LBB116_9
	.quad	.LBB116_9
	.quad	.LBB116_9
	.quad	.LBB116_9
	.quad	.LBB116_9
	.quad	.LBB116_9
	.quad	.LBB116_15
	.quad	.LBB116_15
	.quad	.LBB116_15

	.text
	.globl	mark_locations_array
	.p2align	4, 0x90
	.type	mark_locations_array,@function
mark_locations_array:                   # @mark_locations_array
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi527:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi528:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi529:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi530:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi531:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi532:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi533:
	.cfi_def_cfa_offset 64
.Lcfi534:
	.cfi_offset %rbx, -56
.Lcfi535:
	.cfi_offset %r12, -48
.Lcfi536:
	.cfi_offset %r13, -40
.Lcfi537:
	.cfi_offset %r14, -32
.Lcfi538:
	.cfi_offset %r15, -24
.Lcfi539:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r9
	testq	%r15, %r15
	jle	.LBB117_13
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	movabsq	$-6148914691236517205, %r12 # imm = 0xAAAAAAAAAAAAAAAB
	movabsq	$9223372036854775800, %r13 # imm = 0x7FFFFFFFFFFFFFF8
	jmp	.LBB117_2
.LBB117_11:                             # %looks_pointerp.exit
                                        #   in Loop: Header=BB117_2 Depth=1
	movq	%r9, %rbx
	callq	gc_mark
	movq	%rbx, %r9
	jmp	.LBB117_12
	.p2align	4, 0x90
.LBB117_2:                              # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB117_5 Depth 2
	movq	nheaps(%rip), %rsi
	testq	%rsi, %rsi
	jle	.LBB117_12
# BB#3:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB117_2 Depth=1
	movq	(%r9,%r14,8), %rdi
	testq	%rdi, %rdi
	je	.LBB117_12
# BB#4:                                 # %.lr.ph.split.i.preheader
                                        #   in Loop: Header=BB117_2 Depth=1
	movq	heaps(%rip), %rbx
	movq	heap_size(%rip), %r8
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB117_5:                              # %.lr.ph.split.i
                                        #   Parent Loop BB117_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx,%rbp,8), %rax
	cmpq	%rdi, %rax
	ja	.LBB117_10
# BB#6:                                 # %.lr.ph.split.i
                                        #   in Loop: Header=BB117_5 Depth=2
	testq	%rax, %rax
	je	.LBB117_10
# BB#7:                                 # %.lr.ph.split.i
                                        #   in Loop: Header=BB117_5 Depth=2
	leaq	(%r8,%r8,2), %rcx
	leaq	(%rax,%rcx,8), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB117_10
# BB#8:                                 #   in Loop: Header=BB117_5 Depth=2
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%r12
	shrq	%rdx
	andq	%r13, %rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rcx
	jne	.LBB117_10
# BB#9:                                 #   in Loop: Header=BB117_5 Depth=2
	movzwl	2(%rdi), %eax
	cmpl	$12, %eax
	jne	.LBB117_11
	.p2align	4, 0x90
.LBB117_10:                             #   in Loop: Header=BB117_5 Depth=2
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB117_5
.LBB117_12:                             # %looks_pointerp.exit.thread
                                        #   in Loop: Header=BB117_2 Depth=1
	incq	%r14
	cmpq	%r15, %r14
	jl	.LBB117_2
.LBB117_13:                             # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end117:
	.size	mark_locations_array, .Lfunc_end117-mark_locations_array
	.cfi_endproc

	.globl	looks_pointerp
	.p2align	4, 0x90
	.type	looks_pointerp,@function
looks_pointerp:                         # @looks_pointerp
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi540:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi541:
	.cfi_def_cfa_offset 24
.Lcfi542:
	.cfi_offset %rbx, -24
.Lcfi543:
	.cfi_offset %r14, -16
	movq	nheaps(%rip), %r9
	testq	%r9, %r9
	jle	.LBB118_9
# BB#1:                                 # %.lr.ph
	testq	%rdi, %rdi
	je	.LBB118_10
# BB#2:                                 # %.lr.ph.split.preheader
	movq	heaps(%rip), %rcx
	movq	heap_size(%rip), %rax
	leaq	(%rax,%rax,2), %r10
	movabsq	$-6148914691236517205, %r11 # imm = 0xAAAAAAAAAAAAAAAB
	movabsq	$9223372036854775800, %r14 # imm = 0x7FFFFFFFFFFFFFF8
	movl	$1, %r8d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB118_3:                              # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rbx,8), %rax
	cmpq	%rdi, %rax
	ja	.LBB118_8
# BB#4:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB118_3 Depth=1
	testq	%rax, %rax
	je	.LBB118_8
# BB#5:                                 # %.lr.ph.split
                                        #   in Loop: Header=BB118_3 Depth=1
	leaq	(%rax,%r10,8), %rdx
	cmpq	%rdi, %rdx
	jbe	.LBB118_8
# BB#6:                                 #   in Loop: Header=BB118_3 Depth=1
	movq	%rdi, %rsi
	subq	%rax, %rsi
	movq	%rsi, %rax
	mulq	%r11
	shrq	%rdx
	andq	%r14, %rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rsi
	jne	.LBB118_8
# BB#7:                                 #   in Loop: Header=BB118_3 Depth=1
	movzwl	2(%rdi), %eax
	cmpl	$12, %eax
	jne	.LBB118_15
	.p2align	4, 0x90
.LBB118_8:                              #   in Loop: Header=BB118_3 Depth=1
	incq	%rbx
	cmpq	%r9, %rbx
	jl	.LBB118_3
	jmp	.LBB118_9
.LBB118_10:                             # %.preheader.preheader
	leaq	-1(%r9), %rcx
	movq	%r9, %rdx
	xorl	%eax, %eax
	andq	$7, %rdx
	je	.LBB118_12
	.p2align	4, 0x90
.LBB118_11:                             # %.preheader.prol
                                        # =>This Inner Loop Header: Depth=1
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB118_11
.LBB118_12:                             # %.preheader.prol.loopexit
	cmpq	$7, %rcx
	jae	.LBB118_13
.LBB118_9:
	xorl	%r8d, %r8d
.LBB118_15:                             # %.thread
	movq	%r8, %rax
	popq	%rbx
	popq	%r14
	retq
.LBB118_13:                             # %.preheader.preheader.new
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB118_14:                             # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addq	$8, %rax
	cmpq	%r9, %rax
	jl	.LBB118_14
	jmp	.LBB118_15
.Lfunc_end118:
	.size	looks_pointerp, .Lfunc_end118-looks_pointerp
	.cfi_endproc

	.globl	user_gc
	.p2align	4, 0x90
	.type	user_gc,@function
user_gc:                                # @user_gc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi544:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi545:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi546:
	.cfi_def_cfa_offset 32
.Lcfi547:
	.cfi_offset %rbx, -32
.Lcfi548:
	.cfi_offset %r14, -24
.Lcfi549:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB119_2
# BB#1:
	movl	$.L.str.77, %edi
	xorl	%esi, %esi
	callq	err
.LBB119_2:                              # %gc_kind_check.exit
	movq	nointerrupt(%rip), %r14
	movq	$1, nointerrupt(%rip)
	movq	$0, errjmp_ok(%rip)
	movq	gc_status_flag(%rip), %r15
	testq	%rbx, %rbx
	je	.LBB119_9
# BB#3:
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB119_7
# BB#4:
	cmpl	$1, %eax
	jne	.LBB119_6
# BB#5:
	cmpq	$0, 8(%rbx)
	setne	%al
	jmp	.LBB119_8
.LBB119_6:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB119_7:                              # %car.exit
	xorl	%eax, %eax
.LBB119_8:                              # %car.exit
	movzbl	%al, %eax
	movq	%rax, gc_status_flag(%rip)
.LBB119_9:
	callq	gc_mark_and_sweep
	movq	%r15, gc_status_flag(%rip)
	movq	$1, errjmp_ok(%rip)
	movq	%r14, nointerrupt(%rip)
	testq	%r14, %r14
	jne	.LBB119_12
# BB#10:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB119_12
# BB#11:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB119_12:                             # %no_interrupt.exit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end119:
	.size	user_gc, .Lfunc_end119-user_gc
	.cfi_endproc

	.globl	nactive_heaps
	.p2align	4, 0x90
	.type	nactive_heaps,@function
nactive_heaps:                          # @nactive_heaps
	.cfi_startproc
# BB#0:
	movq	nheaps(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB120_1
# BB#2:                                 # %.lr.ph
	movq	heaps(%rip), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB120_3:                              # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rdx,%rax,8)
	je	.LBB120_5
# BB#4:                                 #   in Loop: Header=BB120_3 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB120_3
.LBB120_5:                              # %.critedge
	retq
.LBB120_1:
	xorl	%eax, %eax
	retq
.Lfunc_end120:
	.size	nactive_heaps, .Lfunc_end120-nactive_heaps
	.cfi_endproc

	.globl	freelist_length
	.p2align	4, 0x90
	.type	freelist_length,@function
freelist_length:                        # @freelist_length
	.cfi_startproc
# BB#0:
	movq	freelist(%rip), %rax
	xorl	%ecx, %ecx
	testq	%rax, %rax
	je	.LBB121_3
	.p2align	4, 0x90
.LBB121_1:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	incq	%rcx
	movq	16(%rax), %rax
	testq	%rax, %rax
	jne	.LBB121_1
.LBB121_3:                              # %._crit_edge
	movq	heap_end(%rip), %rdx
	subq	heap(%rip), %rdx
	sarq	$3, %rdx
	movabsq	$-6148914691236517205, %rax # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rdx, %rax
	addq	%rcx, %rax
	retq
.Lfunc_end121:
	.size	freelist_length, .Lfunc_end121-freelist_length
	.cfi_endproc

	.globl	gc_status
	.p2align	4, 0x90
	.type	gc_status,@function
gc_status:                              # @gc_status
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi550:
	.cfi_def_cfa_offset 16
.Lcfi551:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB122_7
# BB#1:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB122_5
# BB#2:
	cmpl	$1, %ecx
	jne	.LBB122_4
# BB#3:
	cmpq	$0, 8(%rax)
	setne	%al
	jmp	.LBB122_6
.LBB122_7:                              # %thread-pre-split
	movq	gc_status_flag(%rip), %rax
	jmp	.LBB122_8
.LBB122_4:
	movl	$.L.str.27, %edi
	movq	%rax, %rsi
	callq	err
.LBB122_5:                              # %car.exit
	xorl	%eax, %eax
.LBB122_6:                              # %car.exit
	movzbl	%al, %eax
	movq	%rax, gc_status_flag(%rip)
.LBB122_8:
	movq	gc_kind_copying(%rip), %rdx
	movq	stdout(%rip), %rcx
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	cmpq	$1, %rdx
	jne	.LBB122_16
# BB#9:
	testq	%rax, %rax
	je	.LBB122_11
# BB#10:
	movl	$.L.str.78, %edi
	movl	$25, %esi
	jmp	.LBB122_12
.LBB122_16:
	testq	%rax, %rax
	je	.LBB122_18
# BB#17:
	movl	$.L.str.81, %edi
	movl	$27, %esi
	jmp	.LBB122_19
.LBB122_11:
	movl	$.L.str.79, %edi
	movl	$26, %esi
.LBB122_12:
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB122_15
# BB#13:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB122_15
# BB#14:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB122_15:                             # %put_st.exit
	movq	tkbuffer(%rip), %rdi
	movq	heap(%rip), %rdx
	movq	heap_end(%rip), %rcx
	subq	%rdx, %rcx
	subq	heap_org(%rip), %rdx
	sarq	$3, %rdx
	movabsq	$-6148914691236517205, %rax # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rax, %rdx
	sarq	$3, %rcx
	imulq	%rax, %rcx
	movl	$.L.str.80, %esi
	xorl	%eax, %eax
	callq	sprintf
	jmp	.LBB122_31
.LBB122_18:
	movl	$.L.str.82, %edi
	movl	$26, %esi
.LBB122_19:
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB122_22
# BB#20:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB122_22
# BB#21:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB122_22:                             # %put_st.exit16
	movq	nheaps(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB122_23
# BB#24:                                # %.lr.ph.i
	movq	heaps(%rip), %rax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB122_25:                             # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rax,%rdx,8)
	je	.LBB122_27
# BB#26:                                #   in Loop: Header=BB122_25 Depth=1
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB122_25
	jmp	.LBB122_27
.LBB122_23:
	xorl	%edx, %edx
.LBB122_27:                             # %nactive_heaps.exit
	movq	freelist(%rip), %rsi
	xorl	%eax, %eax
	testq	%rsi, %rsi
	je	.LBB122_30
	.p2align	4, 0x90
.LBB122_28:                             # %.lr.ph.i21
                                        # =>This Inner Loop Header: Depth=1
	incq	%rax
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB122_28
.LBB122_30:                             # %freelist_length.exit
	movq	heap_end(%rip), %rsi
	subq	heap(%rip), %rsi
	sarq	$3, %rsi
	movabsq	$-6148914691236517205, %r9 # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rsi, %r9
	addq	%rax, %r9
	movq	tkbuffer(%rip), %rdi
	movq	heap_size(%rip), %r8
	imulq	%rdx, %r8
	subq	%r9, %r8
	movl	$.L.str.83, %esi
	xorl	%eax, %eax
	callq	sprintf
.LBB122_31:                             # %freelist_length.exit
	movq	tkbuffer(%rip), %rdi
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB122_34
# BB#32:                                # %freelist_length.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB122_34
# BB#33:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB122_34:                             # %put_st.exit13
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end122:
	.size	gc_status, .Lfunc_end122-gc_status
	.cfi_endproc

	.globl	gc_info
	.p2align	4, 0x90
	.type	gc_info,@function
gc_info:                                # @gc_info
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi552:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi553:
	.cfi_def_cfa_offset 32
.Lcfi554:
	.cfi_offset %rbx, -16
	callq	get_c_long
	cmpq	$4, %rax
	ja	.LBB123_1
# BB#2:
	jmpq	*.LJTI123_0(,%rax,8)
.LBB123_3:
	xorl	%ebx, %ebx
	cmpq	$1, gc_kind_copying(%rip)
	cmoveq	sym_t(%rip), %rbx
	jmp	.LBB123_45
.LBB123_1:
	xorl	%ebx, %ebx
	jmp	.LBB123_45
.LBB123_4:
	movq	nheaps(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB123_5
# BB#6:                                 # %.lr.ph.i
	movq	heaps(%rip), %rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB123_7:                              # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rdx,%rax,8)
	je	.LBB123_9
# BB#8:                                 #   in Loop: Header=BB123_7 Depth=1
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB123_7
	jmp	.LBB123_9
.LBB123_18:
	movq	nheaps(%rip), %rcx
	jmp	.LBB123_19
.LBB123_27:
	movq	heap_size(%rip), %rcx
.LBB123_19:
	cvtsi2sdq	%rcx, %xmm2
	movq	inums_dim(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB123_14
# BB#20:
	cvttsd2si	%xmm2, %rax
	cmpq	%rdx, %rax
	jge	.LBB123_14
# BB#21:
	testq	%rcx, %rcx
	js	.LBB123_14
# BB#22:
	cvtsi2sdq	%rax, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB123_14
	jp	.LBB123_14
# BB#23:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB123_45
.LBB123_28:
	movq	gc_kind_copying(%rip), %rax
	cmpq	$1, %rax
	jne	.LBB123_30
# BB#29:
	movq	heap_end(%rip), %rcx
	movq	heap(%rip), %rbx
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	sarq	$3, %rdx
	movabsq	$-6148914691236517205, %rsi # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rdx, %rsi
	jmp	.LBB123_34
.LBB123_30:
	movq	freelist(%rip), %rcx
	xorl	%edx, %edx
	testq	%rcx, %rcx
	je	.LBB123_33
	.p2align	4, 0x90
.LBB123_31:                             # %.lr.ph.i21
                                        # =>This Inner Loop Header: Depth=1
	incq	%rdx
	movq	16(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB123_31
.LBB123_33:                             # %freelist_length.exit
	movq	heap_end(%rip), %rcx
	movq	heap(%rip), %rbx
	movq	%rcx, %rdi
	subq	%rbx, %rdi
	sarq	$3, %rdi
	movabsq	$-6148914691236517205, %rsi # imm = 0xAAAAAAAAAAAAAAAB
	imulq	%rdi, %rsi
	addq	%rdx, %rsi
.LBB123_34:
	cvtsi2sdq	%rsi, %xmm2
	movq	inums_dim(%rip), %rdi
	testq	%rdi, %rdi
	jle	.LBB123_39
# BB#35:
	cvttsd2si	%xmm2, %rdx
	cmpq	%rdi, %rdx
	jge	.LBB123_39
# BB#36:
	testq	%rsi, %rsi
	js	.LBB123_39
# BB#37:
	cvtsi2sdq	%rdx, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB123_39
	jp	.LBB123_39
# BB#38:
	movq	inums(%rip), %rax
	movq	(%rax,%rdx,8), %rbx
	jmp	.LBB123_45
.LBB123_39:
	cmpq	$1, %rax
	jne	.LBB123_24
# BB#40:
	cmpq	%rcx, %rbx
	jb	.LBB123_42
# BB#41:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB123_42:
	movq	%rbx, %rax
	addq	$24, %rax
	jmp	.LBB123_43
.LBB123_5:
	xorl	%eax, %eax
.LBB123_9:                              # %nactive_heaps.exit
	cvtsi2sdq	%rax, %xmm2
	movq	inums_dim(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB123_14
# BB#10:
	cvttsd2si	%xmm2, %rcx
	cmpq	%rdx, %rcx
	jge	.LBB123_14
# BB#11:
	testq	%rax, %rax
	js	.LBB123_14
# BB#12:
	cvtsi2sdq	%rcx, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB123_14
	jp	.LBB123_14
# BB#13:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB123_45
.LBB123_14:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB123_24
# BB#15:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB123_17
# BB#16:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB123_17:
	leaq	24(%rbx), %rax
.LBB123_43:
	movq	%rax, heap(%rip)
	jmp	.LBB123_44
.LBB123_24:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB123_26
# BB#25:
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB123_26:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB123_44:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB123_45:                             # %flocons.exit7
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end123:
	.size	gc_info, .Lfunc_end123-gc_info
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI123_0:
	.quad	.LBB123_3
	.quad	.LBB123_4
	.quad	.LBB123_18
	.quad	.LBB123_27
	.quad	.LBB123_28

	.text
	.globl	leval_args
	.p2align	4, 0x90
	.type	leval_args,@function
leval_args:                             # @leval_args
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi555:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi556:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi557:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi558:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi559:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi560:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi561:
	.cfi_def_cfa_offset 64
.Lcfi562:
	.cfi_offset %rbx, -56
.Lcfi563:
	.cfi_offset %r12, -48
.Lcfi564:
	.cfi_offset %r13, -40
.Lcfi565:
	.cfi_offset %r14, -32
.Lcfi566:
	.cfi_offset %r15, -24
.Lcfi567:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbp
	testq	%rbp, %rbp
	je	.LBB124_1
# BB#2:
	movzwl	2(%rbp), %eax
	cmpl	$1, %eax
	je	.LBB124_4
# BB#3:
	movl	$.L.str.84, %edi
	movq	%rbp, %rsi
	callq	err
.LBB124_4:
	movq	8(%rbp), %rdi
	movq	%r13, %rsi
	callq	leval
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB124_8
# BB#5:
	movq	heap(%rip), %r15
	cmpq	heap_end(%rip), %r15
	jb	.LBB124_7
# BB#6:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB124_7:
	leaq	24(%r15), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB124_11
.LBB124_1:
	xorl	%r15d, %r15d
	jmp	.LBB124_23
.LBB124_8:
	movq	freelist(%rip), %r15
	testq	%r15, %r15
	jne	.LBB124_10
# BB#9:
	callq	gc_for_newcell
	movq	freelist(%rip), %r15
.LBB124_10:
	movq	16(%r15), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB124_11:                             # %cons.exit29
	movw	$0, (%r15)
	movw	$1, 2(%r15)
	movq	%r12, 8(%r15)
	movq	$0, 16(%r15)
	movq	16(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB124_23
# BB#12:                                # %.lr.ph.preheader
	movq	%rbp, (%rsp)            # 8-byte Spill
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB124_13:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB124_22
# BB#14:                                #   in Loop: Header=BB124_13 Depth=1
	movq	8(%rbx), %rdi
	movq	%r13, %rsi
	callq	leval
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB124_18
# BB#15:                                #   in Loop: Header=BB124_13 Depth=1
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB124_17
# BB#16:                                #   in Loop: Header=BB124_13 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB124_17:                             #   in Loop: Header=BB124_13 Depth=1
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB124_21
	.p2align	4, 0x90
.LBB124_18:                             #   in Loop: Header=BB124_13 Depth=1
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB124_20
# BB#19:                                #   in Loop: Header=BB124_13 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB124_20:                             #   in Loop: Header=BB124_13 Depth=1
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB124_21:                             # %cons.exit
                                        #   in Loop: Header=BB124_13 Depth=1
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r12, 8(%rbp)
	movq	$0, 16(%rbp)
	movq	%rbp, 16(%r14)
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	movq	%rbp, %r14
	jne	.LBB124_13
	jmp	.LBB124_23
.LBB124_22:
	movl	$.L.str.84, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	err
.LBB124_23:                             # %.thread30
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end124:
	.size	leval_args, .Lfunc_end124-leval_args
	.cfi_endproc

	.globl	extend_env
	.p2align	4, 0x90
	.type	extend_env,@function
extend_env:                             # @extend_env
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi568:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi569:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi570:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi571:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi572:
	.cfi_def_cfa_offset 48
.Lcfi573:
	.cfi_offset %rbx, -48
.Lcfi574:
	.cfi_offset %r12, -40
.Lcfi575:
	.cfi_offset %r13, -32
.Lcfi576:
	.cfi_offset %r14, -24
.Lcfi577:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB125_23
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	jne	.LBB125_23
# BB#2:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB125_6
# BB#3:
	movq	heap(%rip), %r12
	cmpq	heap_end(%rip), %r12
	jb	.LBB125_5
# BB#4:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB125_5:
	leaq	24(%r12), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB125_9
.LBB125_23:                             # %.critedge
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB125_27
# BB#24:
	movq	heap(%rip), %r13
	cmpq	heap_end(%rip), %r13
	jb	.LBB125_26
# BB#25:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB125_26:
	leaq	24(%r13), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB125_30
.LBB125_27:
	movq	freelist(%rip), %r13
	testq	%r13, %r13
	jne	.LBB125_29
# BB#28:
	callq	gc_for_newcell
	movq	freelist(%rip), %r13
.LBB125_29:
	movq	16(%r13), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB125_30:                             # %cons.exit9
	movq	%rbx, %r12
	movq	%r15, %rbx
.LBB125_31:                             # %cons.exit9
	movw	$0, (%r13)
	movw	$1, 2(%r13)
	movq	%r12, 8(%r13)
	movq	%rbx, 16(%r13)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB125_35
# BB#32:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB125_34
# BB#33:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB125_34:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB125_38
.LBB125_35:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB125_37
# BB#36:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB125_37:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB125_38:                             # %cons.exit
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r13, 8(%rbx)
	movq	%r14, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB125_6:
	movq	freelist(%rip), %r12
	testq	%r12, %r12
	jne	.LBB125_8
# BB#7:
	callq	gc_for_newcell
	movq	freelist(%rip), %r12
.LBB125_8:
	movq	16(%r12), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB125_9:                              # %cons.exit15
	movw	$0, (%r12)
	movw	$1, 2(%r12)
	movq	%rbx, 8(%r12)
	movq	$0, 16(%r12)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB125_13
# BB#10:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB125_12
# BB#11:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB125_12:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB125_16
.LBB125_13:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB125_15
# BB#14:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB125_15:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB125_16:                             # %cons.exit13
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	$0, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB125_20
# BB#17:
	movq	heap(%rip), %r13
	cmpq	heap_end(%rip), %r13
	jb	.LBB125_19
# BB#18:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB125_19:
	leaq	24(%r13), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB125_31
.LBB125_20:
	movq	freelist(%rip), %r13
	testq	%r13, %r13
	jne	.LBB125_22
# BB#21:
	callq	gc_for_newcell
	movq	freelist(%rip), %r13
.LBB125_22:
	movq	16(%r13), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
	jmp	.LBB125_31
.Lfunc_end125:
	.size	extend_env, .Lfunc_end125-extend_env
	.cfi_endproc

	.globl	set_eval_hooks
	.p2align	4, 0x90
	.type	set_eval_hooks,@function
set_eval_hooks:                         # @set_eval_hooks
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi578:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi579:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi580:
	.cfi_def_cfa_offset 32
.Lcfi581:
	.cfi_offset %rbx, -32
.Lcfi582:
	.cfi_offset %r14, -24
.Lcfi583:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	user_types(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB126_4
# BB#1:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB126_3
# BB#2:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB126_3:                              # %must_malloc.exit.i
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
.LBB126_4:
	cmpq	$99, %r15
	ja	.LBB126_6
# BB#5:                                 # %get_user_type_hooks.exit
	leaq	(%r15,%r15,4), %rax
	shlq	$4, %rax
	movq	%r14, 40(%rbx,%rax)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB126_6:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end126:
	.size	set_eval_hooks, .Lfunc_end126-set_eval_hooks
	.cfi_endproc

	.globl	err_closure_code
	.p2align	4, 0x90
	.type	err_closure_code,@function
err_closure_code:                       # @err_closure_code
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi584:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	movl	$.L.str.88, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end127:
	.size	err_closure_code, .Lfunc_end127-err_closure_code
	.cfi_endproc

	.globl	lapply
	.p2align	4, 0x90
	.type	lapply,@function
lapply:                                 # @lapply
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi585:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi586:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi587:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi588:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi589:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi590:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi591:
	.cfi_def_cfa_offset 64
.Lcfi592:
	.cfi_offset %rbx, -56
.Lcfi593:
	.cfi_offset %r12, -48
.Lcfi594:
	.cfi_offset %r13, -40
.Lcfi595:
	.cfi_offset %r14, -32
.Lcfi596:
	.cfi_offset %r15, -24
.Lcfi597:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %r15
	movq	%r15, (%rsp)
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB128_2
# BB#1:
	movl	$.L.str.23, %edi
	xorl	%esi, %esi
	callq	err
	movq	(%rsp), %r15
.LBB128_2:
	testq	%r15, %r15
	je	.LBB128_6
# BB#3:
	movswl	2(%r15), %eax
	addl	$-3, %eax
	cmpl	$18, %eax
	ja	.LBB128_36
# BB#4:
	jmpq	*.LJTI128_0(,%rax,8)
.LBB128_5:
	callq	*16(%r15)
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_6:
	xorl	%ebp, %ebp
.LBB128_7:                              # %.thread.thread
	movq	user_types(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB128_11
# BB#8:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB128_10
# BB#9:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB128_10:                             # %must_malloc.exit.i
	movq	%rbx, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%rbx, %rdi
	callq	memset
.LBB128_11:
	cmpq	$99, %rbp
	ja	.LBB128_330
# BB#12:                                # %get_user_type_hooks.exit
	leaq	(%rbp,%rbp,4), %rax
	shlq	$4, %rax
	cmpq	$0, 40(%rbx,%rax)
	je	.LBB128_14
# BB#13:
	movl	$.L.str.91, %edi
	jmp	.LBB128_15
.LBB128_14:
	movl	$.L.str.90, %edi
.LBB128_15:                             # %.thread175
	movq	%r15, %rsi
	callq	err
	xorl	%r14d, %r14d
.LBB128_16:                             # %.thread175
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB128_17:
	movq	16(%r15), %rbx
	testq	%r13, %r13
	je	.LBB128_56
# BB#18:
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_56
# BB#19:
	cmpl	$1, %eax
	jne	.LBB128_55
# BB#20:
	movq	8(%r13), %rdi
	jmp	.LBB128_57
.LBB128_21:
	movq	16(%r15), %rbp
	testq	%r13, %r13
	je	.LBB128_50
# BB#22:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_59
# BB#23:
	cmpl	$1, %ecx
	jne	.LBB128_58
# BB#24:
	movq	8(%r13), %rbx
	jmp	.LBB128_60
.LBB128_25:
	movq	16(%r15), %rbp
	testq	%r13, %r13
	je	.LBB128_51
# BB#26:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_67
# BB#27:
	cmpl	$1, %ecx
	jne	.LBB128_66
# BB#28:
	movq	8(%r13), %r14
	jmp	.LBB128_68
.LBB128_29:
	movq	%r13, %rdi
	callq	*16(%r15)
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_30:
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB128_49
# BB#31:
	movswl	2(%rsi), %eax
	decl	%eax
	cmpl	$19, %eax
	ja	.LBB128_49
# BB#32:
	jmpq	*.LJTI128_1(,%rax,8)
.LBB128_33:
	movq	16(%rsi), %rbx
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_240
# BB#34:
	cmpl	$1, %eax
	jne	.LBB128_239
# BB#35:
	movq	8(%rsi), %rsi
	jmp	.LBB128_241
.LBB128_36:                             # %.thread
	movswq	2(%r15), %rbp
	jmp	.LBB128_7
.LBB128_37:
	movq	16(%r15), %rbp
	testq	%r13, %r13
	je	.LBB128_52
# BB#38:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_75
# BB#39:
	cmpl	$1, %ecx
	jne	.LBB128_74
# BB#40:
	movq	8(%r13), %r14
	jmp	.LBB128_76
.LBB128_41:
	movq	16(%r15), %rbp
	testq	%r13, %r13
	je	.LBB128_53
# BB#42:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_83
# BB#43:
	cmpl	$1, %ecx
	jne	.LBB128_82
# BB#44:
	movq	8(%r13), %r14
	jmp	.LBB128_84
.LBB128_45:
	movq	16(%r15), %rbx
	testq	%r13, %r13
	je	.LBB128_54
# BB#46:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_91
# BB#47:
	cmpl	$1, %ecx
	jne	.LBB128_90
# BB#48:
	movq	8(%r13), %r14
	jmp	.LBB128_92
.LBB128_49:                             # %.thread215
	movl	$.L.str.88, %edi
	jmp	.LBB128_15
.LBB128_50:
	xorl	%ebx, %ebx
	jmp	.LBB128_101
.LBB128_51:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	jmp	.LBB128_253
.LBB128_52:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	jmp	.LBB128_285
.LBB128_53:
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
	jmp	.LBB128_316
.LBB128_54:                             # %car.exit54.thread173
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	*%rbx
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_55:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
.LBB128_56:                             # %car.exit
	xorl	%edi, %edi
.LBB128_57:                             # %car.exit
	callq	*%rbx
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_58:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_59:
	xorl	%ebx, %ebx
.LBB128_60:
	cwtl
	testl	%eax, %eax
	je	.LBB128_101
# BB#61:
	cmpl	$1, %eax
	jne	.LBB128_98
# BB#62:                                # %cdr.exit
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_101
# BB#63:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_101
# BB#64:
	cmpl	$1, %eax
	jne	.LBB128_223
# BB#65:
	movq	8(%rsi), %rsi
	jmp	.LBB128_102
.LBB128_66:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_67:
	xorl	%r14d, %r14d
.LBB128_68:
	cwtl
	testl	%eax, %eax
	je	.LBB128_105
# BB#69:
	cmpl	$1, %eax
	jne	.LBB128_103
# BB#70:                                # %cdr.exit62
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_105
# BB#71:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_105
# BB#72:
	cmpl	$1, %eax
	jne	.LBB128_224
# BB#73:
	movq	8(%rsi), %rbx
	jmp	.LBB128_106
.LBB128_74:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_75:
	xorl	%r14d, %r14d
.LBB128_76:
	cwtl
	testl	%eax, %eax
	je	.LBB128_117
# BB#77:
	cmpl	$1, %eax
	jne	.LBB128_115
# BB#78:                                # %cdr.exit74
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_117
# BB#79:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_117
# BB#80:
	cmpl	$1, %eax
	jne	.LBB128_225
# BB#81:
	movq	8(%rsi), %r15
	jmp	.LBB128_118
.LBB128_82:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_83:
	xorl	%r14d, %r14d
.LBB128_84:
	cwtl
	testl	%eax, %eax
	je	.LBB128_129
# BB#85:
	cmpl	$1, %eax
	jne	.LBB128_127
# BB#86:                                # %cdr.exit94
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_129
# BB#87:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_129
# BB#88:
	cmpl	$1, %eax
	jne	.LBB128_226
# BB#89:
	movq	8(%rsi), %r15
	jmp	.LBB128_130
.LBB128_90:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_91:
	xorl	%r14d, %r14d
.LBB128_92:
	cwtl
	testl	%eax, %eax
	je	.LBB128_141
# BB#93:
	cmpl	$1, %eax
	jne	.LBB128_139
# BB#94:                                # %cdr.exit52
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_141
# BB#95:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_141
# BB#96:
	cmpl	$1, %eax
	jne	.LBB128_227
# BB#97:
	movq	8(%rsi), %rsi
	jmp	.LBB128_142
.LBB128_98:
	movl	$.L.str.28, %edi
.LBB128_99:                             # %car.exit48
	movq	%r13, %rsi
.LBB128_100:                            # %car.exit48
	callq	err
.LBB128_101:                            # %car.exit48
	xorl	%esi, %esi
.LBB128_102:                            # %car.exit48
	movq	%rbx, %rdi
	callq	*%rbp
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_103:                            # %car.exit64
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_104:                            # %car.exit64.thread184
	callq	err
.LBB128_105:                            # %car.exit64.thread184
	xorl	%ebx, %ebx
.LBB128_106:                            # %car.exit64.thread184
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_253
# BB#107:                               # %car.exit64.thread184
	cmpl	$1, %eax
	jne	.LBB128_251
# BB#108:                               # %cdr.exit66
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_253
# BB#109:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_253
# BB#110:
	cmpl	$1, %eax
	jne	.LBB128_228
# BB#111:                               # %cdr.exit68
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_253
# BB#112:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_253
# BB#113:
	cmpl	$1, %eax
	jne	.LBB128_318
# BB#114:
	movq	8(%rsi), %rdx
	jmp	.LBB128_254
.LBB128_115:                            # %car.exit76
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_116:                            # %car.exit76.thread197
	callq	err
.LBB128_117:                            # %car.exit76.thread197
	xorl	%r15d, %r15d
.LBB128_118:                            # %car.exit76.thread197
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_152
# BB#119:                               # %car.exit76.thread197
	cmpl	$1, %eax
	jne	.LBB128_150
# BB#120:                               # %cdr.exit78
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_152
# BB#121:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_152
# BB#122:
	cmpl	$1, %eax
	jne	.LBB128_229
# BB#123:                               # %cdr.exit80
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_152
# BB#124:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_152
# BB#125:
	cmpl	$1, %eax
	jne	.LBB128_319
# BB#126:
	movq	8(%rsi), %rbx
	jmp	.LBB128_153
.LBB128_127:                            # %car.exit96
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_128:                            # %car.exit96.thread210
	callq	err
.LBB128_129:                            # %car.exit96.thread210
	xorl	%r15d, %r15d
.LBB128_130:                            # %car.exit96.thread210
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_167
# BB#131:                               # %car.exit96.thread210
	cmpl	$1, %eax
	jne	.LBB128_165
# BB#132:                               # %cdr.exit98
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_167
# BB#133:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_167
# BB#134:
	cmpl	$1, %eax
	jne	.LBB128_230
# BB#135:                               # %cdr.exit100
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_167
# BB#136:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_167
# BB#137:
	cmpl	$1, %eax
	jne	.LBB128_320
# BB#138:
	movq	8(%rsi), %r12
	jmp	.LBB128_168
.LBB128_139:                            # %car.exit54
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_140:                            # %car.exit54.thread
	callq	err
.LBB128_141:                            # %car.exit54.thread
	xorl	%esi, %esi
.LBB128_142:                            # %car.exit54.thread
	movq	%r14, %rdi
	callq	*%rbx
	movq	%rax, %r14
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_16
# BB#143:                               # %car.exit54.thread
	cmpl	$1, %eax
	jne	.LBB128_180
# BB#144:                               # %cdr.exit56
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_16
# BB#145:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_16
# BB#146:
	cmpl	$1, %eax
	jne	.LBB128_231
# BB#147:                               # %cdr.exit58.preheader
	movq	16(%rsi), %rbx
	testq	%rbx, %rbx
	je	.LBB128_16
.LBB128_149:                            # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB128_16
# BB#148:                               # %cdr.exit58
                                        #   in Loop: Header=BB128_149 Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	*16(%r15)
	movq	%rax, %r14
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB128_149
	jmp	.LBB128_16
.LBB128_150:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_151:                            # %car.exit82.thread
	callq	err
.LBB128_152:                            # %car.exit82.thread
	xorl	%ebx, %ebx
.LBB128_153:                            # %car.exit82.thread
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_285
# BB#154:                               # %car.exit82.thread
	cmpl	$1, %eax
	jne	.LBB128_283
# BB#155:                               # %cdr.exit84
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_285
# BB#156:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_285
# BB#157:
	cmpl	$1, %eax
	jne	.LBB128_203
# BB#158:                               # %cdr.exit86
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_285
# BB#159:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_285
# BB#160:
	cmpl	$1, %eax
	jne	.LBB128_203
# BB#161:                               # %cdr.exit88
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_285
# BB#162:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_285
# BB#163:
	cmpl	$1, %eax
	jne	.LBB128_326
# BB#164:
	movq	8(%rsi), %rcx
	jmp	.LBB128_286
.LBB128_165:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_166:                            # %car.exit102.thread
	callq	err
.LBB128_167:                            # %car.exit102.thread
	xorl	%r12d, %r12d
.LBB128_168:                            # %car.exit102.thread
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_206
# BB#169:                               # %car.exit102.thread
	cmpl	$1, %eax
	jne	.LBB128_181
# BB#170:                               # %cdr.exit104
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_206
# BB#171:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_206
# BB#172:
	cmpl	$1, %eax
	jne	.LBB128_204
# BB#173:                               # %cdr.exit106
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_206
# BB#174:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_206
# BB#175:
	cmpl	$1, %eax
	jne	.LBB128_204
# BB#176:                               # %cdr.exit108
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_206
# BB#177:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_206
# BB#178:
	cmpl	$1, %eax
	jne	.LBB128_327
# BB#179:
	movq	8(%rsi), %rbx
	jmp	.LBB128_207
.LBB128_180:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
	callq	err
	jmp	.LBB128_16
.LBB128_181:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
	jmp	.LBB128_205
.LBB128_182:
	movq	8(%r15), %rdi
	callq	*16(%rsi)
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_183:
	movq	16(%rsi), %rbp
	movq	8(%r15), %rbx
	testq	%r13, %r13
	je	.LBB128_101
# BB#184:
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_101
# BB#185:
	cmpl	$1, %eax
	jne	.LBB128_242
# BB#186:
	movq	8(%r13), %rsi
	jmp	.LBB128_102
.LBB128_187:
	movq	16(%rsi), %rbp
	movq	8(%r15), %r14
	testq	%r13, %r13
	je	.LBB128_236
# BB#188:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_244
# BB#189:
	cmpl	$1, %ecx
	jne	.LBB128_243
# BB#190:
	movq	8(%r13), %rbx
	jmp	.LBB128_245
.LBB128_191:
	movq	16(%rsi), %r14
	movq	8(%r15), %rbp
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB128_232
# BB#192:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB128_194
# BB#193:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB128_194:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB128_235
.LBB128_195:
	movq	16(%rsi), %rbp
	movq	8(%r15), %r14
	testq	%r13, %r13
	je	.LBB128_237
# BB#196:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_256
# BB#197:
	cmpl	$1, %ecx
	jne	.LBB128_255
# BB#198:
	movq	8(%r13), %r15
	jmp	.LBB128_257
.LBB128_199:
	movq	16(%rsi), %rbp
	movq	8(%r15), %r14
	testq	%r13, %r13
	je	.LBB128_238
# BB#200:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB128_264
# BB#201:
	cmpl	$1, %ecx
	jne	.LBB128_263
# BB#202:
	movq	8(%r13), %r15
	jmp	.LBB128_265
.LBB128_203:
	movl	$.L.str.28, %edi
	jmp	.LBB128_284
.LBB128_204:
	movl	$.L.str.28, %edi
.LBB128_205:                            # %car.exit110.thread
	callq	err
.LBB128_206:                            # %car.exit110.thread
	xorl	%ebx, %ebx
.LBB128_207:                            # %car.exit110.thread
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#208:                               # %car.exit110.thread
	cmpl	$1, %eax
	jne	.LBB128_314
# BB#209:                               # %cdr.exit112
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_316
# BB#210:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#211:
	cmpl	$1, %eax
	jne	.LBB128_222
# BB#212:                               # %cdr.exit114
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_316
# BB#213:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#214:
	cmpl	$1, %eax
	jne	.LBB128_222
# BB#215:                               # %cdr.exit116
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_316
# BB#216:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#217:
	cmpl	$1, %eax
	jne	.LBB128_222
# BB#218:                               # %cdr.exit118
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_316
# BB#219:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#220:
	cmpl	$1, %eax
	jne	.LBB128_329
# BB#221:
	movq	8(%rsi), %r8
	jmp	.LBB128_317
.LBB128_222:
	movl	$.L.str.28, %edi
	jmp	.LBB128_315
.LBB128_223:
	movl	$.L.str.27, %edi
	jmp	.LBB128_100
.LBB128_224:
	movl	$.L.str.27, %edi
	jmp	.LBB128_104
.LBB128_225:
	movl	$.L.str.27, %edi
	jmp	.LBB128_116
.LBB128_226:
	movl	$.L.str.27, %edi
	jmp	.LBB128_128
.LBB128_227:
	movl	$.L.str.27, %edi
	jmp	.LBB128_140
.LBB128_228:
	movl	$.L.str.28, %edi
	jmp	.LBB128_252
.LBB128_229:
	movl	$.L.str.28, %edi
	jmp	.LBB128_151
.LBB128_230:
	movl	$.L.str.28, %edi
	jmp	.LBB128_166
.LBB128_231:
	movl	$.L.str.28, %edi
	callq	err
	jmp	.LBB128_16
.LBB128_232:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB128_234
# BB#233:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB128_234:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB128_235:                            # %cons.exit
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%rbp, 8(%rbx)
	movq	%r13, 16(%rbx)
	movq	%rbx, %rdi
	callq	*%r14
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_236:
	xorl	%ebx, %ebx
	jmp	.LBB128_253
.LBB128_237:
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	jmp	.LBB128_285
.LBB128_238:
	xorl	%ebx, %ebx
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	jmp	.LBB128_316
.LBB128_239:
	movl	$.L.str.27, %edi
	callq	err
.LBB128_240:                            # %car.exit124
	xorl	%esi, %esi
.LBB128_241:                            # %car.exit124
	movq	8(%r15), %rdx
	movq	%r13, %rdi
	callq	extend_env
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	leval
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_242:
	movl	$.L.str.27, %edi
	jmp	.LBB128_99
.LBB128_243:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_244:
	xorl	%ebx, %ebx
.LBB128_245:
	cwtl
	testl	%eax, %eax
	je	.LBB128_253
# BB#246:
	cmpl	$1, %eax
	jne	.LBB128_251
# BB#247:                               # %cdr.exit130
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_253
# BB#248:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_253
# BB#249:
	cmpl	$1, %eax
	jne	.LBB128_318
# BB#250:
	movq	8(%rsi), %rdx
	jmp	.LBB128_254
.LBB128_251:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_252:                            # %car.exit70
	callq	err
.LBB128_253:                            # %car.exit70
	xorl	%edx, %edx
.LBB128_254:                            # %car.exit70
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	*%rbp
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_255:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_256:
	xorl	%r15d, %r15d
.LBB128_257:
	cwtl
	testl	%eax, %eax
	je	.LBB128_273
# BB#258:
	cmpl	$1, %eax
	jne	.LBB128_271
# BB#259:                               # %cdr.exit136
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_273
# BB#260:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_273
# BB#261:
	cmpl	$1, %eax
	jne	.LBB128_322
# BB#262:
	movq	8(%rsi), %rbx
	jmp	.LBB128_274
.LBB128_263:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
	movzwl	2(%r13), %eax
.LBB128_264:
	xorl	%r15d, %r15d
.LBB128_265:
	cwtl
	testl	%eax, %eax
	je	.LBB128_289
# BB#266:
	cmpl	$1, %eax
	jne	.LBB128_287
# BB#267:                               # %cdr.exit148
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_289
# BB#268:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_289
# BB#269:
	cmpl	$1, %eax
	jne	.LBB128_323
# BB#270:
	movq	8(%rsi), %r12
	jmp	.LBB128_290
.LBB128_271:                            # %car.exit138
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_272:                            # %car.exit138.thread227
	callq	err
.LBB128_273:                            # %car.exit138.thread227
	xorl	%ebx, %ebx
.LBB128_274:                            # %car.exit138.thread227
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_285
# BB#275:                               # %car.exit138.thread227
	cmpl	$1, %eax
	jne	.LBB128_283
# BB#276:                               # %cdr.exit140
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_285
# BB#277:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_285
# BB#278:
	cmpl	$1, %eax
	jne	.LBB128_324
# BB#279:                               # %cdr.exit142
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_285
# BB#280:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_285
# BB#281:
	cmpl	$1, %eax
	jne	.LBB128_326
# BB#282:
	movq	8(%rsi), %rcx
	jmp	.LBB128_286
.LBB128_283:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_284:                            # %car.exit90
	callq	err
.LBB128_285:                            # %car.exit90
	xorl	%ecx, %ecx
.LBB128_286:                            # %car.exit90
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	*%rbp
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_287:                            # %car.exit150
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_288:                            # %car.exit150.thread240
	callq	err
.LBB128_289:                            # %car.exit150.thread240
	xorl	%r12d, %r12d
.LBB128_290:                            # %car.exit150.thread240
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_301
# BB#291:                               # %car.exit150.thread240
	cmpl	$1, %eax
	jne	.LBB128_299
# BB#292:                               # %cdr.exit152
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_301
# BB#293:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_301
# BB#294:
	cmpl	$1, %eax
	jne	.LBB128_325
# BB#295:                               # %cdr.exit154
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_301
# BB#296:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_301
# BB#297:
	cmpl	$1, %eax
	jne	.LBB128_328
# BB#298:
	movq	8(%rsi), %rbx
	jmp	.LBB128_302
.LBB128_299:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_300:                            # %car.exit156.thread
	callq	err
.LBB128_301:                            # %car.exit156.thread
	xorl	%ebx, %ebx
.LBB128_302:                            # %car.exit156.thread
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#303:                               # %car.exit156.thread
	cmpl	$1, %eax
	jne	.LBB128_314
# BB#304:                               # %cdr.exit158
	movq	16(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB128_316
# BB#305:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#306:
	cmpl	$1, %eax
	jne	.LBB128_321
# BB#307:                               # %cdr.exit160
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_316
# BB#308:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#309:
	cmpl	$1, %eax
	jne	.LBB128_321
# BB#310:                               # %cdr.exit162
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB128_316
# BB#311:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB128_316
# BB#312:
	cmpl	$1, %eax
	jne	.LBB128_329
# BB#313:
	movq	8(%rsi), %r8
	jmp	.LBB128_317
.LBB128_314:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
.LBB128_315:                            # %car.exit120
	callq	err
.LBB128_316:                            # %car.exit120
	xorl	%r8d, %r8d
.LBB128_317:                            # %car.exit120
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	*%rbp
	movq	%rax, %r14
	jmp	.LBB128_16
.LBB128_318:
	movl	$.L.str.27, %edi
	jmp	.LBB128_252
.LBB128_319:
	movl	$.L.str.27, %edi
	jmp	.LBB128_151
.LBB128_320:
	movl	$.L.str.27, %edi
	jmp	.LBB128_166
.LBB128_321:
	movl	$.L.str.28, %edi
	jmp	.LBB128_315
.LBB128_322:
	movl	$.L.str.27, %edi
	jmp	.LBB128_272
.LBB128_323:
	movl	$.L.str.27, %edi
	jmp	.LBB128_288
.LBB128_324:
	movl	$.L.str.28, %edi
	jmp	.LBB128_284
.LBB128_325:
	movl	$.L.str.28, %edi
	jmp	.LBB128_300
.LBB128_326:
	movl	$.L.str.27, %edi
	jmp	.LBB128_284
.LBB128_327:
	movl	$.L.str.27, %edi
	jmp	.LBB128_205
.LBB128_328:
	movl	$.L.str.27, %edi
	jmp	.LBB128_300
.LBB128_329:
	movl	$.L.str.27, %edi
	jmp	.LBB128_315
.LBB128_330:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.Lfunc_end128:
	.size	lapply, .Lfunc_end128-lapply
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI128_0:
	.quad	.LBB128_14
	.quad	.LBB128_5
	.quad	.LBB128_17
	.quad	.LBB128_21
	.quad	.LBB128_25
	.quad	.LBB128_29
	.quad	.LBB128_14
	.quad	.LBB128_14
	.quad	.LBB128_30
	.quad	.LBB128_36
	.quad	.LBB128_36
	.quad	.LBB128_36
	.quad	.LBB128_36
	.quad	.LBB128_36
	.quad	.LBB128_36
	.quad	.LBB128_36
	.quad	.LBB128_37
	.quad	.LBB128_41
	.quad	.LBB128_45
.LJTI128_1:
	.quad	.LBB128_33
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_182
	.quad	.LBB128_183
	.quad	.LBB128_187
	.quad	.LBB128_191
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_49
	.quad	.LBB128_195
	.quad	.LBB128_199

	.text
	.globl	leval_setq
	.p2align	4, 0x90
	.type	leval_setq,@function
leval_setq:                             # @leval_setq
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi598:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi599:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi600:
	.cfi_def_cfa_offset 32
.Lcfi601:
	.cfi_offset %rbx, -32
.Lcfi602:
	.cfi_offset %r14, -24
.Lcfi603:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB129_1
# BB#2:
	movzwl	2(%rbx), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB129_6
# BB#3:
	cmpl	$1, %ecx
	jne	.LBB129_5
# BB#4:
	movq	8(%rbx), %r15
	jmp	.LBB129_7
.LBB129_1:                              # %car.exit6.thread
	xorl	%r15d, %r15d
	xorl	%edi, %edi
	movq	%r14, %rsi
	callq	leval
	movq	%rax, %rbx
	jmp	.LBB129_20
.LBB129_5:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
	movzwl	2(%rbx), %eax
.LBB129_6:
	xorl	%r15d, %r15d
.LBB129_7:
	cwtl
	testl	%eax, %eax
	je	.LBB129_16
# BB#8:
	cmpl	$1, %eax
	jne	.LBB129_9
# BB#10:                                # %cdr.exit
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB129_16
# BB#11:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB129_16
# BB#12:
	cmpl	$1, %eax
	jne	.LBB129_14
# BB#13:
	movq	8(%rsi), %rdi
	jmp	.LBB129_17
.LBB129_9:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
.LBB129_15:                             # %car.exit6
	callq	err
.LBB129_16:                             # %car.exit6
	xorl	%edi, %edi
.LBB129_17:                             # %car.exit6
	movq	%r14, %rsi
	callq	leval
	movq	%rax, %rbx
	testq	%r15, %r15
	je	.LBB129_18
# BB#19:
	movzwl	2(%r15), %eax
	cmpl	$3, %eax
	jne	.LBB129_20
	jmp	.LBB129_21
.LBB129_18:
	xorl	%r15d, %r15d
.LBB129_20:                             # %.critedge.i
	movl	$.L.str.92, %edi
	movq	%r15, %rsi
	callq	err
.LBB129_21:                             # %setvar.exit
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	envlookup
	addq	$16, %r15
	testq	%rax, %rax
	leaq	8(%rax), %rax
	cmoveq	%r15, %rax
	movq	%rbx, (%rax)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB129_14:
	movl	$.L.str.27, %edi
	jmp	.LBB129_15
.Lfunc_end129:
	.size	leval_setq, .Lfunc_end129-leval_setq
	.cfi_endproc

	.globl	syntax_define
	.p2align	4, 0x90
	.type	syntax_define,@function
syntax_define:                          # @syntax_define
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi604:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi605:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi606:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi607:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi608:
	.cfi_def_cfa_offset 48
.Lcfi609:
	.cfi_offset %rbx, -48
.Lcfi610:
	.cfi_offset %r12, -40
.Lcfi611:
	.cfi_offset %r13, -32
.Lcfi612:
	.cfi_offset %r14, -24
.Lcfi613:
	.cfi_offset %r15, -16
	movq	%rdi, %r13
	testq	%r13, %r13
	jne	.LBB130_2
	jmp	.LBB130_63
	.p2align	4, 0x90
.LBB130_61:                             # %cons.exit26
	movw	$0, (%r13)
	movw	$1, 2(%r13)
	movq	%r14, 8(%r13)
	movq	%rbx, 16(%r13)
	testq	%r13, %r13
	je	.LBB130_63
.LBB130_2:
	movzwl	2(%r13), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB130_8
# BB#3:
	cmpl	$1, %ecx
	jne	.LBB130_4
# BB#5:                                 # %car.exit
	movq	8(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB130_8
# BB#6:                                 # %car.exit7
	movzwl	2(%rcx), %eax
	cmpl	$3, %eax
	jne	.LBB130_7
	jmp	.LBB130_62
	.p2align	4, 0x90
.LBB130_63:                             # %cdr.exit.thread36
	movq	sym_lambda(%rip), %r15
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	jmp	.LBB130_32
	.p2align	4, 0x90
.LBB130_4:
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
	callq	err
.LBB130_7:                              # %.critedge..critedge.thread_crit_edge
	movzwl	2(%r13), %eax
.LBB130_8:                              # %.critedge.thread
	cwtl
	testl	%eax, %eax
	je	.LBB130_17
# BB#9:                                 # %.critedge.thread
	cmpl	$1, %eax
	jne	.LBB130_15
# BB#10:                                # %car.exit9
	movq	8(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB130_17
# BB#11:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB130_17
# BB#12:
	cmpl	$1, %eax
	jne	.LBB130_14
# BB#13:
	movq	8(%rsi), %r14
	jmp	.LBB130_18
	.p2align	4, 0x90
.LBB130_15:                             # %car.exit11
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
.LBB130_16:                             # %car.exit11.thread
	callq	err
.LBB130_17:                             # %car.exit11.thread
	xorl	%r14d, %r14d
.LBB130_18:                             # %car.exit11.thread
	movq	sym_lambda(%rip), %r15
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB130_27
# BB#19:                                # %car.exit11.thread
	cmpl	$1, %eax
	jne	.LBB130_25
# BB#20:                                # %car.exit13
	movq	8(%r13), %rsi
	testq	%rsi, %rsi
	je	.LBB130_27
# BB#21:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB130_27
# BB#22:
	cmpl	$1, %eax
	jne	.LBB130_24
# BB#23:
	movq	16(%rsi), %r12
	jmp	.LBB130_28
	.p2align	4, 0x90
.LBB130_25:                             # %cdr.exit
	movl	$.L.str.27, %edi
	movq	%r13, %rsi
.LBB130_26:                             # %cdr.exit.thread37
	callq	err
.LBB130_27:                             # %cdr.exit.thread37
	xorl	%r12d, %r12d
.LBB130_28:                             # %cdr.exit.thread37
	movswl	2(%r13), %eax
	testl	%eax, %eax
	je	.LBB130_32
# BB#29:                                # %cdr.exit.thread37
	cmpl	$1, %eax
	jne	.LBB130_31
# BB#30:
	movq	16(%r13), %r13
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB130_34
	jmp	.LBB130_37
	.p2align	4, 0x90
.LBB130_31:
	movl	$.L.str.28, %edi
	movq	%r13, %rsi
	callq	err
.LBB130_32:                             # %cdr.exit16
	xorl	%r13d, %r13d
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB130_37
.LBB130_34:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB130_36
# BB#35:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB130_36:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB130_40
	.p2align	4, 0x90
.LBB130_37:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB130_39
# BB#38:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB130_39:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB130_40:                             # %cons.exit
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r12, 8(%rbx)
	movq	%r13, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB130_44
# BB#41:
	movq	heap(%rip), %r12
	cmpq	heap_end(%rip), %r12
	jb	.LBB130_43
# BB#42:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB130_43:
	leaq	24(%r12), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB130_47
	.p2align	4, 0x90
.LBB130_44:
	movq	freelist(%rip), %r12
	testq	%r12, %r12
	jne	.LBB130_46
# BB#45:
	callq	gc_for_newcell
	movq	freelist(%rip), %r12
.LBB130_46:
	movq	16(%r12), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB130_47:                             # %cons.exit20
	movw	$0, (%r12)
	movw	$1, 2(%r12)
	movq	%r15, 8(%r12)
	movq	%rbx, 16(%r12)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB130_51
# BB#48:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB130_50
# BB#49:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB130_50:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB130_54
	.p2align	4, 0x90
.LBB130_51:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB130_53
# BB#52:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB130_53:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB130_54:                             # %cons.exit23
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r12, 8(%rbx)
	movq	$0, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB130_58
# BB#55:
	movq	heap(%rip), %r13
	cmpq	heap_end(%rip), %r13
	jb	.LBB130_57
# BB#56:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB130_57:
	leaq	24(%r13), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB130_61
	.p2align	4, 0x90
.LBB130_58:
	movq	freelist(%rip), %r13
	testq	%r13, %r13
	jne	.LBB130_60
# BB#59:
	callq	gc_for_newcell
	movq	freelist(%rip), %r13
.LBB130_60:
	movq	16(%r13), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
	jmp	.LBB130_61
.LBB130_14:
	movl	$.L.str.27, %edi
	jmp	.LBB130_16
.LBB130_24:
	movl	$.L.str.28, %edi
	jmp	.LBB130_26
.LBB130_62:
	movq	%r13, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end130:
	.size	syntax_define, .Lfunc_end130-syntax_define
	.cfi_endproc

	.globl	leval_define
	.p2align	4, 0x90
	.type	leval_define,@function
leval_define:                           # @leval_define
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi614:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi615:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi616:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi617:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi618:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi619:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi620:
	.cfi_def_cfa_offset 64
.Lcfi621:
	.cfi_offset %rbx, -56
.Lcfi622:
	.cfi_offset %r12, -48
.Lcfi623:
	.cfi_offset %r13, -40
.Lcfi624:
	.cfi_offset %r14, -32
.Lcfi625:
	.cfi_offset %r15, -24
.Lcfi626:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	callq	syntax_define
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB131_5
# BB#1:
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB131_8
# BB#2:
	cmpl	$1, %eax
	jne	.LBB131_7
# BB#3:                                 # %car.exit
	movq	8(%rbx), %r12
	testq	%r12, %r12
	je	.LBB131_8
# BB#4:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	jne	.LBB131_9
	jmp	.LBB131_10
.LBB131_5:
	xorl	%r12d, %r12d
	movl	$.L.str.93, %edi
	xorl	%esi, %esi
	jmp	.LBB131_17
.LBB131_7:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB131_8:
	xorl	%r12d, %r12d
.LBB131_9:                              # %.thread56
	movl	$.L.str.93, %edi
	movq	%r12, %rsi
	callq	err
.LBB131_10:                             # %.thread
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB131_18
# BB#11:                                # %.thread
	cmpl	$1, %eax
	jne	.LBB131_16
# BB#12:                                # %cdr.exit
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB131_18
# BB#13:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB131_18
# BB#14:
	cmpl	$1, %eax
	jne	.LBB131_31
# BB#15:
	movq	8(%rsi), %rdi
	jmp	.LBB131_19
.LBB131_16:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
.LBB131_17:                             # %car.exit31
	callq	err
.LBB131_18:                             # %car.exit31
	xorl	%edi, %edi
.LBB131_19:                             # %car.exit31
	movq	%r15, %rsi
	callq	leval
	movq	%rax, %r14
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	envlookup
	testq	%rax, %rax
	je	.LBB131_21
# BB#20:
	movq	%r14, 8(%rax)
	jmp	.LBB131_59
.LBB131_21:
	testq	%r15, %r15
	je	.LBB131_28
# BB#22:
	movswl	2(%r15), %eax
	movb	$1, %r13b
	testl	%eax, %eax
	je	.LBB131_29
# BB#23:
	cmpl	$1, %eax
	jne	.LBB131_30
# BB#24:                                # %car.exit33
	movq	8(%r15), %r15
	testq	%r15, %r15
	je	.LBB131_29
# BB#25:
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB131_33
# BB#26:
	cmpl	$1, %eax
	jne	.LBB131_32
# BB#27:
	movq	8(%r15), %rbx
	xorl	%r13d, %r13d
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB131_35
	jmp	.LBB131_38
.LBB131_28:
	movq	%r14, 16(%r12)
	jmp	.LBB131_59
.LBB131_29:
	xorl	%r15d, %r15d
	jmp	.LBB131_34
.LBB131_30:
	movl	$.L.str.27, %edi
	movq	%r15, %rsi
	callq	err
	xorl	%r15d, %r15d
	jmp	.LBB131_34
.LBB131_31:
	movl	$.L.str.27, %edi
	jmp	.LBB131_17
.LBB131_32:
	movl	$.L.str.27, %edi
	movq	%r15, %rsi
	callq	err
.LBB131_33:                             # %car.exit35
	xorl	%r13d, %r13d
.LBB131_34:                             # %car.exit35
	xorl	%ebx, %ebx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB131_38
.LBB131_35:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB131_37
# BB#36:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB131_37:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB131_41
.LBB131_38:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB131_40
# BB#39:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB131_40:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB131_41:                             # %cons.exit
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r12, 8(%rbp)
	movq	%rbx, 16(%rbp)
	testb	%r13b, %r13b
	je	.LBB131_44
# BB#42:                                # %setcar.exit.thread51
	movl	$.L.str.29, %edi
	movq	%r15, %rsi
	callq	err
	movq	%rbp, 8(%r15)
.LBB131_43:                             # %cdr.exit38
	xorl	%ebx, %ebx
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB131_49
	jmp	.LBB131_52
.LBB131_44:
	movzwl	2(%r15), %eax
	cmpl	$1, %eax
	jne	.LBB131_46
# BB#45:                                # %setcar.exit.thread.thread
	movq	%rbp, 8(%r15)
	jmp	.LBB131_48
.LBB131_46:                             # %setcar.exit.thread
	movl	$.L.str.29, %edi
	movq	%r15, %rsi
	callq	err
	movswl	2(%r15), %eax
	movq	%rbp, 8(%r15)
	testl	%eax, %eax
	je	.LBB131_43
# BB#47:                                # %setcar.exit.thread
	cmpl	$1, %eax
	jne	.LBB131_60
.LBB131_48:
	movq	16(%r15), %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB131_52
.LBB131_49:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB131_51
# BB#50:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB131_51:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB131_55
.LBB131_52:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB131_54
# BB#53:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB131_54:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB131_55:                             # %cons.exit41
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r14, 8(%rbp)
	movq	%rbx, 16(%rbp)
	testb	%r13b, %r13b
	jne	.LBB131_57
# BB#56:
	movzwl	2(%r15), %eax
	cmpl	$1, %eax
	je	.LBB131_58
.LBB131_57:                             # %.critedge.i43
	movl	$.L.str.30, %edi
	movq	%r15, %rsi
	callq	err
.LBB131_58:                             # %setcdr.exit
	movq	%rbp, 16(%r15)
.LBB131_59:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB131_60:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
	callq	err
	jmp	.LBB131_43
.Lfunc_end131:
	.size	leval_define, .Lfunc_end131-leval_define
	.cfi_endproc

	.globl	leval_if
	.p2align	4, 0x90
	.type	leval_if,@function
leval_if:                               # @leval_if
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi627:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi628:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi629:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi630:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi631:
	.cfi_def_cfa_offset 48
.Lcfi632:
	.cfi_offset %rbx, -40
.Lcfi633:
	.cfi_offset %r12, -32
.Lcfi634:
	.cfi_offset %r14, -24
.Lcfi635:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movq	(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB132_4
# BB#1:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB132_4
# BB#2:
	cmpl	$1, %eax
	jne	.LBB132_3
# BB#6:                                 # %cdr.exit
	movq	16(%rsi), %r14
	movq	(%rbx), %r12
	testq	%r14, %r14
	je	.LBB132_5
# BB#7:
	movswl	2(%r14), %eax
	testl	%eax, %eax
	je	.LBB132_11
# BB#8:
	cmpl	$1, %eax
	jne	.LBB132_10
# BB#9:
	movq	8(%r14), %rdi
	jmp	.LBB132_12
.LBB132_3:
	movl	$.L.str.28, %edi
	callq	err
.LBB132_4:                              # %cdr.exit.thread
	movq	(%rbx), %r12
.LBB132_5:                              # %car.exit.thread21
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	movq	%r12, %rsi
	callq	leval
.LBB132_26:                             # %car.exit16
	movq	%rbx, (%r15)
	movq	sym_t(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB132_10:
	movl	$.L.str.27, %edi
	movq	%r14, %rsi
	callq	err
.LBB132_11:
	xorl	%edi, %edi
.LBB132_12:
	movq	%r12, %rsi
	callq	leval
	movswl	2(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB132_25
# BB#13:
	cmpl	$1, %ecx
	jne	.LBB132_27
# BB#14:
	testq	%rax, %rax
	movq	16(%r14), %rsi
	je	.LBB132_15
# BB#19:                                # %cdr.exit14
	testq	%rsi, %rsi
	je	.LBB132_25
.LBB132_20:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB132_25
# BB#21:
	cmpl	$1, %eax
	jne	.LBB132_23
# BB#22:
	movq	8(%rsi), %rbx
	jmp	.LBB132_26
.LBB132_27:
	movl	$.L.str.28, %edi
	movq	%r14, %rsi
.LBB132_24:                             # %car.exit16
	callq	err
.LBB132_25:                             # %car.exit16
	xorl	%ebx, %ebx
	jmp	.LBB132_26
.LBB132_15:
	testq	%rsi, %rsi
	je	.LBB132_25
# BB#16:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB132_25
# BB#17:
	cmpl	$1, %eax
	jne	.LBB132_28
# BB#18:
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	jne	.LBB132_20
	jmp	.LBB132_25
.LBB132_23:
	movl	$.L.str.27, %edi
	jmp	.LBB132_24
.LBB132_28:
	movl	$.L.str.28, %edi
	jmp	.LBB132_24
.Lfunc_end132:
	.size	leval_if, .Lfunc_end132-leval_if
	.cfi_endproc

	.globl	leval_lambda
	.p2align	4, 0x90
	.type	leval_lambda,@function
leval_lambda:                           # @leval_lambda
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi636:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi637:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi638:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi639:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi640:
	.cfi_def_cfa_offset 48
.Lcfi641:
	.cfi_offset %rbx, -48
.Lcfi642:
	.cfi_offset %r12, -40
.Lcfi643:
	.cfi_offset %r13, -32
.Lcfi644:
	.cfi_offset %r14, -24
.Lcfi645:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB133_11
# BB#1:
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB133_14
# BB#2:
	cmpl	$1, %eax
	jne	.LBB133_12
# BB#3:                                 # %cdr.exit
	movq	16(%rbx), %r15
	testq	%r15, %r15
	je	.LBB133_14
# BB#4:
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB133_14
# BB#5:
	cmpl	$1, %eax
	jne	.LBB133_24
# BB#6:                                 # %cdr.exit8
	cmpq	$0, 16(%r15)
	je	.LBB133_14
# BB#7:                                 # %cdr.exit13
	movq	sym_progn(%rip), %r13
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB133_27
# BB#8:
	movq	heap(%rip), %r12
	cmpq	heap_end(%rip), %r12
	jb	.LBB133_10
# BB#9:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB133_10:
	leaq	24(%r12), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB133_30
.LBB133_11:
	xorl	%r12d, %r12d
	jmp	.LBB133_35
.LBB133_12:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
.LBB133_13:                             # %cdr.exit8.thread.thread
	callq	err
.LBB133_14:                             # %cdr.exit8.thread.thread
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB133_20
# BB#15:                                # %cdr.exit8.thread.thread
	cmpl	$1, %eax
	jne	.LBB133_21
# BB#16:                                # %cdr.exit10
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB133_23
# BB#17:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB133_25
# BB#18:
	cmpl	$1, %eax
	jne	.LBB133_26
# BB#19:
	movq	8(%rsi), %r12
	jmp	.LBB133_31
.LBB133_20:
	xorl	%r12d, %r12d
	jmp	.LBB133_31
.LBB133_21:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
.LBB133_22:                             # %car.exit.thread
	callq	err
.LBB133_23:
	xorl	%r12d, %r12d
.LBB133_31:                             # %car.exit.thread
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB133_35
# BB#32:                                # %car.exit.thread
	cmpl	$1, %eax
	jne	.LBB133_34
# BB#33:
	movq	8(%rbx), %rbx
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB133_36
	jmp	.LBB133_39
.LBB133_34:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB133_35:                             # %car.exit16
	xorl	%ebx, %ebx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB133_39
.LBB133_36:
	movq	heap(%rip), %r15
	cmpq	heap_end(%rip), %r15
	jb	.LBB133_38
# BB#37:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB133_38:
	leaq	24(%r15), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB133_42
.LBB133_39:
	movq	freelist(%rip), %r15
	testq	%r15, %r15
	jne	.LBB133_41
# BB#40:
	callq	gc_for_newcell
	movq	freelist(%rip), %r15
.LBB133_41:
	movq	16(%r15), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB133_42:                             # %cons.exit19
	movw	$0, (%r15)
	movw	$1, 2(%r15)
	movq	%rbx, 8(%r15)
	movq	%r12, 16(%r15)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB133_46
# BB#43:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB133_45
# BB#44:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB133_45:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB133_49
.LBB133_46:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB133_48
# BB#47:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB133_48:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB133_49:                             # %closure.exit
	movw	$0, (%rbx)
	movw	$11, 2(%rbx)
	movq	%r14, 8(%rbx)
	movq	%r15, 16(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB133_24:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
	jmp	.LBB133_13
.LBB133_25:
	xorl	%r12d, %r12d
	jmp	.LBB133_31
.LBB133_26:
	movl	$.L.str.27, %edi
	jmp	.LBB133_22
.LBB133_27:
	movq	freelist(%rip), %r12
	testq	%r12, %r12
	jne	.LBB133_29
# BB#28:
	callq	gc_for_newcell
	movq	freelist(%rip), %r12
.LBB133_29:
	movq	16(%r12), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB133_30:                             # %car.exit
	movw	$0, (%r12)
	movw	$1, 2(%r12)
	movq	%r13, 8(%r12)
	movq	%r15, 16(%r12)
	jmp	.LBB133_31
.Lfunc_end133:
	.size	leval_lambda, .Lfunc_end133-leval_lambda
	.cfi_endproc

	.globl	arglchk
	.p2align	4, 0x90
	.type	arglchk,@function
arglchk:                                # @arglchk
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	retq
.Lfunc_end134:
	.size	arglchk, .Lfunc_end134-arglchk
	.cfi_endproc

	.globl	leval_progn
	.p2align	4, 0x90
	.type	leval_progn,@function
leval_progn:                            # @leval_progn
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi646:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi647:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi648:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi649:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi650:
	.cfi_def_cfa_offset 48
.Lcfi651:
	.cfi_offset %rbx, -40
.Lcfi652:
	.cfi_offset %r12, -32
.Lcfi653:
	.cfi_offset %r14, -24
.Lcfi654:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%rsi), %r15
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB135_1
# BB#2:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB135_1
# BB#3:
	cmpl	$1, %eax
	jne	.LBB135_4
# BB#5:                                 # %cdr.exit
	movq	16(%rsi), %r12
	testq	%r12, %r12
	je	.LBB135_1
# BB#6:
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB135_10
# BB#7:
	cmpl	$1, %eax
	jne	.LBB135_9
# BB#8:
	movq	16(%r12), %rax
	jmp	.LBB135_11
.LBB135_1:
	xorl	%r12d, %r12d
	jmp	.LBB135_10
.LBB135_4:
	movl	$.L.str.28, %edi
	callq	err
	xorl	%r12d, %r12d
	jmp	.LBB135_10
.LBB135_9:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
	callq	err
.LBB135_10:                             # %cdr.exit16.preheader
	xorl	%eax, %eax
	jmp	.LBB135_11
	.p2align	4, 0x90
.LBB135_20:                             #   in Loop: Header=BB135_11 Depth=1
	movq	16(%rbx), %rax
	movq	%rbx, %r12
.LBB135_11:                             # %cdr.exit16
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB135_16
# BB#12:                                #   in Loop: Header=BB135_11 Depth=1
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB135_16
# BB#13:                                #   in Loop: Header=BB135_11 Depth=1
	cmpl	$1, %eax
	jne	.LBB135_15
# BB#14:                                #   in Loop: Header=BB135_11 Depth=1
	movq	8(%r12), %rdi
	testq	%rbx, %rbx
	jne	.LBB135_18
	jmp	.LBB135_22
	.p2align	4, 0x90
.LBB135_15:                             #   in Loop: Header=BB135_11 Depth=1
	movl	$.L.str.27, %edi
	movq	%r12, %rsi
	callq	err
.LBB135_16:                             # %car.exit
                                        #   in Loop: Header=BB135_11 Depth=1
	xorl	%edi, %edi
	testq	%rbx, %rbx
	je	.LBB135_22
.LBB135_18:                             #   in Loop: Header=BB135_11 Depth=1
	movq	%r15, %rsi
	callq	leval
	movswl	2(%rbx), %ecx
	testl	%ecx, %ecx
	movq	%rbx, %r12
	movl	$0, %eax
	je	.LBB135_11
# BB#19:                                #   in Loop: Header=BB135_11 Depth=1
	cmpl	$1, %ecx
	je	.LBB135_20
# BB#21:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
	movq	%rbx, %r12
	jmp	.LBB135_10
.LBB135_22:
	movq	%rdi, (%r14)
	movq	sym_t(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end135:
	.size	leval_progn, .Lfunc_end135-leval_progn
	.cfi_endproc

	.globl	leval_or
	.p2align	4, 0x90
	.type	leval_or,@function
leval_or:                               # @leval_or
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi655:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi656:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi657:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi658:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi659:
	.cfi_def_cfa_offset 48
.Lcfi660:
	.cfi_offset %rbx, -40
.Lcfi661:
	.cfi_offset %r12, -32
.Lcfi662:
	.cfi_offset %r14, -24
.Lcfi663:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%rsi), %r15
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB136_1
# BB#2:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB136_1
# BB#3:
	cmpl	$1, %eax
	jne	.LBB136_4
# BB#5:                                 # %cdr.exit
	movq	16(%rsi), %r12
	testq	%r12, %r12
	je	.LBB136_1
# BB#6:
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB136_10
# BB#7:
	cmpl	$1, %eax
	jne	.LBB136_9
# BB#8:
	movq	16(%r12), %rax
	jmp	.LBB136_11
.LBB136_1:
	xorl	%r12d, %r12d
	jmp	.LBB136_10
.LBB136_4:
	movl	$.L.str.28, %edi
	callq	err
	xorl	%r12d, %r12d
	jmp	.LBB136_10
.LBB136_9:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
	callq	err
.LBB136_10:                             # %cdr.exit23.preheader
	xorl	%eax, %eax
	jmp	.LBB136_11
	.p2align	4, 0x90
.LBB136_22:                             #   in Loop: Header=BB136_11 Depth=1
	movq	16(%rbx), %rax
	movq	%rbx, %r12
.LBB136_11:                             # %cdr.exit23
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB136_16
# BB#12:                                #   in Loop: Header=BB136_11 Depth=1
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB136_16
# BB#13:                                #   in Loop: Header=BB136_11 Depth=1
	cmpl	$1, %eax
	jne	.LBB136_15
# BB#14:                                #   in Loop: Header=BB136_11 Depth=1
	movq	8(%r12), %rdi
	testq	%rbx, %rbx
	jne	.LBB136_18
	jmp	.LBB136_24
	.p2align	4, 0x90
.LBB136_15:                             #   in Loop: Header=BB136_11 Depth=1
	movl	$.L.str.27, %edi
	movq	%r12, %rsi
	callq	err
.LBB136_16:                             # %car.exit
                                        #   in Loop: Header=BB136_11 Depth=1
	xorl	%edi, %edi
	testq	%rbx, %rbx
	je	.LBB136_24
.LBB136_18:                             #   in Loop: Header=BB136_11 Depth=1
	movq	%r15, %rsi
	callq	leval
	testq	%rax, %rax
	jne	.LBB136_19
# BB#20:                                #   in Loop: Header=BB136_11 Depth=1
	movswl	2(%rbx), %ecx
	testl	%ecx, %ecx
	movq	%rbx, %r12
	movl	$0, %eax
	je	.LBB136_11
# BB#21:                                #   in Loop: Header=BB136_11 Depth=1
	cmpl	$1, %ecx
	je	.LBB136_22
# BB#23:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
	movq	%rbx, %r12
	jmp	.LBB136_10
.LBB136_24:
	movq	%rdi, (%r14)
	movq	sym_t(%rip), %rax
	jmp	.LBB136_25
.LBB136_19:
	movq	%rax, (%r14)
	xorl	%eax, %eax
.LBB136_25:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end136:
	.size	leval_or, .Lfunc_end136-leval_or
	.cfi_endproc

	.globl	leval_and
	.p2align	4, 0x90
	.type	leval_and,@function
leval_and:                              # @leval_and
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi664:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi665:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi666:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi667:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi668:
	.cfi_def_cfa_offset 48
.Lcfi669:
	.cfi_offset %rbx, -40
.Lcfi670:
	.cfi_offset %r12, -32
.Lcfi671:
	.cfi_offset %r14, -24
.Lcfi672:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	(%r15), %rax
	testq	%rax, %rax
	je	.LBB137_18
# BB#1:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB137_18
# BB#2:
	cmpl	$1, %ecx
	jne	.LBB137_17
# BB#3:                                 # %cdr.exit
	movq	16(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB137_18
# BB#4:
	movq	(%rsi), %r14
	.p2align	4, 0x90
.LBB137_5:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%rbx), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB137_9
# BB#6:                                 # %.preheader
                                        #   in Loop: Header=BB137_5 Depth=1
	cmpl	$1, %ecx
	jne	.LBB137_8
# BB#7:                                 #   in Loop: Header=BB137_5 Depth=1
	movq	16(%rbx), %r12
	jmp	.LBB137_10
	.p2align	4, 0x90
.LBB137_8:                              #   in Loop: Header=BB137_5 Depth=1
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
	movzwl	2(%rbx), %eax
.LBB137_9:                              #   in Loop: Header=BB137_5 Depth=1
	xorl	%r12d, %r12d
.LBB137_10:                             #   in Loop: Header=BB137_5 Depth=1
	cwtl
	testl	%eax, %eax
	je	.LBB137_14
# BB#11:                                #   in Loop: Header=BB137_5 Depth=1
	cmpl	$1, %eax
	jne	.LBB137_13
# BB#12:                                #   in Loop: Header=BB137_5 Depth=1
	movq	8(%rbx), %rdi
	testq	%r12, %r12
	jne	.LBB137_15
	jmp	.LBB137_21
	.p2align	4, 0x90
.LBB137_13:                             #   in Loop: Header=BB137_5 Depth=1
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB137_14:                             # %car.exit
                                        #   in Loop: Header=BB137_5 Depth=1
	xorl	%edi, %edi
	testq	%r12, %r12
	je	.LBB137_21
.LBB137_15:                             #   in Loop: Header=BB137_5 Depth=1
	movq	%r14, %rsi
	callq	leval
	testq	%rax, %rax
	movq	%r12, %rbx
	jne	.LBB137_5
# BB#16:
	movq	$0, (%r15)
	jmp	.LBB137_19
.LBB137_17:
	movl	$.L.str.28, %edi
	movq	%rax, %rsi
	callq	err
.LBB137_18:                             # %cdr.exit.thread
	movq	sym_t(%rip), %rax
	movq	%rax, (%r15)
.LBB137_19:
	xorl	%eax, %eax
.LBB137_20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB137_21:
	movq	%rdi, (%r15)
	movq	sym_t(%rip), %rax
	jmp	.LBB137_20
.Lfunc_end137:
	.size	leval_and, .Lfunc_end137-leval_and
	.cfi_endproc

	.globl	leval_catch_1
	.p2align	4, 0x90
	.type	leval_catch_1,@function
leval_catch_1:                          # @leval_catch_1
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi673:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi674:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi675:
	.cfi_def_cfa_offset 32
.Lcfi676:
	.cfi_offset %rbx, -32
.Lcfi677:
	.cfi_offset %r14, -24
.Lcfi678:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB138_1
	.p2align	4, 0x90
.LBB138_3:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB138_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB138_3 Depth=1
	cmpl	$1, %eax
	jne	.LBB138_6
# BB#5:                                 #   in Loop: Header=BB138_3 Depth=1
	movq	8(%rbx), %rdi
	jmp	.LBB138_8
	.p2align	4, 0x90
.LBB138_6:                              #   in Loop: Header=BB138_3 Depth=1
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB138_7:                              #   in Loop: Header=BB138_3 Depth=1
	xorl	%edi, %edi
.LBB138_8:                              #   in Loop: Header=BB138_3 Depth=1
	movq	%r14, %rsi
	callq	leval
	movq	%rax, %r15
	movswl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB138_9
# BB#2:                                 # %cdr.exit.backedge
                                        #   in Loop: Header=BB138_3 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB138_3
	jmp	.LBB138_11
.LBB138_1:
	xorl	%r15d, %r15d
	jmp	.LBB138_11
.LBB138_9:
	testl	%eax, %eax
	je	.LBB138_11
# BB#10:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
.LBB138_11:                             # %cdr.exit._crit_edge
	movq	catch_framep(%rip), %rax
	movq	216(%rax), %rax
	movq	%rax, catch_framep(%rip)
	movq	%r15, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end138:
	.size	leval_catch_1, .Lfunc_end138-leval_catch_1
	.cfi_endproc

	.globl	leval_catch
	.p2align	4, 0x90
	.type	leval_catch,@function
leval_catch:                            # @leval_catch
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi679:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi680:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi681:
	.cfi_def_cfa_offset 32
	subq	$224, %rsp
.Lcfi682:
	.cfi_def_cfa_offset 256
.Lcfi683:
	.cfi_offset %rbx, -32
.Lcfi684:
	.cfi_offset %r14, -24
.Lcfi685:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB139_5
# BB#1:
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB139_5
# BB#2:
	cmpl	$1, %eax
	jne	.LBB139_4
# BB#3:
	movq	8(%rbx), %rdi
	jmp	.LBB139_6
.LBB139_4:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB139_5:                              # %car.exit
	xorl	%edi, %edi
.LBB139_6:                              # %car.exit
	movq	%r14, %rsi
	callq	leval
	movq	%rax, (%rsp)
	movq	catch_framep(%rip), %rax
	movq	%rax, 216(%rsp)
	leaq	16(%rsp), %rdi
	callq	_setjmp
	movq	%rsp, %rcx
	movq	%rcx, catch_framep(%rip)
	cmpl	$2, %eax
	jne	.LBB139_8
# BB#7:
	movq	216(%rsp), %rax
	movq	%rax, catch_framep(%rip)
	movq	8(%rsp), %r15
	jmp	.LBB139_26
.LBB139_8:
	testq	%rbx, %rbx
	je	.LBB139_20
# BB#9:
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB139_20
# BB#10:
	cmpl	$1, %eax
	jne	.LBB139_21
# BB#11:                                # %cdr.exit
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB139_22
	.p2align	4, 0x90
.LBB139_12:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB139_16
# BB#13:                                # %.lr.ph.i
                                        #   in Loop: Header=BB139_12 Depth=1
	cmpl	$1, %eax
	jne	.LBB139_15
# BB#14:                                #   in Loop: Header=BB139_12 Depth=1
	movq	8(%rbx), %rdi
	jmp	.LBB139_17
	.p2align	4, 0x90
.LBB139_15:                             #   in Loop: Header=BB139_12 Depth=1
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB139_16:                             #   in Loop: Header=BB139_12 Depth=1
	xorl	%edi, %edi
.LBB139_17:                             #   in Loop: Header=BB139_12 Depth=1
	movq	%r14, %rsi
	callq	leval
	movq	%rax, %r15
	movswl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB139_23
# BB#18:                                # %cdr.exit.backedge.i
                                        #   in Loop: Header=BB139_12 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB139_12
	jmp	.LBB139_25
.LBB139_20:
	xorl	%r15d, %r15d
	jmp	.LBB139_25
.LBB139_21:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
.LBB139_22:
	xorl	%r15d, %r15d
.LBB139_25:                             # %leval_catch_1.exit
	movq	catch_framep(%rip), %rax
	movq	216(%rax), %rax
	movq	%rax, catch_framep(%rip)
.LBB139_26:
	movq	%r15, %rax
	addq	$224, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB139_23:
	testl	%eax, %eax
	je	.LBB139_25
# BB#24:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
	jmp	.LBB139_25
.Lfunc_end139:
	.size	leval_catch, .Lfunc_end139-leval_catch
	.cfi_endproc

	.globl	lthrow
	.p2align	4, 0x90
	.type	lthrow,@function
lthrow:                                 # @lthrow
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi686:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	movq	catch_framep(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB140_5
# BB#1:                                 # %.lr.ph
	movq	sym_catchall(%rip), %rcx
	.p2align	4, 0x90
.LBB140_2:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdx
	cmpq	%rax, %rdx
	je	.LBB140_6
# BB#3:                                 #   in Loop: Header=BB140_2 Depth=1
	cmpq	%rcx, %rdx
	je	.LBB140_6
# BB#4:                                 #   in Loop: Header=BB140_2 Depth=1
	movq	216(%rdi), %rdi
	testq	%rdi, %rdi
	jne	.LBB140_2
.LBB140_5:                              # %._crit_edge
	movl	$.L.str.94, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB140_6:
	movq	%rsi, 8(%rdi)
	addq	$16, %rdi
	movl	$2, %esi
	callq	longjmp
.Lfunc_end140:
	.size	lthrow, .Lfunc_end140-lthrow
	.cfi_endproc

	.globl	leval_let
	.p2align	4, 0x90
	.type	leval_let,@function
leval_let:                              # @leval_let
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi687:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi688:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi689:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi690:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi691:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi692:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi693:
	.cfi_def_cfa_offset 64
.Lcfi694:
	.cfi_offset %rbx, -56
.Lcfi695:
	.cfi_offset %r12, -48
.Lcfi696:
	.cfi_offset %r13, -40
.Lcfi697:
	.cfi_offset %r14, -32
.Lcfi698:
	.cfi_offset %r15, -24
.Lcfi699:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB141_4
# BB#1:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB141_4
# BB#2:
	cmpl	$1, %eax
	jne	.LBB141_3
# BB#6:                                 # %cdr.exit
	movq	16(%rsi), %r15
	movq	(%r12), %rbp
	testq	%r15, %r15
	je	.LBB141_5
# BB#7:
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB141_16
# BB#8:
	cmpl	$1, %eax
	jne	.LBB141_9
# BB#10:                                # %cdr.exit13
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB141_16
# BB#11:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB141_16
# BB#12:
	cmpl	$1, %eax
	jne	.LBB141_14
# BB#13:
	movq	8(%rsi), %rdi
	jmp	.LBB141_17
.LBB141_3:
	movl	$.L.str.28, %edi
	callq	err
.LBB141_4:                              # %cdr.exit.thread
	movq	(%r12), %rbp
.LBB141_5:                              # %car.exit16
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	movq	%rbp, %rsi
	callq	leval_args
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%rbp, %rdx
	callq	extend_env
	movq	%rax, (%r12)
.LBB141_36:                             # %car.exit22
	movq	%rbx, (%r14)
	movq	sym_t(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB141_9:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
.LBB141_15:                             # %car.exit.thread31
	callq	err
.LBB141_16:                             # %car.exit.thread31
	xorl	%edi, %edi
.LBB141_17:                             # %car.exit.thread31
	movq	%rbp, %rsi
	callq	leval_args
	movq	%rax, %r13
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB141_21
# BB#18:                                # %car.exit.thread31
	cmpl	$1, %eax
	jne	.LBB141_20
# BB#19:
	movq	8(%r15), %rsi
	jmp	.LBB141_22
.LBB141_20:
	movl	$.L.str.27, %edi
	movq	%r15, %rsi
	callq	err
.LBB141_21:                             # %car.exit16.thread
	xorl	%esi, %esi
.LBB141_22:                             # %car.exit16.thread
	movq	%r13, %rdi
	movq	%rbp, %rdx
	callq	extend_env
	movq	%rax, (%r12)
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB141_35
# BB#23:                                # %car.exit16.thread
	cmpl	$1, %eax
	jne	.LBB141_24
# BB#25:                                # %cdr.exit18
	movq	16(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB141_35
# BB#26:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB141_35
# BB#27:
	cmpl	$1, %eax
	jne	.LBB141_28
# BB#29:                                # %cdr.exit20
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB141_35
# BB#30:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB141_35
# BB#31:
	cmpl	$1, %eax
	jne	.LBB141_33
# BB#32:
	movq	8(%rsi), %rbx
	jmp	.LBB141_36
.LBB141_24:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
.LBB141_34:                             # %car.exit22
	callq	err
.LBB141_35:                             # %car.exit22
	xorl	%ebx, %ebx
	jmp	.LBB141_36
.LBB141_14:
	movl	$.L.str.27, %edi
	jmp	.LBB141_15
.LBB141_28:
	movl	$.L.str.28, %edi
	jmp	.LBB141_34
.LBB141_33:
	movl	$.L.str.27, %edi
	jmp	.LBB141_34
.Lfunc_end141:
	.size	leval_let, .Lfunc_end141-leval_let
	.cfi_endproc

	.globl	letstar_macro
	.p2align	4, 0x90
	.type	letstar_macro,@function
letstar_macro:                          # @letstar_macro
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi700:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi701:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi702:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi703:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi704:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi705:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi706:
	.cfi_def_cfa_offset 64
.Lcfi707:
	.cfi_offset %rbx, -56
.Lcfi708:
	.cfi_offset %r12, -48
.Lcfi709:
	.cfi_offset %r13, -40
.Lcfi710:
	.cfi_offset %r14, -32
.Lcfi711:
	.cfi_offset %r15, -24
.Lcfi712:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB142_14
# BB#1:
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB142_17
# BB#2:
	cmpl	$1, %eax
	jne	.LBB142_15
# BB#3:                                 # %cdr.exit.i
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB142_17
# BB#4:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB142_17
# BB#5:
	cmpl	$1, %eax
	jne	.LBB142_20
# BB#6:                                 # %cadr.exit
	movq	8(%rsi), %r15
	testq	%r15, %r15
	je	.LBB142_17
# BB#7:
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB142_17
# BB#8:
	cmpl	$1, %eax
	jne	.LBB142_21
# BB#9:                                 # %cdr.exit
	cmpq	$0, 16(%r15)
	je	.LBB142_17
# BB#10:                                # %car.exit
	movq	8(%r15), %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB142_22
# BB#11:
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	jb	.LBB142_13
# BB#12:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB142_13:
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB142_25
.LBB142_14:                             # %cadr.exit.thread.thread30
	movl	$.L.str.57, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	jmp	.LBB142_18
.LBB142_15:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
.LBB142_16:                             # %.sink.split
	callq	err
.LBB142_17:                             # %.sink.split
	movl	$.L.str.57, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	je	.LBB142_19
.LBB142_18:                             # %.critedge.i29
	movl	$.L.str.29, %edi
	movq	%r12, %rsi
	callq	err
.LBB142_19:                             # %setcar.exit
	movq	%rbx, 8(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB142_20:
	movl	$.L.str.27, %edi
	jmp	.LBB142_16
.LBB142_21:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
	jmp	.LBB142_16
.LBB142_22:
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	jne	.LBB142_24
# BB#23:
	callq	gc_for_newcell
	movq	freelist(%rip), %r14
.LBB142_24:
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB142_25:
	movw	$0, (%r14)
	movw	$1, 2(%r14)
	movq	%rbx, 8(%r14)
	movq	$0, 16(%r14)
	xorl	%r13d, %r13d
	movl	$.L.str.59, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB142_29
# BB#26:
	cmpl	$1, %eax
	jne	.LBB142_28
# BB#27:
	movq	16(%r15), %r13
	jmp	.LBB142_29
.LBB142_28:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
	callq	err
	xorl	%r13d, %r13d
.LBB142_29:                             # %cdr.exit13
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB142_35
# BB#30:                                # %cdr.exit13
	cmpl	$1, %eax
	movq	%rbx, %r15
	jne	.LBB142_36
# BB#31:                                # %cdr.exit.i14
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB142_38
# BB#32:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB142_38
# BB#33:
	cmpl	$1, %eax
	jne	.LBB142_69
# BB#34:
	movq	16(%rsi), %rbp
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB142_39
	jmp	.LBB142_42
.LBB142_35:
	xorl	%ebp, %ebp
	movq	%rbx, %r15
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB142_39
	jmp	.LBB142_42
.LBB142_36:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
.LBB142_37:                             # %cddr.exit
	callq	err
.LBB142_38:                             # %cddr.exit
	xorl	%ebp, %ebp
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB142_42
.LBB142_39:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB142_41
# BB#40:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB142_41:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB142_45
.LBB142_42:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB142_44
# BB#43:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB142_44:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB142_45:                             # %cons.exit18
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r13, 8(%rbx)
	movq	%rbp, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB142_49
# BB#46:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB142_48
# BB#47:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB142_48:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB142_52
.LBB142_49:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB142_51
# BB#50:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB142_51:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB142_52:                             # %cons.exit21
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r15, 8(%rbp)
	movq	%rbx, 16(%rbp)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB142_56
# BB#53:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB142_55
# BB#54:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB142_55:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB142_59
.LBB142_56:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB142_58
# BB#57:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB142_58:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB142_59:                             # %cons.exit24
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%rbp, 8(%rbx)
	movq	$0, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB142_63
# BB#60:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB142_62
# BB#61:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB142_62:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB142_66
.LBB142_63:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB142_65
# BB#64:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB142_65:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB142_66:
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r14, 8(%rbp)
	movq	%rbx, 16(%rbp)
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	je	.LBB142_68
# BB#67:                                # %cadr.exit.thread
	movl	$.L.str.30, %edi
	movq	%r12, %rsi
	callq	err
.LBB142_68:                             # %.sink.split
	movq	%rbp, 16(%r12)
	jmp	.LBB142_17
.LBB142_69:
	movl	$.L.str.28, %edi
	jmp	.LBB142_37
.Lfunc_end142:
	.size	letstar_macro, .Lfunc_end142-letstar_macro
	.cfi_endproc

	.globl	cadr
	.p2align	4, 0x90
	.type	cadr,@function
cadr:                                   # @cadr
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi713:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB143_10
# BB#1:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB143_10
# BB#2:
	cmpl	$1, %ecx
	jne	.LBB143_3
# BB#4:                                 # %cdr.exit
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB143_10
# BB#5:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB143_10
# BB#6:
	cmpl	$1, %eax
	jne	.LBB143_8
# BB#7:
	movq	8(%rsi), %rax
	popq	%rcx
	retq
.LBB143_3:
	movl	$.L.str.28, %edi
	movq	%rax, %rsi
.LBB143_9:                              # %car.exit
	callq	err
.LBB143_10:                             # %car.exit
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB143_8:
	movl	$.L.str.27, %edi
	jmp	.LBB143_9
.Lfunc_end143:
	.size	cadr, .Lfunc_end143-cadr
	.cfi_endproc

	.globl	cddr
	.p2align	4, 0x90
	.type	cddr,@function
cddr:                                   # @cddr
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi714:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB144_10
# BB#1:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB144_10
# BB#2:
	cmpl	$1, %ecx
	jne	.LBB144_3
# BB#4:                                 # %cdr.exit
	movq	16(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB144_10
# BB#5:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB144_10
# BB#6:
	cmpl	$1, %eax
	jne	.LBB144_8
# BB#7:
	movq	16(%rsi), %rax
	popq	%rcx
	retq
.LBB144_3:
	movl	$.L.str.28, %edi
	movq	%rax, %rsi
.LBB144_9:                              # %cdr.exit2
	callq	err
.LBB144_10:                             # %cdr.exit2
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB144_8:
	movl	$.L.str.28, %edi
	jmp	.LBB144_9
.Lfunc_end144:
	.size	cddr, .Lfunc_end144-cddr
	.cfi_endproc

	.globl	letrec_macro
	.p2align	4, 0x90
	.type	letrec_macro,@function
letrec_macro:                           # @letrec_macro
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi715:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi716:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi717:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi718:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi719:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi720:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi721:
	.cfi_def_cfa_offset 64
.Lcfi722:
	.cfi_offset %rbx, -56
.Lcfi723:
	.cfi_offset %r12, -48
.Lcfi724:
	.cfi_offset %r13, -40
.Lcfi725:
	.cfi_offset %r14, -32
.Lcfi726:
	.cfi_offset %r15, -24
.Lcfi727:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB145_7
# BB#1:
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB145_10
# BB#2:
	cmpl	$1, %eax
	jne	.LBB145_8
# BB#3:                                 # %cdr.exit.i
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB145_10
# BB#4:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB145_10
# BB#5:
	cmpl	$1, %eax
	jne	.LBB145_66
# BB#6:
	movq	16(%rsi), %rbx
	jmp	.LBB145_11
.LBB145_7:
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB145_71
	jmp	.LBB145_74
.LBB145_8:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
.LBB145_9:
	callq	err
.LBB145_10:
	xorl	%ebx, %ebx
.LBB145_11:
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB145_62
# BB#12:
	cmpl	$1, %eax
	jne	.LBB145_63
# BB#13:                                # %cdr.exit.i21
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB145_62
# BB#14:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB145_62
# BB#15:
	cmpl	$1, %eax
	jne	.LBB145_68
# BB#16:                                # %cadr.exit.preheader
	movq	8(%rsi), %r14
	testq	%r14, %r14
	je	.LBB145_69
# BB#17:                                # %.lr.ph.preheader
	movq	%rbx, %rcx
	xorl	%r13d, %r13d
	jmp	.LBB145_21
	.p2align	4, 0x90
.LBB145_18:                             # %cadr.exit.backedge
                                        #   in Loop: Header=BB145_21 Depth=1
	movq	16(%r14), %r14
	testq	%r14, %r14
	movq	%rbx, %rcx
	movq	%r15, %r13
	jne	.LBB145_21
	jmp	.LBB145_70
.LBB145_19:                             #   in Loop: Header=BB145_21 Depth=1
	movl	$.L.str.27, %edi
	jmp	.LBB145_28
.LBB145_20:                             #   in Loop: Header=BB145_21 Depth=1
	movl	$.L.str.27, %edi
	jmp	.LBB145_50
	.p2align	4, 0x90
.LBB145_21:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	2(%r14), %eax
	testl	%eax, %eax
	movq	%rcx, (%rsp)            # 8-byte Spill
	je	.LBB145_29
# BB#22:                                # %.lr.ph
                                        #   in Loop: Header=BB145_21 Depth=1
	cmpl	$1, %eax
	jne	.LBB145_27
# BB#23:                                # %car.exit.i
                                        #   in Loop: Header=BB145_21 Depth=1
	movq	8(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB145_29
# BB#24:                                #   in Loop: Header=BB145_21 Depth=1
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB145_29
# BB#25:                                #   in Loop: Header=BB145_21 Depth=1
	cmpl	$1, %eax
	jne	.LBB145_19
# BB#26:                                #   in Loop: Header=BB145_21 Depth=1
	movq	8(%rsi), %rbp
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB145_30
	jmp	.LBB145_33
.LBB145_27:                             #   in Loop: Header=BB145_21 Depth=1
	movl	$.L.str.27, %edi
	movq	%r14, %rsi
.LBB145_28:                             # %caar.exit
                                        #   in Loop: Header=BB145_21 Depth=1
	callq	err
.LBB145_29:                             # %caar.exit
                                        #   in Loop: Header=BB145_21 Depth=1
	xorl	%ebp, %ebp
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB145_33
.LBB145_30:                             #   in Loop: Header=BB145_21 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB145_32
# BB#31:                                #   in Loop: Header=BB145_21 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB145_32:                             #   in Loop: Header=BB145_21 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB145_36
	.p2align	4, 0x90
.LBB145_33:                             #   in Loop: Header=BB145_21 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB145_35
# BB#34:                                #   in Loop: Header=BB145_21 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB145_35:                             #   in Loop: Header=BB145_21 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB145_36:                             # %cons.exit
                                        #   in Loop: Header=BB145_21 Depth=1
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%rbp, 8(%rbx)
	movq	$0, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB145_40
# BB#37:                                #   in Loop: Header=BB145_21 Depth=1
	movq	heap(%rip), %r15
	cmpq	heap_end(%rip), %r15
	jb	.LBB145_39
# BB#38:                                #   in Loop: Header=BB145_21 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB145_39:                             #   in Loop: Header=BB145_21 Depth=1
	leaq	24(%r15), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB145_43
	.p2align	4, 0x90
.LBB145_40:                             #   in Loop: Header=BB145_21 Depth=1
	movq	freelist(%rip), %r15
	testq	%r15, %r15
	jne	.LBB145_42
# BB#41:                                #   in Loop: Header=BB145_21 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %r15
.LBB145_42:                             #   in Loop: Header=BB145_21 Depth=1
	movq	16(%r15), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB145_43:                             #   in Loop: Header=BB145_21 Depth=1
	movw	$0, (%r15)
	movw	$1, 2(%r15)
	movq	%rbx, 8(%r15)
	movq	%r13, 16(%r15)
	movl	$.L.str.95, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbp
	movswl	2(%r14), %eax
	testl	%eax, %eax
	movl	$0, %ebx
	je	.LBB145_52
# BB#44:                                #   in Loop: Header=BB145_21 Depth=1
	cmpl	$1, %eax
	jne	.LBB145_49
# BB#45:                                # %car.exit.i27
                                        #   in Loop: Header=BB145_21 Depth=1
	movq	8(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB145_51
# BB#46:                                #   in Loop: Header=BB145_21 Depth=1
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB145_51
# BB#47:                                #   in Loop: Header=BB145_21 Depth=1
	cmpl	$1, %eax
	jne	.LBB145_20
# BB#48:                                #   in Loop: Header=BB145_21 Depth=1
	movq	8(%rsi), %rbx
	jmp	.LBB145_52
.LBB145_49:                             #   in Loop: Header=BB145_21 Depth=1
	movl	$.L.str.27, %edi
	movq	%r14, %rsi
.LBB145_50:                             # %caar.exit29
                                        #   in Loop: Header=BB145_21 Depth=1
	callq	err
.LBB145_51:                             # %caar.exit29
                                        #   in Loop: Header=BB145_21 Depth=1
	xorl	%ebx, %ebx
.LBB145_52:                             # %caar.exit29
                                        #   in Loop: Header=BB145_21 Depth=1
	movq	%r14, %rdi
	callq	cadar
	movq	%rax, %rcx
	movl	$3, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	listn
	movq	%rax, %rbp
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB145_56
# BB#53:                                #   in Loop: Header=BB145_21 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB145_55
# BB#54:                                #   in Loop: Header=BB145_21 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB145_55:                             #   in Loop: Header=BB145_21 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB145_59
	.p2align	4, 0x90
.LBB145_56:                             #   in Loop: Header=BB145_21 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB145_58
# BB#57:                                #   in Loop: Header=BB145_21 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB145_58:                             #   in Loop: Header=BB145_21 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB145_59:                             #   in Loop: Header=BB145_21 Depth=1
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%rbp, 8(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 16(%rbx)
	movswl	2(%r14), %eax
	cmpl	$1, %eax
	je	.LBB145_18
# BB#60:
	testl	%eax, %eax
	je	.LBB145_70
# BB#61:
	movl	$.L.str.28, %edi
	movq	%r14, %rsi
	callq	err
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB145_71
	jmp	.LBB145_74
.LBB145_63:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
.LBB145_64:                             # %cadr.exit._crit_edge
	callq	err
.LBB145_62:
	xorl	%r15d, %r15d
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB145_71
	jmp	.LBB145_74
.LBB145_66:
	movl	$.L.str.28, %edi
	jmp	.LBB145_9
.LBB145_68:
	movl	$.L.str.27, %edi
	jmp	.LBB145_64
.LBB145_69:
	xorl	%r15d, %r15d
.LBB145_70:                             # %cadr.exit._crit_edge
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB145_74
.LBB145_71:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB145_73
# BB#72:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB145_73:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB145_77
.LBB145_74:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB145_76
# BB#75:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB145_76:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB145_77:                             # %cons.exit36
	testq	%r12, %r12
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r15, 8(%rbp)
	movq	%rbx, 16(%rbp)
	je	.LBB145_80
# BB#78:
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB145_80
# BB#79:                                # %setcdr.exit.thread
	leaq	2(%r12), %r14
	movq	%rbp, 16(%r12)
	movl	$.L.str.57, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	jmp	.LBB145_82
.LBB145_80:                             # %setcdr.exit
	movl	$.L.str.30, %edi
	movq	%r12, %rsi
	callq	err
	movq	%rbp, 16(%r12)
	movl	$.L.str.57, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB145_83
# BB#81:                                # %setcdr.exit._crit_edge
	leaq	2(%r12), %r14
.LBB145_82:
	movzwl	(%r14), %eax
	cmpl	$1, %eax
	je	.LBB145_84
.LBB145_83:                             # %.critedge.i38
	movl	$.L.str.29, %edi
	movq	%r12, %rsi
	callq	err
.LBB145_84:                             # %setcar.exit
	movq	%rbx, 8(%r12)
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end145:
	.size	letrec_macro, .Lfunc_end145-letrec_macro
	.cfi_endproc

	.globl	caar
	.p2align	4, 0x90
	.type	caar,@function
caar:                                   # @caar
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi728:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB146_10
# BB#1:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB146_10
# BB#2:
	cmpl	$1, %ecx
	jne	.LBB146_3
# BB#4:                                 # %car.exit
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB146_10
# BB#5:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB146_10
# BB#6:
	cmpl	$1, %eax
	jne	.LBB146_8
# BB#7:
	movq	8(%rsi), %rax
	popq	%rcx
	retq
.LBB146_3:
	movl	$.L.str.27, %edi
	movq	%rax, %rsi
.LBB146_9:                              # %car.exit2
	callq	err
.LBB146_10:                             # %car.exit2
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB146_8:
	movl	$.L.str.27, %edi
	jmp	.LBB146_9
.Lfunc_end146:
	.size	caar, .Lfunc_end146-caar
	.cfi_endproc

	.globl	reverse
	.p2align	4, 0x90
	.type	reverse,@function
reverse:                                # @reverse
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi729:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi730:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi731:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi732:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi733:
	.cfi_def_cfa_offset 48
.Lcfi734:
	.cfi_offset %rbx, -40
.Lcfi735:
	.cfi_offset %r12, -32
.Lcfi736:
	.cfi_offset %r14, -24
.Lcfi737:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	je	.LBB147_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB147_3:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	2(%r14), %eax
	testl	%eax, %eax
	je	.LBB147_7
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB147_3 Depth=1
	cmpl	$1, %eax
	jne	.LBB147_6
# BB#5:                                 #   in Loop: Header=BB147_3 Depth=1
	movq	8(%r14), %r12
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB147_9
	jmp	.LBB147_12
	.p2align	4, 0x90
.LBB147_6:                              #   in Loop: Header=BB147_3 Depth=1
	movl	$.L.str.27, %edi
	movq	%r14, %rsi
	callq	err
.LBB147_7:                              # %car.exit
                                        #   in Loop: Header=BB147_3 Depth=1
	xorl	%r12d, %r12d
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB147_12
.LBB147_9:                              #   in Loop: Header=BB147_3 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB147_11
# BB#10:                                #   in Loop: Header=BB147_3 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB147_11:                             #   in Loop: Header=BB147_3 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB147_15
	.p2align	4, 0x90
.LBB147_12:                             #   in Loop: Header=BB147_3 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB147_14
# BB#13:                                #   in Loop: Header=BB147_3 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB147_14:                             #   in Loop: Header=BB147_3 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB147_15:                             #   in Loop: Header=BB147_3 Depth=1
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r12, 8(%rbx)
	movq	%r15, 16(%rbx)
	movswl	2(%r14), %eax
	cmpl	$1, %eax
	jne	.LBB147_16
# BB#19:                                # %cdr.exit.backedge
                                        #   in Loop: Header=BB147_3 Depth=1
	movq	16(%r14), %r14
	testq	%r14, %r14
	movq	%rbx, %r15
	jne	.LBB147_3
	jmp	.LBB147_18
.LBB147_1:
	xorl	%ebx, %ebx
	jmp	.LBB147_18
.LBB147_16:
	testl	%eax, %eax
	je	.LBB147_18
# BB#17:
	movl	$.L.str.28, %edi
	movq	%r14, %rsi
	callq	err
.LBB147_18:                             # %cdr.exit._crit_edge
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end147:
	.size	reverse, .Lfunc_end147-reverse
	.cfi_endproc

	.globl	let_macro
	.p2align	4, 0x90
	.type	let_macro,@function
let_macro:                              # @let_macro
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi738:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi739:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi740:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi741:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi742:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi743:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi744:
	.cfi_def_cfa_offset 80
.Lcfi745:
	.cfi_offset %rbx, -56
.Lcfi746:
	.cfi_offset %r12, -48
.Lcfi747:
	.cfi_offset %r13, -40
.Lcfi748:
	.cfi_offset %r14, -32
.Lcfi749:
	.cfi_offset %r15, -24
.Lcfi750:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB148_59
# BB#1:
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB148_62
# BB#2:
	cmpl	$1, %eax
	jne	.LBB148_60
# BB#3:                                 # %cdr.exit
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB148_62
# BB#4:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB148_62
# BB#5:
	cmpl	$1, %eax
	jne	.LBB148_80
# BB#6:                                 # %car.exit.preheader
	movq	8(%rsi), %r15
	testq	%r15, %r15
	je	.LBB148_62
# BB#7:                                 # %.lr.ph.preheader
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	jmp	.LBB148_12
	.p2align	4, 0x90
.LBB148_8:                              # %car.exit.backedge
                                        #   in Loop: Header=BB148_12 Depth=1
	movq	16(%r15), %r15
	testq	%r15, %r15
	movq	%rbx, %rbp
	jne	.LBB148_12
	jmp	.LBB148_57
.LBB148_9:                              #   in Loop: Header=BB148_12 Depth=1
	movl	$.L.str.27, %edi
	movq	%rsi, %rbx
	callq	err
	movq	%rbx, %rsi
.LBB148_10:                             # %car.exit38
                                        #   in Loop: Header=BB148_12 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB148_23
.LBB148_11:                             #   in Loop: Header=BB148_12 Depth=1
	movl	$.L.str.27, %edi
	jmp	.LBB148_42
	.p2align	4, 0x90
.LBB148_12:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	2(%r15), %eax
	movb	$1, %cl
	testl	%eax, %eax
	je	.LBB148_20
# BB#13:                                # %.lr.ph
                                        #   in Loop: Header=BB148_12 Depth=1
	cmpl	$1, %eax
	jne	.LBB148_21
# BB#14:                                # %car.exit35
                                        #   in Loop: Header=BB148_12 Depth=1
	movq	8(%r15), %rsi
	testq	%rsi, %rsi
	je	.LBB148_20
# BB#15:                                #   in Loop: Header=BB148_12 Depth=1
	movswl	2(%rsi), %eax
	cmpl	$3, %eax
	jne	.LBB148_27
# BB#16:                                #   in Loop: Header=BB148_12 Depth=1
	movq	%r14, %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB148_43
# BB#17:                                #   in Loop: Header=BB148_12 Depth=1
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	jb	.LBB148_19
# BB#18:                                #   in Loop: Header=BB148_12 Depth=1
	movl	$.L.str.26, %edi
	movq	%rsi, %r13
	xorl	%esi, %esi
	callq	err
	movq	%r13, %rsi
.LBB148_19:                             #   in Loop: Header=BB148_12 Depth=1
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB148_46
	.p2align	4, 0x90
.LBB148_20:                             #   in Loop: Header=BB148_12 Depth=1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jmp	.LBB148_22
.LBB148_21:                             #   in Loop: Header=BB148_12 Depth=1
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movl	$.L.str.27, %edi
	movq	%r15, %rsi
	callq	err
	movb	$1, %cl
.LBB148_22:                             # %car.exit38
                                        #   in Loop: Header=BB148_12 Depth=1
	xorl	%esi, %esi
.LBB148_23:                             # %car.exit38
                                        #   in Loop: Header=BB148_12 Depth=1
	xorl	%r13d, %r13d
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB148_30
.LBB148_24:                             #   in Loop: Header=BB148_12 Depth=1
	movq	%r14, %rbx
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	jb	.LBB148_26
# BB#25:                                #   in Loop: Header=BB148_12 Depth=1
	movl	$.L.str.26, %edi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	xorl	%esi, %esi
	movq	%rcx, %rbp
	callq	err
	movq	%rbp, %rcx
	movq	16(%rsp), %rsi          # 8-byte Reload
.LBB148_26:                             #   in Loop: Header=BB148_12 Depth=1
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB148_33
.LBB148_27:                             #   in Loop: Header=BB148_12 Depth=1
	testl	%eax, %eax
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	je	.LBB148_10
# BB#28:                                #   in Loop: Header=BB148_12 Depth=1
	cmpl	$1, %eax
	jne	.LBB148_9
# BB#29:                                #   in Loop: Header=BB148_12 Depth=1
	movq	8(%rsi), %r13
	xorl	%ecx, %ecx
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB148_24
.LBB148_30:                             #   in Loop: Header=BB148_12 Depth=1
	movq	%r14, %rbx
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	jne	.LBB148_32
# BB#31:                                #   in Loop: Header=BB148_12 Depth=1
	movq	%rsi, %r14
	movq	%rcx, %rbp
	callq	gc_for_newcell
	movq	%rbp, %rcx
	movq	%r14, %rsi
	movq	freelist(%rip), %r14
.LBB148_32:                             #   in Loop: Header=BB148_12 Depth=1
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB148_33:                             # %cons.exit41
                                        #   in Loop: Header=BB148_12 Depth=1
	movw	$0, (%r14)
	movw	$1, 2(%r14)
	movq	%r13, 8(%r14)
	movq	%rbx, 16(%r14)
	testb	%cl, %cl
	jne	.LBB148_40
# BB#34:                                #   in Loop: Header=BB148_12 Depth=1
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB148_40
# BB#35:                                #   in Loop: Header=BB148_12 Depth=1
	cmpl	$1, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	jne	.LBB148_41
# BB#36:                                # %cdr.exit43
                                        #   in Loop: Header=BB148_12 Depth=1
	movq	16(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB148_47
# BB#37:                                #   in Loop: Header=BB148_12 Depth=1
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB148_47
# BB#38:                                #   in Loop: Header=BB148_12 Depth=1
	cmpl	$1, %eax
	jne	.LBB148_11
# BB#39:                                #   in Loop: Header=BB148_12 Depth=1
	movq	8(%rsi), %r13
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB148_48
	jmp	.LBB148_51
	.p2align	4, 0x90
.LBB148_40:                             #   in Loop: Header=BB148_12 Depth=1
	xorl	%r13d, %r13d
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB148_48
	jmp	.LBB148_51
.LBB148_41:                             #   in Loop: Header=BB148_12 Depth=1
	movl	$.L.str.28, %edi
.LBB148_42:                             # %car.exit45
                                        #   in Loop: Header=BB148_12 Depth=1
	callq	err
	jmp	.LBB148_47
.LBB148_43:                             #   in Loop: Header=BB148_12 Depth=1
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	jne	.LBB148_45
# BB#44:                                #   in Loop: Header=BB148_12 Depth=1
	movq	%rsi, %r14
	callq	gc_for_newcell
	movq	%r14, %rsi
	movq	freelist(%rip), %r14
.LBB148_45:                             #   in Loop: Header=BB148_12 Depth=1
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB148_46:                             # %cons.exit
                                        #   in Loop: Header=BB148_12 Depth=1
	movw	$0, (%r14)
	movw	$1, 2(%r14)
	movq	%rsi, 8(%r14)
	movq	%rbx, 16(%r14)
.LBB148_47:                             # %car.exit45
                                        #   in Loop: Header=BB148_12 Depth=1
	xorl	%r13d, %r13d
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB148_51
.LBB148_48:                             #   in Loop: Header=BB148_12 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB148_50
# BB#49:                                #   in Loop: Header=BB148_12 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB148_50:                             #   in Loop: Header=BB148_12 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB148_54
	.p2align	4, 0x90
.LBB148_51:                             #   in Loop: Header=BB148_12 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB148_53
# BB#52:                                #   in Loop: Header=BB148_12 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB148_53:                             #   in Loop: Header=BB148_12 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB148_54:                             #   in Loop: Header=BB148_12 Depth=1
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r13, 8(%rbx)
	movq	%rbp, 16(%rbx)
	movswl	2(%r15), %eax
	cmpl	$1, %eax
	je	.LBB148_8
# BB#55:
	testl	%eax, %eax
	je	.LBB148_57
# BB#56:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
	callq	err
.LBB148_57:                             # %car.exit._crit_edge
	testq	%r12, %r12
	jne	.LBB148_63
# BB#58:
	xorl	%r13d, %r13d
	jmp	.LBB148_94
.LBB148_59:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
	jmp	.LBB148_94
.LBB148_60:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
.LBB148_61:
	callq	err
.LBB148_62:
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
.LBB148_63:                             # %car.exit._crit_edge.thread
	movswl	2(%r12), %eax
	testl	%eax, %eax
	je	.LBB148_76
# BB#64:                                # %car.exit._crit_edge.thread
	cmpl	$1, %eax
	jne	.LBB148_77
# BB#65:                                # %cdr.exit52
	movq	16(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB148_79
# BB#66:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB148_81
# BB#67:
	cmpl	$1, %eax
	jne	.LBB148_82
# BB#68:                                # %cdr.exit54
	movq	16(%rsi), %r15
	testq	%r15, %r15
	je	.LBB148_83
# BB#69:
	movzwl	2(%r15), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB148_85
# BB#70:
	cmpl	$1, %ecx
	jne	.LBB148_84
# BB#71:                                # %cdr.exit56
	cmpq	$0, 16(%r15)
	je	.LBB148_85
# BB#72:
	movq	sym_progn(%rip), %rbp
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB148_90
# BB#73:
	movq	heap(%rip), %r13
	cmpq	heap_end(%rip), %r13
	jb	.LBB148_75
# BB#74:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB148_75:
	leaq	24(%r13), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB148_93
.LBB148_76:
	xorl	%r13d, %r13d
	jmp	.LBB148_94
.LBB148_77:
	movl	$.L.str.28, %edi
	movq	%r12, %rsi
.LBB148_78:                             # %car.exit58
	callq	err
.LBB148_79:
	xorl	%r13d, %r13d
.LBB148_94:                             # %car.exit58
	movq	%r14, %rdi
	callq	reverse
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	reverse
	movq	%rax, %r15
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB148_98
# BB#95:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB148_97
# BB#96:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB148_97:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB148_101
.LBB148_98:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB148_100
# BB#99:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB148_100:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB148_101:                            # %cons.exit64
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r13, 8(%rbp)
	movq	$0, 16(%rbp)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB148_105
# BB#102:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB148_104
# BB#103:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB148_104:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB148_108
.LBB148_105:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB148_107
# BB#106:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB148_107:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB148_108:                            # %cons.exit67
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%rbp, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB148_112
# BB#109:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB148_111
# BB#110:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB148_111:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB148_115
.LBB148_112:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB148_114
# BB#113:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB148_114:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB148_115:                            # %cons.exit70
	testq	%r12, %r12
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%r14, 8(%rbp)
	movq	%rbx, 16(%rbp)
	je	.LBB148_118
# BB#116:
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB148_118
# BB#117:                               # %setcdr.exit.thread
	leaq	2(%r12), %r14
	movq	%rbp, 16(%r12)
	movl	$.L.str.96, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	jmp	.LBB148_120
.LBB148_118:                            # %setcdr.exit
	movl	$.L.str.30, %edi
	movq	%r12, %rsi
	callq	err
	movq	%rbp, 16(%r12)
	movl	$.L.str.96, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	testq	%r12, %r12
	je	.LBB148_121
# BB#119:                               # %setcdr.exit._crit_edge
	leaq	2(%r12), %r14
.LBB148_120:
	movzwl	(%r14), %eax
	cmpl	$1, %eax
	je	.LBB148_122
.LBB148_121:                            # %.critedge.i72
	movl	$.L.str.29, %edi
	movq	%r12, %rsi
	callq	err
.LBB148_122:                            # %setcar.exit
	movq	%rbx, 8(%r12)
	movq	%r12, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB148_80:
	movl	$.L.str.27, %edi
	jmp	.LBB148_61
.LBB148_81:
	xorl	%r13d, %r13d
	jmp	.LBB148_94
.LBB148_82:
	movl	$.L.str.28, %edi
	jmp	.LBB148_78
.LBB148_83:
	xorl	%r13d, %r13d
	jmp	.LBB148_94
.LBB148_84:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
	callq	err
	movzwl	2(%r15), %eax
.LBB148_85:                             # %.thread86
	cwtl
	testl	%eax, %eax
	je	.LBB148_88
# BB#86:                                # %.thread86
	cmpl	$1, %eax
	jne	.LBB148_89
# BB#87:
	movq	8(%r15), %r13
	jmp	.LBB148_94
.LBB148_88:
	xorl	%r13d, %r13d
	jmp	.LBB148_94
.LBB148_89:
	movl	$.L.str.27, %edi
	movq	%r15, %rsi
	jmp	.LBB148_78
.LBB148_90:
	movq	freelist(%rip), %r13
	testq	%r13, %r13
	jne	.LBB148_92
# BB#91:
	callq	gc_for_newcell
	movq	freelist(%rip), %r13
.LBB148_92:
	movq	16(%r13), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB148_93:                             # %cons.exit61
	movw	$0, (%r13)
	movw	$1, 2(%r13)
	movq	%rbp, 8(%r13)
	movq	%r15, 16(%r13)
	jmp	.LBB148_94
.Lfunc_end148:
	.size	let_macro, .Lfunc_end148-let_macro
	.cfi_endproc

	.globl	leval_quote
	.p2align	4, 0x90
	.type	leval_quote,@function
leval_quote:                            # @leval_quote
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB149_1
# BB#2:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB149_3
# BB#4:
	cmpl	$1, %ecx
	jne	.LBB149_6
# BB#5:
	movq	8(%rax), %rax
	retq
.LBB149_1:
	xorl	%eax, %eax
	retq
.LBB149_3:
	xorl	%eax, %eax
	retq
.LBB149_6:
	pushq	%rax
.Lcfi751:
	.cfi_def_cfa_offset 16
	movl	$.L.str.27, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%eax, %eax
	addq	$8, %rsp
	retq
.Lfunc_end149:
	.size	leval_quote, .Lfunc_end149-leval_quote
	.cfi_endproc

	.globl	leval_tenv
	.p2align	4, 0x90
	.type	leval_tenv,@function
leval_tenv:                             # @leval_tenv
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	retq
.Lfunc_end150:
	.size	leval_tenv, .Lfunc_end150-leval_tenv
	.cfi_endproc

	.globl	leval_while
	.p2align	4, 0x90
	.type	leval_while,@function
leval_while:                            # @leval_while
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi752:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi753:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi754:
	.cfi_def_cfa_offset 32
.Lcfi755:
	.cfi_offset %rbx, -32
.Lcfi756:
	.cfi_offset %r14, -24
.Lcfi757:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB151_5
	.p2align	4, 0x90
.LBB151_1:                              # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%r15, %rsi
	callq	leval
	testq	%rax, %rax
	jne	.LBB151_1
	jmp	.LBB151_2
	.p2align	4, 0x90
.LBB151_13:                             #   in Loop: Header=BB151_5 Depth=1
	movl	$.L.str.28, %edi
	movq	%r14, %rsi
	callq	err
	jmp	.LBB151_5
	.p2align	4, 0x90
.LBB151_3:                              # %cdr.exit.backedge
                                        #   in Loop: Header=BB151_15 Depth=2
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB151_5
.LBB151_15:                             # %.lr.ph
                                        #   Parent Loop BB151_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB151_19
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB151_15 Depth=2
	cmpl	$1, %eax
	jne	.LBB151_18
# BB#17:                                #   in Loop: Header=BB151_15 Depth=2
	movq	8(%rbx), %rdi
	jmp	.LBB151_20
	.p2align	4, 0x90
.LBB151_18:                             #   in Loop: Header=BB151_15 Depth=2
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB151_19:                             #   in Loop: Header=BB151_15 Depth=2
	xorl	%edi, %edi
.LBB151_20:                             #   in Loop: Header=BB151_15 Depth=2
	movq	%r15, %rsi
	callq	leval
	movswl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB151_3
# BB#21:                                #   in Loop: Header=BB151_5 Depth=1
	testl	%eax, %eax
	je	.LBB151_5
# BB#22:                                #   in Loop: Header=BB151_5 Depth=1
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
	.p2align	4, 0x90
.LBB151_5:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB151_15 Depth 2
	movswl	2(%r14), %eax
	testl	%eax, %eax
	je	.LBB151_9
# BB#6:                                 # %.split
                                        #   in Loop: Header=BB151_5 Depth=1
	cmpl	$1, %eax
	jne	.LBB151_8
# BB#7:                                 #   in Loop: Header=BB151_5 Depth=1
	movq	8(%r14), %rdi
	jmp	.LBB151_10
	.p2align	4, 0x90
.LBB151_8:                              #   in Loop: Header=BB151_5 Depth=1
	movl	$.L.str.27, %edi
	movq	%r14, %rsi
	callq	err
.LBB151_9:                              # %car.exit
                                        #   in Loop: Header=BB151_5 Depth=1
	xorl	%edi, %edi
.LBB151_10:                             # %car.exit
                                        #   in Loop: Header=BB151_5 Depth=1
	movq	%r15, %rsi
	callq	leval
	testq	%rax, %rax
	je	.LBB151_2
# BB#11:                                #   in Loop: Header=BB151_5 Depth=1
	movswl	2(%r14), %eax
	testl	%eax, %eax
	je	.LBB151_5
# BB#12:                                #   in Loop: Header=BB151_5 Depth=1
	cmpl	$1, %eax
	jne	.LBB151_13
# BB#14:                                # %cdr.exit.preheader
                                        #   in Loop: Header=BB151_5 Depth=1
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB151_15
	jmp	.LBB151_5
.LBB151_2:                              # %.us-lcssa.us
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end151:
	.size	leval_while, .Lfunc_end151-leval_while
	.cfi_endproc

	.globl	symbolconc
	.p2align	4, 0x90
	.type	symbolconc,@function
symbolconc:                             # @symbolconc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi758:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi759:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi760:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi761:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi762:
	.cfi_def_cfa_offset 48
.Lcfi763:
	.cfi_offset %rbx, -40
.Lcfi764:
	.cfi_offset %r12, -32
.Lcfi765:
	.cfi_offset %r14, -24
.Lcfi766:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	tkbuffer(%rip), %rax
	movb	$0, (%rax)
	testq	%r15, %r15
	je	.LBB152_16
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB152_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	2(%r15), %eax
	testl	%eax, %eax
	je	.LBB152_8
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB152_2 Depth=1
	cmpl	$1, %eax
	jne	.LBB152_7
# BB#4:                                 # %car.exit
                                        #   in Loop: Header=BB152_2 Depth=1
	movq	8(%r15), %r14
	testq	%r14, %r14
	je	.LBB152_8
# BB#5:                                 #   in Loop: Header=BB152_2 Depth=1
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	jne	.LBB152_9
	jmp	.LBB152_10
	.p2align	4, 0x90
.LBB152_7:                              #   in Loop: Header=BB152_2 Depth=1
	movl	$.L.str.27, %edi
	movq	%r15, %rsi
	callq	err
.LBB152_8:                              #   in Loop: Header=BB152_2 Depth=1
	xorl	%r14d, %r14d
.LBB152_9:                              # %.thread
                                        #   in Loop: Header=BB152_2 Depth=1
	movl	$.L.str.97, %edi
	movq	%r14, %rsi
	callq	err
.LBB152_10:                             #   in Loop: Header=BB152_2 Depth=1
	movq	8(%r14), %rbx
	movq	%rbx, %rdi
	callq	strlen
	addq	%rax, %r12
	cmpq	$5121, %r12             # imm = 0x1401
	jl	.LBB152_12
# BB#11:                                #   in Loop: Header=BB152_2 Depth=1
	movl	$.L.str.98, %edi
	xorl	%esi, %esi
	callq	err
	movq	8(%r14), %rbx
.LBB152_12:                             #   in Loop: Header=BB152_2 Depth=1
	movq	tkbuffer(%rip), %rdi
	movq	%rbx, %rsi
	callq	strcat
	movswl	2(%r15), %eax
	cmpl	$1, %eax
	jne	.LBB152_14
# BB#13:                                # %cdr.exit.backedge
                                        #   in Loop: Header=BB152_2 Depth=1
	movq	16(%r15), %r15
	testq	%r15, %r15
	jne	.LBB152_2
	jmp	.LBB152_16
.LBB152_14:
	testl	%eax, %eax
	je	.LBB152_16
# BB#15:
	movl	$.L.str.28, %edi
	movq	%r15, %rsi
	callq	err
.LBB152_16:                             # %cdr.exit._crit_edge
	movq	tkbuffer(%rip), %rdi
	movl	$1, %esi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	gen_intern              # TAILCALL
.Lfunc_end152:
	.size	symbolconc, .Lfunc_end152-symbolconc
	.cfi_endproc

	.globl	subr_kind_str
	.p2align	4, 0x90
	.type	subr_kind_str,@function
subr_kind_str:                          # @subr_kind_str
	.cfi_startproc
# BB#0:
	leaq	-4(%rdi), %rax
	cmpq	$17, %rax
	ja	.LBB153_2
# BB#1:                                 # %switch.lookup
	movq	.Lswitch.table-32(,%rdi,8), %rax
	retq
.LBB153_2:
	movl	$.L.str.109, %eax
	retq
.Lfunc_end153:
	.size	subr_kind_str, .Lfunc_end153-subr_kind_str
	.cfi_endproc

	.globl	lprin1g
	.p2align	4, 0x90
	.type	lprin1g,@function
lprin1g:                                # @lprin1g
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi767:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi768:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi769:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi770:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi771:
	.cfi_def_cfa_offset 48
.Lcfi772:
	.cfi_offset %rbx, -40
.Lcfi773:
	.cfi_offset %r12, -32
.Lcfi774:
	.cfi_offset %r14, -24
.Lcfi775:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%rbx, (%rsp)
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB154_2
# BB#1:
	movl	$.L.str.23, %edi
	xorl	%esi, %esi
	callq	err
.LBB154_2:
	testq	%rbx, %rbx
	je	.LBB154_5
# BB#3:
	movswl	2(%rbx), %eax
	movslq	%eax, %r12
	cmpl	$21, %r12d
	ja	.LBB154_39
# BB#4:
	jmpq	*.LJTI154_0(,%rax,8)
.LBB154_25:
	movq	tkbuffer(%rip), %rdi
	movq	%r12, %rax
	addq	$-4, %rax
	cmpq	$17, %rax
	ja	.LBB154_26
# BB#27:                                # %switch.lookup.i
	movq	.Lswitch.table-32(,%r12,8), %rdx
	jmp	.LBB154_28
.LBB154_5:                              # %.thread
	movq	16(%r14), %rsi
	movl	$.L.str.110, %edi
	jmp	.LBB154_49
.LBB154_26:
	movl	$.L.str.109, %edx
.LBB154_28:                             # %subr_kind_str.exit
	movl	$.L.str.117, %esi
	xorl	%eax, %eax
	callq	sprintf
	movq	tkbuffer(%rip), %rdi
	movq	16(%r14), %rsi
	callq	*8(%r14)
	movq	8(%rbx), %rdi
	movq	16(%r14), %rsi
	callq	*8(%r14)
.LBB154_38:
	movq	16(%r14), %rsi
	movl	$.L.str.118, %edi
.LBB154_49:
	callq	*8(%r14)
.LBB154_50:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB154_21:
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	cvttsd2si	%xmm0, %rdx
	cvtsi2sdq	%rdx, %xmm1
	movq	tkbuffer(%rip), %rdi
	ucomisd	%xmm0, %xmm1
	jne	.LBB154_23
	jp	.LBB154_23
# BB#22:
	movl	$.L.str.115, %esi
	xorl	%eax, %eax
	callq	sprintf
	jmp	.LBB154_47
.LBB154_6:
	movq	16(%r14), %rsi
	movl	$.L.str.111, %edi
	callq	*8(%r14)
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB154_10
# BB#7:
	cmpl	$1, %eax
	jne	.LBB154_9
# BB#8:
	movq	8(%rbx), %rdi
	jmp	.LBB154_11
.LBB154_24:
	movq	8(%rbx), %rdi
	jmp	.LBB154_48
.LBB154_29:
	movq	16(%r14), %rsi
	movl	$.L.str.119, %edi
	callq	*8(%r14)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB154_37
# BB#30:
	movzwl	2(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB154_37
# BB#31:                                # %car.exit42
	movq	8(%rdi), %rdi
	movq	%r14, %rsi
	callq	lprin1g
	movq	16(%r14), %rsi
	movl	$.L.str.112, %edi
	callq	*8(%r14)
	movq	16(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB154_36
# BB#32:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB154_36
# BB#33:
	cmpl	$1, %eax
	jne	.LBB154_35
# BB#34:
	movq	16(%rsi), %rdi
	jmp	.LBB154_37
.LBB154_39:
	movq	user_types(%rip), %r15
	testq	%r15, %r15
	jne	.LBB154_43
# BB#40:
	movl	$8000, %edi             # imm = 0x1F40
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB154_42
# BB#41:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB154_42:                             # %must_malloc.exit.i
	movq	%r15, user_types(%rip)
	xorl	%esi, %esi
	movl	$8000, %edx             # imm = 0x1F40
	movq	%r15, %rdi
	callq	memset
.LBB154_43:
	movzwl	%r12w, %eax
	cmpl	$99, %eax
	ja	.LBB154_51
# BB#44:                                # %get_user_type_hooks.exit
	leaq	(%r12,%r12,4), %rax
	shlq	$4, %rax
	movq	32(%r15,%rax), %rax
	testq	%rax, %rax
	je	.LBB154_46
# BB#45:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	*%rax
	jmp	.LBB154_50
.LBB154_23:
	movl	$.L.str.116, %esi
	movb	$1, %al
	callq	sprintf
	jmp	.LBB154_47
.LBB154_46:
	movq	tkbuffer(%rip), %rdi
	movswl	2(%rbx), %edx
	movl	$.L.str.120, %esi
	xorl	%eax, %eax
	movq	%rbx, %rcx
	callq	sprintf
.LBB154_47:
	movq	tkbuffer(%rip), %rdi
.LBB154_48:
	movq	16(%r14), %rsi
	jmp	.LBB154_49
.LBB154_35:
	movl	$.L.str.28, %edi
	callq	err
.LBB154_36:                             # %cdr.exit44
	xorl	%edi, %edi
.LBB154_37:                             # %.thread53
	movq	%r14, %rsi
	callq	lprin1g
	jmp	.LBB154_38
.LBB154_51:
	movl	$.L.str.72, %edi
	xorl	%esi, %esi
	callq	err
.LBB154_9:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB154_10:                             # %.lr.ph.preheader
	xorl	%edi, %edi
	jmp	.LBB154_11
.LBB154_18:                             #   in Loop: Header=BB154_11 Depth=1
	movq	8(%rbx), %rdi
.LBB154_11:                             # %.lr.ph.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rsi
	callq	lprin1g
	movswl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB154_12
# BB#14:                                # %cdr.exit
                                        #   in Loop: Header=BB154_11 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB154_20
# BB#15:                                #   in Loop: Header=BB154_11 Depth=1
	movq	8(%r14), %rax
	movq	16(%r14), %rsi
	movzwl	2(%rbx), %ecx
	cmpl	$1, %ecx
	jne	.LBB154_19
# BB#16:                                #   in Loop: Header=BB154_11 Depth=1
	movl	$.L.str.112, %edi
	callq	*%rax
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB154_10
# BB#17:                                #   in Loop: Header=BB154_11 Depth=1
	cmpl	$1, %eax
	je	.LBB154_18
	jmp	.LBB154_9
.LBB154_12:                             # %.lr.ph
	testl	%eax, %eax
	je	.LBB154_20
# BB#13:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
	jmp	.LBB154_20
.LBB154_19:
	movl	$.L.str.113, %edi
	callq	*%rax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	lprin1g
.LBB154_20:                             # %.thread48
	movq	16(%r14), %rsi
	movl	$.L.str.114, %edi
	jmp	.LBB154_49
.Lfunc_end154:
	.size	lprin1g, .Lfunc_end154-lprin1g
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI154_0:
	.quad	.LBB154_5
	.quad	.LBB154_6
	.quad	.LBB154_21
	.quad	.LBB154_24
	.quad	.LBB154_25
	.quad	.LBB154_25
	.quad	.LBB154_25
	.quad	.LBB154_25
	.quad	.LBB154_25
	.quad	.LBB154_25
	.quad	.LBB154_25
	.quad	.LBB154_29
	.quad	.LBB154_39
	.quad	.LBB154_39
	.quad	.LBB154_39
	.quad	.LBB154_39
	.quad	.LBB154_39
	.quad	.LBB154_39
	.quad	.LBB154_39
	.quad	.LBB154_25
	.quad	.LBB154_25
	.quad	.LBB154_25

	.text
	.globl	get_c_file
	.p2align	4, 0x90
	.type	get_c_file,@function
get_c_file:                             # @get_c_file
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi776:
	.cfi_def_cfa_offset 16
.Lcfi777:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB155_2
# BB#1:
	testq	%rsi, %rsi
	jne	.LBB155_7
.LBB155_2:
	testq	%rbx, %rbx
	je	.LBB155_4
# BB#3:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB155_5
.LBB155_4:                              # %.critedge
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB155_5:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB155_7
# BB#6:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %rsi
.LBB155_7:
	movq	%rsi, %rax
	popq	%rbx
	retq
.Lfunc_end155:
	.size	get_c_file, .Lfunc_end155-get_c_file
	.cfi_endproc

	.globl	lprin1f
	.p2align	4, 0x90
	.type	lprin1f,@function
lprin1f:                                # @lprin1f
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi778:
	.cfi_def_cfa_offset 32
	movq	$0, (%rsp)
	movq	$fputs_fcn, 8(%rsp)
	movq	%rsi, 16(%rsp)
	movq	%rsp, %rsi
	callq	lprin1g
	xorl	%eax, %eax
	addq	$24, %rsp
	retq
.Lfunc_end156:
	.size	lprin1f, .Lfunc_end156-lprin1f
	.cfi_endproc

	.globl	lprin1
	.p2align	4, 0x90
	.type	lprin1,@function
lprin1:                                 # @lprin1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi779:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi780:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi781:
	.cfi_def_cfa_offset 48
.Lcfi782:
	.cfi_offset %rbx, -24
.Lcfi783:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB157_2
# BB#1:
	movq	stdout(%rip), %rax
	testq	%rax, %rax
	jne	.LBB157_7
.LBB157_2:
	testq	%rbx, %rbx
	je	.LBB157_4
# BB#3:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB157_5
.LBB157_4:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB157_5:
	movq	8(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB157_7
# BB#6:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %rax
.LBB157_7:                              # %get_c_file.exit
	movq	$0, (%rsp)
	movq	$fputs_fcn, 8(%rsp)
	movq	%rax, 16(%rsp)
	movq	%rsp, %rsi
	movq	%r14, %rdi
	callq	lprin1g
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end157:
	.size	lprin1, .Lfunc_end157-lprin1
	.cfi_endproc

	.globl	lreadf
	.p2align	4, 0x90
	.type	lreadf,@function
lreadf:                                 # @lreadf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi784:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi785:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi786:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi787:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi788:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi789:
	.cfi_def_cfa_offset 80
.Lcfi790:
	.cfi_offset %rbx, -48
.Lcfi791:
	.cfi_offset %r12, -40
.Lcfi792:
	.cfi_offset %r14, -32
.Lcfi793:
	.cfi_offset %r15, -24
.Lcfi794:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	$f_getc, 8(%rsp)
	movq	$f_ungetc, 16(%rsp)
	movq	%r15, 24(%rsp)
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	_IO_getc
	movl	%eax, %r12d
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB158_3
# BB#1:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB158_3
# BB#2:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB158_3:                              # %no_interrupt.exit
	cmpl	$-1, %r12d
	je	.LBB158_11
# BB#4:                                 # %.lr.ph.i.preheader
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB158_5:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	testl	%ebx, %ebx
	je	.LBB158_12
# BB#6:                                 #   in Loop: Header=BB158_5 Depth=1
	cmpl	$10, %r12d
	cmovel	%r14d, %ebx
	jmp	.LBB158_7
	.p2align	4, 0x90
.LBB158_12:                             #   in Loop: Header=BB158_5 Depth=1
	movl	$1, %ebx
	cmpl	$59, %r12d
	je	.LBB158_7
# BB#13:                                #   in Loop: Header=BB158_5 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%r12d, %rcx
	xorl	%ebx, %ebx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB158_14
	.p2align	4, 0x90
.LBB158_7:                              # %.split.us.i.backedge.i
                                        #   in Loop: Header=BB158_5 Depth=1
	movq	nointerrupt(%rip), %rbp
	movq	$1, nointerrupt(%rip)
	movq	%r15, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movq	%rbp, nointerrupt(%rip)
	testq	%rbp, %rbp
	jne	.LBB158_10
# BB#8:                                 # %.split.us.i.backedge.i
                                        #   in Loop: Header=BB158_5 Depth=1
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB158_10
# BB#9:                                 #   in Loop: Header=BB158_5 Depth=1
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB158_10:                             # %no_interrupt.exit5
                                        #   in Loop: Header=BB158_5 Depth=1
	cmpl	$-1, %r12d
	jne	.LBB158_5
.LBB158_11:                             # %.split.us.i._crit_edge.i
	movq	eof_val(%rip), %rax
.LBB158_15:                             # %readtl.exit
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB158_14:                             # %flush_ws.exit.i
	movl	%r12d, %edi
	movq	%r15, %rsi
	callq	ungetc
	leaq	8(%rsp), %rdi
	callq	lreadr
	jmp	.LBB158_15
.Lfunc_end158:
	.size	lreadf, .Lfunc_end158-lreadf
	.cfi_endproc

	.globl	f_getc
	.p2align	4, 0x90
	.type	f_getc,@function
f_getc:                                 # @f_getc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi795:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi796:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi797:
	.cfi_def_cfa_offset 32
.Lcfi798:
	.cfi_offset %rbx, -24
.Lcfi799:
	.cfi_offset %rbp, -16
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB159_3
# BB#1:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB159_3
# BB#2:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB159_3:                              # %no_interrupt.exit
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end159:
	.size	f_getc, .Lfunc_end159-f_getc
	.cfi_endproc

	.globl	f_ungetc
	.p2align	4, 0x90
	.type	f_ungetc,@function
f_ungetc:                               # @f_ungetc
	.cfi_startproc
# BB#0:
	jmp	ungetc                  # TAILCALL
.Lfunc_end160:
	.size	f_ungetc, .Lfunc_end160-f_ungetc
	.cfi_endproc

	.globl	flush_ws
	.p2align	4, 0x90
	.type	flush_ws,@function
flush_ws:                               # @flush_ws
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi800:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi801:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi802:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi803:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi804:
	.cfi_def_cfa_offset 48
.Lcfi805:
	.cfi_offset %rbx, -48
.Lcfi806:
	.cfi_offset %r12, -40
.Lcfi807:
	.cfi_offset %r14, -32
.Lcfi808:
	.cfi_offset %r15, -24
.Lcfi809:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	testq	%r14, %r14
	je	.LBB161_8
# BB#1:                                 # %.split.preheader
	xorl	%r15d, %r15d
	xorl	%ebx, %ebx
	jmp	.LBB161_3
	.p2align	4, 0x90
.LBB161_2:                              #   in Loop: Header=BB161_3 Depth=1
	cmpl	$10, %ebp
	cmovel	%r15d, %ebx
.LBB161_3:                              # %.split
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rdi
	callq	*(%r12)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB161_5
# BB#4:                                 #   in Loop: Header=BB161_3 Depth=1
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	err
.LBB161_5:                              #   in Loop: Header=BB161_3 Depth=1
	testl	%ebx, %ebx
	jne	.LBB161_2
# BB#6:                                 #   in Loop: Header=BB161_3 Depth=1
	movl	$1, %ebx
	cmpl	$59, %ebp
	je	.LBB161_3
# BB#7:                                 #   in Loop: Header=BB161_3 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	movl	$0, %ebx
	jne	.LBB161_3
	jmp	.LBB161_17
.LBB161_8:                              # %.split.us.preheader
	movq	16(%r12), %rdi
	callq	*(%r12)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB161_16
# BB#9:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB161_10:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	je	.LBB161_12
# BB#11:                                #   in Loop: Header=BB161_10 Depth=1
	cmpl	$10, %ebp
	cmovel	%r14d, %r15d
	jmp	.LBB161_14
	.p2align	4, 0x90
.LBB161_12:                             #   in Loop: Header=BB161_10 Depth=1
	movl	$1, %r15d
	cmpl	$59, %ebp
	je	.LBB161_14
# BB#13:                                #   in Loop: Header=BB161_10 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	xorl	%r15d, %r15d
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB161_17
	.p2align	4, 0x90
.LBB161_14:                             # %.split.us.backedge
                                        #   in Loop: Header=BB161_10 Depth=1
	movq	16(%r12), %rdi
	callq	*(%r12)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB161_10
.LBB161_16:
	movl	$-1, %ebp
.LBB161_17:                             # %.us-lcssa.us
	movl	%ebp, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end161:
	.size	flush_ws, .Lfunc_end161-flush_ws
	.cfi_endproc

	.globl	readtl
	.p2align	4, 0x90
	.type	readtl,@function
readtl:                                 # @readtl
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi810:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi811:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi812:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi813:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi814:
	.cfi_def_cfa_offset 48
.Lcfi815:
	.cfi_offset %rbx, -40
.Lcfi816:
	.cfi_offset %r14, -32
.Lcfi817:
	.cfi_offset %r15, -24
.Lcfi818:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	16(%r15), %rdi
	callq	*(%r15)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	je	.LBB162_5
# BB#1:                                 # %.lr.ph.preheader
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB162_2:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	testl	%ebx, %ebx
	je	.LBB162_6
# BB#3:                                 #   in Loop: Header=BB162_2 Depth=1
	cmpl	$10, %ebp
	cmovel	%r14d, %ebx
	jmp	.LBB162_4
	.p2align	4, 0x90
.LBB162_6:                              #   in Loop: Header=BB162_2 Depth=1
	movl	$1, %ebx
	cmpl	$59, %ebp
	je	.LBB162_4
# BB#7:                                 #   in Loop: Header=BB162_2 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movslq	%ebp, %rcx
	xorl	%ebx, %ebx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB162_8
	.p2align	4, 0x90
.LBB162_4:                              # %.split.us.i.backedge
                                        #   in Loop: Header=BB162_2 Depth=1
	movq	16(%r15), %rdi
	callq	*(%r15)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB162_2
.LBB162_5:
	movq	eof_val(%rip), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB162_8:                              # %flush_ws.exit
	movq	16(%r15), %rsi
	movl	%ebp, %edi
	callq	*8(%r15)
	movq	%r15, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	lreadr                  # TAILCALL
.Lfunc_end162:
	.size	readtl, .Lfunc_end162-readtl
	.cfi_endproc

	.globl	lreadr
	.p2align	4, 0x90
	.type	lreadr,@function
lreadr:                                 # @lreadr
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi819:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi820:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi821:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi822:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi823:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi824:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi825:
	.cfi_def_cfa_offset 64
.Lcfi826:
	.cfi_offset %rbx, -56
.Lcfi827:
	.cfi_offset %r12, -48
.Lcfi828:
	.cfi_offset %r13, -40
.Lcfi829:
	.cfi_offset %r14, -32
.Lcfi830:
	.cfi_offset %r15, -24
.Lcfi831:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	%r14, (%rsp)
	movq	tkbuffer(%rip), %r13
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB163_2
# BB#1:
	movl	$.L.str.23, %edi
	xorl	%esi, %esi
	callq	err
.LBB163_2:
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB163_3
	.p2align	4, 0x90
.LBB163_6:                              #   in Loop: Header=BB163_3 Depth=1
	cmpl	$10, %ebp
	cmovel	%r12d, %ebx
.LBB163_3:                              # %.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %rdi
	callq	*(%r14)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB163_5
# BB#4:                                 #   in Loop: Header=BB163_3 Depth=1
	movl	$.L.str.122, %edi
	xorl	%esi, %esi
	callq	err
.LBB163_5:                              #   in Loop: Header=BB163_3 Depth=1
	testl	%ebx, %ebx
	jne	.LBB163_6
# BB#7:                                 #   in Loop: Header=BB163_3 Depth=1
	movl	$1, %ebx
	cmpl	$59, %ebp
	je	.LBB163_3
# BB#8:                                 #   in Loop: Header=BB163_3 Depth=1
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	movl	$0, %ebx
	jne	.LBB163_3
# BB#9:                                 # %flush_ws.exit
	leal	-34(%rbp), %eax
	cmpl	$62, %eax
	ja	.LBB163_38
# BB#10:                                # %flush_ws.exit
	jmpq	*.LJTI163_0(,%rax,8)
.LBB163_36:
	movq	%r14, %rdi
	callq	lreadstring
	movq	%rax, %rbx
	jmp	.LBB163_52
.LBB163_37:
	movq	%r14, %rdi
	callq	lreadsharp
	movq	%rax, %rbx
	jmp	.LBB163_52
.LBB163_38:
	movq	user_readm(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB163_41
# BB#39:
	movq	user_ch_readm(%rip), %rdi
	movl	%ebp, %esi
	callq	strchr
	testq	%rax, %rax
	je	.LBB163_41
# BB#40:
	movl	%ebp, %edi
	movq	%r14, %rsi
	callq	*%rbx
	movq	%rax, %rbx
	jmp	.LBB163_52
.LBB163_11:
	movq	%r14, %rdi
	callq	lreadparen
	movq	%rax, %rbx
	jmp	.LBB163_52
.LBB163_12:
	movl	$.L.str.123, %edi
	xorl	%esi, %esi
	callq	err
.LBB163_13:
	movq	sym_quote(%rip), %r15
	movq	%r14, %rdi
	callq	lreadr
	movq	%rax, %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB163_17
# BB#14:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB163_16
# BB#15:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB163_16:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB163_20
.LBB163_30:
	movq	16(%r14), %rdi
	callq	*(%r14)
	cmpl	$64, %eax
	je	.LBB163_31
# BB#32:
	cmpl	$46, %eax
	jne	.LBB163_34
# BB#33:
	movl	$.L.str.126, %edi
	jmp	.LBB163_35
.LBB163_29:
	movl	$.L.str.124, %edi
	jmp	.LBB163_35
.LBB163_17:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB163_19
# BB#18:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB163_19:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB163_20:                             # %cons.exit
	movw	$0, (%rbp)
	movw	$1, 2(%rbp)
	movq	%rbx, 8(%rbp)
	movq	$0, 16(%rbp)
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB163_22
	jmp	.LBB163_25
.LBB163_41:
	movb	%bpl, (%r13)
	movq	16(%r14), %rdi
	callq	*(%r14)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	movq	%r13, %rbx
	je	.LBB163_42
# BB#44:                                # %.lr.ph.preheader
	leaq	16(%r14), %r13
	movl	$1, %r12d
	.p2align	4, 0x90
.LBB163_45:                             # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB163_43
# BB#46:                                #   in Loop: Header=BB163_45 Depth=1
	movl	$.L.str.128, %edi
	movl	$8, %edx
	movl	%ebp, %esi
	callq	memchr
	testq	%rax, %rax
	jne	.LBB163_48
# BB#47:                                #   in Loop: Header=BB163_45 Depth=1
	movq	user_te_readm(%rip), %rdi
	movl	%ebp, %esi
	callq	strchr
	testq	%rax, %rax
	jne	.LBB163_48
# BB#49:                                #   in Loop: Header=BB163_45 Depth=1
	movq	%rbx, %rax
	movb	%bpl, (%rax,%r12)
	leal	1(%r12), %eax
	cmpl	$5119, %eax             # imm = 0x13FF
	jg	.LBB163_51
# BB#50:                                # %._crit_edge
                                        #   in Loop: Header=BB163_45 Depth=1
	movq	(%rsp), %r14
	leaq	16(%r14), %r13
	movq	16(%r14), %rdi
	callq	*(%r14)
	movl	%eax, %ebp
	incq	%r12
	cmpl	$-1, %ebp
	jne	.LBB163_45
	jmp	.LBB163_43
.LBB163_31:
	movl	$.L.str.125, %edi
	jmp	.LBB163_35
.LBB163_34:
	movq	16(%r14), %rsi
	movl	%eax, %edi
	callq	*8(%r14)
	movl	$.L.str.127, %edi
.LBB163_35:
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	lreadr
	movq	%rax, %rbp
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB163_25
.LBB163_22:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB163_24
# BB#23:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB163_24:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB163_28
.LBB163_25:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB163_27
# BB#26:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB163_27:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB163_28:                             # %cons.exit36
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%rbp, 16(%rbx)
.LBB163_52:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB163_42:
	movl	$1, %r12d
	jmp	.LBB163_43
.LBB163_48:
	movq	(%r13), %rsi
	movl	%ebp, %edi
	callq	*8(%r14)
.LBB163_43:
	movq	%rbx, %rdi
	movq	%r12, %rsi
	callq	lreadtk
	movq	%rax, %rbx
	jmp	.LBB163_52
.LBB163_51:
	xorl	%ebx, %ebx
	movl	$.L.str.129, %edi
	xorl	%esi, %esi
	callq	err
	jmp	.LBB163_52
.Lfunc_end163:
	.size	lreadr, .Lfunc_end163-lreadr
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI163_0:
	.quad	.LBB163_36
	.quad	.LBB163_37
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_13
	.quad	.LBB163_11
	.quad	.LBB163_12
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_30
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_38
	.quad	.LBB163_29

	.text
	.globl	set_read_hooks
	.p2align	4, 0x90
	.type	set_read_hooks,@function
set_read_hooks:                         # @set_read_hooks
	.cfi_startproc
# BB#0:
	movq	%rdi, user_ch_readm(%rip)
	movq	%rsi, user_te_readm(%rip)
	movq	%rdx, user_readm(%rip)
	movq	%rcx, user_readt(%rip)
	retq
.Lfunc_end164:
	.size	set_read_hooks, .Lfunc_end164-set_read_hooks
	.cfi_endproc

	.globl	lreadparen
	.p2align	4, 0x90
	.type	lreadparen,@function
lreadparen:                             # @lreadparen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi832:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi833:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi834:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi835:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi836:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi837:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi838:
	.cfi_def_cfa_offset 64
.Lcfi839:
	.cfi_offset %rbx, -56
.Lcfi840:
	.cfi_offset %r12, -48
.Lcfi841:
	.cfi_offset %r13, -40
.Lcfi842:
	.cfi_offset %r14, -32
.Lcfi843:
	.cfi_offset %r15, -24
.Lcfi844:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	xorl	%r14d, %r14d
	xorl	%ebx, %ebx
	jmp	.LBB165_1
	.p2align	4, 0x90
.LBB165_4:                              #   in Loop: Header=BB165_1 Depth=1
	cmpl	$10, %ebp
	cmovel	%r14d, %ebx
.LBB165_1:                              # %.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rdi
	callq	*(%r13)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB165_3
# BB#2:                                 #   in Loop: Header=BB165_1 Depth=1
	movl	$.L.str.130, %edi
	xorl	%esi, %esi
	callq	err
.LBB165_3:                              #   in Loop: Header=BB165_1 Depth=1
	testl	%ebx, %ebx
	jne	.LBB165_4
# BB#5:                                 #   in Loop: Header=BB165_1 Depth=1
	movl	$1, %ebx
	cmpl	$59, %ebp
	je	.LBB165_1
# BB#6:                                 #   in Loop: Header=BB165_1 Depth=1
	callq	__ctype_b_loc
	movq	%rax, %r15
	movq	(%r15), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	movl	$0, %ebx
	jne	.LBB165_1
# BB#7:                                 # %flush_ws.exit
	cmpl	$41, %ebp
	jne	.LBB165_9
# BB#8:
	xorl	%r14d, %r14d
	jmp	.LBB165_27
.LBB165_9:
	movq	16(%r13), %rsi
	movl	%ebp, %edi
	callq	*8(%r13)
	movq	%r13, %rdi
	callq	lreadr
	movq	%rax, %r12
	cmpq	sym_dot(%rip), %r12
	je	.LBB165_10
# BB#19:
	movq	%r13, %rdi
	callq	lreadparen
	movq	%rax, %rbx
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB165_23
# BB#20:
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	jb	.LBB165_22
# BB#21:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB165_22:
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB165_26
.LBB165_10:
	movq	%r13, %rdi
	callq	lreadr
	movq	%rax, %r14
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	jmp	.LBB165_11
	.p2align	4, 0x90
.LBB165_14:                             #   in Loop: Header=BB165_11 Depth=1
	cmpl	$10, %ebp
	cmovel	%r12d, %ebx
.LBB165_11:                             # %.split.i17
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r13), %rdi
	callq	*(%r13)
	movl	%eax, %ebp
	cmpl	$-1, %ebp
	jne	.LBB165_13
# BB#12:                                #   in Loop: Header=BB165_11 Depth=1
	movl	$.L.str.130, %edi
	xorl	%esi, %esi
	callq	err
.LBB165_13:                             #   in Loop: Header=BB165_11 Depth=1
	testl	%ebx, %ebx
	jne	.LBB165_14
# BB#15:                                #   in Loop: Header=BB165_11 Depth=1
	movl	$1, %ebx
	cmpl	$59, %ebp
	je	.LBB165_11
# BB#16:                                #   in Loop: Header=BB165_11 Depth=1
	movq	(%r15), %rax
	movslq	%ebp, %rcx
	testb	$32, 1(%rax,%rcx,2)
	movl	$0, %ebx
	jne	.LBB165_11
# BB#17:                                # %flush_ws.exit19
	cmpl	$41, %ebp
	je	.LBB165_27
# BB#18:
	movl	$.L.str.131, %edi
	xorl	%esi, %esi
	callq	err
	jmp	.LBB165_27
.LBB165_23:
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	jne	.LBB165_25
# BB#24:
	callq	gc_for_newcell
	movq	freelist(%rip), %r14
.LBB165_25:
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB165_26:                             # %cons.exit
	movw	$0, (%r14)
	movw	$1, 2(%r14)
	movq	%r12, 8(%r14)
	movq	%rbx, 16(%r14)
.LBB165_27:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end165:
	.size	lreadparen, .Lfunc_end165-lreadparen
	.cfi_endproc

	.globl	lreadtk
	.p2align	4, 0x90
	.type	lreadtk,@function
lreadtk:                                # @lreadtk
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi845:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi846:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi847:
	.cfi_def_cfa_offset 48
.Lcfi848:
	.cfi_offset %rbx, -24
.Lcfi849:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movb	$0, (%r14,%rsi)
	movq	user_readt(%rip), %rax
	testq	%rax, %rax
	je	.LBB166_2
# BB#1:
	leaq	20(%rsp), %rdx
	movq	%r14, %rdi
	callq	*%rax
	movq	%rax, %rbx
	cmpl	$0, 20(%rsp)
	jne	.LBB166_31
.LBB166_2:
	leaq	1(%r14), %rbx
	cmpb	$45, (%r14)
	cmovneq	%r14, %rbx
	callq	__ctype_b_loc
	movq	(%rax), %rax
	decq	%rbx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB166_3:                              # =>This Inner Loop Header: Depth=1
	movl	%esi, %edx
	movsbq	1(%rbx), %rcx
	incq	%rbx
	movl	$1, %esi
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB166_3
# BB#4:
	cmpb	$46, %cl
	jne	.LBB166_5
	.p2align	4, 0x90
.LBB166_6:                              # %.preheader35
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, %esi
	movsbq	1(%rbx), %rcx
	incq	%rbx
	movl	$1, %edx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB166_6
	jmp	.LBB166_7
.LBB166_5:
	movl	%edx, %esi
.LBB166_7:                              # %.loopexit36
	testl	%esi, %esi
	je	.LBB166_30
# BB#8:
	cmpb	$101, %cl
	jne	.LBB166_16
# BB#9:
	movb	1(%rbx), %cl
	cmpb	$45, %cl
	je	.LBB166_12
# BB#10:
	cmpb	$43, %cl
	jne	.LBB166_11
.LBB166_12:
	movb	2(%rbx), %cl
	addq	$2, %rbx
	jmp	.LBB166_13
.LBB166_11:
	incq	%rbx
.LBB166_13:
	movsbq	%cl, %rcx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB166_30
# BB#14:                                # %.preheader.preheader
	incq	%rbx
	.p2align	4, 0x90
.LBB166_15:                             # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rcx
	incq	%rbx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB166_15
.LBB166_16:                             # %.loopexit
	testb	%cl, %cl
	je	.LBB166_17
.LBB166_30:
	movl	$1, %esi
	movq	%r14, %rdi
	callq	gen_intern
	movq	%rax, %rbx
.LBB166_31:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB166_17:
	xorl	%esi, %esi
	movq	%r14, %rdi
	callq	strtod
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB166_22
# BB#18:
	cvttsd2si	%xmm0, %rax
	cmpq	%rcx, %rax
	jge	.LBB166_22
# BB#19:
	xorps	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm0
	jb	.LBB166_22
# BB#20:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm3, %xmm2
	jne	.LBB166_22
	jp	.LBB166_22
# BB#21:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB166_31
.LBB166_22:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB166_26
# BB#23:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB166_25
# BB#24:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB166_25:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB166_29
.LBB166_26:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB166_28
# BB#27:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB166_28:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB166_29:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm0, 8(%rbx)
	jmp	.LBB166_31
.Lfunc_end166:
	.size	lreadtk, .Lfunc_end166-lreadtk
	.cfi_endproc

	.globl	copy_list
	.p2align	4, 0x90
	.type	copy_list,@function
copy_list:                              # @copy_list
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi850:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi851:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi852:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi853:
	.cfi_def_cfa_offset 48
.Lcfi854:
	.cfi_offset %rbx, -32
.Lcfi855:
	.cfi_offset %r14, -24
.Lcfi856:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	%rbx, 8(%rsp)
	testq	%rbx, %rbx
	je	.LBB167_1
# BB#2:
	leaq	8(%rsp), %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB167_4
# BB#3:
	movl	$.L.str.23, %edi
	xorl	%esi, %esi
	callq	err
.LBB167_4:
	movzwl	2(%rbx), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB167_8
# BB#5:
	cmpl	$1, %ecx
	jne	.LBB167_7
# BB#6:
	movq	8(%rbx), %r15
	jmp	.LBB167_9
.LBB167_1:
	xorl	%ebx, %ebx
	jmp	.LBB167_22
.LBB167_7:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
	movzwl	2(%rbx), %eax
.LBB167_8:
	xorl	%r15d, %r15d
.LBB167_9:
	cwtl
	testl	%eax, %eax
	je	.LBB167_13
# BB#10:
	cmpl	$1, %eax
	jne	.LBB167_12
# BB#11:
	movq	16(%rbx), %rdi
	jmp	.LBB167_14
.LBB167_12:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
.LBB167_13:                             # %cdr.exit
	xorl	%edi, %edi
.LBB167_14:                             # %cdr.exit
	callq	copy_list
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB167_18
# BB#15:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB167_17
# BB#16:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB167_17:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB167_21
.LBB167_18:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB167_20
# BB#19:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB167_20:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB167_21:                             # %cons.exit
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r15, 8(%rbx)
	movq	%r14, 16(%rbx)
.LBB167_22:
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end167:
	.size	copy_list, .Lfunc_end167-copy_list
	.cfi_endproc

	.globl	apropos
	.p2align	4, 0x90
	.type	apropos,@function
apropos:                                # @apropos
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi857:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi858:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi859:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi860:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi861:
	.cfi_def_cfa_offset 48
.Lcfi862:
	.cfi_offset %rbx, -48
.Lcfi863:
	.cfi_offset %r12, -40
.Lcfi864:
	.cfi_offset %r13, -32
.Lcfi865:
	.cfi_offset %r14, -24
.Lcfi866:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	oblistvar(%rip), %r12
	testq	%r12, %r12
	je	.LBB168_1
# BB#2:                                 # %.lr.ph32
	testq	%r14, %r14
	je	.LBB168_9
# BB#3:                                 # %.lr.ph32.split.preheader
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB168_4:                              # %.lr.ph32.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB168_27 Depth 2
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB168_45
# BB#5:                                 #   in Loop: Header=BB168_4 Depth=1
	movq	8(%r12), %rsi
	testq	%rsi, %rsi
	je	.LBB168_25
# BB#6:                                 #   in Loop: Header=BB168_4 Depth=1
	movzwl	2(%rsi), %eax
	cmpl	$13, %eax
	je	.LBB168_24
# BB#7:                                 #   in Loop: Header=BB168_4 Depth=1
	movzwl	%ax, %eax
	cmpl	$3, %eax
	jne	.LBB168_25
# BB#8:                                 #   in Loop: Header=BB168_4 Depth=1
	movq	8(%rsi), %r13
	jmp	.LBB168_26
	.p2align	4, 0x90
.LBB168_25:                             # %.critedge8.i
                                        #   in Loop: Header=BB168_4 Depth=1
	movl	$.L.str.25, %edi
	callq	err
	xorl	%r13d, %r13d
	jmp	.LBB168_26
	.p2align	4, 0x90
.LBB168_24:                             #   in Loop: Header=BB168_4 Depth=1
	movq	16(%rsi), %r13
.LBB168_26:                             # %.lr.ph
                                        #   in Loop: Header=BB168_4 Depth=1
	movq	%r14, %rbx
	.p2align	4, 0x90
.LBB168_27:                             #   Parent Loop BB168_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB168_44
# BB#28:                                #   in Loop: Header=BB168_27 Depth=2
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB168_33
# BB#29:                                #   in Loop: Header=BB168_27 Depth=2
	movzwl	2(%rsi), %eax
	cmpl	$13, %eax
	je	.LBB168_32
# BB#30:                                #   in Loop: Header=BB168_27 Depth=2
	movzwl	%ax, %eax
	cmpl	$3, %eax
	jne	.LBB168_33
# BB#31:                                #   in Loop: Header=BB168_27 Depth=2
	movq	8(%rsi), %rsi
	jmp	.LBB168_34
	.p2align	4, 0x90
.LBB168_33:                             # %.critedge8.i22
                                        #   in Loop: Header=BB168_27 Depth=2
	movl	$.L.str.25, %edi
	callq	err
	xorl	%esi, %esi
	jmp	.LBB168_34
	.p2align	4, 0x90
.LBB168_32:                             #   in Loop: Header=BB168_27 Depth=2
	movq	16(%rsi), %rsi
.LBB168_34:                             # %get_c_string.exit24
                                        #   in Loop: Header=BB168_27 Depth=2
	movq	%r13, %rdi
	callq	strstr
	testq	%rax, %rax
	je	.LBB168_44
# BB#35:                                #   in Loop: Header=BB168_27 Depth=2
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB168_27
# BB#36:                                # %.critedge
                                        #   in Loop: Header=BB168_4 Depth=1
	movq	8(%r12), %r13
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB168_40
# BB#37:                                #   in Loop: Header=BB168_4 Depth=1
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB168_39
# BB#38:                                #   in Loop: Header=BB168_4 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB168_39:                             #   in Loop: Header=BB168_4 Depth=1
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB168_43
.LBB168_40:                             #   in Loop: Header=BB168_4 Depth=1
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB168_42
# BB#41:                                #   in Loop: Header=BB168_4 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB168_42:                             #   in Loop: Header=BB168_4 Depth=1
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB168_43:                             # %cons.exit
                                        #   in Loop: Header=BB168_4 Depth=1
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r13, 8(%rbx)
	movq	%r15, 16(%rbx)
	movq	%rbx, %r15
.LBB168_44:                             # %.critedge.thread
                                        #   in Loop: Header=BB168_4 Depth=1
	movq	16(%r12), %r12
	testq	%r12, %r12
	jne	.LBB168_4
	jmp	.LBB168_45
.LBB168_1:
	xorl	%r15d, %r15d
	jmp	.LBB168_45
.LBB168_9:                              # %.lr.ph32.split.us.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB168_10:                             # %.lr.ph32.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzwl	2(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB168_11
# BB#12:                                #   in Loop: Header=BB168_10 Depth=1
	movq	8(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB168_15
# BB#13:                                #   in Loop: Header=BB168_10 Depth=1
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB168_16
# BB#14:                                #   in Loop: Header=BB168_10 Depth=1
	movzwl	%ax, %eax
	cmpl	$13, %eax
	je	.LBB168_16
.LBB168_15:                             # %.critedge8.i.us
                                        #   in Loop: Header=BB168_10 Depth=1
	movl	$.L.str.25, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%r12), %rbx
.LBB168_16:                             # %.critedge.us
                                        #   in Loop: Header=BB168_10 Depth=1
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB168_17
# BB#20:                                #   in Loop: Header=BB168_10 Depth=1
	movq	heap(%rip), %r15
	cmpq	heap_end(%rip), %r15
	jb	.LBB168_22
# BB#21:                                #   in Loop: Header=BB168_10 Depth=1
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB168_22:                             #   in Loop: Header=BB168_10 Depth=1
	leaq	24(%r15), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB168_23
	.p2align	4, 0x90
.LBB168_17:                             #   in Loop: Header=BB168_10 Depth=1
	movq	freelist(%rip), %r15
	testq	%r15, %r15
	jne	.LBB168_19
# BB#18:                                #   in Loop: Header=BB168_10 Depth=1
	callq	gc_for_newcell
	movq	freelist(%rip), %r15
.LBB168_19:                             #   in Loop: Header=BB168_10 Depth=1
	movq	16(%r15), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB168_23:                             # %cons.exit.us
                                        #   in Loop: Header=BB168_10 Depth=1
	movw	$0, (%r15)
	movw	$1, 2(%r15)
	movq	%rbx, 8(%r15)
	movq	%r14, 16(%r15)
	movq	16(%r12), %r12
	testq	%r12, %r12
	movq	%r15, %r14
	jne	.LBB168_10
	jmp	.LBB168_45
.LBB168_11:
	movq	%r14, %r15
.LBB168_45:                             # %.thread
	movq	%r15, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end168:
	.size	apropos, .Lfunc_end168-apropos
	.cfi_endproc

	.globl	fopen_cg
	.p2align	4, 0x90
	.type	fopen_cg,@function
fopen_cg:                               # @fopen_cg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi867:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi868:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi869:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi870:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi871:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi872:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi873:
	.cfi_def_cfa_offset 320
.Lcfi874:
	.cfi_offset %rbx, -56
.Lcfi875:
	.cfi_offset %r12, -48
.Lcfi876:
	.cfi_offset %r13, -40
.Lcfi877:
	.cfi_offset %r14, -32
.Lcfi878:
	.cfi_offset %r15, -24
.Lcfi879:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	nointerrupt(%rip), %r13
	movq	$1, nointerrupt(%rip)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB169_4
# BB#1:
	movq	heap(%rip), %rbp
	cmpq	heap_end(%rip), %rbp
	jb	.LBB169_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB169_3:
	leaq	24(%rbp), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB169_7
.LBB169_4:
	movq	freelist(%rip), %rbp
	testq	%rbp, %rbp
	jne	.LBB169_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbp
.LBB169_6:
	movq	16(%rbp), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB169_7:                              # %newcell.exit
	movw	$0, (%rbp)
	movw	$17, 2(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*%r12
	movq	%rax, 8(%rbp)
	testq	%rax, %rax
	jne	.LBB169_16
# BB#8:                                 # %safe_strcpy.exit
	movabsq	$2336916790227006575, %rax # imm = 0x206E65706F20746F
	movq	%rax, 7(%rsp)
	movabsq	$8029390801336627043, %rax # imm = 0x6F6E20646C756F63
	movq	%rax, (%rsp)
	movb	$0, 15(%rsp)
	movl	$256, %r12d             # imm = 0x100
	movq	%rsp, %rbx
	xorl	%esi, %esi
	movl	$256, %edx              # imm = 0x100
	movq	%rbx, %rdi
	callq	memchr
	movq	%rax, %rcx
	subq	%rbx, %rcx
	testq	%rax, %rax
	cmoveq	%r12, %rcx
	subq	%rcx, %r12
	je	.LBB169_15
# BB#9:
	movq	%r13, %rbx
	leaq	(%rsp,%rcx), %r15
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r13
	cmpq	%r12, %r13
	jae	.LBB169_13
# BB#10:
	testq	%r13, %r13
	je	.LBB169_11
# BB#12:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	memcpy
	jmp	.LBB169_14
.LBB169_13:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
	decq	%r12
	movq	%r12, %r13
	jmp	.LBB169_14
.LBB169_11:
	xorl	%r13d, %r13d
.LBB169_14:
	movb	$0, (%r15,%r13)
	movq	%rbx, %r13
.LBB169_15:                             # %safe_strcat.exit
	movl	$-1, %edi
	callq	llast_c_errmsg
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	err
.LBB169_16:
	movq	%r14, %rdi
	callq	strlen
	incq	%rax
	movl	$1, %edi
	cmovneq	%rax, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB169_18
# BB#17:
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	callq	err
.LBB169_18:                             # %must_malloc.exit
	movq	%rbx, 16(%rbp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	%r13, nointerrupt(%rip)
	testq	%r13, %r13
	jne	.LBB169_21
# BB#19:                                # %must_malloc.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB169_21
# BB#20:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB169_21:                             # %no_interrupt.exit
	movq	%rbp, %rax
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end169:
	.size	fopen_cg, .Lfunc_end169-fopen_cg
	.cfi_endproc

	.globl	safe_strcpy
	.p2align	4, 0x90
	.type	safe_strcpy,@function
safe_strcpy:                            # @safe_strcpy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi880:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi881:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi882:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi883:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi884:
	.cfi_def_cfa_offset 48
.Lcfi885:
	.cfi_offset %rbx, -40
.Lcfi886:
	.cfi_offset %r12, -32
.Lcfi887:
	.cfi_offset %r14, -24
.Lcfi888:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB170_7
# BB#1:
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %r12
	cmpq	%rbx, %r12
	jae	.LBB170_5
# BB#2:
	testq	%r12, %r12
	je	.LBB170_3
# BB#4:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
	jmp	.LBB170_6
.LBB170_5:
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	decq	%rbx
	movq	%rbx, %r12
	jmp	.LBB170_6
.LBB170_3:
	xorl	%r12d, %r12d
.LBB170_6:
	movb	$0, (%r15,%r12)
.LBB170_7:
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end170:
	.size	safe_strcpy, .Lfunc_end170-safe_strcpy
	.cfi_endproc

	.globl	safe_strcat
	.p2align	4, 0x90
	.type	safe_strcat,@function
safe_strcat:                            # @safe_strcat
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi889:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi890:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi891:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi892:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi893:
	.cfi_def_cfa_offset 48
.Lcfi894:
	.cfi_offset %rbx, -48
.Lcfi895:
	.cfi_offset %r12, -40
.Lcfi896:
	.cfi_offset %r13, -32
.Lcfi897:
	.cfi_offset %r14, -24
.Lcfi898:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %r14
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memchr
	movq	%rax, %rbx
	subq	%r14, %rbx
	testq	%rax, %rax
	cmoveq	%r12, %rbx
	subq	%rbx, %r12
	je	.LBB171_7
# BB#1:
	addq	%r14, %rbx
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r13
	cmpq	%r12, %r13
	jae	.LBB171_5
# BB#2:
	testq	%r13, %r13
	je	.LBB171_3
# BB#4:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r13, %rdx
	callq	memcpy
	jmp	.LBB171_6
.LBB171_5:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	memcpy
	decq	%r12
	movq	%r12, %r13
	jmp	.LBB171_6
.LBB171_3:
	xorl	%r13d, %r13d
.LBB171_6:
	movb	$0, (%rbx,%r13)
.LBB171_7:                              # %safe_strcpy.exit
	movq	%r14, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end171:
	.size	safe_strcat, .Lfunc_end171-safe_strcat
	.cfi_endproc

	.globl	llast_c_errmsg
	.p2align	4, 0x90
	.type	llast_c_errmsg,@function
llast_c_errmsg:                         # @llast_c_errmsg
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi899:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi900:
	.cfi_def_cfa_offset 32
.Lcfi901:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	testl	%ebx, %ebx
	jns	.LBB172_2
# BB#1:
	callq	__errno_location
	movl	(%rax), %ebx
.LBB172_2:
	movl	%ebx, %edi
	callq	strerror
	testq	%rax, %rax
	je	.LBB172_3
# BB#17:
	xorl	%esi, %esi
	movq	%rax, %rdi
	addq	$16, %rsp
	popq	%rbx
	jmp	gen_intern              # TAILCALL
.LBB172_3:
	cvtsi2sdl	%ebx, %xmm2
	movq	inums_dim(%rip), %rax
	testq	%rax, %rax
	jle	.LBB172_8
# BB#4:
	testl	%ebx, %ebx
	js	.LBB172_8
# BB#5:
	movapd	%xmm2, %xmm0
	subsd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB172_8
	jp	.LBB172_8
# BB#6:
	movslq	%ebx, %rcx
	cmpq	%rax, %rcx
	jge	.LBB172_8
# BB#7:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB172_16
.LBB172_8:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB172_12
# BB#9:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB172_11
# BB#10:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB172_11:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB172_15
.LBB172_12:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB172_14
# BB#13:
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB172_14:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB172_15:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB172_16:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end172:
	.size	llast_c_errmsg, .Lfunc_end172-llast_c_errmsg
	.cfi_endproc

	.globl	fopen_c
	.p2align	4, 0x90
	.type	fopen_c,@function
fopen_c:                                # @fopen_c
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	movq	%rdi, %rcx
	movl	$fopen, %edi
	movq	%rcx, %rsi
	movq	%rax, %rdx
	jmp	fopen_cg                # TAILCALL
.Lfunc_end173:
	.size	fopen_c, .Lfunc_end173-fopen_c
	.cfi_endproc

	.globl	fopen_l
	.p2align	4, 0x90
	.type	fopen_l,@function
fopen_l:                                # @fopen_l
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi902:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi903:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi904:
	.cfi_def_cfa_offset 32
.Lcfi905:
	.cfi_offset %rbx, -24
.Lcfi906:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB174_5
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB174_4
# BB#2:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	jne	.LBB174_5
# BB#3:
	movq	8(%rax), %r14
	testq	%rbx, %rbx
	jne	.LBB174_8
	jmp	.LBB174_7
.LBB174_5:                              # %.critedge8.i
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%r14d, %r14d
	testq	%rbx, %rbx
	jne	.LBB174_8
	jmp	.LBB174_7
.LBB174_4:
	movq	16(%rax), %r14
	testq	%rbx, %rbx
	je	.LBB174_7
.LBB174_8:
	movzwl	2(%rbx), %eax
	cmpl	$13, %eax
	je	.LBB174_11
# BB#9:
	movzwl	%ax, %eax
	cmpl	$3, %eax
	jne	.LBB174_12
# BB#10:
	movq	8(%rbx), %rdx
	jmp	.LBB174_13
.LBB174_7:
	movl	$.L.str.133, %edx
	jmp	.LBB174_13
.LBB174_11:
	movq	16(%rbx), %rdx
	jmp	.LBB174_13
.LBB174_12:                             # %.critedge8.i3
	movl	$.L.str.25, %edi
	movq	%rbx, %rsi
	callq	err
	xorl	%edx, %edx
.LBB174_13:                             # %get_c_string.exit5
	movl	$fopen, %edi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fopen_cg                # TAILCALL
.Lfunc_end174:
	.size	fopen_l, .Lfunc_end174-fopen_l
	.cfi_endproc

	.globl	delq
	.p2align	4, 0x90
	.type	delq,@function
delq:                                   # @delq
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi907:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi908:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi909:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi910:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi911:
	.cfi_def_cfa_offset 48
.Lcfi912:
	.cfi_offset %rbx, -40
.Lcfi913:
	.cfi_offset %r12, -32
.Lcfi914:
	.cfi_offset %r14, -24
.Lcfi915:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%r14, (%rsp)
	testq	%rbx, %rbx
	je	.LBB175_1
# BB#2:
	movq	%rsp, %rax
	cmpq	stack_limit_ptr(%rip), %rax
	jae	.LBB175_4
# BB#3:
	movl	$.L.str.23, %edi
	xorl	%esi, %esi
	callq	err
.LBB175_4:
	movzwl	2(%rbx), %eax
	movswl	%ax, %ecx
	testl	%ecx, %ecx
	je	.LBB175_8
# BB#5:
	cmpl	$1, %ecx
	jne	.LBB175_7
# BB#6:
	movq	8(%rbx), %r12
	jmp	.LBB175_9
.LBB175_1:
	xorl	%ebx, %ebx
	jmp	.LBB175_19
.LBB175_7:
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
	movzwl	2(%rbx), %eax
.LBB175_8:
	xorl	%r12d, %r12d
.LBB175_9:
	cwtl
	testl	%eax, %eax
	je	.LBB175_13
# BB#10:
	cmpl	$1, %eax
	jne	.LBB175_12
# BB#11:
	movq	16(%rbx), %rsi
	jmp	.LBB175_14
.LBB175_12:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
.LBB175_13:                             # %cdr.exit
	xorl	%esi, %esi
.LBB175_14:                             # %cdr.exit
	movq	%r14, %rdi
	callq	delq
	movq	%rax, %r15
	cmpq	%r14, %r12
	je	.LBB175_15
# BB#16:
	movzwl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB175_18
# BB#17:                                # %.critedge.i
	movl	$.L.str.30, %edi
	movq	%rbx, %rsi
	callq	err
.LBB175_18:                             # %setcdr.exit
	movq	%r15, 16(%rbx)
	jmp	.LBB175_19
.LBB175_15:
	movq	%r15, %rbx
.LBB175_19:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end175:
	.size	delq, .Lfunc_end175-delq
	.cfi_endproc

	.globl	fclose_l
	.p2align	4, 0x90
	.type	fclose_l,@function
fclose_l:                               # @fclose_l
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi916:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi917:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi918:
	.cfi_def_cfa_offset 32
.Lcfi919:
	.cfi_offset %rbx, -24
.Lcfi920:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	nointerrupt(%rip), %r14
	movq	$1, nointerrupt(%rip)
	testq	%rbx, %rbx
	je	.LBB176_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB176_3
.LBB176_2:                              # %.critedge
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB176_3:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB176_5
# BB#4:
	callq	fclose
	movq	$0, 8(%rbx)
.LBB176_5:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB176_7
# BB#6:
	callq	free
	movq	$0, 16(%rbx)
.LBB176_7:                              # %file_gc_free.exit
	movq	%r14, nointerrupt(%rip)
	testq	%r14, %r14
	jne	.LBB176_10
# BB#8:                                 # %file_gc_free.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB176_10
# BB#9:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB176_10:                             # %no_interrupt.exit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end176:
	.size	fclose_l, .Lfunc_end176-fclose_l
	.cfi_endproc

	.globl	require
	.p2align	4, 0x90
	.type	require,@function
require:                                # @require
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi921:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi922:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi923:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi924:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi925:
	.cfi_def_cfa_offset 48
.Lcfi926:
	.cfi_offset %rbx, -40
.Lcfi927:
	.cfi_offset %r12, -32
.Lcfi928:
	.cfi_offset %r14, -24
.Lcfi929:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$.L.str.141, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	movl	$.L.str.142, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r12
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB177_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB177_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB177_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB177_7
.LBB177_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB177_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB177_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB177_7:                              # %cons.exit
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r12, 8(%rbx)
	movq	$0, 16(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB177_11
# BB#8:
	movq	heap(%rip), %r12
	cmpq	heap_end(%rip), %r12
	jb	.LBB177_10
# BB#9:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB177_10:
	leaq	24(%r12), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB177_14
.LBB177_11:
	movq	freelist(%rip), %r12
	testq	%r12, %r12
	jne	.LBB177_13
# BB#12:
	callq	gc_for_newcell
	movq	freelist(%rip), %r12
.LBB177_13:
	movq	16(%r12), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB177_14:                             # %cons.exit9
	movw	$0, (%r12)
	movw	$1, 2(%r12)
	movq	%r15, 8(%r12)
	movq	%rbx, 16(%r12)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB177_18
# BB#15:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB177_17
# BB#16:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB177_17:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB177_21
.LBB177_18:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB177_20
# BB#19:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB177_20:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB177_21:                             # %cons.exit12
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r14, 8(%rbx)
	movq	%r12, 16(%rbx)
	movq	%rbx, %rdi
	callq	string_append
	testq	%rax, %rax
	je	.LBB177_26
# BB#22:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB177_25
# BB#23:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	jne	.LBB177_26
# BB#24:
	movq	8(%rax), %rdi
	jmp	.LBB177_27
.LBB177_26:                             # %.critedge8.i.i
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%edi, %edi
	jmp	.LBB177_27
.LBB177_25:
	movq	16(%rax), %rdi
.LBB177_27:                             # %intern.exit
	movl	$1, %esi
	callq	gen_intern
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB177_29
# BB#28:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB177_30
.LBB177_29:                             # %.critedge.i
	movl	$.L.str.50, %edi
	movq	%r12, %rsi
	callq	err
.LBB177_30:                             # %symbol_boundp.exit
	movq	16(%r12), %rbx
	movq	unbound_marker(%rip), %rax
	cmpq	%rax, %rbx
	movq	sym_t(%rip), %r14
	je	.LBB177_38
# BB#31:                                # %symbol_boundp.exit
	testq	%r14, %r14
	je	.LBB177_38
# BB#32:
	movzwl	2(%r12), %ecx
	cmpl	$3, %ecx
	je	.LBB177_34
# BB#33:                                # %.critedge.i14
	movl	$.L.str.50, %edi
	movq	%r12, %rsi
	callq	err
	movq	16(%r12), %rbx
	movq	unbound_marker(%rip), %rax
.LBB177_34:
	cmpq	%rax, %rbx
	jne	.LBB177_36
# BB#35:
	movl	$.L.str.49, %edi
	movq	%r12, %rsi
	callq	err
.LBB177_36:                             # %symbol_value.exit
	testq	%rbx, %rbx
	jne	.LBB177_48
# BB#37:                                # %symbol_value.exit._crit_edge
	movq	sym_t(%rip), %r14
.LBB177_38:
	testq	%r15, %r15
	je	.LBB177_43
# BB#39:
	movzwl	2(%r15), %eax
	cmpl	$13, %eax
	je	.LBB177_42
# BB#40:
	movzwl	%ax, %eax
	cmpl	$3, %eax
	jne	.LBB177_43
# BB#41:
	movq	8(%r15), %rdi
	jmp	.LBB177_44
.LBB177_43:                             # %.critedge8.i.i18
	movl	$.L.str.25, %edi
	movq	%r15, %rsi
	callq	err
	xorl	%edi, %edi
	jmp	.LBB177_44
.LBB177_42:
	movq	16(%r15), %rdi
.LBB177_44:                             # %load.exit
	xorl	%edx, %edx
	testq	%r14, %r14
	setne	%dl
	xorl	%esi, %esi
	callq	vload
	testq	%r12, %r12
	movq	sym_t(%rip), %rbx
	je	.LBB177_46
# BB#45:
	movzwl	2(%r12), %eax
	cmpl	$3, %eax
	je	.LBB177_47
.LBB177_46:                             # %.critedge.i17
	movl	$.L.str.92, %edi
	movq	%r12, %rsi
	callq	err
.LBB177_47:                             # %setvar.exit
	movq	%rbx, 16(%r12)
.LBB177_48:
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end177:
	.size	require, .Lfunc_end177-require
	.cfi_endproc

	.globl	load
	.p2align	4, 0x90
	.type	load,@function
load:                                   # @load
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi930:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi931:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi932:
	.cfi_def_cfa_offset 32
.Lcfi933:
	.cfi_offset %rbx, -24
.Lcfi934:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB178_5
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB178_4
# BB#2:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	jne	.LBB178_5
# BB#3:
	movq	8(%rax), %rdi
	jmp	.LBB178_6
.LBB178_5:                              # %.critedge8.i
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%edi, %edi
	jmp	.LBB178_6
.LBB178_4:
	movq	16(%rax), %rdi
.LBB178_6:                              # %get_c_string.exit
	xorl	%esi, %esi
	testq	%rbx, %rbx
	setne	%sil
	xorl	%edx, %edx
	testq	%r14, %r14
	setne	%dl
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	vload                   # TAILCALL
.Lfunc_end178:
	.size	load, .Lfunc_end178-load
	.cfi_endproc

	.globl	save_forms
	.p2align	4, 0x90
	.type	save_forms,@function
save_forms:                             # @save_forms
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi935:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi936:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi937:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi938:
	.cfi_def_cfa_offset 40
	subq	$24, %rsp
.Lcfi939:
	.cfi_def_cfa_offset 64
.Lcfi940:
	.cfi_offset %rbx, -40
.Lcfi941:
	.cfi_offset %r12, -32
.Lcfi942:
	.cfi_offset %r14, -24
.Lcfi943:
	.cfi_offset %r15, -16
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB179_5
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB179_4
# BB#2:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	jne	.LBB179_5
# BB#3:
	movq	8(%rax), %r14
	testq	%r12, %r12
	jne	.LBB179_8
	jmp	.LBB179_7
.LBB179_5:                              # %.critedge8.i
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%r14d, %r14d
	testq	%r12, %r12
	jne	.LBB179_8
	jmp	.LBB179_7
.LBB179_4:
	movq	16(%rax), %r14
	testq	%r12, %r12
	je	.LBB179_7
.LBB179_8:
	movl	$.L.str.144, %r15d
	movl	$.L.str.144, %edi
	xorl	%esi, %esi
	callq	gen_intern
	cmpq	%r12, %rax
	je	.LBB179_10
# BB#9:
	movl	$.L.str.145, %edi
	movq	%r12, %rsi
	callq	err
	xorl	%r15d, %r15d
	cmpq	$3, siod_verbose_level(%rip)
	jge	.LBB179_11
	jmp	.LBB179_23
.LBB179_7:
	movl	$.L.str.143, %r15d
.LBB179_10:
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB179_23
.LBB179_11:
	cmpb	$97, (%r15)
	movl	$.L.str.146, %eax
	movl	$.L.str.147, %edi
	cmoveq	%rax, %rdi
	movq	stdout(%rip), %rsi
	movq	nointerrupt(%rip), %r12
	movq	$1, nointerrupt(%rip)
	callq	fputs
	movq	%r12, nointerrupt(%rip)
	testq	%r12, %r12
	jne	.LBB179_14
# BB#12:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB179_14
# BB#13:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	movq	nointerrupt(%rip), %r12
.LBB179_14:                             # %put_st.exit
	movq	stdout(%rip), %rcx
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.148, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, nointerrupt(%rip)
	testq	%r12, %r12
	jne	.LBB179_17
# BB#15:                                # %put_st.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB179_17
# BB#16:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	movq	nointerrupt(%rip), %r12
.LBB179_17:                             # %put_st.exit28
	movq	stdout(%rip), %rsi
	movq	$1, nointerrupt(%rip)
	movq	%r14, %rdi
	callq	fputs
	movq	%r12, nointerrupt(%rip)
	testq	%r12, %r12
	jne	.LBB179_20
# BB#18:                                # %put_st.exit28
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB179_20
# BB#19:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
	movq	nointerrupt(%rip), %r12
.LBB179_20:                             # %put_st.exit31
	movq	stdout(%rip), %rsi
	movq	$1, nointerrupt(%rip)
	movl	$10, %edi
	callq	fputc
	movq	%r12, nointerrupt(%rip)
	testq	%r12, %r12
	jne	.LBB179_23
# BB#21:                                # %put_st.exit31
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB179_23
# BB#22:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB179_23:                             # %put_st.exit34
	movl	$fopen, %edi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	fopen_cg
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB179_33
# BB#24:                                # %.lr.ph
	movq	8(%r14), %r15
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB179_25:                             # =>This Inner Loop Header: Depth=1
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB179_29
# BB#26:                                #   in Loop: Header=BB179_25 Depth=1
	cmpl	$1, %eax
	jne	.LBB179_28
# BB#27:                                #   in Loop: Header=BB179_25 Depth=1
	movq	8(%rbx), %rdi
	jmp	.LBB179_30
	.p2align	4, 0x90
.LBB179_28:                             #   in Loop: Header=BB179_25 Depth=1
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB179_29:                             #   in Loop: Header=BB179_25 Depth=1
	xorl	%edi, %edi
.LBB179_30:                             #   in Loop: Header=BB179_25 Depth=1
	movq	$0, (%rsp)
	movq	$fputs_fcn, 8(%rsp)
	movq	%r15, 16(%rsp)
	movq	%r12, %rsi
	callq	lprin1g
	movl	$10, %edi
	movq	%r15, %rsi
	callq	_IO_putc
	movswl	2(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB179_31
# BB#48:                                # %cdr.exit.backedge
                                        #   in Loop: Header=BB179_25 Depth=1
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB179_25
	jmp	.LBB179_33
.LBB179_31:
	testl	%eax, %eax
	je	.LBB179_33
# BB#32:
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
.LBB179_33:                             # %cdr.exit._crit_edge
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	testq	%r14, %r14
	je	.LBB179_35
# BB#34:
	movzwl	2(%r14), %eax
	cmpl	$17, %eax
	je	.LBB179_36
.LBB179_35:                             # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%r14, %rsi
	callq	err
.LBB179_36:
	movq	8(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB179_38
# BB#37:
	callq	fclose
	movq	$0, 8(%r14)
.LBB179_38:
	movq	16(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB179_40
# BB#39:
	callq	free
	movq	$0, 16(%r14)
.LBB179_40:                             # %file_gc_free.exit.i
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB179_43
# BB#41:                                # %file_gc_free.exit.i
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB179_43
# BB#42:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB179_43:                             # %fclose_l.exit
	cmpq	$3, siod_verbose_level(%rip)
	jl	.LBB179_47
# BB#44:
	movq	stdout(%rip), %rcx
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.140, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB179_47
# BB#45:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB179_47
# BB#46:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB179_47:                             # %put_st.exit39
	movq	sym_t(%rip), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end179:
	.size	save_forms, .Lfunc_end179-save_forms
	.cfi_endproc

	.globl	quit
	.p2align	4, 0x90
	.type	quit,@function
quit:                                   # @quit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi944:
	.cfi_def_cfa_offset 16
	xorl	%edi, %edi
	xorl	%esi, %esi
	callq	err
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end180:
	.size	quit, .Lfunc_end180-quit
	.cfi_endproc

	.globl	nullp
	.p2align	4, 0x90
	.type	nullp,@function
nullp:                                  # @nullp
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	testq	%rdi, %rdi
	cmoveq	sym_t(%rip), %rax
	retq
.Lfunc_end181:
	.size	nullp, .Lfunc_end181-nullp
	.cfi_endproc

	.globl	lgetc
	.p2align	4, 0x90
	.type	lgetc,@function
lgetc:                                  # @lgetc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi945:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi946:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi947:
	.cfi_def_cfa_offset 32
.Lcfi948:
	.cfi_offset %rbx, -24
.Lcfi949:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB182_2
# BB#1:
	movq	stdin(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB182_7
.LBB182_2:
	testq	%rbx, %rbx
	je	.LBB182_4
# BB#3:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB182_5
.LBB182_4:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB182_5:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB182_7
# BB#6:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %rdi
.LBB182_7:                              # %get_c_file.exit
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	_IO_getc
	movl	%eax, %ebp
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB182_10
# BB#8:                                 # %get_c_file.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB182_10
# BB#9:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB182_10:                             # %f_getc.exit
	cmpl	$-1, %ebp
	je	.LBB182_11
# BB#12:
	cvtsi2sdl	%ebp, %xmm2
	movq	inums_dim(%rip), %rax
	testq	%rax, %rax
	jle	.LBB182_17
# BB#13:
	testl	%ebp, %ebp
	js	.LBB182_17
# BB#14:
	movapd	%xmm2, %xmm0
	subsd	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	ucomisd	%xmm1, %xmm0
	jne	.LBB182_17
	jp	.LBB182_17
# BB#15:
	movslq	%ebp, %rcx
	cmpq	%rax, %rcx
	jge	.LBB182_17
# BB#16:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB182_25
.LBB182_17:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB182_21
# BB#18:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB182_20
# BB#19:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB182_20:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB182_24
.LBB182_11:
	xorl	%ebx, %ebx
	jmp	.LBB182_25
.LBB182_21:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB182_23
# BB#22:
	movsd	%xmm2, (%rsp)           # 8-byte Spill
	callq	gc_for_newcell
	movsd	(%rsp), %xmm2           # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB182_23:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB182_24:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB182_25:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end182:
	.size	lgetc, .Lfunc_end182-lgetc
	.cfi_endproc

	.globl	lungetc
	.p2align	4, 0x90
	.type	lungetc,@function
lungetc:                                # @lungetc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi950:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi951:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi952:
	.cfi_def_cfa_offset 32
.Lcfi953:
	.cfi_offset %rbx, -24
.Lcfi954:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	testq	%rdi, %rdi
	je	.LBB183_9
# BB#1:
	callq	get_c_long
	movq	%rax, %r14
	testq	%rbx, %rbx
	jne	.LBB183_3
# BB#2:
	movq	stdin(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB183_8
.LBB183_3:
	testq	%rbx, %rbx
	je	.LBB183_5
# BB#4:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB183_6
.LBB183_5:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB183_6:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB183_8
# BB#7:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %rsi
.LBB183_8:                              # %get_c_file.exit
	movl	%r14d, %edi
	callq	ungetc
.LBB183_9:
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end183:
	.size	lungetc, .Lfunc_end183-lungetc
	.cfi_endproc

	.globl	lputc
	.p2align	4, 0x90
	.type	lputc,@function
lputc:                                  # @lputc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi955:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi956:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi957:
	.cfi_def_cfa_offset 32
.Lcfi958:
	.cfi_offset %rbx, -24
.Lcfi959:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB184_2
# BB#1:
	movq	stdout(%rip), %rsi
	testq	%rsi, %rsi
	jne	.LBB184_7
.LBB184_2:
	testq	%rbx, %rbx
	je	.LBB184_4
# BB#3:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB184_5
.LBB184_4:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB184_5:
	movq	8(%rbx), %rsi
	testq	%rsi, %rsi
	jne	.LBB184_7
# BB#6:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %rsi
.LBB184_7:                              # %get_c_file.exit
	testq	%r14, %r14
	je	.LBB184_13
# BB#8:
	movzwl	2(%r14), %eax
	cmpl	$13, %eax
	je	.LBB184_12
# BB#9:
	movzwl	%ax, %eax
	cmpl	$3, %eax
	je	.LBB184_14
# BB#10:
	cmpl	$2, %eax
	jne	.LBB184_13
# BB#11:
	cvttsd2si	8(%r14), %edi
	jmp	.LBB184_16
.LBB184_12:
	addq	$16, %r14
	jmp	.LBB184_15
.LBB184_14:
	addq	$8, %r14
.LBB184_15:                             # %get_c_string.exit
	movq	(%r14), %rax
	movsbl	(%rax), %edi
.LBB184_16:
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	callq	_IO_putc
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB184_19
# BB#17:
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB184_19
# BB#18:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB184_19:                             # %no_interrupt.exit
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB184_13:                             # %.critedge8.i
	movl	$.L.str.25, %edi
	movq	%r14, %rsi
	callq	err
.Lfunc_end184:
	.size	lputc, .Lfunc_end184-lputc
	.cfi_endproc

	.globl	lputs
	.p2align	4, 0x90
	.type	lputs,@function
lputs:                                  # @lputs
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi960:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi961:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi962:
	.cfi_def_cfa_offset 32
.Lcfi963:
	.cfi_offset %rbx, -32
.Lcfi964:
	.cfi_offset %r14, -24
.Lcfi965:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB185_2
# BB#1:
	movq	stdout(%rip), %r15
	testq	%r15, %r15
	jne	.LBB185_7
.LBB185_2:
	testq	%rbx, %rbx
	je	.LBB185_4
# BB#3:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB185_5
.LBB185_4:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB185_5:
	movq	8(%rbx), %r15
	testq	%r15, %r15
	jne	.LBB185_7
# BB#6:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %r15
.LBB185_7:                              # %get_c_file.exit
	testq	%r14, %r14
	je	.LBB185_12
# BB#8:
	movzwl	2(%r14), %eax
	cmpl	$13, %eax
	je	.LBB185_11
# BB#9:
	movzwl	%ax, %eax
	cmpl	$3, %eax
	jne	.LBB185_12
# BB#10:
	movq	8(%r14), %rdi
	jmp	.LBB185_13
.LBB185_12:                             # %.critedge8.i
	movl	$.L.str.25, %edi
	movq	%r14, %rsi
	callq	err
	xorl	%edi, %edi
	jmp	.LBB185_13
.LBB185_11:
	movq	16(%r14), %rdi
.LBB185_13:                             # %get_c_string.exit
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movq	%r15, %rsi
	callq	fputs
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB185_16
# BB#14:                                # %get_c_string.exit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB185_16
# BB#15:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	callq	err
.LBB185_16:                             # %fput_st.exit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end185:
	.size	lputs, .Lfunc_end185-lputs
	.cfi_endproc

	.globl	lftell
	.p2align	4, 0x90
	.type	lftell,@function
lftell:                                 # @lftell
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi966:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi967:
	.cfi_def_cfa_offset 32
.Lcfi968:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB186_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB186_3
.LBB186_2:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB186_3:
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB186_5
# BB#4:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %rdi
.LBB186_5:                              # %get_c_file.exit
	callq	ftell
	cvtsi2sdq	%rax, %xmm2
	movq	inums_dim(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB186_10
# BB#6:
	cvttsd2si	%xmm2, %rcx
	cmpq	%rdx, %rcx
	jge	.LBB186_10
# BB#7:
	testq	%rax, %rax
	js	.LBB186_10
# BB#8:
	cvtsi2sdq	%rcx, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB186_10
	jp	.LBB186_10
# BB#9:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB186_18
.LBB186_10:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB186_14
# BB#11:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB186_13
# BB#12:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB186_13:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB186_17
.LBB186_14:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB186_16
# BB#15:
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB186_16:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB186_17:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB186_18:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end186:
	.size	lftell, .Lfunc_end186-lftell
	.cfi_endproc

	.globl	lfseek
	.p2align	4, 0x90
	.type	lfseek,@function
lfseek:                                 # @lfseek
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi969:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi970:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi971:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi972:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi973:
	.cfi_def_cfa_offset 48
.Lcfi974:
	.cfi_offset %rbx, -40
.Lcfi975:
	.cfi_offset %r12, -32
.Lcfi976:
	.cfi_offset %r14, -24
.Lcfi977:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB187_2
# BB#1:
	movzwl	2(%rbx), %eax
	cmpl	$17, %eax
	je	.LBB187_3
.LBB187_2:                              # %.critedge.i
	movl	$.L.str.134, %edi
	movq	%rbx, %rsi
	callq	err
.LBB187_3:
	movq	8(%rbx), %r12
	testq	%r12, %r12
	jne	.LBB187_5
# BB#4:
	movl	$.L.str.151, %edi
	movq	%rbx, %rsi
	callq	err
	movq	8(%rbx), %r12
.LBB187_5:                              # %get_c_file.exit
	movq	%r15, %rdi
	callq	get_c_long
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	get_c_long
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	fseek
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmoveq	sym_t(%rip), %rcx
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end187:
	.size	lfseek, .Lfunc_end187-lfseek
	.cfi_endproc

	.globl	parse_number
	.p2align	4, 0x90
	.type	parse_number,@function
parse_number:                           # @parse_number
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi978:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi979:
	.cfi_def_cfa_offset 32
.Lcfi980:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB188_5
# BB#1:
	movzwl	2(%rax), %ecx
	cmpl	$13, %ecx
	je	.LBB188_4
# BB#2:
	movzwl	%cx, %ecx
	cmpl	$3, %ecx
	jne	.LBB188_5
# BB#3:
	movq	8(%rax), %rdi
	jmp	.LBB188_6
.LBB188_5:                              # %.critedge8.i
	movl	$.L.str.25, %edi
	movq	%rax, %rsi
	callq	err
	xorl	%edi, %edi
	jmp	.LBB188_6
.LBB188_4:
	movq	16(%rax), %rdi
.LBB188_6:                              # %get_c_string.exit
	xorl	%esi, %esi
	callq	strtod
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB188_11
# BB#7:
	cvttsd2si	%xmm0, %rax
	cmpq	%rcx, %rax
	jge	.LBB188_11
# BB#8:
	xorps	%xmm3, %xmm3
	ucomisd	%xmm3, %xmm0
	jb	.LBB188_11
# BB#9:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm0, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm3, %xmm2
	jne	.LBB188_11
	jp	.LBB188_11
# BB#10:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB188_19
.LBB188_11:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB188_15
# BB#12:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB188_14
# BB#13:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
.LBB188_14:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB188_18
.LBB188_15:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB188_17
# BB#16:
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB188_17:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB188_18:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm0, 8(%rbx)
.LBB188_19:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end188:
	.size	parse_number, .Lfunc_end188-parse_number
	.cfi_endproc

	.globl	init_subrs
	.p2align	4, 0x90
	.type	init_subrs,@function
init_subrs:                             # @init_subrs
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi981:
	.cfi_def_cfa_offset 16
	callq	init_subrs_1
	popq	%rax
	jmp	init_subrs_a            # TAILCALL
.Lfunc_end189:
	.size	init_subrs, .Lfunc_end189-init_subrs
	.cfi_endproc

	.globl	init_subrs_1
	.p2align	4, 0x90
	.type	init_subrs_1,@function
init_subrs_1:                           # @init_subrs_1
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi982:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi983:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi984:
	.cfi_def_cfa_offset 32
.Lcfi985:
	.cfi_offset %rbx, -24
.Lcfi986:
	.cfi_offset %r14, -16
	movl	$.L.str.153, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_4
# BB#1:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_3
# BB#2:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_3:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_7
.LBB190_4:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_6
# BB#5:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_6:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_7:                              # %subrcons.exit.i.i
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.153, 8(%rbx)
	movq	$cons, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_9
# BB#8:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_10
.LBB190_9:                              # %.critedge.i.i.i
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_10:                             # %init_subr_2.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.154, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_14
# BB#11:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_13
# BB#12:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_13:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_17
.LBB190_14:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_16
# BB#15:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_16:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_17:                             # %subrcons.exit.i.i3
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.154, 8(%rbx)
	movq	$car, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_19
# BB#18:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_20
.LBB190_19:                             # %.critedge.i.i.i5
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_20:                             # %init_subr_1.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.155, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_24
# BB#21:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_23
# BB#22:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_23:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_27
.LBB190_24:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_26
# BB#25:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_26:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_27:                             # %subrcons.exit.i.i8
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.155, 8(%rbx)
	movq	$cdr, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_29
# BB#28:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_30
.LBB190_29:                             # %.critedge.i.i.i10
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_30:                             # %init_subr_1.exit11
	movq	%rbx, 16(%r14)
	movl	$.L.str.156, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_34
# BB#31:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_33
# BB#32:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_33:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_37
.LBB190_34:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_36
# BB#35:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_36:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_37:                             # %subrcons.exit.i.i14
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.156, 8(%rbx)
	movq	$setcar, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_39
# BB#38:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_40
.LBB190_39:                             # %.critedge.i.i.i16
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_40:                             # %init_subr_2.exit17
	movq	%rbx, 16(%r14)
	movl	$.L.str.157, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_44
# BB#41:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_43
# BB#42:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_43:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_47
.LBB190_44:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_46
# BB#45:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_46:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_47:                             # %subrcons.exit.i.i20
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.157, 8(%rbx)
	movq	$setcdr, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_49
# BB#48:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_50
.LBB190_49:                             # %.critedge.i.i.i22
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_50:                             # %init_subr_2.exit23
	movq	%rbx, 16(%r14)
	movl	$.L.str.158, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_54
# BB#51:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_53
# BB#52:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_53:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_57
.LBB190_54:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_56
# BB#55:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_56:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_57:                             # %subrcons.exit.i.i26
	movw	$0, (%rbx)
	movw	$21, 2(%rbx)
	movq	$.L.str.158, 8(%rbx)
	movq	$plus, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_59
# BB#58:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_60
.LBB190_59:                             # %.critedge.i.i.i28
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_60:                             # %init_subr_2n.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.159, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_64
# BB#61:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_63
# BB#62:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_63:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_67
.LBB190_64:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_66
# BB#65:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_66:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_67:                             # %subrcons.exit.i.i31
	movw	$0, (%rbx)
	movw	$21, 2(%rbx)
	movq	$.L.str.159, 8(%rbx)
	movq	$difference, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_69
# BB#68:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_70
.LBB190_69:                             # %.critedge.i.i.i33
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_70:                             # %init_subr_2n.exit34
	movq	%rbx, 16(%r14)
	movl	$.L.str.141, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_74
# BB#71:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_73
# BB#72:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_73:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_77
.LBB190_74:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_76
# BB#75:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_76:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_77:                             # %subrcons.exit.i.i37
	movw	$0, (%rbx)
	movw	$21, 2(%rbx)
	movq	$.L.str.141, 8(%rbx)
	movq	$ltimes, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_79
# BB#78:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_80
.LBB190_79:                             # %.critedge.i.i.i39
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_80:                             # %init_subr_2n.exit40
	movq	%rbx, 16(%r14)
	movl	$.L.str.137, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_84
# BB#81:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_83
# BB#82:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_83:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_87
.LBB190_84:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_86
# BB#85:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_86:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_87:                             # %subrcons.exit.i.i43
	movw	$0, (%rbx)
	movw	$21, 2(%rbx)
	movq	$.L.str.137, 8(%rbx)
	movq	$Quotient, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_89
# BB#88:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_90
.LBB190_89:                             # %.critedge.i.i.i45
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_90:                             # %init_subr_2n.exit46
	movq	%rbx, 16(%r14)
	movl	$.L.str.160, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_94
# BB#91:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_93
# BB#92:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_93:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_97
.LBB190_94:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_96
# BB#95:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_96:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_97:                             # %subrcons.exit.i.i49
	movw	$0, (%rbx)
	movw	$21, 2(%rbx)
	movq	$.L.str.160, 8(%rbx)
	movq	$lmin, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_99
# BB#98:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_100
.LBB190_99:                             # %.critedge.i.i.i51
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_100:                            # %init_subr_2n.exit52
	movq	%rbx, 16(%r14)
	movl	$.L.str.161, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_104
# BB#101:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_103
# BB#102:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_103:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_107
.LBB190_104:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_106
# BB#105:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_106:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_107:                            # %subrcons.exit.i.i55
	movw	$0, (%rbx)
	movw	$21, 2(%rbx)
	movq	$.L.str.161, 8(%rbx)
	movq	$lmax, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_109
# BB#108:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_110
.LBB190_109:                            # %.critedge.i.i.i57
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_110:                            # %init_subr_2n.exit58
	movq	%rbx, 16(%r14)
	movl	$.L.str.162, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_114
# BB#111:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_113
# BB#112:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_113:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_117
.LBB190_114:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_116
# BB#115:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_116:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_117:                            # %subrcons.exit.i.i61
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.162, 8(%rbx)
	movq	$lllabs, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_119
# BB#118:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_120
.LBB190_119:                            # %.critedge.i.i.i63
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_120:                            # %init_subr_1.exit64
	movq	%rbx, 16(%r14)
	movl	$.L.str.163, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_124
# BB#121:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_123
# BB#122:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_123:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_127
.LBB190_124:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_126
# BB#125:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_126:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_127:                            # %subrcons.exit.i.i67
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.163, 8(%rbx)
	movq	$lsqrt, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_129
# BB#128:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_130
.LBB190_129:                            # %.critedge.i.i.i69
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_130:                            # %init_subr_1.exit70
	movq	%rbx, 16(%r14)
	movl	$.L.str.118, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_134
# BB#131:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_133
# BB#132:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_133:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_137
.LBB190_134:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_136
# BB#135:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_136:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_137:                            # %subrcons.exit.i.i73
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.118, 8(%rbx)
	movq	$greaterp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_139
# BB#138:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_140
.LBB190_139:                            # %.critedge.i.i.i75
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_140:                            # %init_subr_2.exit76
	movq	%rbx, 16(%r14)
	movl	$.L.str.164, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_144
# BB#141:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_143
# BB#142:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_143:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_147
.LBB190_144:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_146
# BB#145:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_146:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_147:                            # %subrcons.exit.i.i79
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.164, 8(%rbx)
	movq	$lessp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_149
# BB#148:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_150
.LBB190_149:                            # %.critedge.i.i.i81
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_150:                            # %init_subr_2.exit82
	movq	%rbx, 16(%r14)
	movl	$.L.str.165, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_154
# BB#151:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_153
# BB#152:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_153:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_157
.LBB190_154:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_156
# BB#155:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_156:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_157:                            # %subrcons.exit.i.i85
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.165, 8(%rbx)
	movq	$greaterEp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_159
# BB#158:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_160
.LBB190_159:                            # %.critedge.i.i.i87
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_160:                            # %init_subr_2.exit88
	movq	%rbx, 16(%r14)
	movl	$.L.str.166, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_164
# BB#161:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_163
# BB#162:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_163:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_167
.LBB190_164:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_166
# BB#165:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_166:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_167:                            # %subrcons.exit.i.i91
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.166, 8(%rbx)
	movq	$lessEp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_169
# BB#168:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_170
.LBB190_169:                            # %.critedge.i.i.i93
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_170:                            # %init_subr_2.exit94
	movq	%rbx, 16(%r14)
	movl	$.L.str.167, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_174
# BB#171:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_173
# BB#172:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_173:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_177
.LBB190_174:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_176
# BB#175:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_176:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_177:                            # %subrcons.exit.i.i97
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.167, 8(%rbx)
	movq	$eq, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_179
# BB#178:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_180
.LBB190_179:                            # %.critedge.i.i.i99
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_180:                            # %init_subr_2.exit100
	movq	%rbx, 16(%r14)
	movl	$.L.str.168, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_184
# BB#181:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_183
# BB#182:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_183:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_187
.LBB190_184:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_186
# BB#185:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_186:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_187:                            # %subrcons.exit.i.i103
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.168, 8(%rbx)
	movq	$eql, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_189
# BB#188:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_190
.LBB190_189:                            # %.critedge.i.i.i105
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_190:                            # %init_subr_2.exit106
	movq	%rbx, 16(%r14)
	movl	$.L.str.169, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_194
# BB#191:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_193
# BB#192:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_193:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_197
.LBB190_194:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_196
# BB#195:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_196:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_197:                            # %subrcons.exit.i.i109
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.169, 8(%rbx)
	movq	$eql, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_199
# BB#198:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_200
.LBB190_199:                            # %.critedge.i.i.i111
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_200:                            # %init_subr_2.exit112
	movq	%rbx, 16(%r14)
	movl	$.L.str.170, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_204
# BB#201:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_203
# BB#202:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_203:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_207
.LBB190_204:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_206
# BB#205:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_206:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_207:                            # %subrcons.exit.i.i115
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.170, 8(%rbx)
	movq	$assq, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_209
# BB#208:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_210
.LBB190_209:                            # %.critedge.i.i.i117
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_210:                            # %init_subr_2.exit118
	movq	%rbx, 16(%r14)
	movl	$.L.str.171, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_214
# BB#211:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_213
# BB#212:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_213:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_217
.LBB190_214:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_216
# BB#215:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_216:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_217:                            # %subrcons.exit.i.i121
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.171, 8(%rbx)
	movq	$delq, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_219
# BB#218:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_220
.LBB190_219:                            # %.critedge.i.i.i123
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_220:                            # %init_subr_2.exit124
	movq	%rbx, 16(%r14)
	movl	$.L.str.172, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_224
# BB#221:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_223
# BB#222:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_223:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_227
.LBB190_224:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_226
# BB#225:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_226:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_227:                            # %subrcons.exit.i.i127
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.172, 8(%rbx)
	movq	$lread, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_229
# BB#228:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_230
.LBB190_229:                            # %.critedge.i.i.i129
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_230:                            # %init_subr_1.exit130
	movq	%rbx, 16(%r14)
	movl	$.L.str.173, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_234
# BB#231:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_233
# BB#232:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_233:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_237
.LBB190_234:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_236
# BB#235:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_236:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_237:                            # %subrcons.exit.i.i133
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.173, 8(%rbx)
	movq	$parser_read, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_239
# BB#238:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_240
.LBB190_239:                            # %.critedge.i.i.i135
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_240:                            # %init_subr_1.exit136
	movq	%rbx, 16(%r14)
	movl	$.L.str.174, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	movq	sym_t(%rip), %r14
	testq	%rbx, %rbx
	je	.LBB190_242
# BB#241:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB190_243
.LBB190_242:                            # %.critedge.i
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB190_243:                            # %setvar.exit
	movq	%r14, 16(%rbx)
	movl	$.L.str.175, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_247
# BB#244:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_246
# BB#245:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_246:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_250
.LBB190_247:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_249
# BB#248:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_249:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_250:                            # %subrcons.exit.i.i139
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	$.L.str.175, 8(%rbx)
	movq	$get_eof_val, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_252
# BB#251:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_253
.LBB190_252:                            # %.critedge.i.i.i141
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_253:                            # %init_subr_0.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.176, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_257
# BB#254:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_256
# BB#255:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_256:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_260
.LBB190_257:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_259
# BB#258:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_259:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_260:                            # %subrcons.exit.i.i144
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.176, 8(%rbx)
	movq	$lprint, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_262
# BB#261:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_263
.LBB190_262:                            # %.critedge.i.i.i146
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_263:                            # %init_subr_2.exit147
	movq	%rbx, 16(%r14)
	movl	$.L.str.177, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_267
# BB#264:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_266
# BB#265:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_266:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_270
.LBB190_267:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_269
# BB#268:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_269:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_270:                            # %subrcons.exit.i.i150
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.177, 8(%rbx)
	movq	$lprin1, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_272
# BB#271:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_273
.LBB190_272:                            # %.critedge.i.i.i152
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_273:                            # %init_subr_2.exit153
	movq	%rbx, 16(%r14)
	movl	$.L.str.178, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_277
# BB#274:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_276
# BB#275:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_276:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_280
.LBB190_277:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_279
# BB#278:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_279:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_280:                            # %subrcons.exit.i.i156
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.178, 8(%rbx)
	movq	$leval, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_282
# BB#281:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_283
.LBB190_282:                            # %.critedge.i.i.i158
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_283:                            # %init_subr_2.exit159
	movq	%rbx, 16(%r14)
	movl	$.L.str.179, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_287
# BB#284:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_286
# BB#285:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_286:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_290
.LBB190_287:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_289
# BB#288:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_289:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_290:                            # %subrcons.exit.i.i162
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.179, 8(%rbx)
	movq	$lapply, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_292
# BB#291:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_293
.LBB190_292:                            # %.critedge.i.i.i164
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_293:                            # %init_subr_2.exit165
	movq	%rbx, 16(%r14)
	movl	$.L.str.180, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_297
# BB#294:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_296
# BB#295:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_296:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_300
.LBB190_297:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_299
# BB#298:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_299:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_300:                            # %subrcons.exit.i.i168
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.180, 8(%rbx)
	movq	$leval_define, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_302
# BB#301:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_303
.LBB190_302:                            # %.critedge.i.i.i170
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_303:                            # %init_fsubr.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.66, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_307
# BB#304:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_306
# BB#305:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_306:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_310
.LBB190_307:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_309
# BB#308:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_309:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_310:                            # %subrcons.exit.i.i173
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.66, 8(%rbx)
	movq	$leval_lambda, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_312
# BB#311:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_313
.LBB190_312:                            # %.critedge.i.i.i175
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_313:                            # %init_fsubr.exit176
	movq	%rbx, 16(%r14)
	movl	$.L.str.181, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_317
# BB#314:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_316
# BB#315:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_316:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_320
.LBB190_317:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_319
# BB#318:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_319:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_320:                            # %subrcons.exit.i.i179
	movw	$0, (%rbx)
	movw	$10, 2(%rbx)
	movq	$.L.str.181, 8(%rbx)
	movq	$leval_if, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_322
# BB#321:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_323
.LBB190_322:                            # %.critedge.i.i.i181
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_323:                            # %init_msubr.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.182, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_327
# BB#324:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_326
# BB#325:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_326:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_330
.LBB190_327:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_329
# BB#328:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_329:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_330:                            # %subrcons.exit.i.i184
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.182, 8(%rbx)
	movq	$leval_while, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_332
# BB#331:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_333
.LBB190_332:                            # %.critedge.i.i.i186
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_333:                            # %init_fsubr.exit187
	movq	%rbx, 16(%r14)
	movl	$.L.str.65, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_337
# BB#334:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_336
# BB#335:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_336:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_340
.LBB190_337:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_339
# BB#338:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_339:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_340:                            # %subrcons.exit.i.i190
	movw	$0, (%rbx)
	movw	$10, 2(%rbx)
	movq	$.L.str.65, 8(%rbx)
	movq	$leval_progn, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_342
# BB#341:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_343
.LBB190_342:                            # %.critedge.i.i.i192
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_343:                            # %init_msubr.exit193
	movq	%rbx, 16(%r14)
	movl	$.L.str.95, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_347
# BB#344:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_346
# BB#345:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_346:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_350
.LBB190_347:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_349
# BB#348:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_349:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_350:                            # %subrcons.exit.i.i196
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.95, 8(%rbx)
	movq	$leval_setq, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_352
# BB#351:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_353
.LBB190_352:                            # %.critedge.i.i.i198
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_353:                            # %init_fsubr.exit199
	movq	%rbx, 16(%r14)
	movl	$.L.str.183, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_357
# BB#354:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_356
# BB#355:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_356:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_360
.LBB190_357:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_359
# BB#358:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_359:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_360:                            # %subrcons.exit.i.i202
	movw	$0, (%rbx)
	movw	$10, 2(%rbx)
	movq	$.L.str.183, 8(%rbx)
	movq	$leval_or, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_362
# BB#361:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_363
.LBB190_362:                            # %.critedge.i.i.i204
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_363:                            # %init_msubr.exit205
	movq	%rbx, 16(%r14)
	movl	$.L.str.184, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_367
# BB#364:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_366
# BB#365:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_366:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_370
.LBB190_367:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_369
# BB#368:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_369:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_370:                            # %subrcons.exit.i.i208
	movw	$0, (%rbx)
	movw	$10, 2(%rbx)
	movq	$.L.str.184, 8(%rbx)
	movq	$leval_and, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_372
# BB#371:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_373
.LBB190_372:                            # %.critedge.i.i.i210
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_373:                            # %init_msubr.exit211
	movq	%rbx, 16(%r14)
	movl	$.L.str.185, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_377
# BB#374:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_376
# BB#375:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_376:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_380
.LBB190_377:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_379
# BB#378:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_379:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_380:                            # %subrcons.exit.i.i214
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.185, 8(%rbx)
	movq	$leval_catch, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_382
# BB#381:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_383
.LBB190_382:                            # %.critedge.i.i.i216
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_383:                            # %init_fsubr.exit217
	movq	%rbx, 16(%r14)
	movl	$.L.str.186, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_387
# BB#384:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_386
# BB#385:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_386:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_390
.LBB190_387:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_389
# BB#388:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_389:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_390:                            # %subrcons.exit.i.i220
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.186, 8(%rbx)
	movq	$lthrow, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_392
# BB#391:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_393
.LBB190_392:                            # %.critedge.i.i.i222
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_393:                            # %init_subr_2.exit223
	movq	%rbx, 16(%r14)
	movl	$.L.str.67, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_397
# BB#394:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_396
# BB#395:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_396:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_400
.LBB190_397:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_399
# BB#398:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_399:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_400:                            # %subrcons.exit.i.i226
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.67, 8(%rbx)
	movq	$leval_quote, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_402
# BB#401:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_403
.LBB190_402:                            # %.critedge.i.i.i228
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_403:                            # %init_fsubr.exit229
	movq	%rbx, 16(%r14)
	movl	$.L.str.187, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_407
# BB#404:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_406
# BB#405:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_406:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_410
.LBB190_407:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_409
# BB#408:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_409:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_410:                            # %subrcons.exit.i.i232
	movw	$0, (%rbx)
	movw	$8, 2(%rbx)
	movq	$.L.str.187, 8(%rbx)
	movq	$apropos, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_412
# BB#411:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_413
.LBB190_412:                            # %.critedge.i.i.i234
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_413:                            # %init_lsubr.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.188, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_417
# BB#414:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_416
# BB#415:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_416:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_420
.LBB190_417:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_419
# BB#418:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_419:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_420:                            # %subrcons.exit.i.i237
	movw	$0, (%rbx)
	movw	$8, 2(%rbx)
	movq	$.L.str.188, 8(%rbx)
	movq	$siod_verbose, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_422
# BB#421:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_423
.LBB190_422:                            # %.critedge.i.i.i239
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_423:                            # %init_lsubr.exit240
	movq	%rbx, 16(%r14)
	movl	$.L.str.189, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_427
# BB#424:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_426
# BB#425:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_426:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_430
.LBB190_427:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_429
# BB#428:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_429:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_430:                            # %subrcons.exit.i.i243
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.189, 8(%rbx)
	movq	$copy_list, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_432
# BB#431:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_433
.LBB190_432:                            # %.critedge.i.i.i245
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_433:                            # %init_subr_1.exit246
	movq	%rbx, 16(%r14)
	movl	$.L.str.190, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_437
# BB#434:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_436
# BB#435:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_436:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_440
.LBB190_437:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_439
# BB#438:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_439:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_440:                            # %subrcons.exit.i.i249
	movw	$0, (%rbx)
	movw	$8, 2(%rbx)
	movq	$.L.str.190, 8(%rbx)
	movq	$gc_status, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_442
# BB#441:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_443
.LBB190_442:                            # %.critedge.i.i.i251
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_443:                            # %init_lsubr.exit252
	movq	%rbx, 16(%r14)
	movl	$.L.str.191, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_447
# BB#444:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_446
# BB#445:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_446:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_450
.LBB190_447:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_449
# BB#448:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_449:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_450:                            # %subrcons.exit.i.i255
	movw	$0, (%rbx)
	movw	$8, 2(%rbx)
	movq	$.L.str.191, 8(%rbx)
	movq	$user_gc, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_452
# BB#451:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_453
.LBB190_452:                            # %.critedge.i.i.i257
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_453:                            # %init_lsubr.exit258
	movq	%rbx, 16(%r14)
	movl	$.L.str.192, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_457
# BB#454:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_456
# BB#455:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_456:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_460
.LBB190_457:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_459
# BB#458:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_459:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_460:                            # %subrcons.exit.i.i261
	movw	$0, (%rbx)
	movw	$7, 2(%rbx)
	movq	$.L.str.192, 8(%rbx)
	movq	$load, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_462
# BB#461:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_463
.LBB190_462:                            # %.critedge.i.i.i263
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_463:                            # %init_subr_3.exit
	movq	%rbx, 16(%r14)
	movl	$.L.str.193, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_467
# BB#464:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_466
# BB#465:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_466:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_470
.LBB190_467:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_469
# BB#468:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_469:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_470:                            # %subrcons.exit.i.i266
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.193, 8(%rbx)
	movq	$require, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_472
# BB#471:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_473
.LBB190_472:                            # %.critedge.i.i.i268
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_473:                            # %init_subr_1.exit269
	movq	%rbx, 16(%r14)
	movl	$.L.str.194, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_477
# BB#474:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_476
# BB#475:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_476:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_480
.LBB190_477:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_479
# BB#478:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_479:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_480:                            # %subrcons.exit.i.i272
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.194, 8(%rbx)
	movq	$consp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_482
# BB#481:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_483
.LBB190_482:                            # %.critedge.i.i.i274
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_483:                            # %init_subr_1.exit275
	movq	%rbx, 16(%r14)
	movl	$.L.str.195, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_487
# BB#484:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_486
# BB#485:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_486:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_490
.LBB190_487:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_489
# BB#488:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_489:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_490:                            # %subrcons.exit.i.i278
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.195, 8(%rbx)
	movq	$symbolp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_492
# BB#491:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_493
.LBB190_492:                            # %.critedge.i.i.i280
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_493:                            # %init_subr_1.exit281
	movq	%rbx, 16(%r14)
	movl	$.L.str.196, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_497
# BB#494:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_496
# BB#495:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_496:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_500
.LBB190_497:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_499
# BB#498:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_499:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_500:                            # %subrcons.exit.i.i284
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.196, 8(%rbx)
	movq	$numberp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_502
# BB#501:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_503
.LBB190_502:                            # %.critedge.i.i.i286
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_503:                            # %init_subr_1.exit287
	movq	%rbx, 16(%r14)
	movl	$.L.str.96, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_507
# BB#504:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_506
# BB#505:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_506:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_510
.LBB190_507:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_509
# BB#508:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_509:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_510:                            # %subrcons.exit.i.i290
	movw	$0, (%rbx)
	movw	$10, 2(%rbx)
	movq	$.L.str.96, 8(%rbx)
	movq	$leval_let, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_512
# BB#511:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_513
.LBB190_512:                            # %.critedge.i.i.i292
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_513:                            # %init_msubr.exit293
	movq	%rbx, 16(%r14)
	movl	$.L.str.58, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_517
# BB#514:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_516
# BB#515:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_516:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_520
.LBB190_517:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_519
# BB#518:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_519:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_520:                            # %subrcons.exit.i.i296
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.58, 8(%rbx)
	movq	$let_macro, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_522
# BB#521:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_523
.LBB190_522:                            # %.critedge.i.i.i298
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_523:                            # %init_subr_1.exit299
	movq	%rbx, 16(%r14)
	movl	$.L.str.60, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_527
# BB#524:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_526
# BB#525:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_526:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_530
.LBB190_527:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_529
# BB#528:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_529:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_530:                            # %subrcons.exit.i.i302
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.60, 8(%rbx)
	movq	$letstar_macro, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_532
# BB#531:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_533
.LBB190_532:                            # %.critedge.i.i.i304
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_533:                            # %init_subr_1.exit305
	movq	%rbx, 16(%r14)
	movl	$.L.str.62, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_537
# BB#534:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_536
# BB#535:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_536:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_540
.LBB190_537:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_539
# BB#538:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_539:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_540:                            # %subrcons.exit.i.i308
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.62, 8(%rbx)
	movq	$letrec_macro, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_542
# BB#541:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_543
.LBB190_542:                            # %.critedge.i.i.i310
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_543:                            # %init_subr_1.exit311
	movq	%rbx, 16(%r14)
	movl	$.L.str.197, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_547
# BB#544:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_546
# BB#545:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_546:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_550
.LBB190_547:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_549
# BB#548:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_549:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_550:                            # %subrcons.exit.i.i314
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.197, 8(%rbx)
	movq	$symbol_boundp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_552
# BB#551:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_553
.LBB190_552:                            # %.critedge.i.i.i316
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_553:                            # %init_subr_2.exit317
	movq	%rbx, 16(%r14)
	movl	$.L.str.198, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_557
# BB#554:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_556
# BB#555:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_556:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_560
.LBB190_557:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_559
# BB#558:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_559:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_560:                            # %subrcons.exit.i.i320
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.198, 8(%rbx)
	movq	$symbol_value, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_562
# BB#561:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_563
.LBB190_562:                            # %.critedge.i.i.i322
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_563:                            # %init_subr_2.exit323
	movq	%rbx, 16(%r14)
	movl	$.L.str.199, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_567
# BB#564:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_566
# BB#565:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_566:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_570
.LBB190_567:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_569
# BB#568:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_569:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_570:                            # %subrcons.exit.i.i326
	movw	$0, (%rbx)
	movw	$7, 2(%rbx)
	movq	$.L.str.199, 8(%rbx)
	movq	$setvar, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_572
# BB#571:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_573
.LBB190_572:                            # %.critedge.i.i.i328
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_573:                            # %init_subr_3.exit329
	movq	%rbx, 16(%r14)
	movl	$.L.str.200, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_577
# BB#574:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_576
# BB#575:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_576:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_580
.LBB190_577:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_579
# BB#578:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_579:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_580:                            # %subrcons.exit.i.i332
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.200, 8(%rbx)
	movq	$leval_tenv, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_582
# BB#581:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_583
.LBB190_582:                            # %.critedge.i.i.i334
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_583:                            # %init_fsubr.exit335
	movq	%rbx, 16(%r14)
	movl	$.L.str.201, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_587
# BB#584:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_586
# BB#585:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_586:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_590
.LBB190_587:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_589
# BB#588:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_589:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_590:                            # %subrcons.exit.i.i338
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.201, 8(%rbx)
	movq	$lerr, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_592
# BB#591:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_593
.LBB190_592:                            # %.critedge.i.i.i340
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_593:                            # %init_subr_2.exit341
	movq	%rbx, 16(%r14)
	movl	$.L.str.20, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_597
# BB#594:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_596
# BB#595:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_596:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_600
.LBB190_597:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_599
# BB#598:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_599:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_600:                            # %subrcons.exit.i.i344
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	$.L.str.20, 8(%rbx)
	movq	$quit, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_602
# BB#601:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_603
.LBB190_602:                            # %.critedge.i.i.i346
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_603:                            # %init_subr_0.exit347
	movq	%rbx, 16(%r14)
	movl	$.L.str.202, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_607
# BB#604:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_606
# BB#605:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_606:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_610
.LBB190_607:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_609
# BB#608:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_609:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_610:                            # %subrcons.exit.i.i350
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.202, 8(%rbx)
	movq	$nullp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_612
# BB#611:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_613
.LBB190_612:                            # %.critedge.i.i.i352
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_613:                            # %init_subr_1.exit353
	movq	%rbx, 16(%r14)
	movl	$.L.str.203, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_617
# BB#614:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_616
# BB#615:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_616:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_620
.LBB190_617:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_619
# BB#618:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_619:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_620:                            # %subrcons.exit.i.i356
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.203, 8(%rbx)
	movq	$nullp, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_622
# BB#621:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_623
.LBB190_622:                            # %.critedge.i.i.i358
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_623:                            # %init_subr_1.exit359
	movq	%rbx, 16(%r14)
	movl	$.L.str.204, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_627
# BB#624:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_626
# BB#625:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_626:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_630
.LBB190_627:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_629
# BB#628:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_629:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_630:                            # %subrcons.exit.i.i362
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.204, 8(%rbx)
	movq	$envlookup, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_632
# BB#631:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_633
.LBB190_632:                            # %.critedge.i.i.i364
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_633:                            # %init_subr_2.exit365
	movq	%rbx, 16(%r14)
	movl	$.L.str.205, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_637
# BB#634:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_636
# BB#635:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_636:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_640
.LBB190_637:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_639
# BB#638:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_639:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_640:                            # %subrcons.exit.i.i368
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.205, 8(%rbx)
	movq	$reverse, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_642
# BB#641:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_643
.LBB190_642:                            # %.critedge.i.i.i370
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_643:                            # %init_subr_1.exit371
	movq	%rbx, 16(%r14)
	movl	$.L.str.206, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_647
# BB#644:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_646
# BB#645:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_646:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_650
.LBB190_647:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_649
# BB#648:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_649:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_650:                            # %subrcons.exit.i.i374
	movw	$0, (%rbx)
	movw	$8, 2(%rbx)
	movq	$.L.str.206, 8(%rbx)
	movq	$symbolconc, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_652
# BB#651:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_653
.LBB190_652:                            # %.critedge.i.i.i376
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_653:                            # %init_lsubr.exit377
	movq	%rbx, 16(%r14)
	movl	$.L.str.207, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_657
# BB#654:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_656
# BB#655:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_656:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_660
.LBB190_657:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_659
# BB#658:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_659:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_660:                            # %subrcons.exit.i.i380
	movw	$0, (%rbx)
	movw	$7, 2(%rbx)
	movq	$.L.str.207, 8(%rbx)
	movq	$save_forms, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_662
# BB#661:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_663
.LBB190_662:                            # %.critedge.i.i.i382
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_663:                            # %init_subr_3.exit383
	movq	%rbx, 16(%r14)
	movl	$.L.str.208, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_667
# BB#664:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_666
# BB#665:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_666:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_670
.LBB190_667:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_669
# BB#668:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_669:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_670:                            # %subrcons.exit.i.i386
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.208, 8(%rbx)
	movq	$fopen_l, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_672
# BB#671:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_673
.LBB190_672:                            # %.critedge.i.i.i388
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_673:                            # %init_subr_2.exit389
	movq	%rbx, 16(%r14)
	movl	$.L.str.209, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_677
# BB#674:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_676
# BB#675:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_676:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_680
.LBB190_677:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_679
# BB#678:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_679:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_680:                            # %subrcons.exit.i.i392
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.209, 8(%rbx)
	movq	$fclose_l, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_682
# BB#681:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_683
.LBB190_682:                            # %.critedge.i.i.i394
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_683:                            # %init_subr_1.exit395
	movq	%rbx, 16(%r14)
	movl	$.L.str.210, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_687
# BB#684:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_686
# BB#685:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_686:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_690
.LBB190_687:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_689
# BB#688:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_689:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_690:                            # %subrcons.exit.i.i398
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.210, 8(%rbx)
	movq	$lgetc, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_692
# BB#691:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_693
.LBB190_692:                            # %.critedge.i.i.i400
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_693:                            # %init_subr_1.exit401
	movq	%rbx, 16(%r14)
	movl	$.L.str.211, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_697
# BB#694:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_696
# BB#695:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_696:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_700
.LBB190_697:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_699
# BB#698:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_699:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_700:                            # %subrcons.exit.i.i404
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.211, 8(%rbx)
	movq	$lungetc, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_702
# BB#701:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_703
.LBB190_702:                            # %.critedge.i.i.i406
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_703:                            # %init_subr_2.exit407
	movq	%rbx, 16(%r14)
	movl	$.L.str.212, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_707
# BB#704:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_706
# BB#705:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_706:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_710
.LBB190_707:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_709
# BB#708:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_709:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_710:                            # %subrcons.exit.i.i410
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.212, 8(%rbx)
	movq	$lputc, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_712
# BB#711:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_713
.LBB190_712:                            # %.critedge.i.i.i412
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_713:                            # %init_subr_2.exit413
	movq	%rbx, 16(%r14)
	movl	$.L.str.213, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_717
# BB#714:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_716
# BB#715:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_716:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_720
.LBB190_717:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_719
# BB#718:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_719:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_720:                            # %subrcons.exit.i.i416
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.213, 8(%rbx)
	movq	$lputs, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_722
# BB#721:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_723
.LBB190_722:                            # %.critedge.i.i.i418
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_723:                            # %init_subr_2.exit419
	movq	%rbx, 16(%r14)
	movl	$.L.str.214, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_727
# BB#724:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_726
# BB#725:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_726:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_730
.LBB190_727:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_729
# BB#728:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_729:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_730:                            # %subrcons.exit.i.i422
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.214, 8(%rbx)
	movq	$lftell, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_732
# BB#731:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_733
.LBB190_732:                            # %.critedge.i.i.i424
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_733:                            # %init_subr_1.exit425
	movq	%rbx, 16(%r14)
	movl	$.L.str.215, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_737
# BB#734:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_736
# BB#735:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_736:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_740
.LBB190_737:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_739
# BB#738:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_739:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_740:                            # %subrcons.exit.i.i428
	movw	$0, (%rbx)
	movw	$7, 2(%rbx)
	movq	$.L.str.215, 8(%rbx)
	movq	$lfseek, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_742
# BB#741:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_743
.LBB190_742:                            # %.critedge.i.i.i430
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_743:                            # %init_subr_3.exit431
	movq	%rbx, 16(%r14)
	movl	$.L.str.216, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_747
# BB#744:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_746
# BB#745:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_746:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_750
.LBB190_747:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_749
# BB#748:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_749:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_750:                            # %subrcons.exit.i.i434
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.216, 8(%rbx)
	movq	$parse_number, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_752
# BB#751:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_753
.LBB190_752:                            # %.critedge.i.i.i436
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_753:                            # %init_subr_1.exit437
	movq	%rbx, 16(%r14)
	movl	$.L.str.217, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_757
# BB#754:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_756
# BB#755:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_756:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_760
.LBB190_757:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_759
# BB#758:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_759:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_760:                            # %subrcons.exit.i.i440
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.217, 8(%rbx)
	movq	$stack_limit, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_762
# BB#761:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_763
.LBB190_762:                            # %.critedge.i.i.i442
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_763:                            # %init_subr_2.exit443
	movq	%rbx, 16(%r14)
	movl	$.L.str.218, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_767
# BB#764:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_766
# BB#765:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_766:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_770
.LBB190_767:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_769
# BB#768:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_769:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_770:                            # %subrcons.exit.i.i446
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.218, 8(%rbx)
	movq	$intern, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_772
# BB#771:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_773
.LBB190_772:                            # %.critedge.i.i.i448
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_773:                            # %init_subr_1.exit449
	movq	%rbx, 16(%r14)
	movl	$.L.str.219, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_777
# BB#774:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_776
# BB#775:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_776:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_780
.LBB190_777:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_779
# BB#778:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_779:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_780:                            # %subrcons.exit.i.i452
	movw	$0, (%rbx)
	movw	$6, 2(%rbx)
	movq	$.L.str.219, 8(%rbx)
	movq	$closure, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_782
# BB#781:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_783
.LBB190_782:                            # %.critedge.i.i.i454
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_783:                            # %init_subr_2.exit455
	movq	%rbx, 16(%r14)
	movl	$.L.str.220, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_787
# BB#784:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_786
# BB#785:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_786:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_790
.LBB190_787:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_789
# BB#788:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_789:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_790:                            # %subrcons.exit.i.i458
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.220, 8(%rbx)
	movq	$closure_code, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_792
# BB#791:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_793
.LBB190_792:                            # %.critedge.i.i.i460
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_793:                            # %init_subr_1.exit461
	movq	%rbx, 16(%r14)
	movl	$.L.str.221, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_797
# BB#794:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_796
# BB#795:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_796:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_800
.LBB190_797:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_799
# BB#798:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_799:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_800:                            # %subrcons.exit.i.i464
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.221, 8(%rbx)
	movq	$closure_env, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_802
# BB#801:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_803
.LBB190_802:                            # %.critedge.i.i.i466
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_803:                            # %init_subr_1.exit467
	movq	%rbx, 16(%r14)
	movl	$.L.str.182, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_807
# BB#804:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_806
# BB#805:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_806:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_810
.LBB190_807:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_809
# BB#808:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_809:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_810:                            # %subrcons.exit.i.i470
	movw	$0, (%rbx)
	movw	$9, 2(%rbx)
	movq	$.L.str.182, 8(%rbx)
	movq	$lwhile, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_812
# BB#811:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_813
.LBB190_812:                            # %.critedge.i.i.i472
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_813:                            # %init_fsubr.exit473
	movq	%rbx, 16(%r14)
	movl	$.L.str.222, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_817
# BB#814:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_816
# BB#815:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_816:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_820
.LBB190_817:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_819
# BB#818:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_819:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_820:                            # %subrcons.exit.i.i476
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.222, 8(%rbx)
	movq	$nreverse, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_822
# BB#821:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_823
.LBB190_822:                            # %.critedge.i.i.i478
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_823:                            # %init_subr_1.exit479
	movq	%rbx, 16(%r14)
	movl	$.L.str.223, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_827
# BB#824:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_826
# BB#825:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_826:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_830
.LBB190_827:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_829
# BB#828:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_829:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_830:                            # %subrcons.exit.i.i482
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	$.L.str.223, 8(%rbx)
	movq	$allocate_aheap, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_832
# BB#831:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_833
.LBB190_832:                            # %.critedge.i.i.i484
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_833:                            # %init_subr_0.exit485
	movq	%rbx, 16(%r14)
	movl	$.L.str.224, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_837
# BB#834:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_836
# BB#835:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_836:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_840
.LBB190_837:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_839
# BB#838:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_839:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_840:                            # %subrcons.exit.i.i488
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.224, 8(%rbx)
	movq	$gc_info, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_842
# BB#841:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_843
.LBB190_842:                            # %.critedge.i.i.i490
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_843:                            # %init_subr_1.exit491
	movq	%rbx, 16(%r14)
	movl	$.L.str.225, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_847
# BB#844:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_846
# BB#845:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_846:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_850
.LBB190_847:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_849
# BB#848:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_849:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_850:                            # %subrcons.exit.i.i494
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	$.L.str.225, 8(%rbx)
	movq	$lruntime, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_852
# BB#851:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_853
.LBB190_852:                            # %.critedge.i.i.i496
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_853:                            # %init_subr_0.exit497
	movq	%rbx, 16(%r14)
	movl	$.L.str.226, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_857
# BB#854:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_856
# BB#855:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_856:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_860
.LBB190_857:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_859
# BB#858:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_859:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_860:                            # %subrcons.exit.i.i500
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	$.L.str.226, 8(%rbx)
	movq	$lrealtime, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_862
# BB#861:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_863
.LBB190_862:                            # %.critedge.i.i.i502
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_863:                            # %init_subr_0.exit503
	movq	%rbx, 16(%r14)
	movl	$.L.str.227, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_867
# BB#864:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_866
# BB#865:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_866:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_870
.LBB190_867:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_869
# BB#868:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_869:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_870:                            # %subrcons.exit.i.i506
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.227, 8(%rbx)
	movq	$caar, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_872
# BB#871:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_873
.LBB190_872:                            # %.critedge.i.i.i508
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_873:                            # %init_subr_1.exit509
	movq	%rbx, 16(%r14)
	movl	$.L.str.228, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_877
# BB#874:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_876
# BB#875:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_876:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_880
.LBB190_877:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_879
# BB#878:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_879:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_880:                            # %subrcons.exit.i.i512
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.228, 8(%rbx)
	movq	$cadr, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_882
# BB#881:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_883
.LBB190_882:                            # %.critedge.i.i.i514
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_883:                            # %init_subr_1.exit515
	movq	%rbx, 16(%r14)
	movl	$.L.str.229, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_887
# BB#884:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_886
# BB#885:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_886:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_890
.LBB190_887:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_889
# BB#888:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_889:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_890:                            # %subrcons.exit.i.i518
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.229, 8(%rbx)
	movq	$cdar, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_892
# BB#891:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_893
.LBB190_892:                            # %.critedge.i.i.i520
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_893:                            # %init_subr_1.exit521
	movq	%rbx, 16(%r14)
	movl	$.L.str.230, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_897
# BB#894:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_896
# BB#895:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_896:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_900
.LBB190_897:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_899
# BB#898:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_899:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_900:                            # %subrcons.exit.i.i524
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.230, 8(%rbx)
	movq	$cddr, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_902
# BB#901:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_903
.LBB190_902:                            # %.critedge.i.i.i526
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_903:                            # %init_subr_1.exit527
	movq	%rbx, 16(%r14)
	movl	$.L.str.231, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_907
# BB#904:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_906
# BB#905:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_906:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_910
.LBB190_907:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_909
# BB#908:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_909:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_910:                            # %subrcons.exit.i.i530
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.231, 8(%rbx)
	movq	$lrand, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_912
# BB#911:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_913
.LBB190_912:                            # %.critedge.i.i.i532
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_913:                            # %init_subr_1.exit533
	movq	%rbx, 16(%r14)
	movl	$.L.str.232, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_917
# BB#914:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_916
# BB#915:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_916:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_920
.LBB190_917:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_919
# BB#918:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_919:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_920:                            # %subrcons.exit.i.i536
	movw	$0, (%rbx)
	movw	$5, 2(%rbx)
	movq	$.L.str.232, 8(%rbx)
	movq	$lsrand, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_922
# BB#921:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_923
.LBB190_922:                            # %.critedge.i.i.i538
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_923:                            # %init_subr_1.exit539
	movq	%rbx, 16(%r14)
	movl	$.L.str.233, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_927
# BB#924:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_926
# BB#925:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_926:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_930
.LBB190_927:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_929
# BB#928:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_929:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_930:                            # %subrcons.exit.i.i542
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	$.L.str.233, 8(%rbx)
	movq	$lllast_c_errmsg, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_932
# BB#931:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_933
.LBB190_932:                            # %.critedge.i.i.i544
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_933:                            # %init_subr_0.exit545
	movq	%rbx, 16(%r14)
	movl	$.L.str.234, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB190_937
# BB#934:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB190_936
# BB#935:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB190_936:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB190_940
.LBB190_937:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB190_939
# BB#938:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB190_939:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB190_940:                            # %subrcons.exit.i.i548
	movw	$0, (%rbx)
	movw	$4, 2(%rbx)
	movq	$.L.str.234, 8(%rbx)
	movq	$os_classification, 16(%rbx)
	testq	%r14, %r14
	je	.LBB190_942
# BB#941:
	movzwl	2(%r14), %eax
	cmpl	$3, %eax
	je	.LBB190_943
.LBB190_942:                            # %.critedge.i.i.i550
	movl	$.L.str.92, %edi
	movq	%r14, %rsi
	callq	err
.LBB190_943:                            # %init_subr_0.exit551
	movq	%rbx, 16(%r14)
	movl	$.L.str.239, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %rbx
	movl	$.L.str.240, %edi
	xorl	%esi, %esi
	callq	gen_intern
	movq	%rax, %r14
	testq	%rbx, %rbx
	je	.LBB190_945
# BB#944:
	movzwl	2(%rbx), %eax
	cmpl	$3, %eax
	je	.LBB190_946
.LBB190_945:                            # %.critedge.i.i
	movl	$.L.str.92, %edi
	movq	%rbx, %rsi
	callq	err
.LBB190_946:                            # %init_slib_version.exit
	movq	%r14, 16(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end190:
	.size	init_subrs_1, .Lfunc_end190-init_subrs_1
	.cfi_endproc

	.globl	closure_code
	.p2align	4, 0x90
	.type	closure_code,@function
closure_code:                           # @closure_code
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end191:
	.size	closure_code, .Lfunc_end191-closure_code
	.cfi_endproc

	.globl	closure_env
	.p2align	4, 0x90
	.type	closure_env,@function
closure_env:                            # @closure_env
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end192:
	.size	closure_env, .Lfunc_end192-closure_env
	.cfi_endproc

	.globl	lwhile
	.p2align	4, 0x90
	.type	lwhile,@function
lwhile:                                 # @lwhile
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi987:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi988:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi989:
	.cfi_def_cfa_offset 32
.Lcfi990:
	.cfi_offset %rbx, -32
.Lcfi991:
	.cfi_offset %r14, -24
.Lcfi992:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB193_5
	.p2align	4, 0x90
.LBB193_1:                              # %.split.us
                                        # =>This Inner Loop Header: Depth=1
	xorl	%edi, %edi
	movq	%r15, %rsi
	callq	leval
	testq	%rax, %rax
	jne	.LBB193_1
	jmp	.LBB193_2
	.p2align	4, 0x90
.LBB193_13:                             #   in Loop: Header=BB193_5 Depth=1
	movl	$.L.str.28, %edi
	movq	%r14, %rsi
	callq	err
	jmp	.LBB193_5
	.p2align	4, 0x90
.LBB193_3:                              # %cdr.exit.backedge
                                        #   in Loop: Header=BB193_15 Depth=2
	movq	16(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB193_5
.LBB193_15:                             # %.lr.ph
                                        #   Parent Loop BB193_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	2(%rbx), %eax
	testl	%eax, %eax
	je	.LBB193_19
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB193_15 Depth=2
	cmpl	$1, %eax
	jne	.LBB193_18
# BB#17:                                #   in Loop: Header=BB193_15 Depth=2
	movq	8(%rbx), %rdi
	jmp	.LBB193_20
	.p2align	4, 0x90
.LBB193_18:                             #   in Loop: Header=BB193_15 Depth=2
	movl	$.L.str.27, %edi
	movq	%rbx, %rsi
	callq	err
.LBB193_19:                             #   in Loop: Header=BB193_15 Depth=2
	xorl	%edi, %edi
.LBB193_20:                             #   in Loop: Header=BB193_15 Depth=2
	movq	%r15, %rsi
	callq	leval
	movswl	2(%rbx), %eax
	cmpl	$1, %eax
	je	.LBB193_3
# BB#21:                                #   in Loop: Header=BB193_5 Depth=1
	testl	%eax, %eax
	je	.LBB193_5
# BB#22:                                #   in Loop: Header=BB193_5 Depth=1
	movl	$.L.str.28, %edi
	movq	%rbx, %rsi
	callq	err
	.p2align	4, 0x90
.LBB193_5:                              # %.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB193_15 Depth 2
	movswl	2(%r14), %eax
	testl	%eax, %eax
	je	.LBB193_9
# BB#6:                                 # %.split
                                        #   in Loop: Header=BB193_5 Depth=1
	cmpl	$1, %eax
	jne	.LBB193_8
# BB#7:                                 #   in Loop: Header=BB193_5 Depth=1
	movq	8(%r14), %rdi
	jmp	.LBB193_10
	.p2align	4, 0x90
.LBB193_8:                              #   in Loop: Header=BB193_5 Depth=1
	movl	$.L.str.27, %edi
	movq	%r14, %rsi
	callq	err
.LBB193_9:                              # %car.exit
                                        #   in Loop: Header=BB193_5 Depth=1
	xorl	%edi, %edi
.LBB193_10:                             # %car.exit
                                        #   in Loop: Header=BB193_5 Depth=1
	movq	%r15, %rsi
	callq	leval
	testq	%rax, %rax
	je	.LBB193_2
# BB#11:                                #   in Loop: Header=BB193_5 Depth=1
	movswl	2(%r14), %eax
	testl	%eax, %eax
	je	.LBB193_5
# BB#12:                                #   in Loop: Header=BB193_5 Depth=1
	cmpl	$1, %eax
	jne	.LBB193_13
# BB#14:                                # %cdr.exit.preheader
                                        #   in Loop: Header=BB193_5 Depth=1
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	jne	.LBB193_15
	jmp	.LBB193_5
.LBB193_2:                              # %.us-lcssa.us
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end193:
	.size	lwhile, .Lfunc_end193-lwhile
	.cfi_endproc

	.globl	nreverse
	.p2align	4, 0x90
	.type	nreverse,@function
nreverse:                               # @nreverse
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB194_1
# BB#2:                                 # %.lr.ph.preheader
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB194_3:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdi, %rax
	movzwl	2(%rax), %edx
	cmpl	$1, %edx
	jne	.LBB194_4
# BB#5:                                 #   in Loop: Header=BB194_3 Depth=1
	movq	16(%rax), %rdi
	movq	%rcx, 16(%rax)
	testq	%rdi, %rdi
	movq	%rax, %rcx
	jne	.LBB194_3
# BB#6:                                 # %.thread
	retq
.LBB194_1:
	xorl	%eax, %eax
	retq
.LBB194_4:
	movq	%rcx, %rax
	retq
.Lfunc_end194:
	.size	nreverse, .Lfunc_end194-nreverse
	.cfi_endproc

	.globl	siod_verbose
	.p2align	4, 0x90
	.type	siod_verbose,@function
siod_verbose:                           # @siod_verbose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi993:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi994:
	.cfi_def_cfa_offset 32
.Lcfi995:
	.cfi_offset %rbx, -16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB195_1
# BB#2:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB195_6
# BB#3:
	cmpl	$1, %ecx
	jne	.LBB195_5
# BB#4:
	movq	8(%rax), %rdi
	jmp	.LBB195_7
.LBB195_1:                              # %._crit_edge
	movq	siod_verbose_level(%rip), %rax
	jmp	.LBB195_8
.LBB195_5:
	movl	$.L.str.27, %edi
	movq	%rax, %rsi
	callq	err
.LBB195_6:                              # %car.exit
	xorl	%edi, %edi
.LBB195_7:                              # %car.exit
	callq	get_c_long
	movq	%rax, siod_verbose_level(%rip)
.LBB195_8:
	cvtsi2sdq	%rax, %xmm2
	movq	inums_dim(%rip), %rdx
	testq	%rdx, %rdx
	jle	.LBB195_13
# BB#9:
	cvttsd2si	%xmm2, %rcx
	cmpq	%rdx, %rcx
	jge	.LBB195_13
# BB#10:
	testq	%rax, %rax
	js	.LBB195_13
# BB#11:
	cvtsi2sdq	%rcx, %xmm0
	movapd	%xmm2, %xmm1
	subsd	%xmm0, %xmm1
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm1
	jne	.LBB195_13
	jp	.LBB195_13
# BB#12:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	jmp	.LBB195_21
.LBB195_13:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB195_17
# BB#14:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB195_16
# BB#15:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB195_16:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB195_20
.LBB195_17:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB195_19
# BB#18:
	movsd	%xmm2, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm2          # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB195_19:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB195_20:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm2, 8(%rbx)
.LBB195_21:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end195:
	.size	siod_verbose, .Lfunc_end195-siod_verbose
	.cfi_endproc

	.globl	siod_verbose_check
	.p2align	4, 0x90
	.type	siod_verbose_check,@function
siod_verbose_check:                     # @siod_verbose_check
	.cfi_startproc
# BB#0:
	movslq	%edi, %rcx
	xorl	%eax, %eax
	cmpq	%rcx, siod_verbose_level(%rip)
	setge	%al
	retq
.Lfunc_end196:
	.size	siod_verbose_check, .Lfunc_end196-siod_verbose_check
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI197_0:
	.quad	4633641066610819072     # double 60
	.text
	.globl	lruntime
	.p2align	4, 0x90
	.type	lruntime,@function
lruntime:                               # @lruntime
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi996:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi997:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi998:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi999:
	.cfi_def_cfa_offset 80
.Lcfi1000:
	.cfi_offset %rbx, -32
.Lcfi1001:
	.cfi_offset %r14, -24
.Lcfi1002:
	.cfi_offset %r15, -16
	leaq	16(%rsp), %rdi
	callq	times
	cvtsi2sdq	16(%rsp), %xmm0
	cvtsi2sdq	24(%rsp), %xmm3
	addsd	%xmm0, %xmm3
	divsd	.LCPI197_0(%rip), %xmm3
	movq	inums_dim(%rip), %rax
	testq	%rax, %rax
	jle	.LBB197_5
# BB#1:
	cvttsd2si	%xmm3, %rcx
	cmpq	%rax, %rcx
	jge	.LBB197_5
# BB#2:
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB197_5
# BB#3:
	cvtsi2sdq	%rcx, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB197_5
	jp	.LBB197_5
# BB#4:                                 # %flocons.exit.thread
	movq	inums(%rip), %rdx
	movq	(%rdx,%rcx,8), %r14
	movsd	gc_time_taken(%rip), %xmm3 # xmm3 = mem[0],zero
	jmp	.LBB197_13
.LBB197_5:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB197_9
# BB#6:
	movq	heap(%rip), %r14
	cmpq	heap_end(%rip), %r14
	jb	.LBB197_8
# BB#7:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB197_8:
	leaq	24(%r14), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB197_12
.LBB197_9:
	movq	freelist(%rip), %r14
	testq	%r14, %r14
	jne	.LBB197_11
# BB#10:
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %r14
.LBB197_11:
	movq	16(%r14), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB197_12:                             # %flocons.exit
	movw	$0, (%r14)
	movw	$2, 2(%r14)
	movsd	%xmm3, 8(%r14)
	movq	inums_dim(%rip), %rax
	movsd	gc_time_taken(%rip), %xmm3 # xmm3 = mem[0],zero
	testq	%rax, %rax
	jle	.LBB197_17
.LBB197_13:
	cvttsd2si	%xmm3, %rcx
	cmpq	%rax, %rcx
	jge	.LBB197_17
# BB#14:
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB197_17
# BB#15:
	xorps	%xmm1, %xmm1
	cvtsi2sdq	%rcx, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB197_17
	jp	.LBB197_17
# BB#16:
	movq	inums(%rip), %rax
	movq	(%rax,%rcx,8), %rbx
	cmpq	$1, gc_kind_copying(%rip)
	je	.LBB197_26
	jmp	.LBB197_29
.LBB197_17:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB197_21
# BB#18:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB197_20
# BB#19:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB197_20:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB197_24
.LBB197_21:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB197_23
# BB#22:
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB197_23:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB197_24:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB197_29
.LBB197_26:
	movq	heap(%rip), %r15
	cmpq	heap_end(%rip), %r15
	jb	.LBB197_28
# BB#27:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB197_28:
	leaq	24(%r15), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB197_32
.LBB197_29:
	movq	freelist(%rip), %r15
	testq	%r15, %r15
	jne	.LBB197_31
# BB#30:
	callq	gc_for_newcell
	movq	freelist(%rip), %r15
.LBB197_31:
	movq	16(%r15), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB197_32:                             # %cons.exit
	movw	$0, (%r15)
	movw	$1, 2(%r15)
	movq	%rbx, 8(%r15)
	movq	$0, 16(%r15)
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB197_36
# BB#33:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB197_35
# BB#34:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	callq	err
.LBB197_35:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB197_39
.LBB197_36:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB197_38
# BB#37:
	callq	gc_for_newcell
	movq	freelist(%rip), %rbx
.LBB197_38:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB197_39:                             # %cons.exit11
	movw	$0, (%rbx)
	movw	$1, 2(%rbx)
	movq	%r14, 8(%rbx)
	movq	%r15, 16(%rbx)
	movq	%rbx, %rax
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end197:
	.size	lruntime, .Lfunc_end197-lruntime
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI198_0:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.text
	.globl	lrealtime
	.p2align	4, 0x90
	.type	lrealtime,@function
lrealtime:                              # @lrealtime
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1003:
	.cfi_def_cfa_offset 16
	subq	$32, %rsp
.Lcfi1004:
	.cfi_def_cfa_offset 48
.Lcfi1005:
	.cfi_offset %rbx, -16
	leaq	16(%rsp), %rdi
	xorl	%esi, %esi
	callq	gettimeofday
	xorpd	%xmm3, %xmm3
	testl	%eax, %eax
	jne	.LBB198_2
# BB#1:
	cvtsi2sdq	16(%rsp), %xmm0
	xorps	%xmm3, %xmm3
	cvtsi2sdq	24(%rsp), %xmm3
	mulsd	.LCPI198_0(%rip), %xmm3
	addsd	%xmm0, %xmm3
.LBB198_2:                              # %myrealtime.exit
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB198_7
# BB#3:
	cvttsd2si	%xmm3, %rax
	cmpq	%rcx, %rax
	jge	.LBB198_7
# BB#4:
	xorpd	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB198_7
# BB#5:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB198_7
	jp	.LBB198_7
# BB#6:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB198_15
.LBB198_7:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB198_11
# BB#8:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB198_10
# BB#9:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	err
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB198_10:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB198_14
.LBB198_11:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB198_13
# BB#12:
	movsd	%xmm3, 8(%rsp)          # 8-byte Spill
	callq	gc_for_newcell
	movsd	8(%rsp), %xmm3          # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB198_13:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB198_14:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
.LBB198_15:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$32, %rsp
	popq	%rbx
	retq
.Lfunc_end198:
	.size	lrealtime, .Lfunc_end198-lrealtime
	.cfi_endproc

	.globl	cdar
	.p2align	4, 0x90
	.type	cdar,@function
cdar:                                   # @cdar
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1006:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rax
	testq	%rax, %rax
	je	.LBB199_10
# BB#1:
	movswl	2(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB199_10
# BB#2:
	cmpl	$1, %ecx
	jne	.LBB199_3
# BB#4:                                 # %car.exit
	movq	8(%rax), %rsi
	testq	%rsi, %rsi
	je	.LBB199_10
# BB#5:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB199_10
# BB#6:
	cmpl	$1, %eax
	jne	.LBB199_8
# BB#7:
	movq	16(%rsi), %rax
	popq	%rcx
	retq
.LBB199_3:
	movl	$.L.str.27, %edi
	movq	%rax, %rsi
.LBB199_9:                              # %cdr.exit
	callq	err
.LBB199_10:                             # %cdr.exit
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB199_8:
	movl	$.L.str.28, %edi
	jmp	.LBB199_9
.Lfunc_end199:
	.size	cdar, .Lfunc_end199-cdar
	.cfi_endproc

	.globl	lrand
	.p2align	4, 0x90
	.type	lrand,@function
lrand:                                  # @lrand
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1007:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1008:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1009:
	.cfi_def_cfa_offset 32
.Lcfi1010:
	.cfi_offset %rbx, -24
.Lcfi1011:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	rand
	testq	%rbx, %rbx
	je	.LBB200_1
# BB#2:
	movslq	%eax, %r14
	movq	%rbx, %rdi
	callq	get_c_long
	movq	%rax, %rcx
	movq	%r14, %rax
	cqto
	idivq	%rcx
	cvtsi2sdq	%rdx, %xmm3
	jmp	.LBB200_3
.LBB200_1:
	cvtsi2sdl	%eax, %xmm3
.LBB200_3:
	movq	inums_dim(%rip), %rcx
	testq	%rcx, %rcx
	jle	.LBB200_8
# BB#4:
	cvttsd2si	%xmm3, %rax
	cmpq	%rcx, %rax
	jge	.LBB200_8
# BB#5:
	xorps	%xmm0, %xmm0
	ucomisd	%xmm0, %xmm3
	jb	.LBB200_8
# BB#6:
	cvtsi2sdq	%rax, %xmm1
	movapd	%xmm3, %xmm2
	subsd	%xmm1, %xmm2
	ucomisd	%xmm0, %xmm2
	jne	.LBB200_8
	jp	.LBB200_8
# BB#7:
	movq	inums(%rip), %rcx
	movq	(%rcx,%rax,8), %rbx
	jmp	.LBB200_16
.LBB200_8:
	cmpq	$1, gc_kind_copying(%rip)
	jne	.LBB200_12
# BB#9:
	movq	heap(%rip), %rbx
	cmpq	heap_end(%rip), %rbx
	jb	.LBB200_11
# BB#10:
	movl	$.L.str.26, %edi
	xorl	%esi, %esi
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	err
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
.LBB200_11:
	leaq	24(%rbx), %rax
	movq	%rax, heap(%rip)
	jmp	.LBB200_15
.LBB200_12:
	movq	freelist(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB200_14
# BB#13:
	movsd	%xmm3, (%rsp)           # 8-byte Spill
	callq	gc_for_newcell
	movsd	(%rsp), %xmm3           # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movq	freelist(%rip), %rbx
.LBB200_14:
	movq	16(%rbx), %rax
	movq	%rax, freelist(%rip)
	incq	gc_cells_allocated(%rip)
.LBB200_15:
	movw	$0, (%rbx)
	movw	$2, 2(%rbx)
	movsd	%xmm3, 8(%rbx)
.LBB200_16:                             # %flocons.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end200:
	.size	lrand, .Lfunc_end200-lrand
	.cfi_endproc

	.globl	lsrand
	.p2align	4, 0x90
	.type	lsrand,@function
lsrand:                                 # @lsrand
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1012:
	.cfi_def_cfa_offset 16
	callq	get_c_long
	movl	%eax, %edi
	callq	srand
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end201:
	.size	lsrand, .Lfunc_end201-lsrand
	.cfi_endproc

	.globl	a_true_value
	.p2align	4, 0x90
	.type	a_true_value,@function
a_true_value:                           # @a_true_value
	.cfi_startproc
# BB#0:
	movq	sym_t(%rip), %rax
	retq
.Lfunc_end202:
	.size	a_true_value, .Lfunc_end202-a_true_value
	.cfi_endproc

	.globl	poparg
	.p2align	4, 0x90
	.type	poparg,@function
poparg:                                 # @poparg
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1013:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1014:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1015:
	.cfi_def_cfa_offset 32
.Lcfi1016:
	.cfi_offset %rbx, -24
.Lcfi1017:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB203_12
# BB#1:
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB203_2
# BB#3:
	cmpl	$1, %eax
	jne	.LBB203_5
# BB#4:
	movq	8(%rsi), %rbx
	jmp	.LBB203_6
.LBB203_2:
	xorl	%ebx, %ebx
	jmp	.LBB203_6
.LBB203_5:                              # %car.exit
	movl	$.L.str.27, %edi
	callq	err
	movq	(%r14), %rsi
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	movl	$0, %eax
	je	.LBB203_11
.LBB203_6:                              # %car.exit.thread
	movswl	2(%rsi), %eax
	testl	%eax, %eax
	je	.LBB203_10
# BB#7:                                 # %car.exit.thread
	cmpl	$1, %eax
	jne	.LBB203_9
# BB#8:
	movq	16(%rsi), %rax
	jmp	.LBB203_11
.LBB203_9:
	movl	$.L.str.28, %edi
	callq	err
.LBB203_10:                             # %cdr.exit
	xorl	%eax, %eax
.LBB203_11:                             # %cdr.exit
	movq	%rax, (%r14)
.LBB203_12:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end203:
	.size	poparg, .Lfunc_end203-poparg
	.cfi_endproc

	.globl	last_c_errmsg
	.p2align	4, 0x90
	.type	last_c_errmsg,@function
last_c_errmsg:                          # @last_c_errmsg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1018:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1019:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1020:
	.cfi_def_cfa_offset 32
.Lcfi1021:
	.cfi_offset %rbx, -24
.Lcfi1022:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	testl	%ebp, %ebp
	jns	.LBB204_2
# BB#1:
	callq	__errno_location
	movl	(%rax), %ebp
.LBB204_2:
	movl	%ebp, %edi
	callq	strerror
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB204_4
# BB#3:
	movl	$last_c_errmsg.serrmsg, %ebx
	movl	$last_c_errmsg.serrmsg, %edi
	movl	$.L.str.152, %esi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	sprintf
.LBB204_4:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end204:
	.size	last_c_errmsg, .Lfunc_end204-last_c_errmsg
	.cfi_endproc

	.globl	lllast_c_errmsg
	.p2align	4, 0x90
	.type	lllast_c_errmsg,@function
lllast_c_errmsg:                        # @lllast_c_errmsg
	.cfi_startproc
# BB#0:
	movl	$-1, %edi
	jmp	llast_c_errmsg          # TAILCALL
.Lfunc_end205:
	.size	lllast_c_errmsg, .Lfunc_end205-lllast_c_errmsg
	.cfi_endproc

	.globl	safe_strlen
	.p2align	4, 0x90
	.type	safe_strlen,@function
safe_strlen:                            # @safe_strlen
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi1023:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1024:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi1025:
	.cfi_def_cfa_offset 32
.Lcfi1026:
	.cfi_offset %rbx, -24
.Lcfi1027:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	xorl	%esi, %esi
	movq	%r14, %rdx
	callq	memchr
	movq	%rax, %rcx
	subq	%rbx, %rcx
	testq	%rax, %rax
	cmoveq	%r14, %rcx
	movq	%rcx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end206:
	.size	safe_strlen, .Lfunc_end206-safe_strlen
	.cfi_endproc

	.p2align	4, 0x90
	.type	parser_read,@function
parser_read:                            # @parser_read
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi1028:
	.cfi_def_cfa_offset 16
	movl	$.L.str.172, %edi
	xorl	%esi, %esi
	callq	gen_intern
	xorl	%esi, %esi
	movq	%rax, %rdi
	popq	%rax
	jmp	leval                   # TAILCALL
.Lfunc_end207:
	.size	parser_read, .Lfunc_end207-parser_read
	.cfi_endproc

	.p2align	4, 0x90
	.type	os_classification,@function
os_classification:                      # @os_classification
	.cfi_startproc
# BB#0:
	movl	$.L.str.238, %edi
	xorl	%esi, %esi
	jmp	gen_intern              # TAILCALL
.Lfunc_end208:
	.size	os_classification, .Lfunc_end208-os_classification
	.cfi_endproc

	.globl	err0
	.p2align	4, 0x90
	.type	err0,@function
err0:                                   # @err0
	.cfi_startproc
# BB#0:
	movl	$.L.str.235, %edi
	xorl	%esi, %esi
	jmp	err                     # TAILCALL
.Lfunc_end209:
	.size	err0, .Lfunc_end209-err0
	.cfi_endproc

	.globl	pr
	.p2align	4, 0x90
	.type	pr,@function
pr:                                     # @pr
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1029:
	.cfi_def_cfa_offset 16
.Lcfi1030:
	.cfi_offset %rbx, -16
	movq	nheaps(%rip), %r9
	testq	%r9, %r9
	jle	.LBB210_9
# BB#1:                                 # %.lr.ph.i
	testq	%rdi, %rdi
	je	.LBB210_9
# BB#2:                                 # %.lr.ph.split.i.preheader
	movq	heaps(%rip), %r8
	movq	heap_size(%rip), %rax
	xorl	%esi, %esi
	leaq	(%rax,%rax,2), %r10
	movabsq	$-6148914691236517205, %r11 # imm = 0xAAAAAAAAAAAAAAAB
	movabsq	$9223372036854775800, %rbx # imm = 0x7FFFFFFFFFFFFFF8
	.p2align	4, 0x90
.LBB210_3:                              # %.lr.ph.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r8,%rsi,8), %rax
	cmpq	%rdi, %rax
	ja	.LBB210_8
# BB#4:                                 # %.lr.ph.split.i
                                        #   in Loop: Header=BB210_3 Depth=1
	testq	%rax, %rax
	je	.LBB210_8
# BB#5:                                 # %.lr.ph.split.i
                                        #   in Loop: Header=BB210_3 Depth=1
	leaq	(%rax,%r10,8), %rcx
	cmpq	%rdi, %rcx
	jbe	.LBB210_8
# BB#6:                                 #   in Loop: Header=BB210_3 Depth=1
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	%rcx, %rax
	mulq	%r11
	shrq	%rdx
	andq	%rbx, %rdx
	leaq	(%rdx,%rdx,2), %rax
	cmpq	%rax, %rcx
	jne	.LBB210_8
# BB#7:                                 #   in Loop: Header=BB210_3 Depth=1
	movzwl	2(%rdi), %eax
	cmpl	$12, %eax
	jne	.LBB210_12
	.p2align	4, 0x90
.LBB210_8:                              #   in Loop: Header=BB210_3 Depth=1
	incq	%rsi
	cmpq	%r9, %rsi
	jl	.LBB210_3
.LBB210_9:                              # %.loopexit
	movq	stdout(%rip), %rcx
	movq	nointerrupt(%rip), %rbx
	movq	$1, nointerrupt(%rip)
	movl	$.L.str.236, %edi
	movl	$8, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, nointerrupt(%rip)
	testq	%rbx, %rbx
	jne	.LBB210_11
# BB#10:                                # %.loopexit
	cmpq	$1, interrupt_differed(%rip)
	jne	.LBB210_11
# BB#13:
	movq	$0, interrupt_differed(%rip)
	movl	$.L.str.13, %edi
	xorl	%esi, %esi
	popq	%rbx
	jmp	err                     # TAILCALL
.LBB210_11:                             # %put_st.exit
	popq	%rbx
	retq
.LBB210_12:                             # %looks_pointerp.exit
	xorl	%esi, %esi
	popq	%rbx
	jmp	lprint                  # TAILCALL
.Lfunc_end210:
	.size	pr, .Lfunc_end210-pr
	.cfi_endproc

	.globl	prp
	.p2align	4, 0x90
	.type	prp,@function
prp:                                    # @prp
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB211_1
# BB#2:
	movq	(%rdi), %rdi
	jmp	pr                      # TAILCALL
.LBB211_1:
	retq
.Lfunc_end211:
	.size	prp, .Lfunc_end211-prp
	.cfi_endproc

	.p2align	4, 0x90
	.type	rcsp_puts,@function
rcsp_puts:                              # @rcsp_puts
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1031:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1032:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi1033:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi1034:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi1035:
	.cfi_def_cfa_offset 48
.Lcfi1036:
	.cfi_offset %rbx, -48
.Lcfi1037:
	.cfi_offset %r12, -40
.Lcfi1038:
	.cfi_offset %r13, -32
.Lcfi1039:
	.cfi_offset %r14, -24
.Lcfi1040:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	callq	strlen
	movq	%rax, %r15
	movq	(%r12), %rdi
	movq	8(%r12), %r13
	subq	%rdi, %r13
	cmpq	%r13, %r15
	movq	%r15, %rbx
	cmovgq	%r13, %rbx
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movq	(%r12), %rax
	leaq	(%rax,%rbx), %rcx
	cmpq	%r13, %r15
	movq	%rcx, (%r12)
	movb	$0, (%rax,%rbx)
	jle	.LBB212_2
# BB#1:
	movl	$.L.str.237, %edi
	xorl	%esi, %esi
	callq	err
.LBB212_2:
	movl	$1, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end212:
	.size	rcsp_puts, .Lfunc_end212-rcsp_puts
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"3.5 5-MAY-97"
	.size	.L.str, 13

	.type	nheaps,@object          # @nheaps
	.data
	.globl	nheaps
	.p2align	3
nheaps:
	.quad	2                       # 0x2
	.size	nheaps, 8

	.type	heap_size,@object       # @heap_size
	.globl	heap_size
	.p2align	3
heap_size:
	.quad	5000                    # 0x1388
	.size	heap_size, 8

	.type	gc_status_flag,@object  # @gc_status_flag
	.globl	gc_status_flag
	.p2align	3
gc_status_flag:
	.quad	1                       # 0x1
	.size	gc_status_flag, 8

	.type	init_file,@object       # @init_file
	.bss
	.globl	init_file
	.p2align	3
init_file:
	.quad	0
	.size	init_file, 8

	.type	tkbuffer,@object        # @tkbuffer
	.globl	tkbuffer
	.p2align	3
tkbuffer:
	.quad	0
	.size	tkbuffer, 8

	.type	gc_kind_copying,@object # @gc_kind_copying
	.globl	gc_kind_copying
	.p2align	3
gc_kind_copying:
	.quad	0                       # 0x0
	.size	gc_kind_copying, 8

	.type	gc_cells_allocated,@object # @gc_cells_allocated
	.globl	gc_cells_allocated
	.p2align	3
gc_cells_allocated:
	.quad	0                       # 0x0
	.size	gc_cells_allocated, 8

	.type	stack_start_ptr,@object # @stack_start_ptr
	.globl	stack_start_ptr
	.p2align	3
stack_start_ptr:
	.quad	0
	.size	stack_start_ptr, 8

	.type	errjmp_ok,@object       # @errjmp_ok
	.globl	errjmp_ok
	.p2align	3
errjmp_ok:
	.quad	0                       # 0x0
	.size	errjmp_ok, 8

	.type	nointerrupt,@object     # @nointerrupt
	.data
	.globl	nointerrupt
	.p2align	3
nointerrupt:
	.quad	1                       # 0x1
	.size	nointerrupt, 8

	.type	interrupt_differed,@object # @interrupt_differed
	.bss
	.globl	interrupt_differed
	.p2align	3
interrupt_differed:
	.quad	0                       # 0x0
	.size	interrupt_differed, 8

	.type	oblistvar,@object       # @oblistvar
	.globl	oblistvar
	.p2align	3
oblistvar:
	.quad	0
	.size	oblistvar, 8

	.type	sym_t,@object           # @sym_t
	.globl	sym_t
	.p2align	3
sym_t:
	.quad	0
	.size	sym_t, 8

	.type	eof_val,@object         # @eof_val
	.globl	eof_val
	.p2align	3
eof_val:
	.quad	0
	.size	eof_val, 8

	.type	sym_errobj,@object      # @sym_errobj
	.globl	sym_errobj
	.p2align	3
sym_errobj:
	.quad	0
	.size	sym_errobj, 8

	.type	sym_catchall,@object    # @sym_catchall
	.globl	sym_catchall
	.p2align	3
sym_catchall:
	.quad	0
	.size	sym_catchall, 8

	.type	sym_progn,@object       # @sym_progn
	.globl	sym_progn
	.p2align	3
sym_progn:
	.quad	0
	.size	sym_progn, 8

	.type	sym_lambda,@object      # @sym_lambda
	.globl	sym_lambda
	.p2align	3
sym_lambda:
	.quad	0
	.size	sym_lambda, 8

	.type	sym_quote,@object       # @sym_quote
	.globl	sym_quote
	.p2align	3
sym_quote:
	.quad	0
	.size	sym_quote, 8

	.type	sym_dot,@object         # @sym_dot
	.globl	sym_dot
	.p2align	3
sym_dot:
	.quad	0
	.size	sym_dot, 8

	.type	sym_after_gc,@object    # @sym_after_gc
	.globl	sym_after_gc
	.p2align	3
sym_after_gc:
	.quad	0
	.size	sym_after_gc, 8

	.type	sym_eval_history_ptr,@object # @sym_eval_history_ptr
	.globl	sym_eval_history_ptr
	.p2align	3
sym_eval_history_ptr:
	.quad	0
	.size	sym_eval_history_ptr, 8

	.type	unbound_marker,@object  # @unbound_marker
	.globl	unbound_marker
	.p2align	3
unbound_marker:
	.quad	0
	.size	unbound_marker, 8

	.type	obarray_dim,@object     # @obarray_dim
	.data
	.globl	obarray_dim
	.p2align	3
obarray_dim:
	.quad	100                     # 0x64
	.size	obarray_dim, 8

	.type	catch_framep,@object    # @catch_framep
	.bss
	.globl	catch_framep
	.p2align	3
catch_framep:
	.quad	0
	.size	catch_framep, 8

	.type	repl_puts,@object       # @repl_puts
	.globl	repl_puts
	.p2align	3
repl_puts:
	.quad	0
	.size	repl_puts, 8

	.type	repl_read,@object       # @repl_read
	.globl	repl_read
	.p2align	3
repl_read:
	.quad	0
	.size	repl_read, 8

	.type	repl_eval,@object       # @repl_eval
	.globl	repl_eval
	.p2align	3
repl_eval:
	.quad	0
	.size	repl_eval, 8

	.type	repl_print,@object      # @repl_print
	.globl	repl_print
	.p2align	3
repl_print:
	.quad	0
	.size	repl_print, 8

	.type	inums_dim,@object       # @inums_dim
	.data
	.globl	inums_dim
	.p2align	3
inums_dim:
	.quad	256                     # 0x100
	.size	inums_dim, 8

	.type	user_types,@object      # @user_types
	.bss
	.globl	user_types
	.p2align	3
user_types:
	.quad	0
	.size	user_types, 8

	.type	user_tc_next,@object    # @user_tc_next
	.data
	.globl	user_tc_next
	.p2align	3
user_tc_next:
	.quad	50                      # 0x32
	.size	user_tc_next, 8

	.type	protected_registers,@object # @protected_registers
	.bss
	.globl	protected_registers
	.p2align	3
protected_registers:
	.quad	0
	.size	protected_registers, 8

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	user_ch_readm,@object   # @user_ch_readm
	.data
	.globl	user_ch_readm
	.p2align	3
user_ch_readm:
	.quad	.L.str.1
	.size	user_ch_readm, 8

	.type	user_te_readm,@object   # @user_te_readm
	.globl	user_te_readm
	.p2align	3
user_te_readm:
	.quad	.L.str.1
	.size	user_te_readm, 8

	.type	user_readm,@object      # @user_readm
	.bss
	.globl	user_readm
	.p2align	3
user_readm:
	.quad	0
	.size	user_readm, 8

	.type	user_readt,@object      # @user_readt
	.globl	user_readt
	.p2align	3
user_readt:
	.quad	0
	.size	user_readt, 8

	.type	fatal_exit_hook,@object # @fatal_exit_hook
	.globl	fatal_exit_hook
	.p2align	3
fatal_exit_hook:
	.quad	0
	.size	fatal_exit_hook, 8

	.type	stack_limit_ptr,@object # @stack_limit_ptr
	.globl	stack_limit_ptr
	.p2align	3
stack_limit_ptr:
	.quad	0
	.size	stack_limit_ptr, 8

	.type	stack_size,@object      # @stack_size
	.data
	.globl	stack_size
	.p2align	3
stack_size:
	.quad	50000                   # 0xc350
	.size	stack_size, 8

	.type	siod_verbose_level,@object # @siod_verbose_level
	.globl	siod_verbose_level
	.p2align	3
siod_verbose_level:
	.quad	4                       # 0x4
	.size	siod_verbose_level, 8

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"/usr/local/lib/siod"
	.size	.L.str.2, 20

	.type	siod_lib,@object        # @siod_lib
	.data
	.globl	siod_lib
	.p2align	3
siod_lib:
	.quad	.L.str.2
	.size	siod_lib, 8

	.type	process_cla.siod_lib_set,@object # @process_cla.siod_lib_set
	.local	process_cla.siod_lib_set
	.comm	process_cla.siod_lib_set,1,4
	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"SIOD_LIB"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"bad arg: %s\n"
	.size	.L.str.4, 13

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Welcome to SIOD, Scheme In One Defun, Version %s\n"
	.size	.L.str.5, 50

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"%ld heaps. size = %ld cells, %ld bytes. %ld inums. GC is %s\n"
	.size	.L.str.7, 61

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"stop and copy"
	.size	.L.str.8, 14

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"mark and sweep"
	.size	.L.str.9, 15

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"heaps[0] at %p, heaps[1] at %p\n"
	.size	.L.str.10, 32

	.type	heaps,@object           # @heaps
	.comm	heaps,8,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"heaps[0] at %p\n"
	.size	.L.str.11, 16

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"floating point exception"
	.size	.L.str.12, 25

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"control-c interrupt"
	.size	.L.str.13, 20

	.type	repl_driver.osigint,@object # @repl_driver.osigint
	.local	repl_driver.osigint
	.comm	repl_driver.osigint,8,8
	.type	repl_driver.osigfpe,@object # @repl_driver.osigfpe
	.local	repl_driver.osigfpe
	.comm	repl_driver.osigfpe,8,8
	.type	errjmp,@object          # @errjmp
	.comm	errjmp,200,16
	.type	repl_c_string_print_len,@object # @repl_c_string_print_len
	.local	repl_c_string_print_len
	.comm	repl_c_string_print_len,8,8
	.type	repl_c_string_out,@object # @repl_c_string_out
	.local	repl_c_string_out
	.comm	repl_c_string_out,8,8
	.type	repl_c_string_arg,@object # @repl_c_string_arg
	.local	repl_c_string_arg
	.comm	repl_c_string_arg,8,8
	.type	repl_c_string_flag,@object # @repl_c_string_flag
	.local	repl_c_string_flag
	.comm	repl_c_string_flag,1,8
	.type	heap,@object            # @heap
	.comm	heap,8,8
	.type	heap_end,@object        # @heap_end
	.comm	heap_end,8,8
	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"GC took %g seconds, %ld compressed to %ld, %ld free\n"
	.size	.L.str.15, 53

	.type	old_heap_used,@object   # @old_heap_used
	.comm	old_heap_used,8,8
	.type	heap_org,@object        # @heap_org
	.comm	heap_org,8,8
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"> "
	.size	.L.str.16, 3

	.type	gc_time_taken,@object   # @gc_time_taken
	.comm	gc_time_taken,8,8
	.type	inside_err,@object      # @inside_err
	.local	inside_err
	.comm	inside_err,1,8
	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ERROR: %s\n"
	.size	.L.str.17, 11

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"ERROR: %s (errobj %s)\n"
	.size	.L.str.18, 23

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"ERROR: %s (see errobj)\n"
	.size	.L.str.19, 24

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"quit"
	.size	.L.str.20, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"BUG. Reached impossible case"
	.size	.L.str.22, 29

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"the currently assigned stack limit has been exceded"
	.size	.L.str.23, 52

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Stack_size = %ld bytes, [%p,%p]\n"
	.size	.L.str.24, 33

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"not a symbol or string"
	.size	.L.str.25, 23

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ran out of storage"
	.size	.L.str.26, 19

	.type	freelist,@object        # @freelist
	.comm	freelist,8,8
	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"wta to car"
	.size	.L.str.27, 11

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"wta to cdr"
	.size	.L.str.28, 11

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"wta to setcar"
	.size	.L.str.29, 14

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"wta to setcdr"
	.size	.L.str.30, 14

	.type	inums,@object           # @inums
	.comm	inums,8,8
	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"wta(1st) to plus"
	.size	.L.str.31, 17

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"wta(2nd) to plus"
	.size	.L.str.32, 17

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"wta(1st) to times"
	.size	.L.str.33, 18

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"wta(2nd) to times"
	.size	.L.str.34, 18

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"wta(1st) to difference"
	.size	.L.str.35, 23

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"wta(2nd) to difference"
	.size	.L.str.36, 23

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"wta(1st) to quotient"
	.size	.L.str.37, 21

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"wta(2nd) to quotient"
	.size	.L.str.38, 21

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"wta to abs"
	.size	.L.str.39, 11

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"wta to sqrt"
	.size	.L.str.40, 12

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"wta(1st) to greaterp"
	.size	.L.str.41, 21

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"wta(2nd) to greaterp"
	.size	.L.str.42, 21

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"wta(1st) to lessp"
	.size	.L.str.43, 18

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"wta(2nd) to lessp"
	.size	.L.str.44, 18

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"wta(1st) to max"
	.size	.L.str.45, 16

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"wta(2nd) to max"
	.size	.L.str.46, 16

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"wta(1st) to min"
	.size	.L.str.47, 16

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"wta(2nd) to min"
	.size	.L.str.48, 16

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"unbound variable"
	.size	.L.str.49, 17

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"not a symbol"
	.size	.L.str.50, 13

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"failed to allocate storage from system"
	.size	.L.str.51, 39

	.type	obarray,@object         # @obarray
	.comm	obarray,8,8
	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"invalid number of heaps"
	.size	.L.str.52, 24

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"**unbound-marker**"
	.size	.L.str.53, 19

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"eof"
	.size	.L.str.54, 4

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"t"
	.size	.L.str.55, 2

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"nil"
	.size	.L.str.56, 4

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"let"
	.size	.L.str.57, 4

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"let-internal-macro"
	.size	.L.str.58, 19

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"let*"
	.size	.L.str.59, 5

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"let*-macro"
	.size	.L.str.60, 11

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"letrec"
	.size	.L.str.61, 7

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"letrec-macro"
	.size	.L.str.62, 13

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"errobj"
	.size	.L.str.63, 7

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"all"
	.size	.L.str.64, 4

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"begin"
	.size	.L.str.65, 6

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"lambda"
	.size	.L.str.66, 7

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"quote"
	.size	.L.str.67, 6

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"."
	.size	.L.str.68, 2

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"*after-gc*"
	.size	.L.str.69, 11

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"*eval-history-ptr*"
	.size	.L.str.70, 19

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"improper list to assq"
	.size	.L.str.71, 22

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"type number out of range"
	.size	.L.str.72, 25

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"ran out of user type codes"
	.size	.L.str.73, 27

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"[allocating heap %ld]\n"
	.size	.L.str.74, 23

	.type	save_regs_gc_mark,@object # @save_regs_gc_mark
	.comm	save_regs_gc_mark,200,16
	.type	gc_rt,@object           # @gc_rt
	.comm	gc_rt,8,8
	.type	gc_cells_collected,@object # @gc_cells_collected
	.comm	gc_cells_collected,8,8
	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"[GC took %g cpu seconds, %ld cells collected]\n"
	.size	.L.str.76, 47

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"cannot perform operation with stop-and-copy GC mode. Use -g0\n"
	.size	.L.str.77, 62

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"garbage collection is on\n"
	.size	.L.str.78, 26

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"garbage collection is off\n"
	.size	.L.str.79, 27

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"%ld allocated %ld free\n"
	.size	.L.str.80, 24

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"garbage collection verbose\n"
	.size	.L.str.81, 28

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"garbage collection silent\n"
	.size	.L.str.82, 27

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"%ld/%ld heaps, %ld allocated %ld free\n"
	.size	.L.str.83, 39

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"bad syntax argument list"
	.size	.L.str.84, 25

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"damaged frame"
	.size	.L.str.85, 14

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"too few arguments"
	.size	.L.str.86, 18

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"damaged env"
	.size	.L.str.87, 12

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"closure code type not valid"
	.size	.L.str.88, 28

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"bad function"
	.size	.L.str.89, 13

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"cannot be applied"
	.size	.L.str.90, 18

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"have eval, dont know apply"
	.size	.L.str.91, 27

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"wta(non-symbol) to setvar"
	.size	.L.str.92, 26

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"wta(non-symbol) to define"
	.size	.L.str.93, 26

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"no *catch found with this tag"
	.size	.L.str.94, 30

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"set!"
	.size	.L.str.95, 5

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"let-internal"
	.size	.L.str.96, 13

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"wta(non-symbol) to symbolconc"
	.size	.L.str.97, 30

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"symbolconc buffer overflow"
	.size	.L.str.98, 27

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"subr_0"
	.size	.L.str.99, 7

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"subr_1"
	.size	.L.str.100, 7

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"subr_2"
	.size	.L.str.101, 7

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"subr_2n"
	.size	.L.str.102, 8

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"subr_3"
	.size	.L.str.103, 7

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"subr_4"
	.size	.L.str.104, 7

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"subr_5"
	.size	.L.str.105, 7

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"lsubr"
	.size	.L.str.106, 6

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"fsubr"
	.size	.L.str.107, 6

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"msubr"
	.size	.L.str.108, 6

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"???"
	.size	.L.str.109, 4

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"()"
	.size	.L.str.110, 3

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"("
	.size	.L.str.111, 2

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	" "
	.size	.L.str.112, 2

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	" . "
	.size	.L.str.113, 4

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	")"
	.size	.L.str.114, 2

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"%ld"
	.size	.L.str.115, 4

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"%g"
	.size	.L.str.116, 3

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"#<%s "
	.size	.L.str.117, 6

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	">"
	.size	.L.str.118, 2

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"#<CLOSURE "
	.size	.L.str.119, 11

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"#<UNKNOWN %d %p>"
	.size	.L.str.120, 17

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"end of file inside read"
	.size	.L.str.122, 24

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"unexpected close paren"
	.size	.L.str.123, 23

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"+internal-backquote"
	.size	.L.str.124, 20

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"+internal-comma-atsign"
	.size	.L.str.125, 23

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"+internal-comma-dot"
	.size	.L.str.126, 20

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"+internal-comma"
	.size	.L.str.127, 16

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"()'`,;\""
	.size	.L.str.128, 8

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"token larger than TKBUFFERN"
	.size	.L.str.129, 28

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"end of file inside list"
	.size	.L.str.130, 24

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"missing close paren"
	.size	.L.str.131, 20

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"could not open "
	.size	.L.str.132, 16

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"r"
	.size	.L.str.133, 2

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"not a file"
	.size	.L.str.134, 11

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"parser:"
	.size	.L.str.135, 8

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	".scm"
	.size	.L.str.136, 5

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"/"
	.size	.L.str.137, 2

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"loading "
	.size	.L.str.138, 9

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"rb"
	.size	.L.str.139, 3

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"done.\n"
	.size	.L.str.140, 7

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"*"
	.size	.L.str.141, 2

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"-loaded*"
	.size	.L.str.142, 9

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"w"
	.size	.L.str.143, 2

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"a"
	.size	.L.str.144, 2

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"bad argument to save-forms"
	.size	.L.str.145, 27

	.type	.L.str.146,@object      # @.str.146
.L.str.146:
	.asciz	"appending"
	.size	.L.str.146, 10

	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"saving"
	.size	.L.str.147, 7

	.type	.L.str.148,@object      # @.str.148
.L.str.148:
	.asciz	" forms to "
	.size	.L.str.148, 11

	.type	.L.str.149,@object      # @.str.149
.L.str.149:
	.asciz	"#<FILE "
	.size	.L.str.149, 8

	.type	.L.str.150,@object      # @.str.150
.L.str.150:
	.asciz	" %p"
	.size	.L.str.150, 4

	.type	.L.str.151,@object      # @.str.151
.L.str.151:
	.asciz	"file is closed"
	.size	.L.str.151, 15

	.type	last_c_errmsg.serrmsg,@object # @last_c_errmsg.serrmsg
	.local	last_c_errmsg.serrmsg
	.comm	last_c_errmsg.serrmsg,100,16
	.type	.L.str.152,@object      # @.str.152
.L.str.152:
	.asciz	"errno %d"
	.size	.L.str.152, 9

	.type	.L.str.153,@object      # @.str.153
.L.str.153:
	.asciz	"cons"
	.size	.L.str.153, 5

	.type	.L.str.154,@object      # @.str.154
.L.str.154:
	.asciz	"car"
	.size	.L.str.154, 4

	.type	.L.str.155,@object      # @.str.155
.L.str.155:
	.asciz	"cdr"
	.size	.L.str.155, 4

	.type	.L.str.156,@object      # @.str.156
.L.str.156:
	.asciz	"set-car!"
	.size	.L.str.156, 9

	.type	.L.str.157,@object      # @.str.157
.L.str.157:
	.asciz	"set-cdr!"
	.size	.L.str.157, 9

	.type	.L.str.158,@object      # @.str.158
.L.str.158:
	.asciz	"+"
	.size	.L.str.158, 2

	.type	.L.str.159,@object      # @.str.159
.L.str.159:
	.asciz	"-"
	.size	.L.str.159, 2

	.type	.L.str.160,@object      # @.str.160
.L.str.160:
	.asciz	"min"
	.size	.L.str.160, 4

	.type	.L.str.161,@object      # @.str.161
.L.str.161:
	.asciz	"max"
	.size	.L.str.161, 4

	.type	.L.str.162,@object      # @.str.162
.L.str.162:
	.asciz	"abs"
	.size	.L.str.162, 4

	.type	.L.str.163,@object      # @.str.163
.L.str.163:
	.asciz	"sqrt"
	.size	.L.str.163, 5

	.type	.L.str.164,@object      # @.str.164
.L.str.164:
	.asciz	"<"
	.size	.L.str.164, 2

	.type	.L.str.165,@object      # @.str.165
.L.str.165:
	.asciz	">="
	.size	.L.str.165, 3

	.type	.L.str.166,@object      # @.str.166
.L.str.166:
	.asciz	"<="
	.size	.L.str.166, 3

	.type	.L.str.167,@object      # @.str.167
.L.str.167:
	.asciz	"eq?"
	.size	.L.str.167, 4

	.type	.L.str.168,@object      # @.str.168
.L.str.168:
	.asciz	"eqv?"
	.size	.L.str.168, 5

	.type	.L.str.169,@object      # @.str.169
.L.str.169:
	.asciz	"="
	.size	.L.str.169, 2

	.type	.L.str.170,@object      # @.str.170
.L.str.170:
	.asciz	"assq"
	.size	.L.str.170, 5

	.type	.L.str.171,@object      # @.str.171
.L.str.171:
	.asciz	"delq"
	.size	.L.str.171, 5

	.type	.L.str.172,@object      # @.str.172
.L.str.172:
	.asciz	"read"
	.size	.L.str.172, 5

	.type	.L.str.173,@object      # @.str.173
.L.str.173:
	.asciz	"parser_read"
	.size	.L.str.173, 12

	.type	.L.str.174,@object      # @.str.174
.L.str.174:
	.asciz	"*parser_read.scm-loaded*"
	.size	.L.str.174, 25

	.type	.L.str.175,@object      # @.str.175
.L.str.175:
	.asciz	"eof-val"
	.size	.L.str.175, 8

	.type	.L.str.176,@object      # @.str.176
.L.str.176:
	.asciz	"print"
	.size	.L.str.176, 6

	.type	.L.str.177,@object      # @.str.177
.L.str.177:
	.asciz	"prin1"
	.size	.L.str.177, 6

	.type	.L.str.178,@object      # @.str.178
.L.str.178:
	.asciz	"eval"
	.size	.L.str.178, 5

	.type	.L.str.179,@object      # @.str.179
.L.str.179:
	.asciz	"apply"
	.size	.L.str.179, 6

	.type	.L.str.180,@object      # @.str.180
.L.str.180:
	.asciz	"define"
	.size	.L.str.180, 7

	.type	.L.str.181,@object      # @.str.181
.L.str.181:
	.asciz	"if"
	.size	.L.str.181, 3

	.type	.L.str.182,@object      # @.str.182
.L.str.182:
	.asciz	"while"
	.size	.L.str.182, 6

	.type	.L.str.183,@object      # @.str.183
.L.str.183:
	.asciz	"or"
	.size	.L.str.183, 3

	.type	.L.str.184,@object      # @.str.184
.L.str.184:
	.asciz	"and"
	.size	.L.str.184, 4

	.type	.L.str.185,@object      # @.str.185
.L.str.185:
	.asciz	"*catch"
	.size	.L.str.185, 7

	.type	.L.str.186,@object      # @.str.186
.L.str.186:
	.asciz	"*throw"
	.size	.L.str.186, 7

	.type	.L.str.187,@object      # @.str.187
.L.str.187:
	.asciz	"apropos"
	.size	.L.str.187, 8

	.type	.L.str.188,@object      # @.str.188
.L.str.188:
	.asciz	"verbose"
	.size	.L.str.188, 8

	.type	.L.str.189,@object      # @.str.189
.L.str.189:
	.asciz	"copy-list"
	.size	.L.str.189, 10

	.type	.L.str.190,@object      # @.str.190
.L.str.190:
	.asciz	"gc-status"
	.size	.L.str.190, 10

	.type	.L.str.191,@object      # @.str.191
.L.str.191:
	.asciz	"gc"
	.size	.L.str.191, 3

	.type	.L.str.192,@object      # @.str.192
.L.str.192:
	.asciz	"load"
	.size	.L.str.192, 5

	.type	.L.str.193,@object      # @.str.193
.L.str.193:
	.asciz	"require"
	.size	.L.str.193, 8

	.type	.L.str.194,@object      # @.str.194
.L.str.194:
	.asciz	"pair?"
	.size	.L.str.194, 6

	.type	.L.str.195,@object      # @.str.195
.L.str.195:
	.asciz	"symbol?"
	.size	.L.str.195, 8

	.type	.L.str.196,@object      # @.str.196
.L.str.196:
	.asciz	"number?"
	.size	.L.str.196, 8

	.type	.L.str.197,@object      # @.str.197
.L.str.197:
	.asciz	"symbol-bound?"
	.size	.L.str.197, 14

	.type	.L.str.198,@object      # @.str.198
.L.str.198:
	.asciz	"symbol-value"
	.size	.L.str.198, 13

	.type	.L.str.199,@object      # @.str.199
.L.str.199:
	.asciz	"set-symbol-value!"
	.size	.L.str.199, 18

	.type	.L.str.200,@object      # @.str.200
.L.str.200:
	.asciz	"the-environment"
	.size	.L.str.200, 16

	.type	.L.str.201,@object      # @.str.201
.L.str.201:
	.asciz	"error"
	.size	.L.str.201, 6

	.type	.L.str.202,@object      # @.str.202
.L.str.202:
	.asciz	"not"
	.size	.L.str.202, 4

	.type	.L.str.203,@object      # @.str.203
.L.str.203:
	.asciz	"null?"
	.size	.L.str.203, 6

	.type	.L.str.204,@object      # @.str.204
.L.str.204:
	.asciz	"env-lookup"
	.size	.L.str.204, 11

	.type	.L.str.205,@object      # @.str.205
.L.str.205:
	.asciz	"reverse"
	.size	.L.str.205, 8

	.type	.L.str.206,@object      # @.str.206
.L.str.206:
	.asciz	"symbolconc"
	.size	.L.str.206, 11

	.type	.L.str.207,@object      # @.str.207
.L.str.207:
	.asciz	"save-forms"
	.size	.L.str.207, 11

	.type	.L.str.208,@object      # @.str.208
.L.str.208:
	.asciz	"fopen"
	.size	.L.str.208, 6

	.type	.L.str.209,@object      # @.str.209
.L.str.209:
	.asciz	"fclose"
	.size	.L.str.209, 7

	.type	.L.str.210,@object      # @.str.210
.L.str.210:
	.asciz	"getc"
	.size	.L.str.210, 5

	.type	.L.str.211,@object      # @.str.211
.L.str.211:
	.asciz	"ungetc"
	.size	.L.str.211, 7

	.type	.L.str.212,@object      # @.str.212
.L.str.212:
	.asciz	"putc"
	.size	.L.str.212, 5

	.type	.L.str.213,@object      # @.str.213
.L.str.213:
	.asciz	"puts"
	.size	.L.str.213, 5

	.type	.L.str.214,@object      # @.str.214
.L.str.214:
	.asciz	"ftell"
	.size	.L.str.214, 6

	.type	.L.str.215,@object      # @.str.215
.L.str.215:
	.asciz	"fseek"
	.size	.L.str.215, 6

	.type	.L.str.216,@object      # @.str.216
.L.str.216:
	.asciz	"parse-number"
	.size	.L.str.216, 13

	.type	.L.str.217,@object      # @.str.217
.L.str.217:
	.asciz	"%%stack-limit"
	.size	.L.str.217, 14

	.type	.L.str.218,@object      # @.str.218
.L.str.218:
	.asciz	"intern"
	.size	.L.str.218, 7

	.type	.L.str.219,@object      # @.str.219
.L.str.219:
	.asciz	"%%closure"
	.size	.L.str.219, 10

	.type	.L.str.220,@object      # @.str.220
.L.str.220:
	.asciz	"%%closure-code"
	.size	.L.str.220, 15

	.type	.L.str.221,@object      # @.str.221
.L.str.221:
	.asciz	"%%closure-env"
	.size	.L.str.221, 14

	.type	.L.str.222,@object      # @.str.222
.L.str.222:
	.asciz	"nreverse"
	.size	.L.str.222, 9

	.type	.L.str.223,@object      # @.str.223
.L.str.223:
	.asciz	"allocate-heap"
	.size	.L.str.223, 14

	.type	.L.str.224,@object      # @.str.224
.L.str.224:
	.asciz	"gc-info"
	.size	.L.str.224, 8

	.type	.L.str.225,@object      # @.str.225
.L.str.225:
	.asciz	"runtime"
	.size	.L.str.225, 8

	.type	.L.str.226,@object      # @.str.226
.L.str.226:
	.asciz	"realtime"
	.size	.L.str.226, 9

	.type	.L.str.227,@object      # @.str.227
.L.str.227:
	.asciz	"caar"
	.size	.L.str.227, 5

	.type	.L.str.228,@object      # @.str.228
.L.str.228:
	.asciz	"cadr"
	.size	.L.str.228, 5

	.type	.L.str.229,@object      # @.str.229
.L.str.229:
	.asciz	"cdar"
	.size	.L.str.229, 5

	.type	.L.str.230,@object      # @.str.230
.L.str.230:
	.asciz	"cddr"
	.size	.L.str.230, 5

	.type	.L.str.231,@object      # @.str.231
.L.str.231:
	.asciz	"rand"
	.size	.L.str.231, 5

	.type	.L.str.232,@object      # @.str.232
.L.str.232:
	.asciz	"srand"
	.size	.L.str.232, 6

	.type	.L.str.233,@object      # @.str.233
.L.str.233:
	.asciz	"last-c-error"
	.size	.L.str.233, 13

	.type	.L.str.234,@object      # @.str.234
.L.str.234:
	.asciz	"os-classification"
	.size	.L.str.234, 18

	.type	.L.str.235,@object      # @.str.235
.L.str.235:
	.asciz	"0"
	.size	.L.str.235, 2

	.type	.L.str.236,@object      # @.str.236
.L.str.236:
	.asciz	"invalid\n"
	.size	.L.str.236, 9

	.type	.L.str.237,@object      # @.str.237
.L.str.237:
	.asciz	"repl_c_string_print overflow"
	.size	.L.str.237, 29

	.type	.L.str.238,@object      # @.str.238
.L.str.238:
	.asciz	"unix"
	.size	.L.str.238, 5

	.type	.L.str.239,@object      # @.str.239
.L.str.239:
	.asciz	"*slib-version*"
	.size	.L.str.239, 15

	.type	.L.str.240,@object      # @.str.240
.L.str.240:
	.asciz	"$Id: slib.c 9206 2003-10-17 18:48:45Z gaeke $"
	.size	.L.str.240, 46

	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"(C) Copyright 1988-1994 Paradigm Associates Inc."
	.size	.Lstr, 49

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"FATAL ERROR DURING STARTUP OR CRITICAL CODE SECTION"
	.size	.Lstr.1, 52

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.Lstr.2:
	.asciz	"[starting GC]"
	.size	.Lstr.2, 14

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	.L.str.99
	.quad	.L.str.100
	.quad	.L.str.101
	.quad	.L.str.103
	.quad	.L.str.106
	.quad	.L.str.107
	.quad	.L.str.108
	.quad	.L.str.109
	.quad	.L.str.109
	.quad	.L.str.109
	.quad	.L.str.109
	.quad	.L.str.109
	.quad	.L.str.109
	.quad	.L.str.109
	.quad	.L.str.109
	.quad	.L.str.104
	.quad	.L.str.105
	.quad	.L.str.102
	.size	.Lswitch.table, 144


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
