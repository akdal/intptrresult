	.text
	.file	"jcprepct.bc"
	.globl	jinit_c_prep_controller
	.p2align	4, 0x90
	.type	jinit_c_prep_controller,@function
jinit_c_prep_controller:                # @jinit_c_prep_controller
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 240
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	testl	%esi, %esi
	je	.LBB0_2
# BB#1:
	movq	(%r13), %rax
	movl	$4, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_2:
	movq	8(%r13), %rax
	movl	$1, %esi
	movl	$112, %edx
	movq	%r13, %rdi
	callq	*(%rax)
	movq	%rax, 440(%r13)
	movq	$start_pass_prep, (%rax)
	movq	472(%r13), %rcx
	cmpl	$0, 16(%rcx)
	je	.LBB0_6
# BB#3:
	movq	$pre_process_context, 8(%rax)
	movslq	308(%r13), %rcx
	movq	8(%r13), %r8
	movq	%rax, %rbp
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	leal	(%rcx,%rcx,4), %ebx
	movl	68(%r13), %ecx
	imull	%ebx, %ecx
	movslq	%ecx, %rdx
	shlq	$3, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	callq	*(%r8)
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpl	$0, 68(%r13)
	jle	.LBB0_31
# BB#4:                                 # %.lr.ph60.i
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	movq	80(%r13), %r15
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdx,2), %r9
	movq	%r9, %rax
	shlq	$3, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	testl	%edx, %edx
	movslq	%ebx, %r8
	movq	%r9, 8(%rsp)            # 8-byte Spill
	jle	.LBB0_29
# BB#5:                                 # %.lr.ph60.split.us.preheader.i
	movl	%edx, %esi
	leal	(,%rsi,4), %eax
	leal	(%rsi,%rsi), %ecx
	movslq	%ecx, %rcx
	movslq	%eax, %rdi
	shlq	$3, %r8
	leaq	(,%rsi,8), %rax
	leaq	(,%rdi,8), %rbp
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	movq	%rax, 144(%rsp)         # 8-byte Spill
	leaq	(%rax,%rdi,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rsi), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	leaq	-1(%rsi), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$3, %edx
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	subq	%rdx, %rsi
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	leaq	2(%rcx), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	16(,%rdi,8), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	%rcx, %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	1(%rcx), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	%rdi, 160(%rsp)         # 8-byte Spill
	leaq	8(%r14,%rdi,8), %r10
	xorl	%r12d, %r12d
	movq	48(%rsp), %rbp          # 8-byte Reload
	movq	%r13, 176(%rsp)         # 8-byte Spill
	movq	%r8, 16(%rsp)           # 8-byte Spill
	jmp	.LBB0_12
.LBB0_10:                               #   in Loop: Header=BB0_12 Depth=1
	xorl	%eax, %eax
	jmp	.LBB0_23
.LBB0_11:                               #   in Loop: Header=BB0_12 Depth=1
	xorl	%eax, %eax
	movq	8(%rsp), %r9            # 8-byte Reload
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph60.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_20 Depth 2
                                        #     Child Loop BB0_27 Depth 2
	movq	%r10, 32(%rsp)          # 8-byte Spill
	movq	8(%r13), %rbx
	movl	28(%r15), %ecx
	movslq	304(%r13), %rax
	imulq	%rcx, %rax
	shlq	$3, %rax
	movslq	8(%r15), %rcx
	cqto
	idivq	%rcx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	movl	%r9d, %ecx
	callq	*16(%rbx)
	movq	%rax, %r13
	movq	80(%rsp), %rbx          # 8-byte Reload
	leaq	(%r14,%rbx,8), %rdi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%r13, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	cmpl	$4, %ebx
	jb	.LBB0_22
# BB#13:                                # %min.iters.checked
                                        #   in Loop: Header=BB0_12 Depth=1
	cmpq	$0, 64(%rsp)            # 8-byte Folded Reload
	je	.LBB0_22
# BB#14:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	imulq	%r12, %rax
	movq	40(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rax), %rcx
	movq	144(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rax), %rdx
	addq	%rdi, %rdx
	movq	136(%rsp), %rsi         # 8-byte Reload
	leaq	(%rsi,%rax), %rsi
	addq	%rdi, %rsi
	addq	128(%rsp), %rax         # 8-byte Folded Reload
	addq	%rdi, %rax
	movq	72(%rsp), %rdi          # 8-byte Reload
	leaq	(%r13,%rdi,8), %r10
	movq	120(%rsp), %rdi         # 8-byte Reload
	leaq	(%r13,%rdi,8), %rdi
	movq	56(%rsp), %rbp          # 8-byte Reload
	leaq	(%r13,%rbp,8), %r11
	cmpq	%rax, %rcx
	sbbb	%bl, %bl
	cmpq	%rdx, %rsi
	sbbb	%r8b, %r8b
	andb	%bl, %r8b
	cmpq	%rdi, %rcx
	sbbb	%bl, %bl
	cmpq	%rdx, %r10
	sbbb	%r9b, %r9b
	cmpq	%r11, %rcx
	sbbb	%cl, %cl
	cmpq	%rdx, %r13
	sbbb	%bpl, %bpl
	cmpq	%rdi, %rsi
	sbbb	%dl, %dl
	cmpq	%rax, %r10
	sbbb	%dil, %dil
	cmpq	%r11, %rsi
	sbbb	%sil, %sil
	cmpq	%rax, %r13
	sbbb	%al, %al
	testb	$1, %r8b
	jne	.LBB0_22
# BB#15:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_12 Depth=1
	andb	%r9b, %bl
	andb	$1, %bl
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
	jne	.LBB0_11
# BB#16:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_12 Depth=1
	andb	%bpl, %cl
	andb	$1, %cl
	movq	8(%rsp), %r9            # 8-byte Reload
	jne	.LBB0_10
# BB#17:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_12 Depth=1
	andb	%dil, %dl
	andb	$1, %dl
	jne	.LBB0_10
# BB#18:                                # %vector.memcheck
                                        #   in Loop: Header=BB0_12 Depth=1
	andb	%al, %sil
	andb	$1, %sil
	movl	$0, %eax
	jne	.LBB0_23
# BB#19:                                # %vector.body.preheader
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	(%r13,%rax,8), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%r14,%rcx), %rcx
	xorl	%edx, %edx
	movq	64(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_20:                               # %vector.body
                                        #   Parent Loop BB0_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-16(%rax,%rdx,8), %xmm0
	movups	(%rax,%rdx,8), %xmm1
	movups	%xmm0, (%r14,%rdx,8)
	movups	%xmm1, 16(%r14,%rdx,8)
	movups	(%r13,%rdx,8), %xmm0
	movups	16(%r13,%rdx,8), %xmm1
	movups	%xmm0, -16(%rcx,%rdx,8)
	movups	%xmm1, (%rcx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rsi
	jne	.LBB0_20
# BB#21:                                # %middle.block
                                        #   in Loop: Header=BB0_12 Depth=1
	cmpl	$0, 112(%rsp)           # 4-byte Folded Reload
	movq	%rsi, %rax
	jne	.LBB0_23
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_22:                               #   in Loop: Header=BB0_12 Depth=1
	xorl	%eax, %eax
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	movq	32(%rsp), %r10          # 8-byte Reload
	movq	24(%rsp), %r11          # 8-byte Reload
.LBB0_23:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	subl	%eax, %ecx
	testb	$1, %cl
	movq	%rax, %rdi
	je	.LBB0_25
# BB#24:                                # %scalar.ph.prol
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %rcx
	movq	(%r13,%rcx,8), %rcx
	movq	%rcx, (%r14,%rax,8)
	movq	(%r13,%rax,8), %rcx
	movq	160(%rsp), %rdx         # 8-byte Reload
	leaq	(%rax,%rdx), %rdx
	movq	%rcx, (%r14,%rdx,8)
	leaq	1(%rax), %rdi
.LBB0_25:                               # %scalar.ph.prol.loopexit
                                        #   in Loop: Header=BB0_12 Depth=1
	cmpq	%rax, 168(%rsp)         # 8-byte Folded Reload
	je	.LBB0_28
# BB#26:                                # %scalar.ph.preheader.new
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	subq	%rdi, %rax
	leaq	(%r14,%rdi,8), %rcx
	movq	152(%rsp), %rdx         # 8-byte Reload
	leaq	(%r13,%rdx,8), %rdx
	leaq	(%rdx,%rdi,8), %rdx
	leaq	(%r10,%rdi,8), %rsi
	leaq	8(%r13,%rdi,8), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_27:                               # %scalar.ph
                                        #   Parent Loop BB0_12 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rdx,%rbx,8), %rbp
	movq	%rbp, (%rcx,%rbx,8)
	movq	-8(%rdi,%rbx,8), %rbp
	movq	%rbp, -8(%rsi,%rbx,8)
	movq	(%rdx,%rbx,8), %rbp
	movq	%rbp, 8(%rcx,%rbx,8)
	movq	(%rdi,%rbx,8), %rbp
	movq	%rbp, (%rsi,%rbx,8)
	addq	$2, %rbx
	cmpq	%rbx, %rax
	jne	.LBB0_27
.LBB0_28:                               # %._crit_edge.us.i
                                        #   in Loop: Header=BB0_12 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%r11, 16(%rax,%r12,8)
	addq	%r8, %r14
	incq	%r12
	addq	$96, %r15
	movq	176(%rsp), %r13         # 8-byte Reload
	movslq	68(%r13), %rax
	addq	%r8, %r10
	cmpq	%rax, %r12
	movq	48(%rsp), %rbp          # 8-byte Reload
	jl	.LBB0_12
	jmp	.LBB0_31
.LBB0_6:
	movq	$pre_process_data, 8(%rax)
	cmpl	$0, 68(%r13)
	movq	%rax, %r14
	jle	.LBB0_31
# BB#7:                                 # %.lr.ph
	movq	80(%r13), %rbx
	addq	$28, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	movq	8(%r13), %r8
	movl	(%rbx), %ecx
	movslq	304(%r13), %rax
	imulq	%rcx, %rax
	shlq	$3, %rax
	movslq	-20(%rbx), %rcx
	cqto
	idivq	%rcx
	movl	308(%r13), %ecx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	callq	*16(%r8)
	movq	%rax, 16(%r14,%rbp,8)
	incq	%rbp
	movslq	68(%r13), %rax
	addq	$96, %rbx
	cmpq	%rax, %rbp
	jl	.LBB0_8
	jmp	.LBB0_31
.LBB0_29:                               # %.lr.ph60.split.i.preheader
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rdx,8), %rbp
	shlq	$3, %r8
	addq	$28, %r15
	xorl	%ebx, %ebx
	movq	48(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_30:                               # %.lr.ph60.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%r8, %r14
	movq	8(%r13), %r8
	movl	(%r15), %ecx
	movslq	304(%r13), %rax
	imulq	%rcx, %rax
	shlq	$3, %rax
	movslq	-20(%r15), %rcx
	cqto
	idivq	%rcx
	movl	$1, %esi
	movq	%r13, %rdi
	movl	%eax, %edx
	movl	%r9d, %ecx
	callq	*16(%r8)
	movq	%rbp, %rdi
	movq	%rax, %rsi
	movq	%r12, %rdx
	callq	memcpy
	movq	%r14, %r8
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%rbp, 16(%rax,%rbx,8)
	incq	%rbx
	movslq	68(%r13), %rax
	addq	%r8, %rbp
	addq	$96, %r15
	cmpq	%rax, %rbx
	jl	.LBB0_30
.LBB0_31:                               # %create_context_buffer.exit
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_c_prep_controller, .Lfunc_end0-jinit_c_prep_controller
	.cfi_endproc

	.p2align	4, 0x90
	.type	start_pass_prep,@function
start_pass_prep:                        # @start_pass_prep
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	440(%rbx), %r14
	testl	%esi, %esi
	je	.LBB1_2
# BB#1:
	movq	(%rbx), %rax
	movl	$4, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB1_2:
	movl	44(%rbx), %eax
	movl	%eax, 96(%r14)
	movl	$0, 100(%r14)
	movl	$0, 104(%r14)
	movl	308(%rbx), %eax
	addl	%eax, %eax
	movl	%eax, 108(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	start_pass_prep, .Lfunc_end1-start_pass_prep
	.cfi_endproc

	.p2align	4, 0x90
	.type	pre_process_context,@function
pre_process_context:                    # @pre_process_context
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 128
.Lcfi25:
	.cfi_offset %rbx, -56
.Lcfi26:
	.cfi_offset %r12, -48
.Lcfi27:
	.cfi_offset %r13, -40
.Lcfi28:
	.cfi_offset %r14, -32
.Lcfi29:
	.cfi_offset %r15, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movl	%ecx, %ebp
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %r13
	movl	128(%rsp), %eax
	movq	%r9, 40(%rsp)           # 8-byte Spill
	cmpl	%eax, (%r9)
	jae	.LBB2_30
# BB#1:                                 # %.lr.ph92
	movq	440(%r13), %r12
	imull	$3, 308(%r13), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	leaq	16(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB2_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_20 Depth 2
                                        #     Child Loop BB2_22 Depth 2
                                        #     Child Loop BB2_6 Depth 2
                                        #       Child Loop BB2_8 Depth 3
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	%ebp, %eax
	jae	.LBB2_13
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	subl	%eax, %ebp
	movl	100(%r12), %ecx
	movl	108(%r12), %r14d
	subl	%ecx, %r14d
	cmpl	%ebp, %r14d
	cmovael	%ebp, %r14d
	movq	464(%r13), %rbp
	movq	48(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rsi
	movq	%r13, %rdi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%r14d, %r8d
	callq	*8(%rbp)
	movl	96(%r12), %eax
	cmpl	44(%r13), %eax
	jne	.LBB2_11
# BB#4:                                 # %.preheader84
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	68(%r13), %ecx
	testl	%ecx, %ecx
	jle	.LBB2_11
# BB#5:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	308(%r13), %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB2_6:                                # %.preheader
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_8 Depth 3
	testl	%eax, %eax
	jle	.LBB2_10
# BB#7:                                 # %.lr.ph88
                                        #   in Loop: Header=BB2_6 Depth=2
	xorl	%ebp, %ebp
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB2_8:                                #   Parent Loop BB2_2 Depth=1
                                        #     Parent Loop BB2_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r12,%r15,8), %rdi
	movl	40(%r13), %r9d
	xorl	%esi, %esi
	movl	$1, %r8d
	movq	%rdi, %rdx
	movl	%ebx, %ecx
	callq	jcopy_sample_rows
	movl	308(%r13), %eax
	incl	%ebp
	decl	%ebx
	cmpl	%eax, %ebp
	jl	.LBB2_8
# BB#9:                                 # %._crit_edge89.loopexit
                                        #   in Loop: Header=BB2_6 Depth=2
	movl	68(%r13), %ecx
.LBB2_10:                               # %._crit_edge89
                                        #   in Loop: Header=BB2_6 Depth=2
	incq	%r15
	movslq	%ecx, %rdx
	cmpq	%rdx, %r15
	jl	.LBB2_6
.LBB2_11:                               # %.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	addl	%r14d, (%rax)
	movq	%r12, %rax
	movl	100(%rax), %r12d
	addl	%r14d, %r12d
	movl	%r12d, 100(%rax)
	subl	%r14d, 96(%rax)
	movl	108(%rax), %r15d
	jmp	.LBB2_12
	.p2align	4, 0x90
.LBB2_13:                               #   in Loop: Header=BB2_2 Depth=1
	cmpl	$0, 96(%r12)
	jne	.LBB2_30
# BB#14:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rax
	movl	100(%rax), %r12d
	movl	108(%rax), %r15d
	cmpl	%r15d, %r12d
	jge	.LBB2_12
# BB#15:                                # %.preheader85
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	68(%r13), %eax
	testl	%eax, %eax
	jle	.LBB2_16
# BB#17:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB2_2 Depth=1
	xorl	%edx, %edx
	cmpl	%r15d, %r12d
	jge	.LBB2_22
	jmp	.LBB2_19
	.p2align	4, 0x90
.LBB2_23:                               # %expand_bottom_edge.exit..lr.ph_crit_edge
                                        #   in Loop: Header=BB2_22 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	100(%rcx), %r12d
	movl	108(%rcx), %r15d
	cmpl	%r15d, %r12d
	jge	.LBB2_22
.LBB2_19:                               # %.lr.ph.i
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	16(%rax,%rdx,8), %rbx
	movl	40(%r13), %r14d
	leal	-1(%r12), %ebp
	.p2align	4, 0x90
.LBB2_20:                               #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %r8d
	movq	%rbx, %rdi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movl	%r12d, %ecx
	movl	%r14d, %r9d
	callq	jcopy_sample_rows
	incl	%r12d
	cmpl	%r12d, %r15d
	jne	.LBB2_20
# BB#21:                                # %expand_bottom_edge.exit.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	68(%r13), %eax
	movq	64(%rsp), %rdx          # 8-byte Reload
.LBB2_22:                               # %expand_bottom_edge.exit
                                        #   Parent Loop BB2_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rdx
	movslq	%eax, %rcx
	cmpq	%rcx, %rdx
	jl	.LBB2_23
# BB#24:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
	movl	108(%r12), %r15d
	jmp	.LBB2_25
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_2 Depth=1
	cmpl	%r15d, %r12d
	movl	16(%rsp), %ebp          # 4-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	je	.LBB2_26
	jmp	.LBB2_29
.LBB2_16:                               #   in Loop: Header=BB2_2 Depth=1
	movq	8(%rsp), %r12           # 8-byte Reload
.LBB2_25:                               # %.thread
                                        #   in Loop: Header=BB2_2 Depth=1
	movl	%r15d, 100(%r12)
	movl	16(%rsp), %ebp          # 4-byte Reload
.LBB2_26:                               #   in Loop: Header=BB2_2 Depth=1
	movq	472(%r13), %rax
	movl	104(%r12), %edx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %r8d
	movq	%r13, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	callq	*8(%rax)
	incl	(%rbx)
	movl	308(%r13), %eax
	movl	104(%r12), %ecx
	addl	%eax, %ecx
	movl	20(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %ecx
	movl	$0, %esi
	cmovgel	%esi, %ecx
	movl	%ecx, 104(%r12)
	movl	100(%r12), %ecx
	cmpl	%edx, %ecx
	jl	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_2 Depth=1
	movl	$0, 100(%r12)
	xorl	%ecx, %ecx
.LBB2_28:                               #   in Loop: Header=BB2_2 Depth=1
	addl	%ecx, %eax
	movl	%eax, 108(%r12)
.LBB2_29:                               # %.backedge
                                        #   in Loop: Header=BB2_2 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	128(%rsp), %ecx
	cmpl	%ecx, (%rax)
	jb	.LBB2_2
.LBB2_30:                               # %._crit_edge93
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	pre_process_context, .Lfunc_end2-pre_process_context
	.cfi_endproc

	.p2align	4, 0x90
	.type	pre_process_data,@function
pre_process_data:                       # @pre_process_data
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi34:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi35:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi37:
	.cfi_def_cfa_offset 128
.Lcfi38:
	.cfi_offset %rbx, -56
.Lcfi39:
	.cfi_offset %r12, -48
.Lcfi40:
	.cfi_offset %r13, -40
.Lcfi41:
	.cfi_offset %r14, -32
.Lcfi42:
	.cfi_offset %r15, -24
.Lcfi43:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movl	%ecx, %ebp
	movq	%rdx, %r13
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	(%r13), %eax
	cmpl	%ebp, %eax
	jae	.LBB3_29
# BB#1:                                 # %.lr.ph84
	movq	440(%r15), %rbx
	leaq	16(%rbx), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%r12, 16(%rsp)          # 8-byte Spill
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movl	%ebp, 44(%rsp)          # 4-byte Spill
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_9 Depth 2
                                        #     Child Loop BB3_11 Depth 2
	movl	128(%rsp), %ecx
	cmpl	%ecx, (%r12)
	jae	.LBB3_29
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	subl	%eax, %ebp
	movl	308(%r15), %ebx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	100(%rcx), %ecx
	subl	%ecx, %ebx
	cmpl	%ebp, %ebx
	cmovael	%ebp, %ebx
	movq	464(%r15), %rbp
	movl	%eax, %eax
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rax,8), %rsi
	movq	%r15, %rdi
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	*8(%rbp)
	addl	%ebx, (%r13)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	100(%rax), %r13d
	addl	%ebx, %r13d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r13d, 100(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	96(%rax), %eax
	subl	%ebx, %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	%eax, 96(%rbx)
	movl	308(%r15), %r14d
	jne	.LBB3_15
# BB#4:                                 #   in Loop: Header=BB3_2 Depth=1
	cmpl	%r14d, %r13d
	jge	.LBB3_15
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	68(%r15), %eax
	testl	%eax, %eax
	jle	.LBB3_14
# BB#6:                                 # %.lr.ph81.preheader
                                        #   in Loop: Header=BB3_2 Depth=1
	xorl	%ebp, %ebp
	cmpl	%r14d, %r13d
	jge	.LBB3_11
	jmp	.LBB3_8
	.p2align	4, 0x90
.LBB3_12:                               # %expand_bottom_edge.exit..lr.ph81_crit_edge
                                        #   in Loop: Header=BB3_11 Depth=2
	movl	100(%rbx), %r13d
	movl	308(%r15), %r14d
	cmpl	%r14d, %r13d
	jge	.LBB3_11
.LBB3_8:                                # %.lr.ph.i
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rbx,%rbp,8), %rbx
	movl	40(%r15), %r15d
	leal	-1(%r13), %r12d
	.p2align	4, 0x90
.LBB3_9:                                #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %r8d
	movq	%rbx, %rdi
	movl	%r12d, %esi
	movq	%rbx, %rdx
	movl	%r13d, %ecx
	movl	%r15d, %r9d
	callq	jcopy_sample_rows
	incl	%r13d
	cmpl	%r13d, %r14d
	jne	.LBB3_9
# BB#10:                                # %expand_bottom_edge.exit.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
	movl	68(%r15), %eax
	movq	8(%rsp), %rbx           # 8-byte Reload
.LBB3_11:                               # %expand_bottom_edge.exit
                                        #   Parent Loop BB3_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB3_12
# BB#13:                                # %._crit_edge82.loopexit
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	308(%r15), %r14d
.LBB3_14:                               # %.thread
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	%r14d, 100(%rbx)
	jmp	.LBB3_16
	.p2align	4, 0x90
.LBB3_15:                               # %._crit_edge91
                                        #   in Loop: Header=BB3_2 Depth=1
	cmpl	%r14d, %r13d
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	jne	.LBB3_17
.LBB3_16:                               #   in Loop: Header=BB3_2 Depth=1
	movq	472(%r15), %rax
	movl	(%r12), %r8d
	xorl	%edx, %edx
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	*8(%rax)
	movl	$0, 100(%rbx)
	incl	(%r12)
.LBB3_17:                               #   in Loop: Header=BB3_2 Depth=1
	cmpl	$0, 96(%rbx)
	jne	.LBB3_19
# BB#18:                                #   in Loop: Header=BB3_2 Depth=1
	movl	(%r12), %eax
	cmpl	128(%rsp), %eax
	jb	.LBB3_20
.LBB3_19:                               # %.backedge
                                        #   in Loop: Header=BB3_2 Depth=1
	movl	(%r13), %eax
	movl	44(%rsp), %ebp          # 4-byte Reload
	cmpl	%ebp, %eax
	jb	.LBB3_2
	jmp	.LBB3_29
.LBB3_20:
	movl	68(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB3_28
# BB#21:                                # %.lr.ph.preheader
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	80(%rdx), %rsi
	xorl	%r15d, %r15d
	jmp	.LBB3_22
	.p2align	4, 0x90
.LBB3_27:                               # %expand_bottom_edge.exit77..lr.ph_crit_edge
                                        #   in Loop: Header=BB3_22 Depth=1
	addq	$96, %rsi
	movl	(%r12), %eax
.LBB3_22:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_24 Depth 2
	movl	12(%rsi), %ebx
	movl	%ebx, %ebp
	imull	%eax, %ebp
	movl	%ebx, %edx
	imull	128(%rsp), %edx
	cmpl	%edx, %ebp
	jge	.LBB3_26
# BB#23:                                # %.lr.ph.i74
                                        #   in Loop: Header=BB3_22 Depth=1
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx,%r15,8), %r14
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	28(%rsi), %r12d
	shll	$3, %r12d
	leal	-1(%rbp), %r13d
	movl	128(%rsp), %ecx
	subl	%eax, %ecx
	imull	%ecx, %ebx
	.p2align	4, 0x90
.LBB3_24:                               #   Parent Loop BB3_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$1, %r8d
	movq	%r14, %rdi
	movl	%r13d, %esi
	movq	%r14, %rdx
	movl	%ebp, %ecx
	movl	%r12d, %r9d
	callq	jcopy_sample_rows
	incl	%ebp
	decl	%ebx
	jne	.LBB3_24
# BB#25:                                # %expand_bottom_edge.exit77.loopexit
                                        #   in Loop: Header=BB3_22 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	68(%rax), %ecx
	movq	16(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB3_26:                               # %expand_bottom_edge.exit77
                                        #   in Loop: Header=BB3_22 Depth=1
	incq	%r15
	movslq	%ecx, %rax
	cmpq	%rax, %r15
	jl	.LBB3_27
.LBB3_28:                               # %._crit_edge
	movl	128(%rsp), %eax
	movl	%eax, (%r12)
.LBB3_29:                               # %.critedge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	pre_process_data, .Lfunc_end3-pre_process_data
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
