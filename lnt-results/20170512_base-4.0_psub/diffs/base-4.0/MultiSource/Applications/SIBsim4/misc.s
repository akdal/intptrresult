	.text
	.file	"misc.bc"
	.globl	fatal
	.p2align	4, 0x90
	.type	fatal,@function
fatal:                                  # @fatal
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$216, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 240
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	testb	%al, %al
	je	.LBB0_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB0_2:
	movq	%r9, 72(%rsp)
	movq	%r8, 64(%rsp)
	movq	%rcx, 56(%rsp)
	movq	%rdx, 48(%rsp)
	movq	%rsi, 40(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	240(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$8, (%rsp)
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	argv0(%rip), %rbx
	testq	%rbx, %rbx
	je	.LBB0_4
# BB#3:
	movl	$47, %esi
	movq	%rbx, %rdi
	callq	strrchr
	movq	stderr(%rip), %rdi
	testq	%rax, %rax
	leaq	1(%rax), %rdx
	cmoveq	%rbx, %rdx
	movl	$.L.str, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB0_4:
	movq	stderr(%rip), %rdi
	movq	%rsp, %rdx
	movq	%r14, %rsi
	callq	vfprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$dna_seq_head, %edx
	movl	$rna_seq_head, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	fatal, .Lfunc_end0-fatal
	.cfi_endproc

	.globl	xmalloc
	.p2align	4, 0x90
	.type	xmalloc,@function
xmalloc:                                # @xmalloc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	callq	malloc
	testq	%rax, %rax
	je	.LBB1_2
# BB#1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
	callq	__errno_location
	movq	%rax, %r14
	movl	(%r14), %edi
	callq	strerror
	movq	%rax, %rdx
	movl	(%r14), %ecx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	fatal
.Lfunc_end1:
	.size	xmalloc, .Lfunc_end1-xmalloc
	.cfi_endproc

	.globl	xcalloc
	.p2align	4, 0x90
	.type	xcalloc,@function
xcalloc:                                # @xcalloc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	calloc
	testq	%rax, %rax
	je	.LBB2_2
# BB#1:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB2_2:
	callq	__errno_location
	movq	%rax, %r15
	movl	(%r15), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	(%r15), %r8d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	fatal
.Lfunc_end2:
	.size	xcalloc, .Lfunc_end2-xcalloc
	.cfi_endproc

	.globl	xrealloc
	.p2align	4, 0x90
	.type	xrealloc,@function
xrealloc:                               # @xrealloc
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	realloc
	testq	%rax, %rax
	je	.LBB3_2
# BB#1:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB3_2:
	callq	__errno_location
	movq	%rax, %r15
	movl	(%r15), %edi
	callq	strerror
	movq	%rax, %rcx
	movl	(%r15), %r8d
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	fatal
.Lfunc_end3:
	.size	xrealloc, .Lfunc_end3-xrealloc
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%s: "
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n while processing:\n%.256s\n%.256s\n"
	.size	.L.str.1, 35

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"malloc of %zd failed: %s (%d)\n"
	.size	.L.str.2, 31

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"calloc of %zd, %zd failed: %s (%d)\n"
	.size	.L.str.3, 36

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"realloc of %p to %zd failed: %s (%d)\n"
	.size	.L.str.4, 38


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
