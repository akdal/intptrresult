	.text
	.file	"rules-red.bc"
	.globl	red_Init
	.p2align	4, 0x90
	.type	red_Init,@function
red_Init:                               # @red_Init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	callq	term_GetStampID
	movl	%eax, red_STAMPID(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	red_Init, .Lfunc_end0-red_Init
	.cfi_endproc

	.globl	red_ReductionOnDerivedClause
	.p2align	4, 0x90
	.type	red_ReductionOnDerivedClause,@function
red_ReductionOnDerivedClause:           # @red_ReductionOnDerivedClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi4:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi5:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 128
.Lcfi8:
	.cfi_offset %rbx, -56
.Lcfi9:
	.cfi_offset %r12, -48
.Lcfi10:
	.cfi_offset %r13, -40
.Lcfi11:
	.cfi_offset %r14, -32
.Lcfi12:
	.cfi_offset %r15, -24
.Lcfi13:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	%rsi, 24(%rsp)
	movq	$0, 40(%rsp)
	movq	104(%r14), %rbp
	movq	112(%r14), %r15
	leaq	48(%r14), %rax
	leaq	32(%r14), %rcx
	movl	%edx, 60(%rsp)          # 4-byte Spill
	cmpl	$2, %edx
	setne	%bl
	cmoveq	%rcx, %rax
	movq	(%rax), %r13
	movq	$0, 32(%rsp)
	leaq	24(%rsp), %rdi
	leaq	40(%rsp), %rcx
	movq	%r15, %rsi
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	%rbp, %rdx
	callq	red_SimpleStaticReductions
	movl	%eax, %r12d
	testl	%r12d, %r12d
	je	.LBB1_1
.LBB1_61:                               # %red_StaticReductions.exit
	xorl	%r13d, %r13d
.LBB1_64:                               # %red_StaticReductions.exit
	movq	40(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB1_74
# BB#65:                                # %.lr.ph.i10
	movq	24(%rsp), %r15
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB1_66:                               # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movl	12(%r15), %edx
	cmpl	12(%rsi), %edx
	ja	.LBB1_68
# BB#67:                                #   in Loop: Header=BB1_66 Depth=1
	cmpl	132(%r14), %edx
	ja	.LBB1_68
# BB#69:                                #   in Loop: Header=BB1_66 Depth=1
	movq	112(%r14), %rax
	cmpl	$0, 36(%rax)
	je	.LBB1_71
# BB#70:                                #   in Loop: Header=BB1_66 Depth=1
	movq	%r14, %rdi
	callq	prfs_InsertDocProofClause
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_68:                               #   in Loop: Header=BB1_66 Depth=1
	movq	%r14, %rdi
	callq	split_KeepClauseAtLevel
	jmp	.LBB1_72
.LBB1_71:                               #   in Loop: Header=BB1_66 Depth=1
	movq	%rsi, %rdi
	callq	clause_Delete
	.p2align	4, 0x90
.LBB1_72:                               #   in Loop: Header=BB1_66 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB1_66
	.p2align	4, 0x90
.LBB1_73:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB1_73
.LBB1_74:                               # %list_Delete.exit
	testl	%r12d, %r12d
	je	.LBB1_75
# BB#76:
	testq	%r13, %r13
	je	.LBB1_77
# BB#80:
	movl	12(%r13), %edx
	movq	24(%rsp), %rsi
	cmpl	12(%rsi), %edx
	ja	.LBB1_82
# BB#81:
	cmpl	132(%r14), %edx
	jbe	.LBB1_78
.LBB1_82:
	movq	%r14, %rdi
	callq	split_KeepClauseAtLevel
	jmp	.LBB1_84
.LBB1_1:
	movl	372(%r15), %eax
	testl	%eax, %eax
	movb	%bl, 23(%rsp)           # 1-byte Spill
	je	.LBB1_11
# BB#2:
	movq	24(%rsp), %rdi
	movl	156(%r14), %r8d
	xorl	%r9d, %r9d
	cmpl	$2, %eax
	sete	%r9b
	leaq	32(%rsp), %rcx
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	red_AssignmentEquationDeletion
	testl	%eax, %eax
	je	.LBB1_11
# BB#3:
	cmpq	$0, 32(%rsp)
	je	.LBB1_4
# BB#5:
	movq	%r13, %rbp
	movq	24(%rsp), %r13
	movq	40(%rsp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbp, %r13
	movq	%rbx, (%rax)
	movq	%rax, 40(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	$0, 32(%rsp)
	testq	%rax, %rax
	jne	.LBB1_7
	jmp	.LBB1_11
.LBB1_75:                               # %list_Delete.exit._crit_edge
	movq	24(%rsp), %rax
	jmp	.LBB1_85
.LBB1_77:                               # %._crit_edge
	movq	24(%rsp), %rsi
.LBB1_78:
	movq	112(%r14), %rax
	cmpl	$0, 36(%rax)
	je	.LBB1_83
# BB#79:
	movq	%r14, %rdi
	callq	prfs_InsertDocProofClause
	jmp	.LBB1_84
.LBB1_83:
	movq	%rsi, %rdi
	callq	clause_Delete
.LBB1_84:
	movq	$0, 24(%rsp)
	xorl	%eax, %eax
.LBB1_85:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_4:                                # %.red_ExchangeClauses.exit128_crit_edge.i
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB1_11
.LBB1_7:
	cmpl	$0, 68(%rax)
	jne	.LBB1_11
# BB#8:
	cmpl	$0, 72(%rax)
	jne	.LBB1_11
# BB#9:                                 # %clause_IsEmptyClause.exit135.i
	cmpl	$0, 64(%rax)
	je	.LBB1_10
.LBB1_11:                               # %clause_IsEmptyClause.exit135.thread.i
	cmpl	$0, 364(%r15)
	movq	%r13, 64(%rsp)          # 8-byte Spill
	je	.LBB1_12
# BB#13:
	movq	24(%rsp), %r12
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	red_ForwardSubsumer
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_16
# BB#14:
	movl	48(%r15), %eax
	testl	%eax, %eax
	je	.LBB1_16
# BB#15:
	movq	stdout(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
	movl	(%r13), %esi
	movl	12(%r13), %edx
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_16:                               # %red_ForwardSubsumption.exit141.i
	xorl	%ebp, %ebp
	testq	%r13, %r13
	setne	%bpl
	movl	%ebp, %r12d
	jne	.LBB1_64
	jmp	.LBB1_17
.LBB1_12:
	xorl	%ebp, %ebp
.LBB1_17:
	cmpl	$0, 316(%r15)
	je	.LBB1_19
# BB#18:
	movq	24(%rsp), %rdi
	movl	132(%r14), %r9d
	leaq	32(%rsp), %r8
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	red_RewriteRedClause
	testl	%eax, %eax
	jne	.LBB1_21
.LBB1_19:
	cmpl	$0, 324(%r15)
	je	.LBB1_34
# BB#20:
	movq	24(%rsp), %rsi
	movl	132(%r14), %ecx
	leaq	32(%rsp), %r8
	movq	%r14, %rdi
	movl	60(%rsp), %edx          # 4-byte Reload
	callq	red_ContextualRewriting
	testl	%eax, %eax
	je	.LBB1_34
.LBB1_21:
	cmpq	$0, 32(%rsp)
	je	.LBB1_23
# BB#22:
	movq	24(%rsp), %rbx
	movq	40(%rsp), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 40(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	$0, 32(%rsp)
.LBB1_23:                               # %red_ExchangeClauses.exit142.i
	leaq	24(%rsp), %rdi
	leaq	40(%rsp), %rcx
	movq	%r15, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	red_SimpleStaticReductions
	movl	%eax, %r12d
	testl	%r12d, %r12d
	jne	.LBB1_61
# BB#24:
	movq	24(%rsp), %r12
	testq	%r12, %r12
	movq	64(%rsp), %rsi          # 8-byte Reload
	je	.LBB1_28
# BB#25:
	cmpl	$0, 68(%r12)
	jne	.LBB1_28
# BB#26:
	cmpl	$0, 72(%r12)
	jne	.LBB1_28
# BB#27:                                # %clause_IsEmptyClause.exit149.i
	cmpl	$0, 64(%r12)
	je	.LBB1_10
.LBB1_28:                               # %clause_IsEmptyClause.exit149.thread.i
	cmpl	$0, 364(%r15)
	je	.LBB1_29
# BB#30:
	movq	%r12, %rdi
	callq	red_ForwardSubsumer
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB1_33
# BB#31:
	movl	48(%r15), %eax
	testl	%eax, %eax
	je	.LBB1_33
# BB#32:
	movq	stdout(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
	movl	(%r13), %esi
	movl	12(%r13), %edx
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
.LBB1_33:                               # %red_ForwardSubsumption.exit.i
	xorl	%ebp, %ebp
	testq	%r13, %r13
	setne	%bpl
	movl	%ebp, %r12d
	jne	.LBB1_64
	jmp	.LBB1_34
.LBB1_29:
	xorl	%ebp, %ebp
.LBB1_34:
	cmpl	$2, 60(%rsp)            # 4-byte Folded Reload
	movb	23(%rsp), %r13b         # 1-byte Reload
	jne	.LBB1_43
# BB#35:
	cmpl	$0, 360(%r15)
	je	.LBB1_43
# BB#36:
	movq	80(%r14), %rdi
	movq	24(%rsp), %rsi
	movl	132(%r14), %edx
	movl	36(%r15), %ecx
	leaq	32(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%r15, %r8
	movq	48(%rsp), %r9           # 8-byte Reload
	callq	red_SortSimplification
	cmpq	$0, 32(%rsp)
	je	.LBB1_37
# BB#38:
	movq	24(%rsp), %rbx
	movq	40(%rsp), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movb	23(%rsp), %r13b         # 1-byte Reload
	movq	%rax, 40(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	$0, 32(%rsp)
	testq	%rax, %rax
	jne	.LBB1_40
	jmp	.LBB1_43
.LBB1_37:                               # %.red_ExchangeClauses.exit125_crit_edge.i
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB1_43
.LBB1_40:
	cmpl	$0, 68(%rax)
	jne	.LBB1_43
# BB#41:
	cmpl	$0, 72(%rax)
	jne	.LBB1_43
# BB#42:                                # %clause_IsEmptyClause.exit124.i
	cmpl	$0, 64(%rax)
	je	.LBB1_10
.LBB1_43:                               # %clause_IsEmptyClause.exit124.thread.i
	cmpl	$0, 332(%r15)
	je	.LBB1_51
# BB#44:
	movq	24(%rsp), %rdi
	movl	132(%r14), %r9d
	leaq	32(%rsp), %r8
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	red_MatchingReplacementResolution
	cmpq	$0, 32(%rsp)
	je	.LBB1_45
# BB#46:
	movq	24(%rsp), %rbx
	movq	40(%rsp), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movb	23(%rsp), %r13b         # 1-byte Reload
	movq	%rax, 40(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	$0, 32(%rsp)
	testq	%rax, %rax
	jne	.LBB1_48
	jmp	.LBB1_51
.LBB1_45:                               # %.red_ExchangeClauses.exit117_crit_edge.i
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB1_51
.LBB1_48:
	cmpl	$0, 68(%rax)
	jne	.LBB1_51
# BB#49:
	cmpl	$0, 72(%rax)
	jne	.LBB1_51
# BB#50:                                # %clause_IsEmptyClause.exit116.i
	cmpl	$0, 64(%rax)
	je	.LBB1_10
.LBB1_51:                               # %clause_IsEmptyClause.exit116.thread.i
	cmpl	$0, 344(%r15)
	je	.LBB1_62
# BB#52:
	movq	24(%rsp), %rdi
	movl	132(%r14), %r9d
	leaq	32(%rsp), %r8
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	red_UnitConflict
	cmpq	$0, 32(%rsp)
	je	.LBB1_53
# BB#54:
	movq	24(%rsp), %rbx
	movq	40(%rsp), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movb	23(%rsp), %r13b         # 1-byte Reload
	movq	%rax, 40(%rsp)
	movq	32(%rsp), %rax
	movq	%rax, 24(%rsp)
	movq	$0, 32(%rsp)
	testq	%rax, %rax
	jne	.LBB1_56
	jmp	.LBB1_62
.LBB1_53:                               # %.red_ExchangeClauses.exit_crit_edge.i
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB1_62
.LBB1_56:
	cmpl	$0, 68(%rax)
	jne	.LBB1_62
# BB#57:
	cmpl	$0, 72(%rax)
	je	.LBB1_58
.LBB1_62:                               # %clause_IsEmptyClause.exit.thread.i
	cmpl	$2, 60(%rsp)            # 4-byte Folded Reload
	jne	.LBB1_63
.LBB1_59:
	cmpl	$0, 356(%r15)
	je	.LBB1_63
# BB#60:
	movq	64(%r14), %rdi
	movq	24(%rsp), %rsi
	movq	%r15, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	red_ClauseDeletion
	movl	%eax, %r12d
	jmp	.LBB1_61
.LBB1_63:
	xorl	%r13d, %r13d
	movl	%ebp, %r12d
	jmp	.LBB1_64
.LBB1_10:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	jmp	.LBB1_64
.LBB1_58:                               # %clause_IsEmptyClause.exit.i
	movl	64(%rax), %r12d
	testl	%r12d, %r12d
	sete	%al
	cmovnel	%ebp, %r12d
	orb	%r13b, %al
	jne	.LBB1_61
	jmp	.LBB1_59
.Lfunc_end1:
	.size	red_ReductionOnDerivedClause, .Lfunc_end1-red_ReductionOnDerivedClause
	.cfi_endproc

	.globl	red_CompleteReductionOnDerivedClause
	.p2align	4, 0x90
	.type	red_CompleteReductionOnDerivedClause,@function
red_CompleteReductionOnDerivedClause:   # @red_CompleteReductionOnDerivedClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 80
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movl	%edx, %eax
	movq	%rdi, %r12
	movq	%rsi, 8(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 24(%rsp)
	leaq	8(%rsp), %rsi
	leaq	24(%rsp), %rdx
	leaq	16(%rsp), %rcx
	movl	%eax, %r8d
	callq	red_SelectedStaticReductions
	movl	%eax, %r14d
	movq	16(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB2_10
# BB#1:                                 # %.lr.ph.i10
	movq	8(%rsp), %r15
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movl	12(%r15), %edx
	cmpl	12(%rsi), %edx
	ja	.LBB2_4
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpl	132(%r12), %edx
	ja	.LBB2_4
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	112(%r12), %rax
	cmpl	$0, 36(%rax)
	je	.LBB2_7
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rdi
	callq	prfs_InsertDocProofClause
	jmp	.LBB2_8
	.p2align	4, 0x90
.LBB2_4:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%r12, %rdi
	callq	split_KeepClauseAtLevel
	jmp	.LBB2_8
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	movq	%rsi, %rdi
	callq	clause_Delete
	.p2align	4, 0x90
.LBB2_8:                                #   in Loop: Header=BB2_2 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB2_2
	.p2align	4, 0x90
.LBB2_9:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB2_9
.LBB2_10:                               # %list_Delete.exit
	testl	%r14d, %r14d
	je	.LBB2_11
# BB#12:
	movq	24(%rsp), %rax
	testq	%rax, %rax
	je	.LBB2_13
# BB#16:
	movl	12(%rax), %edx
	movq	8(%rsp), %rsi
	cmpl	12(%rsi), %edx
	ja	.LBB2_18
# BB#17:
	cmpl	132(%r12), %edx
	jbe	.LBB2_14
.LBB2_18:
	movq	%r12, %rdi
	callq	split_KeepClauseAtLevel
	jmp	.LBB2_20
.LBB2_11:                               # %list_Delete.exit._crit_edge
	movq	8(%rsp), %rax
	jmp	.LBB2_21
.LBB2_13:                               # %._crit_edge
	movq	8(%rsp), %rsi
.LBB2_14:
	movq	112(%r12), %rax
	cmpl	$0, 36(%rax)
	je	.LBB2_19
# BB#15:
	movq	%r12, %rdi
	callq	prfs_InsertDocProofClause
	jmp	.LBB2_20
.LBB2_19:
	movq	%rsi, %rdi
	callq	clause_Delete
.LBB2_20:
	movq	$0, 8(%rsp)
	xorl	%eax, %eax
.LBB2_21:
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	red_CompleteReductionOnDerivedClause, .Lfunc_end2-red_CompleteReductionOnDerivedClause
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_SelectedStaticReductions,@function
red_SelectedStaticReductions:           # @red_SelectedStaticReductions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 128
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %r12
	movq	104(%r12), %rbp
	movq	112(%r12), %r15
	movl	%r8d, %eax
	orl	$1, %eax
	cmpl	$3, %eax
	jne	.LBB3_2
# BB#1:
	movq	32(%r12), %rax
	jmp	.LBB3_3
.LBB3_2:
	xorl	%eax, %eax
.LBB3_3:
	movq	%rax, 48(%rsp)          # 8-byte Spill
	orl	$2, %r8d
	cmpl	$3, %r8d
	jne	.LBB3_5
# BB#4:
	movq	48(%r12), %rax
	jmp	.LBB3_6
.LBB3_5:
	xorl	%eax, %eax
.LBB3_6:
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	$0, 16(%rsp)
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	callq	red_SimpleStaticReductions
	movl	%eax, %r14d
	testl	%r14d, %r14d
	je	.LBB3_7
.LBB3_119:                              # %.loopexit
	movl	%r14d, %eax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_7:
	movl	372(%r15), %eax
	testl	%eax, %eax
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	je	.LBB3_15
# BB#8:
	movq	(%r13), %rdi
	movl	156(%r12), %r8d
	xorl	%r9d, %r9d
	cmpl	$2, %eax
	sete	%r9b
	leaq	16(%rsp), %rcx
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	red_AssignmentEquationDeletion
	testl	%eax, %eax
	je	.LBB3_15
# BB#9:
	cmpq	$0, 16(%rsp)
	je	.LBB3_11
# BB#10:
	movq	(%r13), %r14
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	16(%rsp), %rax
	movq	%rax, (%r13)
	movq	$0, 16(%rsp)
	testq	%rax, %rax
	jne	.LBB3_12
	jmp	.LBB3_15
.LBB3_11:                               # %.red_ExchangeClauses.exit_crit_edge
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_15
.LBB3_12:
	cmpl	$0, 68(%rax)
	jne	.LBB3_15
# BB#13:
	cmpl	$0, 72(%rax)
	jne	.LBB3_15
# BB#14:                                # %clause_IsEmptyClause.exit
	cmpl	$0, 64(%rax)
	je	.LBB3_118
.LBB3_15:                               # %clause_IsEmptyClause.exit.thread
	cmpl	$0, 364(%r15)
	je	.LBB3_26
# BB#16:
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB3_20
# BB#17:
	movq	(%r13), %r14
	movq	%r14, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	red_ForwardSubsumer
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_19
# BB#18:
	movl	48(%r15), %eax
	testl	%eax, %eax
	jne	.LBB3_23
.LBB3_19:                               # %red_ForwardSubsumption.exit
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	movl	$1, %r14d
	testq	%rbp, %rbp
	movq	32(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_119
.LBB3_20:
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB3_26
# BB#21:
	movq	(%r13), %r14
	movq	%r14, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	red_ForwardSubsumer
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_25
# BB#22:
	movl	48(%r15), %eax
	testl	%eax, %eax
	je	.LBB3_25
.LBB3_23:                               # %red_ForwardSubsumption.exit.thread
	movq	stdout(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
.LBB3_24:                               # %.loopexit
	callq	clause_Print
	movl	(%rbp), %esi
	movl	12(%rbp), %edx
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	callq	printf
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	movl	$1, %r14d
	jmp	.LBB3_119
.LBB3_25:                               # %red_ForwardSubsumption.exit215
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	movl	$1, %r14d
	testq	%rbp, %rbp
	movq	32(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_119
.LBB3_26:
	movl	316(%r15), %ecx
	movl	324(%r15), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	%ecx, 68(%rsp)          # 4-byte Spill
	orl	%ecx, %eax
	je	.LBB3_72
# BB#27:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_28:                               # %.thread.backedge.thread
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	movl	$0, %r14d
	je	.LBB3_49
# BB#29:                                #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	je	.LBB3_31
# BB#30:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rdi
	movl	132(%r12), %r9d
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%rbp, %rcx
	leaq	16(%rsp), %r8
	callq	red_RewriteRedClause
	testl	%eax, %eax
	jne	.LBB3_33
.LBB3_31:                               #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	movl	$0, %r14d
	je	.LBB3_49
# BB#32:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rsi
	movl	132(%r12), %ecx
	movl	$2, %edx
	movq	%r12, %rdi
	leaq	16(%rsp), %r8
	callq	red_ContextualRewriting
	testl	%eax, %eax
	movl	$0, %r14d
	je	.LBB3_49
.LBB3_33:                               #   in Loop: Header=BB3_28 Depth=1
	cmpq	$0, 16(%rsp)
	movq	56(%rsp), %rcx          # 8-byte Reload
	je	.LBB3_35
# BB#34:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rbp
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%r14, (%rax)
	movq	56(%rsp), %rcx          # 8-byte Reload
	movq	%rax, (%rcx)
	movq	16(%rsp), %rax
	movq	%rax, (%r13)
	movq	$0, 16(%rsp)
.LBB3_35:                               # %red_ExchangeClauses.exit216
                                        #   in Loop: Header=BB3_28 Depth=1
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	red_SimpleStaticReductions
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB3_119
# BB#36:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_40
# BB#37:                                #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 68(%rax)
	jne	.LBB3_40
# BB#38:                                #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 72(%rax)
	jne	.LBB3_40
# BB#39:                                # %clause_IsEmptyClause.exit223
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 64(%rax)
	je	.LBB3_118
	.p2align	4, 0x90
.LBB3_40:                               # %clause_IsEmptyClause.exit223.thread
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 364(%r15)
	je	.LBB3_48
# BB#41:                                #   in Loop: Header=BB3_28 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	movq	48(%rsp), %rsi          # 8-byte Reload
	callq	red_ForwardSubsumer
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_43
# BB#42:                                #   in Loop: Header=BB3_28 Depth=1
	movl	48(%r15), %eax
	testl	%eax, %eax
	jne	.LBB3_101
.LBB3_43:                               # %red_ForwardSubsumption.exit229
                                        #   in Loop: Header=BB3_28 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	testq	%rbp, %rbp
	jne	.LBB3_117
# BB#44:                                #   in Loop: Header=BB3_28 Depth=1
	movl	$1, %ebx
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_28
# BB#45:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	red_ForwardSubsumer
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_47
# BB#46:                                #   in Loop: Header=BB3_28 Depth=1
	movl	48(%r15), %eax
	testl	%eax, %eax
	jne	.LBB3_101
.LBB3_47:                               # %red_ForwardSubsumption.exit235
                                        #   in Loop: Header=BB3_28 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	movl	$1, %r14d
	testq	%rbp, %rbp
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_52
	jmp	.LBB3_119
.LBB3_48:                               #   in Loop: Header=BB3_28 Depth=1
	movl	$1, %r14d
	.p2align	4, 0x90
.LBB3_49:                               #   in Loop: Header=BB3_28 Depth=1
	cmpq	$0, 40(%rsp)            # 8-byte Folded Reload
	je	.LBB3_71
# BB#50:                                #   in Loop: Header=BB3_28 Depth=1
	testl	%ebx, %ebx
	je	.LBB3_52
# BB#51:                                #   in Loop: Header=BB3_28 Depth=1
	testl	%r14d, %r14d
	je	.LBB3_72
.LBB3_52:                               # %.thread299
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 68(%rsp)            # 4-byte Folded Reload
	je	.LBB3_54
# BB#53:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rdi
	movl	132(%r12), %r9d
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	%r15, %rdx
	movq	%rbp, %rcx
	leaq	16(%rsp), %r8
	callq	red_RewriteRedClause
	testl	%eax, %eax
	jne	.LBB3_56
.LBB3_54:                               #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	je	.LBB3_71
# BB#55:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rsi
	movl	132(%r12), %ecx
	movl	$1, %edx
	movq	%r12, %rdi
	leaq	16(%rsp), %r8
	callq	red_ContextualRewriting
	testl	%eax, %eax
	je	.LBB3_71
.LBB3_56:                               #   in Loop: Header=BB3_28 Depth=1
	cmpq	$0, 16(%rsp)
	movq	56(%rsp), %rbx          # 8-byte Reload
	je	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %r14
	movq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%rbx)
	movq	16(%rsp), %rax
	movq	%rax, (%r13)
	movq	$0, 16(%rsp)
.LBB3_58:                               # %red_ExchangeClauses.exit236
                                        #   in Loop: Header=BB3_28 Depth=1
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	callq	red_SimpleStaticReductions
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB3_119
# BB#59:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_63
# BB#60:                                #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 68(%rax)
	jne	.LBB3_63
# BB#61:                                #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 72(%rax)
	jne	.LBB3_63
# BB#62:                                # %clause_IsEmptyClause.exit243
                                        #   in Loop: Header=BB3_28 Depth=1
	cmpl	$0, 64(%rax)
	je	.LBB3_118
	.p2align	4, 0x90
.LBB3_63:                               # %clause_IsEmptyClause.exit243.thread
                                        #   in Loop: Header=BB3_28 Depth=1
	movl	$1, %ebx
	cmpl	$0, 364(%r15)
	je	.LBB3_28
# BB#64:                                #   in Loop: Header=BB3_28 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	$0, (%rax)
	movq	48(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB3_68
# BB#65:                                #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	callq	red_ForwardSubsumer
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_67
# BB#66:                                #   in Loop: Header=BB3_28 Depth=1
	movl	48(%r15), %eax
	testl	%eax, %eax
	jne	.LBB3_101
.LBB3_67:                               #   in Loop: Header=BB3_28 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	testq	%rbp, %rbp
	jne	.LBB3_117
.LBB3_68:                               # %.thread303
                                        #   in Loop: Header=BB3_28 Depth=1
	movq	(%r13), %rbx
	movq	%rbx, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	red_ForwardSubsumer
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB3_70
# BB#69:                                # %.thread303
                                        #   in Loop: Header=BB3_28 Depth=1
	movl	48(%r15), %eax
	testl	%eax, %eax
	jne	.LBB3_101
.LBB3_70:                               # %red_ForwardSubsumption.exit255
                                        #   in Loop: Header=BB3_28 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	movl	$1, %ebx
	testq	%rbp, %rbp
	movl	$1, %r14d
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_28
	jmp	.LBB3_119
	.p2align	4, 0x90
.LBB3_71:                               # %.thread.backedge
                                        #   in Loop: Header=BB3_28 Depth=1
	movl	$1, %ebx
	testl	%r14d, %r14d
	jne	.LBB3_28
.LBB3_72:                               # %.thread._crit_edge
	cmpl	$0, 360(%r15)
	movq	56(%rsp), %r14          # 8-byte Reload
	je	.LBB3_79
# BB#73:
	movq	80(%r12), %rdi
	movq	(%r13), %rsi
	movl	132(%r12), %edx
	movl	36(%r15), %ecx
	leaq	16(%rsp), %rax
	movq	%rax, (%rsp)
	movq	%r15, %r8
	movq	%rbp, %r9
	callq	red_SortSimplification
	cmpq	$0, 16(%rsp)
	je	.LBB3_75
# BB#74:
	movq	(%r13), %rbx
	movq	(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%r14)
	movq	16(%rsp), %rax
	movq	%rax, (%r13)
	movq	$0, 16(%rsp)
	testq	%rax, %rax
	jne	.LBB3_76
	jmp	.LBB3_79
.LBB3_75:                               # %.red_ExchangeClauses.exit256_crit_edge
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_79
.LBB3_76:
	cmpl	$0, 68(%rax)
	jne	.LBB3_79
# BB#77:
	cmpl	$0, 72(%rax)
	jne	.LBB3_79
# BB#78:                                # %clause_IsEmptyClause.exit263
	cmpl	$0, 64(%rax)
	je	.LBB3_118
.LBB3_79:                               # %clause_IsEmptyClause.exit263.thread
	cmpl	$0, 332(%r15)
	je	.LBB3_96
# BB#80:
	movq	48(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB3_82
# BB#81:
	movq	(%r13), %rdi
	movl	132(%r12), %r9d
	leaq	16(%rsp), %r8
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	red_MatchingReplacementResolution
.LBB3_82:
	cmpq	$0, 16(%rsp)
	je	.LBB3_84
# BB#83:
	movq	(%r13), %rbx
	movq	(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%r14)
	movq	16(%rsp), %rdi
	movq	%rdi, (%r13)
	movq	$0, 16(%rsp)
	jmp	.LBB3_85
.LBB3_84:                               # %.red_ExchangeClauses.exit264_crit_edge
	movq	(%r13), %rdi
.LBB3_85:                               # %red_ExchangeClauses.exit264
	movq	40(%rsp), %rax          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_89
# BB#86:
	cmpl	$0, 68(%rdi)
	jne	.LBB3_89
# BB#87:
	cmpl	$0, 72(%rdi)
	jne	.LBB3_89
# BB#88:                                # %clause_IsEmptyClause.exit271
	cmpl	$0, 64(%rdi)
	je	.LBB3_118
.LBB3_89:                               # %clause_IsEmptyClause.exit271.thread
	testq	%rax, %rax
	movq	%rax, %rsi
	je	.LBB3_92
# BB#90:
	movl	132(%r12), %r9d
	leaq	16(%rsp), %r8
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	red_MatchingReplacementResolution
	cmpq	$0, 16(%rsp)
	je	.LBB3_92
# BB#91:
	movq	(%r13), %rbx
	movq	(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%r14)
	movq	16(%rsp), %rax
	movq	%rax, (%r13)
	movq	$0, 16(%rsp)
	testq	%rax, %rax
	jne	.LBB3_93
	jmp	.LBB3_96
.LBB3_92:                               # %.red_ExchangeClauses.exit272_crit_edge
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_96
.LBB3_93:
	cmpl	$0, 68(%rax)
	jne	.LBB3_96
# BB#94:
	cmpl	$0, 72(%rax)
	jne	.LBB3_96
# BB#95:                                # %clause_IsEmptyClause.exit279
	cmpl	$0, 64(%rax)
	je	.LBB3_118
.LBB3_96:                               # %clause_IsEmptyClause.exit279.thread
	cmpl	$0, 344(%r15)
	je	.LBB3_114
# BB#97:
	movq	48(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB3_99
# BB#98:
	movq	(%r13), %rdi
	movl	132(%r12), %r9d
	leaq	16(%rsp), %r8
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	red_UnitConflict
.LBB3_99:
	cmpq	$0, 16(%rsp)
	je	.LBB3_102
# BB#100:
	movq	(%r13), %rbx
	movq	(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%r14)
	movq	16(%rsp), %rdi
	movq	%rdi, (%r13)
	movq	$0, 16(%rsp)
	jmp	.LBB3_103
.LBB3_101:                              # %red_ForwardSubsumption.exit229.thread
	movq	stdout(%rip), %rcx
	movl	$.L.str.25, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	jmp	.LBB3_24
.LBB3_102:                              # %.red_ExchangeClauses.exit280_crit_edge
	movq	(%r13), %rdi
.LBB3_103:                              # %red_ExchangeClauses.exit280
	movq	40(%rsp), %rsi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB3_107
# BB#104:
	cmpl	$0, 68(%rdi)
	jne	.LBB3_107
# BB#105:
	cmpl	$0, 72(%rdi)
	jne	.LBB3_107
# BB#106:                               # %clause_IsEmptyClause.exit287
	cmpl	$0, 64(%rdi)
	je	.LBB3_118
.LBB3_107:                              # %clause_IsEmptyClause.exit287.thread
	testq	%rsi, %rsi
	je	.LBB3_110
# BB#108:
	movl	132(%r12), %r9d
	leaq	16(%rsp), %r8
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	red_UnitConflict
	cmpq	$0, 16(%rsp)
	je	.LBB3_110
# BB#109:
	movq	(%r13), %rbx
	movq	(%r14), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rax, (%r14)
	movq	16(%rsp), %rax
	movq	%rax, (%r13)
	movq	$0, 16(%rsp)
	testq	%rax, %rax
	jne	.LBB3_111
	jmp	.LBB3_114
.LBB3_110:                              # %.red_ExchangeClauses.exit288_crit_edge
	movq	(%r13), %rax
	testq	%rax, %rax
	je	.LBB3_114
.LBB3_111:
	cmpl	$0, 68(%rax)
	jne	.LBB3_114
# BB#112:
	cmpl	$0, 72(%rax)
	jne	.LBB3_114
# BB#113:                               # %clause_IsEmptyClause.exit295
	cmpl	$0, 64(%rax)
	je	.LBB3_118
.LBB3_114:                              # %clause_IsEmptyClause.exit295.thread
	cmpl	$0, 356(%r15)
	je	.LBB3_118
# BB#115:
	movq	64(%r12), %rdi
	movq	(%r13), %rsi
	movq	%r15, %rdx
	movq	%rbp, %rcx
	callq	red_ClauseDeletion
	movl	%eax, %r14d
	jmp	.LBB3_119
.LBB3_118:
	xorl	%r14d, %r14d
	jmp	.LBB3_119
.LBB3_117:
	movl	$1, %r14d
	jmp	.LBB3_119
.Lfunc_end3:
	.size	red_SelectedStaticReductions, .Lfunc_end3-red_SelectedStaticReductions
	.cfi_endproc

	.globl	red_BackReduction
	.p2align	4, 0x90
	.type	red_BackReduction,@function
red_BackReduction:                      # @red_BackReduction
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 64
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	$0, (%rsp)
	movq	104(%r15), %r12
	movq	112(%r15), %rbx
	cmpl	$0, 368(%rbx)
	je	.LBB4_13
# BB#1:
	movl	%r13d, %eax
	orl	$1, %eax
	cmpl	$3, %eax
	jne	.LBB4_2
# BB#3:
	movq	32(%r15), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	red_BackSubsumption
	movq	%rax, %rbp
	jmp	.LBB4_4
.LBB4_2:
	xorl	%ebp, %ebp
.LBB4_4:
	movl	%r13d, %eax
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB4_11
# BB#5:
	movq	48(%r15), %rsi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	red_BackSubsumption
	testq	%rbp, %rbp
	je	.LBB4_6
# BB#7:
	testq	%rax, %rax
	je	.LBB4_53
# BB#8:
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB4_9:                                # %.preheader.i98
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_9
# BB#10:
	movq	%rax, (%rdx)
	jmp	.LBB4_11
.LBB4_6:
	movq	%rax, %rbp
.LBB4_11:                               # %list_Nconc.exit100
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	testq	%rbp, %rbp
	je	.LBB4_13
	.p2align	4, 0x90
.LBB4_12:                               # %.lr.ph.i104
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB4_12
.LBB4_13:                               # %list_Delete.exit105
	cmpl	$0, 336(%rbx)
	je	.LBB4_26
# BB#14:
	movl	%r13d, %eax
	orl	$1, %eax
	cmpl	$3, %eax
	jne	.LBB4_15
# BB#16:
	movq	32(%r15), %rsi
	movq	%rsp, %r8
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	red_BackMatchingReplacementResolution
	movq	%rax, %rbp
	jmp	.LBB4_17
.LBB4_15:
	xorl	%ebp, %ebp
.LBB4_17:
	movl	%r13d, %eax
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB4_24
# BB#18:
	movq	48(%r15), %rsi
	movq	%rsp, %r8
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	red_BackMatchingReplacementResolution
	testq	%rbp, %rbp
	je	.LBB4_19
# BB#20:
	testq	%rax, %rax
	je	.LBB4_54
# BB#21:
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB4_22:                               # %.preheader.i92
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_22
# BB#23:
	movq	%rax, (%rdx)
	jmp	.LBB4_24
.LBB4_19:
	movq	%rax, %rbp
.LBB4_24:                               # %list_Nconc.exit94
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	testq	%rbp, %rbp
	je	.LBB4_26
	.p2align	4, 0x90
.LBB4_25:                               # %.lr.ph.i87
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB4_25
.LBB4_26:                               # %list_Delete.exit88
	cmpl	$0, 320(%rbx)
	je	.LBB4_39
# BB#27:
	movl	%r13d, %eax
	orl	$1, %eax
	cmpl	$3, %eax
	jne	.LBB4_28
# BB#29:
	movq	32(%r15), %rsi
	movq	%rsp, %r8
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	red_BackRewriting
	movq	%rax, %rbp
	jmp	.LBB4_30
.LBB4_28:
	xorl	%ebp, %ebp
.LBB4_30:
	movl	%r13d, %eax
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB4_37
# BB#31:
	movq	48(%r15), %rsi
	movq	%rsp, %r8
	movq	%r14, %rdi
	movq	%rbx, %rdx
	movq	%r12, %rcx
	callq	red_BackRewriting
	testq	%rbp, %rbp
	je	.LBB4_32
# BB#33:
	testq	%rax, %rax
	je	.LBB4_55
# BB#34:
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB4_35:                               # %.preheader.i81
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_35
# BB#36:
	movq	%rax, (%rdx)
	jmp	.LBB4_37
.LBB4_32:
	movq	%rax, %rbp
.LBB4_37:                               # %list_Nconc.exit83
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	testq	%rbp, %rbp
	je	.LBB4_39
	.p2align	4, 0x90
.LBB4_38:                               # %.lr.ph.i76
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB4_38
.LBB4_39:                               # %list_Delete.exit77
	cmpl	$0, 328(%rbx)
	je	.LBB4_52
# BB#40:
	movl	%r13d, %eax
	orl	$1, %eax
	cmpl	$3, %eax
	jne	.LBB4_41
# BB#42:
	movq	%rsp, %rcx
	movl	$2, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	red_BackContextualRewriting
	movq	%rax, %rbx
	jmp	.LBB4_43
.LBB4_41:
	xorl	%ebx, %ebx
.LBB4_43:
	orl	$2, %r13d
	cmpl	$3, %r13d
	jne	.LBB4_50
# BB#44:
	movq	%rsp, %rcx
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	red_BackContextualRewriting
	testq	%rbx, %rbx
	je	.LBB4_45
# BB#46:
	testq	%rax, %rax
	je	.LBB4_56
# BB#47:
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB4_48:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB4_48
# BB#49:
	movq	%rax, (%rdx)
	jmp	.LBB4_50
.LBB4_45:
	movq	%rax, %rbx
.LBB4_50:                               # %list_Nconc.exit
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	testq	%rbx, %rbx
	je	.LBB4_52
	.p2align	4, 0x90
.LBB4_51:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB4_51
.LBB4_52:                               # %list_Delete.exit
	movq	(%rsp), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_53:                               # %list_Nconc.exit100.thread
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	jmp	.LBB4_12
.LBB4_54:                               # %list_Nconc.exit94.thread
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	jmp	.LBB4_25
.LBB4_55:                               # %list_Nconc.exit83.thread
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	jmp	.LBB4_38
.LBB4_56:                               # %list_Nconc.exit.thread
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	red_HandleRedundantIndexedClauses
	jmp	.LBB4_51
.Lfunc_end4:
	.size	red_BackReduction, .Lfunc_end4-red_BackReduction
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_BackSubsumption,@function
red_BackSubsumption:                    # @red_BackSubsumption
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 128
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	testq	%r12, %r12
	je	.LBB5_1
# BB#4:
	movl	68(%r12), %edi
	testl	%edi, %edi
	jne	.LBB5_2
# BB#5:
	xorl	%edi, %edi
	cmpl	$0, 72(%r12)
	jne	.LBB5_2
# BB#6:                                 # %clause_IsEmptyClause.exit
	xorl	%edi, %edi
	cmpl	$0, 64(%r12)
	jne	.LBB5_2
# BB#7:
	xorl	%ebx, %ebx
	jmp	.LBB5_139
.LBB5_1:                                # %.clause_IsEmptyClause.exit.thread_crit_edge
	movl	68, %edi
.LBB5_2:                                # %clause_IsEmptyClause.exit.thread
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movl	64(%r12), %r14d
	leal	-1(%r14,%rdi), %eax
	addl	%r14d, %edi
	movl	72(%r12), %r8d
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	(%rax,%r8), %eax
	testl	%eax, %eax
	movq	56(%r12), %rax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jle	.LBB5_3
# BB#8:                                 # %.lr.ph146
	addl	%edi, %r8d
	testb	$1, %r8b
	jne	.LBB5_9
# BB#10:
	movq	(%rax), %rdx
	movq	8(%rax), %rdi
	movl	4(%rdx), %edx
	cmpl	4(%rdi), %edx
	sbbl	%ebx, %ebx
	andl	$1, %ebx
	movl	$2, %edx
	cmpl	$2, %r8d
	jne	.LBB5_12
	jmp	.LBB5_13
.LBB5_3:
	xorl	%ebx, %ebx
	jmp	.LBB5_13
.LBB5_9:
	xorl	%ebx, %ebx
	movl	$1, %edx
	cmpl	$2, %r8d
	je	.LBB5_13
	.p2align	4, 0x90
.LBB5_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdx,8), %rdi
	movl	4(%rdi), %ecx
	movslq	%ebx, %rdi
	movq	(%rax,%rdi,8), %rbp
	cmpl	4(%rbp), %ecx
	cmoval	%edx, %edi
	movq	8(%rax,%rdx,8), %rcx
	movl	4(%rcx), %ecx
	movslq	%edi, %rbx
	movq	(%rax,%rbx,8), %rdi
	leal	1(%rdx), %ebp
	cmpl	4(%rdi), %ecx
	cmoval	%ebp, %ebx
	addq	$2, %rdx
	cmpq	%r8, %rdx
	jne	.LBB5_12
.LBB5_13:                               # %._crit_edge147
	movslq	%ebx, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB5_15
# BB#14:
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB5_15:                               # %clause_GetLiteralAtom.exit
	leal	-1(%r14), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	(%rsi), %rsi
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rcx, %rdx
	callq	st_ExistInstance
	testq	%rax, %rax
	movq	%r14, 40(%rsp)          # 8-byte Spill
	je	.LBB5_16
# BB#17:                                # %.lr.ph139
	cmpl	%r14d, %ebx
	setl	%cl
	movq	%rbx, %r15
	cmpl	32(%rsp), %ebx          # 4-byte Folded Reload
	setg	%dl
	orb	%cl, %dl
	movb	%dl, 7(%rsp)            # 1-byte Spill
	xorl	%ebx, %ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_18:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_22 Depth 2
                                        #       Child Loop BB5_23 Depth 3
                                        #       Child Loop BB5_26 Depth 3
                                        #       Child Loop BB5_31 Depth 3
                                        #       Child Loop BB5_36 Depth 3
                                        #       Child Loop BB5_41 Depth 3
                                        #     Child Loop BB5_46 Depth 2
                                        #       Child Loop BB5_47 Depth 3
                                        #       Child Loop BB5_51 Depth 3
                                        #       Child Loop BB5_56 Depth 3
                                        #       Child Loop BB5_61 Depth 3
	movq	%rax, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_19
# BB#20:                                # %.lr.ph134
                                        #   in Loop: Header=BB5_18 Depth=1
	cmpl	20(%rsp), %r15d         # 4-byte Folded Reload
	jle	.LBB5_21
# BB#45:                                # %.lr.ph134.split.us.preheader
                                        #   in Loop: Header=BB5_18 Depth=1
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB5_46:                               # %.lr.ph134.split.us
                                        #   Parent Loop BB5_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_47 Depth 3
                                        #       Child Loop BB5_51 Depth 3
                                        #       Child Loop BB5_56 Depth 3
                                        #       Child Loop BB5_61 Depth 3
	movq	8(%r14), %rax
	movq	16(%rax), %rbx
	movq	56(%rbx), %rdx
	movl	$-1, %ecx
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_47:                               #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ecx
	cmpq	%rax, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB5_47
# BB#48:                                # %clause_LiteralGetIndex.exit106.us
                                        #   in Loop: Header=BB5_46 Depth=2
	cmpq	%r12, %rbx
	je	.LBB5_67
# BB#49:                                #   in Loop: Header=BB5_46 Depth=2
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB5_50
.LBB5_54:                               #   in Loop: Header=BB5_46 Depth=2
	cmpl	%ebp, %r15d
	jl	.LBB5_67
# BB#55:                                # %.preheader160.preheader
                                        #   in Loop: Header=BB5_46 Depth=2
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB5_56:                               # %.preheader160
                                        #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%esi
	cmpq	%rax, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB5_56
# BB#57:                                # %clause_LiteralIsFromSuccedent.exit75.us
                                        #   in Loop: Header=BB5_46 Depth=2
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	cmpl	%eax, %esi
	jl	.LBB5_67
# BB#58:                                # %clause_LiteralIsFromSuccedent.exit75.us
                                        #   in Loop: Header=BB5_46 Depth=2
	movl	72(%rbx), %edx
	leal	-1(%rdx,%rax), %eax
	cmpl	%eax, %esi
	jle	.LBB5_59
	jmp	.LBB5_67
	.p2align	4, 0x90
.LBB5_50:                               # %.preheader161.preheader
                                        #   in Loop: Header=BB5_46 Depth=2
	movl	$-1, %esi
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB5_51:                               # %.preheader161
                                        #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%esi
	cmpq	%rax, (%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB5_51
# BB#52:                                # %clause_LiteralIsFromAntecedent.exit88.us
                                        #   in Loop: Header=BB5_46 Depth=2
	movl	64(%rbx), %edi
	cmpl	%edi, %esi
	jl	.LBB5_54
# BB#53:                                # %clause_LiteralIsFromAntecedent.exit88.us
                                        #   in Loop: Header=BB5_46 Depth=2
	movl	68(%rbx), %ebp
	leal	-1(%rdi,%rbp), %edi
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpl	%edi, %esi
	jg	.LBB5_54
.LBB5_59:                               #   in Loop: Header=BB5_46 Depth=2
	testq	%r8, %r8
	je	.LBB5_63
# BB#60:                                # %.lr.ph.i57.us.preheader
                                        #   in Loop: Header=BB5_46 Depth=2
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB5_61:                               # %.lr.ph.i57.us
                                        #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_46 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 8(%rax)
	je	.LBB5_67
# BB#62:                                #   in Loop: Header=BB5_61 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_61
.LBB5_63:                               # %.loopexit118.us
                                        #   in Loop: Header=BB5_46 Depth=2
	movq	%rbp, %r13
	movq	%r8, %rbp
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	je	.LBB5_64
# BB#65:                                #   in Loop: Header=BB5_46 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, %r8
	jmp	.LBB5_66
.LBB5_64:                               #   in Loop: Header=BB5_46 Depth=2
	movq	%rbp, %r8
.LBB5_66:                               # %list_PointerMember.exit61.us
                                        #   in Loop: Header=BB5_46 Depth=2
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB5_67:                               # %list_PointerMember.exit61.us
                                        #   in Loop: Header=BB5_46 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB5_46
	jmp	.LBB5_70
	.p2align	4, 0x90
.LBB5_19:                               #   in Loop: Header=BB5_18 Depth=1
	movq	%rbx, %r8
	jmp	.LBB5_70
	.p2align	4, 0x90
.LBB5_21:                               #   in Loop: Header=BB5_18 Depth=1
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB5_22:                               # %.lr.ph134.split
                                        #   Parent Loop BB5_18 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_23 Depth 3
                                        #       Child Loop BB5_26 Depth 3
                                        #       Child Loop BB5_31 Depth 3
                                        #       Child Loop BB5_36 Depth 3
                                        #       Child Loop BB5_41 Depth 3
	movq	8(%r14), %rax
	movq	16(%rax), %r13
	movq	56(%r13), %rbx
	movl	$-1, %ecx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB5_23:                               #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ecx
	cmpq	%rax, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB5_23
# BB#24:                                # %clause_LiteralGetIndex.exit106
                                        #   in Loop: Header=BB5_22 Depth=2
	cmpq	%r12, %r13
	je	.LBB5_69
# BB#25:                                # %.preheader164.preheader
                                        #   in Loop: Header=BB5_22 Depth=2
	movl	$-1, %edi
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB5_26:                               # %.preheader164
                                        #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%edi
	cmpq	%rax, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB5_26
# BB#27:                                # %clause_LiteralIsFromConstraint.exit99
                                        #   in Loop: Header=BB5_22 Depth=2
	movl	64(%r13), %esi
	testl	%edi, %edi
	js	.LBB5_29
# BB#28:                                # %clause_LiteralIsFromConstraint.exit99
                                        #   in Loop: Header=BB5_22 Depth=2
	cmpl	%esi, %edi
	jl	.LBB5_39
.LBB5_29:                               #   in Loop: Header=BB5_22 Depth=2
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB5_30
.LBB5_34:                               #   in Loop: Header=BB5_22 Depth=2
	cmpl	%ebp, %r15d
	jl	.LBB5_69
# BB#35:                                # %.preheader162.preheader
                                        #   in Loop: Header=BB5_22 Depth=2
	movl	$-1, %edi
	.p2align	4, 0x90
.LBB5_36:                               # %.preheader162
                                        #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%edi
	cmpq	%rax, (%rbx)
	leaq	8(%rbx), %rbx
	jne	.LBB5_36
# BB#37:                                # %clause_LiteralIsFromSuccedent.exit75
                                        #   in Loop: Header=BB5_22 Depth=2
	addl	68(%r13), %esi
	cmpl	%esi, %edi
	jl	.LBB5_69
# BB#38:                                # %clause_LiteralIsFromSuccedent.exit75
                                        #   in Loop: Header=BB5_22 Depth=2
	movl	72(%r13), %eax
	leal	-1(%rax,%rsi), %eax
	cmpl	%eax, %edi
	jle	.LBB5_39
	jmp	.LBB5_69
.LBB5_30:                               # %.preheader163.preheader
                                        #   in Loop: Header=BB5_22 Depth=2
	movl	$-1, %edi
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB5_31:                               # %.preheader163
                                        #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%edi
	cmpq	%rax, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB5_31
# BB#32:                                # %clause_LiteralIsFromAntecedent.exit88
                                        #   in Loop: Header=BB5_22 Depth=2
	cmpl	%esi, %edi
	jl	.LBB5_34
# BB#33:                                # %clause_LiteralIsFromAntecedent.exit88
                                        #   in Loop: Header=BB5_22 Depth=2
	movl	68(%r13), %edx
	leal	-1(%rsi,%rdx), %edx
	cmpl	%edx, %edi
	jg	.LBB5_34
	.p2align	4, 0x90
.LBB5_39:                               #   in Loop: Header=BB5_22 Depth=2
	testq	%r8, %r8
	je	.LBB5_43
# BB#40:                                # %.lr.ph.i57.preheader
                                        #   in Loop: Header=BB5_22 Depth=2
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB5_41:                               # %.lr.ph.i57
                                        #   Parent Loop BB5_18 Depth=1
                                        #     Parent Loop BB5_22 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r13, 8(%rax)
	je	.LBB5_69
# BB#42:                                #   in Loop: Header=BB5_41 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_41
.LBB5_43:                               # %.loopexit118
                                        #   in Loop: Header=BB5_22 Depth=2
	movq	%r8, %rbx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movl	%r15d, %edx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	je	.LBB5_44
# BB#68:                                #   in Loop: Header=BB5_22 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %r8
	jmp	.LBB5_69
.LBB5_44:                               #   in Loop: Header=BB5_22 Depth=2
	movq	%rbx, %r8
	.p2align	4, 0x90
.LBB5_69:                               # %list_PointerMember.exit61
                                        #   in Loop: Header=BB5_22 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB5_22
.LBB5_70:                               # %._crit_edge135
                                        #   in Loop: Header=BB5_18 Depth=1
	movq	%r8, %rbx
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB5_18
	jmp	.LBB5_71
.LBB5_16:
	movq	%rbx, %r15
	xorl	%ebx, %ebx
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB5_71:                               # %._crit_edge140
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %r14d
	cmpl	%r14d, fol_EQUALITY(%rip)
	jne	.LBB5_134
# BB#72:
	movq	56(%r12), %rax
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	(%rax,%rdx,8), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB5_134
# BB#73:
	movq	16(%rcx), %rdi
	callq	list_Reverse
	movl	%r14d, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %r14
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	%r14, %rdx
	callq	st_ExistInstance
	testq	%rax, %rax
	movq	%r14, 24(%rsp)          # 8-byte Spill
	je	.LBB5_131
# BB#74:                                # %.lr.ph127
	movq	%r15, %rdx
	cmpl	40(%rsp), %edx          # 4-byte Folded Reload
	setl	%cl
	cmpl	32(%rsp), %edx          # 4-byte Folded Reload
	setg	%dl
	orb	%cl, %dl
	movb	%dl, 7(%rsp)            # 1-byte Spill
	.p2align	4, 0x90
.LBB5_75:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_100 Depth 2
                                        #       Child Loop BB5_101 Depth 3
                                        #       Child Loop BB5_105 Depth 3
                                        #       Child Loop BB5_110 Depth 3
                                        #       Child Loop BB5_116 Depth 3
                                        #       Child Loop BB5_123 Depth 3
                                        #     Child Loop BB5_78 Depth 2
                                        #       Child Loop BB5_79 Depth 3
                                        #       Child Loop BB5_83 Depth 3
                                        #       Child Loop BB5_88 Depth 3
                                        #       Child Loop BB5_93 Depth 3
	movq	%rax, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB5_76
# BB#77:                                # %.lr.ph123
                                        #   in Loop: Header=BB5_75 Depth=1
	cmpl	20(%rsp), %r15d         # 4-byte Folded Reload
	movq	%rbx, %r8
	jle	.LBB5_100
	.p2align	4, 0x90
.LBB5_78:                               # %.lr.ph123.split.us
                                        #   Parent Loop BB5_75 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_79 Depth 3
                                        #       Child Loop BB5_83 Depth 3
                                        #       Child Loop BB5_88 Depth 3
                                        #       Child Loop BB5_93 Depth 3
	movq	8(%r14), %rax
	movq	16(%rax), %rbx
	movq	56(%rbx), %rdx
	movl	$-1, %ecx
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB5_79:                               #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_78 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ecx
	cmpq	%rax, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB5_79
# BB#80:                                # %clause_LiteralGetIndex.exit.us
                                        #   in Loop: Header=BB5_78 Depth=2
	cmpq	%r12, %rbx
	je	.LBB5_99
# BB#81:                                #   in Loop: Header=BB5_78 Depth=2
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB5_82
.LBB5_86:                               #   in Loop: Header=BB5_78 Depth=2
	cmpl	%ebp, %r15d
	jl	.LBB5_99
# BB#87:                                # %.preheader.preheader
                                        #   in Loop: Header=BB5_78 Depth=2
	movl	$-1, %esi
	.p2align	4, 0x90
.LBB5_88:                               # %.preheader
                                        #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_78 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%esi
	cmpq	%rax, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB5_88
# BB#89:                                # %clause_LiteralIsFromSuccedent.exit.us
                                        #   in Loop: Header=BB5_78 Depth=2
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	cmpl	%eax, %esi
	jl	.LBB5_99
# BB#90:                                # %clause_LiteralIsFromSuccedent.exit.us
                                        #   in Loop: Header=BB5_78 Depth=2
	movl	72(%rbx), %edx
	leal	-1(%rdx,%rax), %eax
	cmpl	%eax, %esi
	jle	.LBB5_91
	jmp	.LBB5_99
	.p2align	4, 0x90
.LBB5_82:                               # %.preheader155.preheader
                                        #   in Loop: Header=BB5_78 Depth=2
	movl	$-1, %esi
	movq	%rdx, %rdi
	.p2align	4, 0x90
.LBB5_83:                               # %.preheader155
                                        #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_78 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%esi
	cmpq	%rax, (%rdi)
	leaq	8(%rdi), %rdi
	jne	.LBB5_83
# BB#84:                                # %clause_LiteralIsFromAntecedent.exit.us
                                        #   in Loop: Header=BB5_78 Depth=2
	movl	64(%rbx), %edi
	cmpl	%edi, %esi
	jl	.LBB5_86
# BB#85:                                # %clause_LiteralIsFromAntecedent.exit.us
                                        #   in Loop: Header=BB5_78 Depth=2
	movl	68(%rbx), %ebp
	leal	-1(%rdi,%rbp), %edi
	movq	8(%rsp), %rbp           # 8-byte Reload
	cmpl	%edi, %esi
	jg	.LBB5_86
.LBB5_91:                               #   in Loop: Header=BB5_78 Depth=2
	testq	%r8, %r8
	je	.LBB5_95
# BB#92:                                # %.lr.ph.i30.us.preheader
                                        #   in Loop: Header=BB5_78 Depth=2
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB5_93:                               # %.lr.ph.i30.us
                                        #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_78 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 8(%rax)
	je	.LBB5_99
# BB#94:                                #   in Loop: Header=BB5_93 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_93
.LBB5_95:                               # %.loopexit117.us
                                        #   in Loop: Header=BB5_78 Depth=2
	movq	%rbp, %r13
	movq	%r8, %rbp
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	je	.LBB5_96
# BB#97:                                #   in Loop: Header=BB5_78 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, %r8
	jmp	.LBB5_98
.LBB5_96:                               #   in Loop: Header=BB5_78 Depth=2
	movq	%rbp, %r8
.LBB5_98:                               # %list_PointerMember.exit.us
                                        #   in Loop: Header=BB5_78 Depth=2
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB5_99:                               # %list_PointerMember.exit.us
                                        #   in Loop: Header=BB5_78 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB5_78
	jmp	.LBB5_130
	.p2align	4, 0x90
.LBB5_76:                               #   in Loop: Header=BB5_75 Depth=1
	movq	%rbx, %r8
	jmp	.LBB5_130
.LBB5_118:                              #   in Loop: Header=BB5_100 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB5_129
	.p2align	4, 0x90
.LBB5_100:                              # %.lr.ph123.split
                                        #   Parent Loop BB5_75 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB5_101 Depth 3
                                        #       Child Loop BB5_105 Depth 3
                                        #       Child Loop BB5_110 Depth 3
                                        #       Child Loop BB5_116 Depth 3
                                        #       Child Loop BB5_123 Depth 3
	movq	8(%r14), %rax
	movq	16(%rax), %rbx
	movq	56(%rbx), %rbp
	movl	$-1, %ecx
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB5_101:                              #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ecx
	cmpq	%rax, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB5_101
# BB#102:                               # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	cmpq	%r12, %rbx
	je	.LBB5_103
# BB#104:                               # %.preheader158.preheader
                                        #   in Loop: Header=BB5_100 Depth=2
	movl	$-1, %edi
	movq	%rbp, %rsi
	.p2align	4, 0x90
.LBB5_105:                              # %.preheader158
                                        #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%edi
	cmpq	%rax, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB5_105
# BB#106:                               # %clause_LiteralIsFromConstraint.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	movl	64(%rbx), %esi
	testl	%edi, %edi
	js	.LBB5_108
# BB#107:                               # %clause_LiteralIsFromConstraint.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	cmpl	%esi, %edi
	jge	.LBB5_108
.LBB5_121:                              #   in Loop: Header=BB5_100 Depth=2
	testq	%r8, %r8
	movq	8(%rsp), %rbp           # 8-byte Reload
	je	.LBB5_125
# BB#122:                               # %.lr.ph.i30.preheader
                                        #   in Loop: Header=BB5_100 Depth=2
	movq	%r8, %rax
	.p2align	4, 0x90
.LBB5_123:                              # %.lr.ph.i30
                                        #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbx, 8(%rax)
	je	.LBB5_129
# BB#124:                               #   in Loop: Header=BB5_123 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB5_123
.LBB5_125:                              # %.loopexit117
                                        #   in Loop: Header=BB5_100 Depth=2
	movq	%rbp, %r13
	movq	%r8, %rbp
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%r15d, %edx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	je	.LBB5_126
# BB#127:                               #   in Loop: Header=BB5_100 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, %r8
	jmp	.LBB5_128
	.p2align	4, 0x90
.LBB5_103:                              #   in Loop: Header=BB5_100 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB5_129
	.p2align	4, 0x90
.LBB5_108:                              #   in Loop: Header=BB5_100 Depth=2
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	je	.LBB5_109
.LBB5_113:                              #   in Loop: Header=BB5_100 Depth=2
	cmpl	8(%rsp), %r15d          # 4-byte Folded Reload
	jge	.LBB5_115
# BB#114:                               #   in Loop: Header=BB5_100 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB5_129
.LBB5_109:                              # %.preheader157.preheader
                                        #   in Loop: Header=BB5_100 Depth=2
	movl	$-1, %edi
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB5_110:                              # %.preheader157
                                        #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%edi
	cmpq	%rax, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB5_110
# BB#111:                               # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	cmpl	%esi, %edi
	jl	.LBB5_113
# BB#112:                               # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	movl	68(%rbx), %edx
	leal	-1(%rsi,%rdx), %edx
	cmpl	%edx, %edi
	jle	.LBB5_121
	jmp	.LBB5_113
.LBB5_115:                              # %.preheader156.preheader
                                        #   in Loop: Header=BB5_100 Depth=2
	movl	$-1, %edi
	.p2align	4, 0x90
.LBB5_116:                              # %.preheader156
                                        #   Parent Loop BB5_75 Depth=1
                                        #     Parent Loop BB5_100 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%edi
	cmpq	%rax, (%rbp)
	leaq	8(%rbp), %rbp
	jne	.LBB5_116
# BB#117:                               # %clause_LiteralIsFromSuccedent.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	addl	68(%rbx), %esi
	cmpl	%esi, %edi
	jl	.LBB5_118
# BB#119:                               # %clause_LiteralIsFromSuccedent.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	movl	72(%rbx), %eax
	leal	-1(%rax,%rsi), %eax
	cmpl	%eax, %edi
	jle	.LBB5_121
# BB#120:                               #   in Loop: Header=BB5_100 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB5_129
.LBB5_126:                              #   in Loop: Header=BB5_100 Depth=2
	movq	%rbp, %r8
.LBB5_128:                              # %list_PointerMember.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	movq	%r13, %rbp
.LBB5_129:                              # %list_PointerMember.exit
                                        #   in Loop: Header=BB5_100 Depth=2
	movq	(%r14), %r14
	testq	%r14, %r14
	jne	.LBB5_100
.LBB5_130:                              # %._crit_edge
                                        #   in Loop: Header=BB5_75 Depth=1
	movq	%r8, %rbx
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB5_75
.LBB5_131:                              # %._crit_edge128
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB5_133
	.p2align	4, 0x90
.LBB5_132:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB5_132
.LBB5_133:                              # %list_Delete.exit
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rdi, (%rax)
.LBB5_134:
	movq	64(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 48(%rax)
	je	.LBB5_139
# BB#135:
	testq	%rbx, %rbx
	je	.LBB5_139
# BB#136:                               # %.lr.ph
	movq	%rbx, %r14
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB5_137:                              # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rbx
	movq	stdout(%rip), %rcx
	movl	$.L.str.40, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbx, %rdi
	callq	clause_Print
	movl	(%r12), %esi
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB5_137
# BB#138:
	movq	%r14, %rbx
.LBB5_139:                              # %.loopexit
	movq	%rbx, %rax
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	red_BackSubsumption, .Lfunc_end5-red_BackSubsumption
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_HandleRedundantIndexedClauses,@function
red_HandleRedundantIndexedClauses:      # @red_HandleRedundantIndexedClauses
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi66:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi68:
	.cfi_def_cfa_offset 48
.Lcfi69:
	.cfi_offset %rbx, -40
.Lcfi70:
	.cfi_offset %r12, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%rbx, %rbx
	je	.LBB6_13
# BB#1:                                 # %.lr.ph
	movq	112(%r15), %r12
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movl	12(%r14), %edx
	cmpl	12(%rsi), %edx
	ja	.LBB6_4
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpl	132(%r15), %edx
	ja	.LBB6_4
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	testb	$1, 48(%rsi)
	movl	36(%r12), %eax
	movq	%r15, %rdi
	jne	.LBB6_6
# BB#9:                                 #   in Loop: Header=BB6_2 Depth=1
	testl	%eax, %eax
	je	.LBB6_11
# BB#10:                                #   in Loop: Header=BB6_2 Depth=1
	callq	prfs_MoveUsableDocProof
	jmp	.LBB6_12
	.p2align	4, 0x90
.LBB6_4:                                #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, %rdi
	callq	split_DeleteClauseAtLevel
	jmp	.LBB6_12
	.p2align	4, 0x90
.LBB6_6:                                #   in Loop: Header=BB6_2 Depth=1
	testl	%eax, %eax
	je	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_2 Depth=1
	callq	prfs_MoveWorkedOffDocProof
	jmp	.LBB6_12
.LBB6_11:                               #   in Loop: Header=BB6_2 Depth=1
	callq	prfs_DeleteUsable
	jmp	.LBB6_12
.LBB6_8:                                #   in Loop: Header=BB6_2 Depth=1
	callq	prfs_DeleteWorkedOff
	.p2align	4, 0x90
.LBB6_12:                               #   in Loop: Header=BB6_2 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB6_2
.LBB6_13:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	red_HandleRedundantIndexedClauses, .Lfunc_end6-red_HandleRedundantIndexedClauses
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	red_BackMatchingReplacementResolution,@function
red_BackMatchingReplacementResolution:  # @red_BackMatchingReplacementResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 144
.Lcfi80:
	.cfi_offset %rbx, -56
.Lcfi81:
	.cfi_offset %r12, -48
.Lcfi82:
	.cfi_offset %r13, -40
.Lcfi83:
	.cfi_offset %r14, -32
.Lcfi84:
	.cfi_offset %r15, -24
.Lcfi85:
	.cfi_offset %rbp, -16
	movq	%r8, 56(%rsp)           # 8-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	36(%rdx), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	cmpl	$1, %eax
	jne	.LBB7_45
# BB#1:
	movq	56(%r13), %rax
	movq	(%rax), %r14
	movq	24(%r14), %rdx
	movl	(%rdx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %esi
	jne	.LBB7_3
# BB#2:
	movq	16(%rdx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB7_3:                                # %clause_LiteralAtom.exit
	cmpl	%esi, fol_EQUALITY(%rip)
	jne	.LBB7_6
# BB#4:
	cmpl	%eax, %ecx
	jne	.LBB7_5
# BB#7:                                 # %.thread
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r12), %rsi
	jmp	.LBB7_8
.LBB7_45:
	testl	%eax, %eax
	jle	.LBB7_5
# BB#46:                                # %.lr.ph287
	movslq	(%r13), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_47:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_51 Depth 2
                                        #     Child Loop BB7_56 Depth 2
                                        #       Child Loop BB7_58 Depth 3
                                        #         Child Loop BB7_64 Depth 4
                                        #     Child Loop BB7_87 Depth 2
                                        #       Child Loop BB7_88 Depth 3
                                        #       Child Loop BB7_91 Depth 3
                                        #     Child Loop BB7_73 Depth 2
                                        #       Child Loop BB7_74 Depth 3
                                        #       Child Loop BB7_77 Depth 3
                                        #     Child Loop BB7_104 Depth 2
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %r15
	movq	24(%r15), %rdx
	movl	(%rdx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %esi
	jne	.LBB7_49
# BB#48:                                #   in Loop: Header=BB7_47 Depth=1
	movq	16(%rdx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB7_49:                               # %clause_LiteralAtom.exit220
                                        #   in Loop: Header=BB7_47 Depth=1
	cmpl	%esi, fol_EQUALITY(%rip)
	je	.LBB7_105
# BB#50:                                #   in Loop: Header=BB7_47 Depth=1
	movq	16(%r15), %rsi
	movq	56(%rsi), %rsi
	movl	$-1, %r14d
	.p2align	4, 0x90
.LBB7_51:                               #   Parent Loop BB7_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incl	%r14d
	cmpq	%r15, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB7_51
# BB#52:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB7_47 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r12), %rsi
	cmpl	%eax, %ecx
	jne	.LBB7_54
# BB#53:                                #   in Loop: Header=BB7_47 Depth=1
	movq	16(%rdx), %rax
	movq	8(%rax), %rdx
.LBB7_54:                               # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB7_47 Depth=1
	callq	st_ExistInstance
	testq	%rax, %rax
	je	.LBB7_105
# BB#55:                                # %.lr.ph49.i.preheader
                                        #   in Loop: Header=BB7_47 Depth=1
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB7_56:                               # %.lr.ph49.i
                                        #   Parent Loop BB7_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_58 Depth 3
                                        #         Child Loop BB7_64 Depth 4
	movq	%rax, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB7_58
	jmp	.LBB7_68
	.p2align	4, 0x90
.LBB7_67:                               #   in Loop: Header=BB7_58 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB7_68
.LBB7_58:                               # %.lr.ph.i204
                                        #   Parent Loop BB7_47 Depth=1
                                        #     Parent Loop BB7_56 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB7_64 Depth 4
	movq	8(%rbp), %rbx
	movq	16(%rbx), %rsi
	cmpq	%r13, %rsi
	je	.LBB7_67
# BB#59:                                #   in Loop: Header=BB7_58 Depth=3
	movq	24(%r15), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%rbx), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB7_61
# BB#60:                                #   in Loop: Header=BB7_58 Depth=3
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB7_63
.LBB7_61:                               # %clause_LiteralsAreComplementary.exit.i
                                        #   in Loop: Header=BB7_58 Depth=3
	cmpl	%ecx, %eax
	je	.LBB7_67
# BB#62:                                # %clause_LiteralsAreComplementary.exit.i
                                        #   in Loop: Header=BB7_58 Depth=3
	cmpl	%edx, %eax
	jne	.LBB7_67
.LBB7_63:                               # %clause_LiteralsAreComplementary.exit.thread.i
                                        #   in Loop: Header=BB7_58 Depth=3
	movq	56(%rsi), %rax
	movl	$-1, %ecx
	.p2align	4, 0x90
.LBB7_64:                               #   Parent Loop BB7_47 Depth=1
                                        #     Parent Loop BB7_56 Depth=2
                                        #       Parent Loop BB7_58 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	incl	%ecx
	cmpq	%rbx, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB7_64
# BB#65:                                # %clause_LiteralGetIndex.exit42.i
                                        #   in Loop: Header=BB7_58 Depth=3
	movq	%r13, %rdi
	movl	%r14d, %edx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	je	.LBB7_67
# BB#66:                                #   in Loop: Header=BB7_58 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB7_67
	.p2align	4, 0x90
.LBB7_68:                               # %._crit_edge.i
                                        #   in Loop: Header=BB7_56 Depth=2
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB7_56
# BB#69:                                # %red_GetBackMRResLits.exit
                                        #   in Loop: Header=BB7_47 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB7_70
# BB#71:                                # %.lr.ph278
                                        #   in Loop: Header=BB7_47 Depth=1
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB7_72
	.p2align	4, 0x90
.LBB7_87:                               # %.lr.ph278.split
                                        #   Parent Loop BB7_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_88 Depth 3
                                        #       Child Loop BB7_91 Depth 3
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	8(%rax), %rax
	movq	16(%rax), %r15
	movq	56(%r15), %rcx
	movl	$-1, %r12d
	movabsq	$-4294967296, %rbp      # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %rdx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB7_88:                               #   Parent Loop BB7_47 Depth=1
                                        #     Parent Loop BB7_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	%rdx, %rbp
	incl	%r12d
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB7_88
# BB#89:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB7_87 Depth=2
	movq	%r15, %rdi
	callq	clause_Copy
	movq	%rax, %r14
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB7_93
# BB#90:                                # %.lr.ph.i197.preheader
                                        #   in Loop: Header=BB7_87 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	.p2align	4, 0x90
.LBB7_91:                               # %.lr.ph.i197
                                        #   Parent Loop BB7_47 Depth=1
                                        #     Parent Loop BB7_87 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r15, 8(%rax)
	je	.LBB7_94
# BB#92:                                #   in Loop: Header=BB7_91 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_91
.LBB7_93:                               # %.loopexit244
                                        #   in Loop: Header=BB7_87 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB7_96
	.p2align	4, 0x90
.LBB7_94:                               # %list_PointerMember.exit
                                        #   in Loop: Header=BB7_87 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 36(%rax)
	jne	.LBB7_96
# BB#95:                                #   in Loop: Header=BB7_87 Depth=2
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r14)
	.p2align	4, 0x90
.LBB7_96:                               #   in Loop: Header=BB7_87 Depth=2
	movl	48(%r14), %eax
	testb	$1, %al
	je	.LBB7_98
# BB#97:                                #   in Loop: Header=BB7_87 Depth=2
	decl	%eax
	movl	%eax, 48(%r14)
.LBB7_98:                               # %clause_RemoveFlag.exit
                                        #   in Loop: Header=BB7_87 Depth=2
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	%r14, %rdi
	movl	%r12d, %esi
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%r12, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiteral
	sarq	$32, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rbp)
	movq	$0, (%rbp)
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	$0, (%rax)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	%rax, %rcx
	callq	red_DocumentMatchingReplacementResolution
	cmpl	$0, 80(%r12)
	je	.LBB7_100
# BB#99:                                #   in Loop: Header=BB7_87 Depth=2
	movq	stdout(%rip), %rcx
	movl	$.L.str.42, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r15, %rdi
	callq	clause_Print
	movl	(%r13), %esi
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movq	%r14, %rdi
	callq	clause_Print
.LBB7_100:                              #   in Loop: Header=BB7_87 Depth=2
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%rbp)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_87
	jmp	.LBB7_101
.LBB7_72:                               # %.lr.ph278.split.us.preheader
                                        #   in Loop: Header=BB7_47 Depth=1
	movq	%rax, %r15
	.p2align	4, 0x90
.LBB7_73:                               # %.lr.ph278.split.us
                                        #   Parent Loop BB7_47 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_74 Depth 3
                                        #       Child Loop BB7_77 Depth 3
	movq	8(%r15), %rax
	movq	16(%rax), %r12
	movq	56(%r12), %rcx
	movl	$-1, %ebp
	.p2align	4, 0x90
.LBB7_74:                               #   Parent Loop BB7_47 Depth=1
                                        #     Parent Loop BB7_73 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ebp
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB7_74
# BB#75:                                # %clause_LiteralGetIndex.exit.us
                                        #   in Loop: Header=BB7_73 Depth=2
	movq	%r12, %rdi
	callq	clause_Copy
	movq	%rax, %r14
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB7_79
# BB#76:                                # %.lr.ph.i197.us.preheader
                                        #   in Loop: Header=BB7_73 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	.p2align	4, 0x90
.LBB7_77:                               # %.lr.ph.i197.us
                                        #   Parent Loop BB7_47 Depth=1
                                        #     Parent Loop BB7_73 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r12, 8(%rax)
	je	.LBB7_80
# BB#78:                                #   in Loop: Header=BB7_77 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_77
.LBB7_79:                               # %.loopexit244.us
                                        #   in Loop: Header=BB7_73 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB7_82
	.p2align	4, 0x90
.LBB7_80:                               # %list_PointerMember.exit.us
                                        #   in Loop: Header=BB7_73 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 36(%rax)
	jne	.LBB7_82
# BB#81:                                #   in Loop: Header=BB7_73 Depth=2
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r14)
	.p2align	4, 0x90
.LBB7_82:                               #   in Loop: Header=BB7_73 Depth=2
	movl	48(%r14), %eax
	testb	$1, %al
	je	.LBB7_84
# BB#83:                                #   in Loop: Header=BB7_73 Depth=2
	decl	%eax
	movl	%eax, 48(%r14)
.LBB7_84:                               # %clause_RemoveFlag.exit.us
                                        #   in Loop: Header=BB7_73 Depth=2
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	%r14, %rdi
	movl	%ebp, %esi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiteral
	cmpl	$0, 80(%rbx)
	je	.LBB7_86
# BB#85:                                #   in Loop: Header=BB7_73 Depth=2
	movq	stdout(%rip), %rcx
	movl	$.L.str.42, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
	movl	(%r13), %esi
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movq	%r14, %rdi
	callq	clause_Print
.LBB7_86:                               #   in Loop: Header=BB7_73 Depth=2
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, (%rbx)
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB7_73
.LBB7_101:                              # %._crit_edge279
                                        #   in Loop: Header=BB7_47 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB7_102
# BB#103:                               # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB7_47 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_104:                              # %.lr.ph.i
                                        #   Parent Loop BB7_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rsi
	jne	.LBB7_104
	jmp	.LBB7_105
.LBB7_102:                              #   in Loop: Header=BB7_47 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
.LBB7_70:                               #   in Loop: Header=BB7_47 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB7_105:                              # %list_Delete.exit
                                        #   in Loop: Header=BB7_47 Depth=1
	incq	%rbp
	cmpq	%rbx, %rbp
	jne	.LBB7_47
	jmp	.LBB7_106
.LBB7_6:
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r12), %rsi
	cmpl	%eax, %ecx
	jne	.LBB7_9
.LBB7_8:
	movq	16(%rdx), %rax
	movq	8(%rax), %rdx
.LBB7_9:                                # %clause_LiteralAtom.exit212
	callq	st_ExistInstance
	testq	%rax, %rax
	je	.LBB7_5
# BB#10:                                # %.lr.ph274.preheader
	movslq	(%r13), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB7_11:                               # %.lr.ph274
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_13 Depth 2
	movq	%rax, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB7_13
	jmp	.LBB7_19
	.p2align	4, 0x90
.LBB7_18:                               #   in Loop: Header=BB7_13 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB7_19
.LBB7_13:                               # %.lr.ph269
                                        #   Parent Loop BB7_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rbx
	movq	24(%r14), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%rbx), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB7_15
# BB#14:                                #   in Loop: Header=BB7_13 Depth=2
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB7_17
.LBB7_15:                               # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB7_13 Depth=2
	cmpl	%ecx, %eax
	je	.LBB7_18
# BB#16:                                # %clause_LiteralsAreComplementary.exit
                                        #   in Loop: Header=BB7_13 Depth=2
	cmpl	%edx, %eax
	jne	.LBB7_18
.LBB7_17:                               # %clause_LiteralsAreComplementary.exit.thread
                                        #   in Loop: Header=BB7_13 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
	jmp	.LBB7_18
	.p2align	4, 0x90
.LBB7_19:                               # %._crit_edge270
                                        #   in Loop: Header=BB7_11 Depth=1
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB7_11
# BB#20:                                # %.preheader243
	testq	%r15, %r15
	je	.LBB7_5
# BB#21:                                # %.lr.ph264
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB7_22:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_28 Depth 2
                                        #     Child Loop BB7_35 Depth 2
                                        #       Child Loop BB7_36 Depth 3
                                        #     Child Loop BB7_41 Depth 2
	movq	8(%r15), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%rbx, 8(%rbp)
	movq	$0, (%rbp)
	movq	16(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rax, %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%r12, (%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 80(%rax)
	je	.LBB7_24
# BB#23:                                #   in Loop: Header=BB7_22 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.42, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	clause_Print
	movl	(%r13), %esi
	movl	$.L.str.43, %edi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
.LBB7_24:                               # %.preheader
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	(%r15), %rax
	testq	%rax, %rax
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	je	.LBB7_25
# BB#26:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	%r15, %rsi
	.p2align	4, 0x90
.LBB7_28:                               #   Parent Loop BB7_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rax, %rbp
	movq	8(%rbp), %rax
	cmpq	16(%rax), %r14
	jne	.LBB7_27
# BB#29:                                #   in Loop: Header=BB7_28 Depth=2
	movq	(%rbp), %rax
	movq	%rax, (%rsi)
	movq	%rcx, (%rbp)
	movq	(%rdx), %rax
	testq	%rax, %rax
	movq	%rbp, %rcx
	jne	.LBB7_28
	jmp	.LBB7_30
.LBB7_27:                               # %.outer.loopexit
                                        #   in Loop: Header=BB7_28 Depth=2
	movq	(%rbp), %rax
	testq	%rax, %rax
	movq	%rbp, %rdx
	movq	%rbp, %rsi
	movq	%rcx, %rbp
	jne	.LBB7_28
	.p2align	4, 0x90
.LBB7_30:                               # %.outer._crit_edgethread-pre-split
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	(%r15), %r12
	jmp	.LBB7_31
	.p2align	4, 0x90
.LBB7_25:                               #   in Loop: Header=BB7_22 Depth=1
	xorl	%r12d, %r12d
.LBB7_31:                               # %.outer._crit_edge
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%r15, (%rax)
	movq	%r14, %rdi
	callq	clause_Copy
	movq	%rax, %r14
	movl	48(%r14), %eax
	testb	$1, %al
	je	.LBB7_33
# BB#32:                                #   in Loop: Header=BB7_22 Depth=1
	decl	%eax
	movl	%eax, 48(%r14)
.LBB7_33:                               # %clause_RemoveFlag.exit235
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	clause_UpdateSplitDataFromPartner
	testq	%rbp, %rbp
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	je	.LBB7_38
# BB#34:                                # %.lr.ph260.preheader
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB7_35:                               # %.lr.ph260
                                        #   Parent Loop BB7_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB7_36 Depth 3
	movq	8(%rax), %rcx
	movq	16(%rcx), %rdx
	movq	56(%rdx), %rsi
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB7_36:                               #   Parent Loop BB7_22 Depth=1
                                        #     Parent Loop BB7_35 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	%rdi, %rdx
	cmpq	%rcx, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB7_36
# BB#37:                                # %clause_LiteralGetIndex.exit234
                                        #   in Loop: Header=BB7_35 Depth=2
	sarq	$32, %rdx
	movq	%rdx, 8(%rax)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB7_35
.LBB7_38:                               # %._crit_edge261
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiterals
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB7_40
# BB#39:                                #   in Loop: Header=BB7_22 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%r15)
	movq	$0, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rax)
	movq	%r14, %rdi
	movq	%rbp, %rsi
	movq	%r15, %rdx
	movq	%rax, %rcx
	callq	red_DocumentMatchingReplacementResolution
	jmp	.LBB7_42
	.p2align	4, 0x90
.LBB7_40:                               #   in Loop: Header=BB7_22 Depth=1
	testq	%rbp, %rbp
	je	.LBB7_42
	.p2align	4, 0x90
.LBB7_41:                               # %.lr.ph.i224
                                        #   Parent Loop BB7_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB7_41
.LBB7_42:                               # %list_Delete.exit226
                                        #   in Loop: Header=BB7_22 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 80(%rax)
	je	.LBB7_44
# BB#43:                                #   in Loop: Header=BB7_22 Depth=1
	movq	%r14, %rdi
	callq	clause_Print
.LBB7_44:                               #   in Loop: Header=BB7_22 Depth=1
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, (%rbx)
	testq	%r12, %r12
	movq	%r12, %rax
	movq	(%rsp), %r12            # 8-byte Reload
	movq	%rax, %r15
	jne	.LBB7_22
	jmp	.LBB7_106
.LBB7_5:
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB7_106:                              # %.loopexit
	movq	(%rsp), %rax            # 8-byte Reload
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	red_BackMatchingReplacementResolution, .Lfunc_end7-red_BackMatchingReplacementResolution
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_BackRewriting,@function
red_BackRewriting:                      # @red_BackRewriting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi86:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi87:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi88:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi89:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi90:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 144
.Lcfi93:
	.cfi_offset %rbx, -56
.Lcfi94:
	.cfi_offset %r12, -48
.Lcfi95:
	.cfi_offset %r13, -40
.Lcfi96:
	.cfi_offset %r14, -32
.Lcfi97:
	.cfi_offset %r15, -24
.Lcfi98:
	.cfi_offset %rbp, -16
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movl	72(%r15), %eax
	testl	%eax, %eax
	jle	.LBB8_1
# BB#2:                                 # %.lr.ph
	movslq	68(%r15), %rcx
	movslq	64(%r15), %rdx
	addq	%rcx, %rdx
	addl	%edx, %eax
	cltq
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movabsq	$4294967296, %r12       # imm = 0x100000000
	xorl	%ecx, %ecx
	movq	%r15, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_8 Depth 2
                                        #       Child Loop BB8_13 Depth 3
                                        #         Child Loop BB8_14 Depth 4
                                        #         Child Loop BB8_19 Depth 4
                                        #     Child Loop BB8_38 Depth 2
	movq	56(%r15), %rax
	movq	(%rax,%rdx,8), %rax
	cmpl	$0, 8(%rax)
	je	.LBB8_4
# BB#5:                                 #   in Loop: Header=BB8_3 Depth=1
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	callq	st_ExistInstance
	testq	%rax, %rax
	je	.LBB8_6
# BB#7:                                 # %.lr.ph10.i.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph10.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_13 Depth 3
                                        #         Child Loop BB8_14 Depth 4
                                        #         Child Loop BB8_19 Depth 4
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	jg	.LBB8_33
# BB#9:                                 #   in Loop: Header=BB8_8 Depth=2
	jns	.LBB8_11
# BB#10:                                # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB8_8 Depth=2
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB8_33
.LBB8_11:                               # %symbol_IsPredicate.exit.thread.i
                                        #   in Loop: Header=BB8_8 Depth=2
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	sharing_GetDataList
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB8_33
# BB#12:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB8_8 Depth=2
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph.i
                                        #   Parent Loop BB8_3 Depth=1
                                        #     Parent Loop BB8_8 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB8_14 Depth 4
                                        #         Child Loop BB8_19 Depth 4
	movq	8(%r14), %rax
	movq	16(%rax), %rbp
	movq	56(%rbp), %rcx
	movl	$-1, %r13d
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB8_14:                               #   Parent Loop BB8_3 Depth=1
                                        #     Parent Loop BB8_8 Depth=2
                                        #       Parent Loop BB8_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	addq	%r12, %rbx
	incl	%r13d
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB8_14
# BB#15:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB8_13 Depth=3
	movl	(%r15), %eax
	cmpl	(%rbp), %eax
	je	.LBB8_32
# BB#16:                                #   in Loop: Header=BB8_13 Depth=3
	cmpl	64(%rbp), %r13d
	jl	.LBB8_32
# BB#17:                                #   in Loop: Header=BB8_13 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	testq	%rax, %rax
	jne	.LBB8_19
	jmp	.LBB8_21
	.p2align	4, 0x90
.LBB8_20:                               #   in Loop: Header=BB8_19 Depth=4
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB8_21
.LBB8_19:                               # %.lr.ph.i.i
                                        #   Parent Loop BB8_3 Depth=1
                                        #     Parent Loop BB8_8 Depth=2
                                        #       Parent Loop BB8_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	cmpq	%rbp, 8(%rax)
	jne	.LBB8_20
	jmp	.LBB8_32
.LBB8_21:                               # %.loopexit.i
                                        #   in Loop: Header=BB8_13 Depth=3
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r13d, %ecx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	je	.LBB8_22
# BB#23:                                #   in Loop: Header=BB8_13 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rcx
	movq	%rbp, 8(%rcx)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	%rax, (%rcx)
	movq	%rbp, %rdi
	callq	clause_Copy
	movq	%rax, %r15
	movl	48(%r15), %eax
	testb	$1, %al
	je	.LBB8_25
# BB#24:                                #   in Loop: Header=BB8_13 Depth=3
	decl	%eax
	movl	%eax, 48(%r15)
.LBB8_25:                               # %clause_RemoveFlag.exit.i
                                        #   in Loop: Header=BB8_13 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 36(%rax)
	movq	40(%rsp), %rbp          # 8-byte Reload
	je	.LBB8_27
# BB#26:                                #   in Loop: Header=BB8_13 Depth=3
	movq	%r15, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movq	(%rsp), %rcx            # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	red_DocumentRewriting
.LBB8_27:                               #   in Loop: Header=BB8_13 Depth=3
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 52(%rax)
	movq	%rbp, %r13
	movq	(%rsp), %rbp            # 8-byte Reload
	je	.LBB8_29
# BB#28:                                #   in Loop: Header=BB8_13 Depth=3
	movq	stdout(%rip), %rcx
	movl	$.L.str.44, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r15, %rdi
	callq	clause_Print
	movl	(%r13), %esi
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	printf
.LBB8_29:                               #   in Loop: Header=BB8_13 Depth=3
	movq	56(%r15), %rax
	sarq	$29, %rbx
	movq	%r13, %rcx
	movq	(%rax,%rbx), %r13
	movq	cont_LEFTCONTEXT(%rip), %rbx
	movq	56(%rcx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	cont_ApplyBindingsModuloMatchingReverse
	movq	%rax, %rbx
	movq	24(%r13), %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	term_ReplaceSubtermBy
	movq	%rbx, %rdi
	callq	term_Delete
	movq	%r15, %rdi
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	callq	clause_OrientEqualities
	movq	%r15, %rdi
	callq	clause_Normalize
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	callq	clause_SetMaxLitFlags
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r15)
	movq	%r15, %rdi
	callq	clause_UpdateMaxVar
	movq	%r15, %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rsi
	callq	clause_UpdateSplitDataFromPartner
	cmpl	$0, 52(%rbx)
	je	.LBB8_31
# BB#30:                                #   in Loop: Header=BB8_13 Depth=3
	movq	%r15, %rdi
	callq	clause_Print
.LBB8_31:                               # %red_ApplyRewriting.exit.i
                                        #   in Loop: Header=BB8_13 Depth=3
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx), %r13
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, (%rbx)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	movq	%rbp, %r15
	jmp	.LBB8_32
.LBB8_22:                               #   in Loop: Header=BB8_13 Depth=3
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB8_32:                               # %list_PointerMember.exit.i
                                        #   in Loop: Header=BB8_13 Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB8_13
	.p2align	4, 0x90
.LBB8_33:                               # %.loopexit2.i
                                        #   in Loop: Header=BB8_8 Depth=2
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB8_8
# BB#34:                                # %red_LiteralRewriting.exit
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	testq	%rdi, %rdi
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	je	.LBB8_35
# BB#36:                                #   in Loop: Header=BB8_3 Depth=1
	testq	%rsi, %rsi
	je	.LBB8_40
# BB#37:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB8_38:                               # %.preheader.i
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB8_38
# BB#39:                                #   in Loop: Header=BB8_3 Depth=1
	movq	%rsi, (%rax)
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB8_40
	.p2align	4, 0x90
.LBB8_4:                                #   in Loop: Header=BB8_3 Depth=1
	movq	%rcx, %rdi
	jmp	.LBB8_40
	.p2align	4, 0x90
.LBB8_6:                                #   in Loop: Header=BB8_3 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rsp), %rdx            # 8-byte Reload
	jmp	.LBB8_40
.LBB8_35:                               #   in Loop: Header=BB8_3 Depth=1
	movq	%rsi, %rdi
	.p2align	4, 0x90
.LBB8_40:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB8_3 Depth=1
	incq	%rdx
	cmpq	48(%rsp), %rdx          # 8-byte Folded Reload
	movq	%rdi, %rcx
	jl	.LBB8_3
	jmp	.LBB8_41
.LBB8_1:
	xorl	%edi, %edi
.LBB8_41:                               # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	list_PointerDeleteDuplicates # TAILCALL
.Lfunc_end8:
	.size	red_BackRewriting, .Lfunc_end8-red_BackRewriting
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_BackContextualRewriting,@function
red_BackContextualRewriting:            # @red_BackContextualRewriting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi103:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi105:
	.cfi_def_cfa_offset 176
.Lcfi106:
	.cfi_offset %rbx, -56
.Lcfi107:
	.cfi_offset %r12, -48
.Lcfi108:
	.cfi_offset %r13, -40
.Lcfi109:
	.cfi_offset %r14, -32
.Lcfi110:
	.cfi_offset %r15, -24
.Lcfi111:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rsi, %rbx
	movl	68(%rbx), %esi
	movl	72(%rbx), %eax
	addl	64(%rbx), %esi
	leal	-1(%rax,%rsi), %eax
	cmpl	%eax, %esi
	jle	.LBB9_2
.LBB9_1:
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB9_45
.LBB9_2:                                # %.lr.ph
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	104(%rdi), %r14
	movq	112(%rdi), %r12
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movl	%edx, %ecx
	orl	$1, %ecx
	leaq	48(%rdi), %rbp
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	leaq	32(%rdi), %rdx
	cmpl	$3, %ecx
	cmoveq	%rdx, %rbp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movslq	%esi, %r15
	movslq	%eax, %r13
	decq	%r15
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB9_3:                                # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rax
	movq	8(%rax,%r15,8), %rbp
	cmpl	$0, 8(%rbp)
	je	.LBB9_44
# BB#4:                                 #   in Loop: Header=BB9_3 Depth=1
	testb	$2, (%rbp)
	je	.LBB9_44
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=1
	movq	%rbx, %rdi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	red_LeftTermOfEquationIsStrictlyMaximalTerm
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB9_6
.LBB9_44:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB9_3 Depth=1
	incl	%esi
	incq	%r15
	cmpq	%r13, %r15
	jl	.LBB9_3
.LBB9_45:                               # %.critedge
	movq	(%rsp), %rdi            # 8-byte Reload
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	list_PointerDeleteDuplicates # TAILCALL
.LBB9_6:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	104(%rax), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	112(%rax), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rcx
	movq	24(%rbp), %rax
	movq	16(%rax), %rax
	movq	8(%rax), %rdx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	(%rcx), %rsi
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	callq	st_GetInstance
	testq	%rax, %rax
	je	.LBB9_1
# BB#7:                                 # %.lr.ph135.i
	movabsq	$4294967296, %r13       # imm = 0x100000000
	xorl	%ecx, %ecx
	movq	%rcx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB9_8:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_13 Depth 2
                                        #       Child Loop BB9_14 Depth 3
                                        #       Child Loop BB9_19 Depth 3
                                        #       Child Loop BB9_26 Depth 3
	movq	8(%rax), %rdi
	movl	(%rdi), %ecx
	testl	%ecx, %ecx
	movq	%rax, 112(%rsp)         # 8-byte Spill
	jg	.LBB9_43
# BB#9:                                 #   in Loop: Header=BB9_8 Depth=1
	jns	.LBB9_11
# BB#10:                                # %symbol_IsPredicate.exit.i
                                        #   in Loop: Header=BB9_8 Depth=1
	negl	%ecx
	andl	symbol_TYPEMASK(%rip), %ecx
	cmpl	$2, %ecx
	je	.LBB9_43
.LBB9_11:                               # %symbol_IsPredicate.exit.thread.i
                                        #   in Loop: Header=BB9_8 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	sharing_GetDataList
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB9_43
# BB#12:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB9_8 Depth=1
	movq	8(%rsp), %r8            # 8-byte Reload
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB9_13:                               # %.lr.ph.i
                                        #   Parent Loop BB9_8 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_14 Depth 3
                                        #       Child Loop BB9_19 Depth 3
                                        #       Child Loop BB9_26 Depth 3
	movq	8(%r12), %rax
	movq	16(%rax), %rbp
	movq	56(%rbp), %rcx
	movl	$-1, %r14d
	movq	%rsi, %rbx
	.p2align	4, 0x90
.LBB9_14:                               #   Parent Loop BB9_8 Depth=1
                                        #     Parent Loop BB9_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addq	%r13, %rbx
	incl	%r14d
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB9_14
# BB#15:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	$0, 48(%rsp)
	movl	(%r8), %eax
	cmpl	(%rbp), %eax
	je	.LBB9_42
# BB#16:                                #   in Loop: Header=BB9_13 Depth=2
	cmpl	64(%rbp), %r14d
	jl	.LBB9_42
# BB#17:                                #   in Loop: Header=BB9_13 Depth=2
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB9_21
# BB#18:                                # %.lr.ph.i121.i.preheader
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	.p2align	4, 0x90
.LBB9_19:                               # %.lr.ph.i121.i
                                        #   Parent Loop BB9_8 Depth=1
                                        #     Parent Loop BB9_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rbp, 8(%rax)
	je	.LBB9_42
# BB#20:                                #   in Loop: Header=BB9_19 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB9_19
.LBB9_21:                               # %.loopexit.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%rbp, %rsi
	movl	%r14d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	leaq	48(%rsp), %rax
	pushq	%rax
.Lcfi112:
	.cfi_adjust_cfa_offset 8
	pushq	96(%rsp)                # 8-byte Folded Reload
.Lcfi113:
	.cfi_adjust_cfa_offset 8
	callq	red_CRwTautologyCheck
	addq	$16, %rsp
.Lcfi114:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	je	.LBB9_41
# BB#22:                                #   in Loop: Header=BB9_13 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rbp, %rdi
	callq	clause_Copy
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	48(%rax), %eax
	testb	$1, %al
	je	.LBB9_24
# BB#23:                                #   in Loop: Header=BB9_13 Depth=2
	decl	%eax
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%eax, 48(%rcx)
.LBB9_24:                               # %clause_RemoveFlag.exit.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	64(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	callq	unify_MatchBindings
	movq	cont_LEFTCONTEXT(%rip), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	8(%rax,%r15,8), %rax
	movq	24(%rax), %rax
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movl	$1, %edx
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	cont_ApplyBindingsModuloMatching
	movq	%rax, %r8
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	xorps	%xmm0, %xmm0
	jle	.LBB9_27
# BB#25:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB9_13 Depth=2
	incl	%eax
	.p2align	4, 0x90
.LBB9_26:                               # %.lr.ph.i.i
                                        #   Parent Loop BB9_8 Depth=1
                                        #     Parent Loop BB9_13 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB9_26
.LBB9_27:                               # %._crit_edge.i.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB9_29
# BB#28:                                #   in Loop: Header=BB9_13 Depth=2
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB9_29:                               # %cont_BackTrack.exit.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	movq	56(%rax), %rax
	sarq	$29, %rbx
	movq	(%rax,%rbx), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	movq	16(%rsp), %rsi          # 8-byte Reload
	jne	.LBB9_31
# BB#30:                                #   in Loop: Header=BB9_13 Depth=2
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB9_31:                               # %clause_GetLiteralAtom.exit.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	%r8, %rdx
	movq	%r8, %rbx
	callq	term_ReplaceSubtermBy
	movq	%rbx, %rdi
	callq	term_Delete
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 36(%rax)
	movq	48(%rsp), %rbx
	je	.LBB9_36
# BB#32:                                #   in Loop: Header=BB9_13 Depth=2
	testq	%rbx, %rbx
	xorps	%xmm0, %xmm0
	je	.LBB9_33
# BB#34:                                #   in Loop: Header=BB9_13 Depth=2
	movq	32(%rbx), %r8
	movq	40(%rbx), %r9
	movups	%xmm0, 32(%rbx)
	jmp	.LBB9_35
.LBB9_33:                               #   in Loop: Header=BB9_13 Depth=2
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
.LBB9_35:                               #   in Loop: Header=BB9_13 Depth=2
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	%r14d, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	red_DocumentContextualRewriting
.LBB9_36:                               # %clause_GetLiteralAtom.exit._crit_edge.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	(%rsp), %r14            # 8-byte Reload
	movq	%r14, %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	clause_UpdateSplitDataFromPartner
	testq	%rbx, %rbx
	je	.LBB9_38
# BB#37:                                #   in Loop: Header=BB9_13 Depth=2
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	%rbx, %rdi
	callq	clause_Delete
.LBB9_38:                               #   in Loop: Header=BB9_13 Depth=2
	movq	%r14, %rdi
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdx
	callq	clause_OrientEqualities
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	clause_Normalize
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	(%rsp), %r14            # 8-byte Reload
	callq	clause_SetMaxLitFlags
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r14)
	movq	%r14, %rdi
	callq	clause_UpdateMaxVar
	cmpl	$0, 56(%rbx)
	je	.LBB9_40
# BB#39:                                #   in Loop: Header=BB9_13 Depth=2
	movq	stdout(%rip), %rcx
	movl	$.L.str.45, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbp, %rdi
	callq	clause_Print
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	(%rax), %esi
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movq	24(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	printf
	movq	%r14, %rdi
	callq	clause_Print
.LBB9_40:                               #   in Loop: Header=BB9_13 Depth=2
	movq	80(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, (%rbp)
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%rax, (%rsp)            # 8-byte Spill
.LBB9_41:                               # %list_PointerMember.exit.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	8(%rsp), %r8            # 8-byte Reload
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB9_42:                               # %list_PointerMember.exit.i
                                        #   in Loop: Header=BB9_13 Depth=2
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB9_13
	.p2align	4, 0x90
.LBB9_43:                               # %.loopexit129.i
                                        #   in Loop: Header=BB9_8 Depth=1
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	jne	.LBB9_8
	jmp	.LBB9_45
.Lfunc_end9:
	.size	red_BackContextualRewriting, .Lfunc_end9-red_BackContextualRewriting
	.cfi_endproc

	.globl	red_CompleteReductionOnDerivedClauses
	.p2align	4, 0x90
	.type	red_CompleteReductionOnDerivedClauses,@function
red_CompleteReductionOnDerivedClauses:  # @red_CompleteReductionOnDerivedClauses
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi117:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi118:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi119:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi120:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi121:
	.cfi_def_cfa_offset 96
.Lcfi122:
	.cfi_offset %rbx, -56
.Lcfi123:
	.cfi_offset %r12, -48
.Lcfi124:
	.cfi_offset %r13, -40
.Lcfi125:
	.cfi_offset %r14, -32
.Lcfi126:
	.cfi_offset %r15, -24
.Lcfi127:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movl	%r8d, 8(%rsp)           # 4-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, %r15d
	movq	%rdi, %rbx
	movq	$0, 16(%rsp)
	movq	%rsi, %rdi
	callq	clause_ListSortWeighed
	movq	112(%rbx), %rcx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	cmpl	$-1, 12(%rsp)           # 4-byte Folded Reload
	jne	.LBB10_3
	jmp	.LBB10_35
	.p2align	4, 0x90
.LBB10_1:                               # %.loopexit
	movq	%r12, %rdi
	leaq	16(%rsp), %rsi
	callq	split_ExtractEmptyClauses
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	prfs_InsertUsableClause
	movl	$clause_Weight, %esi
	movq	%rbp, %rdi
	callq	list_NumberSort
	movl	$clause_Weight, %edx
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	list_NNumberMerge
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB10_2:                               # %.thread78.outer
	cmpl	$-1, 12(%rsp)           # 4-byte Folded Reload
	je	.LBB10_35
.LBB10_3:                               # %.thread78.outer.split.us
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB10_12
	.p2align	4, 0x90
.LBB10_4:                               # %.thread78.us.us
                                        # =>This Inner Loop Header: Depth=1
	testq	%rax, %rax
	je	.LBB10_39
# BB#5:                                 #   in Loop: Header=BB10_4 Depth=1
	movq	(%rax), %r13
	movq	8(%rax), %r14
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	cmpq	$0, 120(%rbx)
	jne	.LBB10_7
# BB#6:                                 #   in Loop: Header=BB10_4 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	red_CompleteReductionOnDerivedClause
	movq	%rax, %r14
.LBB10_7:                               #   in Loop: Header=BB10_4 Depth=1
	testq	%r14, %r14
	movq	%r13, %rax
	je	.LBB10_4
# BB#8:                                 # %.us-lcssa88.us
	movl	76(%r14), %eax
	addl	$-15, %eax
	cmpl	$2, %eax
	jb	.LBB10_23
# BB#9:
	movl	8(%rsp), %eax           # 4-byte Reload
	cmpl	$2, %eax
	je	.LBB10_16
# BB#10:
	cmpl	$1, %eax
	jne	.LBB10_40
# BB#11:
	movl	4(%r14), %ebp
	cmpl	12(%rsp), %ebp          # 4-byte Folded Reload
	jbe	.LBB10_23
	jmp	.LBB10_17
	.p2align	4, 0x90
.LBB10_12:                              # %.thread78.us
                                        # =>This Inner Loop Header: Depth=1
	testq	%rax, %rax
	je	.LBB10_39
# BB#13:                                #   in Loop: Header=BB10_12 Depth=1
	movq	(%rax), %r13
	movq	8(%rax), %r14
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	cmpq	$0, 120(%rbx)
	jne	.LBB10_15
# BB#14:                                #   in Loop: Header=BB10_12 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	red_CompleteReductionOnDerivedClause
	movq	%rax, %r14
.LBB10_15:                              #   in Loop: Header=BB10_12 Depth=1
	testq	%r14, %r14
	movq	%r13, %rax
	je	.LBB10_12
	jmp	.LBB10_23
.LBB10_16:
	movq	%r14, %rdi
	callq	clause_ComputeTermDepth
	movl	%eax, %ebp
	cmpl	12(%rsp), %ebp          # 4-byte Folded Reload
	jbe	.LBB10_23
.LBB10_17:
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 140(%rax)
	je	.LBB10_19
# BB#18:
	movq	stdout(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$19, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	clause_Print
.LBB10_19:
	movq	%r14, %rdi
	callq	clause_Delete
	movl	(%r12), %eax
	cmpl	$-1, %eax
	je	.LBB10_21
# BB#20:
	cmpl	%eax, %ebp
	movq	%r13, %rax
	jae	.LBB10_2
.LBB10_21:
	movl	%ebp, (%r12)
	jmp	.LBB10_22
	.p2align	4, 0x90
.LBB10_23:                              # %.thread
	cmpq	$0, 120(%rbx)
	je	.LBB10_25
# BB#24:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	red_CompleteReductionOnDerivedClause
	movq	%rax, %r14
	testq	%r14, %r14
	movq	%r13, %rax
	je	.LBB10_2
.LBB10_25:                              # %.thread79
	incl	140(%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 104(%rax)
	je	.LBB10_27
# BB#26:
	movq	stdout(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	clause_Print
.LBB10_27:
	cmpl	$0, 68(%r14)
	jne	.LBB10_30
# BB#28:
	cmpl	$0, 72(%r14)
	jne	.LBB10_30
# BB#29:                                # %clause_IsEmptyClause.exit
	cmpl	$0, 64(%r14)
	je	.LBB10_34
	.p2align	4, 0x90
.LBB10_30:                              # %clause_IsEmptyClause.exit.thread
	movq	%r12, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	red_BackReduction
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	list_Length
	addl	%eax, 144(%rbx)
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 92(%rax)
	je	.LBB10_1
# BB#31:                                # %clause_IsEmptyClause.exit.thread
	testq	%r12, %r12
	je	.LBB10_1
# BB#32:                                # %.lr.ph.preheader
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB10_33:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.4, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rbp), %rdi
	callq	clause_Print
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB10_33
	jmp	.LBB10_1
.LBB10_34:
	movq	16(%rsp), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 16(%rsp)
.LBB10_22:
	movq	%r13, %rax
	cmpl	$-1, 12(%rsp)           # 4-byte Folded Reload
	jne	.LBB10_3
	.p2align	4, 0x90
.LBB10_35:                              # %.thread78
                                        # =>This Inner Loop Header: Depth=1
	testq	%rax, %rax
	je	.LBB10_39
# BB#36:                                #   in Loop: Header=BB10_35 Depth=1
	movq	(%rax), %r13
	movq	8(%rax), %r14
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rax, (%rcx)
	cmpq	$0, 120(%rbx)
	jne	.LBB10_38
# BB#37:                                #   in Loop: Header=BB10_35 Depth=1
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %edx
	callq	red_CompleteReductionOnDerivedClause
	movq	%rax, %r14
.LBB10_38:                              #   in Loop: Header=BB10_35 Depth=1
	testq	%r14, %r14
	movq	%r13, %rax
	je	.LBB10_35
	jmp	.LBB10_23
.LBB10_39:                              # %.us-lcssa.us
	movq	16(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_40:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end10:
	.size	red_CompleteReductionOnDerivedClauses, .Lfunc_end10-red_CompleteReductionOnDerivedClauses
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_Weight,@function
clause_Weight:                          # @clause_Weight
	.cfi_startproc
# BB#0:
	movl	4(%rdi), %eax
	retq
.Lfunc_end11:
	.size	clause_Weight, .Lfunc_end11-clause_Weight
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi128:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end12:
	.size	misc_Error, .Lfunc_end12-misc_Error
	.cfi_endproc

	.globl	red_ClauseDeletion
	.p2align	4, 0x90
	.type	red_ClauseDeletion,@function
red_ClauseDeletion:                     # @red_ClauseDeletion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi129:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi130:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi131:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi132:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi133:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi135:
	.cfi_def_cfa_offset 176
.Lcfi136:
	.cfi_offset %rbx, -56
.Lcfi137:
	.cfi_offset %r12, -48
.Lcfi138:
	.cfi_offset %r13, -40
.Lcfi139:
	.cfi_offset %r14, -32
.Lcfi140:
	.cfi_offset %r15, -24
.Lcfi141:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r13
	movq	%rdi, %r12
	xorl	%eax, %eax
	testq	%r12, %r12
	je	.LBB13_108
# BB#1:
	cmpl	$0, 64(%r13)
	je	.LBB13_108
# BB#2:
	cmpl	$0, 356(%r14)
	je	.LBB13_108
# BB#3:
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpl	$0, 32(%r14)
	je	.LBB13_5
# BB#4:
	movq	stdout(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$31, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rdi
	callq	clause_Print
.LBB13_5:
	callq	st_IndexCreate
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	64(%r13), %r15d
	movl	clause_CLAUSECOUNTER(%rip), %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	testl	%r15d, %r15d
	movq	%r12, 80(%rsp)          # 8-byte Spill
	jle	.LBB13_6
# BB#7:                                 # %.lr.ph146
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_8:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdi), %eax
	jne	.LBB13_10
# BB#9:                                 #   in Loop: Header=BB13_8 Depth=1
	movq	16(%rdi), %rax
	movq	8(%rax), %rdi
.LBB13_10:                              # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB13_8 Depth=1
	callq	term_Copy
	movq	%rax, %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%rbp, (%rax)
	incq	%rbx
	cmpq	%rbx, %r15
	movq	%rax, %rbp
	movq	80(%rsp), %r12          # 8-byte Reload
	jne	.LBB13_8
	jmp	.LBB13_11
.LBB13_6:
	xorl	%eax, %eax
.LBB13_11:                              # %._crit_edge
	movq	%rax, %rdi
	callq	list_NReverse
	movq	%rax, %rbp
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%r14, %rcx
	movq	40(%rsp), %r8           # 8-byte Reload
	callq	clause_Create
	movq	%rax, %rbx
	testq	%rbp, %rbp
	je	.LBB13_13
	.p2align	4, 0x90
.LBB13_12:                              # %.lr.ph.i102
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB13_12
.LBB13_13:                              # %list_Delete.exit103
	xorps	%xmm0, %xmm0
	movups	%xmm0, 12(%rbx)
	movslq	(%r13), %rbp
	movq	32(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 32(%rbx)
	movq	40(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	$0, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 40(%rbx)
	movl	$0, 76(%rbx)
	movq	%rbx, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	clause_InsertFlatIntoIndex
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%rax, %rbx
	movq	$0, (%rbx)
	movq	%rbx, 56(%rsp)
	movq	$0, 48(%rsp)
	movq	$0, 16(%rsp)
	cmpl	$0, 32(%r14)
	je	.LBB13_15
# BB#14:
	movl	$.L.str.6, %edi
	callq	puts
	movl	$.L.str.7, %edi
	callq	puts
	movq	%rbx, %rdi
	callq	clause_ListPrint
	movl	$.L.str.8, %edi
	callq	puts
	xorl	%edi, %edi
	callq	clause_ListPrint
.LBB13_15:                              # %list_Delete.exit.preheader
	testq	%rbx, %rbx
	je	.LBB13_16
# BB#19:                                # %.lr.ph.lr.ph
	movq	%r13, 112(%rsp)         # 8-byte Spill
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB13_20
	.p2align	4, 0x90
.LBB13_38:                              # %list_Delete.exit
                                        #   in Loop: Header=BB13_20 Depth=1
	testq	%rax, %rax
	je	.LBB13_20
	jmp	.LBB13_39
.LBB13_26:                              # %.preheader
                                        #   in Loop: Header=BB13_20 Depth=1
	testq	%rbp, %rbp
	je	.LBB13_37
# BB#27:                                # %.lr.ph139.preheader
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	%rbp, %rcx
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	jmp	.LBB13_28
.LBB13_32:                              #   in Loop: Header=BB13_28 Depth=2
	movq	96(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 32(%r14)
	jne	.LBB13_90
	jmp	.LBB13_91
.LBB13_58:                              #   in Loop: Header=BB13_28 Depth=2
	movq	72(%rsp), %r12          # 8-byte Reload
	jmp	.LBB13_88
	.p2align	4, 0x90
.LBB13_28:                              # %.lr.ph139
                                        #   Parent Loop BB13_20 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_43 Depth 3
                                        #         Child Loop BB13_47 Depth 4
                                        #           Child Loop BB13_49 Depth 5
                                        #       Child Loop BB13_68 Depth 3
                                        #         Child Loop BB13_70 Depth 4
                                        #           Child Loop BB13_72 Depth 5
                                        #           Child Loop BB13_76 Depth 5
                                        #       Child Loop BB13_82 Depth 3
                                        #       Child Loop BB13_86 Depth 3
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	8(%rcx), %r13
	movq	$0, 104(%rsp)
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	leaq	104(%rsp), %rbx
	movq	%rbx, %r8
	callq	red_ObviousReductions
	movq	%rbx, (%rsp)
	movl	$-1, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movq	%r13, %rsi
	movq	%r14, %r8
	movq	%r15, %r9
	callq	red_SortSimplification
	testq	%r13, %r13
	je	.LBB13_41
# BB#29:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 68(%r13)
	jne	.LBB13_41
# BB#30:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 72(%r13)
	jne	.LBB13_41
# BB#31:                                # %clause_IsEmptyClause.exit.i
                                        #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 64(%r13)
	je	.LBB13_32
	.p2align	4, 0x90
.LBB13_41:                              # %clause_IsEmptyClause.exit.thread.i
                                        #   in Loop: Header=BB13_28 Depth=2
	xorl	%esi, %esi
	movq	%r13, %rdi
	movq	%r14, %rdx
	movq	%r15, %rcx
	leaq	104(%rsp), %r8
	callq	red_Condensing
	movslq	68(%r13), %rax
	movslq	72(%r13), %rcx
	movslq	64(%r13), %rbx
	addq	%rax, %rbx
	addq	%rcx, %rbx
	testl	%ebx, %ebx
	jle	.LBB13_54
# BB#42:                                # %.lr.ph16.i.i.i
                                        #   in Loop: Header=BB13_28 Depth=2
	xorl	%r12d, %r12d
.LBB13_43:                              #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_47 Depth 4
                                        #           Child Loop BB13_49 Depth 5
	movq	56(%r13), %rax
	movq	(%rax,%r12,8), %rax
	movq	24(%rax), %rdx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdx), %eax
	jne	.LBB13_45
# BB#44:                                #   in Loop: Header=BB13_43 Depth=3
	movq	16(%rdx), %rax
	movq	8(%rax), %rdx
.LBB13_45:                              # %clause_GetLiteralAtom.exit.i.i.i
                                        #   in Loop: Header=BB13_43 Depth=3
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	st_ExistGen
	testq	%rax, %rax
	jne	.LBB13_47
	jmp	.LBB13_53
	.p2align	4, 0x90
.LBB13_52:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB13_47 Depth=4
	callq	st_NextCandidate
	testq	%rax, %rax
	je	.LBB13_53
.LBB13_47:                              # %.lr.ph12.i.i.i
                                        #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        #       Parent Loop BB13_43 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB13_49 Depth 5
	movq	8(%rax), %r15
	testq	%r15, %r15
	jne	.LBB13_49
	jmp	.LBB13_52
	.p2align	4, 0x90
.LBB13_51:                              #   in Loop: Header=BB13_49 Depth=5
	movq	(%r15), %r15
	testq	%r15, %r15
	je	.LBB13_52
.LBB13_49:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        #       Parent Loop BB13_43 Depth=3
                                        #         Parent Loop BB13_47 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	8(%r15), %rax
	movq	16(%rax), %rbp
	movq	56(%rbp), %rcx
	cmpq	%rax, (%rcx)
	jne	.LBB13_51
# BB#50:                                #   in Loop: Header=BB13_49 Depth=5
	xorl	%edx, %edx
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%r12d, %ecx
	callq	subs_Subsumes
	testl	%eax, %eax
	je	.LBB13_51
	jmp	.LBB13_59
	.p2align	4, 0x90
.LBB13_53:                              # %._crit_edge13.i.i.i
                                        #   in Loop: Header=BB13_43 Depth=3
	incq	%r12
	cmpq	%rbx, %r12
	jl	.LBB13_43
	jmp	.LBB13_54
	.p2align	4, 0x90
.LBB13_59:                              # %red_CDForwardSubsumer.exit.i.i
                                        #   in Loop: Header=BB13_28 Depth=2
	callq	st_CancelExistRetrieval
	testq	%rbp, %rbp
	movq	40(%rsp), %r15          # 8-byte Reload
	je	.LBB13_54
# BB#60:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 32(%r14)
	je	.LBB13_63
# BB#61:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 48(%r14)
	je	.LBB13_63
# BB#62:                                #   in Loop: Header=BB13_28 Depth=2
	movq	stdout(%rip), %rcx
	movl	$.L.str.46, %edi
	movl	$14, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rdi
	callq	clause_Print
	movl	(%rbp), %esi
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	printf
.LBB13_63:                              #   in Loop: Header=BB13_28 Depth=2
	movq	%r13, %rdi
	callq	clause_Delete
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB13_96
	.p2align	4, 0x90
.LBB13_54:                              # %.loopexit.i
                                        #   in Loop: Header=BB13_28 Depth=2
	testq	%r13, %r13
	je	.LBB13_64
# BB#55:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 68(%r13)
	jne	.LBB13_64
# BB#56:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 72(%r13)
	jne	.LBB13_64
# BB#57:                                # %clause_IsEmptyClause.exit.i.i
                                        #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 64(%r13)
	je	.LBB13_58
	.p2align	4, 0x90
.LBB13_64:                              # %clause_IsEmptyClause.exit.thread.i.i
                                        #   in Loop: Header=BB13_28 Depth=2
	movq	56(%r13), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rdx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rdx), %eax
	movq	72(%rsp), %r12          # 8-byte Reload
	jne	.LBB13_66
# BB#65:                                #   in Loop: Header=BB13_28 Depth=2
	movq	16(%rdx), %rax
	movq	8(%rax), %rdx
.LBB13_66:                              # %clause_GetLiteralAtom.exit.i.i
                                        #   in Loop: Header=BB13_28 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	st_ExistInstance
	testq	%rax, %rax
	je	.LBB13_88
# BB#67:                                # %.lr.ph25.i.i.preheader
                                        #   in Loop: Header=BB13_28 Depth=2
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB13_68:                              # %.lr.ph25.i.i
                                        #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB13_70 Depth 4
                                        #           Child Loop BB13_72 Depth 5
                                        #           Child Loop BB13_76 Depth 5
	movq	8(%rax), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_70
	jmp	.LBB13_80
	.p2align	4, 0x90
.LBB13_78:                              # %.loopexit.i.i
                                        #   in Loop: Header=BB13_70 Depth=4
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, %r15
.LBB13_79:                              # %list_PointerMember.exit.i.i
                                        #   in Loop: Header=BB13_70 Depth=4
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB13_80
.LBB13_70:                              # %.lr.ph21.i.i
                                        #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        #       Parent Loop BB13_68 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB13_72 Depth 5
                                        #           Child Loop BB13_76 Depth 5
	movq	8(%rbx), %rax
	movq	16(%rax), %rbp
	cmpq	%r13, %rbp
	je	.LBB13_79
# BB#71:                                #   in Loop: Header=BB13_70 Depth=4
	movq	56(%rbp), %rdx
	movl	$-1, %ecx
	.p2align	4, 0x90
.LBB13_72:                              #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        #       Parent Loop BB13_68 Depth=3
                                        #         Parent Loop BB13_70 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%ecx
	cmpq	%rax, (%rdx)
	leaq	8(%rdx), %rdx
	jne	.LBB13_72
# BB#73:                                # %clause_LiteralGetIndex.exit.i.i
                                        #   in Loop: Header=BB13_70 Depth=4
	xorl	%edx, %edx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	subs_Subsumes
	testl	%eax, %eax
	je	.LBB13_79
# BB#74:                                #   in Loop: Header=BB13_70 Depth=4
	testq	%r15, %r15
	je	.LBB13_78
# BB#75:                                # %.lr.ph.i9.i.i.preheader
                                        #   in Loop: Header=BB13_70 Depth=4
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB13_76:                              # %.lr.ph.i9.i.i
                                        #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        #       Parent Loop BB13_68 Depth=3
                                        #         Parent Loop BB13_70 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	cmpq	%rbp, 8(%rax)
	je	.LBB13_79
# BB#77:                                #   in Loop: Header=BB13_76 Depth=5
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB13_76
	jmp	.LBB13_78
	.p2align	4, 0x90
.LBB13_80:                              # %._crit_edge22.i.i
                                        #   in Loop: Header=BB13_68 Depth=3
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB13_68
# BB#81:                                # %.preheader.i.i
                                        #   in Loop: Header=BB13_28 Depth=2
	testq	%r15, %r15
	movq	%r15, %rbx
	je	.LBB13_88
	.p2align	4, 0x90
.LBB13_82:                              #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbx), %r12
	cmpl	$0, 32(%r14)
	je	.LBB13_85
# BB#83:                                #   in Loop: Header=BB13_82 Depth=3
	cmpl	$0, 48(%r14)
	je	.LBB13_85
# BB#84:                                #   in Loop: Header=BB13_82 Depth=3
	movq	stdout(%rip), %rcx
	movl	$.L.str.40, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
	movl	(%r13), %esi
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	printf
.LBB13_85:                              #   in Loop: Header=BB13_82 Depth=3
	movl	48(%r12), %eax
	testb	$1, %al
	leaq	48(%rsp), %rbp
	leaq	56(%rsp), %rax
	cmoveq	%rax, %rbp
	movq	(%rbp), %rdi
	movq	%r12, %rsi
	callq	list_PointerDeleteOneElement
	movq	%rax, (%rbp)
	movq	%r12, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	clause_DeleteFlatFromIndex
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB13_82
	.p2align	4, 0x90
.LBB13_86:                              # %.lr.ph.i.i36.i
                                        #   Parent Loop BB13_20 Depth=1
                                        #     Parent Loop BB13_28 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB13_86
# BB#87:                                # %red_CDBackSubsumption.exit.i.loopexit
                                        #   in Loop: Header=BB13_28 Depth=2
	movq	56(%rsp), %r12
.LBB13_88:                              # %red_CDBackSubsumption.exit.i
                                        #   in Loop: Header=BB13_28 Depth=2
	movq	%r13, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	clause_InsertFlatIntoIndex
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%r13, 8(%rbx)
	movq	%r12, (%rbx)
	movq	%rbx, 56(%rsp)
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	movq	40(%rsp), %r15          # 8-byte Reload
	movq	80(%rsp), %r12          # 8-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	cmpl	$0, 32(%r14)
	je	.LBB13_91
.LBB13_90:                              #   in Loop: Header=BB13_28 Depth=2
	movq	stdout(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	movq	%r13, %rdi
	callq	clause_Print
.LBB13_91:                              #   in Loop: Header=BB13_28 Depth=2
	testq	%r13, %r13
	je	.LBB13_96
# BB#92:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 68(%r13)
	jne	.LBB13_96
# BB#93:                                #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 72(%r13)
	jne	.LBB13_96
# BB#94:                                # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB13_28 Depth=2
	cmpl	$0, 64(%r13)
	jne	.LBB13_96
# BB#95:                                #   in Loop: Header=BB13_28 Depth=2
	movq	%rbx, %rbp
	movq	16(%rsp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r13, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rbp, %rbx
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rax, 16(%rsp)
	.p2align	4, 0x90
.LBB13_96:                              # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB13_28 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	$0, 8(%rcx)
	movq	(%rcx), %rcx
	movq	16(%rsp), %rax
	testq	%rcx, %rcx
	je	.LBB13_98
# BB#97:                                # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB13_28 Depth=2
	testq	%rax, %rax
	je	.LBB13_28
.LBB13_98:                              # %.critedge1
                                        #   in Loop: Header=BB13_20 Depth=1
	testq	%rax, %rax
	je	.LBB13_101
# BB#99:                                #   in Loop: Header=BB13_20 Depth=1
	cmpl	$0, 32(%r14)
	je	.LBB13_36
# BB#100:                               #   in Loop: Header=BB13_20 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.11, %edi
	movl	$35, %esi
	jmp	.LBB13_35
.LBB13_101:                             #   in Loop: Header=BB13_20 Depth=1
	testq	%rbp, %rbp
	je	.LBB13_37
	.p2align	4, 0x90
.LBB13_102:                             # %.lr.ph.i
                                        #   Parent Loop BB13_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB13_102
	jmp	.LBB13_37
	.p2align	4, 0x90
.LBB13_20:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_28 Depth 2
                                        #       Child Loop BB13_43 Depth 3
                                        #         Child Loop BB13_47 Depth 4
                                        #           Child Loop BB13_49 Depth 5
                                        #       Child Loop BB13_68 Depth 3
                                        #         Child Loop BB13_70 Depth 4
                                        #           Child Loop BB13_72 Depth 5
                                        #           Child Loop BB13_76 Depth 5
                                        #       Child Loop BB13_82 Depth 3
                                        #       Child Loop BB13_86 Depth 3
                                        #     Child Loop BB13_102 Depth 2
	movq	%rbx, %r13
	movq	8(%rbx), %rbp
	orl	$1, 48(%rbp)
	cmpl	$0, 32(%r14)
	je	.LBB13_22
# BB#21:                                #   in Loop: Header=BB13_20 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbp, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB13_22:                              #   in Loop: Header=BB13_20 Depth=1
	movq	48(%rsp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 48(%rsp)
	movq	%r13, %rdi
	movq	%rbp, %rsi
	callq	list_PointerDeleteOneElement
	movq	%rax, %rbx
	movq	%rbx, 56(%rsp)
	movq	%rbp, %rdi
	callq	clause_HasTermSortConstraintLits
	testl	%eax, %eax
	movq	(%r12), %rsi
	movl	$1, %ecx
	movq	%rbp, %rdi
	movq	%r12, %rdx
	movq	%r14, %r8
	movq	%r15, %r9
	je	.LBB13_24
# BB#23:                                #   in Loop: Header=BB13_20 Depth=1
	callq	inf_ForwardSortResolution
	jmp	.LBB13_25
	.p2align	4, 0x90
.LBB13_24:                              #   in Loop: Header=BB13_20 Depth=1
	callq	inf_ForwardEmptySort
.LBB13_25:                              # %red_CDDerivables.exit
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	%rax, %rdi
	leaq	16(%rsp), %rsi
	callq	split_ExtractEmptyClauses
	movq	%rax, %rbp
	cmpq	$0, 16(%rsp)
	je	.LBB13_26
# BB#33:                                #   in Loop: Header=BB13_20 Depth=1
	cmpl	$0, 32(%r14)
	je	.LBB13_36
# BB#34:                                #   in Loop: Header=BB13_20 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.10, %edi
	movl	$36, %esi
.LBB13_35:                              # %list_Delete.exit
                                        #   in Loop: Header=BB13_20 Depth=1
	movl	$1, %edx
	callq	fwrite
	movq	16(%rsp), %rax
	movq	8(%rax), %rdi
	callq	clause_Print
.LBB13_36:                              # %list_Delete.exit
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	%rbp, %rdi
	callq	clause_DeleteClauseList
.LBB13_37:                              # %list_Delete.exit
                                        #   in Loop: Header=BB13_20 Depth=1
	movq	16(%rsp), %rax
	testq	%rbx, %rbx
	jne	.LBB13_38
.LBB13_39:                              # %.critedge
	testq	%rax, %rax
	je	.LBB13_40
# BB#103:
	cmpl	$0, 32(%r14)
	je	.LBB13_105
# BB#104:
	movl	$.L.str.12, %edi
	callq	puts
	movl	$.L.str.13, %edi
	callq	puts
	jmp	.LBB13_105
.LBB13_16:
	xorl	%ebx, %ebx
	cmpl	$0, 76(%r14)
	jne	.LBB13_18
	jmp	.LBB13_105
.LBB13_40:
	movq	112(%rsp), %r13         # 8-byte Reload
	cmpl	$0, 76(%r14)
	je	.LBB13_105
.LBB13_18:
	movq	stdout(%rip), %rcx
	movl	$.L.str.14, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rdi
	callq	clause_Print
.LBB13_105:
	movq	%rbx, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	callq	clause_DeleteClauseListFlatFromIndex
	movq	48(%rsp), %rdi
	movq	%rbx, %rsi
	callq	clause_DeleteClauseListFlatFromIndex
	movq	%rbx, %rdi
	callq	st_IndexDelete
	movl	92(%rsp), %eax          # 4-byte Reload
	movl	%eax, clause_CLAUSECOUNTER(%rip)
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB13_106
# BB#107:
	callq	clause_DeleteClauseList
	xorl	%eax, %eax
	jmp	.LBB13_108
.LBB13_106:
	movl	$1, %eax
.LBB13_108:
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	red_ClauseDeletion, .Lfunc_end13-red_ClauseDeletion
	.cfi_endproc

	.globl	red_SatUnit
	.p2align	4, 0x90
	.type	red_SatUnit,@function
red_SatUnit:                            # @red_SatUnit
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi142:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi143:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi144:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi145:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi146:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi148:
	.cfi_def_cfa_offset 80
.Lcfi149:
	.cfi_offset %rbx, -56
.Lcfi150:
	.cfi_offset %r12, -48
.Lcfi151:
	.cfi_offset %r13, -40
.Lcfi152:
	.cfi_offset %r14, -32
.Lcfi153:
	.cfi_offset %r15, -24
.Lcfi154:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	104(%r13), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	112(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	220(%rax), %r15d
	movq	$0, (%rsp)
	movq	%rsi, %rdi
	callq	clause_ListSortWeighed
	testq	%rax, %rax
	je	.LBB14_36
.LBB14_1:                               # %.lr.ph81
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_2 Depth 2
                                        #     Child Loop BB14_20 Depth 2
                                        #     Child Loop BB14_24 Depth 2
                                        #     Child Loop BB14_28 Depth 2
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB14_2:                               #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rax
	movq	%r14, %rcx
	movq	8(%rcx), %rsi
	movq	(%rax), %r14
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rcx, (%rax)
	movl	$1, %edx
	movq	%r13, %rdi
	callq	red_ReductionOnDerivedClause
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB14_3
# BB#7:                                 #   in Loop: Header=BB14_2 Depth=2
	cmpl	$0, 68(%rbx)
	jne	.LBB14_13
# BB#8:                                 #   in Loop: Header=BB14_2 Depth=2
	cmpl	$0, 72(%rbx)
	jne	.LBB14_13
# BB#9:                                 # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB14_2 Depth=2
	cmpl	$0, 64(%rbx)
	jne	.LBB14_13
# BB#10:                                #   in Loop: Header=BB14_2 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, (%rsp)
	jmp	.LBB14_11
	.p2align	4, 0x90
.LBB14_3:                               # %..backedge_crit_edge
                                        #   in Loop: Header=BB14_2 Depth=2
	movq	(%rsp), %rax
.LBB14_11:                              # %.backedge
                                        #   in Loop: Header=BB14_2 Depth=2
	testq	%r14, %r14
	sete	%bl
	setne	%cl
	testq	%rax, %rax
	jne	.LBB14_5
# BB#12:                                # %.backedge
                                        #   in Loop: Header=BB14_2 Depth=2
	testb	%cl, %cl
	jne	.LBB14_2
	jmp	.LBB14_5
	.p2align	4, 0x90
.LBB14_13:                              # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB14_1 Depth=1
	movl	$1, %edx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	red_BackReduction
	movq	%rax, %rbp
	testl	%r15d, %r15d
	je	.LBB14_14
# BB#15:                                #   in Loop: Header=BB14_1 Depth=1
	movq	48(%r13), %rsi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	inf_BoundedDepthUnitResolution
	movq	%rax, %r12
	movq	%r12, %rdi
	callq	list_Length
	subl	%eax, %r15d
	movl	$0, %eax
	cmovbl	%eax, %r15d
	testq	%rbp, %rbp
	jne	.LBB14_18
	jmp	.LBB14_17
	.p2align	4, 0x90
.LBB14_14:                              #   in Loop: Header=BB14_1 Depth=1
	xorl	%r12d, %r12d
	xorl	%r15d, %r15d
	testq	%rbp, %rbp
	je	.LBB14_17
.LBB14_18:                              #   in Loop: Header=BB14_1 Depth=1
	testq	%r12, %r12
	je	.LBB14_22
# BB#19:                                # %.preheader.i61.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB14_20:                              # %.preheader.i61
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB14_20
# BB#21:                                #   in Loop: Header=BB14_1 Depth=1
	movq	%r12, (%rax)
	jmp	.LBB14_22
	.p2align	4, 0x90
.LBB14_17:                              #   in Loop: Header=BB14_1 Depth=1
	movq	%r12, %rbp
.LBB14_22:                              # %list_Nconc.exit63
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	%rbp, %rdi
	movq	%rsp, %rsi
	callq	split_ExtractEmptyClauses
	movq	%rax, %rbp
	movq	%r13, %rdi
	movq	%rbx, %rsi
	callq	prfs_InsertUsableClause
	testq	%rbp, %rbp
	je	.LBB14_25
# BB#23:                                # %.lr.ph86.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB14_24:                              # %.lr.ph86
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rcx
	movl	$0, 8(%rcx)
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB14_24
.LBB14_25:                              # %._crit_edge
                                        #   in Loop: Header=BB14_1 Depth=1
	testq	%r14, %r14
	movq	%r14, %rax
	cmoveq	%rbp, %rax
	je	.LBB14_30
# BB#26:                                # %._crit_edge
                                        #   in Loop: Header=BB14_1 Depth=1
	testq	%rbp, %rbp
	je	.LBB14_30
# BB#27:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB14_1 Depth=1
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB14_28:                              # %.preheader.i
                                        #   Parent Loop BB14_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB14_28
# BB#29:                                #   in Loop: Header=BB14_1 Depth=1
	movq	%rbp, (%rax)
	movq	%r14, %rax
.LBB14_30:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB14_1 Depth=1
	testq	%rax, %rax
	sete	%bl
	setne	%cl
	cmpq	$0, (%rsp)
	jne	.LBB14_31
# BB#4:                                 # %list_Nconc.exit
                                        #   in Loop: Header=BB14_1 Depth=1
	testb	%cl, %cl
	movq	%rax, %r14
	jne	.LBB14_1
	jmp	.LBB14_5
.LBB14_31:
	movq	%rax, %r14
.LBB14_5:                               # %.critedge.preheader
	testq	%r14, %r14
	je	.LBB14_6
# BB#32:
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB14_33:                              # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rsi
	movq	%r13, %rdi
	callq	prfs_InsertUsableClause
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB14_33
	jmp	.LBB14_34
.LBB14_6:
	xorl	%r14d, %r14d
.LBB14_34:                              # %.critedge._crit_edge
	testb	%bl, %bl
	jne	.LBB14_36
	.p2align	4, 0x90
.LBB14_35:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB14_35
.LBB14_36:                              # %list_Delete.exit
	movq	(%rsp), %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	red_SatUnit, .Lfunc_end14-red_SatUnit
	.cfi_endproc

	.globl	red_ReduceInput
	.p2align	4, 0x90
	.type	red_ReduceInput,@function
red_ReduceInput:                        # @red_ReduceInput
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi155:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi156:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi157:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi158:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi159:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi161:
	.cfi_def_cfa_offset 96
.Lcfi162:
	.cfi_offset %rbx, -56
.Lcfi163:
	.cfi_offset %r12, -48
.Lcfi164:
	.cfi_offset %r13, -40
.Lcfi165:
	.cfi_offset %r14, -32
.Lcfi166:
	.cfi_offset %r15, -24
.Lcfi167:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	104(%r14), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	112(%r14), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	$0, 8(%rsp)
	movq	%rsi, %rdi
	callq	list_Copy
	movq	%rax, %rdi
	callq	clause_ListSortWeighed
	leaq	8(%rsp), %rsi
	movq	%rax, %rdi
	callq	split_ExtractEmptyClauses
	jmp	.LBB15_1
	.p2align	4, 0x90
.LBB15_36:                              # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB15_1 Depth=1
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	red_BackReduction
	movq	%rax, %rbp
	movq	%rbp, %rdi
	callq	list_Length
	addl	%eax, 144(%r14)
	movq	%rbp, %rdi
	leaq	8(%rsp), %rsi
	callq	split_ExtractEmptyClauses
	movq	%rax, %rbp
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	prfs_InsertUsableClause
	movq	%rbp, %rdi
	callq	clause_ListSortWeighed
	movl	$clause_Weight, %edx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	callq	list_NNumberMerge
.LBB15_1:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_2 Depth 2
                                        #       Child Loop BB15_15 Depth 3
                                        #       Child Loop BB15_23 Depth 3
                                        #       Child Loop BB15_29 Depth 3
	movq	%rax, %rbp
	jmp	.LBB15_2
	.p2align	4, 0x90
.LBB15_35:                              # %.backedge
                                        #   in Loop: Header=BB15_2 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB15_2:                               #   Parent Loop BB15_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_15 Depth 3
                                        #       Child Loop BB15_23 Depth 3
                                        #       Child Loop BB15_29 Depth 3
	testq	%rbp, %rbp
	sete	%r15b
	setne	%al
	cmpq	$0, 8(%rsp)
	jne	.LBB15_4
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=2
	testb	%al, %al
	je	.LBB15_4
# BB#10:                                #   in Loop: Header=BB15_2 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	28(%rax), %eax
	cmpl	$-1, %eax
	je	.LBB15_12
# BB#11:                                #   in Loop: Header=BB15_2 Depth=2
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movl	$1, %edi
	callq	clock_GetSeconds
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB15_4
.LBB15_12:                              # %.critedge2
                                        #   in Loop: Header=BB15_2 Depth=2
	movq	8(%rbp), %r13
	movq	(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbp, (%rax)
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	leal	(%rcx,%rax), %ebx
	movl	%ebx, %edx
	decl	%edx
	js	.LBB15_13
# BB#14:                                # %.lr.ph57.i
                                        #   in Loop: Header=BB15_2 Depth=2
	xorl	%ebp, %ebp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB15_15:                              #   Parent Loop BB15_1 Depth=1
                                        #     Parent Loop BB15_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB15_17
# BB#16:                                #   in Loop: Header=BB15_15 Depth=3
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB15_17:                              # %clause_LiteralAtom.exit51.i
                                        #   in Loop: Header=BB15_15 Depth=3
	cmpl	%eax, fol_TRUE(%rip)
	jne	.LBB15_19
# BB#18:                                #   in Loop: Header=BB15_15 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB15_19:                              #   in Loop: Header=BB15_15 Depth=3
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB15_15
# BB#20:                                # %._crit_edge58.loopexit.i
                                        #   in Loop: Header=BB15_2 Depth=2
	movl	64(%r13), %ecx
	movl	68(%r13), %eax
	jmp	.LBB15_21
	.p2align	4, 0x90
.LBB15_13:                              #   in Loop: Header=BB15_2 Depth=2
	xorl	%r12d, %r12d
.LBB15_21:                              # %._crit_edge58.i
                                        #   in Loop: Header=BB15_2 Depth=2
	movl	72(%r13), %edx
	addl	%ecx, %eax
	leal	-1(%rdx,%rax), %ecx
	cmpl	%ecx, %eax
	jg	.LBB15_28
# BB#22:                                # %.lr.ph.i39
                                        #   in Loop: Header=BB15_2 Depth=2
	movslq	%eax, %rbx
	movslq	%ecx, %r15
	.p2align	4, 0x90
.LBB15_23:                              #   Parent Loop BB15_1 Depth=1
                                        #     Parent Loop BB15_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rcx
	movl	(%rcx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB15_25
# BB#24:                                #   in Loop: Header=BB15_23 Depth=3
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movl	(%rax), %eax
.LBB15_25:                              # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB15_23 Depth=3
	cmpl	%eax, fol_FALSE(%rip)
	jne	.LBB15_27
# BB#26:                                #   in Loop: Header=BB15_23 Depth=3
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, %r12
.LBB15_27:                              #   in Loop: Header=BB15_23 Depth=3
	cmpq	%r15, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB15_23
.LBB15_28:                              # %._crit_edge.i
                                        #   in Loop: Header=BB15_2 Depth=2
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiterals
	testq	%r12, %r12
	je	.LBB15_30
	.p2align	4, 0x90
.LBB15_29:                              # %.lr.ph.i.i
                                        #   Parent Loop BB15_1 Depth=1
                                        #     Parent Loop BB15_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB15_29
.LBB15_30:                              # %red_SpecialInputReductions.exit
                                        #   in Loop: Header=BB15_2 Depth=2
	movl	$1, %edx
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	red_ReductionOnDerivedClause
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB15_35
# BB#31:                                #   in Loop: Header=BB15_2 Depth=2
	incl	140(%r14)
	cmpl	$0, 68(%rbx)
	jne	.LBB15_36
# BB#32:                                #   in Loop: Header=BB15_2 Depth=2
	cmpl	$0, 72(%rbx)
	jne	.LBB15_36
# BB#33:                                # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB15_2 Depth=2
	cmpl	$0, 64(%rbx)
	jne	.LBB15_36
# BB#34:                                #   in Loop: Header=BB15_2 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 8(%rsp)
	jmp	.LBB15_35
.LBB15_4:                               # %.critedge.preheader
	testq	%rbp, %rbp
	je	.LBB15_7
# BB#5:
	movq	%rbp, %rbx
	.p2align	4, 0x90
.LBB15_6:                               # %.critedge
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r14, %rdi
	callq	prfs_InsertUsableClause
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB15_6
.LBB15_7:                               # %.critedge._crit_edge
	testb	$1, %r15b
	jne	.LBB15_9
	.p2align	4, 0x90
.LBB15_8:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB15_8
.LBB15_9:                               # %list_Delete.exit
	movq	8(%rsp), %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	red_ReduceInput, .Lfunc_end15-red_ReduceInput
	.cfi_endproc

	.globl	red_SatInput
	.p2align	4, 0x90
	.type	red_SatInput,@function
red_SatInput:                           # @red_SatInput
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi172:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi173:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi174:
	.cfi_def_cfa_offset 80
.Lcfi175:
	.cfi_offset %rbx, -56
.Lcfi176:
	.cfi_offset %r12, -48
.Lcfi177:
	.cfi_offset %r13, -40
.Lcfi178:
	.cfi_offset %r14, -32
.Lcfi179:
	.cfi_offset %r15, -24
.Lcfi180:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	112(%r14), %r12
	movq	56(%r14), %rbp
	movq	104(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rbp, %rdi
	callq	list_Length
	movl	%eax, %r13d
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	jle	.LBB16_14
# BB#1:
	testq	%rbp, %rbp
	je	.LBB16_14
# BB#2:                                 # %.lr.ph110
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_9 Depth 2
	movl	28(%r12), %eax
	cmpl	$-1, %eax
	je	.LBB16_5
# BB#4:                                 #   in Loop: Header=BB16_3 Depth=1
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movl	$1, %edi
	callq	clock_GetSeconds
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB16_14
.LBB16_5:                               # %.critedge2
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	8(%rbp), %rdi
	testb	$8, 48(%rdi)
	je	.LBB16_12
# BB#6:                                 #   in Loop: Header=BB16_3 Depth=1
	movq	48(%r14), %rsi
	xorl	%edx, %edx
	movq	%r12, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	inf_BoundedDepthUnitResolution
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	list_Length
	subl	%eax, %r13d
	testq	%r15, %r15
	je	.LBB16_12
# BB#7:                                 #   in Loop: Header=BB16_3 Depth=1
	testq	%rbx, %rbx
	je	.LBB16_11
# BB#8:                                 # %.preheader.i93.preheader
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB16_9:                               # %.preheader.i93
                                        #   Parent Loop BB16_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB16_9
# BB#10:                                #   in Loop: Header=BB16_3 Depth=1
	movq	%rbx, (%rax)
.LBB16_11:                              # %list_Nconc.exit95
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	%r15, %rbx
.LBB16_12:                              # %list_Nconc.exit95
                                        #   in Loop: Header=BB16_3 Depth=1
	testl	%r13d, %r13d
	jle	.LBB16_14
# BB#13:                                # %list_Nconc.exit95
                                        #   in Loop: Header=BB16_3 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB16_3
.LBB16_14:                              # %.critedge
	movq	%rbx, %rdi
	callq	list_Length
	addl	%eax, 144(%r14)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	red_ReduceInput
	testq	%rbx, %rbx
	je	.LBB16_16
	.p2align	4, 0x90
.LBB16_15:                              # %.lr.ph.i88
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB16_15
.LBB16_16:                              # %list_Delete.exit89
	testq	%rax, %rax
	jne	.LBB16_34
# BB#17:                                # %.preheader
	xorl	%ebx, %ebx
	testl	%r13d, %r13d
	jle	.LBB16_32
# BB#18:                                # %.preheader
	movq	56(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB16_32
# BB#19:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_20:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_27 Depth 2
	movl	28(%r12), %eax
	cmpl	$-1, %eax
	je	.LBB16_22
# BB#21:                                #   in Loop: Header=BB16_20 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movl	$1, %edi
	callq	clock_GetSeconds
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB16_32
.LBB16_22:                              # %.critedge6
                                        #   in Loop: Header=BB16_20 Depth=1
	movq	8(%rbp), %rdi
	testb	$8, 48(%rdi)
	je	.LBB16_30
# BB#23:                                #   in Loop: Header=BB16_20 Depth=1
	cmpl	$16, 76(%rdi)
	jne	.LBB16_30
# BB#24:                                #   in Loop: Header=BB16_20 Depth=1
	movq	48(%r14), %rsi
	movl	$1, %edx
	movq	%r12, %rcx
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	inf_BoundedDepthUnitResolution
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	list_Length
	subl	%eax, %r13d
	testq	%r15, %r15
	je	.LBB16_30
# BB#25:                                #   in Loop: Header=BB16_20 Depth=1
	testq	%rbx, %rbx
	je	.LBB16_29
# BB#26:                                # %.preheader.i.preheader
                                        #   in Loop: Header=BB16_20 Depth=1
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB16_27:                              # %.preheader.i
                                        #   Parent Loop BB16_20 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB16_27
# BB#28:                                #   in Loop: Header=BB16_20 Depth=1
	movq	%rbx, (%rax)
.LBB16_29:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB16_20 Depth=1
	movq	%r15, %rbx
	.p2align	4, 0x90
.LBB16_30:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB16_20 Depth=1
	testl	%r13d, %r13d
	jle	.LBB16_32
# BB#31:                                # %list_Nconc.exit
                                        #   in Loop: Header=BB16_20 Depth=1
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB16_20
.LBB16_32:                              # %.critedge5
	movq	%rbx, %rdi
	callq	list_Length
	addl	%eax, 144(%r14)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	red_ReduceInput
	testq	%rbx, %rbx
	je	.LBB16_34
	.p2align	4, 0x90
.LBB16_33:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rbx, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rbx
	jne	.LBB16_33
.LBB16_34:                              # %list_Delete.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	red_SatInput, .Lfunc_end16-red_SatInput
	.cfi_endproc

	.globl	red_CheckSplitSubsumptionCondition
	.p2align	4, 0x90
	.type	red_CheckSplitSubsumptionCondition,@function
red_CheckSplitSubsumptionCondition:     # @red_CheckSplitSubsumptionCondition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi181:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi182:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi183:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi184:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi185:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi186:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi187:
	.cfi_def_cfa_offset 64
.Lcfi188:
	.cfi_offset %rbx, -56
.Lcfi189:
	.cfi_offset %r12, -48
.Lcfi190:
	.cfi_offset %r13, -40
.Lcfi191:
	.cfi_offset %r14, -32
.Lcfi192:
	.cfi_offset %r15, -24
.Lcfi193:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	120(%r14), %r13
	testq	%r13, %r13
	je	.LBB17_9
# BB#1:                                 # %.lr.ph50
	movq	104(%r14), %r15
	movq	112(%r14), %r12
	.p2align	4, 0x90
.LBB17_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_4 Depth 2
	movq	8(%r13), %rax
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB17_4
	jmp	.LBB17_8
	.p2align	4, 0x90
.LBB17_7:                               #   in Loop: Header=BB17_4 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB17_8
.LBB17_4:                               # %.lr.ph
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rbx
	movq	32(%r14), %rsi
	movq	%rbx, %rdi
	callq	red_ForwardSubsumer
	testq	%rax, %rax
	jne	.LBB17_7
# BB#5:                                 #   in Loop: Header=BB17_4 Depth=2
	movq	48(%r14), %rsi
	movq	%rbx, %rdi
	callq	red_ForwardSubsumer
	testq	%rax, %rax
	jne	.LBB17_7
# BB#6:                                 #   in Loop: Header=BB17_4 Depth=2
	movq	64(%r14), %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	red_ClauseDeletion
	testl	%eax, %eax
	jne	.LBB17_7
	jmp	.LBB17_10
	.p2align	4, 0x90
.LBB17_8:                               # %._crit_edge
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB17_2
.LBB17_9:                               # %._crit_edge51
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_10:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	movl	$.L.str.15, %esi
	movl	$.L.str.16, %edx
	movl	$4500, %ecx             # imm = 0x1194
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.17, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	%rbx, %rdi
	callq	clause_Print
	movl	$.L.str.18, %edi
	xorl	%eax, %eax
	callq	misc_ErrorReport
	movq	8(%r13), %rdi
	callq	prfs_PrintSplit
	movq	stderr(%rip), %rcx
	movl	$.L.str.19, %edi
	movl	$132, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.47, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	callq	abort
.Lfunc_end17:
	.size	red_CheckSplitSubsumptionCondition, .Lfunc_end17-red_CheckSplitSubsumptionCondition
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_ForwardSubsumer,@function
red_ForwardSubsumer:                    # @red_ForwardSubsumer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi194:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi195:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi196:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi197:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi198:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi199:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi200:
	.cfi_def_cfa_offset 112
.Lcfi201:
	.cfi_offset %rbx, -56
.Lcfi202:
	.cfi_offset %r12, -48
.Lcfi203:
	.cfi_offset %r13, -40
.Lcfi204:
	.cfi_offset %r14, -32
.Lcfi205:
	.cfi_offset %r15, -24
.Lcfi206:
	.cfi_offset %rbp, -16
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	movq	%rdi, %r12
	movslq	64(%r12), %rdi
	movl	68(%r12), %eax
	leal	-1(%rdi,%rax), %edx
	movl	72(%r12), %ecx
	addl	%edx, %ecx
	js	.LBB18_68
# BB#1:                                 # %.lr.ph116
	leal	-1(%rdi), %esi
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	addl	%edi, %eax
	movslq	%edx, %rdx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movslq	%eax, %r14
	movslq	%esi, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	%ecx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
.LBB18_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_6 Depth 2
                                        #       Child Loop BB18_19 Depth 3
                                        #       Child Loop BB18_9 Depth 3
                                        #     Child Loop BB18_36 Depth 2
                                        #       Child Loop BB18_49 Depth 3
                                        #       Child Loop BB18_39 Depth 3
                                        #     Child Loop BB18_65 Depth 2
	movq	56(%r12), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB18_4
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB18_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rdx
	callq	st_ExistGen
	testq	%rax, %rax
	je	.LBB18_32
# BB#5:                                 # %.lr.ph101
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpq	24(%rsp), %r15          # 8-byte Folded Reload
	setl	%cl
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	setg	%r13b
	orb	%cl, %r13b
	jmp	.LBB18_6
	.p2align	4, 0x90
.LBB18_7:                               #   in Loop: Header=BB18_6 Depth=2
	movq	%rax, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB18_31
# BB#8:                                 # %.lr.ph
                                        #   in Loop: Header=BB18_6 Depth=2
	cmpq	40(%rsp), %r15          # 8-byte Folded Reload
	jle	.LBB18_19
	.p2align	4, 0x90
.LBB18_9:                               # %.lr.ph.split.us
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rax
	movq	16(%rax), %rbx
	cmpq	%r12, %rbx
	je	.LBB18_18
# BB#10:                                #   in Loop: Header=BB18_9 Depth=3
	movq	56(%rbx), %rcx
	cmpq	%rax, (%rcx)
	jne	.LBB18_18
# BB#11:                                #   in Loop: Header=BB18_9 Depth=3
	testb	%r13b, %r13b
	jne	.LBB18_14
# BB#12:                                # %clause_LiteralIsFromAntecedent.exit73.us
                                        #   in Loop: Header=BB18_9 Depth=3
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB18_14
# BB#13:                                # %clause_LiteralIsFromAntecedent.exit73.us
                                        #   in Loop: Header=BB18_9 Depth=3
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %eax
	testl	%eax, %eax
	jns	.LBB18_17
.LBB18_14:                              #   in Loop: Header=BB18_9 Depth=3
	cmpq	%r14, %r15
	jl	.LBB18_18
# BB#15:                                # %clause_LiteralIsFromSuccedent.exit60.us
                                        #   in Loop: Header=BB18_9 Depth=3
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB18_18
# BB#16:                                # %clause_LiteralIsFromSuccedent.exit60.us
                                        #   in Loop: Header=BB18_9 Depth=3
	movl	72(%rbx), %ecx
	leal	-1(%rcx,%rax), %eax
	testl	%eax, %eax
	js	.LBB18_18
.LBB18_17:                              #   in Loop: Header=BB18_9 Depth=3
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%r15d, %ecx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	jne	.LBB18_29
	.p2align	4, 0x90
.LBB18_18:                              #   in Loop: Header=BB18_9 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB18_9
	jmp	.LBB18_31
.LBB18_22:                              #   in Loop: Header=BB18_19 Depth=3
	testb	%r13b, %r13b
	jne	.LBB18_25
# BB#23:                                # %clause_LiteralIsFromAntecedent.exit73
                                        #   in Loop: Header=BB18_19 Depth=3
	testl	%eax, %eax
	jg	.LBB18_25
# BB#24:                                # %clause_LiteralIsFromAntecedent.exit73
                                        #   in Loop: Header=BB18_19 Depth=3
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB18_28
.LBB18_25:                              #   in Loop: Header=BB18_19 Depth=3
	cmpq	%r14, %r15
	jl	.LBB18_30
# BB#26:                                # %clause_LiteralIsFromSuccedent.exit60
                                        #   in Loop: Header=BB18_19 Depth=3
	addl	68(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB18_30
# BB#27:                                # %clause_LiteralIsFromSuccedent.exit60
                                        #   in Loop: Header=BB18_19 Depth=3
	movl	72(%rbx), %ecx
	leal	-1(%rcx,%rax), %eax
	testl	%eax, %eax
	jns	.LBB18_28
	jmp	.LBB18_30
	.p2align	4, 0x90
.LBB18_19:                              # %.lr.ph.split
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_6 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rax
	movq	16(%rax), %rbx
	cmpq	%r12, %rbx
	je	.LBB18_30
# BB#20:                                #   in Loop: Header=BB18_19 Depth=3
	movq	56(%rbx), %rcx
	cmpq	%rax, (%rcx)
	jne	.LBB18_30
# BB#21:                                # %clause_LiteralIsFromConstraint.exit84
                                        #   in Loop: Header=BB18_19 Depth=3
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB18_22
.LBB18_28:                              #   in Loop: Header=BB18_19 Depth=3
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%r15d, %ecx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	jne	.LBB18_29
	.p2align	4, 0x90
.LBB18_30:                              #   in Loop: Header=BB18_19 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB18_19
	jmp	.LBB18_31
	.p2align	4, 0x90
.LBB18_6:                               #   Parent Loop BB18_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_19 Depth 3
                                        #       Child Loop BB18_9 Depth 3
	cmpl	$0, (%rax)
	jle	.LBB18_7
.LBB18_31:                              # %.loopexit95
                                        #   in Loop: Header=BB18_6 Depth=2
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB18_6
.LBB18_32:                              # %._crit_edge
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rcx), %ebx
	cmpl	%ebx, fol_EQUALITY(%rip)
	jne	.LBB18_67
# BB#33:                                #   in Loop: Header=BB18_2 Depth=1
	movq	56(%r12), %rax
	movq	(%rax,%r15,8), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB18_67
# BB#34:                                #   in Loop: Header=BB18_2 Depth=1
	movq	16(%rcx), %rdi
	callq	list_Reverse
	movl	%ebx, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rcx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rsi
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rcx, %rdx
	callq	st_ExistGen
	testq	%rax, %rax
	je	.LBB18_64
# BB#35:                                # %.lr.ph110
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpq	24(%rsp), %r15          # 8-byte Folded Reload
	setl	%cl
	cmpq	16(%rsp), %r15          # 8-byte Folded Reload
	setg	%r13b
	orb	%cl, %r13b
	jmp	.LBB18_36
	.p2align	4, 0x90
.LBB18_37:                              #   in Loop: Header=BB18_36 Depth=2
	movq	%rax, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB18_63
# BB#38:                                # %.lr.ph104
                                        #   in Loop: Header=BB18_36 Depth=2
	cmpq	40(%rsp), %r15          # 8-byte Folded Reload
	jle	.LBB18_49
	.p2align	4, 0x90
.LBB18_39:                              # %.lr.ph104.split.us
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rax
	movq	16(%rax), %rbx
	cmpq	%r12, %rbx
	je	.LBB18_48
# BB#40:                                #   in Loop: Header=BB18_39 Depth=3
	movq	56(%rbx), %rcx
	cmpq	%rax, (%rcx)
	jne	.LBB18_48
# BB#41:                                #   in Loop: Header=BB18_39 Depth=3
	testb	%r13b, %r13b
	jne	.LBB18_44
# BB#42:                                # %clause_LiteralIsFromAntecedent.exit.us
                                        #   in Loop: Header=BB18_39 Depth=3
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB18_44
# BB#43:                                # %clause_LiteralIsFromAntecedent.exit.us
                                        #   in Loop: Header=BB18_39 Depth=3
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %eax
	testl	%eax, %eax
	jns	.LBB18_47
.LBB18_44:                              #   in Loop: Header=BB18_39 Depth=3
	cmpq	%r14, %r15
	jl	.LBB18_48
# BB#45:                                # %clause_LiteralIsFromSuccedent.exit.us
                                        #   in Loop: Header=BB18_39 Depth=3
	movl	68(%rbx), %eax
	addl	64(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB18_48
# BB#46:                                # %clause_LiteralIsFromSuccedent.exit.us
                                        #   in Loop: Header=BB18_39 Depth=3
	movl	72(%rbx), %ecx
	leal	-1(%rcx,%rax), %eax
	testl	%eax, %eax
	js	.LBB18_48
.LBB18_47:                              #   in Loop: Header=BB18_39 Depth=3
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%r15d, %ecx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	jne	.LBB18_59
	.p2align	4, 0x90
.LBB18_48:                              #   in Loop: Header=BB18_39 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB18_39
	jmp	.LBB18_63
.LBB18_52:                              #   in Loop: Header=BB18_49 Depth=3
	testb	%r13b, %r13b
	jne	.LBB18_55
# BB#53:                                # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB18_49 Depth=3
	testl	%eax, %eax
	jg	.LBB18_55
# BB#54:                                # %clause_LiteralIsFromAntecedent.exit
                                        #   in Loop: Header=BB18_49 Depth=3
	movl	68(%rbx), %ecx
	leal	-1(%rax,%rcx), %ecx
	testl	%ecx, %ecx
	jns	.LBB18_58
.LBB18_55:                              #   in Loop: Header=BB18_49 Depth=3
	cmpq	%r14, %r15
	jl	.LBB18_62
# BB#56:                                # %clause_LiteralIsFromSuccedent.exit
                                        #   in Loop: Header=BB18_49 Depth=3
	addl	68(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB18_62
# BB#57:                                # %clause_LiteralIsFromSuccedent.exit
                                        #   in Loop: Header=BB18_49 Depth=3
	movl	72(%rbx), %ecx
	leal	-1(%rcx,%rax), %eax
	testl	%eax, %eax
	jns	.LBB18_58
	jmp	.LBB18_62
	.p2align	4, 0x90
.LBB18_49:                              # %.lr.ph104.split
                                        #   Parent Loop BB18_2 Depth=1
                                        #     Parent Loop BB18_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rbp), %rax
	movq	16(%rax), %rbx
	cmpq	%r12, %rbx
	je	.LBB18_62
# BB#50:                                #   in Loop: Header=BB18_49 Depth=3
	movq	56(%rbx), %rcx
	cmpq	%rax, (%rcx)
	jne	.LBB18_62
# BB#51:                                # %clause_LiteralIsFromConstraint.exit
                                        #   in Loop: Header=BB18_49 Depth=3
	movl	64(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB18_52
.LBB18_58:                              #   in Loop: Header=BB18_49 Depth=3
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%r15d, %ecx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	jne	.LBB18_59
	.p2align	4, 0x90
.LBB18_62:                              #   in Loop: Header=BB18_49 Depth=3
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB18_49
	jmp	.LBB18_63
	.p2align	4, 0x90
.LBB18_36:                              #   Parent Loop BB18_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB18_49 Depth 3
                                        #       Child Loop BB18_39 Depth 3
	cmpl	$0, (%rax)
	jle	.LBB18_37
.LBB18_63:                              # %.loopexit
                                        #   in Loop: Header=BB18_36 Depth=2
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB18_36
.LBB18_64:                              # %._crit_edge111
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB18_66
	.p2align	4, 0x90
.LBB18_65:                              # %.lr.ph.i
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB18_65
.LBB18_66:                              # %list_Delete.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rdi, (%rax)
	.p2align	4, 0x90
.LBB18_67:                              #   in Loop: Header=BB18_2 Depth=1
	cmpq	48(%rsp), %r15          # 8-byte Folded Reload
	leaq	1(%r15), %r15
	jl	.LBB18_2
.LBB18_68:
	xorl	%ebx, %ebx
	jmp	.LBB18_69
.LBB18_29:                              # %.us-lcssa.us
	callq	st_CancelExistRetrieval
.LBB18_69:                              # %.loopexit96
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_59:                              # %.us-lcssa105.us
	callq	st_CancelExistRetrieval
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rdi), %rax
	testq	%rax, %rax
	je	.LBB18_61
	.p2align	4, 0x90
.LBB18_60:                              # %.lr.ph.i28
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB18_60
.LBB18_61:                              # %list_Delete.exit29
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdi)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rdi, (%rax)
	jmp	.LBB18_69
.Lfunc_end18:
	.size	red_ForwardSubsumer, .Lfunc_end18-red_ForwardSubsumer
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_SimpleStaticReductions,@function
red_SimpleStaticReductions:             # @red_SimpleStaticReductions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi207:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi208:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi210:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi211:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi212:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi213:
	.cfi_def_cfa_offset 112
.Lcfi214:
	.cfi_offset %rbx, -56
.Lcfi215:
	.cfi_offset %r12, -48
.Lcfi216:
	.cfi_offset %r13, -40
.Lcfi217:
	.cfi_offset %r14, -32
.Lcfi218:
	.cfi_offset %r15, -24
.Lcfi219:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbp
	movq	$0, 8(%rsp)
	movl	36(%rsi), %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movl	352(%rsi), %ecx
	testl	%ecx, %ecx
	je	.LBB19_20
# BB#1:
	movq	(%rbp), %r14
	movl	72(%r14), %eax
	testl	%eax, %eax
	jle	.LBB19_13
# BB#2:                                 # %.lr.ph37.i
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	64(%r14), %esi
	movl	68(%r14), %ecx
	leal	-1(%rcx,%rsi), %edx
	addl	%esi, %ecx
	addl	%ecx, %eax
	testl	%edx, %edx
	js	.LBB19_3
# BB#27:                                # %.lr.ph37.split.us.preheader.i
	movq	%r13, 40(%rsp)          # 8-byte Spill
	movslq	%edx, %rbp
	movslq	%ecx, %r15
	movslq	%eax, %r12
	.p2align	4, 0x90
.LBB19_28:                              # %.lr.ph37.split.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB19_39 Depth 2
	movq	56(%r14), %rax
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rbx
	movl	(%rbx), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB19_30
# BB#29:                                #   in Loop: Header=BB19_28 Depth=1
	movq	16(%rbx), %rcx
	movq	8(%rcx), %rbx
	movl	(%rbx), %ecx
.LBB19_30:                              # %clause_LiteralAtom.exit26.us.i
                                        #   in Loop: Header=BB19_28 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB19_38
# BB#31:                                #   in Loop: Header=BB19_28 Depth=1
	cmpl	$0, 8(%rax)
	jne	.LBB19_38
# BB#32:                                # %.preheader.us.i
                                        #   in Loop: Header=BB19_28 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rsi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB19_33
	.p2align	4, 0x90
.LBB19_38:                              # %.lr.ph.us.i.preheader
                                        #   in Loop: Header=BB19_28 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB19_39:                              # %.lr.ph.us.i
                                        #   Parent Loop BB19_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rax
	movq	(%rax,%r13,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB19_41
# BB#40:                                #   in Loop: Header=BB19_39 Depth=2
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB19_41:                              # %clause_LiteralAtom.exit.us.i
                                        #   in Loop: Header=BB19_39 Depth=2
	movq	%rbx, %rdi
	callq	term_Equal
	cmpq	%rbp, %r13
	jge	.LBB19_34
# BB#42:                                # %clause_LiteralAtom.exit.us.i
                                        #   in Loop: Header=BB19_39 Depth=2
	incq	%r13
	testl	%eax, %eax
	je	.LBB19_39
.LBB19_34:                              # %.critedge1.us.i
                                        #   in Loop: Header=BB19_28 Depth=1
	incq	%r15
	cmpq	%r12, %r15
	jge	.LBB19_36
# BB#35:                                # %.critedge1.us.i
                                        #   in Loop: Header=BB19_28 Depth=1
	testl	%eax, %eax
	je	.LBB19_28
.LBB19_36:                              # %.critedge.i.loopexit
	xorl	%r15d, %r15d
	testl	%eax, %eax
	setne	%al
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB19_12
# BB#37:
	movb	%al, %r15b
	jmp	.LBB19_17
.LBB19_3:                               # %.lr.ph37.split.preheader.i
	movslq	%ecx, %rbx
	movslq	%eax, %rbp
	incq	%rbx
	.p2align	4, 0x90
.LBB19_4:                               # %.lr.ph37.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rax
	movq	-8(%rax,%rbx,8), %rcx
	movq	24(%rcx), %rax
	movl	(%rax), %edx
	cmpl	%edx, fol_NOT(%rip)
	jne	.LBB19_6
# BB#5:                                 #   in Loop: Header=BB19_4 Depth=1
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %edx
.LBB19_6:                               # %clause_LiteralAtom.exit26.i
                                        #   in Loop: Header=BB19_4 Depth=1
	xorl	%r15d, %r15d
	cmpl	%edx, fol_EQUALITY(%rip)
	jne	.LBB19_9
# BB#7:                                 #   in Loop: Header=BB19_4 Depth=1
	cmpl	$0, 8(%rcx)
	jne	.LBB19_9
# BB#8:                                 #   in Loop: Header=BB19_4 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rsi
	callq	term_Equal
	xorl	%r15d, %r15d
	testl	%eax, %eax
	setne	%r15b
	.p2align	4, 0x90
.LBB19_9:                               # %.critedge1.i
                                        #   in Loop: Header=BB19_4 Depth=1
	cmpq	%rbp, %rbx
	jge	.LBB19_11
# BB#10:                                # %.critedge1.i
                                        #   in Loop: Header=BB19_4 Depth=1
	incq	%rbx
	testl	%r15d, %r15d
	je	.LBB19_4
.LBB19_11:                              # %.critedge.i
	testl	%r15d, %r15d
	movq	32(%rsp), %rbp          # 8-byte Reload
	jne	.LBB19_17
.LBB19_12:                              # %.critedge.i..critedge.thread.i_crit_edge
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	352(%rax), %ecx
.LBB19_13:                              # %.critedge.thread.i
	cmpl	$2, %ecx
	jne	.LBB19_20
# BB#14:
	cmpl	$0, 68(%r14)
	je	.LBB19_20
# BB#15:
	cmpl	$0, 72(%r14)
	je	.LBB19_20
# BB#16:
	movq	%r14, %rdi
	callq	cc_Tautology
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB19_17
	jmp	.LBB19_20
.LBB19_33:
	movl	$1, %r15d
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB19_17:                              # %.thread28.i
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 64(%rax)
	je	.LBB19_19
# BB#18:
	movq	stdout(%rip), %rcx
	movl	$.L.str.20, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	clause_Print
.LBB19_19:                              # %red_Tautology.exit
	movl	$1, %r12d
	testl	%r15d, %r15d
	jne	.LBB19_26
.LBB19_20:                              # %red_Tautology.exit.thread
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 340(%rbx)
	je	.LBB19_23
# BB#21:
	movq	(%rbp), %rdi
	leaq	8(%rsp), %r8
	movl	20(%rsp), %esi          # 4-byte Reload
	movq	%rbx, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	red_ObviousReductions
	movq	8(%rsp), %r12
	testq	%r12, %r12
	je	.LBB19_23
# BB#22:
	movq	(%rbp), %r15
	movq	(%r13), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r14, (%rax)
	movq	24(%rsp), %rbx          # 8-byte Reload
	movq	%rax, (%r13)
	movq	%r12, (%rbp)
	movq	$0, 8(%rsp)
.LBB19_23:                              # %red_ExchangeClauses.exit20
	xorl	%r12d, %r12d
	cmpl	$0, 376(%rbx)
	je	.LBB19_26
# BB#24:
	movq	%r13, %r15
	movq	(%rbp), %rdi
	leaq	8(%rsp), %r8
	movl	20(%rsp), %esi          # 4-byte Reload
	movq	%rbx, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	red_Condensing
	movq	%rbp, %r13
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB19_26
# BB#25:
	movq	(%r13), %r14
	movq	%r15, %rbx
	movq	(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, (%rbx)
	movq	%rbp, (%r13)
	movq	$0, 8(%rsp)
.LBB19_26:                              # %red_ExchangeClauses.exit
	movl	%r12d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end19:
	.size	red_SimpleStaticReductions, .Lfunc_end19-red_SimpleStaticReductions
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_AssignmentEquationDeletion,@function
red_AssignmentEquationDeletion:         # @red_AssignmentEquationDeletion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi222:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi223:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi224:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi225:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi226:
	.cfi_def_cfa_offset 80
.Lcfi227:
	.cfi_offset %rbx, -56
.Lcfi228:
	.cfi_offset %r12, -48
.Lcfi229:
	.cfi_offset %r13, -40
.Lcfi230:
	.cfi_offset %r14, -32
.Lcfi231:
	.cfi_offset %r15, -24
.Lcfi232:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, 4(%rsp)           # 4-byte Spill
	movq	%rcx, %rbx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r13
	callq	clause_ContainsNegativeEquations
	testl	%eax, %eax
	je	.LBB20_14
# BB#1:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	64(%r13), %ebx
	movl	68(%r13), %eax
	leal	-1(%rbx,%rax), %eax
	xorl	%r15d, %r15d
	cmpl	%eax, %ebx
	ja	.LBB20_47
# BB#2:                                 # %.lr.ph162
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB20_3:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %edx
	movl	fol_NOT(%rip), %esi
	cmpl	%edx, %esi
	movl	%edx, %ecx
	jne	.LBB20_5
# BB#4:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB20_5:                               # %clause_LiteralIsEquality.exit
                                        #   in Loop: Header=BB20_3 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB20_13
# BB#6:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	cmpl	%edx, %esi
	jne	.LBB20_8
# BB#7:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB20_8:                               # %clause_GetLiteralAtom.exit147
                                        #   in Loop: Header=BB20_3 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbp
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.LBB20_10
# BB#9:                                 #   in Loop: Header=BB20_3 Depth=1
	movq	%r13, %rdi
	callq	clause_NumberOfSymbolOccurrences
	cmpl	$1, %eax
	je	.LBB20_12
.LBB20_10:                              #   in Loop: Header=BB20_3 Depth=1
	movl	(%rbp), %esi
	testl	%esi, %esi
	jle	.LBB20_13
# BB#11:                                #   in Loop: Header=BB20_3 Depth=1
	movq	%r13, %rdi
	callq	clause_NumberOfSymbolOccurrences
	cmpl	$1, %eax
	jne	.LBB20_13
.LBB20_12:                              #   in Loop: Header=BB20_3 Depth=1
	movl	%ebx, %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
.LBB20_13:                              #   in Loop: Header=BB20_3 Depth=1
	incl	%ebx
	movl	64(%r13), %eax
	movl	68(%r13), %ecx
	leal	-1(%rax,%rcx), %eax
	cmpl	%eax, %ebx
	jbe	.LBB20_3
.LBB20_29:                              # %.loopexit
	testq	%r14, %r14
	je	.LBB20_30
# BB#31:
	cmpl	$0, 88(%r12)
	je	.LBB20_33
# BB#32:
	movq	stdout(%rip), %rcx
	movl	$.L.str.24, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
.LBB20_33:
	cmpl	$0, 36(%r12)
	je	.LBB20_43
# BB#34:
	movq	%r13, %rdi
	callq	clause_Copy
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiterals
	movq	32(%rbp), %rax
	testq	%rax, %rax
	je	.LBB20_36
	.p2align	4, 0x90
.LBB20_35:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB20_35
.LBB20_36:                              # %list_Delete.exit.i
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.LBB20_38
	.p2align	4, 0x90
.LBB20_37:                              # %.lr.ph.i17.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB20_37
.LBB20_38:                              # %list_Delete.exit18.i
	movq	$0, 32(%rbp)
	movq	%r14, 40(%rbp)
	movslq	(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 32(%rbp)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%rbp)
	movl	$18, 76(%rbp)
	movl	4(%rsp), %eax           # 4-byte Reload
	testl	%eax, %eax
	je	.LBB20_40
# BB#39:
	movslq	%eax, %rbx
	movq	32(%rbp), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 32(%rbp)
	movq	40(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	$0, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 40(%rbp)
.LBB20_40:                              # %red_DocumentAssignmentEquationDeletion.exit
	cmpl	$0, 88(%r12)
	je	.LBB20_42
# BB#41:
	movq	%rbp, %rdi
	callq	clause_Print
.LBB20_42:
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rbp, (%rax)
	movl	$1, %r15d
	jmp	.LBB20_47
.LBB20_14:
	xorl	%r15d, %r15d
	testl	%r14d, %r14d
	je	.LBB20_47
# BB#15:
	movq	%r13, %rdi
	callq	clause_ContainsPositiveEquations
	testl	%eax, %eax
	je	.LBB20_47
# BB#16:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movl	68(%r13), %ebx
	movl	72(%r13), %eax
	addl	64(%r13), %ebx
	leal	-1(%rax,%rbx), %eax
	cmpl	%eax, %ebx
	ja	.LBB20_47
# BB#17:                                # %.lr.ph
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB20_18:                              # =>This Inner Loop Header: Depth=1
	movq	56(%r13), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rax
	movl	(%rax), %edx
	movl	fol_NOT(%rip), %esi
	cmpl	%edx, %esi
	movl	%edx, %ecx
	jne	.LBB20_20
# BB#19:                                #   in Loop: Header=BB20_18 Depth=1
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB20_20:                              # %clause_LiteralIsEquality.exit127
                                        #   in Loop: Header=BB20_18 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB20_28
# BB#21:                                #   in Loop: Header=BB20_18 Depth=1
	movq	16(%rax), %rcx
	movq	8(%rcx), %rcx
	cmpl	%edx, %esi
	jne	.LBB20_23
# BB#22:                                #   in Loop: Header=BB20_18 Depth=1
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB20_23:                              # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB20_18 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rbp
	movl	(%rcx), %esi
	testl	%esi, %esi
	jle	.LBB20_25
# BB#24:                                #   in Loop: Header=BB20_18 Depth=1
	movq	%r13, %rdi
	callq	clause_NumberOfSymbolOccurrences
	cmpl	$1, %eax
	je	.LBB20_27
.LBB20_25:                              #   in Loop: Header=BB20_18 Depth=1
	movl	(%rbp), %esi
	testl	%esi, %esi
	jle	.LBB20_28
# BB#26:                                #   in Loop: Header=BB20_18 Depth=1
	movq	%r13, %rdi
	callq	clause_NumberOfSymbolOccurrences
	cmpl	$1, %eax
	jne	.LBB20_28
.LBB20_27:                              #   in Loop: Header=BB20_18 Depth=1
	movl	%ebx, %ebp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
.LBB20_28:                              #   in Loop: Header=BB20_18 Depth=1
	incl	%ebx
	movl	64(%r13), %eax
	movl	72(%r13), %ecx
	addl	68(%r13), %eax
	leal	-1(%rcx,%rax), %eax
	cmpl	%eax, %ebx
	jbe	.LBB20_18
	jmp	.LBB20_29
.LBB20_30:
	xorl	%r15d, %r15d
	jmp	.LBB20_47
.LBB20_43:                              # %.lr.ph.i.preheader
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiterals
	.p2align	4, 0x90
.LBB20_44:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB20_44
# BB#45:                                # %list_Delete.exit
	movl	$1, %r15d
	cmpl	$0, 88(%r12)
	je	.LBB20_47
# BB#46:
	movq	%r13, %rdi
	callq	clause_Print
.LBB20_47:                              # %.thread
	movl	%r15d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end20:
	.size	red_AssignmentEquationDeletion, .Lfunc_end20-red_AssignmentEquationDeletion
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_RewriteRedClause,@function
red_RewriteRedClause:                   # @red_RewriteRedClause
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi233:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi234:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi235:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi236:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi237:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi239:
	.cfi_def_cfa_offset 192
.Lcfi240:
	.cfi_offset %rbx, -56
.Lcfi241:
	.cfi_offset %r12, -48
.Lcfi242:
	.cfi_offset %r13, -40
.Lcfi243:
	.cfi_offset %r14, -32
.Lcfi244:
	.cfi_offset %r15, -24
.Lcfi245:
	.cfi_offset %rbp, -16
	movl	%r9d, 92(%rsp)          # 4-byte Spill
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%rdx, %r15
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movq	%rdi, %r12
	movslq	64(%r12), %rdi
	movl	68(%r12), %ebp
	movl	72(%r12), %esi
	leal	(%rbp,%rdi), %ecx
	addl	%esi, %ecx
	movl	36(%r15), %edx
	movl	%edx, 44(%rsp)          # 4-byte Spill
	cmpl	$1, %ecx
	movq	%r12, 56(%rsp)          # 8-byte Spill
	movq	%r15, 48(%rsp)          # 8-byte Spill
	jne	.LBB21_5
# BB#1:
	movq	56(%r12), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	jne	.LBB21_3
# BB#2:
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB21_3:                               # %clause_GetLiteralAtom.exit.i
	testl	%edi, %edi
	jg	.LBB21_119
# BB#61:
	movq	%rbp, %rax
	cmpq	$0, 16(%rax)
	movl	$0, %eax
	je	.LBB21_120
# BB#62:
	movl	red_STAMPID(%rip), %edi
	callq	term_StampOverflow
	testl	%eax, %eax
	je	.LBB21_64
# BB#63:
	movq	56(%r12), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rax
	movl	$0, 24(%rax)
.LBB21_64:
	incl	term_STAMP(%rip)
	movl	stack_POINTER(%rip), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	%r12, %r14
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB21_65:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_70 Depth 2
                                        #       Child Loop BB21_73 Depth 3
                                        #         Child Loop BB21_74 Depth 4
                                        #           Child Loop BB21_79 Depth 5
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	16(%rax), %rdi
	callq	sharing_PushListOnStackNoStamps
	movl	stack_POINTER(%rip), %eax
	cmpl	64(%rsp), %eax          # 4-byte Folded Reload
	je	.LBB21_108
# BB#66:                                # %.lr.ph247.i.preheader
                                        #   in Loop: Header=BB21_65 Depth=1
	xorl	%ebx, %ebx
	jmp	.LBB21_70
.LBB21_67:                              #   in Loop: Header=BB21_70 Depth=2
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB21_91
.LBB21_68:                              #   in Loop: Header=BB21_70 Depth=2
	movl	12(%rbp), %eax
	cmpl	92(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB21_98
# BB#69:                                #   in Loop: Header=BB21_70 Depth=2
	cmpl	12(%rdi), %eax
	movq	%rdi, %r14
	jbe	.LBB21_91
	jmp	.LBB21_98
	.p2align	4, 0x90
.LBB21_70:                              # %.lr.ph247.i
                                        #   Parent Loop BB21_65 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_73 Depth 3
                                        #         Child Loop BB21_74 Depth 4
                                        #           Child Loop BB21_79 Depth 5
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rdx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	callq	st_ExistGen
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB21_87
# BB#71:                                # %.lr.ph247.i
                                        #   in Loop: Header=BB21_70 Depth=2
	testl	%ebx, %ebx
	jne	.LBB21_87
# BB#72:                                # %.preheader350.preheader
                                        #   in Loop: Header=BB21_70 Depth=2
	movq	%r14, 80(%rsp)          # 8-byte Spill
.LBB21_73:                              # %.preheader350
                                        #   Parent Loop BB21_65 Depth=1
                                        #     Parent Loop BB21_70 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB21_74 Depth 4
                                        #           Child Loop BB21_79 Depth 5
	movq	8(%r13), %r14
	testq	%r14, %r14
	je	.LBB21_85
	.p2align	4, 0x90
.LBB21_74:                              # %.lr.ph235.i
                                        #   Parent Loop BB21_65 Depth=1
                                        #     Parent Loop BB21_70 Depth=2
                                        #       Parent Loop BB21_73 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB21_79 Depth 5
	movq	8(%r14), %rbx
	movl	fol_EQUALITY(%rip), %eax
	cmpl	(%rbx), %eax
	jne	.LBB21_84
# BB#75:                                #   in Loop: Header=BB21_74 Depth=4
	movq	16(%rbx), %rax
	movq	8(%rax), %r15
	cmpq	%r15, %r13
	jne	.LBB21_77
# BB#76:                                #   in Loop: Header=BB21_74 Depth=4
	movq	(%rax), %rax
	movq	8(%rax), %r15
.LBB21_77:                              #   in Loop: Header=BB21_74 Depth=4
	movq	%rbx, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %r12
	testq	%r12, %r12
	je	.LBB21_84
	.p2align	4, 0x90
.LBB21_79:                              # %.lr.ph.i
                                        #   Parent Loop BB21_65 Depth=1
                                        #     Parent Loop BB21_70 Depth=2
                                        #       Parent Loop BB21_73 Depth=3
                                        #         Parent Loop BB21_74 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	8(%r12), %rax
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	je	.LBB21_78
# BB#80:                                #   in Loop: Header=BB21_79 Depth=5
	movq	16(%rax), %rbp
	movl	68(%rbp), %ecx
	addl	64(%rbp), %ecx
	addl	72(%rbp), %ecx
	cmpl	$1, %ecx
	jne	.LBB21_78
# BB#81:                                #   in Loop: Header=BB21_79 Depth=5
	cmpl	$0, 8(%rax)
	je	.LBB21_83
# BB#82:                                #   in Loop: Header=BB21_79 Depth=5
	movq	16(%rbx), %rcx
	xorl	%eax, %eax
	cmpq	8(%rcx), %r13
	sete	%al
	testl	%eax, %eax
	je	.LBB21_78
	jmp	.LBB21_89
.LBB21_83:                              # %.thread.thread.i
                                        #   in Loop: Header=BB21_79 Depth=5
	movl	52(%rbp), %edi
	callq	term_StartMaxRenaming
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	term_Rename
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	%r13, %rsi
	movq	%rdi, %rdx
	movq	%r15, %rcx
	movq	48(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r9           # 8-byte Reload
	callq	ord_ContGreater
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	testl	%eax, %eax
	jne	.LBB21_89
	.p2align	4, 0x90
.LBB21_78:                              #   in Loop: Header=BB21_79 Depth=5
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB21_79
.LBB21_84:                              # %.critedge2.i
                                        #   in Loop: Header=BB21_74 Depth=4
	movq	(%r14), %r14
	testq	%r14, %r14
	movq	48(%rsp), %r15          # 8-byte Reload
	jne	.LBB21_74
.LBB21_85:                              # %.critedge1.thread.i
                                        #   in Loop: Header=BB21_73 Depth=3
	callq	st_NextCandidate
	movq	%rax, %r13
	testq	%r13, %r13
	jne	.LBB21_73
# BB#86:                                # %.critedge.i.thread
                                        #   in Loop: Header=BB21_70 Depth=2
	callq	st_CancelExistRetrieval
	movq	80(%rsp), %r14          # 8-byte Reload
	jmp	.LBB21_88
	.p2align	4, 0x90
.LBB21_87:                              # %.critedge.i
                                        #   in Loop: Header=BB21_70 Depth=2
	callq	st_CancelExistRetrieval
	testl	%ebx, %ebx
	jne	.LBB21_106
.LBB21_88:                              #   in Loop: Header=BB21_70 Depth=2
	movl	term_STAMP(%rip), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 24(%rcx)
	xorl	%ebx, %ebx
	jmp	.LBB21_106
	.p2align	4, 0x90
.LBB21_89:                              #   in Loop: Header=BB21_70 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	cmpq	%rdi, %r14
	je	.LBB21_97
# BB#90:                                #   in Loop: Header=BB21_70 Depth=2
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB21_91:                              # %clause_GetLiteralAtom.exit203.i
                                        #   in Loop: Header=BB21_70 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB21_94
# BB#92:                                #   in Loop: Header=BB21_70 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 52(%rax)
	movl	12(%rsp), %eax          # 4-byte Reload
	je	.LBB21_94
# BB#93:                                #   in Loop: Header=BB21_70 Depth=2
	movq	stdout(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB21_94:                              #   in Loop: Header=BB21_70 Depth=2
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	je	.LBB21_101
# BB#95:                                #   in Loop: Header=BB21_70 Depth=2
	testl	%eax, %eax
	je	.LBB21_100
# BB#96:                                #   in Loop: Header=BB21_70 Depth=2
	movq	32(%r14), %r12
	movq	(%r12), %rax
	movq	%r14, %rbx
	movslq	8(%rax), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 32(%rbx)
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	$0, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	movslq	(%rbp), %r12
	movq	32(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	%r14, (%rax)
	movq	%rbx, %r14
	movq	%rax, 32(%r14)
	movq	40(%r14), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	$0, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, 40(%r14)
	jmp	.LBB21_101
.LBB21_97:                              #   in Loop: Header=BB21_70 Depth=2
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB21_68
.LBB21_98:                              #   in Loop: Header=BB21_70 Depth=2
	callq	clause_Copy
	movq	%rax, %r14
	movq	56(%r14), %rax
	movq	(%rax), %rax
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB21_67
# BB#99:                                #   in Loop: Header=BB21_70 Depth=2
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB21_91
.LBB21_100:                             #   in Loop: Header=BB21_70 Depth=2
	xorl	%esi, %esi
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rbp, %rdx
	callq	red_DocumentRewriting
.LBB21_101:                             #   in Loop: Header=BB21_70 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rbx
	movq	%r15, %rdi
	callq	term_Copy
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	cont_ApplyBindingsModuloMatching
	movq	%rax, %rbx
	movq	cont_RIGHTCONTEXT(%rip), %rdi
	callq	cont_BindingsAreRenamingModuloMatching
	testl	%eax, %eax
	je	.LBB21_103
# BB#102:                               #   in Loop: Header=BB21_70 Depth=2
	movq	%rbx, %rdi
	callq	term_SetTermSubtermStamp
.LBB21_103:                             #   in Loop: Header=BB21_70 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	term_ReplaceSubtermBy
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	%rbx, %rdi
	callq	term_Delete
	movl	64(%rsp), %eax          # 4-byte Reload
	movl	%eax, stack_POINTER(%rip)
	movq	48(%rsp), %r15          # 8-byte Reload
	cmpl	$0, 52(%r15)
	je	.LBB21_105
# BB#104:                               #   in Loop: Header=BB21_70 Depth=2
	movl	(%rbp), %esi
	movl	$.L.str.29, %edi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
.LBB21_105:                             # %.critedge.i.thread331
                                        #   in Loop: Header=BB21_70 Depth=2
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r14)
	callq	st_CancelExistRetrieval
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	movl	$1, %ebx
.LBB21_106:                             # %.backedge.i
                                        #   in Loop: Header=BB21_70 Depth=2
	movl	stack_POINTER(%rip), %eax
	cmpl	64(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB21_70
# BB#107:                               # %.loopexit.i
                                        #   in Loop: Header=BB21_65 Depth=1
	testl	%ebx, %ebx
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	jne	.LBB21_65
.LBB21_108:                             # %.loopexit.thread.i
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB21_116
# BB#109:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	clause_OrientEqualities
	movq	%r14, %rdi
	callq	clause_Normalize
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	clause_SetMaxLitFlags
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r14)
	movq	%r14, %rdi
	callq	clause_UpdateMaxVar
	cmpq	%r12, %r14
	je	.LBB21_111
# BB#110:
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	clause_OrientEqualities
	movq	%r12, %rdi
	callq	clause_Normalize
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	clause_SetMaxLitFlags
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r12)
	movq	%r12, %rdi
	callq	clause_UpdateMaxVar
.LBB21_111:
	cmpl	$0, 52(%r15)
	je	.LBB21_113
# BB#112:
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	clause_Print
.LBB21_113:
	cmpq	%r12, %r14
	je	.LBB21_118
# BB#114:
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%r14, (%rax)
	movl	12(%rsp), %eax          # 4-byte Reload
	jmp	.LBB21_120
.LBB21_5:                               # %.preheader213
	xorl	%eax, %eax
	cmpl	%ecx, %edi
	jge	.LBB21_120
# BB#6:                                 # %.lr.ph296
	addl	%esi, %ebp
	addl	%edi, %ebp
	movq	%rbp, 120(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rbx       # imm = 0x100000000
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB21_7:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_19 Depth 2
                                        #       Child Loop BB21_22 Depth 3
                                        #         Child Loop BB21_25 Depth 4
                                        #           Child Loop BB21_29 Depth 5
                                        #             Child Loop BB21_30 Depth 6
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	56(%r12), %rax
	movq	(%rax,%rdi,8), %rax
	movq	24(%rax), %rdx
	movl	(%rdx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movq	%rdx, %rsi
	jne	.LBB21_9
# BB#8:                                 #   in Loop: Header=BB21_7 Depth=1
	movq	16(%rsi), %rdx
	movq	8(%rdx), %rdx
.LBB21_9:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB21_7 Depth=1
	cmpq	$0, 16(%rdx)
	je	.LBB21_53
# BB#10:                                # %.preheader
                                        #   in Loop: Header=BB21_7 Depth=1
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	cmpl	%eax, %ecx
	je	.LBB21_13
	jmp	.LBB21_14
	.p2align	4, 0x90
.LBB21_12:                              # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB21_7 Depth=1
	movq	56(%r12), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rax
	movq	%rax, %rsi
	movl	(%rax), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB21_14
.LBB21_13:                              #   in Loop: Header=BB21_7 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rax
.LBB21_15:                              # %clause_GetLiteralAtom.exit190
                                        #   in Loop: Header=BB21_7 Depth=1
	movl	stack_POINTER(%rip), %ebp
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	16(%rax), %rdi
	callq	sharing_PushListReverseOnStack
	movl	stack_POINTER(%rip), %eax
	movl	%ebp, 80(%rsp)          # 4-byte Spill
	cmpl	%ebp, %eax
	je	.LBB21_52
# BB#16:                                # %.lr.ph289.preheader
                                        #   in Loop: Header=BB21_7 Depth=1
	xorl	%r14d, %r14d
	jmp	.LBB21_19
.LBB21_17:                              #   in Loop: Header=BB21_19 Depth=2
	movl	12(%r14), %eax
	cmpl	92(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB21_38
# BB#18:                                #   in Loop: Header=BB21_19 Depth=2
	movq	56(%rsp), %r12          # 8-byte Reload
	cmpl	12(%r12), %eax
	jbe	.LBB21_41
	jmp	.LBB21_38
	.p2align	4, 0x90
.LBB21_19:                              # %.lr.ph289
                                        #   Parent Loop BB21_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB21_22 Depth 3
                                        #         Child Loop BB21_25 Depth 4
                                        #           Child Loop BB21_29 Depth 5
                                        #             Child Loop BB21_30 Depth 6
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rbp
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	movq	%rbp, %rdx
	callq	st_ExistGen
	testq	%rax, %rax
	je	.LBB21_51
# BB#20:                                # %.lr.ph289
                                        #   in Loop: Header=BB21_19 Depth=2
	testl	%r14d, %r14d
	jne	.LBB21_51
# BB#21:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB21_19 Depth=2
	movq	%rbp, 128(%rsp)         # 8-byte Spill
	movq	%r12, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB21_22:                              # %.lr.ph
                                        #   Parent Loop BB21_7 Depth=1
                                        #     Parent Loop BB21_19 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB21_25 Depth 4
                                        #           Child Loop BB21_29 Depth 5
                                        #             Child Loop BB21_30 Depth 6
	movq	%rax, 64(%rsp)          # 8-byte Spill
	cmpl	$0, (%rax)
	jg	.LBB21_34
# BB#23:                                #   in Loop: Header=BB21_22 Depth=3
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	8(%rax), %r12
	testq	%r12, %r12
	je	.LBB21_34
	.p2align	4, 0x90
.LBB21_25:                              # %.lr.ph280
                                        #   Parent Loop BB21_7 Depth=1
                                        #     Parent Loop BB21_19 Depth=2
                                        #       Parent Loop BB21_22 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB21_29 Depth 5
                                        #             Child Loop BB21_30 Depth 6
	movq	8(%r12), %rcx
	movl	fol_EQUALITY(%rip), %eax
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	cmpl	(%rcx), %eax
	jne	.LBB21_24
# BB#26:                                #   in Loop: Header=BB21_25 Depth=4
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	cmpq	%rcx, 8(%rax)
	jne	.LBB21_24
# BB#27:                                #   in Loop: Header=BB21_25 Depth=4
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	sharing_NAtomDataList
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB21_24
	.p2align	4, 0x90
.LBB21_29:                              # %.lr.ph268
                                        #   Parent Loop BB21_7 Depth=1
                                        #     Parent Loop BB21_19 Depth=2
                                        #       Parent Loop BB21_22 Depth=3
                                        #         Parent Loop BB21_25 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB21_30 Depth 6
	movq	8(%rbp), %rax
	movq	16(%rax), %r14
	movq	56(%r14), %rcx
	movl	$-1, %r13d
	movq	%rsi, %r15
	.p2align	4, 0x90
.LBB21_30:                              #   Parent Loop BB21_7 Depth=1
                                        #     Parent Loop BB21_19 Depth=2
                                        #       Parent Loop BB21_22 Depth=3
                                        #         Parent Loop BB21_25 Depth=4
                                        #           Parent Loop BB21_29 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	addq	%rbx, %r15
	incl	%r13d
	cmpq	%rax, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB21_30
# BB#31:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB21_29 Depth=5
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %edx
	cmpl	(%rcx), %edx
	je	.LBB21_28
# BB#32:                                #   in Loop: Header=BB21_29 Depth=5
	cmpl	$0, 8(%rax)
	je	.LBB21_28
# BB#33:                                #   in Loop: Header=BB21_29 Depth=5
	movq	%r14, %rdi
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%r13d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	subs_SubsumesBasic
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	testl	%eax, %eax
	jne	.LBB21_36
.LBB21_28:                              #   in Loop: Header=BB21_29 Depth=5
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB21_29
	.p2align	4, 0x90
.LBB21_24:                              # %.critedge2
                                        #   in Loop: Header=BB21_25 Depth=4
	movq	(%r12), %r12
	testq	%r12, %r12
	jne	.LBB21_25
.LBB21_34:                              # %.critedge1.thread
                                        #   in Loop: Header=BB21_22 Depth=3
	callq	st_NextCandidate
	testq	%rax, %rax
	jne	.LBB21_22
# BB#35:                                #   in Loop: Header=BB21_19 Depth=2
	xorl	%r14d, %r14d
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r12          # 8-byte Reload
	jmp	.LBB21_51
	.p2align	4, 0x90
.LBB21_36:                              #   in Loop: Header=BB21_19 Depth=2
	movq	24(%rsp), %r12          # 8-byte Reload
	cmpq	56(%rsp), %r12          # 8-byte Folded Reload
	jne	.LBB21_41
# BB#37:                                #   in Loop: Header=BB21_19 Depth=2
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	je	.LBB21_17
.LBB21_38:                              #   in Loop: Header=BB21_19 Depth=2
	movq	56(%rsp), %rdi          # 8-byte Reload
	callq	clause_Copy
	movq	%rax, %r12
	movq	56(%r12), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	movq	%rcx, %rax
	jne	.LBB21_40
# BB#39:                                #   in Loop: Header=BB21_19 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB21_40:                              # %clause_GetLiteralAtom.exit202
                                        #   in Loop: Header=BB21_19 Depth=2
	movq	%rax, 96(%rsp)          # 8-byte Spill
.LBB21_41:                              # %clause_GetLiteralAtom.exit202
                                        #   in Loop: Header=BB21_19 Depth=2
	movl	12(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB21_44
# BB#42:                                #   in Loop: Header=BB21_19 Depth=2
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 52(%rax)
	movl	12(%rsp), %eax          # 4-byte Reload
	je	.LBB21_44
# BB#43:                                #   in Loop: Header=BB21_19 Depth=2
	movq	stdout(%rip), %rcx
	movl	$.L.str.27, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB21_44:                              #   in Loop: Header=BB21_19 Depth=2
	cmpl	$0, 44(%rsp)            # 4-byte Folded Reload
	je	.LBB21_48
# BB#45:                                #   in Loop: Header=BB21_19 Depth=2
	testl	%eax, %eax
	je	.LBB21_47
# BB#46:                                #   in Loop: Header=BB21_19 Depth=2
	movq	32(%r12), %rbp
	movq	(%rbp), %rax
	movslq	8(%rax), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 32(%r12)
	movq	40(%r12), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 40(%r12)
	movslq	(%r14), %rbp
	movq	32(%r12), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 32(%r12)
	sarq	$32, %r15
	movq	40(%r12), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbp, (%rax)
	movq	%rax, 40(%r12)
	jmp	.LBB21_48
.LBB21_47:                              #   in Loop: Header=BB21_19 Depth=2
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	movl	%r13d, %ecx
	callq	red_DocumentRewriting
.LBB21_48:                              #   in Loop: Header=BB21_19 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rbp
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movl	$1, %edx
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	cont_ApplyBindingsModuloMatching
	movq	%rax, %rbp
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	128(%rsp), %rsi         # 8-byte Reload
	movq	%rbp, %rdx
	callq	term_ReplaceSubtermBy
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	%rbp, %rdi
	callq	term_Delete
	movl	80(%rsp), %eax          # 4-byte Reload
	movl	%eax, stack_POINTER(%rip)
	movq	48(%rsp), %r15          # 8-byte Reload
	cmpl	$0, 52(%r15)
	je	.LBB21_50
# BB#49:                                #   in Loop: Header=BB21_19 Depth=2
	movl	(%r14), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	printf
.LBB21_50:                              # %.critedge1
                                        #   in Loop: Header=BB21_19 Depth=2
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r12)
	movl	$1, 12(%rsp)            # 4-byte Folded Spill
	movl	$1, %r14d
.LBB21_51:                              # %.critedge
                                        #   in Loop: Header=BB21_19 Depth=2
	callq	st_CancelExistRetrieval
	movl	stack_POINTER(%rip), %eax
	cmpl	80(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB21_19
	jmp	.LBB21_11
	.p2align	4, 0x90
.LBB21_14:                              #   in Loop: Header=BB21_7 Depth=1
	movq	%rsi, %rax
	jmp	.LBB21_15
	.p2align	4, 0x90
.LBB21_11:                              # %._crit_edge
                                        #   in Loop: Header=BB21_7 Depth=1
	testl	%r14d, %r14d
	jne	.LBB21_12
.LBB21_52:                              #   in Loop: Header=BB21_7 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB21_54
	.p2align	4, 0x90
.LBB21_53:                              #   in Loop: Header=BB21_7 Depth=1
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB21_54:                              # %.loopexit
                                        #   in Loop: Header=BB21_7 Depth=1
	incq	%rdi
	cmpl	120(%rsp), %edi         # 4-byte Folded Reload
	jne	.LBB21_7
# BB#55:                                # %._crit_edge297
	testl	%eax, %eax
	movq	72(%rsp), %rbx          # 8-byte Reload
	je	.LBB21_119
# BB#56:
	movl	%eax, %r14d
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	clause_OrientEqualities
	movq	%r12, %rdi
	callq	clause_Normalize
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	clause_SetMaxLitFlags
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r12)
	movq	%r12, %rdi
	callq	clause_UpdateMaxVar
	cmpl	$0, 52(%r15)
	je	.LBB21_58
# BB#57:
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
.LBB21_58:
	movq	56(%rsp), %rbx          # 8-byte Reload
	cmpq	%rbx, %r12
	je	.LBB21_60
# BB#59:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	callq	clause_OrientEqualities
	movq	%rbx, %rdi
	callq	clause_Normalize
	movq	%rbx, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbx)
	movq	%rbx, %rdi
	callq	clause_UpdateMaxVar
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	%r12, (%rax)
.LBB21_60:                              # %red_RewriteRedUnitClause.exit
	movl	%r14d, %eax
	jmp	.LBB21_120
.LBB21_116:
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	je	.LBB21_119
# BB#117:
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	clause_OrientEqualities
	movq	%r14, %rdi
	callq	clause_Normalize
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbx, %rdx
	callq	clause_SetMaxLitFlags
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r14)
	movq	%r14, %rdi
	callq	clause_UpdateMaxVar
.LBB21_119:
	xorl	%eax, %eax
.LBB21_120:                             # %red_RewriteRedUnitClause.exit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_118:
	movl	12(%rsp), %eax          # 4-byte Reload
	jmp	.LBB21_120
.Lfunc_end21:
	.size	red_RewriteRedClause, .Lfunc_end21-red_RewriteRedClause
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_ContextualRewriting,@function
red_ContextualRewriting:                # @red_ContextualRewriting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi246:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi247:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi248:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi249:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi250:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi251:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi252:
	.cfi_def_cfa_offset 224
.Lcfi253:
	.cfi_offset %rbx, -56
.Lcfi254:
	.cfi_offset %r12, -48
.Lcfi255:
	.cfi_offset %r13, -40
.Lcfi256:
	.cfi_offset %r14, -32
.Lcfi257:
	.cfi_offset %r15, -24
.Lcfi258:
	.cfi_offset %rbp, -16
	movl	%ecx, 60(%rsp)          # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movslq	64(%rsi), %rbx
	movl	72(%rsi), %eax
	movl	68(%rsi), %ecx
	addl	%ebx, %ecx
	leal	-1(%rax,%rcx), %ebp
	xorl	%eax, %eax
	cmpl	%ebp, %ebx
	jg	.LBB22_89
# BB#1:                                 # %.lr.ph295
	movq	%r8, 128(%rsp)          # 8-byte Spill
	movq	104(%rdi), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	112(%rdi), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	36(%rcx), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movl	%edx, %ecx
	orl	$1, %ecx
	leaq	32(%rdi), %rdx
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	leaq	48(%rdi), %rdi
	cmpl	$3, %ecx
	cmoveq	%rdx, %rdi
	movq	(%rdi), %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movslq	%ebp, %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movabsq	$4294967296, %rbp       # imm = 0x100000000
	movl	$0, 16(%rsp)            # 4-byte Folded Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB22_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_12 Depth 2
                                        #       Child Loop BB22_73 Depth 3
                                        #         Child Loop BB22_17 Depth 4
                                        #           Child Loop BB22_21 Depth 5
                                        #             Child Loop BB22_22 Depth 6
                                        #           Child Loop BB22_47 Depth 5
                                        #           Child Loop BB22_54 Depth 5
                                        #           Child Loop BB22_65 Depth 5
                                        #       Child Loop BB22_78 Depth 3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rdx
	movl	(%rdx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movq	%rdx, %rsi
	jne	.LBB22_4
# BB#3:                                 #   in Loop: Header=BB22_2 Depth=1
	movq	16(%rsi), %rdx
	movq	8(%rdx), %rdx
.LBB22_4:                               # %clause_GetLiteralAtom.exit
                                        #   in Loop: Header=BB22_2 Depth=1
	cmpq	$0, 16(%rdx)
	je	.LBB22_81
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB22_2 Depth=1
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	cmpl	%eax, %ecx
	je	.LBB22_8
	jmp	.LBB22_9
	.p2align	4, 0x90
.LBB22_7:                               # %.loopexit._crit_edge
                                        #   in Loop: Header=BB22_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %rax
	movq	%rax, %rsi
	movl	(%rax), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	jne	.LBB22_9
.LBB22_8:                               #   in Loop: Header=BB22_2 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rax
.LBB22_10:                              # %clause_GetLiteralAtom.exit222
                                        #   in Loop: Header=BB22_2 Depth=1
	movl	stack_POINTER(%rip), %ebx
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	16(%rax), %rdi
	callq	sharing_PushListReverseOnStack
	movl	stack_POINTER(%rip), %eax
	movl	%ebx, 68(%rsp)          # 4-byte Spill
	cmpl	%ebx, %eax
	je	.LBB22_80
# BB#11:                                # %.lr.ph284.preheader
                                        #   in Loop: Header=BB22_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB22_12:                              # %.lr.ph284
                                        #   Parent Loop BB22_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB22_73 Depth 3
                                        #         Child Loop BB22_17 Depth 4
                                        #           Child Loop BB22_21 Depth 5
                                        #             Child Loop BB22_22 Depth 6
                                        #           Child Loop BB22_47 Depth 5
                                        #           Child Loop BB22_54 Depth 5
                                        #           Child Loop BB22_65 Depth 5
                                        #       Child Loop BB22_78 Depth 3
	decl	%eax
	movl	%eax, stack_POINTER(%rip)
	movq	stack_STACK(,%rax,8), %rdx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	callq	st_GetGen
	movq	%rax, %r14
	testq	%r14, %r14
	sete	%cl
	setne	%al
	movl	%ebx, %esi
	testl	%esi, %esi
	jne	.LBB22_76
# BB#13:                                # %.lr.ph284
                                        #   in Loop: Header=BB22_12 Depth=2
	testb	%al, %al
	jne	.LBB22_73
.LBB22_76:                              #   in Loop: Header=BB22_12 Depth=2
	movq	%r14, %rax
	jmp	.LBB22_77
	.p2align	4, 0x90
.LBB22_15:                              #   in Loop: Header=BB22_73 Depth=3
	movq	8(%r12), %r15
	testq	%r15, %r15
	je	.LBB22_74
# BB#16:                                # %.lr.ph262.preheader
                                        #   in Loop: Header=BB22_73 Depth=3
	movq	%r14, 120(%rsp)         # 8-byte Spill
	movq	%r12, 96(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB22_17:                              # %.lr.ph262
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        #       Parent Loop BB22_73 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB22_21 Depth 5
                                        #             Child Loop BB22_22 Depth 6
                                        #           Child Loop BB22_47 Depth 5
                                        #           Child Loop BB22_54 Depth 5
                                        #           Child Loop BB22_65 Depth 5
	movq	8(%r15), %r13
	movl	fol_EQUALITY(%rip), %eax
	xorl	%esi, %esi
	cmpl	(%r13), %eax
	jne	.LBB22_30
# BB#18:                                #   in Loop: Header=BB22_17 Depth=4
	movq	16(%r13), %rax
	cmpq	%r12, 8(%rax)
	jne	.LBB22_30
# BB#19:                                #   in Loop: Header=BB22_17 Depth=4
	movq	%r13, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %rbx
	xorl	%esi, %esi
	testq	%rbx, %rbx
	je	.LBB22_30
# BB#20:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB22_17 Depth=4
	movq	%r13, 88(%rsp)          # 8-byte Spill
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	.p2align	4, 0x90
.LBB22_21:                              # %.lr.ph
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        #       Parent Loop BB22_73 Depth=3
                                        #         Parent Loop BB22_17 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB22_22 Depth 6
	movq	8(%rbx), %rsi
	movq	16(%rsi), %r14
	movq	56(%r14), %rax
	movl	$-1, %r13d
	movq	%rdx, %r12
	.p2align	4, 0x90
.LBB22_22:                              #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        #       Parent Loop BB22_73 Depth=3
                                        #         Parent Loop BB22_17 Depth=4
                                        #           Parent Loop BB22_21 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	addq	%rbp, %r12
	incl	%r13d
	cmpq	%rsi, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB22_22
# BB#23:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB22_21 Depth=5
	movq	$0, 72(%rsp)
	movq	24(%rsi), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	je	.LBB22_28
# BB#24:                                #   in Loop: Header=BB22_21 Depth=5
	testb	$2, (%rsi)
	je	.LBB22_28
# BB#25:                                #   in Loop: Header=BB22_21 Depth=5
	cmpl	$0, 8(%rsi)
	je	.LBB22_28
# BB#26:                                #   in Loop: Header=BB22_21 Depth=5
	movq	%r14, %rdi
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	112(%rsp), %rcx         # 8-byte Reload
	callq	red_LeftTermOfEquationIsStrictlyMaximalTerm
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	testl	%eax, %eax
	je	.LBB22_28
# BB#27:                                #   in Loop: Header=BB22_21 Depth=5
	movq	152(%rsp), %rdi         # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%r14, %r8
	movl	%r13d, %r9d
	leaq	72(%rsp), %rax
	pushq	%rax
.Lcfi259:
	.cfi_adjust_cfa_offset 8
	pushq	168(%rsp)               # 8-byte Folded Reload
.Lcfi260:
	.cfi_adjust_cfa_offset 8
	callq	red_CRwTautologyCheck
	movabsq	$-4294967296, %rdx      # imm = 0xFFFFFFFF00000000
	addq	$16, %rsp
.Lcfi261:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB22_32
	.p2align	4, 0x90
.LBB22_28:                              #   in Loop: Header=BB22_21 Depth=5
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB22_21
# BB#29:                                #   in Loop: Header=BB22_17 Depth=4
	movq	120(%rsp), %r14         # 8-byte Reload
	movq	96(%rsp), %r12          # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB22_30:                              #   in Loop: Header=BB22_17 Depth=4
	testl	%esi, %esi
	jne	.LBB22_74
.LBB22_31:                              #   in Loop: Header=BB22_17 Depth=4
	movq	(%r15), %r15
	testq	%r15, %r15
	jne	.LBB22_17
	jmp	.LBB22_74
.LBB22_32:                              #   in Loop: Header=BB22_17 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	48(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB22_37
# BB#33:                                #   in Loop: Header=BB22_17 Depth=4
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	je	.LBB22_56
.LBB22_34:                              #   in Loop: Header=BB22_17 Depth=4
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	clause_Copy
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rax), %rax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB22_36
# BB#35:                                #   in Loop: Header=BB22_17 Depth=4
	movq	16(%rcx), %rax
	movq	8(%rax), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB22_40
	jmp	.LBB22_38
.LBB22_36:                              #   in Loop: Header=BB22_17 Depth=4
	movq	%rcx, 80(%rsp)          # 8-byte Spill
.LBB22_37:                              # %clause_GetLiteralAtom.exit235
                                        #   in Loop: Header=BB22_17 Depth=4
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB22_40
.LBB22_38:                              #   in Loop: Header=BB22_17 Depth=4
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 56(%rax)
	je	.LBB22_40
# BB#39:                                #   in Loop: Header=BB22_17 Depth=4
	movq	stdout(%rip), %rcx
	movl	$.L.str.31, %edi
	movl	$17, %esi
	movl	$1, %edx
	callq	fwrite
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
.LBB22_40:                              #   in Loop: Header=BB22_17 Depth=4
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	je	.LBB22_63
# BB#41:                                #   in Loop: Header=BB22_17 Depth=4
	movq	72(%rsp), %rax
	testq	%rax, %rax
	je	.LBB22_43
# BB#42:                                #   in Loop: Header=BB22_17 Depth=4
	movq	32(%rax), %r8
	movq	40(%rax), %r9
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	jne	.LBB22_44
	jmp	.LBB22_49
.LBB22_43:                              #   in Loop: Header=BB22_17 Depth=4
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB22_49
.LBB22_44:                              #   in Loop: Header=BB22_17 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	32(%rax), %rcx
	movq	(%rcx), %rax
	movq	8(%rax), %rax
	testq	%r8, %r8
	je	.LBB22_50
# BB#45:                                #   in Loop: Header=BB22_17 Depth=4
	testq	%rcx, %rcx
	je	.LBB22_51
# BB#46:                                # %.preheader.i.i.preheader
                                        #   in Loop: Header=BB22_17 Depth=4
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB22_47:                              # %.preheader.i.i
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        #       Parent Loop BB22_73 Depth=3
                                        #         Parent Loop BB22_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB22_47
# BB#48:                                #   in Loop: Header=BB22_17 Depth=4
	movq	%rcx, (%rdx)
	jmp	.LBB22_51
.LBB22_49:                              #   in Loop: Header=BB22_17 Depth=4
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	%r14, %rdx
	movl	%r13d, %ecx
	callq	red_DocumentContextualRewriting
	jmp	.LBB22_63
.LBB22_50:                              #   in Loop: Header=BB22_17 Depth=4
	movq	%rcx, %r8
.LBB22_51:                              # %list_Nconc.exit.i
                                        #   in Loop: Header=BB22_17 Depth=4
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%r8, 32(%rcx)
	movq	40(%rcx), %rcx
	testq	%r9, %r9
	je	.LBB22_61
# BB#52:                                #   in Loop: Header=BB22_17 Depth=4
	testq	%rcx, %rcx
	je	.LBB22_60
# BB#53:                                # %.preheader.i22.i.preheader
                                        #   in Loop: Header=BB22_17 Depth=4
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB22_54:                              # %.preheader.i22.i
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        #       Parent Loop BB22_73 Depth=3
                                        #         Parent Loop BB22_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB22_54
# BB#55:                                #   in Loop: Header=BB22_17 Depth=4
	movq	%rcx, (%rdx)
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	32(%rbx), %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r9, %rcx
	jmp	.LBB22_62
.LBB22_56:                              #   in Loop: Header=BB22_17 Depth=4
	movl	12(%r14), %ecx
	cmpl	60(%rsp), %ecx          # 4-byte Folded Reload
	ja	.LBB22_34
# BB#57:                                #   in Loop: Header=BB22_17 Depth=4
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	12(%rax), %eax
	cmpl	%eax, %ecx
	ja	.LBB22_34
# BB#58:                                #   in Loop: Header=BB22_17 Depth=4
	movq	72(%rsp), %rcx
	movl	12(%rcx), %ecx
	cmpl	%eax, %ecx
	ja	.LBB22_34
# BB#59:                                #   in Loop: Header=BB22_17 Depth=4
	cmpl	60(%rsp), %ecx          # 4-byte Folded Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jbe	.LBB22_37
	jmp	.LBB22_34
.LBB22_60:                              #   in Loop: Header=BB22_17 Depth=4
	movq	%r9, %rcx
.LBB22_61:                              #   in Loop: Header=BB22_17 Depth=4
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%r8, 40(%rsp)           # 8-byte Spill
.LBB22_62:                              # %red_DocumentFurtherCRw.exit
                                        #   in Loop: Header=BB22_17 Depth=4
	movq	%rcx, 40(%rbx)
	cltq
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 32(%rbx)
	movq	40(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 40(%rbx)
	movslq	(%r14), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	32(%rbx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 8(%rax)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 32(%rbx)
	sarq	$32, %r12
	movq	40(%rbx), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r12, 8(%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 40(%rbx)
.LBB22_63:                              #   in Loop: Header=BB22_17 Depth=4
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	104(%rsp), %rdx         # 8-byte Reload
	callq	unify_MatchBindings
	movq	cont_LEFTCONTEXT(%rip), %rbx
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	callq	term_Copy
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	cont_ApplyBindingsModuloMatching
	movl	cont_BINDINGS(%rip), %esi
	testl	%esi, %esi
	xorps	%xmm0, %xmm0
	jle	.LBB22_66
# BB#64:                                # %.lr.ph.i239.preheader
                                        #   in Loop: Header=BB22_17 Depth=4
	incl	%esi
	.p2align	4, 0x90
.LBB22_65:                              # %.lr.ph.i239
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        #       Parent Loop BB22_73 Depth=3
                                        #         Parent Loop BB22_17 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rsi), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB22_65
.LBB22_66:                              # %._crit_edge.i
                                        #   in Loop: Header=BB22_17 Depth=4
	movslq	cont_STACKPOINTER(%rip), %rdx
	testq	%rdx, %rdx
	je	.LBB22_68
# BB#67:                                #   in Loop: Header=BB22_17 Depth=4
	leaq	-1(%rdx), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rdx,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
.LBB22_68:                              # %cont_BackTrack.exit
                                        #   in Loop: Header=BB22_17 Depth=4
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	104(%rsp), %rsi         # 8-byte Reload
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rax, %rdx
	callq	term_ReplaceSubtermBy
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	72(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB22_70
# BB#69:                                #   in Loop: Header=BB22_17 Depth=4
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	%rbx, %rdi
	callq	clause_Delete
.LBB22_70:                              #   in Loop: Header=BB22_17 Depth=4
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	term_Delete
	movl	68(%rsp), %eax          # 4-byte Reload
	movl	%eax, stack_POINTER(%rip)
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 56(%rax)
	je	.LBB22_72
# BB#71:                                #   in Loop: Header=BB22_17 Depth=4
	movl	(%r14), %esi
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	printf
.LBB22_72:                              # %.thread
                                        #   in Loop: Header=BB22_17 Depth=4
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbx)
	movl	$1, 16(%rsp)            # 4-byte Folded Spill
	movl	$1, %esi
	movq	120(%rsp), %r14         # 8-byte Reload
	movq	96(%rsp), %r12          # 8-byte Reload
	testl	%esi, %esi
	je	.LBB22_31
	jmp	.LBB22_74
	.p2align	4, 0x90
.LBB22_73:                              # %.lr.ph273
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB22_17 Depth 4
                                        #           Child Loop BB22_21 Depth 5
                                        #             Child Loop BB22_22 Depth 6
                                        #           Child Loop BB22_47 Depth 5
                                        #           Child Loop BB22_54 Depth 5
                                        #           Child Loop BB22_65 Depth 5
	movq	8(%r14), %r12
	xorl	%esi, %esi
	cmpl	$0, (%r12)
	jle	.LBB22_15
.LBB22_74:                              # %.critedge
                                        #   in Loop: Header=BB22_73 Depth=3
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	sete	%cl
	setne	%dl
	testl	%esi, %esi
	jne	.LBB22_77
# BB#75:                                # %.critedge
                                        #   in Loop: Header=BB22_73 Depth=3
	testb	%dl, %dl
	movq	%rax, %r14
	jne	.LBB22_73
.LBB22_77:                              # %._crit_edge
                                        #   in Loop: Header=BB22_12 Depth=2
	movl	%esi, %ebx
	testb	%cl, %cl
	jne	.LBB22_79
	.p2align	4, 0x90
.LBB22_78:                              # %.lr.ph.i
                                        #   Parent Loop BB22_2 Depth=1
                                        #     Parent Loop BB22_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB22_78
.LBB22_79:                              # %list_Delete.exit.backedge
                                        #   in Loop: Header=BB22_12 Depth=2
	movl	stack_POINTER(%rip), %eax
	cmpl	68(%rsp), %eax          # 4-byte Folded Reload
	jne	.LBB22_12
	jmp	.LBB22_6
	.p2align	4, 0x90
.LBB22_9:                               #   in Loop: Header=BB22_2 Depth=1
	movq	%rsi, %rax
	jmp	.LBB22_10
	.p2align	4, 0x90
.LBB22_6:                               # %.loopexit
                                        #   in Loop: Header=BB22_2 Depth=1
	testl	%ebx, %ebx
	movq	24(%rsp), %rbx          # 8-byte Reload
	jne	.LBB22_7
	jmp	.LBB22_81
	.p2align	4, 0x90
.LBB22_80:                              #   in Loop: Header=BB22_2 Depth=1
	movq	24(%rsp), %rbx          # 8-byte Reload
.LBB22_81:                              # %.loopexit246
                                        #   in Loop: Header=BB22_2 Depth=1
	cmpq	136(%rsp), %rbx         # 8-byte Folded Reload
	leaq	1(%rbx), %rbx
	jl	.LBB22_2
# BB#82:                                # %._crit_edge296
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB22_88
# BB#83:
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	%rbp, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	112(%rsp), %r14         # 8-byte Reload
	movq	%r14, %rdx
	callq	clause_OrientEqualities
	movq	%rbp, %rdi
	callq	clause_Normalize
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbp)
	movq	%rbp, %rdi
	callq	clause_UpdateMaxVar
	cmpl	$0, 56(%rbx)
	je	.LBB22_85
# BB#84:
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbp, %rdi
	callq	clause_Print
.LBB22_85:
	movq	%rbp, %r12
	movq	48(%rsp), %rbp          # 8-byte Reload
	cmpq	%rbp, %r12
	movq	128(%rsp), %r14         # 8-byte Reload
	je	.LBB22_87
# BB#86:
	movq	%rbp, %rdi
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rsi
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdx
	callq	clause_OrientEqualities
	movq	%rbp, %rdi
	callq	clause_Normalize
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	callq	clause_SetMaxLitFlags
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%rbp)
	movq	%rbp, %rdi
	callq	clause_UpdateMaxVar
	movq	%r12, (%r14)
.LBB22_87:                              # %._crit_edge296.thread
	movl	16(%rsp), %eax          # 4-byte Reload
	jmp	.LBB22_89
.LBB22_88:
	xorl	%eax, %eax
.LBB22_89:                              # %._crit_edge296.thread
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end22:
	.size	red_ContextualRewriting, .Lfunc_end22-red_ContextualRewriting
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_SortSimplification,@function
red_SortSimplification:                 # @red_SortSimplification
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi262:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi263:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi264:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi265:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi266:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi267:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi268:
	.cfi_def_cfa_offset 160
.Lcfi269:
	.cfi_offset %rbx, -56
.Lcfi270:
	.cfi_offset %r12, -48
.Lcfi271:
	.cfi_offset %r13, -40
.Lcfi272:
	.cfi_offset %r14, -32
.Lcfi273:
	.cfi_offset %r15, -24
.Lcfi274:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, 92(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	testq	%rdi, %rdi
	je	.LBB23_82
# BB#1:
	movl	64(%rsi), %r15d
	testl	%r15d, %r15d
	jle	.LBB23_82
# BB#2:                                 # %.lr.ph168
	movl	12(%rsi), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	decl	%r15d
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%r8, 72(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB23_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB23_20 Depth 2
                                        #     Child Loop BB23_15 Depth 2
                                        #     Child Loop BB23_25 Depth 2
                                        #     Child Loop BB23_41 Depth 2
                                        #     Child Loop BB23_45 Depth 2
	movq	56(%r12), %rax
	movslq	%ebx, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	jne	.LBB23_5
# BB#4:                                 #   in Loop: Header=BB23_3 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB23_5:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	16(%rbp), %rax
	movq	8(%rax), %rsi
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rdi
	movq	%r12, %rdx
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	movl	%ebx, %ecx
	movq	72(%rsp), %r8           # 8-byte Reload
	movq	64(%rsp), %r9           # 8-byte Reload
	callq	sort_ComputeSortNoResidues
	movq	(%rax), %rcx
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	8(%rax), %r15
	movq	32(%rcx), %r13
	movq	$0, 32(%rcx)
	movl	(%rbp), %esi
	movq	%r14, %rdi
	callq	sort_TheorySortOfSymbol
	movq	%rax, %rbp
	movq	%r14, %rdi
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	sort_TheoryIsSubsortOfNoResidues
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB23_44
# BB#6:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	jne	.LBB23_9
# BB#7:                                 #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 72(%r14)
	je	.LBB23_9
# BB#8:                                 #   in Loop: Header=BB23_3 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.34, %edi
	movl	$21, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.28, %edi
	movl	$6, %esi
	movl	$1, %edx
	callq	fwrite
.LBB23_9:                               #   in Loop: Header=BB23_3 Depth=1
	movq	32(%r15), %rax
	testq	%r13, %r13
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	je	.LBB23_10
# BB#11:                                #   in Loop: Header=BB23_3 Depth=1
	testq	%rax, %rax
	movq	%r13, %rcx
	je	.LBB23_12
	.p2align	4, 0x90
.LBB23_20:                              # %.preheader.i147
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rdx
	movq	(%rdx), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_20
# BB#21:                                #   in Loop: Header=BB23_3 Depth=1
	movq	%rax, (%rdx)
	jmp	.LBB23_22
	.p2align	4, 0x90
.LBB23_44:                              #   in Loop: Header=BB23_3 Depth=1
	testq	%r13, %r13
	je	.LBB23_46
	.p2align	4, 0x90
.LBB23_45:                              # %.lr.ph.i139
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB23_45
.LBB23_46:                              # %list_Delete.exit141
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
	incl	%ebx
	movq	72(%rsp), %r14          # 8-byte Reload
	movl	8(%rsp), %r15d          # 4-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB23_47
	.p2align	4, 0x90
.LBB23_10:                              #   in Loop: Header=BB23_3 Depth=1
	movq	%rax, %r13
.LBB23_22:                              # %list_Nconc.exit149
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	$0, 32(%r15)
	movq	%r15, %rdi
	callq	sort_ConditionDelete
	testq	%r13, %r13
	jne	.LBB23_13
# BB#23:                                #   in Loop: Header=BB23_3 Depth=1
	movb	$1, %bpl
	xorl	%r13d, %r13d
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB23_35
	jmp	.LBB23_34
.LBB23_12:                              # %list_Nconc.exit149.thread
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	$0, 32(%r15)
	movq	%r15, %rdi
	callq	sort_ConditionDelete
.LBB23_13:                              # %.lr.ph
                                        #   in Loop: Header=BB23_3 Depth=1
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB23_24
# BB#14:                                #   in Loop: Header=BB23_3 Depth=1
	movq	%r13, %rbp
	.p2align	4, 0x90
.LBB23_15:                              # %.lr.ph.split
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, %r12
	jne	.LBB23_17
# BB#16:                                #   in Loop: Header=BB23_15 Depth=2
	movq	%rbx, %rdi
	callq	clause_Copy
	movq	%rax, %r12
.LBB23_17:                              #   in Loop: Header=BB23_15 Depth=2
	movq	8(%rbp), %rsi
	movq	%r12, %rdi
	callq	clause_UpdateSplitDataFromPartner
	cmpl	$0, 72(%r14)
	je	.LBB23_19
# BB#18:                                #   in Loop: Header=BB23_15 Depth=2
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	printf
.LBB23_19:                              #   in Loop: Header=BB23_15 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_15
	jmp	.LBB23_32
.LBB23_24:                              # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	%r13, %rbp
	movl	84(%rsp), %r15d         # 4-byte Reload
	.p2align	4, 0x90
.LBB23_25:                              # %.lr.ph.split.us
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%rbx, %r12
	jne	.LBB23_29
# BB#26:                                #   in Loop: Header=BB23_25 Depth=2
	movq	8(%rbp), %rax
	movl	12(%rax), %eax
	cmpl	%r15d, %eax
	ja	.LBB23_28
# BB#27:                                #   in Loop: Header=BB23_25 Depth=2
	cmpl	92(%rsp), %eax          # 4-byte Folded Reload
	movq	%rbx, %r12
	jbe	.LBB23_29
.LBB23_28:                              #   in Loop: Header=BB23_25 Depth=2
	movq	%rbx, %rdi
	callq	clause_Copy
	movq	%rax, %r12
	.p2align	4, 0x90
.LBB23_29:                              # %.lr.ph.split.us._crit_edge
                                        #   in Loop: Header=BB23_25 Depth=2
	movq	8(%rbp), %rsi
	movq	%r12, %rdi
	callq	clause_UpdateSplitDataFromPartner
	cmpl	$0, 72(%r14)
	je	.LBB23_31
# BB#30:                                #   in Loop: Header=BB23_25 Depth=2
	movq	8(%rbp), %rax
	movl	(%rax), %esi
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	callq	printf
.LBB23_31:                              #   in Loop: Header=BB23_25 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB23_25
.LBB23_32:                              #   in Loop: Header=BB23_3 Depth=1
	xorl	%ebp, %ebp
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB23_34
.LBB23_35:                              #   in Loop: Header=BB23_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rbx          # 8-byte Reload
	leal	(%rax,%rbx), %eax
	movl	%ebp, 88(%rsp)          # 4-byte Spill
	movslq	%eax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movl	88(%rsp), %ebp          # 4-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB23_36
	.p2align	4, 0x90
.LBB23_34:                              #   in Loop: Header=BB23_3 Depth=1
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB23_36:                              #   in Loop: Header=BB23_3 Depth=1
	movq	%r12, %rdi
	movl	%ebx, %esi
	movq	%r14, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiteral
	testb	%bpl, %bpl
	je	.LBB23_38
# BB#37:                                #   in Loop: Header=BB23_3 Depth=1
	movq	(%rsp), %r13            # 8-byte Reload
	jmp	.LBB23_39
	.p2align	4, 0x90
.LBB23_38:                              #   in Loop: Header=BB23_3 Depth=1
	cmpq	$0, (%rsp)              # 8-byte Folded Reload
	je	.LBB23_39
# BB#40:                                # %.preheader.i142.preheader
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	%r13, %rcx
	movl	8(%rsp), %r15d          # 4-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB23_41:                              # %.preheader.i142
                                        #   Parent Loop BB23_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_41
# BB#42:                                #   in Loop: Header=BB23_3 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	40(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB23_43
	.p2align	4, 0x90
.LBB23_39:                              #   in Loop: Header=BB23_3 Depth=1
	movl	8(%rsp), %r15d          # 4-byte Reload
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB23_43:                              # %list_Nconc.exit
                                        #   in Loop: Header=BB23_3 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	decl	%r15d
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	48(%rsp), %rbx          # 8-byte Reload
.LBB23_47:                              #   in Loop: Header=BB23_3 Depth=1
	callq	sort_DeleteSortPair
	movq	%rbp, %rdi
	callq	sort_Delete
	cmpl	%r15d, %ebx
	jle	.LBB23_3
# BB#48:                                # %._crit_edge169
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	jle	.LBB23_49
# BB#50:
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB23_76
# BB#51:
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.LBB23_53
	.p2align	4, 0x90
.LBB23_52:                              # %.lr.ph.i45.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB23_52
.LBB23_53:                              # %list_Delete.exit46.i
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.LBB23_55
	.p2align	4, 0x90
.LBB23_54:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB23_54
.LBB23_55:                              # %list_Delete.exit.preheader.i
	movq	24(%rsp), %rax          # 8-byte Reload
	testq	%rax, %rax
	je	.LBB23_56
# BB#59:                                # %.lr.ph53.i
	xorl	%r13d, %r13d
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB23_60:                              # %list_Delete.exit.i
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%r12), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rcx
	movq	%r15, 8(%rcx)
	movq	%r13, (%rcx)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rcx, %r13
	jne	.LBB23_60
	jmp	.LBB23_57
.LBB23_49:
	xorl	%eax, %eax
	jmp	.LBB23_82
.LBB23_76:
	movq	(%rsp), %rcx            # 8-byte Reload
	testq	%rcx, %rcx
	movq	16(%rsp), %rbx          # 8-byte Reload
	je	.LBB23_78
	.p2align	4, 0x90
.LBB23_77:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rax
	movq	%rcx, %rsi
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rsi)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rsi, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rcx
	jne	.LBB23_77
	jmp	.LBB23_78
.LBB23_56:
	xorl	%ecx, %ecx
.LBB23_57:                              # %.preheader.i
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, %rbx
	testq	%rax, %rax
	je	.LBB23_58
# BB#61:                                # %.lr.ph.i135.preheader
	xorl	%r13d, %r13d
	movq	%rax, %rbp
	.p2align	4, 0x90
.LBB23_62:                              # %.lr.ph.i135
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movslq	68(%rax), %rcx
	movslq	64(%rax), %r15
	addq	%rcx, %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r13, (%rax)
	movq	8(%rbp), %rcx
	movslq	(%rcx), %rcx
	movq	%rcx, 8(%rbp)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rax, %r13
	jne	.LBB23_62
	jmp	.LBB23_63
.LBB23_58:
	xorl	%eax, %eax
.LBB23_63:                              # %._crit_edge.i
	movq	24(%rsp), %rcx          # 8-byte Reload
	testq	%rcx, %rcx
	je	.LBB23_64
# BB#66:
	movq	%rcx, %rdx
	testq	%rax, %rax
	je	.LBB23_65
# BB#67:                                # %.preheader.i39.i.preheader
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB23_68:                              # %.preheader.i39.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB23_68
# BB#69:
	movq	%rax, (%rcx)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB23_70
.LBB23_64:
	movq	%rax, %rdx
.LBB23_65:                              # %list_Nconc.exit41.i
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rbx, %rsi
.LBB23_70:                              # %list_Nconc.exit41.i
	movq	16(%rsp), %rbx          # 8-byte Reload
	testq	%rsi, %rsi
	movq	%rsi, %rax
	cmoveq	%rcx, %rax
	testq	%rcx, %rcx
	movq	%rdx, 40(%r12)
	je	.LBB23_75
# BB#71:                                # %list_Nconc.exit41.i
	testq	%rsi, %rsi
	je	.LBB23_75
# BB#72:                                # %.preheader.i.i.preheader
	movq	%rsi, %rcx
	.p2align	4, 0x90
.LBB23_73:                              # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB23_73
# BB#74:
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rsi, %rax
.LBB23_75:                              # %red_DocumentSortSimplification.exit
	movq	%rax, 32(%r12)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r12)
	movl	$20, 76(%r12)
.LBB23_78:                              # %list_Delete.exit
	movq	%r12, %rdi
	callq	clause_Normalize
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	64(%rsp), %rdx          # 8-byte Reload
	callq	clause_SetMaxLitFlags
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	clause_ComputeWeight
	movl	%eax, 4(%r12)
	movq	%r12, %rdi
	callq	clause_UpdateMaxVar
	cmpl	$0, 72(%r14)
	je	.LBB23_80
# BB#79:
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
.LBB23_80:
	movl	$1, %eax
	cmpq	%rbx, %r12
	je	.LBB23_82
# BB#81:                                # %.critedge120
	movq	160(%rsp), %rcx
	movq	%r12, (%rcx)
.LBB23_82:                              # %.critedge
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	red_SortSimplification, .Lfunc_end23-red_SortSimplification
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_MatchingReplacementResolution,@function
red_MatchingReplacementResolution:      # @red_MatchingReplacementResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi275:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi276:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi277:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi278:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi279:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi280:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi281:
	.cfi_def_cfa_offset 176
.Lcfi282:
	.cfi_offset %rbx, -56
.Lcfi283:
	.cfi_offset %r12, -48
.Lcfi284:
	.cfi_offset %r13, -40
.Lcfi285:
	.cfi_offset %r14, -32
.Lcfi286:
	.cfi_offset %r15, -24
.Lcfi287:
	.cfi_offset %rbp, -16
	movl	%r9d, 60(%rsp)          # 4-byte Spill
	movq	%r8, 88(%rsp)           # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	movslq	68(%rdi), %rax
	movslq	72(%rdi), %rcx
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movslq	64(%rdi), %rdx
	addq	%rax, %rdx
	addq	%rcx, %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	testl	%edx, %edx
	jle	.LBB24_59
# BB#1:                                 # %.lr.ph45.lr.ph
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	36(%rax), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	movq	16(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB24_2
.LBB24_37:                              #   in Loop: Header=BB24_2 Depth=1
	movl	12(%rbx), %eax
	cmpl	60(%rsp), %eax          # 4-byte Folded Reload
	ja	.LBB24_39
# BB#38:                                #   in Loop: Header=BB24_2 Depth=1
	movq	16(%rsp), %rbp          # 8-byte Reload
	cmpl	12(%rbp), %eax
	jbe	.LBB24_40
	jmp	.LBB24_39
	.p2align	4, 0x90
.LBB24_2:                               # %.lr.ph45
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_3 Depth 2
                                        #       Child Loop BB24_8 Depth 3
                                        #       Child Loop BB24_13 Depth 3
                                        #         Child Loop BB24_16 Depth 4
                                        #           Child Loop BB24_23 Depth 5
                                        #     Child Loop BB24_34 Depth 2
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movslq	%r13d, %r13
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB24_3:                               #   Parent Loop BB24_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB24_8 Depth 3
                                        #       Child Loop BB24_13 Depth 3
                                        #         Child Loop BB24_16 Depth 4
                                        #           Child Loop BB24_23 Depth 5
	movq	56(%rbp), %rax
	movq	(%rax,%r13,8), %r14
	movq	24(%r14), %rdx
	movl	(%rdx), %eax
	movl	fol_NOT(%rip), %ecx
	cmpl	%eax, %ecx
	movl	%eax, %esi
	jne	.LBB24_5
# BB#4:                                 #   in Loop: Header=BB24_3 Depth=2
	movq	16(%rdx), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %esi
.LBB24_5:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB24_3 Depth=2
	cmpl	%eax, %ecx
	setne	%bl
	cmpl	%esi, fol_EQUALITY(%rip)
	jne	.LBB24_7
# BB#6:                                 # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB24_3 Depth=2
	testb	%bl, %bl
	je	.LBB24_27
.LBB24_7:                               #   in Loop: Header=BB24_3 Depth=2
	movq	16(%r14), %r15
	movq	56(%r15), %rsi
	movl	$-1, %ebx
	.p2align	4, 0x90
.LBB24_8:                               #   Parent Loop BB24_2 Depth=1
                                        #     Parent Loop BB24_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ebx
	cmpq	%r14, (%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB24_8
# BB#9:                                 # %clause_LiteralGetIndex.exit46.i
                                        #   in Loop: Header=BB24_3 Depth=2
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rsi
	cmpl	%eax, %ecx
	jne	.LBB24_11
# BB#10:                                #   in Loop: Header=BB24_3 Depth=2
	movq	16(%rdx), %rax
	movq	8(%rax), %rdx
.LBB24_11:                              # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB24_3 Depth=2
	callq	st_ExistGen
	testq	%rax, %rax
	jne	.LBB24_13
	jmp	.LBB24_27
	.p2align	4, 0x90
.LBB24_26:                              # %.loopexit.i
                                        #   in Loop: Header=BB24_13 Depth=3
	callq	st_NextCandidate
	testq	%rax, %rax
	je	.LBB24_27
.LBB24_13:                              # %.lr.ph53.i
                                        #   Parent Loop BB24_2 Depth=1
                                        #     Parent Loop BB24_3 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB24_16 Depth 4
                                        #           Child Loop BB24_23 Depth 5
	cmpl	$0, (%rax)
	jg	.LBB24_26
# BB#14:                                #   in Loop: Header=BB24_13 Depth=3
	movq	%rax, %rdi
	callq	sharing_NAtomDataList
	movq	%rax, %rbp
	testq	%rbp, %rbp
	jne	.LBB24_16
	jmp	.LBB24_26
	.p2align	4, 0x90
.LBB24_25:                              #   in Loop: Header=BB24_16 Depth=4
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB24_26
.LBB24_16:                              # %.lr.ph.i24
                                        #   Parent Loop BB24_2 Depth=1
                                        #     Parent Loop BB24_3 Depth=2
                                        #       Parent Loop BB24_13 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB24_23 Depth 5
	movq	8(%rbp), %r12
	movq	24(%r14), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %eax
	movq	24(%r12), %rdx
	movl	(%rdx), %edx
	cmpl	%ecx, %eax
	jne	.LBB24_18
# BB#17:                                #   in Loop: Header=BB24_16 Depth=4
	cmpl	%ecx, %edx
	movl	%ecx, %edx
	jne	.LBB24_20
.LBB24_18:                              # %clause_LiteralsAreComplementary.exit.i
                                        #   in Loop: Header=BB24_16 Depth=4
	cmpl	%ecx, %eax
	je	.LBB24_25
# BB#19:                                # %clause_LiteralsAreComplementary.exit.i
                                        #   in Loop: Header=BB24_16 Depth=4
	cmpl	%edx, %eax
	jne	.LBB24_25
.LBB24_20:                              # %clause_LiteralsAreComplementary.exit.thread.i
                                        #   in Loop: Header=BB24_16 Depth=4
	movq	16(%r12), %rdi
	movl	68(%rdi), %eax
	addl	64(%rdi), %eax
	addl	72(%rdi), %eax
	cmpl	$1, %eax
	je	.LBB24_21
# BB#22:                                #   in Loop: Header=BB24_16 Depth=4
	movq	56(%rdi), %rax
	movl	$-1, %edx
	.p2align	4, 0x90
.LBB24_23:                              #   Parent Loop BB24_2 Depth=1
                                        #     Parent Loop BB24_3 Depth=2
                                        #       Parent Loop BB24_13 Depth=3
                                        #         Parent Loop BB24_16 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%edx
	cmpq	%r12, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB24_23
# BB#24:                                # %clause_LiteralGetIndex.exit.i
                                        #   in Loop: Header=BB24_16 Depth=4
	movq	%r15, %rsi
	movl	%ebx, %ecx
	callq	subs_SubsumesBasic
	testl	%eax, %eax
	je	.LBB24_25
# BB#29:                                # %red_GetMRResLit.exit
                                        #   in Loop: Header=BB24_3 Depth=2
	callq	st_CancelExistRetrieval
	testq	%r12, %r12
	jne	.LBB24_30
	.p2align	4, 0x90
.LBB24_27:                              # %.backedge
                                        #   in Loop: Header=BB24_3 Depth=2
	incq	%r13
	cmpq	48(%rsp), %r13          # 8-byte Folded Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	jl	.LBB24_3
	jmp	.LBB24_28
	.p2align	4, 0x90
.LBB24_21:                              # %red_GetMRResLit.exit.thread
                                        #   in Loop: Header=BB24_2 Depth=1
	callq	st_CancelExistRetrieval
.LBB24_30:                              # %.loopexit
                                        #   in Loop: Header=BB24_2 Depth=1
	movq	32(%rsp), %r15          # 8-byte Reload
	testq	%r15, %r15
	movq	80(%rsp), %rbp          # 8-byte Reload
	jne	.LBB24_33
# BB#31:                                #   in Loop: Header=BB24_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 80(%rax)
	je	.LBB24_33
# BB#32:                                #   in Loop: Header=BB24_2 Depth=1
	movq	stdout(%rip), %rcx
	movl	$.L.str.36, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%rbp, %rdi
	callq	clause_Print
.LBB24_33:                              #   in Loop: Header=BB24_2 Depth=1
	movq	16(%r12), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movslq	(%rax), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r14
	movq	%rbx, 8(%r14)
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r14)
	movq	16(%r12), %rax
	movq	56(%rax), %rax
	movabsq	$-4294967296, %rbx      # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB24_34:                              #   Parent Loop BB24_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rcx, %rbx
	cmpq	%r12, (%rax)
	leaq	8(%rax), %rax
	jne	.LBB24_34
# BB#35:                                # %clause_LiteralGetIndex.exit
                                        #   in Loop: Header=BB24_2 Depth=1
	sarq	$32, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	movq	%rbx, 8(%r12)
	movq	%r15, (%r12)
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	(%rax,%r13), %eax
	movslq	%eax, %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%r15)
	cmpq	16(%rsp), %rbp          # 8-byte Folded Reload
	movq	104(%rsp), %rbx         # 8-byte Reload
	jne	.LBB24_40
# BB#36:                                #   in Loop: Header=BB24_2 Depth=1
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB24_37
.LBB24_39:                              #   in Loop: Header=BB24_2 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	clause_Copy
	movq	%rax, %rbp
.LBB24_40:                              # %.outer
                                        #   in Loop: Header=BB24_2 Depth=1
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	%rbp, %rdi
	movl	%r13d, %esi
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	96(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiteral
	movq	48(%rsp), %rcx          # 8-byte Reload
	decq	%rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	%r13d, %rax
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	cmpq	%rcx, %rax
	movq	%r12, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%r15, %rcx
	movq	%r14, %rdx
	jl	.LBB24_2
	jmp	.LBB24_41
.LBB24_28:
	movq	64(%rsp), %r14          # 8-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB24_41:                              # %.outer._crit_edge
	testq	%r14, %r14
	je	.LBB24_59
# BB#42:
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB24_48
# BB#43:
	movq	%r14, %rdi
	callq	list_NReverse
	movq	%rax, %rbx
	movq	%r15, %rdi
	callq	list_NReverse
	movq	%rbp, %r15
	movq	%rax, %rbp
	movq	%r12, %rdi
	callq	list_NReverse
	movq	%r15, %r14
	movq	%r15, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	movq	%rax, %rcx
	callq	red_DocumentMatchingReplacementResolution
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 80(%rax)
	je	.LBB24_57
# BB#44:
	movq	stdout(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	testq	%rbx, %rbx
	je	.LBB24_47
	.p2align	4, 0x90
.LBB24_45:                              # %.lr.ph41
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbx), %esi
	movl	8(%rbp), %edx
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rbx
	movq	(%rbp), %rbp
	testq	%rbx, %rbx
	jne	.LBB24_45
.LBB24_47:                              # %._crit_edge42
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r14, %rdi
	callq	clause_Print
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	jne	.LBB24_58
	jmp	.LBB24_59
.LBB24_48:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 80(%rax)
	je	.LBB24_52
# BB#49:                                # %.lr.ph.preheader
	movq	%rbp, %r13
	movq	stdout(%rip), %rcx
	movl	$.L.str.37, %edi
	movl	$7, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r15, %rbx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB24_50:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	8(%rbp), %esi
	movl	8(%rbx), %edx
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbp), %rbp
	movq	(%rbx), %rbx
	testq	%rbp, %rbp
	jne	.LBB24_50
# BB#51:                                # %._crit_edge
	movq	stdout(%rip), %rcx
	movl	$.L.str.30, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rbp
	movq	%rbp, %rdi
	callq	clause_Print
	.p2align	4, 0x90
.LBB24_52:                              # %.lr.ph.i22
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB24_52
# BB#53:                                # %list_Delete.exit23
	movq	%rbp, %r14
	testq	%r15, %r15
	je	.LBB24_55
	.p2align	4, 0x90
.LBB24_54:                              # %.lr.ph.i17
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r15)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r15, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r15
	jne	.LBB24_54
.LBB24_55:                              # %list_Delete.exit18
	testq	%r12, %r12
	je	.LBB24_57
	.p2align	4, 0x90
.LBB24_56:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r12)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r12, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r12
	jne	.LBB24_56
.LBB24_57:                              # %list_Delete.exit
	cmpq	16(%rsp), %r14          # 8-byte Folded Reload
	je	.LBB24_59
.LBB24_58:
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
.LBB24_59:                              # %.outer._crit_edge.thread
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	red_MatchingReplacementResolution, .Lfunc_end24-red_MatchingReplacementResolution
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.zero	16
	.text
	.p2align	4, 0x90
	.type	red_UnitConflict,@function
red_UnitConflict:                       # @red_UnitConflict
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi288:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi289:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi290:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi291:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi292:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi293:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi294:
	.cfi_def_cfa_offset 96
.Lcfi295:
	.cfi_offset %rbx, -56
.Lcfi296:
	.cfi_offset %r12, -48
.Lcfi297:
	.cfi_offset %r13, -40
.Lcfi298:
	.cfi_offset %r14, -32
.Lcfi299:
	.cfi_offset %r15, -24
.Lcfi300:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
	movq	%r8, 32(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	68(%r13), %eax
	addl	64(%r13), %eax
	addl	72(%r13), %eax
	cmpl	$1, %eax
	jne	.LBB25_68
# BB#1:
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	36(%rax), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	56(%r13), %rax
	movq	(%rax), %r12
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r14), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	24(%r12), %rcx
	movl	fol_NOT(%rip), %eax
	cmpl	(%rcx), %eax
	jne	.LBB25_3
# BB#2:
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB25_3:                               # %clause_LiteralAtom.exit52
	callq	st_ExistUnifier
	jmp	.LBB25_13
	.p2align	4, 0x90
.LBB25_4:                               # %.lr.ph70.us.preheader
                                        #   in Loop: Header=BB25_14 Depth=2
	movq	24(%r12), %rcx
	movl	(%rcx), %ecx
	movl	fol_NOT(%rip), %edx
	.p2align	4, 0x90
.LBB25_5:                               # %.lr.ph70.us
                                        #   Parent Loop BB25_13 Depth=1
                                        #     Parent Loop BB25_14 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %r15
	movq	24(%r15), %rsi
	movl	(%rsi), %esi
	cmpl	%ecx, %edx
	jne	.LBB25_7
# BB#6:                                 #   in Loop: Header=BB25_5 Depth=3
	cmpl	%ecx, %esi
	movl	%ecx, %esi
	jne	.LBB25_9
.LBB25_7:                               # %clause_LiteralsAreComplementary.exit44.us
                                        #   in Loop: Header=BB25_5 Depth=3
	cmpl	%ecx, %edx
	je	.LBB25_10
# BB#8:                                 # %clause_LiteralsAreComplementary.exit44.us
                                        #   in Loop: Header=BB25_5 Depth=3
	cmpl	%esi, %edx
	jne	.LBB25_10
.LBB25_9:                               # %clause_LiteralsAreComplementary.exit44.thread.us
                                        #   in Loop: Header=BB25_5 Depth=3
	movq	16(%r15), %rsi
	movl	68(%rsi), %edi
	addl	64(%rsi), %edi
	addl	72(%rsi), %edi
	cmpl	$1, %edi
	je	.LBB25_11
.LBB25_10:                              # %.backedge62.us
                                        #   in Loop: Header=BB25_5 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB25_5
	jmp	.LBB25_12
	.p2align	4, 0x90
.LBB25_11:                              # %._crit_edge71.us
                                        #   in Loop: Header=BB25_14 Depth=2
	callq	st_CancelExistRetrieval
	testq	%r15, %r15
	je	.LBB25_14
	jmp	.LBB25_38
	.p2align	4, 0x90
.LBB25_12:                              # %.thread
                                        #   in Loop: Header=BB25_13 Depth=1
	callq	st_NextCandidate
.LBB25_13:                              # %clause_LiteralAtom.exit52
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_14 Depth 2
                                        #       Child Loop BB25_5 Depth 3
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB25_16
.LBB25_14:                              #   Parent Loop BB25_13 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_5 Depth 3
	cmpl	$0, (%rbp)
	jg	.LBB25_12
# BB#15:                                #   in Loop: Header=BB25_14 Depth=2
	movq	%rbp, %rdi
	callq	sharing_NAtomDataList
	testq	%rax, %rax
	jne	.LBB25_4
	jmp	.LBB25_12
.LBB25_16:                              # %.outer63._crit_edge
	movq	24(%r12), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %edx
	cmpl	%ecx, %edx
	movl	%ecx, %ebp
	jne	.LBB25_18
# BB#17:
	movq	16(%rax), %rsi
	movq	8(%rsi), %rsi
	movl	(%rsi), %ebp
.LBB25_18:                              # %clause_LiteralAtom.exit36
	cmpl	%ebp, fol_EQUALITY(%rip)
	jne	.LBB25_68
# BB#19:
	cmpl	%ecx, %edx
	movl	%ebx, 28(%rsp)          # 4-byte Spill
	jne	.LBB25_21
# BB#20:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB25_21:                              # %clause_LiteralAtom.exit
	movq	16(%rax), %rdi
	callq	list_Reverse
	movl	%ebp, %edi
	movq	%rax, %rsi
	callq	term_Create
	movq	%rax, %rbx
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	(%r14), %rsi
	movq	cont_RIGHTCONTEXT(%rip), %rdx
	movq	%rbx, %rcx
	callq	st_ExistUnifier
	jmp	.LBB25_31
	.p2align	4, 0x90
.LBB25_22:                              # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB25_32 Depth=2
	movq	24(%r12), %rcx
	movl	(%rcx), %ecx
	movl	fol_NOT(%rip), %edx
	.p2align	4, 0x90
.LBB25_23:                              # %.lr.ph.us
                                        #   Parent Loop BB25_31 Depth=1
                                        #     Parent Loop BB25_32 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	8(%rax), %r15
	movq	24(%r15), %rsi
	movl	(%rsi), %esi
	cmpl	%ecx, %edx
	jne	.LBB25_25
# BB#24:                                #   in Loop: Header=BB25_23 Depth=3
	cmpl	%ecx, %esi
	movl	%ecx, %esi
	jne	.LBB25_27
.LBB25_25:                              # %clause_LiteralsAreComplementary.exit.us
                                        #   in Loop: Header=BB25_23 Depth=3
	cmpl	%ecx, %edx
	je	.LBB25_28
# BB#26:                                # %clause_LiteralsAreComplementary.exit.us
                                        #   in Loop: Header=BB25_23 Depth=3
	cmpl	%esi, %edx
	jne	.LBB25_28
.LBB25_27:                              # %clause_LiteralsAreComplementary.exit.thread.us
                                        #   in Loop: Header=BB25_23 Depth=3
	movq	16(%r15), %rsi
	movl	68(%rsi), %edi
	addl	64(%rsi), %edi
	addl	72(%rsi), %edi
	cmpl	$1, %edi
	je	.LBB25_29
.LBB25_28:                              # %.backedge.us
                                        #   in Loop: Header=BB25_23 Depth=3
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB25_23
	jmp	.LBB25_30
	.p2align	4, 0x90
.LBB25_29:                              # %._crit_edge.us
                                        #   in Loop: Header=BB25_32 Depth=2
	callq	st_CancelExistRetrieval
	testq	%r15, %r15
	je	.LBB25_32
	jmp	.LBB25_35
	.p2align	4, 0x90
.LBB25_30:                              # %.thread54
                                        #   in Loop: Header=BB25_31 Depth=1
	callq	st_NextCandidate
.LBB25_31:                              # %clause_LiteralAtom.exit
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_32 Depth 2
                                        #       Child Loop BB25_23 Depth 3
	movq	%rax, %rbp
	testq	%rbp, %rbp
	je	.LBB25_34
.LBB25_32:                              #   Parent Loop BB25_31 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_23 Depth 3
	cmpl	$0, (%rbp)
	jg	.LBB25_30
# BB#33:                                #   in Loop: Header=BB25_32 Depth=2
	movq	%rbp, %rdi
	callq	sharing_NAtomDataList
	testq	%rax, %rax
	jne	.LBB25_22
	jmp	.LBB25_30
.LBB25_34:
	xorl	%r15d, %r15d
.LBB25_35:                              # %.us-lcssa.us
	movq	16(%rbx), %rax
	testq	%rax, %rax
	je	.LBB25_37
	.p2align	4, 0x90
.LBB25_36:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB25_36
.LBB25_37:                              # %.loopexit
	movq	memory_ARRAY+256(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+256(%rip), %rax
	movq	%rbx, (%rax)
	testq	%r15, %r15
	movl	28(%rsp), %ebx          # 4-byte Reload
	je	.LBB25_68
.LBB25_38:                              # %.thread56
	movl	%ebx, %r14d
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 84(%rax)
	je	.LBB25_40
# BB#39:
	movq	stdout(%rip), %rcx
	movl	$.L.str.38, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rdi
	callq	clause_Print
.LBB25_40:
	movq	16(%r15), %rbp
	movl	(%rsp), %ebx            # 4-byte Reload
	testl	%ebx, %ebx
	je	.LBB25_58
.LBB25_41:
	movq	%r13, %rdi
	callq	clause_Copy
	movq	%rax, %r14
.LBB25_42:
	movq	%r14, %rdi
	movq	%rbp, %rsi
	callq	clause_UpdateSplitDataFromPartner
	xorl	%esi, %esi
	movq	%r14, %rdi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	callq	clause_DeleteLiteral
	testl	%ebx, %ebx
	je	.LBB25_64
# BB#43:
	movl	$16, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movq	%rax, (%rsp)            # 8-byte Spill
	movups	%xmm0, (%rax)
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movslq	(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r15
	movq	%rbx, 8(%r15)
	movq	$0, (%r15)
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %r12
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r12)
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB25_45
	.p2align	4, 0x90
.LBB25_44:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB25_44
.LBB25_45:                              # %list_Delete.exit.i
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB25_47
	.p2align	4, 0x90
.LBB25_46:                              # %.lr.ph.i21.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB25_46
.LBB25_47:                              # %list_Delete.exit22.i
	movslq	(%r14), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	testq	%rax, %rax
	je	.LBB25_53
# BB#48:
	testq	%r15, %r15
	je	.LBB25_52
# BB#49:                                # %.preheader.i15.i.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB25_50:                              # %.preheader.i15.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB25_50
# BB#51:
	movq	%r15, (%rcx)
.LBB25_52:                              # %list_Nconc.exit17.i
	movq	%rax, %r15
.LBB25_53:                              # %list_Nconc.exit17.i
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	%r15, 32(%r14)
	testq	%rdx, %rdx
	je	.LBB25_62
# BB#54:
	testq	%r12, %r12
	je	.LBB25_61
# BB#55:                                # %.preheader.i.i.preheader
	movq	%rdx, %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	.p2align	4, 0x90
.LBB25_56:                              # %.preheader.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB25_56
# BB#57:
	movq	%r12, (%rax)
	movq	%rdx, %r12
	jmp	.LBB25_63
.LBB25_58:
	movl	12(%rbp), %eax
	cmpl	%r14d, %eax
	ja	.LBB25_41
# BB#59:
	cmpl	12(%r13), %eax
	movq	%r13, %r14
	jbe	.LBB25_42
	jmp	.LBB25_41
.LBB25_61:
	movq	%rdx, %r12
.LBB25_62:                              # %red_DocumentUnitConflict.exit
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB25_63:                              # %red_DocumentUnitConflict.exit
	movq	%r12, 40(%r14)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r14)
	movl	$24, 76(%r14)
.LBB25_64:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 84(%rax)
	je	.LBB25_66
# BB#65:
	movl	(%rbp), %esi
	movl	$.L.str.39, %edi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	printf
	movq	%r14, %rdi
	callq	clause_Print
.LBB25_66:
	cmpq	%r13, %r14
	je	.LBB25_68
# BB#67:
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%r14, (%rax)
.LBB25_68:                              # %.thread83
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	red_UnitConflict, .Lfunc_end25-red_UnitConflict
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_ObviousReductions,@function
red_ObviousReductions:                  # @red_ObviousReductions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi301:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi302:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi303:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi304:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi305:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi306:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi307:
	.cfi_def_cfa_offset 128
.Lcfi308:
	.cfi_offset %rbx, -56
.Lcfi309:
	.cfi_offset %r12, -48
.Lcfi310:
	.cfi_offset %r13, -40
.Lcfi311:
	.cfi_offset %r14, -32
.Lcfi312:
	.cfi_offset %r15, -24
.Lcfi313:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	64(%r13), %eax
	movl	68(%r13), %edi
	leal	(%rax,%rdi), %ebp
	movq	%rbp, 16(%rsp)          # 8-byte Spill
                                        # kill: %EBP<def> %EBP<kill> %RBP<kill>
	decl	%ebp
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	%esi, 28(%rsp)          # 4-byte Spill
	js	.LBB26_19
# BB#1:                                 # %.lr.ph127
	movslq	%ebp, %r15
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB26_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_7 Depth 2
	movq	56(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	24(%rax), %r14
	movl	(%r14), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB26_4
# BB#3:                                 #   in Loop: Header=BB26_2 Depth=1
	movq	16(%r14), %rcx
	movq	8(%rcx), %r14
	movl	(%r14), %ecx
.LBB26_4:                               # %clause_LiteralAtom.exit84
                                        #   in Loop: Header=BB26_2 Depth=1
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB26_6
# BB#5:                                 #   in Loop: Header=BB26_2 Depth=1
	cmpl	$0, 8(%rax)
	je	.LBB26_15
.LBB26_6:                               # %.preheader
                                        #   in Loop: Header=BB26_2 Depth=1
	cmpq	%r15, %rbx
	movq	%rbx, %r12
	jge	.LBB26_17
	.p2align	4, 0x90
.LBB26_7:                               # %.lr.ph116
                                        #   Parent Loop BB26_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r13), %rax
	movq	8(%rax,%r12,8), %rax
	movq	24(%rax), %rbp
	movl	fol_NOT(%rip), %eax
	cmpl	(%rbp), %eax
	jne	.LBB26_9
# BB#8:                                 #   in Loop: Header=BB26_7 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rbp
.LBB26_9:                               # %clause_LiteralAtom.exit92
                                        #   in Loop: Header=BB26_7 Depth=2
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB26_16
# BB#10:                                #   in Loop: Header=BB26_7 Depth=2
	movl	(%r14), %eax
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB26_14
# BB#11:                                #   in Loop: Header=BB26_7 Depth=2
	cmpl	(%rbp), %eax
	jne	.LBB26_14
# BB#12:                                #   in Loop: Header=BB26_7 Depth=2
	movq	16(%r14), %rax
	movq	8(%rax), %rdi
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB26_14
# BB#13:                                #   in Loop: Header=BB26_7 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	movq	16(%r14), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB26_16
	.p2align	4, 0x90
.LBB26_14:                              # %.backedge
                                        #   in Loop: Header=BB26_7 Depth=2
	incq	%r12
	cmpq	%r15, %r12
	jl	.LBB26_7
	jmp	.LBB26_17
.LBB26_15:                              #   in Loop: Header=BB26_2 Depth=1
	movq	16(%r14), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	8(%rcx), %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB26_6
.LBB26_16:                              # %.outer
                                        #   in Loop: Header=BB26_2 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB26_17:                              # %.loopexit106
                                        #   in Loop: Header=BB26_2 Depth=1
	incq	%rbx
	cmpq	16(%rsp), %rbx          # 8-byte Folded Reload
	jne	.LBB26_2
# BB#18:                                # %._crit_edge128.loopexit
	movl	64(%r13), %eax
	movl	68(%r13), %edi
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB26_20
.LBB26_19:
	xorl	%ebx, %ebx
.LBB26_20:                              # %._crit_edge128
	movl	72(%r13), %edx
	leal	(%rdi,%rax), %esi
	leal	-1(%rdx,%rsi), %r14d
	cmpl	%r14d, %esi
	jg	.LBB26_40
# BB#21:                                # %.lr.ph112
	movslq	%esi, %rcx
	movslq	%r14d, %rsi
	leal	1(%rdi,%rax), %r12d
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB26_22:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_27 Depth 2
	movq	%rcx, %rdi
	movq	56(%r13), %rax
	movq	(%rax,%rdi,8), %rcx
	movq	24(%rcx), %rbp
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rbp), %ecx
	jne	.LBB26_24
# BB#23:                                #   in Loop: Header=BB26_22 Depth=1
	movq	16(%rbp), %rdx
	movq	8(%rdx), %rbp
.LBB26_24:                              # %clause_LiteralAtom.exit76
                                        #   in Loop: Header=BB26_22 Depth=1
	cmpq	%rsi, %rdi
	jge	.LBB26_39
# BB#25:                                # %.lr.ph
                                        #   in Loop: Header=BB26_22 Depth=1
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movslq	%r12d, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leaq	1(%rdi), %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	jmp	.LBB26_27
	.p2align	4, 0x90
.LBB26_26:                              # %._crit_edge140
                                        #   in Loop: Header=BB26_27 Depth=2
	movq	56(%r13), %rax
	movl	fol_NOT(%rip), %ecx
	incq	%r15
.LBB26_27:                              #   Parent Loop BB26_22 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%r15,8), %rax
	movq	24(%rax), %rbx
	cmpl	(%rbx), %ecx
	jne	.LBB26_29
# BB#28:                                #   in Loop: Header=BB26_27 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
.LBB26_29:                              # %clause_LiteralAtom.exit68
                                        #   in Loop: Header=BB26_27 Depth=2
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB26_36
# BB#30:                                #   in Loop: Header=BB26_27 Depth=2
	movl	(%rbp), %eax
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB26_34
# BB#31:                                #   in Loop: Header=BB26_27 Depth=2
	cmpl	(%rbx), %eax
	jne	.LBB26_34
# BB#32:                                #   in Loop: Header=BB26_27 Depth=2
	movq	16(%rbp), %rax
	movq	8(%rax), %rdi
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	callq	term_Equal
	testl	%eax, %eax
	je	.LBB26_34
# BB#33:                                #   in Loop: Header=BB26_27 Depth=2
	movq	16(%rbx), %rax
	movq	8(%rax), %rdi
	movq	16(%rbp), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	callq	term_Equal
	testl	%eax, %eax
	jne	.LBB26_36
	.p2align	4, 0x90
.LBB26_34:                              #   in Loop: Header=BB26_27 Depth=2
	cmpl	%r14d, %r15d
	jl	.LBB26_26
# BB#35:                                #   in Loop: Header=BB26_22 Depth=1
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB26_37
	.p2align	4, 0x90
.LBB26_36:                              # %.thread
                                        #   in Loop: Header=BB26_22 Depth=1
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, 8(%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rcx, (%rbx)
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB26_37:                              # %.loopexit105
                                        #   in Loop: Header=BB26_22 Depth=1
	movq	56(%rsp), %rcx          # 8-byte Reload
	incl	%r12d
	cmpq	%rsi, %rdx
	jl	.LBB26_22
.LBB26_39:                              # %._crit_edge.loopexit
	movl	64(%r13), %eax
	movl	68(%r13), %edi
	movl	72(%r13), %edx
.LBB26_40:                              # %._crit_edge
	movq	40(%rsp), %r14          # 8-byte Reload
	cmpl	$1, %edi
	jne	.LBB26_58
# BB#41:                                # %._crit_edge
	addl	%eax, %edi
	addl	%edx, %edi
	cmpl	$1, %edi
	jne	.LBB26_58
# BB#42:
	cltq
	testq	%rbx, %rbx
	je	.LBB26_46
# BB#43:                                # %.lr.ph.i58.preheader
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB26_44:                              # %.lr.ph.i58
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rax, 8(%rcx)
	je	.LBB26_58
# BB#45:                                #   in Loop: Header=BB26_44 Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB26_44
.LBB26_46:                              # %.loopexit
	movq	56(%r13), %rcx
	movq	(%rcx,%rax,8), %rax
	movq	24(%rax), %rdx
	movl	(%rdx), %ecx
	movl	fol_NOT(%rip), %eax
	cmpl	%ecx, %eax
	jne	.LBB26_48
# BB#47:
	movq	16(%rdx), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB26_48:                              # %clause_GetLiteralAtom.exit
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB26_58
# BB#49:
	movl	cont_BINDINGS(%rip), %ecx
	movslq	cont_STACKPOINTER(%rip), %rdx
	leal	1(%rdx), %esi
	movl	%esi, cont_STACKPOINTER(%rip)
	movl	%ecx, cont_STACK(,%rdx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r13), %rcx
	movslq	64(%r13), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movq	24(%rcx), %rcx
	movq	16(%rcx), %rdx
	movq	8(%rdx), %rsi
	cmpl	(%rcx), %eax
	jne	.LBB26_51
# BB#50:
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
	movq	16(%rcx), %rax
	movq	8(%rax), %rcx
.LBB26_51:                              # %clause_LiteralAtom.exit
	movq	16(%rcx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdx
	callq	unify_UnifyCom
	testl	%eax, %eax
	je	.LBB26_53
# BB#52:
	movslq	64(%r13), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%rbx, (%rax)
	movq	%rax, %rbx
.LBB26_53:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB26_56
# BB#54:                                # %.lr.ph.i49.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB26_55:                              # %.lr.ph.i49
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB26_55
.LBB26_56:                              # %._crit_edge.i
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB26_58
# BB#57:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB26_58:                              # %list_PointerMember.exit
	testq	%rbx, %rbx
	je	.LBB26_69
# BB#59:
	cmpl	$0, 68(%r14)
	je	.LBB26_61
# BB#60:
	movq	stdout(%rip), %rcx
	movl	$.L.str.21, %edi
	movl	$10, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r13, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
.LBB26_61:
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	je	.LBB26_70
# BB#62:
	movq	%r13, %rdi
	callq	clause_Copy
	movq	%rax, %rbp
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiterals
	movq	32(%rbp), %rax
	testq	%rax, %rax
	je	.LBB26_64
	.p2align	4, 0x90
.LBB26_63:                              # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB26_63
.LBB26_64:                              # %list_Delete.exit.i
	movq	40(%rbp), %rax
	testq	%rax, %rax
	je	.LBB26_66
	.p2align	4, 0x90
.LBB26_65:                              # %.lr.ph.i14.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB26_65
.LBB26_66:                              # %red_DocumentObviousReductions.exit
	movq	$0, 32(%rbp)
	movq	%rbx, 40(%rbp)
	movslq	(%rbp), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 32(%rbp)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%rbp)
	movl	$19, 76(%rbp)
	cmpl	$0, 68(%r14)
	je	.LBB26_68
# BB#67:
	movq	%rbp, %rdi
	callq	clause_Print
.LBB26_68:
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
.LBB26_69:
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_70:                              # %.lr.ph.i.preheader
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	callq	clause_DeleteLiterals
	.p2align	4, 0x90
.LBB26_71:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB26_71
# BB#72:                                # %list_Delete.exit
	cmpl	$0, 68(%r14)
	je	.LBB26_69
# BB#73:
	movq	%r13, %rdi
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	clause_Print            # TAILCALL
.Lfunc_end26:
	.size	red_ObviousReductions, .Lfunc_end26-red_ObviousReductions
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_Condensing,@function
red_Condensing:                         # @red_Condensing
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi314:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi315:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi316:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi317:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi318:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi319:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi320:
	.cfi_def_cfa_offset 64
.Lcfi321:
	.cfi_offset %rbx, -56
.Lcfi322:
	.cfi_offset %r12, -48
.Lcfi323:
	.cfi_offset %r13, -40
.Lcfi324:
	.cfi_offset %r14, -32
.Lcfi325:
	.cfi_offset %r15, -24
.Lcfi326:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movq	%rcx, %r13
	movq	%rdx, %r15
	movl	%esi, %ebp
	movq	%rdi, %r12
	callq	cond_CondFast
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB27_14
# BB#1:
	cmpl	$0, 60(%r15)
	je	.LBB27_3
# BB#2:
	movq	stdout(%rip), %rcx
	movl	$.L.str.23, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movq	%r12, %rdi
	callq	clause_Print
	movq	stdout(%rip), %rcx
	movl	$.L.str.22, %edi
	movl	$5, %esi
	movl	$1, %edx
	callq	fwrite
.LBB27_3:
	testl	%ebp, %ebp
	je	.LBB27_11
# BB#4:
	movq	%r12, %rdi
	callq	clause_Copy
	movq	%rax, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	clause_DeleteLiterals
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.LBB27_6
	.p2align	4, 0x90
.LBB27_5:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB27_5
.LBB27_6:                               # %list_Delete.exit.i
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.LBB27_8
	.p2align	4, 0x90
.LBB27_7:                               # %.lr.ph.i14.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB27_7
.LBB27_8:                               # %red_DocumentCondensing.exit
	movq	$0, 32(%r12)
	movq	%rbx, 40(%r12)
	movslq	(%r12), %rbx
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 32(%r12)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r12)
	movl	$17, 76(%r12)
	cmpl	$0, 60(%r15)
	je	.LBB27_10
# BB#9:
	movq	%r12, %rdi
	callq	clause_Print
.LBB27_10:
	movq	%r12, (%r14)
.LBB27_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB27_11:                              # %.lr.ph.i.preheader
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	clause_DeleteLiterals
	.p2align	4, 0x90
.LBB27_12:                              # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB27_12
# BB#13:                                # %list_Delete.exit
	cmpl	$0, 60(%r15)
	je	.LBB27_14
# BB#15:
	movq	%r12, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	clause_Print            # TAILCALL
.Lfunc_end27:
	.size	red_Condensing, .Lfunc_end27-red_Condensing
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_DocumentRewriting,@function
red_DocumentRewriting:                  # @red_DocumentRewriting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi327:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi328:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi329:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi330:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi331:
	.cfi_def_cfa_offset 48
.Lcfi332:
	.cfi_offset %rbx, -48
.Lcfi333:
	.cfi_offset %r12, -40
.Lcfi334:
	.cfi_offset %r14, -32
.Lcfi335:
	.cfi_offset %r15, -24
.Lcfi336:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB28_2
	.p2align	4, 0x90
.LBB28_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB28_1
.LBB28_2:                               # %list_Delete.exit
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB28_4
	.p2align	4, 0x90
.LBB28_3:                               # %.lr.ph.i17
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB28_3
.LBB28_4:                               # %list_Delete.exit18
	movslq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 32(%rbx)
	movslq	%r12d, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	$0, (%rax)
	movq	%rax, 40(%rbx)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%rbx)
	movl	$21, 76(%rbx)
	movslq	(%r15), %rbp
	movq	32(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 32(%rbx)
	movslq	%r14d, %rbp
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	red_DocumentRewriting, .Lfunc_end28-red_DocumentRewriting
	.cfi_endproc

	.p2align	4, 0x90
	.type	clause_UpdateSplitDataFromPartner,@function
clause_UpdateSplitDataFromPartner:      # @clause_UpdateSplitDataFromPartner
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi337:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi338:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi339:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi340:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi341:
	.cfi_def_cfa_offset 48
.Lcfi342:
	.cfi_offset %rbx, -40
.Lcfi343:
	.cfi_offset %r12, -32
.Lcfi344:
	.cfi_offset %r14, -24
.Lcfi345:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	$8, 48(%r14)
	je	.LBB29_2
# BB#1:
	orb	$8, 48(%rbx)
.LBB29_2:
	movl	12(%r14), %eax
	testl	%eax, %eax
	je	.LBB29_22
# BB#3:
	cmpl	12(%rbx), %eax
	movq	%rbx, %rax
	cmovaq	%r14, %rax
	movl	12(%rax), %eax
	movl	%eax, 12(%rbx)
	movl	24(%r14), %r12d
	cmpl	%r12d, 24(%rbx)
	jae	.LBB29_19
# BB#4:
	leal	(,%r12,8), %edi
	callq	memory_Malloc
	movq	%rax, %r15
	cmpl	$0, 24(%rbx)
	je	.LBB29_5
# BB#15:                                # %.lr.ph27.i.i
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB29_16:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rax
	movl	%ecx, %edx
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%r15,%rdx,8)
	incl	%ecx
	movl	24(%rbx), %eax
	cmpl	%eax, %ecx
	jb	.LBB29_16
	jmp	.LBB29_6
.LBB29_5:
	xorl	%eax, %eax
.LBB29_6:                               # %.preheader.i.i
	cmpl	%r12d, %eax
	jae	.LBB29_8
# BB#7:                                 # %.lr.ph.preheader.i.i
	movl	%eax, %ecx
	leaq	(%r15,%rcx,8), %rdi
	leal	-1(%r12), %ecx
	subl	%eax, %ecx
	leaq	8(,%rcx,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB29_8:                               # %._crit_edge.i.i
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB29_18
# BB#9:
	movl	24(%rbx), %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB29_10
# BB#17:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB29_18
.LBB29_10:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB29_12
# BB#11:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB29_12:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB29_14
# BB#13:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB29_14:
	addq	$-16, %rdi
	callq	free
.LBB29_18:                              # %clause_ExpandSplitField.exit.i
	movq	%r15, 16(%rbx)
	movl	%r12d, 24(%rbx)
	movl	24(%r14), %r12d
.LBB29_19:                              # %.preheader.i
	testl	%r12d, %r12d
	je	.LBB29_22
# BB#20:                                # %.lr.ph.i
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB29_21:                              # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	movl	%eax, %edx
	movq	16(%r14), %rsi
	movq	(%rsi,%rdx,8), %rsi
	orq	%rsi, (%rcx,%rdx,8)
	incl	%eax
	cmpl	24(%r14), %eax
	jb	.LBB29_21
.LBB29_22:                              # %clause_UpdateSplitField.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end29:
	.size	clause_UpdateSplitDataFromPartner, .Lfunc_end29-clause_UpdateSplitDataFromPartner
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_LeftTermOfEquationIsStrictlyMaximalTerm,@function
red_LeftTermOfEquationIsStrictlyMaximalTerm: # @red_LeftTermOfEquationIsStrictlyMaximalTerm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi346:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi347:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi348:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi349:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi350:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi351:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi352:
	.cfi_def_cfa_offset 96
.Lcfi353:
	.cfi_offset %rbx, -56
.Lcfi354:
	.cfi_offset %r12, -48
.Lcfi355:
	.cfi_offset %r13, -40
.Lcfi356:
	.cfi_offset %r14, -32
.Lcfi357:
	.cfi_offset %r15, -24
.Lcfi358:
	.cfi_offset %rbp, -16
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	16(%rsi), %rax
	movq	24(%rsi), %rcx
	movq	16(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	56(%rax), %rcx
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB30_1:                               # =>This Inner Loop Header: Depth=1
	incl	%eax
	cmpq	%rsi, (%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB30_1
# BB#2:                                 # %clause_LiteralGetIndex.exit
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	64(%rdx), %ecx
	addl	68(%rdx), %ecx
	addl	72(%rdx), %ecx
	decl	%ecx
	js	.LBB30_12
# BB#3:                                 # %.lr.ph
	movslq	%ecx, %r14
	movl	%eax, %r15d
	xorl	%r12d, %r12d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB30_4:                               # =>This Inner Loop Header: Depth=1
	cmpq	%r12, %r15
	je	.LBB30_11
# BB#5:                                 #   in Loop: Header=BB30_4 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movq	(%rax,%r12,8), %r13
	movq	24(%r13), %rbx
	movl	(%rbx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB30_7
# BB#6:                                 #   in Loop: Header=BB30_4 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %eax
.LBB30_7:                               # %clause_LiteralAtom.exit
                                        #   in Loop: Header=BB30_4 Depth=1
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB30_13
# BB#8:                                 #   in Loop: Header=BB30_4 Depth=1
	movq	16(%rbx), %rax
	movq	8(%rax), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
	cmpl	$3, %eax
	jne	.LBB30_15
# BB#9:                                 #   in Loop: Header=BB30_4 Depth=1
	cmpl	$0, 8(%r13)
	jne	.LBB30_11
# BB#10:                                #   in Loop: Header=BB30_4 Depth=1
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rsi
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
	cmpl	$3, %eax
	je	.LBB30_11
	jmp	.LBB30_15
	.p2align	4, 0x90
.LBB30_13:                              #   in Loop: Header=BB30_4 Depth=1
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rbx, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	callq	ord_Compare
	cmpl	$3, %eax
	jne	.LBB30_14
.LBB30_11:                              #   in Loop: Header=BB30_4 Depth=1
	cmpq	%r14, %r12
	leaq	1(%r12), %r12
	jl	.LBB30_4
.LBB30_12:
	movl	$1, %ebp
.LBB30_15:                              # %._crit_edge
	movl	%ebp, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB30_14:
	xorl	%ebp, %ebp
	jmp	.LBB30_15
.Lfunc_end30:
	.size	red_LeftTermOfEquationIsStrictlyMaximalTerm, .Lfunc_end30-red_LeftTermOfEquationIsStrictlyMaximalTerm
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_CRwTautologyCheck,@function
red_CRwTautologyCheck:                  # @red_CRwTautologyCheck
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi359:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi360:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi361:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi362:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi363:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi364:
	.cfi_def_cfa_offset 56
	subq	$216, %rsp
.Lcfi365:
	.cfi_def_cfa_offset 272
.Lcfi366:
	.cfi_offset %rbx, -56
.Lcfi367:
	.cfi_offset %r12, -48
.Lcfi368:
	.cfi_offset %r13, -40
.Lcfi369:
	.cfi_offset %r14, -32
.Lcfi370:
	.cfi_offset %r15, -24
.Lcfi371:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movq	%rsi, %rbp
	movq	280(%rsp), %rax
	movq	104(%rdi), %rbx
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movq	112(%rdi), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	$0, (%rax)
	movq	%r8, %rdi
	callq	clause_Copy
	movq	%rax, %r12
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	movl	52(%rbp), %esi
	movq	%r12, %rdi
	callq	clause_RenameVarsBiggerThan
	movq	56(%r12), %rax
	movslq	%r14d, %rbp
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rax
	movl	fol_NOT(%rip), %ecx
	cmpl	(%rax), %ecx
	jne	.LBB31_2
# BB#1:
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB31_2:                               # %clause_GetLiteralAtom.exit
	movq	16(%rax), %rax
	movq	8(%rax), %rsi
	movq	32(%r12), %rax
	testq	%rax, %rax
	je	.LBB31_4
	.p2align	4, 0x90
.LBB31_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB31_3
.LBB31_4:                               # %list_Delete.exit
	movq	$0, 32(%r12)
	movq	40(%r12), %rax
	testq	%rax, %rax
	je	.LBB31_6
	.p2align	4, 0x90
.LBB31_5:                               # %.lr.ph.i129
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB31_5
.LBB31_6:                               # %list_Delete.exit130
	movq	$0, 40(%r12)
	movl	$27, 76(%r12)
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movl	$0, cont_BINDINGS(%rip)
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	96(%rsp), %rdx          # 8-byte Reload
	callq	unify_MatchBindings
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB31_8
# BB#7:
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB31_8:                               # %clause_GetLiteralAtom.exit140
	movl	$1, %edx
	callq	cont_ApplyBindingsModuloMatching
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	68(%rcx), %eax
	addl	64(%rcx), %eax
	cmpl	28(%rsp), %eax          # 4-byte Folded Reload
	jg	.LBB31_13
# BB#9:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	56(%rax), %rax
	movslq	28(%rsp), %rcx          # 4-byte Folded Reload
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdi
	movl	(%rdi), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB31_11
# BB#10:
	movq	16(%rdi), %rcx
	movq	8(%rcx), %rcx
	movl	(%rcx), %ecx
.LBB31_11:                              # %clause_LiteralIsEquality.exit
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB31_13
# BB#12:
	movl	8(%rax), %esi
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rdx
	movq	%rbx, (%rsp)
	movl	$1, %ecx
	xorl	%r8d, %r8d
	movq	88(%rsp), %r9           # 8-byte Reload
	callq	ord_LiteralCompare
	cmpl	$3, %eax
	jne	.LBB31_165
.LBB31_13:
	movl	68(%r12), %ebx
	addl	64(%r12), %ebx
	addl	72(%r12), %ebx
	movl	%ebx, %eax
	decl	%eax
	js	.LBB31_20
# BB#14:                                # %.lr.ph197.preheader
	movl	%r14d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB31_15:                              # %.lr.ph197
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%rbp, %r15
	je	.LBB31_19
# BB#16:                                #   in Loop: Header=BB31_15 Depth=1
	movq	cont_LEFTCONTEXT(%rip), %rdi
	movq	56(%r12), %rax
	movq	(%rax,%rbp,8), %rax
	movq	24(%rax), %rsi
	movl	fol_NOT(%rip), %eax
	cmpl	(%rsi), %eax
	jne	.LBB31_18
# BB#17:                                #   in Loop: Header=BB31_15 Depth=1
	movq	16(%rsi), %rax
	movq	8(%rax), %rsi
.LBB31_18:                              # %clause_GetLiteralAtom.exit162
                                        #   in Loop: Header=BB31_15 Depth=1
	xorl	%edx, %edx
	callq	cont_ApplyBindingsModuloMatching
.LBB31_19:                              #   in Loop: Header=BB31_15 Depth=1
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB31_15
.LBB31_20:                              # %._crit_edge
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB31_23
# BB#21:                                # %.lr.ph.i166.preheader
	incl	%eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB31_22:                              # %.lr.ph.i166
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movups	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB31_22
.LBB31_23:                              # %._crit_edge.i167
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	movq	88(%rsp), %rbx          # 8-byte Reload
	je	.LBB31_25
# BB#24:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB31_25:                              # %vector.body
	movl	$384, %edi              # imm = 0x180
	callq	memory_Malloc
	movq	%rax, %rbp
	movd	flag_CLEAN(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqu	%xmm0, (%rbp)
	movdqu	%xmm0, 16(%rbp)
	movdqu	%xmm0, 32(%rbp)
	movdqu	%xmm0, 48(%rbp)
	movdqu	%xmm0, 64(%rbp)
	movdqu	%xmm0, 80(%rbp)
	movdqu	%xmm0, 96(%rbp)
	movdqu	%xmm0, 112(%rbp)
	movdqu	%xmm0, 128(%rbp)
	movdqu	%xmm0, 144(%rbp)
	movdqu	%xmm0, 160(%rbp)
	movdqu	%xmm0, 176(%rbp)
	movdqu	%xmm0, 192(%rbp)
	movdqu	%xmm0, 208(%rbp)
	movdqu	%xmm0, 224(%rbp)
	movdqu	%xmm0, 240(%rbp)
	movdqu	%xmm0, 256(%rbp)
	movdqu	%xmm0, 272(%rbp)
	movdqu	%xmm0, 288(%rbp)
	movdqu	%xmm0, 304(%rbp)
	movdqu	%xmm0, 320(%rbp)
	movdqu	%xmm0, 336(%rbp)
	movdqu	%xmm0, 352(%rbp)
	movdqu	%xmm0, 368(%rbp)
	leaq	384(%rbp), %rcx
	leaq	384(%rbx), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	cmpq	%rax, %rbp
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	jae	.LBB31_29
# BB#26:                                # %vector.body
	cmpq	%rcx, %rbx
	jae	.LBB31_29
# BB#27:                                # %flag_CreateStore.exit.preheader
	movl	$7, %eax
	.p2align	4, 0x90
.LBB31_28:                              # %flag_CreateStore.exit
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rbx,%rax,4), %ecx
	movl	%ecx, -28(%rbp,%rax,4)
	movl	-24(%rbx,%rax,4), %ecx
	movl	%ecx, -24(%rbp,%rax,4)
	movl	-20(%rbx,%rax,4), %ecx
	movl	%ecx, -20(%rbp,%rax,4)
	movl	-16(%rbx,%rax,4), %ecx
	movl	%ecx, -16(%rbp,%rax,4)
	movl	-12(%rbx,%rax,4), %ecx
	movl	%ecx, -12(%rbp,%rax,4)
	movl	-8(%rbx,%rax,4), %ecx
	movl	%ecx, -8(%rbp,%rax,4)
	movl	-4(%rbx,%rax,4), %ecx
	movl	%ecx, -4(%rbp,%rax,4)
	movl	(%rbx,%rax,4), %ecx
	movl	%ecx, (%rbp,%rax,4)
	addq	$8, %rax
	cmpq	$103, %rax
	jne	.LBB31_28
	jmp	.LBB31_30
.LBB31_29:                              # %vector.body212
	movups	(%rbx), %xmm0
	movups	%xmm0, (%rbp)
	movups	16(%rbx), %xmm0
	movups	%xmm0, 16(%rbp)
	movups	32(%rbx), %xmm0
	movups	%xmm0, 32(%rbp)
	movups	48(%rbx), %xmm0
	movups	%xmm0, 48(%rbp)
	movups	64(%rbx), %xmm0
	movups	%xmm0, 64(%rbp)
	movups	80(%rbx), %xmm0
	movups	%xmm0, 80(%rbp)
	movups	96(%rbx), %xmm0
	movups	%xmm0, 96(%rbp)
	movups	112(%rbx), %xmm0
	movups	%xmm0, 112(%rbp)
	movups	128(%rbx), %xmm0
	movups	%xmm0, 128(%rbp)
	movups	144(%rbx), %xmm0
	movups	%xmm0, 144(%rbp)
	movups	160(%rbx), %xmm0
	movups	%xmm0, 160(%rbp)
	movups	176(%rbx), %xmm0
	movups	%xmm0, 176(%rbp)
	movups	192(%rbx), %xmm0
	movups	%xmm0, 192(%rbp)
	movups	208(%rbx), %xmm0
	movups	%xmm0, 208(%rbp)
	movups	224(%rbx), %xmm0
	movups	%xmm0, 224(%rbp)
	movups	240(%rbx), %xmm0
	movups	%xmm0, 240(%rbp)
	movups	256(%rbx), %xmm0
	movups	%xmm0, 256(%rbp)
	movups	272(%rbx), %xmm0
	movups	%xmm0, 272(%rbp)
	movups	288(%rbx), %xmm0
	movups	%xmm0, 288(%rbp)
	movups	304(%rbx), %xmm0
	movups	%xmm0, 304(%rbp)
	movups	320(%rbx), %xmm0
	movups	%xmm0, 320(%rbp)
	movups	336(%rbx), %xmm0
	movups	%xmm0, 336(%rbp)
	movups	352(%rbx), %xmm0
	movups	%xmm0, 352(%rbp)
	movdqu	368(%rbx), %xmm0
	movdqu	%xmm0, 368(%rbp)
.LBB31_30:                              # %flag_TransferAllFlags.exit
	movq	%rbx, %rdi
	callq	flag_ClearPrinting
	movl	$82, %esi
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	$81, %esi
	movq	%rbx, %rdi
	callq	flag_SetFlagValue
	movl	64(%r12), %eax
	addl	68(%r12), %eax
	addl	72(%r12), %eax
	decl	%eax
	movl	clause_CLAUSECOUNTER(%rip), %ecx
	movl	%ecx, 140(%rsp)         # 4-byte Spill
	movb	$1, %cl
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movl	$1, 68(%rsp)            # 4-byte Folded Spill
	js	.LBB31_157
# BB#31:                                # %.lr.ph
	movl	%r14d, %ecx
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	cltq
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%r12, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB31_32:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB31_37 Depth 2
                                        #     Child Loop BB31_43 Depth 2
                                        #     Child Loop BB31_45 Depth 2
                                        #     Child Loop BB31_55 Depth 2
                                        #     Child Loop BB31_57 Depth 2
                                        #     Child Loop BB31_60 Depth 2
                                        #       Child Loop BB31_78 Depth 3
                                        #       Child Loop BB31_94 Depth 3
                                        #     Child Loop BB31_102 Depth 2
                                        #     Child Loop BB31_115 Depth 2
                                        #       Child Loop BB31_118 Depth 3
                                        #     Child Loop BB31_126 Depth 2
                                        #     Child Loop BB31_129 Depth 2
                                        #     Child Loop BB31_131 Depth 2
                                        #     Child Loop BB31_143 Depth 2
                                        #     Child Loop BB31_149 Depth 2
	cmpq	208(%rsp), %rcx         # 8-byte Folded Reload
	je	.LBB31_154
# BB#33:                                #   in Loop: Header=BB31_32 Depth=1
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	104(%rax), %rsi
	movq	112(%rax), %rdi
	movl	36(%rdi), %ebx
	movq	56(%r12), %rax
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %r14
	movl	(%r14), %r15d
	movl	fol_NOT(%rip), %r12d
	cmpl	%r15d, %r12d
	jne	.LBB31_35
# BB#34:                                #   in Loop: Header=BB31_32 Depth=1
	movq	16(%r14), %rax
	movq	8(%rax), %r14
.LBB31_35:                              # %clause_LiteralAtom.exit.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movslq	64(%rax), %rax
	cmpq	%rcx, %rax
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	jle	.LBB31_48
# BB#36:                                #   in Loop: Header=BB31_32 Depth=1
	movl	%ebx, %r13d
	movq	%r14, %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	clause_Create
	movq	%rax, 8(%rsp)
	movl	$27, 76(%rax)
	testq	%rbx, %rbx
	je	.LBB31_39
	.p2align	4, 0x90
.LBB31_37:                              # %.lr.ph.i.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB31_37
# BB#38:                                # %list_Delete.exit.i.loopexit
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	8(%rsp), %rax
.LBB31_39:                              # %list_Delete.exit.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	$0, 56(%rsp)
	movslq	(%rax), %rbp
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	80(%rcx), %rdi
	leaq	56(%rsp), %rcx
	movq	%rcx, (%rsp)
	movl	$-1, %edx
	movq	%rax, %rsi
	movl	%r13d, %ebx
	movl	%ebx, %ecx
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	red_SortSimplification
	testl	%eax, %eax
	je	.LBB31_47
# BB#40:                                #   in Loop: Header=BB31_32 Depth=1
	testl	%ebx, %ebx
	movq	56(%rsp), %rbx
	je	.LBB31_133
# BB#41:                                #   in Loop: Header=BB31_32 Depth=1
	movq	32(%rbx), %rdi
	movq	$0, 32(%rbx)
	movq	%rbp, %rsi
	callq	list_PointerDeleteElement
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, 32(%rbx)
	movq	%rax, %rdi
	callq	list_Copy
	testq	%rax, %rax
	je	.LBB31_44
# BB#42:                                # %.lr.ph.i73.i.preheader
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB31_43:                              # %.lr.ph.i73.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$0, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB31_43
.LBB31_44:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	40(%rbx), %rcx
	testq	%rcx, %rcx
	movq	48(%rsp), %r12          # 8-byte Reload
	je	.LBB31_46
	.p2align	4, 0x90
.LBB31_45:                              # %.lr.ph.i.i.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB31_45
.LBB31_46:                              # %.thread
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, 40(%rbx)
	movq	8(%rsp), %rbp
	jmp	.LBB31_134
.LBB31_47:                              #   in Loop: Header=BB31_32 Depth=1
	movq	8(%rsp), %rdi
	callq	clause_Delete
	movq	168(%rsp), %rcx         # 8-byte Reload
.LBB31_48:                              #   in Loop: Header=BB31_32 Depth=1
	cmpl	%r15d, %r12d
	movl	%ebx, 148(%rsp)         # 4-byte Spill
	jne	.LBB31_51
# BB#49:                                #   in Loop: Header=BB31_32 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movslq	64(%rax), %rax
	cmpq	%rcx, %rax
	jle	.LBB31_52
# BB#50:                                #   in Loop: Header=BB31_32 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	clause_CopyConstraint
	jmp	.LBB31_53
.LBB31_51:                              #   in Loop: Header=BB31_32 Depth=1
	movq	%r14, %rdi
	callq	term_Copy
	movq	%rax, %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	movq	%rbp, 8(%rbx)
	movq	$0, (%rbx)
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	clause_CopySuccedentExcept
	movq	%rax, %rbp
	jmp	.LBB31_54
.LBB31_52:                              #   in Loop: Header=BB31_32 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	callq	clause_CopyAntecedentExcept
.LBB31_53:                              #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, %rbx
	movq	%r14, %rdi
	callq	term_Copy
	movq	%rax, %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbp
	movq	%r14, 8(%rbp)
	movq	$0, (%rbp)
.LBB31_54:                              #   in Loop: Header=BB31_32 Depth=1
	xorl	%edi, %edi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r8           # 8-byte Reload
	callq	clause_Create
	movq	%rax, 8(%rsp)
	movl	$27, 76(%rax)
	testq	%rbx, %rbx
	je	.LBB31_56
	.p2align	4, 0x90
.LBB31_55:                              # %.lr.ph.i77.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbx)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbx, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbx
	jne	.LBB31_55
.LBB31_56:                              # %list_Delete.exit79.i
                                        #   in Loop: Header=BB31_32 Depth=1
	testq	%rbp, %rbp
	je	.LBB31_58
	.p2align	4, 0x90
.LBB31_57:                              # %.lr.ph.i83.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rbp)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%rbp, (%rcx)
	testq	%rax, %rax
	movq	%rax, %rbp
	jne	.LBB31_57
.LBB31_58:                              # %list_Delete.exit85.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	8(%rsp), %r13
	movslq	64(%r13), %rbp
	movl	68(%r13), %eax
	leal	-1(%rbp,%rax), %eax
	cmpl	%eax, %ebp
	jg	.LBB31_104
# BB#59:                                # %.lr.ph60.i.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movslq	%eax, %r15
	xorl	%r14d, %r14d
	movl	$0, 76(%rsp)            # 4-byte Folded Spill
	movq	%r15, 152(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB31_60:                              #   Parent Loop BB31_32 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_78 Depth 3
                                        #       Child Loop BB31_94 Depth 3
	movq	56(%r13), %rax
	movq	(%rax,%rbp,8), %rbx
	movq	24(%rbx), %rax
	movl	(%rax), %ecx
	cmpl	%ecx, fol_NOT(%rip)
	jne	.LBB31_62
# BB#61:                                #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %ecx
.LBB31_62:                              # %clause_LiteralAtom.exit.i.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	cmpl	%ecx, fol_EQUALITY(%rip)
	jne	.LBB31_99
# BB#63:                                #   in Loop: Header=BB31_60 Depth=2
	cmpl	$0, 8(%rbx)
	jne	.LBB31_99
# BB#64:                                #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	cmpl	$0, (%rdi)
	movq	8(%rcx), %rsi
	jg	.LBB31_66
# BB#65:                                #   in Loop: Header=BB31_60 Depth=2
	cmpl	$0, (%rsi)
	jle	.LBB31_99
.LBB31_66:                              # %._crit_edge.i47.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	callq	term_VariableEqual
	testl	%eax, %eax
	jne	.LBB31_99
# BB#67:                                # %red_LiteralIsDefinition.exit.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	movq	24(%rbx), %rax
	movl	(%rax), %ecx
	movl	fol_NOT(%rip), %edx
	cmpl	%ecx, %edx
	movq	%rax, %rsi
	jne	.LBB31_69
# BB#68:                                #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rax), %rsi
	movq	8(%rsi), %rsi
.LBB31_69:                              # %clause_LiteralAtom.exit46.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rsi), %rsi
	movq	8(%rsi), %rdi
	movl	(%rdi), %esi
	testl	%esi, %esi
	pxor	%xmm0, %xmm0
	jle	.LBB31_73
# BB#70:                                #   in Loop: Header=BB31_60 Depth=2
	cmpl	%ecx, %edx
	jne	.LBB31_72
# BB#71:                                #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB31_72:                              # %clause_LiteralAtom.exit38.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rdi
	jmp	.LBB31_76
.LBB31_73:                              #   in Loop: Header=BB31_60 Depth=2
	cmpl	%ecx, %edx
	jne	.LBB31_75
# BB#74:                                #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rax), %rax
	movq	8(%rax), %rax
.LBB31_75:                              # %clause_LiteralAtom.exit30.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
.LBB31_76:                              #   in Loop: Header=BB31_60 Depth=2
	movl	cont_BINDINGS(%rip), %eax
	movslq	cont_STACKPOINTER(%rip), %rcx
	leal	1(%rcx), %edx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	%eax, cont_STACK(,%rcx,4)
	movq	cont_LEFTCONTEXT(%rip), %rax
	movq	cont_INSTANCECONTEXT(%rip), %rcx
	movl	%esi, 144(%rsp)         # 4-byte Spill
	movslq	%esi, %rdx
	shlq	$5, %rdx
	leaq	(%rax,%rdx), %rsi
	movq	%rsi, cont_CURRENTBINDING(%rip)
	movq	%rdi, 192(%rsp)         # 8-byte Spill
	movq	%rdi, 8(%rax,%rdx)
	movq	%rcx, 16(%rax,%rdx)
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, 24(%rax,%rdx)
	movq	%rsi, cont_LASTBINDING(%rip)
	movl	$1, cont_BINDINGS(%rip)
	movl	64(%r13), %eax
	addl	68(%r13), %eax
	addl	72(%r13), %eax
	decl	%eax
	js	.LBB31_89
# BB#77:                                # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	movq	%r14, 184(%rsp)         # 8-byte Spill
	movl	%ebp, %ecx
	movslq	%eax, %rdx
	movl	$1, %r14d
	xorl	%r15d, %r15d
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB31_78:                              # %.lr.ph.i88.i
                                        #   Parent Loop BB31_32 Depth=1
                                        #     Parent Loop BB31_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%r15, %rcx
	je	.LBB31_87
# BB#79:                                #   in Loop: Header=BB31_78 Depth=3
	movq	%r13, %r12
	movq	56(%r13), %rax
	movq	(%rax,%r15,8), %r13
	movq	24(%r13), %rbx
	movl	(%rbx), %eax
	cmpl	%eax, fol_NOT(%rip)
	jne	.LBB31_81
# BB#80:                                #   in Loop: Header=BB31_78 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rbx
	movl	(%rbx), %eax
.LBB31_81:                              # %clause_LiteralAtom.exit.i.i
                                        #   in Loop: Header=BB31_78 Depth=3
	movq	cont_INSTANCECONTEXT(%rip), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rdx
	cmpl	%eax, fol_EQUALITY(%rip)
	jne	.LBB31_85
# BB#82:                                #   in Loop: Header=BB31_78 Depth=3
	movq	16(%rbx), %rax
	movq	8(%rax), %rcx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	ord_ContGreater
	testl	%eax, %eax
	je	.LBB31_90
# BB#83:                                #   in Loop: Header=BB31_78 Depth=3
	movl	$1, %r14d
	cmpl	$0, 8(%r13)
	jne	.LBB31_86
# BB#84:                                #   in Loop: Header=BB31_78 Depth=3
	movq	cont_INSTANCECONTEXT(%rip), %rdi
	movq	cont_LEFTCONTEXT(%rip), %rdx
	movq	16(%rbx), %rax
	movq	(%rax), %rax
	movq	8(%rax), %rcx
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	ord_ContGreater
	testl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %r13
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
	jne	.LBB31_87
	jmp	.LBB31_91
	.p2align	4, 0x90
.LBB31_85:                              #   in Loop: Header=BB31_78 Depth=3
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rcx
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	16(%rsp), %r9           # 8-byte Reload
	callq	ord_ContGreater
	xorl	%r14d, %r14d
	testl	%eax, %eax
	setne	%r14b
.LBB31_86:                              #   in Loop: Header=BB31_78 Depth=3
	pxor	%xmm0, %xmm0
	movq	%r12, %r13
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	112(%rsp), %rdx         # 8-byte Reload
.LBB31_87:                              #   in Loop: Header=BB31_78 Depth=3
	testl	%r14d, %r14d
	setne	%al
	cmpq	%rdx, %r15
	jge	.LBB31_92
# BB#88:                                #   in Loop: Header=BB31_78 Depth=3
	incq	%r15
	testl	%r14d, %r14d
	jne	.LBB31_78
	jmp	.LBB31_92
.LBB31_89:                              #   in Loop: Header=BB31_60 Depth=2
	movl	$1, %ecx
	movb	$1, %al
	movq	152(%rsp), %r15         # 8-byte Reload
	jmp	.LBB31_93
.LBB31_90:                              #   in Loop: Header=BB31_60 Depth=2
	xorl	%eax, %eax
	pxor	%xmm0, %xmm0
	movq	%r12, %r13
	jmp	.LBB31_92
.LBB31_91:                              #   in Loop: Header=BB31_60 Depth=2
	xorl	%eax, %eax
.LBB31_92:                              # %._crit_edge.i89.i
                                        #   in Loop: Header=BB31_60 Depth=2
	movl	cont_BINDINGS(%rip), %ecx
	testl	%ecx, %ecx
	movq	184(%rsp), %r14         # 8-byte Reload
	movq	152(%rsp), %r15         # 8-byte Reload
	jle	.LBB31_95
.LBB31_93:                              # %.lr.ph.i22.preheader.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	incl	%ecx
	.p2align	4, 0x90
.LBB31_94:                              # %.lr.ph.i22.i.i
                                        #   Parent Loop BB31_32 Depth=1
                                        #     Parent Loop BB31_60 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	cont_LASTBINDING(%rip), %rdx
	movq	%rdx, cont_CURRENTBINDING(%rip)
	movq	24(%rdx), %rsi
	movq	%rsi, cont_LASTBINDING(%rip)
	movdqu	%xmm0, 4(%rdx)
	movl	$0, 20(%rdx)
	movq	cont_CURRENTBINDING(%rip), %rdx
	movq	$0, 24(%rdx)
	leal	-2(%rcx), %edx
	movl	%edx, cont_BINDINGS(%rip)
	decl	%ecx
	cmpl	$1, %ecx
	jg	.LBB31_94
.LBB31_95:                              # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	movslq	cont_STACKPOINTER(%rip), %rcx
	testq	%rcx, %rcx
	je	.LBB31_97
# BB#96:                                #   in Loop: Header=BB31_60 Depth=2
	leaq	-1(%rcx), %rdx
	movl	%edx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rcx,4), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
.LBB31_97:                              # %cont_BackTrack.exit.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	testb	%al, %al
	je	.LBB31_99
# BB#98:                                #   in Loop: Header=BB31_60 Depth=2
	movq	%r13, %rdi
	movl	144(%rsp), %esi         # 4-byte Reload
	movq	192(%rsp), %rdx         # 8-byte Reload
	callq	clause_ReplaceVariable
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movl	$1, 76(%rsp)            # 4-byte Folded Spill
	movq	%rax, %r14
	.p2align	4, 0x90
.LBB31_99:                              # %red_LiteralIsDefinition.exit.thread.i.i
                                        #   in Loop: Header=BB31_60 Depth=2
	cmpq	%r15, %rbp
	leaq	1(%rbp), %rbp
	jl	.LBB31_60
# BB#100:                               # %._crit_edge61.i.i
                                        #   in Loop: Header=BB31_32 Depth=1
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	je	.LBB31_104
# BB#101:                               #   in Loop: Header=BB31_32 Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	callq	clause_DeleteLiterals
	testq	%r14, %r14
	je	.LBB31_103
	.p2align	4, 0x90
.LBB31_102:                             # %.lr.ph.i.i93.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r14)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r14, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r14
	jne	.LBB31_102
.LBB31_103:                             # %list_Delete.exit.i.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	clause_OrientEqualities
.LBB31_104:                             # %red_PropagateDefinitions.exit.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	$0, 56(%rsp)
	movq	$0, 128(%rsp)
	movq	8(%rsp), %rax
	movslq	(%rax), %rbx
	movq	104(%rsp), %rdi         # 8-byte Reload
	leaq	8(%rsp), %rsi
	leaq	56(%rsp), %rdx
	leaq	128(%rsp), %rcx
	movl	272(%rsp), %r8d
	callq	red_SelectedStaticReductions
	movq	8(%rsp), %rdi
	movl	$27, 76(%rdi)
	testl	%eax, %eax
	jne	.LBB31_110
# BB#105:                               #   in Loop: Header=BB31_32 Depth=1
	testq	%rdi, %rdi
	je	.LBB31_109
# BB#106:                               #   in Loop: Header=BB31_32 Depth=1
	cmpl	$0, 68(%rdi)
	jne	.LBB31_109
# BB#107:                               #   in Loop: Header=BB31_32 Depth=1
	cmpl	$0, 72(%rdi)
	jne	.LBB31_109
# BB#108:                               # %clause_IsEmptyClause.exit.i
                                        #   in Loop: Header=BB31_32 Depth=1
	cmpl	$0, 64(%rdi)
	je	.LBB31_136
.LBB31_109:                             # %clause_IsEmptyClause.exit.i.thread
                                        #   in Loop: Header=BB31_32 Depth=1
	callq	cc_Tautology
	testl	%eax, %eax
	je	.LBB31_135
.LBB31_110:                             #   in Loop: Header=BB31_32 Depth=1
	movq	56(%rsp), %r12
	testq	%r12, %r12
	je	.LBB31_112
# BB#111:                               #   in Loop: Header=BB31_32 Depth=1
	movq	8(%rsp), %rdi
	movq	%r12, %rsi
	callq	clause_UpdateSplitDataFromPartner
.LBB31_112:                             #   in Loop: Header=BB31_32 Depth=1
	cmpl	$0, 148(%rsp)           # 4-byte Folded Reload
	je	.LBB31_137
# BB#113:                               #   in Loop: Header=BB31_32 Depth=1
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	8(%rsp), %r15
	movq	128(%rsp), %rbx
	movq	32(%r15), %r13
	movq	$0, 32(%r15)
	testq	%rbx, %rbx
	je	.LBB31_122
# BB#114:                               # %.lr.ph53.i.i.preheader
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB31_115:                             # %.lr.ph53.i.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB31_118 Depth 3
	movq	8(%rbp), %rax
	movq	32(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB31_120
# BB#116:                               #   in Loop: Header=BB31_115 Depth=2
	callq	list_Copy
	movq	%rax, %r14
	testq	%r13, %r13
	je	.LBB31_121
# BB#117:                               # %.preheader.i.i.i.preheader
                                        #   in Loop: Header=BB31_115 Depth=2
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB31_118:                             # %.preheader.i.i.i
                                        #   Parent Loop BB31_32 Depth=1
                                        #     Parent Loop BB31_115 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB31_118
# BB#119:                               #   in Loop: Header=BB31_115 Depth=2
	movq	%r13, (%rax)
	jmp	.LBB31_121
	.p2align	4, 0x90
.LBB31_120:                             #   in Loop: Header=BB31_115 Depth=2
	movq	%r13, %r14
.LBB31_121:                             # %list_Append.exit.i.i
                                        #   in Loop: Header=BB31_115 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%r14, %r13
	jne	.LBB31_115
	jmp	.LBB31_123
.LBB31_122:                             #   in Loop: Header=BB31_32 Depth=1
	movq	%r13, %r14
.LBB31_123:                             # %._crit_edge54.i.i
                                        #   in Loop: Header=BB31_32 Depth=1
	testq	%r12, %r12
	je	.LBB31_125
# BB#124:                               #   in Loop: Header=BB31_32 Depth=1
	movslq	(%r12), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, %r14
.LBB31_125:                             #   in Loop: Header=BB31_32 Depth=1
	movq	%r14, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	list_PointerDeleteElement
	testq	%rbx, %rbx
	je	.LBB31_127
	.p2align	4, 0x90
.LBB31_126:                             # %.lr.ph48.i.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbx), %rcx
	movslq	(%rcx), %rsi
	movq	%rax, %rdi
	callq	list_PointerDeleteElement
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB31_126
.LBB31_127:                             # %._crit_edge49.i.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, %rdi
	callq	list_PointerDeleteDuplicates
	movq	%rax, 32(%r15)
	movq	%rax, %rdi
	callq	list_Copy
	testq	%rax, %rax
	je	.LBB31_130
# BB#128:                               # %.lr.ph.i107.i.preheader
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB31_129:                             # %.lr.ph.i107.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	$0, 8(%rcx)
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB31_129
.LBB31_130:                             # %._crit_edge.i110.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	40(%r15), %rcx
	testq	%rcx, %rcx
	je	.LBB31_132
	.p2align	4, 0x90
.LBB31_131:                             # %.lr.ph.i.i114.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB31_131
.LBB31_132:                             # %red_CRwCalculateAdditionalParents.exit116.i
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, 40(%r15)
	jmp	.LBB31_137
.LBB31_133:                             #   in Loop: Header=BB31_32 Depth=1
	testq	%rbx, %rbx
	movq	8(%rsp), %rbp
	movq	48(%rsp), %r12          # 8-byte Reload
	je	.LBB31_138
.LBB31_134:                             #   in Loop: Header=BB31_32 Depth=1
	movq	%rbp, %rdi
	callq	clause_Delete
	movq	%rbx, %rbp
	testq	%rbp, %rbp
	jne	.LBB31_139
	jmp	.LBB31_156
.LBB31_135:                             # %clause_IsEmptyClause.exit.i.thread._crit_edge
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	8(%rsp), %rdi
.LBB31_136:                             #   in Loop: Header=BB31_32 Depth=1
	callq	clause_Delete
	movq	$0, 8(%rsp)
	.p2align	4, 0x90
.LBB31_137:                             #   in Loop: Header=BB31_32 Depth=1
	movq	128(%rsp), %rdi
	callq	clause_DeleteClauseList
	movq	8(%rsp), %rbp
	movq	48(%rsp), %r12          # 8-byte Reload
.LBB31_138:                             # %red_CRwLitTautologyCheck.exit
                                        #   in Loop: Header=BB31_32 Depth=1
	testq	%rbp, %rbp
	je	.LBB31_156
.LBB31_139:                             #   in Loop: Header=BB31_32 Depth=1
	movq	%r12, %rdi
	movq	%rbp, %rsi
	callq	clause_UpdateSplitDataFromPartner
	movq	88(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 36(%rbx)
	je	.LBB31_153
# BB#140:                               #   in Loop: Header=BB31_32 Depth=1
	movq	32(%rbp), %rcx
	movq	32(%r12), %rax
	testq	%rcx, %rcx
	je	.LBB31_145
# BB#141:                               #   in Loop: Header=BB31_32 Depth=1
	testq	%rax, %rax
	je	.LBB31_146
# BB#142:                               # %.preheader.i.preheader
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB31_143:                             # %.preheader.i
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB31_143
# BB#144:                               #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, (%rdx)
	jmp	.LBB31_146
.LBB31_145:                             #   in Loop: Header=BB31_32 Depth=1
	movq	%rax, %rcx
.LBB31_146:                             # %list_Nconc.exit
                                        #   in Loop: Header=BB31_32 Depth=1
	pxor	%xmm0, %xmm0
	leaq	32(%rbp), %rax
	movq	%rcx, 32(%r12)
	movq	40(%rbp), %rcx
	movq	40(%r12), %rdx
	testq	%rcx, %rcx
	je	.LBB31_151
# BB#147:                               #   in Loop: Header=BB31_32 Depth=1
	testq	%rdx, %rdx
	je	.LBB31_152
# BB#148:                               # %.preheader.i172.preheader
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rcx, %rdi
	.p2align	4, 0x90
.LBB31_149:                             # %.preheader.i172
                                        #   Parent Loop BB31_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rdi, %rsi
	movq	(%rsi), %rdi
	testq	%rdi, %rdi
	jne	.LBB31_149
# BB#150:                               #   in Loop: Header=BB31_32 Depth=1
	movq	%rdx, (%rsi)
	jmp	.LBB31_152
.LBB31_151:                             #   in Loop: Header=BB31_32 Depth=1
	movq	%rdx, %rcx
.LBB31_152:                             # %list_Nconc.exit174
                                        #   in Loop: Header=BB31_32 Depth=1
	movq	%rcx, 40(%r12)
	movdqu	%xmm0, (%rax)
.LBB31_153:                             #   in Loop: Header=BB31_32 Depth=1
	movq	%rbp, %rdi
	callq	clause_Delete
	movq	160(%rsp), %rbp         # 8-byte Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
.LBB31_154:                             #   in Loop: Header=BB31_32 Depth=1
	cmpq	200(%rsp), %rcx         # 8-byte Folded Reload
	leaq	1(%rcx), %rcx
	jl	.LBB31_32
	jmp	.LBB31_158
.LBB31_156:
	movl	$0, 68(%rsp)            # 4-byte Folded Spill
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movq	88(%rsp), %rbx          # 8-byte Reload
	movq	160(%rsp), %rbp         # 8-byte Reload
.LBB31_157:                             # %.critedge
	movq	80(%rsp), %rax          # 8-byte Reload
.LBB31_158:                             # %.critedge
	movl	140(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	cmpq	%rax, %rbx
	jae	.LBB31_162
# BB#159:                               # %.critedge
	cmpq	176(%rsp), %rbp         # 8-byte Folded Reload
	jae	.LBB31_162
# BB#160:                               # %scalar.ph230.preheader
	movl	$7, %eax
	.p2align	4, 0x90
.LBB31_161:                             # %scalar.ph230
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rbp,%rax,4), %ecx
	movl	%ecx, -28(%rbx,%rax,4)
	movl	-24(%rbp,%rax,4), %ecx
	movl	%ecx, -24(%rbx,%rax,4)
	movl	-20(%rbp,%rax,4), %ecx
	movl	%ecx, -20(%rbx,%rax,4)
	movl	-16(%rbp,%rax,4), %ecx
	movl	%ecx, -16(%rbx,%rax,4)
	movl	-12(%rbp,%rax,4), %ecx
	movl	%ecx, -12(%rbx,%rax,4)
	movl	-8(%rbp,%rax,4), %ecx
	movl	%ecx, -8(%rbx,%rax,4)
	movl	-4(%rbp,%rax,4), %ecx
	movl	%ecx, -4(%rbx,%rax,4)
	movl	(%rbp,%rax,4), %ecx
	movl	%ecx, (%rbx,%rax,4)
	addq	$8, %rax
	cmpq	$103, %rax
	jne	.LBB31_161
	jmp	.LBB31_163
.LBB31_162:                             # %vector.body228
	movups	(%rbp), %xmm0
	movups	%xmm0, (%rbx)
	movups	16(%rbp), %xmm0
	movups	%xmm0, 16(%rbx)
	movups	32(%rbp), %xmm0
	movups	%xmm0, 32(%rbx)
	movups	48(%rbp), %xmm0
	movups	%xmm0, 48(%rbx)
	movups	64(%rbp), %xmm0
	movups	%xmm0, 64(%rbx)
	movups	80(%rbp), %xmm0
	movups	%xmm0, 80(%rbx)
	movups	96(%rbp), %xmm0
	movups	%xmm0, 96(%rbx)
	movups	112(%rbp), %xmm0
	movups	%xmm0, 112(%rbx)
	movups	128(%rbp), %xmm0
	movups	%xmm0, 128(%rbx)
	movups	144(%rbp), %xmm0
	movups	%xmm0, 144(%rbx)
	movups	160(%rbp), %xmm0
	movups	%xmm0, 160(%rbx)
	movups	176(%rbp), %xmm0
	movups	%xmm0, 176(%rbx)
	movups	192(%rbp), %xmm0
	movups	%xmm0, 192(%rbx)
	movups	208(%rbp), %xmm0
	movups	%xmm0, 208(%rbx)
	movups	224(%rbp), %xmm0
	movups	%xmm0, 224(%rbx)
	movups	240(%rbp), %xmm0
	movups	%xmm0, 240(%rbx)
	movups	256(%rbp), %xmm0
	movups	%xmm0, 256(%rbx)
	movups	272(%rbp), %xmm0
	movups	%xmm0, 272(%rbx)
	movups	288(%rbp), %xmm0
	movups	%xmm0, 288(%rbx)
	movups	304(%rbp), %xmm0
	movups	%xmm0, 304(%rbx)
	movups	320(%rbp), %xmm0
	movups	%xmm0, 320(%rbx)
	movups	336(%rbp), %xmm0
	movups	%xmm0, 336(%rbx)
	movups	352(%rbp), %xmm0
	movups	%xmm0, 352(%rbx)
	movdqu	368(%rbp), %xmm0
	movdqu	%xmm0, 368(%rbx)
.LBB31_163:                             # %flag_TransferAllFlags.exit178
	movq	memory_ARRAY+3072(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	memory_ARRAY+3072(%rip), %rax
	movq	%rbp, (%rax)
	cmpb	$0, 72(%rsp)            # 1-byte Folded Reload
	je	.LBB31_170
# BB#164:
	movq	280(%rsp), %rax
	movq	%r12, (%rax)
	movl	68(%rsp), %eax          # 4-byte Reload
	jmp	.LBB31_171
.LBB31_165:
	movl	cont_BINDINGS(%rip), %eax
	testl	%eax, %eax
	jle	.LBB31_168
# BB#166:                               # %.lr.ph.i152.preheader
	incl	%eax
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB31_167:                             # %.lr.ph.i152
                                        # =>This Inner Loop Header: Depth=1
	movq	cont_LASTBINDING(%rip), %rcx
	movq	%rcx, cont_CURRENTBINDING(%rip)
	movq	24(%rcx), %rdx
	movq	%rdx, cont_LASTBINDING(%rip)
	movdqu	%xmm0, 4(%rcx)
	movl	$0, 20(%rcx)
	movq	cont_CURRENTBINDING(%rip), %rcx
	movq	$0, 24(%rcx)
	leal	-2(%rax), %ecx
	movl	%ecx, cont_BINDINGS(%rip)
	decl	%eax
	cmpl	$1, %eax
	jg	.LBB31_167
.LBB31_168:                             # %._crit_edge.i
	movslq	cont_STACKPOINTER(%rip), %rax
	testq	%rax, %rax
	je	.LBB31_170
# BB#169:
	leaq	-1(%rax), %rcx
	movl	%ecx, cont_STACKPOINTER(%rip)
	movl	cont_STACK-4(,%rax,4), %eax
	movl	%eax, cont_BINDINGS(%rip)
.LBB31_170:
	movq	%r12, %rdi
	callq	clause_Delete
	xorl	%eax, %eax
.LBB31_171:
	addq	$216, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	red_CRwTautologyCheck, .Lfunc_end31-red_CRwTautologyCheck
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_DocumentContextualRewriting,@function
red_DocumentContextualRewriting:        # @red_DocumentContextualRewriting
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi372:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi373:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi374:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi375:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi376:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi377:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi378:
	.cfi_def_cfa_offset 64
.Lcfi379:
	.cfi_offset %rbx, -56
.Lcfi380:
	.cfi_offset %r12, -48
.Lcfi381:
	.cfi_offset %r13, -40
.Lcfi382:
	.cfi_offset %r14, -32
.Lcfi383:
	.cfi_offset %r15, -24
.Lcfi384:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movl	%esi, %r12d
	movq	%rdi, %rbx
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB32_2
	.p2align	4, 0x90
.LBB32_1:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB32_1
.LBB32_2:                               # %list_Delete.exit
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB32_4
	.p2align	4, 0x90
.LBB32_3:                               # %.lr.ph.i21
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB32_3
.LBB32_4:                               # %list_Delete.exit22
	movq	%r13, 32(%rbx)
	movq	%r9, 40(%rbx)
	movslq	(%rbx), %rbp
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, 32(%rbx)
	movslq	%r12d, %rbp
	movq	40(%rbx), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, 40(%rbx)
	movslq	(%r15), %rbp
	movq	32(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, 32(%rbx)
	movslq	%r14d, %rbp
	movq	40(%rbx), %r14
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbp, 8(%rax)
	movq	%r14, (%rax)
	movq	%rax, 40(%rbx)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%rbx)
	movl	$22, 76(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	red_DocumentContextualRewriting, .Lfunc_end32-red_DocumentContextualRewriting
	.cfi_endproc

	.p2align	4, 0x90
	.type	flag_SetFlagValue,@function
flag_SetFlagValue:                      # @flag_SetFlagValue
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi385:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi386:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi387:
	.cfi_def_cfa_offset 32
.Lcfi388:
	.cfi_offset %rbx, -24
.Lcfi389:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	movq	%rdi, %r14
	movl	%ebx, %edi
	callq	flag_Minimum
	testl	%eax, %eax
	jns	.LBB33_1
# BB#3:
	movl	%ebx, %edi
	callq	flag_Maximum
	testl	%eax, %eax
	jle	.LBB33_4
# BB#5:                                 # %flag_CheckFlagValueInRange.exit
	movl	%ebx, %eax
	movl	$0, (%r14,%rax,4)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB33_1:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.32, %edi
	jmp	.LBB33_2
.LBB33_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	%ebx, %edi
	callq	flag_Name
	movq	%rax, %rcx
	movl	$.L.str.33, %edi
.LBB33_2:
	xorl	%esi, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end33:
	.size	flag_SetFlagValue, .Lfunc_end33-flag_SetFlagValue
	.cfi_endproc

	.p2align	4, 0x90
	.type	red_DocumentMatchingReplacementResolution,@function
red_DocumentMatchingReplacementResolution: # @red_DocumentMatchingReplacementResolution
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi390:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi391:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi392:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi393:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi394:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi395:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi396:
	.cfi_def_cfa_offset 64
.Lcfi397:
	.cfi_offset %rbx, -56
.Lcfi398:
	.cfi_offset %r12, -48
.Lcfi399:
	.cfi_offset %r13, -40
.Lcfi400:
	.cfi_offset %r14, -32
.Lcfi401:
	.cfi_offset %r15, -24
.Lcfi402:
	.cfi_offset %rbp, -16
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r13
	testq	%r14, %r14
	je	.LBB34_1
# BB#2:                                 # %.lr.ph
	xorl	%ebx, %ebx
	movq	%r14, %rbp
	.p2align	4, 0x90
.LBB34_3:                               # =>This Inner Loop Header: Depth=1
	movslq	(%r13), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%rbx, (%rax)
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	movq	%rax, %rbx
	jne	.LBB34_3
	jmp	.LBB34_4
.LBB34_1:
	xorl	%eax, %eax
.LBB34_4:                               # %._crit_edge
	movq	32(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB34_6
	.p2align	4, 0x90
.LBB34_5:                               # %.lr.ph.i29
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB34_5
.LBB34_6:                               # %list_Delete.exit30
	movq	40(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB34_8
	.p2align	4, 0x90
.LBB34_7:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	memory_ARRAY+128(%rip), %rsi
	movslq	32(%rsi), %rdi
	addq	%rdi, memory_FREEDBYTES(%rip)
	movq	(%rsi), %rsi
	movq	%rsi, (%rcx)
	movq	memory_ARRAY+128(%rip), %rsi
	movq	%rcx, (%rsi)
	testq	%rdx, %rdx
	movq	%rdx, %rcx
	jne	.LBB34_7
.LBB34_8:                               # %list_Delete.exit
	testq	%rax, %rax
	je	.LBB34_9
# BB#10:
	testq	%r12, %r12
	je	.LBB34_14
# BB#11:                                # %.preheader.i23.preheader
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB34_12:                              # %.preheader.i23
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, %rcx
	movq	(%rcx), %rdx
	testq	%rdx, %rdx
	jne	.LBB34_12
# BB#13:
	movq	%r12, (%rcx)
	jmp	.LBB34_14
.LBB34_9:
	movq	%r12, %rax
.LBB34_14:                              # %list_Nconc.exit25
	movq	(%rsp), %rdx            # 8-byte Reload
	testq	%r14, %r14
	movq	%rax, 32(%r13)
	je	.LBB34_15
# BB#16:
	testq	%rdx, %rdx
	je	.LBB34_20
# BB#17:                                # %.preheader.i.preheader
	movq	%r14, %rcx
	.p2align	4, 0x90
.LBB34_18:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rax
	movq	(%rax), %rcx
	testq	%rcx, %rcx
	jne	.LBB34_18
# BB#19:
	movq	%rdx, (%rax)
	jmp	.LBB34_20
.LBB34_15:
	movq	%rdx, %r14
.LBB34_20:                              # %list_Nconc.exit
	movq	%r14, 40(%r13)
	movl	clause_CLAUSECOUNTER(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, clause_CLAUSECOUNTER(%rip)
	movl	%eax, (%r13)
	movl	$23, 76(%r13)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	red_DocumentMatchingReplacementResolution, .Lfunc_end34-red_DocumentMatchingReplacementResolution
	.cfi_endproc

	.type	red_USABLE,@object      # @red_USABLE
	.section	.rodata,"a",@progbits
	.globl	red_USABLE
	.p2align	2
red_USABLE:
	.long	1                       # 0x1
	.size	red_USABLE, 4

	.type	red_WORKEDOFF,@object   # @red_WORKEDOFF
	.globl	red_WORKEDOFF
	.p2align	2
red_WORKEDOFF:
	.long	2                       # 0x2
	.size	red_WORKEDOFF, 4

	.type	red_ALL,@object         # @red_ALL
	.globl	red_ALL
	.p2align	2
red_ALL:
	.long	3                       # 0x3
	.size	red_ALL, 4

	.type	red_STAMPID,@object     # @red_STAMPID
	.local	red_STAMPID
	.comm	red_STAMPID,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n Error while applying bound restrictions:"
	.size	.L.str, 43

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\n You selected an unknown bound mode.\n"
	.size	.L.str.1, 39

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\nDeleted by bound: "
	.size	.L.str.2, 20

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\nKept: "
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\nDerived: "
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\n\nStatic Soft Typing tried on: "
	.size	.L.str.5, 32

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\n*************** Static Soft Typing Subproof: ***************"
	.size	.L.str.6, 62

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"The usable list:"
	.size	.L.str.7, 17

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\nThe worked-off list:"
	.size	.L.str.8, 22

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"\n\tSubproof Given clause: "
	.size	.L.str.9, 26

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"\nStatic Soft Typing not successful: "
	.size	.L.str.10, 37

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	" Static Soft Typing not successful!"
	.size	.L.str.11, 36

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nStatic Soft Typing failed, constraint solvable."
	.size	.L.str.12, 49

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"************  Static Soft Typing Subproof finished. ************"
	.size	.L.str.13, 65

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"\nStatic Soft Typing deleted: "
	.size	.L.str.14, 30

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"\n\tError in file %s at line %d\n"
	.size	.L.str.15, 31

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"/mnt/freezedisk/llvm-test-suite-4.0.0/MultiSource/Applications/SPASS/rules-red.c"
	.size	.L.str.16, 81

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\n In red_CheckSplitSubsumptionCondition: No clause found implying "
	.size	.L.str.17, 67

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\n Current Split: "
	.size	.L.str.18, 18

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"\n Please report this error via email to spass@mpi-sb.mpg.de including\n the SPASS version, input problem, options, operating system.\n"
	.size	.L.str.19, 133

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\nTautology: "
	.size	.L.str.20, 13

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"\nObvious: "
	.size	.L.str.21, 11

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	" ==> "
	.size	.L.str.22, 6

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\nCondensing: "
	.size	.L.str.23, 14

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\nAED: "
	.size	.L.str.24, 7

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\nFSubsumption: "
	.size	.L.str.25, 16

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	" by %d %d "
	.size	.L.str.26, 11

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"\nFRewriting: "
	.size	.L.str.27, 14

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	" ==>[ "
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"%d.%d "
	.size	.L.str.29, 7

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"] "
	.size	.L.str.30, 3

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"\nFContRewriting: "
	.size	.L.str.31, 18

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.32, 50

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.33, 50

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"\nSortSimplification: "
	.size	.L.str.34, 22

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%d "
	.size	.L.str.35, 4

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"\nFMatchingReplacementResolution: "
	.size	.L.str.36, 34

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	" ==> [ "
	.size	.L.str.37, 8

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"\nUnitConflict: "
	.size	.L.str.38, 16

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	" ==> [ %d.%d ]"
	.size	.L.str.39, 15

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"\nBSubsumption: "
	.size	.L.str.40, 16

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	" by %d "
	.size	.L.str.41, 8

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"\nBMatchingReplacementResolution: "
	.size	.L.str.42, 34

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	" ==>[ %d.%d ] "
	.size	.L.str.43, 15

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"\nBRewriting: "
	.size	.L.str.44, 14

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"\nBContRewriting: "
	.size	.L.str.45, 18

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"\nFSubsumption:"
	.size	.L.str.46, 15

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"\n\n"
	.size	.L.str.47, 3


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
