	.text
	.file	"insert.bc"
	.globl	insert
	.p2align	4, 0x90
	.type	insert,@function
insert:                                 # @insert
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 48
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	createIndexEntry
	testq	%rax, %rax
	je	.LBB0_1
# BB#3:
	movq	%rbx, (%rax)
	movq	8(%rbx), %rcx
	movl	(%rcx), %edx
	movl	%edx, 8(%rax)
	movl	8(%rcx), %edx
	movl	%edx, 12(%rax)
	movl	16(%rcx), %edx
	movl	%edx, 16(%rax)
	movl	24(%rcx), %edx
	movl	%edx, 20(%rax)
	movl	32(%rcx), %edx
	movl	%edx, 24(%rax)
	movl	40(%rcx), %edx
	movl	%edx, 28(%rax)
	movl	48(%rcx), %edx
	movl	%edx, 32(%rax)
	movl	56(%rcx), %ecx
	movl	%ecx, 36(%rax)
	movq	(%r14), %rdi
	leaq	8(%rsp), %r8
	xorl	%edx, %edx
	movq	%rax, %rsi
	movq	%r15, %rcx
	callq	insertEntry
	movq	%rax, %rcx
	cmpq	$3, %rcx
	ja	.LBB0_12
# BB#4:
	movl	$2, %eax
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_5:
	cmpq	$0, 8(%rsp)
	je	.LBB0_12
# BB#6:
	movq	(%r14), %rax
	movq	(%rax), %rdi
	incq	%rdi
	callq	createIndexNode
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_7
# BB#8:
	callq	createIndexEntry
	movq	%rax, 8(%rbx)
	testq	%rax, %rax
	je	.LBB0_9
# BB#11:
	movq	(%r14), %rcx
	movq	%rcx, (%rax)
	movq	(%r14), %rax
	movq	8(%rax), %rdi
	movq	8(%rbx), %rsi
	addq	$8, %rsi
	callq	keysUnion
	movq	8(%rsp), %rax
	movq	8(%rbx), %rcx
	movq	%rax, 40(%rcx)
	movq	%rbx, (%r14)
.LBB0_12:
	xorl	%eax, %eax
	jmp	.LBB0_13
.LBB0_1:
	movl	$.L.str, %edi
.LBB0_2:
	xorl	%esi, %esi
	callq	errorMessage
	movl	$insert.name, %edi
	movl	$1, %esi
	callq	errorMessage
	movl	$3, %eax
	jmp	.LBB0_13
.LBB0_10:
	movl	$1, %eax
.LBB0_13:
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB0_7:                                # %.critedge
	movl	$.L.str.1, %edi
	jmp	.LBB0_2
.LBB0_9:                                # %.critedge44
	movl	$.L.str.2, %edi
	jmp	.LBB0_2
.Lfunc_end0:
	.size	insert, .Lfunc_end0-insert
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_5
	.quad	.LBB0_13
	.quad	.LBB0_10
	.quad	.LBB0_13

	.type	insert.name,@object     # @insert.name
	.data
insert.name:
	.asciz	"insert"
	.size	insert.name, 7

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"can't create entry for new data object"
	.size	.L.str, 39

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"can't create new root node"
	.size	.L.str.1, 27

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"can't create entry for old root"
	.size	.L.str.2, 32


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
