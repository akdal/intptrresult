	.text
	.file	"outputMetricsData.bc"
	.globl	outputMetricsData
	.p2align	4, 0x90
	.type	outputMetricsData,@function
outputMetricsData:                      # @outputMetricsData
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	%r14, %rdi
	callq	calcMetricsData
	movl	$.L.str, %edi
	movl	$28, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	(%r14), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	8(%r14), %rdx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	16(%r14), %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.4, %edi
	movl	$29, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	64(%r14), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	56(%r14), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movsd	72(%r14), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.7, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	movsd	80(%r14), %xmm0         # xmm0 = mem[0],zero
	movl	$.L.str.8, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.9, %edi
	movl	$28, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	128(%r14), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	120(%r14), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movsd	136(%r14), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.7, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	movsd	144(%r14), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.8, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.10, %edi
	movl	$29, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	192(%r14), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movq	184(%r14), %rdx
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movsd	200(%r14), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.7, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	callq	fprintf
	movsd	208(%r14), %xmm0        # xmm0 = mem[0],zero
	movl	$.L.str.8, %esi
	movb	$1, %al
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fprintf                 # TAILCALL
.Lfunc_end0:
	.size	outputMetricsData, .Lfunc_end0-outputMetricsData
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"DIS Data Management Metrics\n"
	.size	.L.str, 29

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"    total time  = %li msecs\n"
	.size	.L.str.1, 29

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"    input time  = %li msecs\n"
	.size	.L.str.2, 29

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"    output time = %li msecs\n"
	.size	.L.str.3, 29

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"    Insert Commmand Metrics:\n"
	.size	.L.str.4, 30

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"        best time          = %li msecs\n"
	.size	.L.str.5, 40

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"        worst time         = %li msecs\n"
	.size	.L.str.6, 40

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"        average            = %f msecs\n"
	.size	.L.str.7, 39

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"        standard deviation = %f msecs\n"
	.size	.L.str.8, 39

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"    Query Commmand Metrics:\n"
	.size	.L.str.9, 29

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"    Delete Commmand Metrics:\n"
	.size	.L.str.10, 30


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
