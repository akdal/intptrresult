	.text
	.file	"args.bc"
	.globl	mylog
	.p2align	4, 0x90
	.type	mylog,@function
mylog:                                  # @mylog
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$2, %edi
	jl	.LBB0_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB0_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	addl	%ecx, %ecx
	incl	%eax
	cmpl	%edi, %ecx
	jl	.LBB0_2
.LBB0_3:                                # %._crit_edge
	retq
.Lfunc_end0:
	.size	mylog, .Lfunc_end0-mylog
	.cfi_endproc

	.globl	dealwithargs
	.p2align	4, 0x90
	.type	dealwithargs,@function
dealwithargs:                           # @dealwithargs
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	cmpl	$4, %ebp
	jl	.LBB1_5
# BB#1:                                 # %.thread
	movq	24(%rbx), %rdi
	callq	atoi
	movl	%eax, flag(%rip)
	jmp	.LBB1_2
.LBB1_5:
	movl	$0, flag(%rip)
	cmpl	$3, %ebp
	jne	.LBB1_6
.LBB1_2:
	movq	16(%rbx), %rdi
	callq	atoi
	movl	%eax, NumNodes(%rip)
	cmpl	$1, %eax
	jg	.LBB1_7
# BB#3:                                 # %mylog.exit.thread
	movl	$0, NDim(%rip)
	jmp	.LBB1_4
.LBB1_6:                                # %.thread8
	movl	$4, NumNodes(%rip)
	movl	$4, %eax
.LBB1_7:                                # %.lr.ph.i.preheader
	xorl	%ecx, %ecx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB1_8:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	addl	%edx, %edx
	incl	%ecx
	cmpl	%eax, %edx
	jl	.LBB1_8
# BB#9:                                 # %mylog.exit
	movl	%ecx, NDim(%rip)
	cmpl	$2, %ebp
	jl	.LBB1_10
.LBB1_4:
	movq	8(%rbx), %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	jmp	atoi                    # TAILCALL
.LBB1_10:
	movl	$65535, %eax            # imm = 0xFFFF
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	dealwithargs, .Lfunc_end1-dealwithargs
	.cfi_endproc

	.type	flag,@object            # @flag
	.comm	flag,4,4
	.type	NumNodes,@object        # @NumNodes
	.comm	NumNodes,4,4
	.type	NDim,@object            # @NDim
	.comm	NDim,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
