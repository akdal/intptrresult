	.text
	.file	"gram.bc"
	.globl	new_production
	.p2align	4, 0x90
	.type	new_production,@function
new_production:                         # @new_production
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %rdi
	callq	strlen
	movq	%rax, %r12
	movl	8(%rbx), %ebp
	testq	%rbp, %rbp
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	16(%rbx), %r14
	je	.LBB0_5
# BB#1:                                 # %.lr.ph.i
	movslq	%r12d, %r13
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14,%r15,8), %rbx
	cmpl	%r12d, 8(%rbx)
	jne	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	(%rbx), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%r13, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB0_15
.LBB0_4:                                # %.thread.i
                                        #   in Loop: Header=BB0_2 Depth=1
	incq	%r15
	cmpl	%ebp, %r15d
	jb	.LBB0_2
.LBB0_5:                                # %.loopexit
	movl	$232, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 208(%rbx)
	movups	%xmm0, 192(%rbx)
	movups	%xmm0, 176(%rbx)
	movups	%xmm0, 160(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 224(%rbx)
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	24(%rdi), %rax
	testq	%r14, %r14
	je	.LBB0_6
# BB#7:
	addq	$8, %rdi
	cmpq	%rax, %r14
	je	.LBB0_8
# BB#10:
	testb	$7, %bpl
	je	.LBB0_13
# BB#11:
	leal	1(%rbp), %eax
	jmp	.LBB0_12
.LBB0_6:
	movq	%rax, 16(%rdi)
	leal	1(%rbp), %eax
	movl	%eax, 8(%rdi)
	movq	%rbx, 24(%rdi,%rbp,8)
	jmp	.LBB0_14
.LBB0_8:
	cmpl	$2, %ebp
	ja	.LBB0_13
# BB#9:
	movl	%ebp, %eax
	incl	%eax
.LBB0_12:
	movl	%eax, (%rdi)
	movq	%rbx, (%r14,%rbp,8)
	jmp	.LBB0_14
.LBB0_13:
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB0_14:
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%rdi, (%rbx)
	callq	strlen
	movl	%eax, 8(%rbx)
.LBB0_15:                               # %lookup_production.exit
	movq	%rbx, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	new_production, .Lfunc_end0-new_production
	.cfi_endproc

	.globl	lookup_production
	.p2align	4, 0x90
	.type	lookup_production,@function
lookup_production:                      # @lookup_production
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%edx, %r12d
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	8(%rdi), %ebp
	testl	%ebp, %ebp
	je	.LBB1_5
# BB#1:                                 # %.lr.ph
	movq	16(%rdi), %r13
	movslq	%r12d, %r15
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%r13,%r14,8), %rbx
	cmpl	%r12d, 8(%rbx)
	jne	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	movq	(%rbx), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r15, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB1_6
.LBB1_4:                                # %.thread
                                        #   in Loop: Header=BB1_2 Depth=1
	incq	%r14
	cmpl	%ebp, %r14d
	jb	.LBB1_2
.LBB1_5:
	xorl	%ebx, %ebx
.LBB1_6:                                # %._crit_edge
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	lookup_production, .Lfunc_end1-lookup_production
	.cfi_endproc

	.globl	new_rule
	.p2align	4, 0x90
	.type	new_rule,@function
new_rule:                               # @new_rule
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r15, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	new_rule, .Lfunc_end2-new_rule
	.cfi_endproc

	.globl	new_elem_nterm
	.p2align	4, 0x90
	.type	new_elem_nterm,@function
new_elem_nterm:                         # @new_elem_nterm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi34:
	.cfi_def_cfa_offset 32
.Lcfi35:
	.cfi_offset %rbx, -24
.Lcfi36:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbx, 16(%rax)
	movq	%r14, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	new_elem_nterm, .Lfunc_end3-new_elem_nterm
	.cfi_endproc

	.globl	escape_string_for_regex
	.p2align	4, 0x90
	.type	escape_string_for_regex,@function
escape_string_for_regex:                # @escape_string_for_regex
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	strlen
	leaq	2(%rax,%rax), %rdi
	callq	malloc
	movb	(%rbx), %dl
	testb	%dl, %dl
	movq	%rax, %rcx
	je	.LBB4_6
# BB#1:                                 # %.lr.ph.preheader
	incq	%rbx
	movabsq	$29273397586296879, %rsi # imm = 0x6800000080002F
	movq	%rax, %rcx
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%dl, %edi
	addl	$-40, %edi
	cmpl	$54, %edi
	ja	.LBB4_5
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB4_2 Depth=1
	btq	%rdi, %rsi
	jae	.LBB4_5
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	movb	$92, (%rcx)
	incq	%rcx
	movzbl	-1(%rbx), %edx
.LBB4_5:                                #   in Loop: Header=BB4_2 Depth=1
	movb	%dl, (%rcx)
	incq	%rcx
	movzbl	(%rbx), %edx
	incq	%rbx
	testb	%dl, %dl
	jne	.LBB4_2
.LBB4_6:                                # %._crit_edge
	movb	$0, (%rcx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	escape_string_for_regex, .Lfunc_end4-escape_string_for_regex
	.cfi_endproc

	.globl	new_string
	.p2align	4, 0x90
	.type	new_string,@function
new_string:                             # @new_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 80
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	leaq	1(%rbx), %rsi
	decq	%rdx
	callq	new_term_string
	xorl	%ecx, %ecx
	cmpb	$34, (%rbx)
	sete	%cl
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	16(%rax), %rax
	movl	%ecx, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	24(%rax), %rdx
	movq	%rdx, %rbp
	jmp	.LBB5_2
	.p2align	4, 0x90
.LBB5_1:                                #   in Loop: Header=BB5_2 Depth=1
	incq	%r13
	movq	%r13, %rbp
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	cmpb	$92, %al
	je	.LBB5_5
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	testb	%al, %al
	je	.LBB5_42
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	movb	%al, (%rdx)
	jmp	.LBB5_36
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_2 Depth=1
	movsbl	1(%rbp), %ecx
	movb	$92, %al
	addl	$-34, %ecx
	cmpl	$86, %ecx
	ja	.LBB5_35
# BB#6:                                 #   in Loop: Header=BB5_2 Depth=1
	leaq	1(%rbp), %r13
	jmpq	*.LJTI5_0(,%rcx,8)
.LBB5_7:                                #   in Loop: Header=BB5_2 Depth=1
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	__ctype_b_loc
	movsbq	2(%rbp), %rsi
	movl	%esi, %ecx
	orb	$1, %cl
	cmpb	$57, %cl
	movl	$8, %edx
	movl	$1, %ecx
	je	.LBB5_11
# BB#8:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	(%rax), %rax
	movzwl	(%rax,%rsi,2), %esi
	andl	$2048, %esi             # imm = 0x800
	testw	%si, %si
	je	.LBB5_11
# BB#9:                                 #   in Loop: Header=BB5_2 Depth=1
	movsbq	3(%rbp), %rcx
	movzbl	1(%rax,%rcx,2), %eax
	movl	%ecx, %ebx
	orb	$1, %bl
	andb	$8, %al
	shrb	$3, %al
	movzbl	%al, %ecx
	orl	$2, %ecx
	cmpb	$57, %bl
	movl	$2, %eax
	cmovel	%eax, %ecx
	movl	%ecx, %eax
	jmp	.LBB5_33
.LBB5_11:                               #   in Loop: Header=BB5_2 Depth=1
	movl	$1, %eax
.LBB5_33:                               # %.thread.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	%r13, %rbx
	movl	%ecx, %r14d
	leaq	(%rbp,%r14), %r13
	movslq	%eax, %r15
	movzbl	(%rbx,%r15), %r12d
	movb	$0, (%rbx,%r15)
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	strtol
	movq	(%rsp), %rdx            # 8-byte Reload
	movb	%al, (%rdx)
	movb	%r12b, (%rbx,%r15)
	cmpb	$0, (%rbp,%r14)
	jg	.LBB5_37
# BB#34:                                # %.critedge.i
                                        #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	24(%rax), %rsi
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	movq	%rdx, %rbx
	callq	d_fail
	movq	%rbx, %rdx
	movzbl	(%r13), %eax
	movq	%r13, %rbp
.LBB5_35:                               #   in Loop: Header=BB5_2 Depth=1
	movb	%al, (%rdx)
	movzbl	1(%rbp), %eax
	movb	%al, 1(%rdx)
	leaq	1(%rdx), %rax
	incq	%rbp
	movq	%rax, %rdx
.LBB5_36:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rbp, %r13
.LBB5_37:                               #   in Loop: Header=BB5_2 Depth=1
	incq	%rdx
	jmp	.LBB5_1
.LBB5_12:                               #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$1, (%rcx)
	jne	.LBB5_35
# BB#13:                                #   in Loop: Header=BB5_2 Depth=1
	movb	$34, (%rdx)
	jmp	.LBB5_37
.LBB5_14:                               #   in Loop: Header=BB5_2 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	$0, (%rcx)
	jne	.LBB5_35
# BB#15:                                #   in Loop: Header=BB5_2 Depth=1
	movb	$39, (%rdx)
	jmp	.LBB5_37
.LBB5_16:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$7, (%rdx)
	jmp	.LBB5_37
.LBB5_17:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$8, (%rdx)
	jmp	.LBB5_37
.LBB5_18:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	__ctype_b_loc
	movq	(%rax), %rbx
	movsbq	2(%rbp), %rdi
	testb	$8, 1(%rbx,%rdi,2)
	jne	.LBB5_27
# BB#19:                                #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	jmp	.LBB5_1
.LBB5_20:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$12, (%rdx)
	jmp	.LBB5_37
.LBB5_21:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$10, (%rdx)
	jmp	.LBB5_37
.LBB5_22:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$13, (%rdx)
	jmp	.LBB5_37
.LBB5_23:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$9, (%rdx)
	jmp	.LBB5_37
.LBB5_24:                               #   in Loop: Header=BB5_2 Depth=1
	movb	$11, (%rdx)
	jmp	.LBB5_37
.LBB5_25:                               #   in Loop: Header=BB5_2 Depth=1
	movq	%rdx, (%rsp)            # 8-byte Spill
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movsbq	2(%rbp), %rcx
	testb	$16, 1(%rax,%rcx,2)
	jne	.LBB5_31
# BB#26:                                #   in Loop: Header=BB5_2 Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	jmp	.LBB5_1
.LBB5_27:                               #   in Loop: Header=BB5_2 Depth=1
	leaq	2(%rbp), %r13
	movsbq	3(%rbp), %rsi
	movl	$10, %edx
	movl	$1, %eax
	testb	$8, 1(%rbx,%rsi,2)
	je	.LBB5_32
# BB#28:                                #   in Loop: Header=BB5_2 Depth=1
	movsbq	4(%rbp), %rcx
	movl	$2, %eax
	testb	$8, 1(%rbx,%rcx,2)
	je	.LBB5_32
# BB#29:                                #   in Loop: Header=BB5_2 Depth=1
	cmpb	$50, %dil
	jge	.LBB5_38
.LBB5_30:                               #   in Loop: Header=BB5_2 Depth=1
	movl	$3, %eax
	jmp	.LBB5_32
.LBB5_31:                               #   in Loop: Header=BB5_2 Depth=1
	leaq	2(%rbp), %r13
	movsbq	3(%rbp), %rcx
	movzwl	(%rax,%rcx,2), %eax
	andl	$4096, %eax             # imm = 0x1000
	shrl	$12, %eax
	incl	%eax
	movl	$16, %edx
.LBB5_32:                               # %.thread123.i
                                        #   in Loop: Header=BB5_2 Depth=1
	leal	1(%rax), %ecx
	jmp	.LBB5_33
.LBB5_38:                               #   in Loop: Header=BB5_2 Depth=1
	jne	.LBB5_32
# BB#39:                                #   in Loop: Header=BB5_2 Depth=1
	cmpb	$53, %sil
	jl	.LBB5_30
# BB#40:                                #   in Loop: Header=BB5_2 Depth=1
	jne	.LBB5_32
# BB#41:                                #   in Loop: Header=BB5_2 Depth=1
	cmpb	$53, %cl
	jle	.LBB5_30
	jmp	.LBB5_32
.LBB5_42:
	movb	$0, (%rdx)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	24(%rbp), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movl	%eax, 32(%rbp)
	testl	%eax, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	jne	.LBB5_45
# BB#43:
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	d_fail
	jmp	.LBB5_45
.LBB5_44:
	movb	$0, (%rdx)
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB5_45:                               # %unescape_term_string.exit
	movq	%rbp, %rax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	new_string, .Lfunc_end5-new_string
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI5_0:
	.quad	.LBB5_12
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_14
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_7
	.quad	.LBB5_7
	.quad	.LBB5_7
	.quad	.LBB5_7
	.quad	.LBB5_7
	.quad	.LBB5_7
	.quad	.LBB5_7
	.quad	.LBB5_7
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_16
	.quad	.LBB5_17
	.quad	.LBB5_44
	.quad	.LBB5_18
	.quad	.LBB5_35
	.quad	.LBB5_20
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_21
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_35
	.quad	.LBB5_22
	.quad	.LBB5_35
	.quad	.LBB5_23
	.quad	.LBB5_35
	.quad	.LBB5_24
	.quad	.LBB5_35
	.quad	.LBB5_25

	.text
	.p2align	4, 0x90
	.type	new_term_string,@function
new_term_string:                        # @new_term_string
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi58:
	.cfi_def_cfa_offset 64
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r13
	movq	%rdi, %r12
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 16(%r15)
	movups	%xmm0, (%r15)
	subq	%r13, %rbx
	leaq	1(%rbx), %rdi
	callq	malloc
	movq	%rax, %rbp
	movq	%rbp, 24(%r15)
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movb	$0, (%rbp,%rbx)
	movl	%ebx, 32(%r15)
	movq	56(%r12), %rax
	leaq	64(%r12), %rdx
	testq	%rax, %rax
	je	.LBB6_1
# BB#2:
	addq	$48, %r12
	movl	(%r12), %ecx
	cmpq	%rdx, %rax
	je	.LBB6_3
# BB#5:
	testb	$7, %cl
	je	.LBB6_8
# BB#6:
	leal	1(%rcx), %edx
	jmp	.LBB6_7
.LBB6_1:
	movq	%rdx, 56(%r12)
	movl	48(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 48(%r12)
	movq	%r15, 64(%r12,%rax,8)
	jmp	.LBB6_9
.LBB6_3:
	cmpl	$2, %ecx
	ja	.LBB6_8
# BB#4:
	movl	%ecx, %edx
	incl	%edx
.LBB6_7:
	movl	%edx, (%r12)
	movq	%r15, (%rax,%rcx,8)
	jmp	.LBB6_9
.LBB6_8:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	vec_add_internal
.LBB6_9:
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movl	$1, (%rbx)
	movq	%r15, 16(%rbx)
	movq	%r14, 8(%rbx)
	movq	40(%r14), %rax
	leaq	48(%r14), %rdx
	testq	%rax, %rax
	je	.LBB6_10
# BB#11:
	addq	$32, %r14
	movl	(%r14), %ecx
	cmpq	%rdx, %rax
	je	.LBB6_12
# BB#14:
	testb	$7, %cl
	jne	.LBB6_13
	jmp	.LBB6_15
.LBB6_10:
	movq	%rdx, 40(%r14)
	movl	32(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 32(%r14)
	movq	%rbx, 48(%r14,%rax,8)
	jmp	.LBB6_16
.LBB6_12:
	cmpl	$2, %ecx
	ja	.LBB6_15
.LBB6_13:
	leal	1(%rcx), %edx
	movl	%edx, (%r14)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB6_16
.LBB6_15:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB6_16:                               # %new_elem_term.exit
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	new_term_string, .Lfunc_end6-new_term_string
	.cfi_endproc

	.globl	new_ident
	.p2align	4, 0x90
	.type	new_ident,@function
new_ident:                              # @new_ident
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi69:
	.cfi_def_cfa_offset 48
.Lcfi70:
	.cfi_offset %rbx, -40
.Lcfi71:
	.cfi_offset %r12, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movl	$2, (%rbx)
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	dup_str
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	callq	strlen
	movl	%eax, 24(%rbx)
	movq	%r14, 8(%rbx)
	testq	%r14, %r14
	je	.LBB7_10
# BB#1:
	movq	40(%r14), %rax
	leaq	48(%r14), %rdx
	testq	%rax, %rax
	je	.LBB7_2
# BB#3:
	addq	$32, %r14
	movl	(%r14), %ecx
	cmpq	%rdx, %rax
	je	.LBB7_4
# BB#6:
	testb	$7, %cl
	je	.LBB7_9
# BB#7:
	leal	1(%rcx), %edx
	jmp	.LBB7_8
.LBB7_2:
	movq	%rdx, 40(%r14)
	movl	32(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 32(%r14)
	movq	%rbx, 48(%r14,%rax,8)
	jmp	.LBB7_10
.LBB7_4:
	cmpl	$2, %ecx
	ja	.LBB7_9
# BB#5:
	movl	%ecx, %edx
	incl	%edx
.LBB7_8:
	movl	%edx, (%r14)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB7_10
.LBB7_9:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB7_10:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	new_ident, .Lfunc_end7-new_ident
	.cfi_endproc

	.globl	new_token
	.p2align	4, 0x90
	.type	new_token,@function
new_token:                              # @new_token
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -48
.Lcfi80:
	.cfi_offset %r12, -40
.Lcfi81:
	.cfi_offset %r13, -32
.Lcfi82:
	.cfi_offset %r14, -24
.Lcfi83:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r14
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r15
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%r15)
	movups	%xmm0, 16(%r15)
	movups	%xmm0, (%r15)
	subq	%r12, %rbx
	leaq	1(%rbx), %rdi
	callq	malloc
	movq	%rax, %r13
	movq	%r13, 24(%r15)
	movq	%r13, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	movb	$0, (%r13,%rbx)
	movl	%ebx, 32(%r15)
	movq	56(%r14), %rax
	leaq	64(%r14), %rdx
	testq	%rax, %rax
	je	.LBB8_1
# BB#2:
	addq	$48, %r14
	movl	(%r14), %ecx
	cmpq	%rdx, %rax
	je	.LBB8_3
# BB#5:
	testb	$7, %cl
	je	.LBB8_8
# BB#6:
	leal	1(%rcx), %edx
	jmp	.LBB8_7
.LBB8_1:
	movq	%rdx, 56(%r14)
	movl	48(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 48(%r14)
	movq	%r15, 64(%r14,%rax,8)
	jmp	.LBB8_9
.LBB8_3:
	cmpl	$2, %ecx
	ja	.LBB8_8
# BB#4:
	movl	%ecx, %edx
	incl	%edx
.LBB8_7:
	movl	%edx, (%r14)
	movq	%r15, (%rax,%rcx,8)
	jmp	.LBB8_9
.LBB8_8:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	vec_add_internal
.LBB8_9:
	movl	$3, (%r15)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	new_token, .Lfunc_end8-new_token
	.cfi_endproc

	.globl	new_code
	.p2align	4, 0x90
	.type	new_code,@function
new_code:                               # @new_code
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi84:
	.cfi_def_cfa_offset 16
	callq	new_term_string
	movq	16(%rax), %rcx
	movl	$2, (%rcx)
	popq	%rcx
	retq
.Lfunc_end9:
	.size	new_code, .Lfunc_end9-new_code
	.cfi_endproc

	.globl	dup_elem
	.p2align	4, 0x90
	.type	dup_elem,@function
dup_elem:                               # @dup_elem
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -24
.Lcfi89:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$32, %edi
	callq	malloc
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r14, 8(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	dup_elem, .Lfunc_end10-dup_elem
	.cfi_endproc

	.globl	add_global_code
	.p2align	4, 0x90
	.type	add_global_code,@function
add_global_code:                        # @add_global_code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi90:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi91:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi93:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 48
.Lcfi95:
	.cfi_offset %rbx, -48
.Lcfi96:
	.cfi_offset %r12, -40
.Lcfi97:
	.cfi_offset %r14, -32
.Lcfi98:
	.cfi_offset %r15, -24
.Lcfi99:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movq	%rdx, %r15
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	144(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB11_1
# BB#2:
	leaq	152(%rbx), %rbp
	movslq	152(%rbx), %rsi
	leal	1(%rsi), %eax
	testb	$4, %al
	jne	.LBB11_5
# BB#3:
	shlq	$4, %rsi
	addq	$64, %rsi
	callq	realloc
	jmp	.LBB11_4
.LBB11_1:
	movl	$64, %edi
	callq	malloc
	leaq	152(%rbx), %rbp
.LBB11_4:                               # %.sink.split
	movq	%rax, 144(%rbx)
.LBB11_5:
	movq	%r12, %rdi
	movq	%r15, %rsi
	callq	dup_str
	movq	144(%rbx), %rcx
	movslq	(%rbp), %rdx
	leal	1(%rdx), %esi
	shlq	$4, %rdx
	movq	%rax, (%rcx,%rdx)
	movl	%r14d, 8(%rcx,%rdx)
	movl	%esi, (%rbp)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	add_global_code, .Lfunc_end11-add_global_code
	.cfi_endproc

	.globl	new_declaration
	.p2align	4, 0x90
	.type	new_declaration,@function
new_declaration:                        # @new_declaration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi100:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 32
.Lcfi103:
	.cfi_offset %rbx, -32
.Lcfi104:
	.cfi_offset %r14, -24
.Lcfi105:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	malloc
	movq	%rbp, (%rax)
	movl	%r14d, 8(%rax)
	movl	160(%rbx), %ecx
	movl	%ecx, 12(%rax)
	movq	168(%rbx), %rdx
	leaq	176(%rbx), %rsi
	testq	%rdx, %rdx
	je	.LBB12_1
# BB#2:
	addq	$160, %rbx
	cmpq	%rsi, %rdx
	je	.LBB12_3
# BB#5:
	testb	$7, %cl
	je	.LBB12_9
# BB#6:
	leal	1(%rcx), %esi
	jmp	.LBB12_7
.LBB12_1:
	movq	%rsi, 168(%rbx)
	leal	1(%rcx), %edx
	movl	%edx, 160(%rbx)
	movq	%rax, 176(%rbx,%rcx,8)
	jmp	.LBB12_8
.LBB12_3:
	cmpl	$2, %ecx
	ja	.LBB12_9
# BB#4:
	movl	%ecx, %esi
	incl	%esi
.LBB12_7:
	movl	%esi, (%rbx)
	movq	%rax, (%rdx,%rcx,8)
.LBB12_8:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB12_9:
	movq	%rbx, %rdi
	movq	%rax, %rsi
	popq	%rbx
	popq	%r14
	popq	%rbp
	jmp	vec_add_internal        # TAILCALL
.Lfunc_end12:
	.size	new_declaration, .Lfunc_end12-new_declaration
	.cfi_endproc

	.globl	add_declaration
	.p2align	4, 0x90
	.type	add_declaration,@function
add_declaration:                        # @add_declaration
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi109:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi110:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi111:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi112:
	.cfi_def_cfa_offset 64
.Lcfi113:
	.cfi_offset %rbx, -56
.Lcfi114:
	.cfi_offset %r12, -48
.Lcfi115:
	.cfi_offset %r13, -40
.Lcfi116:
	.cfi_offset %r14, -32
.Lcfi117:
	.cfi_offset %r15, -24
.Lcfi118:
	.cfi_offset %rbp, -16
	movl	%r8d, %r12d
	movl	%ecx, %r13d
	movq	%rdx, %r15
	movq	%rsi, %rbx
	movq	%rdi, %r14
	cmpq	%r15, %rbx
	je	.LBB13_1
# BB#9:
	cmpl	$3, %r13d
	je	.LBB13_12
.LBB13_10:
	cmpl	$6, %r13d
	jne	.LBB13_13
# BB#11:
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	dup_str
	movq	%rax, 240(%r14)
	jmp	.LBB13_21
.LBB13_1:
	cmpl	$7, %r13d
	ja	.LBB13_8
# BB#2:
	movl	%r13d, %eax
	jmpq	*.LJTI13_0(,%rax,8)
.LBB13_6:
	movl	$1, 264(%r14)
	jmp	.LBB13_21
.LBB13_4:
	movl	$1, 268(%r14)
	jmp	.LBB13_21
.LBB13_5:
	movl	$0, 268(%r14)
	jmp	.LBB13_21
.LBB13_20:
	movl	$1, 248(%r14)
	jmp	.LBB13_21
.LBB13_3:
	movl	$1, 260(%r14)
	jmp	.LBB13_21
.LBB13_8:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	d_fail
	cmpl	$3, %r13d
	jne	.LBB13_10
.LBB13_12:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	d_fail
.LBB13_13:
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbp)
	movups	%xmm0, (%rbp)
	movl	$2, (%rbp)
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	dup_str
	movq	%rax, 16(%rbp)
	movq	%rax, %rdi
	callq	strlen
	movl	%eax, 24(%rbp)
	movq	$0, 8(%rbp)
	movl	$16, %edi
	callq	malloc
	movq	%rbp, (%rax)
	movl	%r13d, 8(%rax)
	movl	160(%r14), %ecx
	movl	%ecx, 12(%rax)
	movq	168(%r14), %rdx
	leaq	176(%r14), %rsi
	testq	%rdx, %rdx
	je	.LBB13_14
# BB#15:
	addq	$160, %r14
	cmpq	%rsi, %rdx
	je	.LBB13_16
# BB#18:
	testb	$7, %cl
	jne	.LBB13_17
	jmp	.LBB13_19
.LBB13_14:
	movq	%rsi, 168(%r14)
	leal	1(%rcx), %edx
	movl	%edx, 160(%r14)
	movq	%rax, 176(%r14,%rcx,8)
	jmp	.LBB13_21
.LBB13_16:
	cmpl	$2, %ecx
	ja	.LBB13_19
.LBB13_17:
	leal	1(%rcx), %esi
	movl	%esi, (%r14)
	movq	%rax, (%rdx,%rcx,8)
	jmp	.LBB13_21
.LBB13_19:
	movq	%r14, %rdi
	movq	%rax, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	vec_add_internal        # TAILCALL
.LBB13_7:
	movl	$1, 272(%r14)
.LBB13_21:                              # %new_declaration.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	add_declaration, .Lfunc_end13-add_declaration
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI13_0:
	.quad	.LBB13_6
	.quad	.LBB13_4
	.quad	.LBB13_5
	.quad	.LBB13_20
	.quad	.LBB13_3
	.quad	.LBB13_8
	.quad	.LBB13_8
	.quad	.LBB13_7

	.text
	.globl	find_pass
	.p2align	4, 0x90
	.type	find_pass,@function
find_pass:                              # @find_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi119:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi120:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi122:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi123:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi125:
	.cfi_def_cfa_offset 64
.Lcfi126:
	.cfi_offset %rbx, -56
.Lcfi127:
	.cfi_offset %r12, -48
.Lcfi128:
	.cfi_offset %r13, -40
.Lcfi129:
	.cfi_offset %r14, -32
.Lcfi130:
	.cfi_offset %r15, -24
.Lcfi131:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r14
	movq	%rdi, %r15
	movb	(%r14), %bl
	testb	%bl, %bl
	je	.LBB14_4
# BB#1:                                 # %.lr.ph27
	callq	__ctype_b_loc
	movq	(%rax), %rax
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB14_4
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	movzbl	1(%r14), %ebx
	incq	%r14
	testb	%bl, %bl
	jne	.LBB14_2
.LBB14_4:                               # %.critedge
	movl	200(%r15), %r13d
	testl	%r13d, %r13d
	je	.LBB14_9
# BB#5:                                 # %.lr.ph
	movq	%rbp, %rbx
	subq	%r14, %rbx
	movq	208(%r15), %rbp
	movslq	%ebx, %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB14_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp,%r15,8), %r12
	cmpl	8(%r12), %ebx
	jne	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_6 Depth=1
	movq	(%r12), %rdi
	movq	%r14, %rsi
	movq	(%rsp), %rdx            # 8-byte Reload
	callq	strncmp
	testl	%eax, %eax
	je	.LBB14_10
.LBB14_8:                               #   in Loop: Header=BB14_6 Depth=1
	incq	%r15
	cmpl	%r13d, %r15d
	jb	.LBB14_6
.LBB14_9:
	xorl	%r12d, %r12d
.LBB14_10:                              # %._crit_edge
	movq	%r12, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	find_pass, .Lfunc_end14-find_pass
	.cfi_endproc

	.globl	add_pass
	.p2align	4, 0x90
	.type	add_pass,@function
add_pass:                               # @add_pass
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi132:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi133:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi134:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi135:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi136:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi137:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi138:
	.cfi_def_cfa_offset 96
.Lcfi139:
	.cfi_offset %rbx, -56
.Lcfi140:
	.cfi_offset %r12, -48
.Lcfi141:
	.cfi_offset %r13, -40
.Lcfi142:
	.cfi_offset %r14, -32
.Lcfi143:
	.cfi_offset %r15, -24
.Lcfi144:
	.cfi_offset %rbp, -16
	movl	%r8d, 16(%rsp)          # 4-byte Spill
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r15
	movq	%rdi, %r14
	movb	(%r15), %bl
	testb	%bl, %bl
	movq	%r15, %rbp
	je	.LBB15_4
# BB#1:                                 # %.lr.ph27.i
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB15_4
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	movzbl	1(%rbp), %ebx
	incq	%rbp
	testb	%bl, %bl
	jne	.LBB15_2
.LBB15_4:                               # %.critedge.i
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movl	200(%r14), %r15d
	testl	%r15d, %r15d
	je	.LBB15_9
# BB#5:                                 # %.lr.ph.i
	movq	8(%rsp), %r14           # 8-byte Reload
	subq	%rbp, %r14
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	208(%rax), %r13
	movslq	%r14d, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rax
	cmpl	8(%rax), %r14d
	jne	.LBB15_8
# BB#7:                                 #   in Loop: Header=BB15_6 Depth=1
	movq	(%rax), %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB15_18
.LBB15_8:                               #   in Loop: Header=BB15_6 Depth=1
	incq	%rbx
	cmpl	%r15d, %ebx
	jb	.LBB15_6
.LBB15_9:                               # %.loopexit
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %rsi
	callq	dup_str
	movq	%rax, (%r14)
	subl	%ebp, %ebx
	movl	%ebx, 8(%r14)
	movl	20(%rsp), %eax          # 4-byte Reload
	movl	%eax, 12(%r14)
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	584(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 584(%rdi)
	movl	%eax, 16(%r14)
	movq	208(%rdi), %rax
	leaq	216(%rdi), %rdx
	testq	%rax, %rax
	je	.LBB15_10
# BB#11:
	addq	$200, %rdi
	movl	(%rdi), %ecx
	cmpq	%rdx, %rax
	je	.LBB15_12
# BB#14:
	testb	$7, %cl
	je	.LBB15_19
# BB#15:
	leal	1(%rcx), %edx
	jmp	.LBB15_16
.LBB15_10:
	movq	%rdx, 208(%rdi)
	movl	200(%rdi), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 200(%rdi)
	movq	%r14, 216(%rdi,%rax,8)
	jmp	.LBB15_17
.LBB15_18:                              # %find_pass.exit
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	dup_str
	movq	%rax, %rcx
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movl	16(%rsp), %edx          # 4-byte Reload
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	d_fail                  # TAILCALL
.LBB15_12:
	cmpl	$2, %ecx
	ja	.LBB15_19
# BB#13:
	movl	%ecx, %edx
	incl	%edx
.LBB15_16:
	movl	%edx, (%rdi)
	movq	%r14, (%rax,%rcx,8)
.LBB15_17:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_19:
	movq	%r14, %rsi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	vec_add_internal        # TAILCALL
.Lfunc_end15:
	.size	add_pass, .Lfunc_end15-add_pass
	.cfi_endproc

	.globl	add_pass_code
	.p2align	4, 0x90
	.type	add_pass_code,@function
add_pass_code:                          # @add_pass_code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi147:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi148:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi149:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi150:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi151:
	.cfi_def_cfa_offset 112
.Lcfi152:
	.cfi_offset %rbx, -56
.Lcfi153:
	.cfi_offset %r12, -48
.Lcfi154:
	.cfi_offset %r13, -40
.Lcfi155:
	.cfi_offset %r14, -32
.Lcfi156:
	.cfi_offset %r15, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movq	%r9, 32(%rsp)           # 8-byte Spill
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movq	%rdx, %rbp
	movq	%rsi, %r15
	movq	%rdi, %r14
	movb	(%rbp), %bl
	testb	%bl, %bl
	movq	%rbp, %r12
	je	.LBB16_4
# BB#1:                                 # %.lr.ph27.i
	callq	__ctype_b_loc
	movq	(%rax), %rax
	movq	%rbp, %r12
	.p2align	4, 0x90
.LBB16_2:                               # =>This Inner Loop Header: Depth=1
	movsbq	%bl, %rcx
	testb	$32, 1(%rax,%rcx,2)
	je	.LBB16_4
# BB#3:                                 #   in Loop: Header=BB16_2 Depth=1
	movzbl	1(%r12), %ebx
	incq	%r12
	testb	%bl, %bl
	jne	.LBB16_2
.LBB16_4:                               # %.critedge.i
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	200(%r14), %ebx
	testl	%ebx, %ebx
	je	.LBB16_9
# BB#5:                                 # %.lr.ph.i
	movq	8(%rsp), %r13           # 8-byte Reload
	subq	%r12, %r13
	movq	208(%r14), %r14
	movslq	%r13d, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB16_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %r15
	cmpl	8(%r15), %r13d
	jne	.LBB16_8
# BB#7:                                 #   in Loop: Header=BB16_6 Depth=1
	movq	(%r15), %rdi
	movq	%r12, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	strncmp
	testl	%eax, %eax
	je	.LBB16_10
.LBB16_8:                               #   in Loop: Header=BB16_6 Depth=1
	incq	%rbp
	cmpl	%ebx, %ebp
	jb	.LBB16_6
.LBB16_9:                               # %.loopexit
	movl	112(%rsp), %ebx
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	callq	dup_str
	movq	%rax, %rcx
	xorl	%r15d, %r15d
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	movq	%rcx, %rsi
	movl	%ebx, %edx
	callq	d_fail
.LBB16_10:                              # %find_pass.exit.preheader
	movl	120(%rsp), %r14d
	movq	40(%rsp), %rbp          # 8-byte Reload
	movl	112(%rbp), %ecx
	movl	16(%r15), %eax
	cmpl	%eax, %ecx
	ja	.LBB16_21
# BB#11:                                # %.lr.ph
	leaq	112(%rbp), %rbx
	movq	%rbp, %r12
	subq	$-128, %r12
	.p2align	4, 0x90
.LBB16_12:                              # =>This Inner Loop Header: Depth=1
	movq	120(%rbp), %rdx
	testq	%rdx, %rdx
	je	.LBB16_13
# BB#14:                                #   in Loop: Header=BB16_12 Depth=1
	cmpq	%r12, %rdx
	je	.LBB16_15
# BB#17:                                #   in Loop: Header=BB16_12 Depth=1
	testb	$7, %cl
	je	.LBB16_19
# BB#18:                                #   in Loop: Header=BB16_12 Depth=1
	leal	1(%rcx), %esi
	movl	%esi, (%rbx)
	movl	%ecx, %ecx
	movq	$0, (%rdx,%rcx,8)
	jmp	.LBB16_20
	.p2align	4, 0x90
.LBB16_13:                              #   in Loop: Header=BB16_12 Depth=1
	movq	%r12, 120(%rbp)
	jmp	.LBB16_16
	.p2align	4, 0x90
.LBB16_15:                              #   in Loop: Header=BB16_12 Depth=1
	cmpl	$2, %ecx
	ja	.LBB16_19
.LBB16_16:                              #   in Loop: Header=BB16_12 Depth=1
	leal	1(%rcx), %edx
	movl	%edx, 112(%rbp)
	movl	%ecx, %ecx
	movq	$0, 128(%rbp,%rcx,8)
	jmp	.LBB16_20
	.p2align	4, 0x90
.LBB16_19:                              #   in Loop: Header=BB16_12 Depth=1
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	vec_add_internal
	movl	16(%r15), %eax
.LBB16_20:                              # %find_pass.exit.backedge
                                        #   in Loop: Header=BB16_12 Depth=1
	movl	(%rbx), %ecx
	cmpl	%eax, %ecx
	jbe	.LBB16_12
.LBB16_21:                              # %find_pass.exit._crit_edge
	movl	$16, %edi
	callq	malloc
	movq	120(%rbp), %rcx
	movl	16(%r15), %edx
	movq	%rax, (%rcx,%rdx,8)
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	dup_str
	movq	120(%rbp), %rcx
	movl	16(%r15), %edx
	movq	(%rcx,%rdx,8), %rsi
	movq	%rax, (%rsi)
	movq	(%rcx,%rdx,8), %rax
	movl	%r14d, 8(%rax)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	add_pass_code, .Lfunc_end16-add_pass_code
	.cfi_endproc

	.globl	new_internal_production
	.p2align	4, 0x90
	.type	new_internal_production,@function
new_internal_production:                # @new_internal_production
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 48
.Lcfi163:
	.cfi_offset %rbx, -48
.Lcfi164:
	.cfi_offset %r12, -40
.Lcfi165:
	.cfi_offset %r13, -32
.Lcfi166:
	.cfi_offset %r14, -24
.Lcfi167:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r13
	testq	%r14, %r14
	je	.LBB17_1
# BB#2:
	movq	(%r14), %r15
	jmp	.LBB17_3
.LBB17_1:
	movl	$.L.str.4, %r15d
.LBB17_3:
	movq	%r15, %rdi
	callq	strlen
	leaq	20(%rax), %rdi
	callq	malloc
	movq	%rax, %r12
	movl	8(%r13), %ecx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movq	%r15, %rdx
	callq	sprintf
	movq	%r13, %rdi
	movq	%r12, %rsi
	callq	new_production
	movb	60(%rax), %cl
	andb	$-29, %cl
	orb	$4, %cl
	movb	%cl, 60(%rax)
	testq	%r14, %r14
	je	.LBB17_11
# BB#4:
	movb	60(%r14), %dl
	andb	$1, %dl
	andb	$-26, %cl
	orb	%dl, %cl
	movb	%cl, 60(%rax)
	movl	8(%r13), %r8d
	testl	%r8d, %r8d
	je	.LBB17_12
# BB#5:                                 # %.lr.ph
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB17_6:                               # =>This Inner Loop Header: Depth=1
	testl	%esi, %esi
	movq	16(%r13), %rbx
	movslq	%edx, %rcx
	movq	(%rbx,%rcx,8), %rdi
	je	.LBB17_8
# BB#7:                                 #   in Loop: Header=BB17_6 Depth=1
	movq	%r9, (%rbx,%rcx,8)
	movq	%rdi, %r9
	jmp	.LBB17_10
	.p2align	4, 0x90
.LBB17_8:                               #   in Loop: Header=BB17_6 Depth=1
	xorl	%esi, %esi
	cmpq	%r14, %rdi
	jne	.LBB17_10
# BB#9:                                 #   in Loop: Header=BB17_6 Depth=1
	leal	1(%rcx), %edx
	movq	8(%rbx,%rcx,8), %r9
	movq	%rax, 8(%rbx,%rcx,8)
	movl	$1, %esi
	.p2align	4, 0x90
.LBB17_10:                              #   in Loop: Header=BB17_6 Depth=1
	incl	%edx
	cmpl	%r8d, %edx
	jb	.LBB17_6
	jmp	.LBB17_12
.LBB17_11:                              # %.critedge
	andb	$-26, %cl
	movb	%cl, 60(%rax)
.LBB17_12:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	new_internal_production, .Lfunc_end17-new_internal_production
	.cfi_endproc

	.globl	conditional_EBNF
	.p2align	4, 0x90
	.type	conditional_EBNF,@function
conditional_EBNF:                       # @conditional_EBNF
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi168:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi169:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi170:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi171:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi172:
	.cfi_def_cfa_offset 48
.Lcfi173:
	.cfi_offset %rbx, -48
.Lcfi174:
	.cfi_offset %r12, -40
.Lcfi175:
	.cfi_offset %r13, -32
.Lcfi176:
	.cfi_offset %r14, -24
.Lcfi177:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	552(%r14), %rsi
	callq	new_internal_production
	movq	%rax, %r12
	movb	60(%r12), %al
	andb	$-29, %al
	orb	$8, %al
	movb	%al, 60(%r12)
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	leaq	48(%rbx), %r15
	movq	%r12, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	560(%r14), %rax
	movq	40(%rax), %rcx
	movl	32(%rax), %edx
	decl	%edx
	movq	(%rcx,%rdx,8), %rcx
	movq	%r15, 40(%rbx)
	movl	$1, 32(%rbx)
	movq	%rcx, 48(%rbx)
	movq	40(%rax), %rcx
	movl	32(%rax), %eax
	decl	%eax
	movq	(%rcx,%rax,8), %rax
	movq	%rbx, 8(%rax)
	movq	48(%rbx), %rax
	movq	%rbx, 8(%rax)
	movq	24(%r12), %rax
	leaq	16(%r12), %r15
	movq	%r12, %r13
	addq	$32, %r13
	testq	%rax, %rax
	je	.LBB18_1
# BB#2:
	movl	(%r15), %ecx
	cmpq	%r13, %rax
	je	.LBB18_3
# BB#5:
	testb	$7, %cl
	je	.LBB18_8
# BB#6:
	leal	1(%rcx), %edx
	jmp	.LBB18_7
.LBB18_1:
	movq	%r13, 24(%r12)
	movl	16(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r12)
	movq	%rbx, 32(%r12,%rax,8)
	jmp	.LBB18_9
.LBB18_3:
	cmpl	$2, %ecx
	ja	.LBB18_8
# BB#4:
	movl	%ecx, %edx
	incl	%edx
.LBB18_7:
	movl	%edx, (%r15)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB18_9
.LBB18_8:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB18_9:
	movq	24(%r12), %rcx
	testq	%rcx, %rcx
	je	.LBB18_10
# BB#11:
	movl	(%r15), %eax
	cmpq	%r13, %rcx
	je	.LBB18_12
# BB#14:
	testb	$7, %al
	jne	.LBB18_13
	jmp	.LBB18_15
.LBB18_10:
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r12, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	%r13, 24(%r12)
	movl	16(%r12), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r12)
	movq	%rbx, 32(%r12,%rax,8)
	jmp	.LBB18_16
.LBB18_12:
	cmpl	$2, %eax
	ja	.LBB18_15
.LBB18_13:
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r12, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	24(%r12), %rax
	movl	16(%r12), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r12)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB18_16
.LBB18_15:
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r12, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB18_16:
	movq	560(%r14), %rbx
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r12, 16(%rax)
	movq	%rbx, 8(%rax)
	movq	40(%rbx), %rcx
	movl	32(%rbx), %edx
	decl	%edx
	movq	%rax, (%rcx,%rdx,8)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end18:
	.size	conditional_EBNF, .Lfunc_end18-conditional_EBNF
	.cfi_endproc

	.globl	star_EBNF
	.p2align	4, 0x90
	.type	star_EBNF,@function
star_EBNF:                              # @star_EBNF
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi178:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi179:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi180:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi181:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi182:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi183:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi184:
	.cfi_def_cfa_offset 64
.Lcfi185:
	.cfi_offset %rbx, -56
.Lcfi186:
	.cfi_offset %r12, -48
.Lcfi187:
	.cfi_offset %r13, -40
.Lcfi188:
	.cfi_offset %r14, -32
.Lcfi189:
	.cfi_offset %r15, -24
.Lcfi190:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	552(%r14), %rsi
	callq	new_internal_production
	movq	%rax, %r15
	movq	%r15, %r13
	movb	60(%r15), %al
	andb	$-29, %al
	orb	$12, %al
	movb	%al, 60(%r15)
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r15, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	cmpl	$0, 252(%r14)
	je	.LBB19_1
# BB#2:
	movq	%r14, (%rsp)            # 8-byte Spill
	movq	560(%r14), %rbp
	movq	40(%rbp), %rax
	movl	32(%rbp), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rax
	leaq	48(%rbx), %r14
	movq	%r14, 40(%rbx)
	movl	$1, 32(%rbx)
	movq	%rax, 48(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r13, 16(%rax)
	movq	%rbp, 8(%rax)
	movq	40(%rbp), %rcx
	movl	32(%rbp), %edx
	decl	%edx
	movq	%rax, (%rcx,%rdx,8)
	movq	40(%rbx), %r12
	movq	(%r12), %rax
	movq	%rbx, 8(%rax)
	testq	%r12, %r12
	je	.LBB19_3
# BB#4:
	movl	32(%rbx), %ebp
	cmpq	%r14, %r12
	je	.LBB19_5
# BB#7:
	testb	$7, %bpl
	jne	.LBB19_6
	jmp	.LBB19_8
.LBB19_1:
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 8(%rax)
	movq	%rbx, %rcx
	addq	$48, %rcx
	movq	%rcx, 40(%rbx)
	movl	$1, 32(%rbx)
	movq	%rax, 48(%rbx)
	movq	560(%r14), %rbp
	movq	40(%rbp), %rax
	movl	32(%rbp), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %rax
	movl	$2, 32(%rbx)
	movq	%rax, 56(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r13, 16(%rax)
	movq	%rbp, 8(%rax)
	movq	40(%rbp), %rcx
	movl	32(%rbp), %edx
	decl	%edx
	movq	%rax, (%rcx,%rdx,8)
	movq	40(%rbx), %rax
	movq	8(%rax), %rax
	movq	%rbx, 8(%rax)
	jmp	.LBB19_10
.LBB19_3:
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 8(%rax)
	movq	%r14, 40(%rbx)
	movl	32(%rbx), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 32(%rbx)
	movq	%rax, 48(%rbx,%rcx,8)
	jmp	.LBB19_9
.LBB19_5:
	cmpl	$2, %ebp
	ja	.LBB19_8
.LBB19_6:
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 8(%rax)
	leal	1(%rbp), %ecx
	movl	%ecx, 32(%rbx)
	movq	%rax, (%r12,%rbp,8)
	jmp	.LBB19_9
.LBB19_8:
	movq	%rbx, %r12
	addq	$32, %r12
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r13, 16(%rax)
	movq	%rbx, 8(%rax)
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	vec_add_internal
.LBB19_9:
	movq	(%rsp), %r14            # 8-byte Reload
.LBB19_10:
	movq	24(%r13), %rax
	leaq	16(%r15), %r12
	addq	$32, %r15
	testq	%rax, %rax
	je	.LBB19_11
# BB#12:
	movl	(%r12), %ecx
	cmpq	%r15, %rax
	je	.LBB19_13
# BB#15:
	testb	$7, %cl
	je	.LBB19_18
# BB#16:
	leal	1(%rcx), %edx
	jmp	.LBB19_17
.LBB19_11:
	movq	%r15, 24(%r13)
	movl	16(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r13)
	movq	%rbx, 32(%r13,%rax,8)
	jmp	.LBB19_19
.LBB19_13:
	cmpl	$2, %ecx
	ja	.LBB19_18
# BB#14:
	movl	%ecx, %edx
	incl	%edx
.LBB19_17:
	movl	%edx, (%r12)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB19_19
.LBB19_18:
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB19_19:
	movq	24(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB19_20
# BB#21:
	movl	(%r12), %eax
	cmpq	%r15, %rcx
	je	.LBB19_22
# BB#25:
	testb	$7, %al
	jne	.LBB19_23
	jmp	.LBB19_26
.LBB19_20:
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r13, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	%r15, 24(%r13)
	movl	16(%r13), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r13)
	movq	%rbx, 32(%r13,%rax,8)
	jmp	.LBB19_24
.LBB19_22:
	cmpl	$2, %eax
	ja	.LBB19_26
.LBB19_23:
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r13, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	24(%r13), %rax
	movl	16(%r13), %ecx
	leal	1(%rcx), %edx
	movl	%edx, 16(%r13)
	movq	%rbx, (%rax,%rcx,8)
.LBB19_24:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_26:
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%r13, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	vec_add_internal        # TAILCALL
.Lfunc_end19:
	.size	star_EBNF, .Lfunc_end19-star_EBNF
	.cfi_endproc

	.globl	plus_EBNF
	.p2align	4, 0x90
	.type	plus_EBNF,@function
plus_EBNF:                              # @plus_EBNF
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi191:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi192:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi193:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi194:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi195:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi196:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi197:
	.cfi_def_cfa_offset 96
.Lcfi198:
	.cfi_offset %rbx, -56
.Lcfi199:
	.cfi_offset %r12, -48
.Lcfi200:
	.cfi_offset %r13, -40
.Lcfi201:
	.cfi_offset %r14, -32
.Lcfi202:
	.cfi_offset %r15, -24
.Lcfi203:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	552(%r12), %rsi
	callq	new_internal_production
	movq	%rax, %rbp
	movb	60(%rbp), %al
	andb	$-29, %al
	orb	$16, %al
	movb	%al, 60(%rbp)
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	%rbp, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r12), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%eax, 152(%rbx)
	movq	560(%r12), %r13
	movq	40(%r13), %rax
	movl	32(%r13), %ecx
	decl	%ecx
	movq	(%rax,%rcx,8), %r14
	movl	252(%r12), %r15d
	movl	$32, %edi
	callq	malloc
	cmpl	$0, %r15d
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %r15
	movq	%r14, 24(%rsp)          # 8-byte Spill
	je	.LBB20_1
# BB#2:
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r15, 8(%rax)
	addq	$48, %rbx
	movq	%rbx, 40(%r15)
	movl	$1, 32(%r15)
	movq	%rax, 48(%r15)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbp, 16(%rax)
	movq	%r13, 8(%rax)
	movq	40(%r13), %rcx
	movl	32(%r13), %edx
	decl	%edx
	movq	%rax, (%rcx,%rdx,8)
	movq	%rbp, %r14
	movq	40(%r15), %rbp
	movl	$32, %edi
	testq	%rbp, %rbp
	je	.LBB20_3
# BB#4:
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r14, 16(%rax)
	movq	%r15, 8(%rax)
	movl	$2, 32(%r15)
	movq	%rax, 8(%rbp)
	jmp	.LBB20_5
.LBB20_1:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbp, 16(%rax)
	movq	%r15, 8(%rax)
	addq	$48, %rbx
	movq	%rbx, 40(%r15)
	movl	$1, 32(%r15)
	movq	%rax, 48(%r15)
	movl	$32, %edi
	callq	malloc
	movups	(%r14), %xmm0
	movups	16(%r14), %xmm1
	movups	%xmm1, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r15, 8(%rax)
	movl	$2, 32(%r15)
	movq	%rax, 56(%r15)
	movq	560(%r12), %rbx
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbp, 16(%rax)
	movq	%rbx, 8(%rax)
	movq	40(%rbx), %rcx
	movl	32(%rbx), %edx
	decl	%edx
	movq	%rax, (%rcx,%rdx,8)
	movq	16(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB20_6
.LBB20_3:
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r14, 16(%rax)
	movq	%r15, 8(%rax)
	movq	%rbx, 40(%r15)
	movl	$2, 32(%r15)
	movq	%rax, 56(%r15)
.LBB20_5:
	movq	16(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rbp
.LBB20_6:
	movq	24(%rbp), %rax
	leaq	16(%rdx), %rsi
	addq	$32, %rdx
	testq	%rax, %rax
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	je	.LBB20_7
# BB#8:
	movl	(%rsi), %ecx
	cmpq	%rdx, %rax
	je	.LBB20_9
# BB#11:
	testb	$7, %cl
	je	.LBB20_15
# BB#12:
	leal	1(%rcx), %edx
	jmp	.LBB20_13
.LBB20_7:
	movq	%rdx, 24(%rbp)
	movl	16(%rbp), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rbp)
	movq	%r15, 32(%rbp,%rax,8)
	jmp	.LBB20_14
.LBB20_9:
	cmpl	$2, %ecx
	ja	.LBB20_15
# BB#10:
	movl	%ecx, %edx
	incl	%edx
.LBB20_13:
	movl	%edx, (%rsi)
	movq	%r15, (%rax,%rcx,8)
.LBB20_14:
	movq	24(%rsp), %r14          # 8-byte Reload
	movl	12(%rsp), %r13d         # 4-byte Reload
	jmp	.LBB20_16
.LBB20_15:
	movq	%rsi, %rdi
	movq	%r15, %rsi
	callq	vec_add_internal
	movl	576(%r12), %r13d
	movq	24(%rsp), %r14          # 8-byte Reload
.LBB20_16:
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	leaq	48(%rbx), %r15
	movq	%rbp, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	%r13d, 152(%rbx)
	movq	%r15, 40(%rbx)
	movl	$1, 32(%rbx)
	movq	%r14, 48(%rbx)
	movq	%rbx, 8(%r14)
	movq	24(%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB20_17
# BB#18:
	movq	32(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi), %eax
	cmpq	16(%rsp), %rcx          # 8-byte Folded Reload
	je	.LBB20_19
# BB#22:
	testb	$7, %al
	je	.LBB20_25
# BB#23:
	leal	1(%rax), %edx
	movl	%edx, (%rdi)
	movq	%rbx, (%rcx,%rax,8)
	jmp	.LBB20_24
.LBB20_17:
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%rax, 24(%rbp)
	movl	16(%rbp), %eax
	leal	1(%rax), %ecx
	jmp	.LBB20_21
.LBB20_19:
	cmpl	$2, %eax
	ja	.LBB20_25
# BB#20:
	movl	%eax, %ecx
	incl	%ecx
.LBB20_21:
	movl	%ecx, 16(%rbp)
	movq	%rbx, 32(%rbp,%rax,8)
.LBB20_24:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_25:
	movq	%rbx, %rsi
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	vec_add_internal        # TAILCALL
.Lfunc_end20:
	.size	plus_EBNF, .Lfunc_end20-plus_EBNF
	.cfi_endproc

	.globl	initialize_productions
	.p2align	4, 0x90
	.type	initialize_productions,@function
initialize_productions:                 # @initialize_productions
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi204:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi205:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi206:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi207:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi208:
	.cfi_def_cfa_offset 48
.Lcfi209:
	.cfi_offset %rbx, -48
.Lcfi210:
	.cfi_offset %r12, -40
.Lcfi211:
	.cfi_offset %r13, -32
.Lcfi212:
	.cfi_offset %r14, -24
.Lcfi213:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movabsq	$32776860187435056, %r12 # imm = 0x74726174532030
	movl	$8, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB21_2
# BB#1:
	movq	%r12, (%rax)
.LBB21_2:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	new_production
	movq	%rax, %r15
	movb	60(%r15), %al
	andb	$-29, %al
	orb	$4, %al
	movb	%al, 60(%r15)
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	leaq	48(%rbx), %r13
	movq	%r15, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r13, 40(%rbx)
	movl	$1, 32(%rbx)
	movq	%rax, 48(%rbx)
	movq	24(%r15), %rax
	leaq	32(%r15), %rdx
	testq	%rax, %rax
	je	.LBB21_3
# BB#4:
	addq	$16, %r15
	movl	(%r15), %ecx
	cmpq	%rdx, %rax
	je	.LBB21_5
# BB#7:
	testb	$7, %cl
	je	.LBB21_10
# BB#8:
	leal	1(%rcx), %edx
	jmp	.LBB21_9
.LBB21_3:
	movq	%rdx, 24(%r15)
	movl	16(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r15)
	movq	%rbx, 32(%r15,%rax,8)
	jmp	.LBB21_11
.LBB21_5:
	cmpl	$2, %ecx
	ja	.LBB21_10
# BB#6:
	movl	%ecx, %edx
	incl	%edx
.LBB21_9:
	movl	%edx, (%r15)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB21_11
.LBB21_10:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB21_11:
	movl	$8, %edi
	callq	malloc
	testq	%rax, %rax
	je	.LBB21_13
# BB#12:
	incq	%r12
	movq	%r12, (%rax)
.LBB21_13:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	new_production
	movb	60(%rax), %cl
	andb	$-29, %cl
	orb	$4, %cl
	movb	%cl, 60(%rax)
	movq	40(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 16(%rcx)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end21:
	.size	initialize_productions, .Lfunc_end21-initialize_productions
	.cfi_endproc

	.globl	finish_productions
	.p2align	4, 0x90
	.type	finish_productions,@function
finish_productions:                     # @finish_productions
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi214:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi215:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi216:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi218:
	.cfi_def_cfa_offset 48
.Lcfi219:
	.cfi_offset %rbx, -40
.Lcfi220:
	.cfi_offset %r12, -32
.Lcfi221:
	.cfi_offset %r14, -24
.Lcfi222:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	16(%r14), %rax
	movq	8(%rax), %r15
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	leaq	48(%rbx), %r12
	movq	%r15, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r14), %eax
	movl	%eax, 152(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rbx, 8(%rax)
	movq	%r12, 40(%rbx)
	movl	$1, 32(%rbx)
	movq	%rax, 48(%rbx)
	movq	24(%r15), %rax
	leaq	32(%r15), %rdx
	testq	%rax, %rax
	je	.LBB22_1
# BB#2:
	addq	$16, %r15
	movl	(%r15), %ecx
	cmpq	%rdx, %rax
	je	.LBB22_3
# BB#5:
	testb	$7, %cl
	je	.LBB22_8
# BB#6:
	leal	1(%rcx), %edx
	jmp	.LBB22_7
.LBB22_1:
	movq	%rdx, 24(%r15)
	movl	16(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 16(%r15)
	movq	%rbx, 32(%r15,%rax,8)
	jmp	.LBB22_9
.LBB22_3:
	cmpl	$2, %ecx
	ja	.LBB22_8
# BB#4:
	movl	%ecx, %edx
	incl	%edx
.LBB22_7:
	movl	%edx, (%r15)
	movq	%rbx, (%rax,%rcx,8)
	jmp	.LBB22_9
.LBB22_8:
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	vec_add_internal
.LBB22_9:
	movq	16(%r14), %rax
	movq	16(%rax), %rax
	movq	40(%rbx), %rcx
	movq	(%rcx), %rcx
	movq	%rax, 16(%rcx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end22:
	.size	finish_productions, .Lfunc_end22-finish_productions
	.cfi_endproc

	.globl	print_term
	.p2align	4, 0x90
	.type	print_term,@function
print_term:                             # @print_term
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi223:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi224:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi225:
	.cfi_def_cfa_offset 32
.Lcfi226:
	.cfi_offset %rbx, -24
.Lcfi227:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB23_1
# BB#2:
	callq	escape_string
	movq	%rax, %r14
	jmp	.LBB23_3
.LBB23_1:
	xorl	%r14d, %r14d
.LBB23_3:
	movl	(%rbx), %eax
	cmpq	$3, %rax
	ja	.LBB23_13
# BB#4:
	jmpq	*.LJTI23_0(,%rax,8)
.LBB23_5:
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.LBB23_7
# BB#6:
	cmpb	$0, (%rax)
	je	.LBB23_7
# BB#8:
	movl	$.L.str.9, %edi
	jmp	.LBB23_9
.LBB23_13:
	movl	$.L.str.13, %edi
	xorl	%eax, %eax
	callq	d_fail
	testq	%r14, %r14
	jne	.LBB23_16
	jmp	.LBB23_15
.LBB23_10:
	movl	$.L.str.10, %edi
	jmp	.LBB23_9
.LBB23_11:
	movl	$.L.str.11, %edi
	jmp	.LBB23_9
.LBB23_12:
	movl	$.L.str.12, %edi
.LBB23_9:
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	printf
	testq	%r14, %r14
	je	.LBB23_15
.LBB23_16:
	movq	%r14, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	free                    # TAILCALL
.LBB23_7:
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	testq	%r14, %r14
	jne	.LBB23_16
.LBB23_15:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	print_term, .Lfunc_end23-print_term
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI23_0:
	.quad	.LBB23_5
	.quad	.LBB23_10
	.quad	.LBB23_11
	.quad	.LBB23_12

	.text
	.globl	print_elem
	.p2align	4, 0x90
	.type	print_elem,@function
print_elem:                             # @print_elem
	.cfi_startproc
# BB#0:
	movl	(%rdi), %eax
	cmpl	$1, %eax
	jne	.LBB24_1
# BB#5:
	movq	16(%rdi), %rdi
	jmp	print_term              # TAILCALL
.LBB24_1:
	cmpl	$2, %eax
	jne	.LBB24_4
# BB#2:
	movq	16(%rdi), %rsi
	jmp	.LBB24_3
.LBB24_4:
	movq	16(%rdi), %rax
	movq	(%rax), %rsi
.LBB24_3:
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	jmp	printf                  # TAILCALL
.Lfunc_end24:
	.size	print_elem, .Lfunc_end24-print_elem
	.cfi_endproc

	.globl	print_rule
	.p2align	4, 0x90
	.type	print_rule,@function
print_rule:                             # @print_rule
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi228:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi229:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi230:
	.cfi_def_cfa_offset 32
.Lcfi231:
	.cfi_offset %rbx, -24
.Lcfi232:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	8(%r14), %rax
	movq	(%rax), %rsi
	movl	$.L.str.23, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 32(%r14)
	je	.LBB25_9
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB25_2:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rax
	movq	(%rax,%rbx,8), %rax
	movl	(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB25_4
# BB#3:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	16(%rax), %rdi
	callq	print_term
	jmp	.LBB25_8
	.p2align	4, 0x90
.LBB25_4:                               #   in Loop: Header=BB25_2 Depth=1
	cmpl	$2, %ecx
	jne	.LBB25_6
# BB#5:                                 #   in Loop: Header=BB25_2 Depth=1
	movq	16(%rax), %rsi
	jmp	.LBB25_7
	.p2align	4, 0x90
.LBB25_6:                               #   in Loop: Header=BB25_2 Depth=1
	movq	16(%rax), %rax
	movq	(%rax), %rsi
.LBB25_7:                               # %print_elem.exit
                                        #   in Loop: Header=BB25_2 Depth=1
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB25_8:                               # %print_elem.exit
                                        #   in Loop: Header=BB25_2 Depth=1
	incq	%rbx
	cmpl	32(%r14), %ebx
	jb	.LBB25_2
.LBB25_9:                               # %._crit_edge
	movq	80(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB25_11
# BB#10:
	movl	$.L.str.24, %edi
	xorl	%eax, %eax
	callq	printf
.LBB25_11:
	movq	96(%r14), %rsi
	testq	%rsi, %rsi
	je	.LBB25_12
# BB#13:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	printf                  # TAILCALL
.LBB25_12:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end25:
	.size	print_rule, .Lfunc_end25-print_rule
	.cfi_endproc

	.globl	print_grammar
	.p2align	4, 0x90
	.type	print_grammar,@function
print_grammar:                          # @print_grammar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi233:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi234:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi235:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi236:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi237:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi238:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi239:
	.cfi_def_cfa_offset 64
.Lcfi240:
	.cfi_offset %rbx, -56
.Lcfi241:
	.cfi_offset %r12, -48
.Lcfi242:
	.cfi_offset %r13, -40
.Lcfi243:
	.cfi_offset %r14, -32
.Lcfi244:
	.cfi_offset %r15, -24
.Lcfi245:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpl	$0, 8(%r14)
	je	.LBB26_69
# BB#1:
	movl	$.Lstr, %edi
	callq	puts
	cmpl	$0, 8(%r14)
	je	.LBB26_65
# BB#2:                                 # %.lr.ph66
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB26_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB26_5 Depth 2
                                        #       Child Loop BB26_10 Depth 3
	movq	16(%r14), %rax
	movl	%r15d, %ecx
	movq	(%rax,%rcx,8), %r12
	movq	(%r12), %rsi
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	printf
	cmpl	$0, 16(%r12)
	je	.LBB26_64
# BB#4:                                 # %.lr.ph62
                                        #   in Loop: Header=BB26_3 Depth=1
	xorl	%r13d, %r13d
	jmp	.LBB26_5
.LBB26_36:                              #   in Loop: Header=BB26_5 Depth=2
	movl	$assoc_strings+8, %eax
	jmp	.LBB26_37
.LBB26_57:                              #   in Loop: Header=BB26_5 Depth=2
	movl	$assoc_strings+8, %eax
	jmp	.LBB26_58
	.p2align	4, 0x90
.LBB26_5:                               #   Parent Loop BB26_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB26_10 Depth 3
	movq	24(%r12), %rax
	movl	%r13d, %ecx
	movq	(%rax,%rcx,8), %rbx
	testl	%r13d, %r13d
	je	.LBB26_6
# BB#7:                                 #   in Loop: Header=BB26_5 Depth=2
	movl	$.L.str.29, %edi
	jmp	.LBB26_8
	.p2align	4, 0x90
.LBB26_6:                               #   in Loop: Header=BB26_5 Depth=2
	movl	$.L.str.28, %edi
.LBB26_8:                               # %.preheader
                                        #   in Loop: Header=BB26_5 Depth=2
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 32(%rbx)
	je	.LBB26_17
# BB#9:                                 # %.lr.ph58
                                        #   in Loop: Header=BB26_5 Depth=2
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB26_10:                              #   Parent Loop BB26_3 Depth=1
                                        #     Parent Loop BB26_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	40(%rbx), %rax
	movl	%ebp, %ecx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB26_12
# BB#11:                                #   in Loop: Header=BB26_10 Depth=3
	movq	16(%rax), %rdi
	callq	print_term
	jmp	.LBB26_16
	.p2align	4, 0x90
.LBB26_12:                              #   in Loop: Header=BB26_10 Depth=3
	cmpl	$2, %ecx
	jne	.LBB26_14
# BB#13:                                #   in Loop: Header=BB26_10 Depth=3
	movq	16(%rax), %rsi
	jmp	.LBB26_15
	.p2align	4, 0x90
.LBB26_14:                              #   in Loop: Header=BB26_10 Depth=3
	movq	16(%rax), %rax
	movq	(%rax), %rsi
.LBB26_15:                              # %print_elem.exit
                                        #   in Loop: Header=BB26_10 Depth=3
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB26_16:                              # %print_elem.exit
                                        #   in Loop: Header=BB26_10 Depth=3
	incl	%ebp
	cmpl	32(%rbx), %ebp
	jb	.LBB26_10
.LBB26_17:                              # %._crit_edge59
                                        #   in Loop: Header=BB26_5 Depth=2
	movl	16(%rbx), %esi
	testl	%esi, %esi
	je	.LBB26_19
# BB#18:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	callq	printf
.LBB26_19:                              #   in Loop: Header=BB26_5 Depth=2
	movl	20(%rbx), %eax
	testl	%eax, %eax
	je	.LBB26_38
# BB#20:                                #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings(%rip)
	jne	.LBB26_22
# BB#21:                                #   in Loop: Header=BB26_5 Depth=2
	xorl	%eax, %eax
	jmp	.LBB26_24
	.p2align	4, 0x90
.LBB26_22:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+16(%rip)
	jne	.LBB26_25
# BB#23:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$1, %eax
	jmp	.LBB26_24
.LBB26_25:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+32(%rip)
	jne	.LBB26_27
# BB#26:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$2, %eax
	jmp	.LBB26_24
.LBB26_27:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+48(%rip)
	jne	.LBB26_29
# BB#28:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$3, %eax
	jmp	.LBB26_24
.LBB26_29:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+64(%rip)
	jne	.LBB26_31
# BB#30:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$4, %eax
	jmp	.LBB26_24
.LBB26_31:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+80(%rip)
	jne	.LBB26_33
# BB#32:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$5, %eax
	jmp	.LBB26_24
.LBB26_33:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+96(%rip)
	jne	.LBB26_35
# BB#34:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$6, %eax
	jmp	.LBB26_24
.LBB26_35:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+112(%rip)
	jne	.LBB26_36
# BB#70:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$7, %eax
	.p2align	4, 0x90
.LBB26_24:                              #   in Loop: Header=BB26_5 Depth=2
	shlq	$4, %rax
	leaq	assoc_strings+8(%rax), %rax
.LBB26_37:                              # %assoc_str.exit
                                        #   in Loop: Header=BB26_5 Depth=2
	movq	(%rax), %rsi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB26_38:                              #   in Loop: Header=BB26_5 Depth=2
	movl	24(%rbx), %esi
	testl	%esi, %esi
	je	.LBB26_40
# BB#39:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	printf
.LBB26_40:                              #   in Loop: Header=BB26_5 Depth=2
	movl	28(%rbx), %eax
	testl	%eax, %eax
	je	.LBB26_59
# BB#41:                                #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings(%rip)
	jne	.LBB26_43
# BB#42:                                #   in Loop: Header=BB26_5 Depth=2
	xorl	%eax, %eax
	jmp	.LBB26_45
	.p2align	4, 0x90
.LBB26_43:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+16(%rip)
	jne	.LBB26_46
# BB#44:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$1, %eax
	jmp	.LBB26_45
.LBB26_46:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+32(%rip)
	jne	.LBB26_48
# BB#47:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$2, %eax
	jmp	.LBB26_45
.LBB26_48:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+48(%rip)
	jne	.LBB26_50
# BB#49:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$3, %eax
	jmp	.LBB26_45
.LBB26_50:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+64(%rip)
	jne	.LBB26_52
# BB#51:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$4, %eax
	jmp	.LBB26_45
.LBB26_52:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+80(%rip)
	jne	.LBB26_54
# BB#53:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$5, %eax
	jmp	.LBB26_45
.LBB26_54:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+96(%rip)
	jne	.LBB26_56
# BB#55:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$6, %eax
	jmp	.LBB26_45
.LBB26_56:                              #   in Loop: Header=BB26_5 Depth=2
	cmpl	%eax, assoc_strings+112(%rip)
	jne	.LBB26_57
# BB#71:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$7, %eax
	.p2align	4, 0x90
.LBB26_45:                              #   in Loop: Header=BB26_5 Depth=2
	shlq	$4, %rax
	leaq	assoc_strings+8(%rax), %rax
.LBB26_58:                              # %assoc_str.exit55
                                        #   in Loop: Header=BB26_5 Depth=2
	movq	(%rax), %rsi
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB26_59:                              #   in Loop: Header=BB26_5 Depth=2
	movq	80(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB26_61
# BB#60:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB26_61:                              #   in Loop: Header=BB26_5 Depth=2
	movq	96(%rbx), %rsi
	testq	%rsi, %rsi
	je	.LBB26_63
# BB#62:                                #   in Loop: Header=BB26_5 Depth=2
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB26_63:                              #   in Loop: Header=BB26_5 Depth=2
	movl	$10, %edi
	callq	putchar
	incl	%r13d
	cmpl	16(%r12), %r13d
	jb	.LBB26_5
.LBB26_64:                              # %._crit_edge63
                                        #   in Loop: Header=BB26_3 Depth=1
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$10, %edi
	callq	putchar
	incl	%r15d
	cmpl	8(%r14), %r15d
	jb	.LBB26_3
.LBB26_65:                              # %._crit_edge67
	movl	$.Lstr.1, %edi
	callq	puts
	cmpl	$0, 48(%r14)
	je	.LBB26_68
# BB#66:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB26_67:                              # =>This Inner Loop Header: Depth=1
	movl	$9, %edi
	callq	putchar
	movq	56(%r14), %rax
	movl	%ebx, %ecx
	movq	(%rax,%rcx,8), %rdi
	callq	print_term
	movl	8(%r14), %esi
	addl	%ebx, %esi
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	printf
	incl	%ebx
	cmpl	48(%r14), %ebx
	jb	.LBB26_67
.LBB26_68:                              # %._crit_edge
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	putchar                 # TAILCALL
.LBB26_69:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	print_grammar, .Lfunc_end26-print_grammar
	.cfi_endproc

	.globl	print_states
	.p2align	4, 0x90
	.type	print_states,@function
print_states:                           # @print_states
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi246:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi247:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi248:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi249:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi250:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi251:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi252:
	.cfi_def_cfa_offset 80
.Lcfi253:
	.cfi_offset %rbx, -56
.Lcfi254:
	.cfi_offset %r12, -48
.Lcfi255:
	.cfi_offset %r13, -40
.Lcfi256:
	.cfi_offset %r14, -32
.Lcfi257:
	.cfi_offset %r15, -24
.Lcfi258:
	.cfi_offset %rbp, -16
	cmpl	$0, 88(%rdi)
	je	.LBB27_44
# BB#1:                                 # %.lr.ph
	xorl	%ecx, %ecx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB27_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB27_4 Depth 2
                                        #       Child Loop BB27_6 Depth 3
                                        #     Child Loop BB27_21 Depth 2
                                        #     Child Loop BB27_46 Depth 2
                                        #     Child Loop BB27_32 Depth 2
	movq	96(%rdi), %rax
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %r13
	movl	(%r13), %esi
	movl	16(%r13), %edx
	testb	$1, 376(%r13)
	movl	$.L.str.50, %ecx
	movl	$.L.str.49, %eax
	cmovneq	%rax, %rcx
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, 16(%r13)
	je	.LBB27_18
# BB#3:                                 # %.lr.ph62.i
                                        #   in Loop: Header=BB27_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_4:                               #   Parent Loop BB27_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB27_6 Depth 3
	movq	24(%r13), %rax
	movq	(%rax,%rbx,8), %rbp
	movq	8(%rbp), %rax
	movq	8(%rax), %rax
	movq	(%rax), %rsi
	movl	$.L.str.58, %edi
	xorl	%eax, %eax
	callq	printf
	movq	8(%rbp), %rax
	cmpl	$0, 32(%rax)
	je	.LBB27_16
# BB#5:                                 # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB27_4 Depth=2
	movl	$1, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB27_6:                               # %.lr.ph.i.i
                                        #   Parent Loop BB27_2 Depth=1
                                        #     Parent Loop BB27_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	40(%rax), %rax
	movq	(%rax,%r14,8), %r12
	cmpq	%rbp, %r12
	jne	.LBB27_8
# BB#7:                                 #   in Loop: Header=BB27_6 Depth=3
	xorl	%r15d, %r15d
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	printf
.LBB27_8:                               #   in Loop: Header=BB27_6 Depth=3
	movl	(%r12), %eax
	cmpl	$1, %eax
	jne	.LBB27_10
# BB#9:                                 #   in Loop: Header=BB27_6 Depth=3
	movq	16(%r12), %rdi
	callq	print_term
	jmp	.LBB27_14
	.p2align	4, 0x90
.LBB27_10:                              #   in Loop: Header=BB27_6 Depth=3
	cmpl	$2, %eax
	jne	.LBB27_12
# BB#11:                                #   in Loop: Header=BB27_6 Depth=3
	movq	16(%r12), %rsi
	jmp	.LBB27_13
	.p2align	4, 0x90
.LBB27_12:                              #   in Loop: Header=BB27_6 Depth=3
	movq	16(%r12), %rax
	movq	(%rax), %rsi
.LBB27_13:                              # %print_elem.exit.i.i
                                        #   in Loop: Header=BB27_6 Depth=3
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB27_14:                              # %print_elem.exit.i.i
                                        #   in Loop: Header=BB27_6 Depth=3
	incq	%r14
	movq	8(%rbp), %rax
	cmpl	32(%rax), %r14d
	jb	.LBB27_6
# BB#15:                                # %._crit_edge.i.i
                                        #   in Loop: Header=BB27_4 Depth=2
	testl	%r15d, %r15d
	je	.LBB27_17
.LBB27_16:                              # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB27_4 Depth=2
	movl	$.L.str.59, %edi
	xorl	%eax, %eax
	callq	printf
.LBB27_17:                              # %print_item.exit.i
                                        #   in Loop: Header=BB27_4 Depth=2
	movl	$10, %edi
	callq	putchar
	incq	%rbx
	cmpl	16(%r13), %ebx
	jb	.LBB27_4
.LBB27_18:                              # %._crit_edge63.i
                                        #   in Loop: Header=BB27_2 Depth=1
	cmpl	$0, 96(%r13)
	je	.LBB27_28
# BB#19:                                # %.preheader50.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	$.Lstr.4, %edi
	callq	puts
	cmpl	$0, 96(%r13)
	je	.LBB27_28
# BB#20:                                # %.lr.ph58.i
                                        #   in Loop: Header=BB27_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_21:                              #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$9, %edi
	callq	putchar
	movq	104(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rax
	movl	(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB27_23
# BB#22:                                #   in Loop: Header=BB27_21 Depth=2
	movq	16(%rax), %rdi
	callq	print_term
	jmp	.LBB27_27
	.p2align	4, 0x90
.LBB27_23:                              #   in Loop: Header=BB27_21 Depth=2
	cmpl	$2, %ecx
	jne	.LBB27_25
# BB#24:                                #   in Loop: Header=BB27_21 Depth=2
	movq	16(%rax), %rsi
	jmp	.LBB27_26
	.p2align	4, 0x90
.LBB27_25:                              #   in Loop: Header=BB27_21 Depth=2
	movq	16(%rax), %rax
	movq	(%rax), %rsi
.LBB27_26:                              # %print_elem.exit.i
                                        #   in Loop: Header=BB27_21 Depth=2
	movl	$.L.str.14, %edi
	xorl	%eax, %eax
	callq	printf
.LBB27_27:                              # %print_elem.exit.i
                                        #   in Loop: Header=BB27_21 Depth=2
	movq	104(%r13), %rax
	movq	(%rax,%rbx,8), %rax
	movq	8(%rax), %rax
	movl	(%rax), %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpl	96(%r13), %ebx
	jb	.LBB27_21
.LBB27_28:                              # %._crit_edge59.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	$.Lstr.3, %edi
	callq	puts
	cmpl	$0, 176(%r13)
	je	.LBB27_29
# BB#45:                                # %.lr.ph55.i
                                        #   in Loop: Header=BB27_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_46:                              #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	184(%r13), %rax
	movq	(%rax,%rbx,8), %rbp
	movl	(%rbp), %eax
	movq	action_types(,%rax,8), %rsi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	printf
	movq	16(%rbp), %rdi
	callq	print_rule
	movl	$10, %edi
	callq	putchar
	incq	%rbx
	movl	176(%r13), %eax
	cmpl	%eax, %ebx
	jb	.LBB27_46
	jmp	.LBB27_30
	.p2align	4, 0x90
.LBB27_29:                              #   in Loop: Header=BB27_2 Depth=1
	xorl	%eax, %eax
.LBB27_30:                              # %.preheader.i
                                        #   in Loop: Header=BB27_2 Depth=1
	cmpl	$0, 136(%r13)
	je	.LBB27_36
# BB#31:                                # %.lr.ph.i
                                        #   in Loop: Header=BB27_2 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB27_32:                              #   Parent Loop BB27_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	144(%r13), %rax
	movq	(%rax,%rbx,8), %rbp
	movl	(%rbp), %eax
	movq	action_types(,%rax,8), %rsi
	movl	$.L.str.54, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$1, (%rbp)
	jne	.LBB27_34
# BB#33:                                #   in Loop: Header=BB27_32 Depth=2
	movq	8(%rbp), %rdi
	callq	print_term
	movq	24(%rbp), %rax
	movl	(%rax), %esi
	movl	$.L.str.55, %edi
	xorl	%eax, %eax
	callq	printf
.LBB27_34:                              #   in Loop: Header=BB27_32 Depth=2
	movl	$10, %edi
	callq	putchar
	incq	%rbx
	cmpl	136(%r13), %ebx
	jb	.LBB27_32
# BB#35:                                # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	176(%r13), %eax
.LBB27_36:                              # %._crit_edge.i
                                        #   in Loop: Header=BB27_2 Depth=1
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	jb	.LBB27_38
# BB#37:                                # %print_conflict.exit.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.L.str.64, %edi
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
	movl	176(%r13), %eax
	movl	$1, %ecx
.LBB27_38:                              #   in Loop: Header=BB27_2 Depth=1
	testl	%eax, %eax
	je	.LBB27_43
# BB#39:                                #   in Loop: Header=BB27_2 Depth=1
	cmpl	$0, 136(%r13)
	je	.LBB27_43
# BB#40:                                #   in Loop: Header=BB27_2 Depth=1
	testl	%ecx, %ecx
	jne	.LBB27_42
# BB#41:                                #   in Loop: Header=BB27_2 Depth=1
	movl	$.Lstr.5, %edi
	callq	puts
.LBB27_42:                              # %print_conflict.exit45.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	$.L.str.64, %edi
	movl	$.L.str.57, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$10, %edi
	callq	putchar
.LBB27_43:                              # %print_state.exit
                                        #   in Loop: Header=BB27_2 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movq	8(%rsp), %rdi           # 8-byte Reload
	cmpl	88(%rdi), %ecx
	jb	.LBB27_2
.LBB27_44:                              # %._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end27:
	.size	print_states, .Lfunc_end27-print_states
	.cfi_endproc

	.globl	state_for_declaration
	.p2align	4, 0x90
	.type	state_for_declaration,@function
state_for_declaration:                  # @state_for_declaration
	.cfi_startproc
# BB#0:
	movl	160(%rdi), %eax
	testl	%eax, %eax
	je	.LBB28_6
# BB#1:                                 # %.lr.ph
	movq	168(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB28_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rdi
	cmpl	$5, 8(%rdi)
	jne	.LBB28_5
# BB#3:                                 #   in Loop: Header=BB28_2 Depth=1
	movq	(%rdi), %rdi
	movq	16(%rdi), %rdi
	cmpl	%esi, 56(%rdi)
	je	.LBB28_4
.LBB28_5:                               #   in Loop: Header=BB28_2 Depth=1
	incq	%rdx
	cmpl	%eax, %edx
	jb	.LBB28_2
.LBB28_6:
	xorl	%eax, %eax
	retq
.LBB28_4:
	movl	$1, %eax
	retq
.Lfunc_end28:
	.size	state_for_declaration, .Lfunc_end28-state_for_declaration
	.cfi_endproc

	.globl	build_eq
	.p2align	4, 0x90
	.type	build_eq,@function
build_eq:                               # @build_eq
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi259:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi260:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi261:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi262:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi263:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi264:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi265:
	.cfi_def_cfa_offset 112
.Lcfi266:
	.cfi_offset %rbx, -56
.Lcfi267:
	.cfi_offset %r12, -48
.Lcfi268:
	.cfi_offset %r13, -40
.Lcfi269:
	.cfi_offset %r14, -32
.Lcfi270:
	.cfi_offset %r15, -24
.Lcfi271:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	88(%rbx), %ebp
	movq	%rbp, %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,2), %r15
	movq	%r15, %rdi
	callq	malloc
	xorl	%esi, %esi
	movq	%rax, %rdi
	movq	%r15, %rdx
	movq	%rax, %r15
	callq	memset
	movq	%rbx, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB29_1:                               # %.preheader206
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB29_3 Depth 2
                                        #       Child Loop BB29_18 Depth 3
                                        #       Child Loop BB29_31 Depth 3
                                        #       Child Loop BB29_10 Depth 3
	testl	%ebp, %ebp
	je	.LBB29_43
# BB#2:                                 # %.lr.ph226.preheader
                                        #   in Loop: Header=BB29_1 Depth=1
	xorl	%eax, %eax
	movl	$1, %esi
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB29_3:                               # %.lr.ph226
                                        #   Parent Loop BB29_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB29_18 Depth 3
                                        #       Child Loop BB29_31 Depth 3
                                        #       Child Loop BB29_10 Depth 3
	movq	%rax, %rcx
	leaq	1(%rcx), %rax
	cmpl	%ebp, %eax
	jae	.LBB29_41
# BB#4:                                 # %.lr.ph223
                                        #   in Loop: Header=BB29_3 Depth=2
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	96(%rbx), %rax
	movq	(%rax,%rcx,8), %r13
	movl	(%r13), %ecx
	movl	%esi, 32(%rsp)          # 4-byte Spill
	movslq	%esi, %r12
	leaq	(%rcx,%rcx,2), %rcx
	leaq	(%r15,%rcx,8), %r10
	movq	%r10, 8(%rsp)           # 8-byte Spill
	cmpq	$0, (%r10)
	jne	.LBB29_10
	jmp	.LBB29_12
	.p2align	4, 0x90
.LBB29_11:                              # %.loopexit._crit_edge
                                        #   in Loop: Header=BB29_10 Depth=3
	movq	96(%rbx), %rax
	cmpq	$0, (%r10)
	jne	.LBB29_10
.LBB29_12:                              #   in Loop: Header=BB29_3 Depth=2
	movq	(%rax,%r12,8), %rbp
	movl	(%rbp), %eax
	leaq	(%rax,%rax,2), %rcx
	cmpq	$0, (%r15,%rcx,8)
	jne	.LBB29_10
# BB#13:                                #   in Loop: Header=BB29_3 Depth=2
	movq	400(%rbp), %rax
	cmpq	%rax, 400(%r13)
	je	.LBB29_15
# BB#14:                                #   in Loop: Header=BB29_3 Depth=2
	cmpq	%r13, %rax
	jne	.LBB29_10
.LBB29_15:                              #   in Loop: Header=BB29_3 Depth=2
	movl	96(%r13), %eax
	cmpl	96(%rbp), %eax
	jne	.LBB29_10
# BB#16:                                # %.preheader203
                                        #   in Loop: Header=BB29_3 Depth=2
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	testl	%eax, %eax
	je	.LBB29_28
# BB#17:                                # %.lr.ph214
                                        #   in Loop: Header=BB29_3 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	16(%r15,%rax,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	104(%r13), %rax
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_18:                              #   Parent Loop BB29_1 Depth=1
                                        #     Parent Loop BB29_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	%r15, %r14
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rsi
	movq	(%rsp), %r15            # 8-byte Reload
	movq	%r15, %rdi
	callq	elem_symbol
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movq	104(%rbp), %rax
	movq	(%rax,%rbx,8), %rax
	movq	(%rax), %rsi
	movq	%r15, %rdi
	callq	elem_symbol
	cmpl	%eax, 36(%rsp)          # 4-byte Folded Reload
	jne	.LBB29_8
# BB#19:                                #   in Loop: Header=BB29_18 Depth=3
	movq	104(%r13), %rax
	movq	(%rax,%rbx,8), %rcx
	movq	8(%rcx), %rsi
	movq	104(%rbp), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	8(%rcx), %rcx
	cmpq	%rcx, %rsi
	movq	%r14, %r15
	je	.LBB29_27
# BB#20:                                #   in Loop: Header=BB29_18 Depth=3
	movl	(%rsi), %edx
	leaq	(%rdx,%rdx,2), %rdx
	movq	(%r15,%rdx,8), %rdx
	cmpq	%rcx, %rdx
	je	.LBB29_22
# BB#21:                                #   in Loop: Header=BB29_18 Depth=3
	movl	(%rcx), %edi
	leaq	(%rdi,%rdi,2), %rdi
	cmpq	%rsi, (%r15,%rdi,8)
	jne	.LBB29_7
.LBB29_22:                              #   in Loop: Header=BB29_18 Depth=3
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	movq	8(%rsp), %r10           # 8-byte Reload
	je	.LBB29_24
# BB#23:                                #   in Loop: Header=BB29_18 Depth=3
	movl	(%rcx), %edi
	leaq	(%rdi,%rdi,2), %rdi
	cmpq	(%r15,%rdi,8), %rsi
	jne	.LBB29_6
.LBB29_24:                              #   in Loop: Header=BB29_18 Depth=3
	movq	16(%r10), %rsi
	testq	%rsi, %rsi
	je	.LBB29_26
# BB#25:                                #   in Loop: Header=BB29_18 Depth=3
	cmpq	%rdx, %rsi
	jne	.LBB29_6
.LBB29_26:                              # %.critedge
                                        #   in Loop: Header=BB29_18 Depth=3
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	%rcx, (%rdx)
	movq	(%rax,%rbx,8), %rcx
	movq	8(%rcx), %rcx
	movq	%rcx, 16(%r10)
.LBB29_27:                              #   in Loop: Header=BB29_18 Depth=3
	incq	%rbx
	cmpl	96(%r13), %ebx
	jb	.LBB29_18
.LBB29_28:                              # %._crit_edge215
                                        #   in Loop: Header=BB29_3 Depth=2
	movl	176(%r13), %eax
	cmpl	176(%rbp), %eax
	jne	.LBB29_7
# BB#29:                                # %.preheader202
                                        #   in Loop: Header=BB29_3 Depth=2
	testl	%eax, %eax
	movq	8(%rsp), %r10           # 8-byte Reload
	je	.LBB29_5
# BB#30:                                # %.lr.ph217
                                        #   in Loop: Header=BB29_3 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	8(%r15,%rcx,8), %r8
	movq	184(%r13), %rdx
	movq	184(%rbp), %rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB29_31:                              #   Parent Loop BB29_1 Depth=1
                                        #     Parent Loop BB29_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx,%rdi,8), %rcx
	movq	16(%rcx), %rbx
	movq	(%rsi,%rdi,8), %rcx
	movq	16(%rcx), %rbp
	cmpq	%rbp, %rbx
	je	.LBB29_39
# BB#32:                                #   in Loop: Header=BB29_31 Depth=3
	movq	8(%rbx), %rcx
	cmpq	8(%rbp), %rcx
	jne	.LBB29_6
# BB#33:                                #   in Loop: Header=BB29_31 Depth=3
	movl	32(%rbx), %ecx
	cmpl	32(%rbp), %ecx
	je	.LBB29_39
# BB#34:                                #   in Loop: Header=BB29_31 Depth=3
	movq	(%r8), %rcx
	testq	%rcx, %rcx
	je	.LBB29_36
# BB#35:                                #   in Loop: Header=BB29_31 Depth=3
	cmpq	%rbp, %rcx
	jne	.LBB29_6
.LBB29_36:                              #   in Loop: Header=BB29_31 Depth=3
	movq	8(%r10), %rcx
	testq	%rcx, %rcx
	je	.LBB29_38
# BB#37:                                #   in Loop: Header=BB29_31 Depth=3
	cmpq	%rbx, %rcx
	jne	.LBB29_6
.LBB29_38:                              #   in Loop: Header=BB29_31 Depth=3
	movq	%rbp, (%r8)
	movq	(%rdx,%rdi,8), %rcx
	movq	16(%rcx), %rcx
	movq	%rcx, 8(%r10)
.LBB29_39:                              #   in Loop: Header=BB29_31 Depth=3
	incq	%rdi
	cmpl	%eax, %edi
	jb	.LBB29_31
.LBB29_5:                               # %._crit_edge218
                                        #   in Loop: Header=BB29_3 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%r15,%rax,8), %rax
	movq	%r13, (%rax)
	movl	$1, 20(%rsp)            # 4-byte Folded Spill
.LBB29_6:                               # %.loopexit
                                        #   in Loop: Header=BB29_3 Depth=2
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB29_10
.LBB29_7:                               #   in Loop: Header=BB29_3 Depth=2
	movq	(%rsp), %rbx            # 8-byte Reload
	jmp	.LBB29_9
.LBB29_8:                               #   in Loop: Header=BB29_3 Depth=2
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	%r14, %r15
.LBB29_9:                               # %.loopexit
                                        #   in Loop: Header=BB29_3 Depth=2
	movq	8(%rsp), %r10           # 8-byte Reload
	.p2align	4, 0x90
.LBB29_10:                              # %.loopexit
                                        #   Parent Loop BB29_1 Depth=1
                                        #     Parent Loop BB29_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%r12
	movl	88(%rbx), %ebp
	cmpl	%ebp, %r12d
	jb	.LBB29_11
# BB#40:                                #   in Loop: Header=BB29_3 Depth=2
	movl	32(%rsp), %esi          # 4-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
.LBB29_41:                              # %.loopexit205
                                        #   in Loop: Header=BB29_3 Depth=2
	incl	%esi
	cmpl	%ebp, %eax
	jb	.LBB29_3
# BB#42:                                # %.loopexit207
                                        #   in Loop: Header=BB29_1 Depth=1
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	jne	.LBB29_1
.LBB29_43:                              # %.preheader201
	testl	%ebp, %ebp
	je	.LBB29_71
# BB#44:                                # %.lr.ph212.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB29_45:                              # %.lr.ph212
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%rbx), %rax
	movq	(%rax,%r14,8), %rax
	movl	(%rax), %esi
	leaq	(%rsi,%rsi,2), %rbx
	movq	(%r15,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB29_52
# BB#46:                                # %.lr.ph212
                                        #   in Loop: Header=BB29_45 Depth=1
	cmpl	$3, verbose_level(%rip)
	jl	.LBB29_52
# BB#47:                                #   in Loop: Header=BB29_45 Depth=1
	leaq	(%r15,%rbx,8), %rbp
	movl	(%rax), %edx
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	printf
	movq	16(%r15,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB29_49
# BB#48:                                #   in Loop: Header=BB29_45 Depth=1
	movl	(%rax), %esi
	movq	(%rbp), %rax
	movl	(%rax), %eax
	leaq	(%rax,%rax,2), %rax
	movq	16(%r15,%rax,8), %rax
	movl	(%rax), %edx
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	printf
.LBB29_49:                              #   in Loop: Header=BB29_45 Depth=1
	cmpq	$0, 8(%r15,%rbx,8)
	je	.LBB29_51
# BB#50:                                #   in Loop: Header=BB29_45 Depth=1
	leaq	8(%r15,%rbx,8), %rbx
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rdi
	callq	print_rule
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbp), %rax
	movl	(%rax), %eax
	leaq	(%rax,%rax,2), %rax
	movq	8(%r15,%rax,8), %rdi
	callq	print_rule
	movl	$93, %edi
	callq	putchar
.LBB29_51:                              #   in Loop: Header=BB29_45 Depth=1
	movl	$10, %edi
	callq	putchar
	movq	(%rsp), %rax            # 8-byte Reload
	movl	88(%rax), %ebp
.LBB29_52:                              #   in Loop: Header=BB29_45 Depth=1
	incq	%r14
	cmpl	%ebp, %r14d
	movq	(%rsp), %rbx            # 8-byte Reload
	jb	.LBB29_45
# BB#53:                                # %.preheader200
	testl	%ebp, %ebp
	je	.LBB29_71
# BB#54:                                # %.lr.ph210
	movl	%ebp, %r8d
	movq	96(%rbx), %r9
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB29_55:                              # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rsi
	movl	(%rsi), %eax
	leaq	(%rax,%rax,2), %rdi
	movq	(%r15,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB29_64
# BB#56:                                #   in Loop: Header=BB29_55 Depth=1
	movq	16(%r15,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB29_64
# BB#57:                                #   in Loop: Header=BB29_55 Depth=1
	movl	(%rax), %eax
	leaq	(%rax,%rax,2), %rax
	movq	8(%r15,%rax,8), %rax
	testq	%rax, %rax
	je	.LBB29_60
# BB#58:                                #   in Loop: Header=BB29_55 Depth=1
	cmpl	$2, 32(%rax)
	jne	.LBB29_60
# BB#59:                                #   in Loop: Header=BB29_55 Depth=1
	movq	%rbx, 408(%rsi)
	movl	(%rbx), %eax
	leaq	(%rax,%rax,2), %rax
	movq	8(%r15,%rax,8), %rax
	movq	%rax, 416(%rsi)
	movq	8(%r15,%rdi,8), %rax
	jmp	.LBB29_63
	.p2align	4, 0x90
.LBB29_60:                              #   in Loop: Header=BB29_55 Depth=1
	movl	(%rbx), %eax
	leaq	(%rax,%rax,2), %rax
	movq	16(%r15,%rax,8), %rcx
	movl	(%rcx), %ecx
	leaq	(%rcx,%rcx,2), %rcx
	movq	8(%r15,%rcx,8), %rcx
	testq	%rcx, %rcx
	je	.LBB29_64
# BB#61:                                #   in Loop: Header=BB29_55 Depth=1
	cmpl	$2, 32(%rcx)
	jne	.LBB29_64
# BB#62:                                #   in Loop: Header=BB29_55 Depth=1
	movq	%rsi, 408(%rbx)
	movq	8(%r15,%rdi,8), %rcx
	movq	%rcx, 416(%rsi)
	movq	8(%r15,%rax,8), %rax
.LBB29_63:                              #   in Loop: Header=BB29_55 Depth=1
	movq	%rax, 424(%rsi)
.LBB29_64:                              #   in Loop: Header=BB29_55 Depth=1
	incq	%rdx
	cmpq	%r8, %rdx
	jb	.LBB29_55
# BB#65:                                # %.preheader
	testl	%ebp, %ebp
	movq	(%rsp), %rsi            # 8-byte Reload
	je	.LBB29_71
# BB#66:                                # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB29_67:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	96(%rsi), %rax
	movq	(%rax,%rbx,8), %rax
	movq	408(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB29_70
# BB#68:                                # %.lr.ph
                                        #   in Loop: Header=BB29_67 Depth=1
	movl	verbose_level(%rip), %edx
	testl	%edx, %edx
	je	.LBB29_70
# BB#69:                                #   in Loop: Header=BB29_67 Depth=1
	movl	(%rax), %esi
	movl	(%rcx), %edx
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rsp), %rsi            # 8-byte Reload
	movl	88(%rsi), %ebp
.LBB29_70:                              #   in Loop: Header=BB29_67 Depth=1
	incq	%rbx
	cmpl	%ebp, %ebx
	jb	.LBB29_67
.LBB29_71:                              # %._crit_edge
	movq	%r15, %rdi
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end29:
	.size	build_eq, .Lfunc_end29-build_eq
	.cfi_endproc

	.globl	new_D_Grammar
	.p2align	4, 0x90
	.type	new_D_Grammar,@function
new_D_Grammar:                          # @new_D_Grammar
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi272:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi273:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi274:
	.cfi_def_cfa_offset 32
.Lcfi275:
	.cfi_offset %rbx, -24
.Lcfi276:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$592, %edi              # imm = 0x250
	callq	malloc
	movq	%rax, %rbx
	leaq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$584, %edx              # imm = 0x248
	callq	memset
	movq	%r14, %rdi
	callq	strlen
	leaq	(%rax,%r14), %rsi
	movq	%r14, %rdi
	callq	dup_str
	movq	%rax, (%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end30:
	.size	new_D_Grammar, .Lfunc_end30-new_D_Grammar
	.cfi_endproc

	.globl	free_D_Grammar
	.p2align	4, 0x90
	.type	free_D_Grammar,@function
free_D_Grammar:                         # @free_D_Grammar
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rdi
	jmp	free                    # TAILCALL
.Lfunc_end31:
	.size	free_D_Grammar, .Lfunc_end31-free_D_Grammar
	.cfi_endproc

	.globl	parse_grammar
	.p2align	4, 0x90
	.type	parse_grammar,@function
parse_grammar:                          # @parse_grammar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi277:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi278:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi279:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi280:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi281:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi282:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi283:
	.cfi_def_cfa_offset 64
.Lcfi284:
	.cfi_offset %rbx, -56
.Lcfi285:
	.cfi_offset %r12, -48
.Lcfi286:
	.cfi_offset %r13, -40
.Lcfi287:
	.cfi_offset %r14, -32
.Lcfi288:
	.cfi_offset %r15, -24
.Lcfi289:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r12
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movl	$.L.str.44, %esi
	callq	fopen
	movl	$-1, %r15d
	testq	%rax, %rax
	je	.LBB32_5
# BB#1:
	movq	(%rbx), %rdi
	callq	sbuf_read
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB32_5
# BB#2:
	movq	%rbx, %rdi
	callq	initialize_productions
	movq	%r12, %rdi
	movl	%r14d, %esi
	callq	new_D_Parser
	movq	%rax, %rbp
	movq	%rbx, (%rbp)
	movq	(%rbx), %rax
	movq	%rax, 56(%rbp)
	movq	%r13, %rdi
	callq	strlen
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%eax, %edx
	callq	dparse
	testq	%rax, %rax
	je	.LBB32_5
# BB#3:
	xorl	%r15d, %r15d
	cmpl	$3, 8(%rbx)
	jb	.LBB32_5
# BB#4:
	movq	%rbx, %rdi
	callq	finish_productions
.LBB32_5:
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end32:
	.size	parse_grammar, .Lfunc_end32-parse_grammar
	.cfi_endproc

	.globl	build_grammar
	.p2align	4, 0x90
	.type	build_grammar,@function
build_grammar:                          # @build_grammar
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi290:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi291:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi292:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi293:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi294:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi295:
	.cfi_def_cfa_offset 56
	subq	$360, %rsp              # imm = 0x168
.Lcfi296:
	.cfi_def_cfa_offset 416
.Lcfi297:
	.cfi_offset %rbx, -56
.Lcfi298:
	.cfi_offset %r12, -48
.Lcfi299:
	.cfi_offset %r13, -40
.Lcfi300:
	.cfi_offset %r14, -32
.Lcfi301:
	.cfi_offset %r15, -24
.Lcfi302:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movl	$0, 588(%r15)
	movl	8(%r15), %r13d
	testl	%r13d, %r13d
	movq	%r15, 24(%rsp)          # 8-byte Spill
	leaq	48(%r15), %rax
	je	.LBB33_40
# BB#1:                                 # %.lr.ph.i.lr.ph.i
	xorl	%ecx, %ecx
	movq	%rax, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB33_2:                               # %.lr.ph.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_3 Depth 2
                                        #     Child Loop BB33_11 Depth 2
                                        #       Child Loop BB33_13 Depth 3
                                        #         Child Loop BB33_16 Depth 4
                                        #         Child Loop BB33_21 Depth 4
	movq	16(%r15), %r14
	movq	(%r14,%rcx,8), %rax
	movq	(%rax), %r12
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	8(%rax), %r15
	xorl	%ebp, %ebp
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB33_3:                               #   Parent Loop BB33_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rbp,8), %rbx
	cmpl	%r15d, 8(%rbx)
	jne	.LBB33_5
# BB#4:                                 #   in Loop: Header=BB33_3 Depth=2
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movq	%r15, %rdx
	callq	strncmp
	movq	16(%rsp), %rcx          # 8-byte Reload
	testl	%eax, %eax
	je	.LBB33_7
.LBB33_5:                               # %.thread.i.i
                                        #   in Loop: Header=BB33_3 Depth=2
	incq	%rbp
	cmpl	%r13d, %ebp
	jb	.LBB33_3
# BB#6:                                 #   in Loop: Header=BB33_2 Depth=1
	xorl	%ebx, %ebx
.LBB33_7:                               # %lookup_production.exit.i
                                        #   in Loop: Header=BB33_2 Depth=1
	cmpq	%rbx, 56(%rsp)          # 8-byte Folded Reload
	je	.LBB33_9
# BB#8:                                 #   in Loop: Header=BB33_2 Depth=1
	movl	$.L.str.65, %edi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	d_fail
	movq	16(%rsp), %rcx          # 8-byte Reload
.LBB33_9:                               #   in Loop: Header=BB33_2 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%ecx, 56(%rax)
	movq	%rax, %rcx
	cmpl	$0, 16(%rcx)
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	64(%rsp), %rax          # 8-byte Reload
	je	.LBB33_39
# BB#10:                                # %.lr.ph115.i
                                        #   in Loop: Header=BB33_2 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB33_11:                              #   Parent Loop BB33_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_13 Depth 3
                                        #         Child Loop BB33_16 Depth 4
                                        #         Child Loop BB33_21 Depth 4
	movq	24(%rcx), %rax
	movq	(%rax,%rdx,8), %rsi
	movl	588(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 588(%r15)
	movl	%eax, (%rsi)
	cmpl	$0, 32(%rsi)
	je	.LBB33_33
# BB#12:                                # %.lr.ph110.i
                                        #   in Loop: Header=BB33_11 Depth=2
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB33_13:                              #   Parent Loop BB33_2 Depth=1
                                        #     Parent Loop BB33_11 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB33_16 Depth 4
                                        #         Child Loop BB33_21 Depth 4
	movq	40(%rsi), %rax
	movq	(%rax,%rcx,8), %rdi
	movl	%ecx, 4(%rdi)
	movl	(%rdi), %eax
	cmpl	$2, %eax
	jne	.LBB33_27
# BB#14:                                #   in Loop: Header=BB33_13 Depth=3
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movslq	24(%rdi), %r12
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	16(%rdi), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	8(%r15), %r14d
	testl	%r14d, %r14d
	je	.LBB33_19
# BB#15:                                # %.lr.ph.i85.i
                                        #   in Loop: Header=BB33_13 Depth=3
	movq	16(%r15), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB33_16:                              #   Parent Loop BB33_2 Depth=1
                                        #     Parent Loop BB33_11 Depth=2
                                        #       Parent Loop BB33_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r13,%rbp,8), %rbx
	cmpl	%r12d, 8(%rbx)
	jne	.LBB33_18
# BB#17:                                #   in Loop: Header=BB33_16 Depth=4
	movq	(%rbx), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r12, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB33_29
.LBB33_18:                              # %.thread.i88.i
                                        #   in Loop: Header=BB33_16 Depth=4
	incq	%rbp
	cmpl	%r14d, %ebp
	jb	.LBB33_16
.LBB33_19:                              # %.loopexit98.i
                                        #   in Loop: Header=BB33_13 Depth=3
	movq	64(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ebp
	testl	%ebp, %ebp
	je	.LBB33_26
# BB#20:                                # %.lr.ph.i91.i
                                        #   in Loop: Header=BB33_13 Depth=3
	movq	56(%r15), %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB33_21:                              #   Parent Loop BB33_2 Depth=1
                                        #     Parent Loop BB33_11 Depth=2
                                        #       Parent Loop BB33_13 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r14,%r13,8), %r15
	cmpl	$3, (%r15)
	jne	.LBB33_24
# BB#22:                                #   in Loop: Header=BB33_21 Depth=4
	cmpl	%r12d, 32(%r15)
	jne	.LBB33_24
# BB#23:                                #   in Loop: Header=BB33_21 Depth=4
	movq	24(%r15), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r12, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB33_30
.LBB33_24:                              # %.thread.i94.i
                                        #   in Loop: Header=BB33_21 Depth=4
	incq	%r13
	cmpl	%ebp, %r13d
	jb	.LBB33_21
# BB#25:                                #   in Loop: Header=BB33_13 Depth=3
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB33_26:                              # %.loopexit.i
                                        #   in Loop: Header=BB33_13 Depth=3
	leaq	96(%rsp), %rbx
	movq	%rbx, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%r12, %rdx
	callq	strncpy
	cmpl	$255, %r12d
	movl	$255, %eax
	cmovgel	%eax, %r12d
	movslq	%r12d, %rax
	movb	$0, 96(%rsp,%rax)
	movl	$.L.str.66, %edi
	xorl	%eax, %eax
	movq	%rbx, %rsi
	callq	d_fail
	movq	40(%rsp), %rdi          # 8-byte Reload
	movl	(%rdi), %eax
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB33_27:                              # %thread-pre-split.i
                                        #   in Loop: Header=BB33_13 Depth=3
	cmpl	$1, %eax
	jne	.LBB33_32
# BB#28:                                # %thread-pre-split.i._crit_edge
                                        #   in Loop: Header=BB33_13 Depth=3
	movq	16(%rdi), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	jmp	.LBB33_32
.LBB33_29:                              # %thread-pre-split.thread135.i
                                        #   in Loop: Header=BB33_13 Depth=3
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
	movq	%rbx, 16(%rax)
	jmp	.LBB33_31
.LBB33_30:                              # %thread-pre-split.thread.i
                                        #   in Loop: Header=BB33_13 Depth=3
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$1, (%rax)
	movq	%r15, 16(%rax)
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	24(%rsp), %r15          # 8-byte Reload
.LBB33_31:                              #   in Loop: Header=BB33_13 Depth=3
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	.p2align	4, 0x90
.LBB33_32:                              #   in Loop: Header=BB33_13 Depth=3
	incq	%rcx
	movl	32(%rsi), %eax
	cmpl	%eax, %ecx
	jb	.LBB33_13
	jmp	.LBB33_34
	.p2align	4, 0x90
.LBB33_33:                              #   in Loop: Header=BB33_11 Depth=2
	xorl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
.LBB33_34:                              # %._crit_edge111.i
                                        #   in Loop: Header=BB33_11 Depth=2
	movq	72(%rsi), %rcx
	movl	%eax, 4(%rcx)
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB33_38
# BB#35:                                # %._crit_edge111.i
                                        #   in Loop: Header=BB33_11 Depth=2
	movl	248(%r15), %eax
	testl	%eax, %eax
	je	.LBB33_38
# BB#36:                                #   in Loop: Header=BB33_11 Depth=2
	movl	28(%rsi), %eax
	testl	%eax, %eax
	je	.LBB33_38
# BB#37:                                #   in Loop: Header=BB33_11 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 12(%rcx)
	movl	24(%rsi), %eax
	movl	%eax, 16(%rcx)
.LBB33_38:                              #   in Loop: Header=BB33_11 Depth=2
	incq	%rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	cmpl	16(%rcx), %edx
	movq	64(%rsp), %rax          # 8-byte Reload
	jb	.LBB33_11
.LBB33_39:                              # %._crit_edge116.i
                                        #   in Loop: Header=BB33_2 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	incq	%rcx
	movl	8(%r15), %r13d
	cmpl	%r13d, %ecx
	jb	.LBB33_2
	jmp	.LBB33_41
.LBB33_40:                              # %..preheader_crit_edge.i
	xorl	%r13d, %r13d
.LBB33_41:                              # %.preheader.i
	movl	(%rax), %eax
	testq	%rax, %rax
	je	.LBB33_46
# BB#42:                                # %.lr.ph.i
	movq	56(%r15), %rcx
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	xorl	%edx, %edx
	andq	$7, %rdi
	je	.LBB33_44
	.p2align	4, 0x90
.LBB33_43:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rbp
	movl	%edx, 4(%rbp)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB33_43
.LBB33_44:                              # %.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB33_46
	.p2align	4, 0x90
.LBB33_45:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rsi
	movl	%edx, 4(%rsi)
	movq	8(%rcx,%rdx,8), %rsi
	leal	1(%rdx), %edi
	movl	%edi, 4(%rsi)
	movq	16(%rcx,%rdx,8), %rsi
	leal	2(%rdx), %edi
	movl	%edi, 4(%rsi)
	movq	24(%rcx,%rdx,8), %rsi
	leal	3(%rdx), %edi
	movl	%edi, 4(%rsi)
	movq	32(%rcx,%rdx,8), %rsi
	leal	4(%rdx), %edi
	movl	%edi, 4(%rsi)
	movq	40(%rcx,%rdx,8), %rsi
	leal	5(%rdx), %edi
	movl	%edi, 4(%rsi)
	movq	48(%rcx,%rdx,8), %rsi
	leal	6(%rdx), %edi
	movl	%edi, 4(%rsi)
	movq	56(%rcx,%rdx,8), %rsi
	leal	7(%rdx), %edi
	movl	%edi, 4(%rsi)
	addq	$8, %rdx
	cmpq	%rax, %rdx
	jne	.LBB33_45
.LBB33_46:                              # %._crit_edge.i
	testl	%r13d, %r13d
	je	.LBB33_82
# BB#47:                                # %.preheader55.lr.ph.i.i
	movl	%r13d, %r9d
	movq	16(%r15), %r10
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB33_48:                              # %.preheader55.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_50 Depth 2
	movq	(%r10,%rdx,8), %rsi
	movl	16(%rsi), %edi
	testl	%edi, %edi
	je	.LBB33_53
# BB#49:                                # %.lr.ph71.preheader.i.i
                                        #   in Loop: Header=BB33_48 Depth=1
	movq	24(%rsi), %rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB33_50:                              # %.lr.ph71.i.i
                                        #   Parent Loop BB33_48 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax,%rbp,8), %rbx
	cmpl	$0, 32(%rbx)
	je	.LBB33_52
# BB#51:                                #   in Loop: Header=BB33_50 Depth=2
	incq	%rbp
	cmpl	%edi, %ebp
	jb	.LBB33_50
	jmp	.LBB33_53
	.p2align	4, 0x90
.LBB33_52:                              #   in Loop: Header=BB33_48 Depth=1
	movq	%rbx, 64(%rsi)
.LBB33_53:                              # %.loopexit56.i.i
                                        #   in Loop: Header=BB33_48 Depth=1
	incq	%rdx
	cmpq	%r9, %rdx
	jne	.LBB33_48
	.p2align	4, 0x90
.LBB33_54:                              # %.preheader52.us.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_62 Depth 2
                                        #       Child Loop BB33_58 Depth 3
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	jmp	.LBB33_62
	.p2align	4, 0x90
.LBB33_55:                              #   in Loop: Header=BB33_62 Depth=2
	cmpl	$0, 16(%rdi)
	je	.LBB33_63
# BB#56:                                # %.preheader.us.i.i
                                        #   in Loop: Header=BB33_62 Depth=2
	movq	24(%rdi), %rax
	movq	(%rax), %rbp
	movl	32(%rbp), %ebx
	testl	%ebx, %ebx
	je	.LBB33_61
# BB#57:                                # %.lr.ph.us.preheader.i.i
                                        #   in Loop: Header=BB33_62 Depth=2
	movq	40(%rbp), %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB33_58:                              # %.lr.ph.us.i.i
                                        #   Parent Loop BB33_54 Depth=1
                                        #     Parent Loop BB33_62 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rsi,%rax,8), %rcx
	cmpl	$0, (%rcx)
	jne	.LBB33_63
# BB#59:                                #   in Loop: Header=BB33_58 Depth=3
	movq	16(%rcx), %rcx
	cmpq	$0, 64(%rcx)
	je	.LBB33_63
# BB#60:                                #   in Loop: Header=BB33_58 Depth=3
	incq	%rax
	cmpl	%ebx, %eax
	jb	.LBB33_58
.LBB33_61:                              # %._crit_edge.us.i.i
                                        #   in Loop: Header=BB33_62 Depth=2
	movq	%rbp, 64(%rdi)
	movl	$1, %r8d
	jmp	.LBB33_63
	.p2align	4, 0x90
.LBB33_62:                              #   Parent Loop BB33_54 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_58 Depth 3
	movq	(%r10,%rdx,8), %rdi
	cmpq	$0, 64(%rdi)
	je	.LBB33_55
.LBB33_63:                              # %.loopexit.us.i.i
                                        #   in Loop: Header=BB33_62 Depth=2
	incq	%rdx
	cmpq	%r9, %rdx
	jne	.LBB33_62
# BB#64:                                # %..loopexit53_crit_edge.us.i.i
                                        #   in Loop: Header=BB33_54 Depth=1
	testl	%r8d, %r8d
	jne	.LBB33_54
# BB#65:                                # %resolve_grammar.exit
	testl	%r13d, %r13d
	je	.LBB33_82
# BB#66:                                # %.lr.ph51.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB33_67:                              # =>This Inner Loop Header: Depth=1
	movq	16(%r15), %rax
	movq	(%rax,%rbx,8), %rsi
	testb	$1, 60(%rsi)
	je	.LBB33_69
# BB#68:                                #   in Loop: Header=BB33_67 Depth=1
	movq	%r15, %rdi
	callq	convert_regex_production_one
	movl	8(%r15), %r13d
.LBB33_69:                              #   in Loop: Header=BB33_67 Depth=1
	incq	%rbx
	cmpl	%r13d, %ebx
	jb	.LBB33_67
# BB#70:                                # %.preheader.i16
	testl	%r13d, %r13d
	je	.LBB33_82
# BB#71:                                # %.lr.ph47.i
	movl	%r13d, %r8d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB33_72:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_74 Depth 2
                                        #       Child Loop BB33_76 Depth 3
	movq	16(%r15), %rax
	movq	(%rax,%r9,8), %r10
	movl	16(%r10), %r11d
	testq	%r11, %r11
	je	.LBB33_81
# BB#73:                                # %.lr.ph44.i
                                        #   in Loop: Header=BB33_72 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB33_74:                              #   Parent Loop BB33_72 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_76 Depth 3
	movq	24(%r10), %rax
	movq	(%rax,%rdi,8), %rbp
	movl	32(%rbp), %ebx
	testq	%rbx, %rbx
	je	.LBB33_80
# BB#75:                                # %.lr.ph.i18
                                        #   in Loop: Header=BB33_74 Depth=2
	movq	40(%rbp), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB33_76:                              #   Parent Loop BB33_72 Depth=1
                                        #     Parent Loop BB33_74 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax,%rcx,8), %rdx
	cmpl	$0, (%rdx)
	jne	.LBB33_79
# BB#77:                                #   in Loop: Header=BB33_76 Depth=3
	movq	16(%rdx), %rsi
	movq	216(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB33_79
# BB#78:                                #   in Loop: Header=BB33_76 Depth=3
	movq	%rsi, 16(%rdx)
	movq	40(%rbp), %rax
	movq	(%rax,%rcx,8), %rdx
	movl	$1, (%rdx)
.LBB33_79:                              #   in Loop: Header=BB33_76 Depth=3
	incq	%rcx
	cmpq	%rcx, %rbx
	jne	.LBB33_76
.LBB33_80:                              # %._crit_edge.i21
                                        #   in Loop: Header=BB33_74 Depth=2
	incq	%rdi
	cmpq	%r11, %rdi
	jne	.LBB33_74
.LBB33_81:                              # %._crit_edge45.i
                                        #   in Loop: Header=BB33_72 Depth=1
	incq	%r9
	cmpq	%r8, %r9
	jne	.LBB33_72
.LBB33_82:                              # %convert_regex_productions.exit
	cmpl	$0, 264(%r15)
	je	.LBB33_90
# BB#83:
	movq	16(%r15), %rax
	movq	(%rax), %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	$0, 8(%rbx)
	movl	$16, %edi
	callq	malloc
	movq	%rbx, (%rax)
	movl	$0, 8(%rax)
	movl	160(%r15), %ecx
	movl	%ecx, 12(%rax)
	movq	168(%r15), %rdx
	leaq	176(%r15), %rsi
	testq	%rdx, %rdx
	je	.LBB33_86
# BB#84:
	leaq	160(%r15), %rdi
	cmpq	%rsi, %rdx
	je	.LBB33_87
# BB#85:
	testb	$7, %cl
	jne	.LBB33_88
	jmp	.LBB33_89
.LBB33_86:
	movq	%rsi, 168(%r15)
	leal	1(%rcx), %edx
	movl	%edx, 160(%r15)
	movq	%rax, 176(%r15,%rcx,8)
	cmpl	$0, 268(%r15)
	jne	.LBB33_91
	jmp	.LBB33_99
.LBB33_87:
	cmpl	$2, %ecx
	ja	.LBB33_89
.LBB33_88:
	leal	1(%rcx), %esi
	movl	%esi, (%rdi)
	movq	%rax, (%rdx,%rcx,8)
	cmpl	$0, 268(%r15)
	jne	.LBB33_91
	jmp	.LBB33_99
.LBB33_89:
	movq	%rax, %rsi
	callq	vec_add_internal
.LBB33_90:                              # %new_declaration.exit.i
	cmpl	$0, 268(%r15)
	je	.LBB33_99
.LBB33_91:
	movq	16(%r15), %rax
	movq	(%rax), %rbp
	movl	$32, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	%rbp, 16(%rbx)
	movq	$0, 8(%rbx)
	movl	$16, %edi
	callq	malloc
	movq	%rbx, (%rax)
	movl	$1, 8(%rax)
	movl	160(%r15), %ecx
	movl	%ecx, 12(%rax)
	movq	168(%r15), %rdx
	leaq	176(%r15), %rsi
	testq	%rdx, %rdx
	je	.LBB33_95
# BB#92:
	leaq	160(%r15), %rdi
	cmpq	%rsi, %rdx
	je	.LBB33_96
# BB#93:
	testb	$7, %cl
	je	.LBB33_98
# BB#94:
	leal	1(%rcx), %esi
	movl	%esi, (%rdi)
	movq	%rax, (%rdx,%rcx,8)
	jmp	.LBB33_99
.LBB33_95:
	movq	%rsi, 168(%r15)
	jmp	.LBB33_97
.LBB33_96:
	cmpl	$2, %ecx
	ja	.LBB33_98
.LBB33_97:
	leal	1(%rcx), %edx
	movl	%edx, 160(%r15)
	movq	%rax, 176(%r15,%rcx,8)
	jmp	.LBB33_99
.LBB33_98:
	movq	%rax, %rsi
	callq	vec_add_internal
.LBB33_99:                              # %new_declaration.exit70.preheader.i
	movl	160(%r15), %ecx
	leaq	16(%r15), %rax
	testl	%ecx, %ecx
	je	.LBB33_116
# BB#100:                               # %.lr.ph89.i
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB33_101:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_104 Depth 2
	movq	168(%r15), %rax
	movq	(%rax,%r13,8), %rax
	movq	(%rax), %rax
	cmpl	$2, (%rax)
	jne	.LBB33_110
# BB#102:                               #   in Loop: Header=BB33_101 Depth=1
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	16(%rax), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	8(%r15), %ebp
	testl	%ebp, %ebp
	je	.LBB33_107
# BB#103:                               # %.lr.ph.i.i22
                                        #   in Loop: Header=BB33_101 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	24(%rax), %rbx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB33_104:                             #   Parent Loop BB33_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%r12,8), %r14
	cmpl	%ebx, 8(%r14)
	jne	.LBB33_106
# BB#105:                               #   in Loop: Header=BB33_104 Depth=2
	movq	(%r14), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	strncmp
	testl	%eax, %eax
	je	.LBB33_108
.LBB33_106:                             # %.thread.i.i25
                                        #   in Loop: Header=BB33_104 Depth=2
	incq	%r12
	cmpl	%ebp, %r12d
	jb	.LBB33_104
.LBB33_107:                             # %.loopexit.i26
                                        #   in Loop: Header=BB33_101 Depth=1
	movl	$.L.str.73, %edi
	xorl	%eax, %eax
	movq	(%rsp), %rsi            # 8-byte Reload
	callq	d_fail
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	160(%r15), %ecx
	xorl	%r14d, %r14d
	jmp	.LBB33_109
.LBB33_108:                             #   in Loop: Header=BB33_101 Depth=1
	movq	24(%rsp), %r15          # 8-byte Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
.LBB33_109:                             # %lookup_production.exit.i28
                                        #   in Loop: Header=BB33_101 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$0, (%rax)
	movq	%r14, 16(%rax)
.LBB33_110:                             # %new_declaration.exit70.i
                                        #   in Loop: Header=BB33_101 Depth=1
	incq	%r13
	cmpl	%ecx, %r13d
	jb	.LBB33_101
# BB#111:                               # %.preheader75.i
	testl	%ecx, %ecx
	je	.LBB33_117
# BB#112:                               # %.lr.ph87.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB33_113:                             # %.lr.ph87.i
                                        # =>This Inner Loop Header: Depth=1
	movq	168(%r15), %rax
	movq	(%rax,%rbx,8), %rdx
	cmpl	$2, 8(%rdx)
	ja	.LBB33_115
# BB#114:                               #   in Loop: Header=BB33_113 Depth=1
	movq	(%rdx), %rax
	movq	16(%rax), %rdi
	movq	%rdi, %rsi
	callq	set_declaration_group
	movl	160(%r15), %ecx
.LBB33_115:                             #   in Loop: Header=BB33_113 Depth=1
	incq	%rbx
	cmpl	%ecx, %ebx
	jb	.LBB33_113
	jmp	.LBB33_117
.LBB33_116:                             # %new_declaration.exit70.preheader.i..preheader.i29_crit_edge
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB33_117:                             # %.preheader.i29
	movl	8(%r15), %eax
	testl	%eax, %eax
	je	.LBB33_179
# BB#118:                               # %.lr.ph84.i.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB33_119:                             # %.lr.ph84.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_121 Depth 2
                                        #       Child Loop BB33_123 Depth 3
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	(%rcx), %rcx
	movq	(%rcx,%r8,8), %rcx
	movl	16(%rcx), %edi
	testl	%edi, %edi
	je	.LBB33_134
# BB#120:                               # %.lr.ph81.i
                                        #   in Loop: Header=BB33_119 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB33_121:                             #   Parent Loop BB33_119 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_123 Depth 3
	movq	24(%rcx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	cmpl	$0, 32(%rsi)
	je	.LBB33_132
# BB#122:                               # %.lr.ph.i30
                                        #   in Loop: Header=BB33_121 Depth=2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB33_123:                             #   Parent Loop BB33_119 Depth=1
                                        #     Parent Loop BB33_121 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	40(%rsi), %rbp
	movq	(%rbp,%rdi,8), %rbp
	cmpl	$1, (%rbp)
	jne	.LBB33_130
# BB#124:                               #   in Loop: Header=BB33_123 Depth=3
	cmpq	$0, 80(%rcx)
	je	.LBB33_127
# BB#125:                               # %.thread.i
                                        #   in Loop: Header=BB33_123 Depth=3
	cmpq	$0, 88(%rcx)
	je	.LBB33_128
# BB#126:                               #   in Loop: Header=BB33_123 Depth=3
	movq	144(%rcx), %rbx
	movq	152(%rcx), %rax
	movl	12(%rax), %eax
	cmpl	12(%rbx), %eax
	setb	%al
	movq	16(%rbp), %rbp
	movzbl	36(%rbp), %ebx
	andb	$-8, %bl
	orb	%al, %bl
	jmp	.LBB33_129
.LBB33_127:                             #   in Loop: Header=BB33_123 Depth=3
	movq	16(%rbp), %rbp
	andb	$-8, 36(%rbp)
	jmp	.LBB33_130
.LBB33_128:                             #   in Loop: Header=BB33_123 Depth=3
	movq	16(%rbp), %rbp
	movzbl	36(%rbp), %ebx
	andb	$-8, %bl
	orb	$1, %bl
.LBB33_129:                             #   in Loop: Header=BB33_123 Depth=3
	movb	%bl, 36(%rbp)
.LBB33_130:                             #   in Loop: Header=BB33_123 Depth=3
	incq	%rdi
	movl	32(%rsi), %eax
	cmpq	%rax, %rdi
	jb	.LBB33_123
# BB#131:                               # %._crit_edge.i33.loopexit
                                        #   in Loop: Header=BB33_121 Depth=2
	movl	16(%rcx), %edi
.LBB33_132:                             # %._crit_edge.i33
                                        #   in Loop: Header=BB33_121 Depth=2
	incq	%rdx
	movl	%edi, %eax
	cmpq	%rax, %rdx
	jb	.LBB33_121
# BB#133:                               # %._crit_edge82.i.loopexit
                                        #   in Loop: Header=BB33_119 Depth=1
	movl	8(%r15), %eax
.LBB33_134:                             # %._crit_edge82.i
                                        #   in Loop: Header=BB33_119 Depth=1
	incq	%r8
	movl	%eax, %r9d
	cmpq	%r9, %r8
	jb	.LBB33_119
# BB#135:                               # %propogate_declarations.exit
	testl	%eax, %eax
	je	.LBB33_179
# BB#136:                               # %.lr.ph38.i
	movl	%eax, 84(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%r9, 48(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB33_137:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_139 Depth 2
                                        #       Child Loop BB33_141 Depth 3
                                        #         Child Loop BB33_144 Depth 4
	movq	%rax, %rcx
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movl	16(%rax), %eax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB33_156
# BB#138:                               # %.lr.ph34.i
                                        #   in Loop: Header=BB33_137 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB33_139:                             #   Parent Loop BB33_137 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_141 Depth 3
                                        #         Child Loop BB33_144 Depth 4
	movq	%rax, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rax
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	(%rax,%rcx,8), %rcx
	movl	32(%rcx), %edx
	testq	%rdx, %rdx
	je	.LBB33_155
# BB#140:                               # %.lr.ph.i34
                                        #   in Loop: Header=BB33_139 Depth=2
	xorl	%esi, %esi
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB33_141:                             #   Parent Loop BB33_137 Depth=1
                                        #     Parent Loop BB33_139 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB33_144 Depth 4
	movq	40(%rcx), %rax
	movq	(%rax,%rsi,8), %rdi
	cmpl	$1, (%rdi)
	jne	.LBB33_154
# BB#142:                               #   in Loop: Header=BB33_141 Depth=3
	movq	%r15, %rax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	16(%rdi), %r15
	movl	48(%rax), %r12d
	testl	%r12d, %r12d
	movq	%rsi, (%rsp)            # 8-byte Spill
	je	.LBB33_152
# BB#143:                               # %.lr.ph.i.i36
                                        #   in Loop: Header=BB33_141 Depth=3
	movl	(%r15), %r14d
	movq	56(%rax), %r13
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB33_144:                             #   Parent Loop BB33_137 Depth=1
                                        #     Parent Loop BB33_139 Depth=2
                                        #       Parent Loop BB33_141 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%r13,%rbp,8), %rbx
	cmpl	(%rbx), %r14d
	jne	.LBB33_151
# BB#145:                               #   in Loop: Header=BB33_144 Depth=4
	movslq	32(%r15), %rdx
	cmpl	32(%rbx), %edx
	jne	.LBB33_151
# BB#146:                               #   in Loop: Header=BB33_144 Depth=4
	movl	8(%r15), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB33_151
# BB#147:                               #   in Loop: Header=BB33_144 Depth=4
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$0, 248(%rax)
	je	.LBB33_150
# BB#148:                               #   in Loop: Header=BB33_144 Depth=4
	movl	12(%r15), %eax
	cmpl	12(%rbx), %eax
	jne	.LBB33_151
# BB#149:                               #   in Loop: Header=BB33_144 Depth=4
	movl	16(%r15), %eax
	cmpl	16(%rbx), %eax
	jne	.LBB33_151
.LBB33_150:                             #   in Loop: Header=BB33_144 Depth=4
	movq	24(%r15), %rdi
	movq	24(%rbx), %rsi
	callq	strncmp
	testl	%eax, %eax
	je	.LBB33_153
	.p2align	4, 0x90
.LBB33_151:                             #   in Loop: Header=BB33_144 Depth=4
	incq	%rbp
	cmpl	%r12d, %ebp
	jb	.LBB33_144
.LBB33_152:                             #   in Loop: Header=BB33_141 Depth=3
	movq	%r15, %rbx
.LBB33_153:                             # %unique_term.exit.i
                                        #   in Loop: Header=BB33_141 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rbx, 16(%rax)
	movq	24(%rsp), %r15          # 8-byte Reload
	movq	48(%rsp), %r9           # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rsp), %rsi            # 8-byte Reload
.LBB33_154:                             #   in Loop: Header=BB33_141 Depth=3
	incq	%rsi
	cmpq	%rdx, %rsi
	jne	.LBB33_141
.LBB33_155:                             # %._crit_edge.i40
                                        #   in Loop: Header=BB33_139 Depth=2
	movq	72(%rsp), %rax          # 8-byte Reload
	incq	%rax
	cmpq	56(%rsp), %rax          # 8-byte Folded Reload
	jne	.LBB33_139
.LBB33_156:                             # %._crit_edge35.i
                                        #   in Loop: Header=BB33_137 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	incq	%rax
	cmpq	%r9, %rax
	jne	.LBB33_137
# BB#157:                               # %merge_identical_terminals.exit
	leaq	260(%r15), %rdi
	cmpl	$0, 84(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movq	(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	je	.LBB33_180
# BB#158:                               # %.lr.ph79.i
	xorl	%r12d, %r12d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	jmp	.LBB33_164
.LBB33_159:                             #   in Loop: Header=BB33_164 Depth=1
	movl	160(%r15), %eax
	testl	%eax, %eax
	je	.LBB33_177
# BB#160:                               # %.lr.ph.i.i42
                                        #   in Loop: Header=BB33_164 Depth=1
	movq	168(%r15), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB33_161:                             #   Parent Loop BB33_164 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%rdx,8), %rsi
	cmpl	$5, 8(%rsi)
	jne	.LBB33_163
# BB#162:                               #   in Loop: Header=BB33_161 Depth=2
	movq	(%rsi), %rsi
	movq	16(%rsi), %rsi
	movl	56(%rsi), %esi
	cmpq	%r12, %rsi
	je	.LBB33_166
.LBB33_163:                             #   in Loop: Header=BB33_161 Depth=2
	incq	%rdx
	cmpl	%eax, %edx
	jb	.LBB33_161
	jmp	.LBB33_177
	.p2align	4, 0x90
.LBB33_164:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_161 Depth 2
                                        #     Child Loop BB33_167 Depth 2
                                        #       Child Loop BB33_169 Depth 3
                                        #         Child Loop BB33_171 Depth 4
	movq	(%r13,%r12,8), %rax
	testb	$28, 60(%rax)
	jne	.LBB33_177
# BB#165:                               #   in Loop: Header=BB33_164 Depth=1
	cmpl	$0, (%rdi)
	je	.LBB33_159
.LBB33_166:                             # %.preheader.i45.preheader
                                        #   in Loop: Header=BB33_164 Depth=1
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB33_167:                             # %.preheader.i45
                                        #   Parent Loop BB33_164 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_169 Depth 3
                                        #         Child Loop BB33_171 Depth 4
	movq	(%r13,%r8,8), %rcx
	cmpl	$0, 16(%rcx)
	je	.LBB33_175
# BB#168:                               # %.lr.ph73.i.preheader
                                        #   in Loop: Header=BB33_167 Depth=2
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB33_169:                             # %.lr.ph73.i
                                        #   Parent Loop BB33_164 Depth=1
                                        #     Parent Loop BB33_167 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB33_171 Depth 4
	movq	24(%rcx), %rax
	movq	(%rax,%rdx,8), %rdi
	movl	32(%rdi), %esi
	testl	%esi, %esi
	je	.LBB33_174
# BB#170:                               # %.lr.ph.i46
                                        #   in Loop: Header=BB33_169 Depth=3
	movq	40(%rdi), %rbx
	movq	(%r13,%r12,8), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB33_171:                             #   Parent Loop BB33_164 Depth=1
                                        #     Parent Loop BB33_167 Depth=2
                                        #       Parent Loop BB33_169 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rbx,%rbp,8), %rax
	cmpq	%rdi, 16(%rax)
	je	.LBB33_173
# BB#172:                               #   in Loop: Header=BB33_171 Depth=4
	incq	%rbp
	cmpl	%esi, %ebp
	jb	.LBB33_171
	jmp	.LBB33_174
	.p2align	4, 0x90
.LBB33_173:                             #   in Loop: Header=BB33_169 Depth=3
	movq	%rax, 208(%rdi)
	movq	(%r13,%r8,8), %rcx
.LBB33_174:                             # %.loopexit.i49
                                        #   in Loop: Header=BB33_169 Depth=3
	incq	%rdx
	movl	16(%rcx), %eax
	cmpq	%rax, %rdx
	jb	.LBB33_169
.LBB33_175:                             # %state_for_declaration.exit.i
                                        #   in Loop: Header=BB33_167 Depth=2
	incq	%r8
	cmpq	%r9, %r8
	jne	.LBB33_167
# BB#176:                               # %state_for_declaration.exit._crit_edge.i
                                        #   in Loop: Header=BB33_164 Depth=1
	movq	(%r13,%r12,8), %r14
	movl	$168, %edi
	callq	malloc
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movups	%xmm0, 112(%rbx)
	movups	%xmm0, 96(%rbx)
	movups	%xmm0, 80(%rbx)
	movups	%xmm0, 64(%rbx)
	movups	%xmm0, 48(%rbx)
	movups	%xmm0, 32(%rbx)
	movups	%xmm0, 16(%rbx)
	movups	%xmm0, (%rbx)
	movq	$0, 160(%rbx)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 8(%rbx)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%rbx)
	movl	$3, (%rax)
	movq	%rbx, 8(%rax)
	movl	576(%r15), %eax
	movl	%eax, 152(%rbx)
	movl	$32, %edi
	callq	malloc
	movq	48(%rsp), %r9           # 8-byte Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r14, 16(%rax)
	movq	%rbx, 8(%rax)
	movq	(%r13,%r12,8), %rcx
	movq	%rax, 208(%rcx)
	movl	588(%r15), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 588(%r15)
	movq	(%r13,%r12,8), %rcx
	movq	208(%rcx), %rcx
	movq	8(%rcx), %rcx
	movl	%eax, (%rcx)
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB33_177:                             # %state_for_declaration.exit.thread.i
                                        #   in Loop: Header=BB33_164 Depth=1
	incq	%r12
	cmpq	%r9, %r12
	jne	.LBB33_164
# BB#178:
	movl	84(%rsp), %eax          # 4-byte Reload
	jmp	.LBB33_181
.LBB33_179:                             # %merge_identical_terminals.exit.thread
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %r13
	movq	(%r13), %rax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	260(%r15), %rdi
.LBB33_180:                             # %._crit_edge80.i
	xorl	%eax, %eax
.LBB33_181:                             # %._crit_edge80.i
	cmpl	$0, (%rdi)
	movl	%eax, %ebp
	je	.LBB33_187
.LBB33_182:                             # %make_elems_for_productions.exit
	testl	%eax, %eax
	movq	24(%rsp), %r14          # 8-byte Reload
	je	.LBB33_195
# BB#183:                               # %.lr.ph.i.i53.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB33_184:                             # %.lr.ph.i.i53
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rax,8), %rcx
	cmpl	$1, 8(%rcx)
	jne	.LBB33_186
# BB#185:                               #   in Loop: Header=BB33_184 Depth=1
	movq	(%rcx), %rdx
	cmpb	$95, (%rdx)
	je	.LBB33_193
.LBB33_186:                             # %.thread.i.i56
                                        #   in Loop: Header=BB33_184 Depth=1
	incq	%rax
	cmpl	%ebp, %eax
	jb	.LBB33_184
	jmp	.LBB33_195
.LBB33_187:
	testl	%eax, %eax
	je	.LBB33_182
# BB#188:
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	256(%rax), %eax
	testl	%eax, %eax
	movl	%ebp, %eax
	je	.LBB33_182
# BB#189:                               # %.lr.ph.i64.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB33_190:                             # %.lr.ph.i64.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %r15
	cmpl	$10, 8(%r15)
	jne	.LBB33_192
# BB#191:                               #   in Loop: Header=BB33_190 Depth=1
	movq	(%r15), %rdi
	movl	$.L.str.75, %esi
	movl	$10, %edx
	callq	strncmp
	testl	%eax, %eax
	movl	%ebp, %eax
	je	.LBB33_253
.LBB33_192:                             # %.thread.i.i51
                                        #   in Loop: Header=BB33_190 Depth=1
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB33_190
	jmp	.LBB33_182
.LBB33_193:                             # %lookup_production.exit.i57
	cmpl	$2, 16(%rcx)
	jb	.LBB33_195
# BB#194:
	movl	$.L.str.77, %edi
	xorl	%eax, %eax
	callq	d_fail
.LBB33_195:                             # %check_default_actions.exit
	movq	%r14, %rdi
	callq	build_LR_tables
	cmpl	$0, 88(%r14)
	je	.LBB33_198
# BB#196:                               # %.lr.ph31.i
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB33_197:                             # =>This Inner Loop Header: Depth=1
	movq	96(%r14), %rcx
	movq	(%rcx,%rax,8), %rcx
	orb	$24, 376(%rcx)
	incq	%rax
	movl	88(%r14), %ecx
	cmpq	%rcx, %rax
	jb	.LBB33_197
	jmp	.LBB33_199
.LBB33_198:
	xorl	%ecx, %ecx
.LBB33_199:                             # %.preheader25.i
	movl	160(%r14), %eax
	testl	%eax, %eax
	je	.LBB33_243
# BB#200:                               # %.lr.ph28.i
	xorl	%edi, %edi
	movl	%ecx, %edx
	.p2align	4, 0x90
.LBB33_201:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB33_224 Depth 2
                                        #       Child Loop BB33_208 Depth 3
                                        #         Child Loop BB33_210 Depth 4
                                        #       Child Loop BB33_228 Depth 3
	movq	168(%r14), %rsi
	movq	(%rsi,%rdi,8), %r8
	movl	8(%r8), %esi
	cmpl	$2, %esi
	ja	.LBB33_242
# BB#202:                               #   in Loop: Header=BB33_201 Depth=1
	testl	%edx, %edx
	je	.LBB33_241
# BB#203:                               # %.lr.ph6.i.i.preheader
                                        #   in Loop: Header=BB33_201 Depth=1
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	movq	%r8, (%rsp)             # 8-byte Spill
	jmp	.LBB33_224
	.p2align	4, 0x90
.LBB33_204:                             # %.loopexit.i.i
                                        #   in Loop: Header=BB33_224 Depth=2
	incq	%r13
	movl	88(%r14), %ecx
	cmpl	%ecx, %r13d
	jb	.LBB33_223
	jmp	.LBB33_240
	.p2align	4, 0x90
.LBB33_205:                             #   in Loop: Header=BB33_224 Depth=2
	testq	%rbx, %rbx
	movq	%rbp, %rdi
	je	.LBB33_226
# BB#206:                               #   in Loop: Header=BB33_224 Depth=2
	movq	%rbx, 400(%rbp)
	cmpl	$0, 136(%rbp)
	je	.LBB33_225
# BB#207:                               # %.preheader.lr.ph.i.i.i
                                        #   in Loop: Header=BB33_224 Depth=2
	leaq	136(%rbx), %r15
	leaq	152(%rbx), %r14
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB33_208:                             # %.preheader.i.i.i
                                        #   Parent Loop BB33_201 Depth=1
                                        #     Parent Loop BB33_224 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB33_210 Depth 4
	movl	(%r15), %eax
	testl	%eax, %eax
	je	.LBB33_212
# BB#209:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB33_208 Depth=3
	movq	144(%rbp), %rcx
	movq	(%rcx,%r12,8), %rcx
	movq	8(%rcx), %rdx
	movq	144(%rbx), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB33_210:                             #   Parent Loop BB33_201 Depth=1
                                        #     Parent Loop BB33_224 Depth=2
                                        #       Parent Loop BB33_208 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rcx,%rsi,8), %rdi
	cmpq	8(%rdi), %rdx
	je	.LBB33_220
# BB#211:                               #   in Loop: Header=BB33_210 Depth=4
	incq	%rsi
	cmpl	%eax, %esi
	jb	.LBB33_210
	jmp	.LBB33_213
	.p2align	4, 0x90
.LBB33_212:                             # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB33_208 Depth=3
	movq	144(%rbx), %rcx
	xorl	%eax, %eax
	testq	%rcx, %rcx
	je	.LBB33_219
.LBB33_213:                             # %._crit_edge.thread.i.i.i
                                        #   in Loop: Header=BB33_208 Depth=3
	cmpq	%r14, %rcx
	je	.LBB33_216
# BB#214:                               #   in Loop: Header=BB33_208 Depth=3
	testb	$7, %al
	je	.LBB33_218
# BB#215:                               #   in Loop: Header=BB33_208 Depth=3
	movq	144(%rbp), %rdx
	movq	(%rdx,%r12,8), %rdx
	leal	1(%rax), %esi
	movl	%esi, (%r15)
	movl	%eax, %eax
	movq	%rdx, (%rcx,%rax,8)
	jmp	.LBB33_220
	.p2align	4, 0x90
.LBB33_216:                             #   in Loop: Header=BB33_208 Depth=3
	cmpl	$2, %eax
	ja	.LBB33_218
# BB#217:                               #   in Loop: Header=BB33_208 Depth=3
	movq	144(%rbp), %rcx
	movq	(%rcx,%r12,8), %rcx
	leal	1(%rax), %edx
	movl	%edx, 136(%rbx)
	movl	%eax, %eax
	movq	%rcx, 152(%rbx,%rax,8)
	jmp	.LBB33_220
	.p2align	4, 0x90
.LBB33_218:                             #   in Loop: Header=BB33_208 Depth=3
	movq	144(%rbp), %rax
	movq	(%rax,%r12,8), %rsi
	movq	%r15, %rdi
	callq	vec_add_internal
	jmp	.LBB33_220
.LBB33_219:                             #   in Loop: Header=BB33_208 Depth=3
	movq	144(%rbp), %rax
	movq	(%rax,%r12,8), %rax
	movq	%r14, 144(%rbx)
	movl	$1, 136(%rbx)
	movq	%rax, 152(%rbx)
	.p2align	4, 0x90
.LBB33_220:                             # %.loopexit.i.i.i
                                        #   in Loop: Header=BB33_208 Depth=3
	incq	%r12
	cmpl	136(%rbp), %r12d
	jb	.LBB33_208
# BB#221:                               #   in Loop: Header=BB33_224 Depth=2
	movq	%rbx, %rdi
	movq	24(%rsp), %r14          # 8-byte Reload
	movq	(%rsp), %r8             # 8-byte Reload
	jmp	.LBB33_226
	.p2align	4, 0x90
.LBB33_223:                             # %.loopexit._crit_edge.i.i
                                        #   in Loop: Header=BB33_224 Depth=2
	movl	8(%r8), %esi
	movq	%rdi, %rbx
.LBB33_224:                             # %.lr.ph6.i.i
                                        #   Parent Loop BB33_201 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB33_208 Depth 3
                                        #         Child Loop BB33_210 Depth 4
                                        #       Child Loop BB33_228 Depth 3
	movq	96(%r14), %rax
	movq	(%rax,%r13,8), %rbp
	testl	%esi, %esi
	je	.LBB33_205
.LBB33_225:                             #   in Loop: Header=BB33_224 Depth=2
	movq	%rbx, %rdi
.LBB33_226:                             # %.preheader.i.i
                                        #   in Loop: Header=BB33_224 Depth=2
	movl	16(%rbp), %ecx
	testq	%rcx, %rcx
	je	.LBB33_204
# BB#227:                               # %.lr.ph.i.i64
                                        #   in Loop: Header=BB33_224 Depth=2
	movq	24(%rbp), %rdx
	.p2align	4, 0x90
.LBB33_228:                             #   Parent Loop BB33_201 Depth=1
                                        #     Parent Loop BB33_224 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rdx), %rax
	cmpl	$1, (%rax)
	jne	.LBB33_239
# BB#229:                               #   in Loop: Header=BB33_228 Depth=3
	movq	16(%rax), %rax
	movzbl	36(%rax), %eax
	andb	$7, %al
	je	.LBB33_234
# BB#230:                               #   in Loop: Header=BB33_228 Depth=3
	cmpb	$1, %al
	jne	.LBB33_239
# BB#231:                               #   in Loop: Header=BB33_228 Depth=3
	movzbl	376(%rbp), %eax
	movl	%eax, %ebx
	shrb	$3, %bl
	orb	$2, %bl
	movb	$8, %sil
	andb	$3, %bl
	cmpb	$3, %bl
	je	.LBB33_233
# BB#232:                               #   in Loop: Header=BB33_228 Depth=3
	movb	$16, %sil
.LBB33_233:                             #   in Loop: Header=BB33_228 Depth=3
	andb	$-25, %al
	orb	%sil, %al
	movb	%al, 376(%rbp)
	jmp	.LBB33_239
.LBB33_234:                             #   in Loop: Header=BB33_228 Depth=3
	movzbl	376(%rbp), %ebx
	movl	%ebx, %eax
	shrb	$3, %al
	andb	$3, %al
	cmpb	$3, %al
	je	.LBB33_236
# BB#235:                               #   in Loop: Header=BB33_228 Depth=3
	testb	%al, %al
	jne	.LBB33_237
.LBB33_236:                             #   in Loop: Header=BB33_228 Depth=3
	andb	$-25, %bl
	jmp	.LBB33_238
.LBB33_237:                             #   in Loop: Header=BB33_228 Depth=3
	andb	$-25, %bl
	orb	$16, %bl
.LBB33_238:                             #   in Loop: Header=BB33_228 Depth=3
	movb	%bl, 376(%rbp)
	.p2align	4, 0x90
.LBB33_239:                             #   in Loop: Header=BB33_228 Depth=3
	addq	$8, %rdx
	decq	%rcx
	jne	.LBB33_228
	jmp	.LBB33_204
	.p2align	4, 0x90
.LBB33_240:                             # %compute_declaration_states.exit.i.loopexit
                                        #   in Loop: Header=BB33_201 Depth=1
	movl	160(%r14), %eax
	movl	%ecx, %edx
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB33_242
.LBB33_241:                             #   in Loop: Header=BB33_201 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB33_242:                             # %compute_declaration_states.exit.i
                                        #   in Loop: Header=BB33_201 Depth=1
	incq	%rdi
	cmpl	%eax, %edi
	jb	.LBB33_201
.LBB33_243:                             # %.preheader.i58
	testl	%ecx, %ecx
	je	.LBB33_248
# BB#244:                               # %.lr.ph.i59
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB33_245:                             # =>This Inner Loop Header: Depth=1
	movq	96(%r14), %rdx
	movq	(%rdx,%rax,8), %rsi
	movzbl	376(%rsi), %edx
	movl	%edx, %ebx
	andb	$24, %bl
	cmpb	$24, %bl
	jne	.LBB33_247
# BB#246:                               #   in Loop: Header=BB33_245 Depth=1
	andb	$-25, %dl
	movb	%dl, 376(%rsi)
	movl	88(%r14), %ecx
.LBB33_247:                             #   in Loop: Header=BB33_245 Depth=1
	incq	%rax
	movl	%ecx, %edx
	cmpq	%rdx, %rax
	jb	.LBB33_245
.LBB33_248:                             # %map_declarations_to_states.exit
	cmpl	$0, verbose_level(%rip)
	je	.LBB33_251
# BB#249:
	movl	8(%r14), %esi
	movl	48(%r14), %edx
	movl	160(%r14), %r8d
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	printf
	cmpl	$2, verbose_level(%rip)
	jl	.LBB33_251
# BB#250:
	movq	%r14, %rdi
	callq	print_grammar
	movq	%r14, %rdi
	callq	print_states
.LBB33_251:                             # %.thread
	movq	%r14, %rdi
	callq	build_scanners
	movq	%r14, %rdi
	callq	build_eq
	xorl	%eax, %eax
	addq	$360, %rsp              # imm = 0x168
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB33_253:                             # %lookup_production.exit.i52
	movl	$168, %edi
	callq	malloc
	movq	%rax, %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%r14)
	movups	%xmm0, 128(%r14)
	movups	%xmm0, 112(%r14)
	movups	%xmm0, 96(%r14)
	movups	%xmm0, 80(%r14)
	movups	%xmm0, 64(%r14)
	movups	%xmm0, 48(%r14)
	movups	%xmm0, 32(%r14)
	movups	%xmm0, 16(%r14)
	movups	%xmm0, (%r14)
	movq	$0, 160(%r14)
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, 8(%r14)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%rax, 72(%r14)
	movl	$3, (%rax)
	movq	%r14, 8(%rax)
	movq	24(%rsp), %rbx          # 8-byte Reload
	movl	576(%rbx), %eax
	movl	%eax, 152(%r14)
	movl	$32, %edi
	callq	malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	movq	%r15, 16(%rax)
	movq	%r14, 8(%rax)
	movq	%rax, 208(%r15)
	movl	588(%rbx), %eax
	leal	1(%rax), %ecx
	movl	%ecx, 588(%rbx)
	movl	%eax, (%r14)
	movl	%ebp, %eax
	jmp	.LBB33_182
.Lfunc_end33:
	.size	build_grammar, .Lfunc_end33-build_grammar
	.cfi_endproc

	.p2align	4, 0x90
	.type	convert_regex_production_one,@function
convert_regex_production_one:           # @convert_regex_production_one
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi303:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi304:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi305:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi306:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi307:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi308:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi309:
	.cfi_def_cfa_offset 80
.Lcfi310:
	.cfi_offset %rbx, -56
.Lcfi311:
	.cfi_offset %r12, -48
.Lcfi312:
	.cfi_offset %r13, -40
.Lcfi313:
	.cfi_offset %r14, -32
.Lcfi314:
	.cfi_offset %r15, -24
.Lcfi315:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	cmpq	$0, 216(%r13)
	jne	.LBB34_93
# BB#1:
	movb	60(%r13), %al
	testb	$2, %al
	je	.LBB34_3
# BB#2:
	movq	(%r13), %rsi
	movl	$.L.str.67, %edi
	xorl	%eax, %eax
	callq	d_fail
	movb	60(%r13), %al
.LBB34_3:
	orb	$2, %al
	movb	%al, 60(%r13)
	movl	16(%r13), %eax
	testl	%eax, %eax
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	je	.LBB34_4
# BB#5:                                 # %.lr.ph225
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	movl	$0, (%rsp)              # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB34_6:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_12 Depth 2
                                        #       Child Loop BB34_17 Depth 3
	movq	24(%r13), %rcx
	movq	(%rcx,%rbx,8), %r15
	cmpq	$0, 96(%r15)
	jne	.LBB34_9
# BB#7:                                 #   in Loop: Header=BB34_6 Depth=1
	cmpl	$2, %eax
	jb	.LBB34_10
# BB#8:                                 #   in Loop: Header=BB34_6 Depth=1
	movq	80(%r15), %rax
	testq	%rax, %rax
	je	.LBB34_10
.LBB34_9:                               #   in Loop: Header=BB34_6 Depth=1
	movq	(%r13), %rsi
	movl	$.L.str.68, %edi
	xorl	%eax, %eax
	callq	d_fail
.LBB34_10:                              # %.preheader203
                                        #   in Loop: Header=BB34_6 Depth=1
	cmpl	$0, 32(%r15)
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB34_28
# BB#11:                                # %.lr.ph217
                                        #   in Loop: Header=BB34_6 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB34_12
.LBB34_22:                              #   in Loop: Header=BB34_12 Depth=2
	movl	$5, %eax
	movl	$1, (%rsp)              # 4-byte Folded Spill
	jmp	.LBB34_27
	.p2align	4, 0x90
.LBB34_12:                              #   Parent Loop BB34_6 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB34_17 Depth 3
	movq	40(%r15), %rax
	movq	(%rax,%r12,8), %rax
	cmpl	$0, (%rax)
	leaq	16(%rax), %rbx
	je	.LBB34_13
# BB#24:                                #   in Loop: Header=BB34_12 Depth=2
	movq	(%rbx), %rax
	movl	(%rax), %eax
	andl	$-2, %eax
	cmpl	$2, %eax
	jne	.LBB34_26
# BB#25:                                #   in Loop: Header=BB34_12 Depth=2
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	callq	d_fail
	jmp	.LBB34_26
	.p2align	4, 0x90
.LBB34_13:                              #   in Loop: Header=BB34_12 Depth=2
	movq	(%rbx), %r14
	testb	$1, 60(%r14)
	jne	.LBB34_15
# BB#14:                                #   in Loop: Header=BB34_12 Depth=2
	movq	(%r13), %rsi
	movq	(%r14), %rdx
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	callq	d_fail
	movq	(%rbx), %r14
.LBB34_15:                              #   in Loop: Header=BB34_12 Depth=2
	movl	16(%r14), %eax
	testl	%eax, %eax
	je	.LBB34_21
# BB#16:                                # %.lr.ph212
                                        #   in Loop: Header=BB34_12 Depth=2
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB34_17:                              #   Parent Loop BB34_6 Depth=1
                                        #     Parent Loop BB34_12 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	24(%r14), %rcx
	movq	(%rcx,%rbx,8), %rcx
	cmpq	$0, 80(%rcx)
	jne	.LBB34_19
# BB#18:                                #   in Loop: Header=BB34_17 Depth=3
	cmpq	$0, 96(%rcx)
	je	.LBB34_20
.LBB34_19:                              #   in Loop: Header=BB34_17 Depth=3
	movq	(%r13), %rdx
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	d_fail
	movl	16(%r14), %eax
.LBB34_20:                              #   in Loop: Header=BB34_17 Depth=3
	incq	%rbx
	cmpl	%eax, %ebx
	jb	.LBB34_17
.LBB34_21:                              # %._crit_edge213
                                        #   in Loop: Header=BB34_12 Depth=2
	cmpq	%r13, %r14
	je	.LBB34_22
# BB#23:                                #   in Loop: Header=BB34_12 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	%r14, %rsi
	callq	convert_regex_production_one
	addq	$216, %r14
	movq	%r14, %rbx
.LBB34_26:                              # %.sink.split
                                        #   in Loop: Header=BB34_12 Depth=2
	movq	(%rbx), %rax
	movl	32(%rax), %eax
	addl	$5, %eax
.LBB34_27:                              #   in Loop: Header=BB34_12 Depth=2
	addl	%eax, %ebp
	incq	%r12
	cmpl	32(%r15), %r12d
	jb	.LBB34_12
.LBB34_28:                              # %._crit_edge218
                                        #   in Loop: Header=BB34_6 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movl	16(%r13), %eax
	cmpl	%eax, %ebx
	jb	.LBB34_6
	jmp	.LBB34_29
.LBB34_4:
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%ebp, %ebp
.LBB34_29:                              # %._crit_edge226
	incl	%ebp
	movslq	%ebp, %rdi
	callq	malloc
	movq	%rax, %r12
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbp
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbp)
	movups	%xmm0, 8(%rbp)
	movl	$1, (%rbp)
	movq	%r12, 24(%rbp)
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	48(%rdi), %eax
	movl	%eax, 4(%rbp)
	movq	%r13, 40(%rbp)
	movq	56(%rdi), %rcx
	leaq	64(%rdi), %rdx
	testq	%rcx, %rcx
	je	.LBB34_30
# BB#31:
	addq	$48, %rdi
	cmpq	%rdx, %rcx
	je	.LBB34_32
# BB#34:
	testb	$7, %al
	je	.LBB34_37
# BB#35:
	leal	1(%rax), %edx
	jmp	.LBB34_36
.LBB34_30:
	movq	%rdx, 56(%rdi)
	leal	1(%rax), %ecx
	movl	%ecx, 48(%rdi)
	movq	%rbp, 64(%rdi,%rax,8)
	jmp	.LBB34_38
.LBB34_32:
	cmpl	$2, %eax
	ja	.LBB34_37
# BB#33:
	movl	%eax, %edx
	incl	%edx
.LBB34_36:
	movl	%edx, (%rdi)
	movq	%rbp, (%rcx,%rax,8)
	jmp	.LBB34_38
.LBB34_37:
	movq	%rbp, %rsi
	callq	vec_add_internal
.LBB34_38:
	movl	(%rsp), %eax            # 4-byte Reload
	movq	%rbp, 216(%r13)
	testl	%eax, %eax
	movl	16(%r13), %eax
	je	.LBB34_62
# BB#39:
	cmpl	$2, %eax
	jne	.LBB34_40
	jmp	.LBB34_41
.LBB34_62:
	cmpl	$2, %eax
	jb	.LBB34_64
# BB#63:                                # %.preheader.thread
	movb	$40, (%r12)
	incq	%r12
	jmp	.LBB34_65
.LBB34_64:                              # %.preheader
	testl	%eax, %eax
	je	.LBB34_92
.LBB34_65:                              # %.lr.ph
	xorl	%ebp, %ebp
	jmp	.LBB34_66
	.p2align	4, 0x90
.LBB34_75:                              #   in Loop: Header=BB34_66 Depth=1
	movq	%rbx, %rdi
	callq	strlen
	leaq	2(%rax,%rax), %rdi
	callq	malloc
	movq	%rax, %r15
	movb	(%rbx), %cl
	testb	%cl, %cl
	movabsq	$29273397586296879, %rsi # imm = 0x6800000080002F
	je	.LBB34_81
# BB#76:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB34_66 Depth=1
	incq	%rbx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB34_77:                              # %.lr.ph.i
                                        #   Parent Loop BB34_66 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbl	%cl, %edx
	addl	$-40, %edx
	cmpl	$54, %edx
	ja	.LBB34_80
# BB#78:                                # %.lr.ph.i
                                        #   in Loop: Header=BB34_77 Depth=2
	btq	%rdx, %rsi
	jae	.LBB34_80
# BB#79:                                #   in Loop: Header=BB34_77 Depth=2
	movb	$92, (%rax)
	incq	%rax
	movzbl	-1(%rbx), %ecx
.LBB34_80:                              #   in Loop: Header=BB34_77 Depth=2
	movb	%cl, (%rax)
	incq	%rax
	movzbl	(%rbx), %ecx
	incq	%rbx
	testb	%cl, %cl
	jne	.LBB34_77
.LBB34_81:                              # %escape_string_for_regex.exit
                                        #   in Loop: Header=BB34_66 Depth=1
	movb	$0, (%rax)
.LBB34_82:                              #   in Loop: Header=BB34_66 Depth=1
	movq	%r15, %rdi
	callq	strlen
	movq	%r12, %rdi
	movq	%r15, %rsi
	movq	%rax, %rdx
	callq	memcpy
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	$0, (%rax)
	jne	.LBB34_84
# BB#83:                                #   in Loop: Header=BB34_66 Depth=1
	movq	%r15, %rdi
	callq	free
.LBB34_84:                              #   in Loop: Header=BB34_66 Depth=1
	addq	%rbx, %r12
	incl	%r14d
	movq	8(%rsp), %rdx           # 8-byte Reload
	jmp	.LBB34_69
	.p2align	4, 0x90
.LBB34_66:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB34_77 Depth 2
	movq	24(%r13), %rax
	movq	(%rax,%rbp,8), %rdx
	movl	32(%rdx), %eax
	cmpl	$2, %eax
	jb	.LBB34_67
# BB#68:                                #   in Loop: Header=BB34_66 Depth=1
	movb	$40, (%r12)
	incq	%r12
	xorl	%r14d, %r14d
	jmp	.LBB34_69
	.p2align	4, 0x90
.LBB34_67:                              #   in Loop: Header=BB34_66 Depth=1
	xorl	%r14d, %r14d
	cmpl	%eax, %r14d
	jb	.LBB34_71
	jmp	.LBB34_85
	.p2align	4, 0x90
.LBB34_69:                              # %thread-pre-split
                                        #   in Loop: Header=BB34_66 Depth=1
	movl	32(%rdx), %eax
	cmpl	%eax, %r14d
	jae	.LBB34_85
.LBB34_71:                              #   in Loop: Header=BB34_66 Depth=1
	movq	40(%rdx), %rax
	movslq	%r14d, %rcx
	movq	(%rax,%rcx,8), %rax
	cmpl	$1, (%rax)
	leaq	16(%rax), %rax
	je	.LBB34_73
# BB#72:                                #   in Loop: Header=BB34_66 Depth=1
	movq	(%rax), %rax
	movl	$216, %ecx
	addq	%rcx, %rax
.LBB34_73:                              #   in Loop: Header=BB34_66 Depth=1
	movq	(%rax), %rax
	movq	24(%rax), %rbx
	cmpl	$0, (%rax)
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rax, (%rsp)            # 8-byte Spill
	je	.LBB34_75
# BB#74:                                #   in Loop: Header=BB34_66 Depth=1
	movq	%rbx, %r15
	jmp	.LBB34_82
	.p2align	4, 0x90
.LBB34_85:                              #   in Loop: Header=BB34_66 Depth=1
	cmpl	$2, %eax
	jb	.LBB34_87
# BB#86:                                #   in Loop: Header=BB34_66 Depth=1
	movb	$41, (%r12)
	incq	%r12
.LBB34_87:                              #   in Loop: Header=BB34_66 Depth=1
	movl	16(%r13), %eax
	leal	-1(%rax), %ecx
	cmpq	%rcx, %rbp
	je	.LBB34_89
# BB#88:                                #   in Loop: Header=BB34_66 Depth=1
	movb	$124, (%r12)
	incq	%r12
	movl	16(%r13), %eax
.LBB34_89:                              #   in Loop: Header=BB34_66 Depth=1
	incq	%rbp
	cmpl	%eax, %ebp
	jb	.LBB34_66
# BB#90:                                # %._crit_edge
	cmpl	$2, %eax
	jb	.LBB34_92
# BB#91:
	movb	$41, (%r12)
	incq	%r12
	jmp	.LBB34_92
.LBB34_40:
	movq	(%r13), %rsi
	movl	$.L.str.72, %edi
	xorl	%eax, %eax
	callq	d_fail
.LBB34_41:
	movq	24(%r13), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rsi
	movl	32(%rcx), %edx
	movl	32(%rsi), %esi
	leal	(%rsi,%rdx), %ebp
	movl	%ebp, %edi
	orl	$1, %edi
	cmpl	$3, %edi
	jne	.LBB34_40
# BB#42:
	cmpl	$2, %edx
	je	.LBB34_44
# BB#43:
	cmpl	$2, %esi
	jne	.LBB34_40
.LBB34_44:
	xorl	%esi, %esi
	cmpl	$2, %edx
	setne	%sil
	movq	(%rax,%rsi,8), %rdx
	xorl	%esi, %esi
	cmpq	%rdx, %rcx
	sete	%sil
	movq	(%rax,%rsi,8), %rcx
	movq	40(%rdx), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	cmpq	%r13, 16(%rdx)
	je	.LBB34_46
# BB#45:
	cmpq	%r13, 16(%rax)
	jne	.LBB34_40
.LBB34_46:                              # %._crit_edge240
	cmpl	$0, 32(%rcx)
	je	.LBB34_48
# BB#47:
	movq	16(%rax), %rdx
	movq	40(%rcx), %rcx
	movq	(%rcx), %rcx
	cmpq	16(%rcx), %rdx
	jne	.LBB34_40
.LBB34_48:
	cmpl	$1, (%rax)
	leaq	16(%rax), %rax
	je	.LBB34_50
# BB#49:
	movl	$216, %ecx
	addq	(%rax), %rcx
	movq	%rcx, %rax
.LBB34_50:
	movq	(%rax), %rax
	movq	%r12, %r15
	incq	%r15
	movb	$40, (%r12)
	movq	24(%rax), %r12
	cmpl	$0, (%rax)
	movq	%rax, 8(%rsp)           # 8-byte Spill
	je	.LBB34_52
# BB#51:
	movq	%r12, %r14
	jmp	.LBB34_59
.LBB34_52:
	movq	%r12, %rdi
	callq	strlen
	leaq	2(%rax,%rax), %rdi
	callq	malloc
	movq	%rax, %r14
	movb	(%r12), %cl
	testb	%cl, %cl
	je	.LBB34_58
# BB#53:                                # %.lr.ph.i198.preheader
	incq	%r12
	movabsq	$29273397586296879, %rdx # imm = 0x6800000080002F
	movq	%r14, %rax
	.p2align	4, 0x90
.LBB34_54:                              # %.lr.ph.i198
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%cl, %esi
	addl	$-40, %esi
	cmpl	$54, %esi
	ja	.LBB34_57
# BB#55:                                # %.lr.ph.i198
                                        #   in Loop: Header=BB34_54 Depth=1
	btq	%rsi, %rdx
	jae	.LBB34_57
# BB#56:                                #   in Loop: Header=BB34_54 Depth=1
	movb	$92, (%rax)
	incq	%rax
	movzbl	-1(%r12), %ecx
.LBB34_57:                              #   in Loop: Header=BB34_54 Depth=1
	movb	%cl, (%rax)
	incq	%rax
	movzbl	(%r12), %ecx
	incq	%r12
	testb	%cl, %cl
	jne	.LBB34_54
.LBB34_58:                              # %escape_string_for_regex.exit202
	movb	$0, (%rax)
.LBB34_59:
	movq	%r14, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%rax, %rdx
	callq	memcpy
	movq	%r14, %rdi
	callq	strlen
	movq	%rax, %rbx
	addq	%r15, %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, (%rax)
	jne	.LBB34_61
# BB#60:
	movq	%r14, %rdi
	callq	free
.LBB34_61:
	movb	$41, (%rbx)
	cmpl	$2, %ebp
	setne	%al
	orb	$42, %al
	movb	%al, 1(%rbx)
	leaq	2(%rbx), %r12
.LBB34_92:                              # %._crit_edge.thread
	movb	$0, (%r12)
	movq	216(%r13), %rbx
	movq	24(%rbx), %rdi
	callq	strlen
	movl	%eax, 32(%rbx)
	andb	$-3, 60(%r13)
.LBB34_93:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end34:
	.size	convert_regex_production_one, .Lfunc_end34-convert_regex_production_one
	.cfi_endproc

	.p2align	4, 0x90
	.type	set_declaration_group,@function
set_declaration_group:                  # @set_declaration_group
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi316:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi317:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi318:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi319:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi320:
	.cfi_def_cfa_offset 48
.Lcfi321:
	.cfi_offset %rbx, -48
.Lcfi322:
	.cfi_offset %r12, -40
.Lcfi323:
	.cfi_offset %r13, -32
.Lcfi324:
	.cfi_offset %r14, -24
.Lcfi325:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	8(%r14), %eax
	movq	72(%r12,%rax,8), %rcx
	cmpq	%r15, %rcx
	je	.LBB35_11
# BB#1:
	testl	%eax, %eax
	jne	.LBB35_3
# BB#2:
	testq	%rcx, %rcx
	je	.LBB35_3
# BB#12:
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	jmp	d_fail                  # TAILCALL
.LBB35_3:
	movq	%r15, 72(%r12,%rax,8)
	movq	%r14, 136(%r12,%rax,8)
	cmpl	$0, 16(%r12)
	je	.LBB35_11
# BB#4:                                 # %.preheader.lr.ph
	movq	24(%r12), %rax
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB35_5:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_7 Depth 2
	movq	(%rax,%r13,8), %rcx
	cmpl	$0, 32(%rcx)
	je	.LBB35_10
# BB#6:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB35_5 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB35_7:                               # %.lr.ph
                                        #   Parent Loop BB35_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	40(%rcx), %rcx
	movq	(%rcx,%rbx,8), %rcx
	cmpl	$0, (%rcx)
	jne	.LBB35_9
# BB#8:                                 #   in Loop: Header=BB35_7 Depth=2
	movq	16(%rcx), %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	set_declaration_group
	movq	24(%r12), %rax
.LBB35_9:                               #   in Loop: Header=BB35_7 Depth=2
	incq	%rbx
	movq	(%rax,%r13,8), %rcx
	cmpl	32(%rcx), %ebx
	jb	.LBB35_7
.LBB35_10:                              # %._crit_edge
                                        #   in Loop: Header=BB35_5 Depth=1
	incq	%r13
	cmpl	16(%r12), %r13d
	jb	.LBB35_5
.LBB35_11:                              # %.loopexit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end35:
	.size	set_declaration_group, .Lfunc_end35-set_declaration_group
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"declare expects argument, line %d"
	.size	.L.str, 34

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"declare does not expect argument, line %d"
	.size	.L.str.1, 42

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"duplicate pass '%s' line %d"
	.size	.L.str.2, 28

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"unknown pass '%s' line %d"
	.size	.L.str.3, 26

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	" _synthetic"
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"%s.%d"
	.size	.L.str.5, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"<EOF> "
	.size	.L.str.8, 7

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"string(\"%s\") "
	.size	.L.str.9, 14

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"regex(\"%s\") "
	.size	.L.str.10, 13

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"code(\"%s\") "
	.size	.L.str.11, 12

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"token(\"%s\") "
	.size	.L.str.12, 13

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"unknown token kind"
	.size	.L.str.13, 19

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%s "
	.size	.L.str.14, 4

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"$none"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"$left"
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"$right"
	.size	.L.str.17, 7

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"$unary_left"
	.size	.L.str.18, 12

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"$unary_right"
	.size	.L.str.19, 13

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"$binary_left"
	.size	.L.str.20, 13

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"$binary_right"
	.size	.L.str.21, 14

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"$noassoc"
	.size	.L.str.22, 9

	.type	assoc_strings,@object   # @assoc_strings
	.data
	.globl	assoc_strings
	.p2align	4
assoc_strings:
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.15
	.long	5                       # 0x5
	.zero	4
	.quad	.L.str.16
	.long	6                       # 0x6
	.zero	4
	.quad	.L.str.17
	.long	9                       # 0x9
	.zero	4
	.quad	.L.str.18
	.long	10                      # 0xa
	.zero	4
	.quad	.L.str.19
	.long	17                      # 0x11
	.zero	4
	.quad	.L.str.20
	.long	18                      # 0x12
	.zero	4
	.quad	.L.str.21
	.long	32                      # 0x20
	.zero	4
	.quad	.L.str.22
	.size	assoc_strings, 128

	.type	.L.str.23,@object       # @.str.23
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.23:
	.asciz	"%s: "
	.size	.L.str.23, 5

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SPECULATIVE_CODE\n%s\nEND CODE\n"
	.size	.L.str.24, 30

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"FINAL_CODE\n%s\nEND CODE\n"
	.size	.L.str.25, 24

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"%s (%d)\n"
	.size	.L.str.27, 9

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"\t: "
	.size	.L.str.28, 4

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"\t| "
	.size	.L.str.29, 4

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"op %d "
	.size	.L.str.30, 7

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"rule %d "
	.size	.L.str.31, 9

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"(%d)\n"
	.size	.L.str.36, 6

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"eq %d %d "
	.size	.L.str.37, 10

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"diff state (%d %d) "
	.size	.L.str.38, 20

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"diff rule "
	.size	.L.str.39, 11

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"[ "
	.size	.L.str.40, 3

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"][ "
	.size	.L.str.41, 4

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"reduces_to %d %d\n"
	.size	.L.str.43, 18

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"r"
	.size	.L.str.44, 2

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"%d productions %d terminals %d states %d declarations\n"
	.size	.L.str.45, 55

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"encountered an escaped NULL while processing '%s'"
	.size	.L.str.46, 50

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"empty string after unescape '%s'"
	.size	.L.str.47, 33

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"STATE %d (%d ITEMS)%s\n"
	.size	.L.str.48, 23

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	" ACCEPT"
	.size	.L.str.49, 8

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.zero	1
	.size	.L.str.50, 1

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	" : %d\n"
	.size	.L.str.52, 7

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"\t%s\t"
	.size	.L.str.54, 5

	.type	action_types,@object    # @action_types
	.section	.rodata,"a",@progbits
	.p2align	4
action_types:
	.quad	.L.str.60
	.quad	.L.str.61
	.quad	.L.str.62
	.size	action_types, 24

	.type	.L.str.55,@object       # @.str.55
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.55:
	.asciz	"%d"
	.size	.L.str.55, 3

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"reduce/reduce"
	.size	.L.str.56, 14

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"shift/reduce"
	.size	.L.str.57, 13

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"\t%s: "
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	". "
	.size	.L.str.59, 3

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"ACCEPT"
	.size	.L.str.60, 7

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"SHIFT"
	.size	.L.str.61, 6

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"REDUCE"
	.size	.L.str.62, 7

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"\t%s conflict "
	.size	.L.str.64, 14

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"duplicate production '%s'"
	.size	.L.str.65, 26

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"unresolved identifier: '%s'"
	.size	.L.str.66, 28

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"circular regex production '%s'"
	.size	.L.str.67, 31

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"final and/or multi-rule code not permitted in regex productions '%s'"
	.size	.L.str.68, 69

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"regex production '%s' cannot invoke non-regex production '%s'"
	.size	.L.str.69, 62

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"code not permitted in rule %d of regex productions '%s'"
	.size	.L.str.70, 56

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"regex production '%s' cannot include scanners or tokens"
	.size	.L.str.71, 56

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"unable to resolve circular regex production: '%s'"
	.size	.L.str.72, 50

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"unresolved declaration '%s'"
	.size	.L.str.73, 28

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"shared tokenize subtrees"
	.size	.L.str.74, 25

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"whitespace"
	.size	.L.str.75, 11

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"number of rules in default action != 1"
	.size	.L.str.77, 39

	.type	.Lstr,@object           # @str
.Lstr:
	.asciz	"PRODUCTIONS\n"
	.size	.Lstr, 13

	.type	.Lstr.1,@object         # @str.1
.Lstr.1:
	.asciz	"TERMINALS\n"
	.size	.Lstr.1, 11

	.type	.Lstr.2,@object         # @str.2
.Lstr.2:
	.asciz	"\t;"
	.size	.Lstr.2, 3

	.type	.Lstr.3,@object         # @str.3
.Lstr.3:
	.asciz	"  ACTION"
	.size	.Lstr.3, 9

	.type	.Lstr.4,@object         # @str.4
.Lstr.4:
	.asciz	"  GOTO"
	.size	.Lstr.4, 7

	.type	.Lstr.5,@object         # @str.5
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.5:
	.asciz	"  CONFLICT (before precedence and associativity)"
	.size	.Lstr.5, 49


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
