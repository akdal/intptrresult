	.text
	.file	"ppowmod.bc"
	.globl	ppowmod
	.p2align	4, 0x90
	.type	ppowmod,@function
ppowmod:                                # @ppowmod
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	$0, 16(%rsp)
	movq	$0, 8(%rsp)
	movq	$0, (%rsp)
	testq	%rbx, %rbx
	je	.LBB0_2
# BB#1:
	incw	(%rbx)
.LBB0_2:
	testq	%r15, %r15
	je	.LBB0_4
# BB#3:
	incw	(%r15)
.LBB0_4:
	leaq	8(%rsp), %rdi
	movq	%r15, %rsi
	callq	psetq
	testq	%r14, %r14
	je	.LBB0_6
# BB#5:
	incw	(%r14)
.LBB0_6:
	movq	%rsp, %r12
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	psetq
	movq	pone(%rip), %rsi
	leaq	16(%rsp), %r13
	movq	%r13, %rdi
	callq	psetq
	leaq	8(%rsp), %rbp
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_7 Depth=1
	movq	%rdi, %rsi
	callq	pmul
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	pdivmod
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB0_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rdi
	callq	podd
	testl	%eax, %eax
	je	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rsi
	callq	pmul
	xorl	%edx, %edx
	movq	$-1, %rcx
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	pdivmod
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=1
	movq	(%rsp), %rdi
	callq	phalf
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	(%rsp), %rdi
	callq	pcmpz
	testl	%eax, %eax
	movq	8(%rsp), %rdi
	jne	.LBB0_10
# BB#11:
	testq	%rdi, %rdi
	je	.LBB0_14
# BB#12:
	decw	(%rdi)
	jne	.LBB0_14
# BB#13:
	xorl	%eax, %eax
	callq	pfree
.LBB0_14:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_17
# BB#15:
	decw	(%rdi)
	jne	.LBB0_17
# BB#16:
	xorl	%eax, %eax
	callq	pfree
.LBB0_17:
	testq	%r15, %r15
	je	.LBB0_20
# BB#18:
	decw	(%r15)
	jne	.LBB0_20
# BB#19:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB0_20:
	testq	%r14, %r14
	je	.LBB0_23
# BB#21:
	decw	(%r14)
	jne	.LBB0_23
# BB#22:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB0_23:
	testq	%rbx, %rbx
	je	.LBB0_26
# BB#24:
	decw	(%rbx)
	jne	.LBB0_26
# BB#25:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB0_26:
	movq	16(%rsp), %rdi
	callq	presult
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	ppowmod, .Lfunc_end0-ppowmod
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
