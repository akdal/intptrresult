	.text
	.file	"zcolor.bc"
	.globl	zcurrentgscolor
	.p2align	4, 0x90
	.type	zcurrentgscolor,@function
zcurrentgscolor:                        # @zcurrentgscolor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rcx
	movq	%rcx, osp(%rip)
	movl	$-16, %eax
	cmpq	ostop(%rip), %rcx
	ja	.LBB0_5
# BB#1:
	movl	gs_color_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.2, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB0_2
# BB#3:
	movq	%rax, 16(%rbx)
	movw	$56, 24(%rbx)
	movq	igs(%rip), %rdi
	movq	%rax, %rsi
	callq	gs_currentgscolor
	testl	%eax, %eax
	jns	.LBB0_6
	jmp	.LBB0_4
.LBB0_2:
	movl	$-25, %eax
.LBB0_4:                                # %make_color.exit.thread
	movq	osp(%rip), %rbx
	addq	$-16, %rbx
.LBB0_5:                                # %.sink.split
	movq	%rbx, osp(%rip)
.LBB0_6:
	popq	%rbx
	retq
.Lfunc_end0:
	.size	zcurrentgscolor, .Lfunc_end0-zcurrentgscolor
	.cfi_endproc

	.globl	make_color
	.p2align	4, 0x90
	.type	make_color,@function
make_color:                             # @make_color
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	gs_color_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str.2, %edx
	callq	alloc
	testq	%rax, %rax
	je	.LBB1_1
# BB#2:
	movq	%rax, (%rbx)
	movw	$56, 8(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB1_1:
	movl	$-25, %eax
	popq	%rbx
	retq
.Lfunc_end1:
	.size	make_color, .Lfunc_end1-make_color
	.cfi_endproc

	.globl	zsetgscolor
	.p2align	4, 0x90
	.type	zsetgscolor,@function
zsetgscolor:                            # @zsetgscolor
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$56, %ecx
	jne	.LBB2_3
# BB#1:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movq	igs(%rip), %rax
	movq	(%rdi), %rsi
	movq	%rax, %rdi
	callq	gs_setgscolor
	testl	%eax, %eax
	leaq	8(%rsp), %rsp
	js	.LBB2_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB2_3:
	retq
.Lfunc_end2:
	.size	zsetgscolor, .Lfunc_end2-zsetgscolor
	.cfi_endproc

	.globl	zcolor_op_init
	.p2align	4, 0x90
	.type	zcolor_op_init,@function
zcolor_op_init:                         # @zcolor_op_init
	.cfi_startproc
# BB#0:
	movl	$zcolor_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end3:
	.size	zcolor_op_init, .Lfunc_end3-zcolor_op_init
	.cfi_endproc

	.type	zcolor_op_init.my_defs,@object # @zcolor_op_init.my_defs
	.data
	.p2align	4
zcolor_op_init.my_defs:
	.quad	.L.str
	.quad	zcurrentgscolor
	.quad	.L.str.1
	.quad	zsetgscolor
	.zero	16
	.size	zcolor_op_init.my_defs, 48

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"0currentgscolor"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"1setgscolor"
	.size	.L.str.1, 12

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"make_color"
	.size	.L.str.2, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
