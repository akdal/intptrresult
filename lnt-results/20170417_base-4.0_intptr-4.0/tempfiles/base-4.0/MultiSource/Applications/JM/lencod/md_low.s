	.text
	.file	"md_low.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4625196817309499392     # double 16
.LCPI0_1:
	.quad	4602677017732795964     # double 0.49990000000000001
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.zero	16,2
	.text
	.globl	encode_one_macroblock_low
	.p2align	4, 0x90
	.type	encode_one_macroblock_low,@function
encode_one_macroblock_low:              # @encode_one_macroblock_low
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$408, %rsp              # imm = 0x198
.Lcfi6:
	.cfi_def_cfa_offset 464
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movw	$-256, 140(%rsp)
	movl	.Lencode_one_macroblock_low.bmcost+16(%rip), %eax
	movl	%eax, 400(%rsp)
	movapd	.Lencode_one_macroblock_low.bmcost(%rip), %xmm0
	movapd	%xmm0, 384(%rsp)
	movl	$0, 136(%rsp)
	movl	$0, 160(%rsp)
	movl	$0, 316(%rsp)
	movl	$0, 212(%rsp)
	movl	$0, 188(%rsp)
	movq	img(%rip), %rax
	movl	20(%rax), %esi
	cmpl	$1, %esi
	sete	144(%rsp)               # 1-byte Folded Spill
	testl	%esi, %esi
	sete	%cl
	cmpl	$3, %esi
	sete	%dl
	movl	%esi, 184(%rsp)         # 4-byte Spill
	cmpl	$2, %esi
	sete	%bpl
	movb	%cl, 143(%rsp)          # 1-byte Spill
	je	.LBB0_5
# BB#1:
	xorb	%dl, %cl
	xorb	$1, %cl
	jne	.LBB0_5
# BB#2:
	movl	164(%rax), %ecx
	cmpl	112(%rax), %ecx
	jne	.LBB0_3
# BB#4:
	cmpl	116(%rax), %ecx
	setne	%bpl
	jmp	.LBB0_5
.LBB0_3:
	xorl	%ebp, %ebp
.LBB0_5:
	movb	%dl, 167(%rsp)          # 1-byte Spill
	movq	14224(%rax), %r12
	movl	12(%rax), %eax
	movslq	%eax, %rdi
	imulq	$536, %rdi, %r15        # imm = 0x218
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	FmoGetPreviousMBNr
	movq	img(%rip), %rcx
	testl	%eax, %eax
	js	.LBB0_6
# BB#7:
	movl	$0, 156(%rsp)           # 4-byte Folded Spill
	cltq
	imulq	$536, %rax, %rax        # imm = 0x218
	addq	14224(%rcx), %rax
	jmp	.LBB0_8
.LBB0_6:
	movl	$0, 156(%rsp)           # 4-byte Folded Spill
	xorl	%eax, %eax
.LBB0_8:                                # %._crit_edge365
	movq	%rax, 344(%rsp)         # 8-byte Spill
	leaq	(%r12,%r15), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	128(%rcx), %r13
	movq	14384(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, 368(%rsp)         # 8-byte Spill
	movq	input(%rip), %rax
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_11
# BB#9:                                 # %._crit_edge365
	cmpl	$1, %eax
	jne	.LBB0_12
# BB#10:
	callq	UMHEX_decide_intrabk_SAD
	jmp	.LBB0_12
.LBB0_11:
	callq	smpUMHEX_decide_intrabk_SAD
.LBB0_12:
	xorl	%ebx, %ebx
	movl	184(%rsp), %r14d        # 4-byte Reload
	cmpl	$1, %r14d
	sete	%bl
	movq	img(%rip), %rax
	movl	12(%rax), %edi
	callq	RandomIntra
	movzbl	%bpl, %ecx
	orl	%eax, %ecx
	movswl	%cx, %ebp
	leaq	216(%rsp), %rsi
	movq	320(%rsp), %rdi         # 8-byte Reload
	movl	%ebp, %edx
	movl	%ebx, 328(%rsp)         # 4-byte Spill
	movl	%ebx, %ecx
	callq	init_enc_mb_params
	movl	$0, 416(%r12,%r15)
	movq	cs_cm(%rip), %rdi
	callq	store_coding_state
	movl	%ebp, 352(%rsp)         # 4-byte Spill
	testw	%bp, %bp
	movq	%r12, 192(%rsp)         # 8-byte Spill
	movq	%r15, 168(%rsp)         # 8-byte Spill
	movq	%r13, 376(%rsp)         # 8-byte Spill
	je	.LBB0_14
# BB#13:                                # %._crit_edge388
	leaq	472(%r12,%r15), %r13
	movl	$2147483647, %r14d      # imm = 0x7FFFFFFF
	xorl	%eax, %eax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	jmp	.LBB0_81
.LBB0_14:
	movw	$1, best_mode(%rip)
	cmpl	$1, %r14d
	jne	.LBB0_16
# BB#15:
	callq	Get_Direct_Motion_Vectors
	movl	184(%rsp), %r14d        # 4-byte Reload
.LBB0_16:
	movl	%r14d, %r13d
	movl	156(%rsp), %eax         # 4-byte Reload
	movb	144(%rsp), %cl          # 1-byte Reload
	movb	%cl, %al
	movl	%eax, 156(%rsp)         # 4-byte Spill
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_18
# BB#17:
	callq	get_initial_mb16x16_cost
.LBB0_18:                               # %.preheader318
	leaq	472(%r12,%r15), %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movl	$2147483647, 200(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x7FFFFFFF
	movl	$1, %ebx
	leaq	384(%rsp), %r14
	leaq	140(%rsp), %r15
	.p2align	4, 0x90
.LBB0_19:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_21 Depth 2
	movw	$0, bi_pred_me(%rip)
	movq	img(%rip), %rax
	movw	$0, 14408(%rax,%rbx,2)
	cmpw	$0, 260(%rsp,%rbx,2)
	je	.LBB0_56
# BB#20:                                #   in Loop: Header=BB0_19 Depth=1
	movl	$0, 136(%rsp)
	xorl	%ebp, %ebp
	cmpq	$1, %rbx
	setne	%bpl
	incq	%rbp
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_21:                               # %.preheader317
                                        #   Parent Loop BB0_19 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	input(%rip), %rcx
	movl	248(%rsp), %eax
	cmpl	$0, 4172(%rcx)
	je	.LBB0_22
# BB#24:                                #   in Loop: Header=BB0_21 Depth=2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_26
# BB#25:                                # %call.sqrt
                                        #   in Loop: Header=BB0_21 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 144(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	144(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_26:                               # %.split
                                        #   in Loop: Header=BB0_21 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 356(%rsp)
	movl	252(%rsp), %eax
	je	.LBB0_23
# BB#27:                                #   in Loop: Header=BB0_21 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_29
# BB#28:                                # %call.sqrt499
                                        #   in Loop: Header=BB0_21 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 144(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	144(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_29:                               # %.split498
                                        #   in Loop: Header=BB0_21 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	movq	input(%rip), %rcx
	cmpl	$0, 4172(%rcx)
	movl	%eax, 360(%rsp)
	movl	256(%rsp), %eax
	je	.LBB0_33
# BB#30:                                #   in Loop: Header=BB0_21 Depth=2
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movsd	lambda_mf_factor(%rip), %xmm1 # xmm1 = mem[0],zero
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB0_32
# BB#31:                                # %call.sqrt501
                                        #   in Loop: Header=BB0_21 Depth=2
	movapd	%xmm1, %xmm0
	movsd	%xmm2, 144(%rsp)        # 8-byte Spill
	callq	sqrt
	movsd	144(%rsp), %xmm2        # 8-byte Reload
                                        # xmm2 = mem[0],zero
.LBB0_32:                               # %.split500
                                        #   in Loop: Header=BB0_21 Depth=2
	mulsd	%xmm0, %xmm2
	cvttsd2si	%xmm2, %eax
	jmp	.LBB0_33
	.p2align	4, 0x90
.LBB0_22:                               # %.thread
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	%eax, 356(%rsp)
	movl	252(%rsp), %eax
.LBB0_23:                               # %.thread392
                                        #   in Loop: Header=BB0_21 Depth=2
	movl	%eax, 360(%rsp)
	movl	256(%rsp), %eax
.LBB0_33:                               #   in Loop: Header=BB0_21 Depth=2
	movl	%eax, 364(%rsp)
	movl	%ebx, %edi
	movl	%r12d, %esi
	leaq	356(%rsp), %rdx
	callq	PartitionMotionSearch
	movl	$2147483647, 384(%rsp)  # imm = 0x7FFFFFFF
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$0, %edi
	movl	%r12d, %esi
	movl	%ebx, %edx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	list_prediction_cost
	cmpl	$1, %r13d
	jne	.LBB0_39
# BB#34:                                #   in Loop: Header=BB0_21 Depth=2
	movl	$2147483647, 388(%rsp)  # imm = 0x7FFFFFFF
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$1, %edi
	movl	%r12d, %esi
	movl	%ebx, %edx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	list_prediction_cost
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$2, %edi
	movl	%r12d, %esi
	movl	%ebx, %edx
	movq	%r14, %rcx
	movq	%r15, %r8
	callq	list_prediction_cost
	cmpq	$1, %rbx
	jne	.LBB0_37
# BB#35:                                #   in Loop: Header=BB0_21 Depth=2
	movq	input(%rip), %rax
	movl	2120(%rax), %eax
	testl	%eax, %eax
	je	.LBB0_37
# BB#36:                                #   in Loop: Header=BB0_21 Depth=2
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movl	$3, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r14, %rcx
	callq	list_prediction_cost
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movl	$4, %edi
	movl	$1, %edx
	xorl	%r8d, %r8d
	movl	%r12d, %esi
	movq	%r14, %rcx
	callq	list_prediction_cost
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_39:                               #   in Loop: Header=BB0_21 Depth=2
	movb	$0, 135(%rsp)
	movl	384(%rsp), %eax
	addl	%eax, 136(%rsp)
	xorl	%eax, %eax
	jmp	.LBB0_40
	.p2align	4, 0x90
.LBB0_37:                               #   in Loop: Header=BB0_21 Depth=2
	movabsq	$9223372034707292159, %rax # imm = 0x7FFFFFFF7FFFFFFF
	movq	%rax, 396(%rsp)
.LBB0_38:                               #   in Loop: Header=BB0_21 Depth=2
	movl	$bi_pred_me, %r9d
	movl	%ebx, %edi
	movq	%r14, %rsi
	movq	%r15, %rdx
	leaq	135(%rsp), %rcx
	leaq	136(%rsp), %r8
	callq	determine_prediction_list
	movb	135(%rsp), %al
.LBB0_40:                               #   in Loop: Header=BB0_21 Depth=2
	movswl	290(%rsp), %ecx
	movsbl	140(%rsp), %r8d
	movsbl	141(%rsp), %r9d
	movl	328(%rsp), %edx         # 4-byte Reload
	movl	%edx, (%rsp)
	movsbl	%al, %esi
	movl	%ebx, %edi
	movl	%r12d, %edx
	callq	assign_enc_picture_params
	movzbl	140(%rsp), %eax
	cmpl	$2, %ebx
	je	.LBB0_43
# BB#41:                                #   in Loop: Header=BB0_21 Depth=2
	cmpl	$3, %ebx
	jne	.LBB0_44
# BB#42:                                #   in Loop: Header=BB0_21 Depth=2
	movb	%al, best8x8fwref+14(%r12)
	movb	%al, best8x8fwref+12(%r12)
	movb	135(%rsp), %dl
	movb	%dl, best8x8pdir+14(%r12)
	movb	%dl, best8x8pdir+12(%r12)
	movb	141(%rsp), %sil
	movb	%sil, best8x8bwref+14(%r12)
	movb	%sil, best8x8bwref+12(%r12)
	cmpq	$2, %rbx
	jge	.LBB0_46
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_43:                               #   in Loop: Header=BB0_21 Depth=2
	movb	%al, best8x8fwref+9(%r12,%r12)
	movb	%al, best8x8fwref+8(%r12,%r12)
	movb	135(%rsp), %dl
	movb	%dl, best8x8pdir+9(%r12,%r12)
	movb	%dl, best8x8pdir+8(%r12,%r12)
	movb	141(%rsp), %sil
	movb	%sil, best8x8bwref+9(%r12,%r12)
	movb	%sil, best8x8bwref+8(%r12,%r12)
	cmpq	$2, %rbx
	jge	.LBB0_46
	jmp	.LBB0_48
	.p2align	4, 0x90
.LBB0_44:                               #   in Loop: Header=BB0_21 Depth=2
	movzbl	%al, %ecx
	imull	$16843009, %ecx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8fwref+4(%rip)
	movzbl	141(%rsp), %esi
	imull	$16843009, %esi, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8bwref+4(%rip)
	movzbl	135(%rsp), %edx
	imull	$16843009, %edx, %ecx   # imm = 0x1010101
	movl	%ecx, best8x8pdir+4(%rip)
	cmpq	$2, %rbx
	jl	.LBB0_48
.LBB0_46:                               #   in Loop: Header=BB0_21 Depth=2
	testq	%r12, %r12
	jne	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_21 Depth=2
	movsbl	%al, %ecx
	movsbl	%dl, %edx
	movsbl	%sil, %r8d
	xorl	%edi, %edi
	movl	%ebx, %esi
	callq	SetRefAndMotionVectors
.LBB0_48:                               #   in Loop: Header=BB0_21 Depth=2
	incq	%r12
	cmpq	%rbp, %r12
	jl	.LBB0_21
# BB#49:                                #   in Loop: Header=BB0_19 Depth=1
	movq	176(%rsp), %rbp         # 8-byte Reload
	movl	$0, (%rbp)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	je	.LBB0_50
# BB#51:                                #   in Loop: Header=BB0_19 Depth=1
	movl	%ebx, %edi
	callq	SetModesAndRefframeForBlocks
	movl	$-1, %edi
	leaq	136(%rsp), %rsi
	callq	TransformDecision
	movl	%eax, %r12d
	movl	%r12d, (%rbp)
	jmp	.LBB0_52
	.p2align	4, 0x90
.LBB0_50:                               #   in Loop: Header=BB0_19 Depth=1
	xorl	%r12d, %r12d
.LBB0_52:                               #   in Loop: Header=BB0_19 Depth=1
	movl	136(%rsp), %ebp
	cmpl	200(%rsp), %ebp         # 4-byte Folded Reload
	jge	.LBB0_56
# BB#53:                                #   in Loop: Header=BB0_19 Depth=1
	movw	%bx, best_mode(%rip)
	movq	input(%rip), %rax
	cmpl	$1, 4172(%rax)
	jne	.LBB0_55
# BB#54:                                #   in Loop: Header=BB0_19 Depth=1
	movl	%ebp, %edi
	callq	adjust_mb16x16_cost
.LBB0_55:                               #   in Loop: Header=BB0_19 Depth=1
	movl	%r12d, %eax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movl	%ebp, 200(%rsp)         # 4-byte Spill
.LBB0_56:                               #   in Loop: Header=BB0_19 Depth=1
	incq	%rbx
	cmpq	$4, %rbx
	jne	.LBB0_19
# BB#57:
	cmpw	$0, 276(%rsp)
	je	.LBB0_77
# BB#58:
	movl	$1, giRDOpt_B8OnlyFlag(%rip)
	movl	$2147483647, tr8x8(%rip) # imm = 0x7FFFFFFF
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
	movq	cs_mb(%rip), %rdi
	callq	store_coding_state
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	movl	$-1, 468(%rax,%rcx)
	movq	input(%rip), %rax
	cmpl	$0, 5100(%rax)
	movq	320(%rsp), %rbp         # 8-byte Reload
	movl	156(%rsp), %ecx         # 4-byte Reload
	je	.LBB0_60
# BB#59:
	movl	$0, tr8x8(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 160(%rsp)
	movq	cofAC_8x8ts(%rip), %rax
	movq	(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	188(%rsp), %r15
	movq	%r15, 112(%rsp)
	leaq	136(%rsp), %r13
	movq	%r13, 104(%rsp)
	leaq	160(%rsp), %r12
	movq	%r12, 96(%rsp)
	movl	$1, 120(%rsp)
	movzwl	%cx, %ebx
	leaq	316(%rsp), %r14
	movl	$tr8x8, %edi
	xorl	%r9d, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr8x8+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr8x8+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr8x8+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	8(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	movq	%r13, 104(%rsp)
	movq	%r12, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr8x8+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr8x8+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr8x8+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	16(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	movq	%r13, 104(%rsp)
	movq	%r12, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr8x8+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr8x8+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr8x8+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr8x8+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC_8x8ts(%rip), %rax
	movq	24(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	movq	%r13, 104(%rsp)
	movq	%r12, 96(%rsp)
	movl	$1, 120(%rsp)
	movl	$tr8x8, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movl	156(%rsp), %ecx         # 4-byte Reload
	movzwl	tr8x8+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr8x8+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr8x8+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr8x8+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
	movl	cbp8x8(%rip), %eax
	movl	%eax, cbp8_8x8ts(%rip)
	movslq	cbp_blk8x8(%rip), %rax
	movq	%rax, cbp_blk8_8x8ts(%rip)
	movl	cnt_nonz_8x8(%rip), %eax
	movl	%eax, cnt_nonz8_8x8ts(%rip)
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movq	input(%rip), %rax
	cmpl	$2, 5100(%rax)
	je	.LBB0_61
.LBB0_60:                               # %.loopexit316.loopexit353
	movl	$0, tr4x4(%rip)
	movl	$0, cnt_nonz_8x8(%rip)
	movl	$0, cbp_blk8x8(%rip)
	movl	$0, cbp8x8(%rip)
	movl	$0, 160(%rsp)
	movq	cofAC8x8(%rip), %rax
	movq	(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	leaq	188(%rsp), %r15
	movq	%r15, 112(%rsp)
	leaq	136(%rsp), %r13
	movq	%r13, 104(%rsp)
	leaq	160(%rsp), %r12
	movq	%r12, 96(%rsp)
	movl	$0, 120(%rsp)
	movzwl	%cx, %ebx
	leaq	316(%rsp), %r14
	movl	$tr4x4, %edi
	xorl	%r9d, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6148(%rip), %eax
	movw	%ax, best8x8mode(%rip)
	movb	tr4x4+6156(%rip), %al
	movb	%al, best8x8pdir+32(%rip)
	movb	tr4x4+6160(%rip), %al
	movb	%al, best8x8fwref+32(%rip)
	movb	tr4x4+6164(%rip), %al
	movb	%al, best8x8bwref+32(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	8(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	movq	%r13, 104(%rsp)
	movq	%r12, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$1, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6150(%rip), %eax
	movw	%ax, best8x8mode+2(%rip)
	movb	tr4x4+6157(%rip), %al
	movb	%al, best8x8pdir+33(%rip)
	movb	tr4x4+6161(%rip), %al
	movb	%al, best8x8fwref+33(%rip)
	movb	tr4x4+6165(%rip), %al
	movb	%al, best8x8bwref+33(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	16(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movups	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	movq	%r13, 104(%rsp)
	movq	%r12, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$2, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6152(%rip), %eax
	movw	%ax, best8x8mode+4(%rip)
	movb	tr4x4+6158(%rip), %al
	movb	%al, best8x8pdir+34(%rip)
	movb	tr4x4+6162(%rip), %al
	movb	%al, best8x8fwref+34(%rip)
	movb	tr4x4+6166(%rip), %al
	movb	%al, best8x8bwref+34(%rip)
	movq	cofAC8x8(%rip), %rax
	movq	24(%rax), %rdx
	movups	296(%rsp), %xmm0
	movups	%xmm0, 80(%rsp)
	movups	280(%rsp), %xmm0
	movups	%xmm0, 64(%rsp)
	movupd	216(%rsp), %xmm0
	movups	232(%rsp), %xmm1
	movups	248(%rsp), %xmm2
	movups	264(%rsp), %xmm3
	movups	%xmm3, 48(%rsp)
	movups	%xmm2, 32(%rsp)
	movups	%xmm1, 16(%rsp)
	movupd	%xmm0, (%rsp)
	movq	%r15, 112(%rsp)
	movq	%r13, 104(%rsp)
	movq	%r12, 96(%rsp)
	movl	$0, 120(%rsp)
	movl	$tr4x4, %edi
	movl	$3, %r9d
	movq	%rbp, %rsi
	movq	%r14, %rcx
	movl	%ebx, %r8d
	callq	submacroblock_mode_decision
	movzwl	tr4x4+6154(%rip), %eax
	movw	%ax, best8x8mode+6(%rip)
	movb	tr4x4+6159(%rip), %al
	movb	%al, best8x8pdir+35(%rip)
	movb	tr4x4+6163(%rip), %al
	movb	%al, best8x8fwref+35(%rip)
	movb	tr4x4+6167(%rip), %al
	movb	%al, best8x8bwref+35(%rip)
.LBB0_61:                               # %.loopexit316
	movq	cs_mb(%rip), %rdi
	callq	reset_coding_state
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_63
# BB#62:
	movq	img(%rip), %rdx
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
.LBB0_63:
	movl	tr4x4(%rip), %eax
	movl	200(%rsp), %r14d        # 4-byte Reload
	cmpl	%r14d, %eax
	movl	tr8x8(%rip), %ecx
	movq	176(%rsp), %r13         # 8-byte Reload
	jl	.LBB0_65
# BB#64:
	cmpl	%r14d, %ecx
	jge	.LBB0_76
.LBB0_65:
	movw	$8, best_mode(%rip)
	movq	input(%rip), %rdx
	movl	5100(%rdx), %edx
	testl	%edx, %edx
	je	.LBB0_74
# BB#66:
	movl	$1, %ebx
	cmpl	$2, %edx
	jne	.LBB0_68
# BB#67:
	movl	%ecx, %eax
	jmp	.LBB0_75
.LBB0_77:
	movl	$2147483647, tr4x4(%rip) # imm = 0x7FFFFFFF
	movl	%r13d, %eax
	movq	192(%rsp), %r12         # 8-byte Reload
	movq	168(%rsp), %r15         # 8-byte Reload
	movq	176(%rsp), %r13         # 8-byte Reload
	movl	200(%rsp), %r14d        # 4-byte Reload
	cmpl	$3, %eax
	jne	.LBB0_79
	jmp	.LBB0_80
.LBB0_74:
	xorl	%ebx, %ebx
	jmp	.LBB0_75
.LBB0_68:
	cmpl	%eax, %ecx
	jge	.LBB0_70
# BB#69:
	movl	%ecx, %eax
	jmp	.LBB0_75
.LBB0_70:
	xorl	%ebx, %ebx
	cmpl	%ecx, %eax
	jl	.LBB0_75
# BB#71:
	callq	GetBestTransformP8x8
	testl	%eax, %eax
	je	.LBB0_72
# BB#73:
	movl	tr8x8(%rip), %eax
	movl	$1, %ebx
	jmp	.LBB0_75
.LBB0_72:
	movl	tr4x4(%rip), %eax
.LBB0_75:
	movl	%ebx, (%r13)
	movl	%eax, %r14d
.LBB0_76:
	movl	$0, giRDOpt_B8OnlyFlag(%rip)
	movq	192(%rsp), %r12         # 8-byte Reload
	movq	168(%rsp), %r15         # 8-byte Reload
	movl	184(%rsp), %eax         # 4-byte Reload
	cmpl	$3, %eax
	je	.LBB0_80
.LBB0_79:
	testl	%eax, %eax
	jne	.LBB0_81
.LBB0_80:
	callq	FindSkipModeMotionVector
.LBB0_81:
	movl	(%r13), %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movl	476(%r12,%r15), %ebx
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB0_83
# BB#82:
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	IntraChromaPrediction
.LBB0_83:
	movq	192(%rsp), %rax         # 8-byte Reload
	leaq	476(%rax,%r15), %rbp
	cmpl	$1, 184(%rsp)           # 4-byte Folded Reload
	setne	%al
	cmpw	$0, 260(%rsp)
	je	.LBB0_108
# BB#84:
	testb	%al, %al
	jne	.LBB0_108
# BB#85:
	cmpl	$0, 316(%rsp)
	je	.LBB0_94
# BB#86:
	movq	input(%rip), %rax
	movl	5100(%rax), %eax
	movl	%ebx, %r15d
	cmpl	$2, %eax
	je	.LBB0_92
# BB#87:
	cmpl	$1, %eax
	jne	.LBB0_93
# BB#88:
	movl	188(%rsp), %eax
	movl	160(%rsp), %ebx
	cmpl	%ebx, %eax
	jl	.LBB0_95
# BB#89:
	cmpw	$0, 270(%rsp)
	je	.LBB0_95
# BB#90:
	cmpw	$0, 272(%rsp)
	je	.LBB0_95
# BB#91:
	cmpw	$0, 274(%rsp)
	jne	.LBB0_96
	jmp	.LBB0_95
.LBB0_94:
	movl	%ebx, %r15d
	callq	GetDirectCostMB
.LBB0_95:
	movl	%eax, %ebx
	jmp	.LBB0_96
.LBB0_92:
	movl	188(%rsp), %ebx
	jmp	.LBB0_96
.LBB0_93:
	movl	160(%rsp), %ebx
.LBB0_96:
	movl	%ebx, 136(%rsp)
	movl	$2147483647, %eax       # imm = 0x7FFFFFFF
	cmpl	$2147483647, %ebx       # imm = 0x7FFFFFFF
	je	.LBB0_98
# BB#97:
	movsd	216(%rsp), %xmm0        # xmm0 = mem[0],zero
	mulsd	.LCPI0_0(%rip), %xmm0
	addsd	.LCPI0_1(%rip), %xmm0
	callq	floor
	cvttsd2si	%xmm0, %eax
	subl	%eax, %ebx
	movl	%ebx, 136(%rsp)
	movl	%ebx, %eax
.LBB0_98:
	cmpl	%r14d, %eax
	jle	.LBB0_99
# BB#107:
	movl	144(%rsp), %eax         # 4-byte Reload
	movl	%eax, (%r13)
	movl	%r15d, %ebx
	movl	%ebx, (%rbp)
	jmp	.LBB0_108
.LBB0_99:
	movq	active_sps(%rip), %rcx
	movl	$0, 144(%rsp)           # 4-byte Folded Spill
	cmpl	$0, 1156(%rcx)
	movq	input(%rip), %rcx
	movl	%r15d, %ebx
	je	.LBB0_104
# BB#100:
	movl	5100(%rcx), %edx
	testl	%edx, %edx
	je	.LBB0_104
# BB#101:
	cmpl	$2, %edx
	jne	.LBB0_103
# BB#102:
	movl	$1, 144(%rsp)           # 4-byte Folded Spill
	jmp	.LBB0_104
.LBB0_103:
	movl	188(%rsp), %edx
	xorl	%esi, %esi
	cmpl	160(%rsp), %edx
	setl	%sil
	movl	%esi, 144(%rsp)         # 4-byte Spill
.LBB0_104:                              # %._crit_edge375
	movl	144(%rsp), %edx         # 4-byte Reload
	movl	%edx, (%r13)
	cmpl	$0, 5116(%rcx)
	je	.LBB0_106
# BB#105:
	movq	img(%rip), %rdx
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
	movl	136(%rsp), %eax
	movl	(%r13), %ecx
	movl	%ecx, 144(%rsp)         # 4-byte Spill
.LBB0_106:
	movw	$0, best_mode(%rip)
	movl	%eax, %r14d
.LBB0_108:
	cmpw	$0, 286(%rsp)
	movq	%r13, 176(%rsp)         # 8-byte Spill
	movq	%rbp, 200(%rsp)         # 8-byte Spill
	je	.LBB0_120
# BB#109:
	movl	%ebx, %r15d
	movl	144(%rsp), %r12d        # 4-byte Reload
	movl	$1, (%r13)
	movq	192(%rsp), %rbx         # 8-byte Reload
	movl	%r14d, %ebp
	movq	168(%rsp), %r14         # 8-byte Reload
	movl	$13, 72(%rbx,%r14)
	movsd	216(%rsp), %xmm0        # xmm0 = mem[0],zero
	leaq	136(%rsp), %rdi
	callq	Mode_Decision_for_new_Intra8x8Macroblock
	movl	136(%rsp), %ecx
	cmpl	%ebp, %ecx
	jle	.LBB0_110
# BB#118:
	movl	%r12d, (%r13)
	movl	%ebp, %r14d
	jmp	.LBB0_119
.LBB0_110:
	movl	%eax, 364(%rbx,%r14)
	movq	input(%rip), %r8
	cmpl	$2, 5100(%r8)
	jne	.LBB0_112
# BB#111:                               # %..preheader315_crit_edge
	movq	img(%rip), %rdx
	jmp	.LBB0_113
.LBB0_112:
	movq	cofAC(%rip), %rsi
	movq	img(%rip), %rdx
	movq	14160(%rdx), %rdi
	movq	%rdi, cofAC(%rip)
	movq	%rsi, 14160(%rdx)
.LBB0_113:                              # %.preheader315
	movq	enc_picture(%rip), %rbp
	movslq	176(%rdx), %rsi
	movslq	180(%rdx), %rdi
	shlq	$3, %rdi
	addq	6440(%rbp), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_114:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbp), %rbx
	movzwl	(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY(,%rbp,4)
	movzwl	2(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+2(,%rbp,4)
	movzwl	4(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+4(,%rbp,4)
	movzwl	6(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+6(,%rbp,4)
	movzwl	8(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+8(,%rbp,4)
	movzwl	10(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+10(,%rbp,4)
	movzwl	12(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+12(,%rbp,4)
	movzwl	14(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+14(,%rbp,4)
	movzwl	16(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+16(,%rbp,4)
	movzwl	18(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+18(,%rbp,4)
	movzwl	20(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+20(,%rbp,4)
	movzwl	22(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+22(,%rbp,4)
	movzwl	24(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+24(,%rbp,4)
	movzwl	26(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+26(,%rbp,4)
	movzwl	28(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+28(,%rbp,4)
	movzwl	30(%rbx,%rsi,2), %eax
	movw	%ax, temp_imgY+30(,%rbp,4)
	addq	$8, %rbp
	cmpq	$128, %rbp
	jne	.LBB0_114
# BB#115:
	cmpl	$0, 5116(%r8)
	je	.LBB0_117
# BB#116:
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
	movl	136(%rsp), %ecx
.LBB0_117:
	movw	$13, best_mode(%rip)
	movq	176(%rsp), %r13         # 8-byte Reload
	movl	(%r13), %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movl	%ecx, %r14d
.LBB0_119:
	movl	%r15d, %ebx
	movq	200(%rsp), %rbp         # 8-byte Reload
.LBB0_120:
	cmpw	$0, 278(%rsp)
	je	.LBB0_127
# BB#121:
	movl	144(%rsp), %r12d        # 4-byte Reload
	movl	%ebx, 156(%rsp)         # 4-byte Spill
	movl	$0, (%r13)
	movq	192(%rsp), %rbx         # 8-byte Reload
	movq	168(%rsp), %rbp         # 8-byte Reload
	movl	$9, 72(%rbx,%rbp)
	movsd	216(%rsp), %xmm0        # xmm0 = mem[0],zero
	leaq	136(%rsp), %rdi
	callq	Mode_Decision_for_Intra4x4Macroblock
	movl	136(%rsp), %ecx
	cmpl	%r14d, %ecx
	jle	.LBB0_122
# BB#125:
	movl	%r12d, (%r13)
	movq	cofAC(%rip), %rax
	movq	img(%rip), %rcx
	movq	14160(%rcx), %rdx
	movq	%rdx, cofAC(%rip)
	movq	%rax, 14160(%rcx)
	jmp	.LBB0_126
.LBB0_122:
	movl	%eax, 364(%rbx,%rbp)
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_124
# BB#123:
	movq	img(%rip), %rdx
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
	movl	136(%rsp), %ecx
.LBB0_124:
	movw	$9, best_mode(%rip)
	movq	176(%rsp), %r13         # 8-byte Reload
	movl	(%r13), %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	movl	%ecx, %r14d
.LBB0_126:
	movq	200(%rsp), %rbp         # 8-byte Reload
	movl	156(%rsp), %ebx         # 4-byte Reload
.LBB0_127:
	cmpw	$0, 280(%rsp)
	je	.LBB0_133
# BB#128:
	movl	$0, (%r13)
	callq	intrapred_luma_16x16
	leaq	212(%rsp), %rdi
	callq	find_sad_16x16
	movl	%eax, 136(%rsp)
	cmpl	%r14d, %eax
	jge	.LBB0_132
# BB#129:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_131
# BB#130:
	movq	img(%rip), %rax
	movl	192(%rax), %edi
	movl	196(%rax), %esi
	movslq	212(%rsp), %rcx
	shlq	$9, %rcx
	leaq	4816(%rax,%rcx), %rdx
	callq	rc_store_diff
.LBB0_131:
	movw	$10, best_mode(%rip)
	movl	212(%rsp), %edi
	callq	dct_luma_16x16
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	168(%rsp), %rdx         # 8-byte Reload
	movl	%eax, 364(%rcx,%rdx)
	jmp	.LBB0_133
.LBB0_132:
	movl	144(%rsp), %eax         # 4-byte Reload
	movl	%eax, (%r13)
	movl	%ebx, (%rbp)
.LBB0_133:
	movq	192(%rsp), %rbp         # 8-byte Reload
	movq	168(%rsp), %rax         # 8-byte Reload
	movl	72(%rbp,%rax), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%r13b, %r13b
	movb	$51, %r15b
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %r15b
	movswl	best_mode(%rip), %edi
	callq	SetModesAndRefframeForBlocks
	movzwl	best_mode(%rip), %eax
	cmpl	$13, %eax
	je	.LBB0_141
# BB#134:
	movzwl	%ax, %ecx
	cmpl	$8, %ecx
	movq	376(%rsp), %rbx         # 8-byte Reload
	jne	.LBB0_146
# BB#135:
	movq	176(%rsp), %rax         # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB0_139
# BB#136:
	movl	cbp8_8x8ts(%rip), %eax
	testl	%eax, %eax
	jne	.LBB0_139
# BB#137:
	movq	input(%rip), %rax
	cmpl	$2, 5100(%rax)
	je	.LBB0_139
# BB#138:
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
.LBB0_139:                              # %.lr.ph322.preheader
	movq	320(%rsp), %rdi         # 8-byte Reload
	callq	SetCoeffAndReconstruction8x8
	movapd	.LCPI0_2(%rip), %xmm0   # xmm0 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	movq	168(%rsp), %rax         # 8-byte Reload
	movupd	%xmm0, 332(%rbp,%rax)
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rax
	decq	%rax
	.p2align	4, 0x90
.LBB0_140:                              # %.lr.ph322
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movl	$33686018, (%rdx,%rcx)  # imm = 0x2020202
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	addq	$3, %rdx
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB0_140
	jmp	.LBB0_157
.LBB0_141:                              # %.lr.ph326.preheader
	movq	168(%rsp), %rax         # 8-byte Reload
	movups	348(%rbp,%rax), %xmm0
	movups	%xmm0, 332(%rbp,%rax)
	movq	img(%rip), %rax
	movslq	172(%rax), %rcx
	decq	%rcx
	movq	376(%rsp), %rbx         # 8-byte Reload
	.p2align	4, 0x90
.LBB0_142:                              # %.lr.ph326
                                        # =>This Inner Loop Header: Depth=1
	movq	128(%rax), %rdx
	movq	136(%rax), %rsi
	movq	8(%rdx,%rcx,8), %rdx
	movslq	168(%rax), %rax
	movq	8(%rsi,%rcx,8), %rsi
	movl	(%rsi,%rax), %esi
	movl	%esi, (%rdx,%rax)
	movq	img(%rip), %rax
	movslq	172(%rax), %rdx
	addq	$3, %rdx
	incq	%rcx
	cmpq	%rdx, %rcx
	jl	.LBB0_142
# BB#143:                               # %.preheader.preheader
	movq	enc_picture(%rip), %rcx
	movq	6440(%rcx), %rcx
	movslq	180(%rax), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	176(%rax), %rax
	movaps	temp_imgY+16(%rip), %xmm0
	movups	%xmm0, 16(%rcx,%rax,2)
	movaps	temp_imgY(%rip), %xmm0
	movups	%xmm0, (%rcx,%rax,2)
	movq	$-14, %rax
	movl	$temp_imgY+64, %ecx
	jmp	.LBB0_144
	.p2align	4, 0x90
.LBB0_196:                              # %.preheader..preheader_crit_edge.1
                                        #   in Loop: Header=BB0_144 Depth=1
	movq	img(%rip), %rdx
	movq	enc_picture(%rip), %rsi
	movq	6440(%rsi), %rsi
	movl	180(%rdx), %edi
	leal	16(%rax,%rdi), %edi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	176(%rdx), %rdx
	movups	(%rcx), %xmm0
	movups	16(%rcx), %xmm1
	movups	%xmm1, 16(%rsi,%rdx,2)
	movups	%xmm0, (%rsi,%rdx,2)
	addq	$64, %rcx
	addq	$2, %rax
.LBB0_144:                              # %.preheader..preheader_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	img(%rip), %rdx
	movq	enc_picture(%rip), %rsi
	movq	6440(%rsi), %rsi
	movl	180(%rdx), %edi
	leal	15(%rax,%rdi), %edi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	176(%rdx), %rdx
	movupd	-32(%rcx), %xmm0
	movups	-16(%rcx), %xmm1
	movups	%xmm1, 16(%rsi,%rdx,2)
	movupd	%xmm0, (%rsi,%rdx,2)
	testq	%rax, %rax
	jne	.LBB0_196
# BB#145:                               # %thread-pre-split
	movw	best_mode(%rip), %ax
.LBB0_146:
	orl	$4, %eax
	movzwl	%ax, %eax
	cmpl	$13, %eax
	jne	.LBB0_147
.LBB0_157:                              # %.loopexit
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	72(%rbp,%rax), %r12
	testb	$15, 364(%rbp,%rax)
	movq	176(%rsp), %rcx         # 8-byte Reload
	jne	.LBB0_160
# BB#158:
	movl	(%r12), %eax
	orl	$4, %eax
	cmpl	$13, %eax
	je	.LBB0_160
# BB#159:
	movl	$0, (%rcx)
.LBB0_160:
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	je	.LBB0_161
# BB#162:
	xorl	%edi, %edi
	xorl	%esi, %esi
	xorl	%edx, %edx
	callq	IntraChromaPrediction
	movq	img(%rip), %rax
	cmpl	$0, 15536(%rax)
	movl	$0, 15244(%rax)
	movl	$0, 332(%rsp)
	je	.LBB0_164
# BB#163:
	leaq	332(%rsp), %rdi
	callq	ChromaResidualCoding
	jmp	.LBB0_164
.LBB0_161:                              # %.thread391
	movl	$0, 15244(%rax)
	movl	$0, 332(%rsp)
.LBB0_164:
	movb	167(%rsp), %al          # 1-byte Reload
	orb	%al, 143(%rsp)          # 1-byte Folded Spill
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	364(%rax,%rcx), %rbp
	movzwl	best_mode(%rip), %eax
	cmpl	$10, %eax
	jne	.LBB0_166
# BB#165:
	movl	(%rbp), %edi
	movl	212(%rsp), %esi
	callq	I16Offset
	movq	img(%rip), %rcx
	movl	%eax, 15244(%rcx)
.LBB0_166:
	movq	320(%rsp), %rdi         # 8-byte Reload
	movl	328(%rsp), %esi         # 4-byte Reload
	callq	SetMotionVectorsMB
	movzwl	best_mode(%rip), %eax
	cmpl	$1, %eax
	sete	%al
	movb	143(%rsp), %cl          # 1-byte Reload
	andb	%al, %cl
	cmpb	$1, %cl
	jne	.LBB0_172
# BB#167:
	cmpl	$0, (%rbp)
	jne	.LBB0_172
# BB#168:
	movq	enc_picture(%rip), %rax
	movq	6488(%rax), %rcx
	movq	(%rcx), %rdx
	movq	img(%rip), %rsi
	movslq	172(%rsi), %rcx
	movq	(%rdx,%rcx,8), %rdi
	movslq	168(%rsi), %rdx
	cmpb	$0, (%rdi,%rdx)
	jne	.LBB0_172
# BB#169:
	movq	6512(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%rdx,8), %rax
	movzwl	(%rax), %ecx
	movq	368(%rsp), %rdx         # 8-byte Reload
	cmpw	(%rdx), %cx
	jne	.LBB0_172
# BB#170:
	movzwl	2(%rax), %eax
	movq	368(%rsp), %rcx         # 8-byte Reload
	cmpw	2(%rcx), %ax
	jne	.LBB0_172
# BB#171:
	movl	$0, (%r12)
	xorpd	%xmm0, %xmm0
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	168(%rsp), %rcx         # 8-byte Reload
	movupd	%xmm0, 376(%rax,%rcx)
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
.LBB0_172:
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	je	.LBB0_174
# BB#173:
	callq	set_mbaff_parameters
.LBB0_174:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	movq	320(%rsp), %rbx         # 8-byte Reload
	je	.LBB0_176
# BB#175:
	movswl	best_mode(%rip), %esi
	movq	%rbx, %rdi
	callq	update_rc
.LBB0_176:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r14d, %xmm0
	movq	rdopt(%rip), %rax
	movsd	%xmm0, (%rax)
	movq	img(%rip), %rax
	cmpl	$0, 15268(%rax)
	je	.LBB0_186
# BB#177:
	testb	$1, 12(%rax)
	je	.LBB0_186
# BB#178:
	cmpl	$0, (%r12)
	jne	.LBB0_186
# BB#179:
	cmpl	$1, 184(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_181
# BB#180:
	cmpl	$0, (%rbp)
	jne	.LBB0_186
.LBB0_181:
	movq	344(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 72(%rax)
	je	.LBB0_182
.LBB0_186:
	movq	input(%rip), %rax
	cmpl	$0, 4732(%rax)
	je	.LBB0_188
# BB#187:
	andb	%r15b, %r13b
	andb	$1, %r13b
	movzbl	%r13b, %esi
	movl	352(%rsp), %edi         # 4-byte Reload
	movq	%rbx, %rdx
	callq	update_refresh_map
	movq	input(%rip), %rax
.LBB0_188:
	movl	5244(%rax), %eax
	cmpl	$2, %eax
	je	.LBB0_191
# BB#189:
	cmpl	$1, %eax
	jne	.LBB0_192
# BB#190:
	movswl	best_mode(%rip), %edi
	movswq	290(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	UMHEX_skip_intrabk_SAD
	jmp	.LBB0_192
.LBB0_191:
	movswl	best_mode(%rip), %edi
	movswq	290(%rsp), %rax
	movl	listXsize(,%rax,4), %esi
	callq	smpUMHEX_skip_intrabk_SAD
.LBB0_192:
	movq	input(%rip), %rax
	cmpl	$0, 272(%rax)
	je	.LBB0_195
# BB#193:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	ja	.LBB0_195
# BB#194:
	movl	(%r12), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%dl, %dl
	movb	$51, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	andb	%dl, %bl
	andb	$1, %bl
	movzbl	%bl, %ecx
	movq	14240(%rax), %rdx
	movslq	12(%rax), %rax
	movl	%ecx, (%rdx,%rax,4)
.LBB0_195:
	addq	$408, %rsp              # imm = 0x198
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_147:                              # %.lr.ph.preheader
	movapd	.LCPI0_2(%rip), %xmm0   # xmm0 = [2,2,2,2,2,2,2,2,2,2,2,2,2,2,2,2]
	movq	168(%rsp), %rax         # 8-byte Reload
	movupd	%xmm0, 332(%rbp,%rax)
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rax
	decq	%rax
	.p2align	4, 0x90
.LBB0_148:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx,%rax,8), %rdx
	movslq	168(%rcx), %rcx
	movl	$33686018, (%rdx,%rcx)  # imm = 0x2020202
	movq	img(%rip), %rcx
	movslq	172(%rcx), %rdx
	addq	$3, %rdx
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB0_148
# BB#149:                               # %._crit_edge
	movzwl	best_mode(%rip), %eax
	cmpl	$10, %eax
	je	.LBB0_157
# BB#150:
	decl	%eax
	movzwl	%ax, %eax
	cmpl	$2, %eax
	ja	.LBB0_152
# BB#151:
	movq	336(%rsp), %rax         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	movl	%eax, (%rcx)
.LBB0_152:
	callq	LumaResidualCoding
	movq	168(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 364(%rbp,%rax)
	jne	.LBB0_155
# BB#153:
	movzwl	best_mode(%rip), %eax
	testw	%ax, %ax
	jne	.LBB0_155
# BB#154:
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
.LBB0_155:
	movq	input(%rip), %rax
	cmpl	$0, 5116(%rax)
	je	.LBB0_157
# BB#156:
	movq	img(%rip), %rdx
	movl	192(%rdx), %edi
	movl	196(%rdx), %esi
	addq	$12624, %rdx            # imm = 0x3150
	callq	rc_store_diff
	jmp	.LBB0_157
.LBB0_182:
	cmpl	$1, 184(%rsp)           # 4-byte Folded Reload
	jne	.LBB0_184
# BB#183:
	movq	344(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 364(%rax)
	jne	.LBB0_186
.LBB0_184:
	callq	field_flag_inference
	movswl	294(%rsp), %ecx
	cmpl	%ecx, %eax
	je	.LBB0_186
# BB#185:
	movq	rdopt(%rip), %rax
	movabsq	$5055640609639927018, %rcx # imm = 0x46293E5939A08CEA
	movq	%rcx, (%rax)
	jmp	.LBB0_186
.Lfunc_end0:
	.size	encode_one_macroblock_low, .Lfunc_end0-encode_one_macroblock_low
	.cfi_endproc

	.type	.Lencode_one_macroblock_low.bmcost,@object # @encode_one_macroblock_low.bmcost
	.section	.rodata,"a",@progbits
	.p2align	4
.Lencode_one_macroblock_low.bmcost:
	.long	2147483647              # 0x7fffffff
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lencode_one_macroblock_low.bmcost, 20

	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	temp_imgY,@object       # @temp_imgY
	.local	temp_imgY
	.comm	temp_imgY,512,16
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	diffy,@object           # @diffy
	.comm	diffy,1024,16
	.type	qp_mbaff,@object        # @qp_mbaff
	.comm	qp_mbaff,16,16
	.type	delta_qp_mbaff,@object  # @delta_qp_mbaff
	.comm	delta_qp_mbaff,16,16
	.type	generic_RC,@object      # @generic_RC
	.comm	generic_RC,8,8
	.type	generic_RC_init,@object # @generic_RC_init
	.comm	generic_RC_init,8,8
	.type	generic_RC_best,@object # @generic_RC_best
	.comm	generic_RC_best,8,8
	.type	McostState,@object      # @McostState
	.comm	McostState,8,8
	.type	SearchState,@object     # @SearchState
	.comm	SearchState,8,8
	.type	fastme_ref_cost,@object # @fastme_ref_cost
	.comm	fastme_ref_cost,8,8
	.type	fastme_l0_cost,@object  # @fastme_l0_cost
	.comm	fastme_l0_cost,8,8
	.type	fastme_l1_cost,@object  # @fastme_l1_cost
	.comm	fastme_l1_cost,8,8
	.type	fastme_l0_cost_bipred,@object # @fastme_l0_cost_bipred
	.comm	fastme_l0_cost_bipred,8,8
	.type	fastme_l1_cost_bipred,@object # @fastme_l1_cost_bipred
	.comm	fastme_l1_cost_bipred,8,8
	.type	bipred_flag,@object     # @bipred_flag
	.comm	bipred_flag,4,4
	.type	fastme_best_cost,@object # @fastme_best_cost
	.comm	fastme_best_cost,8,8
	.type	pred_SAD,@object        # @pred_SAD
	.comm	pred_SAD,4,4
	.type	pred_MV_ref,@object     # @pred_MV_ref
	.comm	pred_MV_ref,8,4
	.type	pred_MV_uplayer,@object # @pred_MV_uplayer
	.comm	pred_MV_uplayer,8,4
	.type	UMHEX_blocktype,@object # @UMHEX_blocktype
	.comm	UMHEX_blocktype,4,4
	.type	predict_point,@object   # @predict_point
	.comm	predict_point,40,16
	.type	SAD_a,@object           # @SAD_a
	.comm	SAD_a,4,4
	.type	SAD_b,@object           # @SAD_b
	.comm	SAD_b,4,4
	.type	SAD_c,@object           # @SAD_c
	.comm	SAD_c,4,4
	.type	SAD_d,@object           # @SAD_d
	.comm	SAD_d,4,4
	.type	Threshold_DSR_MB,@object # @Threshold_DSR_MB
	.comm	Threshold_DSR_MB,32,16
	.type	Bsize,@object           # @Bsize
	.comm	Bsize,32,16
	.type	AlphaFourth_1,@object   # @AlphaFourth_1
	.comm	AlphaFourth_1,32,16
	.type	AlphaFourth_2,@object   # @AlphaFourth_2
	.comm	AlphaFourth_2,32,16
	.type	flag_intra,@object      # @flag_intra
	.comm	flag_intra,8,8
	.type	flag_intra_SAD,@object  # @flag_intra_SAD
	.comm	flag_intra_SAD,4,4
	.type	SymmetricalCrossSearchThreshold1,@object # @SymmetricalCrossSearchThreshold1
	.comm	SymmetricalCrossSearchThreshold1,2,2
	.type	SymmetricalCrossSearchThreshold2,@object # @SymmetricalCrossSearchThreshold2
	.comm	SymmetricalCrossSearchThreshold2,2,2
	.type	ConvergeThreshold,@object # @ConvergeThreshold
	.comm	ConvergeThreshold,2,2
	.type	SubPelThreshold1,@object # @SubPelThreshold1
	.comm	SubPelThreshold1,2,2
	.type	SubPelThreshold3,@object # @SubPelThreshold3
	.comm	SubPelThreshold3,2,2
	.type	smpUMHEX_SearchState,@object # @smpUMHEX_SearchState
	.comm	smpUMHEX_SearchState,8,8
	.type	smpUMHEX_l0_cost,@object # @smpUMHEX_l0_cost
	.comm	smpUMHEX_l0_cost,8,8
	.type	smpUMHEX_l1_cost,@object # @smpUMHEX_l1_cost
	.comm	smpUMHEX_l1_cost,8,8
	.type	smpUMHEX_flag_intra,@object # @smpUMHEX_flag_intra
	.comm	smpUMHEX_flag_intra,8,8
	.type	smpUMHEX_flag_intra_SAD,@object # @smpUMHEX_flag_intra_SAD
	.comm	smpUMHEX_flag_intra_SAD,4,4
	.type	smpUMHEX_pred_SAD_uplayer,@object # @smpUMHEX_pred_SAD_uplayer
	.comm	smpUMHEX_pred_SAD_uplayer,4,4
	.type	smpUMHEX_pred_MV_uplayer_X,@object # @smpUMHEX_pred_MV_uplayer_X
	.comm	smpUMHEX_pred_MV_uplayer_X,2,2
	.type	smpUMHEX_pred_MV_uplayer_Y,@object # @smpUMHEX_pred_MV_uplayer_Y
	.comm	smpUMHEX_pred_MV_uplayer_Y,2,2

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
