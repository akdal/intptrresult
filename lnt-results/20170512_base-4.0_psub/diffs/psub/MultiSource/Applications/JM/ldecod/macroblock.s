	.text
	.file	"macroblock.bc"
	.globl	start_macroblock
	.p2align	4, 0x90
	.type	start_macroblock,@function
start_macroblock:                       # @start_macroblock
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r12, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	5600(%rbx), %r15
	movl	4(%rbx), %ecx
	cmpl	$0, 5624(%rbx)
	je	.LBB0_2
# BB#1:
	movl	48(%rbx), %eax
	movl	%eax, %esi
	sarl	$31, %esi
	shrl	$29, %esi
	addl	%eax, %esi
	sarl	$3, %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%edx, %esi
	shrl	$31, %esi
	addl	%edx, %esi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	andl	$1, %edx
	leal	(%rdx,%rax,2), %eax
	sarl	%esi
	movl	%esi, 72(%rbx)
	jmp	.LBB0_3
.LBB0_2:
	movq	PicPos(%rip), %rax
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %esi
	movl	%esi, 72(%rbx)
	movl	4(%rax), %eax
.LBB0_3:
	movl	%eax, 68(%rbx)
	leal	(,%rax,4), %edx
	movl	%edx, 76(%rbx)
	movl	%eax, %edx
	shll	$4, %edx
	movl	%edx, 80(%rbx)
	movl	5936(%rbx), %edx
	imull	%eax, %edx
	movl	%edx, 88(%rbx)
	leal	(,%rsi,4), %edx
	movl	%edx, 92(%rbx)
	movl	%esi, %edx
	shll	$4, %edx
	movl	%edx, 84(%rbx)
	movl	5932(%rbx), %edx
	imull	%esi, %edx
	movl	%edx, 96(%rbx)
	movl	12(%rbx), %edx
	imulq	$408, %rcx, %r12        # imm = 0x198
	movl	%edx, 12(%r15,%r12)
	cmpl	$50, %edx
	jl	.LBB0_5
# BB#4:
	movl	$.L.str, %edi
	movl	$200, %esi
	callq	error
	movl	12(%rbx), %edx
	movl	68(%rbx), %eax
	movl	72(%rbx), %esi
.LBB0_5:
	movq	dec_picture(%rip), %rcx
	movq	316944(%rcx), %rdi
	cltq
	movq	(%rdi,%rax,8), %rax
	movslq	%esi, %rsi
	movw	%dx, (%rax,%rsi,2)
	movswl	316860(%rcx), %eax
	cmpl	%eax, %edx
	jle	.LBB0_7
# BB#6:
	movw	%dx, 316860(%rcx)
.LBB0_7:
	callq	CheckAvailabilityOfNeighbors
	movl	28(%rbx), %eax
	movl	%eax, (%r15,%r12)
	leaq	40(%r15,%r12), %rdi
	movl	$0, 16(%r15,%r12)
	movl	$0, 352(%r15,%r12)
	leaq	1384(%rbx), %r14
	xorl	%esi, %esi
	movl	$280, %edx              # imm = 0x118
	callq	memset
	xorl	%esi, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r14, %rdi
	callq	memset
	movq	5592(%rbx), %rax
	movl	136(%rax), %ecx
	movl	%ecx, 340(%r15,%r12)
	movl	140(%rax), %ecx
	movl	%ecx, 344(%r15,%r12)
	movl	144(%rax), %eax
	movl	%eax, 348(%r15,%r12)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	start_macroblock, .Lfunc_end0-start_macroblock
	.cfi_endproc

	.globl	exit_macroblock
	.p2align	4, 0x90
	.type	exit_macroblock,@function
exit_macroblock:                        # @exit_macroblock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -32
.Lcfi13:
	.cfi_offset %r14, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rdi, %rbx
	movl	8(%rbx), %eax
	incl	%eax
	movl	%eax, 8(%rbx)
	movl	$1, %r14d
	cmpl	5836(%rbx), %eax
	je	.LBB1_8
# BB#1:
	movl	4(%rbx), %edi
	callq	FmoGetNextMBNr
	movl	%eax, 4(%rbx)
	cmpl	$-1, %eax
	je	.LBB1_8
# BB#2:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	*nal_startcode_follows(%rip)
	testl	%eax, %eax
	je	.LBB1_3
# BB#4:
	movl	44(%rbx), %eax
	cmpl	$2, %eax
	je	.LBB1_8
# BB#5:
	cmpl	$4, %eax
	je	.LBB1_8
# BB#6:
	movq	active_pps(%rip), %rax
	cmpl	$1, 12(%rax)
	je	.LBB1_8
# BB#7:
	xorl	%r14d, %r14d
	cmpl	$0, 5576(%rbx)
	setle	%r14b
	jmp	.LBB1_8
.LBB1_3:
	xorl	%r14d, %r14d
.LBB1_8:
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	exit_macroblock, .Lfunc_end1-exit_macroblock
	.cfi_endproc

	.globl	interpret_mb_mode_P
	.p2align	4, 0x90
	.type	interpret_mb_mode_P,@function
interpret_mb_mode_P:                    # @interpret_mb_mode_P
	.cfi_startproc
# BB#0:
	movq	5600(%rdi), %rax
	movl	4(%rdi), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movl	40(%rax,%rcx), %esi
	cmpl	$3, %esi
	jg	.LBB2_2
# BB#1:
	movzbl	%sil, %edx
	imull	$16843009, %edx, %edx   # imm = 0x1010101
	movl	%edx, 328(%rax,%rcx)
	movl	$0, 332(%rax,%rcx)
	retq
.LBB2_2:
	leaq	40(%rax,%rcx), %r8
	movl	%esi, %edx
	orl	$1, %edx
	cmpl	$5, %edx
	jne	.LBB2_4
# BB#3:
	xorl	%eax, %eax
	cmpl	$5, %esi
	sete	%al
	movl	$8, (%r8)
	movl	%eax, 100(%rdi)
	retq
.LBB2_4:
	cmpl	$31, %esi
	je	.LBB2_7
# BB#5:
	cmpl	$6, %esi
	jne	.LBB2_8
# BB#6:
	movl	$9, (%r8)
	movabsq	$-4109694197, %rdx      # imm = 0xFFFFFFFF0B0B0B0B
	movq	%rdx, 328(%rax,%rcx)
	retq
.LBB2_7:
	movl	$14, (%r8)
	movl	$-1, 300(%rax,%rcx)
	movl	$0, 324(%rax,%rcx)
	jmp	.LBB2_9
.LBB2_8:
	movl	$10, (%r8)
	addl	$-7, %esi
	movl	%esi, %edx
	sarl	$2, %edx
	movslq	%edx, %rdx
	movl	interpret_mb_mode_SI.ICBPTAB(,%rdx,4), %edx
	movl	%edx, 300(%rax,%rcx)
	andl	$3, %esi
	movl	%esi, 324(%rax,%rcx)
.LBB2_9:
	movl	$0, 328(%rax,%rcx)
	movl	$-1, 332(%rax,%rcx)
	retq
.Lfunc_end2:
	.size	interpret_mb_mode_P, .Lfunc_end2-interpret_mb_mode_P
	.cfi_endproc

	.globl	interpret_mb_mode_I
	.p2align	4, 0x90
	.type	interpret_mb_mode_I,@function
interpret_mb_mode_I:                    # @interpret_mb_mode_I
	.cfi_startproc
# BB#0:
	movq	5600(%rdi), %rax
	movl	4(%rdi), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	leaq	40(%rax,%rcx), %rsi
	movl	40(%rax,%rcx), %edx
	cmpl	$25, %edx
	je	.LBB3_3
# BB#1:
	testl	%edx, %edx
	jne	.LBB3_4
# BB#2:
	movl	$9, (%rsi)
	movl	$185273099, %edx        # imm = 0xB0B0B0B
	jmp	.LBB3_6
.LBB3_3:
	movl	$14, (%rsi)
	movl	$-1, 300(%rax,%rcx)
	movl	$0, 324(%rax,%rcx)
	jmp	.LBB3_5
.LBB3_4:
	movl	$10, (%rsi)
	decl	%edx
	movl	%edx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movl	interpret_mb_mode_SI.ICBPTAB(,%rsi,4), %esi
	movl	%esi, 300(%rax,%rcx)
	andl	$3, %edx
	movl	%edx, 324(%rax,%rcx)
.LBB3_5:
	xorl	%edx, %edx
.LBB3_6:
	movl	%edx, 328(%rax,%rcx)
	movl	$-1, 332(%rax,%rcx)
	retq
.Lfunc_end3:
	.size	interpret_mb_mode_I, .Lfunc_end3-interpret_mb_mode_I
	.cfi_endproc

	.globl	interpret_mb_mode_B
	.p2align	4, 0x90
	.type	interpret_mb_mode_B,@function
interpret_mb_mode_B:                    # @interpret_mb_mode_B
	.cfi_startproc
# BB#0:
	movq	5600(%rdi), %rcx
	movl	4(%rdi), %eax
	imulq	$408, %rax, %rdx        # imm = 0x198
	leaq	40(%rcx,%rdx), %rax
	movslq	40(%rcx,%rdx), %rsi
	cmpq	$23, %rsi
	je	.LBB4_3
# BB#1:
	testl	%esi, %esi
	jne	.LBB4_4
# BB#2:
	movabsq	$144680345642467328, %rsi # imm = 0x202020200000000
	movq	%rsi, 328(%rcx,%rdx)
	xorl	%edi, %edi
	movl	%edi, (%rax)
	retq
.LBB4_3:
	movabsq	$-4109694197, %rsi      # imm = 0xFFFFFFFF0B0B0B0B
	movq	%rsi, 328(%rcx,%rdx)
	movl	$9, %edi
	movl	%edi, (%rax)
	retq
.LBB4_4:
	leal	-24(%rsi), %edi
	cmpl	$23, %edi
	ja	.LBB4_6
# BB#5:
	movl	$0, 328(%rcx,%rdx)
	movl	$-1, 332(%rcx,%rdx)
	movl	%edi, %esi
	andl	$-4, %esi
	movl	interpret_mb_mode_SI.ICBPTAB(%rsi), %esi
	movl	%esi, 300(%rcx,%rdx)
	andl	$3, %edi
	movl	%edi, 324(%rcx,%rdx)
	movl	$10, %edi
	movl	%edi, (%rax)
	retq
.LBB4_6:
	movl	$8, %edi
	cmpl	$22, %esi
	jne	.LBB4_7
# BB#14:                                # %.loopexit
	movl	%edi, (%rax)
	retq
.LBB4_7:
	cmpl	$3, %esi
	jg	.LBB4_9
# BB#8:
	movl	$16843009, 328(%rcx,%rdx) # imm = 0x1010101
	movzbl	interpret_mb_mode_B.offset2pdir16x16(,%rsi,4), %esi
	imull	$16843009, %esi, %esi   # imm = 0x1010101
	movl	%esi, 332(%rcx,%rdx)
	movl	$1, %edi
	movl	%edi, (%rax)
	retq
.LBB4_9:
	cmpl	$48, %esi
	jne	.LBB4_11
# BB#10:
	movl	$0, 328(%rcx,%rdx)
	movl	$-1, 332(%rcx,%rdx)
	movl	$-1, 300(%rcx,%rdx)
	movl	$0, 324(%rcx,%rdx)
	movl	$14, %edi
	movl	%edi, (%rax)
	retq
.LBB4_11:
	leaq	328(%rcx,%rdx), %rdi
	testb	$1, %sil
	jne	.LBB4_13
# BB#12:                                # %.loopexit.loopexit56
	movl	$33686018, (%rdi)       # imm = 0x2020202
	movb	interpret_mb_mode_B.offset2pdir16x8(,%rsi,8), %dil
	movb	%dil, 332(%rcx,%rdx)
	movb	%dil, 333(%rcx,%rdx)
	movb	interpret_mb_mode_B.offset2pdir16x8+4(,%rsi,8), %sil
	movb	%sil, 334(%rcx,%rdx)
	movb	%sil, 335(%rcx,%rdx)
	movl	$2, %edi
	movl	%edi, (%rax)
	retq
.LBB4_13:                               # %.loopexit.loopexit5557
	movl	$50529027, (%rdi)       # imm = 0x3030303
	movb	interpret_mb_mode_B.offset2pdir8x16(,%rsi,8), %dil
	movb	%dil, 332(%rcx,%rdx)
	movb	interpret_mb_mode_B.offset2pdir8x16+4(,%rsi,8), %sil
	movb	%sil, 333(%rcx,%rdx)
	movb	%dil, 334(%rcx,%rdx)
	movb	%sil, 335(%rcx,%rdx)
	movl	$3, %edi
	movl	%edi, (%rax)
	retq
.Lfunc_end4:
	.size	interpret_mb_mode_B, .Lfunc_end4-interpret_mb_mode_B
	.cfi_endproc

	.globl	interpret_mb_mode_SI
	.p2align	4, 0x90
	.type	interpret_mb_mode_SI,@function
interpret_mb_mode_SI:                   # @interpret_mb_mode_SI
	.cfi_startproc
# BB#0:
	movq	5600(%rdi), %rax
	movl	4(%rdi), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	leaq	40(%rax,%rcx), %rsi
	movl	40(%rax,%rcx), %edx
	cmpl	$26, %edx
	je	.LBB5_5
# BB#1:
	cmpl	$1, %edx
	je	.LBB5_4
# BB#2:
	testl	%edx, %edx
	jne	.LBB5_6
# BB#3:
	movl	$12, (%rsi)
	movabsq	$-4109694197, %rdx      # imm = 0xFFFFFFFF0B0B0B0B
	movq	%rdx, 328(%rax,%rcx)
	movq	5568(%rdi), %rax
	movslq	68(%rdi), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	72(%rdi), %rcx
	movl	$1, (%rax,%rcx,4)
	retq
.LBB5_5:
	movl	$14, (%rsi)
	movl	$-1, 300(%rax,%rcx)
	movl	$0, 324(%rax,%rcx)
	jmp	.LBB5_7
.LBB5_4:
	movl	$9, (%rsi)
	movabsq	$-4109694197, %rdx      # imm = 0xFFFFFFFF0B0B0B0B
	movq	%rdx, 328(%rax,%rcx)
	retq
.LBB5_6:
	movl	$10, (%rsi)
	leal	-1(%rdx), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movl	interpret_mb_mode_SI.ICBPTAB(,%rsi,4), %esi
	movl	%esi, 300(%rax,%rcx)
	addl	$2, %edx
	andl	$3, %edx
	movl	%edx, 324(%rax,%rcx)
.LBB5_7:
	movl	$0, 328(%rax,%rcx)
	movl	$-1, 332(%rax,%rcx)
	retq
.Lfunc_end5:
	.size	interpret_mb_mode_SI, .Lfunc_end5-interpret_mb_mode_SI
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	-9223372036854775808    # 0x8000000000000000
	.quad	-9223372036854775808    # 0x8000000000000000
	.text
	.globl	init_macroblock
	.p2align	4, 0x90
	.type	init_macroblock,@function
init_macroblock:                        # @init_macroblock
	.cfi_startproc
# BB#0:                                 # %.lr.ph29
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi19:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi20:
	.cfi_def_cfa_offset 56
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movslq	76(%rdi), %r10
	xorps	%xmm0, %xmm0
	movl	$4, %r8d
	xorl	%r9d, %r9d
	movabsq	$-9223372036854775808, %r11 # imm = 0x8000000000000000
	movaps	.LCPI6_0(%rip), %xmm1   # xmm1 = [9223372036854775808,9223372036854775808]
	.p2align	4, 0x90
.LBB6_1:                                # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_8 Depth 2
                                        #     Child Loop BB6_11 Depth 2
                                        #     Child Loop BB6_15 Depth 2
	movq	dec_picture(%rip), %rax
	movq	316976(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movslq	92(%rdi), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	movq	dec_picture(%rip), %rax
	movq	316976(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movslq	92(%rdi), %rcx
	movq	(%rax,%rcx,8), %rax
	movups	%xmm0, (%rax)
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movslq	92(%rdi), %rcx
	movl	$-1, (%rax,%rcx)
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r10,8), %rax
	movslq	92(%rdi), %rcx
	movl	$-1, (%rax,%rcx)
	movq	5544(%rdi), %rax
	movq	(%rax,%r10,8), %rax
	movslq	92(%rdi), %rcx
	movl	$33686018, (%rax,%rcx)  # imm = 0x2020202
	movslq	92(%rdi), %rcx
	movq	dec_picture(%rip), %rax
	movq	316960(%rax), %rax
	movq	(%rax), %rdx
	movq	8(%rax), %rax
	movq	(%rdx,%r10,8), %r12
	movq	(%rax,%r10,8), %r13
	leaq	3(%rcx), %rsi
	testb	%r9b, %r9b
	jne	.LBB6_14
# BB#2:                                 # %min.iters.checked
                                        #   in Loop: Header=BB6_1 Depth=1
	movl	$4, %r15d
	andq	$-4, %r15
	je	.LBB6_14
# BB#3:                                 # %vector.memcheck
                                        #   in Loop: Header=BB6_1 Depth=1
	leaq	(%r12,%rcx,8), %rax
	leaq	8(%r13,%rsi,8), %rdx
	cmpq	%rdx, %rax
	jae	.LBB6_5
# BB#4:                                 # %vector.memcheck
                                        #   in Loop: Header=BB6_1 Depth=1
	leaq	8(%r12,%rsi,8), %rax
	leaq	(%r13,%rcx,8), %rdx
	cmpq	%rax, %rdx
	jb	.LBB6_14
.LBB6_5:                                # %vector.body.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	leaq	-4(%r15), %r14
	movl	%r14d, %eax
	shrl	$2, %eax
	incl	%eax
	andq	$3, %rax
	je	.LBB6_6
# BB#7:                                 # %vector.body.prol.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	leaq	16(%r13,%rcx,8), %rdx
	leaq	16(%r12,%rcx,8), %rbx
	negq	%rax
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_8:                                # %vector.body.prol
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -16(%rbx,%rbp,8)
	movups	%xmm1, (%rbx,%rbp,8)
	movups	%xmm1, -16(%rdx,%rbp,8)
	movups	%xmm1, (%rdx,%rbp,8)
	addq	$4, %rbp
	incq	%rax
	jne	.LBB6_8
	jmp	.LBB6_9
.LBB6_6:                                #   in Loop: Header=BB6_1 Depth=1
	xorl	%ebp, %ebp
.LBB6_9:                                # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB6_1 Depth=1
	cmpq	$12, %r14
	jb	.LBB6_12
# BB#10:                                # %vector.body.preheader.new
                                        #   in Loop: Header=BB6_1 Depth=1
	movq	%r15, %rbx
	subq	%rbp, %rbx
	addq	%rcx, %rbp
	leaq	112(%r13,%rbp,8), %rdx
	leaq	112(%r12,%rbp,8), %rax
	.p2align	4, 0x90
.LBB6_11:                               # %vector.body
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -112(%rax)
	movups	%xmm1, -96(%rax)
	movups	%xmm1, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	%xmm1, -80(%rax)
	movups	%xmm1, -64(%rax)
	movups	%xmm1, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	%xmm1, -48(%rax)
	movups	%xmm1, -32(%rax)
	movups	%xmm1, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	%xmm1, -16(%rax)
	movups	%xmm1, (%rax)
	movups	%xmm1, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	subq	$-128, %rax
	addq	$-16, %rbx
	jne	.LBB6_11
.LBB6_12:                               # %middle.block
                                        #   in Loop: Header=BB6_1 Depth=1
	cmpq	%r15, %r8
	je	.LBB6_16
# BB#13:                                #   in Loop: Header=BB6_1 Depth=1
	addq	%r15, %rcx
	.p2align	4, 0x90
.LBB6_14:                               # %scalar.ph.preheader
                                        #   in Loop: Header=BB6_1 Depth=1
	decq	%rcx
	.p2align	4, 0x90
.LBB6_15:                               # %scalar.ph
                                        #   Parent Loop BB6_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r11, 8(%r12,%rcx,8)
	movq	%r11, 8(%r13,%rcx,8)
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB6_15
.LBB6_16:                               # %._crit_edge
                                        #   in Loop: Header=BB6_1 Depth=1
	movslq	76(%rdi), %rax
	addq	$3, %rax
	cmpq	%rax, %r10
	leaq	1(%r10), %r10
	jl	.LBB6_1
# BB#17:                                # %._crit_edge30
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	init_macroblock, .Lfunc_end6-init_macroblock
	.cfi_endproc

	.globl	SetB8Mode
	.p2align	4, 0x90
	.type	SetB8Mode,@function
SetB8Mode:                              # @SetB8Mode
	.cfi_startproc
# BB#0:
	movslq	%edx, %rdx
	cmpl	$1, 44(%rdi)
	jne	.LBB7_2
# BB#1:
	movb	SetB8Mode.b_v2b8(,%rdx,4), %dil
	movslq	%ecx, %rax
	movb	%dil, 328(%rsi,%rax)
	leaq	SetB8Mode.b_v2pd(,%rdx,4), %rcx
	jmp	.LBB7_3
.LBB7_2:
	movb	SetB8Mode.p_v2b8(,%rdx,4), %dil
	movslq	%ecx, %rax
	movb	%dil, 328(%rsi,%rax)
	leaq	SetB8Mode.p_v2pd(,%rdx,4), %rcx
.LBB7_3:
	movb	(%rcx), %cl
	movb	%cl, 332(%rsi,%rax)
	retq
.Lfunc_end7:
	.size	SetB8Mode, .Lfunc_end7-SetB8Mode
	.cfi_endproc

	.globl	reset_coeffs
	.p2align	4, 0x90
	.type	reset_coeffs,@function
reset_coeffs:                           # @reset_coeffs
	.cfi_startproc
# BB#0:                                 # %.preheader
	movq	img(%rip), %rax
	movl	5924(%rax), %ecx
	cmpl	$-3, %ecx
	jl	.LBB8_12
# BB#1:                                 # %.lr.ph.preheader
	leaq	2408(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB8_2
# BB#3:                                 # %._crit_edge
	cmpl	$-3, %ecx
	jl	.LBB8_12
# BB#4:                                 # %.lr.ph.1.preheader
	leaq	3176(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph.1
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB8_5
# BB#6:                                 # %._crit_edge.1
	cmpl	$-3, %ecx
	jl	.LBB8_12
# BB#7:                                 # %.lr.ph.2.preheader
	leaq	3944(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_8:                                # %.lr.ph.2
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB8_8
# BB#9:                                 # %._crit_edge.2
	cmpl	$-3, %ecx
	jl	.LBB8_12
# BB#10:                                # %.lr.ph.3.preheader
	leaq	4712(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_11:                               # %.lr.ph.3
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB8_11
.LBB8_12:                               # %._crit_edge.3
	movq	5560(%rax), %rdx
	movl	4(%rax), %eax
	movq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdi
	leal	16(,%rcx,4), %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	jmp	memset                  # TAILCALL
.Lfunc_end8:
	.size	reset_coeffs, .Lfunc_end8-reset_coeffs
	.cfi_endproc

	.globl	field_flag_inference
	.p2align	4, 0x90
	.type	field_flag_inference,@function
field_flag_inference:                   # @field_flag_inference
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rcx
	movq	5600(%rcx), %rax
	movl	4(%rcx), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 380(%rax,%rcx)
	je	.LBB9_2
# BB#1:
	leaq	364(%rax,%rcx), %rdx
	jmp	.LBB9_5
.LBB9_2:
	cmpl	$0, 384(%rax,%rcx)
	je	.LBB9_3
# BB#4:
	leaq	368(%rax,%rcx), %rdx
.LBB9_5:                                # %.sink.split
	movslq	(%rdx), %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	356(%rax,%rdx), %edx
	movl	%edx, 356(%rax,%rcx)
	retq
.LBB9_3:
	xorl	%edx, %edx
	movl	%edx, 356(%rax,%rcx)
	retq
.Lfunc_end9:
	.size	field_flag_inference, .Lfunc_end9-field_flag_inference
	.cfi_endproc

	.globl	set_chroma_qp
	.p2align	4, 0x90
	.type	set_chroma_qp,@function
set_chroma_qp:                          # @set_chroma_qp
	.cfi_startproc
# BB#0:
	movq	img(%rip), %r9
	movq	dec_picture(%rip), %r8
	xorl	%r10d, %r10d
	xorl	%ecx, %ecx
	subl	5888(%r9), %ecx
	movl	(%rdi), %eax
	movl	317076(%r8), %esi
	addl	%eax, %esi
	cmpl	%ecx, %esi
	cmovll	%ecx, %esi
	cmpl	$52, %esi
	movl	$51, %edx
	movl	$51, %ecx
	cmovll	%esi, %ecx
	movl	%ecx, 4(%rdi)
	testl	%ecx, %ecx
	js	.LBB10_2
# BB#1:
	movslq	%ecx, %rcx
	movzbl	QP_SCALE_CR(%rcx), %esi
.LBB10_2:
	movl	%esi, 4(%rdi)
	subl	5888(%r9), %r10d
	addl	317080(%r8), %eax
	cmpl	%r10d, %eax
	cmovll	%r10d, %eax
	cmpl	$52, %eax
	cmovll	%eax, %edx
	movl	%edx, 8(%rdi)
	testl	%edx, %edx
	js	.LBB10_4
# BB#3:
	movslq	%edx, %rax
	movzbl	QP_SCALE_CR(%rax), %eax
.LBB10_4:
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end10:
	.size	set_chroma_qp, .Lfunc_end10-set_chroma_qp
	.cfi_endproc

	.globl	read_one_macroblock
	.p2align	4, 0x90
	.type	read_one_macroblock,@function
read_one_macroblock:                    # @read_one_macroblock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 240
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsi, 176(%rsp)         # 8-byte Spill
	movq	%rdi, %r12
	movq	5600(%r12), %r14
	movl	4(%r12), %eax
	movq	5592(%r12), %rbx
	movslq	28(%rbx), %rcx
	xorl	%edx, %edx
	movl	%eax, %esi
	andl	$1, %esi
	movl	$0, %r9d
	je	.LBB11_1
# BB#2:
	movl	5624(%r12), %edi
	testl	%edi, %edi
	movl	$0, %r15d
	je	.LBB11_6
# BB#3:
	leal	-1(%rax), %edi
	imulq	$408, %rdi, %rdi        # imm = 0x198
	leaq	(%r14,%rdi), %r9
	cmpl	$1, 44(%r12)
	jne	.LBB11_4
# BB#5:
	movl	360(%r14,%rdi), %r15d
	testl	%esi, %esi
	jne	.LBB11_7
	jmp	.LBB11_8
.LBB11_1:
	xorl	%r15d, %r15d
	testl	%esi, %esi
	jne	.LBB11_7
	jmp	.LBB11_8
.LBB11_4:
	xorl	%r15d, %r15d
	cmpl	$0, 40(%r14,%rdi)
	sete	%r15b
.LBB11_6:
	testl	%esi, %esi
	je	.LBB11_8
.LBB11_7:
	leal	-1(%rax), %edx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	356(%r14,%rdx), %edx
.LBB11_8:
	leaq	(%rcx,%rcx,4), %r8
	imulq	$408, %rax, %r13        # imm = 0x198
	movl	%edx, 356(%r14,%r13)
	movl	28(%r12), %esi
	movl	%esi, (%r14,%r13)
	movq	dec_picture(%rip), %rdx
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	subl	5888(%r12), %eax
	addl	317076(%rdx), %esi
	cmpl	%eax, %esi
	cmovll	%eax, %esi
	cmpl	$52, %esi
	movl	$51, %eax
	movl	$51, %ebp
	cmovll	%esi, %ebp
	leaq	4(%r14,%r13), %rdi
	movl	%ebp, 4(%r14,%r13)
	testl	%ebp, %ebp
	js	.LBB11_10
# BB#9:
	movslq	%ebp, %rsi
	movzbl	QP_SCALE_CR(%rsi), %esi
.LBB11_10:
	shlq	$4, %r8
	movl	%esi, (%rdi)
	subl	5888(%r12), %ecx
	movl	317080(%rdx), %edx
	addl	28(%r12), %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	cmpl	$52, %edx
	cmovll	%edx, %eax
	leaq	8(%r14,%r13), %rcx
	movl	%eax, 8(%r14,%r13)
	testl	%eax, %eax
	js	.LBB11_12
# BB#11:
	cltq
	movzbl	QP_SCALE_CR(%rax), %edx
.LBB11_12:
	movl	%edx, (%rcx)
	movl	$2, 32(%rsp)
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	40(%rbx), %rbx
	movslq	assignSE2partition+8(%r8), %rax
	imulq	$56, %rax, %r10
	leaq	(%rbx,%r10), %rbp
	movq	active_pps(%rip), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	je	.LBB11_14
# BB#13:
	movq	(%rbp), %rcx
	cmpl	$0, 24(%rcx)
	je	.LBB11_15
.LBB11_14:
	movq	$linfo_ue, 64(%rsp)
.LBB11_15:
	leaq	356(%r14,%r13), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	44(%r12), %ecx
	cmpl	$4, %ecx
	movq	%r8, (%rsp)             # 8-byte Spill
	je	.LBB11_17
# BB#16:
	cmpl	$2, %ecx
	jne	.LBB11_28
.LBB11_17:
	cmpl	$0, 5624(%r12)
	je	.LBB11_24
# BB#18:
	testb	$1, 4(%r12)
	jne	.LBB11_24
# BB#19:
	testl	%eax, %eax
	movq	(%rbp), %rsi
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%rbx, 168(%rsp)         # 8-byte Spill
	je	.LBB11_21
# BB#20:
	cmpl	$0, 24(%rsi)
	je	.LBB11_22
.LBB11_21:                              # %._crit_edge496
	movq	%r10, %r15
	movl	$1, 44(%rsp)
	leaq	32(%rsp), %rdi
	callq	readSyntaxElement_FLC
	jmp	.LBB11_23
.LBB11_28:
	cmpl	$1, %eax
	jne	.LBB11_58
# BB#29:
	movq	%r10, 80(%rsp)          # 8-byte Spill
	movq	%r9, 160(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	cmpl	$0, 5624(%r12)
	je	.LBB11_39
# BB#30:
	testl	%r15d, %r15d
	jne	.LBB11_32
# BB#31:
	movl	4(%r12), %eax
	andl	$1, %eax
	jne	.LBB11_39
.LBB11_32:
	movq	img(%rip), %rcx
	movq	5600(%rcx), %rax
	movl	4(%rcx), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 380(%rax,%rcx)
	je	.LBB11_34
# BB#33:
	leaq	364(%rax,%rcx), %rdx
	jmp	.LBB11_37
.LBB11_58:
	movl	5576(%r12), %eax
	cmpl	$-1, %eax
	jne	.LBB11_60
# BB#59:
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%r10, %rbp
	callq	*48(%rbx,%rbp)
	movq	%rbp, %r10
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	(%rsp), %r8             # 8-byte Reload
	movl	36(%rsp), %eax
	movl	%eax, 5576(%r12)
.LBB11_60:
	testl	%eax, %eax
	je	.LBB11_61
# BB#72:
	leal	-1(%rax), %ecx
	movl	%ecx, 5576(%r12)
	movl	$0, 40(%r14,%r13)
	movl	$0, 336(%r14,%r13)
	movl	$1, 360(%r14,%r13)
	cmpl	$0, 5624(%r12)
	je	.LBB11_86
# BB#73:
	testl	%ecx, %ecx
	je	.LBB11_74
# BB#76:                                # %thread-pre-split
	cmpl	$2, %eax
	jl	.LBB11_86
# BB#77:
	movl	4(%r12), %esi
	testb	$1, %sil
	jne	.LBB11_86
# BB#78:
	leal	-2(%rsi), %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	mb_is_available
	testl	%eax, %eax
	movl	4(%r12), %ecx
	movl	5820(%r12), %esi
	je	.LBB11_81
# BB#79:
	leal	(%rsi,%rsi), %edi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%edi
	testl	%edx, %edx
	je	.LBB11_81
# BB#80:
	addl	$-2, %ecx
	jmp	.LBB11_84
.LBB11_61:
	cmpl	$0, 5624(%r12)
	je	.LBB11_65
# BB#62:
	testl	%r15d, %r15d
	jne	.LBB11_64
# BB#63:
	movl	4(%r12), %eax
	andl	$1, %eax
	jne	.LBB11_65
.LBB11_64:
	movl	$1, 44(%rsp)
	movq	(%rbp), %rsi
	leaq	32(%rsp), %rdi
	movq	%r10, %r15
	callq	readSyntaxElement_FLC
	movq	%r15, %r10
	movl	36(%rsp), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB11_65:
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbp, %r15
	movq	%rbp, %rdx
	callq	*48(%rbx,%r10)
	movl	44(%r12), %eax
	testl	%eax, %eax
	je	.LBB11_68
# BB#66:
	cmpl	$3, %eax
	jne	.LBB11_67
.LBB11_68:
	movl	36(%rsp), %eax
	incl	%eax
	movl	%eax, 36(%rsp)
	jmp	.LBB11_69
.LBB11_22:
	movq	$readFieldModeInfo_CABAC, 72(%rsp)
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	movq	%r10, %r15
	callq	*48(%rbx,%r10)
.LBB11_23:
	movl	36(%rsp), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movq	active_pps(%rip), %rax
	movl	12(%rax), %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	168(%rsp), %rbx         # 8-byte Reload
	movq	%r15, %r10
.LBB11_24:
	cmpl	$1, %eax
	jne	.LBB11_26
# BB#25:
	xorl	%eax, %eax
	movq	%r10, %r15
	callq	CheckAvailabilityOfNeighborsCABAC
	movq	%r15, %r10
.LBB11_26:
	movq	$readMB_typeInfo_CABAC, 72(%rsp)
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	*48(%rbx,%r10)
	movl	36(%rsp), %eax
	movl	%eax, 40(%r14,%r13)
	movq	(%rbp), %rax
	cmpl	$0, 24(%rax)
	movq	(%rsp), %r8             # 8-byte Reload
	jne	.LBB11_86
# BB#27:
	movl	$0, 336(%r14,%r13)
	jmp	.LBB11_86
.LBB11_74:
	testb	$1, 4(%r12)
	jne	.LBB11_86
# BB#75:
	movl	$1, 44(%rsp)
	movq	(%rbp), %rsi
	leaq	32(%rsp), %rdi
	callq	readSyntaxElement_FLC
	movq	(%rsp), %r8             # 8-byte Reload
	movq	(%rbp), %rax
	decl	8(%rax)
	movl	36(%rsp), %eax
	jmp	.LBB11_85
.LBB11_67:                              # %._crit_edge504
	movl	36(%rsp), %eax
.LBB11_69:
	movq	(%rsp), %r8             # 8-byte Reload
	movl	%eax, 40(%r14,%r13)
	movq	(%r15), %rax
	cmpl	$0, 24(%rax)
	jne	.LBB11_71
# BB#70:
	movl	$0, 336(%r14,%r13)
.LBB11_71:
	decl	5576(%r12)
	movl	$0, 360(%r14,%r13)
	jmp	.LBB11_86
.LBB11_34:
	cmpl	$0, 384(%rax,%rcx)
	je	.LBB11_35
# BB#36:
	leaq	368(%rax,%rcx), %rdx
.LBB11_37:                              # %.sink.split.i
	movslq	(%rdx), %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movl	356(%rax,%rdx), %edx
.LBB11_38:                              # %field_flag_inference.exit
	movl	%edx, 356(%rax,%rcx)
.LBB11_39:
	xorl	%eax, %eax
	callq	CheckAvailabilityOfNeighborsCABAC
	movq	$readMB_skip_flagInfo_CABAC, 72(%rsp)
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	8(%rsp), %r15           # 8-byte Reload
	movq	%r15, %rdx
	movq	80(%rsp), %rax          # 8-byte Reload
	callq	*48(%rbx,%rax)
	movl	36(%rsp), %eax
	movl	%eax, 40(%r14,%r13)
	xorl	%ebp, %ebp
	testl	%eax, %eax
	sete	%bpl
	movl	%ebp, 360(%r14,%r13)
	movl	44(%r12), %ecx
	cmpl	$1, %ecx
	jne	.LBB11_41
# BB#40:
	movl	40(%rsp), %edx
	movl	%edx, 300(%r14,%r13)
.LBB11_41:
	movq	(%r15), %rdx
	cmpl	$0, 24(%rdx)
	movq	160(%rsp), %rdx         # 8-byte Reload
	jne	.LBB11_43
# BB#42:
	movl	$0, 336(%r14,%r13)
.LBB11_43:
	cmpl	$1, %ecx
	movq	80(%rsp), %rcx          # 8-byte Reload
	jne	.LBB11_47
# BB#44:
	testl	%eax, %eax
	jne	.LBB11_47
# BB#45:
	cmpl	$0, 40(%rsp)
	jne	.LBB11_47
# BB#46:
	movl	$0, 5576(%r12)
.LBB11_47:
	leaq	48(%rbx,%rcx), %r15
	leaq	40(%r14,%r13), %rbx
	cmpl	$0, 5624(%r12)
	je	.LBB11_55
# BB#48:
	testb	$1, 4(%r12)
	jne	.LBB11_50
# BB#49:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	setne	%cl
	xorl	%eax, %eax
	orl	%ecx, %eax
	jne	.LBB11_52
	jmp	.LBB11_53
.LBB11_50:
	testl	%eax, %eax
	setne	%al
	cmpl	$0, 360(%rdx)
	setne	%cl
	andb	%al, %cl
	movzbl	%cl, %eax
	xorl	%ebp, %ebp
	xorl	%ecx, %ecx
	orl	%ecx, %eax
	je	.LBB11_53
.LBB11_52:
	movq	$readFieldModeInfo_CABAC, 72(%rsp)
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	*(%r15)
	movl	36(%rsp), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB11_53:
	testl	%ebp, %ebp
	je	.LBB11_55
# BB#54:
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	8(%rsp), %rdx           # 8-byte Reload
	callq	check_next_mb_and_get_field_mode_CABAC
.LBB11_55:
	xorl	%eax, %eax
	callq	CheckAvailabilityOfNeighborsCABAC
	cmpl	$0, (%rbx)
	movq	(%rsp), %r8             # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	je	.LBB11_86
# BB#56:
	movq	$readMB_typeInfo_CABAC, 72(%rsp)
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	movq	%rbx, %rbp
	movq	%rax, %rbx
	movq	%rbx, %rdx
	callq	*(%r15)
	movq	(%rsp), %r8             # 8-byte Reload
	movl	36(%rsp), %eax
	movl	%eax, (%rbp)
	movq	(%rbx), %rax
	cmpl	$0, 24(%rax)
	jne	.LBB11_86
# BB#57:
	movl	$0, 336(%r14,%r13)
	jmp	.LBB11_86
.LBB11_35:
	xorl	%edx, %edx
	jmp	.LBB11_38
.LBB11_81:                              # %._crit_edge500
	addl	%esi, %esi
	movl	%ecx, %edi
	subl	%esi, %edi
	movl	%ecx, %esi
	callq	mb_is_available
	testl	%eax, %eax
	je	.LBB11_82
# BB#83:
	movl	4(%r12), %ecx
	movl	5820(%r12), %eax
	addl	%eax, %eax
	subl	%eax, %ecx
.LBB11_84:                              # %.sink.split
	movq	(%rsp), %r8             # 8-byte Reload
	movq	5600(%r12), %rax
	movl	%ecx, %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movl	356(%rax,%rcx), %eax
	jmp	.LBB11_85
.LBB11_82:
	xorl	%eax, %eax
	movq	(%rsp), %r8             # 8-byte Reload
.LBB11_85:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB11_86:                              # %thread-pre-split.thread
	movq	16(%rsp), %rax          # 8-byte Reload
	movb	(%rax), %al
	movq	dec_picture(%rip), %rcx
	movq	316936(%rcx), %rcx
	movl	4(%r12), %edx
	movb	%al, (%rcx,%rdx)
	movq	5568(%r12), %rax
	movslq	68(%r12), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	72(%r12), %rcx
	movl	$0, (%rax,%rcx,4)
	movl	44(%r12), %eax
	cmpq	$4, %rax
	movq	104(%rsp), %r15         # 8-byte Reload
	ja	.LBB11_112
# BB#87:                                # %thread-pre-split.thread
	jmpq	*.LJTI11_0(,%rax,8)
.LBB11_88:
	movq	5600(%r12), %rax
	movl	4(%r12), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movl	40(%rax,%rcx), %edx
	cmpl	$3, %edx
	jg	.LBB11_90
# BB#89:
	movzbl	%dl, %edx
	imull	$16843009, %edx, %edx   # imm = 0x1010101
	movl	%edx, 328(%rax,%rcx)
	movl	$0, 332(%rax,%rcx)
	cmpl	$0, 5624(%r12)
	jne	.LBB11_113
	jmp	.LBB11_115
.LBB11_102:
	movq	%r12, %rdi
	callq	interpret_mb_mode_B
	movq	(%rsp), %r8             # 8-byte Reload
	cmpl	$0, 5624(%r12)
	jne	.LBB11_113
	jmp	.LBB11_115
.LBB11_95:
	movq	5600(%r12), %rax
	movl	4(%r12), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	leaq	40(%rax,%rcx), %rsi
	movl	40(%rax,%rcx), %edx
	cmpl	$25, %edx
	je	.LBB11_98
# BB#96:
	testl	%edx, %edx
	jne	.LBB11_99
# BB#97:
	movl	$9, (%rsi)
	movl	$185273099, %edx        # imm = 0xB0B0B0B
	movl	%edx, 328(%rax,%rcx)
	jmp	.LBB11_111
.LBB11_105:
	movq	5600(%r12), %rax
	movl	4(%r12), %ecx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	leaq	40(%rax,%rcx), %rsi
	movl	40(%rax,%rcx), %edx
	cmpl	$26, %edx
	je	.LBB11_103
# BB#106:
	cmpl	$1, %edx
	je	.LBB11_94
# BB#107:
	testl	%edx, %edx
	jne	.LBB11_109
# BB#108:
	movl	$12, (%rsi)
	movabsq	$-4109694197, %rdx      # imm = 0xFFFFFFFF0B0B0B0B
	movq	%rdx, 328(%rax,%rcx)
	movq	5568(%r12), %rax
	movslq	68(%r12), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	72(%r12), %rcx
	movl	$1, (%rax,%rcx,4)
	cmpl	$0, 5624(%r12)
	jne	.LBB11_113
	jmp	.LBB11_115
.LBB11_90:
	leaq	40(%rax,%rcx), %rsi
	movl	%edx, %edi
	orl	$1, %edi
	cmpl	$5, %edi
	jne	.LBB11_92
# BB#91:
	xorl	%eax, %eax
	cmpl	$5, %edx
	sete	%al
	movl	$8, (%rsi)
	movl	%eax, 100(%r12)
	cmpl	$0, 5624(%r12)
	jne	.LBB11_113
	jmp	.LBB11_115
.LBB11_92:
	cmpl	$31, %edx
	je	.LBB11_103
# BB#93:
	cmpl	$6, %edx
	jne	.LBB11_104
.LBB11_94:
	movl	$9, (%rsi)
	movabsq	$-4109694197, %rdx      # imm = 0xFFFFFFFF0B0B0B0B
	movq	%rdx, 328(%rax,%rcx)
	cmpl	$0, 5624(%r12)
	jne	.LBB11_113
	jmp	.LBB11_115
.LBB11_103:
	movl	$14, (%rsi)
	movl	$-1, 300(%rax,%rcx)
	movl	$0, 324(%rax,%rcx)
	jmp	.LBB11_110
.LBB11_98:
	movl	$14, (%rsi)
	movl	$-1, 300(%rax,%rcx)
	movl	$0, 324(%rax,%rcx)
	jmp	.LBB11_100
.LBB11_99:
	movl	$10, (%rsi)
	decl	%edx
	movl	%edx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movl	interpret_mb_mode_SI.ICBPTAB(,%rsi,4), %esi
	movl	%esi, 300(%rax,%rcx)
	andl	$3, %edx
	movl	%edx, 324(%rax,%rcx)
.LBB11_100:                             # %interpret_mb_mode_I.exit
	xorl	%edx, %edx
	movl	%edx, 328(%rax,%rcx)
	jmp	.LBB11_111
.LBB11_109:
	movl	$10, (%rsi)
	leal	-1(%rdx), %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movl	interpret_mb_mode_SI.ICBPTAB(,%rsi,4), %esi
	movl	%esi, 300(%rax,%rcx)
	addl	$2, %edx
	andl	$3, %edx
	movl	%edx, 324(%rax,%rcx)
	jmp	.LBB11_110
.LBB11_104:
	movl	$10, (%rsi)
	addl	$-7, %edx
	movl	%edx, %esi
	sarl	$2, %esi
	movslq	%esi, %rsi
	movl	interpret_mb_mode_SI.ICBPTAB(,%rsi,4), %esi
	movl	%esi, 300(%rax,%rcx)
	andl	$3, %edx
	movl	%edx, 324(%rax,%rcx)
.LBB11_110:                             # %interpret_mb_mode_P.exit
	movl	$0, 328(%rax,%rcx)
.LBB11_111:                             # %interpret_mb_mode_P.exit
	movl	$-1, 332(%rax,%rcx)
.LBB11_112:                             # %interpret_mb_mode_P.exit
	cmpl	$0, 5624(%r12)
	je	.LBB11_115
.LBB11_113:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB11_115
# BB#114:
	shll	5640(%r12)
	shll	5644(%r12)
.LBB11_115:
	leaq	assignSE2partition+8(%r8), %rbx
	leaq	40(%r14,%r13), %rbp
	movl	40(%r14,%r13), %eax
	testl	%eax, %eax
	je	.LBB11_116
# BB#121:
	movl	$1, 400(%r14,%r13)
	cmpl	$8, %eax
	jne	.LBB11_132
# BB#122:
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%r13, 88(%rsp)          # 8-byte Spill
	leaq	400(%r14,%r13), %rbp
	movl	$2, 32(%rsp)
	movq	40(%r15), %rax
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movslq	(%rbx), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rbx
	leaq	48(%rax,%rcx), %r13
	movq	$-4, %r14
	leaq	32(%rsp), %r15
	.p2align	4, 0x90
.LBB11_123:                             # =>This Inner Loop Header: Depth=1
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	je	.LBB11_125
# BB#124:                               #   in Loop: Header=BB11_123 Depth=1
	movq	(%rbx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB11_126
.LBB11_125:                             #   in Loop: Header=BB11_123 Depth=1
	movq	$linfo_ue, 64(%rsp)
	jmp	.LBB11_127
	.p2align	4, 0x90
.LBB11_126:                             #   in Loop: Header=BB11_123 Depth=1
	movq	$readB8_typeInfo_CABAC, 72(%rsp)
.LBB11_127:                             # %SetB8Mode.exit
                                        #   in Loop: Header=BB11_123 Depth=1
	movq	%r15, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	*(%r13)
	cmpl	$1, 44(%r12)
	movslq	36(%rsp), %rax
	leaq	SetB8Mode.b_v2b8(,%rax,4), %rcx
	leaq	SetB8Mode.b_v2pd(,%rax,4), %rdx
	leaq	SetB8Mode.p_v2b8(,%rax,4), %rsi
	leaq	SetB8Mode.p_v2pd(,%rax,4), %rdi
	cmoveq	%rcx, %rsi
	cmoveq	%rdx, %rdi
	movzbl	(%rsi), %eax
	movb	%al, -68(%rbp,%r14)
	movzbl	(%rdi), %ecx
	movb	%cl, -64(%rbp,%r14)
	testb	%al, %al
	je	.LBB11_128
.LBB11_129:                             #   in Loop: Header=BB11_123 Depth=1
	cmpb	$4, %al
	sete	%cl
	jmp	.LBB11_130
	.p2align	4, 0x90
.LBB11_128:                             #   in Loop: Header=BB11_123 Depth=1
	movq	active_sps(%rip), %rdx
	movb	$1, %cl
	cmpl	$0, 2084(%rdx)
	je	.LBB11_129
.LBB11_130:                             #   in Loop: Header=BB11_123 Depth=1
	movzbl	%cl, %eax
	andl	%eax, (%rbp)
	incq	%r14
	jne	.LBB11_123
# BB#131:
	movq	%r12, %rdi
	callq	init_macroblock
	movq	%r12, %rdi
	callq	readMotionInfoFromNAL
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	(%rbp), %eax
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	88(%rsp), %r13          # 8-byte Reload
	movq	104(%rsp), %r15         # 8-byte Reload
	movq	(%rsp), %r8             # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
.LBB11_132:
	cmpl	$9, %eax
	jne	.LBB11_120
# BB#133:
	cmpl	$0, 5908(%r12)
	je	.LBB11_120
# BB#134:
	leaq	assignSE2partition(%r8), %rcx
	movl	$0, 32(%rsp)
	movq	40(%r15), %rax
	movslq	(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	movq	$readMB_transform_size_flag_CABAC, 72(%rsp)
	movq	active_pps(%rip), %rdx
	cmpl	$0, 12(%rdx)
	movq	(%rax,%rcx), %rsi
	je	.LBB11_136
# BB#135:
	cmpl	$0, 24(%rsi)
	je	.LBB11_137
.LBB11_136:                             # %._crit_edge507
	movl	$1, 44(%rsp)
	leaq	32(%rsp), %rdi
	callq	readSyntaxElement_FLC
	jmp	.LBB11_138
.LBB11_116:
	cmpl	$1, 44(%r12)
	jne	.LBB11_117
# BB#118:
	movq	active_sps(%rip), %rax
	cmpl	$0, 2084(%rax)
	sete	%al
	jmp	.LBB11_119
.LBB11_117:
	xorl	%eax, %eax
.LBB11_119:                             # %.thread559
	xorb	$1, %al
	movzbl	%al, %eax
	movl	%eax, 400(%r14,%r13)
.LBB11_120:
	movl	$0, 396(%r14,%r13)
.LBB11_140:                             # %.loopexit446
	movq	active_pps(%rip), %rax
	cmpl	$0, 1148(%rax)
	movq	%rbp, %rax
	je	.LBB11_146
# BB#141:
	cmpl	$1, 44(%r12)
	movq	%rbp, %rax
	ja	.LBB11_146
# BB#142:
	movl	(%rbp), %eax
	addl	$-9, %eax
	cmpl	$6, %eax
	jae	.LBB11_143
# BB#144:                               # %switch.hole_check
	movl	$59, %ecx
	btl	%eax, %ecx
	jb	.LBB11_145
.LBB11_143:
	movq	16(%r12), %rax
	movl	4(%r12), %ecx
	movl	$0, (%rax,%rcx,4)
	movq	%rbp, %rax
	jmp	.LBB11_146
.LBB11_145:                             # %switch.lookup
	cltq
	leaq	.Lswitch.table.1(,%rax,4), %rax
.LBB11_146:                             # %thread-pre-split424
	movl	(%rax), %eax
	cmpl	$14, %eax
	ja	.LBB11_151
# BB#147:                               # %thread-pre-split424
	movl	$30208, %ecx            # imm = 0x7600
	btl	%eax, %ecx
	jae	.LBB11_151
# BB#148:
	movslq	assignSE2partition+24(%r8), %rax
	movq	40(%r15), %rcx
	imulq	$56, %rax, %rax
	movq	(%rcx,%rax), %rax
	cmpl	$0, 24(%rax)
	je	.LBB11_151
# BB#149:
	cmpl	$0, (%r12)
	je	.LBB11_151
# BB#150:                               # %.loopexit.loopexit486
	movl	$0, (%rbp)
	movl	$1, 336(%r14,%r13)
	movq	$0, 328(%r14,%r13)
.LBB11_151:                             # %.loopexit
	cmpl	$8, (%rbp)
	je	.LBB11_230
# BB#152:
	movq	%r12, %rdi
	callq	init_macroblock
	movl	(%rbp), %eax
	cmpl	$14, %eax
	je	.LBB11_234
# BB#153:
	testl	%eax, %eax
	jne	.LBB11_230
# BB#154:
	movl	44(%r12), %eax
	testl	%eax, %eax
	je	.LBB11_172
# BB#155:
	cmpl	$3, %eax
	je	.LBB11_172
# BB#156:
	cmpl	$1, %eax
	jne	.LBB11_230
# BB#157:
	cmpl	$0, 5576(%r12)
	js	.LBB11_230
# BB#158:
	movl	$0, 300(%r14,%r13)
	movq	img(%rip), %rax
	movl	5924(%rax), %ecx
	cmpl	$-3, %ecx
	jl	.LBB11_170
# BB#159:                               # %.lr.ph.i407.preheader
	leaq	2408(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB11_160:                             # %.lr.ph.i407
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_160
# BB#161:                               # %._crit_edge.i408
	cmpl	$-3, %ecx
	jl	.LBB11_170
# BB#162:                               # %.lr.ph.1.i411.preheader
	leaq	3176(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB11_163:                             # %.lr.ph.1.i411
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_163
# BB#164:                               # %._crit_edge.1.i412
	cmpl	$-3, %ecx
	jl	.LBB11_170
# BB#165:                               # %.lr.ph.2.i415.preheader
	leaq	3944(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
.LBB11_166:                             # %.lr.ph.2.i415
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_166
# BB#167:                               # %._crit_edge.2.i416
	cmpl	$-3, %ecx
	jl	.LBB11_170
# BB#168:                               # %.lr.ph.3.i419.preheader
	leaq	4712(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
.LBB11_169:                             # %.lr.ph.3.i419
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_169
.LBB11_170:                             # %reset_coeffs.exit420
	movq	5560(%rax), %rdx
	movl	4(%rax), %eax
	movq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdi
	leal	16(,%rcx,4), %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	active_pps(%rip), %rax
	cmpl	$1, 12(%rax)
	jne	.LBB11_235
# BB#171:
	movl	$-1, 5576(%r12)
	jmp	.LBB11_235
.LBB11_230:                             # %.thread442
	movq	%r12, %rdi
	callq	read_ipred_modes
	movl	(%rbp), %eax
	cmpl	$14, %eax
	ja	.LBB11_232
# BB#231:                               # %.thread442
	movl	$26369, %ecx            # imm = 0x6701
	btl	%eax, %ecx
	jae	.LBB11_232
.LBB11_233:
	movq	%r12, %rdi
	movq	176(%rsp), %rsi         # 8-byte Reload
	callq	readCBPandCoeffsFromNAL
.LBB11_235:
	movl	$1, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_234:
	movslq	(%rbx), %rax
	imulq	$56, %rax, %rdx
	addq	40(%r15), %rdx
	movq	%r12, %rdi
	callq	readIPCMcoeffsFromNAL
	jmp	.LBB11_235
.LBB11_232:
	movq	%r12, %rdi
	callq	readMotionInfoFromNAL
	jmp	.LBB11_233
.LBB11_172:
	xorl	%r15d, %r15d
	cmpl	$0, 5624(%r12)
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%r13, 88(%rsp)          # 8-byte Spill
	je	.LBB11_173
# BB#174:
	movq	16(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB11_173
# BB#175:
	movl	4(%r12), %eax
	andl	$1, %eax
	leal	2(%rax,%rax), %eax
	jmp	.LBB11_176
.LBB11_173:
	xorl	%eax, %eax
.LBB11_176:
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	dec_picture(%rip), %rax
	movq	316976(%rax), %rax
	movq	(%rax), %r13
	movl	4(%r12), %edi
	leaq	136(%rsp), %rcx
	movl	$-1, %esi
	xorl	%edx, %edx
	callq	getLuma4x4Neighbour
	movl	4(%r12), %edi
	leaq	112(%rsp), %rcx
	xorl	%esi, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movl	136(%rsp), %eax
	testl	%eax, %eax
	movl	$0, %ecx
	je	.LBB11_182
# BB#177:
	movslq	156(%rsp), %rcx
	movq	(%r13,%rcx,8), %rdx
	movslq	152(%rsp), %rsi
	movq	(%rdx,%rsi,8), %rdx
	movswl	2(%rdx), %r15d
	movq	dec_picture(%rip), %rdx
	movq	316952(%rdx), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rcx,8), %rcx
	movsbl	(%rcx,%rsi), %ecx
	movslq	140(%rsp), %rdx
	imulq	$408, %rdx, %rdx        # imm = 0x198
	movq	16(%rsp), %rsi          # 8-byte Reload
	cmpl	$0, (%rsi)
	movq	5600(%r12), %rsi
	movl	356(%rsi,%rdx), %edx
	je	.LBB11_180
# BB#178:
	testl	%edx, %edx
	jne	.LBB11_182
# BB#179:
	movl	%r15d, %edx
	shrl	$31, %edx
	addl	%r15d, %edx
	sarl	%edx
	addl	%ecx, %ecx
	movl	%edx, %r15d
	jmp	.LBB11_182
.LBB11_137:
	leaq	(%rax,%rcx), %rdx
	leaq	32(%rsp), %rdi
	movq	%r12, %rsi
	callq	*48(%rax,%rcx)
.LBB11_138:
	movq	(%rsp), %r8             # 8-byte Reload
	movl	36(%rsp), %eax
	movl	%eax, 396(%r14,%r13)
	testl	%eax, %eax
	je	.LBB11_140
# BB#139:                               # %.loopexit446.loopexit489
	movl	$13, (%rbp)
	movabsq	$-4076008179, %rax      # imm = 0xFFFFFFFF0D0D0D0D
	movq	%rax, 328(%r14,%r13)
	jmp	.LBB11_140
.LBB11_180:                             # %.thread432
	testl	%edx, %edx
	je	.LBB11_182
# BB#181:
	addl	%r15d, %r15d
	sarl	%ecx
.LBB11_182:
	movl	112(%rsp), %edx
	testl	%edx, %edx
	je	.LBB11_183
# BB#184:
	movslq	132(%rsp), %rdi
	movq	(%r13,%rdi,8), %rsi
	movslq	128(%rsp), %rbp
	movq	(%rsi,%rbp,8), %rsi
	movswl	2(%rsi), %r8d
	movq	dec_picture(%rip), %rbx
	movq	316952(%rbx), %rbx
	movq	(%rbx), %rbx
	movq	(%rbx,%rdi,8), %rdi
	movsbl	(%rdi,%rbp), %edi
	movslq	116(%rsp), %rbp
	imulq	$408, %rbp, %rbp        # imm = 0x198
	movq	16(%rsp), %rsi          # 8-byte Reload
	cmpl	$0, (%rsi)
	movq	5600(%r12), %rbx
	movl	356(%rbx,%rbp), %ebp
	je	.LBB11_187
# BB#185:
	testl	%ebp, %ebp
	jne	.LBB11_189
# BB#186:
	movl	%r8d, %esi
	shrl	$31, %esi
	addl	%r8d, %esi
	sarl	%esi
	addl	%edi, %edi
	movl	%esi, %r8d
	testl	%eax, %eax
	jne	.LBB11_191
	jmp	.LBB11_190
.LBB11_183:
	xorl	%r8d, %r8d
	xorl	%edi, %edi
	testl	%eax, %eax
	jne	.LBB11_191
	jmp	.LBB11_190
.LBB11_187:                             # %.thread437
	testl	%ebp, %ebp
	je	.LBB11_189
# BB#188:
	addl	%r8d, %r8d
	sarl	%edi
.LBB11_189:
	testl	%eax, %eax
	je	.LBB11_190
.LBB11_191:
	testl	%ecx, %ecx
	je	.LBB11_193
# BB#192:
	xorl	%eax, %eax
	jmp	.LBB11_194
.LBB11_190:
	movl	$1, %r15d
	testl	%edx, %edx
	jne	.LBB11_197
	jmp	.LBB11_196
.LBB11_193:
	movslq	156(%rsp), %rax
	movq	(%r13,%rax,8), %rax
	movslq	152(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	cmpw	$0, (%rax)
	sete	%cl
	testl	%r15d, %r15d
	sete	%al
	andb	%cl, %al
.LBB11_194:
	movzbl	%al, %r15d
	testl	%edx, %edx
	je	.LBB11_196
.LBB11_197:
	testl	%edi, %edi
	je	.LBB11_199
# BB#198:
	xorl	%r14d, %r14d
	jmp	.LBB11_200
.LBB11_196:
	movb	$1, %r14b
	jmp	.LBB11_200
.LBB11_199:
	movslq	132(%rsp), %rax
	movq	(%r13,%rax,8), %rax
	movslq	128(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	cmpw	$0, (%rax)
	sete	%al
	testl	%r8d, %r8d
	sete	%r14b
	andb	%al, %r14b
.LBB11_200:
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	$0, 300(%rax,%rcx)
	movq	img(%rip), %rax
	movl	5924(%rax), %ecx
	cmpl	$-3, %ecx
	jl	.LBB11_212
# BB#201:                               # %.lr.ph.i.preheader
	leaq	2408(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB11_202:                             # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_202
# BB#203:                               # %._crit_edge.i
	cmpl	$-3, %ecx
	jl	.LBB11_212
# BB#204:                               # %.lr.ph.1.i.preheader
	leaq	3176(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB11_205:                             # %.lr.ph.1.i
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_205
# BB#206:                               # %._crit_edge.1.i
	cmpl	$-3, %ecx
	jl	.LBB11_212
# BB#207:                               # %.lr.ph.2.i.preheader
	leaq	3944(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB11_208:                             # %.lr.ph.2.i
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_208
# BB#209:                               # %._crit_edge.2.i
	cmpl	$-3, %ecx
	jl	.LBB11_212
# BB#210:                               # %.lr.ph.3.i.preheader
	leaq	4712(%rax), %rdx
	movq	$-1, %rsi
	xorps	%xmm0, %xmm0
.LBB11_211:                             # %.lr.ph.3.i
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 48(%rdx)
	movups	%xmm0, 32(%rdx)
	movups	%xmm0, 16(%rdx)
	movups	%xmm0, (%rdx)
	movslq	5924(%rax), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB11_211
.LBB11_212:                             # %reset_coeffs.exit
	movq	5560(%rax), %rdx
	movl	4(%rax), %eax
	movq	(%rdx,%rax,8), %rax
	movq	(%rax), %rdi
	leal	16(,%rcx,4), %eax
	movslq	%eax, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movslq	76(%r12), %rbp
	testl	%r15d, %r15d
	setne	%al
	orb	%r14b, %al
	cmpb	$1, %al
	jne	.LBB11_222
# BB#213:                               # %.preheader.loopexit
	leaq	92(%r12), %r8
	movq	(%r13,%rbp,8), %rcx
	movslq	92(%r12), %rdx
	movq	(%rcx,%rdx,8), %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rcx)
	movq	8(%r13,%rbp,8), %rcx
	movslq	92(%r12), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movups	%xmm0, (%rcx)
	movq	16(%r13,%rbp,8), %rcx
	movslq	92(%r12), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movups	%xmm0, (%rcx)
	movq	24(%r13,%rbp,8), %rcx
	movslq	92(%r12), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movups	%xmm0, (%rcx)
	jmp	.LBB11_214
.LBB11_222:                             # %.split.us.preheader
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %r8
	movq	316976(%rax), %r9
	leaq	28(%rsp), %rsi
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	pushq	$16
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi44:
	.cfi_adjust_cfa_offset -32
	leaq	92(%r12), %r8
	movslq	92(%r12), %rcx
	movq	(%r13,%rbp,8), %rdx
	movl	28(%rsp), %esi
	leaq	3(%rcx), %rdi
	leaq	-1(%rcx), %rbx
	.p2align	4, 0x90
.LBB11_223:                             # %.preheader444.us
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rbx,8), %rax
	movl	%esi, (%rax)
	incq	%rbx
	cmpq	%rdi, %rbx
	jl	.LBB11_223
# BB#224:                               # %.split.us.1
	movq	8(%r13,%rbp,8), %rdx
	movslq	(%r8), %rsi
	movl	28(%rsp), %edi
	addq	$3, %rsi
	leaq	-1(%rcx), %rbx
	.p2align	4, 0x90
.LBB11_225:                             # %.preheader444.us.1
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rbx,8), %rax
	movl	%edi, (%rax)
	incq	%rbx
	cmpq	%rsi, %rbx
	jl	.LBB11_225
# BB#226:                               # %._crit_edge459.us.1
	movq	16(%r13,%rbp,8), %rdx
	movslq	(%r8), %rsi
	movl	28(%rsp), %edi
	addq	$3, %rsi
	leaq	-1(%rcx), %rbx
	.p2align	4, 0x90
.LBB11_227:                             # %.preheader444.us.2
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rbx,8), %rax
	movl	%edi, (%rax)
	incq	%rbx
	cmpq	%rsi, %rbx
	jl	.LBB11_227
# BB#228:                               # %._crit_edge459.us.2
	movq	24(%r13,%rbp,8), %rdx
	movslq	(%r8), %rsi
	movl	28(%rsp), %edi
	addq	$3, %rsi
	decq	%rcx
	.p2align	4, 0x90
.LBB11_229:                             # %.preheader444.us.3
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rcx,8), %rax
	movl	%edi, (%rax)
	incq	%rcx
	cmpq	%rsi, %rcx
	jl	.LBB11_229
.LBB11_214:                             # %.lr.ph
	movq	(%rsp), %rcx            # 8-byte Reload
	movslq	92(%r12), %rdx
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rbp,8), %rsi
	decq	%rdx
	imulq	$264, %rcx, %r9         # imm = 0x108
	.p2align	4, 0x90
.LBB11_215:                             # =>This Inner Loop Header: Depth=1
	movb	$0, 1(%rsi,%rdx)
	movq	dec_picture(%rip), %rax
	movslq	12(%r12), %rdi
	movq	316952(%rax), %rsi
	movq	316960(%rax), %rbx
	movq	(%rsi), %rsi
	movq	(%rsi,%rbp,8), %rsi
	movsbq	1(%rsi,%rdx), %rcx
	imulq	$1584, %rdi, %rdi       # imm = 0x630
	addq	%rax, %rdi
	addq	%r9, %rdi
	movq	24(%rdi,%rcx,8), %rax
	movq	(%rbx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	%rax, 8(%rcx,%rdx,8)
	movslq	92(%r12), %rax
	addq	$3, %rax
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB11_215
# BB#216:                               # %.lr.ph.1
	movslq	(%r8), %rdx
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	8(%rax,%rbp,8), %rsi
	decq	%rdx
	.p2align	4, 0x90
.LBB11_217:                             # =>This Inner Loop Header: Depth=1
	movb	$0, 1(%rsi,%rdx)
	movq	dec_picture(%rip), %rax
	movslq	12(%r12), %rcx
	movq	316952(%rax), %rsi
	movq	316960(%rax), %rdi
	movq	(%rsi), %rsi
	movq	8(%rsi,%rbp,8), %rsi
	movsbq	1(%rsi,%rdx), %rbx
	imulq	$1584, %rcx, %rcx       # imm = 0x630
	addq	%rax, %rcx
	addq	%r9, %rcx
	movq	24(%rcx,%rbx,8), %rax
	movq	(%rdi), %rcx
	movq	8(%rcx,%rbp,8), %rcx
	movq	%rax, 8(%rcx,%rdx,8)
	movslq	92(%r12), %rax
	addq	$3, %rax
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB11_217
# BB#218:                               # %.lr.ph.2
	movslq	(%r8), %rdx
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	16(%rax,%rbp,8), %rsi
	decq	%rdx
	.p2align	4, 0x90
.LBB11_219:                             # =>This Inner Loop Header: Depth=1
	movb	$0, 1(%rsi,%rdx)
	movq	dec_picture(%rip), %rax
	movslq	12(%r12), %rcx
	movq	316952(%rax), %rsi
	movq	316960(%rax), %rdi
	movq	(%rsi), %rsi
	movq	16(%rsi,%rbp,8), %rsi
	movsbq	1(%rsi,%rdx), %rbx
	imulq	$1584, %rcx, %rcx       # imm = 0x630
	addq	%rax, %rcx
	addq	%r9, %rcx
	movq	24(%rcx,%rbx,8), %rax
	movq	(%rdi), %rcx
	movq	16(%rcx,%rbp,8), %rcx
	movq	%rax, 8(%rcx,%rdx,8)
	movslq	92(%r12), %rax
	addq	$3, %rax
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB11_219
# BB#220:                               # %.lr.ph.3
	movslq	(%r8), %rax
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	24(%rcx,%rbp,8), %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB11_221:                             # =>This Inner Loop Header: Depth=1
	movb	$0, 1(%rdx,%rax)
	movq	dec_picture(%rip), %rcx
	movslq	12(%r12), %rsi
	movq	316952(%rcx), %rdx
	movq	316960(%rcx), %rdi
	movq	(%rdx), %rdx
	movq	24(%rdx,%rbp,8), %rdx
	movsbq	1(%rdx,%rax), %rbx
	imulq	$1584, %rsi, %rsi       # imm = 0x630
	addq	%rcx, %rsi
	addq	%r9, %rsi
	movq	24(%rsi,%rbx,8), %rcx
	movq	(%rdi), %rsi
	movq	24(%rsi,%rbp,8), %rsi
	movq	%rcx, 8(%rsi,%rax,8)
	movslq	92(%r12), %rcx
	addq	$3, %rcx
	incq	%rax
	cmpq	%rcx, %rax
	jl	.LBB11_221
	jmp	.LBB11_235
.Lfunc_end11:
	.size	read_one_macroblock, .Lfunc_end11-read_one_macroblock
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_88
	.quad	.LBB11_102
	.quad	.LBB11_95
	.quad	.LBB11_88
	.quad	.LBB11_105

	.text
	.globl	readMotionInfoFromNAL
	.p2align	4, 0x90
	.type	readMotionInfoFromNAL,@function
readMotionInfoFromNAL:                  # @readMotionInfoFromNAL
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi47:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi48:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi49:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 56
	subq	$440, %rsp              # imm = 0x1B8
.Lcfi51:
	.cfi_def_cfa_offset 496
.Lcfi52:
	.cfi_offset %rbx, -56
.Lcfi53:
	.cfi_offset %r12, -48
.Lcfi54:
	.cfi_offset %r13, -40
.Lcfi55:
	.cfi_offset %r14, -32
.Lcfi56:
	.cfi_offset %r15, -24
.Lcfi57:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movq	5600(%r13), %rdx
	movl	4(%r13), %edi
	movq	5592(%r13), %rbp
	movslq	28(%rbp), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	44(%r13), %eax
	movl	%eax, 224(%rsp)         # 4-byte Spill
	imulq	$408, %rdi, %rsi        # imm = 0x198
	movslq	40(%rdx,%rsi), %rcx
	cmpq	$8, %rcx
	movl	$4, %eax
	cmovneq	%rcx, %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	movl	5624(%r13), %eax
	testl	%eax, %eax
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rbp, 200(%rsp)         # 8-byte Spill
	je	.LBB12_4
# BB#1:
	cmpl	$0, 356(%rdx,%rsi)
	je	.LBB12_4
# BB#2:
	movl	%edi, %edx
	andl	$1, %edx
	leal	2(%rdx,%rdx), %edx
	movq	%rdx, 368(%rsp)         # 8-byte Spill
	movq	Co_located(%rip), %rsi
	jne	.LBB12_5
# BB#3:
	leaq	3232(%rsi), %rbp
	leaq	3216(%rsi), %rbx
	leaq	3240(%rsi), %r8
	addq	$3224, %rsi             # imm = 0xC98
	jmp	.LBB12_6
.LBB12_4:                               # %.thread
	movq	Co_located(%rip), %rsi
	leaq	1624(%rsi), %r8
	leaq	1616(%rsi), %rbp
	leaq	1600(%rsi), %rbx
	leaq	1608(%rsi), %rsi
	xorl	%edx, %edx
	movq	%rdx, 368(%rsp)         # 8-byte Spill
	jmp	.LBB12_6
.LBB12_5:
	leaq	4848(%rsi), %rbp
	leaq	4832(%rsi), %rbx
	leaq	4856(%rsi), %r8
	addq	$4840, %rsi             # imm = 0x12E8
.LBB12_6:
	movq	328(%rsp), %rdx         # 8-byte Reload
	movslq	BLOCK_STEP(,%rdx,8), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rsi), %r12
	movq	(%rbx), %r15
	movq	(%rbp), %rdx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	cmpl	$1, 224(%rsp)           # 4-byte Folded Reload
	movq	%r13, 16(%rsp)          # 8-byte Spill
	movq	%r12, 144(%rsp)         # 8-byte Spill
	movq	%r15, 80(%rsp)          # 8-byte Spill
	jne	.LBB12_162
# BB#7:
	cmpl	$8, %ecx
	jne	.LBB12_162
# BB#8:
	cmpl	$0, 40(%r13)
	je	.LBB12_21
# BB#9:
	movq	(%r8), %rbx
	testl	%eax, %eax
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	je	.LBB12_12
# BB#10:
	cmpl	$0, 356(%rax,%rcx)
	je	.LBB12_12
# BB#11:
	movl	76(%r13), %eax
	leal	-4(%rax), %r12d
	testb	$1, %dil
	cmovel	%eax, %r12d
	sarl	%r12d
	jmp	.LBB12_13
.LBB12_12:
	movl	76(%r13), %r12d
.LBB12_13:
	movl	$0, 260(%rsp)
	movl	$0, 256(%rsp)
	movl	$-1, %r14d
	leaq	96(%rsp), %rcx
	movl	$-1, %esi
	xorl	%edx, %edx
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	getLuma4x4Neighbour
	movl	4(%r13), %edi
	leaq	176(%rsp), %rcx
	xorl	%esi, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movl	4(%r13), %edi
	leaq	280(%rsp), %rcx
	movl	$16, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movl	4(%r13), %edi
	leaq	304(%rsp), %rcx
	movl	$-1, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	cmpl	$0, 5624(%r13)
	je	.LBB12_58
# BB#14:
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	movq	5600(%r13), %rdx
	movl	4(%r13), %eax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rdx,%rax)
	movl	96(%rsp), %edi
	movl	$-1, %r11d
	je	.LBB12_72
# BB#15:
	testl	%edi, %edi
	movl	$-1, %r14d
	je	.LBB12_17
# BB#16:
	movslq	100(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	(%rsi), %rsi
	movslq	116(%rsp), %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	112(%rsp), %rbp
	movsbl	(%rsi,%rbp), %r14d
	testl	%r14d, %r14d
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r14d
.LBB12_17:                              # %._crit_edge1297
	movl	176(%rsp), %r8d
	testl	%r8d, %r8d
	je	.LBB12_19
# BB#18:
	movslq	180(%rsp), %rax
	imulq	$408, %rax, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	196(%rsp), %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	192(%rsp), %rsi
	movsbl	(%rax,%rsi), %r11d
	testl	%r11d, %r11d
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r11d
.LBB12_19:                              # %._crit_edge1310
	movl	304(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB12_78
# BB#20:
	movslq	308(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	(%rsi), %rsi
	movslq	324(%rsp), %rbp
	movq	(%rsi,%rbp,8), %rsi
	movslq	320(%rsp), %rbp
	movsbl	(%rsi,%rbp), %r15d
	testl	%r15d, %r15d
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r15d
	jmp	.LBB12_79
.LBB12_21:                              # %.preheader1034
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	356(%rax,%rcx), %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	leaq	(%rax,%rcx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB12_22:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_24 Depth 2
                                        #       Child Loop BB12_25 Depth 3
                                        #         Child Loop BB12_33 Depth 4
                                        #         Child Loop BB12_43 Depth 4
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 328(%rdx,%rax)
	jne	.LBB12_57
# BB#23:                                #   in Loop: Header=BB12_22 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	andl	$-2, %ecx
	leal	(%rax,%rax), %eax
	andl	$2, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movl	%ecx, %r9d
	.p2align	4, 0x90
.LBB12_24:                              #   Parent Loop BB12_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_25 Depth 3
                                        #         Child Loop BB12_33 Depth 4
                                        #         Child Loop BB12_43 Depth 4
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, %r14d
	movq	%r9, 24(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB12_25:                              #   Parent Loop BB12_22 Depth=1
                                        #     Parent Loop BB12_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_33 Depth 4
                                        #         Child Loop BB12_43 Depth 4
	movl	5624(%r13), %edx
	testl	%edx, %edx
	je	.LBB12_28
# BB#26:                                #   in Loop: Header=BB12_25 Depth=3
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB12_28
# BB#27:                                #   in Loop: Header=BB12_25 Depth=3
	movl	4(%r13), %esi
	movl	76(%r13), %r10d
	andl	$1, %esi
	leal	2(%rsi,%rsi), %ecx
	leal	-4(%r10), %eax
	testl	%esi, %esi
	cmovel	%r10d, %eax
	sarl	%eax
	jmp	.LBB12_29
	.p2align	4, 0x90
.LBB12_28:                              # %.thread994
                                        #   in Loop: Header=BB12_25 Depth=3
	movl	76(%r13), %r10d
	xorl	%ecx, %ecx
	movl	%r10d, %eax
.LBB12_29:                              #   in Loop: Header=BB12_25 Depth=3
	movq	(%r15), %rsi
	addl	%r9d, %eax
	movslq	%eax, %rdi
	movq	(%rsi,%rdi,8), %rax
	movl	92(%r13), %r8d
	leal	(%r8,%r14), %esi
	movslq	%esi, %rsi
	xorl	%ebp, %ebp
	cmpb	$-1, (%rax,%rsi)
	sete	%bpl
	movq	(%r15,%rbp,8), %rax
	movq	(%rax,%rdi,8), %rax
	cmpb	$-1, (%rax,%rsi)
	je	.LBB12_40
# BB#30:                                # %.preheader1032
                                        #   in Loop: Header=BB12_25 Depth=3
	movl	5640(%r13), %ebx
	movl	listXsize(,%rcx,4), %eax
	cmpl	%eax, %ebx
	cmovlel	%ebx, %eax
	testl	%eax, %eax
	jle	.LBB12_41
# BB#31:                                # %.lr.ph1153
                                        #   in Loop: Header=BB12_25 Depth=3
	movl	5584(%r13), %r15d
	movq	listX(%rip), %r9
	movq	dec_picture(%rip), %r11
	testl	%edx, %edx
	je	.LBB12_42
# BB#32:                                # %.lr.ph1153.split.preheader
                                        #   in Loop: Header=BB12_25 Depth=3
	movq	48(%rsp), %rdx          # 8-byte Reload
	orl	(%rdx), %r15d
	movslq	%eax, %r13
	imulq	$264, %rcx, %rax        # imm = 0x108
	leaq	24(%r11,%rax), %r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_33:                              # %.lr.ph1153.split
                                        #   Parent Loop BB12_22 Depth=1
                                        #     Parent Loop BB12_24 Depth=2
                                        #       Parent Loop BB12_25 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%r15d, %r15d
	je	.LBB12_35
# BB#34:                                #   in Loop: Header=BB12_33 Depth=4
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	12(%rax), %rax
	imulq	$1584, %rax, %rax       # imm = 0x630
	addq	%r11, %rax
	movq	(%rax,%rdx,8), %rax
	movq	(%r12,%rbp,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	cmpq	(%rcx,%rsi,8), %rax
	jne	.LBB12_37
	jmp	.LBB12_39
	.p2align	4, 0x90
.LBB12_35:                              #   in Loop: Header=BB12_33 Depth=4
	movq	(%r9,%rdx,8), %rcx
	movslq	8(%rcx), %rbx
	addq	%rbx, %rbx
	movq	(%r12,%rbp,8), %rax
	movq	(%rax,%rdi,8), %rax
	movq	(%rax,%rsi,8), %rax
	cmpq	%rax, %rbx
	je	.LBB12_39
# BB#36:                                #   in Loop: Header=BB12_33 Depth=4
	movslq	12(%rcx), %rcx
	addq	%rcx, %rcx
	cmpq	%rax, %rcx
	je	.LBB12_39
.LBB12_37:                              #   in Loop: Header=BB12_33 Depth=4
	incq	%rdx
	cmpq	%r13, %rdx
	jl	.LBB12_33
# BB#38:                                #   in Loop: Header=BB12_25 Depth=3
	movl	$-135792468, %edx       # imm = 0xF7E7F8AC
.LBB12_39:                              # %..thread995.loopexit1724_crit_edge
                                        #   in Loop: Header=BB12_25 Depth=3
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB12_52
	.p2align	4, 0x90
.LBB12_40:                              #   in Loop: Header=BB12_25 Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	addl	%r9d, %r10d
	movslq	%r10d, %rcx
	addq	(%rax,%rcx,8), %rsi
	xorl	%edx, %edx
	jmp	.LBB12_55
	.p2align	4, 0x90
.LBB12_41:                              #   in Loop: Header=BB12_25 Depth=3
	movl	$-1, %edx
	jmp	.LBB12_54
.LBB12_42:                              # %.lr.ph1153.split.us.preheader
                                        #   in Loop: Header=BB12_25 Depth=3
	movslq	%eax, %r12
	imulq	$264, %rcx, %rcx        # imm = 0x108
	leaq	24(%r11,%rcx), %r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_43:                              # %.lr.ph1153.split.us
                                        #   Parent Loop BB12_22 Depth=1
                                        #     Parent Loop BB12_24 Depth=2
                                        #       Parent Loop BB12_25 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	testl	%r15d, %r15d
	je	.LBB12_45
# BB#44:                                #   in Loop: Header=BB12_43 Depth=4
	movslq	12(%r13), %rcx
	imulq	$1584, %rcx, %rcx       # imm = 0x630
	addq	%r11, %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	144(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rax
	movq	(%rax,%rdi,8), %rax
	cmpq	(%rax,%rsi,8), %rcx
	jne	.LBB12_47
	jmp	.LBB12_51
	.p2align	4, 0x90
.LBB12_45:                              #   in Loop: Header=BB12_43 Depth=4
	movq	(%r9,%rdx,8), %rcx
	movslq	8(%rcx), %rax
	addq	%rax, %rax
	movq	144(%rsp), %rbx         # 8-byte Reload
	movq	(%rbx,%rbp,8), %rbx
	movq	(%rbx,%rdi,8), %rbx
	movq	(%rbx,%rsi,8), %r13
	cmpq	%r13, %rax
	je	.LBB12_50
# BB#46:                                #   in Loop: Header=BB12_43 Depth=4
	movslq	12(%rcx), %rax
	addq	%rax, %rax
	cmpq	%r13, %rax
	movq	16(%rsp), %r13          # 8-byte Reload
	je	.LBB12_51
.LBB12_47:                              #   in Loop: Header=BB12_43 Depth=4
	incq	%rdx
	cmpq	%r12, %rdx
	jl	.LBB12_43
# BB#48:                                #   in Loop: Header=BB12_25 Depth=3
	movl	$-135792468, %edx       # imm = 0xF7E7F8AC
	jmp	.LBB12_51
.LBB12_50:                              # %..thread995.loopexit_crit_edge
                                        #   in Loop: Header=BB12_25 Depth=3
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB12_51:                              # %.thread995
                                        #   in Loop: Header=BB12_25 Depth=3
	movq	144(%rsp), %r12         # 8-byte Reload
.LBB12_52:                              # %.thread995
                                        #   in Loop: Header=BB12_25 Depth=3
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	24(%rsp), %r9           # 8-byte Reload
	cmpl	$-135792468, %edx       # imm = 0xF7E7F8AC
	jne	.LBB12_54
# BB#53:                                #   in Loop: Header=BB12_25 Depth=3
	movl	$.L.str.3, %edi
	movl	$-1111, %esi            # imm = 0xFBA9
	callq	error
	movq	24(%rsp), %r9           # 8-byte Reload
	movl	76(%r13), %r10d
	movl	92(%r13), %r8d
	movl	$-135792468, %edx       # imm = 0xF7E7F8AC
.LBB12_54:                              # %.thread995.thread
                                        #   in Loop: Header=BB12_25 Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	addl	%r9d, %r10d
	movslq	%r10d, %rcx
	addl	%r14d, %r8d
	movslq	%r8d, %rsi
	addq	(%rax,%rcx,8), %rsi
.LBB12_55:                              #   in Loop: Header=BB12_25 Depth=3
	movb	%dl, (%rsi)
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	76(%r13), %rcx
	movslq	%r9d, %rdx
	addq	%rcx, %rdx
	movslq	92(%r13), %rcx
	movq	(%rax,%rdx,8), %rax
	movslq	%r14d, %rdx
	addq	%rdx, %rcx
	movb	$0, (%rax,%rcx)
	leal	1(%rdx), %r14d
	cmpl	8(%rsp), %edx           # 4-byte Folded Reload
	jle	.LBB12_25
# BB#56:                                #   in Loop: Header=BB12_24 Depth=2
	cmpl	40(%rsp), %r9d          # 4-byte Folded Reload
	leal	1(%r9), %eax
	movl	%eax, %r9d
	jle	.LBB12_24
.LBB12_57:                              # %.loopexit1033
                                        #   in Loop: Header=BB12_22 Depth=1
	movq	56(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB12_22
	jmp	.LBB12_162
.LBB12_58:
	movl	96(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB12_60
# BB#59:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	116(%rsp), %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	112(%rsp), %rdx
	movsbl	(%rax,%rdx), %r14d
.LBB12_60:
	movq	%rbx, %r9
	movl	176(%rsp), %edx
	movl	$-1, %r15d
	testl	%edx, %edx
	movl	$-1, %r11d
	je	.LBB12_62
# BB#61:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	196(%rsp), %rsi
	movq	(%rax,%rsi,8), %rax
	movslq	192(%rsp), %rsi
	movsbl	(%rax,%rsi), %r11d
.LBB12_62:
	movl	304(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB12_64
# BB#63:
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	(%rsi), %rsi
	movslq	324(%rsp), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	320(%rsp), %rdi
	movsbl	(%rsi,%rdi), %r15d
.LBB12_64:
	movl	280(%rsp), %eax
	testl	%eax, %eax
	je	.LBB12_66
# BB#65:
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	(%rsi), %rsi
	movslq	300(%rsp), %rdi
	movq	(%rsi,%rdi,8), %rsi
	movslq	296(%rsp), %rdi
	movsbl	(%rsi,%rdi), %r15d
.LBB12_66:
	movl	$-1, %esi
	testl	%ecx, %ecx
	movl	$-1, %edi
	je	.LBB12_68
# BB#67:
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	116(%rsp), %rdi
	movq	(%rcx,%rdi,8), %rcx
	movslq	112(%rsp), %rdi
	movsbl	(%rcx,%rdi), %edi
.LBB12_68:
	testl	%edx, %edx
	je	.LBB12_70
# BB#69:
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	196(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	192(%rsp), %rdx
	movsbl	(%rcx,%rdx), %esi
.LBB12_70:
	testl	%ebx, %ebx
	je	.LBB12_90
# BB#71:
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	324(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	320(%rsp), %rdx
	movsbl	(%rcx,%rdx), %ebp
	jmp	.LBB12_91
.LBB12_72:
	testl	%edi, %edi
	movl	$-1, %r14d
	je	.LBB12_74
# BB#73:
	movslq	100(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 356(%rdx,%rcx)
	setne	%bl
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	116(%rsp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	112(%rsp), %rsi
	movsbl	(%rcx,%rsi), %r14d
	movl	%r14d, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %r14d
.LBB12_74:                              # %._crit_edge1419
	movl	176(%rsp), %r8d
	testl	%r8d, %r8d
	je	.LBB12_76
# BB#75:
	movslq	180(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rdx,%rax)
	setne	%bl
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	196(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	192(%rsp), %rcx
	movsbl	(%rax,%rcx), %r11d
	movl	%r11d, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %r11d
.LBB12_76:                              # %._crit_edge1434
	movl	304(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB12_93
# BB#77:
	movslq	308(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 356(%rdx,%rcx)
	setne	%bl
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	324(%rsp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	320(%rsp), %rsi
	movsbl	(%rcx,%rsi), %r15d
	movl	%r15d, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %r15d
	jmp	.LBB12_94
.LBB12_78:
	movl	$-1, %r15d
.LBB12_79:                              # %._crit_edge1325
	movl	280(%rsp), %r10d
	testl	%r10d, %r10d
	je	.LBB12_81
# BB#80:
	movslq	284(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	(%rsi), %rsi
	movslq	300(%rsp), %rbx
	movq	(%rsi,%rbx,8), %rsi
	movslq	296(%rsp), %rbx
	movsbl	(%rsi,%rbx), %r15d
	testl	%r15d, %r15d
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %r15d
.LBB12_81:                              # %._crit_edge1340
	movl	$-1, %esi
	testl	%edi, %edi
	movl	$-1, %edi
	je	.LBB12_83
# BB#82:
	movslq	100(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rdi
	movq	316952(%rdi), %rdi
	movq	8(%rdi), %rdi
	movslq	116(%rsp), %rbx
	movq	(%rdi,%rbx,8), %rdi
	movslq	112(%rsp), %rbx
	movsbl	(%rdi,%rbx), %edi
	testl	%edi, %edi
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %edi
.LBB12_83:                              # %._crit_edge1355
	testl	%r8d, %r8d
	je	.LBB12_85
# BB#84:
	movslq	180(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	8(%rsi), %rsi
	movslq	196(%rsp), %rbx
	movq	(%rsi,%rbx,8), %rsi
	movslq	192(%rsp), %rbx
	movsbl	(%rsi,%rbx), %esi
	testl	%esi, %esi
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %esi
.LBB12_85:                              # %._crit_edge1371
	testl	%r9d, %r9d
	je	.LBB12_87
# BB#86:
	movslq	308(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rbx
	movq	316952(%rbx), %rbx
	movq	8(%rbx), %rbx
	movslq	324(%rsp), %rbp
	movq	(%rbx,%rbp,8), %rbp
	movslq	320(%rsp), %rbx
	movsbl	(%rbp,%rbx), %eax
	testl	%eax, %eax
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %eax
	testl	%r10d, %r10d
	jne	.LBB12_88
	jmp	.LBB12_89
.LBB12_87:
	movl	$-1, %eax
	testl	%r10d, %r10d
	je	.LBB12_89
.LBB12_88:
	movslq	284(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rbp
	movq	316952(%rbp), %rbp
	movq	8(%rbp), %rbp
	movslq	300(%rsp), %rbx
	movq	(%rbp,%rbx,8), %rbp
	movslq	296(%rsp), %rbx
	movsbl	(%rbp,%rbx), %ebp
	testl	%ebp, %ebp
	setns	%bl
	cmpl	$0, 356(%rdx,%rcx)
	sete	%cl
	andb	%bl, %cl
	shll	%cl, %ebp
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB12_106
.LBB12_89:
	movq	8(%rsp), %rbx           # 8-byte Reload
	jmp	.LBB12_105
.LBB12_90:
	movl	$-1, %ebp
.LBB12_91:
	movq	%r9, %rbx
	testl	%eax, %eax
	je	.LBB12_106
# BB#92:
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	300(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	296(%rsp), %rdx
	movsbl	(%rcx,%rdx), %ebp
	jmp	.LBB12_106
.LBB12_93:
	movl	$-1, %r15d
.LBB12_94:                              # %._crit_edge1449
	movl	280(%rsp), %r10d
	testl	%r10d, %r10d
	je	.LBB12_96
# BB#95:
	movslq	284(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 356(%rdx,%rcx)
	setne	%bl
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	300(%rsp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	296(%rsp), %rsi
	movsbl	(%rcx,%rsi), %r15d
	movl	%r15d, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %r15d
.LBB12_96:                              # %._crit_edge1464
	movl	$-1, %esi
	testl	%edi, %edi
	movl	$-1, %edi
	je	.LBB12_98
# BB#97:
	movslq	100(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 356(%rdx,%rcx)
	setne	%bl
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	116(%rsp), %rdi
	movq	(%rcx,%rdi,8), %rcx
	movslq	112(%rsp), %rdi
	movsbl	(%rcx,%rdi), %edi
	movl	%edi, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %edi
.LBB12_98:                              # %._crit_edge1479
	testl	%r8d, %r8d
	je	.LBB12_100
# BB#99:
	movslq	180(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 356(%rdx,%rcx)
	setne	%bl
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	196(%rsp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	192(%rsp), %rsi
	movsbl	(%rcx,%rsi), %esi
	movl	%esi, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %esi
.LBB12_100:                             # %._crit_edge1495
	testl	%r9d, %r9d
	je	.LBB12_102
# BB#101:
	movslq	308(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 356(%rdx,%rcx)
	setne	%bl
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	324(%rsp), %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	320(%rsp), %rbp
	movsbl	(%rcx,%rbp), %eax
	movl	%eax, %ecx
	shrb	$7, %cl
	orb	%bl, %cl
	sarl	%cl, %eax
	jmp	.LBB12_103
.LBB12_102:
	movl	$-1, %eax
.LBB12_103:                             # %._crit_edge1511
	movq	8(%rsp), %rbx           # 8-byte Reload
	testl	%r10d, %r10d
	je	.LBB12_105
# BB#104:
	movslq	284(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	cmpl	$0, 356(%rdx,%rcx)
	setne	%dl
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	300(%rsp), %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	296(%rsp), %rbp
	movsbl	(%rcx,%rbp), %ebp
	movl	%ebp, %ecx
	shrb	$7, %cl
	orb	%dl, %cl
	sarl	%cl, %ebp
	jmp	.LBB12_106
.LBB12_105:
	movl	%eax, %ebp
.LBB12_106:                             # %._crit_edge1403
	cmpl	%r11d, %r14d
	setg	%dl
	setl	%cl
	movl	%r11d, %eax
	orl	%r14d, %eax
	jns	.LBB12_108
# BB#107:                               # %._crit_edge1403
	movl	%edx, %ecx
.LBB12_108:                             # %._crit_edge1403
	testb	%cl, %cl
	cmovnel	%r14d, %r11d
	movsbl	%r11b, %ecx
	testl	%r15d, %r15d
	js	.LBB12_111
# BB#109:                               # %._crit_edge1403
	shll	$24, %r11d
	cmpl	$-16777215, %r11d       # imm = 0xFF000001
	jl	.LBB12_111
# BB#110:
	movl	%r15d, %r11d
	cmpl	%r11d, %ecx
	setl	%al
	jmp	.LBB12_112
.LBB12_111:
	movl	%r15d, %r11d
	cmpl	%r11d, %ecx
	setg	%al
.LBB12_112:
	testb	%al, %al
	cmovnel	%ecx, %r11d
	cmpl	%esi, %edi
	setg	%cl
	setl	%al
	movl	%esi, %edx
	orl	%edi, %edx
	jns	.LBB12_114
# BB#113:
	movl	%ecx, %eax
.LBB12_114:
	testb	%al, %al
	cmovnel	%edi, %esi
	movsbl	%sil, %eax
	testl	%ebp, %ebp
	js	.LBB12_117
# BB#115:
	shll	$24, %esi
	cmpl	$-16777215, %esi        # imm = 0xFF000001
	jl	.LBB12_117
# BB#116:
	cmpl	%ebp, %eax
	setl	%cl
	jmp	.LBB12_118
.LBB12_117:
	cmpl	%ebp, %eax
	setg	%cl
.LBB12_118:
	testb	%cl, %cl
	cmovnel	%eax, %ebp
	movl	%r11d, %r14d
	shll	$24, %r14d
	cmpl	$-16777215, %r14d       # imm = 0xFF000001
	movl	%r11d, 40(%rsp)         # 4-byte Spill
	jl	.LBB12_120
# BB#119:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %r8
	movq	316976(%rax), %r9
	movsbl	%r11b, %edx
	leaq	260(%rsp), %rsi
	movl	$0, %ecx
	movq	%r13, %rdi
	pushq	$16
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	movl	72(%rsp), %r11d         # 4-byte Reload
	addq	$32, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset -32
.LBB12_120:
	movl	%ebp, %r15d
	shll	$24, %r15d
	cmpl	$-16777215, %r15d       # imm = 0xFF000001
	movl	%ebp, 56(%rsp)          # 4-byte Spill
	jl	.LBB12_122
# BB#121:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %r8
	movq	316976(%rax), %r9
	movsbl	%bpl, %edx
	leaq	256(%rsp), %rsi
	movl	$1, %ecx
	movq	%r13, %rdi
	pushq	$16
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	movl	72(%rsp), %r11d         # 4-byte Reload
	addq	$32, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -32
	movw	256(%rsp), %ax
	movl	%eax, 232(%rsp)         # 4-byte Spill
	movw	258(%rsp), %ax
	movl	%eax, 240(%rsp)         # 4-byte Spill
	jmp	.LBB12_123
.LBB12_122:
	movl	$0, 240(%rsp)           # 4-byte Folded Spill
	movl	$0, 232(%rsp)           # 4-byte Folded Spill
.LBB12_123:                             # %.preheader1037
	movq	368(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	orl	$1, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movzwl	260(%rsp), %eax
	movw	%ax, 88(%rsp)           # 2-byte Spill
	movzwl	262(%rsp), %eax
	movw	%ax, 272(%rsp)          # 2-byte Spill
	movl	%r15d, %r13d
	andl	%r14d, %r13d
	movslq	%r12d, %rax
	leaq	(%rbx,%rax,8), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB12_124:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_130 Depth 2
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	movq	216(%rsp), %rax         # 8-byte Reload
	cmpb	$0, 328(%rdx,%rax)
	jne	.LBB12_160
# BB#125:                               #   in Loop: Header=BB12_124 Depth=1
	movq	264(%rsp), %rax         # 8-byte Reload
	movl	%eax, %r12d
	andl	$-2, %r12d
	leal	(%rax,%rax), %eax
	andl	$2, %eax
	movslq	%r12d, %rcx
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leal	1(%rax), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leaq	-1(%rcx), %r10
	jmp	.LBB12_130
.LBB12_126:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rsi
	movq	316976(%rsi), %rcx
	movq	(%rcx), %rcx
	movslq	%r9d, %rdi
	movq	(%rcx,%rdi,8), %rdx
	leaq	(%rdx,%rax,8), %r8
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	jmp	.LBB12_136
.LBB12_127:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rsi
	movq	316976(%rsi), %rcx
	movq	8(%rcx), %rdx
	movslq	%r9d, %rdi
	leaq	(%rdx,%rdi,8), %rbx
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	xorl	%ecx, %ecx
	jmp	.LBB12_142
.LBB12_128:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rax
	movq	(%rax), %rax
	movslq	%r8d, %rsi
	movq	(%rax,%rsi,8), %rdx
	leaq	(%rdx,%r9,8), %rdi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	jmp	.LBB12_151
.LBB12_129:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rax
	movq	8(%rax), %rdx
	movslq	%r8d, %rsi
	leaq	(%rdx,%rsi,8), %rdi
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	xorl	%ebp, %ebp
	jmp	.LBB12_157
	.p2align	4, 0x90
.LBB12_130:                             #   Parent Loop BB12_124 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	76(%rax), %r9d
	addl	%r12d, %r9d
	movslq	92(%rax), %rcx
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	addq	%rcx, %rax
	cmpl	$-16777215, %r14d       # imm = 0xFF000001
	jl	.LBB12_135
# BB#131:                               #   in Loop: Header=BB12_130 Depth=2
	testb	%r11b, %r11b
	jne	.LBB12_134
# BB#132:                               #   in Loop: Header=BB12_130 Depth=2
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	8(%rcx,%r10,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB12_134
# BB#133:                               #   in Loop: Header=BB12_130 Depth=2
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	listX(,%rcx,8), %rcx
	movq	(%rcx), %rcx
	cmpl	$0, 316844(%rcx)
	je	.LBB12_126
	.p2align	4, 0x90
.LBB12_134:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rsi
	movq	316976(%rsi), %rcx
	movq	(%rcx), %rcx
	movslq	%r9d, %rdi
	movq	(%rcx,%rdi,8), %rdx
	leaq	(%rdx,%rax,8), %r8
	movl	%r11d, %ecx
	movzwl	88(%rsp), %ebp          # 2-byte Folded Reload
	movw	%bp, %r11w
	movzwl	272(%rsp), %ebx         # 2-byte Folded Reload
                                        # kill: %BX<def> %BX<kill> %EBX<def>
                                        # kill: %CL<def> %CL<kill> %ECX<kill> %ECX<def>
	jmp	.LBB12_136
	.p2align	4, 0x90
.LBB12_135:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rsi
	movq	316976(%rsi), %rcx
	movq	(%rcx), %rcx
	movslq	%r9d, %rdi
	movq	(%rcx,%rdi,8), %rdx
	cltq
	leaq	(%rdx,%rax,8), %r8
	movb	$-1, %cl
	xorl	%r11d, %r11d
	xorl	%ebx, %ebx
.LBB12_136:                             #   in Loop: Header=BB12_130 Depth=2
	movq	(%r8), %rbp
	movw	%r11w, (%rbp)
	movq	(%rdx,%rax,8), %rdx
	movw	%bx, 2(%rdx)
	movq	316952(%rsi), %rdx
	movq	(%rdx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movb	%cl, (%rdx,%rax)
	cmpl	$-16777215, %r15d       # imm = 0xFF000001
	jl	.LBB12_141
# BB#137:                               #   in Loop: Header=BB12_130 Depth=2
	testl	%r15d, %r15d
	movl	40(%rsp), %r11d         # 4-byte Reload
	jne	.LBB12_140
# BB#138:                               #   in Loop: Header=BB12_130 Depth=2
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	8(%rcx,%r10,8), %rcx
	cmpb	$0, (%rcx,%rax)
	jne	.LBB12_140
# BB#139:                               #   in Loop: Header=BB12_130 Depth=2
	movq	160(%rsp), %rcx         # 8-byte Reload
	movq	listX(,%rcx,8), %rcx
	movq	(%rcx), %rcx
	cmpl	$0, 316844(%rcx)
	je	.LBB12_127
	.p2align	4, 0x90
.LBB12_140:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rsi
	movq	316976(%rsi), %rcx
	movq	8(%rcx), %rdx
	movslq	%r9d, %rdi
	leaq	(%rdx,%rdi,8), %rbx
	movl	232(%rsp), %ebp         # 4-byte Reload
                                        # kill: %BP<def> %BP<kill> %EBP<kill> %EBP<def>
	movl	240(%rsp), %r8d         # 4-byte Reload
                                        # kill: %R8W<def> %R8W<kill> %R8D<kill> %R8D<def>
	movl	56(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill> %ECX<def>
	jmp	.LBB12_142
	.p2align	4, 0x90
.LBB12_141:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rsi
	movq	316976(%rsi), %rcx
	movq	8(%rcx), %rdx
	movslq	%r9d, %rdi
	leaq	(%rdx,%rdi,8), %rbx
	movb	$-1, %cl
	xorl	%ebp, %ebp
	xorl	%r8d, %r8d
	movl	40(%rsp), %r11d         # 4-byte Reload
.LBB12_142:                             #   in Loop: Header=BB12_130 Depth=2
	movq	(%rbx), %rbx
	movq	(%rbx,%rax,8), %rbx
	movw	%bp, (%rbx)
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx,%rax,8), %rdx
	movw	%r8w, 2(%rdx)
	movq	316952(%rsi), %rdx
	movq	8(%rdx), %rdx
	movq	(%rdx,%rdi,8), %rdx
	movb	%cl, (%rdx,%rax)
	testl	%r13d, %r13d
	jns	.LBB12_144
# BB#143:                               #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	%r9d, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movb	$0, (%rcx,%rax)
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movb	$0, (%rcx,%rax)
.LBB12_144:                             #   in Loop: Header=BB12_130 Depth=2
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	76(%rax), %r8d
	addl	%r12d, %r8d
	movslq	92(%rax), %rax
	movslq	24(%rsp), %r9           # 4-byte Folded Reload
	addq	%rax, %r9
	cmpl	$-16777216, %r14d       # imm = 0xFF000000
	jle	.LBB12_147
# BB#145:                               #   in Loop: Header=BB12_130 Depth=2
	testb	%r11b, %r11b
	je	.LBB12_148
# BB#146:                               # %._crit_edge1626.1
                                        #   in Loop: Header=BB12_130 Depth=2
	movslq	%r9d, %r9
	jmp	.LBB12_150
	.p2align	4, 0x90
.LBB12_147:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rax
	movq	(%rax), %rax
	movslq	%r8d, %rsi
	movq	(%rax,%rsi,8), %rdx
	leaq	(%rdx,%r9,8), %rdi
	movb	$-1, %bpl
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	jmp	.LBB12_151
.LBB12_148:                             #   in Loop: Header=BB12_130 Depth=2
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	8(%rax,%r10,8), %rax
	cmpb	$0, (%rax,%r9)
	jne	.LBB12_150
# BB#149:                               #   in Loop: Header=BB12_130 Depth=2
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rax
	movq	(%rax), %rax
	cmpl	$0, 316844(%rax)
	je	.LBB12_128
	.p2align	4, 0x90
.LBB12_150:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rax
	movq	(%rax), %rax
	movslq	%r8d, %rsi
	movq	(%rax,%rsi,8), %rdx
	leaq	(%rdx,%r9,8), %rdi
	movzwl	88(%rsp), %eax          # 2-byte Folded Reload
	movw	%ax, %bx
	movzwl	272(%rsp), %eax         # 2-byte Folded Reload
                                        # kill: %AX<def> %AX<kill> %EAX<def>
	movb	%r11b, %bpl
.LBB12_151:                             #   in Loop: Header=BB12_130 Depth=2
	cmpl	$-16777216, %r15d       # imm = 0xFF000000
	movq	(%rdi), %rdi
	movw	%bx, (%rdi)
	movq	(%rdx,%r9,8), %rdx
	movw	%ax, 2(%rdx)
	movq	316952(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movb	%bpl, (%rax,%r9)
	jle	.LBB12_156
# BB#152:                               #   in Loop: Header=BB12_130 Depth=2
	testl	%r15d, %r15d
	jne	.LBB12_155
# BB#153:                               #   in Loop: Header=BB12_130 Depth=2
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	8(%rax,%r10,8), %rax
	cmpb	$0, (%rax,%r9)
	jne	.LBB12_155
# BB#154:                               #   in Loop: Header=BB12_130 Depth=2
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rax
	movq	(%rax), %rax
	cmpl	$0, 316844(%rax)
	je	.LBB12_129
	.p2align	4, 0x90
.LBB12_155:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rax
	movq	8(%rax), %rdx
	movslq	%r8d, %rsi
	leaq	(%rdx,%rsi,8), %rdi
	movl	232(%rsp), %ebx         # 4-byte Reload
                                        # kill: %BX<def> %BX<kill> %EBX<kill> %EBX<def>
	movl	240(%rsp), %eax         # 4-byte Reload
                                        # kill: %AX<def> %AX<kill> %EAX<kill> %EAX<def>
	movl	56(%rsp), %ebp          # 4-byte Reload
                                        # kill: %BPL<def> %BPL<kill> %EBP<kill> %EBP<def>
	jmp	.LBB12_157
	.p2align	4, 0x90
.LBB12_156:                             #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rax
	movq	8(%rax), %rdx
	movslq	%r8d, %rsi
	leaq	(%rdx,%rsi,8), %rdi
	movb	$-1, %bpl
	xorl	%ebx, %ebx
	xorl	%eax, %eax
.LBB12_157:                             #   in Loop: Header=BB12_130 Depth=2
	movq	(%rdi), %rdi
	movq	(%rdi,%r9,8), %rdi
	movw	%bx, (%rdi)
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movw	%ax, 2(%rdx)
	movq	316952(%rcx), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movb	%bpl, (%rax,%r9)
	testl	%r13d, %r13d
	jns	.LBB12_159
# BB#158:                               #   in Loop: Header=BB12_130 Depth=2
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	%r8d, %rcx
	movq	(%rax,%rcx,8), %rax
	movb	$0, (%rax,%r9)
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movb	$0, (%rax,%r9)
.LBB12_159:                             #   in Loop: Header=BB12_130 Depth=2
	incq	%r10
	incl	%r12d
	cmpq	48(%rsp), %r10          # 8-byte Folded Reload
	jle	.LBB12_130
.LBB12_160:                             # %.loopexit1036
                                        #   in Loop: Header=BB12_124 Depth=1
	movq	264(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	cmpq	$4, %rdx
	jne	.LBB12_124
# BB#161:
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB12_162:                             # %.loopexit1035
	movq	328(%rsp), %rax         # 8-byte Reload
	movslq	BLOCK_STEP+4(,%rax,8), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	5640(%r13), %eax
	cmpl	$1, %eax
	jle	.LBB12_166
# BB#163:
	movl	%eax, 40(%rsp)          # 4-byte Spill
	movl	$3, 96(%rsp)
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movslq	assignSE2partition+12(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rsi
	movq	active_pps(%rip), %rdx
	cmpl	$0, 12(%rdx)
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	je	.LBB12_165
# BB#164:
	movq	(%rsi), %rdx
	cmpl	$0, 24(%rdx)
	je	.LBB12_175
.LBB12_165:                             # %._crit_edge1634
	movq	$linfo_ue, 128(%rsp)
	jmp	.LBB12_176
.LBB12_166:                             # %.preheader1030
	xorl	%r13d, %r13d
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %r12
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_167:                             # %.preheader1029
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_168 Depth 2
                                        #       Child Loop BB12_172 Depth 3
	movl	%r13d, %r15d
	andl	$-2, %r15d
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	%r13, 8(%rsp)           # 8-byte Spill
	leal	(%rax,%r13), %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_168:                             #   Parent Loop BB12_167 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_172 Depth 3
	movl	%ebx, %eax
	sarl	%eax
	addl	%r15d, %eax
	cltq
	movb	332(%rax,%r12), %cl
	orb	$2, %cl
	cmpb	$2, %cl
	jne	.LBB12_173
# BB#169:                               #   in Loop: Header=BB12_168 Depth=2
	cmpb	$0, 328(%rax,%r12)
	je	.LBB12_173
# BB#170:                               #   in Loop: Header=BB12_168 Depth=2
	movl	76(%r14), %eax
	leal	(%r13,%rax), %ecx
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cmpl	%ecx, %eax
	jge	.LBB12_173
# BB#171:                               # %.lr.ph1149.preheader
                                        #   in Loop: Header=BB12_168 Depth=2
	movslq	%eax, %rbp
	.p2align	4, 0x90
.LBB12_172:                             # %.lr.ph1149
                                        #   Parent Loop BB12_167 Depth=1
                                        #     Parent Loop BB12_168 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	92(%r14), %rcx
	movslq	%ebx, %rdi
	addq	%rcx, %rdi
	addq	(%rax,%rbp,8), %rdi
	xorl	%esi, %esi
	callq	memset
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rbp
	movl	76(%r14), %eax
	addl	%r13d, %eax
	cltq
	cmpq	%rax, %rbp
	jl	.LBB12_172
.LBB12_173:                             # %.loopexit1028
                                        #   in Loop: Header=BB12_168 Depth=2
	addl	%edx, %ebx
	cmpl	$4, %ebx
	jl	.LBB12_168
# BB#174:                               #   in Loop: Header=BB12_167 Depth=1
	cmpl	$4, %r13d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jl	.LBB12_167
	jmp	.LBB12_211
.LBB12_175:
	movq	$readRefFrame_CABAC, 136(%rsp)
.LBB12_176:                             # %.preheader1026
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	leaq	40(%rsi,%rdi), %rbp
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	leaq	48(%rax,%rcx), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	leaq	(%rsi,%rdi), %r14
	.p2align	4, 0x90
.LBB12_177:                             # %.preheader1025
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_193 Depth 2
                                        #       Child Loop BB12_208 Depth 3
                                        #     Child Loop BB12_179 Depth 2
                                        #       Child Loop BB12_190 Depth 3
	movl	%r15d, %eax
	andl	$-2, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	%r15, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%r15), %r15d
	cmpl	$1, 224(%rsp)           # 4-byte Folded Reload
	jne	.LBB12_192
# BB#178:                               # %.preheader1025.split.us.preheader
                                        #   in Loop: Header=BB12_177 Depth=1
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB12_179:                             # %.preheader1025.split.us
                                        #   Parent Loop BB12_177 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_190 Depth 3
	movl	%r12d, %eax
	sarl	%eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cltq
	movb	332(%rax,%r14), %cl
	orb	$2, %cl
	cmpb	$2, %cl
	movq	16(%rsp), %rcx          # 8-byte Reload
	jne	.LBB12_191
# BB#180:                               #   in Loop: Header=BB12_179 Depth=2
	cmpb	$0, 328(%rax,%r14)
	je	.LBB12_191
# BB#181:                               #   in Loop: Header=BB12_179 Depth=2
	leaq	328(%rax,%r14), %rax
	movl	%r12d, 5608(%rcx)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, 5612(%rcx)
	xorl	%ecx, %ecx
	cmpb	$3, (%rax)
	setg	%cl
	movl	%ecx, 120(%rsp)
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	je	.LBB12_184
# BB#182:                               #   in Loop: Header=BB12_179 Depth=2
	cmpl	$2, 40(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	jne	.LBB12_187
# BB#183:                               #   in Loop: Header=BB12_179 Depth=2
	movq	(%rdx), %rsi
	movl	24(%rsi), %eax
	testl	%eax, %eax
	jne	.LBB12_186
	jmp	.LBB12_187
.LBB12_184:                             #   in Loop: Header=BB12_179 Depth=2
	cmpl	$2, 40(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	jne	.LBB12_187
# BB#185:                               # %._crit_edge1553
                                        #   in Loop: Header=BB12_179 Depth=2
	movq	(%rdx), %rsi
.LBB12_186:                             #   in Loop: Header=BB12_179 Depth=2
	movl	$1, 108(%rsp)
	leaq	96(%rsp), %rdi
	callq	readSyntaxElement_FLC
	movl	$1, %ebp
	subl	100(%rsp), %ebp
	movl	%ebp, 100(%rsp)
	movq	16(%rsp), %r13          # 8-byte Reload
	jmp	.LBB12_188
	.p2align	4, 0x90
.LBB12_187:                             #   in Loop: Header=BB12_179 Depth=2
	movl	$0, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rsi
	movq	88(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movl	100(%rsp), %ebp
.LBB12_188:                             #   in Loop: Header=BB12_179 Depth=2
	movl	76(%r13), %eax
	leal	(%r15,%rax), %ecx
	addl	%ebx, %eax
	cmpl	%ecx, %eax
	movq	32(%rsp), %rdx          # 8-byte Reload
	jge	.LBB12_191
# BB#189:                               # %.lr.ph1142.us.preheader
                                        #   in Loop: Header=BB12_179 Depth=2
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB12_190:                             # %.lr.ph1142.us
                                        #   Parent Loop BB12_177 Depth=1
                                        #     Parent Loop BB12_179 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	92(%r13), %rcx
	movslq	%r12d, %rdi
	addq	%rcx, %rdi
	addq	(%rax,%rbx,8), %rdi
	movzbl	%bpl, %esi
	callq	memset
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rbx
	movl	76(%r13), %eax
	addl	%r15d, %eax
	cltq
	cmpq	%rax, %rbx
	jl	.LBB12_190
.LBB12_191:                             # %.loopexit1024.us
                                        #   in Loop: Header=BB12_179 Depth=2
	addl	%edx, %r12d
	cmpl	$4, %r12d
	jl	.LBB12_179
	jmp	.LBB12_210
	.p2align	4, 0x90
.LBB12_192:                             # %.preheader1025.split.preheader
                                        #   in Loop: Header=BB12_177 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB12_193:                             # %.preheader1025.split
                                        #   Parent Loop BB12_177 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_208 Depth 3
	movl	%r13d, %eax
	sarl	%eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cltq
	movb	332(%rax,%r14), %cl
	orb	$2, %cl
	cmpb	$2, %cl
	movq	16(%rsp), %rcx          # 8-byte Reload
	jne	.LBB12_209
# BB#194:                               #   in Loop: Header=BB12_193 Depth=2
	cmpb	$0, 328(%rax,%r14)
	je	.LBB12_209
# BB#195:                               #   in Loop: Header=BB12_193 Depth=2
	movl	%r13d, 5608(%rcx)
	movq	48(%rsp), %rsi          # 8-byte Reload
	movl	%esi, 5612(%rcx)
	movq	56(%rsp), %rcx          # 8-byte Reload
	cmpl	$8, (%rcx)
	jne	.LBB12_198
# BB#196:                               #   in Loop: Header=BB12_193 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 100(%rcx)
	je	.LBB12_198
# BB#197:                               #   in Loop: Header=BB12_193 Depth=2
	xorl	%r12d, %r12d
	jmp	.LBB12_206
	.p2align	4, 0x90
.LBB12_198:                             #   in Loop: Header=BB12_193 Depth=2
	leaq	328(%rax,%r14), %rax
	xorl	%ecx, %ecx
	cmpb	$3, (%rax)
	setg	%cl
	movl	%ecx, 120(%rsp)
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	movq	24(%rsp), %rdx          # 8-byte Reload
	je	.LBB12_201
# BB#199:                               #   in Loop: Header=BB12_193 Depth=2
	cmpl	$2, 40(%rsp)            # 4-byte Folded Reload
	jne	.LBB12_204
# BB#200:                               #   in Loop: Header=BB12_193 Depth=2
	movq	(%rdx), %rsi
	movl	24(%rsi), %eax
	testl	%eax, %eax
	jne	.LBB12_203
	jmp	.LBB12_204
.LBB12_201:                             #   in Loop: Header=BB12_193 Depth=2
	cmpl	$2, 40(%rsp)            # 4-byte Folded Reload
	jne	.LBB12_204
# BB#202:                               # %._crit_edge1550
                                        #   in Loop: Header=BB12_193 Depth=2
	movq	(%rdx), %rsi
.LBB12_203:                             #   in Loop: Header=BB12_193 Depth=2
	movl	$1, 108(%rsp)
	leaq	96(%rsp), %rdi
	callq	readSyntaxElement_FLC
	movl	$1, %r12d
	subl	100(%rsp), %r12d
	movl	%r12d, 100(%rsp)
	jmp	.LBB12_205
.LBB12_204:                             #   in Loop: Header=BB12_193 Depth=2
	movl	$0, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	88(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movl	100(%rsp), %r12d
.LBB12_205:                             #   in Loop: Header=BB12_193 Depth=2
	movq	32(%rsp), %rdx          # 8-byte Reload
.LBB12_206:                             #   in Loop: Header=BB12_193 Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	76(%rbx), %eax
	leal	(%r15,%rax), %ecx
	addl	48(%rsp), %eax          # 4-byte Folded Reload
	cmpl	%ecx, %eax
	jge	.LBB12_209
# BB#207:                               # %.lr.ph1142.preheader
                                        #   in Loop: Header=BB12_193 Depth=2
	movslq	%eax, %rbp
	.p2align	4, 0x90
.LBB12_208:                             # %.lr.ph1142
                                        #   Parent Loop BB12_177 Depth=1
                                        #     Parent Loop BB12_193 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movslq	92(%rbx), %rcx
	movslq	%r13d, %rdi
	addq	%rcx, %rdi
	addq	(%rax,%rbp,8), %rdi
	movzbl	%r12b, %esi
	callq	memset
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rbp
	movl	76(%rbx), %eax
	addl	%r15d, %eax
	cltq
	cmpq	%rax, %rbp
	jl	.LBB12_208
.LBB12_209:                             # %.loopexit1024
                                        #   in Loop: Header=BB12_193 Depth=2
	addl	%edx, %r13d
	cmpl	$4, %r13d
	jl	.LBB12_193
.LBB12_210:                             # %.us-lcssa1144.us
                                        #   in Loop: Header=BB12_177 Depth=1
	cmpl	$4, %r15d
                                        # kill: %R15D<def> %R15D<kill> %R15<kill> %R15<def>
	jl	.LBB12_177
.LBB12_211:                             # %.loopexit1027
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	5644(%rax), %ebx
	cmpl	$1, %ebx
	jle	.LBB12_215
# BB#212:
	movl	$3, 96(%rsp)
	movq	200(%rsp), %rbp         # 8-byte Reload
	movq	40(%rbp), %r8
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movslq	assignSE2partition+12(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%r8,%rcx), %rax
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	je	.LBB12_214
# BB#213:
	movq	(%rax), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB12_225
.LBB12_214:                             # %._crit_edge1633
	movq	$linfo_ue, 128(%rsp)
	jmp	.LBB12_226
.LBB12_215:                             # %.preheader1022
	xorl	%r13d, %r13d
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rax,%rcx), %r12
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_216:                             # %.preheader1021
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_217 Depth 2
                                        #       Child Loop BB12_221 Depth 3
	movl	%r13d, %r15d
	andl	$-2, %r15d
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	%r13, 8(%rsp)           # 8-byte Spill
	leal	(%rax,%r13), %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_217:                             #   Parent Loop BB12_216 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_221 Depth 3
	movl	%ebx, %eax
	sarl	%eax
	addl	%r15d, %eax
	cltq
	movb	332(%rax,%r12), %cl
	decb	%cl
	cmpb	$1, %cl
	ja	.LBB12_222
# BB#218:                               #   in Loop: Header=BB12_217 Depth=2
	cmpb	$0, 328(%rax,%r12)
	je	.LBB12_222
# BB#219:                               #   in Loop: Header=BB12_217 Depth=2
	movl	76(%r14), %eax
	leal	(%r13,%rax), %ecx
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cmpl	%ecx, %eax
	jge	.LBB12_222
# BB#220:                               # %.lr.ph1137.preheader
                                        #   in Loop: Header=BB12_217 Depth=2
	movslq	%eax, %rbp
	.p2align	4, 0x90
.LBB12_221:                             # %.lr.ph1137
                                        #   Parent Loop BB12_216 Depth=1
                                        #     Parent Loop BB12_217 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	92(%r14), %rcx
	movslq	%ebx, %rdi
	addq	%rcx, %rdi
	addq	(%rax,%rbp,8), %rdi
	xorl	%esi, %esi
	callq	memset
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rbp
	movl	76(%r14), %eax
	addl	%r13d, %eax
	cltq
	cmpq	%rax, %rbp
	jl	.LBB12_221
	.p2align	4, 0x90
.LBB12_222:                             # %.loopexit1020
                                        #   in Loop: Header=BB12_217 Depth=2
	addl	%edx, %ebx
	cmpl	$4, %ebx
	jl	.LBB12_217
# BB#223:                               #   in Loop: Header=BB12_216 Depth=1
	cmpl	$4, %r13d
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	jl	.LBB12_216
# BB#224:                               # %.loopexit1019.loopexit1191
	movq	200(%rsp), %rax         # 8-byte Reload
	addq	$40, %rax
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB12_248
.LBB12_225:
	movq	$readRefFrame_CABAC, 136(%rsp)
.LBB12_226:                             # %.preheader1018
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rax, 24(%rsp)          # 8-byte Spill
	addq	$40, %rbp
	movq	%rbp, 200(%rsp)         # 8-byte Spill
	leaq	48(%r8,%rcx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	leaq	(%rsi,%rdi), %r13
	movl	%ebx, 56(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB12_227:                             # %.preheader1017
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_240 Depth 2
                                        #       Child Loop BB12_244 Depth 3
                                        #     Child Loop BB12_229 Depth 2
                                        #       Child Loop BB12_237 Depth 3
	movl	%r14d, %eax
	andl	$-2, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	%r14, 48(%rsp)          # 8-byte Spill
	leal	(%rax,%r14), %r14d
	cmpl	$2, %ebx
	jne	.LBB12_239
# BB#228:                               # %.preheader1017.split.us.preheader
                                        #   in Loop: Header=BB12_227 Depth=1
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB12_229:                             # %.preheader1017.split.us
                                        #   Parent Loop BB12_227 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_237 Depth 3
	movl	%r15d, %eax
	sarl	%eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cltq
	movb	332(%rax,%r13), %cl
	decb	%cl
	cmpb	$1, %cl
	movq	16(%rsp), %rcx          # 8-byte Reload
	ja	.LBB12_238
# BB#230:                               #   in Loop: Header=BB12_229 Depth=2
	cmpb	$0, 328(%rax,%r13)
	je	.LBB12_238
# BB#231:                               #   in Loop: Header=BB12_229 Depth=2
	leaq	328(%rax,%r13), %rax
	movl	%r15d, 5608(%rcx)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, 5612(%rcx)
	xorl	%ecx, %ecx
	cmpb	$3, (%rax)
	setg	%cl
	movl	%ecx, 120(%rsp)
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %rsi
	je	.LBB12_233
# BB#232:                               #   in Loop: Header=BB12_229 Depth=2
	cmpl	$0, 24(%rsi)
	je	.LBB12_234
.LBB12_233:                             # %._crit_edge1556
                                        #   in Loop: Header=BB12_229 Depth=2
	movl	$1, 108(%rsp)
	leaq	96(%rsp), %rdi
	callq	readSyntaxElement_FLC
	movl	$1, %r12d
	subl	100(%rsp), %r12d
	movl	%r12d, 100(%rsp)
	jmp	.LBB12_235
.LBB12_234:                             #   in Loop: Header=BB12_229 Depth=2
	movl	$1, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movl	100(%rsp), %r12d
.LBB12_235:                             #   in Loop: Header=BB12_229 Depth=2
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	76(%rbp), %eax
	leal	(%r14,%rax), %ecx
	addl	%ebx, %eax
	cmpl	%ecx, %eax
	movq	32(%rsp), %rdx          # 8-byte Reload
	jge	.LBB12_238
# BB#236:                               # %.lr.ph1130.us.preheader
                                        #   in Loop: Header=BB12_229 Depth=2
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB12_237:                             # %.lr.ph1130.us
                                        #   Parent Loop BB12_227 Depth=1
                                        #     Parent Loop BB12_229 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	92(%rbp), %rcx
	movslq	%r15d, %rdi
	addq	%rcx, %rdi
	addq	(%rax,%rbx,8), %rdi
	movzbl	%r12b, %esi
	callq	memset
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rbx
	movl	76(%rbp), %eax
	addl	%r14d, %eax
	cltq
	cmpq	%rax, %rbx
	jl	.LBB12_237
	.p2align	4, 0x90
.LBB12_238:                             # %.loopexit1016.us
                                        #   in Loop: Header=BB12_229 Depth=2
	addl	%edx, %r15d
	cmpl	$4, %r15d
	jl	.LBB12_229
	jmp	.LBB12_246
	.p2align	4, 0x90
.LBB12_239:                             # %.preheader1017.split.preheader
                                        #   in Loop: Header=BB12_227 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_240:                             # %.preheader1017.split
                                        #   Parent Loop BB12_227 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_244 Depth 3
	movl	%ebp, %eax
	sarl	%eax
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	cltq
	movb	332(%rax,%r13), %cl
	decb	%cl
	cmpb	$1, %cl
	movq	16(%rsp), %r15          # 8-byte Reload
	ja	.LBB12_245
# BB#241:                               #   in Loop: Header=BB12_240 Depth=2
	cmpb	$0, 328(%rax,%r13)
	je	.LBB12_245
# BB#242:                               #   in Loop: Header=BB12_240 Depth=2
	leaq	328(%rax,%r13), %rax
	movl	%ebp, 5608(%r15)
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, 5612(%r15)
	xorl	%ecx, %ecx
	cmpb	$3, (%rax)
	setg	%cl
	movl	%ecx, 120(%rsp)
	movl	$1, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	%r15, %rsi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movq	32(%rsp), %rdx          # 8-byte Reload
	movl	76(%r15), %eax
	leal	(%r14,%rax), %ecx
	addl	%ebx, %eax
	cmpl	%ecx, %eax
	jge	.LBB12_245
# BB#243:                               # %.lr.ph1130.preheader
                                        #   in Loop: Header=BB12_240 Depth=2
	movb	100(%rsp), %r12b
	movslq	%eax, %rbx
	.p2align	4, 0x90
.LBB12_244:                             # %.lr.ph1130
                                        #   Parent Loop BB12_227 Depth=1
                                        #     Parent Loop BB12_240 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	92(%r15), %rcx
	movslq	%ebp, %rdi
	addq	%rcx, %rdi
	addq	(%rax,%rbx,8), %rdi
	movzbl	%r12b, %esi
	callq	memset
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rbx
	movl	76(%r15), %eax
	addl	%r14d, %eax
	cltq
	cmpq	%rax, %rbx
	jl	.LBB12_244
	.p2align	4, 0x90
.LBB12_245:                             # %.loopexit1016
                                        #   in Loop: Header=BB12_240 Depth=2
	addl	%edx, %ebp
	cmpl	$4, %ebp
	jl	.LBB12_240
.LBB12_246:                             # %.us-lcssa1132.us
                                        #   in Loop: Header=BB12_227 Depth=1
	cmpl	$4, %r14d
                                        # kill: %R14D<def> %R14D<kill> %R14<kill> %R14<def>
	movl	56(%rsp), %ebx          # 4-byte Reload
	jl	.LBB12_227
# BB#247:
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	200(%rsp), %rax         # 8-byte Reload
.LBB12_248:                             # %.loopexit1019
	movl	$5, 96(%rsp)
	movq	(%rax), %r8
	movq	152(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rcx,4), %rcx
	shlq	$4, %rcx
	movslq	assignSE2partition+20(%rcx), %rsi
	imulq	$56, %rsi, %r9
	leaq	(%r8,%r9), %rdi
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	je	.LBB12_250
# BB#249:
	movq	(%rdi), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB12_251
.LBB12_250:
	movq	$linfo_se, 128(%rsp)
	jmp	.LBB12_252
.LBB12_251:
	movq	$readMVD_CABAC, 136(%rsp)
.LBB12_252:                             # %.preheader1015
	leaq	assignSE2partition+20(%rcx), %rax
	movq	%rax, 416(%rsp)         # 8-byte Spill
	leaq	356(%rbp,%rbx), %rax
	movq	%rax, 400(%rsp)         # 8-byte Spill
	leaq	48(%r8,%r9), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	leaq	44(%rbp,%rbx), %rcx
	movq	208(%rsp), %rax         # 8-byte Reload
	shlq	$5, %rax
	movq	%rax, 392(%rsp)         # 8-byte Spill
	shlq	$3, %rdx
	movq	%rdx, 376(%rsp)         # 8-byte Spill
	leaq	48(%rbp,%rbx), %rdx
	xorl	%edi, %edi
	movq	16(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_253:                             # %.preheader1014
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_299 Depth 2
                                        #       Child Loop BB12_305 Depth 3
                                        #         Child Loop BB12_306 Depth 4
                                        #           Child Loop BB12_307 Depth 5
                                        #             Child Loop BB12_308 Depth 6
                                        #           Child Loop BB12_311 Depth 5
                                        #             Child Loop BB12_312 Depth 6
                                        #       Child Loop BB12_262 Depth 3
                                        #       Child Loop BB12_276 Depth 3
                                        #       Child Loop BB12_286 Depth 3
                                        #         Child Loop BB12_288 Depth 4
                                        #       Child Loop BB12_269 Depth 3
                                        #         Child Loop BB12_271 Depth 4
	movl	%edi, %eax
	andl	$-2, %eax
	movl	%eax, 252(%rsp)         # 4-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rdi), %esi
	movq	%rsi, 408(%rsp)         # 8-byte Spill
	movq	%rdi, 352(%rsp)         # 8-byte Spill
	leaq	(%rdi,%rax), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movq	%rdx, 424(%rsp)         # 8-byte Spill
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	movq	%rcx, 432(%rsp)         # 8-byte Spill
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	jmp	.LBB12_299
.LBB12_254:                             #   in Loop: Header=BB12_299 Depth=2
	movl	5624(%r13), %r9d
	testl	%r9d, %r9d
	je	.LBB12_257
# BB#255:                               #   in Loop: Header=BB12_299 Depth=2
	movq	400(%rsp), %rax         # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB12_257
# BB#256:                               #   in Loop: Header=BB12_299 Depth=2
	movq	%r13, %rcx
	movl	4(%rcx), %eax
	movl	76(%rcx), %ecx
	andl	$1, %eax
	leal	2(%rax,%rax), %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	leal	-4(%rcx), %r12d
	testl	%eax, %eax
	cmovel	%ecx, %r12d
	sarl	%r12d
	jmp	.LBB12_258
.LBB12_257:                             # %.thread999
                                        #   in Loop: Header=BB12_299 Depth=2
	movl	76(%r13), %ecx
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %r12d
.LBB12_258:                             #   in Loop: Header=BB12_299 Depth=2
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	352(%rsp), %rbp         # 8-byte Reload
	movq	(%rbx), %rdx
	leal	(%r12,%rbp), %eax
	cltq
	movq	(%rdx,%rax,8), %rdi
	movl	92(%r13), %esi
	leal	(%rsi,%r15), %edx
	movslq	%edx, %rdx
	xorl	%r14d, %r14d
	cmpb	$-1, (%rdi,%rdx)
	sete	%r14b
	movq	(%rbx,%r14,8), %rdi
	movq	(%rdi,%rax,8), %rdi
	cmpb	$-1, (%rdi,%rdx)
	movq	144(%rsp), %r11         # 8-byte Reload
	je	.LBB12_267
# BB#259:                               # %.preheader1012
                                        #   in Loop: Header=BB12_299 Depth=2
	movl	5640(%r13), %esi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	listXsize(,%rcx,4), %ecx
	cmpl	%ecx, %esi
	cmovlel	%esi, %ecx
	testl	%ecx, %ecx
	jle	.LBB12_274
# BB#260:                               # %.lr.ph1094
                                        #   in Loop: Header=BB12_299 Depth=2
	movl	5584(%r13), %esi
	movq	listX(%rip), %r8
	movq	dec_picture(%rip), %rbx
	testl	%r9d, %r9d
	je	.LBB12_275
# BB#261:                               # %.lr.ph1094.split.preheader
                                        #   in Loop: Header=BB12_299 Depth=2
	movq	400(%rsp), %rdi         # 8-byte Reload
	orl	(%rdi), %esi
	movslq	%ecx, %r10
	imulq	$264, 8(%rsp), %rcx     # 8-byte Folded Reload
                                        # imm = 0x108
	leaq	24(%rbx,%rcx), %r9
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_262:                             # %.lr.ph1094.split
                                        #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%esi, %esi
	je	.LBB12_264
# BB#263:                               #   in Loop: Header=BB12_262 Depth=3
	movslq	12(%r13), %rdi
	imulq	$1584, %rdi, %rdi       # imm = 0x630
	addq	%r9, %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	(%r11,%r14,8), %rbp
	movq	(%rbp,%rax,8), %rbp
	cmpq	(%rbp,%rdx,8), %rdi
	jne	.LBB12_266
	jmp	.LBB12_282
	.p2align	4, 0x90
.LBB12_264:                             #   in Loop: Header=BB12_262 Depth=3
	movq	(%r8,%rcx,8), %rdi
	movslq	8(%rdi), %rbp
	addq	%rbp, %rbp
	movq	(%r11,%r14,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	movq	(%rbx,%rdx,8), %rbx
	cmpq	%rbx, %rbp
	je	.LBB12_282
# BB#265:                               #   in Loop: Header=BB12_262 Depth=3
	movslq	12(%rdi), %rdi
	addq	%rdi, %rdi
	cmpq	%rbx, %rdi
	je	.LBB12_282
.LBB12_266:                             #   in Loop: Header=BB12_262 Depth=3
	incq	%rcx
	cmpq	%r10, %rcx
	jl	.LBB12_262
	jmp	.LBB12_281
.LBB12_267:                             #   in Loop: Header=BB12_299 Depth=2
	leal	(%rcx,%rbp), %edx
	movq	408(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx), %eax
	cmpl	%eax, %edx
	jge	.LBB12_317
# BB#268:                               # %.lr.ph1117
                                        #   in Loop: Header=BB12_299 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%r15,%rax), %eax
	movslq	%edx, %rdx
	.p2align	4, 0x90
.LBB12_269:                             #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_271 Depth 4
	leal	(%rsi,%r15), %edi
	leal	(%rax,%rsi), %ebp
	cmpl	%ebp, %edi
	jge	.LBB12_273
# BB#270:                               # %.lr.ph1113
                                        #   in Loop: Header=BB12_269 Depth=3
	movslq	%edi, %rcx
	.p2align	4, 0x90
.LBB12_271:                             #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        #       Parent Loop BB12_269 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	8(%rsi), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movb	$0, (%rsi,%rcx)
	movq	dec_picture(%rip), %rsi
	movq	316952(%rsi), %rsi
	movq	(%rsi), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movb	$0, (%rsi,%rcx)
	movq	dec_picture(%rip), %rsi
	movq	316976(%rsi), %rsi
	movq	(%rsi), %rdi
	movq	8(%rsi), %rsi
	movq	(%rdi,%rdx,8), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	(%rsi,%rdx,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movw	$0, (%rdi)
	movw	$0, (%rsi)
	movw	$0, 2(%rdi)
	movw	$0, 2(%rsi)
	incq	%rcx
	movl	92(%r13), %esi
	leal	(%rax,%rsi), %edi
	movslq	%edi, %rdi
	cmpq	%rdi, %rcx
	jl	.LBB12_271
# BB#272:                               # %._crit_edge1114.loopexit
                                        #   in Loop: Header=BB12_269 Depth=3
	movl	76(%r13), %ecx
.LBB12_273:                             # %._crit_edge1114
                                        #   in Loop: Header=BB12_269 Depth=3
	incq	%rdx
	movq	408(%rsp), %rdi         # 8-byte Reload
	leal	(%rdi,%rcx), %edi
	movslq	%edi, %rdi
	cmpq	%rdi, %rdx
	jl	.LBB12_269
	jmp	.LBB12_317
.LBB12_274:                             #   in Loop: Header=BB12_299 Depth=2
	movl	$-1, %ecx
	cmpq	$0, 328(%rsp)           # 8-byte Folded Reload
	jne	.LBB12_285
	jmp	.LBB12_317
.LBB12_275:                             # %.lr.ph1094.split.us.preheader
                                        #   in Loop: Header=BB12_299 Depth=2
	movslq	%ecx, %r10
	imulq	$264, 8(%rsp), %rcx     # 8-byte Folded Reload
                                        # imm = 0x108
	leaq	24(%rbx,%rcx), %r9
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_276:                             # %.lr.ph1094.split.us
                                        #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	testl	%esi, %esi
	je	.LBB12_278
# BB#277:                               #   in Loop: Header=BB12_276 Depth=3
	movslq	12(%r13), %rdi
	imulq	$1584, %rdi, %rdi       # imm = 0x630
	addq	%r9, %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	(%r11,%r14,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	cmpq	(%rbx,%rdx,8), %rdi
	jne	.LBB12_280
	jmp	.LBB12_282
	.p2align	4, 0x90
.LBB12_278:                             #   in Loop: Header=BB12_276 Depth=3
	movq	(%r8,%rcx,8), %rdi
	movslq	8(%rdi), %rbp
	addq	%rbp, %rbp
	movq	(%r11,%r14,8), %rbx
	movq	(%rbx,%rax,8), %rbx
	movq	(%rbx,%rdx,8), %rbx
	cmpq	%rbx, %rbp
	je	.LBB12_282
# BB#279:                               #   in Loop: Header=BB12_276 Depth=3
	movslq	12(%rdi), %rdi
	addq	%rdi, %rdi
	cmpq	%rbx, %rdi
	je	.LBB12_282
.LBB12_280:                             #   in Loop: Header=BB12_276 Depth=3
	incq	%rcx
	cmpq	%r10, %rcx
	jl	.LBB12_276
.LBB12_281:                             #   in Loop: Header=BB12_299 Depth=2
	movl	$-135792468, %ecx       # imm = 0xF7E7F8AC
.LBB12_282:                             # %.thread1000
                                        #   in Loop: Header=BB12_299 Depth=2
	cmpl	$-135792468, %ecx       # imm = 0xF7E7F8AC
	jne	.LBB12_284
# BB#283:                               #   in Loop: Header=BB12_299 Depth=2
	movl	$.L.str.3, %edi
	movl	$-1111, %esi            # imm = 0xFBA9
	callq	error
	movl	$-135792468, %ecx       # imm = 0xF7E7F8AC
.LBB12_284:                             # %.preheader1010
                                        #   in Loop: Header=BB12_299 Depth=2
	cmpq	$0, 328(%rsp)           # 8-byte Folded Reload
	je	.LBB12_317
.LBB12_285:                             # %.lr.ph1109
                                        #   in Loop: Header=BB12_299 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	leal	(%r15,%rax), %r11d
	movq	%r12, %rdx
	movslq	%ecx, %r12
	movq	8(%rsp), %rax           # 8-byte Reload
	shlq	$7, %rax
	movq	%r13, %rcx
	addq	%rcx, %rax
	leaq	616(%rax,%r12,4), %r10
	movslq	%edx, %r8
	movl	92(%rcx), %eax
	movq	352(%rsp), %r9          # 8-byte Reload
	.p2align	4, 0x90
.LBB12_286:                             #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_288 Depth 4
	leal	(%rax,%r15), %ecx
	leal	(%r11,%rax), %edx
	cmpl	%edx, %ecx
	jge	.LBB12_298
# BB#287:                               # %.lr.ph1106
                                        #   in Loop: Header=BB12_286 Depth=3
	leaq	(%r9,%r8), %r15
	movslq	76(%r13), %rax
	movslq	%r9d, %rsi
	addq	%rax, %rsi
	movslq	%ecx, %rdi
	.p2align	4, 0x90
.LBB12_288:                             #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        #       Parent Loop BB12_286 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movl	(%r10), %ebx
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movb	%r12b, (%rax,%rdi)
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movb	$0, (%rax,%rdi)
	cmpl	$9999, %ebx             # imm = 0x270F
	movq	dec_picture(%rip), %rbp
	jne	.LBB12_290
# BB#289:                               # %.split1102.us.preheader
                                        #   in Loop: Header=BB12_288 Depth=4
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rdi,8), %rcx
	movzwl	(%rcx), %eax
	movq	316976(%rbp), %rdx
	movq	(%rdx), %rbx
	movq	(%rbx,%rsi,8), %rbx
	movq	(%rbx,%rdi,8), %rbx
	movw	%ax, (%rbx)
	movq	8(%rdx), %rax
	movq	(%rax,%rsi,8), %rax
	movq	(%rax,%rdi,8), %rax
	movw	$0, (%rax)
	movzwl	2(%rcx), %ecx
	movw	%cx, 2(%rbx)
	xorl	%ecx, %ecx
	jmp	.LBB12_297
	.p2align	4, 0x90
.LBB12_290:                             # %.split1102.preheader
                                        #   in Loop: Header=BB12_288 Depth=4
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	listX(,%rax,8), %rax
	movq	(%rax,%r12,8), %rax
	movl	316844(%rax), %r13d
	testl	%r13d, %r13d
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rdi,8), %rdx
	movzwl	(%rdx), %ecx
	je	.LBB12_292
# BB#291:                               #   in Loop: Header=BB12_288 Depth=4
	movq	316976(%rbp), %rax
	movq	(%rax), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rbp,%rdi,8), %rbp
	movw	%cx, (%rbp)
	xorl	%ecx, %ecx
	jmp	.LBB12_293
.LBB12_292:                             #   in Loop: Header=BB12_288 Depth=4
	movswl	%cx, %ecx
	imull	%ebx, %ecx
	subl	$-128, %ecx
	shrl	$8, %ecx
	movq	316976(%rbp), %rax
	movq	(%rax), %rbp
	movq	(%rbp,%rsi,8), %rbp
	movq	(%rbp,%rdi,8), %rbp
	movw	%cx, (%rbp)
	subw	(%rdx), %cx
.LBB12_293:                             # %.split1102.11236
                                        #   in Loop: Header=BB12_288 Depth=4
	testl	%r13d, %r13d
	movq	8(%rax), %rax
	movq	(%rax,%rsi,8), %rax
	movq	(%rax,%rdi,8), %rax
	movw	%cx, (%rax)
	movzwl	2(%rdx), %ecx
	je	.LBB12_295
# BB#294:                               #   in Loop: Header=BB12_288 Depth=4
	movw	%cx, 2(%rbp)
	xorl	%ecx, %ecx
	jmp	.LBB12_296
.LBB12_295:                             #   in Loop: Header=BB12_288 Depth=4
	movswl	%cx, %ecx
	imull	%ebx, %ecx
	subl	$-128, %ecx
	shrl	$8, %ecx
	movw	%cx, 2(%rbp)
	subw	2(%rdx), %cx
.LBB12_296:                             # %.us-lcssa1103.us
                                        #   in Loop: Header=BB12_288 Depth=4
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB12_297:                             # %.us-lcssa1103.us
                                        #   in Loop: Header=BB12_288 Depth=4
	movw	%cx, 2(%rax)
	incq	%rdi
	movl	92(%r13), %eax
	leal	(%r11,%rax), %ecx
	movslq	%ecx, %rcx
	cmpq	%rcx, %rdi
	jl	.LBB12_288
.LBB12_298:                             # %._crit_edge1107
                                        #   in Loop: Header=BB12_286 Depth=3
	incq	%r9
	cmpq	216(%rsp), %r9          # 8-byte Folded Reload
	movq	224(%rsp), %r15         # 8-byte Reload
	jl	.LBB12_286
	jmp	.LBB12_317
	.p2align	4, 0x90
.LBB12_299:                             #   Parent Loop BB12_253 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_305 Depth 3
                                        #         Child Loop BB12_306 Depth 4
                                        #           Child Loop BB12_307 Depth 5
                                        #             Child Loop BB12_308 Depth 6
                                        #           Child Loop BB12_311 Depth 5
                                        #             Child Loop BB12_312 Depth 6
                                        #       Child Loop BB12_262 Depth 3
                                        #       Child Loop BB12_276 Depth 3
                                        #       Child Loop BB12_286 Depth 3
                                        #         Child Loop BB12_288 Depth 4
                                        #       Child Loop BB12_269 Depth 3
                                        #         Child Loop BB12_271 Depth 4
	movl	%r15d, %eax
	sarl	%eax
	addl	252(%rsp), %eax         # 4-byte Folded Reload
	cltq
	leaq	(%rbp,%rbx), %rdx
	movb	332(%rax,%rdx), %cl
	orb	$2, %cl
	cmpb	$2, %cl
	movq	%rdx, 384(%rsp)         # 8-byte Spill
	movq	%r15, 224(%rsp)         # 8-byte Spill
	jne	.LBB12_315
# BB#300:                               #   in Loop: Header=BB12_299 Depth=2
	movsbq	328(%rax,%rdx), %rcx
	testq	%rcx, %rcx
	je	.LBB12_316
# BB#301:                               #   in Loop: Header=BB12_299 Depth=2
	cmpq	$0, 328(%rsp)           # 8-byte Folded Reload
	je	.LBB12_317
# BB#302:                               # %.lr.ph1092
                                        #   in Loop: Header=BB12_299 Depth=2
	movq	%r13, %rsi
	movl	76(%rsi), %eax
	movq	352(%rsp), %rbp         # 8-byte Reload
	leal	(%rax,%rbp), %edx
	movslq	92(%rsi), %rsi
	movslq	%r15d, %rdi
	addq	%rsi, %rdi
	movslq	BLOCK_STEP(,%rcx,8), %rbx
	movslq	BLOCK_STEP+4(,%rcx,8), %r12
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movb	(%rcx,%rdi), %cl
	movb	%cl, 88(%rsp)           # 1-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%r15,%rcx), %rcx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	leal	(,%rbx,4), %ecx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	leal	(,%r12,4), %ecx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	movq	%r12, %rcx
	shlq	$5, %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	leaq	(,%rbx,8), %rcx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	jmp	.LBB12_305
	.p2align	4, 0x90
.LBB12_303:                             # %._crit_edge1088
                                        #   in Loop: Header=BB12_305 Depth=3
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	addq	%r12, %rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	216(%rsp), %rcx         # 8-byte Folded Reload
	movq	224(%rsp), %r15         # 8-byte Reload
	jge	.LBB12_317
# BB#304:                               # %._crit_edge1088._crit_edge
                                        #   in Loop: Header=BB12_305 Depth=3
	movl	76(%r13), %eax
	movq	152(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %rsi         # 8-byte Reload
	addq	%rcx, %rsi
	movq	160(%rsp), %rdx         # 8-byte Reload
	addq	%rcx, %rdx
	movq	%rdx, %rcx
.LBB12_305:                             # %.lr.ph1087.preheader
                                        #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_306 Depth 4
                                        #           Child Loop BB12_307 Depth 5
                                        #             Child Loop BB12_308 Depth 6
                                        #           Child Loop BB12_311 Depth 5
                                        #             Child Loop BB12_312 Depth 6
	addl	8(%rsp), %eax           # 4-byte Folded Reload
	movslq	%eax, %r14
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	%rcx, %r11
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	movq	%rsi, %rbp
	.p2align	4, 0x90
.LBB12_306:                             # %.preheader1008.us.us.preheader
                                        #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        #       Parent Loop BB12_305 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB12_307 Depth 5
                                        #             Child Loop BB12_308 Depth 6
                                        #           Child Loop BB12_311 Depth 5
                                        #             Child Loop BB12_312 Depth 6
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	%r13, %rdi
	movl	92(%rdi), %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %r8
	movq	316976(%rax), %r9
	movsbl	88(%rsp), %edx          # 1-byte Folded Reload
	movl	$0, %ecx
	leaq	176(%rsp), %rsi
	pushq	232(%rsp)               # 8-byte Folded Reload
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	248(%rsp)               # 8-byte Folded Reload
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	movq	%r15, %r13
	movq	%rdi, %r15
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset -32
	movl	%r13d, 5608(%r15)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 5612(%r15)
	movl	$0, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r13, %r15
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movl	100(%rsp), %eax
	movzwl	176(%rsp), %ecx
	addl	%eax, %ecx
	movq	dec_picture(%rip), %rdx
	movq	316976(%rdx), %rdx
	movq	(%rdx), %r8
	movl	56(%rsp), %edx          # 4-byte Reload
	addl	%r15d, %edx
	movslq	%edx, %r9
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB12_307:                             # %.preheader1008.us.us
                                        #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        #       Parent Loop BB12_305 Depth=3
                                        #         Parent Loop BB12_306 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB12_308 Depth 6
	leaq	(%r14,%r11), %rdx
	leaq	(,%r9,8), %r13
	movq	(%r8,%rdx,8), %rdx
	addq	%r13, %rdx
	movq	%r10, %rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB12_308:                             #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        #       Parent Loop BB12_305 Depth=3
                                        #         Parent Loop BB12_306 Depth=4
                                        #           Parent Loop BB12_307 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rdx,%rsi,8), %rbp
	movw	%cx, (%rbp)
	movl	%eax, (%rdi)
	incq	%rsi
	addq	$8, %rdi
	cmpq	%rbx, %rsi
	jl	.LBB12_308
# BB#309:                               # %._crit_edge1071.us.us
                                        #   in Loop: Header=BB12_307 Depth=5
	incq	%r11
	addq	$32, %r10
	cmpq	%r12, %r11
	jl	.LBB12_307
# BB#310:                               # %.preheader1008.us.us.preheader.1
                                        #   in Loop: Header=BB12_306 Depth=4
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%r15d, 5608(%rsi)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 5612(%rsi)
	movl	$2, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	24(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movl	100(%rsp), %eax
	movzwl	178(%rsp), %ecx
	addl	%eax, %ecx
	movq	dec_picture(%rip), %rdx
	movq	316976(%rdx), %rdx
	movq	(%rdx), %r8
	movq	40(%rsp), %r11          # 8-byte Reload
	movq	%r11, %r9
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB12_311:                             # %.preheader1008.us.us.1
                                        #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        #       Parent Loop BB12_305 Depth=3
                                        #         Parent Loop BB12_306 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB12_312 Depth 6
	leaq	(%r14,%r10), %rdx
	movq	(%r8,%rdx,8), %rbp
	addq	%r13, %rbp
	movq	%r9, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_312:                             #   Parent Loop BB12_253 Depth=1
                                        #     Parent Loop BB12_299 Depth=2
                                        #       Parent Loop BB12_305 Depth=3
                                        #         Parent Loop BB12_306 Depth=4
                                        #           Parent Loop BB12_311 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rbp,%rdx,8), %rdi
	movw	%cx, 2(%rdi)
	movl	%eax, (%rsi)
	incq	%rdx
	addq	$8, %rsi
	cmpq	%rbx, %rdx
	jl	.LBB12_312
# BB#313:                               # %._crit_edge1071.us.us.1
                                        #   in Loop: Header=BB12_311 Depth=5
	incq	%r10
	addq	$32, %r9
	cmpq	%r12, %r10
	jl	.LBB12_311
# BB#314:                               # %.us-lcssa1076.us
                                        #   in Loop: Header=BB12_306 Depth=4
	addq	%rbx, %r15
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	addq	%rax, %rbp
	addq	%rax, %r11
	cmpq	272(%rsp), %r15         # 8-byte Folded Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	jl	.LBB12_306
	jmp	.LBB12_303
	.p2align	4, 0x90
.LBB12_315:                             #   in Loop: Header=BB12_299 Depth=2
	cmpb	$0, 328(%rax,%rdx)
	jne	.LBB12_317
.LBB12_316:                             # %.thread1640
                                        #   in Loop: Header=BB12_299 Depth=2
	cmpl	$0, 40(%r13)
	je	.LBB12_254
.LBB12_317:                             # %.loopexit1009
                                        #   in Loop: Header=BB12_299 Depth=2
	movq	32(%rsp), %rsi          # 8-byte Reload
	addq	%rsi, %r15
	movq	376(%rsp), %rax         # 8-byte Reload
	addq	%rax, 336(%rsp)         # 8-byte Folded Spill
	addq	%rax, 344(%rsp)         # 8-byte Folded Spill
	cmpq	$4, %r15
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	64(%rsp), %rbx          # 8-byte Reload
	jl	.LBB12_299
# BB#318:                               #   in Loop: Header=BB12_253 Depth=1
	movq	392(%rsp), %rax         # 8-byte Reload
	movq	432(%rsp), %rcx         # 8-byte Reload
	addq	%rax, %rcx
	movq	424(%rsp), %rdx         # 8-byte Reload
	addq	%rax, %rdx
	movq	216(%rsp), %rax         # 8-byte Reload
	cmpq	$4, %rax
	movq	%rax, %rdi
	jl	.LBB12_253
# BB#319:
	movl	$5, 96(%rsp)
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	416(%rsp), %rcx         # 8-byte Reload
	movslq	(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rdi
	movq	active_pps(%rip), %rdx
	cmpl	$0, 12(%rdx)
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	je	.LBB12_321
# BB#320:
	movq	(%rdi), %rdx
	cmpl	$0, 24(%rdx)
	je	.LBB12_322
.LBB12_321:
	movq	$linfo_se, 128(%rsp)
	jmp	.LBB12_323
.LBB12_322:
	movq	$readMVD_CABAC, 136(%rsp)
.LBB12_323:                             # %.preheader1007
	leaq	48(%rax,%rcx), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leaq	172(%rbp,%rbx), %rdx
	leaq	(,%rsi,8), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	leaq	176(%rbp,%rbx), %rsi
	xorl	%edi, %edi
	movq	384(%rsp), %rcx         # 8-byte Reload
	.p2align	4, 0x90
.LBB12_324:                             # %.preheader1006
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_325 Depth 2
                                        #       Child Loop BB12_331 Depth 3
                                        #         Child Loop BB12_332 Depth 4
                                        #           Child Loop BB12_333 Depth 5
                                        #             Child Loop BB12_334 Depth 6
                                        #           Child Loop BB12_337 Depth 5
                                        #             Child Loop BB12_338 Depth 6
	movl	%edi, %eax
	andl	$-2, %eax
	movl	%eax, 252(%rsp)         # 4-byte Spill
	movq	208(%rsp), %rax         # 8-byte Reload
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	leaq	(%rdi,%rax), %rax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movq	%rsi, 376(%rsp)         # 8-byte Spill
	movq	%rsi, 344(%rsp)         # 8-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 336(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_325:                             #   Parent Loop BB12_324 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_331 Depth 3
                                        #         Child Loop BB12_332 Depth 4
                                        #           Child Loop BB12_333 Depth 5
                                        #             Child Loop BB12_334 Depth 6
                                        #           Child Loop BB12_337 Depth 5
                                        #             Child Loop BB12_338 Depth 6
	movl	%ebx, %eax
	sarl	%eax
	addl	252(%rsp), %eax         # 4-byte Folded Reload
	cltq
	movb	332(%rax,%rcx), %cl
	decb	%cl
	cmpb	$1, %cl
	ja	.LBB12_341
# BB#326:                               #   in Loop: Header=BB12_325 Depth=2
	movq	384(%rsp), %rcx         # 8-byte Reload
	movsbq	328(%rax,%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB12_341
# BB#327:                               #   in Loop: Header=BB12_325 Depth=2
	cmpq	$0, 328(%rsp)           # 8-byte Folded Reload
	je	.LBB12_341
# BB#328:                               # %.lr.ph1064
                                        #   in Loop: Header=BB12_325 Depth=2
	movq	%r13, %rsi
	movl	76(%rsi), %eax
	movq	72(%rsp), %rbp          # 8-byte Reload
	leal	(%rax,%rbp), %edx
	movslq	92(%rsi), %rsi
	movslq	%ebx, %rdi
	addq	%rsi, %rdi
	movslq	BLOCK_STEP(,%rcx,8), %r14
	movslq	BLOCK_STEP+4(,%rcx,8), %r15
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	%edx, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movb	(%rcx,%rdi), %cl
	movb	%cl, 240(%rsp)          # 1-byte Spill
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rbx,%rcx), %rcx
	movq	%rcx, 232(%rsp)         # 8-byte Spill
	leal	(,%r14,4), %ecx
	movq	%rcx, 168(%rsp)         # 8-byte Spill
	leal	(,%r15,4), %ecx
	movq	%rcx, 360(%rsp)         # 8-byte Spill
	movq	%r15, %rcx
	shlq	$5, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	leaq	(,%r14,8), %rcx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	336(%rsp), %rsi         # 8-byte Reload
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbx, 216(%rsp)         # 8-byte Spill
	jmp	.LBB12_331
	.p2align	4, 0x90
.LBB12_329:                             # %._crit_edge1060
                                        #   in Loop: Header=BB12_331 Depth=3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	addq	%r15, %rcx
	movq	%rcx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	cmpq	152(%rsp), %rcx         # 8-byte Folded Reload
	movq	216(%rsp), %rbx         # 8-byte Reload
	jge	.LBB12_341
# BB#330:                               # %._crit_edge1060._crit_edge
                                        #   in Loop: Header=BB12_331 Depth=3
	movl	76(%r13), %eax
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	224(%rsp), %rsi         # 8-byte Reload
	addq	%rcx, %rsi
	movq	264(%rsp), %rdx         # 8-byte Reload
	addq	%rcx, %rdx
	movq	%rdx, %rcx
.LBB12_331:                             # %.lr.ph1059.preheader
                                        #   Parent Loop BB12_324 Depth=1
                                        #     Parent Loop BB12_325 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB12_332 Depth 4
                                        #           Child Loop BB12_333 Depth 5
                                        #             Child Loop BB12_334 Depth 6
                                        #           Child Loop BB12_337 Depth 5
                                        #             Child Loop BB12_338 Depth 6
	addl	24(%rsp), %eax          # 4-byte Folded Reload
	movslq	%eax, %r12
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	movq	%rcx, %r10
	movq	%rsi, 224(%rsp)         # 8-byte Spill
	movq	%rsi, %rcx
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB12_332:                             # %.preheader.us.us.preheader
                                        #   Parent Loop BB12_324 Depth=1
                                        #     Parent Loop BB12_325 Depth=2
                                        #       Parent Loop BB12_331 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB12_333 Depth 5
                                        #             Child Loop BB12_334 Depth 6
                                        #           Child Loop BB12_337 Depth 5
                                        #             Child Loop BB12_338 Depth 6
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movq	%r10, 88(%rsp)          # 8-byte Spill
	movl	92(%r13), %eax
	movl	%eax, 272(%rsp)         # 4-byte Spill
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %r8
	movq	316976(%rax), %r9
	movsbl	240(%rsp), %edx         # 1-byte Folded Reload
	movl	$1, %ecx
	movq	%r13, %rdi
	leaq	176(%rsp), %rsi
	pushq	360(%rsp)               # 8-byte Folded Reload
.Lcfi73:
	.cfi_adjust_cfa_offset 8
	pushq	176(%rsp)               # 8-byte Folded Reload
.Lcfi74:
	.cfi_adjust_cfa_offset 8
	movq	40(%rsp), %rbx          # 8-byte Reload
	pushq	%rbx
.Lcfi75:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi76:
	.cfi_adjust_cfa_offset 8
	callq	SetMotionVectorPredictor
	addq	$32, %rsp
.Lcfi77:
	.cfi_adjust_cfa_offset -32
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, 5608(%r13)
	movl	%ebx, 5612(%r13)
	movl	$1, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	%r13, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movq	8(%rsp), %r11           # 8-byte Reload
	movl	100(%rsp), %eax
	movzwl	176(%rsp), %ecx
	addl	%eax, %ecx
	movq	dec_picture(%rip), %rdx
	movq	316976(%rdx), %rdx
	movq	8(%rdx), %r8
	movl	272(%rsp), %edx         # 4-byte Reload
	addl	%r11d, %edx
	movslq	%edx, %r9
	movq	48(%rsp), %r10          # 8-byte Reload
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_333:                             # %.preheader.us.us
                                        #   Parent Loop BB12_324 Depth=1
                                        #     Parent Loop BB12_325 Depth=2
                                        #       Parent Loop BB12_331 Depth=3
                                        #         Parent Loop BB12_332 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB12_334 Depth 6
	leaq	(%r12,%rbx), %rdx
	leaq	(,%r9,8), %r13
	movq	(%r8,%rdx,8), %rdx
	addq	%r13, %rdx
	movq	%r10, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB12_334:                             #   Parent Loop BB12_324 Depth=1
                                        #     Parent Loop BB12_325 Depth=2
                                        #       Parent Loop BB12_331 Depth=3
                                        #         Parent Loop BB12_332 Depth=4
                                        #           Parent Loop BB12_333 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rdx,%rbp,8), %rsi
	movw	%cx, (%rsi)
	movl	%eax, (%rdi)
	incq	%rbp
	addq	$8, %rdi
	cmpq	%r14, %rbp
	jl	.LBB12_334
# BB#335:                               # %._crit_edge1045.us.us
                                        #   in Loop: Header=BB12_333 Depth=5
	incq	%rbx
	addq	$32, %r10
	cmpq	%r15, %rbx
	jl	.LBB12_333
# BB#336:                               # %.preheader.us.us.preheader.1
                                        #   in Loop: Header=BB12_332 Depth=4
	movq	16(%rsp), %rsi          # 8-byte Reload
	movl	%r11d, 5608(%rsi)
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, 5612(%rsi)
	movl	$3, 104(%rsp)
	leaq	96(%rsp), %rdi
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	56(%rsp), %rax          # 8-byte Reload
	callq	*(%rax)
	movl	100(%rsp), %eax
	movzwl	178(%rsp), %ecx
	addl	%eax, %ecx
	movq	dec_picture(%rip), %rdx
	movq	316976(%rdx), %rdx
	movq	8(%rdx), %r8
	movq	88(%rsp), %r10          # 8-byte Reload
	movq	%r10, %r9
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB12_337:                             # %.preheader.us.us.1
                                        #   Parent Loop BB12_324 Depth=1
                                        #     Parent Loop BB12_325 Depth=2
                                        #       Parent Loop BB12_331 Depth=3
                                        #         Parent Loop BB12_332 Depth=4
                                        # =>        This Loop Header: Depth=5
                                        #             Child Loop BB12_338 Depth 6
	leaq	(%r12,%rdi), %rdx
	movq	(%r8,%rdx,8), %rbx
	addq	%r13, %rbx
	movq	%r9, %rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB12_338:                             #   Parent Loop BB12_324 Depth=1
                                        #     Parent Loop BB12_325 Depth=2
                                        #       Parent Loop BB12_331 Depth=3
                                        #         Parent Loop BB12_332 Depth=4
                                        #           Parent Loop BB12_337 Depth=5
                                        # =>          This Inner Loop Header: Depth=6
	movq	(%rbx,%rdx,8), %rbp
	movw	%cx, 2(%rbp)
	movl	%eax, (%rsi)
	incq	%rdx
	addq	$8, %rsi
	cmpq	%r14, %rdx
	jl	.LBB12_338
# BB#339:                               # %._crit_edge1045.us.us.1
                                        #   in Loop: Header=BB12_337 Depth=5
	incq	%rdi
	addq	$32, %r9
	cmpq	%r15, %rdi
	jl	.LBB12_337
# BB#340:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB12_332 Depth=4
	movq	8(%rsp), %rbp           # 8-byte Reload
	addq	%r14, %rbp
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	addq	%rax, %rcx
	addq	%rax, %r10
	cmpq	232(%rsp), %rbp         # 8-byte Folded Reload
	movq	16(%rsp), %r13          # 8-byte Reload
	jl	.LBB12_332
	jmp	.LBB12_329
	.p2align	4, 0x90
.LBB12_341:                             # %.loopexit
                                        #   in Loop: Header=BB12_325 Depth=2
	addq	32(%rsp), %rbx          # 8-byte Folded Reload
	movq	80(%rsp), %rax          # 8-byte Reload
	addq	%rax, 336(%rsp)         # 8-byte Folded Spill
	addq	%rax, 344(%rsp)         # 8-byte Folded Spill
	cmpq	$4, %rbx
	movq	384(%rsp), %rcx         # 8-byte Reload
	jl	.LBB12_325
# BB#342:                               #   in Loop: Header=BB12_324 Depth=1
	movq	392(%rsp), %rax         # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	addq	%rax, %rdx
	movq	376(%rsp), %rsi         # 8-byte Reload
	addq	%rax, %rsi
	movq	152(%rsp), %rax         # 8-byte Reload
	cmpq	$4, %rax
	movq	%rax, %rdi
	jl	.LBB12_324
# BB#343:                               # %.lr.ph1041
	movslq	76(%r13), %rax
	movq	368(%rsp), %rdx         # 8-byte Reload
	movl	%edx, %ecx
	orl	$1, %edx
	movabsq	$-9223372036854775808, %r11 # imm = 0x8000000000000000
	imulq	$264, %rcx, %r9         # imm = 0x108
	imulq	$264, %rdx, %r10        # imm = 0x108
	leaq	3(%rax), %r8
	.p2align	4, 0x90
.LBB12_344:                             # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_345 Depth 2
	movslq	92(%r13), %rbp
	leaq	3(%rbp), %r14
	decq	%rbp
	.p2align	4, 0x90
.LBB12_345:                             #   Parent Loop BB12_344 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	dec_picture(%rip), %rdi
	movq	316952(%rdi), %rdx
	movq	(%rdx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movsbq	1(%rcx,%rbp), %rsi
	testq	%rsi, %rsi
	movq	%r11, %rcx
	js	.LBB12_347
# BB#346:                               #   in Loop: Header=BB12_345 Depth=2
	movslq	12(%r13), %rcx
	imulq	$1584, %rcx, %rcx       # imm = 0x630
	addq	%rdi, %rcx
	addq	%r9, %rcx
	movq	24(%rcx,%rsi,8), %rcx
.LBB12_347:                             #   in Loop: Header=BB12_345 Depth=2
	movq	316960(%rdi), %rsi
	movq	(%rsi), %rbx
	movq	(%rbx,%rax,8), %rbx
	movq	%rcx, 8(%rbx,%rbp,8)
	movq	8(%rdx), %rcx
	movq	(%rcx,%rax,8), %rcx
	movsbq	1(%rcx,%rbp), %rdx
	testq	%rdx, %rdx
	movq	%r11, %rcx
	js	.LBB12_349
# BB#348:                               #   in Loop: Header=BB12_345 Depth=2
	movslq	12(%r13), %rcx
	imulq	$1584, %rcx, %rcx       # imm = 0x630
	addq	%rcx, %rdi
	addq	%r10, %rdi
	movq	24(%rdi,%rdx,8), %rcx
.LBB12_349:                             #   in Loop: Header=BB12_345 Depth=2
	movq	8(%rsi), %rdx
	movq	(%rdx,%rax,8), %rdx
	movq	%rcx, 8(%rdx,%rbp,8)
	incq	%rbp
	cmpq	%r14, %rbp
	jl	.LBB12_345
# BB#350:                               # %._crit_edge
                                        #   in Loop: Header=BB12_344 Depth=1
	cmpq	%r8, %rax
	leaq	1(%rax), %rax
	jl	.LBB12_344
# BB#351:                               # %._crit_edge1042
	addq	$440, %rsp              # imm = 0x1B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	readMotionInfoFromNAL, .Lfunc_end12-readMotionInfoFromNAL
	.cfi_endproc

	.p2align	4, 0x90
	.type	SetMotionVectorPredictor,@function
SetMotionVectorPredictor:               # @SetMotionVectorPredictor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi84:
	.cfi_def_cfa_offset 240
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movl	%ecx, 40(%rsp)          # 4-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, 152(%rsp)         # 8-byte Spill
	movl	248(%rsp), %r14d
	movl	240(%rsp), %r13d
	leal	(,%r13,4), %r12d
	leal	(,%r14,4), %edx
	movq	%rdi, 144(%rsp)         # 8-byte Spill
	movl	4(%rdi), %r15d
	leal	-1(,%r13,4), %ebx
	leaq	80(%rsp), %rcx
	movl	%r15d, %edi
	movl	%ebx, %esi
	movl	%edx, 48(%rsp)          # 4-byte Spill
	callq	getLuma4x4Neighbour
	leal	-1(,%r14,4), %ebp
	leaq	56(%rsp), %rcx
	movl	%r15d, %edi
	movl	%r12d, %esi
	movl	%ebp, %edx
	callq	getLuma4x4Neighbour
	movl	256(%rsp), %eax
	leal	(%rax,%r13,4), %r13d
	leaq	16(%rsp), %rcx
	movl	%r15d, %edi
	movl	%r13d, %esi
	movl	%ebp, %edx
	callq	getLuma4x4Neighbour
	leaq	160(%rsp), %rcx
	movl	%r15d, %edi
	movl	%ebx, %esi
	movl	%ebp, %edx
	callq	getLuma4x4Neighbour
	testl	%r14d, %r14d
	jle	.LBB13_7
# BB#1:
	cmpl	$7, %r12d
	jg	.LBB13_4
# BB#2:
	cmpl	$8, 48(%rsp)            # 4-byte Folded Reload
	jne	.LBB13_5
# BB#3:
	movl	256(%rsp), %eax
	cmpl	$16, %eax
	je	.LBB13_6
	jmp	.LBB13_7
.LBB13_4:
	cmpl	$16, %r13d
	je	.LBB13_6
	jmp	.LBB13_7
.LBB13_5:
	cmpl	$8, %r13d
	jne	.LBB13_7
.LBB13_6:
	movl	$0, 16(%rsp)
.LBB13_7:
	cmpl	$0, 16(%rsp)
	jne	.LBB13_9
# BB#8:
	movq	176(%rsp), %rax
	movq	%rax, 32(%rsp)
	movups	160(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
.LBB13_9:
	movq	144(%rsp), %rax         # 8-byte Reload
	movl	5624(%rax), %r15d
	testl	%r15d, %r15d
	movl	40(%rsp), %r11d         # 4-byte Reload
	movl	256(%rsp), %r14d
	movq	104(%rsp), %rbx         # 8-byte Reload
	je	.LBB13_17
# BB#10:
	movq	5600(%rax), %rbp
	movl	4(%rax), %eax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rbp,%rax)
	movl	80(%rsp), %r10d
	movl	$-1, %r9d
	je	.LBB13_23
# BB#11:
	testl	%r10d, %r10d
	movl	$-1, %esi
	je	.LBB13_13
# BB#12:
	movslq	84(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	movzbl	%r11b, %ecx
	movq	(%rbx,%rcx,8), %rcx
	movslq	100(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	96(%rsp), %rdx
	movsbl	(%rcx,%rdx), %esi
	cmpl	$0, 356(%rbp,%rax)
	sete	%cl
	shll	%cl, %esi
.LBB13_13:
	movl	56(%rsp), %r12d
	testl	%r12d, %r12d
	je	.LBB13_15
# BB#14:
	movslq	60(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	movzbl	%r11b, %ecx
	movq	(%rbx,%rcx,8), %rcx
	movslq	76(%rsp), %rdi
	movq	(%rcx,%rdi,8), %rcx
	movslq	72(%rsp), %rdi
	movsbl	(%rcx,%rdi), %r9d
	cmpl	$0, 356(%rbp,%rax)
	sete	%cl
	shll	%cl, %r9d
.LBB13_15:
	movl	16(%rsp), %r13d
	testl	%r13d, %r13d
	je	.LBB13_29
# BB#16:
	movslq	20(%rsp), %rax
	imulq	$408, %rax, %rcx        # imm = 0x198
	movzbl	%r11b, %eax
	movq	(%rbx,%rax,8), %rax
	movslq	36(%rsp), %rbx
	movq	(%rax,%rbx,8), %rax
	movslq	32(%rsp), %rbx
	movsbl	(%rax,%rbx), %edi
	cmpl	$0, 356(%rbp,%rcx)
	sete	%cl
	shll	%cl, %edi
	jmp	.LBB13_30
.LBB13_17:
	movl	80(%rsp), %r10d
	movl	$-1, %r9d
	testl	%r10d, %r10d
	movl	$-1, %esi
	je	.LBB13_19
# BB#18:
	movzbl	%r11b, %eax
	movq	(%rbx,%rax,8), %rax
	movslq	100(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	96(%rsp), %rcx
	movsbl	(%rax,%rcx), %esi
.LBB13_19:
	movl	56(%rsp), %r12d
	testl	%r12d, %r12d
	je	.LBB13_21
# BB#20:
	movzbl	%r11b, %eax
	movq	(%rbx,%rax,8), %rax
	movslq	76(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	72(%rsp), %rcx
	movsbl	(%rax,%rcx), %r9d
.LBB13_21:
	movl	16(%rsp), %r13d
	testl	%r13d, %r13d
	je	.LBB13_29
# BB#22:
	movzbl	%r11b, %eax
	movq	(%rbx,%rax,8), %rax
	movslq	36(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	32(%rsp), %rcx
	movsbl	(%rax,%rcx), %edi
	jmp	.LBB13_30
.LBB13_23:
	testl	%r10d, %r10d
	movl	$-1, %esi
	je	.LBB13_25
# BB#24:
	movslq	84(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rbp,%rax)
	setne	%cl
	movzbl	%r11b, %eax
	movq	(%rbx,%rax,8), %rax
	movslq	100(%rsp), %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	96(%rsp), %rdx
	movsbl	(%rax,%rdx), %esi
	sarl	%cl, %esi
.LBB13_25:
	movl	56(%rsp), %r12d
	testl	%r12d, %r12d
	je	.LBB13_27
# BB#26:
	movslq	60(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rbp,%rax)
	setne	%cl
	movzbl	%r11b, %eax
	movq	(%rbx,%rax,8), %rax
	movslq	76(%rsp), %rdi
	movq	(%rax,%rdi,8), %rax
	movslq	72(%rsp), %rdi
	movsbl	(%rax,%rdi), %r9d
	sarl	%cl, %r9d
.LBB13_27:
	movl	16(%rsp), %r13d
	testl	%r13d, %r13d
	je	.LBB13_29
# BB#28:
	movslq	20(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rbp,%rax)
	setne	%cl
	movzbl	%r11b, %eax
	movq	(%rbx,%rax,8), %rax
	movslq	36(%rsp), %rbp
	movq	(%rax,%rbp,8), %rax
	movslq	32(%rsp), %rbp
	movsbl	(%rax,%rbp), %edi
	sarl	%cl, %edi
	jmp	.LBB13_30
.LBB13_29:
	movl	$-1, %edi
	xorl	%r13d, %r13d
.LBB13_30:
	movl	8(%rsp), %eax           # 4-byte Reload
	movsbl	%al, %ecx
	cmpl	%ecx, %esi
	setne	%bl
	cmpl	%ecx, %r9d
	je	.LBB13_33
# BB#31:
	testb	%bl, %bl
	jne	.LBB13_33
# BB#32:
	movl	$1, %ebp
	cmpl	%ecx, %edi
	jne	.LBB13_36
.LBB13_33:
	cmpl	%ecx, %r9d
	sete	%dl
	cmpl	%ecx, %esi
	setne	%al
	sete	%r8b
	andb	%dl, %al
	cmpl	%ecx, %edi
	setne	%dl
	movl	%eax, %ebx
	andb	%dl, %bl
	movzbl	%bl, %ebp
	addl	%ebp, %ebp
	andb	%dl, %al
	jne	.LBB13_36
# BB#34:
	testb	%r8b, %r8b
	jne	.LBB13_36
# BB#35:
	xorl	%eax, %eax
	cmpl	%ecx, %edi
	sete	%al
	xorl	%edx, %edx
	cmpl	%ecx, %r9d
	leal	(%rax,%rax,2), %ebp
	cmovel	%edx, %ebp
.LBB13_36:
	movl	264(%rsp), %ebx
	cmpl	$8, %r14d
	jne	.LBB13_40
# BB#37:
	cmpl	$16, %ebx
	jne	.LBB13_40
# BB#38:
	movl	240(%rsp), %eax
	testl	%eax, %eax
	je	.LBB13_43
# BB#39:
	cmpl	%ecx, %edi
	movl	$3, %eax
	jmp	.LBB13_45
.LBB13_40:
	cmpl	$16, %r14d
	jne	.LBB13_46
# BB#41:
	cmpl	$8, %ebx
	jne	.LBB13_46
# BB#42:
	movl	248(%rsp), %eax
	testl	%eax, %eax
	je	.LBB13_44
.LBB13_43:
	cmpl	%ecx, %esi
	movl	$1, %eax
	jmp	.LBB13_45
.LBB13_44:
	cmpl	%ecx, %r9d
	movl	$2, %eax
.LBB13_45:
	cmovel	%eax, %ebp
.LBB13_46:
	xorl	%edi, %edi
	movl	%r10d, %ecx
	testl	%r10d, %r10d
	movzbl	%r11b, %eax
	movslq	36(%rsp), %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movslq	32(%rsp), %rsi
	movslq	76(%rsp), %r8
	movslq	72(%rsp), %rbx
	movslq	100(%rsp), %r10
	movslq	96(%rsp), %r9
	movslq	84(%rsp), %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movslq	20(%rsp), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movslq	60(%rsp), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movl	$0, %r11d
	je	.LBB13_48
# BB#47:
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx,%rax,8), %rdx
	movq	(%rdx,%r10,8), %rdx
	movq	(%rdx,%r9,8), %rdx
	movswl	(%rdx), %r11d
.LBB13_48:
	testl	%r12d, %r12d
	je	.LBB13_50
# BB#49:
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx,%rax,8), %rdx
	movq	(%rdx,%r8,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movswl	(%rdx), %edi
.LBB13_50:
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	testl	%r13d, %r13d
	movl	$0, %ebx
	je	.LBB13_52
# BB#51:
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	(%rdx,%rax,8), %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	(%rdx,%rbx,8), %rdx
	movq	(%rdx,%rsi,8), %rdx
	movswl	(%rdx), %ebx
.LBB13_52:
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	movl	%r13d, %edx
	orl	%r12d, %edx
	movl	%ebp, %esi
	cmpl	$3, %ebp
	ja	.LBB13_60
# BB#53:
	jmpq	*.LJTI13_0(,%rsi,8)
.LBB13_54:
	testl	%edx, %edx
	je	.LBB13_59
# BB#55:
	leal	(%rdi,%r11), %r14d
	addl	%ebx, %r14d
	cmpl	%ebx, %edi
	movl	%r15d, 116(%rsp)        # 4-byte Spill
	movq	%rsi, %r15
	movl	%ebx, %esi
	cmovlel	%edi, %esi
	cmovgel	%edi, %ebx
	cmpl	%esi, %r11d
	cmovlel	%r11d, %esi
	subl	%esi, %r14d
	movq	%r15, %rsi
	movl	116(%rsp), %r15d        # 4-byte Reload
	cmpl	%ebx, %r11d
	cmovgel	%r11d, %ebx
	subl	%ebx, %r14d
	jmp	.LBB13_60
.LBB13_59:
	movl	%r11d, %r14d
	jmp	.LBB13_60
.LBB13_57:
	movl	%edi, %r14d
	jmp	.LBB13_60
.LBB13_58:
	movl	%ebx, %r14d
.LBB13_60:
	testl	%r15d, %r15d
	movq	152(%rsp), %r15         # 8-byte Reload
	movw	%r14w, (%r15)
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	je	.LBB13_71
# BB#61:
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	5600(%rsi), %r11
	movl	4(%rsi), %esi
	imulq	$408, %rsi, %rsi        # imm = 0x198
	cmpl	$0, 356(%r11,%rsi)
	je	.LBB13_77
# BB#62:
	xorl	%edi, %edi
	testl	%ecx, %ecx
	movl	$0, %esi
	je	.LBB13_65
# BB#63:
	imulq	$408, 120(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x198
	cmpl	$0, 356(%r11,%rcx)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movswl	2(%rcx), %esi
	jne	.LBB13_65
# BB#64:                                # %select.false.sink
	movl	%esi, %ecx
	shrl	$31, %ecx
	addl	%esi, %ecx
	sarl	%ecx
	movl	%ecx, %esi
.LBB13_65:
	testl	%r12d, %r12d
	je	.LBB13_68
# BB#66:
	imulq	$408, 128(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x198
	cmpl	$0, 356(%r11,%rcx)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movswl	2(%rcx), %edi
	jne	.LBB13_68
# BB#67:                                # %select.false.sink223
	movl	%edi, %ecx
	shrl	$31, %ecx
	addl	%edi, %ecx
	sarl	%ecx
	movl	%ecx, %edi
.LBB13_68:
	testl	%r13d, %r13d
	je	.LBB13_84
# BB#69:
	imulq	$408, 136(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x198
	cmpl	$0, 356(%r11,%rcx)
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	2(%rax), %eax
	jne	.LBB13_86
# BB#70:                                # %select.false.sink229
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movl	%ecx, %eax
	cmpl	$3, %ebp
	jbe	.LBB13_87
	jmp	.LBB13_94
.LBB13_71:
	xorl	%edi, %edi
	testl	%ecx, %ecx
	movl	$0, %esi
	je	.LBB13_73
# BB#72:
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movq	(%rcx,%r10,8), %rcx
	movq	(%rcx,%r9,8), %rcx
	movswl	2(%rcx), %esi
.LBB13_73:
	testl	%r12d, %r12d
	je	.LBB13_75
# BB#74:
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rcx,%rdi,8), %rcx
	movswl	2(%rcx), %edi
.LBB13_75:
	testl	%r13d, %r13d
	je	.LBB13_84
# BB#76:
	movq	(%rsp), %rcx            # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movswl	2(%rax), %eax
	cmpl	$3, %ebp
	jbe	.LBB13_87
	jmp	.LBB13_94
.LBB13_84:
	xorl	%eax, %eax
	cmpl	$3, %ebp
	jbe	.LBB13_87
	jmp	.LBB13_94
.LBB13_77:
	xorl	%edi, %edi
	testl	%ecx, %ecx
	movl	$0, %esi
	je	.LBB13_79
# BB#78:
	imulq	$408, 120(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x198
	cmpl	$0, 356(%r11,%rcx)
	setne	%cl
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	(%rsi,%rax,8), %rsi
	movq	(%rsi,%r10,8), %rsi
	movq	(%rsi,%r9,8), %rsi
	movswl	2(%rsi), %esi
	shll	%cl, %esi
.LBB13_79:
	testl	%r12d, %r12d
	je	.LBB13_81
# BB#80:
	imulq	$408, 128(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x198
	cmpl	$0, 356(%r11,%rcx)
	setne	%cl
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi,%rax,8), %rdi
	movq	(%rdi,%r8,8), %rdi
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	(%rdi,%rbx,8), %rdi
	movswl	2(%rdi), %edi
	shll	%cl, %edi
.LBB13_81:
	testl	%r13d, %r13d
	je	.LBB13_85
# BB#82:
	imulq	$408, 136(%rsp), %rcx   # 8-byte Folded Reload
                                        # imm = 0x198
	cmpl	$0, 356(%r11,%rcx)
	setne	%cl
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	(%rbx,%rax,8), %rax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movswl	2(%rax), %eax
	shll	%cl, %eax
	cmpl	$3, %ebp
	jbe	.LBB13_87
	jmp	.LBB13_94
.LBB13_85:
	xorl	%eax, %eax
.LBB13_86:
	cmpl	$3, %ebp
	ja	.LBB13_94
.LBB13_87:
	movq	104(%rsp), %rcx         # 8-byte Reload
	jmpq	*.LJTI13_1(,%rcx,8)
.LBB13_88:
	testl	%edx, %edx
	je	.LBB13_93
# BB#89:
	leal	(%rdi,%rsi), %r14d
	addl	%eax, %r14d
	cmpl	%eax, %edi
	movl	%eax, %ecx
	cmovlel	%edi, %ecx
	cmovgel	%edi, %eax
	cmpl	%ecx, %esi
	cmovlel	%esi, %ecx
	subl	%ecx, %r14d
	cmpl	%eax, %esi
	cmovgel	%esi, %eax
	subl	%eax, %r14d
	jmp	.LBB13_94
.LBB13_93:
	movl	%esi, %r14d
	jmp	.LBB13_94
.LBB13_91:
	movl	%edi, %r14d
	jmp	.LBB13_94
.LBB13_92:
	movl	%eax, %r14d
.LBB13_94:
	movw	%r14w, 2(%r15)
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	SetMotionVectorPredictor, .Lfunc_end13-SetMotionVectorPredictor
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI13_0:
	.quad	.LBB13_54
	.quad	.LBB13_59
	.quad	.LBB13_57
	.quad	.LBB13_58
.LJTI13_1:
	.quad	.LBB13_88
	.quad	.LBB13_93
	.quad	.LBB13_91
	.quad	.LBB13_92

	.text
	.globl	read_ipred_modes
	.p2align	4, 0x90
	.type	read_ipred_modes,@function
read_ipred_modes:                       # @read_ipred_modes
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 288
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	5600(%r12), %rcx
	movl	4(%r12), %eax
	imulq	$408, %rax, %rax        # imm = 0x198
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movl	40(%rcx,%rax), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	sbbb	%dl, %dl
	movb	$59, %al
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %al
	andb	%dl, %al
	andb	$1, %al
	movq	5592(%r12), %rcx
	movslq	28(%rcx), %rdx
	movl	$4, 184(%rsp)
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	40(%rcx), %r8
	leaq	(%rdx,%rdx,4), %rbx
	shlq	$4, %rbx
	movslq	assignSE2partition+16(%rbx), %rsi
	imulq	$56, %rsi, %rsi
	leaq	(%r8,%rsi), %rdx
	movq	active_pps(%rip), %rdi
	cmpl	$0, 12(%rdi)
	je	.LBB14_3
# BB#1:
	movq	(%rdx), %rdi
	cmpl	$0, 24(%rdi)
	jne	.LBB14_3
# BB#2:
	movq	$readIntraPredMode_CABAC, 224(%rsp)
.LBB14_3:                               # %.preheader
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	leaq	40(%rdi,%rbp), %rcx
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movzbl	%al, %eax
	leaq	assignSE2partition+16(%rbx), %rbx
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	leaq	48(%r8,%rsi), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	xorl	%esi, %esi
	leaq	(%rdi,%rbp), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB14_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_7 Depth 2
                                        #       Child Loop BB14_8 Depth 3
                                        #         Child Loop BB14_32 Depth 4
	movq	88(%rsp), %rcx          # 8-byte Reload
	movb	328(%rsi,%rcx), %cl
	cmpb	$13, %cl
	je	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_4 Depth=1
	cmpb	$11, %cl
	jne	.LBB14_36
.LBB14_6:                               #   in Loop: Header=BB14_4 Depth=1
	movl	%esi, %eax
	andl	$2, %eax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	cmpb	$13, %cl
	sete	%r13b
	incl	%r13d
	movl	%esi, %eax
	andl	$2, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	leal	(%rsi,%rsi), %eax
	andl	$2, %eax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	leal	(,%rsi,4), %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_7:                               #   Parent Loop BB14_4 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_8 Depth 3
                                        #         Child Loop BB14_32 Depth 4
	movq	112(%rsp), %rax         # 8-byte Reload
	leal	(%rcx,%rax), %eax
	leal	(,%rax,4), %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	leal	-1(,%rax,4), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	104(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx,2), %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movq	120(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 128(%rsp)         # 8-byte Spill
	leal	(%rax,%rcx), %eax
	addl	76(%r12), %eax
	movslq	%eax, %r15
	shlq	$3, %r15
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_8:                               #   Parent Loop BB14_4 Depth=1
                                        #     Parent Loop BB14_7 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB14_32 Depth 4
	movq	168(%rsp), %rax         # 8-byte Reload
	leal	(%rcx,%rax), %ebp
	movslq	92(%r12), %r14
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	je	.LBB14_10
# BB#9:                                 #   in Loop: Header=BB14_8 Depth=3
	movq	(%rdx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB14_11
.LBB14_10:                              #   in Loop: Header=BB14_8 Depth=3
	leaq	184(%rsp), %rdi
	movq	%r12, %rsi
	callq	readSyntaxElement_Intra4x4PredictionMode
	jmp	.LBB14_12
	.p2align	4, 0x90
.LBB14_11:                              #   in Loop: Header=BB14_8 Depth=3
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%rcx), %eax
	movl	%eax, 208(%rsp)
	leaq	184(%rsp), %rdi
	movq	%r12, %rsi
	movq	136(%rsp), %rax         # 8-byte Reload
	callq	*(%rax)
.LBB14_12:                              #   in Loop: Header=BB14_8 Depth=3
	movl	4(%r12), %edi
	leal	(,%rbp,4), %ebx
	leal	-1(,%rbp,4), %esi
	movl	4(%rsp), %edx           # 4-byte Reload
	leaq	32(%rsp), %rcx
	callq	getLuma4x4Neighbour
	movl	4(%r12), %edi
	movl	%ebx, %esi
	movl	(%rsp), %edx            # 4-byte Reload
	leaq	8(%rsp), %rcx
	callq	getLuma4x4Neighbour
	movq	active_pps(%rip), %rax
	cmpl	$0, 1148(%rax)
	je	.LBB14_18
# BB#13:                                #   in Loop: Header=BB14_8 Depth=3
	xorl	%eax, %eax
	cmpl	$0, 32(%rsp)
	movl	$0, %ecx
	je	.LBB14_15
# BB#14:                                #   in Loop: Header=BB14_8 Depth=3
	movq	16(%r12), %rcx
	movslq	36(%rsp), %rdx
	movl	(%rcx,%rdx,4), %ecx
.LBB14_15:                              #   in Loop: Header=BB14_8 Depth=3
	movl	%ecx, 32(%rsp)
	cmpl	$0, 8(%rsp)
	je	.LBB14_17
# BB#16:                                #   in Loop: Header=BB14_8 Depth=3
	movq	16(%r12), %rax
	movslq	12(%rsp), %rcx
	movl	(%rax,%rcx,4), %eax
.LBB14_17:                              #   in Loop: Header=BB14_8 Depth=3
	movl	%eax, 8(%rsp)
.LBB14_18:                              #   in Loop: Header=BB14_8 Depth=3
	movb	$1, %cl
	xorl	%eax, %eax
	movq	152(%rsp), %rdx         # 8-byte Reload
	cmpl	$9, (%rdx)
	jne	.LBB14_25
# BB#19:                                #   in Loop: Header=BB14_8 Depth=3
	cmpl	$4, 44(%r12)
	jne	.LBB14_25
# BB#20:                                #   in Loop: Header=BB14_8 Depth=3
	cmpl	$0, 32(%rsp)
	je	.LBB14_21
# BB#22:                                #   in Loop: Header=BB14_8 Depth=3
	movq	5568(%r12), %rax
	movslq	52(%rsp), %rdx
	movq	(%rax,%rdx,8), %rdx
	movslq	48(%rsp), %rsi
	xorl	%eax, %eax
	cmpl	$0, (%rdx,%rsi,4)
	setne	%al
	cmpl	$0, 8(%rsp)
	jne	.LBB14_24
	jmp	.LBB14_25
.LBB14_21:                              #   in Loop: Header=BB14_8 Depth=3
	xorl	%eax, %eax
	cmpl	$0, 8(%rsp)
	je	.LBB14_25
.LBB14_24:                              #   in Loop: Header=BB14_8 Depth=3
	movq	5568(%r12), %rcx
	movslq	28(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	24(%rsp), %rdx
	cmpl	$0, (%rcx,%rdx,4)
	sete	%cl
	.p2align	4, 0x90
.LBB14_25:                              #   in Loop: Header=BB14_8 Depth=3
	movl	$-1, %edx
	testb	%cl, %cl
	movl	$-1, %ecx
	je	.LBB14_28
# BB#26:                                #   in Loop: Header=BB14_8 Depth=3
	movl	8(%rsp), %ecx
	testl	%ecx, %ecx
	movl	$-1, %ecx
	je	.LBB14_28
# BB#27:                                #   in Loop: Header=BB14_8 Depth=3
	movq	5544(%r12), %rcx
	movslq	28(%rsp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	24(%rsp), %rsi
	movzbl	(%rcx,%rsi), %ecx
.LBB14_28:                              #   in Loop: Header=BB14_8 Depth=3
	movslq	%ebp, %rsi
	testl	%eax, %eax
	jne	.LBB14_31
# BB#29:                                #   in Loop: Header=BB14_8 Depth=3
	movl	32(%rsp), %eax
	testl	%eax, %eax
	je	.LBB14_31
# BB#30:                                #   in Loop: Header=BB14_8 Depth=3
	movq	5544(%r12), %rax
	movslq	52(%rsp), %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	48(%rsp), %rdx
	movzbl	(%rax,%rdx), %edx
.LBB14_31:                              #   in Loop: Header=BB14_8 Depth=3
	addq	%rsi, %r14
	cmpl	%edx, %ecx
	movl	%edx, %eax
	cmovlel	%ecx, %eax
	orl	%ecx, %edx
	movl	$2, %ecx
	cmovsl	%ecx, %eax
	movl	188(%rsp), %ecx
	xorl	%ebx, %ebx
	cmpl	%eax, %ecx
	setge	%bl
	addl	%ecx, %ebx
	cmpl	$-1, %ecx
	cmovel	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_32:                              #   Parent Loop BB14_4 Depth=1
                                        #     Parent Loop BB14_7 Depth=2
                                        #       Parent Loop BB14_8 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	5544(%r12), %rax
	addq	%r15, %rax
	movq	(%rax,%rbp,8), %rdi
	addq	%r14, %rdi
	movzbl	%bl, %esi
	movq	%r13, %rdx
	callq	memset
	incq	%rbp
	cmpq	%r13, %rbp
	jl	.LBB14_32
# BB#33:                                #   in Loop: Header=BB14_8 Depth=3
	movq	176(%rsp), %rcx         # 8-byte Reload
	addl	%r13d, %ecx
	cmpl	$2, %ecx
	movq	160(%rsp), %rdx         # 8-byte Reload
	jl	.LBB14_8
# BB#34:                                #   in Loop: Header=BB14_7 Depth=2
	movq	128(%rsp), %rcx         # 8-byte Reload
	addl	%ebp, %ecx
	cmpl	$2, %ecx
	jl	.LBB14_7
# BB#35:                                #   in Loop: Header=BB14_4 Depth=1
	movl	$1, %eax
	movq	96(%rsp), %rsi          # 8-byte Reload
.LBB14_36:                              # %.loopexit
                                        #   in Loop: Header=BB14_4 Depth=1
	incq	%rsi
	cmpq	$4, %rsi
	jne	.LBB14_4
# BB#37:
	testl	%eax, %eax
	je	.LBB14_45
# BB#38:
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB14_45
# BB#39:
	movl	$4, 184(%rsp)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	40(%rax), %rax
	movq	72(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	je	.LBB14_41
# BB#40:
	movq	(%rdx), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB14_42
.LBB14_41:
	movq	$linfo_ue, 216(%rsp)
	jmp	.LBB14_43
.LBB14_42:
	movq	$readCIPredMode_CABAC, 224(%rsp)
.LBB14_43:
	leaq	184(%rsp), %rdi
	movq	%r12, %rsi
	callq	*48(%rax,%rcx)
	movl	188(%rsp), %eax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movl	%eax, 352(%rcx,%rdx)
	cmpl	$4, %eax
	jb	.LBB14_45
# BB#44:
	movl	$.L.str.2, %edi
	movl	$600, %esi              # imm = 0x258
	callq	error
.LBB14_45:
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end14:
	.size	read_ipred_modes, .Lfunc_end14-read_ipred_modes
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.zero	16
	.text
	.globl	readCBPandCoeffsFromNAL
	.p2align	4, 0x90
	.type	readCBPandCoeffsFromNAL,@function
readCBPandCoeffsFromNAL:                # @readCBPandCoeffsFromNAL
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi106:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi107:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi108:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Lcfi110:
	.cfi_def_cfa_offset 672
.Lcfi111:
	.cfi_offset %rbx, -56
.Lcfi112:
	.cfi_offset %r12, -48
.Lcfi113:
	.cfi_offset %r13, -40
.Lcfi114:
	.cfi_offset %r14, -32
.Lcfi115:
	.cfi_offset %r15, -24
.Lcfi116:
	.cfi_offset %rbp, -16
	movq	%rdi, %r11
	movq	5600(%r11), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movslq	4(%r11), %r15
	movq	5592(%r11), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	movslq	28(%rax), %rdx
	movl	44(%r11), %esi
	cmpl	$3, %esi
	jne	.LBB15_3
# BB#1:
	imulq	$408, %r15, %rax        # imm = 0x198
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	40(%rcx,%rax), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	40(%rcx,%rax), %r8d
	movb	$1, %dil
	cmpl	$14, %r8d
	ja	.LBB15_6
# BB#2:
	movl	$26112, %ecx            # imm = 0x6600
	btl	%r8d, %ecx
	jb	.LBB15_4
	jmp	.LBB15_6
.LBB15_3:                               # %thread-pre-split
	cmpl	$4, %esi
	jne	.LBB15_4
# BB#5:
	imulq	$408, %r15, %rax        # imm = 0x198
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	40(%rcx,%rax), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	40(%rcx,%rax), %r8d
	cmpl	$12, %r8d
	sete	%dil
	jmp	.LBB15_6
.LBB15_4:                               # %._crit_edge1623
	imulq	$408, %r15, %rax        # imm = 0x198
	movq	24(%rsp), %rcx          # 8-byte Reload
	leaq	40(%rcx,%rax), %rcx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	40(%rcx,%rax), %r8d
	xorl	%edi, %edi
.LBB15_6:
	leal	-9(%r8), %ecx
	cmpl	$6, %ecx
	sbbb	%bl, %bl
	movb	%bl, 15(%rsp)           # 1-byte Spill
	movb	$59, %r14b
	shrb	%cl, %r14b
	movq	dec_picture(%rip), %rbp
	movslq	317044(%rbp), %rbp
	movq	%rbp, 296(%rsp)         # 8-byte Spill
	movl	28(%r11), %ebp
	addl	5884(%r11), %ebp
	jne	.LBB15_7
# BB#8:
	cmpl	$1, 5920(%r11)
	sete	%bpl
	movl	%ebp, 52(%rsp)          # 4-byte Spill
	cmpl	$0, 5584(%r11)
	jne	.LBB15_10
	jmp	.LBB15_11
.LBB15_7:
	movl	$0, 52(%rsp)            # 4-byte Folded Spill
	cmpl	$0, 5584(%r11)
	je	.LBB15_11
.LBB15_10:
	movl	$FIELD_SCAN, %ebp
	movq	%rbp, 176(%rsp)         # 8-byte Spill
	movl	$FIELD_SCAN8x8, %eax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	cmpl	$3, %esi
	je	.LBB15_13
	jmp	.LBB15_14
.LBB15_11:
	imulq	$408, %r15, %rbp        # imm = 0x198
	movq	24(%rsp), %rbx          # 8-byte Reload
	cmpl	$0, 356(%rbx,%rbp)
	movl	$SNGL_SCAN8x8, %ebp
	movl	$FIELD_SCAN8x8, %eax
	cmoveq	%rbp, %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movl	$SNGL_SCAN, %ebp
	movl	$FIELD_SCAN, %ebx
	cmoveq	%rbp, %rbx
	movq	%rbx, 176(%rsp)         # 8-byte Spill
	cmpl	$3, %esi
	jne	.LBB15_14
.LBB15_13:
	cmpl	$10, %r8d
	setne	%bl
	orb	%bl, %dil
.LBB15_14:
	movzbl	%dil, %eax
	movl	%eax, 240(%rsp)         # 4-byte Spill
	leaq	(%rdx,%rdx,4), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	cmpl	$0, 296(%rsp)           # 4-byte Folded Reload
	je	.LBB15_16
# BB#15:                                # %.preheader1349
	movl	5888(%r11), %edx
	imulq	$408, %r15, %rsi        # imm = 0x198
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	4(%rax,%rsi), %edi
	addl	%edx, %edi
	movslq	%edi, %rdi
	imulq	$715827883, %rdi, %rbp  # imm = 0x2AAAAAAB
	movq	%rbp, %rbx
	shrq	$63, %rbx
	shrq	$32, %rbp
	addl	%ebx, %ebp
	movl	%ebp, 216(%rsp)
	addl	%ebp, %ebp
	leal	(%rbp,%rbp,2), %ebp
	subl	%ebp, %edi
	movl	%edi, 248(%rsp)
	addl	8(%rax,%rsi), %edx
	movslq	%edx, %rdx
	imulq	$715827883, %rdx, %rsi  # imm = 0x2AAAAAAB
	movq	%rsi, %rdi
	shrq	$63, %rdi
	shrq	$32, %rsi
	addl	%edi, %esi
	movl	%esi, 220(%rsp)
	addl	%esi, %esi
	leal	(%rsi,%rsi,2), %esi
	subl	%esi, %edx
	movl	%edx, 252(%rsp)
.LBB15_16:                              # %.loopexit1350
	shlq	$4, 80(%rsp)            # 8-byte Folded Spill
	movl	$11, %edx
	cmpl	$14, %r8d
	movq	%r11, 16(%rsp)          # 8-byte Spill
	ja	.LBB15_21
# BB#17:                                # %.loopexit1350
	movl	$12800, %esi            # imm = 0x3200
	btl	%r8d, %esi
	jae	.LBB15_18
# BB#20:
	movl	$6, %edx
.LBB15_21:                              # %switch.edge1189
	movl	%edx, 96(%rsp)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movl	%edx, %edx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movslq	assignSE2partition(%rsi,%rdx,4), %rdx
	imulq	$56, %rdx, %rbp
	leaq	(%rax,%rbp), %rdx
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	je	.LBB15_23
# BB#22:
	movq	(%rdx), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB15_24
.LBB15_23:
	movl	$linfo_cbp_intra, %esi
	movl	$linfo_cbp_inter, %edi
	movl	$25, %ebx
	btl	%ecx, %ebx
	cmovaeq	%rdi, %rsi
	cmpl	$5, %ecx
	cmovaeq	%rdi, %rsi
	movq	%rsi, 128(%rsp)
	jmp	.LBB15_25
.LBB15_18:                              # %.loopexit1350
	movl	$17408, %esi            # imm = 0x4400
	btl	%r8d, %esi
	jae	.LBB15_21
# BB#19:
	imulq	$408, %r15, %rax        # imm = 0x198
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	300(%rcx,%rax), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	jmp	.LBB15_48
.LBB15_24:
	movq	$readCBP_CABAC, 136(%rsp)
.LBB15_25:
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rax,%rbp)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	imulq	$408, %r15, %rbx        # imm = 0x198
	movq	24(%rsp), %rdx          # 8-byte Reload
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	%eax, 300(%rdx,%rbx)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	leal	-1(%rax), %ecx
	cmpl	$3, %ecx
	movq	%rdx, %rcx
	jb	.LBB15_30
# BB#26:
	testl	%eax, %eax
	jne	.LBB15_29
# BB#27:
	cmpl	$1, 44(%r11)
	jne	.LBB15_29
# BB#28:
	movq	active_sps(%rip), %rcx
	cmpl	$0, 2084(%rcx)
	movq	24(%rsp), %rcx          # 8-byte Reload
	jne	.LBB15_31
.LBB15_29:
	cmpl	$0, 400(%rcx,%rbx)
	je	.LBB15_38
.LBB15_30:                              # %thread-pre-split1285
	orl	$4, %eax
	cmpl	$13, %eax
	jne	.LBB15_31
.LBB15_38:                              # %.critedge1192
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	je	.LBB15_39
.LBB15_40:
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	addl	$-9, %eax
	movl	$12, %ecx
	xorl	%edx, %edx
	btl	%eax, %ecx
	setae	%dl
	orl	$16, %edx
	cmpl	$5, %eax
	movl	$16, %ecx
	cmovbel	%edx, %ecx
	movl	%ecx, 96(%rsp)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	80(%rsp), %rdx          # 8-byte Reload
	movslq	assignSE2partition(%rdx,%rcx,4), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	je	.LBB15_42
# BB#41:
	movq	(%rdx), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB15_43
.LBB15_42:
	movq	$linfo_se, 128(%rsp)
	jmp	.LBB15_44
.LBB15_31:                              # %thread-pre-split1285.thread
	testb	$15, 64(%rsp)           # 1-byte Folded Reload
	je	.LBB15_38
# BB#32:
	cmpl	$0, 5908(%r11)
	je	.LBB15_38
# BB#33:
	movq	80(%rsp), %rax          # 8-byte Reload
	leaq	assignSE2partition(%rax), %rcx
	movl	$0, 96(%rsp)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movslq	(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	movq	$readMB_transform_size_flag_CABAC, 136(%rsp)
	movq	active_pps(%rip), %rdx
	cmpl	$0, 12(%rdx)
	movq	(%rax,%rcx), %rsi
	je	.LBB15_35
# BB#34:
	cmpl	$0, 24(%rsi)
	je	.LBB15_36
.LBB15_35:                              # %._crit_edge1624
	movl	$1, 108(%rsp)
	leaq	96(%rsp), %rdi
	callq	readSyntaxElement_FLC
	jmp	.LBB15_37
.LBB15_43:
	movq	$readDquant_CABAC, 136(%rsp)
.LBB15_44:
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rax,%rcx)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 16(%rcx,%rbx)
	movl	5884(%r11), %ecx
	movl	%ecx, %edx
	shrl	$31, %edx
	addl	%ecx, %edx
	sarl	%edx
	movl	$-26, %esi
	subl	%edx, %esi
	cmpl	%esi, %eax
	jl	.LBB15_46
# BB#45:
	addl	$25, %edx
	cmpl	%edx, %eax
	jle	.LBB15_47
.LBB15_46:
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax,%rbx), %rbx
	movl	$.L.str.5, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	(%rbx), %eax
	movl	5884(%r11), %ecx
.LBB15_47:
	addl	28(%r11), %eax
	leal	52(%rax,%rcx,2), %eax
	leal	52(%rcx), %esi
	cltd
	idivl	%esi
	subl	%ecx, %edx
	movl	%edx, 28(%r11)
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB15_48
.LBB15_36:
	leaq	(%rax,%rcx), %rdx
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rax,%rcx)
.LBB15_37:
	movl	100(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 396(%rcx,%rbx)
	movq	16(%rsp), %r11          # 8-byte Reload
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	jne	.LBB15_40
.LBB15_39:
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
.LBB15_48:                              # %.preheader1348
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 2648(%r11)
	movdqu	%xmm0, 2632(%r11)
	movdqu	%xmm0, 2616(%r11)
	movdqu	%xmm0, 2600(%r11)
	movdqu	%xmm0, 2584(%r11)
	movdqu	%xmm0, 2568(%r11)
	movdqu	%xmm0, 2552(%r11)
	movdqu	%xmm0, 2536(%r11)
	movdqu	%xmm0, 2520(%r11)
	movdqu	%xmm0, 2504(%r11)
	movdqu	%xmm0, 2488(%r11)
	movdqu	%xmm0, 2472(%r11)
	movdqu	%xmm0, 2456(%r11)
	movdqu	%xmm0, 2440(%r11)
	movdqu	%xmm0, 2424(%r11)
	movdqu	%xmm0, 2408(%r11)
	movdqu	%xmm0, 3416(%r11)
	movdqu	%xmm0, 3400(%r11)
	movdqu	%xmm0, 3384(%r11)
	movdqu	%xmm0, 3368(%r11)
	movdqu	%xmm0, 3352(%r11)
	movdqu	%xmm0, 3336(%r11)
	movdqu	%xmm0, 3320(%r11)
	movdqu	%xmm0, 3304(%r11)
	movdqu	%xmm0, 3288(%r11)
	movdqu	%xmm0, 3272(%r11)
	movdqu	%xmm0, 3256(%r11)
	movdqu	%xmm0, 3240(%r11)
	movdqu	%xmm0, 3224(%r11)
	movdqu	%xmm0, 3208(%r11)
	movdqu	%xmm0, 3192(%r11)
	movdqu	%xmm0, 3176(%r11)
	movdqu	%xmm0, 4184(%r11)
	movdqu	%xmm0, 4168(%r11)
	movdqu	%xmm0, 4152(%r11)
	movdqu	%xmm0, 4136(%r11)
	movdqu	%xmm0, 4120(%r11)
	movdqu	%xmm0, 4104(%r11)
	movdqu	%xmm0, 4088(%r11)
	movdqu	%xmm0, 4072(%r11)
	movdqu	%xmm0, 4056(%r11)
	movdqu	%xmm0, 4040(%r11)
	movdqu	%xmm0, 4024(%r11)
	movdqu	%xmm0, 4008(%r11)
	movdqu	%xmm0, 3992(%r11)
	movdqu	%xmm0, 3976(%r11)
	movdqu	%xmm0, 3960(%r11)
	movdqu	%xmm0, 3944(%r11)
	movdqu	%xmm0, 4952(%r11)
	movdqu	%xmm0, 4936(%r11)
	movdqu	%xmm0, 4920(%r11)
	movdqu	%xmm0, 4904(%r11)
	movdqu	%xmm0, 4888(%r11)
	movdqu	%xmm0, 4872(%r11)
	movdqu	%xmm0, 4856(%r11)
	movdqu	%xmm0, 4840(%r11)
	movdqu	%xmm0, 4824(%r11)
	movdqu	%xmm0, 4808(%r11)
	movdqu	%xmm0, 4792(%r11)
	movdqu	%xmm0, 4776(%r11)
	movdqu	%xmm0, 4760(%r11)
	movdqu	%xmm0, 4744(%r11)
	movdqu	%xmm0, 4728(%r11)
	movdqu	%xmm0, 4712(%r11)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	orl	$4, %eax
	cmpl	$14, %eax
	jne	.LBB15_70
# BB#49:
	movl	$17, 96(%rsp)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movslq	assignSE2partition+68(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	je	.LBB15_51
# BB#50:
	movq	(%rdx), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB15_52
.LBB15_51:
	movq	$linfo_se, 128(%rsp)
	jmp	.LBB15_53
.LBB15_52:
	movq	$readDquant_CABAC, 136(%rsp)
.LBB15_53:
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rax,%rcx)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	imulq	$408, %r15, %rdx        # imm = 0x198
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 16(%rcx,%rdx)
	movl	5884(%r11), %ecx
	movl	%ecx, %esi
	shrl	$31, %esi
	addl	%ecx, %esi
	sarl	%esi
	movl	$-26, %edi
	subl	%esi, %edi
	cmpl	%edi, %eax
	jl	.LBB15_55
# BB#54:
	addl	$25, %esi
	cmpl	%esi, %eax
	jle	.LBB15_56
.LBB15_55:
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	16(%rax,%rdx), %rbx
	movl	$.L.str.5, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	(%rbx), %eax
	movl	5884(%r11), %ecx
.LBB15_56:                              # %.preheader1347
	addl	28(%r11), %eax
	leal	52(%rax,%rcx,2), %eax
	leal	52(%rcx), %esi
	cltd
	idivl	%esi
	subl	%ecx, %edx
	movl	%edx, 28(%r11)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, (%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	8(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, (%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	16(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, (%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	24(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, (%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 1(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	8(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 1(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	16(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 1(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	24(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 1(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 2(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	8(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 2(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	16(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 2(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	24(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 2(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 3(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	8(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 3(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	16(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 3(%rax,%rcx)
	movq	5544(%r11), %rax
	movslq	76(%r11), %rcx
	movq	24(%rax,%rcx,8), %rax
	movslq	92(%r11), %rcx
	movb	$2, 3(%rax,%rcx)
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	movq	%r15, 40(%rsp)          # 8-byte Spill
	je	.LBB15_57
# BB#62:
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movslq	assignSE2partition+28(%rcx), %rcx
	imulq	$56, %rcx, %rcx
	movl	$0, 120(%rsp)
	movl	$7, 96(%rsp)
	movl	$1, 5616(%r11)
	movq	(%rax,%rcx), %rdx
	cmpl	$0, 24(%rdx)
	je	.LBB15_64
# BB#63:
	movl	%r14d, %r15d
	movq	$linfo_levrun_inter, 128(%rsp)
	jmp	.LBB15_65
.LBB15_57:
	movl	%r14d, %r15d
	leaq	36(%rsp), %rax
	leaq	416(%rsp), %rsi
	leaq	480(%rsp), %r9
	movl	$1, %edx
	movl	$0, %ecx
	movl	$0, %r8d
	movq	%r11, %rdi
	pushq	%rax
.Lcfi117:
	.cfi_adjust_cfa_offset 8
	pushq	%rsi
.Lcfi118:
	.cfi_adjust_cfa_offset 8
	callq	readCoeff4x4_CAVLC
	movq	32(%rsp), %r11          # 8-byte Reload
	addq	$16, %rsp
.Lcfi119:
	.cfi_adjust_cfa_offset -16
	movslq	36(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB15_68
# BB#58:                                # %.lr.ph1431
	movl	$-1, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB15_59:                              # =>This Inner Loop Header: Depth=1
	movl	480(%rsp,%rdx,4), %esi
	testl	%esi, %esi
	je	.LBB15_61
# BB#60:                                #   in Loop: Header=BB15_59 Depth=1
	movl	416(%rsp,%rdx,4), %edi
	leal	1(%rcx,%rdi), %ecx
	movslq	%ecx, %rdi
	movq	176(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rbx
	movzbl	(%rbx,%rdi,2), %ebp
	movzbl	1(%rbx,%rdi,2), %edi
	leaq	(%rbp,%rbp,2), %rbp
	shlq	$8, %rbp
	addq	%r11, %rbp
	shlq	$6, %rdi
	movl	%esi, 2408(%rdi,%rbp)
.LBB15_61:                              #   in Loop: Header=BB15_59 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB15_59
	jmp	.LBB15_68
.LBB15_64:
	movl	%r14d, %r15d
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_65:                              # %.preheader1345
	leaq	(%rax,%rcx), %r12
	leaq	48(%rax,%rcx), %rbp
	movl	$-1, %ebx
	xorl	%r13d, %r13d
	leaq	96(%rsp), %r14
	.p2align	4, 0x90
.LBB15_66:                              # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	movq	%r11, %rsi
	movq	%r12, %rdx
	callq	*(%rbp)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	testl	%eax, %eax
	je	.LBB15_68
# BB#67:                                #   in Loop: Header=BB15_66 Depth=1
	movl	104(%rsp), %ecx
	leal	1(%rbx,%rcx), %ebx
	movslq	%ebx, %rcx
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rsi
	movzbl	(%rsi,%rcx,2), %edx
	movzbl	1(%rsi,%rcx,2), %ecx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$8, %rdx
	addq	%r11, %rdx
	shlq	$6, %rcx
	movl	%eax, 2408(%rcx,%rdx)
	incl	%r13d
	cmpl	$17, %r13d
	jl	.LBB15_66
.LBB15_68:                              # %.loopexit1344
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%r15d, %r14d
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB15_70
# BB#69:
	movq	%r11, %rdi
	callq	itrans_2
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB15_70:
	andb	%r14b, 15(%rsp)         # 1-byte Folded Spill
	movl	28(%r11), %eax
	imulq	$408, %r15, %rbp        # imm = 0x198
	movl	%eax, (%rcx,%rbp)
	movq	img(%rip), %r10
	movq	dec_picture(%rip), %r8
	xorl	%edi, %edi
	xorl	%edx, %edx
	subl	5888(%r10), %edx
	movq	%rcx, %rbx
	movl	317076(%r8), %ecx
	addl	%eax, %ecx
	cmpl	%edx, %ecx
	cmovll	%edx, %ecx
	cmpl	$52, %ecx
	movl	$51, %esi
	movl	$51, %edx
	cmovll	%ecx, %edx
	leaq	4(%rbx,%rbp), %r9
	movq	%rbp, 376(%rsp)         # 8-byte Spill
	movl	%edx, 4(%rbx,%rbp)
	testl	%edx, %edx
	js	.LBB15_72
# BB#71:
	movslq	%edx, %rcx
	movzbl	QP_SCALE_CR(%rcx), %ecx
.LBB15_72:
	andb	$1, 15(%rsp)            # 1-byte Folded Spill
	movl	%ecx, (%r9)
	subl	5888(%r10), %edi
	addl	317080(%r8), %eax
	cmpl	%edi, %eax
	cmovll	%edi, %eax
	cmpl	$52, %eax
	cmovll	%eax, %esi
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	376(%rsp), %rbp         # 8-byte Reload
	leaq	8(%rdx,%rbp), %rdi
	movl	%esi, 8(%rdx,%rbp)
	testl	%esi, %esi
	js	.LBB15_74
# BB#73:
	movslq	%esi, %rax
	movzbl	QP_SCALE_CR(%rax), %eax
.LBB15_74:                              # %set_chroma_qp.exit
	movl	%eax, (%rdi)
	movl	5884(%r11), %edx
	addl	28(%r11), %edx
	movslq	%edx, %rsi
	imulq	$715827883, %rsi, %rdi  # imm = 0x2AAAAAAB
	movq	%rdi, %rdx
	shrq	$63, %rdx
	shrq	$32, %rdi
	addl	%edx, %edi
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	leal	(%rdi,%rdi), %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %esi
	movl	$InvLevelScale4x4Luma_Intra, %edx
	movl	$InvLevelScale4x4Luma_Inter, %edi
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	cmovneq	%rdx, %rdi
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	movl	$InvLevelScale8x8Luma_Intra, %edx
	movl	$InvLevelScale8x8Luma_Inter, %edi
	cmovneq	%rdx, %rdi
	movq	%rdi, 384(%rsp)         # 8-byte Spill
	cmpl	$0, 317044(%r8)
	je	.LBB15_76
# BB#75:                                # %.preheader1343
	movl	5888(%r11), %edx
	addl	%edx, %ecx
	movslq	%ecx, %rcx
	imulq	$715827883, %rcx, %rdi  # imm = 0x2AAAAAAB
	movq	%rdi, %rbp
	shrq	$63, %rbp
	shrq	$32, %rdi
	addl	%ebp, %edi
	movl	%edi, 216(%rsp)
	addl	%edi, %edi
	leal	(%rdi,%rdi,2), %edi
	subl	%edi, %ecx
	movl	%ecx, 248(%rsp)
	addl	%edx, %eax
	cltq
	imulq	$715827883, %rax, %rcx  # imm = 0x2AAAAAAB
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	movl	%ecx, 220(%rsp)
	addl	%ecx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	movl	%eax, 252(%rsp)
.LBB15_76:                              # %.preheader1342
	decq	296(%rsp)               # 8-byte Folded Spill
	movslq	%esi, %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	376(%rsp), %rcx         # 8-byte Reload
	leaq	396(%rax,%rcx), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_77:                              # %.preheader1341
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_78 Depth 2
                                        #       Child Loop BB15_110 Depth 3
                                        #         Child Loop BB15_111 Depth 4
                                        #           Child Loop BB15_113 Depth 5
                                        #           Child Loop BB15_120 Depth 5
                                        #       Child Loop BB15_80 Depth 3
                                        #         Child Loop BB15_81 Depth 4
                                        #           Child Loop BB15_86 Depth 5
                                        #           Child Loop BB15_91 Depth 5
                                        #           Child Loop BB15_97 Depth 5
                                        #           Child Loop BB15_102 Depth 5
	leaq	(,%rbx,4), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	%rbx, 312(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB15_78:                              #   Parent Loop BB15_77 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_110 Depth 3
                                        #         Child Loop BB15_111 Depth 4
                                        #           Child Loop BB15_113 Depth 5
                                        #           Child Loop BB15_120 Depth 5
                                        #       Child Loop BB15_80 Depth 3
                                        #         Child Loop BB15_81 Depth 4
                                        #           Child Loop BB15_86 Depth 5
                                        #           Child Loop BB15_91 Depth 5
                                        #           Child Loop BB15_97 Depth 5
                                        #           Child Loop BB15_102 Depth 5
	movl	%edx, %ecx
	sarl	%ecx
	addl	%ebx, %ecx
	movq	active_pps(%rip), %rax
	cmpl	$0, 12(%rax)
	movq	%rdx, 280(%rsp)         # 8-byte Spill
	je	.LBB15_79
# BB#108:                               #   in Loop: Header=BB15_78 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	cmpl	$0, (%rax)
	je	.LBB15_109
# BB#288:                               #   in Loop: Header=BB15_78 Depth=2
	movq	%r11, %rdi
	movl	%ecx, %edx
	callq	readLumaCoeff8x8_CABAC
	movq	280(%rsp), %rdx         # 8-byte Reload
	movq	16(%rsp), %r11          # 8-byte Reload
	jmp	.LBB15_129
	.p2align	4, 0x90
.LBB15_79:                              # %.preheader1337
                                        #   in Loop: Header=BB15_78 Depth=2
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	andl	64(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 272(%rsp)         # 4-byte Spill
	movq	56(%rsp), %rax          # 8-byte Reload
	leal	(%rdx,%rax), %ecx
	movl	$51, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movslq	%eax, %r14
	leaq	(,%rdx,4), %r12
	movq	%rbx, %r13
	.p2align	4, 0x90
.LBB15_80:                              # %.preheader1335
                                        #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB15_81 Depth 4
                                        #           Child Loop BB15_86 Depth 5
                                        #           Child Loop BB15_91 Depth 5
                                        #           Child Loop BB15_97 Depth 5
                                        #           Child Loop BB15_102 Depth 5
	movq	%r13, %rax
	subq	%rbx, %rax
	movq	304(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leaq	(,%r13,4), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	%rdx, %r15
	.p2align	4, 0x90
.LBB15_81:                              #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_80 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB15_86 Depth 5
                                        #           Child Loop BB15_91 Depth 5
                                        #           Child Loop BB15_97 Depth 5
                                        #           Child Loop BB15_102 Depth 5
	cmpl	$0, 272(%rsp)           # 4-byte Folded Reload
	je	.LBB15_105
# BB#82:                                #   in Loop: Header=BB15_81 Depth=4
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	(%rbx), %eax
	orl	$4, %eax
	xorl	%edx, %edx
	cmpl	$14, %eax
	sete	%dl
	addl	%edx, %edx
	movq	%r11, %rdi
	movl	%r15d, %ecx
	movl	%r13d, %r8d
	leaq	480(%rsp), %r9
	leaq	36(%rsp), %rax
	pushq	%rax
.Lcfi120:
	.cfi_adjust_cfa_offset 8
	leaq	424(%rsp), %rax
	pushq	%rax
.Lcfi121:
	.cfi_adjust_cfa_offset 8
	callq	readCoeff4x4_CAVLC
	addq	$16, %rsp
.Lcfi122:
	.cfi_adjust_cfa_offset -16
	movl	(%rbx), %eax
	orl	$4, %eax
	cmpl	$14, %eax
	movl	$0, %r10d
	movl	$-1, %eax
	cmovnel	%eax, %r10d
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	movslq	36(%rsp), %r9
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	je	.LBB15_83
# BB#94:                                #   in Loop: Header=BB15_81 Depth=4
	testl	%ecx, %ecx
	je	.LBB15_95
# BB#100:                               # %.preheader1323
                                        #   in Loop: Header=BB15_81 Depth=4
	testl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	jle	.LBB15_106
# BB#101:                               # %.lr.ph1420
                                        #   in Loop: Header=BB15_81 Depth=4
	movq	328(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_102:                             #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_80 Depth=3
                                        #         Parent Loop BB15_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	480(%rsp,%rsi,4), %edi
	testl	%edi, %edi
	je	.LBB15_104
# BB#103:                               #   in Loop: Header=BB15_102 Depth=5
	movl	416(%rsp,%rsi,4), %eax
	leal	(%r10,%rax), %edx
	movq	40(%rsp), %rbp          # 8-byte Reload
	orq	%r14, -92(%rbp)
	leal	1(%r10,%rax), %r10d
	leal	4(%rcx,%rdx,4), %eax
	cltq
	movq	192(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rbp
	movzbl	1(%rbp,%rax,2), %edx
	movq	56(%rsp), %rbx          # 8-byte Reload
	leal	(%rbx,%rdx), %edx
	movzbl	(%rbp,%rax,2), %eax
	leal	(%r12,%rax), %eax
	movslq	%edx, %rdx
	shlq	$6, %rdx
	addq	%r11, %rdx
	cltq
	movl	%edi, 1384(%rdx,%rax,4)
.LBB15_104:                             #   in Loop: Header=BB15_102 Depth=5
	incq	%rsi
	cmpq	%r9, %rsi
	jl	.LBB15_102
	jmp	.LBB15_106
	.p2align	4, 0x90
.LBB15_105:                             #   in Loop: Header=BB15_81 Depth=4
	movq	5560(%r11), %rax
	movl	4(%r11), %ecx
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%r15,8), %rax
	movl	$0, (%rax,%r13,4)
	jmp	.LBB15_106
	.p2align	4, 0x90
.LBB15_83:                              #   in Loop: Header=BB15_81 Depth=4
	testl	%ecx, %ecx
	je	.LBB15_84
# BB#89:                                # %.preheader1327
                                        #   in Loop: Header=BB15_81 Depth=4
	testl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	jle	.LBB15_106
# BB#90:                                # %.lr.ph1414
                                        #   in Loop: Header=BB15_81 Depth=4
	movq	328(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r15), %eax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_91:                              #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_80 Depth=3
                                        #         Parent Loop BB15_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	480(%rsp,%rdi,4), %ebx
	testl	%ebx, %ebx
	je	.LBB15_93
# BB#92:                                #   in Loop: Header=BB15_91 Depth=5
	movl	416(%rsp,%rdi,4), %esi
	leal	(%r10,%rsi), %ecx
	movq	40(%rsp), %rax          # 8-byte Reload
	orq	%r14, -92(%rax)
	movq	200(%rsp), %rax         # 8-byte Reload
	leal	4(%rax,%rcx,4), %ecx
	movslq	%ecx, %rcx
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	%r11, %r8
	movq	%r12, %r11
	movq	%r14, %r12
	movq	%r13, %r14
	movq	%rax, %r13
	movzbl	1(%r13,%rcx,2), %eax
	movq	208(%rsp), %rbp         # 8-byte Reload
	shlq	$8, %rbp
	addq	384(%rsp), %rbp         # 8-byte Folded Reload
	movq	%rax, %rdx
	shlq	$5, %rdx
	addq	%rbp, %rdx
	movzbl	(%r13,%rcx,2), %ebp
	movq	%r14, %r13
	movq	%r12, %r14
	movq	%r11, %r12
	movq	%r8, %r11
	imull	(%rdx,%rbp,4), %ebx
	movq	56(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rax), %eax
	movq	72(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebx
	addl	$32, %ebx
	sarl	$6, %ebx
	leal	(%r12,%rbp), %ecx
	cltq
	shlq	$6, %rax
	addq	%r11, %rax
	movslq	%ecx, %rcx
	movl	%ebx, 1384(%rax,%rcx,4)
	leal	1(%r10,%rsi), %r10d
.LBB15_93:                              #   in Loop: Header=BB15_91 Depth=5
	incq	%rdi
	cmpq	%r9, %rdi
	jl	.LBB15_91
	jmp	.LBB15_106
.LBB15_95:                              # %.preheader1325
                                        #   in Loop: Header=BB15_81 Depth=4
	testl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	jle	.LBB15_106
# BB#96:                                # %.lr.ph1417
                                        #   in Loop: Header=BB15_81 Depth=4
	movq	320(%rsp), %rax         # 8-byte Reload
	leal	(%r15,%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rsi
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_97:                              #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_80 Depth=3
                                        #         Parent Loop BB15_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	480(%rsp,%rcx,4), %edi
	testl	%edi, %edi
	je	.LBB15_99
# BB#98:                                #   in Loop: Header=BB15_97 Depth=5
	movl	416(%rsp,%rcx,4), %eax
	leal	1(%r10,%rax), %r10d
	movslq	%r10d, %rax
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rbp
	movzbl	1(%rbp,%rax,2), %edx
	movzbl	(%rbp,%rax,2), %eax
	movq	40(%rsp), %rbp          # 8-byte Reload
	orq	%rsi, -92(%rbp)
	leaq	(%r15,%r15,2), %rbp
	shlq	$8, %rbp
	addq	%r11, %rbp
	movq	%r13, %rbx
	shlq	$6, %rbx
	addq	%rbp, %rbx
	shlq	$4, %rdx
	addq	%rbx, %rdx
	movl	%edi, 2408(%rdx,%rax,4)
.LBB15_99:                              #   in Loop: Header=BB15_97 Depth=5
	incq	%rcx
	cmpq	%r9, %rcx
	jl	.LBB15_97
	jmp	.LBB15_106
.LBB15_84:                              # %.preheader1329
                                        #   in Loop: Header=BB15_81 Depth=4
	testl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	jle	.LBB15_106
# BB#85:                                # %.lr.ph1411
                                        #   in Loop: Header=BB15_81 Depth=4
	movq	320(%rsp), %rax         # 8-byte Reload
	leal	(%r15,%rax), %ecx
	movl	$1, %r8d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r8
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_86:                              #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_80 Depth=3
                                        #         Parent Loop BB15_81 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movl	480(%rsp,%rdi,4), %ebx
	testl	%ebx, %ebx
	je	.LBB15_88
# BB#87:                                #   in Loop: Header=BB15_86 Depth=5
	movl	416(%rsp,%rdi,4), %ecx
	leal	1(%r10,%rcx), %r10d
	movslq	%r10d, %rsi
	movq	176(%rsp), %rax         # 8-byte Reload
	movzbl	1(%rax,%rsi,2), %ecx
	movzbl	(%rax,%rsi,2), %esi
	movq	40(%rsp), %rax          # 8-byte Reload
	orq	%r8, -92(%rax)
	movq	208(%rsp), %rbp         # 8-byte Reload
	shlq	$6, %rbp
	addq	184(%rsp), %rbp         # 8-byte Folded Reload
	shlq	$4, %rcx
	addq	%rcx, %rbp
	imull	(%rbp,%rsi,4), %ebx
	leaq	(%r15,%r15,2), %rbp
	shlq	$8, %rbp
	addq	%r11, %rbp
	movq	%r13, %rdx
	shlq	$6, %rdx
	addq	%rbp, %rdx
	addq	%rcx, %rdx
	movq	72(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %ebx
	addl	$8, %ebx
	sarl	$4, %ebx
	movl	%ebx, 2408(%rdx,%rsi,4)
.LBB15_88:                              #   in Loop: Header=BB15_86 Depth=5
	incq	%rdi
	cmpq	%r9, %rdi
	jl	.LBB15_86
	.p2align	4, 0x90
.LBB15_106:                             # %.loopexit1324
                                        #   in Loop: Header=BB15_81 Depth=4
	movq	280(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, %r15
	leaq	1(%r15), %r15
	jle	.LBB15_81
# BB#107:                               #   in Loop: Header=BB15_80 Depth=3
	movq	312(%rsp), %rbx         # 8-byte Reload
	cmpq	%rbx, %r13
	leaq	1(%r13), %r13
	jle	.LBB15_80
	jmp	.LBB15_129
	.p2align	4, 0x90
.LBB15_109:                             # %.preheader1339
                                        #   in Loop: Header=BB15_78 Depth=2
	movl	$1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	andl	64(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, 200(%rsp)         # 4-byte Spill
	movq	%rbx, %rbp
	.p2align	4, 0x90
.LBB15_110:                             # %.preheader1336
                                        #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB15_111 Depth 4
                                        #           Child Loop BB15_113 Depth 5
                                        #           Child Loop BB15_120 Depth 5
	leaq	(,%rbp,4), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	%rdx, %rbx
	movq	168(%rsp), %r15         # 8-byte Reload
	.p2align	4, 0x90
.LBB15_111:                             #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_110 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB15_113 Depth 5
                                        #           Child Loop BB15_120 Depth 5
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	%eax, %ecx
	orl	$4, %ecx
	xorl	%r14d, %r14d
	cmpl	$14, %ecx
	sete	%dl
	cmpl	$0, 200(%rsp)           # 4-byte Folded Reload
	movl	%ebx, 5608(%r11)
	movl	%ebp, 5612(%r11)
	je	.LBB15_127
# BB#112:                               #   in Loop: Header=BB15_111 Depth=4
	movb	%dl, %r14b
	cmpl	$14, %ecx
	movl	$0, %r12d
	movl	$-1, %ecx
	cmovnel	%ecx, %r12d
	leal	-9(%rax), %ecx
	cmpl	$6, %ecx
	sbbb	%sil, %sil
	movb	$59, %dl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %dl
	andb	%sil, %dl
	andb	$1, %dl
	movzbl	%dl, %edx
	movl	%edx, 5616(%r11)
	movq	272(%rsp), %rcx         # 8-byte Reload
	leal	(%rbx,%rcx), %ecx
	movl	$1, %r13d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %r13
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	jne	.LBB15_120
	jmp	.LBB15_113
	.p2align	4, 0x90
.LBB15_126:                             # %._crit_edge1635
                                        #   in Loop: Header=BB15_120 Depth=5
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	5616(%r11), %edx
.LBB15_120:                             # %.preheader1331.preheader
                                        #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_110 Depth=3
                                        #         Parent Loop BB15_111 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%r14d
	orl	$4, %eax
	xorl	%ecx, %ecx
	cmpl	$14, %eax
	setne	%cl
	leal	1(,%rcx,4), %eax
	movl	%eax, 120(%rsp)
	xorl	%eax, %eax
	cmpl	$1, %r14d
	setne	%al
	testl	%edx, %edx
	leal	7(%rax,%rax), %ecx
	leal	12(%rax,%rax), %edx
	cmovnel	%ecx, %edx
	movl	%edx, 96(%rsp)
	movq	40(%r15), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movslq	assignSE2partition(%rcx,%rdx,4), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	je	.LBB15_122
# BB#121:                               #   in Loop: Header=BB15_120 Depth=5
	movq	(%rdx), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB15_123
.LBB15_122:                             #   in Loop: Header=BB15_120 Depth=5
	movq	$linfo_levrun_inter, 128(%rsp)
	jmp	.LBB15_124
	.p2align	4, 0x90
.LBB15_123:                             #   in Loop: Header=BB15_120 Depth=5
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_124:                             #   in Loop: Header=BB15_120 Depth=5
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rax,%rcx)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	testl	%eax, %eax
	je	.LBB15_127
# BB#125:                               #   in Loop: Header=BB15_120 Depth=5
	movl	104(%rsp), %ecx
	leal	1(%r12,%rcx), %r12d
	movslq	%r12d, %rcx
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rsi
	movzbl	1(%rsi,%rcx,2), %edx
	movzbl	(%rsi,%rcx,2), %ecx
	movq	40(%rsp), %rsi          # 8-byte Reload
	orq	%r13, -92(%rsi)
	leaq	(%rbx,%rbx,2), %rsi
	shlq	$8, %rsi
	addq	%r11, %rsi
	movq	%rbp, %rdi
	shlq	$6, %rdi
	addq	%rsi, %rdi
	shlq	$4, %rdx
	addq	%rdi, %rdx
	movl	%eax, 2408(%rdx,%rcx,4)
	cmpl	$16, %r14d
	jle	.LBB15_126
	jmp	.LBB15_127
	.p2align	4, 0x90
.LBB15_119:                             # %._crit_edge1632
                                        #   in Loop: Header=BB15_113 Depth=5
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	movl	5616(%r11), %edx
.LBB15_113:                             # %.preheader1333.preheader
                                        #   Parent Loop BB15_77 Depth=1
                                        #     Parent Loop BB15_78 Depth=2
                                        #       Parent Loop BB15_110 Depth=3
                                        #         Parent Loop BB15_111 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	incl	%r14d
	orl	$4, %eax
	xorl	%ecx, %ecx
	cmpl	$14, %eax
	setne	%cl
	leal	1(,%rcx,4), %eax
	movl	%eax, 120(%rsp)
	xorl	%eax, %eax
	cmpl	$1, %r14d
	setne	%al
	testl	%edx, %edx
	leal	7(%rax,%rax), %ecx
	leal	12(%rax,%rax), %edx
	cmovnel	%ecx, %edx
	movl	%edx, 96(%rsp)
	movq	40(%r15), %rax
	movq	80(%rsp), %rcx          # 8-byte Reload
	movslq	assignSE2partition(%rcx,%rdx,4), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rax,%rcx), %rdx
	movq	active_pps(%rip), %rsi
	cmpl	$0, 12(%rsi)
	je	.LBB15_115
# BB#114:                               #   in Loop: Header=BB15_113 Depth=5
	movq	(%rdx), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB15_116
.LBB15_115:                             #   in Loop: Header=BB15_113 Depth=5
	movq	$linfo_levrun_inter, 128(%rsp)
	jmp	.LBB15_117
	.p2align	4, 0x90
.LBB15_116:                             #   in Loop: Header=BB15_113 Depth=5
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_117:                             #   in Loop: Header=BB15_113 Depth=5
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rax,%rcx)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	testl	%eax, %eax
	je	.LBB15_127
# BB#118:                               #   in Loop: Header=BB15_113 Depth=5
	movl	104(%rsp), %ecx
	leal	1(%r12,%rcx), %r12d
	movslq	%r12d, %rdx
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %rsi
	movzbl	1(%rsi,%rdx,2), %ecx
	movzbl	(%rsi,%rdx,2), %edx
	movq	40(%rsp), %rsi          # 8-byte Reload
	orq	%r13, -92(%rsi)
	movq	208(%rsp), %rsi         # 8-byte Reload
	shlq	$6, %rsi
	addq	184(%rsp), %rsi         # 8-byte Folded Reload
	shlq	$4, %rcx
	addq	%rcx, %rsi
	imull	(%rsi,%rdx,4), %eax
	leaq	(%rbx,%rbx,2), %rsi
	shlq	$8, %rsi
	addq	%r11, %rsi
	movq	%rbp, %rdi
	shlq	$6, %rdi
	addq	%rsi, %rdi
	addq	%rcx, %rdi
	movq	72(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %eax
	addl	$8, %eax
	sarl	$4, %eax
	movl	%eax, 2408(%rdi,%rdx,4)
	cmpl	$16, %r14d
	jle	.LBB15_119
	.p2align	4, 0x90
.LBB15_127:                             # %.loopexit1332
                                        #   in Loop: Header=BB15_111 Depth=4
	movq	280(%rsp), %rdx         # 8-byte Reload
	cmpq	%rdx, %rbx
	leaq	1(%rbx), %rbx
	jle	.LBB15_111
# BB#128:                               #   in Loop: Header=BB15_110 Depth=3
	movq	312(%rsp), %rbx         # 8-byte Reload
	cmpq	%rbx, %rbp
	leaq	1(%rbp), %rbp
	jle	.LBB15_110
.LBB15_129:                             # %.loopexit1338
                                        #   in Loop: Header=BB15_78 Depth=2
	addq	$2, %rdx
	addq	$-2, 304(%rsp)          # 8-byte Folded Spill
	cmpq	$4, %rdx
	jl	.LBB15_78
# BB#130:                               #   in Loop: Header=BB15_77 Depth=1
	addq	$2, %rbx
	cmpq	$4, %rbx
	jl	.LBB15_77
# BB#131:
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB15_265
# BB#132:                               # %.preheader1322
	movl	5924(%r11), %ecx
	testl	%ecx, %ecx
	jle	.LBB15_135
# BB#133:                               # %.preheader1321.preheader
	leaq	2664(%r11), %rdx
	movl	$3, %esi
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB15_134:                             # %.preheader1321
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, 48(%rdx)
	movdqu	%xmm0, 32(%rdx)
	movdqu	%xmm0, 16(%rdx)
	movdqu	%xmm0, (%rdx)
	movdqu	%xmm0, 816(%rdx)
	movdqu	%xmm0, 800(%rdx)
	movdqu	%xmm0, 784(%rdx)
	movdqu	%xmm0, 768(%rdx)
	movdqu	%xmm0, 1584(%rdx)
	movdqu	%xmm0, 1568(%rdx)
	movdqu	%xmm0, 1552(%rdx)
	movdqu	%xmm0, 1536(%rdx)
	movdqu	%xmm0, 2352(%rdx)
	movdqu	%xmm0, 2336(%rdx)
	movdqu	%xmm0, 2320(%rdx)
	movdqu	%xmm0, 2304(%rdx)
	movslq	5924(%r11), %rcx
	leaq	3(%rcx), %rdi
	incq	%rsi
	addq	$64, %rdx
	cmpq	%rdi, %rsi
	jl	.LBB15_134
.LBB15_135:                             # %._crit_edge1402
	cmpl	$16, 64(%rsp)           # 4-byte Folded Reload
	jl	.LBB15_252
# BB#136:                               # %.preheader1320
	leaq	5480(%r11), %rcx
	movq	%rcx, 392(%rsp)         # 8-byte Spill
	cmpl	$0, 240(%rsp)           # 4-byte Folded Reload
	setne	%cl
	orb	52(%rsp), %cl           # 1-byte Folded Reload
	movb	%cl, 71(%rsp)           # 1-byte Spill
	leaq	5484(%r11), %rcx
	movq	%rcx, 568(%rsp)         # 8-byte Spill
	leaq	5488(%r11), %rcx
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	leaq	5492(%r11), %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	leaq	2856(%r11), %r13
	leaq	2664(%r11), %rcx
	movq	%rcx, 408(%rsp)         # 8-byte Spill
	movl	$4, %ecx
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	xorl	%r9d, %r9d
	movq	%r13, 400(%rsp)         # 8-byte Spill
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB15_137
.LBB15_215:                             # %.critedge.loopexit
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r14d, 224(%rsp)
	movl	%ebp, 228(%rsp)
	movl	%r12d, 232(%rsp)
	movl	%edx, 236(%rsp)
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB15_248
.LBB15_221:                             #   in Loop: Header=BB15_137 Depth=1
	movl	$6, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r11, %rdi
	leaq	480(%rsp), %r9
	leaq	36(%rsp), %rax
	pushq	%rax
.Lcfi123:
	.cfi_adjust_cfa_offset 8
	leaq	424(%rsp), %rax
	pushq	%rax
.Lcfi124:
	.cfi_adjust_cfa_offset 8
	callq	readCoeff4x4_CAVLC
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	addq	$16, %rsp
.Lcfi125:
	.cfi_adjust_cfa_offset -16
	movslq	36(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB15_226
# BB#222:                               # %.lr.ph1388
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r9d, %ecx
	shlb	$3, %cl
	movl	$4294901760, %r8d       # imm = 0xFFFF0000
	shlq	%cl, %r8
	movl	$-1, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_223:                             #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	480(%rsp,%rsi,4), %edi
	testl	%edi, %edi
	je	.LBB15_225
# BB#224:                               #   in Loop: Header=BB15_223 Depth=2
	movq	40(%rsp), %rbp          # 8-byte Reload
	orq	%r8, -92(%rbp)
	movl	416(%rsp,%rsi,4), %ebp
	leal	1(%rcx,%rbp), %ecx
	movslq	%ecx, %rbp
	movzbl	SNGL_SCAN(%rbp,%rbp), %ebx
	movzbl	SNGL_SCAN+1(%rbp,%rbp), %ebp
	movq	192(%rsp), %rdx         # 8-byte Reload
	leal	(%rdx,%rbp), %ebp
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$8, %rbx
	addq	%r11, %rbx
	movslq	%ebp, %rbp
	shlq	$6, %rbp
	movl	%edi, 2408(%rbp,%rbx)
.LBB15_225:                             #   in Loop: Header=BB15_223 Depth=2
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB15_223
.LBB15_226:                             # %.preheader1309
                                        #   in Loop: Header=BB15_137 Depth=1
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	jne	.LBB15_249
# BB#227:                               # %.preheader1307
                                        #   in Loop: Header=BB15_137 Depth=1
	leaq	(%r9,%r9), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	leaq	7(%rax), %r12
	movq	240(%rsp), %rax         # 8-byte Reload
	movq	%rax, %rsi
	movq	%rsi, %rdx
	shlq	$6, %rdx
	movl	2408(%r11,%rdx), %r8d
	movl	3176(%r11,%rdx), %r10d
	movl	3944(%r11,%rdx), %r9d
	movl	4712(%r11,%rdx), %r15d
	leal	(%r9,%r8), %edi
	movl	%r8d, %eax
	subl	%r9d, %eax
	movl	%r10d, %ecx
	subl	%r15d, %ecx
	leal	(%r15,%r10), %ebp
	leal	(%rbp,%rdi), %ebx
	movl	%ebx, 2408(%r11,%rdx)
	movl	%edi, %ebx
	subl	%ebp, %ebx
	movl	%ebx, 4712(%r11,%rdx)
	leal	(%rcx,%rax), %ebx
	movl	%ebx, 3176(%r11,%rdx)
	movl	%eax, %ebx
	subl	%ecx, %ebx
	movl	%ebx, 3944(%r11,%rdx)
	movq	%r12, 200(%rsp)         # 8-byte Spill
	cmpq	%r12, %rsi
	jge	.LBB15_229
# BB#228:                               # %.preheader1307.1
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	240(%rsp), %rbx         # 8-byte Reload
	movq	%rbx, %rax
	orq	$1, %rax
	shlq	$6, %rax
	movl	2408(%r11,%rax), %ecx
	movl	3176(%r11,%rax), %edx
	movl	3944(%r11,%rax), %esi
	movl	4712(%r11,%rax), %edi
	leal	(%rsi,%rcx), %ebp
	subl	%esi, %ecx
	movl	%edx, %esi
	subl	%edi, %esi
	addl	%edx, %edi
	leal	(%rdi,%rbp), %edx
	movl	%edx, 2408(%r11,%rax)
	subl	%edi, %ebp
	movl	%ebp, 4712(%r11,%rax)
	leal	(%rsi,%rcx), %edx
	movl	%edx, 3176(%r11,%rax)
	subl	%esi, %ecx
	movl	%ecx, 3944(%r11,%rax)
	movl	2472(%r11,%rax), %ecx
	movl	3240(%r11,%rax), %edx
	movl	4008(%r11,%rax), %esi
	movl	4776(%r11,%rax), %edi
	leal	(%rsi,%rcx), %ebp
	subl	%esi, %ecx
	movl	%edx, %esi
	subl	%edi, %esi
	addl	%edx, %edi
	leal	(%rdi,%rbp), %edx
	movl	%edx, 2472(%r11,%rax)
	subl	%edi, %ebp
	movl	%ebp, 4776(%r11,%rax)
	leal	(%rsi,%rcx), %edx
	movl	%edx, 3240(%r11,%rax)
	subl	%esi, %ecx
	movl	%ecx, 4008(%r11,%rax)
	movq	%rbx, %rdx
	orq	$3, %rdx
	shlq	$6, %rdx
	movl	2408(%r11,%rdx), %r8d
	movl	3176(%r11,%rdx), %r10d
	movl	3944(%r11,%rdx), %r9d
	movl	4712(%r11,%rdx), %r15d
	leal	(%r9,%r8), %edi
	movl	%r8d, %eax
	subl	%r9d, %eax
	movl	%r10d, %ecx
	subl	%r15d, %ecx
	leal	(%r15,%r10), %ebp
	leal	(%rbp,%rdi), %ebx
	movl	%ebx, 2408(%r11,%rdx)
	movl	%edi, %ebx
	subl	%ebp, %ebx
	movl	%ebx, 4712(%r11,%rdx)
	leal	(%rcx,%rax), %ebx
	movl	%ebx, 3176(%r11,%rdx)
	movl	%eax, %ebx
	subl	%ecx, %ebx
	movl	%ebx, 3944(%r11,%rdx)
.LBB15_229:                             # %.preheader1306.lr.ph
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r8d, 256(%rsp)
	movl	%r10d, 260(%rsp)
	movl	%r9d, 264(%rsp)
	movl	%r15d, 268(%rsp)
	movl	%edi, 224(%rsp)
	movl	%eax, 228(%rsp)
	movl	%ecx, 232(%rsp)
	movl	%ebp, 236(%rsp)
	movslq	%r14d, %rax
	movl	216(%rsp,%rax,4), %edi
	movslq	248(%rsp,%rax,4), %rdx
	leaq	(%rax,%rax,2), %rbx
	shlq	$7, %rbx
	shlq	$6, %rdx
	leaq	InvLevelScale4x4Chroma_Intra(%rbx,%rdx), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movl	$3, %ecx
	subl	%edi, %ecx
	movl	$1, %r15d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r15d
	movl	$4, %eax
	subl	%edi, %eax
	leaq	InvLevelScale4x4Chroma_Inter(%rbx,%rdx), %rbx
	movq	%rdi, 280(%rsp)         # 8-byte Spill
	leal	-4(%rdi), %edx
	movq	184(%rsp), %r12         # 8-byte Reload
	leaq	5(%r12), %rcx
	movq	%rcx, 384(%rsp)         # 8-byte Spill
	addq	$6, %r12
	movq	%r12, 184(%rsp)         # 8-byte Spill
	movq	408(%rsp), %rdi         # 8-byte Reload
	movq	%r13, 288(%rsp)         # 8-byte Spill
	movq	%r13, %r14
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB15_230:                             # %.preheader1306
                                        #   Parent Loop BB15_137 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_231 Depth 3
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$8, %rcx
	addq	%r11, %rcx
	movq	192(%rsp), %rbp         # 8-byte Reload
	shlq	$6, %rbp
	movl	2408(%rbp,%rcx), %r8d
	movq	384(%rsp), %rbp         # 8-byte Reload
	shlq	$6, %rbp
	movl	2408(%rbp,%rcx), %r9d
	movq	184(%rsp), %rbp         # 8-byte Reload
	shlq	$6, %rbp
	movl	2408(%rbp,%rcx), %r11d
	movq	200(%rsp), %rbp         # 8-byte Reload
	shlq	$6, %rbp
	movl	2408(%rbp,%rcx), %ebp
	leal	(%r11,%r8), %r10d
	movl	%r10d, 224(%rsp)
	movq	%r8, 328(%rsp)          # 8-byte Spill
	movl	%r8d, %ecx
	movq	%r11, 312(%rsp)         # 8-byte Spill
	subl	%r11d, %ecx
	movl	%ecx, 228(%rsp)
	movl	%r9d, %ecx
	subl	%ebp, %ecx
	movl	%ecx, 232(%rsp)
	movq	%r9, 320(%rsp)          # 8-byte Spill
	movq	%rbp, 304(%rsp)         # 8-byte Spill
	leal	(%rbp,%r9), %ecx
	movl	%ecx, 236(%rsp)
	movq	%rdi, 72(%rsp)          # 8-byte Spill
	movq	%rdi, %r11
	leaq	236(%rsp), %r8
	movq	%r14, %rbp
	xorl	%r12d, %r12d
	movq	280(%rsp), %rdi         # 8-byte Reload
	movq	272(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB15_231
	.p2align	4, 0x90
.LBB15_245:                             # %._crit_edge1655
                                        #   in Loop: Header=BB15_231 Depth=3
	movl	228(%rsp,%r12,4), %r10d
	incq	%r12
	addq	$-64, %r14
	addq	$-4, %r8
	addq	$64, %r11
.LBB15_231:                             #   Parent Loop BB15_137 Depth=1
                                        #     Parent Loop BB15_230 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r8), %r13d
	leal	(%r13,%r10), %r9d
	cmpl	$3, %edi
	jg	.LBB15_240
# BB#232:                               #   in Loop: Header=BB15_231 Depth=3
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB15_239
# BB#233:                               #   in Loop: Header=BB15_231 Depth=3
	imull	(%rsi), %r9d
	addl	%r15d, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r9d
	addl	$2, %r9d
	sarl	$2, %r9d
	movl	%r9d, (%r11)
	subl	%r13d, %r10d
	imull	(%rsi), %r10d
	jmp	.LBB15_234
	.p2align	4, 0x90
.LBB15_240:                             #   in Loop: Header=BB15_231 Depth=3
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	je	.LBB15_242
# BB#241:                               #   in Loop: Header=BB15_231 Depth=3
	imull	(%rsi), %r9d
	movl	%edx, %ecx
	shll	%cl, %r9d
	addl	$2, %r9d
	sarl	$2, %r9d
	movl	%r9d, (%r11)
	subl	%r13d, %r10d
	imull	(%rsi), %r10d
	jmp	.LBB15_243
	.p2align	4, 0x90
.LBB15_239:                             #   in Loop: Header=BB15_231 Depth=3
	imull	(%rbx), %r9d
	addl	%r15d, %r9d
	movl	%eax, %ecx
	sarl	%cl, %r9d
	addl	$2, %r9d
	sarl	$2, %r9d
	movl	%r9d, (%r11)
	subl	%r13d, %r10d
	imull	(%rbx), %r10d
.LBB15_234:                             #   in Loop: Header=BB15_231 Depth=3
	addl	%r15d, %r10d
	movl	%eax, %ecx
	sarl	%cl, %r10d
	jmp	.LBB15_244
	.p2align	4, 0x90
.LBB15_242:                             #   in Loop: Header=BB15_231 Depth=3
	imull	(%rbx), %r9d
	movl	%edx, %ecx
	shll	%cl, %r9d
	addl	$2, %r9d
	sarl	$2, %r9d
	movl	%r9d, (%r11)
	subl	%r13d, %r10d
	imull	(%rbx), %r10d
.LBB15_243:                             #   in Loop: Header=BB15_231 Depth=3
	movl	%edx, %ecx
	shll	%cl, %r10d
.LBB15_244:                             #   in Loop: Header=BB15_231 Depth=3
	addl	$2, %r10d
	sarl	$2, %r10d
	movl	%r10d, (%r14)
	cmpq	$1, %r12
	jne	.LBB15_245
# BB#246:                               # %.critedge39
                                        #   in Loop: Header=BB15_230 Depth=2
	movq	%rbp, %r14
	addq	$768, %r14              # imm = 0x300
	movq	72(%rsp), %rdi          # 8-byte Reload
	addq	$768, %rdi              # imm = 0x300
	movq	208(%rsp), %rcx         # 8-byte Reload
	cmpq	$3, %rcx
	leaq	1(%rcx), %rcx
	movq	16(%rsp), %r11          # 8-byte Reload
	jl	.LBB15_230
# BB#247:                               # %.critedge40.loopexit
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	328(%rsp), %rax         # 8-byte Reload
	movl	%eax, 256(%rsp)
	movq	320(%rsp), %rax         # 8-byte Reload
	movl	%eax, 260(%rsp)
	movq	312(%rsp), %rax         # 8-byte Reload
	movl	%eax, 264(%rsp)
	movq	304(%rsp), %rax         # 8-byte Reload
	movl	%eax, 268(%rsp)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	288(%rsp), %r13         # 8-byte Reload
.LBB15_248:                             # %.critedge40
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	56(%rsp), %r9           # 8-byte Reload
	jmp	.LBB15_249
.LBB15_148:                             #   in Loop: Header=BB15_137 Depth=1
	movl	480(%rsp), %esi
	testl	%esi, %esi
	je	.LBB15_150
# BB#149:                               #   in Loop: Header=BB15_137 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	orq	%rcx, -92(%rdx)
	movslq	416(%rsp), %rdx
	movl	%esi, 5480(%r11,%rdx,4)
.LBB15_150:                             # %.prol.loopexit
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	$1, %esi
	cmpl	$1, %eax
	je	.LBB15_157
	.p2align	4, 0x90
.LBB15_152:                             #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	480(%rsp,%rsi,4), %edi
	testl	%edi, %edi
	je	.LBB15_154
# BB#153:                               #   in Loop: Header=BB15_152 Depth=2
	movq	40(%rsp), %rbp          # 8-byte Reload
	orq	%rcx, -92(%rbp)
	movl	416(%rsp,%rsi,4), %ebp
	leal	1(%rdx,%rbp), %edx
	movslq	%edx, %rbp
	movl	%edi, 5480(%r11,%rbp,4)
.LBB15_154:                             #   in Loop: Header=BB15_152 Depth=2
	movl	484(%rsp,%rsi,4), %edi
	testl	%edi, %edi
	je	.LBB15_156
# BB#155:                               #   in Loop: Header=BB15_152 Depth=2
	movq	40(%rsp), %rbp          # 8-byte Reload
	orq	%rcx, -92(%rbp)
	movl	420(%rsp,%rsi,4), %ebp
	leal	1(%rdx,%rbp), %edx
	movslq	%edx, %rbp
	movl	%edi, 5480(%r11,%rbp,4)
.LBB15_156:                             #   in Loop: Header=BB15_152 Depth=2
	addq	$2, %rsi
	cmpq	%rax, %rsi
	jl	.LBB15_152
	.p2align	4, 0x90
.LBB15_157:                             # %.loopexit1312
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	392(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	cmpb	$0, 71(%rsp)            # 1-byte Folded Reload
	je	.LBB15_163
# BB#158:                               #   in Loop: Header=BB15_137 Depth=1
	movq	568(%rsp), %rsi         # 8-byte Reload
	movq	560(%rsp), %rcx         # 8-byte Reload
	movq	552(%rsp), %rdx         # 8-byte Reload
	movq	%r12, %r13
	jmp	.LBB15_193
.LBB15_163:                             #   in Loop: Header=BB15_137 Depth=1
	movl	5484(%r11), %ecx
	movl	5488(%r11), %edx
	leal	(%rcx,%rax), %esi
	leal	(%rsi,%rdx), %edi
	movl	5492(%r11), %ebp
	addl	%ebp, %edi
	movl	%edi, 144(%rsp)
	subl	%ecx, %eax
	leal	(%rax,%rdx), %ecx
	subl	%ebp, %ecx
	movl	%ecx, 148(%rsp)
	subl	%edx, %esi
	subl	%ebp, %esi
	movl	%esi, 152(%rsp)
	subl	%edx, %eax
	addl	%ebp, %eax
	movl	%eax, 156(%rsp)
	movslq	5928(%r11), %rdx
	testq	%rdx, %rdx
	movq	%r12, %r13
	jle	.LBB15_192
# BB#164:                               # %.lr.ph1382
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	216(%rsp,%r15,4), %ecx
	movq	72(%rsp), %rax          # 8-byte Reload
	shlq	$6, %rax
	movl	(%r14,%rax), %esi
	imull	%esi, %edi
	cmpl	$5, %ecx
	jge	.LBB15_165
# BB#178:                               # %.lr.ph1382.split.us.preheader
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	$5, %eax
	subl	%ecx, %eax
	movl	%eax, %ecx
	sarl	%cl, %edi
	movl	%edi, 144(%rsp)
	cmpl	$2, %edx
	jl	.LBB15_192
# BB#179:                               # %.lr.ph1382.split.us..lr.ph1382.split.us_crit_edge.lr.ph
                                        #   in Loop: Header=BB15_137 Depth=1
	leaq	-1(%rdx), %rcx
	cmpq	$8, %rcx
	jae	.LBB15_181
# BB#180:                               #   in Loop: Header=BB15_137 Depth=1
	movl	$1, %edi
	jmp	.LBB15_191
.LBB15_165:                             # %.lr.ph1382.split.preheader
                                        #   in Loop: Header=BB15_137 Depth=1
	addl	$-5, %ecx
	shll	%cl, %edi
	movl	%edi, 144(%rsp)
	cmpl	$2, %edx
	jl	.LBB15_192
# BB#166:                               # %.lr.ph1382.split..lr.ph1382.split_crit_edge.lr.ph
                                        #   in Loop: Header=BB15_137 Depth=1
	leaq	-1(%rdx), %rdi
	cmpq	$7, %rdi
	jbe	.LBB15_167
# BB#169:                               # %min.iters.checked1753
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	%rdi, %rax
	andq	$-8, %rax
	movq	%rdi, %r8
	andq	$-8, %r8
	je	.LBB15_167
# BB#170:                               # %vector.ph1757
                                        #   in Loop: Header=BB15_137 Depth=1
	movd	%esi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%ecx, %xmm1
	leaq	-8(%r8), %rbp
	movq	%rbp, %r9
	shrq	$3, %r9
	btl	$3, %ebp
	jb	.LBB15_171
# BB#172:                               # %vector.body1749.prol
                                        #   in Loop: Header=BB15_137 Depth=1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movdqu	148(%rsp), %xmm3
	movdqu	164(%rsp), %xmm4
	movdqa	%xmm0, %xmm5
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pshufd	$245, %xmm0, %xmm6      # xmm6 = xmm0[1,1,3,3]
	pmuludq	%xmm6, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movdqa	%xmm0, %xmm3
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	pslld	%xmm2, %xmm5
	pslld	%xmm2, %xmm3
	movdqu	%xmm5, 148(%rsp)
	movdqu	%xmm3, 164(%rsp)
	movl	$8, %r10d
	testq	%r9, %r9
	jne	.LBB15_174
	jmp	.LBB15_176
.LBB15_167:                             #   in Loop: Header=BB15_137 Depth=1
	movl	$1, %eax
	.p2align	4, 0x90
.LBB15_168:                             # %.lr.ph1382.split..lr.ph1382.split_crit_edge
                                        #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	144(%rsp,%rax,4), %edi
	imull	%esi, %edi
	shll	%cl, %edi
	movl	%edi, 144(%rsp,%rax,4)
	incq	%rax
	cmpq	%rdx, %rax
	jl	.LBB15_168
	jmp	.LBB15_192
.LBB15_181:                             # %min.iters.checked
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	%rcx, %rdi
	andq	$-8, %rdi
	movq	%rcx, %r8
	andq	$-8, %r8
	je	.LBB15_182
# BB#183:                               # %vector.ph
                                        #   in Loop: Header=BB15_137 Depth=1
	movd	%esi, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movd	%eax, %xmm1
	leaq	-8(%r8), %rbp
	movq	%rbp, %r9
	shrq	$3, %r9
	btl	$3, %ebp
	jb	.LBB15_184
# BB#185:                               # %vector.body.prol
                                        #   in Loop: Header=BB15_137 Depth=1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movdqu	148(%rsp), %xmm3
	movdqu	164(%rsp), %xmm4
	movdqa	%xmm0, %xmm5
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	pshufd	$245, %xmm0, %xmm6      # xmm6 = xmm0[1,1,3,3]
	pmuludq	%xmm6, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpckldq	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movdqa	%xmm0, %xmm3
	pmuludq	%xmm4, %xmm3
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm6, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	psrad	%xmm2, %xmm5
	psrad	%xmm2, %xmm3
	movdqu	%xmm5, 148(%rsp)
	movdqu	%xmm3, 164(%rsp)
	movl	$8, %r10d
	testq	%r9, %r9
	jne	.LBB15_187
	jmp	.LBB15_189
.LBB15_182:                             #   in Loop: Header=BB15_137 Depth=1
	movl	$1, %edi
	jmp	.LBB15_191
.LBB15_171:                             #   in Loop: Header=BB15_137 Depth=1
	xorl	%r10d, %r10d
	testq	%r9, %r9
	je	.LBB15_176
.LBB15_174:                             # %vector.ph1757.new
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	%r8, %rbp
	subq	%r10, %rbp
	leaq	148(%rsp), %rbx
	leaq	48(%rbx,%r10,4), %rbx
	.p2align	4, 0x90
.LBB15_175:                             # %vector.body1749
                                        #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movdqu	-48(%rbx), %xmm3
	movdqu	-32(%rbx), %xmm4
	movdqa	%xmm0, %xmm5
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pshufd	$245, %xmm0, %xmm3      # xmm3 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	movdqa	%xmm0, %xmm6
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	pslld	%xmm2, %xmm5
	pslld	%xmm2, %xmm6
	movdqu	%xmm5, -48(%rbx)
	movdqu	%xmm6, -32(%rbx)
	movdqu	-16(%rbx), %xmm4
	movdqu	(%rbx), %xmm5
	movdqa	%xmm0, %xmm6
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movdqa	%xmm0, %xmm4
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	pslld	%xmm2, %xmm6
	pslld	%xmm2, %xmm4
	movdqu	%xmm6, -16(%rbx)
	movdqu	%xmm4, (%rbx)
	addq	$64, %rbx
	addq	$-16, %rbp
	jne	.LBB15_175
.LBB15_176:                             # %middle.block1750
                                        #   in Loop: Header=BB15_137 Depth=1
	cmpq	%r8, %rdi
	movq	56(%rsp), %r9           # 8-byte Reload
	je	.LBB15_192
# BB#177:                               #   in Loop: Header=BB15_137 Depth=1
	orq	$1, %rax
	jmp	.LBB15_168
.LBB15_184:                             #   in Loop: Header=BB15_137 Depth=1
	xorl	%r10d, %r10d
	testq	%r9, %r9
	je	.LBB15_189
.LBB15_187:                             # %vector.ph.new
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	%r8, %rbp
	subq	%r10, %rbp
	leaq	148(%rsp), %rbx
	leaq	48(%rbx,%r10,4), %rbx
	.p2align	4, 0x90
.LBB15_188:                             # %vector.body
                                        #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movdqu	-48(%rbx), %xmm3
	movdqu	-32(%rbx), %xmm4
	movdqa	%xmm0, %xmm5
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm5      # xmm5 = xmm5[0,2,2,3]
	pshufd	$245, %xmm3, %xmm6      # xmm6 = xmm3[1,1,3,3]
	pshufd	$245, %xmm0, %xmm3      # xmm3 = xmm0[1,1,3,3]
	pmuludq	%xmm3, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	punpckldq	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	movdqa	%xmm0, %xmm6
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	psrad	%xmm2, %xmm5
	psrad	%xmm2, %xmm6
	movdqu	%xmm5, -48(%rbx)
	movdqu	%xmm6, -32(%rbx)
	movdqu	-16(%rbx), %xmm4
	movdqu	(%rbx), %xmm5
	movdqa	%xmm0, %xmm6
	pmuludq	%xmm4, %xmm6
	pshufd	$232, %xmm6, %xmm6      # xmm6 = xmm6[0,2,2,3]
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movdqa	%xmm0, %xmm4
	pmuludq	%xmm5, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	pshufd	$245, %xmm5, %xmm5      # xmm5 = xmm5[1,1,3,3]
	pmuludq	%xmm3, %xmm5
	pshufd	$232, %xmm5, %xmm3      # xmm3 = xmm5[0,2,2,3]
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	psrad	%xmm2, %xmm6
	psrad	%xmm2, %xmm4
	movdqu	%xmm6, -16(%rbx)
	movdqu	%xmm4, (%rbx)
	addq	$64, %rbx
	addq	$-16, %rbp
	jne	.LBB15_188
.LBB15_189:                             # %middle.block
                                        #   in Loop: Header=BB15_137 Depth=1
	cmpq	%r8, %rcx
	movq	56(%rsp), %r9           # 8-byte Reload
	je	.LBB15_192
# BB#190:                               #   in Loop: Header=BB15_137 Depth=1
	orq	$1, %rdi
	.p2align	4, 0x90
.LBB15_191:                             # %.lr.ph1382.split.us..lr.ph1382.split.us_crit_edge
                                        #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	144(%rsp,%rdi,4), %ebp
	imull	%esi, %ebp
	movl	%eax, %ecx
	sarl	%cl, %ebp
	movl	%ebp, 144(%rsp,%rdi,4)
	incq	%rdi
	cmpq	%rdx, %rdi
	jl	.LBB15_191
.LBB15_192:                             # %._crit_edge
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	144(%rsp), %eax
	leaq	148(%rsp), %rsi
	leaq	152(%rsp), %rcx
	leaq	156(%rsp), %rdx
.LBB15_193:                             #   in Loop: Header=BB15_137 Depth=1
	leaq	(%r9,%r9,2), %rdi
	shlq	$8, %rdi
	movl	%eax, 2664(%r11,%rdi)
	movl	(%rsi), %eax
	movq	%r9, %rsi
	orq	$1, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$8, %rsi
	movl	%eax, 2664(%r11,%rsi)
	movl	(%rcx), %eax
	movl	%eax, 2728(%r11,%rdi)
	movl	(%rdx), %eax
	movl	%eax, 2728(%r11,%rsi)
	movq	24(%rsp), %rcx          # 8-byte Reload
.LBB15_249:                             # %.critedge40
                                        #   in Loop: Header=BB15_137 Depth=1
	addq	$2, %r9
	cmpq	$2, %r9
	jg	.LBB15_251
# BB#250:                               # %.critedge40._crit_edge
                                        #   in Loop: Header=BB15_137 Depth=1
	addq	$4, 240(%rsp)           # 8-byte Folded Spill
	movq	dec_picture(%rip), %rax
	addq	$1536, 400(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x600
	addq	$256, %r13              # imm = 0x100
	addq	$256, 408(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x100
.LBB15_137:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_218 Depth 2
                                        #     Child Loop BB15_223 Depth 2
                                        #     Child Loop BB15_230 Depth 2
                                        #       Child Loop BB15_231 Depth 3
                                        #     Child Loop BB15_142 Depth 2
                                        #     Child Loop BB15_152 Depth 2
                                        #     Child Loop BB15_175 Depth 2
                                        #     Child Loop BB15_168 Depth 2
                                        #     Child Loop BB15_188 Depth 2
                                        #     Child Loop BB15_191 Depth 2
                                        #     Child Loop BB15_196 Depth 2
                                        #     Child Loop BB15_201 Depth 2
                                        #     Child Loop BB15_211 Depth 2
	movl	%r9d, %r14d
	sarl	%r14d
	movl	317044(%rax), %eax
	cmpl	$2, %eax
	movq	%r9, 56(%rsp)           # 8-byte Spill
	je	.LBB15_194
# BB#138:                               #   in Loop: Header=BB15_137 Depth=1
	cmpl	$1, %eax
	jne	.LBB15_216
# BB#139:                               #   in Loop: Header=BB15_137 Depth=1
	movslq	%r14d, %r15
	movq	%r15, %rax
	shlq	$7, %rax
	leaq	InvLevelScale4x4Chroma_Inter(%rax,%rax,2), %rcx
	leaq	InvLevelScale4x4Chroma_Intra(%rax,%rax,2), %r14
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	cmoveq	%rcx, %r14
	movslq	248(%rsp,%r15,4), %rcx
	movq	392(%rsp), %rax         # 8-byte Reload
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	active_pps(%rip), %rax
	movl	12(%rax), %esi
	testl	%esi, %esi
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r13, %r12
	je	.LBB15_145
# BB#140:                               # %.preheader1313
                                        #   in Loop: Header=BB15_137 Depth=1
	cmpl	$0, 5928(%r11)
	js	.LBB15_157
# BB#141:                               # %.lr.ph1377
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r9d, %ecx
	addb	%cl, %cl
	movl	$983040, %eax           # imm = 0xF0000
	shll	%cl, %eax
	movslq	%eax, %r13
	movl	$-1, %ebx
	xorl	%ebp, %ebp
	jmp	.LBB15_142
	.p2align	4, 0x90
.LBB15_162:                             # %._crit_edge1643
                                        #   in Loop: Header=BB15_142 Depth=2
	incl	%ebp
	movq	active_pps(%rip), %rax
	movl	12(%rax), %esi
.LBB15_142:                             #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$6, 120(%rsp)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%al
	movb	$59, %dl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %dl
	xorl	%ecx, %ecx
	andb	%al, %dl
	sete	%cl
	leal	8(%rcx,%rcx,4), %eax
	movl	%eax, 96(%rsp)
	movzbl	%dl, %ecx
	movl	%ecx, 5616(%r11)
	movl	%r9d, 5620(%r11)
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	40(%rcx), %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	movslq	assignSE2partition(%rdx,%rax,4), %rax
	imulq	$56, %rax, %rax
	leaq	(%rcx,%rax), %rdx
	testl	%esi, %esi
	je	.LBB15_144
# BB#143:                               #   in Loop: Header=BB15_142 Depth=2
	movq	(%rdx), %rsi
	cmpl	$0, 24(%rsi)
	je	.LBB15_159
.LBB15_144:                             #   in Loop: Header=BB15_142 Depth=2
	movq	$linfo_levrun_c2x2, 128(%rsp)
	jmp	.LBB15_160
	.p2align	4, 0x90
.LBB15_159:                             #   in Loop: Header=BB15_142 Depth=2
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_160:                             #   in Loop: Header=BB15_142 Depth=2
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rcx,%rax)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	testl	%eax, %eax
	movq	56(%rsp), %r9           # 8-byte Reload
	je	.LBB15_157
# BB#161:                               #   in Loop: Header=BB15_142 Depth=2
	movl	104(%rsp), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	orq	%r13, -92(%rdx)
	leal	1(%rbx,%rcx), %ebx
	movslq	%ebx, %rcx
	movl	%eax, 5480(%r11,%rcx,4)
	cmpl	5928(%r11), %ebp
	jl	.LBB15_162
	jmp	.LBB15_157
	.p2align	4, 0x90
.LBB15_194:                             #   in Loop: Header=BB15_137 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 352(%rsp)
	movdqa	%xmm0, 336(%rsp)
	movdqa	%xmm0, 592(%rsp)
	movdqa	%xmm0, 576(%rsp)
	movslq	%r14d, %r12
	movq	376(%rsp), %rax         # 8-byte Reload
	leaq	(%rcx,%rax), %rax
	movl	4(%rax,%r12,4), %r15d
	movl	5888(%r11), %ecx
	movq	active_pps(%rip), %rax
	movl	12(%rax), %esi
	testl	%esi, %esi
	movq	%r13, 288(%rsp)         # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	je	.LBB15_199
# BB#195:                               # %.preheader1318
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r9d, %ecx
	shlb	$2, %cl
	movl	$16711680, %r13d        # imm = 0xFF0000
	shlq	%cl, %r13
	movl	$-1, %ebx
	movl	$1, %r14d
	jmp	.LBB15_196
	.p2align	4, 0x90
.LBB15_209:                             # %._crit_edge1639
                                        #   in Loop: Header=BB15_196 Depth=2
	movq	active_pps(%rip), %rax
	movl	12(%rax), %esi
	incl	%r14d
	movq	16(%rsp), %r11          # 8-byte Reload
.LBB15_196:                             #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$8, 120(%rsp)
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%dl
	movb	$59, %al
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %al
	xorl	%ecx, %ecx
	andb	%dl, %al
	sete	%cl
	leal	8(%rcx,%rcx,4), %edx
	movl	%edx, 96(%rsp)
	movzbl	%al, %eax
	movl	%eax, 5616(%r11)
	movl	%r9d, 5620(%r11)
	movq	168(%rsp), %rax         # 8-byte Reload
	movq	40(%rax), %rcx
	movq	80(%rsp), %rax          # 8-byte Reload
	movslq	assignSE2partition(%rax,%rdx,4), %rax
	imulq	$56, %rax, %rbp
	leaq	(%rcx,%rbp), %rdx
	testl	%esi, %esi
	je	.LBB15_198
# BB#197:                               #   in Loop: Header=BB15_196 Depth=2
	movq	(%rdx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB15_206
.LBB15_198:                             #   in Loop: Header=BB15_196 Depth=2
	movq	$linfo_levrun_c2x2, 128(%rsp)
	jmp	.LBB15_207
	.p2align	4, 0x90
.LBB15_206:                             #   in Loop: Header=BB15_196 Depth=2
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_207:                             #   in Loop: Header=BB15_196 Depth=2
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rcx,%rbp)
	movl	100(%rsp), %eax
	testl	%eax, %eax
	movq	56(%rsp), %r9           # 8-byte Reload
	je	.LBB15_204
# BB#208:                               #   in Loop: Header=BB15_196 Depth=2
	movl	104(%rsp), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	orq	%r13, -92(%rdx)
	leal	1(%rbx,%rcx), %ebx
	movslq	%ebx, %rcx
	movzbl	SCAN_YUV422(%rcx,%rcx), %edx
	movzbl	SCAN_YUV422+1(%rcx,%rcx), %ecx
	shlq	$4, %rdx
	leaq	336(%rsp,%rdx), %rdx
	movl	%eax, (%rdx,%rcx,4)
	cmpl	$8, %r14d
	jle	.LBB15_209
	jmp	.LBB15_204
	.p2align	4, 0x90
.LBB15_216:                             #   in Loop: Header=BB15_137 Depth=1
	leaq	4(%r9,%r9), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	active_pps(%rip), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	je	.LBB15_221
# BB#217:                               # %.preheader1310
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r9d, %ecx
	shlb	$3, %cl
	movl	$4294901760, %r15d      # imm = 0xFFFF0000
	shlq	%cl, %r15
	movl	$-1, %ebp
	movl	$1, %r12d
	jmp	.LBB15_218
	.p2align	4, 0x90
.LBB15_238:                             # %._crit_edge1651
                                        #   in Loop: Header=BB15_218 Depth=2
	movq	active_pps(%rip), %rax
	movl	12(%rax), %eax
	incl	%r12d
.LBB15_218:                             #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$9, 120(%rsp)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%dl
	movb	$59, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	xorl	%ecx, %ecx
	andb	%dl, %bl
	sete	%cl
	leal	8(%rcx,%rcx,4), %edx
	movl	%edx, 96(%rsp)
	movzbl	%bl, %ecx
	movl	%ecx, 5616(%r11)
	movl	%r9d, 5620(%r11)
	movq	168(%rsp), %rcx         # 8-byte Reload
	movq	40(%rcx), %rcx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movslq	assignSE2partition(%rsi,%rdx,4), %rdx
	imulq	$56, %rdx, %rbx
	leaq	(%rcx,%rbx), %rdx
	testl	%eax, %eax
	je	.LBB15_220
# BB#219:                               #   in Loop: Header=BB15_218 Depth=2
	movq	(%rdx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB15_235
.LBB15_220:                             #   in Loop: Header=BB15_218 Depth=2
	movq	$linfo_levrun_c2x2, 128(%rsp)
	jmp	.LBB15_236
	.p2align	4, 0x90
.LBB15_235:                             #   in Loop: Header=BB15_218 Depth=2
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_236:                             #   in Loop: Header=BB15_218 Depth=2
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rcx,%rbx)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	testl	%eax, %eax
	movq	56(%rsp), %r9           # 8-byte Reload
	je	.LBB15_226
# BB#237:                               #   in Loop: Header=BB15_218 Depth=2
	movl	104(%rsp), %ecx
	movq	40(%rsp), %rdx          # 8-byte Reload
	orq	%r15, -92(%rdx)
	leal	1(%rbp,%rcx), %ebp
	movslq	%ebp, %rcx
	movzbl	SNGL_SCAN(%rcx,%rcx), %edx
	movzbl	SNGL_SCAN+1(%rcx,%rcx), %ecx
	movq	192(%rsp), %rsi         # 8-byte Reload
	leal	(%rsi,%rcx), %ecx
	leaq	(%rdx,%rdx,2), %rdx
	shlq	$8, %rdx
	addq	%r11, %rdx
	movslq	%ecx, %rcx
	shlq	$6, %rcx
	movl	%eax, 2408(%rcx,%rdx)
	cmpl	$16, %r12d
	jle	.LBB15_238
	jmp	.LBB15_226
.LBB15_145:                             #   in Loop: Header=BB15_137 Depth=1
	movl	$6, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r11, %rdi
	leaq	480(%rsp), %r9
	leaq	36(%rsp), %rax
	pushq	%rax
.Lcfi126:
	.cfi_adjust_cfa_offset 8
	leaq	424(%rsp), %rax
	pushq	%rax
.Lcfi127:
	.cfi_adjust_cfa_offset 8
	callq	readCoeff4x4_CAVLC
	movq	72(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %r11          # 8-byte Reload
	addq	$16, %rsp
.Lcfi128:
	.cfi_adjust_cfa_offset -16
	movslq	36(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB15_157
# BB#146:                               # %.lr.ph1380
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r9d, %ecx
	addb	%cl, %cl
	movl	$983040, %edx           # imm = 0xF0000
	shll	%cl, %edx
	movslq	%edx, %rcx
	movl	$-1, %edx
	testb	$1, %al
	jne	.LBB15_148
# BB#147:                               #   in Loop: Header=BB15_137 Depth=1
	xorl	%esi, %esi
	cmpl	$1, %eax
	jne	.LBB15_152
	jmp	.LBB15_157
.LBB15_199:                             #   in Loop: Header=BB15_137 Depth=1
	movl	$6, %edx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r11, %rdi
	leaq	480(%rsp), %r9
	leaq	36(%rsp), %rax
	pushq	%rax
.Lcfi129:
	.cfi_adjust_cfa_offset 8
	leaq	424(%rsp), %rax
	pushq	%rax
.Lcfi130:
	.cfi_adjust_cfa_offset 8
	callq	readCoeff4x4_CAVLC
	movq	72(%rsp), %r9           # 8-byte Reload
	addq	$16, %rsp
.Lcfi131:
	.cfi_adjust_cfa_offset -16
	movslq	36(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB15_204
# BB#200:                               # %.lr.ph1368
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	%r9d, %ecx
	shlb	$2, %cl
	movl	$16711680, %edx         # imm = 0xFF0000
	shlq	%cl, %rdx
	movl	$-1, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB15_201:                             #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	480(%rsp,%rsi,4), %edi
	testl	%edi, %edi
	je	.LBB15_203
# BB#202:                               #   in Loop: Header=BB15_201 Depth=2
	movq	40(%rsp), %rbp          # 8-byte Reload
	orq	%rdx, -92(%rbp)
	movl	416(%rsp,%rsi,4), %ebp
	leal	1(%rcx,%rbp), %ecx
	movslq	%ecx, %rbp
	movzbl	SCAN_YUV422(%rbp,%rbp), %ebx
	movzbl	SCAN_YUV422+1(%rbp,%rbp), %ebp
	shlq	$4, %rbx
	leaq	336(%rsp,%rbx), %rbx
	movl	%edi, (%rbx,%rbp,4)
.LBB15_203:                             #   in Loop: Header=BB15_201 Depth=2
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB15_201
.LBB15_204:                             # %.loopexit1317
                                        #   in Loop: Header=BB15_137 Depth=1
	movl	336(%rsp), %eax
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	je	.LBB15_210
# BB#205:                               # %.preheader1315.thread
                                        #   in Loop: Header=BB15_137 Depth=1
	leaq	(%r9,%r9,2), %rcx
	shlq	$8, %rcx
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	%eax, 2664(%r11,%rcx)
	movl	340(%rsp), %eax
	movl	%eax, 2728(%r11,%rcx)
	movl	344(%rsp), %eax
	movl	%eax, 2792(%r11,%rcx)
	movl	348(%rsp), %eax
	movl	%eax, 2856(%r11,%rcx)
	movq	%r9, %rax
	orq	$1, %rax
	movl	352(%rsp), %ecx
	leaq	(%rax,%rax,2), %rax
	shlq	$8, %rax
	movl	%ecx, 2664(%r11,%rax)
	movl	356(%rsp), %ecx
	movl	%ecx, 2728(%r11,%rax)
	movl	360(%rsp), %ecx
	movl	%ecx, 2792(%r11,%rax)
	movl	364(%rsp), %ecx
	movl	%ecx, 2856(%r11,%rax)
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	288(%rsp), %r13         # 8-byte Reload
	jmp	.LBB15_249
.LBB15_210:                             # %.preheader1315
                                        #   in Loop: Header=BB15_137 Depth=1
	movq	72(%rsp), %rcx          # 8-byte Reload
	leal	3(%r15,%rcx), %r8d
	movslq	%r8d, %rcx
	imulq	$715827883, %rcx, %r9   # imm = 0x2AAAAAAB
	movq	%r9, %rdx
	shrq	$63, %rdx
	shrq	$32, %r9
	addl	%edx, %r9d
	leal	(%r9,%r9), %edx
	leal	(%rdx,%rdx,2), %edx
	subl	%edx, %ecx
	movdqa	352(%rsp), %xmm0
	movd	348(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movd	340(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	punpckldq	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movd	%eax, %xmm1
	movd	344(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	punpckldq	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movdqa	%xmm0, %xmm2
	paddd	%xmm1, %xmm2
	movdqa	%xmm2, 576(%rsp)
	psubd	%xmm0, %xmm1
	movdqa	%xmm1, 592(%rsp)
	movslq	%ecx, %rax
	shlq	$6, %rax
	leaq	(%r12,%r12,2), %rcx
	shlq	$7, %rcx
	leaq	InvLevelScale4x4Chroma_Intra(%rcx,%rax), %rdx
	leaq	InvLevelScale4x4Chroma_Inter(%rcx,%rax), %r15
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	cmovneq	%rdx, %r15
	movl	$3, %ecx
	subl	%r9d, %ecx
	movl	$1, %r10d
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %r10d
	movl	$4, %r11d
	subl	%r9d, %r11d
	addl	$-4, %r9d
	movq	$-1, %rbx
	movq	400(%rsp), %rsi         # 8-byte Reload
	leaq	576(%rsp), %rax
	movq	288(%rsp), %r13         # 8-byte Reload
	.p2align	4, 0x90
.LBB15_211:                             #   Parent Loop BB15_137 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqa	(%rax), %xmm0
	movdqa	%xmm0, 256(%rsp)
	movl	264(%rsp), %ecx
	movl	256(%rsp), %ebp
	movl	260(%rsp), %edi
	leal	(%rcx,%rbp), %r14d
	subl	%ecx, %ebp
	movl	268(%rsp), %edx
	movl	%edi, %r12d
	subl	%edx, %r12d
	addl	%edi, %edx
	leal	(%rdx,%r14), %edi
	cmpl	$24, %r8d
	jge	.LBB15_212
# BB#213:                               # %.split.us.preheader
                                        #   in Loop: Header=BB15_211 Depth=2
	imull	(%r15), %edi
	addl	%r10d, %edi
	movl	%r11d, %ecx
	sarl	%cl, %edi
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, -192(%rsi)
	movl	%r14d, %edi
	subl	%edx, %edi
	imull	(%r15), %edi
	addl	%r10d, %edi
	sarl	%cl, %edi
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, (%rsi)
	leal	(%r12,%rbp), %edi
	imull	(%r15), %edi
	addl	%r10d, %edi
	sarl	%cl, %edi
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, -128(%rsi)
	movl	%ebp, %edi
	subl	%r12d, %edi
	imull	(%r15), %edi
	addl	%r10d, %edi
	sarl	%cl, %edi
	jmp	.LBB15_214
	.p2align	4, 0x90
.LBB15_212:                             # %.split.preheader
                                        #   in Loop: Header=BB15_211 Depth=2
	imull	(%r15), %edi
	movl	%r9d, %ecx
	shll	%cl, %edi
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, -192(%rsi)
	movl	%r14d, %edi
	subl	%edx, %edi
	imull	(%r15), %edi
	shll	%cl, %edi
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, (%rsi)
	leal	(%r12,%rbp), %edi
	imull	(%r15), %edi
	shll	%cl, %edi
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, -128(%rsi)
	movl	%ebp, %edi
	subl	%r12d, %edi
	imull	(%r15), %edi
	shll	%cl, %edi
.LBB15_214:                             # %.us-lcssa.us
                                        #   in Loop: Header=BB15_211 Depth=2
	addl	$2, %edi
	sarl	$2, %edi
	movl	%edi, -64(%rsi)
	addq	$16, %rax
	addq	$768, %rsi              # imm = 0x300
	incq	%rbx
	jle	.LBB15_211
	jmp	.LBB15_215
.LBB15_251:
	movl	5924(%r11), %ecx
	cmpl	$31, 64(%rsp)           # 4-byte Folded Reload
	jg	.LBB15_253
.LBB15_252:                             # %.thread1287.preheader
	movq	5560(%r11), %rax
	movl	4(%r11), %edx
	movq	(%rax,%rdx,8), %rax
	movq	(%rax), %rdi
	addq	$16, %rdi
	movslq	%ecx, %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	movq	%r11, %rbx
	callq	memset
	movq	5560(%rbx), %rax
	movl	4(%rbx), %ecx
	movq	(%rax,%rcx,8), %rax
	movq	8(%rax), %rdi
	addq	$16, %rdi
	movslq	5924(%rbx), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	5560(%rbx), %rax
	movl	4(%rbx), %ecx
	movq	(%rax,%rcx,8), %rax
	movq	16(%rax), %rdi
	addq	$16, %rdi
	movslq	5924(%rbx), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
	movq	5560(%rbx), %rax
	movl	4(%rbx), %ecx
	movq	(%rax,%rcx,8), %rax
	movq	24(%rax), %rdi
	addq	$16, %rdi
	movslq	5924(%rbx), %rdx
	shlq	$2, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB15_265:                             # %.loopexit1305
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB15_253:                             # %.thread1288.preheader
	testl	%ecx, %ecx
	jle	.LBB15_265
# BB#254:                               # %.lr.ph1362
	xorl	%esi, %esi
	shlq	$5, 296(%rsp)           # 8-byte Folded Spill
	.p2align	4, 0x90
.LBB15_255:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_256 Depth 2
                                        #       Child Loop BB15_273 Depth 3
                                        #       Child Loop BB15_281 Depth 3
                                        #       Child Loop BB15_260 Depth 3
                                        #       Child Loop BB15_268 Depth 3
	sarl	%ecx
	movslq	%ecx, %rax
	xorl	%edx, %edx
	cmpq	%rax, %rsi
	setge	%dl
	movq	%rdx, %rax
	shlq	$7, %rax
	leaq	InvLevelScale4x4Chroma_Inter(%rax,%rax,2), %rcx
	leaq	InvLevelScale4x4Chroma_Intra(%rax,%rax,2), %rax
	cmpb	$0, 15(%rsp)            # 1-byte Folded Reload
	cmoveq	%rcx, %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	movslq	248(%rsp,%rdx,4), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	%rdx, 208(%rsp)         # 8-byte Spill
	movl	%edx, 5620(%r11)
	xorl	%edi, %edi
	movq	%rsi, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB15_256:                             #   Parent Loop BB15_255 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB15_273 Depth 3
                                        #       Child Loop BB15_281 Depth 3
                                        #       Child Loop BB15_260 Depth 3
                                        #       Child Loop BB15_268 Depth 3
	movq	296(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rsi,4), %rbp
	movzbl	cofuv_blk_x(%rdi,%rbp), %ecx
	movzbl	cofuv_blk_y(%rdi,%rbp), %r8d
	movq	active_pps(%rip), %rax
	movl	12(%rax), %eax
	testl	%eax, %eax
	movq	%rcx, %r14
	movq	%r8, 72(%rsp)           # 8-byte Spill
	movq	%rdi, 184(%rsp)         # 8-byte Spill
	je	.LBB15_257
# BB#271:                               #   in Loop: Header=BB15_256 Depth=2
	movzbl	subblk_offset_y(%rdi,%rbp), %edx
	shrl	$2, %edx
	movl	%edx, 5612(%r11)
	movzbl	subblk_offset_x(%rdi,%rbp), %ecx
	shrl	$2, %ecx
	movl	%ecx, 5608(%r11)
	movl	$7, 120(%rsp)
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%dl
	movb	$59, %bl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %bl
	xorl	%ecx, %ecx
	andb	%dl, %bl
	sete	%cl
	leal	10(%rcx,%rcx,4), %ecx
	movl	%ecx, 96(%rsp)
	movzbl	%bl, %edx
	movl	%edx, 5616(%r11)
	leaq	cbp_blk_chroma(%rdi,%rsi,4), %r13
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	je	.LBB15_272
# BB#280:                               # %.preheader1301.preheader
                                        #   in Loop: Header=BB15_256 Depth=2
	xorl	%ebx, %ebx
	movl	$1, %r15d
	jmp	.LBB15_281
	.p2align	4, 0x90
.LBB15_287:                             # %..preheader1301_crit_edge
                                        #   in Loop: Header=BB15_281 Depth=3
	movl	96(%rsp), %ecx
	movq	active_pps(%rip), %rax
	movl	12(%rax), %eax
	incl	%r15d
.LBB15_281:                             # %.preheader1301
                                        #   Parent Loop BB15_255 Depth=1
                                        #     Parent Loop BB15_256 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	40(%rdx), %rbp
	movslq	%ecx, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	movslq	assignSE2partition(%rdx,%rcx,4), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rbp,%rcx), %rdx
	testl	%eax, %eax
	je	.LBB15_283
# BB#282:                               #   in Loop: Header=BB15_281 Depth=3
	movq	(%rdx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB15_284
.LBB15_283:                             #   in Loop: Header=BB15_281 Depth=3
	movq	$linfo_levrun_inter, 128(%rsp)
	jmp	.LBB15_285
	.p2align	4, 0x90
.LBB15_284:                             #   in Loop: Header=BB15_281 Depth=3
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_285:                             #   in Loop: Header=BB15_281 Depth=3
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rbp,%rcx)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	testl	%eax, %eax
	movq	%r14, %rdi
	movq	72(%rsp), %rbp          # 8-byte Reload
	je	.LBB15_263
# BB#286:                               #   in Loop: Header=BB15_281 Depth=3
	movl	104(%rsp), %edx
	movzbl	(%r13), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	orq	%rsi, -92(%rcx)
	leal	1(%rbx,%rdx), %ebx
	movslq	%ebx, %rcx
	movq	176(%rsp), %rdx         # 8-byte Reload
	movq	%rdx, %rsi
	movzbl	1(%rsi,%rcx,2), %edx
	movzbl	(%rsi,%rcx,2), %ecx
	leaq	(%rdi,%rdi,2), %rsi
	shlq	$8, %rsi
	addq	%r11, %rsi
	movq	%rbp, %rdi
	shlq	$6, %rdi
	addq	%rsi, %rdi
	shlq	$4, %rdx
	addq	%rdi, %rdx
	movl	%eax, 2408(%rdx,%rcx,4)
	cmpl	$15, %r15d
	jle	.LBB15_287
	jmp	.LBB15_263
	.p2align	4, 0x90
.LBB15_257:                             #   in Loop: Header=BB15_256 Depth=2
	movl	$7, %edx
	movq	%r11, %rdi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	leaq	480(%rsp), %r9
	leaq	36(%rsp), %rax
	pushq	%rax
.Lcfi132:
	.cfi_adjust_cfa_offset 8
	leaq	424(%rsp), %rax
	pushq	%rax
.Lcfi133:
	.cfi_adjust_cfa_offset 8
	callq	readCoeff4x4_CAVLC
	addq	$16, %rsp
.Lcfi134:
	.cfi_adjust_cfa_offset -16
	movslq	36(%rsp), %r9
	cmpb	$0, 52(%rsp)            # 1-byte Folded Reload
	je	.LBB15_258
# BB#266:                               # %.preheader
                                        #   in Loop: Header=BB15_256 Depth=2
	testl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	72(%rsp), %r10          # 8-byte Reload
	jle	.LBB15_263
# BB#267:                               # %.lr.ph1359
                                        #   in Loop: Header=BB15_256 Depth=2
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	cbp_blk_chroma(%rcx,%rax,4), %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_268:                             #   Parent Loop BB15_255 Depth=1
                                        #     Parent Loop BB15_256 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	480(%rsp,%rsi,4), %ebp
	testl	%ebp, %ebp
	je	.LBB15_270
# BB#269:                               #   in Loop: Header=BB15_268 Depth=3
	movzbl	(%r8), %ecx
	movl	$1, %eax
	shlq	%cl, %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	orq	%rax, -92(%rcx)
	movl	416(%rsp,%rsi,4), %eax
	leal	1(%rdi,%rax), %edi
	movslq	%edi, %rax
	movq	176(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %rdx
	movzbl	1(%rdx,%rax,2), %ecx
	movzbl	(%rdx,%rax,2), %eax
	leaq	(%r14,%r14,2), %rbx
	shlq	$8, %rbx
	addq	%r11, %rbx
	movq	%r10, %rdx
	shlq	$6, %rdx
	addq	%rbx, %rdx
	shlq	$4, %rcx
	addq	%rdx, %rcx
	movl	%ebp, 2408(%rcx,%rax,4)
.LBB15_270:                             #   in Loop: Header=BB15_268 Depth=3
	incq	%rsi
	cmpq	%r9, %rsi
	jl	.LBB15_268
	jmp	.LBB15_263
	.p2align	4, 0x90
.LBB15_272:                             # %.preheader1303.preheader
                                        #   in Loop: Header=BB15_256 Depth=2
	xorl	%r12d, %r12d
	movl	$1, %r15d
	jmp	.LBB15_273
	.p2align	4, 0x90
.LBB15_279:                             # %..preheader1303_crit_edge
                                        #   in Loop: Header=BB15_273 Depth=3
	movl	96(%rsp), %ecx
	movq	active_pps(%rip), %rax
	movl	12(%rax), %eax
	incl	%r15d
.LBB15_273:                             # %.preheader1303
                                        #   Parent Loop BB15_255 Depth=1
                                        #     Parent Loop BB15_256 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	168(%rsp), %rdx         # 8-byte Reload
	movq	40(%rdx), %rbp
	movslq	%ecx, %rcx
	movq	80(%rsp), %rdx          # 8-byte Reload
	movslq	assignSE2partition(%rdx,%rcx,4), %rcx
	imulq	$56, %rcx, %rcx
	leaq	(%rbp,%rcx), %rdx
	testl	%eax, %eax
	je	.LBB15_275
# BB#274:                               #   in Loop: Header=BB15_273 Depth=3
	movq	(%rdx), %rax
	cmpl	$0, 24(%rax)
	je	.LBB15_276
.LBB15_275:                             #   in Loop: Header=BB15_273 Depth=3
	movq	$linfo_levrun_inter, 128(%rsp)
	jmp	.LBB15_277
	.p2align	4, 0x90
.LBB15_276:                             #   in Loop: Header=BB15_273 Depth=3
	movq	$readRunLevel_CABAC, 136(%rsp)
.LBB15_277:                             #   in Loop: Header=BB15_273 Depth=3
	leaq	96(%rsp), %rdi
	movq	%r11, %rsi
	callq	*48(%rbp,%rcx)
	movq	16(%rsp), %r11          # 8-byte Reload
	movl	100(%rsp), %eax
	testl	%eax, %eax
	movq	%r14, %rdi
	movq	72(%rsp), %rbp          # 8-byte Reload
	je	.LBB15_263
# BB#278:                               #   in Loop: Header=BB15_273 Depth=3
	movl	104(%rsp), %edx
	movzbl	(%r13), %ecx
	movl	$1, %esi
	shlq	%cl, %rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	orq	%rsi, -92(%rcx)
	leal	1(%r12,%rdx), %r12d
	movslq	%r12d, %rcx
	movq	176(%rsp), %rdx         # 8-byte Reload
	movzbl	1(%rdx,%rcx,2), %esi
	movzbl	(%rdx,%rcx,2), %edx
	movq	192(%rsp), %rcx         # 8-byte Reload
	shlq	$6, %rcx
	addq	56(%rsp), %rcx          # 8-byte Folded Reload
	shlq	$4, %rsi
	addq	%rsi, %rcx
	imull	(%rcx,%rdx,4), %eax
	leaq	(%rdi,%rdi,2), %rcx
	shlq	$8, %rcx
	addq	%r11, %rcx
	movq	%rbp, %rdi
	shlq	$6, %rdi
	addq	%rcx, %rdi
	addq	%rsi, %rdi
	movq	208(%rsp), %rcx         # 8-byte Reload
	movzbl	216(%rsp,%rcx,4), %ecx
	shll	%cl, %eax
	addl	$8, %eax
	sarl	$4, %eax
	movl	%eax, 2408(%rdi,%rdx,4)
	cmpl	$15, %r15d
	jle	.LBB15_279
	jmp	.LBB15_263
.LBB15_258:                             # %.preheader1299
                                        #   in Loop: Header=BB15_256 Depth=2
	testl	%r9d, %r9d
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	72(%rsp), %r10          # 8-byte Reload
	jle	.LBB15_263
# BB#259:                               # %.lr.ph
                                        #   in Loop: Header=BB15_256 Depth=2
	movq	200(%rsp), %rax         # 8-byte Reload
	movq	184(%rsp), %rcx         # 8-byte Reload
	leaq	cbp_blk_chroma(%rcx,%rax,4), %r8
	xorl	%esi, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB15_260:                             #   Parent Loop BB15_255 Depth=1
                                        #     Parent Loop BB15_256 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	480(%rsp,%rsi,4), %ebp
	testl	%ebp, %ebp
	je	.LBB15_262
# BB#261:                               #   in Loop: Header=BB15_260 Depth=3
	movzbl	(%r8), %ecx
	movl	$1, %ebx
	shlq	%cl, %rbx
	movq	40(%rsp), %rax          # 8-byte Reload
	orq	%rbx, -92(%rax)
	movl	416(%rsp,%rsi,4), %ecx
	leal	1(%rdi,%rcx), %edi
	movslq	%edi, %rcx
	movq	176(%rsp), %rax         # 8-byte Reload
	movzbl	1(%rax,%rcx,2), %edx
	movzbl	(%rax,%rcx,2), %ebx
	movq	192(%rsp), %rcx         # 8-byte Reload
	shlq	$6, %rcx
	addq	56(%rsp), %rcx          # 8-byte Folded Reload
	shlq	$4, %rdx
	addq	%rdx, %rcx
	imull	(%rcx,%rbx,4), %ebp
	leaq	(%r14,%r14,2), %rcx
	shlq	$8, %rcx
	addq	%r11, %rcx
	movq	%r10, %rax
	shlq	$6, %rax
	addq	%rcx, %rax
	addq	%rdx, %rax
	movq	208(%rsp), %rcx         # 8-byte Reload
	movzbl	216(%rsp,%rcx,4), %ecx
	shll	%cl, %ebp
	addl	$8, %ebp
	sarl	$4, %ebp
	movl	%ebp, 2408(%rax,%rbx,4)
.LBB15_262:                             #   in Loop: Header=BB15_260 Depth=3
	incq	%rsi
	cmpq	%r9, %rsi
	jl	.LBB15_260
	.p2align	4, 0x90
.LBB15_263:                             # %.loopexit
                                        #   in Loop: Header=BB15_256 Depth=2
	movq	184(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	cmpq	$4, %rdi
	movq	200(%rsp), %rsi         # 8-byte Reload
	jne	.LBB15_256
# BB#264:                               # %.thread1288
                                        #   in Loop: Header=BB15_255 Depth=1
	incq	%rsi
	movslq	5924(%r11), %rcx
	cmpq	%rcx, %rsi
	jl	.LBB15_255
	jmp	.LBB15_265
.Lfunc_end15:
	.size	readCBPandCoeffsFromNAL, .Lfunc_end15-readCBPandCoeffsFromNAL
	.cfi_endproc

	.globl	readIPCMcoeffsFromNAL
	.p2align	4, 0x90
	.type	readIPCMcoeffsFromNAL,@function
readIPCMcoeffsFromNAL:                  # @readIPCMcoeffsFromNAL
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi137:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi138:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi139:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi141:
	.cfi_def_cfa_offset 112
.Lcfi142:
	.cfi_offset %rbx, -56
.Lcfi143:
	.cfi_offset %r12, -48
.Lcfi144:
	.cfi_offset %r13, -40
.Lcfi145:
	.cfi_offset %r14, -32
.Lcfi146:
	.cfi_offset %r15, -24
.Lcfi147:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rdi, %r12
	movq	active_pps(%rip), %rax
	cmpl	$1, 12(%rax)
	jne	.LBB16_26
# BB#1:
	movl	$8, 20(%rsp)
	xorl	%ecx, %ecx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB16_2:                               # %.preheader85
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_3 Depth 2
	movl	%ecx, %eax
	sarl	$2, %eax
	movslq	%eax, %rbp
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%ecx, %ebx
	andl	$3, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB16_3:                               #   Parent Loop BB16_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	movq	%r15, %rdi
	callq	readIPCMBytes_CABAC
	movl	12(%rsp), %eax
	movl	%r13d, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	movl	%r13d, %edx
	andl	$3, %edx
	leaq	(%rbp,%rbp,2), %rsi
	shlq	$8, %rsi
	addq	%r12, %rsi
	shlq	$6, %rcx
	addq	%rsi, %rcx
	movq	%rbx, %rsi
	shlq	$4, %rsi
	addq	%rcx, %rsi
	movl	%eax, 2408(%rsi,%rdx,4)
	incl	%r13d
	cmpl	$16, %r13d
	jne	.LBB16_3
# BB#4:                                 #   in Loop: Header=BB16_2 Depth=1
	movl	4(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	cmpl	$16, %ecx
	jne	.LBB16_2
# BB#5:
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB16_20
# BB#6:                                 # %.preheader84
	movl	5936(%r12), %eax
	testl	%eax, %eax
	jle	.LBB16_20
# BB#7:                                 # %.preheader83.lr.ph
	movl	5932(%r12), %ecx
	xorl	%edx, %edx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB16_8:                               # %.preheader83
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_10 Depth 2
	testl	%ecx, %ecx
	jle	.LBB16_12
# BB#9:                                 # %.lr.ph96
                                        #   in Loop: Header=BB16_8 Depth=1
	movl	%edx, %eax
	sarl	$2, %eax
	movslq	%eax, %r13
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %ebp
	andl	$3, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_10:                              #   Parent Loop BB16_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	movq	%r15, %rdi
	callq	readIPCMBytes_CABAC
	movl	12(%rsp), %eax
	movl	%ebx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	shlq	$6, %rcx
	movl	%ebx, %edx
	andl	$3, %edx
	leaq	(%r13,%r13,2), %rsi
	shlq	$8, %rsi
	addq	%r12, %rsi
	addq	%rcx, %rsi
	movq	%rbp, %rcx
	shlq	$4, %rcx
	addq	%rsi, %rcx
	movl	%eax, 2664(%rcx,%rdx,4)
	incl	%ebx
	movl	5932(%r12), %ecx
	cmpl	%ecx, %ebx
	jl	.LBB16_10
# BB#11:                                # %._crit_edge97.loopexit
                                        #   in Loop: Header=BB16_8 Depth=1
	movl	5936(%r12), %eax
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB16_12:                              # %._crit_edge97
                                        #   in Loop: Header=BB16_8 Depth=1
	incl	%edx
	cmpl	%eax, %edx
	jl	.LBB16_8
# BB#13:                                # %.preheader82
	testl	%eax, %eax
	jle	.LBB16_20
# BB#14:                                # %.preheader.lr.ph
	movl	5932(%r12), %ecx
	xorl	%edx, %edx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB16_15:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_17 Depth 2
	testl	%ecx, %ecx
	jle	.LBB16_19
# BB#16:                                # %.lr.ph
                                        #   in Loop: Header=BB16_15 Depth=1
	movl	%edx, %eax
	sarl	$2, %eax
	addl	$2, %eax
	movslq	%eax, %r13
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %ebp
	andl	$3, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_17:                              #   Parent Loop BB16_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	movq	%r15, %rdi
	callq	readIPCMBytes_CABAC
	movl	12(%rsp), %eax
	movl	%ebx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	shlq	$6, %rcx
	movl	%ebx, %edx
	andl	$3, %edx
	leaq	(%r13,%r13,2), %rsi
	shlq	$8, %rsi
	addq	%r12, %rsi
	addq	%rcx, %rsi
	movq	%rbp, %rcx
	shlq	$4, %rcx
	addq	%rsi, %rcx
	movl	%eax, 2664(%rcx,%rdx,4)
	incl	%ebx
	movl	5932(%r12), %ecx
	cmpl	%ecx, %ebx
	jl	.LBB16_17
# BB#18:                                # %._crit_edge.loopexit
                                        #   in Loop: Header=BB16_15 Depth=1
	movl	5936(%r12), %eax
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB16_19:                              # %._crit_edge
                                        #   in Loop: Header=BB16_15 Depth=1
	incl	%edx
	cmpl	%eax, %edx
	jl	.LBB16_15
.LBB16_20:                              # %.loopexit
	movq	5592(%r12), %r14
	movl	28(%r14), %eax
	testl	%eax, %eax
	je	.LBB16_21
# BB#22:                                # %.loopexit
	cmpl	$1, %eax
	jne	.LBB16_48
# BB#23:
	movl	$3, %r15d
	jmp	.LBB16_24
.LBB16_26:
	movq	(%r14), %rsi
	movl	8(%rsi), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$29, %ecx
	addl	%eax, %ecx
	andl	$-8, %ecx
	subl	%ecx, %eax
	je	.LBB16_28
# BB#27:
	movl	$8, %ecx
	subl	%eax, %ecx
	movl	%ecx, 20(%rsp)
	leaq	8(%rsp), %rdi
	callq	readSyntaxElement_FLC
.LBB16_28:
	movl	5876(%r12), %eax
	movl	%eax, 20(%rsp)
	xorl	%ecx, %ecx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB16_29:                              # %.preheader91
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_30 Depth 2
	movl	%ecx, %eax
	sarl	$2, %eax
	movslq	%eax, %rbp
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	movl	%ecx, %ebx
	andl	$3, %ebx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB16_30:                              #   Parent Loop BB16_29 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	movq	%r15, %rdi
	callq	readSyntaxElement_FLC
	movl	12(%rsp), %eax
	movl	%r13d, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	movl	%r13d, %edx
	andl	$3, %edx
	leaq	(%rbp,%rbp,2), %rsi
	shlq	$8, %rsi
	addq	%r12, %rsi
	shlq	$6, %rcx
	addq	%rsi, %rcx
	movq	%rbx, %rsi
	shlq	$4, %rsi
	addq	%rcx, %rsi
	movl	%eax, 2408(%rsi,%rdx,4)
	incl	%r13d
	cmpl	$16, %r13d
	jne	.LBB16_30
# BB#31:                                #   in Loop: Header=BB16_29 Depth=1
	movl	4(%rsp), %ecx           # 4-byte Reload
	incl	%ecx
	cmpl	$16, %ecx
	jne	.LBB16_29
# BB#32:
	movl	5880(%r12), %eax
	movl	%eax, 20(%rsp)
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB16_47
# BB#33:                                # %.preheader90
	movl	5936(%r12), %eax
	testl	%eax, %eax
	jle	.LBB16_47
# BB#34:                                # %.preheader89.lr.ph
	movl	5932(%r12), %ecx
	xorl	%edx, %edx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB16_35:                              # %.preheader89
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_37 Depth 2
	testl	%ecx, %ecx
	jle	.LBB16_39
# BB#36:                                # %.lr.ph106
                                        #   in Loop: Header=BB16_35 Depth=1
	movl	%edx, %eax
	sarl	$2, %eax
	movslq	%eax, %r13
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %ebp
	andl	$3, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_37:                              #   Parent Loop BB16_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	movq	%r15, %rdi
	callq	readSyntaxElement_FLC
	movl	12(%rsp), %eax
	movl	%ebx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	shlq	$6, %rcx
	movl	%ebx, %edx
	andl	$3, %edx
	leaq	(%r13,%r13,2), %rsi
	shlq	$8, %rsi
	addq	%r12, %rsi
	addq	%rcx, %rsi
	movq	%rbp, %rcx
	shlq	$4, %rcx
	addq	%rsi, %rcx
	movl	%eax, 2664(%rcx,%rdx,4)
	incl	%ebx
	movl	5932(%r12), %ecx
	cmpl	%ecx, %ebx
	jl	.LBB16_37
# BB#38:                                # %._crit_edge107.loopexit
                                        #   in Loop: Header=BB16_35 Depth=1
	movl	5936(%r12), %eax
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB16_39:                              # %._crit_edge107
                                        #   in Loop: Header=BB16_35 Depth=1
	incl	%edx
	cmpl	%eax, %edx
	jl	.LBB16_35
# BB#40:                                # %.preheader87
	testl	%eax, %eax
	jle	.LBB16_47
# BB#41:                                # %.preheader86.lr.ph
	movl	5932(%r12), %ecx
	xorl	%edx, %edx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB16_42:                              # %.preheader86
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_44 Depth 2
	testl	%ecx, %ecx
	jle	.LBB16_46
# BB#43:                                # %.lr.ph102
                                        #   in Loop: Header=BB16_42 Depth=1
	movl	%edx, %eax
	sarl	$2, %eax
	addl	$2, %eax
	movslq	%eax, %r13
	movl	%edx, 4(%rsp)           # 4-byte Spill
	movl	%edx, %ebp
	andl	$3, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_44:                              #   Parent Loop BB16_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rsi
	movq	%r15, %rdi
	callq	readSyntaxElement_FLC
	movl	12(%rsp), %eax
	movl	%ebx, %ecx
	sarl	$2, %ecx
	movslq	%ecx, %rcx
	shlq	$6, %rcx
	movl	%ebx, %edx
	andl	$3, %edx
	leaq	(%r13,%r13,2), %rsi
	shlq	$8, %rsi
	addq	%r12, %rsi
	addq	%rcx, %rsi
	movq	%rbp, %rcx
	shlq	$4, %rcx
	addq	%rsi, %rcx
	movl	%eax, 2664(%rcx,%rdx,4)
	incl	%ebx
	movl	5932(%r12), %ecx
	cmpl	%ecx, %ebx
	jl	.LBB16_44
# BB#45:                                # %._crit_edge103.loopexit
                                        #   in Loop: Header=BB16_42 Depth=1
	movl	5936(%r12), %eax
	movl	4(%rsp), %edx           # 4-byte Reload
.LBB16_46:                              # %._crit_edge103
                                        #   in Loop: Header=BB16_42 Depth=1
	incl	%edx
	cmpl	%eax, %edx
	jl	.LBB16_42
	jmp	.LBB16_47
.LBB16_21:
	movl	$1, %r15d
.LBB16_24:
	xorl	%ebx, %ebx
	movl	$8, %ebp
	.p2align	4, 0x90
.LBB16_25:                              # =>This Inner Loop Header: Depth=1
	movq	40(%r14), %rdi
	movq	-8(%rdi,%rbp), %rcx
	addq	%rbp, %rdi
	movl	(%rcx), %edx
	movq	16(%rcx), %rsi
	movl	44(%r12), %r8d
	callq	arideco_start_decoding
	incq	%rbx
	addq	$56, %rbp
	cmpq	%r15, %rbx
	jl	.LBB16_25
.LBB16_47:                              # %init_decoding_engine_IPCM.exit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_48:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end16:
	.size	readIPCMcoeffsFromNAL, .Lfunc_end16-readIPCMcoeffsFromNAL
	.cfi_endproc

	.globl	init_decoding_engine_IPCM
	.p2align	4, 0x90
	.type	init_decoding_engine_IPCM,@function
init_decoding_engine_IPCM:              # @init_decoding_engine_IPCM
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi148:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi149:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi150:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi151:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi152:
	.cfi_def_cfa_offset 48
.Lcfi153:
	.cfi_offset %rbx, -48
.Lcfi154:
	.cfi_offset %r12, -40
.Lcfi155:
	.cfi_offset %r13, -32
.Lcfi156:
	.cfi_offset %r14, -24
.Lcfi157:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	5592(%r14), %r15
	movl	28(%r15), %eax
	testl	%eax, %eax
	je	.LBB17_1
# BB#2:
	cmpl	$1, %eax
	jne	.LBB17_7
# BB#3:
	movl	$3, %r12d
	jmp	.LBB17_4
.LBB17_1:
	movl	$1, %r12d
.LBB17_4:
	xorl	%r13d, %r13d
	movl	$8, %ebx
	.p2align	4, 0x90
.LBB17_5:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rdi
	movq	-8(%rdi,%rbx), %rcx
	addq	%rbx, %rdi
	movl	(%rcx), %edx
	movq	16(%rcx), %rsi
	movl	44(%r14), %r8d
	callq	arideco_start_decoding
	incq	%r13
	addq	$56, %rbx
	cmpq	%r12, %r13
	jl	.LBB17_5
# BB#6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB17_7:
	movl	$.Lstr, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end17:
	.size	init_decoding_engine_IPCM, .Lfunc_end17-init_decoding_engine_IPCM
	.cfi_endproc

	.globl	BType2CtxRef
	.p2align	4, 0x90
	.type	BType2CtxRef,@function
BType2CtxRef:                           # @BType2CtxRef
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	cmpl	$3, %edi
	setg	%al
	retq
.Lfunc_end18:
	.size	BType2CtxRef, .Lfunc_end18-BType2CtxRef
	.cfi_endproc

	.globl	predict_nnz
	.p2align	4, 0x90
	.type	predict_nnz,@function
predict_nnz:                            # @predict_nnz
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi158:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi159:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi160:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi161:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi162:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi163:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi164:
	.cfi_def_cfa_offset 96
.Lcfi165:
	.cfi_offset %rbx, -56
.Lcfi166:
	.cfi_offset %r12, -48
.Lcfi167:
	.cfi_offset %r13, -40
.Lcfi168:
	.cfi_offset %r14, -32
.Lcfi169:
	.cfi_offset %r15, -24
.Lcfi170:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	movslq	4(%r12), %r14
	movq	5600(%r12), %rbx
	leal	(,%rsi,4), %r15d
	leal	-1(,%rsi,4), %esi
	shll	$2, %ebp
	movq	%rsp, %rcx
	movl	%r14d, %edi
	movl	%ebp, %edx
	callq	getLuma4x4Neighbour
	imulq	$408, %r14, %rax        # imm = 0x198
	leaq	40(%rbx,%rax), %rdx
	movl	40(%rbx,%rax), %eax
	cmpl	$14, %eax
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	ja	.LBB19_2
# BB#1:
	movl	$30208, %ecx            # imm = 0x7600
	btl	%eax, %ecx
	jae	.LBB19_2
# BB#3:
	movl	(%rsp), %eax
	testl	%eax, %eax
	je	.LBB19_4
# BB#5:
	movq	%rsp, %rbx
	movq	active_pps(%rip), %rcx
	xorl	%r13d, %r13d
	cmpl	$0, 1148(%rcx)
	je	.LBB19_6
# BB#8:
	movq	5592(%r12), %rcx
	cmpl	$1, 28(%rcx)
	movl	%r15d, %esi
	jne	.LBB19_7
# BB#9:
	movq	%rsp, %rbx
	movq	16(%r12), %rcx
	movslq	4(%rsp), %rdx
	xorl	%r13d, %r13d
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, (%rsp)
	sete	%r13b
	testl	%eax, %eax
	jne	.LBB19_7
	jmp	.LBB19_11
.LBB19_4:
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	movq	%rsp, %rbx
	movl	%r15d, %esi
	jmp	.LBB19_12
.LBB19_6:
	movl	%r15d, %esi
	jmp	.LBB19_7
.LBB19_2:                               # %._crit_edge
	movq	%rsp, %rbx
	xorl	%r13d, %r13d
	movl	(%rsp), %eax
	movl	%r15d, %esi
	testl	%eax, %eax
	je	.LBB19_11
.LBB19_7:                               # %.thread44
	movq	5560(%r12), %rax
	movslq	4(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	8(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	12(%rsp), %rcx
	movl	(%rax,%rcx,4), %r12d
	incl	%r13d
.LBB19_12:                              # %.thread48
	decl	%ebp
	movq	%rsp, %rcx
	movl	%r14d, %edi
	movl	%ebp, %edx
	callq	getLuma4x4Neighbour
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$14, %eax
	ja	.LBB19_14
# BB#13:                                # %.thread48
	movl	$30208, %ecx            # imm = 0x7600
	btl	%eax, %ecx
	jae	.LBB19_14
# BB#15:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB19_21
# BB#16:
	movq	active_pps(%rip), %rcx
	cmpl	$0, 1148(%rcx)
	je	.LBB19_20
# BB#17:
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	5592(%rdx), %rcx
	cmpl	$1, 28(%rcx)
	jne	.LBB19_20
# BB#18:
	movq	16(%rdx), %rcx
	movslq	4(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, (%rbx)
	cmpl	$1, %eax
	adcl	$0, %r13d
	testl	%eax, %eax
	jne	.LBB19_20
	jmp	.LBB19_21
.LBB19_14:                              # %.thread-pre-split_crit_edge
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB19_21
.LBB19_20:                              # %thread-pre-split.thread
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	5560(%rax), %rax
	movslq	4(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	8(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	12(%rsp), %rcx
	addl	(%rax,%rcx,4), %r12d
	incl	%r13d
.LBB19_21:                              # %.thread
	leal	1(%r12), %eax
	sarl	%eax
	cmpl	$2, %r13d
	cmovnel	%r12d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB19_11:
	xorl	%r12d, %r12d
	jmp	.LBB19_12
.Lfunc_end19:
	.size	predict_nnz, .Lfunc_end19-predict_nnz
	.cfi_endproc

	.globl	predict_nnz_chroma
	.p2align	4, 0x90
	.type	predict_nnz_chroma,@function
predict_nnz_chroma:                     # @predict_nnz_chroma
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi171:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi172:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi173:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi174:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi175:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi176:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi177:
	.cfi_def_cfa_offset 112
.Lcfi178:
	.cfi_offset %rbx, -56
.Lcfi179:
	.cfi_offset %r12, -48
.Lcfi180:
	.cfi_offset %r13, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movl	%edx, %ebx
	movl	%esi, %r12d
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	5600(%rdi), %rbp
	movslq	4(%rdi), %r15
	movq	dec_picture(%rip), %rax
	leal	(,%r12,4), %r13d
	cmpl	$3, 317044(%rax)
	jne	.LBB20_1
# BB#23:
	movslq	%ebx, %rax
	movslq	predict_nnz_chroma.j_off_tab(,%rax,4), %rax
	leal	-1(%r13), %esi
	movq	%rax, 48(%rsp)          # 8-byte Spill
	subl	%eax, %ebx
	shll	$2, %ebx
	leaq	16(%rsp), %r12
	movl	%r15d, %edi
	movl	%ebx, %edx
	movq	%r12, %rcx
	callq	getChroma4x4Neighbour
	imulq	$408, %r15, %rax        # imm = 0x198
	leaq	40(%rbp,%rax), %rdx
	movl	40(%rbp,%rax), %eax
	cmpl	$14, %eax
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	ja	.LBB20_25
# BB#24:
	movl	$30208, %ecx            # imm = 0x7600
	btl	%eax, %ecx
	jae	.LBB20_25
# BB#26:
	movl	16(%rsp), %eax
	testl	%eax, %eax
	je	.LBB20_27
# BB#28:
	leaq	16(%rsp), %r12
	movq	active_pps(%rip), %rcx
	xorl	%r14d, %r14d
	cmpl	$0, 1148(%rcx)
	je	.LBB20_29
# BB#31:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	5592(%rsi), %rcx
	cmpl	$1, 28(%rcx)
	jne	.LBB20_30
# BB#32:
	leaq	16(%rsp), %r12
	movq	16(%rsi), %rcx
	movslq	20(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, 16(%rsp)
	movl	$-1, %r14d
	testl	%eax, %eax
	jne	.LBB20_30
	jmp	.LBB20_34
.LBB20_1:
	andl	$4, %r13d
	leal	-1(%r13), %esi
	leal	(,%rbx,4), %eax
	movl	%eax, 40(%rsp)          # 4-byte Spill
	leal	-16(,%rbx,4), %edx
	leaq	16(%rsp), %rbx
	movl	%r15d, %edi
	movq	%rbx, %rcx
	callq	getChroma4x4Neighbour
	imulq	$408, %r15, %rax        # imm = 0x198
	leaq	40(%rbp,%rax), %rdx
	movl	40(%rbp,%rax), %eax
	cmpl	$14, %eax
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	ja	.LBB20_3
# BB#2:
	movl	$30208, %ecx            # imm = 0x7600
	btl	%eax, %ecx
	jae	.LBB20_3
# BB#4:
	movl	16(%rsp), %eax
	testl	%eax, %eax
	je	.LBB20_5
# BB#6:
	leaq	16(%rsp), %rbx
	movq	active_pps(%rip), %rcx
	xorl	%r14d, %r14d
	cmpl	$0, 1148(%rcx)
	je	.LBB20_7
# BB#9:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	5592(%rsi), %rcx
	cmpl	$1, 28(%rcx)
	movl	40(%rsp), %edx          # 4-byte Reload
	jne	.LBB20_8
# BB#10:
	leaq	16(%rsp), %rbx
	movq	16(%rsi), %rcx
	movslq	20(%rsp), %rdi
	xorl	%r14d, %r14d
	andl	(%rcx,%rdi,4), %eax
	movl	%eax, 16(%rsp)
	sete	%r14b
	testl	%eax, %eax
	jne	.LBB20_8
	jmp	.LBB20_12
.LBB20_27:
	xorl	%r14d, %r14d
	jmp	.LBB20_34
.LBB20_5:
	xorl	%r14d, %r14d
	xorl	%ebp, %ebp
	movl	40(%rsp), %edx          # 4-byte Reload
	jmp	.LBB20_13
.LBB20_29:
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB20_30
.LBB20_7:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	jmp	.LBB20_8
.LBB20_25:                              # %._crit_edge82
	leaq	16(%rsp), %r12
	xorl	%r14d, %r14d
	movl	16(%rsp), %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB20_34
.LBB20_30:                              # %.thread94
	movq	5560(%rsi), %rax
	movslq	20(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	24(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	28(%rsp), %rcx
	addq	48(%rsp), %rcx          # 8-byte Folded Reload
	movl	(%rax,%rcx,4), %ebp
	incl	%r14d
	jmp	.LBB20_35
.LBB20_34:
	xorl	%ebp, %ebp
.LBB20_35:                              # %.thread98
	decl	%ebx
	leaq	16(%rsp), %rcx
	movl	%r15d, %edi
	movl	%r13d, %esi
	movl	%ebx, %edx
	callq	getChroma4x4Neighbour
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$14, %eax
	ja	.LBB20_37
# BB#36:                                # %.thread98
	movl	$30208, %ecx            # imm = 0x7600
	btl	%eax, %ecx
	jae	.LBB20_37
# BB#38:
	movl	(%r12), %eax
	testl	%eax, %eax
	je	.LBB20_46
# BB#39:
	movq	active_pps(%rip), %rcx
	cmpl	$0, 1148(%rcx)
	je	.LBB20_40
# BB#41:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	5592(%rsi), %rcx
	cmpl	$1, 28(%rcx)
	jne	.LBB20_44
# BB#42:
	movq	16(%rsi), %rcx
	movslq	20(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, (%r12)
	decl	%r14d
	testl	%eax, %eax
	jne	.LBB20_44
	jmp	.LBB20_46
.LBB20_40:
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB20_44
.LBB20_3:                               # %._crit_edge
	leaq	16(%rsp), %rbx
	xorl	%r14d, %r14d
	movl	16(%rsp), %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	testl	%eax, %eax
	je	.LBB20_12
.LBB20_8:                               # %.thread87
	movq	5560(%rsi), %rax
	movslq	20(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movl	%r12d, %ecx
	andl	$-2, %ecx
	movslq	24(%rsp), %rsi
	movslq	%ecx, %rcx
	addq	%rsi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	28(%rsp), %rcx
	movl	16(%rax,%rcx,4), %ebp
	incl	%r14d
.LBB20_13:                              # %.thread91
	addl	$-17, %edx
	leaq	16(%rsp), %rcx
	movl	%r15d, %edi
	movl	%r13d, %esi
	callq	getChroma4x4Neighbour
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %eax
	cmpl	$14, %eax
	ja	.LBB20_15
# BB#14:                                # %.thread91
	movl	$30208, %ecx            # imm = 0x7600
	btl	%eax, %ecx
	jae	.LBB20_15
# BB#16:
	movl	(%rbx), %eax
	testl	%eax, %eax
	je	.LBB20_46
# BB#17:
	movq	active_pps(%rip), %rcx
	cmpl	$0, 1148(%rcx)
	je	.LBB20_18
# BB#19:
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	5592(%rsi), %rcx
	cmpl	$1, 28(%rcx)
	jne	.LBB20_22
# BB#20:
	movq	16(%rsi), %rcx
	movslq	20(%rsp), %rdx
	andl	(%rcx,%rdx,4), %eax
	movl	%eax, (%rbx)
	cmpl	$1, %eax
	adcl	$0, %r14d
	testl	%eax, %eax
	jne	.LBB20_22
	jmp	.LBB20_46
.LBB20_18:
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB20_22
.LBB20_37:                              # %.thread-pre-split78_crit_edge
	movl	(%r12), %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB20_46
.LBB20_44:                              # %thread-pre-split78.thread
	movq	5560(%rsi), %rax
	movslq	20(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	24(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	28(%rsp), %rcx
	addq	48(%rsp), %rcx          # 8-byte Folded Reload
	addl	(%rax,%rcx,4), %ebp
	jmp	.LBB20_45
.LBB20_15:                              # %.thread-pre-split_crit_edge
	movl	(%rbx), %eax
	movq	8(%rsp), %rsi           # 8-byte Reload
	testl	%eax, %eax
	je	.LBB20_46
.LBB20_22:                              # %thread-pre-split.thread
	movq	5560(%rsi), %rax
	movslq	20(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	andl	$-2, %r12d
	movslq	24(%rsp), %rcx
	movslq	%r12d, %rdx
	addq	%rcx, %rdx
	movq	(%rax,%rdx,8), %rax
	movslq	28(%rsp), %rcx
	addl	16(%rax,%rcx,4), %ebp
.LBB20_45:                              # %.thread
	incl	%r14d
.LBB20_46:                              # %.thread
	leal	1(%rbp), %eax
	sarl	%eax
	cmpl	$2, %r14d
	cmovnel	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB20_12:
	xorl	%ebp, %ebp
	jmp	.LBB20_13
.Lfunc_end20:
	.size	predict_nnz_chroma, .Lfunc_end20-predict_nnz_chroma
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI21_0:
	.long	4294967295              # 0xffffffff
	.long	4294967294              # 0xfffffffe
	.long	4294967293              # 0xfffffffd
	.long	4294967292              # 0xfffffffc
.LCPI21_1:
	.long	4294967291              # 0xfffffffb
	.long	4294967290              # 0xfffffffa
	.long	4294967289              # 0xfffffff9
	.long	4294967288              # 0xfffffff8
.LCPI21_2:
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
	.long	1065353216              # 0x3f800000
.LCPI21_3:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	readCoeff4x4_CAVLC
	.p2align	4, 0x90
	.type	readCoeff4x4_CAVLC,@function
readCoeff4x4_CAVLC:                     # @readCoeff4x4_CAVLC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi186:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi187:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi188:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi189:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi190:
	.cfi_def_cfa_offset 144
.Lcfi191:
	.cfi_offset %rbx, -56
.Lcfi192:
	.cfi_offset %r12, -48
.Lcfi193:
	.cfi_offset %r13, -40
.Lcfi194:
	.cfi_offset %r14, -32
.Lcfi195:
	.cfi_offset %r15, -24
.Lcfi196:
	.cfi_offset %rbp, -16
	movq	%r9, %r13
	movq	%rdi, %r15
	movq	5592(%r15), %r14
	movslq	28(%r14), %r12
	cmpl	$7, %edx
	ja	.LBB21_5
# BB#1:
	movl	4(%r15), %r10d
	movq	5600(%r15), %rdi
	movslq	%r10d, %rbp
	movl	$16, %eax
	movl	$7, %esi
	xorl	%r9d, %r9d
	movl	%edx, %edx
	xorl	%ebx, %ebx
	jmpq	*.LJTI21_0(,%rdx,8)
.LBB21_2:
	imulq	$408, %rbp, %rdx        # imm = 0x198
	movl	40(%rdi,%rdx), %edi
	addl	$-9, %edi
	movl	$59, %edx
	xorl	%esi, %esi
	btl	%edi, %edx
	setae	%sil
	cmpl	$6, %edi
	leal	9(%rsi,%rsi,4), %edx
	movl	$14, %esi
	cmovbl	%edx, %esi
	jmp	.LBB21_4
.LBB21_3:
	movl	$15, %eax
	movl	$9, %esi
.LBB21_4:
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	jmp	.LBB21_8
.LBB21_5:
	movl	$.L.str.4, %edi
	movl	$600, %esi              # imm = 0x258
	movl	%r8d, %ebx
	movl	%ecx, %ebp
	callq	error
	movl	%ebp, %ecx
	movl	%ebx, %r8d
	movl	4(%r15), %r10d
	xorl	%ebx, %ebx
	xorl	%r9d, %r9d
	xorl	%esi, %esi
	xorl	%eax, %eax
	jmp	.LBB21_8
.LBB21_6:
	movl	5928(%r15), %eax
	imulq	$408, %rbp, %rdx        # imm = 0x198
	movl	40(%rdi,%rdx), %edx
	addl	$-9, %edx
	movl	$59, %esi
	xorl	%edi, %edi
	btl	%edx, %esi
	setae	%dil
	cmpl	$6, %edx
	leal	8(%rdi,%rdi,4), %edx
	movl	$13, %esi
	cmovbl	%edx, %esi
	movb	$1, %bl
	jmp	.LBB21_8
.LBB21_7:
	imulq	$408, %rbp, %rax        # imm = 0x198
	movl	40(%rdi,%rax), %eax
	addl	$-9, %eax
	movl	$59, %edx
	xorl	%esi, %esi
	btl	%eax, %edx
	setae	%sil
	cmpl	$6, %eax
	leal	10(%rsi,%rsi,4), %esi
	movl	$15, %eax
	cmovael	%eax, %esi
	movl	$1, %r9d
	xorl	%ebx, %ebx
.LBB21_8:
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%esi, 16(%rsp)
	movl	%esi, %edx
	leaq	(%r12,%r12,4), %rsi
	shlq	$4, %rsi
	movslq	assignSE2partition(%rsi,%rdx,4), %rdx
	imulq	$56, %rdx, %r12
	addq	40(%r14), %r12
	movq	5560(%r15), %rdx
	movl	%r10d, %eax
	movq	(%rdx,%rax,8), %rax
	movslq	%ecx, %r14
	movq	(%rax,%r14,8), %rax
	movslq	%r8d, %rbp
	movl	$0, (%rax,%rbp,4)
	testb	%bl, %bl
	movl	%ebx, 12(%rsp)          # 4-byte Spill
	je	.LBB21_10
# BB#9:
	leaq	16(%rsp), %rdi
	movq	%r12, %rsi
	callq	readSyntaxElement_NumCoeffTrailingOnesChromaDC
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	20(%rsp), %r15d
	movl	24(%rsp), %ebx
	jmp	.LBB21_17
.LBB21_10:
	movq	%r15, %rdi
	movl	%ecx, %esi
	movl	%r8d, %edx
	testl	%r9d, %r9d
	je	.LBB21_12
# BB#11:
	callq	predict_nnz_chroma
	jmp	.LBB21_13
.LBB21_12:
	callq	predict_nnz
.LBB21_13:
	xorl	%ecx, %ecx
	cmpl	$2, %eax
	jl	.LBB21_16
# BB#14:
	movl	$1, %ecx
	cmpl	$4, %eax
	jl	.LBB21_16
# BB#15:
	xorl	%ecx, %ecx
	cmpl	$7, %eax
	setg	%cl
	orl	$2, %ecx
.LBB21_16:
	movl	%ecx, 20(%rsp)
	leaq	16(%rsp), %rdi
	leaq	73(%rsp), %rdx
	movq	%r12, %rsi
	callq	readSyntaxElement_NumCoeffTrailingOnes
	movl	20(%rsp), %edx
	movl	24(%rsp), %ebx
	movq	5560(%r15), %rax
	movl	4(%r15), %ecx
	movq	%rdx, %r15
	movq	(%rax,%rcx,8), %rax
	movq	(%rax,%r14,8), %rax
	movl	%r15d, (%rax,%rbp,4)
	movl	8(%rsp), %edx           # 4-byte Reload
.LBB21_17:
	movq	152(%rsp), %rax
	movq	144(%rsp), %rbp
	testl	%edx, %edx
	jle	.LBB21_34
# BB#18:                                # %.lr.ph210.preheader
	movl	%edx, %ecx
	cmpl	$8, %edx
	jae	.LBB21_20
# BB#19:
	xorl	%edx, %edx
	jmp	.LBB21_28
.LBB21_20:                              # %min.iters.checked
	movl	%edx, %esi
	andl	$7, %esi
	movq	%rcx, %rdx
	subq	%rsi, %rdx
	je	.LBB21_24
# BB#21:                                # %vector.memcheck
	leaq	(%rbp,%rcx,4), %rdi
	cmpq	%r13, %rdi
	jbe	.LBB21_25
# BB#22:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rdi
	cmpq	%rbp, %rdi
	jbe	.LBB21_25
.LBB21_24:
	xorl	%edx, %edx
	jmp	.LBB21_28
.LBB21_25:                              # %vector.body.preheader
	movl	%ebx, %r9d
	leaq	16(%r13), %rdi
	movq	%rbp, %r8
	leaq	16(%rbp), %rbp
	pxor	%xmm0, %xmm0
	movq	%rdx, %rbx
	.p2align	4, 0x90
.LBB21_26:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	movdqu	%xmm0, -16(%rbp)
	movdqu	%xmm0, (%rbp)
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %rbx
	jne	.LBB21_26
# BB#27:                                # %middle.block
	testl	%esi, %esi
	movq	%r8, %rbp
	movl	%r9d, %ebx
	je	.LBB21_34
.LBB21_28:                              # %.lr.ph210.preheader258
	movl	%ecx, %edi
	subl	%edx, %edi
	leaq	-1(%rcx), %rsi
	subq	%rdx, %rsi
	andq	$7, %rdi
	je	.LBB21_31
# BB#29:                                # %.lr.ph210.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB21_30:                              # %.lr.ph210.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%r13,%rdx,4)
	movl	$0, (%rbp,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB21_30
.LBB21_31:                              # %.lr.ph210.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB21_34
# BB#32:                                # %.lr.ph210.preheader258.new
	subq	%rdx, %rcx
	leaq	28(%rbp,%rdx,4), %rsi
	leaq	28(%r13,%rdx,4), %rdx
	.p2align	4, 0x90
.LBB21_33:                              # %.lr.ph210
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, -28(%rdx)
	movl	$0, -28(%rsi)
	movl	$0, -24(%rdx)
	movl	$0, -24(%rsi)
	movl	$0, -20(%rdx)
	movl	$0, -20(%rsi)
	movl	$0, -16(%rdx)
	movl	$0, -16(%rsi)
	movl	$0, -12(%rdx)
	movl	$0, -12(%rsi)
	movl	$0, -8(%rdx)
	movl	$0, -8(%rsi)
	movl	$0, -4(%rdx)
	movl	$0, -4(%rsi)
	movl	$0, (%rdx)
	movl	$0, (%rsi)
	addq	$32, %rsi
	addq	$32, %rdx
	addq	$-8, %rcx
	jne	.LBB21_33
.LBB21_34:                              # %._crit_edge211
	movl	%r15d, (%rax)
	testl	%r15d, %r15d
	je	.LBB21_66
# BB#35:
	testl	%ebx, %ebx
	movq	%r15, 64(%rsp)          # 8-byte Spill
	je	.LBB21_39
# BB#36:
	movl	%ebx, 28(%rsp)
	movq	(%r12), %rsi
	leaq	16(%rsp), %rdi
	callq	readSyntaxElement_FLC
	leal	-1(%r15), %eax
	movl	%eax, %r10d
	subl	%ebx, %r10d
	testl	%ebx, %ebx
	movl	%eax, 4(%rsp)           # 4-byte Spill
	jle	.LBB21_47
# BB#37:                                # %.lr.ph207.preheader
	movl	32(%rsp), %ecx
	movslq	%eax, %rdx
	movslq	%r10d, %rsi
	movq	%rdx, %rax
	negq	%rax
	movq	%rsi, %rdi
	notq	%rdi
	cmpq	%rdi, %rax
	cmovgeq	%rax, %rdi
	leaq	1(%rdi,%rdx), %r9
	cmpq	$8, %r9
	jb	.LBB21_44
# BB#40:                                # %min.iters.checked234
	movq	%r9, %rax
	andq	$-8, %rax
	movq	%r9, %r8
	andq	$-8, %r8
	je	.LBB21_44
# BB#41:                                # %vector.ph238
	leaq	-12(%r13,%rdx,4), %rbp
	subq	%rax, %rdx
	movl	%ebx, %edi
	subl	%eax, %edi
	movd	%ecx, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	movdqa	.LCPI21_0(%rip), %xmm8  # xmm8 = [4294967295,4294967294,4294967293,4294967292]
	movdqa	.LCPI21_1(%rip), %xmm9  # xmm9 = [4294967291,4294967290,4294967289,4294967288]
	movdqa	.LCPI21_2(%rip), %xmm3  # xmm3 = [1065353216,1065353216,1065353216,1065353216]
	movdqa	.LCPI21_3(%rip), %xmm4  # xmm4 = [1,1,1,1]
	pxor	%xmm5, %xmm5
	pcmpeqd	%xmm6, %xmm6
	movl	%ebx, %eax
	movq	%r8, %rbx
	movl	%eax, %r11d
	.p2align	4, 0x90
.LBB21_42:                              # %vector.body230
                                        # =>This Inner Loop Header: Depth=1
	movd	%eax, %xmm7
	pshufd	$0, %xmm7, %xmm1        # xmm1 = xmm7[0,0,0,0]
	movdqa	%xmm1, %xmm7
	paddd	%xmm8, %xmm7
	paddd	%xmm9, %xmm1
	pslld	$23, %xmm7
	paddd	%xmm3, %xmm7
	cvttps2dq	%xmm7, %xmm7
	pshufd	$245, %xmm7, %xmm2      # xmm2 = xmm7[1,1,3,3]
	pmuludq	%xmm4, %xmm7
	pshufd	$232, %xmm7, %xmm7      # xmm7 = xmm7[0,2,2,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm7    # xmm7 = xmm7[0],xmm2[0],xmm7[1],xmm2[1]
	pslld	$23, %xmm1
	paddd	%xmm3, %xmm1
	cvttps2dq	%xmm1, %xmm1
	pshufd	$245, %xmm1, %xmm2      # xmm2 = xmm1[1,1,3,3]
	pmuludq	%xmm4, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pmuludq	%xmm4, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	punpckldq	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	pand	%xmm0, %xmm7
	pand	%xmm0, %xmm1
	pcmpeqd	%xmm5, %xmm7
	pxor	%xmm6, %xmm7
	pcmpeqd	%xmm5, %xmm1
	pxor	%xmm6, %xmm1
	por	%xmm4, %xmm7
	por	%xmm4, %xmm1
	pshufd	$27, %xmm7, %xmm2       # xmm2 = xmm7[3,2,1,0]
	movdqu	%xmm2, (%rbp)
	pshufd	$27, %xmm1, %xmm1       # xmm1 = xmm1[3,2,1,0]
	movdqu	%xmm1, -16(%rbp)
	addl	$-8, %eax
	addq	$-32, %rbp
	addq	$-8, %rbx
	jne	.LBB21_42
# BB#43:                                # %middle.block231
	cmpq	%r8, %r9
	movl	%r11d, %ebx
	jne	.LBB21_45
	jmp	.LBB21_47
.LBB21_39:                              # %..loopexit_crit_edge
	leal	-1(%r15), %r10d
	movl	%r10d, 4(%rsp)          # 4-byte Spill
	subl	%ebx, %r10d
	testl	%r10d, %r10d
	jns	.LBB21_48
	jmp	.LBB21_55
.LBB21_44:
	movl	%ebx, %edi
.LBB21_45:                              # %.lr.ph207.preheader257
	decl	%edi
	movl	$-1, %eax
	.p2align	4, 0x90
.LBB21_46:                              # %.lr.ph207
                                        # =>This Inner Loop Header: Depth=1
	btl	%edi, %ecx
	movl	$1, %ebp
	cmovbl	%eax, %ebp
	movl	%ebp, (%r13,%rdx,4)
	decq	%rdx
	decl	%edi
	cmpq	%rsi, %rdx
	jg	.LBB21_46
.LBB21_47:                              # %.loopexit
	testl	%r10d, %r10d
	js	.LBB21_55
.LBB21_48:                              # %.lr.ph
	movq	64(%rsp), %rsi          # 8-byte Reload
	cmpl	$10, %esi
	setg	%al
	cmpl	$3, %ebx
	setl	%cl
	setne	%dl
	andb	%al, %cl
	movzbl	%cl, %ebp
	cmpl	$4, %esi
	setl	%al
	orb	%dl, %al
	movzbl	%al, %r14d
	movslq	%r10d, %r15
	leaq	(%r13,%r15,4), %rbx
	incq	%r15
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB21_49:                              # =>This Inner Loop Header: Depth=1
	leaq	16(%rsp), %rdi
	testl	%ebp, %ebp
	je	.LBB21_51
# BB#50:                                #   in Loop: Header=BB21_49 Depth=1
	movl	%ebp, %esi
	movq	%r12, %rdx
	callq	readSyntaxElement_Level_VLCN
	jmp	.LBB21_52
	.p2align	4, 0x90
.LBB21_51:                              #   in Loop: Header=BB21_49 Depth=1
	movq	%r12, %rsi
	callq	readSyntaxElement_Level_VLC0
.LBB21_52:                              #   in Loop: Header=BB21_49 Depth=1
	testl	%r14d, %r14d
	movl	32(%rsp), %eax
	je	.LBB21_54
# BB#53:                                #   in Loop: Header=BB21_49 Depth=1
	testl	%eax, %eax
	movl	$-1, %ecx
	movl	$1, %edx
	cmovgl	%edx, %ecx
	addl	%ecx, %eax
	movl	%eax, 32(%rsp)
.LBB21_54:                              # %._crit_edge219
                                        #   in Loop: Header=BB21_49 Depth=1
	movl	%eax, (%rbx,%r13,4)
	movl	%eax, %ecx
	negl	%ecx
	cmovll	%eax, %ecx
	movslq	%ebp, %rax
	xorl	%edx, %edx
	cmpl	readCoeff4x4_CAVLC.incVlc(,%rax,4), %ecx
	setg	%dl
	addl	%ebp, %edx
	cmpl	$3, %ecx
	movl	%edx, %ebp
	movl	$2, %eax
	cmovgl	%eax, %ebp
	testl	%r13d, %r13d
	cmovnel	%edx, %ebp
	leaq	-1(%r15,%r13), %rax
	decq	%r13
	xorl	%r14d, %r14d
	testq	%rax, %rax
	jg	.LBB21_49
.LBB21_55:                              # %._crit_edge
	xorl	%ebp, %ebp
	movq	64(%rsp), %r14          # 8-byte Reload
	cmpl	8(%rsp), %r14d          # 4-byte Folded Reload
	jge	.LBB21_58
# BB#56:
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	%eax, 20(%rsp)
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB21_59
# BB#57:
	leaq	16(%rsp), %rdi
	movq	%r12, %r13
	movq	%r13, %rsi
	callq	readSyntaxElement_TotalZerosChromaDC
	jmp	.LBB21_60
.LBB21_58:
	movl	4(%rsp), %eax           # 4-byte Reload
	jmp	.LBB21_65
.LBB21_59:
	leaq	16(%rsp), %rdi
	movq	%r12, %r13
	movq	%r13, %rsi
	callq	readSyntaxElement_TotalZeros
.LBB21_60:
	movl	4(%rsp), %eax           # 4-byte Reload
	movl	20(%rsp), %ebp
	cmpl	$2, %r14d
	jl	.LBB21_65
# BB#61:
	testl	%ebp, %ebp
	jle	.LBB21_65
# BB#62:                                # %.preheader
	movslq	%r14d, %rax
	movq	144(%rsp), %rcx
	leaq	-4(%rcx,%rax,4), %rbx
	decl	%r14d
	movq	%r14, %rax
	movl	$6, %r15d
	leaq	16(%rsp), %r14
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	.p2align	4, 0x90
.LBB21_63:                              # =>This Inner Loop Header: Depth=1
	movl	%eax, %r12d
	leal	-1(%rbp), %eax
	cmpl	$7, %eax
	cmovgel	%r15d, %eax
	movl	%eax, 20(%rsp)
	movq	%r14, %rdi
	movq	%r13, %rsi
	callq	readSyntaxElement_Run
	movl	20(%rsp), %eax
	movl	%eax, (%rbx)
	subl	%eax, %ebp
	leal	-1(%r12), %eax
	cmpl	$1, %r12d
	je	.LBB21_65
# BB#64:                                #   in Loop: Header=BB21_63 Depth=1
	addq	$-4, %rbx
	testl	%ebp, %ebp
	jne	.LBB21_63
.LBB21_65:                              # %.thread
	cltq
	movq	144(%rsp), %rcx
	movl	%ebp, (%rcx,%rax,4)
.LBB21_66:
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end21:
	.size	readCoeff4x4_CAVLC, .Lfunc_end21-readCoeff4x4_CAVLC
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI21_0:
	.quad	.LBB21_2
	.quad	.LBB21_8
	.quad	.LBB21_3
	.quad	.LBB21_5
	.quad	.LBB21_5
	.quad	.LBB21_5
	.quad	.LBB21_6
	.quad	.LBB21_7

	.text
	.globl	CalculateQuant8Param
	.p2align	4, 0x90
	.type	CalculateQuant8Param,@function
CalculateQuant8Param:                   # @CalculateQuant8Param
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi197:
	.cfi_def_cfa_offset 16
.Lcfi198:
	.cfi_offset %rbx, -16
	movq	qmatrix+48(%rip), %r11
	movq	qmatrix+56(%rip), %rcx
	movl	$dequant_coef8+28, %r8d
	xorl	%r9d, %r9d
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB22_1:                               # %.preheader31
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_2 Depth 2
	movq	$-8, %rdx
	movq	%r9, %rsi
	movq	%r8, %rdi
	.p2align	4, 0x90
.LBB22_2:                               # %.preheader
                                        #   Parent Loop BB22_1 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-28(%rdi), %eax
	movl	32(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra(%rsi)
	imull	32(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter(%rsi)
	movl	-24(%rdi), %eax
	movl	64(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra+32(%rsi)
	imull	64(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter+32(%rsi)
	movl	-20(%rdi), %eax
	movl	96(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra+64(%rsi)
	imull	96(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter+64(%rsi)
	movl	-16(%rdi), %eax
	movl	128(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra+96(%rsi)
	imull	128(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter+96(%rsi)
	movl	-12(%rdi), %eax
	movl	160(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra+128(%rsi)
	imull	160(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter+128(%rsi)
	movl	-8(%rdi), %eax
	movl	192(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra+160(%rsi)
	imull	192(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter+160(%rsi)
	movl	-4(%rdi), %eax
	movl	224(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra+192(%rsi)
	imull	224(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter+192(%rsi)
	movl	(%rdi), %eax
	movl	256(%r11,%rdx,4), %ebx
	imull	%eax, %ebx
	movl	%ebx, InvLevelScale8x8Luma_Intra+224(%rsi)
	imull	256(%rcx,%rdx,4), %eax
	movl	%eax, InvLevelScale8x8Luma_Inter+224(%rsi)
	addq	$32, %rdi
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB22_2
# BB#3:                                 #   in Loop: Header=BB22_1 Depth=1
	incq	%r10
	addq	$256, %r8               # imm = 0x100
	addq	$256, %r9               # imm = 0x100
	cmpq	$6, %r10
	jne	.LBB22_1
# BB#4:
	popq	%rbx
	retq
.Lfunc_end22:
	.size	CalculateQuant8Param, .Lfunc_end22-CalculateQuant8Param
	.cfi_endproc

	.globl	readLumaCoeff8x8_CABAC
	.p2align	4, 0x90
	.type	readLumaCoeff8x8_CABAC,@function
readLumaCoeff8x8_CABAC:                 # @readLumaCoeff8x8_CABAC
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi199:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi200:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi201:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi202:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi203:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi204:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi205:
	.cfi_def_cfa_offset 160
.Lcfi206:
	.cfi_offset %rbx, -56
.Lcfi207:
	.cfi_offset %r12, -48
.Lcfi208:
	.cfi_offset %r13, -40
.Lcfi209:
	.cfi_offset %r14, -32
.Lcfi210:
	.cfi_offset %r15, -24
.Lcfi211:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	5600(%r14), %rdi
	movslq	4(%r14), %rax
	imulq	$408, %rax, %rbp        # imm = 0x198
	movl	300(%rdi,%rbp), %r10d
	movq	5592(%r14), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	28(%rax), %r8
	movl	5884(%r14), %r11d
	addl	28(%r14), %r11d
	je	.LBB23_2
# BB#1:
	xorl	%r9d, %r9d
	jmp	.LBB23_3
.LBB23_2:
	cmpl	$1, 5920(%r14)
	sete	%r9b
.LBB23_3:
	movl	40(%rdi,%rbp), %ecx
	addl	$-9, %ecx
	cmpl	$5, %ecx
	ja	.LBB23_4
# BB#5:                                 # %switch.lookup
	movslq	%ecx, %rsi
	movq	.Lswitch.table(,%rsi,8), %rax
	jmp	.LBB23_6
.LBB23_4:
	movl	$InvLevelScale8x8Luma_Inter, %eax
.LBB23_6:
	movq	%rax, 16(%rsp)          # 8-byte Spill
	cmpl	$0, 5584(%r14)
	je	.LBB23_8
# BB#7:
	movl	$FIELD_SCAN8x8, %r13d
	jmp	.LBB23_9
.LBB23_8:
	cmpl	$0, 356(%rdi,%rbp)
	movl	$FIELD_SCAN8x8, %esi
	movl	$SNGL_SCAN8x8, %r13d
	cmovneq	%rsi, %r13
.LBB23_9:
	cmpl	$6, %ecx
	sbbb	%bl, %bl
	movb	$59, %al
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %al
	andb	%bl, %al
	andb	$1, %al
	movzbl	%al, %esi
	movl	%esi, 5616(%r14)
	btl	%edx, %r10d
	jae	.LBB23_19
# BB#10:
	movl	%edx, %eax
	andl	$1, %eax
	leal	(,%rax,8), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	shll	$2, %edx
	movl	%edx, %r15d
	andl	$-8, %r15d
	addl	%eax, %eax
	movl	%eax, 5608(%r14)
	movl	%r15d, %ecx
	sarl	$2, %ecx
	movl	%ecx, 5612(%r14)
	subl	%eax, %edx
	movl	$51, %eax
	movl	%edx, %ecx
	shll	%cl, %eax
	cltq
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leaq	304(%rdi,%rbp), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testb	%r9b, %r9b
	je	.LBB23_11
# BB#15:                                # %.preheader.preheader
	movl	$-1, %ebp
	movl	$1, %ebx
	leaq	(%r8,%r8,4), %r12
	shlq	$4, %r12
	jmp	.LBB23_16
	.p2align	4, 0x90
.LBB23_18:                              # %..preheader_crit_edge
                                        #   in Loop: Header=BB23_16 Depth=1
	movl	5616(%r14), %esi
	incl	%ebx
.LBB23_16:                              # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, 80(%rsp)
	xorl	%eax, %eax
	cmpl	$1, %ebx
	setne	%al
	cmpl	$1, %esi
	leal	7(%rax,%rax), %ecx
	leal	12(%rax,%rax), %eax
	cmovel	%ecx, %eax
	movl	%eax, 56(%rsp)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	40(%rcx), %rcx
	movslq	assignSE2partition(%r12,%rax,4), %rax
	imulq	$56, %rax, %rax
	leaq	(%rcx,%rax), %rdx
	movq	$readRunLevel_CABAC, 96(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r14, %rsi
	callq	*48(%rcx,%rax)
	movl	60(%rsp), %eax
	testl	%eax, %eax
	je	.LBB23_19
# BB#17:                                #   in Loop: Header=BB23_16 Depth=1
	movl	64(%rsp), %ecx
	leal	1(%rbp,%rcx), %ebp
	movslq	%ebp, %rcx
	movzbl	(%r13,%rcx,2), %edx
	movzbl	1(%r13,%rcx,2), %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	orq	%rsi, (%rdi)
	movslq	%r15d, %rsi
	addq	%rcx, %rsi
	movl	12(%rsp), %ecx          # 4-byte Reload
	addq	%rdx, %rcx
	shlq	$6, %rsi
	addq	%r14, %rsi
	movl	%eax, 1384(%rsi,%rcx,4)
	cmpl	$64, %ebx
	jle	.LBB23_18
	jmp	.LBB23_19
.LBB23_11:                              # %.preheader142.preheader
	movslq	%r11d, %rax
	imulq	$715827883, %rax, %rbx  # imm = 0x2AAAAAAB
	movq	%rbx, %rcx
	shrq	$63, %rcx
	shrq	$32, %rbx
	addl	%ecx, %ebx
	leal	(%rbx,%rbx), %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	cltq
	movl	$-1, %r12d
	movl	$1, %ebp
	leaq	(%r8,%r8,4), %rcx
	shlq	$4, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	shlq	$8, %rax
	addq	%rax, 16(%rsp)          # 8-byte Folded Spill
	jmp	.LBB23_12
	.p2align	4, 0x90
.LBB23_14:                              # %..preheader142_crit_edge
                                        #   in Loop: Header=BB23_12 Depth=1
	movl	5616(%r14), %esi
	incl	%ebp
.LBB23_12:                              # %.preheader142
                                        # =>This Inner Loop Header: Depth=1
	movl	$2, 80(%rsp)
	xorl	%eax, %eax
	cmpl	$1, %ebp
	setne	%al
	cmpl	$1, %esi
	leal	7(%rax,%rax), %ecx
	leal	12(%rax,%rax), %eax
	cmovel	%ecx, %eax
	movl	%eax, 56(%rsp)
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	40(%rcx), %rcx
	movq	48(%rsp), %rdx          # 8-byte Reload
	movslq	assignSE2partition(%rdx,%rax,4), %rax
	imulq	$56, %rax, %rax
	leaq	(%rcx,%rax), %rdx
	movq	$readRunLevel_CABAC, 96(%rsp)
	leaq	56(%rsp), %rdi
	movq	%r14, %rsi
	callq	*48(%rcx,%rax)
	movl	60(%rsp), %eax
	testl	%eax, %eax
	je	.LBB23_19
# BB#13:                                #   in Loop: Header=BB23_12 Depth=1
	movl	64(%rsp), %ecx
	leal	1(%r12,%rcx), %r12d
	movslq	%r12d, %rcx
	movzbl	(%r13,%rcx,2), %edx
	movzbl	1(%r13,%rcx,2), %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdi          # 8-byte Reload
	orq	%rsi, (%rdi)
	movslq	%r15d, %rsi
	addq	%rcx, %rsi
	shlq	$5, %rcx
	addq	16(%rsp), %rcx          # 8-byte Folded Reload
	imull	(%rcx,%rdx,4), %eax
	movl	%ebx, %ecx
	shll	%cl, %eax
	addl	$32, %eax
	sarl	$6, %eax
	movl	12(%rsp), %ecx          # 4-byte Reload
	addq	%rdx, %rcx
	shlq	$6, %rsi
	addq	%r14, %rsi
	movl	%eax, 1384(%rsi,%rcx,4)
	cmpl	$64, %ebp
	jle	.LBB23_14
.LBB23_19:                              # %.loopexit
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end23:
	.size	readLumaCoeff8x8_CABAC, .Lfunc_end23-readLumaCoeff8x8_CABAC
	.cfi_endproc

	.globl	decode_ipcm_mb
	.p2align	4, 0x90
	.type	decode_ipcm_mb,@function
decode_ipcm_mb:                         # @decode_ipcm_mb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi212:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi213:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi215:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi216:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi217:
	.cfi_def_cfa_offset 56
.Lcfi218:
	.cfi_offset %rbx, -56
.Lcfi219:
	.cfi_offset %r12, -48
.Lcfi220:
	.cfi_offset %r13, -40
.Lcfi221:
	.cfi_offset %r14, -32
.Lcfi222:
	.cfi_offset %r15, -24
.Lcfi223:
	.cfi_offset %rbp, -16
	movq	5600(%rdi), %rax
	movq	%rax, -16(%rsp)         # 8-byte Spill
	movl	4(%rdi), %eax
	movq	%rax, -24(%rsp)         # 8-byte Spill
	movq	dec_picture(%rip), %rdx
	movslq	84(%rdi), %rax
	movslq	80(%rdi), %r10
	shlq	$3, %r10
	addq	316920(%rdx), %r10
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB24_1:                               # %.preheader70
                                        # =>This Inner Loop Header: Depth=1
	movl	%ebp, %edx
	sarl	$2, %edx
	movslq	%edx, %rbx
	movl	%ebp, %edx
	andl	$3, %edx
	movq	(%r10,%rbp,8), %rsi
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$8, %rbx
	addq	%rdi, %rbx
	shlq	$4, %rdx
	movzwl	2408(%rdx,%rbx), %ecx
	movw	%cx, (%rsi,%rax,2)
	movzwl	2412(%rdx,%rbx), %ecx
	movw	%cx, 2(%rsi,%rax,2)
	movzwl	2416(%rdx,%rbx), %ecx
	movw	%cx, 4(%rsi,%rax,2)
	movzwl	2420(%rdx,%rbx), %ecx
	movw	%cx, 6(%rsi,%rax,2)
	movzwl	2472(%rdx,%rbx), %ecx
	movw	%cx, 8(%rsi,%rax,2)
	movzwl	2476(%rdx,%rbx), %ecx
	movw	%cx, 10(%rsi,%rax,2)
	movzwl	2480(%rdx,%rbx), %ecx
	movw	%cx, 12(%rsi,%rax,2)
	movzwl	2484(%rdx,%rbx), %ecx
	movw	%cx, 14(%rsi,%rax,2)
	movzwl	2536(%rdx,%rbx), %ecx
	movw	%cx, 16(%rsi,%rax,2)
	movzwl	2540(%rdx,%rbx), %ecx
	movw	%cx, 18(%rsi,%rax,2)
	movzwl	2544(%rdx,%rbx), %ecx
	movw	%cx, 20(%rsi,%rax,2)
	movzwl	2548(%rdx,%rbx), %ecx
	movw	%cx, 22(%rsi,%rax,2)
	movzwl	2600(%rdx,%rbx), %ecx
	movw	%cx, 24(%rsi,%rax,2)
	movzwl	2604(%rdx,%rbx), %ecx
	movw	%cx, 26(%rsi,%rax,2)
	movzwl	2608(%rdx,%rbx), %ecx
	movw	%cx, 28(%rsi,%rax,2)
	movzwl	2612(%rdx,%rbx), %ecx
	incq	%rbp
	cmpq	$16, %rbp
	movw	%cx, 30(%rsi,%rax,2)
	jne	.LBB24_1
# BB#2:
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB24_23
# BB#3:                                 # %.preheader69
	movl	5936(%rdi), %ecx
	movl	%ecx, -52(%rsp)         # 4-byte Spill
	testl	%ecx, %ecx
	jle	.LBB24_23
# BB#4:                                 # %.preheader68.lr.ph
	movslq	5932(%rdi), %r13
	testq	%r13, %r13
	jle	.LBB24_14
# BB#5:                                 # %.preheader68.us.preheader
	movq	316928(%rax), %rax
	movq	(%rax), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movslq	88(%rdi), %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	movslq	96(%rdi), %rcx
	movl	%r13d, %r12d
	andl	$1, %r12d
	movq	%rcx, %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	leaq	1(%rcx), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB24_6:                               # %.preheader68.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_11 Depth 2
	movl	%r14d, %eax
	sarl	$2, %eax
	movl	%r14d, %r8d
	andl	$3, %r8d
	movslq	%r14d, %rcx
	addq	-40(%rsp), %rcx         # 8-byte Folded Reload
	testq	%r12, %r12
	cltq
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%rcx,8), %rdx
	jne	.LBB24_8
# BB#7:                                 #   in Loop: Header=BB24_6 Depth=1
	xorl	%ecx, %ecx
	cmpl	$1, %r13d
	jne	.LBB24_10
	jmp	.LBB24_12
	.p2align	4, 0x90
.LBB24_8:                               #   in Loop: Header=BB24_6 Depth=1
	leaq	(%rax,%rax,2), %rcx
	shlq	$8, %rcx
	addq	%rdi, %rcx
	movq	%r8, %rbp
	shlq	$4, %rbp
	movzwl	2664(%rbp,%rcx), %ecx
	movq	-48(%rsp), %rsi         # 8-byte Reload
	movw	%cx, (%rdx,%rsi,2)
	movl	$1, %ecx
	cmpl	$1, %r13d
	je	.LBB24_12
.LBB24_10:                              # %.preheader68.us.new
                                        #   in Loop: Header=BB24_6 Depth=1
	movq	-8(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdx,%rsi,2), %rdx
	.p2align	4, 0x90
.LBB24_11:                              #   Parent Loop BB24_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %ebp
	sarl	$2, %ebp
	movslq	%ebp, %rbp
	shlq	$6, %rbp
	movl	%ecx, %r9d
	andl	$3, %r9d
	leaq	(%rax,%rax,2), %r11
	shlq	$8, %r11
	addq	%rdi, %r11
	leaq	1(%rcx), %r10
	movl	%r10d, %r15d
	sarl	$2, %r15d
	movslq	%r15d, %rbx
	shlq	$6, %rbx
	leaq	2408(%rbx,%r11), %rbx
	leaq	2408(%rbp,%r11), %rbp
	movq	%r8, %rsi
	shlq	$4, %rsi
	addq	%rsi, %rbp
	movzwl	256(%rbp,%r9,4), %ebp
	movw	%bp, -2(%rdx,%rcx,2)
	andl	$3, %r10d
	addq	%rbx, %rsi
	movzwl	256(%rsi,%r10,4), %esi
	movw	%si, (%rdx,%rcx,2)
	addq	$2, %rcx
	cmpq	%r13, %rcx
	jl	.LBB24_11
.LBB24_12:                              # %._crit_edge79.us
                                        #   in Loop: Header=BB24_6 Depth=1
	incl	%r14d
	cmpl	-52(%rsp), %r14d        # 4-byte Folded Reload
	jl	.LBB24_6
# BB#13:                                # %.preheader67
	cmpl	$0, -52(%rsp)           # 4-byte Folded Reload
	jle	.LBB24_23
.LBB24_14:                              # %.preheader66.lr.ph
	movslq	5932(%rdi), %r9
	testq	%r9, %r9
	jle	.LBB24_23
# BB#15:                                # %.preheader66.us.preheader
	movq	dec_picture(%rip), %rax
	movq	316928(%rax), %rax
	movq	8(%rax), %rax
	movq	%rax, -32(%rsp)         # 8-byte Spill
	movslq	88(%rdi), %r12
	movslq	96(%rdi), %rcx
	movl	%r9d, %r13d
	andl	$1, %r13d
	movq	%rcx, %rax
	movq	%rax, -40(%rsp)         # 8-byte Spill
	leaq	1(%rcx), %rax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB24_16:                              # %.preheader66.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_21 Depth 2
	movl	%r8d, %eax
	sarl	$2, %eax
	addl	$2, %eax
	movl	%r8d, %r11d
	andl	$3, %r11d
	movslq	%r8d, %rcx
	addq	%r12, %rcx
	testq	%r13, %r13
	movslq	%eax, %r15
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rcx,8), %rcx
	jne	.LBB24_18
# BB#17:                                #   in Loop: Header=BB24_16 Depth=1
	xorl	%eax, %eax
	cmpl	$1, %r9d
	jne	.LBB24_20
	jmp	.LBB24_22
	.p2align	4, 0x90
.LBB24_18:                              #   in Loop: Header=BB24_16 Depth=1
	leaq	(%r15,%r15,2), %rax
	shlq	$8, %rax
	addq	%rdi, %rax
	movq	%r11, %rbx
	shlq	$4, %rbx
	movzwl	2664(%rbx,%rax), %eax
	movq	-40(%rsp), %rdx         # 8-byte Reload
	movw	%ax, (%rcx,%rdx,2)
	movl	$1, %eax
	cmpl	$1, %r9d
	je	.LBB24_22
.LBB24_20:                              # %.preheader66.us.new
                                        #   in Loop: Header=BB24_16 Depth=1
	movq	-48(%rsp), %rdx         # 8-byte Reload
	leaq	(%rcx,%rdx,2), %rcx
	.p2align	4, 0x90
.LBB24_21:                              #   Parent Loop BB24_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%eax, %ebx
	sarl	$2, %ebx
	movslq	%ebx, %rbx
	shlq	$6, %rbx
	movl	%eax, %esi
	andl	$3, %esi
	leaq	(%r15,%r15,2), %rbp
	shlq	$8, %rbp
	addq	%rdi, %rbp
	leaq	1(%rax), %r14
	movl	%r14d, %r10d
	sarl	$2, %r10d
	movslq	%r10d, %rdx
	shlq	$6, %rdx
	leaq	2408(%rdx,%rbp), %rdx
	leaq	2408(%rbx,%rbp), %rbp
	movq	%r11, %rbx
	shlq	$4, %rbx
	addq	%rbx, %rbp
	movzwl	256(%rbp,%rsi,4), %esi
	movw	%si, -2(%rcx,%rax,2)
	andl	$3, %r14d
	addq	%rdx, %rbx
	movzwl	256(%rbx,%r14,4), %edx
	movw	%dx, (%rcx,%rax,2)
	addq	$2, %rax
	cmpq	%r9, %rax
	jl	.LBB24_21
.LBB24_22:                              # %._crit_edge75.us
                                        #   in Loop: Header=BB24_16 Depth=1
	incl	%r8d
	cmpl	-52(%rsp), %r8d         # 4-byte Folded Reload
	jl	.LBB24_16
.LBB24_23:                              # %.loopexit
	imulq	$408, -24(%rsp), %r8    # 8-byte Folded Reload
                                        # imm = 0x198
	movq	-16(%rsp), %r9          # 8-byte Reload
	movl	$0, (%r9,%r8)
	movq	img(%rip), %rsi
	movq	dec_picture(%rip), %rbp
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	subl	5888(%rsi), %ebx
	movl	317076(%rbp), %ecx
	cmpl	%ebx, %ecx
	cmovgel	%ecx, %ebx
	cmpl	$52, %ebx
	movl	$51, %edx
	movl	$51, %ecx
	cmovll	%ebx, %ecx
	leaq	4(%r9,%r8), %r10
	movl	%ecx, 4(%r9,%r8)
	testl	%ecx, %ecx
	js	.LBB24_25
# BB#24:
	movslq	%ecx, %rcx
	movzbl	QP_SCALE_CR(%rcx), %ebx
.LBB24_25:
	movl	%ebx, (%r10)
	subl	5888(%rsi), %eax
	movl	317080(%rbp), %ecx
	cmpl	%eax, %ecx
	cmovgel	%ecx, %eax
	cmpl	$52, %eax
	cmovll	%eax, %edx
	leaq	8(%r9,%r8), %rcx
	movl	%edx, 8(%r9,%r8)
	testl	%edx, %edx
	js	.LBB24_27
# BB#26:
	movslq	%edx, %rax
	movzbl	QP_SCALE_CR(%rax), %eax
.LBB24_27:                              # %set_chroma_qp.exit
	movl	%eax, (%rcx)
	cmpl	$-3, 5924(%rdi)
	jl	.LBB24_39
# BB#28:                                # %.lr.ph
	movq	5560(%rdi), %rax
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB24_29:                              # =>This Inner Loop Header: Depth=1
	movl	4(%rdi), %ecx
	movq	(%rax,%rcx,8), %rcx
	movq	(%rcx), %rcx
	movl	$16, 4(%rcx,%rdx,4)
	movslq	5924(%rdi), %rcx
	leaq	3(%rcx), %rsi
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB24_29
# BB#30:                                # %._crit_edge
	cmpl	$-3, %ecx
	jl	.LBB24_39
# BB#31:                                # %.lr.ph.1
	movq	5560(%rdi), %rax
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB24_32:                              # =>This Inner Loop Header: Depth=1
	movl	4(%rdi), %ecx
	movq	(%rax,%rcx,8), %rcx
	movq	8(%rcx), %rcx
	movl	$16, 4(%rcx,%rdx,4)
	movslq	5924(%rdi), %rcx
	leaq	3(%rcx), %rsi
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB24_32
# BB#33:                                # %._crit_edge.1
	cmpl	$-3, %ecx
	jl	.LBB24_39
# BB#34:                                # %.lr.ph.2
	movq	5560(%rdi), %rax
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB24_35:                              # =>This Inner Loop Header: Depth=1
	movl	4(%rdi), %ecx
	movq	(%rax,%rcx,8), %rcx
	movq	16(%rcx), %rcx
	movl	$16, 4(%rcx,%rdx,4)
	movslq	5924(%rdi), %rcx
	leaq	3(%rcx), %rsi
	incq	%rdx
	cmpq	%rsi, %rdx
	jl	.LBB24_35
# BB#36:                                # %._crit_edge.2
	cmpl	$-3, %ecx
	jl	.LBB24_39
# BB#37:                                # %.lr.ph.3
	movq	5560(%rdi), %rax
	movq	$-1, %rdx
	.p2align	4, 0x90
.LBB24_38:                              # =>This Inner Loop Header: Depth=1
	movl	4(%rdi), %ecx
	movq	(%rax,%rcx,8), %rcx
	movq	24(%rcx), %rcx
	movl	$16, 4(%rcx,%rdx,4)
	movslq	5924(%rdi), %rcx
	addq	$3, %rcx
	incq	%rdx
	cmpq	%rcx, %rdx
	jl	.LBB24_38
.LBB24_39:                              # %._crit_edge.3
	movl	$0, 360(%r9,%r8)
	movq	$65535, 304(%r9,%r8)    # imm = 0xFFFF
	movl	$0, last_dquant(%rip)
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	decode_ipcm_mb, .Lfunc_end24-decode_ipcm_mb
	.cfi_endproc

	.globl	decode_one_macroblock
	.p2align	4, 0x90
	.type	decode_one_macroblock,@function
decode_one_macroblock:                  # @decode_one_macroblock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi224:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi225:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi226:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi227:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi228:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi229:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Lcfi230:
	.cfi_def_cfa_offset 672
.Lcfi231:
	.cfi_offset %rbx, -56
.Lcfi232:
	.cfi_offset %r12, -48
.Lcfi233:
	.cfi_offset %r13, -40
.Lcfi234:
	.cfi_offset %r14, -32
.Lcfi235:
	.cfi_offset %r15, -24
.Lcfi236:
	.cfi_offset %rbp, -16
	movq	5600(%rdi), %r12
	movl	4(%rdi), %ebp
	movl	44(%rdi), %r8d
	cmpl	$3, %r8d
	jne	.LBB25_3
# BB#1:
	imulq	$408, %rbp, %rax        # imm = 0x198
	movl	40(%r12,%rax), %ecx
	addl	$-9, %ecx
	movb	$1, %al
	cmpl	$6, %ecx
	jae	.LBB25_6
# BB#2:                                 # %switch.lookup
	movb	$12, %al
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %al
	andb	$1, %al
	jmp	.LBB25_6
.LBB25_3:                               # %thread-pre-split
	cmpl	$4, %r8d
	jne	.LBB25_5
# BB#4:
	imulq	$408, %rbp, %rax        # imm = 0x198
	cmpl	$12, 40(%r12,%rax)
	sete	%al
	jmp	.LBB25_6
.LBB25_5:
	xorl	%eax, %eax
.LBB25_6:
	movl	$0, 380(%rsp)
	movl	$0, 376(%rsp)
	movl	5624(%rdi), %edx
	testl	%edx, %edx
	je	.LBB25_8
# BB#7:
	imulq	$408, %rbp, %rcx        # imm = 0x198
	cmpl	$0, 356(%r12,%rcx)
	setne	%cl
	movl	%ecx, 100(%rsp)         # 4-byte Spill
	jmp	.LBB25_9
.LBB25_8:
	movl	$0, 100(%rsp)           # 4-byte Folded Spill
.LBB25_9:
	imulq	$408, %rbp, %r13        # imm = 0x198
	movl	40(%r12,%r13), %r9d
	movl	396(%r12,%r13), %ecx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	movq	dec_picture(%rip), %rsi
	movslq	317044(%rsi), %rcx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	cmpl	$3, %r8d
	jne	.LBB25_11
# BB#10:
	cmpl	$10, %r9d
	setne	%bl
	orb	%bl, %al
.LBB25_11:                              # %._crit_edge2340
	movzbl	%al, %eax
	movl	%eax, 308(%rsp)         # 4-byte Spill
	cmpl	$14, %r9d
	jne	.LBB25_14
# BB#12:
	callq	decode_ipcm_mb
.LBB25_13:
	xorl	%eax, %eax
	jmp	.LBB25_420
.LBB25_14:
	movzbl	100(%rsp), %eax         # 1-byte Folded Reload
	movl	%eax, 36(%rsp)          # 4-byte Spill
	testl	%eax, %eax
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movq	%rbp, 152(%rsp)         # 8-byte Spill
	je	.LBB25_17
# BB#15:
	testb	$1, %bpl
	movq	Co_located(%rip), %rdi
	jne	.LBB25_18
# BB#16:
	leaq	3232(%rdi), %rbp
	leaq	3216(%rdi), %rax
	leaq	3240(%rdi), %rbx
	addq	$3224, %rdi             # imm = 0xC98
	movl	$2, %ecx
	jmp	.LBB25_19
.LBB25_17:
	movq	Co_located(%rip), %rdi
	leaq	1624(%rdi), %rbx
	leaq	1616(%rdi), %rbp
	leaq	1600(%rdi), %rax
	leaq	1608(%rdi), %rdi
	movl	316876(%rsi), %ecx
	movl	%ecx, 104(%rsp)         # 4-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	jmp	.LBB25_20
.LBB25_18:
	leaq	4848(%rdi), %rbp
	leaq	4832(%rdi), %rax
	leaq	4856(%rdi), %rbx
	addq	$4840, %rdi             # imm = 0x12E8
	movl	$4, %ecx
.LBB25_19:
	movq	%rcx, 272(%rsp)         # 8-byte Spill
	movl	316876(%rsi), %ecx
	sarl	%ecx
	movl	%ecx, 104(%rsp)         # 4-byte Spill
.LBB25_20:
	movq	(%rdi), %rcx
	movq	%rcx, 560(%rsp)         # 8-byte Spill
	movq	(%rax), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	(%rbp), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	movq	(%rbx), %rax
	movq	%rax, 464(%rsp)         # 8-byte Spill
	testl	%edx, %edx
	je	.LBB25_38
# BB#21:
	movslq	272(%rsp), %rdx         # 4-byte Folded Reload
	movl	listXsize(,%rdx,4), %eax
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB25_56
# BB#22:                                # %.preheader2055.preheader
	testl	%eax, %eax
	jle	.LBB25_30
# BB#23:                                # %.lr.ph2173
	movq	listX(,%rdx,8), %rax
	movq	152(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	andl	$1, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB25_24:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdi,8), %rbp
	movl	$0, 316896(%rbp)
	movl	(%rbp), %ebx
	testl	%esi, %esi
	jne	.LBB25_27
# BB#25:                                #   in Loop: Header=BB25_24 Depth=1
	cmpl	$2, %ebx
	jne	.LBB25_29
# BB#26:                                #   in Loop: Header=BB25_24 Depth=1
	movl	$-2, 316896(%rbp)
	jmp	.LBB25_29
	.p2align	4, 0x90
.LBB25_27:                              #   in Loop: Header=BB25_24 Depth=1
	cmpl	$1, %ebx
	jne	.LBB25_29
# BB#28:                                #   in Loop: Header=BB25_24 Depth=1
	movl	$2, 316896(%rbp)
.LBB25_29:                              # %.thread2724
                                        #   in Loop: Header=BB25_24 Depth=1
	incq	%rdi
	movslq	listXsize(,%rdx,4), %rbp
	cmpq	%rbp, %rdi
	jl	.LBB25_24
.LBB25_30:                              # %.preheader2055.1
	cmpl	$0, listXsize+4(,%rdx,4)
	jle	.LBB25_62
# BB#31:                                # %.lr.ph2173.1
	movq	listX+8(,%rdx,8), %rax
	movq	152(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	andl	$1, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB25_32:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdi,8), %rbp
	movl	$0, 316896(%rbp)
	movl	(%rbp), %ebx
	testl	%esi, %esi
	jne	.LBB25_35
# BB#33:                                #   in Loop: Header=BB25_32 Depth=1
	cmpl	$2, %ebx
	jne	.LBB25_37
# BB#34:                                #   in Loop: Header=BB25_32 Depth=1
	movl	$-2, 316896(%rbp)
	jmp	.LBB25_37
	.p2align	4, 0x90
.LBB25_35:                              #   in Loop: Header=BB25_32 Depth=1
	cmpl	$1, %ebx
	jne	.LBB25_37
# BB#36:                                #   in Loop: Header=BB25_32 Depth=1
	movl	$2, 316896(%rbp)
.LBB25_37:                              # %.thread2729
                                        #   in Loop: Header=BB25_32 Depth=1
	incq	%rdi
	movslq	listXsize+4(,%rdx,4), %rbp
	cmpq	%rbp, %rdi
	jl	.LBB25_32
	jmp	.LBB25_62
.LBB25_38:                              # %.preheader2053
	movslq	272(%rsp), %rdx         # 4-byte Folded Reload
	cmpl	$0, listXsize(,%rdx,4)
	jle	.LBB25_47
# BB#39:                                # %.lr.ph2169
	movq	listX(,%rdx,8), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	5584(%rsi), %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB25_40:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdi,8), %rbp
	movl	$0, 316896(%rbp)
	cmpl	$2, %esi
	je	.LBB25_44
# BB#41:                                #   in Loop: Header=BB25_40 Depth=1
	cmpl	$1, %esi
	jne	.LBB25_46
# BB#42:                                #   in Loop: Header=BB25_40 Depth=1
	cmpl	$1, (%rbp)
	je	.LBB25_46
# BB#43:                                #   in Loop: Header=BB25_40 Depth=1
	movl	$-2, 316896(%rbp)
	jmp	.LBB25_46
	.p2align	4, 0x90
.LBB25_44:                              #   in Loop: Header=BB25_40 Depth=1
	cmpl	$2, (%rbp)
	je	.LBB25_46
# BB#45:                                #   in Loop: Header=BB25_40 Depth=1
	movl	$2, 316896(%rbp)
	.p2align	4, 0x90
.LBB25_46:                              # %thread-pre-split1945.thread
                                        #   in Loop: Header=BB25_40 Depth=1
	incq	%rdi
	movslq	listXsize(,%rdx,4), %rbp
	cmpq	%rbp, %rdi
	jl	.LBB25_40
.LBB25_47:                              # %.preheader2052.1
	cmpl	$0, listXsize+4(,%rdx,4)
	jle	.LBB25_62
# BB#48:                                # %.lr.ph2169.1
	movq	listX+8(,%rdx,8), %rax
	movq	8(%rsp), %rsi           # 8-byte Reload
	movl	5584(%rsi), %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB25_49:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rdi,8), %rbp
	movl	$0, 316896(%rbp)
	cmpl	$2, %esi
	je	.LBB25_53
# BB#50:                                #   in Loop: Header=BB25_49 Depth=1
	cmpl	$1, %esi
	jne	.LBB25_55
# BB#51:                                #   in Loop: Header=BB25_49 Depth=1
	cmpl	$1, (%rbp)
	je	.LBB25_55
# BB#52:                                #   in Loop: Header=BB25_49 Depth=1
	movl	$-2, 316896(%rbp)
	jmp	.LBB25_55
	.p2align	4, 0x90
.LBB25_53:                              #   in Loop: Header=BB25_49 Depth=1
	cmpl	$2, (%rbp)
	je	.LBB25_55
# BB#54:                                #   in Loop: Header=BB25_49 Depth=1
	movl	$2, 316896(%rbp)
	.p2align	4, 0x90
.LBB25_55:                              # %thread-pre-split1945.1.thread
                                        #   in Loop: Header=BB25_49 Depth=1
	incq	%rdi
	movslq	listXsize+4(,%rdx,4), %rbp
	cmpq	%rbp, %rdi
	jl	.LBB25_49
	jmp	.LBB25_62
.LBB25_56:                              # %.preheader2058.preheader
	testl	%eax, %eax
	jle	.LBB25_59
# BB#57:                                # %.lr.ph2177
	movq	listX(,%rdx,8), %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_58:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rsi,8), %rdi
	movl	$0, 316896(%rdi)
	incq	%rsi
	movslq	listXsize(,%rdx,4), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB25_58
.LBB25_59:                              # %.preheader2058.1
	cmpl	$0, listXsize+4(,%rdx,4)
	jle	.LBB25_62
# BB#60:                                # %.lr.ph2177.1
	movq	listX+8(,%rdx,8), %rax
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_61:                              # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rsi,8), %rdi
	movl	$0, 316896(%rdi)
	incq	%rsi
	movslq	listXsize+4(,%rdx,4), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB25_61
.LBB25_62:                              # %.loopexit2054
	orl	$4, %r9d
	cmpl	$14, %r9d
	movq	8(%rsp), %r9            # 8-byte Reload
	jne	.LBB25_64
# BB#63:
	movl	324(%r12,%r13), %esi
	movq	%r9, %rdi
	movq	%r9, %rbp
	callq	intrapred_luma_16x16
	movq	%rbp, %r9
	movl	44(%r9), %r8d
.LBB25_64:
	leaq	40(%r12,%r13), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movb	$-1, %r14b
	cmpl	$1, %r8d
	jne	.LBB25_80
# BB#65:
	cmpl	$0, 40(%r9)
	movq	320(%rsp), %rcx         # 8-byte Reload
	je	.LBB25_82
# BB#66:
	movq	312(%rsp), %r8          # 8-byte Reload
	movl	(%r8), %eax
	testl	%eax, %eax
	je	.LBB25_72
# BB#67:
	cmpl	$8, %eax
	jne	.LBB25_82
# BB#68:
	cmpb	$0, 328(%r12,%r13)
	je	.LBB25_72
# BB#69:
	cmpb	$0, 329(%r12,%r13)
	je	.LBB25_72
# BB#70:
	cmpb	$0, 330(%r12,%r13)
	je	.LBB25_72
# BB#71:
	cmpb	$0, 331(%r12,%r13)
	movb	$-1, %r11b
	jne	.LBB25_149
.LBB25_72:
	movl	4(%r9), %edi
	leaq	192(%rsp), %rcx
	movl	$-1, %esi
	xorl	%edx, %edx
	movq	%r9, %rbx
	callq	getLuma4x4Neighbour
	movl	4(%rbx), %edi
	leaq	384(%rsp), %rcx
	xorl	%esi, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movl	4(%rbx), %edi
	leaq	480(%rsp), %rcx
	movl	$16, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	movl	4(%rbx), %edi
	leaq	504(%rsp), %rcx
	movl	$-1, %esi
	movl	$-1, %edx
	callq	getLuma4x4Neighbour
	cmpl	$0, 5624(%rbx)
	je	.LBB25_83
# BB#73:
	movq	5600(%rbx), %rsi
	movl	4(%rbx), %eax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	movl	192(%rsp), %r10d
	movb	$-1, %r11b
	je	.LBB25_89
# BB#74:
	testl	%r10d, %r10d
	movb	$-1, %r14b
	je	.LBB25_76
# BB#75:
	movslq	196(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	212(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	208(%rsp), %rdx
	movb	(%rcx,%rdx), %r14b
	cmpl	$0, 356(%rsi,%rax)
	sete	%al
	testb	%r14b, %r14b
	setns	%cl
	andb	%al, %cl
	shlb	%cl, %r14b
.LBB25_76:                              # %._crit_edge2344
	movl	384(%rsp), %r8d
	testl	%r8d, %r8d
	je	.LBB25_78
# BB#77:
	movslq	388(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	404(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	400(%rsp), %rdx
	movb	(%rcx,%rdx), %r11b
	cmpl	$0, 356(%rsi,%rax)
	sete	%al
	testb	%r11b, %r11b
	setns	%cl
	andb	%al, %cl
	shlb	%cl, %r11b
.LBB25_78:                              # %._crit_edge2359
	movl	504(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB25_95
# BB#79:
	movslq	508(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	524(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	520(%rsp), %rdx
	movb	(%rcx,%rdx), %dl
	cmpl	$0, 356(%rsi,%rax)
	sete	%al
	testb	%dl, %dl
	setns	%cl
	andb	%al, %cl
	shlb	%cl, %dl
	jmp	.LBB25_96
.LBB25_80:
	movb	$-1, %r11b
	movq	320(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB25_149
.LBB25_82:
	movb	$-1, %r11b
	jmp	.LBB25_149
.LBB25_83:
	movl	192(%rsp), %eax
	movb	$-1, %r11b
	testl	%eax, %eax
	movb	$-1, %r14b
	je	.LBB25_85
# BB#84:
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	212(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	208(%rsp), %rdx
	movb	(%rcx,%rdx), %r14b
.LBB25_85:
	movl	384(%rsp), %ecx
	testl	%ecx, %ecx
	je	.LBB25_87
# BB#86:
	movq	dec_picture(%rip), %rdx
	movq	316952(%rdx), %rdx
	movq	(%rdx), %rdx
	movslq	404(%rsp), %rsi
	movq	(%rdx,%rsi,8), %rdx
	movslq	400(%rsp), %rsi
	movb	(%rdx,%rsi), %r11b
.LBB25_87:
	movl	504(%rsp), %esi
	testl	%esi, %esi
	je	.LBB25_106
# BB#88:
	movq	dec_picture(%rip), %rdx
	movq	316952(%rdx), %rdx
	movq	(%rdx), %rdx
	movslq	524(%rsp), %rdi
	movq	(%rdx,%rdi,8), %rdx
	movslq	520(%rsp), %rdi
	movb	(%rdx,%rdi), %dl
	jmp	.LBB25_107
.LBB25_89:
	testl	%r10d, %r10d
	movb	$-1, %r14b
	je	.LBB25_91
# BB#90:
	movslq	196(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	212(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	208(%rsp), %rdx
	movsbl	(%rcx,%rdx), %r14d
	movl	%r14d, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %r14d
.LBB25_91:                              # %._crit_edge2468
	movl	384(%rsp), %r8d
	testl	%r8d, %r8d
	je	.LBB25_93
# BB#92:
	movslq	388(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	404(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	400(%rsp), %rdx
	movsbl	(%rcx,%rdx), %r11d
	movl	%r11d, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %r11d
.LBB25_93:                              # %._crit_edge2483
	movl	504(%rsp), %r9d
	testl	%r9d, %r9d
	je	.LBB25_117
# BB#94:
	movslq	508(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	524(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	520(%rsp), %rdx
	movsbl	(%rcx,%rdx), %edx
	movl	%edx, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %edx
	jmp	.LBB25_118
.LBB25_95:
	movb	$-1, %dl
.LBB25_96:                              # %._crit_edge2374
	movl	480(%rsp), %eax
	testl	%eax, %eax
	je	.LBB25_98
# BB#97:
	movslq	484(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rdx
	movq	316952(%rdx), %rdx
	movq	(%rdx), %rdx
	movslq	500(%rsp), %rdi
	movq	(%rdx,%rdi,8), %rdx
	movslq	496(%rsp), %rdi
	movb	(%rdx,%rdi), %dl
	cmpl	$0, 356(%rsi,%rcx)
	sete	%bl
	testb	%dl, %dl
	setns	%cl
	andb	%bl, %cl
	shlb	%cl, %dl
.LBB25_98:                              # %._crit_edge2389
	movb	$-1, %dil
	testl	%r10d, %r10d
	movb	$-1, %cl
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	je	.LBB25_100
# BB#99:
	movslq	196(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rbx
	movq	316952(%rbx), %rbx
	movq	8(%rbx), %rbx
	movslq	212(%rsp), %rbp
	movq	(%rbx,%rbp,8), %rbp
	movslq	208(%rsp), %rbx
	movb	(%rbp,%rbx), %bpl
	cmpl	$0, 356(%rsi,%rcx)
	sete	%bl
	testb	%bpl, %bpl
	setns	%cl
	andb	%bl, %cl
	shlb	%cl, %bpl
	movl	%ebp, 112(%rsp)         # 4-byte Spill
.LBB25_100:                             # %._crit_edge2404
	testl	%r8d, %r8d
	je	.LBB25_102
# BB#101:
	movslq	388(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rdi
	movq	316952(%rdi), %rdi
	movq	8(%rdi), %rdi
	movslq	404(%rsp), %rbp
	movq	(%rdi,%rbp,8), %rdi
	movslq	400(%rsp), %rbp
	movb	(%rdi,%rbp), %dil
	cmpl	$0, 356(%rsi,%rcx)
	sete	%bl
	testb	%dil, %dil
	setns	%cl
	andb	%bl, %cl
	shlb	%cl, %dil
.LBB25_102:                             # %._crit_edge2420
	testl	%r9d, %r9d
	je	.LBB25_104
# BB#103:
	movslq	508(%rsp), %rcx
	imulq	$408, %rcx, %rcx        # imm = 0x198
	movq	dec_picture(%rip), %rbp
	movq	316952(%rbp), %rbp
	movq	8(%rbp), %rbp
	movslq	524(%rsp), %rbx
	movq	(%rbp,%rbx,8), %rbp
	movslq	520(%rsp), %rbx
	movb	(%rbp,%rbx), %bpl
	cmpl	$0, 356(%rsi,%rcx)
	sete	%bl
	testb	%bpl, %bpl
	setns	%cl
	andb	%bl, %cl
	shlb	%cl, %bpl
	testl	%eax, %eax
	jne	.LBB25_105
	jmp	.LBB25_128
.LBB25_104:
	movb	$-1, %bpl
	testl	%eax, %eax
	je	.LBB25_128
.LBB25_105:
	movslq	484(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	500(%rsp), %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	496(%rsp), %rbp
	movb	(%rcx,%rbp), %bpl
	cmpl	$0, 356(%rsi,%rax)
	sete	%al
	testb	%bpl, %bpl
	setns	%cl
	andb	%al, %cl
	shlb	%cl, %bpl
	jmp	.LBB25_128
.LBB25_106:
	movb	$-1, %dl
.LBB25_107:
	movl	480(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB25_109
# BB#108:
	movq	dec_picture(%rip), %rdx
	movq	316952(%rdx), %rdx
	movq	(%rdx), %rdx
	movslq	500(%rsp), %rdi
	movq	(%rdx,%rdi,8), %rdx
	movslq	496(%rsp), %rdi
	movb	(%rdx,%rdi), %dl
.LBB25_109:
	movb	$-1, %dil
	testl	%eax, %eax
	movb	$-1, %al
	movl	%eax, 112(%rsp)         # 4-byte Spill
	je	.LBB25_111
# BB#110:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	212(%rsp), %rbp
	movq	(%rax,%rbp,8), %rax
	movslq	208(%rsp), %rbp
	movb	(%rax,%rbp), %al
	movl	%eax, 112(%rsp)         # 4-byte Spill
.LBB25_111:
	testl	%ecx, %ecx
	je	.LBB25_113
# BB#112:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	404(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	400(%rsp), %rcx
	movb	(%rax,%rcx), %dil
.LBB25_113:
	testl	%esi, %esi
	je	.LBB25_115
# BB#114:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	524(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	520(%rsp), %rcx
	movb	(%rax,%rcx), %bpl
	testl	%ebx, %ebx
	jne	.LBB25_116
	jmp	.LBB25_128
.LBB25_115:
	movb	$-1, %bpl
	testl	%ebx, %ebx
	je	.LBB25_128
.LBB25_116:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movslq	500(%rsp), %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	496(%rsp), %rcx
	movb	(%rax,%rcx), %bpl
	jmp	.LBB25_128
.LBB25_117:
	movl	$-1, %edx
.LBB25_118:                             # %._crit_edge2498
	movl	480(%rsp), %ebx
	testl	%ebx, %ebx
	je	.LBB25_120
# BB#119:
	movslq	484(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	500(%rsp), %rdx
	movq	(%rcx,%rdx,8), %rcx
	movslq	496(%rsp), %rdx
	movsbl	(%rcx,%rdx), %edx
	movl	%edx, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %edx
.LBB25_120:                             # %._crit_edge2513
	movb	$-1, %dil
	testl	%r10d, %r10d
	movb	$-1, %al
	movl	%eax, 112(%rsp)         # 4-byte Spill
	je	.LBB25_122
# BB#121:
	movslq	196(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	212(%rsp), %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	208(%rsp), %rbp
	movsbl	(%rcx,%rbp), %ebp
	movl	%ebp, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %ebp
	movl	%ebp, 112(%rsp)         # 4-byte Spill
.LBB25_122:                             # %._crit_edge2528
	testl	%r8d, %r8d
	je	.LBB25_124
# BB#123:
	movslq	388(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	404(%rsp), %rdi
	movq	(%rcx,%rdi,8), %rcx
	movslq	400(%rsp), %rdi
	movsbl	(%rcx,%rdi), %edi
	movl	%edi, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %edi
.LBB25_124:                             # %._crit_edge2544
	testl	%r9d, %r9d
	je	.LBB25_126
# BB#125:
	movslq	508(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	524(%rsp), %rbp
	movq	(%rcx,%rbp,8), %rcx
	movslq	520(%rsp), %rbp
	movsbl	(%rcx,%rbp), %ebp
	movl	%ebp, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %ebp
	testl	%ebx, %ebx
	jne	.LBB25_127
	jmp	.LBB25_128
.LBB25_126:
	movl	$-1, %ebp
	testl	%ebx, %ebx
	je	.LBB25_128
.LBB25_127:
	movslq	484(%rsp), %rax
	imulq	$408, %rax, %rax        # imm = 0x198
	cmpl	$0, 356(%rsi,%rax)
	setne	%al
	movq	dec_picture(%rip), %rcx
	movq	316952(%rcx), %rcx
	movq	8(%rcx), %rcx
	movslq	500(%rsp), %rsi
	movq	(%rcx,%rsi,8), %rcx
	movslq	496(%rsp), %rsi
	movsbl	(%rcx,%rsi), %ebp
	movl	%ebp, %ecx
	shrb	$7, %cl
	orb	%al, %cl
	sarl	%cl, %ebp
.LBB25_128:                             # %._crit_edge2452
	movl	%r14d, %esi
	cmpb	%r11b, %sil
	setg	%cl
	setl	%al
	movl	%r11d, %ebx
	orb	%sil, %bl
	jns	.LBB25_130
# BB#129:                               # %._crit_edge2452
	movl	%ecx, %eax
.LBB25_130:                             # %._crit_edge2452
	testb	%al, %al
	jne	.LBB25_132
# BB#131:                               # %._crit_edge2452
	movb	%r11b, %r14b
.LBB25_132:                             # %._crit_edge2452
	cmpb	%dl, %r14b
	setg	%cl
	setl	%al
	movl	%r14d, %ebx
	orb	%dl, %bl
	jns	.LBB25_134
# BB#133:                               # %._crit_edge2452
	movl	%ecx, %eax
.LBB25_134:                             # %._crit_edge2452
	testb	%al, %al
	movl	112(%rsp), %r11d        # 4-byte Reload
	jne	.LBB25_136
# BB#135:                               # %._crit_edge2452
	movb	%dl, %r14b
.LBB25_136:                             # %._crit_edge2452
	cmpb	%dil, %r11b
	setg	%cl
	setl	%al
	movl	%edi, %edx
	orb	%r11b, %dl
	jns	.LBB25_138
# BB#137:                               # %._crit_edge2452
	movl	%ecx, %eax
.LBB25_138:                             # %._crit_edge2452
	testb	%al, %al
	jne	.LBB25_140
# BB#139:                               # %._crit_edge2452
	movb	%dil, %r11b
.LBB25_140:                             # %._crit_edge2452
	cmpb	%bpl, %r11b
	setg	%cl
	setl	%al
	movl	%r11d, %edx
	orb	%bpl, %dl
	jns	.LBB25_142
# BB#141:                               # %._crit_edge2452
	movl	%ecx, %eax
.LBB25_142:                             # %._crit_edge2452
	testb	%al, %al
	movq	320(%rsp), %rcx         # 8-byte Reload
	jne	.LBB25_144
# BB#143:                               # %._crit_edge2452
	movb	%bpl, %r11b
.LBB25_144:                             # %._crit_edge2452
	testb	%r14b, %r14b
	movq	8(%rsp), %rbp           # 8-byte Reload
	js	.LBB25_146
# BB#145:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %r8
	movq	316976(%rax), %r9
	movsbl	%r14b, %edx
	leaq	380(%rsp), %rsi
	movl	$0, %ecx
	movq	%rbp, %rdi
	pushq	$16
.Lcfi237:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi238:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi239:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi240:
	.cfi_adjust_cfa_offset 8
	movl	%r11d, %ebx
	callq	SetMotionVectorPredictor
	movl	%ebx, %r11d
	movq	352(%rsp), %rcx         # 8-byte Reload
	addq	$32, %rsp
.Lcfi241:
	.cfi_adjust_cfa_offset -32
.LBB25_146:
	testb	%r11b, %r11b
	js	.LBB25_148
# BB#147:
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %r8
	movq	316976(%rax), %r9
	movsbl	%r11b, %edx
	leaq	376(%rsp), %rsi
	movl	$1, %ecx
	movq	%rbp, %rdi
	pushq	$16
.Lcfi242:
	.cfi_adjust_cfa_offset 8
	pushq	$16
.Lcfi243:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi244:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi245:
	.cfi_adjust_cfa_offset 8
	movl	%r11d, %ebx
	callq	SetMotionVectorPredictor
	movl	%ebx, %r11d
	movq	352(%rsp), %rcx         # 8-byte Reload
	addq	$32, %rsp
.Lcfi246:
	.cfi_adjust_cfa_offset -32
.LBB25_148:
	movq	%rbp, %r9
.LBB25_149:                             # %._crit_edge2710
	decq	%rcx
	movq	%rcx, 320(%rsp)         # 8-byte Spill
	decl	104(%rsp)               # 4-byte Folded Spill
	movq	272(%rsp), %rcx         # 8-byte Reload
	movslq	%ecx, %rdx
	movl	100(%rsp), %eax         # 4-byte Reload
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	xorb	$1, %al
	movb	%al, 536(%rsp)          # 1-byte Spill
	leaq	356(%r12,%r13), %rax
	movq	%rax, 328(%rsp)         # 8-byte Spill
	leal	1(%rcx), %eax
	cltq
	movq	%rax, 336(%rsp)         # 8-byte Spill
	movq	152(%rsp), %rax         # 8-byte Reload
	andl	$1, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
	movl	%r11d, 112(%rsp)        # 4-byte Spill
	movl	%r11d, %eax
	movl	%r14d, 352(%rsp)        # 4-byte Spill
	andb	%r14b, %al
	movb	%al, 264(%rsp)          # 1-byte Spill
	leaq	110(%r9), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movq	%rdx, %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	imulq	$264, %rdx, %rax        # imm = 0x108
	movq	%rax, 544(%rsp)         # 8-byte Spill
	leaq	24(%rax), %rax
	movq	%rax, 528(%rsp)         # 8-byte Spill
	leaq	200(%r9), %rax
	movq	%rax, 552(%rsp)         # 8-byte Spill
	leaq	1412(%r9), %rax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	movw	$-1, %r11w
	movl	$-1, 72(%rsp)           # 4-byte Folded Spill
	movl	$0, %ecx
	movl	$0, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movw	$-1, %r15w
	movw	$-1, %ax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movw	$-1, %ax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%r12, 288(%rsp)         # 8-byte Spill
	movq	%r13, 280(%rsp)         # 8-byte Spill
	jmp	.LBB25_151
.LBB25_150:                             #   in Loop: Header=BB25_151 Depth=1
	movq	288(%rsp), %r12         # 8-byte Reload
	movq	280(%rsp), %r13         # 8-byte Reload
	movq	312(%rsp), %r8          # 8-byte Reload
	jmp	.LBB25_326
	.p2align	4, 0x90
.LBB25_151:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_168 Depth 2
                                        #       Child Loop BB25_203 Depth 3
                                        #       Child Loop BB25_259 Depth 3
                                        #       Child Loop BB25_228 Depth 3
                                        #       Child Loop BB25_234 Depth 3
                                        #       Child Loop BB25_163 Depth 3
                                        #       Child Loop BB25_307 Depth 3
                                        #     Child Loop BB25_322 Depth 2
                                        #     Child Loop BB25_153 Depth 2
	leaq	(,%rcx,8), %rdx
	andl	$8, %edx
	leaq	(%r12,%r13), %rax
	movq	%rax, 568(%rsp)         # 8-byte Spill
	cmpb	$13, 328(%rcx,%rax)
	movq	%rcx, %rsi
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movq	%rsi, 168(%rsp)         # 8-byte Spill
	jne	.LBB25_156
# BB#152:                               #   in Loop: Header=BB25_151 Depth=1
	movw	%r15w, 48(%rsp)         # 2-byte Spill
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	leal	(,%rsi,8), %r12d
	andl	$8, %r12d
	movq	%rdx, %rbx
	movq	%r9, %r14
	leal	(,%rsi,4), %ebp
	andl	$-8, %ebp
	movq	%r14, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	intrapred8x8
	movq	%r14, %rdi
	movl	%r12d, %esi
	movl	%ebp, %edx
	callq	itrans8x8
	movq	dec_picture(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	84(%r14), %rdx
	movslq	%ebp, %rcx
	movslq	80(%r14), %rax
	movq	%rbx, %rsi
	orq	$1, %rsi
	orl	$7, %r12d
	leaq	(%rdx,%rsi), %r8
	leaq	1(%rdx,%rsi), %r9
	movq	%rbx, %rsi
	orq	$3, %rsi
	leaq	(%rdx,%rsi), %r10
	leaq	1(%rdx,%rsi), %r11
	leaq	2(%rdx,%rsi), %r14
	leaq	3(%rdx,%rsi), %r15
	movq	%rbx, %r13
	orq	$7, %r13
	addq	%rdx, %r13
	leaq	(%rdx,%rbx), %rsi
	leal	7(%rcx), %edx
	movslq	%edx, %rdi
	shlq	$3, %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	addq	316920(%rdx), %rax
	leaq	-1(%rcx), %rbp
	shlq	$4, %rcx
	addq	%rbx, %rcx
	movq	144(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB25_153:                             # %.preheader2033
                                        #   Parent Loop BB25_151 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r12, 88(%rsp)          # 8-byte Folded Reload
	movq	8(%rax,%rbp,8), %rdx
	movzwl	-28(%rcx), %ebx
	movw	%bx, (%rdx,%rsi,2)
	jae	.LBB25_155
# BB#154:                               #   in Loop: Header=BB25_153 Depth=2
	movzwl	-24(%rcx), %ebx
	movw	%bx, (%rdx,%r8,2)
	movzwl	-20(%rcx), %ebx
	movw	%bx, (%rdx,%r9,2)
	movzwl	-16(%rcx), %ebx
	movw	%bx, (%rdx,%r10,2)
	movzwl	-12(%rcx), %ebx
	movw	%bx, (%rdx,%r11,2)
	movzwl	-8(%rcx), %ebx
	movw	%bx, (%rdx,%r14,2)
	movzwl	-4(%rcx), %ebx
	movw	%bx, (%rdx,%r15,2)
	movzwl	(%rcx), %ebx
	movw	%bx, (%rdx,%r13,2)
.LBB25_155:                             #   in Loop: Header=BB25_153 Depth=2
	incq	%rbp
	addq	$64, %rcx
	cmpq	%rdi, %rbp
	jl	.LBB25_153
	jmp	.LBB25_325
	.p2align	4, 0x90
.LBB25_156:                             #   in Loop: Header=BB25_151 Depth=1
	leaq	3(,%rsi,4), %rax
	movq	%rax, 472(%rsp)         # 8-byte Spill
	movq	160(%rsp), %r13         # 8-byte Reload
	jmp	.LBB25_168
.LBB25_157:                             # %.preheader2029.preheader
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	%esi, %eax
	movl	%r12d, %edx
	movzwl	384(%rsp), %edi
	movq	%rdx, %rsi
	shlq	$5, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	104(%rcx,%rsi), %rbp
	movw	%di, (%rbp,%rax,2)
	movzwl	388(%rsp), %esi
	movq	%rax, %r9
	orq	$1, %r9
	movw	%si, (%rbp,%r9,2)
	movzwl	392(%rsp), %ecx
	movq	%r10, %r11
	movq	%rax, %r10
	orq	$2, %r10
	movw	%cx, (%rbp,%r10,2)
	movzwl	396(%rsp), %ecx
	movq	%rax, %rsi
	orq	$3, %rsi
	movw	%cx, (%rbp,%rsi,2)
	movq	%rdx, %rcx
	orq	$1, %rcx
	movzwl	400(%rsp), %r8d
	shlq	$5, %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	104(%rbp,%rcx), %rcx
	movw	%r8w, (%rcx,%rax,2)
	movzwl	404(%rsp), %ebp
	movw	%bp, (%rcx,%r9,2)
	movzwl	408(%rsp), %ebp
	movw	%bp, (%rcx,%r10,2)
	movzwl	412(%rsp), %ebp
	movw	%bp, (%rcx,%rsi,2)
	movq	%rdx, %rcx
	orq	$2, %rcx
	movzwl	416(%rsp), %r8d
	shlq	$5, %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	104(%rbp,%rcx), %rcx
	movw	%r8w, (%rcx,%rax,2)
	movzwl	420(%rsp), %ebp
	movw	%bp, (%rcx,%r9,2)
	movzwl	424(%rsp), %ebp
	movw	%bp, (%rcx,%r10,2)
	movzwl	428(%rsp), %ebp
	movw	%bp, (%rcx,%rsi,2)
	orq	$3, %rdx
	movzwl	432(%rsp), %ecx
	shlq	$5, %rdx
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	104(%rbp,%rdx), %rbp
	movw	%cx, (%rbp,%rax,2)
	movzwl	436(%rsp), %eax
	movw	%ax, (%rbp,%r9,2)
	movl	%r12d, %r9d
	movzwl	440(%rsp), %eax
	movw	%ax, (%rbp,%r10,2)
	movq	%r11, %r10
	movzwl	444(%rsp), %eax
	movw	%ax, (%rbp,%rsi,2)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	$1, 72(%rsp)            # 4-byte Folded Spill
	jmp	.LBB25_216
.LBB25_158:                             #   in Loop: Header=BB25_168 Depth=2
	cmpl	$1, 1124(%rax)
	jne	.LBB25_162
# BB#159:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB25_162
# BB#160:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$1, 44(%rbp)
	jne	.LBB25_162
.LBB25_161:                             #   in Loop: Header=BB25_168 Depth=2
	sarw	%r11w
.LBB25_162:                             #   in Loop: Header=BB25_168 Depth=2
	movq	5768(%rbp), %rax
	movq	5776(%rbp), %rcx
	movq	8(%rax), %rax
	movswq	%r11w, %rdx
	movq	(%rax,%rdx,8), %rax
	movl	(%rax), %r8d
	movq	8(%rcx), %rax
	movswl	64(%rsp), %esi          # 2-byte Folded Reload
	movl	100(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %esi
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %r9d
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%r12d, %ecx
	shlq	$4, %rcx
	addq	%rax, %rcx
	movq	360(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,2), %r10
	movq	%rbp, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_163:                             # %.preheader2030
                                        #   Parent Loop BB25_151 Depth=1
                                        #     Parent Loop BB25_168 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %ebx
	movl	384(%rsp,%rbp), %edx
	imull	%r8d, %edx
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edx, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	addl	%r9d, %esi
	cmovsl	%r15d, %esi
	cmpl	%ebx, %esi
	cmovlw	%si, %bx
	movw	%bx, -6(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	388(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, -4(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	392(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, -2(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	396(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, (%r10,%rbp,2)
	addq	$16, %rbp
	cmpq	$64, %rbp
	jne	.LBB25_163
# BB#164:                               #   in Loop: Header=BB25_168 Depth=2
	movl	$1, 72(%rsp)            # 4-byte Folded Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r15d
	movq	%rdi, %rbp
	movq	120(%rsp), %r10         # 8-byte Reload
	jmp	.LBB25_205
.LBB25_165:                             #   in Loop: Header=BB25_168 Depth=2
	movq	456(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx,%rbx,8), %r9
	movswl	(%r9), %esi
	imull	%edx, %esi
	subl	$-128, %esi
	shrl	$8, %esi
	movq	dec_picture(%rip), %rcx
	movq	%r10, %r11
	movl	%r8d, %r10d
	movq	316976(%rcx), %r8
	movq	(%r8), %rbp
	movslq	%r10d, %r15
	movq	(%rbp,%r15,8), %rbp
	movq	(%rbp,%rbx,8), %rbp
	movw	%si, (%rbp)
	movswl	2(%r9), %edi
	imull	%edx, %edi
	subl	$-128, %edi
	shrl	$8, %edi
	movw	%di, 2(%rbp)
	subw	(%r9), %si
	movq	8(%r8), %rdx
	movl	%r10d, %r8d
	movq	%r11, %r10
	movq	(%rdx,%r15,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movw	%si, (%rdx)
	movw	2(%rbp), %si
	subw	2(%r9), %si
	jmp	.LBB25_267
.LBB25_166:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%r8d, %r11d
	movslq	%r8d, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx,%rbx,8), %rdi
	movw	$0, (%rdi)
	movq	%rbx, %rdx
	xorl	%r8d, %r8d
	xorl	%ebp, %ebp
	jmp	.LBB25_244
.LBB25_167:                             #   in Loop: Header=BB25_168 Depth=2
	movq	dec_picture(%rip), %rax
	movq	316976(%rax), %rcx
	movq	8(%rcx), %rdx
	movslq	%r8d, %rcx
	movq	(%rdx,%rcx,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movw	$0, (%rdx)
	xorl	%esi, %esi
	movq	%rbx, %rbp
	xorl	%edi, %edi
	jmp	.LBB25_252
	.p2align	4, 0x90
.LBB25_168:                             #   Parent Loop BB25_151 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_203 Depth 3
                                        #       Child Loop BB25_259 Depth 3
                                        #       Child Loop BB25_228 Depth 3
                                        #       Child Loop BB25_234 Depth 3
                                        #       Child Loop BB25_163 Depth 3
                                        #       Child Loop BB25_307 Depth 3
	movzbl	decode_one_macroblock.decode_block_scan(%r13), %edx
	movl	%edx, %r10d
	andl	$3, %r10d
	shrl	$2, %edx
	movl	%edx, %r14d
	andl	$3, %r14d
	leal	(,%r10,4), %esi
	movslq	92(%r9), %rbx
	addq	%r10, %rbx
	movq	%r9, %rdi
	leal	(,%r14,4), %r9d
	movl	76(%rdi), %eax
	leal	(%r14,%rax), %r8d
	andl	$2, %edx
	movl	%r10d, %ecx
	shrl	%ecx
	orl	%edx, %ecx
	movq	568(%rsp), %rdx         # 8-byte Reload
	movb	328(%rcx,%rdx), %r12b
	cmpb	$11, %r12b
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	movl	%r8d, 296(%rsp)         # 4-byte Spill
	movl	%esi, 40(%rsp)          # 4-byte Spill
	jne	.LBB25_170
# BB#169:                               #   in Loop: Header=BB25_168 Depth=2
	movq	%r13, 128(%rsp)         # 8-byte Spill
	movq	%r10, %r13
	movq	%rdi, %rbp
	movl	%r9d, %r12d
	movl	%r9d, %edx
	movl	%ebx, %ecx
	movl	%r11d, %ebx
	callq	intrapred
	movl	%ebx, %r11d
	cmpl	$1, %eax
	jne	.LBB25_311
	jmp	.LBB25_419
	.p2align	4, 0x90
.LBB25_170:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%rdi, %rbp
	movq	312(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %edx
	orl	$4, %edx
	cmpl	$14, %edx
	jne	.LBB25_174
# BB#171:                               # %.loopexit2036
                                        #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	je	.LBB25_310
.LBB25_172:                             #   in Loop: Header=BB25_168 Depth=2
	movq	312(%rsp), %rax         # 8-byte Reload
	movl	(%rax), %eax
	orl	$4, %eax
	cmpl	$14, %eax
	jne	.LBB25_180
# BB#173:                               #   in Loop: Header=BB25_168 Depth=2
	movl	%r9d, %edx
	xorl	%r9d, %r9d
	movq	%rbp, %rdi
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	%r10d, %ecx
	movl	%r14d, %r8d
	movl	%r11d, %ebx
	callq	itrans
	jmp	.LBB25_181
.LBB25_174:                             #   in Loop: Header=BB25_168 Depth=2
	movq	568(%rsp), %rdx         # 8-byte Reload
	movsbl	332(%rcx,%rdx), %ecx
	cmpb	$2, %cl
	movq	%r14, 176(%rsp)         # 8-byte Spill
	jne	.LBB25_182
# BB#175:                               #   in Loop: Header=BB25_168 Depth=2
	testb	%r12b, %r12b
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rdx
	movq	(%rdx), %rdi
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	8(%rdx), %rdi
	movl	%r9d, 56(%rsp)          # 4-byte Spill
	movb	%r12b, 128(%rsp)        # 1-byte Spill
	je	.LBB25_187
# BB#176:                               # %.thread
                                        #   in Loop: Header=BB25_168 Depth=2
	movq	316952(%rcx), %rax
	movslq	%r8d, %r15
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	(%rcx,%r15,8), %rcx
	movsbl	(%rcx,%rbx), %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	(%rax,%r15,8), %rax
	movsbl	(%rax,%rbx), %r11d
	movq	%rbx, %rbp
	movw	%r11w, %ax
	movl	%eax, 64(%rsp)          # 4-byte Spill
.LBB25_177:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%ebx, %ecx
	shll	$4, %ecx
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rbp,8), %rax
	movswl	(%rax), %edx
	addl	%ecx, %edx
	movq	(%rdi,%r15,8), %rsi
	movq	(%rsi,%rbp,8), %rsi
	movswl	(%rsi), %ebp
	addl	%ecx, %ebp
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	movq	%r10, %r14
	je	.LBB25_186
# BB#178:                               #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	76(%rcx), %edi
	addl	%edi, %edi
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_206
# BB#179:                               #   in Loop: Header=BB25_168 Depth=2
	addl	%r9d, %edi
	shll	$2, %edi
	jmp	.LBB25_207
.LBB25_180:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%rbp, %rdi
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	%r9d, %edx
	movl	%r10d, %ecx
	movl	%r14d, %r8d
	movl	%r11d, %ebx
	callq	itrans_sp
.LBB25_181:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%rbp, %r9
	movl	%ebx, %r11d
	movq	88(%rsp), %r8           # 8-byte Reload
	movl	184(%rsp), %eax         # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB25_319
	jmp	.LBB25_318
.LBB25_182:                             #   in Loop: Header=BB25_168 Depth=2
	movw	%r15w, 48(%rsp)         # 2-byte Spill
	movq	dec_picture(%rip), %rdx
	movslq	%ecx, %r15
	movq	316952(%rdx), %rsi
	movq	316976(%rdx), %rdx
	movq	(%rsi,%r15,8), %rsi
	movslq	%r8d, %rdi
	movq	(%rsi,%rdi,8), %rsi
	movsbl	(%rsi,%rbx), %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	(%rdx,%r15,8), %rdx
	addl	272(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	listX(,%rcx,8), %rsi
	movl	%ebx, %ebp
	shll	$4, %ebp
	movq	(%rdx,%rdi,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movswl	(%rcx), %edx
	addl	%ebp, %edx
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	movq	%r10, 120(%rsp)         # 8-byte Spill
	je	.LBB25_185
# BB#183:                               #   in Loop: Header=BB25_168 Depth=2
	addl	%eax, %eax
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_191
# BB#184:                               #   in Loop: Header=BB25_168 Depth=2
	movl	%r9d, %r12d
	addl	%r9d, %eax
	shll	$2, %eax
	jmp	.LBB25_192
.LBB25_185:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%r9d, %r12d
	movl	%r8d, %eax
	shll	$4, %eax
	jmp	.LBB25_192
.LBB25_186:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%r8d, %edi
	shll	$4, %edi
	jmp	.LBB25_207
.LBB25_187:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%rbp, %rdx
	cmpl	$0, 40(%rdx)
	movl	5624(%rdx), %edx
	movq	%rdi, 136(%rsp)         # 8-byte Spill
	je	.LBB25_221
# BB#188:                               #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r12
	testl	%edx, %edx
	je	.LBB25_236
# BB#189:                               #   in Loop: Header=BB25_168 Depth=2
	movq	328(%rsp), %rdx         # 8-byte Reload
	cmpl	$0, (%rdx)
	movl	352(%rsp), %r10d        # 4-byte Reload
	movl	112(%rsp), %r9d         # 4-byte Reload
	je	.LBB25_237
# BB#190:                               #   in Loop: Header=BB25_168 Depth=2
	leal	-4(%rax), %edx
	testb	$1, 4(%rbp)
	cmovnel	%edx, %eax
	sarl	%eax
	jmp	.LBB25_237
.LBB25_191:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%r9d, %r12d
	addl	%r9d, %eax
	leal	-32(,%rax,4), %eax
.LBB25_192:                             #   in Loop: Header=BB25_168 Depth=2
	movswl	2(%rcx), %ecx
	addl	%eax, %ecx
	movswl	24(%rsp), %ebp          # 2-byte Folded Reload
	movl	%ebp, %edi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %r8
	leaq	192(%rsp), %r9
	callq	get_block
	cmpl	$0, 5800(%rbx)
	je	.LBB25_197
# BB#193:                               #   in Loop: Header=BB25_168 Depth=2
	movq	active_pps(%rip), %rax
	cmpl	$0, 1120(%rax)
	je	.LBB25_198
# BB#194:                               #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	44(%rcx), %ecx
	testl	%ecx, %ecx
	je	.LBB25_196
# BB#195:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$3, %ecx
	jne	.LBB25_198
.LBB25_196:                             #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %AX<def> %AX<kill> %RAX<kill> %EAX<def>
	jne	.LBB25_201
	jmp	.LBB25_202
.LBB25_197:                             # %.preheader2046.preheader
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%r12d, %r9d
	movl	%r9d, %edx
	movzwl	192(%rsp), %ecx
	movq	%rdx, %rsi
	shlq	$5, %rsi
	leaq	104(%rbx,%rsi), %rbp
	movw	%cx, (%rbp,%rax,2)
	movzwl	196(%rsp), %esi
	movq	%rax, %r8
	orq	$1, %r8
	movw	%si, (%rbp,%r8,2)
	movzwl	200(%rsp), %edi
	movq	%rax, %rsi
	orq	$2, %rsi
	movw	%di, (%rbp,%rsi,2)
	movzwl	204(%rsp), %ecx
	movq	%rax, %rdi
	orq	$3, %rdi
	movw	%cx, (%rbp,%rdi,2)
	movq	%rdx, %rcx
	orq	$1, %rcx
	movzwl	208(%rsp), %ebp
	shlq	$5, %rcx
	leaq	104(%rbx,%rcx), %rcx
	movw	%bp, (%rcx,%rax,2)
	movzwl	212(%rsp), %ebp
	movw	%bp, (%rcx,%r8,2)
	movzwl	216(%rsp), %ebp
	movw	%bp, (%rcx,%rsi,2)
	movzwl	220(%rsp), %ebp
	movw	%bp, (%rcx,%rdi,2)
	movq	%rdx, %rcx
	orq	$2, %rcx
	movzwl	224(%rsp), %ebp
	shlq	$5, %rcx
	leaq	104(%rbx,%rcx), %rcx
	movw	%bp, (%rcx,%rax,2)
	movzwl	228(%rsp), %ebp
	movw	%bp, (%rcx,%r8,2)
	movzwl	232(%rsp), %ebp
	movw	%bp, (%rcx,%rsi,2)
	movzwl	236(%rsp), %ebp
	movw	%bp, (%rcx,%rdi,2)
	orq	$3, %rdx
	movzwl	240(%rsp), %ecx
	shlq	$5, %rdx
	leaq	104(%rbx,%rdx), %rbp
	movw	%cx, (%rbp,%rax,2)
	movzwl	244(%rsp), %eax
	movw	%ax, (%rbp,%r8,2)
	movzwl	248(%rsp), %eax
	movw	%ax, (%rbp,%rsi,2)
	movzwl	252(%rsp), %eax
	movw	%ax, (%rbp,%rdi,2)
	movq	%rbx, %rbp
	movl	20(%rsp), %r11d         # 4-byte Reload
	movzwl	48(%rsp), %r15d         # 2-byte Folded Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	movq	176(%rsp), %r14         # 8-byte Reload
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_172
	jmp	.LBB25_310
.LBB25_198:                             #   in Loop: Header=BB25_168 Depth=2
	cmpl	$1, 1124(%rax)
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %AX<def> %AX<kill> %RAX<kill> %EAX<def>
	jne	.LBB25_202
# BB#199:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %AX<def> %AX<kill> %RAX<kill> %EAX<def>
	je	.LBB25_202
# BB#200:                               #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$1, 44(%rax)
	movq	24(%rsp), %rax          # 8-byte Reload
                                        # kill: %AX<def> %AX<kill> %RAX<kill> %EAX<def>
	jne	.LBB25_202
.LBB25_201:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%ebp, %eax
	shrl	%eax
.LBB25_202:                             #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rdi           # 8-byte Reload
	movq	5768(%rdi), %rcx
	movq	5776(%rdi), %rsi
	movq	(%rcx,%r15,8), %rcx
	movswq	%ax, %rax
	movq	(%rcx,%rax,8), %rax
	movl	(%rax), %r8d
	movq	(%rsi,%r15,8), %rax
	movl	100(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %ebp
	movslq	%ebp, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %r9d
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	%r12d, %ecx
	shlq	$4, %rcx
	addq	%rax, %rcx
	movq	360(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,2), %r10
	xorl	%ebp, %ebp
	movl	20(%rsp), %r11d         # 4-byte Reload
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB25_203:                             #   Parent Loop BB25_151 Depth=1
                                        #     Parent Loop BB25_168 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %ebx
	movl	192(%rsp,%rbp), %edx
	imull	%r8d, %edx
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edx, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	addl	%r9d, %esi
	cmovsl	%r15d, %esi
	cmpl	%ebx, %esi
	cmovlw	%si, %bx
	movw	%bx, -6(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	196(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, -4(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	200(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, -2(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	204(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, (%r10,%rbp,2)
	addq	$16, %rbp
	cmpq	$64, %rbp
	jne	.LBB25_203
# BB#204:                               #   in Loop: Header=BB25_168 Depth=2
	movq	%rdi, %rbp
	movzwl	48(%rsp), %r15d         # 2-byte Folded Reload
	movq	120(%rsp), %r10         # 8-byte Reload
	movq	176(%rsp), %r14         # 8-byte Reload
.LBB25_205:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%r12d, %r9d
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_172
	jmp	.LBB25_310
.LBB25_206:                             #   in Loop: Header=BB25_168 Depth=2
	addl	%r9d, %edi
	leal	-32(,%rdi,4), %edi
.LBB25_207:                             #   in Loop: Header=BB25_168 Depth=2
	movswl	2(%rax), %ecx
	addl	%edi, %ecx
	movswl	2(%rsi), %r15d
	addl	%edi, %r15d
	movswl	24(%rsp), %edi          # 2-byte Folded Reload
	movq	344(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rsi
	movq	8(%rsp), %rbx           # 8-byte Reload
	movq	%rbx, %r8
	leaq	192(%rsp), %r9
	callq	get_block
	movswl	64(%rsp), %edi          # 2-byte Folded Reload
	movq	336(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rsi
	movl	%ebp, %edx
	movq	%rbx, %rbp
	movl	%r15d, %ecx
	movq	%rbp, %r8
	leaq	384(%rsp), %r9
	callq	get_block
	cmpb	$0, 128(%rsp)           # 1-byte Folded Reload
	movq	%r14, %r10
	movq	176(%rsp), %r14         # 8-byte Reload
	movl	40(%rsp), %esi          # 4-byte Reload
	je	.LBB25_287
.LBB25_208:                             # %.critedge
                                        #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 5800(%rbp)
	je	.LBB25_215
# BB#209:                               #   in Loop: Header=BB25_168 Depth=2
	movq	active_pps(%rip), %rdx
	movl	1124(%rdx), %ecx
	cmpl	$2, %ecx
	movl	$0, %eax
	cmovel	272(%rsp), %eax         # 4-byte Folded Reload
	cmpb	$0, 128(%rsp)           # 1-byte Folded Reload
	movl	20(%rsp), %ebx          # 4-byte Reload
	movl	$0, %r15d
	jne	.LBB25_211
# BB#210:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 40(%rbp)
	cmovew	%r15w, %bx
.LBB25_211:                             #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 1120(%rdx)
	je	.LBB25_217
# BB#212:                               #   in Loop: Header=BB25_168 Depth=2
	movl	44(%rbp), %edx
	testl	%edx, %edx
	je	.LBB25_214
# BB#213:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$3, %edx
	jne	.LBB25_217
.LBB25_214:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r12
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jne	.LBB25_220
	jmp	.LBB25_232
.LBB25_215:                             # %.preheader2031
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	%esi, %esi
	movl	56(%rsp), %r11d         # 4-byte Reload
	movl	%r11d, %eax
	movq	%rsi, %rdx
	orq	$1, %rdx
	movq	%rsi, %rcx
	orq	$2, %rcx
	movq	%rsi, %r8
	orq	$3, %r8
	movl	192(%rsp), %ebp
	movl	384(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movq	%rax, %rdi
	shlq	$5, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	leaq	104(%rbx,%rdi), %rdi
	movw	%bp, (%rdi,%rsi,2)
	movl	196(%rsp), %ebp
	movl	388(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%rdx,2)
	movl	200(%rsp), %ebp
	movl	392(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%rcx,2)
	movl	204(%rsp), %ebp
	movl	396(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%r8,2)
	movq	%rax, %rdi
	orq	$1, %rdi
	movl	208(%rsp), %ebp
	movl	400(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	shlq	$5, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	leaq	104(%rbx,%rdi), %rdi
	movw	%bp, (%rdi,%rsi,2)
	movl	212(%rsp), %ebp
	movl	404(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%rdx,2)
	movl	216(%rsp), %ebp
	movl	408(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%rcx,2)
	movl	220(%rsp), %ebp
	movl	412(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%r8,2)
	movq	%rax, %rdi
	orq	$2, %rdi
	movl	224(%rsp), %ebp
	movl	416(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	shlq	$5, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	leaq	104(%rbx,%rdi), %rdi
	movw	%bp, (%rdi,%rsi,2)
	movl	228(%rsp), %ebp
	movl	420(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%rdx,2)
	movl	232(%rsp), %ebp
	movl	424(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%rcx,2)
	movl	236(%rsp), %ebp
	movl	428(%rsp), %ebx
	leal	1(%rbp,%rbx), %ebp
	shrl	%ebp
	movw	%bp, (%rdi,%r8,2)
	orq	$3, %rax
	movl	240(%rsp), %edi
	movl	432(%rsp), %ebp
	leal	1(%rdi,%rbp), %edi
	movq	8(%rsp), %rbx           # 8-byte Reload
	shrl	%edi
	shlq	$5, %rax
	movq	%rbx, %rbp
	leaq	104(%rbx,%rax), %rax
	movw	%di, (%rax,%rsi,2)
	movl	244(%rsp), %esi
	movl	436(%rsp), %edi
	leal	1(%rsi,%rdi), %esi
	shrl	%esi
	movw	%si, (%rax,%rdx,2)
	movl	248(%rsp), %edx
	movl	440(%rsp), %esi
	leal	1(%rdx,%rsi), %edx
	shrl	%edx
	movw	%dx, (%rax,%rcx,2)
	movl	252(%rsp), %ecx
	movl	444(%rsp), %edx
	leal	1(%rcx,%rdx), %ecx
	shrl	%ecx
	movw	%cx, (%rax,%r8,2)
	movl	%r11d, %r9d
.LBB25_216:                             # %.loopexit2036
                                        #   in Loop: Header=BB25_168 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r15d
	movl	20(%rsp), %r11d         # 4-byte Reload
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_172
	jmp	.LBB25_310
.LBB25_217:                             #   in Loop: Header=BB25_168 Depth=2
	cmpl	$1, %ecx
	jne	.LBB25_231
# BB#218:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB25_231
# BB#219:                               #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r12
	cmpl	$1, 44(%rbp)
	jne	.LBB25_232
.LBB25_220:                             #   in Loop: Header=BB25_168 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CX<def> %CX<kill> %RCX<kill>
	sarw	%cx
	sarw	%bx
	jmp	.LBB25_233
.LBB25_221:                             #   in Loop: Header=BB25_168 Depth=2
	testl	%edx, %edx
	je	.LBB25_224
# BB#222:                               #   in Loop: Header=BB25_168 Depth=2
	movq	328(%rsp), %rdx         # 8-byte Reload
	cmpl	$0, (%rdx)
	je	.LBB25_224
# BB#223:                               #   in Loop: Header=BB25_168 Depth=2
	leal	-4(%rax), %edx
	testb	$1, 4(%rbp)
	cmovnel	%edx, %eax
	sarl	%eax
.LBB25_224:                             #   in Loop: Header=BB25_168 Depth=2
	addl	%r14d, %eax
	movq	256(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi), %rdx
	movslq	%eax, %r15
	movq	(%rdx,%r15,8), %rax
	xorl	%edx, %edx
	cmpb	$-1, (%rax,%rbx)
	sete	%dl
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	(%rsi,%rdx,8), %rax
	movq	%r15, 24(%rsp)          # 8-byte Spill
	movq	(%rax,%r15,8), %rax
	cmpb	$-1, (%rax,%rbx)
	je	.LBB25_256
# BB#225:                               # %.preheader2045
                                        #   in Loop: Header=BB25_168 Depth=2
	movq	%rbp, %rsi
	movl	5640(%rsi), %edx
	movq	344(%rsp), %rax         # 8-byte Reload
	movl	listXsize(,%rax,4), %eax
	cmpl	%eax, %edx
	cmovlel	%edx, %eax
	testl	%eax, %eax
	jle	.LBB25_257
# BB#226:                               # %.lr.ph
                                        #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 5584(%rsi)
	sete	%dl
	testb	536(%rsp), %dl          # 1-byte Folded Reload
	movq	64(%rsp), %r15          # 8-byte Reload
	je	.LBB25_258
# BB#227:                               # %.lr.ph.split.us.preheader
                                        #   in Loop: Header=BB25_168 Depth=2
	movq	listX(%rip), %rcx
	movq	560(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%r15,8), %rdx
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	(%rdx,%rsi,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movslq	%eax, %rsi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB25_228:                             # %.lr.ph.split.us
                                        #   Parent Loop BB25_151 Depth=1
                                        #     Parent Loop BB25_168 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rcx,%rax,8), %rdi
	movslq	8(%rdi), %rbp
	addq	%rbp, %rbp
	cmpq	%rdx, %rbp
	je	.LBB25_261
# BB#229:                               #   in Loop: Header=BB25_228 Depth=3
	movslq	12(%rdi), %rdi
	addq	%rdi, %rdi
	cmpq	%rdx, %rdi
	je	.LBB25_261
# BB#230:                               #   in Loop: Header=BB25_228 Depth=3
	incq	%rax
	cmpq	%rsi, %rax
	movq	8(%rsp), %rbp           # 8-byte Reload
	jl	.LBB25_228
	jmp	.LBB25_263
.LBB25_231:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r12
.LBB25_232:                             #   in Loop: Header=BB25_168 Depth=2
	movq	24(%rsp), %rcx          # 8-byte Reload
                                        # kill: %CX<def> %CX<kill> %RCX<kill>
.LBB25_233:                             #   in Loop: Header=BB25_168 Depth=2
	movq	5776(%rbp), %r10
	movq	5784(%rbp), %rsi
	cltq
	movq	(%rsi,%rax,8), %rdx
	movw	%cx, 48(%rsp)           # 2-byte Spill
	movswq	%cx, %rdi
	movq	(%rdx,%rdi,8), %rdx
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	movswq	%bx, %rcx
	movq	(%rdx,%rcx,8), %rdx
	movl	(%rdx), %r8d
	movq	8(%rsi,%rax,8), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movq	(%rsi,%rcx,8), %rsi
	movl	(%rsi), %r9d
	movq	(%r10,%rax,8), %rbx
	movq	(%rbx,%rdi,8), %rdi
	movl	(%rdi), %edi
	movq	8(%r10,%rax,8), %rax
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %eax
	leal	1(%rdi,%rax), %r10d
	sarl	%r10d
	movl	56(%rsp), %eax          # 4-byte Reload
	movl	40(%rsp), %ecx          # 4-byte Reload
	shlq	$4, %rax
	addq	%rcx, %rax
	movq	552(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,2), %r11
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB25_234:                             # %.preheader2032
                                        #   Parent Loop BB25_151 Depth=1
                                        #     Parent Loop BB25_168 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	5760(%rbp), %ecx
	movl	5900(%rbp), %edx
	movl	192(%rsp,%rbx,4), %eax
	imull	%r8d, %eax
	movl	384(%rsp,%rbx,4), %esi
	imull	%r9d, %esi
	addl	%eax, %esi
	leal	1(%rcx), %eax
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edi
	addl	%esi, %edi
	movl	%eax, %ecx
	sarl	%cl, %edi
	addl	%r10d, %edi
	cmovsl	%r15d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -96(%r11,%rbx,2)
	movl	5760(%rbp), %ecx
	movl	5900(%rbp), %edx
	movl	208(%rsp,%rbx,4), %eax
	imull	%r8d, %eax
	movl	400(%rsp,%rbx,4), %esi
	imull	%r9d, %esi
	addl	%eax, %esi
	leal	1(%rcx), %eax
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edi
	addl	%esi, %edi
	movl	%eax, %ecx
	sarl	%cl, %edi
	addl	%r10d, %edi
	cmovsl	%r15d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -64(%r11,%rbx,2)
	movl	5760(%rbp), %ecx
	movl	5900(%rbp), %edx
	movl	224(%rsp,%rbx,4), %eax
	imull	%r8d, %eax
	movl	416(%rsp,%rbx,4), %esi
	imull	%r9d, %esi
	addl	%eax, %esi
	leal	1(%rcx), %eax
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edi
	addl	%esi, %edi
	movl	%eax, %ecx
	sarl	%cl, %edi
	addl	%r10d, %edi
	cmovsl	%r15d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, -32(%r11,%rbx,2)
	movl	5760(%rbp), %ecx
	movl	5900(%rbp), %edx
	movl	240(%rsp,%rbx,4), %eax
	imull	%r8d, %eax
	movl	432(%rsp,%rbx,4), %esi
	imull	%r9d, %esi
	addl	%eax, %esi
	leal	1(%rcx), %eax
	movl	$1, %edi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %edi
	addl	%esi, %edi
	movl	%eax, %ecx
	sarl	%cl, %edi
	addl	%r10d, %edi
	cmovsl	%r15d, %edi
	cmpl	%edx, %edi
	cmovlw	%di, %dx
	movw	%dx, (%r11,%rbx,2)
	incq	%rbx
	cmpq	$4, %rbx
	jne	.LBB25_234
# BB#235:                               #   in Loop: Header=BB25_168 Depth=2
	movl	20(%rsp), %r11d         # 4-byte Reload
	jmp	.LBB25_309
.LBB25_236:                             #   in Loop: Header=BB25_168 Depth=2
	movl	352(%rsp), %r10d        # 4-byte Reload
	movl	112(%rsp), %r9d         # 4-byte Reload
.LBB25_237:                             #   in Loop: Header=BB25_168 Depth=2
	addl	%r14d, %eax
	testb	%r10b, %r10b
	js	.LBB25_240
# BB#238:                               #   in Loop: Header=BB25_168 Depth=2
	je	.LBB25_241
# BB#239:                               # %._crit_edge2705
                                        #   in Loop: Header=BB25_168 Depth=2
	movslq	%ebx, %rdx
	jmp	.LBB25_243
.LBB25_240:                             #   in Loop: Header=BB25_168 Depth=2
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movslq	%r8d, %rdx
	movq	(%rcx,%rdx,8), %rcx
	movb	$-1, (%rcx,%rbx)
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movl	$0, (%rcx)
	movq	%r12, %r10
	testb	%r9b, %r9b
	jns	.LBB25_245
	jmp	.LBB25_247
.LBB25_241:                             #   in Loop: Header=BB25_168 Depth=2
	movslq	%eax, %rdx
	movq	464(%rsp), %rsi         # 8-byte Reload
	movq	(%rsi,%rdx,8), %rdx
	cmpb	$0, (%rdx,%rbx)
	movq	%rbx, %rdx
	jne	.LBB25_243
# BB#242:                               #   in Loop: Header=BB25_168 Depth=2
	movq	336(%rsp), %rdx         # 8-byte Reload
	movq	listX(,%rdx,8), %rdx
	movq	(%rdx), %rdx
	cmpl	$0, 316844(%rdx)
	movq	%rbx, %rdx
	je	.LBB25_166
.LBB25_243:                             #   in Loop: Header=BB25_168 Depth=2
	movzwl	380(%rsp), %ebp
	movl	%r8d, %r11d
	movslq	%r8d, %rsi
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rsi,8), %rdi
	movq	(%rdi,%rdx,8), %rdi
	movw	%bp, (%rdi)
	movw	382(%rsp), %r8w
	movb	%r10b, %bpl
.LBB25_244:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r12, %r10
	movw	%r8w, 2(%rdi)
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movb	%bpl, (%rcx,%rdx)
	movl	%r11d, %r8d
	testb	%r9b, %r9b
	js	.LBB25_247
.LBB25_245:                             #   in Loop: Header=BB25_168 Depth=2
	je	.LBB25_248
# BB#246:                               # %._crit_edge2706
                                        #   in Loop: Header=BB25_168 Depth=2
	movslq	%ebx, %rbp
	jmp	.LBB25_251
.LBB25_247:                             #   in Loop: Header=BB25_168 Depth=2
	movq	dec_picture(%rip), %rax
	movq	316976(%rax), %rcx
	movq	8(%rcx), %rdx
	movslq	%r8d, %rcx
	movq	(%rdx,%rcx,8), %rdx
	movslq	%ebx, %rbp
	movq	(%rdx,%rbp,8), %rdx
	movw	$0, (%rdx)
	movb	$-1, %dil
	xorl	%esi, %esi
	jmp	.LBB25_252
.LBB25_248:                             #   in Loop: Header=BB25_168 Depth=2
	cltq
	movq	464(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	cmpb	$0, (%rax,%rbx)
	jne	.LBB25_250
# BB#249:                               #   in Loop: Header=BB25_168 Depth=2
	movq	336(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rax
	movq	(%rax), %rax
	cmpl	$0, 316844(%rax)
	je	.LBB25_167
.LBB25_250:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%rbx, %rbp
.LBB25_251:                             #   in Loop: Header=BB25_168 Depth=2
	movzwl	376(%rsp), %esi
	movq	dec_picture(%rip), %rax
	movq	316976(%rax), %rcx
	movq	8(%rcx), %rdx
	movslq	%r8d, %rcx
	movq	(%rdx,%rcx,8), %rdx
	movq	(%rdx,%rbp,8), %rdx
	movw	%si, (%rdx)
	movw	378(%rsp), %si
	movb	%r9b, %dil
.LBB25_252:                             #   in Loop: Header=BB25_168 Depth=2
	cmpb	$0, 264(%rsp)           # 1-byte Folded Reload
	movw	%si, 2(%rdx)
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%rcx,8), %rax
	movb	%dil, (%rax,%rbp)
	js	.LBB25_254
# BB#253:                               # %._crit_edge2707
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	%r8d, %r12d
	movslq	%r8d, %r15
	jmp	.LBB25_255
.LBB25_254:                             #   in Loop: Header=BB25_168 Depth=2
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	(%rax), %rax
	movl	%r8d, %r12d
	movslq	%r8d, %r15
	movq	(%rax,%r15,8), %rax
	movb	$0, (%rax,%rbp)
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	$0, (%rax,%rbp)
.LBB25_255:                             #   in Loop: Header=BB25_168 Depth=2
	movq	dec_picture(%rip), %rdx
	movq	316952(%rdx), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdi
	movq	(%rcx,%r15,8), %r8
	movsbl	(%r8,%rbp), %r9d
	movzwl	%r9w, %eax
	xorl	%esi, %esi
	cmpl	$65535, %eax            # imm = 0xFFFF
	setne	%sil
	movl	$0, %ebx
	cmovew	%bx, %ax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	(%rdi,%r15,8), %rcx
	movsbl	(%rcx,%rbp), %edi
	movzwl	%di, %eax
	incl	%esi
	cmpl	$65535, %eax            # imm = 0xFFFF
	cmovew	%bx, %ax
	cmovel	%ebx, %esi
	movl	%esi, 72(%rsp)          # 4-byte Spill
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movw	%ax, %r11w
	jmp	.LBB25_269
.LBB25_256:                             # %.preheader2044
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	%r8d, %r12d
	movslq	%r8d, %r15
	movq	(%rdi,%r15,8), %rax
	movq	(%rax,%rbx,8), %rax
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%r15,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movw	$0, (%rdx)
	movw	$0, (%rax)
	movw	$0, 2(%rdx)
	movw	$0, 2(%rax)
	movq	316952(%rcx), %rax
	movq	(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	$0, (%rax,%rbx)
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB25_268
.LBB25_257:                             #   in Loop: Header=BB25_168 Depth=2
	xorl	%eax, %eax
	movq	64(%rsp), %r15          # 8-byte Reload
	jmp	.LBB25_264
.LBB25_258:                             # %.lr.ph.split.preheader
                                        #   in Loop: Header=BB25_168 Depth=2
	movq	560(%rsp), %rdx         # 8-byte Reload
	movq	(%rdx,%r15,8), %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	(%rdx,%rdi,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movslq	12(%rsi), %rdi
	movslq	%eax, %rsi
	addq	528(%rsp), %rcx         # 8-byte Folded Reload
	imulq	$1584, %rdi, %rdi       # imm = 0x630
	addq	%rcx, %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB25_259:                             # %.lr.ph.split
                                        #   Parent Loop BB25_151 Depth=1
                                        #     Parent Loop BB25_168 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpq	%rdx, (%rdi,%rax,8)
	je	.LBB25_262
# BB#260:                               #   in Loop: Header=BB25_259 Depth=3
	incq	%rax
	cmpq	%rsi, %rax
	jl	.LBB25_259
	jmp	.LBB25_263
.LBB25_261:                             #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
.LBB25_262:                             # %._crit_edge2124
                                        #   in Loop: Header=BB25_168 Depth=2
	cmpl	$-135792468, %eax       # imm = 0xF7E7F8AC
	jne	.LBB25_264
.LBB25_263:                             # %._crit_edge2124.thread
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	$.L.str.3, %edi
	movl	$-1111, %esi            # imm = 0xFBA9
	movl	%r8d, %r12d
	movq	%r10, 120(%rsp)         # 8-byte Spill
	callq	error
	movq	120(%rsp), %r10         # 8-byte Reload
	movl	%r12d, %r8d
	movl	$-135792468, %eax       # imm = 0xF7E7F8AC
.LBB25_264:                             # %._crit_edge2124.thread2727
                                        #   in Loop: Header=BB25_168 Depth=2
	cltq
	movq	344(%rsp), %rcx         # 8-byte Reload
	shlq	$7, %rcx
	addq	%rbp, %rcx
	movl	616(%rcx,%rax,4), %edx
	cmpl	$9999, %edx             # imm = 0x270F
	je	.LBB25_266
# BB#265:                               #   in Loop: Header=BB25_168 Depth=2
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	listX(,%rcx,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	cmpl	$0, 316844(%rcx)
	je	.LBB25_165
.LBB25_266:                             #   in Loop: Header=BB25_168 Depth=2
	movq	456(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%r15,8), %rcx
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx,%rbx,8), %rdx
	movzwl	(%rdx), %esi
	movq	dec_picture(%rip), %rcx
	movq	316976(%rcx), %rdi
	movq	(%rdi), %rbp
	movslq	%r8d, %r15
	movq	(%rbp,%r15,8), %rbp
	movq	(%rbp,%rbx,8), %rbp
	movw	%si, (%rbp)
	movzwl	2(%rdx), %edx
	movw	%dx, 2(%rbp)
	movq	8(%rdi), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	(%rdx,%rbx,8), %rdx
	movw	$0, (%rdx)
	xorl	%esi, %esi
.LBB25_267:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%r8d, %r12d
	movw	%si, 2(%rdx)
	movq	316952(%rcx), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r15,8), %rcx
	movb	%al, (%rcx,%rbx)
	movsbl	%al, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r11d, %r11d
.LBB25_268:                             #   in Loop: Header=BB25_168 Depth=2
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rax
	movb	$0, (%rax,%rbx)
	movq	dec_picture(%rip), %rdx
	movq	316952(%rdx), %rax
	movq	(%rax), %rsi
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rcx
	movq	(%rsi,%r15,8), %r8
	movb	(%r8,%rbx), %r9b
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	movq	%rbx, %rbp
.LBB25_269:                             #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rbx           # 8-byte Reload
	movslq	12(%rbx), %rdi
	movsbq	%r9b, %rsi
	imulq	$1584, %rdi, %rdi       # imm = 0x630
	addq	%rdx, %rdi
	movq	544(%rsp), %rax         # 8-byte Reload
	leaq	24(%rax,%rdi), %rax
	movq	(%rax,%rsi,8), %rax
	movq	316960(%rdx), %rdx
	movq	(%rdx), %rsi
	movq	(%rsi,%r15,8), %rsi
	movq	%rax, (%rsi,%rbp,8)
	movsbq	(%rcx,%rbp), %rax
	imulq	$264, 336(%rsp), %rsi   # 8-byte Folded Reload
                                        # imm = 0x108
	leaq	24(%rsi,%rdi), %rsi
	movq	(%rsi,%rax,8), %rax
	movq	8(%rdx), %rdx
	movq	(%rdx,%r15,8), %rdx
	movq	%rax, (%rdx,%rbp,8)
	cmpl	$0, 40(%rbx)
	je	.LBB25_274
# BB#270:                               #   in Loop: Header=BB25_168 Depth=2
	cmpb	$0, (%r8,%rbp)
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	js	.LBB25_278
# BB#271:                               #   in Loop: Header=BB25_168 Depth=2
	movq	80(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	shll	$4, %ecx
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rbp,8), %rax
	movswl	(%rax), %edx
	addl	%ecx, %edx
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movl	%r12d, %edi
	je	.LBB25_275
# BB#272:                               #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	76(%rcx), %esi
	addl	%esi, %esi
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_276
# BB#273:                               #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %rbx
	movl	%edi, %r12d
	addl	56(%rsp), %esi          # 4-byte Folded Reload
	shll	$2, %esi
	jmp	.LBB25_277
.LBB25_274:                             #   in Loop: Header=BB25_168 Depth=2
	movq	80(%rsp), %rbx          # 8-byte Reload
	movl	%r12d, %r8d
	movl	56(%rsp), %r9d          # 4-byte Reload
	movq	136(%rsp), %rdi         # 8-byte Reload
	jmp	.LBB25_177
.LBB25_275:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %rbx
	movl	%edi, %r12d
	movl	%edi, %esi
	shll	$4, %esi
	jmp	.LBB25_277
.LBB25_276:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %rbx
	movl	%edi, %r12d
	addl	56(%rsp), %esi          # 4-byte Folded Reload
	leal	-32(,%rsi,4), %esi
.LBB25_277:                             #   in Loop: Header=BB25_168 Depth=2
	movswl	2(%rax), %ecx
	addl	%esi, %ecx
	movswl	24(%rsp), %edi          # 2-byte Folded Reload
	movq	344(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rsi
	movq	8(%rsp), %r8            # 8-byte Reload
	leaq	192(%rsp), %r9
	callq	get_block
	movq	dec_picture(%rip), %rax
	movq	316952(%rax), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r15,8), %rcx
	movq	%rbx, %r10
.LBB25_278:                             #   in Loop: Header=BB25_168 Depth=2
	cmpb	$0, (%rcx,%rbp)
	movq	80(%rsp), %r8           # 8-byte Reload
	js	.LBB25_282
# BB#279:                               #   in Loop: Header=BB25_168 Depth=2
	movl	%r8d, %ecx
	shll	$4, %ecx
	movq	136(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r15,8), %rax
	movq	(%rax,%rbp,8), %rax
	movswl	(%rax), %edx
	addl	%ecx, %edx
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB25_283
# BB#280:                               #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	76(%rbp), %esi
	addl	%esi, %esi
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_284
# BB#281:                               #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r14
	addl	56(%rsp), %esi          # 4-byte Folded Reload
	shll	$2, %esi
	jmp	.LBB25_285
.LBB25_282:                             #   in Loop: Header=BB25_168 Depth=2
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB25_286
.LBB25_283:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r14
	movl	%r12d, %esi
	shll	$4, %esi
	movq	8(%rsp), %rbp           # 8-byte Reload
	jmp	.LBB25_285
.LBB25_284:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r14
	addl	56(%rsp), %esi          # 4-byte Folded Reload
	leal	-32(,%rsi,4), %esi
.LBB25_285:                             #   in Loop: Header=BB25_168 Depth=2
	movswl	2(%rax), %ecx
	addl	%esi, %ecx
	movswl	64(%rsp), %edi          # 2-byte Folded Reload
	movq	336(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rsi
	movq	%rbp, %r8
	leaq	384(%rsp), %r9
	callq	get_block
	movq	%r14, %r10
.LBB25_286:                             # %.thread1958
                                        #   in Loop: Header=BB25_168 Depth=2
	movq	176(%rsp), %r14         # 8-byte Reload
	movl	40(%rsp), %esi          # 4-byte Reload
.LBB25_287:                             # %.thread1958
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	40(%rbp), %eax
	movl	72(%rsp), %ecx          # 4-byte Reload
	testl	%ecx, %ecx
	jne	.LBB25_294
# BB#288:                               # %.thread1958
                                        #   in Loop: Header=BB25_168 Depth=2
	testl	%eax, %eax
	je	.LBB25_294
# BB#289:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 5800(%rbp)
	movl	20(%rsp), %r11d         # 4-byte Reload
	je	.LBB25_301
# BB#290:                               #   in Loop: Header=BB25_168 Depth=2
	movq	active_pps(%rip), %rax
	cmpl	$0, 1120(%rax)
	je	.LBB25_302
# BB#291:                               #   in Loop: Header=BB25_168 Depth=2
	movl	44(%rbp), %ecx
	testl	%ecx, %ecx
	je	.LBB25_293
# BB#292:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$3, %ecx
	jne	.LBB25_302
.LBB25_293:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r12
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %edx
	jne	.LBB25_305
	jmp	.LBB25_306
.LBB25_294:                             #   in Loop: Header=BB25_168 Depth=2
	cmpl	$1, %ecx
	jne	.LBB25_208
# BB#295:                               #   in Loop: Header=BB25_168 Depth=2
	testl	%eax, %eax
	movl	56(%rsp), %r12d         # 4-byte Reload
	je	.LBB25_208
# BB#296:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 5800(%rbp)
	je	.LBB25_157
# BB#297:                               #   in Loop: Header=BB25_168 Depth=2
	movq	active_pps(%rip), %rax
	cmpl	$0, 1120(%rax)
	movl	20(%rsp), %r11d         # 4-byte Reload
	movl	$0, %r15d
	movq	%r10, 120(%rsp)         # 8-byte Spill
	je	.LBB25_158
# BB#298:                               #   in Loop: Header=BB25_168 Depth=2
	movl	44(%rbp), %ecx
	testl	%ecx, %ecx
	je	.LBB25_300
# BB#299:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$3, %ecx
	jne	.LBB25_158
.LBB25_300:                             #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	jne	.LBB25_161
	jmp	.LBB25_162
.LBB25_301:                             # %.preheader2027.preheader
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	%esi, %eax
	movl	56(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %edx
	movzwl	192(%rsp), %ebx
	movq	%rdx, %rsi
	shlq	$5, %rsi
	movq	8(%rsp), %rcx           # 8-byte Reload
	leaq	104(%rcx,%rsi), %rbp
	movw	%bx, (%rbp,%rax,2)
	movzwl	196(%rsp), %esi
	movq	%rax, %r9
	orq	$1, %r9
	movw	%si, (%rbp,%r9,2)
	movzwl	200(%rsp), %ecx
	movq	%r10, %r15
	movq	%rax, %r10
	orq	$2, %r10
	movw	%cx, (%rbp,%r10,2)
	movzwl	204(%rsp), %ecx
	movq	%rax, %rsi
	orq	$3, %rsi
	movw	%cx, (%rbp,%rsi,2)
	movq	%rdx, %rcx
	orq	$1, %rcx
	movzwl	208(%rsp), %r8d
	shlq	$5, %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	104(%rbp,%rcx), %rcx
	movw	%r8w, (%rcx,%rax,2)
	movzwl	212(%rsp), %ebp
	movw	%bp, (%rcx,%r9,2)
	movzwl	216(%rsp), %ebp
	movw	%bp, (%rcx,%r10,2)
	movzwl	220(%rsp), %ebp
	movw	%bp, (%rcx,%rsi,2)
	movq	%rdx, %rcx
	orq	$2, %rcx
	movzwl	224(%rsp), %r8d
	shlq	$5, %rcx
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	104(%rbp,%rcx), %rcx
	movw	%r8w, (%rcx,%rax,2)
	movzwl	228(%rsp), %ebp
	movw	%bp, (%rcx,%r9,2)
	movzwl	232(%rsp), %ebp
	movw	%bp, (%rcx,%r10,2)
	movzwl	236(%rsp), %ebp
	movw	%bp, (%rcx,%rsi,2)
	orq	$3, %rdx
	movzwl	240(%rsp), %ecx
	shlq	$5, %rdx
	movq	8(%rsp), %rbp           # 8-byte Reload
	leaq	104(%rbp,%rdx), %rbp
	movw	%cx, (%rbp,%rax,2)
	movzwl	244(%rsp), %eax
	movw	%ax, (%rbp,%r9,2)
	movl	%r12d, %r9d
	movzwl	248(%rsp), %eax
	movw	%ax, (%rbp,%r10,2)
	movq	%r15, %r10
	movzwl	252(%rsp), %eax
	movw	%ax, (%rbp,%rsi,2)
	movq	8(%rsp), %rbp           # 8-byte Reload
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	%eax, %r15d
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_172
	jmp	.LBB25_310
.LBB25_302:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%r10, %r12
	cmpl	$1, 1124(%rax)
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %edx
	jne	.LBB25_306
# BB#303:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movl	%esi, %edx
	je	.LBB25_306
# BB#304:                               #   in Loop: Header=BB25_168 Depth=2
	cmpl	$1, 44(%rbp)
	movl	%esi, %edx
	jne	.LBB25_306
.LBB25_305:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%esi, %edx
	sarw	%dx
.LBB25_306:                             #   in Loop: Header=BB25_168 Depth=2
	movq	5768(%rbp), %rax
	movq	5776(%rbp), %rcx
	movq	(%rax), %rax
	movw	%dx, 48(%rsp)           # 2-byte Spill
	movswq	%dx, %rdx
	movq	(%rax,%rdx,8), %rax
	movl	(%rax), %r8d
	movq	(%rcx), %rax
	movswl	%si, %esi
	movl	100(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %esi
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	movl	(%rax), %r9d
	movl	40(%rsp), %eax          # 4-byte Reload
	movl	56(%rsp), %ecx          # 4-byte Reload
	shlq	$4, %rcx
	addq	%rax, %rcx
	movq	360(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rcx,2), %r10
	movq	%rbp, %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB25_307:                             # %.preheader2028
                                        #   Parent Loop BB25_151 Depth=1
                                        #     Parent Loop BB25_168 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %ebx
	movl	192(%rsp,%rbp), %edx
	imull	%r8d, %edx
	leal	-1(%rax), %ecx
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	addl	%edx, %esi
	movl	%eax, %ecx
	sarl	%cl, %esi
	xorl	%r15d, %r15d
	addl	%r9d, %esi
	cmovsl	%r15d, %esi
	cmpl	%ebx, %esi
	cmovlw	%si, %bx
	movw	%bx, -6(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	196(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, -4(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	200(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, -2(%r10,%rbp,2)
	movl	5760(%rdi), %eax
	movl	5900(%rdi), %edx
	movl	204(%rsp,%rbp), %esi
	imull	%r8d, %esi
	leal	-1(%rax), %ecx
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %ebx
	addl	%esi, %ebx
	movl	%eax, %ecx
	sarl	%cl, %ebx
	addl	%r9d, %ebx
	cmovsl	%r15d, %ebx
	cmpl	%edx, %ebx
	cmovlw	%bx, %dx
	movw	%dx, (%r10,%rbp,2)
	addq	$16, %rbp
	cmpq	$64, %rbp
	jne	.LBB25_307
# BB#308:                               #   in Loop: Header=BB25_168 Depth=2
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movq	%rdi, %rbp
.LBB25_309:                             # %.loopexit2036
                                        #   in Loop: Header=BB25_168 Depth=2
	movzwl	48(%rsp), %r15d         # 2-byte Folded Reload
	movq	%r12, %r10
	movl	56(%rsp), %r9d          # 4-byte Reload
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_172
.LBB25_310:                             #   in Loop: Header=BB25_168 Depth=2
	movl	%r9d, %r12d
	movq	%r13, 128(%rsp)         # 8-byte Spill
	movq	%r10, %r13
.LBB25_311:                             # %.thread1967
                                        #   in Loop: Header=BB25_168 Depth=2
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	je	.LBB25_313
# BB#312:                               #   in Loop: Header=BB25_168 Depth=2
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	128(%rsp), %r13         # 8-byte Reload
	movq	%rbp, %r9
	jmp	.LBB25_319
	.p2align	4, 0x90
.LBB25_313:                             #   in Loop: Header=BB25_168 Depth=2
	movq	%rbp, %rdi
	cmpl	$3, 44(%rdi)
	jne	.LBB25_315
# BB#314:                               #   in Loop: Header=BB25_168 Depth=2
	movq	312(%rsp), %rax         # 8-byte Reload
	cmpl	$10, (%rax)
	jne	.LBB25_316
.LBB25_315:                             #   in Loop: Header=BB25_168 Depth=2
	xorl	%r9d, %r9d
	movq	%rbp, %rdi
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	%r12d, %edx
	movl	%r13d, %ecx
	movl	%r14d, %r8d
	movl	%r11d, %ebx
	callq	itrans
	jmp	.LBB25_317
.LBB25_316:                             #   in Loop: Header=BB25_168 Depth=2
	movl	40(%rsp), %esi          # 4-byte Reload
	movl	%r12d, %edx
	movl	%r13d, %ecx
	movl	%r14d, %r8d
	movl	%r11d, %ebx
	callq	itrans_sp
.LBB25_317:                             # %.thread1985
                                        #   in Loop: Header=BB25_168 Depth=2
	movq	%rbp, %r9
	movl	%ebx, %r11d
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	128(%rsp), %r13         # 8-byte Reload
.LBB25_318:                             # %.thread1985
                                        #   in Loop: Header=BB25_168 Depth=2
	movl	296(%rsp), %r14d        # 4-byte Reload
	shll	$2, %r14d
	movq	80(%rsp), %rax          # 8-byte Reload
	leal	(,%rax,4), %eax
	movq	dec_picture(%rip), %rcx
	movq	316920(%rcx), %rcx
	cltq
	movslq	%r14d, %rdx
	movq	(%rcx,%rdx,8), %rsi
	movzwl	1384(%r9), %edi
	movw	%di, (%rsi,%rax,2)
	movzwl	1388(%r9), %edi
	movw	%di, 2(%rsi,%rax,2)
	movzwl	1392(%r9), %edi
	movw	%di, 4(%rsi,%rax,2)
	movzwl	1396(%r9), %edi
	movw	%di, 6(%rsi,%rax,2)
	movq	8(%rcx,%rdx,8), %rsi
	movzwl	1448(%r9), %edi
	movw	%di, (%rsi,%rax,2)
	movzwl	1452(%r9), %edi
	movw	%di, 2(%rsi,%rax,2)
	movzwl	1456(%r9), %edi
	movw	%di, 4(%rsi,%rax,2)
	movzwl	1460(%r9), %edi
	movw	%di, 6(%rsi,%rax,2)
	movq	16(%rcx,%rdx,8), %rsi
	movzwl	1512(%r9), %edi
	movw	%di, (%rsi,%rax,2)
	movzwl	1516(%r9), %edi
	movw	%di, 2(%rsi,%rax,2)
	movzwl	1520(%r9), %edi
	movw	%di, 4(%rsi,%rax,2)
	movzwl	1524(%r9), %edi
	movw	%di, 6(%rsi,%rax,2)
	movq	24(%rcx,%rdx,8), %rcx
	movzwl	1576(%r9), %edx
	movw	%dx, (%rcx,%rax,2)
	movzwl	1580(%r9), %edx
	movw	%dx, 2(%rcx,%rax,2)
	movzwl	1584(%r9), %edx
	movw	%dx, 4(%rcx,%rax,2)
	movzwl	1588(%r9), %edx
	movw	%dx, 6(%rcx,%rax,2)
.LBB25_319:                             # %.critedge1926
                                        #   in Loop: Header=BB25_168 Depth=2
	cmpq	472(%rsp), %r13         # 8-byte Folded Reload
	leaq	1(%r13), %r13
	jl	.LBB25_168
# BB#320:                               #   in Loop: Header=BB25_151 Depth=1
	movl	184(%rsp), %ebx         # 4-byte Reload
	testl	%ebx, %ebx
	je	.LBB25_150
# BB#321:                               #   in Loop: Header=BB25_151 Depth=1
	movw	%r15w, 48(%rsp)         # 2-byte Spill
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	movq	168(%rsp), %rax         # 8-byte Reload
	leal	(,%rax,8), %r12d
	andl	$8, %r12d
	movq	%r8, %rbx
	movq	%r9, %r14
	leal	(,%rax,4), %ebp
	andl	$-8, %ebp
	movq	%r14, %rdi
	movl	%r12d, %esi
	movl	%ebp, %edx
	callq	itrans8x8
	movq	dec_picture(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movslq	84(%r14), %rdx
	movslq	%ebp, %rcx
	movslq	80(%r14), %rax
	movq	%rbx, %rsi
	orq	$1, %rsi
	orl	$7, %r12d
	leaq	(%rdx,%rsi), %r8
	leaq	1(%rdx,%rsi), %r9
	movq	%rbx, %rsi
	orq	$3, %rsi
	leaq	(%rdx,%rsi), %r10
	leaq	1(%rdx,%rsi), %r11
	leaq	2(%rdx,%rsi), %r14
	leaq	3(%rdx,%rsi), %r15
	movq	%rbx, %r13
	orq	$7, %r13
	addq	%rdx, %r13
	leaq	(%rdx,%rbx), %rsi
	leal	7(%rcx), %edx
	movslq	%edx, %rdi
	shlq	$3, %rax
	movq	40(%rsp), %rdx          # 8-byte Reload
	addq	316920(%rdx), %rax
	leaq	-1(%rcx), %rbp
	shlq	$4, %rcx
	addq	%rbx, %rcx
	movq	144(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB25_322:                             # %.preheader2034
                                        #   Parent Loop BB25_151 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	%r12, 88(%rsp)          # 8-byte Folded Reload
	movq	8(%rax,%rbp,8), %rdx
	movzwl	-28(%rcx), %ebx
	movw	%bx, (%rdx,%rsi,2)
	jae	.LBB25_324
# BB#323:                               #   in Loop: Header=BB25_322 Depth=2
	movzwl	-24(%rcx), %ebx
	movw	%bx, (%rdx,%r8,2)
	movzwl	-20(%rcx), %ebx
	movw	%bx, (%rdx,%r9,2)
	movzwl	-16(%rcx), %ebx
	movw	%bx, (%rdx,%r10,2)
	movzwl	-12(%rcx), %ebx
	movw	%bx, (%rdx,%r11,2)
	movzwl	-8(%rcx), %ebx
	movw	%bx, (%rdx,%r14,2)
	movzwl	-4(%rcx), %ebx
	movw	%bx, (%rdx,%r15,2)
	movzwl	(%rcx), %ebx
	movw	%bx, (%rdx,%r13,2)
.LBB25_324:                             #   in Loop: Header=BB25_322 Depth=2
	incq	%rbp
	addq	$64, %rcx
	cmpq	%rdi, %rbp
	jl	.LBB25_322
.LBB25_325:                             #   in Loop: Header=BB25_151 Depth=1
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	312(%rsp), %r8          # 8-byte Reload
	movq	288(%rsp), %r12         # 8-byte Reload
	movq	280(%rsp), %r13         # 8-byte Reload
	movl	20(%rsp), %r11d         # 4-byte Reload
	movzwl	48(%rsp), %r15d         # 2-byte Folded Reload
.LBB25_326:                             # %.loopexit2049
                                        #   in Loop: Header=BB25_151 Depth=1
	movq	168(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	addq	$4, 160(%rsp)           # 8-byte Folded Spill
	cmpq	$4, %rcx
	jl	.LBB25_151
# BB#327:
	movq	dec_picture(%rip), %rax
	cmpl	$0, 317044(%rax)
	je	.LBB25_13
# BB#328:
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	5932(%r9)
	movl	%eax, %ecx
	leal	-1(%rcx), %eax
	movl	%eax, 128(%rsp)         # 4-byte Spill
	movl	$64, %eax
	xorl	%edx, %edx
	idivl	5936(%r9)
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-1(%rax), %edx
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	movq	%rax, %rdx
	movq	%rdx, 296(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	imull	%ecx, %eax
	movl	%eax, 472(%rsp)         # 4-byte Spill
	sarl	%eax
	movl	%eax, 176(%rsp)         # 4-byte Spill
	leaq	104(%r9), %rax
	movq	%rax, 584(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movl	64(%rsp), %r12d         # 4-byte Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB25_329:                             # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_333 Depth 2
                                        #       Child Loop BB25_334 Depth 3
                                        #         Child Loop BB25_389 Depth 4
                                        #           Child Loop BB25_395 Depth 5
                                        #         Child Loop BB25_338 Depth 4
                                        #           Child Loop BB25_344 Depth 5
                                        #     Child Loop BB25_417 Depth 2
	movw	%r15w, 48(%rsp)         # 2-byte Spill
	movl	5924(%r9), %ebx
	movl	(%r8), %ecx
	addl	$-9, %ecx
	cmpl	$6, %ecx
	setb	%al
	movb	$59, %dl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrb	%cl, %dl
	andb	%al, %dl
	movb	%dl, 111(%rsp)          # 1-byte Spill
	movl	%ebx, %eax
	je	.LBB25_331
# BB#330:                               #   in Loop: Header=BB25_329 Depth=1
	movq	%r9, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r11d, %r14d
	movq	%r9, %rbp
	callq	intrapred_chroma
	movq	%rbp, %r9
	movl	%r14d, %r11d
	movl	5924(%r9), %eax
.LBB25_331:                             # %.preheader2024
                                        #   in Loop: Header=BB25_329 Depth=1
	sarl	%eax
	testl	%eax, %eax
	jle	.LBB25_415
# BB#332:                               # %.preheader2022.lr.ph
                                        #   in Loop: Header=BB25_329 Depth=1
	sarl	%ebx
	imull	40(%rsp), %ebx          # 4-byte Folded Reload
	movslq	%ebx, %rax
	movq	%rax, 592(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movq	320(%rsp), %r8          # 8-byte Reload
	.p2align	4, 0x90
.LBB25_333:                             # %.preheader2022
                                        #   Parent Loop BB25_329 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB25_334 Depth 3
                                        #         Child Loop BB25_389 Depth 4
                                        #           Child Loop BB25_395 Depth 5
                                        #         Child Loop BB25_338 Depth 4
                                        #           Child Loop BB25_344 Depth 5
	movq	%rcx, 576(%rsp)         # 8-byte Spill
	movq	592(%rsp), %rax         # 8-byte Reload
	movq	576(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax), %rax
	movq	%rax, 600(%rsp)         # 8-byte Spill
	movq	576(%rsp), %rax         # 8-byte Reload
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB25_334:                             #   Parent Loop BB25_329 Depth=1
                                        #     Parent Loop BB25_333 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB25_389 Depth 4
                                        #           Child Loop BB25_395 Depth 5
                                        #         Child Loop BB25_338 Depth 4
                                        #           Child Loop BB25_344 Depth 5
	movq	%r8, %rdx
	shlq	$5, %rdx
	movq	%rax, %rcx
	movq	%rdx, 608(%rsp)         # 8-byte Spill
	leaq	(%rdx,%rcx,4), %rax
	movzbl	subblk_offset_y(%rsi,%rax), %edx
	movslq	88(%r9), %rdi
	movq	%rdx, %rbx
	movq	%rbx, 256(%rsp)         # 8-byte Spill
	addq	%rdx, %rdi
	movq	%rdi, 536(%rsp)         # 8-byte Spill
	movzbl	subblk_offset_x(%rsi,%rax), %edx
	movl	96(%r9), %eax
	movq	%rdx, 528(%rsp)         # 8-byte Spill
	leal	(%rax,%rdx), %edx
	movl	%edx, 372(%rsp)         # 4-byte Spill
	testb	$1, 111(%rsp)           # 1-byte Folded Reload
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	je	.LBB25_336
# BB#335:                               #   in Loop: Header=BB25_334 Depth=3
	movq	%rsi, %rdx
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_413
	jmp	.LBB25_412
	.p2align	4, 0x90
.LBB25_336:                             #   in Loop: Header=BB25_334 Depth=3
	shlq	$4, %rcx
	movq	%r8, %rdx
	shlq	$6, %rdx
	addq	%rcx, %rdx
	movslq	block8x8_idx(%rdx,%rsi,4), %rsi
	movq	568(%rsp), %rdi         # 8-byte Reload
	movsbl	332(%rsi,%rdi), %ecx
	cmpb	$2, %cl
	movq	dec_picture(%rip), %rdx
	movq	%rdx, 360(%rsp)         # 8-byte Spill
	movq	316976(%rdx), %rdx
	jne	.LBB25_388
# BB#337:                               #   in Loop: Header=BB25_334 Depth=3
	movb	328(%rsi,%rdi), %cl
	movb	%cl, 552(%rsp)          # 1-byte Spill
	movq	(%rdx), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movq	8(%rdx), %rcx
	movq	%rcx, 352(%rsp)         # 8-byte Spill
	movl	536(%rsp), %ecx         # 4-byte Reload
	movq	%rcx, 464(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	movl	372(%rsp), %esi         # 4-byte Reload
	imull	%esi, %edx
	movq	%rdx, 288(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rdx         # 8-byte Reload
	leal	1(%rax,%rdx), %eax
	imull	%ecx, %eax
	decl	%eax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	%esi, %eax
	movq	%rax, 544(%rsp)         # 8-byte Spill
	movq	584(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, 456(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB25_338:                             #   Parent Loop BB25_329 Depth=1
                                        #     Parent Loop BB25_333 Depth=2
                                        #       Parent Loop BB25_334 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB25_344 Depth 5
	movq	%rax, %rsi
	movq	464(%rsp), %rax         # 8-byte Reload
	leal	(%rsi,%rax), %eax
	movl	%eax, %edi
	cltd
	idivl	5984(%r9)
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	%rsi, 264(%rsp)         # 8-byte Spill
	je	.LBB25_342
# BB#339:                               #   in Loop: Header=BB25_338 Depth=4
	movl	88(%r9), %esi
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	je	.LBB25_341
# BB#340:                               #   in Loop: Header=BB25_338 Depth=4
	subl	5936(%r9), %esi
.LBB25_341:                             #   in Loop: Header=BB25_338 Depth=4
	sarl	%esi
	movq	256(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rcx
	addl	%ecx, %esi
	jmp	.LBB25_343
	.p2align	4, 0x90
.LBB25_342:                             # %._crit_edge2709
                                        #   in Loop: Header=BB25_338 Depth=4
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rsi,%rcx), %rcx
	movl	%edi, %esi
.LBB25_343:                             #   in Loop: Header=BB25_338 Depth=4
	imull	296(%rsp), %esi         # 4-byte Folded Reload
	movl	%esi, 168(%rsp)         # 4-byte Spill
	movslq	%eax, %rdx
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	andl	$-2, %eax
	cltq
	movq	%rax, 328(%rsp)         # 8-byte Spill
	shlq	$5, %rcx
	addq	456(%rsp), %rcx         # 8-byte Folded Reload
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB25_344:                             #   Parent Loop BB25_329 Depth=1
                                        #     Parent Loop BB25_333 Depth=2
                                        #       Parent Loop BB25_334 Depth=3
                                        #         Parent Loop BB25_338 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	544(%rsp), %rax         # 8-byte Reload
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	leal	(%rax,%rsi), %eax
	cltd
	idivl	5980(%r9)
	movl	%eax, %r10d
	cmpb	$0, 552(%rsp)           # 1-byte Folded Reload
	movq	%r14, 80(%rsp)          # 8-byte Spill
	je	.LBB25_355
.LBB25_345:                             #   in Loop: Header=BB25_344 Depth=5
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	316952(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rax
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	(%rcx,%rsi,8), %rcx
	movslq	%r10d, %rdi
	movsbq	(%rcx,%rdi), %rdx
	movq	(%rax,%rsi,8), %rax
	movsbl	(%rax,%rdi), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	movq	(%rax,%rdi,8), %rax
	movswl	(%rax), %edi
	movq	288(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r14), %r11d
	leal	(%r11,%rdi), %r14d
	movswl	2(%rax), %ecx
	addl	168(%rsp), %ecx         # 4-byte Folded Reload
	movq	active_sps(%rip), %rax
	movl	32(%rax), %esi
	movq	344(%rsp), %rax         # 8-byte Reload
	movq	listX(,%rax,8), %rax
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	(%rax,%rdx,8), %rax
	movl	%esi, 160(%rsp)         # 4-byte Spill
	cmpl	$1, %esi
	movq	%rax, 72(%rsp)          # 8-byte Spill
	jne	.LBB25_347
# BB#346:                               #   in Loop: Header=BB25_344 Depth=5
	addl	316896(%rax), %ecx
.LBB25_347:                             # %._crit_edge2648
                                        #   in Loop: Header=BB25_344 Depth=5
	movl	60(%r9), %r9d
	movl	%r14d, %eax
	cltd
	movq	88(%rsp), %r12          # 8-byte Reload
	idivl	%r12d
	movl	%eax, %ebx
	testl	%ebx, %ebx
	movl	$0, %ebp
	cmovsl	%ebp, %ebx
	cmpl	%r9d, %ebx
	cmovgl	%r9d, %ebx
	movl	%ecx, %eax
	cltd
	movq	296(%rsp), %r15         # 8-byte Reload
	idivl	%r15d
	movl	%eax, %r8d
	testl	%r8d, %r8d
	cmovsl	%ebp, %r8d
	movl	104(%rsp), %esi         # 4-byte Reload
	cmpl	%esi, %r8d
	cmovgl	%esi, %r8d
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	leal	(%rax,%rdx), %r10d
	addl	%r10d, %edi
	movl	%edi, %eax
	cltd
	idivl	%r12d
	movl	%eax, %edi
	testl	%edi, %edi
	cmovsl	%ebp, %edi
	cmpl	%r9d, %edi
	cmovgl	%r9d, %edi
	movq	184(%rsp), %r13         # 8-byte Reload
	leal	(%rcx,%r13), %eax
	cltd
	idivl	%r15d
	testl	%eax, %eax
	cmovsl	%ebp, %eax
	cmpl	%esi, %eax
	cmovgl	%esi, %eax
	andl	128(%rsp), %r14d        # 4-byte Folded Reload
	movl	%r12d, %edx
	subl	%r14d, %edx
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	316928(%rsi), %rsi
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	(%rsi,%rbp,8), %rsi
	movslq	%r8d, %rbp
	movq	(%rsi,%rbp,8), %r8
	movslq	%ebx, %rbx
	cltq
	movq	(%rsi,%rax,8), %rsi
	movzwl	(%r8,%rbx,2), %ebp
	imull	%edx, %ebp
	movzwl	(%rsi,%rbx,2), %ebx
	imull	%edx, %ebx
	movslq	%edi, %rdx
	movzwl	(%r8,%rdx,2), %eax
	movzwl	(%rsi,%rdx,2), %edx
	imull	%r14d, %eax
	imull	%r14d, %edx
	andl	%r13d, %ecx
	addl	%ebx, %edx
	movl	%r15d, %esi
	subl	%ecx, %esi
	imull	%ecx, %edx
	addl	%ebp, %eax
	imull	%esi, %eax
	addl	176(%rsp), %eax         # 4-byte Folded Reload
	addl	%edx, %eax
	cltd
	idivl	472(%rsp)               # 4-byte Folded Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	336(%rsp), %rcx         # 8-byte Reload
	movq	listX(,%rcx,8), %rcx
	movl	64(%rsp), %esi          # 4-byte Reload
	movswq	%si, %rdx
	movq	(%rcx,%rdx,8), %r8
	movswl	(%rax), %ebx
	addl	%ebx, %r11d
	movswl	2(%rax), %r14d
	addl	168(%rsp), %r14d        # 4-byte Folded Reload
	cmpl	$1, 160(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_349
# BB#348:                               #   in Loop: Header=BB25_344 Depth=5
	addl	316896(%r8), %r14d
.LBB25_349:                             # %._crit_edge2652
                                        #   in Loop: Header=BB25_344 Depth=5
	movl	%r11d, %eax
	cltd
	movq	88(%rsp), %r15          # 8-byte Reload
	idivl	%r15d
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movl	$0, %r12d
	cmovsl	%r12d, %ebp
	cmpl	%r9d, %ebp
	cmovgl	%r9d, %ebp
	movl	%r14d, %eax
	cltd
	movq	296(%rsp), %r13         # 8-byte Reload
	idivl	%r13d
	movl	%eax, %edi
	testl	%edi, %edi
	cmovsl	%r12d, %edi
	movl	104(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %edi
	cmovgl	%ecx, %edi
	addl	%ebx, %r10d
	movl	%r10d, %eax
	cltd
	idivl	%r15d
	movl	%eax, %ebx
	testl	%ebx, %ebx
	cmovsl	%r12d, %ebx
	cmpl	%r9d, %ebx
	cmovgl	%r9d, %ebx
	movq	184(%rsp), %rsi         # 8-byte Reload
	leal	(%r14,%rsi), %eax
	cltd
	idivl	%r13d
	testl	%eax, %eax
	cmovsl	%r12d, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	andl	128(%rsp), %r11d        # 4-byte Folded Reload
	andl	%esi, %r14d
	movl	%r13d, %edx
	subl	%r14d, %edx
	movq	316928(%r8), %rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rsi,%rcx,8), %rsi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %rdi
	movslq	%ebp, %rbp
	movzwl	(%rdi,%rbp,2), %ecx
	imull	%edx, %ecx
	imull	%r11d, %edx
	movslq	%ebx, %rbx
	movzwl	(%rdi,%rbx,2), %edi
	imull	%edx, %edi
	cltq
	movq	(%rsi,%rax,8), %rdx
	movzwl	(%rdx,%rbp,2), %eax
	imull	%r14d, %eax
	addl	%ecx, %eax
	movl	%r15d, %ecx
	subl	%r11d, %ecx
	imull	%ecx, %eax
	addl	%edi, %eax
	imull	%r11d, %r14d
	leaq	(%rdx,%rbx,2), %rdx
	movl	$2, %r15d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %esi
	movl	64(%rsp), %r12d         # 4-byte Reload
	movw	%r12w, %r11w
	movq	%rcx, %r13
.LBB25_350:                             # %.sink.split
                                        #   in Loop: Header=BB25_344 Depth=5
	movzwl	(%rdx), %ecx
	imull	%r14d, %ecx
	addl	176(%rsp), %eax         # 4-byte Folded Reload
	addl	%ecx, %eax
	cltd
	idivl	472(%rsp)               # 4-byte Folded Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movl	%r15d, %eax
	movq	8(%rsp), %r9            # 8-byte Reload
	cmpl	$0, 5800(%r9)
	je	.LBB25_379
.LBB25_351:                             #   in Loop: Header=BB25_344 Depth=5
	movq	active_pps(%rip), %rcx
	cmpl	$0, 1120(%rcx)
	je	.LBB25_358
# BB#352:                               #   in Loop: Header=BB25_344 Depth=5
	movl	44(%r9), %edx
	testl	%edx, %edx
	je	.LBB25_354
# BB#353:                               #   in Loop: Header=BB25_344 Depth=5
	cmpl	$3, %edx
	jne	.LBB25_358
.LBB25_354:                             #   in Loop: Header=BB25_344 Depth=5
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	jne	.LBB25_361
	jmp	.LBB25_362
	.p2align	4, 0x90
.LBB25_355:                             #   in Loop: Header=BB25_344 Depth=5
	cmpl	$0, 40(%r9)
	je	.LBB25_345
# BB#356:                               #   in Loop: Header=BB25_344 Depth=5
	movq	360(%rsp), %rax         # 8-byte Reload
	movq	316952(%rax), %rax
	movq	(%rax), %rcx
	movq	8(%rax), %rdx
	movq	328(%rsp), %rdi         # 8-byte Reload
	movq	(%rcx,%rdi,8), %rax
	movl	%r10d, %ecx
	sarl	%ecx
	movslq	%ecx, %rcx
	movsbl	(%rax,%rcx,2), %eax
	movzwl	%ax, %esi
	cmpl	$65535, %esi            # imm = 0xFFFF
	cmovnew	%ax, %r13w
	movzwl	48(%rsp), %esi          # 2-byte Folded Reload
	cmovnew	%ax, %si
	movq	(%rdx,%rdi,8), %rdx
	movsbl	(%rdx,%rcx,2), %ecx
	movzwl	%cx, %ecx
	cmpl	$65535, %ecx            # imm = 0xFFFF
	cmovnew	%cx, %r12w
	cmovnew	%cx, %r11w
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	movw	%si, 48(%rsp)           # 2-byte Spill
	movl	%r12d, 64(%rsp)         # 4-byte Spill
	je	.LBB25_370
# BB#357:                               #   in Loop: Header=BB25_344 Depth=5
	xorl	%r15d, %r15d
	cmpb	$-1, %al
	setne	%r15b
	incl	%r15d
	movl	%r15d, %eax
	orl	$2, %eax
	cmpl	$2, %eax
	je	.LBB25_371
	jmp	.LBB25_374
	.p2align	4, 0x90
.LBB25_358:                             #   in Loop: Header=BB25_344 Depth=5
	cmpl	$1, 1124(%rcx)
	movq	80(%rsp), %r14          # 8-byte Reload
	jne	.LBB25_362
# BB#359:                               #   in Loop: Header=BB25_344 Depth=5
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	je	.LBB25_362
# BB#360:                               #   in Loop: Header=BB25_344 Depth=5
	cmpl	$1, 44(%r9)
	jne	.LBB25_362
.LBB25_361:                             #   in Loop: Header=BB25_344 Depth=5
	sarw	%si
	sarw	%r11w
.LBB25_362:                             #   in Loop: Header=BB25_344 Depth=5
	movl	40(%r9), %edx
	cmpl	$1, %eax
	movw	%si, 48(%rsp)           # 2-byte Spill
	jne	.LBB25_365
# BB#363:                               #   in Loop: Header=BB25_344 Depth=5
	testl	%edx, %edx
	je	.LBB25_365
# BB#364:                               #   in Loop: Header=BB25_344 Depth=5
	movl	5904(%r9), %edx
	movq	5768(%r9), %rax
	movq	8(%rax), %rax
	movswq	%r11w, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	4(%rax,%rbx,4), %eax
	imull	160(%rsp), %eax         # 4-byte Folded Reload
	addl	5796(%r9), %eax
	movzbl	5764(%r9), %ecx
	sarl	%cl, %eax
	movq	5776(%r9), %rcx
	movq	8(%rcx), %rsi
	movswl	%r12w, %edi
	jmp	.LBB25_368
	.p2align	4, 0x90
.LBB25_365:                             #   in Loop: Header=BB25_344 Depth=5
	testl	%eax, %eax
	jne	.LBB25_369
# BB#366:                               #   in Loop: Header=BB25_344 Depth=5
	testl	%edx, %edx
	je	.LBB25_369
# BB#367:                               #   in Loop: Header=BB25_344 Depth=5
	movl	5904(%r9), %edx
	movq	5768(%r9), %rax
	movq	(%rax), %rax
	movswq	%si, %rcx
	movq	(%rax,%rcx,8), %rax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	4(%rax,%rbx,4), %eax
	imull	72(%rsp), %eax          # 4-byte Folded Reload
	addl	5796(%r9), %eax
	movzbl	5764(%r9), %ecx
	sarl	%cl, %eax
	movq	5776(%r9), %rcx
	movq	(%rcx), %rsi
	movswl	%r13w, %edi
.LBB25_368:                             #   in Loop: Header=BB25_344 Depth=5
	movl	100(%rsp), %ecx         # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	movslq	%edi, %rcx
	movq	(%rsi,%rcx,8), %rcx
	addl	4(%rcx,%rbx,4), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	jmp	.LBB25_386
.LBB25_369:                             #   in Loop: Header=BB25_344 Depth=5
	cmpl	$2, 1124(%rcx)
	movl	$0, %eax
	cmovel	272(%rsp), %eax         # 4-byte Folded Reload
	movq	5784(%r9), %rcx
	cltq
	movq	(%rcx,%rax,8), %rdx
	movswq	%si, %rsi
	movq	(%rdx,%rsi,8), %rdi
	movswq	%r11w, %r8
	movq	(%rdi,%r8,8), %rdi
	movq	8(%rcx,%rax,8), %rcx
	movq	(%rcx,%rsi,8), %rcx
	movq	(%rcx,%r8,8), %rcx
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	4(%rdi,%rdx,4), %edi
	imull	72(%rsp), %edi          # 4-byte Folded Reload
	movl	4(%rcx,%rdx,4), %ebp
	imull	160(%rsp), %ebp         # 4-byte Folded Reload
	addl	%edi, %ebp
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	5764(%rcx), %ecx
	movl	$1, %edi
	shll	%cl, %edi
	addl	%ebp, %edi
	movq	8(%rsp), %rbp           # 8-byte Reload
	movq	5776(%rbp), %rbp
	movq	(%rbp,%rax,8), %rbx
	movq	(%rbx,%rsi,8), %rsi
	movq	8(%rbp,%rax,8), %rax
	movq	8(%rsp), %r9            # 8-byte Reload
	movq	(%rax,%r8,8), %rax
	movl	4(%rsi,%rdx,4), %esi
	movl	4(%rax,%rdx,4), %eax
	leal	1(%rsi,%rax), %eax
	incl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	sarl	%cl, %edi
	sarl	%eax
	addl	%edi, %eax
	movl	5904(%r9), %ecx
	movl	$0, %edx
	cmovsl	%edx, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	jmp	.LBB25_386
.LBB25_370:                             #   in Loop: Header=BB25_344 Depth=5
	xorl	%r15d, %r15d
.LBB25_371:                             # %.thread1991
                                        #   in Loop: Header=BB25_344 Depth=5
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movslq	%r10d, %rcx
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %ebx
	movq	288(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r14), %r8d
	addl	%ebx, %r8d
	movswl	2(%rax), %r9d
	addl	168(%rsp), %r9d         # 4-byte Folded Reload
	movq	active_sps(%rip), %rax
	movq	344(%rsp), %rcx         # 8-byte Reload
	movq	listX(,%rcx,8), %rcx
	movswq	%r13w, %rdx
	movq	(%rcx,%rdx,8), %rcx
	cmpl	$1, 32(%rax)
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	jne	.LBB25_373
# BB#372:                               #   in Loop: Header=BB25_344 Depth=5
	addl	316896(%rcx), %r9d
.LBB25_373:                             # %.thread1991._crit_edge
                                        #   in Loop: Header=BB25_344 Depth=5
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	60(%rax), %esi
	movl	%r8d, %eax
	cltd
	movq	88(%rsp), %r13          # 8-byte Reload
	idivl	%r13d
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movl	$0, %r11d
	cmovsl	%r11d, %ebp
	cmpl	%esi, %ebp
	cmovgl	%esi, %ebp
	movl	%r9d, %eax
	cltd
	movq	296(%rsp), %r12         # 8-byte Reload
	idivl	%r12d
	movl	%eax, %ecx
	testl	%ecx, %ecx
	cmovsl	%r11d, %ecx
	movl	104(%rsp), %edi         # 4-byte Reload
	cmpl	%edi, %ecx
	cmovgl	%edi, %ecx
	movq	280(%rsp), %rax         # 8-byte Reload
	leal	(%rax,%r14), %eax
	addl	%ebx, %eax
	cltd
	idivl	%r13d
	movl	%eax, %ebx
	testl	%ebx, %ebx
	cmovsl	%r11d, %ebx
	cmpl	%esi, %ebx
	cmovgl	%esi, %ebx
	movq	184(%rsp), %r14         # 8-byte Reload
	leal	(%r9,%r14), %eax
	cltd
	idivl	%r12d
	testl	%eax, %eax
	cmovsl	%r11d, %eax
	cmpl	%edi, %eax
	cmovgl	%edi, %eax
	andl	128(%rsp), %r8d         # 4-byte Folded Reload
	movl	%r13d, %edx
	subl	%r8d, %edx
	movq	72(%rsp), %rsi          # 8-byte Reload
	movq	316928(%rsi), %rsi
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rsi,%rdi,8), %rsi
	movslq	%ecx, %rcx
	movq	(%rsi,%rcx,8), %r11
	movslq	%ebp, %rbp
	cltq
	movq	(%rsi,%rax,8), %rsi
	movzwl	(%r11,%rbp,2), %ecx
	imull	%edx, %ecx
	movzwl	(%rsi,%rbp,2), %ebp
	imull	%edx, %ebp
	movslq	%ebx, %rdx
	movzwl	(%r11,%rdx,2), %eax
	movzwl	(%rsi,%rdx,2), %edx
	imull	%r8d, %eax
	imull	%r8d, %edx
	andl	%r14d, %r9d
	movq	80(%rsp), %r14          # 8-byte Reload
	addl	%ebp, %edx
	movl	%r12d, %esi
	subl	%r9d, %esi
	imull	%r9d, %edx
	addl	%ecx, %eax
	imull	%esi, %eax
	addl	176(%rsp), %eax         # 4-byte Folded Reload
	addl	%edx, %eax
	cltd
	idivl	472(%rsp)               # 4-byte Folded Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movzwl	48(%rsp), %esi          # 2-byte Folded Reload
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	64(%rsp), %r12d         # 4-byte Reload
.LBB25_374:                             #   in Loop: Header=BB25_344 Depth=5
	leal	-1(%r15), %ecx
	xorl	%eax, %eax
	cmpl	$1, %ecx
	ja	.LBB25_378
# BB#375:                               #   in Loop: Header=BB25_344 Depth=5
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movslq	%r10d, %rcx
	movq	(%rax,%rcx,8), %rax
	movswl	(%rax), %edi
	movq	288(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%r14), %r8d
	addl	%edi, %r8d
	movswl	2(%rax), %r14d
	addl	168(%rsp), %r14d        # 4-byte Folded Reload
	movq	active_sps(%rip), %rax
	movq	336(%rsp), %rcx         # 8-byte Reload
	movq	listX(,%rcx,8), %rdx
	movswq	%r12w, %rsi
	movq	(%rdx,%rsi,8), %r10
	cmpl	$1, 32(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB25_377
# BB#376:                               #   in Loop: Header=BB25_344 Depth=5
	addl	316896(%r10), %r14d
.LBB25_377:                             # %._crit_edge2644
                                        #   in Loop: Header=BB25_344 Depth=5
	movl	60(%rax), %esi
	movl	%r8d, %eax
	cltd
	movq	88(%rsp), %r9           # 8-byte Reload
	idivl	%r9d
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movl	$0, %r11d
	cmovsl	%r11d, %ebp
	cmpl	%esi, %ebp
	cmovgl	%esi, %ebp
	movl	%r14d, %eax
	cltd
	movq	296(%rsp), %r12         # 8-byte Reload
	idivl	%r12d
	movl	%eax, %ebx
	testl	%ebx, %ebx
	cmovsl	%r11d, %ebx
	movl	104(%rsp), %ecx         # 4-byte Reload
	cmpl	%ecx, %ebx
	cmovgl	%ecx, %ebx
	movq	280(%rsp), %rax         # 8-byte Reload
	movq	80(%rsp), %rdx          # 8-byte Reload
	leal	(%rax,%rdx), %eax
	addl	%edi, %eax
	cltd
	idivl	%r9d
	movl	%eax, %edi
	testl	%edi, %edi
	cmovsl	%r11d, %edi
	cmpl	%esi, %edi
	cmovgl	%esi, %edi
	movq	184(%rsp), %rsi         # 8-byte Reload
	leal	(%r14,%rsi), %eax
	cltd
	idivl	%r12d
	testl	%eax, %eax
	cmovsl	%r11d, %eax
	cmpl	%ecx, %eax
	cmovgl	%ecx, %eax
	andl	128(%rsp), %r8d         # 4-byte Folded Reload
	andl	%esi, %r14d
	movl	%r12d, %edx
	subl	%r14d, %edx
	movq	316928(%r10), %rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rsi,%rcx,8), %r10
	movslq	%ebx, %rbx
	movq	(%r10,%rbx,8), %rbx
	movslq	%ebp, %rbp
	movzwl	(%rbx,%rbp,2), %esi
	imull	%edx, %esi
	imull	%r8d, %edx
	movslq	%edi, %rdi
	movzwl	(%rbx,%rdi,2), %ebx
	imull	%edx, %ebx
	cltq
	movq	(%r10,%rax,8), %rdx
	movzwl	(%rdx,%rbp,2), %eax
	imull	%r14d, %eax
	addl	%esi, %eax
	movl	%r9d, %esi
	subl	%r8d, %esi
	imull	%esi, %eax
	addl	%ebx, %eax
	imull	%r8d, %r14d
	leaq	(%rdx,%rdi,2), %rdx
	movzwl	48(%rsp), %esi          # 2-byte Folded Reload
	movl	20(%rsp), %r11d         # 4-byte Reload
	movl	64(%rsp), %r12d         # 4-byte Reload
	jmp	.LBB25_350
.LBB25_378:                             #   in Loop: Header=BB25_344 Depth=5
	movq	8(%rsp), %r9            # 8-byte Reload
	movl	20(%rsp), %r11d         # 4-byte Reload
	cmpl	$0, 5800(%r9)
	jne	.LBB25_351
	.p2align	4, 0x90
.LBB25_379:                             #   in Loop: Header=BB25_344 Depth=5
	movl	40(%r9), %ecx
	cmpl	$1, %eax
	movq	80(%rsp), %r14          # 8-byte Reload
	movw	%si, 48(%rsp)           # 2-byte Spill
	jne	.LBB25_382
# BB#380:                               #   in Loop: Header=BB25_344 Depth=5
	testl	%ecx, %ecx
	je	.LBB25_382
# BB#381:                               #   in Loop: Header=BB25_344 Depth=5
	movq	160(%rsp), %rax         # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	jmp	.LBB25_386
.LBB25_382:                             #   in Loop: Header=BB25_344 Depth=5
	testl	%eax, %eax
	jne	.LBB25_385
# BB#383:                               #   in Loop: Header=BB25_344 Depth=5
	testl	%ecx, %ecx
	je	.LBB25_385
# BB#384:                               #   in Loop: Header=BB25_344 Depth=5
	movq	72(%rsp), %rax          # 8-byte Reload
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	jmp	.LBB25_386
.LBB25_385:                             #   in Loop: Header=BB25_344 Depth=5
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	160(%rsp), %rcx         # 8-byte Reload
	leal	1(%rax,%rcx), %eax
	shrl	%eax
	.p2align	4, 0x90
.LBB25_386:                             #   in Loop: Header=BB25_344 Depth=5
	movq	136(%rsp), %rdx         # 8-byte Reload
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movw	%ax, (%rcx,%rsi,2)
	incq	%rsi
	addl	88(%rsp), %r14d         # 4-byte Folded Reload
	cmpq	$4, %rsi
	jne	.LBB25_344
# BB#387:                               #   in Loop: Header=BB25_338 Depth=4
	movq	264(%rsp), %rax         # 8-byte Reload
	incq	%rax
	cmpq	$4, %rax
	jne	.LBB25_338
	jmp	.LBB25_411
	.p2align	4, 0x90
.LBB25_388:                             #   in Loop: Header=BB25_334 Depth=3
	movslq	%ecx, %rsi
	movq	%rsi, 328(%rsp)         # 8-byte Spill
	movq	(%rdx,%rsi,8), %rdx
	movq	%rdx, 464(%rsp)         # 8-byte Spill
	addl	272(%rsp), %ecx         # 4-byte Folded Reload
	movslq	%ecx, %rcx
	movq	listX(,%rcx,8), %rcx
	movq	%rcx, 552(%rsp)         # 8-byte Spill
	movl	536(%rsp), %ecx         # 4-byte Reload
	movq	%rcx, 456(%rsp)         # 8-byte Spill
	movq	88(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	movl	372(%rsp), %esi         # 4-byte Reload
	imull	%esi, %edx
	movq	%rdx, 544(%rsp)         # 8-byte Spill
	movq	528(%rsp), %rdx         # 8-byte Reload
	leal	1(%rax,%rdx), %eax
	imull	%ecx, %eax
	decl	%eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%esi, %eax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	584(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdx,2), %rax
	movq	%rax, 560(%rsp)         # 8-byte Spill
	xorl	%ecx, %ecx
	movl	%r12d, 64(%rsp)         # 4-byte Spill
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB25_389:                             #   Parent Loop BB25_329 Depth=1
                                        #     Parent Loop BB25_333 Depth=2
                                        #       Parent Loop BB25_334 Depth=3
                                        # =>      This Loop Header: Depth=4
                                        #           Child Loop BB25_395 Depth 5
	movq	456(%rsp), %rax         # 8-byte Reload
	leal	(%rcx,%rax), %esi
	movl	%esi, %eax
	cltd
	idivl	5984(%r9)
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movq	%rcx, 264(%rsp)         # 8-byte Spill
	je	.LBB25_393
# BB#390:                               #   in Loop: Header=BB25_389 Depth=4
	movl	88(%r9), %esi
	cmpl	$0, 152(%rsp)           # 4-byte Folded Reload
	je	.LBB25_392
# BB#391:                               #   in Loop: Header=BB25_389 Depth=4
	subl	5936(%r9), %esi
.LBB25_392:                             #   in Loop: Header=BB25_389 Depth=4
	sarl	%esi
	movq	256(%rsp), %rcx         # 8-byte Reload
	movq	264(%rsp), %rdx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rdi
	addl	%edi, %esi
	jmp	.LBB25_394
	.p2align	4, 0x90
.LBB25_393:                             # %._crit_edge2708
                                        #   in Loop: Header=BB25_389 Depth=4
	movq	%rcx, %rdx
	movq	256(%rsp), %rcx         # 8-byte Reload
	leaq	(%rdx,%rcx), %rdi
.LBB25_394:                             #   in Loop: Header=BB25_389 Depth=4
	imull	296(%rsp), %esi         # 4-byte Folded Reload
	cltq
	movq	360(%rsp), %rcx         # 8-byte Reload
	movq	316952(%rcx), %rcx
	movq	328(%rsp), %rdx         # 8-byte Reload
	movq	(%rcx,%rdx,8), %rcx
	movq	(%rcx,%rax,8), %rcx
	movq	%rcx, 288(%rsp)         # 8-byte Spill
	movq	464(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	active_sps(%rip), %rax
	movl	32(%rax), %eax
	movl	%eax, 144(%rsp)         # 4-byte Spill
	shlq	$5, %rdi
	addq	560(%rsp), %rdi         # 8-byte Folded Reload
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	xorl	%ebx, %ebx
	xorl	%ecx, %ecx
	movl	%esi, 168(%rsp)         # 4-byte Spill
	.p2align	4, 0x90
.LBB25_395:                             #   Parent Loop BB25_329 Depth=1
                                        #     Parent Loop BB25_333 Depth=2
                                        #       Parent Loop BB25_334 Depth=3
                                        #         Parent Loop BB25_389 Depth=4
                                        # =>        This Inner Loop Header: Depth=5
	movq	352(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	leal	(%rax,%rcx), %eax
	cltd
	idivl	5980(%r9)
	cltq
	movq	288(%rsp), %rcx         # 8-byte Reload
	movsbq	(%rcx,%rax), %rdx
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movswl	(%rax), %r15d
	movq	544(%rsp), %rcx         # 8-byte Reload
	leal	(%rcx,%rbx), %r8d
	addl	%r15d, %r8d
	movswl	2(%rax), %r13d
	addl	%esi, %r13d
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	552(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %r12
	cmpl	$1, 144(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_397
# BB#396:                               #   in Loop: Header=BB25_395 Depth=5
	addl	316896(%r12), %r13d
.LBB25_397:                             # %._crit_edge2637
                                        #   in Loop: Header=BB25_395 Depth=5
	movl	60(%r9), %esi
	movl	%r8d, %eax
	cltd
	movq	88(%rsp), %r11          # 8-byte Reload
	idivl	%r11d
	movl	%eax, %ebp
	testl	%ebp, %ebp
	movl	$0, %r14d
	cmovsl	%r14d, %ebp
	cmpl	%esi, %ebp
	cmovgl	%esi, %ebp
	movl	%r13d, %eax
	cltd
	movq	296(%rsp), %r9          # 8-byte Reload
	idivl	%r9d
	movl	%eax, %edi
	testl	%edi, %edi
	cmovsl	%r14d, %edi
	movl	104(%rsp), %r10d        # 4-byte Reload
	cmpl	%r10d, %edi
	cmovgl	%r10d, %edi
	movq	112(%rsp), %rax         # 8-byte Reload
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	leal	(%rax,%rbx), %eax
	addl	%r15d, %eax
	cltd
	idivl	%r11d
	movl	%eax, %ebx
	testl	%ebx, %ebx
	cmovsl	%r14d, %ebx
	cmpl	%esi, %ebx
	cmovgl	%esi, %ebx
	movq	184(%rsp), %r15         # 8-byte Reload
	leal	(%r13,%r15), %eax
	cltd
	idivl	%r9d
	testl	%eax, %eax
	cmovsl	%r14d, %eax
	cmpl	%r10d, %eax
	cmovgl	%r10d, %eax
	andl	128(%rsp), %r8d         # 4-byte Folded Reload
	movl	%r11d, %edx
	subl	%r8d, %edx
	movq	316928(%r12), %rsi
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rsi,%rcx,8), %rsi
	movslq	%edi, %rdi
	movq	(%rsi,%rdi,8), %r12
	movslq	%ebp, %rbp
	cltq
	movq	(%rsi,%rax,8), %rsi
	movzwl	(%r12,%rbp,2), %edi
	imull	%edx, %edi
	movzwl	(%rsi,%rbp,2), %ebp
	imull	%edx, %ebp
	movslq	%ebx, %rdx
	movzwl	(%r12,%rdx,2), %eax
	movzwl	(%rsi,%rdx,2), %edx
	imull	%r8d, %eax
	imull	%r8d, %edx
	andl	%r15d, %r13d
	addl	%ebp, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	%r9d, %esi
	movq	%rcx, %r9
	subl	%r13d, %esi
	imull	%r13d, %edx
	addl	%edi, %eax
	imull	%esi, %eax
	addl	176(%rsp), %eax         # 4-byte Folded Reload
	addl	%edx, %eax
	cltd
	idivl	472(%rsp)               # 4-byte Folded Reload
	cmpl	$0, 5800(%r9)
	je	.LBB25_402
# BB#398:                               #   in Loop: Header=BB25_395 Depth=5
	movq	active_pps(%rip), %rcx
	cmpl	$0, 1120(%rcx)
	movq	24(%rsp), %r13          # 8-byte Reload
	je	.LBB25_403
# BB#399:                               #   in Loop: Header=BB25_395 Depth=5
	movl	44(%r9), %edx
	testl	%edx, %edx
	je	.LBB25_401
# BB#400:                               #   in Loop: Header=BB25_395 Depth=5
	cmpl	$3, %edx
	jne	.LBB25_403
.LBB25_401:                             #   in Loop: Header=BB25_395 Depth=5
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movw	%r13w, %cx
	movq	80(%rsp), %rbx          # 8-byte Reload
	jne	.LBB25_406
	jmp	.LBB25_407
	.p2align	4, 0x90
.LBB25_402:                             #   in Loop: Header=BB25_395 Depth=5
	movq	24(%rsp), %r13          # 8-byte Reload
	movl	168(%rsp), %esi         # 4-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	80(%rsp), %rbx          # 8-byte Reload
	jmp	.LBB25_408
	.p2align	4, 0x90
.LBB25_403:                             #   in Loop: Header=BB25_395 Depth=5
	cmpl	$1, 1124(%rcx)
	movw	%r13w, %cx
	movq	80(%rsp), %rbx          # 8-byte Reload
	jne	.LBB25_407
# BB#404:                               #   in Loop: Header=BB25_395 Depth=5
	cmpl	$0, 36(%rsp)            # 4-byte Folded Reload
	movw	%r13w, %cx
	je	.LBB25_407
# BB#405:                               #   in Loop: Header=BB25_395 Depth=5
	cmpl	$1, 44(%r9)
	movw	%r13w, %cx
	jne	.LBB25_407
.LBB25_406:                             #   in Loop: Header=BB25_395 Depth=5
	movl	%r13d, %ecx
	sarb	%cl
	movsbl	%cl, %ecx
.LBB25_407:                             #   in Loop: Header=BB25_395 Depth=5
	movl	5904(%r9), %edx
	movq	5768(%r9), %rsi
	movq	328(%rsp), %rbp         # 8-byte Reload
	movq	(%rsi,%rbp,8), %rsi
	movswq	%cx, %rdi
	movq	(%rsi,%rdi,8), %rcx
	movq	40(%rsp), %rsi          # 8-byte Reload
	imull	4(%rcx,%rsi,4), %eax
	addl	5796(%r9), %eax
	movzbl	5764(%r9), %ecx
	sarl	%cl, %eax
	movq	5776(%r9), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx,%rdi,8), %rcx
	addl	4(%rcx,%rsi,4), %eax
	movl	$0, %ecx
	cmovsl	%ecx, %eax
	cmpl	%edx, %eax
	cmovgl	%edx, %eax
	movl	168(%rsp), %esi         # 4-byte Reload
	movq	120(%rsp), %rcx         # 8-byte Reload
.LBB25_408:                             #   in Loop: Header=BB25_395 Depth=5
	movq	56(%rsp), %rdx          # 8-byte Reload
	movw	%ax, (%rcx,%rdx,2)
	movq	%rdx, %rcx
	incq	%rcx
	addl	88(%rsp), %ebx          # 4-byte Folded Reload
	cmpq	$4, %rcx
	jne	.LBB25_395
# BB#409:                               #   in Loop: Header=BB25_389 Depth=4
	movq	264(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpq	$4, %rcx
	movl	64(%rsp), %r12d         # 4-byte Reload
	movl	20(%rsp), %r11d         # 4-byte Reload
	jne	.LBB25_389
# BB#410:                               #   in Loop: Header=BB25_334 Depth=3
	movq	136(%rsp), %rdx         # 8-byte Reload
.LBB25_411:                             # %.loopexit2020
                                        #   in Loop: Header=BB25_334 Depth=3
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	jne	.LBB25_413
.LBB25_412:                             # %.preheader2019
                                        #   in Loop: Header=BB25_334 Depth=3
	movq	dec_picture(%rip), %rax
	movq	316928(%rax), %rax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %r14
	movq	600(%rsp), %rax         # 8-byte Reload
	movq	608(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,4), %rax
	movzbl	cofuv_blk_x(%rdx,%rax), %ecx
	movzbl	cofuv_blk_y(%rdx,%rax), %r8d
	movq	%r9, %rbp
	movl	$1, %r9d
	movq	%rbp, %rdi
	movq	528(%rsp), %rsi         # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movq	256(%rsp), %rdx         # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r11d, %ebx
	callq	itrans
	movq	%rbp, %r9
	movl	%ebx, %r11d
	movslq	372(%rsp), %rax         # 4-byte Folded Reload
	movq	536(%rsp), %rsi         # 8-byte Reload
	movq	(%r14,%rsi,8), %rcx
	movzwl	1384(%r9), %edx
	movw	%dx, (%rcx,%rax,2)
	movzwl	1388(%r9), %edx
	movw	%dx, 2(%rcx,%rax,2)
	movzwl	1392(%r9), %edx
	movw	%dx, 4(%rcx,%rax,2)
	movzwl	1396(%r9), %edx
	movw	%dx, 6(%rcx,%rax,2)
	movq	8(%r14,%rsi,8), %rcx
	movzwl	1448(%r9), %edx
	movw	%dx, (%rcx,%rax,2)
	movzwl	1452(%r9), %edx
	movw	%dx, 2(%rcx,%rax,2)
	movzwl	1456(%r9), %edx
	movw	%dx, 4(%rcx,%rax,2)
	movzwl	1460(%r9), %edx
	movw	%dx, 6(%rcx,%rax,2)
	movq	16(%r14,%rsi,8), %rcx
	movzwl	1512(%r9), %edx
	movw	%dx, (%rcx,%rax,2)
	movzwl	1516(%r9), %edx
	movw	%dx, 2(%rcx,%rax,2)
	movzwl	1520(%r9), %edx
	movw	%dx, 4(%rcx,%rax,2)
	movzwl	1524(%r9), %edx
	movw	%dx, 6(%rcx,%rax,2)
	movq	24(%r14,%rsi,8), %rcx
	movzwl	1576(%r9), %edx
	movw	%dx, (%rcx,%rax,2)
	movzwl	1580(%r9), %edx
	movw	%dx, 2(%rcx,%rax,2)
	movzwl	1584(%r9), %edx
	movw	%dx, 4(%rcx,%rax,2)
	movzwl	1588(%r9), %edx
	movw	%dx, 6(%rcx,%rax,2)
	movq	136(%rsp), %rdx         # 8-byte Reload
.LBB25_413:                             # %.loopexit
                                        #   in Loop: Header=BB25_334 Depth=3
	incq	%rdx
	cmpq	$4, %rdx
	movq	%rdx, %rsi
	movq	320(%rsp), %r8          # 8-byte Reload
	movq	576(%rsp), %rax         # 8-byte Reload
	jne	.LBB25_334
# BB#414:                               #   in Loop: Header=BB25_333 Depth=2
	incq	%rax
	movq	%rax, %rcx
	movl	5924(%r9), %eax
	sarl	%eax
	cltq
	cmpq	%rax, %rcx
	jl	.LBB25_333
.LBB25_415:                             # %._crit_edge
                                        #   in Loop: Header=BB25_329 Depth=1
	movl	%r11d, 20(%rsp)         # 4-byte Spill
	cmpl	$0, 308(%rsp)           # 4-byte Folded Reload
	je	.LBB25_418
# BB#416:                               #   in Loop: Header=BB25_329 Depth=1
	movq	dec_picture(%rip), %rax
	movq	316928(%rax), %rax
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	leal	(%rbx,%rbx), %esi
	movq	%r9, %rdi
	movl	%esi, 80(%rsp)          # 4-byte Spill
	movq	%r9, %rbp
	callq	itrans_sp_chroma
	movq	%rbp, %r9
	leal	1(%rbx,%rbx), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	$4, %r14d
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB25_417:                             # %.preheader
                                        #   Parent Loop BB25_329 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%edx, 24(%rsp)          # 4-byte Spill
	movl	88(%r9), %eax
	addl	%edx, %eax
	movslq	%eax, %rbp
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	96(%rax), %rbx
	xorl	%esi, %esi
	movl	$1, %r9d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	80(%rsp), %ecx          # 4-byte Reload
	movl	%r14d, %r8d
	callq	itrans
	movq	56(%rsp), %r15          # 8-byte Reload
	movq	(%r15,%rbp,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1384(%rcx), %ecx
	movw	%cx, (%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1388(%rcx), %ecx
	movw	%cx, 2(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1392(%rcx), %ecx
	movw	%cx, 4(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1396(%rcx), %ecx
	movw	%cx, 6(%rax,%rbx,2)
	movq	8(%r15,%rbp,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1448(%rcx), %ecx
	movw	%cx, (%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1452(%rcx), %ecx
	movw	%cx, 2(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1456(%rcx), %ecx
	movw	%cx, 4(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1460(%rcx), %ecx
	movw	%cx, 6(%rax,%rbx,2)
	movq	16(%r15,%rbp,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1512(%rcx), %ecx
	movw	%cx, (%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1516(%rcx), %ecx
	movw	%cx, 2(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1520(%rcx), %ecx
	movw	%cx, 4(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1524(%rcx), %ecx
	movw	%cx, 6(%rax,%rbx,2)
	movq	24(%r15,%rbp,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1576(%rcx), %ecx
	movw	%cx, (%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1580(%rcx), %ecx
	movw	%cx, 2(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1584(%rcx), %ecx
	movw	%cx, 4(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1588(%rcx), %ecx
	movw	%cx, 6(%rax,%rbx,2)
	movq	8(%rsp), %rax           # 8-byte Reload
	movslq	96(%rax), %rbx
	movl	$4, %esi
	movl	$1, %r9d
	movq	8(%rsp), %rdi           # 8-byte Reload
	movl	24(%rsp), %edx          # 4-byte Reload
	movl	64(%rsp), %ecx          # 4-byte Reload
	movl	%r14d, %r8d
	callq	itrans
	movl	24(%rsp), %edx          # 4-byte Reload
	movq	(%r15,%rbp,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1384(%rcx), %ecx
	movw	%cx, 8(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1388(%rcx), %ecx
	movw	%cx, 10(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1392(%rcx), %ecx
	movw	%cx, 12(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1396(%rcx), %ecx
	movw	%cx, 14(%rax,%rbx,2)
	movq	8(%r15,%rbp,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1448(%rcx), %ecx
	movw	%cx, 8(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1452(%rcx), %ecx
	movw	%cx, 10(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1456(%rcx), %ecx
	movw	%cx, 12(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1460(%rcx), %ecx
	movw	%cx, 14(%rax,%rbx,2)
	movq	16(%r15,%rbp,8), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1512(%rcx), %ecx
	movw	%cx, 8(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1516(%rcx), %ecx
	movw	%cx, 10(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1520(%rcx), %ecx
	movw	%cx, 12(%rax,%rbx,2)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movzwl	1524(%rcx), %ecx
	movw	%cx, 14(%rax,%rbx,2)
	movq	24(%r15,%rbp,8), %rax
	movq	8(%rsp), %r9            # 8-byte Reload
	movzwl	1576(%r9), %ecx
	movw	%cx, 8(%rax,%rbx,2)
	movzwl	1580(%r9), %ecx
	movw	%cx, 10(%rax,%rbx,2)
	movzwl	1584(%r9), %ecx
	movw	%cx, 12(%rax,%rbx,2)
	movzwl	1588(%r9), %ecx
	incl	%r14d
	addl	$4, %edx
	cmpl	$6, %r14d
	movw	%cx, 14(%rax,%rbx,2)
	jne	.LBB25_417
.LBB25_418:                             # %.loopexit2023
                                        #   in Loop: Header=BB25_329 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	$2, %rcx
	movq	312(%rsp), %r8          # 8-byte Reload
	movl	20(%rsp), %r11d         # 4-byte Reload
	movzwl	48(%rsp), %r15d         # 2-byte Folded Reload
	jne	.LBB25_329
	jmp	.LBB25_13
.LBB25_419:
	movl	$1, %eax
.LBB25_420:                             # %.loopexit2025
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	decode_one_macroblock, .Lfunc_end25-decode_one_macroblock
	.cfi_endproc

	.type	SNGL_SCAN,@object       # @SNGL_SCAN
	.section	.rodata,"a",@progbits
	.globl	SNGL_SCAN
	.p2align	4
SNGL_SCAN:
	.zero	2
	.asciz	"\001"
	.ascii	"\000\001"
	.ascii	"\000\002"
	.zero	2,1
	.asciz	"\002"
	.asciz	"\003"
	.ascii	"\002\001"
	.ascii	"\001\002"
	.ascii	"\000\003"
	.ascii	"\001\003"
	.zero	2,2
	.ascii	"\003\001"
	.ascii	"\003\002"
	.ascii	"\002\003"
	.zero	2,3
	.size	SNGL_SCAN, 32

	.type	FIELD_SCAN,@object      # @FIELD_SCAN
	.globl	FIELD_SCAN
	.p2align	4
FIELD_SCAN:
	.zero	2
	.ascii	"\000\001"
	.asciz	"\001"
	.ascii	"\000\002"
	.ascii	"\000\003"
	.zero	2,1
	.ascii	"\001\002"
	.ascii	"\001\003"
	.asciz	"\002"
	.ascii	"\002\001"
	.zero	2,2
	.ascii	"\002\003"
	.asciz	"\003"
	.ascii	"\003\001"
	.ascii	"\003\002"
	.zero	2,3
	.size	FIELD_SCAN, 32

	.type	NCBP,@object            # @NCBP
	.globl	NCBP
	.p2align	4
NCBP:
	.asciz	"\017"
	.ascii	"\000\001"
	.ascii	"\007\002"
	.ascii	"\013\004"
	.ascii	"\r\b"
	.ascii	"\016\003"
	.ascii	"\003\005"
	.ascii	"\005\n"
	.ascii	"\n\f"
	.ascii	"\f\017"
	.ascii	"\001\007"
	.ascii	"\002\013"
	.ascii	"\004\r"
	.ascii	"\b\016"
	.zero	2,6
	.zero	2,9
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.zero	2
	.asciz	"/"
	.ascii	"\037\020"
	.ascii	"\017\001"
	.ascii	"\000\002"
	.ascii	"\027\004"
	.ascii	"\033\b"
	.ascii	"\035 "
	.ascii	"\036\003"
	.ascii	"\007\005"
	.ascii	"\013\n"
	.ascii	"\r\f"
	.ascii	"\016\017"
	.ascii	"'/"
	.ascii	"+\007"
	.ascii	"-\013"
	.ascii	".\r"
	.ascii	"\020\016"
	.ascii	"\003\006"
	.ascii	"\005\t"
	.ascii	"\n\037"
	.ascii	"\f#"
	.ascii	"\023%"
	.ascii	"\025*"
	.ascii	"\032,"
	.ascii	"\034!"
	.ascii	"#\""
	.ascii	"%$"
	.ascii	"*("
	.ascii	",'"
	.ascii	"\001+"
	.ascii	"\002-"
	.ascii	"\004."
	.ascii	"\b\021"
	.ascii	"\021\022"
	.ascii	"\022\024"
	.ascii	"\024\030"
	.ascii	"\030\023"
	.ascii	"\006\025"
	.ascii	"\t\032"
	.ascii	"\026\034"
	.ascii	"\031\027"
	.ascii	" \033"
	.ascii	"!\035"
	.ascii	"\"\036"
	.ascii	"$\026"
	.ascii	"(\031"
	.zero	2,38
	.zero	2,41
	.size	NCBP, 192

	.type	BLOCK_STEP,@object      # @BLOCK_STEP
	.globl	BLOCK_STEP
	.p2align	4
BLOCK_STEP:
	.zero	8
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	4                       # 0x4
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.size	BLOCK_STEP, 64

	.type	dequant_coef,@object    # @dequant_coef
	.globl	dequant_coef
	.p2align	4
dequant_coef:
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	10                      # 0xa
	.long	13                      # 0xd
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	11                      # 0xb
	.long	14                      # 0xe
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	13                      # 0xd
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	14                      # 0xe
	.long	18                      # 0x12
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	16                      # 0x10
	.long	20                      # 0x14
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	20                      # 0x14
	.long	25                      # 0x19
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	18                      # 0x12
	.long	23                      # 0x17
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.long	23                      # 0x17
	.long	29                      # 0x1d
	.size	dequant_coef, 384

	.type	QP_SCALE_CR,@object     # @QP_SCALE_CR
	.globl	QP_SCALE_CR
	.p2align	4
QP_SCALE_CR:
	.ascii	"\000\001\002\003\004\005\006\007\b\t\n\013\f\r\016\017\020\021\022\023\024\025\026\027\030\031\032\033\034\035\035\036\037  !\"\"##$$%%%&&&''''"
	.size	QP_SCALE_CR, 52

	.type	SNGL_SCAN8x8,@object    # @SNGL_SCAN8x8
	.globl	SNGL_SCAN8x8
	.p2align	4
SNGL_SCAN8x8:
	.zero	2
	.asciz	"\001"
	.ascii	"\000\001"
	.ascii	"\000\002"
	.zero	2,1
	.asciz	"\002"
	.asciz	"\003"
	.ascii	"\002\001"
	.ascii	"\001\002"
	.ascii	"\000\003"
	.ascii	"\000\004"
	.ascii	"\001\003"
	.zero	2,2
	.ascii	"\003\001"
	.asciz	"\004"
	.asciz	"\005"
	.ascii	"\004\001"
	.ascii	"\003\002"
	.ascii	"\002\003"
	.ascii	"\001\004"
	.ascii	"\000\005"
	.ascii	"\000\006"
	.ascii	"\001\005"
	.ascii	"\002\004"
	.zero	2,3
	.ascii	"\004\002"
	.ascii	"\005\001"
	.asciz	"\006"
	.asciz	"\007"
	.ascii	"\006\001"
	.ascii	"\005\002"
	.ascii	"\004\003"
	.ascii	"\003\004"
	.ascii	"\002\005"
	.ascii	"\001\006"
	.ascii	"\000\007"
	.ascii	"\001\007"
	.ascii	"\002\006"
	.ascii	"\003\005"
	.zero	2,4
	.ascii	"\005\003"
	.ascii	"\006\002"
	.ascii	"\007\001"
	.ascii	"\007\002"
	.ascii	"\006\003"
	.ascii	"\005\004"
	.ascii	"\004\005"
	.ascii	"\003\006"
	.ascii	"\002\007"
	.ascii	"\003\007"
	.ascii	"\004\006"
	.zero	2,5
	.ascii	"\006\004"
	.ascii	"\007\003"
	.ascii	"\007\004"
	.ascii	"\006\005"
	.ascii	"\005\006"
	.ascii	"\004\007"
	.ascii	"\005\007"
	.zero	2,6
	.ascii	"\007\005"
	.ascii	"\007\006"
	.ascii	"\006\007"
	.zero	2,7
	.size	SNGL_SCAN8x8, 128

	.type	FIELD_SCAN8x8,@object   # @FIELD_SCAN8x8
	.globl	FIELD_SCAN8x8
	.p2align	4
FIELD_SCAN8x8:
	.zero	2
	.ascii	"\000\001"
	.ascii	"\000\002"
	.asciz	"\001"
	.zero	2,1
	.ascii	"\000\003"
	.ascii	"\000\004"
	.ascii	"\001\002"
	.asciz	"\002"
	.ascii	"\001\003"
	.ascii	"\000\005"
	.ascii	"\000\006"
	.ascii	"\000\007"
	.ascii	"\001\004"
	.ascii	"\002\001"
	.asciz	"\003"
	.zero	2,2
	.ascii	"\001\005"
	.ascii	"\001\006"
	.ascii	"\001\007"
	.ascii	"\002\003"
	.ascii	"\003\001"
	.asciz	"\004"
	.ascii	"\003\002"
	.ascii	"\002\004"
	.ascii	"\002\005"
	.ascii	"\002\006"
	.ascii	"\002\007"
	.zero	2,3
	.ascii	"\004\001"
	.asciz	"\005"
	.ascii	"\004\002"
	.ascii	"\003\004"
	.ascii	"\003\005"
	.ascii	"\003\006"
	.ascii	"\003\007"
	.ascii	"\004\003"
	.ascii	"\005\001"
	.asciz	"\006"
	.ascii	"\005\002"
	.zero	2,4
	.ascii	"\004\005"
	.ascii	"\004\006"
	.ascii	"\004\007"
	.ascii	"\005\003"
	.ascii	"\006\001"
	.ascii	"\006\002"
	.ascii	"\005\004"
	.zero	2,5
	.ascii	"\005\006"
	.ascii	"\005\007"
	.ascii	"\006\003"
	.asciz	"\007"
	.ascii	"\007\001"
	.ascii	"\006\004"
	.ascii	"\006\005"
	.zero	2,6
	.ascii	"\006\007"
	.ascii	"\007\002"
	.ascii	"\007\003"
	.ascii	"\007\004"
	.ascii	"\007\005"
	.ascii	"\007\006"
	.zero	2,7
	.size	FIELD_SCAN8x8, 128

	.type	SCAN_YUV422,@object     # @SCAN_YUV422
	.globl	SCAN_YUV422
	.p2align	4
SCAN_YUV422:
	.zero	2
	.ascii	"\000\001"
	.asciz	"\001"
	.ascii	"\000\002"
	.ascii	"\000\003"
	.zero	2,1
	.ascii	"\001\002"
	.ascii	"\001\003"
	.size	SCAN_YUV422, 16

	.type	subblk_offset_x,@object # @subblk_offset_x
	.globl	subblk_offset_x
	.p2align	4
subblk_offset_x:
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.ascii	"\000\004\000\004"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.ascii	"\000\004\000\004"
	.ascii	"\b\f\b\f"
	.size	subblk_offset_x, 96

	.type	subblk_offset_y,@object # @subblk_offset_y
	.globl	subblk_offset_y
	.p2align	4
subblk_offset_y:
	.ascii	"\000\000\004\004"
	.ascii	"\000\000\004\004"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\000\004\004"
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.ascii	"\b\b\f\f"
	.ascii	"\000\000\004\004"
	.ascii	"\000\000\004\004"
	.ascii	"\b\b\f\f"
	.ascii	"\b\b\f\f"
	.size	subblk_offset_y, 96

	.type	block8x8_idx,@object    # @block8x8_idx
	.data
	.globl	block8x8_idx
	.p2align	4
block8x8_idx:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.zero	16
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	3                       # 0x3
	.zero	16
	.zero	16
	.zero	16
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.size	block8x8_idx, 192

	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"maximum number of supported slices exceeded, please recompile with increased value for MAX_NUM_SLICES"
	.size	.L.str, 102

	.type	nal_startcode_follows,@object # @nal_startcode_follows
	.comm	nal_startcode_follows,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	interpret_mb_mode_B.offset2pdir16x16,@object # @interpret_mb_mode_B.offset2pdir16x16
	.section	.rodata,"a",@progbits
	.p2align	4
interpret_mb_mode_B.offset2pdir16x16:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	interpret_mb_mode_B.offset2pdir16x16, 48

	.type	interpret_mb_mode_B.offset2pdir16x8,@object # @interpret_mb_mode_B.offset2pdir16x8
	.p2align	4
interpret_mb_mode_B.offset2pdir16x8:
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.long	1                       # 0x1
	.long	1                       # 0x1
	.zero	8
	.long	0                       # 0x0
	.long	1                       # 0x1
	.zero	8
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	8
	.long	0                       # 0x0
	.long	2                       # 0x2
	.zero	8
	.long	1                       # 0x1
	.long	2                       # 0x2
	.zero	8
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	8
	.long	2                       # 0x2
	.long	1                       # 0x1
	.zero	8
	.long	2                       # 0x2
	.long	2                       # 0x2
	.zero	8
	.size	interpret_mb_mode_B.offset2pdir16x8, 176

	.type	interpret_mb_mode_B.offset2pdir8x16,@object # @interpret_mb_mode_B.offset2pdir8x16
	.p2align	4
interpret_mb_mode_B.offset2pdir8x16:
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.zero	8
	.long	1                       # 0x1
	.long	1                       # 0x1
	.zero	8
	.long	0                       # 0x0
	.long	1                       # 0x1
	.zero	8
	.long	1                       # 0x1
	.long	0                       # 0x0
	.zero	8
	.long	0                       # 0x0
	.long	2                       # 0x2
	.zero	8
	.long	1                       # 0x1
	.long	2                       # 0x2
	.zero	8
	.long	2                       # 0x2
	.long	0                       # 0x0
	.zero	8
	.long	2                       # 0x2
	.long	1                       # 0x1
	.zero	8
	.long	2                       # 0x2
	.long	2                       # 0x2
	.size	interpret_mb_mode_B.offset2pdir8x16, 176

	.type	interpret_mb_mode_SI.ICBPTAB,@object # @interpret_mb_mode_SI.ICBPTAB
	.p2align	4
interpret_mb_mode_SI.ICBPTAB:
	.long	0                       # 0x0
	.long	16                      # 0x10
	.long	32                      # 0x20
	.long	15                      # 0xf
	.long	31                      # 0x1f
	.long	47                      # 0x2f
	.size	interpret_mb_mode_SI.ICBPTAB, 24

	.type	SetB8Mode.p_v2b8,@object # @SetB8Mode.p_v2b8
	.p2align	4
SetB8Mode.p_v2b8:
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	11                      # 0xb
	.size	SetB8Mode.p_v2b8, 20

	.type	SetB8Mode.p_v2pd,@object # @SetB8Mode.p_v2pd
	.p2align	4
SetB8Mode.p_v2pd:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.size	SetB8Mode.p_v2pd, 20

	.type	SetB8Mode.b_v2b8,@object # @SetB8Mode.b_v2b8
	.p2align	4
SetB8Mode.b_v2b8:
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	5                       # 0x5
	.long	6                       # 0x6
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	11                      # 0xb
	.size	SetB8Mode.b_v2b8, 56

	.type	SetB8Mode.b_v2pd,@object # @SetB8Mode.b_v2pd
	.p2align	4
SetB8Mode.b_v2pd:
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	4294967295              # 0xffffffff
	.size	SetB8Mode.b_v2pd, 56

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"illegal chroma intra pred mode!\n"
	.size	.L.str.2, 33

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"temporal direct error\ncolocated block has ref that is unavailable"
	.size	.L.str.3, 66

	.type	predict_nnz_chroma.j_off_tab,@object # @predict_nnz_chroma.j_off_tab
	.section	.rodata,"a",@progbits
	.p2align	4
predict_nnz_chroma.j_off_tab:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.size	predict_nnz_chroma.j_off_tab, 48

	.type	readCoeff4x4_CAVLC.incVlc,@object # @readCoeff4x4_CAVLC.incVlc
	.p2align	4
readCoeff4x4_CAVLC.incVlc:
	.long	0                       # 0x0
	.long	3                       # 0x3
	.long	6                       # 0x6
	.long	12                      # 0xc
	.long	24                      # 0x18
	.long	48                      # 0x30
	.long	32768                   # 0x8000
	.size	readCoeff4x4_CAVLC.incVlc, 28

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"readCoeff4x4_CAVLC: invalid block type"
	.size	.L.str.4, 39

	.type	dequant_coef8,@object   # @dequant_coef8
	.section	.rodata,"a",@progbits
	.p2align	4
dequant_coef8:
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	20                      # 0x14
	.long	19                      # 0x13
	.long	25                      # 0x19
	.long	19                      # 0x13
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	25                      # 0x19
	.long	24                      # 0x18
	.long	32                      # 0x20
	.long	24                      # 0x18
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	19                      # 0x13
	.long	18                      # 0x12
	.long	24                      # 0x18
	.long	18                      # 0x12
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	22                      # 0x16
	.long	21                      # 0x15
	.long	28                      # 0x1c
	.long	21                      # 0x15
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	21                      # 0x15
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	19                      # 0x13
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	26                      # 0x1a
	.long	24                      # 0x18
	.long	33                      # 0x21
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	33                      # 0x21
	.long	31                      # 0x1f
	.long	42                      # 0x2a
	.long	31                      # 0x1f
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	24                      # 0x18
	.long	23                      # 0x17
	.long	31                      # 0x1f
	.long	23                      # 0x17
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	28                      # 0x1c
	.long	26                      # 0x1a
	.long	35                      # 0x23
	.long	26                      # 0x1a
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	35                      # 0x23
	.long	33                      # 0x21
	.long	45                      # 0x2d
	.long	33                      # 0x21
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	26                      # 0x1a
	.long	25                      # 0x19
	.long	33                      # 0x21
	.long	25                      # 0x19
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	32                      # 0x20
	.long	30                      # 0x1e
	.long	40                      # 0x28
	.long	30                      # 0x1e
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	40                      # 0x28
	.long	38                      # 0x26
	.long	51                      # 0x33
	.long	38                      # 0x26
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	30                      # 0x1e
	.long	28                      # 0x1c
	.long	38                      # 0x26
	.long	28                      # 0x1c
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	36                      # 0x24
	.long	34                      # 0x22
	.long	46                      # 0x2e
	.long	34                      # 0x22
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	46                      # 0x2e
	.long	43                      # 0x2b
	.long	58                      # 0x3a
	.long	43                      # 0x2b
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.long	34                      # 0x22
	.long	32                      # 0x20
	.long	43                      # 0x2b
	.long	32                      # 0x20
	.size	dequant_coef8, 1536

	.type	qmatrix,@object         # @qmatrix
	.comm	qmatrix,64,16
	.type	InvLevelScale8x8Luma_Intra,@object # @InvLevelScale8x8Luma_Intra
	.comm	InvLevelScale8x8Luma_Intra,1536,16
	.type	InvLevelScale8x8Luma_Inter,@object # @InvLevelScale8x8Luma_Inter
	.comm	InvLevelScale8x8Luma_Inter,1536,16
	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"mb_qp_delta is out of range"
	.size	.L.str.5, 28

	.type	InvLevelScale4x4Luma_Intra,@object # @InvLevelScale4x4Luma_Intra
	.comm	InvLevelScale4x4Luma_Intra,384,16
	.type	InvLevelScale4x4Luma_Inter,@object # @InvLevelScale4x4Luma_Inter
	.comm	InvLevelScale4x4Luma_Inter,384,16
	.type	InvLevelScale4x4Chroma_Intra,@object # @InvLevelScale4x4Chroma_Intra
	.comm	InvLevelScale4x4Chroma_Intra,768,16
	.type	InvLevelScale4x4Chroma_Inter,@object # @InvLevelScale4x4Chroma_Inter
	.comm	InvLevelScale4x4Chroma_Inter,768,16
	.type	cofuv_blk_x,@object     # @cofuv_blk_x
	.section	.rodata,"a",@progbits
	.p2align	4
cofuv_blk_x:
	.ascii	"\000\001\000\001"
	.ascii	"\002\003\002\003"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\001\000\001"
	.ascii	"\000\001\000\001"
	.ascii	"\002\003\002\003"
	.ascii	"\002\003\002\003"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\000\001\000\001"
	.ascii	"\002\003\002\003"
	.ascii	"\000\001\000\001"
	.ascii	"\002\003\002\003"
	.ascii	"\000\001\000\001"
	.ascii	"\002\003\002\003"
	.ascii	"\000\001\000\001"
	.ascii	"\002\003\002\003"
	.size	cofuv_blk_x, 96

	.type	cofuv_blk_y,@object     # @cofuv_blk_y
	.p2align	4
cofuv_blk_y:
	.ascii	"\004\004\005\005"
	.ascii	"\004\004\005\005"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\004\004\005\005"
	.ascii	"\006\006\007\007"
	.ascii	"\004\004\005\005"
	.ascii	"\006\006\007\007"
	.zero	4
	.zero	4
	.zero	4
	.zero	4
	.ascii	"\004\004\005\005"
	.ascii	"\004\004\005\005"
	.ascii	"\006\006\007\007"
	.ascii	"\006\006\007\007"
	.ascii	"\b\b\t\t"
	.ascii	"\b\b\t\t"
	.ascii	"\n\n\013\013"
	.ascii	"\n\n\013\013"
	.size	cofuv_blk_y, 96

	.type	cbp_blk_chroma,@object  # @cbp_blk_chroma
	.section	.rodata.cst32,"aM",@progbits,32
	.p2align	4
cbp_blk_chroma:
	.ascii	"\020\021\022\023"
	.ascii	"\024\025\026\027"
	.ascii	"\030\031\032\033"
	.ascii	"\034\035\036\037"
	.ascii	" !\"#"
	.ascii	"$%&'"
	.ascii	"()*+"
	.ascii	",-./"
	.size	cbp_blk_chroma, 32

	.type	decode_one_macroblock.decode_block_scan,@object # @decode_one_macroblock.decode_block_scan
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
decode_one_macroblock.decode_block_scan:
	.ascii	"\000\001\004\005\002\003\006\007\b\t\f\r\n\013\016\017"
	.size	decode_one_macroblock.decode_block_scan, 16

	.type	imgY_ref,@object        # @imgY_ref
	.comm	imgY_ref,8,8
	.type	imgUV_ref,@object       # @imgUV_ref
	.comm	imgUV_ref,8,8
	.type	ReMapRef,@object        # @ReMapRef
	.comm	ReMapRef,80,16
	.type	Bframe_ctr,@object      # @Bframe_ctr
	.comm	Bframe_ctr,4,4
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	g_nFrame,@object        # @g_nFrame
	.comm	g_nFrame,4,4
	.type	TopFieldForSkip_Y,@object # @TopFieldForSkip_Y
	.comm	TopFieldForSkip_Y,1024,16
	.type	TopFieldForSkip_UV,@object # @TopFieldForSkip_UV
	.comm	TopFieldForSkip_UV,2048,16
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	p_out,@object           # @p_out
	.comm	p_out,4,4
	.type	p_ref,@object           # @p_ref
	.comm	p_ref,4,4
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	previous_frame_num,@object # @previous_frame_num
	.comm	previous_frame_num,4,4
	.type	ref_flag,@object        # @ref_flag
	.comm	ref_flag,68,16
	.type	Is_primary_correct,@object # @Is_primary_correct
	.comm	Is_primary_correct,4,4
	.type	Is_redundant_correct,@object # @Is_redundant_correct
	.comm	Is_redundant_correct,4,4
	.type	redundant_slice_ref_idx,@object # @redundant_slice_ref_idx
	.comm	redundant_slice_ref_idx,4,4
	.type	getNeighbour,@object    # @getNeighbour
	.comm	getNeighbour,8,8
	.type	get_mb_block_pos,@object # @get_mb_block_pos
	.comm	get_mb_block_pos,8,8
	.type	cofAC8x8_intra,@object  # @cofAC8x8_intra
	.comm	cofAC8x8_intra,8,8
	.type	cofAC8x8_iintra,@object # @cofAC8x8_iintra
	.comm	cofAC8x8_iintra,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Partition Mode is not supported"
	.size	.Lstr, 32

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.quad	InvLevelScale8x8Luma_Intra
	.quad	InvLevelScale8x8Luma_Intra
	.quad	InvLevelScale8x8Luma_Inter
	.quad	InvLevelScale8x8Luma_Intra
	.quad	InvLevelScale8x8Luma_Intra
	.quad	InvLevelScale8x8Luma_Intra
	.size	.Lswitch.table, 48

	.type	.Lswitch.table.1,@object # @switch.table.1
	.p2align	4
.Lswitch.table.1:
	.long	9                       # 0x9
	.long	10                      # 0xa
	.long	9                       # 0x9
	.long	12                      # 0xc
	.long	13                      # 0xd
	.long	14                      # 0xe
	.size	.Lswitch.table.1, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
