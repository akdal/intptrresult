	.text
	.file	"zstack.bc"
	.globl	zpop
	.p2align	4, 0x90
	.type	zpop,@function
zpop:                                   # @zpop
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB0_2
# BB#1:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB0_2:
	retq
.Lfunc_end0:
	.size	zpop, .Lfunc_end0-zpop
	.cfi_endproc

	.globl	zexch
	.p2align	4, 0x90
	.type	zexch,@function
zexch:                                  # @zexch
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs+8(%rip)
	ja	.LBB1_2
# BB#1:
	movups	-16(%rdi), %xmm0
	movaps	%xmm0, -24(%rsp)
	movups	(%rdi), %xmm0
	movups	%xmm0, -16(%rdi)
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, (%rdi)
	xorl	%eax, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	zexch, .Lfunc_end1-zexch
	.cfi_endproc

	.globl	zdup
	.p2align	4, 0x90
	.type	zdup,@function
zdup:                                   # @zdup
	.cfi_startproc
# BB#0:
	movl	$-17, %eax
	cmpq	%rdi, osp_nargs(%rip)
	ja	.LBB2_4
# BB#1:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB2_3
# BB#2:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB2_3:
	movups	(%rdi), %xmm0
	movups	%xmm0, (%rax)
	xorl	%eax, %eax
.LBB2_4:
	retq
.Lfunc_end2:
	.size	zdup, .Lfunc_end2-zdup
	.cfi_endproc

	.globl	zindex
	.p2align	4, 0x90
	.type	zindex,@function
zindex:                                 # @zindex
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB3_3
# BB#1:
	movq	(%rdi), %rcx
	movq	%rdi, %rdx
	subq	osbot(%rip), %rdx
	sarq	$4, %rdx
	movl	$-15, %eax
	cmpq	%rdx, %rcx
	jae	.LBB3_3
# BB#2:
	xorq	$-1, %rcx
	shlq	$32, %rcx
	sarq	$28, %rcx
	movups	(%rdi,%rcx), %xmm0
	movups	%xmm0, (%rdi)
	xorl	%eax, %eax
.LBB3_3:
	retq
.Lfunc_end3:
	.size	zindex, .Lfunc_end3-zindex
	.cfi_endproc

	.globl	zroll
	.p2align	4, 0x90
	.type	zroll,@function
zroll:                                  # @zroll
	.cfi_startproc
# BB#0:
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB4_14
# BB#1:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB4_14
# BB#2:
	leaq	-16(%rdi), %rcx
	movq	(%rcx), %r8
	subq	osbot(%rip), %rcx
	sarq	$4, %rcx
	movl	$-15, %eax
	cmpq	%rcx, %r8
	ja	.LBB4_14
# BB#3:
	testl	%r8d, %r8d
	je	.LBB4_4
# BB#5:
	movq	(%rdi), %rax
	movslq	%r8d, %rcx
	cqto
	idivq	%rcx
	addq	$-32, osp(%rip)
	testl	%edx, %edx
	js	.LBB4_6
# BB#7:
	testl	%edx, %edx
	jne	.LBB4_8
	jmp	.LBB4_13
.LBB4_4:
	addq	$-32, osp(%rip)
	jmp	.LBB4_13
.LBB4_6:
	addl	%r8d, %edx
.LBB4_8:                                # %.lr.ph61
	addq	$-32, %rdi
	movl	%r8d, %r9d
	subl	%edx, %r9d
	shlq	$4, %rcx
	subq	%rcx, %rdi
	addq	$16, %rdi
	xorl	%r10d, %r10d
	movl	%r8d, %esi
	.p2align	4, 0x90
.LBB4_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
	movq	%r10, %rax
	shlq	$4, %rax
	leaq	(%rdi,%rax), %r11
	movups	(%rdi,%rax), %xmm0
	movaps	%xmm0, -24(%rsp)
	decl	%esi
	leal	(%r9,%r10), %eax
	cltd
	idivl	%r8d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpq	%r10, %rdx
	jne	.LBB4_11
# BB#10:                                #   in Loop: Header=BB4_9 Depth=1
	movq	%r11, %rcx
	jmp	.LBB4_12
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph
                                        #   Parent Loop BB4_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rax
	movq	%rax, %rdx
	shlq	$4, %rdx
	leaq	(%rdi,%rdx), %rcx
	movups	(%rdi,%rdx), %xmm0
	movups	%xmm0, (%r11)
	decl	%esi
	addl	%r9d, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	cltd
	idivl	%r8d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpq	%r10, %rdx
	movq	%rcx, %r11
	jne	.LBB4_11
.LBB4_12:                               # %._crit_edge
                                        #   in Loop: Header=BB4_9 Depth=1
	movaps	-24(%rsp), %xmm0
	movups	%xmm0, (%rcx)
	incq	%r10
	testl	%esi, %esi
	jne	.LBB4_9
.LBB4_13:
	xorl	%eax, %eax
.LBB4_14:                               # %.loopexit
	retq
.Lfunc_end4:
	.size	zroll, .Lfunc_end4-zroll
	.cfi_endproc

	.globl	zclear_stack
	.p2align	4, 0x90
	.type	zclear_stack,@function
zclear_stack:                           # @zclear_stack
	.cfi_startproc
# BB#0:
	movq	osbot(%rip), %rax
	addq	$-16, %rax
	movq	%rax, osp(%rip)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	zclear_stack, .Lfunc_end5-zclear_stack
	.cfi_endproc

	.globl	zcount
	.p2align	4, 0x90
	.type	zcount,@function
zcount:                                 # @zcount
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB6_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB6_2:
	subq	osbot(%rip), %rax
	sarq	$4, %rax
	movq	%rax, 16(%rdi)
	movw	$20, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	zcount, .Lfunc_end6-zcount
	.cfi_endproc

	.globl	zcleartomark
	.p2align	4, 0x90
	.type	zcleartomark,@function
zcleartomark:                           # @zcleartomark
	.cfi_startproc
# BB#0:
	movq	osbot(%rip), %rax
	.p2align	4, 0x90
.LBB7_1:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rax, %rdi
	jb	.LBB7_2
# BB#3:                                 #   in Loop: Header=BB7_1 Depth=1
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	addq	$-16, %rdi
	cmpl	$24, %ecx
	jne	.LBB7_1
# BB#4:
	movq	%rdi, osp(%rip)
	xorl	%eax, %eax
	retq
.LBB7_2:
	movl	$-24, %eax
	retq
.Lfunc_end7:
	.size	zcleartomark, .Lfunc_end7-zcleartomark
	.cfi_endproc

	.globl	zcounttomark
	.p2align	4, 0x90
	.type	zcounttomark,@function
zcounttomark:                           # @zcounttomark
	.cfi_startproc
# BB#0:
	movq	osbot(%rip), %rdx
	movl	$-24, %eax
	cmpq	%rdi, %rdx
	ja	.LBB8_7
# BB#1:                                 # %.lr.ph.preheader
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	8(%rcx), %esi
	andl	$252, %esi
	cmpl	$24, %esi
	je	.LBB8_3
# BB#6:                                 #   in Loop: Header=BB8_2 Depth=1
	addq	$-16, %rcx
	cmpq	%rdx, %rcx
	jae	.LBB8_2
.LBB8_7:                                # %.loopexit
	retq
.LBB8_3:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB8_5
# BB#4:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB8_5:
	subq	%rcx, %rax
	sarq	$4, %rax
	decq	%rax
	movq	%rax, 16(%rdi)
	movw	$20, 24(%rdi)
	xorl	%eax, %eax
	retq
.Lfunc_end8:
	.size	zcounttomark, .Lfunc_end8-zcounttomark
	.cfi_endproc

	.globl	zstack_op_init
	.p2align	4, 0x90
	.type	zstack_op_init,@function
zstack_op_init:                         # @zstack_op_init
	.cfi_startproc
# BB#0:
	movl	$zstack_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end9:
	.size	zstack_op_init, .Lfunc_end9-zstack_op_init
	.cfi_endproc

	.type	zstack_op_init.my_defs,@object # @zstack_op_init.my_defs
	.data
	.p2align	4
zstack_op_init.my_defs:
	.quad	.L.str
	.quad	zclear_stack
	.quad	.L.str.1
	.quad	zcleartomark
	.quad	.L.str.2
	.quad	zcount
	.quad	.L.str.3
	.quad	zcounttomark
	.quad	.L.str.4
	.quad	zdup
	.quad	.L.str.5
	.quad	zexch
	.quad	.L.str.6
	.quad	zindex
	.quad	.L.str.7
	.quad	zpop
	.quad	.L.str.8
	.quad	zroll
	.zero	16
	.size	zstack_op_init.my_defs, 160

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"0clear"
	.size	.L.str, 7

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"0cleartomark"
	.size	.L.str.1, 13

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"0count"
	.size	.L.str.2, 7

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"0counttomark"
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"1dup"
	.size	.L.str.4, 5

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"2exch"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"2index"
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"1pop"
	.size	.L.str.7, 5

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"2roll"
	.size	.L.str.8, 6


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
