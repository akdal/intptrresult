	.text
	.file	"random.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4636737291354636288     # double 100
.LCPI0_1:
	.quad	4684049276697837568     # double 139968
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$400000000, %ebx        # imm = 0x17D78400
	cmpl	$2, %edi
	jne	.LBB0_3
# BB#1:                                 # %.preheader
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
	testl	%ebx, %ebx
	je	.LBB0_2
.LBB0_3:                                # %.lr.ph
	movq	_ZZ10gen_randomdE4last(%rip), %rcx
	testb	$1, %bl
	jne	.LBB0_5
# BB#4:
	movl	%ebx, %esi
	cmpl	$1, %ebx
	jne	.LBB0_7
	jmp	.LBB0_9
.LBB0_5:
	leal	-1(%rbx), %esi
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movabsq	$4318579316753219217, %rdx # imm = 0x3BEEAD01FD6CBE91
	movq	%rcx, %rax
	imulq	%rdx
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	cmpl	$1, %ebx
	je	.LBB0_9
.LBB0_7:                                # %.lr.ph.new
	movabsq	$4318579316753219217, %rdi # imm = 0x3BEEAD01FD6CBE91
	.p2align	4, 0x90
.LBB0_8:                                # =>This Inner Loop Header: Depth=1
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	imulq	$3877, %rcx, %rcx       # imm = 0xF25
	addq	$29573, %rcx            # imm = 0x7385
	movq	%rcx, %rax
	imulq	%rdi
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$15, %rdx
	addq	%rax, %rdx
	imulq	$139968, %rdx, %rax     # imm = 0x222C0
	subq	%rax, %rcx
	addl	$-2, %esi
	jne	.LBB0_8
.LBB0_9:                                # %._crit_edge
	movq	%rcx, _ZZ10gen_randomdE4last(%rip)
	cvtsi2sdq	%rcx, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	divsd	.LCPI0_1(%rip), %xmm0
.LBB0_10:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	$9, _ZSt4cout+8(%rax)
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	orl	$4, _ZSt4cout+24(%rax)
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertIdEERSoT_
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB0_15
# BB#11:                                # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB0_13
# BB#12:
	movb	67(%rbx), %al
	jmp	.LBB0_14
.LBB0_13:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB0_14:                               # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
	xorpd	%xmm0, %xmm0
	jmp	.LBB0_10
.LBB0_15:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_random.ii,@function
_GLOBAL__sub_I_random.ii:               # @_GLOBAL__sub_I_random.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end1:
	.size	_GLOBAL__sub_I_random.ii, .Lfunc_end1-_GLOBAL__sub_I_random.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.hidden	__dso_handle
	.type	_ZZ10gen_randomdE4last,@object # @_ZZ10gen_randomdE4last
	.section	.data._ZZ10gen_randomdE4last,"aGw",@progbits,_ZZ10gen_randomdE4last,comdat
	.weak	_ZZ10gen_randomdE4last
	.p2align	3
_ZZ10gen_randomdE4last:
	.quad	42                      # 0x2a
	.size	_ZZ10gen_randomdE4last, 8

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_random.ii

	.ident	"clang version 5.0.0 (https://github.com/aqjune/clang-intptr.git 8e2e88f063bfae95e3be980dbf26473d5a086d27) (https://github.com/aqjune/llvm-intptr.git 51d41dcad277119de970b737044ce4a7e91ad33d)"
	.section	".note.GNU-stack","",@progbits
