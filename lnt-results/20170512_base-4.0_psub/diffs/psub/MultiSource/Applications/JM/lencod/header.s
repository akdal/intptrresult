	.text
	.file	"header.bc"
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_0:
	.long	1065353216              # float 1
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_1:
	.quad	4604418534313441775     # double 0.69314718055994529
	.text
	.globl	SliceHeader
	.p2align	4, 0x90
	.type	SliceHeader,@function
SliceHeader:                            # @SliceHeader
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	input(%rip), %rax
	movslq	4016(%rax), %rax
	movq	assignSE2partition(,%rax,8), %rax
	movq	img(%rip), %rcx
	movq	14216(%rcx), %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	24(%rdx), %rdx
	movslq	(%rax), %rax
	imulq	$104, %rax, %rax
	movq	(%rdx,%rax), %r15
	movl	12(%rcx), %esi
	cmpl	$0, 15268(%rcx)
	setne	%cl
	sarl	%cl, %esi
	movl	$.L.str, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %ebx
	movq	img(%rip), %rax
	movl	20(%rax), %esi
	cmpl	$4, %esi
	jae	.LBB0_2
# BB#1:                                 # %switch.lookup.i
	addl	$5, %esi
	jmp	.LBB0_3
.LBB0_2:
	movl	$.L.str.25, %edi
	movl	$1, %esi
	callq	error
	xorl	%esi, %esi
.LBB0_3:                                # %get_picture_type.exit
	movl	$.L.str.1, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	active_pps(%rip), %rax
	movl	4(%rax), %esi
	movl	$.L.str.2, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	log2_max_frame_num_minus4(%rip), %edi
	addl	$4, %edi
	movq	img(%rip), %rax
	movl	15332(%rax), %edx
	movl	$.L.str.3, %esi
	movq	%r15, %rcx
	callq	u_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	active_sps(%rip), %rax
	xorl	%r14d, %r14d
	cmpl	$0, 1148(%rax)
	je	.LBB0_5
# BB#4:
	movl	%ebp, %r12d
	jmp	.LBB0_7
.LBB0_5:
	movq	img(%rip), %rax
	movl	24(%rax), %ebx
	decl	%ebx
	cmpl	$2, %ebx
	sbbl	%r14d, %r14d
	andl	$1, %r14d
	movl	$.L.str.4, %edi
	movl	%r14d, %esi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %r12d
	addl	%ebp, %r12d
	cmpl	$1, %ebx
	ja	.LBB0_7
# BB#6:
	movq	img(%rip), %rax
	xorl	%esi, %esi
	cmpl	$2, 24(%rax)
	sete	%sil
	movl	$.L.str.5, %edi
	movq	%r15, %rdx
	callq	u_1
	addl	%eax, %r12d
.LBB0_7:
	movq	img(%rip), %rax
	movq	14208(%rax), %rcx
	cmpl	$0, 4(%rcx)
	je	.LBB0_9
# BB#8:
	movl	(%rax), %esi
	movl	%esi, %eax
	shrl	$31, %eax
	addl	%esi, %eax
	andl	$-2, %eax
	subl	%eax, %esi
	movl	$.L.str.6, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %r12d
	movq	img(%rip), %rax
.LBB0_9:
	cmpl	$0, 15272(%rax)
	jne	.LBB0_21
# BB#10:
	movq	active_sps(%rip), %rcx
	cmpl	$0, 1148(%rcx)
	je	.LBB0_12
.LBB0_16:
	leaq	15316(%rax), %rsi
.LBB0_17:                               # %.sink.split
	movl	log2_max_pic_order_cnt_lsb_minus4(%rip), %edi
	leal	4(%rdi), %ecx
	movl	$-1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	notl	%edx
	andl	(%rsi), %edx
	movl	%edx, 15296(%rax)
.LBB0_18:
	addl	$4, %edi
	movl	$.L.str.7, %esi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r15, %rcx
	callq	u_v
	addl	%eax, %r12d
	movq	img(%rip), %rax
	testl	%r14d, %r14d
	jne	.LBB0_21
# BB#19:
	movl	15356(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB0_21
# BB#20:
	movl	15300(%rax), %esi
	movl	$.L.str.8, %edi
	movq	%r15, %rdx
	callq	se_v
	addl	%eax, %r12d
	movq	img(%rip), %rax
.LBB0_21:
	cmpl	$1, 15272(%rax)
	jne	.LBB0_26
# BB#22:
	cmpl	$0, 15276(%rax)
	jne	.LBB0_26
# BB#23:
	movl	15304(%rax), %esi
	movl	$.L.str.9, %edi
	movq	%r15, %rdx
	callq	se_v
	addl	%eax, %r12d
	testl	%r14d, %r14d
	jne	.LBB0_26
# BB#24:
	movq	img(%rip), %rax
	movl	15356(%rax), %ecx
	testl	%ecx, %ecx
	je	.LBB0_26
# BB#25:
	movl	15308(%rax), %esi
	movl	$.L.str.10, %edi
	movq	%r15, %rdx
	callq	se_v
	addl	%eax, %r12d
.LBB0_26:
	movq	active_pps(%rip), %rax
	cmpl	$0, 228(%rax)
	je	.LBB0_28
# BB#27:
	movq	img(%rip), %rax
	movl	15264(%rax), %esi
	movl	$.L.str.11, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %r12d
.LBB0_28:
	movq	img(%rip), %rax
	movl	20(%rax), %ecx
	cmpl	$1, %ecx
	jne	.LBB0_30
# BB#29:
	movl	14452(%rax), %esi
	movl	$.L.str.12, %edi
	movq	%r15, %rdx
	callq	u_1
	addl	%eax, %r12d
	movq	img(%rip), %rax
	movl	20(%rax), %ecx
.LBB0_30:
	cmpl	$3, %ecx
	je	.LBB0_33
# BB#31:
	cmpl	$1, %ecx
	je	.LBB0_34
# BB#32:
	testl	%ecx, %ecx
	jne	.LBB0_40
.LBB0_33:
	movl	14456(%rax), %r14d
	movq	active_pps(%rip), %rax
	movl	184(%rax), %ebp
	jmp	.LBB0_36
.LBB0_34:
	movq	active_pps(%rip), %rcx
	movl	184(%rcx), %edx
	incl	%edx
	cmpl	%edx, 14456(%rax)
	jne	.LBB0_37
# BB#35:
	movl	14460(%rax), %r14d
	movl	188(%rcx), %ebp
.LBB0_36:
	incl	%ebp
	xorl	%esi, %esi
	cmpl	%ebp, %r14d
	setne	%sil
	movl	$.L.str.13, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	addl	%r12d, %ebx
	cmpl	%ebp, %r14d
	movl	%ebx, %r12d
	jne	.LBB0_38
	jmp	.LBB0_40
.LBB0_12:
	testl	%r14d, %r14d
	je	.LBB0_16
# BB#13:
	movl	24(%rax), %ecx
	cmpl	$1, %ecx
	je	.LBB0_16
# BB#14:
	cmpl	$2, %ecx
	jne	.LBB0_137
# BB#15:
	leaq	15320(%rax), %rsi
	jmp	.LBB0_17
.LBB0_37:                               # %.thread
	movl	$.L.str.13, %edi
	movl	$1, %esi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	addl	%r12d, %ebx
.LBB0_38:
	movq	img(%rip), %rax
	movl	14456(%rax), %esi
	decl	%esi
	movl	$.L.str.14, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %r12d
	addl	%ebx, %r12d
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB0_40
# BB#39:
	movl	14460(%rax), %esi
	decl	%esi
	movl	$.L.str.15, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %r12d
.LBB0_40:
	movq	img(%rip), %rax
	movq	14216(%rax), %rbp
	movq	input(%rip), %rcx
	cmpl	$0, 5084(%rcx)
	movl	%r12d, 20(%rsp)         # 4-byte Spill
	je	.LBB0_43
# BB#41:
	movl	redundant_coding(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB0_43
# BB#42:
	movl	$1, 48(%rbp)
	movq	56(%rbp), %rcx
	movl	$0, (%rcx)
	movl	$3, 4(%rcx)
	movl	redundant_ref_idx(%rip), %edx
	decl	%edx
	movq	64(%rbp), %r8
	movl	%edx, (%r8)
	movq	72(%rbp), %r9
	movl	$0, (%r9)
	movq	listX(%rip), %rdi
	movl	14456(%rax), %edx
	decl	%edx
	movl	$listXsize, %esi
	callq	reorder_ref_pic_list
	movq	img(%rip), %rax
.LBB0_43:
	xorl	%r12d, %r12d
	cmpl	$2, 20(%rax)
	je	.LBB0_53
# BB#44:
	movl	48(%rbp), %esi
	movl	$.L.str.26, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	cmpl	$0, 48(%rbp)
	je	.LBB0_52
# BB#45:                                # %.preheader76.i
	leaq	64(%rbp), %r13
	leaq	72(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	56(%rbp), %rax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_46:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%r14), %esi
	movl	$.L.str.27, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %r12d
	addl	%ebx, %r12d
	movq	56(%rbp), %rax
	movl	(%rax,%r14), %ecx
	cmpl	$2, %ecx
	jae	.LBB0_48
# BB#47:                                #   in Loop: Header=BB0_46 Depth=1
	movq	%r13, %rax
	movl	$.L.str.28, %edi
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_48:                               #   in Loop: Header=BB0_46 Depth=1
	jne	.LBB0_51
# BB#49:                                #   in Loop: Header=BB0_46 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$.L.str.29, %edi
.LBB0_50:                               # %.sink.split.i
                                        #   in Loop: Header=BB0_46 Depth=1
	movq	(%rax), %rax
	movl	(%rax,%r14), %esi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %r12d
	movq	56(%rbp), %rax
	movl	(%rax,%r14), %ecx
.LBB0_51:                               #   in Loop: Header=BB0_46 Depth=1
	addq	$4, %r14
	cmpl	$3, %ecx
	movl	%r12d, %ebx
	jne	.LBB0_46
	jmp	.LBB0_53
.LBB0_52:
	movl	%ebx, %r12d
.LBB0_53:                               # %.loopexit77.i
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	jne	.LBB0_63
# BB#54:
	movl	80(%rbp), %esi
	movl	$.L.str.30, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	addl	%r12d, %ebx
	cmpl	$0, 80(%rbp)
	je	.LBB0_62
# BB#55:                                # %.preheader.i
	leaq	96(%rbp), %r13
	leaq	104(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	88(%rbp), %rax
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_56:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax,%r14), %esi
	movl	$.L.str.31, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %r12d
	addl	%ebx, %r12d
	movq	88(%rbp), %rax
	movl	(%rax,%r14), %ecx
	cmpl	$2, %ecx
	jae	.LBB0_58
# BB#57:                                #   in Loop: Header=BB0_56 Depth=1
	movq	%r13, %rax
	movl	$.L.str.32, %edi
	jmp	.LBB0_60
	.p2align	4, 0x90
.LBB0_58:                               #   in Loop: Header=BB0_56 Depth=1
	jne	.LBB0_61
# BB#59:                                #   in Loop: Header=BB0_56 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	$.L.str.33, %edi
.LBB0_60:                               # %.sink.split69.i
                                        #   in Loop: Header=BB0_56 Depth=1
	movq	(%rax), %rax
	movl	(%rax,%r14), %esi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %r12d
	movq	88(%rbp), %rax
	movl	(%rax,%r14), %ecx
.LBB0_61:                               #   in Loop: Header=BB0_56 Depth=1
	addq	$4, %r14
	cmpl	$3, %ecx
	movl	%r12d, %ebx
	jne	.LBB0_56
	jmp	.LBB0_63
.LBB0_62:
	movl	%ebx, %r12d
.LBB0_63:                               # %ref_pic_list_reordering.exit
	addl	20(%rsp), %r12d         # 4-byte Folded Reload
	movq	img(%rip), %rax
	movl	20(%rax), %ecx
	cmpl	$3, %ecx
	je	.LBB0_66
# BB#64:                                # %ref_pic_list_reordering.exit
	cmpl	$1, %ecx
	je	.LBB0_68
# BB#65:                                # %ref_pic_list_reordering.exit
	testl	%ecx, %ecx
	jne	.LBB0_101
.LBB0_66:
	movq	active_pps(%rip), %rdx
	cmpl	$0, 192(%rdx)
	jne	.LBB0_69
# BB#67:
	cmpl	$1, %ecx
	jne	.LBB0_101
.LBB0_68:
	movq	active_pps(%rip), %rcx
	cmpl	$1, 196(%rcx)
	jne	.LBB0_101
.LBB0_69:
	movl	luma_log_weight_denom(%rip), %esi
	movl	$.L.str.43, %edi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %r14d
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB0_71
# BB#70:
	movl	chroma_log_weight_denom(%rip), %esi
	movl	$.L.str.44, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %r14d
.LBB0_71:                               # %.preheader79.i
	movq	img(%rip), %rax
	cmpl	$0, 14456(%rax)
	jle	.LBB0_85
# BB#72:                                # %.lr.ph88.i.preheader
	xorl	%r13d, %r13d
	jmp	.LBB0_75
.LBB0_73:                               #   in Loop: Header=BB0_75 Depth=1
	movl	$.L.str.45, %edi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	u_1
	addl	%eax, %r14d
	jmp	.LBB0_78
.LBB0_74:                               #   in Loop: Header=BB0_75 Depth=1
	movl	$.L.str.48, %edi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	u_1
	addl	%eax, %r14d
	jmp	.LBB0_84
	.p2align	4, 0x90
.LBB0_75:                               # %.lr.ph88.i
                                        # =>This Inner Loop Header: Depth=1
	movq	wp_weight(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movzbl	luma_log_weight_denom(%rip), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%edx, (%rax)
	jne	.LBB0_77
# BB#76:                                #   in Loop: Header=BB0_75 Depth=1
	movq	wp_offset(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	cmpl	$0, (%rax)
	je	.LBB0_73
.LBB0_77:                               #   in Loop: Header=BB0_75 Depth=1
	movl	$.L.str.45, %edi
	movl	$1, %esi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebp
	addl	%r14d, %ebp
	movq	wp_weight(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	(%rax), %esi
	movl	$.L.str.46, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movq	wp_offset(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	(%rax), %esi
	movl	$.L.str.47, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %r14d
	addl	%ebx, %r14d
.LBB0_78:                               #   in Loop: Header=BB0_75 Depth=1
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB0_84
# BB#79:                                #   in Loop: Header=BB0_75 Depth=1
	movq	wp_weight(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	4(%rax), %edx
	movzbl	chroma_log_weight_denom(%rip), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	cmpl	%esi, %edx
	jne	.LBB0_83
# BB#80:                                #   in Loop: Header=BB0_75 Depth=1
	movq	wp_offset(%rip), %rcx
	movq	(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	cmpl	$0, 4(%rcx)
	jne	.LBB0_83
# BB#81:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	%edx, 8(%rax)
	jne	.LBB0_83
# BB#82:                                #   in Loop: Header=BB0_75 Depth=1
	cmpl	$0, 8(%rcx)
	je	.LBB0_74
	.p2align	4, 0x90
.LBB0_83:                               # %.loopexit78.loopexit97.i
                                        #   in Loop: Header=BB0_75 Depth=1
	movl	$.L.str.48, %edi
	movl	$1, %esi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movq	wp_weight(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	4(%rax), %esi
	movl	$.L.str.49, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	wp_offset(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	4(%rax), %esi
	movl	$.L.str.50, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movq	wp_weight(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	8(%rax), %esi
	movl	$.L.str.49, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	wp_offset(%rip), %rax
	movq	(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	8(%rax), %esi
	movl	$.L.str.50, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %r14d
	addl	%ebp, %r14d
.LBB0_84:                               # %.loopexit78.i
                                        #   in Loop: Header=BB0_75 Depth=1
	incq	%r13
	movq	img(%rip), %rax
	movslq	14456(%rax), %rcx
	cmpq	%rcx, %r13
	jl	.LBB0_75
.LBB0_85:                               # %._crit_edge.i
	cmpl	$1, 20(%rax)
	jne	.LBB0_100
# BB#86:                                # %.preheader.i99
	cmpl	$0, 14460(%rax)
	jle	.LBB0_100
# BB#87:                                # %.lr.ph.i.preheader
	xorl	%r13d, %r13d
	jmp	.LBB0_90
.LBB0_88:                               #   in Loop: Header=BB0_90 Depth=1
	movl	$.L.str.51, %edi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	u_1
	addl	%eax, %r14d
	jmp	.LBB0_93
.LBB0_89:                               #   in Loop: Header=BB0_90 Depth=1
	movl	$.L.str.54, %edi
	xorl	%esi, %esi
	movq	%r15, %rdx
	callq	u_1
	addl	%eax, %r14d
	jmp	.LBB0_99
	.p2align	4, 0x90
.LBB0_90:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	wp_weight(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movzbl	luma_log_weight_denom(%rip), %ecx
	movl	$1, %edx
	shll	%cl, %edx
	cmpl	%edx, (%rax)
	jne	.LBB0_92
# BB#91:                                #   in Loop: Header=BB0_90 Depth=1
	movq	wp_offset(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	cmpl	$0, (%rax)
	je	.LBB0_88
.LBB0_92:                               #   in Loop: Header=BB0_90 Depth=1
	movl	$.L.str.51, %edi
	movl	$1, %esi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movq	wp_weight(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	(%rax), %esi
	movl	$.L.str.52, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	wp_offset(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	(%rax), %esi
	movl	$.L.str.53, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %r14d
	addl	%ebp, %r14d
.LBB0_93:                               #   in Loop: Header=BB0_90 Depth=1
	movq	active_sps(%rip), %rax
	cmpl	$0, 32(%rax)
	je	.LBB0_99
# BB#94:                                #   in Loop: Header=BB0_90 Depth=1
	movq	wp_weight(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	4(%rax), %edx
	movzbl	chroma_log_weight_denom(%rip), %ecx
	movl	$1, %esi
	shll	%cl, %esi
	cmpl	%esi, %edx
	jne	.LBB0_98
# BB#95:                                #   in Loop: Header=BB0_90 Depth=1
	movq	wp_offset(%rip), %rcx
	movq	8(%rcx), %rcx
	movq	(%rcx,%r13,8), %rcx
	cmpl	$0, 4(%rcx)
	jne	.LBB0_98
# BB#96:                                #   in Loop: Header=BB0_90 Depth=1
	cmpl	%edx, 8(%rax)
	jne	.LBB0_98
# BB#97:                                #   in Loop: Header=BB0_90 Depth=1
	cmpl	$0, 8(%rcx)
	je	.LBB0_89
	.p2align	4, 0x90
.LBB0_98:                               # %.loopexit.loopexit94.i
                                        #   in Loop: Header=BB0_90 Depth=1
	movl	$.L.str.54, %edi
	movl	$1, %esi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movq	wp_weight(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	4(%rax), %esi
	movl	$.L.str.55, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	wp_offset(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	4(%rax), %esi
	movl	$.L.str.56, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movq	wp_weight(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	8(%rax), %esi
	movl	$.L.str.55, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	wp_offset(%rip), %rax
	movq	8(%rax), %rax
	movq	(%rax,%r13,8), %rax
	movl	8(%rax), %esi
	movl	$.L.str.56, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %r14d
	addl	%ebp, %r14d
.LBB0_99:                               # %.loopexit.i
                                        #   in Loop: Header=BB0_90 Depth=1
	incq	%r13
	movq	img(%rip), %rax
	movslq	14460(%rax), %rcx
	cmpq	%rcx, %r13
	jl	.LBB0_90
.LBB0_100:                              # %pred_weight_table.exit
	addl	%r14d, %r12d
.LBB0_101:
	cmpl	$0, 15360(%rax)
	je	.LBB0_122
# BB#102:
	movq	14208(%rax), %rcx
	cmpl	$0, 4(%rcx)
	je	.LBB0_104
# BB#103:
	movl	15368(%rax), %esi
	movl	$.L.str.34, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebp
	movq	img(%rip), %rax
	movl	15372(%rax), %esi
	movl	$.L.str.35, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebx
	addl	%ebp, %ebx
	jmp	.LBB0_121
.LBB0_104:
	xorl	%esi, %esi
	cmpq	$0, 15376(%rax)
	setne	%sil
	movl	%esi, 15364(%rax)
	movl	$.L.str.36, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %r14d
	movq	img(%rip), %r13
	cmpl	$0, 15364(%r13)
	je	.LBB0_120
# BB#105:
	addq	$15376, %r13            # imm = 0x3C10
	jmp	.LBB0_107
	.p2align	4, 0x90
.LBB0_106:                              # %.backedge.backedge.i
                                        #   in Loop: Header=BB0_107 Depth=1
	addq	$24, %r13
	movl	%ebx, %r14d
.LBB0_107:                              # %.backedge.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB0_109
# BB#108:                               #   in Loop: Header=BB0_107 Depth=1
	movl	$.L.str.37, %edi
	movl	$500, %esi              # imm = 0x1F4
	callq	error
.LBB0_109:                              #   in Loop: Header=BB0_107 Depth=1
	movl	(%r13), %ebp
	movl	$.L.str.38, %edi
	movl	%ebp, %esi
	movq	%r15, %rdx
	callq	ue_v
	movl	%eax, %ebx
	addl	%r14d, %ebx
	movl	%ebp, %eax
	orl	$2, %eax
	cmpl	$3, %eax
	jne	.LBB0_111
# BB#110:                               #   in Loop: Header=BB0_107 Depth=1
	movl	4(%r13), %esi
	movl	$.L.str.39, %edi
	movq	%r15, %rdx
	callq	ue_v
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rbx,%rax), %ebx
.LBB0_111:                              #   in Loop: Header=BB0_107 Depth=1
	cmpl	$6, %ebp
	je	.LBB0_115
# BB#112:                               #   in Loop: Header=BB0_107 Depth=1
	cmpl	$3, %ebp
	je	.LBB0_115
# BB#113:                               #   in Loop: Header=BB0_107 Depth=1
	cmpl	$2, %ebp
	jne	.LBB0_116
# BB#114:                               # %.thread40.i
                                        #   in Loop: Header=BB0_107 Depth=1
	leaq	8(%r13), %rax
	movl	$.L.str.40, %edi
	jmp	.LBB0_119
	.p2align	4, 0x90
.LBB0_115:                              #   in Loop: Header=BB0_107 Depth=1
	movl	12(%r13), %esi
	movl	$.L.str.41, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%ebx, %eax
	movl	%eax, %ebx
.LBB0_116:                              #   in Loop: Header=BB0_107 Depth=1
	cmpl	$4, %ebp
	je	.LBB0_118
# BB#117:                               #   in Loop: Header=BB0_107 Depth=1
	testl	%ebp, %ebp
	jne	.LBB0_106
	jmp	.LBB0_121
	.p2align	4, 0x90
.LBB0_118:                              #   in Loop: Header=BB0_107 Depth=1
	leaq	16(%r13), %rax
	movl	$.L.str.42, %edi
.LBB0_119:                              # %.thread41.i
                                        #   in Loop: Header=BB0_107 Depth=1
	movl	(%rax), %esi
	movq	%r15, %rdx
	callq	ue_v
	addl	%ebx, %eax
	movl	%eax, %ebx
	jmp	.LBB0_106
.LBB0_120:
	movl	%r14d, %ebx
.LBB0_121:                              # %dec_ref_pic_marking.exit
	addl	%r12d, %ebx
	movl	%ebx, %r12d
.LBB0_122:
	movq	input(%rip), %rax
	cmpl	$1, 4008(%rax)
	jne	.LBB0_125
# BB#123:
	movq	img(%rip), %rax
	cmpl	$2, 20(%rax)
	je	.LBB0_125
# BB#124:
	movl	15384(%rax), %esi
	movl	$.L.str.16, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %r12d
.LBB0_125:
	movq	(%rsp), %rax            # 8-byte Reload
	movl	4(%rax), %esi
	addl	$-26, %esi
	movq	active_pps(%rip), %rax
	subl	200(%rax), %esi
	movl	$.L.str.17, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebx
	addl	%r12d, %ebx
	movq	img(%rip), %rax
	cmpl	$3, 20(%rax)
	jne	.LBB0_127
# BB#126:
	movl	sp2_frame_indicator(%rip), %eax
	xorl	%esi, %esi
	orl	si_frame_indicator(%rip), %eax
	setne	%sil
	movl	$.L.str.18, %edi
	movq	%r15, %rdx
	callq	u_1
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	img(%rip), %rax
	movl	40(%rax), %esi
	addl	$-26, %esi
	movl	$.L.str.19, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebx
	addl	%ebp, %ebx
.LBB0_127:
	movq	active_pps(%rip), %rax
	cmpl	$0, 220(%rax)
	je	.LBB0_130
# BB#128:
	movq	img(%rip), %rax
	movl	14440(%rax), %esi
	movl	$.L.str.20, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %ebx
	movq	img(%rip), %rax
	cmpl	$1, 14440(%rax)
	je	.LBB0_130
# BB#129:
	movl	14444(%rax), %eax
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	movl	$.L.str.21, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movq	img(%rip), %rax
	movl	14448(%rax), %eax
	movl	%eax, %esi
	shrl	$31, %esi
	addl	%eax, %esi
	sarl	%esi
	movl	$.L.str.22, %edi
	movq	%r15, %rdx
	callq	se_v
	movl	%eax, %ebx
	addl	%ebp, %ebx
.LBB0_130:
	movq	active_pps(%rip), %rax
	cmpl	$0, 60(%rax)
	je	.LBB0_133
# BB#131:
	movl	64(%rax), %ecx
	addl	$-3, %ecx
	cmpl	$2, %ecx
	ja	.LBB0_133
# BB#132:
	movq	img(%rip), %rcx
	movl	15336(%rcx), %edx
	imull	15340(%rcx), %edx
	cvtsi2ssq	%rdx, %xmm0
	movl	168(%rax), %eax
	incl	%eax
	cvtsi2ssq	%rax, %xmm1
	divss	%xmm1, %xmm0
	addss	.LCPI0_0(%rip), %xmm0
	cvtss2sd	%xmm0, %xmm0
	callq	log
	divsd	.LCPI0_1(%rip), %xmm0
	callq	ceil
	cvttsd2si	%xmm0, %edi
	movq	img(%rip), %rax
	movl	15436(%rax), %edx
	movl	$.L.str.23, %esi
	movq	%r15, %rcx
	callq	u_v
	addl	%eax, %ebx
.LBB0_133:
	movq	input(%rip), %rax
	cmpl	$0, 4016(%rax)
	je	.LBB0_136
# BB#134:
	movq	img(%rip), %rax
	movq	14208(%rax), %rcx
	cmpl	$0, 4(%rcx)
	jne	.LBB0_136
# BB#135:
	movl	16(%rax), %esi
	movl	$.L.str.24, %edi
	movq	%r15, %rdx
	callq	ue_v
	addl	%eax, %ebx
.LBB0_136:
	movl	%ebx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_137:                              # %._crit_edge
	movl	log2_max_pic_order_cnt_lsb_minus4(%rip), %edi
	movl	15296(%rax), %edx
	jmp	.LBB0_18
.Lfunc_end0:
	.size	SliceHeader, .Lfunc_end0-SliceHeader
	.cfi_endproc

	.globl	get_picture_type
	.p2align	4, 0x90
	.type	get_picture_type,@function
get_picture_type:                       # @get_picture_type
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movl	20(%rax), %eax
	cmpl	$4, %eax
	jae	.LBB1_1
# BB#2:                                 # %switch.lookup
	addl	$5, %eax
	retq
.LBB1_1:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movl	$.L.str.25, %edi
	movl	$1, %esi
	callq	error
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end1:
	.size	get_picture_type, .Lfunc_end1-get_picture_type
	.cfi_endproc

	.globl	Partition_BC_Header
	.p2align	4, 0x90
	.type	Partition_BC_Header,@function
Partition_BC_Header:                    # @Partition_BC_Header
	.cfi_startproc
# BB#0:
	subq	$40, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 48
	movq	img(%rip), %rax
	movq	14216(%rax), %rcx
	movslq	%edi, %rdx
	imulq	$104, %rdx, %rsi
	addq	24(%rcx), %rsi
	movl	$0, (%rsp)
	movl	$0, 8(%rsp)
	movl	16(%rax), %eax
	movl	%eax, 4(%rsp)
	movq	%rsp, %rdi
	callq	writeSE_UVLC
	movl	12(%rsp), %eax
	addq	$40, %rsp
	retq
.Lfunc_end2:
	.size	Partition_BC_Header, .Lfunc_end2-Partition_BC_Header
	.cfi_endproc

	.type	assignSE2partition_NoDP,@object # @assignSE2partition_NoDP
	.bss
	.globl	assignSE2partition_NoDP
	.p2align	4
assignSE2partition_NoDP:
	.zero	72
	.size	assignSE2partition_NoDP, 72

	.type	assignSE2partition_DP,@object # @assignSE2partition_DP
	.data
	.globl	assignSE2partition_DP
	.p2align	4
assignSE2partition_DP:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	assignSE2partition_DP, 72

	.type	assignSE2partition,@object # @assignSE2partition
	.comm	assignSE2partition,16,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"SH: first_mb_in_slice"
	.size	.L.str, 22

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SH: slice_type"
	.size	.L.str.1, 15

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"SH: pic_parameter_set_id"
	.size	.L.str.2, 25

	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"SH: frame_num"
	.size	.L.str.3, 14

	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"SH: field_pic_flag"
	.size	.L.str.4, 19

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"SH: bottom_field_flag"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"SH: idr_pic_id"
	.size	.L.str.6, 15

	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"SH: pic_order_cnt_lsb"
	.size	.L.str.7, 22

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"SH: delta_pic_order_cnt_bottom"
	.size	.L.str.8, 31

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"SH: delta_pic_order_cnt[0]"
	.size	.L.str.9, 27

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"SH: delta_pic_order_cnt[1]"
	.size	.L.str.10, 27

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"SH: redundant_pic_cnt"
	.size	.L.str.11, 22

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"SH: direct_spatial_mv_pred_flag"
	.size	.L.str.12, 32

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"SH: num_ref_idx_active_override_flag"
	.size	.L.str.13, 37

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"SH: num_ref_idx_l0_active_minus1"
	.size	.L.str.14, 33

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"SH: num_ref_idx_l1_active_minus1"
	.size	.L.str.15, 33

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SH: cabac_init_idc"
	.size	.L.str.16, 19

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"SH: slice_qp_delta"
	.size	.L.str.17, 19

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"SH: sp_for_switch_flag"
	.size	.L.str.18, 23

	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"SH: slice_qs_delta"
	.size	.L.str.19, 19

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"SH: disable_deblocking_filter_idc"
	.size	.L.str.20, 34

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SH: slice_alpha_c0_offset_div2"
	.size	.L.str.21, 31

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SH: slice_beta_offset_div2"
	.size	.L.str.22, 27

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SH: slice_group_change_cycle"
	.size	.L.str.23, 29

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"DPA: slice_id"
	.size	.L.str.24, 14

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Picture Type not supported!"
	.size	.L.str.25, 28

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	WriteNALU,@object       # @WriteNALU
	.comm	WriteNALU,8,8
	.type	seiHasBufferingPeriod_info,@object # @seiHasBufferingPeriod_info
	.comm	seiHasBufferingPeriod_info,4,4
	.type	seiBufferingPeriod,@object # @seiBufferingPeriod
	.comm	seiBufferingPeriod,280,8
	.type	seiHasPicTiming_info,@object # @seiHasPicTiming_info
	.comm	seiHasPicTiming_info,4,4
	.type	seiPicTiming,@object    # @seiPicTiming
	.comm	seiPicTiming,152,8
	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"SH: ref_pic_list_reordering_flag_l0"
	.size	.L.str.26, 36

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"SH: reordering_of_pic_nums_idc"
	.size	.L.str.27, 31

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"SH: abs_diff_pic_num_minus1_l0"
	.size	.L.str.28, 31

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"SH: long_term_pic_idx_l0"
	.size	.L.str.29, 25

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"SH: ref_pic_list_reordering_flag_l1"
	.size	.L.str.30, 36

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"SH: remapping_of_pic_num_idc"
	.size	.L.str.31, 29

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"SH: abs_diff_pic_num_minus1_l1"
	.size	.L.str.32, 31

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"SH: long_term_pic_idx_l1"
	.size	.L.str.33, 25

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"SH: no_output_of_prior_pics_flag"
	.size	.L.str.34, 33

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"SH: long_term_reference_flag"
	.size	.L.str.35, 29

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"SH: adaptive_ref_pic_buffering_flag"
	.size	.L.str.36, 36

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Error encoding MMCO commands"
	.size	.L.str.37, 29

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"SH: memory_management_control_operation"
	.size	.L.str.38, 40

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"SH: difference_of_pic_nums_minus1"
	.size	.L.str.39, 34

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"SH: long_term_pic_num"
	.size	.L.str.40, 22

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"SH: long_term_frame_idx"
	.size	.L.str.41, 24

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"SH: max_long_term_pic_idx_plus1"
	.size	.L.str.42, 32

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"SH: luma_log_weight_denom"
	.size	.L.str.43, 26

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"SH: chroma_log_weight_denom"
	.size	.L.str.44, 28

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"SH: luma_weight_flag_l0"
	.size	.L.str.45, 24

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"SH: luma_weight_l0"
	.size	.L.str.46, 19

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"SH: luma_offset_l0"
	.size	.L.str.47, 19

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"chroma_weight_flag_l0"
	.size	.L.str.48, 22

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"chroma_weight_l0"
	.size	.L.str.49, 17

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"chroma_offset_l0"
	.size	.L.str.50, 17

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"SH: luma_weight_flag_l1"
	.size	.L.str.51, 24

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"SH: luma_weight_l1"
	.size	.L.str.52, 19

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"SH: luma_offset_l1"
	.size	.L.str.53, 19

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"chroma_weight_flag_l1"
	.size	.L.str.54, 22

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"chroma_weight_l1"
	.size	.L.str.55, 17

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"chroma_offset_l1"
	.size	.L.str.56, 17


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
