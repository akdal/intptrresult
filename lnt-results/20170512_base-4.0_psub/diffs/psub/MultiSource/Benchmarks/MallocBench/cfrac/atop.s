	.text
	.file	"atop.bc"
	.globl	atop
	.p2align	4, 0x90
	.type	atop,@function
atop:                                   # @atop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	$0, 8(%rsp)
	movq	$0, 16(%rsp)
	testq	%rbx, %rbx
	je	.LBB0_20
# BB#1:                                 # %.preheader33
	callq	__ctype_b_loc
	movq	%rax, %r14
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	movsbq	(%rbx), %rcx
	incq	%rbx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB0_2
# BB#3:
	cmpb	$43, %cl
	je	.LBB0_6
# BB#4:
	cmpb	$45, %cl
	jne	.LBB0_5
# BB#41:
	movl	$1, %r13d
	jmp	.LBB0_7
.LBB0_5:                                # %._crit_edge81
	decq	%rbx
.LBB0_6:
	xorl	%r13d, %r13d
.LBB0_7:
	movzbl	(%rbx), %ebp
	testb	$8, 1(%rax,%rbp,2)
	je	.LBB0_17
# BB#8:
	movq	pzero(%rip), %rsi
	leaq	8(%rsp), %r12
	movq	%r12, %rdi
	callq	psetq
	movl	$1000000000, %edi       # imm = 0x3B9ACA00
	callq	utop
	leaq	16(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	(%r14), %rax
	addq	$9, %rbx
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movzbl	%bpl, %r15d
	addl	$-48, %r15d
	movzbl	-8(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_10
# BB#11:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %edx
	leal	-48(%rcx,%rdx,2), %r15d
	movzbl	-7(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_12
# BB#21:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %edx
	leal	-48(%rcx,%rdx,2), %r15d
	movzbl	-6(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_22
# BB#23:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %edx
	leal	-48(%rcx,%rdx,2), %r15d
	movzbl	-5(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_24
# BB#25:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %edx
	leal	-48(%rcx,%rdx,2), %r15d
	movzbl	-4(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_26
# BB#27:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %edx
	leal	-48(%rcx,%rdx,2), %r15d
	movzbl	-3(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_28
# BB#29:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %edx
	leal	-48(%rcx,%rdx,2), %r15d
	movzbl	-2(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_30
# BB#31:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %edx
	leal	-48(%rcx,%rdx,2), %r15d
	movzbl	-1(%rbx), %ecx
	testb	$8, 1(%rax,%rcx,2)
	je	.LBB0_32
# BB#33:                                #   in Loop: Header=BB0_9 Depth=1
	leal	(%r15,%r15,4), %eax
	leal	-48(%rcx,%rax,2), %r15d
	movq	8(%rsp), %rdi
	movq	16(%rsp), %rsi
	callq	pmul
	movq	%rax, %rbp
	movl	%r15d, %edi
	callq	utop
	movq	%rbp, %rdi
	movq	%rax, %rsi
	callq	padd
	movq	%r12, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	(%r14), %rax
	movzbl	(%rbx), %ebp
	addq	$9, %rbx
	testb	$8, 1(%rax,%rbp,2)
	jne	.LBB0_9
	jmp	.LBB0_15
.LBB0_10:
	movl	$10, %edi
	jmp	.LBB0_14
.LBB0_12:
	movl	$8, %eax
	jmp	.LBB0_13
.LBB0_22:
	movl	$7, %eax
	jmp	.LBB0_13
.LBB0_24:
	movl	$6, %eax
	jmp	.LBB0_13
.LBB0_26:
	movl	$5, %eax
	jmp	.LBB0_13
.LBB0_28:
	movl	$4, %eax
	jmp	.LBB0_13
.LBB0_30:
	movl	$3, %eax
	jmp	.LBB0_13
.LBB0_32:
	movl	$2, %eax
.LBB0_13:                               # %.lr.ph
	movl	$100, %edi
	cmpl	$8, %eax
	je	.LBB0_14
# BB#34:                                # %.lr.ph.1
	movl	$1000, %edi             # imm = 0x3E8
	cmpl	$7, %eax
	je	.LBB0_14
# BB#35:                                # %.lr.ph.2
	movl	$10000, %edi            # imm = 0x2710
	cmpl	$6, %eax
	je	.LBB0_14
# BB#36:                                # %.lr.ph.3
	movl	$100000, %edi           # imm = 0x186A0
	cmpl	$5, %eax
	je	.LBB0_14
# BB#37:                                # %.lr.ph.4
	movl	$1000000, %edi          # imm = 0xF4240
	cmpl	$4, %eax
	je	.LBB0_14
# BB#38:                                # %.lr.ph.5
	movl	$10000000, %edi         # imm = 0x989680
	cmpl	$3, %eax
	je	.LBB0_14
# BB#39:                                # %.lr.ph.6
	movl	$100000000, %edi        # imm = 0x5F5E100
	cmpl	$2, %eax
	je	.LBB0_14
# BB#40:                                # %.lr.ph.7
	movl	$1000000000, %edi       # imm = 0x3B9ACA00
.LBB0_14:                               # %._crit_edge
	movq	8(%rsp), %rbx
	callq	utop
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	pmul
	movq	%rax, %rbx
	movl	%r15d, %edi
	callq	utop
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	padd
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB0_15:                               # %.loopexit
	testl	%r13d, %r13d
	je	.LBB0_17
# BB#16:
	movq	8(%rsp), %rdi
	callq	pneg
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB0_17:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_20
# BB#18:
	decw	(%rdi)
	jne	.LBB0_20
# BB#19:
	xorl	%eax, %eax
	callq	pfree
.LBB0_20:                               # %.thread
	movq	8(%rsp), %rdi
	callq	presult
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	atop, .Lfunc_end0-atop
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
