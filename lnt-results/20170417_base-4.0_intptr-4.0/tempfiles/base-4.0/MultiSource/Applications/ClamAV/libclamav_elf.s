	.text
	.file	"libclamav_elf.bc"
	.globl	cli_scanelf
	.p2align	4, 0x90
	.type	cli_scanelf,@function
cli_scanelf:                            # @cli_scanelf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %ebp
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	32(%rsp), %rsi
	movl	$52, %edx
	movl	%ebp, %edi
	callq	read
	cmpq	$52, %rax
	jne	.LBB0_1
# BB#3:
	cmpl	$1179403647, 32(%rsp)   # imm = 0x464C457F
	je	.LBB0_5
# BB#4:
	xorl	%ebx, %ebx
	movl	$.L.str.3, %edi
	jmp	.LBB0_2
.LBB0_1:
	movl	$.L.str.1, %edi
.LBB0_2:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_127:
	movl	%ebx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_5:
	cmpb	$1, 36(%rsp)
	jne	.LBB0_6
# BB#7:
	cmpb	$1, 37(%rsp)
	jne	.LBB0_9
# BB#8:
	movl	$.L.str.5, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movb	$1, %r13b
	jmp	.LBB0_10
.LBB0_6:
	xorl	%ebx, %ebx
	movl	$.L.str.4, %edi
	jmp	.LBB0_2
.LBB0_9:
	xorl	%r13d, %r13d
	movl	$.L.str.6, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_10:
	movzwl	48(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testb	%r13b, %r13b
	cmovnew	%ax, %cx
	movzwl	%cx, %esi
	cmpl	$4, %esi
	ja	.LBB0_17
# BB#11:
	jmpq	*.LJTI0_0(,%rsi,8)
.LBB0_16:
	movl	$.L.str.10, %edi
	jmp	.LBB0_13
.LBB0_17:
	movl	$.L.str.11, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB0_18
.LBB0_12:
	movl	$.L.str.7, %edi
	jmp	.LBB0_13
.LBB0_14:
	movl	$.L.str.8, %edi
	jmp	.LBB0_13
.LBB0_15:
	movl	$.L.str.9, %edi
.LBB0_13:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_18:
	movzwl	50(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testb	%r13b, %r13b
	cmovnew	%ax, %cx
	movzwl	%cx, %esi
	cmpl	$80, %esi
	ja	.LBB0_34
# BB#19:
	jmpq	*.LJTI0_1(,%rsi,8)
.LBB0_20:
	movl	$.L.str.12, %edi
	jmp	.LBB0_21
.LBB0_34:
	movl	$.L.str.25, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	cli_dbgmsg
	jmp	.LBB0_35
.LBB0_22:
	movl	$.L.str.13, %edi
	jmp	.LBB0_21
.LBB0_23:
	movl	$.L.str.14, %edi
	jmp	.LBB0_21
.LBB0_24:
	movl	$.L.str.15, %edi
	jmp	.LBB0_21
.LBB0_25:
	movl	$.L.str.16, %edi
	jmp	.LBB0_21
.LBB0_26:
	movl	$.L.str.17, %edi
	jmp	.LBB0_21
.LBB0_27:
	movl	$.L.str.18, %edi
	jmp	.LBB0_21
.LBB0_28:
	movl	$.L.str.19, %edi
	jmp	.LBB0_21
.LBB0_29:
	movl	$.L.str.20, %edi
	jmp	.LBB0_21
.LBB0_30:
	movl	$.L.str.21, %edi
	jmp	.LBB0_21
.LBB0_31:
	movl	$.L.str.22, %edi
	jmp	.LBB0_21
.LBB0_32:
	movl	$.L.str.23, %edi
	jmp	.LBB0_21
.LBB0_33:
	movl	$.L.str.24, %edi
.LBB0_21:
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_35:
	movl	56(%rsp), %eax
	movl	%eax, %r15d
	bswapl	%r15d
	movzwl	76(%rsp), %ecx
	movl	%ecx, %ebx
	rolw	$8, %bx
	testb	%r13b, %r13b
	cmovnel	%eax, %r15d
	cmovnew	%cx, %bx
	movzwl	%bx, %r12d
	movl	$.L.str.26, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	cmpl	$129, %r12d
	jb	.LBB0_39
# BB#36:
	movl	$.L.str.27, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-124, %ebx
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#37:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#38:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_39:
	testl	%r15d, %r15d
	je	.LBB0_75
# BB#40:
	testw	%bx, %bx
	je	.LBB0_75
# BB#41:
	movzwl	74(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testb	%r13b, %r13b
	cmovnew	%ax, %cx
	movzwl	%cx, %eax
	cmpl	$32, %eax
	jne	.LBB0_42
# BB#45:
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	60(%rsp), %eax
	movl	%r13d, %ecx
	movl	%eax, %r13d
	bswapl	%r13d
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	testb	%cl, %cl
	cmovnel	%eax, %r13d
	xorl	%ebx, %ebx
	movl	$.L.str.30, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	lseek
	cmpl	%r13d, %eax
	jne	.LBB0_46
# BB#49:
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	cli_calloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB0_50
# BB#52:                                # %.lr.ph184
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	je	.LBB0_53
# BB#58:                                # %.lr.ph184.split.us.preheader
	xorl	%r13d, %r13d
.LBB0_59:                               # %.lr.ph184.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	read
	cmpq	$32, %rax
	jne	.LBB0_55
# BB#60:                                #   in Loop: Header=BB0_59 Depth=1
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movl	(%rbx), %esi
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rbx), %esi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rbx), %esi
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%rbx), %esi
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	20(%rbx), %esi
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incq	%r13
	addq	$32, %rbx
	cmpq	%r12, %r13
	jb	.LBB0_59
	jmp	.LBB0_62
.LBB0_42:
	movl	$.L.str.29, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-124, %ebx
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#43:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#44:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_46:
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#47:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#48:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_50:
	movl	$.L.str.31, %edi
	jmp	.LBB0_51
.LBB0_53:                               # %.lr.ph184.split.preheader
	xorl	%r13d, %r13d
.LBB0_54:                               # %.lr.ph184.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edx
	movl	%ebp, %edi
	movq	%rbx, %rsi
	callq	read
	cmpq	$32, %rax
	jne	.LBB0_55
# BB#61:                                #   in Loop: Header=BB0_54 Depth=1
	movl	$.L.str.35, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movl	(%rbx), %esi
	bswapl	%esi
	movl	$.L.str.36, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	4(%rbx), %esi
	bswapl	%esi
	movl	$.L.str.37, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%rbx), %esi
	bswapl	%esi
	movl	$.L.str.38, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	16(%rbx), %esi
	bswapl	%esi
	movl	$.L.str.39, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	20(%rbx), %esi
	bswapl	%esi
	movl	$.L.str.40, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incq	%r13
	addq	$32, %rbx
	cmpq	%r12, %r13
	jb	.LBB0_54
.LBB0_62:                               # %.lr.ph.i
	movl	12(%rsp), %r13d         # 4-byte Reload
	testb	%r13b, %r13b
	je	.LBB0_63
# BB#67:                                # %.lr.ph.split.us.i.preheader
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rdx
	addq	$20, %rdx
	xorl	%eax, %eax
.LBB0_68:                               # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %ecx
	cmpl	%r15d, %ecx
	ja	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_68 Depth=1
	movl	(%rdx), %esi
	addl	%ecx, %esi
	cmpl	%r15d, %esi
	ja	.LBB0_74
.LBB0_70:                               #   in Loop: Header=BB0_68 Depth=1
	incq	%rax
	addq	$32, %rdx
	cmpq	%r12, %rax
	jb	.LBB0_68
	jmp	.LBB0_71
.LBB0_55:                               # %.us-lcssa.us
	xorl	%ebx, %ebx
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#56:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#57:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_63:                               # %.lr.ph.split.i.preheader
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rdx
	addq	$20, %rdx
	xorl	%eax, %eax
.LBB0_64:                               # %.lr.ph.split.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %ecx
	movl	%ecx, %esi
	bswapl	%esi
	cmpl	%r15d, %esi
	ja	.LBB0_66
# BB#65:                                #   in Loop: Header=BB0_64 Depth=1
	movl	(%rdx), %ebx
	bswapl	%ebx
	addl	%esi, %ebx
	cmpl	%r15d, %ebx
	ja	.LBB0_74
.LBB0_66:                               #   in Loop: Header=BB0_64 Depth=1
	incq	%rax
	addq	$32, %rdx
	cmpq	%r12, %rax
	jb	.LBB0_64
.LBB0_71:                               # %.loopexit172
	callq	free
	movl	$.L.str.41, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-124, %ebx
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#72:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#73:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_74:                               # %.loopexit
	movl	%ecx, %edx
	bswapl	%edx
	shlq	$5, %rax
	testb	%r13b, %r13b
	cmovnel	%ecx, %edx
	movl	4(%rdi,%rax), %eax
	movl	%eax, %ecx
	bswapl	%ecx
	cmovnel	%eax, %ecx
	movl	%r15d, %ebx
	subl	%edx, %ebx
	addl	%ecx, %ebx
	callq	free
	movl	$.L.str.42, %edi
	xorl	%eax, %eax
	movl	%r15d, %esi
	callq	cli_dbgmsg
	movl	$.L.str.43, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	movl	%ebx, %edx
	callq	cli_dbgmsg
.LBB0_75:
	movzwl	80(%rsp), %eax
	movl	%eax, %r15d
	rolw	$8, %r15w
	testb	%r13b, %r13b
	cmovnew	%ax, %r15w
	movzwl	%r15w, %ebx
	movl	$.L.str.44, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	cli_dbgmsg
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	cmpl	$257, %ebx              # imm = 0x101
	jb	.LBB0_79
# BB#76:
	movl	$.L.str.45, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-124, %ebx
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#77:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#78:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_79:
	movzwl	78(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	testb	%r13b, %r13b
	cmovnew	%ax, %cx
	movzwl	%cx, %r12d
	cmpl	$40, %r12d
	jne	.LBB0_80
# BB#83:
	movl	64(%rsp), %eax
	movl	%r13d, %ecx
	movl	%eax, %r13d
	bswapl	%r13d
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	testb	%cl, %cl
	cmovnel	%eax, %r13d
	xorl	%ebx, %ebx
	movl	$.L.str.47, %edi
	xorl	%eax, %eax
	movl	%r13d, %esi
	callq	cli_dbgmsg
	xorl	%edx, %edx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	lseek
	cmpl	%r13d, %eax
	jne	.LBB0_84
# BB#87:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%r12, %rsi
	callq	cli_calloc
	movq	%rax, 24(%rsp)          # 8-byte Spill
	testq	%rax, %rax
	je	.LBB0_88
# BB#89:
	xorl	%ebx, %ebx
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testw	%r15w, %r15w
	je	.LBB0_126
# BB#90:                                # %.lr.ph.preheader
	movq	24(%rsp), %r13          # 8-byte Reload
	xorl	%r12d, %r12d
	movl	12(%rsp), %r15d         # 4-byte Reload
.LBB0_91:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$40, %edx
	movl	%ebp, %edi
	movq	%r13, %rsi
	callq	read
	cmpq	$40, %rax
	jne	.LBB0_92
# BB#95:                                #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.50, %edi
	xorl	%eax, %eax
	movl	%r12d, %esi
	callq	cli_dbgmsg
	movl	16(%r13), %eax
	movl	%eax, %esi
	bswapl	%esi
	testb	%r15b, %r15b
	cmovnel	%eax, %esi
	movl	$.L.str.51, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	%r15b, %r15b
	movl	20(%r13), %eax
	movl	%eax, %esi
	bswapl	%esi
	cmovnel	%eax, %esi
	movl	$.L.str.52, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	testb	%r15b, %r15b
	movl	4(%r13), %ecx
	movl	%ecx, %eax
	bswapl	%eax
	cmovnel	%ecx, %eax
	cmpl	$1879048188, %eax       # imm = 0x6FFFFFFC
	jg	.LBB0_98
# BB#96:                                #   in Loop: Header=BB0_91 Depth=1
	cmpl	$16, %eax
	ja	.LBB0_118
# BB#97:                                #   in Loop: Header=BB0_91 Depth=1
	movl	%eax, %eax
	jmpq	*.LJTI0_2(,%rax,8)
.LBB0_109:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.60, %edi
	jmp	.LBB0_119
.LBB0_98:                               #   in Loop: Header=BB0_91 Depth=1
	cmpl	$1879048189, %eax       # imm = 0x6FFFFFFD
	je	.LBB0_116
# BB#99:                                #   in Loop: Header=BB0_91 Depth=1
	cmpl	$1879048190, %eax       # imm = 0x6FFFFFFE
	je	.LBB0_117
# BB#100:                               #   in Loop: Header=BB0_91 Depth=1
	cmpl	$1879048191, %eax       # imm = 0x6FFFFFFF
	jne	.LBB0_118
# BB#101:                               #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.69, %edi
	jmp	.LBB0_119
.LBB0_111:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.62, %edi
	jmp	.LBB0_119
.LBB0_115:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.66, %edi
	jmp	.LBB0_119
.LBB0_114:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.65, %edi
	jmp	.LBB0_119
.LBB0_113:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.64, %edi
	jmp	.LBB0_119
.LBB0_105:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.56, %edi
	jmp	.LBB0_119
.LBB0_102:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.53, %edi
	jmp	.LBB0_119
.LBB0_108:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.59, %edi
	jmp	.LBB0_119
.LBB0_107:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.58, %edi
	jmp	.LBB0_119
.LBB0_112:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.63, %edi
	jmp	.LBB0_119
.LBB0_103:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.54, %edi
	jmp	.LBB0_119
.LBB0_106:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.57, %edi
	jmp	.LBB0_119
.LBB0_104:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.55, %edi
	jmp	.LBB0_119
.LBB0_110:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.61, %edi
	jmp	.LBB0_119
.LBB0_116:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.67, %edi
	jmp	.LBB0_119
.LBB0_117:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.68, %edi
	jmp	.LBB0_119
.LBB0_118:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.70, %edi
.LBB0_119:                              #   in Loop: Header=BB0_91 Depth=1
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%r13), %eax
	movl	%eax, %ecx
	shrl	$24, %ecx
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	cmovnel	%eax, %ecx
	testb	$1, %cl
	je	.LBB0_121
# BB#120:                               #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.71, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%r13), %eax
.LBB0_121:                              #   in Loop: Header=BB0_91 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	cmovnel	%eax, %ecx
	testb	$2, %cl
	je	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.72, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	8(%r13), %eax
.LBB0_123:                              #   in Loop: Header=BB0_91 Depth=1
	movl	%eax, %ecx
	shrl	$24, %ecx
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	cmovnel	%eax, %ecx
	testb	$4, %cl
	je	.LBB0_125
# BB#124:                               #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.73, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
.LBB0_125:                              #   in Loop: Header=BB0_91 Depth=1
	movl	$.L.str.32, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	incq	%r12
	addq	$40, %r13
	cmpq	16(%rsp), %r12          # 8-byte Folded Reload
	jb	.LBB0_91
.LBB0_126:                              # %._crit_edge
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB0_127
.LBB0_80:
	movl	$.L.str.46, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-124, %ebx
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#81:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#82:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_84:
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#85:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#86:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.LBB0_88:
	movl	$.L.str.48, %edi
.LBB0_51:
	xorl	%eax, %eax
	callq	cli_errmsg
	movl	$-114, %ebx
	jmp	.LBB0_127
.LBB0_92:
	xorl	%ebx, %ebx
	movl	$.L.str.49, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$.L.str.34, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	free
	testb	$64, 40(%r14)
	je	.LBB0_127
# BB#93:
	movq	(%r14), %rax
	movl	$1, %ebx
	testq	%rax, %rax
	je	.LBB0_127
# BB#94:
	movq	$.L.str.28, (%rax)
	jmp	.LBB0_127
.Lfunc_end0:
	.size	cli_scanelf, .Lfunc_end0-cli_scanelf
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_12
	.quad	.LBB0_14
	.quad	.LBB0_15
	.quad	.LBB0_16
	.quad	.LBB0_16
.LJTI0_1:
	.quad	.LBB0_20
	.quad	.LBB0_34
	.quad	.LBB0_22
	.quad	.LBB0_23
	.quad	.LBB0_24
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_25
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_26
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_27
	.quad	.LBB0_28
	.quad	.LBB0_29
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_30
	.quad	.LBB0_31
	.quad	.LBB0_34
	.quad	.LBB0_32
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_34
	.quad	.LBB0_33
.LJTI0_2:
	.quad	.LBB0_109
	.quad	.LBB0_111
	.quad	.LBB0_115
	.quad	.LBB0_114
	.quad	.LBB0_113
	.quad	.LBB0_105
	.quad	.LBB0_102
	.quad	.LBB0_108
	.quad	.LBB0_107
	.quad	.LBB0_112
	.quad	.LBB0_118
	.quad	.LBB0_103
	.quad	.LBB0_118
	.quad	.LBB0_118
	.quad	.LBB0_106
	.quad	.LBB0_104
	.quad	.LBB0_110

	.text
	.globl	cli_elfheader
	.p2align	4, 0x90
	.type	cli_elfheader,@function
cli_elfheader:                          # @cli_elfheader
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 144
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r12d
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	leaq	32(%rsp), %rsi
	movl	$52, %edx
	movl	%r12d, %edi
	callq	read
	cmpq	$52, %rax
	jne	.LBB1_1
# BB#3:
	cmpl	$1179403647, 32(%rsp)   # imm = 0x464C457F
	je	.LBB1_5
# BB#4:
	movl	$.L.str.3, %edi
	jmp	.LBB1_2
.LBB1_1:
	movl	$.L.str.1, %edi
.LBB1_2:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	movl	$-1, %ebx
.LBB1_52:
	movl	%ebx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_5:
	cmpb	$1, 36(%rsp)
	jne	.LBB1_6
# BB#7:
	movb	37(%rsp), %bpl
	movzwl	76(%rsp), %ecx
	movl	%ecx, %eax
	rolw	$8, %ax
	cmpb	$1, %bpl
	cmovew	%cx, %ax
	movzwl	%ax, %edx
	cmpl	$129, %edx
	jb	.LBB1_9
# BB#8:
	movl	$.L.str.27, %edi
	jmp	.LBB1_2
.LBB1_6:
	movl	$.L.str.4, %edi
	jmp	.LBB1_2
.LBB1_9:
	cmpb	$1, %bpl
	movl	56(%rsp), %ecx
	movl	%ecx, %r15d
	bswapl	%r15d
	cmovel	%ecx, %r15d
	xorl	%ecx, %ecx
	testw	%ax, %ax
	je	.LBB1_31
# BB#10:
	testl	%r15d, %r15d
	je	.LBB1_31
# BB#11:
	movzwl	74(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	cmpb	$1, %bpl
	cmovew	%ax, %cx
	movzwl	%cx, %r13d
	cmpl	$32, %r13d
	jne	.LBB1_12
# BB#13:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movb	%bpl, 15(%rsp)          # 1-byte Spill
	cmpb	$1, %bpl
	movl	60(%rsp), %eax
	movl	%eax, %ebp
	bswapl	%ebp
	cmovel	%eax, %ebp
	xorl	%edx, %edx
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	lseek
	movl	$-1, %ebx
	cmpl	%ebp, %eax
	jne	.LBB1_52
# BB#14:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB1_16
# BB#15:                                # %.lr.ph120.preheader
	xorl	%ebp, %ebp
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rax, %r13
.LBB1_18:                               # %.lr.ph120
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edx
	movl	%r12d, %edi
	movq	%r13, %rsi
	callq	read
	cmpq	$32, %rax
	jne	.LBB1_19
# BB#17:                                #   in Loop: Header=BB1_18 Depth=1
	incq	%rbp
	addq	$32, %r13
	cmpq	24(%rsp), %rbp          # 8-byte Folded Reload
	jb	.LBB1_18
# BB#20:                                # %.lr.ph.i
	movb	15(%rsp), %bpl          # 1-byte Reload
	cmpb	$1, %bpl
	jne	.LBB1_21
# BB#25:                                # %.lr.ph.split.us.i.preheader
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rdx
	addq	$20, %rdx
	xorl	%eax, %eax
	movq	24(%rsp), %r8           # 8-byte Reload
.LBB1_26:                               # %.lr.ph.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %r9d
	cmpl	%r15d, %r9d
	ja	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_26 Depth=1
	movl	(%rdx), %esi
	addl	%r9d, %esi
	cmpl	%r15d, %esi
	ja	.LBB1_30
.LBB1_28:                               #   in Loop: Header=BB1_26 Depth=1
	incq	%rax
	addq	$32, %rdx
	cmpq	%r8, %rax
	jb	.LBB1_26
	jmp	.LBB1_29
.LBB1_12:
	movl	$.L.str.29, %edi
	jmp	.LBB1_2
.LBB1_16:
	movl	$.L.str.31, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_52
.LBB1_19:
	movl	$.L.str.33, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	cli_dbgmsg
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	jmp	.LBB1_52
.LBB1_21:                               # %.lr.ph.split.i.preheader
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %rdx
	addq	$20, %rdx
	xorl	%eax, %eax
	movq	24(%rsp), %r8           # 8-byte Reload
.LBB1_22:                               # %.lr.ph.split.i
                                        # =>This Inner Loop Header: Depth=1
	movl	-12(%rdx), %r9d
	movl	%r9d, %esi
	bswapl	%esi
	cmpl	%r15d, %esi
	ja	.LBB1_24
# BB#23:                                #   in Loop: Header=BB1_22 Depth=1
	movl	(%rdx), %ecx
	bswapl	%ecx
	addl	%esi, %ecx
	cmpl	%r15d, %ecx
	ja	.LBB1_30
.LBB1_24:                               #   in Loop: Header=BB1_22 Depth=1
	incq	%rax
	addq	$32, %rdx
	cmpq	%r8, %rax
	jb	.LBB1_22
.LBB1_29:                               # %.loopexit
	callq	free
	movl	$.L.str.41, %edi
	jmp	.LBB1_38
.LBB1_30:                               # %cli_rawaddr.exit
	shlq	$5, %rax
	cmpb	$1, %bpl
	movl	%r9d, %edx
	bswapl	%edx
	cmovel	%r9d, %edx
	movl	4(%rdi,%rax), %eax
	movl	%eax, %ecx
	bswapl	%ecx
	cmovel	%eax, %ecx
	subl	%edx, %r15d
	addl	%ecx, %r15d
	callq	free
	movl	%r15d, %ecx
.LBB1_31:
	movl	%ecx, (%r14)
	movzwl	80(%rsp), %eax
	movl	%eax, %r15d
	rolw	$8, %r15w
	cmpb	$1, %bpl
	cmovew	%ax, %r15w
	movzwl	%r15w, %edx
	cmpl	$257, %edx              # imm = 0x101
	jb	.LBB1_33
# BB#32:
	movl	$.L.str.45, %edi
	jmp	.LBB1_2
.LBB1_33:
	movw	%r15w, 4(%r14)
	movzwl	78(%rsp), %eax
	movl	%eax, %ecx
	rolw	$8, %cx
	cmpb	$1, %bpl
	cmovew	%ax, %cx
	movzwl	%cx, %r13d
	cmpl	$40, %r13d
	jne	.LBB1_34
# BB#35:
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movb	%bpl, 15(%rsp)          # 1-byte Spill
	cmpb	$1, %bpl
	movl	64(%rsp), %eax
	movl	%eax, %ebp
	bswapl	%ebp
	cmovel	%eax, %ebp
	xorl	%edx, %edx
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	lseek
	movl	$-1, %ebx
	cmpl	%ebp, %eax
	jne	.LBB1_52
# BB#36:
	movzwl	4(%r14), %edi
	movl	$36, %esi
	callq	cli_calloc
	movq	%rax, 16(%r14)
	testq	%rax, %rax
	je	.LBB1_37
# BB#39:
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r13, %rsi
	callq	cli_calloc
	testq	%rax, %rax
	je	.LBB1_48
# BB#40:                                # %.preheader
	movq	%rax, 16(%rsp)          # 8-byte Spill
	testw	%r15w, %r15w
	je	.LBB1_51
# BB#41:                                # %.lr.ph
	cmpb	$1, 15(%rsp)            # 1-byte Folded Reload
	jne	.LBB1_42
# BB#45:                                # %.lr.ph.split.us.preheader
	xorl	%r15d, %r15d
	movl	$12, %r13d
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB1_46:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movl	$40, %edx
	movl	%r12d, %edi
	movq	%rbp, %rsi
	callq	read
	cmpq	$40, %rax
	jne	.LBB1_44
# BB#47:                                #   in Loop: Header=BB1_46 Depth=1
	movl	12(%rbp), %eax
	movq	16(%r14), %rcx
	movl	%eax, -12(%rcx,%r13)
	movl	16(%rbp), %eax
	movl	%eax, -4(%rcx,%r13)
	movl	20(%rbp), %eax
	movl	%eax, (%rcx,%r13)
	incq	%r15
	addq	$40, %rbp
	addq	$36, %r13
	cmpq	24(%rsp), %r15          # 8-byte Folded Reload
	jb	.LBB1_46
	jmp	.LBB1_51
.LBB1_34:
	movl	$.L.str.46, %edi
	jmp	.LBB1_2
.LBB1_37:
	movl	$.L.str.48, %edi
.LBB1_38:
	xorl	%eax, %eax
	callq	cli_dbgmsg
	jmp	.LBB1_52
.LBB1_48:
	movl	$.L.str.48, %edi
	xorl	%eax, %eax
	callq	cli_errmsg
	jmp	.LBB1_49
.LBB1_42:                               # %.lr.ph.split.preheader
	xorl	%r15d, %r15d
	movl	$12, %ebp
	movq	16(%rsp), %r13          # 8-byte Reload
.LBB1_43:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movl	$40, %edx
	movl	%r12d, %edi
	movq	%r13, %rsi
	callq	read
	cmpq	$40, %rax
	jne	.LBB1_44
# BB#50:                                #   in Loop: Header=BB1_43 Depth=1
	movl	12(%r13), %eax
	bswapl	%eax
	movq	16(%r14), %rcx
	movl	%eax, -12(%rcx,%rbp)
	movl	16(%r13), %eax
	bswapl	%eax
	movl	%eax, -4(%rcx,%rbp)
	movl	20(%r13), %eax
	bswapl	%eax
	movl	%eax, (%rcx,%rbp)
	incq	%r15
	addq	$40, %r13
	addq	$36, %rbp
	cmpq	24(%rsp), %r15          # 8-byte Folded Reload
	jb	.LBB1_43
.LBB1_51:                               # %._crit_edge
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
	xorl	%ebx, %ebx
	jmp	.LBB1_52
.LBB1_44:                               # %.us-lcssa.us
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	free
.LBB1_49:
	movq	16(%r14), %rdi
	callq	free
	movq	$0, 16(%r14)
	jmp	.LBB1_52
.Lfunc_end1:
	.size	cli_elfheader, .Lfunc_end1-cli_elfheader
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"in cli_scanelf\n"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"ELF: Can't read file header\n"
	.size	.L.str.1, 29

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\177ELF"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"ELF: Not an ELF file\n"
	.size	.L.str.3, 22

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"ELF: 64-bit binaries are not supported (yet)\n"
	.size	.L.str.4, 46

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"ELF: File is little-endian - conversion not required\n"
	.size	.L.str.5, 54

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"ELF: File is big-endian - data conversion enabled\n"
	.size	.L.str.6, 51

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"ELF: File type: None\n"
	.size	.L.str.7, 22

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"ELF: File type: Relocatable\n"
	.size	.L.str.8, 29

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"ELF: File type: Executable\n"
	.size	.L.str.9, 28

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"ELF: File type: Core\n"
	.size	.L.str.10, 22

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"ELF: File type: Unknown (%d)\n"
	.size	.L.str.11, 30

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"ELF: Machine type: None\n"
	.size	.L.str.12, 25

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"ELF: Machine type: SPARC\n"
	.size	.L.str.13, 26

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"ELF: Machine type: Intel 80386\n"
	.size	.L.str.14, 32

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"ELF: Machine type: Motorola 68000\n"
	.size	.L.str.15, 35

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"ELF: Machine type: MIPS RS3000\n"
	.size	.L.str.16, 32

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"ELF: Machine type: HPPA\n"
	.size	.L.str.17, 25

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"ELF: Machine type: PowerPC\n"
	.size	.L.str.18, 28

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"ELF: Machine type: PowerPC 64-bit\n"
	.size	.L.str.19, 35

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"ELF: Machine type: IBM S390\n"
	.size	.L.str.20, 29

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"ELF: Machine type: ARM\n"
	.size	.L.str.21, 24

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"ELF: Machine type: Digital Alpha\n"
	.size	.L.str.22, 34

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"ELF: Machine type: SPARC v9 64-bit\n"
	.size	.L.str.23, 36

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"ELF: Machine type: IA64\n"
	.size	.L.str.24, 25

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"ELF: Machine type: Unknown (%d)\n"
	.size	.L.str.25, 33

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"ELF: Number of program headers: %d\n"
	.size	.L.str.26, 36

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"ELF: Suspicious number of program headers\n"
	.size	.L.str.27, 43

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Broken.Executable"
	.size	.L.str.28, 18

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"ELF: phentsize != sizeof(struct elf_program_hdr32)\n"
	.size	.L.str.29, 52

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"ELF: Program header table offset: %d\n"
	.size	.L.str.30, 38

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"ELF: Can't allocate memory for program headers\n"
	.size	.L.str.31, 48

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"------------------------------------\n"
	.size	.L.str.32, 38

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"ELF: Can't read segment #%d\n"
	.size	.L.str.33, 29

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"ELF: Possibly broken ELF file\n"
	.size	.L.str.34, 31

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"ELF: Segment #%d\n"
	.size	.L.str.35, 18

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"ELF: Segment type: 0x%x\n"
	.size	.L.str.36, 25

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"ELF: Segment offset: 0x%x\n"
	.size	.L.str.37, 27

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"ELF: Segment virtual address: 0x%x\n"
	.size	.L.str.38, 36

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"ELF: Segment real size: 0x%x\n"
	.size	.L.str.39, 30

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"ELF: Segment virtual size: 0x%x\n"
	.size	.L.str.40, 33

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"ELF: Can't calculate file offset of entry point\n"
	.size	.L.str.41, 49

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"ELF: Entry point address: 0x%.8x\n"
	.size	.L.str.42, 34

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"ELF: Entry point offset: 0x%.8x (%d)\n"
	.size	.L.str.43, 38

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"ELF: Number of sections: %d\n"
	.size	.L.str.44, 29

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"ELF: Suspicious number of sections\n"
	.size	.L.str.45, 36

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"ELF: shentsize != sizeof(struct elf_section_hdr32)\n"
	.size	.L.str.46, 52

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"ELF: Section header table offset: %d\n"
	.size	.L.str.47, 38

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"ELF: Can't allocate memory for section headers\n"
	.size	.L.str.48, 48

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"ELF: Can't read section header\n"
	.size	.L.str.49, 32

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"ELF: Section %d\n"
	.size	.L.str.50, 17

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"ELF: Section offset: %d\n"
	.size	.L.str.51, 25

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"ELF: Section size: %d\n"
	.size	.L.str.52, 23

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"ELF: Section type: Dynamic linking information\n"
	.size	.L.str.53, 48

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"ELF: Section type: Symbols for dynamic linking\n"
	.size	.L.str.54, 48

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"ELF: Section type: Array of pointers to termination functions\n"
	.size	.L.str.55, 63

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"ELF: Section type: Symbol hash table\n"
	.size	.L.str.56, 38

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"ELF: Section type: Array of pointers to initialization functions\n"
	.size	.L.str.57, 66

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"ELF: Section type: Empty section (NOBITS)\n"
	.size	.L.str.58, 43

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"ELF: Section type: Note section\n"
	.size	.L.str.59, 33

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"ELF: Section type: Null (no associated section)\n"
	.size	.L.str.60, 49

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"ELF: Section type: Array of pointers to preinit functions\n"
	.size	.L.str.61, 59

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"ELF: Section type: Program information\n"
	.size	.L.str.62, 40

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"ELF: Section type: Relocation entries w/o explicit addends\n"
	.size	.L.str.63, 60

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"ELF: Section type: Relocation entries with explicit addends\n"
	.size	.L.str.64, 61

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"ELF: Section type: String table\n"
	.size	.L.str.65, 33

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"ELF: Section type: Symbol table\n"
	.size	.L.str.66, 33

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"ELF: Section type: Provided symbol versions\n"
	.size	.L.str.67, 45

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"ELF: Section type: Required symbol versions\n"
	.size	.L.str.68, 45

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"ELF: Section type: Symbol Version Table\n"
	.size	.L.str.69, 41

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"ELF: Section type: Unknown\n"
	.size	.L.str.70, 28

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"ELF: Section contains writable data\n"
	.size	.L.str.71, 37

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"ELF: Section occupies memory\n"
	.size	.L.str.72, 30

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"ELF: Section contains executable code\n"
	.size	.L.str.73, 39

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"in cli_elfheader\n"
	.size	.L.str.74, 18


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
