	.text
	.file	"Ralignmm.bc"
	.globl	imp_match_out_scR
	.p2align	4, 0x90
	.type	imp_match_out_scR,@function
imp_match_out_scR:                      # @imp_match_out_scR
	.cfi_startproc
# BB#0:
	movq	impmtx(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end0:
	.size	imp_match_out_scR, .Lfunc_end0-imp_match_out_scR
	.cfi_endproc

	.globl	imp_match_init_strictR
	.p2align	4, 0x90
	.type	imp_match_init_strictR,@function
imp_match_init_strictR:                 # @imp_match_init_strictR
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%r8d, %r15d
	movl	%ecx, %r13d
	movl	%edx, %ebp
	movl	%esi, %r14d
	movl	imp_match_init_strictR.impalloclen(%rip), %eax
	leal	2(%r13), %ecx
	cmpl	%ecx, %eax
	jl	.LBB1_2
# BB#1:
	leal	2(%r15), %ecx
	cmpl	%ecx, %eax
	jge	.LBB1_9
.LBB1_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	FreeFloatMtx
.LBB1_4:
	movq	imp_match_init_strictR.nocount1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_6
# BB#5:
	callq	free
.LBB1_6:
	movq	imp_match_init_strictR.nocount2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_8
# BB#7:
	callq	free
.LBB1_8:
	cmpl	%r15d, %r13d
	movl	%r15d, %edi
	cmovgel	%r13d, %edi
	addl	$2, %edi
	movl	%edi, imp_match_init_strictR.impalloclen(%rip)
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
	movl	imp_match_init_strictR.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strictR.nocount1(%rip)
	movl	imp_match_init_strictR.impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strictR.nocount2(%rip)
.LBB1_9:                                # %.preheader209
	testl	%r13d, %r13d
	jle	.LBB1_19
# BB#10:                                # %.preheader208.lr.ph
	testl	%r14d, %r14d
	movq	imp_match_init_strictR.nocount1(%rip), %rax
	jle	.LBB1_11
# BB#23:                                # %.preheader208.us.preheader
	movslq	%r14d, %rcx
	movl	%r13d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB1_27
	.p2align	4, 0x90
.LBB1_24:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB1_26
# BB#25:                                #   in Loop: Header=BB1_24 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB1_24
.LBB1_26:                               # %._crit_edge230.us.prol
	cmpl	%r14d, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB1_27:                               # %.preheader208.us.prol.loopexit
	cmpl	$1, %r13d
	je	.LBB1_19
	.p2align	4, 0x90
.LBB1_28:                               # %.preheader208.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_29 Depth 2
                                        #     Child Loop BB1_32 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_29:                               #   Parent Loop BB1_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB1_31
# BB#30:                                #   in Loop: Header=BB1_29 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_29
.LBB1_31:                               # %._crit_edge230.us
                                        #   in Loop: Header=BB1_28 Depth=1
	cmpl	%r14d, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_32:                               #   Parent Loop BB1_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB1_34
# BB#33:                                #   in Loop: Header=BB1_32 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_32
.LBB1_34:                               # %._crit_edge230.us.1
                                        #   in Loop: Header=BB1_28 Depth=1
	cmpl	%r14d, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB1_28
	jmp	.LBB1_19
.LBB1_11:                               # %.preheader208.preheader
	setne	%cl
	movl	%r13d, %edx
	cmpl	$31, %r13d
	jbe	.LBB1_12
# BB#15:                                # %min.iters.checked
	movl	%r13d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB1_12
# BB#16:                                # %vector.ph
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB1_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB1_17
# BB#18:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB1_13
	jmp	.LBB1_19
.LBB1_12:
	xorl	%edi, %edi
.LBB1_13:                               # %.preheader208.preheader308
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB1_14:                               # %.preheader208
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB1_14
.LBB1_19:                               # %.preheader207
	testl	%r15d, %r15d
	jle	.LBB1_41
# BB#20:                                # %.preheader206.lr.ph
	testl	%ebp, %ebp
	movq	imp_match_init_strictR.nocount2(%rip), %rax
	jle	.LBB1_21
# BB#45:                                # %.preheader206.us.preheader
	movslq	%ebp, %rcx
	movl	%r15d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB1_49
	.p2align	4, 0x90
.LBB1_46:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rsp), %rdi
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_46 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB1_46
.LBB1_48:                               # %._crit_edge224.us.prol
	cmpl	%ebp, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB1_49:                               # %.preheader206.us.prol.loopexit
	cmpl	$1, %r15d
	je	.LBB1_41
	.p2align	4, 0x90
.LBB1_50:                               # %.preheader206.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_51 Depth 2
                                        #     Child Loop BB1_54 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_51:                               #   Parent Loop BB1_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB1_53
# BB#52:                                #   in Loop: Header=BB1_51 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_51
.LBB1_53:                               # %._crit_edge224.us
                                        #   in Loop: Header=BB1_50 Depth=1
	cmpl	%ebp, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_54:                               #   Parent Loop BB1_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB1_56
# BB#55:                                #   in Loop: Header=BB1_54 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB1_54
.LBB1_56:                               # %._crit_edge224.us.1
                                        #   in Loop: Header=BB1_50 Depth=1
	cmpl	%ebp, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB1_50
	jmp	.LBB1_41
.LBB1_21:                               # %.preheader206.preheader
	setne	%cl
	movl	%r15d, %edx
	cmpl	$31, %r15d
	jbe	.LBB1_22
# BB#35:                                # %min.iters.checked291
	movl	%r15d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB1_22
# BB#36:                                # %vector.ph295
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB1_37:                               # %vector.body287
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB1_37
# BB#38:                                # %middle.block288
	testl	%r8d, %r8d
	jne	.LBB1_39
	jmp	.LBB1_41
.LBB1_22:
	xorl	%edi, %edi
.LBB1_39:                               # %.preheader206.preheader306
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB1_40:                               # %.preheader206
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB1_40
.LBB1_41:                               # %.preheader205
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	testl	%r13d, %r13d
	jle	.LBB1_62
# BB#42:                                # %.preheader204.lr.ph
	testl	%r15d, %r15d
	jle	.LBB1_62
# BB#43:                                # %.preheader204.us.preheader
	movq	impmtx(%rip), %r12
	decl	%r15d
	leaq	4(,%r15,4), %rbp
	movl	%r13d, %r15d
	leaq	-1(%r15), %rax
	movq	%r15, %rbx
	andq	$7, %rbx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB1_44
# BB#57:                                # %.preheader204.us.prol.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_58:                               # %.preheader204.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	incq	%r13
	cmpq	%r13, %rbx
	jne	.LBB1_58
	jmp	.LBB1_59
.LBB1_44:
	xorl	%r13d, %r13d
.LBB1_59:                               # %.preheader204.us.prol.loopexit
	cmpq	$7, 24(%rsp)            # 8-byte Folded Reload
	jb	.LBB1_62
# BB#60:                                # %.preheader204.us.preheader.new
	subq	%r13, %r15
	leaq	56(%r12,%r13,8), %rbx
	.p2align	4, 0x90
.LBB1_61:                               # %.preheader204.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB1_61
.LBB1_62:                               # %._crit_edge221
	testl	%r14d, %r14d
	movl	4(%rsp), %eax           # 4-byte Reload
	jle	.LBB1_100
# BB#63:                                # %.preheader203.lr.ph
	movsd	fastathreshold(%rip), %xmm0 # xmm0 = mem[0],zero
	movq	impmtx(%rip), %r12
	movl	%eax, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%r14d, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$4294967295, %esi       # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB1_64:                               # %.preheader203
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_66 Depth 2
                                        #       Child Loop BB1_68 Depth 3
                                        #         Child Loop BB1_69 Depth 4
                                        #         Child Loop BB1_73 Depth 4
                                        #         Child Loop BB1_77 Depth 4
                                        #         Child Loop BB1_80 Depth 4
                                        #         Child Loop BB1_84 Depth 4
	testl	%eax, %eax
	jle	.LBB1_99
# BB#65:                                # %.lr.ph213
                                        #   in Loop: Header=BB1_64 Depth=1
	movq	120(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	136(%rsp), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_66:                               #   Parent Loop BB1_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_68 Depth 3
                                        #         Child Loop BB1_69 Depth 4
                                        #         Child Loop BB1_73 Depth 4
                                        #         Child Loop BB1_77 Depth 4
                                        #         Child Loop BB1_80 Depth 4
                                        #         Child Loop BB1_84 Depth 4
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r8,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_98
# BB#67:                                # %.lr.ph
                                        #   in Loop: Header=BB1_66 Depth=2
	movq	128(%rsp), %rax
	movsd	(%rax,%r8,8), %xmm2     # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %r14
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	subq	%r14, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB1_68:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_69 Depth 4
                                        #         Child Loop BB1_73 Depth 4
                                        #         Child Loop BB1_77 Depth 4
                                        #         Child Loop BB1_80 Depth 4
                                        #         Child Loop BB1_84 Depth 4
	movl	$-1, %ebp
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB1_69:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.LBB1_70
# BB#71:                                #   in Loop: Header=BB1_69 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %al
	setne	%cl
	addl	%ecx, %ebp
	cmpl	24(%rbx), %ebp
	movl	%ebp, %edx
	jne	.LBB1_69
	jmp	.LBB1_72
	.p2align	4, 0x90
.LBB1_70:                               # %._crit_edge273
                                        #   in Loop: Header=BB1_68 Depth=3
	movl	24(%rbx), %edx
.LBB1_72:                               # %.loopexit
                                        #   in Loop: Header=BB1_68 Depth=3
	movq	%rdi, %r9
	subq	%r14, %r9
	addq	%rsi, %r9
	movl	28(%rbx), %eax
	cmpl	%eax, %edx
	movl	%r9d, %r13d
	je	.LBB1_76
	.p2align	4, 0x90
.LBB1_73:                               # %.preheader202
                                        #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.LBB1_75
# BB#74:                                #   in Loop: Header=BB1_73 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %dl
	setne	%cl
	addl	%ecx, %ebp
	cmpl	%eax, %ebp
	jne	.LBB1_73
.LBB1_75:                               #   in Loop: Header=BB1_68 Depth=3
	addl	24(%rsp), %edi          # 4-byte Folded Reload
	movl	%edi, %r13d
.LBB1_76:                               #   in Loop: Header=BB1_68 Depth=3
	movq	112(%rsp), %rax
	movq	(%rax,%r8,8), %r10
	movl	$-1, %ebp
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB1_77:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.LBB1_79
# BB#78:                                #   in Loop: Header=BB1_77 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %al
	setne	%cl
	addl	%ecx, %ebp
	cmpl	32(%rbx), %ebp
	jne	.LBB1_77
.LBB1_79:                               #   in Loop: Header=BB1_68 Depth=3
	movq	%rdi, %r11
	subq	%r10, %r11
	addq	%rsi, %r11
	movl	36(%rbx), %eax
	cmpl	%eax, 32(%rbx)
	movl	%r11d, %r15d
	je	.LBB1_83
	.p2align	4, 0x90
.LBB1_80:                               # %.preheader
                                        #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.LBB1_82
# BB#81:                                #   in Loop: Header=BB1_80 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %dl
	setne	%cl
	addl	%ecx, %ebp
	cmpl	%eax, %ebp
	jne	.LBB1_80
.LBB1_82:                               #   in Loop: Header=BB1_68 Depth=3
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	subl	%r10d, %eax
	addl	%eax, %edi
	movl	%edi, %r15d
.LBB1_83:                               #   in Loop: Header=BB1_68 Depth=3
	movq	%r8, %rdi
	movslq	%r9d, %r8
	addq	%r14, %r8
	movslq	%r11d, %rax
	addq	%rax, %r10
	.p2align	4, 0x90
.LBB1_84:                               #   Parent Loop BB1_64 Depth=1
                                        #     Parent Loop BB1_66 Depth=2
                                        #       Parent Loop BB1_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r8), %edx
	testb	%dl, %dl
	je	.LBB1_97
# BB#85:                                #   in Loop: Header=BB1_84 Depth=4
	movzbl	(%r10), %eax
	testb	%al, %al
	je	.LBB1_97
# BB#86:                                #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %dl
	je	.LBB1_89
# BB#87:                                #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %al
	je	.LBB1_89
# BB#88:                                #   in Loop: Header=BB1_84 Depth=4
	movss	64(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movslq	%r9d, %r9
	movq	(%r12,%r9,8), %rax
	movslq	%r11d, %r11
	addss	(%rax,%r11,4), %xmm3
	movss	%xmm3, (%rax,%r11,4)
	incl	%r9d
	incl	%r11d
	incq	%r8
	jmp	.LBB1_94
	.p2align	4, 0x90
.LBB1_89:                               #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %al
	setne	%cl
	cmpb	$45, %dl
	je	.LBB1_91
# BB#90:                                #   in Loop: Header=BB1_84 Depth=4
	testb	%cl, %cl
	je	.LBB1_93
.LBB1_91:                               #   in Loop: Header=BB1_84 Depth=4
	cmpb	$45, %dl
	jne	.LBB1_95
# BB#92:                                #   in Loop: Header=BB1_84 Depth=4
	incl	%r9d
	incq	%r8
	cmpb	$45, %al
	jne	.LBB1_95
	.p2align	4, 0x90
.LBB1_93:                               #   in Loop: Header=BB1_84 Depth=4
	incl	%r11d
.LBB1_94:                               # %.thread
                                        #   in Loop: Header=BB1_84 Depth=4
	incq	%r10
.LBB1_95:                               # %.thread
                                        #   in Loop: Header=BB1_84 Depth=4
	cmpl	%r15d, %r11d
	jg	.LBB1_97
# BB#96:                                # %.thread
                                        #   in Loop: Header=BB1_84 Depth=4
	cmpl	%r13d, %r9d
	jle	.LBB1_84
	.p2align	4, 0x90
.LBB1_97:                               # %.critedge
                                        #   in Loop: Header=BB1_68 Depth=3
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	movl	$4294967295, %esi       # imm = 0xFFFFFFFF
	movq	%rdi, %r8
	jne	.LBB1_68
.LBB1_98:                               # %._crit_edge
                                        #   in Loop: Header=BB1_66 Depth=2
	incq	%r8
	cmpq	40(%rsp), %r8           # 8-byte Folded Reload
	jne	.LBB1_66
.LBB1_99:                               # %._crit_edge214
                                        #   in Loop: Header=BB1_64 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	movl	4(%rsp), %eax           # 4-byte Reload
	jne	.LBB1_64
.LBB1_100:                              # %._crit_edge216
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	imp_match_init_strictR, .Lfunc_end1-imp_match_init_strictR
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4608533498688228557     # double 1.3
.LCPI2_1:
	.quad	-4620693217682128896    # double -0.5
.LCPI2_2:
	.quad	4602678819172646912     # double 0.5
.LCPI2_3:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_4:
	.long	1176256512              # float 1.0E+4
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_5:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI2_6:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI2_7:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI2_8:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI2_9:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.globl	R__align
	.p2align	4, 0x90
	.type	R__align,@function
R__align:                               # @R__align
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$488, %rsp              # imm = 0x1E8
.Lcfi19:
	.cfi_def_cfa_offset 544
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movl	%r8d, 28(%rsp)          # 4-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 108(%rsp)        # 4-byte Spill
	movl	R__align.orlgth1(%rip), %r13d
	testl	%r13d, %r13d
	jne	.LBB2_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, R__align.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, R__align.mseq2(%rip)
	movl	R__align.orlgth1(%rip), %r13d
.LBB2_2:
	movq	%rbx, 64(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%r14), %rdi
	callq	strlen
	movq	%rbx, 72(%rsp)          # 8-byte Spill
	cmpl	%r13d, %ebx
	movl	R__align.orlgth2(%rip), %r15d
	movq	%r14, 80(%rsp)          # 8-byte Spill
	movl	%r12d, 52(%rsp)         # 4-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jg	.LBB2_4
# BB#3:
	cmpl	%r15d, %eax
	jle	.LBB2_9
.LBB2_4:
	testl	%r13d, %r13d
	jle	.LBB2_5
# BB#6:
	testl	%r15d, %r15d
	movq	72(%rsp), %rbx          # 8-byte Reload
	jle	.LBB2_8
# BB#7:
	movq	R__align.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.match(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.m(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.mp(%rip), %rdi
	callq	FreeIntVec
	movq	R__align.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	R__align.digf1(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.digf2(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.diaf1(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.diaf2(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.gapz1(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.gapz2(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.gapf1(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.gapf2(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.ogcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.ogcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.fgcp1(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.fgcp2(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.ogcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.ogcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.fgcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.fgcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	R__align.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	R__align.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	R__align.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	R__align.intwork(%rip), %rdi
	callq	FreeIntMtx
	movl	R__align.orlgth1(%rip), %r13d
	movl	R__align.orlgth2(%rip), %r15d
	jmp	.LBB2_8
.LBB2_5:
	movq	72(%rsp), %rbx          # 8-byte Reload
.LBB2_8:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	leal	100(%r13), %ebx
	movq	88(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	100(%r15), %r12d
	leal	102(%r15), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.w1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.w2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.match(%rip)
	leal	102(%r13), %r14d
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.initverticalw(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.lastverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.m(%rip)
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, R__align.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r15,%r13), %esi
	callq	AllocateCharMtx
	movq	%rax, R__align.mseq(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.digf1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.digf2(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.diaf1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.diaf2(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.gapz1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.gapz2(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.gapf1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.gapf2(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.ogcp1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.ogcp2(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.fgcp1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.fgcp2(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.ogcp1g(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.ogcp2g(%rip)
	movl	%r14d, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.fgcp1g(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, R__align.fgcp2g(%rip)
	movl	$26, %edi
	movl	%r14d, %esi
	callq	AllocateFloatMtx
	movq	%rax, R__align.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, R__align.cpmx2(%rip)
	cmpl	%r12d, %ebx
	cmovgel	%ebx, %r12d
	addl	$2, %r12d
	movl	$26, %esi
	movl	%r12d, %edi
	callq	AllocateFloatMtx
	movq	%rax, R__align.floatwork(%rip)
	movl	$27, %esi
	movl	%r12d, %edi
	callq	AllocateIntMtx
	movq	%rax, R__align.intwork(%rip)
	movl	%r13d, R__align.orlgth1(%rip)
	movl	%r15d, R__align.orlgth2(%rip)
	movq	80(%rsp), %r14          # 8-byte Reload
	movl	52(%rsp), %r12d         # 4-byte Reload
.LBB2_9:                                # %.preheader590
	movl	28(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB2_16
# BB#10:                                # %.lr.ph640
	movq	R__align.mseq(%rip), %r9
	movq	R__align.mseq1(%rip), %rdi
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	movl	28(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%ebp, %ebp
	andq	$3, %rbx
	je	.LBB2_13
# BB#11:                                # %.prol.preheader1075
	movq	64(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rbp,8), %rdx
	movq	%rdx, (%rdi,%rbp,8)
	movq	(%rsi,%rbp,8), %rdx
	movb	$0, (%rdx,%rax)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_12
.LBB2_13:                               # %.prol.loopexit1076
	cmpq	$3, %r8
	jb	.LBB2_16
# BB#14:                                # %.lr.ph640.new
	subq	%rbp, %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rbp,8), %rdx
	leaq	24(%rdi,%rbp,8), %rdi
	leaq	24(%r9,%rbp,8), %rsi
	.p2align	4, 0x90
.LBB2_15:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rsi), %rbp
	movq	%rbp, (%rdi)
	movq	(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB2_15
.LBB2_16:                               # %.preheader589
	testl	%r12d, %r12d
	jle	.LBB2_24
# BB#17:                                # %.lr.ph637
	movq	R__align.mseq(%rip), %r8
	movq	R__align.mseq2(%rip), %rdi
	movslq	88(%rsp), %rax          # 4-byte Folded Reload
	movslq	28(%rsp), %r10          # 4-byte Folded Reload
	movl	%r12d, %ecx
	leaq	-1(%rcx), %r9
	movq	%rcx, %rdx
	andq	$3, %rdx
	je	.LBB2_18
# BB#19:                                # %.prol.preheader1070
	leaq	(%r8,%r10,8), %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movq	%rbp, (%rdi,%rbx,8)
	movq	(%r14,%rbx,8), %rbp
	movb	$0, (%rbp,%rax)
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB2_20
	jmp	.LBB2_21
.LBB2_18:
	xorl	%ebx, %ebx
.LBB2_21:                               # %.prol.loopexit1071
	cmpq	$3, %r9
	jb	.LBB2_24
# BB#22:                                # %.lr.ph637.new
	subq	%rbx, %rcx
	leaq	24(%r14,%rbx,8), %rsi
	leaq	24(%rdi,%rbx,8), %rdi
	addq	%rbx, %r10
	leaq	24(%r8,%r10,8), %rdx
	.p2align	4, 0x90
.LBB2_23:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rdx), %rbp
	movq	%rbp, (%rdi)
	movq	(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB2_23
.LBB2_24:                               # %._crit_edge638
	movl	commonAlloc1(%rip), %ebp
	cmpl	%ebp, %r13d
	movl	commonAlloc2(%rip), %ebx
	movl	28(%rsp), %r8d          # 4-byte Reload
	jg	.LBB2_27
# BB#25:                                # %._crit_edge638
	cmpl	%ebx, %r15d
	jg	.LBB2_27
# BB#26:                                # %._crit_edge712
	movq	commonIP(%rip), %rax
	movq	72(%rsp), %r12          # 8-byte Reload
	jmp	.LBB2_32
.LBB2_27:                               # %._crit_edge638._crit_edge
	testl	%ebp, %ebp
	je	.LBB2_28
# BB#29:                                # %._crit_edge638._crit_edge
	testl	%ebx, %ebx
	movq	72(%rsp), %r12          # 8-byte Reload
	je	.LBB2_31
# BB#30:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	R__align.orlgth1(%rip), %r13d
	movl	commonAlloc1(%rip), %ebp
	movl	R__align.orlgth2(%rip), %r15d
	movl	commonAlloc2(%rip), %ebx
	jmp	.LBB2_31
.LBB2_28:
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB2_31:
	cmpl	%ebp, %r13d
	cmovgel	%r13d, %ebp
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	leal	10(%rbp), %edi
	leal	10(%rbx), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebp, commonAlloc1(%rip)
	movl	%ebx, commonAlloc2(%rip)
	movl	28(%rsp), %r8d          # 4-byte Reload
.LBB2_32:
	movq	88(%rsp), %rbx          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r13          # 8-byte Reload
	movq	%rax, R__align.ijp(%rip)
	movq	R__align.cpmx1(%rip), %rsi
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdi
	movq	%r13, %rdx
	movl	%r12d, %ecx
	movl	%r8d, %r13d
	callq	cpmx_calc_new
	movq	R__align.cpmx2(%rip), %rsi
	movq	%r14, %rdi
	movq	%rbp, %rdx
	movl	%ebx, %ecx
	movl	52(%rsp), %r8d          # 4-byte Reload
	callq	cpmx_calc_new
	movq	568(%rsp), %rax
	testq	%rax, %rax
	movq	R__align.ogcp1g(%rip), %rdi
	je	.LBB2_34
# BB#33:
	movq	%r15, %r12
	movq	584(%rsp), %rcx
	movq	%rcx, (%rsp)
	movl	%r13d, %esi
	movq	%r12, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	movl	%r14d, %r8d
	movq	%rax, %r9
	callq	new_OpeningGapCount_zure
	movq	R__align.ogcp2g(%rip), %rdi
	movq	592(%rsp), %rax
	movq	%rax, (%rsp)
	movl	52(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	movl	%ebx, %r8d
	movq	576(%rsp), %r9
	callq	new_OpeningGapCount_zure
	movq	R__align.fgcp1g(%rip), %rdi
	movq	584(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %esi
	movq	%r12, %rdx
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movl	%r14d, %r8d
	movq	568(%rsp), %r9
	callq	new_FinalGapCount_zure
	movq	R__align.fgcp2g(%rip), %rdi
	movq	592(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r15d, %esi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	movl	%ebx, %r8d
	movq	576(%rsp), %r9
	callq	new_FinalGapCount_zure
	movq	R__align.digf1(%rip), %rdi
	movq	584(%rsp), %rax
	movq	%rax, (%rsp)
	movl	28(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movl	%r14d, %r8d
	movq	568(%rsp), %rax
	movq	%rax, %r9
	callq	getdigapfreq_part
	movq	R__align.digf2(%rip), %rdi
	movq	592(%rsp), %rax
	movq	%rax, (%rsp)
	movl	52(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %esi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	movq	576(%rsp), %r9
	callq	getdigapfreq_part
	movq	R__align.diaf1(%rip), %rdi
	movq	584(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r15d, %esi
	movq	%rbp, %rdx
	movq	%r12, %rcx
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	%r12d, %r8d
	movq	568(%rsp), %r9
	callq	getdiaminofreq_part
	movq	R__align.diaf2(%rip), %rdi
	movq	592(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r13d, %esi
	movq	80(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movl	%ebx, %r8d
	movq	576(%rsp), %r9
	movq	80(%rsp), %rbx          # 8-byte Reload
	callq	getdiaminofreq_part
	movq	R__align.gapf1(%rip), %rdi
	movl	%r15d, %esi
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movl	%r12d, %r8d
	callq	getgapfreq
	movq	R__align.gapf2(%rip), %rdi
	movl	%r13d, %esi
	movq	%rbx, %rdx
	movq	%rbp, %rcx
	movq	88(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getgapfreq
	movq	R__align.gapz1(%rip), %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rdx
	movq	%r14, %rcx
	movl	%r12d, %r8d
	movq	568(%rsp), %r15
	movq	%r15, %r9
	callq	getgapfreq_zure_part
	movq	R__align.gapz2(%rip), %rdi
	movl	%r13d, %esi
	movq	%rbx, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	movq	%r15, %r9
	callq	getgapfreq_zure_part
	movq	%r12, %r8
	movq	%rbp, %r13
	jmp	.LBB2_35
.LBB2_34:
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %r8d
	callq	st_OpeningGapCount
	movq	R__align.ogcp2g(%rip), %rdi
	movq	%r15, %r12
	movl	52(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	movq	%r14, %rdx
	movq	%rbp, %rcx
	movl	%ebx, %r8d
	movq	%rbx, %rbp
	callq	st_OpeningGapCount
	movq	R__align.fgcp1g(%rip), %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r12, %rdx
	movq	32(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movl	%r13d, %r8d
	movq	%r13, %rbx
	callq	st_FinalGapCount_zure
	movq	R__align.fgcp2g(%rip), %rdi
	movl	%r15d, %esi
	movl	%r15d, %r13d
	movq	%r14, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	callq	st_FinalGapCount_zure
	movq	R__align.digf1(%rip), %rdi
	movl	28(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	movl	%ebx, %r8d
	callq	getdigapfreq_st
	movq	R__align.digf2(%rip), %rdi
	movl	%r13d, %esi
	movq	%r14, %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	88(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	getdigapfreq_st
	movq	R__align.diaf1(%rip), %rdi
	movl	%ebp, %esi
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	72(%rsp), %r15          # 8-byte Reload
	movl	%r15d, %r8d
	callq	getdiaminofreq_x
	movq	R__align.diaf2(%rip), %rdi
	movl	%r13d, %esi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	movq	88(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	getdiaminofreq_x
	movq	R__align.gapf1(%rip), %rdi
	movl	%ebp, %esi
	movq	64(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdx
	movq	%r12, %rcx
	movq	%r15, %rbp
	movl	%ebp, %r8d
	callq	getgapfreq
	movq	R__align.gapf2(%rip), %rdi
	movl	52(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	movq	%r14, %rdx
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movl	%ebx, %r8d
	callq	getgapfreq
	movq	R__align.gapz1(%rip), %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getgapfreq_zure
	movq	R__align.gapz2(%rip), %rdi
	movl	%r15d, %esi
	movq	%r14, %rdx
	movq	%r12, %rcx
	movl	%ebx, %r8d
	callq	getgapfreq_zure
	movq	%rbp, %r8
	movq	%rbx, %r13
.LBB2_35:
	movq	552(%rsp), %r12
	movq	%r12, %rbp
	movq	R__align.w1(%rip), %r15
	movq	R__align.w2(%rip), %r14
	movq	R__align.initverticalw(%rip), %rdi
	movq	R__align.cpmx2(%rip), %rsi
	movq	R__align.cpmx1(%rip), %rdx
	movq	R__align.floatwork(%rip), %r9
	movq	R__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	%r8, %rbx
	callq	match_calc
	testq	%rbp, %rbp
	movq	%r15, %r12
	je	.LBB2_60
# BB#36:
	testl	%ebx, %ebx
	movq	%rbx, %r10
	jle	.LBB2_45
# BB#37:                                # %.lr.ph.i
	movq	R__align.initverticalw(%rip), %rax
	movq	impmtx(%rip), %r8
	movl	%r10d, %ecx
	leaq	-1(%rcx), %rsi
	movq	%r10, %rbp
	andq	$3, %rbp
	je	.LBB2_38
# BB#39:                                # %.prol.preheader1065
	negq	%rbp
	xorl	%edi, %edi
	movq	%r8, %rdx
	.p2align	4, 0x90
.LBB2_40:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	leaq	4(%rax), %rax
	decq	%rdi
	addq	$8, %rdx
	cmpq	%rdi, %rbp
	jne	.LBB2_40
# BB#41:                                # %.prol.loopexit1066.unr-lcssa
	negq	%rdi
	cmpq	$3, %rsi
	jae	.LBB2_43
	jmp	.LBB2_45
.LBB2_60:                               # %.critedge
	movq	R__align.cpmx1(%rip), %rsi
	movq	R__align.cpmx2(%rip), %rdx
	movq	R__align.floatwork(%rip), %r9
	movq	R__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	movl	%r13d, %r8d
	callq	match_calc
	movss	108(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movq	%rbx, %r11
	cmpl	$1, outgap(%rip)
	je	.LBB2_72
	jmp	.LBB2_62
.LBB2_38:
	xorl	%edi, %edi
	cmpq	$3, %rsi
	jb	.LBB2_45
.LBB2_43:                               # %.lr.ph.i.new
	subq	%rdi, %rcx
	leaq	24(%r8,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB2_44:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	movq	-16(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	4(%rax), %xmm0
	movss	%xmm0, 4(%rax)
	movq	-8(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	8(%rax), %xmm0
	movss	%xmm0, 8(%rax)
	movq	(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	12(%rax), %xmm0
	movss	%xmm0, 12(%rax)
	addq	$32, %rdx
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB2_44
.LBB2_45:                               # %imp_match_out_vead_tateR.exit
	movq	R__align.cpmx1(%rip), %rsi
	movq	R__align.cpmx2(%rip), %rdx
	movq	R__align.floatwork(%rip), %r9
	movq	R__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	%r12, %rdi
	movl	%r13d, %r8d
	movq	%r10, %rbx
	callq	match_calc
	movq	%rbx, %r11
	testl	%r13d, %r13d
	movss	108(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	je	.LBB2_61
# BB#46:                                # %.lr.ph.preheader.i537
	movq	impmtx(%rip), %rax
	movq	(%rax), %rsi
	leal	-1(%r13), %edi
	incq	%rdi
	cmpq	$8, %rdi
	jae	.LBB2_48
# BB#47:
	movq	%rsi, %rax
	movl	%r13d, %ecx
	movq	%r12, %rdx
	jmp	.LBB2_55
.LBB2_48:                               # %min.iters.checked
	movl	%r13d, %ebp
	andl	$7, %ebp
	subq	%rbp, %rdi
	movq	%r12, %rbx
	je	.LBB2_49
# BB#50:                                # %vector.memcheck
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	addl	%r13d, %eax
	leaq	4(%rsi,%rax,4), %rcx
	cmpq	%rcx, %rbx
	jae	.LBB2_52
# BB#51:                                # %vector.memcheck
	leaq	4(%rbx,%rax,4), %rax
	cmpq	%rax, %rsi
	jae	.LBB2_52
.LBB2_49:
	movq	%rsi, %rax
	movl	%r13d, %ecx
	movq	%rbx, %rdx
	jmp	.LBB2_55
.LBB2_52:                               # %vector.body.preheader
	leaq	(%rsi,%rdi,4), %rax
	movl	%r13d, %ecx
	subl	%edi, %ecx
	leaq	(%rbx,%rdi,4), %rdx
	leaq	16(%rbx), %rbx
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB2_53:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbx)
	movups	%xmm3, (%rbx)
	addq	$32, %rbx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB2_53
# BB#54:                                # %middle.block
	testq	%rbp, %rbp
	je	.LBB2_61
.LBB2_55:                               # %.lr.ph.i538.preheader
	leal	-1(%rcx), %esi
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB2_58
# BB#56:                                # %.lr.ph.i538.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB2_57:                               # %.lr.ph.i538.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rax
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	leaq	4(%rdx), %rdx
	incl	%edi
	jne	.LBB2_57
.LBB2_58:                               # %.lr.ph.i538.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB2_61
	.p2align	4, 0x90
.LBB2_59:                               # %.lr.ph.i538
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 4(%rdx)
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	movss	%xmm0, 8(%rdx)
	addl	$-4, %ecx
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rax), %rax
	addss	12(%rdx), %xmm0
	movss	%xmm0, 12(%rdx)
	leaq	16(%rdx), %rdx
	jne	.LBB2_59
.LBB2_61:                               # %imp_match_out_veadR.exit
	cmpl	$1, outgap(%rip)
	jne	.LBB2_62
.LBB2_72:
	movq	R__align.ogcp1g(%rip), %rdx
	movss	(%rdx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm4
	movq	R__align.ogcp2g(%rip), %r10
	movss	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm5
	movsd	.LCPI2_3(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm6
	subsd	%xmm5, %xmm6
	mulsd	%xmm4, %xmm6
	cvtss2sd	%xmm11, %xmm10
	mulsd	%xmm10, %xmm6
	movsd	.LCPI2_2(%rip), %xmm9   # xmm9 = mem[0],zero
	mulsd	%xmm9, %xmm6
	xorpd	%xmm8, %xmm8
	addsd	%xmm8, %xmm6
	cvtsd2ss	%xmm6, %xmm6
	movapd	%xmm1, %xmm7
	subsd	%xmm4, %xmm7
	mulsd	%xmm5, %xmm7
	mulsd	%xmm10, %xmm7
	mulsd	%xmm9, %xmm7
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm6, %xmm4
	addsd	%xmm7, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movq	R__align.fgcp1g(%rip), %rsi
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	movq	R__align.fgcp2g(%rip), %r8
	movss	(%r8), %xmm6            # xmm6 = mem[0],zero,zero,zero
	cvtss2sd	%xmm6, %xmm6
	movapd	%xmm1, %xmm7
	subsd	%xmm6, %xmm7
	mulsd	%xmm5, %xmm7
	mulsd	%xmm10, %xmm7
	mulsd	%xmm9, %xmm7
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm7, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movapd	%xmm1, %xmm7
	subsd	%xmm5, %xmm7
	mulsd	%xmm6, %xmm7
	mulsd	%xmm10, %xmm7
	mulsd	%xmm9, %xmm7
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm7, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movq	R__align.initverticalw(%rip), %rdi
	movss	(%rdi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%rdi)
	movq	%r12, %rax
	addss	(%rax), %xmm4
	movss	%xmm4, (%rax)
	testl	%r11d, %r11d
	jle	.LBB2_75
# BB#73:                                # %.lr.ph631
	movq	R__align.gapz2(%rip), %rbp
	movq	R__align.digf1(%rip), %rbx
	movq	R__align.diaf1(%rip), %rcx
	leal	1(%r11), %r9d
	decq	%r9
	xorps	%xmm4, %xmm4
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB2_74:                               # =>This Inner Loop Header: Depth=1
	movss	(%rbp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	movapd	%xmm1, %xmm6
	subsd	%xmm5, %xmm6
	movss	(%rdx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	cvtss2sd	%xmm7, %xmm7
	movapd	%xmm1, %xmm0
	subsd	%xmm7, %xmm0
	addsd	%xmm8, %xmm0
	mulsd	%xmm6, %xmm0
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	cvtss2sd	%xmm6, %xmm6
	movapd	%xmm1, %xmm7
	subsd	%xmm6, %xmm7
	movss	(%rcx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	cvtss2sd	%xmm6, %xmm6
	subsd	%xmm6, %xmm7
	mulsd	%xmm5, %xmm7
	addsd	%xmm0, %xmm7
	mulsd	%xmm9, %xmm7
	mulsd	%xmm10, %xmm7
	xorps	%xmm5, %xmm5
	cvtsd2ss	%xmm7, %xmm5
	addss	4(%rdi,%rax,4), %xmm5
	movss	%xmm5, 4(%rdi,%rax,4)
	movss	4(%rbp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movapd	%xmm1, %xmm6
	subsd	%xmm0, %xmm6
	movss	4(%rsi,%rax,4), %xmm7   # xmm7 = mem[0],zero,zero,zero
	cvtss2sd	%xmm7, %xmm7
	movapd	%xmm1, %xmm3
	subsd	%xmm7, %xmm3
	movss	4(%rdx,%rax,4), %xmm7   # xmm7 = mem[0],zero,zero,zero
	cvtss2sd	%xmm7, %xmm7
	addsd	%xmm3, %xmm7
	mulsd	%xmm6, %xmm7
	movss	4(%rbx,%rax,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm6
	subsd	%xmm3, %xmm6
	movss	4(%rcx,%rax,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm3, %xmm6
	mulsd	%xmm0, %xmm6
	addsd	%xmm7, %xmm6
	mulsd	%xmm9, %xmm6
	mulsd	%xmm10, %xmm6
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm6, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 4(%rdi,%rax,4)
	movss	4(%rsi,%rax,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm5
	mulss	%xmm3, %xmm5
	addss	4(%rdx,%rax,4), %xmm3
	mulss	%xmm11, %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm9, %xmm3
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm4, %xmm3
	cvtss2sd	%xmm5, %xmm5
	mulsd	%xmm9, %xmm5
	subsd	%xmm5, %xmm3
	cvtss2sd	%xmm0, %xmm0
	addsd	%xmm3, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 4(%rdi,%rax,4)
	incq	%rax
	cmpq	%rax, %r9
	jne	.LBB2_74
.LBB2_75:                               # %.preheader584
	testl	%r13d, %r13d
	jle	.LBB2_76
# BB#77:                                # %.lr.ph627
	movq	R__align.gapz1(%rip), %rax
	movq	R__align.digf2(%rip), %rcx
	movq	R__align.diaf2(%rip), %rsi
	leal	1(%r13), %edi
	xorps	%xmm4, %xmm4
	movl	$1, %ebp
	xorpd	%xmm0, %xmm0
	movq	%r12, %rdx
	movq	552(%rsp), %r12
	.p2align	4, 0x90
.LBB2_78:                               # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm3           # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm5
	subsd	%xmm3, %xmm5
	movss	(%r10), %xmm6           # xmm6 = mem[0],zero,zero,zero
	cvtss2sd	%xmm6, %xmm6
	movapd	%xmm1, %xmm7
	subsd	%xmm6, %xmm7
	addsd	%xmm0, %xmm7
	mulsd	%xmm5, %xmm7
	movss	(%rcx,%rbp,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	movapd	%xmm1, %xmm6
	subsd	%xmm5, %xmm6
	movss	(%rsi,%rbp,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm6
	mulsd	%xmm3, %xmm6
	addsd	%xmm7, %xmm6
	mulsd	%xmm9, %xmm6
	mulsd	%xmm10, %xmm6
	xorps	%xmm5, %xmm5
	cvtsd2ss	%xmm6, %xmm5
	addss	(%rdx,%rbp,4), %xmm5
	movss	%xmm5, (%rdx,%rbp,4)
	movss	4(%rax), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	movapd	%xmm1, %xmm6
	subsd	%xmm3, %xmm6
	movss	(%r8,%rbp,4), %xmm7     # xmm7 = mem[0],zero,zero,zero
	cvtss2sd	%xmm7, %xmm7
	movapd	%xmm1, %xmm2
	subsd	%xmm7, %xmm2
	movss	(%r10,%rbp,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	cvtss2sd	%xmm7, %xmm7
	addsd	%xmm2, %xmm7
	mulsd	%xmm6, %xmm7
	movss	(%rcx,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm1, %xmm6
	subsd	%xmm2, %xmm6
	movss	(%rsi,%rbp,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm2, %xmm6
	mulsd	%xmm3, %xmm6
	addsd	%xmm7, %xmm6
	mulsd	%xmm9, %xmm6
	mulsd	%xmm10, %xmm6
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm6, %xmm2
	addss	%xmm5, %xmm2
	movss	%xmm2, (%rdx,%rbp,4)
	movss	(%r8,%rbp,4), %xmm3     # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm5
	mulss	%xmm3, %xmm5
	addss	(%r10,%rbp,4), %xmm3
	mulss	%xmm11, %xmm3
	cvtss2sd	%xmm3, %xmm3
	mulsd	%xmm9, %xmm3
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm4, %xmm3
	cvtss2sd	%xmm5, %xmm5
	mulsd	%xmm9, %xmm5
	subsd	%xmm5, %xmm3
	cvtss2sd	%xmm2, %xmm2
	addsd	%xmm3, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movss	%xmm2, (%rdx,%rbp,4)
	incq	%rbp
	cmpq	%rbp, %rdi
	jne	.LBB2_78
	jmp	.LBB2_83
.LBB2_62:                               # %.preheader588
	testl	%r13d, %r13d
	movq	%r12, %rdx
	movq	552(%rsp), %r12
	jle	.LBB2_69
# BB#63:                                # %.lr.ph635
	movslq	offset(%rip), %r8
	leaq	1(%r13), %rax
	movl	%eax, %ecx
	testb	$1, %al
	jne	.LBB2_64
# BB#65:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	mulsd	.LCPI2_1(%rip), %xmm0
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rdx)
	movl	$2, %eax
	cmpq	$2, %rcx
	jne	.LBB2_67
	jmp	.LBB2_69
.LBB2_76:                               # %.loopexit585.thread
	movq	R__align.m(%rip), %r15
	movl	$0, (%r15)
	movb	$1, 27(%rsp)            # 1-byte Folded Spill
	movq	%r14, %rbx
	movq	%r12, %rdx
	movq	552(%rsp), %r12
	jmp	.LBB2_99
.LBB2_64:
	movl	$1, %eax
	cmpq	$2, %rcx
	je	.LBB2_69
.LBB2_67:                               # %.lr.ph635.new
	subq	%rax, %rcx
	leaq	4(%rdx,%rax,4), %rsi
	leaq	1(%rax), %rdi
	imull	%r8d, %eax
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%ebp, %ebp
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_68:                               # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%r8d, %ebp
	addq	$-2, %rcx
	jne	.LBB2_68
.LBB2_69:                               # %.preheader586
	testl	%r11d, %r11d
	jle	.LBB2_83
# BB#70:                                # %.lr.ph633
	movq	R__align.initverticalw(%rip), %rsi
	movslq	offset(%rip), %r8
	leaq	1(%r11), %rax
	movl	%eax, %ecx
	testb	$1, %al
	jne	.LBB2_71
# BB#79:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r8d, %xmm0
	mulsd	.LCPI2_1(%rip), %xmm0
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	movl	$2, %eax
	cmpq	$2, %rcx
	jne	.LBB2_81
	jmp	.LBB2_83
.LBB2_71:
	movl	$1, %eax
	cmpq	$2, %rcx
	je	.LBB2_83
.LBB2_81:                               # %.lr.ph633.new
	subq	%rax, %rcx
	leaq	4(%rsi,%rax,4), %rsi
	leaq	1(%rax), %rdi
	imull	%r8d, %eax
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%ebp, %ebp
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_82:                               # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%r8d, %ebp
	addq	$-2, %rcx
	jne	.LBB2_82
.LBB2_83:                               # %.loopexit585
	movq	R__align.m(%rip), %r15
	movl	$0, (%r15)
	testl	%r13d, %r13d
	setle	27(%rsp)                # 1-byte Folded Spill
	jle	.LBB2_84
# BB#85:                                # %.lr.ph623
	movq	R__align.mp(%rip), %rax
	movss	.LCPI2_4(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	leaq	1(%r13), %rbp
	movl	%ebp, %edi
	leaq	-1(%rdi), %r9
	cmpq	$8, %r9
	jb	.LBB2_86
# BB#92:                                # %min.iters.checked743
	movl	%r13d, %r8d
	andl	$7, %r8d
	movq	%r9, %rbx
	subq	%r8, %rbx
	je	.LBB2_86
# BB#93:                                # %vector.memcheck758
	leaq	4(%r15), %rcx
	leaq	-4(%rdx,%rdi,4), %rsi
	cmpq	%rsi, %rcx
	jae	.LBB2_95
# BB#94:                                # %vector.memcheck758
	leaq	(%r15,%rdi,4), %rcx
	cmpq	%rcx, %rdx
	jae	.LBB2_95
.LBB2_86:
	movl	$1, %esi
.LBB2_87:                               # %scalar.ph741.preheader
	subl	%esi, %ebp
	subq	%rsi, %r9
	andq	$3, %rbp
	je	.LBB2_90
# BB#88:                                # %scalar.ph741.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB2_89:                               # %scalar.ph741.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rsi,4)
	movss	-4(%rdx,%rsi,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15,%rsi,4)
	incq	%rsi
	incq	%rbp
	jne	.LBB2_89
.LBB2_90:                               # %scalar.ph741.prol.loopexit
	movq	%r14, %rbx
	cmpq	$3, %r9
	jb	.LBB2_99
	.p2align	4, 0x90
.LBB2_91:                               # %scalar.ph741
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rsi,4)
	movss	-4(%rdx,%rsi,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r15,%rsi,4)
	movl	$0, 4(%rax,%rsi,4)
	movss	(%rdx,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15,%rsi,4)
	movl	$0, 8(%rax,%rsi,4)
	movss	4(%rdx,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 8(%r15,%rsi,4)
	movl	$0, 12(%rax,%rsi,4)
	movss	8(%rdx,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 12(%r15,%rsi,4)
	addq	$4, %rsi
	cmpq	%rsi, %rdi
	jne	.LBB2_91
	jmp	.LBB2_99
.LBB2_84:
	movb	$1, 27(%rsp)            # 1-byte Folded Spill
.LBB2_98:
	movq	%r14, %rbx
.LBB2_99:                               # %._crit_edge624
	movabsq	$-4294967296, %r8       # imm = 0xFFFFFFFF00000000
	xorpd	%xmm10, %xmm10
	testl	%r13d, %r13d
	xorpd	%xmm0, %xmm0
	je	.LBB2_101
# BB#100:
	movq	%r13, %rcx
	shlq	$32, %rcx
	addq	%r8, %rcx
	sarq	$30, %rcx
	movss	(%rdx,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB2_101:
	movq	R__align.lastverticalw(%rip), %rbp
	movss	%xmm0, (%rbp)
	movl	outgap(%rip), %eax
	cmpl	$1, %eax
	movl	%r11d, %ecx
	sbbl	$-1, %ecx
	cmpl	$2, %ecx
	jl	.LBB2_102
# BB#103:                               # %.lr.ph615
	movl	%eax, 188(%rsp)         # 4-byte Spill
	testl	%r13d, %r13d
	sete	%r9b
	movq	R__align.initverticalw(%rip), %rdi
	movq	%rdi, 216(%rsp)         # 8-byte Spill
	movq	R__align.cpmx1(%rip), %rdi
	movq	R__align.floatwork(%rip), %rsi
	movq	%rsi, 328(%rsp)         # 8-byte Spill
	movq	R__align.intwork(%rip), %rsi
	movq	%rsi, 320(%rsp)         # 8-byte Spill
	testq	%r12, %r12
	sete	%al
	orb	%r9b, %al
	movb	%al, 59(%rsp)           # 1-byte Spill
	movss	.LCPI2_4(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm4
	movq	R__align.ijp(%rip), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	R__align.mp(%rip), %rsi
	movq	%r13, %rax
	shlq	$32, %rax
	addq	%r8, %rax
	sarq	$32, %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	cvtss2sd	%xmm11, %xmm11
	leal	1(%r13), %r8d
	movl	%ecx, %ecx
	movq	%rcx, 304(%rsp)         # 8-byte Spill
	leal	-1(%r13), %ecx
	incq	%rcx
	movabsq	$8589934584, %rax       # imm = 0x1FFFFFFF8
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	andq	%rcx, %rax
	leaq	-8(%rax), %r9
	shrq	$3, %r9
	movsd	.LCPI2_2(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm11, %xmm0
	unpcklpd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movapd	%xmm0, 144(%rsp)        # 16-byte Spill
	movapd	%xmm1, %xmm13
	movapd	%xmm1, %xmm0
	unpcklpd	%xmm11, %xmm0   # xmm0 = xmm0[0],xmm11[0]
	movapd	%xmm0, 240(%rsp)        # 16-byte Spill
	movl	%r13d, %ecx
	movq	%rax, 120(%rsp)         # 8-byte Spill
	subl	%eax, %ecx
	movl	%ecx, 192(%rsp)         # 4-byte Spill
	movq	%r9, 272(%rsp)          # 8-byte Spill
                                        # kill: %R9D<def> %R9D<kill> %R9<kill> %R9<def>
	andl	$1, %r9d
	movq	%r9, 264(%rsp)          # 8-byte Spill
	addq	$-2, %r8
	movq	%r8, 168(%rsp)          # 8-byte Spill
	xorpd	%xmm10, %xmm10
	movsd	.LCPI2_3(%rip), %xmm14  # xmm14 = mem[0],zero
	movq	R__align.ogcp1g(%rip), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	R__align.ogcp2g(%rip), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	R__align.fgcp1g(%rip), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movq	R__align.fgcp2g(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	R__align.gapz1(%rip), %r11
	movq	R__align.digf2(%rip), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	R__align.diaf2(%rip), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	movq	R__align.gapz2(%rip), %r12
	movq	R__align.digf1(%rip), %rax
	movq	%rax, 352(%rsp)         # 8-byte Spill
	movq	R__align.diaf1(%rip), %rax
	movq	%rax, 344(%rsp)         # 8-byte Spill
	movq	impmtx(%rip), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movl	$1, %eax
	movq	%rbp, 208(%rsp)         # 8-byte Spill
	movss	%xmm4, 196(%rsp)        # 4-byte Spill
	movq	%rsi, 288(%rsp)         # 8-byte Spill
	movapd	%xmm11, 368(%rsp)       # 16-byte Spill
	jmp	.LBB2_104
.LBB2_119:                              # %vector.body774.preheader
                                        #   in Loop: Header=BB2_104 Depth=1
	cmpq	$0, 264(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_120
# BB#121:                               # %vector.body774.prol
                                        #   in Loop: Header=BB2_104 Depth=1
	movups	(%rsi), %xmm0
	movups	16(%rsi), %xmm1
	movups	(%r14), %xmm2
	movups	16(%r14), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, (%r14)
	movups	%xmm3, 16(%r14)
	movl	$8, %edx
	cmpq	$0, 272(%rsp)           # 8-byte Folded Reload
	jne	.LBB2_123
	jmp	.LBB2_125
.LBB2_120:                              #   in Loop: Header=BB2_104 Depth=1
	xorl	%edx, %edx
	cmpq	$0, 272(%rsp)           # 8-byte Folded Reload
	je	.LBB2_125
.LBB2_123:                              # %vector.body774.preheader.new
                                        #   in Loop: Header=BB2_104 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	subq	%rdx, %rcx
	leaq	48(%r14,%rdx,4), %rbp
	leaq	48(%rsi,%rdx,4), %rbx
	.p2align	4, 0x90
.LBB2_124:                              # %vector.body774
                                        #   Parent Loop BB2_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	-48(%rbp), %xmm2
	movups	-32(%rbp), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -48(%rbp)
	movups	%xmm3, -32(%rbp)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	-16(%rbp), %xmm2
	movups	(%rbp), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbp)
	movups	%xmm3, (%rbp)
	addq	$64, %rbp
	addq	$64, %rbx
	addq	$-16, %rcx
	jne	.LBB2_124
.LBB2_125:                              # %middle.block775
                                        #   in Loop: Header=BB2_104 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, 200(%rsp)         # 8-byte Folded Reload
	je	.LBB2_132
# BB#126:                               #   in Loop: Header=BB2_104 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	leaq	(%rsi,%rcx,4), %rsi
	leaq	(%r14,%rcx,4), %rbp
	movl	192(%rsp), %ecx         # 4-byte Reload
	movl	%ecx, %ebx
	jmp	.LBB2_127
	.p2align	4, 0x90
.LBB2_104:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_105 Depth 2
                                        #       Child Loop BB2_106 Depth 3
                                        #     Child Loop BB2_109 Depth 2
                                        #       Child Loop BB2_111 Depth 3
                                        #     Child Loop BB2_124 Depth 2
                                        #     Child Loop BB2_129 Depth 2
                                        #     Child Loop BB2_131 Depth 2
                                        #     Child Loop BB2_135 Depth 2
	movq	%rbx, %r14
	leaq	-1(%rax), %rcx
	movq	%rcx, 336(%rsp)         # 8-byte Spill
	movq	216(%rsp), %rcx         # 8-byte Reload
	movl	-4(%rcx,%rax,4), %ecx
	movq	%rdx, %r10
	movl	%ecx, (%rdx)
	movl	$n_dis, %ebp
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_105:                              #   Parent Loop BB2_104 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_106 Depth 3
	movl	$0, 384(%rsp,%rsi,4)
	xorpd	%xmm0, %xmm0
	movl	$1, %edx
	movq	%rbp, %rcx
	.p2align	4, 0x90
.LBB2_106:                              #   Parent Loop BB2_104 Depth=1
                                        #     Parent Loop BB2_105 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rcx), %xmm1
	movq	-8(%rdi,%rdx,8), %rbx
	mulss	(%rbx,%rax,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rcx), %xmm0
	movq	(%rdi,%rdx,8), %rbx
	mulss	(%rbx,%rax,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rcx
	addq	$2, %rdx
	cmpq	$27, %rdx
	jne	.LBB2_106
# BB#107:                               #   in Loop: Header=BB2_105 Depth=2
	movss	%xmm0, 384(%rsp,%rsi,4)
	incq	%rsi
	addq	$4, %rbp
	cmpq	$26, %rsi
	jne	.LBB2_105
# BB#108:                               # %.preheader.i539
                                        #   in Loop: Header=BB2_104 Depth=1
	testl	%r13d, %r13d
	movl	%r13d, %r8d
	movq	320(%rsp), %rbp         # 8-byte Reload
	movq	328(%rsp), %r9          # 8-byte Reload
	movq	%r14, %rcx
	je	.LBB2_113
	.p2align	4, 0x90
.LBB2_109:                              # %.lr.ph84.i
                                        #   Parent Loop BB2_104 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_111 Depth 3
	decl	%r8d
	movl	$0, (%rcx)
	movq	(%rbp), %rdx
	movl	(%rdx), %ebx
	testl	%ebx, %ebx
	js	.LBB2_112
# BB#110:                               # %.lr.ph.preheader.i543
                                        #   in Loop: Header=BB2_109 Depth=2
	movq	(%r9), %rsi
	addq	$4, %rdx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB2_111:                              # %.lr.ph.i544
                                        #   Parent Loop BB2_104 Depth=1
                                        #     Parent Loop BB2_109 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebx, %rbx
	movss	384(%rsp,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rsi), %xmm1
	addq	$4, %rsi
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rcx)
	movl	(%rdx), %ebx
	addq	$4, %rdx
	testl	%ebx, %ebx
	jns	.LBB2_111
.LBB2_112:                              # %._crit_edge.i
                                        #   in Loop: Header=BB2_109 Depth=2
	addq	$8, %rbp
	addq	$8, %r9
	addq	$4, %rcx
	testl	%r8d, %r8d
	jne	.LBB2_109
.LBB2_113:                              # %match_calc.exit
                                        #   in Loop: Header=BB2_104 Depth=1
	cmpb	$0, 59(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_132
# BB#114:                               # %.lr.ph.preheader.i545
                                        #   in Loop: Header=BB2_104 Depth=1
	movq	200(%rsp), %rdx         # 8-byte Reload
	cmpq	$8, %rdx
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rsi
	jb	.LBB2_115
# BB#116:                               # %min.iters.checked778
                                        #   in Loop: Header=BB2_104 Depth=1
	cmpq	$0, 120(%rsp)           # 8-byte Folded Reload
	je	.LBB2_115
# BB#117:                               # %vector.memcheck792
                                        #   in Loop: Header=BB2_104 Depth=1
	leaq	(%rsi,%rdx,4), %rcx
	cmpq	%rcx, %r14
	jae	.LBB2_119
# BB#118:                               # %vector.memcheck792
                                        #   in Loop: Header=BB2_104 Depth=1
	leaq	(%r14,%rdx,4), %rcx
	cmpq	%rcx, %rsi
	jae	.LBB2_119
	.p2align	4, 0x90
.LBB2_115:                              #   in Loop: Header=BB2_104 Depth=1
	movl	%r13d, %ebx
	movq	%r14, %rbp
.LBB2_127:                              # %.lr.ph.i549.preheader
                                        #   in Loop: Header=BB2_104 Depth=1
	leal	-1(%rbx), %ecx
	movl	%ebx, %edx
	andl	$3, %edx
	je	.LBB2_130
# BB#128:                               # %.lr.ph.i549.prol.preheader
                                        #   in Loop: Header=BB2_104 Depth=1
	negl	%edx
	.p2align	4, 0x90
.LBB2_129:                              # %.lr.ph.i549.prol
                                        #   Parent Loop BB2_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ebx
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rsi
	addss	(%rbp), %xmm0
	movss	%xmm0, (%rbp)
	leaq	4(%rbp), %rbp
	incl	%edx
	jne	.LBB2_129
.LBB2_130:                              # %.lr.ph.i549.prol.loopexit
                                        #   in Loop: Header=BB2_104 Depth=1
	cmpl	$3, %ecx
	jb	.LBB2_132
	.p2align	4, 0x90
.LBB2_131:                              # %.lr.ph.i549
                                        #   Parent Loop BB2_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rbp), %xmm0
	movss	%xmm0, (%rbp)
	movss	4(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rbp), %xmm0
	movss	%xmm0, 4(%rbp)
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rbp), %xmm0
	movss	%xmm0, 8(%rbp)
	addl	$-4, %ebx
	movss	12(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rsi), %rsi
	addss	12(%rbp), %xmm0
	movss	%xmm0, 12(%rbp)
	leaq	16(%rbp), %rbp
	jne	.LBB2_131
.LBB2_132:                              # %imp_match_out_veadR.exit551
                                        #   in Loop: Header=BB2_104 Depth=1
	movq	216(%rsp), %rcx         # 8-byte Reload
	movl	(%rcx,%rax,4), %ecx
	movl	%ecx, (%r14)
	movq	%r10, %rbx
	movss	(%rbx), %xmm15          # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	addss	%xmm15, %xmm1
	movss	%xmm1, R__align.mi(%rip)
	cmpb	$0, 27(%rsp)            # 1-byte Folded Reload
	je	.LBB2_134
# BB#133:                               # %imp_match_out_veadR.exit551.._crit_edge609_crit_edge
                                        #   in Loop: Header=BB2_104 Depth=1
	leaq	1(%rax), %rdx
	xorl	%r10d, %r10d
	movq	208(%rsp), %rbp         # 8-byte Reload
	jmp	.LBB2_150
	.p2align	4, 0x90
.LBB2_134:                              # %.lr.ph608
                                        #   in Loop: Header=BB2_104 Depth=1
	movq	296(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %r9
	leaq	1(%rax), %rdx
	xorl	%r8d, %r8d
	movl	$-1, %r13d
	xorl	%r10d, %r10d
	movq	208(%rsp), %rbp         # 8-byte Reload
	movq	288(%rsp), %rsi         # 8-byte Reload
	jmp	.LBB2_135
	.p2align	4, 0x90
.LBB2_353:                              # %._crit_edge714
                                        #   in Loop: Header=BB2_135 Depth=2
	movaps	%xmm15, %xmm1
	movss	4(%rbx,%r8,4), %xmm15   # xmm15 = mem[0],zero,zero,zero
	incq	%r8
	decl	%r13d
.LBB2_135:                              #   Parent Loop BB2_104 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	160(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rax,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	cvtss2sd	%xmm0, %xmm4
	movq	224(%rsp), %rcx         # 8-byte Reload
	movss	4(%rcx,%r8,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	cvtss2sd	%xmm0, %xmm0
	movapd	%xmm14, %xmm7
	subsd	%xmm0, %xmm7
	movapd	%xmm4, %xmm2
	mulsd	%xmm7, %xmm2
	mulsd	%xmm11, %xmm2
	movapd	%xmm13, %xmm3
	mulsd	%xmm3, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	addss	%xmm15, %xmm2
	movapd	%xmm14, %xmm5
	movsd	%xmm4, 40(%rsp)         # 8-byte Spill
	subsd	%xmm4, %xmm5
	movapd	%xmm5, %xmm4
	mulsd	%xmm0, %xmm4
	mulsd	%xmm11, %xmm4
	mulsd	%xmm3, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	movq	112(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 176(%rsp)        # 4-byte Spill
	cvtss2sd	%xmm2, %xmm13
	movq	136(%rsp), %rcx         # 8-byte Reload
	movss	4(%rcx,%r8,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 60(%rsp)         # 4-byte Spill
	cvtss2sd	%xmm2, %xmm9
	movapd	%xmm14, %xmm10
	subsd	%xmm9, %xmm10
	movapd	%xmm13, %xmm2
	mulsd	%xmm10, %xmm2
	mulsd	%xmm11, %xmm2
	movapd	%xmm14, %xmm8
	movapd	%xmm3, %xmm14
	mulsd	%xmm3, %xmm2
	cvtsd2ss	%xmm2, %xmm11
	addss	%xmm4, %xmm11
	movl	$0, 4(%r9,%r8,4)
	movss	4(%r11,%rax,4), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	cvtss2sd	%xmm2, %xmm4
	movapd	%xmm8, %xmm2
	subsd	%xmm4, %xmm2
	addsd	%xmm0, %xmm10
	mulsd	%xmm2, %xmm10
	movq	128(%rsp), %rcx         # 8-byte Reload
	movss	4(%rcx,%r8,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movapd	%xmm8, %xmm12
	subsd	%xmm0, %xmm12
	movq	360(%rsp), %rcx         # 8-byte Reload
	movss	4(%rcx,%r8,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	subsd	%xmm0, %xmm12
	movapd	%xmm8, %xmm3
	movapd	%xmm8, %xmm0
	movapd	%xmm13, %xmm8
	subsd	%xmm13, %xmm0
	mulsd	%xmm12, %xmm4
	addsd	%xmm10, %xmm4
	movapd	%xmm0, %xmm2
	mulsd	%xmm9, %xmm2
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	movaps	%xmm1, %xmm2
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	mulpd	240(%rsp), %xmm4        # 16-byte Folded Reload
	mulpd	144(%rsp), %xmm4        # 16-byte Folded Reload
	cvtpd2ps	%xmm4, %xmm4
	addps	%xmm4, %xmm2
	movaps	%xmm2, %xmm10
	shufps	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	ucomiss	%xmm10, %xmm2
	movaps	%xmm1, %xmm6
	jbe	.LBB2_137
# BB#136:                               #   in Loop: Header=BB2_135 Depth=2
	leal	(%r10,%r13), %ecx
	movl	%ecx, 4(%r9,%r8,4)
	movaps	%xmm2, %xmm10
.LBB2_137:                              #   in Loop: Header=BB2_135 Depth=2
	movapd	368(%rsp), %xmm11       # 16-byte Reload
	movapd	%xmm14, %xmm13
	movapd	%xmm3, %xmm14
	movsd	40(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	movss	(%r11,%rax,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm14, %xmm4
	subsd	%xmm2, %xmm4
	addsd	%xmm9, %xmm7
	mulsd	%xmm4, %xmm7
	mulsd	%xmm2, %xmm12
	addsd	%xmm7, %xmm12
	mulsd	%xmm13, %xmm12
	mulsd	%xmm11, %xmm12
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm12, %xmm2
	addss	%xmm2, %xmm15
	ucomiss	%xmm6, %xmm15
	jae	.LBB2_138
# BB#139:                               #   in Loop: Header=BB2_135 Depth=2
	testq	%r8, %r8
	je	.LBB2_140
# BB#141:                               #   in Loop: Header=BB2_135 Depth=2
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	60(%rsp), %xmm1         # 4-byte Folded Reload
	mulss	108(%rsp), %xmm1        # 4-byte Folded Reload
	cvtss2sd	%xmm1, %xmm1
	mulsd	%xmm13, %xmm1
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm6, %xmm2
	addsd	%xmm1, %xmm2
	xorps	%xmm15, %xmm15
	cvtsd2ss	%xmm2, %xmm15
	movss	%xmm15, R__align.mi(%rip)
	jmp	.LBB2_142
	.p2align	4, 0x90
.LBB2_138:                              #   in Loop: Header=BB2_135 Depth=2
	movss	%xmm15, R__align.mi(%rip)
	movl	%r8d, %r10d
	jmp	.LBB2_142
.LBB2_140:                              #   in Loop: Header=BB2_135 Depth=2
	movaps	%xmm6, %xmm15
	.p2align	4, 0x90
.LBB2_142:                              #   in Loop: Header=BB2_135 Depth=2
	movss	8(%r12,%r8,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	cvtss2sd	%xmm1, %xmm2
	movapd	%xmm14, %xmm1
	subsd	%xmm2, %xmm1
	addsd	%xmm3, %xmm0
	mulsd	%xmm1, %xmm0
	movq	352(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rax,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	cvtss2sd	%xmm1, %xmm3
	movapd	%xmm14, %xmm1
	subsd	%xmm3, %xmm1
	movq	344(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx,%rax,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm3, %xmm1
	mulsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	mulsd	%xmm13, %xmm2
	mulsd	%xmm11, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movss	4(%r15,%r8,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm2
	ucomiss	%xmm10, %xmm2
	jbe	.LBB2_144
# BB#143:                               #   in Loop: Header=BB2_135 Depth=2
	movl	%eax, %ecx
	subl	4(%rsi,%r8,4), %ecx
	movl	%ecx, 4(%r9,%r8,4)
	movaps	%xmm2, %xmm10
.LBB2_144:                              #   in Loop: Header=BB2_135 Depth=2
	movss	4(%r12,%r8,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	movapd	%xmm14, %xmm3
	subsd	%xmm2, %xmm3
	addsd	%xmm8, %xmm5
	mulsd	%xmm3, %xmm5
	mulsd	%xmm2, %xmm1
	addsd	%xmm5, %xmm1
	mulsd	%xmm13, %xmm1
	mulsd	%xmm11, %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addss	(%rbx,%r8,4), %xmm1
	ucomiss	%xmm0, %xmm1
	jae	.LBB2_145
# BB#146:                               #   in Loop: Header=BB2_135 Depth=2
	cmpq	$1, %rax
	je	.LBB2_148
# BB#147:                               #   in Loop: Header=BB2_135 Depth=2
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	176(%rsp), %xmm0        # 4-byte Folded Reload
	mulss	108(%rsp), %xmm0        # 4-byte Folded Reload
	cvtss2sd	%xmm0, %xmm0
	mulsd	%xmm13, %xmm0
	movss	4(%r15,%r8,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r15,%r8,4)
	jmp	.LBB2_148
	.p2align	4, 0x90
.LBB2_145:                              #   in Loop: Header=BB2_135 Depth=2
	movss	%xmm1, 4(%r15,%r8,4)
	movq	336(%rsp), %rcx         # 8-byte Reload
	movl	%ecx, 4(%rsi,%r8,4)
.LBB2_148:                              #   in Loop: Header=BB2_135 Depth=2
	movss	4(%r14,%r8,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	%xmm10, %xmm0
	movss	%xmm0, 4(%r14,%r8,4)
	cmpq	%r8, 168(%rsp)          # 8-byte Folded Reload
	jne	.LBB2_353
# BB#149:                               #   in Loop: Header=BB2_104 Depth=1
	movq	88(%rsp), %r13          # 8-byte Reload
	movss	196(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB2_150:                              # %._crit_edge609
                                        #   in Loop: Header=BB2_104 Depth=1
	movq	312(%rsp), %rcx         # 8-byte Reload
	movl	(%r14,%rcx,4), %ecx
	movl	%ecx, (%rbp,%rax,4)
	cmpq	304(%rsp), %rdx         # 8-byte Folded Reload
	movq	%rdx, %rax
	movq	%r14, %rdx
	jne	.LBB2_104
# BB#151:                               # %._crit_edge616
	movl	%r10d, R__align.mpi(%rip)
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r11          # 8-byte Reload
	movq	552(%rsp), %r12
	movl	188(%rsp), %eax         # 4-byte Reload
	testl	%eax, %eax
	jne	.LBB2_167
	jmp	.LBB2_153
.LBB2_102:
	movq	%rdx, %r14
	movq	64(%rsp), %r8           # 8-byte Reload
	testl	%eax, %eax
	jne	.LBB2_167
.LBB2_153:                              # %.preheader583
	movq	%rbp, %r9
	cmpb	$0, 27(%rsp)            # 1-byte Folded Reload
	jne	.LBB2_160
# BB#154:                               # %.lr.ph597
	movslq	offset(%rip), %rax
	movq	%r13, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB2_155
# BB#156:
	leal	-1(%r13), %edx
	imull	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI2_1(%rip), %xmm0
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r14)
	movl	$2, %ebp
	cmpq	$2, %rcx
	jne	.LBB2_158
	jmp	.LBB2_160
.LBB2_155:
	movl	$1, %ebp
	cmpq	$2, %rcx
	je	.LBB2_160
.LBB2_158:                              # %.lr.ph597.new
	subq	%rbp, %rcx
	movl	%r13d, %edx
	subl	%ebp, %edx
	imull	%eax, %edx
	leaq	4(%r14,%rbp,4), %rsi
	leal	-1(%r13), %edi
	subl	%ebp, %edi
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI2_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_159:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	subl	%eax, %ebp
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB2_159
.LBB2_160:                              # %.preheader582
	testl	%r11d, %r11d
	movq	%r9, %rbp
	jle	.LBB2_167
# BB#161:                               # %.lr.ph595
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r11d, %xmm1
	incq	%r11
	movl	%r11d, %eax
	testb	$1, %r11b
	jne	.LBB2_162
# BB#163:
	movsd	.LCPI2_1(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movss	4(%rbp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%rbp)
	movl	$2, %ecx
	cmpq	$2, %rax
	jne	.LBB2_165
	jmp	.LBB2_167
.LBB2_162:
	movl	$1, %ecx
	cmpq	$2, %rax
	je	.LBB2_167
.LBB2_165:
	movsd	.LCPI2_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB2_166:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%rbp,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rbp,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%rbp,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%rbp,%rcx,4)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB2_166
.LBB2_167:                              # %.loopexit
	movapd	%xmm10, 224(%rsp)       # 16-byte Spill
	movq	R__align.mseq1(%rip), %r15
	movq	R__align.mseq2(%rip), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	movq	R__align.ijp(%rip), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	(%r8), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	leal	(%rax,%rbx), %ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	leal	1(%rax,%rbx), %ebx
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, 112(%rsp)         # 8-byte Spill
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, %r8
	movl	outgap(%rip), %eax
	testq	%r12, %r12
	movq	%r15, 240(%rsp)         # 8-byte Spill
	movq	%r8, 128(%rsp)          # 8-byte Spill
	je	.LBB2_257
# BB#168:
	cmpl	$1, %eax
	movq	96(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	je	.LBB2_169
# BB#180:
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	32(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB2_186
# BB#181:                               # %.lr.ph55.i
	movslq	%esi, %rax
	movslq	%r10d, %rcx
	movl	%eax, %edx
	addq	$4, %rbp
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB2_184
	jmp	.LBB2_183
	.p2align	4, 0x90
.LBB2_354:                              # %._crit_edge97.i
                                        #   in Loop: Header=BB2_184 Depth=1
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB2_184
.LBB2_183:
	movq	(%r11,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB2_184:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB2_354
# BB#185:
	movaps	%xmm1, %xmm0
.LBB2_186:                              # %.preheader9.i
	testl	%r10d, %r10d
	jle	.LBB2_169
# BB#187:                               # %.lr.ph51.i
	movslq	32(%rsp), %rax          # 4-byte Folded Reload
	movslq	%r10d, %rcx
	movl	%ecx, %edx
	testb	$1, %r10b
	jne	.LBB2_189
# BB#188:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB2_193
	jmp	.LBB2_169
.LBB2_257:
	cmpl	$1, %eax
	movq	96(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	je	.LBB2_258
# BB#269:
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	32(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB2_275
# BB#270:                               # %.lr.ph54.i
	movslq	%esi, %rax
	movslq	%r10d, %rcx
	movl	%eax, %edx
	addq	$4, %rbp
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB2_273
	jmp	.LBB2_272
	.p2align	4, 0x90
.LBB2_355:                              # %._crit_edge96.i
                                        #   in Loop: Header=BB2_273 Depth=1
	movss	(%rbp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbp
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB2_273
.LBB2_272:
	movq	(%r11,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB2_273:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB2_355
# BB#274:
	movaps	%xmm1, %xmm0
.LBB2_275:                              # %.preheader8.i
	testl	%r10d, %r10d
	jle	.LBB2_258
# BB#276:                               # %.lr.ph50.i
	movslq	32(%rsp), %rax          # 4-byte Folded Reload
	movslq	%r10d, %rcx
	movl	%ecx, %edx
	testb	$1, %r10b
	jne	.LBB2_278
# BB#277:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB2_282
	jmp	.LBB2_258
.LBB2_189:
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB2_191
# BB#190:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB2_193
	jmp	.LBB2_169
.LBB2_95:                               # %vector.ph759
	leaq	1(%rbx), %rsi
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	xorl	%ecx, %ecx
	xorps	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB2_96:                               # %vector.body739
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm2, 4(%rax,%rcx,4)
	movups	%xmm2, 20(%rax,%rcx,4)
	movups	(%rdx,%rcx,4), %xmm3
	movups	16(%rdx,%rcx,4), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm1, %xmm4
	movups	%xmm3, 4(%r15,%rcx,4)
	movups	%xmm4, 20(%r15,%rcx,4)
	addq	$8, %rcx
	cmpq	%rcx, %rbx
	jne	.LBB2_96
# BB#97:                                # %middle.block740
	testq	%r8, %r8
	jne	.LBB2_87
	jmp	.LBB2_98
.LBB2_278:
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB2_280
# BB#279:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB2_282
	jmp	.LBB2_258
.LBB2_191:
	movl	%r10d, %esi
	negl	%esi
	movq	(%r11,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB2_169
.LBB2_193:                              # %.lr.ph51.i.new
	movl	%r10d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB2_194:                              # =>This Inner Loop Header: Depth=1
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_196
# BB#195:                               #   in Loop: Header=BB2_194 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r11,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB2_196:                              #   in Loop: Header=BB2_194 Depth=1
	movss	4(%r14,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_198
# BB#197:                               #   in Loop: Header=BB2_194 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r11,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB2_198:                              #   in Loop: Header=BB2_194 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB2_194
.LBB2_169:                              # %.preheader8.i552
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	js	.LBB2_175
# BB#170:                               # %.lr.ph48.preheader.i
	movq	32(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB2_172
	.p2align	4, 0x90
.LBB2_171:                              # %.lr.ph48.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB2_171
.LBB2_172:                              # %.lr.ph48.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB2_175
# BB#173:                               # %.lr.ph48.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r11,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_174:                              # %.lr.ph48.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB2_174
.LBB2_175:                              # %.preheader7.i559
	testl	%r10d, %r10d
	js	.LBB2_203
# BB#176:                               # %.lr.ph45.i
	movq	(%r11), %rcx
	movq	%r10, %rsi
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB2_177
# BB#199:                               # %min.iters.checked915
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB2_177
# BB#200:                               # %vector.body911.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI2_5(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_6(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI2_7(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_201:                              # %vector.body911
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB2_201
# BB#202:                               # %middle.block912
	testq	%rsi, %rsi
	jne	.LBB2_178
	jmp	.LBB2_203
.LBB2_177:
	xorl	%ebp, %ebp
.LBB2_178:                              # %scalar.ph913.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB2_179:                              # %scalar.ph913
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB2_179
.LBB2_203:                              # %._crit_edge46.i
	movq	32(%rsp), %rsi          # 8-byte Reload
	movslq	%esi, %rax
	movq	112(%rsp), %r13         # 8-byte Reload
	addq	%rax, %r13
	movslq	%r10d, %rcx
	movb	$0, (%rcx,%r13)
	addq	%rcx, %r13
	addq	%rax, %r8
	leaq	(%r8,%rcx), %r14
	movb	$0, (%rcx,%r8)
	movq	560(%rsp), %rax
	movl	$0, (%rax)
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	js	.LBB2_250
# BB#204:                               # %.lr.ph37.preheader.i
	xorl	%ecx, %ecx
	movq	impmtx(%rip), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movdqa	.LCPI2_9(%rip), %xmm0   # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI2_8(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	movl	%esi, %r15d
	movl	%r10d, %r9d
	.p2align	4, 0x90
.LBB2_205:                              # %.lr.ph37.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_220 Depth 2
                                        #     Child Loop BB2_224 Depth 2
                                        #     Child Loop BB2_227 Depth 2
                                        #     Child Loop BB2_235 Depth 2
                                        #     Child Loop BB2_239 Depth 2
                                        #     Child Loop BB2_242 Depth 2
	movslq	%r15d, %rax
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movq	(%r11,%rax,8), %rax
	movslq	%r9d, %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movl	(%rax,%rdx,4), %r12d
	testl	%r12d, %r12d
	js	.LBB2_206
# BB#207:                               #   in Loop: Header=BB2_205 Depth=1
	je	.LBB2_209
# BB#208:                               #   in Loop: Header=BB2_205 Depth=1
	movl	%r15d, %eax
	subl	%r12d, %eax
	jmp	.LBB2_210
	.p2align	4, 0x90
.LBB2_206:                              #   in Loop: Header=BB2_205 Depth=1
	leal	-1(%r15), %eax
	jmp	.LBB2_211
	.p2align	4, 0x90
.LBB2_209:                              #   in Loop: Header=BB2_205 Depth=1
	leal	-1(%r15), %eax
.LBB2_210:                              #   in Loop: Header=BB2_205 Depth=1
	movl	$-1, %r12d
.LBB2_211:                              #   in Loop: Header=BB2_205 Depth=1
	movl	%r15d, %edi
	subl	%eax, %edi
	decl	%edi
	je	.LBB2_229
# BB#212:                               # %.lr.ph18.preheader.i
                                        #   in Loop: Header=BB2_205 Depth=1
	leal	-2(%r15), %ebp
	subl	%eax, %ebp
	movq	%rbp, %r8
	negq	%r8
	leaq	1(%rbp), %rsi
	cmpq	$16, %rsi
	jae	.LBB2_214
# BB#213:                               #   in Loop: Header=BB2_205 Depth=1
	movq	%r14, %rbp
	movq	%r13, %rbx
	jmp	.LBB2_222
	.p2align	4, 0x90
.LBB2_214:                              # %min.iters.checked974
                                        #   in Loop: Header=BB2_205 Depth=1
	movl	%esi, %r10d
	andl	$15, %r10d
	subq	%r10, %rsi
	je	.LBB2_215
# BB#216:                               # %vector.memcheck987
                                        #   in Loop: Header=BB2_205 Depth=1
	movl	%eax, 144(%rsp)         # 4-byte Spill
	leaq	-1(%r13,%r8), %rax
	cmpq	%r14, %rax
	jae	.LBB2_219
# BB#217:                               # %vector.memcheck987
                                        #   in Loop: Header=BB2_205 Depth=1
	leaq	-1(%r14,%r8), %rax
	cmpq	%r13, %rax
	jae	.LBB2_219
# BB#218:                               #   in Loop: Header=BB2_205 Depth=1
	movq	%r14, %rbp
	movq	%r13, %rbx
	movq	40(%rsp), %r10          # 8-byte Reload
	movl	144(%rsp), %eax         # 4-byte Reload
	jmp	.LBB2_222
.LBB2_215:                              #   in Loop: Header=BB2_205 Depth=1
	movq	%r14, %rbp
	movq	%r13, %rbx
	movq	40(%rsp), %r10          # 8-byte Reload
	jmp	.LBB2_222
.LBB2_219:                              # %vector.body970.preheader
                                        #   in Loop: Header=BB2_205 Depth=1
	subl	%esi, %edi
	leaq	-1(%r10), %rbx
	subq	%rbp, %rbx
	leaq	(%r14,%rbx), %rbp
	addq	%r13, %rbx
	leaq	-8(%r13), %rdx
	leaq	-8(%r14), %rax
	.p2align	4, 0x90
.LBB2_220:                              # %vector.body970
                                        #   Parent Loop BB2_205 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rdx)
	movq	%xmm1, -8(%rdx)
	movq	%xmm0, (%rax)
	movq	%xmm0, -8(%rax)
	addq	$-16, %rdx
	addq	$-16, %rax
	addq	$-16, %rsi
	jne	.LBB2_220
# BB#221:                               # %middle.block971
                                        #   in Loop: Header=BB2_205 Depth=1
	testq	%r10, %r10
	movq	40(%rsp), %r10          # 8-byte Reload
	movl	144(%rsp), %eax         # 4-byte Reload
	je	.LBB2_228
	.p2align	4, 0x90
.LBB2_222:                              # %.lr.ph18.i.preheader
                                        #   in Loop: Header=BB2_205 Depth=1
	leal	-1(%rdi), %edx
	movl	%edi, %esi
	andl	$7, %esi
	je	.LBB2_225
# BB#223:                               # %.lr.ph18.i.prol.preheader
                                        #   in Loop: Header=BB2_205 Depth=1
	negl	%esi
	.p2align	4, 0x90
.LBB2_224:                              # %.lr.ph18.i.prol
                                        #   Parent Loop BB2_205 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rbx)
	decq	%rbx
	movb	$45, -1(%rbp)
	decq	%rbp
	decl	%edi
	incl	%esi
	jne	.LBB2_224
.LBB2_225:                              # %.lr.ph18.i.prol.loopexit
                                        #   in Loop: Header=BB2_205 Depth=1
	cmpl	$7, %edx
	jb	.LBB2_228
# BB#226:                               # %.lr.ph18.i.preheader.new
                                        #   in Loop: Header=BB2_205 Depth=1
	decq	%rbp
	decq	%rbx
	.p2align	4, 0x90
.LBB2_227:                              # %.lr.ph18.i
                                        #   Parent Loop BB2_205 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rbx)
	movb	$45, (%rbp)
	movb	$111, -1(%rbx)
	movb	$45, -1(%rbp)
	movb	$111, -2(%rbx)
	movb	$45, -2(%rbp)
	movb	$111, -3(%rbx)
	movb	$45, -3(%rbp)
	movb	$111, -4(%rbx)
	movb	$45, -4(%rbp)
	movb	$111, -5(%rbx)
	movb	$45, -5(%rbp)
	movb	$111, -6(%rbx)
	movb	$45, -6(%rbp)
	movb	$111, -7(%rbx)
	movb	$45, -7(%rbp)
	addq	$-8, %rbp
	addq	$-8, %rbx
	addl	$-8, %edi
	jne	.LBB2_227
.LBB2_228:                              # %._crit_edge19.loopexit.i
                                        #   in Loop: Header=BB2_205 Depth=1
	leaq	-1(%r13,%r8), %r13
	leaq	-1(%r14,%r8), %r14
	leal	-1(%rcx,%r15), %ecx
	subl	%eax, %ecx
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB2_229:                              # %._crit_edge19.i
                                        #   in Loop: Header=BB2_205 Depth=1
	cmpl	$-1, %r12d
	je	.LBB2_244
# BB#230:                               # %.lr.ph26.preheader.i
                                        #   in Loop: Header=BB2_205 Depth=1
	movl	%r12d, %edi
	notl	%edi
	movl	$-2, %esi
	subl	%r12d, %esi
	movq	%rsi, %rdx
	negq	%rdx
	leaq	1(%rsi), %r10
	cmpq	$16, %r10
	movl	%edi, %r8d
	movq	%r14, %rbp
	movq	%r13, %rbx
	jb	.LBB2_237
# BB#231:                               # %min.iters.checked936
                                        #   in Loop: Header=BB2_205 Depth=1
	movl	%r10d, %r11d
	andl	$15, %r11d
	subq	%r11, %r10
	movl	%edi, %r8d
	movq	%r14, %rbp
	movq	%r13, %rbx
	je	.LBB2_237
# BB#232:                               # %vector.memcheck949
                                        #   in Loop: Header=BB2_205 Depth=1
	movl	%eax, 144(%rsp)         # 4-byte Spill
	leaq	-1(%r13,%rdx), %rax
	cmpq	%r14, %rax
	jae	.LBB2_234
# BB#233:                               # %vector.memcheck949
                                        #   in Loop: Header=BB2_205 Depth=1
	leaq	-1(%r14,%rdx), %rax
	cmpq	%r13, %rax
	movl	144(%rsp), %eax         # 4-byte Reload
	movl	%edi, %r8d
	movq	%r14, %rbp
	movq	%r13, %rbx
	jb	.LBB2_237
.LBB2_234:                              # %vector.body932.preheader
                                        #   in Loop: Header=BB2_205 Depth=1
	movl	%edi, %r8d
	subl	%r10d, %r8d
	leaq	-1(%r11), %rbx
	subq	%rsi, %rbx
	leaq	(%r14,%rbx), %rbp
	addq	%r13, %rbx
	leaq	-8(%r13), %rsi
	leaq	-8(%r14), %rax
	.p2align	4, 0x90
.LBB2_235:                              # %vector.body932
                                        #   Parent Loop BB2_205 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rsi)
	movq	%xmm0, -8(%rsi)
	movq	%xmm1, (%rax)
	movq	%xmm1, -8(%rax)
	addq	$-16, %rsi
	addq	$-16, %rax
	addq	$-16, %r10
	jne	.LBB2_235
# BB#236:                               # %middle.block933
                                        #   in Loop: Header=BB2_205 Depth=1
	testq	%r11, %r11
	movl	144(%rsp), %eax         # 4-byte Reload
	je	.LBB2_243
	.p2align	4, 0x90
.LBB2_237:                              # %.lr.ph26.i.preheader
                                        #   in Loop: Header=BB2_205 Depth=1
	leal	-1(%r8), %r10d
	movl	%r8d, %esi
	andl	$7, %esi
	je	.LBB2_240
# BB#238:                               # %.lr.ph26.i.prol.preheader
                                        #   in Loop: Header=BB2_205 Depth=1
	negl	%esi
	.p2align	4, 0x90
.LBB2_239:                              # %.lr.ph26.i.prol
                                        #   Parent Loop BB2_205 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rbx)
	decq	%rbx
	movb	$111, -1(%rbp)
	decq	%rbp
	decl	%r8d
	incl	%esi
	jne	.LBB2_239
.LBB2_240:                              # %.lr.ph26.i.prol.loopexit
                                        #   in Loop: Header=BB2_205 Depth=1
	cmpl	$7, %r10d
	jb	.LBB2_243
# BB#241:                               # %.lr.ph26.i.preheader.new
                                        #   in Loop: Header=BB2_205 Depth=1
	decq	%rbp
	decq	%rbx
	.p2align	4, 0x90
.LBB2_242:                              # %.lr.ph26.i
                                        #   Parent Loop BB2_205 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rbx)
	movb	$111, (%rbp)
	movb	$45, -1(%rbx)
	movb	$111, -1(%rbp)
	movb	$45, -2(%rbx)
	movb	$111, -2(%rbp)
	movb	$45, -3(%rbx)
	movb	$111, -3(%rbp)
	movb	$45, -4(%rbx)
	movb	$111, -4(%rbp)
	movb	$45, -5(%rbx)
	movb	$111, -5(%rbp)
	movb	$45, -6(%rbx)
	movb	$111, -6(%rbp)
	movb	$45, -7(%rbx)
	movb	$111, -7(%rbp)
	addq	$-8, %rbp
	addq	$-8, %rbx
	addl	$-8, %r8d
	jne	.LBB2_242
.LBB2_243:                              # %._crit_edge27.loopexit.i
                                        #   in Loop: Header=BB2_205 Depth=1
	leaq	-1(%r13,%rdx), %r13
	leaq	-1(%r14,%rdx), %r14
	addl	%edi, %ecx
	movq	96(%rsp), %r11          # 8-byte Reload
	movq	40(%rsp), %r10          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
.LBB2_244:                              # %._crit_edge27.i
                                        #   in Loop: Header=BB2_205 Depth=1
	cmpl	%esi, %r15d
	je	.LBB2_247
# BB#245:                               # %._crit_edge27.i
                                        #   in Loop: Header=BB2_205 Depth=1
	cmpl	%r10d, %r9d
	je	.LBB2_247
# BB#246:                               #   in Loop: Header=BB2_205 Depth=1
	movl	%eax, %edx
	movq	160(%rsp), %rax         # 8-byte Reload
	movq	176(%rsp), %rdi         # 8-byte Reload
	movq	(%rax,%rdi,8), %rax
	movq	168(%rsp), %rdi         # 8-byte Reload
	movss	(%rax,%rdi,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	560(%rsp), %rax
	addss	(%rax), %xmm2
	movss	%xmm2, (%rax)
	movl	%edx, %eax
.LBB2_247:                              #   in Loop: Header=BB2_205 Depth=1
	testl	%r15d, %r15d
	jle	.LBB2_250
# BB#248:                               #   in Loop: Header=BB2_205 Depth=1
	testl	%r9d, %r9d
	jle	.LBB2_250
# BB#249:                               #   in Loop: Header=BB2_205 Depth=1
	addl	%r12d, %r9d
	movb	$111, -1(%r13)
	decq	%r13
	movb	$111, -1(%r14)
	decq	%r14
	addl	$2, %ecx
	cmpl	60(%rsp), %ecx          # 4-byte Folded Reload
	movl	%eax, %r15d
	jle	.LBB2_205
.LBB2_250:                              # %._crit_edge38.i
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	240(%rsp), %rbx         # 8-byte Reload
	jle	.LBB2_253
# BB#251:                               # %.lr.ph13.preheader.i
	movl	28(%rsp), %r15d         # 4-byte Reload
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB2_252:                              # %.lr.ph13.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r13, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r15
	jne	.LBB2_252
.LBB2_253:                              # %.preheader.i573
	movl	52(%rsp), %r15d         # 4-byte Reload
	testl	%r15d, %r15d
	movq	136(%rsp), %rbx         # 8-byte Reload
	jle	.LBB2_256
# BB#254:                               # %.lr.ph.preheader.i575
	movl	%r15d, %r13d
	movq	80(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_255:                              # %.lr.ph.i579
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r14, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r13
	jne	.LBB2_255
.LBB2_256:                              # %Atracking_localhom.exit
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	80(%rsp), %r14          # 8-byte Reload
	jmp	.LBB2_343
.LBB2_280:
	movl	%r10d, %esi
	negl	%esi
	movq	(%r11,%rax,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB2_258
.LBB2_282:                              # %.lr.ph50.i.new
	movl	%r10d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB2_283:                              # =>This Inner Loop Header: Depth=1
	movss	(%r14,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_285
# BB#284:                               #   in Loop: Header=BB2_283 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r11,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB2_285:                              #   in Loop: Header=BB2_283 Depth=1
	movss	4(%r14,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB2_287
# BB#286:                               #   in Loop: Header=BB2_283 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r11,%rax,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB2_287:                              #   in Loop: Header=BB2_283 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB2_283
.LBB2_258:                              # %.preheader7.i
	cmpl	$0, 32(%rsp)            # 4-byte Folded Reload
	js	.LBB2_264
# BB#259:                               # %.lr.ph47.preheader.i
	movq	32(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB2_261
	.p2align	4, 0x90
.LBB2_260:                              # %.lr.ph47.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r11,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB2_260
.LBB2_261:                              # %.lr.ph47.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB2_264
# BB#262:                               # %.lr.ph47.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %rax
	leaq	56(%r11,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_263:                              # %.lr.ph47.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%rax,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB2_263
.LBB2_264:                              # %.preheader6.i
	testl	%r10d, %r10d
	js	.LBB2_292
# BB#265:                               # %.lr.ph44.i
	movq	(%r11), %rcx
	movq	%r10, %rsi
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB2_266
# BB#288:                               # %min.iters.checked821
	andl	$7, %esi
	movq	%rax, %rbp
	subq	%rsi, %rbp
	je	.LBB2_266
# BB#289:                               # %vector.body817.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI2_5(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI2_6(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI2_7(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdx
	.p2align	4, 0x90
.LBB2_290:                              # %vector.body817
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB2_290
# BB#291:                               # %middle.block818
	testq	%rsi, %rsi
	jne	.LBB2_267
	jmp	.LBB2_292
.LBB2_266:
	xorl	%ebp, %ebp
.LBB2_267:                              # %scalar.ph819.preheader
	movl	%ebp, %edx
	notl	%edx
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rax
	.p2align	4, 0x90
.LBB2_268:                              # %scalar.ph819
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rax
	jne	.LBB2_268
.LBB2_292:                              # %._crit_edge45.i
	movq	32(%rsp), %rsi          # 8-byte Reload
	movslq	%esi, %rax
	movq	112(%rsp), %r12         # 8-byte Reload
	addq	%rax, %r12
	movslq	%r10d, %rcx
	movb	$0, (%rcx,%r12)
	addq	%rcx, %r12
	addq	%rax, %r8
	leaq	(%r8,%rcx), %r14
	movb	$0, (%rcx,%r8)
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	js	.LBB2_336
# BB#293:                               # %.lr.ph36.i.preheader
	xorl	%eax, %eax
	movdqa	.LCPI2_9(%rip), %xmm0   # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI2_8(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	.p2align	4, 0x90
.LBB2_294:                              # %.lr.ph36.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_309 Depth 2
                                        #     Child Loop BB2_313 Depth 2
                                        #     Child Loop BB2_316 Depth 2
                                        #     Child Loop BB2_324 Depth 2
                                        #     Child Loop BB2_328 Depth 2
                                        #     Child Loop BB2_331 Depth 2
	movslq	%esi, %rcx
	movq	(%r11,%rcx,8), %rcx
	movslq	%r10d, %rdx
	movl	(%rcx,%rdx,4), %r9d
	testl	%r9d, %r9d
	js	.LBB2_295
# BB#296:                               #   in Loop: Header=BB2_294 Depth=1
	je	.LBB2_298
# BB#297:                               #   in Loop: Header=BB2_294 Depth=1
	movl	%esi, %r8d
	subl	%r9d, %r8d
	jmp	.LBB2_299
	.p2align	4, 0x90
.LBB2_295:                              #   in Loop: Header=BB2_294 Depth=1
	leal	-1(%rsi), %r8d
	jmp	.LBB2_300
	.p2align	4, 0x90
.LBB2_298:                              #   in Loop: Header=BB2_294 Depth=1
	leal	-1(%rsi), %r8d
.LBB2_299:                              #   in Loop: Header=BB2_294 Depth=1
	movl	$-1, %r9d
.LBB2_300:                              #   in Loop: Header=BB2_294 Depth=1
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%rsi, %rcx
	subl	%r8d, %esi
	decl	%esi
	je	.LBB2_301
# BB#302:                               # %.lr.ph17.preheader.i
                                        #   in Loop: Header=BB2_294 Depth=1
	movq	%rcx, %r13
	leal	-2(%rcx), %ecx
	subl	%r8d, %ecx
	movq	%rcx, %r10
	negq	%r10
	leaq	1(%rcx), %rdx
	cmpq	$16, %rdx
	jae	.LBB2_304
# BB#303:                               #   in Loop: Header=BB2_294 Depth=1
	movq	%r14, %rcx
	movq	%r12, %rbp
	jmp	.LBB2_311
	.p2align	4, 0x90
.LBB2_301:                              #   in Loop: Header=BB2_294 Depth=1
	movq	%rcx, %r13
	cmpl	$-1, %r9d
	jne	.LBB2_319
	jmp	.LBB2_333
	.p2align	4, 0x90
.LBB2_304:                              # %min.iters.checked877
                                        #   in Loop: Header=BB2_294 Depth=1
	movl	%edx, %r11d
	andl	$15, %r11d
	subq	%r11, %rdx
	je	.LBB2_305
# BB#306:                               # %vector.memcheck890
                                        #   in Loop: Header=BB2_294 Depth=1
	leaq	-1(%r12,%r10), %rdi
	cmpq	%r14, %rdi
	jae	.LBB2_308
# BB#307:                               # %vector.memcheck890
                                        #   in Loop: Header=BB2_294 Depth=1
	leaq	-1(%r14,%r10), %rdi
	cmpq	%r12, %rdi
	jae	.LBB2_308
.LBB2_305:                              #   in Loop: Header=BB2_294 Depth=1
	movq	%r14, %rcx
	movq	%r12, %rbp
	movq	96(%rsp), %r11          # 8-byte Reload
	jmp	.LBB2_311
.LBB2_308:                              # %vector.body873.preheader
                                        #   in Loop: Header=BB2_294 Depth=1
	subl	%edx, %esi
	leaq	-1(%r11), %rbp
	subq	%rcx, %rbp
	leaq	(%r14,%rbp), %rcx
	addq	%r12, %rbp
	leaq	-8(%r12), %rdi
	leaq	-8(%r14), %rbx
	.p2align	4, 0x90
.LBB2_309:                              # %vector.body873
                                        #   Parent Loop BB2_294 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rdi)
	movq	%xmm1, -8(%rdi)
	movq	%xmm0, (%rbx)
	movq	%xmm0, -8(%rbx)
	addq	$-16, %rdi
	addq	$-16, %rbx
	addq	$-16, %rdx
	jne	.LBB2_309
# BB#310:                               # %middle.block874
                                        #   in Loop: Header=BB2_294 Depth=1
	testq	%r11, %r11
	movq	96(%rsp), %r11          # 8-byte Reload
	je	.LBB2_317
	.p2align	4, 0x90
.LBB2_311:                              # %.lr.ph17.i.preheader
                                        #   in Loop: Header=BB2_294 Depth=1
	leal	-1(%rsi), %edx
	movl	%esi, %ebx
	andl	$7, %ebx
	je	.LBB2_314
# BB#312:                               # %.lr.ph17.i.prol.preheader
                                        #   in Loop: Header=BB2_294 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB2_313:                              # %.lr.ph17.i.prol
                                        #   Parent Loop BB2_294 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rbp)
	decq	%rbp
	movb	$45, -1(%rcx)
	decq	%rcx
	decl	%esi
	incl	%ebx
	jne	.LBB2_313
.LBB2_314:                              # %.lr.ph17.i.prol.loopexit
                                        #   in Loop: Header=BB2_294 Depth=1
	cmpl	$7, %edx
	jb	.LBB2_317
# BB#315:                               # %.lr.ph17.i.preheader.new
                                        #   in Loop: Header=BB2_294 Depth=1
	decq	%rcx
	decq	%rbp
	.p2align	4, 0x90
.LBB2_316:                              # %.lr.ph17.i
                                        #   Parent Loop BB2_294 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rbp)
	movb	$45, (%rcx)
	movb	$111, -1(%rbp)
	movb	$45, -1(%rcx)
	movb	$111, -2(%rbp)
	movb	$45, -2(%rcx)
	movb	$111, -3(%rbp)
	movb	$45, -3(%rcx)
	movb	$111, -4(%rbp)
	movb	$45, -4(%rcx)
	movb	$111, -5(%rbp)
	movb	$45, -5(%rcx)
	movb	$111, -6(%rbp)
	movb	$45, -6(%rcx)
	movb	$111, -7(%rbp)
	movb	$45, -7(%rcx)
	addq	$-8, %rcx
	addq	$-8, %rbp
	addl	$-8, %esi
	jne	.LBB2_316
.LBB2_317:                              # %._crit_edge18.loopexit.i
                                        #   in Loop: Header=BB2_294 Depth=1
	leaq	-1(%r12,%r10), %r12
	leaq	-1(%r14,%r10), %r14
	leal	-1(%rax,%r13), %eax
	subl	%r8d, %eax
	cmpl	$-1, %r9d
	je	.LBB2_333
.LBB2_319:                              # %.lr.ph25.preheader.i
                                        #   in Loop: Header=BB2_294 Depth=1
	movl	%r9d, %r15d
	notl	%r15d
	movl	$-2, %edi
	subl	%r9d, %edi
	movq	%rdi, %r11
	negq	%r11
	leaq	1(%rdi), %rdx
	cmpq	$16, %rdx
	movl	%r15d, %ecx
	movq	%r14, %rbp
	movq	%r12, %rbx
	jb	.LBB2_326
# BB#320:                               # %min.iters.checked839
                                        #   in Loop: Header=BB2_294 Depth=1
	movl	%edx, %r10d
	andl	$15, %r10d
	subq	%r10, %rdx
	movl	%r15d, %ecx
	movq	%r14, %rbp
	movq	%r12, %rbx
	je	.LBB2_326
# BB#321:                               # %vector.memcheck852
                                        #   in Loop: Header=BB2_294 Depth=1
	leaq	-1(%r12,%r11), %rcx
	cmpq	%r14, %rcx
	jae	.LBB2_323
# BB#322:                               # %vector.memcheck852
                                        #   in Loop: Header=BB2_294 Depth=1
	leaq	-1(%r14,%r11), %rcx
	cmpq	%r12, %rcx
	movl	%r15d, %ecx
	movq	%r14, %rbp
	movq	%r12, %rbx
	jb	.LBB2_326
.LBB2_323:                              # %vector.body835.preheader
                                        #   in Loop: Header=BB2_294 Depth=1
	movl	%r15d, %ecx
	subl	%edx, %ecx
	leaq	-1(%r10), %rbx
	subq	%rdi, %rbx
	leaq	(%r14,%rbx), %rbp
	addq	%r12, %rbx
	leaq	-8(%r12), %rdi
	leaq	-8(%r14), %rsi
	.p2align	4, 0x90
.LBB2_324:                              # %vector.body835
                                        #   Parent Loop BB2_294 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rdi)
	movq	%xmm0, -8(%rdi)
	movq	%xmm1, (%rsi)
	movq	%xmm1, -8(%rsi)
	addq	$-16, %rdi
	addq	$-16, %rsi
	addq	$-16, %rdx
	jne	.LBB2_324
# BB#325:                               # %middle.block836
                                        #   in Loop: Header=BB2_294 Depth=1
	testq	%r10, %r10
	je	.LBB2_332
	.p2align	4, 0x90
.LBB2_326:                              # %.lr.ph25.i.preheader
                                        #   in Loop: Header=BB2_294 Depth=1
	leal	-1(%rcx), %edx
	movl	%ecx, %edi
	andl	$7, %edi
	je	.LBB2_329
# BB#327:                               # %.lr.ph25.i.prol.preheader
                                        #   in Loop: Header=BB2_294 Depth=1
	negl	%edi
	.p2align	4, 0x90
.LBB2_328:                              # %.lr.ph25.i.prol
                                        #   Parent Loop BB2_294 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rbx)
	decq	%rbx
	movb	$111, -1(%rbp)
	decq	%rbp
	decl	%ecx
	incl	%edi
	jne	.LBB2_328
.LBB2_329:                              # %.lr.ph25.i.prol.loopexit
                                        #   in Loop: Header=BB2_294 Depth=1
	cmpl	$7, %edx
	jb	.LBB2_332
# BB#330:                               # %.lr.ph25.i.preheader.new
                                        #   in Loop: Header=BB2_294 Depth=1
	decq	%rbp
	decq	%rbx
	.p2align	4, 0x90
.LBB2_331:                              # %.lr.ph25.i
                                        #   Parent Loop BB2_294 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rbx)
	movb	$111, (%rbp)
	movb	$45, -1(%rbx)
	movb	$111, -1(%rbp)
	movb	$45, -2(%rbx)
	movb	$111, -2(%rbp)
	movb	$45, -3(%rbx)
	movb	$111, -3(%rbp)
	movb	$45, -4(%rbx)
	movb	$111, -4(%rbp)
	movb	$45, -5(%rbx)
	movb	$111, -5(%rbp)
	movb	$45, -6(%rbx)
	movb	$111, -6(%rbp)
	movb	$45, -7(%rbx)
	movb	$111, -7(%rbp)
	addq	$-8, %rbp
	addq	$-8, %rbx
	addl	$-8, %ecx
	jne	.LBB2_331
.LBB2_332:                              # %._crit_edge26.loopexit.i
                                        #   in Loop: Header=BB2_294 Depth=1
	leaq	-1(%r12,%r11), %r12
	leaq	-1(%r14,%r11), %r14
	addl	%r15d, %eax
	movq	240(%rsp), %r15         # 8-byte Reload
	movq	96(%rsp), %r11          # 8-byte Reload
.LBB2_333:                              # %._crit_edge26.i
                                        #   in Loop: Header=BB2_294 Depth=1
	testl	%r13d, %r13d
	movq	40(%rsp), %r10          # 8-byte Reload
	jle	.LBB2_336
# BB#334:                               # %._crit_edge26.i
                                        #   in Loop: Header=BB2_294 Depth=1
	testl	%r10d, %r10d
	jle	.LBB2_336
# BB#335:                               #   in Loop: Header=BB2_294 Depth=1
	addl	%r9d, %r10d
	movb	$111, -1(%r12)
	decq	%r12
	movb	$111, -1(%r14)
	decq	%r14
	addl	$2, %eax
	cmpl	60(%rsp), %eax          # 4-byte Folded Reload
	movl	%r8d, %esi
	jle	.LBB2_294
.LBB2_336:                              # %._crit_edge37.i
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB2_339
# BB#337:                               # %.lr.ph12.preheader.i
	movl	28(%rsp), %ebx          # 4-byte Reload
	movq	64(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_338:                              # %.lr.ph12.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rdi
	movq	(%rbp), %rsi
	movq	%r12, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %r15
	decq	%rbx
	jne	.LBB2_338
.LBB2_339:                              # %.preheader.i
	movl	52(%rsp), %r15d         # 4-byte Reload
	testl	%r15d, %r15d
	movq	136(%rsp), %rbx         # 8-byte Reload
	jle	.LBB2_342
# BB#340:                               # %.lr.ph.preheader.i
	movl	%r15d, %r12d
	movq	80(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB2_341:                              # %.lr.ph.i536
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r14, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r12
	jne	.LBB2_341
.LBB2_342:                              # %Atracking.exit
	movq	112(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	80(%rsp), %r14          # 8-byte Reload
	movq	64(%rsp), %r12          # 8-byte Reload
.LBB2_343:
	movl	544(%rsp), %ebx
	movq	R__align.mseq1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	cmpl	%ebx, %ecx
	jg	.LBB2_345
# BB#344:
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB2_345
.LBB2_346:                              # %.preheader581
	movl	28(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB2_349
# BB#347:                               # %.lr.ph593.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_348:                              # %.lr.ph593
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	movq	R__align.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_348
.LBB2_349:                              # %.preheader
	testl	%r15d, %r15d
	jle	.LBB2_352
# BB#350:                               # %.lr.ph.preheader
	movl	%r15d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB2_351:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	R__align.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB2_351
.LBB2_352:                              # %._crit_edge
	movq	stderr(%rip), %rdi
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	cvtss2sd	%xmm0, %xmm0
	movl	$.L.str.2, %esi
	movb	$1, %al
	callq	fprintf
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	addq	$488, %rsp              # imm = 0x1E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_345:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
	movl	%ebx, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.1, %edi
	callq	ErrorExit
	jmp	.LBB2_346
.Lfunc_end2:
	.size	R__align, .Lfunc_end2-R__align
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
.Lcfi30:
	.cfi_offset %rbx, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r11
	cmpl	$0, 48(%rsp)
	je	.LBB3_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB3_10
# BB#2:                                 # %.preheader77.preheader
	movl	%r8d, %r10d
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_3:                                # %.preheader77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_4 Depth 2
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_4:                                #   Parent Loop BB3_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB3_5
	jnp	.LBB3_6
.LBB3_5:                                #   in Loop: Header=BB3_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	movl	%eax, (%rbx,%r15,4)
	incl	%r15d
.LBB3_6:                                #   in Loop: Header=BB3_4 Depth=2
	movq	8(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB3_7
	jnp	.LBB3_8
.LBB3_7:                                #   in Loop: Header=BB3_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r15,4)
	incl	%r15d
.LBB3_8:                                #   in Loop: Header=BB3_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB3_4
# BB#9:                                 #   in Loop: Header=BB3_3 Depth=1
	movq	(%r11,%r14,8), %rax
	movslq	%r15d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB3_3
.LBB3_10:                               # %.preheader76
	movl	$n_dis, %r10d
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
	movl	$0, -104(%rsp,%r14,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ebx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm1, %xmm1
	cvtsi2ssl	(%rax), %xmm1
	movq	-8(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	104(%rax), %xmm0
	movq	(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rax
	addq	$2, %rbx
	cmpq	$27, %rbx
	jne	.LBB3_12
# BB#13:                                #   in Loop: Header=BB3_11 Depth=1
	movss	%xmm0, -104(%rsp,%r14,4)
	incq	%r14
	addq	$4, %r10
	cmpq	$26, %r14
	jne	.LBB3_11
	jmp	.LBB3_14
	.p2align	4, 0x90
.LBB3_18:                               # %._crit_edge
                                        #   in Loop: Header=BB3_14 Depth=1
	addq	$8, %r11
	addq	$8, %r9
	addq	$4, %rdi
.LBB3_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB3_19
# BB#15:                                # %.lr.ph84
                                        #   in Loop: Header=BB3_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB3_18
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB3_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_17:                               # %.lr.ph
                                        #   Parent Loop BB3_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addq	$4, %rcx
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB3_17
	jmp	.LBB3_18
.LBB3_19:                               # %._crit_edge85
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	match_calc, .Lfunc_end3-match_calc
	.cfi_endproc

	.type	impmtx,@object          # @impmtx
	.local	impmtx
	.comm	impmtx,8,8
	.type	imp_match_init_strictR.impalloclen,@object # @imp_match_init_strictR.impalloclen
	.local	imp_match_init_strictR.impalloclen
	.comm	imp_match_init_strictR.impalloclen,4,4
	.type	imp_match_init_strictR.nocount1,@object # @imp_match_init_strictR.nocount1
	.local	imp_match_init_strictR.nocount1
	.comm	imp_match_init_strictR.nocount1,8,8
	.type	imp_match_init_strictR.nocount2,@object # @imp_match_init_strictR.nocount2
	.local	imp_match_init_strictR.nocount2
	.comm	imp_match_init_strictR.nocount2,8,8
	.type	R__align.mi,@object     # @R__align.mi
	.local	R__align.mi
	.comm	R__align.mi,4,4
	.type	R__align.m,@object      # @R__align.m
	.local	R__align.m
	.comm	R__align.m,8,8
	.type	R__align.ijp,@object    # @R__align.ijp
	.local	R__align.ijp
	.comm	R__align.ijp,8,8
	.type	R__align.mpi,@object    # @R__align.mpi
	.local	R__align.mpi
	.comm	R__align.mpi,4,4
	.type	R__align.mp,@object     # @R__align.mp
	.local	R__align.mp
	.comm	R__align.mp,8,8
	.type	R__align.w1,@object     # @R__align.w1
	.local	R__align.w1
	.comm	R__align.w1,8,8
	.type	R__align.w2,@object     # @R__align.w2
	.local	R__align.w2
	.comm	R__align.w2,8,8
	.type	R__align.match,@object  # @R__align.match
	.local	R__align.match
	.comm	R__align.match,8,8
	.type	R__align.initverticalw,@object # @R__align.initverticalw
	.local	R__align.initverticalw
	.comm	R__align.initverticalw,8,8
	.type	R__align.lastverticalw,@object # @R__align.lastverticalw
	.local	R__align.lastverticalw
	.comm	R__align.lastverticalw,8,8
	.type	R__align.mseq1,@object  # @R__align.mseq1
	.local	R__align.mseq1
	.comm	R__align.mseq1,8,8
	.type	R__align.mseq2,@object  # @R__align.mseq2
	.local	R__align.mseq2
	.comm	R__align.mseq2,8,8
	.type	R__align.mseq,@object   # @R__align.mseq
	.local	R__align.mseq
	.comm	R__align.mseq,8,8
	.type	R__align.digf1,@object  # @R__align.digf1
	.local	R__align.digf1
	.comm	R__align.digf1,8,8
	.type	R__align.digf2,@object  # @R__align.digf2
	.local	R__align.digf2
	.comm	R__align.digf2,8,8
	.type	R__align.diaf1,@object  # @R__align.diaf1
	.local	R__align.diaf1
	.comm	R__align.diaf1,8,8
	.type	R__align.diaf2,@object  # @R__align.diaf2
	.local	R__align.diaf2
	.comm	R__align.diaf2,8,8
	.type	R__align.gapz1,@object  # @R__align.gapz1
	.local	R__align.gapz1
	.comm	R__align.gapz1,8,8
	.type	R__align.gapz2,@object  # @R__align.gapz2
	.local	R__align.gapz2
	.comm	R__align.gapz2,8,8
	.type	R__align.gapf1,@object  # @R__align.gapf1
	.local	R__align.gapf1
	.comm	R__align.gapf1,8,8
	.type	R__align.gapf2,@object  # @R__align.gapf2
	.local	R__align.gapf2
	.comm	R__align.gapf2,8,8
	.type	R__align.ogcp1g,@object # @R__align.ogcp1g
	.local	R__align.ogcp1g
	.comm	R__align.ogcp1g,8,8
	.type	R__align.ogcp2g,@object # @R__align.ogcp2g
	.local	R__align.ogcp2g
	.comm	R__align.ogcp2g,8,8
	.type	R__align.fgcp1g,@object # @R__align.fgcp1g
	.local	R__align.fgcp1g
	.comm	R__align.fgcp1g,8,8
	.type	R__align.fgcp2g,@object # @R__align.fgcp2g
	.local	R__align.fgcp2g
	.comm	R__align.fgcp2g,8,8
	.type	R__align.ogcp1,@object  # @R__align.ogcp1
	.local	R__align.ogcp1
	.comm	R__align.ogcp1,8,8
	.type	R__align.ogcp2,@object  # @R__align.ogcp2
	.local	R__align.ogcp2
	.comm	R__align.ogcp2,8,8
	.type	R__align.fgcp1,@object  # @R__align.fgcp1
	.local	R__align.fgcp1
	.comm	R__align.fgcp1,8,8
	.type	R__align.fgcp2,@object  # @R__align.fgcp2
	.local	R__align.fgcp2
	.comm	R__align.fgcp2,8,8
	.type	R__align.cpmx1,@object  # @R__align.cpmx1
	.local	R__align.cpmx1
	.comm	R__align.cpmx1,8,8
	.type	R__align.cpmx2,@object  # @R__align.cpmx2
	.local	R__align.cpmx2
	.comm	R__align.cpmx2,8,8
	.type	R__align.intwork,@object # @R__align.intwork
	.local	R__align.intwork
	.comm	R__align.intwork,8,8
	.type	R__align.floatwork,@object # @R__align.floatwork
	.local	R__align.floatwork
	.comm	R__align.floatwork,8,8
	.type	R__align.orlgth1,@object # @R__align.orlgth1
	.local	R__align.orlgth1
	.comm	R__align.orlgth1,4,4
	.type	R__align.orlgth2,@object # @R__align.orlgth2
	.local	R__align.orlgth2
	.comm	R__align.orlgth2,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.1, 14

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"wm = %f\n"
	.size	.L.str.2, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
