	.text
	.file	"print.bc"
	.globl	terse
	.p2align	4, 0x90
	.type	terse,@function
terse:                                  # @terse
	.cfi_startproc
# BB#0:
	cmpb	$0, any_conflicts(%rip)
	je	.LBB0_1
# BB#2:
	jmp	conflict_log            # TAILCALL
.LBB0_1:
	retq
.Lfunc_end0:
	.size	terse, .Lfunc_end0-terse
	.cfi_endproc

	.globl	verbose
	.p2align	4, 0x90
	.type	verbose,@function
verbose:                                # @verbose
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	cmpb	$0, any_conflicts(%rip)
	je	.LBB1_2
# BB#1:
	callq	verbose_conflict_log
.LBB1_2:
	movq	foutput(%rip), %rcx
	movl	$.L.str, %edi
	movl	$15, %esi
	movl	$1, %edx
	callq	fwrite
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	(%rax), %rcx
	movl	$.L.str.1, %esi
	movl	$-1, %edx
	xorl	%eax, %eax
	callq	fprintf
	cmpl	$0, translations(%rip)
	je	.LBB1_12
# BB#3:                                 # %.preheader14
	movl	max_user_token_number(%rip), %eax
	testl	%eax, %eax
	js	.LBB1_8
# BB#4:                                 # %.lr.ph20.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph20
                                        # =>This Inner Loop Header: Depth=1
	movq	token_translations(%rip), %rcx
	movswq	(%rcx,%rbx,2), %rcx
	cmpq	$2, %rcx
	je	.LBB1_7
# BB#6:                                 #   in Loop: Header=BB1_5 Depth=1
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	(%rax,%rcx,8), %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	max_user_token_number(%rip), %eax
.LBB1_7:                                #   in Loop: Header=BB1_5 Depth=1
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	leaq	1(%rbx), %rbx
	jl	.LBB1_5
	jmp	.LBB1_8
.LBB1_12:                               # %.preheader13
	cmpl	$2, ntokens(%rip)
	jl	.LBB1_8
# BB#13:                                # %.lr.ph18.preheader
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB1_14:                               # %.lr.ph18
                                        # =>This Inner Loop Header: Depth=1
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	(%rax,%rbx,8), %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	incq	%rbx
	movslq	ntokens(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB1_14
.LBB1_8:                                # %.preheader
	cmpl	$0, nstates(%rip)
	jle	.LBB1_11
# BB#9:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	foutput(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	%ebx, %edi
	callq	print_core
	movl	%ebx, %edi
	callq	print_actions
	incl	%ebx
	cmpl	nstates(%rip), %ebx
	jl	.LBB1_10
.LBB1_11:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end1:
	.size	verbose, .Lfunc_end1-verbose
	.cfi_endproc

	.globl	print_token
	.p2align	4, 0x90
	.type	print_token,@function
print_token:                            # @print_token
	.cfi_startproc
# BB#0:
	movl	%edi, %edx
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rcx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	jmp	fprintf                 # TAILCALL
.Lfunc_end2:
	.size	print_token, .Lfunc_end2-print_token
	.cfi_endproc

	.globl	print_state
	.p2align	4, 0x90
	.type	print_state,@function
print_state:                            # @print_state
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movq	foutput(%rip), %rdi
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	%ebx, %edi
	callq	print_core
	movl	%ebx, %edi
	popq	%rbx
	jmp	print_actions           # TAILCALL
.Lfunc_end3:
	.size	print_state, .Lfunc_end3-print_state
	.cfi_endproc

	.globl	print_core
	.p2align	4, 0x90
	.type	print_core,@function
print_core:                             # @print_core
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi4:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi7:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 64
.Lcfi11:
	.cfi_offset %rbx, -56
.Lcfi12:
	.cfi_offset %r12, -48
.Lcfi13:
	.cfi_offset %r13, -40
.Lcfi14:
	.cfi_offset %r14, -32
.Lcfi15:
	.cfi_offset %r15, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movq	state_table(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %r14
	movswq	20(%r14), %rax
	testq	%rax, %rax
	je	.LBB4_13
# BB#1:                                 # %.preheader
	testw	%ax, %ax
	jle	.LBB4_12
# BB#2:                                 # %.lr.ph41.preheader
	movl	%eax, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB4_3:                                # %.lr.ph41
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
                                        #     Child Loop BB4_7 Depth 2
                                        #     Child Loop BB4_10 Depth 2
	movswq	22(%r14,%r12,2), %rbp
	addq	%rbp, %rbp
	addq	ritem(%rip), %rbp
	movq	%rbp, %rax
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswq	(%rax), %r13
	addq	$2, %rax
	testq	%r13, %r13
	jg	.LBB4_4
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rax
	movq	rlhs(%rip), %rcx
	movq	%r13, %rbx
	addq	%rbx, %rbx
	subq	%rbx, %rcx
	movswq	(%rcx), %rcx
	movq	(%rax,%rcx,8), %rdx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	rrhs(%rip), %rax
	subq	%rbx, %rax
	movswq	(%rax), %rbx
	addq	%rbx, %rbx
	addq	ritem(%rip), %rbx
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        #   in Loop: Header=BB4_7 Depth=2
	movq	tags(%rip), %rax
	movswq	(%rbx), %rdx
	movq	(%rax,%rdx,8), %rdx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	fprintf
	addq	$2, %rbx
.LBB4_7:                                # %.lr.ph
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	foutput(%rip), %rcx
	cmpq	%rbp, %rbx
	jb	.LBB4_6
# BB#8:                                 # %._crit_edge
                                        #   in Loop: Header=BB4_3 Depth=1
	negl	%r13d
	movl	$46, %edi
	movq	%rcx, %rsi
	callq	_IO_putc
	movzwl	(%rbx), %eax
	movq	foutput(%rip), %rdi
	testw	%ax, %ax
	jle	.LBB4_11
# BB#9:                                 # %.lr.ph37.preheader
                                        #   in Loop: Header=BB4_3 Depth=1
	addq	$2, %rbx
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph37
                                        #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	tags(%rip), %rcx
	movswq	%ax, %rax
	movq	(%rcx,%rax,8), %rdx
	movl	$.L.str.5, %esi
	xorl	%eax, %eax
	callq	fprintf
	movzwl	(%rbx), %eax
	addq	$2, %rbx
	testw	%ax, %ax
	movq	foutput(%rip), %rdi
	jg	.LBB4_10
.LBB4_11:                               # %._crit_edge38
                                        #   in Loop: Header=BB4_3 Depth=1
	movl	$.L.str.6, %esi
	xorl	%eax, %eax
	movl	%r13d, %edx
	callq	fprintf
	movq	foutput(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
	incq	%r12
	cmpq	%r15, %r12
	jne	.LBB4_3
.LBB4_12:                               # %._crit_edge42
	movq	foutput(%rip), %rsi
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB4_13:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	print_core, .Lfunc_end4-print_core
	.cfi_endproc

	.globl	print_actions
	.p2align	4, 0x90
	.type	print_actions,@function
print_actions:                          # @print_actions
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 80
.Lcfi24:
	.cfi_offset %rbx, -56
.Lcfi25:
	.cfi_offset %r12, -48
.Lcfi26:
	.cfi_offset %r13, -40
.Lcfi27:
	.cfi_offset %r14, -32
.Lcfi28:
	.cfi_offset %r15, -24
.Lcfi29:
	.cfi_offset %rbp, -16
	movq	shift_table(%rip), %rax
	movslq	%edi, %r13
	movq	(%rax,%r13,8), %r15
	movq	reduction_table(%rip), %rax
	movq	(%rax,%r13,8), %r12
	movq	err_table(%rip), %rax
	movq	(%rax,%r13,8), %rbx
	testq	%r15, %r15
	jne	.LBB5_2
# BB#1:
	testq	%r12, %r12
	jne	.LBB5_2
# BB#32:
	movq	foutput(%rip), %rcx
	movl	$.L.str.7, %edi
	movl	$15, %esi
	movl	$1, %edx
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	fwrite                  # TAILCALL
.LBB5_2:
	movl	%edi, 12(%rsp)          # 4-byte Spill
	testq	%r15, %r15
	je	.LBB5_10
# BB#3:
	movswq	10(%r15), %rax
	testq	%rax, %rax
	jle	.LBB5_10
# BB#4:                                 # %.lr.ph73.preheader
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%eax, %ebp
	leaq	12(%r15), %r14
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph73
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB5_7
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	movq	accessing_symbol(%rip), %rax
	movslq	%ecx, %rdx
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rsi
	movswq	(%rax,%rdx,2), %rax
	movq	(%rsi,%rax,8), %rdx
	movl	$.L.str.8, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB5_7:                                #   in Loop: Header=BB5_5 Depth=1
	addq	$2, %r14
	decq	%rbp
	jne	.LBB5_5
# BB#8:                                 # %._crit_edge74
	cmpw	$0, 16(%rsp)            # 2-byte Folded Reload
	jle	.LBB5_10
# BB#9:
	movq	foutput(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB5_10:                               # %._crit_edge74.thread
	xorl	%ebp, %ebp
	testq	%rbx, %rbx
	je	.LBB5_11
# BB#12:
	movswl	(%rbx), %r14d
	testl	%r14d, %r14d
	jle	.LBB5_20
# BB#13:                                # %.lr.ph69.preheader
	movl	%r14d, %ebp
	addq	$2, %rbx
	.p2align	4, 0x90
.LBB5_14:                               # %.lr.ph69
                                        # =>This Inner Loop Header: Depth=1
	movswq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB5_16
# BB#15:                                #   in Loop: Header=BB5_14 Depth=1
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rcx
	movq	(%rcx,%rax,8), %rdx
	movl	$.L.str.9, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB5_16:                               #   in Loop: Header=BB5_14 Depth=1
	addq	$2, %rbx
	decq	%rbp
	jne	.LBB5_14
# BB#17:                                # %._crit_edge70
	testw	%r14w, %r14w
	jle	.LBB5_19
# BB#18:
	movq	foutput(%rip), %rsi
	movl	$10, %edi
	callq	_IO_putc
.LBB5_19:                               # %._crit_edge70.thread
	movl	%r14d, %ebp
	testq	%r12, %r12
	jne	.LBB5_21
	jmp	.LBB5_23
.LBB5_11:
	xorl	%r14d, %r14d
.LBB5_20:                               # %._crit_edge70.thread
	testq	%r12, %r12
	je	.LBB5_23
.LBB5_21:                               # %._crit_edge70.thread
	movq	consistent(%rip), %rax
	movb	(%rax,%r13), %al
	testb	%al, %al
	je	.LBB5_23
# BB#22:
	movswq	12(%r12), %rdx
	movq	rlhs(%rip), %rax
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rcx
	movswq	(%rax,%rdx,2), %rax
	movq	(%rcx,%rax,8), %rcx
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	cmpl	%r14d, %ebp
	jl	.LBB5_26
	jmp	.LBB5_31
.LBB5_23:
	testq	%r12, %r12
	je	.LBB5_25
# BB#24:
	movl	12(%rsp), %edi          # 4-byte Reload
	callq	print_reductions
.LBB5_25:
	cmpl	%r14d, %ebp
	jge	.LBB5_31
.LBB5_26:                               # %.lr.ph.preheader
	movslq	%ebp, %rax
	leaq	12(%r15,%rax,2), %rbx
	subl	%ebp, %r14d
	.p2align	4, 0x90
.LBB5_27:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movswl	(%rbx), %ecx
	testl	%ecx, %ecx
	je	.LBB5_29
# BB#28:                                #   in Loop: Header=BB5_27 Depth=1
	movq	accessing_symbol(%rip), %rax
	movslq	%ecx, %rdx
	movq	foutput(%rip), %rdi
	movq	tags(%rip), %rsi
	movswq	(%rax,%rdx,2), %rax
	movq	(%rsi,%rax,8), %rdx
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	fprintf
.LBB5_29:                               #   in Loop: Header=BB5_27 Depth=1
	addq	$2, %rbx
	decl	%r14d
	jne	.LBB5_27
# BB#30:                                # %._crit_edge
	movq	foutput(%rip), %rsi
	movl	$10, %edi
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_IO_putc                # TAILCALL
.LBB5_31:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	print_actions, .Lfunc_end5-print_actions
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"\n\ntoken types:\n"
	.size	.L.str, 16

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" type %d is %s\n"
	.size	.L.str.1, 16

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\n\nstate %d\n\n"
	.size	.L.str.2, 13

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"    %s  ->  "
	.size	.L.str.3, 13

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s "
	.size	.L.str.4, 4

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	" %s"
	.size	.L.str.5, 4

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"   (%d)"
	.size	.L.str.6, 8

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"    NO ACTIONS\n"
	.size	.L.str.7, 16

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"    %-4s\tshift  %d\n"
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"    %-4s\terror (nonassociative)\n"
	.size	.L.str.9, 33

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"    $default\treduce  %d  (%s)\n\n"
	.size	.L.str.10, 32

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"    %-4s\tgoto  %d\n"
	.size	.L.str.11, 19


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
