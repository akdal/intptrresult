	.text
	.file	"tabinit.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
.LCPI0_1:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI0_2:
	.quad	4580160821035794432     # double 0.015625
.LCPI0_3:
	.quad	4535124824762089472     # double 1.52587890625E-5
	.text
	.globl	make_decode_tables
	.p2align	4, 0x90
	.type	make_decode_tables,@function
make_decode_tables:                     # @make_decode_tables
	.cfi_startproc
# BB#0:                                 # %.lr.ph71
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movq	pnts(%rip), %r14
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addsd	%xmm0, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_2(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	cos
	addsd	%xmm0, %xmm0
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%r14,%rbx,8)
	leal	1(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	addsd	%xmm0, %xmm0
	addsd	%xmm2, %xmm0
	mulsd	.LCPI0_1(%rip), %xmm0
	mulsd	.LCPI0_2(%rip), %xmm0
	callq	cos
	addsd	%xmm0, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 8(%r14,%rbx,8)
	addq	$2, %rbx
	cmpq	$16, %rbx
	jne	.LBB0_1
# BB#2:                                 # %._crit_edge72.485
	movq	pnts+8(%rip), %rax
	movabsq	$4602700610165397873, %rcx # imm = 0x3FE013D19C61D971
	movq	%rcx, (%rax)
	movabsq	$4602881468680364382, %rcx # imm = 0x3FE0B84F03EBE15E
	movq	%rcx, 8(%rax)
	movabsq	$4603281797433154164, %rcx # imm = 0x3FE22467D1065A74
	movq	%rcx, 16(%rax)
	movabsq	$4604001272230306758, %rcx # imm = 0x3FE4B2C398BBE3C6
	movq	%rcx, 24(%rax)
	movabsq	$4605274285282247184, %rcx # imm = 0x3FE938900B7D4210
	movq	%rcx, 32(%rax)
	movabsq	$4607455686804033239, %rcx # imm = 0x3FF0F8892A4ECED7
	movq	%rcx, 40(%rax)
	movabsq	$4610436031282438466, %rcx # imm = 0x3FFB8F24B0406142
	movq	%rcx, 48(%rax)
	movabsq	$4617429401181960404, %rcx # imm = 0x4014679380E538D4
	movq	%rcx, 56(%rax)
	movq	pnts+16(%rip), %rax
	movabsq	$4602767049905453651, %rcx # imm = 0x3FE0503ED17CBA53
	movq	%rcx, (%rax)
	movabsq	$4603591652762720062, %rcx # imm = 0x3FE33E37A1E0173E
	movq	%rcx, 8(%rax)
	movabsq	$4606281484711595351, %rcx # imm = 0x3FECCC9AEFB18D57
	movq	%rcx, 16(%rax)
	movabsq	$4612953591327732774, %rcx # imm = 0x400480D9D073B426
	movq	%rcx, 24(%rax)
	movq	pnts+24(%rip), %rax
	movabsq	$4603049880655181973, %rcx # imm = 0x3FE1517A7BDB3895
	movq	%rcx, (%rax)
	movabsq	$4608563055654400251, %rcx # imm = 0x3FF4E7AE9144F0FB
	movq	%rcx, 8(%rax)
	movq	pnts+32(%rip), %rax
	movabsq	$4604544271217802188, %rcx # imm = 0x3FE6A09E667F3BCC
	movq	%rcx, (%rax)
	movl	$decwin, %eax
	negq	%r15
	xorl	%ecx, %ecx
	movl	$decwin+4224, %edx
	movsd	.LCPI0_3(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rdx, %rax
	jae	.LBB0_5
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdq	intwinbase(,%rcx,8), %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%r15, %xmm2
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax)
	movsd	%xmm2, 128(%rax)
.LBB0_5:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%ecx, %esi
	sarl	$31, %esi
	movl	%esi, %edi
	shrl	$27, %edi
	addl	%ecx, %edi
	andl	$-32, %edi
	movl	%ecx, %ebx
	subl	%edi, %ebx
	leaq	-8184(%rax), %rdi
	cmpl	$31, %ebx
	cmoveq	%rdi, %rax
	shrl	$26, %esi
	addl	%ecx, %esi
	andl	$-64, %esi
	movl	%ecx, %edi
	subl	%esi, %edi
	movq	%r15, %rsi
	negq	%rsi
	cmpl	$63, %edi
	cmoveq	%rsi, %r15
	incq	%rcx
	addq	$256, %rax              # imm = 0x100
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB0_3
# BB#6:                                 # %.lr.ph.preheader
	movl	$256, %esi              # imm = 0x100
	movl	$intwinbase+2048, %edx
	movl	$decwin+4224, %r8d
	.p2align	4, 0x90
.LBB0_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r8, %rax
	jae	.LBB0_9
# BB#8:                                 #   in Loop: Header=BB0_7 Depth=1
	xorps	%xmm1, %xmm1
	cvtsi2sdq	(%rdx), %xmm1
	mulsd	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	cvtsi2sdq	%r15, %xmm2
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax)
	movsd	%xmm2, 128(%rax)
.LBB0_9:                                #   in Loop: Header=BB0_7 Depth=1
	movl	%esi, %ecx
	sarl	$31, %ecx
	movl	%ecx, %edi
	shrl	$27, %edi
	addl	%esi, %edi
	andl	$-32, %edi
	movl	%esi, %ebx
	subl	%edi, %ebx
	leaq	-8184(%rax), %rdi
	cmpl	$31, %ebx
	cmovneq	%rax, %rdi
	shrl	$26, %ecx
	addl	%esi, %ecx
	andl	$-64, %ecx
	movl	%esi, %eax
	subl	%ecx, %eax
	movq	%r15, %rcx
	negq	%rcx
	cmpl	$63, %eax
	cmoveq	%rcx, %r15
	incl	%esi
	addq	$256, %rdi              # imm = 0x100
	addq	$-8, %rdx
	cmpl	$512, %esi              # imm = 0x200
	movq	%rdi, %rax
	jne	.LBB0_7
# BB#10:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	make_decode_tables, .Lfunc_end0-make_decode_tables
	.cfi_endproc

	.type	cos64,@object           # @cos64
	.local	cos64
	.comm	cos64,128,16
	.type	cos32,@object           # @cos32
	.local	cos32
	.comm	cos32,64,16
	.type	cos16,@object           # @cos16
	.local	cos16
	.comm	cos16,32,16
	.type	cos8,@object            # @cos8
	.local	cos8
	.comm	cos8,16,16
	.type	cos4,@object            # @cos4
	.local	cos4
	.comm	cos4,8,8
	.type	pnts,@object            # @pnts
	.data
	.globl	pnts
	.p2align	4
pnts:
	.quad	cos64
	.quad	cos32
	.quad	cos16
	.quad	cos8
	.quad	cos4
	.size	pnts, 40

	.type	decwin,@object          # @decwin
	.comm	decwin,4352,16
	.type	intwinbase,@object      # @intwinbase
	.section	.rodata,"a",@progbits
	.p2align	4
intwinbase:
	.quad	0                       # 0x0
	.quad	-1                      # 0xffffffffffffffff
	.quad	-1                      # 0xffffffffffffffff
	.quad	-1                      # 0xffffffffffffffff
	.quad	-1                      # 0xffffffffffffffff
	.quad	-1                      # 0xffffffffffffffff
	.quad	-1                      # 0xffffffffffffffff
	.quad	-2                      # 0xfffffffffffffffe
	.quad	-2                      # 0xfffffffffffffffe
	.quad	-2                      # 0xfffffffffffffffe
	.quad	-2                      # 0xfffffffffffffffe
	.quad	-3                      # 0xfffffffffffffffd
	.quad	-3                      # 0xfffffffffffffffd
	.quad	-4                      # 0xfffffffffffffffc
	.quad	-4                      # 0xfffffffffffffffc
	.quad	-5                      # 0xfffffffffffffffb
	.quad	-5                      # 0xfffffffffffffffb
	.quad	-6                      # 0xfffffffffffffffa
	.quad	-7                      # 0xfffffffffffffff9
	.quad	-7                      # 0xfffffffffffffff9
	.quad	-8                      # 0xfffffffffffffff8
	.quad	-9                      # 0xfffffffffffffff7
	.quad	-10                     # 0xfffffffffffffff6
	.quad	-11                     # 0xfffffffffffffff5
	.quad	-13                     # 0xfffffffffffffff3
	.quad	-14                     # 0xfffffffffffffff2
	.quad	-16                     # 0xfffffffffffffff0
	.quad	-17                     # 0xffffffffffffffef
	.quad	-19                     # 0xffffffffffffffed
	.quad	-21                     # 0xffffffffffffffeb
	.quad	-24                     # 0xffffffffffffffe8
	.quad	-26                     # 0xffffffffffffffe6
	.quad	-29                     # 0xffffffffffffffe3
	.quad	-31                     # 0xffffffffffffffe1
	.quad	-35                     # 0xffffffffffffffdd
	.quad	-38                     # 0xffffffffffffffda
	.quad	-41                     # 0xffffffffffffffd7
	.quad	-45                     # 0xffffffffffffffd3
	.quad	-49                     # 0xffffffffffffffcf
	.quad	-53                     # 0xffffffffffffffcb
	.quad	-58                     # 0xffffffffffffffc6
	.quad	-63                     # 0xffffffffffffffc1
	.quad	-68                     # 0xffffffffffffffbc
	.quad	-73                     # 0xffffffffffffffb7
	.quad	-79                     # 0xffffffffffffffb1
	.quad	-85                     # 0xffffffffffffffab
	.quad	-91                     # 0xffffffffffffffa5
	.quad	-97                     # 0xffffffffffffff9f
	.quad	-104                    # 0xffffffffffffff98
	.quad	-111                    # 0xffffffffffffff91
	.quad	-117                    # 0xffffffffffffff8b
	.quad	-125                    # 0xffffffffffffff83
	.quad	-132                    # 0xffffffffffffff7c
	.quad	-139                    # 0xffffffffffffff75
	.quad	-147                    # 0xffffffffffffff6d
	.quad	-154                    # 0xffffffffffffff66
	.quad	-161                    # 0xffffffffffffff5f
	.quad	-169                    # 0xffffffffffffff57
	.quad	-176                    # 0xffffffffffffff50
	.quad	-183                    # 0xffffffffffffff49
	.quad	-190                    # 0xffffffffffffff42
	.quad	-196                    # 0xffffffffffffff3c
	.quad	-202                    # 0xffffffffffffff36
	.quad	-208                    # 0xffffffffffffff30
	.quad	-213                    # 0xffffffffffffff2b
	.quad	-218                    # 0xffffffffffffff26
	.quad	-222                    # 0xffffffffffffff22
	.quad	-225                    # 0xffffffffffffff1f
	.quad	-227                    # 0xffffffffffffff1d
	.quad	-228                    # 0xffffffffffffff1c
	.quad	-228                    # 0xffffffffffffff1c
	.quad	-227                    # 0xffffffffffffff1d
	.quad	-224                    # 0xffffffffffffff20
	.quad	-221                    # 0xffffffffffffff23
	.quad	-215                    # 0xffffffffffffff29
	.quad	-208                    # 0xffffffffffffff30
	.quad	-200                    # 0xffffffffffffff38
	.quad	-189                    # 0xffffffffffffff43
	.quad	-177                    # 0xffffffffffffff4f
	.quad	-163                    # 0xffffffffffffff5d
	.quad	-146                    # 0xffffffffffffff6e
	.quad	-127                    # 0xffffffffffffff81
	.quad	-106                    # 0xffffffffffffff96
	.quad	-83                     # 0xffffffffffffffad
	.quad	-57                     # 0xffffffffffffffc7
	.quad	-29                     # 0xffffffffffffffe3
	.quad	2                       # 0x2
	.quad	36                      # 0x24
	.quad	72                      # 0x48
	.quad	111                     # 0x6f
	.quad	153                     # 0x99
	.quad	197                     # 0xc5
	.quad	244                     # 0xf4
	.quad	294                     # 0x126
	.quad	347                     # 0x15b
	.quad	401                     # 0x191
	.quad	459                     # 0x1cb
	.quad	519                     # 0x207
	.quad	581                     # 0x245
	.quad	645                     # 0x285
	.quad	711                     # 0x2c7
	.quad	779                     # 0x30b
	.quad	848                     # 0x350
	.quad	919                     # 0x397
	.quad	991                     # 0x3df
	.quad	1064                    # 0x428
	.quad	1137                    # 0x471
	.quad	1210                    # 0x4ba
	.quad	1283                    # 0x503
	.quad	1356                    # 0x54c
	.quad	1428                    # 0x594
	.quad	1498                    # 0x5da
	.quad	1567                    # 0x61f
	.quad	1634                    # 0x662
	.quad	1698                    # 0x6a2
	.quad	1759                    # 0x6df
	.quad	1817                    # 0x719
	.quad	1870                    # 0x74e
	.quad	1919                    # 0x77f
	.quad	1962                    # 0x7aa
	.quad	2001                    # 0x7d1
	.quad	2032                    # 0x7f0
	.quad	2057                    # 0x809
	.quad	2075                    # 0x81b
	.quad	2085                    # 0x825
	.quad	2087                    # 0x827
	.quad	2080                    # 0x820
	.quad	2063                    # 0x80f
	.quad	2037                    # 0x7f5
	.quad	2000                    # 0x7d0
	.quad	1952                    # 0x7a0
	.quad	1893                    # 0x765
	.quad	1822                    # 0x71e
	.quad	1739                    # 0x6cb
	.quad	1644                    # 0x66c
	.quad	1535                    # 0x5ff
	.quad	1414                    # 0x586
	.quad	1280                    # 0x500
	.quad	1131                    # 0x46b
	.quad	970                     # 0x3ca
	.quad	794                     # 0x31a
	.quad	605                     # 0x25d
	.quad	402                     # 0x192
	.quad	185                     # 0xb9
	.quad	-45                     # 0xffffffffffffffd3
	.quad	-288                    # 0xfffffffffffffee0
	.quad	-545                    # 0xfffffffffffffddf
	.quad	-814                    # 0xfffffffffffffcd2
	.quad	-1095                   # 0xfffffffffffffbb9
	.quad	-1388                   # 0xfffffffffffffa94
	.quad	-1692                   # 0xfffffffffffff964
	.quad	-2006                   # 0xfffffffffffff82a
	.quad	-2330                   # 0xfffffffffffff6e6
	.quad	-2663                   # 0xfffffffffffff599
	.quad	-3004                   # 0xfffffffffffff444
	.quad	-3351                   # 0xfffffffffffff2e9
	.quad	-3705                   # 0xfffffffffffff187
	.quad	-4063                   # 0xfffffffffffff021
	.quad	-4425                   # 0xffffffffffffeeb7
	.quad	-4788                   # 0xffffffffffffed4c
	.quad	-5153                   # 0xffffffffffffebdf
	.quad	-5517                   # 0xffffffffffffea73
	.quad	-5879                   # 0xffffffffffffe909
	.quad	-6237                   # 0xffffffffffffe7a3
	.quad	-6589                   # 0xffffffffffffe643
	.quad	-6935                   # 0xffffffffffffe4e9
	.quad	-7271                   # 0xffffffffffffe399
	.quad	-7597                   # 0xffffffffffffe253
	.quad	-7910                   # 0xffffffffffffe11a
	.quad	-8209                   # 0xffffffffffffdfef
	.quad	-8491                   # 0xffffffffffffded5
	.quad	-8755                   # 0xffffffffffffddcd
	.quad	-8998                   # 0xffffffffffffdcda
	.quad	-9219                   # 0xffffffffffffdbfd
	.quad	-9416                   # 0xffffffffffffdb38
	.quad	-9585                   # 0xffffffffffffda8f
	.quad	-9727                   # 0xffffffffffffda01
	.quad	-9838                   # 0xffffffffffffd992
	.quad	-9916                   # 0xffffffffffffd944
	.quad	-9959                   # 0xffffffffffffd919
	.quad	-9966                   # 0xffffffffffffd912
	.quad	-9935                   # 0xffffffffffffd931
	.quad	-9863                   # 0xffffffffffffd979
	.quad	-9750                   # 0xffffffffffffd9ea
	.quad	-9592                   # 0xffffffffffffda88
	.quad	-9389                   # 0xffffffffffffdb53
	.quad	-9139                   # 0xffffffffffffdc4d
	.quad	-8840                   # 0xffffffffffffdd78
	.quad	-8492                   # 0xffffffffffffded4
	.quad	-8092                   # 0xffffffffffffe064
	.quad	-7640                   # 0xffffffffffffe228
	.quad	-7134                   # 0xffffffffffffe422
	.quad	-6574                   # 0xffffffffffffe652
	.quad	-5959                   # 0xffffffffffffe8b9
	.quad	-5288                   # 0xffffffffffffeb58
	.quad	-4561                   # 0xffffffffffffee2f
	.quad	-3776                   # 0xfffffffffffff140
	.quad	-2935                   # 0xfffffffffffff489
	.quad	-2037                   # 0xfffffffffffff80b
	.quad	-1082                   # 0xfffffffffffffbc6
	.quad	-70                     # 0xffffffffffffffba
	.quad	998                     # 0x3e6
	.quad	2122                    # 0x84a
	.quad	3300                    # 0xce4
	.quad	4533                    # 0x11b5
	.quad	5818                    # 0x16ba
	.quad	7154                    # 0x1bf2
	.quad	8540                    # 0x215c
	.quad	9975                    # 0x26f7
	.quad	11455                   # 0x2cbf
	.quad	12980                   # 0x32b4
	.quad	14548                   # 0x38d4
	.quad	16155                   # 0x3f1b
	.quad	17799                   # 0x4587
	.quad	19478                   # 0x4c16
	.quad	21189                   # 0x52c5
	.quad	22929                   # 0x5991
	.quad	24694                   # 0x6076
	.quad	26482                   # 0x6772
	.quad	28289                   # 0x6e81
	.quad	30112                   # 0x75a0
	.quad	31947                   # 0x7ccb
	.quad	33791                   # 0x83ff
	.quad	35640                   # 0x8b38
	.quad	37489                   # 0x9271
	.quad	39336                   # 0x99a8
	.quad	41176                   # 0xa0d8
	.quad	43006                   # 0xa7fe
	.quad	44821                   # 0xaf15
	.quad	46617                   # 0xb619
	.quad	48390                   # 0xbd06
	.quad	50137                   # 0xc3d9
	.quad	51853                   # 0xca8d
	.quad	53534                   # 0xd11e
	.quad	55178                   # 0xd78a
	.quad	56778                   # 0xddca
	.quad	58333                   # 0xe3dd
	.quad	59838                   # 0xe9be
	.quad	61289                   # 0xef69
	.quad	62684                   # 0xf4dc
	.quad	64019                   # 0xfa13
	.quad	65290                   # 0xff0a
	.quad	66494                   # 0x103be
	.quad	67629                   # 0x1082d
	.quad	68692                   # 0x10c54
	.quad	69679                   # 0x1102f
	.quad	70590                   # 0x113be
	.quad	71420                   # 0x116fc
	.quad	72169                   # 0x119e9
	.quad	72835                   # 0x11c83
	.quad	73415                   # 0x11ec7
	.quad	73908                   # 0x120b4
	.quad	74313                   # 0x12249
	.quad	74630                   # 0x12386
	.quad	74856                   # 0x12468
	.quad	74992                   # 0x124f0
	.quad	75038                   # 0x1251e
	.size	intwinbase, 2056


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
