	.text
	.file	"layer3.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	-4625196817309499392    # double -0.25
.LCPI0_1:
	.quad	4608683618675807573     # double 1.3333333333333333
.LCPI0_10:
	.quad	4586449032370930823     # double 0.043633231299858237
.LCPI0_11:
	.quad	4602678819172646912     # double 0.5
.LCPI0_12:
	.quad	4614256656552045848     # double 3.1415926535897931
.LCPI0_13:
	.quad	4634766966517661696     # double 72
.LCPI0_14:
	.quad	4593884178791887717     # double 0.1308996938995747
.LCPI0_24:
	.quad	4627448617123184640     # double 24
.LCPI0_25:
	.quad	9218868437227405312     # double +Inf
.LCPI0_26:
	.quad	-4503599627370496       # double -Inf
.LCPI0_28:
	.quad	4622945017495814144     # double 12
.LCPI0_29:
	.quad	4607182418800017408     # double 1
.LCPI0_30:
	.quad	4609047870845172685     # double 1.4142135623730951
.LCPI0_31:
	.quad	4605749341110064045     # double 0.8408964152537145
.LCPI0_32:
	.quad	4604544271217802189     # double 0.70710678118654757
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_2:
	.quad	4605898829186700118     # double 0.85749292571254432
	.quad	4606117245406390284     # double 0.88174199731770519
.LCPI0_3:
	.quad	-4620562651524645171    # double -0.51449575542752657
	.quad	-4621202449265478048    # double -0.47173196856497235
.LCPI0_4:
	.quad	4606728714005755246     # double 0.94962864910273281
	.quad	4607032130009944411     # double 0.9833145924917902
.LCPI0_5:
	.quad	-4624055110592953860    # double -0.31337745420390184
	.quad	-4627649902820386068    # double -0.18191319961098118
.LCPI0_6:
	.quad	4607142046876241755     # double 0.99551781606758582
	.quad	4607174857780265220     # double 0.99916055817814752
.LCPI0_7:
	.quad	-4631892827420077222    # double -0.094574192526420658
	.quad	-4637307453136828720    # double -0.040965582885304053
.LCPI0_8:
	.quad	4607181510831498318     # double 0.99989919524444715
	.quad	4607182357146371538     # double 0.9999931550702803
.LCPI0_9:
	.quad	-4644033497552803903    # double -0.01419856857247115
	.quad	-4652694053592706060    # double -0.0036999746737600373
.LCPI0_15:
	.quad	4607182418800017408     # double 1
	.quad	4607045579204271127     # double 0.98480775301220802
.LCPI0_16:
	.quad	4606639218218904860     # double 0.93969262078590842
	.quad	4605975682916830379     # double 0.86602540378443871
.LCPI0_17:
	.quad	4605075134482436153     # double 0.76604444311897801
	.quad	4603964935624201757     # double 0.64278760968653936
.LCPI0_18:
	.quad	4602678819172646913     # double 0.50000000000000011
	.quad	4599832907078044663     # double 0.34202014332566882
.LCPI0_19:
	.quad	4602696022191583747     # double 0.50190991877167368
	.quad	4602837688965596816     # double 0.51763809020504148
.LCPI0_20:
	.quad	4603144391929964727     # double 0.55168895948124586
	.quad	4603673099528325880     # double 0.61038729438072803
.LCPI0_21:
	.quad	4604544271217802188     # double 0.70710678118654746
	.quad	4606027005884375877     # double 0.87172339781054886
.LCPI0_22:
	.quad	4608007031456731447     # double 1.1831007915762493
	.quad	4611379105555332878     # double 1.9318516525781351
.LCPI0_23:
	.quad	4602837688965596816     # double 0.51763809020504148
	.quad	4604544271217802188     # double 0.70710678118654746
.LCPI0_27:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI0_33:
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
	.long	7                       # 0x7
.LCPI0_34:
	.long	954437177               # 0x38e38e39
	.long	954437177               # 0x38e38e39
	.long	954437177               # 0x38e38e39
	.long	954437177               # 0x38e38e39
.LCPI0_35:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI0_36:
	.long	12288                   # 0x3000
	.long	12352                   # 0x3040
	.long	12416                   # 0x3080
	.long	12480                   # 0x30c0
.LCPI0_37:
	.long	12544                   # 0x3100
	.long	12608                   # 0x3140
	.long	12296                   # 0x3008
	.long	12360                   # 0x3048
.LCPI0_38:
	.long	12424                   # 0x3088
	.long	12488                   # 0x30c8
	.long	12552                   # 0x3108
	.long	12616                   # 0x3148
.LCPI0_39:
	.long	12304                   # 0x3010
	.long	12368                   # 0x3050
	.long	12432                   # 0x3090
	.long	12496                   # 0x30d0
.LCPI0_40:
	.long	12560                   # 0x3110
	.long	12624                   # 0x3150
	.long	12312                   # 0x3018
	.long	12376                   # 0x3058
.LCPI0_41:
	.long	12440                   # 0x3098
	.long	12504                   # 0x30d8
	.long	12568                   # 0x3118
	.long	12632                   # 0x3158
.LCPI0_42:
	.long	12320                   # 0x3020
	.long	12384                   # 0x3060
	.long	12448                   # 0x30a0
	.long	12512                   # 0x30e0
.LCPI0_43:
	.long	12576                   # 0x3120
	.long	12640                   # 0x3160
	.long	12328                   # 0x3028
	.long	12392                   # 0x3068
.LCPI0_44:
	.long	12456                   # 0x30a8
	.long	12520                   # 0x30e8
	.long	12584                   # 0x3128
	.long	12648                   # 0x3168
.LCPI0_45:
	.long	12289                   # 0x3001
	.long	12353                   # 0x3041
	.long	12417                   # 0x3081
	.long	12481                   # 0x30c1
.LCPI0_46:
	.long	12545                   # 0x3101
	.long	12609                   # 0x3141
	.long	12297                   # 0x3009
	.long	12361                   # 0x3049
.LCPI0_47:
	.long	12425                   # 0x3089
	.long	12489                   # 0x30c9
	.long	12553                   # 0x3109
	.long	12617                   # 0x3149
.LCPI0_48:
	.long	12305                   # 0x3011
	.long	12369                   # 0x3051
	.long	12433                   # 0x3091
	.long	12497                   # 0x30d1
.LCPI0_49:
	.long	12561                   # 0x3111
	.long	12625                   # 0x3151
	.long	12313                   # 0x3019
	.long	12377                   # 0x3059
.LCPI0_50:
	.long	12441                   # 0x3099
	.long	12505                   # 0x30d9
	.long	12569                   # 0x3119
	.long	12633                   # 0x3159
.LCPI0_51:
	.long	12321                   # 0x3021
	.long	12385                   # 0x3061
	.long	12449                   # 0x30a1
	.long	12513                   # 0x30e1
.LCPI0_52:
	.long	12577                   # 0x3121
	.long	12641                   # 0x3161
	.long	12329                   # 0x3029
	.long	12393                   # 0x3069
.LCPI0_53:
	.long	12457                   # 0x30a9
	.long	12521                   # 0x30e9
	.long	12585                   # 0x3129
	.long	12649                   # 0x3169
.LCPI0_54:
	.long	12290                   # 0x3002
	.long	12354                   # 0x3042
	.long	12418                   # 0x3082
	.long	12482                   # 0x30c2
.LCPI0_55:
	.long	12546                   # 0x3102
	.long	12610                   # 0x3142
	.long	12298                   # 0x300a
	.long	12362                   # 0x304a
.LCPI0_56:
	.long	12426                   # 0x308a
	.long	12490                   # 0x30ca
	.long	12554                   # 0x310a
	.long	12618                   # 0x314a
.LCPI0_57:
	.long	12306                   # 0x3012
	.long	12370                   # 0x3052
	.long	12434                   # 0x3092
	.long	12498                   # 0x30d2
.LCPI0_58:
	.long	12562                   # 0x3112
	.long	12626                   # 0x3152
	.long	12314                   # 0x301a
	.long	12378                   # 0x305a
.LCPI0_59:
	.long	12442                   # 0x309a
	.long	12506                   # 0x30da
	.long	12570                   # 0x311a
	.long	12634                   # 0x315a
.LCPI0_60:
	.long	12322                   # 0x3022
	.long	12386                   # 0x3062
	.long	12450                   # 0x30a2
	.long	12514                   # 0x30e2
.LCPI0_61:
	.long	12578                   # 0x3122
	.long	12642                   # 0x3162
	.long	12330                   # 0x302a
	.long	12394                   # 0x306a
.LCPI0_62:
	.long	12458                   # 0x30aa
	.long	12522                   # 0x30ea
	.long	12586                   # 0x312a
	.long	12650                   # 0x316a
.LCPI0_63:
	.long	12291                   # 0x3003
	.long	12355                   # 0x3043
	.long	12419                   # 0x3083
	.long	12483                   # 0x30c3
.LCPI0_64:
	.long	12547                   # 0x3103
	.long	12611                   # 0x3143
	.long	12299                   # 0x300b
	.long	12363                   # 0x304b
.LCPI0_65:
	.long	12427                   # 0x308b
	.long	12491                   # 0x30cb
	.long	12555                   # 0x310b
	.long	12619                   # 0x314b
.LCPI0_66:
	.long	12307                   # 0x3013
	.long	12371                   # 0x3053
	.long	12435                   # 0x3093
	.long	12499                   # 0x30d3
.LCPI0_67:
	.long	12563                   # 0x3113
	.long	12627                   # 0x3153
	.long	12315                   # 0x301b
	.long	12379                   # 0x305b
.LCPI0_68:
	.long	12443                   # 0x309b
	.long	12507                   # 0x30db
	.long	12571                   # 0x311b
	.long	12635                   # 0x315b
.LCPI0_69:
	.long	12323                   # 0x3023
	.long	12387                   # 0x3063
	.long	12451                   # 0x30a3
	.long	12515                   # 0x30e3
.LCPI0_70:
	.long	12579                   # 0x3123
	.long	12643                   # 0x3163
	.long	12331                   # 0x302b
	.long	12395                   # 0x306b
.LCPI0_71:
	.long	12459                   # 0x30ab
	.long	12523                   # 0x30eb
	.long	12587                   # 0x312b
	.long	12651                   # 0x316b
.LCPI0_72:
	.long	12292                   # 0x3004
	.long	12356                   # 0x3044
	.long	12420                   # 0x3084
	.long	12484                   # 0x30c4
.LCPI0_73:
	.long	12548                   # 0x3104
	.long	12612                   # 0x3144
	.long	12300                   # 0x300c
	.long	12364                   # 0x304c
.LCPI0_74:
	.long	12428                   # 0x308c
	.long	12492                   # 0x30cc
	.long	12556                   # 0x310c
	.long	12620                   # 0x314c
.LCPI0_75:
	.long	12308                   # 0x3014
	.long	12372                   # 0x3054
	.long	12436                   # 0x3094
	.long	12500                   # 0x30d4
.LCPI0_76:
	.long	12564                   # 0x3114
	.long	12628                   # 0x3154
	.long	12316                   # 0x301c
	.long	12380                   # 0x305c
.LCPI0_77:
	.long	12444                   # 0x309c
	.long	12508                   # 0x30dc
	.long	12572                   # 0x311c
	.long	12636                   # 0x315c
.LCPI0_78:
	.long	12324                   # 0x3024
	.long	12388                   # 0x3064
	.long	12452                   # 0x30a4
	.long	12516                   # 0x30e4
.LCPI0_79:
	.long	12580                   # 0x3124
	.long	12644                   # 0x3164
	.long	12332                   # 0x302c
	.long	12396                   # 0x306c
.LCPI0_80:
	.long	12460                   # 0x30ac
	.long	12524                   # 0x30ec
	.long	12588                   # 0x312c
	.long	12652                   # 0x316c
.LCPI0_81:
	.long	16384                   # 0x4000
	.long	16448                   # 0x4040
	.long	16512                   # 0x4080
	.long	16576                   # 0x40c0
.LCPI0_82:
	.long	16392                   # 0x4008
	.long	16456                   # 0x4048
	.long	16520                   # 0x4088
	.long	16584                   # 0x40c8
.LCPI0_83:
	.long	16400                   # 0x4010
	.long	16464                   # 0x4050
	.long	16528                   # 0x4090
	.long	16592                   # 0x40d0
.LCPI0_84:
	.long	16408                   # 0x4018
	.long	16472                   # 0x4058
	.long	16536                   # 0x4098
	.long	16600                   # 0x40d8
.LCPI0_85:
	.long	16385                   # 0x4001
	.long	16449                   # 0x4041
	.long	16513                   # 0x4081
	.long	16577                   # 0x40c1
.LCPI0_86:
	.long	16393                   # 0x4009
	.long	16457                   # 0x4049
	.long	16521                   # 0x4089
	.long	16585                   # 0x40c9
.LCPI0_87:
	.long	16401                   # 0x4011
	.long	16465                   # 0x4051
	.long	16529                   # 0x4091
	.long	16593                   # 0x40d1
.LCPI0_88:
	.long	16409                   # 0x4019
	.long	16473                   # 0x4059
	.long	16537                   # 0x4099
	.long	16601                   # 0x40d9
.LCPI0_89:
	.long	16386                   # 0x4002
	.long	16450                   # 0x4042
	.long	16514                   # 0x4082
	.long	16578                   # 0x40c2
.LCPI0_90:
	.long	16394                   # 0x400a
	.long	16458                   # 0x404a
	.long	16522                   # 0x408a
	.long	16586                   # 0x40ca
.LCPI0_91:
	.long	16402                   # 0x4012
	.long	16466                   # 0x4052
	.long	16530                   # 0x4092
	.long	16594                   # 0x40d2
.LCPI0_92:
	.long	16410                   # 0x401a
	.long	16474                   # 0x405a
	.long	16538                   # 0x409a
	.long	16602                   # 0x40da
.LCPI0_93:
	.long	16387                   # 0x4003
	.long	16451                   # 0x4043
	.long	16515                   # 0x4083
	.long	16579                   # 0x40c3
.LCPI0_94:
	.long	16395                   # 0x400b
	.long	16459                   # 0x404b
	.long	16523                   # 0x408b
	.long	16587                   # 0x40cb
.LCPI0_95:
	.long	16403                   # 0x4013
	.long	16467                   # 0x4053
	.long	16531                   # 0x4093
	.long	16595                   # 0x40d3
.LCPI0_96:
	.long	16411                   # 0x401b
	.long	16475                   # 0x405b
	.long	16539                   # 0x409b
	.long	16603                   # 0x40db
.LCPI0_97:
	.long	20480                   # 0x5000
	.long	20488                   # 0x5008
	.long	20496                   # 0x5010
	.long	20481                   # 0x5001
.LCPI0_98:
	.long	40960                   # 0xa000
	.long	40968                   # 0xa008
	.long	40976                   # 0xa010
	.long	40961                   # 0xa001
.LCPI0_99:
	.long	20489                   # 0x5009
	.long	20497                   # 0x5011
	.long	20482                   # 0x5002
	.long	20490                   # 0x500a
.LCPI0_100:
	.long	40969                   # 0xa009
	.long	40977                   # 0xa011
	.long	40962                   # 0xa002
	.long	40970                   # 0xa00a
.LCPI0_101:
	.long	20498                   # 0x5012
	.long	20483                   # 0x5003
	.long	20491                   # 0x500b
	.long	20499                   # 0x5013
.LCPI0_102:
	.long	40978                   # 0xa012
	.long	40963                   # 0xa003
	.long	40971                   # 0xa00b
	.long	40979                   # 0xa013
.LCPI0_103:
	.long	512                     # 0x200
	.long	1024                    # 0x400
	.long	1536                    # 0x600
	.long	64                      # 0x40
.LCPI0_104:
	.long	576                     # 0x240
	.long	1088                    # 0x440
	.long	1600                    # 0x640
	.long	128                     # 0x80
.LCPI0_105:
	.long	640                     # 0x280
	.long	1152                    # 0x480
	.long	1664                    # 0x680
	.long	192                     # 0xc0
.LCPI0_106:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI0_107:
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
	.long	16                      # 0x10
.LCPI0_108:
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
	.long	24                      # 0x18
.LCPI0_109:
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
	.long	32                      # 0x20
.LCPI0_110:
	.long	4096                    # 0x1000
	.long	4160                    # 0x1040
	.long	4224                    # 0x1080
	.long	4288                    # 0x10c0
.LCPI0_111:
	.long	4104                    # 0x1008
	.long	4168                    # 0x1048
	.long	4232                    # 0x1088
	.long	4296                    # 0x10c8
.LCPI0_112:
	.long	4112                    # 0x1010
	.long	4176                    # 0x1050
	.long	4240                    # 0x1090
	.long	4304                    # 0x10d0
.LCPI0_113:
	.long	4120                    # 0x1018
	.long	4184                    # 0x1058
	.long	4248                    # 0x1098
	.long	4312                    # 0x10d8
.LCPI0_114:
	.long	4128                    # 0x1020
	.long	4192                    # 0x1060
	.long	4256                    # 0x10a0
	.long	4320                    # 0x10e0
.LCPI0_115:
	.long	4097                    # 0x1001
	.long	4161                    # 0x1041
	.long	4225                    # 0x1081
	.long	4289                    # 0x10c1
.LCPI0_116:
	.long	4105                    # 0x1009
	.long	4169                    # 0x1049
	.long	4233                    # 0x1089
	.long	4297                    # 0x10c9
.LCPI0_117:
	.long	4113                    # 0x1011
	.long	4177                    # 0x1051
	.long	4241                    # 0x1091
	.long	4305                    # 0x10d1
.LCPI0_118:
	.long	4121                    # 0x1019
	.long	4185                    # 0x1059
	.long	4249                    # 0x1099
	.long	4313                    # 0x10d9
.LCPI0_119:
	.long	4129                    # 0x1021
	.long	4193                    # 0x1061
	.long	4257                    # 0x10a1
	.long	4321                    # 0x10e1
.LCPI0_120:
	.long	4098                    # 0x1002
	.long	4162                    # 0x1042
	.long	4226                    # 0x1082
	.long	4290                    # 0x10c2
.LCPI0_121:
	.long	4106                    # 0x100a
	.long	4170                    # 0x104a
	.long	4234                    # 0x108a
	.long	4298                    # 0x10ca
.LCPI0_122:
	.long	4114                    # 0x1012
	.long	4178                    # 0x1052
	.long	4242                    # 0x1092
	.long	4306                    # 0x10d2
.LCPI0_123:
	.long	4122                    # 0x101a
	.long	4186                    # 0x105a
	.long	4250                    # 0x109a
	.long	4314                    # 0x10da
.LCPI0_124:
	.long	4130                    # 0x1022
	.long	4194                    # 0x1062
	.long	4258                    # 0x10a2
	.long	4322                    # 0x10e2
.LCPI0_125:
	.long	4099                    # 0x1003
	.long	4163                    # 0x1043
	.long	4227                    # 0x1083
	.long	4291                    # 0x10c3
.LCPI0_126:
	.long	4107                    # 0x100b
	.long	4171                    # 0x104b
	.long	4235                    # 0x108b
	.long	4299                    # 0x10cb
.LCPI0_127:
	.long	4115                    # 0x1013
	.long	4179                    # 0x1053
	.long	4243                    # 0x1093
	.long	4307                    # 0x10d3
.LCPI0_128:
	.long	4123                    # 0x101b
	.long	4187                    # 0x105b
	.long	4251                    # 0x109b
	.long	4315                    # 0x10db
.LCPI0_129:
	.long	4131                    # 0x1023
	.long	4195                    # 0x1063
	.long	4259                    # 0x10a3
	.long	4323                    # 0x10e3
.LCPI0_130:
	.long	4100                    # 0x1004
	.long	4164                    # 0x1044
	.long	4228                    # 0x1084
	.long	4292                    # 0x10c4
.LCPI0_131:
	.long	4108                    # 0x100c
	.long	4172                    # 0x104c
	.long	4236                    # 0x108c
	.long	4300                    # 0x10cc
.LCPI0_132:
	.long	4116                    # 0x1014
	.long	4180                    # 0x1054
	.long	4244                    # 0x1094
	.long	4308                    # 0x10d4
.LCPI0_133:
	.long	4124                    # 0x101c
	.long	4188                    # 0x105c
	.long	4252                    # 0x109c
	.long	4316                    # 0x10dc
.LCPI0_134:
	.long	4132                    # 0x1024
	.long	4196                    # 0x1064
	.long	4260                    # 0x10a4
	.long	4324                    # 0x10e4
	.text
	.globl	init_layer3
	.p2align	4, 0x90
	.type	init_layer3,@function
init_layer3:                            # @init_layer3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%edi, %r15d
	movq	$-3024, %rbp            # imm = 0xF430
	movl	$-46, %ebx
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	exp2
	movsd	%xmm0, gainpow2+3024(%rbp)
	leal	1(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	callq	exp2
	movsd	%xmm0, gainpow2+3032(%rbp)
	leal	2(%rbx), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_0(%rip), %xmm0
	callq	exp2
	movsd	%xmm0, gainpow2+3040(%rbp)
	addl	$3, %ebx
	addq	$24, %rbp
	jne	.LBB0_1
# BB#2:                                 # %.preheader373.preheader
	xorl	%ebp, %ebp
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_48:                               # %.preheader373.3
                                        #   in Loop: Header=BB0_3 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, ispow+24(,%rbp,8)
	incq	%rbx
	movq	%rbx, %rbp
.LBB0_3:                                # %.preheader373
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, ispow(,%rbp,8)
	leal	1(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, ispow+8(,%rbp,8)
	leal	2(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, ispow+16(,%rbp,8)
	leaq	3(%rbp), %rbx
	cmpq	$8207, %rbx             # imm = 0x200F
	jne	.LBB0_48
# BB#4:                                 # %.preheader372.preheader
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [8.574929e-01,8.817420e-01]
	movaps	%xmm0, aa_cs(%rip)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [-5.144958e-01,-4.717320e-01]
	movaps	%xmm0, aa_ca(%rip)
	movaps	.LCPI0_4(%rip), %xmm0   # xmm0 = [9.496286e-01,9.833146e-01]
	movaps	%xmm0, aa_cs+16(%rip)
	movaps	.LCPI0_5(%rip), %xmm0   # xmm0 = [-3.133775e-01,-1.819132e-01]
	movaps	%xmm0, aa_ca+16(%rip)
	movaps	.LCPI0_6(%rip), %xmm0   # xmm0 = [9.955178e-01,9.991606e-01]
	movaps	%xmm0, aa_cs+32(%rip)
	movaps	.LCPI0_7(%rip), %xmm0   # xmm0 = [-9.457419e-02,-4.096558e-02]
	movaps	%xmm0, aa_ca+32(%rip)
	movaps	.LCPI0_8(%rip), %xmm0   # xmm0 = [9.998992e-01,9.999932e-01]
	movaps	%xmm0, aa_cs+48(%rip)
	movapd	.LCPI0_9(%rip), %xmm0   # xmm0 = [-1.419857e-02,-3.699975e-03]
	movapd	%xmm0, aa_ca+48(%rip)
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader371
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_10(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	sin
	movapd	%xmm0, %xmm1
	movsd	.LCPI0_11(%rip), %xmm0  # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	leal	19(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_12(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_13(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm1, %xmm0
	callq	cos
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, win+288(,%rbp,4)
	movsd	%xmm1, win(,%rbp,4)
	leal	37(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_10(%rip), %xmm0
	callq	sin
	mulsd	.LCPI0_11(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	leal	55(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_12(%rip), %xmm0
	divsd	.LCPI0_13(%rip), %xmm0
	callq	cos
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, win+1008(,%rbp,4)
	movsd	%xmm1, win+144(,%rbp,4)
	addq	$2, %rbp
	cmpq	$36, %rbp
	jne	.LBB0_5
# BB#6:                                 # %.preheader370.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader370
                                        # =>This Inner Loop Header: Depth=1
	leal	55(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_12(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_13(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm1, %xmm0
	callq	cos
	movsd	.LCPI0_11(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, win+432(,%rbp,4)
	leal	43(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_12(%rip), %xmm0
	divsd	.LCPI0_13(%rip), %xmm0
	callq	cos
	movsd	.LCPI0_11(%rip), %xmm1  # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, win+960(,%rbp,4)
	leal	13(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	callq	sin
	mulsd	.LCPI0_11(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	leal	67(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_12(%rip), %xmm0
	divsd	.LCPI0_13(%rip), %xmm0
	callq	cos
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, win+480(,%rbp,4)
	movq	$0, win+864(,%rbp,4)
	movq	$0, win+528(,%rbp,4)
	leal	1(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_14(%rip), %xmm0
	callq	sin
	mulsd	.LCPI0_11(%rip), %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	leal	31(%rbp), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI0_12(%rip), %xmm0
	divsd	.LCPI0_13(%rip), %xmm0
	callq	cos
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, win+912(,%rbp,4)
	addq	$2, %rbp
	cmpq	$12, %rbp
	jne	.LBB0_7
# BB#8:                                 # %.preheader369.preheader
	movaps	.LCPI0_15(%rip), %xmm0  # xmm0 = [1.000000e+00,9.848078e-01]
	movaps	%xmm0, COS9(%rip)
	movaps	.LCPI0_16(%rip), %xmm0  # xmm0 = [9.396926e-01,8.660254e-01]
	movaps	%xmm0, COS9+16(%rip)
	movaps	.LCPI0_17(%rip), %xmm0  # xmm0 = [7.660444e-01,6.427876e-01]
	movaps	%xmm0, COS9+32(%rip)
	movaps	.LCPI0_18(%rip), %xmm0  # xmm0 = [5.000000e-01,3.420201e-01]
	movaps	%xmm0, COS9+48(%rip)
	movabsq	$4595424355236410252, %rax # imm = 0x3FC63A1A7E0B738C
	movq	%rax, COS9+64(%rip)
	movaps	.LCPI0_19(%rip), %xmm0  # xmm0 = [5.019099e-01,5.176381e-01]
	movaps	%xmm0, tfcos36(%rip)
	movaps	.LCPI0_20(%rip), %xmm0  # xmm0 = [5.516890e-01,6.103873e-01]
	movaps	%xmm0, tfcos36+16(%rip)
	movaps	.LCPI0_21(%rip), %xmm0  # xmm0 = [7.071068e-01,8.717234e-01]
	movaps	%xmm0, tfcos36+32(%rip)
	movaps	.LCPI0_22(%rip), %xmm0  # xmm0 = [1.183101e+00,1.931852e+00]
	movaps	%xmm0, tfcos36+48(%rip)
	movabsq	$4618145144764607242, %rax # imm = 0x4016F28A8AE3AB0A
	movq	%rax, tfcos36+64(%rip)
	movapd	.LCPI0_23(%rip), %xmm0  # xmm0 = [5.176381e-01,7.071068e-01]
	movapd	%xmm0, tfcos12(%rip)
	movabsq	$4611379105555332886, %rax # imm = 0x3FFEE8DD4748BF16
	movq	%rax, tfcos12+16(%rip)
	movabsq	$4605975682916830379, %rax # imm = 0x3FEBB67AE8584CAB
	movq	%rax, COS6_1(%rip)
	movabsq	$4602678819172646913, %rax # imm = 0x3FE0000000000001
	movq	%rax, COS6_2(%rip)
	movl	$77, %eax
	movl	$63, %ebx
	movl	$49, %r12d
	movl	$35, %r13d
	movl	$21, %ebp
	xorl	%r14d, %r14d
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	jmp	.LBB0_9
.LBB0_11:                               # %cdce.call
                                        #   in Loop: Header=BB0_9 Depth=1
	callq	cos
	movsd	.LCPI0_26(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI0_25(%rip), %xmm2  # xmm2 = mem[0],zero
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	jmp	.LBB0_12
.LBB0_14:                               # %cdce.call.1
                                        #   in Loop: Header=BB0_9 Depth=1
	callq	cos
	movsd	.LCPI0_26(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI0_25(%rip), %xmm2  # xmm2 = mem[0],zero
	movl	(%rsp), %eax            # 4-byte Reload
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	jmp	.LBB0_15
.LBB0_17:                               # %cdce.call.2
                                        #   in Loop: Header=BB0_9 Depth=1
	callq	cos
	movsd	.LCPI0_26(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI0_25(%rip), %xmm2  # xmm2 = mem[0],zero
	movl	(%rsp), %eax            # 4-byte Reload
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	jmp	.LBB0_18
.LBB0_20:                               # %cdce.call.3
                                        #   in Loop: Header=BB0_9 Depth=1
	callq	cos
	movsd	.LCPI0_26(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI0_25(%rip), %xmm2  # xmm2 = mem[0],zero
	movl	(%rsp), %eax            # 4-byte Reload
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	jmp	.LBB0_21
.LBB0_23:                               # %cdce.call.4
                                        #   in Loop: Header=BB0_9 Depth=1
	callq	cos
	movsd	.LCPI0_26(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI0_25(%rip), %xmm2  # xmm2 = mem[0],zero
	movl	(%rsp), %eax            # 4-byte Reload
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	jmp	.LBB0_24
.LBB0_26:                               # %cdce.call.5
                                        #   in Loop: Header=BB0_9 Depth=1
	callq	cos
	movl	(%rsp), %eax            # 4-byte Reload
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movl	%eax, (%rsp)            # 4-byte Spill
	leal	1(%r14), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	callq	sin
	mulsd	.LCPI0_11(%rip), %xmm0
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	leal	7(%r14), %eax
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	mulsd	.LCPI0_12(%rip), %xmm0
	divsd	.LCPI0_24(%rip), %xmm0
	callq	cos
	movsd	.LCPI0_26(%rip), %xmm3  # xmm3 = mem[0],zero
	movsd	.LCPI0_14(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	16(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	divsd	%xmm0, %xmm2
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm2, win+576(,%r14,4)
	mulsd	%xmm1, %xmm0
	movsd	.LCPI0_25(%rip), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	jae	.LBB0_11
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	ucomisd	%xmm0, %xmm3
	jae	.LBB0_11
.LBB0_12:                               # %cdce.end
                                        #   in Loop: Header=BB0_9 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	mulsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	movl	(%rsp), %eax            # 4-byte Reload
	jae	.LBB0_14
# BB#13:                                # %cdce.end
                                        #   in Loop: Header=BB0_9 Depth=1
	ucomisd	%xmm0, %xmm3
	jae	.LBB0_14
.LBB0_15:                               # %cdce.end.1
                                        #   in Loop: Header=BB0_9 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r13d, %xmm0
	mulsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	jae	.LBB0_17
# BB#16:                                # %cdce.end.1
                                        #   in Loop: Header=BB0_9 Depth=1
	ucomisd	%xmm0, %xmm3
	jae	.LBB0_17
.LBB0_18:                               # %cdce.end.2
                                        #   in Loop: Header=BB0_9 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	mulsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	jae	.LBB0_20
# BB#19:                                # %cdce.end.2
                                        #   in Loop: Header=BB0_9 Depth=1
	ucomisd	%xmm0, %xmm3
	jae	.LBB0_20
.LBB0_21:                               # %cdce.end.3
                                        #   in Loop: Header=BB0_9 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	mulsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	jae	.LBB0_23
# BB#22:                                # %cdce.end.3
                                        #   in Loop: Header=BB0_9 Depth=1
	ucomisd	%xmm0, %xmm3
	jae	.LBB0_23
.LBB0_24:                               # %cdce.end.4
                                        #   in Loop: Header=BB0_9 Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	ucomisd	%xmm2, %xmm0
	jae	.LBB0_26
# BB#25:                                # %cdce.end.4
                                        #   in Loop: Header=BB0_9 Depth=1
	ucomisd	%xmm0, %xmm3
	jae	.LBB0_26
.LBB0_27:                               # %cdce.end.5
                                        #   in Loop: Header=BB0_9 Depth=1
	addq	$2, %r14
	addl	$22, %eax
	addl	$18, %ebx
	addl	$14, %r12d
	addl	$10, %r13d
	addl	$6, %ebp
	cmpq	$24, %r14
	jne	.LBB0_9
# BB#28:                                # %.lr.ph418.3
	movq	win(%rip), %rax
	movq	%rax, win1(%rip)
	movq	win+16(%rip), %rax
	movq	%rax, win1+16(%rip)
	movq	win+32(%rip), %rax
	movq	%rax, win1+32(%rip)
	movq	win+48(%rip), %rax
	movq	%rax, win1+48(%rip)
	movq	win+64(%rip), %rax
	movq	%rax, win1+64(%rip)
	movq	win+80(%rip), %rax
	movq	%rax, win1+80(%rip)
	movq	win+96(%rip), %rax
	movq	%rax, win1+96(%rip)
	movq	win+112(%rip), %rax
	movq	%rax, win1+112(%rip)
	movq	win+128(%rip), %rax
	movq	%rax, win1+128(%rip)
	movq	win+144(%rip), %rax
	movq	%rax, win1+144(%rip)
	movq	win+160(%rip), %rax
	movq	%rax, win1+160(%rip)
	movq	win+176(%rip), %rax
	movq	%rax, win1+176(%rip)
	movq	win+192(%rip), %rax
	movq	%rax, win1+192(%rip)
	movq	win+208(%rip), %rax
	movq	%rax, win1+208(%rip)
	movq	win+224(%rip), %rax
	movq	%rax, win1+224(%rip)
	movq	win+240(%rip), %rax
	movq	%rax, win1+240(%rip)
	movq	win+256(%rip), %rax
	movq	%rax, win1+256(%rip)
	movq	win+272(%rip), %rax
	movq	%rax, win1+272(%rip)
	movsd	win+8(%rip), %xmm1      # xmm1 = mem[0],zero
	movapd	.LCPI0_27(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00]
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+8(%rip)
	movsd	win+24(%rip), %xmm1     # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+24(%rip)
	movsd	win+40(%rip), %xmm1     # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+40(%rip)
	movsd	win+56(%rip), %xmm1     # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+56(%rip)
	movsd	win+72(%rip), %xmm1     # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+72(%rip)
	movsd	win+88(%rip), %xmm1     # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+88(%rip)
	movsd	win+104(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+104(%rip)
	movsd	win+120(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+120(%rip)
	movsd	win+136(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+136(%rip)
	movsd	win+152(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+152(%rip)
	movsd	win+168(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+168(%rip)
	movsd	win+184(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+184(%rip)
	movsd	win+200(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+200(%rip)
	movsd	win+216(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+216(%rip)
	movsd	win+232(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+232(%rip)
	movsd	win+248(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+248(%rip)
	movsd	win+264(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+264(%rip)
	movsd	win+280(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+280(%rip)
	movq	win+288(%rip), %rax
	movq	%rax, win1+288(%rip)
	movq	win+304(%rip), %rax
	movq	%rax, win1+304(%rip)
	movq	win+320(%rip), %rax
	movq	%rax, win1+320(%rip)
	movq	win+336(%rip), %rax
	movq	%rax, win1+336(%rip)
	movq	win+352(%rip), %rax
	movq	%rax, win1+352(%rip)
	movq	win+368(%rip), %rax
	movq	%rax, win1+368(%rip)
	movq	win+384(%rip), %rax
	movq	%rax, win1+384(%rip)
	movq	win+400(%rip), %rax
	movq	%rax, win1+400(%rip)
	movq	win+416(%rip), %rax
	movq	%rax, win1+416(%rip)
	movq	win+432(%rip), %rax
	movq	%rax, win1+432(%rip)
	movq	win+448(%rip), %rax
	movq	%rax, win1+448(%rip)
	movq	win+464(%rip), %rax
	movq	%rax, win1+464(%rip)
	movq	win+480(%rip), %rax
	movq	%rax, win1+480(%rip)
	movq	win+496(%rip), %rax
	movq	%rax, win1+496(%rip)
	movq	win+512(%rip), %rax
	movq	%rax, win1+512(%rip)
	movq	win+528(%rip), %rax
	movq	%rax, win1+528(%rip)
	movq	win+544(%rip), %rax
	movq	%rax, win1+544(%rip)
	movq	win+560(%rip), %rax
	movq	%rax, win1+560(%rip)
	movsd	win+296(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+296(%rip)
	movsd	win+312(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+312(%rip)
	movsd	win+328(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+328(%rip)
	movsd	win+344(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+344(%rip)
	movsd	win+360(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+360(%rip)
	movsd	win+376(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+376(%rip)
	movsd	win+392(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+392(%rip)
	movsd	win+408(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+408(%rip)
	movsd	win+424(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+424(%rip)
	movsd	win+440(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+440(%rip)
	movsd	win+456(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+456(%rip)
	movsd	win+472(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+472(%rip)
	movsd	win+488(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+488(%rip)
	movsd	win+504(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+504(%rip)
	movsd	win+520(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+520(%rip)
	movsd	win+536(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+536(%rip)
	movsd	win+552(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+552(%rip)
	movsd	win+568(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+568(%rip)
	movq	win+576(%rip), %rax
	movq	%rax, win1+576(%rip)
	movq	win+592(%rip), %rax
	movq	%rax, win1+592(%rip)
	movq	win+608(%rip), %rax
	movq	%rax, win1+608(%rip)
	movq	win+624(%rip), %rax
	movq	%rax, win1+624(%rip)
	movq	win+640(%rip), %rax
	movq	%rax, win1+640(%rip)
	movq	win+656(%rip), %rax
	movq	%rax, win1+656(%rip)
	movsd	win+584(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+584(%rip)
	movsd	win+600(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+600(%rip)
	movsd	win+616(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+616(%rip)
	movsd	win+632(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+632(%rip)
	movsd	win+648(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+648(%rip)
	movsd	win+664(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+664(%rip)
	movq	win+864(%rip), %rax
	movq	%rax, win1+864(%rip)
	movq	win+880(%rip), %rax
	movq	%rax, win1+880(%rip)
	movq	win+896(%rip), %rax
	movq	%rax, win1+896(%rip)
	movq	win+912(%rip), %rax
	movq	%rax, win1+912(%rip)
	movq	win+928(%rip), %rax
	movq	%rax, win1+928(%rip)
	movq	win+944(%rip), %rax
	movq	%rax, win1+944(%rip)
	movq	win+960(%rip), %rax
	movq	%rax, win1+960(%rip)
	movq	win+976(%rip), %rax
	movq	%rax, win1+976(%rip)
	movq	win+992(%rip), %rax
	movq	%rax, win1+992(%rip)
	movq	win+1008(%rip), %rax
	movq	%rax, win1+1008(%rip)
	movq	win+1024(%rip), %rax
	movq	%rax, win1+1024(%rip)
	movq	win+1040(%rip), %rax
	movq	%rax, win1+1040(%rip)
	movq	win+1056(%rip), %rax
	movq	%rax, win1+1056(%rip)
	movq	win+1072(%rip), %rax
	movq	%rax, win1+1072(%rip)
	movq	win+1088(%rip), %rax
	movq	%rax, win1+1088(%rip)
	movq	win+1104(%rip), %rax
	movq	%rax, win1+1104(%rip)
	movq	win+1120(%rip), %rax
	movq	%rax, win1+1120(%rip)
	movq	win+1136(%rip), %rax
	movq	%rax, win1+1136(%rip)
	movsd	win+872(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+872(%rip)
	movsd	win+888(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+888(%rip)
	movsd	win+904(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+904(%rip)
	movsd	win+920(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+920(%rip)
	movsd	win+936(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+936(%rip)
	movsd	win+952(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+952(%rip)
	movsd	win+968(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+968(%rip)
	movsd	win+984(%rip), %xmm1    # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+984(%rip)
	movsd	win+1000(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1000(%rip)
	movsd	win+1016(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1016(%rip)
	movsd	win+1032(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1032(%rip)
	movsd	win+1048(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1048(%rip)
	movsd	win+1064(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1064(%rip)
	movsd	win+1080(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1080(%rip)
	movsd	win+1096(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1096(%rip)
	movsd	win+1112(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1112(%rip)
	movsd	win+1128(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1128(%rip)
	movsd	win+1144(%rip), %xmm1   # xmm1 = mem[0],zero
	xorpd	%xmm0, %xmm1
	movlpd	%xmm1, win1+1144(%rip)
	xorl	%ebp, %ebp
	movabsq	$4607182418800017408, %r14 # imm = 0x3FF0000000000000
	movabsq	$4609047870845172685, %rbx # imm = 0x3FF6A09E667F3BCD
	movsd	.LCPI0_12(%rip), %xmm1  # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB0_29:                               # %.preheader363
                                        # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebp, %xmm0
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	mulsd	%xmm1, %xmm0
	divsd	.LCPI0_28(%rip), %xmm0
	callq	tan
	movapd	%xmm0, %xmm1
	movsd	.LCPI0_29(%rip), %xmm2  # xmm2 = mem[0],zero
	movapd	%xmm2, %xmm3
	addsd	%xmm3, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, tan1_1(,%rbp,8)
	movapd	%xmm3, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, tan2_1(,%rbp,8)
	movsd	.LCPI0_30(%rip), %xmm2  # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, tan1_2(,%rbp,8)
	movapd	%xmm2, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, tan2_2(,%rbp,8)
	testq	%rbp, %rbp
	jle	.LBB0_30
# BB#31:                                # %.split.us
                                        #   in Loop: Header=BB0_29 Depth=1
	testb	$1, %bpl
	jne	.LBB0_32
# BB#33:                                # %.split.us.split.us.preheader
                                        #   in Loop: Header=BB0_29 Depth=1
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	.LCPI0_11(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	.LCPI0_31(%rip), %xmm0  # xmm0 = mem[0],zero
	callq	pow
	movq	%r14, pow1_1(,%rbp,8)
	movsd	%xmm0, pow2_1(,%rbp,8)
	movq	%rbx, pow1_2(,%rbp,8)
	mulsd	.LCPI0_30(%rip), %xmm0
	movsd	%xmm0, pow2_2(,%rbp,8)
	movsd	.LCPI0_32(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	pow
	movq	%r14, pow1_1+128(,%rbp,8)
	movsd	%xmm0, pow2_1+128(,%rbp,8)
	movq	%rbx, pow1_2+128(,%rbp,8)
	mulsd	.LCPI0_30(%rip), %xmm0
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_30:                               # %.split.preheader
                                        #   in Loop: Header=BB0_29 Depth=1
	movq	%r14, pow1_1(,%rbp,8)
	movq	%r14, pow2_1(,%rbp,8)
	movq	%rbx, pow1_2(,%rbp,8)
	movq	%rbx, pow2_2(,%rbp,8)
	movq	%r14, pow1_1+128(,%rbp,8)
	movq	%r14, pow2_1+128(,%rbp,8)
	movq	%rbx, pow1_2+128(,%rbp,8)
	movapd	%xmm2, %xmm0
	jmp	.LBB0_34
	.p2align	4, 0x90
.LBB0_32:                               # %.split.us.split.preheader
                                        #   in Loop: Header=BB0_29 Depth=1
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	addsd	.LCPI0_29(%rip), %xmm1
	mulsd	.LCPI0_11(%rip), %xmm1
	movsd	%xmm1, (%rsp)           # 8-byte Spill
	movsd	.LCPI0_31(%rip), %xmm0  # xmm0 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow1_1(,%rbp,8)
	movq	%r14, pow2_1(,%rbp,8)
	mulsd	.LCPI0_30(%rip), %xmm0
	movsd	%xmm0, pow1_2(,%rbp,8)
	movq	%rbx, pow2_2(,%rbp,8)
	movsd	.LCPI0_32(%rip), %xmm0  # xmm0 = mem[0],zero
	movsd	(%rsp), %xmm1           # 8-byte Reload
                                        # xmm1 = mem[0],zero
	callq	pow
	movsd	%xmm0, pow1_1+128(,%rbp,8)
	movq	%r14, pow2_1+128(,%rbp,8)
	movsd	.LCPI0_30(%rip), %xmm1  # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	movsd	%xmm0, pow1_2+128(,%rbp,8)
	movapd	%xmm1, %xmm0
.LBB0_34:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB0_29 Depth=1
	movsd	.LCPI0_12(%rip), %xmm1  # xmm1 = mem[0],zero
	movsd	%xmm0, pow2_2+128(,%rbp,8)
	incq	%rbp
	cmpq	$16, %rbp
	jne	.LBB0_29
# BB#35:                                # %.preheader362.preheader
	movl	$mapbuf0+128, %ecx
	movl	$mapbuf0+608, %r8d
	movl	$mapbuf1+624, %eax
	movl	$mapbuf2+176, %edx
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_36:                               # %.preheader362
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_37 Depth 2
                                        #     Child Loop BB0_39 Depth 2
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	imulq	$608, %r12, %rsi        # imm = 0x260
	leaq	mapbuf0(%rsi), %rax
	leaq	(%r12,%r12,2), %rdx
	movq	%rax, map(,%rdx,8)
	movq	%r12, %rdi
	shlq	$4, %rdi
	leaq	bandInfo+46(%rdi,%rdi,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movswl	bandInfo+46(%rdi,%rdi,8), %eax
	movl	%eax, %edx
	sarl	%edx
	movl	%edx, mapbuf0(%rsi)
	movl	$0, mapbuf0+4(%rsi)
	movl	$3, mapbuf0+8(%rsi)
	movl	$0, mapbuf0+12(%rsi)
	movswl	bandInfo+48(%rdi,%rdi,8), %edx
	movl	%edx, %ebp
	sarl	%ebp
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	movl	%ebp, mapbuf0+16(%rsi)
	movl	%eax, mapbuf0+20(%rsi)
	movl	$3, mapbuf0+24(%rsi)
	movl	$1, mapbuf0+28(%rsi)
	addl	%eax, %edx
	movswl	bandInfo+50(%rdi,%rdi,8), %ebp
	movl	%ebp, %eax
	sarl	%eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%eax, mapbuf0+32(%rsi)
	movl	%edx, mapbuf0+36(%rsi)
	movl	$3, mapbuf0+40(%rsi)
	movl	$2, mapbuf0+44(%rsi)
	addl	%ebp, %edx
	movswl	bandInfo+52(%rdi,%rdi,8), %ebp
	movl	%ebp, %eax
	sarl	%eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	%eax, mapbuf0+48(%rsi)
	movl	%edx, mapbuf0+52(%rsi)
	movl	$3, mapbuf0+56(%rsi)
	movl	$3, mapbuf0+60(%rsi)
	addl	%ebp, %edx
	movswl	bandInfo+54(%rdi,%rdi,8), %eax
	movl	%eax, %r10d
	sarl	%r10d
	movl	%r10d, mapbuf0+64(%rsi)
	movl	%edx, mapbuf0+68(%rsi)
	movl	$3, mapbuf0+72(%rsi)
	movl	$4, mapbuf0+76(%rsi)
	addl	%eax, %edx
	movswl	bandInfo+56(%rdi,%rdi,8), %eax
	movl	%eax, %r11d
	sarl	%r11d
	movl	%r11d, mapbuf0+80(%rsi)
	movl	%edx, mapbuf0+84(%rsi)
	movl	$3, mapbuf0+88(%rsi)
	movl	$5, mapbuf0+92(%rsi)
	addl	%eax, %edx
	movswl	bandInfo+58(%rdi,%rdi,8), %eax
	movl	%eax, %r14d
	sarl	%r14d
	movl	%r14d, mapbuf0+96(%rsi)
	movl	%edx, mapbuf0+100(%rsi)
	movl	$3, mapbuf0+104(%rsi)
	movl	$6, mapbuf0+108(%rsi)
	addl	%eax, %edx
	movswl	bandInfo+60(%rdi,%rdi,8), %eax
	movl	%eax, %r9d
	sarl	%r9d
	movl	%r9d, mapbuf0+112(%rsi)
	movl	%edx, mapbuf0+116(%rsi)
	movl	$3, mapbuf0+120(%rsi)
	movl	$7, mapbuf0+124(%rsi)
	addl	%eax, %edx
	leaq	bandInfo+124(%rdi,%rdi,8), %rax
	movl	$3, %esi
	movq	%rcx, (%rsp)            # 8-byte Spill
	movq	%rcx, %rbp
	.p2align	4, 0x90
.LBB0_37:                               #   Parent Loop BB0_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rax), %ebx
	sarl	%ebx
	movl	%ebx, (%rbp)
	movl	%edx, 4(%rbp)
	movl	$0, 8(%rbp)
	movl	%esi, 12(%rbp)
	movl	%ebx, 16(%rbp)
	leal	1(%rdx), %ecx
	movl	%ecx, 20(%rbp)
	movl	$1, 24(%rbp)
	movl	%esi, 28(%rbp)
	movl	%ebx, 32(%rbp)
	leal	2(%rdx), %ecx
	movl	%ecx, 36(%rbp)
	movl	$2, 40(%rbp)
	movl	%esi, 44(%rbp)
	addq	$2, %rax
	leal	(%rbx,%rbx,2), %ecx
	leal	(%rdx,%rcx,2), %edx
	incl	%esi
	addq	$48, %rbp
	cmpl	$13, %esi
	jne	.LBB0_37
# BB#38:                                #   in Loop: Header=BB0_36 Depth=1
	leaq	(%r12,%r12,8), %rax
	shlq	$4, %rax
	leaq	bandInfo+118(%rax), %rsi
	leaq	(,%r12,8), %r13
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movq	%r8, mapend(%r13,%r13,2)
	imulq	$624, %r12, %rax        # imm = 0x270
	leaq	mapbuf1(%rax), %rax
	movq	%rax, map+8(%r13,%r13,2)
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rsi), %ecx
	sarl	%ecx
	movl	%ecx, (%rax)
	movl	%ebp, 4(%rax)
	movl	$0, 8(%rax)
	movl	%ebx, 12(%rax)
	movl	%ecx, 16(%rax)
	movl	%ebp, %edx
	orl	$1, %edx
	movl	%edx, 20(%rax)
	movl	$1, 24(%rax)
	movl	%ebx, 28(%rax)
	movl	%ecx, 32(%rax)
	leal	2(%rbp), %edx
	movl	%edx, 36(%rax)
	movl	$2, 40(%rax)
	movl	%ebx, 44(%rax)
	addq	$2, %rsi
	leal	(%rcx,%rcx,2), %ecx
	leal	(%rbp,%rcx,2), %ebp
	incl	%ebx
	addq	$48, %rax
	cmpl	$13, %ebx
	jne	.LBB0_39
# BB#40:                                #   in Loop: Header=BB0_36 Depth=1
	imulq	$176, %r12, %r8
	leaq	mapbuf2(%r8), %rax
	movq	%rax, map+16(%r13,%r13,2)
	movq	40(%rsp), %rax          # 8-byte Reload
	movswl	(%rax), %eax
	sarl	%eax
	movl	%eax, mapbuf2(%r8)
	movl	$0, mapbuf2+4(%r8)
	movl	36(%rsp), %eax          # 4-byte Reload
	movl	%eax, mapbuf2+8(%r8)
	movl	$1, mapbuf2+12(%r8)
	movl	32(%rsp), %eax          # 4-byte Reload
	movl	%eax, mapbuf2+16(%r8)
	movl	$2, mapbuf2+20(%r8)
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, mapbuf2+24(%r8)
	movl	$3, mapbuf2+28(%r8)
	movl	%r10d, mapbuf2+32(%r8)
	movl	$4, mapbuf2+36(%r8)
	movl	%r11d, mapbuf2+40(%r8)
	movl	$5, mapbuf2+44(%r8)
	movl	%r14d, mapbuf2+48(%r8)
	movl	$6, mapbuf2+52(%r8)
	movl	%r9d, mapbuf2+56(%r8)
	movl	$7, mapbuf2+60(%r8)
	movswl	bandInfo+62(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+64(%r8)
	movl	$8, mapbuf2+68(%r8)
	movswl	bandInfo+64(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+72(%r8)
	movl	$9, mapbuf2+76(%r8)
	movswl	bandInfo+66(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+80(%r8)
	movl	$10, mapbuf2+84(%r8)
	movswl	bandInfo+68(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+88(%r8)
	movl	$11, mapbuf2+92(%r8)
	movswl	bandInfo+70(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+96(%r8)
	movl	$12, mapbuf2+100(%r8)
	movswl	bandInfo+72(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+104(%r8)
	movl	$13, mapbuf2+108(%r8)
	movswl	bandInfo+74(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+112(%r8)
	movl	$14, mapbuf2+116(%r8)
	movswl	bandInfo+76(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+120(%r8)
	movl	$15, mapbuf2+124(%r8)
	movswl	bandInfo+78(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+128(%r8)
	movl	$16, mapbuf2+132(%r8)
	movswl	bandInfo+80(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+136(%r8)
	movl	$17, mapbuf2+140(%r8)
	movswl	bandInfo+82(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+144(%r8)
	movl	$18, mapbuf2+148(%r8)
	movswl	bandInfo+84(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+152(%r8)
	movl	$19, mapbuf2+156(%r8)
	movswl	bandInfo+86(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+160(%r8)
	movl	$20, mapbuf2+164(%r8)
	movswl	bandInfo+88(%rdi,%rdi,8), %eax
	sarl	%eax
	movl	%eax, mapbuf2+168(%r8)
	movl	$21, mapbuf2+172(%r8)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, mapend+8(%r13,%r13,2)
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rdx, mapend+16(%r13,%r13,2)
	incq	%r12
	movq	(%rsp), %rcx            # 8-byte Reload
	addq	$608, %rcx              # imm = 0x260
	movq	16(%rsp), %r8           # 8-byte Reload
	addq	$608, %r8               # imm = 0x260
	addq	$624, %rax              # imm = 0x270
	addq	$176, %rdx
	cmpq	$9, %r12
	jne	.LBB0_36
# BB#41:                                # %.preheader360.preheader
	movl	$shortLimit, %r8d
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	xorl	%ecx, %ecx
	movl	$bandInfo+90, %r9d
	movdqa	.LCPI0_33(%rip), %xmm8  # xmm8 = [7,7,7,7]
	movdqa	.LCPI0_34(%rip), %xmm2  # xmm2 = [954437177,954437177,954437177,954437177]
	movdqa	%xmm2, %xmm3
	psrad	$31, %xmm3
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	movdqa	.LCPI0_35(%rip), %xmm5  # xmm5 = [1,1,1,1]
	.p2align	4, 0x90
.LBB0_42:                               # %scalar.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_43 Depth 2
	movq	%rcx, %rsi
	shlq	$4, %rsi
	movq	bandInfo(%rsi,%rsi,8), %xmm6 # xmm6 = mem[0],zero
	punpcklwd	%xmm6, %xmm6    # xmm6 = xmm6[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm6
	paddd	%xmm8, %xmm6
	movdqa	%xmm3, %xmm7
	pand	%xmm6, %xmm7
	movdqa	%xmm6, %xmm1
	psrad	$31, %xmm1
	pand	%xmm2, %xmm1
	paddd	%xmm7, %xmm1
	pshufd	$245, %xmm6, %xmm7      # xmm7 = xmm6[1,1,3,3]
	pmuludq	%xmm2, %xmm6
	pshufd	$237, %xmm6, %xmm6      # xmm6 = xmm6[1,3,2,3]
	pmuludq	%xmm4, %xmm7
	pshufd	$237, %xmm7, %xmm7      # xmm7 = xmm7[1,3,2,3]
	punpckldq	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	psubd	%xmm1, %xmm6
	movdqa	%xmm6, %xmm1
	psrld	$31, %xmm1
	psrad	$2, %xmm6
	paddd	%xmm1, %xmm6
	imulq	$92, %rcx, %rdi
	movdqa	%xmm0, %xmm1
	pcmpgtd	%xmm6, %xmm1
	paddd	%xmm5, %xmm6
	pand	%xmm1, %xmm6
	pandn	%xmm0, %xmm1
	por	%xmm6, %xmm1
	movdqu	%xmm1, longLimit(%rdi)
	movq	bandInfo+8(%rsi,%rsi,8), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	paddd	%xmm8, %xmm1
	movdqa	%xmm3, %xmm6
	pand	%xmm1, %xmm6
	movdqa	%xmm1, %xmm7
	psrad	$31, %xmm7
	pand	%xmm2, %xmm7
	paddd	%xmm6, %xmm7
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$237, %xmm1, %xmm1      # xmm1 = xmm1[1,3,2,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$237, %xmm6, %xmm6      # xmm6 = xmm6[1,3,2,3]
	punpckldq	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	psubd	%xmm7, %xmm1
	movdqa	%xmm1, %xmm6
	psrld	$31, %xmm6
	psrad	$2, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm6
	pcmpgtd	%xmm1, %xmm6
	paddd	%xmm5, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm0, %xmm6
	por	%xmm1, %xmm6
	movdqu	%xmm6, longLimit+16(%rdi)
	movq	bandInfo+16(%rsi,%rsi,8), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	paddd	%xmm8, %xmm1
	movdqa	%xmm3, %xmm6
	pand	%xmm1, %xmm6
	movdqa	%xmm1, %xmm7
	psrad	$31, %xmm7
	pand	%xmm2, %xmm7
	paddd	%xmm6, %xmm7
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$237, %xmm1, %xmm1      # xmm1 = xmm1[1,3,2,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$237, %xmm6, %xmm6      # xmm6 = xmm6[1,3,2,3]
	punpckldq	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	psubd	%xmm7, %xmm1
	movdqa	%xmm1, %xmm6
	psrld	$31, %xmm6
	psrad	$2, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm6
	pcmpgtd	%xmm1, %xmm6
	paddd	%xmm5, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm0, %xmm6
	por	%xmm1, %xmm6
	movdqu	%xmm6, longLimit+32(%rdi)
	movq	bandInfo+24(%rsi,%rsi,8), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	paddd	%xmm8, %xmm1
	movdqa	%xmm3, %xmm6
	pand	%xmm1, %xmm6
	movdqa	%xmm1, %xmm7
	psrad	$31, %xmm7
	pand	%xmm2, %xmm7
	paddd	%xmm6, %xmm7
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$237, %xmm1, %xmm1      # xmm1 = xmm1[1,3,2,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$237, %xmm6, %xmm6      # xmm6 = xmm6[1,3,2,3]
	punpckldq	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	psubd	%xmm7, %xmm1
	movdqa	%xmm1, %xmm6
	psrld	$31, %xmm6
	psrad	$2, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm6
	pcmpgtd	%xmm1, %xmm6
	paddd	%xmm5, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm0, %xmm6
	por	%xmm1, %xmm6
	movdqu	%xmm6, longLimit+48(%rdi)
	movq	bandInfo+32(%rsi,%rsi,8), %xmm1 # xmm1 = mem[0],zero
	punpcklwd	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1,2,2,3,3]
	psrad	$16, %xmm1
	paddd	%xmm8, %xmm1
	movdqa	%xmm3, %xmm6
	pand	%xmm1, %xmm6
	movdqa	%xmm1, %xmm7
	psrad	$31, %xmm7
	pand	%xmm2, %xmm7
	paddd	%xmm6, %xmm7
	pshufd	$245, %xmm1, %xmm6      # xmm6 = xmm1[1,1,3,3]
	pmuludq	%xmm2, %xmm1
	pshufd	$237, %xmm1, %xmm1      # xmm1 = xmm1[1,3,2,3]
	pmuludq	%xmm4, %xmm6
	pshufd	$237, %xmm6, %xmm6      # xmm6 = xmm6[1,3,2,3]
	punpckldq	%xmm6, %xmm1    # xmm1 = xmm1[0],xmm6[0],xmm1[1],xmm6[1]
	psubd	%xmm7, %xmm1
	movdqa	%xmm1, %xmm6
	psrld	$31, %xmm6
	psrad	$2, %xmm1
	paddd	%xmm6, %xmm1
	movdqa	%xmm0, %xmm6
	pcmpgtd	%xmm1, %xmm6
	paddd	%xmm5, %xmm1
	pand	%xmm6, %xmm1
	pandn	%xmm0, %xmm6
	por	%xmm1, %xmm6
	movdqu	%xmm6, longLimit+64(%rdi)
	movswl	bandInfo+40(%rsi,%rsi,8), %edx
	addl	$7, %edx
	movslq	%edx, %rdx
	imulq	$954437177, %rdx, %rdx  # imm = 0x38E38E39
	movq	%rdx, %rbx
	shrq	$63, %rbx
	shrq	$32, %rdx
	sarl	$2, %edx
	leal	(%rdx,%rbx), %eax
	leal	1(%rdx,%rbx), %edx
	cmpl	%r15d, %eax
	cmovgel	%r15d, %edx
	movl	%edx, longLimit+80(%rdi)
	movswl	bandInfo+42(%rsi,%rsi,8), %eax
	addl	$7, %eax
	cltq
	imulq	$954437177, %rax, %rax  # imm = 0x38E38E39
	movq	%rax, %rdx
	shrq	$63, %rdx
	shrq	$32, %rax
	sarl	$2, %eax
	leal	(%rax,%rdx), %ebx
	leal	1(%rax,%rdx), %eax
	cmpl	%r15d, %ebx
	cmovgel	%r15d, %eax
	movl	%eax, longLimit+84(%rdi)
	movswl	bandInfo+44(%rsi,%rsi,8), %eax
	addl	$7, %eax
	cltq
	imulq	$954437177, %rax, %rax  # imm = 0x38E38E39
	movq	%rax, %rdx
	shrq	$63, %rdx
	shrq	$32, %rax
	sarl	$2, %eax
	leal	(%rax,%rdx), %esi
	leal	1(%rax,%rdx), %eax
	cmpl	%r15d, %esi
	cmovgel	%r15d, %eax
	movl	%eax, longLimit+88(%rdi)
	movl	$14, %esi
	movq	%r8, %rdi
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB0_43:                               # %.preheader359
                                        #   Parent Loop BB0_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movswl	(%rdx), %eax
	decl	%eax
	cltq
	imulq	$954437177, %rax, %rax  # imm = 0x38E38E39
	movq	%rax, %rbx
	shrq	$63, %rbx
	sarq	$34, %rax
	leal	(%rax,%rbx), %ebp
	leal	1(%rax,%rbx), %eax
	cmpl	%r15d, %ebp
	cmovgel	%r15d, %eax
	movl	%eax, (%rdi)
	addq	$2, %rdx
	addq	$4, %rdi
	decq	%rsi
	jne	.LBB0_43
# BB#44:                                #   in Loop: Header=BB0_42 Depth=1
	incq	%rcx
	addq	$144, %r9
	addq	$56, %r8
	cmpq	$9, %rcx
	jne	.LBB0_42
# BB#45:                                # %.preheader357.preheader
	movaps	.LCPI0_36(%rip), %xmm0  # xmm0 = [12288,12352,12416,12480]
	movaps	%xmm0, i_slen2(%rip)
	movaps	.LCPI0_37(%rip), %xmm0  # xmm0 = [12544,12608,12296,12360]
	movaps	%xmm0, i_slen2+16(%rip)
	movaps	.LCPI0_38(%rip), %xmm0  # xmm0 = [12424,12488,12552,12616]
	movaps	%xmm0, i_slen2+32(%rip)
	movaps	.LCPI0_39(%rip), %xmm0  # xmm0 = [12304,12368,12432,12496]
	movaps	%xmm0, i_slen2+48(%rip)
	movaps	.LCPI0_40(%rip), %xmm0  # xmm0 = [12560,12624,12312,12376]
	movaps	%xmm0, i_slen2+64(%rip)
	movaps	.LCPI0_41(%rip), %xmm0  # xmm0 = [12440,12504,12568,12632]
	movaps	%xmm0, i_slen2+80(%rip)
	movaps	.LCPI0_42(%rip), %xmm0  # xmm0 = [12320,12384,12448,12512]
	movaps	%xmm0, i_slen2+96(%rip)
	movaps	.LCPI0_43(%rip), %xmm0  # xmm0 = [12576,12640,12328,12392]
	movaps	%xmm0, i_slen2+112(%rip)
	movaps	.LCPI0_44(%rip), %xmm0  # xmm0 = [12456,12520,12584,12648]
	movaps	%xmm0, i_slen2+128(%rip)
	movaps	.LCPI0_45(%rip), %xmm0  # xmm0 = [12289,12353,12417,12481]
	movaps	%xmm0, i_slen2+144(%rip)
	movaps	.LCPI0_46(%rip), %xmm0  # xmm0 = [12545,12609,12297,12361]
	movaps	%xmm0, i_slen2+160(%rip)
	movaps	.LCPI0_47(%rip), %xmm0  # xmm0 = [12425,12489,12553,12617]
	movaps	%xmm0, i_slen2+176(%rip)
	movaps	.LCPI0_48(%rip), %xmm0  # xmm0 = [12305,12369,12433,12497]
	movaps	%xmm0, i_slen2+192(%rip)
	movaps	.LCPI0_49(%rip), %xmm0  # xmm0 = [12561,12625,12313,12377]
	movaps	%xmm0, i_slen2+208(%rip)
	movaps	.LCPI0_50(%rip), %xmm0  # xmm0 = [12441,12505,12569,12633]
	movaps	%xmm0, i_slen2+224(%rip)
	movaps	.LCPI0_51(%rip), %xmm0  # xmm0 = [12321,12385,12449,12513]
	movaps	%xmm0, i_slen2+240(%rip)
	movaps	.LCPI0_52(%rip), %xmm0  # xmm0 = [12577,12641,12329,12393]
	movaps	%xmm0, i_slen2+256(%rip)
	movaps	.LCPI0_53(%rip), %xmm0  # xmm0 = [12457,12521,12585,12649]
	movaps	%xmm0, i_slen2+272(%rip)
	movaps	.LCPI0_54(%rip), %xmm0  # xmm0 = [12290,12354,12418,12482]
	movaps	%xmm0, i_slen2+288(%rip)
	movaps	.LCPI0_55(%rip), %xmm0  # xmm0 = [12546,12610,12298,12362]
	movaps	%xmm0, i_slen2+304(%rip)
	movaps	.LCPI0_56(%rip), %xmm0  # xmm0 = [12426,12490,12554,12618]
	movaps	%xmm0, i_slen2+320(%rip)
	movaps	.LCPI0_57(%rip), %xmm0  # xmm0 = [12306,12370,12434,12498]
	movaps	%xmm0, i_slen2+336(%rip)
	movaps	.LCPI0_58(%rip), %xmm0  # xmm0 = [12562,12626,12314,12378]
	movaps	%xmm0, i_slen2+352(%rip)
	movaps	.LCPI0_59(%rip), %xmm0  # xmm0 = [12442,12506,12570,12634]
	movaps	%xmm0, i_slen2+368(%rip)
	movaps	.LCPI0_60(%rip), %xmm0  # xmm0 = [12322,12386,12450,12514]
	movaps	%xmm0, i_slen2+384(%rip)
	movaps	.LCPI0_61(%rip), %xmm0  # xmm0 = [12578,12642,12330,12394]
	movaps	%xmm0, i_slen2+400(%rip)
	movaps	.LCPI0_62(%rip), %xmm0  # xmm0 = [12458,12522,12586,12650]
	movaps	%xmm0, i_slen2+416(%rip)
	movaps	.LCPI0_63(%rip), %xmm0  # xmm0 = [12291,12355,12419,12483]
	movaps	%xmm0, i_slen2+432(%rip)
	movaps	.LCPI0_64(%rip), %xmm0  # xmm0 = [12547,12611,12299,12363]
	movaps	%xmm0, i_slen2+448(%rip)
	movaps	.LCPI0_65(%rip), %xmm0  # xmm0 = [12427,12491,12555,12619]
	movaps	%xmm0, i_slen2+464(%rip)
	movaps	.LCPI0_66(%rip), %xmm0  # xmm0 = [12307,12371,12435,12499]
	movaps	%xmm0, i_slen2+480(%rip)
	movaps	.LCPI0_67(%rip), %xmm0  # xmm0 = [12563,12627,12315,12379]
	movaps	%xmm0, i_slen2+496(%rip)
	movaps	.LCPI0_68(%rip), %xmm0  # xmm0 = [12443,12507,12571,12635]
	movaps	%xmm0, i_slen2+512(%rip)
	movaps	.LCPI0_69(%rip), %xmm0  # xmm0 = [12323,12387,12451,12515]
	movaps	%xmm0, i_slen2+528(%rip)
	movaps	.LCPI0_70(%rip), %xmm0  # xmm0 = [12579,12643,12331,12395]
	movaps	%xmm0, i_slen2+544(%rip)
	movaps	.LCPI0_71(%rip), %xmm0  # xmm0 = [12459,12523,12587,12651]
	movaps	%xmm0, i_slen2+560(%rip)
	movaps	.LCPI0_72(%rip), %xmm0  # xmm0 = [12292,12356,12420,12484]
	movaps	%xmm0, i_slen2+576(%rip)
	movaps	.LCPI0_73(%rip), %xmm0  # xmm0 = [12548,12612,12300,12364]
	movaps	%xmm0, i_slen2+592(%rip)
	movaps	.LCPI0_74(%rip), %xmm0  # xmm0 = [12428,12492,12556,12620]
	movaps	%xmm0, i_slen2+608(%rip)
	movaps	.LCPI0_75(%rip), %xmm0  # xmm0 = [12308,12372,12436,12500]
	movaps	%xmm0, i_slen2+624(%rip)
	movaps	.LCPI0_76(%rip), %xmm0  # xmm0 = [12564,12628,12316,12380]
	movaps	%xmm0, i_slen2+640(%rip)
	movaps	.LCPI0_77(%rip), %xmm0  # xmm0 = [12444,12508,12572,12636]
	movaps	%xmm0, i_slen2+656(%rip)
	movaps	.LCPI0_78(%rip), %xmm0  # xmm0 = [12324,12388,12452,12516]
	movaps	%xmm0, i_slen2+672(%rip)
	movaps	.LCPI0_79(%rip), %xmm0  # xmm0 = [12580,12644,12332,12396]
	movaps	%xmm0, i_slen2+688(%rip)
	movaps	.LCPI0_80(%rip), %xmm0  # xmm0 = [12460,12524,12588,12652]
	movaps	%xmm0, i_slen2+704(%rip)
	movaps	.LCPI0_81(%rip), %xmm0  # xmm0 = [16384,16448,16512,16576]
	movaps	%xmm0, i_slen2+720(%rip)
	movaps	.LCPI0_82(%rip), %xmm0  # xmm0 = [16392,16456,16520,16584]
	movaps	%xmm0, i_slen2+736(%rip)
	movaps	.LCPI0_83(%rip), %xmm0  # xmm0 = [16400,16464,16528,16592]
	movaps	%xmm0, i_slen2+752(%rip)
	movaps	.LCPI0_84(%rip), %xmm0  # xmm0 = [16408,16472,16536,16600]
	movaps	%xmm0, i_slen2+768(%rip)
	movaps	.LCPI0_85(%rip), %xmm0  # xmm0 = [16385,16449,16513,16577]
	movaps	%xmm0, i_slen2+784(%rip)
	movaps	.LCPI0_86(%rip), %xmm0  # xmm0 = [16393,16457,16521,16585]
	movaps	%xmm0, i_slen2+800(%rip)
	movaps	.LCPI0_87(%rip), %xmm0  # xmm0 = [16401,16465,16529,16593]
	movaps	%xmm0, i_slen2+816(%rip)
	movaps	.LCPI0_88(%rip), %xmm0  # xmm0 = [16409,16473,16537,16601]
	movaps	%xmm0, i_slen2+832(%rip)
	movaps	.LCPI0_89(%rip), %xmm0  # xmm0 = [16386,16450,16514,16578]
	movaps	%xmm0, i_slen2+848(%rip)
	movaps	.LCPI0_90(%rip), %xmm0  # xmm0 = [16394,16458,16522,16586]
	movaps	%xmm0, i_slen2+864(%rip)
	movaps	.LCPI0_91(%rip), %xmm0  # xmm0 = [16402,16466,16530,16594]
	movaps	%xmm0, i_slen2+880(%rip)
	movaps	.LCPI0_92(%rip), %xmm0  # xmm0 = [16410,16474,16538,16602]
	movaps	%xmm0, i_slen2+896(%rip)
	movaps	.LCPI0_93(%rip), %xmm0  # xmm0 = [16387,16451,16515,16579]
	movaps	%xmm0, i_slen2+912(%rip)
	movaps	.LCPI0_94(%rip), %xmm0  # xmm0 = [16395,16459,16523,16587]
	movaps	%xmm0, i_slen2+928(%rip)
	movaps	.LCPI0_95(%rip), %xmm0  # xmm0 = [16403,16467,16531,16595]
	movaps	%xmm0, i_slen2+944(%rip)
	movaps	.LCPI0_96(%rip), %xmm0  # xmm0 = [16411,16475,16539,16603]
	movaps	%xmm0, i_slen2+960(%rip)
	movaps	.LCPI0_97(%rip), %xmm0  # xmm0 = [20480,20488,20496,20481]
	movaps	%xmm0, i_slen2+976(%rip)
	movaps	.LCPI0_98(%rip), %xmm0  # xmm0 = [40960,40968,40976,40961]
	movaps	%xmm0, n_slen2+2000(%rip)
	movaps	.LCPI0_99(%rip), %xmm0  # xmm0 = [20489,20497,20482,20490]
	movaps	%xmm0, i_slen2+992(%rip)
	movaps	.LCPI0_100(%rip), %xmm0 # xmm0 = [40969,40977,40962,40970]
	movaps	%xmm0, n_slen2+2016(%rip)
	movaps	.LCPI0_101(%rip), %xmm0 # xmm0 = [20498,20483,20491,20499]
	movaps	%xmm0, i_slen2+1008(%rip)
	movaps	.LCPI0_102(%rip), %xmm0 # xmm0 = [40978,40963,40971,40979]
	movaps	%xmm0, n_slen2+2032(%rip)
	xorl	%eax, %eax
	movl	$n_slen2+316, %ecx
	movdqa	.LCPI0_103(%rip), %xmm0 # xmm0 = [512,1024,1536,64]
	movdqa	.LCPI0_104(%rip), %xmm1 # xmm1 = [576,1088,1600,128]
	movdqa	.LCPI0_105(%rip), %xmm2 # xmm2 = [640,1152,1664,192]
	movdqa	.LCPI0_106(%rip), %xmm8 # xmm8 = [8,8,8,8]
	movdqa	.LCPI0_107(%rip), %xmm9 # xmm9 = [16,16,16,16]
	movdqa	.LCPI0_108(%rip), %xmm5 # xmm5 = [24,24,24,24]
	movdqa	.LCPI0_109(%rip), %xmm6 # xmm6 = [32,32,32,32]
	.p2align	4, 0x90
.LBB0_46:                               # %.preheader348
                                        # =>This Inner Loop Header: Depth=1
	movl	%eax, -316(%rcx)
	movd	%eax, %xmm7
	pshufd	$0, %xmm7, %xmm4        # xmm4 = xmm7[0,0,0,0]
	movdqa	%xmm4, %xmm7
	por	%xmm0, %xmm7
	movdqu	%xmm7, -312(%rcx)
	movdqa	%xmm4, %xmm7
	por	%xmm1, %xmm7
	movdqu	%xmm7, -296(%rcx)
	movdqa	%xmm4, %xmm7
	por	%xmm2, %xmm7
	movdqu	%xmm7, -280(%rcx)
	movl	%eax, %edx
	orl	$704, %edx              # imm = 0x2C0
	movl	%edx, -264(%rcx)
	movl	%eax, %edx
	orl	$1216, %edx             # imm = 0x4C0
	movl	%edx, -260(%rcx)
	movl	%eax, %edx
	orl	$1728, %edx             # imm = 0x6C0
	movl	%edx, -256(%rcx)
	movl	%eax, %edx
	orl	$8, %edx
	movl	%edx, -252(%rcx)
	movdqa	%xmm4, %xmm3
	por	%xmm8, %xmm3
	movdqa	%xmm3, %xmm7
	por	%xmm0, %xmm7
	movdqu	%xmm7, -248(%rcx)
	movdqa	%xmm3, %xmm7
	por	%xmm1, %xmm7
	movdqu	%xmm7, -232(%rcx)
	por	%xmm2, %xmm3
	movdqu	%xmm3, -216(%rcx)
	movl	%eax, %edx
	orl	$712, %edx              # imm = 0x2C8
	movl	%edx, -200(%rcx)
	movl	%eax, %edx
	orl	$1224, %edx             # imm = 0x4C8
	movl	%edx, -196(%rcx)
	movl	%eax, %edx
	orl	$1736, %edx             # imm = 0x6C8
	movl	%edx, -192(%rcx)
	movl	%eax, %edx
	orl	$16, %edx
	movl	%edx, -188(%rcx)
	movdqa	%xmm4, %xmm3
	por	%xmm9, %xmm3
	movdqa	%xmm3, %xmm7
	por	%xmm0, %xmm7
	movdqu	%xmm7, -184(%rcx)
	movdqa	%xmm3, %xmm7
	por	%xmm1, %xmm7
	movdqu	%xmm7, -168(%rcx)
	por	%xmm2, %xmm3
	movdqu	%xmm3, -152(%rcx)
	movl	%eax, %edx
	orl	$720, %edx              # imm = 0x2D0
	movl	%edx, -136(%rcx)
	movl	%eax, %edx
	orl	$1232, %edx             # imm = 0x4D0
	movl	%edx, -132(%rcx)
	movl	%eax, %edx
	orl	$1744, %edx             # imm = 0x6D0
	movl	%edx, -128(%rcx)
	movl	%eax, %edx
	orl	$24, %edx
	movl	%edx, -124(%rcx)
	movdqa	%xmm4, %xmm3
	por	%xmm5, %xmm3
	movdqa	%xmm3, %xmm7
	por	%xmm0, %xmm7
	movdqu	%xmm7, -120(%rcx)
	movdqa	%xmm3, %xmm7
	por	%xmm1, %xmm7
	movdqu	%xmm7, -104(%rcx)
	por	%xmm2, %xmm3
	movdqu	%xmm3, -88(%rcx)
	movl	%eax, %edx
	orl	$728, %edx              # imm = 0x2D8
	movl	%edx, -72(%rcx)
	movl	%eax, %edx
	orl	$1240, %edx             # imm = 0x4D8
	movl	%edx, -68(%rcx)
	movl	%eax, %edx
	orl	$1752, %edx             # imm = 0x6D8
	movl	%edx, -64(%rcx)
	movl	%eax, %edx
	orl	$32, %edx
	movl	%edx, -60(%rcx)
	por	%xmm6, %xmm4
	movdqa	%xmm4, %xmm3
	por	%xmm0, %xmm3
	movdqu	%xmm3, -56(%rcx)
	movdqa	%xmm4, %xmm3
	por	%xmm1, %xmm3
	movdqu	%xmm3, -40(%rcx)
	por	%xmm2, %xmm4
	movdqu	%xmm4, -24(%rcx)
	movl	%eax, %edx
	orl	$736, %edx              # imm = 0x2E0
	movl	%edx, -8(%rcx)
	movl	%eax, %edx
	orl	$1248, %edx             # imm = 0x4E0
	movl	%edx, -4(%rcx)
	movl	%eax, %edx
	orl	$1760, %edx             # imm = 0x6E0
	movl	%edx, (%rcx)
	incq	%rax
	addq	$320, %rcx              # imm = 0x140
	cmpq	$5, %rax
	jne	.LBB0_46
# BB#47:                                # %.preheader345.preheader
	movaps	.LCPI0_110(%rip), %xmm0 # xmm0 = [4096,4160,4224,4288]
	movaps	%xmm0, n_slen2+1600(%rip)
	movaps	.LCPI0_111(%rip), %xmm0 # xmm0 = [4104,4168,4232,4296]
	movaps	%xmm0, n_slen2+1616(%rip)
	movaps	.LCPI0_112(%rip), %xmm0 # xmm0 = [4112,4176,4240,4304]
	movaps	%xmm0, n_slen2+1632(%rip)
	movaps	.LCPI0_113(%rip), %xmm0 # xmm0 = [4120,4184,4248,4312]
	movaps	%xmm0, n_slen2+1648(%rip)
	movaps	.LCPI0_114(%rip), %xmm0 # xmm0 = [4128,4192,4256,4320]
	movaps	%xmm0, n_slen2+1664(%rip)
	movaps	.LCPI0_115(%rip), %xmm0 # xmm0 = [4097,4161,4225,4289]
	movaps	%xmm0, n_slen2+1680(%rip)
	movaps	.LCPI0_116(%rip), %xmm0 # xmm0 = [4105,4169,4233,4297]
	movaps	%xmm0, n_slen2+1696(%rip)
	movaps	.LCPI0_117(%rip), %xmm0 # xmm0 = [4113,4177,4241,4305]
	movaps	%xmm0, n_slen2+1712(%rip)
	movaps	.LCPI0_118(%rip), %xmm0 # xmm0 = [4121,4185,4249,4313]
	movaps	%xmm0, n_slen2+1728(%rip)
	movaps	.LCPI0_119(%rip), %xmm0 # xmm0 = [4129,4193,4257,4321]
	movaps	%xmm0, n_slen2+1744(%rip)
	movaps	.LCPI0_120(%rip), %xmm0 # xmm0 = [4098,4162,4226,4290]
	movaps	%xmm0, n_slen2+1760(%rip)
	movaps	.LCPI0_121(%rip), %xmm0 # xmm0 = [4106,4170,4234,4298]
	movaps	%xmm0, n_slen2+1776(%rip)
	movaps	.LCPI0_122(%rip), %xmm0 # xmm0 = [4114,4178,4242,4306]
	movaps	%xmm0, n_slen2+1792(%rip)
	movaps	.LCPI0_123(%rip), %xmm0 # xmm0 = [4122,4186,4250,4314]
	movaps	%xmm0, n_slen2+1808(%rip)
	movaps	.LCPI0_124(%rip), %xmm0 # xmm0 = [4130,4194,4258,4322]
	movaps	%xmm0, n_slen2+1824(%rip)
	movaps	.LCPI0_125(%rip), %xmm0 # xmm0 = [4099,4163,4227,4291]
	movaps	%xmm0, n_slen2+1840(%rip)
	movaps	.LCPI0_126(%rip), %xmm0 # xmm0 = [4107,4171,4235,4299]
	movaps	%xmm0, n_slen2+1856(%rip)
	movaps	.LCPI0_127(%rip), %xmm0 # xmm0 = [4115,4179,4243,4307]
	movaps	%xmm0, n_slen2+1872(%rip)
	movaps	.LCPI0_128(%rip), %xmm0 # xmm0 = [4123,4187,4251,4315]
	movaps	%xmm0, n_slen2+1888(%rip)
	movaps	.LCPI0_129(%rip), %xmm0 # xmm0 = [4131,4195,4259,4323]
	movaps	%xmm0, n_slen2+1904(%rip)
	movaps	.LCPI0_130(%rip), %xmm0 # xmm0 = [4100,4164,4228,4292]
	movaps	%xmm0, n_slen2+1920(%rip)
	movaps	.LCPI0_131(%rip), %xmm0 # xmm0 = [4108,4172,4236,4300]
	movaps	%xmm0, n_slen2+1936(%rip)
	movaps	.LCPI0_132(%rip), %xmm0 # xmm0 = [4116,4180,4244,4308]
	movaps	%xmm0, n_slen2+1952(%rip)
	movaps	.LCPI0_133(%rip), %xmm0 # xmm0 = [4124,4188,4252,4316]
	movaps	%xmm0, n_slen2+1968(%rip)
	movaps	.LCPI0_134(%rip), %xmm0 # xmm0 = [4132,4196,4260,4324]
	movaps	%xmm0, n_slen2+1984(%rip)
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	init_layer3, .Lfunc_end0-init_layer3
	.cfi_endproc

	.globl	do_layer3
	.p2align	4, 0x90
	.type	do_layer3,@function
do_layer3:                              # @do_layer3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$1000, %rsp             # imm = 0x3E8
.Lcfi19:
	.cfi_def_cfa_offset 1056
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	(%rdi), %eax
	movl	8(%rdi), %ebx
	xorl	%ecx, %ecx
	cmpq	$1, %rax
	movslq	36(%rdi), %rbp
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	movl	%ebx, 32(%rsp)          # 4-byte Spill
	movl	%ebx, %ebp
	movl	$0, 60(%rsp)            # 4-byte Folded Spill
	cmovel	%ecx, %ebp
	movl	%ebp, 56(%rsp)          # 4-byte Spill
	cmpl	$1, 48(%rdi)
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	jne	.LBB1_2
# BB#1:
	movl	52(%rdi), %ebp
	movl	%ebp, %ecx
	andl	$2, %ecx
	movl	%ecx, 60(%rsp)          # 4-byte Spill
	andl	$1, %ebp
	movl	%ebp, 72(%rsp)          # 4-byte Spill
.LBB1_2:
	movq	%rsi, 168(%rsp)         # 8-byte Spill
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movslq	%eax, %r12
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	cmpl	$0, 12(%rdi)
	movq	%rax, %rbx
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	je	.LBB1_5
# BB#3:
	movl	$8, %edi
	callq	getbits
	movl	%eax, %ebp
	movl	%ebp, 200(%rsp)
	cmpl	$1, %ebx
	jne	.LBB1_19
# BB#4:                                 # %.thread.i
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	shrl	$7, %edx
	andl	$1, %edx
	movl	%edx, 204(%rsp)
	jmp	.LBB1_20
.LBB1_5:
	xorl	%ebx, %ebx
	cmpl	$1, %r12d
	sete	%bl
	movl	$9, %edi
	callq	getbits
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%eax, 200(%rsp)
	leal	3(%rbx,%rbx), %edi
	callq	getbits_fast
	movl	%eax, 204(%rsp)
	testl	%r12d, %r12d
	jle	.LBB1_8
# BB#6:                                 # %.lr.ph92.preheader.i
	leaq	328(%rsp), %rbx
	movq	96(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph92.i
                                        # =>This Inner Loop Header: Depth=1
	movl	$-1, -120(%rbx)
	movl	$4, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$240, %rbx
	decq	%rbp
	jne	.LBB1_7
.LBB1_8:                                # %.preheader85.i
	xorl	%ebp, %ebp
	cmpl	$3, 56(%rsp)            # 4-byte Folded Reload
	sete	%bpl
	shlq	$2, %rbp
	leaq	212(%rsp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ecx, %ecx
	shlq	$3, %rbp
	movl	$2, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB1_9:                                # %.preheader84.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_12 Depth 2
	movq	%rcx, (%rsp)            # 8-byte Spill
	cmpl	$0, 96(%rsp)            # 4-byte Folded Reload
	jle	.LBB1_18
# BB#10:                                # %.lr.ph.i184.preheader
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	8(%rsp), %r13           # 8-byte Reload
	xorl	%r15d, %r15d
	jmp	.LBB1_12
.LBB1_11:                               #   in Loop: Header=BB1_12 Depth=2
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$288, 4(%r13)           # imm = 0x120
	jmp	.LBB1_13
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph.i184
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$12, %edi
	callq	getbits
	movl	%eax, (%r13)
	movl	$9, %edi
	callq	getbits_fast
	movl	%eax, 4(%r13)
	cmpl	$289, %eax              # imm = 0x121
	jae	.LBB1_11
.LBB1_13:                               #   in Loop: Header=BB1_12 Depth=2
	movl	$8, %edi
	callq	getbits_fast
	movl	%eax, %eax
	shlq	$3, %rax
	movq	%rbp, %rcx
	subq	%rax, %rcx
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	leaq	gainpow2+2048(%rcx), %rax
	leaq	gainpow2+2064(%rcx), %r14
	cmoveq	%rax, %r14
	movq	%r14, 108(%r13)
	movl	$4, %edi
	callq	getbits_fast
	movl	%eax, 8(%r13)
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%dl, %dl
	js	.LBB1_15
# BB#14:                                # %.preheader.preheader.i186
                                        #   in Loop: Header=BB1_12 Depth=2
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 20(%r13)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 24(%r13)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 28(%r13)
	movl	$4, %edi
	callq	getbits_fast
	movl	%eax, %ebx
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movslq	%ebx, %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdx,8), %rdx
	shlq	$4, %rdx
	movswl	bandInfo+2(%rdx,%rcx,2), %ecx
	sarl	%ecx
	movl	%ecx, 64(%r13)
	leal	2(%rax,%rbx), %eax
	cltq
	movswl	bandInfo(%rdx,%rax,2), %eax
	sarl	%eax
	movl	%eax, 68(%r13)
	movl	$0, 12(%r13)
	movl	$0, 16(%r13)
	jmp	.LBB1_17
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_12 Depth=2
	movl	$2, %edi
	callq	getbits_fast
	movl	%eax, %ebx
	movl	%ebx, 12(%r13)
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	shrl	$7, %edx
	andl	$1, %edx
	movl	%edx, 16(%r13)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 20(%r13)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 24(%r13)
	movl	$0, 28(%r13)
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$3, %eax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, 84(%r13)
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$3, %eax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, 92(%r13)
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$3, %eax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, 100(%r13)
	testl	%ebx, %ebx
	je	.LBB1_241
# BB#16:                                #   in Loop: Header=BB1_12 Depth=2
	movabsq	$1236950581266, %rax    # imm = 0x12000000012
	movq	%rax, 64(%r13)
.LBB1_17:                               #   in Loop: Header=BB1_12 Depth=2
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %edi
	movl	bitindex(%rip), %eax
	movl	%eax, %ecx
	shll	%cl, %edi
	leal	1(%rax), %ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rbx
	leaq	(%rdx,%rbx), %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	shrl	$7, %edi
	andl	$1, %edi
	movl	%edi, 72(%r13)
	movzbl	(%rdx,%rbx), %edi
	shll	%cl, %edi
	incl	%ecx
	shrl	$3, %ecx
	leaq	(%rsi,%rcx), %rbx
	movq	%rbx, wordpointer(%rip)
	leal	2(%rax), %edx
	andl	$7, %edx
	movl	%edx, bitindex(%rip)
	shrl	$7, %edi
	andl	$1, %edi
	movl	%edi, 76(%r13)
	movzbl	(%rcx,%rsi), %esi
	movl	%edx, %ecx
	shll	%cl, %esi
	incl	%edx
	shrl	$3, %edx
	addq	%rbx, %rdx
	movq	%rdx, wordpointer(%rip)
	addl	$3, %eax
	andl	$7, %eax
	movl	%eax, bitindex(%rip)
	shrl	$7, %esi
	andl	$1, %esi
	movl	%esi, 80(%r13)
	incq	%r15
	addq	$240, %r13
	cmpq	%r12, %r15
	jl	.LBB1_12
.LBB1_18:                               # %._crit_edge.i
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	(%rsp), %rcx            # 8-byte Reload
	incq	%rcx
	addq	$120, 8(%rsp)           # 8-byte Folded Spill
	cmpq	$2, %rcx
	jl	.LBB1_9
	jmp	.LBB1_30
.LBB1_19:
	movl	$2, %edi
	callq	getbits_fast
	movl	%eax, 204(%rsp)
	testl	%ebx, %ebx
	jle	.LBB1_240
.LBB1_20:                               # %.lr.ph.i
	movl	%ebp, 16(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	cmpl	$3, 56(%rsp)            # 4-byte Folded Reload
	sete	%r15b
	shlq	$2, %r15
	cmpl	$8, 64(%rsp)            # 4-byte Folded Reload
	movl	$54, %eax
	movl	$27, %ecx
	cmovel	%eax, %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	leaq	212(%rsp), %rbp
	xorl	%r13d, %r13d
	movl	$1, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	shlq	$3, %r15
	jmp	.LBB1_22
.LBB1_21:                               #   in Loop: Header=BB1_22 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$22, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$288, 4(%rbp)           # imm = 0x120
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_22:                               # =>This Inner Loop Header: Depth=1
	movl	$12, %edi
	callq	getbits
	movl	%eax, (%rbp)
	movl	$9, %edi
	callq	getbits_fast
	movl	%eax, 4(%rbp)
	cmpl	$289, %eax              # imm = 0x121
	jae	.LBB1_21
.LBB1_23:                               #   in Loop: Header=BB1_22 Depth=1
	movl	$8, %edi
	callq	getbits_fast
	movl	%eax, %eax
	shlq	$3, %rax
	movq	%r15, %rcx
	subq	%rax, %rcx
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	leaq	gainpow2+2048(%rcx), %rax
	leaq	gainpow2+2064(%rcx), %r14
	cmoveq	%rax, %r14
	movq	%r14, 108(%rbp)
	movl	$9, %edi
	callq	getbits
	movl	%eax, 8(%rbp)
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%dl, %dl
	js	.LBB1_25
# BB#24:                                # %.preheader.preheader.i
                                        #   in Loop: Header=BB1_22 Depth=1
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 20(%rbp)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 24(%rbp)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 28(%rbp)
	movl	$4, %edi
	callq	getbits_fast
	movl	%eax, %ebx
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movslq	%ebx, %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rdx,8), %rdx
	shlq	$4, %rdx
	movswl	bandInfo+2(%rdx,%rcx,2), %ecx
	sarl	%ecx
	movl	%ecx, 64(%rbp)
	leal	2(%rax,%rbx), %eax
	cltq
	movswl	bandInfo(%rdx,%rax,2), %eax
	sarl	%eax
	movl	%eax, 68(%rbp)
	movl	$0, 12(%rbp)
	movl	$0, 16(%rbp)
	jmp	.LBB1_29
	.p2align	4, 0x90
.LBB1_25:                               #   in Loop: Header=BB1_22 Depth=1
	movl	$2, %edi
	callq	getbits_fast
	movl	%eax, %ebx
	movl	%ebx, 12(%rbp)
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	shrl	$7, %edx
	andl	$1, %edx
	movl	%edx, 16(%rbp)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 20(%rbp)
	movl	$5, %edi
	callq	getbits_fast
	movl	%eax, 24(%rbp)
	movl	$0, 28(%rbp)
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$3, %eax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, 84(%rbp)
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$3, %eax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, 92(%rbp)
	movl	$3, %edi
	callq	getbits_fast
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	shll	$3, %eax
	leaq	(%r14,%rax,8), %rax
	movq	%rax, 100(%rbp)
	cmpl	$2, %ebx
	je	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_22 Depth=1
	testl	%ebx, %ebx
	movl	(%rsp), %eax            # 4-byte Reload
	jne	.LBB1_28
	jmp	.LBB1_241
.LBB1_27:                               #   in Loop: Header=BB1_22 Depth=1
	movl	$18, %eax
.LBB1_28:                               #   in Loop: Header=BB1_22 Depth=1
	movl	%eax, 64(%rbp)
	movl	$288, 68(%rbp)          # imm = 0x120
.LBB1_29:                               #   in Loop: Header=BB1_22 Depth=1
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %eax
	movl	%eax, %ecx
	shll	%cl, %esi
	leal	1(%rax), %ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	leaq	(%rdx,%rdi), %rbx
	movq	%rbx, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	shrl	$7, %esi
	andl	$1, %esi
	movl	%esi, 76(%rbp)
	movzbl	(%rdx,%rdi), %edx
	shll	%cl, %edx
	incl	%ecx
	shrl	$3, %ecx
	addq	%rbx, %rcx
	movq	%rcx, wordpointer(%rip)
	addl	$2, %eax
	andl	$7, %eax
	movl	%eax, bitindex(%rip)
	shrl	$7, %edx
	andl	$1, %edx
	movl	%edx, 80(%rbp)
	incq	%r13
	addq	$240, %rbp
	cmpq	%r12, %r13
	jl	.LBB1_22
.LBB1_30:
	movl	16(%rsp), %ebp          # 4-byte Reload
.LBB1_31:                               # %III_get_side_info_2.exit
	movl	%ebp, %edi
	callq	set_pointer
	cmpl	$-1, %eax
	je	.LBB1_237
# BB#32:                                # %.preheader245
	movl	32(%rsp), %esi          # 4-byte Reload
	shrl	$31, %esi
	incl	%esi
	cmpl	$3, 56(%rsp)            # 4-byte Folded Reload
	sete	%al
	movl	72(%rsp), %ecx          # 4-byte Reload
	movl	60(%rsp), %edx          # 4-byte Reload
	orl	%edx, %ecx
	setne	%cl
	orb	%al, %cl
	movb	%cl, 31(%rsp)           # 1-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	shlq	$4, %rax
	testl	%edx, %edx
	movl	$tan2_1, %ecx
	movl	$tan2_2, %edx
	cmoveq	%rcx, %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movl	$tan1_1, %ecx
	movl	$tan1_2, %edx
	cmoveq	%rcx, %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movl	$pow2_1, %ecx
	movl	$pow2_2, %edx
	cmoveq	%rcx, %rdx
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movl	$pow1_1, %ecx
	movl	$pow1_2, %edx
	cmoveq	%rcx, %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	cmpl	$1, 96(%rsp)            # 4-byte Folded Reload
	leaq	bandInfo+114(%rax,%rax,8), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movl	%esi, %eax
	movl	$1, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	leaq	688(%rsp), %rdi
	xorl	%ecx, %ecx
	xorl	%eax, %eax
	jmp	.LBB1_197
	.p2align	4, 0x90
.LBB1_33:                               #   in Loop: Header=BB1_197 Depth=1
	cmpl	$2, 96(%rsp)            # 4-byte Folded Reload
	movq	80(%rsp), %r9           # 8-byte Reload
	jne	.LBB1_161
# BB#34:                                #   in Loop: Header=BB1_197 Depth=1
	leaq	448(%rsp,%r9), %rbp
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 12(%rax)
	je	.LBB1_43
# BB#35:                                #   in Loop: Header=BB1_197 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movl	460(%rsp,%r9), %eax
	leaq	n_slen2(,%rax,4), %rcx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	shrl	%eax
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	leaq	i_slen2(,%rax,4), %rax
	cmovneq	%rax, %rcx
	movl	(%rcx), %r15d
	movl	%r15d, %eax
	shrl	$15, %eax
	andl	$1, %eax
	movl	%eax, 524(%rsp,%r9)
	xorl	%eax, %eax
	cmpl	$2, 464(%rsp,%r9)
	jne	.LBB1_37
# BB#36:                                #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, 468(%rsp,%r9)
	movl	$1, %eax
	sbbl	$-1, %eax
.LBB1_37:                               #   in Loop: Header=BB1_197 Depth=1
	movl	%eax, %eax
	movl	%r15d, %ebp
	shrl	$12, %ebp
	andl	$7, %ebp
	movl	%r15d, %r12d
	shrl	$3, %r12d
	shlq	$2, %rbp
	movl	%r15d, %ebx
	andl	$7, %ebx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,2), %rcx
	movzbl	III_get_scale_factors_2.stab(%rbp,%rcx,8), %r14d
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	je	.LBB1_44
# BB#38:                                # %.preheader57.i203
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	leaq	844(%rsp), %rax
	movq	%rax, %rdi
	je	.LBB1_42
# BB#39:                                # %.lr.ph.i209.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	leal	-1(%r14), %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r14, (%rsp)            # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill>
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB1_40:                               # %.lr.ph.i209
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %edi
	callq	getbits_fast
	movl	%eax, (%r13)
	addq	$4, %r13
	decl	%r14d
	jne	.LBB1_40
# BB#41:                                # %._crit_edge.i211.loopexit
                                        #   in Loop: Header=BB1_197 Depth=1
	leaq	844(%rsp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	4(%rax,%rcx,4), %rdi
	movq	(%rsp), %r14            # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB1_42:                               # %._crit_edge.i211
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%ebx, %r14d
	movq	%r14, (%rsp)            # 8-byte Spill
	jmp	.LBB1_47
.LBB1_43:                               #   in Loop: Header=BB1_197 Depth=1
	leaq	844(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	III_get_scale_factors_1
	movl	%eax, %r13d
	jmp	.LBB1_73
.LBB1_44:                               # %.preheader.i204
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	je	.LBB1_46
# BB#45:                                # %.lr.ph65.i206
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r14d
	movl	$1, %eax
	cmoval	%r14d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	leaq	844(%rsp), %rbx
	movq	%rbx, %rdi
	callq	memset
	movq	8(%rsp), %rcx           # 8-byte Reload
	decl	%r14d
	leaq	4(%rbx,%r14,4), %rdi
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB1_47
.LBB1_46:                               #   in Loop: Header=BB1_197 Depth=1
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	844(%rsp), %rax
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB1_47:                               # %.loopexit.i218
                                        #   in Loop: Header=BB1_197 Depth=1
	movl	%r15d, %r13d
	shrl	$6, %r13d
	andl	$7, %r12d
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movzbl	III_get_scale_factors_2.stab+1(%rbp,%rcx,8), %r14d
	je	.LBB1_53
# BB#48:                                # %.preheader57.i203.1
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	je	.LBB1_52
# BB#49:                                # %.lr.ph.i209.preheader.1
                                        #   in Loop: Header=BB1_197 Depth=1
	leal	-1(%r14), %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r14d, %ebp
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB1_50:                               # %.lr.ph.i209.1
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB1_50
# BB#51:                                # %._crit_edge.i211.loopexit.1
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	4(%rdi,%rax,4), %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB1_52:                               # %._crit_edge.i211.1
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%r12d, %r14d
	movq	(%rsp), %rax            # 8-byte Reload
	addl	%r14d, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	jmp	.LBB1_55
.LBB1_53:                               # %.preheader.i204.1
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	je	.LBB1_55
# BB#54:                                # %.lr.ph65.i206.1
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r14d
	movl	$1, %eax
	cmoval	%r14d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	callq	memset
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbx, %rdi
	decl	%r14d
	leaq	4(%rdi,%r14,4), %rdi
.LBB1_55:                               # %.loopexit.i218.1
                                        #   in Loop: Header=BB1_197 Depth=1
	shrl	$9, %r15d
	andl	$7, %r13d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movzbl	III_get_scale_factors_2.stab+2(%rbp,%rcx,8), %r12d
	je	.LBB1_61
# BB#56:                                # %.preheader57.i203.2
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_60
# BB#57:                                # %.lr.ph.i209.preheader.2
                                        #   in Loop: Header=BB1_197 Depth=1
	leal	-1(%r12), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %ebp
	movq	%rdi, %r14
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB1_58:                               # %.lr.ph.i209.2
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB1_58
# BB#59:                                # %._crit_edge.i211.loopexit.2
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%r14, %rdi
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	4(%rdi,%rax,4), %rdi
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB1_60:                               # %._crit_edge.i211.2
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%r13d, %r12d
	movq	(%rsp), %r13            # 8-byte Reload
	addl	%r12d, %r13d
	jmp	.LBB1_64
.LBB1_61:                               # %.preheader.i204.2
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_63
# BB#62:                                # %.lr.ph65.i206.2
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r12d
	movl	$1, %eax
	cmoval	%r12d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	callq	memset
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbx, %rdi
	decl	%r12d
	leaq	4(%rdi,%r12,4), %rdi
.LBB1_63:                               # %.loopexit.i218.2
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	(%rsp), %r13            # 8-byte Reload
.LBB1_64:                               # %.loopexit.i218.2
                                        #   in Loop: Header=BB1_197 Depth=1
	andl	$7, %r15d
	movzbl	III_get_scale_factors_2.stab+3(%rbp,%rcx,8), %r12d
	je	.LBB1_70
# BB#65:                                # %.preheader57.i203.3
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_69
# BB#66:                                # %.lr.ph.i209.preheader.3
                                        #   in Loop: Header=BB1_197 Depth=1
	leal	-1(%r12), %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%r12d, %ebp
	movq	%rdi, %r14
	movq	%rdi, %rbx
	.p2align	4, 0x90
.LBB1_67:                               # %.lr.ph.i209.3
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB1_67
# BB#68:                                # %._crit_edge.i211.loopexit.3
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%r14, %rdi
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	4(%rdi,%rax,4), %rdi
.LBB1_69:                               # %._crit_edge.i211.3
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%r15d, %r12d
	addl	%r12d, %r13d
	jmp	.LBB1_72
.LBB1_70:                               # %.preheader.i204.3
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_72
# BB#71:                                # %.lr.ph65.i206.3
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r12d
	movl	$1, %eax
	cmoval	%r12d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rdi, %rbx
	callq	memset
	movq	%rbx, %rdi
	decl	%r12d
	leaq	4(%rdi,%r12,4), %rdi
.LBB1_72:                               # %.loopexit.i218.3
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	4(,%rax,8), %edx
	addl	$-4, %edx
	orl	$4, %edx
	xorl	%esi, %esi
	callq	memset
	leaq	844(%rsp), %rbx
.LBB1_73:                               #   in Loop: Header=BB1_197 Depth=1
	movl	$do_layer3.hybridIn+4608, %edi
	movq	%rbx, %rsi
	movq	%rbp, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r13d, %r8d
	callq	III_dequantize_sample
	testl	%eax, %eax
	jne	.LBB1_238
# BB#74:                                #   in Loop: Header=BB1_197 Depth=1
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	je	.LBB1_77
# BB#75:                                # %vector.body471.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	$-4608, %rax            # imm = 0xEE00
	.p2align	4, 0x90
.LBB1_76:                               # %vector.body471
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	do_layer3.hybridIn+4608(%rax), %xmm0
	movapd	do_layer3.hybridIn+9216(%rax), %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	movapd	%xmm2, do_layer3.hybridIn+9216(%rax)
	addpd	%xmm1, %xmm0
	movapd	%xmm0, do_layer3.hybridIn+4608(%rax)
	movapd	do_layer3.hybridIn+4624(%rax), %xmm0
	movapd	do_layer3.hybridIn+9232(%rax), %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	movapd	%xmm2, do_layer3.hybridIn+9232(%rax)
	addpd	%xmm1, %xmm0
	movapd	%xmm0, do_layer3.hybridIn+4624(%rax)
	movapd	do_layer3.hybridIn+4640(%rax), %xmm0
	movapd	do_layer3.hybridIn+9248(%rax), %xmm1
	movapd	%xmm0, %xmm2
	subpd	%xmm1, %xmm2
	movapd	%xmm2, do_layer3.hybridIn+9248(%rax)
	addpd	%xmm1, %xmm0
	movapd	%xmm0, do_layer3.hybridIn+4640(%rax)
	addq	$48, %rax
	jne	.LBB1_76
.LBB1_77:                               # %.loopexit
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	movq	80(%rsp), %r9           # 8-byte Reload
	je	.LBB1_142
# BB#78:                                #   in Loop: Header=BB1_197 Depth=1
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 12(%rax)
	movq	160(%rsp), %r14         # 8-byte Reload
	movq	152(%rsp), %r15         # 8-byte Reload
	je	.LBB1_80
# BB#79:                                #   in Loop: Header=BB1_197 Depth=1
	movl	460(%rsp,%r9), %r15d
	andl	$1, %r15d
	shlq	$7, %r15
	movq	144(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%r15), %r14
	addq	136(%rsp), %r15         # 8-byte Folded Reload
.LBB1_80:                               #   in Loop: Header=BB1_197 Depth=1
	cmpl	$2, 464(%rsp,%r9)
	jne	.LBB1_113
# BB#81:                                #   in Loop: Header=BB1_197 Depth=1
	movl	468(%rsp,%r9), %esi
	xorl	%edx, %edx
	testl	%esi, %esi
	setne	%dl
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB1_82:                               #   Parent Loop BB1_197 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_83 Depth 3
                                        #         Child Loop BB1_86 Depth 4
                                        #       Child Loop BB1_91 Depth 3
	leaq	200(%rsp,%r9), %rax
	movslq	296(%rax,%rdi,4), %rbp
	cmpq	$3, %rbp
	movl	$0, %eax
	cmovgl	%eax, %edx
	cmpq	$11, %rbp
	jg	.LBB1_88
	.p2align	4, 0x90
.LBB1_83:                               #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_82 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB1_86 Depth 4
	leal	(%rbp,%rbp,2), %eax
	leal	(%rdi,%rax), %eax
	subl	%esi, %eax
	movslq	844(%rsp,%rax,4), %rcx
	cmpq	$7, %rcx
	je	.LBB1_87
# BB#84:                                #   in Loop: Header=BB1_83 Depth=3
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %rax
	shlq	$4, %rax
	movswl	bandInfo+118(%rax,%rbp,2), %ebx
	testl	%ebx, %ebx
	jle	.LBB1_87
# BB#85:                                # %.lr.ph19.preheader.i
                                        #   in Loop: Header=BB1_83 Depth=3
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movswl	bandInfo+90(%rax,%rbp,2), %eax
	addl	%edi, %eax
	cltq
	leaq	do_layer3.hybridIn(,%rax,8), %rcx
	incl	%ebx
	.p2align	4, 0x90
.LBB1_86:                               # %.lr.ph19.i
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_82 Depth=2
                                        #       Parent Loop BB1_83 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm2, %xmm3
	movsd	%xmm3, (%rcx)
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 4608(%rcx)
	addq	$24, %rcx
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB1_86
.LBB1_87:                               # %.loopexit8.i
                                        #   in Loop: Header=BB1_83 Depth=3
	incq	%rbp
	cmpq	$12, %rbp
	jne	.LBB1_83
.LBB1_88:                               # %._crit_edge.i222
                                        #   in Loop: Header=BB1_82 Depth=2
	leal	33(%rdi), %eax
	subl	%esi, %eax
	movslq	844(%rsp,%rax,4), %rcx
	cmpq	$7, %rcx
	je	.LBB1_92
# BB#89:                                #   in Loop: Header=BB1_82 Depth=2
	movq	128(%rsp), %rax         # 8-byte Reload
	movswl	28(%rax), %ebp
	testl	%ebp, %ebp
	jle	.LBB1_92
# BB#90:                                # %.lr.ph25.preheader.i
                                        #   in Loop: Header=BB1_82 Depth=2
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	128(%rsp), %rax         # 8-byte Reload
	movswl	(%rax), %eax
	addl	%edi, %eax
	cltq
	leaq	do_layer3.hybridIn(,%rax,8), %rcx
	incl	%ebp
	.p2align	4, 0x90
.LBB1_91:                               # %.lr.ph25.i
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_82 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
	movapd	%xmm0, %xmm3
	mulsd	%xmm2, %xmm3
	movsd	%xmm3, (%rcx)
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 4608(%rcx)
	addq	$24, %rcx
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB1_91
.LBB1_92:                               # %.loopexit9.i
                                        #   in Loop: Header=BB1_82 Depth=2
	incq	%rdi
	cmpq	$3, %rdi
	jne	.LBB1_82
# BB#93:                                #   in Loop: Header=BB1_197 Depth=1
	testl	%edx, %edx
	je	.LBB1_142
# BB#94:                                #   in Loop: Header=BB1_197 Depth=1
	movslq	508(%rsp,%r9), %rdx
	cmpq	$7, %rdx
	jg	.LBB1_142
# BB#95:                                # %.lr.ph16.preheader.i
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %r8
	shlq	$4, %r8
	movswl	bandInfo(%r8,%rdx,2), %edi
	.p2align	4, 0x90
.LBB1_96:                               # %.lr.ph16.i
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_105 Depth 3
                                        #       Child Loop BB1_111 Depth 3
	movswl	bandInfo+46(%r8,%rdx,2), %ebp
	movslq	844(%rsp,%rdx,4), %rcx
	cmpq	$7, %rcx
	jne	.LBB1_98
# BB#97:                                #   in Loop: Header=BB1_96 Depth=2
	addl	%edi, %ebp
	movl	%ebp, %edi
	jmp	.LBB1_112
	.p2align	4, 0x90
.LBB1_98:                               #   in Loop: Header=BB1_96 Depth=2
	testw	%bp, %bp
	jle	.LBB1_112
# BB#99:                                # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB1_96 Depth=2
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movslq	%edi, %rdi
	movl	%ebp, %eax
	notl	%eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovlel	%ecx, %eax
	leal	1(%rax,%rbp), %r11d
	incq	%r11
	cmpq	$2, %r11
	jb	.LBB1_110
# BB#100:                               # %min.iters.checked383
                                        #   in Loop: Header=BB1_96 Depth=2
	movq	%r11, %r9
	movabsq	$8589934590, %rax       # imm = 0x1FFFFFFFE
	andq	%rax, %r9
	movq	%r11, %r10
	andq	%rax, %r10
	je	.LBB1_109
# BB#101:                               # %vector.ph387
                                        #   in Loop: Header=BB1_96 Depth=2
	movaps	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movaps	%xmm1, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	leaq	-2(%r10), %rax
	movq	%rax, %rcx
	shrq	%rcx
	btl	$1, %eax
	jb	.LBB1_103
# BB#102:                               # %vector.body379.prol
                                        #   in Loop: Header=BB1_96 Depth=2
	movupd	do_layer3.hybridIn(,%rdi,8), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, do_layer3.hybridIn(,%rdi,8)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, do_layer3.hybridIn+4608(,%rdi,8)
	movl	$2, %ebx
	testq	%rcx, %rcx
	jne	.LBB1_104
	jmp	.LBB1_106
.LBB1_103:                              #   in Loop: Header=BB1_96 Depth=2
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.LBB1_106
.LBB1_104:                              # %vector.ph387.new
                                        #   in Loop: Header=BB1_96 Depth=2
	movq	%r10, %rsi
	subq	%rbx, %rsi
	addq	%rdi, %rbx
	leaq	do_layer3.hybridIn(,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_105:                              # %vector.body379
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, (%rbx)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, 4608(%rbx)
	movupd	16(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, 16(%rbx)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, 4624(%rbx)
	addq	$32, %rbx
	addq	$-4, %rsi
	jne	.LBB1_105
.LBB1_106:                              # %middle.block380
                                        #   in Loop: Header=BB1_96 Depth=2
	addq	%r9, %rdi
	cmpq	%r10, %r11
	jne	.LBB1_108
# BB#107:                               #   in Loop: Header=BB1_96 Depth=2
	movq	80(%rsp), %r9           # 8-byte Reload
	jmp	.LBB1_112
.LBB1_108:                              #   in Loop: Header=BB1_96 Depth=2
	subl	%r9d, %ebp
.LBB1_109:                              # %.lr.ph.i225.preheader
                                        #   in Loop: Header=BB1_96 Depth=2
	movq	80(%rsp), %r9           # 8-byte Reload
.LBB1_110:                              # %.lr.ph.i225.preheader
                                        #   in Loop: Header=BB1_96 Depth=2
	incl	%ebp
	.p2align	4, 0x90
.LBB1_111:                              # %.lr.ph.i225
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	do_layer3.hybridIn(,%rdi,8), %xmm2 # xmm2 = mem[0],zero
	movaps	%xmm0, %xmm3
	mulsd	%xmm2, %xmm3
	movsd	%xmm3, do_layer3.hybridIn(,%rdi,8)
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, do_layer3.hybridIn+4608(,%rdi,8)
	incq	%rdi
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB1_111
.LBB1_112:                              # %.loopexit.i227
                                        #   in Loop: Header=BB1_96 Depth=2
	incq	%rdx
	cmpq	$8, %rdx
	jne	.LBB1_96
	jmp	.LBB1_142
.LBB1_113:                              #   in Loop: Header=BB1_197 Depth=1
	movslq	508(%rsp,%r9), %rsi
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %r9
	shlq	$4, %r9
	cmpq	$20, %rsi
	movswl	bandInfo(%r9,%rsi,2), %ebp
	jg	.LBB1_129
	.p2align	4, 0x90
.LBB1_114:                              # %.lr.ph41.i
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_123 Depth 3
                                        #       Child Loop BB1_127 Depth 3
	movswl	bandInfo+46(%r9,%rsi,2), %edx
	movslq	844(%rsp,%rsi,4), %rcx
	cmpq	$7, %rcx
	jne	.LBB1_116
# BB#115:                               #   in Loop: Header=BB1_114 Depth=2
	addl	%ebp, %edx
	movl	%edx, %ebp
	jmp	.LBB1_128
	.p2align	4, 0x90
.LBB1_116:                              #   in Loop: Header=BB1_114 Depth=2
	testw	%dx, %dx
	jle	.LBB1_128
# BB#117:                               # %.lr.ph36.preheader.i
                                        #   in Loop: Header=BB1_114 Depth=2
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movslq	%ebp, %rbp
	movl	%edx, %eax
	notl	%eax
	cmpl	$-3, %eax
	movl	$-2, %ecx
	cmovlel	%ecx, %eax
	leal	1(%rax,%rdx), %r11d
	incq	%r11
	cmpq	$2, %r11
	jb	.LBB1_126
# BB#118:                               # %min.iters.checked445
                                        #   in Loop: Header=BB1_114 Depth=2
	movq	%r11, %r8
	movabsq	$8589934590, %rax       # imm = 0x1FFFFFFFE
	andq	%rax, %r8
	movq	%r11, %r10
	andq	%rax, %r10
	je	.LBB1_126
# BB#119:                               # %vector.ph449
                                        #   in Loop: Header=BB1_114 Depth=2
	movaps	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movaps	%xmm1, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	leaq	-2(%r10), %rax
	movq	%rax, %rcx
	shrq	%rcx
	btl	$1, %eax
	jb	.LBB1_121
# BB#120:                               # %vector.body440.prol
                                        #   in Loop: Header=BB1_114 Depth=2
	movupd	do_layer3.hybridIn(,%rbp,8), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, do_layer3.hybridIn(,%rbp,8)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, do_layer3.hybridIn+4608(,%rbp,8)
	movl	$2, %ebx
	testq	%rcx, %rcx
	jne	.LBB1_122
	jmp	.LBB1_124
.LBB1_121:                              #   in Loop: Header=BB1_114 Depth=2
	xorl	%ebx, %ebx
	testq	%rcx, %rcx
	je	.LBB1_124
.LBB1_122:                              # %vector.ph449.new
                                        #   in Loop: Header=BB1_114 Depth=2
	movq	%r10, %rdi
	subq	%rbx, %rdi
	addq	%rbp, %rbx
	leaq	do_layer3.hybridIn(,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_123:                              # %vector.body440
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_114 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, (%rbx)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, 4608(%rbx)
	movupd	16(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, 16(%rbx)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, 4624(%rbx)
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB1_123
.LBB1_124:                              # %middle.block441
                                        #   in Loop: Header=BB1_114 Depth=2
	addq	%r8, %rbp
	cmpq	%r10, %r11
	je	.LBB1_128
# BB#125:                               #   in Loop: Header=BB1_114 Depth=2
	subl	%r8d, %edx
	.p2align	4, 0x90
.LBB1_126:                              # %.lr.ph36.i.preheader
                                        #   in Loop: Header=BB1_114 Depth=2
	incl	%edx
	.p2align	4, 0x90
.LBB1_127:                              # %.lr.ph36.i
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_114 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	do_layer3.hybridIn(,%rbp,8), %xmm2 # xmm2 = mem[0],zero
	movaps	%xmm0, %xmm3
	mulsd	%xmm2, %xmm3
	movsd	%xmm3, do_layer3.hybridIn(,%rbp,8)
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, do_layer3.hybridIn+4608(,%rbp,8)
	incq	%rbp
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB1_127
.LBB1_128:                              # %.loopexit11.i
                                        #   in Loop: Header=BB1_114 Depth=2
	incq	%rsi
	cmpq	$21, %rsi
	jne	.LBB1_114
.LBB1_129:                              # %._crit_edge42.i
                                        #   in Loop: Header=BB1_197 Depth=1
	movslq	924(%rsp), %rcx
	cmpq	$7, %rcx
	movq	80(%rsp), %r9           # 8-byte Reload
	je	.LBB1_142
# BB#130:                               #   in Loop: Header=BB1_197 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movswl	-26(%rax), %edx
	testl	%edx, %edx
	jle	.LBB1_142
# BB#131:                               # %.lr.ph32.preheader.i
                                        #   in Loop: Header=BB1_197 Depth=1
	movsd	(%r15,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movsd	(%r14,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movslq	%ebp, %rax
	movl	%edx, %ecx
	notl	%ecx
	cmpl	$-3, %ecx
	movl	$-2, %esi
	cmovlel	%esi, %ecx
	leal	1(%rcx,%rdx), %ecx
	incq	%rcx
	cmpq	$2, %rcx
	jb	.LBB1_140
# BB#132:                               # %min.iters.checked414
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%rcx, %r8
	movabsq	$8589934590, %rsi       # imm = 0x1FFFFFFFE
	andq	%rsi, %r8
	movq	%rcx, %rbp
	andq	%rsi, %rbp
	je	.LBB1_140
# BB#133:                               # %vector.ph418
                                        #   in Loop: Header=BB1_197 Depth=1
	movaps	%xmm0, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	movaps	%xmm1, %xmm3
	movlhps	%xmm3, %xmm3            # xmm3 = xmm3[0,0]
	leaq	-2(%rbp), %rdi
	movq	%rdi, %rsi
	shrq	%rsi
	btl	$1, %edi
	jb	.LBB1_135
# BB#134:                               # %vector.body409.prol
                                        #   in Loop: Header=BB1_197 Depth=1
	movupd	do_layer3.hybridIn(,%rax,8), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, do_layer3.hybridIn(,%rax,8)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, do_layer3.hybridIn+4608(,%rax,8)
	movl	$2, %ebx
	testq	%rsi, %rsi
	jne	.LBB1_136
	jmp	.LBB1_138
.LBB1_135:                              #   in Loop: Header=BB1_197 Depth=1
	xorl	%ebx, %ebx
	testq	%rsi, %rsi
	je	.LBB1_138
.LBB1_136:                              # %vector.ph418.new
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%rbp, %rdi
	subq	%rbx, %rdi
	addq	%rax, %rbx
	leaq	do_layer3.hybridIn(,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB1_137:                              # %vector.body409
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, (%rbx)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, 4608(%rbx)
	movupd	16(%rbx), %xmm4
	movaps	%xmm2, %xmm5
	mulpd	%xmm4, %xmm5
	movupd	%xmm5, 16(%rbx)
	mulpd	%xmm3, %xmm4
	movupd	%xmm4, 4624(%rbx)
	addq	$32, %rbx
	addq	$-4, %rdi
	jne	.LBB1_137
.LBB1_138:                              # %middle.block410
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpq	%rbp, %rcx
	je	.LBB1_142
# BB#139:                               #   in Loop: Header=BB1_197 Depth=1
	addq	%r8, %rax
	subl	%r8d, %edx
.LBB1_140:                              # %.lr.ph32.i.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	leaq	do_layer3.hybridIn(,%rax,8), %rax
	incl	%edx
	.p2align	4, 0x90
.LBB1_141:                              # %.lr.ph32.i
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	movaps	%xmm0, %xmm3
	mulsd	%xmm2, %xmm3
	movsd	%xmm3, (%rax)
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 4608(%rax)
	addq	$8, %rax
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB1_141
	.p2align	4, 0x90
.LBB1_142:                              # %III_i_stereo.exit
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	je	.LBB1_144
# BB#143:                               #   in Loop: Header=BB1_197 Depth=1
	movl	272(%rsp,%r9), %eax
	movl	512(%rsp,%r9), %ecx
	xorl	%edx, %edx
	cmpl	%eax, %ecx
	setbe	%dl
	cmoval	%ecx, %eax
	imulq	$240, %rdx, %rcx
	leaq	208(%rsp,%rcx), %rcx
	movl	%eax, 64(%r9,%rcx)
.LBB1_144:                              #   in Loop: Header=BB1_197 Depth=1
	movl	56(%rsp), %eax          # 4-byte Reload
	cmpl	$1, %eax
	je	.LBB1_151
# BB#145:                               #   in Loop: Header=BB1_197 Depth=1
	cmpl	$3, %eax
	jne	.LBB1_161
# BB#146:                               # %.preheader240
                                        #   in Loop: Header=BB1_197 Depth=1
	movl	512(%rsp,%r9), %eax
	addl	%eax, %eax
	leal	(%rax,%rax,8), %eax
	testl	%eax, %eax
	je	.LBB1_161
# BB#147:                               # %.lr.ph266.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	leal	-1(%rax), %ecx
	incq	%rcx
	xorl	%esi, %esi
	cmpq	$4, %rcx
	jb	.LBB1_157
# BB#148:                               # %min.iters.checked
                                        #   in Loop: Header=BB1_197 Depth=1
	movabsq	$8589934590, %rdx       # imm = 0x1FFFFFFFE
	leaq	-2(%rdx), %rdx
	andq	%rcx, %rdx
	je	.LBB1_157
# BB#149:                               # %vector.body.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	leaq	-4(%rdx), %rsi
	movq	%rsi, %rdi
	shrq	$2, %rdi
	btl	$2, %esi
	jb	.LBB1_188
# BB#150:                               # %vector.body.prol
                                        #   in Loop: Header=BB1_197 Depth=1
	movapd	do_layer3.hybridIn(%rip), %xmm0
	movapd	do_layer3.hybridIn+16(%rip), %xmm1
	addpd	do_layer3.hybridIn+4608(%rip), %xmm0
	addpd	do_layer3.hybridIn+4624(%rip), %xmm1
	movapd	%xmm0, do_layer3.hybridIn(%rip)
	movapd	%xmm1, do_layer3.hybridIn+16(%rip)
	movl	$4, %esi
	testq	%rdi, %rdi
	jne	.LBB1_189
	jmp	.LBB1_190
.LBB1_151:                              # %.preheader241
                                        #   in Loop: Header=BB1_197 Depth=1
	movl	512(%rsp,%r9), %eax
	addl	%eax, %eax
	leal	(%rax,%rax,8), %eax
	testl	%eax, %eax
	je	.LBB1_161
# BB#152:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	leal	-1(%rax), %ecx
	incq	%rcx
	xorl	%esi, %esi
	cmpq	$3, %rcx
	jbe	.LBB1_159
# BB#153:                               # %min.iters.checked353
                                        #   in Loop: Header=BB1_197 Depth=1
	movabsq	$8589934590, %rdx       # imm = 0x1FFFFFFFE
	leaq	-2(%rdx), %rdx
	andq	%rcx, %rdx
	je	.LBB1_159
# BB#154:                               # %vector.body349.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	leaq	-4(%rdx), %rbp
	movl	%ebp, %edi
	shrl	$2, %edi
	incl	%edi
	andq	$3, %rdi
	je	.LBB1_192
# BB#155:                               # %vector.body349.prol.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	negq	%rdi
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_156:                              # %vector.body349.prol
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	do_layer3.hybridIn+4608(,%rsi,8), %xmm0
	movapd	do_layer3.hybridIn+4624(,%rsi,8), %xmm1
	movapd	%xmm0, do_layer3.hybridIn(,%rsi,8)
	movapd	%xmm1, do_layer3.hybridIn+16(,%rsi,8)
	addq	$4, %rsi
	incq	%rdi
	jne	.LBB1_156
	jmp	.LBB1_193
.LBB1_157:                              #   in Loop: Header=BB1_197 Depth=1
	movl	$do_layer3.hybridIn+4608, %ecx
	movl	$do_layer3.hybridIn, %edi
	.p2align	4, 0x90
.LBB1_158:                              # %.lr.ph266
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	addsd	(%rcx), %xmm0
	addq	$8, %rcx
	movsd	%xmm0, (%rdi)
	incl	%esi
	addq	$8, %rdi
	cmpl	%eax, %esi
	jb	.LBB1_158
	jmp	.LBB1_161
.LBB1_159:                              #   in Loop: Header=BB1_197 Depth=1
	movl	$do_layer3.hybridIn+4608, %ecx
	movl	$do_layer3.hybridIn, %edi
	.p2align	4, 0x90
.LBB1_160:                              # %.lr.ph
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx), %rdx
	addq	$8, %rcx
	movq	%rdx, (%rdi)
	addq	$8, %rdi
	incl	%esi
	cmpl	%eax, %esi
	jb	.LBB1_160
	.p2align	4, 0x90
.LBB1_161:                              # %.thread.preheader.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	movl	$do_layer3.hybridIn, %r13d
	movl	$do_layer3.hybridIn+192, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_162:                              # %.thread.preheader
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_166 Depth 3
                                        #       Child Loop BB1_176 Depth 3
                                        #       Child Loop BB1_173 Depth 3
                                        #       Child Loop BB1_181 Depth 3
	movq	%r13, 32(%rsp)          # 8-byte Spill
	imulq	$240, %rsi, %rax
	leaq	200(%rsp,%rax), %rdi
	cmpl	$2, 24(%r9,%rdi)
	jne	.LBB1_164
# BB#163:                               #   in Loop: Header=BB1_162 Depth=2
	movl	$1, %eax
	cmpl	$0, 28(%r9,%rdi)
	jne	.LBB1_165
	jmp	.LBB1_167
	.p2align	4, 0x90
.LBB1_164:                              #   in Loop: Header=BB1_162 Depth=2
	movl	72(%r9,%rdi), %eax
	decl	%eax
	je	.LBB1_167
.LBB1_165:                              # %.preheader.preheader.i229
                                        #   in Loop: Header=BB1_162 Depth=2
	movapd	aa_cs(%rip), %xmm8
	movapd	%xmm8, %xmm10
	shufpd	$1, %xmm10, %xmm10      # xmm10 = xmm10[1,0]
	movapd	aa_ca(%rip), %xmm2
	movapd	aa_cs+16(%rip), %xmm9
	movapd	%xmm9, %xmm12
	shufpd	$1, %xmm12, %xmm12      # xmm12 = xmm12[1,0]
	movapd	aa_ca+16(%rip), %xmm5
	movapd	aa_cs+32(%rip), %xmm11
	movapd	%xmm11, %xmm14
	shufpd	$1, %xmm14, %xmm14      # xmm14 = xmm14[1,0]
	movapd	aa_ca+32(%rip), %xmm0
	movapd	aa_cs+48(%rip), %xmm13
	movapd	%xmm13, %xmm15
	shufpd	$1, %xmm15, %xmm15      # xmm15 = xmm15[1,0]
	movapd	aa_ca+48(%rip), %xmm6
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB1_166:                              # %.preheader.i230
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_162 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	-64(%rcx), %xmm4
	movupd	-48(%rcx), %xmm3
	movapd	%xmm4, %xmm7
	mulpd	%xmm10, %xmm7
	movapd	%xmm3, %xmm1
	mulpd	%xmm2, %xmm1
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	subpd	%xmm1, %xmm7
	movupd	%xmm7, -64(%rcx)
	mulpd	%xmm8, %xmm3
	shufpd	$1, %xmm4, %xmm4        # xmm4 = xmm4[1,0]
	mulpd	%xmm2, %xmm4
	addpd	%xmm3, %xmm4
	movupd	%xmm4, -48(%rcx)
	movupd	-80(%rcx), %xmm1
	movupd	-32(%rcx), %xmm3
	movapd	%xmm1, %xmm4
	mulpd	%xmm12, %xmm4
	movapd	%xmm3, %xmm7
	mulpd	%xmm5, %xmm7
	shufpd	$1, %xmm7, %xmm7        # xmm7 = xmm7[1,0]
	subpd	%xmm7, %xmm4
	movupd	%xmm4, -80(%rcx)
	mulpd	%xmm9, %xmm3
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm5, %xmm1
	addpd	%xmm3, %xmm1
	movupd	%xmm1, -32(%rcx)
	movupd	-96(%rcx), %xmm1
	movupd	-16(%rcx), %xmm3
	movapd	%xmm1, %xmm4
	mulpd	%xmm14, %xmm4
	movapd	%xmm3, %xmm7
	mulpd	%xmm0, %xmm7
	shufpd	$1, %xmm7, %xmm7        # xmm7 = xmm7[1,0]
	subpd	%xmm7, %xmm4
	movupd	%xmm4, -96(%rcx)
	mulpd	%xmm11, %xmm3
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm0, %xmm1
	addpd	%xmm3, %xmm1
	movupd	%xmm1, -16(%rcx)
	movupd	-112(%rcx), %xmm1
	movupd	(%rcx), %xmm3
	movapd	%xmm1, %xmm4
	mulpd	%xmm15, %xmm4
	movapd	%xmm3, %xmm7
	mulpd	%xmm6, %xmm7
	shufpd	$1, %xmm7, %xmm7        # xmm7 = xmm7[1,0]
	subpd	%xmm7, %xmm4
	movupd	%xmm4, -112(%rcx)
	mulpd	%xmm13, %xmm3
	shufpd	$1, %xmm1, %xmm1        # xmm1 = xmm1[1,0]
	mulpd	%xmm6, %xmm1
	addpd	%xmm3, %xmm1
	movupd	%xmm1, (%rcx)
	addq	$144, %rcx
	decl	%eax
	jne	.LBB1_166
.LBB1_167:                              # %III_antialias.exit
                                        #   in Loop: Header=BB1_162 Depth=2
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	leaq	24(%r9,%rdi), %r8
	movq	%rsi, %r12
	shlq	$9, %r12
	leaq	do_layer3.hybridOut(%r12,%r12,8), %rbx
	leaq	(%r12,%r12,8), %r13
	movq	gmp(%rip), %rax
	movslq	23144(%rax,%rsi,4), %rcx
	leaq	(%rcx,%rcx,8), %rdx
	shlq	$10, %rdx
	leaq	4712(%rax,%rdx), %r10
	leaq	(%r10,%r13), %r15
	movl	$1, %edx
	subl	%ecx, %edx
	movslq	%edx, %rcx
	leaq	(%rcx,%rcx,8), %rdx
	shlq	$10, %rdx
	leaq	4712(%rax,%rdx), %r14
	leaq	(%r14,%r13), %rbp
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 23144(%rax,%rsi,4)
	cmpl	$0, 28(%r9,%rdi)
	je	.LBB1_169
# BB#168:                               #   in Loop: Header=BB1_162 Depth=2
	movq	%rdi, (%rsp)            # 8-byte Spill
	leaq	do_layer3.hybridIn(%r12,%r12,8), %rdi
	movl	$win, %ecx
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%r8, %rbp
	movq	%rbx, %r8
	movq	%r10, %rbx
	callq	dct36
	leaq	do_layer3.hybridIn+144(%r12,%r12,8), %rdi
	leaq	144(%rbx,%r13), %rsi
	leaq	144(%r14,%r13), %rdx
	leaq	do_layer3.hybridOut+8(%r12,%r12,8), %r8
	movl	$win1, %ecx
	callq	dct36
	movq	%rbp, %r8
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	80(%rsp), %r9           # 8-byte Reload
	leaq	288(%rbx,%r13), %r15
	leaq	288(%r14,%r13), %rbp
	leaq	do_layer3.hybridOut+16(%r12,%r12,8), %rbx
	movl	$2, %r12d
	jmp	.LBB1_170
	.p2align	4, 0x90
.LBB1_169:                              #   in Loop: Header=BB1_162 Depth=2
	xorl	%r12d, %r12d
.LBB1_170:                              #   in Loop: Header=BB1_162 Depth=2
	movq	32(%rsp), %r13          # 8-byte Reload
	movslq	(%r8), %rax
	cmpq	$2, %rax
	leaq	72(%r9,%rdi), %rcx
	movq	%rcx, (%rsp)            # 8-byte Spill
	movl	72(%r9,%rdi), %ecx
	jne	.LBB1_174
# BB#171:                               # %.preheader101.i
                                        #   in Loop: Header=BB1_162 Depth=2
	cmpl	%ecx, %r12d
	jae	.LBB1_179
# BB#172:                               # %.lr.ph.preheader.i232
                                        #   in Loop: Header=BB1_162 Depth=2
	movl	%r12d, %eax
	shlq	$4, %rax
	leaq	(%rax,%rax,8), %r14
	.p2align	4, 0x90
.LBB1_173:                              # %.lr.ph.i235
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_162 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r13,%r14), %rdi
	movl	$win+576, %ecx
	movq	%r15, %rsi
	movq	%rbp, %rdx
	movq	%rbx, %r8
	callq	dct12
	leaq	144(%r13,%r14), %rdi
	leaq	144(%r15), %rsi
	leaq	144(%rbp), %rdx
	leaq	8(%rbx), %r8
	movl	$win1+576, %ecx
	callq	dct12
	addq	$16, %rbx
	addq	$288, %r15              # imm = 0x120
	addq	$288, %rbp              # imm = 0x120
	addq	$288, %r14              # imm = 0x120
	addl	$2, %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	(%rax), %r12d
	jb	.LBB1_173
	jmp	.LBB1_178
	.p2align	4, 0x90
.LBB1_174:                              # %.preheader102.i
                                        #   in Loop: Header=BB1_162 Depth=2
	cmpl	%ecx, %r12d
	jae	.LBB1_179
# BB#175:                               # %.lr.ph122.i
                                        #   in Loop: Header=BB1_162 Depth=2
	shlq	$5, %rax
	leaq	win(%rax,%rax,8), %rcx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	win1(%rax,%rax,8), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r12d, %eax
	leaq	(%rax,%rax,8), %r14
	shlq	$4, %r14
	addq	%r13, %r14
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB1_176:                              #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_162 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	leaq	(%r15,%r13), %rsi
	leaq	(%rbp,%r13), %rdx
	leaq	(%r14,%r13), %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	%rbx, %r8
	callq	dct36
	leaq	144(%r14,%r13), %rdi
	leaq	144(%r15,%r13), %rsi
	leaq	144(%rbp,%r13), %rdx
	leaq	8(%rbx), %r8
	movq	16(%rsp), %rcx          # 8-byte Reload
	callq	dct36
	addq	$16, %rbx
	addq	$288, %r13              # imm = 0x120
	addl	$2, %r12d
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	(%rax), %r12d
	jb	.LBB1_176
# BB#177:                               # %.preheader100.i.loopexit485
                                        #   in Loop: Header=BB1_162 Depth=2
	addq	%r13, %r15
	addq	%r13, %rbp
	movq	32(%rsp), %r13          # 8-byte Reload
.LBB1_178:                              # %.preheader100.i
                                        #   in Loop: Header=BB1_162 Depth=2
	cmpl	$31, %r12d
	movq	80(%rsp), %r9           # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	jle	.LBB1_180
	jmp	.LBB1_182
	.p2align	4, 0x90
.LBB1_179:                              #   in Loop: Header=BB1_162 Depth=2
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
.LBB1_180:                              # %.preheader.i237.preheader
                                        #   in Loop: Header=BB1_162 Depth=2
	movl	$32, %eax
	subl	%r12d, %eax
	.p2align	4, 0x90
.LBB1_181:                              # %.preheader.i237
                                        #   Parent Loop BB1_197 Depth=1
                                        #     Parent Loop BB1_162 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15), %rcx
	movq	%rcx, (%rbx)
	movq	$0, (%rbp)
	movq	8(%r15), %rcx
	movq	%rcx, 256(%rbx)
	movq	$0, 8(%rbp)
	movq	16(%r15), %rcx
	movq	%rcx, 512(%rbx)
	movq	$0, 16(%rbp)
	movq	24(%r15), %rcx
	movq	%rcx, 768(%rbx)
	movq	$0, 24(%rbp)
	movq	32(%r15), %rcx
	movq	%rcx, 1024(%rbx)
	movq	$0, 32(%rbp)
	movq	40(%r15), %rcx
	movq	%rcx, 1280(%rbx)
	movq	$0, 40(%rbp)
	movq	48(%r15), %rcx
	movq	%rcx, 1536(%rbx)
	movq	$0, 48(%rbp)
	movq	56(%r15), %rcx
	movq	%rcx, 1792(%rbx)
	movq	$0, 56(%rbp)
	movq	64(%r15), %rcx
	movq	%rcx, 2048(%rbx)
	movq	$0, 64(%rbp)
	movq	72(%r15), %rcx
	movq	%rcx, 2304(%rbx)
	movq	$0, 72(%rbp)
	movq	80(%r15), %rcx
	movq	%rcx, 2560(%rbx)
	movq	$0, 80(%rbp)
	movq	88(%r15), %rcx
	movq	%rcx, 2816(%rbx)
	movq	$0, 88(%rbp)
	movq	96(%r15), %rcx
	movq	%rcx, 3072(%rbx)
	movq	$0, 96(%rbp)
	movq	104(%r15), %rcx
	movq	%rcx, 3328(%rbx)
	movq	$0, 104(%rbp)
	movq	112(%r15), %rcx
	movq	%rcx, 3584(%rbx)
	movq	$0, 112(%rbp)
	movq	120(%r15), %rcx
	movq	%rcx, 3840(%rbx)
	movq	$0, 120(%rbp)
	movq	128(%r15), %rcx
	movq	%rcx, 4096(%rbx)
	movq	$0, 128(%rbp)
	movq	136(%r15), %rcx
	movq	%rcx, 4352(%rbx)
	movq	$0, 136(%rbp)
	addq	$144, %r15
	addq	$8, %rbx
	addq	$144, %rbp
	decl	%eax
	jne	.LBB1_181
.LBB1_182:                              # %III_hybrid.exit
                                        #   in Loop: Header=BB1_162 Depth=2
	incq	%rsi
	addq	$4608, %rdx             # imm = 0x1200
	addq	$4608, %r13             # imm = 0x1200
	cmpq	192(%rsp), %rsi         # 8-byte Folded Reload
	jl	.LBB1_162
# BB#183:                               # %.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$0, 56(%rsp)            # 4-byte Folded Reload
	movq	176(%rsp), %r14         # 8-byte Reload
	movq	168(%rsp), %r15         # 8-byte Reload
	js	.LBB1_185
# BB#184:                               # %.preheader.split.us.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	movl	$do_layer3.hybridOut, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	76(%rsp), %ebx          # 4-byte Folded Reload
	movl	$do_layer3.hybridOut+256, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+512, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+768, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+1024, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+1280, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+1536, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+1792, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+2048, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+2304, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+2560, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+2816, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+3072, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+3328, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+3584, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+3840, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebp
	addl	%ebx, %ebp
	movl	$do_layer3.hybridOut+4096, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	movl	%eax, %ebx
	addl	%ebp, %ebx
	movl	$do_layer3.hybridOut+4352, %edi
	movq	%r15, %rsi
	movq	%r14, %rdx
	callq	synth_1to1_mono
	addl	%ebx, %eax
	jmp	.LBB1_187
	.p2align	4, 0x90
.LBB1_185:                              # %.preheader.split.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	xorl	%ebp, %ebp
	leaq	92(%rsp), %rcx
	movl	76(%rsp), %eax          # 4-byte Reload
	.p2align	4, 0x90
.LBB1_186:                              # %.preheader.split
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r14), %edx
	movl	%edx, 92(%rsp)
	leaq	do_layer3.hybridOut(%rbp), %rdi
	xorl	%esi, %esi
	movl	%eax, %r12d
	movq	%r15, %rdx
	callq	synth_1to1
	movl	%eax, %ebx
	addl	%r12d, %ebx
	leaq	do_layer3.hybridOut+4608(%rbp), %rdi
	movl	$1, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	synth_1to1
	addl	%ebx, %eax
	leaq	92(%rsp), %rcx
	addq	$256, %rbp              # imm = 0x100
	cmpq	$4608, %rbp             # imm = 0x1200
	jne	.LBB1_186
.LBB1_187:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	184(%rsp), %rcx         # 8-byte Reload
	incq	%rcx
	cmpq	112(%rsp), %rcx         # 8-byte Folded Reload
	leaq	688(%rsp), %rdi
	jl	.LBB1_197
	jmp	.LBB1_239
.LBB1_188:                              #   in Loop: Header=BB1_197 Depth=1
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB1_190
	.p2align	4, 0x90
.LBB1_189:                              # %vector.body
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	do_layer3.hybridIn(,%rsi,8), %xmm0
	movapd	do_layer3.hybridIn+16(,%rsi,8), %xmm1
	addpd	do_layer3.hybridIn+4608(,%rsi,8), %xmm0
	addpd	do_layer3.hybridIn+4624(,%rsi,8), %xmm1
	movapd	%xmm0, do_layer3.hybridIn(,%rsi,8)
	movapd	%xmm1, do_layer3.hybridIn+16(,%rsi,8)
	movapd	do_layer3.hybridIn+32(,%rsi,8), %xmm0
	movapd	do_layer3.hybridIn+48(,%rsi,8), %xmm1
	addpd	do_layer3.hybridIn+4640(,%rsi,8), %xmm0
	addpd	do_layer3.hybridIn+4656(,%rsi,8), %xmm1
	movapd	%xmm0, do_layer3.hybridIn+32(,%rsi,8)
	movapd	%xmm1, do_layer3.hybridIn+48(,%rsi,8)
	addq	$8, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB1_189
.LBB1_190:                              # %middle.block
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpq	%rdx, %rcx
	je	.LBB1_161
# BB#191:                               #   in Loop: Header=BB1_197 Depth=1
	leaq	do_layer3.hybridIn+4608(,%rdx,8), %rcx
	leaq	do_layer3.hybridIn(,%rdx,8), %rdi
	movl	%edx, %esi
	jmp	.LBB1_158
.LBB1_192:                              #   in Loop: Header=BB1_197 Depth=1
	xorl	%esi, %esi
.LBB1_193:                              # %vector.body349.prol.loopexit
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpq	$12, %rbp
	jb	.LBB1_195
	.p2align	4, 0x90
.LBB1_194:                              # %vector.body349
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	do_layer3.hybridIn+4608(,%rsi,8), %xmm0
	movaps	do_layer3.hybridIn+4624(,%rsi,8), %xmm1
	movaps	%xmm0, do_layer3.hybridIn(,%rsi,8)
	movaps	%xmm1, do_layer3.hybridIn+16(,%rsi,8)
	movaps	do_layer3.hybridIn+4640(,%rsi,8), %xmm0
	movaps	do_layer3.hybridIn+4656(,%rsi,8), %xmm1
	movaps	%xmm0, do_layer3.hybridIn+32(,%rsi,8)
	movaps	%xmm1, do_layer3.hybridIn+48(,%rsi,8)
	movaps	do_layer3.hybridIn+4672(,%rsi,8), %xmm0
	movaps	do_layer3.hybridIn+4688(,%rsi,8), %xmm1
	movaps	%xmm0, do_layer3.hybridIn+64(,%rsi,8)
	movaps	%xmm1, do_layer3.hybridIn+80(,%rsi,8)
	movapd	do_layer3.hybridIn+4704(,%rsi,8), %xmm0
	movapd	do_layer3.hybridIn+4720(,%rsi,8), %xmm1
	movapd	%xmm0, do_layer3.hybridIn+96(,%rsi,8)
	movapd	%xmm1, do_layer3.hybridIn+112(,%rsi,8)
	addq	$16, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB1_194
.LBB1_195:                              # %middle.block350
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpq	%rdx, %rcx
	je	.LBB1_161
# BB#196:                               #   in Loop: Header=BB1_197 Depth=1
	leaq	do_layer3.hybridIn+4608(,%rdx,8), %rcx
	leaq	do_layer3.hybridIn(,%rdx,8), %rdi
	movl	%edx, %esi
	jmp	.LBB1_160
	.p2align	4, 0x90
.LBB1_197:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_203 Depth 2
                                        #     Child Loop BB1_213 Depth 2
                                        #     Child Loop BB1_221 Depth 2
                                        #     Child Loop BB1_229 Depth 2
                                        #     Child Loop BB1_40 Depth 2
                                        #     Child Loop BB1_50 Depth 2
                                        #     Child Loop BB1_58 Depth 2
                                        #     Child Loop BB1_67 Depth 2
                                        #     Child Loop BB1_76 Depth 2
                                        #     Child Loop BB1_114 Depth 2
                                        #       Child Loop BB1_123 Depth 3
                                        #       Child Loop BB1_127 Depth 3
                                        #     Child Loop BB1_137 Depth 2
                                        #     Child Loop BB1_141 Depth 2
                                        #     Child Loop BB1_82 Depth 2
                                        #       Child Loop BB1_83 Depth 3
                                        #         Child Loop BB1_86 Depth 4
                                        #       Child Loop BB1_91 Depth 3
                                        #     Child Loop BB1_96 Depth 2
                                        #       Child Loop BB1_105 Depth 3
                                        #       Child Loop BB1_111 Depth 3
                                        #     Child Loop BB1_189 Depth 2
                                        #     Child Loop BB1_158 Depth 2
                                        #     Child Loop BB1_156 Depth 2
                                        #     Child Loop BB1_194 Depth 2
                                        #     Child Loop BB1_160 Depth 2
                                        #     Child Loop BB1_162 Depth 2
                                        #       Child Loop BB1_166 Depth 3
                                        #       Child Loop BB1_176 Depth 3
                                        #       Child Loop BB1_173 Depth 3
                                        #       Child Loop BB1_181 Depth 3
                                        #     Child Loop BB1_186 Depth 2
	movl	%eax, 76(%rsp)          # 4-byte Spill
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	imulq	$120, %rcx, %rcx
	leaq	208(%rsp,%rcx), %r13
	movq	120(%rsp), %rax         # 8-byte Reload
	cmpl	$0, 12(%rax)
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	je	.LBB1_206
# BB#198:                               #   in Loop: Header=BB1_197 Depth=1
	movl	220(%rsp,%rcx), %eax
	movl	n_slen2(,%rax,4), %r15d
	movl	%r15d, %eax
	shrl	$15, %eax
	andl	$1, %eax
	movl	%eax, 284(%rsp,%rcx)
	xorl	%eax, %eax
	cmpl	$2, 224(%rsp,%rcx)
	jne	.LBB1_200
# BB#199:                               #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, 228(%rsp,%rcx)
	movl	$1, %eax
	sbbl	$-1, %eax
.LBB1_200:                              #   in Loop: Header=BB1_197 Depth=1
	movl	%eax, %eax
	movl	%r15d, %ebp
	shrl	$12, %ebp
	andl	$7, %ebp
	movl	%r15d, %r12d
	shrl	$3, %r12d
	shlq	$2, %rbp
	movl	%r15d, %ebx
	andl	$7, %ebx
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	(%rax,%rax,2), %rdx
	movzbl	III_get_scale_factors_2.stab(%rbp,%rdx,8), %r14d
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%r13, 32(%rsp)          # 8-byte Spill
	je	.LBB1_207
# BB#201:                               # %.preheader57.i
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	movq	%rdi, %rcx
	je	.LBB1_205
# BB#202:                               # %.lr.ph.i191.preheader
                                        #   in Loop: Header=BB1_197 Depth=1
	leal	-1(%r14), %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%r14, (%rsp)            # 8-byte Spill
                                        # kill: %R14D<def> %R14D<kill> %R14<kill>
	movq	%rdi, %r13
	.p2align	4, 0x90
.LBB1_203:                              # %.lr.ph.i191
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ebx, %edi
	callq	getbits_fast
	movl	%eax, (%r13)
	addq	$4, %r13
	decl	%r14d
	jne	.LBB1_203
# BB#204:                               # %._crit_edge.i192.loopexit
                                        #   in Loop: Header=BB1_197 Depth=1
	leaq	844(%rsp), %rax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leaq	-152(%rax,%rcx,4), %rcx
	movq	(%rsp), %r14            # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB1_205:                              # %._crit_edge.i192
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%ebx, %r14d
	movq	%r14, %rbx
	jmp	.LBB1_210
	.p2align	4, 0x90
.LBB1_206:                              #   in Loop: Header=BB1_197 Depth=1
	movq	%r13, %rsi
	movq	%rdi, %rbx
	callq	III_get_scale_factors_1
	movl	%eax, %r14d
	jmp	.LBB1_235
	.p2align	4, 0x90
.LBB1_207:                              # %.preheader.i
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	je	.LBB1_209
# BB#208:                               # %.lr.ph65.i
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r14d
	movl	$1, %eax
	cmoval	%r14d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	decl	%r14d
	leaq	844(%rsp), %rax
	leaq	-152(%rax,%r14,4), %rcx
	xorl	%ebx, %ebx
	jmp	.LBB1_210
.LBB1_209:                              #   in Loop: Header=BB1_197 Depth=1
	xorl	%ebx, %ebx
	movq	%rdi, %rcx
	.p2align	4, 0x90
.LBB1_210:                              # %.loopexit.i
                                        #   in Loop: Header=BB1_197 Depth=1
	movl	%r15d, %r13d
	shrl	$6, %r13d
	andl	$7, %r12d
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movzbl	III_get_scale_factors_2.stab+1(%rbp,%rdx,8), %r14d
	je	.LBB1_216
# BB#211:                               # %.preheader57.i.1
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	je	.LBB1_215
# BB#212:                               # %.lr.ph.i191.preheader.1
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%rbx, (%rsp)            # 8-byte Spill
	leal	-1(%r14), %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	%r14d, %ebp
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB1_213:                              # %.lr.ph.i191.1
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB1_213
# BB#214:                               # %._crit_edge.i192.loopexit.1
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	104(%rsp), %rax         # 8-byte Reload
	leaq	4(%rcx,%rax,4), %rcx
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB1_215:                              # %._crit_edge.i192.1
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%r12d, %r14d
	addl	%r14d, %ebx
	jmp	.LBB1_218
	.p2align	4, 0x90
.LBB1_216:                              # %.preheader.i.1
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r14b, %r14b
	je	.LBB1_218
# BB#217:                               # %.lr.ph65.i.1
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r14d
	movl	$1, %eax
	cmoval	%r14d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rcx, %rbp
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rbp, %rcx
	decl	%r14d
	leaq	4(%rcx,%r14,4), %rcx
.LBB1_218:                              # %.loopexit.i.1
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%rbx, %r14
	shrl	$9, %r15d
	andl	$7, %r13d
	movq	16(%rsp), %rbp          # 8-byte Reload
	movzbl	III_get_scale_factors_2.stab+2(%rbp,%rdx,8), %r12d
	je	.LBB1_224
# BB#219:                               # %.preheader57.i.2
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_223
# BB#220:                               # %.lr.ph.i191.preheader.2
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%r14, (%rsp)            # 8-byte Spill
	leal	-1(%r12), %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %ebp
	movq	%rcx, %r14
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB1_221:                              # %.lr.ph.i191.2
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r13d, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB1_221
# BB#222:                               # %._crit_edge.i192.loopexit.2
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%r14, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	4(%rcx,%rax,4), %rcx
	movq	(%rsp), %r14            # 8-byte Reload
	movq	16(%rsp), %rbp          # 8-byte Reload
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB1_223:                              # %._crit_edge.i192.2
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%r13d, %r12d
	addl	%r12d, %r14d
	jmp	.LBB1_226
	.p2align	4, 0x90
.LBB1_224:                              # %.preheader.i.2
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_226
# BB#225:                               # %.lr.ph65.i.2
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r12d
	movl	$1, %eax
	cmoval	%r12d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rcx, %rbx
	callq	memset
	movq	8(%rsp), %rdx           # 8-byte Reload
	movq	%rbx, %rcx
	decl	%r12d
	leaq	4(%rcx,%r12,4), %rcx
.LBB1_226:                              # %.loopexit.i.2
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	32(%rsp), %r13          # 8-byte Reload
	andl	$7, %r15d
	movzbl	III_get_scale_factors_2.stab+3(%rbp,%rdx,8), %r12d
	je	.LBB1_232
# BB#227:                               # %.preheader57.i.3
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_231
# BB#228:                               # %.lr.ph.i191.preheader.3
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%r14, (%rsp)            # 8-byte Spill
	leal	-1(%r12), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%r12d, %ebp
	movq	%rcx, %r14
	movq	%rcx, %rbx
	.p2align	4, 0x90
.LBB1_229:                              # %.lr.ph.i191.3
                                        #   Parent Loop BB1_197 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB1_229
# BB#230:                               # %._crit_edge.i192.loopexit.3
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	%r14, %rcx
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	4(%rcx,%rax,4), %rcx
	movq	(%rsp), %r14            # 8-byte Reload
.LBB1_231:                              # %._crit_edge.i192.3
                                        #   in Loop: Header=BB1_197 Depth=1
	imull	%r15d, %r12d
	addl	%r12d, %r14d
	jmp	.LBB1_234
	.p2align	4, 0x90
.LBB1_232:                              # %.preheader.i.3
                                        #   in Loop: Header=BB1_197 Depth=1
	testb	%r12b, %r12b
	je	.LBB1_234
# BB#233:                               # %.lr.ph65.i.3
                                        #   in Loop: Header=BB1_197 Depth=1
	cmpl	$1, %r12d
	movl	$1, %eax
	cmoval	%r12d, %eax
	decl	%eax
	leaq	4(,%rax,4), %rdx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	movq	%rcx, %rbx
	callq	memset
	movq	%rbx, %rcx
	decl	%r12d
	leaq	4(%rcx,%r12,4), %rcx
.LBB1_234:                              # %.loopexit.i.3
                                        #   in Loop: Header=BB1_197 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	4(,%rax,8), %edx
	addl	$-4, %edx
	orl	$4, %edx
	xorl	%esi, %esi
	movq	%rcx, %rdi
	callq	memset
	leaq	688(%rsp), %rbx
.LBB1_235:                              #   in Loop: Header=BB1_197 Depth=1
	movl	$do_layer3.hybridIn, %edi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	64(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r14d, %r8d
	callq	III_dequantize_sample
	testl	%eax, %eax
	je	.LBB1_33
.LBB1_238:
	movl	76(%rsp), %eax          # 4-byte Reload
	jmp	.LBB1_239
.LBB1_237:
	xorl	%eax, %eax
.LBB1_239:                              # %.loopexit246
	addq	$1000, %rsp             # imm = 0x3E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_240:
	movl	$1, %eax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	jmp	.LBB1_31
.LBB1_241:
	movq	stderr(%rip), %rcx
	movl	$.L.str.1, %edi
	movl	$54, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	do_layer3, .Lfunc_end1-do_layer3
	.cfi_endproc

	.p2align	4, 0x90
	.type	III_get_scale_factors_1,@function
III_get_scale_factors_1:                # @III_get_scale_factors_1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi32:
	.cfi_def_cfa_offset 64
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	movl	12(%rsi), %eax
	movzbl	III_get_scale_factors_1.slen.0(%rax), %r15d
	movzbl	III_get_scale_factors_1.slen.1(%rax), %r14d
	cmpl	$2, 16(%rsi)
	jne	.LBB2_7
# BB#1:
	leal	(%r14,%r15), %eax
	addl	%eax, %eax
	leal	(%rax,%rax,8), %ebx
	cmpl	$0, 20(%rsi)
	je	.LBB2_2
# BB#3:                                 # %.preheader92.preheader
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, (%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 4(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 8(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 12(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 16(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 20(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 24(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 28(%r13)
	addq	$32, %r13
	subl	%r15d, %ebx
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	$9, %ebp
	jmp	.LBB2_4
.LBB2_7:
	movl	(%rsi), %r12d
	testl	%r12d, %r12d
	js	.LBB2_8
# BB#9:
	xorl	%ebx, %ebx
	testb	$8, %r12b
	jne	.LBB2_11
# BB#10:                                # %.preheader98.preheader
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, (%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 4(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 8(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 12(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 16(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 20(%r13)
	leal	(%r15,%r15), %eax
	leal	(%rax,%rax,2), %ebx
.LBB2_11:
	testb	$4, %r12b
	jne	.LBB2_13
# BB#12:                                # %.preheader97.preheader
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 24(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 28(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 32(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 36(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 40(%r13)
	leal	(%r15,%r15,4), %eax
	addl	%eax, %ebx
.LBB2_13:
	testb	$2, %r12b
	jne	.LBB2_15
# BB#14:                                # %.preheader96.preheader
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 44(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 48(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 52(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 56(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 60(%r13)
	leal	(%r14,%r14,4), %eax
	addl	%eax, %ebx
.LBB2_15:
	testb	$1, %r12b
	jne	.LBB2_17
# BB#16:                                # %.preheader95.preheader
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 64(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 68(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 72(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 76(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 80(%r13)
	addq	$84, %r13
	leal	(%r14,%r14,4), %eax
	addl	%eax, %ebx
	jmp	.LBB2_18
.LBB2_2:
	movl	%ebx, 4(%rsp)           # 4-byte Spill
	movl	$18, %ebp
.LBB2_4:                                # %.preheader91
	leal	-1(%rbp), %r12d
	incq	%r12
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB2_5:                                # =>This Inner Loop Header: Depth=1
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, (%rbx)
	addq	$4, %rbx
	decl	%ebp
	jne	.LBB2_5
# BB#6:                                 # %.preheader.preheader
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, (%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 4(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 8(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 12(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 16(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 20(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 24(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 28(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 32(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 36(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 40(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 44(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 48(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 52(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 56(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 60(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 64(%r13,%r12,4)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 68(%r13,%r12,4)
	movl	$0, 72(%r13,%r12,4)
	leaq	80(%r13,%r12,4), %rax
	movl	$0, 76(%r13,%r12,4)
	movl	4(%rsp), %ebx           # 4-byte Reload
	jmp	.LBB2_19
.LBB2_8:                                # %.preheader94.preheader
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, (%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 4(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 8(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 12(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 16(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 20(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 24(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 28(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 32(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 36(%r13)
	movl	%r15d, %edi
	callq	getbits_fast
	movl	%eax, 40(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 44(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 48(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 52(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 56(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 60(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 64(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 68(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 72(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 76(%r13)
	movl	%r14d, %edi
	callq	getbits_fast
	movl	%eax, 80(%r13)
	addq	$84, %r13
	addl	%r15d, %r14d
	leal	(%r14,%r14,4), %eax
	leal	(%r15,%rax,2), %ebx
	jmp	.LBB2_18
.LBB2_17:
	addq	$84, %r13
.LBB2_18:
	movq	%r13, %rax
.LBB2_19:
	movl	$0, (%rax)
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	III_get_scale_factors_1, .Lfunc_end2-III_get_scale_factors_1
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
.LCPI3_1:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.p2align	4, 0x90
	.type	III_dequantize_sample,@function
III_dequantize_sample:                  # @III_dequantize_sample
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 224
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%ecx, 72(%rsp)          # 4-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movl	80(%rdx), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	4(%rdx), %ebx
	movl	8(%rdx), %eax
	movl	68(%rdx), %r14d
	movl	$288, %ecx              # imm = 0x120
	subl	%eax, %ecx
	movl	%ecx, 76(%rsp)          # 4-byte Spill
	movl	%eax, %ecx
	subl	%r14d, %ecx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	jle	.LBB3_3
# BB#1:
	movl	72(%rdx), %edx
	movl	%r14d, 148(%rsp)
	subl	%edx, %eax
	jle	.LBB3_4
# BB#2:
	subl	%r14d, %edx
	movl	%edx, 152(%rsp)
	jmp	.LBB3_6
.LBB3_3:
	movl	%eax, 148(%rsp)
	movl	$0, 152(%rsp)
	movl	%eax, %r14d
	jmp	.LBB3_5
.LBB3_4:
	movl	%ecx, 152(%rsp)
.LBB3_5:
	xorl	%eax, %eax
.LBB3_6:
	incl	28(%rsp)                # 4-byte Folded Spill
	subl	%r8d, %ebx
	sarl	76(%rsp)                # 4-byte Folded Spill
	movl	%eax, 156(%rsp)
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpl	$2, 16(%rax)
	jne	.LBB3_9
# BB#7:
	cmpl	$0, 20(%rax)
	movl	$-1, 124(%rsp)
	je	.LBB3_74
# BB#8:
	movl	$2, 120(%rsp)
	movslq	72(%rsp), %rcx          # 4-byte Folded Reload
	shlq	$3, %rcx
	leaq	map(%rcx,%rcx,2), %rax
	leaq	mapend(%rcx,%rcx,2), %rcx
	movl	$2, %edx
	jmp	.LBB3_75
.LBB3_9:
	cmpl	$0, 76(%rax)
	movl	$pretab1, %eax
	movl	$pretab2, %ecx
	cmovneq	%rax, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	movq	%rax, 160(%rsp)         # 8-byte Spill
	leaq	(%rax,%rax,2), %rax
	movq	map+16(,%rax,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$-1, %r9d
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	xorl	%ecx, %ecx
	xorl	%r13d, %r13d
	movl	$0, (%rsp)              # 4-byte Folded Spill
	movq	96(%rsp), %rdi          # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB3_14
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_13:                               # %._crit_edge691._crit_edge
	movl	152(%rsp,%rcx,4), %r14d
	movq	%rax, %rcx
	testl	%r14d, %r14d
	je	.LBB3_11
.LBB3_14:                               # %.lr.ph690
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movl	24(%rax,%rcx,4), %eax
	shlq	$4, %rax
	leaq	ht+8(%rax), %r8
	leal	-1(%r14), %eax
	addq	%rax, %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	%rdi, 104(%rsp)         # 8-byte Spill
	movq	%rdi, %rbp
	movq	%r8, 48(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB3_15:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_19 Depth 2
	testl	%r13d, %r13d
	jne	.LBB3_17
# BB#16:                                #   in Loop: Header=BB3_15 Depth=1
	movq	40(%rsp), %rsi          # 8-byte Reload
	movl	(%rsi), %r13d
	movl	4(%rsi), %eax
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	112(%rax), %rax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %edx
	addq	$4, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	addl	(%rcx), %edx
	addq	$4, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	28(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movslq	%edx, %rcx
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
	addq	$8, %rsi
	movq	%rsi, 40(%rsp)          # 8-byte Spill
.LBB3_17:                               #   in Loop: Header=BB3_15 Depth=1
	movq	(%r8), %rax
	jmp	.LBB3_19
	.p2align	4, 0x90
.LBB3_18:                               # %.lr.ph673
                                        #   in Loop: Header=BB3_19 Depth=2
	addq	$2, %rax
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	movswq	%r12w, %rcx
	addq	%rcx, %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	testb	%sil, %sil
	cmovsq	%rdx, %rax
	decl	%ebx
.LBB3_19:                               # %.lr.ph673
                                        #   Parent Loop BB3_15 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax), %r12d
	testw	%r12w, %r12w
	js	.LBB3_18
# BB#20:                                # %._crit_edge674
                                        #   in Loop: Header=BB3_15 Depth=1
	movswl	%r12w, %r15d
	movl	%r15d, %eax
	sarl	$4, %eax
	je	.LBB3_24
# BB#21:                                # %._crit_edge674
                                        #   in Loop: Header=BB3_15 Depth=1
	cmpl	$15, %eax
	jne	.LBB3_25
# BB#22:                                #   in Loop: Header=BB3_15 Depth=1
	movl	-8(%r8), %edi
	decl	%ebx
	subl	%edi, %ebx
	callq	getbits
	addl	$15, %eax
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	cltq
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	80(%rsp), %xmm0         # 16-byte Folded Reload
	js	.LBB3_28
# BB#23:                                #   in Loop: Header=BB3_15 Depth=1
	movsd	%xmm0, (%rbp)
	jmp	.LBB3_29
	.p2align	4, 0x90
.LBB3_24:                               #   in Loop: Header=BB3_15 Depth=1
	movq	$0, (%rbp)
	andb	$15, %r12b
	jne	.LBB3_30
	jmp	.LBB3_33
	.p2align	4, 0x90
.LBB3_25:                               #   in Loop: Header=BB3_15 Depth=1
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	cltq
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	jns	.LBB3_27
# BB#26:                                #   in Loop: Header=BB3_15 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_27:                               #   in Loop: Header=BB3_15 Depth=1
	mulsd	80(%rsp), %xmm0         # 16-byte Folded Reload
	movsd	%xmm0, (%rbp)
	decl	%ebx
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r9d
	andb	$15, %r12b
	jne	.LBB3_30
	jmp	.LBB3_33
.LBB3_28:                               #   in Loop: Header=BB3_15 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
	movlpd	%xmm0, (%rbp)
.LBB3_29:                               #   in Loop: Header=BB3_15 Depth=1
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r9d
	movq	48(%rsp), %r8           # 8-byte Reload
	andb	$15, %r12b
	je	.LBB3_33
.LBB3_30:                               #   in Loop: Header=BB3_15 Depth=1
	andl	$15, %r15d
	cmpb	$15, %r12b
	jne	.LBB3_34
# BB#31:                                #   in Loop: Header=BB3_15 Depth=1
	movl	-8(%r8), %edi
	decl	%ebx
	subl	%edi, %ebx
	callq	getbits
	addl	%r15d, %eax
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	cltq
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	80(%rsp), %xmm0         # 16-byte Folded Reload
	js	.LBB3_37
# BB#32:                                #   in Loop: Header=BB3_15 Depth=1
	movsd	%xmm0, 8(%rbp)
	jmp	.LBB3_38
	.p2align	4, 0x90
.LBB3_33:                               #   in Loop: Header=BB3_15 Depth=1
	movq	$0, 8(%rbp)
	jmp	.LBB3_39
	.p2align	4, 0x90
.LBB3_34:                               #   in Loop: Header=BB3_15 Depth=1
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%dl, %dl
	movl	%r15d, %eax
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	jns	.LBB3_36
# BB#35:                                #   in Loop: Header=BB3_15 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_36:                               #   in Loop: Header=BB3_15 Depth=1
	mulsd	80(%rsp), %xmm0         # 16-byte Folded Reload
	movsd	%xmm0, 8(%rbp)
	decl	%ebx
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r9d
	jmp	.LBB3_39
.LBB3_37:                               #   in Loop: Header=BB3_15 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
	movlpd	%xmm0, 8(%rbp)
.LBB3_38:                               #   in Loop: Header=BB3_15 Depth=1
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r9d
	movq	48(%rsp), %r8           # 8-byte Reload
.LBB3_39:                               #   in Loop: Header=BB3_15 Depth=1
	addq	$16, %rbp
	decl	%r13d
	decl	%r14d
	jne	.LBB3_15
# BB#10:                                # %._crit_edge691.loopexit
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	128(%rsp), %rax         # 8-byte Reload
	leaq	16(%rdi,%rax,8), %rdi
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	136(%rsp), %rcx         # 8-byte Reload
	jmp	.LBB3_12
	.p2align	4, 0x90
.LBB3_11:
	movq	32(%rsp), %r14          # 8-byte Reload
.LBB3_12:                               # %._crit_edge691
	leaq	1(%rcx), %rax
	cmpq	$3, %rax
	jne	.LBB3_13
# BB#40:                                # %.preheader546
	testl	%ebx, %ebx
	jle	.LBB3_153
# BB#41:                                # %.preheader546
	cmpl	$0, 76(%rsp)            # 4-byte Folded Reload
	je	.LBB3_153
	.p2align	4, 0x90
.LBB3_42:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_44 Depth 2
	movl	84(%r14), %eax
	shlq	$4, %rax
	movq	htc+8(%rax), %rdx
	movw	(%rdx), %ax
	movq	%rdi, %r8
	testw	%ax, %ax
	jns	.LBB3_46
	.p2align	4, 0x90
.LBB3_44:                               # %.lr.ph639
                                        #   Parent Loop BB3_42 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%ebx, %ebx
	jle	.LBB3_45
# BB#43:                                # %.backedge545
                                        #   in Loop: Header=BB3_44 Depth=2
	addq	$2, %rdx
	decl	%ebx
	movq	wordpointer(%rip), %rsi
	movzbl	(%rsi), %edi
	movl	bitindex(%rip), %ecx
	shll	%cl, %edi
	incl	%ecx
	movl	%ecx, %ebp
	sarl	$3, %ebp
	movslq	%ebp, %rbp
	addq	%rsi, %rbp
	movq	%rbp, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	movswq	%ax, %rax
	addq	%rax, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	testb	%dil, %dil
	cmovsq	%rcx, %rdx
	movw	(%rdx), %ax
	testw	%ax, %ax
	js	.LBB3_44
	jmp	.LBB3_46
.LBB3_45:                               #   in Loop: Header=BB3_42 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_46:                               # %.loopexit735
                                        #   in Loop: Header=BB3_42 Depth=1
	cwtl
	testl	%r13d, %r13d
	jne	.LBB3_48
# BB#47:                                #   in Loop: Header=BB3_42 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %r13d
	movl	4(%rdx), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	addq	$8, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	112(%r14), %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %esi
	addq	$4, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	addl	(%rcx), %esi
	addq	$4, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	28(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	movsd	(%rdx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
.LBB3_48:                               #   in Loop: Header=BB3_42 Depth=1
	movq	%r8, %rdi
	xorpd	%xmm0, %xmm0
	testb	$8, %al
	jne	.LBB3_50
# BB#49:                                #   in Loop: Header=BB3_42 Depth=1
	xorpd	%xmm1, %xmm1
	jmp	.LBB3_54
	.p2align	4, 0x90
.LBB3_50:                               #   in Loop: Header=BB3_42 Depth=1
	testl	%ebx, %ebx
	jle	.LBB3_152
# BB#51:                                #   in Loop: Header=BB3_42 Depth=1
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %ebp
	sarl	$3, %ebp
	movslq	%ebp, %rbp
	addq	%rdx, %rbp
	movq	%rbp, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	movapd	80(%rsp), %xmm1         # 16-byte Reload
	jns	.LBB3_53
# BB#52:                                #   in Loop: Header=BB3_42 Depth=1
	movapd	80(%rsp), %xmm1         # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm1
.LBB3_53:                               #   in Loop: Header=BB3_42 Depth=1
	decl	%ebx
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, %r9d
.LBB3_54:                               #   in Loop: Header=BB3_42 Depth=1
	movsd	%xmm1, (%rdi)
	testb	$4, %al
	je	.LBB3_59
# BB#55:                                #   in Loop: Header=BB3_42 Depth=1
	testl	%ebx, %ebx
	jle	.LBB3_149
# BB#56:                                #   in Loop: Header=BB3_42 Depth=1
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %ebp
	sarl	$3, %ebp
	movslq	%ebp, %rbp
	addq	%rdx, %rbp
	movq	%rbp, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	jns	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_42 Depth=1
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_58:                               #   in Loop: Header=BB3_42 Depth=1
	decl	%ebx
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, %r9d
.LBB3_59:                               #   in Loop: Header=BB3_42 Depth=1
	decl	%r13d
	movsd	%xmm0, 8(%rdi)
	jne	.LBB3_61
# BB#60:                                #   in Loop: Header=BB3_42 Depth=1
	movq	40(%rsp), %rdx          # 8-byte Reload
	movl	(%rdx), %r13d
	movl	4(%rdx), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	addq	$8, %rdx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	112(%r14), %rdx
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %esi
	addq	$4, %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	movq	64(%rsp), %rcx          # 8-byte Reload
	addl	(%rcx), %esi
	addq	$4, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	28(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	movsd	(%rdx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, 80(%rsp)         # 16-byte Spill
.LBB3_61:                               #   in Loop: Header=BB3_42 Depth=1
	xorpd	%xmm0, %xmm0
	testb	$2, %al
	jne	.LBB3_63
# BB#62:                                #   in Loop: Header=BB3_42 Depth=1
	xorpd	%xmm1, %xmm1
	jmp	.LBB3_67
	.p2align	4, 0x90
.LBB3_63:                               #   in Loop: Header=BB3_42 Depth=1
	testl	%ebx, %ebx
	jle	.LBB3_150
# BB#64:                                #   in Loop: Header=BB3_42 Depth=1
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %ebp
	sarl	$3, %ebp
	movslq	%ebp, %rbp
	addq	%rdx, %rbp
	movq	%rbp, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	movapd	80(%rsp), %xmm1         # 16-byte Reload
	jns	.LBB3_66
# BB#65:                                #   in Loop: Header=BB3_42 Depth=1
	movapd	80(%rsp), %xmm1         # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm1
.LBB3_66:                               #   in Loop: Header=BB3_42 Depth=1
	decl	%ebx
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ecx, %r9d
.LBB3_67:                               #   in Loop: Header=BB3_42 Depth=1
	movsd	%xmm1, 16(%rdi)
	testb	$1, %al
	je	.LBB3_72
# BB#68:                                #   in Loop: Header=BB3_42 Depth=1
	testl	%ebx, %ebx
	jle	.LBB3_151
# BB#69:                                #   in Loop: Header=BB3_42 Depth=1
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%dl, %dl
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	jns	.LBB3_71
# BB#70:                                #   in Loop: Header=BB3_42 Depth=1
	movapd	80(%rsp), %xmm0         # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_71:                               #   in Loop: Header=BB3_42 Depth=1
	decl	%ebx
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r9d
.LBB3_72:                               #   in Loop: Header=BB3_42 Depth=1
	movsd	%xmm0, 24(%rdi)
	addq	$32, %rdi
	testl	%ebx, %ebx
	jle	.LBB3_153
# BB#73:                                #   in Loop: Header=BB3_42 Depth=1
	decl	%r13d
	decl	76(%rsp)                # 4-byte Folded Spill
	jne	.LBB3_42
	jmp	.LBB3_153
.LBB3_74:
	movl	$-1, 120(%rsp)
	movslq	72(%rsp), %rcx          # 4-byte Folded Reload
	shlq	$3, %rcx
	leaq	map+8(%rcx,%rcx,2), %rax
	leaq	mapend+8(%rcx,%rcx,2), %rcx
	movl	$-1, %edx
.LBB3_75:
	movl	%edx, 116(%rsp)
	movl	%edx, 112(%rsp)
	movq	(%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movq	(%rcx), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	112(%rax), %rax
	movq	%rax, 136(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, (%rsp)           # 16-byte Spill
	xorl	%eax, %eax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movq	96(%rsp), %r15          # 8-byte Reload
	xorl	%ebp, %ebp
	xorl	%r9d, %r9d
	xorl	%r11d, %r11d
	xorl	%r8d, %r8d
	testl	%r14d, %r14d
	jne	.LBB3_78
	jmp	.LBB3_76
	.p2align	4, 0x90
.LBB3_77:                               # %._crit_edge612._crit_edge
                                        #   in Loop: Header=BB3_76 Depth=1
	movl	152(%rsp,%rcx,4), %r14d
	movq	%rax, 128(%rsp)         # 8-byte Spill
	testl	%r14d, %r14d
	je	.LBB3_76
.LBB3_78:                               # %.lr.ph611
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	128(%rsp), %rcx         # 8-byte Reload
	movl	24(%rax,%rcx,4), %eax
	shlq	$4, %rax
	leaq	ht+8(%rax), %rax
	movq	%rax, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB3_79:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_83 Depth 2
	testl	%ebp, %ebp
	jne	.LBB3_81
# BB#80:                                # %.sink.split
                                        #   in Loop: Header=BB3_79 Depth=1
	movq	104(%rsp), %rdx         # 8-byte Reload
	movl	(%rdx), %ebp
	movl	12(%rdx), %r9d
	movslq	4(%rdx), %rax
	movq	96(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %r15
	movslq	8(%rdx), %r11
	addq	$16, %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	cmpq	$3, %r11
	setne	%al
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	88(%rcx,%r11,8), %rcx
	cmoveq	136(%rsp), %rcx         # 8-byte Folded Reload
	leal	1(%rax,%rax), %r8d
	movq	(%rcx), %rax
	movq	64(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx), %edx
	addq	$4, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movl	28(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movslq	%edx, %rcx
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, (%rsp)           # 16-byte Spill
.LBB3_81:                               #   in Loop: Header=BB3_79 Depth=1
	movq	56(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	jmp	.LBB3_83
	.p2align	4, 0x90
.LBB3_82:                               # %.lr.ph594
                                        #   in Loop: Header=BB3_83 Depth=2
	addq	$2, %rax
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	movswq	%r12w, %rcx
	addq	%rcx, %rcx
	movq	%rax, %rdx
	subq	%rcx, %rdx
	testb	%sil, %sil
	cmovsq	%rdx, %rax
	decl	%ebx
.LBB3_83:                               # %.lr.ph594
                                        #   Parent Loop BB3_79 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rax), %r12d
	testw	%r12w, %r12w
	js	.LBB3_82
# BB#84:                                # %._crit_edge595
                                        #   in Loop: Header=BB3_79 Depth=1
	movswl	%r12w, %r13d
	movl	%r13d, %eax
	sarl	$4, %eax
	movq	%r14, 80(%rsp)          # 8-byte Spill
	je	.LBB3_88
# BB#85:                                # %._crit_edge595
                                        #   in Loop: Header=BB3_79 Depth=1
	cmpl	$15, %eax
	jne	.LBB3_89
# BB#86:                                #   in Loop: Header=BB3_79 Depth=1
	movl	%r8d, 40(%rsp)          # 4-byte Spill
	movq	%r11, 48(%rsp)          # 8-byte Spill
	movslq	%r11d, %rax
	movl	%r9d, %r14d
	movl	%r9d, 112(%rsp,%rax,4)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	-8(%rax), %edi
	decl	%ebx
	subl	%edi, %ebx
	callq	getbits
	addl	$15, %eax
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	cltq
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	(%rsp), %xmm0           # 16-byte Folded Reload
	js	.LBB3_92
# BB#87:                                #   in Loop: Header=BB3_79 Depth=1
	movsd	%xmm0, (%r15)
	jmp	.LBB3_93
	.p2align	4, 0x90
.LBB3_88:                               #   in Loop: Header=BB3_79 Depth=1
	movq	$0, (%r15)
	jmp	.LBB3_94
	.p2align	4, 0x90
.LBB3_89:                               #   in Loop: Header=BB3_79 Depth=1
	movslq	%r11d, %rcx
	movl	%r9d, 112(%rsp,%rcx,4)
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	cltq
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	jns	.LBB3_91
# BB#90:                                #   in Loop: Header=BB3_79 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_91:                               #   in Loop: Header=BB3_79 Depth=1
	mulsd	(%rsp), %xmm0           # 16-byte Folded Reload
	movsd	%xmm0, (%r15)
	decl	%ebx
	jmp	.LBB3_94
.LBB3_92:                               #   in Loop: Header=BB3_79 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
	movlpd	%xmm0, (%r15)
.LBB3_93:                               #   in Loop: Header=BB3_79 Depth=1
	movl	40(%rsp), %r8d          # 4-byte Reload
	movl	%r14d, %r9d
	movq	48(%rsp), %r11          # 8-byte Reload
.LBB3_94:                               #   in Loop: Header=BB3_79 Depth=1
	movslq	%r8d, %r14
	leaq	(%r15,%r14,8), %r15
	andb	$15, %r12b
	je	.LBB3_98
# BB#95:                                #   in Loop: Header=BB3_79 Depth=1
	andl	$15, %r13d
	cmpb	$15, %r12b
	jne	.LBB3_99
# BB#96:                                #   in Loop: Header=BB3_79 Depth=1
	movl	%r8d, 40(%rsp)          # 4-byte Spill
	movq	%r11, 48(%rsp)          # 8-byte Spill
	movslq	%r11d, %rax
	movl	%r9d, %r12d
	movl	%r9d, 112(%rsp,%rax,4)
	movq	56(%rsp), %rax          # 8-byte Reload
	movl	-8(%rax), %edi
	decl	%ebx
	subl	%edi, %ebx
	callq	getbits
	addl	%r13d, %eax
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	cltq
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	mulsd	(%rsp), %xmm0           # 16-byte Folded Reload
	js	.LBB3_102
# BB#97:                                #   in Loop: Header=BB3_79 Depth=1
	movsd	%xmm0, (%r15)
	jmp	.LBB3_103
	.p2align	4, 0x90
.LBB3_98:                               #   in Loop: Header=BB3_79 Depth=1
	movq	$0, (%r15)
	jmp	.LBB3_104
	.p2align	4, 0x90
.LBB3_99:                               #   in Loop: Header=BB3_79 Depth=1
	movslq	%r11d, %rax
	movl	%r9d, 112(%rsp,%rax,4)
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %edx
	movl	bitindex(%rip), %ecx
	shll	%cl, %edx
	incl	%ecx
	movl	%ecx, %esi
	sarl	$3, %esi
	movslq	%esi, %rsi
	addq	%rax, %rsi
	movq	%rsi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%dl, %dl
	movl	%r13d, %eax
	movsd	ispow(,%rax,8), %xmm0   # xmm0 = mem[0],zero
	jns	.LBB3_101
# BB#100:                               #   in Loop: Header=BB3_79 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_101:                              #   in Loop: Header=BB3_79 Depth=1
	mulsd	(%rsp), %xmm0           # 16-byte Folded Reload
	movsd	%xmm0, (%r15)
	decl	%ebx
	jmp	.LBB3_104
.LBB3_102:                              #   in Loop: Header=BB3_79 Depth=1
	xorpd	.LCPI3_0(%rip), %xmm0
	movlpd	%xmm0, (%r15)
.LBB3_103:                              #   in Loop: Header=BB3_79 Depth=1
	movl	40(%rsp), %r8d          # 4-byte Reload
	movl	%r12d, %r9d
	movq	48(%rsp), %r11          # 8-byte Reload
.LBB3_104:                              #   in Loop: Header=BB3_79 Depth=1
	leaq	(%r15,%r14,8), %r15
	decl	%ebp
	movq	80(%rsp), %r14          # 8-byte Reload
	decl	%r14d
	jne	.LBB3_79
.LBB3_76:                               # %._crit_edge612
                                        # =>This Inner Loop Header: Depth=1
	movq	128(%rsp), %rcx         # 8-byte Reload
	leaq	1(%rcx), %rax
	cmpq	$2, %rax
	jne	.LBB3_77
# BB#105:                               # %.preheader544
	testl	%ebx, %ebx
	jle	.LBB3_142
# BB#106:                               # %.preheader544
	movq	%r11, %r13
	movq	%r15, %r10
	movl	%r8d, %r12d
	movl	%ebp, %r15d
	movl	76(%rsp), %r11d         # 4-byte Reload
	testl	%r11d, %r11d
	je	.LBB3_143
# BB#107:
	movq	64(%rsp), %r8           # 8-byte Reload
	movq	104(%rsp), %r14         # 8-byte Reload
	.p2align	4, 0x90
.LBB3_108:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_110 Depth 2
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	84(%rax), %eax
	shlq	$4, %rax
	movq	htc+8(%rax), %rdx
	jmp	.LBB3_110
	.p2align	4, 0x90
.LBB3_109:                              # %.backedge
                                        #   in Loop: Header=BB3_110 Depth=2
	addq	$2, %rdx
	decl	%ebx
	movq	wordpointer(%rip), %rsi
	movzbl	(%rsi), %edi
	movl	bitindex(%rip), %ecx
	shll	%cl, %edi
	incl	%ecx
	movl	%ecx, %ebp
	sarl	$3, %ebp
	movslq	%ebp, %rbp
	addq	%rsi, %rbp
	movq	%rbp, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	movswq	%ax, %rax
	addq	%rax, %rax
	movq	%rdx, %rcx
	subq	%rax, %rcx
	testb	%dil, %dil
	cmovsq	%rcx, %rdx
.LBB3_110:                              #   Parent Loop BB3_108 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movw	(%rdx), %ax
	testw	%ax, %ax
	jns	.LBB3_113
# BB#111:                               # %.lr.ph559
                                        #   in Loop: Header=BB3_110 Depth=2
	testl	%ebx, %ebx
	jg	.LBB3_109
# BB#112:                               #   in Loop: Header=BB3_108 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_113:                              # %.loopexit
                                        #   in Loop: Header=BB3_108 Depth=1
	cwtl
	testl	%r15d, %r15d
	jne	.LBB3_115
# BB#114:                               # %.sink.split526
                                        #   in Loop: Header=BB3_108 Depth=1
	movl	(%r14), %r15d
	movl	12(%r14), %r9d
	movslq	4(%r14), %rcx
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r10
	movslq	8(%r14), %rdx
	addq	$16, %r14
	xorl	%ecx, %ecx
	cmpq	$3, %rdx
	setne	%cl
	movq	%rdx, %r13
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	88(%rsi,%rdx,8), %rdx
	cmoveq	136(%rsp), %rdx         # 8-byte Folded Reload
	leal	1(%rcx,%rcx), %r12d
	movq	(%rdx), %rdx
	movl	(%r8), %esi
	addq	$4, %r8
	movl	28(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	movsd	(%rdx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, (%rsp)           # 16-byte Spill
.LBB3_115:                              #   in Loop: Header=BB3_108 Depth=1
	decl	%r15d
	xorpd	%xmm0, %xmm0
	testb	$8, %al
	jne	.LBB3_117
# BB#116:                               #   in Loop: Header=BB3_108 Depth=1
	xorpd	%xmm1, %xmm1
	jmp	.LBB3_121
	.p2align	4, 0x90
.LBB3_117:                              #   in Loop: Header=BB3_108 Depth=1
	movslq	%r13d, %rcx
	movl	%r9d, 112(%rsp,%rcx,4)
	testl	%ebx, %ebx
	jle	.LBB3_144
# BB#118:                               #   in Loop: Header=BB3_108 Depth=1
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	movapd	(%rsp), %xmm1           # 16-byte Reload
	jns	.LBB3_120
# BB#119:                               #   in Loop: Header=BB3_108 Depth=1
	movapd	(%rsp), %xmm1           # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm1
.LBB3_120:                              #   in Loop: Header=BB3_108 Depth=1
	decl	%ebx
.LBB3_121:                              #   in Loop: Header=BB3_108 Depth=1
	movsd	%xmm1, (%r10)
	movslq	%r12d, %rdx
	leaq	(%r10,%rdx,8), %r10
	testb	$4, %al
	je	.LBB3_126
# BB#122:                               #   in Loop: Header=BB3_108 Depth=1
	movslq	%r13d, %rcx
	movl	%r9d, 112(%rsp,%rcx,4)
	testl	%ebx, %ebx
	jle	.LBB3_144
# BB#123:                               #   in Loop: Header=BB3_108 Depth=1
	movq	wordpointer(%rip), %rsi
	movzbl	(%rsi), %edi
	movl	bitindex(%rip), %ecx
	shll	%cl, %edi
	incl	%ecx
	movl	%ecx, %ebp
	sarl	$3, %ebp
	movslq	%ebp, %rbp
	addq	%rsi, %rbp
	movq	%rbp, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%dil, %dil
	movapd	(%rsp), %xmm0           # 16-byte Reload
	jns	.LBB3_125
# BB#124:                               #   in Loop: Header=BB3_108 Depth=1
	movapd	(%rsp), %xmm0           # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_125:                              #   in Loop: Header=BB3_108 Depth=1
	decl	%ebx
.LBB3_126:                              #   in Loop: Header=BB3_108 Depth=1
	movsd	%xmm0, (%r10)
	testl	%r15d, %r15d
	je	.LBB3_128
# BB#127:                               #   in Loop: Header=BB3_108 Depth=1
	leaq	(%r10,%rdx,8), %r10
	jmp	.LBB3_129
	.p2align	4, 0x90
.LBB3_128:                              # %.sink.split526.2
                                        #   in Loop: Header=BB3_108 Depth=1
	movl	(%r14), %r15d
	movl	12(%r14), %r9d
	movslq	4(%r14), %rcx
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rcx,8), %r10
	movslq	8(%r14), %rdx
	addq	$16, %r14
	xorl	%ecx, %ecx
	cmpq	$3, %rdx
	setne	%cl
	movq	%rdx, %r13
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	88(%rsi,%rdx,8), %rdx
	cmoveq	136(%rsp), %rdx         # 8-byte Folded Reload
	leal	1(%rcx,%rcx), %r12d
	movq	(%rdx), %rdx
	movl	(%r8), %esi
	addq	$4, %r8
	movl	28(%rsp), %ecx          # 4-byte Reload
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	%esi, %rcx
	movsd	(%rdx,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movapd	%xmm0, (%rsp)           # 16-byte Spill
.LBB3_129:                              #   in Loop: Header=BB3_108 Depth=1
	decl	%r15d
	xorpd	%xmm0, %xmm0
	testb	$2, %al
	jne	.LBB3_131
# BB#130:                               #   in Loop: Header=BB3_108 Depth=1
	xorpd	%xmm1, %xmm1
	jmp	.LBB3_135
	.p2align	4, 0x90
.LBB3_131:                              #   in Loop: Header=BB3_108 Depth=1
	movslq	%r13d, %rcx
	movl	%r9d, 112(%rsp,%rcx,4)
	testl	%ebx, %ebx
	jle	.LBB3_144
# BB#132:                               #   in Loop: Header=BB3_108 Depth=1
	movq	wordpointer(%rip), %rdx
	movzbl	(%rdx), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rdx, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	movapd	(%rsp), %xmm1           # 16-byte Reload
	jns	.LBB3_134
# BB#133:                               #   in Loop: Header=BB3_108 Depth=1
	movapd	(%rsp), %xmm1           # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm1
.LBB3_134:                              #   in Loop: Header=BB3_108 Depth=1
	decl	%ebx
.LBB3_135:                              #   in Loop: Header=BB3_108 Depth=1
	movsd	%xmm1, (%r10)
	movslq	%r12d, %rdx
	leaq	(%r10,%rdx,8), %r10
	testb	$1, %al
	je	.LBB3_140
# BB#136:                               #   in Loop: Header=BB3_108 Depth=1
	movslq	%r13d, %rax
	movl	%r9d, 112(%rsp,%rax,4)
	testl	%ebx, %ebx
	jle	.LBB3_144
# BB#137:                               #   in Loop: Header=BB3_108 Depth=1
	movq	wordpointer(%rip), %rax
	movzbl	(%rax), %esi
	movl	bitindex(%rip), %ecx
	shll	%cl, %esi
	incl	%ecx
	movl	%ecx, %edi
	sarl	$3, %edi
	movslq	%edi, %rdi
	addq	%rax, %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$7, %ecx
	movl	%ecx, bitindex(%rip)
	testb	%sil, %sil
	movapd	(%rsp), %xmm0           # 16-byte Reload
	jns	.LBB3_139
# BB#138:                               #   in Loop: Header=BB3_108 Depth=1
	movapd	(%rsp), %xmm0           # 16-byte Reload
	xorpd	.LCPI3_0(%rip), %xmm0
.LBB3_139:                              #   in Loop: Header=BB3_108 Depth=1
	decl	%ebx
.LBB3_140:                              #   in Loop: Header=BB3_108 Depth=1
	movsd	%xmm0, (%r10)
	leaq	(%r10,%rdx,8), %r10
	testl	%ebx, %ebx
	jle	.LBB3_144
# BB#141:                               #   in Loop: Header=BB3_108 Depth=1
	decl	%r11d
	jne	.LBB3_108
	jmp	.LBB3_144
.LBB3_142:
	movq	%r15, %r10
	movl	%r8d, %r12d
	movl	%ebp, %r15d
.LBB3_143:                              # %.preheader543
	movq	104(%rsp), %r14         # 8-byte Reload
.LBB3_144:                              # %.preheader543
	cmpq	160(%rsp), %r14         # 8-byte Folded Reload
	movq	96(%rsp), %rdx          # 8-byte Reload
	jae	.LBB3_148
	.p2align	4, 0x90
.LBB3_145:                              # %.lr.ph555
                                        # =>This Inner Loop Header: Depth=1
	testl	%r15d, %r15d
	jne	.LBB3_147
# BB#146:                               #   in Loop: Header=BB3_145 Depth=1
	movl	(%r14), %r15d
	movslq	4(%r14), %rax
	xorl	%ecx, %ecx
	cmpl	$3, 8(%r14)
	leaq	(%rdx,%rax,8), %r10
	setne	%cl
	leal	1(%rcx,%rcx), %r12d
	addq	$16, %r14
.LBB3_147:                              #   in Loop: Header=BB3_145 Depth=1
	decl	%r15d
	movq	$0, (%r10)
	movslq	%r12d, %rax
	leaq	(%r10,%rax,8), %rcx
	movq	$0, (%r10,%rax,8)
	leaq	(%rcx,%rax,8), %r10
	cmpq	160(%rsp), %r14         # 8-byte Folded Reload
	jb	.LBB3_145
.LBB3_148:                              # %._crit_edge556
	movdqa	112(%rsp), %xmm0
	movdqa	.LCPI3_1(%rip), %xmm1   # xmm1 = [1,1,1,1]
	paddd	%xmm0, %xmm1
	movq	32(%rsp), %rdi          # 8-byte Reload
	movdqu	%xmm1, 48(%rdi)
	movd	%xmm0, %eax
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	movd	%xmm2, %ecx
	cmpl	%ecx, %eax
	cmovgel	%eax, %ecx
	pshufd	$78, %xmm0, %xmm0       # xmm0 = xmm0[2,3,0,1]
	movd	%xmm0, %eax
	cmpl	%eax, %ecx
	cmovll	%eax, %ecx
	movslq	72(%rsp), %rax          # 4-byte Folded Reload
	imulq	$92, %rax, %rdx
	imulq	$56, %rax, %rax
	cmpl	$-1, %ecx
	pshufd	$231, %xmm1, %xmm0      # xmm0 = xmm1[3,1,2,3]
	movd	%xmm0, %esi
	movslq	%esi, %rsi
	leaq	longLimit(%rdx,%rsi,4), %rdx
	movslq	%ecx, %rcx
	leaq	shortLimit+4(%rax,%rcx,4), %rax
	cmoveq	%rdx, %rax
	movl	(%rax), %eax
	movl	%eax, 64(%rdi)
	cmpl	$17, %ebx
	jge	.LBB3_156
	jmp	.LBB3_159
.LBB3_149:
	addq	$8, %rdi
	jmp	.LBB3_152
.LBB3_150:
	addq	$16, %rdi
	jmp	.LBB3_152
.LBB3_151:
	addq	$24, %rdi
.LBB3_152:
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, %r9d
.LBB3_153:                              # %._crit_edge667
	movq	96(%rsp), %rdx          # 8-byte Reload
	addq	$4608, %rdx             # imm = 0x1200
	subq	%rdi, %rdx
	shrq	$4, %rdx
	testl	%edx, %edx
	je	.LBB3_155
# BB#154:                               # %.lr.ph635.preheader
	shlq	$4, %rdx
	movabsq	$68719476720, %rax      # imm = 0xFFFFFFFF0
	addq	%rax, %rdx
	andq	%rax, %rdx
	addq	$16, %rdx
	xorl	%esi, %esi
	movq	%r9, %rbp
	callq	memset
	movq	%rbp, %r9
.LBB3_155:                              # %._crit_edge636
	incl	%r9d
	movl	%r9d, 60(%r14)
	imulq	$92, 160(%rsp), %rax    # 8-byte Folded Reload
	movl	longLimit(%rax,%r9,4), %eax
	movl	%eax, 64(%r14)
	cmpl	$17, %ebx
	jl	.LBB3_159
.LBB3_156:                              # %.lr.ph.preheader
	leal	-17(%rbx), %r14d
	andl	$-16, %r14d
	movl	%ebx, %ebp
	.p2align	4, 0x90
.LBB3_157:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$16, %edi
	callq	getbits
	addl	$-16, %ebp
	cmpl	$16, %ebp
	jg	.LBB3_157
# BB#158:                               # %._crit_edge.loopexit
	addl	$-16, %ebx
	subl	%r14d, %ebx
.LBB3_159:                              # %._crit_edge
	testl	%ebx, %ebx
	jle	.LBB3_161
# BB#160:
	movl	%ebx, %edi
	callq	getbits
	xorl	%eax, %eax
	jmp	.LBB3_162
.LBB3_161:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	js	.LBB3_163
.LBB3_162:
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_163:
	movq	stderr(%rip), %rdi
	negl	%ebx
	movl	$.L.str.2, %esi
	xorl	%eax, %eax
	movl	%ebx, %edx
	callq	fprintf
	movl	$1, %eax
	jmp	.LBB3_162
.Lfunc_end3:
	.size	III_dequantize_sample, .Lfunc_end3-III_dequantize_sample
	.cfi_endproc

	.p2align	4, 0x90
	.type	dct36,@function
dct36:                                  # @dct36
	.cfi_startproc
# BB#0:
	movsd	128(%rdi), %xmm13       # xmm13 = mem[0],zero
	movsd	136(%rdi), %xmm14       # xmm14 = mem[0],zero
	addsd	%xmm13, %xmm14
	movsd	120(%rdi), %xmm0        # xmm0 = mem[0],zero
	movsd	104(%rdi), %xmm10       # xmm10 = mem[0],zero
	movsd	112(%rdi), %xmm1        # xmm1 = mem[0],zero
	movapd	%xmm0, %xmm15
	addsd	%xmm1, %xmm15
	addsd	%xmm10, %xmm1
	movsd	%xmm1, -8(%rsp)         # 8-byte Spill
	movsd	%xmm1, 112(%rdi)
	movsd	88(%rdi), %xmm5         # xmm5 = mem[0],zero
	movsd	96(%rdi), %xmm1         # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm10
	addsd	%xmm5, %xmm1
	movsd	%xmm1, -40(%rsp)        # 8-byte Spill
	movsd	%xmm1, 96(%rdi)
	movsd	72(%rdi), %xmm6         # xmm6 = mem[0],zero
	movsd	80(%rdi), %xmm8         # xmm8 = mem[0],zero
	addsd	%xmm8, %xmm5
	addsd	%xmm6, %xmm8
	movsd	%xmm8, 80(%rdi)
	movsd	56(%rdi), %xmm11        # xmm11 = mem[0],zero
	movsd	64(%rdi), %xmm7         # xmm7 = mem[0],zero
	addsd	%xmm7, %xmm6
	addsd	%xmm11, %xmm7
	movsd	%xmm7, 64(%rdi)
	movsd	40(%rdi), %xmm3         # xmm3 = mem[0],zero
	movsd	48(%rdi), %xmm1         # xmm1 = mem[0],zero
	addsd	%xmm1, %xmm11
	addsd	%xmm3, %xmm1
	movsd	%xmm1, -48(%rsp)        # 8-byte Spill
	movsd	%xmm1, 48(%rdi)
	movsd	24(%rdi), %xmm4         # xmm4 = mem[0],zero
	movsd	32(%rdi), %xmm9         # xmm9 = mem[0],zero
	addsd	%xmm9, %xmm3
	addsd	%xmm4, %xmm9
	movsd	%xmm9, 32(%rdi)
	addsd	%xmm15, %xmm14
	unpcklpd	%xmm0, %xmm15   # xmm15 = xmm15[0],xmm0[0]
	movapd	%xmm10, %xmm12
	unpcklpd	%xmm13, %xmm12  # xmm12 = xmm12[0],xmm13[0]
	movsd	8(%rdi), %xmm13         # xmm13 = mem[0],zero
	addpd	%xmm15, %xmm12
	movsd	16(%rdi), %xmm0         # xmm0 = mem[0],zero
	addsd	%xmm0, %xmm4
	addsd	%xmm13, %xmm0
	movsd	%xmm0, 16(%rdi)
	movsd	(%rdi), %xmm1           # xmm1 = mem[0],zero
	movsd	%xmm1, -16(%rsp)        # 8-byte Spill
	addsd	%xmm1, %xmm13
	movsd	%xmm13, 8(%rdi)
	movsd	%xmm14, 136(%rdi)
	movupd	%xmm12, 120(%rdi)
	addsd	%xmm5, %xmm10
	movsd	%xmm10, 104(%rdi)
	addsd	%xmm6, %xmm5
	movsd	%xmm5, 88(%rdi)
	movapd	%xmm11, %xmm15
	addsd	%xmm15, %xmm6
	movsd	%xmm6, 72(%rdi)
	addsd	%xmm3, %xmm15
	movsd	%xmm15, 56(%rdi)
	addsd	%xmm4, %xmm3
	movsd	%xmm3, 40(%rdi)
	addsd	%xmm13, %xmm4
	movsd	%xmm4, 24(%rdi)
	movsd	COS9+24(%rip), %xmm2    # xmm2 = mem[0],zero
	movsd	-48(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, -48(%rsp)        # 8-byte Spill
	mulsd	%xmm2, %xmm15
	movsd	COS9+48(%rip), %xmm2    # xmm2 = mem[0],zero
	movsd	-40(%rsp), %xmm11       # 8-byte Reload
                                        # xmm11 = mem[0],zero
	mulsd	%xmm2, %xmm11
	mulsd	%xmm2, %xmm10
	movapd	%xmm10, -40(%rsp)       # 16-byte Spill
	movsd	COS9+8(%rip), %xmm10    # xmm10 = mem[0],zero
	mulsd	%xmm10, %xmm0
	addsd	%xmm1, %xmm0
	movsd	COS9+40(%rip), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm8
	addsd	%xmm0, %xmm8
	movsd	COS9+56(%rip), %xmm0    # xmm0 = mem[0],zero
	movsd	-8(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	%xmm8, %xmm1
	movapd	%xmm1, %xmm8
	mulsd	%xmm10, %xmm4
	mulsd	%xmm2, %xmm5
	addsd	%xmm15, %xmm4
	addsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm12
	addsd	%xmm5, %xmm12
	movsd	COS9+16(%rip), %xmm1    # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm9
	addsd	-16(%rsp), %xmm9        # 8-byte Folded Reload
	movsd	COS9+32(%rip), %xmm2    # xmm2 = mem[0],zero
	mulsd	%xmm2, %xmm7
	addsd	%xmm9, %xmm7
	addsd	%xmm11, %xmm7
	movsd	COS9+64(%rip), %xmm4    # xmm4 = mem[0],zero
	movsd	128(%rdi), %xmm0        # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	addsd	%xmm7, %xmm0
	mulsd	%xmm1, %xmm3
	addsd	%xmm13, %xmm3
	mulsd	%xmm2, %xmm6
	addsd	%xmm3, %xmm6
	movapd	-40(%rsp), %xmm9        # 16-byte Reload
	addsd	%xmm9, %xmm6
	mulsd	%xmm4, %xmm14
	addsd	%xmm6, %xmm14
	movapd	%xmm8, %xmm1
	addsd	%xmm0, %xmm1
	movapd	%xmm12, %xmm2
	addsd	%xmm14, %xmm2
	mulsd	tfcos36(%rip), %xmm2
	movapd	%xmm1, %xmm3
	addsd	%xmm2, %xmm3
	movsd	216(%rcx), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	movsd	%xmm4, 72(%rdx)
	mulsd	208(%rcx), %xmm3
	movsd	%xmm3, 64(%rdx)
	subsd	%xmm2, %xmm1
	movsd	64(%rcx), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	addsd	64(%rsi), %xmm2
	movsd	%xmm2, 2048(%r8)
	mulsd	72(%rcx), %xmm1
	addsd	72(%rsi), %xmm1
	movsd	%xmm1, 2304(%r8)
	subsd	%xmm8, %xmm0
	subsd	%xmm12, %xmm14
	mulsd	tfcos36+64(%rip), %xmm14
	movapd	%xmm0, %xmm1
	addsd	%xmm14, %xmm1
	movsd	280(%rcx), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 136(%rdx)
	mulsd	144(%rcx), %xmm1
	movsd	%xmm1, (%rdx)
	subsd	%xmm14, %xmm0
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rsi), %xmm1
	movsd	%xmm1, (%r8)
	mulsd	136(%rcx), %xmm0
	addsd	136(%rsi), %xmm0
	movsd	%xmm0, 4352(%r8)
	movsd	16(%rdi), %xmm2         # xmm2 = mem[0],zero
	movsd	24(%rdi), %xmm1         # xmm1 = mem[0],zero
	subsd	80(%rdi), %xmm2
	subsd	112(%rdi), %xmm2
	movsd	COS9+24(%rip), %xmm0    # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm2
	subsd	88(%rdi), %xmm1
	subsd	120(%rdi), %xmm1
	mulsd	%xmm0, %xmm1
	movsd	32(%rdi), %xmm0         # xmm0 = mem[0],zero
	subsd	64(%rdi), %xmm0
	subsd	128(%rdi), %xmm0
	movsd	COS9+48(%rip), %xmm4    # xmm4 = mem[0],zero
	mulsd	%xmm4, %xmm0
	subsd	96(%rdi), %xmm0
	addsd	(%rdi), %xmm0
	movsd	40(%rdi), %xmm3         # xmm3 = mem[0],zero
	subsd	72(%rdi), %xmm3
	subsd	136(%rdi), %xmm3
	mulsd	%xmm4, %xmm3
	subsd	104(%rdi), %xmm3
	addsd	8(%rdi), %xmm3
	movapd	%xmm2, %xmm4
	addsd	%xmm0, %xmm4
	movapd	%xmm1, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	tfcos36+8(%rip), %xmm5
	movapd	%xmm4, %xmm6
	addsd	%xmm5, %xmm6
	movsd	224(%rcx), %xmm7        # xmm7 = mem[0],zero
	mulsd	%xmm6, %xmm7
	movsd	%xmm7, 80(%rdx)
	mulsd	200(%rcx), %xmm6
	movsd	%xmm6, 56(%rdx)
	subsd	%xmm5, %xmm4
	movsd	56(%rcx), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	addsd	56(%rsi), %xmm5
	movsd	%xmm5, 1792(%r8)
	mulsd	80(%rcx), %xmm4
	addsd	80(%rsi), %xmm4
	movsd	%xmm4, 2560(%r8)
	subsd	%xmm2, %xmm0
	subsd	%xmm1, %xmm3
	mulsd	tfcos36+56(%rip), %xmm3
	movapd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	movsd	272(%rcx), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 128(%rdx)
	mulsd	152(%rcx), %xmm1
	movsd	%xmm1, 8(%rdx)
	subsd	%xmm3, %xmm0
	movsd	8(%rcx), %xmm1          # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	8(%rsi), %xmm1
	movsd	%xmm1, 256(%r8)
	mulsd	128(%rcx), %xmm0
	addsd	128(%rsi), %xmm0
	movsd	%xmm0, 4096(%r8)
	movsd	COS9+40(%rip), %xmm2    # xmm2 = mem[0],zero
	movsd	16(%rdi), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	movsd	-48(%rsp), %xmm10       # 8-byte Reload
                                        # xmm10 = mem[0],zero
	subsd	%xmm10, %xmm3
	movsd	COS9+56(%rip), %xmm4    # xmm4 = mem[0],zero
	movsd	80(%rdi), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm4, %xmm0
	subsd	%xmm0, %xmm3
	movsd	COS9+8(%rip), %xmm0     # xmm0 = mem[0],zero
	movsd	112(%rdi), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	mulsd	24(%rdi), %xmm2
	subsd	%xmm15, %xmm2
	mulsd	88(%rdi), %xmm4
	subsd	%xmm4, %xmm2
	mulsd	120(%rdi), %xmm0
	addsd	%xmm2, %xmm0
	movsd	COS9+64(%rip), %xmm4    # xmm4 = mem[0],zero
	movsd	32(%rdi), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm4, %xmm2
	movsd	(%rdi), %xmm5           # xmm5 = mem[0],zero
	movsd	8(%rdi), %xmm6          # xmm6 = mem[0],zero
	subsd	%xmm2, %xmm5
	movsd	COS9+16(%rip), %xmm7    # xmm7 = mem[0],zero
	movsd	64(%rdi), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm7, %xmm2
	subsd	%xmm2, %xmm5
	addsd	%xmm11, %xmm5
	movsd	COS9+32(%rip), %xmm3    # xmm3 = mem[0],zero
	movsd	128(%rdi), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	addsd	%xmm5, %xmm2
	mulsd	40(%rdi), %xmm4
	subsd	%xmm4, %xmm6
	mulsd	72(%rdi), %xmm7
	subsd	%xmm7, %xmm6
	addsd	%xmm9, %xmm6
	mulsd	136(%rdi), %xmm3
	addsd	%xmm6, %xmm3
	movapd	%xmm1, %xmm4
	addsd	%xmm2, %xmm4
	movapd	%xmm0, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	tfcos36+16(%rip), %xmm5
	movapd	%xmm4, %xmm6
	addsd	%xmm5, %xmm6
	movsd	232(%rcx), %xmm7        # xmm7 = mem[0],zero
	mulsd	%xmm6, %xmm7
	movsd	%xmm7, 88(%rdx)
	mulsd	192(%rcx), %xmm6
	movsd	%xmm6, 48(%rdx)
	subsd	%xmm5, %xmm4
	movsd	48(%rcx), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	addsd	48(%rsi), %xmm5
	movsd	%xmm5, 1536(%r8)
	mulsd	88(%rcx), %xmm4
	addsd	88(%rsi), %xmm4
	movsd	%xmm4, 2816(%r8)
	subsd	%xmm1, %xmm2
	subsd	%xmm0, %xmm3
	mulsd	tfcos36+48(%rip), %xmm3
	movapd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	movsd	264(%rcx), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 120(%rdx)
	mulsd	160(%rcx), %xmm0
	movsd	%xmm0, 16(%rdx)
	subsd	%xmm3, %xmm2
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm0
	addsd	16(%rsi), %xmm0
	movsd	%xmm0, 512(%r8)
	mulsd	120(%rcx), %xmm2
	addsd	120(%rsi), %xmm2
	movsd	%xmm2, 3840(%r8)
	movsd	COS9+56(%rip), %xmm2    # xmm2 = mem[0],zero
	movsd	16(%rdi), %xmm3         # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	subsd	%xmm10, %xmm3
	movsd	COS9+8(%rip), %xmm0     # xmm0 = mem[0],zero
	movsd	80(%rdi), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	movsd	COS9+40(%rip), %xmm3    # xmm3 = mem[0],zero
	movsd	112(%rdi), %xmm4        # xmm4 = mem[0],zero
	mulsd	%xmm3, %xmm4
	subsd	%xmm4, %xmm1
	mulsd	24(%rdi), %xmm2
	subsd	%xmm15, %xmm2
	mulsd	88(%rdi), %xmm0
	addsd	%xmm2, %xmm0
	mulsd	120(%rdi), %xmm3
	subsd	%xmm3, %xmm0
	movsd	COS9+32(%rip), %xmm4    # xmm4 = mem[0],zero
	movsd	32(%rdi), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm4, %xmm2
	movsd	(%rdi), %xmm5           # xmm5 = mem[0],zero
	subsd	%xmm2, %xmm5
	movsd	COS9+64(%rip), %xmm3    # xmm3 = mem[0],zero
	movsd	64(%rdi), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm3, %xmm2
	addsd	%xmm5, %xmm2
	addsd	%xmm11, %xmm2
	movsd	COS9+16(%rip), %xmm5    # xmm5 = mem[0],zero
	movsd	128(%rdi), %xmm6        # xmm6 = mem[0],zero
	mulsd	%xmm5, %xmm6
	subsd	%xmm6, %xmm2
	movsd	8(%rdi), %xmm6          # xmm6 = mem[0],zero
	mulsd	40(%rdi), %xmm4
	subsd	%xmm4, %xmm6
	mulsd	72(%rdi), %xmm3
	addsd	%xmm6, %xmm3
	addsd	%xmm9, %xmm3
	mulsd	136(%rdi), %xmm5
	subsd	%xmm5, %xmm3
	movapd	%xmm1, %xmm4
	addsd	%xmm2, %xmm4
	movapd	%xmm0, %xmm5
	addsd	%xmm3, %xmm5
	mulsd	tfcos36+24(%rip), %xmm5
	movapd	%xmm4, %xmm6
	addsd	%xmm5, %xmm6
	movsd	240(%rcx), %xmm7        # xmm7 = mem[0],zero
	mulsd	%xmm6, %xmm7
	movsd	%xmm7, 96(%rdx)
	mulsd	184(%rcx), %xmm6
	movsd	%xmm6, 40(%rdx)
	subsd	%xmm5, %xmm4
	movsd	40(%rcx), %xmm5         # xmm5 = mem[0],zero
	mulsd	%xmm4, %xmm5
	addsd	40(%rsi), %xmm5
	movsd	%xmm5, 1280(%r8)
	mulsd	96(%rcx), %xmm4
	addsd	96(%rsi), %xmm4
	movsd	%xmm4, 3072(%r8)
	subsd	%xmm1, %xmm2
	subsd	%xmm0, %xmm3
	mulsd	tfcos36+40(%rip), %xmm3
	movapd	%xmm2, %xmm0
	addsd	%xmm3, %xmm0
	movsd	256(%rcx), %xmm1        # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	movsd	%xmm1, 112(%rdx)
	mulsd	168(%rcx), %xmm0
	movsd	%xmm0, 24(%rdx)
	subsd	%xmm3, %xmm2
	movsd	24(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm0
	addsd	24(%rsi), %xmm0
	movsd	%xmm0, 768(%r8)
	mulsd	112(%rcx), %xmm2
	addsd	112(%rsi), %xmm2
	movsd	%xmm2, 3584(%r8)
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rdi), %xmm1          # xmm1 = mem[0],zero
	subsd	32(%rdi), %xmm0
	addsd	64(%rdi), %xmm0
	subsd	96(%rdi), %xmm0
	addsd	128(%rdi), %xmm0
	subsd	40(%rdi), %xmm1
	addsd	72(%rdi), %xmm1
	subsd	104(%rdi), %xmm1
	addsd	136(%rdi), %xmm1
	mulsd	tfcos36+32(%rip), %xmm1
	movapd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	248(%rcx), %xmm3        # xmm3 = mem[0],zero
	mulsd	%xmm2, %xmm3
	movsd	%xmm3, 104(%rdx)
	mulsd	176(%rcx), %xmm2
	movsd	%xmm2, 32(%rdx)
	subsd	%xmm1, %xmm0
	movsd	32(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	32(%rsi), %xmm1
	movsd	%xmm1, 1024(%r8)
	mulsd	104(%rcx), %xmm0
	addsd	104(%rsi), %xmm0
	movsd	%xmm0, 3328(%r8)
	retq
.Lfunc_end4:
	.size	dct36, .Lfunc_end4-dct36
	.cfi_endproc

	.p2align	4, 0x90
	.type	dct12,@function
dct12:                                  # @dct12
	.cfi_startproc
# BB#0:
	movq	(%rsi), %rax
	movq	%rax, (%r8)
	movq	8(%rsi), %rax
	movq	%rax, 256(%r8)
	movq	16(%rsi), %rax
	movq	%rax, 512(%r8)
	movq	24(%rsi), %rax
	movq	%rax, 768(%r8)
	movq	32(%rsi), %rax
	movq	%rax, 1024(%r8)
	movq	40(%rsi), %rax
	movq	%rax, 1280(%r8)
	movsd	96(%rdi), %xmm0         # xmm0 = mem[0],zero
	movsd	120(%rdi), %xmm1        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	72(%rdi), %xmm2         # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	movsd	48(%rdi), %xmm5         # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm2
	movsd	(%rdi), %xmm8           # xmm8 = mem[0],zero
	movsd	24(%rdi), %xmm4         # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm5
	addsd	%xmm8, %xmm4
	addsd	%xmm2, %xmm1
	addsd	%xmm4, %xmm2
	movsd	COS6_1(%rip), %xmm6     # xmm6 = mem[0],zero
	mulsd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm2
	movapd	%xmm8, %xmm3
	subsd	%xmm0, %xmm3
	movapd	%xmm4, %xmm7
	subsd	%xmm1, %xmm7
	mulsd	tfcos12+8(%rip), %xmm7
	movapd	%xmm3, %xmm6
	addsd	%xmm7, %xmm6
	subsd	%xmm7, %xmm3
	movsd	80(%rcx), %xmm7         # xmm7 = mem[0],zero
	mulsd	%xmm6, %xmm7
	addsd	128(%rsi), %xmm7
	movsd	%xmm7, 4096(%r8)
	mulsd	56(%rcx), %xmm6
	addsd	104(%rsi), %xmm6
	movsd	%xmm6, 3328(%r8)
	movsd	8(%rcx), %xmm6          # xmm6 = mem[0],zero
	mulsd	%xmm3, %xmm6
	addsd	56(%rsi), %xmm6
	movsd	%xmm6, 1792(%r8)
	mulsd	32(%rcx), %xmm3
	addsd	80(%rsi), %xmm3
	movsd	%xmm3, 2560(%r8)
	movsd	COS6_2(%rip), %xmm6     # xmm6 = mem[0],zero
	mulsd	%xmm6, %xmm0
	addsd	%xmm8, %xmm0
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm3
	subsd	%xmm5, %xmm0
	mulsd	%xmm6, %xmm1
	addsd	%xmm4, %xmm1
	movapd	%xmm2, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	tfcos12(%rip), %xmm4
	subsd	%xmm2, %xmm1
	mulsd	tfcos12+16(%rip), %xmm1
	movapd	%xmm3, %xmm2
	addsd	%xmm4, %xmm2
	subsd	%xmm4, %xmm3
	movapd	%xmm0, %xmm4
	addsd	%xmm1, %xmm4
	subsd	%xmm1, %xmm0
	movsd	88(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm4, %xmm1
	addsd	136(%rsi), %xmm1
	movsd	%xmm1, 4352(%r8)
	mulsd	48(%rcx), %xmm4
	addsd	96(%rsi), %xmm4
	movsd	%xmm4, 3072(%r8)
	movsd	64(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	addsd	112(%rsi), %xmm1
	movsd	%xmm1, 3584(%r8)
	mulsd	72(%rcx), %xmm2
	addsd	120(%rsi), %xmm2
	movsd	%xmm2, 3840(%r8)
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	48(%rsi), %xmm1
	movsd	%xmm1, 1536(%r8)
	mulsd	40(%rcx), %xmm0
	addsd	88(%rsi), %xmm0
	movsd	%xmm0, 2816(%r8)
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	64(%rsi), %xmm0
	movsd	%xmm0, 2048(%r8)
	mulsd	24(%rcx), %xmm3
	addsd	72(%rsi), %xmm3
	movsd	%xmm3, 2304(%r8)
	movsd	104(%rdi), %xmm0        # xmm0 = mem[0],zero
	movsd	128(%rdi), %xmm1        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	80(%rdi), %xmm2         # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	movsd	56(%rdi), %xmm5         # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm2
	movsd	8(%rdi), %xmm8          # xmm8 = mem[0],zero
	movsd	32(%rdi), %xmm4         # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm5
	addsd	%xmm8, %xmm4
	addsd	%xmm2, %xmm1
	addsd	%xmm4, %xmm2
	movsd	COS6_1(%rip), %xmm6     # xmm6 = mem[0],zero
	mulsd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm2
	movapd	%xmm8, %xmm6
	subsd	%xmm0, %xmm6
	movapd	%xmm4, %xmm7
	subsd	%xmm1, %xmm7
	mulsd	tfcos12+8(%rip), %xmm7
	movapd	%xmm6, %xmm3
	addsd	%xmm7, %xmm3
	subsd	%xmm7, %xmm6
	movsd	80(%rcx), %xmm7         # xmm7 = mem[0],zero
	mulsd	%xmm3, %xmm7
	movsd	%xmm7, 32(%rdx)
	mulsd	56(%rcx), %xmm3
	movsd	%xmm3, 8(%rdx)
	movsd	8(%rcx), %xmm3          # xmm3 = mem[0],zero
	mulsd	%xmm6, %xmm3
	addsd	3328(%r8), %xmm3
	movsd	%xmm3, 3328(%r8)
	mulsd	32(%rcx), %xmm6
	addsd	4096(%r8), %xmm6
	movsd	%xmm6, 4096(%r8)
	movsd	COS6_2(%rip), %xmm6     # xmm6 = mem[0],zero
	mulsd	%xmm6, %xmm0
	addsd	%xmm8, %xmm0
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm3
	subsd	%xmm5, %xmm0
	mulsd	%xmm6, %xmm1
	addsd	%xmm4, %xmm1
	movapd	%xmm2, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	tfcos12(%rip), %xmm4
	subsd	%xmm2, %xmm1
	mulsd	tfcos12+16(%rip), %xmm1
	movapd	%xmm3, %xmm2
	addsd	%xmm4, %xmm2
	subsd	%xmm4, %xmm3
	movapd	%xmm0, %xmm4
	addsd	%xmm1, %xmm4
	subsd	%xmm1, %xmm0
	movsd	88(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm4, %xmm1
	movsd	%xmm1, 40(%rdx)
	mulsd	48(%rcx), %xmm4
	movsd	%xmm4, (%rdx)
	movsd	64(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, 16(%rdx)
	mulsd	72(%rcx), %xmm2
	movsd	%xmm2, 24(%rdx)
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	3072(%r8), %xmm1
	movsd	%xmm1, 3072(%r8)
	mulsd	40(%rcx), %xmm0
	addsd	4352(%r8), %xmm0
	movsd	%xmm0, 4352(%r8)
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	3584(%r8), %xmm0
	movsd	%xmm0, 3584(%r8)
	mulsd	24(%rcx), %xmm3
	addsd	3840(%r8), %xmm3
	movsd	%xmm3, 3840(%r8)
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 128(%rdx)
	movupd	%xmm0, 112(%rdx)
	movupd	%xmm0, 96(%rdx)
	movsd	112(%rdi), %xmm0        # xmm0 = mem[0],zero
	movsd	136(%rdi), %xmm1        # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	88(%rdi), %xmm2         # xmm2 = mem[0],zero
	addsd	%xmm2, %xmm0
	movsd	64(%rdi), %xmm5         # xmm5 = mem[0],zero
	addsd	%xmm5, %xmm2
	movsd	16(%rdi), %xmm8         # xmm8 = mem[0],zero
	movsd	40(%rdi), %xmm4         # xmm4 = mem[0],zero
	addsd	%xmm4, %xmm5
	addsd	%xmm8, %xmm4
	addsd	%xmm2, %xmm1
	addsd	%xmm4, %xmm2
	movsd	COS6_1(%rip), %xmm6     # xmm6 = mem[0],zero
	mulsd	%xmm6, %xmm5
	mulsd	%xmm6, %xmm2
	movapd	%xmm8, %xmm6
	subsd	%xmm0, %xmm6
	movapd	%xmm4, %xmm7
	subsd	%xmm1, %xmm7
	mulsd	tfcos12+8(%rip), %xmm7
	movapd	%xmm6, %xmm3
	addsd	%xmm7, %xmm3
	subsd	%xmm7, %xmm6
	movsd	80(%rcx), %xmm7         # xmm7 = mem[0],zero
	mulsd	%xmm3, %xmm7
	movsd	%xmm7, 80(%rdx)
	mulsd	56(%rcx), %xmm3
	movsd	%xmm3, 56(%rdx)
	movsd	8(%rcx), %xmm3          # xmm3 = mem[0],zero
	mulsd	%xmm6, %xmm3
	addsd	8(%rdx), %xmm3
	movsd	%xmm3, 8(%rdx)
	mulsd	32(%rcx), %xmm6
	addsd	32(%rdx), %xmm6
	movsd	%xmm6, 32(%rdx)
	movsd	COS6_2(%rip), %xmm6     # xmm6 = mem[0],zero
	mulsd	%xmm6, %xmm0
	addsd	%xmm8, %xmm0
	movapd	%xmm5, %xmm3
	addsd	%xmm0, %xmm3
	subsd	%xmm5, %xmm0
	mulsd	%xmm6, %xmm1
	addsd	%xmm4, %xmm1
	movapd	%xmm2, %xmm4
	addsd	%xmm1, %xmm4
	mulsd	tfcos12(%rip), %xmm4
	subsd	%xmm2, %xmm1
	mulsd	tfcos12+16(%rip), %xmm1
	movapd	%xmm3, %xmm2
	addsd	%xmm4, %xmm2
	subsd	%xmm4, %xmm3
	movapd	%xmm0, %xmm4
	addsd	%xmm1, %xmm4
	subsd	%xmm1, %xmm0
	movsd	88(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm4, %xmm1
	movsd	%xmm1, 88(%rdx)
	mulsd	48(%rcx), %xmm4
	movsd	%xmm4, 48(%rdx)
	movsd	64(%rcx), %xmm1         # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	movsd	%xmm1, 64(%rdx)
	mulsd	72(%rcx), %xmm2
	movsd	%xmm2, 72(%rdx)
	movsd	(%rcx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	(%rdx), %xmm1
	movsd	%xmm1, (%rdx)
	mulsd	40(%rcx), %xmm0
	addsd	40(%rdx), %xmm0
	movsd	%xmm0, 40(%rdx)
	movsd	16(%rcx), %xmm0         # xmm0 = mem[0],zero
	mulsd	%xmm3, %xmm0
	addsd	16(%rdx), %xmm0
	movsd	%xmm0, 16(%rdx)
	mulsd	24(%rcx), %xmm3
	addsd	24(%rdx), %xmm3
	movsd	%xmm3, 24(%rdx)
	retq
.Lfunc_end5:
	.size	dct12, .Lfunc_end5-dct12
	.cfi_endproc

	.type	bandInfo,@object        # @bandInfo
	.data
	.globl	bandInfo
	.p2align	4
bandInfo:
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	8                       # 0x8
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	44                      # 0x2c
	.short	52                      # 0x34
	.short	62                      # 0x3e
	.short	74                      # 0x4a
	.short	90                      # 0x5a
	.short	110                     # 0x6e
	.short	134                     # 0x86
	.short	162                     # 0xa2
	.short	196                     # 0xc4
	.short	238                     # 0xee
	.short	288                     # 0x120
	.short	342                     # 0x156
	.short	418                     # 0x1a2
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	28                      # 0x1c
	.short	34                      # 0x22
	.short	42                      # 0x2a
	.short	50                      # 0x32
	.short	54                      # 0x36
	.short	76                      # 0x4c
	.short	158                     # 0x9e
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	48                      # 0x30
	.short	66                      # 0x42
	.short	90                      # 0x5a
	.short	120                     # 0x78
	.short	156                     # 0x9c
	.short	198                     # 0xc6
	.short	252                     # 0xfc
	.short	318                     # 0x13e
	.short	408                     # 0x198
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	18                      # 0x12
	.short	22                      # 0x16
	.short	30                      # 0x1e
	.short	56                      # 0x38
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	8                       # 0x8
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	42                      # 0x2a
	.short	50                      # 0x32
	.short	60                      # 0x3c
	.short	72                      # 0x48
	.short	88                      # 0x58
	.short	106                     # 0x6a
	.short	128                     # 0x80
	.short	156                     # 0x9c
	.short	190                     # 0xbe
	.short	230                     # 0xe6
	.short	276                     # 0x114
	.short	330                     # 0x14a
	.short	384                     # 0x180
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	22                      # 0x16
	.short	28                      # 0x1c
	.short	34                      # 0x22
	.short	40                      # 0x28
	.short	46                      # 0x2e
	.short	54                      # 0x36
	.short	54                      # 0x36
	.short	192                     # 0xc0
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	48                      # 0x30
	.short	66                      # 0x42
	.short	84                      # 0x54
	.short	114                     # 0x72
	.short	150                     # 0x96
	.short	192                     # 0xc0
	.short	240                     # 0xf0
	.short	300                     # 0x12c
	.short	378                     # 0x17a
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	26                      # 0x1a
	.short	66                      # 0x42
	.short	0                       # 0x0
	.short	4                       # 0x4
	.short	8                       # 0x8
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	44                      # 0x2c
	.short	54                      # 0x36
	.short	66                      # 0x42
	.short	82                      # 0x52
	.short	102                     # 0x66
	.short	126                     # 0x7e
	.short	156                     # 0x9c
	.short	194                     # 0xc2
	.short	240                     # 0xf0
	.short	296                     # 0x128
	.short	364                     # 0x16c
	.short	448                     # 0x1c0
	.short	550                     # 0x226
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	38                      # 0x26
	.short	46                      # 0x2e
	.short	56                      # 0x38
	.short	68                      # 0x44
	.short	84                      # 0x54
	.short	102                     # 0x66
	.short	26                      # 0x1a
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	48                      # 0x30
	.short	66                      # 0x42
	.short	90                      # 0x5a
	.short	126                     # 0x7e
	.short	174                     # 0xae
	.short	234                     # 0xea
	.short	312                     # 0x138
	.short	414                     # 0x19e
	.short	540                     # 0x21c
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	26                      # 0x1a
	.short	34                      # 0x22
	.short	42                      # 0x2a
	.short	12                      # 0xc
	.short	0                       # 0x0
	.short	6                       # 0x6
	.short	12                      # 0xc
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	44                      # 0x2c
	.short	54                      # 0x36
	.short	66                      # 0x42
	.short	80                      # 0x50
	.short	96                      # 0x60
	.short	116                     # 0x74
	.short	140                     # 0x8c
	.short	168                     # 0xa8
	.short	200                     # 0xc8
	.short	238                     # 0xee
	.short	284                     # 0x11c
	.short	336                     # 0x150
	.short	396                     # 0x18c
	.short	464                     # 0x1d0
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	38                      # 0x26
	.short	46                      # 0x2e
	.short	52                      # 0x34
	.short	60                      # 0x3c
	.short	68                      # 0x44
	.short	58                      # 0x3a
	.short	54                      # 0x36
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	54                      # 0x36
	.short	72                      # 0x48
	.short	96                      # 0x60
	.short	126                     # 0x7e
	.short	168                     # 0xa8
	.short	222                     # 0xde
	.short	300                     # 0x12c
	.short	396                     # 0x18c
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	14                      # 0xe
	.short	18                      # 0x12
	.short	26                      # 0x1a
	.short	32                      # 0x20
	.short	42                      # 0x2a
	.short	18                      # 0x12
	.short	0                       # 0x0
	.short	6                       # 0x6
	.short	12                      # 0xc
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	44                      # 0x2c
	.short	54                      # 0x36
	.short	66                      # 0x42
	.short	80                      # 0x50
	.short	96                      # 0x60
	.short	114                     # 0x72
	.short	136                     # 0x88
	.short	162                     # 0xa2
	.short	194                     # 0xc2
	.short	232                     # 0xe8
	.short	278                     # 0x116
	.short	332                     # 0x14c
	.short	394                     # 0x18a
	.short	464                     # 0x1d0
	.short	540                     # 0x21c
	.short	576                     # 0x240
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	16                      # 0x10
	.short	18                      # 0x12
	.short	22                      # 0x16
	.short	26                      # 0x1a
	.short	32                      # 0x20
	.short	38                      # 0x26
	.short	46                      # 0x2e
	.short	52                      # 0x34
	.short	64                      # 0x40
	.short	70                      # 0x46
	.short	76                      # 0x4c
	.short	36                      # 0x24
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	54                      # 0x36
	.short	78                      # 0x4e
	.short	108                     # 0x6c
	.short	144                     # 0x90
	.short	186                     # 0xba
	.short	240                     # 0xf0
	.short	312                     # 0x138
	.short	408                     # 0x198
	.short	540                     # 0x21c
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	32                      # 0x20
	.short	44                      # 0x2c
	.short	12                      # 0xc
	.short	0                       # 0x0
	.short	6                       # 0x6
	.short	12                      # 0xc
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	44                      # 0x2c
	.short	54                      # 0x36
	.short	66                      # 0x42
	.short	80                      # 0x50
	.short	96                      # 0x60
	.short	116                     # 0x74
	.short	140                     # 0x8c
	.short	168                     # 0xa8
	.short	200                     # 0xc8
	.short	238                     # 0xee
	.short	284                     # 0x11c
	.short	336                     # 0x150
	.short	396                     # 0x18c
	.short	464                     # 0x1d0
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	38                      # 0x26
	.short	46                      # 0x2e
	.short	52                      # 0x34
	.short	60                      # 0x3c
	.short	68                      # 0x44
	.short	58                      # 0x3a
	.short	54                      # 0x36
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	54                      # 0x36
	.short	78                      # 0x4e
	.short	108                     # 0x6c
	.short	144                     # 0x90
	.short	186                     # 0xba
	.short	240                     # 0xf0
	.short	312                     # 0x138
	.short	402                     # 0x192
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	40                      # 0x28
	.short	18                      # 0x12
	.short	0                       # 0x0
	.short	6                       # 0x6
	.short	12                      # 0xc
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	44                      # 0x2c
	.short	54                      # 0x36
	.short	66                      # 0x42
	.short	80                      # 0x50
	.short	96                      # 0x60
	.short	116                     # 0x74
	.short	140                     # 0x8c
	.short	168                     # 0xa8
	.short	200                     # 0xc8
	.short	238                     # 0xee
	.short	284                     # 0x11c
	.short	336                     # 0x150
	.short	396                     # 0x18c
	.short	464                     # 0x1d0
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	38                      # 0x26
	.short	46                      # 0x2e
	.short	52                      # 0x34
	.short	60                      # 0x3c
	.short	68                      # 0x44
	.short	58                      # 0x3a
	.short	54                      # 0x36
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	54                      # 0x36
	.short	78                      # 0x4e
	.short	108                     # 0x6c
	.short	144                     # 0x90
	.short	186                     # 0xba
	.short	240                     # 0xf0
	.short	312                     # 0x138
	.short	402                     # 0x192
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	40                      # 0x28
	.short	18                      # 0x12
	.short	0                       # 0x0
	.short	6                       # 0x6
	.short	12                      # 0xc
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	36                      # 0x24
	.short	44                      # 0x2c
	.short	54                      # 0x36
	.short	66                      # 0x42
	.short	80                      # 0x50
	.short	96                      # 0x60
	.short	116                     # 0x74
	.short	140                     # 0x8c
	.short	168                     # 0xa8
	.short	200                     # 0xc8
	.short	238                     # 0xee
	.short	284                     # 0x11c
	.short	336                     # 0x150
	.short	396                     # 0x18c
	.short	464                     # 0x1d0
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	38                      # 0x26
	.short	46                      # 0x2e
	.short	52                      # 0x34
	.short	60                      # 0x3c
	.short	68                      # 0x44
	.short	58                      # 0x3a
	.short	54                      # 0x36
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	54                      # 0x36
	.short	78                      # 0x4e
	.short	108                     # 0x6c
	.short	144                     # 0x90
	.short	186                     # 0xba
	.short	240                     # 0xf0
	.short	312                     # 0x138
	.short	402                     # 0x192
	.short	522                     # 0x20a
	.short	576                     # 0x240
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	4                       # 0x4
	.short	6                       # 0x6
	.short	8                       # 0x8
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	14                      # 0xe
	.short	18                      # 0x12
	.short	24                      # 0x18
	.short	30                      # 0x1e
	.short	40                      # 0x28
	.short	18                      # 0x12
	.short	0                       # 0x0
	.short	12                      # 0xc
	.short	24                      # 0x18
	.short	36                      # 0x24
	.short	48                      # 0x30
	.short	60                      # 0x3c
	.short	72                      # 0x48
	.short	88                      # 0x58
	.short	108                     # 0x6c
	.short	132                     # 0x84
	.short	160                     # 0xa0
	.short	192                     # 0xc0
	.short	232                     # 0xe8
	.short	280                     # 0x118
	.short	336                     # 0x150
	.short	400                     # 0x190
	.short	476                     # 0x1dc
	.short	566                     # 0x236
	.short	568                     # 0x238
	.short	570                     # 0x23a
	.short	572                     # 0x23c
	.short	574                     # 0x23e
	.short	576                     # 0x240
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	28                      # 0x1c
	.short	32                      # 0x20
	.short	40                      # 0x28
	.short	48                      # 0x30
	.short	56                      # 0x38
	.short	64                      # 0x40
	.short	76                      # 0x4c
	.short	90                      # 0x5a
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	0                       # 0x0
	.short	24                      # 0x18
	.short	48                      # 0x30
	.short	72                      # 0x48
	.short	108                     # 0x6c
	.short	156                     # 0x9c
	.short	216                     # 0xd8
	.short	288                     # 0x120
	.short	372                     # 0x174
	.short	480                     # 0x1e0
	.short	486                     # 0x1e6
	.short	492                     # 0x1ec
	.short	498                     # 0x1f2
	.short	576                     # 0x240
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	8                       # 0x8
	.short	12                      # 0xc
	.short	16                      # 0x10
	.short	20                      # 0x14
	.short	24                      # 0x18
	.short	28                      # 0x1c
	.short	36                      # 0x24
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	2                       # 0x2
	.short	26                      # 0x1a
	.size	bandInfo, 1296

	.type	gainpow2,@object        # @gainpow2
	.local	gainpow2
	.comm	gainpow2,3024,16
	.type	ispow,@object           # @ispow
	.local	ispow
	.comm	ispow,65656,16
	.type	aa_cs,@object           # @aa_cs
	.local	aa_cs
	.comm	aa_cs,64,16
	.type	aa_ca,@object           # @aa_ca
	.local	aa_ca
	.comm	aa_ca,64,16
	.type	win,@object             # @win
	.local	win
	.comm	win,1152,16
	.type	COS9,@object            # @COS9
	.local	COS9
	.comm	COS9,72,16
	.type	tfcos36,@object         # @tfcos36
	.local	tfcos36
	.comm	tfcos36,72,16
	.type	tfcos12,@object         # @tfcos12
	.local	tfcos12
	.comm	tfcos12,24,16
	.type	COS6_1,@object          # @COS6_1
	.local	COS6_1
	.comm	COS6_1,8,8
	.type	COS6_2,@object          # @COS6_2
	.local	COS6_2
	.comm	COS6_2,8,8
	.type	win1,@object            # @win1
	.local	win1
	.comm	win1,1152,16
	.type	tan1_1,@object          # @tan1_1
	.local	tan1_1
	.comm	tan1_1,128,16
	.type	tan2_1,@object          # @tan2_1
	.local	tan2_1
	.comm	tan2_1,128,16
	.type	tan1_2,@object          # @tan1_2
	.local	tan1_2
	.comm	tan1_2,128,16
	.type	tan2_2,@object          # @tan2_2
	.local	tan2_2
	.comm	tan2_2,128,16
	.type	pow1_1,@object          # @pow1_1
	.local	pow1_1
	.comm	pow1_1,256,16
	.type	pow2_1,@object          # @pow2_1
	.local	pow2_1
	.comm	pow2_1,256,16
	.type	pow1_2,@object          # @pow1_2
	.local	pow1_2
	.comm	pow1_2,256,16
	.type	pow2_2,@object          # @pow2_2
	.local	pow2_2
	.comm	pow2_2,256,16
	.type	mapbuf0,@object         # @mapbuf0
	.local	mapbuf0
	.comm	mapbuf0,5472,16
	.type	map,@object             # @map
	.local	map
	.comm	map,216,16
	.type	mapend,@object          # @mapend
	.local	mapend
	.comm	mapend,216,16
	.type	mapbuf1,@object         # @mapbuf1
	.local	mapbuf1
	.comm	mapbuf1,5616,16
	.type	mapbuf2,@object         # @mapbuf2
	.local	mapbuf2
	.comm	mapbuf2,1584,16
	.type	longLimit,@object       # @longLimit
	.comm	longLimit,828,16
	.type	shortLimit,@object      # @shortLimit
	.comm	shortLimit,504,16
	.type	i_slen2,@object         # @i_slen2
	.local	i_slen2
	.comm	i_slen2,1024,16
	.type	n_slen2,@object         # @n_slen2
	.local	n_slen2
	.comm	n_slen2,2048,16
	.type	do_layer3.hybridIn,@object # @do_layer3.hybridIn
	.local	do_layer3.hybridIn
	.comm	do_layer3.hybridIn,9216,16
	.type	do_layer3.hybridOut,@object # @do_layer3.hybridOut
	.local	do_layer3.hybridOut
	.comm	do_layer3.hybridOut,9216,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"big_values too large!\n"
	.size	.L.str, 23

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Blocktype == 0 and window-switching == 1 not allowed.\n"
	.size	.L.str.1, 55

	.type	III_get_scale_factors_2.stab,@object # @III_get_scale_factors_2.stab
	.section	.rodata,"a",@progbits
	.p2align	4
III_get_scale_factors_2.stab:
	.ascii	"\006\005\005\005"
	.ascii	"\006\005\007\003"
	.asciz	"\013\n\000"
	.asciz	"\007\007\007"
	.ascii	"\006\006\006\003"
	.asciz	"\b\b\005"
	.zero	4,9
	.ascii	"\t\t\f\006"
	.asciz	"\022\022\000"
	.asciz	"\f\f\f"
	.ascii	"\f\t\t\006"
	.asciz	"\017\f\t"
	.ascii	"\006\t\t\t"
	.ascii	"\006\t\f\006"
	.asciz	"\017\022\000"
	.asciz	"\006\017\f"
	.ascii	"\006\f\t\006"
	.asciz	"\006\022\t"
	.size	III_get_scale_factors_2.stab, 72

	.type	ht,@object              # @ht
	.p2align	4
ht:
	.long	0                       # 0x0
	.zero	4
	.quad	tab0
	.long	0                       # 0x0
	.zero	4
	.quad	tab1
	.long	0                       # 0x0
	.zero	4
	.quad	tab2
	.long	0                       # 0x0
	.zero	4
	.quad	tab3
	.long	0                       # 0x0
	.zero	4
	.quad	tab0
	.long	0                       # 0x0
	.zero	4
	.quad	tab5
	.long	0                       # 0x0
	.zero	4
	.quad	tab6
	.long	0                       # 0x0
	.zero	4
	.quad	tab7
	.long	0                       # 0x0
	.zero	4
	.quad	tab8
	.long	0                       # 0x0
	.zero	4
	.quad	tab9
	.long	0                       # 0x0
	.zero	4
	.quad	tab10
	.long	0                       # 0x0
	.zero	4
	.quad	tab11
	.long	0                       # 0x0
	.zero	4
	.quad	tab12
	.long	0                       # 0x0
	.zero	4
	.quad	tab13
	.long	0                       # 0x0
	.zero	4
	.quad	tab0
	.long	0                       # 0x0
	.zero	4
	.quad	tab15
	.long	1                       # 0x1
	.zero	4
	.quad	tab16
	.long	2                       # 0x2
	.zero	4
	.quad	tab16
	.long	3                       # 0x3
	.zero	4
	.quad	tab16
	.long	4                       # 0x4
	.zero	4
	.quad	tab16
	.long	6                       # 0x6
	.zero	4
	.quad	tab16
	.long	8                       # 0x8
	.zero	4
	.quad	tab16
	.long	10                      # 0xa
	.zero	4
	.quad	tab16
	.long	13                      # 0xd
	.zero	4
	.quad	tab16
	.long	4                       # 0x4
	.zero	4
	.quad	tab24
	.long	5                       # 0x5
	.zero	4
	.quad	tab24
	.long	6                       # 0x6
	.zero	4
	.quad	tab24
	.long	7                       # 0x7
	.zero	4
	.quad	tab24
	.long	8                       # 0x8
	.zero	4
	.quad	tab24
	.long	9                       # 0x9
	.zero	4
	.quad	tab24
	.long	11                      # 0xb
	.zero	4
	.quad	tab24
	.long	13                      # 0xd
	.zero	4
	.quad	tab24
	.size	ht, 512

	.type	htc,@object             # @htc
	.p2align	4
htc:
	.long	0                       # 0x0
	.zero	4
	.quad	tab_c0
	.long	0                       # 0x0
	.zero	4
	.quad	tab_c1
	.size	htc, 32

	.type	pretab1,@object         # @pretab1
	.p2align	4
pretab1:
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	2                       # 0x2
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	3                       # 0x3
	.long	2                       # 0x2
	.long	0                       # 0x0
	.size	pretab1, 88

	.type	pretab2,@object         # @pretab2
	.p2align	4
pretab2:
	.zero	88
	.size	pretab2, 88

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"mpg123: Can't rewind stream by %d bits!\n"
	.size	.L.str.2, 41

	.type	tab0,@object            # @tab0
	.local	tab0
	.comm	tab0,2,2
	.type	tab1,@object            # @tab1
	.data
	.p2align	1
tab1:
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	17                      # 0x11
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab1, 14

	.type	tab2,@object            # @tab2
	.p2align	4
tab2:
	.short	65521                   # 0xfff1
	.short	65525                   # 0xfff5
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	34                      # 0x22
	.short	2                       # 0x2
	.short	18                      # 0x12
	.short	65535                   # 0xffff
	.short	33                      # 0x21
	.short	32                      # 0x20
	.short	17                      # 0x11
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab2, 34

	.type	tab3,@object            # @tab3
	.p2align	4
tab3:
	.short	65523                   # 0xfff3
	.short	65525                   # 0xfff5
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	34                      # 0x22
	.short	2                       # 0x2
	.short	18                      # 0x12
	.short	65535                   # 0xffff
	.short	33                      # 0x21
	.short	32                      # 0x20
	.short	16                      # 0x10
	.short	17                      # 0x11
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	0                       # 0x0
	.size	tab3, 34

	.type	tab5,@object            # @tab5
	.p2align	4
tab5:
	.short	65507                   # 0xffe3
	.short	65511                   # 0xffe7
	.short	65513                   # 0xffe9
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	51                      # 0x33
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	49                      # 0x31
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	3                       # 0x3
	.short	65535                   # 0xffff
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	33                      # 0x21
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	17                      # 0x11
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab5, 62

	.type	tab6,@object            # @tab6
	.p2align	4
tab6:
	.short	65511                   # 0xffe7
	.short	65517                   # 0xffed
	.short	65523                   # 0xfff3
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	51                      # 0x33
	.short	3                       # 0x3
	.short	35                      # 0x23
	.short	65535                   # 0xffff
	.short	50                      # 0x32
	.short	48                      # 0x30
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	49                      # 0x31
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	34                      # 0x22
	.short	2                       # 0x2
	.short	18                      # 0x12
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	33                      # 0x21
	.short	32                      # 0x20
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.short	17                      # 0x11
	.short	65535                   # 0xffff
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab6, 62

	.type	tab7,@object            # @tab7
	.p2align	4
tab7:
	.short	65467                   # 0xffbb
	.short	65471                   # 0xffbf
	.short	65479                   # 0xffc7
	.short	65497                   # 0xffd9
	.short	65507                   # 0xffe3
	.short	65519                   # 0xffef
	.short	65525                   # 0xfff5
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	85                      # 0x55
	.short	69                      # 0x45
	.short	65535                   # 0xffff
	.short	84                      # 0x54
	.short	83                      # 0x53
	.short	65535                   # 0xffff
	.short	53                      # 0x35
	.short	68                      # 0x44
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	37                      # 0x25
	.short	82                      # 0x52
	.short	21                      # 0x15
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	81                      # 0x51
	.short	65535                   # 0xffff
	.short	5                       # 0x5
	.short	52                      # 0x34
	.short	65535                   # 0xffff
	.short	80                      # 0x50
	.short	65535                   # 0xffff
	.short	67                      # 0x43
	.short	51                      # 0x33
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	36                      # 0x24
	.short	66                      # 0x42
	.short	20                      # 0x14
	.short	65535                   # 0xffff
	.short	65                      # 0x41
	.short	64                      # 0x40
	.short	65525                   # 0xfff5
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	4                       # 0x4
	.short	35                      # 0x23
	.short	65535                   # 0xffff
	.short	50                      # 0x32
	.short	3                       # 0x3
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	49                      # 0x31
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	18                      # 0x12
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	33                      # 0x21
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	17                      # 0x11
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab7, 142

	.type	tab8,@object            # @tab8
	.p2align	4
tab8:
	.short	65471                   # 0xffbf
	.short	65473                   # 0xffc1
	.short	65477                   # 0xffc5
	.short	65491                   # 0xffd3
	.short	65505                   # 0xffe1
	.short	65517                   # 0xffed
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	85                      # 0x55
	.short	84                      # 0x54
	.short	69                      # 0x45
	.short	83                      # 0x53
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	53                      # 0x35
	.short	68                      # 0x44
	.short	37                      # 0x25
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	82                      # 0x52
	.short	5                       # 0x5
	.short	21                      # 0x15
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	81                      # 0x51
	.short	65535                   # 0xffff
	.short	52                      # 0x34
	.short	67                      # 0x43
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	80                      # 0x50
	.short	51                      # 0x33
	.short	36                      # 0x24
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	66                      # 0x42
	.short	20                      # 0x14
	.short	65                      # 0x41
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	4                       # 0x4
	.short	64                      # 0x40
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	65527                   # 0xfff7
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	49                      # 0x31
	.short	65535                   # 0xffff
	.short	3                       # 0x3
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	33                      # 0x21
	.short	17                      # 0x11
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab8, 142

	.type	tab9,@object            # @tab9
	.p2align	4
tab9:
	.short	65473                   # 0xffc1
	.short	65483                   # 0xffcb
	.short	65495                   # 0xffd7
	.short	65507                   # 0xffe3
	.short	65517                   # 0xffed
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	85                      # 0x55
	.short	69                      # 0x45
	.short	53                      # 0x35
	.short	65535                   # 0xffff
	.short	83                      # 0x53
	.short	65535                   # 0xffff
	.short	84                      # 0x54
	.short	5                       # 0x5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	68                      # 0x44
	.short	37                      # 0x25
	.short	65535                   # 0xffff
	.short	82                      # 0x52
	.short	21                      # 0x15
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	81                      # 0x51
	.short	52                      # 0x34
	.short	65535                   # 0xffff
	.short	67                      # 0x43
	.short	65535                   # 0xffff
	.short	80                      # 0x50
	.short	4                       # 0x4
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	36                      # 0x24
	.short	66                      # 0x42
	.short	65535                   # 0xffff
	.short	51                      # 0x33
	.short	64                      # 0x40
	.short	65535                   # 0xffff
	.short	20                      # 0x14
	.short	65                      # 0x41
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	19                      # 0x13
	.short	65535                   # 0xffff
	.short	49                      # 0x31
	.short	65535                   # 0xffff
	.short	3                       # 0x3
	.short	48                      # 0x30
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	34                      # 0x22
	.short	2                       # 0x2
	.short	18                      # 0x12
	.short	65535                   # 0xffff
	.short	33                      # 0x21
	.short	32                      # 0x20
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	17                      # 0x11
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab9, 142

	.type	tab10,@object           # @tab10
	.p2align	4
tab10:
	.short	65411                   # 0xff83
	.short	65415                   # 0xff87
	.short	65425                   # 0xff91
	.short	65453                   # 0xffad
	.short	65481                   # 0xffc9
	.short	65501                   # 0xffdd
	.short	65515                   # 0xffeb
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	119                     # 0x77
	.short	103                     # 0x67
	.short	65535                   # 0xffff
	.short	118                     # 0x76
	.short	87                      # 0x57
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	117                     # 0x75
	.short	102                     # 0x66
	.short	71                      # 0x47
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	116                     # 0x74
	.short	86                      # 0x56
	.short	65535                   # 0xffff
	.short	101                     # 0x65
	.short	55                      # 0x37
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	115                     # 0x73
	.short	70                      # 0x46
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	85                      # 0x55
	.short	84                      # 0x54
	.short	99                      # 0x63
	.short	65535                   # 0xffff
	.short	39                      # 0x27
	.short	114                     # 0x72
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	100                     # 0x64
	.short	7                       # 0x7
	.short	112                     # 0x70
	.short	65535                   # 0xffff
	.short	98                      # 0x62
	.short	65535                   # 0xffff
	.short	69                      # 0x45
	.short	53                      # 0x35
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	6                       # 0x6
	.short	65535                   # 0xffff
	.short	83                      # 0x53
	.short	68                      # 0x44
	.short	23                      # 0x17
	.short	65519                   # 0xffef
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	113                     # 0x71
	.short	65535                   # 0xffff
	.short	54                      # 0x36
	.short	38                      # 0x26
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	37                      # 0x25
	.short	82                      # 0x52
	.short	21                      # 0x15
	.short	65535                   # 0xffff
	.short	81                      # 0x51
	.short	65535                   # 0xffff
	.short	52                      # 0x34
	.short	67                      # 0x43
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	22                      # 0x16
	.short	97                      # 0x61
	.short	65535                   # 0xffff
	.short	96                      # 0x60
	.short	65535                   # 0xffff
	.short	5                       # 0x5
	.short	80                      # 0x50
	.short	65517                   # 0xffed
	.short	65525                   # 0xfff5
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	36                      # 0x24
	.short	66                      # 0x42
	.short	65535                   # 0xffff
	.short	51                      # 0x33
	.short	4                       # 0x4
	.short	65535                   # 0xffff
	.short	20                      # 0x14
	.short	65                      # 0x41
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	64                      # 0x40
	.short	35                      # 0x23
	.short	65535                   # 0xffff
	.short	50                      # 0x32
	.short	3                       # 0x3
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	49                      # 0x31
	.short	65535                   # 0xffff
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	33                      # 0x21
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	17                      # 0x11
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab10, 254

	.type	tab11,@object           # @tab11
	.p2align	4
tab11:
	.short	65415                   # 0xff87
	.short	65423                   # 0xff8f
	.short	65447                   # 0xffa7
	.short	65477                   # 0xffc5
	.short	65493                   # 0xffd5
	.short	65509                   # 0xffe5
	.short	65519                   # 0xffef
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	119                     # 0x77
	.short	103                     # 0x67
	.short	65535                   # 0xffff
	.short	118                     # 0x76
	.short	117                     # 0x75
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	102                     # 0x66
	.short	71                      # 0x47
	.short	65535                   # 0xffff
	.short	116                     # 0x74
	.short	65535                   # 0xffff
	.short	87                      # 0x57
	.short	85                      # 0x55
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	86                      # 0x56
	.short	101                     # 0x65
	.short	55                      # 0x37
	.short	65535                   # 0xffff
	.short	115                     # 0x73
	.short	70                      # 0x46
	.short	65527                   # 0xfff7
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	69                      # 0x45
	.short	84                      # 0x54
	.short	65535                   # 0xffff
	.short	53                      # 0x35
	.short	83                      # 0x53
	.short	39                      # 0x27
	.short	65535                   # 0xffff
	.short	114                     # 0x72
	.short	65535                   # 0xffff
	.short	100                     # 0x64
	.short	7                       # 0x7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	113                     # 0x71
	.short	65535                   # 0xffff
	.short	23                      # 0x17
	.short	112                     # 0x70
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	54                      # 0x36
	.short	99                      # 0x63
	.short	65535                   # 0xffff
	.short	96                      # 0x60
	.short	65535                   # 0xffff
	.short	68                      # 0x44
	.short	37                      # 0x25
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	82                      # 0x52
	.short	5                       # 0x5
	.short	21                      # 0x15
	.short	98                      # 0x62
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	38                      # 0x26
	.short	6                       # 0x6
	.short	22                      # 0x16
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	97                      # 0x61
	.short	65535                   # 0xffff
	.short	81                      # 0x51
	.short	52                      # 0x34
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	80                      # 0x50
	.short	65535                   # 0xffff
	.short	67                      # 0x43
	.short	51                      # 0x33
	.short	65535                   # 0xffff
	.short	36                      # 0x24
	.short	66                      # 0x42
	.short	65521                   # 0xfff1
	.short	65525                   # 0xfff5
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	20                      # 0x14
	.short	65                      # 0x41
	.short	65535                   # 0xffff
	.short	4                       # 0x4
	.short	64                      # 0x40
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	49                      # 0x31
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	3                       # 0x3
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	33                      # 0x21
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	17                      # 0x11
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab11, 254

	.type	tab12,@object           # @tab12
	.p2align	4
tab12:
	.short	65421                   # 0xff8d
	.short	65437                   # 0xff9d
	.short	65463                   # 0xffb7
	.short	65491                   # 0xffd3
	.short	65509                   # 0xffe5
	.short	65519                   # 0xffef
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	119                     # 0x77
	.short	103                     # 0x67
	.short	118                     # 0x76
	.short	65535                   # 0xffff
	.short	87                      # 0x57
	.short	117                     # 0x75
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	102                     # 0x66
	.short	71                      # 0x47
	.short	65535                   # 0xffff
	.short	116                     # 0x74
	.short	101                     # 0x65
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	86                      # 0x56
	.short	55                      # 0x37
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	115                     # 0x73
	.short	85                      # 0x55
	.short	39                      # 0x27
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	114                     # 0x72
	.short	70                      # 0x46
	.short	65535                   # 0xffff
	.short	100                     # 0x64
	.short	23                      # 0x17
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	113                     # 0x71
	.short	65535                   # 0xffff
	.short	7                       # 0x7
	.short	112                     # 0x70
	.short	65535                   # 0xffff
	.short	54                      # 0x36
	.short	99                      # 0x63
	.short	65523                   # 0xfff3
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	69                      # 0x45
	.short	84                      # 0x54
	.short	65535                   # 0xffff
	.short	68                      # 0x44
	.short	65535                   # 0xffff
	.short	6                       # 0x6
	.short	5                       # 0x5
	.short	65535                   # 0xffff
	.short	38                      # 0x26
	.short	98                      # 0x62
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	97                      # 0x61
	.short	65535                   # 0xffff
	.short	22                      # 0x16
	.short	96                      # 0x60
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	53                      # 0x35
	.short	83                      # 0x53
	.short	65535                   # 0xffff
	.short	37                      # 0x25
	.short	82                      # 0x52
	.short	65519                   # 0xffef
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	21                      # 0x15
	.short	81                      # 0x51
	.short	65535                   # 0xffff
	.short	52                      # 0x34
	.short	67                      # 0x43
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	80                      # 0x50
	.short	4                       # 0x4
	.short	36                      # 0x24
	.short	65535                   # 0xffff
	.short	66                      # 0x42
	.short	20                      # 0x14
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	51                      # 0x33
	.short	65                      # 0x41
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	65525                   # 0xfff5
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	64                      # 0x40
	.short	3                       # 0x3
	.short	48                      # 0x30
	.short	19                      # 0x13
	.short	65535                   # 0xffff
	.short	49                      # 0x31
	.short	34                      # 0x22
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	33                      # 0x21
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	0                       # 0x0
	.short	17                      # 0x11
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.size	tab12, 254

	.type	tab13,@object           # @tab13
	.p2align	4
tab13:
	.short	65027                   # 0xfe03
	.short	65033                   # 0xfe09
	.short	65061                   # 0xfe25
	.short	65131                   # 0xfe6b
	.short	65203                   # 0xfeb3
	.short	65271                   # 0xfef7
	.short	65331                   # 0xff33
	.short	65383                   # 0xff67
	.short	65421                   # 0xff8d
	.short	65453                   # 0xffad
	.short	65483                   # 0xffcb
	.short	65501                   # 0xffdd
	.short	65515                   # 0xffeb
	.short	65523                   # 0xfff3
	.short	65527                   # 0xfff7
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	254                     # 0xfe
	.short	252                     # 0xfc
	.short	253                     # 0xfd
	.short	237                     # 0xed
	.short	255                     # 0xff
	.short	65535                   # 0xffff
	.short	239                     # 0xef
	.short	223                     # 0xdf
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	238                     # 0xee
	.short	207                     # 0xcf
	.short	65535                   # 0xffff
	.short	222                     # 0xde
	.short	191                     # 0xbf
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	251                     # 0xfb
	.short	206                     # 0xce
	.short	65535                   # 0xffff
	.short	220                     # 0xdc
	.short	65535                   # 0xffff
	.short	175                     # 0xaf
	.short	233                     # 0xe9
	.short	65535                   # 0xffff
	.short	236                     # 0xec
	.short	221                     # 0xdd
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	250                     # 0xfa
	.short	205                     # 0xcd
	.short	190                     # 0xbe
	.short	65535                   # 0xffff
	.short	235                     # 0xeb
	.short	159                     # 0x9f
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	249                     # 0xf9
	.short	234                     # 0xea
	.short	65535                   # 0xffff
	.short	189                     # 0xbd
	.short	219                     # 0xdb
	.short	65519                   # 0xffef
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	143                     # 0x8f
	.short	248                     # 0xf8
	.short	65535                   # 0xffff
	.short	204                     # 0xcc
	.short	65535                   # 0xffff
	.short	174                     # 0xae
	.short	158                     # 0x9e
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	142                     # 0x8e
	.short	65535                   # 0xffff
	.short	127                     # 0x7f
	.short	126                     # 0x7e
	.short	247                     # 0xf7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	218                     # 0xda
	.short	65535                   # 0xffff
	.short	173                     # 0xad
	.short	188                     # 0xbc
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	203                     # 0xcb
	.short	246                     # 0xf6
	.short	111                     # 0x6f
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	232                     # 0xe8
	.short	95                      # 0x5f
	.short	65535                   # 0xffff
	.short	157                     # 0x9d
	.short	217                     # 0xd9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	245                     # 0xf5
	.short	231                     # 0xe7
	.short	65535                   # 0xffff
	.short	172                     # 0xac
	.short	187                     # 0xbb
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	79                      # 0x4f
	.short	244                     # 0xf4
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	202                     # 0xca
	.short	230                     # 0xe6
	.short	243                     # 0xf3
	.short	65535                   # 0xffff
	.short	63                      # 0x3f
	.short	65535                   # 0xffff
	.short	141                     # 0x8d
	.short	216                     # 0xd8
	.short	65515                   # 0xffeb
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	47                      # 0x2f
	.short	242                     # 0xf2
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	110                     # 0x6e
	.short	156                     # 0x9c
	.short	15                      # 0xf
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	201                     # 0xc9
	.short	94                      # 0x5e
	.short	171                     # 0xab
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	125                     # 0x7d
	.short	215                     # 0xd7
	.short	78                      # 0x4e
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	200                     # 0xc8
	.short	214                     # 0xd6
	.short	62                      # 0x3e
	.short	65535                   # 0xffff
	.short	185                     # 0xb9
	.short	65535                   # 0xffff
	.short	155                     # 0x9b
	.short	170                     # 0xaa
	.short	65535                   # 0xffff
	.short	31                      # 0x1f
	.short	241                     # 0xf1
	.short	65513                   # 0xffe9
	.short	65523                   # 0xfff3
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	240                     # 0xf0
	.short	65535                   # 0xffff
	.short	186                     # 0xba
	.short	229                     # 0xe5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	228                     # 0xe4
	.short	140                     # 0x8c
	.short	65535                   # 0xffff
	.short	109                     # 0x6d
	.short	227                     # 0xe3
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	226                     # 0xe2
	.short	65535                   # 0xffff
	.short	46                      # 0x2e
	.short	14                      # 0xe
	.short	65535                   # 0xffff
	.short	30                      # 0x1e
	.short	225                     # 0xe1
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	224                     # 0xe0
	.short	93                      # 0x5d
	.short	65535                   # 0xffff
	.short	213                     # 0xd5
	.short	124                     # 0x7c
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	199                     # 0xc7
	.short	77                      # 0x4d
	.short	65535                   # 0xffff
	.short	139                     # 0x8b
	.short	184                     # 0xb8
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	212                     # 0xd4
	.short	154                     # 0x9a
	.short	65535                   # 0xffff
	.short	169                     # 0xa9
	.short	108                     # 0x6c
	.short	65535                   # 0xffff
	.short	198                     # 0xc6
	.short	61                      # 0x3d
	.short	65499                   # 0xffdb
	.short	65515                   # 0xffeb
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	211                     # 0xd3
	.short	123                     # 0x7b
	.short	45                      # 0x2d
	.short	65535                   # 0xffff
	.short	210                     # 0xd2
	.short	29                      # 0x1d
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	183                     # 0xb7
	.short	65535                   # 0xffff
	.short	92                      # 0x5c
	.short	197                     # 0xc5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	153                     # 0x99
	.short	122                     # 0x7a
	.short	195                     # 0xc3
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	167                     # 0xa7
	.short	151                     # 0x97
	.short	75                      # 0x4b
	.short	209                     # 0xd1
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	13                      # 0xd
	.short	208                     # 0xd0
	.short	65535                   # 0xffff
	.short	138                     # 0x8a
	.short	168                     # 0xa8
	.short	65525                   # 0xfff5
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	76                      # 0x4c
	.short	196                     # 0xc4
	.short	65535                   # 0xffff
	.short	107                     # 0x6b
	.short	182                     # 0xb6
	.short	65535                   # 0xffff
	.short	60                      # 0x3c
	.short	44                      # 0x2c
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	194                     # 0xc2
	.short	91                      # 0x5b
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	181                     # 0xb5
	.short	137                     # 0x89
	.short	28                      # 0x1c
	.short	65493                   # 0xffd5
	.short	65513                   # 0xffe9
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	193                     # 0xc1
	.short	65535                   # 0xffff
	.short	152                     # 0x98
	.short	12                      # 0xc
	.short	65535                   # 0xffff
	.short	192                     # 0xc0
	.short	65535                   # 0xffff
	.short	180                     # 0xb4
	.short	106                     # 0x6a
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	166                     # 0xa6
	.short	121                     # 0x79
	.short	59                      # 0x3b
	.short	65535                   # 0xffff
	.short	179                     # 0xb3
	.short	65535                   # 0xffff
	.short	136                     # 0x88
	.short	90                      # 0x5a
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	43                      # 0x2b
	.short	65535                   # 0xffff
	.short	165                     # 0xa5
	.short	105                     # 0x69
	.short	65535                   # 0xffff
	.short	164                     # 0xa4
	.short	65535                   # 0xffff
	.short	120                     # 0x78
	.short	135                     # 0x87
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	148                     # 0x94
	.short	65535                   # 0xffff
	.short	119                     # 0x77
	.short	118                     # 0x76
	.short	178                     # 0xb2
	.short	65525                   # 0xfff5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	27                      # 0x1b
	.short	177                     # 0xb1
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	11                      # 0xb
	.short	176                     # 0xb0
	.short	65535                   # 0xffff
	.short	150                     # 0x96
	.short	74                      # 0x4a
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	58                      # 0x3a
	.short	163                     # 0xa3
	.short	65535                   # 0xffff
	.short	89                      # 0x59
	.short	149                     # 0x95
	.short	65535                   # 0xffff
	.short	42                      # 0x2a
	.short	162                     # 0xa2
	.short	65489                   # 0xffd1
	.short	65513                   # 0xffe9
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	26                      # 0x1a
	.short	161                     # 0xa1
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	10                      # 0xa
	.short	104                     # 0x68
	.short	160                     # 0xa0
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	134                     # 0x86
	.short	73                      # 0x49
	.short	147                     # 0x93
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	57                      # 0x39
	.short	88                      # 0x58
	.short	65535                   # 0xffff
	.short	133                     # 0x85
	.short	103                     # 0x67
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	41                      # 0x29
	.short	146                     # 0x92
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	87                      # 0x57
	.short	117                     # 0x75
	.short	56                      # 0x38
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	131                     # 0x83
	.short	65535                   # 0xffff
	.short	102                     # 0x66
	.short	71                      # 0x47
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	116                     # 0x74
	.short	86                      # 0x56
	.short	65535                   # 0xffff
	.short	101                     # 0x65
	.short	115                     # 0x73
	.short	65525                   # 0xfff5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	25                      # 0x19
	.short	145                     # 0x91
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	9                       # 0x9
	.short	144                     # 0x90
	.short	65535                   # 0xffff
	.short	72                      # 0x48
	.short	132                     # 0x84
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	114                     # 0x72
	.short	65535                   # 0xffff
	.short	70                      # 0x46
	.short	100                     # 0x64
	.short	40                      # 0x28
	.short	65535                   # 0xffff
	.short	130                     # 0x82
	.short	24                      # 0x18
	.short	65495                   # 0xffd7
	.short	65509                   # 0xffe5
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	55                      # 0x37
	.short	39                      # 0x27
	.short	23                      # 0x17
	.short	65535                   # 0xffff
	.short	113                     # 0x71
	.short	65535                   # 0xffff
	.short	85                      # 0x55
	.short	7                       # 0x7
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	112                     # 0x70
	.short	54                      # 0x36
	.short	65535                   # 0xffff
	.short	99                      # 0x63
	.short	69                      # 0x45
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	84                      # 0x54
	.short	38                      # 0x26
	.short	65535                   # 0xffff
	.short	98                      # 0x62
	.short	53                      # 0x35
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	129                     # 0x81
	.short	65535                   # 0xffff
	.short	8                       # 0x8
	.short	128                     # 0x80
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	22                      # 0x16
	.short	97                      # 0x61
	.short	65535                   # 0xffff
	.short	6                       # 0x6
	.short	96                      # 0x60
	.short	65523                   # 0xfff3
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	83                      # 0x53
	.short	68                      # 0x44
	.short	37                      # 0x25
	.short	65535                   # 0xffff
	.short	82                      # 0x52
	.short	5                       # 0x5
	.short	65535                   # 0xffff
	.short	21                      # 0x15
	.short	81                      # 0x51
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	52                      # 0x34
	.short	67                      # 0x43
	.short	65535                   # 0xffff
	.short	80                      # 0x50
	.short	36                      # 0x24
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	66                      # 0x42
	.short	51                      # 0x33
	.short	20                      # 0x14
	.short	65517                   # 0xffed
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	65                      # 0x41
	.short	65535                   # 0xffff
	.short	4                       # 0x4
	.short	64                      # 0x40
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	19                      # 0x13
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	49                      # 0x31
	.short	3                       # 0x3
	.short	65535                   # 0xffff
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	33                      # 0x21
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	17                      # 0x11
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab13, 1022

	.type	tab15,@object           # @tab15
	.p2align	4
tab15:
	.short	65041                   # 0xfe11
	.short	65091                   # 0xfe43
	.short	65181                   # 0xfe9d
	.short	65273                   # 0xfef9
	.short	65353                   # 0xff49
	.short	65421                   # 0xff8d
	.short	65459                   # 0xffb3
	.short	65493                   # 0xffd5
	.short	65509                   # 0xffe5
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	255                     # 0xff
	.short	239                     # 0xef
	.short	65535                   # 0xffff
	.short	254                     # 0xfe
	.short	223                     # 0xdf
	.short	65535                   # 0xffff
	.short	238                     # 0xee
	.short	65535                   # 0xffff
	.short	253                     # 0xfd
	.short	207                     # 0xcf
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	252                     # 0xfc
	.short	222                     # 0xde
	.short	65535                   # 0xffff
	.short	237                     # 0xed
	.short	191                     # 0xbf
	.short	65535                   # 0xffff
	.short	251                     # 0xfb
	.short	65535                   # 0xffff
	.short	206                     # 0xce
	.short	236                     # 0xec
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	221                     # 0xdd
	.short	175                     # 0xaf
	.short	65535                   # 0xffff
	.short	250                     # 0xfa
	.short	190                     # 0xbe
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	235                     # 0xeb
	.short	205                     # 0xcd
	.short	65535                   # 0xffff
	.short	220                     # 0xdc
	.short	159                     # 0x9f
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	249                     # 0xf9
	.short	234                     # 0xea
	.short	65535                   # 0xffff
	.short	189                     # 0xbd
	.short	219                     # 0xdb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	143                     # 0x8f
	.short	248                     # 0xf8
	.short	65535                   # 0xffff
	.short	204                     # 0xcc
	.short	158                     # 0x9e
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	233                     # 0xe9
	.short	127                     # 0x7f
	.short	65535                   # 0xffff
	.short	247                     # 0xf7
	.short	173                     # 0xad
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	218                     # 0xda
	.short	188                     # 0xbc
	.short	65535                   # 0xffff
	.short	111                     # 0x6f
	.short	65535                   # 0xffff
	.short	174                     # 0xae
	.short	15                      # 0xf
	.short	65517                   # 0xffed
	.short	65525                   # 0xfff5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	203                     # 0xcb
	.short	246                     # 0xf6
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	142                     # 0x8e
	.short	232                     # 0xe8
	.short	65535                   # 0xffff
	.short	95                      # 0x5f
	.short	157                     # 0x9d
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	245                     # 0xf5
	.short	126                     # 0x7e
	.short	65535                   # 0xffff
	.short	231                     # 0xe7
	.short	172                     # 0xac
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	202                     # 0xca
	.short	187                     # 0xbb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	217                     # 0xd9
	.short	141                     # 0x8d
	.short	79                      # 0x4f
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	244                     # 0xf4
	.short	63                      # 0x3f
	.short	65535                   # 0xffff
	.short	243                     # 0xf3
	.short	216                     # 0xd8
	.short	65503                   # 0xffdf
	.short	65519                   # 0xffef
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	230                     # 0xe6
	.short	47                      # 0x2f
	.short	65535                   # 0xffff
	.short	242                     # 0xf2
	.short	65535                   # 0xffff
	.short	110                     # 0x6e
	.short	240                     # 0xf0
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	31                      # 0x1f
	.short	241                     # 0xf1
	.short	65535                   # 0xffff
	.short	156                     # 0x9c
	.short	201                     # 0xc9
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	94                      # 0x5e
	.short	171                     # 0xab
	.short	65535                   # 0xffff
	.short	186                     # 0xba
	.short	229                     # 0xe5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	125                     # 0x7d
	.short	215                     # 0xd7
	.short	65535                   # 0xffff
	.short	78                      # 0x4e
	.short	228                     # 0xe4
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	140                     # 0x8c
	.short	200                     # 0xc8
	.short	65535                   # 0xffff
	.short	62                      # 0x3e
	.short	109                     # 0x6d
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	214                     # 0xd6
	.short	227                     # 0xe3
	.short	65535                   # 0xffff
	.short	155                     # 0x9b
	.short	185                     # 0xb9
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	46                      # 0x2e
	.short	170                     # 0xaa
	.short	65535                   # 0xffff
	.short	226                     # 0xe2
	.short	30                      # 0x1e
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	225                     # 0xe1
	.short	65535                   # 0xffff
	.short	14                      # 0xe
	.short	224                     # 0xe0
	.short	65535                   # 0xffff
	.short	93                      # 0x5d
	.short	213                     # 0xd5
	.short	65491                   # 0xffd3
	.short	65511                   # 0xffe7
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	124                     # 0x7c
	.short	199                     # 0xc7
	.short	65535                   # 0xffff
	.short	77                      # 0x4d
	.short	139                     # 0x8b
	.short	65535                   # 0xffff
	.short	212                     # 0xd4
	.short	65535                   # 0xffff
	.short	184                     # 0xb8
	.short	154                     # 0x9a
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	169                     # 0xa9
	.short	108                     # 0x6c
	.short	65535                   # 0xffff
	.short	198                     # 0xc6
	.short	61                      # 0x3d
	.short	65535                   # 0xffff
	.short	211                     # 0xd3
	.short	210                     # 0xd2
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	45                      # 0x2d
	.short	13                      # 0xd
	.short	29                      # 0x1d
	.short	65535                   # 0xffff
	.short	123                     # 0x7b
	.short	183                     # 0xb7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	209                     # 0xd1
	.short	65535                   # 0xffff
	.short	92                      # 0x5c
	.short	208                     # 0xd0
	.short	65535                   # 0xffff
	.short	197                     # 0xc5
	.short	138                     # 0x8a
	.short	65519                   # 0xffef
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	168                     # 0xa8
	.short	76                      # 0x4c
	.short	65535                   # 0xffff
	.short	196                     # 0xc4
	.short	107                     # 0x6b
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	182                     # 0xb6
	.short	65535                   # 0xffff
	.short	153                     # 0x99
	.short	12                      # 0xc
	.short	65535                   # 0xffff
	.short	60                      # 0x3c
	.short	195                     # 0xc3
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	122                     # 0x7a
	.short	167                     # 0xa7
	.short	65535                   # 0xffff
	.short	166                     # 0xa6
	.short	65535                   # 0xffff
	.short	192                     # 0xc0
	.short	11                      # 0xb
	.short	65535                   # 0xffff
	.short	194                     # 0xc2
	.short	65535                   # 0xffff
	.short	44                      # 0x2c
	.short	91                      # 0x5b
	.short	65481                   # 0xffc9
	.short	65507                   # 0xffe3
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	181                     # 0xb5
	.short	28                      # 0x1c
	.short	65535                   # 0xffff
	.short	137                     # 0x89
	.short	152                     # 0x98
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	193                     # 0xc1
	.short	75                      # 0x4b
	.short	65535                   # 0xffff
	.short	180                     # 0xb4
	.short	106                     # 0x6a
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	59                      # 0x3b
	.short	121                     # 0x79
	.short	179                     # 0xb3
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	151                     # 0x97
	.short	136                     # 0x88
	.short	65535                   # 0xffff
	.short	43                      # 0x2b
	.short	90                      # 0x5a
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	178                     # 0xb2
	.short	65535                   # 0xffff
	.short	165                     # 0xa5
	.short	27                      # 0x1b
	.short	65535                   # 0xffff
	.short	177                     # 0xb1
	.short	65535                   # 0xffff
	.short	176                     # 0xb0
	.short	105                     # 0x69
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	150                     # 0x96
	.short	74                      # 0x4a
	.short	65535                   # 0xffff
	.short	164                     # 0xa4
	.short	120                     # 0x78
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	135                     # 0x87
	.short	58                      # 0x3a
	.short	163                     # 0xa3
	.short	65519                   # 0xffef
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	89                      # 0x59
	.short	149                     # 0x95
	.short	65535                   # 0xffff
	.short	42                      # 0x2a
	.short	162                     # 0xa2
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	26                      # 0x1a
	.short	161                     # 0xa1
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	10                      # 0xa
	.short	160                     # 0xa0
	.short	104                     # 0x68
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	134                     # 0x86
	.short	73                      # 0x49
	.short	65535                   # 0xffff
	.short	148                     # 0x94
	.short	57                      # 0x39
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	147                     # 0x93
	.short	65535                   # 0xffff
	.short	119                     # 0x77
	.short	9                       # 0x9
	.short	65535                   # 0xffff
	.short	88                      # 0x58
	.short	133                     # 0x85
	.short	65483                   # 0xffcb
	.short	65507                   # 0xffe3
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	41                      # 0x29
	.short	103                     # 0x67
	.short	65535                   # 0xffff
	.short	118                     # 0x76
	.short	146                     # 0x92
	.short	65535                   # 0xffff
	.short	145                     # 0x91
	.short	65535                   # 0xffff
	.short	25                      # 0x19
	.short	144                     # 0x90
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	72                      # 0x48
	.short	132                     # 0x84
	.short	65535                   # 0xffff
	.short	87                      # 0x57
	.short	117                     # 0x75
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	56                      # 0x38
	.short	131                     # 0x83
	.short	65535                   # 0xffff
	.short	102                     # 0x66
	.short	71                      # 0x47
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	40                      # 0x28
	.short	130                     # 0x82
	.short	65535                   # 0xffff
	.short	24                      # 0x18
	.short	129                     # 0x81
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	116                     # 0x74
	.short	8                       # 0x8
	.short	65535                   # 0xffff
	.short	128                     # 0x80
	.short	86                      # 0x56
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	101                     # 0x65
	.short	55                      # 0x37
	.short	65535                   # 0xffff
	.short	115                     # 0x73
	.short	70                      # 0x46
	.short	65519                   # 0xffef
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	39                      # 0x27
	.short	114                     # 0x72
	.short	65535                   # 0xffff
	.short	100                     # 0x64
	.short	23                      # 0x17
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	85                      # 0x55
	.short	113                     # 0x71
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	7                       # 0x7
	.short	112                     # 0x70
	.short	54                      # 0x36
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	99                      # 0x63
	.short	69                      # 0x45
	.short	65535                   # 0xffff
	.short	84                      # 0x54
	.short	38                      # 0x26
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	98                      # 0x62
	.short	22                      # 0x16
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	6                       # 0x6
	.short	96                      # 0x60
	.short	53                      # 0x35
	.short	65503                   # 0xffdf
	.short	65517                   # 0xffed
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	97                      # 0x61
	.short	65535                   # 0xffff
	.short	83                      # 0x53
	.short	68                      # 0x44
	.short	65535                   # 0xffff
	.short	37                      # 0x25
	.short	82                      # 0x52
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	21                      # 0x15
	.short	81                      # 0x51
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	5                       # 0x5
	.short	80                      # 0x50
	.short	52                      # 0x34
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	67                      # 0x43
	.short	36                      # 0x24
	.short	65535                   # 0xffff
	.short	66                      # 0x42
	.short	51                      # 0x33
	.short	65535                   # 0xffff
	.short	65                      # 0x41
	.short	65535                   # 0xffff
	.short	20                      # 0x14
	.short	4                       # 0x4
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	64                      # 0x40
	.short	3                       # 0x3
	.short	19                      # 0x13
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	49                      # 0x31
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	65527                   # 0xfff7
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	33                      # 0x21
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	17                      # 0x11
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab15, 1022

	.type	tab16,@object           # @tab16
	.p2align	4
tab16:
	.short	65027                   # 0xfe03
	.short	65033                   # 0xfe09
	.short	65075                   # 0xfe33
	.short	65213                   # 0xfebd
	.short	65433                   # 0xff99
	.short	65499                   # 0xffdb
	.short	65509                   # 0xffe5
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	239                     # 0xef
	.short	254                     # 0xfe
	.short	65535                   # 0xffff
	.short	223                     # 0xdf
	.short	253                     # 0xfd
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	207                     # 0xcf
	.short	252                     # 0xfc
	.short	65535                   # 0xffff
	.short	191                     # 0xbf
	.short	251                     # 0xfb
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	175                     # 0xaf
	.short	65535                   # 0xffff
	.short	250                     # 0xfa
	.short	159                     # 0x9f
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	249                     # 0xf9
	.short	248                     # 0xf8
	.short	143                     # 0x8f
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	127                     # 0x7f
	.short	247                     # 0xf7
	.short	65535                   # 0xffff
	.short	111                     # 0x6f
	.short	246                     # 0xf6
	.short	255                     # 0xff
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	95                      # 0x5f
	.short	245                     # 0xf5
	.short	79                      # 0x4f
	.short	65535                   # 0xffff
	.short	244                     # 0xf4
	.short	243                     # 0xf3
	.short	65483                   # 0xffcb
	.short	65535                   # 0xffff
	.short	240                     # 0xf0
	.short	65535                   # 0xffff
	.short	63                      # 0x3f
	.short	65507                   # 0xffe3
	.short	65517                   # 0xffed
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	206                     # 0xce
	.short	65535                   # 0xffff
	.short	236                     # 0xec
	.short	221                     # 0xdd
	.short	222                     # 0xde
	.short	65535                   # 0xffff
	.short	233                     # 0xe9
	.short	65535                   # 0xffff
	.short	234                     # 0xea
	.short	217                     # 0xd9
	.short	65535                   # 0xffff
	.short	238                     # 0xee
	.short	65535                   # 0xffff
	.short	237                     # 0xed
	.short	235                     # 0xeb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	190                     # 0xbe
	.short	205                     # 0xcd
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	220                     # 0xdc
	.short	219                     # 0xdb
	.short	174                     # 0xae
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	204                     # 0xcc
	.short	65535                   # 0xffff
	.short	173                     # 0xad
	.short	218                     # 0xda
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	126                     # 0x7e
	.short	172                     # 0xac
	.short	202                     # 0xca
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	201                     # 0xc9
	.short	125                     # 0x7d
	.short	94                      # 0x5e
	.short	189                     # 0xbd
	.short	242                     # 0xf2
	.short	65443                   # 0xffa3
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	47                      # 0x2f
	.short	15                      # 0xf
	.short	31                      # 0x1f
	.short	65535                   # 0xffff
	.short	241                     # 0xf1
	.short	65487                   # 0xffcf
	.short	65511                   # 0xffe7
	.short	65523                   # 0xfff3
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	158                     # 0x9e
	.short	65535                   # 0xffff
	.short	188                     # 0xbc
	.short	203                     # 0xcb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	142                     # 0x8e
	.short	232                     # 0xe8
	.short	65535                   # 0xffff
	.short	157                     # 0x9d
	.short	231                     # 0xe7
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	187                     # 0xbb
	.short	141                     # 0x8d
	.short	65535                   # 0xffff
	.short	216                     # 0xd8
	.short	110                     # 0x6e
	.short	65535                   # 0xffff
	.short	230                     # 0xe6
	.short	156                     # 0x9c
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	171                     # 0xab
	.short	186                     # 0xba
	.short	65535                   # 0xffff
	.short	229                     # 0xe5
	.short	215                     # 0xd7
	.short	65535                   # 0xffff
	.short	78                      # 0x4e
	.short	65535                   # 0xffff
	.short	228                     # 0xe4
	.short	140                     # 0x8c
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	200                     # 0xc8
	.short	62                      # 0x3e
	.short	65535                   # 0xffff
	.short	109                     # 0x6d
	.short	65535                   # 0xffff
	.short	214                     # 0xd6
	.short	155                     # 0x9b
	.short	65517                   # 0xffed
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	185                     # 0xb9
	.short	170                     # 0xaa
	.short	225                     # 0xe1
	.short	65535                   # 0xffff
	.short	212                     # 0xd4
	.short	65535                   # 0xffff
	.short	184                     # 0xb8
	.short	169                     # 0xa9
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	123                     # 0x7b
	.short	65535                   # 0xffff
	.short	183                     # 0xb7
	.short	208                     # 0xd0
	.short	227                     # 0xe3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	14                      # 0xe
	.short	224                     # 0xe0
	.short	65535                   # 0xffff
	.short	93                      # 0x5d
	.short	213                     # 0xd5
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	124                     # 0x7c
	.short	199                     # 0xc7
	.short	65535                   # 0xffff
	.short	77                      # 0x4d
	.short	139                     # 0x8b
	.short	65461                   # 0xffb5
	.short	65491                   # 0xffd3
	.short	65509                   # 0xffe5
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	154                     # 0x9a
	.short	108                     # 0x6c
	.short	65535                   # 0xffff
	.short	198                     # 0xc6
	.short	61                      # 0x3d
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	92                      # 0x5c
	.short	197                     # 0xc5
	.short	13                      # 0xd
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	138                     # 0x8a
	.short	168                     # 0xa8
	.short	65535                   # 0xffff
	.short	153                     # 0x99
	.short	76                      # 0x4c
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	182                     # 0xb6
	.short	122                     # 0x7a
	.short	60                      # 0x3c
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	91                      # 0x5b
	.short	137                     # 0x89
	.short	28                      # 0x1c
	.short	65535                   # 0xffff
	.short	192                     # 0xc0
	.short	65535                   # 0xffff
	.short	152                     # 0x98
	.short	121                     # 0x79
	.short	65535                   # 0xffff
	.short	226                     # 0xe2
	.short	65535                   # 0xffff
	.short	46                      # 0x2e
	.short	30                      # 0x1e
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	211                     # 0xd3
	.short	45                      # 0x2d
	.short	65535                   # 0xffff
	.short	210                     # 0xd2
	.short	209                     # 0xd1
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	59                      # 0x3b
	.short	65535                   # 0xffff
	.short	151                     # 0x97
	.short	136                     # 0x88
	.short	29                      # 0x1d
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	196                     # 0xc4
	.short	107                     # 0x6b
	.short	65535                   # 0xffff
	.short	195                     # 0xc3
	.short	167                     # 0xa7
	.short	65535                   # 0xffff
	.short	44                      # 0x2c
	.short	65535                   # 0xffff
	.short	194                     # 0xc2
	.short	181                     # 0xb5
	.short	65513                   # 0xffe9
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	193                     # 0xc1
	.short	12                      # 0xc
	.short	65535                   # 0xffff
	.short	75                      # 0x4b
	.short	180                     # 0xb4
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	106                     # 0x6a
	.short	166                     # 0xa6
	.short	179                     # 0xb3
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	90                      # 0x5a
	.short	165                     # 0xa5
	.short	43                      # 0x2b
	.short	65535                   # 0xffff
	.short	178                     # 0xb2
	.short	27                      # 0x1b
	.short	65523                   # 0xfff3
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	177                     # 0xb1
	.short	65535                   # 0xffff
	.short	11                      # 0xb
	.short	176                     # 0xb0
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	105                     # 0x69
	.short	150                     # 0x96
	.short	65535                   # 0xffff
	.short	74                      # 0x4a
	.short	164                     # 0xa4
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	120                     # 0x78
	.short	135                     # 0x87
	.short	163                     # 0xa3
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	58                      # 0x3a
	.short	89                      # 0x59
	.short	42                      # 0x2a
	.short	65439                   # 0xff9f
	.short	65479                   # 0xffc7
	.short	65503                   # 0xffdf
	.short	65517                   # 0xffed
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	149                     # 0x95
	.short	104                     # 0x68
	.short	161                     # 0xa1
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	134                     # 0x86
	.short	119                     # 0x77
	.short	148                     # 0x94
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	73                      # 0x49
	.short	87                      # 0x57
	.short	103                     # 0x67
	.short	162                     # 0xa2
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	26                      # 0x1a
	.short	65535                   # 0xffff
	.short	10                      # 0xa
	.short	160                     # 0xa0
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	57                      # 0x39
	.short	147                     # 0x93
	.short	65535                   # 0xffff
	.short	88                      # 0x58
	.short	133                     # 0x85
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	41                      # 0x29
	.short	146                     # 0x92
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	118                     # 0x76
	.short	9                       # 0x9
	.short	25                      # 0x19
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	145                     # 0x91
	.short	65535                   # 0xffff
	.short	144                     # 0x90
	.short	72                      # 0x48
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	132                     # 0x84
	.short	117                     # 0x75
	.short	65535                   # 0xffff
	.short	56                      # 0x38
	.short	131                     # 0x83
	.short	65515                   # 0xffeb
	.short	65525                   # 0xfff5
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	102                     # 0x66
	.short	40                      # 0x28
	.short	130                     # 0x82
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	71                      # 0x47
	.short	116                     # 0x74
	.short	24                      # 0x18
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	129                     # 0x81
	.short	128                     # 0x80
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	8                       # 0x8
	.short	86                      # 0x56
	.short	55                      # 0x37
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	115                     # 0x73
	.short	65535                   # 0xffff
	.short	101                     # 0x65
	.short	70                      # 0x46
	.short	65535                   # 0xffff
	.short	39                      # 0x27
	.short	114                     # 0x72
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	100                     # 0x64
	.short	85                      # 0x55
	.short	7                       # 0x7
	.short	23                      # 0x17
	.short	65513                   # 0xffe9
	.short	65523                   # 0xfff3
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	113                     # 0x71
	.short	65535                   # 0xffff
	.short	112                     # 0x70
	.short	54                      # 0x36
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	99                      # 0x63
	.short	69                      # 0x45
	.short	65535                   # 0xffff
	.short	84                      # 0x54
	.short	38                      # 0x26
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	98                      # 0x62
	.short	22                      # 0x16
	.short	65535                   # 0xffff
	.short	97                      # 0x61
	.short	65535                   # 0xffff
	.short	6                       # 0x6
	.short	96                      # 0x60
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	83                      # 0x53
	.short	65535                   # 0xffff
	.short	53                      # 0x35
	.short	68                      # 0x44
	.short	65535                   # 0xffff
	.short	37                      # 0x25
	.short	82                      # 0x52
	.short	65535                   # 0xffff
	.short	81                      # 0x51
	.short	65535                   # 0xffff
	.short	21                      # 0x15
	.short	5                       # 0x5
	.short	65503                   # 0xffdf
	.short	65513                   # 0xffe9
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	52                      # 0x34
	.short	67                      # 0x43
	.short	65535                   # 0xffff
	.short	80                      # 0x50
	.short	36                      # 0x24
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	66                      # 0x42
	.short	51                      # 0x33
	.short	20                      # 0x14
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	65                      # 0x41
	.short	65535                   # 0xffff
	.short	4                       # 0x4
	.short	64                      # 0x40
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	49                      # 0x31
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	3                       # 0x3
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	18                      # 0x12
	.short	33                      # 0x21
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	17                      # 0x11
	.short	1                       # 0x1
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab16, 1022

	.type	tab24,@object           # @tab24
	.p2align	4
tab24:
	.short	65085                   # 0xfe3d
	.short	65419                   # 0xff8b
	.short	65493                   # 0xffd5
	.short	65511                   # 0xffe7
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	239                     # 0xef
	.short	254                     # 0xfe
	.short	65535                   # 0xffff
	.short	223                     # 0xdf
	.short	253                     # 0xfd
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	207                     # 0xcf
	.short	252                     # 0xfc
	.short	65535                   # 0xffff
	.short	191                     # 0xbf
	.short	251                     # 0xfb
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	250                     # 0xfa
	.short	65535                   # 0xffff
	.short	175                     # 0xaf
	.short	159                     # 0x9f
	.short	65535                   # 0xffff
	.short	249                     # 0xf9
	.short	248                     # 0xf8
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	143                     # 0x8f
	.short	127                     # 0x7f
	.short	247                     # 0xf7
	.short	65535                   # 0xffff
	.short	111                     # 0x6f
	.short	246                     # 0xf6
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	95                      # 0x5f
	.short	245                     # 0xf5
	.short	65535                   # 0xffff
	.short	79                      # 0x4f
	.short	244                     # 0xf4
	.short	65465                   # 0xffb9
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	63                      # 0x3f
	.short	243                     # 0xf3
	.short	65535                   # 0xffff
	.short	47                      # 0x2f
	.short	242                     # 0xf2
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	241                     # 0xf1
	.short	65535                   # 0xffff
	.short	31                      # 0x1f
	.short	240                     # 0xf0
	.short	65511                   # 0xffe7
	.short	65527                   # 0xfff7
	.short	65535                   # 0xffff
	.short	15                      # 0xf
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	238                     # 0xee
	.short	222                     # 0xde
	.short	65535                   # 0xffff
	.short	237                     # 0xed
	.short	206                     # 0xce
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	236                     # 0xec
	.short	221                     # 0xdd
	.short	65535                   # 0xffff
	.short	190                     # 0xbe
	.short	235                     # 0xeb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	205                     # 0xcd
	.short	220                     # 0xdc
	.short	65535                   # 0xffff
	.short	174                     # 0xae
	.short	234                     # 0xea
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	189                     # 0xbd
	.short	219                     # 0xdb
	.short	65535                   # 0xffff
	.short	204                     # 0xcc
	.short	158                     # 0x9e
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	233                     # 0xe9
	.short	173                     # 0xad
	.short	65535                   # 0xffff
	.short	218                     # 0xda
	.short	188                     # 0xbc
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	203                     # 0xcb
	.short	142                     # 0x8e
	.short	65535                   # 0xffff
	.short	232                     # 0xe8
	.short	157                     # 0x9d
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	217                     # 0xd9
	.short	126                     # 0x7e
	.short	65535                   # 0xffff
	.short	231                     # 0xe7
	.short	172                     # 0xac
	.short	255                     # 0xff
	.short	65301                   # 0xff15
	.short	65393                   # 0xff71
	.short	65459                   # 0xffb3
	.short	65491                   # 0xffd3
	.short	65511                   # 0xffe7
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	202                     # 0xca
	.short	187                     # 0xbb
	.short	65535                   # 0xffff
	.short	141                     # 0x8d
	.short	216                     # 0xd8
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	14                      # 0xe
	.short	224                     # 0xe0
	.short	13                      # 0xd
	.short	230                     # 0xe6
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	110                     # 0x6e
	.short	156                     # 0x9c
	.short	201                     # 0xc9
	.short	65535                   # 0xffff
	.short	94                      # 0x5e
	.short	186                     # 0xba
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	229                     # 0xe5
	.short	65535                   # 0xffff
	.short	171                     # 0xab
	.short	125                     # 0x7d
	.short	65535                   # 0xffff
	.short	215                     # 0xd7
	.short	228                     # 0xe4
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	140                     # 0x8c
	.short	200                     # 0xc8
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	78                      # 0x4e
	.short	46                      # 0x2e
	.short	62                      # 0x3e
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	109                     # 0x6d
	.short	214                     # 0xd6
	.short	65535                   # 0xffff
	.short	227                     # 0xe3
	.short	155                     # 0x9b
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	185                     # 0xb9
	.short	170                     # 0xaa
	.short	65535                   # 0xffff
	.short	226                     # 0xe2
	.short	30                      # 0x1e
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	225                     # 0xe1
	.short	93                      # 0x5d
	.short	65535                   # 0xffff
	.short	213                     # 0xd5
	.short	124                     # 0x7c
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	199                     # 0xc7
	.short	77                      # 0x4d
	.short	65535                   # 0xffff
	.short	139                     # 0x8b
	.short	184                     # 0xb8
	.short	65505                   # 0xffe1
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	212                     # 0xd4
	.short	154                     # 0x9a
	.short	65535                   # 0xffff
	.short	169                     # 0xa9
	.short	108                     # 0x6c
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	198                     # 0xc6
	.short	61                      # 0x3d
	.short	65535                   # 0xffff
	.short	211                     # 0xd3
	.short	45                      # 0x2d
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	210                     # 0xd2
	.short	29                      # 0x1d
	.short	65535                   # 0xffff
	.short	123                     # 0x7b
	.short	183                     # 0xb7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	209                     # 0xd1
	.short	92                      # 0x5c
	.short	65535                   # 0xffff
	.short	197                     # 0xc5
	.short	138                     # 0x8a
	.short	65519                   # 0xffef
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	168                     # 0xa8
	.short	153                     # 0x99
	.short	65535                   # 0xffff
	.short	76                      # 0x4c
	.short	196                     # 0xc4
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	107                     # 0x6b
	.short	182                     # 0xb6
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	208                     # 0xd0
	.short	12                      # 0xc
	.short	60                      # 0x3c
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	195                     # 0xc3
	.short	122                     # 0x7a
	.short	65535                   # 0xffff
	.short	167                     # 0xa7
	.short	44                      # 0x2c
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	194                     # 0xc2
	.short	91                      # 0x5b
	.short	65535                   # 0xffff
	.short	181                     # 0xb5
	.short	28                      # 0x1c
	.short	65479                   # 0xffc7
	.short	65501                   # 0xffdd
	.short	65517                   # 0xffed
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	137                     # 0x89
	.short	152                     # 0x98
	.short	65535                   # 0xffff
	.short	193                     # 0xc1
	.short	75                      # 0x4b
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	192                     # 0xc0
	.short	11                      # 0xb
	.short	59                      # 0x3b
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	176                     # 0xb0
	.short	10                      # 0xa
	.short	26                      # 0x1a
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	180                     # 0xb4
	.short	65535                   # 0xffff
	.short	106                     # 0x6a
	.short	166                     # 0xa6
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	121                     # 0x79
	.short	151                     # 0x97
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	160                     # 0xa0
	.short	9                       # 0x9
	.short	144                     # 0x90
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	179                     # 0xb3
	.short	136                     # 0x88
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	43                      # 0x2b
	.short	90                      # 0x5a
	.short	178                     # 0xb2
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	165                     # 0xa5
	.short	27                      # 0x1b
	.short	65535                   # 0xffff
	.short	177                     # 0xb1
	.short	105                     # 0x69
	.short	65535                   # 0xffff
	.short	150                     # 0x96
	.short	164                     # 0xa4
	.short	65519                   # 0xffef
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	74                      # 0x4a
	.short	120                     # 0x78
	.short	135                     # 0x87
	.short	65535                   # 0xffff
	.short	58                      # 0x3a
	.short	163                     # 0xa3
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	89                      # 0x59
	.short	149                     # 0x95
	.short	65535                   # 0xffff
	.short	42                      # 0x2a
	.short	162                     # 0xa2
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	161                     # 0xa1
	.short	104                     # 0x68
	.short	65535                   # 0xffff
	.short	134                     # 0x86
	.short	119                     # 0x77
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	73                      # 0x49
	.short	148                     # 0x94
	.short	65535                   # 0xffff
	.short	57                      # 0x39
	.short	147                     # 0x93
	.short	65473                   # 0xffc1
	.short	65505                   # 0xffe1
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	88                      # 0x58
	.short	133                     # 0x85
	.short	65535                   # 0xffff
	.short	41                      # 0x29
	.short	103                     # 0x67
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	118                     # 0x76
	.short	146                     # 0x92
	.short	65535                   # 0xffff
	.short	25                      # 0x19
	.short	145                     # 0x91
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	72                      # 0x48
	.short	132                     # 0x84
	.short	65535                   # 0xffff
	.short	87                      # 0x57
	.short	117                     # 0x75
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	56                      # 0x38
	.short	131                     # 0x83
	.short	65535                   # 0xffff
	.short	102                     # 0x66
	.short	40                      # 0x28
	.short	65519                   # 0xffef
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	130                     # 0x82
	.short	24                      # 0x18
	.short	65535                   # 0xffff
	.short	71                      # 0x47
	.short	116                     # 0x74
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	129                     # 0x81
	.short	65535                   # 0xffff
	.short	8                       # 0x8
	.short	128                     # 0x80
	.short	65535                   # 0xffff
	.short	86                      # 0x56
	.short	101                     # 0x65
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	23                      # 0x17
	.short	65535                   # 0xffff
	.short	7                       # 0x7
	.short	112                     # 0x70
	.short	115                     # 0x73
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	55                      # 0x37
	.short	39                      # 0x27
	.short	114                     # 0x72
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	70                      # 0x46
	.short	100                     # 0x64
	.short	65535                   # 0xffff
	.short	85                      # 0x55
	.short	113                     # 0x71
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	54                      # 0x36
	.short	99                      # 0x63
	.short	65535                   # 0xffff
	.short	69                      # 0x45
	.short	84                      # 0x54
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	38                      # 0x26
	.short	98                      # 0x62
	.short	65535                   # 0xffff
	.short	22                      # 0x16
	.short	97                      # 0x61
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	6                       # 0x6
	.short	96                      # 0x60
	.short	53                      # 0x35
	.short	65535                   # 0xffff
	.short	83                      # 0x53
	.short	68                      # 0x44
	.short	65485                   # 0xffcd
	.short	65499                   # 0xffdb
	.short	65513                   # 0xffe9
	.short	65521                   # 0xfff1
	.short	65527                   # 0xfff7
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	37                      # 0x25
	.short	82                      # 0x52
	.short	65535                   # 0xffff
	.short	21                      # 0x15
	.short	65535                   # 0xffff
	.short	5                       # 0x5
	.short	80                      # 0x50
	.short	65535                   # 0xffff
	.short	81                      # 0x51
	.short	65535                   # 0xffff
	.short	52                      # 0x34
	.short	67                      # 0x43
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	36                      # 0x24
	.short	66                      # 0x42
	.short	65535                   # 0xffff
	.short	51                      # 0x33
	.short	20                      # 0x14
	.short	65527                   # 0xfff7
	.short	65531                   # 0xfffb
	.short	65535                   # 0xffff
	.short	65                      # 0x41
	.short	65535                   # 0xffff
	.short	4                       # 0x4
	.short	64                      # 0x40
	.short	65535                   # 0xffff
	.short	35                      # 0x23
	.short	50                      # 0x32
	.short	65535                   # 0xffff
	.short	19                      # 0x13
	.short	49                      # 0x31
	.short	65529                   # 0xfff9
	.short	65531                   # 0xfffb
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	3                       # 0x3
	.short	48                      # 0x30
	.short	34                      # 0x22
	.short	18                      # 0x12
	.short	65535                   # 0xffff
	.short	33                      # 0x21
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	32                      # 0x20
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	17                      # 0x11
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.short	16                      # 0x10
	.short	0                       # 0x0
	.size	tab24, 1022

	.type	tab_c0,@object          # @tab_c0
	.p2align	4
tab_c0:
	.short	65507                   # 0xffe3
	.short	65515                   # 0xffeb
	.short	65523                   # 0xfff3
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	11                      # 0xb
	.short	15                      # 0xf
	.short	65535                   # 0xffff
	.short	13                      # 0xd
	.short	14                      # 0xe
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	7                       # 0x7
	.short	5                       # 0x5
	.short	9                       # 0x9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	6                       # 0x6
	.short	3                       # 0x3
	.short	65535                   # 0xffff
	.short	10                      # 0xa
	.short	12                      # 0xc
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	2                       # 0x2
	.short	1                       # 0x1
	.short	65535                   # 0xffff
	.short	4                       # 0x4
	.short	8                       # 0x8
	.short	0                       # 0x0
	.size	tab_c0, 62

	.type	tab_c1,@object          # @tab_c1
	.p2align	4
tab_c1:
	.short	65521                   # 0xfff1
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	15                      # 0xf
	.short	14                      # 0xe
	.short	65535                   # 0xffff
	.short	13                      # 0xd
	.short	12                      # 0xc
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	11                      # 0xb
	.short	10                      # 0xa
	.short	65535                   # 0xffff
	.short	9                       # 0x9
	.short	8                       # 0x8
	.short	65529                   # 0xfff9
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	7                       # 0x7
	.short	6                       # 0x6
	.short	65535                   # 0xffff
	.short	5                       # 0x5
	.short	4                       # 0x4
	.short	65533                   # 0xfffd
	.short	65535                   # 0xffff
	.short	3                       # 0x3
	.short	2                       # 0x2
	.short	65535                   # 0xffff
	.short	1                       # 0x1
	.short	0                       # 0x0
	.size	tab_c1, 62

	.type	III_get_scale_factors_1.slen.0,@object # @III_get_scale_factors_1.slen.0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
III_get_scale_factors_1.slen.0:
	.ascii	"\000\000\000\000\003\001\001\001\002\002\002\003\003\003\004\004"
	.size	III_get_scale_factors_1.slen.0, 16

	.type	III_get_scale_factors_1.slen.1,@object # @III_get_scale_factors_1.slen.1
	.p2align	4
III_get_scale_factors_1.slen.1:
	.ascii	"\000\001\002\003\000\001\002\003\001\002\003\001\002\003\002\003"
	.size	III_get_scale_factors_1.slen.1, 16


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
