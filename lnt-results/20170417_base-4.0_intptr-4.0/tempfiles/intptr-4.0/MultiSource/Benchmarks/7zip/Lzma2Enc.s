	.text
	.file	"Lzma2Enc.bc"
	.globl	Lzma2EncProps_Init
	.p2align	4, 0x90
	.type	Lzma2EncProps_Init,@function
Lzma2EncProps_Init:                     # @Lzma2EncProps_Init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	LzmaEncProps_Init
	movl	$-1, 60(%rbx)
	movl	$-1, 56(%rbx)
	movq	$0, 48(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	Lzma2EncProps_Init, .Lfunc_end0-Lzma2EncProps_Init
	.cfi_endproc

	.globl	Lzma2EncProps_Normalize
	.p2align	4, 0x90
	.type	Lzma2EncProps_Normalize,@function
Lzma2EncProps_Normalize:                # @Lzma2EncProps_Normalize
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
	subq	$48, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 64
.Lcfi4:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdi
	callq	LzmaEncProps_Normalize
	movl	44(%rsp), %ecx
	movl	44(%rbx), %r8d
	movl	56(%rbx), %eax
	movl	60(%rbx), %esi
	cmpl	$33, %eax
	movl	$32, %edi
	cmovll	%eax, %edi
	testl	%esi, %esi
	jle	.LBB1_1
# BB#3:
	testl	%edi, %edi
	jle	.LBB1_4
# BB#5:
	testl	%r8d, %r8d
	jg	.LBB1_2
# BB#6:
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%edi
	cmpl	%esi, %edi
	movl	$1, %r8d
	cmovbel	%eax, %r8d
	jmp	.LBB1_7
.LBB1_1:
	testl	%edi, %edi
	movl	$1, %eax
	cmovlel	%eax, %edi
.LBB1_2:
	imull	%edi, %ecx
	movl	%ecx, %esi
	jmp	.LBB1_7
.LBB1_4:
	movl	%esi, %eax
	cltd
	idivl	%ecx
	testl	%eax, %eax
	cmovel	%esi, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r8d
	cmpl	$33, %eax
	movl	$32, %edi
	cmovll	%eax, %edi
.LBB1_7:
	movl	%r8d, 44(%rbx)
	movl	%edi, 56(%rbx)
	movl	%esi, 60(%rbx)
	movq	%rbx, %rdi
	callq	LzmaEncProps_Normalize
	cmpq	$0, 48(%rbx)
	jne	.LBB1_9
# BB#8:
	movl	4(%rbx), %eax
	leaq	(,%rax,4), %rcx
	cmpq	$262144, %rax           # imm = 0x40000
	movl	$1048576, %edx          # imm = 0x100000
	cmovaeq	%rcx, %rdx
	cmpq	$268435456, %rdx        # imm = 0x10000000
	movl	$268435456, %ecx        # imm = 0x10000000
	cmovbq	%rdx, %rcx
	cmpq	%rax, %rcx
	cmovbq	%rax, %rcx
	movq	%rcx, 48(%rbx)
.LBB1_9:
	addq	$48, %rsp
	popq	%rbx
	retq
.Lfunc_end1:
	.size	Lzma2EncProps_Normalize, .Lfunc_end1-Lzma2EncProps_Normalize
	.cfi_endproc

	.globl	Lzma2Enc_Create
	.p2align	4, 0x90
	.type	Lzma2Enc_Create,@function
Lzma2Enc_Create:                        # @Lzma2Enc_Create
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	subq	$56, %rsp
.Lcfi9:
	.cfi_def_cfa_offset 96
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r12, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	$18456, %esi            # imm = 0x4818
	callq	*(%r15)
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_1
# BB#2:
	leaq	8(%rbx), %r12
	movq	%r12, %rdi
	callq	LzmaEncProps_Init
	movl	$-1, 68(%rbx)
	movl	$-1, 64(%rbx)
	movq	$0, 56(%rbx)
	movups	8(%rbx), %xmm0
	movups	24(%rbx), %xmm1
	movups	40(%rbx), %xmm2
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdi
	callq	LzmaEncProps_Normalize
	movl	44(%rsp), %ecx
	movl	52(%rbx), %r8d
	movl	64(%rbx), %eax
	movl	68(%rbx), %esi
	cmpl	$33, %eax
	movl	$32, %edi
	cmovll	%eax, %edi
	testl	%esi, %esi
	jle	.LBB2_3
# BB#5:
	testl	%edi, %edi
	jle	.LBB2_6
# BB#7:
	testl	%r8d, %r8d
	jg	.LBB2_4
# BB#8:
	xorl	%edx, %edx
	movl	%esi, %eax
	divl	%edi
	cmpl	%esi, %edi
	movl	$1, %r8d
	cmovbel	%eax, %r8d
	jmp	.LBB2_9
.LBB2_1:
	xorl	%ebx, %ebx
	jmp	.LBB2_12
.LBB2_3:
	testl	%edi, %edi
	movl	$1, %eax
	cmovlel	%eax, %edi
.LBB2_4:
	imull	%edi, %ecx
	movl	%ecx, %esi
	jmp	.LBB2_9
.LBB2_6:
	movl	%esi, %eax
	cltd
	idivl	%ecx
	testl	%eax, %eax
	cmovel	%esi, %eax
	movl	$1, %ecx
	cmovel	%ecx, %r8d
	cmpl	$33, %eax
	movl	$32, %edi
	cmovll	%eax, %edi
.LBB2_9:
	movl	%r8d, 52(%rbx)
	movl	%edi, 64(%rbx)
	movl	%esi, 68(%rbx)
	movq	%r12, %rdi
	callq	LzmaEncProps_Normalize
	cmpq	$0, 56(%rbx)
	jne	.LBB2_11
# BB#10:
	movl	12(%rbx), %eax
	leaq	(,%rax,4), %rcx
	cmpq	$262144, %rax           # imm = 0x40000
	movl	$1048576, %edx          # imm = 0x100000
	cmovaeq	%rcx, %rdx
	cmpq	$268435456, %rdx        # imm = 0x10000000
	movl	$268435456, %ecx        # imm = 0x10000000
	cmovbq	%rdx, %rcx
	cmpq	%rax, %rcx
	cmovbq	%rax, %rcx
	movq	%rcx, 56(%rbx)
.LBB2_11:                               # %Lzma2EncProps_Normalize.exit
	movq	$0, 72(%rbx)
	movq	%r15, 80(%rbx)
	movq	%r14, 88(%rbx)
	movq	$0, 96(%rbx)
	movq	$0, 128(%rbx)
	movq	$0, 160(%rbx)
	movq	$0, 192(%rbx)
	movq	$0, 224(%rbx)
	movq	$0, 256(%rbx)
	movq	$0, 288(%rbx)
	movq	$0, 320(%rbx)
	movq	$0, 352(%rbx)
	movq	$0, 384(%rbx)
	movq	$0, 416(%rbx)
	movq	$0, 448(%rbx)
	movq	$0, 480(%rbx)
	movq	$0, 512(%rbx)
	movq	$0, 544(%rbx)
	movq	$0, 576(%rbx)
	movq	$0, 608(%rbx)
	movq	$0, 640(%rbx)
	movq	$0, 672(%rbx)
	movq	$0, 704(%rbx)
	movq	$0, 736(%rbx)
	movq	$0, 768(%rbx)
	movq	$0, 800(%rbx)
	movq	$0, 832(%rbx)
	movq	$0, 864(%rbx)
	movq	$0, 896(%rbx)
	movq	$0, 928(%rbx)
	movq	$0, 960(%rbx)
	movq	$0, 992(%rbx)
	movq	$0, 1024(%rbx)
	movq	$0, 1056(%rbx)
	movq	$0, 1088(%rbx)
	movq	%rbx, %rdi
	addq	$1120, %rdi             # imm = 0x460
	callq	MtCoder_Construct
.LBB2_12:
	movq	%rbx, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	Lzma2Enc_Create, .Lfunc_end2-Lzma2Enc_Create
	.cfi_endproc

	.globl	Lzma2Enc_Destroy
	.p2align	4, 0x90
	.type	Lzma2Enc_Destroy,@function
Lzma2Enc_Destroy:                       # @Lzma2Enc_Destroy
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$96, %ebx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_3
# BB#2:                                 #   in Loop: Header=BB3_1 Depth=1
	movq	80(%r14), %rsi
	movq	88(%r14), %rdx
	callq	LzmaEnc_Destroy
	movq	$0, (%r14,%rbx)
.LBB3_3:                                #   in Loop: Header=BB3_1 Depth=1
	addq	$32, %rbx
	cmpq	$1120, %rbx             # imm = 0x460
	jne	.LBB3_1
# BB#4:
	leaq	1120(%r14), %rdi
	callq	MtCoder_Destruct
	movq	72(%r14), %rsi
	movq	80(%r14), %rdi
	callq	*8(%rdi)
	movq	80(%r14), %rdi
	movq	8(%rdi), %rax
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*%rax                   # TAILCALL
.Lfunc_end3:
	.size	Lzma2Enc_Destroy, .Lfunc_end3-Lzma2Enc_Destroy
	.cfi_endproc

	.globl	Lzma2Enc_SetProps
	.p2align	4, 0x90
	.type	Lzma2Enc_SetProps,@function
Lzma2Enc_SetProps:                      # @Lzma2Enc_SetProps
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
	subq	$96, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 128
.Lcfi23:
	.cfi_offset %rbx, -32
.Lcfi24:
	.cfi_offset %r14, -24
.Lcfi25:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movaps	%xmm2, 32(%rsp)
	movaps	%xmm1, 16(%rsp)
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdi
	callq	LzmaEncProps_Normalize
	movl	12(%rsp), %ecx
	addl	8(%rsp), %ecx
	movl	$5, %eax
	cmpl	$4, %ecx
	jg	.LBB4_10
# BB#1:
	leaq	8(%r15), %r14
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movups	48(%rbx), %xmm3
	movups	%xmm3, 56(%r15)
	movups	%xmm2, 40(%r15)
	movups	%xmm1, 24(%r15)
	movups	%xmm0, 8(%r15)
	movups	(%rbx), %xmm0
	movups	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movaps	%xmm2, 80(%rsp)
	movaps	%xmm1, 64(%rsp)
	movaps	%xmm0, 48(%rsp)
	leaq	48(%rsp), %rdi
	callq	LzmaEncProps_Normalize
	movl	92(%rsp), %ecx
	movl	52(%r15), %esi
	movl	64(%r15), %eax
	movl	68(%r15), %ebx
	cmpl	$33, %eax
	movl	$32, %edi
	cmovll	%eax, %edi
	testl	%ebx, %ebx
	jle	.LBB4_2
# BB#4:
	testl	%edi, %edi
	jle	.LBB4_5
# BB#6:
	testl	%esi, %esi
	jg	.LBB4_3
# BB#7:
	xorl	%edx, %edx
	movl	%ebx, %eax
	divl	%edi
	cmpl	%ebx, %edi
	movl	$1, %esi
	cmovbel	%eax, %esi
	jmp	.LBB4_8
.LBB4_2:
	testl	%edi, %edi
	movl	$1, %eax
	cmovlel	%eax, %edi
.LBB4_3:
	imull	%edi, %ecx
	movl	%ecx, %ebx
	jmp	.LBB4_8
.LBB4_5:
	movl	%ebx, %eax
	cltd
	idivl	%ecx
	testl	%eax, %eax
	cmovel	%ebx, %eax
	movl	$1, %ecx
	cmovel	%ecx, %esi
	cmpl	$33, %eax
	movl	$32, %edi
	cmovll	%eax, %edi
.LBB4_8:
	movl	%esi, 52(%r15)
	movl	%edi, 64(%r15)
	movl	%ebx, 68(%r15)
	movq	%r14, %rdi
	callq	LzmaEncProps_Normalize
	xorl	%eax, %eax
	cmpq	$0, 56(%r15)
	jne	.LBB4_10
# BB#9:
	movl	12(%r15), %ecx
	leaq	(,%rcx,4), %rdx
	cmpq	$262144, %rcx           # imm = 0x40000
	movl	$1048576, %esi          # imm = 0x100000
	cmovaeq	%rdx, %rsi
	cmpq	$268435456, %rsi        # imm = 0x10000000
	movl	$268435456, %edx        # imm = 0x10000000
	cmovbq	%rsi, %rdx
	cmpq	%rcx, %rdx
	cmovbq	%rcx, %rdx
	movq	%rdx, 56(%r15)
.LBB4_10:                               # %Lzma2EncProps_Normalize.exit
	addq	$96, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	Lzma2Enc_SetProps, .Lfunc_end4-Lzma2Enc_SetProps
	.cfi_endproc

	.globl	Lzma2Enc_WriteProperties
	.p2align	4, 0x90
	.type	Lzma2Enc_WriteProperties,@function
Lzma2Enc_WriteProperties:               # @Lzma2Enc_WriteProperties
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi26:
	.cfi_def_cfa_offset 16
	addq	$8, %rdi
	callq	LzmaEncProps_GetDictSize
	movl	%eax, %edx
	xorl	%ecx, %ecx
	movl	$1, %esi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movl	$4096, %edi             # imm = 0x1000
	shll	%cl, %edi
	cmpl	%edi, %edx
	jbe	.LBB5_4
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	movl	$6144, %edi             # imm = 0x1800
	shll	%cl, %edi
	cmpl	%edi, %edx
	jbe	.LBB5_3
# BB#5:                                 #   in Loop: Header=BB5_1 Depth=1
	addl	$2, %eax
	incl	%ecx
	leal	2(%rsi), %edi
	incl	%esi
	cmpl	$40, %esi
	movl	%edi, %esi
	jb	.LBB5_1
	jmp	.LBB5_4
.LBB5_3:
	movl	%esi, %eax
.LBB5_4:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	popq	%rcx
	retq
.Lfunc_end5:
	.size	Lzma2Enc_WriteProperties, .Lfunc_end5-Lzma2Enc_WriteProperties
	.cfi_endproc

	.globl	Lzma2Enc_Encode
	.p2align	4, 0x90
	.type	Lzma2Enc_Encode,@function
Lzma2Enc_Encode:                        # @Lzma2Enc_Encode
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi30:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -56
.Lcfi35:
	.cfi_offset %r12, -48
.Lcfi36:
	.cfi_offset %r13, -40
.Lcfi37:
	.cfi_offset %r14, -32
.Lcfi38:
	.cfi_offset %r15, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	64(%r13), %eax
	testl	%eax, %eax
	jle	.LBB6_7
# BB#1:                                 # %.lr.ph
	leaq	96(%r13), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	cmpq	$0, (%rbp)
	jne	.LBB6_5
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	80(%r13), %rdi
	callq	LzmaEnc_Create
	movq	%rax, (%rbp)
	testq	%rax, %rax
	je	.LBB6_19
# BB#4:                                 # %..critedge_crit_edge
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	64(%r13), %eax
.LBB6_5:                                # %.critedge
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	addq	$32, %rbp
	cmpq	%rcx, %rbx
	jl	.LBB6_2
# BB#6:                                 # %._crit_edge
	cmpl	$1, %eax
	jg	.LBB6_17
.LBB6_7:                                # %._crit_edge.thread
	cmpq	$0, 72(%r13)
	jne	.LBB6_9
# BB#8:
	movq	80(%r13), %rdi
	movl	$65552, %esi            # imm = 0x10010
	callq	*(%rdi)
	movq	%rax, 72(%r13)
	testq	%rax, %rax
	je	.LBB6_19
.LBB6_9:
	movq	$5, (%rsp)
	movq	96(%r13), %rdi
	leaq	8(%r13), %rsi
	callq	LzmaEnc_SetProps
	testl	%eax, %eax
	jne	.LBB6_20
# BB#10:
	leaq	96(%r13), %rbx
	movq	(%rbx), %rdi
	leaq	19(%rsp), %rsi
	movq	%rsp, %rdx
	callq	LzmaEnc_WriteProperties
	testl	%eax, %eax
	jne	.LBB6_20
# BB#11:                                # %Lzma2EncInt_Init.exit.i
	movq	$0, 104(%r13)
	movb	19(%rsp), %al
	movb	%al, 112(%r13)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 116(%r13)
	movq	96(%r13), %rdi
	movq	80(%r13), %rcx
	movq	88(%r13), %r8
	movl	$2097152, %edx          # imm = 0x200000
	movq	%r12, %rsi
	callq	LzmaEnc_PrepareForLzma2
	testl	%eax, %eax
	jne	.LBB6_20
# BB#12:                                # %.preheader.i
	testq	%r15, %r15
	je	.LBB6_21
# BB#13:                                # %.preheader.split.i.preheader
	xorl	%r12d, %r12d
.LBB6_14:                               # %.preheader.split.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$65552, (%rsp)          # imm = 0x10010
	movq	72(%r13), %rsi
	movq	%rbx, %rdi
	movq	%rsp, %rdx
	movq	%r14, %rcx
	callq	Lzma2EncInt_EncodeSubblock
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB6_27
# BB#15:                                #   in Loop: Header=BB6_14 Depth=1
	movq	(%rsp), %rbp
	addq	%rbp, %r12
	movq	104(%r13), %rsi
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	*(%r15)
	testl	%eax, %eax
	jne	.LBB6_26
# BB#16:                                # %Progress.exit.i
                                        #   in Loop: Header=BB6_14 Depth=1
	testq	%rbp, %rbp
	jne	.LBB6_14
	jmp	.LBB6_24
.LBB6_19:
	movl	$2, %eax
	jmp	.LBB6_20
.LBB6_17:
	movq	$MtCallbackImp_Code, (%rsp)
	movq	%r13, 8(%rsp)
	movq	%r15, 1160(%r13)
	movq	%r12, 1144(%r13)
	movq	%r14, 1152(%r13)
	movq	80(%r13), %rcx
	movq	%rcx, 1168(%r13)
	movq	%rsp, %rcx
	movq	%rcx, 1176(%r13)
	movq	56(%r13), %rcx
	movq	%rcx, 1120(%r13)
	movq	%rcx, %rdx
	shrq	$10, %rdx
	leaq	16(%rcx,%rdx), %rcx
	movq	%rcx, 1128(%r13)
	movl	%eax, 1136(%r13)
	leaq	1120(%r13), %rdi
	callq	MtCoder_Code
.LBB6_20:                               # %Lzma2Enc_EncodeMt1.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_21:                               # %.preheader.split.us.i.preheader
	movq	%rsp, %r15
.LBB6_22:                               # %.preheader.split.us.i
                                        # =>This Inner Loop Header: Depth=1
	movq	$65552, (%rsp)          # imm = 0x10010
	movq	72(%r13), %rsi
	movq	%rbx, %rdi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	Lzma2EncInt_EncodeSubblock
	movl	%eax, %ebp
	testl	%ebp, %ebp
	jne	.LBB6_27
# BB#23:                                # %Progress.exit.us.i
                                        #   in Loop: Header=BB6_22 Depth=1
	cmpq	$0, (%rsp)
	jne	.LBB6_22
.LBB6_24:                               # %.us-lcssa58.us.i
	movq	(%rbx), %rdi
	callq	LzmaEnc_Finish
	movb	$0, (%rsp)
	movq	%rsp, %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	*(%r14)
	movq	%rax, %rcx
	movl	$9, %eax
	xorl	%ebp, %ebp
	cmpq	$1, %rcx
	jne	.LBB6_20
# BB#25:
	movl	%ebp, %eax
	jmp	.LBB6_20
.LBB6_26:
	movl	$10, %ebp
.LBB6_27:                               # %.us-lcssa.us.i
	movq	(%rbx), %rdi
	callq	LzmaEnc_Finish
	movl	%ebp, %eax
	jmp	.LBB6_20
.Lfunc_end6:
	.size	Lzma2Enc_Encode, .Lfunc_end6-Lzma2Enc_Encode
	.cfi_endproc

	.p2align	4, 0x90
	.type	MtCallbackImp_Code,@function
MtCallbackImp_Code:                     # @MtCallbackImp_Code
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi43:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi44:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 112
.Lcfi47:
	.cfi_offset %rbx, -56
.Lcfi48:
	.cfi_offset %r12, -48
.Lcfi49:
	.cfi_offset %r13, -40
.Lcfi50:
	.cfi_offset %r14, -32
.Lcfi51:
	.cfi_offset %r15, -24
.Lcfi52:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movq	%rcx, %rbx
	movq	8(%rdi), %rbp
	movq	(%rbx), %rcx
	movq	$0, (%rbx)
	testq	%r12, %r12
	je	.LBB7_14
# BB#1:
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	%esi, 36(%rsp)          # 4-byte Spill
	movl	%esi, %r15d
	shlq	$5, %r15
	leaq	96(%rbp,%r15), %r13
	movq	$5, 8(%rsp)
	movq	(%r13), %rdi
	leaq	8(%rbp), %rsi
	callq	LzmaEnc_SetProps
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB7_17
# BB#2:
	movq	(%r13), %rdi
	leaq	43(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	LzmaEnc_WriteProperties
	movl	%eax, %r14d
	testl	%r14d, %r14d
	jne	.LBB7_17
# BB#3:
	movq	$0, 104(%rbp,%r15)
	movb	43(%rsp), %al
	movb	%al, 112(%rbp,%r15)
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 116(%rbp,%r15)
	movq	(%r13), %rdi
	movq	80(%rbp), %r8
	movq	88(%rbp), %r9
	movl	$2097152, %ecx          # imm = 0x200000
	movq	48(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	callq	LzmaEnc_MemPrepare
	movl	%eax, %r14d
	testl	%r14d, %r14d
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	jne	.LBB7_17
# BB#4:                                 # %.preheader
	leaq	104(%rbp,%r15), %r15
	addq	$1232, %rbp             # imm = 0x4D0
	xorl	%r14d, %r14d
	movq	%r13, %rdx
	.p2align	4, 0x90
.LBB7_5:                                # =>This Inner Loop Header: Depth=1
	cmpq	%r12, (%r15)
	jae	.LBB7_13
# BB#6:                                 #   in Loop: Header=BB7_5 Depth=1
	movq	(%rbx), %rsi
	subq	%rsi, %rax
	movq	%rax, 8(%rsp)
	addq	%rcx, %rsi
	xorl	%ecx, %ecx
	movq	%rdx, %r13
	movq	%rdx, %rdi
	leaq	8(%rsp), %rdx
	callq	Lzma2EncInt_EncodeSubblock
	testl	%eax, %eax
	jne	.LBB7_7
# BB#8:                                 #   in Loop: Header=BB7_5 Depth=1
	movq	8(%rsp), %rax
	movq	(%rbx), %rcx
	addq	%rax, %rcx
	movq	%rcx, (%rbx)
	testq	%rax, %rax
	je	.LBB7_9
# BB#11:                                #   in Loop: Header=BB7_5 Depth=1
	movq	(%r15), %rdx
	movq	%rbp, %rdi
	movl	36(%rsp), %esi          # 4-byte Reload
	callq	MtProgress_Set
	testl	%eax, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	%r13, %rdx
	je	.LBB7_5
# BB#12:
	movl	$10, %r14d
.LBB7_13:
	movq	(%rdx), %rdi
	callq	LzmaEnc_Finish
	testl	%r14d, %r14d
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	16(%rsp), %rcx          # 8-byte Reload
	jne	.LBB7_17
.LBB7_14:
	movl	112(%rsp), %eax
	xorl	%r14d, %r14d
	testl	%eax, %eax
	je	.LBB7_17
# BB#15:
	movq	(%rbx), %rax
	movl	$7, %r14d
	cmpq	%rcx, %rax
	je	.LBB7_17
# BB#16:
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbx)
	movb	$0, (%rdx,%rax)
	xorl	%r14d, %r14d
.LBB7_17:
	movl	%r14d, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_7:
	movl	%eax, %r14d
	jmp	.LBB7_10
.LBB7_9:
	movl	$11, %r14d
.LBB7_10:                               # %.thread73
	movq	(%r13), %rdi
	callq	LzmaEnc_Finish
	jmp	.LBB7_17
.Lfunc_end7:
	.size	MtCallbackImp_Code, .Lfunc_end7-MtCallbackImp_Code
	.cfi_endproc

	.p2align	4, 0x90
	.type	Lzma2EncInt_EncodeSubblock,@function
Lzma2EncInt_EncodeSubblock:             # @Lzma2EncInt_EncodeSubblock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi56:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi57:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi59:
	.cfi_def_cfa_offset 112
.Lcfi60:
	.cfi_offset %rbx, -56
.Lcfi61:
	.cfi_offset %r12, -48
.Lcfi62:
	.cfi_offset %r13, -40
.Lcfi63:
	.cfi_offset %r14, -32
.Lcfi64:
	.cfi_offset %r15, -24
.Lcfi65:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r13
	movq	(%r14), %rcx
	movq	%rcx, 24(%rsp)
	movl	$2097152, 12(%rsp)      # imm = 0x200000
	cmpl	$1, 24(%r13)
	movq	$0, (%r14)
	movl	$5, %ebp
	sbbq	$-1, %rbp
	movq	%rcx, %rax
	subq	%rbp, %rax
	jae	.LBB8_2
# BB#1:
	movl	$7, %eax
	jmp	.LBB8_31
.LBB8_2:
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)
	movq	(%r13), %rdi
	callq	LzmaEnc_SaveState
	movq	(%r13), %rdi
	movl	20(%r13), %esi
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	addq	%rbx, %rbp
	leaq	24(%rsp), %rcx
	leaq	12(%rsp), %r9
	movl	$65536, %r8d            # imm = 0x10000
	movq	%rbp, %rdx
	callq	LzmaEnc_CodeOneMemBlock
	movl	12(%rsp), %r15d
	testq	%r15, %r15
	je	.LBB8_31
# BB#3:
	cmpl	$7, %eax
	je	.LBB8_7
# BB#4:
	testl	%eax, %eax
	jne	.LBB8_31
# BB#5:
	movq	24(%rsp), %rbp
	cmpq	$65536, %rbp            # imm = 0x10000
	ja	.LBB8_7
# BB#6:
	leaq	2(%rbp), %rax
	cmpq	%r15, %rax
	jae	.LBB8_7
# BB#19:
	leal	-1(%r15), %ecx
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	addq	%rbp, %rax
	cmpq	$0, 8(%r13)
	je	.LBB8_20
# BB#21:
	cmpl	$0, 20(%r13)
	je	.LBB8_22
# BB#23:
	cmpl	$0, 24(%r13)
	movl	$192, %esi
	movl	$160, %edx
	cmovnel	%esi, %edx
	jmp	.LBB8_24
.LBB8_7:                                # %.lr.ph
	movq	%r14, 32(%rsp)          # 8-byte Spill
	testq	%r12, %r12
	je	.LBB8_8
# BB#12:                                # %.lr.ph.split.preheader
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	3(%rax), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB8_13:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$65536, %r15d           # imm = 0x10000
	movl	$65536, %eax            # imm = 0x10000
	cmovael	%eax, %r15d
	leal	3(%r15), %eax
	cmpq	%rax, 40(%rsp)          # 8-byte Folded Reload
	jb	.LBB8_14
# BB#15:                                #   in Loop: Header=BB8_13 Depth=1
	cmpq	$0, 8(%r13)
	setne	%al
	incb	%al
	movq	16(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx)
	leal	-1(%r15), %eax
	movb	%ah, 1(%rcx)  # NOREX
	movb	%al, 2(%rcx)
	movq	%rcx, %r14
	movq	(%r13), %rdi
	callq	LzmaEnc_GetCurBuf
	movl	12(%rsp), %ebp
	subq	%rbp, %rax
	movl	%r15d, %ebx
	movq	48(%rsp), %rdi          # 8-byte Reload
	movq	%rax, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	subl	%r15d, %ebp
	movl	%ebp, 12(%rsp)
	addq	%rbx, 8(%r13)
	addq	$3, %rbx
	movq	32(%rsp), %rax          # 8-byte Reload
	addq	%rbx, (%rax)
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	*(%r12)
	cmpq	%rbx, %rax
	jne	.LBB8_16
# BB#17:                                # %.critedge.backedge
                                        #   in Loop: Header=BB8_13 Depth=1
	movl	12(%rsp), %r15d
	testl	%r15d, %r15d
	jne	.LBB8_13
	jmp	.LBB8_18
.LBB8_8:                                # %.lr.ph.split.us.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_9:                                # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$65536, %r15d           # imm = 0x10000
	movl	$65536, %eax            # imm = 0x10000
	cmovael	%eax, %r15d
	movq	40(%rsp), %rax          # 8-byte Reload
	subq	%rbp, %rax
	leal	3(%r15), %ecx
	cmpq	%rcx, %rax
	jb	.LBB8_10
# BB#11:                                # %.critedge.backedge.us
                                        #   in Loop: Header=BB8_9 Depth=1
	cmpq	$0, 8(%r13)
	setne	%al
	incb	%al
	movq	16(%rsp), %rcx          # 8-byte Reload
	movb	%al, (%rcx,%rbp)
	leal	-1(%r15), %eax
	movb	%ah, 1(%rcx,%rbp)  # NOREX
	movb	%al, 2(%rcx,%rbp)
	leaq	3(%rcx,%rbp), %r14
	movq	(%r13), %rdi
	callq	LzmaEnc_GetCurBuf
	movl	12(%rsp), %r12d
	subq	%r12, %rax
	movl	%r15d, %ebx
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	callq	memcpy
	subl	%r15d, %r12d
	movl	%r12d, 12(%rsp)
	leaq	3(%rbx,%rbp), %rbp
	addq	%rbx, 8(%r13)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rbp, (%rax)
	testl	%r12d, %r12d
	movl	%r12d, %r15d
	jne	.LBB8_9
.LBB8_18:                               # %.critedge._crit_edge
	movq	(%r13), %rdi
	callq	LzmaEnc_RestoreState
	jmp	.LBB8_30
.LBB8_14:
	movl	$7, %eax
	jmp	.LBB8_31
.LBB8_16:
	movl	$9, %eax
	jmp	.LBB8_31
.LBB8_10:
	movl	$7, %eax
	jmp	.LBB8_31
.LBB8_20:
	movl	$224, %edx
	jmp	.LBB8_24
.LBB8_22:
	movl	$128, %edx
.LBB8_24:
	movl	%ecx, %esi
	shrl	$16, %esi
	andl	$31, %esi
	orl	%edx, %esi
	movq	16(%rsp), %rdx          # 8-byte Reload
	movb	%sil, (%rdx)
	movb	%ch, 1(%rdx)  # NOREX
	movb	%cl, 2(%rdx)
	movb	%ah, 3(%rdx)  # NOREX
	movb	%al, 4(%rdx)
	movq	%rdx, %rax
	cmpl	$0, 24(%r13)
	je	.LBB8_25
# BB#26:
	movq	%rax, %rcx
	movb	16(%r13), %al
	movb	%al, 5(%rcx)
	movl	$6, %eax
	jmp	.LBB8_27
.LBB8_25:
	movl	$5, %eax
.LBB8_27:
	movq	$0, 20(%r13)
	addq	%rax, %rbp
	addq	%r15, 8(%r13)
	testq	%r12, %r12
	je	.LBB8_29
# BB#28:
	movq	%r12, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rbp, %rdx
	callq	*(%r12)
	movq	%rax, %rcx
	movl	$9, %eax
	cmpq	%rbp, %rcx
	jne	.LBB8_31
.LBB8_29:
	movq	%rbp, (%r14)
.LBB8_30:                               # %.thread
	xorl	%eax, %eax
.LBB8_31:                               # %.thread
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	Lzma2EncInt_EncodeSubblock, .Lfunc_end8-Lzma2EncInt_EncodeSubblock
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
