	.text
	.file	"MyVector.bc"
	.globl	_ZN17CBaseRecordVectorD2Ev
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVectorD2Ev,@function
_ZN17CBaseRecordVectorD2Ev:             # @_ZN17CBaseRecordVectorD2Ev
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV17CBaseRecordVector+16, (%rbx)
	movq	16(%rbx), %rdi
	cmpl	$0, 12(%rbx)
	jle	.LBB0_2
# BB#1:
	movl	$0, 12(%rbx)
.LBB0_2:                                # %_ZN17CBaseRecordVector6DeleteEii.exit
	testq	%rdi, %rdi
	je	.LBB0_4
# BB#3:
	callq	_ZdaPv
.LBB0_4:                                # %_ZN17CBaseRecordVector12ClearAndFreeEv.exit
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN17CBaseRecordVectorD2Ev, .Lfunc_end0-_ZN17CBaseRecordVectorD2Ev
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector12ClearAndFreeEv
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector12ClearAndFreeEv,@function
_ZN17CBaseRecordVector12ClearAndFreeEv: # @_ZN17CBaseRecordVector12ClearAndFreeEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movl	12(%rbx), %edx
	xorl	%esi, %esi
	callq	*16(%rax)
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	_ZdaPv
.LBB1_2:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 8(%rbx)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	_ZN17CBaseRecordVector12ClearAndFreeEv, .Lfunc_end1-_ZN17CBaseRecordVector12ClearAndFreeEv
	.cfi_endproc

	.globl	_ZN17CBaseRecordVectorD0Ev
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVectorD0Ev,@function
_ZN17CBaseRecordVectorD0Ev:             # @_ZN17CBaseRecordVectorD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 16
.Lcfi5:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV17CBaseRecordVector+16, (%rbx)
	movq	16(%rbx), %rdi
	cmpl	$0, 12(%rbx)
	jle	.LBB2_2
# BB#1:
	movl	$0, 12(%rbx)
.LBB2_2:                                # %_ZN17CBaseRecordVector6DeleteEii.exit.i
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	callq	_ZdaPv
.LBB2_4:
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN17CBaseRecordVectorD0Ev, .Lfunc_end2-_ZN17CBaseRecordVectorD0Ev
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector5ClearEv
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector5ClearEv,@function
_ZN17CBaseRecordVector5ClearEv:         # @_ZN17CBaseRecordVector5ClearEv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movl	12(%rdi), %edx
	xorl	%esi, %esi
	jmpq	*%rax                   # TAILCALL
.Lfunc_end3:
	.size	_ZN17CBaseRecordVector5ClearEv, .Lfunc_end3-_ZN17CBaseRecordVector5ClearEv
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector10DeleteFromEi
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector10DeleteFromEi,@function
_ZN17CBaseRecordVector10DeleteFromEi:   # @_ZN17CBaseRecordVector10DeleteFromEi
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movl	12(%rdi), %edx
	subl	%esi, %edx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end4:
	.size	_ZN17CBaseRecordVector10DeleteFromEi, .Lfunc_end4-_ZN17CBaseRecordVector10DeleteFromEi
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector10DeleteBackEv
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector10DeleteBackEv,@function
_ZN17CBaseRecordVector10DeleteBackEv:   # @_ZN17CBaseRecordVector10DeleteBackEv
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movl	12(%rdi), %esi
	decl	%esi
	movl	$1, %edx
	jmpq	*%rax                   # TAILCALL
.Lfunc_end5:
	.size	_ZN17CBaseRecordVector10DeleteBackEv, .Lfunc_end5-_ZN17CBaseRecordVector10DeleteBackEv
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector18ReserveOnePositionEv
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector18ReserveOnePositionEv,@function
_ZN17CBaseRecordVector18ReserveOnePositionEv: # @_ZN17CBaseRecordVector18ReserveOnePositionEv
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	cmpl	8(%rdi), %eax
	jne	.LBB6_1
# BB#2:
	movl	%eax, %ecx
	shrl	$2, %ecx
	cmpl	$7, %eax
	movl	$8, %edx
	movl	$1, %esi
	cmovgl	%edx, %esi
	cmpl	$63, %eax
	cmovgl	%ecx, %esi
	addl	%eax, %esi
	jmp	_ZN17CBaseRecordVector7ReserveEi # TAILCALL
.LBB6_1:
	retq
.Lfunc_end6:
	.size	_ZN17CBaseRecordVector18ReserveOnePositionEv, .Lfunc_end6-_ZN17CBaseRecordVector18ReserveOnePositionEv
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector7ReserveEi
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector7ReserveEi,@function
_ZN17CBaseRecordVector7ReserveEi:       # @_ZN17CBaseRecordVector7ReserveEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 48
.Lcfi11:
	.cfi_offset %rbx, -48
.Lcfi12:
	.cfi_offset %r12, -40
.Lcfi13:
	.cfi_offset %r14, -32
.Lcfi14:
	.cfi_offset %r15, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	cmpl	%ebp, 8(%rbx)
	je	.LBB7_12
# BB#1:
	testl	%ebp, %ebp
	js	.LBB7_2
# BB#4:
	movl	%ebp, %esi
	movq	24(%rbx), %r12
	movq	%r12, %rcx
	imulq	%rsi, %rcx
	xorl	%edx, %edx
	movq	%rcx, %rax
	divq	%r12
	cmpq	%rsi, %rax
	jne	.LBB7_5
# BB#6:
	testq	%rcx, %rcx
	je	.LBB7_7
# BB#8:
	movq	%rcx, %rdi
	callq	_Znam
	movq	%rax, %r14
	movl	12(%rbx), %eax
	cmpl	%ebp, %eax
	cmovgl	%ebp, %eax
	movq	16(%rbx), %r15
	movslq	%eax, %rdx
	imulq	%r12, %rdx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	memcpy
	testq	%r15, %r15
	jne	.LBB7_10
	jmp	.LBB7_11
.LBB7_7:                                # %._crit_edge
	movq	16(%rbx), %r15
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB7_11
.LBB7_10:
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB7_11:
	movq	%r14, 16(%rbx)
	movl	%ebp, 8(%rbx)
.LBB7_12:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_2:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1052353, (%rax)        # imm = 0x100EC1
	jmp	.LBB7_3
.LBB7_5:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1052354, (%rax)        # imm = 0x100EC2
.LBB7_3:
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Lfunc_end7:
	.size	_ZN17CBaseRecordVector7ReserveEi, .Lfunc_end7-_ZN17CBaseRecordVector7ReserveEi
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector11ReserveDownEv
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector11ReserveDownEv,@function
_ZN17CBaseRecordVector11ReserveDownEv:  # @_ZN17CBaseRecordVector11ReserveDownEv
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %esi
	jmp	_ZN17CBaseRecordVector7ReserveEi # TAILCALL
.Lfunc_end8:
	.size	_ZN17CBaseRecordVector11ReserveDownEv, .Lfunc_end8-_ZN17CBaseRecordVector11ReserveDownEv
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector9MoveItemsEii
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector9MoveItemsEii,@function
_ZN17CBaseRecordVector9MoveItemsEii:    # @_ZN17CBaseRecordVector9MoveItemsEii
	.cfi_startproc
# BB#0:
	movslq	%esi, %rax
	movq	16(%rdi), %r8
	movq	24(%rdi), %rsi
	imulq	%rsi, %rax
	addq	%r8, %rax
	movslq	%edx, %rcx
	movslq	12(%rdi), %rdx
	subq	%rcx, %rdx
	imulq	%rsi, %rdx
	imulq	%rcx, %rsi
	addq	%r8, %rsi
	movq	%rax, %rdi
	jmp	memmove                 # TAILCALL
.Lfunc_end9:
	.size	_ZN17CBaseRecordVector9MoveItemsEii, .Lfunc_end9-_ZN17CBaseRecordVector9MoveItemsEii
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector13InsertOneItemEi
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector13InsertOneItemEi,@function
_ZN17CBaseRecordVector13InsertOneItemEi: # @_ZN17CBaseRecordVector13InsertOneItemEi
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -24
.Lcfi20:
	.cfi_offset %r14, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movl	12(%rbx), %eax
	cmpl	8(%rbx), %eax
	jne	.LBB10_2
# BB#1:
	movl	%eax, %ecx
	shrl	$2, %ecx
	cmpl	$7, %eax
	movl	$8, %edx
	movl	$1, %esi
	cmovgl	%edx, %esi
	cmpl	$63, %eax
	cmovgl	%ecx, %esi
	addl	%eax, %esi
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	movl	12(%rbx), %eax
.LBB10_2:                               # %_ZN17CBaseRecordVector18ReserveOnePositionEv.exit
	leal	1(%r14), %ecx
	movslq	%ecx, %rdi
	movq	16(%rbx), %r8
	movq	24(%rbx), %rsi
	imulq	%rsi, %rdi
	addq	%r8, %rdi
	movslq	%r14d, %rcx
	subl	%ecx, %eax
	movslq	%eax, %rdx
	imulq	%rsi, %rdx
	imulq	%rcx, %rsi
	addq	%r8, %rsi
	callq	memmove
	incl	12(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZN17CBaseRecordVector13InsertOneItemEi, .Lfunc_end10-_ZN17CBaseRecordVector13InsertOneItemEi
	.cfi_endproc

	.globl	_ZN17CBaseRecordVector6DeleteEii
	.p2align	4, 0x90
	.type	_ZN17CBaseRecordVector6DeleteEii,@function
_ZN17CBaseRecordVector6DeleteEii:       # @_ZN17CBaseRecordVector6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -24
.Lcfi25:
	.cfi_offset %r14, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r14
	leal	(%rdx,%rsi), %ecx
	movl	12(%r14), %eax
	movl	%eax, %ebx
	subl	%esi, %ebx
	cmpl	%eax, %ecx
	cmovlel	%edx, %ebx
	testl	%ebx, %ebx
	jle	.LBB11_2
# BB#1:
	leal	(%rbx,%rsi), %edx
	movslq	%esi, %rdi
	movq	16(%r14), %r8
	movq	24(%r14), %rsi
	imulq	%rsi, %rdi
	addq	%r8, %rdi
	movslq	%edx, %rcx
	subl	%ecx, %eax
	movslq	%eax, %rdx
	imulq	%rsi, %rdx
	imulq	%rcx, %rsi
	addq	%r8, %rsi
	callq	memmove
	subl	%ebx, 12(%r14)
.LBB11_2:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN17CBaseRecordVector6DeleteEii, .Lfunc_end11-_ZN17CBaseRecordVector6DeleteEii
	.cfi_endproc

	.type	_ZTV17CBaseRecordVector,@object # @_ZTV17CBaseRecordVector
	.section	.rodata,"a",@progbits
	.globl	_ZTV17CBaseRecordVector
	.p2align	3
_ZTV17CBaseRecordVector:
	.quad	0
	.quad	_ZTI17CBaseRecordVector
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN17CBaseRecordVectorD0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV17CBaseRecordVector, 40

	.type	_ZTS17CBaseRecordVector,@object # @_ZTS17CBaseRecordVector
	.globl	_ZTS17CBaseRecordVector
	.p2align	4
_ZTS17CBaseRecordVector:
	.asciz	"17CBaseRecordVector"
	.size	_ZTS17CBaseRecordVector, 20

	.type	_ZTI17CBaseRecordVector,@object # @_ZTI17CBaseRecordVector
	.globl	_ZTI17CBaseRecordVector
	.p2align	3
_ZTI17CBaseRecordVector:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS17CBaseRecordVector
	.size	_ZTI17CBaseRecordVector, 16


	.globl	_ZN17CBaseRecordVectorD1Ev
	.type	_ZN17CBaseRecordVectorD1Ev,@function
_ZN17CBaseRecordVectorD1Ev = _ZN17CBaseRecordVectorD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
