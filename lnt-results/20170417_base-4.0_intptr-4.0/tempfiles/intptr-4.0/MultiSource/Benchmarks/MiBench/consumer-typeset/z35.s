	.text
	.file	"z35.bc"
	.globl	TimeString
	.p2align	4, 0x90
	.type	TimeString,@function
TimeString:                             # @TimeString
	.cfi_startproc
# BB#0:
	movl	$time_string, %eax
	retq
.Lfunc_end0:
	.size	TimeString, .Lfunc_end0-TimeString
	.cfi_endproc

	.globl	InitTime
	.p2align	4, 0x90
	.type	InitTime,@function
InitTime:                               # @InitTime
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 160
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	no_fpos(%rip), %rbx
	movq	StartSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str, %edi
	movl	$143, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi15:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi17:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %rbx
	movq	%rbx, MomentSym(%rip)
	movq	no_fpos(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbp, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.2, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbp, %rdx
	pushq	%rax
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi22:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r14
	orb	$1, 43(%r14)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.3, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi24:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi25:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi27:
	.cfi_adjust_cfa_offset -32
	movq	%rax, %r12
	orb	$1, 43(%r12)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.4, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 24(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.5, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 32(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi38:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.6, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 40(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.7, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi47:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 48(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.8, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi52:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 56(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.9, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi54:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi55:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi56:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi57:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 64(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.10, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi59:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi60:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi61:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi62:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 72(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.11, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 80(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	movq	no_fpos(%rip), %rbx
	movq	MomentSym(%rip), %rbp
	movl	$11, %edi
	movl	$.L.str.1, %esi
	movq	%rbx, %rdx
	callq	MakeWord
	subq	$8, %rsp
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	movl	$.L.str.12, %edi
	movl	$145, %esi
	movl	$100, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%rbx, %rdx
	pushq	%rax
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	callq	InsertSym
	addq	$32, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 88(%rsp)          # 8-byte Spill
	orb	$1, 43(%rax)
	leaq	96(%rsp), %rdi
	callq	time
	cmpq	$-1, %rax
	jne	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$35, %edi
	movl	$1, %esi
	movl	$.L.str.13, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	leaq	96(%rsp), %rdi
	callq	localtime
	movq	%rax, %r13
	movq	%r13, %rdi
	callq	asctime
	movl	$time_string, %edi
	movq	%rax, %rsi
	callq	strcpy
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_3
# BB#4:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_5
.LBB1_3:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_5:
	movb	$2, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, current_moment(%rip)
	movq	MomentSym(%rip), %rcx
	movq	%rcx, 80(%rax)
	movl	$7827310, (%rsp)        # imm = 0x776F6E
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB1_6
# BB#7:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_8
.LBB1_6:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB1_8:
	movb	$10, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movq	%r14, 80(%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_9
# BB#10:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_11
.LBB1_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_11:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_14
# BB#12:
	testq	%rcx, %rcx
	je	.LBB1_14
# BB#13:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_14:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_17
# BB#15:
	testq	%rax, %rax
	je	.LBB1_17
# BB#16:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_17:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_18
# BB#19:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_20
.LBB1_18:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_20:
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB1_23
# BB#21:
	testq	%rax, %rax
	je	.LBB1_23
# BB#22:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_23:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_26
# BB#24:
	testq	%rax, %rax
	je	.LBB1_26
# BB#25:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_26:
	movl	(%r13), %edx
	movq	%rsp, %rdi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_27
# BB#28:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_29
.LBB1_27:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_29:
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%r12, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_30
# BB#31:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_32
.LBB1_30:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_32:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_35
# BB#33:
	testq	%rcx, %rcx
	je	.LBB1_35
# BB#34:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_35:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_38
# BB#36:
	testq	%rax, %rax
	je	.LBB1_38
# BB#37:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_38:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_39
# BB#40:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_41
.LBB1_39:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_41:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_44
# BB#42:
	testq	%rax, %rax
	je	.LBB1_44
# BB#43:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_44:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_47
# BB#45:
	testq	%rax, %rax
	je	.LBB1_47
# BB#46:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_47:
	movl	4(%r13), %edx
	movq	%rsp, %rdi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_48
# BB#49:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_50
.LBB1_48:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_50:
	movq	24(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_51
# BB#52:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_53
.LBB1_51:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_53:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_56
# BB#54:
	testq	%rcx, %rcx
	je	.LBB1_56
# BB#55:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_56:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_59
# BB#57:
	testq	%rax, %rax
	je	.LBB1_59
# BB#58:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_59:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_60
# BB#61:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_62
.LBB1_60:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_62:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_65
# BB#63:
	testq	%rax, %rax
	je	.LBB1_65
# BB#64:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_65:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_68
# BB#66:
	testq	%rax, %rax
	je	.LBB1_68
# BB#67:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_68:
	movl	8(%r13), %edx
	movq	%rsp, %rdi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_69
# BB#70:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_71
.LBB1_69:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_71:
	movq	32(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_72
# BB#73:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_74
.LBB1_72:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_74:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_77
# BB#75:
	testq	%rcx, %rcx
	je	.LBB1_77
# BB#76:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_77:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_80
# BB#78:
	testq	%rax, %rax
	je	.LBB1_80
# BB#79:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_80:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_81
# BB#82:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_83
.LBB1_81:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_83:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_86
# BB#84:
	testq	%rax, %rax
	je	.LBB1_86
# BB#85:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_86:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_89
# BB#87:
	testq	%rax, %rax
	je	.LBB1_89
# BB#88:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_89:
	movl	12(%r13), %edx
	movq	%rsp, %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_90
# BB#91:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_92
.LBB1_90:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_92:
	movq	40(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_93
# BB#94:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_95
.LBB1_93:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_95:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_98
# BB#96:
	testq	%rcx, %rcx
	je	.LBB1_98
# BB#97:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_98:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_101
# BB#99:
	testq	%rax, %rax
	je	.LBB1_101
# BB#100:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_101:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_102
# BB#103:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_104
.LBB1_102:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_104:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_107
# BB#105:
	testq	%rax, %rax
	je	.LBB1_107
# BB#106:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_107:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_110
# BB#108:
	testq	%rax, %rax
	je	.LBB1_110
# BB#109:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_110:
	movl	16(%r13), %edx
	incl	%edx
	movq	%rsp, %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_111
# BB#112:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_113
.LBB1_111:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_113:
	movq	48(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_114
# BB#115:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_116
.LBB1_114:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_116:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_119
# BB#117:
	testq	%rcx, %rcx
	je	.LBB1_119
# BB#118:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_119:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_122
# BB#120:
	testq	%rax, %rax
	je	.LBB1_122
# BB#121:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_122:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_123
# BB#124:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_125
.LBB1_123:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_125:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_128
# BB#126:
	testq	%rax, %rax
	je	.LBB1_128
# BB#127:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_128:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_131
# BB#129:
	testq	%rax, %rax
	je	.LBB1_131
# BB#130:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_131:
	movslq	20(%r13), %rdx
	imulq	$1374389535, %rdx, %rax # imm = 0x51EB851F
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$37, %rax
	addl	%ecx, %eax
	imull	$100, %eax, %eax
	subl	%eax, %edx
	movq	%rsp, %rdi
	movl	$.L.str.17, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_132
# BB#133:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_134
.LBB1_132:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_134:
	movq	56(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_135
# BB#136:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_137
.LBB1_135:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_137:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_140
# BB#138:
	testq	%rcx, %rcx
	je	.LBB1_140
# BB#139:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_140:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_143
# BB#141:
	testq	%rax, %rax
	je	.LBB1_143
# BB#142:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_143:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_144
# BB#145:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_146
.LBB1_144:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_146:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_149
# BB#147:
	testq	%rax, %rax
	je	.LBB1_149
# BB#148:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_149:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_152
# BB#150:
	testq	%rax, %rax
	je	.LBB1_152
# BB#151:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_152:
	movl	20(%r13), %eax
	addl	$1900, %eax             # imm = 0x76C
	cltq
	imulq	$1374389535, %rax, %rdx # imm = 0x51EB851F
	movq	%rdx, %rax
	shrq	$63, %rax
	sarq	$37, %rdx
	addl	%eax, %edx
	movq	%rsp, %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_153
# BB#154:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_155
.LBB1_153:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_155:
	movq	64(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_156
# BB#157:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_158
.LBB1_156:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_158:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_161
# BB#159:
	testq	%rcx, %rcx
	je	.LBB1_161
# BB#160:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_161:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_164
# BB#162:
	testq	%rax, %rax
	je	.LBB1_164
# BB#163:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_164:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_165
# BB#166:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_167
.LBB1_165:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_167:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_170
# BB#168:
	testq	%rax, %rax
	je	.LBB1_170
# BB#169:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_170:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_173
# BB#171:
	testq	%rax, %rax
	je	.LBB1_173
# BB#172:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_173:
	movl	24(%r13), %edx
	incl	%edx
	movq	%rsp, %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_174
# BB#175:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_176
.LBB1_174:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_176:
	movq	72(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_177
# BB#178:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_179
.LBB1_177:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_179:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_182
# BB#180:
	testq	%rcx, %rcx
	je	.LBB1_182
# BB#181:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_182:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_185
# BB#183:
	testq	%rax, %rax
	je	.LBB1_185
# BB#184:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_185:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_186
# BB#187:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_188
.LBB1_186:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_188:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_191
# BB#189:
	testq	%rax, %rax
	je	.LBB1_191
# BB#190:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_191:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_194
# BB#192:
	testq	%rax, %rax
	je	.LBB1_194
# BB#193:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_194:
	movl	28(%r13), %edx
	movq	%rsp, %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_195
# BB#196:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_197
.LBB1_195:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_197:
	movq	80(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movq	%rax, 80(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_198
# BB#199:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_200
.LBB1_198:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_200:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_203
# BB#201:
	testq	%rcx, %rcx
	je	.LBB1_203
# BB#202:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_203:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_206
# BB#204:
	testq	%rax, %rax
	je	.LBB1_206
# BB#205:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_206:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbx
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_207
# BB#208:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_209
.LBB1_207:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_209:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_212
# BB#210:
	testq	%rax, %rax
	je	.LBB1_212
# BB#211:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_212:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_215
# BB#213:
	testq	%rax, %rax
	je	.LBB1_215
# BB#214:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_215:
	movl	32(%r13), %edx
	movq	%rsp, %rdi
	movl	$.L.str.18, %esi
	xorl	%eax, %eax
	callq	sprintf
	movzbl	zz_lengths+10(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB1_216
# BB#217:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_218
.LBB1_216:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB1_218:
	movq	88(%rsp), %rax          # 8-byte Reload
	movb	$10, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	%rax, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_219
# BB#220:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_221
.LBB1_219:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_221:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	current_moment(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_224
# BB#222:
	testq	%rcx, %rcx
	je	.LBB1_224
# BB#223:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_224:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB1_227
# BB#225:
	testq	%rax, %rax
	je	.LBB1_227
# BB#226:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_227:
	movq	no_fpos(%rip), %rdx
	movq	%rsp, %rsi
	movl	$11, %edi
	callq	MakeWord
	movq	%rax, %rbp
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_228
# BB#229:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_230
.LBB1_228:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_230:
	testq	%rbx, %rbx
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	je	.LBB1_233
# BB#231:
	testq	%rax, %rax
	je	.LBB1_233
# BB#232:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_233:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_236
# BB#234:
	testq	%rax, %rax
	je	.LBB1_236
# BB#235:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_236:
	movzbl	zz_lengths+82(%rip), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rdi
	testq	%rdi, %rdi
	je	.LBB1_237
# BB#238:
	movq	%rdi, zz_hold(%rip)
	movq	(%rdi), %rcx
	movq	%rcx, zz_free(,%rax,8)
	jmp	.LBB1_239
.LBB1_237:
	movq	no_fpos(%rip), %rsi
	movl	%eax, %edi
	callq	GetMemory
	movq	%rax, %rdi
	movq	%rdi, zz_hold(%rip)
.LBB1_239:
	movb	$82, 32(%rdi)
	movq	%rdi, 24(%rdi)
	movq	%rdi, 16(%rdi)
	movq	%rdi, 8(%rdi)
	movq	%rdi, (%rdi)
	movq	current_moment(%rip), %rsi
	callq	AttachEnv
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	InitTime, .Lfunc_end1-InitTime
	.cfi_endproc

	.globl	StartMoment
	.p2align	4, 0x90
	.type	StartMoment,@function
StartMoment:                            # @StartMoment
	.cfi_startproc
# BB#0:
	movq	current_moment(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB2_2
# BB#1:
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 16
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.19, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.20, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	current_moment(%rip), %rdi
	addq	$8, %rsp
.LBB2_2:
	movq	no_fpos(%rip), %rsi
	jmp	CopyObject              # TAILCALL
.Lfunc_end2:
	.size	StartMoment, .Lfunc_end2-StartMoment
	.cfi_endproc

	.type	MomentSym,@object       # @MomentSym
	.bss
	.globl	MomentSym
	.p2align	3
MomentSym:
	.quad	0
	.size	MomentSym, 8

	.type	time_string,@object     # @time_string
	.local	time_string
	.comm	time_string,30,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"@Moment"
	.size	.L.str, 8

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.zero	1
	.size	.L.str.1, 1

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"@Tag"
	.size	.L.str.2, 5

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"@Second"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"@Minute"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"@Hour"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"@Day"
	.size	.L.str.6, 5

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"@Month"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"@Year"
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"@Century"
	.size	.L.str.9, 9

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"@WeekDay"
	.size	.L.str.10, 9

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"@YearDay"
	.size	.L.str.11, 9

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"@DaylightSaving"
	.size	.L.str.12, 16

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"unable to obtain the current time"
	.size	.L.str.13, 34

	.type	current_moment,@object  # @current_moment
	.local	current_moment
	.comm	current_moment,8,8
	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"%.2d"
	.size	.L.str.17, 5

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"%d"
	.size	.L.str.18, 3

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"assert failed in %s"
	.size	.L.str.19, 20

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"StartMoment: current_moment == nilobj!"
	.size	.L.str.20, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
