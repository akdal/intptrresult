	.text
	.file	"HYPRE_pcg.bc"
	.globl	HYPRE_PCGSetup
	.p2align	4, 0x90
	.type	HYPRE_PCGSetup,@function
HYPRE_PCGSetup:                         # @HYPRE_PCGSetup
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetup          # TAILCALL
.Lfunc_end0:
	.size	HYPRE_PCGSetup, .Lfunc_end0-HYPRE_PCGSetup
	.cfi_endproc

	.globl	HYPRE_PCGSolve
	.p2align	4, 0x90
	.type	HYPRE_PCGSolve,@function
HYPRE_PCGSolve:                         # @HYPRE_PCGSolve
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSolve          # TAILCALL
.Lfunc_end1:
	.size	HYPRE_PCGSolve, .Lfunc_end1-HYPRE_PCGSolve
	.cfi_endproc

	.globl	HYPRE_PCGSetTol
	.p2align	4, 0x90
	.type	HYPRE_PCGSetTol,@function
HYPRE_PCGSetTol:                        # @HYPRE_PCGSetTol
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetTol         # TAILCALL
.Lfunc_end2:
	.size	HYPRE_PCGSetTol, .Lfunc_end2-HYPRE_PCGSetTol
	.cfi_endproc

	.globl	HYPRE_PCGSetMaxIter
	.p2align	4, 0x90
	.type	HYPRE_PCGSetMaxIter,@function
HYPRE_PCGSetMaxIter:                    # @HYPRE_PCGSetMaxIter
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetMaxIter     # TAILCALL
.Lfunc_end3:
	.size	HYPRE_PCGSetMaxIter, .Lfunc_end3-HYPRE_PCGSetMaxIter
	.cfi_endproc

	.globl	HYPRE_PCGSetStopCrit
	.p2align	4, 0x90
	.type	HYPRE_PCGSetStopCrit,@function
HYPRE_PCGSetStopCrit:                   # @HYPRE_PCGSetStopCrit
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetStopCrit    # TAILCALL
.Lfunc_end4:
	.size	HYPRE_PCGSetStopCrit, .Lfunc_end4-HYPRE_PCGSetStopCrit
	.cfi_endproc

	.globl	HYPRE_PCGSetTwoNorm
	.p2align	4, 0x90
	.type	HYPRE_PCGSetTwoNorm,@function
HYPRE_PCGSetTwoNorm:                    # @HYPRE_PCGSetTwoNorm
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetTwoNorm     # TAILCALL
.Lfunc_end5:
	.size	HYPRE_PCGSetTwoNorm, .Lfunc_end5-HYPRE_PCGSetTwoNorm
	.cfi_endproc

	.globl	HYPRE_PCGSetRelChange
	.p2align	4, 0x90
	.type	HYPRE_PCGSetRelChange,@function
HYPRE_PCGSetRelChange:                  # @HYPRE_PCGSetRelChange
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetRelChange   # TAILCALL
.Lfunc_end6:
	.size	HYPRE_PCGSetRelChange, .Lfunc_end6-HYPRE_PCGSetRelChange
	.cfi_endproc

	.globl	HYPRE_PCGSetPrecond
	.p2align	4, 0x90
	.type	HYPRE_PCGSetPrecond,@function
HYPRE_PCGSetPrecond:                    # @HYPRE_PCGSetPrecond
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetPrecond     # TAILCALL
.Lfunc_end7:
	.size	HYPRE_PCGSetPrecond, .Lfunc_end7-HYPRE_PCGSetPrecond
	.cfi_endproc

	.globl	HYPRE_PCGGetPrecond
	.p2align	4, 0x90
	.type	HYPRE_PCGGetPrecond,@function
HYPRE_PCGGetPrecond:                    # @HYPRE_PCGGetPrecond
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGGetPrecond     # TAILCALL
.Lfunc_end8:
	.size	HYPRE_PCGGetPrecond, .Lfunc_end8-HYPRE_PCGGetPrecond
	.cfi_endproc

	.globl	HYPRE_PCGSetLogging
	.p2align	4, 0x90
	.type	HYPRE_PCGSetLogging,@function
HYPRE_PCGSetLogging:                    # @HYPRE_PCGSetLogging
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGSetLogging     # TAILCALL
.Lfunc_end9:
	.size	HYPRE_PCGSetLogging, .Lfunc_end9-HYPRE_PCGSetLogging
	.cfi_endproc

	.globl	HYPRE_PCGGetNumIterations
	.p2align	4, 0x90
	.type	HYPRE_PCGGetNumIterations,@function
HYPRE_PCGGetNumIterations:              # @HYPRE_PCGGetNumIterations
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGGetNumIterations # TAILCALL
.Lfunc_end10:
	.size	HYPRE_PCGGetNumIterations, .Lfunc_end10-HYPRE_PCGGetNumIterations
	.cfi_endproc

	.globl	HYPRE_PCGGetFinalRelativeResidualNorm
	.p2align	4, 0x90
	.type	HYPRE_PCGGetFinalRelativeResidualNorm,@function
HYPRE_PCGGetFinalRelativeResidualNorm:  # @HYPRE_PCGGetFinalRelativeResidualNorm
	.cfi_startproc
# BB#0:
	jmp	hypre_PCGGetFinalRelativeResidualNorm # TAILCALL
.Lfunc_end11:
	.size	HYPRE_PCGGetFinalRelativeResidualNorm, .Lfunc_end11-HYPRE_PCGGetFinalRelativeResidualNorm
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
