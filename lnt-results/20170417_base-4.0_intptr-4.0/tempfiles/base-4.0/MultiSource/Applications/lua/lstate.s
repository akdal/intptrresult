	.text
	.file	"lstate.bc"
	.hidden	luaE_newthread
	.globl	luaE_newthread
	.p2align	4, 0x90
	.type	luaE_newthread,@function
luaE_newthread:                         # @luaE_newthread
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$184, %ecx
	callq	luaM_realloc_
	movq	%rax, %rbx
	movl	$8, %edx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	luaC_link
	movq	32(%r14), %rax
	movq	%rax, 32(%rbx)
	movq	$0, 64(%rbx)
	movl	$0, 88(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 112(%rbx)
	movb	$0, 100(%rbx)
	movl	$0, 104(%rbx)
	movb	$1, 101(%rbx)
	movl	$0, 108(%rbx)
	movq	$0, 152(%rbx)
	movb	$0, 10(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 176(%rbx)
	movl	$0, 128(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	$0, 92(%rbx)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$320, %ecx              # imm = 0x140
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	%rax, 80(%rbx)
	movq	%rax, 40(%rbx)
	movl	$8, 92(%rbx)
	addq	$280, %rax              # imm = 0x118
	movq	%rax, 72(%rbx)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$720, %ecx              # imm = 0x2D0
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	%rax, 64(%rbx)
	movl	$45, 88(%rbx)
	leaq	624(%rax), %rcx
	movq	%rcx, 56(%rbx)
	movq	40(%rbx), %rcx
	movq	%rax, 8(%rcx)
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movl	$0, 8(%rax)
	movq	%rdx, (%rcx)
	movq	%rdx, 24(%rbx)
	addq	$336, %rax              # imm = 0x150
	movq	%rax, 16(%rcx)
	movq	120(%r14), %rax
	movq	%rax, 120(%rbx)
	movl	128(%r14), %eax
	movl	%eax, 128(%rbx)
	movb	100(%r14), %al
	movb	%al, 100(%rbx)
	movl	104(%r14), %eax
	movl	%eax, 104(%rbx)
	movq	112(%r14), %rcx
	movq	%rcx, 112(%rbx)
	movl	%eax, 108(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	luaE_newthread, .Lfunc_end0-luaE_newthread
	.cfi_endproc

	.hidden	luaE_freethread
	.globl	luaE_freethread
	.p2align	4, 0x90
	.type	luaE_freethread,@function
luaE_freethread:                        # @luaE_freethread
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	64(%rbx), %rsi
	movq	%rbx, %rdi
	callq	luaF_close
	movq	80(%rbx), %rsi
	movslq	92(%rbx), %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movq	64(%rbx), %rsi
	movslq	88(%rbx), %rdx
	shlq	$4, %rdx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	callq	luaM_realloc_
	movl	$184, %edx
	xorl	%ecx, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	luaM_realloc_           # TAILCALL
.Lfunc_end1:
	.size	luaE_freethread, .Lfunc_end1-luaE_freethread
	.cfi_endproc

	.globl	lua_newstate
	.p2align	4, 0x90
	.type	lua_newstate,@function
lua_newstate:                           # @lua_newstate
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r12
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$616, %ecx              # imm = 0x268
	movq	%r15, %rdi
	callq	*%r12
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_4
# BB#1:
	leaq	184(%rbx), %rax
	movq	$0, (%rbx)
	movb	$8, 8(%rbx)
	movb	$33, 216(%rbx)
	movb	$97, 9(%rbx)
	movq	%rax, 32(%rbx)
	movq	$0, 64(%rbx)
	movl	$0, 88(%rbx)
	movq	$0, 168(%rbx)
	movq	$0, 112(%rbx)
	movb	$0, 100(%rbx)
	movl	$0, 104(%rbx)
	movb	$1, 101(%rbx)
	movl	$0, 108(%rbx)
	movq	$0, 152(%rbx)
	movb	$0, 10(%rbx)
	movq	$0, 80(%rbx)
	movq	$0, 176(%rbx)
	movl	$0, 128(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 40(%rbx)
	movq	$0, 92(%rbx)
	movq	%r12, 200(%rbx)
	movq	%r15, 208(%rbx)
	movq	%rbx, 360(%rbx)
	leaq	368(%rbx), %rax
	movq	%rax, 392(%rbx)
	movq	%rax, 400(%rbx)
	movq	$0, 296(%rbx)
	movups	%xmm0, 184(%rbx)
	movl	$0, 352(%rbx)
	movq	$0, 272(%rbx)
	movq	$0, 288(%rbx)
	movq	$0, 336(%rbx)
	movb	$0, 217(%rbx)
	movq	%rbx, %rax
	addq	$224, %rax
	movq	%rbx, 224(%rbx)
	movl	$0, 220(%rbx)
	movq	%rax, 232(%rbx)
	movups	%xmm0, 256(%rbx)
	movups	%xmm0, 240(%rbx)
	movq	$616, 304(%rbx)         # imm = 0x268
	movl	$200, 328(%rbx)
	movl	$200, 332(%rbx)
	movq	$0, 320(%rbx)
	movups	%xmm0, 456(%rbx)
	movups	%xmm0, 440(%rbx)
	movups	%xmm0, 424(%rbx)
	movups	%xmm0, 408(%rbx)
	movq	$0, 472(%rbx)
	movl	$f_luaopen, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaD_rawrunprotected
	testl	%eax, %eax
	je	.LBB2_2
# BB#3:
	movq	32(%rbx), %r15
	movq	64(%rbx), %rsi
	movq	%rbx, %rdi
	callq	luaF_close
	movq	%rbx, %rdi
	callq	luaC_freeall
	movq	32(%rbx), %rax
	movq	(%rax), %rsi
	movslq	12(%rax), %rdx
	shlq	$3, %rdx
	xorl	%r14d, %r14d
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	88(%r15), %rsi
	movq	104(%r15), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	%rax, 88(%r15)
	movq	$0, 104(%r15)
	movq	80(%rbx), %rsi
	movslq	92(%rbx), %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	64(%rbx), %rsi
	movslq	88(%rbx), %rdx
	shlq	$4, %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	24(%r15), %rdi
	movl	$616, %edx              # imm = 0x268
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	callq	*16(%r15)
	jmp	.LBB2_4
.LBB2_2:
	movq	%rbx, %r14
.LBB2_4:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	lua_newstate, .Lfunc_end2-lua_newstate
	.cfi_endproc

	.p2align	4, 0x90
	.type	f_luaopen,@function
f_luaopen:                              # @f_luaopen
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 32
.Lcfi22:
	.cfi_offset %rbx, -32
.Lcfi23:
	.cfi_offset %r14, -24
.Lcfi24:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movq	32(%rbx), %r14
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$320, %ecx              # imm = 0x140
	callq	luaM_realloc_
	movq	%rax, 80(%rbx)
	movq	%rax, 40(%rbx)
	movl	$8, 92(%rbx)
	addq	$280, %rax              # imm = 0x118
	movq	%rax, 72(%rbx)
	xorl	%esi, %esi
	xorl	%edx, %edx
	movl	$720, %ecx              # imm = 0x2D0
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	%rax, 64(%rbx)
	movl	$45, 88(%rbx)
	leaq	624(%rax), %rcx
	movq	%rcx, 56(%rbx)
	movq	40(%rbx), %rcx
	movq	%rax, 8(%rcx)
	leaq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movl	$0, 8(%rax)
	movq	%rdx, (%rcx)
	movq	%rdx, 24(%rbx)
	addq	$336, %rax              # imm = 0x150
	movq	%rax, 16(%rcx)
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	luaH_new
	movq	%rax, 120(%rbx)
	movl	$5, 128(%rbx)
	movq	32(%rbx), %r15
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	luaH_new
	movq	%rax, 160(%r15)
	movl	$5, 168(%r15)
	movl	$32, %esi
	movq	%rbx, %rdi
	callq	luaS_resize
	movq	%rbx, %rdi
	callq	luaT_init
	movq	%rbx, %rdi
	callq	luaX_init
	movl	$.L.str, %esi
	movl	$17, %edx
	movq	%rbx, %rdi
	callq	luaS_newlstr
	orb	$32, 9(%rax)
	movq	120(%r14), %rax
	shlq	$2, %rax
	movq	%rax, 112(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	f_luaopen, .Lfunc_end3-f_luaopen
	.cfi_endproc

	.globl	lua_close
	.p2align	4, 0x90
	.type	lua_close,@function
lua_close:                              # @lua_close
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -24
.Lcfi29:
	.cfi_offset %r14, -16
	movq	32(%rdi), %rax
	movq	176(%rax), %rbx
	movq	64(%rbx), %rsi
	movq	%rbx, %rdi
	callq	luaF_close
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	luaC_separateudata
	movq	$0, 176(%rbx)
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rax
	movq	%rax, 40(%rbx)
	movq	(%rax), %rax
	movq	%rax, 16(%rbx)
	movq	%rax, 24(%rbx)
	movl	$0, 96(%rbx)
	movl	$callallgcTM, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	luaD_rawrunprotected
	testl	%eax, %eax
	jne	.LBB4_1
# BB#2:
	movq	32(%rbx), %r14
	movq	64(%rbx), %rsi
	movq	%rbx, %rdi
	callq	luaF_close
	movq	%rbx, %rdi
	callq	luaC_freeall
	movq	32(%rbx), %rax
	movq	(%rax), %rsi
	movslq	12(%rax), %rdx
	shlq	$3, %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	88(%r14), %rsi
	movq	104(%r14), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	%rax, 88(%r14)
	movq	$0, 104(%r14)
	movq	80(%rbx), %rsi
	movslq	92(%rbx), %rax
	shlq	$3, %rax
	leaq	(%rax,%rax,4), %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	64(%rbx), %rsi
	movslq	88(%rbx), %rdx
	shlq	$4, %rdx
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	callq	luaM_realloc_
	movq	16(%r14), %rax
	movq	24(%r14), %rdi
	movl	$616, %edx              # imm = 0x268
	xorl	%ecx, %ecx
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*%rax                   # TAILCALL
.Lfunc_end4:
	.size	lua_close, .Lfunc_end4-lua_close
	.cfi_endproc

	.p2align	4, 0x90
	.type	callallgcTM,@function
callallgcTM:                            # @callallgcTM
	.cfi_startproc
# BB#0:
	jmp	luaC_callGCTM           # TAILCALL
.Lfunc_end5:
	.size	callallgcTM, .Lfunc_end5-callallgcTM
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"not enough memory"
	.size	.L.str, 18

	.hidden	luaM_realloc_
	.hidden	luaC_link
	.hidden	luaF_close
	.hidden	luaD_rawrunprotected
	.hidden	luaC_separateudata
	.hidden	luaH_new
	.hidden	luaS_resize
	.hidden	luaT_init
	.hidden	luaX_init
	.hidden	luaS_newlstr
	.hidden	luaC_freeall
	.hidden	luaC_callGCTM

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
