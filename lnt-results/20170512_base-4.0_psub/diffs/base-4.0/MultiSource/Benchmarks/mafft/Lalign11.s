	.text
	.file	"Lalign11.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4608533498688228557     # double 1.3
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI0_1:
	.long	3463342888              # float -1.0E+9
	.text
	.globl	L__align11
	.p2align	4, 0x90
	.type	L__align11,@function
L__align11:                             # @L__align11
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 192
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r8, 104(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movl	%edx, 88(%rsp)          # 4-byte Spill
	movq	%rsi, %r12
	xorl	%eax, %eax
	subl	offset(%rip), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	penalty_ex(%rip), %xmm0
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movl	L__align11.orlgth1(%rip), %r14d
	testl	%r14d, %r14d
	movq	%rdi, 40(%rsp)          # 8-byte Spill
	jne	.LBB0_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, L__align11.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	%rax, L__align11.mseq2(%rip)
	movl	L__align11.orlgth1(%rip), %r14d
.LBB0_2:
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%r12), %rdi
	callq	strlen
	cmpl	%r14d, %ebx
	movl	L__align11.orlgth2(%rip), %r15d
	movq	%r12, 72(%rsp)          # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jg	.LBB0_5
# BB#3:
	cmpl	%r15d, %eax
	jg	.LBB0_5
# BB#4:                                 # %._crit_edge284
	movq	%rbx, %r13
	movq	L__align11.mseq(%rip), %rax
	jmp	.LBB0_9
.LBB0_5:
	testl	%r14d, %r14d
	jle	.LBB0_8
# BB#6:
	testl	%r15d, %r15d
	jle	.LBB0_8
# BB#7:
	movq	L__align11.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	L__align11.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	L__align11.match(%rip), %rdi
	callq	FreeFloatVec
	movq	L__align11.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	L__align11.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	L__align11.m(%rip), %rdi
	callq	FreeFloatVec
	movq	L__align11.mp(%rip), %rdi
	callq	FreeIntVec
	movq	L__align11.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	24(%rsp), %rax          # 8-byte Reload
	movl	L__align11.orlgth1(%rip), %r14d
	movl	L__align11.orlgth2(%rip), %r15d
.LBB0_8:
	movq	%rbx, %r13
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI0_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %ecx
	cmpl	%r14d, %ecx
	cmovgel	%ecx, %r14d
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	102(%r15), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, L__align11.w1(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, L__align11.w2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, L__align11.match(%rip)
	leal	102(%r14), %r12d
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, L__align11.initverticalw(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, L__align11.lastverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, L__align11.m(%rip)
	movl	%ebp, %edi
	callq	AllocateIntVec
	movq	%rax, L__align11.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r14,%r15), %esi
	callq	AllocateCharMtx
	movq	%rax, L__align11.mseq(%rip)
	movl	%r14d, L__align11.orlgth1(%rip)
	movl	%r15d, L__align11.orlgth2(%rip)
	movq	72(%rsp), %r12          # 8-byte Reload
.LBB0_9:
	movq	40(%rsp), %rsi          # 8-byte Reload
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movq	(%rax), %rcx
	movq	L__align11.mseq1(%rip), %rdx
	movq	%rcx, (%rdx)
	movq	8(%rax), %rax
	movq	L__align11.mseq2(%rip), %rcx
	movq	%rax, (%rcx)
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r14d
	movl	commonAlloc2(%rip), %ebp
	jg	.LBB0_12
# BB#10:
	cmpl	%ebp, %r15d
	jg	.LBB0_12
# BB#11:                                # %._crit_edge292
	movq	commonIP(%rip), %rax
	jmp	.LBB0_16
.LBB0_12:                               # %._crit_edge286
	testl	%ebx, %ebx
	je	.LBB0_15
# BB#13:                                # %._crit_edge286
	testl	%ebp, %ebp
	je	.LBB0_15
# BB#14:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	L__align11.orlgth1(%rip), %r14d
	movl	commonAlloc1(%rip), %ebx
	movl	L__align11.orlgth2(%rip), %r15d
	movl	commonAlloc2(%rip), %ebp
.LBB0_15:
	cmpl	%ebx, %r14d
	cmovgel	%r14d, %ebx
	cmpl	%ebp, %r15d
	cmovgel	%r15d, %ebp
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
	movq	40(%rsp), %rsi          # 8-byte Reload
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
.LBB0_16:
	movq	%r13, %rbx
	movq	%rax, L__align11.ijp(%rip)
	movq	L__align11.w1(%rip), %r11
	movq	L__align11.w2(%rip), %r9
	movq	L__align11.initverticalw(%rip), %rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	(%r12), %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	(%rsi), %rcx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	testl	%ebx, %ebx
	je	.LBB0_22
# BB#17:                                # %.lr.ph.i.preheader
	movq	48(%rsp), %rcx          # 8-byte Reload
	movsbq	(%rcx), %rbp
	testb	$1, %bl
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, %edx
	je	.LBB0_19
# BB#18:                                # %.lr.ph.i.prol
	leal	-1(%rbx), %edx
	movq	56(%rsp), %rsi          # 8-byte Reload
	leaq	1(%rsi), %rcx
	movsbq	(%rsi), %rsi
	movq	%rbp, %rdi
	shlq	$9, %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdi,%rsi,4), %xmm0
	movq	16(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi), %rsi
	movss	%xmm0, (%rdi)
.LBB0_19:                               # %.lr.ph.i.prol.loopexit
	cmpl	$1, %ebx
	je	.LBB0_22
# BB#20:                                # %.lr.ph.i.preheader.new
	shlq	$9, %rbp
	.p2align	4, 0x90
.LBB0_21:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rcx), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rdi,4), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %edx
	movsbq	1(%rcx), %rdi
	leaq	2(%rcx), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rbp,%rdi,4), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB0_21
.LBB0_22:                               # %match_calc.exit
	movb	$1, 12(%rsp)            # 1-byte Folded Spill
	movq	24(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB0_45
# BB#23:                                # %.lr.ph.i223.preheader
	movq	56(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax), %rax
	testb	$1, %dil
	movq	%r11, %rsi
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	%edi, %edx
	je	.LBB0_25
# BB#24:                                # %.lr.ph.i223.prol
	leal	-1(%rdi), %edx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	1(%rsi), %rcx
	movsbq	(%rsi), %rsi
	movq	%rax, %rdi
	shlq	$9, %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rdi,%rsi,4), %xmm0
	movq	24(%rsp), %rdi          # 8-byte Reload
	leaq	4(%r11), %rsi
	movss	%xmm0, (%r11)
.LBB0_25:                               # %.lr.ph.i223.prol.loopexit
	cmpl	$1, %edi
	je	.LBB0_28
# BB#26:                                # %.lr.ph.i223.preheader.new
	shlq	$9, %rax
	.p2align	4, 0x90
.LBB0_27:                               # %.lr.ph.i223
                                        # =>This Inner Loop Header: Depth=1
	movsbq	(%rcx), %rdi
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %edx
	movsbq	1(%rcx), %rdi
	leaq	2(%rcx), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	amino_dis(%rax,%rdi,4), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB0_27
.LBB0_28:                               # %match_calc.exit225.preheader
	movq	24(%rsp), %rdi          # 8-byte Reload
	testl	%edi, %edi
	setle	%r8b
	jle	.LBB0_45
# BB#29:                                # %.lr.ph262
	movq	%rbx, %r13
	movq	L__align11.m(%rip), %rdx
	movq	L__align11.mp(%rip), %rsi
	leaq	1(%rdi), %rbx
	movl	%ebx, %r15d
	leaq	-1(%r15), %r14
	cmpq	$8, %r14
	jb	.LBB0_30
# BB#38:                                # %min.iters.checked
	movl	%edi, %r10d
	andl	$7, %r10d
	movq	%r14, %r12
	subq	%r10, %r12
	je	.LBB0_30
# BB#39:                                # %vector.memcheck
	leaq	4(%rdx), %rax
	leaq	-4(%r11,%r15,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB0_41
# BB#40:                                # %vector.memcheck
	leaq	(%rdx,%r15,4), %rax
	cmpq	%rax, %r11
	jae	.LBB0_41
.LBB0_30:
	movl	$1, %ecx
.LBB0_31:                               # %match_calc.exit225.preheader355
	subl	%ecx, %ebx
	subq	%rcx, %r14
	andq	$3, %rbx
	je	.LBB0_34
# BB#32:                                # %match_calc.exit225.prol.preheader
	negq	%rbx
	.p2align	4, 0x90
.LBB0_33:                               # %match_calc.exit225.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r11,%rcx,4), %eax
	movl	%eax, (%rdx,%rcx,4)
	movl	$0, (%rsi,%rcx,4)
	incq	%rcx
	incq	%rbx
	jne	.LBB0_33
.LBB0_34:                               # %match_calc.exit225.prol.loopexit
	cmpq	$3, %r14
	jae	.LBB0_35
.LBB0_44:
	movb	%r8b, 12(%rsp)          # 1-byte Spill
	movq	%r13, %rbx
	jmp	.LBB0_45
.LBB0_35:                               # %match_calc.exit225.preheader355.new
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB0_36:                               # %match_calc.exit225
                                        # =>This Inner Loop Header: Depth=1
	movl	-4(%r11,%rcx,4), %eax
	movl	%eax, (%rdx,%rcx,4)
	movl	$0, (%rsi,%rcx,4)
	movl	(%r11,%rcx,4), %eax
	movl	%eax, 4(%rdx,%rcx,4)
	movl	$0, 4(%rsi,%rcx,4)
	movl	4(%r11,%rcx,4), %eax
	movl	%eax, 8(%rdx,%rcx,4)
	movl	$0, 8(%rsi,%rcx,4)
	movl	8(%r11,%rcx,4), %eax
	movl	%eax, 12(%rdx,%rcx,4)
	movl	$0, 12(%rsi,%rcx,4)
	addq	$4, %rcx
	cmpq	%rcx, %r15
	jne	.LBB0_36
# BB#37:
	movb	%r8b, 12(%rsp)          # 1-byte Spill
.LBB0_45:                               # %match_calc.exit225._crit_edge
	movq	%rdi, %rax
	shlq	$32, %rax
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	addq	%rax, %rsi
	movq	%rsi, %rax
	sarq	$30, %rax
	movl	(%r11,%rax), %eax
	movq	L__align11.lastverticalw(%rip), %rcx
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movl	%eax, (%rcx)
	leal	1(%rbx,%rdi), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%eax, localstop(%rip)
	testl	%ebx, %ebx
	jle	.LBB0_46
# BB#47:                                # %.lr.ph255
	movl	64(%rsp), %eax          # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	sarq	$32, %rsi
	incl	%ebx
	testl	%edi, %edi
	movq	L__align11.m(%rip), %r13
	movq	L__align11.mp(%rip), %r10
	movl	%ebx, %ebx
	je	.LBB0_55
# BB#48:                                # %.lr.ph255.split.preheader
	movl	%edi, %eax
	andl	$1, %eax
	movl	%eax, 92(%rsp)          # 4-byte Spill
	leal	-1(%rdi), %r8d
	movq	48(%rsp), %rax          # 8-byte Reload
	incq	%rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	movss	.LCPI0_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movl	$1, %r15d
	xorl	%r12d, %r12d
	xorl	%edx, %edx
	movq	%rsi, 120(%rsp)         # 8-byte Spill
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB0_49:                               # %.lr.ph255.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_52 Depth 2
                                        #     Child Loop BB0_68 Depth 2
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movq	%r11, %rcx
	cmpl	$0, 92(%rsp)            # 4-byte Folded Reload
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%r15,4), %edx
	movl	%edx, (%rcx)
	movq	56(%rsp), %rax          # 8-byte Reload
	movsbq	(%rax,%r15), %rax
	movq	%r9, %rsi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	%edi, %ebx
	je	.LBB0_51
# BB#50:                                # %.lr.ph.i229.prol
                                        #   in Loop: Header=BB0_49 Depth=1
	movq	48(%rsp), %rdx          # 8-byte Reload
	movsbq	(%rdx), %rdx
	movq	%rax, %rsi
	shlq	$9, %rsi
	cvtsi2ssl	amino_dis(%rsi,%rdx,4), %xmm2
	leaq	4(%r9), %rsi
	movss	%xmm2, (%r9)
	movq	128(%rsp), %rbp         # 8-byte Reload
	movl	%r8d, %ebx
.LBB0_51:                               # %.lr.ph.i229.prol.loopexit
                                        #   in Loop: Header=BB0_49 Depth=1
	cmpl	$1, %edi
	je	.LBB0_53
	.p2align	4, 0x90
.LBB0_52:                               # %.lr.ph.i229
                                        #   Parent Loop BB0_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsbq	(%rbp), %rdx
	movq	%rax, %rdi
	shlq	$9, %rdi
	xorps	%xmm2, %xmm2
	cvtsi2ssl	amino_dis(%rdi,%rdx,4), %xmm2
	movss	%xmm2, (%rsi)
	addl	$-2, %ebx
	movsbq	1(%rbp), %rdx
	leaq	2(%rbp), %rbp
	xorps	%xmm2, %xmm2
	cvtsi2ssl	amino_dis(%rdi,%rdx,4), %xmm2
	movss	%xmm2, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB0_52
.LBB0_53:                               # %match_calc.exit231
                                        #   in Loop: Header=BB0_49 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%r15,4), %eax
	movl	%eax, (%r9)
	movl	(%rcx), %esi
	movl	%esi, L__align11.mi(%rip)
	xorl	%r14d, %r14d
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB0_67
# BB#54:                                #   in Loop: Header=BB0_49 Depth=1
	movaps	%xmm1, %xmm7
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB0_78
	.p2align	4, 0x90
.LBB0_67:                               # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_49 Depth=1
	leaq	-1(%r15), %r11
	movd	%esi, %xmm2
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r15,8), %rdi
	xorl	%ebx, %ebx
	movl	$-1, %ebp
	movdqa	%xmm2, %xmm3
	xorl	%r14d, %r14d
	movq	64(%rsp), %rdx          # 8-byte Reload
	jmp	.LBB0_68
	.p2align	4, 0x90
.LBB0_129:                              # %..lr.ph_crit_edge
                                        #   in Loop: Header=BB0_68 Depth=2
	movss	4(%rcx,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	decl	%ebp
	movq	%rsi, %rbx
	movaps	%xmm7, %xmm1
.LBB0_68:                               # %.lr.ph
                                        #   Parent Loop BB0_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm5, %xmm7
	addss	%xmm2, %xmm7
	leal	(%r14,%rbp), %eax
	ucomiss	%xmm3, %xmm7
	movl	$0, %esi
	cmovbel	%esi, %eax
	maxss	%xmm3, %xmm7
	movl	%eax, 4(%rdi,%rbx,4)
	ucomiss	%xmm2, %xmm3
	jbe	.LBB0_70
# BB#69:                                #   in Loop: Header=BB0_68 Depth=2
	movss	%xmm3, L__align11.mi(%rip)
	movaps	%xmm3, %xmm2
	movl	%ebx, %r14d
.LBB0_70:                               #   in Loop: Header=BB0_68 Depth=2
	addss	%xmm6, %xmm2
	movss	%xmm2, L__align11.mi(%rip)
	movss	4(%r13,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm7, %xmm4
	jbe	.LBB0_72
# BB#71:                                #   in Loop: Header=BB0_68 Depth=2
	movl	%r15d, %eax
	subl	4(%r10,%rbx,4), %eax
	movl	%eax, 4(%rdi,%rbx,4)
	movaps	%xmm4, %xmm7
.LBB0_72:                               #   in Loop: Header=BB0_68 Depth=2
	leaq	1(%rbx), %rsi
	movss	(%rcx,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm4
	jbe	.LBB0_74
# BB#73:                                #   in Loop: Header=BB0_68 Depth=2
	movss	%xmm4, 4(%r13,%rbx,4)
	movl	%r11d, 4(%r10,%rbx,4)
	movaps	%xmm4, %xmm3
.LBB0_74:                               #   in Loop: Header=BB0_68 Depth=2
	addss	%xmm6, %xmm3
	movss	%xmm3, 4(%r13,%rbx,4)
	ucomiss	%xmm1, %xmm7
	cmoval	%r15d, %edx
	cmoval	%esi, %r12d
	ucomiss	%xmm7, %xmm0
	movaps	%xmm7, %xmm3
	jbe	.LBB0_76
# BB#75:                                #   in Loop: Header=BB0_68 Depth=2
	movl	8(%rsp), %eax           # 4-byte Reload
	movl	%eax, 4(%rdi,%rbx,4)
	movaps	%xmm0, %xmm3
.LBB0_76:                               #   in Loop: Header=BB0_68 Depth=2
	maxss	%xmm1, %xmm7
	addss	4(%r9,%rbx,4), %xmm3
	movss	%xmm3, 4(%r9,%rbx,4)
	cmpl	%ebx, %r8d
	jne	.LBB0_129
# BB#77:                                #   in Loop: Header=BB0_49 Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
.LBB0_78:                               # %._crit_edge
                                        #   in Loop: Header=BB0_49 Depth=1
	movq	120(%rsp), %rsi         # 8-byte Reload
	movq	112(%rsp), %rbx         # 8-byte Reload
	movl	(%r9,%rsi,4), %eax
	movq	80(%rsp), %rbp          # 8-byte Reload
	movl	%eax, (%rbp,%r15,4)
	incq	%r15
	cmpq	%rbx, %r15
	movq	%r9, %r11
	movq	%rcx, %r9
	movaps	%xmm7, %xmm1
	jne	.LBB0_49
	jmp	.LBB0_81
.LBB0_46:
	xorl	%edx, %edx
	movss	.LCPI0_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	xorl	%r12d, %r12d
	movl	8(%rsp), %ecx           # 4-byte Reload
	jmp	.LBB0_82
.LBB0_55:                               # %.lr.ph255.split.us.preheader
	decq	%rbx
	xorl	%r14d, %r14d
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	80(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_56:                               # %.lr.ph255.split.us
                                        # =>This Inner Loop Header: Depth=1
	movq	%r11, %rdi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	(%rcx,%r14,4), %eax
	movl	%eax, (%rdi)
	movl	4(%rcx,%r14,4), %eax
	movl	%eax, (%r9)
	movl	(%rdi), %eax
	cmpb	$0, 12(%rsp)            # 1-byte Folded Reload
	je	.LBB0_57
# BB#79:                                # %._crit_edge.us
                                        #   in Loop: Header=BB0_56 Depth=1
	movl	(%r9,%rsi,4), %ecx
	movl	%ecx, 4(%rbp,%r14,4)
	incq	%r14
	cmpq	%r14, %rbx
	movq	%r9, %r11
	movq	%rdi, %r9
	jne	.LBB0_56
# BB#80:                                # %._crit_edge256.loopexit
	movl	%eax, L__align11.mi(%rip)
	movss	.LCPI0_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	xorl	%r14d, %r14d
	xorl	%edx, %edx
	xorl	%r12d, %r12d
.LBB0_81:                               # %._crit_edge256
	movl	%r14d, L__align11.mpi(%rip)
	movl	localstop(%rip), %ecx
.LBB0_82:
	movslq	%edx, %rsi
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rsi,8), %rax
	movslq	%r12d, %rdi
	cmpl	%ecx, (%rax,%rdi,4)
	jne	.LBB0_84
# BB#83:
	movq	56(%rsp), %rax          # 8-byte Reload
	movb	$0, (%rax)
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movb	$0, (%rax)
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	$0, (%rax)
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	$0, (%rax)
	xorps	%xmm0, %xmm0
	jmp	.LBB0_128
.LBB0_84:
	movq	%rdi, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	movq	L__align11.mseq1(%rip), %r15
	movq	L__align11.mseq2(%rip), %r14
	movq	56(%rsp), %rdi          # 8-byte Reload
	movl	%ecx, %ebx
	callq	strlen
	movq	%rax, %r13
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	strlen
	movl	%ebx, %edi
	testl	%r13d, %r13d
	js	.LBB0_91
# BB#85:                                # %.lr.ph27.i
	movq	%r13, %rdx
	incq	%rdx
	movl	%edx, %ecx
	leaq	-1(%rcx), %r8
	xorl	%esi, %esi
	andq	$7, %rdx
	je	.LBB0_88
# BB#86:                                # %.prol.preheader
	movq	32(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_87:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rsi,8), %rbp
	movl	%edi, (%rbp)
	incq	%rsi
	cmpq	%rsi, %rdx
	jne	.LBB0_87
.LBB0_88:                               # %.prol.loopexit
	cmpq	$7, %r8
	jb	.LBB0_91
# BB#89:                                # %.lr.ph27.i.new
	subq	%rsi, %rcx
	movq	32(%rsp), %rdx          # 8-byte Reload
	leaq	56(%rdx,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB0_90:                               # =>This Inner Loop Header: Depth=1
	movq	-56(%rdx), %rsi
	movl	%edi, (%rsi)
	movq	-48(%rdx), %rsi
	movl	%edi, (%rsi)
	movq	-40(%rdx), %rsi
	movl	%edi, (%rsi)
	movq	-32(%rdx), %rsi
	movl	%edi, (%rsi)
	movq	-24(%rdx), %rsi
	movl	%edi, (%rsi)
	movq	-16(%rdx), %rsi
	movl	%edi, (%rsi)
	movq	-8(%rdx), %rsi
	movl	%edi, (%rsi)
	movq	(%rdx), %rsi
	movl	%edi, (%rsi)
	addq	$64, %rdx
	addq	$-8, %rcx
	jne	.LBB0_90
.LBB0_91:                               # %.preheader.i
	testl	%eax, %eax
	movq	72(%rsp), %rbp          # 8-byte Reload
	js	.LBB0_100
# BB#92:                                # %.lr.ph23.i
	movl	localstop(%rip), %r10d
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx), %r8
	movq	%rax, %rsi
	incq	%rsi
	movl	%esi, %edx
	cmpq	$7, %rdx
	jbe	.LBB0_93
# BB#96:                                # %min.iters.checked339
	andl	$7, %esi
	movq	%rdx, %r9
	subq	%rsi, %r9
	je	.LBB0_93
# BB#97:                                # %vector.ph343
	movd	%r10d, %xmm0
	pshufd	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	16(%r8), %rbx
	movq	%r9, %rdi
	.p2align	4, 0x90
.LBB0_98:                               # %vector.body335
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-8, %rdi
	jne	.LBB0_98
# BB#99:                                # %middle.block336
	testq	%rsi, %rsi
	jne	.LBB0_94
	jmp	.LBB0_100
.LBB0_93:
	xorl	%r9d, %r9d
.LBB0_94:                               # %scalar.ph337.preheader
	subq	%r9, %rdx
	leaq	(%r8,%r9,4), %rsi
	.p2align	4, 0x90
.LBB0_95:                               # %scalar.ph337
                                        # =>This Inner Loop Header: Depth=1
	movl	%r10d, (%rsi)
	addq	$4, %rsi
	decq	%rdx
	jne	.LBB0_95
.LBB0_100:                              # %._crit_edge24.i
	leal	(%rax,%r13), %edx
	movq	(%r15), %rax
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movslq	%edx, %rsi
	leaq	(%rax,%rsi), %rdx
	movq	%rdx, (%r15)
	movb	$0, (%rax,%rsi)
	movq	(%r14), %rax
	leaq	(%rax,%rsi), %rdx
	movq	%rdx, (%r14)
	movb	$0, (%rax,%rsi)
	testl	%esi, %esi
	movl	$0, %r13d
	movl	$0, %r11d
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	64(%rsp), %r10          # 8-byte Reload
	js	.LBB0_124
# BB#101:                               # %.lr.ph12.i
	movl	localstop(%rip), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
	movq	(%rsi,%rax,8), %rax
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	(%rax,%rsi,4), %eax
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_102:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_112 Depth 2
                                        #     Child Loop BB0_118 Depth 2
	testl	%eax, %eax
	js	.LBB0_103
# BB#104:                               #   in Loop: Header=BB0_102 Depth=1
	je	.LBB0_106
# BB#105:                               #   in Loop: Header=BB0_102 Depth=1
	movq	%r12, %r8
	movl	%r10d, %r13d
	subl	%eax, %r13d
	jmp	.LBB0_107
	.p2align	4, 0x90
.LBB0_103:                              #   in Loop: Header=BB0_102 Depth=1
	movq	%r12, %r8
	leal	-1(%r10), %r13d
	jmp	.LBB0_108
	.p2align	4, 0x90
.LBB0_106:                              #   in Loop: Header=BB0_102 Depth=1
	movq	%r12, %r8
	leal	-1(%r10), %r13d
.LBB0_107:                              #   in Loop: Header=BB0_102 Depth=1
	movl	$-1, %eax
.LBB0_108:                              #   in Loop: Header=BB0_102 Depth=1
	movl	%r10d, %ecx
	subl	%r13d, %ecx
	decl	%ecx
	je	.LBB0_114
# BB#109:                               # %.lr.ph.preheader.i
                                        #   in Loop: Header=BB0_102 Depth=1
	movq	%r10, %rsi
	movslq	%ecx, %rdx
	movslq	%r13d, %rbp
	leal	-2(%rsi), %r12d
	testb	$1, %dl
	je	.LBB0_111
# BB#110:                               # %.lr.ph.i219.prol
                                        #   in Loop: Header=BB0_102 Depth=1
	movq	(%rdi), %r11
	leaq	(%rdx,%rbp), %rbx
	movb	(%r11,%rbx), %r11b
	movq	(%r15), %rbx
	leaq	-1(%rbx), %rcx
	movq	%rcx, (%r15)
	movb	%r11b, -1(%rbx)
	movq	(%r14), %rcx
	leaq	-1(%rcx), %rbx
	movq	%rbx, (%r14)
	movb	$45, -1(%rcx)
	decq	%rdx
.LBB0_111:                              # %.lr.ph.i219.prol.loopexit
                                        #   in Loop: Header=BB0_102 Depth=1
	decl	%r9d
	cmpl	%r13d, %r12d
	je	.LBB0_113
	.p2align	4, 0x90
.LBB0_112:                              # %.lr.ph.i219
                                        #   Parent Loop BB0_102 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi), %rcx
	addq	%rbp, %rcx
	movzbl	(%rdx,%rcx), %ecx
	movq	(%r15), %rbx
	leaq	-1(%rbx), %rsi
	movq	%rsi, (%r15)
	movb	%cl, -1(%rbx)
	movq	(%r14), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r14)
	movb	$45, -1(%rcx)
	movq	(%rdi), %rcx
	addq	%rbp, %rcx
	movzbl	-1(%rdx,%rcx), %ecx
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rbx
	movq	%rbx, (%r15)
	movb	%cl, -1(%rsi)
	movq	(%r14), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r14)
	movb	$45, -1(%rcx)
	addq	$-2, %rdx
	testl	%edx, %edx
	jne	.LBB0_112
.LBB0_113:                              # %._crit_edge.loopexit.i
                                        #   in Loop: Header=BB0_102 Depth=1
	addl	%r10d, %r9d
	subl	%r13d, %r9d
	movq	72(%rsp), %rbp          # 8-byte Reload
.LBB0_114:                              # %._crit_edge.i
                                        #   in Loop: Header=BB0_102 Depth=1
	movq	%r8, %r12
	leal	(%rax,%r12), %r11d
	cmpl	$-1, %eax
	je	.LBB0_120
# BB#115:                               # %.lr.ph5.preheader.i
                                        #   in Loop: Header=BB0_102 Depth=1
	movl	%eax, %r8d
	notl	%r8d
	movslq	%r8d, %rbx
	movslq	%r11d, %rcx
	testb	$1, %bl
	je	.LBB0_117
# BB#116:                               # %.lr.ph5.i.prol
                                        #   in Loop: Header=BB0_102 Depth=1
	movq	(%r15), %rsi
	leaq	-1(%rsi), %rdx
	movq	%rdx, (%r15)
	movb	$45, -1(%rsi)
	movq	(%rbp), %rdx
	leaq	(%rbx,%rcx), %rsi
	movq	%r12, %rdi
	movb	(%rdx,%rsi), %r12b
	movq	(%r14), %rsi
	leaq	-1(%rsi), %rdx
	movq	%rdx, (%r14)
	movb	%r12b, -1(%rsi)
	movq	%rdi, %r12
	movq	72(%rsp), %rbp          # 8-byte Reload
	decq	%rbx
.LBB0_117:                              # %.lr.ph5.i.prol.loopexit
                                        #   in Loop: Header=BB0_102 Depth=1
	cmpl	$-2, %eax
	je	.LBB0_119
	.p2align	4, 0x90
.LBB0_118:                              # %.lr.ph5.i
                                        #   Parent Loop BB0_102 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%r15)
	movb	$45, -1(%rax)
	movq	(%rbp), %rax
	addq	%rcx, %rax
	movzbl	(%rbx,%rax), %eax
	movq	(%r14), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r14)
	movb	%al, -1(%rdx)
	movq	(%r15), %rax
	leaq	-1(%rax), %rdx
	movq	%rdx, (%r15)
	movb	$45, -1(%rax)
	movq	(%rbp), %rax
	addq	%rcx, %rax
	movzbl	-1(%rbx,%rax), %eax
	movq	(%r14), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r14)
	movb	%al, -1(%rdx)
	addq	$-2, %rbx
	testl	%ebx, %ebx
	jne	.LBB0_118
.LBB0_119:                              # %._crit_edge6.loopexit.i
                                        #   in Loop: Header=BB0_102 Depth=1
	addl	%r8d, %r9d
	movq	40(%rsp), %rdi          # 8-byte Reload
.LBB0_120:                              # %._crit_edge6.i
                                        #   in Loop: Header=BB0_102 Depth=1
	testl	%r10d, %r10d
	jle	.LBB0_124
# BB#121:                               # %._crit_edge6.i
                                        #   in Loop: Header=BB0_102 Depth=1
	testl	%r12d, %r12d
	jle	.LBB0_124
# BB#122:                               #   in Loop: Header=BB0_102 Depth=1
	movq	(%rdi), %rcx
	movslq	%r13d, %rax
	movb	(%rcx,%rax), %cl
	movq	(%r15), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15)
	movb	%cl, -1(%rdx)
	movq	(%rbp), %rdx
	movslq	%r11d, %rcx
	movb	(%rdx,%rcx), %dl
	movq	(%r14), %rsi
	leaq	-1(%rsi), %rbx
	movq	%rbx, (%r14)
	movb	%dl, -1(%rsi)
	addl	$2, %r9d
	cmpl	8(%rsp), %r9d           # 4-byte Folded Reload
	jg	.LBB0_124
# BB#123:                               #   in Loop: Header=BB0_102 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rax,8), %rax
	movl	(%rax,%rcx,4), %eax
	cmpl	16(%rsp), %eax          # 4-byte Folded Reload
	movl	%r13d, %r10d
	movl	%r11d, %r12d
	jne	.LBB0_102
.LBB0_124:                              # %Ltracking.exit
	cmpl	$-1, %r13d
	movl	$0, %ecx
	cmovel	%ecx, %r13d
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	cmpl	$-1, %r11d
	cmovel	%ecx, %r11d
	movq	104(%rsp), %rax         # 8-byte Reload
	movl	%r11d, (%rax)
	movq	(%r15), %rbx
	movq	%rbx, %rdi
	callq	strlen
	movq	%rax, %rcx
	movl	88(%rsp), %edx          # 4-byte Reload
	cmpl	%edx, %ecx
	jg	.LBB0_126
# BB#125:                               # %Ltracking.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB0_126
.LBB0_127:
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	(%rbp), %rdi
	movq	L__align11.mseq2(%rip), %rax
	movq	(%rax), %rsi
	callq	strcpy
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB0_128:
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_57:                               # %.lr.ph.us.preheader
	movl	%eax, L__align11.mi(%rip)
	movd	%eax, %xmm1
	movq	8(%rdx,%r14,8), %rbp
	xorl	%r8d, %r8d
	movl	$-1, %esi
	xorl	%ebx, %ebx
	movdqa	%xmm1, %xmm3
	xorl	%eax, %eax
	movl	8(%rsp), %r11d          # 4-byte Reload
	jmp	.LBB0_58
	.p2align	4, 0x90
.LBB0_66:                               # %..lr.ph.us_crit_edge
                                        #   in Loop: Header=BB0_58 Depth=1
	addss	4(%r9,%rbx,4), %xmm2
	movss	%xmm2, 4(%r9,%rbx,4)
	movss	4(%rdi,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	incq	%rbx
	decl	%esi
.LBB0_58:                               # %.lr.ph.us
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	leal	(%rax,%rsi), %ecx
	ucomiss	%xmm3, %xmm2
	cmovbel	%r8d, %ecx
	maxss	%xmm3, %xmm2
	movl	%ecx, 4(%rbp,%rbx,4)
	ucomiss	%xmm1, %xmm3
	jbe	.LBB0_60
# BB#59:                                #   in Loop: Header=BB0_58 Depth=1
	movss	%xmm3, L__align11.mi(%rip)
	movaps	%xmm3, %xmm1
	movl	%ebx, %eax
.LBB0_60:                               #   in Loop: Header=BB0_58 Depth=1
	addss	%xmm6, %xmm1
	movss	%xmm1, L__align11.mi(%rip)
	movss	4(%r13,%rbx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	%xmm2, %xmm4
	jbe	.LBB0_62
# BB#61:                                #   in Loop: Header=BB0_58 Depth=1
	movl	4(%r10,%rbx,4), %ecx
	movl	%r14d, %edx
	subl	%ecx, %edx
	incl	%edx
	movl	%edx, 4(%rbp,%rbx,4)
	movaps	%xmm4, %xmm2
.LBB0_62:                               #   in Loop: Header=BB0_58 Depth=1
	movss	(%rdi,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm4
	jbe	.LBB0_64
# BB#63:                                #   in Loop: Header=BB0_58 Depth=1
	movss	%xmm4, 4(%r13,%rbx,4)
	movl	%r14d, 4(%r10,%rbx,4)
	movaps	%xmm4, %xmm3
.LBB0_64:                               #   in Loop: Header=BB0_58 Depth=1
	addss	%xmm6, %xmm3
	movss	%xmm3, 4(%r13,%rbx,4)
	ucomiss	%xmm2, %xmm0
	jbe	.LBB0_66
# BB#65:                                #   in Loop: Header=BB0_58 Depth=1
	movl	%r11d, 4(%rbp,%rbx,4)
	movaps	%xmm0, %xmm2
	jmp	.LBB0_66
.LBB0_41:                               # %vector.body.preheader
	leaq	16(%r11), %rax
	leaq	20(%rdx), %rdi
	leaq	20(%rsi), %rbp
	xorpd	%xmm0, %xmm0
	leaq	1(%r12), %rcx
	.p2align	4, 0x90
.LBB0_42:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movupd	-16(%rax), %xmm1
	movups	(%rax), %xmm2
	movupd	%xmm1, -16(%rdi)
	movups	%xmm2, (%rdi)
	movupd	%xmm0, -16(%rbp)
	movupd	%xmm0, (%rbp)
	addq	$32, %rax
	addq	$32, %rdi
	addq	$32, %rbp
	addq	$-8, %r12
	jne	.LBB0_42
# BB#43:                                # %middle.block
	testq	%r10, %r10
	movq	24(%rsp), %rdi          # 8-byte Reload
	jne	.LBB0_31
	jmp	.LBB0_44
.LBB0_126:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.2, %edi
	callq	ErrorExit
	movq	L__align11.mseq1(%rip), %rax
	movq	(%rax), %rbx
	jmp	.LBB0_127
.Lfunc_end0:
	.size	L__align11, .Lfunc_end0-L__align11
	.cfi_endproc

	.type	L__align11.mi,@object   # @L__align11.mi
	.local	L__align11.mi
	.comm	L__align11.mi,4,4
	.type	L__align11.m,@object    # @L__align11.m
	.local	L__align11.m
	.comm	L__align11.m,8,8
	.type	L__align11.ijp,@object  # @L__align11.ijp
	.local	L__align11.ijp
	.comm	L__align11.ijp,8,8
	.type	L__align11.mpi,@object  # @L__align11.mpi
	.local	L__align11.mpi
	.comm	L__align11.mpi,4,4
	.type	L__align11.mp,@object   # @L__align11.mp
	.local	L__align11.mp
	.comm	L__align11.mp,8,8
	.type	L__align11.w1,@object   # @L__align11.w1
	.local	L__align11.w1
	.comm	L__align11.w1,8,8
	.type	L__align11.w2,@object   # @L__align11.w2
	.local	L__align11.w2
	.comm	L__align11.w2,8,8
	.type	L__align11.match,@object # @L__align11.match
	.local	L__align11.match
	.comm	L__align11.match,8,8
	.type	L__align11.initverticalw,@object # @L__align11.initverticalw
	.local	L__align11.initverticalw
	.comm	L__align11.initverticalw,8,8
	.type	L__align11.lastverticalw,@object # @L__align11.lastverticalw
	.local	L__align11.lastverticalw
	.comm	L__align11.lastverticalw,8,8
	.type	L__align11.mseq1,@object # @L__align11.mseq1
	.local	L__align11.mseq1
	.comm	L__align11.mseq1,8,8
	.type	L__align11.mseq2,@object # @L__align11.mseq2
	.local	L__align11.mseq2
	.comm	L__align11.mseq2,8,8
	.type	L__align11.mseq,@object # @L__align11.mseq
	.local	L__align11.mseq
	.comm	L__align11.mseq,8,8
	.type	L__align11.orlgth1,@object # @L__align11.orlgth1
	.local	L__align11.orlgth1
	.comm	L__align11.orlgth1,4,4
	.type	L__align11.orlgth2,@object # @L__align11.orlgth2
	.local	L__align11.orlgth2
	.comm	L__align11.orlgth2,4,4
	.type	localstop,@object       # @localstop
	.local	localstop
	.comm	localstop,4,4
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str.1, 33

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.2, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
