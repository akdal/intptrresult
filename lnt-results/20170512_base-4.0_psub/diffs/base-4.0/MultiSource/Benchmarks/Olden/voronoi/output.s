	.text
	.file	"output.bc"
	.globl	plot_dedge
	.p2align	4, 0x90
	.type	plot_dedge,@function
plot_dedge:                             # @plot_dedge
	.cfi_startproc
# BB#0:
	movsd	(%rdi), %xmm0           # xmm0 = mem[0],zero
	movsd	8(%rdi), %xmm1          # xmm1 = mem[0],zero
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	movsd	8(%rsi), %xmm3          # xmm3 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm2
	cvtss2sd	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm3
	cvtss2sd	%xmm3, %xmm3
	movl	$.L.str, %edi
	movb	$4, %al
	jmp	printf                  # TAILCALL
.Lfunc_end0:
	.size	plot_dedge, .Lfunc_end0-plot_dedge
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	plot_vedge
	.p2align	4, 0x90
	.type	plot_vedge,@function
plot_vedge:                             # @plot_vedge
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Lcfi0:
	.cfi_def_cfa_offset 80
	movsd	80(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	88(%rsp), %xmm1         # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	104(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, (%rsp)           # 8-byte Spill
	movsd	112(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rsp)         # 8-byte Spill
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	movaps	%xmm2, %xmm0
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB1_2
# BB#1:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	andps	.LCPI1_0(%rip), %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
.LBB1_2:
	movsd	(%rsp), %xmm0           # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB1_4
# BB#3:
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	andps	.LCPI1_0(%rip), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
.LBB1_4:
	movsd	16(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movaps	(%rsp), %xmm0           # 16-byte Reload
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB1_6
# BB#5:
	movaps	(%rsp), %xmm0           # 16-byte Reload
	andps	.LCPI1_0(%rip), %xmm0
	movaps	%xmm0, (%rsp)           # 16-byte Spill
.LBB1_6:
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB1_7
# BB#8:
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	andps	.LCPI1_0(%rip), %xmm3
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	jmp	.LBB1_9
.LBB1_7:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movaps	(%rsp), %xmm2           # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
.LBB1_9:
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm3
	movl	$.L.str.1, %edi
	movb	$4, %al
	addq	$72, %rsp
	jmp	printf                  # TAILCALL
.Lfunc_end1:
	.size	plot_vedge, .Lfunc_end1-plot_vedge
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4602678819172646912     # double 0.5
.LCPI2_1:
	.quad	-4611686018427387904    # double -2
	.text
	.globl	circle_center
	.p2align	4, 0x90
	.type	circle_center,@function
circle_center:                          # @circle_center
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 40
	subq	$376, %rsp              # imm = 0x178
.Lcfi5:
	.cfi_def_cfa_offset 416
.Lcfi6:
	.cfi_offset %rbx, -40
.Lcfi7:
	.cfi_offset %r12, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movq	480(%rsp), %rax
	movq	%rax, 40(%rsp)
	movaps	464(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	456(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	440(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	352(%rsp), %rdi
	callq	V2_sub
	movq	368(%rsp), %rax
	movq	%rax, 16(%rsp)
	movupd	352(%rsp), %xmm0
	movupd	%xmm0, (%rsp)
	callq	V2_magn
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movq	456(%rsp), %rax
	movq	%rax, 40(%rsp)
	movups	440(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	432(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	416(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	328(%rsp), %rdi
	callq	V2_sum
	movq	344(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	328(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	304(%rsp), %rdi
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	V2_times
	movq	320(%rsp), %rax
	movq	%rax, 112(%rsp)
	movups	304(%rsp), %xmm0
	movaps	%xmm0, 96(%rsp)
	xorps	%xmm0, %xmm0
	ucomisd	56(%rsp), %xmm0         # 8-byte Folded Reload
	jbe	.LBB2_2
# BB#1:
	movq	112(%rsp), %rax
	movq	%rax, 16(%r12)
	movaps	96(%rsp), %xmm0
	jmp	.LBB2_3
.LBB2_2:
	leaq	464(%rsp), %r14
	leaq	440(%rsp), %r15
	leaq	416(%rsp), %rbx
	movq	16(%rbx), %rax
	movq	%rax, 40(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	16(%r15), %rax
	movq	%rax, 16(%rsp)
	movups	(%r15), %xmm0
	movups	%xmm0, (%rsp)
	leaq	280(%rsp), %rdi
	callq	V2_sub
	movq	296(%rsp), %rax
	movq	%rax, 80(%rsp)
	movups	280(%rsp), %xmm0
	movaps	%xmm0, 64(%rsp)
	movq	16(%rbx), %rax
	movq	%rax, 40(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	16(%r14), %rax
	movq	%rax, 16(%rsp)
	movups	(%r14), %xmm0
	movups	%xmm0, (%rsp)
	leaq	136(%rsp), %rdi
	callq	V2_sub
	movq	152(%rsp), %rax
	movq	%rax, 176(%rsp)
	movups	136(%rsp), %xmm0
	movaps	%xmm0, 160(%rsp)
	movq	152(%rsp), %rax
	movq	%rax, 40(%rsp)
	movups	136(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	80(%rsp), %rax
	movq	%rax, 16(%rsp)
	movapd	64(%rsp), %xmm0
	movupd	%xmm0, (%rsp)
	callq	V2_cprod
	mulsd	.LCPI2_1(%rip), %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movq	16(%r15), %rax
	movq	%rax, 40(%rsp)
	movups	(%r15), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	16(%r14), %rax
	movq	%rax, 16(%rsp)
	movups	(%r14), %xmm0
	movups	%xmm0, (%rsp)
	leaq	256(%rsp), %rdi
	callq	V2_sub
	movq	176(%rsp), %rax
	movq	%rax, 40(%rsp)
	movaps	160(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	272(%rsp), %rax
	movq	%rax, 16(%rsp)
	movupd	256(%rsp), %xmm0
	movupd	%xmm0, (%rsp)
	callq	V2_dot
	movsd	%xmm0, 128(%rsp)        # 8-byte Spill
	movq	80(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	64(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	232(%rsp), %rdi
	callq	V2_cross
	movsd	128(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	divsd	56(%rsp), %xmm0         # 8-byte Folded Reload
	movq	248(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	232(%rsp), %xmm1
	movups	%xmm1, (%rsp)
	leaq	208(%rsp), %rdi
	callq	V2_times
	movq	224(%rsp), %rax
	movq	%rax, 40(%rsp)
	movups	208(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	112(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	96(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	184(%rsp), %rdi
	callq	V2_sum
	movq	200(%rsp), %rax
	movq	%rax, 16(%r12)
	movups	184(%rsp), %xmm0
.LBB2_3:
	movups	%xmm0, (%r12)
	movq	%r12, %rax
	addq	$376, %rsp              # imm = 0x178
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	circle_center, .Lfunc_end2-circle_center
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4602678819172646912     # double 0.5
.LCPI3_1:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	output_voronoi_diagram
	.p2align	4, 0x90
	.type	output_voronoi_diagram,@function
output_voronoi_diagram:                 # @output_voronoi_diagram
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 56
	subq	$616, %rsp              # imm = 0x268
.Lcfi16:
	.cfi_def_cfa_offset 672
.Lcfi17:
	.cfi_offset %rbx, -56
.Lcfi18:
	.cfi_offset %r12, -48
.Lcfi19:
	.cfi_offset %r13, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rdi, %r15
	cmpl	$0, voronoi(%rip)
	je	.LBB3_12
# BB#1:
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%r15, %rbx
	movq	%rbx, %rsi
	callq	zero_seen
	leaq	416(%rsp), %r12
	leaq	392(%rsp), %r14
	leaq	544(%rsp), %r13
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rbx, %rax
	xorq	$64, %rax
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 192(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 176(%rsp)
	movq	(%rbx), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 144(%rsp)
	movq	8(%rbx), %rax
	xorq	$64, %rax
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 528(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 512(%rsp)
	movq	160(%rsp), %rax
	movq	%rax, 40(%rsp)
	movaps	144(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	192(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	176(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	592(%rsp), %rdi
	callq	V2_sub
	movq	608(%rsp), %rax
	movq	%rax, 368(%rsp)
	movups	592(%rsp), %xmm0
	movaps	%xmm0, 352(%rsp)
	movq	528(%rsp), %rax
	movq	%rax, 64(%rsp)
	movaps	512(%rsp), %xmm0
	movups	%xmm0, 48(%rsp)
	movq	192(%rsp), %rax
	movq	%rax, 40(%rsp)
	movaps	176(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	160(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	144(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	568(%rsp), %rdi
	callq	circle_center
	movq	584(%rsp), %rax
	movq	%rax, 240(%rsp)
	movups	568(%rsp), %xmm0
	movaps	%xmm0, 224(%rsp)
	movq	192(%rsp), %rax
	movq	%rax, 40(%rsp)
	movaps	176(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	160(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	144(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	488(%rsp), %rdi
	callq	V2_sum
	movq	504(%rsp), %rax
	movq	%rax, 304(%rsp)
	movups	488(%rsp), %xmm0
	movaps	%xmm0, 288(%rsp)
	movq	504(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	488(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	464(%rsp), %rdi
	movsd	.LCPI3_0(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	V2_times
	movq	480(%rsp), %rax
	movq	%rax, 272(%rsp)
	movups	464(%rsp), %xmm0
	movaps	%xmm0, 256(%rsp)
	movq	480(%rsp), %rax
	movq	%rax, 40(%rsp)
	movups	464(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	240(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	224(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	440(%rsp), %rdi
	callq	V2_sub
	movq	456(%rsp), %rax
	movq	%rax, 336(%rsp)
	movups	440(%rsp), %xmm0
	movaps	%xmm0, 320(%rsp)
	movq	456(%rsp), %rax
	movq	%rax, 16(%rsp)
	movupd	440(%rsp), %xmm0
	movupd	%xmm0, (%rsp)
	callq	V2_magn
	addsd	.LCPI3_1(%rip), %xmm0
	movsd	%xmm0, 112(%rsp)        # 8-byte Spill
	movq	368(%rsp), %rax
	movq	%rax, 16(%rsp)
	movapd	352(%rsp), %xmm0
	movupd	%xmm0, (%rsp)
	callq	V2_magn
	movsd	112(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, 112(%rsp)        # 8-byte Spill
	movq	368(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	352(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	movq	%r12, %rdi
	callq	V2_cross
	movq	432(%rsp), %rax
	movq	%rax, 304(%rsp)
	movups	416(%rsp), %xmm0
	movaps	%xmm0, 288(%rsp)
	movq	432(%rsp), %rax
	movq	%rax, 16(%rsp)
	movups	416(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	movq	%r14, %rdi
	movsd	112(%rsp), %xmm0        # 8-byte Reload
                                        # xmm0 = mem[0],zero
	callq	V2_times
	movq	408(%rsp), %rax
	movq	%rax, 272(%rsp)
	movups	392(%rsp), %xmm0
	movaps	%xmm0, 256(%rsp)
	movq	408(%rsp), %rax
	movq	%rax, 40(%rsp)
	movups	392(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	240(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	224(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	movq	%r13, %rdi
	callq	V2_sum
	movq	560(%rsp), %rax
	movq	%rax, 336(%rsp)
	movups	544(%rsp), %xmm0
	movaps	%xmm0, 320(%rsp)
	movsd	320(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movsd	328(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 128(%rsp)        # 8-byte Spill
	movsd	224(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	232(%rsp), %xmm1        # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movaps	%xmm2, %xmm0
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_4
# BB#3:                                 #   in Loop: Header=BB3_2 Depth=1
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm0
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
.LBB3_4:                                #   in Loop: Header=BB3_2 Depth=1
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movsd	80(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_6
# BB#5:                                 #   in Loop: Header=BB3_2 Depth=1
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
.LBB3_6:                                #   in Loop: Header=BB3_2 Depth=1
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movsd	128(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_8
# BB#7:                                 #   in Loop: Header=BB3_2 Depth=1
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
.LBB3_8:                                #   in Loop: Header=BB3_2 Depth=1
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_9
# BB#10:                                #   in Loop: Header=BB3_2 Depth=1
	movaps	128(%rsp), %xmm3        # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm3
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movaps	112(%rsp), %xmm1        # 16-byte Reload
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	jmp	.LBB3_11
	.p2align	4, 0x90
.LBB3_9:                                #   in Loop: Header=BB3_2 Depth=1
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movaps	112(%rsp), %xmm1        # 16-byte Reload
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	movaps	128(%rsp), %xmm3        # 16-byte Reload
.LBB3_11:                               # %plot_vedge.exit
                                        #   in Loop: Header=BB3_2 Depth=1
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm3
	movl	$.L.str.1, %edi
	movb	$4, %al
	callq	printf
	leal	32(%rbx), %eax
	andl	$127, %eax
	andq	$-128, %rbx
	movq	8(%rax,%rbx), %rbx
	leal	96(%rbx), %eax
	andl	$127, %eax
	andq	$-128, %rbx
	orq	%rax, %rbx
	cmpq	%r15, %rbx
	jne	.LBB3_2
.LBB3_12:                               # %.loopexit124
	movl	$0, (%rbp)
	movq	%rbp, %rdi
	movq	%r15, %rbx
	movq	%rbx, %rsi
	callq	push_ring
	movl	(%rbp), %esi
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$0, (%rbp)
	je	.LBB3_30
# BB#13:                                # %.lr.ph
	movq	%rbp, 376(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_14:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_16 Depth 2
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	pop_edge
	movq	%rax, %rbx
	cmpq	$1, 16(%rbx)
	jne	.LBB3_29
# BB#15:                                # %.preheader.preheader
                                        #   in Loop: Header=BB3_14 Depth=1
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 384(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_16:                               # %.preheader
                                        #   Parent Loop BB3_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %r14         # 8-byte Reload
	movq	(%r14), %rax
	movq	8(%r14), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	movq	%r14, %r12
	xorq	$64, %r12
	movq	(%r12), %rcx
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
	ucomisd	%xmm2, %xmm0
	jb	.LBB3_28
# BB#17:                                #   in Loop: Header=BB3_16 Depth=2
	movsd	8(%rax), %xmm1          # xmm1 = mem[0],zero
	movsd	8(%rcx), %xmm3          # xmm3 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	cvtss2sd	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm1
	cvtss2sd	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm2
	cvtss2sd	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm3
	cvtss2sd	%xmm3, %xmm3
	movl	$.L.str, %edi
	movb	$4, %al
	callq	printf
	movq	(%r12), %rbp
	movq	8(%r12), %rax
	movq	(%r14), %rbx
	movq	112(%rsp), %r13         # 8-byte Reload
	xorq	$64, %r13
	movq	(%r13), %rdx
	xorq	$64, %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movq	(%rax), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	ccw
	movl	%eax, %r15d
	movq	%rbx, %rdi
	movq	384(%rsp), %rbx         # 8-byte Reload
	movq	%rbp, %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	callq	ccw
	cmpl	%eax, %r15d
	je	.LBB3_28
# BB#18:                                #   in Loop: Header=BB3_16 Depth=2
	movq	(%r14), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 192(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 176(%rsp)
	movq	(%r12), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 144(%rsp)
	movq	(%r13), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 64(%rsp)
	movups	(%rax), %xmm0
	movups	%xmm0, 48(%rsp)
	movq	160(%rsp), %rax
	movq	%rax, 40(%rsp)
	movaps	144(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	192(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	176(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	288(%rsp), %rdi
	callq	circle_center
	movq	(%r12), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 192(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 176(%rsp)
	movq	(%r14), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 160(%rsp)
	movups	(%rax), %xmm0
	movaps	%xmm0, 144(%rsp)
	movq	96(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rax
	movq	16(%rax), %rcx
	movq	%rcx, 64(%rsp)
	movups	(%rax), %xmm0
	movups	%xmm0, 48(%rsp)
	movq	160(%rsp), %rax
	movq	%rax, 40(%rsp)
	movaps	144(%rsp), %xmm0
	movups	%xmm0, 24(%rsp)
	movq	192(%rsp), %rax
	movq	%rax, 16(%rsp)
	movaps	176(%rsp), %xmm0
	movups	%xmm0, (%rsp)
	leaq	256(%rsp), %rdi
	callq	circle_center
	movsd	256(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 96(%rsp)         # 8-byte Spill
	movsd	264(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 208(%rsp)        # 8-byte Spill
	movsd	288(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	296(%rsp), %xmm1        # xmm1 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movaps	%xmm2, 128(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm0
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_20
# BB#19:                                #   in Loop: Header=BB3_16 Depth=2
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
.LBB3_20:                               #   in Loop: Header=BB3_16 Depth=2
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movsd	96(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	movaps	%xmm1, 96(%rsp)         # 16-byte Spill
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_22
# BB#21:                                #   in Loop: Header=BB3_16 Depth=2
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
.LBB3_22:                               #   in Loop: Header=BB3_16 Depth=2
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movsd	208(%rsp), %xmm1        # 8-byte Reload
                                        # xmm1 = mem[0],zero
	cvtsd2ss	%xmm1, %xmm1
	movaps	%xmm1, 208(%rsp)        # 16-byte Spill
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_24
# BB#23:                                #   in Loop: Header=BB3_16 Depth=2
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm0
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
.LBB3_24:                               #   in Loop: Header=BB3_16 Depth=2
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	callq	__isnanf
	testl	%eax, %eax
	je	.LBB3_25
# BB#26:                                #   in Loop: Header=BB3_16 Depth=2
	movaps	208(%rsp), %xmm3        # 16-byte Reload
	andps	.LCPI3_2(%rip), %xmm3
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	jmp	.LBB3_27
.LBB3_25:                               #   in Loop: Header=BB3_16 Depth=2
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movaps	208(%rsp), %xmm3        # 16-byte Reload
.LBB3_27:                               # %plot_vedge.exit94
                                        #   in Loop: Header=BB3_16 Depth=2
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm2, %xmm2
	cvtss2sd	%xmm3, %xmm3
	movl	$.L.str.1, %edi
	movb	$4, %al
	callq	printf
.LBB3_28:                               #   in Loop: Header=BB3_16 Depth=2
	movq	$2, 16(%r14)
	cmpq	%rbx, 112(%rsp)         # 8-byte Folded Reload
	jne	.LBB3_16
.LBB3_29:                               # %.loopexit
                                        #   in Loop: Header=BB3_14 Depth=1
	movq	%rbx, %rsi
	xorq	$64, %rsi
	movq	376(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rdi
	callq	push_ring
	cmpl	$0, (%rbp)
	jne	.LBB3_14
.LBB3_30:                               # %._crit_edge
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	zero_seen
	addq	$616, %rsp              # imm = 0x268
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	output_voronoi_diagram, .Lfunc_end3-output_voronoi_diagram
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Dedge %g %g %g %g \n"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Vedge %g %g %g %g \n"
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"mystack_ptr = %d\n"
	.size	.L.str.2, 18

	.type	earray,@object          # @earray
	.comm	earray,8,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
