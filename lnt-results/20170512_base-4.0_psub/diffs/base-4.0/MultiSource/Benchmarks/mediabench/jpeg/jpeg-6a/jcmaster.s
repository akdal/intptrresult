	.text
	.file	"jcmaster.bc"
	.globl	jinit_c_master_control
	.p2align	4, 0x90
	.type	jinit_c_master_control,@function
jinit_c_master_control:                 # @jinit_c_master_control
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$2696, %rsp             # imm = 0xA88
.Lcfi6:
	.cfi_def_cfa_offset 2752
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movq	%rdi, %r13
	movq	8(%r13), %rax
	movl	$1, %esi
	movl	$48, %edx
	callq	*(%rax)
	movq	%rax, %r15
	movq	%r15, 424(%r13)
	movl	$pass_startup, %eax
	movd	%rax, %xmm0
	movl	$prepare_for_pass, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%r15)
	movq	$finish_pass_master, 16(%r15)
	movl	$0, 28(%r15)
	movl	44(%r13), %eax
	testl	%eax, %eax
	je	.LBB0_4
# BB#1:
	cmpl	$0, 40(%r13)
	je	.LBB0_4
# BB#2:
	cmpl	$0, 68(%r13)
	jle	.LBB0_4
# BB#3:
	cmpl	$0, 48(%r13)
	jg	.LBB0_5
.LBB0_4:                                # %._crit_edge
	movq	(%r13), %rax
	movl	$31, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	44(%r13), %eax
.LBB0_5:
	cmpl	$65500, %eax            # imm = 0xFFDC
	ja	.LBB0_7
# BB#6:
	movl	40(%r13), %eax
	cmpl	$65501, %eax            # imm = 0xFFDD
	jb	.LBB0_8
.LBB0_7:                                # %._crit_edge116.i
	movq	(%r13), %rax
	movl	$40, 40(%rax)
	movl	$65500, 44(%rax)        # imm = 0xFFDC
	movq	%r13, %rdi
	callq	*(%rax)
	movl	40(%r13), %eax
.LBB0_8:
	movl	%eax, %eax
	movslq	48(%r13), %rcx
	imulq	%rax, %rcx
	movl	%ecx, %eax
	cmpq	%rcx, %rax
	je	.LBB0_10
# BB#9:
	movq	(%r13), %rax
	movl	$69, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_10:
	movl	64(%r13), %eax
	cmpl	$8, %eax
	je	.LBB0_12
# BB#11:
	movq	(%r13), %rcx
	movl	$13, 40(%rcx)
	movl	%eax, 44(%rcx)
	movq	%r13, %rdi
	callq	*(%rcx)
.LBB0_12:
	movl	68(%r13), %ecx
	cmpl	$11, %ecx
	jl	.LBB0_14
# BB#13:
	movq	(%r13), %rax
	movl	$24, 40(%rax)
	movl	%ecx, 44(%rax)
	movl	$10, 48(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	68(%r13), %ecx
.LBB0_14:
	movabsq	$4294967297, %rax       # imm = 0x100000001
	movq	%rax, 304(%r13)
	testl	%ecx, %ecx
	movl	%ebx, 36(%rsp)          # 4-byte Spill
	jle	.LBB0_15
# BB#16:                                # %.lr.ph104.i
	movq	80(%r13), %rbp
	addq	$12, %rbp
	xorl	%r14d, %r14d
	movl	$1, %edx
	movl	$1, %eax
	.p2align	4, 0x90
.LBB0_17:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rbp), %esi
	leal	-1(%rsi), %edi
	cmpl	$3, %edi
	ja	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	movl	(%rbp), %edi
	decl	%edi
	cmpl	$4, %edi
	jb	.LBB0_20
.LBB0_19:                               # %._crit_edge118.i
                                        #   in Loop: Header=BB0_17 Depth=1
	movq	(%r13), %rax
	movl	$16, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	68(%r13), %ecx
	movl	304(%r13), %eax
	movl	-4(%rbp), %esi
	movl	308(%r13), %edx
.LBB0_20:                               #   in Loop: Header=BB0_17 Depth=1
	movl	%eax, %edi
	movl	%edx, %ebx
	cmpl	%esi, %edi
	movl	%esi, %eax
	cmovgel	%edi, %eax
	movl	%eax, 304(%r13)
	movl	(%rbp), %edx
	cmpl	%edx, %ebx
	cmovgel	%ebx, %edx
	movl	%edx, 308(%r13)
	incl	%r14d
	addq	$96, %rbp
	cmpl	%ecx, %r14d
	jl	.LBB0_17
# BB#21:                                # %._crit_edge105.i
	testl	%ecx, %ecx
	jle	.LBB0_26
# BB#22:                                # %.lr.ph.preheader.i
	movq	80(%r13), %rbp
	addq	$48, %rbp
	movl	$1, %r14d
	jmp	.LBB0_23
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph..lr.ph_crit_edge.i
                                        #   in Loop: Header=BB0_23 Depth=1
	movl	304(%r13), %eax
	incl	%r14d
	addq	$96, %rbp
.LBB0_23:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%r14), %ecx
	movl	%ecx, -44(%rbp)
	movl	$8, -12(%rbp)
	movl	40(%r13), %ecx
	movslq	-40(%rbp), %rdi
	imulq	%rcx, %rdi
	shll	$3, %eax
	movslq	%eax, %rsi
	callq	jdiv_round_up
	movl	%eax, -20(%rbp)
	movl	44(%r13), %eax
	movslq	-36(%rbp), %rdi
	imulq	%rax, %rdi
	movslq	308(%r13), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, -16(%rbp)
	movl	40(%r13), %eax
	movslq	-40(%rbp), %rdi
	imulq	%rax, %rdi
	movslq	304(%r13), %rsi
	callq	jdiv_round_up
	movl	%eax, -8(%rbp)
	movl	44(%r13), %eax
	movslq	-36(%rbp), %rdi
	imulq	%rax, %rdi
	movslq	308(%r13), %rsi
	callq	jdiv_round_up
	movl	%eax, -4(%rbp)
	movl	$1, (%rbp)
	cmpl	68(%r13), %r14d
	jl	.LBB0_24
# BB#25:                                # %._crit_edge.loopexit.i
	movl	308(%r13), %edx
	jmp	.LBB0_26
.LBB0_15:
	movl	$1, %edx
.LBB0_26:                               # %initial_setup.exit
	movl	44(%r13), %edi
	shll	$3, %edx
	movslq	%edx, %rsi
	callq	jdiv_round_up
	movl	%eax, 312(%r13)
	movq	240(%r13), %r12
	testq	%r12, %r12
	je	.LBB0_136
# BB#27:
	cmpl	$0, 232(%r13)
	jg	.LBB0_29
# BB#28:
	movq	(%r13), %rax
	movl	$17, 40(%rax)
	movl	$0, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movq	240(%r13), %r12
.LBB0_29:
	movq	%r15, 72(%rsp)          # 8-byte Spill
	cmpl	$0, 20(%r12)
	jne	.LBB0_31
# BB#30:
	cmpl	$63, 24(%r12)
	jne	.LBB0_31
# BB#39:
	movl	$0, 300(%r13)
	movl	68(%r13), %r15d
	testl	%r15d, %r15d
	jle	.LBB0_40
# BB#41:                                # %.lr.ph240.i
	leal	-1(%r15), %eax
	leaq	4(,%rax,4), %rdx
	leaq	80(%rsp), %rdi
	xorl	%r14d, %r14d
	xorl	%esi, %esi
	jmp	.LBB0_33
.LBB0_31:
	movl	$1, 300(%r13)
	movl	68(%r13), %r15d
	movl	$1, %r14d
	testl	%r15d, %r15d
	jle	.LBB0_34
# BB#32:                                # %.preheader212.i.preheader
	leal	-1(%r15), %edx
	shlq	$8, %rdx
	addq	$256, %rdx              # imm = 0x100
	leaq	128(%rsp), %rdi
	movl	$255, %esi
.LBB0_33:                               # %.preheader211.i
	callq	memset
.LBB0_34:                               # %.preheader211.i
	cmpl	$0, 232(%r13)
	jle	.LBB0_118
.LBB0_35:                               # %.lr.ph236.i
	movl	$1, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	.p2align	4, 0x90
.LBB0_36:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_43 Depth 2
                                        #     Child Loop BB0_65 Depth 2
                                        #       Child Loop BB0_102 Depth 3
                                        #       Child Loop BB0_104 Depth 3
                                        #       Child Loop BB0_71 Depth 3
                                        #       Child Loop BB0_97 Depth 3
                                        #     Child Loop BB0_76 Depth 2
                                        #       Child Loop BB0_80 Depth 3
                                        #       Child Loop BB0_82 Depth 3
                                        #       Child Loop BB0_87 Depth 3
                                        #       Child Loop BB0_92 Depth 3
                                        #     Child Loop BB0_113 Depth 2
	movl	(%r12), %r14d
	leal	-1(%r14), %eax
	cmpl	$4, %eax
	movq	%r14, 56(%rsp)          # 8-byte Spill
	jb	.LBB0_42
# BB#37:                                # %.preheader210.i
                                        #   in Loop: Header=BB0_36 Depth=1
	movq	(%r13), %rax
	movl	$24, 40(%rax)
	movl	%r14d, 44(%rax)
	movl	$4, 48(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	testl	%r14d, %r14d
	jle	.LBB0_38
.LBB0_42:                               # %.lr.ph218.preheader.i
                                        #   in Loop: Header=BB0_36 Depth=1
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_43:                               # %.lr.ph218.i
                                        #   Parent Loop BB0_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	4(%r12,%rbp,4), %ebx
	testl	%ebx, %ebx
	js	.LBB0_45
# BB#44:                                #   in Loop: Header=BB0_43 Depth=2
	cmpl	68(%r13), %ebx
	jl	.LBB0_46
.LBB0_45:                               #   in Loop: Header=BB0_43 Depth=2
	movq	(%r13), %rax
	movl	$17, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_46:                               #   in Loop: Header=BB0_43 Depth=2
	testq	%rbp, %rbp
	jle	.LBB0_49
# BB#47:                                #   in Loop: Header=BB0_43 Depth=2
	cmpl	(%r12,%rbp,4), %ebx
	jg	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_43 Depth=2
	movq	(%r13), %rax
	movl	$17, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_49:                               #   in Loop: Header=BB0_43 Depth=2
	incq	%rbp
	cmpq	%rbp, %r14
	jne	.LBB0_43
# BB#50:                                #   in Loop: Header=BB0_36 Depth=1
	movb	$1, %r15b
.LBB0_51:                               # %._crit_edge.i
                                        #   in Loop: Header=BB0_36 Depth=1
	movslq	20(%r12), %rbp
	movslq	24(%r12), %rcx
	movl	28(%r12), %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	32(%r12), %r14d
	cmpl	$0, 300(%r13)
	je	.LBB0_108
# BB#52:                                #   in Loop: Header=BB0_36 Depth=1
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	cmpl	$63, %ebp
	jbe	.LBB0_53
.LBB0_57:                               #   in Loop: Header=BB0_36 Depth=1
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	testl	%ebp, %ebp
	jne	.LBB0_60
	jmp	.LBB0_59
	.p2align	4, 0x90
.LBB0_108:                              #   in Loop: Header=BB0_36 Depth=1
	cmpl	$63, %ecx
	jne	.LBB0_110
# BB#109:                               #   in Loop: Header=BB0_36 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	orl	%ebp, %eax
	orl	%r14d, %eax
	je	.LBB0_111
.LBB0_110:                              #   in Loop: Header=BB0_36 Depth=1
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_111:                              # %.preheader206.i
                                        #   in Loop: Header=BB0_36 Depth=1
	testb	%r15b, %r15b
	je	.LBB0_116
# BB#112:                               # %.lr.ph226.preheader.i
                                        #   in Loop: Header=BB0_36 Depth=1
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_113:                              # %.lr.ph226.i
                                        #   Parent Loop BB0_36 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	4(%r12,%rbx,4), %rbp
	cmpl	$0, 80(%rsp,%rbp,4)
	je	.LBB0_115
# BB#114:                               #   in Loop: Header=BB0_113 Depth=2
	movq	(%r13), %rax
	movl	$17, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_115:                              #   in Loop: Header=BB0_113 Depth=2
	movl	$1, 80(%rsp,%rbp,4)
	incq	%rbx
	cmpq	%rbx, 56(%rsp)          # 8-byte Folded Reload
	jne	.LBB0_113
	jmp	.LBB0_116
	.p2align	4, 0x90
.LBB0_53:                               #   in Loop: Header=BB0_36 Depth=1
	cmpl	$13, %r14d
	ja	.LBB0_57
# BB#54:                                #   in Loop: Header=BB0_36 Depth=1
	cmpl	$13, 24(%rsp)           # 4-byte Folded Reload
	ja	.LBB0_57
# BB#55:                                #   in Loop: Header=BB0_36 Depth=1
	cmpl	%ebp, 8(%rsp)           # 4-byte Folded Reload
	jl	.LBB0_57
# BB#56:                                #   in Loop: Header=BB0_36 Depth=1
	cmpl	$64, 8(%rsp)            # 4-byte Folded Reload
	jge	.LBB0_57
# BB#58:                                #   in Loop: Header=BB0_36 Depth=1
	testl	%ebp, %ebp
	je	.LBB0_59
.LBB0_60:                               #   in Loop: Header=BB0_36 Depth=1
	cmpl	$1, 56(%rsp)            # 4-byte Folded Reload
	jne	.LBB0_61
	jmp	.LBB0_62
	.p2align	4, 0x90
.LBB0_59:                               #   in Loop: Header=BB0_36 Depth=1
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB0_62
.LBB0_61:                               #   in Loop: Header=BB0_36 Depth=1
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_62:                               # %.preheader208.i
                                        #   in Loop: Header=BB0_36 Depth=1
	testb	%r15b, %r15b
	je	.LBB0_116
# BB#63:                                # %.lr.ph224.i
                                        #   in Loop: Header=BB0_36 Depth=1
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	testl	%ebp, %ebp
	je	.LBB0_75
# BB#64:                                # %.lr.ph224.i.split.preheader
                                        #   in Loop: Header=BB0_36 Depth=1
	leaq	-1(%rbp), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_65:                               # %.lr.ph224.i.split
                                        #   Parent Loop BB0_36 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_102 Depth 3
                                        #       Child Loop BB0_104 Depth 3
                                        #       Child Loop BB0_71 Depth 3
                                        #       Child Loop BB0_97 Depth 3
	movslq	4(%r12,%r15,4), %rbx
	shlq	$8, %rbx
	cmpl	$0, 128(%rsp,%rbx)
	jns	.LBB0_67
# BB#66:                                #   in Loop: Header=BB0_65 Depth=2
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB0_67:                               # %.preheader205.i
                                        #   in Loop: Header=BB0_65 Depth=2
	cmpl	%eax, %ebp
	jg	.LBB0_107
# BB#68:                                # %.lr.ph220.i
                                        #   in Loop: Header=BB0_65 Depth=2
	cmpl	52(%rsp), %r14d         # 4-byte Folded Reload
	jne	.LBB0_100
# BB#69:                                # %.lr.ph220.split.us.i
                                        #   in Loop: Header=BB0_65 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_96
# BB#70:                                # %.lr.ph220.split.us.split.i.preheader
                                        #   in Loop: Header=BB0_65 Depth=2
	leaq	128(%rsp,%rbx), %rbp
	movq	16(%rsp), %rbx          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_71:                               # %.lr.ph220.split.us.split.i
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	4(%rbp,%rbx,4), %eax
	testl	%eax, %eax
	js	.LBB0_73
# BB#72:                                #   in Loop: Header=BB0_71 Depth=3
	cmpl	%eax, 24(%rsp)          # 4-byte Folded Reload
	je	.LBB0_74
.LBB0_73:                               #   in Loop: Header=BB0_71 Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_74:                               #   in Loop: Header=BB0_71 Depth=3
	movl	%r14d, 4(%rbp,%rbx,4)
	incq	%rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	%rax, %rbx
	jl	.LBB0_71
	jmp	.LBB0_107
	.p2align	4, 0x90
.LBB0_100:                              # %.lr.ph220.split.i
                                        #   in Loop: Header=BB0_65 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_103
# BB#101:                               # %.lr.ph220.split.split.i.preheader
                                        #   in Loop: Header=BB0_65 Depth=2
	leaq	128(%rsp,%rbx), %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_102:                              # %.lr.ph220.split.split.i
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%r14d, 4(%rbx,%rbp,4)
	incq	%rbp
	cmpq	%rax, %rbp
	jl	.LBB0_102
	jmp	.LBB0_107
.LBB0_96:                               # %.lr.ph220.split.us.split.us.i.preheader
                                        #   in Loop: Header=BB0_65 Depth=2
	leaq	128(%rsp,%rbx), %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_97:                               # %.lr.ph220.split.us.split.us.i
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, 4(%rbx,%rbp,4)
	jle	.LBB0_99
# BB#98:                                #   in Loop: Header=BB0_97 Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB0_99:                               #   in Loop: Header=BB0_97 Depth=3
	movl	%r14d, 4(%rbx,%rbp,4)
	incq	%rbp
	cmpq	%rax, %rbp
	jl	.LBB0_97
	jmp	.LBB0_107
.LBB0_103:                              # %.lr.ph220.split.split.us.i.preheader
                                        #   in Loop: Header=BB0_65 Depth=2
	leaq	128(%rsp,%rbx), %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_104:                              # %.lr.ph220.split.split.us.i
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_65 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, 4(%rbx,%rbp,4)
	js	.LBB0_106
# BB#105:                               #   in Loop: Header=BB0_104 Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movq	8(%rsp), %rax           # 8-byte Reload
.LBB0_106:                              #   in Loop: Header=BB0_104 Depth=3
	movl	%r14d, 4(%rbx,%rbp,4)
	incq	%rbp
	cmpq	%rax, %rbp
	jl	.LBB0_104
	.p2align	4, 0x90
.LBB0_107:                              # %._crit_edge221.i
                                        #   in Loop: Header=BB0_65 Depth=2
	incq	%r15
	cmpq	56(%rsp), %r15          # 8-byte Folded Reload
	movq	40(%rsp), %rbp          # 8-byte Reload
	jne	.LBB0_65
	jmp	.LBB0_116
.LBB0_38:                               #   in Loop: Header=BB0_36 Depth=1
	xorl	%r15d, %r15d
	jmp	.LBB0_51
.LBB0_75:                               # %.lr.ph224.i.split.us.preheader
                                        #   in Loop: Header=BB0_36 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movq	%r12, 64(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_76:                               # %.lr.ph224.i.split.us
                                        #   Parent Loop BB0_36 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_80 Depth 3
                                        #       Child Loop BB0_82 Depth 3
                                        #       Child Loop BB0_87 Depth 3
                                        #       Child Loop BB0_92 Depth 3
	testl	%eax, %eax
	js	.LBB0_95
# BB#77:                                # %.lr.ph220.i.us
                                        #   in Loop: Header=BB0_76 Depth=2
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movslq	4(%rax,%rcx,4), %rax
	cmpl	52(%rsp), %r14d         # 4-byte Folded Reload
	jne	.LBB0_78
# BB#85:                                # %.lr.ph220.split.us.i.us
                                        #   in Loop: Header=BB0_76 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_91
# BB#86:                                # %.lr.ph220.split.us.split.i.us.preheader
                                        #   in Loop: Header=BB0_76 Depth=2
	shlq	$8, %rax
	leaq	128(%rsp,%rax), %r12
	movq	16(%rsp), %r15          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_87:                               # %.lr.ph220.split.us.split.i.us
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_76 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r12), %eax
	testl	%eax, %eax
	js	.LBB0_89
# BB#88:                                #   in Loop: Header=BB0_87 Depth=3
	cmpl	%eax, 24(%rsp)          # 4-byte Folded Reload
	je	.LBB0_90
.LBB0_89:                               #   in Loop: Header=BB0_87 Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_90:                               #   in Loop: Header=BB0_87 Depth=3
	movl	%r14d, (%r12)
	addq	$4, %r12
	decq	%r15
	jne	.LBB0_87
	jmp	.LBB0_95
	.p2align	4, 0x90
.LBB0_78:                               # %.lr.ph220.split.i.us
                                        #   in Loop: Header=BB0_76 Depth=2
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB0_81
# BB#79:                                # %.lr.ph220.split.split.i.us.preheader
                                        #   in Loop: Header=BB0_76 Depth=2
	shlq	$8, %rax
	leaq	128(%rsp,%rax), %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_80:                               # %.lr.ph220.split.split.i.us
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_76 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	%r14d, (%rbx)
	addq	$4, %rbx
	decq	%rbp
	jne	.LBB0_80
	jmp	.LBB0_95
.LBB0_91:                               # %.lr.ph220.split.us.split.us.i.us.preheader
                                        #   in Loop: Header=BB0_76 Depth=2
	shlq	$8, %rax
	leaq	128(%rsp,%rax), %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_92:                               # %.lr.ph220.split.us.split.us.i.us
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_76 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, (%rbx)
	jle	.LBB0_94
# BB#93:                                #   in Loop: Header=BB0_92 Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_94:                               #   in Loop: Header=BB0_92 Depth=3
	movl	%r14d, (%rbx)
	addq	$4, %rbx
	decq	%rbp
	jne	.LBB0_92
	jmp	.LBB0_95
.LBB0_81:                               # %.lr.ph220.split.split.us.i.us.preheader
                                        #   in Loop: Header=BB0_76 Depth=2
	shlq	$8, %rax
	leaq	128(%rsp,%rax), %rbx
	movq	16(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_82:                               # %.lr.ph220.split.split.us.i.us
                                        #   Parent Loop BB0_36 Depth=1
                                        #     Parent Loop BB0_76 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	$0, (%rbx)
	js	.LBB0_84
# BB#83:                                #   in Loop: Header=BB0_82 Depth=3
	movq	(%r13), %rax
	movl	$15, 40(%rax)
	movq	(%rsp), %rcx            # 8-byte Reload
	movl	%ecx, 44(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
.LBB0_84:                               #   in Loop: Header=BB0_82 Depth=3
	movl	%r14d, (%rbx)
	addq	$4, %rbx
	decq	%rbp
	jne	.LBB0_82
	.p2align	4, 0x90
.LBB0_95:                               # %._crit_edge221.i.us
                                        #   in Loop: Header=BB0_76 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	cmpq	56(%rsp), %rcx          # 8-byte Folded Reload
	movq	64(%rsp), %r12          # 8-byte Reload
	movq	8(%rsp), %rax           # 8-byte Reload
	jne	.LBB0_76
	.p2align	4, 0x90
.LBB0_116:                              # %.loopexit207.i
                                        #   in Loop: Header=BB0_36 Depth=1
	addq	$36, %r12
	movq	(%rsp), %rax            # 8-byte Reload
	cmpl	232(%r13), %eax
	leal	1(%rax), %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, (%rsp)            # 8-byte Spill
	jl	.LBB0_36
# BB#117:                               # %._crit_edge237.loopexit.i
	movl	68(%r13), %r15d
	movl	300(%r13), %r14d
.LBB0_118:                              # %._crit_edge237.i
	testl	%r14d, %r14d
	je	.LBB0_124
# BB#119:                               # %.preheader203.i
	testl	%r15d, %r15d
	movl	36(%rsp), %r14d         # 4-byte Reload
	jle	.LBB0_129
# BB#120:                               # %.lr.ph216.i
	leaq	128(%rsp), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_121:                              # =>This Inner Loop Header: Depth=1
	cmpl	$0, (%rbx)
	jns	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_121 Depth=1
	movq	(%r13), %rax
	movl	$44, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	68(%r13), %r15d
.LBB0_123:                              #   in Loop: Header=BB0_121 Depth=1
	incq	%rbp
	movslq	%r15d, %rax
	addq	$256, %rbx              # imm = 0x100
	cmpq	%rax, %rbp
	jl	.LBB0_121
	jmp	.LBB0_129
.LBB0_136:                              # %.thread
	movl	$0, 300(%r13)
	leaq	232(%r13), %rax
	movl	36(%rsp), %r14d         # 4-byte Reload
	jmp	.LBB0_131
.LBB0_124:                              # %.preheader.i
	testl	%r15d, %r15d
	movl	36(%rsp), %r14d         # 4-byte Reload
	jle	.LBB0_129
# BB#125:                               # %.lr.ph.i29
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_126:                              # =>This Inner Loop Header: Depth=1
	cmpl	$0, 80(%rsp,%rbx,4)
	jne	.LBB0_128
# BB#127:                               #   in Loop: Header=BB0_126 Depth=1
	movq	(%r13), %rax
	movl	$44, 40(%rax)
	movq	%r13, %rdi
	callq	*(%rax)
	movl	68(%r13), %r15d
.LBB0_128:                              #   in Loop: Header=BB0_126 Depth=1
	incq	%rbx
	movslq	%r15d, %rax
	cmpq	%rax, %rbx
	jl	.LBB0_126
.LBB0_129:                              # %.loopexit
	cmpl	$0, 300(%r13)
	movq	72(%rsp), %r15          # 8-byte Reload
	je	.LBB0_132
# BB#130:
	leaq	256(%r13), %rax
.LBB0_131:                              # %.sink.split
	movl	$1, (%rax)
.LBB0_132:
	testl	%r14d, %r14d
	je	.LBB0_134
# BB#133:
	movl	256(%r13), %eax
	cmpl	$1, %eax
	movl	$1, %ecx
	adcl	$0, %ecx
	movl	%ecx, 32(%r15)
	jmp	.LBB0_135
.LBB0_134:
	movl	$0, 32(%r15)
	movl	256(%r13), %eax
.LBB0_135:
	movl	$0, 44(%r15)
	movl	$0, 36(%r15)
	testl	%eax, %eax
	setne	%cl
	movl	232(%r13), %eax
	shll	%cl, %eax
	movl	%eax, 40(%r15)
	addq	$2696, %rsp             # imm = 0xA88
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_40:
	xorl	%r14d, %r14d
	cmpl	$0, 232(%r13)
	jg	.LBB0_35
	jmp	.LBB0_118
.Lfunc_end0:
	.size	jinit_c_master_control, .Lfunc_end0-jinit_c_master_control
	.cfi_endproc

	.p2align	4, 0x90
	.type	prepare_for_pass,@function
prepare_for_pass:                       # @prepare_for_pass
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	424(%rbx), %r14
	movl	32(%r14), %eax
	cmpl	$2, %eax
	je	.LBB1_12
# BB#1:
	cmpl	$1, %eax
	je	.LBB1_6
# BB#2:
	testl	%eax, %eax
	jne	.LBB1_8
# BB#3:
	movq	%rbx, %rdi
	callq	select_scan_parameters
	movq	%rbx, %rdi
	callq	per_scan_setup
	cmpl	$0, 248(%rbx)
	jne	.LBB1_5
# BB#4:
	movq	464(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	472(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	440(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB1_5:
	movq	480(%rbx), %rax
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	488(%rbx), %rax
	movl	256(%rbx), %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	448(%rbx), %rax
	xorl	%ecx, %ecx
	cmpl	$1, 40(%r14)
	setg	%cl
	leal	(%rcx,%rcx,2), %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	432(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	xorl	%eax, %eax
	cmpl	$0, 256(%rbx)
	sete	%al
	movl	%eax, 24(%r14)
	jmp	.LBB1_18
.LBB1_6:
	movq	%rbx, %rdi
	callq	select_scan_parameters
	movq	%rbx, %rdi
	callq	per_scan_setup
	cmpl	$0, 404(%rbx)
	je	.LBB1_9
.LBB1_7:
	movq	488(%rbx), %rax
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	448(%rbx), %rax
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	jmp	.LBB1_17
.LBB1_8:
	movq	(%rbx), %rax
	movl	$47, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	jmp	.LBB1_18
.LBB1_9:
	cmpl	$0, 412(%rbx)
	je	.LBB1_7
# BB#10:
	cmpl	$0, 252(%rbx)
	jne	.LBB1_7
# BB#11:
	movl	$2, 32(%r14)
	incl	36(%r14)
.LBB1_12:
	cmpl	$0, 256(%rbx)
	jne	.LBB1_14
# BB#13:
	movq	%rbx, %rdi
	callq	select_scan_parameters
	movq	%rbx, %rdi
	callq	per_scan_setup
.LBB1_14:
	movq	488(%rbx), %rax
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	movq	448(%rbx), %rax
	movl	$2, %esi
	movq	%rbx, %rdi
	callq	*(%rax)
	cmpl	$0, 44(%r14)
	jne	.LBB1_16
# BB#15:
	movq	456(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
.LBB1_16:                               # %._crit_edge
	movq	456(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
.LBB1_17:
	movl	$0, 24(%r14)
.LBB1_18:
	movl	36(%r14), %eax
	movl	40(%r14), %ecx
	leal	-1(%rcx), %edx
	xorl	%esi, %esi
	cmpl	%edx, %eax
	sete	%sil
	movl	%esi, 28(%r14)
	movq	16(%rbx), %rdx
	testq	%rdx, %rdx
	je	.LBB1_20
# BB#19:
	movl	%eax, 24(%rdx)
	movl	%ecx, 28(%rdx)
.LBB1_20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	prepare_for_pass, .Lfunc_end1-prepare_for_pass
	.cfi_endproc

	.p2align	4, 0x90
	.type	pass_startup,@function
pass_startup:                           # @pass_startup
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	424(%rbx), %rax
	movl	$0, 24(%rax)
	movq	456(%rbx), %rax
	callq	*16(%rax)
	movq	456(%rbx), %rax
	movq	%rbx, %rdi
	popq	%rbx
	jmpq	*24(%rax)               # TAILCALL
.Lfunc_end2:
	.size	pass_startup, .Lfunc_end2-pass_startup
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_pass_master,@function
finish_pass_master:                     # @finish_pass_master
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 32
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	424(%rbx), %r14
	movq	488(%rbx), %rax
	callq	*16(%rax)
	movl	32(%r14), %eax
	cmpl	$2, %eax
	je	.LBB3_5
# BB#1:
	cmpl	$1, %eax
	je	.LBB3_4
# BB#2:
	testl	%eax, %eax
	jne	.LBB3_8
# BB#3:
	movl	$2, 32(%r14)
	cmpl	$0, 256(%rbx)
	jne	.LBB3_8
	jmp	.LBB3_7
.LBB3_5:
	cmpl	$0, 256(%rbx)
	je	.LBB3_7
# BB#6:
	movl	$1, 32(%r14)
.LBB3_7:
	incl	44(%r14)
	jmp	.LBB3_8
.LBB3_4:
	movl	$2, 32(%r14)
.LBB3_8:
	incl	36(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	finish_pass_master, .Lfunc_end3-finish_pass_master
	.cfi_endproc

	.p2align	4, 0x90
	.type	select_scan_parameters,@function
select_scan_parameters:                 # @select_scan_parameters
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
.Lcfi26:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	240(%rbx), %rax
	testq	%rax, %rax
	je	.LBB4_9
# BB#1:
	movq	424(%rbx), %rcx
	movslq	44(%rcx), %rcx
	leaq	(%rcx,%rcx,8), %r8
	movslq	(%rax,%r8,4), %rdx
	testq	%rdx, %rdx
	movl	%edx, 316(%rbx)
	jle	.LBB4_8
# BB#2:                                 # %.lr.ph49
	testb	$1, %dl
	jne	.LBB4_4
# BB#3:
	xorl	%esi, %esi
	cmpl	$1, %edx
	jne	.LBB4_6
	jmp	.LBB4_8
.LBB4_9:
	movl	68(%rbx), %eax
	cmpl	$5, %eax
	jl	.LBB4_11
# BB#10:
	movq	(%rbx), %rcx
	movl	$24, 40(%rcx)
	movl	%eax, 44(%rcx)
	movl	$4, 48(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
	movl	68(%rbx), %eax
.LBB4_11:
	movl	%eax, 316(%rbx)
	testl	%eax, %eax
	jle	.LBB4_14
# BB#12:                                # %.lr.ph
	xorl	%eax, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_13:                               # =>This Inner Loop Header: Depth=1
	movq	80(%rbx), %rdx
	addq	%rax, %rdx
	movq	%rdx, 320(%rbx,%rcx,8)
	incq	%rcx
	movslq	68(%rbx), %rdx
	addq	$96, %rax
	cmpq	%rdx, %rcx
	jl	.LBB4_13
.LBB4_14:                               # %._crit_edge
	movabsq	$270582939648, %rax     # imm = 0x3F00000000
	movq	%rax, 404(%rbx)
	movl	$0, 412(%rbx)
	xorl	%eax, %eax
	jmp	.LBB4_15
.LBB4_4:
	movslq	4(%rax,%r8,4), %rsi
	leaq	(%rsi,%rsi,2), %rsi
	shlq	$5, %rsi
	addq	80(%rbx), %rsi
	movq	%rsi, 320(%rbx)
	movl	$1, %esi
	cmpl	$1, %edx
	je	.LBB4_8
.LBB4_6:                                # %.lr.ph49.new
	leaq	8(%rax,%r8,4), %rdi
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movslq	-4(%rdi,%rsi,4), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	addq	80(%rbx), %rcx
	movq	%rcx, 320(%rbx,%rsi,8)
	movslq	(%rdi,%rsi,4), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	shlq	$5, %rcx
	addq	80(%rbx), %rcx
	movq	%rcx, 328(%rbx,%rsi,8)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jl	.LBB4_7
.LBB4_8:                                # %._crit_edge50
	movl	20(%rax,%r8,4), %ecx
	movl	%ecx, 404(%rbx)
	movl	24(%rax,%r8,4), %ecx
	movl	%ecx, 408(%rbx)
	movl	28(%rax,%r8,4), %ecx
	movl	%ecx, 412(%rbx)
	movl	32(%rax,%r8,4), %eax
.LBB4_15:
	movl	%eax, 416(%rbx)
	popq	%rbx
	retq
.Lfunc_end4:
	.size	select_scan_parameters, .Lfunc_end4-select_scan_parameters
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	8                       # 0x8
	.text
	.p2align	4, 0x90
	.type	per_scan_setup,@function
per_scan_setup:                         # @per_scan_setup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 32
.Lcfi30:
	.cfi_offset %rbx, -32
.Lcfi31:
	.cfi_offset %r14, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	316(%rbx), %eax
	cmpl	$1, %eax
	jne	.LBB5_5
# BB#1:
	movq	320(%rbx), %rcx
	movl	28(%rcx), %eax
	movl	%eax, 352(%rbx)
	movl	32(%rcx), %eax
	movl	%eax, 356(%rbx)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [1,1,1,8]
	movups	%xmm0, 52(%rcx)
	movl	$1, 68(%rcx)
	movl	12(%rcx), %esi
	xorl	%edx, %edx
	divl	%esi
	testl	%edx, %edx
	cmovel	%esi, %edx
	movl	%edx, 72(%rcx)
	movq	$1, 360(%rbx)
	jmp	.LBB5_2
.LBB5_5:
	leal	-1(%rax), %ecx
	cmpl	$4, %ecx
	jb	.LBB5_7
# BB#6:
	movq	(%rbx), %rcx
	movl	$24, 40(%rcx)
	movl	%eax, 44(%rcx)
	movl	$4, 48(%rcx)
	movq	%rbx, %rdi
	callq	*(%rcx)
.LBB5_7:
	movl	40(%rbx), %edi
	movslq	304(%rbx), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, 352(%rbx)
	movl	44(%rbx), %edi
	movslq	308(%rbx), %rsi
	shlq	$3, %rsi
	callq	jdiv_round_up
	movl	%eax, 356(%rbx)
	movl	$0, 360(%rbx)
	cmpl	$0, 316(%rbx)
	jle	.LBB5_2
# BB#8:                                 # %.lr.ph88
	xorl	%r8d, %r8d
	xorl	%r14d, %r14d
	jmp	.LBB5_9
	.p2align	4, 0x90
.LBB5_15:                               # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB5_9 Depth=1
	movl	360(%rbx), %r8d
.LBB5_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_13 Depth 2
	movq	320(%rbx,%r14,8), %rsi
	movl	8(%rsi), %edi
	movl	%edi, 52(%rsi)
	movl	12(%rsi), %ecx
	movl	%ecx, 56(%rsi)
	movl	%ecx, %ebp
	imull	%edi, %ebp
	movl	%ebp, 60(%rsi)
	leal	(,%rdi,8), %eax
	movl	%eax, 64(%rsi)
	movl	28(%rsi), %eax
	xorl	%edx, %edx
	divl	%edi
	testl	%edx, %edx
	cmovel	%edi, %edx
	movl	%edx, 68(%rsi)
	movl	32(%rsi), %eax
	xorl	%edx, %edx
	divl	%ecx
	testl	%edx, %edx
	cmovel	%ecx, %edx
	movl	%edx, 72(%rsi)
	addl	%ebp, %r8d
	cmpl	$11, %r8d
	jl	.LBB5_11
# BB#10:                                #   in Loop: Header=BB5_9 Depth=1
	movq	(%rbx), %rax
	movl	$11, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB5_11:                               # %.preheader
                                        #   in Loop: Header=BB5_9 Depth=1
	testl	%ebp, %ebp
	jle	.LBB5_14
# BB#12:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB5_9 Depth=1
	incl	%ebp
	.p2align	4, 0x90
.LBB5_13:                               # %.lr.ph
                                        #   Parent Loop BB5_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	360(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 360(%rbx)
	movl	%r14d, 364(%rbx,%rax,4)
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB5_13
.LBB5_14:                               # %._crit_edge
                                        #   in Loop: Header=BB5_9 Depth=1
	incq	%r14
	movslq	316(%rbx), %rax
	cmpq	%rax, %r14
	jl	.LBB5_15
.LBB5_2:                                # %.loopexit
	movslq	276(%rbx), %rax
	testq	%rax, %rax
	jle	.LBB5_4
# BB#3:
	movl	352(%rbx), %ecx
	imulq	%rax, %rcx
	cmpq	$65535, %rcx            # imm = 0xFFFF
	movl	$65535, %eax            # imm = 0xFFFF
	cmovll	%ecx, %eax
	movl	%eax, 272(%rbx)
.LBB5_4:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	per_scan_setup, .Lfunc_end5-per_scan_setup
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
