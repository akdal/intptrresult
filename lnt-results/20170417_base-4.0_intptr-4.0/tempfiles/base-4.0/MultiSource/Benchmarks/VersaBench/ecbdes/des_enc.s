	.text
	.file	"des_enc.bc"
	.globl	des_encrypt
	.p2align	4, 0x90
	.type	des_encrypt,@function
des_encrypt:                            # @des_encrypt
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
.Lcfi4:
	.cfi_offset %rbx, -40
.Lcfi5:
	.cfi_offset %r12, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	(%rdi), %rbx
	movq	8(%rdi), %rax
	movl	%eax, %ecx
	shrl	$4, %ecx
	xorl	%ebx, %ecx
	andl	$252645135, %ecx        # imm = 0xF0F0F0F
	xorq	%rcx, %rbx
	shlq	$4, %rcx
	xorq	%rax, %rcx
	movl	%ebx, %eax
	shrl	$16, %eax
	xorl	%ecx, %eax
	movzwl	%ax, %r8d
	xorq	%r8, %rcx
	shlq	$16, %r8
	xorq	%rbx, %r8
	movl	%ecx, %eax
	shrl	$2, %eax
	xorl	%r8d, %eax
	andl	$858993459, %eax        # imm = 0x33333333
	xorq	%rax, %r8
	shlq	$2, %rax
	xorq	%rcx, %rax
	movl	%r8d, %ecx
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx         # imm = 0xFF00FF
	xorq	%rcx, %rax
	shlq	$8, %rcx
	xorq	%r8, %rcx
	movl	%eax, %ebx
	shrl	%ebx
	xorl	%ecx, %ebx
	andl	$1431655765, %ebx       # imm = 0x55555555
	xorq	%rbx, %rcx
	addq	%rbx, %rbx
	xorq	%rax, %rbx
	movq	%rcx, %rax
	shrq	$29, %rax
	leal	(%rax,%rcx,8), %eax
	movq	%rbx, %rcx
	shrq	$29, %rcx
	leal	(%rcx,%rbx,8), %ecx
	testl	%edx, %edx
	je	.LBB0_3
# BB#1:                                 # %.preheader235.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_2:                                # %.preheader235
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	8(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	16(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	24(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	movq	32(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	40(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	48(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	56(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	addq	$8, %r8
	cmpq	$32, %r8
	jl	.LBB0_2
	jmp	.LBB0_5
.LBB0_3:                                # %.preheader.preheader
	movl	$38, %r8d
	.p2align	4, 0x90
.LBB0_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	-64(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	-56(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	-80(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	-72(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	movq	-96(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	-88(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	-112(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	-104(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	addq	$-8, %r8
	cmpq	$8, %r8
	jg	.LBB0_4
.LBB0_5:                                # %.loopexit
	movq	%rcx, %rdx
	shrq	$3, %rdx
	shlq	$29, %rcx
	addq	%rdx, %rcx
	movl	%ecx, %edx
	movq	%rax, %rsi
	shrq	$3, %rsi
	shlq	$29, %rax
	addq	%rsi, %rax
	movl	%eax, %esi
	shrl	%eax
	xorl	%ecx, %eax
	andl	$1431655765, %eax       # imm = 0x55555555
	xorq	%rax, %rdx
	addq	%rax, %rax
	xorq	%rsi, %rax
	movl	%edx, %ecx
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx         # imm = 0xFF00FF
	xorq	%rcx, %rax
	shlq	$8, %rcx
	xorq	%rdx, %rcx
	movl	%eax, %edx
	shrl	$2, %edx
	xorl	%ecx, %edx
	andl	$858993459, %edx        # imm = 0x33333333
	xorq	%rdx, %rcx
	shlq	$2, %rdx
	xorq	%rax, %rdx
	movq	%rcx, %rax
	shrq	$16, %rax
	movzwl	%dx, %esi
	xorq	%rax, %rsi
	xorq	%rsi, %rdx
	shlq	$16, %rsi
	xorq	%rcx, %rsi
	movl	%edx, %eax
	shrl	$4, %eax
	xorl	%esi, %eax
	andl	$252645135, %eax        # imm = 0xF0F0F0F
	xorq	%rax, %rsi
	shlq	$4, %rax
	xorq	%rdx, %rax
	movq	%rsi, (%rdi)
	movq	%rax, 8(%rdi)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	des_encrypt, .Lfunc_end0-des_encrypt
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.long	4294967295              # 0xffffffff
	.long	0                       # 0x0
	.text
	.globl	des_encrypt2
	.p2align	4, 0x90
	.type	des_encrypt2,@function
des_encrypt2:                           # @des_encrypt2
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 40
.Lcfi12:
	.cfi_offset %rbx, -40
.Lcfi13:
	.cfi_offset %r12, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %r15, -16
	movq	(%rdi), %rbx
	movq	8(%rdi), %r8
	movq	%rbx, %rax
	shrq	$29, %rax
	leal	(%rax,%rbx,8), %eax
	movq	%r8, %rcx
	shrq	$29, %rcx
	leal	(%rcx,%r8,8), %ecx
	testl	%edx, %edx
	je	.LBB1_3
# BB#1:                                 # %.preheader169.preheader
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader169
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	8(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	16(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	24(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	movq	32(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	40(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	48(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	56(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	addq	$8, %r8
	cmpq	$32, %r8
	jl	.LBB1_2
	jmp	.LBB1_5
.LBB1_3:                                # %.preheader.preheader
	movl	$38, %r8d
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	-64(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	-56(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	-80(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	-72(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	movq	-96(%rsi,%r8,8), %r9
	xorq	%rax, %r9
	movq	-88(%rsi,%r8,8), %rdx
	xorq	%rax, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rcx
	xorq	des_SPtrans+1024(%r11), %rcx
	xorq	des_SPtrans+2048(%r15), %rcx
	xorq	des_SPtrans+3072(%r9), %rcx
	xorq	des_SPtrans+512(%r14,%r14), %rcx
	xorq	des_SPtrans+1536(%r12), %rcx
	xorq	des_SPtrans+2560(%rbx), %rcx
	xorq	des_SPtrans+3584(%rdx), %rcx
	movq	-112(%rsi,%r8,8), %r9
	xorq	%rcx, %r9
	movq	-104(%rsi,%r8,8), %rdx
	xorq	%rcx, %rdx
	movq	%rdx, %rbx
	shrq	$4, %rbx
	shlq	$28, %rdx
	addq	%rbx, %rdx
	movl	%r9d, %r10d
	andl	$252, %r10d
	movq	%r9, %r11
	shrq	$7, %r11
	andl	$504, %r11d             # imm = 0x1F8
	movq	%r9, %r15
	shrq	$15, %r15
	andl	$504, %r15d             # imm = 0x1F8
	shrq	$23, %r9
	andl	$504, %r9d              # imm = 0x1F8
	movl	%edx, %r14d
	andl	$252, %r14d
	movq	%rdx, %r12
	shrq	$7, %r12
	andl	$504, %r12d             # imm = 0x1F8
	movq	%rdx, %rbx
	shrq	$15, %rbx
	andl	$504, %ebx              # imm = 0x1F8
	shrq	$23, %rdx
	andl	$504, %edx              # imm = 0x1F8
	xorq	des_SPtrans(%r10,%r10), %rax
	xorq	des_SPtrans+1024(%r11), %rax
	xorq	des_SPtrans+2048(%r15), %rax
	xorq	des_SPtrans+3072(%r9), %rax
	xorq	des_SPtrans+512(%r14,%r14), %rax
	xorq	des_SPtrans+1536(%r12), %rax
	xorq	des_SPtrans+2560(%rbx), %rax
	xorq	des_SPtrans+3584(%rdx), %rax
	addq	$-8, %r8
	cmpq	$8, %r8
	jg	.LBB1_4
.LBB1_5:                                # %.loopexit
	movd	%rax, %xmm0
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, %xmm0
	psrlq	$3, %xmm0
	psllq	$29, %xmm1
	paddq	%xmm0, %xmm1
	pand	.LCPI1_0(%rip), %xmm1
	movdqu	%xmm1, (%rdi)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	des_encrypt2, .Lfunc_end1-des_encrypt2
	.cfi_endproc

	.globl	des_encrypt3
	.p2align	4, 0x90
	.type	des_encrypt3,@function
des_encrypt3:                           # @des_encrypt3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %r15
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	8(%rbx), %rdx
	movl	%edx, %ecx
	shrl	$4, %ecx
	xorl	%eax, %ecx
	andl	$252645135, %ecx        # imm = 0xF0F0F0F
	xorq	%rcx, %rax
	shlq	$4, %rcx
	xorq	%rdx, %rcx
	movl	%eax, %edx
	shrl	$16, %edx
	xorl	%ecx, %edx
	movzwl	%dx, %edx
	xorq	%rdx, %rcx
	shlq	$16, %rdx
	xorq	%rax, %rdx
	movl	%ecx, %eax
	shrl	$2, %eax
	xorl	%edx, %eax
	andl	$858993459, %eax        # imm = 0x33333333
	xorq	%rax, %rdx
	shlq	$2, %rax
	xorq	%rcx, %rax
	movl	%edx, %ecx
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx         # imm = 0xFF00FF
	xorq	%rcx, %rax
	shlq	$8, %rcx
	xorq	%rdx, %rcx
	movl	%eax, %edx
	shrl	%edx
	xorl	%ecx, %edx
	andl	$1431655765, %edx       # imm = 0x55555555
	xorq	%rdx, %rcx
	addq	%rdx, %rdx
	xorq	%rax, %rdx
	movq	%rcx, (%rbx)
	movq	%rdx, 8(%rbx)
	movl	$1, %edx
	callq	des_encrypt2
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	des_encrypt2
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	des_encrypt2
	movq	(%rbx), %rdx
	movq	8(%rbx), %rcx
	movl	%ecx, %eax
	shrl	%eax
	xorl	%edx, %eax
	andl	$1431655765, %eax       # imm = 0x55555555
	xorq	%rax, %rdx
	addq	%rax, %rax
	xorq	%rcx, %rax
	movl	%edx, %ecx
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx         # imm = 0xFF00FF
	xorq	%rcx, %rax
	shlq	$8, %rcx
	xorq	%rdx, %rcx
	movl	%eax, %edx
	shrl	$2, %edx
	xorl	%ecx, %edx
	andl	$858993459, %edx        # imm = 0x33333333
	xorq	%rdx, %rcx
	shlq	$2, %rdx
	xorq	%rax, %rdx
	movl	%ecx, %eax
	shrl	$16, %eax
	xorl	%edx, %eax
	movzwl	%ax, %eax
	xorq	%rax, %rdx
	shlq	$16, %rax
	xorq	%rcx, %rax
	movl	%edx, %ecx
	shrl	$4, %ecx
	xorl	%eax, %ecx
	andl	$252645135, %ecx        # imm = 0xF0F0F0F
	xorq	%rcx, %rax
	shlq	$4, %rcx
	xorq	%rdx, %rcx
	movq	%rax, (%rbx)
	movq	%rcx, 8(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end2:
	.size	des_encrypt3, .Lfunc_end2-des_encrypt3
	.cfi_endproc

	.globl	des_decrypt3
	.p2align	4, 0x90
	.type	des_decrypt3,@function
des_decrypt3:                           # @des_decrypt3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 32
.Lcfi25:
	.cfi_offset %rbx, -32
.Lcfi26:
	.cfi_offset %r14, -24
.Lcfi27:
	.cfi_offset %r15, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movq	8(%rbx), %rsi
	movl	%esi, %edx
	shrl	$4, %edx
	xorl	%eax, %edx
	andl	$252645135, %edx        # imm = 0xF0F0F0F
	xorq	%rdx, %rax
	shlq	$4, %rdx
	xorq	%rsi, %rdx
	movl	%eax, %esi
	shrl	$16, %esi
	xorl	%edx, %esi
	movzwl	%si, %esi
	xorq	%rsi, %rdx
	shlq	$16, %rsi
	xorq	%rax, %rsi
	movl	%edx, %eax
	shrl	$2, %eax
	xorl	%esi, %eax
	andl	$858993459, %eax        # imm = 0x33333333
	xorq	%rax, %rsi
	shlq	$2, %rax
	xorq	%rdx, %rax
	movl	%esi, %edx
	shrl	$8, %edx
	xorl	%eax, %edx
	andl	$16711935, %edx         # imm = 0xFF00FF
	xorq	%rdx, %rax
	shlq	$8, %rdx
	xorq	%rsi, %rdx
	movl	%eax, %esi
	shrl	%esi
	xorl	%edx, %esi
	andl	$1431655765, %esi       # imm = 0x55555555
	xorq	%rsi, %rdx
	addq	%rsi, %rsi
	xorq	%rax, %rsi
	movq	%rdx, (%rbx)
	movq	%rsi, 8(%rbx)
	xorl	%edx, %edx
	movq	%rcx, %rsi
	callq	des_encrypt2
	movl	$1, %edx
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	des_encrypt2
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	des_encrypt2
	movq	(%rbx), %rdx
	movq	8(%rbx), %rcx
	movl	%ecx, %eax
	shrl	%eax
	xorl	%edx, %eax
	andl	$1431655765, %eax       # imm = 0x55555555
	xorq	%rax, %rdx
	addq	%rax, %rax
	xorq	%rcx, %rax
	movl	%edx, %ecx
	shrl	$8, %ecx
	xorl	%eax, %ecx
	andl	$16711935, %ecx         # imm = 0xFF00FF
	xorq	%rcx, %rax
	shlq	$8, %rcx
	xorq	%rdx, %rcx
	movl	%eax, %edx
	shrl	$2, %edx
	xorl	%ecx, %edx
	andl	$858993459, %edx        # imm = 0x33333333
	xorq	%rdx, %rcx
	shlq	$2, %rdx
	xorq	%rax, %rdx
	movl	%ecx, %eax
	shrl	$16, %eax
	xorl	%edx, %eax
	movzwl	%ax, %eax
	xorq	%rax, %rdx
	shlq	$16, %rax
	xorq	%rcx, %rax
	movl	%edx, %ecx
	shrl	$4, %ecx
	xorl	%eax, %ecx
	andl	$252645135, %ecx        # imm = 0xF0F0F0F
	xorq	%rcx, %rax
	shlq	$4, %rcx
	xorq	%rdx, %rcx
	movq	%rax, (%rbx)
	movq	%rcx, 8(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	des_decrypt3, .Lfunc_end3-des_decrypt3
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
