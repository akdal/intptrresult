	.text
	.file	"btUniformScalingShape.bc"
	.globl	_ZN21btUniformScalingShapeC2EP13btConvexShapef
	.p2align	4, 0x90
	.type	_ZN21btUniformScalingShapeC2EP13btConvexShapef,@function
_ZN21btUniformScalingShapeC2EP13btConvexShapef: # @_ZN21btUniformScalingShapeC2EP13btConvexShapef
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %rbx
	callq	_ZN13btConvexShapeC2Ev
	movq	$_ZTV21btUniformScalingShape+16, (%rbx)
	movq	%r14, 24(%rbx)
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rbx)
	movl	$14, 8(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_ZN21btUniformScalingShapeC2EP13btConvexShapef, .Lfunc_end0-_ZN21btUniformScalingShapeC2EP13btConvexShapef
	.cfi_endproc

	.globl	_ZN21btUniformScalingShapeD2Ev
	.p2align	4, 0x90
	.type	_ZN21btUniformScalingShapeD2Ev,@function
_ZN21btUniformScalingShapeD2Ev:         # @_ZN21btUniformScalingShapeD2Ev
	.cfi_startproc
# BB#0:
	jmp	_ZN13btConvexShapeD2Ev  # TAILCALL
.Lfunc_end1:
	.size	_ZN21btUniformScalingShapeD2Ev, .Lfunc_end1-_ZN21btUniformScalingShapeD2Ev
	.cfi_endproc

	.globl	_ZN21btUniformScalingShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN21btUniformScalingShapeD0Ev,@function
_ZN21btUniformScalingShapeD0Ev:         # @_ZN21btUniformScalingShapeD0Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp0:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp1:
# BB#1:                                 # %_ZN21btUniformScalingShapeD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB2_2:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_4:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN21btUniformScalingShapeD0Ev, .Lfunc_end2-_ZN21btUniformScalingShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.text
	.globl	_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 16
.Lcfi11:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*104(%rax)
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm2, %xmm1
	popq	%rbx
	retq
.Lfunc_end4:
	.size	_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end4-_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc

	.globl	_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi12:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 32
.Lcfi15:
	.cfi_offset %rbx, -32
.Lcfi16:
	.cfi_offset %r14, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movq	24(%r14), %rdi
	movq	(%rdi), %rax
	callq	*112(%rax)
	testl	%ebp, %ebp
	jle	.LBB5_3
# BB#1:                                 # %.lr.ph
	movl	%ebp, %eax
	addq	$8, %rbx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movss	32(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	-8(%rbx), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	(%rbx), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, -8(%rbx)
	movlps	%xmm1, (%rbx)
	addq	$16, %rbx
	decq	%rax
	jne	.LBB5_2
.LBB5_3:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end5-_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc

	.globl	_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3,@function
_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3: # @_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 16
.Lcfi19:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*96(%rax)
	movss	32(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movaps	%xmm2, %xmm1
	popq	%rbx
	retq
.Lfunc_end6:
	.size	_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3, .Lfunc_end6-_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.globl	_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3,@function
_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3: # @_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 48
.Lcfi23:
	.cfi_offset %rbx, -24
.Lcfi24:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	8(%rsp), %rsi
	callq	*64(%rax)
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	8(%rsp), %xmm1          # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	16(%rsp), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, (%r14)
	movlps	%xmm1, 8(%r14)
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end7:
	.size	_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3, .Lfunc_end7-_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_1:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_,@function
_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_: # @_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 32
.Lcfi28:
	.cfi_offset %rbx, -32
.Lcfi29:
	.cfi_offset %r14, -24
.Lcfi30:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rdi, %r15
	movq	24(%r15), %rdi
	movq	(%rdi), %rax
	callq	*16(%rax)
	movsd	(%r14), %xmm3           # xmm3 = mem[0],zero
	movsd	(%rbx), %xmm4           # xmm4 = mem[0],zero
	movaps	%xmm3, %xmm1
	addps	%xmm4, %xmm1
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	addss	%xmm5, %xmm2
	movaps	.LCPI8_0(%rip), %xmm6   # xmm6 = <0.5,0.5,u,u>
	mulps	%xmm6, %xmm1
	movss	.LCPI8_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	subps	%xmm4, %xmm3
	subss	%xmm5, %xmm0
	mulps	%xmm6, %xmm3
	mulss	%xmm7, %xmm0
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movaps	%xmm1, %xmm3
	subps	%xmm4, %xmm3
	movaps	%xmm2, %xmm5
	subss	%xmm0, %xmm5
	xorps	%xmm6, %xmm6
	xorps	%xmm7, %xmm7
	movss	%xmm5, %xmm7            # xmm7 = xmm5[0],xmm7[1,2,3]
	movlps	%xmm3, (%rbx)
	movlps	%xmm7, 8(%rbx)
	addps	%xmm1, %xmm4
	addss	%xmm2, %xmm0
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movlps	%xmm4, (%r14)
	movlps	%xmm6, 8(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_, .Lfunc_end8-_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_1:
	.long	1056964608              # float 0.5
	.text
	.globl	_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_,@function
_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_: # @_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rdi, %r15
	movq	24(%r15), %rdi
	movq	(%rdi), %rax
	callq	*120(%rax)
	movsd	(%r14), %xmm3           # xmm3 = mem[0],zero
	movsd	(%rbx), %xmm4           # xmm4 = mem[0],zero
	movaps	%xmm3, %xmm1
	addps	%xmm4, %xmm1
	movss	8(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	addss	%xmm5, %xmm2
	movaps	.LCPI9_0(%rip), %xmm6   # xmm6 = <0.5,0.5,u,u>
	mulps	%xmm6, %xmm1
	movss	.LCPI9_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	subps	%xmm4, %xmm3
	subss	%xmm5, %xmm0
	mulps	%xmm6, %xmm3
	mulss	%xmm7, %xmm0
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	movaps	%xmm1, %xmm3
	subps	%xmm4, %xmm3
	movaps	%xmm2, %xmm5
	subss	%xmm0, %xmm5
	xorps	%xmm6, %xmm6
	xorps	%xmm7, %xmm7
	movss	%xmm5, %xmm7            # xmm7 = xmm5[0],xmm7[1,2,3]
	movlps	%xmm3, (%rbx)
	movlps	%xmm7, 8(%rbx)
	addps	%xmm1, %xmm4
	addss	%xmm2, %xmm0
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movlps	%xmm4, (%r14)
	movlps	%xmm6, 8(%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_, .Lfunc_end9-_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_
	.cfi_endproc

	.globl	_ZN21btUniformScalingShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN21btUniformScalingShape15setLocalScalingERK9btVector3,@function
_ZN21btUniformScalingShape15setLocalScalingERK9btVector3: # @_ZN21btUniformScalingShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end10:
	.size	_ZN21btUniformScalingShape15setLocalScalingERK9btVector3, .Lfunc_end10-_ZN21btUniformScalingShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZNK21btUniformScalingShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape15getLocalScalingEv,@function
_ZNK21btUniformScalingShape15getLocalScalingEv: # @_ZNK21btUniformScalingShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 16
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	popq	%rcx
	retq
.Lfunc_end11:
	.size	_ZNK21btUniformScalingShape15getLocalScalingEv, .Lfunc_end11-_ZNK21btUniformScalingShape15getLocalScalingEv
	.cfi_endproc

	.globl	_ZN21btUniformScalingShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btUniformScalingShape9setMarginEf,@function
_ZN21btUniformScalingShape9setMarginEf: # @_ZN21btUniformScalingShape9setMarginEf
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	80(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end12:
	.size	_ZN21btUniformScalingShape9setMarginEf, .Lfunc_end12-_ZN21btUniformScalingShape9setMarginEf
	.cfi_endproc

	.globl	_ZNK21btUniformScalingShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape9getMarginEv,@function
_ZNK21btUniformScalingShape9getMarginEv: # @_ZNK21btUniformScalingShape9getMarginEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 16
.Lcfi39:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	24(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	mulss	32(%rbx), %xmm0
	popq	%rbx
	retq
.Lfunc_end13:
	.size	_ZNK21btUniformScalingShape9getMarginEv, .Lfunc_end13-_ZNK21btUniformScalingShape9getMarginEv
	.cfi_endproc

	.globl	_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	jmpq	*128(%rax)              # TAILCALL
.Lfunc_end14:
	.size	_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end14-_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.globl	_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %rdi
	movq	(%rdi), %rax
	movq	136(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end15:
	.size	_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end15-_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZNK21btUniformScalingShape7getNameEv,"axG",@progbits,_ZNK21btUniformScalingShape7getNameEv,comdat
	.weak	_ZNK21btUniformScalingShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK21btUniformScalingShape7getNameEv,@function
_ZNK21btUniformScalingShape7getNameEv:  # @_ZNK21btUniformScalingShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end16:
	.size	_ZNK21btUniformScalingShape7getNameEv, .Lfunc_end16-_ZNK21btUniformScalingShape7getNameEv
	.cfi_endproc

	.type	_ZTV21btUniformScalingShape,@object # @_ZTV21btUniformScalingShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV21btUniformScalingShape
	.p2align	3
_ZTV21btUniformScalingShape:
	.quad	0
	.quad	_ZTI21btUniformScalingShape
	.quad	_ZN21btUniformScalingShapeD2Ev
	.quad	_ZN21btUniformScalingShapeD0Ev
	.quad	_ZNK21btUniformScalingShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN21btUniformScalingShape15setLocalScalingERK9btVector3
	.quad	_ZNK21btUniformScalingShape15getLocalScalingEv
	.quad	_ZNK21btUniformScalingShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK21btUniformScalingShape7getNameEv
	.quad	_ZN21btUniformScalingShape9setMarginEf
	.quad	_ZNK21btUniformScalingShape9getMarginEv
	.quad	_ZNK21btUniformScalingShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK21btUniformScalingShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK21btUniformScalingShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btUniformScalingShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btUniformScalingShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btUniformScalingShape32getPreferredPenetrationDirectionEiR9btVector3
	.size	_ZTV21btUniformScalingShape, 160

	.type	_ZTS21btUniformScalingShape,@object # @_ZTS21btUniformScalingShape
	.globl	_ZTS21btUniformScalingShape
	.p2align	4
_ZTS21btUniformScalingShape:
	.asciz	"21btUniformScalingShape"
	.size	_ZTS21btUniformScalingShape, 24

	.type	_ZTI21btUniformScalingShape,@object # @_ZTI21btUniformScalingShape
	.globl	_ZTI21btUniformScalingShape
	.p2align	4
_ZTI21btUniformScalingShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21btUniformScalingShape
	.quad	_ZTI13btConvexShape
	.size	_ZTI21btUniformScalingShape, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"UniformScalingShape"
	.size	.L.str, 20


	.globl	_ZN21btUniformScalingShapeC1EP13btConvexShapef
	.type	_ZN21btUniformScalingShapeC1EP13btConvexShapef,@function
_ZN21btUniformScalingShapeC1EP13btConvexShapef = _ZN21btUniformScalingShapeC2EP13btConvexShapef
	.globl	_ZN21btUniformScalingShapeD1Ev
	.type	_ZN21btUniformScalingShapeD1Ev,@function
_ZN21btUniformScalingShapeD1Ev = _ZN21btUniformScalingShapeD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
