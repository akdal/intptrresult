	.text
	.file	"jcinit.bc"
	.globl	jinit_compress_master
	.p2align	4, 0x90
	.type	jinit_compress_master,@function
jinit_compress_master:                  # @jinit_compress_master
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%esi, %esi
	callq	jinit_c_master_control
	cmpl	$0, 248(%rbx)
	jne	.LBB0_2
# BB#1:
	movq	%rbx, %rdi
	callq	jinit_color_converter
	movq	%rbx, %rdi
	callq	jinit_downsampler
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	jinit_c_prep_controller
.LBB0_2:
	movq	%rbx, %rdi
	callq	jinit_forward_dct
	cmpl	$0, 252(%rbx)
	je	.LBB0_4
# BB#3:
	movq	(%rbx), %rax
	movl	$1, 40(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
	jmp	.LBB0_7
.LBB0_4:
	cmpl	$0, 300(%rbx)
	je	.LBB0_6
# BB#5:
	movq	%rbx, %rdi
	callq	jinit_phuff_encoder
	jmp	.LBB0_7
.LBB0_6:
	movq	%rbx, %rdi
	callq	jinit_huff_encoder
.LBB0_7:
	movb	$1, %al
	cmpl	$1, 232(%rbx)
	jg	.LBB0_9
# BB#8:
	cmpl	$0, 256(%rbx)
	setne	%al
.LBB0_9:
	movzbl	%al, %esi
	movq	%rbx, %rdi
	callq	jinit_c_coef_controller
	xorl	%esi, %esi
	movq	%rbx, %rdi
	callq	jinit_c_main_controller
	movq	%rbx, %rdi
	callq	jinit_marker_writer
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	callq	*48(%rax)
	movq	456(%rbx), %rax
	movq	%rbx, %rdi
	popq	%rbx
	jmpq	*8(%rax)                # TAILCALL
.Lfunc_end0:
	.size	jinit_compress_master, .Lfunc_end0-jinit_compress_master
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
