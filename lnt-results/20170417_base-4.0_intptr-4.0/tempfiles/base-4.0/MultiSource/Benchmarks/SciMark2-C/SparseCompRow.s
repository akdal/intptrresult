	.text
	.file	"SparseCompRow.bc"
	.globl	SparseCompRow_num_flops
	.p2align	4, 0x90
	.type	SparseCompRow_num_flops,@function
SparseCompRow_num_flops:                # @SparseCompRow_num_flops
	.cfi_startproc
# BB#0:
	movl	%edx, %ecx
	movl	%esi, %eax
	cltd
	idivl	%edi
	subl	%edx, %esi
	cvtsi2sdl	%esi, %xmm1
	addsd	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm1, %xmm0
	retq
.Lfunc_end0:
	.size	SparseCompRow_num_flops, .Lfunc_end0-SparseCompRow_num_flops
	.cfi_endproc

	.globl	SparseCompRow_matmult
	.p2align	4, 0x90
	.type	SparseCompRow_matmult,@function
SparseCompRow_matmult:                  # @SparseCompRow_matmult
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movq	%r8, -16(%rsp)          # 8-byte Spill
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	cmpl	$0, 56(%rsp)
	jle	.LBB1_12
# BB#1:
	testl	%edi, %edi
	jle	.LBB1_12
# BB#2:                                 # %.preheader.us.preheader
	movl	(%rcx), %eax
	movl	%eax, -28(%rsp)         # 4-byte Spill
	movl	%edi, %r13d
	movq	-16(%rsp), %rax         # 8-byte Reload
	leaq	4(%rax), %rdx
	movq	-24(%rsp), %rax         # 8-byte Reload
	leaq	8(%rax), %r12
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_4 Depth 2
                                        #       Child Loop BB1_9 Depth 3
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movl	-28(%rsp), %r15d        # 4-byte Reload
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_4:                                #   Parent Loop BB1_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_9 Depth 3
	movq	%r8, %r14
	movl	%r15d, %ebp
	leaq	1(%r14), %r8
	movl	4(%rcx,%r14,4), %r15d
	xorpd	%xmm1, %xmm1
	cmpl	%r15d, %ebp
	jge	.LBB1_10
# BB#5:                                 # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB1_4 Depth=2
	movslq	%r15d, %rdi
	movslq	%ebp, %rbx
	movl	%r15d, %eax
	subl	%ebp, %eax
	leaq	-1(%rdi), %r10
	xorpd	%xmm1, %xmm1
	testb	$1, %al
	movq	%rbx, %r11
	je	.LBB1_7
# BB#6:                                 # %.lr.ph.us.prol
                                        #   in Loop: Header=BB1_4 Depth=2
	movq	-16(%rsp), %rax         # 8-byte Reload
	movslq	(%rax,%rbx,4), %rax
	movsd	(%r9,%rax,8), %xmm1     # xmm1 = mem[0],zero
	movq	-24(%rsp), %rax         # 8-byte Reload
	mulsd	(%rax,%rbx,8), %xmm1
	addsd	%xmm0, %xmm1
	leaq	1(%rbx), %r11
.LBB1_7:                                # %.lr.ph.us.prol.loopexit
                                        #   in Loop: Header=BB1_4 Depth=2
	cmpq	%rbx, %r10
	je	.LBB1_10
# BB#8:                                 # %.lr.ph.us.preheader.new
                                        #   in Loop: Header=BB1_4 Depth=2
	subq	%r11, %rdi
	leaq	(%rdx,%r11,4), %r10
	leaq	(%r12,%r11,8), %r11
	.p2align	4, 0x90
.LBB1_9:                                # %.lr.ph.us
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_4 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	-4(%r10), %rax
	movsd	(%r9,%rax,8), %xmm2     # xmm2 = mem[0],zero
	mulsd	-8(%r11), %xmm2
	addsd	%xmm1, %xmm2
	movslq	(%r10), %rax
	movsd	(%r9,%rax,8), %xmm1     # xmm1 = mem[0],zero
	mulsd	(%r11), %xmm1
	addsd	%xmm2, %xmm1
	addq	$8, %r10
	addq	$16, %r11
	addq	$-2, %rdi
	jne	.LBB1_9
.LBB1_10:                               # %._crit_edge.us
                                        #   in Loop: Header=BB1_4 Depth=2
	movsd	%xmm1, (%rsi,%r14,8)
	cmpq	%r13, %r8
	jne	.LBB1_4
# BB#11:                                # %._crit_edge40.us
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	-8(%rsp), %rax          # 8-byte Reload
	incl	%eax
	cmpl	56(%rsp), %eax
	jne	.LBB1_3
.LBB1_12:                               # %._crit_edge42
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	SparseCompRow_matmult, .Lfunc_end1-SparseCompRow_matmult
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
