	.text
	.file	"btDiscreteDynamicsWorld.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1101004800              # float 20
	.long	1065353216              # float 1
	.long	1045220557              # float 0.200000003
	.long	1036831949              # float 0.100000001
	.text
	.globl	_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration,@function
_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration: # @_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	%r8, %rcx
	callq	_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration
	xorps	%xmm0, %xmm0
	movups	%xmm0, 136(%rbx)
	movq	$0, 152(%rbx)
	movl	$1058642330, 160(%rbx)  # imm = 0x3F19999A
	movl	$1065353216, 164(%rbx)  # imm = 0x3F800000
	movl	$1050253722, 168(%rbx)  # imm = 0x3E99999A
	movl	$0, 176(%rbx)
	movl	$10, 180(%rbx)
	movl	$0, 200(%rbx)
	movaps	.LCPI0_0(%rip), %xmm0   # xmm0 = [2.000000e+01,1.000000e+00,2.000000e-01,1.000000e-01]
	movups	%xmm0, 184(%rbx)
	movl	$0, 204(%rbx)
	movl	$-1130113270, 208(%rbx) # imm = 0xBCA3D70A
	movl	$0, 212(%rbx)
	movl	$1062836634, 216(%rbx)  # imm = 0x3F59999A
	movl	$260, 220(%rbx)         # imm = 0x104
	movl	$2, 224(%rbx)
	movq	$_ZTV23btDiscreteDynamicsWorld+16, (%rbx)
	movq	%r14, 232(%rbx)
	movb	$1, 272(%rbx)
	movq	$0, 264(%rbx)
	movl	$0, 252(%rbx)
	movl	$0, 256(%rbx)
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movl	$0, 284(%rbx)
	movl	$0, 288(%rbx)
	movl	$0, 312(%rbx)
	movl	$-1054867456, 316(%rbx) # imm = 0xC1200000
	movl	$0, 320(%rbx)
	movl	$0, 324(%rbx)
	movl	$1015580809, 328(%rbx)  # imm = 0x3C888889
	movb	$0, 334(%rbx)
	movb	$1, 360(%rbx)
	movq	$0, 352(%rbx)
	movq	$0, 340(%rbx)
	movl	$0, 368(%rbx)
	testq	%r14, %r14
	je	.LBB0_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB0_5
.LBB0_2:
.Ltmp0:
	movl	$240, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
.Ltmp1:
# BB#3:
.Ltmp2:
	movq	%r14, %rdi
	callq	_ZN35btSequentialImpulseConstraintSolverC1Ev
.Ltmp3:
# BB#4:
	movq	%r14, 232(%rbx)
	movb	$1, %al
.LBB0_5:
	movb	%al, 333(%rbx)
.Ltmp5:
	movl	$112, %edi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r14
.Ltmp6:
# BB#6:
.Ltmp8:
	movq	%r14, %rdi
	callq	_ZN25btSimulationIslandManagerC1Ev
.Ltmp9:
# BB#7:
	movq	%r14, 240(%rbx)
	movb	$1, 332(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_9:
.Ltmp4:
	jmp	.LBB0_11
.LBB0_10:
.Ltmp10:
	jmp	.LBB0_11
.LBB0_8:
.Ltmp7:
.LBB0_11:
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_15
# BB#12:
	cmpb	$0, 360(%rbx)
	je	.LBB0_14
# BB#13:
.Ltmp11:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp12:
.LBB0_14:                               # %.noexc23
	movq	$0, 352(%rbx)
.LBB0_15:
	movb	$1, 360(%rbx)
	movq	$0, 352(%rbx)
	movq	$0, 340(%rbx)
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_19
# BB#16:
	cmpb	$0, 304(%rbx)
	je	.LBB0_18
# BB#17:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB0_18:                               # %.noexc21
	movq	$0, 296(%rbx)
.LBB0_19:
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
	movq	264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_23
# BB#20:
	cmpb	$0, 272(%rbx)
	je	.LBB0_22
# BB#21:
.Ltmp15:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp16:
.LBB0_22:                               # %.noexc
	movq	$0, 264(%rbx)
.LBB0_23:
	movb	$1, 272(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 252(%rbx)
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN16btCollisionWorldD2Ev
.Ltmp18:
# BB#24:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_25:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration, .Lfunc_end0-_ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp2-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp2           #   Call between .Ltmp2 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp18-.Ltmp11         #   Call between .Ltmp11 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Lfunc_end0-.Ltmp18     #   Call between .Ltmp18 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN23btDiscreteDynamicsWorldD2Ev
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorldD2Ev,@function
_ZN23btDiscreteDynamicsWorldD2Ev:       # @_ZN23btDiscreteDynamicsWorldD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV23btDiscreteDynamicsWorld+16, (%rbx)
	cmpb	$0, 332(%rbx)
	je	.LBB2_3
# BB#1:
	movq	240(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp20:
	callq	*(%rax)
.Ltmp21:
# BB#2:
	movq	240(%rbx), %rdi
.Ltmp22:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp23:
.LBB2_3:
	cmpb	$0, 333(%rbx)
	je	.LBB2_6
# BB#4:
	movq	232(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp24:
	callq	*(%rax)
.Ltmp25:
# BB#5:
	movq	232(%rbx), %rdi
.Ltmp26:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp27:
.LBB2_6:
	movq	352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_10
# BB#7:
	cmpb	$0, 360(%rbx)
	je	.LBB2_9
# BB#8:
.Ltmp31:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp32:
.LBB2_9:                                # %.noexc6
	movq	$0, 352(%rbx)
.LBB2_10:
	movb	$1, 360(%rbx)
	movq	$0, 352(%rbx)
	movq	$0, 340(%rbx)
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_14
# BB#11:
	cmpb	$0, 304(%rbx)
	je	.LBB2_13
# BB#12:
.Ltmp36:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp37:
.LBB2_13:                               # %.noexc9
	movq	$0, 296(%rbx)
.LBB2_14:
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
	movq	264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#15:
	cmpb	$0, 272(%rbx)
	je	.LBB2_17
# BB#16:
.Ltmp41:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp42:
.LBB2_17:                               # %.noexc11
	movq	$0, 264(%rbx)
.LBB2_18:
	movb	$1, 272(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 252(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN16btCollisionWorldD2Ev # TAILCALL
.LBB2_30:
.Ltmp43:
	movq	%rax, %r14
	jmp	.LBB2_37
.LBB2_31:
.Ltmp38:
	movq	%rax, %r14
	jmp	.LBB2_32
.LBB2_24:
.Ltmp33:
	movq	%rax, %r14
	jmp	.LBB2_25
.LBB2_19:
.Ltmp28:
	movq	%rax, %r14
	movq	352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_23
# BB#20:
	cmpb	$0, 360(%rbx)
	je	.LBB2_22
# BB#21:
.Ltmp29:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp30:
.LBB2_22:                               # %.noexc
	movq	$0, 352(%rbx)
.LBB2_23:                               # %_ZN20btAlignedObjectArrayIP17btActionInterfaceED2Ev.exit
	movb	$1, 360(%rbx)
	movq	$0, 352(%rbx)
	movq	$0, 340(%rbx)
.LBB2_25:
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_29
# BB#26:
	cmpb	$0, 304(%rbx)
	je	.LBB2_28
# BB#27:
.Ltmp34:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp35:
.LBB2_28:                               # %.noexc13
	movq	$0, 296(%rbx)
.LBB2_29:                               # %_ZN20btAlignedObjectArrayIP11btRigidBodyED2Ev.exit14
	movb	$1, 304(%rbx)
	movq	$0, 296(%rbx)
	movq	$0, 284(%rbx)
.LBB2_32:
	movq	264(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_36
# BB#33:
	cmpb	$0, 272(%rbx)
	je	.LBB2_35
# BB#34:
.Ltmp39:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp40:
.LBB2_35:                               # %.noexc16
	movq	$0, 264(%rbx)
.LBB2_36:                               # %_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev.exit17
	movb	$1, 272(%rbx)
	movq	$0, 264(%rbx)
	movq	$0, 252(%rbx)
.LBB2_37:
.Ltmp44:
	movq	%rbx, %rdi
	callq	_ZN16btCollisionWorldD2Ev
.Ltmp45:
# BB#38:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB2_39:
.Ltmp46:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN23btDiscreteDynamicsWorldD2Ev, .Lfunc_end2-_ZN23btDiscreteDynamicsWorldD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp27-.Ltmp20         #   Call between .Ltmp20 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp31-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp32-.Ltmp31         #   Call between .Ltmp31 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin1   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp37-.Ltmp36         #   Call between .Ltmp36 and .Ltmp37
	.long	.Ltmp38-.Lfunc_begin1   #     jumps to .Ltmp38
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin1   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Ltmp29-.Ltmp42         #   Call between .Ltmp42 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin1   # >> Call Site 6 <<
	.long	.Ltmp45-.Ltmp29         #   Call between .Ltmp29 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin1   #     jumps to .Ltmp46
	.byte	1                       #   On action: 1
	.long	.Ltmp45-.Lfunc_begin1   # >> Call Site 7 <<
	.long	.Lfunc_end2-.Ltmp45     #   Call between .Ltmp45 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btDiscreteDynamicsWorldD0Ev
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorldD0Ev,@function
_ZN23btDiscreteDynamicsWorldD0Ev:       # @_ZN23btDiscreteDynamicsWorldD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp47:
	callq	_ZN23btDiscreteDynamicsWorldD2Ev
.Ltmp48:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp49:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN23btDiscreteDynamicsWorldD0Ev, .Lfunc_end3-_ZN23btDiscreteDynamicsWorldD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp47-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin2   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp48     #   Call between .Ltmp48 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf,@function
_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf: # @_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 32
.Lcfi18:
	.cfi_offset %rbx, -24
.Lcfi19:
	.cfi_offset %r14, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.LBB4_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rcx
	movq	(%rcx,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB4_7
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	$2, 256(%rdi)
	jne	.LBB4_7
# BB#4:                                 #   in Loop: Header=BB4_2 Depth=1
	cmpl	$2, 228(%rdi)
	je	.LBB4_7
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	testb	$2, 216(%rdi)
	je	.LBB4_7
# BB#6:                                 #   in Loop: Header=BB4_2 Depth=1
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody18saveKinematicStateEf
	movl	12(%r14), %eax
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB4_2
.LBB4_8:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf, .Lfunc_end4-_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv,@function
_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv: # @_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi26:
	.cfi_def_cfa_offset 128
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	$.L.str, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movq	(%r14), %rax
.Ltmp50:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp51:
# BB#1:
	testq	%rax, %rax
	je	.LBB5_15
# BB#2:
	movq	(%r14), %rax
.Ltmp52:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp53:
# BB#3:
	movq	(%rax), %rcx
.Ltmp54:
	movq	%rax, %rdi
	callq	*96(%rcx)
.Ltmp55:
# BB#4:
	testb	$8, %al
	je	.LBB5_15
# BB#5:
	movq	40(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp56:
	callq	*72(%rax)
.Ltmp57:
# BB#6:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	testl	%eax, %eax
	jle	.LBB5_15
# BB#7:                                 # %.lr.ph217.preheader
	xorl	%ecx, %ecx
	movq	%rsp, %r12
	movl	%eax, 28(%rsp)          # 4-byte Spill
	.p2align	4, 0x90
.LBB5_8:                                # %.lr.ph217
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_11 Depth 2
	movq	40(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp59:
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rcx, %rsi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*80(%rax)
	movq	%rax, %rbp
.Ltmp60:
# BB#9:                                 #   in Loop: Header=BB5_8 Depth=1
	movslq	728(%rbp), %r15
	testq	%r15, %r15
	jle	.LBB5_14
# BB#10:                                # %.lr.ph213.preheader
                                        #   in Loop: Header=BB5_8 Depth=1
	addq	$72, %rbp
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_11:                               # %.lr.ph213
                                        #   Parent Loop BB5_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14), %rax
.Ltmp62:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp63:
# BB#12:                                #   in Loop: Header=BB5_11 Depth=2
	movq	(%rax), %rbx
	leaq	-32(%rbp), %rsi
	movss	16(%rbp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movl	72(%rbp), %ecx
.Ltmp64:
	movq	%rax, %rdi
	movq	%rbp, %rdx
	movq	%r12, %r8
	callq	*64(%rbx)
.Ltmp65:
# BB#13:                                #   in Loop: Header=BB5_11 Depth=2
	incq	%r13
	addq	$176, %rbp
	cmpq	%r15, %r13
	jl	.LBB5_11
.LBB5_14:                               # %._crit_edge214
                                        #   in Loop: Header=BB5_8 Depth=1
	movq	32(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movl	28(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ecx
	jl	.LBB5_8
.LBB5_15:
	movq	(%r14), %rax
.Ltmp67:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp68:
# BB#16:
	testq	%rax, %rax
	je	.LBB5_28
# BB#17:
	movq	(%r14), %rax
.Ltmp69:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp70:
# BB#18:
	movq	(%rax), %rcx
.Ltmp71:
	movq	%rax, %rdi
	callq	*96(%rcx)
.Ltmp72:
# BB#19:
	testb	$24, %ah
	je	.LBB5_28
# BB#20:
	movq	(%r14), %rax
.Ltmp73:
	movq	%r14, %rdi
	callq	*168(%rax)
	movl	%eax, %ebp
.Ltmp74:
	.p2align	4, 0x90
.LBB5_21:                               # =>This Inner Loop Header: Depth=1
	testl	%ebp, %ebp
	jle	.LBB5_28
# BB#22:                                #   in Loop: Header=BB5_21 Depth=1
	decl	%ebp
	movq	(%r14), %rax
.Ltmp75:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	*176(%rax)
.Ltmp76:
# BB#23:                                #   in Loop: Header=BB5_21 Depth=1
.Ltmp78:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint
.Ltmp79:
	jmp	.LBB5_21
.LBB5_28:                               # %.thread
	movq	(%r14), %rax
.Ltmp81:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp82:
# BB#29:
	testq	%rax, %rax
	je	.LBB5_63
# BB#30:
	movq	(%r14), %rax
.Ltmp83:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp84:
# BB#31:
	movq	(%rax), %rcx
.Ltmp85:
	movq	%rax, %rdi
	callq	*96(%rcx)
.Ltmp86:
# BB#32:
	testb	$3, %al
	je	.LBB5_63
# BB#33:                                # %.preheader203
	cmpl	$0, 12(%r14)
	jle	.LBB5_54
# BB#34:                                # %.lr.ph210
	xorl	%ebp, %ebp
	movq	%rsp, %rbx
	leaq	56(%rsp), %r15
	leaq	40(%rsp), %r12
	.p2align	4, 0x90
.LBB5_35:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movq	24(%r14), %rcx
	movq	(%rcx,%rbp,8), %r13
.Ltmp88:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp89:
# BB#36:                                #   in Loop: Header=BB5_35 Depth=1
	testq	%rax, %rax
	je	.LBB5_48
# BB#37:                                #   in Loop: Header=BB5_35 Depth=1
	movq	(%r14), %rax
.Ltmp90:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp91:
# BB#38:                                #   in Loop: Header=BB5_35 Depth=1
	movq	(%rax), %rcx
.Ltmp92:
	movq	%rax, %rdi
	callq	*96(%rcx)
.Ltmp93:
# BB#39:                                #   in Loop: Header=BB5_35 Depth=1
	testb	$1, %al
	je	.LBB5_48
# BB#40:                                #   in Loop: Header=BB5_35 Depth=1
	movabsq	$4863606123715821568, %rax # imm = 0x437F0000437F0000
	movq	%rax, (%rsp)
	movq	$1132396544, 8(%rsp)    # imm = 0x437F0000
	movl	228(%r13), %eax
	decl	%eax
	cmpl	$5, %eax
	jae	.LBB5_41
# BB#46:                                # %switch.lookup
                                        #   in Loop: Header=BB5_35 Depth=1
	movslq	%eax, %rdx
	movl	.Lswitch.table(,%rdx,4), %ecx
	movl	.Lswitch.table.1(,%rdx,4), %eax
	movl	.Lswitch.table.2(,%rdx,4), %edx
	jmp	.LBB5_47
.LBB5_41:                               #   in Loop: Header=BB5_35 Depth=1
	xorl	%eax, %eax
	movl	$1132396544, %ecx       # imm = 0x437F0000
	xorl	%edx, %edx
.LBB5_47:                               #   in Loop: Header=BB5_35 Depth=1
	movl	%ecx, (%rsp)
	movl	%eax, 4(%rsp)
	movl	%edx, 8(%rsp)
	movl	$0, 12(%rsp)
	leaq	8(%r13), %rsi
	movq	200(%r13), %rdx
.Ltmp94:
	movq	%r14, %rdi
	movq	%rbx, %rcx
	callq	_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3
.Ltmp95:
.LBB5_48:                               #   in Loop: Header=BB5_35 Depth=1
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_53
# BB#49:                                #   in Loop: Header=BB5_35 Depth=1
	movq	(%rdi), %rax
.Ltmp97:
	callq	*96(%rax)
.Ltmp98:
# BB#50:                                #   in Loop: Header=BB5_35 Depth=1
	testb	$2, %al
	je	.LBB5_53
# BB#51:                                #   in Loop: Header=BB5_35 Depth=1
	movq	$1065353216, 40(%rsp)   # imm = 0x3F800000
	movq	$0, 48(%rsp)
	movq	200(%r13), %rdi
	movq	(%rdi), %rax
	addq	$8, %r13
.Ltmp100:
	movq	%r13, %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	*16(%rax)
.Ltmp101:
# BB#52:                                #   in Loop: Header=BB5_35 Depth=1
	movq	120(%r14), %rdi
.Ltmp102:
	movq	%rbx, %rsi
	movq	%r15, %rdx
	movq	%r12, %rcx
	callq	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
.Ltmp103:
.LBB5_53:                               #   in Loop: Header=BB5_35 Depth=1
	incq	%rbp
	movslq	12(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB5_35
.LBB5_54:                               # %._crit_edge
	movq	(%r14), %rax
.Ltmp105:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp106:
# BB#55:
	testq	%rax, %rax
	je	.LBB5_63
# BB#56:
	movq	(%r14), %rax
.Ltmp107:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp108:
# BB#57:
	movq	(%rax), %rcx
.Ltmp109:
	movq	%rax, %rdi
	callq	*96(%rcx)
.Ltmp110:
# BB#58:
	testl	%eax, %eax
	je	.LBB5_63
# BB#59:                                # %.preheader
	cmpl	$0, 340(%r14)
	jle	.LBB5_63
# BB#60:                                # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_61:                               # =>This Inner Loop Header: Depth=1
	movq	120(%r14), %rsi
	movq	352(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
.Ltmp112:
	callq	*24(%rax)
.Ltmp113:
# BB#62:                                #   in Loop: Header=BB5_61 Depth=1
	incq	%rbx
	movslq	340(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB5_61
.LBB5_63:                               # %.loopexit
	callq	_ZN15CProfileManager12Stop_ProfileEv
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB5_43:                               # %.loopexit.split-lp
.Ltmp111:
	jmp	.LBB5_65
.LBB5_45:
.Ltmp96:
	jmp	.LBB5_65
.LBB5_64:
.Ltmp58:
	jmp	.LBB5_65
.LBB5_42:                               # %.loopexit202
.Ltmp114:
	jmp	.LBB5_65
.LBB5_69:
.Ltmp104:
	jmp	.LBB5_65
.LBB5_25:
.Ltmp87:
	jmp	.LBB5_65
.LBB5_68:
.Ltmp61:
	jmp	.LBB5_65
.LBB5_27:
.Ltmp80:
	jmp	.LBB5_65
.LBB5_26:
.Ltmp77:
	jmp	.LBB5_65
.LBB5_44:
.Ltmp99:
	jmp	.LBB5_65
.LBB5_24:
.Ltmp66:
.LBB5_65:
	movq	%rax, %rbx
.Ltmp115:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp116:
# BB#66:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB5_67:
.Ltmp117:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv, .Lfunc_end5-_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\346\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\335\001"              # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp50-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp57-.Ltmp50         #   Call between .Ltmp50 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin3   #     jumps to .Ltmp58
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin3   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp65-.Ltmp62         #   Call between .Ltmp62 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin3   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp67-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp74-.Ltmp67         #   Call between .Ltmp67 and .Ltmp74
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp75-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp76-.Ltmp75         #   Call between .Ltmp75 and .Ltmp76
	.long	.Ltmp77-.Lfunc_begin3   #     jumps to .Ltmp77
	.byte	0                       #   On action: cleanup
	.long	.Ltmp78-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp79-.Ltmp78         #   Call between .Ltmp78 and .Ltmp79
	.long	.Ltmp80-.Lfunc_begin3   #     jumps to .Ltmp80
	.byte	0                       #   On action: cleanup
	.long	.Ltmp81-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp86-.Ltmp81         #   Call between .Ltmp81 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin3   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Ltmp93-.Ltmp88         #   Call between .Ltmp88 and .Ltmp93
	.long	.Ltmp99-.Lfunc_begin3   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin3   # >> Call Site 10 <<
	.long	.Ltmp95-.Ltmp94         #   Call between .Ltmp94 and .Ltmp95
	.long	.Ltmp96-.Lfunc_begin3   #     jumps to .Ltmp96
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin3   # >> Call Site 11 <<
	.long	.Ltmp98-.Ltmp97         #   Call between .Ltmp97 and .Ltmp98
	.long	.Ltmp99-.Lfunc_begin3   #     jumps to .Ltmp99
	.byte	0                       #   On action: cleanup
	.long	.Ltmp100-.Lfunc_begin3  # >> Call Site 12 <<
	.long	.Ltmp103-.Ltmp100       #   Call between .Ltmp100 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin3  #     jumps to .Ltmp104
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin3  # >> Call Site 13 <<
	.long	.Ltmp110-.Ltmp105       #   Call between .Ltmp105 and .Ltmp110
	.long	.Ltmp111-.Lfunc_begin3  #     jumps to .Ltmp111
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin3  # >> Call Site 14 <<
	.long	.Ltmp113-.Ltmp112       #   Call between .Ltmp112 and .Ltmp113
	.long	.Ltmp114-.Lfunc_begin3  #     jumps to .Ltmp114
	.byte	0                       #   On action: cleanup
	.long	.Ltmp113-.Lfunc_begin3  # >> Call Site 15 <<
	.long	.Ltmp115-.Ltmp113       #   Call between .Ltmp113 and .Ltmp115
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp115-.Lfunc_begin3  # >> Call Site 16 <<
	.long	.Ltmp116-.Ltmp115       #   Call between .Ltmp115 and .Ltmp116
	.long	.Ltmp117-.Lfunc_begin3  #     jumps to .Ltmp117
	.byte	1                       #   On action: 1
	.long	.Ltmp116-.Lfunc_begin3  # >> Call Site 17 <<
	.long	.Lfunc_end5-.Ltmp116    #   Call between .Ltmp116 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_0:
	.long	1092616192              # float 10
.LCPI6_1:
	.long	1063675494              # float 0.899999976
.LCPI6_3:
	.long	3226013659              # float -3.14159274
.LCPI6_4:
	.long	1078530011              # float 3.14159274
.LCPI6_5:
	.long	1086506843              # float 6.08683538
.LCPI6_6:
	.long	1086918618              # float 6.283185
.LCPI6_7:
	.long	1023410176              # float 0.03125
.LCPI6_8:
	.long	1086918619              # float 6.28318548
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI6_9:
	.zero	16
	.text
	.globl	_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint,@function
_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint: # @_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi36:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi37:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi39:
	.cfi_def_cfa_offset 512
.Lcfi40:
	.cfi_offset %rbx, -56
.Lcfi41:
	.cfi_offset %r12, -48
.Lcfi42:
	.cfi_offset %r13, -40
.Lcfi43:
	.cfi_offset %r14, -32
.Lcfi44:
	.cfi_offset %r15, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*96(%rcx)
	movl	%eax, %ebp
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	%rax, %rdi
	callq	*96(%rcx)
	movl	%eax, %r15d
	movss	44(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm1, %xmm0
	jae	.LBB6_43
# BB#1:
	movl	8(%r13), %eax
	addl	$-3, %eax
	cmpl	$4, %eax
	ja	.LBB6_43
# BB#2:
	andl	$2048, %ebp             # imm = 0x800
	andl	$4096, %r15d            # imm = 0x1000
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	jmpq	*.LJTI6_0(,%rax,8)
.LBB6_3:
	movl	$1065353216, 16(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 20(%rsp)
	movl	$1065353216, 36(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 40(%rsp)
	movl	$1065353216, 56(%rsp)   # imm = 0x3F800000
	movups	%xmm0, 60(%rsp)
	movl	$0, 76(%rsp)
	movq	348(%r13), %xmm1        # xmm1 = mem[0],zero
	movq	356(%r13), %xmm0        # xmm0 = mem[0],zero
	movq	24(%r13), %rax
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	pshufd	$224, %xmm1, %xmm3      # xmm3 = xmm1[0,0,2,3]
	movss	24(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	mulps	%xmm2, %xmm6
	addps	%xmm5, %xmm6
	pshufd	$224, %xmm0, %xmm3      # xmm3 = xmm0[0,0,2,3]
	movss	32(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	movsd	56(%rax), %xmm3         # xmm3 = mem[0],zero
	addps	%xmm5, %xmm3
	movss	40(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	44(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	%xmm4, %xmm1
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	addss	64(%rax), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, 64(%rsp)
	movlps	%xmm0, 72(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	leaq	16(%rsp), %rsi
	movq	%rax, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	movq	364(%r13), %xmm1        # xmm1 = mem[0],zero
	movq	372(%r13), %xmm0        # xmm0 = mem[0],zero
	movq	32(%r13), %rax
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	pshufd	$224, %xmm1, %xmm3      # xmm3 = xmm1[0,0,2,3]
	movss	24(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	mulps	%xmm2, %xmm6
	addps	%xmm5, %xmm6
	pshufd	$224, %xmm0, %xmm3      # xmm3 = xmm0[0,0,2,3]
	movss	32(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	movsd	56(%rax), %xmm3         # xmm3 = mem[0],zero
	addps	%xmm5, %xmm3
	movss	40(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	44(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	addss	%xmm4, %xmm1
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	addss	64(%rax), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm3, 64(%rsp)
	movlps	%xmm0, 72(%rsp)
	testl	%ebp, %ebp
	je	.LBB6_43
# BB#4:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	leaq	16(%rsp), %rsi
	movq	%rax, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	jmp	.LBB6_43
.LBB6_5:
	movq	24(%r13), %rax
	movss	600(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	movss	616(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	632(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm11
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	604(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	620(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	636(%r13), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	608(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	movss	624(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	640(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	24(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 128(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movss	44(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm4
	addss	%xmm9, %xmm4
	mulss	%xmm7, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm6
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm7, %xmm10
	addss	%xmm2, %xmm10
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	648(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	128(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	652(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	unpcklps	112(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	656(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm7, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 20(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm13, 36(%rsp)
	movss	%xmm8, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm12, 52(%rsp)
	movss	%xmm10, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	testl	%ebp, %ebp
	je	.LBB6_7
# BB#6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	leaq	16(%rsp), %rbp
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	movq	32(%r13), %rax
	movss	664(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	movss	680(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	696(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm11
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	668(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	684(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	700(%r13), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	672(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	movss	688(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	704(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	24(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 128(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movss	44(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm4
	addss	%xmm9, %xmm4
	mulss	%xmm7, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm6
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm7, %xmm10
	addss	%xmm2, %xmm10
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	712(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	128(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	716(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	unpcklps	112(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	720(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm7, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 20(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm13, 36(%rsp)
	movss	%xmm8, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm12, 52(%rsp)
	movss	%xmm10, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	jmp	.LBB6_8
.LBB6_14:
	movq	24(%r13), %rax
	movss	348(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	movss	364(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	380(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm11
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	352(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	368(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	384(%r13), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	356(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	movss	372(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	388(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	24(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 128(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movss	44(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm4
	addss	%xmm9, %xmm4
	mulss	%xmm7, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm6
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm7, %xmm10
	addss	%xmm2, %xmm10
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	396(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	128(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	400(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	unpcklps	112(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	404(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm7, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 20(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm13, 36(%rsp)
	movss	%xmm8, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm12, 52(%rsp)
	movss	%xmm10, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	testl	%ebp, %ebp
	je	.LBB6_16
# BB#15:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	leaq	16(%rsp), %rbp
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	movq	32(%r13), %rax
	movss	412(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	movss	428(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	444(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm11
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	416(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	432(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	448(%r13), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	420(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	movss	436(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	452(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	24(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 128(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movss	44(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm4
	addss	%xmm9, %xmm4
	mulss	%xmm7, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm6
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm7, %xmm10
	addss	%xmm2, %xmm10
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	460(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	128(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	464(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	unpcklps	112(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	468(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm7, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 20(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm13, 36(%rsp)
	movss	%xmm8, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm12, 52(%rsp)
	movss	%xmm10, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	testl	%r15d, %r15d
	jne	.LBB6_18
	jmp	.LBB6_43
.LBB6_26:
	movups	1040(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)
	leaq	32(%rsp), %r12
	movups	1056(%r13), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	1072(%r13), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	1088(%r13), %xmm0
	movaps	%xmm0, 64(%rsp)
	testl	%ebp, %ebp
	je	.LBB6_28
# BB#27:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	leaq	16(%rsp), %rbp
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	movups	1104(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	1120(%r13), %xmm0
	movups	%xmm0, (%r12)
	movups	1136(%r13), %xmm0
	movups	%xmm0, 16(%r12)
	movups	1152(%r13), %xmm0
	movups	%xmm0, 32(%r12)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	testl	%r15d, %r15d
	jne	.LBB6_30
	jmp	.LBB6_43
.LBB6_36:
	movups	844(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)
	leaq	32(%rsp), %r14
	movups	860(%r13), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	876(%r13), %xmm0
	movaps	%xmm0, 48(%rsp)
	movups	892(%r13), %xmm0
	movaps	%xmm0, 64(%rsp)
	testl	%ebp, %ebp
	je	.LBB6_38
# BB#37:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	leaq	16(%rsp), %rbp
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	movups	908(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	924(%r13), %xmm0
	movups	%xmm0, (%r14)
	movups	940(%r13), %xmm0
	movups	%xmm0, 16(%r14)
	movups	956(%r13), %xmm0
	movups	%xmm0, 32(%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	testl	%r15d, %r15d
	jne	.LBB6_40
	jmp	.LBB6_43
.LBB6_7:                                # %.critedge
	movq	32(%r13), %rax
	movss	664(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	movss	680(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	696(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm11
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	668(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	684(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	700(%r13), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	672(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	movss	688(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	704(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	24(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 128(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movss	44(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm4
	addss	%xmm9, %xmm4
	mulss	%xmm7, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm6
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm7, %xmm10
	addss	%xmm2, %xmm10
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	712(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	128(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	716(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	unpcklps	112(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	720(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm7, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 20(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm13, 36(%rsp)
	movss	%xmm8, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm12, 52(%rsp)
	movss	%xmm10, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
.LBB6_8:
	movss	748(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	752(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jne	.LBB6_9
	jnp	.LBB6_43
.LBB6_9:
	testl	%r15d, %r15d
	je	.LBB6_43
# BB#10:
	leaq	64(%rsp), %rbp
	ucomiss	%xmm1, %xmm0
	movaps	%xmm1, %xmm2
	cmpltss	%xmm0, %xmm2
	andnps	%xmm0, %xmm2
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	ja	.LBB6_11
# BB#12:
	movss	%xmm1, 80(%rsp)         # 4-byte Spill
	jmp	.LBB6_13
.LBB6_16:                               # %.critedge241
	movq	32(%r13), %rax
	movss	412(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm1, %xmm3
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	movss	428(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm8
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	movss	444(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm11
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	416(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movss	432(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movss	448(%r13), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	420(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	movss	436(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm0, %xmm3
	movss	452(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	24(%rax), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	mulss	%xmm11, %xmm0
	movss	28(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm8, 128(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm3
	movss	32(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm1, %xmm15
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm15
	movaps	%xmm9, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm12, %xmm13
	mulss	%xmm1, %xmm13
	addss	%xmm3, %xmm13
	movaps	%xmm6, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm8
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	movss	40(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	movss	44(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm7, %xmm5
	movss	48(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm14
	addss	%xmm5, %xmm14
	mulss	%xmm0, %xmm9
	mulss	%xmm3, %xmm4
	addss	%xmm9, %xmm4
	mulss	%xmm7, %xmm12
	addss	%xmm4, %xmm12
	mulss	%xmm0, %xmm6
	mulss	%xmm3, %xmm2
	addss	%xmm6, %xmm2
	mulss	%xmm7, %xmm10
	addss	%xmm2, %xmm10
	movaps	176(%rsp), %xmm2        # 16-byte Reload
	unpcklps	%xmm11, %xmm2   # xmm2 = xmm2[0],xmm11[0],xmm2[1],xmm11[1]
	movss	460(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	movaps	208(%rsp), %xmm4        # 16-byte Reload
	unpcklps	128(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	464(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movaps	192(%rsp), %xmm4        # 16-byte Reload
	unpcklps	112(%rsp), %xmm4 # 16-byte Folded Reload
                                        # xmm4 = xmm4[0],mem[0],xmm4[1],mem[1]
	movss	468(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movsd	56(%rax), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	%xmm3, %xmm0
	addss	%xmm7, %xmm0
	addss	64(%rax), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 20(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 24(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm15, 32(%rsp)
	movss	%xmm13, 36(%rsp)
	movss	%xmm8, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	%xmm14, 48(%rsp)
	movss	%xmm12, 52(%rsp)
	movss	%xmm10, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm2, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	testl	%r15d, %r15d
	je	.LBB6_43
.LBB6_18:
	movq	%rbx, 96(%rsp)          # 8-byte Spill
	movss	.LCPI6_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movq	%r13, %rdi
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	callq	_ZNK21btConeTwistConstraint16GetPointForAngleEff
	movaps	%xmm0, %xmm2
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	32(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movss	20(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	36(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	mulps	%xmm0, %xmm4
	addps	%xmm2, %xmm4
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	movss	56(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	addps	%xmm4, %xmm1
	movsd	64(%rsp), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm1, %xmm4
	movss	52(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm1, %xmm2
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	72(%rsp), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 240(%rsp)
	movlps	%xmm0, 248(%rsp)
	xorl	%r15d, %r15d
	leaq	240(%rsp), %r12
	leaq	160(%rsp), %r14
	leaq	144(%rsp), %rbx
	.p2align	4, 0x90
.LBB6_19:                               # =>This Inner Loop Header: Depth=1
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r15d, %xmm0
	mulss	.LCPI6_6(%rip), %xmm0
	mulss	.LCPI6_7(%rip), %xmm0
	movq	%r13, %rdi
	callq	_ZNK21btConeTwistConstraint16GetPointForAngleEff
	movaps	%xmm0, %xmm2
	movss	48(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	32(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movss	20(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movss	36(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	mulps	%xmm0, %xmm4
	addps	%xmm2, %xmm4
	movss	40(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	movss	56(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm5, %xmm1
	addps	%xmm4, %xmm1
	movsd	64(%rsp), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm1, %xmm4
	movss	52(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	addss	%xmm1, %xmm2
	addss	72(%rsp), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, 160(%rsp)
	movlps	%xmm0, 168(%rsp)
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	*%rbp
	testb	$3, %r15b
	jne	.LBB6_21
# BB#20:                                #   in Loop: Header=BB6_19 Depth=1
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movq	%rax, %rdi
	leaq	64(%rsp), %rsi
	movq	%r14, %rdx
	movq	%rbx, %rcx
	callq	*%rbp
.LBB6_21:                               #   in Loop: Header=BB6_19 Depth=1
	movups	160(%rsp), %xmm0
	movaps	%xmm0, 240(%rsp)
	incl	%r15d
	cmpl	$32, %r15d
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	jne	.LBB6_19
# BB#22:
	movss	500(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movss	560(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movq	32(%r13), %rax
	movss	360(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB6_24
# BB#23:
	movss	8(%rax), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	412(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	416(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm14
	movss	428(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	444(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm0, 400(%rsp)        # 16-byte Spill
	movaps	%xmm12, %xmm0
	mulss	%xmm9, %xmm0
	movaps	%xmm0, 384(%rsp)        # 16-byte Spill
	movss	448(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm2, 368(%rsp)        # 16-byte Spill
	movss	40(%rax), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm1
	movss	44(%rax), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm2
	mulss	%xmm10, %xmm2
	addss	%xmm1, %xmm2
	movss	48(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	addss	%xmm2, %xmm5
	movss	%xmm5, 208(%rsp)        # 4-byte Spill
	movaps	%xmm4, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 352(%rsp)        # 16-byte Spill
	movss	452(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 336(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm1
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	mulss	%xmm13, %xmm9
	movss	432(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movaps	%xmm3, %xmm2
	movaps	%xmm3, %xmm5
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	movss	436(%r13), %xmm11       # xmm11 = mem[0],zero,zero,zero
	unpcklps	%xmm11, %xmm6   # xmm6 = xmm6[0],xmm11[0],xmm6[1],xmm11[1]
	movaps	%xmm6, 288(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	unpcklps	%xmm1, %xmm11   # xmm11 = xmm11[0],xmm1[0],xmm11[1],xmm1[1]
	mulss	%xmm10, %xmm1
	addss	%xmm9, %xmm1
	mulss	%xmm4, %xmm2
	movaps	%xmm2, 320(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm4
	addss	%xmm1, %xmm4
	movss	%xmm4, 192(%rsp)        # 4-byte Spill
	movss	420(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	mulss	%xmm4, %xmm2
	movaps	%xmm4, %xmm1
	mulss	%xmm13, %xmm4
	mulss	%xmm10, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm7, %xmm5
	mulss	%xmm8, %xmm7
	addss	%xmm6, %xmm7
	movss	%xmm7, 176(%rsp)        # 4-byte Spill
	mulss	%xmm12, %xmm14
	movaps	%xmm14, 304(%rsp)       # 16-byte Spill
	movss	460(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm12
	movss	12(%rax), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movss	464(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm4, %xmm15
	addss	%xmm12, %xmm15
	movss	24(%rax), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm12, %xmm6
	movaps	%xmm6, 128(%rsp)        # 16-byte Spill
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	mulss	%xmm12, %xmm6
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	mulss	%xmm12, %xmm1
	mulss	%xmm9, %xmm12
	movss	28(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm4, %xmm7
	addss	%xmm12, %xmm7
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	unpcklps	288(%rsp), %xmm11 # 16-byte Folded Reload
                                        # xmm11 = xmm11[0],mem[0],xmm11[1],mem[1]
	movss	468(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm8
	shufps	$0, %xmm14, %xmm0       # xmm0 = xmm0[0,0],xmm14[0,0]
	unpcklps	%xmm6, %xmm14   # xmm14 = xmm14[0],xmm6[0],xmm14[1],xmm6[1]
	unpcklps	%xmm6, %xmm6    # xmm6 = xmm6[0,0,1,1]
	unpcklps	%xmm6, %xmm14   # xmm14 = xmm14[0],xmm6[0],xmm14[1],xmm6[1]
	mulps	%xmm11, %xmm14
	mulps	%xmm3, %xmm0
	movaps	128(%rsp), %xmm3        # 16-byte Reload
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	unpcklps	112(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	unpcklps	384(%rsp), %xmm7 # 16-byte Folded Reload
                                        # xmm7 = xmm7[0],mem[0],xmm7[1],mem[1]
	unpcklps	304(%rsp), %xmm15 # 16-byte Folded Reload
                                        # xmm15 = xmm15[0],mem[0],xmm15[1],mem[1]
	unpcklps	%xmm7, %xmm15   # xmm15 = xmm15[0],xmm7[0],xmm15[1],xmm7[1]
	addps	%xmm0, %xmm15
	addps	%xmm14, %xmm2
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	unpcklps	336(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	unpcklps	352(%rsp), %xmm5 # 16-byte Folded Reload
                                        # xmm5 = xmm5[0],mem[0],xmm5[1],mem[1]
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movss	60(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	320(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movss	56(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	400(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	addps	%xmm2, %xmm5
	addps	%xmm15, %xmm0
	mulss	%xmm9, %xmm13
	mulss	%xmm4, %xmm10
	addss	%xmm13, %xmm10
	jmp	.LBB6_25
.LBB6_28:                               # %.critedge242
	movups	1104(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	1120(%r13), %xmm0
	movups	%xmm0, (%r12)
	movups	1136(%r13), %xmm0
	movups	%xmm0, 16(%r12)
	movups	1152(%r13), %xmm0
	movups	%xmm0, 32(%r12)
	testl	%r15d, %r15d
	je	.LBB6_43
.LBB6_30:
	leaq	1040(%r13), %r14
	leaq	1056(%r13), %r15
	leaq	1072(%r13), %rcx
	leaq	1088(%r13), %rax
	movups	(%r14), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	(%r15), %xmm0
	movups	%xmm0, (%r12)
	movq	%rcx, 208(%rsp)         # 8-byte Spill
	movups	(%rcx), %xmm0
	movups	%xmm0, 16(%r12)
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%r12)
	leaq	1152(%r13), %rbp
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	56(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, 240(%rsp)
	movlps	%xmm1, 248(%rsp)
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movss	924(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	928(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movss	980(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	movss	984(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 176(%rsp)        # 4-byte Spill
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movss	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	movaps	%xmm1, 144(%rsp)
	leaq	240(%rsp), %rdx
	leaq	160(%rsp), %rcx
	leaq	144(%rsp), %r8
	movss	.LCPI6_0(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	96(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	192(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	176(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	callq	_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f
	movss	20(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	52(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movl	$1, %esi
	movq	%r13, %rdi
	callq	_ZNK23btGeneric6DofConstraint8getAngleEi
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movl	$2, %esi
	movq	%r13, %rdi
	callq	_ZNK23btGeneric6DofConstraint8getAngleEi
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	80(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	movss	80(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	%xmm0, 176(%rsp)        # 4-byte Spill
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movss	192(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm1
	movss	176(%rsp), %xmm6        # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	movss	160(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	164(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movaps	%xmm5, %xmm4
	movaps	%xmm5, %xmm8
	mulss	%xmm0, %xmm4
	mulss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	movss	168(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm5
	mulss	%xmm1, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, 144(%rsp)
	movaps	%xmm7, %xmm4
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm0
	movaps	%xmm6, %xmm5
	mulss	%xmm5, %xmm4
	mulss	%xmm3, %xmm5
	subss	%xmm0, %xmm5
	movss	%xmm5, 148(%rsp)
	mulss	%xmm2, %xmm4
	mulss	%xmm3, %xmm7
	addss	%xmm4, %xmm7
	movaps	%xmm8, %xmm0
	mulss	%xmm1, %xmm0
	addss	%xmm7, %xmm0
	movss	%xmm0, 152(%rsp)
	movups	1104(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	1120(%r13), %xmm0
	movups	%xmm0, (%r12)
	movups	1136(%r13), %xmm0
	movups	%xmm0, 16(%r12)
	movups	1152(%r13), %xmm0
	movups	%xmm0, 32(%r12)
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movaps	.LCPI6_2(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	movss	48(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 272(%rsp)
	movlps	%xmm1, 280(%rsp)
	movss	868(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	872(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	jbe	.LBB6_32
# BB#31:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 256(%rsp)
	leaq	272(%rsp), %rdx
	leaq	144(%rsp), %rcx
	movss	.LCPI6_3(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movss	.LCPI6_4(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	leaq	256(%rsp), %r8
	movss	.LCPI6_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	xorl	%r9d, %r9d
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	jmp	.LBB6_34
.LBB6_38:                               # %.critedge243
	movups	908(%r13), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	924(%r13), %xmm0
	movups	%xmm0, (%r14)
	movups	940(%r13), %xmm0
	movups	%xmm0, 16(%r14)
	movups	956(%r13), %xmm0
	movups	%xmm0, 32(%r14)
	testl	%r15d, %r15d
	je	.LBB6_43
.LBB6_40:
	movss	876(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movsd	892(%r13), %xmm9        # xmm9 = mem[0],zero
	movss	900(%r13), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	860(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	844(%r13), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	848(%r13), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	864(%r13), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	868(%r13), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	852(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	movss	880(%r13), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	mulss	884(%r13), %xmm2
	movss	880(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	232(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm7    # xmm7 = xmm7[0],xmm5[0],xmm7[1],xmm5[1]
	movaps	%xmm1, %xmm5
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm7, %xmm0
	movaps	%xmm7, %xmm12
	movaps	%xmm12, 208(%rsp)       # 16-byte Spill
	unpcklps	%xmm6, %xmm11   # xmm11 = xmm11[0],xmm6[0],xmm11[1],xmm6[1]
	movaps	%xmm11, 80(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm6
	xorps	%xmm7, %xmm7
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm0
	unpcklps	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1]
	mulps	%xmm7, %xmm4
	addps	%xmm4, %xmm0
	addps	%xmm9, %xmm0
	addss	%xmm3, %xmm5
	addss	%xmm2, %xmm5
	addss	%xmm8, %xmm5
	xorps	%xmm7, %xmm7
	movss	%xmm5, %xmm7            # xmm7 = xmm5[0],xmm7[1,2,3]
	movlps	%xmm0, 240(%rsp)
	movlps	%xmm7, 248(%rsp)
	movss	236(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	addps	%xmm6, %xmm0
	addps	%xmm4, %xmm0
	addps	%xmm9, %xmm0
	addss	%xmm3, %xmm5
	addss	%xmm2, %xmm5
	addss	%xmm8, %xmm5
	xorps	%xmm1, %xmm1
	movss	%xmm5, %xmm1            # xmm1 = xmm5[0],xmm1[1,2,3]
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	leaq	240(%rsp), %rsi
	leaq	160(%rsp), %rdx
	leaq	144(%rsp), %rcx
	movq	%rax, %rdi
	callq	*%rbp
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 144(%rsp)
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 152(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 272(%rsp)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 280(%rsp)
	movss	240(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 96(%rsp)         # 4-byte Spill
	movss	244(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	addq	$956, %r13              # imm = 0x3BC
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 256(%rsp)
	leaq	144(%rsp), %rdx
	leaq	272(%rsp), %rcx
	leaq	256(%rsp), %r8
	movss	.LCPI6_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movl	$1, %r9d
	movq	%rax, %rdi
	movq	%r13, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	movss	96(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	jmp	.LBB6_41
.LBB6_24:
	movq	24(%r13), %rax
	movss	348(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm11
	movss	364(%r13), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	380(%r13), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm7, %xmm0
	movaps	%xmm0, 400(%rsp)        # 16-byte Spill
	movss	352(%r13), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm9, %xmm0
	movaps	%xmm0, 384(%rsp)        # 16-byte Spill
	movss	384(%r13), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm0, %xmm4
	movaps	%xmm4, 368(%rsp)        # 16-byte Spill
	movss	40(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movaps	%xmm4, %xmm6
	movss	44(%rax), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm4
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	movss	48(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movss	%xmm5, 208(%rsp)        # 4-byte Spill
	movaps	%xmm12, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 352(%rsp)        # 16-byte Spill
	movss	388(%r13), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 336(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm1
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm9
	movaps	%xmm6, %xmm14
	movss	%xmm14, 304(%rsp)       # 4-byte Spill
	movss	368(%r13), %xmm1        # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movaps	%xmm7, %xmm2
	movaps	%xmm7, %xmm5
	unpcklps	%xmm15, %xmm7   # xmm7 = xmm7[0],xmm15[0],xmm7[1],xmm15[1]
	movss	372(%r13), %xmm13       # xmm13 = mem[0],zero,zero,zero
	unpcklps	%xmm13, %xmm15  # xmm15 = xmm15[0],xmm13[0],xmm15[1],xmm13[1]
	movaps	%xmm13, %xmm6
	unpcklps	%xmm1, %xmm13   # xmm13 = xmm13[0],xmm1[0],xmm13[1],xmm1[1]
	mulss	%xmm10, %xmm1
	addss	%xmm9, %xmm1
	mulss	%xmm12, %xmm2
	movaps	%xmm2, 320(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm12
	addss	%xmm1, %xmm12
	movss	%xmm12, 192(%rsp)       # 4-byte Spill
	movss	356(%r13), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm2, %xmm1
	movaps	%xmm2, %xmm12
	mulss	%xmm14, %xmm2
	mulss	%xmm10, %xmm6
	addss	%xmm2, %xmm6
	mulss	%xmm4, %xmm5
	mulss	%xmm8, %xmm4
	addss	%xmm6, %xmm4
	movss	%xmm4, 176(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm11
	movaps	%xmm11, 288(%rsp)       # 16-byte Spill
	movss	396(%r13), %xmm11       # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm3
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	400(%r13), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm9
	mulss	%xmm14, %xmm9
	addss	%xmm3, %xmm9
	movss	24(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm4, %xmm3
	movaps	%xmm3, 128(%rsp)        # 16-byte Spill
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm4, %xmm3
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	mulss	%xmm4, %xmm12
	mulss	%xmm11, %xmm4
	movss	28(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm14, %xmm3
	addss	%xmm4, %xmm3
	unpcklps	%xmm0, %xmm7    # xmm7 = xmm7[0],xmm0[0],xmm7[1],xmm0[1]
	unpcklps	%xmm15, %xmm13  # xmm13 = xmm13[0],xmm15[0],xmm13[1],xmm15[1]
	movss	404(%r13), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm8
	shufps	$0, %xmm2, %xmm0        # xmm0 = xmm0[0,0],xmm2[0,0]
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	unpcklps	%xmm6, %xmm6    # xmm6 = xmm6[0,0,1,1]
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	mulps	%xmm13, %xmm2
	mulps	%xmm7, %xmm0
	movaps	128(%rsp), %xmm4        # 16-byte Reload
	unpcklps	%xmm12, %xmm4   # xmm4 = xmm4[0],xmm12[0],xmm4[1],xmm12[1]
	unpcklps	112(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	384(%rsp), %xmm3 # 16-byte Folded Reload
                                        # xmm3 = xmm3[0],mem[0],xmm3[1],mem[1]
	unpcklps	288(%rsp), %xmm9 # 16-byte Folded Reload
                                        # xmm9 = xmm9[0],mem[0],xmm9[1],mem[1]
	unpcklps	%xmm3, %xmm9    # xmm9 = xmm9[0],xmm3[0],xmm9[1],xmm3[1]
	addps	%xmm0, %xmm9
	addps	%xmm2, %xmm1
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	unpcklps	336(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	unpcklps	352(%rsp), %xmm5 # 16-byte Folded Reload
                                        # xmm5 = xmm5[0],mem[0],xmm5[1],mem[1]
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movss	60(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	320(%rsp), %xmm2 # 16-byte Folded Reload
                                        # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	movss	56(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	400(%rsp), %xmm0 # 16-byte Folded Reload
                                        # xmm0 = xmm0[0],mem[0],xmm0[1],mem[1]
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	addps	%xmm1, %xmm5
	addps	%xmm9, %xmm0
	movss	304(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm1
	mulss	%xmm14, %xmm10
	addss	%xmm1, %xmm10
.LBB6_25:
	movq	96(%rsp), %rdi          # 8-byte Reload
	addss	%xmm10, %xmm8
	addss	64(%rax), %xmm8
	xorps	%xmm1, %xmm1
	movss	%xmm8, %xmm1            # xmm1 = xmm8[0],xmm1[1,2,3]
	movaps	%xmm0, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movss	%xmm2, 16(%rsp)
	movaps	%xmm0, %xmm2
	shufps	$231, %xmm2, %xmm2      # xmm2 = xmm2[3,1,2,3]
	movss	%xmm2, 20(%rsp)
	movss	%xmm5, 24(%rsp)
	movl	$0, 28(%rsp)
	movaps	%xmm5, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	%xmm2, 32(%rsp)
	movaps	%xmm5, %xmm2
	movhlps	%xmm2, %xmm2            # xmm2 = xmm2[1,1]
	movss	%xmm2, 36(%rsp)
	movaps	%xmm5, %xmm2
	shufps	$231, %xmm2, %xmm2      # xmm2 = xmm2[3,1,2,3]
	movss	%xmm2, 40(%rsp)
	movl	$0, 44(%rsp)
	movss	208(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 48(%rsp)
	movss	192(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 52(%rsp)
	movss	176(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 56(%rsp)
	movl	$0, 60(%rsp)
	movlps	%xmm0, 64(%rsp)
	movlps	%xmm1, 72(%rsp)
	leaq	64(%rsp), %rax
	movups	(%rax), %xmm1
	movaps	%xmm1, 160(%rsp)
	movaps	%xmm5, %xmm1
	shufps	$33, %xmm0, %xmm1       # xmm1 = xmm1[1,0],xmm0[2,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movlps	%xmm1, 144(%rsp)
	movlps	%xmm2, 152(%rsp)
	shufps	$50, %xmm0, %xmm5       # xmm5 = xmm5[2,0],xmm0[3,0]
	shufps	$226, %xmm0, %xmm5      # xmm5 = xmm5[2,0],xmm0[2,3]
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, 272(%rsp)
	movlps	%xmm0, 280(%rsp)
	movq	(%rdi), %rax
	callq	*32(%rax)
	movaps	.LCPI6_2(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	xorps	%xmm0, %xmm2
	movss	80(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	subss	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 256(%rsp)
	leaq	160(%rsp), %rsi
	leaq	144(%rsp), %rdx
	leaq	272(%rsp), %rcx
	leaq	256(%rsp), %r8
	movss	.LCPI6_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movl	$1, %r9d
	movq	%rax, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	jmp	.LBB6_42
.LBB6_32:
	ucomiss	%xmm0, %xmm1
	jbe	.LBB6_35
# BB#33:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	movss	%xmm1, 96(%rsp)         # 4-byte Spill
	callq	*32(%rax)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 256(%rsp)
	leaq	272(%rsp), %rdx
	leaq	144(%rsp), %rcx
	leaq	256(%rsp), %r8
	movss	.LCPI6_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movl	$1, %r9d
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	movss	80(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB6_34:
	callq	_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf
.LBB6_35:
	movups	(%r14), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	(%r15), %xmm0
	movups	%xmm0, (%r12)
	movq	208(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	%xmm0, 16(%r12)
	movq	224(%rsp), %rax         # 8-byte Reload
	movups	(%rax), %xmm0
	movups	%xmm0, 32(%r12)
	movups	728(%r13), %xmm0
	movaps	%xmm0, 256(%rsp)
	movups	744(%r13), %xmm0
	movaps	%xmm0, 432(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 416(%rsp)
	leaq	256(%rsp), %rsi
	leaq	432(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	416(%rsp), %r8
	movq	%rax, %rdi
	callq	_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_
	jmp	.LBB6_43
.LBB6_11:
	movss	.LCPI6_8(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
.LBB6_13:
	setbe	%r14b
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	56(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, 240(%rsp)
	movlps	%xmm1, 248(%rsp)
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movss	48(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movlps	%xmm0, 160(%rsp)
	movlps	%xmm1, 168(%rsp)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 144(%rsp)
	movzbl	%r14b, %r9d
	leaq	240(%rsp), %rdx
	leaq	160(%rsp), %rcx
	leaq	144(%rsp), %r8
	movss	.LCPI6_0(%rip), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movq	%rax, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	movaps	96(%rsp), %xmm2         # 16-byte Reload
.LBB6_41:
	movss	80(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB6_42:
	callq	_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf
.LBB6_43:
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint, .Lfunc_end6-_ZN23btDiscreteDynamicsWorld19debugDrawConstraintEP17btTypedConstraint
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI6_0:
	.quad	.LBB6_3
	.quad	.LBB6_5
	.quad	.LBB6_14
	.quad	.LBB6_26
	.quad	.LBB6_36

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI7_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI7_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI7_1:
	.long	1060439283              # float 0.707106769
.LCPI7_2:
	.long	1065353216              # float 1
.LCPI7_4:
	.long	3267887104              # float -100
.LCPI7_5:
	.long	1120403456              # float 100
.LCPI7_6:
	.long	1056964608              # float 0.5
.LCPI7_7:
	.long	0                       # float 0
	.text
	.globl	_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3,@function
_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3: # @_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi49:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi50:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 56
	subq	$504, %rsp              # imm = 0x1F8
.Lcfi52:
	.cfi_def_cfa_offset 560
.Lcfi53:
	.cfi_offset %rbx, -56
.Lcfi54:
	.cfi_offset %r12, -48
.Lcfi55:
	.cfi_offset %r13, -40
.Lcfi56:
	.cfi_offset %r14, -32
.Lcfi57:
	.cfi_offset %r15, -24
.Lcfi58:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movups	48(%r14), %xmm0
	movaps	%xmm0, 96(%rsp)
	movq	(%r15), %rax
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbx
	movss	20(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	xorps	%xmm0, %xmm0
	mulps	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	movss	16(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	addps	%xmm2, %xmm1
	movss	24(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	%xmm3, %xmm2
	addps	%xmm1, %xmm2
	xorps	%xmm3, %xmm3
	movss	36(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	32(%r14), %xmm0
	movss	40(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	addps	96(%rsp), %xmm2
	addss	104(%rsp), %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movlps	%xmm2, 240(%rsp)
	movlps	%xmm0, 248(%rsp)
	movq	$1065353216, (%rsp)     # imm = 0x3F800000
	movq	$0, 8(%rsp)
	leaq	96(%rsp), %rbp
	leaq	240(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	*%rbx
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbx
	movss	16(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	xorps	%xmm3, %xmm3
	mulps	%xmm3, %xmm1
	movss	20(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	addps	%xmm1, %xmm2
	movss	24(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm3, %xmm1
	addps	%xmm2, %xmm1
	movss	32(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	mulss	%xmm3, %xmm0
	addss	36(%r14), %xmm0
	movss	40(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	addps	96(%rsp), %xmm1
	addss	104(%rsp), %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm1, 240(%rsp)
	movlps	%xmm0, 248(%rsp)
	movabsq	$4575657221408423936, %rcx # imm = 0x3F80000000000000
	movq	%rcx, (%rsp)
	movq	$0, 8(%rsp)
	leaq	240(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	*%rbx
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbx
	movss	16(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	xorps	%xmm3, %xmm3
	mulps	%xmm3, %xmm1
	movss	20(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	%xmm3, %xmm2
	xorps	%xmm3, %xmm3
	addps	%xmm1, %xmm2
	movss	24(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	addps	%xmm2, %xmm1
	movss	32(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	mulss	%xmm2, %xmm0
	mulss	36(%r14), %xmm2
	addss	%xmm0, %xmm2
	addss	40(%r14), %xmm2
	addps	96(%rsp), %xmm1
	addss	104(%rsp), %xmm2
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm1, 240(%rsp)
	movlps	%xmm3, 248(%rsp)
	movq	$0, (%rsp)
	movq	$1065353216, 8(%rsp)    # imm = 0x3F800000
	leaq	240(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	*%rbx
	movl	8(%r12), %eax
	leal	-8(%rax), %ecx
	cmpl	$23, %ecx
	ja	.LBB7_23
# BB#1:
	jmpq	*.LJTI7_0(,%rcx,8)
.LBB7_5:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*88(%rax)
	movq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r13, %rdx
	callq	_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3
	jmp	.LBB7_9
.LBB7_23:
	leaq	48(%r14), %rbx
	leal	-21(%rax), %ecx
	cmpl	$8, %ecx
	ja	.LBB7_27
# BB#24:
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, (%rsp)
	movq	$1566444395, 8(%rsp)    # imm = 0x5D5E0B6B
	movabsq	$-2495544585613341845, %rax # imm = 0xDD5E0B6BDD5E0B6B
	movq	%rax, 16(%rsp)
	movl	$3713928043, %eax       # imm = 0xDD5E0B6B
	movq	%rax, 24(%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movl	$_ZTV17DebugDrawcallback+64, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTV17DebugDrawcallback+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 96(%rsp)
	movq	%rax, 112(%rsp)
	movups	(%r13), %xmm0
	movups	%xmm0, 120(%rsp)
	movups	(%r14), %xmm0
	movups	%xmm0, 136(%rsp)
	movups	16(%r14), %xmm0
	movups	%xmm0, 152(%rsp)
	movups	32(%r14), %xmm0
	movups	%xmm0, 168(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 184(%rsp)
	movq	(%r12), %rax
.Ltmp118:
	leaq	96(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%r12, %rdi
	callq	*96(%rax)
.Ltmp119:
# BB#25:
	leaq	104(%rsp), %rdi
.Ltmp129:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp130:
# BB#26:                                # %_ZN17DebugDrawcallbackD2Ev.exit329
	leaq	96(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
	movl	8(%r12), %eax
.LBB7_27:
	cmpl	$3, %eax
	jne	.LBB7_31
# BB#28:
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, 208(%rsp)
	movq	$1566444395, 216(%rsp)  # imm = 0x5D5E0B6B
	movabsq	$-2495544585613341845, %rax # imm = 0xDD5E0B6BDD5E0B6B
	movq	%rax, 448(%rsp)
	movl	$3713928043, %eax       # imm = 0xDD5E0B6B
	movq	%rax, 456(%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movl	$_ZTV17DebugDrawcallback+64, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTV17DebugDrawcallback+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 240(%rsp)
	movq	%rax, 256(%rsp)
	movups	(%r13), %xmm0
	movups	%xmm0, 264(%rsp)
	movups	(%r14), %xmm0
	movups	%xmm0, 280(%rsp)
	movups	16(%r14), %xmm0
	movups	%xmm0, 296(%rsp)
	movups	32(%r14), %xmm0
	movups	%xmm0, 312(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 328(%rsp)
	movq	104(%r12), %rdi
	movq	(%rdi), %rax
	leaq	248(%rsp), %rbp
.Ltmp135:
	leaq	448(%rsp), %rdx
	leaq	208(%rsp), %rcx
	movq	%rbp, %rsi
	callq	*16(%rax)
.Ltmp136:
# BB#29:
.Ltmp147:
	movq	%rbp, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp148:
# BB#30:                                # %_ZN17DebugDrawcallbackD2Ev.exit313
	leaq	240(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
	movl	8(%r12), %eax
.LBB7_31:
	cmpl	$6, %eax
	jg	.LBB7_9
# BB#32:
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*152(%rax)
	testl	%eax, %eax
	jle	.LBB7_9
# BB#33:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB7_34:                               # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rax
	movq	%r12, %rdi
	movl	%ebp, %esi
	leaq	96(%rsp), %rdx
	leaq	240(%rsp), %rcx
	callq	*160(%rax)
	movss	96(%rsp), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	20(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm9, %xmm5
	addps	%xmm3, %xmm5
	movss	24(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm5, %xmm2
	movsd	48(%r14), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm2
	movss	32(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm7
	movss	36(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm4
	addss	%xmm7, %xmm4
	movss	40(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	addss	%xmm4, %xmm0
	movss	56(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm0
	xorps	%xmm6, %xmm6
	movss	%xmm0, %xmm6            # xmm6 = xmm0[0],xmm6[1,2,3]
	movlps	%xmm2, (%rsp)
	movlps	%xmm6, 8(%rsp)
	movss	240(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	244(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	248(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm9, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm1, %xmm0
	addps	%xmm8, %xmm0
	mulss	%xmm10, %xmm5
	mulss	%xmm11, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm7, %xmm6
	addss	%xmm2, %xmm6
	addss	%xmm4, %xmm6
	xorps	%xmm1, %xmm1
	movss	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rbx
	movq	%rax, %rdi
	movq	%rsp, %rsi
	leaq	16(%rsp), %rdx
	movq	%r13, %rcx
	callq	*40(%rbx)
	incl	%ebp
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*152(%rax)
	cmpl	%eax, %ebp
	jl	.LBB7_34
	jmp	.LBB7_9
.LBB7_15:
	movss	76(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	60(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm5, %xmm10
	mulss	%xmm4, %xmm10
	movss	68(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm5
	movaps	.LCPI7_0(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm7, %xmm1
	ucomiss	.LCPI7_1(%rip), %xmm1
	jbe	.LBB7_19
# BB#16:
	mulss	%xmm4, %xmm4
	mulss	%xmm7, %xmm7
	addss	%xmm4, %xmm7
	xorps	%xmm0, %xmm0
	sqrtss	%xmm7, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB7_18
# BB#17:                                # %call.sqrt
	movaps	%xmm7, %xmm0
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	80(%rsp), %xmm7         # 16-byte Reload
	movaps	64(%rsp), %xmm9         # 16-byte Reload
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	32(%rsp), %xmm5         # 16-byte Reload
.LBB7_18:                               # %.split
	movss	.LCPI7_2(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	68(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	xorps	.LCPI7_3(%rip), %xmm1
	mulss	%xmm2, %xmm7
	mulss	64(%r12), %xmm2
	movss	60(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm1, %xmm4
	mulss	%xmm3, %xmm4
	mulss	.LCPI7_5(%rip), %xmm2
	xorps	%xmm3, %xmm3
	jmp	.LBB7_22
.LBB7_6:
	movslq	108(%r12), %rbx
	testq	%rbx, %rbx
	jle	.LBB7_9
# BB#7:                                 # %.lr.ph1278
	leaq	1(%rbx), %rbp
	shlq	$4, %rbx
	addq	$-8, %rbx
	.p2align	4, 0x90
.LBB7_8:                                # =>This Inner Loop Header: Depth=1
	movq	120(%r12), %rax
	movss	-8(%rax,%rbx), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movss	-4(%rax,%rbx), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	(%r14), %xmm14          # xmm14 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	xorps	%xmm4, %xmm4
	mulss	%xmm4, %xmm1
	movaps	%xmm14, %xmm0
	addss	%xmm1, %xmm0
	movss	8(%r14), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm2, %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movaps	%xmm14, %xmm15
	mulss	%xmm4, %xmm15
	movaps	%xmm15, %xmm0
	addss	%xmm12, %xmm0
	addss	%xmm2, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	addss	%xmm1, %xmm15
	addss	%xmm10, %xmm15
	movss	16(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movss	20(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm9
	xorps	%xmm1, %xmm1
	mulss	%xmm1, %xmm9
	movaps	%xmm9, %xmm13
	addss	%xmm7, %xmm13
	unpcklps	%xmm7, %xmm12   # xmm12 = xmm12[0],xmm7[0],xmm12[1],xmm7[1]
	mulss	%xmm1, %xmm7
	unpcklps	%xmm11, %xmm14  # xmm14 = xmm14[0],xmm11[0],xmm14[1],xmm11[1]
	addss	%xmm7, %xmm11
	movss	24(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm9
	addss	%xmm0, %xmm9
	unpcklps	%xmm0, %xmm10   # xmm10 = xmm10[0],xmm0[0],xmm10[1],xmm0[1]
	mulss	%xmm1, %xmm0
	addss	%xmm0, %xmm11
	addss	%xmm0, %xmm13
	movss	32(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm1, %xmm2
	xorps	%xmm8, %xmm8
	movaps	%xmm2, %xmm4
	addss	%xmm7, %xmm4
	movaps	%xmm5, %xmm6
	mulss	%xmm7, %xmm5
	mulss	%xmm8, %xmm7
	movaps	%xmm3, %xmm8
	mulss	%xmm0, %xmm3
	movaps	%xmm3, 64(%rsp)         # 16-byte Spill
	movaps	%xmm0, %xmm1
	addss	%xmm7, %xmm1
	addss	%xmm7, %xmm2
	movss	40(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	movss	(%rax,%rbx), %xmm0      # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm7, %xmm0
	mulss	.LCPI7_7, %xmm7
	addss	%xmm7, %xmm1
	addss	%xmm7, %xmm4
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm8, %xmm14
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm6, %xmm12
	addps	%xmm14, %xmm12
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm10
	addps	%xmm12, %xmm10
	movsd	48(%r14), %xmm3         # xmm3 = mem[0],zero
	addps	%xmm10, %xmm3
	addss	64(%rsp), %xmm5         # 16-byte Folded Reload
	movq	152(%r12), %rax
	addss	%xmm5, %xmm0
	addss	56(%r14), %xmm0
	xorps	%xmm5, %xmm5
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	movss	-8(%rax,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movss	%xmm6, 96(%rsp)
	movaps	48(%rsp), %xmm6         # 16-byte Reload
	movss	%xmm6, 100(%rsp)
	movss	%xmm15, 104(%rsp)
	movl	$0, 108(%rsp)
	movss	%xmm11, 112(%rsp)
	movss	%xmm13, 116(%rsp)
	movss	%xmm9, 120(%rsp)
	movl	$0, 124(%rsp)
	movss	%xmm1, 128(%rsp)
	movss	%xmm4, 132(%rsp)
	movss	%xmm2, 136(%rsp)
	movl	$0, 140(%rsp)
	movlps	%xmm3, 144(%rsp)
	movlps	%xmm5, 152(%rsp)
	movq	%r15, %rdi
	leaq	96(%rsp), %rsi
	movq	%r13, %rdx
	callq	_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3
	decq	%rbp
	addq	$-16, %rbx
	cmpq	$1, %rbp
	jg	.LBB7_8
	jmp	.LBB7_9
.LBB7_2:
	movslq	28(%r12), %rbp
	testq	%rbp, %rbp
	jle	.LBB7_9
# BB#3:                                 # %.lr.ph1275
	imulq	$88, %rbp, %rbx
	incq	%rbp
	addq	$-24, %rbx
	.p2align	4, 0x90
.LBB7_4:                                # =>This Inner Loop Header: Depth=1
	movq	40(%r12), %rax
	movss	-64(%rax,%rbx), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	-60(%rax,%rbx), %xmm15  # xmm15 = mem[0],zero,zero,zero
	movss	-56(%rax,%rbx), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	-48(%rax,%rbx), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	-44(%rax,%rbx), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-40(%rax,%rbx), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	-32(%rax,%rbx), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movss	%xmm13, 32(%rsp)        # 4-byte Spill
	movss	-28(%rax,%rbx), %xmm14  # xmm14 = mem[0],zero,zero,zero
	movss	-24(%rax,%rbx), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movss	(%r14), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm0, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movss	8(%r14), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 48(%rsp)         # 4-byte Spill
	movaps	%xmm15, %xmm2
	movss	%xmm15, 352(%rsp)       # 4-byte Spill
	mulss	%xmm7, %xmm2
	movaps	%xmm1, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm14, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 64(%rsp)         # 4-byte Spill
	movaps	%xmm10, %xmm2
	movaps	%xmm10, %xmm11
	mulss	%xmm7, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm9, %xmm2
	movaps	%xmm9, %xmm10
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 80(%rsp)         # 4-byte Spill
	movss	16(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm9, %xmm2
	movss	20(%r14), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movss	24(%r14), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 224(%rsp)        # 4-byte Spill
	mulss	%xmm9, %xmm15
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm15, %xmm3
	movaps	%xmm14, %xmm2
	mulss	%xmm13, %xmm2
	movaps	%xmm13, %xmm15
	movaps	%xmm15, 368(%rsp)       # 16-byte Spill
	addss	%xmm3, %xmm2
	movss	%xmm2, 384(%rsp)        # 4-byte Spill
	movaps	%xmm11, %xmm2
	mulss	%xmm9, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm10, %xmm13
	mulss	%xmm15, %xmm13
	addss	%xmm3, %xmm13
	movss	32(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm8
	movss	36(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm8, %xmm0
	movss	40(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm15
	addss	%xmm0, %xmm15
	movss	%xmm15, 32(%rsp)        # 4-byte Spill
	movss	352(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm8, %xmm14
	addss	%xmm1, %xmm14
	mulss	%xmm3, %xmm11
	mulss	%xmm2, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm8, %xmm10
	addss	%xmm4, %xmm10
	unpcklps	%xmm9, %xmm7    # xmm7 = xmm7[0],xmm9[0],xmm7[1],xmm9[1]
	movss	-16(%rax,%rbx), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm7
	unpcklps	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1]
	movss	-12(%rax,%rbx), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm4, %xmm5
	addps	%xmm7, %xmm5
	unpcklps	368(%rsp), %xmm6 # 16-byte Folded Reload
                                        # xmm6 = xmm6[0],mem[0],xmm6[1],mem[1]
	movss	-8(%rax,%rbx), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm5, %xmm6
	movsd	48(%r14), %xmm5         # xmm5 = mem[0],zero
	addps	%xmm6, %xmm5
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm8, %xmm4
	addss	%xmm1, %xmm4
	addss	56(%r14), %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movq	(%rax,%rbx), %rdx
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 96(%rsp)
	movss	64(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 100(%rsp)
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 104(%rsp)
	movl	$0, 108(%rsp)
	movss	224(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 112(%rsp)
	movss	384(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 116(%rsp)
	movss	%xmm13, 120(%rsp)
	movl	$0, 124(%rsp)
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 128(%rsp)
	movss	%xmm14, 132(%rsp)
	movss	%xmm10, 136(%rsp)
	movl	$0, 140(%rsp)
	movlps	%xmm5, 144(%rsp)
	movlps	%xmm0, 152(%rsp)
	movq	%r15, %rdi
	leaq	96(%rsp), %rsi
	movq	%r13, %rcx
	callq	_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3
	decq	%rbp
	addq	$-88, %rbx
	cmpq	$1, %rbp
	jg	.LBB7_4
	jmp	.LBB7_9
.LBB7_10:
	movslq	64(%r12), %rbp
	leal	2(%rbp), %eax
	cltq
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%rbp,%rax), %eax
	cltq
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movss	40(%r12,%rax,4), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	movss	40(%r12,%rbp,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	movaps	%xmm1, (%rsp)
	movss	%xmm0, (%rsp,%rbp,4)
	xorps	.LCPI7_3(%rip), %xmm0
	movaps	%xmm1, 240(%rsp)
	movss	%xmm0, 240(%rsp,%rbp,4)
	movups	(%r14), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	16(%r14), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	32(%r14), %xmm0
	movaps	%xmm0, 128(%rsp)
	movss	240(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	244(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	248(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	20(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	24(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movsd	48(%r14), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	mulss	32(%r14), %xmm2
	mulss	36(%r14), %xmm1
	addss	%xmm2, %xmm1
	mulss	40(%r14), %xmm0
	addss	%xmm1, %xmm0
	addss	56(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 144(%rsp)
	movlps	%xmm1, 152(%rsp)
	leaq	96(%rsp), %rsi
	movq	%r15, %rdi
	movaps	%xmm6, %xmm0
	movq	%r13, %rdx
	callq	_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3
	movups	(%r14), %xmm0
	movaps	%xmm0, 96(%rsp)
	movups	16(%r14), %xmm0
	movaps	%xmm0, 112(%rsp)
	movups	32(%r14), %xmm0
	movaps	%xmm0, 128(%rsp)
	movss	(%rsp), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	20(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	24(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	movsd	48(%r14), %xmm4         # xmm4 = mem[0],zero
	addps	%xmm3, %xmm4
	mulss	32(%r14), %xmm2
	mulss	36(%r14), %xmm1
	addss	%xmm2, %xmm1
	mulss	40(%r14), %xmm0
	addss	%xmm1, %xmm0
	addss	56(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm4, 144(%rsp)
	movlps	%xmm1, 152(%rsp)
	leaq	96(%rsp), %rsi
	movq	%r15, %rdi
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movq	%r13, %rdx
	callq	_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3
	movsd	48(%r14), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movss	56(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	leal	1(%rbp), %eax
	cltq
	imulq	$1431655766, %rax, %rax # imm = 0x55555556
	movq	%rax, %rcx
	shrq	$63, %rcx
	shrq	$32, %rax
	addl	%ecx, %eax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%rbp,%rax), %eax
	movslq	%eax, %rbp
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 240(%rsp,%rbp,4)
	movss	%xmm0, (%rsp,%rbp,4)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbx
	movss	240(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	244(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	248(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm8          # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	20(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm8    # xmm8 = xmm8[0],xmm5[0],xmm8[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	movss	24(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm5, %xmm2
	movss	32(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	movss	36(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm7, %xmm4
	movss	40(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	addss	%xmm4, %xmm0
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	addps	%xmm7, %xmm2
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm0
	xorps	%xmm4, %xmm4
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm2, 96(%rsp)
	movlps	%xmm4, 104(%rsp)
	movss	(%rsp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm9, %xmm5
	mulss	%xmm10, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm11, %xmm4
	addss	%xmm2, %xmm4
	addps	%xmm7, %xmm0
	addss	%xmm6, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	leaq	96(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbx
	movaps	.LCPI7_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	32(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movss	%xmm0, 240(%rsp,%rbp,4)
	movss	%xmm0, (%rsp,%rbp,4)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbx
	movss	240(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	244(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	248(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm8          # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	20(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm8    # xmm8 = xmm8[0],xmm5[0],xmm8[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	movss	24(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm5, %xmm2
	movss	32(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	movss	36(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm7, %xmm4
	movss	40(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	addss	%xmm4, %xmm0
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	addps	%xmm7, %xmm2
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm0
	xorps	%xmm4, %xmm4
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm2, 96(%rsp)
	movlps	%xmm4, 104(%rsp)
	movss	(%rsp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm9, %xmm5
	mulss	%xmm10, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm11, %xmm4
	addss	%xmm2, %xmm4
	addps	%xmm7, %xmm0
	addss	%xmm6, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	leaq	96(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbx
	movl	$0, 240(%rsp,%rbp,4)
	movl	$0, (%rsp,%rbp,4)
	movq	80(%rsp), %rax          # 8-byte Reload
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movss	%xmm0, 240(%rsp,%rax,4)
	movss	%xmm0, (%rsp,%rax,4)
	movq	%rax, %rbx
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movss	240(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	244(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	248(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm8          # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	20(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm8    # xmm8 = xmm8[0],xmm5[0],xmm8[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	movss	24(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm5, %xmm2
	movss	32(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	movss	36(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm7, %xmm4
	movss	40(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	addss	%xmm4, %xmm0
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	addps	%xmm7, %xmm2
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm0
	xorps	%xmm4, %xmm4
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm2, 96(%rsp)
	movlps	%xmm4, 104(%rsp)
	movss	(%rsp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm9, %xmm5
	mulss	%xmm10, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm11, %xmm4
	addss	%xmm2, %xmm4
	addps	%xmm7, %xmm0
	addss	%xmm6, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	leaq	96(%rsp), %rsi
	leaq	16(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbp
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, 240(%rsp,%rbx,4)
	movss	%xmm0, (%rsp,%rbx,4)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movss	240(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	244(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	248(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm8          # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	movaps	%xmm7, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movss	20(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm8    # xmm8 = xmm8[0],xmm5[0],xmm8[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	movss	24(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm3, %xmm2
	addps	%xmm5, %xmm2
	movss	32(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm7
	movss	36(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm7, %xmm4
	movss	40(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	addss	%xmm4, %xmm0
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	addps	%xmm7, %xmm2
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm0
	xorps	%xmm4, %xmm4
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm2, 96(%rsp)
	movlps	%xmm4, 104(%rsp)
	movss	(%rsp), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm3, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm9, %xmm5
	mulss	%xmm10, %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm11, %xmm4
	addss	%xmm2, %xmm4
	addps	%xmm7, %xmm0
	addss	%xmm6, %xmm4
	xorps	%xmm1, %xmm1
	movss	%xmm4, %xmm1            # xmm1 = xmm4[0],xmm1[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	leaq	96(%rsp), %rsi
	jmp	.LBB7_13
.LBB7_11:
	movl	68(%r12), %eax
	movss	72(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	48(%r14), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movss	56(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	movslq	80(%r12), %rcx
	mulss	.LCPI7_6(%rip), %xmm0
	xorps	%xmm1, %xmm1
	movaps	%xmm1, 96(%rsp)
	xorps	%xmm1, %xmm1
	movss	%xmm0, 96(%rsp,%rcx,4)
	leal	1(%rcx), %edx
	movslq	%edx, %rdx
	imulq	$1431655766, %rdx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	leal	(%rdx,%rdx,2), %edx
	negl	%edx
	leal	1(%rcx,%rdx), %edx
	movslq	%edx, %rdx
	movaps	%xmm1, 240(%rsp)
	movl	%eax, 240(%rsp,%rdx,4)
	leal	2(%rcx), %edx
	movslq	%edx, %rdx
	imulq	$1431655766, %rdx, %rdx # imm = 0x55555556
	movq	%rdx, %rsi
	shrq	$63, %rsi
	shrq	$32, %rdx
	addl	%esi, %edx
	leal	(%rdx,%rdx,2), %edx
	negl	%edx
	leal	2(%rcx,%rdx), %ecx
	movslq	%ecx, %rcx
	movaps	%xmm1, (%rsp)
	movl	%eax, (%rsp,%rcx,4)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movss	96(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	%xmm2, 432(%rsp)        # 16-byte Spill
	movss	240(%rsp), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	%xmm10, 64(%rsp)        # 4-byte Spill
	subss	%xmm3, %xmm10
	movaps	%xmm3, %xmm9
	movaps	%xmm9, 352(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	mulps	%xmm2, %xmm1
	movss	20(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	movaps	%xmm5, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	%xmm2, 416(%rsp)        # 16-byte Spill
	movss	244(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 80(%rsp)         # 4-byte Spill
	subss	%xmm5, %xmm4
	movaps	%xmm5, %xmm11
	movaps	%xmm11, 368(%rsp)       # 16-byte Spill
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm0, %xmm5
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1]
	movaps	%xmm7, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	%xmm2, 400(%rsp)        # 16-byte Spill
	movss	248(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 224(%rsp)        # 4-byte Spill
	subss	%xmm7, %xmm1
	movaps	%xmm7, %xmm12
	movaps	%xmm12, 384(%rsp)       # 16-byte Spill
	movaps	%xmm1, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm6, %xmm7
	mulps	%xmm2, %xmm6
	addps	%xmm0, %xmm6
	movss	32(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm8, %xmm0
	movss	36(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	movss	40(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm0
	mulss	%xmm11, %xmm0
	addss	%xmm2, %xmm0
	movss	48(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	addss	%xmm12, %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movaps	32(%rsp), %xmm13        # 16-byte Reload
	addps	%xmm13, %xmm6
	movlps	%xmm6, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	addps	%xmm3, %xmm5
	addps	%xmm5, %xmm7
	mulss	%xmm8, %xmm10
	mulss	%xmm9, %xmm4
	addss	%xmm10, %xmm4
	mulss	%xmm11, %xmm1
	addss	%xmm4, %xmm1
	addss	%xmm12, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	addps	%xmm13, %xmm7
	movlps	%xmm7, 208(%rsp)
	movlps	%xmm0, 216(%rsp)
	leaq	16(%rsp), %rsi
	leaq	208(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbp
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movss	16(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movaps	432(%rsp), %xmm3        # 16-byte Reload
	mulps	%xmm1, %xmm3
	movss	20(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm0    # xmm0 = xmm0[0],xmm2[0],xmm0[1],xmm2[1]
	movaps	416(%rsp), %xmm2        # 16-byte Reload
	mulps	%xmm0, %xmm2
	addps	%xmm3, %xmm2
	movaps	%xmm2, %xmm4
	movss	24(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	400(%rsp), %xmm3        # 16-byte Reload
	mulps	%xmm2, %xmm3
	addps	%xmm4, %xmm3
	movaps	%xmm3, %xmm5
	movss	32(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	352(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm8, %xmm4
	movss	36(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	368(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm9, %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm3, %xmm4
	movss	40(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	384(%rsp), %xmm3        # 16-byte Reload
	mulss	%xmm10, %xmm3
	addss	%xmm4, %xmm3
	movaps	32(%rsp), %xmm11        # 16-byte Reload
	addps	%xmm11, %xmm5
	movss	48(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	addss	%xmm12, %xmm3
	movaps	%xmm3, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm3, 24(%rsp)
	movss	96(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, 352(%rsp)        # 16-byte Spill
	movss	100(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 368(%rsp)        # 16-byte Spill
	movaps	.LCPI7_3(%rip), %xmm7   # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm7, %xmm4
	movaps	%xmm3, %xmm6
	xorps	%xmm7, %xmm6
	subss	64(%rsp), %xmm4         # 4-byte Folded Reload
	movss	104(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 384(%rsp)        # 16-byte Spill
	subss	80(%rsp), %xmm6         # 4-byte Folded Reload
	xorps	%xmm7, %xmm3
	subss	224(%rsp), %xmm3        # 4-byte Folded Reload
	movaps	%xmm4, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm1, %xmm7
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	addps	%xmm7, %xmm1
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm8, %xmm4
	mulss	%xmm9, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm10, %xmm3
	addss	%xmm6, %xmm3
	addss	%xmm12, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	addps	%xmm11, %xmm0
	movlps	%xmm0, 208(%rsp)
	movlps	%xmm1, 216(%rsp)
	leaq	16(%rsp), %rsi
	leaq	208(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbp
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movss	16(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movaps	352(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	movss	20(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm12   # xmm12 = xmm12[0],xmm3[0],xmm12[1],xmm3[1]
	movaps	368(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm12, %xmm3
	addps	%xmm2, %xmm3
	movss	24(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	movaps	384(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm2, %xmm6
	addps	%xmm3, %xmm6
	movss	32(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm5
	movaps	%xmm7, %xmm13
	mulss	%xmm8, %xmm5
	movss	36(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm5, %xmm7
	movss	40(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	movaps	%xmm4, %xmm14
	mulss	%xmm10, %xmm3
	addss	%xmm7, %xmm3
	movaps	32(%rsp), %xmm11        # 16-byte Reload
	addps	%xmm11, %xmm6
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	xorps	%xmm7, %xmm7
	movss	%xmm3, %xmm7            # xmm7 = xmm3[0],xmm7[1,2,3]
	movlps	%xmm6, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	(%rsp), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	%xmm4, 80(%rsp)         # 4-byte Spill
	movss	4(%rsp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	%xmm7, 64(%rsp)         # 4-byte Spill
	subss	%xmm13, %xmm4
	subss	%xmm0, %xmm7
	movss	8(%rsp), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	%xmm6, 224(%rsp)        # 4-byte Spill
	subss	%xmm14, %xmm6
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movaps	%xmm7, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm12, %xmm1
	addps	%xmm3, %xmm1
	movaps	%xmm6, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm8, %xmm4
	mulss	%xmm9, %xmm7
	addss	%xmm4, %xmm7
	mulss	%xmm10, %xmm6
	addss	%xmm7, %xmm6
	addps	%xmm11, %xmm0
	addss	%xmm5, %xmm6
	xorps	%xmm1, %xmm1
	movss	%xmm6, %xmm1            # xmm1 = xmm6[0],xmm1[1,2,3]
	movlps	%xmm0, 208(%rsp)
	movlps	%xmm1, 216(%rsp)
	leaq	16(%rsp), %rsi
	leaq	208(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbp
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movss	96(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	104(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	16(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%r14), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm10   # xmm10 = xmm10[0],xmm5[0],xmm10[1],xmm5[1]
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm10, %xmm5
	movss	20(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm9    # xmm9 = xmm9[0],xmm6[0],xmm9[1],xmm6[1]
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm9, %xmm6
	addps	%xmm5, %xmm6
	movss	24(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	addps	%xmm6, %xmm0
	movss	32(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm8, %xmm6
	movss	36(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm11, %xmm1
	addss	%xmm6, %xmm1
	movss	40(%r14), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm1, %xmm3
	movss	48(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	addps	%xmm3, %xmm0
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	movaps	.LCPI7_3(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm2
	xorps	%xmm0, %xmm4
	xorps	%xmm0, %xmm7
	subss	80(%rsp), %xmm2         # 4-byte Folded Reload
	subss	64(%rsp), %xmm4         # 4-byte Folded Reload
	subss	224(%rsp), %xmm7        # 4-byte Folded Reload
	movaps	%xmm2, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm10, %xmm0
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm9, %xmm1
	addps	%xmm0, %xmm1
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm8, %xmm2
	mulss	%xmm11, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm12, %xmm7
	addss	%xmm4, %xmm7
	addps	%xmm3, %xmm0
	addss	%xmm6, %xmm7
	xorps	%xmm1, %xmm1
	movss	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1,2,3]
	movlps	%xmm0, 208(%rsp)
	movlps	%xmm1, 216(%rsp)
	leaq	16(%rsp), %rsi
	leaq	208(%rsp), %rdx
	jmp	.LBB7_14
.LBB7_12:
	movslq	64(%r12), %rbx
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*144(%rax)
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movsd	40(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movsd	48(%r12), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*88(%rax)
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*88(%rax)
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	unpcklps	224(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	addps	64(%rsp), %xmm1         # 16-byte Folded Reload
	movaps	%xmm1, %xmm2
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm0, %xmm1
	movlps	%xmm2, 464(%rsp)
	movlps	%xmm1, 472(%rsp)
	movl	464(%rsp,%rbx,4), %eax
	movsd	48(%r14), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	56(%r14), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 64(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 96(%rsp)
	xorps	%xmm0, %xmm0
	movl	%eax, 96(%rsp,%rbx,4)
	leal	1(%rbx), %eax
	cltq
	imulq	$1431655766, %rax, %rcx # imm = 0x55555556
	movq	%rcx, %rdx
	shrq	$63, %rdx
	shrq	$32, %rcx
	addl	%edx, %ecx
	leal	(%rcx,%rcx,2), %ecx
	subl	%ecx, %eax
	cltq
	movaps	%xmm0, 240(%rsp)
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 240(%rsp,%rax,4)
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movss	96(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	240(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	244(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	movaps	%xmm1, %xmm13
	movaps	%xmm13, 352(%rsp)       # 16-byte Spill
	addss	%xmm3, %xmm6
	movaps	%xmm3, %xmm15
	movss	%xmm15, 384(%rsp)       # 4-byte Spill
	movaps	%xmm0, %xmm4
	movaps	%xmm0, %xmm12
	movaps	%xmm12, 368(%rsp)       # 16-byte Spill
	addss	%xmm2, %xmm4
	movaps	%xmm2, %xmm14
	movss	%xmm14, 224(%rsp)       # 4-byte Spill
	movss	104(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movss	248(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	addss	%xmm1, %xmm0
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	16(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm8          # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	mulps	%xmm1, %xmm3
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	20(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm8    # xmm8 = xmm8[0],xmm7[0],xmm8[1],xmm7[1]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	24(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	mulps	%xmm3, %xmm2
	addps	%xmm5, %xmm2
	movss	32(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm6
	movss	36(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm6, %xmm4
	movss	40(%r14), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	addss	%xmm4, %xmm0
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	addps	%xmm6, %xmm2
	movss	64(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm0
	xorps	%xmm4, %xmm4
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm2, (%rsp)
	movlps	%xmm4, 8(%rsp)
	movaps	%xmm15, %xmm0
	subss	%xmm13, %xmm0
	movaps	%xmm14, %xmm2
	subss	%xmm12, %xmm2
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm8, %xmm1
	movss	48(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	subss	32(%rsp), %xmm5         # 16-byte Folded Reload
	addps	%xmm4, %xmm1
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm1, %xmm4
	mulss	%xmm9, %xmm0
	mulss	%xmm10, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm11, %xmm5
	addss	%xmm2, %xmm5
	addss	%xmm7, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	addps	%xmm6, %xmm4
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm0, 24(%rsp)
	movq	%rsp, %rsi
	leaq	16(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbp
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movaps	352(%rsp), %xmm0        # 16-byte Reload
	subss	384(%rsp), %xmm0        # 4-byte Folded Reload
	movaps	368(%rsp), %xmm4        # 16-byte Reload
	subss	224(%rsp), %xmm4        # 4-byte Folded Reload
	movaps	32(%rsp), %xmm7         # 16-byte Reload
	subss	48(%rsp), %xmm7         # 4-byte Folded Reload
	movaps	%xmm0, %xmm2
	movaps	%xmm0, %xmm5
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	mulps	%xmm1, %xmm2
	movaps	%xmm4, %xmm3
	movaps	%xmm4, %xmm10
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	20(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm7, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	24(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	mulps	%xmm2, %xmm6
	addps	%xmm3, %xmm6
	movss	32(%r14), %xmm8         # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm5
	movss	36(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm3
	mulss	%xmm9, %xmm3
	addss	%xmm5, %xmm3
	movss	40(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm7
	addss	%xmm3, %xmm7
	movaps	80(%rsp), %xmm11        # 16-byte Reload
	addps	%xmm11, %xmm6
	movss	64(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm7
	xorps	%xmm3, %xmm3
	movss	%xmm7, %xmm3            # xmm3 = xmm7[0],xmm3[1,2,3]
	movlps	%xmm6, (%rsp)
	movlps	%xmm3, 8(%rsp)
	movss	96(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	100(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	.LCPI7_3(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm4, %xmm3
	xorps	%xmm4, %xmm6
	movss	104(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm7
	subss	240(%rsp), %xmm3
	subss	244(%rsp), %xmm6
	subss	248(%rsp), %xmm7
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm1, %xmm4
	movaps	%xmm6, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	addps	%xmm4, %xmm1
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	mulss	%xmm8, %xmm3
	mulss	%xmm9, %xmm6
	addss	%xmm3, %xmm6
	mulss	%xmm10, %xmm7
	addss	%xmm6, %xmm7
	addps	%xmm11, %xmm0
	addss	%xmm5, %xmm7
	xorps	%xmm1, %xmm1
	movss	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm1, 24(%rsp)
	movq	%rsp, %rsi
.LBB7_13:                               # %.loopexit
	leaq	16(%rsp), %rdx
	jmp	.LBB7_14
.LBB7_19:
	mulss	%xmm0, %xmm0
	mulss	%xmm4, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	sqrtss	%xmm4, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB7_21
# BB#20:                                # %call.sqrt1330
	movaps	%xmm4, %xmm0
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	movaps	64(%rsp), %xmm9         # 16-byte Reload
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	32(%rsp), %xmm5         # 16-byte Reload
.LBB7_21:                               # %.split1329
	movss	.LCPI7_2(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	64(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	mulss	%xmm1, %xmm4
	mulss	60(%r12), %xmm1
	movss	68(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm7, %xmm0
	mulss	%xmm1, %xmm7
	xorps	.LCPI7_3(%rip), %xmm7
	mulss	.LCPI7_4(%rip), %xmm3
	xorps	%xmm2, %xmm2
.LBB7_22:                               # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit
	movss	.LCPI7_5(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm1
	movaps	%xmm9, %xmm6
	addss	%xmm3, %xmm6
	movaps	%xmm6, 480(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm6, 400(%rsp)        # 16-byte Spill
	movaps	%xmm5, %xmm6
	addss	%xmm2, %xmm6
	movaps	%xmm6, 416(%rsp)        # 16-byte Spill
	movaps	%xmm9, %xmm6
	subss	%xmm3, %xmm6
	movaps	%xmm6, 432(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm3
	subss	%xmm1, %xmm3
	movaps	%xmm3, 352(%rsp)        # 16-byte Spill
	movaps	%xmm5, %xmm1
	subss	%xmm2, %xmm1
	movaps	%xmm1, 368(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm7
	mulss	.LCPI7_4(%rip), %xmm0
	mulss	%xmm8, %xmm4
	movaps	%xmm9, %xmm1
	addss	%xmm7, %xmm1
	movaps	%xmm1, 384(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm1, 224(%rsp)        # 16-byte Spill
	movaps	%xmm5, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	subss	%xmm7, %xmm9
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	subss	%xmm0, %xmm10
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	subss	%xmm4, %xmm5
	movaps	%xmm5, 32(%rsp)         # 16-byte Spill
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movaps	480(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	mulps	%xmm1, %xmm2
	movaps	400(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	20(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movaps	416(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	24(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	mulps	%xmm2, %xmm4
	addps	%xmm3, %xmm4
	movsd	48(%r14), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm4
	movss	32(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	mulss	%xmm9, %xmm5
	movss	36(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm3, %xmm5
	movss	40(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm5, %xmm3
	movss	56(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm3
	movaps	%xmm3, %xmm5
	xorps	%xmm3, %xmm3
	movss	%xmm5, %xmm3            # xmm3 = xmm5[0],xmm3[1,2,3]
	movlps	%xmm4, 96(%rsp)
	movlps	%xmm3, 104(%rsp)
	movaps	432(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movaps	352(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	addps	%xmm3, %xmm1
	movaps	368(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	addps	%xmm8, %xmm0
	movaps	%xmm4, %xmm2
	mulss	%xmm9, %xmm2
	movaps	%xmm5, %xmm1
	mulss	%xmm10, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	addss	%xmm7, %xmm1
	movaps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm0, 240(%rsp)
	movlps	%xmm1, 248(%rsp)
	leaq	96(%rsp), %rsi
	leaq	240(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbp
	movq	(%r15), %rax
	movq	%r15, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbp
	movaps	384(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	16(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%r14), %xmm0          # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	mulps	%xmm1, %xmm2
	movaps	224(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	20(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movaps	80(%rsp), %xmm7         # 16-byte Reload
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	24(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%r14), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	mulps	%xmm2, %xmm4
	addps	%xmm3, %xmm4
	movsd	48(%r14), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm4
	movss	32(%r14), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	mulss	%xmm9, %xmm5
	movss	36(%r14), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm3, %xmm5
	movss	40(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm5, %xmm3
	movss	56(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm3
	movaps	%xmm3, %xmm5
	xorps	%xmm3, %xmm3
	movss	%xmm5, %xmm3            # xmm3 = xmm5[0],xmm3[1,2,3]
	movlps	%xmm4, 96(%rsp)
	movlps	%xmm3, 104(%rsp)
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	%xmm5, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	addps	%xmm3, %xmm1
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm1, %xmm0
	addps	%xmm8, %xmm0
	mulss	%xmm9, %xmm4
	movaps	%xmm5, %xmm1
	mulss	%xmm10, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	addss	%xmm7, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 240(%rsp)
	movlps	%xmm2, 248(%rsp)
	leaq	96(%rsp), %rsi
	leaq	240(%rsp), %rdx
.LBB7_14:                               # %.loopexit
	movq	%rax, %rdi
	movq	%r13, %rcx
	callq	*%rbp
.LBB7_9:                                # %.loopexit
	addq	$504, %rsp              # imm = 0x1F8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_42:
.Ltmp149:
	movq	%rax, %rbx
.Ltmp150:
	leaq	240(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp151:
	jmp	.LBB7_36
.LBB7_43:
.Ltmp152:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_35:
.Ltmp131:
	movq	%rax, %rbx
.Ltmp132:
	leaq	96(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp133:
	jmp	.LBB7_36
.LBB7_37:
.Ltmp134:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_44:
.Ltmp137:
	movq	%rax, %rbx
.Ltmp138:
	movq	%rbp, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp139:
# BB#45:
.Ltmp144:
	leaq	240(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp145:
	jmp	.LBB7_36
.LBB7_46:
.Ltmp140:
	movq	%rax, %rbx
.Ltmp141:
	leaq	240(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp142:
	jmp	.LBB7_49
.LBB7_47:
.Ltmp143:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_38:
.Ltmp120:
	movq	%rax, %rbx
	leaq	104(%rsp), %rdi
.Ltmp121:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp122:
# BB#39:
.Ltmp127:
	leaq	96(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp128:
.LBB7_36:                               # %unwind_resume
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_40:
.Ltmp123:
	movq	%rax, %rbx
.Ltmp124:
	leaq	96(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp125:
	jmp	.LBB7_49
.LBB7_41:
.Ltmp126:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB7_48:
.Ltmp146:
	movq	%rax, %rbx
.LBB7_49:                               # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3, .Lfunc_end7-_ZN23btDiscreteDynamicsWorld15debugDrawObjectERK11btTransformPK16btCollisionShapeRK9btVector3
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI7_0:
	.quad	.LBB7_5
	.quad	.LBB7_6
	.quad	.LBB7_10
	.quad	.LBB7_11
	.quad	.LBB7_23
	.quad	.LBB7_12
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_15
	.quad	.LBB7_23
	.quad	.LBB7_23
	.quad	.LBB7_2
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\331\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\320\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp118-.Lfunc_begin4  #   Call between .Lfunc_begin4 and .Ltmp118
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp118-.Lfunc_begin4  # >> Call Site 2 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin4  #     jumps to .Ltmp120
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp130-.Ltmp129       #   Call between .Ltmp129 and .Ltmp130
	.long	.Ltmp131-.Lfunc_begin4  #     jumps to .Ltmp131
	.byte	0                       #   On action: cleanup
	.long	.Ltmp130-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp135-.Ltmp130       #   Call between .Ltmp130 and .Ltmp135
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp135-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp136-.Ltmp135       #   Call between .Ltmp135 and .Ltmp136
	.long	.Ltmp137-.Lfunc_begin4  #     jumps to .Ltmp137
	.byte	0                       #   On action: cleanup
	.long	.Ltmp147-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp148-.Ltmp147       #   Call between .Ltmp147 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin4  #     jumps to .Ltmp149
	.byte	0                       #   On action: cleanup
	.long	.Ltmp148-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp150-.Ltmp148       #   Call between .Ltmp148 and .Ltmp150
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin4  #     jumps to .Ltmp152
	.byte	1                       #   On action: 1
	.long	.Ltmp132-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp133-.Ltmp132       #   Call between .Ltmp132 and .Ltmp133
	.long	.Ltmp134-.Lfunc_begin4  #     jumps to .Ltmp134
	.byte	1                       #   On action: 1
	.long	.Ltmp138-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp139-.Ltmp138       #   Call between .Ltmp138 and .Ltmp139
	.long	.Ltmp140-.Lfunc_begin4  #     jumps to .Ltmp140
	.byte	1                       #   On action: 1
	.long	.Ltmp144-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin4  #     jumps to .Ltmp146
	.byte	1                       #   On action: 1
	.long	.Ltmp141-.Lfunc_begin4  # >> Call Site 12 <<
	.long	.Ltmp142-.Ltmp141       #   Call between .Ltmp141 and .Ltmp142
	.long	.Ltmp143-.Lfunc_begin4  #     jumps to .Ltmp143
	.byte	1                       #   On action: 1
	.long	.Ltmp121-.Lfunc_begin4  # >> Call Site 13 <<
	.long	.Ltmp122-.Ltmp121       #   Call between .Ltmp121 and .Ltmp122
	.long	.Ltmp123-.Lfunc_begin4  #     jumps to .Ltmp123
	.byte	1                       #   On action: 1
	.long	.Ltmp127-.Lfunc_begin4  # >> Call Site 14 <<
	.long	.Ltmp128-.Ltmp127       #   Call between .Ltmp127 and .Ltmp128
	.long	.Ltmp146-.Lfunc_begin4  #     jumps to .Ltmp146
	.byte	1                       #   On action: 1
	.long	.Ltmp128-.Lfunc_begin4  # >> Call Site 15 <<
	.long	.Ltmp124-.Ltmp128       #   Call between .Ltmp128 and .Ltmp124
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp124-.Lfunc_begin4  # >> Call Site 16 <<
	.long	.Ltmp125-.Ltmp124       #   Call between .Ltmp124 and .Ltmp125
	.long	.Ltmp126-.Lfunc_begin4  #     jumps to .Ltmp126
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_0:
	.long	1056964608              # float 0.5
.LCPI8_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_,"axG",@progbits,_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_,comdat
	.weak	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
	.p2align	4, 0x90
	.type	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_,@function
_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_: # @_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 160
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %r13
	movss	(%rdx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	subss	%xmm9, %xmm6
	movaps	%xmm5, %xmm7
	subss	%xmm8, %xmm7
	movss	8(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	subss	%xmm2, %xmm0
	movss	.LCPI8_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	movss	%xmm6, 88(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm7
	movss	%xmm7, 84(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm0
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	addss	%xmm9, %xmm4
	addss	%xmm8, %xmm5
	addss	%xmm2, %xmm1
	mulss	%xmm3, %xmm4
	movss	%xmm4, 92(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm5
	movss	%xmm5, 52(%rsp)         # 4-byte Spill
	mulss	%xmm3, %xmm1
	movss	%xmm1, 48(%rsp)         # 4-byte Spill
	movabsq	$4575657222473777152, %rax # imm = 0x3F8000003F800000
	movq	%rax, 64(%rsp)
	movq	$1065353216, 72(%rsp)   # imm = 0x3F800000
	movss	.LCPI8_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	xorl	%ebx, %ebx
	leaq	32(%rsp), %r15
	leaq	16(%rsp), %r12
	movl	$3212836864, %ebp       # imm = 0xBF800000
	movaps	%xmm1, %xmm2
	movaps	%xmm1, %xmm0
	jmp	.LBB8_1
	.p2align	4, 0x90
.LBB8_4:                                # %..preheader_crit_edge
                                        #   in Loop: Header=BB8_1 Depth=1
	incq	%rbx
	movss	64(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	68(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	72(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
.LBB8_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	mulss	88(%rsp), %xmm0         # 4-byte Folded Reload
	mulss	84(%rsp), %xmm2         # 4-byte Folded Reload
	mulss	80(%rsp), %xmm1         # 4-byte Folded Reload
	movl	$0, 44(%rsp)
	movaps	%xmm2, %xmm4
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	92(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	movss	%xmm3, 100(%rsp)        # 4-byte Spill
	movaps	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 32(%rsp)
	movss	52(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 96(%rsp)         # 4-byte Spill
	movss	%xmm5, 36(%rsp)
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	movss	%xmm1, 56(%rsp)         # 4-byte Spill
	movss	%xmm1, 40(%rsp)
	movl	$0, 28(%rsp)
	subss	%xmm0, %xmm2
	movss	%xmm2, 60(%rsp)         # 4-byte Spill
	movss	%xmm2, 16(%rsp)
	movss	%xmm5, 20(%rsp)
	movss	%xmm1, 24(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	$0, 44(%rsp)
	movss	60(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)
	movss	96(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 36(%rsp)
	movss	56(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 40(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm0, 16(%rsp)
	movss	52(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movss	%xmm0, 20(%rsp)
	movss	%xmm1, 24(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movl	$0, 44(%rsp)
	movss	60(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 32(%rsp)
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 36(%rsp)
	movss	56(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 40(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm0, 16(%rsp)
	movss	%xmm1, 20(%rsp)
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	100(%rsp), %xmm0        # 4-byte Folded Reload
	movss	%xmm0, 24(%rsp)
	movq	(%r13), %rax
	movq	%r13, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movabsq	$-4647714812233515008, %rax # imm = 0xBF800000BF800000
	movq	%rax, 64(%rsp)
	movq	%rbp, 72(%rsp)
	cmpq	$2, %rbx
	jg	.LBB8_3
# BB#2:                                 #   in Loop: Header=BB8_1 Depth=1
	movss	64(%rsp,%rbx,4), %xmm0  # xmm0 = mem[0],zero,zero,zero
	xorps	.LCPI8_2(%rip), %xmm0
	movss	%xmm0, 64(%rsp,%rbx,4)
.LBB8_3:                                #   in Loop: Header=BB8_1 Depth=1
	cmpq	$3, %rbx
	jne	.LBB8_4
# BB#5:
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_, .Lfunc_end8-_ZN12btIDebugDraw8drawAabbERK9btVector3S2_S2_
	.cfi_endproc

	.text
	.globl	_ZN23btDiscreteDynamicsWorld11clearForcesEv
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld11clearForcesEv,@function
_ZN23btDiscreteDynamicsWorld11clearForcesEv: # @_ZN23btDiscreteDynamicsWorld11clearForcesEv
	.cfi_startproc
# BB#0:
	cmpl	$0, 284(%rdi)
	jle	.LBB9_3
# BB#1:                                 # %.lr.ph
	xorl	%eax, %eax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	296(%rdi), %rcx
	movq	(%rcx,%rax,8), %rcx
	movups	%xmm0, 460(%rcx)
	movups	%xmm0, 444(%rcx)
	incq	%rax
	movslq	284(%rdi), %rcx
	cmpq	%rcx, %rax
	jl	.LBB9_2
.LBB9_3:                                # %._crit_edge
	retq
.Lfunc_end9:
	.size	_ZN23btDiscreteDynamicsWorld11clearForcesEv, .Lfunc_end9-_ZN23btDiscreteDynamicsWorld11clearForcesEv
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld12applyGravityEv
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld12applyGravityEv,@function
_ZN23btDiscreteDynamicsWorld12applyGravityEv: # @_ZN23btDiscreteDynamicsWorld12applyGravityEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi74:
	.cfi_def_cfa_offset 32
.Lcfi75:
	.cfi_offset %rbx, -24
.Lcfi76:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	284(%r14), %eax
	testl	%eax, %eax
	jle	.LBB10_6
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r14), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movl	228(%rdi), %ecx
	cmpl	$2, %ecx
	je	.LBB10_5
# BB#3:                                 #   in Loop: Header=BB10_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB10_5
# BB#4:                                 #   in Loop: Header=BB10_2 Depth=1
	callq	_ZN11btRigidBody12applyGravityEv
	movl	284(%r14), %eax
.LBB10_5:                               #   in Loop: Header=BB10_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB10_2
.LBB10_6:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	_ZN23btDiscreteDynamicsWorld12applyGravityEv, .Lfunc_end10-_ZN23btDiscreteDynamicsWorld12applyGravityEv
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody,@function
_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody: # @_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi78:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi79:
	.cfi_def_cfa_offset 96
.Lcfi80:
	.cfi_offset %rbx, -24
.Lcfi81:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	cmpq	$0, 512(%rbx)
	je	.LBB11_3
# BB#1:
	testb	$3, 216(%rbx)
	jne	.LBB11_3
# BB#2:
	leaq	72(%rbx), %rax
	leaq	136(%rbx), %rsi
	leaq	152(%rbx), %rdx
	movss	328(%rdi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	260(%rbx), %xmm0
	leaq	8(%rsp), %r14
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	movq	512(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*24(%rax)
.LBB11_3:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end11:
	.size	_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody, .Lfunc_end11-_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1061752795              # float 0.785398185
.LCPI12_1:
	.long	981668463               # float 0.00100000005
.LCPI12_2:
	.long	1056964608              # float 0.5
.LCPI12_3:
	.long	3165301419              # float -0.020833334
.LCPI12_4:
	.long	1065353216              # float 1
.LCPI12_5:
	.long	1073741824              # float 2
	.section	.text._ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,"axG",@progbits,_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,comdat
	.weak	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,@function
_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_: # @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi85:
	.cfi_def_cfa_offset 96
.Lcfi86:
	.cfi_offset %rbx, -32
.Lcfi87:
	.cfi_offset %r14, -24
.Lcfi88:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movaps	%xmm0, %xmm3
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movsd	48(%r14), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	56(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 48(%r15)
	movlps	%xmm1, 56(%r15)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm2
	jnp	.LBB12_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm0, %xmm2
.LBB12_2:                               # %.split
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	ucomiss	.LCPI12_0(%rip), %xmm0
	jbe	.LBB12_4
# BB#3:                                 # %select.true.sink
	movss	.LCPI12_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm2
.LBB12_4:                               # %select.end
	movss	.LCPI12_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB12_6
# BB#5:
	movss	.LCPI12_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	mulss	.LCPI12_3(%rip), %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	jmp	.LBB12_7
.LBB12_6:
	movss	.LCPI12_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	callq	sinf
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	divss	%xmm2, %xmm0
.LBB12_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	8(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm2
	mulss	.LCPI12_2(%rip), %xmm2
	movaps	%xmm2, %xmm0
	callq	cosf
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	leaq	48(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	48(%rsp), %xmm12        # xmm12 = mem[0],zero
	movq	56(%rsp), %xmm9         # xmm9 = mem[0],zero
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	pshufd	$229, %xmm9, %xmm8      # xmm8 = xmm9[1,1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm4, %xmm7
	pshufd	$229, %xmm12, %xmm11    # xmm11 = xmm12[1,1,2,3]
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm4
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm7
	movaps	%xmm1, %xmm4
	movaps	%xmm1, %xmm13
	mulss	%xmm11, %xmm4
	movaps	%xmm2, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm3, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm0, %xmm4
	mulss	%xmm9, %xmm4
	subss	%xmm4, %xmm6
	movaps	%xmm13, %xmm4
	mulss	%xmm9, %xmm4
	movaps	%xmm3, %xmm5
	movaps	%xmm3, %xmm1
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm0, %xmm10
	mulss	%xmm11, %xmm10
	addss	%xmm5, %xmm10
	movaps	%xmm2, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm10
	movaps	%xmm13, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm3
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB12_9
# BB#8:                                 # %call.sqrt113
	movaps	%xmm1, %xmm0
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm10, 32(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
.LBB12_9:                               # %.split112
	movss	.LCPI12_4(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm7
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm10
	mulss	%xmm3, %xmm2
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	.LCPI12_5(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	movaps	%xmm7, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm10, %xmm4
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm2, %xmm11
	mulss	%xmm3, %xmm11
	mulss	%xmm4, %xmm2
	mulss	%xmm7, %xmm0
	movaps	%xmm7, %xmm5
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm3
	mulss	%xmm4, %xmm6
	mulss	%xmm10, %xmm4
	movaps	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm8, %xmm10
	subss	%xmm1, %xmm10
	movaps	%xmm5, %xmm12
	subss	%xmm2, %xmm12
	movaps	%xmm7, %xmm1
	addss	%xmm11, %xmm1
	addss	%xmm2, %xmm5
	addss	%xmm0, %xmm4
	movaps	%xmm8, %xmm2
	subss	%xmm4, %xmm2
	movaps	%xmm6, %xmm4
	subss	%xmm9, %xmm4
	subss	%xmm11, %xmm7
	addss	%xmm9, %xmm6
	addss	%xmm0, %xmm3
	subss	%xmm3, %xmm8
	movss	%xmm10, (%r15)
	movss	%xmm12, 4(%r15)
	movss	%xmm1, 8(%r15)
	movl	$0, 12(%r15)
	movss	%xmm5, 16(%r15)
	movss	%xmm2, 20(%r15)
	movss	%xmm4, 24(%r15)
	movl	$0, 28(%r15)
	movss	%xmm7, 32(%r15)
	movss	%xmm6, 36(%r15)
	movss	%xmm8, 40(%r15)
	movl	$0, 44(%r15)
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_, .Lfunc_end12-_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_endproc

	.text
	.globl	_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv,@function
_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv: # @_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi91:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi92:
	.cfi_def_cfa_offset 40
	subq	$136, %rsp
.Lcfi93:
	.cfi_def_cfa_offset 176
.Lcfi94:
	.cfi_offset %rbx, -40
.Lcfi95:
	.cfi_offset %r12, -32
.Lcfi96:
	.cfi_offset %r14, -24
.Lcfi97:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	movl	$.L.str.1, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpb	$0, 334(%r15)
	je	.LBB13_11
# BB#1:                                 # %.preheader37
	movl	12(%r15), %eax
	testl	%eax, %eax
	jle	.LBB13_21
# BB#2:                                 # %.lr.ph41
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB13_3:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rcx
	movq	(%rcx,%rbx,8), %r12
	testq	%r12, %r12
	je	.LBB13_10
# BB#4:                                 #   in Loop: Header=BB13_3 Depth=1
	cmpl	$2, 256(%r12)
	jne	.LBB13_10
# BB#5:                                 #   in Loop: Header=BB13_3 Depth=1
	cmpq	$0, 512(%r12)
	je	.LBB13_10
# BB#6:                                 #   in Loop: Header=BB13_3 Depth=1
	testb	$3, 216(%r12)
	jne	.LBB13_10
# BB#7:                                 #   in Loop: Header=BB13_3 Depth=1
	leaq	72(%r12), %rdi
	leaq	136(%r12), %rsi
	leaq	152(%r12), %rdx
	movss	328(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	260(%r12), %xmm0
.Ltmp153:
	movq	%r14, %rcx
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
.Ltmp154:
# BB#8:                                 # %.noexc
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	512(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp155:
	movq	%r14, %rsi
	callq	*24(%rax)
.Ltmp156:
# BB#9:                                 # %.noexc33
                                        #   in Loop: Header=BB13_3 Depth=1
	movl	12(%r15), %eax
	.p2align	4, 0x90
.LBB13_10:                              # %_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody.exit
                                        #   in Loop: Header=BB13_3 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB13_3
	jmp	.LBB13_21
.LBB13_11:                              # %.preheader
	movl	284(%r15), %ecx
	testl	%ecx, %ecx
	jle	.LBB13_21
# BB#12:                                # %.lr.ph
	xorl	%ebx, %ebx
	leaq	72(%rsp), %r14
	.p2align	4, 0x90
.LBB13_13:                              # =>This Inner Loop Header: Depth=1
	movq	296(%r15), %rax
	movq	(%rax,%rbx,8), %r12
	movl	228(%r12), %eax
	cmpl	$2, %eax
	je	.LBB13_20
# BB#14:                                #   in Loop: Header=BB13_13 Depth=1
	cmpl	$5, %eax
	je	.LBB13_20
# BB#15:                                #   in Loop: Header=BB13_13 Depth=1
	cmpq	$0, 512(%r12)
	je	.LBB13_20
# BB#16:                                #   in Loop: Header=BB13_13 Depth=1
	testb	$3, 216(%r12)
	jne	.LBB13_20
# BB#17:                                #   in Loop: Header=BB13_13 Depth=1
	leaq	72(%r12), %rdi
	leaq	136(%r12), %rsi
	leaq	152(%r12), %rdx
	movss	328(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	260(%r12), %xmm0
.Ltmp158:
	movq	%r14, %rcx
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
.Ltmp159:
# BB#18:                                # %.noexc34
                                        #   in Loop: Header=BB13_13 Depth=1
	movq	512(%r12), %rdi
	movq	(%rdi), %rax
.Ltmp160:
	movq	%r14, %rsi
	callq	*24(%rax)
.Ltmp161:
# BB#19:                                # %.noexc35
                                        #   in Loop: Header=BB13_13 Depth=1
	movl	284(%r15), %ecx
	.p2align	4, 0x90
.LBB13_20:                              # %_ZN23btDiscreteDynamicsWorld28synchronizeSingleMotionStateEP11btRigidBody.exit36
                                        #   in Loop: Header=BB13_13 Depth=1
	incq	%rbx
	movslq	%ecx, %rax
	cmpq	%rax, %rbx
	jl	.LBB13_13
.LBB13_21:                              # %.loopexit
	callq	_ZN15CProfileManager12Stop_ProfileEv
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB13_26:
.Ltmp162:
	jmp	.LBB13_23
.LBB13_22:
.Ltmp157:
.LBB13_23:
	movq	%rax, %rbx
.Ltmp163:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp164:
# BB#24:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB13_25:
.Ltmp165:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end13:
	.size	_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv, .Lfunc_end13-_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp153-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp153
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp156-.Ltmp153       #   Call between .Ltmp153 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin5  #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp158-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp161-.Ltmp158       #   Call between .Ltmp158 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin5  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp161-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp163-.Ltmp161       #   Call between .Ltmp161 and .Ltmp163
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin5  #     jumps to .Ltmp165
	.byte	1                       #   On action: 1
	.long	.Ltmp164-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Lfunc_end13-.Ltmp164   #   Call between .Ltmp164 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_1:
	.long	872415232               # float 1.1920929E-7
	.text
	.globl	_ZN23btDiscreteDynamicsWorld14stepSimulationEfif
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld14stepSimulationEfif,@function
_ZN23btDiscreteDynamicsWorld14stepSimulationEfif: # @_ZN23btDiscreteDynamicsWorld14stepSimulationEfif
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi98:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 80
.Lcfi103:
	.cfi_offset %rbx, -40
.Lcfi104:
	.cfi_offset %r14, -32
.Lcfi105:
	.cfi_offset %r15, -24
.Lcfi106:
	.cfi_offset %rbp, -16
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movl	%esi, %r15d
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	%rdi, %rbx
	callq	_ZN15CProfileManager5ResetEv
	movl	$.L.str.2, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	testl	%r15d, %r15d
	je	.LBB14_3
# BB#1:
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	addss	328(%rbx), %xmm2
	movss	%xmm2, 328(%rbx)
	xorl	%r14d, %r14d
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	movaps	%xmm0, %xmm1
	jb	.LBB14_4
# BB#2:
	movaps	%xmm2, %xmm0
	divss	%xmm1, %xmm0
	cvttss2si	%xmm0, %r14d
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r14d, %xmm0
	mulss	%xmm1, %xmm0
	subss	%xmm0, %xmm2
	movss	%xmm2, 328(%rbx)
	jmp	.LBB14_4
.LBB14_3:
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	movss	%xmm2, 328(%rbx)
	movaps	.LCPI14_0(%rip), %xmm0  # xmm0 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm0
	movss	.LCPI14_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	xorl	%r14d, %r14d
	ucomiss	%xmm0, %xmm1
	setbe	%r14b
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movl	%r14d, %r15d
.LBB14_4:
	movq	(%rbx), %rax
.Ltmp166:
	movq	%rbx, %rdi
	callq	*32(%rax)
.Ltmp167:
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
# BB#5:
	testq	%rax, %rax
	je	.LBB14_9
# BB#6:
	movq	(%rbx), %rax
.Ltmp168:
	movq	%rbx, %rdi
	callq	*32(%rax)
.Ltmp169:
# BB#7:
	movq	(%rax), %rcx
.Ltmp170:
	movq	%rax, %rdi
	callq	*96(%rcx)
.Ltmp171:
# BB#8:
	shrb	$4, %al
	andb	$1, %al
	movb	%al, gDisableDeactivation(%rip)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB14_9:
	testl	%r14d, %r14d
	je	.LBB14_18
# BB#10:
	movq	(%rbx), %rax
.Ltmp173:
	movq	%rbx, %rdi
	callq	*280(%rax)
.Ltmp174:
# BB#11:
	movq	(%rbx), %rax
.Ltmp175:
	movq	%rbx, %rdi
	callq	*296(%rax)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.Ltmp176:
# BB#12:
	cmpl	%r15d, %r14d
	cmovlel	%r14d, %r15d
	testl	%r15d, %r15d
	jle	.LBB14_19
# BB#13:                                # %.lr.ph.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB14_14:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
.Ltmp177:
	movq	%rbx, %rdi
	callq	*272(%rax)
.Ltmp178:
# BB#15:                                #   in Loop: Header=BB14_14 Depth=1
	movq	(%rbx), %rax
.Ltmp179:
	movq	%rbx, %rdi
	callq	*128(%rax)
.Ltmp180:
# BB#16:                                #   in Loop: Header=BB14_14 Depth=1
	incl	%ebp
	cmpl	%r15d, %ebp
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	jl	.LBB14_14
	jmp	.LBB14_19
.LBB14_18:
	movq	(%rbx), %rax
.Ltmp182:
	movq	%rbx, %rdi
	callq	*128(%rax)
.Ltmp183:
.LBB14_19:                              # %.loopexit
	movq	(%rbx), %rax
.Ltmp184:
	movq	%rbx, %rdi
	callq	*200(%rax)
.Ltmp185:
# BB#20:
.Ltmp186:
	callq	_ZN15CProfileManager23Increment_Frame_CounterEv
.Ltmp187:
# BB#21:
	callq	_ZN15CProfileManager12Stop_ProfileEv
	movl	%r14d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB14_26:
.Ltmp172:
	jmp	.LBB14_23
.LBB14_22:
.Ltmp188:
	jmp	.LBB14_23
.LBB14_17:
.Ltmp181:
.LBB14_23:
	movq	%rax, %rbx
.Ltmp189:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp190:
# BB#24:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_25:
.Ltmp191:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN23btDiscreteDynamicsWorld14stepSimulationEfif, .Lfunc_end14-_ZN23btDiscreteDynamicsWorld14stepSimulationEfif
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp166-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp166
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp169-.Ltmp166       #   Call between .Ltmp166 and .Ltmp169
	.long	.Ltmp188-.Lfunc_begin6  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp171-.Ltmp170       #   Call between .Ltmp170 and .Ltmp171
	.long	.Ltmp172-.Lfunc_begin6  #     jumps to .Ltmp172
	.byte	0                       #   On action: cleanup
	.long	.Ltmp173-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp176-.Ltmp173       #   Call between .Ltmp173 and .Ltmp176
	.long	.Ltmp188-.Lfunc_begin6  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp177-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp180-.Ltmp177       #   Call between .Ltmp177 and .Ltmp180
	.long	.Ltmp181-.Lfunc_begin6  #     jumps to .Ltmp181
	.byte	0                       #   On action: cleanup
	.long	.Ltmp182-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp187-.Ltmp182       #   Call between .Ltmp182 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin6  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp189-.Ltmp187       #   Call between .Ltmp187 and .Ltmp189
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin6  # >> Call Site 8 <<
	.long	.Ltmp190-.Ltmp189       #   Call between .Ltmp189 and .Ltmp190
	.long	.Ltmp191-.Lfunc_begin6  #     jumps to .Ltmp191
	.byte	1                       #   On action: 1
	.long	.Ltmp190-.Lfunc_begin6  # >> Call Site 9 <<
	.long	.Lfunc_end14-.Ltmp190   #   Call between .Ltmp190 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btDiscreteDynamicsWorld14startProfilingEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld14startProfilingEf,@function
_ZN23btDiscreteDynamicsWorld14startProfilingEf: # @_ZN23btDiscreteDynamicsWorld14startProfilingEf
	.cfi_startproc
# BB#0:
	jmp	_ZN15CProfileManager5ResetEv # TAILCALL
.Lfunc_end15:
	.size	_ZN23btDiscreteDynamicsWorld14startProfilingEf, .Lfunc_end15-_ZN23btDiscreteDynamicsWorld14startProfilingEf
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf,@function
_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf: # @_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi109:
	.cfi_def_cfa_offset 32
.Lcfi110:
	.cfi_offset %rbx, -24
.Lcfi111:
	.cfi_offset %r14, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	$.L.str.3, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movq	144(%r14), %rax
	testq	%rax, %rax
	je	.LBB16_2
# BB#1:
.Ltmp192:
	movq	%r14, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*%rax
.Ltmp193:
.LBB16_2:
	movq	(%r14), %rax
.Ltmp194:
	movq	%r14, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*240(%rax)
.Ltmp195:
# BB#3:
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%r14)
	movl	$0, 52(%r14)
	movq	(%r14), %rax
.Ltmp197:
	movq	%r14, %rdi
	callq	*32(%rax)
.Ltmp198:
# BB#4:
	movq	%rax, 72(%r14)
	movq	(%r14), %rax
.Ltmp199:
	movq	%r14, %rdi
	callq	*56(%rax)
.Ltmp200:
# BB#5:
	movq	(%r14), %rax
.Ltmp201:
	movq	%r14, %rdi
	callq	*256(%rax)
.Ltmp202:
# BB#6:
	leaq	160(%r14), %rsi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 172(%r14)
	movq	(%r14), %rax
.Ltmp203:
	movq	%r14, %rdi
	callq	*264(%rax)
.Ltmp204:
# BB#7:
	movq	(%r14), %rax
.Ltmp205:
	movq	%r14, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*248(%rax)
.Ltmp206:
# BB#8:
.Ltmp207:
	movl	$.L.str.4, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
.Ltmp208:
# BB#9:                                 # %.noexc
	cmpl	$0, 340(%r14)
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	jle	.LBB16_13
# BB#10:                                # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB16_11:                              # =>This Inner Loop Header: Depth=1
	movq	352(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
.Ltmp209:
	movq	%r14, %rsi
	callq	*16(%rax)
.Ltmp210:
# BB#12:                                #   in Loop: Header=BB16_11 Depth=1
	incq	%rbx
	movslq	340(%r14), %rax
	cmpq	%rax, %rbx
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	jl	.LBB16_11
.LBB16_13:                              # %._crit_edge.i
.Ltmp215:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp216:
# BB#14:                                # %_ZN23btDiscreteDynamicsWorld13updateActionsEf.exit
.Ltmp217:
	movq	%r14, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN23btDiscreteDynamicsWorld21updateActivationStateEf
.Ltmp218:
# BB#15:
	movq	136(%r14), %rax
	testq	%rax, %rax
	je	.LBB16_17
# BB#16:
.Ltmp219:
	movq	%r14, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*%rax
.Ltmp220:
.LBB16_17:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB16_21:
.Ltmp196:
	jmp	.LBB16_22
.LBB16_20:
.Ltmp221:
.LBB16_22:                              # %.body
	movq	%rax, %rbx
	jmp	.LBB16_23
.LBB16_18:
.Ltmp211:
	movq	%rax, %rbx
.Ltmp212:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp213:
.LBB16_23:                              # %.body
.Ltmp222:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp223:
# BB#24:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_19:
.Ltmp214:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_25:
.Ltmp224:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf, .Lfunc_end16-_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin7-.Lfunc_begin7 # >> Call Site 1 <<
	.long	.Ltmp192-.Lfunc_begin7  #   Call between .Lfunc_begin7 and .Ltmp192
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp192-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp195-.Ltmp192       #   Call between .Ltmp192 and .Ltmp195
	.long	.Ltmp196-.Lfunc_begin7  #     jumps to .Ltmp196
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp208-.Ltmp197       #   Call between .Ltmp197 and .Ltmp208
	.long	.Ltmp221-.Lfunc_begin7  #     jumps to .Ltmp221
	.byte	0                       #   On action: cleanup
	.long	.Ltmp209-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Ltmp210-.Ltmp209       #   Call between .Ltmp209 and .Ltmp210
	.long	.Ltmp211-.Lfunc_begin7  #     jumps to .Ltmp211
	.byte	0                       #   On action: cleanup
	.long	.Ltmp215-.Lfunc_begin7  # >> Call Site 5 <<
	.long	.Ltmp220-.Ltmp215       #   Call between .Ltmp215 and .Ltmp220
	.long	.Ltmp221-.Lfunc_begin7  #     jumps to .Ltmp221
	.byte	0                       #   On action: cleanup
	.long	.Ltmp220-.Lfunc_begin7  # >> Call Site 6 <<
	.long	.Ltmp212-.Ltmp220       #   Call between .Ltmp220 and .Ltmp212
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp212-.Lfunc_begin7  # >> Call Site 7 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin7  #     jumps to .Ltmp214
	.byte	1                       #   On action: 1
	.long	.Ltmp222-.Lfunc_begin7  # >> Call Site 8 <<
	.long	.Ltmp223-.Ltmp222       #   Call between .Ltmp222 and .Ltmp223
	.long	.Ltmp224-.Lfunc_begin7  #     jumps to .Ltmp224
	.byte	1                       #   On action: 1
	.long	.Ltmp223-.Lfunc_begin7  # >> Call Site 9 <<
	.long	.Lfunc_end16-.Ltmp223   #   Call between .Ltmp223 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btDiscreteDynamicsWorld13updateActionsEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld13updateActionsEf,@function
_ZN23btDiscreteDynamicsWorld13updateActionsEf: # @_ZN23btDiscreteDynamicsWorld13updateActionsEf
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi113:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 32
.Lcfi115:
	.cfi_offset %rbx, -24
.Lcfi116:
	.cfi_offset %r14, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	$.L.str.4, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpl	$0, 340(%r14)
	jle	.LBB17_4
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB17_2:                               # =>This Inner Loop Header: Depth=1
	movq	352(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
.Ltmp225:
	movq	%r14, %rsi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*16(%rax)
.Ltmp226:
# BB#3:                                 #   in Loop: Header=BB17_2 Depth=1
	incq	%rbx
	movslq	340(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB17_2
.LBB17_4:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB17_5:
.Ltmp227:
	movq	%rax, %rbx
.Ltmp228:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp229:
# BB#6:                                 # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB17_7:
.Ltmp230:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN23btDiscreteDynamicsWorld13updateActionsEf, .Lfunc_end17-_ZN23btDiscreteDynamicsWorld13updateActionsEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp225-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp225
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp225-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp226-.Ltmp225       #   Call between .Ltmp225 and .Ltmp226
	.long	.Ltmp227-.Lfunc_begin8  #     jumps to .Ltmp227
	.byte	0                       #   On action: cleanup
	.long	.Ltmp226-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp228-.Ltmp226       #   Call between .Ltmp226 and .Ltmp228
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin8  #     jumps to .Ltmp230
	.byte	1                       #   On action: 1
	.long	.Ltmp229-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Lfunc_end17-.Ltmp229   #   Call between .Ltmp229 and .Lfunc_end17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_0:
	.long	0                       # float 0
	.text
	.globl	_ZN23btDiscreteDynamicsWorld21updateActivationStateEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld21updateActivationStateEf,@function
_ZN23btDiscreteDynamicsWorld21updateActivationStateEf: # @_ZN23btDiscreteDynamicsWorld21updateActivationStateEf
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi119:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi120:
	.cfi_def_cfa_offset 48
.Lcfi121:
	.cfi_offset %rbx, -32
.Lcfi122:
	.cfi_offset %r14, -24
.Lcfi123:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %r14
	movl	$.L.str.5, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpl	$0, 284(%r14)
	jle	.LBB18_24
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	jmp	.LBB18_2
.LBB18_17:                              #   in Loop: Header=BB18_2 Depth=1
	cmpl	$1, %eax
	jne	.LBB18_20
# BB#18:                                #   in Loop: Header=BB18_2 Depth=1
.Ltmp235:
	movl	$3, %esi
	movq	%r15, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
.Ltmp236:
# BB#19:                                # %thread-pre-split
                                        #   in Loop: Header=BB18_2 Depth=1
	movl	228(%r15), %eax
.LBB18_20:                              #   in Loop: Header=BB18_2 Depth=1
	cmpl	$2, %eax
	jne	.LBB18_23
# BB#21:                                #   in Loop: Header=BB18_2 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 344(%r15)
	movups	%xmm0, 328(%r15)
	jmp	.LBB18_23
	.p2align	4, 0x90
.LBB18_2:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r14), %rax
	movq	(%rax,%rbx,8), %r15
	testq	%r15, %r15
	je	.LBB18_23
# BB#3:                                 #   in Loop: Header=BB18_2 Depth=1
	movl	228(%r15), %eax
	cmpl	$2, %eax
	je	.LBB18_10
# BB#4:                                 #   in Loop: Header=BB18_2 Depth=1
	cmpl	$4, %eax
	je	.LBB18_10
# BB#5:                                 #   in Loop: Header=BB18_2 Depth=1
	movss	328(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	332(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	336(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	504(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_8
# BB#6:                                 #   in Loop: Header=BB18_2 Depth=1
	movss	344(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	348(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	352(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	508(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_8
# BB#7:                                 # %_ZN11btRigidBody18updateDeactivationEf.exit.thread
                                        #   in Loop: Header=BB18_2 Depth=1
	movss	232(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	12(%rsp), %xmm0         # 4-byte Folded Reload
	movss	%xmm0, 232(%r15)
	cmpb	$0, gDisableDeactivation(%rip)
	je	.LBB18_12
	jmp	.LBB18_22
.LBB18_8:                               #   in Loop: Header=BB18_2 Depth=1
	movl	$0, 232(%r15)
.Ltmp231:
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
.Ltmp232:
# BB#9:                                 # %._ZN11btRigidBody18updateDeactivationEf.exitthread-pre-split_crit_edge
                                        #   in Loop: Header=BB18_2 Depth=1
	movl	228(%r15), %eax
.LBB18_10:                              # %_ZN11btRigidBody18updateDeactivationEf.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	cmpl	$4, %eax
	je	.LBB18_23
# BB#11:                                #   in Loop: Header=BB18_2 Depth=1
	cmpb	$0, gDisableDeactivation(%rip)
	jne	.LBB18_22
.LBB18_12:                              #   in Loop: Header=BB18_2 Depth=1
	movss	gDeactivationTime(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI18_0, %xmm0
	jne	.LBB18_13
	jnp	.LBB18_22
.LBB18_13:                              #   in Loop: Header=BB18_2 Depth=1
	movl	%eax, %ecx
	andl	$-2, %ecx
	cmpl	$2, %ecx
	je	.LBB18_15
# BB#14:                                # %_ZN11btRigidBody13wantsSleepingEv.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movss	232(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB18_22
.LBB18_15:                              # %_ZN11btRigidBody13wantsSleepingEv.exit.thread52
                                        #   in Loop: Header=BB18_2 Depth=1
	testb	$3, 216(%r15)
	je	.LBB18_17
# BB#16:                                #   in Loop: Header=BB18_2 Depth=1
.Ltmp233:
	movl	$2, %esi
	movq	%r15, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
.Ltmp234:
	jmp	.LBB18_23
	.p2align	4, 0x90
.LBB18_22:                              # %_ZN11btRigidBody13wantsSleepingEv.exit.thread
                                        #   in Loop: Header=BB18_2 Depth=1
.Ltmp237:
	movl	$1, %esi
	movq	%r15, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
.Ltmp238:
.LBB18_23:                              # %_ZN11btRigidBody13wantsSleepingEv.exit.thread.thread
                                        #   in Loop: Header=BB18_2 Depth=1
	incq	%rbx
	movslq	284(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB18_2
.LBB18_24:                              # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB18_25:
.Ltmp239:
	movq	%rax, %rbx
.Ltmp240:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp241:
# BB#26:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB18_27:
.Ltmp242:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end18:
	.size	_ZN23btDiscreteDynamicsWorld21updateActivationStateEf, .Lfunc_end18-_ZN23btDiscreteDynamicsWorld21updateActivationStateEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table18:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin9-.Lfunc_begin9 # >> Call Site 1 <<
	.long	.Ltmp235-.Lfunc_begin9  #   Call between .Lfunc_begin9 and .Ltmp235
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp235-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp238-.Ltmp235       #   Call between .Ltmp235 and .Ltmp238
	.long	.Ltmp239-.Lfunc_begin9  #     jumps to .Ltmp239
	.byte	0                       #   On action: cleanup
	.long	.Ltmp238-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp240-.Ltmp238       #   Call between .Ltmp238 and .Ltmp240
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp240-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Ltmp241-.Ltmp240       #   Call between .Ltmp240 and .Ltmp241
	.long	.Ltmp242-.Lfunc_begin9  #     jumps to .Ltmp242
	.byte	1                       #   On action: 1
	.long	.Ltmp241-.Lfunc_begin9  # >> Call Site 5 <<
	.long	.Lfunc_end18-.Ltmp241   #   Call between .Ltmp241 and .Lfunc_end18
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3,@function
_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3: # @_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi124:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi125:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 32
.Lcfi127:
	.cfi_offset %rbx, -32
.Lcfi128:
	.cfi_offset %r14, -24
.Lcfi129:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movups	(%r14), %xmm0
	movups	%xmm0, 312(%r15)
	movl	284(%r15), %eax
	testl	%eax, %eax
	jle	.LBB19_6
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB19_2:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r15), %rcx
	movq	(%rcx,%rbx,8), %rdi
	movl	228(%rdi), %ecx
	cmpl	$2, %ecx
	je	.LBB19_5
# BB#3:                                 #   in Loop: Header=BB19_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB19_5
# BB#4:                                 #   in Loop: Header=BB19_2 Depth=1
	movq	%r14, %rsi
	callq	_ZN11btRigidBody10setGravityERK9btVector3
	movl	284(%r15), %eax
.LBB19_5:                               #   in Loop: Header=BB19_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB19_2
.LBB19_6:                               # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3, .Lfunc_end19-_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3
	.cfi_endproc

	.globl	_ZNK23btDiscreteDynamicsWorld10getGravityEv
	.p2align	4, 0x90
	.type	_ZNK23btDiscreteDynamicsWorld10getGravityEv,@function
_ZNK23btDiscreteDynamicsWorld10getGravityEv: # @_ZNK23btDiscreteDynamicsWorld10getGravityEv
	.cfi_startproc
# BB#0:
	movsd	312(%rdi), %xmm0        # xmm0 = mem[0],zero
	movsd	320(%rdi), %xmm1        # xmm1 = mem[0],zero
	retq
.Lfunc_end20:
	.size	_ZNK23btDiscreteDynamicsWorld10getGravityEv, .Lfunc_end20-_ZNK23btDiscreteDynamicsWorld10getGravityEv
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss,@function
_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss: # @_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss
	.cfi_startproc
# BB#0:
	jmp	_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss # TAILCALL
.Lfunc_end21:
	.size	_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss, .Lfunc_end21-_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject,@function
_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject: # @_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB22_3
# BB#1:
	cmpl	$2, 256(%rsi)
	jne	.LBB22_3
# BB#2:
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB22_3:
	jmp	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject # TAILCALL
.Lfunc_end22:
	.size	_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject, .Lfunc_end22-_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody,@function
_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody: # @_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody
	.cfi_startproc
# BB#0:
	movslq	284(%rdi), %r10
	testq	%r10, %r10
	jle	.LBB23_6
# BB#1:                                 # %.lr.ph.i.i
	movq	296(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB23_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rcx,%rdx,8)
	je	.LBB23_4
# BB#3:                                 #   in Loop: Header=BB23_2 Depth=1
	incq	%rdx
	cmpq	%r10, %rdx
	jl	.LBB23_2
	jmp	.LBB23_6
.LBB23_4:                               # %_ZNK20btAlignedObjectArrayIP11btRigidBodyE16findLinearSearchERKS1_.exit.i
	cmpl	%r10d, %edx
	jge	.LBB23_6
# BB#5:
	leal	-1(%r10), %r8d
	movq	(%rcx,%rdx,8), %r9
	movq	-8(%rcx,%r10,8), %rax
	movq	%rax, (%rcx,%rdx,8)
	movq	296(%rdi), %rax
	movq	%r9, -8(%rax,%r10,8)
	movl	%r8d, 284(%rdi)
.LBB23_6:                               # %_ZN20btAlignedObjectArrayIP11btRigidBodyE6removeERKS1_.exit
	jmp	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject # TAILCALL
.Lfunc_end23:
	.size	_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody, .Lfunc_end23-_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody,@function
_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody: # @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi130:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi131:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi132:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi133:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi134:
	.cfi_def_cfa_offset 48
.Lcfi135:
	.cfi_offset %rbx, -40
.Lcfi136:
	.cfi_offset %r14, -32
.Lcfi137:
	.cfi_offset %r15, -24
.Lcfi138:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testb	$3, 216(%r14)
	jne	.LBB24_2
# BB#1:
	leaq	312(%rbx), %rsi
	movq	%r14, %rdi
	callq	_ZN11btRigidBody10setGravityERK9btVector3
.LBB24_2:
	cmpq	$0, 200(%r14)
	je	.LBB24_22
# BB#3:
	testb	$1, 216(%r14)
	jne	.LBB24_20
# BB#4:
	movl	284(%rbx), %eax
	cmpl	288(%rbx), %eax
	jne	.LBB24_19
# BB#5:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB24_19
# BB#6:
	testl	%ebp, %ebp
	je	.LBB24_7
# BB#8:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	284(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB24_10
	jmp	.LBB24_14
.LBB24_22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_20:
	movl	$2, %esi
	movq	%r14, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
	jmp	.LBB24_21
.LBB24_7:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB24_14
.LBB24_10:                              # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB24_12
	.p2align	4, 0x90
.LBB24_11:                              # =>This Inner Loop Header: Depth=1
	movq	296(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB24_11
.LBB24_12:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB24_14
	.p2align	4, 0x90
.LBB24_13:                              # =>This Inner Loop Header: Depth=1
	movq	296(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	296(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	296(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	296(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB24_13
.LBB24_14:                              # %_ZNK20btAlignedObjectArrayIP11btRigidBodyE4copyEiiPS1_.exit.i.i
	movq	296(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB24_18
# BB#15:
	cmpb	$0, 304(%rbx)
	je	.LBB24_17
# BB#16:
	callq	_Z21btAlignedFreeInternalPv
	movl	284(%rbx), %eax
.LBB24_17:
	movq	$0, 296(%rbx)
.LBB24_18:                              # %_ZN20btAlignedObjectArrayIP11btRigidBodyE10deallocateEv.exit.i.i
	movb	$1, 304(%rbx)
	movq	%r15, 296(%rbx)
	movl	%ebp, 288(%rbx)
.LBB24_19:                              # %_ZN20btAlignedObjectArrayIP11btRigidBodyE9push_backERKS1_.exit
	movq	296(%rbx), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 284(%rbx)
.LBB24_21:
	movl	216(%r14), %eax
	xorl	%edx, %edx
	andl	$3, %eax
	setne	%dl
	incl	%edx
	cmpl	$1, %eax
	sbbl	%eax, %eax
	orl	$65533, %eax            # imm = 0xFFFD
	movq	(%rbx), %rcx
	movq	40(%rcx), %r8
	movswl	%ax, %ecx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%r8                    # TAILCALL
.Lfunc_end24:
	.size	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody, .Lfunc_end24-_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss,@function
_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss: # @_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi139:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi140:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi141:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi142:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi143:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi144:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi145:
	.cfi_def_cfa_offset 64
.Lcfi146:
	.cfi_offset %rbx, -56
.Lcfi147:
	.cfi_offset %r12, -48
.Lcfi148:
	.cfi_offset %r13, -40
.Lcfi149:
	.cfi_offset %r14, -32
.Lcfi150:
	.cfi_offset %r15, -24
.Lcfi151:
	.cfi_offset %rbp, -16
	movl	%ecx, %r14d
	movl	%edx, %r15d
	movq	%rsi, %r12
	movq	%rdi, %rbp
	testb	$3, 216(%r12)
	jne	.LBB25_2
# BB#1:
	leaq	312(%rbp), %rsi
	movq	%r12, %rdi
	callq	_ZN11btRigidBody10setGravityERK9btVector3
.LBB25_2:
	cmpq	$0, 200(%r12)
	je	.LBB25_22
# BB#3:
	testb	$1, 216(%r12)
	jne	.LBB25_20
# BB#4:
	movl	284(%rbp), %eax
	cmpl	288(%rbp), %eax
	jne	.LBB25_19
# BB#5:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r13d
	cmovnel	%ecx, %r13d
	cmpl	%r13d, %eax
	jge	.LBB25_19
# BB#6:
	testl	%r13d, %r13d
	je	.LBB25_7
# BB#8:
	movslq	%r13d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	284(%rbp), %eax
	testl	%eax, %eax
	jg	.LBB25_10
	jmp	.LBB25_14
.LBB25_22:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_20:
	movl	$2, %esi
	movq	%r12, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
	jmp	.LBB25_21
.LBB25_7:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB25_14
.LBB25_10:                              # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB25_12
	.p2align	4, 0x90
.LBB25_11:                              # =>This Inner Loop Header: Depth=1
	movq	296(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB25_11
.LBB25_12:                              # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB25_14
	.p2align	4, 0x90
.LBB25_13:                              # =>This Inner Loop Header: Depth=1
	movq	296(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	296(%rbp), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	296(%rbp), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	296(%rbp), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB25_13
.LBB25_14:                              # %_ZNK20btAlignedObjectArrayIP11btRigidBodyE4copyEiiPS1_.exit.i.i
	movq	296(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB25_18
# BB#15:
	cmpb	$0, 304(%rbp)
	je	.LBB25_17
# BB#16:
	callq	_Z21btAlignedFreeInternalPv
	movl	284(%rbp), %eax
.LBB25_17:
	movq	$0, 296(%rbp)
.LBB25_18:                              # %_ZN20btAlignedObjectArrayIP11btRigidBodyE10deallocateEv.exit.i.i
	movb	$1, 304(%rbp)
	movq	%rbx, 296(%rbp)
	movl	%r13d, 288(%rbp)
.LBB25_19:                              # %_ZN20btAlignedObjectArrayIP11btRigidBodyE9push_backERKS1_.exit
	movq	296(%rbp), %rcx
	cltq
	movq	%r12, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 284(%rbp)
.LBB25_21:
	movq	(%rbp), %rax
	movq	40(%rax), %rax
	movswl	%r15w, %edx
	movswl	%r14w, %ecx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end25:
	.size	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss, .Lfunc_end25-_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb,@function
_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb: # @_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi152:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi153:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi154:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi155:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi156:
	.cfi_def_cfa_offset 48
.Lcfi157:
	.cfi_offset %rbx, -48
.Lcfi158:
	.cfi_offset %r12, -40
.Lcfi159:
	.cfi_offset %r14, -32
.Lcfi160:
	.cfi_offset %r15, -24
.Lcfi161:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbp
	movl	252(%rbp), %eax
	cmpl	256(%rbp), %eax
	jne	.LBB26_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r12d
	cmovnel	%ecx, %r12d
	cmpl	%r12d, %eax
	jge	.LBB26_15
# BB#2:
	testl	%r12d, %r12d
	je	.LBB26_3
# BB#4:
	movslq	%r12d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbx
	movl	252(%rbp), %eax
	testl	%eax, %eax
	jg	.LBB26_6
	jmp	.LBB26_10
.LBB26_3:
	xorl	%ebx, %ebx
	testl	%eax, %eax
	jle	.LBB26_10
.LBB26_6:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB26_8
	.p2align	4, 0x90
.LBB26_7:                               # =>This Inner Loop Header: Depth=1
	movq	264(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB26_7
.LBB26_8:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB26_10
	.p2align	4, 0x90
.LBB26_9:                               # =>This Inner Loop Header: Depth=1
	movq	264(%rbp), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbx,%rdx,8)
	movq	264(%rbp), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbx,%rdx,8)
	movq	264(%rbp), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbx,%rdx,8)
	movq	264(%rbp), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbx,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB26_9
.LBB26_10:                              # %_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_.exit.i.i
	movq	264(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB26_14
# BB#11:
	cmpb	$0, 272(%rbp)
	je	.LBB26_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	252(%rbp), %eax
.LBB26_13:
	movq	$0, 264(%rbp)
.LBB26_14:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE10deallocateEv.exit.i.i
	movb	$1, 272(%rbp)
	movq	%rbx, 264(%rbp)
	movl	%r12d, 256(%rbp)
.LBB26_15:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE9push_backERKS1_.exit
	movq	264(%rbp), %rcx
	cltq
	movq	%r15, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 252(%rbp)
	testb	%r14b, %r14b
	je	.LBB26_16
# BB#17:
	movq	24(%r15), %rdi
	movq	%r15, %rsi
	callq	_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint
	movq	32(%r15), %rdi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN11btRigidBody16addConstraintRefEP17btTypedConstraint # TAILCALL
.LBB26_16:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end26:
	.size	_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb, .Lfunc_end26-_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint,@function
_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint: # @_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi162:
	.cfi_def_cfa_offset 16
.Lcfi163:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movslq	252(%rdi), %rax
	testq	%rax, %rax
	jle	.LBB27_6
# BB#1:                                 # %.lr.ph.i.i
	movq	264(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB27_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, (%rcx,%rdx,8)
	je	.LBB27_4
# BB#3:                                 #   in Loop: Header=BB27_2 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB27_2
	jmp	.LBB27_6
.LBB27_4:                               # %_ZNK20btAlignedObjectArrayIP17btTypedConstraintE16findLinearSearchERKS1_.exit.i
	cmpl	%eax, %edx
	jge	.LBB27_6
# BB#5:
	leal	-1(%rax), %r8d
	movq	(%rcx,%rdx,8), %r9
	movq	-8(%rcx,%rax,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	264(%rdi), %rcx
	movq	%r9, -8(%rcx,%rax,8)
	movl	%r8d, 252(%rdi)
.LBB27_6:                               # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE6removeERKS1_.exit
	movq	24(%rbx), %rdi
	movq	%rbx, %rsi
	callq	_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint
	movq	32(%rbx), %rdi
	movq	%rbx, %rsi
	popq	%rbx
	jmp	_ZN11btRigidBody19removeConstraintRefEP17btTypedConstraint # TAILCALL
.Lfunc_end27:
	.size	_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint, .Lfunc_end27-_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface,@function
_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface: # @_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi166:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi167:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi168:
	.cfi_def_cfa_offset 48
.Lcfi169:
	.cfi_offset %rbx, -40
.Lcfi170:
	.cfi_offset %r14, -32
.Lcfi171:
	.cfi_offset %r15, -24
.Lcfi172:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	340(%rbx), %eax
	cmpl	344(%rbx), %eax
	jne	.LBB28_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %ebp
	cmovnel	%ecx, %ebp
	cmpl	%ebp, %eax
	jge	.LBB28_15
# BB#2:
	testl	%ebp, %ebp
	je	.LBB28_3
# BB#4:
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
	movl	340(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB28_6
	jmp	.LBB28_10
.LBB28_3:
	xorl	%r15d, %r15d
	testl	%eax, %eax
	jle	.LBB28_10
.LBB28_6:                               # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB28_8
	.p2align	4, 0x90
.LBB28_7:                               # =>This Inner Loop Header: Depth=1
	movq	352(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB28_7
.LBB28_8:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB28_10
	.p2align	4, 0x90
.LBB28_9:                               # =>This Inner Loop Header: Depth=1
	movq	352(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%r15,%rdx,8)
	movq	352(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%r15,%rdx,8)
	movq	352(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%r15,%rdx,8)
	movq	352(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%r15,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB28_9
.LBB28_10:                              # %_ZNK20btAlignedObjectArrayIP17btActionInterfaceE4copyEiiPS1_.exit.i.i
	movq	352(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB28_14
# BB#11:
	cmpb	$0, 360(%rbx)
	je	.LBB28_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	340(%rbx), %eax
.LBB28_13:
	movq	$0, 352(%rbx)
.LBB28_14:                              # %_ZN20btAlignedObjectArrayIP17btActionInterfaceE10deallocateEv.exit.i.i
	movb	$1, 360(%rbx)
	movq	%r15, 352(%rbx)
	movl	%ebp, 344(%rbx)
.LBB28_15:                              # %_ZN20btAlignedObjectArrayIP17btActionInterfaceE9push_backERKS1_.exit
	movq	352(%rbx), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 340(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end28:
	.size	_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface, .Lfunc_end28-_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface,@function
_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface: # @_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface
	.cfi_startproc
# BB#0:
	movslq	340(%rdi), %rax
	testq	%rax, %rax
	jle	.LBB29_6
# BB#1:                                 # %.lr.ph.i.i
	movq	352(%rdi), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB29_2:                               # =>This Inner Loop Header: Depth=1
	cmpq	%rsi, (%rcx,%rdx,8)
	je	.LBB29_4
# BB#3:                                 #   in Loop: Header=BB29_2 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB29_2
	jmp	.LBB29_6
.LBB29_4:                               # %_ZNK20btAlignedObjectArrayIP17btActionInterfaceE16findLinearSearchERKS1_.exit.i
	cmpl	%eax, %edx
	jge	.LBB29_6
# BB#5:
	leal	-1(%rax), %r8d
	movq	(%rcx,%rdx,8), %r9
	movq	-8(%rcx,%rax,8), %rsi
	movq	%rsi, (%rcx,%rdx,8)
	movq	352(%rdi), %rcx
	movq	%r9, -8(%rcx,%rax,8)
	movl	%r8d, 340(%rdi)
.LBB29_6:                               # %_ZN20btAlignedObjectArrayIP17btActionInterfaceE6removeERKS1_.exit
	retq
.Lfunc_end29:
	.size	_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface, .Lfunc_end29-_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface,@function
_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface: # @_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end30:
	.size	_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface, .Lfunc_end30-_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface,@function
_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface: # @_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end31:
	.size	_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface, .Lfunc_end31-_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface,@function
_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface: # @_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	96(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end32:
	.size	_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface, .Lfunc_end32-_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface,@function
_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface: # @_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end33:
	.size	_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface, .Lfunc_end33-_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo,@function
_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo: # @_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%rbp
.Lcfi173:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi174:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi175:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi176:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi177:
	.cfi_def_cfa_offset 48
	subq	$96, %rsp
.Lcfi178:
	.cfi_def_cfa_offset 144
.Lcfi179:
	.cfi_offset %rbx, -48
.Lcfi180:
	.cfi_offset %r12, -40
.Lcfi181:
	.cfi_offset %r14, -32
.Lcfi182:
	.cfi_offset %r15, -24
.Lcfi183:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$.L.str.6, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movb	$1, 24(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 4(%rsp)
	movslq	252(%rbx), %r12
	testq	%r12, %r12
	jle	.LBB34_19
# BB#1:
	leaq	(,%r12,8), %rdi
.Ltmp243:
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %r15
.Ltmp244:
# BB#2:                                 # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE8allocateEi.exit.i.i
	movslq	4(%rsp), %rax
	testq	%rax, %rax
	jle	.LBB34_7
# BB#3:                                 # %.lr.ph.i.i.i
	leaq	-1(%rax), %rdx
	movq	%rax, %rsi
	xorl	%ecx, %ecx
	andq	$3, %rsi
	je	.LBB34_5
	.p2align	4, 0x90
.LBB34_4:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi
	movq	(%rdi,%rcx,8), %rdi
	movq	%rdi, (%r15,%rcx,8)
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB34_4
.LBB34_5:                               # %.prol.loopexit
	cmpq	$3, %rdx
	jb	.LBB34_7
	.p2align	4, 0x90
.LBB34_6:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdx
	movq	(%rdx,%rcx,8), %rdx
	movq	%rdx, (%r15,%rcx,8)
	movq	16(%rsp), %rdx
	movq	8(%rdx,%rcx,8), %rdx
	movq	%rdx, 8(%r15,%rcx,8)
	movq	16(%rsp), %rdx
	movq	16(%rdx,%rcx,8), %rdx
	movq	%rdx, 16(%r15,%rcx,8)
	movq	16(%rsp), %rdx
	movq	24(%rdx,%rcx,8), %rdx
	movq	%rdx, 24(%r15,%rcx,8)
	addq	$4, %rcx
	cmpq	%rcx, %rax
	jne	.LBB34_6
.LBB34_7:                               # %_ZNK20btAlignedObjectArrayIP17btTypedConstraintE4copyEiiPS1_.exit.i.i
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB34_11
# BB#8:
	cmpb	$0, 24(%rsp)
	je	.LBB34_10
# BB#9:
.Ltmp245:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp246:
.LBB34_10:                              # %.noexc20
	movq	$0, 16(%rsp)
.LBB34_11:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.preheader.i
	movb	$1, 24(%rsp)
	movq	%r15, 16(%rsp)
	movl	%r12d, 8(%rsp)
	movq	$0, (%r15)
	cmpl	$1, %r12d
	je	.LBB34_19
# BB#12:                                # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i._ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i_crit_edge.preheader
	leal	7(%r12), %edx
	leaq	-2(%r12), %rcx
	andq	$7, %rdx
	je	.LBB34_13
# BB#14:                                # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i._ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i_crit_edge.prol.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB34_15:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i._ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i_crit_edge.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rsi
	movq	$0, 8(%rsi,%rax,8)
	incq	%rax
	cmpq	%rax, %rdx
	jne	.LBB34_15
# BB#16:                                # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i._ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i_crit_edge.prol.loopexit.unr-lcssa
	incq	%rax
	cmpq	$7, %rcx
	jae	.LBB34_18
	jmp	.LBB34_19
.LBB34_13:
	movl	$1, %eax
	cmpq	$7, %rcx
	jb	.LBB34_19
	.p2align	4, 0x90
.LBB34_18:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i._ZN20btAlignedObjectArrayIP17btTypedConstraintE7reserveEi.exit.i_crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rcx
	movq	$0, (%rcx,%rax,8)
	movq	16(%rsp), %rcx
	movq	$0, 8(%rcx,%rax,8)
	movq	16(%rsp), %rcx
	movq	$0, 16(%rcx,%rax,8)
	movq	16(%rsp), %rcx
	movq	$0, 24(%rcx,%rax,8)
	movq	16(%rsp), %rcx
	movq	$0, 32(%rcx,%rax,8)
	movq	16(%rsp), %rcx
	movq	$0, 40(%rcx,%rax,8)
	movq	16(%rsp), %rcx
	movq	$0, 48(%rcx,%rax,8)
	movq	16(%rsp), %rcx
	movq	$0, 56(%rcx,%rax,8)
	addq	$8, %rax
	cmpq	%rax, %r12
	jne	.LBB34_18
.LBB34_19:                              # %.loopexit29
	movl	%r12d, 4(%rsp)
	xorl	%ebp, %ebp
	jmp	.LBB34_20
	.p2align	4, 0x90
.LBB34_22:                              #   in Loop: Header=BB34_20 Depth=1
	movq	264(%rbx), %rax
	movq	(%rax,%rbp,8), %rax
	movq	16(%rsp), %rcx
	movq	%rax, (%rcx,%rbp,8)
	incq	%rbp
.LBB34_20:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rax
.Ltmp248:
	movq	%rbx, %rdi
	callq	*168(%rax)
.Ltmp249:
# BB#21:                                #   in Loop: Header=BB34_20 Depth=1
	cltq
	cmpq	%rax, %rbp
	jl	.LBB34_22
# BB#25:
	cmpl	$2, %r12d
	jl	.LBB34_27
# BB#26:
	leal	-1(%r12), %edx
.Ltmp251:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	callq	_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii
.Ltmp252:
.LBB34_27:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintE9quickSortI33btSortConstraintOnIslandPredicateEEvT_.exit
	movq	(%rbx), %rax
.Ltmp253:
	movq	%rbx, %rdi
	callq	*168(%rax)
.Ltmp254:
# BB#28:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	cmovneq	16(%rsp), %rcx
	movq	232(%rbx), %r15
	movl	4(%rsp), %eax
	movq	120(%rbx), %rdx
	movq	40(%rbx), %rdi
	movq	104(%rbx), %rsi
	movq	$_ZTVZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback+16, 32(%rsp)
	movq	%r14, 40(%rsp)
	movq	%r15, 48(%rsp)
	movq	%rcx, 56(%rsp)
	movl	%eax, 64(%rsp)
	movq	%rdx, 72(%rsp)
	movq	%rsi, 80(%rsp)
	movq	%rdi, 88(%rsp)
	movq	(%r15), %rax
	movq	16(%rax), %r12
	movl	12(%rbx), %ebp
	movq	(%rdi), %rax
.Ltmp256:
	callq	*72(%rax)
.Ltmp257:
# BB#29:
.Ltmp258:
	movq	%r15, %rdi
	movl	%ebp, %esi
	movl	%eax, %edx
	callq	*%r12
.Ltmp259:
# BB#30:
	movq	40(%rbx), %rsi
	movq	240(%rbx), %rdi
.Ltmp260:
	leaq	32(%rsp), %rcx
	movq	%rbx, %rdx
	callq	_ZN25btSimulationIslandManager22buildAndProcessIslandsEP12btDispatcherP16btCollisionWorldPNS_14IslandCallbackE
.Ltmp261:
# BB#31:
	movq	232(%rbx), %rdi
	movq	(%rdi), %rax
	movq	104(%rbx), %rcx
	movq	120(%rbx), %rdx
.Ltmp262:
	movq	%r14, %rsi
	callq	*32(%rax)
.Ltmp263:
# BB#32:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB34_36
# BB#33:
	cmpb	$0, 24(%rsp)
	je	.LBB34_35
# BB#34:
.Ltmp267:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp268:
.LBB34_35:                              # %.noexc24
	movq	$0, 16(%rsp)
.LBB34_36:
	callq	_ZN15CProfileManager12Stop_ProfileEv
	addq	$96, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB34_38:
.Ltmp269:
	movq	%rax, %rbx
	jmp	.LBB34_45
.LBB34_39:
.Ltmp247:
	jmp	.LBB34_40
.LBB34_24:                              # %.loopexit.split-lp
.Ltmp255:
	jmp	.LBB34_40
.LBB34_37:
.Ltmp264:
	jmp	.LBB34_40
.LBB34_23:                              # %.loopexit
.Ltmp250:
.LBB34_40:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB34_44
# BB#41:
	cmpb	$0, 24(%rsp)
	je	.LBB34_43
# BB#42:
.Ltmp265:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp266:
.LBB34_43:                              # %.noexc21
	movq	$0, 16(%rsp)
.LBB34_44:                              # %_ZN20btAlignedObjectArrayIP17btTypedConstraintED2Ev.exit
	movb	$1, 24(%rsp)
	movq	$0, 16(%rsp)
	movq	$0, 4(%rsp)
.LBB34_45:
.Ltmp270:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp271:
# BB#46:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB34_47:
.Ltmp272:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end34:
	.size	_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo, .Lfunc_end34-_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin10-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp243-.Lfunc_begin10 #   Call between .Lfunc_begin10 and .Ltmp243
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp243-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp246-.Ltmp243       #   Call between .Ltmp243 and .Ltmp246
	.long	.Ltmp247-.Lfunc_begin10 #     jumps to .Ltmp247
	.byte	0                       #   On action: cleanup
	.long	.Ltmp248-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp249-.Ltmp248       #   Call between .Ltmp248 and .Ltmp249
	.long	.Ltmp250-.Lfunc_begin10 #     jumps to .Ltmp250
	.byte	0                       #   On action: cleanup
	.long	.Ltmp251-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp254-.Ltmp251       #   Call between .Ltmp251 and .Ltmp254
	.long	.Ltmp255-.Lfunc_begin10 #     jumps to .Ltmp255
	.byte	0                       #   On action: cleanup
	.long	.Ltmp256-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp263-.Ltmp256       #   Call between .Ltmp256 and .Ltmp263
	.long	.Ltmp264-.Lfunc_begin10 #     jumps to .Ltmp264
	.byte	0                       #   On action: cleanup
	.long	.Ltmp267-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp268-.Ltmp267       #   Call between .Ltmp267 and .Ltmp268
	.long	.Ltmp269-.Lfunc_begin10 #     jumps to .Ltmp269
	.byte	0                       #   On action: cleanup
	.long	.Ltmp268-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp265-.Ltmp268       #   Call between .Ltmp268 and .Ltmp265
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp271-.Ltmp265       #   Call between .Ltmp265 and .Ltmp271
	.long	.Ltmp272-.Lfunc_begin10 #     jumps to .Ltmp272
	.byte	1                       #   On action: 1
	.long	.Ltmp271-.Lfunc_begin10 # >> Call Site 9 <<
	.long	.Lfunc_end34-.Ltmp271   #   Call between .Ltmp271 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv,@function
_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv: # @_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi185:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi186:
	.cfi_def_cfa_offset 32
.Lcfi187:
	.cfi_offset %rbx, -24
.Lcfi188:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str.7, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movq	40(%r14), %rdx
	movq	240(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp273:
	movq	%r14, %rsi
	callq	*16(%rax)
.Ltmp274:
# BB#1:
	movl	252(%r14), %r9d
	testl	%r9d, %r9d
	jle	.LBB35_18
# BB#2:                                 # %.lr.ph
	movq	264(%r14), %r8
	xorl	%r10d, %r10d
	jmp	.LBB35_3
.LBB35_5:                               #   in Loop: Header=BB35_3 Depth=1
	movl	228(%rcx), %edx
	cmpl	$5, %edx
	je	.LBB35_7
# BB#6:                                 #   in Loop: Header=BB35_3 Depth=1
	cmpl	$2, %edx
	jne	.LBB35_9
.LBB35_7:                               #   in Loop: Header=BB35_3 Depth=1
	movl	228(%rax), %edx
	cmpl	$2, %edx
	je	.LBB35_17
# BB#8:                                 #   in Loop: Header=BB35_3 Depth=1
	cmpl	$5, %edx
	je	.LBB35_17
.LBB35_9:                               #   in Loop: Header=BB35_3 Depth=1
	movq	240(%r14), %rdx
	movl	220(%rcx), %ecx
	movl	220(%rax), %r11d
	movslq	%r11d, %rdi
	movq	24(%rdx), %rsi
	movslq	%ecx, %rax
	movl	(%rsi,%rax,8), %edx
	cmpl	%eax, %edx
	je	.LBB35_12
# BB#10:                                # %.lr.ph.i.i.preheader
                                        #   in Loop: Header=BB35_3 Depth=1
	leaq	(%rsi,%rax,8), %rbx
	.p2align	4, 0x90
.LBB35_11:                              # %.lr.ph.i.i
                                        #   Parent Loop BB35_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rcx
	movl	(%rsi,%rcx,8), %ecx
	movl	%ecx, (%rbx)
	movl	(%rsi,%rax,8), %ecx
	movslq	%ecx, %rax
	leaq	(%rsi,%rax,8), %rbx
	movl	(%rsi,%rax,8), %edx
	cmpl	%edx, %ecx
	jne	.LBB35_11
.LBB35_12:                              # %_ZN11btUnionFind4findEi.exit.i
                                        #   in Loop: Header=BB35_3 Depth=1
	movl	(%rsi,%rdi,8), %eax
	cmpl	%edi, %eax
	je	.LBB35_15
# BB#13:                                # %.lr.ph.i11.i.preheader
                                        #   in Loop: Header=BB35_3 Depth=1
	leaq	(%rsi,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB35_14:                              # %.lr.ph.i11.i
                                        #   Parent Loop BB35_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cltq
	movl	(%rsi,%rax,8), %eax
	movl	%eax, (%rdx)
	movl	(%rsi,%rdi,8), %r11d
	movslq	%r11d, %rdi
	leaq	(%rsi,%rdi,8), %rdx
	movl	(%rsi,%rdi,8), %eax
	cmpl	%eax, %r11d
	jne	.LBB35_14
.LBB35_15:                              # %_ZN11btUnionFind4findEi.exit13.i
                                        #   in Loop: Header=BB35_3 Depth=1
	cmpl	%r11d, %ecx
	je	.LBB35_17
# BB#16:                                #   in Loop: Header=BB35_3 Depth=1
	movslq	%ecx, %rax
	movl	%r11d, (%rsi,%rax,8)
	movl	4(%rsi,%rax,8), %eax
	movslq	%r11d, %rcx
	addl	%eax, 4(%rsi,%rcx,8)
	jmp	.LBB35_17
	.p2align	4, 0x90
.LBB35_3:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB35_11 Depth 2
                                        #     Child Loop BB35_14 Depth 2
	movq	(%r8,%r10,8), %rax
	movq	24(%rax), %rcx
	testb	$3, 216(%rcx)
	jne	.LBB35_17
# BB#4:                                 #   in Loop: Header=BB35_3 Depth=1
	movq	32(%rax), %rax
	testb	$3, 216(%rax)
	je	.LBB35_5
.LBB35_17:                              # %_ZN11btUnionFind5uniteEii.exit
                                        #   in Loop: Header=BB35_3 Depth=1
	incq	%r10
	cmpq	%r9, %r10
	jne	.LBB35_3
.LBB35_18:                              # %._crit_edge
	movq	240(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp275:
	movq	%r14, %rsi
	callq	*24(%rax)
.Ltmp276:
# BB#19:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB35_20:
.Ltmp277:
	movq	%rax, %rbx
.Ltmp278:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp279:
# BB#21:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB35_22:
.Ltmp280:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv, .Lfunc_end35-_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin11-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp273-.Lfunc_begin11 #   Call between .Lfunc_begin11 and .Ltmp273
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp273-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp276-.Ltmp273       #   Call between .Ltmp273 and .Ltmp276
	.long	.Ltmp277-.Lfunc_begin11 #     jumps to .Ltmp277
	.byte	0                       #   On action: cleanup
	.long	.Ltmp276-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp278-.Ltmp276       #   Call between .Ltmp276 and .Ltmp278
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp278-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp279-.Ltmp278       #   Call between .Ltmp278 and .Ltmp279
	.long	.Ltmp280-.Lfunc_begin11 #     jumps to .Ltmp280
	.byte	1                       #   On action: 1
	.long	.Ltmp279-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Lfunc_end35-.Ltmp279   #   Call between .Ltmp279 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI36_0:
	.long	1065353216              # float 1
.LCPI36_1:
	.long	0                       # float 0
	.text
	.globl	_ZN23btDiscreteDynamicsWorld19integrateTransformsEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld19integrateTransformsEf,@function
_ZN23btDiscreteDynamicsWorld19integrateTransformsEf: # @_ZN23btDiscreteDynamicsWorld19integrateTransformsEf
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%rbp
.Lcfi189:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi190:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi191:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi192:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi193:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi194:
	.cfi_def_cfa_offset 56
	subq	$264, %rsp              # imm = 0x108
.Lcfi195:
	.cfi_def_cfa_offset 320
.Lcfi196:
	.cfi_offset %rbx, -56
.Lcfi197:
	.cfi_offset %r12, -48
.Lcfi198:
	.cfi_offset %r13, -40
.Lcfi199:
	.cfi_offset %r14, -32
.Lcfi200:
	.cfi_offset %r15, -24
.Lcfi201:
	.cfi_offset %rbp, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %r13
	movl	$.L.str.8, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movl	284(%r13), %eax
	testl	%eax, %eax
	jle	.LBB36_21
# BB#1:                                 # %.lr.ph
	leaq	248(%rsp), %r14
	xorl	%r12d, %r12d
	leaq	200(%rsp), %rbp
	.p2align	4, 0x90
.LBB36_2:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r13), %rcx
	movq	(%rcx,%r12,8), %rbx
	movl	$1065353216, 260(%rbx)  # imm = 0x3F800000
	movl	228(%rbx), %ecx
	cmpl	$2, %ecx
	je	.LBB36_20
# BB#3:                                 #   in Loop: Header=BB36_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB36_20
# BB#4:                                 #   in Loop: Header=BB36_2 Depth=1
	testb	$3, 216(%rbx)
	jne	.LBB36_20
# BB#5:                                 #   in Loop: Header=BB36_2 Depth=1
.Ltmp281:
	movq	%rbx, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%rbp, %rsi
	callq	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
.Ltmp282:
# BB#6:                                 #   in Loop: Header=BB36_2 Depth=1
	movss	268(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	ucomiss	.LCPI36_1, %xmm0
	jne	.LBB36_7
	jnp	.LBB36_18
.LBB36_7:                               #   in Loop: Header=BB36_2 Depth=1
	movss	248(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	252(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	subss	56(%rbx), %xmm1
	subss	60(%rbx), %xmm2
	movss	256(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	subss	64(%rbx), %xmm3
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	ucomiss	%xmm0, %xmm3
	jbe	.LBB36_18
# BB#8:                                 #   in Loop: Header=BB36_2 Depth=1
.Ltmp284:
	movl	$.L.str.9, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
.Ltmp285:
# BB#9:                                 #   in Loop: Header=BB36_2 Depth=1
	movq	200(%rbx), %rax
	cmpl	$19, 8(%rax)
	jg	.LBB36_17
# BB#10:                                #   in Loop: Header=BB36_2 Depth=1
	incl	gNumClampedCcdMotions(%rip)
	movq	112(%r13), %rdi
	movq	(%rdi), %rax
.Ltmp286:
	callq	*64(%rax)
.Ltmp287:
# BB#11:                                #   in Loop: Header=BB36_2 Depth=1
	leaq	56(%rbx), %rcx
	movq	40(%r13), %rdx
	movl	$1065353216, 88(%rsp)   # imm = 0x3F800000
	movl	$-65535, 92(%rsp)       # imm = 0xFFFF0001
	movups	(%rcx), %xmm0
	leaq	96(%rsp), %rcx
	movups	%xmm0, (%rcx)
	movq	%r14, %r15
	movups	(%r14), %xmm0
	movups	%xmm0, 16(%rcx)
	movq	$0, 160(%rsp)
	movq	$_ZTV34btClosestNotMeConvexResultCallback+16, 80(%rsp)
	movq	%rbx, 168(%rsp)
	movl	$0, 176(%rsp)
	movq	%rax, 184(%rsp)
	movq	%rdx, 192(%rsp)
	movl	264(%rbx), %ebp
.Ltmp289:
	leaq	16(%rsp), %r14
	movq	%r14, %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
.Ltmp290:
# BB#12:                                #   in Loop: Header=BB36_2 Depth=1
	leaq	8(%rbx), %rdx
	movq	$_ZTV13btSphereShape+16, 16(%rsp)
	movl	$8, 24(%rsp)
	movl	%ebp, 56(%rsp)
	movl	%ebp, 72(%rsp)
	movq	192(%rbx), %rax
	movzwl	8(%rax), %ecx
	movw	%cx, 92(%rsp)
	movzwl	10(%rax), %eax
	movw	%ax, 94(%rsp)
.Ltmp292:
	xorps	%xmm0, %xmm0
	movq	%r13, %rdi
	movq	%r14, %rsi
	leaq	200(%rsp), %rbp
	movq	%rbp, %rcx
	leaq	80(%rsp), %r8
	callq	_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf
.Ltmp293:
	movq	%r15, %r14
# BB#13:                                #   in Loop: Header=BB36_2 Depth=1
	movss	88(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI36_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB36_16
# BB#14:                                #   in Loop: Header=BB36_2 Depth=1
	movss	%xmm0, 260(%rbx)
	mulss	12(%rsp), %xmm0         # 4-byte Folded Reload
.Ltmp294:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
.Ltmp295:
# BB#15:                                #   in Loop: Header=BB36_2 Depth=1
	movl	$0, 260(%rbx)
.LBB36_16:                              #   in Loop: Header=BB36_2 Depth=1
.Ltmp299:
	leaq	16(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp300:
.LBB36_17:                              #   in Loop: Header=BB36_2 Depth=1
.Ltmp304:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp305:
.LBB36_18:                              # %_ZN14CProfileSampleD2Ev.exit45
                                        #   in Loop: Header=BB36_2 Depth=1
.Ltmp307:
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	_ZN11btRigidBody18proceedToTransformERK11btTransform
.Ltmp308:
# BB#19:                                # %_ZN14CProfileSampleD2Ev.exit45._crit_edge
                                        #   in Loop: Header=BB36_2 Depth=1
	movl	284(%r13), %eax
	.p2align	4, 0x90
.LBB36_20:                              #   in Loop: Header=BB36_2 Depth=1
	incq	%r12
	movslq	%eax, %rcx
	cmpq	%rcx, %r12
	jl	.LBB36_2
.LBB36_21:                              # %._crit_edge
	callq	_ZN15CProfileManager12Stop_ProfileEv
	addq	$264, %rsp              # imm = 0x108
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB36_25:
.Ltmp301:
	jmp	.LBB36_28
.LBB36_23:
.Ltmp291:
	jmp	.LBB36_28
.LBB36_27:
.Ltmp288:
.LBB36_28:
	movq	%rax, %rbx
	jmp	.LBB36_29
.LBB36_24:
.Ltmp296:
	movq	%rax, %rbx
.Ltmp297:
	leaq	16(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp298:
.LBB36_29:
.Ltmp302:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp303:
	jmp	.LBB36_32
.LBB36_26:
.Ltmp306:
	jmp	.LBB36_31
.LBB36_30:
.Ltmp283:
	jmp	.LBB36_31
.LBB36_22:
.Ltmp309:
.LBB36_31:
	movq	%rax, %rbx
.LBB36_32:
.Ltmp310:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp311:
# BB#33:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB36_34:
.Ltmp312:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end36:
	.size	_ZN23btDiscreteDynamicsWorld19integrateTransformsEf, .Lfunc_end36-_ZN23btDiscreteDynamicsWorld19integrateTransformsEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\245\201\200\200"      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\234\001"              # Call site table length
	.long	.Lfunc_begin12-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp281-.Lfunc_begin12 #   Call between .Lfunc_begin12 and .Ltmp281
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp281-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp282-.Ltmp281       #   Call between .Ltmp281 and .Ltmp282
	.long	.Ltmp283-.Lfunc_begin12 #     jumps to .Ltmp283
	.byte	0                       #   On action: cleanup
	.long	.Ltmp284-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp285-.Ltmp284       #   Call between .Ltmp284 and .Ltmp285
	.long	.Ltmp309-.Lfunc_begin12 #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp286-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp287-.Ltmp286       #   Call between .Ltmp286 and .Ltmp287
	.long	.Ltmp288-.Lfunc_begin12 #     jumps to .Ltmp288
	.byte	0                       #   On action: cleanup
	.long	.Ltmp289-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp290-.Ltmp289       #   Call between .Ltmp289 and .Ltmp290
	.long	.Ltmp291-.Lfunc_begin12 #     jumps to .Ltmp291
	.byte	0                       #   On action: cleanup
	.long	.Ltmp292-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp295-.Ltmp292       #   Call between .Ltmp292 and .Ltmp295
	.long	.Ltmp296-.Lfunc_begin12 #     jumps to .Ltmp296
	.byte	0                       #   On action: cleanup
	.long	.Ltmp299-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp300-.Ltmp299       #   Call between .Ltmp299 and .Ltmp300
	.long	.Ltmp301-.Lfunc_begin12 #     jumps to .Ltmp301
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin12 #     jumps to .Ltmp306
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin12 #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp308-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp297-.Ltmp308       #   Call between .Ltmp308 and .Ltmp297
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp297-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp311-.Ltmp297       #   Call between .Ltmp297 and .Ltmp311
	.long	.Ltmp312-.Lfunc_begin12 #     jumps to .Ltmp312
	.byte	1                       #   On action: 1
	.long	.Ltmp311-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Lfunc_end36-.Ltmp311   #   Call between .Ltmp311 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN16btCollisionWorld20ConvexResultCallbackD2Ev,"axG",@progbits,_ZN16btCollisionWorld20ConvexResultCallbackD2Ev,comdat
	.weak	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev,@function
_ZN16btCollisionWorld20ConvexResultCallbackD2Ev: # @_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end37:
	.size	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev, .Lfunc_end37-_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.cfi_endproc

	.text
	.globl	_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf,@function
_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf: # @_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r15
.Lcfi202:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi203:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi204:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi205:
	.cfi_def_cfa_offset 48
.Lcfi206:
	.cfi_offset %rbx, -32
.Lcfi207:
	.cfi_offset %r14, -24
.Lcfi208:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %r14
	movl	$.L.str.10, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movl	284(%r14), %eax
	testl	%eax, %eax
	jle	.LBB38_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	movq	296(%r14), %rcx
	movq	(%rcx,%rbx,8), %r15
	testb	$3, 216(%r15)
	jne	.LBB38_7
# BB#3:                                 #   in Loop: Header=BB38_2 Depth=1
.Ltmp313:
	movq	%r15, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody19integrateVelocitiesEf
.Ltmp314:
# BB#4:                                 #   in Loop: Header=BB38_2 Depth=1
.Ltmp315:
	movq	%r15, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody12applyDampingEf
.Ltmp316:
# BB#5:                                 #   in Loop: Header=BB38_2 Depth=1
.Ltmp317:
	leaq	72(%r15), %rsi
	movq	%r15, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
.Ltmp318:
# BB#6:                                 # %._crit_edge19
                                        #   in Loop: Header=BB38_2 Depth=1
	movl	284(%r14), %eax
.LBB38_7:                               #   in Loop: Header=BB38_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB38_2
.LBB38_8:                               # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB38_9:
.Ltmp319:
	movq	%rax, %rbx
.Ltmp320:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp321:
# BB#10:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB38_11:
.Ltmp322:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end38:
	.size	_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf, .Lfunc_end38-_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin13-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp313-.Lfunc_begin13 #   Call between .Lfunc_begin13 and .Ltmp313
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp313-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp318-.Ltmp313       #   Call between .Ltmp313 and .Ltmp318
	.long	.Ltmp319-.Lfunc_begin13 #     jumps to .Ltmp319
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp320-.Ltmp318       #   Call between .Ltmp318 and .Ltmp320
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp320-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp321-.Ltmp320       #   Call between .Ltmp320 and .Ltmp321
	.long	.Ltmp322-.Lfunc_begin13 #     jumps to .Ltmp322
	.byte	1                       #   On action: 1
	.long	.Ltmp321-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Lfunc_end38-.Ltmp321   #   Call between .Ltmp321 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3,@function
_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3: # @_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi209:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi210:
	.cfi_def_cfa_offset 24
	subq	$248, %rsp
.Lcfi211:
	.cfi_def_cfa_offset 272
.Lcfi212:
	.cfi_offset %rbx, -24
.Lcfi213:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rdi, %rbx
	movss	32(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm11
	mulss	%xmm0, %xmm11
	movss	36(%rsi), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm8
	movss	40(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm9
	movaps	%xmm4, %xmm7
	mulss	%xmm0, %xmm10
	mulss	%xmm0, %xmm7
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	16(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm3, %xmm4
	movss	24(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm6    # xmm6 = xmm6[0],xmm2[0],xmm6[1],xmm2[1]
	movaps	%xmm6, %xmm2
	mulps	%xmm0, %xmm3
	mulps	%xmm0, %xmm6
	mulps	%xmm5, %xmm0
	xorps	%xmm12, %xmm12
	mulps	%xmm12, %xmm4
	addps	%xmm4, %xmm0
	mulps	%xmm12, %xmm2
	addps	%xmm2, %xmm0
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	mulps	%xmm12, %xmm5
	addps	%xmm5, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	xorps	%xmm0, %xmm0
	mulss	%xmm0, %xmm8
	mulss	%xmm0, %xmm9
	mulss	%xmm0, %xmm1
	addss	%xmm8, %xmm11
	addss	%xmm9, %xmm11
	movss	%xmm11, 80(%rsp)        # 4-byte Spill
	addss	%xmm1, %xmm10
	addss	%xmm9, %xmm10
	movss	%xmm10, 96(%rsp)        # 4-byte Spill
	addps	%xmm4, %xmm5
	addps	%xmm5, %xmm6
	movaps	%xmm6, 224(%rsp)        # 16-byte Spill
	addss	%xmm8, %xmm1
	movsd	48(%rsi), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	addss	%xmm1, %xmm7
	movss	%xmm7, 44(%rsp)         # 4-byte Spill
	movss	56(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	movq	(%rbx), %rax
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm2
	subps	48(%rsp), %xmm2         # 16-byte Folded Reload
	movaps	%xmm2, 160(%rsp)        # 16-byte Spill
	movss	40(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	subss	80(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	movlps	%xmm2, 24(%rsp)
	movlps	%xmm3, 32(%rsp)
	movaps	%xmm1, %xmm2
	addps	112(%rsp), %xmm2        # 16-byte Folded Reload
	movaps	%xmm2, 192(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm0
	addss	96(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movaps	%xmm1, 208(%rsp)        # 16-byte Spill
	movlps	%xmm2, 8(%rsp)
	movlps	%xmm1, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	addps	64(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	80(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	addss	40(%rsp), %xmm2         # 4-byte Folded Reload
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	movlps	%xmm0, 8(%rsp)
	movlps	%xmm1, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	subps	112(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	subss	96(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movlps	%xmm1, 8(%rsp)
	movlps	%xmm2, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	addps	224(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	movss	40(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	44(%rsp), %xmm0         # 4-byte Folded Reload
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movaps	%xmm2, 128(%rsp)        # 16-byte Spill
	movlps	%xmm1, 8(%rsp)
	movlps	%xmm2, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	subps	224(%rsp), %xmm1        # 16-byte Folded Reload
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	40(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	subss	44(%rsp), %xmm2         # 4-byte Folded Reload
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movlps	%xmm1, 8(%rsp)
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %r8
	movaps	192(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%r8
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*32(%rax)
	movq	(%rax), %rcx
	movq	40(%rcx), %rbx
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 24(%rsp)
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 32(%rsp)
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movlps	%xmm0, 8(%rsp)
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movlps	%xmm0, 16(%rsp)
	leaq	24(%rsp), %rsi
	leaq	8(%rsp), %rdx
	movq	%rax, %rdi
	movq	%r14, %rcx
	callq	*%rbx
	addq	$248, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end39:
	.size	_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3, .Lfunc_end39-_ZN23btDiscreteDynamicsWorld15debugDrawSphereEfRK11btTransformRK9btVector3
	.cfi_endproc

	.section	.text._ZN17DebugDrawcallbackD2Ev,"axG",@progbits,_ZN17DebugDrawcallbackD2Ev,comdat
	.weak	_ZN17DebugDrawcallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN17DebugDrawcallbackD2Ev,@function
_ZN17DebugDrawcallbackD2Ev:             # @_ZN17DebugDrawcallbackD2Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi214:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi215:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi216:
	.cfi_def_cfa_offset 32
.Lcfi217:
	.cfi_offset %rbx, -24
.Lcfi218:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %rdi
.Ltmp323:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp324:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN18btTriangleCallbackD2Ev # TAILCALL
.LBB40_2:
.Ltmp325:
	movq	%rax, %r14
.Ltmp326:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp327:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_4:
.Ltmp328:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN17DebugDrawcallbackD2Ev, .Lfunc_end40-_ZN17DebugDrawcallbackD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp323-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp324-.Ltmp323       #   Call between .Ltmp323 and .Ltmp324
	.long	.Ltmp325-.Lfunc_begin14 #     jumps to .Ltmp325
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Ltmp326-.Ltmp324       #   Call between .Ltmp324 and .Ltmp326
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin14 # >> Call Site 3 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin14 #     jumps to .Ltmp328
	.byte	1                       #   On action: 1
	.long	.Ltmp327-.Lfunc_begin14 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp327   #   Call between .Ltmp327 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12btIDebugDraw13drawTransformERK11btTransformf,"axG",@progbits,_ZN12btIDebugDraw13drawTransformERK11btTransformf,comdat
	.weak	_ZN12btIDebugDraw13drawTransformERK11btTransformf
	.p2align	4, 0x90
	.type	_ZN12btIDebugDraw13drawTransformERK11btTransformf,@function
_ZN12btIDebugDraw13drawTransformERK11btTransformf: # @_ZN12btIDebugDraw13drawTransformERK11btTransformf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi219:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi220:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 32
	subq	$80, %rsp
.Lcfi222:
	.cfi_def_cfa_offset 112
.Lcfi223:
	.cfi_offset %rbx, -32
.Lcfi224:
	.cfi_offset %r14, -24
.Lcfi225:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movups	48(%rbx), %xmm1
	movaps	%xmm1, 32(%rsp)
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movaps	%xmm0, %xmm4
	movaps	%xmm0, %xmm3
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm4, %xmm1
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	xorps	%xmm4, %xmm4
	mulps	%xmm4, %xmm2
	addps	%xmm1, %xmm2
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm4, %xmm1
	addps	%xmm2, %xmm1
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	xorps	%xmm3, %xmm3
	movss	36(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	addps	32(%rsp), %xmm1
	addss	40(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movq	$1060320051, (%rsp)     # imm = 0x3F333333
	movq	$0, 8(%rsp)
	leaq	32(%rsp), %r15
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%r15, %rsi
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	xorps	%xmm3, %xmm3
	mulps	%xmm3, %xmm1
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	64(%rsp), %xmm2         # 16-byte Folded Reload
	addps	%xmm1, %xmm2
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	%xmm3, %xmm1
	addps	%xmm2, %xmm1
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm3, %xmm3
	mulss	%xmm3, %xmm0
	movss	36(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	48(%rsp), %xmm2         # 16-byte Folded Reload
	addss	%xmm0, %xmm2
	movss	40(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	addps	32(%rsp), %xmm1
	addss	40(%rsp), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 16(%rsp)
	movlps	%xmm2, 24(%rsp)
	movabsq	$4554039942338052096, %rcx # imm = 0x3F33333300000000
	movq	%rcx, (%rsp)
	movq	$0, 8(%rsp)
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*%rax
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movss	16(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	xorps	%xmm3, %xmm3
	mulps	%xmm3, %xmm1
	movss	20(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	mulps	%xmm3, %xmm2
	xorps	%xmm3, %xmm3
	addps	%xmm1, %xmm2
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0],xmm1[1],xmm0[1]
	mulps	64(%rsp), %xmm1         # 16-byte Folded Reload
	addps	%xmm2, %xmm1
	movss	32(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm2
	mulss	%xmm2, %xmm0
	mulss	36(%rbx), %xmm2
	addss	%xmm0, %xmm2
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	mulss	40(%rbx), %xmm0
	addss	%xmm2, %xmm0
	addps	32(%rsp), %xmm1
	addss	40(%rsp), %xmm0
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm1, 16(%rsp)
	movlps	%xmm3, 24(%rsp)
	movq	$0, (%rsp)
	movq	$1060320051, 8(%rsp)    # imm = 0x3F333333
	leaq	16(%rsp), %rdx
	movq	%rsp, %rcx
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	*%rax
	addq	$80, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end41:
	.size	_ZN12btIDebugDraw13drawTransformERK11btTransformf, .Lfunc_end41-_ZN12btIDebugDraw13drawTransformERK11btTransformf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI42_0:
	.long	1016003125              # float 0.0174532924
	.section	.text._ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf,"axG",@progbits,_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf,comdat
	.weak	_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf
	.p2align	4, 0x90
	.type	_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf,@function
_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf: # @_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi226:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi227:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi228:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi229:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi230:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi231:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi232:
	.cfi_def_cfa_offset 240
.Lcfi233:
	.cfi_offset %rbx, -56
.Lcfi234:
	.cfi_offset %r12, -48
.Lcfi235:
	.cfi_offset %r13, -40
.Lcfi236:
	.cfi_offset %r14, -32
.Lcfi237:
	.cfi_offset %r15, -24
.Lcfi238:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movq	%r8, %r15
	movaps	%xmm1, 96(%rsp)         # 16-byte Spill
	movq	%rcx, %rbp
	movq	%rsi, %rbx
	movq	%rdi, %r12
	movss	8(%rbp), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movsd	4(%rdx), %xmm6          # xmm6 = mem[0],zero
	movsd	(%rbp), %xmm5           # xmm5 = mem[0],zero
	movaps	%xmm7, %xmm1
	movaps	%xmm7, %xmm10
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	mulps	%xmm6, %xmm1
	movaps	%xmm1, %xmm9
	movss	(%rdx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	pshufd	$229, %xmm6, %xmm8      # xmm8 = xmm6[1,1,2,3]
	pshufd	$229, %xmm5, %xmm7      # xmm7 = xmm5[1,1,2,3]
	movaps	%xmm5, %xmm11
	movaps	%xmm10, %xmm5
	shufps	$0, %xmm7, %xmm5        # xmm5 = xmm5[0,0],xmm7[0,0]
	shufps	$226, %xmm7, %xmm5      # xmm5 = xmm5[2,0],xmm7[2,3]
	mulss	%xmm1, %xmm7
	shufps	$0, %xmm8, %xmm1        # xmm1 = xmm1[0,0],xmm8[0,0]
	shufps	$226, %xmm8, %xmm1      # xmm1 = xmm1[2,0],xmm8[2,3]
	mulps	%xmm1, %xmm5
	subps	%xmm5, %xmm9
	movaps	%xmm9, 112(%rsp)        # 16-byte Spill
	mulss	%xmm11, %xmm6
	subss	%xmm6, %xmm7
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	mulss	.LCPI42_0(%rip), %xmm4
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	subss	%xmm2, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm1
	divss	%xmm4, %xmm1
	cvttss2si	%xmm1, %eax
	testl	%eax, %eax
	movl	$1, %r14d
	cmovnel	%eax, %r14d
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	mulps	%xmm1, %xmm11
	movaps	%xmm11, 16(%rsp)        # 16-byte Spill
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm10
	movaps	%xmm10, 32(%rsp)        # 16-byte Spill
	movaps	%xmm2, %xmm0
	callq	cosf
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	addps	%xmm0, %xmm2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	addss	8(%rbx), %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	112(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	96(%rsp), %xmm0         # 16-byte Folded Reload
	addps	16(%rsp), %xmm0         # 16-byte Folded Reload
	addss	32(%rsp), %xmm1         # 16-byte Folded Reload
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 64(%rsp)
	movlps	%xmm2, 72(%rsp)
	movl	%r13d, 56(%rsp)         # 4-byte Spill
	testb	%r13b, %r13b
	je	.LBB42_2
# BB#1:
	movq	(%r12), %rax
	leaq	64(%rsp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rcx
	callq	*40(%rax)
.LBB42_2:                               # %.preheader
	testl	%r14d, %r14d
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	movaps	144(%rsp), %xmm3        # 16-byte Reload
	jle	.LBB42_5
# BB#3:                                 # %.lr.ph
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%r14d, %xmm4
	movss	%xmm4, 60(%rsp)         # 4-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB42_4:                               # =>This Inner Loop Header: Depth=1
	incl	%r13d
	xorps	%xmm4, %xmm4
	cvtsi2ssl	%r13d, %xmm4
	mulss	%xmm0, %xmm4
	divss	60(%rsp), %xmm4         # 4-byte Folded Reload
	addss	%xmm1, %xmm4
	movaps	%xmm4, %xmm0
	movss	%xmm0, 112(%rsp)        # 4-byte Spill
	movsd	(%rbp), %xmm1           # xmm1 = mem[0],zero
	mulps	%xmm3, %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movss	8(%rbp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
	callq	cosf
	movss	32(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	addps	%xmm0, %xmm2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	addss	8(%rbx), %xmm1
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
	movss	112(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	96(%rsp), %xmm0         # 16-byte Folded Reload
	addps	16(%rsp), %xmm0         # 16-byte Folded Reload
	addss	32(%rsp), %xmm1         # 4-byte Folded Reload
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm0, 128(%rsp)
	movlps	%xmm2, 136(%rsp)
	movq	(%r12), %rax
	movq	%r12, %rdi
	leaq	64(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movq	%r15, %rcx
	callq	*40(%rax)
	movaps	144(%rsp), %xmm3        # 16-byte Reload
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	movss	8(%rsp), %xmm1          # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movups	128(%rsp), %xmm0
	movaps	%xmm0, 64(%rsp)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cmpl	%r14d, %r13d
	jl	.LBB42_4
.LBB42_5:                               # %._crit_edge
	cmpb	$0, 56(%rsp)            # 1-byte Folded Reload
	je	.LBB42_7
# BB#6:
	movq	(%r12), %rax
	leaq	64(%rsp), %rdx
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r15, %rcx
	callq	*40(%rax)
.LBB42_7:
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end42:
	.size	_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf, .Lfunc_end42-_ZN12btIDebugDraw7drawArcERK9btVector3S2_S2_ffffS2_bf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI43_0:
	.long	1016003125              # float 0.0174532924
.LCPI43_1:
	.long	3217625051              # float -1.57079637
.LCPI43_2:
	.long	1070141403              # float 1.57079637
.LCPI43_3:
	.long	1086918619              # float 6.28318548
.LCPI43_4:
	.long	3226013659              # float -3.14159274
.LCPI43_5:
	.long	1078530011              # float 3.14159274
	.section	.text._ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f,"axG",@progbits,_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f,comdat
	.weak	_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f
	.p2align	4, 0x90
	.type	_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f,@function
_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f: # @_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi239:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi240:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi241:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi242:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi243:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 56
	subq	$2648, %rsp             # imm = 0xA58
.Lcfi245:
	.cfi_def_cfa_offset 2704
.Lcfi246:
	.cfi_offset %rbx, -56
.Lcfi247:
	.cfi_offset %r12, -48
.Lcfi248:
	.cfi_offset %r13, -40
.Lcfi249:
	.cfi_offset %r14, -32
.Lcfi250:
	.cfi_offset %r15, -24
.Lcfi251:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movq	%rdi, %rbx
	movss	(%rdx), %xmm10          # xmm10 = mem[0],zero,zero,zero
	movq	%rdx, 112(%rsp)         # 8-byte Spill
	movsd	4(%rdx), %xmm9          # xmm9 = mem[0],zero
	movaps	%xmm0, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movaps	%xmm10, %xmm6
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	mulps	%xmm7, %xmm6
	pshufd	$229, %xmm9, %xmm8      # xmm8 = xmm9[1,1,2,3]
	movdqa	%xmm8, %xmm12
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm12
	movsd	(%rsi), %xmm11          # xmm11 = mem[0],zero
	movq	%rsi, 72(%rsp)          # 8-byte Spill
	movss	8(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm7
	addss	%xmm13, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movaps	%xmm6, %xmm7
	addps	%xmm11, %xmm7
	movlps	%xmm7, 176(%rsp)
	xorps	%xmm7, %xmm7
	movlps	%xmm0, 184(%rsp)
	subps	%xmm6, %xmm11
	subss	%xmm12, %xmm13
	movss	%xmm13, %xmm7           # xmm7 = xmm13[0],xmm7[1,2,3]
	movlps	%xmm11, 160(%rsp)
	movlps	%xmm7, 168(%rsp)
	mulss	.LCPI43_0(%rip), %xmm5
	movss	(%rcx), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movss	.LCPI43_1(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	addss	%xmm7, %xmm6
	ucomiss	%xmm1, %xmm7
	setae	7(%rsp)                 # 1-byte Folded Spill
	movaps	%xmm1, %xmm0
	cmpnless	%xmm7, %xmm0
	movaps	%xmm0, %xmm7
	andnps	%xmm6, %xmm7
	andps	%xmm1, %xmm0
	orps	%xmm7, %xmm0
	movss	.LCPI43_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm7
	ucomiss	%xmm1, %xmm2
	cmpnless	%xmm2, %xmm1
	movaps	%xmm1, %xmm13
	andps	%xmm2, %xmm1
	movsd	4(%rcx), %xmm14         # xmm14 = mem[0],zero
	subss	%xmm5, %xmm7
	andnps	%xmm7, %xmm13
	orps	%xmm13, %xmm1
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movss	4(%rcx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	movss	%xmm2, 20(%rsp)         # 4-byte Spill
	setae	8(%rsp)                 # 1-byte Folded Spill
	ucomiss	%xmm1, %xmm0
	jbe	.LBB43_1
# BB#2:
	movb	$1, 8(%rsp)             # 1-byte Folded Spill
	movb	$1, 7(%rsp)             # 1-byte Folded Spill
	jmp	.LBB43_3
.LBB43_1:
	movaps	%xmm1, %xmm7
	movaps	%xmm0, %xmm6
.LBB43_3:
	subss	%xmm6, %xmm7
	movaps	%xmm7, %xmm0
	divss	%xmm5, %xmm0
	cvttss2si	%xmm0, %ecx
	incl	%ecx
	cmpl	$1, %ecx
	movl	$2, %eax
	cmovlel	%eax, %ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movaps	%xmm4, %xmm0
	subss	%xmm3, %xmm0
	ucomiss	.LCPI43_3(%rip), %xmm0
	setae	%cl
	ucomiss	%xmm4, %xmm3
	seta	9(%rsp)                 # 1-byte Folded Spill
	jbe	.LBB43_5
# BB#4:
	movss	.LCPI43_4(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm3
	movss	.LCPI43_5(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
.LBB43_5:
	subss	%xmm3, %xmm4
	movaps	%xmm4, %xmm0
	divss	%xmm5, %xmm0
	cvttss2si	%xmm0, %edx
	incl	%edx
	cmpl	$1, %edx
	cmovgl	%edx, %eax
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	jle	.LBB43_11
# BB#6:                                 # %.lr.ph269
	shufps	$0, %xmm8, %xmm10       # xmm10 = xmm10[0,0],xmm8[0,0]
	unpcklps	%xmm12, %xmm11  # xmm11 = xmm11[0],xmm12[0],xmm11[1],xmm12[1]
	shufps	$226, %xmm8, %xmm10     # xmm10 = xmm10[2,0],xmm8[2,3]
	movq	24(%rsp), %rdx          # 8-byte Reload
	leal	-1(%rdx), %edx
	mulps	%xmm9, %xmm11
	mulps	%xmm14, %xmm10
	mulss	%xmm12, %xmm9
	movl	%edx, 36(%rsp)          # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	subps	%xmm10, %xmm11
	movaps	%xmm11, 224(%rsp)       # 16-byte Spill
	movss	20(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	subss	%xmm9, %xmm1
	movss	%xmm1, 20(%rsp)         # 4-byte Spill
	divss	%xmm0, %xmm7
	orb	%cl, 9(%rsp)            # 1-byte Folded Spill
	leal	-1(%rax), %edx
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%edx, %xmm0
	divss	%xmm0, %xmm4
	leaq	272(%rsp), %r14
	leaq	1456(%rsp), %rcx
	movl	%edx, %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	%eax, %eax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	xorl	%ebp, %ebp
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movaps	%xmm6, 208(%rsp)        # 16-byte Spill
	movaps	%xmm7, 192(%rsp)        # 16-byte Spill
	.p2align	4, 0x90
.LBB43_7:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB43_8 Depth 2
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	cmpb	$0, 7(%rsp)             # 1-byte Folded Reload
	sete	%r15b
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%ebp, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm6, %xmm0
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	callq	sinf
	mulss	128(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movss	80(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	128(%rsp), %xmm0        # 16-byte Folded Reload
	movss	%xmm0, 40(%rsp)         # 4-byte Spill
	cmpl	36(%rsp), %ebp          # 4-byte Folded Reload
	sete	%al
	setne	%cl
	orb	%r15b, %cl
	movb	%cl, 11(%rsp)           # 1-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	testl	%ebp, %ebp
	sete	%cl
	orb	%al, %cl
	movb	%cl, 10(%rsp)           # 1-byte Spill
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	%xmm0, 240(%rsp)        # 16-byte Spill
	xorl	%r15d, %r15d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB43_8:                               #   Parent Loop BB43_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%r12d, %xmm0
	mulss	%xmm4, %xmm0
	addss	%xmm3, %xmm0
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	callq	sinf
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movss	40(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movq	120(%rsp), %rax         # 8-byte Reload
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	8(%rax), %xmm0
	movq	72(%rsp), %rax          # 8-byte Reload
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	addps	%xmm2, %xmm1
	addss	8(%rax), %xmm0
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm3, %xmm4
	movss	20(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	224(%rsp), %xmm4        # 16-byte Folded Reload
	addps	%xmm1, %xmm4
	addss	%xmm0, %xmm2
	movq	112(%rsp), %rax         # 8-byte Reload
	movsd	(%rax), %xmm0           # xmm0 = mem[0],zero
	mulps	240(%rsp), %xmm0        # 16-byte Folded Reload
	movss	8(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	144(%rsp), %xmm1        # 16-byte Folded Reload
	addps	%xmm4, %xmm0
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	leaq	(%r14,%r15), %rbp
	movlps	%xmm0, (%r14,%r15)
	movlps	%xmm2, 8(%r14,%r15)
	cmpl	$0, 64(%rsp)            # 4-byte Folded Reload
	je	.LBB43_12
# BB#9:                                 #   in Loop: Header=BB43_8 Depth=2
	movq	(%rbx), %rax
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r15), %rsi
	movq	%rbx, %rdi
	jmp	.LBB43_14
	.p2align	4, 0x90
.LBB43_12:                              #   in Loop: Header=BB43_8 Depth=2
	cmpb	$0, 8(%rsp)             # 1-byte Folded Reload
	je	.LBB43_15
# BB#13:                                #   in Loop: Header=BB43_8 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	160(%rsp), %rsi
.LBB43_14:                              #   in Loop: Header=BB43_8 Depth=2
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*40(%rax)
.LBB43_15:                              #   in Loop: Header=BB43_8 Depth=2
	testq	%r15, %r15
	je	.LBB43_17
# BB#16:                                #   in Loop: Header=BB43_8 Depth=2
	movq	(%rbx), %rax
	leaq	-16(%r14,%r15), %rsi
	movq	%rbx, %rdi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*40(%rax)
	jmp	.LBB43_18
	.p2align	4, 0x90
.LBB43_17:                              #   in Loop: Header=BB43_8 Depth=2
	movups	(%r14,%r15), %xmm0
	movaps	%xmm0, 256(%rsp)
.LBB43_18:                              #   in Loop: Header=BB43_8 Depth=2
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	cmpb	$0, 11(%rsp)            # 1-byte Folded Reload
	jne	.LBB43_20
# BB#19:                                #   in Loop: Header=BB43_8 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	176(%rsp), %rsi
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*40(%rax)
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB43_20:                              #   in Loop: Header=BB43_8 Depth=2
	cmpb	$0, 9(%rsp)             # 1-byte Folded Reload
	je	.LBB43_23
# BB#21:                                #   in Loop: Header=BB43_8 Depth=2
	cmpq	%r12, 56(%rsp)          # 8-byte Folded Reload
	jne	.LBB43_28
# BB#22:                                #   in Loop: Header=BB43_8 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	leaq	256(%rsp), %rsi
	jmp	.LBB43_27
	.p2align	4, 0x90
.LBB43_23:                              #   in Loop: Header=BB43_8 Depth=2
	cmpb	$0, 10(%rsp)            # 1-byte Folded Reload
	je	.LBB43_28
# BB#24:                                #   in Loop: Header=BB43_8 Depth=2
	testq	%r15, %r15
	sete	%al
	cmpq	%r12, 56(%rsp)          # 8-byte Folded Reload
	je	.LBB43_26
# BB#25:                                #   in Loop: Header=BB43_8 Depth=2
	testb	%al, %al
	je	.LBB43_28
.LBB43_26:                              #   in Loop: Header=BB43_8 Depth=2
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	movq	72(%rsp), %rsi          # 8-byte Reload
.LBB43_27:                              #   in Loop: Header=BB43_8 Depth=2
	movq	%rbp, %rdx
	movq	%r13, %rcx
	callq	*40(%rax)
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB43_28:                              #   in Loop: Header=BB43_8 Depth=2
	incq	%r12
	addq	$16, %r15
	cmpq	104(%rsp), %r12         # 8-byte Folded Reload
	jl	.LBB43_8
# BB#10:                                # %._crit_edge
                                        #   in Loop: Header=BB43_7 Depth=1
	movq	64(%rsp), %rbp          # 8-byte Reload
	incl	%ebp
	cmpl	24(%rsp), %ebp          # 4-byte Folded Reload
	movq	%r14, %rcx
	movq	48(%rsp), %r14          # 8-byte Reload
	movaps	208(%rsp), %xmm6        # 16-byte Reload
	movaps	192(%rsp), %xmm7        # 16-byte Reload
	jl	.LBB43_7
.LBB43_11:                              # %._crit_edge270
	addq	$2648, %rsp             # imm = 0xA58
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end43:
	.size	_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f, .Lfunc_end43-_ZN12btIDebugDraw15drawSpherePatchERK9btVector3S2_S2_fffffS2_f
	.cfi_endproc

	.section	.text._ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_,"axG",@progbits,_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_,comdat
	.weak	_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_
	.p2align	4, 0x90
	.type	_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_,@function
_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_: # @_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi252:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi254:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi255:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi256:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi257:
	.cfi_def_cfa_offset 80
.Lcfi258:
	.cfi_offset %rbx, -48
.Lcfi259:
	.cfi_offset %r12, -40
.Lcfi260:
	.cfi_offset %r13, -32
.Lcfi261:
	.cfi_offset %r14, -24
.Lcfi262:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %rbx
	movq	%rdx, %r13
	movq	%rsi, %r12
	movq	%rdi, %r15
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm8    # xmm8 = xmm8[0],xmm6[0],xmm8[1],xmm6[1]
	mulps	%xmm8, %xmm5
	movaps	%xmm1, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	20(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm2    # xmm2 = xmm2[0],xmm7[0],xmm2[1],xmm7[1]
	mulps	%xmm6, %xmm2
	addps	%xmm2, %xmm5
	movaps	%xmm0, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	movss	32(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	mulss	36(%rbx), %xmm1
	addss	%xmm1, %xmm4
	mulss	40(%rbx), %xmm0
	addss	%xmm0, %xmm4
	movss	56(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r13), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	mulps	%xmm5, %xmm2
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	movaps	%xmm1, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	mulss	32(%rbx), %xmm8
	movss	36(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm8, %xmm4
	mulss	40(%rbx), %xmm1
	addss	%xmm1, %xmm4
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	4(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm8, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	20(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm2    # xmm2 = xmm2[0],xmm7[0],xmm2[1],xmm7[1]
	mulps	%xmm6, %xmm2
	addps	%xmm2, %xmm5
	movaps	%xmm8, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	movss	32(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	mulss	36(%rbx), %xmm1
	addss	%xmm1, %xmm4
	mulss	40(%rbx), %xmm8
	addss	%xmm8, %xmm4
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm8, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	4(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	mulps	%xmm5, %xmm2
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	movaps	%xmm8, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	mulss	32(%rbx), %xmm1
	movss	36(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	mulss	40(%rbx), %xmm8
	addss	%xmm8, %xmm4
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm8, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r12), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm4, %xmm6
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	20(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	mulps	%xmm4, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	movsd	48(%rbx), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm4
	mulss	32(%rbx), %xmm3
	mulss	36(%rbx), %xmm0
	addss	%xmm3, %xmm0
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movss	56(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	xorps	%xmm5, %xmm5
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm5, 24(%rsp)
	movss	8(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	addps	%xmm8, %xmm4
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm7, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r13), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm4, %xmm6
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	20(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	mulps	%xmm4, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	movsd	48(%rbx), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm4
	mulss	32(%rbx), %xmm3
	mulss	36(%rbx), %xmm0
	addss	%xmm3, %xmm0
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movss	56(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	xorps	%xmm5, %xmm5
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm5, 24(%rsp)
	movss	8(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	addps	%xmm8, %xmm4
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm7, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r13), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm4, %xmm6
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	20(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	mulps	%xmm4, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	movsd	48(%rbx), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm4
	mulss	32(%rbx), %xmm3
	mulss	36(%rbx), %xmm0
	addss	%xmm3, %xmm0
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movss	56(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	xorps	%xmm5, %xmm5
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm5, 24(%rsp)
	movss	8(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	addps	%xmm8, %xmm4
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm7, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%r12), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm4, %xmm6
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	20(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	mulps	%xmm4, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	24(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	movsd	48(%rbx), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm4
	mulss	32(%rbx), %xmm3
	mulss	36(%rbx), %xmm0
	addss	%xmm3, %xmm0
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movss	56(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm2
	xorps	%xmm5, %xmm5
	movss	%xmm2, %xmm5            # xmm5 = xmm2[0],xmm5[1,2,3]
	movlps	%xmm4, 16(%rsp)
	movlps	%xmm5, 24(%rsp)
	movss	8(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm1, %xmm4
	addps	%xmm8, %xmm4
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm7, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm4, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	20(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm2    # xmm2 = xmm2[0],xmm7[0],xmm2[1],xmm7[1]
	mulps	%xmm6, %xmm2
	addps	%xmm2, %xmm5
	movaps	%xmm8, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	movss	32(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	mulss	36(%rbx), %xmm1
	addss	%xmm1, %xmm4
	mulss	40(%rbx), %xmm8
	addss	%xmm8, %xmm4
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm8, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	(%r13), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	mulps	%xmm5, %xmm2
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	movaps	%xmm8, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	mulss	32(%rbx), %xmm1
	movss	36(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	mulss	40(%rbx), %xmm8
	addss	%xmm8, %xmm4
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	4(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm8, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r13), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm8          # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	20(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm2    # xmm2 = xmm2[0],xmm7[0],xmm2[1],xmm7[1]
	mulps	%xmm6, %xmm2
	addps	%xmm2, %xmm5
	movaps	%xmm8, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	movss	32(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	mulss	36(%rbx), %xmm1
	addss	%xmm1, %xmm4
	mulss	40(%rbx), %xmm8
	addss	%xmm8, %xmm4
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm8, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	movq	(%r15), %rax
	movq	40(%rax), %rax
	movss	(%r12), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	16(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm2    # xmm2 = xmm2[0],xmm6[0],xmm2[1],xmm6[1]
	mulps	%xmm5, %xmm2
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm3    # xmm3 = xmm3[0],xmm6[0],xmm3[1],xmm6[1]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	movaps	%xmm1, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm7, %xmm6
	addps	%xmm6, %xmm5
	movsd	48(%rbx), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm9, %xmm5
	mulss	32(%rbx), %xmm8
	movss	36(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm8, %xmm4
	mulss	40(%rbx), %xmm1
	addss	%xmm1, %xmm4
	movss	56(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	xorps	%xmm7, %xmm7
	movss	%xmm4, %xmm7            # xmm7 = xmm4[0],xmm7[1,2,3]
	movlps	%xmm5, 16(%rsp)
	movlps	%xmm7, 24(%rsp)
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm3, %xmm5
	addps	%xmm2, %xmm5
	addps	%xmm6, %xmm5
	addps	%xmm9, %xmm5
	mulss	%xmm10, %xmm4
	addss	%xmm8, %xmm4
	addss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movlps	%xmm5, (%rsp)
	movlps	%xmm0, 8(%rsp)
	leaq	16(%rsp), %rsi
	movq	%rsp, %rdx
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	*%rax
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end44:
	.size	_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_, .Lfunc_end44-_ZN12btIDebugDraw7drawBoxERK9btVector3S2_RK11btTransformS2_
	.cfi_endproc

	.text
	.globl	_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver,@function
_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver: # @_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi263:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi264:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi265:
	.cfi_def_cfa_offset 32
.Lcfi266:
	.cfi_offset %rbx, -24
.Lcfi267:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 333(%rbx)
	je	.LBB45_2
# BB#1:
	movq	232(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB45_2:                               # %._crit_edge
	movb	$0, 333(%rbx)
	movq	%r14, 232(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end45:
	.size	_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver, .Lfunc_end45-_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv,@function
_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv: # @_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv
	.cfi_startproc
# BB#0:
	movq	232(%rdi), %rax
	retq
.Lfunc_end46:
	.size	_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv, .Lfunc_end46-_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv
	.cfi_endproc

	.globl	_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv
	.p2align	4, 0x90
	.type	_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv,@function
_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv: # @_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv
	.cfi_startproc
# BB#0:
	movl	252(%rdi), %eax
	retq
.Lfunc_end47:
	.size	_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv, .Lfunc_end47-_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv
	.cfi_endproc

	.globl	_ZN23btDiscreteDynamicsWorld13getConstraintEi
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld13getConstraintEi,@function
_ZN23btDiscreteDynamicsWorld13getConstraintEi: # @_ZN23btDiscreteDynamicsWorld13getConstraintEi
	.cfi_startproc
# BB#0:
	movq	264(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	retq
.Lfunc_end48:
	.size	_ZN23btDiscreteDynamicsWorld13getConstraintEi, .Lfunc_end48-_ZN23btDiscreteDynamicsWorld13getConstraintEi
	.cfi_endproc

	.globl	_ZNK23btDiscreteDynamicsWorld13getConstraintEi
	.p2align	4, 0x90
	.type	_ZNK23btDiscreteDynamicsWorld13getConstraintEi,@function
_ZNK23btDiscreteDynamicsWorld13getConstraintEi: # @_ZNK23btDiscreteDynamicsWorld13getConstraintEi
	.cfi_startproc
# BB#0:
	movq	264(%rdi), %rax
	movslq	%esi, %rcx
	movq	(%rax,%rcx,8), %rax
	retq
.Lfunc_end49:
	.size	_ZNK23btDiscreteDynamicsWorld13getConstraintEi, .Lfunc_end49-_ZNK23btDiscreteDynamicsWorld13getConstraintEi
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,"axG",@progbits,_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,comdat
	.weak	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,@function
_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw: # @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	movq	%rsi, 120(%rdi)
	retq
.Lfunc_end50:
	.size	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw, .Lfunc_end50-_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14getDebugDrawerEv,"axG",@progbits,_ZN16btCollisionWorld14getDebugDrawerEv,comdat
	.weak	_ZN16btCollisionWorld14getDebugDrawerEv
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14getDebugDrawerEv,@function
_ZN16btCollisionWorld14getDebugDrawerEv: # @_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_startproc
# BB#0:
	movq	120(%rdi), %rax
	retq
.Lfunc_end51:
	.size	_ZN16btCollisionWorld14getDebugDrawerEv, .Lfunc_end51-_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_endproc

	.section	.text._ZNK23btDiscreteDynamicsWorld12getWorldTypeEv,"axG",@progbits,_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv,comdat
	.weak	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.p2align	4, 0x90
	.type	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv,@function
_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv: # @_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.cfi_startproc
# BB#0:
	movl	$2, %eax
	retq
.Lfunc_end52:
	.size	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv, .Lfunc_end52-_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.cfi_endproc

	.section	.text._ZN23btDiscreteDynamicsWorld11setNumTasksEi,"axG",@progbits,_ZN23btDiscreteDynamicsWorld11setNumTasksEi,comdat
	.weak	_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld11setNumTasksEi,@function
_ZN23btDiscreteDynamicsWorld11setNumTasksEi: # @_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end53:
	.size	_ZN23btDiscreteDynamicsWorld11setNumTasksEi, .Lfunc_end53-_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.cfi_endproc

	.section	.text._ZN23btDiscreteDynamicsWorld14updateVehiclesEf,"axG",@progbits,_ZN23btDiscreteDynamicsWorld14updateVehiclesEf,comdat
	.weak	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.p2align	4, 0x90
	.type	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf,@function
_ZN23btDiscreteDynamicsWorld14updateVehiclesEf: # @_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi268:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi269:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi270:
	.cfi_def_cfa_offset 32
.Lcfi271:
	.cfi_offset %rbx, -24
.Lcfi272:
	.cfi_offset %r14, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rdi, %r14
	movl	$.L.str.4, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpl	$0, 340(%r14)
	jle	.LBB54_4
# BB#1:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB54_2:                               # =>This Inner Loop Header: Depth=1
	movq	352(%r14), %rax
	movq	(%rax,%rbx,8), %rdi
	movq	(%rdi), %rax
.Ltmp329:
	movq	%r14, %rsi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	*16(%rax)
.Ltmp330:
# BB#3:                                 #   in Loop: Header=BB54_2 Depth=1
	incq	%rbx
	movslq	340(%r14), %rax
	cmpq	%rax, %rbx
	jl	.LBB54_2
.LBB54_4:                               # %_ZN23btDiscreteDynamicsWorld13updateActionsEf.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB54_5:
.Ltmp331:
	movq	%rax, %rbx
.Ltmp332:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp333:
# BB#6:                                 # %_ZN14CProfileSampleD2Ev.exit.i
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB54_7:
.Ltmp334:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end54:
	.size	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf, .Lfunc_end54-_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table54:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin15-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp329-.Lfunc_begin15 #   Call between .Lfunc_begin15 and .Ltmp329
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp329-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp331-.Lfunc_begin15 #     jumps to .Ltmp331
	.byte	0                       #   On action: cleanup
	.long	.Ltmp330-.Lfunc_begin15 # >> Call Site 3 <<
	.long	.Ltmp332-.Ltmp330       #   Call between .Ltmp330 and .Ltmp332
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp332-.Lfunc_begin15 # >> Call Site 4 <<
	.long	.Ltmp333-.Ltmp332       #   Call between .Ltmp332 and .Ltmp333
	.long	.Ltmp334-.Lfunc_begin15 #     jumps to .Ltmp334
	.byte	1                       #   On action: 1
	.long	.Ltmp333-.Lfunc_begin15 # >> Call Site 5 <<
	.long	.Lfunc_end54-.Ltmp333   #   Call between .Ltmp333 and .Lfunc_end54
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI55_0:
	.long	1065353216              # float 1
.LCPI55_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi273:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi274:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi276:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi277:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi278:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi279:
	.cfi_def_cfa_offset 80
.Lcfi280:
	.cfi_offset %rbx, -56
.Lcfi281:
	.cfi_offset %r12, -48
.Lcfi282:
	.cfi_offset %r13, -40
.Lcfi283:
	.cfi_offset %r14, -32
.Lcfi284:
	.cfi_offset %r15, -24
.Lcfi285:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB55_4
# BB#1:
	addss	.LCPI55_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB55_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB55_3:                               # %.split
	movss	.LCPI55_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB55_7
.LBB55_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI55_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB55_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB55_6:                               # %.split71
	movss	.LCPI55_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB55_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end55:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end55-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallbackD0Ev,@function
_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallbackD0Ev: # @_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end56:
	.size	_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallbackD0Ev, .Lfunc_end56-_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallbackD0Ev
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallback13ProcessIslandEPP17btCollisionObjectiPP20btPersistentManifoldii,@function
_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallback13ProcessIslandEPP17btCollisionObjectiPP20btPersistentManifoldii: # @_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallback13ProcessIslandEPP17btCollisionObjectiPP20btPersistentManifoldii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi286:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi287:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi288:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi289:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi290:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi291:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi292:
	.cfi_def_cfa_offset 64
.Lcfi293:
	.cfi_offset %rbx, -56
.Lcfi294:
	.cfi_offset %r12, -48
.Lcfi295:
	.cfi_offset %r13, -40
.Lcfi296:
	.cfi_offset %r14, -32
.Lcfi297:
	.cfi_offset %r15, -24
.Lcfi298:
	.cfi_offset %rbp, -16
	movq	%rdi, %r10
	testl	%r9d, %r9d
	movslq	32(%r10), %r11
	js	.LBB57_3
# BB#1:                                 # %.preheader33
	xorl	%ebp, %ebp
	testl	%r11d, %r11d
	jle	.LBB57_2
# BB#5:                                 # %.lr.ph37
	movq	24(%r10), %r13
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB57_6:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13), %rax
	movq	24(%rax), %rbx
	cmpl	$0, 220(%rbx)
	cmovsq	32(%rax), %rbx
	cmpl	%r9d, 220(%rbx)
	je	.LBB57_7
# BB#15:                                #   in Loop: Header=BB57_6 Depth=1
	incq	%rdi
	addq	$8, %r13
	cmpq	%r11, %rdi
	jl	.LBB57_6
# BB#16:                                # %.preheader.loopexitsplit
	xorl	%r13d, %r13d
	cmpl	%r11d, %edi
	jl	.LBB57_8
	jmp	.LBB57_13
.LBB57_3:
	movl	%r11d, %eax
	addl	%r8d, %eax
	je	.LBB57_17
# BB#4:
	movq	16(%r10), %rdi
	movq	(%rdi), %rax
	movq	24(%r10), %r9
	subq	$8, %rsp
.Lcfi299:
	.cfi_adjust_cfa_offset 8
	pushq	56(%r10)
.Lcfi300:
	.cfi_adjust_cfa_offset 8
	pushq	48(%r10)
.Lcfi301:
	.cfi_adjust_cfa_offset 8
	pushq	40(%r10)
.Lcfi302:
	.cfi_adjust_cfa_offset 8
	pushq	8(%r10)
.Lcfi303:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi304:
	.cfi_adjust_cfa_offset 8
	callq	*24(%rax)
	addq	$56, %rsp
.Lcfi305:
	.cfi_adjust_cfa_offset -48
	jmp	.LBB57_18
.LBB57_2:
	xorl	%edi, %edi
	xorl	%r13d, %r13d
.LBB57_7:                               # %.preheader
	cmpl	%r11d, %edi
	jge	.LBB57_13
.LBB57_8:                               # %.lr.ph
	movq	24(%r10), %r14
	movslq	%edi, %r15
	movl	%r11d, %eax
	subl	%edi, %eax
	leaq	-1(%r11), %r12
	testb	$1, %al
	jne	.LBB57_10
# BB#9:
	xorl	%ebp, %ebp
	movq	%r15, %rdi
	cmpq	%r15, %r12
	jne	.LBB57_12
	jmp	.LBB57_13
.LBB57_10:
	movq	(%r14,%r15,8), %rax
	movq	24(%rax), %rdi
	cmpl	$0, 220(%rdi)
	cmovsq	32(%rax), %rdi
	xorl	%ebp, %ebp
	cmpl	%r9d, 220(%rdi)
	sete	%bpl
	leaq	1(%r15), %rdi
	cmpq	%r15, %r12
	je	.LBB57_13
	.p2align	4, 0x90
.LBB57_12:                              # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rdi,8), %rax
	movq	24(%rax), %rbx
	cmpl	$0, 220(%rbx)
	cmovsq	32(%rax), %rbx
	xorl	%eax, %eax
	cmpl	%r9d, 220(%rbx)
	sete	%al
	addl	%ebp, %eax
	movq	8(%r14,%rdi,8), %rbp
	movq	24(%rbp), %rbx
	cmpl	$0, 220(%rbx)
	cmovsq	32(%rbp), %rbx
	xorl	%ebp, %ebp
	cmpl	%r9d, 220(%rbx)
	sete	%bpl
	addl	%eax, %ebp
	addq	$2, %rdi
	cmpq	%r11, %rdi
	jl	.LBB57_12
.LBB57_13:                              # %._crit_edge
	movl	%ebp, %eax
	addl	%r8d, %eax
	jne	.LBB57_14
.LBB57_17:
	addq	$8, %rsp
	jmp	.LBB57_18
.LBB57_14:
	movq	16(%r10), %rdi
	movq	(%rdi), %rax
	subq	$8, %rsp
.Lcfi306:
	.cfi_adjust_cfa_offset 8
	movq	%r13, %r9
	pushq	56(%r10)
.Lcfi307:
	.cfi_adjust_cfa_offset 8
	pushq	48(%r10)
.Lcfi308:
	.cfi_adjust_cfa_offset 8
	pushq	40(%r10)
.Lcfi309:
	.cfi_adjust_cfa_offset 8
	pushq	8(%r10)
.Lcfi310:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi311:
	.cfi_adjust_cfa_offset 8
	callq	*24(%rax)
	addq	$56, %rsp
.Lcfi312:
	.cfi_adjust_cfa_offset -48
.LBB57_18:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end57:
	.size	_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallback13ProcessIslandEPP17btCollisionObjectiPP20btPersistentManifoldii, .Lfunc_end57-_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallback13ProcessIslandEPP17btCollisionObjectiPP20btPersistentManifoldii
	.cfi_endproc

	.section	.text._ZN25btSimulationIslandManager14IslandCallbackD2Ev,"axG",@progbits,_ZN25btSimulationIslandManager14IslandCallbackD2Ev,comdat
	.weak	_ZN25btSimulationIslandManager14IslandCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN25btSimulationIslandManager14IslandCallbackD2Ev,@function
_ZN25btSimulationIslandManager14IslandCallbackD2Ev: # @_ZN25btSimulationIslandManager14IslandCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end58:
	.size	_ZN25btSimulationIslandManager14IslandCallbackD2Ev, .Lfunc_end58-_ZN25btSimulationIslandManager14IslandCallbackD2Ev
	.cfi_endproc

	.section	.text._ZN34btClosestNotMeConvexResultCallbackD0Ev,"axG",@progbits,_ZN34btClosestNotMeConvexResultCallbackD0Ev,comdat
	.weak	_ZN34btClosestNotMeConvexResultCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN34btClosestNotMeConvexResultCallbackD0Ev,@function
_ZN34btClosestNotMeConvexResultCallbackD0Ev: # @_ZN34btClosestNotMeConvexResultCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end59:
	.size	_ZN34btClosestNotMeConvexResultCallbackD0Ev, .Lfunc_end59-_ZN34btClosestNotMeConvexResultCallbackD0Ev
	.cfi_endproc

	.section	.text._ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy,"axG",@progbits,_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy,comdat
	.weak	_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy
	.p2align	4, 0x90
	.type	_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy,@function
_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy: # @_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%r14
.Lcfi313:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi314:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi315:
	.cfi_def_cfa_offset 64
.Lcfi316:
	.cfi_offset %rbx, -24
.Lcfi317:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%r14), %rdx
	movq	88(%rbx), %rsi
	cmpq	%rsi, %rdx
	je	.LBB60_14
# BB#1:
	movzwl	14(%rbx), %eax
	testw	8(%r14), %ax
	je	.LBB60_14
# BB#2:                                 # %_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy.exit
	movzwl	10(%r14), %eax
	testw	12(%rbx), %ax
	je	.LBB60_14
# BB#3:
	movq	112(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*56(%rax)
	testb	%al, %al
	je	.LBB60_21
# BB#4:
	movb	$1, 32(%rsp)
	movq	$0, 24(%rsp)
	movq	$0, 12(%rsp)
	movq	88(%rbx), %rax
	movq	104(%rbx), %rdi
	movq	(%rdi), %rcx
	movq	192(%rax), %rsi
.Ltmp335:
	movq	%r14, %rdx
	callq	*104(%rcx)
.Ltmp336:
# BB#5:
	xorl	%ebx, %ebx
	testq	%rax, %rax
	je	.LBB60_19
# BB#6:
	movq	16(%rax), %rdi
	testq	%rdi, %rdi
	je	.LBB60_19
# BB#7:                                 # %.loopexit
	movq	(%rdi), %rax
	movq	32(%rax), %rax
	movl	$0, 12(%rsp)
.Ltmp338:
	leaq	8(%rsp), %rsi
	callq	*%rax
.Ltmp339:
# BB#8:                                 # %.preheader
	movslq	12(%rsp), %rax
	testq	%rax, %rax
	movq	24(%rsp), %rdi
	jle	.LBB60_12
# BB#9:                                 # %.lr.ph
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB60_10:                              # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rcx,8), %rdx
	cmpl	$0, 728(%rdx)
	jg	.LBB60_15
# BB#11:                                #   in Loop: Header=BB60_10 Depth=1
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB60_10
.LBB60_12:                              # %.thread
	xorl	%ebx, %ebx
	testq	%rdi, %rdi
	jne	.LBB60_16
	jmp	.LBB60_19
.LBB60_14:
	xorl	%eax, %eax
.LBB60_22:                              # %_ZNK16btCollisionWorld20ConvexResultCallback14needsCollisionEP17btBroadphaseProxy.exit.thread
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB60_15:
	movl	$1, %ebx
.LBB60_16:                              # %.thread.thread53
	cmpb	$0, 32(%rsp)
	je	.LBB60_18
# BB#17:
	callq	_Z21btAlignedFreeInternalPv
.LBB60_18:
	movq	$0, 24(%rsp)
.LBB60_19:                              # %_ZN20btAlignedObjectArrayIP20btPersistentManifoldED2Ev.exit38
	testl	%ebx, %ebx
	je	.LBB60_21
# BB#20:
	xorl	%eax, %eax
	jmp	.LBB60_22
.LBB60_21:
	movb	$1, %al
	jmp	.LBB60_22
.LBB60_23:
.Ltmp340:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB60_27
# BB#24:
	cmpb	$0, 32(%rsp)
	je	.LBB60_26
# BB#25:
.Ltmp341:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp342:
.LBB60_26:                              # %.noexc
	movq	$0, 24(%rsp)
.LBB60_27:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB60_28:
.Ltmp343:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB60_29:                              # %.thread55
.Ltmp337:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end60:
	.size	_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy, .Lfunc_end60-_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table60:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp335-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp335
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp335-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp336-.Ltmp335       #   Call between .Ltmp335 and .Ltmp336
	.long	.Ltmp337-.Lfunc_begin16 #     jumps to .Ltmp337
	.byte	0                       #   On action: cleanup
	.long	.Ltmp338-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Ltmp339-.Ltmp338       #   Call between .Ltmp338 and .Ltmp339
	.long	.Ltmp340-.Lfunc_begin16 #     jumps to .Ltmp340
	.byte	0                       #   On action: cleanup
	.long	.Ltmp339-.Lfunc_begin16 # >> Call Site 4 <<
	.long	.Ltmp341-.Ltmp339       #   Call between .Ltmp339 and .Ltmp341
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp341-.Lfunc_begin16 # >> Call Site 5 <<
	.long	.Ltmp342-.Ltmp341       #   Call between .Ltmp341 and .Ltmp342
	.long	.Ltmp343-.Lfunc_begin16 #     jumps to .Ltmp343
	.byte	1                       #   On action: 1
	.long	.Ltmp342-.Lfunc_begin16 # >> Call Site 6 <<
	.long	.Lfunc_end60-.Ltmp342   #   Call between .Ltmp342 and .Lfunc_end60
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI61_0:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI61_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.text._ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb,"axG",@progbits,_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb,comdat
	.weak	_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.p2align	4, 0x90
	.type	_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb,@function
_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb: # @_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.cfi_startproc
# BB#0:
	movq	(%rsi), %rax
	movss	.LCPI61_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	cmpq	88(%rdi), %rax
	je	.LBB61_7
# BB#1:
	testb	$4, 216(%rax)
	jne	.LBB61_7
# BB#2:
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	16(%rdi), %xmm1
	subss	20(%rdi), %xmm2
	movss	40(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	subss	24(%rdi), %xmm3
	mulss	16(%rsi), %xmm1
	mulss	20(%rsi), %xmm2
	addss	%xmm1, %xmm2
	mulss	24(%rsi), %xmm3
	addss	%xmm2, %xmm3
	movss	96(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	xorps	.LCPI61_1(%rip), %xmm1
	ucomiss	%xmm1, %xmm3
	jae	.LBB61_7
# BB#3:
	movl	48(%rsi), %ecx
	movl	%ecx, 8(%rdi)
	movq	%rax, 80(%rdi)
	testb	%dl, %dl
	je	.LBB61_5
# BB#4:
	leaq	16(%rsi), %rax
	movups	(%rax), %xmm0
	movups	%xmm0, 48(%rdi)
	jmp	.LBB61_6
.LBB61_5:
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	mulss	40(%rax), %xmm2
	mulss	44(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	48(%rax), %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm3, 48(%rdi)
	movlps	%xmm1, 56(%rdi)
.LBB61_6:                               # %_ZN16btCollisionWorld27ClosestConvexResultCallback15addSingleResultERNS_17LocalConvexResultEb.exit
	movups	32(%rsi), %xmm0
	movups	%xmm0, 64(%rdi)
	movss	48(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
.LBB61_7:
	retq
.Lfunc_end61:
	.size	_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb, .Lfunc_end61-_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.cfi_endproc

	.section	.text._ZN17DebugDrawcallbackD0Ev,"axG",@progbits,_ZN17DebugDrawcallbackD0Ev,comdat
	.weak	_ZN17DebugDrawcallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN17DebugDrawcallbackD0Ev,@function
_ZN17DebugDrawcallbackD0Ev:             # @_ZN17DebugDrawcallbackD0Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi318:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi319:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi320:
	.cfi_def_cfa_offset 32
.Lcfi321:
	.cfi_offset %rbx, -24
.Lcfi322:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	leaq	8(%rbx), %rdi
.Ltmp344:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp345:
# BB#1:
.Ltmp350:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp351:
# BB#2:                                 # %_ZN17DebugDrawcallbackD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB62_5:
.Ltmp352:
	movq	%rax, %r14
	jmp	.LBB62_6
.LBB62_3:
.Ltmp346:
	movq	%rax, %r14
.Ltmp347:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp348:
.LBB62_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB62_4:
.Ltmp349:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end62:
	.size	_ZN17DebugDrawcallbackD0Ev, .Lfunc_end62-_ZN17DebugDrawcallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table62:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp344-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin17 #     jumps to .Ltmp346
	.byte	0                       #   On action: cleanup
	.long	.Ltmp350-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin17 #     jumps to .Ltmp352
	.byte	0                       #   On action: cleanup
	.long	.Ltmp347-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin17 #     jumps to .Ltmp349
	.byte	1                       #   On action: 1
	.long	.Ltmp348-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end62-.Ltmp348   #   Call between .Ltmp348 and .Lfunc_end62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN17DebugDrawcallback15processTriangleEP9btVector3ii,"axG",@progbits,_ZN17DebugDrawcallback15processTriangleEP9btVector3ii,comdat
	.weak	_ZN17DebugDrawcallback15processTriangleEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN17DebugDrawcallback15processTriangleEP9btVector3ii,@function
_ZN17DebugDrawcallback15processTriangleEP9btVector3ii: # @_ZN17DebugDrawcallback15processTriangleEP9btVector3ii
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi323:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi324:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi325:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi326:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi327:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi328:
	.cfi_def_cfa_offset 96
.Lcfi329:
	.cfi_offset %rbx, -48
.Lcfi330:
	.cfi_offset %r12, -40
.Lcfi331:
	.cfi_offset %r13, -32
.Lcfi332:
	.cfi_offset %r14, -24
.Lcfi333:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	56(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	44(%rbx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	movaps	%xmm6, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	movss	60(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm10   # xmm10 = xmm10[0],xmm5[0],xmm10[1],xmm5[1]
	movaps	%xmm3, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm10, %xmm5
	addps	%xmm2, %xmm5
	movss	64(%rbx), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	48(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm2    # xmm2 = xmm2[0],xmm7[0],xmm2[1],xmm7[1]
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm2, %xmm4
	addps	%xmm5, %xmm4
	movsd	88(%rbx), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm8, %xmm4
	movss	72(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm6
	movss	76(%rbx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm3
	addss	%xmm6, %xmm3
	movss	80(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm3, %xmm0
	movss	96(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm0
	xorps	%xmm13, %xmm13
	xorps	%xmm5, %xmm5
	movss	%xmm0, %xmm5            # xmm5 = xmm0[0],xmm5[1,2,3]
	movlps	%xmm4, 32(%rsp)
	movlps	%xmm5, 40(%rsp)
	movss	16(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movss	24(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm1, %xmm0
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm10, %xmm4
	addps	%xmm0, %xmm4
	movaps	%xmm5, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	addps	%xmm4, %xmm0
	addps	%xmm8, %xmm0
	mulss	%xmm9, %xmm6
	mulss	%xmm11, %xmm7
	addss	%xmm6, %xmm7
	mulss	%xmm12, %xmm5
	addss	%xmm7, %xmm5
	addss	%xmm3, %xmm5
	xorps	%xmm4, %xmm4
	movss	%xmm5, %xmm4            # xmm4 = xmm5[0],xmm4[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movlps	%xmm4, 24(%rsp)
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm1, %xmm6
	movaps	%xmm4, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm10, %xmm1
	addps	%xmm6, %xmm1
	movaps	%xmm5, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm2, %xmm6
	addps	%xmm1, %xmm6
	addps	%xmm8, %xmm6
	mulss	%xmm9, %xmm0
	mulss	%xmm11, %xmm4
	addss	%xmm0, %xmm4
	mulss	%xmm12, %xmm5
	addss	%xmm4, %xmm5
	addss	%xmm3, %xmm5
	movss	%xmm5, %xmm13           # xmm13 = xmm5[0],xmm13[1,2,3]
	movlps	%xmm6, (%rsp)
	movlps	%xmm13, 8(%rsp)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	24(%rbx), %r14
	leaq	32(%rsp), %r15
	leaq	16(%rsp), %r12
	movq	%r15, %rsi
	movq	%r12, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %r13
	movq	%r12, %rsi
	movq	%r13, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	movq	16(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r13, %rsi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	*40(%rax)
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end63:
	.size	_ZN17DebugDrawcallback15processTriangleEP9btVector3ii, .Lfunc_end63-_ZN17DebugDrawcallback15processTriangleEP9btVector3ii
	.cfi_endproc

	.section	.text._ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii,"axG",@progbits,_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii,comdat
	.weak	_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii,@function
_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii: # @_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end64:
	.size	_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii, .Lfunc_end64-_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.cfi_endproc

	.section	.text._ZThn8_N17DebugDrawcallbackD1Ev,"axG",@progbits,_ZThn8_N17DebugDrawcallbackD1Ev,comdat
	.weak	_ZThn8_N17DebugDrawcallbackD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N17DebugDrawcallbackD1Ev,@function
_ZThn8_N17DebugDrawcallbackD1Ev:        # @_ZThn8_N17DebugDrawcallbackD1Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi334:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi335:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi336:
	.cfi_def_cfa_offset 32
.Lcfi337:
	.cfi_offset %rbx, -24
.Lcfi338:
	.cfi_offset %r14, -16
.Ltmp353:
	leaq	-8(%rdi), %rbx
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp354:
# BB#1:                                 # %_ZN17DebugDrawcallbackD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN18btTriangleCallbackD2Ev # TAILCALL
.LBB65_2:
.Ltmp355:
	movq	%rax, %r14
.Ltmp356:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp357:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB65_4:
.Ltmp358:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end65:
	.size	_ZThn8_N17DebugDrawcallbackD1Ev, .Lfunc_end65-_ZThn8_N17DebugDrawcallbackD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table65:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp353-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin18 #     jumps to .Ltmp355
	.byte	0                       #   On action: cleanup
	.long	.Ltmp354-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp356-.Ltmp354       #   Call between .Ltmp354 and .Ltmp356
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp356-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin18 #     jumps to .Ltmp358
	.byte	1                       #   On action: 1
	.long	.Ltmp357-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end65-.Ltmp357   #   Call between .Ltmp357 and .Lfunc_end65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N17DebugDrawcallbackD0Ev,"axG",@progbits,_ZThn8_N17DebugDrawcallbackD0Ev,comdat
	.weak	_ZThn8_N17DebugDrawcallbackD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N17DebugDrawcallbackD0Ev,@function
_ZThn8_N17DebugDrawcallbackD0Ev:        # @_ZThn8_N17DebugDrawcallbackD0Ev
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:
	pushq	%r14
.Lcfi339:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi340:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi341:
	.cfi_def_cfa_offset 32
.Lcfi342:
	.cfi_offset %rbx, -24
.Lcfi343:
	.cfi_offset %r14, -16
.Ltmp359:
	leaq	-8(%rdi), %rbx
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp360:
# BB#1:
.Ltmp365:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp366:
# BB#2:                                 # %_ZN17DebugDrawcallbackD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB66_5:
.Ltmp367:
	movq	%rax, %r14
	jmp	.LBB66_6
.LBB66_3:
.Ltmp361:
	movq	%rax, %r14
.Ltmp362:
	movq	%rbx, %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp363:
.LBB66_6:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB66_4:
.Ltmp364:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end66:
	.size	_ZThn8_N17DebugDrawcallbackD0Ev, .Lfunc_end66-_ZThn8_N17DebugDrawcallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table66:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp359-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin19 #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp365-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Ltmp366-.Ltmp365       #   Call between .Ltmp365 and .Ltmp366
	.long	.Ltmp367-.Lfunc_begin19 #     jumps to .Ltmp367
	.byte	0                       #   On action: cleanup
	.long	.Ltmp362-.Lfunc_begin19 # >> Call Site 3 <<
	.long	.Ltmp363-.Ltmp362       #   Call between .Ltmp362 and .Ltmp363
	.long	.Ltmp364-.Lfunc_begin19 #     jumps to .Ltmp364
	.byte	1                       #   On action: 1
	.long	.Ltmp363-.Lfunc_begin19 # >> Call Site 4 <<
	.long	.Lfunc_end66-.Ltmp363   #   Call between .Ltmp363 and .Lfunc_end66
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii,"axG",@progbits,_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii,comdat
	.weak	_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii,@function
_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii: # @_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.cfi_startproc
# BB#0:
	movq	-8(%rdi), %rax
	addq	$-8, %rdi
	movq	16(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end67:
	.size	_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii, .Lfunc_end67-_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.cfi_endproc

	.section	.text._ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii,"axG",@progbits,_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii,comdat
	.weak	_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii
	.p2align	4, 0x90
	.type	_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii,@function
_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii: # @_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi344:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi345:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi346:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi347:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi348:
	.cfi_def_cfa_offset 48
.Lcfi349:
	.cfi_offset %rbx, -48
.Lcfi350:
	.cfi_offset %r12, -40
.Lcfi351:
	.cfi_offset %r14, -32
.Lcfi352:
	.cfi_offset %r15, -24
.Lcfi353:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movl	%esi, %r12d
	movq	%rdi, %r15
	.p2align	4, 0x90
.LBB68_1:                               # %tailrecurse
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB68_2 Depth 2
                                        #       Child Loop BB68_3 Depth 3
                                        #       Child Loop BB68_5 Depth 3
	movl	%r12d, %esi
	movq	16(%r15), %r9
	leal	(%rsi,%r14), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	movslq	%ecx, %rax
	movq	(%r9,%rax,8), %r8
	movl	%r14d, %r10d
	jmp	.LBB68_2
	.p2align	4, 0x90
.LBB68_9:                               # %._crit_edge
                                        #   in Loop: Header=BB68_2 Depth=2
	movq	16(%r15), %r9
	movl	%edx, %r10d
.LBB68_2:                               #   Parent Loop BB68_1 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB68_3 Depth 3
                                        #       Child Loop BB68_5 Depth 3
	movq	24(%r8), %rax
	cmpl	$0, 220(%rax)
	cmovsq	32(%r8), %rax
	movl	220(%rax), %r11d
	movslq	%r12d, %rax
	leaq	(%r9,%rax,8), %rax
	movl	%r12d, %ebx
	.p2align	4, 0x90
.LBB68_3:                               #   Parent Loop BB68_1 Depth=1
                                        #     Parent Loop BB68_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%rax), %rbp
	movq	24(%rbp), %rcx
	cmpl	$0, 220(%rcx)
	cmovsq	32(%rbp), %rcx
	incl	%ebx
	addq	$8, %rax
	cmpl	%r11d, 220(%rcx)
	jl	.LBB68_3
# BB#4:                                 # %.preheader
                                        #   in Loop: Header=BB68_2 Depth=2
	movslq	%r10d, %rcx
	shlq	$3, %rcx
	.p2align	4, 0x90
.LBB68_5:                               #   Parent Loop BB68_1 Depth=1
                                        #     Parent Loop BB68_2 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r9,%rcx), %rdi
	movq	24(%rdi), %rdx
	cmpl	$0, 220(%rdx)
	cmovsq	32(%rdi), %rdx
	addq	$-8, %rcx
	decl	%r10d
	cmpl	220(%rdx), %r11d
	jl	.LBB68_5
# BB#6:                                 #   in Loop: Header=BB68_2 Depth=2
	leal	-1(%rbx), %r12d
	leal	1(%r10), %edx
	cmpl	%edx, %r12d
	jg	.LBB68_8
# BB#7:                                 #   in Loop: Header=BB68_2 Depth=2
	movq	%rdi, -8(%rax)
	movq	16(%r15), %rax
	movq	%rbp, 8(%rax,%rcx)
	movl	%r10d, %edx
	movl	%ebx, %r12d
.LBB68_8:                               #   in Loop: Header=BB68_2 Depth=2
	cmpl	%edx, %r12d
	jle	.LBB68_9
# BB#10:                                #   in Loop: Header=BB68_1 Depth=1
	cmpl	%esi, %edx
	jle	.LBB68_12
# BB#11:                                #   in Loop: Header=BB68_1 Depth=1
	movq	%r15, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii
.LBB68_12:                              #   in Loop: Header=BB68_1 Depth=1
	cmpl	%r14d, %r12d
	jl	.LBB68_1
# BB#13:
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end68:
	.size	_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii, .Lfunc_end68-_ZN20btAlignedObjectArrayIP17btTypedConstraintE17quickSortInternalI33btSortConstraintOnIslandPredicateEEvT_ii
	.cfi_endproc

	.type	_ZTV23btDiscreteDynamicsWorld,@object # @_ZTV23btDiscreteDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTV23btDiscreteDynamicsWorld
	.p2align	3
_ZTV23btDiscreteDynamicsWorld:
	.quad	0
	.quad	_ZTI23btDiscreteDynamicsWorld
	.quad	_ZN23btDiscreteDynamicsWorldD2Ev
	.quad	_ZN23btDiscreteDynamicsWorldD0Ev
	.quad	_ZN16btCollisionWorld11updateAabbsEv
	.quad	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.quad	_ZN16btCollisionWorld14getDebugDrawerEv
	.quad	_ZN23btDiscreteDynamicsWorld18addCollisionObjectEP17btCollisionObjectss
	.quad	_ZN23btDiscreteDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.quad	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
	.quad	_ZN23btDiscreteDynamicsWorld14stepSimulationEfif
	.quad	_ZN23btDiscreteDynamicsWorld14debugDrawWorldEv
	.quad	_ZN23btDiscreteDynamicsWorld13addConstraintEP17btTypedConstraintb
	.quad	_ZN23btDiscreteDynamicsWorld16removeConstraintEP17btTypedConstraint
	.quad	_ZN23btDiscreteDynamicsWorld9addActionEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld12removeActionEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld10setGravityERK9btVector3
	.quad	_ZNK23btDiscreteDynamicsWorld10getGravityEv
	.quad	_ZN23btDiscreteDynamicsWorld23synchronizeMotionStatesEv
	.quad	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBody
	.quad	_ZN23btDiscreteDynamicsWorld15removeRigidBodyEP11btRigidBody
	.quad	_ZN23btDiscreteDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.quad	_ZN23btDiscreteDynamicsWorld19getConstraintSolverEv
	.quad	_ZNK23btDiscreteDynamicsWorld17getNumConstraintsEv
	.quad	_ZN23btDiscreteDynamicsWorld13getConstraintEi
	.quad	_ZNK23btDiscreteDynamicsWorld13getConstraintEi
	.quad	_ZNK23btDiscreteDynamicsWorld12getWorldTypeEv
	.quad	_ZN23btDiscreteDynamicsWorld11clearForcesEv
	.quad	_ZN23btDiscreteDynamicsWorld10addVehicleEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld13removeVehicleEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld12addCharacterEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld15removeCharacterEP17btActionInterface
	.quad	_ZN23btDiscreteDynamicsWorld25predictUnconstraintMotionEf
	.quad	_ZN23btDiscreteDynamicsWorld19integrateTransformsEf
	.quad	_ZN23btDiscreteDynamicsWorld26calculateSimulationIslandsEv
	.quad	_ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfo
	.quad	_ZN23btDiscreteDynamicsWorld28internalSingleStepSimulationEf
	.quad	_ZN23btDiscreteDynamicsWorld18saveKinematicStateEf
	.quad	_ZN23btDiscreteDynamicsWorld12addRigidBodyEP11btRigidBodyss
	.quad	_ZN23btDiscreteDynamicsWorld12applyGravityEv
	.quad	_ZN23btDiscreteDynamicsWorld11setNumTasksEi
	.quad	_ZN23btDiscreteDynamicsWorld14updateVehiclesEf
	.size	_ZTV23btDiscreteDynamicsWorld, 336

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"debugDrawWorld"
	.size	.L.str, 15

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"synchronizeMotionStates"
	.size	.L.str.1, 24

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"stepSimulation"
	.size	.L.str.2, 15

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"internalSingleStepSimulation"
	.size	.L.str.3, 29

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"updateActions"
	.size	.L.str.4, 14

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"updateActivationState"
	.size	.L.str.5, 22

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"solveConstraints"
	.size	.L.str.6, 17

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"calculateSimulationIslands"
	.size	.L.str.7, 27

	.type	gNumClampedCcdMotions,@object # @gNumClampedCcdMotions
	.bss
	.globl	gNumClampedCcdMotions
	.p2align	2
gNumClampedCcdMotions:
	.long	0                       # 0x0
	.size	gNumClampedCcdMotions, 4

	.type	.L.str.8,@object        # @.str.8
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.8:
	.asciz	"integrateTransforms"
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"CCD motion clamping"
	.size	.L.str.9, 20

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"predictUnconstraintMotion"
	.size	.L.str.10, 26

	.type	_ZTS23btDiscreteDynamicsWorld,@object # @_ZTS23btDiscreteDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTS23btDiscreteDynamicsWorld
	.p2align	4
_ZTS23btDiscreteDynamicsWorld:
	.asciz	"23btDiscreteDynamicsWorld"
	.size	_ZTS23btDiscreteDynamicsWorld, 26

	.type	_ZTS15btDynamicsWorld,@object # @_ZTS15btDynamicsWorld
	.section	.rodata._ZTS15btDynamicsWorld,"aG",@progbits,_ZTS15btDynamicsWorld,comdat
	.weak	_ZTS15btDynamicsWorld
	.p2align	4
_ZTS15btDynamicsWorld:
	.asciz	"15btDynamicsWorld"
	.size	_ZTS15btDynamicsWorld, 18

	.type	_ZTI15btDynamicsWorld,@object # @_ZTI15btDynamicsWorld
	.section	.rodata._ZTI15btDynamicsWorld,"aG",@progbits,_ZTI15btDynamicsWorld,comdat
	.weak	_ZTI15btDynamicsWorld
	.p2align	4
_ZTI15btDynamicsWorld:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btDynamicsWorld
	.quad	_ZTI16btCollisionWorld
	.size	_ZTI15btDynamicsWorld, 24

	.type	_ZTI23btDiscreteDynamicsWorld,@object # @_ZTI23btDiscreteDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTI23btDiscreteDynamicsWorld
	.p2align	4
_ZTI23btDiscreteDynamicsWorld:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS23btDiscreteDynamicsWorld
	.quad	_ZTI15btDynamicsWorld
	.size	_ZTI23btDiscreteDynamicsWorld, 24

	.type	_ZTVZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback,@object # @_ZTVZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback
	.p2align	3
_ZTVZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback:
	.quad	0
	.quad	_ZTIZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback
	.quad	_ZN25btSimulationIslandManager14IslandCallbackD2Ev
	.quad	_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallbackD0Ev
	.quad	_ZZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoEN27InplaceSolverIslandCallback13ProcessIslandEPP17btCollisionObjectiPP20btPersistentManifoldii
	.size	_ZTVZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback, 40

	.type	_ZTSZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback,@object # @_ZTSZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback
	.p2align	4
_ZTSZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback:
	.asciz	"ZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback"
	.size	_ZTSZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback, 99

	.type	_ZTSN25btSimulationIslandManager14IslandCallbackE,@object # @_ZTSN25btSimulationIslandManager14IslandCallbackE
	.section	.rodata._ZTSN25btSimulationIslandManager14IslandCallbackE,"aG",@progbits,_ZTSN25btSimulationIslandManager14IslandCallbackE,comdat
	.weak	_ZTSN25btSimulationIslandManager14IslandCallbackE
	.p2align	4
_ZTSN25btSimulationIslandManager14IslandCallbackE:
	.asciz	"N25btSimulationIslandManager14IslandCallbackE"
	.size	_ZTSN25btSimulationIslandManager14IslandCallbackE, 46

	.type	_ZTIN25btSimulationIslandManager14IslandCallbackE,@object # @_ZTIN25btSimulationIslandManager14IslandCallbackE
	.section	.rodata._ZTIN25btSimulationIslandManager14IslandCallbackE,"aG",@progbits,_ZTIN25btSimulationIslandManager14IslandCallbackE,comdat
	.weak	_ZTIN25btSimulationIslandManager14IslandCallbackE
	.p2align	3
_ZTIN25btSimulationIslandManager14IslandCallbackE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN25btSimulationIslandManager14IslandCallbackE
	.size	_ZTIN25btSimulationIslandManager14IslandCallbackE, 16

	.type	_ZTIZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback,@object # @_ZTIZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback
	.section	.rodata,"a",@progbits
	.p2align	4
_ZTIZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback
	.quad	_ZTIN25btSimulationIslandManager14IslandCallbackE
	.size	_ZTIZN23btDiscreteDynamicsWorld16solveConstraintsER19btContactSolverInfoE27InplaceSolverIslandCallback, 24

	.type	_ZTV34btClosestNotMeConvexResultCallback,@object # @_ZTV34btClosestNotMeConvexResultCallback
	.section	.rodata._ZTV34btClosestNotMeConvexResultCallback,"aG",@progbits,_ZTV34btClosestNotMeConvexResultCallback,comdat
	.weak	_ZTV34btClosestNotMeConvexResultCallback
	.p2align	3
_ZTV34btClosestNotMeConvexResultCallback:
	.quad	0
	.quad	_ZTI34btClosestNotMeConvexResultCallback
	.quad	_ZN16btCollisionWorld20ConvexResultCallbackD2Ev
	.quad	_ZN34btClosestNotMeConvexResultCallbackD0Ev
	.quad	_ZNK34btClosestNotMeConvexResultCallback14needsCollisionEP17btBroadphaseProxy
	.quad	_ZN34btClosestNotMeConvexResultCallback15addSingleResultERN16btCollisionWorld17LocalConvexResultEb
	.size	_ZTV34btClosestNotMeConvexResultCallback, 48

	.type	_ZTS34btClosestNotMeConvexResultCallback,@object # @_ZTS34btClosestNotMeConvexResultCallback
	.section	.rodata._ZTS34btClosestNotMeConvexResultCallback,"aG",@progbits,_ZTS34btClosestNotMeConvexResultCallback,comdat
	.weak	_ZTS34btClosestNotMeConvexResultCallback
	.p2align	4
_ZTS34btClosestNotMeConvexResultCallback:
	.asciz	"34btClosestNotMeConvexResultCallback"
	.size	_ZTS34btClosestNotMeConvexResultCallback, 37

	.type	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE,@object # @_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE
	.section	.rodata._ZTSN16btCollisionWorld27ClosestConvexResultCallbackE,"aG",@progbits,_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE,comdat
	.weak	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE
	.p2align	4
_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE:
	.asciz	"N16btCollisionWorld27ClosestConvexResultCallbackE"
	.size	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE, 50

	.type	_ZTSN16btCollisionWorld20ConvexResultCallbackE,@object # @_ZTSN16btCollisionWorld20ConvexResultCallbackE
	.section	.rodata._ZTSN16btCollisionWorld20ConvexResultCallbackE,"aG",@progbits,_ZTSN16btCollisionWorld20ConvexResultCallbackE,comdat
	.weak	_ZTSN16btCollisionWorld20ConvexResultCallbackE
	.p2align	4
_ZTSN16btCollisionWorld20ConvexResultCallbackE:
	.asciz	"N16btCollisionWorld20ConvexResultCallbackE"
	.size	_ZTSN16btCollisionWorld20ConvexResultCallbackE, 43

	.type	_ZTIN16btCollisionWorld20ConvexResultCallbackE,@object # @_ZTIN16btCollisionWorld20ConvexResultCallbackE
	.section	.rodata._ZTIN16btCollisionWorld20ConvexResultCallbackE,"aG",@progbits,_ZTIN16btCollisionWorld20ConvexResultCallbackE,comdat
	.weak	_ZTIN16btCollisionWorld20ConvexResultCallbackE
	.p2align	3
_ZTIN16btCollisionWorld20ConvexResultCallbackE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN16btCollisionWorld20ConvexResultCallbackE
	.size	_ZTIN16btCollisionWorld20ConvexResultCallbackE, 16

	.type	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE,@object # @_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE
	.section	.rodata._ZTIN16btCollisionWorld27ClosestConvexResultCallbackE,"aG",@progbits,_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE,comdat
	.weak	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE
	.p2align	4
_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN16btCollisionWorld27ClosestConvexResultCallbackE
	.quad	_ZTIN16btCollisionWorld20ConvexResultCallbackE
	.size	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE, 24

	.type	_ZTI34btClosestNotMeConvexResultCallback,@object # @_ZTI34btClosestNotMeConvexResultCallback
	.section	.rodata._ZTI34btClosestNotMeConvexResultCallback,"aG",@progbits,_ZTI34btClosestNotMeConvexResultCallback,comdat
	.weak	_ZTI34btClosestNotMeConvexResultCallback
	.p2align	4
_ZTI34btClosestNotMeConvexResultCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS34btClosestNotMeConvexResultCallback
	.quad	_ZTIN16btCollisionWorld27ClosestConvexResultCallbackE
	.size	_ZTI34btClosestNotMeConvexResultCallback, 24

	.type	_ZTV17DebugDrawcallback,@object # @_ZTV17DebugDrawcallback
	.section	.rodata._ZTV17DebugDrawcallback,"aG",@progbits,_ZTV17DebugDrawcallback,comdat
	.weak	_ZTV17DebugDrawcallback
	.p2align	3
_ZTV17DebugDrawcallback:
	.quad	0
	.quad	_ZTI17DebugDrawcallback
	.quad	_ZN17DebugDrawcallbackD2Ev
	.quad	_ZN17DebugDrawcallbackD0Ev
	.quad	_ZN17DebugDrawcallback15processTriangleEP9btVector3ii
	.quad	_ZN17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.quad	-8
	.quad	_ZTI17DebugDrawcallback
	.quad	_ZThn8_N17DebugDrawcallbackD1Ev
	.quad	_ZThn8_N17DebugDrawcallbackD0Ev
	.quad	_ZThn8_N17DebugDrawcallback28internalProcessTriangleIndexEP9btVector3ii
	.size	_ZTV17DebugDrawcallback, 88

	.type	_ZTS17DebugDrawcallback,@object # @_ZTS17DebugDrawcallback
	.section	.rodata._ZTS17DebugDrawcallback,"aG",@progbits,_ZTS17DebugDrawcallback,comdat
	.weak	_ZTS17DebugDrawcallback
	.p2align	4
_ZTS17DebugDrawcallback:
	.asciz	"17DebugDrawcallback"
	.size	_ZTS17DebugDrawcallback, 20

	.type	_ZTI17DebugDrawcallback,@object # @_ZTI17DebugDrawcallback
	.section	.rodata._ZTI17DebugDrawcallback,"aG",@progbits,_ZTI17DebugDrawcallback,comdat
	.weak	_ZTI17DebugDrawcallback
	.p2align	4
_ZTI17DebugDrawcallback:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17DebugDrawcallback
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI18btTriangleCallback
	.quad	2                       # 0x2
	.quad	_ZTI31btInternalTriangleIndexCallback
	.quad	2050                    # 0x802
	.size	_ZTI17DebugDrawcallback, 56

	.type	.Lswitch.table,@object  # @switch.table
	.section	.rodata,"a",@progbits
	.p2align	4
.Lswitch.table:
	.long	1132396544              # 0x437f0000
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1132396544              # 0x437f0000
	.long	1132396544              # 0x437f0000
	.size	.Lswitch.table, 20

	.type	.Lswitch.table.1,@object # @switch.table.1
	.p2align	4
.Lswitch.table.1:
	.long	1132396544              # 0x437f0000
	.long	1132396544              # 0x437f0000
	.long	1132396544              # 0x437f0000
	.long	0                       # 0x0
	.long	1132396544              # 0x437f0000
	.size	.Lswitch.table.1, 20

	.type	.Lswitch.table.2,@object # @switch.table.2
	.p2align	4
.Lswitch.table.2:
	.long	1132396544              # 0x437f0000
	.long	0                       # 0x0
	.long	1132396544              # 0x437f0000
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	.Lswitch.table.2, 20


	.globl	_ZN23btDiscreteDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.type	_ZN23btDiscreteDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration,@function
_ZN23btDiscreteDynamicsWorldC1EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration = _ZN23btDiscreteDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.globl	_ZN23btDiscreteDynamicsWorldD1Ev
	.type	_ZN23btDiscreteDynamicsWorldD1Ev,@function
_ZN23btDiscreteDynamicsWorldD1Ev = _ZN23btDiscreteDynamicsWorldD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
