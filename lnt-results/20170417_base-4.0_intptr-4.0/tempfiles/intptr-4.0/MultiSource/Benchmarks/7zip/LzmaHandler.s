	.text
	.file	"LzmaHandler.bc"
	.globl	_ZN8NArchive5NLzma7CHeader5ParseEPKhb
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma7CHeader5ParseEPKhb,@function
_ZN8NArchive5NLzma7CHeader5ParseEPKhb:  # @_ZN8NArchive5NLzma7CHeader5ParseEPKhb
	.cfi_startproc
# BB#0:
	movb	$0, 8(%rdi)
	movb	$1, %r8b
	testb	%dl, %dl
	je	.LBB0_2
# BB#1:
	movb	(%rsi), %al
	movb	%al, 8(%rdi)
	cmpb	$2, %al
	setb	%r8b
.LBB0_2:
	movzbl	%dl, %eax
	movb	(%rsi,%rax), %cl
	movb	%cl, 9(%rdi)
	movb	1(%rsi,%rax), %dl
	movb	%dl, 10(%rdi)
	movb	2(%rsi,%rax), %dl
	movb	%dl, 11(%rdi)
	movb	3(%rsi,%rax), %dl
	movb	%dl, 12(%rdi)
	movb	4(%rsi,%rax), %dl
	movb	%dl, 13(%rdi)
	movq	5(%rsi,%rax), %rdx
	movq	%rdx, (%rdi)
	xorl	%eax, %eax
	cmpb	$-32, %cl
	ja	.LBB0_16
# BB#3:
	testb	%r8b, %r8b
	je	.LBB0_16
# BB#4:
	cmpq	$-1, %rdx
	je	.LBB0_13
# BB#5:
	shrq	$56, %rdx
	je	.LBB0_13
# BB#6:
	xorl	%eax, %eax
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB0_13:
	movl	10(%rdi), %edx
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movl	$2, %eax
	shll	%cl, %eax
	movl	$3, %esi
	shll	%cl, %esi
	cmpl	%eax, %edx
	movb	$1, %al
	je	.LBB0_16
# BB#15:                                #   in Loop: Header=BB0_14 Depth=1
	cmpl	%esi, %edx
	je	.LBB0_16
# BB#7:                                 #   in Loop: Header=BB0_14 Depth=1
	incl	%ecx
	movl	$2, %edi
	shll	%cl, %edi
	movl	$3, %esi
	shll	%cl, %esi
	cmpl	%edi, %edx
	je	.LBB0_16
# BB#8:                                 #   in Loop: Header=BB0_14 Depth=1
	cmpl	%esi, %edx
	je	.LBB0_16
# BB#9:                                 #   in Loop: Header=BB0_14 Depth=1
	incl	%ecx
	movl	$2, %edi
	shll	%cl, %edi
	movl	$3, %esi
	shll	%cl, %esi
	cmpl	%edi, %edx
	je	.LBB0_16
# BB#10:                                #   in Loop: Header=BB0_14 Depth=1
	cmpl	%esi, %edx
	je	.LBB0_16
# BB#11:                                #   in Loop: Header=BB0_14 Depth=1
	incl	%ecx
	cmpl	$31, %ecx
	jl	.LBB0_14
# BB#12:
	cmpl	$-1, %edx
	sete	%al
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.LBB0_16:                               # %_ZN8NArchive5NLzmaL12CheckDicSizeEPKh.exit
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	retq
.Lfunc_end0:
	.size	_ZN8NArchive5NLzma7CHeader5ParseEPKhb, .Lfunc_end0-_ZN8NArchive5NLzma7CHeader5ParseEPKhb
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CDecoder6CreateEbP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CDecoder6CreateEbP19ISequentialInStream,@function
_ZN8NArchive5NLzma8CDecoder6CreateEbP19ISequentialInStream: # @_ZN8NArchive5NLzma8CDecoder6CreateEbP19ISequentialInStream
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %r15d
	movq	%rdi, %rbx
	cmpq	$0, 8(%rbx)
	jne	.LBB1_5
# BB#1:
	movl	$280, %edi              # imm = 0x118
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp0:
	movq	%rbp, %rdi
	callq	_ZN9NCompress5NLzma8CDecoderC1Ev
.Ltmp1:
# BB#2:
	movq	%rbp, (%rbx)
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*8(%rax)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB1_4:                                # %_ZN9CMyComPtrI14ICompressCoderEaSEPS0_.exit
	movq	%rbp, 8(%rbx)
.LBB1_5:
	testb	%r15b, %r15b
	je	.LBB1_19
# BB#6:
	cmpq	$0, 16(%rbx)
	je	.LBB1_7
.LBB1_19:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r14, %rsi
	callq	*80(%rax)
	movl	%eax, %ebp
.LBB1_20:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_7:
	movq	$0, (%rsp)
.Ltmp3:
	movq	%rsp, %rsi
	movl	$50528515, %edi         # imm = 0x3030103
	xorl	%edx, %edx
	callq	_Z11CreateCoderyR9CMyComPtrI14ICompressCoderEb
	movl	%eax, %ebp
.Ltmp4:
# BB#8:
	testl	%ebp, %ebp
	je	.LBB1_12
# BB#9:
	movl	$1, %r15d
	jmp	.LBB1_16
.LBB1_12:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_13
# BB#14:
	leaq	16(%rbx), %rbp
	movq	(%rdi), %rax
.Ltmp5:
	movl	$IID_ISequentialOutStream, %esi
	movq	%rbp, %rdx
	callq	*(%rax)
.Ltmp6:
# BB#15:                                # %_ZNK9CMyComPtrI14ICompressCoderE14QueryInterfaceI20ISequentialOutStreamEEiRK4GUIDPPT_.exit
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	cmpq	$0, (%rbp)
	sete	%r15b
	movl	$-2147467263, %ebp      # imm = 0x80004001
	cmovnel	%eax, %ebp
.LBB1_16:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_18
# BB#17:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB1_18:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit16
	testl	%r15d, %r15d
	jne	.LBB1_20
	jmp	.LBB1_19
.LBB1_13:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit16.thread
	movl	$-2147467263, %ebp      # imm = 0x80004001
	jmp	.LBB1_20
.LBB1_10:
.Ltmp7:
	movq	%rax, %rbx
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_22
# BB#11:
	movq	(%rdi), %rax
.Ltmp8:
	callq	*16(%rax)
.Ltmp9:
	jmp	.LBB1_22
.LBB1_23:
.Ltmp10:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_21:
.Ltmp2:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB1_22:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN8NArchive5NLzma8CDecoder6CreateEbP19ISequentialInStream, .Lfunc_end1-_ZN8NArchive5NLzma8CDecoder6CreateEbP19ISequentialInStream
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp8-.Ltmp6           #   Call between .Ltmp6 and .Ltmp8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	1                       #   On action: 1
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Lfunc_end1-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN8NArchive5NLzma8CDecoderD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CDecoderD2Ev,@function
_ZN8NArchive5NLzma8CDecoderD2Ev:        # @_ZN8NArchive5NLzma8CDecoderD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi11:
	.cfi_def_cfa_offset 32
.Lcfi12:
	.cfi_offset %rbx, -24
.Lcfi13:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	cmpq	$0, 8(%rbx)
	je	.LBB3_2
# BB#1:
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp11:
	callq	*88(%rax)
.Ltmp12:
.LBB3_2:                                # %_ZN8NArchive5NLzma8CDecoder15ReleaseInStreamEv.exit
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp16:
	callq	*16(%rax)
.Ltmp17:
.LBB3_4:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_5
# BB#13:
	movq	(%rdi), %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*16(%rax)               # TAILCALL
.LBB3_5:                                # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB3_8:
.Ltmp18:
	movq	%rax, %r14
	jmp	.LBB3_9
.LBB3_6:
.Ltmp13:
	movq	%rax, %r14
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_9
# BB#7:
	movq	(%rdi), %rax
.Ltmp14:
	callq	*16(%rax)
.Ltmp15:
.LBB3_9:                                # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit5
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_11
# BB#10:
	movq	(%rdi), %rax
.Ltmp19:
	callq	*16(%rax)
.Ltmp20:
.LBB3_11:                               # %_ZN9CMyComPtrI14ICompressCoderED2Ev.exit7
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_12:
.Ltmp21:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN8NArchive5NLzma8CDecoderD2Ev, .Lfunc_end3-_ZN8NArchive5NLzma8CDecoderD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp16-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin1   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp17         #   Call between .Ltmp17 and .Ltmp14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp14         #   Call between .Ltmp14 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin1   #     jumps to .Ltmp21
	.byte	1                       #   On action: 1
	.long	.Ltmp20-.Lfunc_begin1   # >> Call Site 5 <<
	.long	.Lfunc_end3-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive5NLzma8CDecoder4CodeERKNS0_7CHeaderEP20ISequentialOutStreamP21ICompressProgressInfo
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CDecoder4CodeERKNS0_7CHeaderEP20ISequentialOutStreamP21ICompressProgressInfo,@function
_ZN8NArchive5NLzma8CDecoder4CodeERKNS0_7CHeaderEP20ISequentialOutStreamP21ICompressProgressInfo: # @_ZN8NArchive5NLzma8CDecoder4CodeERKNS0_7CHeaderEP20ISequentialOutStreamP21ICompressProgressInfo
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 80
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movl	$-2147467263, %ebp      # imm = 0x80004001
	cmpb	$1, 8(%rbx)
	ja	.LBB4_21
# BB#1:
	movq	$0, 16(%rsp)
	movq	8(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp22:
	leaq	16(%rsp), %rdx
	movl	$IID_ICompressSetDecoderProperties2, %esi
	callq	*(%rax)
.Ltmp23:
# BB#2:                                 # %_ZNK9CMyComPtrI14ICompressCoderE14QueryInterfaceI30ICompressSetDecoderProperties2EEiRK4GUIDPPT_.exit
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#3:
	movq	(%rdi), %rax
	leaq	9(%rbx), %rsi
.Ltmp24:
	movl	$5, %edx
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp25:
# BB#4:
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_6
# BB#5:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB4_6:                                # %_ZN9CMyComPtrI30ICompressSetDecoderProperties2ED2Ev.exit70
	testl	%ebp, %ebp
	jne	.LBB4_21
# BB#7:
	movq	$0, (%rsp)
	movb	8(%rbx), %r13b
	cmpb	$1, %r13b
	jne	.LBB4_16
# BB#8:
	movq	16(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp29:
	movq	%rsp, %rdx
	movl	$IID_ICompressSetOutStream, %esi
	callq	*(%rax)
.Ltmp30:
# BB#9:                                 # %_ZNK9CMyComPtrI20ISequentialOutStreamE14QueryInterfaceI21ICompressSetOutStreamEEiRK4GUIDPPT_.exit
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_10
# BB#13:
	movq	(%rdi), %rax
.Ltmp31:
	movq	%r12, %rsi
	callq	*40(%rax)
	movl	%eax, %ebp
.Ltmp32:
# BB#14:
	testl	%ebp, %ebp
	jne	.LBB4_19
# BB#15:
	movq	16(%r15), %r12
.LBB4_16:
	xorl	%edx, %edx
	cmpq	$-1, (%rbx)
	cmovneq	%rbx, %rdx
	movq	(%r15), %rdi
.Ltmp34:
	movq	%r12, %rsi
	movq	%r14, %rcx
	callq	_ZN9NCompress5NLzma8CDecoder10CodeResumeEP20ISequentialOutStreamPKyP21ICompressProgressInfo
	movl	%eax, %ebx
.Ltmp35:
# BB#17:
	cmpb	$1, %r13b
	jne	.LBB4_18
# BB#22:
	movq	$0, 8(%rsp)
	movq	16(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp37:
	leaq	8(%rsp), %rdx
	movl	$IID_IOutStreamFlush, %esi
	callq	*(%rax)
.Ltmp38:
# BB#23:                                # %_ZNK9CMyComPtrI20ISequentialOutStreamE14QueryInterfaceI15IOutStreamFlushEEiRK4GUIDPPT_.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_26
# BB#24:
	movq	(%rdi), %rax
.Ltmp39:
	callq	*40(%rax)
.Ltmp40:
# BB#25:
	testl	%ebx, %ebx
	cmovel	%eax, %ebx
.LBB4_26:
	movq	(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp41:
	callq	*48(%rax)
	movl	%eax, %ebp
.Ltmp42:
# BB#27:
	testl	%ebx, %ebx
	cmovnel	%ebx, %ebp
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_19
# BB#28:
	movq	(%rdi), %rax
.Ltmp46:
	callq	*16(%rax)
.Ltmp47:
	jmp	.LBB4_19
.LBB4_18:
	movl	%ebx, %ebp
.LBB4_19:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_21
# BB#20:
	movq	(%rdi), %rax
	callq	*16(%rax)
	jmp	.LBB4_21
.LBB4_10:
	movl	$-2147467263, %ebp      # imm = 0x80004001
.LBB4_21:
	movl	%ebp, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_32:
.Ltmp48:
	jmp	.LBB4_34
.LBB4_29:
.Ltmp36:
	jmp	.LBB4_34
.LBB4_30:
.Ltmp43:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_35
# BB#31:
	movq	(%rdi), %rax
.Ltmp44:
	callq	*16(%rax)
.Ltmp45:
	jmp	.LBB4_35
.LBB4_33:
.Ltmp33:
.LBB4_34:
	movq	%rax, %rbx
.LBB4_35:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_37
# BB#36:
	movq	(%rdi), %rax
.Ltmp49:
	callq	*16(%rax)
.Ltmp50:
	jmp	.LBB4_37
.LBB4_11:
.Ltmp26:
	movq	%rax, %rbx
	movq	16(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_37
# BB#12:
	movq	(%rdi), %rax
.Ltmp27:
	callq	*16(%rax)
.Ltmp28:
.LBB4_37:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB4_38:
.Ltmp51:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end4:
	.size	_ZN8NArchive5NLzma8CDecoder4CodeERKNS0_7CHeaderEP20ISequentialOutStreamP21ICompressProgressInfo, .Lfunc_end4-_ZN8NArchive5NLzma8CDecoder4CodeERKNS0_7CHeaderEP20ISequentialOutStreamP21ICompressProgressInfo
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp22-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp25-.Ltmp22         #   Call between .Ltmp22 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin2   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp29-.Ltmp25         #   Call between .Ltmp25 and .Ltmp29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp32-.Ltmp29         #   Call between .Ltmp29 and .Ltmp32
	.long	.Ltmp33-.Lfunc_begin2   #     jumps to .Ltmp33
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin2   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp42-.Ltmp37         #   Call between .Ltmp37 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin2   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp46-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp47-.Ltmp46         #   Call between .Ltmp46 and .Ltmp47
	.long	.Ltmp48-.Lfunc_begin2   #     jumps to .Ltmp48
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Ltmp44-.Ltmp47         #   Call between .Ltmp47 and .Ltmp44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin2   # >> Call Site 8 <<
	.long	.Ltmp28-.Ltmp44         #   Call between .Ltmp44 and .Ltmp28
	.long	.Ltmp51-.Lfunc_begin2   #     jumps to .Ltmp51
	.byte	1                       #   On action: 1
	.long	.Ltmp28-.Lfunc_begin2   # >> Call Site 9 <<
	.long	.Lfunc_end4-.Ltmp28     #   Call between .Ltmp28 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive5NLzma8CHandler21GetNumberOfPropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler21GetNumberOfPropertiesEPj,@function
_ZN8NArchive5NLzma8CHandler21GetNumberOfPropertiesEPj: # @_ZN8NArchive5NLzma8CHandler21GetNumberOfPropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$3, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZN8NArchive5NLzma8CHandler21GetNumberOfPropertiesEPj, .Lfunc_end5-_ZN8NArchive5NLzma8CHandler21GetNumberOfPropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler15GetPropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler15GetPropertyInfoEjPPwPjPt,@function
_ZN8NArchive5NLzma8CHandler15GetPropertyInfoEjPPwPjPt: # @_ZN8NArchive5NLzma8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147024809, %eax      # imm = 0x80070057
	cmpl	$2, %esi
	ja	.LBB6_2
# BB#1:
	movl	%esi, %eax
	shlq	$4, %rax
	movl	_ZN8NArchive5NLzma6kPropsE+8(%rax), %esi
	movl	%esi, (%rcx)
	movzwl	_ZN8NArchive5NLzma6kPropsE+12(%rax), %eax
	movw	%ax, (%r8)
	movq	$0, (%rdx)
	xorl	%eax, %eax
.LBB6_2:
	retq
.Lfunc_end6:
	.size	_ZN8NArchive5NLzma8CHandler15GetPropertyInfoEjPPwPjPt, .Lfunc_end6-_ZN8NArchive5NLzma8CHandler15GetPropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler28GetNumberOfArchivePropertiesEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler28GetNumberOfArchivePropertiesEPj,@function
_ZN8NArchive5NLzma8CHandler28GetNumberOfArchivePropertiesEPj: # @_ZN8NArchive5NLzma8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_startproc
# BB#0:
	movl	$0, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end7:
	.size	_ZN8NArchive5NLzma8CHandler28GetNumberOfArchivePropertiesEPj, .Lfunc_end7-_ZN8NArchive5NLzma8CHandler28GetNumberOfArchivePropertiesEPj
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler22GetArchivePropertyInfoEjPPwPjPt,@function
_ZN8NArchive5NLzma8CHandler22GetArchivePropertyInfoEjPPwPjPt: # @_ZN8NArchive5NLzma8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_startproc
# BB#0:
	movl	$-2147467263, %eax      # imm = 0x80004001
	retq
.Lfunc_end8:
	.size	_ZN8NArchive5NLzma8CHandler22GetArchivePropertyInfoEjPPwPjPt, .Lfunc_end8-_ZN8NArchive5NLzma8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler18GetArchivePropertyEjP14tagPROPVARIANT,@function
_ZN8NArchive5NLzma8CHandler18GetArchivePropertyEjP14tagPROPVARIANT: # @_ZN8NArchive5NLzma8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	movl	$0, (%rsp)
	cmpl	$44, %esi
	jne	.LBB9_3
# BB#1:
	cmpb	$0, 64(%rdi)
	je	.LBB9_3
# BB#2:
	movq	56(%rdi), %rsi
.Ltmp52:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp53:
.LBB9_3:
.Ltmp54:
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp55:
# BB#4:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$16, %rsp
	popq	%rbx
	retq
.LBB9_5:
.Ltmp56:
	movq	%rax, %rbx
.Ltmp57:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp58:
# BB#6:                                 # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_7:
.Ltmp59:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN8NArchive5NLzma8CHandler18GetArchivePropertyEjP14tagPROPVARIANT, .Lfunc_end9-_ZN8NArchive5NLzma8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp52-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp55-.Ltmp52         #   Call between .Ltmp52 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin3   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp55-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp57-.Ltmp55         #   Call between .Ltmp55 and .Ltmp57
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin3   #     jumps to .Ltmp59
	.byte	1                       #   On action: 1
	.long	.Ltmp58-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Lfunc_end9-.Ltmp58     #   Call between .Ltmp58 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive5NLzma8CHandler16GetNumberOfItemsEPj
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler16GetNumberOfItemsEPj,@function
_ZN8NArchive5NLzma8CHandler16GetNumberOfItemsEPj: # @_ZN8NArchive5NLzma8CHandler16GetNumberOfItemsEPj
	.cfi_startproc
# BB#0:
	movl	$1, (%rsi)
	xorl	%eax, %eax
	retq
.Lfunc_end10:
	.size	_ZN8NArchive5NLzma8CHandler16GetNumberOfItemsEPj, .Lfunc_end10-_ZN8NArchive5NLzma8CHandler16GetNumberOfItemsEPj
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler11GetPropertyEjjP14tagPROPVARIANT,@function
_ZN8NArchive5NLzma8CHandler11GetPropertyEjjP14tagPROPVARIANT: # @_ZN8NArchive5NLzma8CHandler11GetPropertyEjjP14tagPROPVARIANT
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 144
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rcx, %r12
	movl	$0, (%rsp)
	cmpl	$22, %edx
	je	.LBB11_10
# BB#1:
	cmpl	$8, %edx
	je	.LBB11_6
# BB#2:
	cmpl	$7, %edx
	jne	.LBB11_8
# BB#3:
	cmpq	$0, 72(%rdi)
	je	.LBB11_8
# BB#4:
	movq	24(%rdi), %rsi
	cmpq	$-1, %rsi
	je	.LBB11_8
# BB#5:
.Ltmp69:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp70:
	jmp	.LBB11_8
.LBB11_6:
	cmpb	$0, 64(%rdi)
	je	.LBB11_8
# BB#7:
	movq	56(%rdi), %rsi
.Ltmp67:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEy
.Ltmp68:
	jmp	.LBB11_8
.LBB11_10:
	cmpq	$0, 72(%rdi)
	je	.LBB11_8
# BB#11:
	movb	$0, 16(%rsp)
	cmpb	$0, 32(%rdi)
	je	.LBB11_12
# BB#21:                                # %_Z11MyStringLenIcEiPKT_.exit.i9
	movl	$541737794, 16(%rsp)    # imm = 0x204A4342
	movb	$0, 20(%rsp)
.LBB11_12:                              # %_ZN8NArchive5NLzmaL8MyStrCatEPcPKc.exit.preheader
	movabsq	$-4294967296, %r15      # imm = 0xFFFFFFFF00000000
	leaq	16(%rsp), %rcx
	movabsq	$4294967296, %rbx       # imm = 0x100000000
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB11_13:                              # %_ZN8NArchive5NLzmaL8MyStrCatEPcPKc.exit
                                        # =>This Inner Loop Header: Depth=1
	addq	%rbx, %rax
	cmpb	$0, (%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB11_13
# BB#14:                                # %_Z11MyStringLenIcEiPKT_.exit.i13
	sarq	$32, %rax
	movl	$1095588428, 16(%rsp,%rax) # imm = 0x414D5A4C
	movw	$58, 20(%rsp,%rax)
	movl	34(%rdi), %edi
	movl	$-1, %ebp
	leaq	16(%rsp), %rcx
	movq	%r15, %rax
	.p2align	4, 0x90
.LBB11_15:                              # =>This Inner Loop Header: Depth=1
	incl	%ebp
	addq	%rbx, %rax
	cmpb	$0, (%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB11_15
# BB#16:                                # %_Z11MyStringLenIcEiPKT_.exit
	sarq	$32, %rax
	leaq	16(%rsp,%rax), %r14
	cmpl	$32767, %edi            # imm = 0x7FFF
	jg	.LBB11_39
# BB#17:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$127, %edi
	jg	.LBB11_24
# BB#18:                                # %_Z11MyStringLenIcEiPKT_.exit
	leal	-1(%rdi), %ecx
	cmpl	$63, %ecx
	ja	.LBB11_19
# BB#23:                                # %_Z11MyStringLenIcEiPKT_.exit
	xorl	%eax, %eax
	jmpq	*.LJTI11_0(,%rcx,8)
.LBB11_70:                              # %.critedge.fold.split.i
	movl	$1, %eax
	jmp	.LBB11_88
.LBB11_39:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$8388607, %edi          # imm = 0x7FFFFF
	jg	.LBB11_55
# BB#40:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$524287, %edi           # imm = 0x7FFFF
	jg	.LBB11_48
# BB#41:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$131071, %edi           # imm = 0x1FFFF
	jg	.LBB11_45
# BB#42:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$32768, %edi            # imm = 0x8000
	je	.LBB11_80
# BB#43:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$65536, %edi            # imm = 0x10000
	jne	.LBB11_97
# BB#44:                                # %.critedge.fold.split41.i
	movl	$16, %eax
	jmp	.LBB11_88
.LBB11_24:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$2047, %edi             # imm = 0x7FF
	jg	.LBB11_32
# BB#25:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$511, %edi              # imm = 0x1FF
	jg	.LBB11_29
# BB#26:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$128, %edi
	je	.LBB11_76
# BB#27:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$256, %edi              # imm = 0x100
	jne	.LBB11_97
# BB#28:                                # %.critedge.fold.split33.i
	movl	$8, %eax
	jmp	.LBB11_88
.LBB11_55:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$134217727, %edi        # imm = 0x7FFFFFF
	jg	.LBB11_63
# BB#56:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$33554431, %edi         # imm = 0x1FFFFFF
	jg	.LBB11_60
# BB#57:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$8388608, %edi          # imm = 0x800000
	je	.LBB11_84
# BB#58:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$16777216, %edi         # imm = 0x1000000
	jne	.LBB11_97
# BB#59:                                # %.critedge.fold.split49.i
	movl	$24, %eax
	jmp	.LBB11_88
.LBB11_48:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$2097151, %edi          # imm = 0x1FFFFF
	jg	.LBB11_52
# BB#49:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$524288, %edi           # imm = 0x80000
	je	.LBB11_82
# BB#50:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$1048576, %edi          # imm = 0x100000
	jne	.LBB11_97
# BB#51:                                # %.critedge.fold.split45.i
	movl	$20, %eax
	jmp	.LBB11_88
.LBB11_32:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$8191, %edi             # imm = 0x1FFF
	jg	.LBB11_36
# BB#33:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$2048, %edi             # imm = 0x800
	je	.LBB11_78
# BB#34:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$4096, %edi             # imm = 0x1000
	jne	.LBB11_97
# BB#35:                                # %.critedge.fold.split37.i
	movl	$12, %eax
	jmp	.LBB11_88
.LBB11_63:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$536870911, %edi        # imm = 0x1FFFFFFF
	jg	.LBB11_67
# BB#64:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$134217728, %edi        # imm = 0x8000000
	je	.LBB11_86
# BB#65:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$268435456, %edi        # imm = 0x10000000
	jne	.LBB11_97
# BB#66:                                # %.critedge.fold.split53.i
	movl	$28, %eax
	jmp	.LBB11_88
.LBB11_45:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$131072, %edi           # imm = 0x20000
	je	.LBB11_81
# BB#46:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$262144, %edi           # imm = 0x40000
	jne	.LBB11_97
# BB#47:                                # %.critedge.fold.split43.i
	movl	$18, %eax
	jmp	.LBB11_88
.LBB11_29:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$512, %edi              # imm = 0x200
	je	.LBB11_77
# BB#30:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$1024, %edi             # imm = 0x400
	jne	.LBB11_97
# BB#31:                                # %.critedge.fold.split35.i
	movl	$10, %eax
	jmp	.LBB11_88
.LBB11_60:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$33554432, %edi         # imm = 0x2000000
	je	.LBB11_85
# BB#61:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$67108864, %edi         # imm = 0x4000000
	jne	.LBB11_97
# BB#62:                                # %.critedge.fold.split51.i
	movl	$26, %eax
	jmp	.LBB11_88
.LBB11_52:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$2097152, %edi          # imm = 0x200000
	je	.LBB11_83
# BB#53:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$4194304, %edi          # imm = 0x400000
	jne	.LBB11_97
# BB#54:                                # %.critedge.fold.split47.i
	movl	$22, %eax
	jmp	.LBB11_88
.LBB11_36:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$8192, %edi             # imm = 0x2000
	je	.LBB11_79
# BB#37:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$16384, %edi            # imm = 0x4000
	jne	.LBB11_97
# BB#38:                                # %.critedge.fold.split39.i
	movl	$14, %eax
	jmp	.LBB11_88
.LBB11_67:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$536870912, %edi        # imm = 0x20000000
	je	.LBB11_87
# BB#68:                                # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$1073741824, %edi       # imm = 0x40000000
	jne	.LBB11_97
# BB#69:                                # %.critedge.fold.split55.i
	movl	$30, %eax
	jmp	.LBB11_88
.LBB11_19:                              # %_Z11MyStringLenIcEiPKT_.exit
	cmpl	$-2147483648, %edi      # imm = 0x80000000
	jne	.LBB11_97
# BB#20:                                # %.critedge.fold.split56.i
	movl	$31, %eax
	jmp	.LBB11_88
.LBB11_71:                              # %.critedge.fold.split27.i
	movl	$2, %eax
	jmp	.LBB11_88
.LBB11_72:                              # %.critedge.fold.split28.i
	movl	$3, %eax
	jmp	.LBB11_88
.LBB11_74:                              # %.critedge.fold.split30.i
	movl	$5, %eax
	jmp	.LBB11_88
.LBB11_73:                              # %.critedge.fold.split29.i
	movl	$4, %eax
	jmp	.LBB11_88
.LBB11_75:                              # %.critedge.fold.split31.i
	movl	$6, %eax
	jmp	.LBB11_88
.LBB11_97:
	testl	$1048575, %edi          # imm = 0xFFFFF
	je	.LBB11_98
# BB#90:
	movl	%edi, %eax
	shrl	$10, %eax
	movl	%edi, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movb	$107, %r13b
	je	.LBB11_92
# BB#91:
	movb	$98, %r13b
.LBB11_92:
	testl	%ecx, %ecx
	cmovel	%eax, %edi
	jmp	.LBB11_93
.LBB11_80:                              # %.critedge.fold.split40.i
	movl	$15, %eax
	jmp	.LBB11_88
.LBB11_76:                              # %.critedge.fold.split32.i
	movl	$7, %eax
	jmp	.LBB11_88
.LBB11_84:                              # %.critedge.fold.split48.i
	movl	$23, %eax
	jmp	.LBB11_88
.LBB11_82:                              # %.critedge.fold.split44.i
	movl	$19, %eax
	jmp	.LBB11_88
.LBB11_78:                              # %.critedge.fold.split36.i
	movl	$11, %eax
	jmp	.LBB11_88
.LBB11_86:                              # %.critedge.fold.split52.i
	movl	$27, %eax
	jmp	.LBB11_88
.LBB11_81:                              # %.critedge.fold.split42.i
	movl	$17, %eax
	jmp	.LBB11_88
.LBB11_77:                              # %.critedge.fold.split34.i
	movl	$9, %eax
	jmp	.LBB11_88
.LBB11_85:                              # %.critedge.fold.split50.i
	movl	$25, %eax
	jmp	.LBB11_88
.LBB11_83:                              # %.critedge.fold.split46.i
	movl	$21, %eax
	jmp	.LBB11_88
.LBB11_79:                              # %.critedge.fold.split38.i
	movl	$13, %eax
	jmp	.LBB11_88
.LBB11_87:                              # %.critedge.fold.split54.i
	movl	$29, %eax
.LBB11_88:                              # %.critedge.i
.Ltmp60:
	movl	%eax, %edi
	movq	%r14, %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp61:
.LBB11_89:                              # %_ZN8NArchive5NLzmaL16DictSizeToStringEjPc.exit
.Ltmp64:
	movq	%rsp, %rdi
	leaq	16(%rsp), %rsi
	callq	_ZN8NWindows4NCOM12CPropVariantaSEPKc
.Ltmp65:
.LBB11_8:
.Ltmp71:
	movq	%rsp, %rdi
	movq	%r12, %rsi
	callq	_ZN8NWindows4NCOM12CPropVariant6DetachEP14tagPROPVARIANT
.Ltmp72:
# BB#9:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
	xorl	%eax, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_98:
	shrl	$20, %edi
	movb	$109, %r13b
.LBB11_93:
.Ltmp62:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movq	%r14, %rsi
	callq	_Z21ConvertUInt32ToStringjPc
.Ltmp63:
# BB#94:                                # %.noexc5.preheader
	movslq	%ebp, %rax
	leaq	16(%rsp,%rax), %rcx
	xorl	%eax, %eax
.LBB11_95:                              # %.noexc5
                                        # =>This Inner Loop Header: Depth=1
	addq	%rbx, %rax
	cmpb	$0, (%rcx)
	leaq	1(%rcx), %rcx
	jne	.LBB11_95
# BB#96:                                # %_Z11MyStringLenIcEiPKT_.exit.i
	addq	%rax, %r15
	sarq	$32, %r15
	movb	%r13b, (%r14,%r15)
	sarq	$32, %rax
	movb	$0, (%r14,%rax)
	jmp	.LBB11_89
.LBB11_22:
.Ltmp66:
	jmp	.LBB11_100
.LBB11_99:
.Ltmp73:
.LBB11_100:
	movq	%rax, %rbx
.Ltmp74:
	movq	%rsp, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp75:
# BB#101:                               # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB11_102:
.Ltmp76:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN8NArchive5NLzma8CHandler11GetPropertyEjjP14tagPROPVARIANT, .Lfunc_end11-_ZN8NArchive5NLzma8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI11_0:
	.quad	.LBB11_88
	.quad	.LBB11_70
	.quad	.LBB11_97
	.quad	.LBB11_71
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_72
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_73
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_74
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_97
	.quad	.LBB11_75
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp69-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp68-.Ltmp69         #   Call between .Ltmp69 and .Ltmp68
	.long	.Ltmp73-.Lfunc_begin4   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp65-.Ltmp60         #   Call between .Ltmp60 and .Ltmp65
	.long	.Ltmp66-.Lfunc_begin4   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin4   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp72-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp62-.Ltmp72         #   Call between .Ltmp72 and .Ltmp62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp66-.Lfunc_begin4   #     jumps to .Ltmp66
	.byte	0                       #   On action: cleanup
	.long	.Ltmp74-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp75-.Ltmp74         #   Call between .Ltmp74 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin4   #     jumps to .Ltmp76
	.byte	1                       #   On action: 1
	.long	.Ltmp75-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Lfunc_end11-.Ltmp75    #   Call between .Ltmp75 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN8NArchive5NLzma8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback,@function
_ZN8NArchive5NLzma8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback: # @_ZN8NArchive5NLzma8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 48
.Lcfi46:
	.cfi_offset %rbx, -24
.Lcfi47:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%rbx), %rax
	leaq	48(%r14), %rcx
	xorl	%esi, %esi
	movl	$1, %edx
	movq	%rbx, %rdi
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB12_25
# BB#1:
	leaq	1(%rsp), %rsi
	movl	$15, %edx
	movq	%rbx, %rdi
	callq	_Z16ReadStream_FALSEP19ISequentialInStreamPvm
	testl	%eax, %eax
	je	.LBB12_2
.LBB12_25:
	addq	$24, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB12_2:
	movzbl	40(%r14), %edx
	testq	%rdx, %rdx
	movb	$0, 32(%r14)
	je	.LBB12_3
# BB#4:
	movb	1(%rsp), %al
	movb	%al, 32(%r14)
	cmpb	$2, %al
	setb	%sil
	jmp	.LBB12_5
.LBB12_3:
	movb	$1, %sil
.LBB12_5:
	movb	1(%rsp,%rdx), %cl
	movb	%cl, 33(%r14)
	movb	2(%rsp,%rdx), %al
	movb	%al, 34(%r14)
	movb	3(%rsp,%rdx), %al
	movb	%al, 35(%r14)
	movb	4(%rsp,%rdx), %al
	movb	%al, 36(%r14)
	movb	5(%rsp,%rdx), %al
	movb	%al, 37(%r14)
	movq	6(%rsp,%rdx), %rdi
	movq	%rdi, 24(%r14)
	movl	$1, %eax
	testb	%sil, %sil
	je	.LBB12_25
# BB#6:
	cmpb	$-32, %cl
	ja	.LBB12_25
# BB#7:
	cmpq	$-1, %rdi
	je	.LBB12_9
# BB#8:
	shrq	$56, %rdi
	jne	.LBB12_25
.LBB12_9:
	movl	34(%r14), %esi
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB12_10:                              # =>This Inner Loop Header: Depth=1
	movl	$2, %edi
	shll	%cl, %edi
	movl	$3, %eax
	shll	%cl, %eax
	cmpl	%edi, %esi
	je	.LBB12_18
# BB#11:                                #   in Loop: Header=BB12_10 Depth=1
	cmpl	%eax, %esi
	je	.LBB12_18
# BB#12:                                #   in Loop: Header=BB12_10 Depth=1
	incl	%ecx
	movl	$2, %edi
	shll	%cl, %edi
	movl	$3, %eax
	shll	%cl, %eax
	cmpl	%edi, %esi
	je	.LBB12_18
# BB#13:                                #   in Loop: Header=BB12_10 Depth=1
	cmpl	%eax, %esi
	je	.LBB12_18
# BB#14:                                #   in Loop: Header=BB12_10 Depth=1
	incl	%ecx
	movl	$2, %edi
	shll	%cl, %edi
	movl	$3, %eax
	shll	%cl, %eax
	cmpl	%edi, %esi
	je	.LBB12_18
# BB#15:                                #   in Loop: Header=BB12_10 Depth=1
	cmpl	%eax, %esi
	je	.LBB12_18
# BB#16:                                #   in Loop: Header=BB12_10 Depth=1
	incl	%ecx
	cmpl	$31, %ecx
	jl	.LBB12_10
# BB#17:                                # %_ZN8NArchive5NLzma7CHeader5ParseEPKhb.exit
	movl	$1, %eax
	cmpl	$-1, %esi
	jne	.LBB12_25
.LBB12_18:                              # %_ZN8NArchive5NLzma7CHeader5ParseEPKhb.exit.thread32
	movl	$1, %eax
	cmpb	$0, 14(%rsp,%rdx)
	jne	.LBB12_25
# BB#19:
	movq	(%rbx), %rax
	leaq	16(%rsp), %rcx
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB12_25
# BB#20:
	movq	16(%rsp), %rax
	subq	48(%r14), %rax
	movq	%rax, 56(%r14)
	movb	$1, 64(%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	72(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_22
# BB#21:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB12_22:
	movq	%rbx, 72(%r14)
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	movq	80(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB12_24
# BB#23:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB12_24:                              # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%rbx, 80(%r14)
	xorl	%eax, %eax
	jmp	.LBB12_25
.Lfunc_end12:
	.size	_ZN8NArchive5NLzma8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback, .Lfunc_end12-_ZN8NArchive5NLzma8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream: # @_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi50:
	.cfi_def_cfa_offset 32
.Lcfi51:
	.cfi_offset %rbx, -24
.Lcfi52:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB13_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB13_2:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB13_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB13_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamEaSEPS0_.exit
	movq	%r14, 80(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end13-_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZThn8_N8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream,@function
_ZThn8_N8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream: # @_ZThn8_N8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -24
.Lcfi57:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movq	-8(%rbx), %rax
	addq	$-8, %rbx
	movq	%rbx, %rdi
	callq	*48(%rax)
	testq	%r14, %r14
	je	.LBB14_2
# BB#1:
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*8(%rax)
.LBB14_2:
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
.LBB14_4:                               # %_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream.exit
	movq	%r14, 80(%rbx)
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end14:
	.size	_ZThn8_N8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream, .Lfunc_end14-_ZThn8_N8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler5CloseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler5CloseEv,@function
_ZN8NArchive5NLzma8CHandler5CloseEv:    # @_ZN8NArchive5NLzma8CHandler5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 16
.Lcfi59:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$0, 64(%rbx)
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_2
# BB#1:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 72(%rbx)
.LBB15_2:                               # %_ZN9CMyComPtrI9IInStreamE7ReleaseEv.exit
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB15_4
# BB#3:
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 80(%rbx)
.LBB15_4:                               # %_ZN9CMyComPtrI19ISequentialInStreamE7ReleaseEv.exit
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end15:
	.size	_ZN8NArchive5NLzma8CHandler5CloseEv, .Lfunc_end15-_ZN8NArchive5NLzma8CHandler5CloseEv
	.cfi_endproc

	.globl	_ZN8NArchive5NLzma8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler7ExtractEPKjjiP23IArchiveExtractCallback,@function
_ZN8NArchive5NLzma8CHandler7ExtractEPKjjiP23IArchiveExtractCallback: # @_ZN8NArchive5NLzma8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi66:
	.cfi_def_cfa_offset 144
.Lcfi67:
	.cfi_offset %rbx, -56
.Lcfi68:
	.cfi_offset %r12, -48
.Lcfi69:
	.cfi_offset %r13, -40
.Lcfi70:
	.cfi_offset %r14, -32
.Lcfi71:
	.cfi_offset %r15, -24
.Lcfi72:
	.cfi_offset %rbp, -16
	movq	%r8, %r13
	movl	%ecx, %ebx
	movq	%rdi, %r12
	cmpl	$-1, %edx
	je	.LBB16_6
# BB#1:
	testl	%edx, %edx
	je	.LBB16_2
# BB#3:
	cmpl	$1, %edx
	jne	.LBB16_5
# BB#4:
	cmpl	$0, (%rsi)
	je	.LBB16_6
.LBB16_5:
	movl	$-2147024809, %r14d     # imm = 0x80070057
	jmp	.LBB16_122
.LBB16_6:
	cmpq	$0, 72(%r12)
	je	.LBB16_8
# BB#7:
	movq	(%r13), %rax
	movq	56(%r12), %rsi
.Ltmp77:
	movq	%r13, %rdi
	callq	*40(%rax)
.Ltmp78:
.LBB16_8:
	movq	$0, 8(%rsp)
	xorl	%ebp, %ebp
	testl	%ebx, %ebx
	setne	%bpl
	movq	(%r13), %rax
.Ltmp80:
	leaq	8(%rsp), %rdx
	xorl	%esi, %esi
	movq	%r13, %rdi
	movl	%ebp, %ecx
	callq	*56(%rax)
	movl	%eax, %r14d
.Ltmp81:
# BB#9:
	testl	%r14d, %r14d
	je	.LBB16_10
.LBB16_92:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit142
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_122
# BB#93:
	movq	(%rdi), %rax
.Ltmp166:
	callq	*16(%rax)
.Ltmp167:
	jmp	.LBB16_122
.LBB16_2:
	xorl	%r14d, %r14d
	jmp	.LBB16_122
.LBB16_10:
	testl	%ebx, %ebx
	sete	%al
	cmpq	$0, 8(%rsp)
	jne	.LBB16_12
# BB#11:
	xorl	%r14d, %r14d
	testb	%al, %al
	je	.LBB16_12
.LBB16_122:
	movl	%r14d, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_12:
	movq	(%r13), %rax
.Ltmp82:
	movq	%r13, %rdi
	movl	%ebp, %esi
	callq	*64(%rax)
.Ltmp83:
# BB#13:
.Ltmp84:
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp85:
# BB#14:
	movl	$0, 8(%rbx)
	movq	$_ZTV15CDummyOutStream+16, (%rbx)
	movq	$0, 16(%rbx)
.Ltmp87:
	movq	%rbx, %rdi
	callq	*_ZTV15CDummyOutStream+24(%rip)
.Ltmp88:
# BB#15:                                # %_ZN9CMyComPtrI20ISequentialOutStreamEC2EPS0_.exit
	movq	8(%rsp), %rbp
	testq	%rbp, %rbp
	je	.LBB16_17
# BB#16:
	movq	(%rbp), %rax
.Ltmp90:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp91:
.LBB16_17:                              # %.noexc143
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB16_19
# BB#18:
	movq	(%rdi), %rax
.Ltmp92:
	callq	*16(%rax)
.Ltmp93:
.LBB16_19:
	movq	%rbp, 16(%rbx)
	movq	$0, 24(%rbx)
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_22
# BB#20:
	movq	(%rdi), %rax
.Ltmp94:
	callq	*16(%rax)
.Ltmp95:
# BB#21:                                # %.noexc145
	movq	$0, 8(%rsp)
.LBB16_22:                              # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit
.Ltmp96:
	movl	$72, %edi
	callq	_Znwm
	movq	%rax, %r15
.Ltmp97:
# BB#23:
.Ltmp99:
	movq	%r15, %rdi
	callq	_ZN14CLocalProgressC1Ev
.Ltmp100:
# BB#24:
	movq	(%r15), %rax
.Ltmp102:
	movq	%r15, %rdi
	callq	*8(%rax)
.Ltmp103:
# BB#25:                                # %_ZN9CMyComPtrI21ICompressProgressInfoEC2EPS0_.exit
.Ltmp105:
	movl	$1, %edx
	movq	%r15, %rdi
	movq	%r13, %rsi
	callq	_ZN14CLocalProgress4InitEP9IProgressb
.Ltmp106:
# BB#26:
	movq	72(%r12), %rdi
	testq	%rdi, %rdi
	je	.LBB16_29
# BB#27:
	movq	(%rdi), %rax
	movq	48(%r12), %rsi
.Ltmp107:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	movl	%eax, %r14d
.Ltmp108:
# BB#28:
	testl	%r14d, %r14d
	jne	.LBB16_89
.LBB16_29:
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rsp)
	movq	80(%r12), %rdx
	movzbl	40(%r12), %esi
.Ltmp110:
	leaq	16(%rsp), %rdi
	callq	_ZN8NArchive5NLzma8CDecoder6CreateEbP19ISequentialInStream
	movl	%eax, %r14d
.Ltmp111:
# BB#30:
	testl	%r14d, %r14d
	je	.LBB16_31
.LBB16_83:                              # %.loopexit183
	cmpq	$0, 24(%rsp)
	je	.LBB16_85
.LBB16_84:
	movq	16(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp139:
	callq	*88(%rax)
.Ltmp140:
.LBB16_85:                              # %_ZN8NArchive5NLzma8CDecoder15ReleaseInStreamEv.exit.i151
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_87
# BB#86:
	movq	(%rdi), %rax
.Ltmp144:
	callq	*16(%rax)
.Ltmp145:
.LBB16_87:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit.i152
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_89
# BB#88:
	movq	(%rdi), %rax
.Ltmp150:
	callq	*16(%rax)
.Ltmp151:
.LBB16_89:
	movq	(%r15), %rax
.Ltmp155:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp156:
# BB#90:                                # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit148
	testq	%rbx, %rbx
	je	.LBB16_92
# BB#91:
	movq	(%rbx), %rax
.Ltmp160:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp161:
	jmp	.LBB16_92
.LBB16_31:                              # %.preheader
	movb	$1, %al
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	jmp	.LBB16_32
.LBB16_70:                              # %_ZN8NArchive5NLzma7CHeader5ParseEPKhb.exit
                                        #   in Loop: Header=BB16_32 Depth=1
	cmpl	$-1, %eax
	jne	.LBB16_78
.LBB16_71:                              # %_ZN8NArchive5NLzma7CHeader5ParseEPKhb.exit.thread173
                                        #   in Loop: Header=BB16_32 Depth=1
.Ltmp118:
	leaq	16(%rsp), %rdi
	leaq	40(%rsp), %rsi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	_ZN8NArchive5NLzma8CDecoder4CodeERKNS0_7CHeaderEP20ISequentialOutStreamP21ICompressProgressInfo
	movl	%eax, %r14d
.Ltmp119:
# BB#72:                                #   in Loop: Header=BB16_32 Depth=1
	testl	%r14d, %r14d
	jne	.LBB16_73
# BB#80:                                #   in Loop: Header=BB16_32 Depth=1
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB16_32:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_63 Depth 2
	movq	24(%rbx), %rax
	movq	%rax, 56(%r15)
	movq	16(%rsp), %rax
	movq	232(%rax), %rax
	movq	%rax, 56(%r12)
	movq	%rax, 48(%r15)
	movb	$1, 64(%r12)
.Ltmp113:
	movq	%r15, %rdi
	callq	_ZN14CLocalProgress6SetCurEv
	movl	%eax, %r14d
.Ltmp114:
# BB#33:                                #   in Loop: Header=BB16_32 Depth=1
	testl	%r14d, %r14d
	jne	.LBB16_83
# BB#34:                                #   in Loop: Header=BB16_32 Depth=1
	movzbl	40(%r12), %ebp
	addl	$13, %ebp
	movq	16(%rsp), %rdi
.Ltmp116:
	leaq	62(%rsp), %rsi
	movl	%ebp, %edx
	leaq	76(%rsp), %rcx
	callq	_ZN9NCompress5NLzma8CDecoder19ReadFromInputStreamEPvjPj
	movl	%eax, %r14d
.Ltmp117:
# BB#35:                                # %_ZN8NArchive5NLzma8CDecoder9ReadInputEPhjPj.exit
                                        #   in Loop: Header=BB16_32 Depth=1
	testl	%r14d, %r14d
	jne	.LBB16_83
# BB#36:                                #   in Loop: Header=BB16_32 Depth=1
	xorl	%r14d, %r14d
	cmpl	%ebp, 76(%rsp)
	jne	.LBB16_78
# BB#37:                                #   in Loop: Header=BB16_32 Depth=1
	movzbl	40(%r12), %eax
	testq	%rax, %rax
	movb	$0, 48(%rsp)
	je	.LBB16_38
# BB#56:                                #   in Loop: Header=BB16_32 Depth=1
	movb	62(%rsp), %cl
	movb	%cl, 48(%rsp)
	cmpb	$2, %cl
	setb	%cl
	jmp	.LBB16_57
.LBB16_38:                              #   in Loop: Header=BB16_32 Depth=1
	movb	$1, %cl
.LBB16_57:                              #   in Loop: Header=BB16_32 Depth=1
	movb	62(%rsp,%rax), %dl
	movb	%dl, 49(%rsp)
	movb	63(%rsp,%rax), %bl
	movb	%bl, 50(%rsp)
	movb	64(%rsp,%rax), %bl
	movb	%bl, 51(%rsp)
	movb	65(%rsp,%rax), %bl
	movb	%bl, 52(%rsp)
	movb	66(%rsp,%rax), %bl
	movb	%bl, 53(%rsp)
	movq	67(%rsp,%rax), %rax
	movq	%rax, 40(%rsp)
	testb	%cl, %cl
	je	.LBB16_58
# BB#59:                                #   in Loop: Header=BB16_32 Depth=1
	cmpb	$-32, %dl
	movq	80(%rsp), %rbx          # 8-byte Reload
	ja	.LBB16_78
# BB#60:                                #   in Loop: Header=BB16_32 Depth=1
	cmpq	$-1, %rax
	je	.LBB16_62
# BB#61:                                #   in Loop: Header=BB16_32 Depth=1
	shrq	$56, %rax
	jne	.LBB16_78
.LBB16_62:                              #   in Loop: Header=BB16_32 Depth=1
	movl	$1, %ecx
	movl	50(%rsp), %eax
	.p2align	4, 0x90
.LBB16_63:                              #   Parent Loop BB16_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	$2, %esi
	shll	%cl, %esi
	movl	$3, %edx
	shll	%cl, %edx
	cmpl	%esi, %eax
	je	.LBB16_71
# BB#64:                                #   in Loop: Header=BB16_63 Depth=2
	cmpl	%edx, %eax
	je	.LBB16_71
# BB#65:                                #   in Loop: Header=BB16_63 Depth=2
	incl	%ecx
	movl	$2, %esi
	shll	%cl, %esi
	movl	$3, %edx
	shll	%cl, %edx
	cmpl	%esi, %eax
	je	.LBB16_71
# BB#66:                                #   in Loop: Header=BB16_63 Depth=2
	cmpl	%edx, %eax
	je	.LBB16_71
# BB#67:                                #   in Loop: Header=BB16_63 Depth=2
	incl	%ecx
	movl	$2, %esi
	shll	%cl, %esi
	movl	$3, %edx
	shll	%cl, %edx
	cmpl	%esi, %eax
	je	.LBB16_71
# BB#68:                                #   in Loop: Header=BB16_63 Depth=2
	cmpl	%edx, %eax
	je	.LBB16_71
# BB#69:                                #   in Loop: Header=BB16_63 Depth=2
	incl	%ecx
	cmpl	$31, %ecx
	jl	.LBB16_63
	jmp	.LBB16_70
.LBB16_73:
	cmpl	$-2147467263, %r14d     # imm = 0x80004001
	je	.LBB16_74
# BB#75:
	cmpl	$1, %r14d
	jne	.LBB16_83
# BB#76:                                # %.thread.loopexit199
	movl	$2, %r14d
	jmp	.LBB16_77
.LBB16_58:
	movq	80(%rsp), %rbx          # 8-byte Reload
	testb	$1, 4(%rsp)             # 1-byte Folded Reload
	je	.LBB16_81
	jmp	.LBB16_79
.LBB16_74:
	movl	$1, %r14d
.LBB16_77:                              # %.thread
	movl	$0, 4(%rsp)             # 4-byte Folded Spill
.LBB16_78:                              # %.thread
	testb	$1, 4(%rsp)             # 1-byte Folded Reload
	jne	.LBB16_79
.LBB16_81:
	movq	(%rbx), %rax
.Ltmp121:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp122:
# BB#82:                                # %_ZN9CMyComPtrI20ISequentialOutStreamE7ReleaseEv.exit162
	movq	(%r13), %rax
	xorl	%ebx, %ebx
.Ltmp123:
	movq	%r13, %rdi
	movl	%r14d, %esi
	callq	*72(%rax)
	movl	%eax, %r14d
.Ltmp124:
	jmp	.LBB16_83
.LBB16_79:
	movl	$-2147467259, %r14d     # imm = 0x80004005
	cmpq	$0, 24(%rsp)
	jne	.LBB16_84
	jmp	.LBB16_85
.LBB16_54:                              # %.loopexit.split-lp
.Ltmp125:
	jmp	.LBB16_47
.LBB16_100:
.Ltmp152:
	jmp	.LBB16_108
.LBB16_96:
.Ltmp146:
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB16_97
.LBB16_94:
.Ltmp141:
	movq	%rdx, %r12
	movq	%rax, %rbp
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_97
# BB#95:
	movq	(%rdi), %rax
.Ltmp142:
	callq	*16(%rax)
.Ltmp143:
.LBB16_97:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit5.i155
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_109
# BB#98:
	movq	(%rdi), %rax
.Ltmp147:
	callq	*16(%rax)
.Ltmp148:
	jmp	.LBB16_109
.LBB16_99:
.Ltmp149:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_110:
.Ltmp162:
	jmp	.LBB16_40
.LBB16_46:
.Ltmp112:
	jmp	.LBB16_47
.LBB16_111:
.Ltmp157:
	movq	%rdx, %r12
	movq	%rax, %rbp
	testq	%rbx, %rbx
	jne	.LBB16_113
	jmp	.LBB16_114
.LBB16_45:
.Ltmp104:
	jmp	.LBB16_43
.LBB16_44:
.Ltmp101:
	movq	%rdx, %r12
	movq	%rax, %rbp
	movq	%r15, %rdi
	callq	_ZdlPv
	jmp	.LBB16_113
.LBB16_41:
.Ltmp89:
	jmp	.LBB16_40
.LBB16_107:
.Ltmp109:
.LBB16_108:
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB16_109
.LBB16_116:
.Ltmp168:
	jmp	.LBB16_117
.LBB16_42:
.Ltmp98:
.LBB16_43:                              # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.thread
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB16_113
.LBB16_126:
.Ltmp79:
.LBB16_117:
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB16_118
.LBB16_39:
.Ltmp86:
.LBB16_40:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit138
	movq	%rdx, %r12
	movq	%rax, %rbp
	jmp	.LBB16_114
.LBB16_53:                              # %.loopexit
.Ltmp115:
	jmp	.LBB16_47
.LBB16_55:
.Ltmp120:
.LBB16_47:
	movq	%rdx, %r12
	movq	%rax, %rbp
	cmpq	$0, 24(%rsp)
	je	.LBB16_49
# BB#48:
	movq	16(%rsp), %rdi
	movq	(%rdi), %rax
.Ltmp126:
	callq	*88(%rax)
.Ltmp127:
.LBB16_49:                              # %_ZN8NArchive5NLzma8CDecoder15ReleaseInStreamEv.exit.i
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_51
# BB#50:
	movq	(%rdi), %rax
.Ltmp131:
	callq	*16(%rax)
.Ltmp132:
.LBB16_51:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit.i
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_109
# BB#52:
	movq	(%rdi), %rax
.Ltmp137:
	callq	*16(%rax)
.Ltmp138:
.LBB16_109:
	movq	(%r15), %rax
.Ltmp153:
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp154:
# BB#112:                               # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit
	testq	%rbx, %rbx
	je	.LBB16_114
.LBB16_113:                             # %_ZN9CMyComPtrI21ICompressProgressInfoED2Ev.exit.thread
	movq	(%rbx), %rax
.Ltmp158:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp159:
.LBB16_114:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit138
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_118
# BB#115:
	movq	(%rdi), %rax
.Ltmp163:
	callq	*16(%rax)
.Ltmp164:
.LBB16_118:
	movq	%rbp, %rdi
	callq	__cxa_begin_catch
	movq	%rax, %rbx
	cmpl	$2, %r12d
	je	.LBB16_119
# BB#121:
	callq	__cxa_end_catch
	movl	$-2147024882, %r14d     # imm = 0x8007000E
	jmp	.LBB16_122
.LBB16_119:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	%rbx, (%rax)
.Ltmp169:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp170:
# BB#125:
.LBB16_120:
.Ltmp171:
	movq	%rax, %rbx
	callq	__cxa_end_catch
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB16_103:
.Ltmp133:
	movq	%rax, %rbx
	jmp	.LBB16_104
.LBB16_101:
.Ltmp128:
	movq	%rax, %rbx
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_104
# BB#102:
	movq	(%rdi), %rax
.Ltmp129:
	callq	*16(%rax)
.Ltmp130:
.LBB16_104:                             # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit5.i
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB16_124
# BB#105:
	movq	(%rdi), %rax
.Ltmp134:
	callq	*16(%rax)
.Ltmp135:
	jmp	.LBB16_124
.LBB16_106:
.Ltmp136:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_123:
.Ltmp165:
	movq	%rax, %rbx
.LBB16_124:                             # %.body
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN8NArchive5NLzma8CHandler7ExtractEPKjjiP23IArchiveExtractCallback, .Lfunc_end16-_ZN8NArchive5NLzma8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\356\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\337\002"              # Call site table length
	.long	.Ltmp77-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp78-.Ltmp77         #   Call between .Ltmp77 and .Ltmp78
	.long	.Ltmp79-.Lfunc_begin5   #     jumps to .Ltmp79
	.byte	3                       #   On action: 2
	.long	.Ltmp80-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp81-.Ltmp80         #   Call between .Ltmp80 and .Ltmp81
	.long	.Ltmp86-.Lfunc_begin5   #     jumps to .Ltmp86
	.byte	3                       #   On action: 2
	.long	.Ltmp166-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp168-.Lfunc_begin5  #     jumps to .Ltmp168
	.byte	3                       #   On action: 2
	.long	.Ltmp82-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp85-.Ltmp82         #   Call between .Ltmp82 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin5   #     jumps to .Ltmp86
	.byte	3                       #   On action: 2
	.long	.Ltmp87-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin5   #     jumps to .Ltmp89
	.byte	3                       #   On action: 2
	.long	.Ltmp90-.Lfunc_begin5   # >> Call Site 6 <<
	.long	.Ltmp97-.Ltmp90         #   Call between .Ltmp90 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin5   #     jumps to .Ltmp98
	.byte	3                       #   On action: 2
	.long	.Ltmp99-.Lfunc_begin5   # >> Call Site 7 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp101-.Lfunc_begin5  #     jumps to .Ltmp101
	.byte	3                       #   On action: 2
	.long	.Ltmp102-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp103-.Ltmp102       #   Call between .Ltmp102 and .Ltmp103
	.long	.Ltmp104-.Lfunc_begin5  #     jumps to .Ltmp104
	.byte	3                       #   On action: 2
	.long	.Ltmp105-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp108-.Ltmp105       #   Call between .Ltmp105 and .Ltmp108
	.long	.Ltmp109-.Lfunc_begin5  #     jumps to .Ltmp109
	.byte	3                       #   On action: 2
	.long	.Ltmp110-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp111-.Ltmp110       #   Call between .Ltmp110 and .Ltmp111
	.long	.Ltmp112-.Lfunc_begin5  #     jumps to .Ltmp112
	.byte	3                       #   On action: 2
	.long	.Ltmp139-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp140-.Ltmp139       #   Call between .Ltmp139 and .Ltmp140
	.long	.Ltmp141-.Lfunc_begin5  #     jumps to .Ltmp141
	.byte	3                       #   On action: 2
	.long	.Ltmp144-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp145-.Ltmp144       #   Call between .Ltmp144 and .Ltmp145
	.long	.Ltmp146-.Lfunc_begin5  #     jumps to .Ltmp146
	.byte	3                       #   On action: 2
	.long	.Ltmp150-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp151-.Ltmp150       #   Call between .Ltmp150 and .Ltmp151
	.long	.Ltmp152-.Lfunc_begin5  #     jumps to .Ltmp152
	.byte	3                       #   On action: 2
	.long	.Ltmp155-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin5  #     jumps to .Ltmp157
	.byte	3                       #   On action: 2
	.long	.Ltmp160-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin5  #     jumps to .Ltmp162
	.byte	3                       #   On action: 2
	.long	.Ltmp118-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp119-.Ltmp118       #   Call between .Ltmp118 and .Ltmp119
	.long	.Ltmp120-.Lfunc_begin5  #     jumps to .Ltmp120
	.byte	3                       #   On action: 2
	.long	.Ltmp113-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp114-.Ltmp113       #   Call between .Ltmp113 and .Ltmp114
	.long	.Ltmp115-.Lfunc_begin5  #     jumps to .Ltmp115
	.byte	3                       #   On action: 2
	.long	.Ltmp116-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp120-.Lfunc_begin5  #     jumps to .Ltmp120
	.byte	3                       #   On action: 2
	.long	.Ltmp121-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Ltmp124-.Ltmp121       #   Call between .Ltmp121 and .Ltmp124
	.long	.Ltmp125-.Lfunc_begin5  #     jumps to .Ltmp125
	.byte	3                       #   On action: 2
	.long	.Ltmp142-.Lfunc_begin5  # >> Call Site 20 <<
	.long	.Ltmp148-.Ltmp142       #   Call between .Ltmp142 and .Ltmp148
	.long	.Ltmp149-.Lfunc_begin5  #     jumps to .Ltmp149
	.byte	1                       #   On action: 1
	.long	.Ltmp126-.Lfunc_begin5  # >> Call Site 21 <<
	.long	.Ltmp127-.Ltmp126       #   Call between .Ltmp126 and .Ltmp127
	.long	.Ltmp128-.Lfunc_begin5  #     jumps to .Ltmp128
	.byte	1                       #   On action: 1
	.long	.Ltmp131-.Lfunc_begin5  # >> Call Site 22 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin5  #     jumps to .Ltmp133
	.byte	1                       #   On action: 1
	.long	.Ltmp137-.Lfunc_begin5  # >> Call Site 23 <<
	.long	.Ltmp164-.Ltmp137       #   Call between .Ltmp137 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin5  #     jumps to .Ltmp165
	.byte	1                       #   On action: 1
	.long	.Ltmp164-.Lfunc_begin5  # >> Call Site 24 <<
	.long	.Ltmp169-.Ltmp164       #   Call between .Ltmp164 and .Ltmp169
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin5  # >> Call Site 25 <<
	.long	.Ltmp170-.Ltmp169       #   Call between .Ltmp169 and .Ltmp170
	.long	.Ltmp171-.Lfunc_begin5  #     jumps to .Ltmp171
	.byte	0                       #   On action: cleanup
	.long	.Ltmp170-.Lfunc_begin5  # >> Call Site 26 <<
	.long	.Ltmp129-.Ltmp170       #   Call between .Ltmp170 and .Ltmp129
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin5  # >> Call Site 27 <<
	.long	.Ltmp135-.Ltmp129       #   Call between .Ltmp129 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin5  #     jumps to .Ltmp136
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
	.byte	2                       # >> Action Record 2 <<
                                        #   Catch TypeInfo 2
	.byte	125                     #   Continue to action 1
                                        # >> Catch TypeInfos <<
	.long	_ZTIPKc                 # TypeInfo 2
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi75:
	.cfi_def_cfa_offset 32
.Lcfi76:
	.cfi_offset %rbx, -32
.Lcfi77:
	.cfi_offset %r14, -24
.Lcfi78:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$IID_IUnknown, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	jne	.LBB17_1
# BB#2:
	movl	$IID_IInArchive, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB17_3
.LBB17_1:
	movq	%rbx, (%r14)
.LBB17_6:
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB17_7:
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB17_3:
	movl	$IID_IArchiveOpenSeq, %esi
	movq	%r15, %rdi
	callq	_ZeqRK4GUIDS1_
	testl	%eax, %eax
	je	.LBB17_4
# BB#5:
	leaq	8(%rbx), %rax
	movq	%rax, (%r14)
	jmp	.LBB17_6
.LBB17_4:
	movl	$-2147467262, %eax      # imm = 0x80004002
	jmp	.LBB17_7
.Lfunc_end17:
	.size	_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end17-_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN8NArchive5NLzma8CHandler6AddRefEv,"axG",@progbits,_ZN8NArchive5NLzma8CHandler6AddRefEv,comdat
	.weak	_ZN8NArchive5NLzma8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler6AddRefEv,@function
_ZN8NArchive5NLzma8CHandler6AddRefEv:   # @_ZN8NArchive5NLzma8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	16(%rdi), %eax
	incl	%eax
	movl	%eax, 16(%rdi)
	retq
.Lfunc_end18:
	.size	_ZN8NArchive5NLzma8CHandler6AddRefEv, .Lfunc_end18-_ZN8NArchive5NLzma8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZN8NArchive5NLzma8CHandler7ReleaseEv,"axG",@progbits,_ZN8NArchive5NLzma8CHandler7ReleaseEv,comdat
	.weak	_ZN8NArchive5NLzma8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandler7ReleaseEv,@function
_ZN8NArchive5NLzma8CHandler7ReleaseEv:  # @_ZN8NArchive5NLzma8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi79:
	.cfi_def_cfa_offset 16
	movl	16(%rdi), %eax
	decl	%eax
	movl	%eax, 16(%rdi)
	jne	.LBB19_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB19_2:
	popq	%rcx
	retq
.Lfunc_end19:
	.size	_ZN8NArchive5NLzma8CHandler7ReleaseEv, .Lfunc_end19-_ZN8NArchive5NLzma8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZN8NArchive5NLzma8CHandlerD2Ev,"axG",@progbits,_ZN8NArchive5NLzma8CHandlerD2Ev,comdat
	.weak	_ZN8NArchive5NLzma8CHandlerD2Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandlerD2Ev,@function
_ZN8NArchive5NLzma8CHandlerD2Ev:        # @_ZN8NArchive5NLzma8CHandlerD2Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi82:
	.cfi_def_cfa_offset 32
.Lcfi83:
	.cfi_offset %rbx, -24
.Lcfi84:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp172:
	callq	*16(%rax)
.Ltmp173:
.LBB20_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp178:
	callq	*16(%rax)
.Ltmp179:
.LBB20_4:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB20_7:
.Ltmp180:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_5:
.Ltmp174:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB20_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp175:
	callq	*16(%rax)
.Ltmp176:
.LBB20_8:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit6
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB20_9:
.Ltmp177:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end20:
	.size	_ZN8NArchive5NLzma8CHandlerD2Ev, .Lfunc_end20-_ZN8NArchive5NLzma8CHandlerD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp172-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp173-.Ltmp172       #   Call between .Ltmp172 and .Ltmp173
	.long	.Ltmp174-.Lfunc_begin6  #     jumps to .Ltmp174
	.byte	0                       #   On action: cleanup
	.long	.Ltmp178-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp179-.Ltmp178       #   Call between .Ltmp178 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin6  #     jumps to .Ltmp180
	.byte	0                       #   On action: cleanup
	.long	.Ltmp179-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp175-.Ltmp179       #   Call between .Ltmp179 and .Ltmp175
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp175-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp176-.Ltmp175       #   Call between .Ltmp175 and .Ltmp176
	.long	.Ltmp177-.Lfunc_begin6  #     jumps to .Ltmp177
	.byte	1                       #   On action: 1
	.long	.Ltmp176-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Lfunc_end20-.Ltmp176   #   Call between .Ltmp176 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN8NArchive5NLzma8CHandlerD0Ev,"axG",@progbits,_ZN8NArchive5NLzma8CHandlerD0Ev,comdat
	.weak	_ZN8NArchive5NLzma8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzma8CHandlerD0Ev,@function
_ZN8NArchive5NLzma8CHandlerD0Ev:        # @_ZN8NArchive5NLzma8CHandlerD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi87:
	.cfi_def_cfa_offset 32
.Lcfi88:
	.cfi_offset %rbx, -24
.Lcfi89:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	80(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp181:
	callq	*16(%rax)
.Ltmp182:
.LBB21_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp187:
	callq	*16(%rax)
.Ltmp188:
.LBB21_4:                               # %_ZN8NArchive5NLzma8CHandlerD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB21_8:
.Ltmp189:
	movq	%rax, %r14
	jmp	.LBB21_9
.LBB21_5:
.Ltmp183:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp184:
	callq	*16(%rax)
.Ltmp185:
.LBB21_9:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_7:
.Ltmp186:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN8NArchive5NLzma8CHandlerD0Ev, .Lfunc_end21-_ZN8NArchive5NLzma8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp181-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp182-.Ltmp181       #   Call between .Ltmp181 and .Ltmp182
	.long	.Ltmp183-.Lfunc_begin7  #     jumps to .Ltmp183
	.byte	0                       #   On action: cleanup
	.long	.Ltmp187-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp188-.Ltmp187       #   Call between .Ltmp187 and .Ltmp188
	.long	.Ltmp189-.Lfunc_begin7  #     jumps to .Ltmp189
	.byte	0                       #   On action: cleanup
	.long	.Ltmp184-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp185-.Ltmp184       #   Call between .Ltmp184 and .Ltmp185
	.long	.Ltmp186-.Lfunc_begin7  #     jumps to .Ltmp186
	.byte	1                       #   On action: 1
	.long	.Ltmp185-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end21-.Ltmp185   #   Call between .Ltmp185 and .Lfunc_end21
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv,@function
_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv: # @_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi90:
	.cfi_def_cfa_offset 16
	addq	$-8, %rdi
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB22_17
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB22_17
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB22_17
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB22_17
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB22_17
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB22_17
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB22_17
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB22_17
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB22_17
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB22_17
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB22_17
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB22_17
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB22_17
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB22_17
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB22_17
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB22_16
.LBB22_17:                              # %_ZeqRK4GUIDS1_.exit.thread
	cmpb	IID_IInArchive(%rip), %cl
	jne	.LBB22_33
# BB#18:
	movb	1(%rsi), %al
	cmpb	IID_IInArchive+1(%rip), %al
	jne	.LBB22_33
# BB#19:
	movb	2(%rsi), %al
	cmpb	IID_IInArchive+2(%rip), %al
	jne	.LBB22_33
# BB#20:
	movb	3(%rsi), %al
	cmpb	IID_IInArchive+3(%rip), %al
	jne	.LBB22_33
# BB#21:
	movb	4(%rsi), %al
	cmpb	IID_IInArchive+4(%rip), %al
	jne	.LBB22_33
# BB#22:
	movb	5(%rsi), %al
	cmpb	IID_IInArchive+5(%rip), %al
	jne	.LBB22_33
# BB#23:
	movb	6(%rsi), %al
	cmpb	IID_IInArchive+6(%rip), %al
	jne	.LBB22_33
# BB#24:
	movb	7(%rsi), %al
	cmpb	IID_IInArchive+7(%rip), %al
	jne	.LBB22_33
# BB#25:
	movb	8(%rsi), %al
	cmpb	IID_IInArchive+8(%rip), %al
	jne	.LBB22_33
# BB#26:
	movb	9(%rsi), %al
	cmpb	IID_IInArchive+9(%rip), %al
	jne	.LBB22_33
# BB#27:
	movb	10(%rsi), %al
	cmpb	IID_IInArchive+10(%rip), %al
	jne	.LBB22_33
# BB#28:
	movb	11(%rsi), %al
	cmpb	IID_IInArchive+11(%rip), %al
	jne	.LBB22_33
# BB#29:
	movb	12(%rsi), %al
	cmpb	IID_IInArchive+12(%rip), %al
	jne	.LBB22_33
# BB#30:
	movb	13(%rsi), %al
	cmpb	IID_IInArchive+13(%rip), %al
	jne	.LBB22_33
# BB#31:
	movb	14(%rsi), %al
	cmpb	IID_IInArchive+14(%rip), %al
	jne	.LBB22_33
# BB#32:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %al
	cmpb	IID_IInArchive+15(%rip), %al
	jne	.LBB22_33
.LBB22_16:
	movq	%rdi, (%rdx)
	jmp	.LBB22_50
.LBB22_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IArchiveOpenSeq(%rip), %cl
	jne	.LBB22_51
# BB#34:
	movb	1(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+1(%rip), %cl
	jne	.LBB22_51
# BB#35:
	movb	2(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+2(%rip), %cl
	jne	.LBB22_51
# BB#36:
	movb	3(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+3(%rip), %cl
	jne	.LBB22_51
# BB#37:
	movb	4(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+4(%rip), %cl
	jne	.LBB22_51
# BB#38:
	movb	5(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+5(%rip), %cl
	jne	.LBB22_51
# BB#39:
	movb	6(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+6(%rip), %cl
	jne	.LBB22_51
# BB#40:
	movb	7(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+7(%rip), %cl
	jne	.LBB22_51
# BB#41:
	movb	8(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+8(%rip), %cl
	jne	.LBB22_51
# BB#42:
	movb	9(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+9(%rip), %cl
	jne	.LBB22_51
# BB#43:
	movb	10(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+10(%rip), %cl
	jne	.LBB22_51
# BB#44:
	movb	11(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+11(%rip), %cl
	jne	.LBB22_51
# BB#45:
	movb	12(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+12(%rip), %cl
	jne	.LBB22_51
# BB#46:
	movb	13(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+13(%rip), %cl
	jne	.LBB22_51
# BB#47:
	movb	14(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+14(%rip), %cl
	jne	.LBB22_51
# BB#48:                                # %_ZeqRK4GUIDS1_.exit4
	movb	15(%rsi), %cl
	cmpb	IID_IArchiveOpenSeq+15(%rip), %cl
	jne	.LBB22_51
# BB#49:
	leaq	8(%rdi), %rax
	movq	%rax, (%rdx)
.LBB22_50:                              # %_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv.exit
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB22_51:                              # %_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv.exit
	popq	%rcx
	retq
.Lfunc_end22:
	.size	_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv, .Lfunc_end22-_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive5NLzma8CHandler6AddRefEv,"axG",@progbits,_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv,comdat
	.weak	_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv,@function
_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv: # @_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end23:
	.size	_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv, .Lfunc_end23-_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv,"axG",@progbits,_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv,comdat
	.weak	_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv,@function
_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv: # @_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB24_2
# BB#1:
	addq	$-8, %rdi
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB24_2:                               # %_ZN8NArchive5NLzma8CHandler7ReleaseEv.exit
	popq	%rcx
	retq
.Lfunc_end24:
	.size	_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv, .Lfunc_end24-_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv
	.cfi_endproc

	.section	.text._ZThn8_N8NArchive5NLzma8CHandlerD1Ev,"axG",@progbits,_ZThn8_N8NArchive5NLzma8CHandlerD1Ev,comdat
	.weak	_ZThn8_N8NArchive5NLzma8CHandlerD1Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NLzma8CHandlerD1Ev,@function
_ZThn8_N8NArchive5NLzma8CHandlerD1Ev:   # @_ZThn8_N8NArchive5NLzma8CHandlerD1Ev
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r14
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi94:
	.cfi_def_cfa_offset 32
.Lcfi95:
	.cfi_offset %rbx, -24
.Lcfi96:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+160, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rbx)
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp190:
	callq	*16(%rax)
.Ltmp191:
.LBB25_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp196:
	callq	*16(%rax)
.Ltmp197:
.LBB25_4:                               # %_ZN8NArchive5NLzma8CHandlerD2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB25_7:
.Ltmp198:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_5:
.Ltmp192:
	movq	%rax, %r14
	movq	64(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB25_8
# BB#6:
	movq	(%rdi), %rax
.Ltmp193:
	callq	*16(%rax)
.Ltmp194:
.LBB25_8:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit6.i
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_9:
.Ltmp195:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZThn8_N8NArchive5NLzma8CHandlerD1Ev, .Lfunc_end25-_ZThn8_N8NArchive5NLzma8CHandlerD1Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp190-.Lfunc_begin8  # >> Call Site 1 <<
	.long	.Ltmp191-.Ltmp190       #   Call between .Ltmp190 and .Ltmp191
	.long	.Ltmp192-.Lfunc_begin8  #     jumps to .Ltmp192
	.byte	0                       #   On action: cleanup
	.long	.Ltmp196-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp197-.Ltmp196       #   Call between .Ltmp196 and .Ltmp197
	.long	.Ltmp198-.Lfunc_begin8  #     jumps to .Ltmp198
	.byte	0                       #   On action: cleanup
	.long	.Ltmp197-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp193-.Ltmp197       #   Call between .Ltmp197 and .Ltmp193
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp193-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp194-.Ltmp193       #   Call between .Ltmp193 and .Ltmp194
	.long	.Ltmp195-.Lfunc_begin8  #     jumps to .Ltmp195
	.byte	1                       #   On action: 1
	.long	.Ltmp194-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Lfunc_end25-.Ltmp194   #   Call between .Ltmp194 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZThn8_N8NArchive5NLzma8CHandlerD0Ev,"axG",@progbits,_ZThn8_N8NArchive5NLzma8CHandlerD0Ev,comdat
	.weak	_ZThn8_N8NArchive5NLzma8CHandlerD0Ev
	.p2align	4, 0x90
	.type	_ZThn8_N8NArchive5NLzma8CHandlerD0Ev,@function
_ZThn8_N8NArchive5NLzma8CHandlerD0Ev:   # @_ZThn8_N8NArchive5NLzma8CHandlerD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi97:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi98:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi99:
	.cfi_def_cfa_offset 32
.Lcfi100:
	.cfi_offset %rbx, -24
.Lcfi101:
	.cfi_offset %r14, -16
	movq	%rdi, %rax
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+160, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, -8(%rax)
	movq	72(%rax), %rdi
	leaq	-8(%rax), %rbx
	testq	%rdi, %rdi
	je	.LBB26_2
# BB#1:
	movq	(%rdi), %rax
.Ltmp199:
	callq	*16(%rax)
.Ltmp200:
.LBB26_2:                               # %_ZN9CMyComPtrI19ISequentialInStreamED2Ev.exit.i.i
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_4
# BB#3:
	movq	(%rdi), %rax
.Ltmp205:
	callq	*16(%rax)
.Ltmp206:
.LBB26_4:                               # %_ZN8NArchive5NLzma8CHandlerD0Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_8:
.Ltmp207:
	movq	%rax, %r14
	jmp	.LBB26_9
.LBB26_5:
.Ltmp201:
	movq	%rax, %r14
	movq	72(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB26_9
# BB#6:
	movq	(%rdi), %rax
.Ltmp202:
	callq	*16(%rax)
.Ltmp203:
.LBB26_9:                               # %.body.i
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB26_7:
.Ltmp204:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZThn8_N8NArchive5NLzma8CHandlerD0Ev, .Lfunc_end26-_ZThn8_N8NArchive5NLzma8CHandlerD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp199-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp200-.Ltmp199       #   Call between .Ltmp199 and .Ltmp200
	.long	.Ltmp201-.Lfunc_begin9  #     jumps to .Ltmp201
	.byte	0                       #   On action: cleanup
	.long	.Ltmp205-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp206-.Ltmp205       #   Call between .Ltmp205 and .Ltmp206
	.long	.Ltmp207-.Lfunc_begin9  #     jumps to .Ltmp207
	.byte	0                       #   On action: cleanup
	.long	.Ltmp202-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp203-.Ltmp202       #   Call between .Ltmp202 and .Ltmp203
	.long	.Ltmp204-.Lfunc_begin9  #     jumps to .Ltmp204
	.byte	1                       #   On action: 1
	.long	.Ltmp203-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end26-.Ltmp203   #   Call between .Ltmp203 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzmaL9CreateArcEv,@function
_ZN8NArchive5NLzmaL9CreateArcEv:        # @_ZN8NArchive5NLzmaL9CreateArcEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi102:
	.cfi_def_cfa_offset 16
	movl	$88, %edi
	callq	_Znwm
	movl	$0, 16(%rax)
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+160, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 72(%rax)
	movb	$0, 40(%rax)
	popq	%rcx
	retq
.Lfunc_end27:
	.size	_ZN8NArchive5NLzmaL9CreateArcEv, .Lfunc_end27-_ZN8NArchive5NLzmaL9CreateArcEv
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZN8NArchive5NLzmaL11CreateArc86Ev,@function
_ZN8NArchive5NLzmaL11CreateArc86Ev:     # @_ZN8NArchive5NLzmaL11CreateArc86Ev
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 16
	movl	$88, %edi
	callq	_Znwm
	movl	$0, 16(%rax)
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+160, %ecx
	movd	%rcx, %xmm0
	movl	$_ZTVN8NArchive5NLzma8CHandlerE+16, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rax)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 72(%rax)
	movb	$1, 40(%rax)
	popq	%rcx
	retq
.Lfunc_end28:
	.size	_ZN8NArchive5NLzmaL11CreateArc86Ev, .Lfunc_end28-_ZN8NArchive5NLzmaL11CreateArc86Ev
	.cfi_endproc

	.section	.text._ZeqRK4GUIDS1_,"axG",@progbits,_ZeqRK4GUIDS1_,comdat
	.weak	_ZeqRK4GUIDS1_
	.p2align	4, 0x90
	.type	_ZeqRK4GUIDS1_,@function
_ZeqRK4GUIDS1_:                         # @_ZeqRK4GUIDS1_
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	cmpb	(%rsi), %al
	jne	.LBB29_16
# BB#1:
	movb	1(%rdi), %al
	cmpb	1(%rsi), %al
	jne	.LBB29_16
# BB#2:
	movb	2(%rdi), %al
	cmpb	2(%rsi), %al
	jne	.LBB29_16
# BB#3:
	movb	3(%rdi), %al
	cmpb	3(%rsi), %al
	jne	.LBB29_16
# BB#4:
	movb	4(%rdi), %al
	cmpb	4(%rsi), %al
	jne	.LBB29_16
# BB#5:
	movb	5(%rdi), %al
	cmpb	5(%rsi), %al
	jne	.LBB29_16
# BB#6:
	movb	6(%rdi), %al
	cmpb	6(%rsi), %al
	jne	.LBB29_16
# BB#7:
	movb	7(%rdi), %al
	cmpb	7(%rsi), %al
	jne	.LBB29_16
# BB#8:
	movb	8(%rdi), %al
	cmpb	8(%rsi), %al
	jne	.LBB29_16
# BB#9:
	movb	9(%rdi), %al
	cmpb	9(%rsi), %al
	jne	.LBB29_16
# BB#10:
	movb	10(%rdi), %al
	cmpb	10(%rsi), %al
	jne	.LBB29_16
# BB#11:
	movb	11(%rdi), %al
	cmpb	11(%rsi), %al
	jne	.LBB29_16
# BB#12:
	movb	12(%rdi), %al
	cmpb	12(%rsi), %al
	jne	.LBB29_16
# BB#13:
	movb	13(%rdi), %al
	cmpb	13(%rsi), %al
	jne	.LBB29_16
# BB#14:
	movb	14(%rdi), %al
	cmpb	14(%rsi), %al
	jne	.LBB29_16
# BB#15:
	movb	15(%rdi), %cl
	xorl	%eax, %eax
	cmpb	15(%rsi), %cl
	sete	%al
	retq
.LBB29_16:
	xorl	%eax, %eax
	retq
.Lfunc_end29:
	.size	_ZeqRK4GUIDS1_, .Lfunc_end29-_ZeqRK4GUIDS1_
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_LzmaHandler.ii,@function
_GLOBAL__sub_I_LzmaHandler.ii:          # @_GLOBAL__sub_I_LzmaHandler.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 16
	movl	$_ZN8NArchive5NLzma7NLzmaArL9g_ArcInfoE, %edi
	callq	_Z11RegisterArcPK8CArcInfo
	movl	$_ZN8NArchive5NLzma9NLzma86ArL9g_ArcInfoE, %edi
	popq	%rax
	jmp	_Z11RegisterArcPK8CArcInfo # TAILCALL
.Lfunc_end30:
	.size	_GLOBAL__sub_I_LzmaHandler.ii, .Lfunc_end30-_GLOBAL__sub_I_LzmaHandler.ii
	.cfi_endproc

	.type	_ZN8NArchive5NLzma6kPropsE,@object # @_ZN8NArchive5NLzma6kPropsE
	.data
	.globl	_ZN8NArchive5NLzma6kPropsE
	.p2align	4
_ZN8NArchive5NLzma6kPropsE:
	.quad	0
	.long	7                       # 0x7
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	8                       # 0x8
	.short	21                      # 0x15
	.zero	2
	.quad	0
	.long	22                      # 0x16
	.short	8                       # 0x8
	.zero	2
	.size	_ZN8NArchive5NLzma6kPropsE, 48

	.type	_ZTVN8NArchive5NLzma8CHandlerE,@object # @_ZTVN8NArchive5NLzma8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTVN8NArchive5NLzma8CHandlerE
	.p2align	3
_ZTVN8NArchive5NLzma8CHandlerE:
	.quad	0
	.quad	_ZTIN8NArchive5NLzma8CHandlerE
	.quad	_ZN8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZN8NArchive5NLzma8CHandler6AddRefEv
	.quad	_ZN8NArchive5NLzma8CHandler7ReleaseEv
	.quad	_ZN8NArchive5NLzma8CHandlerD2Ev
	.quad	_ZN8NArchive5NLzma8CHandlerD0Ev
	.quad	_ZN8NArchive5NLzma8CHandler4OpenEP9IInStreamPKyP20IArchiveOpenCallback
	.quad	_ZN8NArchive5NLzma8CHandler5CloseEv
	.quad	_ZN8NArchive5NLzma8CHandler16GetNumberOfItemsEPj
	.quad	_ZN8NArchive5NLzma8CHandler11GetPropertyEjjP14tagPROPVARIANT
	.quad	_ZN8NArchive5NLzma8CHandler7ExtractEPKjjiP23IArchiveExtractCallback
	.quad	_ZN8NArchive5NLzma8CHandler18GetArchivePropertyEjP14tagPROPVARIANT
	.quad	_ZN8NArchive5NLzma8CHandler21GetNumberOfPropertiesEPj
	.quad	_ZN8NArchive5NLzma8CHandler15GetPropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive5NLzma8CHandler28GetNumberOfArchivePropertiesEPj
	.quad	_ZN8NArchive5NLzma8CHandler22GetArchivePropertyInfoEjPPwPjPt
	.quad	_ZN8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.quad	-8
	.quad	_ZTIN8NArchive5NLzma8CHandlerE
	.quad	_ZThn8_N8NArchive5NLzma8CHandler14QueryInterfaceERK4GUIDPPv
	.quad	_ZThn8_N8NArchive5NLzma8CHandler6AddRefEv
	.quad	_ZThn8_N8NArchive5NLzma8CHandler7ReleaseEv
	.quad	_ZThn8_N8NArchive5NLzma8CHandlerD1Ev
	.quad	_ZThn8_N8NArchive5NLzma8CHandlerD0Ev
	.quad	_ZThn8_N8NArchive5NLzma8CHandler7OpenSeqEP19ISequentialInStream
	.size	_ZTVN8NArchive5NLzma8CHandlerE, 208

	.type	_ZTSN8NArchive5NLzma8CHandlerE,@object # @_ZTSN8NArchive5NLzma8CHandlerE
	.globl	_ZTSN8NArchive5NLzma8CHandlerE
	.p2align	4
_ZTSN8NArchive5NLzma8CHandlerE:
	.asciz	"N8NArchive5NLzma8CHandlerE"
	.size	_ZTSN8NArchive5NLzma8CHandlerE, 27

	.type	_ZTS10IInArchive,@object # @_ZTS10IInArchive
	.section	.rodata._ZTS10IInArchive,"aG",@progbits,_ZTS10IInArchive,comdat
	.weak	_ZTS10IInArchive
_ZTS10IInArchive:
	.asciz	"10IInArchive"
	.size	_ZTS10IInArchive, 13

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI10IInArchive,@object # @_ZTI10IInArchive
	.section	.rodata._ZTI10IInArchive,"aG",@progbits,_ZTI10IInArchive,comdat
	.weak	_ZTI10IInArchive
	.p2align	4
_ZTI10IInArchive:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IInArchive
	.quad	_ZTI8IUnknown
	.size	_ZTI10IInArchive, 24

	.type	_ZTS15IArchiveOpenSeq,@object # @_ZTS15IArchiveOpenSeq
	.section	.rodata._ZTS15IArchiveOpenSeq,"aG",@progbits,_ZTS15IArchiveOpenSeq,comdat
	.weak	_ZTS15IArchiveOpenSeq
	.p2align	4
_ZTS15IArchiveOpenSeq:
	.asciz	"15IArchiveOpenSeq"
	.size	_ZTS15IArchiveOpenSeq, 18

	.type	_ZTI15IArchiveOpenSeq,@object # @_ZTI15IArchiveOpenSeq
	.section	.rodata._ZTI15IArchiveOpenSeq,"aG",@progbits,_ZTI15IArchiveOpenSeq,comdat
	.weak	_ZTI15IArchiveOpenSeq
	.p2align	4
_ZTI15IArchiveOpenSeq:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15IArchiveOpenSeq
	.quad	_ZTI8IUnknown
	.size	_ZTI15IArchiveOpenSeq, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTIN8NArchive5NLzma8CHandlerE,@object # @_ZTIN8NArchive5NLzma8CHandlerE
	.section	.rodata,"a",@progbits
	.globl	_ZTIN8NArchive5NLzma8CHandlerE
	.p2align	4
_ZTIN8NArchive5NLzma8CHandlerE:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTSN8NArchive5NLzma8CHandlerE
	.long	1                       # 0x1
	.long	3                       # 0x3
	.quad	_ZTI10IInArchive
	.quad	2                       # 0x2
	.quad	_ZTI15IArchiveOpenSeq
	.quad	2050                    # 0x802
	.quad	_ZTI13CMyUnknownImp
	.quad	4098                    # 0x1002
	.size	_ZTIN8NArchive5NLzma8CHandlerE, 72

	.type	_ZN8NArchive5NLzma7NLzmaArL9g_ArcInfoE,@object # @_ZN8NArchive5NLzma7NLzmaArL9g_ArcInfoE
	.data
	.p2align	3
_ZN8NArchive5NLzma7NLzmaArL9g_ArcInfoE:
	.quad	.L.str.3
	.quad	.L.str.3
	.quad	0
	.byte	10                      # 0xa
	.zero	28
	.zero	3
	.long	0                       # 0x0
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZN8NArchive5NLzmaL9CreateArcEv
	.quad	0
	.size	_ZN8NArchive5NLzma7NLzmaArL9g_ArcInfoE, 80

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.3:
	.long	108                     # 0x6c
	.long	122                     # 0x7a
	.long	109                     # 0x6d
	.long	97                      # 0x61
	.long	0                       # 0x0
	.size	.L.str.3, 20

	.type	_ZN8NArchive5NLzma9NLzma86ArL9g_ArcInfoE,@object # @_ZN8NArchive5NLzma9NLzma86ArL9g_ArcInfoE
	.data
	.p2align	3
_ZN8NArchive5NLzma9NLzma86ArL9g_ArcInfoE:
	.quad	.L.str.4
	.quad	.L.str.4
	.quad	0
	.byte	11                      # 0xb
	.zero	28
	.zero	3
	.long	0                       # 0x0
	.byte	1                       # 0x1
	.zero	3
	.quad	_ZN8NArchive5NLzmaL11CreateArc86Ev
	.quad	0
	.size	_ZN8NArchive5NLzma9NLzma86ArL9g_ArcInfoE, 80

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.4:
	.long	108                     # 0x6c
	.long	122                     # 0x7a
	.long	109                     # 0x6d
	.long	97                      # 0x61
	.long	56                      # 0x38
	.long	54                      # 0x36
	.long	0                       # 0x0
	.size	.L.str.4, 28

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_LzmaHandler.ii

	.globl	_ZN8NArchive5NLzma8CDecoderD1Ev
	.type	_ZN8NArchive5NLzma8CDecoderD1Ev,@function
_ZN8NArchive5NLzma8CDecoderD1Ev = _ZN8NArchive5NLzma8CDecoderD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
