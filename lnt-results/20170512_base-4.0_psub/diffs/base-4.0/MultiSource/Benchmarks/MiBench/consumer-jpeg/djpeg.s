	.text
	.file	"djpeg.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$784, %rsp              # imm = 0x310
.Lcfi5:
	.cfi_def_cfa_offset 832
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movl	%edi, %r13d
	movq	(%r12), %rax
	movq	%rax, progname(%rip)
	testq	%rax, %rax
	je	.LBB0_2
# BB#1:
	cmpb	$0, (%rax)
	jne	.LBB0_3
.LBB0_2:
	movq	$.L.str, progname(%rip)
.LBB0_3:
	leaq	616(%rsp), %rdi
	callq	jpeg_std_error
	movq	%rax, (%rsp)
	movq	%rsp, %rbx
	movl	$61, %esi
	movl	$616, %edx              # imm = 0x268
	movq	%rbx, %rdi
	callq	jpeg_CreateDecompress
	movq	$cdjpeg_message_table, 768(%rsp)
	movabsq	$4479650890728, %rax    # imm = 0x413000003E8
	movq	%rax, 776(%rsp)
	movl	$254, %esi
	movl	$COM_handler, %edx
	movq	%rbx, %rdi
	callq	jpeg_set_marker_processor
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%r12, %rdx
	callq	parse_switches
	leal	-1(%r13), %ecx
	cmpl	%ecx, %eax
	jl	.LBB0_31
# BB#4:
	cmpl	%r13d, %eax
	jge	.LBB0_7
# BB#5:
	movslq	%eax, %rbx
	movq	(%r12,%rbx,8), %rdi
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB0_8
# BB#6:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	(%r12,%rbx,8), %rcx
	jmp	.LBB0_11
.LBB0_7:
	callq	read_stdin
	movq	%rax, %r14
.LBB0_8:
	movq	outfilename(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_12
# BB#9:
	movl	$.L.str.4, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	jne	.LBB0_13
# BB#10:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	outfilename(%rip), %rcx
.LBB0_11:
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_12:
	callq	write_stdout
	movq	%rax, %r15
.LBB0_13:
	movq	%rsp, %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	jpeg_stdio_src
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	jpeg_read_header
	movl	$1, %ecx
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movq	%r12, %rdx
	callq	parse_switches
	movl	requested_fmt(%rip), %eax
	cmpq	$5, %rax
	ja	.LBB0_20
# BB#14:
	jmpq	*.LJTI0_0(,%rax,8)
.LBB0_15:
	movq	%rsp, %rdi
	xorl	%esi, %esi
	jmp	.LBB0_18
.LBB0_16:
	movq	%rsp, %rdi
	callq	jinit_write_gif
	jmp	.LBB0_22
.LBB0_17:
	movq	%rsp, %rdi
	movl	$1, %esi
.LBB0_18:
	callq	jinit_write_bmp
	jmp	.LBB0_22
.LBB0_19:
	movq	%rsp, %rdi
	callq	jinit_write_ppm
	jmp	.LBB0_22
.LBB0_20:
	movq	(%rsp), %rax
	movl	$1042, 40(%rax)         # imm = 0x412
	movq	%rsp, %rdi
	callq	*(%rax)
	xorl	%ebx, %ebx
	jmp	.LBB0_23
.LBB0_21:
	movq	%rsp, %rdi
	callq	jinit_write_targa
.LBB0_22:
	movq	%rax, %rbx
.LBB0_23:
	movq	%r15, 24(%rbx)
	movq	%rsp, %r12
	movq	%r12, %rdi
	callq	jpeg_start_decompress
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	*(%rbx)
	movl	160(%rsp), %eax
	cmpl	132(%rsp), %eax
	jae	.LBB0_26
# BB#24:                                # %.lr.ph
	movq	%rsp, %r12
	.p2align	4, 0x90
.LBB0_25:                               # =>This Inner Loop Header: Depth=1
	movq	32(%rbx), %rsi
	movl	40(%rbx), %edx
	movq	%r12, %rdi
	callq	jpeg_read_scanlines
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movl	%eax, %edx
	callq	*8(%rbx)
	movl	160(%rsp), %eax
	cmpl	132(%rsp), %eax
	jb	.LBB0_25
.LBB0_26:                               # %._crit_edge
	movq	%rsp, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	callq	*16(%rbx)
	movq	%r12, %rdi
	callq	jpeg_finish_decompress
	movq	%r12, %rdi
	callq	jpeg_destroy_decompress
	cmpq	stdin(%rip), %r14
	je	.LBB0_28
# BB#27:
	movq	%r14, %rdi
	callq	fclose
.LBB0_28:
	cmpq	stdout(%rip), %r15
	je	.LBB0_30
# BB#29:
	movq	%r15, %rdi
	callq	fclose
.LBB0_30:
	xorl	%edi, %edi
	cmpq	$0, 744(%rsp)
	setne	%dil
	addl	%edi, %edi
	callq	exit
.LBB0_31:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	callq	fprintf
	callq	usage
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_15
	.quad	.LBB0_16
	.quad	.LBB0_17
	.quad	.LBB0_19
	.quad	.LBB0_20
	.quad	.LBB0_21

	.text
	.p2align	4, 0x90
	.type	COM_handler,@function
COM_handler:                            # @COM_handler
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi15:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi17:
	.cfi_def_cfa_offset 64
.Lcfi18:
	.cfi_offset %rbx, -56
.Lcfi19:
	.cfi_offset %r12, -48
.Lcfi20:
	.cfi_offset %r13, -40
.Lcfi21:
	.cfi_offset %r14, -32
.Lcfi22:
	.cfi_offset %r15, -24
.Lcfi23:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movq	(%r12), %rax
	movq	32(%r12), %rbp
	movl	124(%rax), %r15d
	cmpq	$0, 8(%rbp)
	jne	.LBB1_3
# BB#1:
	movq	%r12, %rdi
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB1_3
# BB#2:
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB1_3:                                # %jpeg_getc.exit
	decq	8(%rbp)
	movq	(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %r14d
	shlq	$8, %r14
	movq	32(%r12), %rbp
	cmpq	$0, 8(%rbp)
	jne	.LBB1_6
# BB#4:
	movq	%r12, %rdi
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB1_6
# BB#5:
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
.LBB1_6:                                # %jpeg_getc.exit32
	decq	8(%rbp)
	movq	(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %ebp
	orq	%r14, %rbp
	testl	%r15d, %r15d
	jg	.LBB1_16
# BB#7:                                 # %.outer.split.preheader
	cmpq	$3, %rbp
	jb	.LBB1_13
# BB#8:                                 # %.lr.ph50
	addq	$-2, %rbp
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movq	32(%r12), %rbx
	cmpq	$0, 8(%rbx)
	jne	.LBB1_12
# BB#10:                                #   in Loop: Header=BB1_9 Depth=1
	movq	%r12, %rdi
	callq	*24(%rbx)
	testl	%eax, %eax
	jne	.LBB1_12
# BB#11:                                #   in Loop: Header=BB1_9 Depth=1
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB1_12:                               # %jpeg_getc.exit33
                                        #   in Loop: Header=BB1_9 Depth=1
	decq	8(%rbx)
	incq	(%rbx)
	decq	%rbp
	jg	.LBB1_9
.LBB1_13:                               # %.us-lcssa.us
	testl	%r15d, %r15d
	jg	.LBB1_14
.LBB1_15:
	movl	$1, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_16:                               # %.outer.us.preheader
	leaq	-2(%rbp), %r14
	movq	stderr(%rip), %rdi
	movl	$.L.str.47, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	callq	fprintf
	cmpq	$3, %rbp
	jb	.LBB1_13
# BB#17:                                # %.lr.ph
	xorl	%r13d, %r13d
	jmp	.LBB1_25
.LBB1_14:
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	jmp	.LBB1_15
.LBB1_20:                               #   in Loop: Header=BB1_25 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %edi
	callq	fputc
	movl	$13, %r15d
	jmp	.LBB1_24
.LBB1_31:                               #   in Loop: Header=BB1_25 Depth=1
	movq	stderr(%rip), %rcx
	movl	$.L.str.49, %edi
	movl	$2, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$92, %r15d
	jmp	.LBB1_24
.LBB1_19:                               #   in Loop: Header=BB1_25 Depth=1
	movq	stderr(%rip), %rsi
	movl	$10, %r15d
	movl	$10, %edi
	callq	fputc
	jmp	.LBB1_24
.LBB1_23:                               #   in Loop: Header=BB1_25 Depth=1
	movl	$.L.str.50, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdi
	movl	%r15d, %edx
	callq	fprintf
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_25:                               # =>This Inner Loop Header: Depth=1
	movl	%r15d, %ebx
	movq	32(%r12), %rbp
	cmpq	$0, 8(%rbp)
	jne	.LBB1_28
# BB#26:                                #   in Loop: Header=BB1_25 Depth=1
	movq	%r12, %rdi
	callq	*24(%rbp)
	testl	%eax, %eax
	jne	.LBB1_28
# BB#27:                                #   in Loop: Header=BB1_25 Depth=1
	movq	(%r12), %rax
	movl	$22, 40(%rax)
	movq	%r12, %rdi
	callq	*(%rax)
	.p2align	4, 0x90
.LBB1_28:                               # %jpeg_getc.exit33.us.us
                                        #   in Loop: Header=BB1_25 Depth=1
	decq	8(%rbp)
	movq	(%rbp), %rax
	leaq	1(%rax), %rcx
	movq	%rcx, (%rbp)
	movzbl	(%rax), %r15d
	cmpl	$10, %r15d
	je	.LBB1_18
# BB#29:                                # %jpeg_getc.exit33.us.us
                                        #   in Loop: Header=BB1_25 Depth=1
	cmpb	$13, %r15b
	je	.LBB1_20
# BB#30:                                # %jpeg_getc.exit33.us.us
                                        #   in Loop: Header=BB1_25 Depth=1
	cmpb	$92, %r15b
	je	.LBB1_31
# BB#21:                                #   in Loop: Header=BB1_25 Depth=1
	callq	__ctype_b_loc
	movq	(%rax), %rax
	testb	$64, 1(%rax,%r15,2)
	movq	stderr(%rip), %rcx
	je	.LBB1_23
# BB#22:                                #   in Loop: Header=BB1_25 Depth=1
	movl	%r15d, %edi
	movq	%rcx, %rsi
	callq	_IO_putc
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_25 Depth=1
	movl	$10, %r15d
	cmpl	$13, %r13d
	jne	.LBB1_19
.LBB1_24:                               # %.outer.us.backedge
                                        #   in Loop: Header=BB1_25 Depth=1
	decq	%r14
	movl	%r15d, %r13d
	movl	%ebx, %r15d
	jg	.LBB1_25
	jmp	.LBB1_13
.Lfunc_end1:
	.size	COM_handler, .Lfunc_end1-COM_handler
	.cfi_endproc

	.p2align	4, 0x90
	.type	parse_switches,@function
parse_switches:                         # @parse_switches
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi30:
	.cfi_def_cfa_offset 112
.Lcfi31:
	.cfi_offset %rbx, -56
.Lcfi32:
	.cfi_offset %r12, -48
.Lcfi33:
	.cfi_offset %r13, -40
.Lcfi34:
	.cfi_offset %r14, -32
.Lcfi35:
	.cfi_offset %r15, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%ecx, 24(%rsp)          # 4-byte Spill
	movq	%rdx, %r12
	movl	%esi, %ebp
	movq	%rdi, %r15
	movl	$3, requested_fmt(%rip)
	movq	$0, outfilename(%rip)
	movq	(%r15), %rax
	movl	$0, 124(%rax)
	movl	$1, %ebx
	cmpl	$2, %ebp
	jl	.LBB2_77
# BB#1:                                 # %.lr.ph
	leaq	60(%r15), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	64(%r15), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$1, %ebx
	leaq	28(%rsp), %r13
	jmp	.LBB2_63
.LBB2_2:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.57, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_6
# BB#3:                                 #   in Loop: Header=BB2_63 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jge	.LBB2_78
# BB#4:                                 #   in Loop: Header=BB2_63 Depth=1
	movslq	%ebx, %r14
	movq	(%r12,%r14,8), %rdi
	movl	$.L.str.58, %esi
	movl	$1, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_10
# BB#5:                                 #   in Loop: Header=BB2_63 Depth=1
	movl	$0, 88(%r15)
	jmp	.LBB2_76
.LBB2_6:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.61, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_12
# BB#7:                                 #   in Loop: Header=BB2_63 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jge	.LBB2_78
# BB#8:                                 #   in Loop: Header=BB2_63 Depth=1
	movslq	%ebx, %r14
	movq	(%r12,%r14,8), %rdi
	movl	$.L.str.62, %esi
	movl	$2, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_16
# BB#9:                                 #   in Loop: Header=BB2_63 Depth=1
	movl	$2, 104(%r15)
	jmp	.LBB2_76
.LBB2_10:                               #   in Loop: Header=BB2_63 Depth=1
	movq	(%r12,%r14,8), %rdi
	movl	$.L.str.59, %esi
	movl	$2, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_18
# BB#11:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$1, 88(%r15)
	jmp	.LBB2_76
.LBB2_12:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.65, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB2_14
# BB#13:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.66, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_22
.LBB2_14:                               #   in Loop: Header=BB2_63 Depth=1
	movzbl	parse_switches.printed_version(%rip), %eax
	testb	%al, %al
	je	.LBB2_28
.LBB2_15:                               #   in Loop: Header=BB2_63 Depth=1
	movq	(%r15), %rax
	incl	124(%rax)
	jmp	.LBB2_76
.LBB2_16:                               #   in Loop: Header=BB2_63 Depth=1
	movq	(%r12,%r14,8), %rdi
	movl	$.L.str.63, %esi
	movl	$2, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_20
# BB#17:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$0, 104(%r15)
	jmp	.LBB2_76
.LBB2_18:                               #   in Loop: Header=BB2_63 Depth=1
	movq	(%r12,%r14,8), %rdi
	movl	$.L.str.60, %esi
	movl	$2, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_79
# BB#19:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$2, 88(%r15)
	jmp	.LBB2_76
.LBB2_20:                               #   in Loop: Header=BB2_63 Depth=1
	movq	(%r12,%r14,8), %rdi
	movl	$.L.str.64, %esi
	movl	$2, %edx
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_80
# BB#21:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$1, 104(%r15)
	jmp	.LBB2_76
.LBB2_22:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.59, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_26
# BB#23:                                #   in Loop: Header=BB2_63 Depth=1
	movq	$1, 104(%r15)
	cmpl	$0, 100(%r15)
	jne	.LBB2_25
# BB#24:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$216, 112(%r15)
.LBB2_25:                               #   in Loop: Header=BB2_63 Depth=1
	movq	$1, 88(%r15)
	jmp	.LBB2_76
.LBB2_26:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.70, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_29
# BB#27:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$1, requested_fmt(%rip)
	jmp	.LBB2_76
.LBB2_28:                               #   in Loop: Header=BB2_63 Depth=1
	movq	stderr(%rip), %rdi
	movl	$.L.str.67, %esi
	movl	$.L.str.68, %edx
	movl	$.L.str.69, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movb	$1, parse_switches.printed_version(%rip)
	jmp	.LBB2_15
.LBB2_29:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.71, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.72, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_32
.LBB2_31:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$1, 56(%r15)
	jmp	.LBB2_76
.LBB2_32:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.73, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_37
# BB#33:                                #   in Loop: Header=BB2_63 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jge	.LBB2_78
# BB#34:                                #   in Loop: Header=BB2_63 Depth=1
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	je	.LBB2_76
# BB#35:                                #   in Loop: Header=BB2_63 Depth=1
	movslq	%ebx, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%r12,%rax,8), %rdi
	movl	$.L.str.2, %esi
	callq	fopen
	movq	%rax, %r14
	testq	%r14, %r14
	je	.LBB2_81
# BB#36:                                #   in Loop: Header=BB2_63 Depth=1
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	read_color_map
	movq	%r14, %rdi
	callq	fclose
	jmp	.LBB2_75
.LBB2_37:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.74, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_42
# BB#38:                                #   in Loop: Header=BB2_63 Depth=1
	movb	$120, 15(%rsp)
	incl	%ebx
	cmpl	%ebp, %ebx
	jge	.LBB2_83
# BB#39:                                #   in Loop: Header=BB2_63 Depth=1
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %rdi
	movl	$.L.str.75, %esi
	xorl	%eax, %eax
	leaq	16(%rsp), %rdx
	leaq	15(%rsp), %rcx
	callq	sscanf
	testl	%eax, %eax
	jle	.LBB2_83
# BB#40:                                #   in Loop: Header=BB2_63 Depth=1
	movzbl	15(%rsp), %eax
	orb	$32, %al
	cmpb	$109, %al
	jne	.LBB2_44
# BB#41:                                #   in Loop: Header=BB2_63 Depth=1
	imulq	$1000, 16(%rsp), %rax   # imm = 0x3E8
	movq	%rax, 16(%rsp)
	jmp	.LBB2_45
.LBB2_42:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.76, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_46
# BB#43:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$0, 92(%r15)
	jmp	.LBB2_76
.LBB2_44:                               # %._crit_edge18
                                        #   in Loop: Header=BB2_63 Depth=1
	movq	16(%rsp), %rax
.LBB2_45:                               #   in Loop: Header=BB2_63 Depth=1
	imulq	$1000, %rax, %rax       # imm = 0x3E8
	movq	8(%r15), %rcx
	movq	%rax, 88(%rcx)
	jmp	.LBB2_76
.LBB2_46:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.77, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$0, 108(%r15)
	jmp	.LBB2_76
.LBB2_48:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.78, %esi
	movl	$3, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_50
# BB#49:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$2, requested_fmt(%rip)
	jmp	.LBB2_76
.LBB2_50:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.79, %esi
	movl	$4, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_53
# BB#51:                                #   in Loop: Header=BB2_63 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jge	.LBB2_83
# BB#52:                                #   in Loop: Header=BB2_63 Depth=1
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %rax
	movq	%rax, outfilename(%rip)
	jmp	.LBB2_76
.LBB2_53:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.80, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB2_55
# BB#54:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.81, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_56
.LBB2_55:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$3, requested_fmt(%rip)
	jmp	.LBB2_76
.LBB2_56:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.82, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_58
# BB#57:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$4, requested_fmt(%rip)
	jmp	.LBB2_76
.LBB2_58:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.83, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_61
# BB#59:                                #   in Loop: Header=BB2_63 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jge	.LBB2_83
# BB#60:                                #   in Loop: Header=BB2_63 Depth=1
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %rdi
	movl	$.L.str.84, %esi
	xorl	%eax, %eax
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	callq	sscanf
	cmpl	$2, %eax
	je	.LBB2_76
	jmp	.LBB2_82
.LBB2_61:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.85, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_83
# BB#62:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$5, requested_fmt(%rip)
	jmp	.LBB2_76
	.p2align	4, 0x90
.LBB2_63:                               # =>This Inner Loop Header: Depth=1
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB2_66
# BB#64:                                #   in Loop: Header=BB2_63 Depth=1
	incq	%r14
	movl	$.L.str.51, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_68
# BB#65:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$0, requested_fmt(%rip)
	jmp	.LBB2_76
	.p2align	4, 0x90
.LBB2_66:                               #   in Loop: Header=BB2_63 Depth=1
	testl	%ebx, %ebx
	jg	.LBB2_77
# BB#67:                                #   in Loop: Header=BB2_63 Depth=1
	movq	$0, outfilename(%rip)
	jmp	.LBB2_76
.LBB2_68:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.52, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB2_72
# BB#69:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.53, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB2_72
# BB#70:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.54, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	jne	.LBB2_72
# BB#71:                                #   in Loop: Header=BB2_63 Depth=1
	movl	$.L.str.55, %esi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	keymatch
	testl	%eax, %eax
	je	.LBB2_2
.LBB2_72:                               #   in Loop: Header=BB2_63 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jge	.LBB2_78
# BB#73:                                #   in Loop: Header=BB2_63 Depth=1
	movslq	%ebx, %rax
	movq	(%r12,%rax,8), %rdi
	movl	$.L.str.56, %esi
	xorl	%eax, %eax
	movq	%r13, %rdx
	callq	sscanf
	cmpl	$1, %eax
	jne	.LBB2_78
# BB#74:                                #   in Loop: Header=BB2_63 Depth=1
	movl	28(%rsp), %eax
	movl	%eax, 112(%r15)
.LBB2_75:                               #   in Loop: Header=BB2_63 Depth=1
	movl	$1, 100(%r15)
	.p2align	4, 0x90
.LBB2_76:                               #   in Loop: Header=BB2_63 Depth=1
	incl	%ebx
	cmpl	%ebp, %ebx
	jl	.LBB2_63
.LBB2_77:                               # %._crit_edge
	movl	%ebx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_78:
	callq	usage
.LBB2_79:
	callq	usage
.LBB2_80:
	callq	usage
.LBB2_81:
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%r12,%rax,8), %rcx
	movl	$.L.str.3, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB2_82:
	callq	usage
.LBB2_83:
	callq	usage
.Lfunc_end2:
	.size	parse_switches, .Lfunc_end2-parse_switches
	.cfi_endproc

	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi37:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	movq	progname(%rip), %rdx
	movl	$.L.str.86, %esi
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.87, %edi
	movl	$12, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.88, %edi
	movl	$37, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.89, %edi
	movl	$55, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.90, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.91, %edi
	movl	$40, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.92, %edi
	movl	$61, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movl	$.L.str.93, %esi
	movl	$.L.str.94, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.95, %esi
	movl	$.L.str.94, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.96, %esi
	movl	$.L.str.94, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.97, %esi
	movl	$.L.str.98, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.99, %esi
	movl	$.L.str.94, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.100, %edi
	movl	$29, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movl	$.L.str.101, %esi
	movl	$.L.str.98, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.102, %esi
	movl	$.L.str.94, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rdi
	movl	$.L.str.103, %esi
	movl	$.L.str.94, %edx
	xorl	%eax, %eax
	callq	fprintf
	movq	stderr(%rip), %rcx
	movl	$.L.str.104, %edi
	movl	$45, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.105, %edi
	movl	$53, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.106, %edi
	movl	$62, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.107, %edi
	movl	$56, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.108, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.109, %edi
	movl	$61, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.110, %edi
	movl	$51, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.111, %edi
	movl	$46, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rcx
	movl	$.L.str.112, %edi
	movl	$43, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end3:
	.size	usage, .Lfunc_end3-usage
	.cfi_endproc

	.type	progname,@object        # @progname
	.local	progname
	.comm	progname,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"djpeg"
	.size	.L.str, 6

	.type	cdjpeg_message_table,@object # @cdjpeg_message_table
	.section	.rodata,"a",@progbits
	.p2align	4
cdjpeg_message_table:
	.quad	0
	.quad	.L.str.5
	.quad	.L.str.6
	.quad	.L.str.7
	.quad	.L.str.8
	.quad	.L.str.9
	.quad	.L.str.10
	.quad	.L.str.11
	.quad	.L.str.12
	.quad	.L.str.13
	.quad	.L.str.14
	.quad	.L.str.15
	.quad	.L.str.16
	.quad	.L.str.17
	.quad	.L.str.18
	.quad	.L.str.19
	.quad	.L.str.20
	.quad	.L.str.21
	.quad	.L.str.22
	.quad	.L.str.23
	.quad	.L.str.24
	.quad	.L.str.25
	.quad	.L.str.26
	.quad	.L.str.27
	.quad	.L.str.28
	.quad	.L.str.29
	.quad	.L.str.30
	.quad	.L.str.31
	.quad	.L.str.32
	.quad	.L.str.33
	.quad	.L.str.34
	.quad	.L.str.35
	.quad	.L.str.36
	.quad	.L.str.37
	.quad	.L.str.38
	.quad	.L.str.39
	.quad	.L.str.40
	.quad	.L.str.41
	.quad	.L.str.42
	.quad	.L.str.43
	.quad	.L.str.44
	.quad	.L.str.45
	.quad	.L.str.46
	.quad	0
	.size	cdjpeg_message_table, 352

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"%s: only one input file\n"
	.size	.L.str.1, 25

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: can't open %s\n"
	.size	.L.str.3, 19

	.type	outfilename,@object     # @outfilename
	.local	outfilename
	.comm	outfilename,8,8
	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"wb"
	.size	.L.str.4, 3

	.type	requested_fmt,@object   # @requested_fmt
	.local	requested_fmt
	.comm	requested_fmt,4,4
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Unsupported BMP colormap format"
	.size	.L.str.5, 32

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Only 8- and 24-bit BMP files are supported"
	.size	.L.str.6, 43

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Invalid BMP file: bad header length"
	.size	.L.str.7, 36

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Invalid BMP file: biPlanes not equal to 1"
	.size	.L.str.8, 42

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"BMP output must be grayscale or RGB"
	.size	.L.str.9, 36

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"Sorry, compressed BMPs not yet supported"
	.size	.L.str.10, 41

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"Not a BMP file - does not start with BM"
	.size	.L.str.11, 40

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%ux%u 24-bit BMP image"
	.size	.L.str.12, 23

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"%ux%u 8-bit colormapped BMP image"
	.size	.L.str.13, 34

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"%ux%u 24-bit OS2 BMP image"
	.size	.L.str.14, 27

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"%ux%u 8-bit colormapped OS2 BMP image"
	.size	.L.str.15, 38

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"GIF output got confused"
	.size	.L.str.16, 24

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"Bogus GIF codesize %d"
	.size	.L.str.17, 22

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"GIF output must be grayscale or RGB"
	.size	.L.str.18, 36

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"Too few images in GIF file"
	.size	.L.str.19, 27

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"Not a GIF file"
	.size	.L.str.20, 15

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"%ux%ux%d GIF image"
	.size	.L.str.21, 19

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"Warning: unexpected GIF version number '%c%c%c'"
	.size	.L.str.22, 48

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"Ignoring GIF extension block of type 0x%02x"
	.size	.L.str.23, 44

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"Caution: nonsquare pixels in input"
	.size	.L.str.24, 35

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"Corrupt data in GIF file"
	.size	.L.str.25, 25

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"Bogus char 0x%02x in GIF file, ignoring"
	.size	.L.str.26, 40

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"Premature end of GIF image"
	.size	.L.str.27, 27

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"Ran out of GIF bits"
	.size	.L.str.28, 20

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"PPM output must be grayscale or RGB"
	.size	.L.str.29, 36

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"Nonnumeric data in PPM file"
	.size	.L.str.30, 28

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"Not a PPM file"
	.size	.L.str.31, 15

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"%ux%u PGM image"
	.size	.L.str.32, 16

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"%ux%u text PGM image"
	.size	.L.str.33, 21

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"%ux%u PPM image"
	.size	.L.str.34, 16

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"%ux%u text PPM image"
	.size	.L.str.35, 21

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"Unsupported Targa colormap format"
	.size	.L.str.36, 34

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"Invalid or unsupported Targa file"
	.size	.L.str.37, 34

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"Targa output must be grayscale or RGB"
	.size	.L.str.38, 38

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"%ux%u RGB Targa image"
	.size	.L.str.39, 22

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"%ux%u grayscale Targa image"
	.size	.L.str.40, 28

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"%ux%u colormapped Targa image"
	.size	.L.str.41, 30

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"Color map file is invalid or of unsupported format"
	.size	.L.str.42, 51

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"Output file format cannot handle %d colormap entries"
	.size	.L.str.43, 53

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"ungetc failed"
	.size	.L.str.44, 14

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"Unrecognized input file format --- perhaps you need -targa"
	.size	.L.str.45, 59

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"Unsupported output file format"
	.size	.L.str.46, 31

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"Comment, length %ld:\n"
	.size	.L.str.47, 22

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"\\\\"
	.size	.L.str.49, 3

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"\\%03o"
	.size	.L.str.50, 6

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"bmp"
	.size	.L.str.51, 4

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"colors"
	.size	.L.str.52, 7

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"colours"
	.size	.L.str.53, 8

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"quantize"
	.size	.L.str.54, 9

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"quantise"
	.size	.L.str.55, 9

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"%d"
	.size	.L.str.56, 3

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"dct"
	.size	.L.str.57, 4

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"int"
	.size	.L.str.58, 4

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"fast"
	.size	.L.str.59, 5

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"float"
	.size	.L.str.60, 6

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"dither"
	.size	.L.str.61, 7

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"fs"
	.size	.L.str.62, 3

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"none"
	.size	.L.str.63, 5

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"ordered"
	.size	.L.str.64, 8

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"debug"
	.size	.L.str.65, 6

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"verbose"
	.size	.L.str.66, 8

	.type	parse_switches.printed_version,@object # @parse_switches.printed_version
	.local	parse_switches.printed_version
	.comm	parse_switches.printed_version,1,4
	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"Independent JPEG Group's DJPEG, version %s\n%s\n"
	.size	.L.str.67, 47

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"6a  7-Feb-96"
	.size	.L.str.68, 13

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"Copyright (C) 1996, Thomas G. Lane"
	.size	.L.str.69, 35

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"gif"
	.size	.L.str.70, 4

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"grayscale"
	.size	.L.str.71, 10

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"greyscale"
	.size	.L.str.72, 10

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"map"
	.size	.L.str.73, 4

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"maxmemory"
	.size	.L.str.74, 10

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"%ld%c"
	.size	.L.str.75, 6

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"nosmooth"
	.size	.L.str.76, 9

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"onepass"
	.size	.L.str.77, 8

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"os2"
	.size	.L.str.78, 4

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"outfile"
	.size	.L.str.79, 8

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"pnm"
	.size	.L.str.80, 4

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"ppm"
	.size	.L.str.81, 4

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"rle"
	.size	.L.str.82, 4

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"scale"
	.size	.L.str.83, 6

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"%d/%d"
	.size	.L.str.84, 6

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"targa"
	.size	.L.str.85, 6

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"usage: %s [switches] "
	.size	.L.str.86, 22

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"[inputfile]\n"
	.size	.L.str.87, 13

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"Switches (names may be abbreviated):\n"
	.size	.L.str.88, 38

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"  -colors N      Reduce image to no more than N colors\n"
	.size	.L.str.89, 56

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"  -fast          Fast, low-quality processing\n"
	.size	.L.str.90, 47

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"  -grayscale     Force grayscale output\n"
	.size	.L.str.91, 41

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"  -scale M/N     Scale output image by fraction M/N, eg, 1/8\n"
	.size	.L.str.92, 62

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"  -bmp           Select BMP output format (Windows style)%s\n"
	.size	.L.str.93, 61

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.zero	1
	.size	.L.str.94, 1

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"  -gif           Select GIF output format%s\n"
	.size	.L.str.95, 45

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"  -os2           Select BMP output format (OS/2 style)%s\n"
	.size	.L.str.96, 58

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"  -pnm           Select PBMPLUS (PPM/PGM) output format%s\n"
	.size	.L.str.97, 59

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	" (default)"
	.size	.L.str.98, 11

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"  -targa         Select Targa output format%s\n"
	.size	.L.str.99, 47

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"Switches for advanced users:\n"
	.size	.L.str.100, 30

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"  -dct int       Use integer DCT method%s\n"
	.size	.L.str.101, 43

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"  -dct fast      Use fast integer DCT (less accurate)%s\n"
	.size	.L.str.102, 57

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"  -dct float     Use floating-point DCT method%s\n"
	.size	.L.str.103, 50

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"  -dither fs     Use F-S dithering (default)\n"
	.size	.L.str.104, 46

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"  -dither none   Don't use dithering in quantization\n"
	.size	.L.str.105, 54

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"  -dither ordered  Use ordered dither (medium speed, quality)\n"
	.size	.L.str.106, 63

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"  -map FILE      Map to colors used in named image file\n"
	.size	.L.str.107, 57

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"  -nosmooth      Don't use high-quality upsampling\n"
	.size	.L.str.108, 52

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"  -onepass       Use 1-pass quantization (fast, low quality)\n"
	.size	.L.str.109, 62

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"  -maxmemory N   Maximum memory to use (in kbytes)\n"
	.size	.L.str.110, 52

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"  -outfile name  Specify name for output file\n"
	.size	.L.str.111, 47

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"  -verbose  or  -debug   Emit debug output\n"
	.size	.L.str.112, 44


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
