	.text
	.file	"7zAesRegister.bc"
	.p2align	4, 0x90
	.type	_ZL11CreateCodecv,@function
_ZL11CreateCodecv:                      # @_ZL11CreateCodecv
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movl	$184, %edi
	callq	_Znwm
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 160(%rbx)
	movaps	%xmm0, 144(%rbx)
	movaps	%xmm0, 128(%rbx)
	movaps	%xmm0, 112(%rbx)
	movaps	%xmm0, 96(%rbx)
	movaps	%xmm0, 80(%rbx)
	movaps	%xmm0, 64(%rbx)
	movaps	%xmm0, 48(%rbx)
	movaps	%xmm0, 32(%rbx)
	movaps	%xmm0, 16(%rbx)
	movaps	%xmm0, (%rbx)
	movq	$0, 176(%rbx)
	movl	$_ZTV18ICryptoSetPassword+16, %eax
	movd	%rax, %xmm0
	movl	$_ZTV15ICompressFilter+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$0, 16(%rbx)
	leaq	24(%rbx), %rdi
.Ltmp0:
	callq	_ZN7NCrypto7NSevenZ5CBaseC2Ev
.Ltmp1:
# BB#1:
	movq	$0, 168(%rbx)
	movl	$_ZTVN7NCrypto7NSevenZ8CDecoderE+112, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto7NSevenZ8CDecoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movq	$_ZTVN7NCrypto7NSevenZ8CDecoderE+176, 176(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end0:
	.size	_ZL11CreateCodecv, .Lfunc_end0-_ZL11CreateCodecv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp1      #   Call between .Ltmp1 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZL14CreateCodecOutv,@function
_ZL14CreateCodecOutv:                   # @_ZL14CreateCodecOutv
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movl	$192, %edi
	callq	_Znwm
	movq	%rax, %rbx
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 176(%rbx)
	movaps	%xmm0, 160(%rbx)
	movaps	%xmm0, 144(%rbx)
	movaps	%xmm0, 128(%rbx)
	movaps	%xmm0, 112(%rbx)
	movaps	%xmm0, 96(%rbx)
	movaps	%xmm0, 80(%rbx)
	movaps	%xmm0, 64(%rbx)
	movaps	%xmm0, 48(%rbx)
	movaps	%xmm0, 32(%rbx)
	movaps	%xmm0, 16(%rbx)
	movaps	%xmm0, (%rbx)
	movl	$_ZTV18ICryptoSetPassword+16, %eax
	movd	%rax, %xmm0
	movl	$_ZTV15ICompressFilter+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$0, 16(%rbx)
	leaq	24(%rbx), %rdi
.Ltmp3:
	callq	_ZN7NCrypto7NSevenZ5CBaseC2Ev
.Ltmp4:
# BB#1:
	movq	$0, 168(%rbx)
	movl	$_ZTVN7NCrypto7NSevenZ8CEncoderE+120, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto7NSevenZ8CEncoderE+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, (%rbx)
	movl	$_ZTVN7NCrypto7NSevenZ8CEncoderE+248, %eax
	movd	%rax, %xmm0
	movl	$_ZTVN7NCrypto7NSevenZ8CEncoderE+184, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 176(%rbx)
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_2:
.Ltmp5:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZL14CreateCodecOutv, .Lfunc_end1-_ZL14CreateCodecOutv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp3-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin1    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN15ICompressFilterD0Ev,"axG",@progbits,_ZN15ICompressFilterD0Ev,comdat
	.weak	_ZN15ICompressFilterD0Ev
	.p2align	4, 0x90
	.type	_ZN15ICompressFilterD0Ev,@function
_ZN15ICompressFilterD0Ev:               # @_ZN15ICompressFilterD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end2:
	.size	_ZN15ICompressFilterD0Ev, .Lfunc_end2-_ZN15ICompressFilterD0Ev
	.cfi_endproc

	.section	.text._ZN18ICryptoSetPasswordD0Ev,"axG",@progbits,_ZN18ICryptoSetPasswordD0Ev,comdat
	.weak	_ZN18ICryptoSetPasswordD0Ev
	.p2align	4, 0x90
	.type	_ZN18ICryptoSetPasswordD0Ev,@function
_ZN18ICryptoSetPasswordD0Ev:            # @_ZN18ICryptoSetPasswordD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end3:
	.size	_ZN18ICryptoSetPasswordD0Ev, .Lfunc_end3-_ZN18ICryptoSetPasswordD0Ev
	.cfi_endproc

	.section	.text._ZN8IUnknownD2Ev,"axG",@progbits,_ZN8IUnknownD2Ev,comdat
	.weak	_ZN8IUnknownD2Ev
	.p2align	4, 0x90
	.type	_ZN8IUnknownD2Ev,@function
_ZN8IUnknownD2Ev:                       # @_ZN8IUnknownD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end4:
	.size	_ZN8IUnknownD2Ev, .Lfunc_end4-_ZN8IUnknownD2Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_7zAesRegister.ii,@function
_GLOBAL__sub_I_7zAesRegister.ii:        # @_GLOBAL__sub_I_7zAesRegister.ii
	.cfi_startproc
# BB#0:
	movl	$_ZL11g_CodecInfo, %edi
	jmp	_Z13RegisterCodecPK10CCodecInfo # TAILCALL
.Lfunc_end5:
	.size	_GLOBAL__sub_I_7zAesRegister.ii, .Lfunc_end5-_GLOBAL__sub_I_7zAesRegister.ii
	.cfi_endproc

	.type	_ZL11g_CodecInfo,@object # @_ZL11g_CodecInfo
	.data
	.p2align	3
_ZL11g_CodecInfo:
	.quad	_ZL11CreateCodecv
	.quad	_ZL14CreateCodecOutv
	.quad	116459265               # 0x6f10701
	.quad	.L.str
	.long	1                       # 0x1
	.byte	1                       # 0x1
	.zero	3
	.size	_ZL11g_CodecInfo, 40

	.type	.L.str,@object          # @.str
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str:
	.long	55                      # 0x37
	.long	122                     # 0x7a
	.long	65                      # 0x41
	.long	69                      # 0x45
	.long	83                      # 0x53
	.long	0                       # 0x0
	.size	.L.str, 24

	.type	_ZTV15ICompressFilter,@object # @_ZTV15ICompressFilter
	.section	.rodata._ZTV15ICompressFilter,"aG",@progbits,_ZTV15ICompressFilter,comdat
	.weak	_ZTV15ICompressFilter
	.p2align	3
_ZTV15ICompressFilter:
	.quad	0
	.quad	_ZTI15ICompressFilter
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN15ICompressFilterD0Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV15ICompressFilter, 72

	.type	_ZTS15ICompressFilter,@object # @_ZTS15ICompressFilter
	.section	.rodata._ZTS15ICompressFilter,"aG",@progbits,_ZTS15ICompressFilter,comdat
	.weak	_ZTS15ICompressFilter
	.p2align	4
_ZTS15ICompressFilter:
	.asciz	"15ICompressFilter"
	.size	_ZTS15ICompressFilter, 18

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI15ICompressFilter,@object # @_ZTI15ICompressFilter
	.section	.rodata._ZTI15ICompressFilter,"aG",@progbits,_ZTI15ICompressFilter,comdat
	.weak	_ZTI15ICompressFilter
	.p2align	4
_ZTI15ICompressFilter:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15ICompressFilter
	.quad	_ZTI8IUnknown
	.size	_ZTI15ICompressFilter, 24

	.type	_ZTV18ICryptoSetPassword,@object # @_ZTV18ICryptoSetPassword
	.section	.rodata._ZTV18ICryptoSetPassword,"aG",@progbits,_ZTV18ICryptoSetPassword,comdat
	.weak	_ZTV18ICryptoSetPassword
	.p2align	3
_ZTV18ICryptoSetPassword:
	.quad	0
	.quad	_ZTI18ICryptoSetPassword
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN8IUnknownD2Ev
	.quad	_ZN18ICryptoSetPasswordD0Ev
	.quad	__cxa_pure_virtual
	.size	_ZTV18ICryptoSetPassword, 64

	.type	_ZTS18ICryptoSetPassword,@object # @_ZTS18ICryptoSetPassword
	.section	.rodata._ZTS18ICryptoSetPassword,"aG",@progbits,_ZTS18ICryptoSetPassword,comdat
	.weak	_ZTS18ICryptoSetPassword
	.p2align	4
_ZTS18ICryptoSetPassword:
	.asciz	"18ICryptoSetPassword"
	.size	_ZTS18ICryptoSetPassword, 21

	.type	_ZTI18ICryptoSetPassword,@object # @_ZTI18ICryptoSetPassword
	.section	.rodata._ZTI18ICryptoSetPassword,"aG",@progbits,_ZTI18ICryptoSetPassword,comdat
	.weak	_ZTI18ICryptoSetPassword
	.p2align	4
_ZTI18ICryptoSetPassword:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18ICryptoSetPassword
	.quad	_ZTI8IUnknown
	.size	_ZTI18ICryptoSetPassword, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_7zAesRegister.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
