	.text
	.file	"z18.bc"
	.globl	TransferInit
	.p2align	4, 0x90
	.type	TransferInit,@function
TransferInit:                           # @TransferInit
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi3:
	.cfi_def_cfa_offset 64
.Lcfi4:
	.cfi_offset %rbx, -32
.Lcfi5:
	.cfi_offset %r14, -24
.Lcfi6:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$8388606, initial_constraint(%rip) # imm = 0x7FFFFE
	movl	$8388606, initial_constraint+4(%rip) # imm = 0x7FFFFE
	movl	$8388606, initial_constraint+8(%rip) # imm = 0x7FFFFE
	movq	%r14, InitialEnvironment(%rip)
	movzwl	InitialStyle(%rip), %eax
	andl	$127, %eax
	orl	$25600, %eax            # imm = 0x6400
	movw	%ax, InitialStyle(%rip)
	movw	$360, InitialStyle+2(%rip) # imm = 0x168
	movzwl	InitialStyle+4(%rip), %eax
	andl	$127, %eax
	orl	$9728, %eax             # imm = 0x2600
	movw	%ax, InitialStyle+4(%rip)
	movw	$120, InitialStyle+6(%rip)
	movb	$0, InitialStyle+4(%rip)
	movb	$0, InitialStyle(%rip)
	movq	$0, InitialStyle+8(%rip)
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_1
# BB#2:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_3
.LBB0_1:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_3:
	movb	$8, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	42(%rbx), %eax
	andl	$61183, %eax            # imm = 0xEEFF
	movq	$0, 80(%rbx)
	orl	$256, %eax              # imm = 0x100
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movw	%ax, 42(%rbx)
	movzbl	zz_lengths+122(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB0_4
# BB#5:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_6
.LBB0_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB0_6:
	movb	$122, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_7
# BB#8:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_9
.LBB0_7:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_9:
	movb	$2, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	PrintSym(%rip), %rcx
	movq	%rcx, 80(%rax)
	movq	%rax, 80(%r15)
	movzwl	42(%rax), %ecx
	andl	$65507, %ecx            # imm = 0xFFE3
	orl	$16, %ecx
	movw	%cx, 42(%rax)
	andb	$-33, 42(%r15)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_10
# BB#11:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_12
.LBB0_10:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_12:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_15
# BB#13:
	testq	%rax, %rax
	je	.LBB0_15
# BB#14:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_15:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_17
# BB#16:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_17:
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_18
# BB#19:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_20
.LBB0_18:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_20:
	movb	$8, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, root_galley(%rip)
	movzwl	42(%rax), %ecx
	andl	$61183, %ecx            # imm = 0xEEFF
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rax)
	movups	%xmm0, 144(%rax)
	movups	%xmm0, 128(%rax)
	orl	$256, %ecx              # imm = 0x100
	movw	%cx, 42(%rax)
	movq	no_fpos(%rip), %rsi
	movzwl	2(%rsi), %ecx
	movw	%cx, 34(%rax)
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	4(%rsi), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	movl	36(%rax), %edi
	andl	%edx, %edi
	orl	%ecx, %edi
	movl	%edi, 36(%rax)
	andl	4(%rsi), %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rax)
	movups	%xmm0, 80(%rax)
	movq	$0, 96(%rax)
	movb	42(%rax), %cl
	movl	%ecx, %edx
	andb	$-3, %dl
	movb	%dl, 42(%rax)
	andb	$125, %cl
	movb	%cl, 42(%rax)
	movb	$-127, 40(%rax)
	movzbl	zz_lengths+2(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB0_21
# BB#22:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB0_23
.LBB0_21:
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB0_23:
	movb	$2, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movq	InputSym(%rip), %rax
	movq	%rax, 80(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_24
# BB#25:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_26
.LBB0_24:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_26:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	root_galley(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_29
# BB#27:
	testq	%rcx, %rcx
	je	.LBB0_29
# BB#28:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_29:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB0_32
# BB#30:
	testq	%rax, %rax
	je	.LBB0_32
# BB#31:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB0_32:
	movq	root_galley(%rip), %rdi
	subq	$8, %rsp
.Lcfi7:
	.cfi_adjust_cfa_offset 8
	leaq	8(%rsp), %rax
	leaq	16(%rsp), %rbx
	leaq	32(%rsp), %r10
	movl	$1, %edx
	movl	$0, %ecx
	movl	$0, %r8d
	movl	$0, %r9d
	movq	%r14, %rsi
	pushq	$0
.Lcfi8:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi9:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi10:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	pushq	$initial_constraint
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$InitialStyle
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	SizeGalley
	addq	$64, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -64
	cmpq	$0, 8(%rsp)
	je	.LBB0_34
# BB#33:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.2, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_34:
	cmpq	$0, (%rsp)
	je	.LBB0_36
# BB#35:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.3, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_36:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_37
# BB#38:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB0_39
.LBB0_37:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB0_39:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_41
# BB#40:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB0_41:
	movq	%rax, zz_res(%rip)
	movq	root_galley(%rip), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_44
# BB#42:
	testq	%rcx, %rcx
	je	.LBB0_44
# BB#43:
	movq	16(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rcx)
	movq	16(%rax), %rsi
	movq	%rcx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB0_44:
	movq	8(%rcx), %rbx
	.p2align	4, 0x90
.LBB0_45:                               # =>This Inner Loop Header: Depth=1
	addq	$16, %rbx
	movq	(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB0_45
# BB#46:
	leaq	80(%rbx), %r14
	cmpb	$121, %al
	jne	.LBB0_49
# BB#47:
	movq	80(%rbx), %rax
	cmpb	$2, 32(%rax)
	jne	.LBB0_49
# BB#48:
	movq	80(%rax), %rcx
	cmpq	InputSym(%rip), %rcx
	je	.LBB0_50
.LBB0_49:                               # %.loopexit
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.4, %r9d
	xorl	%eax, %eax
	callq	Error
	movq	(%r14), %rax
.LBB0_50:                               # %._crit_edge
	testb	$16, 42(%rax)
	jne	.LBB0_52
# BB#51:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB0_52:
	orb	$32, 42(%rbx)
	movl	$0, itop(%rip)
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_53
# BB#54:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	xorl	%ecx, %ecx
	jmp	.LBB0_55
.LBB0_53:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	itop(%rip), %ecx
.LBB0_55:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movslq	%ecx, %rdx
	movq	%rax, targets(,%rdx,8)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB0_56
# BB#57:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB0_58
.LBB0_56:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	itop(%rip), %ecx
.LBB0_58:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movslq	%ecx, %rcx
	movq	targets(,%rcx,8), %rdx
	movq	%rdx, zz_hold(%rip)
	testq	%rdx, %rdx
	je	.LBB0_61
# BB#59:
	testq	%rax, %rax
	je	.LBB0_61
# BB#60:
	movq	(%rdx), %rsi
	movq	%rsi, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rdx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rdx
	movq	(%rdx), %rsi
	movq	%rax, 8(%rsi)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rdx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rdx
	movq	%rax, 8(%rdx)
	movq	xx_link(%rip), %rax
.LBB0_61:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB0_63
# BB#62:
	movq	16(%rbx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	16(%rax), %rsi
	movq	%rsi, 16(%rbx)
	movq	16(%rax), %rsi
	movq	%rbx, 24(%rsi)
	movq	%rdx, 16(%rax)
	movq	%rax, 24(%rdx)
.LBB0_63:
	movq	(%r14), %rdi
	shlq	$4, %rcx
	leaq	constraints(%rcx), %rsi
	leaq	16(%rsp), %rcx
	xorl	%edx, %edx
	callq	Constrained
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	TransferInit, .Lfunc_end0-TransferInit
	.cfi_endproc

	.globl	TransferBegin
	.p2align	4, 0x90
	.type	TransferBegin,@function
TransferBegin:                          # @TransferBegin
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi19:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi20:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi21:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi22:
	.cfi_def_cfa_offset 112
.Lcfi23:
	.cfi_offset %rbx, -56
.Lcfi24:
	.cfi_offset %r12, -48
.Lcfi25:
	.cfi_offset %r13, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %r13
	cmpb	$2, 32(%r13)
	je	.LBB1_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB1_2:
	movq	80(%r13), %rax
	movzwl	41(%rax), %eax
	testb	$2, %al
	je	.LBB1_4
# BB#3:
	movq	%r13, %rdi
	callq	CrossAddTag
.LBB1_4:
	leaq	32(%r13), %rbx
	movslq	itop(%rip), %rax
	movq	targets(,%rax,8), %rcx
	cmpq	%rcx, 8(%rcx)
	jne	.LBB1_6
# BB#5:
	movq	80(%r13), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$18, %edi
	movl	$1, %esi
	movl	$.L.str.7, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
	movl	itop(%rip), %eax
.LBB1_6:
	cltq
	movq	targets(,%rax,8), %rax
	movq	8(%rax), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB1_7:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %r12
	leaq	16(%r12), %rax
	cmpb	$0, 32(%r12)
	je	.LBB1_7
# BB#8:
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	80(%r13), %rbp
	movq	80(%r12), %rdi
	callq	GetEnv
	movq	%rax, %rbx
	movq	%rbp, 8(%rsp)           # 8-byte Spill
	movzwl	41(%rbp), %eax
	testb	$1, %ah
	movq	%rbx, %rbp
	je	.LBB1_10
# BB#9:
	movq	no_fpos(%rip), %rsi
	movq	%r13, %rdi
	callq	CopyObject
	movq	%rax, %rbp
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	AttachEnv
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	SetEnv
	movq	%rax, %rbp
.LBB1_10:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r14
	testq	%r14, %r14
	je	.LBB1_11
# BB#12:
	movq	%r14, zz_hold(%rip)
	movq	(%r14), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_13
.LBB1_11:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r14
	movq	%r14, zz_hold(%rip)
.LBB1_13:
	movb	$17, 32(%r14)
	movq	%r14, 24(%r14)
	movq	%r14, 16(%r14)
	movq	%r14, 8(%r14)
	movq	%r14, (%r14)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_14
# BB#15:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_16
.LBB1_14:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_16:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r14, zz_hold(%rip)
	testq	%r14, %r14
	je	.LBB1_19
# BB#17:
	testq	%rax, %rax
	je	.LBB1_19
# BB#18:
	movq	(%r14), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r14)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_19:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_22
# BB#20:
	testq	%rax, %rax
	je	.LBB1_22
# BB#21:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_22:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movzbl	zz_lengths+120(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB1_23
# BB#24:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_25
.LBB1_23:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB1_25:
	movb	$120, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	movq	$0, 88(%r15)
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbp
	testq	%rbp, %rbp
	je	.LBB1_26
# BB#27:
	movq	%rbp, zz_hold(%rip)
	movq	(%rbp), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB1_28
.LBB1_26:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbp
	movq	%rbp, zz_hold(%rip)
.LBB1_28:
	movb	$8, 32(%rbp)
	movq	%rbp, 24(%rbp)
	movq	%rbp, 16(%rbp)
	movq	%rbp, 8(%rbp)
	movq	%rbp, (%rbp)
	movzwl	34(%r13), %eax
	movw	%ax, 34(%rbp)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r13), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%rbp), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%rbp)
	andl	36(%r13), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%rbp)
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, 80(%rbp)
	movq	$0, 128(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbp)
	movzwl	42(%rbp), %eax
	movq	$0, 96(%rbp)
	andl	$65149, %eax            # imm = 0xFE7D
	orl	$384, %eax              # imm = 0x180
	movw	%ax, 42(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_29
# BB#30:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_31
.LBB1_29:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_31:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%r15, %r15
	je	.LBB1_34
# BB#32:
	testq	%rax, %rax
	je	.LBB1_34
# BB#33:
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_34:
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	testq	%rbp, %rbp
	je	.LBB1_37
# BB#35:
	testq	%rax, %rax
	je	.LBB1_37
# BB#36:
	movq	16(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbp)
	movq	16(%rax), %rdx
	movq	%rbp, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_37:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_38
# BB#39:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_40
.LBB1_38:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_40:
	testq	%rbp, %rbp
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbp, zz_hold(%rip)
	je	.LBB1_43
# BB#41:
	testq	%rax, %rax
	je	.LBB1_43
# BB#42:
	movq	(%rbp), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbp)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_43:
	movq	%rax, zz_res(%rip)
	movq	%r13, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_45
# BB#44:
	movq	16(%r13), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r13)
	movq	16(%rax), %rdx
	movq	%r13, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_45:
	movq	%rbx, %rdi
	movq	%r13, %rsi
	callq	AttachEnv
	movq	%rbp, %rdi
	callq	SetTarget
	movq	80(%rbp), %rax
	testb	$32, 126(%rax)
	jne	.LBB1_47
# BB#46:
	xorl	%eax, %eax
	jmp	.LBB1_48
.LBB1_47:
	movq	%rbp, %rdi
	callq	BuildEnclose
.LBB1_48:
	movq	%rax, 136(%rbp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 144(%rbp)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_49
# BB#50:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_51
.LBB1_49:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_51:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	24(%r12), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_54
# BB#52:
	testq	%rcx, %rcx
	je	.LBB1_54
# BB#53:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_54:
	testq	%r15, %r15
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB1_57
# BB#55:
	testq	%rax, %rax
	je	.LBB1_57
# BB#56:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_57:
	movq	88(%rbp), %rax
	testq	%rax, %rax
	je	.LBB1_59
# BB#58:
	movzwl	41(%rax), %eax
	testb	$8, %ah
	jne	.LBB1_60
.LBB1_59:
	movq	%rbp, %rdi
	callq	FlushGalley
.LBB1_60:
	leaq	32(%rbp), %r15
	movq	24(%rbp), %rax
	.p2align	4, 0x90
.LBB1_61:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rax
	movzbl	32(%rax), %ecx
	testb	%cl, %cl
	je	.LBB1_61
# BB#62:
	cmpb	$120, %cl
	jne	.LBB1_88
# BB#63:
	testb	$2, 42(%rbp)
	jne	.LBB1_88
# BB#64:
	movq	%rax, xx_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_71
	.p2align	4, 0x90
.LBB1_65:                               # %.lr.ph203
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB1_67
# BB#66:                                #   in Loop: Header=BB1_65 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rcx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB1_67:                               #   in Loop: Header=BB1_65 Depth=1
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB1_69
# BB#68:                                #   in Loop: Header=BB1_65 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB1_69:                               #   in Loop: Header=BB1_65 Depth=1
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %eax
	leaq	zz_lengths(%rax), %rdx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rcx), %rsi
	cmpb	$2, %al
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %edx
	movq	zz_free(,%rdx,8), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	movq	xx_hold(%rip), %rax
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB1_65
# BB#70:                                # %..preheader193_crit_edge
	movl	%edx, zz_size(%rip)
.LBB1_71:                               # %.preheader193
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_78
	.p2align	4, 0x90
.LBB1_72:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, xx_link(%rip)
	movq	%rcx, zz_hold(%rip)
	movq	24(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB1_74
# BB#73:                                #   in Loop: Header=BB1_72 Depth=1
	movq	%rax, zz_res(%rip)
	movq	16(%rcx), %rdx
	movq	%rdx, 16(%rax)
	movq	16(%rcx), %rdx
	movq	%rax, 24(%rdx)
	movq	%rcx, 24(%rcx)
	movq	%rcx, 16(%rcx)
.LBB1_74:                               #   in Loop: Header=BB1_72 Depth=1
	movq	%rcx, zz_hold(%rip)
	movq	8(%rcx), %rax
	cmpq	%rcx, %rax
	je	.LBB1_76
# BB#75:                                #   in Loop: Header=BB1_72 Depth=1
	movq	%rax, zz_res(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rax)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rcx
.LBB1_76:                               #   in Loop: Header=BB1_72 Depth=1
	movq	%rcx, zz_hold(%rip)
	movzbl	32(%rcx), %eax
	leaq	zz_lengths(%rax), %rdx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rcx), %rsi
	cmpb	$2, %al
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %edx
	movq	zz_free(,%rdx,8), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rdx,8)
	movq	xx_hold(%rip), %rax
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	jne	.LBB1_72
# BB#77:                                # %._crit_edge
	movl	%edx, zz_size(%rip)
.LBB1_78:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	%r14, %rdi
	callq	DisposeObject
	movq	(%r13), %rax
	cmpq	%r13, %rax
	je	.LBB1_124
# BB#79:
	leaq	16(%rax), %rcx
	jmp	.LBB1_80
	.p2align	4, 0x90
.LBB1_125:                              #   in Loop: Header=BB1_80 Depth=1
	addq	$16, %rcx
.LBB1_80:                               # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	movzbl	32(%rcx), %edx
	testb	%dl, %dl
	je	.LBB1_125
# BB#81:
	cmpb	$82, %dl
	jne	.LBB1_124
# BB#82:
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_83
# BB#84:
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	jmp	.LBB1_85
.LBB1_88:                               # %.loopexit194
	movq	80(%rbp), %rax
	movzwl	41(%rax), %eax
	testb	$8, %al
	jne	.LBB1_89
# BB#120:
	movq	no_fpos(%rip), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movzbl	40(%rax), %r8d
	movl	$108, %edi
	jmp	.LBB1_121
.LBB1_89:
	movl	itop(%rip), %eax
	leal	1(%rax), %ecx
	movl	%ecx, itop(%rip)
	cmpl	$29, %eax
	jl	.LBB1_91
# BB#90:
	movl	$18, %edi
	movl	$2, %esi
	movl	$.L.str.8, %edx
	movl	$1, %ecx
	movl	$30, %r9d
	xorl	%eax, %eax
	movq	24(%rsp), %r8           # 8-byte Reload
	callq	Error
.LBB1_91:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_92
# BB#93:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB1_94
.LBB1_92:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB1_94:
	movb	$17, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movslq	itop(%rip), %rsi
	movq	%rax, targets(,%rsi,8)
	movq	8(%rbp), %rax
	cmpq	%rbp, %rax
	je	.LBB1_118
# BB#95:                                # %.preheader.lr.ph
	movq	InputSym(%rip), %rdx
	.p2align	4, 0x90
.LBB1_96:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_97 Depth 2
	movq	%rax, %rbx
	.p2align	4, 0x90
.LBB1_97:                               #   Parent Loop BB1_96 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %ecx
	testb	%cl, %cl
	je	.LBB1_97
# BB#98:                                #   in Loop: Header=BB1_96 Depth=1
	cmpb	$121, %cl
	jne	.LBB1_117
# BB#99:                                #   in Loop: Header=BB1_96 Depth=1
	movq	80(%rbx), %rdi
	cmpq	%rdx, 80(%rdi)
	je	.LBB1_100
.LBB1_117:                              # %.loopexit
                                        #   in Loop: Header=BB1_96 Depth=1
	movq	8(%rax), %rax
	cmpq	%rbp, %rax
	jne	.LBB1_96
	jmp	.LBB1_118
.LBB1_100:
	shlq	$4, %rsi
	leaq	constraints(%rsi), %rsi
	leaq	48(%rsp), %rcx
	xorl	%edx, %edx
	callq	Constrained
	movl	itop(%rip), %ecx
	movslq	%ecx, %rax
	shlq	$4, %rax
	cmpl	$0, constraints(%rax)
	js	.LBB1_116
# BB#101:
	cmpl	$0, constraints+4(%rax)
	js	.LBB1_116
# BB#102:
	cmpl	$0, constraints+8(%rax)
	js	.LBB1_116
# BB#103:
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB1_104
# BB#105:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rdx
	movq	%rdx, zz_free(,%rdi,8)
	jmp	.LBB1_106
.LBB1_116:
	movq	80(%rbp), %rdi
	callq	SymName
	movq	%rax, %rbp
	movl	$18, %edi
	movl	$3, %esi
	movl	$.L.str.9, %edx
	movl	$2, %ecx
	xorl	%eax, %eax
	movq	%r15, %r8
	movq	%rbp, %r9
	callq	Error
.LBB1_118:                              # %.thread
	movq	no_fpos(%rip), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movzbl	40(%rax), %r8d
	movl	$110, %edi
.LBB1_121:
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	xorl	%r9d, %r9d
	callq	NewToken
	movq	%rax, %r13
.LBB1_122:
	movq	%r14, %rdi
.LBB1_123:                              # %.loopexit192
	callq	DisposeObject
.LBB1_124:                              # %.loopexit192
	movq	%r13, %rax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_83:
	xorl	%ecx, %ecx
.LBB1_85:
	movq	%rcx, xx_tmp(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB1_87
# BB#86:
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB1_87:
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movl	%ecx, zz_size(%rip)
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_tmp(%rip), %rdi
	cmpq	%rdi, 24(%rdi)
	jne	.LBB1_124
	jmp	.LBB1_123
.LBB1_104:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
	movl	itop(%rip), %ecx
.LBB1_106:
	movq	16(%rsp), %rbp          # 8-byte Reload
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movslq	%ecx, %rcx
	movq	targets(,%rcx,8), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rcx, %rcx
	je	.LBB1_109
# BB#107:
	testq	%rax, %rax
	je	.LBB1_109
# BB#108:
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB1_109:
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB1_111
# BB#110:
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB1_111:
	movq	80(%rbx), %rdi
	callq	DetachEnv
	movq	80(%rbx), %rsi
	movq	%rbp, %rdi
	callq	AttachEnv
	movq	80(%rbx), %rdi
	testb	$16, 42(%rdi)
	jne	.LBB1_118
# BB#112:
	leaq	32(%rsp), %rsi
	leaq	48(%rsp), %rcx
	movl	$1, %edx
	callq	Constrained
	cmpl	$8388607, 32(%rsp)      # imm = 0x7FFFFF
	jne	.LBB1_115
# BB#113:
	cmpl	$8388607, 36(%rsp)      # imm = 0x7FFFFF
	jne	.LBB1_115
# BB#114:
	cmpl	$8388607, 40(%rsp)      # imm = 0x7FFFFF
	jne	.LBB1_115
# BB#119:
	movq	no_fpos(%rip), %rsi
	movq	8(%rsp), %rax           # 8-byte Reload
	movzbl	40(%rax), %r8d
	movl	$109, %edi
	jmp	.LBB1_121
.LBB1_115:
	addq	$32, %rbx
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	SymName
	movq	%rax, %rbp
	movl	$18, %edi
	movl	$4, %esi
	movl	$.L.str.10, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r8
	movq	%rbp, %r9
	callq	Error
                                        # implicit-def: %R13
	jmp	.LBB1_122
.Lfunc_end1:
	.size	TransferBegin, .Lfunc_end1-TransferBegin
	.cfi_endproc

	.globl	TransferComponent
	.p2align	4, 0x90
	.type	TransferComponent,@function
TransferComponent:                      # @TransferComponent
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi33:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi34:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi35:
	.cfi_def_cfa_offset 80
.Lcfi36:
	.cfi_offset %rbx, -56
.Lcfi37:
	.cfi_offset %r12, -48
.Lcfi38:
	.cfi_offset %r13, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movslq	itop(%rip), %rax
	movq	targets(,%rax,8), %rax
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	je	.LBB2_1
	.p2align	4, 0x90
.LBB2_2:                                # %.preheader119
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB2_2
# BB#3:
	movq	80(%r14), %rax
	testb	$16, 42(%rax)
	jne	.LBB2_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.11, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_5:
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_6
# BB#7:
	movq	%rbx, zz_hold(%rip)
	movq	(%rbx), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_8
.LBB2_1:
	movq	%r15, %rdi
	callq	DisposeObject
	jmp	.LBB2_66
.LBB2_6:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %rbx
	movq	%rbx, zz_hold(%rip)
.LBB2_8:
	movb	$8, 32(%rbx)
	movq	%rbx, 24(%rbx)
	movq	%rbx, 16(%rbx)
	movq	%rbx, 8(%rbx)
	movq	%rbx, (%rbx)
	movzwl	42(%rbx), %eax
	andl	$61183, %eax            # imm = 0xEEFF
	orl	$256, %eax              # imm = 0x100
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rbx)
	movups	%xmm0, 144(%rbx)
	movups	%xmm0, 128(%rbx)
	movw	%ax, 42(%rbx)
	movzwl	34(%r15), %ecx
	movw	%cx, 34(%rbx)
	movl	$1048575, %ecx          # imm = 0xFFFFF
	andl	36(%r15), %ecx
	movl	$-1048576, %edx         # imm = 0xFFF00000
	movl	36(%rbx), %esi
	andl	%edx, %esi
	orl	%ecx, %esi
	movl	%esi, 36(%rbx)
	andl	36(%r15), %edx
	orl	%ecx, %edx
	movl	%edx, 36(%rbx)
	movups	%xmm0, 80(%rbx)
	movq	$0, 96(%rbx)
	movb	$-127, 40(%rbx)
	andl	$61309, %eax            # imm = 0xEF7D
	movw	%ax, 42(%rbx)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_9
# BB#10:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_11
.LBB2_9:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_11:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_14
# BB#12:
	testq	%rax, %rax
	je	.LBB2_14
# BB#13:
	movq	(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_14:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_16
# BB#15:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_16:
	movq	80(%r14), %r15
	movq	%r15, %rdi
	callq	GetEnv
	movzwl	42(%r15), %ecx
	shrl	$2, %ecx
	andl	$1, %ecx
	leaq	64(%r15), %r10
	movslq	itop(%rip), %rdx
	shlq	$4, %rdx
	leaq	constraints(%rdx), %r11
	subq	$8, %rsp
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	leaq	8(%rsp), %r12
	leaq	16(%rsp), %r13
	leaq	24(%rsp), %rbp
	movl	$1, %edx
	movl	$0, %r8d
	movl	$1, %r9d
	movq	%rax, %rsi
	movq	%rbx, %rdi
	pushq	$0
.Lcfi43:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi48:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	callq	SizeGalley
	addq	$64, %rsp
.Lcfi50:
	.cfi_adjust_cfa_offset -64
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#17:
	callq	ExpandRecursives
.LBB2_18:
	movq	24(%r14), %rax
	movq	(%rax), %r12
	movl	48(%rbx), %esi
	movl	56(%rbx), %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	callq	AdjustSize
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	Promote
	movq	%rbx, xx_hold(%rip)
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_25
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph125
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_21
# BB#20:                                #   in Loop: Header=BB2_19 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_21:                               #   in Loop: Header=BB2_19 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_23
# BB#22:                                #   in Loop: Header=BB2_19 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_23:                               #   in Loop: Header=BB2_19 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	24(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_19
# BB#24:                                # %..preheader118_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB2_25:                               # %.preheader118
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	je	.LBB2_32
	.p2align	4, 0x90
.LBB2_26:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_28
# BB#27:                                #   in Loop: Header=BB2_26 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB2_28:                               #   in Loop: Header=BB2_26 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_30
# BB#29:                                #   in Loop: Header=BB2_26 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_30:                               #   in Loop: Header=BB2_26 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %rbx
	movq	8(%rbx), %rax
	cmpq	%rbx, %rax
	jne	.LBB2_26
# BB#31:                                # %._crit_edge122
	movl	%ecx, zz_size(%rip)
.LBB2_32:
	movq	%rbx, zz_hold(%rip)
	movzbl	32(%rbx), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%rbx), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%rbx)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	cmpq	%r14, 8(%r14)
	je	.LBB2_60
# BB#33:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB2_34
# BB#35:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB2_36
.LBB2_34:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB2_36:
	movb	$17, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	jmp	.LBB2_37
	.p2align	4, 0x90
.LBB2_58:                               #   in Loop: Header=BB2_37 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB2_37:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_38 Depth 2
                                        #     Child Loop BB2_44 Depth 2
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB2_59
	.p2align	4, 0x90
.LBB2_38:                               #   Parent Loop BB2_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB2_38
# BB#39:                                #   in Loop: Header=BB2_37 Depth=1
	cmpb	$8, %al
	je	.LBB2_41
# BB#40:                                #   in Loop: Header=BB2_37 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.12, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB2_41:                               # %.loopexit
                                        #   in Loop: Header=BB2_37 Depth=1
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_43
# BB#42:                                #   in Loop: Header=BB2_37 Depth=1
	callq	DisposeObject
	movq	$0, 104(%rbx)
.LBB2_43:                               #   in Loop: Header=BB2_37 Depth=1
	movq	%rbx, %rdi
	callq	DetachGalley
	movq	24(%rbx), %rbx
	.p2align	4, 0x90
.LBB2_44:                               #   Parent Loop BB2_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB2_44
# BB#45:                                #   in Loop: Header=BB2_37 Depth=1
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB2_47
# BB#46:                                #   in Loop: Header=BB2_37 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB2_47:                               #   in Loop: Header=BB2_37 Depth=1
	movq	%rax, zz_res(%rip)
	movq	8(%r12), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB2_50
# BB#48:                                #   in Loop: Header=BB2_37 Depth=1
	testq	%rcx, %rcx
	je	.LBB2_50
# BB#49:                                #   in Loop: Header=BB2_37 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB2_50:                               #   in Loop: Header=BB2_37 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB2_51
# BB#52:                                #   in Loop: Header=BB2_37 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB2_53
	.p2align	4, 0x90
.LBB2_51:                               #   in Loop: Header=BB2_37 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB2_53:                               #   in Loop: Header=BB2_37 Depth=1
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB2_56
# BB#54:                                #   in Loop: Header=BB2_37 Depth=1
	testq	%rax, %rax
	je	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_37 Depth=1
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB2_56:                               #   in Loop: Header=BB2_37 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB2_37
# BB#57:                                #   in Loop: Header=BB2_37 Depth=1
	testq	%rax, %rax
	jne	.LBB2_58
	jmp	.LBB2_37
.LBB2_59:                               # %._crit_edge
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	FlushInners
.LBB2_60:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB2_62
# BB#61:
	xorl	%esi, %esi
	callq	FlushInners
.LBB2_62:
	movzwl	42(%r14), %eax
	testb	$32, %al
	je	.LBB2_66
# BB#63:
	andl	$65503, %eax            # imm = 0xFFDF
	movw	%ax, 42(%r14)
	movq	24(%r14), %rdi
	.p2align	4, 0x90
.LBB2_64:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB2_64
# BB#65:
	callq	FlushGalley
.LBB2_66:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	TransferComponent, .Lfunc_end2-TransferComponent
	.cfi_endproc

	.globl	TransferEnd
	.p2align	4, 0x90
	.type	TransferEnd,@function
TransferEnd:                            # @TransferEnd
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi53:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi54:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi55:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi57:
	.cfi_def_cfa_offset 80
.Lcfi58:
	.cfi_offset %rbx, -56
.Lcfi59:
	.cfi_offset %r12, -48
.Lcfi60:
	.cfi_offset %r13, -40
.Lcfi61:
	.cfi_offset %r14, -32
.Lcfi62:
	.cfi_offset %r15, -24
.Lcfi63:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movslq	itop(%rip), %rax
	movq	targets(,%rax,8), %rax
	movq	8(%rax), %r14
	cmpq	%rax, %r14
	je	.LBB3_1
	.p2align	4, 0x90
.LBB3_2:                                # %.preheader162
                                        # =>This Inner Loop Header: Depth=1
	movq	16(%r14), %r14
	cmpb	$0, 32(%r14)
	je	.LBB3_2
# BB#3:
	movzbl	zz_lengths+8(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r12
	testq	%r12, %r12
	je	.LBB3_4
# BB#5:
	movq	%r12, zz_hold(%rip)
	movq	(%r12), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_6
.LBB3_1:
	movq	%r15, %rdi
	callq	DisposeObject
	jmp	.LBB3_97
.LBB3_4:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r12
	movq	%r12, zz_hold(%rip)
.LBB3_6:
	movb	$8, 32(%r12)
	movq	%r12, 24(%r12)
	movq	%r12, 16(%r12)
	movq	%r12, 8(%r12)
	movq	%r12, (%r12)
	movzwl	34(%r15), %eax
	movw	%ax, 34(%r12)
	movl	$1048575, %eax          # imm = 0xFFFFF
	andl	36(%r15), %eax
	movl	$-1048576, %ecx         # imm = 0xFFF00000
	movl	36(%r12), %edx
	andl	%ecx, %edx
	orl	%eax, %edx
	movl	%edx, 36(%r12)
	andl	36(%r15), %ecx
	orl	%eax, %ecx
	movl	%ecx, 36(%r12)
	movzwl	42(%r12), %eax
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%r12)
	movups	%xmm0, 144(%r12)
	movups	%xmm0, 128(%r12)
	movups	%xmm0, 80(%r12)
	movq	$0, 96(%r12)
	movb	$-127, 40(%r12)
	andl	$61053, %eax            # imm = 0xEE7D
	orl	$256, %eax              # imm = 0x100
	movw	%ax, 42(%r12)
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_7
# BB#8:
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_9
.LBB3_7:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_9:
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r12, zz_hold(%rip)
	testq	%r12, %r12
	je	.LBB3_12
# BB#10:
	testq	%rax, %rax
	je	.LBB3_12
# BB#11:
	movq	(%r12), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r12)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_12:
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_14
# BB#13:
	movq	16(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%r15)
	movq	16(%rax), %rdx
	movq	%r15, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_14:
	movq	80(%r14), %r15
	movq	%r15, %rdi
	callq	GetEnv
	movzwl	42(%r15), %ecx
	movl	%ecx, %edx
	shrl	$4, %edx
	andl	$1, %edx
	shrl	$2, %ecx
	andl	$1, %ecx
	leaq	64(%r15), %r10
	movslq	itop(%rip), %rsi
	shlq	$4, %rsi
	leaq	constraints(%rsi), %rbx
	subq	$8, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset 8
	leaq	8(%rsp), %r11
	leaq	16(%rsp), %r13
	leaq	24(%rsp), %rbp
	movl	$0, %r8d
	movl	$1, %r9d
	movq	%rax, %rsi
	movq	%r12, %rdi
	pushq	$0
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	%r11
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi67:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi68:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi69:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi70:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi71:
	.cfi_adjust_cfa_offset 8
	callq	SizeGalley
	addq	$64, %rsp
.Lcfi72:
	.cfi_adjust_cfa_offset -64
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_16
# BB#15:
	callq	ExpandRecursives
.LBB3_16:
	movq	24(%r14), %rax
	movq	(%rax), %r13
	movl	48(%r12), %esi
	movl	56(%r12), %edx
	xorl	%ecx, %ecx
	movq	%r15, %rdi
	callq	AdjustSize
	testb	$16, 42(%r15)
	jne	.LBB3_20
# BB#17:
	movq	(%r12), %rax
	addq	$16, %rax
	.p2align	4, 0x90
.LBB3_18:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rbx
	leaq	16(%rbx), %rax
	cmpb	$0, 32(%rbx)
	je	.LBB3_18
# BB#19:
	movl	52(%rbx), %esi
	movl	60(%rbx), %edx
	movl	$1, %ecx
	movq	%r15, %rdi
	callq	AdjustSize
	movl	$19, %esi
	movq	%r15, %rdi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	callq	Interpose
.LBB3_20:
	movl	$1, %ecx
	movq	%r12, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	Promote
	movq	%r12, xx_hold(%rip)
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB3_27
	.p2align	4, 0x90
.LBB3_21:                               # %.lr.ph184
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_23
# BB#22:                                #   in Loop: Header=BB3_21 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_23:                               #   in Loop: Header=BB3_21 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_25
# BB#24:                                #   in Loop: Header=BB3_21 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_25:                               #   in Loop: Header=BB3_21 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r12
	movq	24(%r12), %rax
	cmpq	%r12, %rax
	jne	.LBB3_21
# BB#26:                                # %..preheader161_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB3_27:                               # %.preheader161
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	je	.LBB3_34
	.p2align	4, 0x90
.LBB3_28:                               # %.lr.ph179
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_30
# BB#29:                                #   in Loop: Header=BB3_28 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_30:                               #   in Loop: Header=BB3_28 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_32
# BB#31:                                #   in Loop: Header=BB3_28 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_32:                               #   in Loop: Header=BB3_28 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r12
	movq	8(%r12), %rax
	cmpq	%r12, %rax
	jne	.LBB3_28
# BB#33:                                # %._crit_edge180
	movl	%ecx, zz_size(%rip)
.LBB3_34:
	movq	%r12, zz_hold(%rip)
	movzbl	32(%r12), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r12), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r12)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	cmpq	%r14, 8(%r14)
	je	.LBB3_62
# BB#35:
	movzbl	zz_lengths+17(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %r15
	testq	%r15, %r15
	je	.LBB3_36
# BB#37:
	movq	%r15, zz_hold(%rip)
	movq	(%r15), %rax
	movq	%rax, zz_free(,%rdi,8)
	jmp	.LBB3_38
.LBB3_36:
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, %r15
	movq	%r15, zz_hold(%rip)
.LBB3_38:
	movb	$17, 32(%r15)
	movq	%r15, 24(%r15)
	movq	%r15, 16(%r15)
	movq	%r15, 8(%r15)
	movq	%r15, (%r15)
	jmp	.LBB3_39
	.p2align	4, 0x90
.LBB3_60:                               #   in Loop: Header=BB3_39 Depth=1
	movq	16(%rbx), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rbx)
	movq	16(%rax), %rdx
	movq	%rbx, 24(%rdx)
	movq	%rcx, 16(%rax)
	movq	%rax, 24(%rcx)
.LBB3_39:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_40 Depth 2
                                        #     Child Loop BB3_46 Depth 2
	movq	8(%r14), %rbx
	cmpq	%r14, %rbx
	je	.LBB3_61
	.p2align	4, 0x90
.LBB3_40:                               #   Parent Loop BB3_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rbx
	movzbl	32(%rbx), %eax
	testb	%al, %al
	je	.LBB3_40
# BB#41:                                #   in Loop: Header=BB3_39 Depth=1
	cmpb	$8, %al
	je	.LBB3_43
# BB#42:                                #   in Loop: Header=BB3_39 Depth=1
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.1, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.13, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB3_43:                               # %.loopexit
                                        #   in Loop: Header=BB3_39 Depth=1
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB3_45
# BB#44:                                #   in Loop: Header=BB3_39 Depth=1
	callq	DisposeObject
	movq	$0, 104(%rbx)
.LBB3_45:                               #   in Loop: Header=BB3_39 Depth=1
	movq	%rbx, %rdi
	callq	DetachGalley
	movq	24(%rbx), %rbx
	.p2align	4, 0x90
.LBB3_46:                               #   Parent Loop BB3_39 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbx), %rbx
	cmpb	$0, 32(%rbx)
	je	.LBB3_46
# BB#47:                                #   in Loop: Header=BB3_39 Depth=1
	movq	24(%rbx), %rax
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_49
# BB#48:                                #   in Loop: Header=BB3_39 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_49:                               #   in Loop: Header=BB3_39 Depth=1
	movq	%rax, zz_res(%rip)
	movq	8(%r13), %rcx
	movq	%rcx, zz_hold(%rip)
	testq	%rax, %rax
	je	.LBB3_52
# BB#50:                                #   in Loop: Header=BB3_39 Depth=1
	testq	%rcx, %rcx
	je	.LBB3_52
# BB#51:                                #   in Loop: Header=BB3_39 Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
.LBB3_52:                               #   in Loop: Header=BB3_39 Depth=1
	movzbl	zz_lengths(%rip), %edi
	movl	%edi, zz_size(%rip)
	movq	zz_free(,%rdi,8), %rax
	testq	%rax, %rax
	je	.LBB3_53
# BB#54:                                #   in Loop: Header=BB3_39 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	(%rax), %rcx
	movq	%rcx, zz_free(,%rdi,8)
	jmp	.LBB3_55
	.p2align	4, 0x90
.LBB3_53:                               #   in Loop: Header=BB3_39 Depth=1
	movq	no_fpos(%rip), %rsi
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	callq	GetMemory
	movq	%rax, zz_hold(%rip)
.LBB3_55:                               #   in Loop: Header=BB3_39 Depth=1
	testq	%r15, %r15
	movb	$0, 32(%rax)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
	movq	%rax, 8(%rax)
	movq	%rax, (%rax)
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_res(%rip)
	movq	%r15, zz_hold(%rip)
	je	.LBB3_58
# BB#56:                                #   in Loop: Header=BB3_39 Depth=1
	testq	%rax, %rax
	je	.LBB3_58
# BB#57:                                #   in Loop: Header=BB3_39 Depth=1
	movq	(%r15), %rcx
	movq	%rcx, zz_tmp(%rip)
	movq	(%rax), %rax
	movq	%rax, (%r15)
	movq	zz_hold(%rip), %rax
	movq	zz_res(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	zz_tmp(%rip), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_tmp(%rip), %rcx
	movq	%rax, 8(%rcx)
	movq	xx_link(%rip), %rax
.LBB3_58:                               #   in Loop: Header=BB3_39 Depth=1
	movq	%rax, zz_res(%rip)
	movq	%rbx, zz_hold(%rip)
	testq	%rbx, %rbx
	je	.LBB3_39
# BB#59:                                #   in Loop: Header=BB3_39 Depth=1
	testq	%rax, %rax
	jne	.LBB3_60
	jmp	.LBB3_39
.LBB3_61:                               # %._crit_edge178
	xorl	%esi, %esi
	movq	%r15, %rdi
	callq	FlushInners
.LBB3_62:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_64
# BB#63:
	xorl	%esi, %esi
	callq	FlushInners
.LBB3_64:
	testb	$32, 42(%r14)
	jne	.LBB3_65
# BB#82:
	movq	%r14, xx_hold(%rip)
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB3_89
	.p2align	4, 0x90
.LBB3_83:                               # %.lr.ph169
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_85
# BB#84:                                #   in Loop: Header=BB3_83 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_85:                               #   in Loop: Header=BB3_83 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_87
# BB#86:                                #   in Loop: Header=BB3_83 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_87:                               #   in Loop: Header=BB3_83 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r14
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB3_83
# BB#88:                                # %..preheader_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB3_89:                               # %.preheader
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB3_96
	.p2align	4, 0x90
.LBB3_90:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_92
# BB#91:                                #   in Loop: Header=BB3_90 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_92:                               #   in Loop: Header=BB3_90 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_94
# BB#93:                                #   in Loop: Header=BB3_90 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_94:                               #   in Loop: Header=BB3_90 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r14
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB3_90
# BB#95:                                # %._crit_edge
	movl	%ecx, zz_size(%rip)
.LBB3_96:
	movq	%r14, zz_hold(%rip)
	movzbl	32(%r14), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r14), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r14)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	jmp	.LBB3_97
.LBB3_65:
	movq	24(%r14), %rax
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB3_66:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdi), %rdi
	cmpb	$0, 32(%rdi)
	je	.LBB3_66
# BB#67:
	movq	%r14, xx_hold(%rip)
	cmpq	%r14, %rax
	je	.LBB3_74
	.p2align	4, 0x90
.LBB3_68:                               # %.lr.ph176
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_70
# BB#69:                                #   in Loop: Header=BB3_68 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_70:                               #   in Loop: Header=BB3_68 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_72
# BB#71:                                #   in Loop: Header=BB3_68 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_72:                               #   in Loop: Header=BB3_68 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r14
	movq	24(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB3_68
# BB#73:                                # %..preheader159_crit_edge
	movl	%ecx, zz_size(%rip)
.LBB3_74:                               # %.preheader159
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	je	.LBB3_81
	.p2align	4, 0x90
.LBB3_75:                               # %.lr.ph171
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, xx_link(%rip)
	movq	%rax, zz_hold(%rip)
	movq	24(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_77
# BB#76:                                #   in Loop: Header=BB3_75 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	16(%rax), %rdx
	movq	%rdx, 16(%rcx)
	movq	16(%rax), %rdx
	movq	%rcx, 24(%rdx)
	movq	%rax, 24(%rax)
	movq	%rax, 16(%rax)
.LBB3_77:                               #   in Loop: Header=BB3_75 Depth=1
	movq	%rax, zz_hold(%rip)
	movq	8(%rax), %rcx
	cmpq	%rax, %rcx
	je	.LBB3_79
# BB#78:                                #   in Loop: Header=BB3_75 Depth=1
	movq	%rcx, zz_res(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rcx)
	movq	zz_res(%rip), %rax
	movq	zz_hold(%rip), %rcx
	movq	(%rcx), %rdx
	movq	%rax, 8(%rdx)
	movq	%rcx, 8(%rcx)
	movq	%rcx, (%rcx)
	movq	xx_link(%rip), %rax
.LBB3_79:                               #   in Loop: Header=BB3_75 Depth=1
	movq	%rax, zz_hold(%rip)
	movzbl	32(%rax), %ecx
	leaq	zz_lengths(%rcx), %rdx
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	addb	$-11, %cl
	leaq	33(%rax), %rsi
	cmpb	$2, %cl
	cmovbq	%rsi, %rdx
	movzbl	(%rdx), %ecx
	movq	zz_free(,%rcx,8), %rdx
	movq	%rdx, (%rax)
	movq	zz_hold(%rip), %rax
	movq	%rax, zz_free(,%rcx,8)
	movq	xx_hold(%rip), %r14
	movq	8(%r14), %rax
	cmpq	%r14, %rax
	jne	.LBB3_75
# BB#80:                                # %._crit_edge172
	movl	%ecx, zz_size(%rip)
.LBB3_81:
	movq	%r14, zz_hold(%rip)
	movzbl	32(%r14), %eax
	leaq	zz_lengths(%rax), %rcx
                                        # kill: %AL<def> %AL<kill> %RAX<kill>
	addb	$-11, %al
	leaq	33(%r14), %rdx
	cmpb	$2, %al
	cmovbq	%rdx, %rcx
	movzbl	(%rcx), %eax
	movl	%eax, zz_size(%rip)
	movq	zz_free(,%rax,8), %rcx
	movq	%rcx, (%r14)
	movq	zz_hold(%rip), %rcx
	movq	%rcx, zz_free(,%rax,8)
	callq	FlushGalley
.LBB3_97:
	movslq	itop(%rip), %rax
	leal	-1(%rax), %ecx
	movl	%ecx, itop(%rip)
	movq	targets(,%rax,8), %rdi
	callq	DisposeObject
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	TransferEnd, .Lfunc_end3-TransferEnd
	.cfi_endproc

	.globl	TransferClose
	.p2align	4, 0x90
	.type	TransferClose,@function
TransferClose:                          # @TransferClose
	.cfi_startproc
# BB#0:
	movq	root_galley(%rip), %rdi
	cmpq	%rdi, (%rdi)
	je	.LBB4_4
# BB#1:
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 16
	movq	$0, (%rsp)
	movq	%rsp, %rdx
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%rdi, %rsi
	callq	FreeGalley
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB4_3
# BB#2:
	xorl	%esi, %esi
	callq	FlushInners
.LBB4_3:
	movq	root_galley(%rip), %rdi
	callq	FlushGalley
	addq	$8, %rsp
.LBB4_4:
	retq
.Lfunc_end4:
	.size	TransferClose, .Lfunc_end4-TransferClose
	.cfi_endproc

	.type	initial_constraint,@object # @initial_constraint
	.local	initial_constraint
	.comm	initial_constraint,16,4
	.type	InitialEnvironment,@object # @InitialEnvironment
	.comm	InitialEnvironment,8,8
	.type	InitialStyle,@object    # @InitialStyle
	.comm	InitialStyle,16,4
	.type	root_galley,@object     # @root_galley
	.local	root_galley
	.comm	root_galley,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"assert failed in %s"
	.size	.L.str.1, 20

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"TransferInit: recs   != nilobj!"
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"TransferInit: inners != nilobj!"
	.size	.L.str.3, 32

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"TransferInit: initial galley!"
	.size	.L.str.4, 30

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"TransferInit: input sym not external!"
	.size	.L.str.5, 38

	.type	itop,@object            # @itop
	.local	itop
	.comm	itop,4,4
	.type	targets,@object         # @targets
	.local	targets
	.comm	targets,240,16
	.type	constraints,@object     # @constraints
	.local	constraints
	.comm	constraints,480,16
	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"TransferBegin: non-CLOSURE!"
	.size	.L.str.6, 28

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"cannot attach galley %s"
	.size	.L.str.7, 24

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"galley nested too deeply (max is %d)"
	.size	.L.str.8, 37

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"galley %s deleted (insufficient width at target)"
	.size	.L.str.9, 49

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"right parameter of %s is vertically constrained"
	.size	.L.str.10, 48

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"TransferComponent: internal!"
	.size	.L.str.11, 29

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"TransferComponent: input child!"
	.size	.L.str.12, 32

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"TransferEnd: input child!"
	.size	.L.str.13, 26


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
