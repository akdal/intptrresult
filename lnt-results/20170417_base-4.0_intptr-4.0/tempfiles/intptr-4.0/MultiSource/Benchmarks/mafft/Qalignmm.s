	.text
	.file	"Qalignmm.bc"
	.globl	imp_match_out_scQ
	.p2align	4, 0x90
	.type	imp_match_out_scQ,@function
imp_match_out_scQ:                      # @imp_match_out_scQ
	.cfi_startproc
# BB#0:
	movq	impmtx(%rip), %rax
	movslq	%edi, %rcx
	movq	(%rax,%rcx,8), %rax
	movslq	%esi, %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end0:
	.size	imp_match_out_scQ, .Lfunc_end0-imp_match_out_scQ
	.cfi_endproc

	.globl	imp_rnaQ
	.p2align	4, 0x90
	.type	imp_rnaQ,@function
imp_rnaQ:                               # @imp_rnaQ
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	48(%rsp)
.Lcfi1:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi2:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)
.Lcfi3:
	.cfi_adjust_cfa_offset 8
	pushq	impmtx(%rip)
.Lcfi4:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi5:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)
.Lcfi6:
	.cfi_adjust_cfa_offset 8
	callq	foldrna
	addq	$48, %rsp
.Lcfi7:
	.cfi_adjust_cfa_offset -48
	popq	%rax
	retq
.Lfunc_end1:
	.size	imp_rnaQ, .Lfunc_end1-imp_rnaQ
	.cfi_endproc

	.globl	imp_match_init_strictQ
	.p2align	4, 0x90
	.type	imp_match_init_strictQ,@function
imp_match_init_strictQ:                 # @imp_match_init_strictQ
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi11:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi14:
	.cfi_def_cfa_offset 112
.Lcfi15:
	.cfi_offset %rbx, -56
.Lcfi16:
	.cfi_offset %r12, -48
.Lcfi17:
	.cfi_offset %r13, -40
.Lcfi18:
	.cfi_offset %r14, -32
.Lcfi19:
	.cfi_offset %r15, -24
.Lcfi20:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%r8d, %r15d
	movl	%ecx, %r13d
	movl	%edx, %ebp
	movl	%esi, %r14d
	movl	impalloclen(%rip), %eax
	leal	2(%r13), %ecx
	cmpl	%ecx, %eax
	jl	.LBB2_2
# BB#1:
	leal	2(%r15), %ecx
	cmpl	%ecx, %eax
	jge	.LBB2_9
.LBB2_2:
	movq	impmtx(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_4
# BB#3:
	callq	FreeFloatMtx
.LBB2_4:
	movq	imp_match_init_strictQ.nocount1(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	callq	free
.LBB2_6:
	movq	imp_match_init_strictQ.nocount2(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_8
# BB#7:
	callq	free
.LBB2_8:
	cmpl	%r15d, %r13d
	movl	%r15d, %edi
	cmovgel	%r13d, %edi
	addl	$2, %edi
	movl	%edi, impalloclen(%rip)
	movl	%edi, %esi
	callq	AllocateFloatMtx
	movq	%rax, impmtx(%rip)
	movl	impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strictQ.nocount1(%rip)
	movl	impalloclen(%rip), %edi
	callq	AllocateCharVec
	movq	%rax, imp_match_init_strictQ.nocount2(%rip)
.LBB2_9:                                # %.preheader209
	testl	%r13d, %r13d
	jle	.LBB2_19
# BB#10:                                # %.preheader208.lr.ph
	testl	%r14d, %r14d
	movq	imp_match_init_strictQ.nocount1(%rip), %rax
	jle	.LBB2_11
# BB#23:                                # %.preheader208.us.preheader
	movslq	%r14d, %rcx
	movl	%r13d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB2_27
	.p2align	4, 0x90
.LBB2_24:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB2_26
# BB#25:                                #   in Loop: Header=BB2_24 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB2_24
.LBB2_26:                               # %._crit_edge230.us.prol
	cmpl	%r14d, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB2_27:                               # %.preheader208.us.prol.loopexit
	cmpl	$1, %r13d
	je	.LBB2_19
	.p2align	4, 0x90
.LBB2_28:                               # %.preheader208.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_29 Depth 2
                                        #     Child Loop BB2_32 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_29:                               #   Parent Loop BB2_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB2_31
# BB#30:                                #   in Loop: Header=BB2_29 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_29
.LBB2_31:                               # %._crit_edge230.us
                                        #   in Loop: Header=BB2_28 Depth=1
	cmpl	%r14d, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_32:                               #   Parent Loop BB2_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB2_34
# BB#33:                                #   in Loop: Header=BB2_32 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_32
.LBB2_34:                               # %._crit_edge230.us.1
                                        #   in Loop: Header=BB2_28 Depth=1
	cmpl	%r14d, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_28
	jmp	.LBB2_19
.LBB2_11:                               # %.preheader208.preheader
	setne	%cl
	movl	%r13d, %edx
	cmpl	$31, %r13d
	jbe	.LBB2_12
# BB#15:                                # %min.iters.checked
	movl	%r13d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB2_12
# BB#16:                                # %vector.ph
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB2_17:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB2_17
# BB#18:                                # %middle.block
	testl	%r8d, %r8d
	jne	.LBB2_13
	jmp	.LBB2_19
.LBB2_12:
	xorl	%edi, %edi
.LBB2_13:                               # %.preheader208.preheader308
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_14:                               # %.preheader208
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB2_14
.LBB2_19:                               # %.preheader207
	testl	%r15d, %r15d
	jle	.LBB2_41
# BB#20:                                # %.preheader206.lr.ph
	testl	%ebp, %ebp
	movq	imp_match_init_strictQ.nocount2(%rip), %rax
	jle	.LBB2_21
# BB#45:                                # %.preheader206.us.preheader
	movslq	%ebp, %rcx
	movl	%r15d, %edx
	xorl	%esi, %esi
	testb	$1, %dl
	je	.LBB2_49
	.p2align	4, 0x90
.LBB2_46:                               # =>This Inner Loop Header: Depth=1
	movq	112(%rsp), %rdi
	movq	(%rdi,%rsi,8), %rdi
	cmpb	$45, (%rdi)
	je	.LBB2_48
# BB#47:                                #   in Loop: Header=BB2_46 Depth=1
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB2_46
.LBB2_48:                               # %._crit_edge224.us.prol
	cmpl	%ebp, %esi
	setne	(%rax)
	movl	$1, %esi
.LBB2_49:                               # %.preheader206.us.prol.loopexit
	cmpl	$1, %r15d
	je	.LBB2_41
	.p2align	4, 0x90
.LBB2_50:                               # %.preheader206.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_51 Depth 2
                                        #     Child Loop BB2_54 Depth 2
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_51:                               #   Parent Loop BB2_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, (%rbx,%rsi)
	je	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_51 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_51
.LBB2_53:                               # %._crit_edge224.us
                                        #   in Loop: Header=BB2_50 Depth=1
	cmpl	%ebp, %edi
	setne	(%rax,%rsi)
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB2_54:                               #   Parent Loop BB2_50 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	112(%rsp), %rbx
	movq	(%rbx,%rdi,8), %rbx
	cmpb	$45, 1(%rbx,%rsi)
	je	.LBB2_56
# BB#55:                                #   in Loop: Header=BB2_54 Depth=2
	incq	%rdi
	cmpq	%rcx, %rdi
	jl	.LBB2_54
.LBB2_56:                               # %._crit_edge224.us.1
                                        #   in Loop: Header=BB2_50 Depth=1
	cmpl	%ebp, %edi
	setne	1(%rax,%rsi)
	addq	$2, %rsi
	cmpq	%rdx, %rsi
	jne	.LBB2_50
	jmp	.LBB2_41
.LBB2_21:                               # %.preheader206.preheader
	setne	%cl
	movl	%r15d, %edx
	cmpl	$31, %r15d
	jbe	.LBB2_22
# BB#35:                                # %min.iters.checked291
	movl	%r15d, %r8d
	andl	$31, %r8d
	movq	%rdx, %rdi
	subq	%r8, %rdi
	je	.LBB2_22
# BB#36:                                # %vector.ph295
	movzbl	%cl, %esi
	movd	%esi, %xmm0
	punpcklbw	%xmm0, %xmm0    # xmm0 = xmm0[0,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7]
	pshuflw	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0,4,5,6,7]
	pshufd	$80, %xmm0, %xmm0       # xmm0 = xmm0[0,0,1,1]
	leaq	16(%rax), %rbx
	movq	%rdi, %rsi
	.p2align	4, 0x90
.LBB2_37:                               # %vector.body287
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbx)
	movdqu	%xmm0, (%rbx)
	addq	$32, %rbx
	addq	$-32, %rsi
	jne	.LBB2_37
# BB#38:                                # %middle.block288
	testl	%r8d, %r8d
	jne	.LBB2_39
	jmp	.LBB2_41
.LBB2_22:
	xorl	%edi, %edi
.LBB2_39:                               # %.preheader206.preheader306
	addq	%rdi, %rax
	subq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_40:                               # %.preheader206
                                        # =>This Inner Loop Header: Depth=1
	movb	%cl, (%rax)
	incq	%rax
	decq	%rdx
	jne	.LBB2_40
.LBB2_41:                               # %.preheader205
	movl	%ebp, 4(%rsp)           # 4-byte Spill
	testl	%r13d, %r13d
	jle	.LBB2_62
# BB#42:                                # %.preheader204.lr.ph
	testl	%r15d, %r15d
	jle	.LBB2_62
# BB#43:                                # %.preheader204.us.preheader
	movq	impmtx(%rip), %r12
	decl	%r15d
	leaq	4(,%r15,4), %rbp
	movl	%r13d, %r15d
	leaq	-1(%r15), %rax
	movq	%r15, %rbx
	andq	$7, %rbx
	movq	%rax, 24(%rsp)          # 8-byte Spill
	je	.LBB2_44
# BB#57:                                # %.preheader204.us.prol.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB2_58:                               # %.preheader204.us.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%r13,8), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	incq	%r13
	cmpq	%r13, %rbx
	jne	.LBB2_58
	jmp	.LBB2_59
.LBB2_44:
	xorl	%r13d, %r13d
.LBB2_59:                               # %.preheader204.us.prol.loopexit
	cmpq	$7, 24(%rsp)            # 8-byte Folded Reload
	jb	.LBB2_62
# BB#60:                                # %.preheader204.us.preheader.new
	subq	%r13, %r15
	leaq	56(%r12,%r13,8), %rbx
	.p2align	4, 0x90
.LBB2_61:                               # %.preheader204.us
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB2_61
.LBB2_62:                               # %._crit_edge221
	testl	%r14d, %r14d
	movl	4(%rsp), %eax           # 4-byte Reload
	jle	.LBB2_100
# BB#63:                                # %.preheader203.lr.ph
	movsd	fastathreshold(%rip), %xmm0 # xmm0 = mem[0],zero
	movq	impmtx(%rip), %r12
	movl	%eax, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	movl	%r14d, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	xorl	%ecx, %ecx
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$4294967295, %esi       # imm = 0xFFFFFFFF
	.p2align	4, 0x90
.LBB2_64:                               # %.preheader203
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_66 Depth 2
                                        #       Child Loop BB2_68 Depth 3
                                        #         Child Loop BB2_69 Depth 4
                                        #         Child Loop BB2_73 Depth 4
                                        #         Child Loop BB2_77 Depth 4
                                        #         Child Loop BB2_80 Depth 4
                                        #         Child Loop BB2_84 Depth 4
	testl	%eax, %eax
	jle	.LBB2_99
# BB#65:                                # %.lr.ph213
                                        #   in Loop: Header=BB2_64 Depth=1
	movq	120(%rsp), %rax
	movq	8(%rsp), %rcx           # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm1    # xmm1 = mem[0],zero
	movq	136(%rsp), %rax
	movq	(%rax,%rcx,8), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB2_66:                               #   Parent Loop BB2_64 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_68 Depth 3
                                        #         Child Loop BB2_69 Depth 4
                                        #         Child Loop BB2_73 Depth 4
                                        #         Child Loop BB2_77 Depth 4
                                        #         Child Loop BB2_80 Depth 4
                                        #         Child Loop BB2_84 Depth 4
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r8,8), %rbx
	testq	%rbx, %rbx
	je	.LBB2_98
# BB#67:                                # %.lr.ph
                                        #   in Loop: Header=BB2_66 Depth=2
	movq	128(%rsp), %rax
	movsd	(%rax,%r8,8), %xmm2     # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	cvtsd2ss	%xmm2, %xmm2
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	8(%rsp), %rcx           # 8-byte Reload
	movq	(%rax,%rcx,8), %r14
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	subq	%r14, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB2_68:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_69 Depth 4
                                        #         Child Loop BB2_73 Depth 4
                                        #         Child Loop BB2_77 Depth 4
                                        #         Child Loop BB2_80 Depth 4
                                        #         Child Loop BB2_84 Depth 4
	movl	$-1, %ebp
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB2_69:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.LBB2_70
# BB#71:                                #   in Loop: Header=BB2_69 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %al
	setne	%cl
	addl	%ecx, %ebp
	cmpl	24(%rbx), %ebp
	movl	%ebp, %edx
	jne	.LBB2_69
	jmp	.LBB2_72
	.p2align	4, 0x90
.LBB2_70:                               # %._crit_edge273
                                        #   in Loop: Header=BB2_68 Depth=3
	movl	24(%rbx), %edx
.LBB2_72:                               # %.loopexit
                                        #   in Loop: Header=BB2_68 Depth=3
	movq	%rdi, %r9
	subq	%r14, %r9
	addq	%rsi, %r9
	movl	28(%rbx), %eax
	cmpl	%eax, %edx
	movl	%r9d, %r13d
	je	.LBB2_76
	.p2align	4, 0x90
.LBB2_73:                               # %.preheader202
                                        #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.LBB2_75
# BB#74:                                #   in Loop: Header=BB2_73 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %dl
	setne	%cl
	addl	%ecx, %ebp
	cmpl	%eax, %ebp
	jne	.LBB2_73
.LBB2_75:                               #   in Loop: Header=BB2_68 Depth=3
	addl	24(%rsp), %edi          # 4-byte Folded Reload
	movl	%edi, %r13d
.LBB2_76:                               #   in Loop: Header=BB2_68 Depth=3
	movq	112(%rsp), %rax
	movq	(%rax,%r8,8), %r10
	movl	$-1, %ebp
	movq	%r10, %rdi
	.p2align	4, 0x90
.LBB2_77:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %eax
	testb	%al, %al
	je	.LBB2_79
# BB#78:                                #   in Loop: Header=BB2_77 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %al
	setne	%cl
	addl	%ecx, %ebp
	cmpl	32(%rbx), %ebp
	jne	.LBB2_77
.LBB2_79:                               #   in Loop: Header=BB2_68 Depth=3
	movq	%rdi, %r11
	subq	%r10, %r11
	addq	%rsi, %r11
	movl	36(%rbx), %eax
	cmpl	%eax, 32(%rbx)
	movl	%r11d, %r15d
	je	.LBB2_83
	.p2align	4, 0x90
.LBB2_80:                               # %.preheader
                                        #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%rdi), %edx
	testb	%dl, %dl
	je	.LBB2_82
# BB#81:                                #   in Loop: Header=BB2_80 Depth=4
	incq	%rdi
	xorl	%ecx, %ecx
	cmpb	$45, %dl
	setne	%cl
	addl	%ecx, %ebp
	cmpl	%eax, %ebp
	jne	.LBB2_80
.LBB2_82:                               #   in Loop: Header=BB2_68 Depth=3
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	subl	%r10d, %eax
	addl	%eax, %edi
	movl	%edi, %r15d
.LBB2_83:                               #   in Loop: Header=BB2_68 Depth=3
	movq	%r8, %rdi
	movslq	%r9d, %r8
	addq	%r14, %r8
	movslq	%r11d, %rax
	addq	%rax, %r10
	.p2align	4, 0x90
.LBB2_84:                               #   Parent Loop BB2_64 Depth=1
                                        #     Parent Loop BB2_66 Depth=2
                                        #       Parent Loop BB2_68 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzbl	(%r8), %edx
	testb	%dl, %dl
	je	.LBB2_97
# BB#85:                                #   in Loop: Header=BB2_84 Depth=4
	movzbl	(%r10), %eax
	testb	%al, %al
	je	.LBB2_97
# BB#86:                                #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %dl
	je	.LBB2_89
# BB#87:                                #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %al
	je	.LBB2_89
# BB#88:                                #   in Loop: Header=BB2_84 Depth=4
	movss	64(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movslq	%r9d, %r9
	movq	(%r12,%r9,8), %rax
	movslq	%r11d, %r11
	addss	(%rax,%r11,4), %xmm3
	movss	%xmm3, (%rax,%r11,4)
	incl	%r9d
	incl	%r11d
	incq	%r8
	jmp	.LBB2_94
	.p2align	4, 0x90
.LBB2_89:                               #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %al
	setne	%cl
	cmpb	$45, %dl
	je	.LBB2_91
# BB#90:                                #   in Loop: Header=BB2_84 Depth=4
	testb	%cl, %cl
	je	.LBB2_93
.LBB2_91:                               #   in Loop: Header=BB2_84 Depth=4
	cmpb	$45, %dl
	jne	.LBB2_95
# BB#92:                                #   in Loop: Header=BB2_84 Depth=4
	incl	%r9d
	incq	%r8
	cmpb	$45, %al
	jne	.LBB2_95
	.p2align	4, 0x90
.LBB2_93:                               #   in Loop: Header=BB2_84 Depth=4
	incl	%r11d
.LBB2_94:                               # %.thread
                                        #   in Loop: Header=BB2_84 Depth=4
	incq	%r10
.LBB2_95:                               # %.thread
                                        #   in Loop: Header=BB2_84 Depth=4
	cmpl	%r15d, %r11d
	jg	.LBB2_97
# BB#96:                                # %.thread
                                        #   in Loop: Header=BB2_84 Depth=4
	cmpl	%r13d, %r9d
	jle	.LBB2_84
	.p2align	4, 0x90
.LBB2_97:                               # %.critedge
                                        #   in Loop: Header=BB2_68 Depth=3
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	movl	$4294967295, %esi       # imm = 0xFFFFFFFF
	movq	%rdi, %r8
	jne	.LBB2_68
.LBB2_98:                               # %._crit_edge
                                        #   in Loop: Header=BB2_66 Depth=2
	incq	%r8
	cmpq	40(%rsp), %r8           # 8-byte Folded Reload
	jne	.LBB2_66
.LBB2_99:                               # %._crit_edge214
                                        #   in Loop: Header=BB2_64 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movq	%rax, %rcx
	incq	%rcx
	movq	%rcx, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	32(%rsp), %rcx          # 8-byte Folded Reload
	movl	4(%rsp), %eax           # 4-byte Reload
	jne	.LBB2_64
.LBB2_100:                              # %._crit_edge216
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	imp_match_init_strictQ, .Lfunc_end2-imp_match_init_strictQ
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4608533498688228557     # double 1.3
.LCPI3_1:
	.quad	4607182418800017408     # double 1
.LCPI3_2:
	.quad	4602678819172646912     # double 0.5
.LCPI3_4:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_3:
	.long	1065353216              # float 1
.LCPI3_5:
	.long	1176256512              # float 1.0E+4
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_6:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI3_7:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI3_8:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI3_9:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI3_10:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.globl	Q__align
	.p2align	4, 0x90
	.type	Q__align,@function
Q__align:                               # @Q__align
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi25:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi26:
	.cfi_def_cfa_offset 56
	subq	$456, %rsp              # imm = 0x1C8
.Lcfi27:
	.cfi_def_cfa_offset 512
.Lcfi28:
	.cfi_offset %rbx, -56
.Lcfi29:
	.cfi_offset %r12, -48
.Lcfi30:
	.cfi_offset %r13, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movl	%r9d, 28(%rsp)          # 4-byte Spill
	movl	%r8d, %r15d
	movq	%rcx, %rbp
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %r13
	movq	%rdi, %rbx
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 68(%rsp)         # 4-byte Spill
	movl	Q__align.orlgth1(%rip), %r12d
	testl	%r12d, %r12d
	jne	.LBB3_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Q__align.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Q__align.mseq2(%rip)
	movl	Q__align.orlgth1(%rip), %r12d
.LBB3_2:
	movq	%rbx, 88(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %rbx
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %r10
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	cmpl	%r12d, %ebx
	movl	Q__align.orlgth2(%rip), %r14d
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%r10, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	jg	.LBB3_4
# BB#3:
	cmpl	%r14d, %r10d
	jle	.LBB3_9
.LBB3_4:
	testl	%r12d, %r12d
	jle	.LBB3_5
# BB#6:
	testl	%r14d, %r14d
	movq	56(%rsp), %rbx          # 8-byte Reload
	jle	.LBB3_8
# BB#7:
	movq	Q__align.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.match(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.m(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.mp(%rip), %rdi
	callq	FreeIntVec
	movq	Q__align.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	Q__align.digf1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.digf2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.diaf1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.diaf2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.gapz1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.gapz2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.gapf1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.gapf2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.ogcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.ogcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.fgcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.fgcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.og_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.og_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.fg_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.fg_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.og_t_fg_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.og_t_fg_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.fg_t_og_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.fg_t_og_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.gapz_n1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.gapz_n2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	Q__align.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	Q__align.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	Q__align.intwork(%rip), %rdi
	callq	FreeIntMtx
	movq	96(%rsp), %r10          # 8-byte Reload
	movl	Q__align.orlgth1(%rip), %r12d
	movl	Q__align.orlgth2(%rip), %r14d
	jmp	.LBB3_8
.LBB3_5:
	movq	56(%rsp), %rbx          # 8-byte Reload
.LBB3_8:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	movsd	.LCPI3_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r12d, %eax
	cmovgel	%eax, %r12d
	leal	100(%r12), %eax
	movl	%eax, 104(%rsp)         # 4-byte Spill
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r10d, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	leal	100(%r14), %r13d
	leal	102(%r14), %ebx
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.w1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.w2(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.match(%rip)
	leal	102(%r12), %ebp
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.initverticalw(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.lastverticalw(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.m(%rip)
	movl	%ebx, %edi
	callq	AllocateIntVec
	movq	%rax, Q__align.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r14,%r12), %esi
	callq	AllocateCharMtx
	movq	%rax, Q__align.mseq(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.digf1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.digf2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.diaf1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.diaf2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.gapz1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.gapz2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.gapf1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.gapf2(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.ogcp1g(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.ogcp2g(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.fgcp1g(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.fgcp2g(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.og_h_dg_n1_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.og_h_dg_n2_p(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.fg_h_dg_n1_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.fg_h_dg_n2_p(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.og_t_fg_h_dg_n1_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.og_t_fg_h_dg_n2_p(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.fg_t_og_h_dg_n1_p(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.fg_t_og_h_dg_n2_p(%rip)
	movl	%ebp, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.gapz_n1(%rip)
	movl	%ebx, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align.gapz_n2(%rip)
	movl	$26, %edi
	movl	%ebp, %esi
	callq	AllocateFloatMtx
	movq	%rax, Q__align.cpmx1(%rip)
	movl	$26, %edi
	movl	%ebx, %esi
	callq	AllocateFloatMtx
	movq	%rax, Q__align.cpmx2(%rip)
	movl	104(%rsp), %eax         # 4-byte Reload
	cmpl	%r13d, %eax
	cmovgel	%eax, %r13d
	addl	$2, %r13d
	movl	$26, %esi
	movl	%r13d, %edi
	callq	AllocateFloatMtx
	movq	%rax, Q__align.floatwork(%rip)
	movl	$27, %esi
	movl	%r13d, %edi
	callq	AllocateIntMtx
	movq	96(%rsp), %r10          # 8-byte Reload
	movq	%rax, Q__align.intwork(%rip)
	movl	%r12d, Q__align.orlgth1(%rip)
	movl	%r14d, Q__align.orlgth2(%rip)
	movq	72(%rsp), %r13          # 8-byte Reload
.LBB3_9:                                # %.preheader606
	testl	%r15d, %r15d
	jle	.LBB3_16
# BB#10:                                # %.lr.ph677
	movq	Q__align.mseq(%rip), %r9
	movq	Q__align.mseq1(%rip), %rdi
	movslq	56(%rsp), %rax          # 4-byte Folded Reload
	movl	%r15d, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%ebp, %ebp
	andq	$3, %rbx
	je	.LBB3_13
# BB#11:                                # %.prol.preheader1092
	movq	88(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_12:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rbp,8), %rdx
	movq	%rdx, (%rdi,%rbp,8)
	movq	(%rsi,%rbp,8), %rdx
	movb	$0, (%rdx,%rax)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_12
.LBB3_13:                               # %.prol.loopexit1093
	cmpq	$3, %r8
	jb	.LBB3_16
# BB#14:                                # %.lr.ph677.new
	subq	%rbp, %rcx
	movq	88(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rbp,8), %rdx
	leaq	24(%rdi,%rbp,8), %rdi
	leaq	24(%r9,%rbp,8), %rsi
	.p2align	4, 0x90
.LBB3_15:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rsi), %rbp
	movq	%rbp, (%rdi)
	movq	(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB3_15
.LBB3_16:                               # %.preheader605
	cmpl	$0, 28(%rsp)            # 4-byte Folded Reload
	jle	.LBB3_24
# BB#17:                                # %.lr.ph674
	movq	Q__align.mseq(%rip), %r8
	movq	Q__align.mseq2(%rip), %rdi
	movslq	%r10d, %rax
	movslq	%r15d, %r10
	movl	28(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %r9
	movq	%rcx, %rdx
	andq	$3, %rdx
	je	.LBB3_18
# BB#19:                                # %.prol.preheader1087
	leaq	(%r8,%r10,8), %rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_20:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movq	%rbp, (%rdi,%rbx,8)
	movq	(%r13,%rbx,8), %rbp
	movb	$0, (%rbp,%rax)
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB3_20
	jmp	.LBB3_21
.LBB3_18:
	xorl	%ebx, %ebx
.LBB3_21:                               # %.prol.loopexit1088
	cmpq	$3, %r9
	jb	.LBB3_24
# BB#22:                                # %.lr.ph674.new
	subq	%rbx, %rcx
	leaq	24(%r13,%rbx,8), %rsi
	leaq	24(%rdi,%rbx,8), %rdi
	addq	%rbx, %r10
	leaq	24(%r8,%r10,8), %rdx
	.p2align	4, 0x90
.LBB3_23:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rdx), %rbp
	movq	%rbp, (%rdi)
	movq	(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB3_23
.LBB3_24:                               # %._crit_edge675
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r12d
	movl	commonAlloc2(%rip), %ebp
	movl	%r15d, 52(%rsp)         # 4-byte Spill
	jg	.LBB3_27
# BB#25:                                # %._crit_edge675
	cmpl	%ebp, %r14d
	jg	.LBB3_27
# BB#26:                                # %._crit_edge747
	movq	commonIP(%rip), %rax
	movl	28(%rsp), %r12d         # 4-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	jmp	.LBB3_32
.LBB3_27:                               # %._crit_edge675._crit_edge
	testl	%ebx, %ebx
	je	.LBB3_28
# BB#29:                                # %._crit_edge675._crit_edge
	testl	%ebp, %ebp
	movq	56(%rsp), %r13          # 8-byte Reload
	je	.LBB3_31
# BB#30:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	Q__align.orlgth1(%rip), %r12d
	movl	commonAlloc1(%rip), %ebx
	movl	Q__align.orlgth2(%rip), %r14d
	movl	commonAlloc2(%rip), %ebp
	jmp	.LBB3_31
.LBB3_28:
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB3_31:
	cmpl	%ebx, %r12d
	cmovgel	%r12d, %ebx
	cmpl	%ebp, %r14d
	cmovgel	%r14d, %ebp
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
	movl	28(%rsp), %r12d         # 4-byte Reload
.LBB3_32:
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%rax, Q__align.ijp(%rip)
	movq	Q__align.cpmx1(%rip), %rsi
	movq	88(%rsp), %rdi          # 8-byte Reload
	movq	%rdi, %r15
	movq	%r14, %rdx
	movl	%r13d, %ecx
	movq	%r13, %rbp
	movl	52(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %r8d
	callq	cpmx_calc_new
	movq	Q__align.cpmx2(%rip), %rsi
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	96(%rsp), %r13          # 8-byte Reload
	movl	%r13d, %ecx
	movl	%r12d, %r8d
	callq	cpmx_calc_new
	movq	536(%rsp), %rax
	testq	%rax, %rax
	movq	Q__align.ogcp1g(%rip), %rdi
	je	.LBB3_34
# BB#33:
	movq	560(%rsp), %r12
	movq	552(%rsp), %rcx
	movq	%rcx, (%rsp)
	movl	%ebx, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movq	%rbp, %r14
	movl	%r14d, %r8d
	movq	%rax, %r9
	callq	new_OpeningGapCount_zure
	movq	Q__align.ogcp2g(%rip), %rdi
	movq	%r12, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	40(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rcx
	movl	%r13d, %r8d
	movq	544(%rsp), %r9
	callq	new_OpeningGapCount_zure
	movq	Q__align.fgcp1g(%rip), %rdi
	movq	552(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%ebx, %esi
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	32(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movl	%r14d, %r8d
	movq	536(%rsp), %r9
	callq	new_FinalGapCount_zure
	movq	Q__align.fgcp2g(%rip), %rdi
	movq	560(%rsp), %rax
	movq	%rax, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	movl	%r13d, %r8d
	movq	544(%rsp), %r9
	callq	new_FinalGapCount_zure
	movq	Q__align.digf1(%rip), %rdi
	movq	552(%rsp), %rax
	movq	%rax, (%rsp)
	movl	52(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %esi
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movl	%r14d, %r8d
	movq	%r14, %rbx
	movq	536(%rsp), %r9
	callq	getdigapfreq_part
	movq	Q__align.digf2(%rip), %rdi
	movq	560(%rsp), %rax
	movq	%rax, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rdx
	movq	%r13, %r15
	movq	40(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rcx
	movl	%r15d, %r8d
	movq	544(%rsp), %r9
	callq	getdigapfreq_part
	movq	Q__align.diaf1(%rip), %rdi
	movq	552(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r12d, %esi
	movq	%rbp, %rdx
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movl	%ebx, %r8d
	movq	536(%rsp), %r9
	callq	getdiaminofreq_part
	movq	Q__align.diaf2(%rip), %rdi
	movq	560(%rsp), %rax
	movq	%rax, (%rsp)
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	72(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	%r13, %rcx
	movl	%r15d, %r8d
	movq	544(%rsp), %r9
	movl	28(%rsp), %r12d         # 4-byte Reload
	callq	getdiaminofreq_part
	movq	Q__align.gapf1(%rip), %rdi
	movl	52(%rsp), %r13d         # 4-byte Reload
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getgapfreq
	movq	Q__align.gapf2(%rip), %rdi
	movl	%r12d, %esi
	movq	%rbx, %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	callq	getgapfreq
	movq	Q__align.gapz1(%rip), %rdi
	movl	%r13d, %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	%r14, %rcx
	movl	%ebp, %r8d
	movq	536(%rsp), %r14
	movq	%r14, %r9
	callq	getgapfreq_zure_part
	movq	Q__align.gapz2(%rip), %rdi
	movl	%r12d, %esi
	movq	72(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movq	%r14, %r9
	callq	getgapfreq_zure_part
	movq	%rbp, %r14
	jmp	.LBB3_35
.LBB3_34:
	movl	%ebx, %esi
	movq	%r15, %rdx
	movq	%r14, %rbx
	movq	%rbx, %rcx
	movq	%rbp, %r8
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	st_OpeningGapCount
	movq	Q__align.ogcp2g(%rip), %rdi
	movl	%r12d, %esi
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movl	%r13d, %r8d
	callq	st_OpeningGapCount
	movq	Q__align.fgcp1g(%rip), %rdi
	movl	52(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %esi
	movq	88(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	56(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	st_FinalGapCount_zure
	movq	Q__align.fgcp2g(%rip), %rdi
	movl	%r12d, %esi
	movq	%r15, %rdx
	movq	%r15, %r12
	movq	%r14, %rcx
	movq	%r14, %r15
	movl	%r13d, %r8d
	callq	st_FinalGapCount_zure
	movq	Q__align.digf1(%rip), %rdi
	movl	%ebp, %esi
	movq	88(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%rbx, %rcx
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	getdigapfreq_st
	movq	Q__align.digf2(%rip), %rdi
	movl	28(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %esi
	movq	%r12, %rdx
	movq	%r15, %rcx
	movl	%r13d, %r8d
	callq	getdigapfreq_st
	movq	Q__align.diaf1(%rip), %rdi
	movl	52(%rsp), %r15d         # 4-byte Reload
	movl	%r15d, %esi
	movq	%rbp, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movl	%ebx, %r8d
	callq	getdiaminofreq_x
	movq	Q__align.diaf2(%rip), %rdi
	movl	%r14d, %esi
	movq	%r12, %rdx
	movq	40(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rcx
	movl	%r13d, %r8d
	callq	getdiaminofreq_x
	movq	Q__align.gapf1(%rip), %rdi
	movl	%r15d, %esi
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movq	56(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getgapfreq
	movq	Q__align.gapf2(%rip), %rdi
	movl	%r14d, %esi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movl	%r13d, %r8d
	callq	getgapfreq
	movq	Q__align.gapz1(%rip), %rdi
	movl	52(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getgapfreq_zure
	movq	Q__align.gapz2(%rip), %rdi
	movl	%r14d, %esi
	movq	%r12, %rdx
	movq	%rbx, %rcx
	movl	%r13d, %r8d
	callq	getgapfreq_zure
	movq	%rbp, %r14
	movq	%r13, %r15
.LBB3_35:
	movss	68(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	cmpl	$-1, %r15d
	jl	.LBB3_38
# BB#36:                                # %.lr.ph671
	movq	Q__align.ogcp2g(%rip), %rax
	movq	Q__align.digf2(%rip), %rcx
	cvtss2sd	%xmm12, %xmm0
	movq	Q__align.og_h_dg_n2_p(%rip), %r11
	movq	Q__align.fgcp2g(%rip), %rsi
	movq	Q__align.fg_h_dg_n2_p(%rip), %r9
	movq	Q__align.og_t_fg_h_dg_n2_p(%rip), %rbp
	movq	Q__align.fg_t_og_h_dg_n2_p(%rip), %rbx
	movq	Q__align.gapz2(%rip), %rdx
	movq	Q__align.gapz_n2(%rip), %rdi
	leal	2(%r15), %r10d
	movsd	.LCPI3_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movss	.LCPI3_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB3_37:                               # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r11)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r9)
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbp)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbx)
	movaps	%xmm3, %xmm4
	subss	(%rdx), %xmm4
	movss	%xmm4, (%rdi)
	addq	$4, %rax
	addq	$4, %rcx
	addq	$4, %r11
	addq	$4, %rsi
	addq	$4, %r9
	addq	$4, %rbp
	addq	$4, %rbx
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%r10
	jne	.LBB3_37
.LBB3_38:                               # %._crit_edge672
	movq	%r14, %r13
	cmpl	$-1, %r13d
	movq	72(%rsp), %r12          # 8-byte Reload
	jl	.LBB3_41
# BB#39:                                # %.lr.ph667
	movq	Q__align.ogcp1g(%rip), %rax
	movq	Q__align.digf1(%rip), %rcx
	cvtss2sd	%xmm12, %xmm0
	movq	Q__align.og_h_dg_n1_p(%rip), %r11
	movq	Q__align.fgcp1g(%rip), %rsi
	movq	Q__align.fg_h_dg_n1_p(%rip), %r9
	movq	Q__align.og_t_fg_h_dg_n1_p(%rip), %rbp
	movq	Q__align.fg_t_og_h_dg_n1_p(%rip), %rbx
	movq	Q__align.gapz1(%rip), %rdx
	movq	Q__align.gapz_n1(%rip), %rdi
	leal	2(%r13), %r10d
	movsd	.LCPI3_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movss	.LCPI3_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB3_40:                               # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r11)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r9)
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbp)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbx)
	movaps	%xmm3, %xmm4
	subss	(%rdx), %xmm4
	movss	%xmm4, (%rdi)
	addq	$4, %rax
	addq	$4, %rcx
	addq	$4, %r11
	addq	$4, %rsi
	addq	$4, %r9
	addq	$4, %rbp
	addq	$4, %rbx
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%r10
	jne	.LBB3_40
.LBB3_41:                               # %._crit_edge668
	movq	520(%rsp), %rbx
	movabsq	$17179869180, %rcx      # imm = 0x3FFFFFFFC
	movq	Q__align.w1(%rip), %rbp
	movq	Q__align.w2(%rip), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	cmpb	$114, RNAscoremtx(%rip)
	movq	Q__align.initverticalw(%rip), %rdi
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	jne	.LBB3_42
# BB#43:
	testl	%r13d, %r13d
	je	.LBB3_44
# BB#45:                                # %.lr.ph.preheader.i
	leaq	(%rcx,%r13,4), %rdx
	andq	%rcx, %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	callq	memset
	jmp	.LBB3_46
.LBB3_42:
	movq	Q__align.cpmx2(%rip), %rsi
	movq	Q__align.cpmx1(%rip), %rdx
	movq	Q__align.floatwork(%rip), %r9
	movq	Q__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movl	%r13d, %r8d
	callq	match_calc
.LBB3_46:                               # %clearvec.exit
	movss	68(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movq	96(%rsp), %r15          # 8-byte Reload
	testq	%rbx, %rbx
	setne	%r14b
	je	.LBB3_56
# BB#47:                                # %clearvec.exit
	testl	%r13d, %r13d
	jle	.LBB3_56
# BB#48:                                # %.lr.ph.i
	movq	Q__align.initverticalw(%rip), %rax
	movq	impmtx(%rip), %r9
	movl	%r13d, %ecx
	leaq	-1(%rcx), %rsi
	movq	%r13, %rbp
	andq	$3, %rbp
	je	.LBB3_49
# BB#50:                                # %.prol.preheader1082
	negq	%rbp
	xorl	%edi, %edi
	movq	%r9, %rdx
	.p2align	4, 0x90
.LBB3_51:                               # =>This Inner Loop Header: Depth=1
	movq	(%rdx), %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	leaq	4(%rax), %rax
	decq	%rdi
	addq	$8, %rdx
	cmpq	%rdi, %rbp
	jne	.LBB3_51
# BB#52:                                # %.prol.loopexit1083.unr-lcssa
	negq	%rdi
	jmp	.LBB3_53
.LBB3_44:                               # %clearvec.exit.thread
	testq	%rbx, %rbx
	setne	%r14b
	cmpb	$114, RNAscoremtx(%rip)
	je	.LBB3_58
	jmp	.LBB3_57
.LBB3_49:
	xorl	%edi, %edi
.LBB3_53:                               # %.prol.loopexit1083
	movq	32(%rsp), %rbp          # 8-byte Reload
	cmpq	$3, %rsi
	jb	.LBB3_56
# BB#54:                                # %.lr.ph.i.new
	subq	%rdi, %rcx
	leaq	24(%r9,%rdi,8), %rdx
	.p2align	4, 0x90
.LBB3_55:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	movq	-16(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	4(%rax), %xmm0
	movss	%xmm0, 4(%rax)
	movq	-8(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	8(%rax), %xmm0
	movss	%xmm0, 8(%rax)
	movq	(%rdx), %rsi
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	12(%rax), %xmm0
	movss	%xmm0, 12(%rax)
	addq	$32, %rdx
	addq	$16, %rax
	addq	$-4, %rcx
	jne	.LBB3_55
.LBB3_56:                               # %imp_match_out_vead_tateQ.exit
	cmpb	$114, RNAscoremtx(%rip)
	jne	.LBB3_57
.LBB3_58:
	testl	%r15d, %r15d
	je	.LBB3_59
# BB#60:                                # %.lr.ph.preheader.i574
	movabsq	$17179869180, %rax      # imm = 0x3FFFFFFFC
	leaq	(%rax,%r15,4), %rdx
	andq	%rax, %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	movq	%rbp, %rdi
	callq	memset
	jmp	.LBB3_61
.LBB3_57:
	movq	Q__align.cpmx1(%rip), %rsi
	movq	Q__align.cpmx2(%rip), %rdx
	movq	Q__align.floatwork(%rip), %r9
	movq	Q__align.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	%rbp, %rdi
	movl	%r15d, %r8d
	callq	match_calc
.LBB3_61:                               # %clearvec.exit575
	movq	96(%rsp), %r15          # 8-byte Reload
	movl	%r14d, %r9d
	xorb	$1, %r9b
	testl	%r15d, %r15d
	sete	%al
	movb	%al, 136(%rsp)          # 1-byte Spill
	orb	%al, %r9b
	je	.LBB3_63
# BB#62:
	movb	$1, %r9b
	movss	68(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	jmp	.LBB3_78
.LBB3_63:                               # %.lr.ph.preheader.i576
	movq	impmtx(%rip), %rax
	movq	(%rax), %rsi
	leal	-1(%r15), %edi
	incq	%rdi
	cmpq	$8, %rdi
	movss	68(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	jae	.LBB3_65
# BB#64:
	movq	%rsi, %rax
	movl	%r15d, %ecx
	movq	%rbp, %rdx
	jmp	.LBB3_73
.LBB3_65:                               # %min.iters.checked
	movl	%r15d, %ebp
	andl	$7, %ebp
	subq	%rbp, %rdi
	je	.LBB3_66
# BB#67:                                # %vector.memcheck
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	addl	%r15d, %eax
	leaq	4(%rsi,%rax,4), %rcx
	movq	32(%rsp), %rbx          # 8-byte Reload
	cmpq	%rcx, %rbx
	jae	.LBB3_70
# BB#68:                                # %vector.memcheck
	leaq	4(%rbx,%rax,4), %rax
	cmpq	%rax, %rsi
	jae	.LBB3_70
# BB#69:
	movq	%rsi, %rax
	movl	%r15d, %ecx
	movq	%rbx, %rdx
	movq	%rbx, %rbp
	jmp	.LBB3_73
.LBB3_59:
	movb	$1, %r9b
	movb	$1, 136(%rsp)           # 1-byte Folded Spill
	jmp	.LBB3_78
.LBB3_66:
	movq	%rsi, %rax
	movl	%r15d, %ecx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	jmp	.LBB3_73
.LBB3_70:                               # %vector.body.preheader
	leaq	(%rsi,%rdi,4), %rax
	movl	%r15d, %ecx
	subl	%edi, %ecx
	leaq	(%rbx,%rdi,4), %rdx
	leaq	16(%rbx), %rbx
	addq	$16, %rsi
	.p2align	4, 0x90
.LBB3_71:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	-16(%rbx), %xmm2
	movups	(%rbx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rbx)
	movups	%xmm3, (%rbx)
	addq	$32, %rbx
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB3_71
# BB#72:                                # %middle.block
	testq	%rbp, %rbp
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_78
.LBB3_73:                               # %.lr.ph.i577.preheader
	leal	-1(%rcx), %esi
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB3_76
# BB#74:                                # %.lr.ph.i577.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB3_75:                               # %.lr.ph.i577.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rax
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	leaq	4(%rdx), %rdx
	incl	%edi
	jne	.LBB3_75
.LBB3_76:                               # %.lr.ph.i577.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB3_78
	.p2align	4, 0x90
.LBB3_77:                               # %.lr.ph.i577
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 4(%rdx)
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	movss	%xmm0, 8(%rdx)
	addl	$-4, %ecx
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rax), %rax
	addss	12(%rdx), %xmm0
	movss	%xmm0, 12(%rdx)
	leaq	16(%rdx), %rdx
	jne	.LBB3_77
.LBB3_78:                               # %imp_match_out_veadQ.exit
	cmpl	$1, outgap(%rip)
	movb	%r14b, 27(%rsp)         # 1-byte Spill
	movb	%r9b, 168(%rsp)         # 1-byte Spill
	jne	.LBB3_79
# BB#89:
	movq	Q__align.ogcp1g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	Q__align.og_h_dg_n2_p(%rip), %rax
	mulss	(%rax), %xmm0
	xorpd	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movq	Q__align.ogcp2g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	Q__align.og_h_dg_n1_p(%rip), %rax
	mulss	(%rax), %xmm0
	addss	%xmm1, %xmm0
	movq	Q__align.fgcp1g(%rip), %rax
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movq	Q__align.fg_h_dg_n2_p(%rip), %rax
	mulss	(%rax), %xmm1
	addss	%xmm0, %xmm1
	movq	Q__align.fgcp2g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	Q__align.fg_h_dg_n1_p(%rip), %rax
	mulss	(%rax), %xmm0
	addss	%xmm1, %xmm0
	movq	Q__align.initverticalw(%rip), %r14
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
	addss	(%rbp), %xmm0
	movss	%xmm0, (%rbp)
	testl	%r13d, %r13d
	jle	.LBB3_105
# BB#90:                                # %.lr.ph660
	movq	Q__align.gapz_n2(%rip), %rax
	movq	Q__align.og_t_fg_h_dg_n1_p(%rip), %r9
	movq	Q__align.fg_t_og_h_dg_n1_p(%rip), %r8
	leaq	1(%r13), %r11
	movl	%r11d, %edx
	leaq	-1(%rdx), %r10
	cmpq	$3, %r10
	jbe	.LBB3_91
# BB#97:                                # %min.iters.checked781
	movl	%r13d, %ecx
	andl	$3, %ecx
	movq	%r10, %rbp
	subq	%rcx, %rbp
	je	.LBB3_91
# BB#98:                                # %vector.memcheck808
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	movq	%r8, %r15
	leaq	4(%rax), %r8
	leaq	4(%r14), %rbx
	leaq	(%r14,%rdx,4), %rdi
	leaq	4(%r15), %rsi
	movq	%r15, 40(%rsp)          # 8-byte Spill
	leaq	(%r15,%rdx,4), %r15
	cmpq	%rax, %r14
	sbbb	%r12b, %r12b
	cmpq	%rdi, %r8
	sbbb	%r8b, %r8b
	andb	%r12b, %r8b
	cmpq	%r9, %rbx
	sbbb	%r13b, %r13b
	cmpq	%rdi, %r9
	sbbb	%cl, %cl
	movb	%cl, 80(%rsp)           # 1-byte Spill
	cmpq	%r15, %rbx
	sbbb	%r12b, %r12b
	cmpq	%rdi, %rsi
	sbbb	%dil, %dil
	movl	$1, %r15d
	testb	$1, %r8b
	jne	.LBB3_99
# BB#100:                               # %vector.memcheck808
	andb	80(%rsp), %r13b         # 1-byte Folded Reload
	andb	$1, %r13b
	jne	.LBB3_99
# BB#101:                               # %vector.memcheck808
	andb	%dil, %r12b
	andb	$1, %r12b
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	jne	.LBB3_92
# BB#102:                               # %vector.body777.preheader
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%r9), %xmm1            # xmm1 = mem[0],zero,zero,zero
	mulps	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	1(%rbp), %r15
	.p2align	4, 0x90
.LBB3_103:                              # %vector.body777
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx), %xmm2
	addps	%xmm1, %xmm2
	movups	%xmm2, (%rbx)
	movups	(%rsi), %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movups	%xmm3, (%rbx)
	addq	$16, %rbx
	addq	$16, %rsi
	addq	$-4, %rbp
	jne	.LBB3_103
# BB#104:                               # %middle.block778
	cmpq	$0, 152(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_92
	jmp	.LBB3_105
.LBB3_79:                               # %.preheader604
	testl	%r15d, %r15d
	jle	.LBB3_86
# BB#80:                                # %.lr.ph664
	movslq	offset(%rip), %rax
	leaq	1(%r15), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB3_81
# BB#82:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI3_4(%rip), %xmm0
	movq	32(%rsp), %rdx          # 8-byte Reload
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rdx)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB3_84
	jmp	.LBB3_86
.LBB3_91:
	movl	$1, %r15d
.LBB3_92:                               # %scalar.ph779.preheader
	subl	%r15d, %r11d
	testb	$1, %r11b
	movq	%r15, %rdi
	je	.LBB3_94
# BB#93:                                # %scalar.ph779.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r9), %xmm0
	addss	(%r14,%r15,4), %xmm0
	movss	%xmm0, (%r14,%r15,4)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%r8,%r15,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14,%r15,4)
	leaq	1(%r15), %rdi
.LBB3_94:                               # %scalar.ph779.prol.loopexit
	cmpq	%r15, %r10
	je	.LBB3_105
# BB#95:                                # %scalar.ph779.preheader.new
	subq	%rdi, %rdx
	leaq	4(%r14,%rdi,4), %rsi
	leaq	4(%r8,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB3_96:                               # %scalar.ph779
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r9), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%r9), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB3_96
.LBB3_105:                              # %.preheader600
	movq	96(%rsp), %r8           # 8-byte Reload
	testl	%r8d, %r8d
	jle	.LBB3_106
# BB#107:                               # %.lr.ph657
	movq	Q__align.gapz_n1(%rip), %rax
	movq	Q__align.og_t_fg_h_dg_n2_p(%rip), %rcx
	movq	Q__align.fg_t_og_h_dg_n2_p(%rip), %r15
	movq	%r8, %r9
	leaq	1(%r9), %r11
	movl	%r11d, %edx
	leaq	-1(%rdx), %r10
	cmpq	$3, %r10
	jbe	.LBB3_108
# BB#114:                               # %min.iters.checked825
                                        # kill: %R9D<def> %R9D<kill> %R9<kill> %R9<def>
	andl	$3, %r9d
	movq	%r10, %rdi
	subq	%r9, %rdi
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_108
# BB#115:                               # %vector.memcheck854
	leaq	4(%rax), %r14
	leaq	4(%rbp), %rsi
	leaq	(%rbp,%rdx,4), %rbx
	movq	%rbp, %r8
	leaq	4(%r15), %rbp
	movq	%r15, 40(%rsp)          # 8-byte Spill
	leaq	(%r15,%rdx,4), %r15
	cmpq	%rax, %r8
	sbbb	%r12b, %r12b
	cmpq	%rbx, %r14
	sbbb	%r8b, %r8b
	andb	%r12b, %r8b
	cmpq	%rcx, %rsi
	sbbb	%r12b, %r12b
	cmpq	%rbx, %rcx
	sbbb	%r13b, %r13b
	cmpq	%r15, %rsi
	sbbb	%r14b, %r14b
	cmpq	%rbx, %rbp
	sbbb	%r15b, %r15b
	movl	$1, %ebx
	testb	$1, %r8b
	jne	.LBB3_116
# BB#117:                               # %vector.memcheck854
	andb	%r13b, %r12b
	andb	$1, %r12b
	jne	.LBB3_116
# BB#118:                               # %vector.memcheck854
	andb	%r15b, %r14b
	andb	$1, %r14b
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movb	27(%rsp), %r14b         # 1-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jne	.LBB3_109
# BB#119:                               # %vector.body821.preheader
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulps	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	1(%rdi), %rbx
	.p2align	4, 0x90
.LBB3_120:                              # %vector.body821
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi), %xmm2
	addps	%xmm1, %xmm2
	movups	%xmm2, (%rsi)
	movups	(%rbp), %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movups	%xmm3, (%rsi)
	addq	$16, %rsi
	addq	$16, %rbp
	addq	$-4, %rdi
	jne	.LBB3_120
# BB#121:                               # %middle.block822
	testq	%r9, %r9
	jne	.LBB3_109
	jmp	.LBB3_126
.LBB3_108:
	movl	$1, %ebx
	movb	27(%rsp), %r14b         # 1-byte Reload
.LBB3_109:                              # %scalar.ph823.preheader
	subl	%ebx, %r11d
	testb	$1, %r11b
	movq	%rbx, %rdi
	je	.LBB3_111
# BB#110:                               # %scalar.ph823.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	movq	32(%rsp), %rsi          # 8-byte Reload
	addss	(%rsi,%rbx,4), %xmm0
	movss	%xmm0, (%rsi,%rbx,4)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%r15,%rbx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%rbx,4)
	leaq	1(%rbx), %rdi
.LBB3_111:                              # %scalar.ph823.prol.loopexit
	cmpq	%rbx, %r10
	je	.LBB3_126
# BB#112:                               # %scalar.ph823.preheader.new
	subq	%rdi, %rdx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi,%rdi,4), %rsi
	leaq	4(%r15,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB3_113:                              # %scalar.ph823
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB3_113
	jmp	.LBB3_126
.LBB3_106:                              # %.loopexit601.thread
	movq	Q__align.m(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, (%rax)
	movb	$1, 128(%rsp)           # 1-byte Folded Spill
	movl	28(%rsp), %esi          # 4-byte Reload
	movb	27(%rsp), %r14b         # 1-byte Reload
	movq	104(%rsp), %r10         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_144
.LBB3_81:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB3_86
.LBB3_84:                               # %.lr.ph664.new
	subq	%rdx, %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_85:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB3_85
.LBB3_86:                               # %.preheader602
	testl	%r13d, %r13d
	jle	.LBB3_126
# BB#87:                                # %.lr.ph662
	movq	Q__align.initverticalw(%rip), %rsi
	movslq	offset(%rip), %rax
	leaq	1(%r13), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB3_88
# BB#122:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI3_4(%rip), %xmm0
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB3_124
	jmp	.LBB3_126
.LBB3_88:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB3_126
.LBB3_124:                              # %.lr.ph662.new
	subq	%rdx, %rcx
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_125:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB3_125
.LBB3_126:                              # %.loopexit601
	movq	Q__align.m(%rip), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$0, (%rax)
	movq	96(%rsp), %r8           # 8-byte Reload
	testl	%r8d, %r8d
	setle	128(%rsp)               # 1-byte Folded Spill
	jle	.LBB3_127
# BB#128:                               # %.lr.ph654
	movq	Q__align.mp(%rip), %rax
	movss	.LCPI3_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	leaq	1(%r8), %rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %r11
	cmpq	$8, %r11
	jae	.LBB3_130
# BB#129:
	movl	$1, %edx
	movq	104(%rsp), %r10         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_137
.LBB3_127:
	movb	$1, 128(%rsp)           # 1-byte Folded Spill
	movq	104(%rsp), %r10         # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB3_144
.LBB3_130:                              # %min.iters.checked871
	movq	%r8, %r9
                                        # kill: %R8D<def> %R8D<kill> %R8<kill> %R8<def>
	andl	$7, %r8d
	movq	%r11, %rsi
	subq	%r8, %rsi
	movq	104(%rsp), %r10         # 8-byte Reload
	movq	32(%rsp), %rbp          # 8-byte Reload
	je	.LBB3_131
# BB#132:                               # %vector.memcheck886
	movq	40(%rsp), %r15          # 8-byte Reload
	leaq	4(%r15), %rdx
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	-4(%rbx,%rcx,4), %rbp
	cmpq	%rbp, %rdx
	movq	32(%rsp), %rbp          # 8-byte Reload
	jae	.LBB3_134
# BB#133:                               # %vector.memcheck886
	leaq	(%r15,%rcx,4), %rdx
	cmpq	%rdx, %rbp
	jae	.LBB3_134
.LBB3_131:
	movl	$1, %edx
	movq	%r9, %r8
	jmp	.LBB3_137
.LBB3_134:                              # %vector.ph887
	leaq	1(%rsi), %rdx
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movq	%rbp, %rbx
	xorl	%ebp, %ebp
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB3_135:                              # %vector.body867
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm2, 4(%rax,%rbp,4)
	movupd	%xmm2, 20(%rax,%rbp,4)
	movups	(%rbx,%rbp,4), %xmm3
	movups	16(%rbx,%rbp,4), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm1, %xmm4
	movups	%xmm3, 4(%r15,%rbp,4)
	movups	%xmm4, 20(%r15,%rbp,4)
	addq	$8, %rbp
	cmpq	%rbp, %rsi
	jne	.LBB3_135
# BB#136:                               # %middle.block868
	testq	%r8, %r8
	movq	%rbx, %rbp
	movq	%r9, %r8
	je	.LBB3_143
.LBB3_137:                              # %scalar.ph869.preheader
	subl	%edx, %edi
	subq	%rdx, %r11
	andq	$3, %rdi
	je	.LBB3_140
# BB#138:                               # %scalar.ph869.prol.preheader
	negq	%rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_139:                              # %scalar.ph869.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rdx,4)
	movss	-4(%rbp,%rdx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB3_139
.LBB3_140:                              # %scalar.ph869.prol.loopexit
	cmpq	$3, %r11
	jb	.LBB3_143
# BB#141:                               # %scalar.ph869.preheader.new
	movq	40(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_142:                              # %scalar.ph869
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rdx,4)
	movss	-4(%rbp,%rdx,4), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi,%rdx,4)
	movl	$0, 4(%rax,%rdx,4)
	movss	(%rbp,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%rsi,%rdx,4)
	movl	$0, 8(%rax,%rdx,4)
	movss	4(%rbp,%rdx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsi,%rdx,4)
	movl	$0, 12(%rax,%rdx,4)
	movss	8(%rbp,%rdx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsi,%rdx,4)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB3_142
.LBB3_143:
	movl	28(%rsp), %esi          # 4-byte Reload
.LBB3_144:                              # %._crit_edge655
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	cmpb	$0, 136(%rsp)           # 1-byte Folded Reload
	xorps	%xmm0, %xmm0
	jne	.LBB3_146
# BB#145:
	movq	%r8, %rcx
	shlq	$32, %rcx
	addq	%rax, %rcx
	sarq	$30, %rcx
	movss	(%rbp,%rcx), %xmm0      # xmm0 = mem[0],zero,zero,zero
.LBB3_146:
	movq	Q__align.lastverticalw(%rip), %rbx
	movss	%xmm0, (%rbx)
	movl	outgap(%rip), %edx
	cmpl	$1, %edx
	movl	%r13d, %ecx
	sbbl	$-1, %ecx
	cmpl	$2, %ecx
	movb	168(%rsp), %r9b         # 1-byte Reload
	movq	%rbx, 144(%rsp)         # 8-byte Spill
	jl	.LBB3_147
# BB#148:                               # %.lr.ph646
	movq	Q__align.initverticalw(%rip), %rdx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movabsq	$17179869180, %rdx      # imm = 0x3FFFFFFFC
	leaq	(%rdx,%r8,4), %rsi
	andq	%rdx, %rsi
	addq	$4, %rsi
	movq	%rsi, 240(%rsp)         # 8-byte Spill
	mulss	.LCPI3_5(%rip), %xmm12
	movq	Q__align.ijp(%rip), %rdx
	movq	%rdx, 328(%rsp)         # 8-byte Spill
	movq	Q__align.mp(%rip), %rdx
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	Q__align.fg_t_og_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 104(%rsp)         # 8-byte Spill
	movq	Q__align.og_t_fg_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	movq	Q__align.og_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movq	%r8, %rdx
	shlq	$32, %rdx
	addq	%rax, %rdx
	sarq	$32, %rdx
	movq	%rdx, 344(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	movq	%rax, 336(%rsp)         # 8-byte Spill
	leal	-1(%r8), %eax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	incq	%rax
	movabsq	$8589934584, %rcx       # imm = 0x1FFFFFFF8
	movq	%rax, 216(%rsp)         # 8-byte Spill
	andq	%rax, %rcx
	leaq	-8(%rcx), %rax
	shrq	$3, %rax
	movl	%r8d, %edx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	subl	%ecx, %edx
	movl	%edx, 212(%rsp)         # 4-byte Spill
	movq	%rax, 232(%rsp)         # 8-byte Spill
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill> %RAX<def>
	andl	$1, %eax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	xorps	%xmm4, %xmm4
	movq	Q__align.fg_h_dg_n2_p(%rip), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	movq	Q__align.gapz_n2(%rip), %r13
	movq	Q__align.fgcp2g(%rip), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	movq	Q__align.ogcp2g(%rip), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movq	Q__align.fg_t_og_h_dg_n1_p(%rip), %rax
	movq	%rax, 320(%rsp)         # 8-byte Spill
	movq	Q__align.og_t_fg_h_dg_n1_p(%rip), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	Q__align.og_h_dg_n1_p(%rip), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	Q__align.fg_h_dg_n1_p(%rip), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	Q__align.gapz_n1(%rip), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	Q__align.fgcp1g(%rip), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	movq	Q__align.ogcp1g(%rip), %rax
	movq	%rax, 272(%rsp)         # 8-byte Spill
	movq	impmtx(%rip), %rax
	movq	%rax, 264(%rsp)         # 8-byte Spill
	movq	Q__align.cpmx1(%rip), %r12
	movq	Q__align.floatwork(%rip), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movq	Q__align.intwork(%rip), %rax
	movq	%rax, 248(%rsp)         # 8-byte Spill
	movl	$1, %r14d
	movss	%xmm12, 68(%rsp)        # 4-byte Spill
	jmp	.LBB3_149
.LBB3_168:                              # %vector.body902.preheader
                                        #   in Loop: Header=BB3_149 Depth=1
	cmpq	$0, 224(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_169
# BB#170:                               # %vector.body902.prol
                                        #   in Loop: Header=BB3_149 Depth=1
	movups	(%rax), %xmm0
	movups	16(%rax), %xmm1
	movups	(%r15), %xmm2
	movups	16(%r15), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, (%r15)
	movups	%xmm3, 16(%r15)
	movl	$8, %esi
	cmpq	$0, 232(%rsp)           # 8-byte Folded Reload
	jne	.LBB3_172
	jmp	.LBB3_174
.LBB3_169:                              #   in Loop: Header=BB3_149 Depth=1
	xorl	%esi, %esi
	cmpq	$0, 232(%rsp)           # 8-byte Folded Reload
	je	.LBB3_174
.LBB3_172:                              # %vector.body902.preheader.new
                                        #   in Loop: Header=BB3_149 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	subq	%rsi, %rcx
	leaq	48(%r15,%rsi,4), %rdx
	leaq	48(%rax,%rsi,4), %rsi
	.p2align	4, 0x90
.LBB3_173:                              # %vector.body902
                                        #   Parent Loop BB3_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	-48(%rdx), %xmm2
	movups	-32(%rdx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -48(%rdx)
	movups	%xmm3, -32(%rdx)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	-16(%rdx), %xmm2
	movups	(%rdx), %xmm3
	addps	%xmm0, %xmm2
	addps	%xmm1, %xmm3
	movups	%xmm2, -16(%rdx)
	movups	%xmm3, (%rdx)
	addq	$64, %rdx
	addq	$64, %rsi
	addq	$-16, %rcx
	jne	.LBB3_173
.LBB3_174:                              # %middle.block903
                                        #   in Loop: Header=BB3_149 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	cmpq	%rcx, 216(%rsp)         # 8-byte Folded Reload
	je	.LBB3_181
# BB#175:                               #   in Loop: Header=BB3_149 Depth=1
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rax,%rcx,4), %rax
	leaq	(%r15,%rcx,4), %rdx
	movl	212(%rsp), %ecx         # 4-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	jmp	.LBB3_176
	.p2align	4, 0x90
.LBB3_149:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_151 Depth 2
                                        #       Child Loop BB3_152 Depth 3
                                        #     Child Loop BB3_155 Depth 2
                                        #       Child Loop BB3_157 Depth 3
                                        #     Child Loop BB3_173 Depth 2
                                        #     Child Loop BB3_178 Depth 2
                                        #     Child Loop BB3_180 Depth 2
                                        #     Child Loop BB3_183 Depth 2
	movq	%r10, %r15
	movq	%rbp, %r10
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	-4(%rax,%r14,4), %eax
	movl	%eax, (%r10)
	cmpb	$114, RNAscoremtx(%rip)
	jne	.LBB3_150
# BB#160:                               #   in Loop: Header=BB3_149 Depth=1
	cmpb	$0, 136(%rsp)           # 1-byte Folded Reload
	jne	.LBB3_162
# BB#161:                               # %.lr.ph.preheader.i584
                                        #   in Loop: Header=BB3_149 Depth=1
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	240(%rsp), %rdx         # 8-byte Reload
	movq	%r10, %rbx
	movss	%xmm4, 116(%rsp)        # 4-byte Spill
	movl	%r9d, %ebp
	callq	memset
	movl	%ebp, %r9d
	movss	116(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	%rbx, %r10
	movq	144(%rsp), %rbx         # 8-byte Reload
	movss	68(%rsp), %xmm12        # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movq	96(%rsp), %r8           # 8-byte Reload
	jmp	.LBB3_162
	.p2align	4, 0x90
.LBB3_150:                              #   in Loop: Header=BB3_149 Depth=1
	movl	$n_dis_consweight_multi, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_151:                              #   Parent Loop BB3_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_152 Depth 3
	movl	$0, 352(%rsp,%rcx,4)
	xorps	%xmm0, %xmm0
	movl	$1, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB3_152:                              #   Parent Loop BB3_149 Depth=1
                                        #     Parent Loop BB3_151 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%r12,%rdx,8), %rdi
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi,%r14,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%r12,%rdx,8), %rdi
	mulss	(%rdi,%r14,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rsi
	addq	$2, %rdx
	cmpq	$27, %rdx
	jne	.LBB3_152
# BB#153:                               #   in Loop: Header=BB3_151 Depth=2
	movss	%xmm0, 352(%rsp,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	cmpq	$26, %rcx
	jne	.LBB3_151
# BB#154:                               # %.preheader.i578
                                        #   in Loop: Header=BB3_149 Depth=1
	cmpb	$0, 136(%rsp)           # 1-byte Folded Reload
	movl	%r8d, %eax
	movq	248(%rsp), %rcx         # 8-byte Reload
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	%r15, %rsi
	jne	.LBB3_159
	.p2align	4, 0x90
.LBB3_155:                              # %.lr.ph84.i
                                        #   Parent Loop BB3_149 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_157 Depth 3
	decl	%eax
	movl	$0, (%rsi)
	movq	(%rcx), %rdi
	movl	(%rdi), %ebx
	testl	%ebx, %ebx
	js	.LBB3_158
# BB#156:                               # %.lr.ph.preheader.i582
                                        #   in Loop: Header=BB3_155 Depth=2
	movq	(%rdx), %rbp
	addq	$4, %rdi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB3_157:                              # %.lr.ph.i583
                                        #   Parent Loop BB3_149 Depth=1
                                        #     Parent Loop BB3_155 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebx, %rbx
	movss	352(%rsp,%rbx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rbp), %xmm1
	addq	$4, %rbp
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rsi)
	movl	(%rdi), %ebx
	addq	$4, %rdi
	testl	%ebx, %ebx
	jns	.LBB3_157
.LBB3_158:                              # %._crit_edge.i
                                        #   in Loop: Header=BB3_155 Depth=2
	addq	$8, %rcx
	addq	$8, %rdx
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB3_155
.LBB3_159:                              # %match_calc.exit
                                        #   in Loop: Header=BB3_149 Depth=1
	movq	144(%rsp), %rbx         # 8-byte Reload
.LBB3_162:                              # %clearvec.exit586
                                        #   in Loop: Header=BB3_149 Depth=1
	testb	$1, %r9b
	movq	40(%rsp), %rbp          # 8-byte Reload
	jne	.LBB3_181
# BB#163:                               # %.lr.ph.preheader.i587
                                        #   in Loop: Header=BB3_149 Depth=1
	movq	216(%rsp), %rdx         # 8-byte Reload
	cmpq	$8, %rdx
	movq	264(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rax
	jb	.LBB3_164
# BB#165:                               # %min.iters.checked906
                                        #   in Loop: Header=BB3_149 Depth=1
	cmpq	$0, 160(%rsp)           # 8-byte Folded Reload
	je	.LBB3_164
# BB#166:                               # %vector.memcheck919
                                        #   in Loop: Header=BB3_149 Depth=1
	leaq	(%rax,%rdx,4), %rcx
	cmpq	%rcx, %r15
	jae	.LBB3_168
# BB#167:                               # %vector.memcheck919
                                        #   in Loop: Header=BB3_149 Depth=1
	leaq	(%r15,%rdx,4), %rcx
	cmpq	%rcx, %rax
	jae	.LBB3_168
	.p2align	4, 0x90
.LBB3_164:                              #   in Loop: Header=BB3_149 Depth=1
	movl	%r8d, %ecx
	movq	%r15, %rdx
.LBB3_176:                              # %.lr.ph.i591.preheader
                                        #   in Loop: Header=BB3_149 Depth=1
	leal	-1(%rcx), %esi
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB3_179
# BB#177:                               # %.lr.ph.i591.prol.preheader
                                        #   in Loop: Header=BB3_149 Depth=1
	negl	%edi
	.p2align	4, 0x90
.LBB3_178:                              # %.lr.ph.i591.prol
                                        #   Parent Loop BB3_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	decl	%ecx
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rax
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	leaq	4(%rdx), %rdx
	incl	%edi
	jne	.LBB3_178
.LBB3_179:                              # %.lr.ph.i591.prol.loopexit
                                        #   in Loop: Header=BB3_149 Depth=1
	cmpl	$3, %esi
	jb	.LBB3_181
	.p2align	4, 0x90
.LBB3_180:                              # %.lr.ph.i591
                                        #   Parent Loop BB3_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addss	(%rdx), %xmm0
	movss	%xmm0, (%rdx)
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	4(%rdx), %xmm0
	movss	%xmm0, 4(%rdx)
	movss	8(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	addss	8(%rdx), %xmm0
	movss	%xmm0, 8(%rdx)
	addl	$-4, %ecx
	movss	12(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	leaq	16(%rax), %rax
	addss	12(%rdx), %xmm0
	movss	%xmm0, 12(%rdx)
	leaq	16(%rdx), %rdx
	jne	.LBB3_180
.LBB3_181:                              # %imp_match_out_veadQ.exit593
                                        #   in Loop: Header=BB3_149 Depth=1
	movq	%r8, %r11
	movq	176(%rsp), %rax         # 8-byte Reload
	movl	(%rax,%r14,4), %eax
	movl	%eax, (%r15)
	movss	(%r10), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, Q__align.mi(%rip)
	leaq	1(%r14), %r8
	xorl	%edx, %edx
	cmpb	$0, 128(%rsp)           # 1-byte Folded Reload
	jne	.LBB3_193
# BB#182:                               # %.lr.ph639.preheader
                                        #   in Loop: Header=BB3_149 Depth=1
	leaq	-1(%r14), %rcx
	movq	320(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r14,4), %xmm8    # xmm8 = mem[0],zero,zero,zero
	movq	312(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r14,4), %xmm9    # xmm9 = mem[0],zero,zero,zero
	movq	304(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r14,4), %xmm10   # xmm10 = mem[0],zero,zero,zero
	movq	296(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r14,4), %xmm11   # xmm11 = mem[0],zero,zero,zero
	movq	288(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r14,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	4(%rax,%r14,4), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movq	280(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r14,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movq	272(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	328(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r14,8), %rsi
	xorl	%edi, %edi
	movl	$-1, %eax
	xorl	%edx, %edx
	jmp	.LBB3_183
	.p2align	4, 0x90
.LBB3_311:                              # %..lr.ph639_crit_edge
                                        #   in Loop: Header=BB3_183 Depth=2
	movss	4(%r10,%rdi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	incq	%rdi
	decl	%eax
.LBB3_183:                              # %.lr.ph639
                                        #   Parent Loop BB3_149 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	152(%rsp), %rbx         # 8-byte Reload
	movss	4(%rbx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	movq	192(%rsp), %rbx         # 8-byte Reload
	movss	4(%rbx,%rdi,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movq	120(%rsp), %rbx         # 8-byte Reload
	movss	4(%rbx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	movq	200(%rsp), %rbx         # 8-byte Reload
	movss	4(%rbx,%rdi,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm4
	addss	%xmm3, %xmm4
	movl	$0, 4(%rsi,%rdi,4)
	movq	104(%rsp), %rbx         # 8-byte Reload
	movss	4(%rbx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm2, %xmm3
	ucomiss	%xmm4, %xmm3
	jbe	.LBB3_185
# BB#184:                               #   in Loop: Header=BB3_183 Depth=2
	leal	(%rdx,%rax), %ebp
	movl	%ebp, 4(%rsi,%rdi,4)
	movq	40(%rsp), %rbp          # 8-byte Reload
	movaps	%xmm3, %xmm4
.LBB3_185:                              #   in Loop: Header=BB3_183 Depth=2
	movq	80(%rsp), %rbx          # 8-byte Reload
	movss	4(%rbx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	addss	%xmm3, %xmm0
	ucomiss	%xmm2, %xmm0
	jb	.LBB3_187
# BB#186:                               #   in Loop: Header=BB3_183 Depth=2
	movss	%xmm0, Q__align.mi(%rip)
	movaps	%xmm0, %xmm2
	movl	%edi, %edx
.LBB3_187:                              #   in Loop: Header=BB3_183 Depth=2
	movss	8(%r13,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movss	4(%rbp,%rdi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	ucomiss	%xmm4, %xmm3
	jbe	.LBB3_189
# BB#188:                               #   in Loop: Header=BB3_183 Depth=2
	movl	%r14d, %ebp
	movq	32(%rsp), %r9           # 8-byte Reload
	subl	4(%r9,%rdi,4), %ebp
	movl	%ebp, 4(%rsi,%rdi,4)
	movq	40(%rsp), %rbp          # 8-byte Reload
	movaps	%xmm3, %xmm4
.LBB3_189:                              #   in Loop: Header=BB3_183 Depth=2
	movss	4(%r13,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	addss	(%r10,%rdi,4), %xmm3
	ucomiss	%xmm0, %xmm3
	jb	.LBB3_191
# BB#190:                               #   in Loop: Header=BB3_183 Depth=2
	movss	%xmm3, 4(%rbp,%rdi,4)
	movq	32(%rsp), %rbx          # 8-byte Reload
	movl	%ecx, 4(%rbx,%rdi,4)
.LBB3_191:                              #   in Loop: Header=BB3_183 Depth=2
	movss	4(%r15,%rdi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm0
	movss	%xmm0, 4(%r15,%rdi,4)
	cmpl	%edi, 184(%rsp)         # 4-byte Folded Reload
	jne	.LBB3_311
# BB#192:                               #   in Loop: Header=BB3_149 Depth=1
	movq	144(%rsp), %rbx         # 8-byte Reload
.LBB3_193:                              # %._crit_edge640
                                        #   in Loop: Header=BB3_149 Depth=1
	movq	344(%rsp), %rax         # 8-byte Reload
	movl	(%r15,%rax,4), %eax
	movl	%eax, (%rbx,%r14,4)
	cmpq	336(%rsp), %r8          # 8-byte Folded Reload
	movq	%r8, %r14
	movq	%r15, %rbp
	movq	%r11, %r8
	movb	168(%rsp), %r9b         # 1-byte Reload
	jne	.LBB3_149
# BB#194:                               # %._crit_edge647
	movss	%xmm4, 116(%rsp)        # 4-byte Spill
	movl	%edx, Q__align.mpi(%rip)
	movl	outgap(%rip), %edx
	movq	72(%rsp), %r12          # 8-byte Reload
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movb	27(%rsp), %r14b         # 1-byte Reload
	testl	%edx, %edx
	jne	.LBB3_210
	jmp	.LBB3_196
.LBB3_147:
	xorps	%xmm0, %xmm0
	movss	%xmm0, 116(%rsp)        # 4-byte Spill
	movq	%rbp, %r15
	testl	%edx, %edx
	jne	.LBB3_210
.LBB3_196:                              # %.preheader599
	movq	%r8, %rdi
	movl	%esi, %r8d
	cmpb	$0, 128(%rsp)           # 1-byte Folded Reload
	jne	.LBB3_203
# BB#197:                               # %.lr.ph613
	movslq	offset(%rip), %rax
	movq	%rdi, %rsi
	movq	%rsi, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB3_198
# BB#199:
	leal	-1(%rsi), %edx
	imull	%eax, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI3_4(%rip), %xmm0
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r15)
	movl	$2, %ebp
	cmpq	$2, %rcx
	jne	.LBB3_201
	jmp	.LBB3_203
.LBB3_198:
	movl	$1, %ebp
	cmpq	$2, %rcx
	je	.LBB3_203
.LBB3_201:                              # %.lr.ph613.new
	subq	%rbp, %rcx
	movl	%edi, %edx
	subl	%ebp, %edx
	imull	%eax, %edx
	leaq	4(%r15,%rbp,4), %rsi
	leal	-1(%rdi), %edi
	subl	%ebp, %edi
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI3_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB3_202:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	subl	%eax, %ebp
	addq	$8, %rsi
	addq	$-2, %rcx
	jne	.LBB3_202
.LBB3_203:                              # %.preheader598
	testl	%r13d, %r13d
	movl	%r8d, %esi
	movq	144(%rsp), %rdi         # 8-byte Reload
	jle	.LBB3_210
# BB#204:                               # %.lr.ph611
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r13d, %xmm1
	incq	%r13
	movl	%r13d, %eax
	testb	$1, %r13b
	jne	.LBB3_205
# BB#206:
	movsd	.LCPI3_4(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movss	4(%rdi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%rdi)
	movl	$2, %ecx
	cmpq	$2, %rax
	jne	.LBB3_208
	jmp	.LBB3_210
.LBB3_205:
	movl	$1, %ecx
	cmpq	$2, %rax
	je	.LBB3_210
.LBB3_208:
	movsd	.LCPI3_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB3_209:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%rdi,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rdi,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%rdi,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%rdi,%rcx,4)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB3_209
.LBB3_210:                              # %.loopexit
	movl	512(%rsp), %ebx
	movq	Q__align.mseq1(%rip), %r8
	movq	Q__align.mseq2(%rip), %r13
	movq	Q__align.ijp(%rip), %rcx
	testb	%r14b, %r14b
	je	.LBB3_300
# BB#211:
	movq	%r8, 168(%rsp)          # 8-byte Spill
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdi
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	callq	strlen
	movq	%rax, %rbx
	movq	(%r12), %rdi
	callq	strlen
	movq	%rax, %r12
	movq	%rbx, 80(%rsp)          # 8-byte Spill
	leal	1(%r12,%rbx), %ebx
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	%rax, %r14
	movl	%ebx, %edi
	callq	AllocateCharVec
	movq	120(%rsp), %r9          # 8-byte Reload
	cmpl	$1, outgap(%rip)
	je	.LBB3_212
# BB#223:
	movq	144(%rsp), %rbx         # 8-byte Reload
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	80(%rsp), %rsi          # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB3_229
# BB#224:                               # %.lr.ph55.i
	movslq	%esi, %rbp
	movslq	%r12d, %rcx
	movl	%ebp, %edx
	addq	$4, %rbx
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB3_227
	jmp	.LBB3_226
	.p2align	4, 0x90
.LBB3_312:                              # %._crit_edge97.i
                                        #   in Loop: Header=BB3_227 Depth=1
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rbx
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB3_227
.LBB3_226:
	movq	(%r9,%rbp,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movaps	%xmm0, %xmm1
.LBB3_227:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB3_312
# BB#228:
	movaps	%xmm1, %xmm0
.LBB3_229:                              # %.preheader9.i
	testl	%r12d, %r12d
	jle	.LBB3_212
# BB#230:                               # %.lr.ph51.i
	movslq	80(%rsp), %r8           # 4-byte Folded Reload
	movslq	%r12d, %rcx
	movl	%ecx, %edx
	testb	$1, %r12b
	jne	.LBB3_232
# BB#231:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB3_236
	jmp	.LBB3_212
.LBB3_300:
	movl	%esi, %r14d
	movl	%esi, 16(%rsp)
	movl	52(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%rsp)
	movq	%rcx, (%rsp)
	movq	%r15, %rdi
	movq	144(%rsp), %rsi         # 8-byte Reload
	movq	88(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%r12, %rcx
	movq	%r13, %r9
	callq	Atracking
	jmp	.LBB3_301
.LBB3_232:
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB3_234
# BB#233:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB3_236
	jmp	.LBB3_212
.LBB3_234:
	movl	%r12d, %esi
	negl	%esi
	movq	(%r9,%r8,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB3_212
.LBB3_236:                              # %.lr.ph51.i.new
	movl	%r12d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB3_237:                              # =>This Inner Loop Header: Depth=1
	movss	(%r15,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB3_239
# BB#238:                               #   in Loop: Header=BB3_237 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB3_239:                              #   in Loop: Header=BB3_237 Depth=1
	movss	4(%r15,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB3_241
# BB#240:                               #   in Loop: Header=BB3_237 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movaps	%xmm1, %xmm0
.LBB3_241:                              #   in Loop: Header=BB3_237 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB3_237
.LBB3_212:                              # %.preheader8.i
	cmpl	$0, 80(%rsp)            # 4-byte Folded Reload
	js	.LBB3_218
# BB#213:                               # %.lr.ph48.preheader.i
	movq	80(%rsp), %rsi          # 8-byte Reload
	incq	%rsi
	movl	%esi, %ebp
	leaq	-1(%rbp), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB3_215
	.p2align	4, 0x90
.LBB3_214:                              # %.lr.ph48.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB3_214
.LBB3_215:                              # %.lr.ph48.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB3_218
# BB#216:                               # %.lr.ph48.preheader.i.new
	negq	%rbp
	leaq	4(%rdx,%rbp), %r8
	leaq	56(%r9,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_217:                              # %.lr.ph48.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%r8,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB3_217
.LBB3_218:                              # %.preheader7.i
	movq	80(%rsp), %rsi          # 8-byte Reload
	leal	(%r12,%rsi), %ecx
	movl	%ecx, 184(%rsp)         # 4-byte Spill
	testl	%r12d, %r12d
	js	.LBB3_246
# BB#219:                               # %.lr.ph45.i
	movq	(%r9), %rcx
	movq	%r12, %rsi
	incq	%rsi
	movl	%esi, %ebp
	cmpq	$7, %rbp
	jbe	.LBB3_220
# BB#242:                               # %min.iters.checked948
	andl	$7, %esi
	movq	%rbp, %rbx
	subq	%rsi, %rbx
	je	.LBB3_220
# BB#243:                               # %vector.body944.preheader
	leaq	16(%rcx), %rdi
	movdqa	.LCPI3_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI3_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI3_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbx, %rdx
	.p2align	4, 0x90
.LBB3_244:                              # %vector.body944
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB3_244
# BB#245:                               # %middle.block945
	testq	%rsi, %rsi
	movq	80(%rsp), %rsi          # 8-byte Reload
	jne	.LBB3_221
	jmp	.LBB3_246
.LBB3_220:
	xorl	%ebx, %ebx
	movq	80(%rsp), %rsi          # 8-byte Reload
.LBB3_221:                              # %scalar.ph946.preheader
	movl	%ebx, %edx
	notl	%edx
	leaq	(%rcx,%rbx,4), %rcx
	subq	%rbx, %rbp
	.p2align	4, 0x90
.LBB3_222:                              # %scalar.ph946
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rcx)
	decl	%edx
	addq	$4, %rcx
	decq	%rbp
	jne	.LBB3_222
.LBB3_246:                              # %._crit_edge46.i
	movq	%r13, 176(%rsp)         # 8-byte Spill
	movslq	%esi, %rdx
	leaq	(%r14,%rdx), %r13
	movslq	%r12d, %rcx
	movb	$0, (%rcx,%r13)
	addq	%rcx, %r13
	addq	%rax, %rdx
	leaq	(%rdx,%rcx), %r15
	movb	$0, (%rcx,%rdx)
	movq	528(%rsp), %rcx
	movl	$0, (%rcx)
	cmpl	$0, 184(%rsp)           # 4-byte Folded Reload
	movq	%r14, 136(%rsp)         # 8-byte Spill
	movq	%rax, 128(%rsp)         # 8-byte Spill
	js	.LBB3_293
# BB#247:                               # %.lr.ph37.preheader.i
	xorl	%r10d, %r10d
	movq	impmtx(%rip), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	movdqa	.LCPI3_10(%rip), %xmm0  # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI3_9(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	movl	%esi, %r14d
	movl	%r12d, %edx
	movq	%r12, 200(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB3_248:                              # %.lr.ph37.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_262 Depth 2
                                        #     Child Loop BB3_266 Depth 2
                                        #     Child Loop BB3_269 Depth 2
                                        #     Child Loop BB3_278 Depth 2
                                        #     Child Loop BB3_282 Depth 2
                                        #     Child Loop BB3_285 Depth 2
	movslq	%r14d, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	(%r9,%rax,8), %rax
	movl	%edx, %r11d
	movslq	%edx, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movl	(%rax,%rcx,4), %edx
	testl	%edx, %edx
	js	.LBB3_249
# BB#250:                               #   in Loop: Header=BB3_248 Depth=1
	je	.LBB3_252
# BB#251:                               #   in Loop: Header=BB3_248 Depth=1
	movl	%r14d, %esi
	subl	%edx, %esi
	jmp	.LBB3_253
	.p2align	4, 0x90
.LBB3_249:                              #   in Loop: Header=BB3_248 Depth=1
	leal	-1(%r14), %esi
	jmp	.LBB3_254
	.p2align	4, 0x90
.LBB3_252:                              #   in Loop: Header=BB3_248 Depth=1
	leal	-1(%r14), %esi
.LBB3_253:                              #   in Loop: Header=BB3_248 Depth=1
	movl	$-1, %edx
.LBB3_254:                              #   in Loop: Header=BB3_248 Depth=1
	movl	%r14d, %eax
	subl	%esi, %eax
	decl	%eax
	movl	%edx, 40(%rsp)          # 4-byte Spill
	je	.LBB3_271
# BB#255:                               # %.lr.ph18.preheader.i
                                        #   in Loop: Header=BB3_248 Depth=1
	leal	-2(%r14), %ecx
	subl	%esi, %ecx
	movq	%rcx, %r8
	negq	%r8
	leaq	1(%rcx), %rbp
	cmpq	$16, %rbp
	jae	.LBB3_257
# BB#256:                               #   in Loop: Header=BB3_248 Depth=1
	movq	%r15, %rcx
	movq	%r13, %rdi
	jmp	.LBB3_264
	.p2align	4, 0x90
.LBB3_257:                              # %min.iters.checked1004
                                        #   in Loop: Header=BB3_248 Depth=1
	movq	%r12, %rdx
	movl	%esi, %r12d
	movl	%ebp, %r9d
	andl	$15, %r9d
	subq	%r9, %rbp
	je	.LBB3_258
# BB#259:                               # %vector.memcheck1017
                                        #   in Loop: Header=BB3_248 Depth=1
	leaq	-1(%r13,%r8), %rsi
	cmpq	%r15, %rsi
	jae	.LBB3_261
# BB#260:                               # %vector.memcheck1017
                                        #   in Loop: Header=BB3_248 Depth=1
	leaq	-1(%r15,%r8), %rsi
	cmpq	%r13, %rsi
	jae	.LBB3_261
.LBB3_258:                              #   in Loop: Header=BB3_248 Depth=1
	movq	%r15, %rcx
	movq	%r13, %rdi
	movl	%r12d, %esi
	movq	%rdx, %r12
	movq	120(%rsp), %r9          # 8-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	jmp	.LBB3_264
.LBB3_261:                              # %vector.body1000.preheader
                                        #   in Loop: Header=BB3_248 Depth=1
	subl	%ebp, %eax
	leaq	-1(%r9), %rdi
	subq	%rcx, %rdi
	leaq	(%r15,%rdi), %rcx
	addq	%r13, %rdi
	leaq	-8(%r13), %rbx
	leaq	-8(%r15), %rsi
	.p2align	4, 0x90
.LBB3_262:                              # %vector.body1000
                                        #   Parent Loop BB3_248 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%rbx)
	movq	%xmm1, -8(%rbx)
	movq	%xmm0, (%rsi)
	movq	%xmm0, -8(%rsi)
	addq	$-16, %rbx
	addq	$-16, %rsi
	addq	$-16, %rbp
	jne	.LBB3_262
# BB#263:                               # %middle.block1001
                                        #   in Loop: Header=BB3_248 Depth=1
	testq	%r9, %r9
	movl	%r12d, %esi
	movq	%rdx, %r12
	movq	120(%rsp), %r9          # 8-byte Reload
	movl	40(%rsp), %edx          # 4-byte Reload
	je	.LBB3_270
	.p2align	4, 0x90
.LBB3_264:                              # %.lr.ph18.i.preheader
                                        #   in Loop: Header=BB3_248 Depth=1
	leal	-1(%rax), %ebp
	movl	%eax, %ebx
	andl	$7, %ebx
	je	.LBB3_267
# BB#265:                               # %.lr.ph18.i.prol.preheader
                                        #   in Loop: Header=BB3_248 Depth=1
	negl	%ebx
	.p2align	4, 0x90
.LBB3_266:                              # %.lr.ph18.i.prol
                                        #   Parent Loop BB3_248 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rdi)
	decq	%rdi
	movb	$45, -1(%rcx)
	decq	%rcx
	decl	%eax
	incl	%ebx
	jne	.LBB3_266
.LBB3_267:                              # %.lr.ph18.i.prol.loopexit
                                        #   in Loop: Header=BB3_248 Depth=1
	cmpl	$7, %ebp
	jb	.LBB3_270
# BB#268:                               # %.lr.ph18.i.preheader.new
                                        #   in Loop: Header=BB3_248 Depth=1
	decq	%rcx
	decq	%rdi
	.p2align	4, 0x90
.LBB3_269:                              # %.lr.ph18.i
                                        #   Parent Loop BB3_248 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rdi)
	movb	$45, (%rcx)
	movb	$111, -1(%rdi)
	movb	$45, -1(%rcx)
	movb	$111, -2(%rdi)
	movb	$45, -2(%rcx)
	movb	$111, -3(%rdi)
	movb	$45, -3(%rcx)
	movb	$111, -4(%rdi)
	movb	$45, -4(%rcx)
	movb	$111, -5(%rdi)
	movb	$45, -5(%rcx)
	movb	$111, -6(%rdi)
	movb	$45, -6(%rcx)
	movb	$111, -7(%rdi)
	movb	$45, -7(%rcx)
	addq	$-8, %rcx
	addq	$-8, %rdi
	addl	$-8, %eax
	jne	.LBB3_269
.LBB3_270:                              # %._crit_edge19.loopexit.i
                                        #   in Loop: Header=BB3_248 Depth=1
	leaq	-1(%r13,%r8), %r13
	leaq	-1(%r15,%r8), %r15
	leal	-1(%r10,%r14), %r10d
	subl	%esi, %r10d
.LBB3_271:                              # %._crit_edge19.i
                                        #   in Loop: Header=BB3_248 Depth=1
	cmpl	$-1, %edx
	je	.LBB3_272
# BB#273:                               # %.lr.ph26.preheader.i
                                        #   in Loop: Header=BB3_248 Depth=1
	movl	%esi, 152(%rsp)         # 4-byte Spill
	movl	%edx, %r12d
	notl	%r12d
	movl	$-2, %r8d
	subl	%edx, %r8d
	movq	%r8, %r9
	negq	%r9
	leaq	1(%r8), %rcx
	cmpq	$16, %rcx
	movl	%r12d, %edi
	movq	%r15, %rbx
	movq	%r13, %rbp
	movl	%r11d, %edx
	jb	.LBB3_280
# BB#274:                               # %min.iters.checked966
                                        #   in Loop: Header=BB3_248 Depth=1
	movl	%ecx, %r11d
	andl	$15, %r11d
	subq	%r11, %rcx
	movl	%r12d, %edi
	movq	%r15, %rbx
	movq	%r13, %rbp
	je	.LBB3_280
# BB#275:                               # %vector.memcheck979
                                        #   in Loop: Header=BB3_248 Depth=1
	leaq	-1(%r13,%r9), %rax
	cmpq	%r15, %rax
	jae	.LBB3_277
# BB#276:                               # %vector.memcheck979
                                        #   in Loop: Header=BB3_248 Depth=1
	leaq	-1(%r15,%r9), %rax
	cmpq	%r13, %rax
	movl	%r12d, %edi
	movq	%r15, %rbx
	movq	%r13, %rbp
	jb	.LBB3_280
.LBB3_277:                              # %vector.body962.preheader
                                        #   in Loop: Header=BB3_248 Depth=1
	movl	%r12d, %edi
	subl	%ecx, %edi
	leaq	-1(%r11), %rbp
	subq	%r8, %rbp
	leaq	(%r15,%rbp), %rbx
	addq	%r13, %rbp
	leaq	-8(%r13), %rax
	leaq	-8(%r15), %rsi
	.p2align	4, 0x90
.LBB3_278:                              # %vector.body962
                                        #   Parent Loop BB3_248 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rax)
	movq	%xmm0, -8(%rax)
	movq	%xmm1, (%rsi)
	movq	%xmm1, -8(%rsi)
	addq	$-16, %rax
	addq	$-16, %rsi
	addq	$-16, %rcx
	jne	.LBB3_278
# BB#279:                               # %middle.block963
                                        #   in Loop: Header=BB3_248 Depth=1
	testq	%r11, %r11
	je	.LBB3_286
	.p2align	4, 0x90
.LBB3_280:                              # %.lr.ph26.i.preheader
                                        #   in Loop: Header=BB3_248 Depth=1
	leal	-1(%rdi), %r8d
	movl	%edi, %ecx
	andl	$7, %ecx
	je	.LBB3_283
# BB#281:                               # %.lr.ph26.i.prol.preheader
                                        #   in Loop: Header=BB3_248 Depth=1
	negl	%ecx
	.p2align	4, 0x90
.LBB3_282:                              # %.lr.ph26.i.prol
                                        #   Parent Loop BB3_248 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rbp)
	decq	%rbp
	movb	$111, -1(%rbx)
	decq	%rbx
	decl	%edi
	incl	%ecx
	jne	.LBB3_282
.LBB3_283:                              # %.lr.ph26.i.prol.loopexit
                                        #   in Loop: Header=BB3_248 Depth=1
	cmpl	$7, %r8d
	jb	.LBB3_286
# BB#284:                               # %.lr.ph26.i.preheader.new
                                        #   in Loop: Header=BB3_248 Depth=1
	decq	%rbx
	decq	%rbp
	.p2align	4, 0x90
.LBB3_285:                              # %.lr.ph26.i
                                        #   Parent Loop BB3_248 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rbp)
	movb	$111, (%rbx)
	movb	$45, -1(%rbp)
	movb	$111, -1(%rbx)
	movb	$45, -2(%rbp)
	movb	$111, -2(%rbx)
	movb	$45, -3(%rbp)
	movb	$111, -3(%rbx)
	movb	$45, -4(%rbp)
	movb	$111, -4(%rbx)
	movb	$45, -5(%rbp)
	movb	$111, -5(%rbx)
	movb	$45, -6(%rbp)
	movb	$111, -6(%rbx)
	movb	$45, -7(%rbp)
	movb	$111, -7(%rbx)
	addq	$-8, %rbx
	addq	$-8, %rbp
	addl	$-8, %edi
	jne	.LBB3_285
.LBB3_286:                              # %._crit_edge27.loopexit.i
                                        #   in Loop: Header=BB3_248 Depth=1
	leaq	-1(%r13,%r9), %r13
	leaq	-1(%r15,%r9), %r15
	addl	%r12d, %r10d
	movq	120(%rsp), %r9          # 8-byte Reload
	movq	200(%rsp), %r12         # 8-byte Reload
	movl	152(%rsp), %esi         # 4-byte Reload
	cmpl	80(%rsp), %r14d         # 4-byte Folded Reload
	jne	.LBB3_288
	jmp	.LBB3_290
	.p2align	4, 0x90
.LBB3_272:                              #   in Loop: Header=BB3_248 Depth=1
	movl	%r11d, %edx
	cmpl	80(%rsp), %r14d         # 4-byte Folded Reload
	je	.LBB3_290
.LBB3_288:                              # %._crit_edge27.i
                                        #   in Loop: Header=BB3_248 Depth=1
	cmpl	%r12d, %edx
	je	.LBB3_290
# BB#289:                               #   in Loop: Header=BB3_248 Depth=1
	movq	192(%rsp), %rax         # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	(%rax,%rcx,8), %rax
	movq	104(%rsp), %rcx         # 8-byte Reload
	movss	(%rax,%rcx,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movq	528(%rsp), %rax
	addss	(%rax), %xmm2
	movss	%xmm2, (%rax)
.LBB3_290:                              #   in Loop: Header=BB3_248 Depth=1
	testl	%r14d, %r14d
	movl	40(%rsp), %eax          # 4-byte Reload
	jle	.LBB3_293
# BB#291:                               #   in Loop: Header=BB3_248 Depth=1
	testl	%edx, %edx
	jle	.LBB3_293
# BB#292:                               #   in Loop: Header=BB3_248 Depth=1
	addl	%eax, %edx
	movb	$111, -1(%r13)
	decq	%r13
	movb	$111, -1(%r15)
	decq	%r15
	addl	$2, %r10d
	cmpl	184(%rsp), %r10d        # 4-byte Folded Reload
	movl	%esi, %r14d
	jle	.LBB3_248
.LBB3_293:                              # %._crit_edge38.i
	cmpl	$0, 52(%rsp)            # 4-byte Folded Reload
	movq	168(%rsp), %rbx         # 8-byte Reload
	jle	.LBB3_296
# BB#294:                               # %.lr.ph13.preheader.i
	movl	52(%rsp), %r14d         # 4-byte Reload
	movq	88(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_295:                              # %.lr.ph13.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r13, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r14
	jne	.LBB3_295
.LBB3_296:                              # %.preheader.i
	movl	28(%rsp), %eax          # 4-byte Reload
	movl	%eax, %r14d
	testl	%eax, %eax
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	176(%rsp), %rbx         # 8-byte Reload
	jle	.LBB3_299
# BB#297:                               # %.lr.ph.preheader.i569
	movl	%r14d, %r13d
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB3_298:                              # %.lr.ph.i573
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rbp), %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r13
	jne	.LBB3_298
.LBB3_299:                              # %Atracking_localhom.exit
	movq	136(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	128(%rsp), %rdi         # 8-byte Reload
	callq	free
	movq	88(%rsp), %r15          # 8-byte Reload
	movl	512(%rsp), %ebx
.LBB3_301:
	movq	Q__align.mseq1(%rip), %rax
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, %rcx
	cmpl	%ebx, %ecx
	jg	.LBB3_303
# BB#302:
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB3_303
.LBB3_304:                              # %.preheader597
	movl	52(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB3_307
# BB#305:                               # %.lr.ph609.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_306:                              # %.lr.ph609
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	Q__align.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_306
.LBB3_307:                              # %.preheader
	testl	%r14d, %r14d
	jle	.LBB3_310
# BB#308:                               # %.lr.ph.preheader
	movl	%r14d, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB3_309:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12,%rbp,8), %rdi
	movq	Q__align.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB3_309
.LBB3_310:                              # %._crit_edge
	movss	116(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$456, %rsp              # imm = 0x1C8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_303:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
	movl	%ebx, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.1, %edi
	callq	ErrorExit
	jmp	.LBB3_304
.LBB3_99:
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movq	40(%rsp), %r8           # 8-byte Reload
	jmp	.LBB3_92
.LBB3_116:
	movq	72(%rsp), %r12          # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	movb	27(%rsp), %r14b         # 1-byte Reload
	movq	40(%rsp), %r15          # 8-byte Reload
	jmp	.LBB3_109
.Lfunc_end3:
	.size	Q__align, .Lfunc_end3-Q__align
	.cfi_endproc

	.p2align	4, 0x90
	.type	match_calc,@function
match_calc:                             # @match_calc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 40
.Lcfi38:
	.cfi_offset %rbx, -40
.Lcfi39:
	.cfi_offset %r14, -32
.Lcfi40:
	.cfi_offset %r15, -24
.Lcfi41:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r11
	cmpl	$0, 48(%rsp)
	je	.LBB4_10
# BB#1:
	testl	%r8d, %r8d
	jle	.LBB4_10
# BB#2:                                 # %.preheader77.preheader
	movl	%r8d, %r10d
	xorl	%r14d, %r14d
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader77
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	xorl	%eax, %eax
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB4_5
	jnp	.LBB4_6
.LBB4_5:                                #   in Loop: Header=BB4_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	movl	%eax, (%rbx,%r15,4)
	incl	%r15d
.LBB4_6:                                #   in Loop: Header=BB4_4 Depth=2
	movq	8(%rdx,%rax,8), %rbx
	movss	(%rbx,%r14,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jne	.LBB4_7
	jnp	.LBB4_8
.LBB4_7:                                #   in Loop: Header=BB4_4 Depth=2
	movq	(%r9,%r14,8), %rbx
	movslq	%r15d, %r15
	movss	%xmm1, (%rbx,%r15,4)
	movq	(%r11,%r14,8), %rbx
	leal	1(%rax), %ebp
	movl	%ebp, (%rbx,%r15,4)
	incl	%r15d
.LBB4_8:                                #   in Loop: Header=BB4_4 Depth=2
	addq	$2, %rax
	cmpq	$26, %rax
	jne	.LBB4_4
# BB#9:                                 #   in Loop: Header=BB4_3 Depth=1
	movq	(%r11,%r14,8), %rax
	movslq	%r15d, %rbx
	movl	$-1, (%rax,%rbx,4)
	incq	%r14
	cmpq	%r10, %r14
	jne	.LBB4_3
.LBB4_10:                               # %.preheader76
	movl	$n_dis_consweight_multi, %r10d
	movslq	%ecx, %rcx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB4_11:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_12 Depth 2
	movl	$0, -104(%rsp,%r14,4)
	xorps	%xmm0, %xmm0
	movl	$1, %ebx
	movq	%r10, %rax
	.p2align	4, 0x90
.LBB4_12:                               #   Parent Loop BB4_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-8(%rsi,%rbx,8), %rdx
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdx,%rcx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rsi,%rbx,8), %rdx
	mulss	(%rdx,%rcx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rax
	addq	$2, %rbx
	cmpq	$27, %rbx
	jne	.LBB4_12
# BB#13:                                #   in Loop: Header=BB4_11 Depth=1
	movss	%xmm0, -104(%rsp,%r14,4)
	incq	%r14
	addq	$4, %r10
	cmpq	$26, %r14
	jne	.LBB4_11
	jmp	.LBB4_14
	.p2align	4, 0x90
.LBB4_18:                               # %._crit_edge
                                        #   in Loop: Header=BB4_14 Depth=1
	addq	$8, %r11
	addq	$8, %r9
	addq	$4, %rdi
.LBB4_14:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_17 Depth 2
	testl	%r8d, %r8d
	je	.LBB4_19
# BB#15:                                # %.lr.ph84
                                        #   in Loop: Header=BB4_14 Depth=1
	decl	%r8d
	movl	$0, (%rdi)
	movq	(%r11), %rax
	movl	(%rax), %edx
	testl	%edx, %edx
	js	.LBB4_18
# BB#16:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_14 Depth=1
	movq	(%r9), %rcx
	addq	$4, %rax
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph
                                        #   Parent Loop BB4_14 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%edx, %rdx
	movss	-104(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm1
	addq	$4, %rcx
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rdi)
	movl	(%rax), %edx
	addq	$4, %rax
	testl	%edx, %edx
	jns	.LBB4_17
	jmp	.LBB4_18
.LBB4_19:                               # %._crit_edge85
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	match_calc, .Lfunc_end4-match_calc
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI5_1:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI5_2:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
.LCPI5_3:
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
	.byte	111                     # 0x6f
	.byte	0                       # 0x0
.LCPI5_4:
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.byte	45                      # 0x2d
	.byte	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	Atracking,@function
Atracking:                              # @Atracking
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 128
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%r9, 64(%rsp)           # 8-byte Spill
	movq	%r8, %r14
	movq	%rcx, %rbx
	movq	%rdx, %r13
	movq	%rsi, %r15
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movl	144(%rsp), %eax
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	136(%rsp), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movq	128(%rsp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	(%r13), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%eax, %ebx
	movq	%rbp, (%rsp)            # 8-byte Spill
	leal	1(%rbx,%rbp), %r12d
	movl	%r12d, %edi
	callq	AllocateCharVec
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %edi
	callq	AllocateCharVec
	movq	16(%rsp), %r11          # 8-byte Reload
	movq	%rbx, %r10
	movq	48(%rsp), %r9           # 8-byte Reload
	cmpl	$1, outgap(%rip)
	je	.LBB5_1
# BB#12:
	movd	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	(%rsp), %rsi            # 8-byte Reload
	testl	%esi, %esi
	jle	.LBB5_18
# BB#13:                                # %.lr.ph54
	movslq	%esi, %rbp
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	movl	%ebp, %edx
	addq	$4, %r15
	decq	%rdx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movdqa	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB5_16
	jmp	.LBB5_15
	.p2align	4, 0x90
.LBB5_85:                               # %._crit_edge96
                                        #   in Loop: Header=BB5_16 Depth=1
	movd	(%r15), %xmm0           # xmm0 = mem[0],zero,zero,zero
	decl	%esi
	addq	$4, %r15
	decq	%rdx
	ucomiss	%xmm1, %xmm0
	jb	.LBB5_16
.LBB5_15:
	movq	(%r9,%rbp,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movdqa	%xmm0, %xmm1
.LBB5_16:                               # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB5_85
# BB#17:
	movdqa	%xmm1, %xmm0
.LBB5_18:                               # %.preheader8
	testl	%r10d, %r10d
	jle	.LBB5_1
# BB#19:                                # %.lr.ph50
	movslq	(%rsp), %r8             # 4-byte Folded Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movslq	%esi, %rcx
	movl	%ecx, %edx
	testb	$1, %sil
	jne	.LBB5_21
# BB#20:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB5_25
	jmp	.LBB5_1
.LBB5_21:
	movd	(%r11), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB5_23
# BB#22:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB5_25
	jmp	.LBB5_1
.LBB5_23:
	movl	%r10d, %esi
	negl	%esi
	movq	(%r9,%r8,8), %rdi
	movl	%esi, (%rdi,%rcx,4)
	movl	$1, %esi
	movdqa	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB5_1
.LBB5_25:                               # %.lr.ph50.new
	movl	%r10d, %edi
	negl	%edi
	.p2align	4, 0x90
.LBB5_26:                               # =>This Inner Loop Header: Depth=1
	movd	(%r11,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB5_28
# BB#27:                                #   in Loop: Header=BB5_26 Depth=1
	leal	(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movdqa	%xmm1, %xmm0
.LBB5_28:                               #   in Loop: Header=BB5_26 Depth=1
	movd	4(%r11,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB5_30
# BB#29:                                #   in Loop: Header=BB5_26 Depth=1
	leal	1(%rdi,%rsi), %ebp
	movq	(%r9,%r8,8), %rbx
	movl	%ebp, (%rbx,%rcx,4)
	movdqa	%xmm1, %xmm0
.LBB5_30:                               #   in Loop: Header=BB5_26 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB5_26
.LBB5_1:                                # %.preheader7
	cmpl	$0, (%rsp)              # 4-byte Folded Reload
	js	.LBB5_7
# BB#2:                                 # %.lr.ph47.preheader
	movq	(%rsp), %rsi            # 8-byte Reload
	incq	%rsi
	movl	%esi, %ebp
	leaq	-1(%rbp), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB5_4
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph47.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB5_3
.LBB5_4:                                # %.lr.ph47.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB5_7
# BB#5:                                 # %.lr.ph47.preheader.new
	negq	%rbp
	leaq	4(%rdx,%rbp), %r8
	leaq	56(%r9,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB5_6:                                # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %ebp
	leal	-3(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-48(%rcx,%rsi,8), %rdi
	leal	-2(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-40(%rcx,%rsi,8), %rdi
	leal	-1(%rdx,%rsi), %ebx
	movl	%ebx, (%rdi)
	movq	-32(%rcx,%rsi,8), %rdi
	movl	%ebp, (%rdi)
	movq	-24(%rcx,%rsi,8), %rdi
	leal	1(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-16(%rcx,%rsi,8), %rdi
	leal	2(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	-8(%rcx,%rsi,8), %rdi
	leal	3(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	movq	(%rcx,%rsi,8), %rdi
	leal	4(%rdx,%rsi), %ebp
	movl	%ebp, (%rdi)
	leaq	8(%r8,%rsi), %rdi
	addq	$8, %rsi
	cmpq	$4, %rdi
	jne	.LBB5_6
.LBB5_7:                                # %.preheader6
	movq	(%rsp), %r8             # 8-byte Reload
	leal	(%r10,%r8), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	testl	%r10d, %r10d
	js	.LBB5_35
# BB#8:                                 # %.lr.ph44
	movq	(%r9), %rcx
	movq	24(%rsp), %rbx          # 8-byte Reload
	incq	%rbx
	movl	%ebx, %edx
	cmpq	$7, %rdx
	jbe	.LBB5_9
# BB#31:                                # %min.iters.checked
	andl	$7, %ebx
	movq	%rdx, %rbp
	subq	%rbx, %rbp
	je	.LBB5_9
# BB#32:                                # %vector.body.preheader
	leaq	16(%rcx), %rsi
	movdqa	.LCPI5_0(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI5_1(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI5_2(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rbp, %rdi
	.p2align	4, 0x90
.LBB5_33:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rsi)
	movdqu	%xmm4, (%rsi)
	paddd	%xmm3, %xmm0
	addq	$32, %rsi
	addq	$-8, %rdi
	jne	.LBB5_33
# BB#34:                                # %middle.block
	testq	%rbx, %rbx
	jne	.LBB5_10
	jmp	.LBB5_35
.LBB5_9:
	xorl	%ebp, %ebp
.LBB5_10:                               # %scalar.ph.preheader
	movl	%ebp, %esi
	notl	%esi
	leaq	(%rcx,%rbp,4), %rcx
	subq	%rbp, %rdx
	.p2align	4, 0x90
.LBB5_11:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	%esi, (%rcx)
	decl	%esi
	addq	$4, %rcx
	decq	%rdx
	jne	.LBB5_11
.LBB5_35:                               # %._crit_edge45
	movslq	%r8d, %rdx
	movq	40(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx), %r12
	movslq	8(%rsp), %rcx           # 4-byte Folded Reload
	movb	$0, (%rcx,%r12)
	addq	%rcx, %r12
	movq	%rax, 24(%rsp)          # 8-byte Spill
	addq	%rax, %rdx
	leaq	(%rdx,%rcx), %r15
	movb	$0, (%rcx,%rdx)
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	js	.LBB5_78
# BB#36:                                # %.lr.ph36.preheader
	xorl	%ecx, %ecx
	movdqa	.LCPI5_4(%rip), %xmm0   # xmm0 = [45,0,45,0,45,0,45,0,45,0,45,0,45,0,45,0]
	packuswb	%xmm0, %xmm0
	movdqa	.LCPI5_3(%rip), %xmm1   # xmm1 = [111,0,111,0,111,0,111,0,111,0,111,0,111,0,111,0]
	packuswb	%xmm1, %xmm1
	jmp	.LBB5_37
.LBB5_49:                               # %vector.body129.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	%r8, (%rsp)             # 8-byte Spill
	subl	%ebp, %edi
	leaq	-1(%rsi), %rbx
	subq	%rax, %rbx
	leaq	(%r15,%rbx), %rax
	addq	%r12, %rbx
	leaq	-8(%r12), %r8
	addq	$-8, %r15
	.p2align	4, 0x90
.LBB5_50:                               # %vector.body129
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm1, (%r8)
	movq	%xmm1, -8(%r8)
	movq	%xmm0, (%r15)
	movq	%xmm0, -8(%r15)
	addq	$-16, %r8
	addq	$-16, %r15
	addq	$-16, %rbp
	jne	.LBB5_50
# BB#51:                                # %middle.block130
                                        #   in Loop: Header=BB5_37 Depth=1
	testq	%rsi, %rsi
	movq	(%rsp), %r8             # 8-byte Reload
	jne	.LBB5_52
	jmp	.LBB5_58
.LBB5_65:                               # %vector.body103.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	subl	%ebp, %edi
	leaq	-1(%rsi), %rax
	subq	%rbx, %rax
	leaq	(%r15,%rax), %rbx
	addq	%r12, %rax
	leaq	-8(%r12), %rdx
	addq	$-8, %r15
	.p2align	4, 0x90
.LBB5_66:                               # %vector.body103
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%xmm0, (%rdx)
	movq	%xmm0, -8(%rdx)
	movq	%xmm1, (%r15)
	movq	%xmm1, -8(%r15)
	addq	$-16, %rdx
	addq	$-16, %r15
	addq	$-16, %rbp
	jne	.LBB5_66
# BB#67:                                # %middle.block104
                                        #   in Loop: Header=BB5_37 Depth=1
	testq	%rsi, %rsi
	jne	.LBB5_68
	jmp	.LBB5_74
	.p2align	4, 0x90
.LBB5_37:                               # %.lr.ph36
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_50 Depth 2
                                        #     Child Loop BB5_54 Depth 2
                                        #     Child Loop BB5_57 Depth 2
                                        #     Child Loop BB5_66 Depth 2
                                        #     Child Loop BB5_70 Depth 2
                                        #     Child Loop BB5_73 Depth 2
	movslq	%r8d, %rax
	movq	(%r9,%rax,8), %rax
	movslq	%r10d, %rdx
	movl	(%rax,%rdx,4), %r11d
	testl	%r11d, %r11d
	js	.LBB5_38
# BB#39:                                #   in Loop: Header=BB5_37 Depth=1
	je	.LBB5_41
# BB#40:                                #   in Loop: Header=BB5_37 Depth=1
	movl	%r8d, %r9d
	subl	%r11d, %r9d
	jmp	.LBB5_42
	.p2align	4, 0x90
.LBB5_38:                               #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%r8), %r9d
	jmp	.LBB5_43
	.p2align	4, 0x90
.LBB5_41:                               #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%r8), %r9d
.LBB5_42:                               #   in Loop: Header=BB5_37 Depth=1
	movl	$-1, %r11d
.LBB5_43:                               #   in Loop: Header=BB5_37 Depth=1
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movl	%r8d, %edi
	subl	%r9d, %edi
	decl	%edi
	je	.LBB5_59
# BB#44:                                # %.lr.ph17.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	leal	-2(%r8), %eax
	subl	%r9d, %eax
	movq	%rax, %rdx
	negq	%rdx
	leaq	-1(%r15), %r10
	leal	-1(%r8,%rcx), %ecx
	leaq	1(%rax), %rbp
	cmpq	$16, %rbp
	jb	.LBB5_45
# BB#46:                                # %min.iters.checked133
                                        #   in Loop: Header=BB5_37 Depth=1
	movl	%ebp, %esi
	andl	$15, %esi
	subq	%rsi, %rbp
	je	.LBB5_45
# BB#47:                                # %vector.memcheck146
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%rdx), %rbx
	cmpq	%r15, %rbx
	jae	.LBB5_49
# BB#48:                                # %vector.memcheck146
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r15,%rdx), %rbx
	cmpq	%r12, %rbx
	jae	.LBB5_49
	.p2align	4, 0x90
.LBB5_45:                               #   in Loop: Header=BB5_37 Depth=1
	movq	%r15, %rax
	movq	%r12, %rbx
.LBB5_52:                               # %.lr.ph17.preheader168
                                        #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%rdi), %esi
	movl	%edi, %ebp
	andl	$7, %ebp
	je	.LBB5_55
# BB#53:                                # %.lr.ph17.prol.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB5_54:                               # %.lr.ph17.prol
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, -1(%rbx)
	decq	%rbx
	movb	$45, -1(%rax)
	decq	%rax
	decl	%edi
	incl	%ebp
	jne	.LBB5_54
.LBB5_55:                               # %.lr.ph17.prol.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	cmpl	$7, %esi
	jb	.LBB5_58
# BB#56:                                # %.lr.ph17.preheader168.new
                                        #   in Loop: Header=BB5_37 Depth=1
	decq	%rax
	decq	%rbx
	.p2align	4, 0x90
.LBB5_57:                               # %.lr.ph17
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$111, (%rbx)
	movb	$45, (%rax)
	movb	$111, -1(%rbx)
	movb	$45, -1(%rax)
	movb	$111, -2(%rbx)
	movb	$45, -2(%rax)
	movb	$111, -3(%rbx)
	movb	$45, -3(%rax)
	movb	$111, -4(%rbx)
	movb	$45, -4(%rax)
	movb	$111, -5(%rbx)
	movb	$45, -5(%rax)
	movb	$111, -6(%rbx)
	movb	$45, -6(%rax)
	movb	$111, -7(%rbx)
	movb	$45, -7(%rax)
	addq	$-8, %rax
	addq	$-8, %rbx
	addl	$-8, %edi
	jne	.LBB5_57
.LBB5_58:                               # %._crit_edge18.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%rdx), %r12
	addq	%rdx, %r10
	subl	%r9d, %ecx
	movq	%r10, %r15
.LBB5_59:                               # %._crit_edge18
                                        #   in Loop: Header=BB5_37 Depth=1
	cmpl	$-1, %r11d
	je	.LBB5_75
# BB#60:                                # %.lr.ph25.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	%r8, (%rsp)             # 8-byte Spill
	movl	%r11d, %edi
	notl	%edi
	movl	$-2, %ebx
	subl	%r11d, %ebx
	movq	%rbx, %r8
	negq	%r8
	leaq	-1(%r15), %r10
	decl	%ecx
	leaq	1(%rbx), %rbp
	cmpq	$16, %rbp
	jb	.LBB5_61
# BB#62:                                # %min.iters.checked107
                                        #   in Loop: Header=BB5_37 Depth=1
	movl	%ebp, %esi
	andl	$15, %esi
	subq	%rsi, %rbp
	je	.LBB5_61
# BB#63:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%r8), %rax
	cmpq	%r15, %rax
	jae	.LBB5_65
# BB#64:                                # %vector.memcheck
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r15,%r8), %rax
	cmpq	%r12, %rax
	jae	.LBB5_65
	.p2align	4, 0x90
.LBB5_61:                               #   in Loop: Header=BB5_37 Depth=1
	movq	%r15, %rbx
	movq	%r12, %rax
.LBB5_68:                               # %.lr.ph25.preheader167
                                        #   in Loop: Header=BB5_37 Depth=1
	leal	-1(%rdi), %esi
	movl	%edi, %ebp
	andl	$7, %ebp
	je	.LBB5_71
# BB#69:                                # %.lr.ph25.prol.preheader
                                        #   in Loop: Header=BB5_37 Depth=1
	negl	%ebp
	.p2align	4, 0x90
.LBB5_70:                               # %.lr.ph25.prol
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, -1(%rax)
	decq	%rax
	movb	$111, -1(%rbx)
	decq	%rbx
	decl	%edi
	incl	%ebp
	jne	.LBB5_70
.LBB5_71:                               # %.lr.ph25.prol.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	cmpl	$7, %esi
	jb	.LBB5_74
# BB#72:                                # %.lr.ph25.preheader167.new
                                        #   in Loop: Header=BB5_37 Depth=1
	decq	%rbx
	decq	%rax
	.p2align	4, 0x90
.LBB5_73:                               # %.lr.ph25
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	$45, (%rax)
	movb	$111, (%rbx)
	movb	$45, -1(%rax)
	movb	$111, -1(%rbx)
	movb	$45, -2(%rax)
	movb	$111, -2(%rbx)
	movb	$45, -3(%rax)
	movb	$111, -3(%rbx)
	movb	$45, -4(%rax)
	movb	$111, -4(%rbx)
	movb	$45, -5(%rax)
	movb	$111, -5(%rbx)
	movb	$45, -6(%rax)
	movb	$111, -6(%rbx)
	movb	$45, -7(%rax)
	movb	$111, -7(%rbx)
	addq	$-8, %rbx
	addq	$-8, %rax
	addl	$-8, %edi
	jne	.LBB5_73
.LBB5_74:                               # %._crit_edge26.loopexit
                                        #   in Loop: Header=BB5_37 Depth=1
	leaq	-1(%r12,%r8), %r12
	addq	%r8, %r10
	subl	%r11d, %ecx
	movq	%r10, %r15
	movq	(%rsp), %r8             # 8-byte Reload
.LBB5_75:                               # %._crit_edge26
                                        #   in Loop: Header=BB5_37 Depth=1
	movq	8(%rsp), %r10           # 8-byte Reload
	testl	%r10d, %r10d
	jle	.LBB5_78
# BB#76:                                # %._crit_edge26
                                        #   in Loop: Header=BB5_37 Depth=1
	testl	%r8d, %r8d
	jle	.LBB5_78
# BB#77:                                #   in Loop: Header=BB5_37 Depth=1
	addl	%r11d, %r10d
	movb	$111, -1(%r12)
	decq	%r12
	movb	$111, -1(%r15)
	decq	%r15
	addl	$2, %ecx
	cmpl	16(%rsp), %ecx          # 4-byte Folded Reload
	movl	%r9d, %r8d
	movq	48(%rsp), %r9           # 8-byte Reload
	jle	.LBB5_37
.LBB5_78:                               # %._crit_edge37
	movl	32(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	jle	.LBB5_81
# BB#79:                                # %.lr.ph12.preheader
	movl	%eax, %ebx
	.p2align	4, 0x90
.LBB5_80:                               # %.lr.ph12
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rdi
	movq	(%r13), %rsi
	movq	%r12, %rdx
	callq	gapireru
	addq	$8, %r14
	addq	$8, %r13
	decq	%rbx
	jne	.LBB5_80
.LBB5_81:                               # %.preheader
	movl	36(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	56(%rsp), %rbx          # 8-byte Reload
	jle	.LBB5_84
# BB#82:                                # %.lr.ph.preheader
	movl	%eax, %r14d
	.p2align	4, 0x90
.LBB5_83:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	movq	(%rbx), %rsi
	movq	%r15, %rdx
	callq	gapireru
	addq	$8, %rbp
	addq	$8, %rbx
	decq	%r14
	jne	.LBB5_83
.LBB5_84:                               # %._crit_edge
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	free
	movq	24(%rsp), %rdi          # 8-byte Reload
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	free                    # TAILCALL
.Lfunc_end5:
	.size	Atracking, .Lfunc_end5-Atracking
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_0:
	.quad	4608533498688228557     # double 1.3
.LCPI6_1:
	.quad	4607182418800017408     # double 1
.LCPI6_2:
	.quad	4602678819172646912     # double 0.5
.LCPI6_4:
	.quad	-4620693217682128896    # double -0.5
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI6_3:
	.long	1065353216              # float 1
.LCPI6_5:
	.long	1176256512              # float 1.0E+4
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_6:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	3                       # 0x3
.LCPI6_7:
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
	.long	4                       # 0x4
.LCPI6_8:
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.long	8                       # 0x8
	.text
	.globl	Q__align_gapmap
	.p2align	4, 0x90
	.type	Q__align_gapmap,@function
Q__align_gapmap:                        # @Q__align_gapmap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$424, %rsp              # imm = 0x1A8
.Lcfi61:
	.cfi_def_cfa_offset 480
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movl	%r9d, %r13d
	movl	%r8d, %r12d
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbx
	cvtsi2ssl	penalty(%rip), %xmm0
	movss	%xmm0, 120(%rsp)        # 4-byte Spill
	movl	Q__align_gapmap.orlgth1(%rip), %r15d
	testl	%r15d, %r15d
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	jne	.LBB6_2
# BB#1:
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	%rax, Q__align_gapmap.mseq1(%rip)
	movl	njob(%rip), %edi
	xorl	%esi, %esi
	callq	AllocateCharMtx
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	%rax, Q__align_gapmap.mseq2(%rip)
	movl	Q__align_gapmap.orlgth1(%rip), %r15d
.LBB6_2:
	movq	(%rdi), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	%rbx, 104(%rsp)         # 8-byte Spill
	movq	(%rbx), %rdi
	callq	strlen
	movq	%rax, %rcx
	movq	%rbp, 96(%rsp)          # 8-byte Spill
	cmpl	%r15d, %ebp
	movl	Q__align_gapmap.orlgth2(%rip), %r14d
	movl	%r13d, 44(%rsp)         # 4-byte Spill
	movl	%r12d, 60(%rsp)         # 4-byte Spill
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	jg	.LBB6_4
# BB#3:
	cmpl	%r14d, %ecx
	jle	.LBB6_8
.LBB6_4:
	testl	%r15d, %r15d
	jle	.LBB6_7
# BB#5:
	testl	%r14d, %r14d
	jle	.LBB6_7
# BB#6:
	movq	Q__align_gapmap.w1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.w2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.match(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.initverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.lastverticalw(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.m(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.mp(%rip), %rdi
	callq	FreeIntVec
	movq	Q__align_gapmap.mseq(%rip), %rdi
	callq	FreeCharMtx
	movq	Q__align_gapmap.digf1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.digf2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.diaf1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.diaf2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.gapz1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.gapz2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.gapf1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.gapf2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.ogcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.ogcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.fgcp1g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.fgcp2g(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.og_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.og_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.fg_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.fg_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.og_t_fg_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.og_t_fg_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.fg_t_og_h_dg_n1_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.fg_t_og_h_dg_n2_p(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.gapz_n1(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.gapz_n2(%rip), %rdi
	callq	FreeFloatVec
	movq	Q__align_gapmap.cpmx1(%rip), %rdi
	callq	FreeFloatMtx
	movq	Q__align_gapmap.cpmx2(%rip), %rdi
	callq	FreeFloatMtx
	movq	Q__align_gapmap.floatwork(%rip), %rdi
	callq	FreeFloatMtx
	movq	Q__align_gapmap.intwork(%rip), %rdi
	callq	FreeIntMtx
	movq	48(%rsp), %rcx          # 8-byte Reload
	movl	Q__align_gapmap.orlgth1(%rip), %r15d
	movl	Q__align_gapmap.orlgth2(%rip), %r14d
.LBB6_7:
	movq	96(%rsp), %rax          # 8-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	.LCPI6_0(%rip), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r15d, %eax
	cmovgel	%eax, %r15d
	leal	100(%r15), %ebp
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	mulsd	%xmm1, %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	%r14d, %eax
	cmovgel	%eax, %r14d
	leal	100(%r14), %ebx
	leal	102(%r14), %r12d
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.w1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.w2(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.match(%rip)
	leal	102(%r15), %r13d
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.initverticalw(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.lastverticalw(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.m(%rip)
	movl	%r12d, %edi
	callq	AllocateIntVec
	movq	%rax, Q__align_gapmap.mp(%rip)
	movl	njob(%rip), %edi
	leal	200(%r14,%r15), %esi
	callq	AllocateCharMtx
	movq	%rax, Q__align_gapmap.mseq(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.digf1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.digf2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.diaf1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.diaf2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.gapz1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.gapz2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.gapf1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.gapf2(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.ogcp1g(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.ogcp2g(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.fgcp1g(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.fgcp2g(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.og_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.og_h_dg_n2_p(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.fg_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.fg_h_dg_n2_p(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.og_t_fg_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.og_t_fg_h_dg_n2_p(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.fg_t_og_h_dg_n1_p(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.fg_t_og_h_dg_n2_p(%rip)
	movl	%r13d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.gapz_n1(%rip)
	movl	%r12d, %edi
	callq	AllocateFloatVec
	movq	%rax, Q__align_gapmap.gapz_n2(%rip)
	movl	$26, %edi
	movl	%r13d, %esi
	callq	AllocateFloatMtx
	movq	%rax, Q__align_gapmap.cpmx1(%rip)
	movl	$26, %edi
	movl	%r12d, %esi
	callq	AllocateFloatMtx
	movq	%rax, Q__align_gapmap.cpmx2(%rip)
	cmpl	%ebx, %ebp
	cmovgel	%ebp, %ebx
	addl	$2, %ebx
	movl	$26, %esi
	movl	%ebx, %edi
	callq	AllocateFloatMtx
	movq	%rax, Q__align_gapmap.floatwork(%rip)
	movl	$27, %esi
	movl	%ebx, %edi
	callq	AllocateIntMtx
	movq	%rax, Q__align_gapmap.intwork(%rip)
	movl	%r15d, Q__align_gapmap.orlgth1(%rip)
	movl	%r14d, Q__align_gapmap.orlgth2(%rip)
	movl	44(%rsp), %r13d         # 4-byte Reload
	movl	60(%rsp), %r12d         # 4-byte Reload
.LBB6_8:                                # %.preheader607
	testl	%r12d, %r12d
	jle	.LBB6_15
# BB#9:                                 # %.lr.ph678
	movq	Q__align_gapmap.mseq(%rip), %r9
	movq	Q__align_gapmap.mseq1(%rip), %rdi
	movslq	96(%rsp), %rax          # 4-byte Folded Reload
	movl	%r12d, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rbx
	xorl	%ebp, %ebp
	andq	$3, %rbx
	je	.LBB6_12
# BB#10:                                # %.prol.preheader983
	movq	64(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	movq	(%r9,%rbp,8), %rdx
	movq	%rdx, (%rdi,%rbp,8)
	movq	(%rsi,%rbp,8), %rdx
	movb	$0, (%rdx,%rax)
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_11
.LBB6_12:                               # %.prol.loopexit984
	cmpq	$3, %r8
	jb	.LBB6_15
# BB#13:                                # %.lr.ph678.new
	subq	%rbp, %rcx
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	24(%rdx,%rbp,8), %rdx
	leaq	24(%rdi,%rbp,8), %rdi
	leaq	24(%r9,%rbp,8), %rsi
	.p2align	4, 0x90
.LBB6_14:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rsi), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rsi), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rsi), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rsi), %rbp
	movq	%rbp, (%rdi)
	movq	(%rdx), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rdx
	addq	$32, %rdi
	addq	$32, %rsi
	addq	$-4, %rcx
	jne	.LBB6_14
.LBB6_15:                               # %.preheader606
	testl	%r13d, %r13d
	jle	.LBB6_23
# BB#16:                                # %.lr.ph675
	movq	Q__align_gapmap.mseq(%rip), %r8
	movq	Q__align_gapmap.mseq2(%rip), %r11
	movslq	48(%rsp), %rax          # 4-byte Folded Reload
	movslq	%r12d, %r10
	movl	44(%rsp), %ecx          # 4-byte Reload
	leaq	-1(%rcx), %r9
	movq	%rcx, %rdx
	andq	$3, %rdx
	je	.LBB6_17
# BB#18:                                # %.prol.preheader978
	leaq	(%r8,%r10,8), %rsi
	xorl	%ebx, %ebx
	movq	104(%rsp), %rdi         # 8-byte Reload
	.p2align	4, 0x90
.LBB6_19:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movq	%rbp, (%r11,%rbx,8)
	movq	(%rdi,%rbx,8), %rbp
	movb	$0, (%rbp,%rax)
	incq	%rbx
	cmpq	%rbx, %rdx
	jne	.LBB6_19
	jmp	.LBB6_20
.LBB6_17:
	xorl	%ebx, %ebx
.LBB6_20:                               # %.prol.loopexit979
	cmpq	$3, %r9
	jb	.LBB6_23
# BB#21:                                # %.lr.ph675.new
	subq	%rbx, %rcx
	movq	104(%rsp), %rdx         # 8-byte Reload
	leaq	24(%rdx,%rbx,8), %rsi
	leaq	24(%r11,%rbx,8), %rdi
	addq	%rbx, %r10
	leaq	24(%r8,%r10,8), %rdx
	.p2align	4, 0x90
.LBB6_22:                               # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rbp
	movq	%rbp, -24(%rdi)
	movq	-24(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-16(%rdx), %rbp
	movq	%rbp, -16(%rdi)
	movq	-16(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	-8(%rdx), %rbp
	movq	%rbp, -8(%rdi)
	movq	-8(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	movq	(%rdx), %rbp
	movq	%rbp, (%rdi)
	movq	(%rsi), %rbp
	movb	$0, (%rbp,%rax)
	addq	$32, %rsi
	addq	$32, %rdi
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB6_22
.LBB6_23:                               # %._crit_edge676
	movl	commonAlloc1(%rip), %ebx
	cmpl	%ebx, %r15d
	movl	commonAlloc2(%rip), %ebp
	jg	.LBB6_26
# BB#24:                                # %._crit_edge676
	cmpl	%ebp, %r14d
	jg	.LBB6_26
# BB#25:                                # %._crit_edge746
	movq	commonIP(%rip), %rax
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB6_31
.LBB6_26:                               # %._crit_edge676._crit_edge
	testl	%ebx, %ebx
	je	.LBB6_27
# BB#28:                                # %._crit_edge676._crit_edge
	testl	%ebp, %ebp
	movq	32(%rsp), %r12          # 8-byte Reload
	je	.LBB6_30
# BB#29:
	movq	commonIP(%rip), %rdi
	callq	FreeIntMtx
	movl	Q__align_gapmap.orlgth1(%rip), %r15d
	movl	commonAlloc1(%rip), %ebx
	movl	Q__align_gapmap.orlgth2(%rip), %r14d
	movl	commonAlloc2(%rip), %ebp
	jmp	.LBB6_30
.LBB6_27:
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB6_30:
	cmpl	%ebx, %r15d
	cmovgel	%r15d, %ebx
	cmpl	%ebp, %r14d
	cmovgel	%r14d, %ebp
	leal	10(%rbx), %edi
	leal	10(%rbp), %esi
	callq	AllocateIntMtx
	movq	%rax, commonIP(%rip)
	movl	%ebx, commonAlloc1(%rip)
	movl	%ebp, commonAlloc2(%rip)
	movq	64(%rsp), %rdi          # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	movq	72(%rsp), %rbp          # 8-byte Reload
.LBB6_31:
	movq	%rax, Q__align_gapmap.ijp(%rip)
	movq	Q__align_gapmap.cpmx1(%rip), %rsi
	movq	%r12, %rdx
	movl	%ebx, %ecx
	movl	60(%rsp), %r8d          # 4-byte Reload
	movl	%r8d, %r14d
	callq	cpmx_calc_new
	movq	Q__align_gapmap.cpmx2(%rip), %rsi
	movq	104(%rsp), %rdi         # 8-byte Reload
	movq	%rbp, %r13
	movq	%rbp, %rdx
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	44(%rsp), %ebp          # 4-byte Reload
	movl	%ebp, %r8d
	callq	cpmx_calc_new
	movq	504(%rsp), %rax
	testq	%rax, %rax
	movq	Q__align_gapmap.ogcp1g(%rip), %rdi
	je	.LBB6_33
# BB#32:
	movq	%r12, %rcx
	movq	%r13, %rbx
	movq	528(%rsp), %r13
	movq	520(%rsp), %rdx
	movq	%rdx, (%rsp)
	movl	%r14d, %r12d
	movl	%r12d, %esi
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdx
	movq	%rcx, %r14
	movq	96(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%rax, %r9
	callq	new_OpeningGapCount_zure
	movq	Q__align_gapmap.ogcp2g(%rip), %rdi
	movq	%r13, (%rsp)
	movl	44(%rsp), %esi          # 4-byte Reload
	movq	104(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movl	%r15d, %r8d
	movq	512(%rsp), %r9
	callq	new_OpeningGapCount_zure
	movq	Q__align_gapmap.fgcp1g(%rip), %rdi
	movq	520(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r12d, %esi
	movq	%rbp, %rdx
	movq	%r14, %rcx
	movq	96(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	504(%rsp), %r9
	callq	new_FinalGapCount_zure
	movq	Q__align_gapmap.fgcp2g(%rip), %rdi
	movq	528(%rsp), %rax
	movq	%rax, (%rsp)
	movl	44(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	%rbx, %rcx
	movq	%rbx, %r14
	movl	%r15d, %r8d
	movq	512(%rsp), %r9
	callq	new_FinalGapCount_zure
	movq	Q__align_gapmap.digf1(%rip), %rdi
	movq	520(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%r12d, %esi
	movl	%r12d, %ebx
	movq	%rbp, %rdx
	movq	32(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	movq	96(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	504(%rsp), %r9
	callq	getdigapfreq_part
	movq	Q__align_gapmap.digf2(%rip), %rdi
	movq	528(%rsp), %rax
	movq	%rax, (%rsp)
	movl	44(%rsp), %esi          # 4-byte Reload
	movq	%r13, %rdx
	movq	48(%rsp), %r12          # 8-byte Reload
	movq	%r14, %rcx
	movl	%r12d, %r8d
	movq	512(%rsp), %r9
	callq	getdigapfreq_part
	movq	Q__align_gapmap.diaf1(%rip), %rdi
	movq	520(%rsp), %rax
	movq	%rax, (%rsp)
	movl	%ebx, %esi
	movq	%rbp, %rdx
	movq	%r15, %rcx
	movq	96(%rsp), %r8           # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	504(%rsp), %r9
	callq	getdiaminofreq_part
	movq	Q__align_gapmap.diaf2(%rip), %rdi
	movq	528(%rsp), %rax
	movq	%rax, (%rsp)
	movq	96(%rsp), %rbp          # 8-byte Reload
	movl	44(%rsp), %esi          # 4-byte Reload
	movq	%r13, %r15
	movq	%r15, %rdx
	movq	%r14, %rcx
	movl	%r12d, %r8d
	movq	512(%rsp), %r9
	movl	44(%rsp), %r13d         # 4-byte Reload
	callq	getdiaminofreq_part
	movq	Q__align_gapmap.gapf1(%rip), %rdi
	movl	%ebx, %esi
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getgapfreq
	movq	Q__align_gapmap.gapf2(%rip), %rdi
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movl	%r12d, %r8d
	callq	getgapfreq
	movq	Q__align_gapmap.gapz1(%rip), %rdi
	movl	60(%rsp), %esi          # 4-byte Reload
	movq	%rbx, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%ebp, %r8d
	movq	504(%rsp), %rbp
	movq	%rbp, %r9
	callq	getgapfreq_zure_part
	movq	Q__align_gapmap.gapz2(%rip), %rdi
	movl	%r13d, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	movl	%r12d, %r8d
	movq	%rbp, %r9
	callq	getgapfreq_zure_part
	movq	%r12, %r8
	jmp	.LBB6_34
.LBB6_33:
	movl	%r14d, %esi
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%r12, %rcx
	movl	%ebx, %r8d
	callq	st_OpeningGapCount
	movq	Q__align_gapmap.ogcp2g(%rip), %rdi
	movl	%ebp, %esi
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%r13, %r14
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	st_OpeningGapCount
	movq	Q__align_gapmap.fgcp1g(%rip), %rdi
	movl	60(%rsp), %r12d         # 4-byte Reload
	movl	%r12d, %esi
	movq	%rbx, %r13
	movq	64(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%r13d, %r8d
	callq	st_FinalGapCount_zure
	movq	Q__align_gapmap.fgcp2g(%rip), %rdi
	movl	%ebp, %esi
	movq	104(%rsp), %rdx         # 8-byte Reload
	movq	%r14, %rcx
	movl	%r15d, %r8d
	callq	st_FinalGapCount_zure
	movq	Q__align_gapmap.digf1(%rip), %rdi
	movl	%r12d, %esi
	movq	%rbx, %rdx
	movl	%ebp, %r15d
	movq	32(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rcx
	movl	%r13d, %r8d
	callq	getdigapfreq_st
	movq	Q__align_gapmap.digf2(%rip), %rdi
	movl	%r15d, %esi
	movq	104(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rdx
	movq	%r14, %rcx
	movq	48(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	getdigapfreq_st
	movq	Q__align_gapmap.diaf1(%rip), %rdi
	movl	60(%rsp), %esi          # 4-byte Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	movl	%r13d, %r8d
	callq	getdiaminofreq_x
	movq	Q__align_gapmap.diaf2(%rip), %rdi
	movl	%r15d, %esi
	movq	%r12, %rdx
	movq	72(%rsp), %rcx          # 8-byte Reload
	movl	%ebx, %r8d
	callq	getdiaminofreq_x
	movq	Q__align_gapmap.gapf1(%rip), %rdi
	movl	60(%rsp), %ebx          # 4-byte Reload
	movl	%ebx, %esi
	movq	64(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rdx
	movq	%rbp, %rcx
	movl	%r13d, %r8d
	callq	getgapfreq
	movq	Q__align_gapmap.gapf2(%rip), %rdi
	movl	44(%rsp), %esi          # 4-byte Reload
	movq	%r12, %rdx
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rcx
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %r8d
	callq	getgapfreq
	movq	Q__align_gapmap.gapz1(%rip), %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%r13d, %r8d
	callq	getgapfreq_zure
	movq	Q__align_gapmap.gapz2(%rip), %rdi
	movl	44(%rsp), %esi          # 4-byte Reload
	movq	%r12, %rdx
	movq	%r14, %rcx
	movl	%ebp, %r8d
	callq	getgapfreq_zure
	movq	%rbp, %r8
.LBB6_34:
	movss	120(%rsp), %xmm12       # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	cmpl	$-1, %r8d
	jl	.LBB6_37
# BB#35:                                # %.lr.ph672
	movq	Q__align_gapmap.ogcp2g(%rip), %rax
	movq	Q__align_gapmap.digf2(%rip), %rcx
	cvtss2sd	%xmm12, %xmm0
	movq	Q__align_gapmap.og_h_dg_n2_p(%rip), %r11
	movq	Q__align_gapmap.fgcp2g(%rip), %rsi
	movq	Q__align_gapmap.fg_h_dg_n2_p(%rip), %r9
	movq	Q__align_gapmap.og_t_fg_h_dg_n2_p(%rip), %rbp
	movq	Q__align_gapmap.fg_t_og_h_dg_n2_p(%rip), %rbx
	movq	Q__align_gapmap.gapz2(%rip), %rdx
	movq	Q__align_gapmap.gapz_n2(%rip), %rdi
	leal	2(%r8), %r10d
	movsd	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movss	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB6_36:                               # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r11)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r9)
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbp)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbx)
	movaps	%xmm3, %xmm4
	subss	(%rdx), %xmm4
	movss	%xmm4, (%rdi)
	addq	$4, %rax
	addq	$4, %rcx
	addq	$4, %r11
	addq	$4, %rsi
	addq	$4, %r9
	addq	$4, %rbp
	addq	$4, %rbx
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%r10
	jne	.LBB6_36
.LBB6_37:                               # %._crit_edge673
	movq	96(%rsp), %r15          # 8-byte Reload
	cmpl	$-1, %r15d
	jl	.LBB6_40
# BB#38:                                # %.lr.ph668
	movq	Q__align_gapmap.ogcp1g(%rip), %rax
	movq	Q__align_gapmap.digf1(%rip), %rcx
	cvtss2sd	%xmm12, %xmm0
	movq	Q__align_gapmap.og_h_dg_n1_p(%rip), %r11
	movq	Q__align_gapmap.fgcp1g(%rip), %rsi
	movq	Q__align_gapmap.fg_h_dg_n1_p(%rip), %r9
	movq	Q__align_gapmap.og_t_fg_h_dg_n1_p(%rip), %rbp
	movq	Q__align_gapmap.fg_t_og_h_dg_n1_p(%rip), %rbx
	movq	Q__align_gapmap.gapz1(%rip), %rdx
	movq	Q__align_gapmap.gapz_n1(%rip), %rdi
	leal	2(%r15), %r10d
	movsd	.LCPI6_1(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	movss	.LCPI6_3(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	.p2align	4, 0x90
.LBB6_39:                               # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r11)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rcx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	subsd	%xmm4, %xmm5
	mulsd	%xmm0, %xmm5
	mulsd	%xmm2, %xmm5
	xorps	%xmm4, %xmm4
	cvtsd2ss	%xmm5, %xmm4
	movss	%xmm4, (%r9)
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbp)
	movss	(%rsi), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	movss	(%rax), %xmm4           # xmm4 = mem[0],zero,zero,zero
	cvtss2sd	%xmm4, %xmm4
	addsd	%xmm5, %xmm4
	movss	(%rcx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	cvtss2sd	%xmm5, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm2, %xmm4
	mulsd	%xmm0, %xmm4
	cvtsd2ss	%xmm4, %xmm4
	movss	%xmm4, (%rbx)
	movaps	%xmm3, %xmm4
	subss	(%rdx), %xmm4
	movss	%xmm4, (%rdi)
	addq	$4, %rax
	addq	$4, %rcx
	addq	$4, %r11
	addq	$4, %rsi
	addq	$4, %r9
	addq	$4, %rbp
	addq	$4, %rbx
	addq	$4, %rdx
	addq	$4, %rdi
	decq	%r10
	jne	.LBB6_39
.LBB6_40:                               # %._crit_edge669
	movq	488(%rsp), %rbx
	movabsq	$17179869180, %r12      # imm = 0x3FFFFFFFC
	movq	Q__align_gapmap.w1(%rip), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	Q__align_gapmap.w2(%rip), %rax
	movq	%rax, 184(%rsp)         # 8-byte Spill
	cmpb	$114, RNAscoremtx(%rip)
	movq	Q__align_gapmap.initverticalw(%rip), %rdi
	jne	.LBB6_41
# BB#42:
	testl	%r15d, %r15d
	je	.LBB6_43
# BB#44:                                # %.lr.ph.preheader.i
	leaq	(%r12,%r15,4), %rdx
	andq	%r12, %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	callq	memset
	jmp	.LBB6_45
.LBB6_41:
	movq	Q__align_gapmap.cpmx2(%rip), %rsi
	movq	Q__align_gapmap.cpmx1(%rip), %rdx
	movq	Q__align_gapmap.floatwork(%rip), %r9
	movq	Q__align_gapmap.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movl	%r15d, %r8d
	callq	match_calc
.LBB6_45:                               # %clearvec.exit
	movss	120(%rsp), %xmm12       # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movq	48(%rsp), %r8           # 8-byte Reload
	testq	%rbx, %rbx
	setne	%bl
	sete	%al
	testl	%r15d, %r15d
	je	.LBB6_52
# BB#46:                                # %clearvec.exit
	testb	%al, %al
	jne	.LBB6_52
# BB#47:                                # %.lr.ph.i
	movq	Q__align_gapmap.initverticalw(%rip), %rax
	movq	impmtx(%rip), %rcx
	movq	544(%rsp), %rdx
	movslq	(%rdx), %rdx
	testb	$1, %r15b
	jne	.LBB6_49
# BB#48:
	movq	536(%rsp), %rsi
	movl	%r15d, %edi
	cmpl	$1, %r15d
	jne	.LBB6_51
	jmp	.LBB6_52
.LBB6_49:
	leal	-1(%r15), %edi
	movq	536(%rsp), %rsi
	movq	%rsi, %rbp
	leaq	4(%rbp), %rsi
	movslq	(%rbp), %rbp
	movq	(%rcx,%rbp,8), %rbp
	movss	(%rbp,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	leaq	4(%rax), %rax
	cmpl	$1, %r15d
	je	.LBB6_52
	.p2align	4, 0x90
.LBB6_51:                               # =>This Inner Loop Header: Depth=1
	movslq	(%rsi), %rbp
	movq	(%rcx,%rbp,8), %rbp
	movss	(%rbp,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
	addl	$-2, %edi
	movslq	4(%rsi), %rbp
	leaq	8(%rsi), %rsi
	movq	(%rcx,%rbp,8), %rbp
	movss	(%rbp,%rdx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%rax), %xmm0
	movss	%xmm0, 4(%rax)
	leaq	8(%rax), %rax
	jne	.LBB6_51
	jmp	.LBB6_52
.LBB6_43:                               # %clearvec.exit.thread
	testq	%rbx, %rbx
	setne	%bl
.LBB6_52:                               # %imp_match_out_vead_tateQ_gapmap.exit
	cmpb	$114, RNAscoremtx(%rip)
	movb	%bl, 144(%rsp)          # 1-byte Spill
	jne	.LBB6_53
# BB#54:
	testl	%r8d, %r8d
	je	.LBB6_57
# BB#55:                                # %.lr.ph.preheader.i576
	leaq	(%r12,%r8,4), %rdx
	andq	%r12, %rdx
	addq	$4, %rdx
	xorl	%esi, %esi
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	memset
	jmp	.LBB6_56
.LBB6_53:
	movq	Q__align_gapmap.cpmx1(%rip), %rsi
	movq	Q__align_gapmap.cpmx2(%rip), %rdx
	movq	Q__align_gapmap.floatwork(%rip), %r9
	movq	Q__align_gapmap.intwork(%rip), %rax
	movq	%rax, (%rsp)
	movl	$1, 8(%rsp)
	xorl	%ecx, %ecx
	movq	32(%rsp), %rdi          # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	callq	match_calc
.LBB6_56:                               # %clearvec.exit578
	movb	144(%rsp), %bl          # 1-byte Reload
	movss	120(%rsp), %xmm12       # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movq	48(%rsp), %r8           # 8-byte Reload
.LBB6_57:                               # %clearvec.exit578
	testb	%bl, %bl
	movq	64(%rsp), %r9           # 8-byte Reload
	je	.LBB6_63
# BB#58:
	testl	%r8d, %r8d
	je	.LBB6_63
# BB#59:                                # %.lr.ph.i579.preheader
	movq	impmtx(%rip), %rax
	movq	536(%rsp), %rcx
	movslq	(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
	testb	$1, %r8b
	movq	544(%rsp), %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	movl	%r8d, %edx
	je	.LBB6_61
# BB#60:                                # %.lr.ph.i579.prol
	leal	-1(%r8), %edx
	movq	544(%rsp), %rcx
	movq	%rcx, %rsi
	leaq	4(%rsi), %rcx
	movslq	(%rsi), %rsi
	movss	(%rax,%rsi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	4(%rdi), %rsi
	addss	(%rdi), %xmm0
	movss	%xmm0, (%rdi)
.LBB6_61:                               # %.lr.ph.i579.prol.loopexit
	cmpl	$1, %r8d
	je	.LBB6_63
	.p2align	4, 0x90
.LBB6_62:                               # %.lr.ph.i579
                                        # =>This Inner Loop Header: Depth=1
	movslq	(%rcx), %rdi
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	addl	$-2, %edx
	movslq	4(%rcx), %rdi
	leaq	8(%rcx), %rcx
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%rsi), %xmm0
	movss	%xmm0, 4(%rsi)
	leaq	8(%rsi), %rsi
	jne	.LBB6_62
.LBB6_63:                               # %imp_match_out_veadQ_gapmap.exit
	cmpl	$1, outgap(%rip)
	jne	.LBB6_64
# BB#74:
	movq	Q__align_gapmap.ogcp1g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	Q__align_gapmap.og_h_dg_n2_p(%rip), %rax
	mulss	(%rax), %xmm0
	xorpd	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movq	Q__align_gapmap.ogcp2g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	Q__align_gapmap.og_h_dg_n1_p(%rip), %rax
	mulss	(%rax), %xmm0
	addss	%xmm1, %xmm0
	movq	Q__align_gapmap.fgcp1g(%rip), %rax
	movss	(%rax), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movq	Q__align_gapmap.fg_h_dg_n2_p(%rip), %rax
	mulss	(%rax), %xmm1
	addss	%xmm0, %xmm1
	movq	Q__align_gapmap.fgcp2g(%rip), %rax
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movq	Q__align_gapmap.fg_h_dg_n1_p(%rip), %rax
	mulss	(%rax), %xmm0
	addss	%xmm1, %xmm0
	movq	Q__align_gapmap.initverticalw(%rip), %r14
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14)
	movq	32(%rsp), %r8           # 8-byte Reload
	addss	(%r8), %xmm0
	movss	%xmm0, (%r8)
	testl	%r15d, %r15d
	jle	.LBB6_91
# BB#75:                                # %.lr.ph661
	movq	Q__align_gapmap.gapz_n2(%rip), %rax
	movq	Q__align_gapmap.og_t_fg_h_dg_n1_p(%rip), %rcx
	movq	Q__align_gapmap.fg_t_og_h_dg_n1_p(%rip), %r13
	leaq	1(%r15), %r11
	movl	%r11d, %edx
	leaq	-1(%rdx), %r10
	cmpq	$3, %r10
	jbe	.LBB6_76
# BB#82:                                # %min.iters.checked
	movl	%r15d, %esi
	andl	$3, %esi
	movq	%r10, %rbp
	subq	%rsi, %rbp
	je	.LBB6_76
# BB#83:                                # %vector.memcheck
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	leaq	4(%rax), %r8
	leaq	4(%r14), %rbx
	leaq	(%r14,%rdx,4), %rdi
	leaq	4(%r13), %rsi
	movq	%r13, 72(%rsp)          # 8-byte Spill
	leaq	(%r13,%rdx,4), %r15
	cmpq	%rax, %r14
	sbbb	%r12b, %r12b
	cmpq	%rdi, %r8
	sbbb	%r8b, %r8b
	andb	%r12b, %r8b
	cmpq	%rcx, %rbx
	sbbb	%r13b, %r13b
	cmpq	%rdi, %rcx
	sbbb	%r9b, %r9b
	cmpq	%r15, %rbx
	sbbb	%r15b, %r15b
	cmpq	%rdi, %rsi
	sbbb	%dil, %dil
	movl	$1, %r12d
	testb	$1, %r8b
	jne	.LBB6_84
# BB#85:                                # %vector.memcheck
	andb	%r9b, %r13b
	andb	$1, %r13b
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	jne	.LBB6_86
# BB#87:                                # %vector.memcheck
	andb	%dil, %r15b
	andb	$1, %r15b
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	jne	.LBB6_77
# BB#88:                                # %vector.body.preheader
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulps	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	1(%rbp), %r12
	.p2align	4, 0x90
.LBB6_89:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rbx), %xmm2
	addps	%xmm1, %xmm2
	movups	%xmm2, (%rbx)
	movups	(%rsi), %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movups	%xmm3, (%rbx)
	addq	$16, %rbx
	addq	$16, %rsi
	addq	$-4, %rbp
	jne	.LBB6_89
# BB#90:                                # %middle.block
	cmpq	$0, 136(%rsp)           # 8-byte Folded Reload
	jne	.LBB6_77
	jmp	.LBB6_91
.LBB6_64:                               # %.preheader605
	testl	%r8d, %r8d
	jle	.LBB6_71
# BB#65:                                # %.lr.ph665
	movslq	offset(%rip), %rax
	leaq	1(%r8), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB6_66
# BB#67:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI6_4(%rip), %xmm0
	movq	32(%rsp), %rdx          # 8-byte Reload
	movss	4(%rdx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rdx)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB6_69
	jmp	.LBB6_71
.LBB6_76:
	movl	$1, %r12d
.LBB6_77:                               # %scalar.ph.preheader
	subl	%r12d, %r11d
	testb	$1, %r11b
	movq	%r12, %rdi
	je	.LBB6_79
# BB#78:                                # %scalar.ph.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%r14,%r12,4), %xmm0
	movss	%xmm0, (%r14,%r12,4)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%r13,%r12,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r14,%r12,4)
	leaq	1(%r12), %rdi
.LBB6_79:                               # %scalar.ph.prol.loopexit
	cmpq	%r12, %r10
	je	.LBB6_91
# BB#80:                                # %scalar.ph.preheader.new
	subq	%rdi, %rdx
	leaq	4(%r14,%rdi,4), %rsi
	leaq	4(%r13,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB6_81:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB6_81
.LBB6_91:                               # %.preheader601
	movq	48(%rsp), %r10          # 8-byte Reload
	testl	%r10d, %r10d
	jle	.LBB6_92
# BB#93:                                # %.lr.ph658
	movq	Q__align_gapmap.gapz_n1(%rip), %rax
	movq	Q__align_gapmap.og_t_fg_h_dg_n2_p(%rip), %rcx
	movq	Q__align_gapmap.fg_t_og_h_dg_n2_p(%rip), %r14
	movq	%r10, %rsi
	leaq	1(%rsi), %r11
	movl	%r11d, %edx
	leaq	-1(%rdx), %r10
	cmpq	$3, %r10
	jbe	.LBB6_94
# BB#100:                               # %min.iters.checked787
	movl	%esi, %r13d
	andl	$3, %r13d
	movq	%r10, %rdi
	subq	%r13, %rdi
	je	.LBB6_94
# BB#101:                               # %vector.memcheck816
	movq	%r14, %r9
	leaq	4(%rax), %r14
	leaq	4(%r8), %rsi
	leaq	(%r8,%rdx,4), %rbx
	leaq	4(%r9), %rbp
	movq	%r9, 72(%rsp)           # 8-byte Spill
	leaq	(%r9,%rdx,4), %r12
	cmpq	%rax, %r8
	sbbb	%r15b, %r15b
	cmpq	%rbx, %r14
	sbbb	%r8b, %r8b
	andb	%r15b, %r8b
	cmpq	%rcx, %rsi
	sbbb	%r15b, %r15b
	cmpq	%rbx, %rcx
	sbbb	%r9b, %r9b
	cmpq	%r12, %rsi
	sbbb	%r14b, %r14b
	cmpq	%rbx, %rbp
	sbbb	%r12b, %r12b
	movl	$1, %ebx
	testb	$1, %r8b
	jne	.LBB6_102
# BB#103:                               # %vector.memcheck816
	andb	%r9b, %r15b
	andb	$1, %r15b
	movq	64(%rsp), %r9           # 8-byte Reload
	jne	.LBB6_104
# BB#105:                               # %vector.memcheck816
	andb	%r12b, %r14b
	andb	$1, %r14b
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movabsq	$17179869180, %r12      # imm = 0x3FFFFFFFC
	movq	72(%rsp), %r14          # 8-byte Reload
	jne	.LBB6_95
# BB#106:                               # %vector.body783.preheader
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	(%rcx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulps	%xmm2, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	leaq	1(%rdi), %rbx
	.p2align	4, 0x90
.LBB6_107:                              # %vector.body783
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rsi), %xmm2
	addps	%xmm1, %xmm2
	movups	%xmm2, (%rsi)
	movups	(%rbp), %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm2, %xmm3
	movups	%xmm3, (%rsi)
	addq	$16, %rsi
	addq	$16, %rbp
	addq	$-4, %rdi
	jne	.LBB6_107
# BB#108:                               # %middle.block784
	testq	%r13, %r13
	jne	.LBB6_95
	jmp	.LBB6_113
.LBB6_94:
	movl	$1, %ebx
	movabsq	$17179869180, %r12      # imm = 0x3FFFFFFFC
.LBB6_95:                               # %scalar.ph785.preheader
	subl	%ebx, %r11d
	testb	$1, %r11b
	movq	%rbx, %rdi
	je	.LBB6_97
# BB#96:                                # %scalar.ph785.prol
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%r8,%rbx,4), %xmm0
	movss	%xmm0, (%r8,%rbx,4)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%r14,%rbx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r8,%rbx,4)
	leaq	1(%rbx), %rdi
.LBB6_97:                               # %scalar.ph785.prol.loopexit
	cmpq	%rbx, %r10
	je	.LBB6_113
# BB#98:                                # %scalar.ph785.preheader.new
	subq	%rdi, %rdx
	leaq	4(%r8,%rdi,4), %rsi
	leaq	4(%r14,%rdi,4), %rdi
	.p2align	4, 0x90
.LBB6_99:                               # %scalar.ph785
                                        # =>This Inner Loop Header: Depth=1
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	-4(%rsi), %xmm0
	movss	%xmm0, -4(%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	-4(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, -4(%rsi)
	movss	(%rax), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	(%rcx), %xmm0
	addss	(%rsi), %xmm0
	movss	%xmm0, (%rsi)
	movss	4(%rax), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi), %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addq	$8, %rdi
	addq	$-2, %rdx
	jne	.LBB6_99
	jmp	.LBB6_113
.LBB6_92:                               # %.loopexit602.thread
	movq	Q__align_gapmap.m(%rip), %r13
	movl	$0, (%r13)
	movb	$1, 84(%rsp)            # 1-byte Folded Spill
	movabsq	$17179869180, %r12      # imm = 0x3FFFFFFFC
	jmp	.LBB6_130
.LBB6_66:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB6_71
.LBB6_69:                               # %.lr.ph665.new
	subq	%rdx, %rcx
	movq	32(%rsp), %rsi          # 8-byte Reload
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_70:                               # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB6_70
.LBB6_71:                               # %.preheader603
	testl	%r15d, %r15d
	movq	32(%rsp), %r8           # 8-byte Reload
	jle	.LBB6_113
# BB#72:                                # %.lr.ph663
	movq	Q__align_gapmap.initverticalw(%rip), %rsi
	movslq	offset(%rip), %rax
	leaq	1(%r15), %rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB6_73
# BB#109:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	mulsd	.LCPI6_4(%rip), %xmm0
	movss	4(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%rsi)
	movl	$2, %edx
	cmpq	$2, %rcx
	jne	.LBB6_111
	jmp	.LBB6_113
.LBB6_73:
	movl	$1, %edx
	cmpq	$2, %rcx
	je	.LBB6_113
.LBB6_111:                              # %.lr.ph663.new
	subq	%rdx, %rcx
	leaq	4(%rsi,%rdx,4), %rsi
	leaq	1(%rdx), %rdi
	imull	%eax, %edx
	imull	%eax, %edi
	addl	%eax, %eax
	xorl	%ebp, %ebp
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_112:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rsi)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rsi)
	addq	$8, %rsi
	addl	%eax, %ebp
	addq	$-2, %rcx
	jne	.LBB6_112
.LBB6_113:                              # %.loopexit602
	movq	Q__align_gapmap.m(%rip), %r13
	movl	$0, (%r13)
	movq	48(%rsp), %r10          # 8-byte Reload
	testl	%r10d, %r10d
	setle	84(%rsp)                # 1-byte Folded Spill
	jle	.LBB6_114
# BB#115:                               # %.lr.ph655
	movq	Q__align_gapmap.mp(%rip), %rax
	movss	.LCPI6_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	leaq	1(%r10), %rdi
	movl	%edi, %ecx
	leaq	-1(%rcx), %r14
	cmpq	$8, %r14
	jae	.LBB6_117
# BB#116:
	movl	$1, %edx
	jmp	.LBB6_125
.LBB6_114:
	movb	$1, 84(%rsp)            # 1-byte Folded Spill
	jmp	.LBB6_130
.LBB6_117:                              # %min.iters.checked833
	movl	%r10d, %r8d
	andl	$7, %r8d
	movq	%r14, %rsi
	subq	%r8, %rsi
	je	.LBB6_118
# BB#119:                               # %vector.memcheck848
	leaq	4(%r13), %rdx
	movq	32(%rsp), %rbx          # 8-byte Reload
	leaq	-4(%rbx,%rcx,4), %rbp
	cmpq	%rbp, %rdx
	jae	.LBB6_122
# BB#120:                               # %vector.memcheck848
	leaq	(%r13,%rcx,4), %rdx
	cmpq	%rdx, %rbx
	jae	.LBB6_122
# BB#121:
	movl	$1, %edx
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	%rbx, %r8
	jmp	.LBB6_125
.LBB6_118:
	movl	$1, %edx
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	jmp	.LBB6_125
.LBB6_122:                              # %vector.ph849
	leaq	1(%rsi), %rdx
	movaps	%xmm0, %xmm1
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	xorl	%ebp, %ebp
	xorpd	%xmm2, %xmm2
	.p2align	4, 0x90
.LBB6_123:                              # %vector.body829
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm2, 4(%rax,%rbp,4)
	movupd	%xmm2, 20(%rax,%rbp,4)
	movups	(%rbx,%rbp,4), %xmm3
	movups	16(%rbx,%rbp,4), %xmm4
	addps	%xmm1, %xmm3
	addps	%xmm1, %xmm4
	movups	%xmm3, 4(%r13,%rbp,4)
	movups	%xmm4, 20(%r13,%rbp,4)
	addq	$8, %rbp
	cmpq	%rbp, %rsi
	jne	.LBB6_123
# BB#124:                               # %middle.block830
	testq	%r8, %r8
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	%rbx, %r8
	je	.LBB6_130
.LBB6_125:                              # %scalar.ph831.preheader
	subl	%edx, %edi
	subq	%rdx, %r14
	andq	$3, %rdi
	je	.LBB6_128
# BB#126:                               # %scalar.ph831.prol.preheader
	negq	%rdi
	.p2align	4, 0x90
.LBB6_127:                              # %scalar.ph831.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rdx,4)
	movss	-4(%r8,%rdx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r13,%rdx,4)
	incq	%rdx
	incq	%rdi
	jne	.LBB6_127
.LBB6_128:                              # %scalar.ph831.prol.loopexit
	cmpq	$3, %r14
	jb	.LBB6_130
	.p2align	4, 0x90
.LBB6_129:                              # %scalar.ph831
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, (%rax,%rdx,4)
	movss	-4(%r8,%rdx,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, (%r13,%rdx,4)
	movl	$0, 4(%rax,%rdx,4)
	movss	(%r8,%rdx,4), %xmm1     # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 4(%r13,%rdx,4)
	movl	$0, 8(%rax,%rdx,4)
	movss	4(%r8,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 8(%r13,%rdx,4)
	movl	$0, 12(%rax,%rdx,4)
	movss	8(%r8,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	%xmm1, 12(%r13,%rdx,4)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB6_129
.LBB6_130:                              # %._crit_edge656
	movabsq	$-4294967296, %rax      # imm = 0xFFFFFFFF00000000
	testl	%r10d, %r10d
	xorps	%xmm0, %xmm0
	je	.LBB6_132
# BB#131:
	movq	%r10, %rcx
	shlq	$32, %rcx
	addq	%rax, %rcx
	sarq	$30, %rcx
	movss	(%r8,%rcx), %xmm0       # xmm0 = mem[0],zero,zero,zero
.LBB6_132:
	movq	Q__align_gapmap.lastverticalw(%rip), %rcx
	movq	%rcx, 112(%rsp)         # 8-byte Spill
	movss	%xmm0, (%rcx)
	movl	outgap(%rip), %r14d
	cmpl	$1, %r14d
	movl	%r15d, %ecx
	sbbl	$-1, %ecx
	cmpl	$2, %ecx
	jl	.LBB6_133
# BB#134:                               # %.lr.ph647
	movq	Q__align_gapmap.initverticalw(%rip), %rdx
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leaq	(%r12,%r10,4), %rdx
	andq	%r12, %rdx
	addq	$4, %rdx
	movq	%rdx, 272(%rsp)         # 8-byte Spill
	movq	impmtx(%rip), %rdx
	movq	%rdx, 264(%rsp)         # 8-byte Spill
	mulss	.LCPI6_5(%rip), %xmm12
	movq	Q__align_gapmap.ijp(%rip), %rdx
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.mp(%rip), %rdx
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movq	Q__align_gapmap.fg_t_og_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 136(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.og_t_fg_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.og_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 168(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.fg_h_dg_n2_p(%rip), %rdx
	movq	%rdx, 160(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.gapz_n2(%rip), %r14
	movq	Q__align_gapmap.fgcp2g(%rip), %rdx
	movq	%rdx, 200(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.ogcp2g(%rip), %rdx
	movq	%rdx, 192(%rsp)         # 8-byte Spill
	movq	%r10, %rdx
	shlq	$32, %rdx
	addq	%rax, %rdx
	sarq	$32, %rdx
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movl	%ecx, %eax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	movl	%r10d, %eax
	andl	$1, %eax
	movl	%eax, 252(%rsp)         # 4-byte Spill
	xorps	%xmm4, %xmm4
	movq	Q__align_gapmap.fg_t_og_h_dg_n1_p(%rip), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.og_t_fg_h_dg_n1_p(%rip), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.og_h_dg_n1_p(%rip), %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.fg_h_dg_n1_p(%rip), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.gapz_n1(%rip), %rax
	movq	%rax, 312(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.fgcp1g(%rip), %rax
	movq	%rax, 304(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.ogcp1g(%rip), %rax
	movq	%rax, 296(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.cpmx1(%rip), %rbp
	movq	Q__align_gapmap.floatwork(%rip), %rax
	movq	%rax, 288(%rsp)         # 8-byte Spill
	movq	Q__align_gapmap.intwork(%rip), %rax
	movq	%rax, 280(%rsp)         # 8-byte Spill
	leal	-1(%r10), %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	544(%rsp), %rax
	leaq	4(%rax), %rax
	movq	%rax, 256(%rsp)         # 8-byte Spill
	movl	$1, %ebx
	movq	184(%rsp), %rcx         # 8-byte Reload
	movss	%xmm12, 120(%rsp)       # 4-byte Spill
	.p2align	4, 0x90
.LBB6_135:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_137 Depth 2
                                        #       Child Loop BB6_138 Depth 3
                                        #     Child Loop BB6_141 Depth 2
                                        #       Child Loop BB6_143 Depth 3
                                        #     Child Loop BB6_152 Depth 2
                                        #     Child Loop BB6_155 Depth 2
	movq	%rcx, %r12
	movq	%r8, %r15
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	-4(%rax,%rbx,4), %eax
	movl	%eax, (%r15)
	cmpb	$114, RNAscoremtx(%rip)
	jne	.LBB6_136
# BB#145:                               #   in Loop: Header=BB6_135 Depth=1
	movq	48(%rsp), %r10          # 8-byte Reload
	testl	%r10d, %r10d
	je	.LBB6_147
# BB#146:                               # %.lr.ph.preheader.i587
                                        #   in Loop: Header=BB6_135 Depth=1
	xorl	%esi, %esi
	movq	%r12, %rdi
	movq	272(%rsp), %rdx         # 8-byte Reload
	movss	%xmm4, 184(%rsp)        # 4-byte Spill
	callq	memset
	movss	184(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	120(%rsp), %xmm12       # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movq	48(%rsp), %r10          # 8-byte Reload
	cmpb	$0, 144(%rsp)           # 1-byte Folded Reload
	jne	.LBB6_148
	jmp	.LBB6_153
	.p2align	4, 0x90
.LBB6_136:                              #   in Loop: Header=BB6_135 Depth=1
	movl	$n_dis_consweight_multi, %eax
	xorl	%ecx, %ecx
	movq	48(%rsp), %r10          # 8-byte Reload
	.p2align	4, 0x90
.LBB6_137:                              #   Parent Loop BB6_135 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_138 Depth 3
	movl	$0, 320(%rsp,%rcx,4)
	xorps	%xmm0, %xmm0
	movl	$1, %edx
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB6_138:                              #   Parent Loop BB6_135 Depth=1
                                        #     Parent Loop BB6_137 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-8(%rbp,%rdx,8), %rdi
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	(%rdi,%rbx,4), %xmm1
	addss	%xmm0, %xmm1
	movss	104(%rsi), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	(%rbp,%rdx,8), %rdi
	mulss	(%rdi,%rbx,4), %xmm0
	addss	%xmm1, %xmm0
	addq	$208, %rsi
	addq	$2, %rdx
	cmpq	$27, %rdx
	jne	.LBB6_138
# BB#139:                               #   in Loop: Header=BB6_137 Depth=2
	movss	%xmm0, 320(%rsp,%rcx,4)
	incq	%rcx
	addq	$4, %rax
	cmpq	$26, %rcx
	jne	.LBB6_137
# BB#140:                               # %.preheader.i
                                        #   in Loop: Header=BB6_135 Depth=1
	testl	%r10d, %r10d
	movl	%r10d, %r8d
	movq	280(%rsp), %rcx         # 8-byte Reload
	movq	288(%rsp), %r9          # 8-byte Reload
	movq	%r12, %rsi
	je	.LBB6_147
	.p2align	4, 0x90
.LBB6_141:                              # %.lr.ph84.i
                                        #   Parent Loop BB6_135 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_143 Depth 3
	decl	%r8d
	movl	$0, (%rsi)
	movq	(%rcx), %rdi
	movl	(%rdi), %edx
	testl	%edx, %edx
	js	.LBB6_144
# BB#142:                               # %.lr.ph.preheader.i594
                                        #   in Loop: Header=BB6_141 Depth=2
	movq	(%r9), %rax
	addq	$4, %rdi
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB6_143:                              # %.lr.ph.i595
                                        #   Parent Loop BB6_135 Depth=1
                                        #     Parent Loop BB6_141 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%edx, %rdx
	movss	320(%rsp,%rdx,4), %xmm1 # xmm1 = mem[0],zero,zero,zero
	mulss	(%rax), %xmm1
	addq	$4, %rax
	addss	%xmm1, %xmm0
	movss	%xmm0, (%rsi)
	movl	(%rdi), %edx
	addq	$4, %rdi
	testl	%edx, %edx
	jns	.LBB6_143
.LBB6_144:                              # %._crit_edge.i596
                                        #   in Loop: Header=BB6_141 Depth=2
	addq	$8, %rcx
	addq	$8, %r9
	addq	$4, %rsi
	testl	%r8d, %r8d
	jne	.LBB6_141
.LBB6_147:                              # %clearvec.exit589
                                        #   in Loop: Header=BB6_135 Depth=1
	cmpb	$0, 144(%rsp)           # 1-byte Folded Reload
	je	.LBB6_153
.LBB6_148:                              #   in Loop: Header=BB6_135 Depth=1
	testl	%r10d, %r10d
	je	.LBB6_153
# BB#149:                               # %.lr.ph.i584.preheader
                                        #   in Loop: Header=BB6_135 Depth=1
	movq	536(%rsp), %rax
	movslq	(%rax,%rbx,4), %rax
	movq	264(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	cmpl	$0, 252(%rsp)           # 4-byte Folded Reload
	movq	544(%rsp), %rdx
	movq	%r12, %rcx
	movl	%r10d, %esi
	je	.LBB6_151
# BB#150:                               # %.lr.ph.i584.prol
                                        #   in Loop: Header=BB6_135 Depth=1
	movq	544(%rsp), %rcx
	movslq	(%rcx), %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	leaq	4(%r12), %rcx
	addss	(%r12), %xmm0
	movss	%xmm0, (%r12)
	movq	256(%rsp), %rdx         # 8-byte Reload
	movq	32(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
.LBB6_151:                              # %.lr.ph.i584.prol.loopexit
                                        #   in Loop: Header=BB6_135 Depth=1
	cmpl	$1, %r10d
	je	.LBB6_153
	.p2align	4, 0x90
.LBB6_152:                              # %.lr.ph.i584
                                        #   Parent Loop BB6_135 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	(%rdx), %rdi
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	(%rcx), %xmm0
	movss	%xmm0, (%rcx)
	addl	$-2, %esi
	movslq	4(%rdx), %rdi
	leaq	8(%rdx), %rdx
	movss	(%rax,%rdi,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	addss	4(%rcx), %xmm0
	movss	%xmm0, 4(%rcx)
	leaq	8(%rcx), %rcx
	jne	.LBB6_152
	.p2align	4, 0x90
.LBB6_153:                              # %imp_match_out_veadQ_gapmap.exit586
                                        #   in Loop: Header=BB6_135 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	(%rax,%rbx,4), %eax
	movl	%eax, (%r12)
	movq	%r15, %rcx
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm2, Q__align_gapmap.mi(%rip)
	leaq	1(%rbx), %r8
	xorl	%r10d, %r10d
	cmpb	$0, 84(%rsp)            # 1-byte Folded Reload
	jne	.LBB6_165
# BB#154:                               # %.lr.ph640.preheader
                                        #   in Loop: Header=BB6_135 Depth=1
	leaq	-1(%rbx), %r9
	movq	232(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rbx,4), %xmm8    # xmm8 = mem[0],zero,zero,zero
	movq	224(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rbx,4), %xmm9    # xmm9 = mem[0],zero,zero,zero
	movq	216(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rbx,4), %xmm10   # xmm10 = mem[0],zero,zero,zero
	movq	208(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rbx,4), %xmm11   # xmm11 = mem[0],zero,zero,zero
	movq	312(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rbx,4), %xmm5    # xmm5 = mem[0],zero,zero,zero
	movss	4(%rax,%rbx,4), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movq	304(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rbx,4), %xmm7    # xmm7 = mem[0],zero,zero,zero
	movq	296(%rsp), %rax         # 8-byte Reload
	movss	(%rax,%rbx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbx,8), %rsi
	xorl	%edi, %edi
	movl	$-1, %eax
	xorl	%r10d, %r10d
	jmp	.LBB6_155
	.p2align	4, 0x90
.LBB6_164:                              # %..lr.ph640_crit_edge
                                        #   in Loop: Header=BB6_155 Depth=2
	movss	4(%rcx,%rdi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	incq	%rdi
	decl	%eax
.LBB6_155:                              # %.lr.ph640
                                        #   Parent Loop BB6_135 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	168(%rsp), %rdx         # 8-byte Reload
	movss	4(%rdx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm0, %xmm3
	movq	192(%rsp), %rdx         # 8-byte Reload
	movss	4(%rdx,%rdi,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm3, %xmm4
	movq	160(%rsp), %rdx         # 8-byte Reload
	movss	4(%rdx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	movq	200(%rsp), %rdx         # 8-byte Reload
	movss	4(%rdx,%rdi,4), %xmm4   # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm4
	addss	%xmm3, %xmm4
	movl	$0, 4(%rsi,%rdi,4)
	movq	136(%rsp), %rdx         # 8-byte Reload
	movss	4(%rdx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm2, %xmm3
	ucomiss	%xmm4, %xmm3
	jbe	.LBB6_157
# BB#156:                               #   in Loop: Header=BB6_155 Depth=2
	leal	(%r10,%rax), %ecx
	movl	%ecx, 4(%rsi,%rdi,4)
	movq	%r15, %rcx
	movaps	%xmm3, %xmm4
.LBB6_157:                              #   in Loop: Header=BB6_155 Depth=2
	movq	176(%rsp), %rdx         # 8-byte Reload
	movss	4(%rdx,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	addss	%xmm3, %xmm0
	ucomiss	%xmm2, %xmm0
	jb	.LBB6_159
# BB#158:                               #   in Loop: Header=BB6_155 Depth=2
	movss	%xmm0, Q__align_gapmap.mi(%rip)
	movaps	%xmm0, %xmm2
	movl	%edi, %r10d
.LBB6_159:                              #   in Loop: Header=BB6_155 Depth=2
	movss	8(%r14,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	movss	4(%r13,%rdi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	ucomiss	%xmm4, %xmm3
	jbe	.LBB6_161
# BB#160:                               #   in Loop: Header=BB6_155 Depth=2
	movl	%ebx, %ecx
	movq	72(%rsp), %r11          # 8-byte Reload
	subl	4(%r11,%rdi,4), %ecx
	movl	%ecx, 4(%rsi,%rdi,4)
	movq	%r15, %rcx
	movaps	%xmm3, %xmm4
.LBB6_161:                              #   in Loop: Header=BB6_155 Depth=2
	movss	4(%r14,%rdi,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	addss	(%rcx,%rdi,4), %xmm3
	ucomiss	%xmm0, %xmm3
	jb	.LBB6_163
# BB#162:                               #   in Loop: Header=BB6_155 Depth=2
	movss	%xmm3, 4(%r13,%rdi,4)
	movq	72(%rsp), %rdx          # 8-byte Reload
	movl	%r9d, 4(%rdx,%rdi,4)
.LBB6_163:                              #   in Loop: Header=BB6_155 Depth=2
	movss	4(%r12,%rdi,4), %xmm0   # xmm0 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm0
	movss	%xmm0, 4(%r12,%rdi,4)
	cmpl	%edi, 32(%rsp)          # 4-byte Folded Reload
	jne	.LBB6_164
.LBB6_165:                              # %._crit_edge641
                                        #   in Loop: Header=BB6_135 Depth=1
	movq	152(%rsp), %rax         # 8-byte Reload
	movl	(%r12,%rax,4), %eax
	movq	112(%rsp), %rsi         # 8-byte Reload
	movl	%eax, (%rsi,%rbx,4)
	cmpq	240(%rsp), %r8          # 8-byte Folded Reload
	movq	%r8, %rbx
	movq	%r12, %r8
	movq	64(%rsp), %r9           # 8-byte Reload
	jne	.LBB6_135
# BB#166:                               # %._crit_edge648
	movss	%xmm4, 184(%rsp)        # 4-byte Spill
	movl	%r10d, Q__align_gapmap.mpi(%rip)
	movl	outgap(%rip), %r14d
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	112(%rsp), %rsi         # 8-byte Reload
	movq	48(%rsp), %r10          # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB6_182
	jmp	.LBB6_168
.LBB6_133:
	xorps	%xmm0, %xmm0
	movss	%xmm0, 184(%rsp)        # 4-byte Spill
	movq	%r8, %r12
	movq	112(%rsp), %rsi         # 8-byte Reload
	testl	%r14d, %r14d
	jne	.LBB6_182
.LBB6_168:                              # %.preheader600
	cmpb	$0, 84(%rsp)            # 1-byte Folded Reload
	jne	.LBB6_175
# BB#169:                               # %.lr.ph614
	movslq	offset(%rip), %r8
	movq	%r10, %rdx
	incq	%rdx
	movl	%edx, %ecx
	testb	$1, %dl
	jne	.LBB6_170
# BB#171:
	leal	-1(%r10), %edx
	imull	%r8d, %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	mulsd	.LCPI6_4(%rip), %xmm0
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm1, %xmm1
	addsd	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm1, %xmm0
	movss	%xmm0, 4(%r12)
	movl	$2, %ebp
	cmpq	$2, %rcx
	jne	.LBB6_173
	jmp	.LBB6_175
.LBB6_170:
	movl	$1, %ebp
	cmpq	$2, %rcx
	je	.LBB6_175
.LBB6_173:                              # %.lr.ph614.new
	subq	%rbp, %rcx
	movl	%r10d, %edx
	subl	%ebp, %edx
	imull	%r8d, %edx
	leaq	4(%r12,%rbp,4), %rax
	leal	-1(%r10), %edi
	subl	%ebp, %edi
	imull	%r8d, %edi
	addl	%r8d, %r8d
	xorl	%ebp, %ebp
	movsd	.LCPI6_2(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB6_174:                              # =>This Inner Loop Header: Depth=1
	leal	(%rdx,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	-4(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, -4(%rax)
	leal	(%rdi,%rbp), %ebx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	mulsd	%xmm0, %xmm1
	movss	(%rax), %xmm2           # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	subsd	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	cvtsd2ss	%xmm2, %xmm1
	movss	%xmm1, (%rax)
	subl	%r8d, %ebp
	addq	$8, %rax
	addq	$-2, %rcx
	jne	.LBB6_174
.LBB6_175:                              # %.preheader599
	testl	%r15d, %r15d
	jle	.LBB6_182
# BB#176:                               # %.lr.ph612
	xorps	%xmm0, %xmm0
	cvtsi2sdl	offset(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%r15d, %xmm1
	incq	%r15
	movl	%r15d, %eax
	testb	$1, %r15b
	jne	.LBB6_177
# BB#178:
	movsd	.LCPI6_4(%rip), %xmm2   # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm2
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm2, %xmm3
	xorps	%xmm2, %xmm2
	cvtsd2ss	%xmm3, %xmm2
	movss	%xmm2, 4(%rsi)
	movl	$2, %ecx
	cmpq	$2, %rax
	jne	.LBB6_180
	jmp	.LBB6_182
.LBB6_177:
	movl	$1, %ecx
	cmpq	$2, %rax
	je	.LBB6_182
.LBB6_180:
	movsd	.LCPI6_2(%rip), %xmm2   # xmm2 = mem[0],zero
	.p2align	4, 0x90
.LBB6_181:                              # =>This Inner Loop Header: Depth=1
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%ecx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	(%rsi,%rcx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, (%rsi,%rcx,4)
	leal	1(%rcx), %edx
	xorps	%xmm3, %xmm3
	cvtsi2sdl	%edx, %xmm3
	mulsd	%xmm2, %xmm3
	movapd	%xmm1, %xmm4
	subsd	%xmm3, %xmm4
	mulsd	%xmm0, %xmm4
	movss	4(%rsi,%rcx,4), %xmm3   # xmm3 = mem[0],zero,zero,zero
	cvtss2sd	%xmm3, %xmm3
	subsd	%xmm4, %xmm3
	cvtsd2ss	%xmm3, %xmm3
	movss	%xmm3, 4(%rsi,%rcx,4)
	addq	$2, %rcx
	cmpq	%rax, %rcx
	jne	.LBB6_181
.LBB6_182:                              # %.loopexit
	movq	Q__align_gapmap.mseq1(%rip), %r15
	movq	Q__align_gapmap.mseq2(%rip), %r13
	movq	Q__align_gapmap.ijp(%rip), %rbx
	cmpb	$0, 144(%rsp)           # 1-byte Folded Reload
	je	.LBB6_320
# BB#183:
	movq	(%r9), %rdi
	callq	strlen
	movq	%rax, %rbp
	movq	104(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rdi
	callq	strlen
	movq	%rax, 128(%rsp)         # 8-byte Spill
	cmpl	$1, %r14d
	je	.LBB6_184
# BB#195:
	movq	112(%rsp), %rcx         # 8-byte Reload
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	testl	%ebp, %ebp
	jle	.LBB6_201
# BB#196:                               # %.lr.ph75.i
	movslq	%ebp, %rax
	movslq	128(%rsp), %r8          # 4-byte Folded Reload
	movl	%eax, %edx
	addq	$4, %rcx
	decq	%rdx
	movl	%ebp, %esi
	movaps	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB6_199
	jmp	.LBB6_198
	.p2align	4, 0x90
.LBB6_331:                              # %._crit_edge.i
                                        #   in Loop: Header=BB6_199 Depth=1
	movss	(%rcx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	addq	$4, %rcx
	decq	%rdx
	decl	%esi
	ucomiss	%xmm1, %xmm0
	jb	.LBB6_199
.LBB6_198:
	movq	(%rbx,%rax,8), %rdi
	movl	%esi, (%rdi,%r8,4)
	movaps	%xmm0, %xmm1
.LBB6_199:                              # =>This Inner Loop Header: Depth=1
	testq	%rdx, %rdx
	jne	.LBB6_331
# BB#200:
	movaps	%xmm1, %xmm0
.LBB6_201:                              # %.preheader16.i
	cmpl	$0, 128(%rsp)           # 4-byte Folded Reload
	jle	.LBB6_184
# BB#202:                               # %.lr.ph71.i
	movslq	%ebp, %r8
	movq	128(%rsp), %rax         # 8-byte Reload
	movslq	%eax, %r9
	movl	%r9d, %edx
	testb	$1, %al
	jne	.LBB6_204
# BB#203:
	xorl	%esi, %esi
	cmpq	$1, %rdx
	jne	.LBB6_208
	jmp	.LBB6_184
.LBB6_320:
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, 16(%rsp)
	movl	60(%rsp), %eax          # 4-byte Reload
	movl	%eax, 8(%rsp)
	movq	%rbx, (%rsp)
	movq	%r12, %rdi
	movq	%r9, %rdx
	movq	104(%rsp), %rcx         # 8-byte Reload
	movq	%r15, %r8
	movq	%r13, %r9
	callq	Atracking
	movq	Q__align_gapmap.mseq1(%rip), %r15
	jmp	.LBB6_321
.LBB6_204:
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB6_206
# BB#205:
	movl	$1, %esi
	cmpq	$1, %rdx
	jne	.LBB6_208
	jmp	.LBB6_184
.LBB6_206:
	movq	128(%rsp), %rax         # 8-byte Reload
	movl	%eax, %esi
	negl	%esi
	movq	(%rbx,%r8,8), %rdi
	movl	%esi, (%rdi,%r9,4)
	movl	$1, %esi
	movaps	%xmm1, %xmm0
	cmpq	$1, %rdx
	je	.LBB6_184
.LBB6_208:                              # %.lr.ph71.i.new
	movq	128(%rsp), %rdi         # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill> %RDI<def>
	negl	%edi
	.p2align	4, 0x90
.LBB6_209:                              # =>This Inner Loop Header: Depth=1
	movss	(%r12,%rsi,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB6_211
# BB#210:                               #   in Loop: Header=BB6_209 Depth=1
	leal	(%rdi,%rsi), %eax
	movq	(%rbx,%r8,8), %rcx
	movl	%eax, (%rcx,%r9,4)
	movaps	%xmm1, %xmm0
.LBB6_211:                              #   in Loop: Header=BB6_209 Depth=1
	movss	4(%r12,%rsi,4), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB6_213
# BB#212:                               #   in Loop: Header=BB6_209 Depth=1
	leal	1(%rdi,%rsi), %eax
	movq	(%rbx,%r8,8), %rcx
	movl	%eax, (%rcx,%r9,4)
	movaps	%xmm1, %xmm0
.LBB6_213:                              #   in Loop: Header=BB6_209 Depth=1
	addq	$2, %rsi
	cmpq	%rsi, %rdx
	jne	.LBB6_209
.LBB6_184:                              # %.preheader15.i
	testl	%ebp, %ebp
	movq	104(%rsp), %r10         # 8-byte Reload
	movl	44(%rsp), %r14d         # 4-byte Reload
	movl	60(%rsp), %r11d         # 4-byte Reload
	movq	128(%rsp), %r12         # 8-byte Reload
	js	.LBB6_190
# BB#185:                               # %.lr.ph68.preheader.i
	movq	%rbp, %rsi
	incq	%rsi
	movl	%esi, %eax
	leaq	-1(%rax), %rcx
	xorl	%edx, %edx
	andq	$7, %rsi
	je	.LBB6_187
	.p2align	4, 0x90
.LBB6_186:                              # %.lr.ph68.i.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rdx,8), %rdi
	incq	%rdx
	movl	%edx, (%rdi)
	cmpq	%rdx, %rsi
	jne	.LBB6_186
.LBB6_187:                              # %.lr.ph68.i.prol.loopexit
	cmpq	$7, %rcx
	jb	.LBB6_190
# BB#188:                               # %.lr.ph68.preheader.i.new
	negq	%rax
	leaq	4(%rdx,%rax), %r8
	leaq	56(%rbx,%rdx,8), %rcx
	leaq	4(%rdx), %rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB6_189:                              # %.lr.ph68.i
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rcx,%rsi,8), %rdi
	leal	(%rdx,%rsi), %r9d
	leal	-3(%rdx,%rsi), %eax
	movl	%eax, (%rdi)
	movq	-48(%rcx,%rsi,8), %rax
	leal	-2(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-40(%rcx,%rsi,8), %rax
	leal	-1(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-32(%rcx,%rsi,8), %rax
	movl	%r9d, (%rax)
	movq	-24(%rcx,%rsi,8), %rax
	leal	1(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-16(%rcx,%rsi,8), %rax
	leal	2(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	-8(%rcx,%rsi,8), %rax
	leal	3(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	movq	(%rcx,%rsi,8), %rax
	leal	4(%rdx,%rsi), %edi
	movl	%edi, (%rax)
	leaq	8(%r8,%rsi), %rax
	addq	$8, %rsi
	cmpq	$4, %rax
	jne	.LBB6_189
.LBB6_190:                              # %.preheader14.i
	testl	%r12d, %r12d
	js	.LBB6_218
# BB#191:                               # %.lr.ph66.i
	movq	(%rbx), %r8
	movq	%r12, %rsi
	incq	%rsi
	movl	%esi, %eax
	cmpq	$7, %rax
	jbe	.LBB6_192
# BB#214:                               # %min.iters.checked868
	andl	$7, %esi
	movq	%rax, %rcx
	subq	%rsi, %rcx
	je	.LBB6_192
# BB#215:                               # %vector.body864.preheader
	leaq	16(%r8), %rdi
	movdqa	.LCPI6_6(%rip), %xmm0   # xmm0 = [0,1,2,3]
	movdqa	.LCPI6_7(%rip), %xmm1   # xmm1 = [4,4,4,4]
	pcmpeqd	%xmm2, %xmm2
	movdqa	.LCPI6_8(%rip), %xmm3   # xmm3 = [8,8,8,8]
	movq	%rcx, %rdx
	.p2align	4, 0x90
.LBB6_216:                              # %vector.body864
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm4
	paddd	%xmm1, %xmm4
	movdqa	%xmm0, %xmm5
	pxor	%xmm2, %xmm5
	pxor	%xmm2, %xmm4
	movdqu	%xmm5, -16(%rdi)
	movdqu	%xmm4, (%rdi)
	paddd	%xmm3, %xmm0
	addq	$32, %rdi
	addq	$-8, %rdx
	jne	.LBB6_216
# BB#217:                               # %middle.block865
	testq	%rsi, %rsi
	jne	.LBB6_193
	jmp	.LBB6_218
.LBB6_192:
	xorl	%ecx, %ecx
.LBB6_193:                              # %scalar.ph866.preheader
	movl	%ecx, %edx
	notl	%edx
	leaq	(%r8,%rcx,4), %rsi
	subq	%rcx, %rax
	.p2align	4, 0x90
.LBB6_194:                              # %scalar.ph866
                                        # =>This Inner Loop Header: Depth=1
	movl	%edx, (%rsi)
	decl	%edx
	addq	$4, %rsi
	decq	%rax
	jne	.LBB6_194
.LBB6_218:                              # %.preheader13.i
	testl	%r11d, %r11d
	jle	.LBB6_224
# BB#219:                               # %.lr.ph64.i
	leal	(%r12,%rbp), %eax
	cltq
	movl	%r11d, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %r9
	xorl	%esi, %esi
	andq	$3, %r9
	je	.LBB6_221
	.p2align	4, 0x90
.LBB6_220:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rsi,8), %rdx
	leaq	(%rdx,%rax), %rdi
	movq	%rdi, (%r15,%rsi,8)
	movb	$0, (%rdx,%rax)
	incq	%rsi
	cmpq	%rsi, %r9
	jne	.LBB6_220
.LBB6_221:                              # %.prol.loopexit936
	cmpq	$3, %r8
	jb	.LBB6_224
# BB#222:                               # %.lr.ph64.i.new
	subq	%rsi, %rcx
	leaq	24(%r15,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB6_223:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB6_223
.LBB6_224:                              # %.preheader12.i
	testl	%r14d, %r14d
	jle	.LBB6_230
# BB#225:                               # %.lr.ph61.i
	leal	(%r12,%rbp), %eax
	cltq
	movl	%r14d, %ecx
	leaq	-1(%rcx), %r8
	movq	%rcx, %r9
	xorl	%esi, %esi
	andq	$3, %r9
	je	.LBB6_227
	.p2align	4, 0x90
.LBB6_226:                              # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rsi,8), %rdx
	leaq	(%rdx,%rax), %rdi
	movq	%rdi, (%r13,%rsi,8)
	movb	$0, (%rdx,%rax)
	incq	%rsi
	cmpq	%rsi, %r9
	jne	.LBB6_226
.LBB6_227:                              # %.prol.loopexit931
	cmpq	$3, %r8
	jb	.LBB6_230
# BB#228:                               # %.lr.ph61.i.new
	subq	%rsi, %rcx
	leaq	24(%r13,%rsi,8), %rdx
	.p2align	4, 0x90
.LBB6_229:                              # =>This Inner Loop Header: Depth=1
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -24(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -16(%rdx)
	movb	$0, (%rsi,%rax)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, -8(%rdx)
	movb	$0, (%rsi,%rax)
	movq	(%rdx), %rsi
	leaq	(%rsi,%rax), %rdi
	movq	%rdi, (%rdx)
	movb	$0, (%rsi,%rax)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB6_229
.LBB6_230:                              # %._crit_edge62.i
	movq	496(%rsp), %rax
	movl	$0, (%rax)
	movl	%r12d, %eax
	addl	%ebp, %eax
	movl	%eax, 232(%rsp)         # 4-byte Spill
	js	.LBB6_321
# BB#231:                               # %.lr.ph57.i
	movq	%rbx, 224(%rsp)         # 8-byte Spill
	movq	impmtx(%rip), %rax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	movl	%r11d, %r11d
	movl	%r14d, %eax
	leaq	-1(%rax), %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	leaq	-1(%r11), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	%r11, 136(%rsp)         # 8-byte Spill
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	andl	$3, %r11d
	movq	%r12, %rcx
	movq	%rax, 176(%rsp)         # 8-byte Spill
	movl	%eax, %r12d
	andl	$3, %r12d
	leaq	24(%r13), %rax
	movq	%rax, 168(%rsp)         # 8-byte Spill
	leaq	24(%r15), %rax
	movq	%rax, 160(%rsp)         # 8-byte Spill
	movq	64(%rsp), %r8           # 8-byte Reload
	leaq	24(%r8), %rax
	movq	%rax, 200(%rsp)         # 8-byte Spill
	leaq	24(%r10), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %rax
	movq	%rax, 216(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%ecx, 84(%rsp)          # 4-byte Spill
	jmp	.LBB6_232
.LBB6_289:                              # %.preheader7.preheader.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	%edx, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jmp	.LBB6_298
	.p2align	4, 0x90
.LBB6_232:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_260 Depth 2
                                        #       Child Loop BB6_261 Depth 3
                                        #       Child Loop BB6_264 Depth 3
                                        #     Child Loop BB6_252 Depth 2
                                        #       Child Loop BB6_253 Depth 3
                                        #       Child Loop BB6_256 Depth 3
                                        #     Child Loop BB6_241 Depth 2
                                        #       Child Loop BB6_242 Depth 3
                                        #       Child Loop BB6_245 Depth 3
                                        #       Child Loop BB6_247 Depth 3
                                        #       Child Loop BB6_250 Depth 3
                                        #     Child Loop BB6_291 Depth 2
                                        #       Child Loop BB6_292 Depth 3
                                        #       Child Loop BB6_295 Depth 3
                                        #     Child Loop BB6_282 Depth 2
                                        #       Child Loop BB6_283 Depth 3
                                        #       Child Loop BB6_286 Depth 3
                                        #     Child Loop BB6_271 Depth 2
                                        #       Child Loop BB6_272 Depth 3
                                        #       Child Loop BB6_275 Depth 3
                                        #       Child Loop BB6_277 Depth 3
                                        #       Child Loop BB6_280 Depth 3
                                        #     Child Loop BB6_307 Depth 2
                                        #     Child Loop BB6_310 Depth 2
                                        #     Child Loop BB6_315 Depth 2
                                        #     Child Loop BB6_318 Depth 2
	movl	84(%rsp), %ecx          # 4-byte Reload
	movl	%ecx, %edx
	movl	%eax, %esi
	movslq	%esi, %rcx
	movq	224(%rsp), %rax         # 8-byte Reload
	movq	%rcx, 240(%rsp)         # 8-byte Spill
	movq	(%rax,%rcx,8), %rax
	movq	%rdx, %rcx
	movq	%rcx, 144(%rsp)         # 8-byte Spill
	movslq	%edx, %rcx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movl	(%rax,%rcx,4), %edx
	testl	%edx, %edx
	movq	%rsi, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	js	.LBB6_233
# BB#234:                               #   in Loop: Header=BB6_232 Depth=1
	je	.LBB6_236
# BB#235:                               #   in Loop: Header=BB6_232 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%edx, %ecx
	jmp	.LBB6_237
	.p2align	4, 0x90
.LBB6_233:                              #   in Loop: Header=BB6_232 Depth=1
	leal	-1(%rsi), %ecx
	movq	%rsi, %rax
	jmp	.LBB6_238
	.p2align	4, 0x90
.LBB6_236:                              #   in Loop: Header=BB6_232 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %ecx
.LBB6_237:                              #   in Loop: Header=BB6_232 Depth=1
	movl	$-1, %edx
.LBB6_238:                              #   in Loop: Header=BB6_232 Depth=1
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	%ecx, 112(%rsp)         # 4-byte Spill
	subl	%ecx, %eax
	decl	%eax
	je	.LBB6_267
# BB#239:                               # %.preheader9.lr.ph.i
                                        #   in Loop: Header=BB6_232 Depth=1
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	jle	.LBB6_259
# BB#240:                               # %.preheader9.us.preheader.i
                                        #   in Loop: Header=BB6_232 Depth=1
	testl	%r14d, %r14d
	movslq	%eax, %r10
	movslq	112(%rsp), %r14         # 4-byte Folded Reload
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	jle	.LBB6_252
	.p2align	4, 0x90
.LBB6_241:                              # %.preheader9.us.i.us
                                        #   Parent Loop BB6_232 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_242 Depth 3
                                        #       Child Loop BB6_245 Depth 3
                                        #       Child Loop BB6_247 Depth 3
                                        #       Child Loop BB6_250 Depth 3
	leaq	(%r10,%r14), %r9
	xorl	%eax, %eax
	testq	%r11, %r11
	je	.LBB6_243
	.p2align	4, 0x90
.LBB6_242:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_241 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rax,8), %rdx
	movzbl	(%rdx,%r9), %edx
	movq	(%r15,%rax,8), %rsi
	leaq	-1(%rsi), %rdi
	movq	%rdi, (%r15,%rax,8)
	movb	%dl, -1(%rsi)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB6_242
.LBB6_243:                              # %.prol.loopexit893
                                        #   in Loop: Header=BB6_241 Depth=2
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_246
# BB#244:                               # %.preheader9.us.i.us.new
                                        #   in Loop: Header=BB6_241 Depth=2
	movq	136(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	200(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB6_245:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_241 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rax), %rdi
	movzbl	(%rdi,%r9), %ecx
	movq	-24(%rsi), %rdi
	leaq	-1(%rdi), %r8
	movq	%r8, -24(%rsi)
	movb	%cl, -1(%rdi)
	movq	-16(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-16(%rsi), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, -16(%rsi)
	movb	%cl, -1(%rdi)
	movq	-8(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-8(%rsi), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, -8(%rsi)
	movb	%cl, -1(%rdi)
	movq	(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	(%rsi), %rdi
	leaq	-1(%rdi), %rbx
	movq	%rbx, (%rsi)
	movb	%cl, -1(%rdi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB6_245
.LBB6_246:                              # %.lr.ph19.us.i.us.preheader
                                        #   in Loop: Header=BB6_241 Depth=2
	xorl	%edx, %edx
	testq	%r12, %r12
	je	.LBB6_248
	.p2align	4, 0x90
.LBB6_247:                              # %.lr.ph19.us.i.us.prol
                                        #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_241 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%rdx,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r13,%rdx,8)
	movb	$45, -1(%rax)
	incq	%rdx
	cmpq	%rdx, %r12
	jne	.LBB6_247
.LBB6_248:                              # %.lr.ph19.us.i.us.prol.loopexit
                                        #   in Loop: Header=BB6_241 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_251
# BB#249:                               # %.lr.ph19.us.i.us.preheader.new
                                        #   in Loop: Header=BB6_241 Depth=2
	movq	176(%rsp), %rax         # 8-byte Reload
	subq	%rdx, %rax
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB6_250:                              # %.lr.ph19.us.i.us
                                        #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_241 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movb	$45, -1(%rcx)
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movb	$45, -1(%rcx)
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movb	$45, -1(%rcx)
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%rdx)
	movb	$45, -1(%rcx)
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB6_250
.LBB6_251:                              # %._crit_edge.us.i.loopexit.us
                                        #   in Loop: Header=BB6_241 Depth=2
	decq	%r10
	testl	%r10d, %r10d
	movq	64(%rsp), %r8           # 8-byte Reload
	jne	.LBB6_241
	jmp	.LBB6_258
	.p2align	4, 0x90
.LBB6_252:                              # %.preheader9.us.i
                                        #   Parent Loop BB6_232 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_253 Depth 3
                                        #       Child Loop BB6_256 Depth 3
	leaq	(%r10,%r14), %rdx
	xorl	%edi, %edi
	testq	%r11, %r11
	je	.LBB6_254
	.p2align	4, 0x90
.LBB6_253:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_252 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r8,%rdi,8), %rax
	movzbl	(%rax,%rdx), %eax
	movq	(%r15,%rdi,8), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r15,%rdi,8)
	movb	%al, -1(%rcx)
	incq	%rdi
	cmpq	%rdi, %r11
	jne	.LBB6_253
.LBB6_254:                              # %.prol.loopexit888
                                        #   in Loop: Header=BB6_252 Depth=2
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_257
# BB#255:                               # %.preheader9.us.i.new
                                        #   in Loop: Header=BB6_252 Depth=2
	movq	136(%rsp), %rsi         # 8-byte Reload
	subq	%rdi, %rsi
	movq	160(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	movq	200(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB6_256:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_252 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-24(%rax), %rbx
	leaq	-1(%rbx), %rbp
	movq	%rbp, -24(%rax)
	movb	%cl, -1(%rbx)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-16(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -16(%rax)
	movb	%cl, -1(%rbp)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-8(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -8(%rax)
	movb	%cl, -1(%rbp)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, (%rax)
	movb	%cl, -1(%rbp)
	addq	$32, %rax
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB6_256
.LBB6_257:                              # %..preheader8_crit_edge.us.i
                                        #   in Loop: Header=BB6_252 Depth=2
	decq	%r10
	testl	%r10d, %r10d
	jne	.LBB6_252
.LBB6_258:                              # %._crit_edge21.loopexit.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	movq	48(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rax,%rcx), %eax
	subl	112(%rsp), %eax         # 4-byte Folded Reload
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	104(%rsp), %r10         # 8-byte Reload
	movl	44(%rsp), %r14d         # 4-byte Reload
	movq	152(%rsp), %rdx         # 8-byte Reload
	jmp	.LBB6_267
	.p2align	4, 0x90
.LBB6_259:                              # %.preheader9.lr.ph.split.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	%rdx, %rbp
	movq	88(%rsp), %rcx          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	leal	-1(%rcx,%rdx), %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	testl	%r14d, %r14d
	jle	.LBB6_266
	.p2align	4, 0x90
.LBB6_260:                              # %.preheader9.us22.i
                                        #   Parent Loop BB6_232 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_261 Depth 3
                                        #       Child Loop BB6_264 Depth 3
	xorl	%esi, %esi
	testq	%r12, %r12
	je	.LBB6_262
	.p2align	4, 0x90
.LBB6_261:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_260 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r13,%rsi,8), %rcx
	leaq	-1(%rcx), %rdx
	movq	%rdx, (%r13,%rsi,8)
	movb	$45, -1(%rcx)
	incq	%rsi
	cmpq	%rsi, %r12
	jne	.LBB6_261
.LBB6_262:                              # %.prol.loopexit
                                        #   in Loop: Header=BB6_260 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_265
# BB#263:                               # %.preheader9.us22.i.new
                                        #   in Loop: Header=BB6_260 Depth=2
	movq	176(%rsp), %rdx         # 8-byte Reload
	subq	%rsi, %rdx
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rsi,8), %rsi
	.p2align	4, 0x90
.LBB6_264:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_260 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -24(%rsi)
	movb	$45, -1(%rcx)
	movq	-16(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -16(%rsi)
	movb	$45, -1(%rcx)
	movq	-8(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, -8(%rsi)
	movb	$45, -1(%rcx)
	movq	(%rsi), %rcx
	leaq	-1(%rcx), %rdi
	movq	%rdi, (%rsi)
	movb	$45, -1(%rcx)
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB6_264
.LBB6_265:                              # %._crit_edge.us30.i
                                        #   in Loop: Header=BB6_260 Depth=2
	decl	%eax
	jne	.LBB6_260
.LBB6_266:                              # %._crit_edge21.loopexit79.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	subl	112(%rsp), %eax         # 4-byte Folded Reload
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movq	%rbp, %rdx
.LBB6_267:                              # %._crit_edge21.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	leal	(%rdx,%rax), %eax
	movl	%eax, 84(%rsp)          # 4-byte Spill
	cmpl	$-1, %edx
	je	.LBB6_298
# BB#268:                               # %.preheader7.lr.ph.i
                                        #   in Loop: Header=BB6_232 Depth=1
	notl	%edx
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	jle	.LBB6_288
# BB#269:                               # %.preheader7.us.preheader.i
                                        #   in Loop: Header=BB6_232 Depth=1
	testl	%r14d, %r14d
	movslq	%edx, %r14
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	jle	.LBB6_282
# BB#270:                               # %.preheader7.us.i.us.preheader
                                        #   in Loop: Header=BB6_232 Depth=1
	movslq	84(%rsp), %r9           # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB6_271:                              # %.preheader7.us.i.us
                                        #   Parent Loop BB6_232 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_272 Depth 3
                                        #       Child Loop BB6_275 Depth 3
                                        #       Child Loop BB6_277 Depth 3
                                        #       Child Loop BB6_280 Depth 3
	xorl	%edx, %edx
	testq	%r11, %r11
	je	.LBB6_273
	.p2align	4, 0x90
.LBB6_272:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_271 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rdx,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r15,%rdx,8)
	movb	$45, -1(%rax)
	incq	%rdx
	cmpq	%rdx, %r11
	jne	.LBB6_272
.LBB6_273:                              # %.prol.loopexit911
                                        #   in Loop: Header=BB6_271 Depth=2
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_276
# BB#274:                               # %.preheader7.us.i.us.new
                                        #   in Loop: Header=BB6_271 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	subq	%rdx, %rax
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB6_275:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_271 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movb	$45, -1(%rcx)
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movb	$45, -1(%rcx)
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movb	$45, -1(%rcx)
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%rdx)
	movb	$45, -1(%rcx)
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB6_275
.LBB6_276:                              # %.lr.ph34.us.i.us
                                        #   in Loop: Header=BB6_271 Depth=2
	leaq	(%r14,%r9), %rdx
	xorl	%edi, %edi
	testq	%r12, %r12
	je	.LBB6_278
	.p2align	4, 0x90
.LBB6_277:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_271 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r10,%rdi,8), %rax
	movzbl	(%rax,%rdx), %eax
	movq	(%r13,%rdi,8), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r13,%rdi,8)
	movb	%al, -1(%rcx)
	incq	%rdi
	cmpq	%rdi, %r12
	jne	.LBB6_277
.LBB6_278:                              # %.prol.loopexit916
                                        #   in Loop: Header=BB6_271 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_281
# BB#279:                               # %.lr.ph34.us.i.us.new
                                        #   in Loop: Header=BB6_271 Depth=2
	movq	176(%rsp), %rsi         # 8-byte Reload
	subq	%rdi, %rsi
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB6_280:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_271 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-24(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -24(%rax)
	movb	%cl, -1(%rbp)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-16(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -16(%rax)
	movb	%cl, -1(%rbp)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-8(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -8(%rax)
	movb	%cl, -1(%rbp)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, (%rax)
	movb	%cl, -1(%rbp)
	addq	$32, %rax
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB6_280
.LBB6_281:                              # %._crit_edge35.us.i.loopexit.us
                                        #   in Loop: Header=BB6_271 Depth=2
	decq	%r14
	testl	%r14d, %r14d
	jne	.LBB6_271
	jmp	.LBB6_297
	.p2align	4, 0x90
.LBB6_282:                              # %.preheader7.us.i
                                        #   Parent Loop BB6_232 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_283 Depth 3
                                        #       Child Loop BB6_286 Depth 3
	xorl	%edx, %edx
	testq	%r11, %r11
	je	.LBB6_284
	.p2align	4, 0x90
.LBB6_283:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_282 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r15,%rdx,8), %rax
	leaq	-1(%rax), %rcx
	movq	%rcx, (%r15,%rdx,8)
	movb	$45, -1(%rax)
	incq	%rdx
	cmpq	%rdx, %r11
	jne	.LBB6_283
.LBB6_284:                              # %.prol.loopexit906
                                        #   in Loop: Header=BB6_282 Depth=2
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_287
# BB#285:                               # %.preheader7.us.i.new
                                        #   in Loop: Header=BB6_282 Depth=2
	movq	136(%rsp), %rax         # 8-byte Reload
	subq	%rdx, %rax
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB6_286:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_282 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -24(%rdx)
	movb	$45, -1(%rcx)
	movq	-16(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -16(%rdx)
	movb	$45, -1(%rcx)
	movq	-8(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, -8(%rdx)
	movb	$45, -1(%rcx)
	movq	(%rdx), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%rdx)
	movb	$45, -1(%rcx)
	addq	$32, %rdx
	addq	$-4, %rax
	jne	.LBB6_286
.LBB6_287:                              # %..preheader_crit_edge.us.i
                                        #   in Loop: Header=BB6_282 Depth=2
	decq	%r14
	testl	%r14d, %r14d
	jne	.LBB6_282
	jmp	.LBB6_297
	.p2align	4, 0x90
.LBB6_288:                              # %.preheader7.lr.ph.split.i
                                        #   in Loop: Header=BB6_232 Depth=1
	testl	%r14d, %r14d
	jle	.LBB6_289
# BB#290:                               # %.preheader7.us39.preheader.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	movslq	%edx, %r14
	movslq	84(%rsp), %r9           # 4-byte Folded Reload
	.p2align	4, 0x90
.LBB6_291:                              # %.preheader7.us39.i
                                        #   Parent Loop BB6_232 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_292 Depth 3
                                        #       Child Loop BB6_295 Depth 3
	leaq	(%r14,%r9), %rdx
	xorl	%edi, %edi
	testq	%r12, %r12
	je	.LBB6_293
	.p2align	4, 0x90
.LBB6_292:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_291 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	(%r10,%rdi,8), %rax
	movzbl	(%rax,%rdx), %eax
	movq	(%r13,%rdi,8), %rcx
	leaq	-1(%rcx), %rsi
	movq	%rsi, (%r13,%rdi,8)
	movb	%al, -1(%rcx)
	incq	%rdi
	cmpq	%rdi, %r12
	jne	.LBB6_292
.LBB6_293:                              # %.prol.loopexit901
                                        #   in Loop: Header=BB6_291 Depth=2
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_296
# BB#294:                               # %.preheader7.us39.i.new
                                        #   in Loop: Header=BB6_291 Depth=2
	movq	176(%rsp), %rsi         # 8-byte Reload
	subq	%rdi, %rsi
	movq	168(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rdi,8), %rax
	movq	192(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB6_295:                              #   Parent Loop BB6_232 Depth=1
                                        #     Parent Loop BB6_291 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	-24(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-24(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -24(%rax)
	movb	%cl, -1(%rbp)
	movq	-16(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-16(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -16(%rax)
	movb	%cl, -1(%rbp)
	movq	-8(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	-8(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, -8(%rax)
	movb	%cl, -1(%rbp)
	movq	(%rdi), %rcx
	movzbl	(%rcx,%rdx), %ecx
	movq	(%rax), %rbp
	leaq	-1(%rbp), %rbx
	movq	%rbx, (%rax)
	movb	%cl, -1(%rbp)
	addq	$32, %rax
	addq	$32, %rdi
	addq	$-4, %rsi
	jne	.LBB6_295
.LBB6_296:                              # %._crit_edge35.us47.i
                                        #   in Loop: Header=BB6_291 Depth=2
	decq	%r14
	testl	%r14d, %r14d
	jne	.LBB6_291
.LBB6_297:                              # %._crit_edge37.loopexit77.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	152(%rsp), %eax         # 4-byte Folded Reload
	movq	%rax, 88(%rsp)          # 8-byte Spill
	movl	44(%rsp), %r14d         # 4-byte Reload
.LBB6_298:                              # %._crit_edge37.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	48(%rsp), %rax          # 8-byte Reload
	cmpl	216(%rsp), %eax         # 4-byte Folded Reload
	je	.LBB6_301
# BB#299:                               # %._crit_edge37.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	144(%rsp), %rax         # 8-byte Reload
	cmpl	128(%rsp), %eax         # 4-byte Folded Reload
	je	.LBB6_301
# BB#300:                               #   in Loop: Header=BB6_232 Depth=1
	movq	536(%rsp), %rax
	movq	240(%rsp), %rcx         # 8-byte Reload
	movslq	(%rax,%rcx,4), %rax
	movq	208(%rsp), %rcx         # 8-byte Reload
	movq	(%rcx,%rax,8), %rax
	movq	544(%rsp), %rcx
	movq	120(%rsp), %rdx         # 8-byte Reload
	movslq	(%rcx,%rdx,4), %rcx
	movss	(%rax,%rcx,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	496(%rsp), %rax
	addss	(%rax), %xmm0
	movss	%xmm0, (%rax)
.LBB6_301:                              #   in Loop: Header=BB6_232 Depth=1
	cmpl	$0, 48(%rsp)            # 4-byte Folded Reload
	jle	.LBB6_321
# BB#302:                               #   in Loop: Header=BB6_232 Depth=1
	cmpl	$0, 144(%rsp)           # 4-byte Folded Reload
	jle	.LBB6_321
# BB#303:                               # %.preheader11.i
                                        #   in Loop: Header=BB6_232 Depth=1
	cmpl	$0, 60(%rsp)            # 4-byte Folded Reload
	jle	.LBB6_311
# BB#304:                               # %.lr.ph49.i
                                        #   in Loop: Header=BB6_232 Depth=1
	testq	%r11, %r11
	movslq	112(%rsp), %r9          # 4-byte Folded Reload
	je	.LBB6_305
# BB#306:                               # %.prol.preheader920
                                        #   in Loop: Header=BB6_232 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_307:                              #   Parent Loop BB6_232 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rax,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	(%r15,%rax,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r15,%rax,8)
	movb	%cl, -1(%rdx)
	incq	%rax
	cmpq	%rax, %r11
	jne	.LBB6_307
	jmp	.LBB6_308
	.p2align	4, 0x90
.LBB6_305:                              #   in Loop: Header=BB6_232 Depth=1
	xorl	%eax, %eax
.LBB6_308:                              # %.prol.loopexit921
                                        #   in Loop: Header=BB6_232 Depth=1
	cmpq	$3, 72(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_311
# BB#309:                               # %.lr.ph49.i.new
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	160(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	200(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB6_310:                              #   Parent Loop BB6_232 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-24(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -24(%rsi)
	movb	%cl, -1(%rdi)
	movq	-16(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-16(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -16(%rsi)
	movb	%cl, -1(%rdi)
	movq	-8(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-8(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -8(%rsi)
	movb	%cl, -1(%rdi)
	movq	(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%rsi)
	movb	%cl, -1(%rdi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB6_310
.LBB6_311:                              # %.preheader10.i
                                        #   in Loop: Header=BB6_232 Depth=1
	testl	%r14d, %r14d
	jle	.LBB6_319
# BB#312:                               # %.lr.ph51.i
                                        #   in Loop: Header=BB6_232 Depth=1
	testq	%r12, %r12
	movslq	84(%rsp), %r9           # 4-byte Folded Reload
	je	.LBB6_313
# BB#314:                               # %.prol.preheader925
                                        #   in Loop: Header=BB6_232 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_315:                              #   Parent Loop BB6_232 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r10,%rax,8), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	(%r13,%rax,8), %rdx
	leaq	-1(%rdx), %rsi
	movq	%rsi, (%r13,%rax,8)
	movb	%cl, -1(%rdx)
	incq	%rax
	cmpq	%rax, %r12
	jne	.LBB6_315
	jmp	.LBB6_316
	.p2align	4, 0x90
.LBB6_313:                              #   in Loop: Header=BB6_232 Depth=1
	xorl	%eax, %eax
.LBB6_316:                              # %.prol.loopexit926
                                        #   in Loop: Header=BB6_232 Depth=1
	cmpq	$3, 32(%rsp)            # 8-byte Folded Reload
	jb	.LBB6_319
# BB#317:                               # %.lr.ph51.i.new
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	176(%rsp), %rdx         # 8-byte Reload
	subq	%rax, %rdx
	movq	168(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rsi
	movq	192(%rsp), %rcx         # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	.p2align	4, 0x90
.LBB6_318:                              #   Parent Loop BB6_232 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	-24(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-24(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -24(%rsi)
	movb	%cl, -1(%rdi)
	movq	-16(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-16(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -16(%rsi)
	movb	%cl, -1(%rdi)
	movq	-8(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	-8(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, -8(%rsi)
	movb	%cl, -1(%rdi)
	movq	(%rax), %rcx
	movzbl	(%rcx,%r9), %ecx
	movq	(%rsi), %rdi
	leaq	-1(%rdi), %rbp
	movq	%rbp, (%rsi)
	movb	%cl, -1(%rdi)
	addq	$32, %rsi
	addq	$32, %rax
	addq	$-4, %rdx
	jne	.LBB6_318
.LBB6_319:                              # %._crit_edge52.i
                                        #   in Loop: Header=BB6_232 Depth=1
	movq	88(%rsp), %rax          # 8-byte Reload
	addl	$2, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	cmpl	232(%rsp), %eax         # 4-byte Folded Reload
	movl	112(%rsp), %eax         # 4-byte Reload
	jle	.LBB6_232
.LBB6_321:                              # %Atracking_localhom_gapmap.exit
	movl	480(%rsp), %ebx
	movq	(%r15), %rdi
	callq	strlen
	movq	%rax, %rcx
	cmpl	%ebx, %ecx
	jg	.LBB6_323
# BB#322:                               # %Atracking_localhom_gapmap.exit
	cmpl	$5000001, %ecx          # imm = 0x4C4B41
	jge	.LBB6_323
.LBB6_324:                              # %.preheader598
	movl	60(%rsp), %eax          # 4-byte Reload
	testl	%eax, %eax
	movq	64(%rsp), %r15          # 8-byte Reload
	jle	.LBB6_327
# BB#325:                               # %.lr.ph610.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_326:                              # %.lr.ph610
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rbp,8), %rdi
	movq	Q__align_gapmap.mseq1(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_326
.LBB6_327:                              # %.preheader
	testl	%r14d, %r14d
	movl	%r14d, %eax
	movq	104(%rsp), %r14         # 8-byte Reload
	jle	.LBB6_330
# BB#328:                               # %.lr.ph.preheader
	movl	%eax, %ebx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_329:                              # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbp,8), %rdi
	movq	Q__align_gapmap.mseq2(%rip), %rax
	movq	(%rax,%rbp,8), %rsi
	callq	strcpy
	incq	%rbp
	cmpq	%rbp, %rbx
	jne	.LBB6_329
.LBB6_330:                              # %._crit_edge
	movss	184(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addq	$424, %rsp              # imm = 0x1A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_323:
	movq	stderr(%rip), %rdi
	movl	$.L.str, %esi
	movl	$5000000, %r8d          # imm = 0x4C4B40
	xorl	%eax, %eax
	movl	%ebx, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	fprintf
	movl	$.L.str.1, %edi
	callq	ErrorExit
	jmp	.LBB6_324
.LBB6_84:
	movq	64(%rsp), %r9           # 8-byte Reload
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	jmp	.LBB6_77
.LBB6_86:
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	72(%rsp), %r13          # 8-byte Reload
	jmp	.LBB6_77
.LBB6_102:
	movq	64(%rsp), %r9           # 8-byte Reload
.LBB6_104:
	movq	96(%rsp), %r15          # 8-byte Reload
	movq	32(%rsp), %r8           # 8-byte Reload
	movabsq	$17179869180, %r12      # imm = 0x3FFFFFFFC
	movq	72(%rsp), %r14          # 8-byte Reload
	jmp	.LBB6_95
.Lfunc_end6:
	.size	Q__align_gapmap, .Lfunc_end6-Q__align_gapmap
	.cfi_endproc

	.type	impmtx,@object          # @impmtx
	.local	impmtx
	.comm	impmtx,8,8
	.type	imp_match_init_strictQ.nocount1,@object # @imp_match_init_strictQ.nocount1
	.local	imp_match_init_strictQ.nocount1
	.comm	imp_match_init_strictQ.nocount1,8,8
	.type	imp_match_init_strictQ.nocount2,@object # @imp_match_init_strictQ.nocount2
	.local	imp_match_init_strictQ.nocount2
	.comm	imp_match_init_strictQ.nocount2,8,8
	.type	impalloclen,@object     # @impalloclen
	.local	impalloclen
	.comm	impalloclen,4,4
	.type	Q__align.mi,@object     # @Q__align.mi
	.local	Q__align.mi
	.comm	Q__align.mi,4,4
	.type	Q__align.m,@object      # @Q__align.m
	.local	Q__align.m
	.comm	Q__align.m,8,8
	.type	Q__align.ijp,@object    # @Q__align.ijp
	.local	Q__align.ijp
	.comm	Q__align.ijp,8,8
	.type	Q__align.mpi,@object    # @Q__align.mpi
	.local	Q__align.mpi
	.comm	Q__align.mpi,4,4
	.type	Q__align.mp,@object     # @Q__align.mp
	.local	Q__align.mp
	.comm	Q__align.mp,8,8
	.type	Q__align.w1,@object     # @Q__align.w1
	.local	Q__align.w1
	.comm	Q__align.w1,8,8
	.type	Q__align.w2,@object     # @Q__align.w2
	.local	Q__align.w2
	.comm	Q__align.w2,8,8
	.type	Q__align.match,@object  # @Q__align.match
	.local	Q__align.match
	.comm	Q__align.match,8,8
	.type	Q__align.initverticalw,@object # @Q__align.initverticalw
	.local	Q__align.initverticalw
	.comm	Q__align.initverticalw,8,8
	.type	Q__align.lastverticalw,@object # @Q__align.lastverticalw
	.local	Q__align.lastverticalw
	.comm	Q__align.lastverticalw,8,8
	.type	Q__align.mseq1,@object  # @Q__align.mseq1
	.local	Q__align.mseq1
	.comm	Q__align.mseq1,8,8
	.type	Q__align.mseq2,@object  # @Q__align.mseq2
	.local	Q__align.mseq2
	.comm	Q__align.mseq2,8,8
	.type	Q__align.mseq,@object   # @Q__align.mseq
	.local	Q__align.mseq
	.comm	Q__align.mseq,8,8
	.type	Q__align.digf1,@object  # @Q__align.digf1
	.local	Q__align.digf1
	.comm	Q__align.digf1,8,8
	.type	Q__align.digf2,@object  # @Q__align.digf2
	.local	Q__align.digf2
	.comm	Q__align.digf2,8,8
	.type	Q__align.diaf1,@object  # @Q__align.diaf1
	.local	Q__align.diaf1
	.comm	Q__align.diaf1,8,8
	.type	Q__align.diaf2,@object  # @Q__align.diaf2
	.local	Q__align.diaf2
	.comm	Q__align.diaf2,8,8
	.type	Q__align.gapz1,@object  # @Q__align.gapz1
	.local	Q__align.gapz1
	.comm	Q__align.gapz1,8,8
	.type	Q__align.gapz2,@object  # @Q__align.gapz2
	.local	Q__align.gapz2
	.comm	Q__align.gapz2,8,8
	.type	Q__align.gapf1,@object  # @Q__align.gapf1
	.local	Q__align.gapf1
	.comm	Q__align.gapf1,8,8
	.type	Q__align.gapf2,@object  # @Q__align.gapf2
	.local	Q__align.gapf2
	.comm	Q__align.gapf2,8,8
	.type	Q__align.ogcp1g,@object # @Q__align.ogcp1g
	.local	Q__align.ogcp1g
	.comm	Q__align.ogcp1g,8,8
	.type	Q__align.ogcp2g,@object # @Q__align.ogcp2g
	.local	Q__align.ogcp2g
	.comm	Q__align.ogcp2g,8,8
	.type	Q__align.fgcp1g,@object # @Q__align.fgcp1g
	.local	Q__align.fgcp1g
	.comm	Q__align.fgcp1g,8,8
	.type	Q__align.fgcp2g,@object # @Q__align.fgcp2g
	.local	Q__align.fgcp2g
	.comm	Q__align.fgcp2g,8,8
	.type	Q__align.og_h_dg_n1_p,@object # @Q__align.og_h_dg_n1_p
	.local	Q__align.og_h_dg_n1_p
	.comm	Q__align.og_h_dg_n1_p,8,8
	.type	Q__align.og_h_dg_n2_p,@object # @Q__align.og_h_dg_n2_p
	.local	Q__align.og_h_dg_n2_p
	.comm	Q__align.og_h_dg_n2_p,8,8
	.type	Q__align.fg_h_dg_n1_p,@object # @Q__align.fg_h_dg_n1_p
	.local	Q__align.fg_h_dg_n1_p
	.comm	Q__align.fg_h_dg_n1_p,8,8
	.type	Q__align.fg_h_dg_n2_p,@object # @Q__align.fg_h_dg_n2_p
	.local	Q__align.fg_h_dg_n2_p
	.comm	Q__align.fg_h_dg_n2_p,8,8
	.type	Q__align.og_t_fg_h_dg_n1_p,@object # @Q__align.og_t_fg_h_dg_n1_p
	.local	Q__align.og_t_fg_h_dg_n1_p
	.comm	Q__align.og_t_fg_h_dg_n1_p,8,8
	.type	Q__align.og_t_fg_h_dg_n2_p,@object # @Q__align.og_t_fg_h_dg_n2_p
	.local	Q__align.og_t_fg_h_dg_n2_p
	.comm	Q__align.og_t_fg_h_dg_n2_p,8,8
	.type	Q__align.fg_t_og_h_dg_n1_p,@object # @Q__align.fg_t_og_h_dg_n1_p
	.local	Q__align.fg_t_og_h_dg_n1_p
	.comm	Q__align.fg_t_og_h_dg_n1_p,8,8
	.type	Q__align.fg_t_og_h_dg_n2_p,@object # @Q__align.fg_t_og_h_dg_n2_p
	.local	Q__align.fg_t_og_h_dg_n2_p
	.comm	Q__align.fg_t_og_h_dg_n2_p,8,8
	.type	Q__align.gapz_n1,@object # @Q__align.gapz_n1
	.local	Q__align.gapz_n1
	.comm	Q__align.gapz_n1,8,8
	.type	Q__align.gapz_n2,@object # @Q__align.gapz_n2
	.local	Q__align.gapz_n2
	.comm	Q__align.gapz_n2,8,8
	.type	Q__align.cpmx1,@object  # @Q__align.cpmx1
	.local	Q__align.cpmx1
	.comm	Q__align.cpmx1,8,8
	.type	Q__align.cpmx2,@object  # @Q__align.cpmx2
	.local	Q__align.cpmx2
	.comm	Q__align.cpmx2,8,8
	.type	Q__align.intwork,@object # @Q__align.intwork
	.local	Q__align.intwork
	.comm	Q__align.intwork,8,8
	.type	Q__align.floatwork,@object # @Q__align.floatwork
	.local	Q__align.floatwork
	.comm	Q__align.floatwork,8,8
	.type	Q__align.orlgth1,@object # @Q__align.orlgth1
	.local	Q__align.orlgth1
	.comm	Q__align.orlgth1,4,4
	.type	Q__align.orlgth2,@object # @Q__align.orlgth2
	.local	Q__align.orlgth2
	.comm	Q__align.orlgth2,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"alloclen=%d, resultlen=%d, N=%d\n"
	.size	.L.str, 33

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"LENGTH OVER!\n"
	.size	.L.str.1, 14

	.type	Q__align_gapmap.mi,@object # @Q__align_gapmap.mi
	.local	Q__align_gapmap.mi
	.comm	Q__align_gapmap.mi,4,4
	.type	Q__align_gapmap.m,@object # @Q__align_gapmap.m
	.local	Q__align_gapmap.m
	.comm	Q__align_gapmap.m,8,8
	.type	Q__align_gapmap.ijp,@object # @Q__align_gapmap.ijp
	.local	Q__align_gapmap.ijp
	.comm	Q__align_gapmap.ijp,8,8
	.type	Q__align_gapmap.mpi,@object # @Q__align_gapmap.mpi
	.local	Q__align_gapmap.mpi
	.comm	Q__align_gapmap.mpi,4,4
	.type	Q__align_gapmap.mp,@object # @Q__align_gapmap.mp
	.local	Q__align_gapmap.mp
	.comm	Q__align_gapmap.mp,8,8
	.type	Q__align_gapmap.w1,@object # @Q__align_gapmap.w1
	.local	Q__align_gapmap.w1
	.comm	Q__align_gapmap.w1,8,8
	.type	Q__align_gapmap.w2,@object # @Q__align_gapmap.w2
	.local	Q__align_gapmap.w2
	.comm	Q__align_gapmap.w2,8,8
	.type	Q__align_gapmap.match,@object # @Q__align_gapmap.match
	.local	Q__align_gapmap.match
	.comm	Q__align_gapmap.match,8,8
	.type	Q__align_gapmap.initverticalw,@object # @Q__align_gapmap.initverticalw
	.local	Q__align_gapmap.initverticalw
	.comm	Q__align_gapmap.initverticalw,8,8
	.type	Q__align_gapmap.lastverticalw,@object # @Q__align_gapmap.lastverticalw
	.local	Q__align_gapmap.lastverticalw
	.comm	Q__align_gapmap.lastverticalw,8,8
	.type	Q__align_gapmap.mseq1,@object # @Q__align_gapmap.mseq1
	.local	Q__align_gapmap.mseq1
	.comm	Q__align_gapmap.mseq1,8,8
	.type	Q__align_gapmap.mseq2,@object # @Q__align_gapmap.mseq2
	.local	Q__align_gapmap.mseq2
	.comm	Q__align_gapmap.mseq2,8,8
	.type	Q__align_gapmap.mseq,@object # @Q__align_gapmap.mseq
	.local	Q__align_gapmap.mseq
	.comm	Q__align_gapmap.mseq,8,8
	.type	Q__align_gapmap.digf1,@object # @Q__align_gapmap.digf1
	.local	Q__align_gapmap.digf1
	.comm	Q__align_gapmap.digf1,8,8
	.type	Q__align_gapmap.digf2,@object # @Q__align_gapmap.digf2
	.local	Q__align_gapmap.digf2
	.comm	Q__align_gapmap.digf2,8,8
	.type	Q__align_gapmap.diaf1,@object # @Q__align_gapmap.diaf1
	.local	Q__align_gapmap.diaf1
	.comm	Q__align_gapmap.diaf1,8,8
	.type	Q__align_gapmap.diaf2,@object # @Q__align_gapmap.diaf2
	.local	Q__align_gapmap.diaf2
	.comm	Q__align_gapmap.diaf2,8,8
	.type	Q__align_gapmap.gapz1,@object # @Q__align_gapmap.gapz1
	.local	Q__align_gapmap.gapz1
	.comm	Q__align_gapmap.gapz1,8,8
	.type	Q__align_gapmap.gapz2,@object # @Q__align_gapmap.gapz2
	.local	Q__align_gapmap.gapz2
	.comm	Q__align_gapmap.gapz2,8,8
	.type	Q__align_gapmap.gapf1,@object # @Q__align_gapmap.gapf1
	.local	Q__align_gapmap.gapf1
	.comm	Q__align_gapmap.gapf1,8,8
	.type	Q__align_gapmap.gapf2,@object # @Q__align_gapmap.gapf2
	.local	Q__align_gapmap.gapf2
	.comm	Q__align_gapmap.gapf2,8,8
	.type	Q__align_gapmap.ogcp1g,@object # @Q__align_gapmap.ogcp1g
	.local	Q__align_gapmap.ogcp1g
	.comm	Q__align_gapmap.ogcp1g,8,8
	.type	Q__align_gapmap.ogcp2g,@object # @Q__align_gapmap.ogcp2g
	.local	Q__align_gapmap.ogcp2g
	.comm	Q__align_gapmap.ogcp2g,8,8
	.type	Q__align_gapmap.fgcp1g,@object # @Q__align_gapmap.fgcp1g
	.local	Q__align_gapmap.fgcp1g
	.comm	Q__align_gapmap.fgcp1g,8,8
	.type	Q__align_gapmap.fgcp2g,@object # @Q__align_gapmap.fgcp2g
	.local	Q__align_gapmap.fgcp2g
	.comm	Q__align_gapmap.fgcp2g,8,8
	.type	Q__align_gapmap.og_h_dg_n1_p,@object # @Q__align_gapmap.og_h_dg_n1_p
	.local	Q__align_gapmap.og_h_dg_n1_p
	.comm	Q__align_gapmap.og_h_dg_n1_p,8,8
	.type	Q__align_gapmap.og_h_dg_n2_p,@object # @Q__align_gapmap.og_h_dg_n2_p
	.local	Q__align_gapmap.og_h_dg_n2_p
	.comm	Q__align_gapmap.og_h_dg_n2_p,8,8
	.type	Q__align_gapmap.fg_h_dg_n1_p,@object # @Q__align_gapmap.fg_h_dg_n1_p
	.local	Q__align_gapmap.fg_h_dg_n1_p
	.comm	Q__align_gapmap.fg_h_dg_n1_p,8,8
	.type	Q__align_gapmap.fg_h_dg_n2_p,@object # @Q__align_gapmap.fg_h_dg_n2_p
	.local	Q__align_gapmap.fg_h_dg_n2_p
	.comm	Q__align_gapmap.fg_h_dg_n2_p,8,8
	.type	Q__align_gapmap.og_t_fg_h_dg_n1_p,@object # @Q__align_gapmap.og_t_fg_h_dg_n1_p
	.local	Q__align_gapmap.og_t_fg_h_dg_n1_p
	.comm	Q__align_gapmap.og_t_fg_h_dg_n1_p,8,8
	.type	Q__align_gapmap.og_t_fg_h_dg_n2_p,@object # @Q__align_gapmap.og_t_fg_h_dg_n2_p
	.local	Q__align_gapmap.og_t_fg_h_dg_n2_p
	.comm	Q__align_gapmap.og_t_fg_h_dg_n2_p,8,8
	.type	Q__align_gapmap.fg_t_og_h_dg_n1_p,@object # @Q__align_gapmap.fg_t_og_h_dg_n1_p
	.local	Q__align_gapmap.fg_t_og_h_dg_n1_p
	.comm	Q__align_gapmap.fg_t_og_h_dg_n1_p,8,8
	.type	Q__align_gapmap.fg_t_og_h_dg_n2_p,@object # @Q__align_gapmap.fg_t_og_h_dg_n2_p
	.local	Q__align_gapmap.fg_t_og_h_dg_n2_p
	.comm	Q__align_gapmap.fg_t_og_h_dg_n2_p,8,8
	.type	Q__align_gapmap.gapz_n1,@object # @Q__align_gapmap.gapz_n1
	.local	Q__align_gapmap.gapz_n1
	.comm	Q__align_gapmap.gapz_n1,8,8
	.type	Q__align_gapmap.gapz_n2,@object # @Q__align_gapmap.gapz_n2
	.local	Q__align_gapmap.gapz_n2
	.comm	Q__align_gapmap.gapz_n2,8,8
	.type	Q__align_gapmap.cpmx1,@object # @Q__align_gapmap.cpmx1
	.local	Q__align_gapmap.cpmx1
	.comm	Q__align_gapmap.cpmx1,8,8
	.type	Q__align_gapmap.cpmx2,@object # @Q__align_gapmap.cpmx2
	.local	Q__align_gapmap.cpmx2
	.comm	Q__align_gapmap.cpmx2,8,8
	.type	Q__align_gapmap.intwork,@object # @Q__align_gapmap.intwork
	.local	Q__align_gapmap.intwork
	.comm	Q__align_gapmap.intwork,8,8
	.type	Q__align_gapmap.floatwork,@object # @Q__align_gapmap.floatwork
	.local	Q__align_gapmap.floatwork
	.comm	Q__align_gapmap.floatwork,8,8
	.type	Q__align_gapmap.orlgth1,@object # @Q__align_gapmap.orlgth1
	.local	Q__align_gapmap.orlgth1
	.comm	Q__align_gapmap.orlgth1,4,4
	.type	Q__align_gapmap.orlgth2,@object # @Q__align_gapmap.orlgth2
	.local	Q__align_gapmap.orlgth2
	.comm	Q__align_gapmap.orlgth2,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
