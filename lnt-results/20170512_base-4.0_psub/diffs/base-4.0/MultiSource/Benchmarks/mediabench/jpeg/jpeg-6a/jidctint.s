	.text
	.file	"jidctint.bc"
	.globl	jpeg_idct_islow
	.p2align	4, 0x90
	.type	jpeg_idct_islow,@function
jpeg_idct_islow:                        # @jpeg_idct_islow
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$200, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 256
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, -120(%rsp)        # 4-byte Spill
	movq	%rcx, -88(%rsp)         # 8-byte Spill
	movq	408(%rdi), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	movq	88(%rsi), %r13
	movl	$9, %r15d
	leaq	-64(%rsp), %r9
	.p2align	4, 0x90
.LBB0_1:                                # =>This Inner Loop Header: Depth=1
	movswq	16(%rdx), %r8
	movswq	32(%rdx), %rcx
	movl	%ecx, %esi
	orl	%r8d, %esi
	movswq	48(%rdx), %r10
	movswq	64(%rdx), %rdi
	movl	%edi, %ebx
	orl	%r10d, %ebx
	orl	%esi, %ebx
	movswq	80(%rdx), %r11
	movswq	96(%rdx), %rsi
	movl	%esi, %ebp
	orl	%r11d, %ebp
	orl	%ebx, %ebp
	movswq	112(%rdx), %rax
	movl	%eax, %ebx
	orw	%bp, %bx
	je	.LBB0_2
# BB#3:                                 #   in Loop: Header=BB0_1 Depth=1
	movslq	64(%r13), %rbx
	imulq	%rcx, %rbx
	movslq	192(%r13), %rcx
	imulq	%rsi, %rcx
	leaq	(%rcx,%rbx), %rsi
	imulq	$4433, %rsi, %rsi       # imm = 0x1151
	imulq	$-15137, %rcx, %r12     # imm = 0xC4DF
	addq	%rsi, %r12
	imulq	$6270, %rbx, %rcx       # imm = 0x187E
	addq	%rsi, %rcx
	movswq	(%rdx), %rsi
	movslq	(%r13), %rbx
	imulq	%rsi, %rbx
	movslq	128(%r13), %rsi
	imulq	%rdi, %rsi
	leaq	(%rsi,%rbx), %rdi
	shlq	$13, %rdi
	subq	%rsi, %rbx
	shlq	$13, %rbx
	leaq	1024(%rdi,%rcx), %r14
	subq	%rcx, %rdi
	movq	%r8, %rsi
	leaq	1024(%rbx,%r12), %r8
	subq	%r12, %rbx
	movslq	224(%r13), %r12
	imulq	%rax, %r12
	movslq	160(%r13), %rax
	imulq	%r11, %rax
	movslq	96(%r13), %rbp
	imulq	%r10, %rbp
	movslq	32(%r13), %rcx
	imulq	%rsi, %rcx
	leaq	(%rbp,%r12), %r10
	leaq	(%rcx,%rax), %r11
	leaq	(%r11,%r10), %rsi
	imulq	$9633, %rsi, %rsi       # imm = 0x25A1
	imulq	$-16069, %r10, %r10     # imm = 0xC13B
	imulq	$-3196, %r11, %r11      # imm = 0xF384
	addq	%rsi, %r10
	addq	%rsi, %r11
	leaq	(%rbp,%rax), %rsi
	imulq	$16819, %rax, %rax      # imm = 0x41B3
	imulq	$25172, %rbp, %rbp      # imm = 0x6254
	imulq	$-20995, %rsi, %rsi     # imm = 0xADFD
	addq	%rsi, %rax
	addq	%rsi, %rbp
	leaq	(%rcx,%r12), %rsi
	imulq	$2446, %r12, %r12       # imm = 0x98E
	imulq	$-7373, %rsi, %rsi      # imm = 0xE333
	addq	%rsi, %r12
	addq	%r10, %r12
	addq	%r10, %rbp
	imulq	$12299, %rcx, %rcx      # imm = 0x300B
	addq	%rsi, %rcx
	addq	%r11, %rax
	addq	%r11, %rcx
	leaq	(%r14,%rcx), %rsi
	shrq	$11, %rsi
	movl	%esi, (%r9)
	subq	%rcx, %r14
	shrq	$11, %r14
	movl	%r14d, 224(%r9)
	leaq	(%r8,%rbp), %rcx
	shrq	$11, %rcx
	subq	%rbp, %r8
	shrq	$11, %r8
	leaq	1024(%rbx,%rax), %rsi
	addq	$1024, %rbx             # imm = 0x400
	shrq	$11, %rsi
	subq	%rax, %rbx
	shrq	$11, %rbx
	leaq	1024(%rdi,%r12), %rax
	addq	$1024, %rdi             # imm = 0x400
	shrq	$11, %rax
	subq	%r12, %rdi
	shrq	$11, %rdi
	movl	$32, %r10d
	jmp	.LBB0_4
	.p2align	4, 0x90
.LBB0_2:                                #   in Loop: Header=BB0_1 Depth=1
	movswl	(%rdx), %ecx
	imull	(%r13), %ecx
	shll	$2, %ecx
	movl	%ecx, (%r9)
	movl	%ecx, 128(%r9)
	movl	%ecx, %r8d
	movl	%ecx, %esi
	movl	%ecx, %ebx
	movl	%ecx, %eax
	movl	$56, %r10d
	movl	%ecx, %edi
.LBB0_4:                                #   in Loop: Header=BB0_1 Depth=1
	movl	%ecx, 32(%r9)
	movl	%r8d, 192(%r9)
	movl	%esi, 64(%r9)
	movl	%ebx, 160(%r9)
	movl	%eax, 96(%r9)
	movl	%edi, (%r9,%r10,4)
	addq	$4, %r9
	addq	$4, %r13
	addq	$2, %rdx
	decl	%r15d
	cmpl	$1, %r15d
	jg	.LBB0_1
# BB#5:                                 # %.preheader
	movl	-120(%rsp), %eax        # 4-byte Reload
	movq	%rax, -96(%rsp)         # 8-byte Spill
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB0_6:                                # =>This Inner Loop Header: Depth=1
	movq	-88(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%r11), %r10
	addq	-96(%rsp), %r10         # 8-byte Folded Reload
	movslq	-60(%rsp,%r11,4), %rcx
	movslq	-56(%rsp,%r11,4), %rbp
	movl	%ebp, %eax
	movq	%rcx, %r9
	orl	%ecx, %eax
	movslq	-52(%rsp,%r11,4), %rsi
	movslq	-48(%rsp,%r11,4), %rdx
	movl	%edx, %ecx
	orl	%esi, %ecx
	orl	%eax, %ecx
	movslq	-44(%rsp,%r11,4), %r8
	movslq	-40(%rsp,%r11,4), %rax
	movl	%eax, %ebx
	orl	%r8d, %ebx
	orl	%ecx, %ebx
	movslq	-36(%rsp,%r11,4), %rcx
	movl	%ecx, %edi
	orl	%ebx, %edi
	je	.LBB0_7
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	leaq	(%rax,%rbp), %rdi
	imulq	$4433, %rdi, %rdi       # imm = 0x1151
	imull	$-15137, %eax, %eax     # imm = 0xC4DF
	addl	%edi, %eax
	movq	%rax, -80(%rsp)         # 8-byte Spill
	imull	$6270, %ebp, %r12d      # imm = 0x187E
	addl	%edi, %r12d
	movslq	-64(%rsp,%r11,4), %rbx
	leal	(%rbx,%rdx), %r13d
	subl	%edx, %ebx
	shll	$13, %r13d
	leal	(%r13,%r12), %eax
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movq	%rcx, %rax
	addq	%rsi, %rax
	movq	%r9, -72(%rsp)          # 8-byte Spill
	movq	%r9, %rbp
	addq	%r8, %rbp
	leaq	(%rax,%rbp), %rdx
	imulq	$9633, %rdx, %rdi       # imm = 0x25A1
	imull	$2446, %ecx, %r14d      # imm = 0x98E
	leal	(%rcx,%r9), %ecx
	imull	$-7373, %ecx, %ecx      # imm = 0xE333
	movl	%ecx, -108(%rsp)        # 4-byte Spill
	movq	%rsi, %rdx
	imull	$-16069, %eax, %r9d     # imm = 0xC13B
	addl	%edi, %r9d
	movq	%rdi, %rsi
	addl	%ecx, %r14d
	addl	%r9d, %r14d
	leal	(%r14,%r12), %r15d
	negl	%r15d
	leal	131072(%r13,%r15), %eax
                                        # kill: %R13D<def> %R13D<kill> %R13<kill> %R13<def>
	subl	%r12d, %r13d
	shll	$13, %ebx
	movq	-80(%rsp), %r12         # 8-byte Reload
	leal	(%rbx,%r12), %r15d
	imull	$16819, %r8d, %edi      # imm = 0x41B3
	addl	%edx, %r8d
	imull	$-20995, %r8d, %r8d     # imm = 0xADFD
	imull	$-3196, %ebp, %ebp      # imm = 0xF384
	addl	%esi, %ebp
	addl	%r8d, %edi
	addl	%ebp, %edi
	leal	(%rdi,%r12), %ecx
	movq	%r12, %rsi
	negl	%ecx
	leal	131072(%rbx,%rcx), %r12d
                                        # kill: %EBX<def> %EBX<kill> %RBX<kill> %RBX<def>
	subl	%esi, %ebx
	imull	$25172, %edx, %esi      # imm = 0x6254
	addl	%r8d, %esi
	addl	%r9d, %esi
	imull	$12299, -72(%rsp), %ecx # 4-byte Folded Reload
                                        # imm = 0x300B
	addl	-108(%rsp), %ecx        # 4-byte Folded Reload
	addl	%ebp, %ecx
	movl	$131072, %ebp           # imm = 0x20000
	subl	%ecx, %ebp
	movq	-120(%rsp), %r9         # 8-byte Reload
	leal	131072(%rcx,%r9), %r8d
	addl	%r9d, %ebp
	movl	$131072, %ecx           # imm = 0x20000
	subl	%esi, %ecx
	leal	131072(%rsi,%r15), %esi
	addl	%r15d, %ecx
	leal	131072(%rbx,%rdi), %edi
	leal	131072(%r13,%r14), %r9d
	shrl	$18, %r8d
	andl	$1023, %r8d             # imm = 0x3FF
	movq	-104(%rsp), %rdx        # 8-byte Reload
	movzbl	128(%rdx,%r8), %ebx
	movb	%bl, (%r10)
	shrl	$18, %ebp
	andl	$1023, %ebp             # imm = 0x3FF
	movzbl	128(%rdx,%rbp), %ebx
	movb	%bl, 7(%r10)
	shrl	$18, %esi
	andl	$1023, %esi             # imm = 0x3FF
	movzbl	128(%rdx,%rsi), %ebx
	movb	%bl, 1(%r10)
	shrl	$18, %ecx
	andl	$1023, %ecx             # imm = 0x3FF
	movzbl	128(%rdx,%rcx), %ecx
	movb	%cl, 6(%r10)
	shrl	$18, %edi
	andl	$1023, %edi             # imm = 0x3FF
	movzbl	128(%rdx,%rdi), %ecx
	movb	%cl, 2(%r10)
	shrl	$18, %r12d
	andl	$1023, %r12d            # imm = 0x3FF
	movzbl	128(%rdx,%r12), %ecx
	movb	%cl, 5(%r10)
	shrl	$18, %r9d
	andl	$1023, %r9d             # imm = 0x3FF
	movzbl	128(%rdx,%r9), %ecx
	movb	%cl, 3(%r10)
	shrl	$18, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movb	128(%rdx,%rax), %al
	movl	$4, %ecx
	jmp	.LBB0_9
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_6 Depth=1
	movl	-64(%rsp,%r11,4), %eax
	addl	$16, %eax
	shrl	$5, %eax
	andl	$1023, %eax             # imm = 0x3FF
	movq	-104(%rsp), %rcx        # 8-byte Reload
	movzbl	128(%rcx,%rax), %eax
	movb	%al, (%r10)
	imull	$16843009, %eax, %ecx   # imm = 0x1010101
	movw	%cx, 5(%r10)
	movl	%ecx, 1(%r10)
	movl	$7, %ecx
.LBB0_9:                                #   in Loop: Header=BB0_6 Depth=1
	movb	%al, (%r10,%rcx)
	addq	$8, %r11
	cmpq	$64, %r11
	jne	.LBB0_6
# BB#10:
	addq	$200, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jpeg_idct_islow, .Lfunc_end0-jpeg_idct_islow
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
