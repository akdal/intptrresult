	.text
	.file	"maketree.bc"
	.globl	MakeTree
	.p2align	4, 0x90
	.type	MakeTree,@function
MakeTree:                               # @MakeTree
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%r8, (%rsp)             # 8-byte Spill
	movl	%ecx, %r12d
	movl	%edx, %r13d
	movl	%esi, %ebx
	movl	%edi, %r14d
	movl	96(%rsp), %r15d
	movl	$48, %edi
	callq	malloc
	movq	%rbx, %r9
	movq	%rax, %rbx
	movq	%rbp, 40(%rbx)
	movl	%r15d, 4(%rbx)
	leal	(%r9,%r14), %esi
	leal	(%r13,%r14), %eax
	imull	%esi, %esi
	imull	%eax, %eax
	leal	(%rax,%rsi), %ecx
	cmpl	$1048576, %ecx          # imm = 0x100000
	sbbl	%edx, %edx
	cmpl	$4194304, %ecx          # imm = 0x400000
	movl	$1, %r8d
	cmoval	%r8d, %edx
	movl	%r13d, %edi
	subl	%r14d, %edi
	imull	%edi, %edi
	testl	%edx, %edx
	jne	.LBB0_4
# BB#1:
	leal	-1048576(%rsi,%rdi), %ebp
	cmpl	$3145728, %ebp          # imm = 0x300000
	ja	.LBB0_4
# BB#2:
	movl	%r9d, %ebp
	subl	%r14d, %ebp
	imull	%ebp, %ebp
	leal	-1048576(%rbp,%rdi), %ecx
	cmpl	$3145728, %ecx          # imm = 0x300000
	ja	.LBB0_4
# BB#3:
	leal	-1048576(%rbp,%rax), %ecx
	xorl	%ebp, %ebp
	cmpl	$3145729, %ecx          # imm = 0x300001
	jb	.LBB0_9
.LBB0_4:                                # %._crit_edge.i
	addl	%edi, %esi
	cmpl	$1048576, %esi          # imm = 0x100000
	sbbl	%ecx, %ecx
	cmpl	$4194304, %esi          # imm = 0x400000
	cmoval	%r8d, %ecx
	addl	%ecx, %edx
	movl	%r9d, %ecx
	subl	%r14d, %ecx
	imull	%ecx, %ecx
	addl	%ecx, %edi
	cmpl	$1048576, %edi          # imm = 0x100000
	sbbl	%esi, %esi
	cmpl	$4194304, %edi          # imm = 0x400000
	cmoval	%r8d, %esi
	addl	%edx, %esi
	addl	%ecx, %eax
	cmpl	$1048576, %eax          # imm = 0x100000
	sbbl	%edx, %edx
	cmpl	$4194304, %eax          # imm = 0x400000
	cmoval	%r8d, %edx
	addl	%esi, %edx
	cmpl	$4, %edx
	je	.LBB0_6
# BB#5:                                 # %._crit_edge.i
	cmpl	$-4, %edx
	jne	.LBB0_7
.LBB0_6:                                # %CheckIntersect.exit
	movl	$1, %ebp
	cmpl	$1024, %r14d            # imm = 0x400
	jl	.LBB0_9
.LBB0_7:                                # %.thread
	movl	104(%rsp), %edx
	testl	%edx, %edx
	je	.LBB0_8
# BB#11:
	movl	%r14d, %ebp
	shrl	$31, %ebp
	addl	%r14d, %ebp
	sarl	%ebp
	movq	(%rsp), %r8             # 8-byte Reload
	leal	(%r8,%r12), %eax
	movq	%r13, %rdi
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	movl	%eax, %r13d
	shrl	$31, %r13d
	addl	%eax, %r13d
	sarl	%r13d
	leal	1(%r8,%r12), %ecx
	shrl	$31, %ecx
	leal	1(%rcx,%rax), %r14d
	sarl	%r14d
	movl	%r9d, %eax
	subl	%ebp, %eax
	movl	%eax, %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movl	%edi, %r15d
	subl	%ebp, %r15d
	movq	%r12, 16(%rsp)          # 8-byte Spill
	leal	(%r14,%r8), %r12d
	leal	1(%r14,%r8), %eax
	shrl	$31, %eax
	leal	1(%rax,%r12), %ecx
	sarl	%ecx
	decl	%edx
	movl	%ebp, %edi
	movq	%rdx, %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%r15d, %edx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%r9, (%rsp)             # 8-byte Spill
	movq	%rbx, %r9
	pushq	%rax
.Lcfi13:
	.cfi_adjust_cfa_offset 8
	pushq	$2
.Lcfi14:
	.cfi_adjust_cfa_offset 8
	callq	MakeTree
	addq	$16, %rsp
.Lcfi15:
	.cfi_adjust_cfa_offset -16
	movq	%rax, 24(%rbx)
	movq	(%rsp), %rsi            # 8-byte Reload
	addl	%ebp, %esi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	%r12d, %r8d
	shrl	$31, %r8d
	addl	%r12d, %r8d
	sarl	%r8d
	movl	%ebp, %edi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	movl	%r14d, %ecx
	movq	%rbx, %r9
	movq	24(%rsp), %r12          # 8-byte Reload
	pushq	%r12
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	$3
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	MakeTree
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	movq	%rax, 32(%rbx)
	movq	32(%rsp), %r15          # 8-byte Reload
	addl	%ebp, %r15d
	movq	16(%rsp), %rax          # 8-byte Reload
	leal	(%r13,%rax), %r14d
	leal	1(%r13,%rax), %eax
	shrl	$31, %eax
	leal	1(%rax,%r14), %ecx
	sarl	%ecx
	movl	%ebp, %edi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	movl	%r13d, %r8d
	movq	%rbx, %r9
	pushq	%r12
.Lcfi19:
	.cfi_adjust_cfa_offset 8
	pushq	$1
.Lcfi20:
	.cfi_adjust_cfa_offset 8
	callq	MakeTree
	addq	$16, %rsp
.Lcfi21:
	.cfi_adjust_cfa_offset -16
	movq	%rax, 16(%rbx)
	movl	%r14d, %r8d
	shrl	$31, %r8d
	addl	%r14d, %r8d
	sarl	%r8d
	movl	%ebp, %edi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	%r15d, %edx
	movq	16(%rsp), %rcx          # 8-byte Reload
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movq	%rbx, %r9
	pushq	%r12
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi23:
	.cfi_adjust_cfa_offset 8
	callq	MakeTree
	addq	$16, %rsp
.Lcfi24:
	.cfi_adjust_cfa_offset -16
	movq	%rax, 8(%rbx)
	movl	$2, (%rbx)
	jmp	.LBB0_10
.LBB0_8:
	xorl	%ebp, %ebp
.LBB0_9:
	movl	%ebp, (%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%rbx)
	movups	%xmm0, 8(%rbx)
.LBB0_10:
	movq	%rbx, %rax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	MakeTree, .Lfunc_end0-MakeTree
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
