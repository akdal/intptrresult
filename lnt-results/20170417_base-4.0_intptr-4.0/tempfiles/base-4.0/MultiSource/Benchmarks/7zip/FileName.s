	.text
	.file	"FileName.bc"
	.globl	_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE,@function
_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE: # @_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movslq	8(%rbx), %rax
	testq	%rax, %rax
	je	.LBB0_8
# BB#1:
	movq	(%rbx), %rcx
	leaq	(,%rax,4), %rdx
	.p2align	4, 0x90
.LBB0_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$47, -4(%rcx,%rdx)
	je	.LBB0_3
# BB#4:                                 #   in Loop: Header=BB0_2 Depth=1
	addq	$-4, %rdx
	jne	.LBB0_2
# BB#5:
	movl	$-1, %edx
	jmp	.LBB0_6
.LBB0_3:
	leaq	-4(%rcx,%rdx), %rdx
	subq	%rcx, %rdx
	shrq	$2, %rdx
.LBB0_6:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	decl	%eax
	cmpl	%eax, %edx
	je	.LBB0_8
# BB#7:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movl	$47, (%rax,%rcx,4)
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movl	$0, 4(%rax,%rcx,4)
.LBB0_8:
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE, .Lfunc_end0-_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE
	.cfi_endproc

	.globl	_ZN8NWindows5NFile5NName31SplitNameToPureNameAndExtensionERK11CStringBaseIwERS3_S6_S6_
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile5NName31SplitNameToPureNameAndExtensionERK11CStringBaseIwERS3_S6_S6_,@function
_ZN8NWindows5NFile5NName31SplitNameToPureNameAndExtensionERK11CStringBaseIwERS3_S6_S6_: # @_ZN8NWindows5NFile5NName31SplitNameToPureNameAndExtensionERK11CStringBaseIwERS3_S6_S6_
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 96
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rdx, %r15
	movq	%rsi, %r14
	movq	%rdi, %r12
	movslq	8(%r12), %rax
	testq	%rax, %rax
	je	.LBB1_5
# BB#1:
	movq	(%r12), %rdx
	shlq	$2, %rax
	.p2align	4, 0x90
.LBB1_2:                                # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rdx,%rax)
	je	.LBB1_4
# BB#3:                                 #   in Loop: Header=BB1_2 Depth=1
	addq	$-4, %rax
	jne	.LBB1_2
	jmp	.LBB1_5
.LBB1_4:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	-4(%rdx,%rax), %r13
	subq	%rdx, %r13
	shrq	$2, %r13
	testl	%r13d, %r13d
	js	.LBB1_5
# BB#18:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	leaq	24(%rsp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r12, %rsi
	movl	%r13d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r14, %rbx
	je	.LBB1_19
# BB#20:
	movl	$0, 8(%r14)
	movq	(%r14), %rbp
	movl	$0, (%rbp)
	movslq	32(%rsp), %rax
	incq	%rax
	movl	12(%r14), %ebx
	cmpl	%ebx, %eax
	je	.LBB1_26
# BB#21:
	movl	$4, %ecx
	movq	%rax, 16(%rsp)          # 8-byte Spill
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp0:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp1:
# BB#22:                                # %.noexc
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB1_25
# BB#23:                                # %.noexc
	testl	%ebx, %ebx
	jle	.LBB1_25
# BB#24:                                # %._crit_edge.thread.i.i21
	movq	%rbp, %rdi
	movq	%rcx, %rbx
	callq	_ZdaPv
	movq	%rbx, %rcx
	movslq	8(%r14), %rax
.LBB1_25:                               # %._crit_edge16.i.i22
	movq	%rcx, (%r14)
	movl	$0, (%rcx,%rax,4)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%eax, 12(%r14)
	movq	%rcx, %rbp
.LBB1_26:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i23
	movq	24(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_27:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_27
# BB#28:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i26
	movl	32(%rsp), %eax
	movl	%eax, 8(%r14)
	testq	%rdi, %rdi
	jne	.LBB1_30
	jmp	.LBB1_31
.LBB1_5:                                # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
	cmpq	%r14, %r12
	je	.LBB1_16
# BB#6:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movl	$0, 8(%r14)
	movq	(%r14), %rbp
	movl	$0, (%rbp)
	movslq	8(%r12), %rbx
	incq	%rbx
	movl	12(%r14), %r15d
	cmpl	%r15d, %ebx
	jne	.LBB1_8
# BB#7:
	movq	16(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_13
.LBB1_8:
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB1_9
# BB#10:
	testl	%r15d, %r15d
	movq	16(%rsp), %r15          # 8-byte Reload
	jle	.LBB1_12
# BB#11:                                # %._crit_edge.thread.i.i
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	8(%r14), %rax
	jmp	.LBB1_12
.LBB1_19:                               # %._ZN11CStringBaseIwEaSERKS0_.exit27_crit_edge
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB1_31
.LBB1_30:
	callq	_ZdaPv
.LBB1_31:                               # %_ZN11CStringBaseIwED2Ev.exit28
	movl	$0, 8(%r15)
	movq	(%r15), %rbx
	movl	$0, (%rbx)
	movl	12(%r15), %ebp
	cmpl	$2, %ebp
	jne	.LBB1_33
# BB#32:
	leaq	24(%rsp), %rbp
	jmp	.LBB1_38
.LBB1_33:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB1_34
# BB#35:
	testl	%ebp, %ebp
	leaq	24(%rsp), %rbp
	jle	.LBB1_37
# BB#36:                                # %._crit_edge.thread.i.i32
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r15), %rax
	jmp	.LBB1_37
.LBB1_9:
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB1_12:                               # %._crit_edge16.i.i
	movq	%r13, (%r14)
	movl	$0, (%r13,%rax,4)
	movl	%ebx, 12(%r14)
	movq	%r13, %rbp
	movq	8(%rsp), %rcx           # 8-byte Reload
.LBB1_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r12), %rax
	.p2align	4, 0x90
.LBB1_14:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rbp)
	addq	$4, %rbp
	testl	%edx, %edx
	jne	.LBB1_14
# BB#15:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r12), %eax
	movl	%eax, 8(%r14)
.LBB1_16:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	movl	$0, 8(%r15)
	movq	(%r15), %rax
	movl	$0, (%rax)
	movl	$0, 8(%rcx)
	movq	(%rcx), %rax
	movl	$0, (%rax)
.LBB1_17:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_34:
	leaq	24(%rsp), %rbp
.LBB1_37:                               # %._crit_edge16.i.i33
	movq	%r14, (%r15)
	movl	$0, (%r14,%rax,4)
	movl	$2, 12(%r15)
	movq	%r14, %rbx
.LBB1_38:                               # %_ZN11CStringBaseIwEaSEw.exit
	movl	$46, (%rbx)
	movl	$0, 4(%rbx)
	movl	$1, 8(%r15)
	incl	%r13d
	movl	8(%r12), %ecx
	subl	%r13d, %ecx
	movq	%rbp, %rdi
	movq	%r12, %rsi
	movl	%r13d, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	8(%rsp), %rdx           # 8-byte Reload
	cmpq	%rdx, %rbp
	je	.LBB1_39
# BB#40:
	movl	$0, 8(%rdx)
	movq	(%rdx), %rbx
	movl	$0, (%rbx)
	movslq	32(%rsp), %rbp
	incq	%rbp
	movl	12(%rdx), %r15d
	cmpl	%r15d, %ebp
	je	.LBB1_47
# BB#41:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp3:
	callq	_Znam
	movq	%rax, %r14
.Ltmp4:
# BB#42:                                # %.noexc44
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB1_43
# BB#44:                                # %.noexc44
	testl	%r15d, %r15d
	movq	8(%rsp), %rdx           # 8-byte Reload
	jle	.LBB1_46
# BB#45:                                # %._crit_edge.thread.i.i38
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	8(%rsp), %rdx           # 8-byte Reload
	movslq	8(%rdx), %rax
	jmp	.LBB1_46
.LBB1_39:                               # %_ZN11CStringBaseIwEaSEw.exit._ZN11CStringBaseIwEaSERKS0_.exit45_crit_edge
	movq	(%rdx), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_51
	jmp	.LBB1_17
.LBB1_43:
	movq	8(%rsp), %rdx           # 8-byte Reload
.LBB1_46:                               # %._crit_edge16.i.i39
	movq	%r14, (%rdx)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 12(%rdx)
	movq	%r14, %rbx
.LBB1_47:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i40
	movq	24(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_48:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_48
# BB#49:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i43
	movl	32(%rsp), %eax
	movl	%eax, 8(%rdx)
	testq	%rdi, %rdi
	je	.LBB1_17
.LBB1_51:
	callq	_ZdaPv
	jmp	.LBB1_17
.LBB1_53:
.Ltmp5:
	jmp	.LBB1_54
.LBB1_52:
.Ltmp2:
.LBB1_54:
	movq	%rax, %rbx
	movq	24(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB1_56
# BB#55:
	callq	_ZdaPv
.LBB1_56:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end1:
	.size	_ZN8NWindows5NFile5NName31SplitNameToPureNameAndExtensionERK11CStringBaseIwERS3_S6_S6_, .Lfunc_end1-_ZN8NWindows5NFile5NName31SplitNameToPureNameAndExtensionERK11CStringBaseIwERS3_S6_S6_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Lfunc_end1-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
.Lcfi20:
	.cfi_offset %rbx, -48
.Lcfi21:
	.cfi_offset %r12, -40
.Lcfi22:
	.cfi_offset %r14, -32
.Lcfi23:
	.cfi_offset %r15, -24
.Lcfi24:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB2_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB2_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB2_3:                                # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB2_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB2_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB2_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB2_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB2_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB2_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB2_17
.LBB2_7:
	xorl	%ecx, %ecx
.LBB2_8:                                # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB2_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB2_10:                               # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB2_10
.LBB2_11:                               # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB2_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB2_13:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB2_13
	jmp	.LBB2_26
.LBB2_25:                               # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB2_27
.LBB2_26:                               # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB2_27:                               # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB2_28:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB2_17:                               # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB2_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_20:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB2_20
	jmp	.LBB2_21
.LBB2_18:
	xorl	%ebx, %ebx
.LBB2_21:                               # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB2_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB2_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB2_23
.LBB2_24:                               # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB2_8
	jmp	.LBB2_26
.Lfunc_end2:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end2-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbp
.Lcfi25:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi28:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 64
.Lcfi32:
	.cfi_offset %rbx, -56
.Lcfi33:
	.cfi_offset %r12, -48
.Lcfi34:
	.cfi_offset %r13, -40
.Lcfi35:
	.cfi_offset %r14, -32
.Lcfi36:
	.cfi_offset %r15, -24
.Lcfi37:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB3_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB3_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB3_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB3_5
.LBB3_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB3_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp6:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp7:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB3_35
# BB#12:
	movq	%rbx, %r13
.LBB3_13:                               # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB3_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB3_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB3_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB3_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB3_15
.LBB3_14:
	xorl	%esi, %esi
.LBB3_15:                               # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB3_16:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB3_16
.LBB3_29:
	movq	%r13, %rbx
.LBB3_30:                               # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp8:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp9:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB3_32:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB3_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB3_8
.LBB3_3:
	xorl	%eax, %eax
.LBB3_5:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB3_6:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB3_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB3_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB3_30
.LBB3_21:                               # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB3_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_24:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB3_24
	jmp	.LBB3_25
.LBB3_22:
	xorl	%ecx, %ecx
.LBB3_25:                               # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB3_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB3_27:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB3_27
.LBB3_28:                               # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB3_15
	jmp	.LBB3_29
.LBB3_34:                               # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp10:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end3-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp6-.Lfunc_begin1    #   Call between .Lfunc_begin1 and .Ltmp6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp6           #   Call between .Ltmp6 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Lfunc_end3-.Ltmp9      #   Call between .Ltmp9 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
