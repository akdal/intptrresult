	.text
	.file	"volume.bc"
	.globl	volume
	.p2align	4, 0x90
	.type	volume,@function
volume:                                 # @volume
	.cfi_startproc
# BB#0:
	movss	24(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	28(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	8(%rsp), %xmm0
	subss	12(%rsp), %xmm1
	mulss	%xmm0, %xmm1
	movss	32(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	16(%rsp), %xmm2
	mulss	%xmm1, %xmm2
	movss	36(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	subss	20(%rsp), %xmm0
	mulss	%xmm2, %xmm0
	retq
.Lfunc_end0:
	.size	volume, .Lfunc_end0-volume
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
