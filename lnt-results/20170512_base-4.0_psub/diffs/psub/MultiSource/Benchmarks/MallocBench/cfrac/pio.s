	.text
	.file	"pio.bc"
	.globl	fputp
	.p2align	4, 0x90
	.type	fputp,@function
fputp:                                  # @fputp
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -48
.Lcfi6:
	.cfi_offset %r12, -40
.Lcfi7:
	.cfi_offset %r13, -32
.Lcfi8:
	.cfi_offset %r14, -24
.Lcfi9:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	testq	%r14, %r14
	je	.LBB0_2
# BB#1:
	incw	(%r14)
.LBB0_2:
	movq	%r14, %rdi
	callq	ptoa
	movq	%rax, %r15
	xorl	%r13d, %r13d
	testq	%r15, %r15
	je	.LBB0_8
# BB#3:
	movb	(%r15), %cl
	testb	%cl, %cl
	je	.LBB0_8
# BB#4:                                 # %.preheader.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%cl, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	leaq	1(%rbx), %r13
	cmpl	$-1, %eax
	je	.LBB0_7
# BB#6:                                 # %.preheader.i
                                        #   in Loop: Header=BB0_5 Depth=1
	movzbl	1(%r15,%rbx), %ecx
	testb	%cl, %cl
	movq	%r13, %rbx
	jne	.LBB0_5
.LBB0_7:                                # %.loopexit.loopexit.i
	cmpl	$-1, %eax
	cmovel	%eax, %r13d
.LBB0_8:                                # %fouts.exit
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	free
	testq	%r14, %r14
	je	.LBB0_11
# BB#9:
	decw	(%r14)
	jne	.LBB0_11
# BB#10:
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	pfree
.LBB0_11:
	movl	%r13d, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	fputp, .Lfunc_end0-fputp
	.cfi_endproc

	.globl	putp
	.p2align	4, 0x90
	.type	putp,@function
putp:                                   # @putp
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi12:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi14:
	.cfi_def_cfa_offset 48
.Lcfi15:
	.cfi_offset %rbx, -40
.Lcfi16:
	.cfi_offset %r12, -32
.Lcfi17:
	.cfi_offset %r14, -24
.Lcfi18:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	testq	%r15, %r15
	je	.LBB1_2
# BB#1:
	incw	(%r15)
.LBB1_2:
	movq	%r15, %rdi
	callq	ptoa
	movq	%rax, %r14
	movq	stdout(%rip), %r12
	testq	%r14, %r14
	je	.LBB1_8
# BB#3:
	movb	(%r14), %al
	testb	%al, %al
	je	.LBB1_8
# BB#4:                                 # %.preheader.i.preheader
	movq	%r14, %rbx
	incq	%rbx
	.p2align	4, 0x90
.LBB1_5:                                # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%al, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	cmpl	$-1, %eax
	je	.LBB1_7
# BB#6:                                 # %.preheader.i
                                        #   in Loop: Header=BB1_5 Depth=1
	movzbl	(%rbx), %eax
	incq	%rbx
	testb	%al, %al
	jne	.LBB1_5
.LBB1_7:                                # %fouts.exit.loopexit
	movq	stdout(%rip), %r12
.LBB1_8:                                # %fouts.exit
	movl	$10, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	movl	%eax, %ebx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	free
	testq	%r15, %r15
	je	.LBB1_11
# BB#9:
	decw	(%r15)
	jne	.LBB1_11
# BB#10:
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	pfree
.LBB1_11:
	movl	%ebx, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	putp, .Lfunc_end1-putp
	.cfi_endproc

	.globl	fprintp
	.p2align	4, 0x90
	.type	fprintp,@function
fprintp:                                # @fprintp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 64
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %rbx
	movq	%rdi, %r12
	testq	%rbx, %rbx
	je	.LBB2_2
# BB#1:
	incw	(%rbx)
.LBB2_2:
	movq	%rbx, %rdi
	callq	ptoa
	movq	%rax, %r15
	movq	%r15, %rdi
	callq	strlen
	movq	%rax, %r13
	testl	%ebp, %ebp
	js	.LBB2_11
# BB#3:                                 # %.preheader
	cmpl	%ebp, %r13d
	jge	.LBB2_5
	.p2align	4, 0x90
.LBB2_4:                                # %.lr.ph37
                                        # =>This Inner Loop Header: Depth=1
	decl	%ebp
	movl	$32, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	cmpl	%r13d, %ebp
	jg	.LBB2_4
.LBB2_5:                                # %._crit_edge
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB2_20
# BB#6:
	movb	(%r15), %cl
	testb	%cl, %cl
	je	.LBB2_20
# BB#7:                                 # %.preheader.i26.preheader
	movq	%rbx, %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_8:                                # %.preheader.i26
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%cl, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	leaq	1(%rbx), %r14
	cmpl	$-1, %eax
	je	.LBB2_10
# BB#9:                                 # %.preheader.i26
                                        #   in Loop: Header=BB2_8 Depth=1
	movzbl	1(%r15,%rbx), %ecx
	testb	%cl, %cl
	movq	%r14, %rbx
	jne	.LBB2_8
.LBB2_10:                               # %.loopexit.loopexit.i29
	cmpl	$-1, %eax
	cmovel	%eax, %r14d
	movq	%rbp, %rbx
	jmp	.LBB2_20
.LBB2_11:
	xorl	%r14d, %r14d
	testq	%r15, %r15
	je	.LBB2_17
# BB#12:
	movb	(%r15), %cl
	testb	%cl, %cl
	je	.LBB2_17
# BB#13:                                # %.preheader.i.preheader
	movq	%rbx, (%rsp)            # 8-byte Spill
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_14:                               # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movsbl	%cl, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	leaq	1(%rbx), %r14
	cmpl	$-1, %eax
	je	.LBB2_16
# BB#15:                                # %.preheader.i
                                        #   in Loop: Header=BB2_14 Depth=1
	movzbl	1(%r15,%rbx), %ecx
	testb	%cl, %cl
	movq	%r14, %rbx
	jne	.LBB2_14
.LBB2_16:                               # %.loopexit.loopexit.i
	cmpl	$-1, %eax
	cmovel	%eax, %r14d
	movq	(%rsp), %rbx            # 8-byte Reload
.LBB2_17:                               # %fouts.exit
	movl	%r13d, %eax
	negl	%eax
	cmpl	%eax, %ebp
	jge	.LBB2_20
# BB#18:                                # %.lr.ph.preheader
	negl	%ebp
	subl	%r13d, %ebp
	.p2align	4, 0x90
.LBB2_19:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	$32, %edi
	movq	%r12, %rsi
	callq	_IO_putc
	decl	%ebp
	jne	.LBB2_19
.LBB2_20:                               # %fouts.exit31
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	free
	testq	%rbx, %rbx
	je	.LBB2_23
# BB#21:
	decw	(%rbx)
	jne	.LBB2_23
# BB#22:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	pfree
.LBB2_23:
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	fprintp, .Lfunc_end2-fprintp
	.cfi_endproc

	.globl	fgetp
	.p2align	4, 0x90
	.type	fgetp,@function
fgetp:                                  # @fgetp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi36:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi38:
	.cfi_def_cfa_offset 80
.Lcfi39:
	.cfi_offset %rbx, -56
.Lcfi40:
	.cfi_offset %r12, -48
.Lcfi41:
	.cfi_offset %r13, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	$0, (%rsp)
	movq	$0, 8(%rsp)
	callq	_IO_getc
	movl	%eax, %ebx
	cmpl	$-1, %ebx
	je	.LBB3_22
# BB#1:                                 # %.preheader42
	callq	__ctype_b_loc
	movq	%rax, %r15
	jmp	.LBB3_3
	.p2align	4, 0x90
.LBB3_2:                                # %.lr.ph51
                                        #   in Loop: Header=BB3_3 Depth=1
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
.LBB3_3:                                # %.lr.ph51
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movslq	%ebx, %rcx
	testb	$32, 1(%rax,%rcx,2)
	jne	.LBB3_2
# BB#4:                                 # %._crit_edge52
	xorl	%ebp, %ebp
	cmpl	$43, %ebx
	je	.LBB3_7
# BB#5:                                 # %._crit_edge52
	cmpl	$45, %ebx
	jne	.LBB3_8
# BB#6:
	movl	$1, %ebp
.LBB3_7:                                # %.sink.split
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movq	(%r15), %rax
.LBB3_8:
	movslq	%ebx, %rcx
	testb	$8, 1(%rax,%rcx,2)
	jne	.LBB3_9
# BB#20:
	cmpl	$-1, %ebx
	je	.LBB3_22
# BB#21:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	ungetc
	jmp	.LBB3_23
.LBB3_22:
	movq	$0, (%rsp)
	jmp	.LBB3_23
.LBB3_9:
	movl	%ebp, 20(%rsp)          # 4-byte Spill
	movq	pzero(%rip), %rsi
	movq	%rsp, %r13
	movq	%r13, %rdi
	callq	psetq
	movl	$1000000000, %edi       # imm = 0x3B9ACA00
	callq	utop
	leaq	8(%rsp), %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB3_10:                               # =>This Inner Loop Header: Depth=1
	addl	$-48, %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_11
# BB#12:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_13
# BB#30:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_31
# BB#32:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_33
# BB#34:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_35
# BB#36:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_37
# BB#38:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_39
# BB#40:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebx
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %r12d
	movslq	%r12d, %rax
	testb	$8, 1(%rbp,%rax,2)
	je	.LBB3_41
# BB#42:                                #   in Loop: Header=BB3_10 Depth=1
	leal	(%rbx,%rbx,4), %eax
	leal	-48(%r12,%rax,2), %ebp
	movq	(%rsp), %rdi
	movq	8(%rsp), %rsi
	callq	pmul
	movq	%rax, %rbx
	movl	%ebp, %edi
	callq	utop
	movq	%rbx, %rdi
	movq	%rax, %rsi
	callq	padd
	movq	%r13, %rdi
	movq	%rax, %rsi
	callq	psetq
	movq	(%r15), %rbp
	movq	%r14, %rdi
	callq	_IO_getc
	movl	%eax, %ebx
	movslq	%ebx, %rax
	testb	$8, 1(%rbp,%rax,2)
	jne	.LBB3_10
	jmp	.LBB3_16
.LBB3_11:
	movl	$10, %edi
	jmp	.LBB3_15
.LBB3_13:
	movl	$8, %eax
	jmp	.LBB3_14
.LBB3_31:
	movl	$7, %eax
	jmp	.LBB3_14
.LBB3_33:
	movl	$6, %eax
	jmp	.LBB3_14
.LBB3_35:
	movl	$5, %eax
	jmp	.LBB3_14
.LBB3_37:
	movl	$4, %eax
	jmp	.LBB3_14
.LBB3_39:
	movl	$3, %eax
	jmp	.LBB3_14
.LBB3_41:
	movl	$2, %eax
.LBB3_14:                               # %.lr.ph
	movl	$100, %edi
	cmpl	$8, %eax
	je	.LBB3_15
# BB#43:                                # %.lr.ph.1
	movl	$1000, %edi             # imm = 0x3E8
	cmpl	$7, %eax
	je	.LBB3_15
# BB#44:                                # %.lr.ph.2
	movl	$10000, %edi            # imm = 0x2710
	cmpl	$6, %eax
	je	.LBB3_15
# BB#45:                                # %.lr.ph.3
	movl	$100000, %edi           # imm = 0x186A0
	cmpl	$5, %eax
	je	.LBB3_15
# BB#46:                                # %.lr.ph.4
	movl	$1000000, %edi          # imm = 0xF4240
	cmpl	$4, %eax
	je	.LBB3_15
# BB#47:                                # %.lr.ph.5
	movl	$10000000, %edi         # imm = 0x989680
	cmpl	$3, %eax
	je	.LBB3_15
# BB#48:                                # %.lr.ph.6
	movl	$100000000, %edi        # imm = 0x5F5E100
	cmpl	$2, %eax
	je	.LBB3_15
# BB#49:                                # %.lr.ph.7
	movl	$1000000000, %edi       # imm = 0x3B9ACA00
.LBB3_15:                               # %._crit_edge
	movq	(%rsp), %r15
	callq	utop
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	pmul
	movq	%rax, %r15
	movl	%ebx, %edi
	callq	utop
	movq	%r15, %rdi
	movq	%rax, %rsi
	callq	padd
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	psetq
	movl	%r12d, %ebx
.LBB3_16:                               # %.loopexit
	cmpl	$-1, %ebx
	je	.LBB3_18
# BB#17:
	movl	%ebx, %edi
	movq	%r14, %rsi
	callq	ungetc
.LBB3_18:
	cmpl	$0, 20(%rsp)            # 4-byte Folded Reload
	je	.LBB3_23
# BB#19:
	movq	(%rsp), %rdi
	callq	pneg
	movq	%rsp, %rdi
	movq	%rax, %rsi
	callq	psetq
.LBB3_23:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_26
# BB#24:
	decw	(%rdi)
	jne	.LBB3_26
# BB#25:
	xorl	%eax, %eax
	callq	pfree
.LBB3_26:
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB3_27
# BB#28:
	callq	presult
	jmp	.LBB3_29
.LBB3_27:
	xorl	%eax, %eax
.LBB3_29:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end3:
	.size	fgetp, .Lfunc_end3-fgetp
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
