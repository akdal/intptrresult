	.text
	.file	"ag_enc.bc"
	.globl	dyn_comp
	.p2align	4, 0x90
	.type	dyn_comp,@function
dyn_comp:                               # @dyn_comp
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 176
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, %r15d
	movq	%rsi, %rbp
	movl	24(%rdi), %ebx
	movl	28(%rdi), %r11d
	movl	$0, (%r9)
	leal	-1(%r8), %ecx
	movl	$-50, %eax
	cmpl	$31, %ecx
	ja	.LBB0_54
# BB#1:
	movq	%r8, 48(%rsp)           # 8-byte Spill
	movq	%r9, 96(%rsp)           # 8-byte Spill
	movq	(%rdx), %r8
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	movl	16(%rdx), %eax
	movl	4(%rdi), %r10d
	movl	%r10d, (%rdi)
	testl	%r15d, %r15d
	movl	%eax, 36(%rsp)          # 4-byte Spill
	movl	%eax, %esi
	movq	%rbp, %r9
	je	.LBB0_52
# BB#2:                                 # %.lr.ph
	subl	%r11d, %ebx
	movl	8(%rdi), %eax
	movl	%eax, 68(%rsp)          # 4-byte Spill
	movl	12(%rdi), %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	16(%rdi), %eax
	movl	%eax, 60(%rsp)          # 4-byte Spill
	movslq	%ebx, %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	movl	$32, %ecx
	subl	48(%rsp), %ecx          # 4-byte Folded Reload
	movl	$-1, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %eax
	movl	%eax, 56(%rsp)          # 4-byte Spill
	movl	36(%rsp), %ecx          # 4-byte Reload
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	xorl	%ebp, %ebp
                                        # implicit-def: %EAX
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%r15d, 40(%rsp)         # 4-byte Spill
	movl	%r11d, 72(%rsp)         # 4-byte Spill
	movl	%ecx, %r11d
	movq	%r8, 80(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB0_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_4 Depth 2
                                        #     Child Loop BB0_27 Depth 2
                                        #     Child Loop BB0_39 Depth 2
	movl	%r10d, %ecx
	shrl	$9, %ecx
	addl	$3, %ecx
	movl	$3, %edx
	movl	$2147483648, %esi       # imm = 0x80000000
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_4:                                #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rsi
	jne	.LBB0_12
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=2
	movq	%rsi, %rdi
	shrq	%rdi
	testq	%rcx, %rdi
	jne	.LBB0_9
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=2
	movq	%rsi, %rdi
	shrq	$2, %rdi
	testq	%rcx, %rdi
	jne	.LBB0_10
# BB#7:                                 #   in Loop: Header=BB0_4 Depth=2
	movq	%rsi, %rdi
	shrq	$3, %rdi
	testq	%rcx, %rdi
	jne	.LBB0_11
# BB#8:                                 #   in Loop: Header=BB0_4 Depth=2
	shrq	$4, %rsi
	addq	$4, %rax
	leaq	4(%rdx), %rdi
	incq	%rdx
	cmpq	$32, %rdx
	movq	%rdi, %rdx
	jl	.LBB0_4
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_3 Depth=1
	orq	$1, %rax
	jmp	.LBB0_12
	.p2align	4, 0x90
.LBB0_10:                               #   in Loop: Header=BB0_3 Depth=1
	orq	$2, %rax
	jmp	.LBB0_12
.LBB0_11:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB0_12:                               # %lg3a.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	$31, %ecx
	subl	%eax, %ecx
	movl	64(%rsp), %eax          # 4-byte Reload
	cmpl	%eax, %ecx
	cmoval	%eax, %ecx
	movl	$1, %esi
	shll	%cl, %esi
	decl	%esi
	movl	(%r9), %eax
	movl	%eax, %edx
	sarl	$31, %edx
	movl	%edx, %r15d
	xorl	%eax, %r15d
	subl	%edx, %r15d
	addl	%r15d, %r15d
	shrl	$31, %eax
	subl	%eax, %r15d
	movl	%r15d, %ebx
	subl	%ebp, %ebx
	xorl	%edx, %edx
	movl	%ebx, %eax
	divl	%esi
	cmpl	$8, %eax
	movq	%r10, 24(%rsp)          # 8-byte Spill
	movl	%ebx, 76(%rsp)          # 4-byte Spill
	ja	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_3 Depth=1
	imull	%eax, %esi
	movl	%ebx, %edx
	subl	%esi, %edx
	movl	$0, %esi
	movl	$-1, %edi
	cmovel	%edi, %esi
	addl	%eax, %ecx
	leal	1(%rsi,%rcx), %ebp
	movl	$1, %edi
	movl	%eax, %ecx
	shll	%cl, %edi
	decl	%edi
	movl	%ebp, %ecx
	subl	%eax, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	cmpl	$26, %ebp
	jb	.LBB0_15
.LBB0_14:                               #   in Loop: Header=BB0_3 Depth=1
	movl	$511, %r14d             # imm = 0x1FF
	movl	$9, %ebp
	movl	$0, 44(%rsp)            # 4-byte Folded Spill
	movl	%ebx, 20(%rsp)          # 4-byte Spill
	jmp	.LBB0_16
	.p2align	4, 0x90
.LBB0_15:                               #   in Loop: Header=BB0_3 Depth=1
	addl	%esi, %edx
	leal	1(%rdi,%rdx), %r14d
	movb	$1, %al
	movl	%eax, 44(%rsp)          # 4-byte Spill
.LBB0_16:                               # %dyn_code_32bit.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	addq	$4, %r9
	movq	%r9, 112(%rsp)          # 8-byte Spill
	incl	%r13d
	movl	%r11d, %r12d
	shrl	$3, %r12d
	movl	(%r8,%r12), %edi
	movl	%r11d, %ebx
	callq	Swap32NtoB
	movl	%ebx, %ecx
	andl	$7, %ecx
	movl	$32, %edx
	subl	%ecx, %edx
	subl	%ebp, %edx
	movl	$32, %ecx
	subl	%ebp, %ecx
	movl	$-1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movl	%esi, %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	andl	%r14d, %esi
	shll	%cl, %esi
	notl	%edi
	andl	%eax, %edi
	orl	%esi, %edi
	callq	Swap32BtoN
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	%eax, (%r8,%r12)
	addl	%ebx, %ebp
	cmpb	$0, 44(%rsp)            # 1-byte Folded Reload
	je	.LBB0_18
# BB#17:                                #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, %r11d
	jmp	.LBB0_22
	.p2align	4, 0x90
.LBB0_18:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%ebp, %ebx
	shrl	$3, %ebx
	movl	%ebp, %eax
	andl	$7, %eax
	movl	$32, %r12d
	subl	%eax, %r12d
	movl	(%r8,%rbx), %edi
	movq	%r8, %r14
	callq	Swap32NtoB
	subl	48(%rsp), %r12d         # 4-byte Folded Reload
	js	.LBB0_20
# BB#19:                                #   in Loop: Header=BB0_3 Depth=1
	movl	56(%rsp), %edi          # 4-byte Reload
	movl	%edi, %edx
	movl	%r12d, %ecx
	shll	%cl, %edx
	movl	20(%rsp), %esi          # 4-byte Reload
	andl	%edi, %esi
	shll	%cl, %esi
	notl	%edx
	andl	%edx, %eax
	orl	%esi, %eax
	jmp	.LBB0_21
.LBB0_20:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r12d, %ecx
	negl	%ecx
	movl	20(%rsp), %edi          # 4-byte Reload
	movl	%edi, %edx
	shrl	%cl, %edx
	movl	$-1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	notl	%esi
	andl	%esi, %eax
	orl	%edx, %eax
	addl	$8, %r12d
	movl	%edi, %edx
	movl	%r12d, %ecx
	shll	%cl, %edx
	movb	%dl, 4(%r14,%rbx)
.LBB0_21:                               # %dyn_jam_noDeref_large.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %edi
	callq	Swap32BtoN
	movl	%eax, (%r14,%rbx)
	addl	48(%rsp), %ebp          # 4-byte Folded Reload
	movl	%ebp, %r11d
	movq	%r14, %r8
.LBB0_22:                               #   in Loop: Header=BB0_3 Depth=1
	movq	8(%rsp), %rsi           # 8-byte Reload
	leal	1(%rsi), %r12d
	movl	72(%rsp), %ebx          # 4-byte Reload
	cmpl	%ebx, %r13d
	movq	104(%rsp), %rbp         # 8-byte Reload
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%rax,%rbp,4), %r14
	movl	$0, %r9d
	cmovgel	%r9d, %r13d
	cmovlq	%rax, %r14
	movl	68(%rsp), %ecx          # 4-byte Reload
	imull	%ecx, %r15d
	movq	24(%rsp), %r10          # 8-byte Reload
	movl	%r10d, %eax
	imull	%ecx, %eax
	shrl	$9, %eax
	subl	%eax, %r10d
	addl	%r15d, %r10d
	cmpl	$65535, 76(%rsp)        # 4-byte Folded Reload
                                        # imm = 0xFFFF
	movl	$65535, %eax            # imm = 0xFFFF
	cmoval	%eax, %r10d
	movl	40(%rsp), %edi          # 4-byte Reload
	cmpl	%edi, %r12d
	ja	.LBB0_53
# BB#23:                                #   in Loop: Header=BB0_3 Depth=1
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	cmpl	%edi, %r12d
	jae	.LBB0_34
# BB#24:                                #   in Loop: Header=BB0_3 Depth=1
	leal	(,%r10,4), %eax
	cmpl	$511, %eax              # imm = 0x1FF
	ja	.LBB0_34
# BB#25:                                # %.preheader.preheader
                                        #   in Loop: Header=BB0_3 Depth=1
	xorl	%eax, %eax
	movl	$1, 24(%rsp)            # 4-byte Folded Spill
	movl	$1, %r15d
	jmp	.LBB0_27
	.p2align	4, 0x90
.LBB0_26:                               #   in Loop: Header=BB0_27 Depth=2
	addl	$2, %eax
	addl	$2, %r12d
	incl	%r13d
	cmpl	%ebx, %r13d
	leaq	4(%r14,%rbp,4), %rcx
	leaq	4(%r14), %r14
	cmovgel	%r9d, %r13d
	cmovgeq	%rcx, %r14
	addl	$2, %r15d
	movq	8(%rsp), %rsi           # 8-byte Reload
.LBB0_27:                               # %.preheader
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%r15), %ecx
	cmpl	%edi, %ecx
	jae	.LBB0_36
# BB#28:                                #   in Loop: Header=BB0_27 Depth=2
	cmpl	$0, (%r14)
	jne	.LBB0_36
# BB#29:                                #   in Loop: Header=BB0_27 Depth=2
	incl	%r13d
	cmpl	%ebx, %r13d
	leaq	4(%r14,%rbp,4), %rcx
	leaq	4(%r14), %r14
	cmovgel	%r9d, %r13d
	cmovgeq	%rcx, %r14
	cmpl	$65535, %r15d           # imm = 0xFFFF
	jae	.LBB0_37
# BB#30:                                # %.preheader.1
                                        #   in Loop: Header=BB0_27 Depth=2
	movl	%eax, %edx
	orl	$1, %edx
	leal	1(%r12), %ecx
	leal	1(%rsi,%r15), %esi
	cmpl	%edi, %esi
	jae	.LBB0_32
# BB#31:                                #   in Loop: Header=BB0_27 Depth=2
	cmpl	$0, (%r14)
	je	.LBB0_26
.LBB0_32:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%edx, %r15d
	movl	%ecx, %r12d
	jmp	.LBB0_38
	.p2align	4, 0x90
.LBB0_34:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r11d, %ebp
	jmp	.LBB0_51
	.p2align	4, 0x90
.LBB0_36:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%eax, %r15d
	jmp	.LBB0_38
.LBB0_37:                               # %..critedge_crit_edge
                                        #   in Loop: Header=BB0_3 Depth=1
	leal	1(%rsi,%r15), %r12d
	movl	$0, 24(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB0_38:                               # %.critedge
                                        #   in Loop: Header=BB0_3 Depth=1
	movslq	%r10d, %rcx
	movl	$3, %edx
	movl	$2147483648, %esi       # imm = 0x80000000
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB0_39:                               #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testq	%rcx, %rsi
	jne	.LBB0_47
# BB#40:                                #   in Loop: Header=BB0_39 Depth=2
	movq	%rsi, %rdi
	shrq	%rdi
	testq	%rcx, %rdi
	jne	.LBB0_44
# BB#41:                                #   in Loop: Header=BB0_39 Depth=2
	movq	%rsi, %rdi
	shrq	$2, %rdi
	testq	%rcx, %rdi
	jne	.LBB0_45
# BB#42:                                #   in Loop: Header=BB0_39 Depth=2
	movq	%rsi, %rdi
	shrq	$3, %rdi
	testq	%rcx, %rdi
	jne	.LBB0_46
# BB#43:                                #   in Loop: Header=BB0_39 Depth=2
	shrq	$4, %rsi
	addq	$4, %rax
	leaq	4(%rdx), %rdi
	incq	%rdx
	cmpq	$32, %rdx
	movq	%rdi, %rdx
	jl	.LBB0_39
	jmp	.LBB0_47
.LBB0_44:                               #   in Loop: Header=BB0_3 Depth=1
	orq	$1, %rax
	jmp	.LBB0_47
.LBB0_45:                               #   in Loop: Header=BB0_3 Depth=1
	orq	$2, %rax
	jmp	.LBB0_47
.LBB0_46:                               #   in Loop: Header=BB0_3 Depth=1
	movq	%rdx, %rax
	.p2align	4, 0x90
.LBB0_47:                               # %lead.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	addl	$16, %r10d
	shrl	$6, %r10d
	leal	-24(%r10,%rax), %ecx
	movl	$1, %edi
	shll	%cl, %edi
	decl	%edi
	andl	60(%rsp), %edi          # 4-byte Folded Reload
	movl	%r15d, %eax
	cltd
	idivl	%edi
	movl	%eax, %esi
	cmpl	$9, %esi
	jb	.LBB0_49
# BB#48:                                #   in Loop: Header=BB0_3 Depth=1
	addl	$33488896, %r15d        # imm = 0x1FF0000
	movl	$25, 8(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_50
	.p2align	4, 0x90
.LBB0_49:                               #   in Loop: Header=BB0_3 Depth=1
	movl	%r15d, %eax
	cltd
	idivl	%edi
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	cmpl	$1, %edx
	sbbl	%eax, %eax
	addl	%esi, %ecx
	leal	1(%rax,%rcx), %ebp
	movl	$1, %edi
	movl	%esi, %ecx
	shll	%cl, %edi
	decl	%edi
	movl	%ebp, %ecx
	subl	%esi, %ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edi
	addl	%eax, %edx
	leal	1(%rdi,%rdx), %eax
	addl	$33488896, %r15d        # imm = 0x1FF0000
	cmpl	$25, %ebp
	movl	$25, %ecx
	cmoval	%ecx, %ebp
	movl	%ebp, 8(%rsp)           # 4-byte Spill
	cmovbel	%eax, %r15d
.LBB0_50:                               # %dyn_code.exit
                                        #   in Loop: Header=BB0_3 Depth=1
	movl	%r11d, %ebp
	movl	%ebp, %ebx
	shrl	$3, %ebx
	movl	(%r8,%rbx), %edi
	callq	Swap32NtoB
	movl	%ebp, %ecx
	andl	$7, %ecx
	movl	$32, %edx
	subl	%ecx, %edx
	movl	8(%rsp), %esi           # 4-byte Reload
	subl	%esi, %edx
	movl	$32, %ecx
	subl	%esi, %ecx
	movl	$-1, %esi
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	movl	%esi, %edi
	movl	%edx, %ecx
	shll	%cl, %edi
	andl	%r15d, %esi
	shll	%cl, %esi
	notl	%edi
	andl	%eax, %edi
	orl	%esi, %edi
	callq	Swap32BtoN
	movq	80(%rsp), %r8           # 8-byte Reload
	movl	%eax, (%r8,%rbx)
	addl	8(%rsp), %ebp           # 4-byte Folded Reload
	xorl	%r10d, %r10d
	movl	40(%rsp), %edi          # 4-byte Reload
.LBB0_51:                               # %.backedge
                                        #   in Loop: Header=BB0_3 Depth=1
	cmpl	%edi, %r12d
	movq	%r14, %r9
	movl	%ebp, %r11d
                                        # kill: %R12D<def> %R12D<kill> %R12<kill> %R12<def>
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %esi
	movl	24(%rsp), %ebp          # 4-byte Reload
	jb	.LBB0_3
.LBB0_52:                               # %._crit_edge
	subl	36(%rsp), %esi          # 4-byte Folded Reload
	movq	96(%rsp), %rax          # 8-byte Reload
	movl	%esi, (%rax)
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	BitBufferAdvance
	xorl	%eax, %eax
	jmp	.LBB0_54
.LBB0_53:
	movl	$-50, %eax
.LBB0_54:                               # %.loopexit
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	dyn_comp, .Lfunc_end0-dyn_comp
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
