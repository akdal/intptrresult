	.text
	.file	"btCollisionWorld.bc"
	.globl	_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration,@function
_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration: # @_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV16btCollisionWorld+16, (%rbx)
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movl	$0, 12(%rbx)
	movl	$0, 16(%rbx)
	movq	%rsi, 40(%rbx)
	movl	$0, 48(%rbx)
	movl	$0, 52(%rbx)
	movl	$1, 56(%rbx)
	movl	$1065353216, 60(%rbx)   # imm = 0x3F800000
	movb	$0, 64(%rbx)
	movq	$0, 72(%rbx)
	movb	$0, 80(%rbx)
	movb	$1, 81(%rbx)
	movb	$1, 82(%rbx)
	movl	$1025758986, 84(%rbx)   # imm = 0x3D23D70A
	movb	$0, 88(%rbx)
	movl	$0, 92(%rbx)
	movq	$0, 96(%rbx)
	movq	%rdx, 112(%rbx)
	movq	$0, 120(%rbx)
	movb	$1, 128(%rbx)
	movq	(%rcx), %rax
.Ltmp0:
	movq	%rcx, %rdi
	callq	*32(%rax)
.Ltmp1:
# BB#1:
	movq	%rax, 104(%rbx)
	movq	%rax, 96(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#3:
	cmpb	$0, 32(%rbx)
	je	.LBB0_5
# BB#4:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB0_5:                                # %.noexc
	movq	$0, 24(%rbx)
.LBB0_6:
	movb	$1, 32(%rbx)
	movq	$0, 24(%rbx)
	movq	$0, 12(%rbx)
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_7:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration, .Lfunc_end0-_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\257\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.text
	.globl	_ZN16btCollisionWorldD2Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorldD2Ev,@function
_ZN16btCollisionWorldD2Ev:              # @_ZN16btCollisionWorldD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -40
.Lcfi11:
	.cfi_offset %r12, -32
.Lcfi12:
	.cfi_offset %r14, -24
.Lcfi13:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	$_ZTV16btCollisionWorld+16, (%r14)
	movl	12(%r14), %eax
	movq	24(%r14), %rdi
	testl	%eax, %eax
	jle	.LBB2_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rdi,%rbx,8), %r12
	movq	192(%r12), %r15
	testq	%r15, %r15
	je	.LBB2_7
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	112(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp6:
	callq	*64(%rax)
.Ltmp7:
# BB#4:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	(%rax), %rcx
	movq	40(%r14), %rdx
.Ltmp8:
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	*80(%rcx)
.Ltmp9:
# BB#5:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	40(%r14), %rdx
	movq	112(%r14), %rdi
	movq	(%rdi), %rax
.Ltmp10:
	movq	%r15, %rsi
	callq	*24(%rax)
.Ltmp11:
# BB#6:                                 #   in Loop: Header=BB2_2 Depth=1
	movq	$0, 192(%r12)
	movl	12(%r14), %eax
	movq	24(%r14), %rdi
.LBB2_7:                                #   in Loop: Header=BB2_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB2_2
.LBB2_8:                                # %._crit_edge
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#9:
	cmpb	$0, 32(%r14)
	je	.LBB2_11
# BB#10:
	callq	_Z21btAlignedFreeInternalPv
.LBB2_11:
	movq	$0, 24(%r14)
.LBB2_12:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectED2Ev.exit
	movb	$1, 32(%r14)
	movq	$0, 24(%r14)
	movq	$0, 12(%r14)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_13:
.Ltmp12:
	movq	%rax, %rbx
	movq	24(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_17
# BB#14:
	cmpb	$0, 32(%r14)
	je	.LBB2_16
# BB#15:
.Ltmp13:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp14:
.LBB2_16:                               # %.noexc
	movq	$0, 24(%r14)
.LBB2_17:
	movb	$1, 32(%r14)
	movq	$0, 24(%r14)
	movq	$0, 12(%r14)
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB2_18:
.Ltmp15:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZN16btCollisionWorldD2Ev, .Lfunc_end2-_ZN16btCollisionWorldD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp11-.Ltmp6          #   Call between .Ltmp6 and .Ltmp11
	.long	.Ltmp12-.Lfunc_begin1   #     jumps to .Ltmp12
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp11         #   Call between .Ltmp11 and .Ltmp13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp14-.Ltmp13         #   Call between .Ltmp13 and .Ltmp14
	.long	.Ltmp15-.Lfunc_begin1   #     jumps to .Ltmp15
	.byte	1                       #   On action: 1
	.long	.Ltmp14-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp14     #   Call between .Ltmp14 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btCollisionWorldD0Ev
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorldD0Ev,@function
_ZN16btCollisionWorldD0Ev:              # @_ZN16btCollisionWorldD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 32
.Lcfi17:
	.cfi_offset %rbx, -24
.Lcfi18:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp16:
	callq	_ZN16btCollisionWorldD2Ev
.Ltmp17:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB3_2:
.Ltmp18:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end3:
	.size	_ZN16btCollisionWorldD0Ev, .Lfunc_end3-_ZN16btCollisionWorldD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp17-.Ltmp16         #   Call between .Ltmp16 and .Ltmp17
	.long	.Ltmp18-.Lfunc_begin2   #     jumps to .Ltmp18
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Lfunc_end3-.Ltmp17     #   Call between .Ltmp17 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.globl	_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss,@function
_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss: # @_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi25:
	.cfi_def_cfa_offset 160
.Lcfi26:
	.cfi_offset %rbx, -56
.Lcfi27:
	.cfi_offset %r12, -48
.Lcfi28:
	.cfi_offset %r13, -40
.Lcfi29:
	.cfi_offset %r14, -32
.Lcfi30:
	.cfi_offset %r15, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movl	%ecx, %ebp
	movl	%edx, %r12d
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	12(%rbx), %eax
	cmpl	16(%rbx), %eax
	jne	.LBB4_15
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r13d
	cmovnel	%ecx, %r13d
	cmpl	%r13d, %eax
	jge	.LBB4_15
# BB#2:
	movl	%ebp, %r15d
	testl	%r13d, %r13d
	je	.LBB4_3
# BB#4:
	movslq	%r13d, %rdi
	shlq	$3, %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	12(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB4_6
	jmp	.LBB4_10
.LBB4_3:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB4_10
.LBB4_6:                                # %.lr.ph.i.i.i
	movslq	%eax, %rcx
	leaq	-1(%rcx), %r8
	movq	%rcx, %rdi
	xorl	%edx, %edx
	andq	$3, %rdi
	je	.LBB4_8
	.p2align	4, 0x90
.LBB4_7:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbp,%rdx,8)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB4_7
.LBB4_8:                                # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB4_10
	.p2align	4, 0x90
.LBB4_9:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rbx), %rsi
	movq	(%rsi,%rdx,8), %rsi
	movq	%rsi, (%rbp,%rdx,8)
	movq	24(%rbx), %rsi
	movq	8(%rsi,%rdx,8), %rsi
	movq	%rsi, 8(%rbp,%rdx,8)
	movq	24(%rbx), %rsi
	movq	16(%rsi,%rdx,8), %rsi
	movq	%rsi, 16(%rbp,%rdx,8)
	movq	24(%rbx), %rsi
	movq	24(%rsi,%rdx,8), %rsi
	movq	%rsi, 24(%rbp,%rdx,8)
	addq	$4, %rdx
	cmpq	%rdx, %rcx
	jne	.LBB4_9
.LBB4_10:                               # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE4copyEiiPS1_.exit.i.i
	movq	24(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB4_14
# BB#11:
	cmpb	$0, 32(%rbx)
	je	.LBB4_13
# BB#12:
	callq	_Z21btAlignedFreeInternalPv
	movl	12(%rbx), %eax
.LBB4_13:
	movq	$0, 24(%rbx)
.LBB4_14:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE10deallocateEv.exit.i.i
	movb	$1, 32(%rbx)
	movq	%rbp, 24(%rbx)
	movl	%r13d, 16(%rbx)
	movl	%r15d, %ebp
.LBB4_15:                               # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE9push_backERKS1_.exit
	movq	24(%rbx), %rcx
	cltq
	movq	%r14, (%rcx,%rax,8)
	incl	%eax
	movl	%eax, 12(%rbx)
	movups	8(%r14), %xmm0
	movaps	%xmm0, (%rsp)
	movups	24(%r14), %xmm0
	movaps	%xmm0, 16(%rsp)
	movups	40(%r14), %xmm0
	movaps	%xmm0, 32(%rsp)
	movups	56(%r14), %xmm0
	movaps	%xmm0, 48(%rsp)
	movq	200(%r14), %rdi
	movq	(%rdi), %rax
	movq	%rsp, %rsi
	leaq	88(%rsp), %r15
	leaq	72(%rsp), %r13
	movq	%r15, %rdx
	movq	%r13, %rcx
	callq	*16(%rax)
	movq	200(%r14), %rax
	movl	8(%rax), %ecx
	movq	112(%rbx), %rdi
	movq	(%rdi), %rax
	subq	$8, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	movswl	%bp, %ebp
	movswl	%r12w, %r9d
	movq	%r15, %rsi
	movq	%r13, %rdx
	movq	%r14, %r8
	pushq	$0
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rbx)
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	callq	*16(%rax)
	addq	$32, %rsp
.Lcfi36:
	.cfi_adjust_cfa_offset -32
	movq	%rax, 192(%r14)
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss, .Lfunc_end4-_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1399379109              # float 9.99999995E+11
	.text
	.globl	_ZN16btCollisionWorld16updateSingleAabbEP17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld16updateSingleAabbEP17btCollisionObject,@function
_ZN16btCollisionWorld16updateSingleAabbEP17btCollisionObject: # @_ZN16btCollisionWorld16updateSingleAabbEP17btCollisionObject
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 64
.Lcfi40:
	.cfi_offset %rbx, -24
.Lcfi41:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	200(%rbx), %rdi
	movq	(%rdi), %rax
	leaq	8(%rbx), %rsi
	leaq	24(%rsp), %rdx
	leaq	8(%rsp), %rcx
	callq	*16(%rax)
	movss	gContactBreakingThreshold(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	24(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%rsp)
	movss	28(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm2
	movss	%xmm2, 28(%rsp)
	movss	32(%rsp), %xmm5         # xmm5 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm5
	movss	%xmm5, 32(%rsp)
	movss	8(%rsp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm3
	movss	%xmm3, 8(%rsp)
	movss	12(%rsp), %xmm4         # xmm4 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm4
	movss	%xmm4, 12(%rsp)
	addss	16(%rsp), %xmm0
	movss	%xmm0, 16(%rsp)
	movq	112(%r14), %rdi
	testb	$1, 216(%rbx)
	jne	.LBB5_2
# BB#1:
	subss	%xmm1, %xmm3
	subss	%xmm2, %xmm4
	subss	%xmm5, %xmm0
	mulss	%xmm3, %xmm3
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	mulss	%xmm0, %xmm0
	addss	%xmm4, %xmm0
	movss	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB5_3
.LBB5_2:
	movq	(%rdi), %rax
	movq	192(%rbx), %rsi
	movq	40(%r14), %r8
	leaq	24(%rsp), %rdx
	leaq	8(%rsp), %rcx
	callq	*32(%rax)
.LBB5_6:
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB5_3:
	movl	$5, %esi
	movq	%rbx, %rdi
	callq	_ZN17btCollisionObject18setActivationStateEi
	movb	_ZZN16btCollisionWorld16updateSingleAabbEP17btCollisionObjectE8reportMe(%rip), %al
	testb	%al, %al
	jne	.LBB5_6
# BB#4:
	movq	120(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB5_6
# BB#5:
	movb	$1, _ZZN16btCollisionWorld16updateSingleAabbEP17btCollisionObjectE8reportMe(%rip)
	movq	(%rdi), %rax
	movl	$.L.str, %esi
	callq	*72(%rax)
	movq	120(%r14), %rdi
	movq	(%rdi), %rax
	movl	$.L.str.1, %esi
	callq	*72(%rax)
	movq	120(%r14), %rdi
	movq	(%rdi), %rax
	movl	$.L.str.2, %esi
	callq	*72(%rax)
	movq	120(%r14), %rdi
	movq	(%rdi), %rax
	movl	$.L.str.3, %esi
	callq	*72(%rax)
	jmp	.LBB5_6
.Lfunc_end5:
	.size	_ZN16btCollisionWorld16updateSingleAabbEP17btCollisionObject, .Lfunc_end5-_ZN16btCollisionWorld16updateSingleAabbEP17btCollisionObject
	.cfi_endproc

	.globl	_ZN16btCollisionWorld11updateAabbsEv
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld11updateAabbsEv,@function
_ZN16btCollisionWorld11updateAabbsEv:   # @_ZN16btCollisionWorld11updateAabbsEv
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r14
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -24
.Lcfi46:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	$.L.str.4, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.LBB6_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	jmp	.LBB6_2
	.p2align	4, 0x90
.LBB6_3:                                #   in Loop: Header=BB6_2 Depth=1
	movl	228(%rsi), %ecx
	cmpl	$2, %ecx
	je	.LBB6_7
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB6_7
	jmp	.LBB6_5
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rcx
	movq	(%rcx,%rbx,8), %rsi
	cmpb	$0, 128(%r14)
	je	.LBB6_3
.LBB6_5:                                #   in Loop: Header=BB6_2 Depth=1
.Ltmp19:
	movq	%r14, %rdi
	callq	_ZN16btCollisionWorld16updateSingleAabbEP17btCollisionObject
.Ltmp20:
# BB#6:                                 # %._crit_edge17
                                        #   in Loop: Header=BB6_2 Depth=1
	movl	12(%r14), %eax
.LBB6_7:                                #   in Loop: Header=BB6_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_2
.LBB6_8:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB6_9:
.Ltmp21:
	movq	%rax, %rbx
.Ltmp22:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp23:
# BB#10:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB6_11:
.Ltmp24:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN16btCollisionWorld11updateAabbsEv, .Lfunc_end6-_ZN16btCollisionWorld11updateAabbsEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	73                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp19-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp19
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp19-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp20-.Ltmp19         #   Call between .Ltmp19 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin3   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp22-.Ltmp20         #   Call between .Ltmp20 and .Ltmp22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp22-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp23-.Ltmp22         #   Call between .Ltmp22 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin3   #     jumps to .Ltmp24
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Lfunc_end6-.Ltmp23     #   Call between .Ltmp23 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv,@function
_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv: # @_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r15
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi49:
	.cfi_def_cfa_offset 32
.Lcfi50:
	.cfi_offset %rbx, -32
.Lcfi51:
	.cfi_offset %r14, -24
.Lcfi52:
	.cfi_offset %r15, -16
	movq	%rdi, %rbx
	movl	$.L.str.5, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movq	(%rbx), %rax
.Ltmp25:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp26:
# BB#1:
.Ltmp27:
	movl	$.L.str.6, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
.Ltmp28:
# BB#2:                                 # %_ZN14CProfileSampleC2EPKc.exit
	movq	40(%rbx), %rsi
	movq	112(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp30:
	callq	*56(%rax)
.Ltmp31:
# BB#3:
.Ltmp35:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp36:
# BB#4:                                 # %_ZN14CProfileSampleD2Ev.exit
	movq	40(%rbx), %r14
.Ltmp38:
	movl	$.L.str.7, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
.Ltmp39:
# BB#5:                                 # %_ZN14CProfileSampleC2EPKc.exit15
	testq	%r14, %r14
	je	.LBB7_8
# BB#6:
	movq	(%r14), %rax
	movq	64(%rax), %r15
	movq	112(%rbx), %rdi
	movq	(%rdi), %rax
.Ltmp41:
	callq	*64(%rax)
.Ltmp42:
# BB#7:
	movq	40(%rbx), %rcx
	addq	$48, %rbx
.Ltmp43:
	movq	%r14, %rdi
	movq	%rax, %rsi
	movq	%rbx, %rdx
	callq	*%r15
.Ltmp44:
.LBB7_8:
.Ltmp48:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp49:
# BB#9:                                 # %_ZN14CProfileSampleD2Ev.exit18
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN15CProfileManager12Stop_ProfileEv # TAILCALL
.LBB7_15:
.Ltmp50:
	jmp	.LBB7_16
.LBB7_13:
.Ltmp40:
	jmp	.LBB7_16
.LBB7_12:
.Ltmp37:
	jmp	.LBB7_16
.LBB7_11:
.Ltmp32:
	movq	%rax, %rbx
.Ltmp33:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp34:
	jmp	.LBB7_17
.LBB7_14:
.Ltmp45:
	movq	%rax, %rbx
.Ltmp46:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp47:
	jmp	.LBB7_17
.LBB7_10:
.Ltmp29:
.LBB7_16:                               # %_ZN14CProfileSampleD2Ev.exit16
	movq	%rax, %rbx
.LBB7_17:                               # %_ZN14CProfileSampleD2Ev.exit16
.Ltmp51:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp52:
# BB#18:                                # %_ZN14CProfileSampleD2Ev.exit14
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB7_19:
.Ltmp53:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end7:
	.size	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv, .Lfunc_end7-_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\213\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\202\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp25-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp28-.Ltmp25         #   Call between .Ltmp25 and .Ltmp28
	.long	.Ltmp29-.Lfunc_begin4   #     jumps to .Ltmp29
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp31-.Ltmp30         #   Call between .Ltmp30 and .Ltmp31
	.long	.Ltmp32-.Lfunc_begin4   #     jumps to .Ltmp32
	.byte	0                       #   On action: cleanup
	.long	.Ltmp35-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin4   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin4   # >> Call Site 5 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin4   #     jumps to .Ltmp40
	.byte	0                       #   On action: cleanup
	.long	.Ltmp41-.Lfunc_begin4   # >> Call Site 6 <<
	.long	.Ltmp44-.Ltmp41         #   Call between .Ltmp41 and .Ltmp44
	.long	.Ltmp45-.Lfunc_begin4   #     jumps to .Ltmp45
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin4   # >> Call Site 7 <<
	.long	.Ltmp49-.Ltmp48         #   Call between .Ltmp48 and .Ltmp49
	.long	.Ltmp50-.Lfunc_begin4   #     jumps to .Ltmp50
	.byte	0                       #   On action: cleanup
	.long	.Ltmp49-.Lfunc_begin4   # >> Call Site 8 <<
	.long	.Ltmp33-.Ltmp49         #   Call between .Ltmp49 and .Ltmp33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp33-.Lfunc_begin4   # >> Call Site 9 <<
	.long	.Ltmp52-.Ltmp33         #   Call between .Ltmp33 and .Ltmp52
	.long	.Ltmp53-.Lfunc_begin4   #     jumps to .Ltmp53
	.byte	1                       #   On action: 1
	.long	.Ltmp52-.Lfunc_begin4   # >> Call Site 10 <<
	.long	.Lfunc_end7-.Ltmp52     #   Call between .Ltmp52 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject,@function
_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject: # @_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 32
.Lcfi56:
	.cfi_offset %rbx, -32
.Lcfi57:
	.cfi_offset %r14, -24
.Lcfi58:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	192(%rbx), %r15
	testq	%r15, %r15
	je	.LBB8_2
# BB#1:
	movq	112(%r14), %rdi
	movq	(%rdi), %rax
	callq	*64(%rax)
	movq	(%rax), %rcx
	movq	40(%r14), %rdx
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	*80(%rcx)
	movq	40(%r14), %rdx
	movq	112(%r14), %rdi
	movq	(%rdi), %rax
	movq	%r15, %rsi
	callq	*24(%rax)
	movq	$0, 192(%rbx)
.LBB8_2:
	movslq	12(%r14), %rax
	testq	%rax, %rax
	jle	.LBB8_8
# BB#3:                                 # %.lr.ph.i.i
	movq	24(%r14), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB8_4:                                # =>This Inner Loop Header: Depth=1
	cmpq	%rbx, (%rcx,%rdx,8)
	je	.LBB8_6
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB8_4
	jmp	.LBB8_8
.LBB8_6:                                # %_ZNK20btAlignedObjectArrayIP17btCollisionObjectE16findLinearSearchERKS1_.exit.i
	cmpl	%eax, %edx
	jge	.LBB8_8
# BB#7:
	leal	-1(%rax), %esi
	movq	(%rcx,%rdx,8), %rdi
	movq	-8(%rcx,%rax,8), %rbx
	movq	%rbx, (%rcx,%rdx,8)
	movq	24(%r14), %rcx
	movq	%rdi, -8(%rcx,%rax,8)
	movl	%esi, 12(%r14)
.LBB8_8:                                # %_ZN20btAlignedObjectArrayIP17btCollisionObjectE6removeERKS1_.exit
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject, .Lfunc_end8-_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_1:
	.long	953267991               # float 9.99999974E-5
.LCPI9_2:
	.long	1065353216              # float 1
	.text
	.globl	_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE,@function
_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE: # @_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 56
	subq	$1080, %rsp             # imm = 0x438
.Lcfi65:
	.cfi_def_cfa_offset 1136
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%r8, %rbx
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	leaq	520(%rsp), %rdi
	callq	_ZN21btConvexInternalShapeC2Ev
	movq	$_ZTV13btSphereShape+16, 520(%rsp)
	movl	$8, 528(%rsp)
	movl	$0, 560(%rsp)
	movl	$0, 576(%rsp)
	movl	8(%r15), %eax
	cmpl	$19, %eax
	jg	.LBB9_15
# BB#1:
	movq	$_ZTVN12btConvexCast10CastResultE+16, 128(%rsp)
	movq	$0, 304(%rsp)
	movl	$0, 312(%rsp)
	movl	8(%rbp), %eax
	movl	%eax, 296(%rsp)
	movb	$0, 1048(%rsp)
.Ltmp79:
	leaq	488(%rsp), %rdi
	leaq	520(%rsp), %rsi
	leaq	720(%rsp), %rcx
	movq	%r15, %rdx
	callq	_ZN22btSubsimplexConvexCastC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolver
.Ltmp80:
# BB#2:
.Ltmp82:
	leaq	488(%rsp), %rdi
	leaq	128(%rsp), %r9
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%rbx, %r8
	callq	_ZN22btSubsimplexConvexCast16calcTimeOfImpactERK11btTransformS2_S2_S2_RN12btConvexCast10CastResultE
.Ltmp83:
# BB#3:
	testb	%al, %al
	je	.LBB9_9
# BB#4:
	movss	264(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	268(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm1, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	272(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm4, %xmm4
	addss	%xmm3, %xmm4
	ucomiss	.LCPI9_1(%rip), %xmm4
	jbe	.LBB9_9
# BB#5:
	movss	8(%rbp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	ucomiss	296(%rsp), %xmm3
	jbe	.LBB9_9
# BB#6:
	leaq	264(%rsp), %rbx
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	16(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	(%rax), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rax), %xmm6          # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	20(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm3, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	mulps	%xmm3, %xmm5
	addps	%xmm6, %xmm5
	mulss	32(%rax), %xmm2
	mulss	36(%rax), %xmm1
	addss	%xmm2, %xmm1
	mulss	40(%rax), %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm5, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm5, 264(%rsp)
	movlps	%xmm2, 272(%rsp)
	mulss	%xmm5, %xmm5
	mulss	%xmm1, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB9_8
# BB#7:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB9_8:                                # %.split
	movss	.LCPI9_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	264(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 264(%rsp)
	movss	268(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 268(%rsp)
	mulss	272(%rsp), %xmm0
	movss	%xmm0, 272(%rsp)
	movl	296(%rsp), %eax
	movq	%r12, 328(%rsp)
	movq	$0, 336(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 344(%rsp)
	movl	%eax, 360(%rsp)
	movq	(%rbp), %rax
.Ltmp85:
	leaq	328(%rsp), %rsi
	movl	$1, %edx
	movq	%rbp, %rdi
	callq	*24(%rax)
.Ltmp86:
.LBB9_9:
.Ltmp90:
	leaq	488(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp91:
	jmp	.LBB9_50
.LBB9_15:
	leal	-21(%rax), %ecx
	cmpl	$8, %ecx
	ja	.LBB9_45
# BB#16:
	cmpl	$21, %eax
	jne	.LBB9_23
# BB#17:
	movsd	(%rbx), %xmm10          # xmm10 = mem[0],zero
	movsd	16(%rbx), %xmm9         # xmm9 = mem[0],zero
	movsd	32(%rbx), %xmm8         # xmm8 = mem[0],zero
	movss	8(%rbx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movd	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	.LCPI9_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm4, %xmm1
	movd	52(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movdqa	%xmm5, %xmm7
	pxor	%xmm4, %xmm7
	movd	56(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	pxor	%xmm6, %xmm4
	pshufd	$224, %xmm1, %xmm2      # xmm2 = xmm1[0,0,2,3]
	mulps	%xmm10, %xmm2
	pshufd	$224, %xmm7, %xmm3      # xmm3 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm3
	addps	%xmm2, %xmm3
	pshufd	$224, %xmm4, %xmm7      # xmm7 = xmm4[0,0,2,3]
	mulps	%xmm8, %xmm7
	addps	%xmm3, %xmm7
	mulss	%xmm11, %xmm1
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm12, %xmm6
	subss	%xmm6, %xmm1
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	56(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm10, %xmm2
	movaps	%xmm0, %xmm6
	mulss	%xmm3, %xmm6
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm9, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm12, %xmm2
	mulss	%xmm5, %xmm2
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	addss	%xmm4, %xmm6
	addss	%xmm2, %xmm6
	addss	%xmm1, %xmm6
	xorps	%xmm4, %xmm4
	xorps	%xmm2, %xmm2
	movss	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1,2,3]
	movlps	%xmm5, 112(%rsp)
	movlps	%xmm2, 120(%rsp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	56(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm11
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm10, %xmm2
	mulss	%xmm3, %xmm0
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm9, %xmm3
	addps	%xmm2, %xmm3
	mulss	%xmm5, %xmm12
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	addss	%xmm11, %xmm0
	addss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm5, 96(%rsp)
	movlps	%xmm4, 104(%rsp)
	movl	28(%rbp), %ecx
.Ltmp68:
	leaq	328(%rsp), %rdi
	leaq	112(%rsp), %rsi
	leaq	96(%rsp), %rdx
	callq	_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j
.Ltmp69:
# BB#18:
	leaq	16(%rbx), %rax
	leaq	32(%rbx), %rcx
	movq	$_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback+16, 328(%rsp)
	movq	%rbp, 376(%rsp)
	movq	%r12, 384(%rsp)
	movq	%r15, 392(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 400(%rsp)
	movups	(%rax), %xmm0
	movups	%xmm0, 416(%rsp)
	movups	(%rcx), %xmm0
	movups	%xmm0, 432(%rsp)
	movups	48(%rbx), %xmm0
	movups	%xmm0, 448(%rsp)
	movl	8(%rbp), %eax
	movl	%eax, 372(%rsp)
.Ltmp71:
	leaq	328(%rsp), %rsi
	leaq	112(%rsp), %rdx
	leaq	96(%rsp), %rcx
	movq	%r15, %rdi
	callq	_ZN22btBvhTriangleMeshShape14performRaycastEP18btTriangleCallbackRK9btVector3S4_
.Ltmp72:
# BB#19:
.Ltmp76:
	leaq	328(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp77:
	jmp	.LBB9_50
.LBB9_45:
	movq	%rbp, 88(%rsp)          # 8-byte Spill
	cmpl	$31, %eax
	jne	.LBB9_50
# BB#46:
	cmpl	$0, 28(%r15)
	jle	.LBB9_50
# BB#47:                                # %.lr.ph
	xorl	%r14d, %r14d
	movl	$64, %r13d
	.p2align	4, 0x90
.LBB9_48:                               # =>This Inner Loop Header: Depth=1
	movq	40(%r15), %rax
	movss	-64(%rax,%r13), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	-60(%rax,%r13), %xmm15  # xmm15 = mem[0],zero,zero,zero
	movss	-56(%rax,%r13), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	-48(%rax,%r13), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	-44(%rax,%r13), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-40(%rax,%r13), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	-32(%rax,%r13), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movss	%xmm13, 12(%rsp)        # 4-byte Spill
	movss	-28(%rax,%r13), %xmm14  # xmm14 = mem[0],zero,zero,zero
	movss	-24(%rax,%r13), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm0, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 52(%rsp)         # 4-byte Spill
	movaps	%xmm15, %xmm2
	movss	%xmm15, 32(%rsp)        # 4-byte Spill
	mulss	%xmm7, %xmm2
	movaps	%xmm1, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm14, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 48(%rsp)         # 4-byte Spill
	movaps	%xmm10, %xmm2
	movaps	%xmm10, %xmm11
	mulss	%xmm7, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm9, %xmm2
	movaps	%xmm9, %xmm10
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 44(%rsp)         # 4-byte Spill
	movss	16(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm9, %xmm2
	movss	20(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movss	24(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 40(%rsp)         # 4-byte Spill
	mulss	%xmm9, %xmm15
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm15, %xmm3
	movaps	%xmm14, %xmm2
	mulss	%xmm13, %xmm2
	movaps	%xmm13, %xmm15
	movaps	%xmm15, 464(%rsp)       # 16-byte Spill
	addss	%xmm3, %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movaps	%xmm11, %xmm2
	mulss	%xmm9, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm10, %xmm13
	mulss	%xmm15, %xmm13
	addss	%xmm3, %xmm13
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm8
	movss	36(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm8, %xmm0
	movss	40(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm15
	addss	%xmm0, %xmm15
	movss	%xmm15, 12(%rsp)        # 4-byte Spill
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm8, %xmm14
	addss	%xmm1, %xmm14
	mulss	%xmm3, %xmm11
	mulss	%xmm2, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm8, %xmm10
	addss	%xmm4, %xmm10
	unpcklps	%xmm9, %xmm7    # xmm7 = xmm7[0],xmm9[0],xmm7[1],xmm9[1]
	movss	-16(%rax,%r13), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm7
	unpcklps	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1]
	movss	-12(%rax,%r13), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm4, %xmm5
	addps	%xmm7, %xmm5
	unpcklps	464(%rsp), %xmm6 # 16-byte Folded Reload
                                        # xmm6 = xmm6[0],mem[0],xmm6[1],mem[1]
	movss	-8(%rax,%r13), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm5, %xmm6
	movsd	48(%rbx), %xmm5         # xmm5 = mem[0],zero
	addps	%xmm6, %xmm5
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm8, %xmm4
	addss	%xmm1, %xmm4
	addss	56(%rbx), %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movq	(%rax,%r13), %rcx
	movss	52(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 720(%rsp)
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 724(%rsp)
	movss	44(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 728(%rsp)
	movl	$0, 732(%rsp)
	movss	40(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 736(%rsp)
	movss	36(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 740(%rsp)
	movss	%xmm13, 744(%rsp)
	movl	$0, 748(%rsp)
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 752(%rsp)
	movss	%xmm14, 756(%rsp)
	movss	%xmm10, 760(%rsp)
	movl	$0, 764(%rsp)
	movlps	%xmm5, 768(%rsp)
	movlps	%xmm0, 776(%rsp)
	movq	200(%r12), %rbp
	movq	%rcx, 200(%r12)
.Ltmp54:
	movq	16(%rsp), %rdi          # 8-byte Reload
	movq	24(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %rdx
	leaq	720(%rsp), %r8
	movq	88(%rsp), %r9           # 8-byte Reload
	callq	_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE
.Ltmp55:
# BB#49:                                #   in Loop: Header=BB9_48 Depth=1
	movq	%rbp, 200(%r12)
	incq	%r14
	movslq	28(%r15), %rax
	addq	$88, %r13
	cmpq	%rax, %r14
	jl	.LBB9_48
	jmp	.LBB9_50
.LBB9_23:
	movsd	(%rbx), %xmm10          # xmm10 = mem[0],zero
	movsd	16(%rbx), %xmm9         # xmm9 = mem[0],zero
	movsd	32(%rbx), %xmm8         # xmm8 = mem[0],zero
	movss	8(%rbx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movd	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	.LCPI9_0(%rip), %xmm4   # xmm4 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm4, %xmm1
	movd	52(%rbx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movdqa	%xmm5, %xmm7
	pxor	%xmm4, %xmm7
	movd	56(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	pxor	%xmm6, %xmm4
	pshufd	$224, %xmm1, %xmm2      # xmm2 = xmm1[0,0,2,3]
	mulps	%xmm10, %xmm2
	pshufd	$224, %xmm7, %xmm3      # xmm3 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm3
	addps	%xmm2, %xmm3
	pshufd	$224, %xmm4, %xmm7      # xmm7 = xmm4[0,0,2,3]
	mulps	%xmm8, %xmm7
	addps	%xmm3, %xmm7
	mulss	%xmm11, %xmm1
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm12, %xmm6
	subss	%xmm6, %xmm1
	movq	16(%rsp), %rax          # 8-byte Reload
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	56(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm10, %xmm2
	movaps	%xmm0, %xmm6
	mulss	%xmm3, %xmm6
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm9, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm12, %xmm2
	mulss	%xmm5, %xmm2
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	addss	%xmm4, %xmm6
	addss	%xmm2, %xmm6
	addss	%xmm1, %xmm6
	xorps	%xmm4, %xmm4
	xorps	%xmm2, %xmm2
	movss	%xmm6, %xmm2            # xmm2 = xmm6[0],xmm2[1,2,3]
	movlps	%xmm5, 72(%rsp)
	movlps	%xmm2, 80(%rsp)
	movq	24(%rsp), %rax          # 8-byte Reload
	movss	48(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	56(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm11
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm10, %xmm2
	mulss	%xmm3, %xmm0
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm9, %xmm3
	addps	%xmm2, %xmm3
	mulss	%xmm5, %xmm12
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm8, %xmm5
	addps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	addss	%xmm11, %xmm0
	addss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm5, 56(%rsp)
	movlps	%xmm4, 64(%rsp)
	movl	28(%rbp), %ecx
.Ltmp57:
	leaq	584(%rsp), %rdi
	leaq	72(%rsp), %rsi
	leaq	56(%rsp), %rdx
	callq	_ZN25btTriangleRaycastCallbackC2ERK9btVector3S2_j
.Ltmp58:
# BB#24:
	leaq	16(%rbx), %rax
	leaq	32(%rbx), %rcx
	movq	$_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0+16, 584(%rsp)
	movq	%rbp, 632(%rsp)
	movq	%r12, 640(%rsp)
	movq	%r15, 648(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 656(%rsp)
	movups	(%rax), %xmm0
	movups	%xmm0, 672(%rsp)
	movups	(%rcx), %xmm0
	movups	%xmm0, 688(%rsp)
	movups	48(%rbx), %xmm0
	movups	%xmm0, 704(%rsp)
	movl	8(%rbp), %eax
	movl	%eax, 628(%rsp)
	movups	72(%rsp), %xmm0
	movaps	%xmm0, 720(%rsp)
	movss	56(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	720(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB9_26
# BB#25:
	movss	%xmm0, 720(%rsp)
.LBB9_26:                               # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	movss	60(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	724(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm2
	jbe	.LBB9_28
# BB#27:
	movss	%xmm1, 724(%rsp)
.LBB9_28:                               # %_Z8btSetMinIfEvRT_RKS0_.exit7.i
	movss	64(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	728(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	jbe	.LBB9_30
# BB#29:
	movss	%xmm2, 728(%rsp)
.LBB9_30:                               # %_Z8btSetMinIfEvRT_RKS0_.exit6.i
	movss	68(%rsp), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	732(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm4
	jbe	.LBB9_32
# BB#31:
	movss	%xmm3, 732(%rsp)
.LBB9_32:                               # %_ZN9btVector36setMinERKS_.exit
	movups	72(%rsp), %xmm4
	movaps	%xmm4, 128(%rsp)
	ucomiss	128(%rsp), %xmm0
	jbe	.LBB9_34
# BB#33:
	movss	%xmm0, 128(%rsp)
.LBB9_34:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	ucomiss	132(%rsp), %xmm1
	jbe	.LBB9_36
# BB#35:
	movss	%xmm1, 132(%rsp)
.LBB9_36:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i
	ucomiss	136(%rsp), %xmm2
	jbe	.LBB9_38
# BB#37:
	movss	%xmm2, 136(%rsp)
.LBB9_38:                               # %_Z8btSetMaxIfEvRT_RKS0_.exit6.i
	ucomiss	140(%rsp), %xmm3
	jbe	.LBB9_40
# BB#39:
	movss	%xmm3, 140(%rsp)
.LBB9_40:                               # %_ZN9btVector36setMaxERKS_.exit
	movq	(%r15), %rax
.Ltmp60:
	leaq	584(%rsp), %rsi
	leaq	720(%rsp), %rdx
	leaq	128(%rsp), %rcx
	movq	%r15, %rdi
	callq	*96(%rax)
.Ltmp61:
# BB#41:
.Ltmp65:
	leaq	584(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp66:
.LBB9_50:                               # %.loopexit
	leaq	520(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
	addq	$1080, %rsp             # imm = 0x438
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_11:
.Ltmp87:
	jmp	.LBB9_14
.LBB9_44:
.Ltmp67:
	jmp	.LBB9_52
.LBB9_43:
.Ltmp62:
	movq	%rax, %rbx
.Ltmp63:
	leaq	584(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp64:
	jmp	.LBB9_53
.LBB9_22:
.Ltmp78:
	jmp	.LBB9_52
.LBB9_21:
.Ltmp73:
	movq	%rax, %rbx
.Ltmp74:
	leaq	328(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp75:
	jmp	.LBB9_53
.LBB9_42:
.Ltmp59:
	jmp	.LBB9_52
.LBB9_20:
.Ltmp70:
	jmp	.LBB9_52
.LBB9_12:
.Ltmp92:
	jmp	.LBB9_52
.LBB9_13:
.Ltmp84:
.LBB9_14:
	movq	%rax, %rbx
.Ltmp88:
	leaq	488(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp89:
	jmp	.LBB9_53
.LBB9_10:
.Ltmp81:
	jmp	.LBB9_52
.LBB9_51:
.Ltmp56:
.LBB9_52:
	movq	%rax, %rbx
.LBB9_53:
.Ltmp93:
	leaq	520(%rsp), %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp94:
# BB#54:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB9_55:
.Ltmp95:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE, .Lfunc_end9-_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\314\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\303\001"              # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp79-.Lfunc_begin5   #   Call between .Lfunc_begin5 and .Ltmp79
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp79-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Ltmp80-.Ltmp79         #   Call between .Ltmp79 and .Ltmp80
	.long	.Ltmp81-.Lfunc_begin5   #     jumps to .Ltmp81
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin5   # >> Call Site 3 <<
	.long	.Ltmp83-.Ltmp82         #   Call between .Ltmp82 and .Ltmp83
	.long	.Ltmp84-.Lfunc_begin5   #     jumps to .Ltmp84
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin5   # >> Call Site 4 <<
	.long	.Ltmp86-.Ltmp85         #   Call between .Ltmp85 and .Ltmp86
	.long	.Ltmp87-.Lfunc_begin5   #     jumps to .Ltmp87
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin5   # >> Call Site 5 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin5   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin5   # >> Call Site 6 <<
	.long	.Ltmp69-.Ltmp68         #   Call between .Ltmp68 and .Ltmp69
	.long	.Ltmp70-.Lfunc_begin5   #     jumps to .Ltmp70
	.byte	0                       #   On action: cleanup
	.long	.Ltmp71-.Lfunc_begin5   # >> Call Site 7 <<
	.long	.Ltmp72-.Ltmp71         #   Call between .Ltmp71 and .Ltmp72
	.long	.Ltmp73-.Lfunc_begin5   #     jumps to .Ltmp73
	.byte	0                       #   On action: cleanup
	.long	.Ltmp76-.Lfunc_begin5   # >> Call Site 8 <<
	.long	.Ltmp77-.Ltmp76         #   Call between .Ltmp76 and .Ltmp77
	.long	.Ltmp78-.Lfunc_begin5   #     jumps to .Ltmp78
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin5   # >> Call Site 9 <<
	.long	.Ltmp55-.Ltmp54         #   Call between .Ltmp54 and .Ltmp55
	.long	.Ltmp56-.Lfunc_begin5   #     jumps to .Ltmp56
	.byte	0                       #   On action: cleanup
	.long	.Ltmp57-.Lfunc_begin5   # >> Call Site 10 <<
	.long	.Ltmp58-.Ltmp57         #   Call between .Ltmp57 and .Ltmp58
	.long	.Ltmp59-.Lfunc_begin5   #     jumps to .Ltmp59
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin5   # >> Call Site 11 <<
	.long	.Ltmp61-.Ltmp60         #   Call between .Ltmp60 and .Ltmp61
	.long	.Ltmp62-.Lfunc_begin5   #     jumps to .Ltmp62
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin5   # >> Call Site 12 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin5   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp66-.Lfunc_begin5   # >> Call Site 13 <<
	.long	.Ltmp63-.Ltmp66         #   Call between .Ltmp66 and .Ltmp63
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin5   # >> Call Site 14 <<
	.long	.Ltmp94-.Ltmp63         #   Call between .Ltmp63 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin5   #     jumps to .Ltmp95
	.byte	1                       #   On action: 1
	.long	.Ltmp94-.Lfunc_begin5   # >> Call Site 15 <<
	.long	.Lfunc_end9-.Ltmp94     #   Call between .Ltmp94 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12btConvexCast10CastResultD2Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD2Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD2Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD2Ev,@function
_ZN12btConvexCast10CastResultD2Ev:      # @_ZN12btConvexCast10CastResultD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN12btConvexCast10CastResultD2Ev, .Lfunc_end10-_ZN12btConvexCast10CastResultD2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI11_3:
	.zero	16
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_1:
	.long	953267991               # float 9.99999974E-5
.LCPI11_2:
	.long	1065353216              # float 1
	.text
	.globl	_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf,@function
_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf: # @_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$1592, %rsp             # imm = 0x638
.Lcfi78:
	.cfi_def_cfa_offset 1648
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movq	%r8, %rbp
	movq	%rcx, %r13
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, 40(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	movq	1648(%rsp), %r14
	movl	8(%rbp), %eax
	cmpl	$19, %eax
	jg	.LBB11_10
# BB#1:
	movq	$_ZTVN12btConvexCast10CastResultE+16, 384(%rsp)
	movq	$0, 560(%rsp)
	movss	%xmm0, 568(%rsp)
	movl	8(%r14), %eax
	movl	%eax, 552(%rsp)
	movb	$0, 1320(%rsp)
	movq	$_ZTV30btGjkEpaPenetrationDepthSolver+16, 280(%rsp)
.Ltmp116:
	leaq	712(%rsp), %rdi
	leaq	992(%rsp), %rcx
	leaq	280(%rsp), %r8
	movq	%r15, %rsi
	movq	%rbp, %rdx
	callq	_ZN27btContinuousConvexCollisionC1EPK13btConvexShapeS2_P22btVoronoiSimplexSolverP30btConvexPenetrationDepthSolver
.Ltmp117:
# BB#2:
	movq	712(%rsp), %rax
.Ltmp119:
	leaq	712(%rsp), %rdi
	leaq	384(%rsp), %r9
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%rbx, %rcx
	movq	%rbx, %r8
	callq	*16(%rax)
.Ltmp120:
# BB#3:
	testb	%al, %al
	je	.LBB11_9
# BB#4:
	movss	520(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	524(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	528(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI11_1(%rip), %xmm0
	jbe	.LBB11_9
# BB#5:
	movss	8(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	552(%rsp), %xmm1
	jbe	.LBB11_9
# BB#6:
	leaq	520(%rsp), %rbx
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB11_8
# BB#7:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB11_8:                               # %.split
	movss	.LCPI11_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	520(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 520(%rsp)
	movss	524(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 524(%rsp)
	mulss	528(%rsp), %xmm0
	movss	%xmm0, 528(%rsp)
	movl	552(%rsp), %eax
	movq	%r13, 752(%rsp)
	movq	$0, 760(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 768(%rsp)
	movups	536(%rsp), %xmm0
	movups	%xmm0, 784(%rsp)
	movl	%eax, 800(%rsp)
	movq	(%r14), %rax
.Ltmp122:
	leaq	752(%rsp), %rsi
	movl	$1, %edx
	movq	%r14, %rdi
	callq	*24(%rax)
.Ltmp123:
.LBB11_9:
.Ltmp128:
	leaq	712(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp129:
	jmp	.LBB11_37
.LBB11_10:
	leal	-21(%rax), %ecx
	cmpl	$8, %ecx
	ja	.LBB11_15
# BB#11:
	cmpl	$21, %eax
	jne	.LBB11_21
# BB#12:
	movsd	(%rbx), %xmm15          # xmm15 = mem[0],zero
	movsd	16(%rbx), %xmm8         # xmm8 = mem[0],zero
	movsd	32(%rbx), %xmm12        # xmm12 = mem[0],zero
	movss	8(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	24(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movd	48(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movdqa	.LCPI11_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm6
	movd	52(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movdqa	%xmm1, %xmm2
	pxor	%xmm0, %xmm2
	movd	56(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	pxor	%xmm3, %xmm0
	pshufd	$224, %xmm6, %xmm7      # xmm7 = xmm6[0,0,2,3]
	mulps	%xmm15, %xmm7
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm8, %xmm2
	addps	%xmm7, %xmm2
	pshufd	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	addps	%xmm2, %xmm0
	mulss	%xmm5, %xmm6
	mulss	%xmm4, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm9, %xmm3
	subss	%xmm3, %xmm6
	movq	40(%rsp), %r14          # 8-byte Reload
	movss	48(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	56(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm7
	movaps	%xmm5, %xmm10
	movss	%xmm10, 16(%rsp)        # 4-byte Spill
	mulss	%xmm2, %xmm7
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm15, %xmm2
	movaps	%xmm4, %xmm5
	movss	%xmm4, 144(%rsp)        # 4-byte Spill
	mulss	%xmm3, %xmm5
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm8, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm9, %xmm2
	movss	%xmm9, 128(%rsp)        # 4-byte Spill
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm12, %xmm1
	addps	%xmm3, %xmm1
	addps	%xmm0, %xmm1
	addss	%xmm7, %xmm5
	addss	%xmm2, %xmm5
	addss	%xmm6, %xmm5
	xorps	%xmm2, %xmm2
	movss	%xmm5, %xmm2            # xmm2 = xmm5[0],xmm2[1,2,3]
	movlps	%xmm1, 368(%rsp)
	movlps	%xmm2, 376(%rsp)
	movq	48(%rsp), %r12          # 8-byte Reload
	movss	48(%r12), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	52(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	56(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	mulss	%xmm2, %xmm5
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm15, %xmm2
	movaps	%xmm4, %xmm7
	mulss	%xmm3, %xmm7
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm8, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm9, %xmm2
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm12, %xmm1
	addps	%xmm3, %xmm1
	addps	%xmm0, %xmm1
	addss	%xmm5, %xmm7
	addss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	xorps	%xmm0, %xmm0
	movss	%xmm7, %xmm0            # xmm0 = xmm7[0],xmm0[1,2,3]
	movlps	%xmm1, 352(%rsp)
	movlps	%xmm0, 360(%rsp)
	movss	(%r12), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm0
	mulss	%xmm11, %xmm0
	movss	16(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	32(%r12), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm2
	movaps	%xmm2, %xmm0
	mulss	%xmm10, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	movaps	%xmm15, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, %xmm12
	movss	%xmm12, 64(%rsp)        # 4-byte Spill
	movss	20(%r12), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm0, %xmm1
	movss	36(%r12), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm14
	mulss	%xmm9, %xmm14
	addss	%xmm1, %xmm14
	movss	8(%r12), %xmm0          # xmm0 = mem[0],zero,zero,zero
	pshufd	$229, %xmm15, %xmm4     # xmm4 = xmm15[1,1,2,3]
	mulss	%xmm0, %xmm15
	movaps	%xmm0, %xmm13
	movss	24(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 80(%rsp)         # 4-byte Spill
	pshufd	$229, %xmm8, %xmm3      # xmm3 = xmm8[1,1,2,3]
	mulss	%xmm0, %xmm8
	addss	%xmm15, %xmm8
	movss	40(%r12), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	pshufd	$229, %xmm0, %xmm5      # xmm5 = xmm0[1,1,2,3]
	mulss	%xmm15, %xmm0
	addss	%xmm8, %xmm0
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movdqa	%xmm4, %xmm2
	mulss	%xmm11, %xmm2
	movdqa	%xmm3, %xmm0
	mulss	%xmm7, %xmm0
	addss	%xmm2, %xmm0
	movdqa	%xmm5, %xmm8
	mulss	%xmm10, %xmm8
	addss	%xmm0, %xmm8
	movdqa	%xmm4, %xmm0
	mulss	%xmm12, %xmm0
	movdqa	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm5, %xmm2
	mulss	%xmm9, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm13, %xmm12
	mulss	%xmm12, %xmm4
	movss	80(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm15, %xmm5
	addss	%xmm3, %xmm5
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	movss	144(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm11, %xmm7
	movss	128(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm10
	addss	%xmm7, %xmm10
	movss	64(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	mulss	%xmm1, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm0, %xmm9
	addss	%xmm6, %xmm9
	mulss	%xmm12, %xmm3
	mulss	%xmm13, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm15, %xmm0
	addss	%xmm1, %xmm0
	movaps	112(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 160(%rsp)
	movss	%xmm14, 164(%rsp)
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	movss	%xmm1, 168(%rsp)
	movl	$0, 172(%rsp)
	movss	%xmm8, 176(%rsp)
	movss	%xmm2, 180(%rsp)
	movss	%xmm5, 184(%rsp)
	movl	$0, 188(%rsp)
	movss	%xmm10, 192(%rsp)
	movss	%xmm9, 196(%rsp)
	movss	%xmm0, 200(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 204(%rsp)
	movl	$0, 220(%rsp)
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*88(%rax)
	leaq	752(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rcx
	movq	%rbx, %r8
	callq	_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f
	movq	$_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback+16, 752(%rsp)
	movq	1648(%rsp), %rax
	movq	%rax, 968(%rsp)
	movq	%r13, 976(%rsp)
	movq	%rbp, 984(%rsp)
	movl	8(%rax), %eax
	movl	%eax, 960(%rsp)
	movq	(%r15), %rax
.Ltmp109:
	leaq	160(%rsp), %rsi
	leaq	992(%rsp), %rdx
	leaq	1352(%rsp), %rcx
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp110:
# BB#13:
.Ltmp111:
	leaq	752(%rsp), %rsi
	leaq	368(%rsp), %rdx
	leaq	352(%rsp), %rcx
	leaq	992(%rsp), %r8
	leaq	1352(%rsp), %r9
	movq	%rbp, %rdi
	callq	_ZN22btBvhTriangleMeshShape17performConvexcastEP18btTriangleCallbackRK9btVector3S4_S4_S4_
.Ltmp112:
# BB#14:
	leaq	752(%rsp), %rdi
	jmp	.LBB11_36
.LBB11_15:
	cmpl	$31, %eax
	jne	.LBB11_37
# BB#16:
	movss	%xmm0, 224(%rsp)        # 4-byte Spill
	movq	%r15, 240(%rsp)         # 8-byte Spill
	movl	$.L.str.8, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	cmpl	$0, 28(%rbp)
	jle	.LBB11_20
# BB#17:                                # %.lr.ph
	xorl	%r12d, %r12d
	movl	$64, %r14d
	.p2align	4, 0x90
.LBB11_18:                              # =>This Inner Loop Header: Depth=1
	movq	40(%rbp), %rax
	movss	-64(%rax,%r14), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movss	-60(%rax,%r14), %xmm15  # xmm15 = mem[0],zero,zero,zero
	movss	-56(%rax,%r14), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movss	-48(%rax,%r14), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movss	-44(%rax,%r14), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-40(%rax,%r14), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	-32(%rax,%r14), %xmm13  # xmm13 = mem[0],zero,zero,zero
	movss	%xmm13, 16(%rsp)        # 4-byte Spill
	movss	-28(%rax,%r14), %xmm14  # xmm14 = mem[0],zero,zero,zero
	movss	-24(%rax,%r14), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm7, %xmm2
	movaps	%xmm0, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movss	8(%rbx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 144(%rsp)        # 4-byte Spill
	movaps	%xmm15, %xmm2
	movss	%xmm15, 256(%rsp)       # 4-byte Spill
	mulss	%xmm7, %xmm2
	movaps	%xmm1, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm14, %xmm2
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 128(%rsp)        # 4-byte Spill
	movaps	%xmm10, %xmm2
	movaps	%xmm10, %xmm11
	mulss	%xmm7, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm9, %xmm2
	movaps	%xmm9, %xmm10
	mulss	%xmm6, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 112(%rsp)        # 4-byte Spill
	movss	16(%rbx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	mulss	%xmm9, %xmm2
	movss	20(%rbx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movss	24(%rbx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 96(%rsp)         # 4-byte Spill
	mulss	%xmm9, %xmm15
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm15, %xmm3
	movaps	%xmm14, %xmm2
	mulss	%xmm13, %xmm2
	movaps	%xmm13, %xmm15
	movaps	%xmm15, 64(%rsp)        # 16-byte Spill
	addss	%xmm3, %xmm2
	movss	%xmm2, 80(%rsp)         # 4-byte Spill
	movaps	%xmm11, %xmm2
	mulss	%xmm9, %xmm2
	movaps	%xmm4, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm10, %xmm13
	mulss	%xmm15, %xmm13
	addss	%xmm3, %xmm13
	movss	32(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm8
	movss	36(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm8, %xmm0
	movss	40(%rbx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm15
	addss	%xmm0, %xmm15
	movss	%xmm15, 16(%rsp)        # 4-byte Spill
	movss	256(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm8, %xmm14
	addss	%xmm1, %xmm14
	mulss	%xmm3, %xmm11
	mulss	%xmm2, %xmm4
	addss	%xmm11, %xmm4
	mulss	%xmm8, %xmm10
	addss	%xmm4, %xmm10
	unpcklps	%xmm9, %xmm7    # xmm7 = xmm7[0],xmm9[0],xmm7[1],xmm9[1]
	movss	-16(%rax,%r14), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm7
	unpcklps	%xmm12, %xmm5   # xmm5 = xmm5[0],xmm12[0],xmm5[1],xmm12[1]
	movss	-12(%rax,%r14), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm4, %xmm5
	addps	%xmm7, %xmm5
	unpcklps	64(%rsp), %xmm6 # 16-byte Folded Reload
                                        # xmm6 = xmm6[0],mem[0],xmm6[1],mem[1]
	movss	-8(%rax,%r14), %xmm4    # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm5, %xmm6
	movsd	48(%rbx), %xmm5         # xmm5 = mem[0],zero
	addps	%xmm6, %xmm5
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm8, %xmm4
	addss	%xmm1, %xmm4
	addss	56(%rbx), %xmm4
	xorps	%xmm0, %xmm0
	movss	%xmm4, %xmm0            # xmm0 = xmm4[0],xmm0[1,2,3]
	movq	(%rax,%r14), %r8
	movss	144(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 992(%rsp)
	movss	128(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 996(%rsp)
	movss	112(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 1000(%rsp)
	movl	$0, 1004(%rsp)
	movss	96(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 1008(%rsp)
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 1012(%rsp)
	movss	%xmm13, 1016(%rsp)
	movl	$0, 1020(%rsp)
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 1024(%rsp)
	movss	%xmm14, 1028(%rsp)
	movss	%xmm10, 1032(%rsp)
	movl	$0, 1036(%rsp)
	movlps	%xmm5, 1040(%rsp)
	movlps	%xmm0, 1048(%rsp)
	movq	200(%r13), %r15
	movq	%r8, 200(%r13)
.Ltmp96:
	movq	1648(%rsp), %rax
	movq	%rax, (%rsp)
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	40(%rsp), %rsi          # 8-byte Reload
	movq	48(%rsp), %rdx          # 8-byte Reload
	movq	%r13, %rcx
	leaq	992(%rsp), %r9
	movss	224(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf
.Ltmp97:
# BB#19:                                #   in Loop: Header=BB11_18 Depth=1
	movq	%r15, 200(%r13)
	incq	%r12
	movslq	28(%rbp), %rax
	addq	$88, %r14
	cmpq	%rax, %r12
	jl	.LBB11_18
.LBB11_20:                              # %._crit_edge
	callq	_ZN15CProfileManager12Stop_ProfileEv
	jmp	.LBB11_37
.LBB11_21:
	movss	16(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	32(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movq	48(%rsp), %r12          # 8-byte Reload
	movss	(%r12), %xmm11          # xmm11 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm11, %xmm0
	movss	16(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm0, %xmm1
	movss	32(%r12), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm3, %xmm0
	movss	20(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm0, %xmm3
	movss	36(%r12), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm9, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	movss	8(%r12), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm13, %xmm0
	movss	24(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movaps	%xmm2, %xmm3
	mulss	%xmm4, %xmm3
	addss	%xmm0, %xmm3
	movss	40(%r12), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm6, %xmm12
	mulss	%xmm8, %xmm12
	addss	%xmm3, %xmm12
	movss	4(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm11, %xmm0
	movss	20(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm6
	mulss	%xmm7, %xmm6
	addss	%xmm0, %xmm6
	movss	36(%rbx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm10
	mulss	%xmm14, %xmm10
	addss	%xmm6, %xmm10
	movaps	%xmm2, %xmm0
	mulss	16(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	%xmm1, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm15, %xmm6
	mulss	%xmm9, %xmm6
	addss	%xmm3, %xmm6
	movaps	%xmm2, 240(%rsp)        # 16-byte Spill
	mulss	%xmm13, %xmm2
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	mulss	%xmm4, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm15, 256(%rsp)       # 16-byte Spill
	mulss	%xmm8, %xmm15
	addss	%xmm1, %xmm15
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm11
	movss	24(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	addss	%xmm11, %xmm7
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm14
	addss	%xmm7, %xmm14
	movss	16(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	mulss	%xmm1, %xmm5
	addss	%xmm7, %xmm5
	mulss	%xmm2, %xmm9
	addss	%xmm5, %xmm9
	movaps	%xmm3, 688(%rsp)        # 16-byte Spill
	mulss	%xmm3, %xmm13
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	mulss	%xmm1, %xmm4
	addss	%xmm13, %xmm4
	movaps	%xmm2, 224(%rsp)        # 16-byte Spill
	mulss	%xmm2, %xmm8
	addss	%xmm4, %xmm8
	movss	48(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 672(%rsp)        # 16-byte Spill
	movss	52(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 56(%rsp)         # 4-byte Spill
	movss	56(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 60(%rsp)         # 4-byte Spill
	movss	48(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 576(%rsp)        # 16-byte Spill
	movss	52(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 592(%rsp)        # 16-byte Spill
	movq	40(%rsp), %r14          # 8-byte Reload
	movss	48(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 624(%rsp)        # 16-byte Spill
	movss	52(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 640(%rsp)        # 16-byte Spill
	movss	56(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 608(%rsp)        # 16-byte Spill
	movss	56(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 656(%rsp)        # 16-byte Spill
	movaps	144(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 288(%rsp)
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	movss	%xmm1, 292(%rsp)
	movss	%xmm12, 296(%rsp)
	movl	$0, 300(%rsp)
	movss	%xmm10, 304(%rsp)
	movss	%xmm6, 308(%rsp)
	movss	%xmm15, 312(%rsp)
	movl	$0, 316(%rsp)
	movss	%xmm14, 320(%rsp)
	movss	%xmm9, 324(%rsp)
	movss	%xmm8, 328(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 332(%rsp)
	movl	$0, 348(%rsp)
	movq	(%rbp), %rax
	movq	%rbp, %rdi
	callq	*88(%rax)
	leaq	1352(%rsp), %rdi
	movq	%r15, %rsi
	movq	%r14, %rdx
	movq	%r12, %rcx
	movq	%rbx, %r8
	callq	_ZN28btTriangleConvexcastCallbackC2EPK13btConvexShapeRK11btTransformS5_S5_f
	movq	$_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0+16, 1352(%rsp)
	movq	1648(%rsp), %rax
	movq	%rax, 1568(%rsp)
	movq	%r13, 1576(%rsp)
	movq	%rbp, 1584(%rsp)
	movl	8(%rax), %eax
	movl	%eax, 1560(%rsp)
	movq	(%r15), %rax
.Ltmp101:
	leaq	288(%rsp), %rsi
	leaq	992(%rsp), %rdx
	leaq	752(%rsp), %rcx
	movq	%r15, %rdi
	callq	*16(%rax)
.Ltmp102:
# BB#22:
	movaps	672(%rsp), %xmm0        # 16-byte Reload
	xorps	.LCPI11_0(%rip), %xmm0
	movaps	80(%rsp), %xmm14        # 16-byte Reload
	movaps	%xmm14, %xmm11
	mulss	%xmm0, %xmm11
	movaps	96(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm1
	movss	56(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm1
	subss	%xmm1, %xmm11
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm1
	movss	60(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm11
	movaps	240(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm15
	mulss	%xmm0, %xmm15
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm2
	mulss	%xmm6, %xmm2
	subss	%xmm2, %xmm15
	movaps	256(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm2
	mulss	%xmm5, %xmm2
	subss	%xmm2, %xmm15
	movaps	688(%rsp), %xmm10       # 16-byte Reload
	mulss	%xmm10, %xmm0
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm2, %xmm6
	subss	%xmm6, %xmm0
	movaps	224(%rsp), %xmm9        # 16-byte Reload
	mulss	%xmm9, %xmm5
	subss	%xmm5, %xmm0
	movaps	624(%rsp), %xmm13       # 16-byte Reload
	unpcklps	576(%rsp), %xmm13 # 16-byte Folded Reload
                                        # xmm13 = xmm13[0],mem[0],xmm13[1],mem[1]
	movaps	640(%rsp), %xmm12       # 16-byte Reload
	unpcklps	592(%rsp), %xmm12 # 16-byte Folded Reload
                                        # xmm12 = xmm12[0],mem[0],xmm12[1],mem[1]
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm13, %xmm14
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm12, %xmm4
	addps	%xmm14, %xmm4
	movaps	656(%rsp), %xmm5        # 16-byte Reload
	unpcklps	608(%rsp), %xmm5 # 16-byte Folded Reload
                                        # xmm5 = xmm5[0],mem[0],xmm5[1],mem[1]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	addps	%xmm3, %xmm11
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm13, %xmm8
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm12, %xmm1
	addps	%xmm8, %xmm1
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm5, %xmm7
	addps	%xmm1, %xmm7
	shufps	$224, %xmm15, %xmm15    # xmm15 = xmm15[0,0,2,3]
	addps	%xmm7, %xmm15
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm13, %xmm10
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm12, %xmm2
	addps	%xmm10, %xmm2
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm5, %xmm9
	addps	%xmm2, %xmm9
	movaps	%xmm11, %xmm4
	unpcklps	%xmm15, %xmm4   # xmm4 = xmm4[0],xmm15[0],xmm4[1],xmm15[1]
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	addps	%xmm9, %xmm0
	movaps	%xmm0, %xmm2
	unpcklps	.LCPI11_3, %xmm2 # xmm2 = xmm2[0],mem[0],xmm2[1],mem[1]
	unpcklpd	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0]
	movupd	%xmm4, 384(%rsp)
	movaps	%xmm11, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	ucomiss	%xmm5, %xmm11
	movaps	%xmm11, %xmm8
	movaps	%xmm0, %xmm3
	jbe	.LBB11_24
# BB#23:
	movss	%xmm5, 384(%rsp)
	movaps	%xmm5, %xmm8
.LBB11_24:                              # %_Z8btSetMinIfEvRT_RKS0_.exit.i
	movaps	%xmm15, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	ucomiss	%xmm6, %xmm15
	movaps	%xmm15, %xmm9
	jbe	.LBB11_26
# BB#25:
	movss	%xmm6, 388(%rsp)
	movaps	%xmm6, %xmm9
.LBB11_26:                              # %_Z8btSetMinIfEvRT_RKS0_.exit7.i
	movaps	%xmm3, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	ucomiss	%xmm2, %xmm3
	movaps	%xmm3, %xmm7
	jbe	.LBB11_28
# BB#27:
	movss	%xmm2, 392(%rsp)
	movaps	%xmm2, %xmm7
.LBB11_28:                              # %_ZN9btVector36setMinERKS_.exit
	movupd	%xmm4, 160(%rsp)
	ucomiss	%xmm11, %xmm5
	jbe	.LBB11_30
# BB#29:
	movss	%xmm5, 160(%rsp)
	movaps	%xmm5, %xmm11
.LBB11_30:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit.i
	ucomiss	%xmm15, %xmm6
	jbe	.LBB11_32
# BB#31:
	movss	%xmm6, 164(%rsp)
	movaps	%xmm6, %xmm15
.LBB11_32:                              # %_Z8btSetMaxIfEvRT_RKS0_.exit7.i
	ucomiss	%xmm3, %xmm2
	jbe	.LBB11_34
# BB#33:
	movss	%xmm2, 168(%rsp)
	movaps	%xmm2, %xmm3
.LBB11_34:                              # %_ZN9btVector36setMaxERKS_.exit
	addss	992(%rsp), %xmm8
	movss	%xmm8, 384(%rsp)
	addss	996(%rsp), %xmm9
	movss	%xmm9, 388(%rsp)
	addss	1000(%rsp), %xmm7
	movss	%xmm7, 392(%rsp)
	addss	752(%rsp), %xmm11
	movss	%xmm11, 160(%rsp)
	addss	756(%rsp), %xmm15
	movss	%xmm15, 164(%rsp)
	addss	760(%rsp), %xmm3
	movss	%xmm3, 168(%rsp)
	movq	(%rbp), %rax
.Ltmp104:
	leaq	1352(%rsp), %rbx
	leaq	384(%rsp), %rdx
	leaq	160(%rsp), %rcx
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	*96(%rax)
.Ltmp105:
# BB#35:
	leaq	1352(%rsp), %rdi
.LBB11_36:
	callq	_ZN18btTriangleCallbackD2Ev
.LBB11_37:
	addq	$1592, %rsp             # imm = 0x638
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB11_38:
.Ltmp124:
	jmp	.LBB11_45
.LBB11_39:
.Ltmp106:
	movq	%rbx, %rdi
	jmp	.LBB11_41
.LBB11_40:
.Ltmp103:
	leaq	1352(%rsp), %rdi
.LBB11_41:
	movq	%rax, %rbx
.Ltmp107:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp108:
	jmp	.LBB11_48
.LBB11_42:
.Ltmp113:
	movq	%rax, %rbx
.Ltmp114:
	leaq	752(%rsp), %rdi
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp115:
	jmp	.LBB11_48
.LBB11_43:
.Ltmp130:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB11_44:
.Ltmp121:
.LBB11_45:
	movq	%rax, %rbx
.Ltmp125:
	leaq	712(%rsp), %rdi
	callq	_ZN12btConvexCastD2Ev
.Ltmp126:
	jmp	.LBB11_48
.LBB11_46:
.Ltmp118:
	movq	%rax, %rbx
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB11_47:
.Ltmp98:
	movq	%rax, %rbx
.Ltmp99:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp100:
.LBB11_48:                              # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB11_49:
.Ltmp127:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf, .Lfunc_end11-_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\363\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\352\001"              # Call site table length
	.long	.Ltmp116-.Lfunc_begin6  # >> Call Site 1 <<
	.long	.Ltmp117-.Ltmp116       #   Call between .Ltmp116 and .Ltmp117
	.long	.Ltmp118-.Lfunc_begin6  #     jumps to .Ltmp118
	.byte	0                       #   On action: cleanup
	.long	.Ltmp119-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp120-.Ltmp119       #   Call between .Ltmp119 and .Ltmp120
	.long	.Ltmp121-.Lfunc_begin6  #     jumps to .Ltmp121
	.byte	0                       #   On action: cleanup
	.long	.Ltmp122-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp123-.Ltmp122       #   Call between .Ltmp122 and .Ltmp123
	.long	.Ltmp124-.Lfunc_begin6  #     jumps to .Ltmp124
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin6  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp129-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp109-.Ltmp129       #   Call between .Ltmp129 and .Ltmp109
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp109-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Ltmp112-.Ltmp109       #   Call between .Ltmp109 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin6  #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp112-.Lfunc_begin6  # >> Call Site 7 <<
	.long	.Ltmp96-.Ltmp112        #   Call between .Ltmp112 and .Ltmp96
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin6   # >> Call Site 8 <<
	.long	.Ltmp97-.Ltmp96         #   Call between .Ltmp96 and .Ltmp97
	.long	.Ltmp98-.Lfunc_begin6   #     jumps to .Ltmp98
	.byte	0                       #   On action: cleanup
	.long	.Ltmp97-.Lfunc_begin6   # >> Call Site 9 <<
	.long	.Ltmp101-.Ltmp97        #   Call between .Ltmp97 and .Ltmp101
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp101-.Lfunc_begin6  # >> Call Site 10 <<
	.long	.Ltmp102-.Ltmp101       #   Call between .Ltmp101 and .Ltmp102
	.long	.Ltmp103-.Lfunc_begin6  #     jumps to .Ltmp103
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin6  # >> Call Site 11 <<
	.long	.Ltmp105-.Ltmp104       #   Call between .Ltmp104 and .Ltmp105
	.long	.Ltmp106-.Lfunc_begin6  #     jumps to .Ltmp106
	.byte	0                       #   On action: cleanup
	.long	.Ltmp105-.Lfunc_begin6  # >> Call Site 12 <<
	.long	.Ltmp107-.Ltmp105       #   Call between .Ltmp105 and .Ltmp107
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp107-.Lfunc_begin6  # >> Call Site 13 <<
	.long	.Ltmp115-.Ltmp107       #   Call between .Ltmp107 and .Ltmp115
	.long	.Ltmp127-.Lfunc_begin6  #     jumps to .Ltmp127
	.byte	1                       #   On action: 1
	.long	.Ltmp115-.Lfunc_begin6  # >> Call Site 14 <<
	.long	.Ltmp125-.Ltmp115       #   Call between .Ltmp115 and .Ltmp125
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin6  # >> Call Site 15 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin6  #     jumps to .Ltmp127
	.byte	1                       #   On action: 1
	.long	.Ltmp126-.Lfunc_begin6  # >> Call Site 16 <<
	.long	.Ltmp99-.Ltmp126        #   Call between .Ltmp126 and .Ltmp99
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp99-.Lfunc_begin6   # >> Call Site 17 <<
	.long	.Ltmp100-.Ltmp99        #   Call between .Ltmp99 and .Ltmp100
	.long	.Ltmp127-.Lfunc_begin6  #     jumps to .Ltmp127
	.byte	1                       #   On action: 1
	.long	.Ltmp100-.Lfunc_begin6  # >> Call Site 18 <<
	.long	.Lfunc_end11-.Ltmp100   #   Call between .Ltmp100 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1065353216              # float 1
.LCPI12_1:
	.long	1566444395              # float 9.99999984E+17
	.text
	.globl	_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE
	.p2align	4, 0x90
	.type	_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE,@function
_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE: # @_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 32
	subq	$288, %rsp              # imm = 0x120
.Lcfi88:
	.cfi_def_cfa_offset 320
.Lcfi89:
	.cfi_offset %rbx, -32
.Lcfi90:
	.cfi_offset %r14, -24
.Lcfi91:
	.cfi_offset %r15, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	$_ZTV19btSingleRayCallback+16, 24(%rsp)
	movups	(%r14), %xmm0
	movups	%xmm0, 64(%rsp)
	movups	(%rbx), %xmm0
	movups	%xmm0, 80(%rsp)
	movq	%r15, 240(%rsp)
	movq	%rcx, 248(%rsp)
	movl	$1065353216, 96(%rsp)   # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 100(%rsp)
	movl	$1065353216, 116(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 120(%rsp)
	movq	$1065353216, 136(%rsp)  # imm = 0x3F800000
	movups	64(%rsp), %xmm1
	movups	%xmm1, 144(%rsp)
	movl	$1065353216, 160(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 164(%rsp)
	movl	$1065353216, 180(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 184(%rsp)
	movq	$1065353216, 200(%rsp)  # imm = 0x3F800000
	movups	(%rbx), %xmm0
	movups	%xmm0, 208(%rsp)
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	subss	(%r14), %xmm6
	subss	4(%r14), %xmm5
	movss	8(%rbx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	subss	8(%r14), %xmm7
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB12_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm5, 20(%rsp)         # 4-byte Spill
	movss	%xmm6, 16(%rsp)         # 4-byte Spill
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	12(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
.LBB12_2:                               # %.split
	movss	.LCPI12_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	mulss	%xmm3, %xmm6
	mulss	%xmm3, %xmm5
	movss	.LCPI12_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm6
	movaps	%xmm0, %xmm2
	jne	.LBB12_3
	jnp	.LBB12_4
.LBB12_3:                               # %select.false.sink
	movaps	%xmm1, %xmm2
	divss	%xmm6, %xmm2
.LBB12_4:                               # %select.end
	mulss	%xmm3, %xmm7
	movss	%xmm2, 32(%rsp)
	ucomiss	%xmm4, %xmm5
	movaps	%xmm0, %xmm3
	jne	.LBB12_5
	jnp	.LBB12_6
.LBB12_5:                               # %select.false.sink25
	movaps	%xmm1, %xmm3
	divss	%xmm5, %xmm3
.LBB12_6:                               # %select.end24
	movss	%xmm3, 36(%rsp)
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm7
	jne	.LBB12_7
	jnp	.LBB12_8
.LBB12_7:                               # %select.false.sink27
	divss	%xmm7, %xmm1
	movaps	%xmm1, %xmm0
.LBB12_8:                               # %select.end26
	movss	%xmm0, 40(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm2, %xmm4
	seta	%al
	movl	%eax, 48(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm3, %xmm4
	seta	%al
	movl	%eax, 52(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm4
	seta	%al
	movl	%eax, 56(%rsp)
	movss	80(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	84(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	64(%rsp), %xmm0
	subss	68(%rsp), %xmm1
	movss	88(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	72(%rsp), %xmm2
	mulss	%xmm0, %xmm6
	mulss	%xmm1, %xmm5
	addss	%xmm6, %xmm5
	mulss	%xmm2, %xmm7
	addss	%xmm5, %xmm7
	movss	%xmm7, 60(%rsp)
	movq	112(%r15), %rdi
	movq	(%rdi), %rax
	movq	48(%rax), %rax
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
.Ltmp131:
	leaq	24(%rsp), %rcx
	leaq	272(%rsp), %r8
	leaq	256(%rsp), %r9
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	*%rax
.Ltmp132:
# BB#9:
	addq	$288, %rsp              # imm = 0x120
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB12_10:
.Ltmp133:
	movq	%rax, %rdi
	callq	_Unwind_Resume
.Lfunc_end12:
	.size	_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE, .Lfunc_end12-_ZNK16btCollisionWorld7rayTestERK9btVector3S2_RNS_17RayResultCallbackE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table12:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp131-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin7  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp132-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Lfunc_end12-.Ltmp132   #   Call between .Ltmp132 and .Lfunc_end12
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN23btBroadphaseRayCallbackD2Ev,"axG",@progbits,_ZN23btBroadphaseRayCallbackD2Ev,comdat
	.weak	_ZN23btBroadphaseRayCallbackD2Ev
	.p2align	4, 0x90
	.type	_ZN23btBroadphaseRayCallbackD2Ev,@function
_ZN23btBroadphaseRayCallbackD2Ev:       # @_ZN23btBroadphaseRayCallbackD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN23btBroadphaseRayCallbackD2Ev, .Lfunc_end13-_ZN23btBroadphaseRayCallbackD2Ev
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1073741824              # float 2
.LCPI14_1:
	.long	1065353216              # float 1
.LCPI14_2:
	.long	1566444395              # float 9.99999984E+17
	.text
	.globl	_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf
	.p2align	4, 0x90
	.type	_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf,@function
_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf: # @_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 48
	subq	$448, %rsp              # imm = 0x1C0
.Lcfi97:
	.cfi_def_cfa_offset 496
.Lcfi98:
	.cfi_offset %rbx, -48
.Lcfi99:
	.cfi_offset %r12, -40
.Lcfi100:
	.cfi_offset %r13, -32
.Lcfi101:
	.cfi_offset %r14, -24
.Lcfi102:
	.cfi_offset %r15, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%r8, %r14
	movq	%rcx, %r13
	movq	%rdx, %rbx
	movq	%rsi, %r12
	movq	%rdi, %r15
	movl	$.L.str.9, %edi
	callq	_ZN15CProfileManager13Start_ProfileEPKc
	movups	(%rbx), %xmm0
	movaps	%xmm0, 320(%rsp)
	movups	16(%rbx), %xmm0
	movaps	%xmm0, 336(%rsp)
	movups	32(%rbx), %xmm0
	movaps	%xmm0, 352(%rsp)
	movups	48(%rbx), %xmm0
	movaps	%xmm0, 368(%rsp)
	movups	(%r13), %xmm0
	movaps	%xmm0, 384(%rsp)
	movups	16(%r13), %xmm0
	movaps	%xmm0, 400(%rsp)
	movups	32(%r13), %xmm0
	movaps	%xmm0, 416(%rsp)
	movups	48(%r13), %xmm0
	movaps	%xmm0, 432(%rsp)
.Ltmp134:
	leaq	320(%rsp), %rdi
	leaq	384(%rsp), %rsi
	leaq	40(%rsp), %rdx
	leaq	16(%rsp), %rcx
	callq	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
.Ltmp135:
# BB#1:
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movsd	40(%rsp), %xmm1         # xmm1 = mem[0],zero
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm1, %xmm2
	mulss	48(%rsp), %xmm0
	xorps	%xmm1, %xmm1
	xorps	%xmm3, %xmm3
	movss	%xmm0, %xmm3            # xmm3 = xmm0[0],xmm3[1,2,3]
	movlps	%xmm2, 256(%rsp)
	movlps	%xmm3, 264(%rsp)
	movaps	%xmm1, 16(%rsp)
	movl	$1065353216, 40(%rsp)   # imm = 0x3F800000
	movups	%xmm1, 44(%rsp)
	movl	$1065353216, 60(%rsp)   # imm = 0x3F800000
	movups	%xmm1, 64(%rsp)
	movl	$1065353216, 80(%rsp)   # imm = 0x3F800000
	movups	%xmm1, 84(%rsp)
	movl	$0, 100(%rsp)
.Ltmp137:
	leaq	320(%rsp), %rdi
	leaq	272(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
.Ltmp138:
# BB#2:
	movsd	272(%rsp), %xmm1        # xmm1 = mem[0],zero
	movsd	280(%rsp), %xmm8        # xmm8 = mem[0],zero
	movaps	%xmm1, %xmm2
	mulss	%xmm2, %xmm2
	pshufd	$229, %xmm1, %xmm12     # xmm12 = xmm1[1,1,2,3]
	movdqa	%xmm12, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm8, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm3, %xmm2
	pshufd	$229, %xmm8, %xmm5      # xmm5 = xmm8[1,1,2,3]
	movdqa	%xmm5, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm2, %xmm3
	movss	.LCPI14_0(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm6
	movaps	%xmm1, %xmm2
	mulss	%xmm6, %xmm2
	movdqa	%xmm12, %xmm3
	mulss	%xmm6, %xmm3
	mulss	%xmm8, %xmm6
	movdqa	%xmm5, %xmm9
	mulss	%xmm2, %xmm9
	movdqa	%xmm5, %xmm10
	mulss	%xmm3, %xmm10
	mulss	%xmm6, %xmm5
	mulss	%xmm1, %xmm2
	movaps	%xmm1, %xmm7
	mulss	%xmm3, %xmm7
	mulss	%xmm6, %xmm1
	mulss	%xmm12, %xmm3
	mulss	%xmm6, %xmm12
	mulss	%xmm8, %xmm6
	movaps	%xmm3, %xmm4
	addss	%xmm6, %xmm4
	movss	.LCPI14_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm8
	subss	%xmm4, %xmm8
	movaps	%xmm7, %xmm11
	subss	%xmm5, %xmm11
	movaps	%xmm1, %xmm4
	addss	%xmm10, %xmm4
	addss	%xmm5, %xmm7
	addss	%xmm2, %xmm6
	movaps	%xmm0, %xmm5
	subss	%xmm6, %xmm5
	movaps	%xmm12, %xmm6
	subss	%xmm9, %xmm6
	subss	%xmm10, %xmm1
	addss	%xmm9, %xmm12
	addss	%xmm2, %xmm3
	movaps	%xmm0, %xmm2
	subss	%xmm3, %xmm2
	movss	%xmm8, 40(%rsp)
	movss	%xmm11, 44(%rsp)
	movss	%xmm4, 48(%rsp)
	movl	$0, 52(%rsp)
	movss	%xmm7, 56(%rsp)
	movss	%xmm5, 60(%rsp)
	movss	%xmm6, 64(%rsp)
	movl	$0, 68(%rsp)
	movss	%xmm1, 72(%rsp)
	movss	%xmm12, 76(%rsp)
	movss	%xmm2, 80(%rsp)
	movl	$0, 84(%rsp)
.Ltmp140:
	leaq	40(%rsp), %rsi
	leaq	16(%rsp), %rdx
	leaq	256(%rsp), %rcx
	leaq	304(%rsp), %r8
	leaq	288(%rsp), %r9
	movq	%r12, %rdi
	callq	_ZNK16btCollisionShape21calculateTemporalAabbERK11btTransformRK9btVector3S5_fRS3_S6_
.Ltmp141:
# BB#3:
	movq	$_ZTV21btSingleSweepCallback+16, 40(%rsp)
	movups	(%rbx), %xmm0
	leaq	16(%rbx), %rax
	leaq	32(%rbx), %rcx
	leaq	48(%rbx), %rdx
	movups	%xmm0, 80(%rsp)
	movups	(%rax), %xmm0
	movups	%xmm0, 96(%rsp)
	movups	(%rcx), %xmm0
	movups	%xmm0, 112(%rsp)
	movups	(%rdx), %xmm0
	movups	%xmm0, 128(%rsp)
	movups	(%r13), %xmm0
	leaq	16(%r13), %rax
	leaq	32(%r13), %rcx
	leaq	48(%r13), %rdx
	movups	%xmm0, 144(%rsp)
	movups	(%rax), %xmm0
	movups	%xmm0, 160(%rsp)
	movups	(%rcx), %xmm0
	movups	%xmm0, 176(%rsp)
	movups	(%rdx), %xmm0
	movups	%xmm0, 192(%rsp)
	movq	%r15, 224(%rsp)
	movq	%r14, 232(%rsp)
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 240(%rsp)
	movq	%r12, 248(%rsp)
	movss	192(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	196(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	subss	128(%rsp), %xmm2
	subss	132(%rsp), %xmm6
	movss	200(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	subss	136(%rsp), %xmm4
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB14_5
# BB#4:                                 # %call.sqrt
	movss	%xmm6, 4(%rsp)          # 4-byte Spill
	movss	%xmm2, 12(%rsp)         # 4-byte Spill
	movss	%xmm4, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm6          # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB14_5:                               # %.split
	movss	.LCPI14_1(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm9
	movaps	%xmm9, %xmm0
	divss	%xmm1, %xmm0
	movaps	%xmm2, %xmm10
	mulss	%xmm0, %xmm10
	movaps	%xmm6, %xmm7
	mulss	%xmm0, %xmm7
	movss	.LCPI14_2(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm5, %xmm5
	ucomiss	%xmm5, %xmm10
	movaps	%xmm3, %xmm1
	jne	.LBB14_6
	jnp	.LBB14_7
.LBB14_6:                               # %select.false.sink
	movaps	%xmm9, %xmm1
	divss	%xmm10, %xmm1
.LBB14_7:                               # %select.end
	mulss	%xmm4, %xmm0
	movss	%xmm1, 48(%rsp)
	ucomiss	%xmm5, %xmm7
	movaps	%xmm3, %xmm5
	jne	.LBB14_8
	jnp	.LBB14_9
.LBB14_8:                               # %select.false.sink51
	movaps	%xmm9, %xmm5
	divss	%xmm7, %xmm5
.LBB14_9:                               # %select.end50
	leaq	368(%rsp), %rsi
	leaq	432(%rsp), %rdx
	movss	%xmm5, 52(%rsp)
	xorps	%xmm8, %xmm8
	ucomiss	%xmm8, %xmm0
	jne	.LBB14_10
	jnp	.LBB14_11
.LBB14_10:                              # %select.false.sink53
	divss	%xmm0, %xmm9
	movaps	%xmm9, %xmm3
.LBB14_11:                              # %select.end52
	movss	%xmm3, 56(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm8
	seta	%al
	movl	%eax, 64(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm5, %xmm8
	seta	%al
	movl	%eax, 68(%rsp)
	xorl	%eax, %eax
	ucomiss	%xmm3, %xmm8
	seta	%al
	movl	%eax, 72(%rsp)
	mulss	%xmm10, %xmm2
	mulss	%xmm7, %xmm6
	addss	%xmm2, %xmm6
	mulss	%xmm0, %xmm4
	addss	%xmm6, %xmm4
	movss	%xmm4, 76(%rsp)
	movq	112(%r15), %rdi
	movq	(%rdi), %rax
.Ltmp143:
	leaq	40(%rsp), %rcx
	leaq	304(%rsp), %r8
	leaq	288(%rsp), %r9
	callq	*48(%rax)
.Ltmp144:
# BB#12:
	callq	_ZN15CProfileManager12Stop_ProfileEv
	addq	$448, %rsp              # imm = 0x1C0
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB14_16:
.Ltmp145:
	jmp	.LBB14_17
.LBB14_14:
.Ltmp142:
	jmp	.LBB14_17
.LBB14_15:
.Ltmp139:
	jmp	.LBB14_17
.LBB14_13:
.Ltmp136:
.LBB14_17:
	movq	%rax, %rbx
.Ltmp146:
	callq	_ZN15CProfileManager12Stop_ProfileEv
.Ltmp147:
# BB#18:                                # %_ZN14CProfileSampleD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_19:
.Ltmp148:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf, .Lfunc_end14-_ZNK16btCollisionWorld15convexSweepTestEPK13btConvexShapeRK11btTransformS5_RNS_20ConvexResultCallbackEf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp134-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp134
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp135-.Ltmp134       #   Call between .Ltmp134 and .Ltmp135
	.long	.Ltmp136-.Lfunc_begin8  #     jumps to .Ltmp136
	.byte	0                       #   On action: cleanup
	.long	.Ltmp137-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp138-.Ltmp137       #   Call between .Ltmp137 and .Ltmp138
	.long	.Ltmp139-.Lfunc_begin8  #     jumps to .Ltmp139
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin8  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin8  #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp144-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp146-.Ltmp144       #   Call between .Ltmp144 and .Ltmp146
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Ltmp147-.Ltmp146       #   Call between .Ltmp146 and .Ltmp147
	.long	.Ltmp148-.Lfunc_begin8  #     jumps to .Ltmp148
	.byte	1                       #   On action: 1
	.long	.Ltmp147-.Lfunc_begin8  # >> Call Site 8 <<
	.long	.Lfunc_end14-.Ltmp147   #   Call between .Ltmp147 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,"axG",@progbits,_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,comdat
	.weak	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,@function
_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw: # @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	movq	%rsi, 120(%rdi)
	retq
.Lfunc_end15:
	.size	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw, .Lfunc_end15-_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14getDebugDrawerEv,"axG",@progbits,_ZN16btCollisionWorld14getDebugDrawerEv,comdat
	.weak	_ZN16btCollisionWorld14getDebugDrawerEv
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14getDebugDrawerEv,@function
_ZN16btCollisionWorld14getDebugDrawerEv: # @_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_startproc
# BB#0:
	movq	120(%rdi), %rax
	retq
.Lfunc_end16:
	.size	_ZN16btCollisionWorld14getDebugDrawerEv, .Lfunc_end16-_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult9DebugDrawEf,"axG",@progbits,_ZN12btConvexCast10CastResult9DebugDrawEf,comdat
	.weak	_ZN12btConvexCast10CastResult9DebugDrawEf
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult9DebugDrawEf,@function
_ZN12btConvexCast10CastResult9DebugDrawEf: # @_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end17:
	.size	_ZN12btConvexCast10CastResult9DebugDrawEf, .Lfunc_end17-_ZN12btConvexCast10CastResult9DebugDrawEf
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,"axG",@progbits,_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,comdat
	.weak	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform,@function
_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform: # @_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end18:
	.size	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform, .Lfunc_end18-_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.cfi_endproc

	.section	.text._ZN12btConvexCast10CastResultD0Ev,"axG",@progbits,_ZN12btConvexCast10CastResultD0Ev,comdat
	.weak	_ZN12btConvexCast10CastResultD0Ev
	.p2align	4, 0x90
	.type	_ZN12btConvexCast10CastResultD0Ev,@function
_ZN12btConvexCast10CastResultD0Ev:      # @_ZN12btConvexCast10CastResultD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end19:
	.size	_ZN12btConvexCast10CastResultD0Ev, .Lfunc_end19-_ZN12btConvexCast10CastResultD0Ev
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0Ev,@function
_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0Ev: # @_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi103:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi104:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi105:
	.cfi_def_cfa_offset 32
.Lcfi106:
	.cfi_offset %rbx, -24
.Lcfi107:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp149:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp150:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB20_2:
.Ltmp151:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end20:
	.size	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0Ev, .Lfunc_end20-_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table20:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp149-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp150-.Ltmp149       #   Call between .Ltmp149 and .Ltmp150
	.long	.Ltmp151-.Lfunc_begin9  #     jumps to .Ltmp151
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Lfunc_end20-.Ltmp150   #   Call between .Ltmp150 and .Lfunc_end20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitERK9btVector3fii,@function
_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitERK9btVector3fii: # @_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitERK9btVector3fii
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi108:
	.cfi_def_cfa_offset 64
	movl	%edx, 8(%rsp)
	movl	%ecx, 12(%rsp)
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	76(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	movss	92(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	addps	%xmm4, %xmm5
	movss	96(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm5, %xmm4
	mulss	104(%rdi), %xmm3
	mulss	108(%rdi), %xmm2
	addss	%xmm3, %xmm2
	mulss	112(%rdi), %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movq	56(%rdi), %rax
	movq	%rax, 16(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 24(%rsp)
	movlps	%xmm4, 32(%rsp)
	movlps	%xmm2, 40(%rsp)
	movss	%xmm0, 48(%rsp)
	movq	48(%rdi), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	movl	$1, %edx
	callq	*24(%rax)
	addq	$56, %rsp
	retq
.Lfunc_end21:
	.size	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitERK9btVector3fii, .Lfunc_end21-_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitERK9btVector3fii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0E_0v,@function
_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0E_0v: # @_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0E_0v
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 32
.Lcfi112:
	.cfi_offset %rbx, -24
.Lcfi113:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp152:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp153:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_2:
.Ltmp154:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0E_0v, .Lfunc_end22-_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0E_0v
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp152-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp153-.Ltmp152       #   Call between .Ltmp152 and .Ltmp153
	.long	.Ltmp154-.Lfunc_begin10 #     jumps to .Ltmp154
	.byte	0                       #   On action: cleanup
	.long	.Ltmp153-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp153   #   Call between .Ltmp153 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitE_0RK9btVector3fii,@function
_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitE_0RK9btVector3fii: # @_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitE_0RK9btVector3fii
	.cfi_startproc
# BB#0:
	subq	$56, %rsp
.Lcfi114:
	.cfi_def_cfa_offset 64
	movl	%edx, 8(%rsp)
	movl	%ecx, 12(%rsp)
	movss	(%rsi), %xmm3           # xmm3 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movss	88(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	72(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	76(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm3, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	movss	92(%rdi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm6    # xmm6 = xmm6[0],xmm5[0],xmm6[1],xmm5[1]
	movaps	%xmm2, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm6, %xmm5
	addps	%xmm4, %xmm5
	movss	96(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	80(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	addps	%xmm5, %xmm4
	mulss	104(%rdi), %xmm3
	mulss	108(%rdi), %xmm2
	addss	%xmm3, %xmm2
	mulss	112(%rdi), %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movq	56(%rdi), %rax
	movq	%rax, 16(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 24(%rsp)
	movlps	%xmm4, 32(%rsp)
	movlps	%xmm2, 40(%rsp)
	movss	%xmm0, 48(%rsp)
	movq	48(%rdi), %rdi
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	movl	$1, %edx
	callq	*24(%rax)
	addq	$56, %rsp
	retq
.Lfunc_end23:
	.size	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitE_0RK9btVector3fii, .Lfunc_end23-_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitE_0RK9btVector3fii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0Ev,@function
_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0Ev: # @_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 32
.Lcfi118:
	.cfi_offset %rbx, -24
.Lcfi119:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp155:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp156:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB24_2:
.Ltmp157:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0Ev, .Lfunc_end24-_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp155-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp156-.Ltmp155       #   Call between .Ltmp155 and .Ltmp156
	.long	.Ltmp157-.Lfunc_begin11 #     jumps to .Ltmp157
	.byte	0                       #   On action: cleanup
	.long	.Ltmp156-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Lfunc_end24-.Ltmp156   #   Call between .Ltmp156 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitERK9btVector3SG_fii,@function
_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitERK9btVector3SG_fii: # @_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitERK9btVector3SG_fii
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Lcfi120:
	.cfi_def_cfa_offset 80
	movq	%rdi, %rax
	movl	%ecx, 8(%rsp)
	movl	%r8d, 12(%rsp)
	movq	216(%rax), %rdi
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB25_2
# BB#1:
	movq	224(%rax), %rax
	movq	%rax, 16(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 24(%rsp)
	movups	(%rsi), %xmm1
	movups	%xmm1, 32(%rsp)
	movups	(%rdx), %xmm1
	movups	%xmm1, 48(%rsp)
	movss	%xmm0, 64(%rsp)
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	movl	$1, %edx
	callq	*24(%rax)
.LBB25_2:
	addq	$72, %rsp
	retq
.Lfunc_end25:
	.size	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitERK9btVector3SG_fii, .Lfunc_end25-_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitERK9btVector3SG_fii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0E_0v,@function
_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0E_0v: # @_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0E_0v
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r14
.Lcfi121:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi122:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi123:
	.cfi_def_cfa_offset 32
.Lcfi124:
	.cfi_offset %rbx, -24
.Lcfi125:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp158:
	callq	_ZN18btTriangleCallbackD2Ev
.Ltmp159:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp160:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0E_0v, .Lfunc_end26-_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0E_0v
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp158-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp159-.Ltmp158       #   Call between .Ltmp158 and .Ltmp159
	.long	.Ltmp160-.Lfunc_begin12 #     jumps to .Ltmp160
	.byte	0                       #   On action: cleanup
	.long	.Ltmp159-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp159   #   Call between .Ltmp159 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.text
	.p2align	4, 0x90
	.type	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitE_0RK9btVector3SG_fii,@function
_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitE_0RK9btVector3SG_fii: # @_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitE_0RK9btVector3SG_fii
	.cfi_startproc
# BB#0:
	subq	$72, %rsp
.Lcfi126:
	.cfi_def_cfa_offset 80
	movq	%rdi, %rax
	movl	%ecx, 8(%rsp)
	movl	%r8d, 12(%rsp)
	movq	216(%rax), %rdi
	movss	8(%rdi), %xmm1          # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jb	.LBB27_2
# BB#1:
	movq	224(%rax), %rax
	movq	%rax, 16(%rsp)
	leaq	8(%rsp), %rax
	movq	%rax, 24(%rsp)
	movups	(%rsi), %xmm1
	movups	%xmm1, 32(%rsp)
	movups	(%rdx), %xmm1
	movups	%xmm1, 48(%rsp)
	movss	%xmm0, 64(%rsp)
	movq	(%rdi), %rax
	leaq	16(%rsp), %rsi
	xorl	%edx, %edx
	callq	*24(%rax)
.LBB27_2:
	addq	$72, %rsp
	retq
.Lfunc_end27:
	.size	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitE_0RK9btVector3SG_fii, .Lfunc_end27-_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitE_0RK9btVector3SG_fii
	.cfi_endproc

	.section	.text._ZN19btSingleRayCallbackD0Ev,"axG",@progbits,_ZN19btSingleRayCallbackD0Ev,comdat
	.weak	_ZN19btSingleRayCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN19btSingleRayCallbackD0Ev,@function
_ZN19btSingleRayCallbackD0Ev:           # @_ZN19btSingleRayCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end28:
	.size	_ZN19btSingleRayCallbackD0Ev, .Lfunc_end28-_ZN19btSingleRayCallbackD0Ev
	.cfi_endproc

	.section	.text._ZN19btSingleRayCallback7processEPK17btBroadphaseProxy,"axG",@progbits,_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy,comdat
	.weak	_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy
	.p2align	4, 0x90
	.type	_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy,@function
_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy: # @_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi129:
	.cfi_def_cfa_offset 32
.Lcfi130:
	.cfi_offset %rbx, -32
.Lcfi131:
	.cfi_offset %r14, -24
.Lcfi132:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	224(%rbx), %rdi
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB29_2
	jp	.LBB29_2
# BB#1:
	xorl	%ebp, %ebp
	jmp	.LBB29_4
.LBB29_2:
	movq	(%rsi), %r14
	movq	(%rdi), %rax
	movq	192(%r14), %rsi
	callq	*16(%rax)
	movb	$1, %bpl
	testb	%al, %al
	je	.LBB29_4
# BB#3:
	movq	200(%r14), %rcx
	movq	224(%rbx), %r9
	leaq	72(%rbx), %rdi
	leaq	136(%rbx), %rsi
	leaq	8(%r14), %r8
	movq	%r14, %rdx
	callq	_ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackE
.LBB29_4:
	movl	%ebp, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end29:
	.size	_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy, .Lfunc_end29-_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI30_0:
	.long	1065353216              # float 1
.LCPI30_1:
	.long	679477248               # float 1.42108547E-14
	.section	.text._ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,"axG",@progbits,_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,comdat
	.weak	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,@function
_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf: # @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi133:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Lcfi135:
	.cfi_def_cfa_offset 112
.Lcfi136:
	.cfi_offset %rbx, -24
.Lcfi137:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movss	20(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	40(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm4, %xmm0
	movss	24(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm14, %xmm1
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm5
	mulss	%xmm1, %xmm5
	movss	16(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm5
	movaps	%xmm14, %xmm15
	mulss	%xmm11, %xmm15
	movaps	%xmm8, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm15
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	4(%rdi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movss	8(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm14, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm12, %xmm10
	mulss	%xmm13, %xmm10
	movaps	%xmm8, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm1, %xmm9
	mulss	%xmm6, %xmm9
	mulss	%xmm11, %xmm6
	mulss	%xmm13, %xmm1
	mulss	%xmm13, %xmm11
	mulss	%xmm5, %xmm13
	addss	36(%rsp), %xmm13        # 4-byte Folded Reload
	addss	%xmm13, %xmm2
	movss	.LCPI30_0(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm13
	subss	%xmm3, %xmm7
	subss	%xmm0, %xmm10
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	mulss	%xmm13, %xmm7
	mulss	%xmm13, %xmm10
	mulss	%xmm13, %xmm5
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	subss	%xmm9, %xmm4
	mulss	%xmm13, %xmm4
	mulss	%xmm0, %xmm12
	subss	%xmm12, %xmm6
	mulss	%xmm13, %xmm6
	mulss	%xmm13, %xmm15
	mulss	%xmm0, %xmm14
	subss	%xmm14, %xmm1
	mulss	%xmm13, %xmm1
	mulss	%xmm0, %xmm8
	subss	%xmm11, %xmm8
	mulss	%xmm13, %xmm8
	movss	(%rsi), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm9
	mulss	%xmm3, %xmm9
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm9, %xmm2
	movss	8(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm12, %xmm11
	mulss	%xmm7, %xmm11
	movaps	%xmm13, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm11, %xmm2
	movaps	%xmm9, %xmm11
	mulss	%xmm1, %xmm11
	addss	%xmm2, %xmm11
	mulss	%xmm10, %xmm12
	mulss	%xmm6, %xmm13
	addss	%xmm12, %xmm13
	mulss	%xmm8, %xmm9
	addss	%xmm13, %xmm9
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm12
	mulss	%xmm2, %xmm12
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm13
	mulss	%xmm0, %xmm13
	addss	%xmm12, %xmm13
	movss	24(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm12
	mulss	%xmm14, %xmm12
	addss	%xmm13, %xmm12
	movaps	%xmm7, %xmm13
	mulss	%xmm2, %xmm13
	movaps	%xmm4, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm13, %xmm3
	movaps	%xmm1, %xmm13
	mulss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	mulss	%xmm10, %xmm2
	mulss	%xmm6, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm8, %xmm14
	addss	%xmm0, %xmm14
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm0, %xmm10
	mulss	%xmm2, %xmm6
	addss	%xmm10, %xmm6
	mulss	%xmm3, %xmm8
	addss	%xmm6, %xmm8
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rsp)
	movss	%xmm11, 44(%rsp)
	movss	%xmm9, 48(%rsp)
	movl	$0, 52(%rsp)
	movss	%xmm12, 56(%rsp)
	movss	%xmm13, 60(%rsp)
	movss	%xmm14, 64(%rsp)
	movl	$0, 68(%rsp)
	movss	%xmm15, 72(%rsp)
	movss	%xmm1, 76(%rsp)
	movss	%xmm8, 80(%rsp)
	movl	$0, 84(%rsp)
	leaq	40(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB30_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB30_2:                               # %.split
	movss	.LCPI30_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	mulps	16(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	shufps	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	movl	16(%rsp), %eax
	movl	20(%rsp), %ecx
	movl	24(%rsp), %edx
	movl	%eax, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	%edx, 8(%rbx)
	movl	$0, 12(%rbx)
	movd	%eax, %xmm0
	mulss	%xmm0, %xmm0
	movd	%ecx, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%edx, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI30_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB30_4
# BB#3:
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	movq	$0, 8(%rbx)
	jmp	.LBB30_7
.LBB30_4:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB30_6
# BB#5:                                 # %call.sqrt52
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB30_6:                               # %.split51
	movss	.LCPI30_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 4(%rbx)
	mulss	8(%rbx), %xmm2
	movss	%xmm2, 8(%rbx)
.LBB30_7:
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end30:
	.size	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf, .Lfunc_end30-_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI31_0:
	.long	1065353216              # float 1
.LCPI31_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi138:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi139:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi141:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi142:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi143:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi144:
	.cfi_def_cfa_offset 80
.Lcfi145:
	.cfi_offset %rbx, -56
.Lcfi146:
	.cfi_offset %r12, -48
.Lcfi147:
	.cfi_offset %r13, -40
.Lcfi148:
	.cfi_offset %r14, -32
.Lcfi149:
	.cfi_offset %r15, -24
.Lcfi150:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB31_4
# BB#1:
	addss	.LCPI31_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB31_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB31_3:                               # %.split
	movss	.LCPI31_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB31_7
.LBB31_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI31_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB31_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB31_6:                               # %.split71
	movss	.LCPI31_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB31_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end31:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end31-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.section	.text._ZN21btSingleSweepCallbackD0Ev,"axG",@progbits,_ZN21btSingleSweepCallbackD0Ev,comdat
	.weak	_ZN21btSingleSweepCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN21btSingleSweepCallbackD0Ev,@function
_ZN21btSingleSweepCallbackD0Ev:         # @_ZN21btSingleSweepCallbackD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end32:
	.size	_ZN21btSingleSweepCallbackD0Ev, .Lfunc_end32-_ZN21btSingleSweepCallbackD0Ev
	.cfi_endproc

	.section	.text._ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy,"axG",@progbits,_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy,comdat
	.weak	_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy
	.p2align	4, 0x90
	.type	_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy,@function
_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy: # @_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi154:
	.cfi_def_cfa_offset 48
.Lcfi155:
	.cfi_offset %rbx, -32
.Lcfi156:
	.cfi_offset %r14, -24
.Lcfi157:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	192(%rbx), %rdi
	movss	8(%rdi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB33_2
	jp	.LBB33_2
# BB#1:
	xorl	%ebp, %ebp
	jmp	.LBB33_4
.LBB33_2:
	movq	(%rsi), %r14
	movq	(%rdi), %rax
	movq	192(%r14), %rsi
	callq	*16(%rax)
	movb	$1, %bpl
	testb	%al, %al
	je	.LBB33_4
# BB#3:
	movq	208(%rbx), %rdi
	movq	200(%r14), %r8
	movq	192(%rbx), %rax
	movss	200(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	leaq	40(%rbx), %rsi
	leaq	104(%rbx), %rdx
	movq	%rax, (%rsp)
	leaq	8(%r14), %r9
	movq	%r14, %rcx
	callq	_ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEf
.LBB33_4:
	movl	%ebp, %eax
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end33:
	.size	_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy, .Lfunc_end33-_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy
	.cfi_endproc

	.type	_ZTV16btCollisionWorld,@object # @_ZTV16btCollisionWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTV16btCollisionWorld
	.p2align	3
_ZTV16btCollisionWorld:
	.quad	0
	.quad	_ZTI16btCollisionWorld
	.quad	_ZN16btCollisionWorldD2Ev
	.quad	_ZN16btCollisionWorldD0Ev
	.quad	_ZN16btCollisionWorld11updateAabbsEv
	.quad	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.quad	_ZN16btCollisionWorld14getDebugDrawerEv
	.quad	_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss
	.quad	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject
	.quad	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
	.size	_ZTV16btCollisionWorld, 80

	.type	_ZZN16btCollisionWorld16updateSingleAabbEP17btCollisionObjectE8reportMe,@object # @_ZZN16btCollisionWorld16updateSingleAabbEP17btCollisionObjectE8reportMe
	.local	_ZZN16btCollisionWorld16updateSingleAabbEP17btCollisionObjectE8reportMe
	.comm	_ZZN16btCollisionWorld16updateSingleAabbEP17btCollisionObjectE8reportMe,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Overflow in AABB, object removed from simulation"
	.size	.L.str, 49

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"If you can reproduce this, please email bugs@continuousphysics.com\n"
	.size	.L.str.1, 68

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Please include above information, your Platform, version of OS.\n"
	.size	.L.str.2, 65

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Thanks.\n"
	.size	.L.str.3, 9

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"updateAabbs"
	.size	.L.str.4, 12

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"performDiscreteCollisionDetection"
	.size	.L.str.5, 34

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"calculateOverlappingPairs"
	.size	.L.str.6, 26

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"dispatchAllCollisionPairs"
	.size	.L.str.7, 26

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"convexSweepCompound"
	.size	.L.str.8, 20

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"convexSweepTest"
	.size	.L.str.9, 16

	.type	_ZTS16btCollisionWorld,@object # @_ZTS16btCollisionWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTS16btCollisionWorld
	.p2align	4
_ZTS16btCollisionWorld:
	.asciz	"16btCollisionWorld"
	.size	_ZTS16btCollisionWorld, 19

	.type	_ZTI16btCollisionWorld,@object # @_ZTI16btCollisionWorld
	.globl	_ZTI16btCollisionWorld
	.p2align	3
_ZTI16btCollisionWorld:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS16btCollisionWorld
	.size	_ZTI16btCollisionWorld, 16

	.type	_ZTVN12btConvexCast10CastResultE,@object # @_ZTVN12btConvexCast10CastResultE
	.section	.rodata._ZTVN12btConvexCast10CastResultE,"aG",@progbits,_ZTVN12btConvexCast10CastResultE,comdat
	.weak	_ZTVN12btConvexCast10CastResultE
	.p2align	3
_ZTVN12btConvexCast10CastResultE:
	.quad	0
	.quad	_ZTIN12btConvexCast10CastResultE
	.quad	_ZN12btConvexCast10CastResult9DebugDrawEf
	.quad	_ZN12btConvexCast10CastResult15drawCoordSystemERK11btTransform
	.quad	_ZN12btConvexCast10CastResultD2Ev
	.quad	_ZN12btConvexCast10CastResultD0Ev
	.size	_ZTVN12btConvexCast10CastResultE, 48

	.type	_ZTSN12btConvexCast10CastResultE,@object # @_ZTSN12btConvexCast10CastResultE
	.section	.rodata._ZTSN12btConvexCast10CastResultE,"aG",@progbits,_ZTSN12btConvexCast10CastResultE,comdat
	.weak	_ZTSN12btConvexCast10CastResultE
	.p2align	4
_ZTSN12btConvexCast10CastResultE:
	.asciz	"N12btConvexCast10CastResultE"
	.size	_ZTSN12btConvexCast10CastResultE, 29

	.type	_ZTIN12btConvexCast10CastResultE,@object # @_ZTIN12btConvexCast10CastResultE
	.section	.rodata._ZTIN12btConvexCast10CastResultE,"aG",@progbits,_ZTIN12btConvexCast10CastResultE,comdat
	.weak	_ZTIN12btConvexCast10CastResultE
	.p2align	3
_ZTIN12btConvexCast10CastResultE:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTSN12btConvexCast10CastResultE
	.size	_ZTIN12btConvexCast10CastResultE, 16

	.type	_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback,@object # @_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback
	.section	.rodata,"a",@progbits
	.p2align	3
_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback:
	.quad	0
	.quad	_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0Ev
	.quad	_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii
	.quad	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitERK9btVector3fii
	.size	_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback, 48

	.type	_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback,@object # @_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback
	.p2align	4
_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback:
	.asciz	"ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback"
	.size	_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback, 154

	.type	_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback,@object # @_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback
	.p2align	4
_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback
	.quad	_ZTI25btTriangleRaycastCallback
	.size	_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback, 24

	.type	_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0,@object # @_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0
	.p2align	3
_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0:
	.quad	0
	.quad	_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallbackD0E_0v
	.quad	_ZN25btTriangleRaycastCallback15processTriangleEP9btVector3ii
	.quad	_ZZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEEN29BridgeTriangleRaycastCallback9reportHitE_0RK9btVector3fii
	.size	_ZTVZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0, 48

	.type	_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0,@object # @_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0
	.p2align	4
_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0:
	.asciz	"ZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0"
	.size	_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0, 156

	.type	_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0,@object # @_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0
	.p2align	4
_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0
	.quad	_ZTI25btTriangleRaycastCallback
	.size	_ZTIZN16btCollisionWorld13rayTestSingleERK11btTransformS2_P17btCollisionObjectPK16btCollisionShapeS2_RNS_17RayResultCallbackEE29BridgeTriangleRaycastCallback_0, 24

	.type	_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback,@object # @_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback
	.p2align	3
_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback:
	.quad	0
	.quad	_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0Ev
	.quad	_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii
	.quad	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitERK9btVector3SG_fii
	.size	_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback, 48

	.type	_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback,@object # @_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback
	.p2align	4
_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback:
	.asciz	"ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback"
	.size	_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback, 182

	.type	_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback,@object # @_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback
	.p2align	4
_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback
	.quad	_ZTI28btTriangleConvexcastCallback
	.size	_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback, 24

	.type	_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0,@object # @_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0
	.p2align	3
_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0:
	.quad	0
	.quad	_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0
	.quad	_ZN18btTriangleCallbackD2Ev
	.quad	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallbackD0E_0v
	.quad	_ZN28btTriangleConvexcastCallback15processTriangleEP9btVector3ii
	.quad	_ZZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfEN32BridgeTriangleConvexcastCallback9reportHitE_0RK9btVector3SG_fii
	.size	_ZTVZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0, 48

	.type	_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0,@object # @_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0
	.p2align	4
_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0:
	.asciz	"ZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0"
	.size	_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0, 184

	.type	_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0,@object # @_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0
	.p2align	4
_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0
	.quad	_ZTI28btTriangleConvexcastCallback
	.size	_ZTIZN16btCollisionWorld17objectQuerySingleEPK13btConvexShapeRK11btTransformS5_P17btCollisionObjectPK16btCollisionShapeS5_RNS_20ConvexResultCallbackEfE32BridgeTriangleConvexcastCallback_0, 24

	.type	_ZTV19btSingleRayCallback,@object # @_ZTV19btSingleRayCallback
	.section	.rodata._ZTV19btSingleRayCallback,"aG",@progbits,_ZTV19btSingleRayCallback,comdat
	.weak	_ZTV19btSingleRayCallback
	.p2align	3
_ZTV19btSingleRayCallback:
	.quad	0
	.quad	_ZTI19btSingleRayCallback
	.quad	_ZN23btBroadphaseRayCallbackD2Ev
	.quad	_ZN19btSingleRayCallbackD0Ev
	.quad	_ZN19btSingleRayCallback7processEPK17btBroadphaseProxy
	.size	_ZTV19btSingleRayCallback, 40

	.type	_ZTS19btSingleRayCallback,@object # @_ZTS19btSingleRayCallback
	.section	.rodata._ZTS19btSingleRayCallback,"aG",@progbits,_ZTS19btSingleRayCallback,comdat
	.weak	_ZTS19btSingleRayCallback
	.p2align	4
_ZTS19btSingleRayCallback:
	.asciz	"19btSingleRayCallback"
	.size	_ZTS19btSingleRayCallback, 22

	.type	_ZTS23btBroadphaseRayCallback,@object # @_ZTS23btBroadphaseRayCallback
	.section	.rodata._ZTS23btBroadphaseRayCallback,"aG",@progbits,_ZTS23btBroadphaseRayCallback,comdat
	.weak	_ZTS23btBroadphaseRayCallback
	.p2align	4
_ZTS23btBroadphaseRayCallback:
	.asciz	"23btBroadphaseRayCallback"
	.size	_ZTS23btBroadphaseRayCallback, 26

	.type	_ZTI23btBroadphaseRayCallback,@object # @_ZTI23btBroadphaseRayCallback
	.section	.rodata._ZTI23btBroadphaseRayCallback,"aG",@progbits,_ZTI23btBroadphaseRayCallback,comdat
	.weak	_ZTI23btBroadphaseRayCallback
	.p2align	3
_ZTI23btBroadphaseRayCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS23btBroadphaseRayCallback
	.size	_ZTI23btBroadphaseRayCallback, 16

	.type	_ZTI19btSingleRayCallback,@object # @_ZTI19btSingleRayCallback
	.section	.rodata._ZTI19btSingleRayCallback,"aG",@progbits,_ZTI19btSingleRayCallback,comdat
	.weak	_ZTI19btSingleRayCallback
	.p2align	4
_ZTI19btSingleRayCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS19btSingleRayCallback
	.quad	_ZTI23btBroadphaseRayCallback
	.size	_ZTI19btSingleRayCallback, 24

	.type	_ZTV21btSingleSweepCallback,@object # @_ZTV21btSingleSweepCallback
	.section	.rodata._ZTV21btSingleSweepCallback,"aG",@progbits,_ZTV21btSingleSweepCallback,comdat
	.weak	_ZTV21btSingleSweepCallback
	.p2align	3
_ZTV21btSingleSweepCallback:
	.quad	0
	.quad	_ZTI21btSingleSweepCallback
	.quad	_ZN23btBroadphaseRayCallbackD2Ev
	.quad	_ZN21btSingleSweepCallbackD0Ev
	.quad	_ZN21btSingleSweepCallback7processEPK17btBroadphaseProxy
	.size	_ZTV21btSingleSweepCallback, 40

	.type	_ZTS21btSingleSweepCallback,@object # @_ZTS21btSingleSweepCallback
	.section	.rodata._ZTS21btSingleSweepCallback,"aG",@progbits,_ZTS21btSingleSweepCallback,comdat
	.weak	_ZTS21btSingleSweepCallback
	.p2align	4
_ZTS21btSingleSweepCallback:
	.asciz	"21btSingleSweepCallback"
	.size	_ZTS21btSingleSweepCallback, 24

	.type	_ZTI21btSingleSweepCallback,@object # @_ZTI21btSingleSweepCallback
	.section	.rodata._ZTI21btSingleSweepCallback,"aG",@progbits,_ZTI21btSingleSweepCallback,comdat
	.weak	_ZTI21btSingleSweepCallback
	.p2align	4
_ZTI21btSingleSweepCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21btSingleSweepCallback
	.quad	_ZTI23btBroadphaseRayCallback
	.size	_ZTI21btSingleSweepCallback, 24


	.globl	_ZN16btCollisionWorldC1EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration
	.type	_ZN16btCollisionWorldC1EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration,@function
_ZN16btCollisionWorldC1EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration = _ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration
	.globl	_ZN16btCollisionWorldD1Ev
	.type	_ZN16btCollisionWorldD1Ev,@function
_ZN16btCollisionWorldD1Ev = _ZN16btCollisionWorldD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
