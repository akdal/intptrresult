	.text
	.file	"btTriangleIndexVertexMaterialArray.bc"
	.globl	_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i
	.p2align	4, 0x90
	.type	_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i,@function
_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i: # @_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	144(%rsp), %r15
	movl	136(%rsp), %r12d
	movq	128(%rsp), %r13
	movl	120(%rsp), %r14d
	movl	112(%rsp), %eax
	movl	%eax, (%rsp)
	callq	_ZN26btTriangleIndexVertexArrayC2EiPiiiPfi
	movq	$_ZTV34btTriangleIndexVertexMaterialArray+16, (%rbx)
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movl	%r14d, 8(%rsp)
	movq	%r13, 16(%rsp)
	movl	%r12d, 24(%rsp)
	movl	$0, 28(%rsp)
	movl	%ebp, 32(%rsp)
	movq	%r15, 40(%rsp)
	movl	152(%rsp), %eax
	movl	%eax, 48(%rsp)
	movl	$2, 52(%rsp)
.Ltmp0:
	leaq	8(%rsp), %rsi
	movl	$2, %edx
	movq	%rbx, %rdi
	callq	_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType
.Ltmp1:
# BB#1:
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_2:
.Ltmp2:
	movq	%rax, %r14
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#3:
	cmpb	$0, 128(%rbx)
	je	.LBB0_5
# BB#4:
.Ltmp3:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp4:
.LBB0_5:                                # %.noexc
	movq	$0, 120(%rbx)
.LBB0_6:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.Ltmp5:
	movq	%rbx, %rdi
	callq	_ZN26btTriangleIndexVertexArrayD2Ev
.Ltmp6:
# BB#7:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_8:
.Ltmp7:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i, .Lfunc_end0-_ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp3           #   Call between .Ltmp3 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	1                       #   On action: 1
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp6      #   Call between .Ltmp6 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType,"axG",@progbits,_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType,comdat
	.weak	_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType
	.p2align	4, 0x90
	.type	_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType,@function
_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType: # @_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 48
.Lcfi18:
	.cfi_offset %rbx, -48
.Lcfi19:
	.cfi_offset %r12, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	108(%rbx), %eax
	cmpl	112(%rbx), %eax
	jne	.LBB1_18
# BB#1:
	leal	(%rax,%rax), %ecx
	testl	%eax, %eax
	movl	$1, %r12d
	cmovnel	%ecx, %r12d
	cmpl	%r12d, %eax
	jge	.LBB1_18
# BB#2:
	testl	%r12d, %r12d
	je	.LBB1_3
# BB#4:
	movslq	%r12d, %rax
	shlq	$4, %rax
	leaq	(%rax,%rax,2), %rdi
	movl	$16, %esi
	callq	_Z22btAlignedAllocInternalmi
	movq	%rax, %rbp
	movl	108(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB1_6
	jmp	.LBB1_13
.LBB1_3:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	jle	.LBB1_13
.LBB1_6:                                # %.lr.ph.i.i.i
	cltq
	leaq	-1(%rax), %r8
	movq	%rax, %rsi
	andq	$3, %rsi
	je	.LBB1_7
# BB#8:                                 # %.prol.preheader
	xorl	%edi, %edi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_9:                                # =>This Inner Loop Header: Depth=1
	movq	120(%rbx), %rcx
	movups	(%rcx,%rdi), %xmm0
	movups	16(%rcx,%rdi), %xmm1
	movups	32(%rcx,%rdi), %xmm2
	movups	%xmm2, 32(%rbp,%rdi)
	movups	%xmm1, 16(%rbp,%rdi)
	movups	%xmm0, (%rbp,%rdi)
	incq	%rdx
	addq	$48, %rdi
	cmpq	%rdx, %rsi
	jne	.LBB1_9
	jmp	.LBB1_10
.LBB1_7:
	xorl	%edx, %edx
.LBB1_10:                               # %.prol.loopexit
	cmpq	$3, %r8
	jb	.LBB1_13
# BB#11:                                # %.lr.ph.i.i.i.new
	subq	%rdx, %rax
	leaq	(%rdx,%rdx,2), %rcx
	shlq	$4, %rcx
	addq	$144, %rcx
	.p2align	4, 0x90
.LBB1_12:                               # =>This Inner Loop Header: Depth=1
	movq	120(%rbx), %rdx
	movups	-144(%rdx,%rcx), %xmm0
	movups	-128(%rdx,%rcx), %xmm1
	movups	-112(%rdx,%rcx), %xmm2
	movups	%xmm2, -112(%rbp,%rcx)
	movups	%xmm1, -128(%rbp,%rcx)
	movups	%xmm0, -144(%rbp,%rcx)
	movq	120(%rbx), %rdx
	movups	-96(%rdx,%rcx), %xmm0
	movups	-80(%rdx,%rcx), %xmm1
	movups	-64(%rdx,%rcx), %xmm2
	movups	%xmm2, -64(%rbp,%rcx)
	movups	%xmm1, -80(%rbp,%rcx)
	movups	%xmm0, -96(%rbp,%rcx)
	movq	120(%rbx), %rdx
	movups	-48(%rdx,%rcx), %xmm0
	movups	-32(%rdx,%rcx), %xmm1
	movups	-16(%rdx,%rcx), %xmm2
	movups	%xmm2, -16(%rbp,%rcx)
	movups	%xmm1, -32(%rbp,%rcx)
	movups	%xmm0, -48(%rbp,%rcx)
	movq	120(%rbx), %rdx
	movups	(%rdx,%rcx), %xmm0
	movups	16(%rdx,%rcx), %xmm1
	movups	32(%rdx,%rcx), %xmm2
	movups	%xmm2, 32(%rbp,%rcx)
	movups	%xmm1, 16(%rbp,%rcx)
	movups	%xmm0, (%rbp,%rcx)
	addq	$192, %rcx
	addq	$-4, %rax
	jne	.LBB1_12
.LBB1_13:                               # %_ZNK20btAlignedObjectArrayI20btMaterialPropertiesE4copyEiiPS0_.exit.i.i
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_17
# BB#14:
	cmpb	$0, 128(%rbx)
	je	.LBB1_16
# BB#15:
	callq	_Z21btAlignedFreeInternalPv
.LBB1_16:
	movq	$0, 120(%rbx)
.LBB1_17:                               # %_ZN20btAlignedObjectArrayI20btMaterialPropertiesE10deallocateEv.exit.i.i
	movb	$1, 128(%rbx)
	movq	%rbp, 120(%rbx)
	movl	%r12d, 112(%rbx)
	movl	108(%rbx), %eax
.LBB1_18:                               # %_ZN20btAlignedObjectArrayI20btMaterialPropertiesE9push_backERKS0_.exit
	movq	120(%rbx), %rcx
	cltq
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movups	(%r15), %xmm0
	movups	16(%r15), %xmm1
	movups	32(%r15), %xmm2
	movups	%xmm2, 32(%rcx,%rax)
	movups	%xmm1, 16(%rcx,%rax)
	movups	%xmm0, (%rcx,%rax)
	movslq	108(%rbx), %rax
	leal	1(%rax), %ecx
	movl	%ecx, 108(%rbx)
	movq	120(%rbx), %rcx
	leaq	(%rax,%rax,2), %rax
	shlq	$4, %rax
	movl	%r14d, 44(%rcx,%rax)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType, .Lfunc_end1-_ZN34btTriangleIndexVertexMaterialArray21addMaterialPropertiesERK20btMaterialProperties14PHY_ScalarType
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.p2align	4, 0x90
	.type	_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i,@function
_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i: # @_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r10
	movq	32(%rsp), %r11
	movq	24(%rsp), %rax
	movq	120(%rdi), %rdi
	movslq	48(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$4, %rbx
	movl	(%rdi,%rbx), %ebp
	movl	%ebp, (%rdx)
	movq	8(%rdi,%rbx), %rdx
	movq	%rdx, (%rsi)
	movl	$0, (%rcx)
	movl	16(%rdi,%rbx), %ecx
	movl	%ecx, (%r8)
	movl	24(%rdi,%rbx), %ecx
	movl	%ecx, (%rax)
	movq	32(%rdi,%rbx), %rax
	movq	%rax, (%r9)
	movl	40(%rdi,%rbx), %eax
	movl	%eax, (%r11)
	movl	44(%rdi,%rbx), %eax
	movl	%eax, (%r10)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end3:
	.size	_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i, .Lfunc_end3-_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.cfi_endproc

	.globl	_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.p2align	4, 0x90
	.type	_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i,@function
_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i: # @_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 24
.Lcfi29:
	.cfi_offset %rbx, -24
.Lcfi30:
	.cfi_offset %rbp, -16
	movq	40(%rsp), %r10
	movq	32(%rsp), %r11
	movq	24(%rsp), %rax
	movq	120(%rdi), %rdi
	movslq	48(%rsp), %rbx
	leaq	(%rbx,%rbx,2), %rbx
	shlq	$4, %rbx
	movl	(%rdi,%rbx), %ebp
	movl	%ebp, (%rdx)
	movq	8(%rdi,%rbx), %rdx
	movq	%rdx, (%rsi)
	movl	$0, (%rcx)
	movl	16(%rdi,%rbx), %ecx
	movl	%ecx, (%r8)
	movl	24(%rdi,%rbx), %ecx
	movl	%ecx, (%rax)
	movq	32(%rdi,%rbx), %rax
	movq	%rax, (%r9)
	movl	40(%rdi,%rbx), %eax
	movl	%eax, (%r11)
	movl	44(%rdi,%rbx), %eax
	movl	%eax, (%r10)
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i, .Lfunc_end4-_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.cfi_endproc

	.section	.text._ZN34btTriangleIndexVertexMaterialArrayD2Ev,"axG",@progbits,_ZN34btTriangleIndexVertexMaterialArrayD2Ev,comdat
	.weak	_ZN34btTriangleIndexVertexMaterialArrayD2Ev
	.p2align	4, 0x90
	.type	_ZN34btTriangleIndexVertexMaterialArrayD2Ev,@function
_ZN34btTriangleIndexVertexMaterialArrayD2Ev: # @_ZN34btTriangleIndexVertexMaterialArrayD2Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV34btTriangleIndexVertexMaterialArray+16, (%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_4
# BB#1:
	cmpb	$0, 128(%rbx)
	je	.LBB5_3
# BB#2:
.Ltmp8:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp9:
.LBB5_3:                                # %.noexc
	movq	$0, 120(%rbx)
.LBB5_4:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN26btTriangleIndexVertexArrayD2Ev # TAILCALL
.LBB5_5:
.Ltmp10:
	movq	%rax, %r14
.Ltmp11:
	movq	%rbx, %rdi
	callq	_ZN26btTriangleIndexVertexArrayD2Ev
.Ltmp12:
# BB#6:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB5_7:
.Ltmp13:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end5:
	.size	_ZN34btTriangleIndexVertexMaterialArrayD2Ev, .Lfunc_end5-_ZN34btTriangleIndexVertexMaterialArrayD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table5:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp8-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin1   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp11-.Ltmp9          #   Call between .Ltmp9 and .Ltmp11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin1   #     jumps to .Ltmp13
	.byte	1                       #   On action: 1
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end5-.Ltmp12     #   Call between .Ltmp12 and .Lfunc_end5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN34btTriangleIndexVertexMaterialArrayD0Ev,"axG",@progbits,_ZN34btTriangleIndexVertexMaterialArrayD0Ev,comdat
	.weak	_ZN34btTriangleIndexVertexMaterialArrayD0Ev
	.p2align	4, 0x90
	.type	_ZN34btTriangleIndexVertexMaterialArrayD0Ev,@function
_ZN34btTriangleIndexVertexMaterialArrayD0Ev: # @_ZN34btTriangleIndexVertexMaterialArrayD0Ev
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%r14
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 32
.Lcfi39:
	.cfi_offset %rbx, -24
.Lcfi40:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV34btTriangleIndexVertexMaterialArray+16, (%rbx)
	movq	120(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB6_4
# BB#1:
	cmpb	$0, 128(%rbx)
	je	.LBB6_3
# BB#2:
.Ltmp14:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp15:
.LBB6_3:                                # %.noexc.i
	movq	$0, 120(%rbx)
.LBB6_4:
	movb	$1, 128(%rbx)
	movq	$0, 120(%rbx)
	movq	$0, 108(%rbx)
.Ltmp20:
	movq	%rbx, %rdi
	callq	_ZN26btTriangleIndexVertexArrayD2Ev
.Ltmp21:
# BB#5:                                 # %_ZN34btTriangleIndexVertexMaterialArrayD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB6_6:
.Ltmp16:
	movq	%rax, %r14
.Ltmp17:
	movq	%rbx, %rdi
	callq	_ZN26btTriangleIndexVertexArrayD2Ev
.Ltmp18:
	jmp	.LBB6_9
.LBB6_7:
.Ltmp19:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB6_8:
.Ltmp22:
	movq	%rax, %r14
.LBB6_9:                                # %.body
.Ltmp23:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp24:
# BB#10:                                # %_ZN34btTriangleIndexVertexMaterialArraydlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB6_11:
.Ltmp25:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end6:
	.size	_ZN34btTriangleIndexVertexMaterialArrayD0Ev, .Lfunc_end6-_ZN34btTriangleIndexVertexMaterialArrayD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\326\200\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Ltmp14-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin2   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin2   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp17-.Ltmp21         #   Call between .Ltmp21 and .Ltmp17
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin2   #     jumps to .Ltmp19
	.byte	1                       #   On action: 1
	.long	.Ltmp23-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp25-.Lfunc_begin2   #     jumps to .Ltmp25
	.byte	1                       #   On action: 1
	.long	.Ltmp24-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Lfunc_end6-.Ltmp24     #   Call between .Ltmp24 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,"axG",@progbits,_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,comdat
	.weak	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi,@function
_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi: # @_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi, .Lfunc_end7-_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.cfi_endproc

	.section	.text._ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,"axG",@progbits,_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,comdat
	.weak	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi,@function
_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi: # @_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi, .Lfunc_end8-_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.cfi_endproc

	.section	.text._ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,"axG",@progbits,_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,comdat
	.weak	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.p2align	4, 0x90
	.type	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv,@function
_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv: # @_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.cfi_startproc
# BB#0:
	movl	28(%rdi), %eax
	retq
.Lfunc_end9:
	.size	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv, .Lfunc_end9-_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.cfi_endproc

	.section	.text._ZN26btTriangleIndexVertexArray19preallocateVerticesEi,"axG",@progbits,_ZN26btTriangleIndexVertexArray19preallocateVerticesEi,comdat
	.weak	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi,@function
_ZN26btTriangleIndexVertexArray19preallocateVerticesEi: # @_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi, .Lfunc_end10-_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.cfi_endproc

	.section	.text._ZN26btTriangleIndexVertexArray18preallocateIndicesEi,"axG",@progbits,_ZN26btTriangleIndexVertexArray18preallocateIndicesEi,comdat
	.weak	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.p2align	4, 0x90
	.type	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi,@function
_ZN26btTriangleIndexVertexArray18preallocateIndicesEi: # @_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end11:
	.size	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi, .Lfunc_end11-_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.cfi_endproc

	.type	_ZTV34btTriangleIndexVertexMaterialArray,@object # @_ZTV34btTriangleIndexVertexMaterialArray
	.section	.rodata,"a",@progbits
	.globl	_ZTV34btTriangleIndexVertexMaterialArray
	.p2align	3
_ZTV34btTriangleIndexVertexMaterialArray:
	.quad	0
	.quad	_ZTI34btTriangleIndexVertexMaterialArray
	.quad	_ZN34btTriangleIndexVertexMaterialArrayD2Ev
	.quad	_ZN34btTriangleIndexVertexMaterialArrayD0Ev
	.quad	_ZNK23btStridingMeshInterface27InternalProcessAllTrianglesEP31btInternalTriangleIndexCallbackRK9btVector3S4_
	.quad	_ZN26btTriangleIndexVertexArray24getLockedVertexIndexBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.quad	_ZNK26btTriangleIndexVertexArray32getLockedReadOnlyVertexIndexBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.quad	_ZN26btTriangleIndexVertexArray16unLockVertexBaseEi
	.quad	_ZNK26btTriangleIndexVertexArray24unLockReadOnlyVertexBaseEi
	.quad	_ZNK26btTriangleIndexVertexArray14getNumSubPartsEv
	.quad	_ZN26btTriangleIndexVertexArray19preallocateVerticesEi
	.quad	_ZN26btTriangleIndexVertexArray18preallocateIndicesEi
	.quad	_ZNK26btTriangleIndexVertexArray14hasPremadeAabbEv
	.quad	_ZNK26btTriangleIndexVertexArray14setPremadeAabbERK9btVector3S2_
	.quad	_ZNK26btTriangleIndexVertexArray14getPremadeAabbEP9btVector3S1_
	.quad	_ZN34btTriangleIndexVertexMaterialArray21getLockedMaterialBaseEPPhRiR14PHY_ScalarTypeS2_S1_S2_S2_S4_i
	.quad	_ZN34btTriangleIndexVertexMaterialArray29getLockedReadOnlyMaterialBaseEPPKhRiR14PHY_ScalarTypeS3_S2_S3_S3_S5_i
	.size	_ZTV34btTriangleIndexVertexMaterialArray, 136

	.type	_ZTS34btTriangleIndexVertexMaterialArray,@object # @_ZTS34btTriangleIndexVertexMaterialArray
	.globl	_ZTS34btTriangleIndexVertexMaterialArray
	.p2align	4
_ZTS34btTriangleIndexVertexMaterialArray:
	.asciz	"34btTriangleIndexVertexMaterialArray"
	.size	_ZTS34btTriangleIndexVertexMaterialArray, 37

	.type	_ZTI34btTriangleIndexVertexMaterialArray,@object # @_ZTI34btTriangleIndexVertexMaterialArray
	.globl	_ZTI34btTriangleIndexVertexMaterialArray
	.p2align	4
_ZTI34btTriangleIndexVertexMaterialArray:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS34btTriangleIndexVertexMaterialArray
	.quad	_ZTI26btTriangleIndexVertexArray
	.size	_ZTI34btTriangleIndexVertexMaterialArray, 24


	.globl	_ZN34btTriangleIndexVertexMaterialArrayC1EiPiiiPfiiPhiS0_i
	.type	_ZN34btTriangleIndexVertexMaterialArrayC1EiPiiiPfiiPhiS0_i,@function
_ZN34btTriangleIndexVertexMaterialArrayC1EiPiiiPfiiPhiS0_i = _ZN34btTriangleIndexVertexMaterialArrayC2EiPiiiPfiiPhiS0_i
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
