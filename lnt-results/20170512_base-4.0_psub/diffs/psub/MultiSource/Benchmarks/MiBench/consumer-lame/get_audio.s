	.text
	.file	"get_audio.bc"
	.globl	lame_init_infile
	.p2align	4, 0x90
	.type	lame_init_infile,@function
lame_init_infile:                       # @lame_init_infile
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movb	$0, count_samples_carefully(%rip)
	movq	128(%rbx), %rsi
	movl	8(%rbx), %ecx
	movl	12(%rbx), %edx
	callq	OpenSndFile
	movl	samp_freq(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_2
# BB#1:
	movl	%eax, 12(%rbx)
.LBB0_2:
	movl	num_channels(%rip), %eax
	testl	%eax, %eax
	je	.LBB0_4
# BB#3:
	movl	%eax, 8(%rbx)
.LBB0_4:
	movq	num_samples(%rip), %rax
	movq	%rax, (%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	lame_init_infile, .Lfunc_end0-lame_init_infile
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4620693217682128896     # double 8
.LCPI1_1:
	.quad	4652007308841189376     # double 1000
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI1_2:
	.long	1593835520              # float 9.22337203E+18
	.text
	.globl	OpenSndFile
	.p2align	4, 0x90
	.type	OpenSndFile,@function
OpenSndFile:                            # @OpenSndFile
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi4:
	.cfi_def_cfa_offset 176
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, num_samples(%rip)
	movl	%edx, samp_freq(%rip)
	movl	%ecx, num_channels(%rip)
	cmpb	$45, (%rbx)
	jne	.LBB1_8
# BB#1:
	cmpb	$0, 1(%rbx)
	je	.LBB1_2
.LBB1_8:                                # %.thread
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, musicin(%rip)
	testq	%rax, %rax
	je	.LBB1_9
.LBB1_3:
	movl	$0, input_bitrate(%rip)
	movl	120(%r14), %ecx
	cmpl	$3, %ecx
	je	.LBB1_11
# BB#4:
	cmpl	$4, %ecx
	je	.LBB1_6
# BB#5:
	movq	%r14, %rdi
	movq	%rax, %rsi
	callq	parse_file_header
	cmpl	$4, 120(%r14)
	jne	.LBB1_14
.LBB1_6:                                # %.thread43
	movq	stderr(%rip), %rcx
	movl	$.L.str.5, %edi
	movl	$27, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rax
	cmpl	$1, 124(%r14)
	jne	.LBB1_13
# BB#7:
	movl	$.L.str.6, %edi
	movl	$25, %esi
	movl	$1, %edx
	movq	%rax, %rcx
	callq	fwrite
	jmp	.LBB1_14
.LBB1_11:
	movl	$num_channels, %esi
	movl	$samp_freq, %edx
	movl	$input_bitrate, %ecx
	movl	$num_samples, %r8d
	movq	%rax, %rdi
	callq	lame_decode_initfile
	cmpl	$-1, %eax
	je	.LBB1_12
.LBB1_14:
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	cmpq	%rax, num_samples(%rip)
	jne	.LBB1_21
# BB#15:
	movq	musicin(%rip), %rax
	cmpq	stdin(%rip), %rax
	je	.LBB1_21
# BB#16:
	leaq	8(%rsp), %rdx
	movl	$1, %edi
	movq	%rbx, %rsi
	callq	__xstat
	testl	%eax, %eax
	jne	.LBB1_21
# BB#17:
	movq	56(%rsp), %rax
	cmpl	$3, 120(%r14)
	jne	.LBB1_19
# BB#18:
	cvtsi2sdq	%rax, %xmm0
	mulsd	.LCPI1_0(%rip), %xmm0
	cvtsi2sdl	input_bitrate(%rip), %xmm1
	mulsd	.LCPI1_1(%rip), %xmm1
	divsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2ssl	samp_freq(%rip), %xmm1
	mulss	%xmm0, %xmm1
	movss	.LCPI1_2(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	subss	%xmm0, %xmm2
	cvttss2si	%xmm2, %rax
	movabsq	$-9223372036854775808, %rcx # imm = 0x8000000000000000
	xorq	%rax, %rcx
	cvttss2si	%xmm1, %rax
	ucomiss	%xmm0, %xmm1
	cmovaeq	%rcx, %rax
	jmp	.LBB1_20
.LBB1_2:
	movq	stdin(%rip), %rax
	movq	%rax, musicin(%rip)
	jmp	.LBB1_3
.LBB1_19:
	movslq	num_channels(%rip), %rcx
	addq	%rcx, %rcx
	cqto
	idivq	%rcx
.LBB1_20:
	movq	%rax, num_samples(%rip)
.LBB1_21:
	movq	musicin(%rip), %rax
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_13:
	movl	$10, %edi
	movq	%rax, %rsi
	callq	fputc
	jmp	.LBB1_14
.LBB1_9:
	movq	stderr(%rip), %rdi
	movl	$.L.str.3, %esi
	jmp	.LBB1_10
.LBB1_12:
	movq	stderr(%rip), %rdi
	movl	$.L.str.4, %esi
.LBB1_10:
	xorl	%eax, %eax
	movq	%rbx, %rdx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	OpenSndFile, .Lfunc_end1-OpenSndFile
	.cfi_endproc

	.globl	GetSndSampleRate
	.p2align	4, 0x90
	.type	GetSndSampleRate,@function
GetSndSampleRate:                       # @GetSndSampleRate
	.cfi_startproc
# BB#0:
	movl	samp_freq(%rip), %eax
	retq
.Lfunc_end2:
	.size	GetSndSampleRate, .Lfunc_end2-GetSndSampleRate
	.cfi_endproc

	.globl	GetSndChannels
	.p2align	4, 0x90
	.type	GetSndChannels,@function
GetSndChannels:                         # @GetSndChannels
	.cfi_startproc
# BB#0:
	movl	num_channels(%rip), %eax
	retq
.Lfunc_end3:
	.size	GetSndChannels, .Lfunc_end3-GetSndChannels
	.cfi_endproc

	.globl	GetSndSamples
	.p2align	4, 0x90
	.type	GetSndSamples,@function
GetSndSamples:                          # @GetSndSamples
	.cfi_startproc
# BB#0:
	movq	num_samples(%rip), %rax
	retq
.Lfunc_end4:
	.size	GetSndSamples, .Lfunc_end4-GetSndSamples
	.cfi_endproc

	.globl	lame_close_infile
	.p2align	4, 0x90
	.type	lame_close_infile,@function
lame_close_infile:                      # @lame_close_infile
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 16
	movq	musicin(%rip), %rdi
	callq	fclose
	testl	%eax, %eax
	jne	.LBB5_2
# BB#1:                                 # %CloseSndFile.exit
	popq	%rax
	retq
.LBB5_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %edi
	callq	exit
.Lfunc_end5:
	.size	lame_close_infile, .Lfunc_end5-lame_close_infile
	.cfi_endproc

	.globl	CloseSndFile
	.p2align	4, 0x90
	.type	CloseSndFile,@function
CloseSndFile:                           # @CloseSndFile
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 16
	movq	musicin(%rip), %rdi
	callq	fclose
	testl	%eax, %eax
	jne	.LBB6_2
# BB#1:
	popq	%rax
	retq
.LBB6_2:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$33, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %edi
	callq	exit
.Lfunc_end6:
	.size	CloseSndFile, .Lfunc_end6-CloseSndFile
	.cfi_endproc

	.globl	lame_readframe
	.p2align	4, 0x90
	.type	lame_readframe,@function
lame_readframe:                         # @lame_readframe
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	callq	get_audio
	testl	%eax, %eax
	je	.LBB7_2
# BB#1:                                 # %._crit_edge
	movq	168(%rbx), %rcx
	movq	176(%rbx), %rdx
	addq	$176, %rbx
	cmpq	%rdx, %rcx
	jge	.LBB7_4
	jmp	.LBB7_5
.LBB7_2:
	movq	168(%rbx), %rcx
	movq	176(%rbx), %rsi
	leaq	2(%rcx), %rdx
	cmpq	%rdx, %rsi
	cmovleq	%rsi, %rdx
	movq	%rdx, 176(%rbx)
	leaq	176(%rbx), %rbx
	cmpq	%rdx, %rcx
	jl	.LBB7_5
.LBB7_4:
	movq	%rcx, (%rbx)
.LBB7_5:
	popq	%rbx
	retq
.Lfunc_end7:
	.size	lame_readframe, .Lfunc_end7-lame_readframe
	.cfi_endproc

	.globl	get_audio
	.p2align	4, 0x90
	.type	get_audio,@function
get_audio:                              # @get_audio
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
	subq	$4608, %rsp             # imm = 0x1200
.Lcfi16:
	.cfi_def_cfa_offset 4656
.Lcfi17:
	.cfi_offset %rbx, -48
.Lcfi18:
	.cfi_offset %r12, -40
.Lcfi19:
	.cfi_offset %r13, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movslq	8(%rdi), %r15
	cmpq	$0, 168(%rdi)
	jne	.LBB8_2
# BB#1:
	movq	$0, get_audio.num_samples_read(%rip)
.LBB8_2:
	movslq	200(%rdi), %r13
	movq	%r13, %rax
	shlq	$6, %rax
	leaq	(%rax,%rax,8), %r12
	cmpb	$1, count_samples_carefully(%rip)
	movl	%r12d, %ecx
	jne	.LBB8_4
# BB#3:
	movq	num_samples(%rip), %rcx
	movq	get_audio.num_samples_read(%rip), %rax
	cmpq	%rax, %rcx
	cmovbq	%rcx, %rax
	subq	%rax, %rcx
	cmpq	%r12, %rcx
	cmovael	%r12d, %ecx
.LBB8_4:
	cmpl	$3, 120(%rdi)
	jne	.LBB8_9
# BB#5:
	movq	musicin(%rip), %rdi
	leaq	2304(%rbx), %rdx
	movq	%rbx, %rsi
	callq	lame_decode_fromfile
	movl	%eax, %ecx
	cmpl	$-1, %ecx
	jne	.LBB8_8
# BB#6:                                 # %vector.body77.preheader
	movq	$-1152, %rax            # imm = 0xFB80
	pxor	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_7:                                # %vector.body77
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, 2304(%rbx,%rax,2)
	movdqu	%xmm0, 4608(%rbx,%rax,2)
	movdqu	%xmm0, 2320(%rbx,%rax,2)
	movdqu	%xmm0, 4624(%rbx,%rax,2)
	movdqu	%xmm0, 2336(%rbx,%rax,2)
	movdqu	%xmm0, 4640(%rbx,%rax,2)
	movdqu	%xmm0, 2352(%rbx,%rax,2)
	movdqu	%xmm0, 4656(%rbx,%rax,2)
	movdqu	%xmm0, 2368(%rbx,%rax,2)
	movdqu	%xmm0, 4672(%rbx,%rax,2)
	movdqu	%xmm0, 2384(%rbx,%rax,2)
	movdqu	%xmm0, 4688(%rbx,%rax,2)
	movdqu	%xmm0, 2400(%rbx,%rax,2)
	movdqu	%xmm0, 4704(%rbx,%rax,2)
	movdqu	%xmm0, 2416(%rbx,%rax,2)
	movdqu	%xmm0, 4720(%rbx,%rax,2)
	addq	$64, %rax
	jne	.LBB8_7
.LBB8_8:                                # %read_samples_mp3.exit
	xorl	%eax, %eax
	cmpl	$-1, %ecx
	cmovnel	%ecx, %eax
	jmp	.LBB8_31
.LBB8_9:
	movl	%r12d, %edx
	imull	%r15d, %edx
	imull	%r15d, %ecx
	movq	%rsp, %r14
	movq	%r14, %rsi
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	read_samples_pcm
	cltd
	idivl	%r15d
	testl	%r13d, %r13d
	jle	.LBB8_31
# BB#10:                                # %.lr.ph
	movabsq	$9223372036854775744, %r8 # imm = 0x7FFFFFFFFFFFFFC0
	movslq	%r12d, %rdx
	cmpl	$2, %r15d
	jne	.LBB8_13
# BB#11:                                # %.lr.ph.split.us.preheader
	testq	%rdx, %rdx
	movl	$1, %r9d
	cmovgq	%rdx, %r9
	cmpq	$8, %r9
	jb	.LBB8_28
# BB#16:                                # %min.iters.checked61
	andq	%r9, %r8
	je	.LBB8_28
# BB#17:                                # %vector.scevcheck67
	testq	%rdx, %rdx
	movl	$1, %esi
	cmovgq	%rdx, %rsi
	decq	%rsi
	movl	%esi, %edi
	addl	%edi, %edi
	setb	%r10b
	xorl	%ecx, %ecx
	testl	%edi, %edi
	js	.LBB8_29
# BB#18:                                # %vector.scevcheck67
	shrq	$32, %rsi
	jne	.LBB8_29
# BB#19:                                # %vector.scevcheck67
	testb	%r10b, %r10b
	jne	.LBB8_29
# BB#20:                                # %vector.body56.preheader.new
	leaq	32(%rsp), %rdi
	leaq	2320(%rbx), %rcx
	movq	%r8, %rsi
	.p2align	4, 0x90
.LBB8_21:                               # %vector.body56
                                        # =>This Inner Loop Header: Depth=1
	movdqa	-32(%rdi), %xmm0
	movdqa	-16(%rdi), %xmm1
	pshuflw	$232, %xmm1, %xmm2      # xmm2 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm0, %xmm3      # xmm3 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpcklqdq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	pshuflw	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movdqu	%xmm3, -2320(%rcx)
	movdqu	%xmm0, -16(%rcx)
	movdqa	(%rdi), %xmm0
	movdqa	16(%rdi), %xmm1
	pshuflw	$232, %xmm1, %xmm2      # xmm2 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshuflw	$232, %xmm0, %xmm3      # xmm3 = xmm0[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm3, %xmm3      # xmm3 = xmm3[0,2,2,3]
	punpcklqdq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0]
	pshuflw	$231, %xmm1, %xmm1      # xmm1 = xmm1[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm1, %xmm1      # xmm1 = xmm1[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	pshuflw	$177, %xmm1, %xmm1      # xmm1 = xmm1[1,0,3,2,4,5,6,7]
	pshuflw	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3,4,5,6,7]
	pshufhw	$231, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,7,5,6,7]
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshuflw	$177, %xmm0, %xmm0      # xmm0 = xmm0[1,0,3,2,4,5,6,7]
	punpcklqdq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0]
	movdqu	%xmm3, -2304(%rcx)
	movdqu	%xmm0, (%rcx)
	addq	$64, %rdi
	addq	$32, %rcx
	addq	$-16, %rsi
	jne	.LBB8_21
# BB#22:                                # %middle.block57
	cmpq	%r8, %r9
	movq	%r8, %rcx
	jne	.LBB8_29
	jmp	.LBB8_31
.LBB8_13:                               # %.lr.ph.split.preheader
	testq	%rdx, %rdx
	movl	$1, %r9d
	cmovgq	%rdx, %r9
	xorl	%esi, %esi
	cmpq	$7, %r9
	ja	.LBB8_23
.LBB8_14:                               # %.lr.ph.split.preheader90
	movq	%rsi, %rcx
	imulq	%r15, %rcx
	leaq	(%rsp,%rcx,2), %rcx
	addq	%r15, %r15
	.p2align	4, 0x90
.LBB8_15:                               # %.lr.ph.split
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rcx), %edi
	movw	%di, (%rbx,%rsi,2)
	movw	$0, 2304(%rbx,%rsi,2)
	incq	%rsi
	addq	%r15, %rcx
	cmpq	%rdx, %rsi
	jl	.LBB8_15
	jmp	.LBB8_31
.LBB8_28:
	xorl	%ecx, %ecx
.LBB8_29:                               # %.lr.ph.split.us.preheader89
	leal	1(%rcx,%rcx), %esi
	.p2align	4, 0x90
.LBB8_30:                               # %.lr.ph.split.us
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rsp,%rcx,4), %edi
	movw	%di, (%rbx,%rcx,2)
	movslq	%esi, %rsi
	movzwl	(%rsp,%rsi,2), %edi
	movw	%di, 2304(%rbx,%rcx,2)
	incq	%rcx
	addl	$2, %esi
	cmpq	%rdx, %rcx
	jl	.LBB8_30
.LBB8_31:                               # %.loopexit
	movl	$4294967295, %ecx       # imm = 0xFFFFFFFF
	cmpq	%rcx, num_samples(%rip)
	je	.LBB8_33
# BB#32:
	movslq	%eax, %rcx
	addq	%rcx, get_audio.num_samples_read(%rip)
.LBB8_33:
	addq	$4608, %rsp             # imm = 0x1200
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB8_23:                               # %min.iters.checked
	andq	%r9, %r8
	je	.LBB8_14
# BB#24:                                # %min.iters.checked
	cmpl	$1, %r15d
	jne	.LBB8_14
# BB#25:                                # %vector.body.preheader.new
	movq	%r15, %r11
	shlq	$4, %r11
	leaq	(%r11,%r11,2), %r10
	movq	%r15, %r12
	shlq	$6, %r12
	leaq	2352(%rbx), %rdi
	movq	%r15, %rsi
	shlq	$5, %rsi
	pxor	%xmm0, %xmm0
	movq	%r8, %rcx
	.p2align	4, 0x90
.LBB8_26:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movaps	(%r14), %xmm1
	movups	%xmm1, -2352(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movaps	(%r11,%r14), %xmm1
	movups	%xmm1, -2336(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movaps	(%rsi,%r14), %xmm1
	movups	%xmm1, -2320(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqa	(%r10,%r14), %xmm1
	movdqu	%xmm1, -2304(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	%r12, %r14
	addq	$64, %rdi
	addq	$-32, %rcx
	jne	.LBB8_26
# BB#27:                                # %middle.block
	cmpq	%r8, %r9
	movq	%r8, %rsi
	jne	.LBB8_14
	jmp	.LBB8_31
.Lfunc_end8:
	.size	get_audio, .Lfunc_end8-get_audio
	.cfi_endproc

	.globl	read_samples_mp3
	.p2align	4, 0x90
	.type	read_samples_mp3,@function
read_samples_mp3:                       # @read_samples_mp3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 16
.Lcfi23:
	.cfi_offset %rbx, -16
	movq	%rdx, %rbx
	leaq	2304(%rbx), %rdx
	movq	%rsi, %rdi
	movq	%rbx, %rsi
	callq	lame_decode_fromfile
	cmpl	$-1, %eax
	jne	.LBB9_3
# BB#1:                                 # %vector.body.preheader
	movq	$-1152, %rcx            # imm = 0xFB80
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB9_2:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm0, 2304(%rbx,%rcx,2)
	movups	%xmm0, 4608(%rbx,%rcx,2)
	movups	%xmm0, 2320(%rbx,%rcx,2)
	movups	%xmm0, 4624(%rbx,%rcx,2)
	movups	%xmm0, 2336(%rbx,%rcx,2)
	movups	%xmm0, 4640(%rbx,%rcx,2)
	movups	%xmm0, 2352(%rbx,%rcx,2)
	movups	%xmm0, 4656(%rbx,%rcx,2)
	movups	%xmm0, 2368(%rbx,%rcx,2)
	movups	%xmm0, 4672(%rbx,%rcx,2)
	movups	%xmm0, 2384(%rbx,%rcx,2)
	movups	%xmm0, 4688(%rbx,%rcx,2)
	movups	%xmm0, 2400(%rbx,%rcx,2)
	movups	%xmm0, 4704(%rbx,%rcx,2)
	movups	%xmm0, 2416(%rbx,%rcx,2)
	movups	%xmm0, 4720(%rbx,%rcx,2)
	addq	$64, %rcx
	jne	.LBB9_2
.LBB9_3:                                # %.loopexit
	xorl	%ecx, %ecx
	cmpl	$-1, %eax
	cmovnel	%eax, %ecx
	movl	%ecx, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	read_samples_mp3, .Lfunc_end9-read_samples_mp3
	.cfi_endproc

	.globl	read_samples_pcm
	.p2align	4, 0x90
	.type	read_samples_pcm,@function
read_samples_pcm:                       # @read_samples_pcm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi24:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi25:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi26:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 48
.Lcfi29:
	.cfi_offset %rbx, -48
.Lcfi30:
	.cfi_offset %r12, -40
.Lcfi31:
	.cfi_offset %r14, -32
.Lcfi32:
	.cfi_offset %r15, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %r15
	movq	%rdi, %r12
	movl	120(%r12), %ebp
	movslq	%ecx, %rdx
	movq	musicin(%rip), %rcx
	movl	$2, %esi
	movq	%r15, %rdi
	callq	fread
	movq	%rax, %rbx
	movq	musicin(%rip), %rdi
	callq	ferror
	testl	%eax, %eax
	jne	.LBB10_15
# BB#1:
	movl	NativeByteOrder(%rip), %eax
	testl	%eax, %eax
	jne	.LBB10_3
# BB#2:
	callq	DetermineByteOrder
	movl	%eax, NativeByteOrder(%rip)
	testl	%eax, %eax
	je	.LBB10_16
.LBB10_3:
	cmpl	$1, %ebp
	setne	%cl
	cmpl	$2, %eax
	jne	.LBB10_6
# BB#4:
	testb	%cl, %cl
	je	.LBB10_6
# BB#5:
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	SwapBytesInWords
	movl	NativeByteOrder(%rip), %eax
.LBB10_6:
	cmpl	$1, %ebp
	jne	.LBB10_9
# BB#7:
	cmpl	$1, %eax
	jne	.LBB10_9
# BB#8:
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	SwapBytesInWords
.LBB10_9:
	cmpl	$1, 124(%r12)
	jne	.LBB10_11
# BB#10:
	movq	%r15, %rdi
	movl	%ebx, %esi
	callq	SwapBytesInWords
.LBB10_11:
	cmpl	%r14d, %ebx
	jge	.LBB10_14
# BB#12:
	xorl	%eax, %eax
	testl	%ebx, %ebx
	cmovnsl	%ebx, %eax
	cmpl	%r14d, %eax
	jge	.LBB10_14
# BB#13:                                # %.lr.ph.preheader
	movl	%eax, %ecx
	leaq	(%r15,%rcx,2), %rdi
	decl	%r14d
	subl	%eax, %r14d
	leaq	2(%r14,%r14), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB10_14:                              # %.loopexit
	movl	%ebx, %eax
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_15:
	movq	stderr(%rip), %rcx
	movl	$.L.str.8, %edi
	movl	$25, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$2, %edi
	callq	exit
.LBB10_16:
	movq	stderr(%rip), %rcx
	movl	$.L.str.9, %edi
	movl	$26, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.Lfunc_end10:
	.size	read_samples_pcm, .Lfunc_end10-read_samples_pcm
	.cfi_endproc

	.globl	GetSndBitrate
	.p2align	4, 0x90
	.type	GetSndBitrate,@function
GetSndBitrate:                          # @GetSndBitrate
	.cfi_startproc
# BB#0:
	movl	input_bitrate(%rip), %eax
	retq
.Lfunc_end11:
	.size	GetSndBitrate, .Lfunc_end11-GetSndBitrate
	.cfi_endproc

	.globl	fskip
	.p2align	4, 0x90
	.type	fskip,@function
fskip:                                  # @fskip
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 32
	subq	$1024, %rsp             # imm = 0x400
.Lcfi37:
	.cfi_def_cfa_offset 1056
.Lcfi38:
	.cfi_offset %rbx, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %r15, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jle	.LBB12_3
# BB#1:                                 # %.lr.ph.preheader
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB12_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$1025, %rbx             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovlq	%rbx, %rax
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%r14, %rcx
	callq	fread
	subq	%rax, %rbx
	testq	%rbx, %rbx
	jg	.LBB12_2
.LBB12_3:                               # %._crit_edge
	movl	%ebx, %eax
	addq	$1024, %rsp             # imm = 0x400
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	fskip, .Lfunc_end12-fskip
	.cfi_endproc

	.globl	parse_file_header
	.p2align	4, 0x90
	.type	parse_file_header,@function
parse_file_header:                      # @parse_file_header
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi41:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi42:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi43:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi44:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi45:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi46:
	.cfi_def_cfa_offset 56
	subq	$1064, %rsp             # imm = 0x428
.Lcfi47:
	.cfi_def_cfa_offset 1120
.Lcfi48:
	.cfi_offset %rbx, -56
.Lcfi49:
	.cfi_offset %r12, -48
.Lcfi50:
	.cfi_offset %r13, -40
.Lcfi51:
	.cfi_offset %r14, -32
.Lcfi52:
	.cfi_offset %r15, -24
.Lcfi53:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movb	$0, count_samples_carefully(%rip)
	movl	$4, 120(%rbp)
	cmpl	$1179603533, %eax       # imm = 0x464F524D
	je	.LBB13_16
# BB#1:
	cmpl	$1380533830, %eax       # imm = 0x52494646
	jne	.LBB13_44
# BB#2:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	cmpl	$1463899717, %eax       # imm = 0x57415645
	jne	.LBB13_43
# BB#3:                                 # %.preheader.i
	xorl	%r14d, %r14d
	leaq	32(%rsp), %r15
	movl	$0, (%rsp)              # 4-byte Folded Spill
	xorl	%r13d, %r13d
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB13_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
                                        #     Child Loop BB13_8 Depth 2
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movl	%eax, %ebp
	movq	%rbx, %rdi
	cmpl	$1718449184, %ebp       # imm = 0x666D7420
	jne	.LBB13_9
# BB#5:                                 #   in Loop: Header=BB13_4 Depth=1
	callq	Read32Bits
	movl	%eax, %ebp
	cmpl	$16, %ebp
	jl	.LBB13_43
# BB#6:                                 #   in Loop: Header=BB13_4 Depth=1
	movq	%rbx, %rdi
	callq	Read16BitsLowHigh
	movq	%rbx, %rdi
	callq	Read16BitsLowHigh
	movl	%eax, (%rsp)            # 4-byte Spill
	movq	%rbx, %rdi
	callq	Read32Bits
	movslq	%eax, %r12
	movq	%rbx, %rdi
	callq	Read32Bits
	movq	%rbx, %rdi
	callq	Read16BitsLowHigh
	movq	%rbx, %rdi
	callq	Read16BitsLowHigh
	movl	%eax, %r13d
	cmpl	$16, %ebp
	je	.LBB13_13
# BB#7:                                 # %.lr.ph.i.preheader.i
                                        #   in Loop: Header=BB13_4 Depth=1
	movslq	%ebp, %rbp
	addq	$-16, %rbp
	.p2align	4, 0x90
.LBB13_8:                               # %.lr.ph.i.i
                                        #   Parent Loop BB13_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1025, %rbp             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovlq	%rbp, %rax
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	fread
	subq	%rax, %rbp
	testq	%rbp, %rbp
	jg	.LBB13_8
	jmp	.LBB13_12
	.p2align	4, 0x90
.LBB13_9:                               #   in Loop: Header=BB13_4 Depth=1
	callq	Read32Bits
	cmpl	$1684108385, %ebp       # imm = 0x64617461
	movslq	%eax, %rbp
	je	.LBB13_14
# BB#10:                                #   in Loop: Header=BB13_4 Depth=1
	testl	%eax, %eax
	jle	.LBB13_12
	.p2align	4, 0x90
.LBB13_11:                              # %.lr.ph.i53.i
                                        #   Parent Loop BB13_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1025, %rbp             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovlq	%rbp, %rax
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	fread
	subq	%rax, %rbp
	testq	%rbp, %rbp
	jg	.LBB13_11
.LBB13_12:                              # %fskip.exit55.i
                                        #   in Loop: Header=BB13_4 Depth=1
	testl	%ebp, %ebp
	jne	.LBB13_43
.LBB13_13:                              #   in Loop: Header=BB13_4 Depth=1
	incl	%r14d
	cmpl	$20, %r14d
	jl	.LBB13_4
	jmp	.LBB13_43
.LBB13_16:
	movq	%rbp, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movl	%eax, %ebp
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	cmpl	$1095321158, %eax       # imm = 0x41494646
	jne	.LBB13_43
# BB#17:                                # %.thread73.preheader.i
	movslq	%ebp, %r15
	xorpd	%xmm0, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	32(%rsp), %r13
	xorl	%r14d, %r14d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB13_18:                              # %.thread73.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_32 Depth 2
                                        #     Child Loop BB13_21 Depth 2
	testq	%r15, %r15
	jle	.LBB13_43
# BB#19:                                #   in Loop: Header=BB13_18 Depth=1
	addq	$-4, %r15
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movl	%eax, %ebp
	movq	%rbx, %rdi
	cmpl	$1129270605, %ebp       # imm = 0x434F4D4D
	je	.LBB13_20
# BB#23:                                #   in Loop: Header=BB13_18 Depth=1
	callq	Read32BitsHighLow
	cmpl	$1397968452, %ebp       # imm = 0x53534E44
	je	.LBB13_24
# BB#30:                                #   in Loop: Header=BB13_18 Depth=1
	movslq	%eax, %r12
	testl	%r12d, %r12d
	movq	%r12, %rbp
	jle	.LBB13_33
# BB#31:                                # %.lr.ph.i53.i17.preheader
                                        #   in Loop: Header=BB13_18 Depth=1
	movq	%r12, %rbp
	.p2align	4, 0x90
.LBB13_32:                              # %.lr.ph.i53.i17
                                        #   Parent Loop BB13_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1025, %rbp             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovlq	%rbp, %rax
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	fread
	subq	%rax, %rbp
	testq	%rbp, %rbp
	jg	.LBB13_32
.LBB13_33:                              # %fskip.exit55.i19
                                        #   in Loop: Header=BB13_18 Depth=1
	subq	%r12, %r15
	testl	%ebp, %ebp
	je	.LBB13_18
	jmp	.LBB13_43
.LBB13_20:                              #   in Loop: Header=BB13_18 Depth=1
	callq	Read32BitsHighLow
	movslq	%eax, %rbp
	movq	%rbx, %rdi
	callq	Read16BitsHighLow
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movl	%eax, %r14d
	movq	%rbx, %rdi
	callq	Read16BitsHighLow
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movq	%rax, (%rsp)            # 8-byte Spill
	movq	%rbx, %rdi
	callq	ReadIeeeExtendedHighLow
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	leaq	-18(%rbp), %r12
	cmpl	$19, %ebp
	jl	.LBB13_22
	.p2align	4, 0x90
.LBB13_21:                              # %.lr.ph.i.i13
                                        #   Parent Loop BB13_18 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1025, %r12             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovlq	%r12, %rax
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	fread
	subq	%rax, %r12
	testq	%r12, %r12
	jg	.LBB13_21
.LBB13_22:                              # %fskip.exit.i14
                                        #   in Loop: Header=BB13_18 Depth=1
	subq	%rbp, %r15
	movslq	%r14d, %r14
	movsd	8(%rsp), %xmm0          # 8-byte Reload
                                        # xmm0 = mem[0],zero
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	testl	%r12d, %r12d
	je	.LBB13_18
	jmp	.LBB13_43
.LBB13_24:
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movl	%eax, %r15d
	movslq	%r15d, %rbp
	movq	%rbx, %rdi
	callq	Read32BitsHighLow
	movl	%eax, %r12d
	testl	%ebp, %ebp
	jle	.LBB13_27
# BB#25:                                # %.lr.ph.i48.i.preheader
	leaq	32(%rsp), %r13
	.p2align	4, 0x90
.LBB13_26:                              # %.lr.ph.i48.i
                                        # =>This Inner Loop Header: Depth=1
	cmpq	$1025, %rbp             # imm = 0x401
	movl	$1024, %eax             # imm = 0x400
	cmovlq	%rbp, %rax
	movslq	%eax, %rdx
	movl	$1, %esi
	movq	%r13, %rdi
	movq	%rbx, %rcx
	callq	fread
	subq	%rax, %rbp
	testq	%rbp, %rbp
	jg	.LBB13_26
.LBB13_27:                              # %fskip.exit50.i
	testl	%ebp, %ebp
	jne	.LBB13_43
# BB#28:
	movzwl	(%rsp), %eax            # 2-byte Folded Reload
	cmpl	$16, %eax
	jne	.LBB13_29
# BB#34:
	movq	24(%rsp), %rcx          # 8-byte Reload
	leal	-1(%rcx), %eax
	movzwl	%ax, %eax
	cmpl	$2, %eax
	jae	.LBB13_47
# BB#35:
	testl	%r12d, %r12d
	jne	.LBB13_36
# BB#39:
	testl	%r15d, %r15d
	jne	.LBB13_40
# BB#41:
	movswl	%cx, %eax
	movl	%eax, num_channels(%rip)
	cvttss2si	8(%rsp), %eax   # 4-byte Folded Reload
	movl	%eax, samp_freq(%rip)
	movq	%r14, num_samples(%rip)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$2, 120(%rax)
	jmp	.LBB13_42
.LBB13_14:
	movzwl	%r13w, %edx
	cmpl	$16, %edx
	jne	.LBB13_46
# BB#15:
	movzwl	(%rsp), %ecx            # 2-byte Folded Reload
	movl	%ecx, num_channels(%rip)
	movl	%r12d, samp_freq(%rip)
	addq	%rcx, %rcx
	movq	%rbp, %rax
	cqto
	idivq	%rcx
	movq	%rax, num_samples(%rip)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$1, 120(%rax)
.LBB13_42:                              # %parse_wave_header.exit.thread
	movb	$1, count_samples_carefully(%rip)
.LBB13_43:                              # %parse_wave_header.exit.thread
	movq	16(%rsp), %rbp          # 8-byte Reload
	cmpl	$4, 120(%rbp)
	jne	.LBB13_45
.LBB13_44:                              # %.thread
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%rbx, %rdi
	callq	fseek
	movl	$4, 120(%rbp)
.LBB13_45:
	addq	$1064, %rsp             # imm = 0x428
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_46:
	movq	stderr(%rip), %rdi
	movl	$.L.str.11, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB13_29:
	movq	stderr(%rip), %rdi
	movl	$.L.str.13, %esi
	movl	$16, %edx
	jmp	.LBB13_38
.LBB13_47:
	movq	stderr(%rip), %rdi
	movl	$.L.str.14, %esi
	movl	$.L.str.10, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB13_36:
	movq	stderr(%rip), %rdi
	movl	$.L.str.15, %esi
	jmp	.LBB13_37
.LBB13_40:
	movq	stderr(%rip), %rdi
	movl	$.L.str.16, %esi
.LBB13_37:
	xorl	%edx, %edx
.LBB13_38:
	movl	$.L.str.10, %ecx
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end13:
	.size	parse_file_header, .Lfunc_end13-parse_file_header
	.cfi_endproc

	.type	count_samples_carefully,@object # @count_samples_carefully
	.local	count_samples_carefully
	.comm	count_samples_carefully,1,4
	.type	get_audio.num_samples_read,@object # @get_audio.num_samples_read
	.local	get_audio.num_samples_read
	.comm	get_audio.num_samples_read,8,8
	.type	num_samples,@object     # @num_samples
	.local	num_samples
	.comm	num_samples,8,8
	.type	musicin,@object         # @musicin
	.local	musicin
	.comm	musicin,8,8
	.type	input_bitrate,@object   # @input_bitrate
	.local	input_bitrate
	.comm	input_bitrate,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Could not close audio input file\n"
	.size	.L.str, 34

	.type	samp_freq,@object       # @samp_freq
	.local	samp_freq
	.comm	samp_freq,4,4
	.type	num_channels,@object    # @num_channels
	.local	num_channels
	.comm	num_channels,4,4
	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"-"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"rb"
	.size	.L.str.2, 3

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Could not find \"%s\".\n"
	.size	.L.str.3, 22

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Error reading headers in mp3 input file %s.\n"
	.size	.L.str.4, 45

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Assuming raw pcm input file"
	.size	.L.str.5, 28

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	" : Forcing byte-swapping\n"
	.size	.L.str.6, 26

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"Error reading input file\n"
	.size	.L.str.8, 26

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"byte order not determined\n"
	.size	.L.str.9, 27

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"name"
	.size	.L.str.10, 5

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"%d-bit sample-size is not supported!\n"
	.size	.L.str.11, 38

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"Sound data is not %d bits in \"%s\".\n"
	.size	.L.str.13, 36

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"Sound data is not mono or stereo in \"%s\".\n"
	.size	.L.str.14, 43

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Block size is not %d bytes in \"%s\".\n"
	.size	.L.str.15, 37

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"Block offset is not %d bytes in \"%s\".\n"
	.size	.L.str.16, 39


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
