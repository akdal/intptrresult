	.text
	.file	"interface.bc"
	.globl	InitMP3
	.p2align	4, 0x90
	.type	InitMP3,@function
InitMP3:                                # @InitMP3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	xorl	%esi, %esi
	movl	$31880, %edx            # imm = 0x7C88
	callq	memset
	movl	$-1, 24(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movl	$0, 16(%rbx)
	movl	$-1, 36(%rbx)
	movl	$0, 23160(%rbx)
	movl	$1, 31872(%rbx)
	movl	$32767, %edi            # imm = 0x7FFF
	callq	make_decode_tables
	movl	$32, %edi
	callq	init_layer3
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end0:
	.size	InitMP3, .Lfunc_end0-InitMP3
	.cfi_endproc

	.globl	ExitMP3
	.p2align	4, 0x90
	.type	ExitMP3,@function
ExitMP3:                                # @ExitMP3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %r14, -16
	movq	8(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB1_2
	.p2align	4, 0x90
.LBB1_1:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	callq	free
	movq	24(%rbx), %r14
	movq	%rbx, %rdi
	callq	free
	testq	%r14, %r14
	movq	%r14, %rbx
	jne	.LBB1_1
.LBB1_2:                                # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	ExitMP3, .Lfunc_end1-ExitMP3
	.cfi_endproc

	.globl	decodeMP3
	.p2align	4, 0x90
	.type	decodeMP3,@function
decodeMP3:                              # @decodeMP3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi9:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi10:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi11:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi13:
	.cfi_def_cfa_offset 80
.Lcfi14:
	.cfi_offset %rbx, -56
.Lcfi15:
	.cfi_offset %r12, -48
.Lcfi16:
	.cfi_offset %r13, -40
.Lcfi17:
	.cfi_offset %r14, -32
.Lcfi18:
	.cfi_offset %r15, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
	movq	%rcx, %r15
	movl	%edx, %ebp
	movq	%rsi, %r12
	movq	%rdi, %r13
	movq	%r13, gmp(%rip)
	cmpl	$4607, %r8d             # imm = 0x11FF
	jle	.LBB2_23
# BB#1:
	testq	%r12, %r12
	je	.LBB2_5
# BB#2:
	movl	$40, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB2_24
# BB#3:
	movslq	%ebp, %rbp
	movq	%rbp, %rdi
	callq	malloc
	movq	%rax, (%rbx)
	testq	%rax, %rax
	je	.LBB2_22
# BB#4:                                 # %addbuf.exit
	movq	%rbp, 8(%rbx)
	movq	%rax, %rdi
	movq	%r12, %rsi
	movq	%rbp, %rdx
	callq	memcpy
	movq	$0, 24(%rbx)
	movq	(%r13), %rax
	movq	%rax, 32(%rbx)
	movq	$0, 16(%rbx)
	leaq	8(%r13), %rcx
	addq	$24, %rax
	cmpq	$0, 8(%r13)
	cmoveq	%rcx, %rax
	movq	%rbx, (%rax)
	movq	%rbx, (%r13)
	addl	%ebp, 16(%r13)
.LBB2_5:
	movl	20(%r13), %ecx
	testl	%ecx, %ecx
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r15, 8(%rsp)           # 8-byte Spill
	je	.LBB2_7
# BB#6:                                 # %._crit_edge59
	leaq	28(%r13), %rbx
	movl	96(%r13), %edx
	leaq	16(%r13), %r15
	jmp	.LBB2_9
.LBB2_7:
	movl	$1, %eax
	cmpl	$4, 16(%r13)
	jl	.LBB2_27
# BB#8:
	leaq	16(%r13), %r15
	movq	%r13, %rdi
	callq	read_buf_byte
	movslq	%eax, %rbx
	shlq	$8, %rbx
	movq	%r13, %rdi
	callq	read_buf_byte
	movslq	%eax, %rbp
	orq	%rbx, %rbp
	shlq	$8, %rbp
	movq	%r13, %rdi
	callq	read_buf_byte
	movslq	%eax, %rbx
	orq	%rbp, %rbx
	shlq	$8, %rbx
	movq	%r13, %rdi
	callq	read_buf_byte
	movslq	%eax, %rsi
	orq	%rbx, %rsi
	movq	%rsi, 23152(%r13)
	leaq	28(%r13), %rbx
	movq	%rbx, %rdi
	callq	decode_header
	movl	96(%r13), %ecx
	movl	%ecx, 20(%r13)
	movl	%ecx, %edx
.LBB2_9:
	movl	$1, %eax
	cmpl	(%r15), %edx
	jg	.LBB2_27
# BB#10:
	movq	%rbx, (%rsp)            # 8-byte Spill
	movslq	23160(%r13), %rax
	leaq	(%rax,%rax,8), %rdx
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	notl	%eax
	shlq	$8, %rdx
	leaq	612(%r13,%rdx), %rdi
	movq	%rdi, wordpointer(%rip)
	andl	$1, %eax
	movl	%eax, 23160(%r13)
	movl	$0, bitindex(%rip)
	testl	%ecx, %ecx
	jle	.LBB2_19
# BB#11:                                # %.lr.ph
	leaq	8(%r13), %r14
	xorl	%r12d, %r12d
	jmp	.LBB2_13
	.p2align	4, 0x90
.LBB2_12:                               # %.backedge._crit_edge
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	wordpointer(%rip), %rdi
.LBB2_13:                               # =>This Inner Loop Header: Depth=1
	movq	(%r14), %rax
	movl	8(%rax), %edx
	movq	16(%rax), %rsi
	subl	%esi, %edx
	subl	%r12d, %ecx
	cmpl	%edx, %ecx
	cmovgl	%edx, %ecx
	movslq	%r12d, %r12
	addq	%r12, %rdi
	addq	(%rax), %rsi
	movslq	%ecx, %rbp
	movq	%rbp, %rdx
	callq	memcpy
	addl	%ebp, %r12d
	movq	(%r14), %rbx
	movq	%rbp, %rax
	addq	16(%rbx), %rax
	movq	%rax, 16(%rbx)
	subl	%ebp, (%r15)
	cmpq	8(%rbx), %rax
	jne	.LBB2_18
# BB#14:                                #   in Loop: Header=BB2_13 Depth=1
	movq	24(%rbx), %rax
	movq	%rax, (%r14)
	testq	%rax, %rax
	je	.LBB2_16
# BB#15:                                #   in Loop: Header=BB2_13 Depth=1
	addq	$32, %rax
	jmp	.LBB2_17
.LBB2_16:                               #   in Loop: Header=BB2_13 Depth=1
	movq	$0, (%r13)
	movq	%r14, %rax
.LBB2_17:                               # %remove_buf.exit
                                        #   in Loop: Header=BB2_13 Depth=1
	movq	$0, (%rax)
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
.LBB2_18:                               # %.backedge
                                        #   in Loop: Header=BB2_13 Depth=1
	movl	20(%r13), %ecx
	cmpl	%r12d, %ecx
	jg	.LBB2_12
.LBB2_19:                               # %._crit_edge
	movq	16(%rsp), %rbx          # 8-byte Reload
	movl	$0, (%rbx)
	cmpl	$0, 56(%r13)
	je	.LBB2_21
# BB#20:
	movl	$16, %edi
	callq	getbits
.LBB2_21:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	8(%rsp), %rsi           # 8-byte Reload
	movq	%rbx, %rdx
	callq	do_layer3
	movl	20(%r13), %eax
	movl	%eax, 24(%r13)
	movl	$0, 20(%r13)
	xorl	%eax, %eax
	jmp	.LBB2_27
.LBB2_22:
	movq	%rbx, %rdi
	callq	free
	jmp	.LBB2_26
.LBB2_23:
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$18, %esi
	jmp	.LBB2_25
.LBB2_24:
	movq	stderr(%rip), %rcx
	movl	$.L.str.2, %edi
	movl	$15, %esi
.LBB2_25:                               # %addbuf.exit.thread
	movl	$1, %edx
	callq	fwrite
.LBB2_26:                               # %addbuf.exit.thread
	movl	$-1, %eax
.LBB2_27:                               # %addbuf.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	decodeMP3, .Lfunc_end2-decodeMP3
	.cfi_endproc

	.globl	set_pointer
	.p2align	4, 0x90
	.type	set_pointer,@function
set_pointer:                            # @set_pointer
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 16
	movq	%rdi, %rcx
	movq	gmp(%rip), %rax
	movl	24(%rax), %edx
	testq	%rcx, %rcx
	jle	.LBB3_3
# BB#1:
	testl	%edx, %edx
	js	.LBB3_2
.LBB3_3:
	movslq	23160(%rax), %rsi
	movq	wordpointer(%rip), %rdi
	subq	%rcx, %rdi
	movq	%rdi, wordpointer(%rip)
	testq	%rcx, %rcx
	je	.LBB3_5
# BB#4:
	movslq	%edx, %r8
	movq	%rcx, %rdx
	negq	%rdx
	leaq	(%rsi,%rsi,8), %rsi
	shlq	$8, %rsi
	addq	%rsi, %rax
	addq	%r8, %rax
	leaq	612(%rdx,%rax), %rsi
	movq	%rcx, %rdx
	callq	memcpy
.LBB3_5:
	movl	$0, bitindex(%rip)
	xorl	%eax, %eax
	popq	%rcx
	retq
.LBB3_2:
	movq	stderr(%rip), %rdi
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%rcx, %rdx
	callq	fprintf
	movl	$-1, %eax
	popq	%rcx
	retq
.Lfunc_end3:
	.size	set_pointer, .Lfunc_end3-set_pointer
	.cfi_endproc

	.p2align	4, 0x90
	.type	read_buf_byte,@function
read_buf_byte:                          # @read_buf_byte
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 32
.Lcfi24:
	.cfi_offset %rbx, -32
.Lcfi25:
	.cfi_offset %r14, -24
.Lcfi26:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	leaq	8(%r14), %r15
	movq	8(%r14), %rbx
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	movq	16(%rbx), %rcx
	movslq	%ecx, %rax
	cmpq	8(%rbx), %rax
	jl	.LBB4_7
# BB#2:                                 #   in Loop: Header=BB4_1 Depth=1
	movq	24(%rbx), %rax
	movq	%rax, (%r15)
	testq	%rax, %rax
	je	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_1 Depth=1
	addq	$32, %rax
	jmp	.LBB4_5
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_1 Depth=1
	movq	$0, (%r14)
	movq	%r15, %rax
.LBB4_5:                                # %remove_buf.exit
                                        #   in Loop: Header=BB4_1 Depth=1
	movq	$0, (%rax)
	movq	(%rbx), %rdi
	callq	free
	movq	%rbx, %rdi
	callq	free
	movq	(%r15), %rbx
	testq	%rbx, %rbx
	jne	.LBB4_1
# BB#6:
	movq	stderr(%rip), %rcx
	movl	$.L.str.3, %edi
	movl	$13, %esi
	movl	$1, %edx
	callq	fwrite
	movl	$1, %edi
	callq	exit
.LBB4_7:
	movq	(%rbx), %rdx
	movzbl	(%rdx,%rax), %eax
	decl	16(%r14)
	incq	%rcx
	movq	%rcx, 16(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	read_buf_byte, .Lfunc_end4-read_buf_byte
	.cfi_endproc

	.type	gmp,@object             # @gmp
	.comm	gmp,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"To less out space\n"
	.size	.L.str, 19

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Can't step back %ld!\n"
	.size	.L.str.1, 22

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Out of memory!\n"
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Fatal error!\n"
	.size	.L.str.3, 14


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
