	.text
	.file	"tsc.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	set1d
	.p2align	4, 0x90
	.type	set1d,@function
set1d:                                  # @set1d
	.cfi_startproc
# BB#0:
	cmpl	$-1, %esi
	je	.LBB0_18
# BB#1:
	cmpl	$-2, %esi
	jne	.LBB0_4
# BB#2:                                 # %.preheader27.preheader
	movl	$2, %eax
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_3:                                # %.preheader27
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, -16(%rdi,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, -8(%rdi,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB0_3
	jmp	.LBB0_20
.LBB0_18:                               # %.preheader25.preheader
	xorl	%eax, %eax
	movsd	.LCPI0_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB0_19:                               # %.preheader25
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, (%rdi,%rax,8)
	leaq	2(%rax), %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rdi,%rax,8)
	cmpq	$32000, %rcx            # imm = 0x7D00
	movq	%rcx, %rax
	jne	.LBB0_19
	jmp	.LBB0_20
.LBB0_4:                                # %.preheader.preheader
	movslq	%esi, %r9
	movl	$31999, %eax            # imm = 0x7CFF
	xorl	%edx, %edx
	divq	%r9
	incq	%rax
	cmpq	$3, %rax
	jbe	.LBB0_5
# BB#7:                                 # %min.iters.checked
	movq	%rax, %rdx
	andq	$65532, %rdx            # imm = 0xFFFC
	je	.LBB0_5
# BB#8:                                 # %vector.scevcheck
	cmpl	$1, %esi
	jne	.LBB0_5
# BB#9:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	-4(%rdx), %r8
	movl	%r8d, %r10d
	shrl	$2, %r10d
	incl	%r10d
	andq	$3, %r10
	je	.LBB0_10
# BB#11:                                # %vector.body.prol.preheader
	leaq	16(%rdi), %rcx
	movq	%r9, %r11
	shlq	$5, %r11
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB0_12:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, -16(%rcx)
	movups	%xmm1, (%rcx)
	addq	$4, %rsi
	addq	%r11, %rcx
	decq	%r10
	jne	.LBB0_12
	jmp	.LBB0_13
.LBB0_5:
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_6:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, (%rdi,%rdx,8)
	addq	%r9, %rdx
	cmpq	$32000, %rdx            # imm = 0x7D00
	jl	.LBB0_6
.LBB0_20:                               # %.loopexit
	xorl	%eax, %eax
	retq
.LBB0_10:
	xorl	%esi, %esi
.LBB0_13:                               # %vector.body.prol.loopexit
	cmpq	$12, %r8
	jb	.LBB0_16
# BB#14:                                # %vector.ph.new
	movq	%rsi, %rcx
	imulq	%r9, %rcx
	leaq	(%rdi,%rcx,8), %rcx
	movq	%r9, %r8
	shlq	$7, %r8
	movq	%r9, %r10
	shlq	$5, %r10
	.p2align	4, 0x90
.LBB0_15:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, (%rcx)
	movups	%xmm1, 16(%rcx)
	leaq	(%rcx,%r10), %r11
	movups	%xmm1, (%rcx,%r10)
	movups	%xmm1, 16(%rcx,%r10)
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	leaq	(%r11,%r10), %r11
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	addq	$16, %rsi
	addq	%r8, %rcx
	cmpq	%rdx, %rsi
	jne	.LBB0_15
.LBB0_16:                               # %middle.block
	cmpq	%rdx, %rax
	je	.LBB0_20
# BB#17:
	imulq	%r9, %rdx
	jmp	.LBB0_6
.Lfunc_end0:
	.size	set1d, .Lfunc_end0-set1d
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	set1ds
	.p2align	4, 0x90
	.type	set1ds,@function
set1ds:                                 # @set1ds
	.cfi_startproc
# BB#0:
	movl	%edx, %ecx
	cmpl	$-1, %ecx
	je	.LBB1_18
# BB#1:
	cmpl	$-2, %ecx
	jne	.LBB1_4
# BB#2:                                 # %.preheader27.preheader
	movl	$2, %eax
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB1_3:                                # %.preheader27
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, -16(%rsi,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, -8(%rsi,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB1_3
	jmp	.LBB1_20
.LBB1_18:                               # %.preheader25.preheader
	xorl	%eax, %eax
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB1_19:                               # %.preheader25
                                        # =>This Inner Loop Header: Depth=1
	leal	1(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, (%rsi,%rax,8)
	leaq	2(%rax), %rcx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, 8(%rsi,%rax,8)
	cmpq	$32000, %rcx            # imm = 0x7D00
	movq	%rcx, %rax
	jne	.LBB1_19
	jmp	.LBB1_20
.LBB1_4:                                # %.preheader.preheader
	movslq	%ecx, %r9
	movl	$31999, %eax            # imm = 0x7CFF
	xorl	%edx, %edx
	divq	%r9
	incq	%rax
	cmpq	$3, %rax
	jbe	.LBB1_5
# BB#7:                                 # %min.iters.checked
	movq	%rax, %rdx
	andq	$65532, %rdx            # imm = 0xFFFC
	je	.LBB1_5
# BB#8:                                 # %vector.scevcheck
	cmpl	$1, %ecx
	jne	.LBB1_5
# BB#9:                                 # %vector.ph
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	leaq	-4(%rdx), %r8
	movl	%r8d, %r10d
	shrl	$2, %r10d
	incl	%r10d
	andq	$3, %r10
	je	.LBB1_10
# BB#11:                                # %vector.body.prol.preheader
	leaq	16(%rsi), %rdi
	movq	%r9, %r11
	shlq	$5, %r11
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_12:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, -16(%rdi)
	movups	%xmm1, (%rdi)
	addq	$4, %rcx
	addq	%r11, %rdi
	decq	%r10
	jne	.LBB1_12
	jmp	.LBB1_13
.LBB1_5:
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movsd	%xmm0, (%rsi,%rdx,8)
	addq	%r9, %rdx
	cmpq	$32000, %rdx            # imm = 0x7D00
	jl	.LBB1_6
.LBB1_20:                               # %.loopexit
	xorl	%eax, %eax
	retq
.LBB1_10:
	xorl	%ecx, %ecx
.LBB1_13:                               # %vector.body.prol.loopexit
	cmpq	$12, %r8
	jb	.LBB1_16
# BB#14:                                # %vector.ph.new
	movq	%rcx, %rdi
	imulq	%r9, %rdi
	leaq	(%rsi,%rdi,8), %rdi
	movq	%r9, %r8
	shlq	$7, %r8
	movq	%r9, %r10
	shlq	$5, %r10
	.p2align	4, 0x90
.LBB1_15:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	%xmm1, (%rdi)
	movups	%xmm1, 16(%rdi)
	leaq	(%rdi,%r10), %r11
	movups	%xmm1, (%rdi,%r10)
	movups	%xmm1, 16(%rdi,%r10)
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	leaq	(%r11,%r10), %r11
	movups	%xmm1, (%r10,%r11)
	movups	%xmm1, 16(%r10,%r11)
	addq	$16, %rcx
	addq	%r8, %rdi
	cmpq	%rdx, %rcx
	jne	.LBB1_15
.LBB1_16:                               # %middle.block
	cmpq	%rdx, %rax
	je	.LBB1_20
# BB#17:
	imulq	%r9, %rdx
	jmp	.LBB1_6
.Lfunc_end1:
	.size	set1ds, .Lfunc_end1-set1ds
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	set2d
	.p2align	4, 0x90
	.type	set2d,@function
set2d:                                  # @set2d
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
.Lcfi6:
	.cfi_offset %rbx, -56
.Lcfi7:
	.cfi_offset %r12, -48
.Lcfi8:
	.cfi_offset %r13, -40
.Lcfi9:
	.cfi_offset %r14, -32
.Lcfi10:
	.cfi_offset %r15, -24
.Lcfi11:
	.cfi_offset %rbp, -16
	movl	%esi, %r8d
	cmpl	$-1, %r8d
	je	.LBB2_6
# BB#1:
	cmpl	$-2, %r8d
	jne	.LBB2_10
# BB#2:                                 # %.preheader48.preheader
	addq	$112, %rdi
	xorl	%eax, %eax
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_3:                                # %.preheader48
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_4 Depth 2
	incq	%rax
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %ecx              # imm = 0x100
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_4:                                # %vector.body
                                        #   Parent Loop BB2_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	%xmm1, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	%xmm1, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	%xmm1, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	addq	$-16, %rcx
	jne	.LBB2_4
# BB#5:                                 # %middle.block
                                        #   in Loop: Header=BB2_3 Depth=1
	addq	$2048, %rdi             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB2_3
	jmp	.LBB2_26
.LBB2_6:                                # %.preheader44.preheader
	addq	$112, %rdi
	xorl	%eax, %eax
	movsd	.LCPI2_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB2_7:                                # %.preheader44
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_8 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %ecx              # imm = 0x100
	movq	%rdi, %rdx
	.p2align	4, 0x90
.LBB2_8:                                # %vector.body82
                                        #   Parent Loop BB2_7 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -112(%rdx)
	movups	%xmm1, -96(%rdx)
	movups	%xmm1, -80(%rdx)
	movups	%xmm1, -64(%rdx)
	movups	%xmm1, -48(%rdx)
	movups	%xmm1, -32(%rdx)
	movups	%xmm1, -16(%rdx)
	movups	%xmm1, (%rdx)
	subq	$-128, %rdx
	addq	$-16, %rcx
	jne	.LBB2_8
# BB#9:                                 # %middle.block83
                                        #   in Loop: Header=BB2_7 Depth=1
	addq	$2048, %rdi             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB2_7
	jmp	.LBB2_26
.LBB2_10:                               # %.preheader.preheader
	movslq	%r8d, %r11
	movl	$255, %eax
	xorl	%edx, %edx
	divq	%r11
	incq	%rax
	movl	%eax, %esi
	andl	$508, %esi              # imm = 0x1FC
	leaq	-4(%rsi), %rcx
	movq	%rcx, -16(%rsp)         # 8-byte Spill
	movl	%ecx, %r10d
	shrl	$2, %r10d
	incl	%r10d
	movq	%r11, %rcx
	imulq	%rsi, %rcx
	movq	%rcx, -32(%rsp)         # 8-byte Spill
	movaps	%xmm0, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	andl	$3, %r10d
	movq	%r11, %r12
	shlq	$5, %r12
	leaq	(,%r11,8), %rcx
	movq	%rcx, -40(%rsp)         # 8-byte Spill
	movq	%r11, %rcx
	shlq	$7, %rcx
	movq	%rcx, -48(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	leaq	16(%rdi), %r9
	movl	%r8d, -52(%rsp)         # 4-byte Spill
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	%r10, -24(%rsp)         # 8-byte Spill
	jmp	.LBB2_11
.LBB2_18:                               #   in Loop: Header=BB2_11 Depth=1
	xorl	%edx, %edx
.LBB2_21:                               # %vector.body97.prol.loopexit
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpq	$12, -16(%rsp)          # 8-byte Folded Reload
	jb	.LBB2_24
# BB#22:                                # %vector.ph102.new
                                        #   in Loop: Header=BB2_11 Depth=1
	leaq	4(%rdx), %rbp
	movq	-40(%rsp), %rax         # 8-byte Reload
	imulq	%rax, %rbp
	leaq	12(%rdx), %r8
	imulq	%rax, %r8
	movq	%rax, %r14
	imulq	%rdx, %r14
	leaq	8(%rdx), %r10
	imulq	%rax, %r10
	movq	-48(%rsp), %rax         # 8-byte Reload
	movq	%r9, %r13
	.p2align	4, 0x90
.LBB2_23:                               # %vector.body97
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -16(%r14,%r13)
	movups	%xmm1, (%r14,%r13)
	movups	%xmm1, -16(%rbp,%r13)
	movups	%xmm1, (%rbp,%r13)
	movups	%xmm1, -16(%r10,%r13)
	movups	%xmm1, (%r10,%r13)
	movups	%xmm1, -16(%r8,%r13)
	movups	%xmm1, (%r8,%r13)
	addq	$16, %rdx
	addq	%rax, %r13
	cmpq	%rsi, %rdx
	jne	.LBB2_23
.LBB2_24:                               # %middle.block98
                                        #   in Loop: Header=BB2_11 Depth=1
	movq	-8(%rsp), %rax          # 8-byte Reload
	cmpq	%rsi, %rax
	movq	-32(%rsp), %rdx         # 8-byte Reload
	movl	-52(%rsp), %r8d         # 4-byte Reload
	movq	-24(%rsp), %r10         # 8-byte Reload
	jne	.LBB2_13
	jmp	.LBB2_25
	.p2align	4, 0x90
.LBB2_11:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_20 Depth 2
                                        #     Child Loop BB2_23 Depth 2
                                        #     Child Loop BB2_14 Depth 2
	cmpq	$3, %rax
	jbe	.LBB2_12
# BB#15:                                # %min.iters.checked100
                                        #   in Loop: Header=BB2_11 Depth=1
	testq	%rsi, %rsi
	je	.LBB2_12
# BB#16:                                # %vector.scevcheck
                                        #   in Loop: Header=BB2_11 Depth=1
	cmpl	$1, %r8d
	jne	.LBB2_12
# BB#17:                                # %vector.ph102
                                        #   in Loop: Header=BB2_11 Depth=1
	testq	%r10, %r10
	je	.LBB2_18
# BB#19:                                # %vector.body97.prol.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	movq	%r9, %rbp
	xorl	%edx, %edx
	movq	%r10, %rbx
	.p2align	4, 0x90
.LBB2_20:                               # %vector.body97.prol
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movups	%xmm1, -16(%rbp)
	movups	%xmm1, (%rbp)
	addq	$4, %rdx
	addq	%r12, %rbp
	decq	%rbx
	jne	.LBB2_20
	jmp	.LBB2_21
	.p2align	4, 0x90
.LBB2_12:                               #   in Loop: Header=BB2_11 Depth=1
	xorl	%edx, %edx
.LBB2_13:                               # %scalar.ph99.preheader
                                        #   in Loop: Header=BB2_11 Depth=1
	leaq	(%rdi,%rdx,8), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_14:                               # %scalar.ph99
                                        #   Parent Loop BB2_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	%xmm0, (%rbp,%rbx,8)
	addq	%r11, %rbx
	leaq	(%rdx,%rbx), %rcx
	cmpq	$256, %rcx              # imm = 0x100
	jl	.LBB2_14
.LBB2_25:                               # %.loopexit115
                                        #   in Loop: Header=BB2_11 Depth=1
	incq	%r15
	addq	$2048, %r9              # imm = 0x800
	addq	$2048, %rdi             # imm = 0x800
	cmpq	$256, %r15              # imm = 0x100
	jne	.LBB2_11
.LBB2_26:                               # %.loopexit
	xorl	%eax, %eax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	set2d, .Lfunc_end2-set2d
	.cfi_endproc

	.globl	sum1d
	.p2align	4, 0x90
	.type	sum1d,@function
sum1d:                                  # @sum1d
	.cfi_startproc
# BB#0:
	xorpd	%xmm0, %xmm0
	movl	$7, %eax
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	addsd	-56(%rdi,%rax,8), %xmm0
	addsd	-48(%rdi,%rax,8), %xmm0
	addsd	-40(%rdi,%rax,8), %xmm0
	addsd	-32(%rdi,%rax,8), %xmm0
	addsd	-24(%rdi,%rax,8), %xmm0
	addsd	-16(%rdi,%rax,8), %xmm0
	addsd	-8(%rdi,%rax,8), %xmm0
	addsd	(%rdi,%rax,8), %xmm0
	addq	$8, %rax
	cmpq	$32007, %rax            # imm = 0x7D07
	jne	.LBB3_1
# BB#2:
	retq
.Lfunc_end3:
	.size	sum1d, .Lfunc_end3-sum1d
	.cfi_endproc

	.globl	check
	.p2align	4, 0x90
	.type	check,@function
check:                                  # @check
	.cfi_startproc
# BB#0:
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	xorpd	%xmm3, %xmm3
	movq	$-256000, %rax          # imm = 0xFFFC1800
	xorpd	%xmm4, %xmm4
	xorpd	%xmm2, %xmm2
	xorpd	%xmm8, %xmm8
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_1:                                # =>This Inner Loop Header: Depth=1
	addsd	global_data+256000(%rax), %xmm0
	addsd	global_data+512032(%rax), %xmm8
	addsd	global_data+768064(%rax), %xmm2
	addsd	global_data+1024096(%rax), %xmm4
	addsd	global_data+1280160(%rax), %xmm3
	addsd	global_data+256008(%rax), %xmm0
	addsd	global_data+512040(%rax), %xmm8
	addsd	global_data+768072(%rax), %xmm2
	addsd	global_data+1024104(%rax), %xmm4
	addsd	global_data+1280168(%rax), %xmm3
	addq	$16, %rax
	jne	.LBB4_1
# BB#2:                                 # %.preheader111.preheader
	xorpd	%xmm6, %xmm6
	xorl	%eax, %eax
	movl	$global_data+2329000, %ecx
	xorpd	%xmm7, %xmm7
	xorpd	%xmm5, %xmm5
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader111
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_4 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB4_4:                                #   Parent Loop BB4_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-1048776(%rsi), %xmm5
	addsd	-524392(%rsi), %xmm7
	addsd	-8(%rsi), %xmm6
	addsd	-1048768(%rsi), %xmm5
	addsd	-524384(%rsi), %xmm7
	addsd	(%rsi), %xmm6
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB4_4
# BB#5:                                 #   in Loop: Header=BB4_3 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB4_3
# BB#6:                                 # %.preheader.preheader
	xorpd	%xmm1, %xmm1
	movq	$-524288, %rax          # imm = 0xFFF80000
	.p2align	4, 0x90
.LBB4_7:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	addsd	array+524288(%rax), %xmm1
	addsd	array+524296(%rax), %xmm1
	addsd	array+524304(%rax), %xmm1
	addsd	array+524312(%rax), %xmm1
	addsd	array+524320(%rax), %xmm1
	addsd	array+524328(%rax), %xmm1
	addsd	array+524336(%rax), %xmm1
	addsd	array+524344(%rax), %xmm1
	addq	$64, %rax
	jne	.LBB4_7
# BB#8:
	leal	12(%rdi), %eax
	cmpl	$135, %eax
	ja	.LBB4_9
# BB#34:
	jmpq	*.LJTI4_0(,%rax,8)
.LBB4_32:
	movl	digits(%rip), %esi
	addsd	temp(%rip), %xmm8
	jmp	.LBB4_14
.LBB4_9:
	cmpl	$1122, %edi             # imm = 0x462
	je	.LBB4_29
# BB#10:
	cmpl	$112233, %edi           # imm = 0x1B669
	jne	.LBB4_33
# BB#11:
	movl	digits(%rip), %esi
	addsd	%xmm7, %xmm5
	addsd	%xmm6, %xmm5
	jmp	.LBB4_19
.LBB4_33:
	retq
.LBB4_31:
	movl	digits(%rip), %esi
	movsd	temp(%rip), %xmm0       # xmm0 = mem[0],zero
	jmp	.LBB4_27
.LBB4_22:
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm1, %xmm0
	jmp	printf                  # TAILCALL
.LBB4_12:
	movl	digits(%rip), %esi
	jmp	.LBB4_27
.LBB4_13:
	movl	digits(%rip), %esi
	jmp	.LBB4_14
.LBB4_15:
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm2, %xmm0
	jmp	printf                  # TAILCALL
.LBB4_16:
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm4, %xmm0
	jmp	printf                  # TAILCALL
.LBB4_17:
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm3, %xmm0
	jmp	printf                  # TAILCALL
.LBB4_18:
	movl	digits(%rip), %esi
	jmp	.LBB4_19
.LBB4_23:
	movl	digits(%rip), %esi
	addsd	%xmm8, %xmm0
	jmp	.LBB4_27
.LBB4_25:
	movl	digits(%rip), %esi
	jmp	.LBB4_26
.LBB4_20:
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm7, %xmm0
	jmp	printf                  # TAILCALL
.LBB4_24:
	movl	digits(%rip), %esi
	addsd	%xmm3, %xmm8
.LBB4_14:
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm8, %xmm0
	jmp	printf                  # TAILCALL
.LBB4_21:
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm6, %xmm0
	jmp	printf                  # TAILCALL
.LBB4_30:
	movl	digits(%rip), %esi
	addsd	%xmm5, %xmm0
	jmp	.LBB4_27
.LBB4_28:
	movl	digits(%rip), %esi
	addsd	%xmm8, %xmm0
.LBB4_26:
	addsd	%xmm2, %xmm0
.LBB4_27:
	movl	$.L.str, %edi
	movb	$1, %al
	jmp	printf                  # TAILCALL
.LBB4_29:
	movl	digits(%rip), %esi
	addsd	%xmm7, %xmm5
.LBB4_19:
	movl	$.L.str, %edi
	movb	$1, %al
	movapd	%xmm5, %xmm0
	jmp	printf                  # TAILCALL
.Lfunc_end4:
	.size	check, .Lfunc_end4-check
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI4_0:
	.quad	.LBB4_32
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_31
	.quad	.LBB4_22
	.quad	.LBB4_12
	.quad	.LBB4_13
	.quad	.LBB4_15
	.quad	.LBB4_16
	.quad	.LBB4_17
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_18
	.quad	.LBB4_23
	.quad	.LBB4_25
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_20
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_24
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_21
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_30
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_33
	.quad	.LBB4_28

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI5_0:
	.quad	4607182418800017408     # double 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_1:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI5_2:
	.quad	4611686018427387904     # double 2
	.quad	4611686018427387904     # double 2
.LCPI5_3:
	.quad	4602678819172646912     # double 0.5
	.quad	4602678819172646912     # double 0.5
.LCPI5_4:
	.quad	-4616189618054758400    # double -1
	.quad	-4616189618054758400    # double -1
.LCPI5_5:
	.quad	4517329193108106637     # double 9.9999999999999995E-7
	.quad	4517329193108106637     # double 9.9999999999999995E-7
.LCPI5_6:
	.quad	4607182423303617035     # double 1.0000009999999999
	.quad	4607182423303617035     # double 1.0000009999999999
	.text
	.globl	init
	.p2align	4, 0x90
	.type	init,@function
init:                                   # @init
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 16
.Lcfi13:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$.L.str.1, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_1
# BB#3:
	movl	$.L.str.2, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_4
# BB#14:
	movl	$.L.str.3, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_15
# BB#19:
	movl	$.L.str.4, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_20
# BB#24:
	movl	$.L.str.5, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_25
# BB#33:
	movl	$.L.str.6, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_34
# BB#48:
	movl	$.L.str.7, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_49
# BB#51:
	movl	$.L.str.8, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_52
# BB#58:
	movl	$.L.str.9, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_59
# BB#67:
	movl	$.L.str.10, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_68
# BB#72:
	movl	$.L.str.11, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_73
# BB#77:
	movl	$.L.str.12, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_78
# BB#86:
	movl	$.L.str.13, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_87
# BB#95:
	movl	$.L.str.14, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_96
# BB#108:
	movl	$.L.str.15, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_109
# BB#119:
	movl	$.L.str.16, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_120
# BB#128:
	movl	$.L.str.17, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_129
# BB#135:
	movl	$.L.str.18, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_136
# BB#140:
	movl	$.L.str.19, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_141
# BB#149:
	movl	$.L.str.20, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_150
# BB#156:
	movl	$.L.str.21, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_157
# BB#161:
	movl	$.L.str.22, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_162
# BB#170:
	movl	$.L.str.23, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_171
# BB#183:
	movl	$.L.str.24, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_184
# BB#190:
	movl	$.L.str.25, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_191
# BB#195:
	movl	$.L.str.26, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_196
# BB#200:
	movl	$.L.str.27, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_201
# BB#205:
	movl	$.L.str.28, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_206
# BB#210:
	movl	$.L.str.29, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_211
# BB#215:
	movl	$.L.str.30, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_216
# BB#222:
	movl	$.L.str.31, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_223
# BB#231:
	movl	$.L.str.32, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_232
# BB#240:
	movl	$.L.str.33, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_241
# BB#249:
	movl	$.L.str.34, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_250
# BB#254:
	movl	$.L.str.35, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_255
# BB#263:
	movl	$.L.str.36, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_264
# BB#270:
	movl	$.L.str.37, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_271
# BB#283:
	movl	$.L.str.38, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_284
# BB#296:
	movl	$.L.str.39, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_297
# BB#311:
	movl	$.L.str.40, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_312
# BB#320:
	movl	$.L.str.41, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_321
# BB#329:
	movl	$.L.str.42, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_330
# BB#338:
	movl	$.L.str.43, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_339
# BB#345:
	movl	$.L.str.44, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_346
# BB#354:
	movl	$.L.str.45, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_355
# BB#359:
	movl	$.L.str.46, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_360
# BB#368:
	movl	$.L.str.47, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_369
# BB#371:
	movl	$.L.str.48, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_372
# BB#374:
	movl	$.L.str.49, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_375
# BB#385:
	movl	$.L.str.50, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_386
# BB#396:
	movl	$.L.str.51, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_397
# BB#407:
	movl	$.L.str.52, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_408
# BB#416:
	movl	$.L.str.53, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_417
# BB#423:
	movl	$.L.str.54, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_424
# BB#434:
	movl	$.L.str.55, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_435
# BB#445:
	movl	$.L.str.56, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_446
# BB#454:
	movl	$.L.str.57, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_455
# BB#467:
	movl	$.L.str.58, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_468
# BB#476:
	movl	$.L.str.59, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_477
# BB#489:
	movl	$.L.str.60, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_490
# BB#502:
	movl	$.L.str.61, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_503
# BB#515:
	movl	$.L.str.62, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_516
# BB#526:
	movl	$.L.str.63, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_527
# BB#533:
	movl	$.L.str.64, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_534
# BB#540:
	movl	$.L.str.65, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_541
# BB#545:
	movl	$.L.str.66, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_546
# BB#548:
	movl	$.L.str.67, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_549
# BB#551:
	movl	$.L.str.68, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_552
# BB#554:
	movl	$.L.str.69, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_555
# BB#567:
	movl	$.L.str.70, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_568
# BB#569:
	movl	$.L.str.71, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_568
# BB#570:
	movl	$.L.str.72, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_571
# BB#573:
	movl	$.L.str.73, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_574
# BB#576:
	movl	$.L.str.74, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_577
# BB#581:
	movl	$.L.str.75, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_582
# BB#584:
	movl	$.L.str.76, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_585
# BB#587:
	movl	$.L.str.77, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_588
# BB#590:
	movl	$.L.str.78, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_940
# BB#591:
	movl	$.L.str.79, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_592
# BB#595:
	movl	$.L.str.80, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_596
# BB#602:
	movl	$.L.str.81, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_603
# BB#608:
	movl	$.L.str.82, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_609
# BB#611:
	movl	$.L.str.83, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_612
# BB#616:
	movl	$.L.str.84, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_617
# BB#619:
	movl	$.L.str.85, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_620
# BB#622:
	movl	$.L.str.86, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_623
# BB#626:
	movl	$.L.str.87, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_627
# BB#637:
	movl	$.L.str.88, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_638
# BB#641:
	movl	$.L.str.89, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_642
# BB#645:
	movl	$.L.str.90, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_646
# BB#648:
	movl	$.L.str.91, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_649
# BB#653:
	movl	$.L.str.92, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_654
# BB#662:
	movl	$.L.str.93, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_663
# BB#668:
	movl	$.L.str.94, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_669
# BB#673:
	movl	$.L.str.95, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_674
# BB#678:
	movl	$.L.str.96, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_679
# BB#685:
	movl	$.L.str.97, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_686
# BB#692:
	movl	$.L.str.98, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_693
# BB#701:
	movl	$.L.str.99, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_702
# BB#714:
	movl	$.L.str.100, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_715
# BB#721:
	movl	$.L.str.101, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_722
# BB#726:
	movl	$.L.str.102, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_727
# BB#731:
	movl	$.L.str.103, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_732
# BB#734:
	movl	$.L.str.104, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_735
# BB#739:
	movl	$.L.str.105, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_740
# BB#744:
	movl	$.L.str.106, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_745
# BB#749:
	movl	$.L.str.107, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_750
# BB#760:
	movl	$.L.str.108, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_761
# BB#771:
	movl	$.L.str.109, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_772
# BB#778:
	movl	$.L.str.110, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_779
# BB#783:
	movl	$.L.str.111, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_784
# BB#788:
	movl	$.L.str.112, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_789
# BB#791:
	movl	$.L.str.113, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_792
# BB#802:
	movl	$.L.str.114, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_803
# BB#811:
	movl	$.L.str.115, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_812
# BB#818:
	movl	$.L.str.116, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_819
# BB#825:
	movl	$.L.str.117, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_826
# BB#830:
	movl	$.L.str.118, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_831
# BB#835:
	movl	$.L.str.119, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_836
# BB#842:
	movl	$.L.str.120, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_843
# BB#847:
	movl	$.L.str.121, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_848
# BB#854:
	movl	$.L.str.122, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_855
# BB#861:
	movl	$.L.str.123, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_862
# BB#868:
	cmpb	$118, (%rbx)
	jne	.LBB5_874
# BB#869:
	cmpb	$97, 1(%rbx)
	jne	.LBB5_874
# BB#870:
	cmpb	$9, 2(%rbx)
	jne	.LBB5_874
# BB#871:
	cmpb	$0, 3(%rbx)
	je	.LBB5_872
.LBB5_874:                              # %.thread
	movl	$.L.str.125, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_875
# BB#877:
	movl	$.L.str.126, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_878
# BB#880:
	movl	$.L.str.127, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_881
# BB#883:
	movl	$.L.str.128, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_884
# BB#886:
	movl	$.L.str.129, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_887
# BB#891:
	movl	$.L.str.130, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_892
# BB#898:
	movl	$.L.str.131, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_899
# BB#903:
	movl	$.L.str.132, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_904
# BB#910:
	movl	$.L.str.133, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_911
# BB#917:
	movl	$.L.str.134, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_918
# BB#920:
	movl	$.L.str.135, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_921
# BB#925:
	movl	$.L.str.136, %esi
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB5_940
# BB#926:                               # %.preheader25.i5053.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_927:                              # %.preheader25.i5053
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_927
# BB#928:                               # %.preheader25.i5048.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_929:                              # %.preheader25.i5048
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_929
# BB#930:                               # %.preheader25.i5043.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_931:                              # %.preheader25.i5043
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_931
# BB#932:                               # %.preheader25.i5038.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_933:                              # %.preheader25.i5038
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_933
# BB#934:                               # %.preheader25.i.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_935:                              # %.preheader25.i
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_935
# BB#936:                               # %.preheader44.i.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
.LBB5_937:                              # %.preheader44.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_938 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_938:                              # %vector.body
                                        #   Parent Loop BB5_937 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_938
# BB#939:                               # %middle.block
                                        #   in Loop: Header=BB5_937 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_937
	jmp	.LBB5_940
.LBB5_1:                                # %.preheader.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB5_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rax), %rcx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	%xmm0, X(,%rax,8)
	leal	2(%rax), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, Y(,%rax,8)
	leal	3(%rax), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, Z(,%rax,8)
	leal	4(%rax), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, U(,%rax,8)
	leal	5(%rax), %edx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edx, %xmm0
	movsd	%xmm0, V(,%rax,8)
	cmpq	$32000, %rcx            # imm = 0x7D00
	movq	%rcx, %rax
	jne	.LBB5_2
	jmp	.LBB5_940
.LBB5_4:                                # %vector.body9970.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_5:                                # %vector.body9970
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_5
# BB#6:                                 # %.preheader27.i6926.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_7:                                # %.preheader27.i6926
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_7
# BB#8:                                 # %.preheader27.i6921.preheader
	movq	$-32000, %rax           # imm = 0x8300
	.p2align	4, 0x90
.LBB5_9:                                # %.preheader27.i6921
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_9
# BB#10:                                # %.preheader27.i6916.preheader
	movq	$-32000, %rax           # imm = 0x8300
	.p2align	4, 0x90
.LBB5_11:                               # %.preheader27.i6916
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_11
# BB#12:                                # %.preheader27.i6911.preheader
	movq	$-32000, %rax           # imm = 0x8300
	.p2align	4, 0x90
.LBB5_13:                               # %.preheader27.i6911
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_13
	jmp	.LBB5_940
.LBB5_15:                               # %vector.body9957.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_16:                               # %vector.body9957
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_16
# BB#17:                                # %.preheader27.i6902.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_18:                               # %.preheader27.i6902
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_18
	jmp	.LBB5_940
.LBB5_20:                               # %vector.body9944.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_21:                               # %vector.body9944
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_21
# BB#22:                                # %.preheader27.i6893.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_23:                               # %.preheader27.i6893
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_23
	jmp	.LBB5_940
.LBB5_25:                               # %.preheader44.i6885.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_26:                               # %.preheader44.i6885
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_27 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_27:                               # %vector.body9914
                                        #   Parent Loop BB5_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_27
# BB#28:                                # %middle.block9915
                                        #   in Loop: Header=BB5_26 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_26
# BB#29:                                # %.preheader48.i6876.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	.p2align	4, 0x90
.LBB5_30:                               # %.preheader48.i6876
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_31 Depth 2
	incq	%rax
	movl	%eax, %edx
	imull	%edx, %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_31:                               # %vector.body9929
                                        #   Parent Loop BB5_30 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_31
# BB#32:                                # %middle.block9930
                                        #   in Loop: Header=BB5_30 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_30
	jmp	.LBB5_940
.LBB5_34:                               # %vector.body9862.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_35:                               # %vector.body9862
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_35
# BB#36:                                # %.preheader.i6862.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movapd	.LCPI5_5(%rip), %xmm0   # xmm0 = [1.000000e-06,1.000000e-06]
	.p2align	4, 0x90
.LBB5_37:                               # %.preheader.i6862
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_38 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_38:                               # %vector.body9875
                                        #   Parent Loop BB5_37 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_38
# BB#39:                                # %middle.block9876
                                        #   in Loop: Header=BB5_37 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_37
# BB#40:                                # %.preheader.i6855.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	.p2align	4, 0x90
.LBB5_41:                               # %.preheader.i6855
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_42 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_42:                               # %vector.body9888
                                        #   Parent Loop BB5_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_42
# BB#43:                                # %middle.block9889
                                        #   in Loop: Header=BB5_41 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_41
# BB#44:                                # %.preheader.i6848.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
	.p2align	4, 0x90
.LBB5_45:                               # %.preheader.i6848
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_46 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_46:                               # %vector.body9901
                                        #   Parent Loop BB5_45 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_46
# BB#47:                                # %middle.block9902
                                        #   in Loop: Header=BB5_45 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_45
	jmp	.LBB5_940
.LBB5_49:                               # %vector.body9849.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_50:                               # %vector.body9849
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_50
	jmp	.LBB5_940
.LBB5_52:                               # %vector.body9823.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_53:                               # %vector.body9823
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_53
# BB#54:                                # %.preheader.i6833.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movapd	.LCPI5_5(%rip), %xmm0   # xmm0 = [1.000000e-06,1.000000e-06]
	.p2align	4, 0x90
.LBB5_55:                               # %.preheader.i6833
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_56 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_56:                               # %vector.body9836
                                        #   Parent Loop BB5_55 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_56
# BB#57:                                # %middle.block9837
                                        #   in Loop: Header=BB5_55 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_55
	jmp	.LBB5_940
.LBB5_59:                               # %.preheader.i6826.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB5_60:                               # %.preheader.i6826
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_61 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_61:                               # %vector.body9795
                                        #   Parent Loop BB5_60 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_61
# BB#62:                                # %middle.block9796
                                        #   in Loop: Header=BB5_60 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_60
# BB#63:                                # %.preheader48.i6820.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB5_64:                               # %.preheader48.i6820
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_65 Depth 2
	incq	%rax
	movl	%eax, %edx
	imull	%edx, %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_65:                               # %vector.body9808
                                        #   Parent Loop BB5_64 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_65
# BB#66:                                # %middle.block9809
                                        #   in Loop: Header=BB5_64 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_64
	jmp	.LBB5_940
.LBB5_68:                               # %vector.body9782.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_69:                               # %vector.body9782
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_69
# BB#70:                                # %.preheader27.i6810.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_71:                               # %.preheader27.i6810
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_71
	jmp	.LBB5_940
.LBB5_73:                               # %vector.body9769.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_74:                               # %vector.body9769
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_74
# BB#75:                                # %.preheader27.i6801.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_76:                               # %.preheader27.i6801
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_76
	jmp	.LBB5_940
.LBB5_78:                               # %.preheader.i6796.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_79:                               # %vector.body9743
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_79
# BB#80:                                # %vector.body9756.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_81:                               # %vector.body9756
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_81
# BB#82:                                # %.preheader25.i6784.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_83:                               # %.preheader25.i6784
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_83
# BB#84:                                # %.preheader25.i6779.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_85:                               # %.preheader25.i6779
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_85
	jmp	.LBB5_940
.LBB5_87:                               # %.preheader.i6774.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_88:                               # %vector.body9717
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_88
# BB#89:                                # %vector.body9730.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_90:                               # %vector.body9730
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_90
# BB#91:                                # %.preheader25.i6762.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_92:                               # %.preheader25.i6762
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_92
# BB#93:                                # %.preheader25.i6757.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_94:                               # %.preheader25.i6757
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_94
	jmp	.LBB5_940
.LBB5_96:                               # %.preheader.i6752.preheader
	xorl	%ebx, %ebx
	movl	$array, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$global_data+1280336, %eax
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_97:                               # %.preheader.i6744
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_98 Depth 2
	movl	$256, %ecx              # imm = 0x100
	movq	%rax, %rdx
	.p2align	4, 0x90
.LBB5_98:                               # %vector.body9678
                                        #   Parent Loop BB5_97 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rdx)
	movaps	%xmm0, -96(%rdx)
	movaps	%xmm0, -80(%rdx)
	movaps	%xmm0, -64(%rdx)
	movaps	%xmm0, -48(%rdx)
	movaps	%xmm0, -32(%rdx)
	movaps	%xmm0, -16(%rdx)
	movaps	%xmm0, (%rdx)
	subq	$-128, %rdx
	addq	$-16, %rcx
	jne	.LBB5_98
# BB#99:                                # %middle.block9679
                                        #   in Loop: Header=BB5_97 Depth=1
	incq	%rbx
	addq	$2048, %rax             # imm = 0x800
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB5_97
# BB#100:                               # %.preheader.i6737.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movaps	.LCPI5_3(%rip), %xmm0   # xmm0 = [5.000000e-01,5.000000e-01]
.LBB5_101:                              # %.preheader.i6737
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_102 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_102:                              # %vector.body9691
                                        #   Parent Loop BB5_101 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_102
# BB#103:                               # %middle.block9692
                                        #   in Loop: Header=BB5_101 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_101
# BB#104:                               # %.preheader.i6730.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
	movapd	.LCPI5_2(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00]
.LBB5_105:                              # %.preheader.i6730
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_106 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_106:                              # %vector.body9704
                                        #   Parent Loop BB5_105 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_106
# BB#107:                               # %middle.block9705
                                        #   in Loop: Header=BB5_105 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_105
	jmp	.LBB5_940
.LBB5_109:                              # %.preheader.i6723.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_110:                              # %.preheader.i6723
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_111 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_111:                              # %vector.body9650
                                        #   Parent Loop BB5_110 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_111
# BB#112:                               # %middle.block9651
                                        #   in Loop: Header=BB5_110 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_110
# BB#113:                               # %.preheader25.i6720.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_114:                              # %.preheader25.i6720
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, array+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, array+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_114
# BB#115:                               # %.preheader44.i6712.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
.LBB5_116:                              # %.preheader44.i6712
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_117 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB5_117:                              # %vector.body9663
                                        #   Parent Loop BB5_116 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_117
# BB#118:                               # %middle.block9664
                                        #   in Loop: Header=BB5_116 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_116
	jmp	.LBB5_940
.LBB5_120:                              # %.preheader.i6706.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_121:                              # %vector.body9637
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_121
# BB#122:                               # %.preheader25.i6698.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_123:                              # %.preheader25.i6698
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_123
# BB#124:                               # %.preheader25.i6693.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_125:                              # %.preheader25.i6693
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_125
# BB#126:                               # %.preheader25.i6688.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_127:                              # %.preheader25.i6688
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_127
	jmp	.LBB5_940
.LBB5_129:                              # %.preheader.i6683.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00]
.LBB5_130:                              # %vector.body9598
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_130
# BB#131:                               # %vector.body9611.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_132:                              # %vector.body9611
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_132
# BB#133:                               # %vector.body9624.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_134:                              # %vector.body9624
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+1024096(%rax)
	movapd	%xmm0, global_data+1024112(%rax)
	movapd	%xmm0, global_data+1024128(%rax)
	movapd	%xmm0, global_data+1024144(%rax)
	movapd	%xmm0, global_data+1024160(%rax)
	movapd	%xmm0, global_data+1024176(%rax)
	movapd	%xmm0, global_data+1024192(%rax)
	movapd	%xmm0, global_data+1024208(%rax)
	movapd	%xmm0, global_data+1024224(%rax)
	movapd	%xmm0, global_data+1024240(%rax)
	movapd	%xmm0, global_data+1024256(%rax)
	movapd	%xmm0, global_data+1024272(%rax)
	movapd	%xmm0, global_data+1024288(%rax)
	movapd	%xmm0, global_data+1024304(%rax)
	movapd	%xmm0, global_data+1024320(%rax)
	movapd	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_134
	jmp	.LBB5_940
.LBB5_136:                              # %vector.body9585.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_137:                              # %vector.body9585
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_137
# BB#138:                               # %.preheader27.i6663.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_139:                              # %.preheader27.i6663
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_139
	jmp	.LBB5_940
.LBB5_141:                              # %.preheader.i6654.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_142:                              # %.preheader.i6654
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_143 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_143:                              # %vector.body9572
                                        #   Parent Loop BB5_142 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_143
# BB#144:                               # %middle.block9573
                                        #   in Loop: Header=BB5_142 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_142
# BB#145:                               # %.preheader25.i6651.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_146:                              # %.preheader25.i6651
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_146
# BB#147:                               # %.preheader25.i6646.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_148:                              # %.preheader25.i6646
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_148
	jmp	.LBB5_940
.LBB5_150:                              # %vector.body9544.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_151:                              # %vector.body9544
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, array+256000(%rax)
	movaps	%xmm0, array+256016(%rax)
	movaps	%xmm0, array+256032(%rax)
	movaps	%xmm0, array+256048(%rax)
	movaps	%xmm0, array+256064(%rax)
	movaps	%xmm0, array+256080(%rax)
	movaps	%xmm0, array+256096(%rax)
	movaps	%xmm0, array+256112(%rax)
	movaps	%xmm0, array+256128(%rax)
	movaps	%xmm0, array+256144(%rax)
	movaps	%xmm0, array+256160(%rax)
	movaps	%xmm0, array+256176(%rax)
	movaps	%xmm0, array+256192(%rax)
	movaps	%xmm0, array+256208(%rax)
	movaps	%xmm0, array+256224(%rax)
	movaps	%xmm0, array+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_151
# BB#152:                               # %.preheader48.i6634.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_153:                              # %.preheader48.i6634
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_154 Depth 2
	incq	%rax
	movl	%eax, %edx
	imull	%edx, %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_154:                              # %vector.body9557
                                        #   Parent Loop BB5_153 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_154
# BB#155:                               # %middle.block9558
                                        #   in Loop: Header=BB5_153 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_153
	jmp	.LBB5_940
.LBB5_157:                              # %vector.body9531.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_158:                              # %vector.body9531
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_158
# BB#159:                               # %.preheader27.i6624.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_160:                              # %.preheader27.i6624
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_160
	jmp	.LBB5_940
.LBB5_162:                              # %vector.body9518.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_163:                              # %vector.body9518
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_163
# BB#164:                               # %.preheader.i6615.preheader
	movl	$global_data+256032, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_165:                              # %.preheader25.i6611
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_165
# BB#166:                               # %.preheader25.i6606.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_167:                              # %.preheader25.i6606
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_167
# BB#168:                               # %.preheader25.i6601.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_169:                              # %.preheader25.i6601
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_169
	jmp	.LBB5_940
.LBB5_171:                              # %vector.body9492.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_172:                              # %vector.body9492
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_172
# BB#173:                               # %.preheader.i6592.preheader
	xorl	%eax, %eax
	movabsq	$4607182418800017408, %rcx # imm = 0x3FF0000000000000
.LBB5_174:                              # %.preheader.i6592
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, global_data+256032(,%rax,8)
	movq	%rcx, global_data+256048(,%rax,8)
	movq	%rcx, global_data+256064(,%rax,8)
	movq	%rcx, global_data+256080(,%rax,8)
	movq	%rcx, global_data+256096(,%rax,8)
	movq	%rcx, global_data+256112(,%rax,8)
	movq	%rcx, global_data+256128(,%rax,8)
	movq	%rcx, global_data+256144(,%rax,8)
	movq	%rcx, global_data+256160(,%rax,8)
	movq	%rcx, global_data+256176(,%rax,8)
	addq	$20, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jl	.LBB5_174
# BB#175:                               # %.preheader.i6588.preheader
	xorl	%eax, %eax
	movabsq	$-4616189618054758400, %rcx # imm = 0xBFF0000000000000
.LBB5_176:                              # %.preheader.i6588
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, global_data+256040(,%rax,8)
	movq	%rcx, global_data+256056(,%rax,8)
	movq	%rcx, global_data+256072(,%rax,8)
	movq	%rcx, global_data+256088(,%rax,8)
	movq	%rcx, global_data+256104(,%rax,8)
	movq	%rcx, global_data+256120(,%rax,8)
	movq	%rcx, global_data+256136(,%rax,8)
	movq	%rcx, global_data+256152(,%rax,8)
	movq	%rcx, global_data+256168(,%rax,8)
	movq	%rcx, global_data+256184(,%rax,8)
	addq	$20, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jl	.LBB5_176
# BB#177:                               # %vector.body9505.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_178:                              # %vector.body9505
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_178
# BB#179:                               # %.preheader25.i6580.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_180:                              # %.preheader25.i6580
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_180
# BB#181:                               # %.preheader25.i6575.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_182:                              # %.preheader25.i6575
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_182
	jmp	.LBB5_940
.LBB5_184:                              # %vector.body9479.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_185:                              # %vector.body9479
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_185
# BB#186:                               # %.preheader25.i6566.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_187:                              # %.preheader25.i6566
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_187
# BB#188:                               # %.preheader25.i6561.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_189:                              # %.preheader25.i6561
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_189
	jmp	.LBB5_940
.LBB5_191:                              # %vector.body9466.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_192:                              # %vector.body9466
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_192
# BB#193:                               # %.preheader27.i6552.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_194:                              # %.preheader27.i6552
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_194
	jmp	.LBB5_940
.LBB5_196:                              # %vector.body9453.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_197:                              # %vector.body9453
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_197
# BB#198:                               # %.preheader27.i6543.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_199:                              # %.preheader27.i6543
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_199
	jmp	.LBB5_940
.LBB5_201:                              # %vector.body9440.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_202:                              # %vector.body9440
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_202
# BB#203:                               # %.preheader27.i6534.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_204:                              # %.preheader27.i6534
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_204
	jmp	.LBB5_940
.LBB5_206:                              # %vector.body9427.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_207:                              # %vector.body9427
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_207
# BB#208:                               # %.preheader27.i6525.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_209:                              # %.preheader27.i6525
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_209
	jmp	.LBB5_940
.LBB5_211:                              # %vector.body9414.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_212:                              # %vector.body9414
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_212
# BB#213:                               # %.preheader27.i6516.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_214:                              # %.preheader27.i6516
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_214
	jmp	.LBB5_940
.LBB5_216:                              # %vector.body9401.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_217:                              # %vector.body9401
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_217
# BB#218:                               # %.preheader25.i6507.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_219:                              # %.preheader25.i6507
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_219
# BB#220:                               # %.preheader25.i6502.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_221:                              # %.preheader25.i6502
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_221
	jmp	.LBB5_940
.LBB5_223:                              # %.preheader.i6497.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_224:                              # %vector.body9388
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_224
# BB#225:                               # %.preheader25.i6489.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_226:                              # %.preheader25.i6489
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_226
# BB#227:                               # %.preheader25.i6484.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_228:                              # %.preheader25.i6484
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_228
# BB#229:                               # %.preheader25.i6479.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_230:                              # %.preheader25.i6479
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_230
	jmp	.LBB5_940
.LBB5_232:                              # %.preheader25.i6474.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_233:                              # %.preheader25.i6474
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_233
# BB#234:                               # %vector.body9362.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00]
.LBB5_235:                              # %vector.body9362
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm1, global_data+512032(%rax)
	movapd	%xmm1, global_data+512048(%rax)
	movapd	%xmm1, global_data+512064(%rax)
	movapd	%xmm1, global_data+512080(%rax)
	movapd	%xmm1, global_data+512096(%rax)
	movapd	%xmm1, global_data+512112(%rax)
	movapd	%xmm1, global_data+512128(%rax)
	movapd	%xmm1, global_data+512144(%rax)
	movapd	%xmm1, global_data+512160(%rax)
	movapd	%xmm1, global_data+512176(%rax)
	movapd	%xmm1, global_data+512192(%rax)
	movapd	%xmm1, global_data+512208(%rax)
	movapd	%xmm1, global_data+512224(%rax)
	movapd	%xmm1, global_data+512240(%rax)
	movapd	%xmm1, global_data+512256(%rax)
	movapd	%xmm1, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_235
# BB#236:                               # %vector.body9375.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_237:                              # %vector.body9375
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm1, global_data+768064(%rax)
	movapd	%xmm1, global_data+768080(%rax)
	movapd	%xmm1, global_data+768096(%rax)
	movapd	%xmm1, global_data+768112(%rax)
	movapd	%xmm1, global_data+768128(%rax)
	movapd	%xmm1, global_data+768144(%rax)
	movapd	%xmm1, global_data+768160(%rax)
	movapd	%xmm1, global_data+768176(%rax)
	movapd	%xmm1, global_data+768192(%rax)
	movapd	%xmm1, global_data+768208(%rax)
	movapd	%xmm1, global_data+768224(%rax)
	movapd	%xmm1, global_data+768240(%rax)
	movapd	%xmm1, global_data+768256(%rax)
	movapd	%xmm1, global_data+768272(%rax)
	movapd	%xmm1, global_data+768288(%rax)
	movapd	%xmm1, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_237
# BB#238:                               # %.preheader25.i6461.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_239:                              # %.preheader25.i6461
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_239
	jmp	.LBB5_940
.LBB5_241:                              # %vector.body9349.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_242:                              # %vector.body9349
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_242
# BB#243:                               # %.preheader25.i6452.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_244:                              # %.preheader25.i6452
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_244
# BB#245:                               # %.preheader25.i6447.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_246:                              # %.preheader25.i6447
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_246
# BB#247:                               # %.preheader25.i6442.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_248:                              # %.preheader25.i6442
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_248
	jmp	.LBB5_940
.LBB5_250:                              # %.preheader.i6437.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_251:                              # %vector.body9323
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_251
# BB#252:                               # %vector.body9336.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_253:                              # %vector.body9336
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_253
	jmp	.LBB5_940
.LBB5_255:                              # %.preheader.i6421.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_256:                              # %.preheader.i6421
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_257 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_257:                              # %vector.body9295
                                        #   Parent Loop BB5_256 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_257
# BB#258:                               # %middle.block9296
                                        #   in Loop: Header=BB5_256 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_256
# BB#259:                               # %.preheader48.i6415.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_260:                              # %.preheader48.i6415
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_261 Depth 2
	incq	%rax
	movl	%eax, %edx
	imull	%edx, %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_261:                              # %vector.body9308
                                        #   Parent Loop BB5_260 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_261
# BB#262:                               # %middle.block9309
                                        #   in Loop: Header=BB5_260 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_260
	jmp	.LBB5_940
.LBB5_264:                              # %.preheader.i6405.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_265:                              # %.preheader.i6405
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_266 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_266:                              # %vector.body9282
                                        #   Parent Loop BB5_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_266
# BB#267:                               # %middle.block9283
                                        #   in Loop: Header=BB5_265 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_265
# BB#268:                               # %.preheader.i6398.preheader
	movl	$global_data+1804608, %edi
.LBB5_269:                              # %set1d.exit6912
	xorl	%esi, %esi
	movl	$524288, %edx           # imm = 0x80000
	callq	memset
	jmp	.LBB5_940
.LBB5_271:                              # %.preheader44.i6392.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_272:                              # %.preheader44.i6392
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_273 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_273:                              # %vector.body9237
                                        #   Parent Loop BB5_272 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_273
# BB#274:                               # %middle.block9238
                                        #   in Loop: Header=BB5_272 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_272
# BB#275:                               # %.preheader44.i6383.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
.LBB5_276:                              # %.preheader44.i6383
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_277 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_277:                              # %vector.body9252
                                        #   Parent Loop BB5_276 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_277
# BB#278:                               # %middle.block9253
                                        #   in Loop: Header=BB5_276 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_276
# BB#279:                               # %.preheader44.i6374.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
.LBB5_280:                              # %.preheader44.i6374
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_281 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_281:                              # %vector.body9267
                                        #   Parent Loop BB5_280 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_281
# BB#282:                               # %middle.block9268
                                        #   in Loop: Header=BB5_280 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_280
	jmp	.LBB5_940
.LBB5_284:                              # %.preheader.i6364.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_285:                              # %.preheader.i6364
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_286 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_286:                              # %vector.body9194
                                        #   Parent Loop BB5_285 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_286
# BB#287:                               # %middle.block9195
                                        #   in Loop: Header=BB5_285 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_285
# BB#288:                               # %.preheader44.i6358.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_289:                              # %.preheader44.i6358
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_290 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_290:                              # %vector.body9207
                                        #   Parent Loop BB5_289 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_290
# BB#291:                               # %middle.block9208
                                        #   in Loop: Header=BB5_289 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_289
# BB#292:                               # %.preheader44.i6349.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
.LBB5_293:                              # %.preheader44.i6349
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_294 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_294:                              # %vector.body9222
                                        #   Parent Loop BB5_293 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_294
# BB#295:                               # %middle.block9223
                                        #   in Loop: Header=BB5_293 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_293
	jmp	.LBB5_940
.LBB5_297:                              # %vector.body9153.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm1   # xmm1 = [1.000000e+00,1.000000e+00]
.LBB5_298:                              # %vector.body9153
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm1, global_data+256000(%rax)
	movaps	%xmm1, global_data+256016(%rax)
	movaps	%xmm1, global_data+256032(%rax)
	movaps	%xmm1, global_data+256048(%rax)
	movaps	%xmm1, global_data+256064(%rax)
	movaps	%xmm1, global_data+256080(%rax)
	movaps	%xmm1, global_data+256096(%rax)
	movaps	%xmm1, global_data+256112(%rax)
	movaps	%xmm1, global_data+256128(%rax)
	movaps	%xmm1, global_data+256144(%rax)
	movaps	%xmm1, global_data+256160(%rax)
	movaps	%xmm1, global_data+256176(%rax)
	movaps	%xmm1, global_data+256192(%rax)
	movaps	%xmm1, global_data+256208(%rax)
	movaps	%xmm1, global_data+256224(%rax)
	movaps	%xmm1, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_298
# BB#299:                               # %.preheader25.i6339.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_300:                              # %.preheader25.i6339
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_300
# BB#301:                               # %.preheader25.i6334.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_302:                              # %.preheader25.i6334
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm0, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_302
# BB#303:                               # %.preheader.i6325.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
.LBB5_304:                              # %.preheader.i6325
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_305 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_305:                              # %vector.body9166
                                        #   Parent Loop BB5_304 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_305
# BB#306:                               # %middle.block9167
                                        #   in Loop: Header=BB5_304 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_304
# BB#307:                               # %.preheader48.i.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
.LBB5_308:                              # %.preheader48.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_309 Depth 2
	incq	%rax
	movl	%eax, %edx
	imull	%edx, %edx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%edx, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_309:                              # %vector.body9179
                                        #   Parent Loop BB5_308 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_309
# BB#310:                               # %middle.block9180
                                        #   in Loop: Header=BB5_308 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_308
	jmp	.LBB5_940
.LBB5_312:                              # %vector.body9101.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_313:                              # %vector.body9101
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_313
# BB#314:                               # %vector.body9114.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_315:                              # %vector.body9114
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_315
# BB#316:                               # %vector.body9127.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_317:                              # %vector.body9127
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_317
# BB#318:                               # %vector.body9140.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_319:                              # %vector.body9140
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+1024096(%rax)
	movapd	%xmm0, global_data+1024112(%rax)
	movapd	%xmm0, global_data+1024128(%rax)
	movapd	%xmm0, global_data+1024144(%rax)
	movapd	%xmm0, global_data+1024160(%rax)
	movapd	%xmm0, global_data+1024176(%rax)
	movapd	%xmm0, global_data+1024192(%rax)
	movapd	%xmm0, global_data+1024208(%rax)
	movapd	%xmm0, global_data+1024224(%rax)
	movapd	%xmm0, global_data+1024240(%rax)
	movapd	%xmm0, global_data+1024256(%rax)
	movapd	%xmm0, global_data+1024272(%rax)
	movapd	%xmm0, global_data+1024288(%rax)
	movapd	%xmm0, global_data+1024304(%rax)
	movapd	%xmm0, global_data+1024320(%rax)
	movapd	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_319
	jmp	.LBB5_940
.LBB5_321:                              # %vector.body9049.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_5(%rip), %xmm0   # xmm0 = [1.000000e-06,1.000000e-06]
.LBB5_322:                              # %vector.body9049
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_322
# BB#323:                               # %vector.body9062.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_324:                              # %vector.body9062
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_324
# BB#325:                               # %vector.body9075.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_326:                              # %vector.body9075
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_326
# BB#327:                               # %vector.body9088.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_328:                              # %vector.body9088
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+1024096(%rax)
	movapd	%xmm0, global_data+1024112(%rax)
	movapd	%xmm0, global_data+1024128(%rax)
	movapd	%xmm0, global_data+1024144(%rax)
	movapd	%xmm0, global_data+1024160(%rax)
	movapd	%xmm0, global_data+1024176(%rax)
	movapd	%xmm0, global_data+1024192(%rax)
	movapd	%xmm0, global_data+1024208(%rax)
	movapd	%xmm0, global_data+1024224(%rax)
	movapd	%xmm0, global_data+1024240(%rax)
	movapd	%xmm0, global_data+1024256(%rax)
	movapd	%xmm0, global_data+1024272(%rax)
	movapd	%xmm0, global_data+1024288(%rax)
	movapd	%xmm0, global_data+1024304(%rax)
	movapd	%xmm0, global_data+1024320(%rax)
	movapd	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_328
	jmp	.LBB5_940
.LBB5_330:                              # %.preheader.i6289.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_331:                              # %vector.body9036
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_331
# BB#332:                               # %.preheader25.i6281.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_333:                              # %.preheader25.i6281
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_333
# BB#334:                               # %.preheader25.i6276.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_335:                              # %.preheader25.i6276
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_335
# BB#336:                               # %.preheader25.i6271.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_337:                              # %.preheader25.i6271
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_337
	jmp	.LBB5_940
.LBB5_339:                              # %.preheader.i6266.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_340:                              # %vector.body8997
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_340
# BB#341:                               # %vector.body9010.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_5(%rip), %xmm0   # xmm0 = [1.000000e-06,1.000000e-06]
.LBB5_342:                              # %vector.body9010
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_342
# BB#343:                               # %vector.body9023.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_344:                              # %vector.body9023
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+1024096(%rax)
	movapd	%xmm0, global_data+1024112(%rax)
	movapd	%xmm0, global_data+1024128(%rax)
	movapd	%xmm0, global_data+1024144(%rax)
	movapd	%xmm0, global_data+1024160(%rax)
	movapd	%xmm0, global_data+1024176(%rax)
	movapd	%xmm0, global_data+1024192(%rax)
	movapd	%xmm0, global_data+1024208(%rax)
	movapd	%xmm0, global_data+1024224(%rax)
	movapd	%xmm0, global_data+1024240(%rax)
	movapd	%xmm0, global_data+1024256(%rax)
	movapd	%xmm0, global_data+1024272(%rax)
	movapd	%xmm0, global_data+1024288(%rax)
	movapd	%xmm0, global_data+1024304(%rax)
	movapd	%xmm0, global_data+1024320(%rax)
	movapd	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_344
	jmp	.LBB5_940
.LBB5_346:                              # %.preheader.i6250.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_347:                              # %vector.body8984
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_347
# BB#348:                               # %.preheader25.i6242.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_349:                              # %.preheader25.i6242
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_349
# BB#350:                               # %.preheader25.i6237.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_351:                              # %.preheader25.i6237
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_351
# BB#352:                               # %.preheader25.i6232.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_353:                              # %.preheader25.i6232
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_353
	jmp	.LBB5_940
.LBB5_355:                              # %.preheader.i6227.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_356:                              # %vector.body8958
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_356
# BB#357:                               # %vector.body8971.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_358:                              # %vector.body8971
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_358
	jmp	.LBB5_940
.LBB5_360:                              # %vector.body8919.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_361:                              # %vector.body8919
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_361
# BB#362:                               # %vector.body8932.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_5(%rip), %xmm1   # xmm1 = [1.000000e-06,1.000000e-06]
.LBB5_363:                              # %vector.body8932
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm1, global_data+512032(%rax)
	movapd	%xmm1, global_data+512048(%rax)
	movapd	%xmm1, global_data+512064(%rax)
	movapd	%xmm1, global_data+512080(%rax)
	movapd	%xmm1, global_data+512096(%rax)
	movapd	%xmm1, global_data+512112(%rax)
	movapd	%xmm1, global_data+512128(%rax)
	movapd	%xmm1, global_data+512144(%rax)
	movapd	%xmm1, global_data+512160(%rax)
	movapd	%xmm1, global_data+512176(%rax)
	movapd	%xmm1, global_data+512192(%rax)
	movapd	%xmm1, global_data+512208(%rax)
	movapd	%xmm1, global_data+512224(%rax)
	movapd	%xmm1, global_data+512240(%rax)
	movapd	%xmm1, global_data+512256(%rax)
	movapd	%xmm1, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_363
# BB#364:                               # %vector.body8945.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_365:                              # %vector.body8945
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_365
# BB#366:                               # %.preheader25.i6203.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_367:                              # %.preheader25.i6203
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_367
	jmp	.LBB5_940
.LBB5_369:                              # %.preheader.i6198.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_370:                              # %vector.body8906
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_370
	jmp	.LBB5_940
.LBB5_372:                              # %.preheader.i6190.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_373:                              # %vector.body8893
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_373
	jmp	.LBB5_940
.LBB5_375:                              # %vector.body8854.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_376:                              # %vector.body8854
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_376
# BB#377:                               # %.preheader.i6174.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [2.000000e+00,2.000000e+00]
.LBB5_378:                              # %.preheader.i6174
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_379 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_379:                              # %vector.body8867
                                        #   Parent Loop BB5_378 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_379
# BB#380:                               # %middle.block8868
                                        #   in Loop: Header=BB5_378 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_378
# BB#381:                               # %.preheader.i6167.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
.LBB5_382:                              # %.preheader.i6167
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_383 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_383:                              # %vector.body8880
                                        #   Parent Loop BB5_382 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_383
# BB#384:                               # %middle.block8881
                                        #   in Loop: Header=BB5_382 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_382
	jmp	.LBB5_940
.LBB5_386:                              # %vector.body8815.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_387:                              # %vector.body8815
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_387
# BB#388:                               # %.preheader.i6156.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [2.000000e+00,2.000000e+00]
.LBB5_389:                              # %.preheader.i6156
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_390 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_390:                              # %vector.body8828
                                        #   Parent Loop BB5_389 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_390
# BB#391:                               # %middle.block8829
                                        #   in Loop: Header=BB5_389 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_389
# BB#392:                               # %.preheader.i6149.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
.LBB5_393:                              # %.preheader.i6149
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_394 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_394:                              # %vector.body8841
                                        #   Parent Loop BB5_393 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_394
# BB#395:                               # %middle.block8842
                                        #   in Loop: Header=BB5_393 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_393
	jmp	.LBB5_940
.LBB5_397:                              # %.preheader25.i6146.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_398:                              # %.preheader25.i6146
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_398
# BB#399:                               # %.preheader.i6141.preheader
	movl	$global_data+256032, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_400:                              # %.preheader25.i6137
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_400
# BB#401:                               # %.preheader25.i6132.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_402:                              # %.preheader25.i6132
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
	movapd	%xmm1, %xmm2
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movapd	%xmm2, %xmm1
	divsd	%xmm0, %xmm1
	movsd	%xmm1, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_402
# BB#403:                               # %.preheader.i6127.preheader
	xorl	%ebx, %ebx
	movl	$global_data+1024160, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$global_data+1280336, %eax
.LBB5_404:                              # %.preheader44.i6120
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_405 Depth 2
	incq	%rbx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebx, %xmm1
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
	divsd	%xmm1, %xmm0
	movlhps	%xmm0, %xmm0            # xmm0 = xmm0[0,0]
	movl	$256, %ecx              # imm = 0x100
	movq	%rax, %rdx
.LBB5_405:                              # %vector.body8800
                                        #   Parent Loop BB5_404 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rdx)
	movaps	%xmm0, -96(%rdx)
	movaps	%xmm0, -80(%rdx)
	movaps	%xmm0, -64(%rdx)
	movaps	%xmm0, -48(%rdx)
	movaps	%xmm0, -32(%rdx)
	movaps	%xmm0, -16(%rdx)
	movaps	%xmm0, (%rdx)
	subq	$-128, %rdx
	addq	$-16, %rcx
	jne	.LBB5_405
# BB#406:                               # %middle.block8801
                                        #   in Loop: Header=BB5_404 Depth=1
	addq	$2048, %rax             # imm = 0x800
	cmpq	$256, %rbx              # imm = 0x100
	jne	.LBB5_404
	jmp	.LBB5_940
.LBB5_408:                              # %vector.body8774.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_409:                              # %vector.body8774
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_409
# BB#410:                               # %.preheader27.i6110.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm1   # xmm1 = mem[0],zero
.LBB5_411:                              # %.preheader27.i6110
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_411
# BB#412:                               # %.preheader27.i6105.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_413:                              # %.preheader27.i6105
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%ecx, %xmm2
	movapd	%xmm1, %xmm3
	divsd	%xmm2, %xmm3
	movsd	%xmm3, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_413
# BB#414:                               # %vector.body8787.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_415:                              # %vector.body8787
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+1024096(%rax)
	movapd	%xmm0, global_data+1024112(%rax)
	movapd	%xmm0, global_data+1024128(%rax)
	movapd	%xmm0, global_data+1024144(%rax)
	movapd	%xmm0, global_data+1024160(%rax)
	movapd	%xmm0, global_data+1024176(%rax)
	movapd	%xmm0, global_data+1024192(%rax)
	movapd	%xmm0, global_data+1024208(%rax)
	movapd	%xmm0, global_data+1024224(%rax)
	movapd	%xmm0, global_data+1024240(%rax)
	movapd	%xmm0, global_data+1024256(%rax)
	movapd	%xmm0, global_data+1024272(%rax)
	movapd	%xmm0, global_data+1024288(%rax)
	movapd	%xmm0, global_data+1024304(%rax)
	movapd	%xmm0, global_data+1024320(%rax)
	movapd	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_415
	jmp	.LBB5_940
.LBB5_417:                              # %vector.body8761.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_418:                              # %vector.body8761
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_418
# BB#419:                               # %.preheader25.i6092.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_420:                              # %.preheader25.i6092
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_420
# BB#421:                               # %.preheader25.i6087.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_422:                              # %.preheader25.i6087
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_422
	jmp	.LBB5_940
.LBB5_424:                              # %vector.body8722.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_425:                              # %vector.body8722
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_425
# BB#426:                               # %vector.body8735.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_427:                              # %vector.body8735
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_427
# BB#428:                               # %.preheader25.i6074.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_429:                              # %.preheader25.i6074
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_429
# BB#430:                               # %.preheader25.i6069.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_431:                              # %.preheader25.i6069
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_431
# BB#432:                               # %vector.body8748.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_2(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00]
.LBB5_433:                              # %vector.body8748
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+1280160(%rax)
	movapd	%xmm0, global_data+1280176(%rax)
	movapd	%xmm0, global_data+1280192(%rax)
	movapd	%xmm0, global_data+1280208(%rax)
	movapd	%xmm0, global_data+1280224(%rax)
	movapd	%xmm0, global_data+1280240(%rax)
	movapd	%xmm0, global_data+1280256(%rax)
	movapd	%xmm0, global_data+1280272(%rax)
	movapd	%xmm0, global_data+1280288(%rax)
	movapd	%xmm0, global_data+1280304(%rax)
	movapd	%xmm0, global_data+1280320(%rax)
	movapd	%xmm0, global_data+1280336(%rax)
	movapd	%xmm0, global_data+1280352(%rax)
	movapd	%xmm0, global_data+1280368(%rax)
	movapd	%xmm0, global_data+1280384(%rax)
	movapd	%xmm0, global_data+1280400(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_433
	jmp	.LBB5_940
.LBB5_435:                              # %vector.body8670.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_436:                              # %vector.body8670
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_436
# BB#437:                               # %vector.body8683.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_438:                              # %vector.body8683
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_438
# BB#439:                               # %vector.body8696.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_440:                              # %vector.body8696
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_440
# BB#441:                               # %vector.body8709.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_5(%rip), %xmm0   # xmm0 = [1.000000e-06,1.000000e-06]
.LBB5_442:                              # %vector.body8709
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1024096(%rax)
	movaps	%xmm0, global_data+1024112(%rax)
	movaps	%xmm0, global_data+1024128(%rax)
	movaps	%xmm0, global_data+1024144(%rax)
	movaps	%xmm0, global_data+1024160(%rax)
	movaps	%xmm0, global_data+1024176(%rax)
	movaps	%xmm0, global_data+1024192(%rax)
	movaps	%xmm0, global_data+1024208(%rax)
	movaps	%xmm0, global_data+1024224(%rax)
	movaps	%xmm0, global_data+1024240(%rax)
	movaps	%xmm0, global_data+1024256(%rax)
	movaps	%xmm0, global_data+1024272(%rax)
	movaps	%xmm0, global_data+1024288(%rax)
	movaps	%xmm0, global_data+1024304(%rax)
	movaps	%xmm0, global_data+1024320(%rax)
	movaps	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_442
# BB#443:                               # %.preheader25.i6044.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_444:                              # %.preheader25.i6044
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_444
	jmp	.LBB5_940
.LBB5_446:                              # %.preheader.i6039.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_447:                              # %vector.body8644
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_447
# BB#448:                               # %vector.body8657.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_449:                              # %vector.body8657
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_449
# BB#450:                               # %.preheader25.i6027.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_451:                              # %.preheader25.i6027
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_451
# BB#452:                               # %.preheader25.i6022.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_453:                              # %.preheader25.i6022
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_453
	jmp	.LBB5_940
.LBB5_455:                              # %.preheader.i6013.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_456:                              # %.preheader.i6013
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_457 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_457:                              # %vector.body8605
                                        #   Parent Loop BB5_456 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_457
# BB#458:                               # %middle.block8606
                                        #   in Loop: Header=BB5_456 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_456
# BB#459:                               # %.preheader.i6006.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movapd	.LCPI5_5(%rip), %xmm0   # xmm0 = [1.000000e-06,1.000000e-06]
.LBB5_460:                              # %.preheader.i6006
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_461 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_461:                              # %vector.body8618
                                        #   Parent Loop BB5_460 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_461
# BB#462:                               # %middle.block8619
                                        #   in Loop: Header=BB5_460 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_460
# BB#463:                               # %.preheader.i5999.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
.LBB5_464:                              # %.preheader.i5999
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_465 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_465:                              # %vector.body8631
                                        #   Parent Loop BB5_464 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_465
# BB#466:                               # %middle.block8632
                                        #   in Loop: Header=BB5_464 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_464
	jmp	.LBB5_940
.LBB5_468:                              # %vector.body8592.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_469:                              # %vector.body8592
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_469
# BB#470:                               # %.preheader25.i5992.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_471:                              # %.preheader25.i5992
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_471
# BB#472:                               # %.preheader25.i5987.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_473:                              # %.preheader25.i5987
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_473
# BB#474:                               # %.preheader25.i5982.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_475:                              # %.preheader25.i5982
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_475
	jmp	.LBB5_940
.LBB5_477:                              # %vector.body8553.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_478:                              # %vector.body8553
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_478
# BB#479:                               # %vector.body8566.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_480:                              # %vector.body8566
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_480
# BB#481:                               # %vector.body8579.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_4(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
.LBB5_482:                              # %vector.body8579
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+640032(%rax)
	movaps	%xmm0, global_data+640048(%rax)
	movaps	%xmm0, global_data+640064(%rax)
	movaps	%xmm0, global_data+640080(%rax)
	movaps	%xmm0, global_data+640096(%rax)
	movaps	%xmm0, global_data+640112(%rax)
	movaps	%xmm0, global_data+640128(%rax)
	movaps	%xmm0, global_data+640144(%rax)
	movaps	%xmm0, global_data+640160(%rax)
	movaps	%xmm0, global_data+640176(%rax)
	movaps	%xmm0, global_data+640192(%rax)
	movaps	%xmm0, global_data+640208(%rax)
	movaps	%xmm0, global_data+640224(%rax)
	movaps	%xmm0, global_data+640240(%rax)
	movaps	%xmm0, global_data+640256(%rax)
	movaps	%xmm0, global_data+640272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_482
# BB#483:                               # %.preheader25.i5965.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_484:                              # %.preheader25.i5965
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_484
# BB#485:                               # %.preheader25.i5960.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_486:                              # %.preheader25.i5960
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_486
# BB#487:                               # %.preheader25.i5955.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_488:                              # %.preheader25.i5955
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_488
	jmp	.LBB5_940
.LBB5_490:                              # %vector.body8514.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_4(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
.LBB5_491:                              # %vector.body8514
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_491
# BB#492:                               # %vector.body8527.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_493:                              # %vector.body8527
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384000(%rax)
	movaps	%xmm0, global_data+384016(%rax)
	movaps	%xmm0, global_data+384032(%rax)
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_493
# BB#494:                               # %vector.body8540.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_495:                              # %vector.body8540
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_495
# BB#496:                               # %.preheader25.i5938.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_497:                              # %.preheader25.i5938
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_497
# BB#498:                               # %.preheader25.i5933.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_499:                              # %.preheader25.i5933
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_499
# BB#500:                               # %.preheader25.i5928.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_501:                              # %.preheader25.i5928
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_501
	jmp	.LBB5_940
.LBB5_503:                              # %vector.body8475.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_4(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
.LBB5_504:                              # %vector.body8475
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_504
# BB#505:                               # %vector.body8488.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_506:                              # %vector.body8488
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+384000(%rax)
	movaps	%xmm0, global_data+384016(%rax)
	movaps	%xmm0, global_data+384032(%rax)
	movaps	%xmm0, global_data+384048(%rax)
	movaps	%xmm0, global_data+384064(%rax)
	movaps	%xmm0, global_data+384080(%rax)
	movaps	%xmm0, global_data+384096(%rax)
	movaps	%xmm0, global_data+384112(%rax)
	movaps	%xmm0, global_data+384128(%rax)
	movaps	%xmm0, global_data+384144(%rax)
	movaps	%xmm0, global_data+384160(%rax)
	movaps	%xmm0, global_data+384176(%rax)
	movaps	%xmm0, global_data+384192(%rax)
	movaps	%xmm0, global_data+384208(%rax)
	movaps	%xmm0, global_data+384224(%rax)
	movaps	%xmm0, global_data+384240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_506
# BB#507:                               # %vector.body8501.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_508:                              # %vector.body8501
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_508
# BB#509:                               # %.preheader25.i5911.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_510:                              # %.preheader25.i5911
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_510
# BB#511:                               # %.preheader25.i5906.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_512:                              # %.preheader25.i5906
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_512
# BB#513:                               # %.preheader25.i5901.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_514:                              # %.preheader25.i5901
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_514
	jmp	.LBB5_940
.LBB5_516:                              # %vector.body8449.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_517:                              # %vector.body8449
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_517
# BB#518:                               # %vector.body8462.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_519:                              # %vector.body8462
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_519
# BB#520:                               # %.preheader25.i5888.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_521:                              # %.preheader25.i5888
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_521
# BB#522:                               # %.preheader25.i5883.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_523:                              # %.preheader25.i5883
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_523
# BB#524:                               # %.preheader25.i5878.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_525:                              # %.preheader25.i5878
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_525
	jmp	.LBB5_940
.LBB5_527:                              # %vector.body8436.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_528:                              # %vector.body8436
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_528
# BB#529:                               # %.preheader25.i5869.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_530:                              # %.preheader25.i5869
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_530
# BB#531:                               # %.preheader25.i5864.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_532:                              # %.preheader25.i5864
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_532
	jmp	.LBB5_940
.LBB5_534:                              # %vector.body8423.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_535:                              # %vector.body8423
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_535
# BB#536:                               # %.preheader25.i5855.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_537:                              # %.preheader25.i5855
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_537
# BB#538:                               # %.preheader25.i5850.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_539:                              # %.preheader25.i5850
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_539
	jmp	.LBB5_940
.LBB5_541:                              # %.preheader.i5845.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_542:                              # %vector.body8397
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_542
# BB#543:                               # %vector.body8410.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_544:                              # %vector.body8410
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_544
	jmp	.LBB5_940
.LBB5_546:                              # %.preheader.i5833.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_547:                              # %vector.body8384
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_547
	jmp	.LBB5_940
.LBB5_549:                              # %.preheader.i5825.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_550:                              # %vector.body8371
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_550
	jmp	.LBB5_940
.LBB5_552:                              # %.preheader25.i5817.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_553:                              # %.preheader25.i5817
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_553
	jmp	.LBB5_940
.LBB5_568:                              # %.preheader.i5783.preheader
	movl	$global_data+1280224, %edi
	jmp	.LBB5_269
.LBB5_555:                              # %.preheader.i5808.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_556:                              # %.preheader.i5808
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_557 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_557:                              # %vector.body8328
                                        #   Parent Loop BB5_556 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_557
# BB#558:                               # %middle.block8329
                                        #   in Loop: Header=BB5_556 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_556
# BB#559:                               # %.preheader44.i5802.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_560:                              # %.preheader44.i5802
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_561 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_561:                              # %vector.body8341
                                        #   Parent Loop BB5_560 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_561
# BB#562:                               # %middle.block8342
                                        #   in Loop: Header=BB5_560 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_560
# BB#563:                               # %.preheader44.i5793.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
.LBB5_564:                              # %.preheader44.i5793
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_565 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_565:                              # %vector.body8356
                                        #   Parent Loop BB5_564 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_565
# BB#566:                               # %middle.block8357
                                        #   in Loop: Header=BB5_564 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_564
.LBB5_940:                              # %set1d.exit6912
	xorl	%eax, %eax
	popq	%rbx
	retq
.LBB5_571:                              # %.preheader25.i5773.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_572:                              # %.preheader25.i5773
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_572
	jmp	.LBB5_940
.LBB5_574:                              # %vector.body8315.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_6(%rip), %xmm0   # xmm0 = [1.000001e+00,1.000001e+00]
.LBB5_575:                              # %vector.body8315
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_575
	jmp	.LBB5_940
.LBB5_577:                              # %.preheader25.i5764.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_578:                              # %.preheader25.i5764
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_578
# BB#579:                               # %.preheader25.i5759.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_580:                              # %.preheader25.i5759
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_580
	jmp	.LBB5_940
.LBB5_582:                              # %.preheader25.i5754.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_583:                              # %.preheader25.i5754
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_583
	jmp	.LBB5_940
.LBB5_585:                              # %.preheader25.i5749.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_586:                              # %.preheader25.i5749
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_586
	jmp	.LBB5_940
.LBB5_588:                              # %.preheader25.i5744.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_589:                              # %.preheader25.i5744
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_589
	jmp	.LBB5_940
.LBB5_592:                              # %.preheader25.i5739.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_593:                              # %.preheader25.i5739
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_593
.LBB5_594:                              # %set1d.exit5740
	movabsq	$-4611686018427387904, %rax # imm = 0xC000000000000000
	movq	%rax, global_data+255992(%rip)
	jmp	.LBB5_940
.LBB5_596:                              # %.preheader.i5734.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$global_data+256032, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_597:                              # %.preheader25.i5726
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_597
# BB#598:                               # %.preheader25.i5721.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_599:                              # %.preheader25.i5721
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_599
# BB#600:                               # %.preheader25.i5716.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_601:                              # %.preheader25.i5716
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_601
	jmp	.LBB5_940
.LBB5_603:                              # %.preheader44.i5708.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_604:                              # %.preheader44.i5708
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_605 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_605:                              # %vector.body8300
                                        #   Parent Loop BB5_604 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_605
# BB#606:                               # %middle.block8301
                                        #   in Loop: Header=BB5_604 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_604
# BB#607:                               # %set2d.exit5712
	movabsq	$4611686018427387904, %rax # imm = 0x4000000000000000
	movq	%rax, global_data+1804504(%rip)
	jmp	.LBB5_940
.LBB5_609:                              # %.preheader25.i5702.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_610:                              # %.preheader25.i5702
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_610
	jmp	.LBB5_940
.LBB5_612:                              # %.preheader27.i5697.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_613:                              # %.preheader27.i5697
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-16(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-8(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_613
	jmp	.LBB5_614
.LBB5_617:                              # %.preheader25.i5688.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_618:                              # %.preheader25.i5688
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_618
	jmp	.LBB5_594
.LBB5_620:                              # %vector.body8287.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_621:                              # %vector.body8287
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_621
.LBB5_614:                              # %.preheader.i5692.preheader
	movl	$global_data+256032, %edi
	jmp	.LBB5_615
.LBB5_623:                              # %vector.body8274.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_624:                              # %vector.body8274
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_624
# BB#625:                               # %.preheader.i5671.preheader
	movl	$global_data+256032, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$global_data+512064, %edi
.LBB5_615:                              # %set1d.exit6912
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	jmp	.LBB5_940
.LBB5_627:                              # %vector.body8248.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_628:                              # %vector.body8248
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_628
# BB#629:                               # %vector.body8261.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_630:                              # %vector.body8261
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_630
# BB#631:                               # %.preheader25.i5655.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_632:                              # %.preheader25.i5655
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_632
# BB#633:                               # %.preheader25.i5650.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_634:                              # %.preheader25.i5650
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_634
# BB#635:                               # %.preheader25.i5645.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_636:                              # %.preheader25.i5645
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_636
	jmp	.LBB5_940
.LBB5_638:                              # %.preheader25.i5640.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_639:                              # %.preheader25.i5640
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_639
	jmp	.LBB5_640
.LBB5_642:                              # %.preheader27.i5635.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_643:                              # %.preheader27.i5635
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-16(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-8(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_643
# BB#644:                               # %set1d.exit5636
	movabsq	$4611686018427387904, %rax # imm = 0x4000000000000000
	movq	%rax, global_data+255992(%rip)
	jmp	.LBB5_940
.LBB5_646:                              # %.preheader.i5630.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_647:                              # %.preheader25.i5626
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_647
	jmp	.LBB5_940
.LBB5_649:                              # %.preheader25.i5621.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_650:                              # %.preheader25.i5621
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_650
# BB#651:                               # %.preheader25.i5616.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_652:                              # %.preheader25.i5616
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_652
	jmp	.LBB5_940
.LBB5_654:                              # %.preheader44.i5608.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_655:                              # %.preheader44.i5608
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_656 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_656:                              # %vector.body8220
                                        #   Parent Loop BB5_655 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_656
# BB#657:                               # %middle.block8221
                                        #   in Loop: Header=BB5_655 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_655
# BB#658:                               # %.preheader.i5598.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_659:                              # %.preheader.i5598
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_660 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_660:                              # %vector.body8235
                                        #   Parent Loop BB5_659 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	%xmm0, -112(%rsi)
	movapd	%xmm0, -96(%rsi)
	movapd	%xmm0, -80(%rsi)
	movapd	%xmm0, -64(%rsi)
	movapd	%xmm0, -48(%rsi)
	movapd	%xmm0, -32(%rsi)
	movapd	%xmm0, -16(%rsi)
	movapd	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_660
# BB#661:                               # %middle.block8236
                                        #   in Loop: Header=BB5_659 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_659
	jmp	.LBB5_940
.LBB5_663:                              # %vector.body8194.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_664:                              # %vector.body8194
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_664
# BB#665:                               # %vector.body8207.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_666:                              # %vector.body8207
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_666
	jmp	.LBB5_667
.LBB5_669:                              # %.preheader25.i5587.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_670:                              # %.preheader25.i5587
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_670
# BB#671:                               # %.preheader25.i5582.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_672:                              # %.preheader25.i5582
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_672
	jmp	.LBB5_940
.LBB5_674:                              # %vector.body8168.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_675:                              # %vector.body8168
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_675
# BB#676:                               # %vector.body8181.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_677:                              # %vector.body8181
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_677
.LBB5_667:                              # %set1d.exit5592
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, global_data+512064(%rip)
	jmp	.LBB5_940
.LBB5_679:                              # %vector.body8155.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_680:                              # %vector.body8155
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_680
# BB#681:                               # %.preheader25.i5565.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_682:                              # %.preheader25.i5565
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_682
# BB#683:                               # %.preheader25.i5560.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_684:                              # %.preheader25.i5560
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_684
	jmp	.LBB5_940
.LBB5_686:                              # %vector.body8142.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_687:                              # %vector.body8142
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_687
# BB#688:                               # %.preheader25.i5551.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_689:                              # %.preheader25.i5551
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_689
# BB#690:                               # %.preheader25.i5546.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_691:                              # %.preheader25.i5546
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_691
	jmp	.LBB5_940
.LBB5_693:                              # %.preheader.i5541.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_694:                              # %vector.body8116
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_694
# BB#695:                               # %vector.body8129.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_696:                              # %vector.body8129
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_696
# BB#697:                               # %.preheader25.i5529.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_698:                              # %.preheader25.i5529
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_698
# BB#699:                               # %.preheader25.i5524.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_700:                              # %.preheader25.i5524
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_700
	jmp	.LBB5_940
.LBB5_702:                              # %.preheader.i5516.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_703:                              # %.preheader.i5516
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_704 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_704:                              # %vector.body8073
                                        #   Parent Loop BB5_703 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm0, -112(%rsi)
	movaps	%xmm0, -96(%rsi)
	movaps	%xmm0, -80(%rsi)
	movaps	%xmm0, -64(%rsi)
	movaps	%xmm0, -48(%rsi)
	movaps	%xmm0, -32(%rsi)
	movaps	%xmm0, -16(%rsi)
	movaps	%xmm0, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_704
# BB#705:                               # %middle.block8074
                                        #   in Loop: Header=BB5_703 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_703
# BB#706:                               # %.preheader44.i5511.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_707:                              # %.preheader44.i5511
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_708 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_708:                              # %vector.body8086
                                        #   Parent Loop BB5_707 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_708
# BB#709:                               # %middle.block8087
                                        #   in Loop: Header=BB5_707 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_707
# BB#710:                               # %.preheader44.i5502.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
.LBB5_711:                              # %.preheader44.i5502
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_712 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_712:                              # %vector.body8101
                                        #   Parent Loop BB5_711 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_712
# BB#713:                               # %middle.block8102
                                        #   in Loop: Header=BB5_711 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_711
	jmp	.LBB5_940
.LBB5_715:                              # %vector.body8060.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_716:                              # %vector.body8060
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_716
# BB#717:                               # %.preheader25.i5492.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_718:                              # %.preheader25.i5492
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_718
# BB#719:                               # %.preheader25.i5487.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_720:                              # %.preheader25.i5487
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_720
.LBB5_640:                              # %set1d.exit5641
	movabsq	$-4616189618054758400, %rax # imm = 0xBFF0000000000000
	movq	%rax, global_data+255992(%rip)
	jmp	.LBB5_940
.LBB5_722:                              # %.preheader27.i5482.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_723:                              # %.preheader27.i5482
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-16(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-8(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_723
# BB#724:                               # %vector.body8047.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_725:                              # %vector.body8047
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_725
	jmp	.LBB5_940
.LBB5_727:                              # %vector.body8034.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_728:                              # %vector.body8034
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, array+256000(%rax)
	movaps	%xmm0, array+256016(%rax)
	movaps	%xmm0, array+256032(%rax)
	movaps	%xmm0, array+256048(%rax)
	movaps	%xmm0, array+256064(%rax)
	movaps	%xmm0, array+256080(%rax)
	movaps	%xmm0, array+256096(%rax)
	movaps	%xmm0, array+256112(%rax)
	movaps	%xmm0, array+256128(%rax)
	movaps	%xmm0, array+256144(%rax)
	movaps	%xmm0, array+256160(%rax)
	movaps	%xmm0, array+256176(%rax)
	movaps	%xmm0, array+256192(%rax)
	movaps	%xmm0, array+256208(%rax)
	movaps	%xmm0, array+256224(%rax)
	movaps	%xmm0, array+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_728
# BB#729:                               # %.preheader27.i5469.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_730:                              # %.preheader27.i5469
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-16(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-8(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_730
	jmp	.LBB5_940
.LBB5_732:                              # %.preheader.i5464.preheader
	movl	$array, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_733:                              # %.preheader27.i5460
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-16(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-8(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_733
	jmp	.LBB5_940
.LBB5_735:                              # %vector.body8021.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_736:                              # %vector.body8021
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, array+256000(%rax)
	movaps	%xmm0, array+256016(%rax)
	movaps	%xmm0, array+256032(%rax)
	movaps	%xmm0, array+256048(%rax)
	movaps	%xmm0, array+256064(%rax)
	movaps	%xmm0, array+256080(%rax)
	movaps	%xmm0, array+256096(%rax)
	movaps	%xmm0, array+256112(%rax)
	movaps	%xmm0, array+256128(%rax)
	movaps	%xmm0, array+256144(%rax)
	movaps	%xmm0, array+256160(%rax)
	movaps	%xmm0, array+256176(%rax)
	movaps	%xmm0, array+256192(%rax)
	movaps	%xmm0, array+256208(%rax)
	movaps	%xmm0, array+256224(%rax)
	movaps	%xmm0, array+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_736
# BB#737:                               # %.preheader27.i5451.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_738:                              # %.preheader27.i5451
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-16(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-8(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_738
	jmp	.LBB5_940
.LBB5_740:                              # %vector.body8008.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_741:                              # %vector.body8008
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_741
# BB#742:                               # %.preheader27.i5442.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_743:                              # %.preheader27.i5442
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_743
	jmp	.LBB5_940
.LBB5_745:                              # %vector.body7995.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_746:                              # %vector.body7995
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_746
# BB#747:                               # %.preheader27.i5433.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_748:                              # %.preheader27.i5433
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_748
	jmp	.LBB5_940
.LBB5_750:                              # %vector.body7956.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_751:                              # %vector.body7956
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_751
# BB#752:                               # %.preheader25.i5424.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_753:                              # %.preheader25.i5424
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_753
# BB#754:                               # %.preheader25.i5419.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_755:                              # %.preheader25.i5419
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_755
# BB#756:                               # %vector.body7969.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_4(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
.LBB5_757:                              # %vector.body7969
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1024096(%rax)
	movaps	%xmm0, global_data+1024112(%rax)
	movaps	%xmm0, global_data+1024128(%rax)
	movaps	%xmm0, global_data+1024144(%rax)
	movaps	%xmm0, global_data+1024160(%rax)
	movaps	%xmm0, global_data+1024176(%rax)
	movaps	%xmm0, global_data+1024192(%rax)
	movaps	%xmm0, global_data+1024208(%rax)
	movaps	%xmm0, global_data+1024224(%rax)
	movaps	%xmm0, global_data+1024240(%rax)
	movaps	%xmm0, global_data+1024256(%rax)
	movaps	%xmm0, global_data+1024272(%rax)
	movaps	%xmm0, global_data+1024288(%rax)
	movaps	%xmm0, global_data+1024304(%rax)
	movaps	%xmm0, global_data+1024320(%rax)
	movaps	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_757
# BB#758:                               # %.preheader.i5410.preheader
	movl	$global_data+853424, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_759:                              # %vector.body7982
                                        # =>This Inner Loop Header: Depth=1
	movupd	%xmm0, global_data+1194760(%rax)
	movupd	%xmm0, global_data+1194776(%rax)
	movupd	%xmm0, global_data+1194792(%rax)
	movupd	%xmm0, global_data+1194808(%rax)
	movupd	%xmm0, global_data+1194824(%rax)
	movupd	%xmm0, global_data+1194840(%rax)
	movupd	%xmm0, global_data+1194856(%rax)
	movupd	%xmm0, global_data+1194872(%rax)
	movupd	%xmm0, global_data+1194888(%rax)
	movupd	%xmm0, global_data+1194904(%rax)
	movupd	%xmm0, global_data+1194920(%rax)
	movupd	%xmm0, global_data+1194936(%rax)
	movupd	%xmm0, global_data+1194952(%rax)
	movupd	%xmm0, global_data+1194968(%rax)
	movupd	%xmm0, global_data+1194984(%rax)
	movupd	%xmm0, global_data+1195000(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_759
	jmp	.LBB5_940
.LBB5_761:                              # %vector.body7943.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_762:                              # %vector.body7943
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_762
# BB#763:                               # %.preheader25.i5399.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_764:                              # %.preheader25.i5399
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_764
# BB#765:                               # %.preheader25.i5394.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_766:                              # %.preheader25.i5394
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_766
# BB#767:                               # %.preheader25.i5389.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_768:                              # %.preheader25.i5389
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_768
# BB#769:                               # %.preheader25.i5384.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_770:                              # %.preheader25.i5384
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_770
	jmp	.LBB5_940
.LBB5_772:                              # %vector.body7930.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_773:                              # %vector.body7930
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_773
# BB#774:                               # %.preheader25.i5375.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_775:                              # %.preheader25.i5375
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_775
# BB#776:                               # %.preheader25.i5370.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_777:                              # %.preheader25.i5370
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_777
	jmp	.LBB5_940
.LBB5_779:                              # %.preheader25.i5365.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_780:                              # %.preheader25.i5365
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_780
# BB#781:                               # %.preheader25.i5360.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_782:                              # %.preheader25.i5360
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_782
	jmp	.LBB5_940
.LBB5_784:                              # %.preheader.i5355.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_785:                              # %vector.body7904
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_785
# BB#786:                               # %vector.body7917.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_5(%rip), %xmm0   # xmm0 = [1.000000e-06,1.000000e-06]
.LBB5_787:                              # %vector.body7917
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_787
	jmp	.LBB5_940
.LBB5_789:                              # %.preheader.i5343.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_790:                              # %.preheader27.i5339
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_790
	jmp	.LBB5_940
.LBB5_792:                              # %vector.body7865.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_793:                              # %vector.body7865
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_793
# BB#794:                               # %vector.body7878.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_795:                              # %vector.body7878
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_795
# BB#796:                               # %vector.body7891.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_797:                              # %vector.body7891
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_797
# BB#798:                               # %.preheader25.i5322.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_799:                              # %.preheader25.i5322
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_799
# BB#800:                               # %.preheader25.i5317.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_801:                              # %.preheader25.i5317
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280160(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1280168(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_801
	jmp	.LBB5_940
.LBB5_803:                              # %vector.body7852.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_804:                              # %vector.body7852
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_804
# BB#805:                               # %.preheader25.i5308.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_806:                              # %.preheader25.i5308
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_806
# BB#807:                               # %.preheader25.i5303.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_808:                              # %.preheader25.i5303
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_808
# BB#809:                               # %.preheader25.i5298.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_810:                              # %.preheader25.i5298
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_810
	jmp	.LBB5_940
.LBB5_812:                              # %vector.body7839.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_813:                              # %vector.body7839
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_813
# BB#814:                               # %.preheader25.i5289.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_815:                              # %.preheader25.i5289
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_815
# BB#816:                               # %.preheader25.i5284.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_817:                              # %.preheader25.i5284
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_817
	jmp	.LBB5_940
.LBB5_819:                              # %.preheader.i5279.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_820:                              # %vector.body7826
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_820
# BB#821:                               # %.preheader25.i5271.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_822:                              # %.preheader25.i5271
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_822
# BB#823:                               # %.preheader25.i5266.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_824:                              # %.preheader25.i5266
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_824
	jmp	.LBB5_940
.LBB5_826:                              # %vector.body7813.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_827:                              # %vector.body7813
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_827
# BB#828:                               # %.preheader25.i5257.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_829:                              # %.preheader25.i5257
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_829
	jmp	.LBB5_940
.LBB5_831:                              # %.preheader.i5252.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_832:                              # %vector.body7800
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_832
# BB#833:                               # %.preheader27.i5244.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_834:                              # %.preheader27.i5244
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_834
	jmp	.LBB5_940
.LBB5_836:                              # %.preheader.i5239.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_837:                              # %vector.body7787
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_837
# BB#838:                               # %.preheader25.i5231.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_839:                              # %.preheader25.i5231
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_839
# BB#840:                               # %.preheader25.i5226.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_841:                              # %.preheader25.i5226
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_841
	jmp	.LBB5_940
.LBB5_843:                              # %.preheader25.i5221.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_844:                              # %.preheader25.i5221
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_844
# BB#845:                               # %.preheader25.i5216.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_846:                              # %.preheader25.i5216
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_846
	jmp	.LBB5_940
.LBB5_848:                              # %.preheader25.i5211.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_849:                              # %.preheader25.i5211
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_849
# BB#850:                               # %.preheader44.i5203.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
.LBB5_851:                              # %.preheader44.i5203
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_852 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
.LBB5_852:                              # %vector.body7772
                                        #   Parent Loop BB5_851 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB5_852
# BB#853:                               # %middle.block7773
                                        #   in Loop: Header=BB5_851 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB5_851
	jmp	.LBB5_940
.LBB5_855:                              # %.preheader.i5197.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_856:                              # %vector.body7759
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_856
# BB#857:                               # %.preheader25.i5189.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_858:                              # %.preheader25.i5189
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_858
# BB#859:                               # %.preheader25.i5184.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_860:                              # %.preheader25.i5184
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024096(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+1024104(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_860
	jmp	.LBB5_940
.LBB5_862:                              # %vector.body7746.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_863:                              # %vector.body7746
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_863
# BB#864:                               # %.preheader25.i5175.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_865:                              # %.preheader25.i5175
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_865
# BB#866:                               # %.preheader25.i5170.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_867:                              # %.preheader25.i5170
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_867
	jmp	.LBB5_940
.LBB5_872:                              # %.preheader.i5165.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_873:                              # %.preheader27.i5161
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_873
	jmp	.LBB5_940
.LBB5_875:                              # %.preheader.i5156.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_876:                              # %.preheader27.i5152
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_876
	jmp	.LBB5_940
.LBB5_878:                              # %.preheader.i5147.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_879:                              # %.preheader27.i5143
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_879
	jmp	.LBB5_940
.LBB5_881:                              # %.preheader.i5138.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_882:                              # %.preheader27.i5134
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_882
	jmp	.LBB5_940
.LBB5_884:                              # %.preheader.i5129.preheader
	movl	$global_data, %edi
	xorl	%esi, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	memset
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_885:                              # %.preheader27.i5125
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_885
	jmp	.LBB5_940
.LBB5_887:                              # %vector.body7720.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_888:                              # %vector.body7720
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+256000(%rax)
	movapd	%xmm0, global_data+256016(%rax)
	movapd	%xmm0, global_data+256032(%rax)
	movapd	%xmm0, global_data+256048(%rax)
	movapd	%xmm0, global_data+256064(%rax)
	movapd	%xmm0, global_data+256080(%rax)
	movapd	%xmm0, global_data+256096(%rax)
	movapd	%xmm0, global_data+256112(%rax)
	movapd	%xmm0, global_data+256128(%rax)
	movapd	%xmm0, global_data+256144(%rax)
	movapd	%xmm0, global_data+256160(%rax)
	movapd	%xmm0, global_data+256176(%rax)
	movapd	%xmm0, global_data+256192(%rax)
	movapd	%xmm0, global_data+256208(%rax)
	movapd	%xmm0, global_data+256224(%rax)
	movapd	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_888
# BB#889:                               # %vector.body7733.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
.LBB5_890:                              # %vector.body7733
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+512032(%rax)
	movapd	%xmm0, global_data+512048(%rax)
	movapd	%xmm0, global_data+512064(%rax)
	movapd	%xmm0, global_data+512080(%rax)
	movapd	%xmm0, global_data+512096(%rax)
	movapd	%xmm0, global_data+512112(%rax)
	movapd	%xmm0, global_data+512128(%rax)
	movapd	%xmm0, global_data+512144(%rax)
	movapd	%xmm0, global_data+512160(%rax)
	movapd	%xmm0, global_data+512176(%rax)
	movapd	%xmm0, global_data+512192(%rax)
	movapd	%xmm0, global_data+512208(%rax)
	movapd	%xmm0, global_data+512224(%rax)
	movapd	%xmm0, global_data+512240(%rax)
	movapd	%xmm0, global_data+512256(%rax)
	movapd	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_890
	jmp	.LBB5_940
.LBB5_892:                              # %vector.body7707.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_893:                              # %vector.body7707
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_893
# BB#894:                               # %.preheader25.i5108.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_895:                              # %.preheader25.i5108
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_895
# BB#896:                               # %.preheader25.i5103.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_897:                              # %.preheader25.i5103
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768064(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+768072(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_897
	jmp	.LBB5_940
.LBB5_899:                              # %vector.body7694.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_900:                              # %vector.body7694
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_900
# BB#901:                               # %.preheader27.i5094.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_902:                              # %.preheader27.i5094
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256016(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256024(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_902
	jmp	.LBB5_940
.LBB5_904:                              # %.preheader27.i.preheader
	movl	$2, %eax
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_905:                              # %.preheader27.i
                                        # =>This Inner Loop Header: Depth=1
	leal	-1(%rax), %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-16(,%rax,8)
	movl	%eax, %ecx
	imull	%ecx, %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data-8(,%rax,8)
	addq	$2, %rax
	cmpq	$32002, %rax            # imm = 0x7D02
	jne	.LBB5_905
# BB#906:                               # %vector.body7668.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_907:                              # %vector.body7668
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_907
# BB#908:                               # %vector.body7681.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_4(%rip), %xmm0   # xmm0 = [-1.000000e+00,-1.000000e+00]
.LBB5_909:                              # %vector.body7681
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_909
	jmp	.LBB5_940
.LBB5_911:                              # %vector.body7629.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_1(%rip), %xmm0   # xmm0 = [1.000000e+00,1.000000e+00]
.LBB5_912:                              # %vector.body7629
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_912
# BB#913:                               # %vector.body7642.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [2.000000e+00,2.000000e+00]
.LBB5_914:                              # %vector.body7642
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_914
# BB#915:                               # %vector.body7655.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movapd	.LCPI5_3(%rip), %xmm0   # xmm0 = [5.000000e-01,5.000000e-01]
.LBB5_916:                              # %vector.body7655
                                        # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, global_data+768064(%rax)
	movapd	%xmm0, global_data+768080(%rax)
	movapd	%xmm0, global_data+768096(%rax)
	movapd	%xmm0, global_data+768112(%rax)
	movapd	%xmm0, global_data+768128(%rax)
	movapd	%xmm0, global_data+768144(%rax)
	movapd	%xmm0, global_data+768160(%rax)
	movapd	%xmm0, global_data+768176(%rax)
	movapd	%xmm0, global_data+768192(%rax)
	movapd	%xmm0, global_data+768208(%rax)
	movapd	%xmm0, global_data+768224(%rax)
	movapd	%xmm0, global_data+768240(%rax)
	movapd	%xmm0, global_data+768256(%rax)
	movapd	%xmm0, global_data+768272(%rax)
	movapd	%xmm0, global_data+768288(%rax)
	movapd	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB5_916
	jmp	.LBB5_940
.LBB5_918:                              # %.preheader25.i5068.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_919:                              # %.preheader25.i5068
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_919
	jmp	.LBB5_940
.LBB5_921:                              # %.preheader25.i5063.preheader
	movq	$-32000, %rax           # imm = 0x8300
	movsd	.LCPI5_0(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB5_922:                              # %.preheader25.i5063
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256008(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_922
# BB#923:                               # %.preheader25.i5058.preheader
	movq	$-32000, %rax           # imm = 0x8300
.LBB5_924:                              # %.preheader25.i5058
                                        # =>This Inner Loop Header: Depth=1
	leal	32001(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512032(,%rax,8)
	leal	32002(%rax), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	movapd	%xmm0, %xmm2
	divsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+512040(,%rax,8)
	addq	$2, %rax
	jne	.LBB5_924
	jmp	.LBB5_940
.Lfunc_end5:
	.size	init, .Lfunc_end5-init
	.cfi_endproc

	.globl	s221
	.p2align	4, 0x90
	.type	s221,@function
s221:                                   # @s221
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 16
.Lcfi15:
	.cfi_offset %rbx, -16
	movl	$.L.str.33, %edi
	callq	init
	cmpl	$2, ntimes(%rip)
	jl	.LBB6_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
	movq	$-255984, %rax          # imm = 0xFFFC1810
	movsd	global_data+256032(%rip), %xmm0 # xmm0 = mem[0],zero
	jmp	.LBB6_3
	.p2align	4, 0x90
.LBB6_12:                               #   in Loop: Header=BB6_3 Depth=2
	movsd	global_data+1024096(%rax), %xmm2 # xmm2 = mem[0],zero
	movsd	global_data+768064(%rax), %xmm0 # xmm0 = mem[0],zero
	mulsd	%xmm2, %xmm0
	addsd	global_data+256000(%rax), %xmm0
	movsd	%xmm0, global_data+256000(%rax)
	addsd	%xmm1, %xmm0
	addsd	%xmm2, %xmm0
	movsd	%xmm0, global_data+512032(%rax)
	addq	$16, %rax
.LBB6_3:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	global_data+1024088(%rax), %xmm2 # xmm2 = mem[0],zero
	movsd	global_data+768056(%rax), %xmm1 # xmm1 = mem[0],zero
	mulsd	%xmm2, %xmm1
	addsd	global_data+255992(%rax), %xmm1
	movsd	%xmm1, global_data+255992(%rax)
	addsd	%xmm0, %xmm1
	addsd	%xmm2, %xmm1
	movsd	%xmm1, global_data+512024(%rax)
	testq	%rax, %rax
	jne	.LBB6_12
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	xorpd	%xmm0, %xmm0
	pushq	$global_data+2328992
.Lcfi16:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi17:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi18:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	cmpl	%ecx, %ebx
	jl	.LBB6_2
.LBB6_5:                                # %._crit_edge
	movl	$.L.str.137, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm1, %xmm1
	movq	$-256000, %rax          # imm = 0xFFFC1800
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB6_6:                                # =>This Inner Loop Header: Depth=1
	addsd	global_data+256000(%rax), %xmm0
	addsd	global_data+512032(%rax), %xmm1
	addsd	global_data+256008(%rax), %xmm0
	addsd	global_data+512040(%rax), %xmm1
	addsd	global_data+256016(%rax), %xmm0
	addsd	global_data+512048(%rax), %xmm1
	addsd	global_data+256024(%rax), %xmm0
	addsd	global_data+512056(%rax), %xmm1
	addsd	global_data+256032(%rax), %xmm0
	addsd	global_data+512064(%rax), %xmm1
	addq	$40, %rax
	jne	.LBB6_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB6_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB6_9:                                #   Parent Loop BB6_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB6_9
# BB#10:                                #   in Loop: Header=BB6_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB6_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end6:
	.size	s221, .Lfunc_end6-s221
	.cfi_endproc

	.globl	s1221
	.p2align	4, 0x90
	.type	s1221,@function
s1221:                                  # @s1221
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 16
.Lcfi20:
	.cfi_offset %rbx, -16
	movl	$.L.str.33, %edi
	callq	init
	cmpl	$0, ntimes(%rip)
	jle	.LBB7_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB7_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_3 Depth 2
	movq	$-255968, %rax          # imm = 0xFFFC1820
	.p2align	4, 0x90
.LBB7_3:                                # %vector.body
                                        #   Parent Loop BB7_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movapd	global_data+512000(%rax), %xmm0
	addpd	global_data+256000(%rax), %xmm0
	movapd	%xmm0, global_data+512032(%rax)
	movapd	global_data+512016(%rax), %xmm0
	addpd	global_data+256016(%rax), %xmm0
	movapd	%xmm0, global_data+512048(%rax)
	addq	$32, %rax
	jne	.LBB7_3
# BB#4:                                 # %middle.block
                                        #   in Loop: Header=BB7_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	xorpd	%xmm0, %xmm0
	pushq	$global_data+2328992
.Lcfi21:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi22:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi23:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	cmpl	ntimes(%rip), %ebx
	jl	.LBB7_2
.LBB7_5:                                # %._crit_edge
	movl	$.L.str.138, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm1, %xmm1
	movq	$-256000, %rax          # imm = 0xFFFC1800
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB7_6:                                # =>This Inner Loop Header: Depth=1
	addsd	global_data+256000(%rax), %xmm0
	addsd	global_data+512032(%rax), %xmm1
	addsd	global_data+256008(%rax), %xmm0
	addsd	global_data+512040(%rax), %xmm1
	addsd	global_data+256016(%rax), %xmm0
	addsd	global_data+512048(%rax), %xmm1
	addsd	global_data+256024(%rax), %xmm0
	addsd	global_data+512056(%rax), %xmm1
	addsd	global_data+256032(%rax), %xmm0
	addsd	global_data+512064(%rax), %xmm1
	addq	$40, %rax
	jne	.LBB7_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB7_9:                                #   Parent Loop BB7_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB7_9
# BB#10:                                #   in Loop: Header=BB7_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB7_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	s1221, .Lfunc_end7-s1221
	.cfi_endproc

	.globl	s222
	.p2align	4, 0x90
	.type	s222,@function
s222:                                   # @s222
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 16
.Lcfi25:
	.cfi_offset %rbx, -16
	movl	$.L.str.34, %edi
	callq	init
	cmpl	$2, ntimes(%rip)
	jl	.LBB8_5
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_3 Depth 2
	movq	$-255984, %rax          # imm = 0xFFFC1810
	movsd	global_data+1024160(%rip), %xmm0 # xmm0 = mem[0],zero
	jmp	.LBB8_3
	.p2align	4, 0x90
.LBB8_12:                               #   in Loop: Header=BB8_3 Depth=2
	movsd	global_data+512032(%rax), %xmm1 # xmm1 = mem[0],zero
	mulsd	global_data+768064(%rax), %xmm1
	movsd	global_data+256000(%rax), %xmm2 # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, global_data+1280160(%rax)
	subsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+256000(%rax)
	addq	$16, %rax
.LBB8_3:                                #   Parent Loop BB8_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	global_data+512024(%rax), %xmm1 # xmm1 = mem[0],zero
	mulsd	global_data+768056(%rax), %xmm1
	movsd	global_data+255992(%rax), %xmm2 # xmm2 = mem[0],zero
	addsd	%xmm1, %xmm2
	mulsd	%xmm0, %xmm0
	movsd	%xmm0, global_data+1280152(%rax)
	subsd	%xmm1, %xmm2
	movsd	%xmm2, global_data+255992(%rax)
	testq	%rax, %rax
	jne	.LBB8_12
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	xorpd	%xmm0, %xmm0
	pushq	$global_data+2328992
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi28:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	shrl	$31, %ecx
	addl	%eax, %ecx
	sarl	%ecx
	cmpl	%ecx, %ebx
	jl	.LBB8_2
.LBB8_5:                                # %._crit_edge
	movl	$.L.str.139, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm1, %xmm1
	movq	$-256000, %rax          # imm = 0xFFFC1800
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_6:                                # =>This Inner Loop Header: Depth=1
	addsd	global_data+256000(%rax), %xmm0
	addsd	global_data+512032(%rax), %xmm1
	addsd	global_data+256008(%rax), %xmm0
	addsd	global_data+512040(%rax), %xmm1
	addsd	global_data+256016(%rax), %xmm0
	addsd	global_data+512048(%rax), %xmm1
	addsd	global_data+256024(%rax), %xmm0
	addsd	global_data+512056(%rax), %xmm1
	addsd	global_data+256032(%rax), %xmm0
	addsd	global_data+512064(%rax), %xmm1
	addq	$40, %rax
	jne	.LBB8_6
# BB#7:                                 # %.preheader111.i.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB8_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_9 Depth 2
	movl	$256, %ecx              # imm = 0x100
	.p2align	4, 0x90
.LBB8_9:                                #   Parent Loop BB8_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$-16, %rcx
	jne	.LBB8_9
# BB#10:                                #   in Loop: Header=BB8_8 Depth=1
	incq	%rax
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB8_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	s222, .Lfunc_end8-s222
	.cfi_endproc

	.globl	s231
	.p2align	4, 0x90
	.type	s231,@function
s231:                                   # @s231
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	movl	$.L.str.35, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB9_7
# BB#1:                                 # %.preheader30.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                # %.preheader30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_3 Depth 2
                                        #       Child Loop BB9_4 Depth 3
	movl	$global_data+1810752, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_3:                                # %.preheader
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_4 Depth 3
	movsd	global_data+1280224(,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	movq	%rax, %rdx
	movl	$255, %esi
	.p2align	4, 0x90
.LBB9_4:                                #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addsd	-4096(%rdx), %xmm0
	movsd	%xmm0, -528480(%rdx)
	addsd	-2048(%rdx), %xmm0
	movsd	%xmm0, -526432(%rdx)
	addsd	(%rdx), %xmm0
	movsd	%xmm0, -524384(%rdx)
	addq	$6144, %rdx             # imm = 0x1800
	addq	$-3, %rsi
	jne	.LBB9_4
# BB#5:                                 #   in Loop: Header=BB9_3 Depth=2
	incq	%rcx
	addq	$8, %rax
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB9_3
# BB#6:                                 #   in Loop: Header=BB9_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	xorpd	%xmm0, %xmm0
	pushq	$global_data+2328992
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi32:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi33:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$100, %ecx, %eax
	cmpl	%eax, %ebx
	jl	.LBB9_2
.LBB9_7:                                # %._crit_edge
	movl	$.L.str.140, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$global_data+1280248, %ecx
	.p2align	4, 0x90
.LBB9_8:                                # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_9 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB9_9:                                #   Parent Loop BB9_8 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-24(%rsi), %xmm0
	addsd	-16(%rsi), %xmm0
	addsd	-8(%rsi), %xmm0
	addsd	(%rsi), %xmm0
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB9_9
# BB#10:                                #   in Loop: Header=BB9_8 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB9_8
# BB#11:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end9:
	.size	s231, .Lfunc_end9-s231
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI10_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	s232
	.p2align	4, 0x90
	.type	s232,@function
s232:                                   # @s232
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -24
.Lcfi38:
	.cfi_offset %r14, -16
	movl	$.L.str.36, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB10_5
# BB#1:                                 # %.preheader33.preheader
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB10_2:                               # %.preheader33
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_3 Depth 2
                                        #       Child Loop BB10_13 Depth 3
	movl	$global_data+1282272, %eax
	xorl	%ecx, %ecx
	movl	$1, %edx
	movl	$2, %esi
	.p2align	4, 0x90
.LBB10_3:                               # %.lr.ph
                                        #   Parent Loop BB10_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB10_13 Depth 3
	movq	%rdx, %rdi
	shlq	$11, %rdi
	movsd	global_data+1280224(%rdi), %xmm0 # xmm0 = mem[0],zero
	testb	$1, %cl
	jne	.LBB10_4
# BB#10:                                #   in Loop: Header=BB10_3 Depth=2
	mulsd	%xmm0, %xmm0
	addsd	global_data+1804616(%rdi), %xmm0
	movsd	%xmm0, global_data+1280232(%rdi)
	movl	$2, %ebx
	testq	%rcx, %rcx
	jne	.LBB10_12
	jmp	.LBB10_14
	.p2align	4, 0x90
.LBB10_4:                               #   in Loop: Header=BB10_3 Depth=2
	movl	$1, %ebx
	testq	%rcx, %rcx
	je	.LBB10_14
.LBB10_12:                              # %.lr.ph.new
                                        #   in Loop: Header=BB10_3 Depth=2
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	leaq	(%rax,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB10_13:                              #   Parent Loop BB10_2 Depth=1
                                        #     Parent Loop BB10_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	mulsd	%xmm0, %xmm0
	addsd	524384(%rbx), %xmm0
	movsd	%xmm0, (%rbx)
	mulsd	%xmm0, %xmm0
	addsd	524392(%rbx), %xmm0
	movsd	%xmm0, 8(%rbx)
	addq	$16, %rbx
	addq	$-2, %rdi
	jne	.LBB10_13
.LBB10_14:                              # %._crit_edge
                                        #   in Loop: Header=BB10_3 Depth=2
	incq	%rdx
	incq	%rsi
	incq	%rcx
	addq	$2048, %rax             # imm = 0x800
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB10_3
# BB#15:                                #   in Loop: Header=BB10_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	movsd	.LCPI10_0(%rip), %xmm0  # xmm0 = mem[0],zero
	pushq	$global_data+2328992
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi41:
	.cfi_adjust_cfa_offset -16
	incl	%r14d
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$100, %ecx, %eax
	cmpl	%eax, %r14d
	jl	.LBB10_2
.LBB10_5:                               # %._crit_edge39
	movl	$.L.str.141, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$global_data+1280248, %ecx
	.p2align	4, 0x90
.LBB10_6:                               # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB10_7 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB10_7:                               #   Parent Loop BB10_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-24(%rsi), %xmm0
	addsd	-16(%rsi), %xmm0
	addsd	-8(%rsi), %xmm0
	addsd	(%rsi), %xmm0
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB10_7
# BB#8:                                 #   in Loop: Header=BB10_6 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB10_6
# BB#9:                                 # %.preheader.i.preheader
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end10:
	.size	s232, .Lfunc_end10-s232
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI11_0:
	.quad	4607182418800017408     # double 1
	.text
	.globl	s1232
	.p2align	4, 0x90
	.type	s1232,@function
s1232:                                  # @s1232
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 16
.Lcfi43:
	.cfi_offset %rbx, -16
	movl	$.L.str.36, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB11_10
# BB#1:                                 # %.preheader31.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB11_2:                               # %.preheader31
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_3 Depth 2
                                        #       Child Loop BB11_7 Depth 3
	movl	$global_data+1280224, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB11_3:                               # %.lr.ph
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB11_7 Depth 3
	testb	$1, %cl
	movq	%rcx, %rdx
	je	.LBB11_5
# BB#4:                                 #   in Loop: Header=BB11_3 Depth=2
	movq	%rcx, %rdx
	shlq	$11, %rdx
	movsd	global_data+1804608(%rdx,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	addsd	global_data+2328992(%rdx,%rcx,8), %xmm0
	movsd	%xmm0, global_data+1280224(%rdx,%rcx,8)
	leaq	1(%rcx), %rdx
.LBB11_5:                               # %.prol.loopexit
                                        #   in Loop: Header=BB11_3 Depth=2
	cmpq	$255, %rcx
	je	.LBB11_8
# BB#6:                                 # %.lr.ph.new
                                        #   in Loop: Header=BB11_3 Depth=2
	movl	$256, %esi              # imm = 0x100
	subq	%rdx, %rsi
	shlq	$11, %rdx
	addq	%rax, %rdx
	.p2align	4, 0x90
.LBB11_7:                               #   Parent Loop BB11_2 Depth=1
                                        #     Parent Loop BB11_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	524384(%rdx), %xmm0     # xmm0 = mem[0],zero
	addsd	1048768(%rdx), %xmm0
	movsd	%xmm0, (%rdx)
	movsd	526432(%rdx), %xmm0     # xmm0 = mem[0],zero
	addsd	1050816(%rdx), %xmm0
	movsd	%xmm0, 2048(%rdx)
	addq	$4096, %rdx             # imm = 0x1000
	addq	$-2, %rsi
	jne	.LBB11_7
.LBB11_8:                               # %._crit_edge
                                        #   in Loop: Header=BB11_3 Depth=2
	incq	%rcx
	addq	$8, %rax
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB11_3
# BB#9:                                 #   in Loop: Header=BB11_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	movsd	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero
	pushq	$global_data+2328992
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi46:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$100, %ecx, %eax
	cmpl	%eax, %ebx
	jl	.LBB11_2
.LBB11_10:                              # %._crit_edge37
	movl	$.L.str.142, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm0, %xmm0
	xorl	%eax, %eax
	movl	$global_data+1280248, %ecx
	.p2align	4, 0x90
.LBB11_11:                              # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_12 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB11_12:                              #   Parent Loop BB11_11 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-24(%rsi), %xmm0
	addsd	-16(%rsi), %xmm0
	addsd	-8(%rsi), %xmm0
	addsd	(%rsi), %xmm0
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB11_12
# BB#13:                                #   in Loop: Header=BB11_11 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB11_11
# BB#14:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end11:
	.size	s1232, .Lfunc_end11-s1232
	.cfi_endproc

	.globl	s233
	.p2align	4, 0x90
	.type	s233,@function
s233:                                   # @s233
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
.Lcfi48:
	.cfi_offset %rbx, -16
	movl	$.L.str.37, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB12_9
# BB#1:                                 # %.preheader42.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB12_2:                               # %.preheader42
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_3 Depth 2
                                        #       Child Loop BB12_4 Depth 3
                                        #       Child Loop BB12_6 Depth 3
	movl	$global_data+2335144, %eax
	movl	$1, %ecx
	.p2align	4, 0x90
.LBB12_3:                               # %.preheader41
                                        #   Parent Loop BB12_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_4 Depth 3
                                        #       Child Loop BB12_6 Depth 3
	movsd	global_data+1280224(,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	movq	%rax, %rdx
	movl	$255, %esi
	.p2align	4, 0x90
.LBB12_4:                               #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addsd	-4096(%rdx), %xmm0
	movsd	%xmm0, -1052864(%rdx)
	addsd	-2048(%rdx), %xmm0
	movsd	%xmm0, -1050816(%rdx)
	addsd	(%rdx), %xmm0
	movsd	%xmm0, -1048768(%rdx)
	addq	$6144, %rdx             # imm = 0x1800
	addq	$-3, %rsi
	jne	.LBB12_4
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB12_3 Depth=2
	movq	$-522240, %rdx          # imm = 0xFFF80800
	.p2align	4, 0x90
.LBB12_6:                               #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-6248(%rax,%rdx), %xmm0 # xmm0 = mem[0],zero
	addsd	518144(%rax,%rdx), %xmm0
	movsd	%xmm0, -6240(%rax,%rdx)
	movsd	-4200(%rax,%rdx), %xmm0 # xmm0 = mem[0],zero
	addsd	520192(%rax,%rdx), %xmm0
	movsd	%xmm0, -4192(%rax,%rdx)
	movsd	-2152(%rax,%rdx), %xmm0 # xmm0 = mem[0],zero
	addsd	522240(%rax,%rdx), %xmm0
	movsd	%xmm0, -2144(%rax,%rdx)
	addq	$6144, %rdx             # imm = 0x1800
	jne	.LBB12_6
# BB#7:                                 #   in Loop: Header=BB12_3 Depth=2
	incq	%rcx
	addq	$8, %rax
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB12_3
# BB#8:                                 #   in Loop: Header=BB12_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	xorpd	%xmm0, %xmm0
	pushq	$global_data+2328992
.Lcfi49:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi50:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi51:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$100, %ecx, %eax
	cmpl	%eax, %ebx
	jl	.LBB12_2
.LBB12_9:                               # %._crit_edge
	movl	$.L.str.143, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	movl	$global_data+1804616, %ecx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB12_10:                              # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_11 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB12_11:                              #   Parent Loop BB12_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-524392(%rsi), %xmm0
	addsd	-8(%rsi), %xmm1
	addsd	-524384(%rsi), %xmm0
	addsd	(%rsi), %xmm1
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB12_11
# BB#12:                                #   in Loop: Header=BB12_10 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB12_10
# BB#13:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	s233, .Lfunc_end12-s233
	.cfi_endproc

	.globl	s2233
	.p2align	4, 0x90
	.type	s2233,@function
s2233:                                  # @s2233
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi54:
	.cfi_def_cfa_offset 32
.Lcfi55:
	.cfi_offset %rbx, -24
.Lcfi56:
	.cfi_offset %rbp, -16
	movl	$.L.str.37, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB13_9
# BB#1:                                 # %.preheader42.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB13_2:                               # %.preheader42
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_3 Depth 2
                                        #       Child Loop BB13_4 Depth 3
                                        #       Child Loop BB13_6 Depth 3
	movl	$global_data+1804616, %ebx
	movl	$global_data+2335144, %ecx
	movl	$1, %edx
	.p2align	4, 0x90
.LBB13_3:                               # %.preheader41
                                        #   Parent Loop BB13_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB13_4 Depth 3
                                        #       Child Loop BB13_6 Depth 3
	movsd	global_data+1280224(,%rdx,8), %xmm0 # xmm0 = mem[0],zero
	movq	%rcx, %rsi
	movl	$255, %eax
	.p2align	4, 0x90
.LBB13_4:                               #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	addsd	-4096(%rsi), %xmm0
	movsd	%xmm0, -1052864(%rsi)
	addsd	-2048(%rsi), %xmm0
	movsd	%xmm0, -1050816(%rsi)
	addsd	(%rsi), %xmm0
	movsd	%xmm0, -1048768(%rsi)
	addq	$6144, %rsi             # imm = 0x1800
	addq	$-3, %rax
	jne	.LBB13_4
# BB#5:                                 # %.preheader
                                        #   in Loop: Header=BB13_3 Depth=2
	leaq	-1(%rdx), %rsi
	movl	$252, %edi
	movq	%rbx, %rax
	jmp	.LBB13_6
	.p2align	4, 0x90
.LBB13_14:                              # %vector.body.1
                                        #   in Loop: Header=BB13_6 Depth=3
	movupd	16(%rax), %xmm0
	movupd	526448(%rax), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, 2064(%rax)
	addq	$32, %rax
	addq	$-4, %rdi
.LBB13_6:                               # %vector.body
                                        #   Parent Loop BB13_2 Depth=1
                                        #     Parent Loop BB13_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movupd	(%rax), %xmm0
	movupd	526432(%rax), %xmm1
	addpd	%xmm0, %xmm1
	movupd	%xmm1, 2048(%rax)
	testq	%rdi, %rdi
	jne	.LBB13_14
# BB#7:                                 # %scalar.ph
                                        #   in Loop: Header=BB13_3 Depth=2
	shlq	$11, %rsi
	movsd	global_data+1806648(%rsi), %xmm0 # xmm0 = mem[0],zero
	movq	%rdx, %rax
	shlq	$11, %rax
	addsd	global_data+2331032(%rax), %xmm0
	movsd	%xmm0, global_data+1806648(%rax)
	incq	%rdx
	addq	$8, %rcx
	addq	$2048, %rbx             # imm = 0x800
	cmpq	$256, %rdx              # imm = 0x100
	jne	.LBB13_3
# BB#8:                                 #   in Loop: Header=BB13_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	xorpd	%xmm0, %xmm0
	pushq	$global_data+2328992
.Lcfi57:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi58:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi59:
	.cfi_adjust_cfa_offset -16
	incl	%ebp
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$100, %ecx, %eax
	cmpl	%eax, %ebp
	jl	.LBB13_2
.LBB13_9:                               # %._crit_edge
	movl	$.L.str.144, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	movl	$global_data+1804616, %ecx
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB13_10:                              # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB13_11 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB13_11:                              #   Parent Loop BB13_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-524392(%rsi), %xmm0
	addsd	-8(%rsi), %xmm1
	addsd	-524384(%rsi), %xmm0
	addsd	(%rsi), %xmm1
	addq	$16, %rsi
	addq	$-2, %rdx
	jne	.LBB13_11
# BB#12:                                #   in Loop: Header=BB13_10 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB13_10
# BB#13:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end13:
	.size	s2233, .Lfunc_end13-s2233
	.cfi_endproc

	.globl	s235
	.p2align	4, 0x90
	.type	s235,@function
s235:                                   # @s235
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 16
.Lcfi61:
	.cfi_offset %rbx, -16
	movl	$.L.str.39, %edi
	callq	init
	cmpl	$256, ntimes(%rip)      # imm = 0x100
	jl	.LBB14_7
# BB#1:                                 # %.preheader.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB14_2:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_3 Depth 2
                                        #       Child Loop BB14_4 Depth 3
	movl	$global_data+1810752, %eax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB14_3:                               #   Parent Loop BB14_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB14_4 Depth 3
	movsd	global_data+256032(,%rcx,8), %xmm0 # xmm0 = mem[0],zero
	mulsd	global_data+512064(,%rcx,8), %xmm0
	addsd	global_data(,%rcx,8), %xmm0
	movsd	%xmm0, global_data(,%rcx,8)
	movsd	global_data+1280224(,%rcx,8), %xmm1 # xmm1 = mem[0],zero
	movq	%rax, %rdx
	movl	$255, %esi
	.p2align	4, 0x90
.LBB14_4:                               #   Parent Loop BB14_2 Depth=1
                                        #     Parent Loop BB14_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	-4096(%rdx), %xmm2      # xmm2 = mem[0],zero
	mulsd	%xmm0, %xmm2
	addsd	%xmm1, %xmm2
	movsd	%xmm2, -528480(%rdx)
	movsd	-2048(%rdx), %xmm3      # xmm3 = mem[0],zero
	mulsd	%xmm0, %xmm3
	addsd	%xmm2, %xmm3
	movsd	%xmm3, -526432(%rdx)
	movsd	(%rdx), %xmm1           # xmm1 = mem[0],zero
	mulsd	%xmm0, %xmm1
	addsd	%xmm3, %xmm1
	movsd	%xmm1, -524384(%rdx)
	addq	$6144, %rdx             # imm = 0x1800
	addq	$-3, %rsi
	jne	.LBB14_4
# BB#5:                                 #   in Loop: Header=BB14_3 Depth=2
	incq	%rcx
	addq	$8, %rax
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB14_3
# BB#6:                                 #   in Loop: Header=BB14_2 Depth=1
	movl	$global_data, %edi
	movl	$global_data+256032, %esi
	movl	$global_data+512064, %edx
	movl	$global_data+768096, %ecx
	movl	$global_data+1024160, %r8d
	movl	$global_data+1280224, %r9d
	xorpd	%xmm0, %xmm0
	pushq	$global_data+2328992
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	$global_data+1804608
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	callq	dummy
	addq	$16, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset -16
	incl	%ebx
	movl	ntimes(%rip), %eax
	movl	%eax, %ecx
	sarl	$31, %ecx
	shrl	$24, %ecx
	addl	%eax, %ecx
	sarl	$8, %ecx
	imull	$200, %ecx, %eax
	cmpl	%eax, %ebx
	jl	.LBB14_2
.LBB14_7:                               # %._crit_edge
	movl	$.L.str.145, %edi
	xorpd	%xmm0, %xmm0
	movb	$1, %al
	callq	printf
	xorpd	%xmm0, %xmm0
	movq	$-256000, %rax          # imm = 0xFFFC1800
	.p2align	4, 0x90
.LBB14_8:                               # =>This Inner Loop Header: Depth=1
	addsd	global_data+256000(%rax), %xmm0
	addsd	global_data+256008(%rax), %xmm0
	addsd	global_data+256016(%rax), %xmm0
	addsd	global_data+256024(%rax), %xmm0
	addsd	global_data+256032(%rax), %xmm0
	addsd	global_data+256040(%rax), %xmm0
	addsd	global_data+256048(%rax), %xmm0
	addsd	global_data+256056(%rax), %xmm0
	addq	$64, %rax
	jne	.LBB14_8
# BB#9:                                 # %.preheader111.i.preheader
	xorpd	%xmm1, %xmm1
	xorl	%eax, %eax
	movl	$global_data+1280248, %ecx
	.p2align	4, 0x90
.LBB14_10:                              # %.preheader111.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB14_11 Depth 2
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB14_11:                              #   Parent Loop BB14_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addsd	-24(%rsi), %xmm1
	addsd	-16(%rsi), %xmm1
	addsd	-8(%rsi), %xmm1
	addsd	(%rsi), %xmm1
	addq	$32, %rsi
	addq	$-4, %rdx
	jne	.LBB14_11
# BB#12:                                #   in Loop: Header=BB14_10 Depth=1
	incq	%rax
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB14_10
# BB#13:                                # %.preheader.i.preheader
	movl	digits(%rip), %esi
	addsd	%xmm1, %xmm0
	movl	$.L.str, %edi
	movb	$1, %al
	callq	printf
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end14:
	.size	s235, .Lfunc_end14-s235
	.cfi_endproc

	.globl	min
	.p2align	4, 0x90
	.type	min,@function
min:                                    # @min
	.cfi_startproc
# BB#0:
	cmpl	%esi, %edi
	cmovlel	%edi, %esi
	movl	%esi, %eax
	retq
.Lfunc_end15:
	.size	min, .Lfunc_end15-min
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.quad	4607182418800017408     # double 1
	.quad	4607182418800017408     # double 1
.LCPI16_2:
	.quad	2                       # 0x2
	.quad	3                       # 0x3
.LCPI16_3:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
.LCPI16_4:
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
	.long	5                       # 0x5
.LCPI16_5:
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
	.long	4294967292              # 0xfffffffc
.LCPI16_6:
	.quad	8                       # 0x8
	.quad	8                       # 0x8
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI16_1:
	.quad	4607182418800017408     # double 1
	.text
	.globl	set
	.p2align	4, 0x90
	.type	set,@function
set:                                    # @set
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 32
.Lcfi68:
	.cfi_offset %rbx, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movl	$xx, %edi
	movl	$32, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	posix_memalign
	movl	$10, %edi
	callq	putchar
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	leal	4(%rax), %ecx
	movl	%ecx, (%rbx,%rax,4)
	leal	2(%rax), %ecx
	movl	%ecx, 4(%rbx,%rax,4)
	movl	%eax, 8(%rbx,%rax,4)
	leal	3(%rax), %ecx
	movl	%ecx, 12(%rbx,%rax,4)
	leal	1(%rax), %ecx
	movl	%ecx, 16(%rbx,%rax,4)
	leal	9(%rax), %ecx
	movl	%ecx, 20(%rbx,%rax,4)
	leal	7(%rax), %ecx
	movl	%ecx, 24(%rbx,%rax,4)
	leal	5(%rax), %ecx
	movl	%ecx, 28(%rbx,%rax,4)
	leal	8(%rax), %ecx
	movl	%ecx, 32(%rbx,%rax,4)
	leal	6(%rax), %ecx
	movl	%ecx, 36(%rbx,%rax,4)
	addq	$10, %rax
	cmpq	$32000, %rax            # imm = 0x7D00
	jl	.LBB16_1
# BB#2:                                 # %vector.body.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	movaps	.LCPI16_0(%rip), %xmm0  # xmm0 = [1.000000e+00,1.000000e+00]
	.p2align	4, 0x90
.LBB16_3:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+256000(%rax)
	movaps	%xmm0, global_data+256016(%rax)
	movaps	%xmm0, global_data+256032(%rax)
	movaps	%xmm0, global_data+256048(%rax)
	movaps	%xmm0, global_data+256064(%rax)
	movaps	%xmm0, global_data+256080(%rax)
	movaps	%xmm0, global_data+256096(%rax)
	movaps	%xmm0, global_data+256112(%rax)
	movaps	%xmm0, global_data+256128(%rax)
	movaps	%xmm0, global_data+256144(%rax)
	movaps	%xmm0, global_data+256160(%rax)
	movaps	%xmm0, global_data+256176(%rax)
	movaps	%xmm0, global_data+256192(%rax)
	movaps	%xmm0, global_data+256208(%rax)
	movaps	%xmm0, global_data+256224(%rax)
	movaps	%xmm0, global_data+256240(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB16_3
# BB#4:                                 # %vector.body70.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	.p2align	4, 0x90
.LBB16_5:                               # %vector.body70
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+512032(%rax)
	movaps	%xmm0, global_data+512048(%rax)
	movaps	%xmm0, global_data+512064(%rax)
	movaps	%xmm0, global_data+512080(%rax)
	movaps	%xmm0, global_data+512096(%rax)
	movaps	%xmm0, global_data+512112(%rax)
	movaps	%xmm0, global_data+512128(%rax)
	movaps	%xmm0, global_data+512144(%rax)
	movaps	%xmm0, global_data+512160(%rax)
	movaps	%xmm0, global_data+512176(%rax)
	movaps	%xmm0, global_data+512192(%rax)
	movaps	%xmm0, global_data+512208(%rax)
	movaps	%xmm0, global_data+512224(%rax)
	movaps	%xmm0, global_data+512240(%rax)
	movaps	%xmm0, global_data+512256(%rax)
	movaps	%xmm0, global_data+512272(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB16_5
# BB#6:                                 # %vector.body83.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	.p2align	4, 0x90
.LBB16_7:                               # %vector.body83
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+768064(%rax)
	movaps	%xmm0, global_data+768080(%rax)
	movaps	%xmm0, global_data+768096(%rax)
	movaps	%xmm0, global_data+768112(%rax)
	movaps	%xmm0, global_data+768128(%rax)
	movaps	%xmm0, global_data+768144(%rax)
	movaps	%xmm0, global_data+768160(%rax)
	movaps	%xmm0, global_data+768176(%rax)
	movaps	%xmm0, global_data+768192(%rax)
	movaps	%xmm0, global_data+768208(%rax)
	movaps	%xmm0, global_data+768224(%rax)
	movaps	%xmm0, global_data+768240(%rax)
	movaps	%xmm0, global_data+768256(%rax)
	movaps	%xmm0, global_data+768272(%rax)
	movaps	%xmm0, global_data+768288(%rax)
	movaps	%xmm0, global_data+768304(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB16_7
# BB#8:                                 # %vector.body96.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	.p2align	4, 0x90
.LBB16_9:                               # %vector.body96
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1024096(%rax)
	movaps	%xmm0, global_data+1024112(%rax)
	movaps	%xmm0, global_data+1024128(%rax)
	movaps	%xmm0, global_data+1024144(%rax)
	movaps	%xmm0, global_data+1024160(%rax)
	movaps	%xmm0, global_data+1024176(%rax)
	movaps	%xmm0, global_data+1024192(%rax)
	movaps	%xmm0, global_data+1024208(%rax)
	movaps	%xmm0, global_data+1024224(%rax)
	movaps	%xmm0, global_data+1024240(%rax)
	movaps	%xmm0, global_data+1024256(%rax)
	movaps	%xmm0, global_data+1024272(%rax)
	movaps	%xmm0, global_data+1024288(%rax)
	movaps	%xmm0, global_data+1024304(%rax)
	movaps	%xmm0, global_data+1024320(%rax)
	movaps	%xmm0, global_data+1024336(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB16_9
# BB#10:                                # %vector.body109.preheader
	movq	$-256000, %rax          # imm = 0xFFFC1800
	.p2align	4, 0x90
.LBB16_11:                              # %vector.body109
                                        # =>This Inner Loop Header: Depth=1
	movaps	%xmm0, global_data+1280160(%rax)
	movaps	%xmm0, global_data+1280176(%rax)
	movaps	%xmm0, global_data+1280192(%rax)
	movaps	%xmm0, global_data+1280208(%rax)
	movaps	%xmm0, global_data+1280224(%rax)
	movaps	%xmm0, global_data+1280240(%rax)
	movaps	%xmm0, global_data+1280256(%rax)
	movaps	%xmm0, global_data+1280272(%rax)
	movaps	%xmm0, global_data+1280288(%rax)
	movaps	%xmm0, global_data+1280304(%rax)
	movaps	%xmm0, global_data+1280320(%rax)
	movaps	%xmm0, global_data+1280336(%rax)
	movaps	%xmm0, global_data+1280352(%rax)
	movaps	%xmm0, global_data+1280368(%rax)
	movaps	%xmm0, global_data+1280384(%rax)
	movaps	%xmm0, global_data+1280400(%rax)
	addq	$256, %rax              # imm = 0x100
	jne	.LBB16_11
# BB#12:                                # %.preheader44.i39.preheader
	xorl	%eax, %eax
	movl	$global_data+1280336, %ecx
	movsd	.LCPI16_1(%rip), %xmm0  # xmm0 = mem[0],zero
	.p2align	4, 0x90
.LBB16_13:                              # %.preheader44.i39
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_14 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB16_14:                              # %vector.body122
                                        #   Parent Loop BB16_13 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB16_14
# BB#15:                                # %middle.block123
                                        #   in Loop: Header=BB16_13 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB16_13
# BB#16:                                # %.preheader44.i30.preheader
	xorl	%eax, %eax
	movl	$global_data+1804720, %ecx
	.p2align	4, 0x90
.LBB16_17:                              # %.preheader44.i30
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_18 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB16_18:                              # %vector.body137
                                        #   Parent Loop BB16_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB16_18
# BB#19:                                # %middle.block138
                                        #   in Loop: Header=BB16_17 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB16_17
# BB#20:                                # %.preheader44.i.preheader
	xorl	%eax, %eax
	movl	$global_data+2329104, %ecx
	.p2align	4, 0x90
.LBB16_21:                              # %.preheader44.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB16_22 Depth 2
	incq	%rax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	movapd	%xmm0, %xmm1
	divsd	%xmm2, %xmm1
	movlhps	%xmm1, %xmm1            # xmm1 = xmm1[0,0]
	movl	$256, %edx              # imm = 0x100
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB16_22:                              # %vector.body152
                                        #   Parent Loop BB16_21 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movaps	%xmm1, -112(%rsi)
	movaps	%xmm1, -96(%rsi)
	movaps	%xmm1, -80(%rsi)
	movaps	%xmm1, -64(%rsi)
	movaps	%xmm1, -48(%rsi)
	movaps	%xmm1, -32(%rsi)
	movaps	%xmm1, -16(%rsi)
	movaps	%xmm1, (%rsi)
	subq	$-128, %rsi
	addq	$-16, %rdx
	jne	.LBB16_22
# BB#23:                                # %middle.block153
                                        #   in Loop: Header=BB16_21 Depth=1
	addq	$2048, %rcx             # imm = 0x800
	cmpq	$256, %rax              # imm = 0x100
	jne	.LBB16_21
# BB#24:                                # %vector.body167.preheader
	movl	$1, %eax
	movd	%rax, %xmm0
	pslldq	$8, %xmm0               # xmm0 = zero,zero,zero,zero,zero,zero,zero,zero,xmm0[0,1,2,3,4,5,6,7]
	movaps	.LCPI16_2(%rip), %xmm1  # xmm1 = [2,3]
	movq	$-128000, %rax          # imm = 0xFFFE0C00
	movdqa	.LCPI16_3(%rip), %xmm2  # xmm2 = [1,1,1,1]
	movdqa	.LCPI16_4(%rip), %xmm8  # xmm8 = [5,5,5,5]
	movdqa	.LCPI16_5(%rip), %xmm4  # xmm4 = [4294967292,4294967292,4294967292,4294967292]
	movdqa	.LCPI16_6(%rip), %xmm5  # xmm5 = [8,8]
	.p2align	4, 0x90
.LBB16_25:                              # %vector.body167
                                        # =>This Inner Loop Header: Depth=1
	movdqa	%xmm0, %xmm6
	shufps	$136, %xmm1, %xmm6      # xmm6 = xmm6[0,2],xmm1[0,2]
	movaps	%xmm6, %xmm3
	paddd	%xmm2, %xmm3
	paddd	%xmm8, %xmm6
	movdqa	%xmm3, %xmm7
	psrad	$31, %xmm7
	psrld	$30, %xmm7
	paddd	%xmm3, %xmm7
	pand	%xmm4, %xmm7
	psubd	%xmm7, %xmm3
	movdqa	%xmm6, %xmm7
	psrad	$31, %xmm7
	psrld	$30, %xmm7
	paddd	%xmm6, %xmm7
	pand	%xmm4, %xmm7
	psubd	%xmm7, %xmm6
	paddd	%xmm2, %xmm3
	paddd	%xmm2, %xmm6
	movdqa	%xmm3, indx+128000(%rax)
	movdqa	%xmm6, indx+128016(%rax)
	paddq	%xmm5, %xmm0
	paddq	%xmm5, %xmm1
	addq	$32, %rax
	jne	.LBB16_25
# BB#26:                                # %middle.block168
	movabsq	$4607182418800017408, %rax # imm = 0x3FF0000000000000
	movq	%rax, (%r15)
	movabsq	$4611686018427387904, %rax # imm = 0x4000000000000000
	movq	%rax, (%r14)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	set, .Lfunc_end16-set
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	subq	$24, %rsp
.Lcfi73:
	.cfi_def_cfa_offset 48
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movl	%edi, %ebp
	movq	%rsp, %rdi
	movl	$32, %esi
	movl	$256000, %edx           # imm = 0x3E800
	callq	posix_memalign
	cmpl	$1, %ebp
	jle	.LBB17_1
# BB#2:
	movq	8(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rcx
	movl	%ecx, ntimes(%rip)
	movl	$.L.str.147, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	cmpl	$2, %ebp
	je	.LBB17_4
# BB#3:
	movq	16(%rbx), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movl	%eax, digits(%rip)
	jmp	.LBB17_4
.LBB17_1:                               # %.thread
	movl	ntimes(%rip), %esi
	movl	$.L.str.147, %edi
	xorl	%eax, %eax
	callq	printf
.LBB17_4:
	movq	(%rsp), %rdi
	leaq	16(%rsp), %rsi
	leaq	8(%rsp), %rdx
	callq	set
	movl	$.Lstr, %edi
	callq	puts
	callq	s221
	callq	s1221
	callq	s222
	callq	s231
	callq	s232
	callq	s1232
	callq	s233
	callq	s2233
	callq	s235
	xorl	%eax, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end17:
	.size	main, .Lfunc_end17-main
	.cfi_endproc

	.type	global_data,@object     # @global_data
	.comm	global_data,3377664,32
	.type	a,@object               # @a
	.section	.rodata,"a",@progbits
	.globl	a
	.p2align	5
a:
	.quad	global_data
	.size	a, 8

	.type	b,@object               # @b
	.globl	b
	.p2align	5
b:
	.quad	global_data+256032
	.size	b, 8

	.type	c,@object               # @c
	.globl	c
	.p2align	5
c:
	.quad	global_data+512064
	.size	c, 8

	.type	d,@object               # @d
	.globl	d
	.p2align	5
d:
	.quad	global_data+768096
	.size	d, 8

	.type	e,@object               # @e
	.globl	e
	.p2align	5
e:
	.quad	global_data+1024160
	.size	e, 8

	.type	aa,@object              # @aa
	.globl	aa
	.p2align	5
aa:
	.quad	global_data+1280224
	.size	aa, 8

	.type	bb,@object              # @bb
	.globl	bb
	.p2align	5
bb:
	.quad	global_data+1804608
	.size	bb, 8

	.type	cc,@object              # @cc
	.globl	cc
	.p2align	5
cc:
	.quad	global_data+2328992
	.size	cc, 8

	.type	tt,@object              # @tt
	.globl	tt
	.p2align	5
tt:
	.quad	global_data+2853376
	.size	tt, 8

	.type	array,@object           # @array
	.comm	array,524288,32
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%.*g \n"
	.size	.L.str, 7

	.type	digits,@object          # @digits
	.data
	.p2align	2
digits:
	.long	6                       # 0x6
	.size	digits, 4

	.type	temp,@object            # @temp
	.comm	temp,8,8
	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"s000 "
	.size	.L.str.1, 6

	.type	X,@object               # @X
	.comm	X,256000,32
	.type	Y,@object               # @Y
	.comm	Y,256000,32
	.type	Z,@object               # @Z
	.comm	Z,256000,32
	.type	U,@object               # @U
	.comm	U,256000,32
	.type	V,@object               # @V
	.comm	V,256000,32
	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"s111 "
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"s112 "
	.size	.L.str.3, 6

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"s113 "
	.size	.L.str.4, 6

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"s114 "
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"s115 "
	.size	.L.str.6, 6

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"s116 "
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"s118 "
	.size	.L.str.8, 6

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"s119 "
	.size	.L.str.9, 6

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"s121 "
	.size	.L.str.10, 6

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"s122 "
	.size	.L.str.11, 6

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"s123 "
	.size	.L.str.12, 6

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"s124 "
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"s125 "
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"s126 "
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"s127 "
	.size	.L.str.16, 6

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"s128 "
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"s131 "
	.size	.L.str.18, 6

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"s132 "
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"s141 "
	.size	.L.str.20, 6

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"s151 "
	.size	.L.str.21, 6

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"s152 "
	.size	.L.str.22, 6

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"s161 "
	.size	.L.str.23, 6

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"s162 "
	.size	.L.str.24, 6

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"s171 "
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"s172 "
	.size	.L.str.26, 6

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"s173 "
	.size	.L.str.27, 6

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"s174 "
	.size	.L.str.28, 6

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"s175 "
	.size	.L.str.29, 6

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"s176 "
	.size	.L.str.30, 6

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"s211 "
	.size	.L.str.31, 6

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"s212 "
	.size	.L.str.32, 6

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"s221 "
	.size	.L.str.33, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"s222 "
	.size	.L.str.34, 6

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"s231 "
	.size	.L.str.35, 6

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"s232 "
	.size	.L.str.36, 6

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"s233 "
	.size	.L.str.37, 6

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"s234 "
	.size	.L.str.38, 6

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"s235 "
	.size	.L.str.39, 6

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"s241 "
	.size	.L.str.40, 6

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"s242 "
	.size	.L.str.41, 6

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"s243 "
	.size	.L.str.42, 6

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"s244 "
	.size	.L.str.43, 6

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"s251 "
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"s252 "
	.size	.L.str.45, 6

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"s253 "
	.size	.L.str.46, 6

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"s254 "
	.size	.L.str.47, 6

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"s255 "
	.size	.L.str.48, 6

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"s256 "
	.size	.L.str.49, 6

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"s257 "
	.size	.L.str.50, 6

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"s258 "
	.size	.L.str.51, 6

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"s261 "
	.size	.L.str.52, 6

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"s271 "
	.size	.L.str.53, 6

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"s272 "
	.size	.L.str.54, 6

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"s273 "
	.size	.L.str.55, 6

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"s274 "
	.size	.L.str.56, 6

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"s275 "
	.size	.L.str.57, 6

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"s276 "
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"s277 "
	.size	.L.str.59, 6

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"s278 "
	.size	.L.str.60, 6

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"s279 "
	.size	.L.str.61, 6

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"s2710"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"s2711"
	.size	.L.str.63, 6

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"s2712"
	.size	.L.str.64, 6

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"s281 "
	.size	.L.str.65, 6

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"s291 "
	.size	.L.str.66, 6

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"s292 "
	.size	.L.str.67, 6

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"s293 "
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"s2101"
	.size	.L.str.69, 6

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"s2102"
	.size	.L.str.70, 6

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"s2111"
	.size	.L.str.71, 6

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"s311 "
	.size	.L.str.72, 6

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"s312 "
	.size	.L.str.73, 6

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"s313 "
	.size	.L.str.74, 6

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"s314 "
	.size	.L.str.75, 6

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"s315 "
	.size	.L.str.76, 6

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"s316 "
	.size	.L.str.77, 6

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"s317 "
	.size	.L.str.78, 6

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"s318 "
	.size	.L.str.79, 6

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"s319 "
	.size	.L.str.80, 6

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"s3110"
	.size	.L.str.81, 6

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"s3111"
	.size	.L.str.82, 6

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"s3112"
	.size	.L.str.83, 6

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"s3113"
	.size	.L.str.84, 6

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"s321 "
	.size	.L.str.85, 6

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"s322 "
	.size	.L.str.86, 6

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"s323 "
	.size	.L.str.87, 6

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"s331 "
	.size	.L.str.88, 6

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"s332 "
	.size	.L.str.89, 6

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"s341 "
	.size	.L.str.90, 6

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"s342 "
	.size	.L.str.91, 6

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"s343 "
	.size	.L.str.92, 6

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"s351 "
	.size	.L.str.93, 6

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"s352 "
	.size	.L.str.94, 6

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"s353 "
	.size	.L.str.95, 6

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"s411 "
	.size	.L.str.96, 6

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"s412 "
	.size	.L.str.97, 6

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"s413 "
	.size	.L.str.98, 6

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"s414 "
	.size	.L.str.99, 6

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"s415 "
	.size	.L.str.100, 6

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"s421 "
	.size	.L.str.101, 6

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"s422 "
	.size	.L.str.102, 6

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"s423 "
	.size	.L.str.103, 6

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"s424 "
	.size	.L.str.104, 6

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	"s431 "
	.size	.L.str.105, 6

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	"s432 "
	.size	.L.str.106, 6

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"s441 "
	.size	.L.str.107, 6

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"s442 "
	.size	.L.str.108, 6

	.type	.L.str.109,@object      # @.str.109
.L.str.109:
	.asciz	"s443 "
	.size	.L.str.109, 6

	.type	.L.str.110,@object      # @.str.110
.L.str.110:
	.asciz	"s451 "
	.size	.L.str.110, 6

	.type	.L.str.111,@object      # @.str.111
.L.str.111:
	.asciz	"s452 "
	.size	.L.str.111, 6

	.type	.L.str.112,@object      # @.str.112
.L.str.112:
	.asciz	"s453 "
	.size	.L.str.112, 6

	.type	.L.str.113,@object      # @.str.113
.L.str.113:
	.asciz	"s471 "
	.size	.L.str.113, 6

	.type	.L.str.114,@object      # @.str.114
.L.str.114:
	.asciz	"s481 "
	.size	.L.str.114, 6

	.type	.L.str.115,@object      # @.str.115
.L.str.115:
	.asciz	"s482 "
	.size	.L.str.115, 6

	.type	.L.str.116,@object      # @.str.116
.L.str.116:
	.asciz	"s491 "
	.size	.L.str.116, 6

	.type	.L.str.117,@object      # @.str.117
.L.str.117:
	.asciz	"s4112"
	.size	.L.str.117, 6

	.type	.L.str.118,@object      # @.str.118
.L.str.118:
	.asciz	"s4113"
	.size	.L.str.118, 6

	.type	.L.str.119,@object      # @.str.119
.L.str.119:
	.asciz	"s4114"
	.size	.L.str.119, 6

	.type	.L.str.120,@object      # @.str.120
.L.str.120:
	.asciz	"s4115"
	.size	.L.str.120, 6

	.type	.L.str.121,@object      # @.str.121
.L.str.121:
	.asciz	"s4116"
	.size	.L.str.121, 6

	.type	.L.str.122,@object      # @.str.122
.L.str.122:
	.asciz	"s4117"
	.size	.L.str.122, 6

	.type	.L.str.123,@object      # @.str.123
.L.str.123:
	.asciz	"s4121"
	.size	.L.str.123, 6

	.type	.L.str.124,@object      # @.str.124
.L.str.124:
	.asciz	"va\t"
	.size	.L.str.124, 4

	.type	.L.str.125,@object      # @.str.125
.L.str.125:
	.asciz	"vag  "
	.size	.L.str.125, 6

	.type	.L.str.126,@object      # @.str.126
.L.str.126:
	.asciz	"vas  "
	.size	.L.str.126, 6

	.type	.L.str.127,@object      # @.str.127
.L.str.127:
	.asciz	"vif  "
	.size	.L.str.127, 6

	.type	.L.str.128,@object      # @.str.128
.L.str.128:
	.asciz	"vpv  "
	.size	.L.str.128, 6

	.type	.L.str.129,@object      # @.str.129
.L.str.129:
	.asciz	"vtv  "
	.size	.L.str.129, 6

	.type	.L.str.130,@object      # @.str.130
.L.str.130:
	.asciz	"vpvtv"
	.size	.L.str.130, 6

	.type	.L.str.131,@object      # @.str.131
.L.str.131:
	.asciz	"vpvts"
	.size	.L.str.131, 6

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"vpvpv"
	.size	.L.str.132, 6

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"vtvtv"
	.size	.L.str.133, 6

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"vsumr"
	.size	.L.str.134, 6

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"vdotr"
	.size	.L.str.135, 6

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"vbor "
	.size	.L.str.136, 6

	.type	ntimes,@object          # @ntimes
	.data
	.p2align	2
ntimes:
	.long	200000                  # 0x30d40
	.size	ntimes, 4

	.type	.L.str.137,@object      # @.str.137
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.137:
	.asciz	"S221\t %.2f \t\t"
	.size	.L.str.137, 14

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"S1221\t %.2f \t\t"
	.size	.L.str.138, 15

	.type	.L.str.139,@object      # @.str.139
.L.str.139:
	.asciz	"S222\t %.2f \t\t"
	.size	.L.str.139, 14

	.type	.L.str.140,@object      # @.str.140
.L.str.140:
	.asciz	"S231\t %.2f \t\t"
	.size	.L.str.140, 14

	.type	.L.str.141,@object      # @.str.141
.L.str.141:
	.asciz	"S232\t %.2f \t\t"
	.size	.L.str.141, 14

	.type	.L.str.142,@object      # @.str.142
.L.str.142:
	.asciz	"S1232\t %.2f \t\t"
	.size	.L.str.142, 15

	.type	.L.str.143,@object      # @.str.143
.L.str.143:
	.asciz	"S233\t %.2f \t\t"
	.size	.L.str.143, 14

	.type	.L.str.144,@object      # @.str.144
.L.str.144:
	.asciz	"S2233\t %.2f \t\t"
	.size	.L.str.144, 15

	.type	.L.str.145,@object      # @.str.145
.L.str.145:
	.asciz	"S235\t %.2f \t\t"
	.size	.L.str.145, 14

	.type	xx,@object              # @xx
	.comm	xx,8,8
	.type	indx,@object            # @indx
	.comm	indx,128000,32
	.type	.L.str.147,@object      # @.str.147
.L.str.147:
	.asciz	"Running each loop %d times...\n"
	.size	.L.str.147, 31

	.type	x,@object               # @x
	.comm	x,256000,32
	.type	temp_int,@object        # @temp_int
	.comm	temp_int,4,4
	.type	yy,@object              # @yy
	.comm	yy,8,8
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"Loop \t Time(Sec) \t Checksum "
	.size	.Lstr, 29


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
