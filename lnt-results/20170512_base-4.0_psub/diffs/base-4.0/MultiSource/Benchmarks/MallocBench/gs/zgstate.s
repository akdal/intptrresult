	.text
	.file	"zgstate.bc"
	.globl	gs_init
	.p2align	4, 0x90
	.type	gs_init,@function
gs_init:                                # @gs_init
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	$alloc, %edi
	movl	$alloc_free, %esi
	callq	gs_state_alloc
	movq	%rax, igs(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, istate(%rip)
	movw	$32, istate+16(%rip)
	movq	$0, istate+24(%rip)
	movw	$32, istate+32(%rip)
	popq	%rax
	retq
.Lfunc_end0:
	.size	gs_init, .Lfunc_end0-gs_init
	.cfi_endproc

	.globl	zgsave
	.p2align	4, 0x90
	.type	zgsave,@function
zgsave:                                 # @zgsave
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 16
.Lcfi2:
	.cfi_offset %rbx, -16
	movl	$1, %edi
	movl	$72, %esi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %rbx
	movq	igs(%rip), %rdi
	callq	gs_gsave
	movl	%eax, %ecx
	testq	%rbx, %rbx
	movl	$-13, %eax
	je	.LBB1_3
# BB#1:
	testl	%ecx, %ecx
	js	.LBB1_3
# BB#2:
	movq	istate+64(%rip), %rax
	movq	%rax, 64(%rbx)
	movups	istate+48(%rip), %xmm0
	movups	%xmm0, 48(%rbx)
	movups	istate+32(%rip), %xmm0
	movups	%xmm0, 32(%rbx)
	movups	istate+16(%rip), %xmm0
	movups	%xmm0, 16(%rbx)
	movups	istate(%rip), %xmm0
	movups	%xmm0, (%rbx)
	movq	%rbx, istate(%rip)
	xorl	%eax, %eax
.LBB1_3:
	popq	%rbx
	retq
.Lfunc_end1:
	.size	zgsave, .Lfunc_end1-zgsave
	.cfi_endproc

	.globl	zgrestore
	.p2align	4, 0x90
	.type	zgrestore,@function
zgrestore:                              # @zgrestore
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 16
	movq	igs(%rip), %rdi
	callq	gs_grestore
	testl	%eax, %eax
	js	.LBB2_2
# BB#1:
	movq	istate(%rip), %rdi
	movq	64(%rdi), %rax
	movq	%rax, istate+64(%rip)
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movups	48(%rdi), %xmm3
	movups	%xmm3, istate+48(%rip)
	movups	%xmm2, istate+32(%rip)
	movups	%xmm1, istate+16(%rip)
	movups	%xmm0, istate(%rip)
	movl	$1, %esi
	movl	$72, %edx
	movl	$.L.str.1, %ecx
	callq	alloc_free
.LBB2_2:
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end2:
	.size	zgrestore, .Lfunc_end2-zgrestore
	.cfi_endproc

	.globl	zgrestoreall
	.p2align	4, 0x90
	.type	zgrestoreall,@function
zgrestoreall:                           # @zgrestoreall
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movq	igs(%rip), %rdi
	callq	gs_grestoreall
	jmp	.LBB3_2
	.p2align	4, 0x90
.LBB3_1:                                # %.lr.ph
                                        #   in Loop: Header=BB3_2 Depth=1
	movq	64(%rdi), %rax
	movq	%rax, istate+64(%rip)
	movups	(%rdi), %xmm0
	movups	16(%rdi), %xmm1
	movups	32(%rdi), %xmm2
	movups	48(%rdi), %xmm3
	movups	%xmm3, istate+48(%rip)
	movups	%xmm2, istate+32(%rip)
	movups	%xmm1, istate+16(%rip)
	movups	%xmm0, istate(%rip)
	movl	$1, %esi
	movl	$72, %edx
	movl	$.L.str.2, %ecx
	callq	alloc_free
.LBB3_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	istate(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB3_1
# BB#3:                                 # %._crit_edge
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end3:
	.size	zgrestoreall, .Lfunc_end3-zgrestoreall
	.cfi_endproc

	.globl	zinitgraphics
	.p2align	4, 0x90
	.type	zinitgraphics,@function
zinitgraphics:                          # @zinitgraphics
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_initgraphics         # TAILCALL
.Lfunc_end4:
	.size	zinitgraphics, .Lfunc_end4-zinitgraphics
	.cfi_endproc

	.globl	zsetlinewidth
	.p2align	4, 0x90
	.type	zsetlinewidth,@function
zsetlinewidth:                          # @zsetlinewidth
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi5:
	.cfi_def_cfa_offset 16
	leaq	4(%rsp), %rsi
	xorl	%edx, %edx
	callq	real_param
	testl	%eax, %eax
	jne	.LBB5_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	gs_setlinewidth
	testl	%eax, %eax
	jne	.LBB5_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB5_3:                                # %num_param.exit
	popq	%rcx
	retq
.Lfunc_end5:
	.size	zsetlinewidth, .Lfunc_end5-zsetlinewidth
	.cfi_endproc

	.globl	num_param
	.p2align	4, 0x90
	.type	num_param,@function
num_param:                              # @num_param
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	leaq	12(%rsp), %rsi
	xorl	%edx, %edx
	callq	real_param
	testl	%eax, %eax
	jne	.LBB6_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	*%rbx
	testl	%eax, %eax
	jne	.LBB6_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB6_3:                                # %.thread
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end6:
	.size	num_param, .Lfunc_end6-num_param
	.cfi_endproc

	.globl	zcurrentlinewidth
	.p2align	4, 0x90
	.type	zcurrentlinewidth,@function
zcurrentlinewidth:                      # @zcurrentlinewidth
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 16
.Lcfi10:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB7_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB7_2:
	movq	igs(%rip), %rdi
	callq	gs_currentlinewidth
	movss	%xmm0, 16(%rbx)
	movw	$44, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end7:
	.size	zcurrentlinewidth, .Lfunc_end7-zcurrentlinewidth
	.cfi_endproc

	.globl	zsetlinecap
	.p2align	4, 0x90
	.type	zsetlinecap,@function
zsetlinecap:                            # @zsetlinecap
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB8_2
# BB#1:
	movq	(%rdi), %rsi
	movl	$-15, %eax
	cmpq	$2, %rsi
	jbe	.LBB8_3
.LBB8_2:                                # %line_param.exit.thread
	retq
.LBB8_3:
	addq	$-16, osp(%rip)
	movq	igs(%rip), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	jmp	gs_setlinecap           # TAILCALL
.Lfunc_end8:
	.size	zsetlinecap, .Lfunc_end8-zsetlinecap
	.cfi_endproc

	.globl	line_param
	.p2align	4, 0x90
	.type	line_param,@function
line_param:                             # @line_param
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB9_3
# BB#1:
	movq	(%rdi), %rcx
	movl	$-15, %eax
	cmpq	$2, %rcx
	ja	.LBB9_3
# BB#2:
	movl	%ecx, (%rsi)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB9_3:
	retq
.Lfunc_end9:
	.size	line_param, .Lfunc_end9-line_param
	.cfi_endproc

	.globl	zcurrentlinecap
	.p2align	4, 0x90
	.type	zcurrentlinecap,@function
zcurrentlinecap:                        # @zcurrentlinecap
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB10_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB10_2:
	movq	igs(%rip), %rdi
	callq	gs_currentlinecap
	cltq
	movq	%rax, 16(%rbx)
	movw	$20, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end10:
	.size	zcurrentlinecap, .Lfunc_end10-zcurrentlinecap
	.cfi_endproc

	.globl	zsetlinejoin
	.p2align	4, 0x90
	.type	zsetlinejoin,@function
zsetlinejoin:                           # @zsetlinejoin
	.cfi_startproc
# BB#0:
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB11_2
# BB#1:
	movq	(%rdi), %rsi
	movl	$-15, %eax
	cmpq	$2, %rsi
	jbe	.LBB11_3
.LBB11_2:                               # %line_param.exit.thread
	retq
.LBB11_3:
	addq	$-16, osp(%rip)
	movq	igs(%rip), %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	jmp	gs_setlinejoin          # TAILCALL
.Lfunc_end11:
	.size	zsetlinejoin, .Lfunc_end11-zsetlinejoin
	.cfi_endproc

	.globl	zcurrentlinejoin
	.p2align	4, 0x90
	.type	zcurrentlinejoin,@function
zcurrentlinejoin:                       # @zcurrentlinejoin
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 16
.Lcfi14:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB12_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB12_2:
	movq	igs(%rip), %rdi
	callq	gs_currentlinejoin
	cltq
	movq	%rax, 16(%rbx)
	movw	$20, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end12:
	.size	zcurrentlinejoin, .Lfunc_end12-zcurrentlinejoin
	.cfi_endproc

	.globl	zsetmiterlimit
	.p2align	4, 0x90
	.type	zsetmiterlimit,@function
zsetmiterlimit:                         # @zsetmiterlimit
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	leaq	4(%rsp), %rsi
	xorl	%edx, %edx
	callq	real_param
	testl	%eax, %eax
	jne	.LBB13_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	gs_setmiterlimit
	testl	%eax, %eax
	jne	.LBB13_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB13_3:                               # %num_param.exit
	popq	%rcx
	retq
.Lfunc_end13:
	.size	zsetmiterlimit, .Lfunc_end13-zsetmiterlimit
	.cfi_endproc

	.globl	zcurrentmiterlimit
	.p2align	4, 0x90
	.type	zcurrentmiterlimit,@function
zcurrentmiterlimit:                     # @zcurrentmiterlimit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi16:
	.cfi_def_cfa_offset 16
.Lcfi17:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB14_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB14_2:
	movq	igs(%rip), %rdi
	callq	gs_currentmiterlimit
	movss	%xmm0, 16(%rbx)
	movw	$44, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end14:
	.size	zcurrentmiterlimit, .Lfunc_end14-zcurrentmiterlimit
	.cfi_endproc

	.globl	zsetdash
	.p2align	4, 0x90
	.type	zsetdash,@function
zsetdash:                               # @zsetdash
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi20:
	.cfi_def_cfa_offset 32
.Lcfi21:
	.cfi_offset %rbx, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbp
	leaq	4(%rsp), %rsi
	xorl	%edx, %edx
	callq	real_param
	testl	%eax, %eax
	je	.LBB15_1
.LBB15_14:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB15_1:
	movw	-8(%rbp), %cx
	movl	%ecx, %edx
	shrb	$2, %dl
	movl	$-20, %eax
	cmpb	$10, %dl
	je	.LBB15_3
# BB#2:
	testb	%dl, %dl
	jne	.LBB15_14
.LBB15_3:
	movl	$-7, %eax
	testb	$2, %ch
	je	.LBB15_14
# BB#4:
	movq	-16(%rbp), %rbx
	movzwl	-6(%rbp), %ebp
	movl	$4, %esi
	movl	$.L.str.3, %edx
	movl	%ebp, %edi
	callq	alloc
	testl	%ebp, %ebp
	je	.LBB15_12
# BB#5:                                 # %.lr.ph.preheader
	movl	%ebp, %ecx
	negl	%ecx
	movq	%rax, %rdi
	.p2align	4, 0x90
.LBB15_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzbl	8(%rbx), %edx
	shrb	$2, %dl
	cmpb	$11, %dl
	je	.LBB15_10
# BB#7:                                 # %.lr.ph
                                        #   in Loop: Header=BB15_6 Depth=1
	cmpb	$5, %dl
	jne	.LBB15_9
# BB#8:                                 #   in Loop: Header=BB15_6 Depth=1
	cvtsi2ssq	(%rbx), %xmm0
	movss	%xmm0, (%rdi)
	jmp	.LBB15_11
	.p2align	4, 0x90
.LBB15_10:                              #   in Loop: Header=BB15_6 Depth=1
	movl	(%rbx), %edx
	movl	%edx, (%rdi)
.LBB15_11:                              #   in Loop: Header=BB15_6 Depth=1
	addq	$4, %rdi
	addq	$16, %rbx
	incl	%ecx
	jne	.LBB15_6
.LBB15_12:                              # %._crit_edge
	movq	igs(%rip), %rdi
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	movq	%rax, %rsi
	movl	%ebp, %edx
	callq	gs_setdash
	testl	%eax, %eax
	jne	.LBB15_14
# BB#13:
	addq	$-32, osp(%rip)
	xorl	%eax, %eax
	jmp	.LBB15_14
.LBB15_9:
	movl	$4, %edx
	movl	$.L.str.3, %ecx
	movl	%ebp, %esi
	callq	alloc_free
	movl	$-20, %eax
	jmp	.LBB15_14
.Lfunc_end15:
	.size	zsetdash, .Lfunc_end15-zsetdash
	.cfi_endproc

	.globl	zcurrentdash
	.p2align	4, 0x90
	.type	zcurrentdash,@function
zcurrentdash:                           # @zcurrentdash
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi29:
	.cfi_def_cfa_offset 64
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	igs(%rip), %rdi
	callq	gs_currentdash_length
	movl	%eax, %r12d
	movl	%r12d, %r13d
	movl	$16, %esi
	movl	$.L.str.4, %edx
	movl	%r12d, %edi
	callq	alloc
	movq	%rax, %r14
	movslq	%r12d, %rbp
	leaq	(%rbp,%rbp,2), %rax
	leaq	(%r14,%rax,4), %rbx
	movq	igs(%rip), %rdi
	movq	%rbx, %rsi
	callq	gs_currentdash_pattern
	testl	%ebp, %ebp
	je	.LBB16_7
# BB#1:                                 # %.lr.ph.preheader
	leal	-1(%r12), %edx
	andl	$3, %r12d
	je	.LBB16_2
# BB#3:                                 # %.lr.ph.prol.preheader
	negl	%r12d
	movq	%r14, %rax
	movl	%r13d, %ecx
	.p2align	4, 0x90
.LBB16_4:                               # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	decl	%ecx
	movl	(%rbx), %esi
	movl	%esi, (%rax)
	movw	$44, 8(%rax)
	addq	$16, %rax
	addq	$4, %rbx
	incl	%r12d
	jne	.LBB16_4
	jmp	.LBB16_5
.LBB16_2:
	movq	%r14, %rax
	movl	%r13d, %ecx
.LBB16_5:                               # %.lr.ph.prol.loopexit
	cmpl	$3, %edx
	jb	.LBB16_7
	.p2align	4, 0x90
.LBB16_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	movl	%edx, (%rax)
	movw	$44, 8(%rax)
	movl	4(%rbx), %edx
	movl	%edx, 16(%rax)
	movw	$44, 24(%rax)
	movl	8(%rbx), %edx
	movl	%edx, 32(%rax)
	movw	$44, 40(%rax)
	addl	$-4, %ecx
	movl	12(%rbx), %edx
	leaq	16(%rbx), %rbx
	movl	%edx, 48(%rax)
	movw	$44, 56(%rax)
	leaq	64(%rax), %rax
	jne	.LBB16_6
.LBB16_7:                               # %._crit_edge
	leaq	32(%r15), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB16_9
# BB#8:
	movq	%r15, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB16_10
.LBB16_9:
	movq	%r14, 16(%r15)
	movw	$770, 24(%r15)          # imm = 0x302
	movw	%r13w, 26(%r15)
	movq	igs(%rip), %rdi
	callq	gs_currentdash_offset
	movss	%xmm0, 32(%r15)
	movw	$44, 40(%r15)
	xorl	%eax, %eax
.LBB16_10:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end16:
	.size	zcurrentdash, .Lfunc_end16-zcurrentdash
	.cfi_endproc

	.globl	zsetflat
	.p2align	4, 0x90
	.type	zsetflat,@function
zsetflat:                               # @zsetflat
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 16
	leaq	4(%rsp), %rsi
	xorl	%edx, %edx
	callq	real_param
	testl	%eax, %eax
	jne	.LBB17_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	gs_setflat
	testl	%eax, %eax
	jne	.LBB17_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB17_3:                               # %num_param.exit
	popq	%rcx
	retq
.Lfunc_end17:
	.size	zsetflat, .Lfunc_end17-zsetflat
	.cfi_endproc

	.globl	zcurrentflat
	.p2align	4, 0x90
	.type	zcurrentflat,@function
zcurrentflat:                           # @zcurrentflat
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi37:
	.cfi_def_cfa_offset 16
.Lcfi38:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB18_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB18_2:
	movq	igs(%rip), %rdi
	callq	gs_currentflat
	movss	%xmm0, 16(%rbx)
	movw	$44, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end18:
	.size	zcurrentflat, .Lfunc_end18-zcurrentflat
	.cfi_endproc

	.globl	zsetgray
	.p2align	4, 0x90
	.type	zsetgray,@function
zsetgray:                               # @zsetgray
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi39:
	.cfi_def_cfa_offset 16
	leaq	4(%rsp), %rsi
	xorl	%edx, %edx
	callq	real_param
	testl	%eax, %eax
	jne	.LBB19_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	4(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	callq	gs_setgray
	testl	%eax, %eax
	jne	.LBB19_3
# BB#2:
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB19_3:                               # %num_param.exit
	popq	%rcx
	retq
.Lfunc_end19:
	.size	zsetgray, .Lfunc_end19-zsetgray
	.cfi_endproc

	.globl	zcurrentgray
	.p2align	4, 0x90
	.type	zcurrentgray,@function
zcurrentgray:                           # @zcurrentgray
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 16
.Lcfi41:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	leaq	16(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB20_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	popq	%rbx
	retq
.LBB20_2:
	movq	igs(%rip), %rdi
	callq	gs_currentgray
	movss	%xmm0, 16(%rbx)
	movw	$44, 24(%rbx)
	xorl	%eax, %eax
	popq	%rbx
	retq
.Lfunc_end20:
	.size	zcurrentgray, .Lfunc_end20-zcurrentgray
	.cfi_endproc

	.globl	zsethsbcolor
	.p2align	4, 0x90
	.type	zsethsbcolor,@function
zsethsbcolor:                           # @zsethsbcolor
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 32
	leaq	12(%rsp), %rdx
	movl	$3, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB21_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	20(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	callq	gs_sethsbcolor
	testl	%eax, %eax
	js	.LBB21_3
# BB#2:
	addq	$-48, osp(%rip)
.LBB21_3:
	addq	$24, %rsp
	retq
.Lfunc_end21:
	.size	zsethsbcolor, .Lfunc_end21-zsethsbcolor
	.cfi_endproc

	.globl	zcurrenthsbcolor
	.p2align	4, 0x90
	.type	zcurrenthsbcolor,@function
zcurrenthsbcolor:                       # @zcurrenthsbcolor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 32
.Lcfi45:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	igs(%rip), %rdi
	leaq	4(%rsp), %rsi
	callq	gs_currenthsbcolor
	leaq	48(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB22_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB22_3
.LBB22_2:
	movl	4(%rsp), %eax
	movl	%eax, 16(%rbx)
	movw	$44, 24(%rbx)
	movl	8(%rsp), %eax
	movl	%eax, 32(%rbx)
	movw	$44, 40(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 48(%rbx)
	movw	$44, 56(%rbx)
	xorl	%eax, %eax
.LBB22_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end22:
	.size	zcurrenthsbcolor, .Lfunc_end22-zcurrenthsbcolor
	.cfi_endproc

	.globl	tri_put
	.p2align	4, 0x90
	.type	tri_put,@function
tri_put:                                # @tri_put
	.cfi_startproc
# BB#0:
	movl	(%rsi), %eax
	movl	%eax, -32(%rdi)
	movw	$44, -24(%rdi)
	movl	4(%rsi), %eax
	movl	%eax, -16(%rdi)
	movw	$44, -8(%rdi)
	movl	8(%rsi), %eax
	movl	%eax, (%rdi)
	movw	$44, 8(%rdi)
	retq
.Lfunc_end23:
	.size	tri_put, .Lfunc_end23-tri_put
	.cfi_endproc

	.globl	zsetrgbcolor
	.p2align	4, 0x90
	.type	zsetrgbcolor,@function
zsetrgbcolor:                           # @zsetrgbcolor
	.cfi_startproc
# BB#0:
	subq	$24, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 32
	leaq	12(%rsp), %rdx
	movl	$3, %esi
	callq	num_params
	testl	%eax, %eax
	js	.LBB24_3
# BB#1:
	movq	igs(%rip), %rdi
	movss	12(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	movss	20(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	callq	gs_setrgbcolor
	testl	%eax, %eax
	js	.LBB24_3
# BB#2:
	addq	$-48, osp(%rip)
.LBB24_3:
	addq	$24, %rsp
	retq
.Lfunc_end24:
	.size	zsetrgbcolor, .Lfunc_end24-zsetrgbcolor
	.cfi_endproc

	.globl	zcurrentrgbcolor
	.p2align	4, 0x90
	.type	zcurrentrgbcolor,@function
zcurrentrgbcolor:                       # @zcurrentrgbcolor
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	igs(%rip), %rdi
	leaq	4(%rsp), %rsi
	callq	gs_currentrgbcolor
	leaq	48(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB25_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB25_3
.LBB25_2:
	movl	4(%rsp), %eax
	movl	%eax, 16(%rbx)
	movw	$44, 24(%rbx)
	movl	8(%rsp), %eax
	movl	%eax, 32(%rbx)
	movw	$44, 40(%rbx)
	movl	12(%rsp), %eax
	movl	%eax, 48(%rbx)
	movw	$44, 56(%rbx)
	xorl	%eax, %eax
.LBB25_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end25:
	.size	zcurrentrgbcolor, .Lfunc_end25-zcurrentrgbcolor
	.cfi_endproc

	.globl	zsettransfer
	.p2align	4, 0x90
	.type	zsettransfer,@function
zsettransfer:                           # @zsettransfer
	.cfi_startproc
# BB#0:
	movw	8(%rdi), %cx
	movl	%ecx, %edx
	shrb	$2, %dl
	cmpb	$10, %dl
	je	.LBB26_2
# BB#1:
	movl	$-20, %eax
	testb	%dl, %dl
	jne	.LBB26_4
.LBB26_2:
	andl	$3, %ecx
	movl	$-7, %eax
	cmpl	$3, %ecx
	jne	.LBB26_4
# BB#3:
	movups	(%rdi), %xmm0
	movups	%xmm0, istate+24(%rip)
	addq	$-16, osp(%rip)
	xorl	%eax, %eax
.LBB26_4:
	retq
.Lfunc_end26:
	.size	zsettransfer, .Lfunc_end26-zsettransfer
	.cfi_endproc

	.globl	zcurrenttransfer
	.p2align	4, 0x90
	.type	zcurrenttransfer,@function
zcurrenttransfer:                       # @zcurrenttransfer
	.cfi_startproc
# BB#0:
	leaq	16(%rdi), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB27_2
# BB#1:
	movq	%rdi, osp(%rip)
	movl	$-16, %eax
	retq
.LBB27_2:
	movups	istate+24(%rip), %xmm0
	movups	%xmm0, (%rax)
	xorl	%eax, %eax
	retq
.Lfunc_end27:
	.size	zcurrenttransfer, .Lfunc_end27-zcurrenttransfer
	.cfi_endproc

	.globl	zcurrentscreen
	.p2align	4, 0x90
	.type	zcurrentscreen,@function
zcurrentscreen:                         # @zcurrentscreen
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 16
	subq	$16, %rsp
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	igs(%rip), %rdi
	leaq	4(%rsp), %rsi
	movq	%rsp, %rdx
	leaq	8(%rsp), %rcx
	callq	gs_currentscreen
	leaq	48(%rbx), %rax
	movq	%rax, osp(%rip)
	cmpq	ostop(%rip), %rax
	jbe	.LBB28_2
# BB#1:
	movq	%rbx, osp(%rip)
	movl	$-16, %eax
	jmp	.LBB28_3
.LBB28_2:
	movl	4(%rsp), %ecx
	movl	%ecx, 16(%rbx)
	movw	$44, 24(%rbx)
	movl	(%rsp), %ecx
	movl	%ecx, 32(%rbx)
	movw	$44, 40(%rbx)
	movups	istate+8(%rip), %xmm0
	movups	%xmm0, (%rax)
	xorl	%eax, %eax
.LBB28_3:
	addq	$16, %rsp
	popq	%rbx
	retq
.Lfunc_end28:
	.size	zcurrentscreen, .Lfunc_end28-zcurrentscreen
	.cfi_endproc

	.globl	zgstate_op_init
	.p2align	4, 0x90
	.type	zgstate_op_init,@function
zgstate_op_init:                        # @zgstate_op_init
	.cfi_startproc
# BB#0:
	movl	$zgstate_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end29:
	.size	zgstate_op_init, .Lfunc_end29-zgstate_op_init
	.cfi_endproc

	.type	igs,@object             # @igs
	.comm	igs,8,8
	.type	istate,@object          # @istate
	.comm	istate,72,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"gsave"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"grestore"
	.size	.L.str.1, 9

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"restoreall"
	.size	.L.str.2, 11

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"setdash"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"currentdash"
	.size	.L.str.4, 12

	.type	zgstate_op_init.my_defs,@object # @zgstate_op_init.my_defs
	.data
	.p2align	4
zgstate_op_init.my_defs:
	.quad	.L.str.5
	.quad	zcurrentdash
	.quad	.L.str.6
	.quad	zcurrentflat
	.quad	.L.str.7
	.quad	zcurrentgray
	.quad	.L.str.8
	.quad	zcurrenthsbcolor
	.quad	.L.str.9
	.quad	zcurrentlinecap
	.quad	.L.str.10
	.quad	zcurrentlinejoin
	.quad	.L.str.11
	.quad	zcurrentlinewidth
	.quad	.L.str.12
	.quad	zcurrentmiterlimit
	.quad	.L.str.13
	.quad	zcurrentrgbcolor
	.quad	.L.str.14
	.quad	zcurrentscreen
	.quad	.L.str.15
	.quad	zcurrenttransfer
	.quad	.L.str.16
	.quad	zgrestore
	.quad	.L.str.17
	.quad	zgrestoreall
	.quad	.L.str.18
	.quad	zgsave
	.quad	.L.str.19
	.quad	zinitgraphics
	.quad	.L.str.20
	.quad	zsetdash
	.quad	.L.str.21
	.quad	zsetflat
	.quad	.L.str.22
	.quad	zsetgray
	.quad	.L.str.23
	.quad	zsethsbcolor
	.quad	.L.str.24
	.quad	zsetlinecap
	.quad	.L.str.25
	.quad	zsetlinejoin
	.quad	.L.str.26
	.quad	zsetlinewidth
	.quad	.L.str.27
	.quad	zsetmiterlimit
	.quad	.L.str.28
	.quad	zsetrgbcolor
	.quad	.L.str.29
	.quad	zsettransfer
	.zero	16
	.size	zgstate_op_init.my_defs, 416

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"0currentdash"
	.size	.L.str.5, 13

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"0currentflat"
	.size	.L.str.6, 13

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"0currentgray"
	.size	.L.str.7, 13

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"0currenthsbcolor"
	.size	.L.str.8, 17

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"0currentlinecap"
	.size	.L.str.9, 16

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"0currentlinejoin"
	.size	.L.str.10, 17

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"0currentlinewidth"
	.size	.L.str.11, 18

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"0currentmiterlimit"
	.size	.L.str.12, 19

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"0currentrgbcolor"
	.size	.L.str.13, 17

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"0currentscreen"
	.size	.L.str.14, 15

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"0currenttransfer"
	.size	.L.str.15, 17

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"0grestore"
	.size	.L.str.16, 10

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"0grestoreall"
	.size	.L.str.17, 13

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"0gsave"
	.size	.L.str.18, 7

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"0initgraphics"
	.size	.L.str.19, 14

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"2setdash"
	.size	.L.str.20, 9

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"1setflat"
	.size	.L.str.21, 9

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"1.setgray"
	.size	.L.str.22, 10

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"3sethsbcolor"
	.size	.L.str.23, 13

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"1setlinecap"
	.size	.L.str.24, 12

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"1setlinejoin"
	.size	.L.str.25, 13

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"1setlinewidth"
	.size	.L.str.26, 14

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"1setmiterlimit"
	.size	.L.str.27, 15

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"3setrgbcolor"
	.size	.L.str.28, 13

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"1settransfer"
	.size	.L.str.29, 13


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
