	.text
	.file	"aesxam.bc"
	.globl	fillrand
	.p2align	4, 0x90
	.type	fillrand,@function
fillrand:                               # @fillrand
	.cfi_startproc
# BB#0:
	movb	fillrand.mt(%rip), %al
	testb	%al, %al
	jne	.LBB0_2
# BB#1:
	movb	$1, fillrand.mt(%rip)
	movq	$60147, fillrand.a.0(%rip) # imm = 0xEAF3
	movq	$13822, fillrand.a.1(%rip) # imm = 0x35FE
.LBB0_2:                                # %.preheader
	testl	%esi, %esi
	jle	.LBB0_9
# BB#3:                                 # %.lr.ph
	movq	fillrand.count(%rip), %rcx
	movl	%esi, %eax
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	cmpq	$4, %rcx
	jne	.LBB0_5
# BB#6:                                 #   in Loop: Header=BB0_4 Depth=1
	movq	fillrand.a.0(%rip), %rcx
	movzwl	%cx, %edx
	imulq	$36969, %rdx, %rdx      # imm = 0x9069
	shrq	$16, %rcx
	addq	%rdx, %rcx
	movq	%rcx, fillrand.a.0(%rip)
	shlq	$16, %rcx
	movq	fillrand.a.1(%rip), %rdx
	movzwl	%dx, %esi
	imulq	$18000, %rsi, %rsi      # imm = 0x4650
	shrq	$16, %rdx
	addq	%rsi, %rdx
	movq	%rdx, fillrand.a.1(%rip)
	addq	%rcx, %rdx
	movq	%rdx, fillrand.r(%rip)
	xorl	%ecx, %ecx
	jmp	.LBB0_7
	.p2align	4, 0x90
.LBB0_5:                                # %._crit_edge6
                                        #   in Loop: Header=BB0_4 Depth=1
	movb	fillrand.r(%rcx), %dl
.LBB0_7:                                #   in Loop: Header=BB0_4 Depth=1
	incq	%rcx
	movb	%dl, (%rdi)
	incq	%rdi
	decq	%rax
	jne	.LBB0_4
# BB#8:                                 # %._crit_edge
	movq	%rcx, fillrand.count(%rip)
.LBB0_9:
	retq
.Lfunc_end0:
	.size	fillrand, .Lfunc_end0-fillrand
	.cfi_endproc

	.globl	encfile
	.p2align	4, 0x90
	.type	encfile,@function
encfile:                                # @encfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %rbx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movq	%rsi, %r12
	movq	%rdi, %r14
	movb	fillrand.mt(%rip), %al
	testb	%al, %al
	jne	.LBB1_2
# BB#1:
	movb	$1, fillrand.mt(%rip)
	movq	$60147, fillrand.a.0(%rip) # imm = 0xEAF3
	movq	$13822, fillrand.a.1(%rip) # imm = 0x35FE
.LBB1_2:                                # %.preheader.i
	movq	fillrand.count(%rip), %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	cmpq	$4, %rcx
	jne	.LBB1_4
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	movq	fillrand.a.0(%rip), %rcx
	movzwl	%cx, %edx
	imulq	$36969, %rdx, %rdx      # imm = 0x9069
	shrq	$16, %rcx
	addq	%rdx, %rcx
	movq	%rcx, fillrand.a.0(%rip)
	shlq	$16, %rcx
	movq	fillrand.a.1(%rip), %rdx
	movzwl	%dx, %esi
	imulq	$18000, %rsi, %rsi      # imm = 0x4650
	shrq	$16, %rdx
	addq	%rsi, %rdx
	movq	%rdx, fillrand.a.1(%rip)
	addq	%rcx, %rdx
	movq	%rdx, fillrand.r(%rip)
	xorl	%ecx, %ecx
	jmp	.LBB1_6
	.p2align	4, 0x90
.LBB1_4:                                # %._crit_edge6.i
                                        #   in Loop: Header=BB1_3 Depth=1
	movb	fillrand.r(%rcx), %dl
.LBB1_6:                                #   in Loop: Header=BB1_3 Depth=1
	incq	%rcx
	movb	%dl, 16(%rsp,%rax)
	incq	%rax
	cmpq	$16, %rax
	jne	.LBB1_3
# BB#7:                                 # %fillrand.exit
	movq	%rcx, fillrand.count(%rip)
	xorl	%esi, %esi
	movl	$2, %edx
	movq	%r14, %rdi
	callq	fseek
	movq	%r14, %rdi
	callq	ftell
	movq	%rax, %r13
	xorl	%esi, %esi
	xorl	%edx, %edx
	movq	%r14, %rdi
	callq	fseek
	leaq	16(%rsp), %rdi
	movl	$1, %esi
	movl	$16, %edx
	movq	%r12, %rcx
	callq	fwrite
	movb	fillrand.mt(%rip), %al
	testb	%al, %al
	jne	.LBB1_9
# BB#8:
	movb	$1, fillrand.mt(%rip)
	movq	$60147, fillrand.a.0(%rip) # imm = 0xEAF3
	movq	$13822, fillrand.a.1(%rip) # imm = 0x35FE
.LBB1_9:                                # %.preheader.i38
	movq	fillrand.count(%rip), %rcx
	cmpq	$4, %rcx
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	jne	.LBB1_10
# BB#11:
	movq	fillrand.a.0(%rip), %rcx
	movzwl	%cx, %eax
	imulq	$36969, %rax, %rax      # imm = 0x9069
	shrq	$16, %rcx
	addq	%rax, %rcx
	movq	%rcx, fillrand.a.0(%rip)
	shlq	$16, %rcx
	movq	fillrand.a.1(%rip), %rax
	movzwl	%ax, %edx
	imulq	$18000, %rdx, %rdx      # imm = 0x4650
	shrq	$16, %rax
	addq	%rdx, %rax
	movq	%rax, fillrand.a.1(%rip)
	addq	%rcx, %rax
	movq	%rax, fillrand.r(%rip)
	movl	$1, %ecx
	jmp	.LBB1_12
.LBB1_10:                               # %._crit_edge6.i43
	movb	fillrand.r(%rcx), %al
	incq	%rcx
.LBB1_12:                               # %fillrand.exit4657
	movq	%rcx, fillrand.count(%rip)
	andb	$15, %r13b
	andb	$-16, %al
	orb	%r13b, %al
	movb	%al, (%rsp)
	movl	$15, %r15d
	xorl	%ebx, %ebx
	movq	%rsp, %r13
	leaq	16(%rsp), %rbp
	.p2align	4, 0x90
.LBB1_13:                               # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	feof
	testl	%eax, %eax
	jne	.LBB1_17
# BB#14:                                #   in Loop: Header=BB1_13 Depth=1
	movq	%r13, %rdi
	subq	%r15, %rdi
	addq	$16, %rdi
	movl	$1, %esi
	movq	%r15, %rdx
	movq	%r14, %rcx
	callq	fread
	movq	%rax, %rbx
	cmpq	%r15, %rbx
	jb	.LBB1_17
# BB#15:                                # %.preheader48.preheader
                                        #   in Loop: Header=BB1_13 Depth=1
	movaps	(%rsp), %xmm0
	xorps	16(%rsp), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	encrypt
	movl	$1, %esi
	movl	$16, %edx
	movq	%rbp, %rdi
	movq	%r12, %rcx
	callq	fwrite
	cmpq	$16, %rax
	movl	$16, %ebx
	movl	$16, %r15d
	je	.LBB1_13
# BB#16:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	printf
	movl	$-7, %ebp
	jmp	.LBB1_22
.LBB1_17:
	xorl	%eax, %eax
	cmpq	$15, %r15
	sete	%al
	xorl	%ebp, %ebp
	movq	%rbx, %rcx
	addq	%rax, %rcx
	je	.LBB1_22
# BB#18:                                # %.preheader47
	cmpq	$15, %rcx
	ja	.LBB1_20
# BB#19:                                # %.lr.ph.preheader
	leaq	(%rsp,%rcx), %rdi
	movl	$16, %edx
	subq	%rbx, %rdx
	subq	%rax, %rdx
	xorl	%esi, %esi
	callq	memset
.LBB1_20:                               # %.preheader.preheader
	movaps	(%rsp), %xmm0
	xorps	16(%rsp), %xmm0
	movaps	%xmm0, (%rsp)
	movq	%rsp, %rdi
	leaq	16(%rsp), %rbx
	movq	%rbx, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
	callq	encrypt
	movl	$1, %esi
	movl	$16, %edx
	movq	%rbx, %rdi
	movq	%r12, %rcx
	callq	fwrite
	cmpq	$16, %rax
	je	.LBB1_22
# BB#21:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	printf
	movl	$-8, %ebp
.LBB1_22:
	movl	%ebp, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	encfile, .Lfunc_end1-encfile
	.cfi_endproc

	.globl	decfile
	.p2align	4, 0x90
	.type	decfile,@function
decfile:                                # @decfile
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 176
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r8, %r15
	movq	%rcx, %rbp
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	movq	%rdi, %rbx
	leaq	64(%rsp), %rdi
	movl	$1, %esi
	movl	$16, %edx
	movq	%rbx, %rcx
	callq	fread
	cmpq	$16, %rax
	jne	.LBB2_1
# BB#2:
	leaq	96(%rsp), %rdi
	movl	$1, %esi
	movl	$16, %edx
	movq	%rbx, %rcx
	callq	fread
	orl	$16, %eax
	cmpl	$16, %eax
	jne	.LBB2_3
# BB#4:
	leaq	96(%rsp), %r14
	leaq	16(%rsp), %r13
	movq	%r14, %rdi
	movq	%r13, %rsi
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	decrypt
	movaps	16(%rsp), %xmm0
	xorps	64(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	movaps	%xmm0, 80(%rsp)
	movb	80(%rsp), %r12b
	leaq	64(%rsp), %rbp
	movl	$1, %esi
	movl	$16, %edx
	movq	%rbp, %rdi
	movq	%rbx, %rcx
	callq	fread
	movb	$1, %cl
	cmpl	$16, %eax
	jne	.LBB2_10
# BB#5:                                 # %.lr.ph
	movb	%r12b, 15(%rsp)         # 1-byte Spill
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	$15, %r12d
	.p2align	4, 0x90
.LBB2_6:                                # =>This Inner Loop Header: Depth=1
	movq	%r14, %r15
	movq	%r13, %rdi
	subq	%r12, %rdi
	addq	$16, %rdi
	movl	$1, %esi
	movq	%r12, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpq	%r12, %rax
	jne	.LBB2_7
# BB#8:                                 # %.loopexit60
                                        #   in Loop: Header=BB2_6 Depth=1
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	decrypt
	movups	(%r15), %xmm0
	xorps	16(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	movl	$1, %esi
	movl	$16, %edx
	movq	%r15, %rdi
	movq	%rbx, %rcx
	callq	fread
	cmpl	$16, %eax
	movl	$16, %r12d
	movq	%rbp, %r14
	movq	%r15, %rbp
	je	.LBB2_6
# BB#9:
	xorl	%ecx, %ecx
	movq	40(%rsp), %r15          # 8-byte Reload
	movb	15(%rsp), %r12b         # 1-byte Reload
.LBB2_10:                               # %._crit_edge
	andb	$15, %r12b
	movzbl	%r12b, %edx
	movl	%ecx, %eax
	xorb	$1, %al
	movzbl	%al, %eax
	xorl	%ebx, %ebx
	addl	%edx, %eax
	je	.LBB2_13
# BB#11:
	movzbl	%cl, %ecx
	leaq	16(%rsp,%rcx), %rdi
	movl	%eax, %ebp
	movl	$1, %esi
	movq	%rbp, %rdx
	movq	48(%rsp), %rcx          # 8-byte Reload
	callq	fwrite
	cmpq	%rbp, %rax
	je	.LBB2_13
# BB#12:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	%r15, %rsi
	callq	printf
	movl	$-12, %ebx
	jmp	.LBB2_13
.LBB2_1:
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	movq	%rbp, %rsi
	callq	printf
	movl	$9, %ebx
	jmp	.LBB2_13
.LBB2_3:
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-10, %ebx
	jmp	.LBB2_13
.LBB2_7:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	printf
	movl	$-11, %ebx
.LBB2_13:
	movl	%ebx, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	decfile, .Lfunc_end2-decfile
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$568, %rsp              # imm = 0x238
.Lcfi32:
	.cfi_def_cfa_offset 624
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	cmpl	$5, %edi
	jne	.LBB3_2
# BB#1:
	callq	__ctype_toupper_loc
	movq	%rax, %r12
	movq	(%r12), %rax
	movq	24(%r14), %rcx
	movsbq	(%rcx), %rcx
	movl	(%rax,%rcx,4), %eax
	andl	$-2, %eax
	cmpl	$68, %eax
	jne	.LBB3_2
# BB#3:
	movq	32(%r14), %rsi
	movb	(%rsi), %al
	testb	%al, %al
	sete	%bl
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_4:                                # =>This Inner Loop Header: Depth=1
	testb	$1, %bl
	jne	.LBB3_13
# BB#5:                                 #   in Loop: Header=BB3_4 Depth=1
	movq	(%r12), %rdi
	movsbq	%al, %rax
	movl	(%rdi,%rax,4), %eax
	movl	%eax, %ebp
	shll	$24, %ebp
	leal	-788529153(%rbp), %ebx
	movl	$-48, %edi
	cmpl	$184549375, %ebx        # imm = 0xAFFFFFF
	jb	.LBB3_8
# BB#6:                                 #   in Loop: Header=BB3_4 Depth=1
	addl	$-1073741825, %ebp      # imm = 0xBFFFFFFF
	movl	$-55, %edi
	cmpl	$117440511, %ebp        # imm = 0x6FFFFFF
	jae	.LBB3_7
.LBB3_8:                                #   in Loop: Header=BB3_4 Depth=1
	movsbl	%al, %eax
	shll	$4, %edx
	addl	%eax, %edx
	addl	%edi, %edx
	leaq	1(%rcx), %rbp
	testb	$1, %cl
	je	.LBB3_10
# BB#9:                                 #   in Loop: Header=BB3_4 Depth=1
	movl	%ebp, %eax
	shrl	$31, %eax
	addl	%ebp, %eax
	sarl	%eax
	cltq
	movb	%dl, -1(%rsp,%rax)
.LBB3_10:                               # %.backedge
                                        #   in Loop: Header=BB3_4 Depth=1
	movzbl	1(%rsi,%rcx), %eax
	testb	%al, %al
	sete	%bl
	cmpl	$64, %ebp
	movq	%rbp, %rcx
	jl	.LBB3_4
# BB#11:                                # %.critedge
	testb	%al, %al
	je	.LBB3_14
# BB#12:
	movl	$.Lstr.1, %edi
	callq	puts
	movl	$-3, %ebx
	jmp	.LBB3_26
.LBB3_2:
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$-1, %ebx
.LBB3_26:                               # %.thread
	movl	%ebx, %eax
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_13:                               # %.critedge.thread.loopexit
	movl	%ecx, %ebp
.LBB3_14:                               # %.critedge.thread
	cmpl	$32, %ebp
	jl	.LBB3_16
# BB#15:                                # %.critedge.thread
	movl	%ebp, %eax
	andl	$15, %eax
	jne	.LBB3_16
# BB#17:
	movq	8(%r14), %rdi
	movl	$.L.str.7, %esi
	callq	fopen
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB3_18
# BB#19:
	movq	16(%r14), %rdi
	movl	$.L.str.9, %esi
	callq	fopen
	movq	%rax, %r13
	testq	%r13, %r13
	je	.LBB3_24
# BB#20:
	shrl	%ebp
	movq	(%r12), %rax
	movq	24(%r14), %rcx
	movsbq	(%rcx), %rcx
	cmpl	$69, (%rax,%rcx,4)
	jne	.LBB3_22
# BB#21:
	movq	%rsp, %rdi
	leaq	32(%rsp), %r12
	movl	$1, %edx
	movl	%ebp, %esi
	movq	%r12, %rcx
	callq	set_key
	movq	8(%r14), %rcx
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	%r12, %rdx
	callq	encfile
	jmp	.LBB3_23
.LBB3_16:
	movl	$.Lstr, %edi
	callq	puts
	movl	$-4, %ebx
	jmp	.LBB3_26
.LBB3_7:
	movl	$.Lstr.2, %edi
	callq	puts
	movl	$-2, %ebx
	jmp	.LBB3_26
.LBB3_18:
	movq	8(%r14), %rsi
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-5, %ebx
	jmp	.LBB3_26
.LBB3_24:
	movq	8(%r14), %rsi
	movl	$.L.str.10, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$-6, %ebx
	jmp	.LBB3_25
.LBB3_22:
	movq	%rsp, %rdi
	leaq	32(%rsp), %r12
	movl	$2, %edx
	movl	%ebp, %esi
	movq	%r12, %rcx
	callq	set_key
	movq	stdout(%rip), %rsi
	movq	8(%r14), %rcx
	movq	16(%r14), %r8
	movq	%r15, %rdi
	movq	%r12, %rdx
	callq	decfile
.LBB3_23:                               # %.thread82
	movl	%eax, %ebx
	movq	%r13, %rdi
	callq	fclose
.LBB3_25:
	movq	%r15, %rdi
	callq	fclose
	jmp	.LBB3_26
.Lfunc_end3:
	.size	main, .Lfunc_end3-main
	.cfi_endproc

	.type	fillrand.mt,@object     # @fillrand.mt
	.local	fillrand.mt
	.comm	fillrand.mt,1,8
	.type	fillrand.count,@object  # @fillrand.count
	.data
	.p2align	3
fillrand.count:
	.quad	4                       # 0x4
	.size	fillrand.count, 8

	.type	fillrand.r,@object      # @fillrand.r
	.local	fillrand.r
	.comm	fillrand.r,4,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Error writing to output file: %s\n"
	.size	.L.str, 34

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"Error reading from input file: %s\n"
	.size	.L.str.1, 35

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\nThe input file is corrupt"
	.size	.L.str.2, 27

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"rb"
	.size	.L.str.7, 3

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"The input file: %s could not be opened\n"
	.size	.L.str.8, 40

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"wb"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"The output file: %s could not be opened\n"
	.size	.L.str.10, 41

	.type	fillrand.a.0,@object    # @fillrand.a.0
	.local	fillrand.a.0
	.comm	fillrand.a.0,8,16
	.type	fillrand.a.1,@object    # @fillrand.a.1
	.local	fillrand.a.1
	.comm	fillrand.a.1,8,16
	.type	.Lstr,@object           # @str
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr:
	.asciz	"The key length must be 32, 48 or 64 hexadecimal digits"
	.size	.Lstr, 55

	.type	.Lstr.1,@object         # @str.1
	.p2align	4
.Lstr.1:
	.asciz	"The key value is too long"
	.size	.Lstr.1, 26

	.type	.Lstr.2,@object         # @str.2
	.p2align	4
.Lstr.2:
	.asciz	"key must be in hexadecimal notation"
	.size	.Lstr.2, 36

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"usage: rijndael in_filename out_filename [d/e] key_in_hex"
	.size	.Lstr.3, 58


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
