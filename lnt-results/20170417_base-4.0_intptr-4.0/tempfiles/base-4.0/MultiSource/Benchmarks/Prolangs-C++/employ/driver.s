	.text
	.file	"driver.bc"
	.globl	_ZN8EmployeeC2EPKcS1_
	.p2align	4, 0x90
	.type	_ZN8EmployeeC2EPKcS1_,@function
_ZN8EmployeeC2EPKcS1_:                  # @_ZN8EmployeeC2EPKcS1_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	strcpy                  # TAILCALL
.Lfunc_end0:
	.size	_ZN8EmployeeC2EPKcS1_, .Lfunc_end0-_ZN8EmployeeC2EPKcS1_
	.cfi_endproc

	.globl	_ZN8EmployeeD0Ev
	.p2align	4, 0x90
	.type	_ZN8EmployeeD0Ev,@function
_ZN8EmployeeD0Ev:                       # @_ZN8EmployeeD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_2
# BB#1:
	callq	_ZdaPv
.LBB1_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	callq	_ZdaPv
.LBB1_4:                                # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end1:
	.size	_ZN8EmployeeD0Ev, .Lfunc_end1-_ZN8EmployeeD0Ev
	.cfi_endproc

	.globl	_ZN8Employee9FirstNameEv
	.p2align	4, 0x90
	.type	_ZN8Employee9FirstNameEv,@function
_ZN8Employee9FirstNameEv:               # @_ZN8Employee9FirstNameEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end2:
	.size	_ZN8Employee9FirstNameEv, .Lfunc_end2-_ZN8Employee9FirstNameEv
	.cfi_endproc

	.globl	_ZN8Employee8LastNameEv
	.p2align	4, 0x90
	.type	_ZN8Employee8LastNameEv,@function
_ZN8Employee8LastNameEv:                # @_ZN8Employee8LastNameEv
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rax
	retq
.Lfunc_end3:
	.size	_ZN8Employee8LastNameEv, .Lfunc_end3-_ZN8Employee8LastNameEv
	.cfi_endproc

	.globl	_ZN8Employee17PrintWithEarningsEi
	.p2align	4, 0x90
	.type	_ZN8Employee17PrintWithEarningsEi,@function
_ZN8Employee17PrintWithEarningsEi:      # @_ZN8Employee17PrintWithEarningsEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	callq	*24(%rax)
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	cvtss2sd	%xmm0, %xmm0
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertIdEERSoT_
	movq	%rax, %rbx
	movl	$.L.str.1, %esi
	movl	$9, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%rbx, %rdi
	movl	%ebp, %esi
	callq	_ZNSolsEi
	movb	$10, 7(%rsp)
	leaq	7(%rsp), %rsi
	movl	$1, %edx
	movq	%rax, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end4:
	.size	_ZN8Employee17PrintWithEarningsEi, .Lfunc_end4-_ZN8Employee17PrintWithEarningsEi
	.cfi_endproc

	.globl	_ZN12EmployeeNodeC2EP8EmployeePS_
	.p2align	4, 0x90
	.type	_ZN12EmployeeNodeC2EP8EmployeePS_,@function
_ZN12EmployeeNodeC2EP8EmployeePS_:      # @_ZN12EmployeeNodeC2EP8EmployeePS_
	.cfi_startproc
# BB#0:
	movq	%rsi, (%rdi)
	movq	%rdx, 8(%rdi)
	retq
.Lfunc_end5:
	.size	_ZN12EmployeeNodeC2EP8EmployeePS_, .Lfunc_end5-_ZN12EmployeeNodeC2EP8EmployeePS_
	.cfi_endproc

	.globl	_ZN12EmployeeNode4NextEv
	.p2align	4, 0x90
	.type	_ZN12EmployeeNode4NextEv,@function
_ZN12EmployeeNode4NextEv:               # @_ZN12EmployeeNode4NextEv
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rax
	retq
.Lfunc_end6:
	.size	_ZN12EmployeeNode4NextEv, .Lfunc_end6-_ZN12EmployeeNode4NextEv
	.cfi_endproc

	.globl	_ZN7CompanyC2Ev
	.p2align	4, 0x90
	.type	_ZN7CompanyC2Ev,@function
_ZN7CompanyC2Ev:                        # @_ZN7CompanyC2Ev
	.cfi_startproc
# BB#0:
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rdi)
	retq
.Lfunc_end7:
	.size	_ZN7CompanyC2Ev, .Lfunc_end7-_ZN7CompanyC2Ev
	.cfi_endproc

	.globl	_ZN7Company13EmployeeCountEv
	.p2align	4, 0x90
	.type	_ZN7Company13EmployeeCountEv,@function
_ZN7Company13EmployeeCountEv:           # @_ZN7Company13EmployeeCountEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	retq
.Lfunc_end8:
	.size	_ZN7Company13EmployeeCountEv, .Lfunc_end8-_ZN7Company13EmployeeCountEv
	.cfi_endproc

	.globl	_ZN7Company11AddEmployeeEP8Employee
	.p2align	4, 0x90
	.type	_ZN7Company11AddEmployeeEP8Employee,@function
_ZN7Company11AddEmployeeEP8Employee:    # @_ZN7Company11AddEmployeeEP8Employee
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	movl	$16, %edi
	callq	_Znwm
	movq	(%rbx), %rcx
	movq	%r14, (%rax)
	movq	%rcx, 8(%rax)
	movq	%rax, (%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end9:
	.size	_ZN7Company11AddEmployeeEP8Employee, .Lfunc_end9-_ZN7Company11AddEmployeeEP8Employee
	.cfi_endproc

	.globl	_ZN7Company17PrintWithEarningsEv
	.p2align	4, 0x90
	.type	_ZN7Company17PrintWithEarningsEv,@function
_ZN7Company17PrintWithEarningsEv:       # @_ZN7Company17PrintWithEarningsEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
	subq	$16, %rsp
.Lcfi23:
	.cfi_def_cfa_offset 64
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r14, -32
.Lcfi27:
	.cfi_offset %r15, -24
.Lcfi28:
	.cfi_offset %rbp, -16
	movq	%rdi, %r15
	movq	(%r15), %rbp
	testq	%rbp, %rbp
	je	.LBB10_3
# BB#1:                                 # %.lr.ph
	leaq	15(%rsp), %r14
	.p2align	4, 0x90
.LBB10_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rbx
	movl	12(%r15), %r12d
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*24(%rax)
	movl	$_ZSt4cout, %edi
	movl	$.L.str, %esi
	movl	$9, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	cvtss2sd	%xmm0, %xmm0
	movl	$_ZSt4cout, %edi
	callq	_ZNSo9_M_insertIdEERSoT_
	movq	%rax, %rbx
	movl	$.L.str.1, %esi
	movl	$9, %edx
	movq	%rbx, %rdi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%rbx, %rdi
	movl	%r12d, %esi
	callq	_ZNSolsEi
	movb	$10, 15(%rsp)
	movl	$1, %edx
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB10_2
.LBB10_3:                               # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	_ZN7Company17PrintWithEarningsEv, .Lfunc_end10-_ZN7Company17PrintWithEarningsEv
	.cfi_endproc

	.globl	_ZN7Company7NewWeekEv
	.p2align	4, 0x90
	.type	_ZN7Company7NewWeekEv,@function
_ZN7Company7NewWeekEv:                  # @_ZN7Company7NewWeekEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 16
.Lcfi30:
	.cfi_offset %rbx, -16
	incl	12(%rdi)
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB11_3
	.p2align	4, 0x90
.LBB11_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	callq	*40(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB11_1
.LBB11_3:                               # %._crit_edge
	popq	%rbx
	retq
.Lfunc_end11:
	.size	_ZN7Company7NewWeekEv, .Lfunc_end11-_ZN7Company7NewWeekEv
	.cfi_endproc

	.globl	_ZN7Company19AcrossTheBoardRaiseEi
	.p2align	4, 0x90
	.type	_ZN7Company19AcrossTheBoardRaiseEi,@function
_ZN7Company19AcrossTheBoardRaiseEi:     # @_ZN7Company19AcrossTheBoardRaiseEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movl	%esi, %ebp
	movq	(%rdi), %rbx
	testq	%rbx, %rbx
	je	.LBB12_3
	.p2align	4, 0x90
.LBB12_1:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	(%rdi), %rax
	movl	%ebp, %esi
	callq	*32(%rax)
	movq	8(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB12_1
.LBB12_3:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN7Company19AcrossTheBoardRaiseEi, .Lfunc_end12-_ZN7Company19AcrossTheBoardRaiseEi
	.cfi_endproc

	.globl	_ZN4BossC2EPKcS1_f
	.p2align	4, 0x90
	.type	_ZN4BossC2EPKcS1_f,@function
_ZN4BossC2EPKcS1_f:                     # @_ZN4BossC2EPKcS1_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi38:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi39:
	.cfi_def_cfa_offset 48
.Lcfi40:
	.cfi_offset %rbx, -32
.Lcfi41:
	.cfi_offset %r14, -24
.Lcfi42:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	$_ZTV4Boss+16, (%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbx)
	movl	$1120403456, 24(%rbx)   # imm = 0x42C80000
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end13:
	.size	_ZN4BossC2EPKcS1_f, .Lfunc_end13-_ZN4BossC2EPKcS1_f
	.cfi_endproc

	.globl	_ZN4Boss15SetWeeklySalaryEf
	.p2align	4, 0x90
	.type	_ZN4Boss15SetWeeklySalaryEf,@function
_ZN4Boss15SetWeeklySalaryEf:            # @_ZN4Boss15SetWeeklySalaryEf
	.cfi_startproc
# BB#0:
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rdi)
	retq
.Lfunc_end14:
	.size	_ZN4Boss15SetWeeklySalaryEf, .Lfunc_end14-_ZN4Boss15SetWeeklySalaryEf
	.cfi_endproc

	.globl	_ZN4Boss8EarningsEv
	.p2align	4, 0x90
	.type	_ZN4Boss8EarningsEv,@function
_ZN4Boss8EarningsEv:                    # @_ZN4Boss8EarningsEv
	.cfi_startproc
# BB#0:
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end15:
	.size	_ZN4Boss8EarningsEv, .Lfunc_end15-_ZN4Boss8EarningsEv
	.cfi_endproc

	.globl	_ZN4Boss5PrintEv
	.p2align	4, 0x90
	.type	_ZN4Boss5PrintEv,@function
_ZN4Boss5PrintEv:                       # @_ZN4Boss5PrintEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi45:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi46:
	.cfi_def_cfa_offset 48
.Lcfi47:
	.cfi_offset %rbx, -32
.Lcfi48:
	.cfi_offset %r14, -24
.Lcfi49:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$_ZSt4cout, %edi
	movl	$.L.str.2, %esi
	movl	$21, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB16_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB16_3
.LBB16_1:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	leaq	_ZSt4cout(%rax), %rdi
	movl	_ZSt4cout+32(%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB16_3:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movb	$32, 15(%rsp)
	leaq	15(%rsp), %rsi
	movl	$_ZSt4cout, %edi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%rax, %r15
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB16_4
# BB#5:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB16_6
.LBB16_4:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r15, %rdi
	addq	%rax, %rdi
	movl	32(%r15,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB16_6:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit1
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end16:
	.size	_ZN4Boss5PrintEv, .Lfunc_end16-_ZN4Boss5PrintEv
	.cfi_endproc

	.globl	_ZN4Boss5RaiseEi
	.p2align	4, 0x90
	.type	_ZN4Boss5RaiseEi,@function
_ZN4Boss5RaiseEi:                       # @_ZN4Boss5RaiseEi
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB17_2
# BB#1:
	cvtsi2ssl	%esi, %xmm0
	mulss	24(%rdi), %xmm0
	addss	28(%rdi), %xmm0
	movss	%xmm0, 28(%rdi)
.LBB17_2:
	retq
.Lfunc_end17:
	.size	_ZN4Boss5RaiseEi, .Lfunc_end17-_ZN4Boss5RaiseEi
	.cfi_endproc

	.globl	_ZN4Boss7NewWeekEv
	.p2align	4, 0x90
	.type	_ZN4Boss7NewWeekEv,@function
_ZN4Boss7NewWeekEv:                     # @_ZN4Boss7NewWeekEv
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end18:
	.size	_ZN4Boss7NewWeekEv, .Lfunc_end18-_ZN4Boss7NewWeekEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI19_0:
	.long	1114636288              # float 60
	.text
	.globl	_ZN18CommissionedWorkerC2EPKcS1_ff
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorkerC2EPKcS1_ff,@function
_ZN18CommissionedWorkerC2EPKcS1_ff:     # @_ZN18CommissionedWorkerC2EPKcS1_ff
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 32
	subq	$32, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 64
.Lcfi54:
	.cfi_offset %rbx, -32
.Lcfi55:
	.cfi_offset %r14, -24
.Lcfi56:
	.cfi_offset %r15, -16
	movapd	%xmm1, 16(%rsp)         # 16-byte Spill
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	$_ZTV18CommissionedWorker+16, (%rbx)
	movl	$0, 36(%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbx)
	movss	.LCPI19_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	minss	%xmm2, %xmm0
	xorpd	%xmm1, %xmm1
	cmpltss	%xmm1, %xmm2
	andnps	%xmm0, %xmm2
	movss	%xmm2, 32(%rbx)
	movl	$1092616192, 24(%rbx)   # imm = 0x41200000
	addq	$32, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	_ZN18CommissionedWorkerC2EPKcS1_ff, .Lfunc_end19-_ZN18CommissionedWorkerC2EPKcS1_ff
	.cfi_endproc

	.globl	_ZN18CommissionedWorker15SetWeeklySalaryEf
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorker15SetWeeklySalaryEf,@function
_ZN18CommissionedWorker15SetWeeklySalaryEf: # @_ZN18CommissionedWorker15SetWeeklySalaryEf
	.cfi_startproc
# BB#0:
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rdi)
	retq
.Lfunc_end20:
	.size	_ZN18CommissionedWorker15SetWeeklySalaryEf, .Lfunc_end20-_ZN18CommissionedWorker15SetWeeklySalaryEf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI21_0:
	.long	1114636288              # float 60
	.text
	.globl	_ZN18CommissionedWorker17SetCommissionRateEf
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorker17SetCommissionRateEf,@function
_ZN18CommissionedWorker17SetCommissionRateEf: # @_ZN18CommissionedWorker17SetCommissionRateEf
	.cfi_startproc
# BB#0:
	movss	.LCPI21_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	minss	%xmm0, %xmm1
	xorps	%xmm2, %xmm2
	cmpltss	%xmm2, %xmm0
	andnps	%xmm1, %xmm0
	movss	%xmm0, 32(%rdi)
	retq
.Lfunc_end21:
	.size	_ZN18CommissionedWorker17SetCommissionRateEf, .Lfunc_end21-_ZN18CommissionedWorker17SetCommissionRateEf
	.cfi_endproc

	.globl	_ZN18CommissionedWorker13SalesThisWeekEf
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorker13SalesThisWeekEf,@function
_ZN18CommissionedWorker13SalesThisWeekEf: # @_ZN18CommissionedWorker13SalesThisWeekEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 36(%rdi)
	retq
.Lfunc_end22:
	.size	_ZN18CommissionedWorker13SalesThisWeekEf, .Lfunc_end22-_ZN18CommissionedWorker13SalesThisWeekEf
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI23_0:
	.quad	4636737291354636288     # double 100
	.text
	.globl	_ZN18CommissionedWorker8EarningsEv
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorker8EarningsEv,@function
_ZN18CommissionedWorker8EarningsEv:     # @_ZN18CommissionedWorker8EarningsEv
	.cfi_startproc
# BB#0:
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	cvtss2sd	%xmm1, %xmm1
	divsd	.LCPI23_0(%rip), %xmm1
	movss	36(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	cvtss2sd	%xmm2, %xmm2
	mulsd	%xmm1, %xmm2
	addsd	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	cvtsd2ss	%xmm2, %xmm0
	retq
.Lfunc_end23:
	.size	_ZN18CommissionedWorker8EarningsEv, .Lfunc_end23-_ZN18CommissionedWorker8EarningsEv
	.cfi_endproc

	.globl	_ZN18CommissionedWorker5PrintEv
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorker5PrintEv,@function
_ZN18CommissionedWorker5PrintEv:        # @_ZN18CommissionedWorker5PrintEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi57:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi58:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi60:
	.cfi_def_cfa_offset 48
.Lcfi61:
	.cfi_offset %rbx, -32
.Lcfi62:
	.cfi_offset %r14, -24
.Lcfi63:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$_ZSt4cout, %edi
	movl	$.L.str.3, %esi
	movl	$21, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB24_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB24_3
.LBB24_1:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	leaq	_ZSt4cout(%rax), %rdi
	movl	_ZSt4cout+32(%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB24_3:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movb	$32, 15(%rsp)
	leaq	15(%rsp), %rsi
	movl	$_ZSt4cout, %edi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%rax, %r15
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB24_4
# BB#5:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB24_6
.LBB24_4:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r15, %rdi
	addq	%rax, %rdi
	movl	32(%r15,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB24_6:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit1
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	_ZN18CommissionedWorker5PrintEv, .Lfunc_end24-_ZN18CommissionedWorker5PrintEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_0:
	.long	1114636288              # float 60
	.text
	.globl	_ZN18CommissionedWorker5RaiseEi
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorker5RaiseEi,@function
_ZN18CommissionedWorker5RaiseEi:        # @_ZN18CommissionedWorker5RaiseEi
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB25_2
# BB#1:
	cvtsi2ssl	%esi, %xmm0
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	addss	%xmm0, %xmm1
	movss	.LCPI25_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	minss	%xmm1, %xmm2
	xorps	%xmm3, %xmm3
	cmpltss	%xmm3, %xmm1
	andnps	%xmm2, %xmm1
	movss	%xmm1, 32(%rdi)
	mulss	24(%rdi), %xmm0
	addss	28(%rdi), %xmm0
	movss	%xmm0, 28(%rdi)
.LBB25_2:
	retq
.Lfunc_end25:
	.size	_ZN18CommissionedWorker5RaiseEi, .Lfunc_end25-_ZN18CommissionedWorker5RaiseEi
	.cfi_endproc

	.globl	_ZN18CommissionedWorker7NewWeekEv
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorker7NewWeekEv,@function
_ZN18CommissionedWorker7NewWeekEv:      # @_ZN18CommissionedWorker7NewWeekEv
	.cfi_startproc
# BB#0:
	movl	$1187205120, 36(%rdi)   # imm = 0x46C35000
	retq
.Lfunc_end26:
	.size	_ZN18CommissionedWorker7NewWeekEv, .Lfunc_end26-_ZN18CommissionedWorker7NewWeekEv
	.cfi_endproc

	.globl	_ZN10WageWorkerC2EPKcS1_f
	.p2align	4, 0x90
	.type	_ZN10WageWorkerC2EPKcS1_f,@function
_ZN10WageWorkerC2EPKcS1_f:              # @_ZN10WageWorkerC2EPKcS1_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi67:
	.cfi_def_cfa_offset 48
.Lcfi68:
	.cfi_offset %rbx, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movq	$_ZTV10WageWorker+16, (%rbx)
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end27:
	.size	_ZN10WageWorkerC2EPKcS1_f, .Lfunc_end27-_ZN10WageWorkerC2EPKcS1_f
	.cfi_endproc

	.globl	_ZN10WageWorker7SetWageEf
	.p2align	4, 0x90
	.type	_ZN10WageWorker7SetWageEf,@function
_ZN10WageWorker7SetWageEf:              # @_ZN10WageWorker7SetWageEf
	.cfi_startproc
# BB#0:
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rdi)
	retq
.Lfunc_end28:
	.size	_ZN10WageWorker7SetWageEf, .Lfunc_end28-_ZN10WageWorker7SetWageEf
	.cfi_endproc

	.globl	_ZN10WageWorker4WageEv
	.p2align	4, 0x90
	.type	_ZN10WageWorker4WageEv,@function
_ZN10WageWorker4WageEv:                 # @_ZN10WageWorker4WageEv
	.cfi_startproc
# BB#0:
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end29:
	.size	_ZN10WageWorker4WageEv, .Lfunc_end29-_ZN10WageWorker4WageEv
	.cfi_endproc

	.globl	_ZN10WageWorker5RaiseEi
	.p2align	4, 0x90
	.type	_ZN10WageWorker5RaiseEi,@function
_ZN10WageWorker5RaiseEi:                # @_ZN10WageWorker5RaiseEi
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB30_2
# BB#1:
	cvtsi2ssl	%esi, %xmm0
	mulss	24(%rdi), %xmm0
	addss	28(%rdi), %xmm0
	movss	%xmm0, 28(%rdi)
.LBB30_2:
	retq
.Lfunc_end30:
	.size	_ZN10WageWorker5RaiseEi, .Lfunc_end30-_ZN10WageWorker5RaiseEi
	.cfi_endproc

	.globl	_ZN11PieceWorkerC2EPKcS1_f
	.p2align	4, 0x90
	.type	_ZN11PieceWorkerC2EPKcS1_f,@function
_ZN11PieceWorkerC2EPKcS1_f:             # @_ZN11PieceWorkerC2EPKcS1_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 48
.Lcfi75:
	.cfi_offset %rbx, -32
.Lcfi76:
	.cfi_offset %r14, -24
.Lcfi77:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbx)
	movq	$_ZTV11PieceWorker+16, (%rbx)
	movl	$1097859072, 24(%rbx)   # imm = 0x41700000
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end31:
	.size	_ZN11PieceWorkerC2EPKcS1_f, .Lfunc_end31-_ZN11PieceWorkerC2EPKcS1_f
	.cfi_endproc

	.globl	_ZN11PieceWorker16ProducedThisWeekEi
	.p2align	4, 0x90
	.type	_ZN11PieceWorker16ProducedThisWeekEi,@function
_ZN11PieceWorker16ProducedThisWeekEi:   # @_ZN11PieceWorker16ProducedThisWeekEi
	.cfi_startproc
# BB#0:
	movl	%esi, 32(%rdi)
	retq
.Lfunc_end32:
	.size	_ZN11PieceWorker16ProducedThisWeekEi, .Lfunc_end32-_ZN11PieceWorker16ProducedThisWeekEi
	.cfi_endproc

	.globl	_ZN11PieceWorker8EarningsEv
	.p2align	4, 0x90
	.type	_ZN11PieceWorker8EarningsEv,@function
_ZN11PieceWorker8EarningsEv:            # @_ZN11PieceWorker8EarningsEv
	.cfi_startproc
# BB#0:
	cvtsi2ssl	32(%rdi), %xmm0
	mulss	28(%rdi), %xmm0
	retq
.Lfunc_end33:
	.size	_ZN11PieceWorker8EarningsEv, .Lfunc_end33-_ZN11PieceWorker8EarningsEv
	.cfi_endproc

	.globl	_ZN11PieceWorker5PrintEv
	.p2align	4, 0x90
	.type	_ZN11PieceWorker5PrintEv,@function
_ZN11PieceWorker5PrintEv:               # @_ZN11PieceWorker5PrintEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi80:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi81:
	.cfi_def_cfa_offset 48
.Lcfi82:
	.cfi_offset %rbx, -32
.Lcfi83:
	.cfi_offset %r14, -24
.Lcfi84:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$_ZSt4cout, %edi
	movl	$.L.str.4, %esi
	movl	$21, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB34_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB34_3
.LBB34_1:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	leaq	_ZSt4cout(%rax), %rdi
	movl	_ZSt4cout+32(%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB34_3:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movb	$32, 15(%rsp)
	leaq	15(%rsp), %rsi
	movl	$_ZSt4cout, %edi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%rax, %r15
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB34_4
# BB#5:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB34_6
.LBB34_4:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r15, %rdi
	addq	%rax, %rdi
	movl	32(%r15,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB34_6:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit1
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end34:
	.size	_ZN11PieceWorker5PrintEv, .Lfunc_end34-_ZN11PieceWorker5PrintEv
	.cfi_endproc

	.globl	_ZN11PieceWorker7NewWeekEv
	.p2align	4, 0x90
	.type	_ZN11PieceWorker7NewWeekEv,@function
_ZN11PieceWorker7NewWeekEv:             # @_ZN11PieceWorker7NewWeekEv
	.cfi_startproc
# BB#0:
	movl	$3, 32(%rdi)
	retq
.Lfunc_end35:
	.size	_ZN11PieceWorker7NewWeekEv, .Lfunc_end35-_ZN11PieceWorker7NewWeekEv
	.cfi_endproc

	.globl	_ZN12HourlyWorkerC2EPKcS1_f
	.p2align	4, 0x90
	.type	_ZN12HourlyWorkerC2EPKcS1_f,@function
_ZN12HourlyWorkerC2EPKcS1_f:            # @_ZN12HourlyWorkerC2EPKcS1_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi88:
	.cfi_def_cfa_offset 48
.Lcfi89:
	.cfi_offset %rbx, -32
.Lcfi90:
	.cfi_offset %r14, -24
.Lcfi91:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbx)
	movq	$_ZTV12HourlyWorker+16, (%rbx)
	movl	$1056964608, 24(%rbx)   # imm = 0x3F000000
	movl	$0, 32(%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end36:
	.size	_ZN12HourlyWorkerC2EPKcS1_f, .Lfunc_end36-_ZN12HourlyWorkerC2EPKcS1_f
	.cfi_endproc

	.globl	_ZN12HourlyWorker16SetThisWeekHoursEf
	.p2align	4, 0x90
	.type	_ZN12HourlyWorker16SetThisWeekHoursEf,@function
_ZN12HourlyWorker16SetThisWeekHoursEf:  # @_ZN12HourlyWorker16SetThisWeekHoursEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 32(%rdi)
	retq
.Lfunc_end37:
	.size	_ZN12HourlyWorker16SetThisWeekHoursEf, .Lfunc_end37-_ZN12HourlyWorker16SetThisWeekHoursEf
	.cfi_endproc

	.globl	_ZN12HourlyWorker5PrintEv
	.p2align	4, 0x90
	.type	_ZN12HourlyWorker5PrintEv,@function
_ZN12HourlyWorker5PrintEv:              # @_ZN12HourlyWorker5PrintEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi94:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi95:
	.cfi_def_cfa_offset 48
.Lcfi96:
	.cfi_offset %rbx, -32
.Lcfi97:
	.cfi_offset %r14, -24
.Lcfi98:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movl	$_ZSt4cout, %edi
	movl	$.L.str.5, %esi
	movl	$21, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	8(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB38_1
# BB#2:
	movq	%rbx, %rdi
	callq	strlen
	movl	$_ZSt4cout, %edi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB38_3
.LBB38_1:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	leaq	_ZSt4cout(%rax), %rdi
	movl	_ZSt4cout+32(%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB38_3:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit
	movb	$32, 15(%rsp)
	leaq	15(%rsp), %rsi
	movl	$_ZSt4cout, %edi
	movl	$1, %edx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	movq	%rax, %r15
	movq	16(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB38_4
# BB#5:
	movq	%rbx, %rdi
	callq	strlen
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	%rax, %rdx
	callq	_ZSt16__ostream_insertIcSt11char_traitsIcEERSt13basic_ostreamIT_T0_ES6_PKS3_l
	jmp	.LBB38_6
.LBB38_4:
	movq	(%r15), %rax
	movq	-24(%rax), %rax
	movq	%r15, %rdi
	addq	%rax, %rdi
	movl	32(%r15,%rax), %esi
	orl	$1, %esi
	callq	_ZNSt9basic_iosIcSt11char_traitsIcEE5clearESt12_Ios_Iostate
.LBB38_6:                               # %_ZStlsISt11char_traitsIcEERSt13basic_ostreamIcT_ES5_PKc.exit1
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end38:
	.size	_ZN12HourlyWorker5PrintEv, .Lfunc_end38-_ZN12HourlyWorker5PrintEv
	.cfi_endproc

	.globl	_ZN12HourlyWorker5RaiseEi
	.p2align	4, 0x90
	.type	_ZN12HourlyWorker5RaiseEi,@function
_ZN12HourlyWorker5RaiseEi:              # @_ZN12HourlyWorker5RaiseEi
	.cfi_startproc
# BB#0:
	testl	%esi, %esi
	jle	.LBB39_2
# BB#1:
	cvtsi2ssl	%esi, %xmm0
	mulss	24(%rdi), %xmm0
	addss	28(%rdi), %xmm0
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rdi)
.LBB39_2:
	retq
.Lfunc_end39:
	.size	_ZN12HourlyWorker5RaiseEi, .Lfunc_end39-_ZN12HourlyWorker5RaiseEi
	.cfi_endproc

	.globl	_ZN12HourlyWorker7NewWeekEv
	.p2align	4, 0x90
	.type	_ZN12HourlyWorker7NewWeekEv,@function
_ZN12HourlyWorker7NewWeekEv:            # @_ZN12HourlyWorker7NewWeekEv
	.cfi_startproc
# BB#0:
	movl	$1110441984, 32(%rdi)   # imm = 0x42300000
	retq
.Lfunc_end40:
	.size	_ZN12HourlyWorker7NewWeekEv, .Lfunc_end40-_ZN12HourlyWorker7NewWeekEv
	.cfi_endproc

	.globl	_ZN22HourlyWorkerNoOvertimeC2EPKcS1_f
	.p2align	4, 0x90
	.type	_ZN22HourlyWorkerNoOvertimeC2EPKcS1_f,@function
_ZN22HourlyWorkerNoOvertimeC2EPKcS1_f:  # @_ZN22HourlyWorkerNoOvertimeC2EPKcS1_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi102:
	.cfi_def_cfa_offset 48
.Lcfi103:
	.cfi_offset %rbx, -32
.Lcfi104:
	.cfi_offset %r14, -24
.Lcfi105:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbx)
	movl	$1056964608, 24(%rbx)   # imm = 0x3F000000
	movl	$0, 32(%rbx)
	movq	$_ZTV22HourlyWorkerNoOvertime+16, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end41:
	.size	_ZN22HourlyWorkerNoOvertimeC2EPKcS1_f, .Lfunc_end41-_ZN22HourlyWorkerNoOvertimeC2EPKcS1_f
	.cfi_endproc

	.globl	_ZN22HourlyWorkerNoOvertime8EarningsEv
	.p2align	4, 0x90
	.type	_ZN22HourlyWorkerNoOvertime8EarningsEv,@function
_ZN22HourlyWorkerNoOvertime8EarningsEv: # @_ZN22HourlyWorkerNoOvertime8EarningsEv
	.cfi_startproc
# BB#0:
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	32(%rdi), %xmm0
	retq
.Lfunc_end42:
	.size	_ZN22HourlyWorkerNoOvertime8EarningsEv, .Lfunc_end42-_ZN22HourlyWorkerNoOvertime8EarningsEv
	.cfi_endproc

	.globl	_ZN20HourlyWorkerOvertimeC2EPKcS1_f
	.p2align	4, 0x90
	.type	_ZN20HourlyWorkerOvertimeC2EPKcS1_f,@function
_ZN20HourlyWorkerOvertimeC2EPKcS1_f:    # @_ZN20HourlyWorkerOvertimeC2EPKcS1_f
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi106:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi107:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 48
.Lcfi110:
	.cfi_offset %rbx, -32
.Lcfi111:
	.cfi_offset %r14, -24
.Lcfi112:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	%r15, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 8(%rbx)
	movq	%rax, %rdi
	movq	%r15, %rsi
	callq	strcpy
	movq	%r14, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
	callq	_Znam
	movq	%rax, 16(%rbx)
	movq	%rax, %rdi
	movq	%r14, %rsi
	callq	strcpy
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	maxsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbx)
	movl	$1056964608, 24(%rbx)   # imm = 0x3F000000
	movl	$0, 32(%rbx)
	movq	$_ZTV20HourlyWorkerOvertime+16, (%rbx)
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end43:
	.size	_ZN20HourlyWorkerOvertimeC2EPKcS1_f, .Lfunc_end43-_ZN20HourlyWorkerOvertimeC2EPKcS1_f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI44_0:
	.long	1109393408              # float 40
.LCPI44_1:
	.long	3256877056              # float -40
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI44_2:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	_ZN20HourlyWorkerOvertime8EarningsEv
	.p2align	4, 0x90
	.type	_ZN20HourlyWorkerOvertime8EarningsEv,@function
_ZN20HourlyWorkerOvertime8EarningsEv:   # @_ZN20HourlyWorkerOvertime8EarningsEv
	.cfi_startproc
# BB#0:
	movss	28(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	ucomiss	.LCPI44_0(%rip), %xmm1
	ja	.LBB44_1
# BB#2:
	xorpd	%xmm2, %xmm2
	jmp	.LBB44_3
.LBB44_1:
	movss	.LCPI44_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	addss	%xmm1, %xmm2
	cvtss2sd	%xmm2, %xmm2
.LBB44_3:
	mulss	%xmm0, %xmm1
	cvtss2sd	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm0
	mulsd	.LCPI44_2(%rip), %xmm0
	mulsd	%xmm2, %xmm0
	addsd	%xmm1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	retq
.Lfunc_end44:
	.size	_ZN20HourlyWorkerOvertime8EarningsEv, .Lfunc_end44-_ZN20HourlyWorkerOvertime8EarningsEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI45_0:
	.long	1114636288              # float 60
.LCPI45_2:
	.long	0                       # float 0
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI45_1:
	.quad	0                       # double 0
	.text
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi117:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi118:
	.cfi_def_cfa_offset 56
	subq	$232, %rsp
.Lcfi119:
	.cfi_def_cfa_offset 288
.Lcfi120:
	.cfi_offset %rbx, -56
.Lcfi121:
	.cfi_offset %r12, -48
.Lcfi122:
	.cfi_offset %r13, -40
.Lcfi123:
	.cfi_offset %r14, -32
.Lcfi124:
	.cfi_offset %r15, -24
.Lcfi125:
	.cfi_offset %rbp, -16
	cmpl	$2, %edi
	jne	.LBB45_1
# BB#2:
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	orl	$1028, _ZSt4cout+24(%rax) # imm = 0x404
	movq	_ZSt4cout(%rip), %rax
	movq	-24(%rax), %rax
	movq	$2, _ZSt4cout+8(%rax)
	movq	8(%rsi), %rdi
	xorl	%ebx, %ebx
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %r13
	movl	$16, %edi
	callq	_Znwm
	xorps	%xmm0, %xmm0
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movups	%xmm0, (%rax)
	leaq	12(%rsp), %rsi
	leaq	144(%rsp), %rdx
	leaq	64(%rsp), %rcx
	leaq	8(%rsp), %r8
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	scanf
	cmpl	$4, %eax
	movl	$0, %r15d
	jne	.LBB45_7
# BB#3:                                 # %.lr.ph50
	movq	%r13, 56(%rsp)          # 8-byte Spill
	leaq	144(%rsp), %r14
	leaq	64(%rsp), %r13
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB45_4:                               # =>This Inner Loop Header: Depth=1
	movl	12(%rsp), %edx
	leal	-1(%rdx), %eax
	cmpl	$6, %eax
	ja	.LBB45_37
# BB#5:                                 #   in Loop: Header=BB45_4 Depth=1
	jmpq	*.LJTI45_0(,%rax,8)
.LBB45_15:                              #   in Loop: Header=BB45_4 Depth=1
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	$_ZTV8Employee+16, (%rbp)
	movq	%r14, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp20:
	callq	_Znam
.Ltmp21:
# BB#16:                                # %.noexc31
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 8(%rbp)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp22:
	callq	_Znam
.Ltmp23:
# BB#17:                                # %_ZN4BossC2EPKcS1_f.exit
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 16(%rbp)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	$_ZTV4Boss+16, (%rbp)
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	.LCPI45_1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbp)
	movl	$1120403456, 24(%rbp)   # imm = 0x42C80000
	jmp	.LBB45_35
.LBB45_32:                              #   in Loop: Header=BB45_4 Depth=1
	movl	$.L.str.9, %edi
	xorl	%eax, %eax
	leaq	52(%rsp), %rsi
	callq	scanf
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	movss	52(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movq	$_ZTV8Employee+16, (%rbp)
	movq	%r14, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp0:
	callq	_Znam
.Ltmp1:
# BB#33:                                # %.noexc
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 8(%rbp)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp2:
	callq	_Znam
.Ltmp3:
# BB#34:                                # %_ZN18CommissionedWorkerC2EPKcS1_ff.exit
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 16(%rbp)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movq	$_ZTV18CommissionedWorker+16, (%rbp)
	movl	$0, 36(%rbp)
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	.LCPI45_1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbp)
	movss	.LCPI45_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	minss	%xmm1, %xmm0
	cmpltss	.LCPI45_2, %xmm1
	andnps	%xmm0, %xmm1
	movss	%xmm1, 32(%rbp)
	movl	$1092616192, 24(%rbp)   # imm = 0x41200000
	jmp	.LBB45_35
.LBB45_28:                              #   in Loop: Header=BB45_4 Depth=1
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	$_ZTV8Employee+16, (%rbp)
	movq	%r14, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp5:
	callq	_Znam
.Ltmp6:
# BB#29:                                # %.noexc37
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 8(%rbp)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp7:
	callq	_Znam
.Ltmp8:
# BB#30:                                # %_ZN11PieceWorkerC2EPKcS1_f.exit
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 16(%rbp)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	.LCPI45_1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbp)
	movq	$_ZTV11PieceWorker+16, (%rbp)
	movl	$1097859072, 24(%rbp)   # imm = 0x41700000
	jmp	.LBB45_35
.LBB45_24:                              #   in Loop: Header=BB45_4 Depth=1
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	$_ZTV8Employee+16, (%rbp)
	movq	%r14, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp10:
	callq	_Znam
.Ltmp11:
# BB#25:                                # %.noexc35
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 8(%rbp)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp12:
	callq	_Znam
.Ltmp13:
# BB#26:                                # %_ZN20HourlyWorkerOvertimeC2EPKcS1_f.exit
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 16(%rbp)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	.LCPI45_1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbp)
	movl	$1056964608, 24(%rbp)   # imm = 0x3F000000
	movl	$0, 32(%rbp)
	movq	$_ZTV20HourlyWorkerOvertime+16, (%rbp)
	jmp	.LBB45_35
.LBB45_20:                              #   in Loop: Header=BB45_4 Depth=1
	movl	$40, %edi
	callq	_Znwm
	movq	%rax, %rbp
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	movq	$_ZTV8Employee+16, (%rbp)
	movq	%r14, %rbx
	movq	%rbx, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp15:
	callq	_Znam
.Ltmp16:
# BB#21:                                # %.noexc33
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 8(%rbp)
	movq	%rax, %rdi
	movq	%rbx, %rsi
	callq	strcpy
	movq	%r13, %rdi
	callq	strlen
	leaq	1(%rax), %rdi
.Ltmp17:
	callq	_Znam
.Ltmp18:
# BB#22:                                # %_ZN22HourlyWorkerNoOvertimeC2EPKcS1_f.exit
                                        #   in Loop: Header=BB45_4 Depth=1
	movq	%rax, 16(%rbp)
	movq	%rax, %rdi
	movq	%r13, %rsi
	callq	strcpy
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	cvtss2sd	%xmm0, %xmm0
	maxsd	.LCPI45_1, %xmm0
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, 28(%rbp)
	movl	$1056964608, 24(%rbp)   # imm = 0x3F000000
	movl	$0, 32(%rbp)
	movq	$_ZTV22HourlyWorkerNoOvertime+16, (%rbp)
	.p2align	4, 0x90
.LBB45_35:                              #   in Loop: Header=BB45_4 Depth=1
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %r15
	movq	%rbp, (%r15)
	movq	%r12, 8(%r15)
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%r15, (%rax)
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	leaq	12(%rsp), %rsi
	movq	%r14, %rdx
	movq	%r13, %rcx
	leaq	8(%rsp), %r8
	callq	scanf
	cmpl	$4, %eax
	movq	%r15, %r12
	je	.LBB45_4
# BB#6:                                 # %.preheader.loopexit
	xorl	%ebx, %ebx
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB45_7:                               # %.preheader
	testl	%r13d, %r13d
	jle	.LBB45_43
# BB#8:                                 # %.lr.ph
	xorl	%ebx, %ebx
	movq	%r15, %r14
	.p2align	4, 0x90
.LBB45_9:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB45_11 Depth 2
                                        #     Child Loop BB45_39 Depth 2
	movl	%ebx, %ebp
	leal	1(%rbp), %ebx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%ebx, 12(%rax)
	testq	%r14, %r14
	je	.LBB45_12
# BB#10:                                # %.lr.ph.i29.preheader
                                        #   in Loop: Header=BB45_9 Depth=1
	movq	%r14, %r12
	.p2align	4, 0x90
.LBB45_11:                              # %.lr.ph.i29
                                        #   Parent Loop BB45_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12), %rdi
	movq	(%rdi), %rax
	callq	*40(%rax)
	movq	8(%r12), %r12
	testq	%r12, %r12
	jne	.LBB45_11
.LBB45_12:                              # %_ZN7Company7NewWeekEv.exit
                                        #   in Loop: Header=BB45_9 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZN7Company17PrintWithEarningsEv
	movslq	%ebp, %rax
	imulq	$1717986919, %rax, %rax # imm = 0x66666667
	movq	%rax, %rcx
	shrq	$63, %rcx
	sarq	$34, %rax
	addl	%ecx, %eax
	addl	%eax, %eax
	leal	(%rax,%rax,4), %eax
	cmpl	%eax, %ebp
	jne	.LBB45_41
# BB#13:                                #   in Loop: Header=BB45_9 Depth=1
	testq	%r15, %r15
	je	.LBB45_14
# BB#38:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB45_9 Depth=1
	movq	%r15, %rbp
	.p2align	4, 0x90
.LBB45_39:                              # %.lr.ph.i
                                        #   Parent Loop BB45_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rbp), %rdi
	movq	(%rdi), %rax
	movl	$1, %esi
	callq	*32(%rax)
	movq	8(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB45_39
# BB#40:                                #   in Loop: Header=BB45_9 Depth=1
	movq	%r15, %r14
	cmpl	%r13d, %ebx
	jne	.LBB45_9
	jmp	.LBB45_42
.LBB45_14:                              #   in Loop: Header=BB45_9 Depth=1
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB45_41:                              # %_ZN7Company19AcrossTheBoardRaiseEi.exit
                                        #   in Loop: Header=BB45_9 Depth=1
	cmpl	%r13d, %ebx
	jne	.LBB45_9
.LBB45_42:
	xorl	%ebx, %ebx
	jmp	.LBB45_43
.LBB45_37:
	movq	stderr(%rip), %rdi
	movl	$.L.str.10, %esi
	xorl	%eax, %eax
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	fprintf
	movl	$-1, %ebx
.LBB45_43:                              # %.loopexit
	movl	%ebx, %eax
	addq	$232, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB45_1:
	movq	stderr(%rip), %rdi
	movl	$.L.str.6, %esi
	movl	$.L.str.7, %edx
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %ebx
	jmp	.LBB45_43
.LBB45_23:
.Ltmp19:
	jmp	.LBB45_19
.LBB45_27:
.Ltmp14:
	jmp	.LBB45_19
.LBB45_31:
.Ltmp9:
	jmp	.LBB45_19
.LBB45_36:
.Ltmp4:
	jmp	.LBB45_19
.LBB45_18:
.Ltmp24:
.LBB45_19:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end45:
	.size	main, .Lfunc_end45-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI45_0:
	.quad	.LBB45_15
	.quad	.LBB45_32
	.quad	.LBB45_37
	.quad	.LBB45_28
	.quad	.LBB45_37
	.quad	.LBB45_24
	.quad	.LBB45_20
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table45:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\222\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp20-.Lfunc_begin0   #   Call between .Lfunc_begin0 and .Ltmp20
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 2 <<
	.long	.Ltmp23-.Ltmp20         #   Call between .Ltmp20 and .Ltmp23
	.long	.Ltmp24-.Lfunc_begin0   #     jumps to .Ltmp24
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 3 <<
	.long	.Ltmp0-.Ltmp23          #   Call between .Ltmp23 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 5 <<
	.long	.Ltmp5-.Ltmp3           #   Call between .Ltmp3 and .Ltmp5
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 6 <<
	.long	.Ltmp8-.Ltmp5           #   Call between .Ltmp5 and .Ltmp8
	.long	.Ltmp9-.Lfunc_begin0    #     jumps to .Ltmp9
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 7 <<
	.long	.Ltmp10-.Ltmp8          #   Call between .Ltmp8 and .Ltmp10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp10-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp13-.Ltmp10         #   Call between .Ltmp10 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin0   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp18-.Ltmp15         #   Call between .Ltmp15 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp18-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Lfunc_end45-.Ltmp18    #   Call between .Ltmp18 and .Lfunc_end45
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN4BossD0Ev,"axG",@progbits,_ZN4BossD0Ev,comdat
	.weak	_ZN4BossD0Ev
	.p2align	4, 0x90
	.type	_ZN4BossD0Ev,@function
_ZN4BossD0Ev:                           # @_ZN4BossD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi126:
	.cfi_def_cfa_offset 16
.Lcfi127:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB46_2
# BB#1:
	callq	_ZdaPv
.LBB46_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB46_4
# BB#3:
	callq	_ZdaPv
.LBB46_4:                               # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end46:
	.size	_ZN4BossD0Ev, .Lfunc_end46-_ZN4BossD0Ev
	.cfi_endproc

	.section	.text._ZN18CommissionedWorkerD0Ev,"axG",@progbits,_ZN18CommissionedWorkerD0Ev,comdat
	.weak	_ZN18CommissionedWorkerD0Ev
	.p2align	4, 0x90
	.type	_ZN18CommissionedWorkerD0Ev,@function
_ZN18CommissionedWorkerD0Ev:            # @_ZN18CommissionedWorkerD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi128:
	.cfi_def_cfa_offset 16
.Lcfi129:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB47_2
# BB#1:
	callq	_ZdaPv
.LBB47_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB47_4
# BB#3:
	callq	_ZdaPv
.LBB47_4:                               # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end47:
	.size	_ZN18CommissionedWorkerD0Ev, .Lfunc_end47-_ZN18CommissionedWorkerD0Ev
	.cfi_endproc

	.section	.text._ZN10WageWorkerD0Ev,"axG",@progbits,_ZN10WageWorkerD0Ev,comdat
	.weak	_ZN10WageWorkerD0Ev
	.p2align	4, 0x90
	.type	_ZN10WageWorkerD0Ev,@function
_ZN10WageWorkerD0Ev:                    # @_ZN10WageWorkerD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 16
.Lcfi131:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB48_2
# BB#1:
	callq	_ZdaPv
.LBB48_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB48_4
# BB#3:
	callq	_ZdaPv
.LBB48_4:                               # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end48:
	.size	_ZN10WageWorkerD0Ev, .Lfunc_end48-_ZN10WageWorkerD0Ev
	.cfi_endproc

	.section	.text._ZN11PieceWorkerD0Ev,"axG",@progbits,_ZN11PieceWorkerD0Ev,comdat
	.weak	_ZN11PieceWorkerD0Ev
	.p2align	4, 0x90
	.type	_ZN11PieceWorkerD0Ev,@function
_ZN11PieceWorkerD0Ev:                   # @_ZN11PieceWorkerD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 16
.Lcfi133:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB49_2
# BB#1:
	callq	_ZdaPv
.LBB49_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB49_4
# BB#3:
	callq	_ZdaPv
.LBB49_4:                               # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end49:
	.size	_ZN11PieceWorkerD0Ev, .Lfunc_end49-_ZN11PieceWorkerD0Ev
	.cfi_endproc

	.text
	.globl	_ZN8EmployeeD2Ev
	.p2align	4, 0x90
	.type	_ZN8EmployeeD2Ev,@function
_ZN8EmployeeD2Ev:                       # @_ZN8EmployeeD2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi134:
	.cfi_def_cfa_offset 16
.Lcfi135:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB50_2
# BB#1:
	callq	_ZdaPv
.LBB50_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB50_3
# BB#4:
	popq	%rbx
	jmp	_ZdaPv                  # TAILCALL
.LBB50_3:
	popq	%rbx
	retq
.Lfunc_end50:
	.size	_ZN8EmployeeD2Ev, .Lfunc_end50-_ZN8EmployeeD2Ev
	.cfi_endproc

	.section	.text._ZN12HourlyWorkerD0Ev,"axG",@progbits,_ZN12HourlyWorkerD0Ev,comdat
	.weak	_ZN12HourlyWorkerD0Ev
	.p2align	4, 0x90
	.type	_ZN12HourlyWorkerD0Ev,@function
_ZN12HourlyWorkerD0Ev:                  # @_ZN12HourlyWorkerD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 16
.Lcfi137:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB51_2
# BB#1:
	callq	_ZdaPv
.LBB51_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB51_4
# BB#3:
	callq	_ZdaPv
.LBB51_4:                               # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end51:
	.size	_ZN12HourlyWorkerD0Ev, .Lfunc_end51-_ZN12HourlyWorkerD0Ev
	.cfi_endproc

	.section	.text._ZN22HourlyWorkerNoOvertimeD0Ev,"axG",@progbits,_ZN22HourlyWorkerNoOvertimeD0Ev,comdat
	.weak	_ZN22HourlyWorkerNoOvertimeD0Ev
	.p2align	4, 0x90
	.type	_ZN22HourlyWorkerNoOvertimeD0Ev,@function
_ZN22HourlyWorkerNoOvertimeD0Ev:        # @_ZN22HourlyWorkerNoOvertimeD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi138:
	.cfi_def_cfa_offset 16
.Lcfi139:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB52_2
# BB#1:
	callq	_ZdaPv
.LBB52_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB52_4
# BB#3:
	callq	_ZdaPv
.LBB52_4:                               # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end52:
	.size	_ZN22HourlyWorkerNoOvertimeD0Ev, .Lfunc_end52-_ZN22HourlyWorkerNoOvertimeD0Ev
	.cfi_endproc

	.section	.text._ZN20HourlyWorkerOvertimeD0Ev,"axG",@progbits,_ZN20HourlyWorkerOvertimeD0Ev,comdat
	.weak	_ZN20HourlyWorkerOvertimeD0Ev
	.p2align	4, 0x90
	.type	_ZN20HourlyWorkerOvertimeD0Ev,@function
_ZN20HourlyWorkerOvertimeD0Ev:          # @_ZN20HourlyWorkerOvertimeD0Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi140:
	.cfi_def_cfa_offset 16
.Lcfi141:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	$_ZTV8Employee+16, (%rbx)
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB53_2
# BB#1:
	callq	_ZdaPv
.LBB53_2:
	movq	16(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB53_4
# BB#3:
	callq	_ZdaPv
.LBB53_4:                               # %_ZN8EmployeeD2Ev.exit
	movq	%rbx, %rdi
	popq	%rbx
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end53:
	.size	_ZN20HourlyWorkerOvertimeD0Ev, .Lfunc_end53-_ZN20HourlyWorkerOvertimeD0Ev
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_driver.ii,@function
_GLOBAL__sub_I_driver.ii:               # @_GLOBAL__sub_I_driver.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end54:
	.size	_GLOBAL__sub_I_driver.ii, .Lfunc_end54-_GLOBAL__sub_I_driver.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	_ZTV8Employee,@object   # @_ZTV8Employee
	.section	.rodata,"a",@progbits
	.globl	_ZTV8Employee
	.p2align	3
_ZTV8Employee:
	.quad	0
	.quad	_ZTI8Employee
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN8EmployeeD0Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.size	_ZTV8Employee, 64

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	" earned $"
	.size	.L.str, 10

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	" in week "
	.size	.L.str.1, 10

	.type	_ZTV4Boss,@object       # @_ZTV4Boss
	.section	.rodata,"a",@progbits
	.globl	_ZTV4Boss
	.p2align	3
_ZTV4Boss:
	.quad	0
	.quad	_ZTI4Boss
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN4BossD0Ev
	.quad	_ZN4Boss8EarningsEv
	.quad	_ZN4Boss5PrintEv
	.quad	_ZN4Boss5RaiseEi
	.quad	_ZN4Boss7NewWeekEv
	.size	_ZTV4Boss, 64

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"               Boss: "
	.size	.L.str.2, 22

	.type	_ZTV18CommissionedWorker,@object # @_ZTV18CommissionedWorker
	.section	.rodata,"a",@progbits
	.globl	_ZTV18CommissionedWorker
	.p2align	3
_ZTV18CommissionedWorker:
	.quad	0
	.quad	_ZTI18CommissionedWorker
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN18CommissionedWorkerD0Ev
	.quad	_ZN18CommissionedWorker8EarningsEv
	.quad	_ZN18CommissionedWorker5PrintEv
	.quad	_ZN18CommissionedWorker5RaiseEi
	.quad	_ZN18CommissionedWorker7NewWeekEv
	.size	_ZTV18CommissionedWorker, 64

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"Commissioned Worker: "
	.size	.L.str.3, 22

	.type	_ZTV10WageWorker,@object # @_ZTV10WageWorker
	.section	.rodata,"a",@progbits
	.globl	_ZTV10WageWorker
	.p2align	3
_ZTV10WageWorker:
	.quad	0
	.quad	_ZTI10WageWorker
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN10WageWorkerD0Ev
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN10WageWorker5RaiseEi
	.quad	__cxa_pure_virtual
	.size	_ZTV10WageWorker, 64

	.type	_ZTV11PieceWorker,@object # @_ZTV11PieceWorker
	.globl	_ZTV11PieceWorker
	.p2align	3
_ZTV11PieceWorker:
	.quad	0
	.quad	_ZTI11PieceWorker
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN11PieceWorkerD0Ev
	.quad	_ZN11PieceWorker8EarningsEv
	.quad	_ZN11PieceWorker5PrintEv
	.quad	_ZN10WageWorker5RaiseEi
	.quad	_ZN11PieceWorker7NewWeekEv
	.size	_ZTV11PieceWorker, 64

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"       Piece Worker: "
	.size	.L.str.4, 22

	.type	_ZTV12HourlyWorker,@object # @_ZTV12HourlyWorker
	.section	.rodata,"a",@progbits
	.globl	_ZTV12HourlyWorker
	.p2align	3
_ZTV12HourlyWorker:
	.quad	0
	.quad	_ZTI12HourlyWorker
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN12HourlyWorkerD0Ev
	.quad	__cxa_pure_virtual
	.quad	_ZN12HourlyWorker5PrintEv
	.quad	_ZN12HourlyWorker5RaiseEi
	.quad	_ZN12HourlyWorker7NewWeekEv
	.size	_ZTV12HourlyWorker, 64

	.type	.L.str.5,@object        # @.str.5
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.5:
	.asciz	"      Hourly Worker: "
	.size	.L.str.5, 22

	.type	_ZTV22HourlyWorkerNoOvertime,@object # @_ZTV22HourlyWorkerNoOvertime
	.section	.rodata,"a",@progbits
	.globl	_ZTV22HourlyWorkerNoOvertime
	.p2align	3
_ZTV22HourlyWorkerNoOvertime:
	.quad	0
	.quad	_ZTI22HourlyWorkerNoOvertime
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN22HourlyWorkerNoOvertimeD0Ev
	.quad	_ZN22HourlyWorkerNoOvertime8EarningsEv
	.quad	_ZN12HourlyWorker5PrintEv
	.quad	_ZN12HourlyWorker5RaiseEi
	.quad	_ZN12HourlyWorker7NewWeekEv
	.size	_ZTV22HourlyWorkerNoOvertime, 64

	.type	_ZTV20HourlyWorkerOvertime,@object # @_ZTV20HourlyWorkerOvertime
	.globl	_ZTV20HourlyWorkerOvertime
	.p2align	3
_ZTV20HourlyWorkerOvertime:
	.quad	0
	.quad	_ZTI20HourlyWorkerOvertime
	.quad	_ZN8EmployeeD2Ev
	.quad	_ZN20HourlyWorkerOvertimeD0Ev
	.quad	_ZN20HourlyWorkerOvertime8EarningsEv
	.quad	_ZN12HourlyWorker5PrintEv
	.quad	_ZN12HourlyWorker5RaiseEi
	.quad	_ZN12HourlyWorker7NewWeekEv
	.size	_ZTV20HourlyWorkerOvertime, 64

	.type	.L.str.6,@object        # @.str.6
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.6:
	.asciz	"usage: %s <number_of_weeks>\n"
	.size	.L.str.6, 29

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"employ"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%d%s%s%f"
	.size	.L.str.8, 9

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"%f"
	.size	.L.str.9, 3

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"INVALID EMPLOYEE CODE(%d)\n"
	.size	.L.str.10, 27

	.type	_ZTS8Employee,@object   # @_ZTS8Employee
	.section	.rodata,"a",@progbits
	.globl	_ZTS8Employee
_ZTS8Employee:
	.asciz	"8Employee"
	.size	_ZTS8Employee, 10

	.type	_ZTI8Employee,@object   # @_ZTI8Employee
	.globl	_ZTI8Employee
	.p2align	3
_ZTI8Employee:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8Employee
	.size	_ZTI8Employee, 16

	.type	_ZTS4Boss,@object       # @_ZTS4Boss
	.globl	_ZTS4Boss
_ZTS4Boss:
	.asciz	"4Boss"
	.size	_ZTS4Boss, 6

	.type	_ZTI4Boss,@object       # @_ZTI4Boss
	.globl	_ZTI4Boss
	.p2align	4
_ZTI4Boss:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS4Boss
	.quad	_ZTI8Employee
	.size	_ZTI4Boss, 24

	.type	_ZTS18CommissionedWorker,@object # @_ZTS18CommissionedWorker
	.globl	_ZTS18CommissionedWorker
	.p2align	4
_ZTS18CommissionedWorker:
	.asciz	"18CommissionedWorker"
	.size	_ZTS18CommissionedWorker, 21

	.type	_ZTI18CommissionedWorker,@object # @_ZTI18CommissionedWorker
	.globl	_ZTI18CommissionedWorker
	.p2align	4
_ZTI18CommissionedWorker:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS18CommissionedWorker
	.quad	_ZTI8Employee
	.size	_ZTI18CommissionedWorker, 24

	.type	_ZTS10WageWorker,@object # @_ZTS10WageWorker
	.globl	_ZTS10WageWorker
_ZTS10WageWorker:
	.asciz	"10WageWorker"
	.size	_ZTS10WageWorker, 13

	.type	_ZTI10WageWorker,@object # @_ZTI10WageWorker
	.globl	_ZTI10WageWorker
	.p2align	4
_ZTI10WageWorker:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10WageWorker
	.quad	_ZTI8Employee
	.size	_ZTI10WageWorker, 24

	.type	_ZTS11PieceWorker,@object # @_ZTS11PieceWorker
	.globl	_ZTS11PieceWorker
_ZTS11PieceWorker:
	.asciz	"11PieceWorker"
	.size	_ZTS11PieceWorker, 14

	.type	_ZTI11PieceWorker,@object # @_ZTI11PieceWorker
	.globl	_ZTI11PieceWorker
	.p2align	4
_ZTI11PieceWorker:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS11PieceWorker
	.quad	_ZTI10WageWorker
	.size	_ZTI11PieceWorker, 24

	.type	_ZTS12HourlyWorker,@object # @_ZTS12HourlyWorker
	.globl	_ZTS12HourlyWorker
_ZTS12HourlyWorker:
	.asciz	"12HourlyWorker"
	.size	_ZTS12HourlyWorker, 15

	.type	_ZTI12HourlyWorker,@object # @_ZTI12HourlyWorker
	.globl	_ZTI12HourlyWorker
	.p2align	4
_ZTI12HourlyWorker:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS12HourlyWorker
	.quad	_ZTI10WageWorker
	.size	_ZTI12HourlyWorker, 24

	.type	_ZTS22HourlyWorkerNoOvertime,@object # @_ZTS22HourlyWorkerNoOvertime
	.globl	_ZTS22HourlyWorkerNoOvertime
	.p2align	4
_ZTS22HourlyWorkerNoOvertime:
	.asciz	"22HourlyWorkerNoOvertime"
	.size	_ZTS22HourlyWorkerNoOvertime, 25

	.type	_ZTI22HourlyWorkerNoOvertime,@object # @_ZTI22HourlyWorkerNoOvertime
	.globl	_ZTI22HourlyWorkerNoOvertime
	.p2align	4
_ZTI22HourlyWorkerNoOvertime:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS22HourlyWorkerNoOvertime
	.quad	_ZTI12HourlyWorker
	.size	_ZTI22HourlyWorkerNoOvertime, 24

	.type	_ZTS20HourlyWorkerOvertime,@object # @_ZTS20HourlyWorkerOvertime
	.globl	_ZTS20HourlyWorkerOvertime
	.p2align	4
_ZTS20HourlyWorkerOvertime:
	.asciz	"20HourlyWorkerOvertime"
	.size	_ZTS20HourlyWorkerOvertime, 23

	.type	_ZTI20HourlyWorkerOvertime,@object # @_ZTI20HourlyWorkerOvertime
	.globl	_ZTI20HourlyWorkerOvertime
	.p2align	4
_ZTI20HourlyWorkerOvertime:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20HourlyWorkerOvertime
	.quad	_ZTI12HourlyWorker
	.size	_ZTI20HourlyWorkerOvertime, 24

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_driver.ii

	.globl	_ZN8EmployeeD1Ev
	.type	_ZN8EmployeeD1Ev,@function
_ZN8EmployeeD1Ev = _ZN8EmployeeD2Ev
	.globl	_ZN12EmployeeNodeC1EP8EmployeePS_
	.type	_ZN12EmployeeNodeC1EP8EmployeePS_,@function
_ZN12EmployeeNodeC1EP8EmployeePS_ = _ZN12EmployeeNodeC2EP8EmployeePS_
	.globl	_ZN7CompanyC1Ev
	.type	_ZN7CompanyC1Ev,@function
_ZN7CompanyC1Ev = _ZN7CompanyC2Ev
	.globl	_ZN4BossC1EPKcS1_f
	.type	_ZN4BossC1EPKcS1_f,@function
_ZN4BossC1EPKcS1_f = _ZN4BossC2EPKcS1_f
	.globl	_ZN18CommissionedWorkerC1EPKcS1_ff
	.type	_ZN18CommissionedWorkerC1EPKcS1_ff,@function
_ZN18CommissionedWorkerC1EPKcS1_ff = _ZN18CommissionedWorkerC2EPKcS1_ff
	.globl	_ZN11PieceWorkerC1EPKcS1_f
	.type	_ZN11PieceWorkerC1EPKcS1_f,@function
_ZN11PieceWorkerC1EPKcS1_f = _ZN11PieceWorkerC2EPKcS1_f
	.globl	_ZN22HourlyWorkerNoOvertimeC1EPKcS1_f
	.type	_ZN22HourlyWorkerNoOvertimeC1EPKcS1_f,@function
_ZN22HourlyWorkerNoOvertimeC1EPKcS1_f = _ZN22HourlyWorkerNoOvertimeC2EPKcS1_f
	.globl	_ZN20HourlyWorkerOvertimeC1EPKcS1_f
	.type	_ZN20HourlyWorkerOvertimeC1EPKcS1_f,@function
_ZN20HourlyWorkerOvertimeC1EPKcS1_f = _ZN20HourlyWorkerOvertimeC2EPKcS1_f
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
