; ModuleID = 'yacr2.base-4.0/maze.bc'
source_filename = "maze.i"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._IO_FILE = type { i32, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, i8*, %struct._IO_marker*, %struct._IO_FILE*, i32, i32, i64, i16, i8, [1 x i8], i8*, i64, i8*, i8*, i8*, i8*, i64, i32, [20 x i8] }
%struct._IO_marker = type { %struct._IO_marker*, %struct._IO_FILE*, i32 }

@channelColumns = external local_unnamed_addr global i64, align 8
@channelTracks = external local_unnamed_addr global i64, align 8
@horzPlane = internal unnamed_addr global i8* null, align 8
@vertPlane = internal unnamed_addr global i8* null, align 8
@viaPlane = internal unnamed_addr global i8* null, align 8
@mazeRoute = internal unnamed_addr global i8* null, align 8
@stderr = external local_unnamed_addr global %struct._IO_FILE*, align 8
@.str = private unnamed_addr constant [42 x i8] c"unable to allocate plane allocation maps\0A\00", align 1
@.str.1 = private unnamed_addr constant [12 x i8] c"           \00", align 1
@.str.2 = private unnamed_addr constant [5 x i8] c" %d \00", align 1
@TOP = external local_unnamed_addr global i64*, align 8
@.str.4 = private unnamed_addr constant [7 x i8] c"%%%c%%\00", align 1
@.str.5 = private unnamed_addr constant [4 x i8] c" | \00", align 1
@.str.6 = private unnamed_addr constant [4 x i8] c"   \00", align 1
@.str.7 = private unnamed_addr constant [12 x i8] c"Track %3d: \00", align 1
@BOT = external local_unnamed_addr global i64*, align 8
@channelNets = external local_unnamed_addr global i64, align 8
@FIRST = external local_unnamed_addr global i64*, align 8
@LAST = external local_unnamed_addr global i64*, align 8
@netsAssign = external local_unnamed_addr global i64*, align 8

; Function Attrs: nounwind uwtable
define void @InitAllocMaps() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %add = add i64 %0, 1
  %1 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add1 = add i64 %1, 3
  %mul = mul i64 %add1, %add
  %call = tail call noalias i8* @malloc(i64 %mul) #6
  store i8* %call, i8** @horzPlane, align 8, !tbaa !5
  %call5 = tail call noalias i8* @malloc(i64 %mul) #6
  store i8* %call5, i8** @vertPlane, align 8, !tbaa !5
  %call9 = tail call noalias i8* @malloc(i64 %mul) #6
  store i8* %call9, i8** @viaPlane, align 8, !tbaa !5
  %call11 = tail call noalias i8* @malloc(i64 %add) #6
  store i8* %call11, i8** @mazeRoute, align 8, !tbaa !5
  %cmp = icmp eq i8* %call, null
  %cmp12 = icmp eq i8* %call5, null
  %or.cond = or i1 %cmp, %cmp12
  %cmp14 = icmp eq i8* %call9, null
  %or.cond1 = or i1 %or.cond, %cmp14
  %cmp16 = icmp eq i8* %call11, null
  %or.cond2 = or i1 %cmp16, %or.cond1
  br i1 %or.cond2, label %if.then, label %if.end

if.then:                                          ; preds = %entry
  %2 = load %struct._IO_FILE*, %struct._IO_FILE** @stderr, align 8, !tbaa !5
  %3 = tail call i64 @fwrite(i8* getelementptr inbounds ([42 x i8], [42 x i8]* @.str, i64 0, i64 0), i64 41, i64 1, %struct._IO_FILE* %2) #7
  tail call void @exit(i32 1) #8
  unreachable

if.end:                                           ; preds = %entry
  ret void
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #1

; Function Attrs: noreturn nounwind
declare void @exit(i32) local_unnamed_addr #2

; Function Attrs: nounwind uwtable
define void @FreeAllocMaps() local_unnamed_addr #0 {
entry:
  %0 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  tail call void @free(i8* %0) #6
  %1 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  tail call void @free(i8* %1) #6
  %2 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  tail call void @free(i8* %2) #6
  %3 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  tail call void @free(i8* %3) #6
  ret void
}

; Function Attrs: nounwind
declare void @free(i8* nocapture) local_unnamed_addr #1

; Function Attrs: norecurse nounwind uwtable
define void @DrawSegment(i8* nocapture %plane, i64 %x1, i64 %y1, i64 %x2, i64 %y2) local_unnamed_addr #3 {
entry:
  %cmp = icmp eq i64 %x1, %x2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %cmp1 = icmp ult i64 %y1, %y2
  %cond = select i1 %cmp1, i64 %y1, i64 %y2
  %0 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul = mul i64 %0, %cond
  %add1953 = add i64 %mul, %x1
  %arrayidx2054 = getelementptr inbounds i8, i8* %plane, i64 %add1953
  %1 = load i8, i8* %arrayidx2054, align 1, !tbaa !7
  %or2255 = or i8 %1, 8
  store i8 %or2255, i8* %arrayidx2054, align 1, !tbaa !7
  %y.056 = add i64 %cond, 1
  %cond15 = select i1 %cmp1, i64 %y2, i64 %y1
  %cmp1657 = icmp ult i64 %y.056, %cond15
  %2 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp1657, label %for.body.preheader, label %for.end

for.body.preheader:                               ; preds = %if.then
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %3 = phi i64 [ %5, %for.body ], [ %2, %for.body.preheader ]
  %y.058 = phi i64 [ %y.0, %for.body ], [ %y.056, %for.body.preheader ]
  %mul18 = mul i64 %3, %y.058
  %add19 = add i64 %mul18, %x1
  %arrayidx20 = getelementptr inbounds i8, i8* %plane, i64 %add19
  %4 = load i8, i8* %arrayidx20, align 1, !tbaa !7
  %or22 = or i8 %4, 12
  store i8 %or22, i8* %arrayidx20, align 1, !tbaa !7
  %y.0 = add nuw i64 %y.058, 1
  %cmp16 = icmp ult i64 %y.0, %cond15
  %5 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16, label %for.body, label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %if.then
  %.lcssa = phi i64 [ %2, %if.then ], [ %5, %for.end.loopexit ]
  %mul30 = mul i64 %.lcssa, %cond15
  %add31 = add i64 %mul30, %x1
  br label %if.end

if.else:                                          ; preds = %entry
  %6 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul36 = mul i64 %6, %y1
  %cmp37 = icmp ult i64 %x1, %x2
  %cond42 = select i1 %cmp37, i64 %x1, i64 %x2
  %add43 = add i64 %mul36, %cond42
  %arrayidx44 = getelementptr inbounds i8, i8* %plane, i64 %add43
  %7 = load i8, i8* %arrayidx44, align 1, !tbaa !7
  %or46 = or i8 %7, 2
  store i8 %or46, i8* %arrayidx44, align 1, !tbaa !7
  %x.060 = add i64 %cond42, 1
  %cond61 = select i1 %cmp37, i64 %x2, i64 %x1
  %cmp6261 = icmp ult i64 %x.060, %cond61
  %8 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul6562 = mul i64 %8, %y1
  br i1 %cmp6261, label %for.body64.preheader, label %for.end70

for.body64.preheader:                             ; preds = %if.else
  br label %for.body64

for.body64:                                       ; preds = %for.body64.preheader, %for.body64
  %mul6564 = phi i64 [ %mul65, %for.body64 ], [ %mul6562, %for.body64.preheader ]
  %x.063 = phi i64 [ %x.0, %for.body64 ], [ %x.060, %for.body64.preheader ]
  %add66 = add i64 %mul6564, %x.063
  %arrayidx67 = getelementptr inbounds i8, i8* %plane, i64 %add66
  store i8 3, i8* %arrayidx67, align 1, !tbaa !7
  %x.0 = add nuw i64 %x.063, 1
  %cmp62 = icmp ult i64 %x.0, %cond61
  %9 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65 = mul i64 %9, %y1
  br i1 %cmp62, label %for.body64, label %for.end70.loopexit

for.end70.loopexit:                               ; preds = %for.body64
  br label %for.end70

for.end70:                                        ; preds = %for.end70.loopexit, %if.else
  %mul65.lcssa = phi i64 [ %mul6562, %if.else ], [ %mul65, %for.end70.loopexit ]
  %add78 = add i64 %mul65.lcssa, %cond61
  br label %if.end

if.end:                                           ; preds = %for.end70, %for.end
  %add78.sink = phi i64 [ %add78, %for.end70 ], [ %add31, %for.end ]
  %.sink = phi i8 [ 1, %for.end70 ], [ 4, %for.end ]
  %arrayidx79 = getelementptr inbounds i8, i8* %plane, i64 %add78.sink
  %10 = load i8, i8* %arrayidx79, align 1, !tbaa !7
  %or81 = or i8 %10, %.sink
  store i8 %or81, i8* %arrayidx79, align 1, !tbaa !7
  ret void
}

; Function Attrs: norecurse nounwind uwtable
define void @DrawVia(i64 %x, i64 %y) local_unnamed_addr #3 {
entry:
  %0 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  %1 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul = mul i64 %1, %y
  %add = add i64 %mul, %x
  %arrayidx = getelementptr inbounds i8, i8* %0, i64 %add
  store i8 1, i8* %arrayidx, align 1, !tbaa !7
  ret void
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @HasVia(i64 %x, i64 %y) local_unnamed_addr #4 {
entry:
  %0 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  %1 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul = mul i64 %1, %y
  %add = add i64 %mul, %x
  %arrayidx = getelementptr inbounds i8, i8* %0, i64 %add
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !7
  %conv = sext i8 %2 to i32
  ret i32 %conv
}

; Function Attrs: norecurse nounwind readonly uwtable
define i32 @SegmentFree(i8* nocapture readonly %plane, i64 %x1, i64 %y1, i64 %x2, i64 %y2) local_unnamed_addr #4 {
entry:
  %cmp = icmp eq i64 %x1, %x2
  br i1 %cmp, label %if.then, label %if.else

if.then:                                          ; preds = %entry
  %cmp1 = icmp ult i64 %y1, %y2
  %cond = select i1 %cmp1, i64 %y1, i64 %y2
  %0 = load i64, i64* @channelColumns, align 8
  %mul = mul i64 %0, %cond
  %add = add i64 %mul, %x1
  %cond11 = select i1 %cmp1, i64 %y2, i64 %y1
  br label %for.body

for.body:                                         ; preds = %if.then, %for.inc
  %index.036 = phi i64 [ %add, %if.then ], [ %add14, %for.inc ]
  %y.035 = phi i64 [ %cond, %if.then ], [ %inc, %for.inc ]
  %arrayidx = getelementptr inbounds i8, i8* %plane, i64 %index.036
  %1 = load i8, i8* %arrayidx, align 1, !tbaa !7
  %tobool = icmp eq i8 %1, 0
  br i1 %tobool, label %for.inc, label %cleanup.loopexit

for.inc:                                          ; preds = %for.body
  %inc = add i64 %y.035, 1
  %add14 = add i64 %0, %index.036
  %cmp12 = icmp ugt i64 %inc, %cond11
  br i1 %cmp12, label %cleanup.loopexit, label %for.body

if.else:                                          ; preds = %entry
  %2 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul15 = mul i64 %2, %y1
  %cmp16 = icmp ult i64 %x1, %x2
  %cond20 = select i1 %cmp16, i64 %x1, i64 %x2
  %add21 = add i64 %mul15, %cond20
  %cond32 = select i1 %cmp16, i64 %x2, i64 %x1
  br label %for.body34

for.body34:                                       ; preds = %if.else, %for.inc39
  %index.138 = phi i64 [ %add21, %if.else ], [ %inc41, %for.inc39 ]
  %x.037 = phi i64 [ %cond20, %if.else ], [ %inc40, %for.inc39 ]
  %arrayidx35 = getelementptr inbounds i8, i8* %plane, i64 %index.138
  %3 = load i8, i8* %arrayidx35, align 1, !tbaa !7
  %tobool36 = icmp eq i8 %3, 0
  br i1 %tobool36, label %for.inc39, label %cleanup.loopexit45

for.inc39:                                        ; preds = %for.body34
  %inc40 = add i64 %x.037, 1
  %inc41 = add i64 %index.138, 1
  %cmp33 = icmp ugt i64 %inc40, %cond32
  br i1 %cmp33, label %cleanup.loopexit45, label %for.body34

cleanup.loopexit:                                 ; preds = %for.body, %for.inc
  %retval.0.ph = phi i32 [ 1, %for.inc ], [ 0, %for.body ]
  br label %cleanup

cleanup.loopexit45:                               ; preds = %for.body34, %for.inc39
  %retval.0.ph46 = phi i32 [ 1, %for.inc39 ], [ 0, %for.body34 ]
  br label %cleanup

cleanup:                                          ; preds = %cleanup.loopexit45, %cleanup.loopexit
  %retval.0 = phi i32 [ %retval.0.ph, %cleanup.loopexit ], [ %retval.0.ph46, %cleanup.loopexit45 ]
  ret i32 %retval.0
}

; Function Attrs: nounwind uwtable
define void @PrintChannel() local_unnamed_addr #0 {
entry:
  %call = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %0 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp124 = icmp eq i64 %0, 0
  br i1 %cmp124, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %x.0125 = phi i64 [ %inc, %for.body ], [ 1, %for.body.preheader ]
  %1 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i64, i64* %1, i64 %x.0125
  %2 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %div = udiv i64 %2, 100
  %call1 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i64 %div)
  %inc = add i64 %x.0125, 1
  %3 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc, %3
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %putchar = tail call i32 @putchar(i32 10) #6
  %call3 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %4 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp5122 = icmp eq i64 %4, 0
  br i1 %cmp5122, label %for.end14, label %for.body6.preheader

for.body6.preheader:                              ; preds = %for.end
  br label %for.body6

for.body6:                                        ; preds = %for.body6.preheader, %for.body6
  %x.1123 = phi i64 [ %inc13, %for.body6 ], [ 1, %for.body6.preheader ]
  %5 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx7 = getelementptr inbounds i64, i64* %5, i64 %x.1123
  %6 = load i64, i64* %arrayidx7, align 8, !tbaa !1
  %div9 = urem i64 %6, 100
  %div10 = udiv i64 %div9, 10
  %call11 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i64 %div10)
  %inc13 = add i64 %x.1123, 1
  %7 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp5 = icmp ugt i64 %inc13, %7
  br i1 %cmp5, label %for.end14.loopexit, label %for.body6

for.end14.loopexit:                               ; preds = %for.body6
  br label %for.end14

for.end14:                                        ; preds = %for.end14.loopexit, %for.end
  %putchar66 = tail call i32 @putchar(i32 10) #6
  %call16 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %8 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp18120 = icmp eq i64 %8, 0
  br i1 %cmp18120, label %for.end24, label %for.body19.preheader

for.body19.preheader:                             ; preds = %for.end14
  br label %for.body19

for.body19:                                       ; preds = %for.body19.preheader, %for.body19
  %x.2121 = phi i64 [ %inc23, %for.body19 ], [ 1, %for.body19.preheader ]
  %9 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx20 = getelementptr inbounds i64, i64* %9, i64 %x.2121
  %10 = load i64, i64* %arrayidx20, align 8, !tbaa !1
  %rem = urem i64 %10, 10
  %call21 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i64 %rem)
  %inc23 = add i64 %x.2121, 1
  %11 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp18 = icmp ugt i64 %inc23, %11
  br i1 %cmp18, label %for.end24.loopexit, label %for.body19

for.end24.loopexit:                               ; preds = %for.body19
  br label %for.end24

for.end24:                                        ; preds = %for.end24.loopexit, %for.end14
  %putchar67 = tail call i32 @putchar(i32 10) #6
  %call26 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %12 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp28118 = icmp eq i64 %12, 0
  br i1 %cmp28118, label %for.end35, label %for.body29.preheader

for.body29.preheader:                             ; preds = %for.end24
  br label %for.body29

for.body29:                                       ; preds = %for.body29.preheader, %for.body29
  %x.3119 = phi i64 [ %inc34, %for.body29 ], [ 1, %for.body29.preheader ]
  %13 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %arrayidx31 = getelementptr inbounds i8, i8* %13, i64 %x.3119
  %14 = load i8, i8* %arrayidx31, align 1, !tbaa !7
  %tobool = icmp ne i8 %14, 0
  %cond = select i1 %tobool, i32 124, i32 32
  %call32 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.4, i64 0, i64 0), i32 %cond)
  %inc34 = add i64 %x.3119, 1
  %15 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp28 = icmp ugt i64 %inc34, %15
  br i1 %cmp28, label %for.end35.loopexit, label %for.body29

for.end35.loopexit:                               ; preds = %for.body29
  br label %for.end35

for.end35:                                        ; preds = %for.end35.loopexit, %for.end24
  %putchar68 = tail call i32 @putchar(i32 10) #6
  %16 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp38114 = icmp eq i64 %16, 0
  %call41115 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  br i1 %cmp38114, label %for.cond205.preheader, label %for.cond42.preheader.preheader

for.cond42.preheader.preheader:                   ; preds = %for.end35
  br label %for.cond42.preheader

for.cond42.preheader:                             ; preds = %for.cond42.preheader.preheader, %for.end199
  %y.0116 = phi i64 [ %inc202, %for.end199 ], [ 1, %for.cond42.preheader.preheader ]
  %17 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp43107 = icmp eq i64 %17, 0
  br i1 %cmp43107, label %for.end55, label %for.body45.preheader

for.body45.preheader:                             ; preds = %for.cond42.preheader
  br label %for.body45

for.cond205.preheader.loopexit:                   ; preds = %for.end199
  br label %for.cond205.preheader

for.cond205.preheader:                            ; preds = %for.cond205.preheader.loopexit, %for.end35
  %18 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp206105 = icmp eq i64 %18, 0
  br i1 %cmp206105, label %for.end219, label %for.body208.preheader

for.body208.preheader:                            ; preds = %for.cond205.preheader
  br label %for.body208

for.body45:                                       ; preds = %for.body45.preheader, %for.inc53
  %19 = phi i64 [ %23, %for.inc53 ], [ %17, %for.body45.preheader ]
  %x.4108 = phi i64 [ %inc54, %for.inc53 ], [ 1, %for.body45.preheader ]
  %20 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %mul46 = mul i64 %19, %y.0116
  %add47 = add i64 %mul46, %x.4108
  %arrayidx48 = getelementptr inbounds i8, i8* %20, i64 %add47
  %21 = load i8, i8* %arrayidx48, align 1, !tbaa !7
  %22 = and i8 %21, 4
  %tobool50 = icmp eq i8 %22, 0
  br i1 %tobool50, label %if.else, label %if.then

if.then:                                          ; preds = %for.body45
  %call51 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i64 0, i64 0))
  br label %for.inc53

if.else:                                          ; preds = %for.body45
  %call52 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.6, i64 0, i64 0))
  br label %for.inc53

for.inc53:                                        ; preds = %if.then, %if.else
  %inc54 = add i64 %x.4108, 1
  %23 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp43 = icmp ugt i64 %inc54, %23
  br i1 %cmp43, label %for.end55.loopexit, label %for.body45

for.end55.loopexit:                               ; preds = %for.inc53
  br label %for.end55

for.end55:                                        ; preds = %for.end55.loopexit, %for.cond42.preheader
  %putchar73 = tail call i32 @putchar(i32 10) #6
  %call57 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.7, i64 0, i64 0), i64 %y.0116)
  %24 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp59109 = icmp eq i64 %24, 0
  br i1 %cmp59109, label %for.end179, label %for.body61.preheader

for.body61.preheader:                             ; preds = %for.end55
  br label %for.body61

for.body61:                                       ; preds = %for.body61.preheader, %for.inc177
  %25 = phi i64 [ %46, %for.inc177 ], [ %24, %for.body61.preheader ]
  %x.5110 = phi i64 [ %inc178, %for.inc177 ], [ 1, %for.body61.preheader ]
  %26 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %mul62 = mul i64 %25, %y.0116
  %add63 = add i64 %mul62, %x.5110
  %arrayidx64 = getelementptr inbounds i8, i8* %26, i64 %add63
  %27 = load i8, i8* %arrayidx64, align 1, !tbaa !7
  %28 = and i8 %27, 1
  %tobool67 = icmp eq i8 %28, 0
  %29 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %arrayidx88 = getelementptr inbounds i8, i8* %29, i64 %add63
  %30 = load i8, i8* %arrayidx88, align 1, !tbaa !7
  %31 = and i8 %30, 1
  %tobool91 = icmp eq i8 %31, 0
  br i1 %tobool67, label %if.else85, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body61
  br i1 %tobool91, label %if.then83, label %if.then74

if.then74:                                        ; preds = %land.lhs.true
  %putchar97 = tail call i32 @putchar(i32 61) #6
  br label %if.end98

if.then83:                                        ; preds = %land.lhs.true
  %putchar95 = tail call i32 @putchar(i32 45) #6
  br label %if.end98

if.else85:                                        ; preds = %for.body61
  br i1 %tobool91, label %if.else94, label %if.then92

if.then92:                                        ; preds = %if.else85
  %putchar94 = tail call i32 @putchar(i32 94) #6
  br label %if.end98

if.else94:                                        ; preds = %if.else85
  %putchar80 = tail call i32 @putchar(i32 32) #6
  br label %if.end98

if.end98:                                         ; preds = %if.then83, %if.else94, %if.then92, %if.then74
  %32 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  %33 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul99 = mul i64 %33, %y.0116
  %add100 = add i64 %mul99, %x.5110
  %arrayidx101 = getelementptr inbounds i8, i8* %32, i64 %add100
  %34 = load i8, i8* %arrayidx101, align 1, !tbaa !7
  %tobool102 = icmp eq i8 %34, 0
  br i1 %tobool102, label %if.else105, label %if.then103

if.then103:                                       ; preds = %if.end98
  %putchar93 = tail call i32 @putchar(i32 88) #6
  br label %if.end138

if.else105:                                       ; preds = %if.end98
  %35 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %arrayidx108 = getelementptr inbounds i8, i8* %35, i64 %add100
  %36 = load i8, i8* %arrayidx108, align 1, !tbaa !7
  %tobool110 = icmp eq i8 %36, 0
  %37 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %arrayidx129 = getelementptr inbounds i8, i8* %37, i64 %add100
  %38 = load i8, i8* %arrayidx129, align 1, !tbaa !7
  %tobool130 = icmp eq i8 %38, 0
  br i1 %tobool110, label %if.else126, label %land.lhs.true111

land.lhs.true111:                                 ; preds = %if.else105
  br i1 %tobool130, label %if.then124, label %if.then117

if.then117:                                       ; preds = %land.lhs.true111
  %putchar92 = tail call i32 @putchar(i32 43) #6
  br label %if.end138

if.then124:                                       ; preds = %land.lhs.true111
  %putchar91 = tail call i32 @putchar(i32 45) #6
  br label %if.end138

if.else126:                                       ; preds = %if.else105
  br i1 %tobool130, label %if.else133, label %if.then131

if.then131:                                       ; preds = %if.else126
  %putchar90 = tail call i32 @putchar(i32 124) #6
  br label %if.end138

if.else133:                                       ; preds = %if.else126
  %putchar81 = tail call i32 @putchar(i32 32) #6
  br label %if.end138

if.end138:                                        ; preds = %if.then117, %if.then131, %if.else133, %if.then124, %if.then103
  %39 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %40 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul139 = mul i64 %40, %y.0116
  %add140 = add i64 %mul139, %x.5110
  %arrayidx141 = getelementptr inbounds i8, i8* %39, i64 %add140
  %41 = load i8, i8* %arrayidx141, align 1, !tbaa !7
  %42 = and i8 %41, 2
  %tobool144 = icmp eq i8 %42, 0
  %43 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %arrayidx166 = getelementptr inbounds i8, i8* %43, i64 %add140
  %44 = load i8, i8* %arrayidx166, align 1, !tbaa !7
  %45 = and i8 %44, 2
  %tobool169 = icmp eq i8 %45, 0
  br i1 %tobool144, label %if.else163, label %land.lhs.true145

land.lhs.true145:                                 ; preds = %if.end138
  br i1 %tobool169, label %if.then161, label %if.then152

if.then152:                                       ; preds = %land.lhs.true145
  %putchar89 = tail call i32 @putchar(i32 61) #6
  br label %for.inc177

if.then161:                                       ; preds = %land.lhs.true145
  %putchar87 = tail call i32 @putchar(i32 45) #6
  br label %for.inc177

if.else163:                                       ; preds = %if.end138
  br i1 %tobool169, label %if.else172, label %if.then170

if.then170:                                       ; preds = %if.else163
  %putchar86 = tail call i32 @putchar(i32 94) #6
  br label %for.inc177

if.else172:                                       ; preds = %if.else163
  %putchar85 = tail call i32 @putchar(i32 32) #6
  br label %for.inc177

for.inc177:                                       ; preds = %if.then152, %if.then170, %if.else172, %if.then161
  %inc178 = add i64 %x.5110, 1
  %46 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp59 = icmp ugt i64 %inc178, %46
  br i1 %cmp59, label %for.end179.loopexit, label %for.body61

for.end179.loopexit:                              ; preds = %for.inc177
  br label %for.end179

for.end179:                                       ; preds = %for.end179.loopexit, %for.end55
  %putchar74 = tail call i32 @putchar(i32 10) #6
  %call181 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %47 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp183112 = icmp eq i64 %47, 0
  br i1 %cmp183112, label %for.end199, label %for.body185.preheader

for.body185.preheader:                            ; preds = %for.end179
  br label %for.body185

for.body185:                                      ; preds = %for.body185.preheader, %for.inc197
  %48 = phi i64 [ %52, %for.inc197 ], [ %47, %for.body185.preheader ]
  %x.6113 = phi i64 [ %inc198, %for.inc197 ], [ 1, %for.body185.preheader ]
  %49 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %mul186 = mul i64 %48, %y.0116
  %add187 = add i64 %mul186, %x.6113
  %arrayidx188 = getelementptr inbounds i8, i8* %49, i64 %add187
  %50 = load i8, i8* %arrayidx188, align 1, !tbaa !7
  %51 = and i8 %50, 8
  %tobool191 = icmp eq i8 %51, 0
  br i1 %tobool191, label %if.else194, label %if.then192

if.then192:                                       ; preds = %for.body185
  %call193 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.5, i64 0, i64 0))
  br label %for.inc197

if.else194:                                       ; preds = %for.body185
  %call195 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([4 x i8], [4 x i8]* @.str.6, i64 0, i64 0))
  br label %for.inc197

for.inc197:                                       ; preds = %if.then192, %if.else194
  %inc198 = add i64 %x.6113, 1
  %52 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp183 = icmp ugt i64 %inc198, %52
  br i1 %cmp183, label %for.end199.loopexit, label %for.body185

for.end199.loopexit:                              ; preds = %for.inc197
  br label %for.end199

for.end199:                                       ; preds = %for.end199.loopexit, %for.end179
  %putchar75 = tail call i32 @putchar(i32 10) #6
  %inc202 = add i64 %y.0116, 1
  %53 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp38 = icmp ugt i64 %inc202, %53
  %call41 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  br i1 %cmp38, label %for.cond205.preheader.loopexit, label %for.cond42.preheader

for.body208:                                      ; preds = %for.body208.preheader, %for.body208
  %54 = phi i64 [ %58, %for.body208 ], [ %18, %for.body208.preheader ]
  %x.7106 = phi i64 [ %inc218, %for.body208 ], [ 1, %for.body208.preheader ]
  %55 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %56 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add209 = add i64 %56, 1
  %mul210 = mul i64 %add209, %54
  %add211 = add i64 %mul210, %x.7106
  %arrayidx212 = getelementptr inbounds i8, i8* %55, i64 %add211
  %57 = load i8, i8* %arrayidx212, align 1, !tbaa !7
  %tobool214 = icmp ne i8 %57, 0
  %cond215 = select i1 %tobool214, i32 124, i32 32
  %call216 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([7 x i8], [7 x i8]* @.str.4, i64 0, i64 0), i32 %cond215)
  %inc218 = add i64 %x.7106, 1
  %58 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp206 = icmp ugt i64 %inc218, %58
  br i1 %cmp206, label %for.end219.loopexit, label %for.body208

for.end219.loopexit:                              ; preds = %for.body208
  br label %for.end219

for.end219:                                       ; preds = %for.end219.loopexit, %for.cond205.preheader
  %putchar69 = tail call i32 @putchar(i32 10) #6
  %call221 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %59 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp223103 = icmp eq i64 %59, 0
  br i1 %cmp223103, label %for.end231, label %for.body225.preheader

for.body225.preheader:                            ; preds = %for.end219
  br label %for.body225

for.body225:                                      ; preds = %for.body225.preheader, %for.body225
  %x.8104 = phi i64 [ %inc230, %for.body225 ], [ 1, %for.body225.preheader ]
  %60 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx226 = getelementptr inbounds i64, i64* %60, i64 %x.8104
  %61 = load i64, i64* %arrayidx226, align 8, !tbaa !1
  %div227 = udiv i64 %61, 100
  %call228 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i64 %div227)
  %inc230 = add i64 %x.8104, 1
  %62 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp223 = icmp ugt i64 %inc230, %62
  br i1 %cmp223, label %for.end231.loopexit, label %for.body225

for.end231.loopexit:                              ; preds = %for.body225
  br label %for.end231

for.end231:                                       ; preds = %for.end231.loopexit, %for.end219
  %putchar70 = tail call i32 @putchar(i32 10) #6
  %call233 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %63 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp235101 = icmp eq i64 %63, 0
  br i1 %cmp235101, label %for.end247, label %for.body237.preheader

for.body237.preheader:                            ; preds = %for.end231
  br label %for.body237

for.body237:                                      ; preds = %for.body237.preheader, %for.body237
  %x.9102 = phi i64 [ %inc246, %for.body237 ], [ 1, %for.body237.preheader ]
  %64 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx238 = getelementptr inbounds i64, i64* %64, i64 %x.9102
  %65 = load i64, i64* %arrayidx238, align 8, !tbaa !1
  %div240 = urem i64 %65, 100
  %div243 = udiv i64 %div240, 10
  %call244 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i64 %div243)
  %inc246 = add i64 %x.9102, 1
  %66 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp235 = icmp ugt i64 %inc246, %66
  br i1 %cmp235, label %for.end247.loopexit, label %for.body237

for.end247.loopexit:                              ; preds = %for.body237
  br label %for.end247

for.end247:                                       ; preds = %for.end247.loopexit, %for.end231
  %putchar71 = tail call i32 @putchar(i32 10) #6
  %call249 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([12 x i8], [12 x i8]* @.str.1, i64 0, i64 0))
  %67 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp25199 = icmp eq i64 %67, 0
  br i1 %cmp25199, label %for.end259, label %for.body253.preheader

for.body253.preheader:                            ; preds = %for.end247
  br label %for.body253

for.body253:                                      ; preds = %for.body253.preheader, %for.body253
  %x.10100 = phi i64 [ %inc258, %for.body253 ], [ 1, %for.body253.preheader ]
  %68 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx254 = getelementptr inbounds i64, i64* %68, i64 %x.10100
  %69 = load i64, i64* %arrayidx254, align 8, !tbaa !1
  %rem255 = urem i64 %69, 10
  %call256 = tail call i32 (i8*, ...) @printf(i8* getelementptr inbounds ([5 x i8], [5 x i8]* @.str.2, i64 0, i64 0), i64 %rem255)
  %inc258 = add i64 %x.10100, 1
  %70 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp251 = icmp ugt i64 %inc258, %70
  br i1 %cmp251, label %for.end259.loopexit, label %for.body253

for.end259.loopexit:                              ; preds = %for.body253
  br label %for.end259

for.end259:                                       ; preds = %for.end259.loopexit, %for.end247
  %putchar72 = tail call i32 @putchar(i32 10) #6
  ret void
}

; Function Attrs: nounwind
declare i32 @printf(i8* nocapture readonly, ...) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define i32 @DrawNets() local_unnamed_addr #0 {
entry:
  %0 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %1 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %2 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add1 = add i64 %2, 2
  %add = shl i64 %1, 32
  %mul = add i64 %add, 4294967296
  %sext = mul i64 %mul, %add1
  %conv2 = ashr exact i64 %sext, 32
  tail call void @llvm.memset.p0i8.i64(i8* %0, i8 0, i64 %conv2, i32 1, i1 false)
  %3 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %4 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %5 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add4 = add i64 %5, 2
  %add3 = shl i64 %4, 32
  %mul5 = add i64 %add3, 4294967296
  %sext54 = mul i64 %mul5, %add4
  %conv7 = ashr exact i64 %sext54, 32
  tail call void @llvm.memset.p0i8.i64(i8* %3, i8 0, i64 %conv7, i32 1, i1 false)
  %6 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  %7 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %8 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add9 = add i64 %8, 2
  %add8 = shl i64 %7, 32
  %mul10 = add i64 %add8, 4294967296
  %sext55 = mul i64 %mul10, %add9
  %conv12 = ashr exact i64 %sext55, 32
  tail call void @llvm.memset.p0i8.i64(i8* %6, i8 0, i64 %conv12, i32 1, i1 false)
  %9 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %10 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %add13 = shl i64 %10, 32
  %sext56 = add i64 %add13, 4294967296
  %conv15 = ashr exact i64 %sext56, 32
  tail call void @llvm.memset.p0i8.i64(i8* %9, i8 0, i64 %conv15, i32 1, i1 false)
  %11 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp259 = icmp eq i64 %11, 0
  br i1 %cmp259, label %for.cond24.preheader, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.cond24.preheader.loopexit:                    ; preds = %for.inc
  br label %for.cond24.preheader

for.cond24.preheader:                             ; preds = %for.cond24.preheader.loopexit, %entry
  %12 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp25247 = icmp eq i64 %12, 0
  br i1 %cmp25247, label %for.end112, label %for.body27.preheader

for.body27.preheader:                             ; preds = %for.cond24.preheader
  br label %for.body27

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %13 = phi i64 [ %25, %for.inc ], [ %11, %for.body.preheader ]
  %i.0260 = phi i64 [ %inc, %for.inc ], [ 1, %for.body.preheader ]
  %14 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i64, i64* %14, i64 %i.0260
  %15 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %16 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx17 = getelementptr inbounds i64, i64* %16, i64 %i.0260
  %17 = load i64, i64* %arrayidx17, align 8, !tbaa !1
  %cmp18 = icmp eq i64 %15, %17
  br i1 %cmp18, label %for.inc, label %if.else.i

if.else.i:                                        ; preds = %for.body
  %18 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx21 = getelementptr inbounds i64, i64* %18, i64 %i.0260
  %19 = load i64, i64* %arrayidx21, align 8, !tbaa !1
  %20 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul36.i = mul i64 %20, %19
  %cmp37.i = icmp ult i64 %15, %17
  %cond42.i = select i1 %cmp37.i, i64 %15, i64 %17
  %add43.i = add i64 %mul36.i, %cond42.i
  %arrayidx44.i = getelementptr inbounds i8, i8* %0, i64 %add43.i
  %21 = load i8, i8* %arrayidx44.i, align 1, !tbaa !7
  %or46.i = or i8 %21, 2
  store i8 %or46.i, i8* %arrayidx44.i, align 1, !tbaa !7
  %x.0.i252 = add i64 %cond42.i, 1
  %cond61.i = select i1 %cmp37.i, i64 %17, i64 %15
  %cmp62.i253 = icmp ult i64 %x.0.i252, %cond61.i
  %22 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i254 = mul i64 %22, %19
  br i1 %cmp62.i253, label %for.body64.i.preheader, label %DrawSegment.exit

for.body64.i.preheader:                           ; preds = %if.else.i
  br label %for.body64.i

for.body64.i:                                     ; preds = %for.body64.i.preheader, %for.body64.i
  %mul65.i256 = phi i64 [ %mul65.i, %for.body64.i ], [ %mul65.i254, %for.body64.i.preheader ]
  %x.0.i255 = phi i64 [ %x.0.i, %for.body64.i ], [ %x.0.i252, %for.body64.i.preheader ]
  %add66.i = add i64 %mul65.i256, %x.0.i255
  %arrayidx67.i = getelementptr inbounds i8, i8* %0, i64 %add66.i
  store i8 3, i8* %arrayidx67.i, align 1, !tbaa !7
  %x.0.i = add nuw i64 %x.0.i255, 1
  %cmp62.i = icmp ult i64 %x.0.i, %cond61.i
  %23 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i = mul i64 %23, %19
  br i1 %cmp62.i, label %for.body64.i, label %DrawSegment.exit.loopexit

DrawSegment.exit.loopexit:                        ; preds = %for.body64.i
  br label %DrawSegment.exit

DrawSegment.exit:                                 ; preds = %DrawSegment.exit.loopexit, %if.else.i
  %mul65.i.lcssa = phi i64 [ %mul65.i254, %if.else.i ], [ %mul65.i, %DrawSegment.exit.loopexit ]
  %add78.i = add i64 %mul65.i.lcssa, %cond61.i
  %arrayidx79.i = getelementptr inbounds i8, i8* %0, i64 %add78.i
  %24 = load i8, i8* %arrayidx79.i, align 1, !tbaa !7
  %or81.i = or i8 %24, 1
  store i8 %or81.i, i8* %arrayidx79.i, align 1, !tbaa !7
  %.pre = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %for.inc

for.inc:                                          ; preds = %for.body, %DrawSegment.exit
  %25 = phi i64 [ %13, %for.body ], [ %.pre, %DrawSegment.exit ]
  %inc = add i64 %i.0260, 1
  %cmp = icmp ugt i64 %inc, %25
  br i1 %cmp, label %for.cond24.preheader.loopexit, label %for.body

for.body27:                                       ; preds = %for.body27.preheader, %for.inc110
  %26 = phi i64 [ %134, %for.inc110 ], [ %12, %for.body27.preheader ]
  %numLeft.0250 = phi i32 [ %numLeft.1, %for.inc110 ], [ 0, %for.body27.preheader ]
  %i.1248 = phi i64 [ %inc111, %for.inc110 ], [ 1, %for.body27.preheader ]
  %27 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx28 = getelementptr inbounds i64, i64* %27, i64 %i.1248
  %28 = load i64, i64* %arrayidx28, align 8, !tbaa !1
  %cmp29 = icmp eq i64 %28, 0
  %29 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx31 = getelementptr inbounds i64, i64* %29, i64 %i.1248
  %30 = load i64, i64* %arrayidx31, align 8, !tbaa !1
  %cmp32 = icmp eq i64 %30, 0
  br i1 %cmp29, label %land.lhs.true, label %if.else47

land.lhs.true:                                    ; preds = %for.body27
  br i1 %cmp32, label %for.inc110, label %land.lhs.true38

land.lhs.true38:                                  ; preds = %land.lhs.true
  %31 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx39 = getelementptr inbounds i64, i64* %31, i64 %i.1248
  %32 = load i64, i64* %arrayidx39, align 8, !tbaa !1
  %cmp40 = icmp eq i64 %32, 0
  br i1 %cmp40, label %if.else61, label %if.then42

if.then42:                                        ; preds = %land.lhs.true38
  %33 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx44 = getelementptr inbounds i64, i64* %33, i64 %32
  %34 = load i64, i64* %arrayidx44, align 8, !tbaa !1
  %arrayidx20.i187209 = getelementptr inbounds i8, i8* %3, i64 %i.1248
  %35 = load i8, i8* %arrayidx20.i187209, align 1, !tbaa !7
  %or22.i188210 = or i8 %35, 8
  store i8 %or22.i188210, i8* %arrayidx20.i187209, align 1, !tbaa !7
  %cmp16.i191211 = icmp ugt i64 %34, 1
  %36 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i191211, label %for.body.i194.preheader, label %DrawSegment.exit202

for.body.i194.preheader:                          ; preds = %if.then42
  %37 = and i64 %34, 1
  %lcmp.mod = icmp eq i64 %37, 0
  br i1 %lcmp.mod, label %for.body.i194.prol.preheader, label %for.body.i194.prol.loopexit.unr-lcssa

for.body.i194.prol.preheader:                     ; preds = %for.body.i194.preheader
  br label %for.body.i194.prol

for.body.i194.prol:                               ; preds = %for.body.i194.prol.preheader
  %add19.i186.prol = add i64 %36, %i.1248
  %arrayidx20.i187.prol = getelementptr inbounds i8, i8* %3, i64 %add19.i186.prol
  %38 = load i8, i8* %arrayidx20.i187.prol, align 1, !tbaa !7
  %or22.i188.prol = or i8 %38, 12
  store i8 %or22.i188.prol, i8* %arrayidx20.i187.prol, align 1, !tbaa !7
  %39 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i194.prol.loopexit.unr-lcssa

for.body.i194.prol.loopexit.unr-lcssa:            ; preds = %for.body.i194.preheader, %for.body.i194.prol
  %.lcssa290.unr.ph = phi i64 [ %39, %for.body.i194.prol ], [ undef, %for.body.i194.preheader ]
  %.unr.ph = phi i64 [ %39, %for.body.i194.prol ], [ %36, %for.body.i194.preheader ]
  %y.0.i189212.unr.ph = phi i64 [ 2, %for.body.i194.prol ], [ 1, %for.body.i194.preheader ]
  br label %for.body.i194.prol.loopexit

for.body.i194.prol.loopexit:                      ; preds = %for.body.i194.prol.loopexit.unr-lcssa
  %40 = icmp eq i64 %34, 2
  br i1 %40, label %DrawSegment.exit202.loopexit, label %for.body.i194.preheader.new

for.body.i194.preheader.new:                      ; preds = %for.body.i194.prol.loopexit
  br label %for.body.i194

for.body.i194:                                    ; preds = %for.body.i194, %for.body.i194.preheader.new
  %41 = phi i64 [ %.unr.ph, %for.body.i194.preheader.new ], [ %45, %for.body.i194 ]
  %y.0.i189212 = phi i64 [ %y.0.i189212.unr.ph, %for.body.i194.preheader.new ], [ %y.0.i189.1, %for.body.i194 ]
  %mul18.i193 = mul i64 %41, %y.0.i189212
  %add19.i186 = add i64 %mul18.i193, %i.1248
  %arrayidx20.i187 = getelementptr inbounds i8, i8* %3, i64 %add19.i186
  %42 = load i8, i8* %arrayidx20.i187, align 1, !tbaa !7
  %or22.i188 = or i8 %42, 12
  store i8 %or22.i188, i8* %arrayidx20.i187, align 1, !tbaa !7
  %y.0.i189 = add nuw i64 %y.0.i189212, 1
  %43 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i193.1 = mul i64 %43, %y.0.i189
  %add19.i186.1 = add i64 %mul18.i193.1, %i.1248
  %arrayidx20.i187.1 = getelementptr inbounds i8, i8* %3, i64 %add19.i186.1
  %44 = load i8, i8* %arrayidx20.i187.1, align 1, !tbaa !7
  %or22.i188.1 = or i8 %44, 12
  store i8 %or22.i188.1, i8* %arrayidx20.i187.1, align 1, !tbaa !7
  %y.0.i189.1 = add i64 %y.0.i189212, 2
  %45 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %exitcond.1 = icmp eq i64 %y.0.i189.1, %34
  br i1 %exitcond.1, label %DrawSegment.exit202.loopexit.unr-lcssa, label %for.body.i194

DrawSegment.exit202.loopexit.unr-lcssa:           ; preds = %for.body.i194
  br label %DrawSegment.exit202.loopexit

DrawSegment.exit202.loopexit:                     ; preds = %for.body.i194.prol.loopexit, %DrawSegment.exit202.loopexit.unr-lcssa
  %.lcssa290 = phi i64 [ %.lcssa290.unr.ph, %for.body.i194.prol.loopexit ], [ %45, %DrawSegment.exit202.loopexit.unr-lcssa ]
  br label %DrawSegment.exit202

DrawSegment.exit202:                              ; preds = %DrawSegment.exit202.loopexit, %if.then42
  %.lcssa = phi i64 [ %36, %if.then42 ], [ %.lcssa290, %DrawSegment.exit202.loopexit ]
  %mul30.i195 = mul i64 %.lcssa, %34
  %add31.i196 = add i64 %mul30.i195, %i.1248
  %arrayidx79.i200 = getelementptr inbounds i8, i8* %3, i64 %add31.i196
  %46 = load i8, i8* %arrayidx79.i200, align 1, !tbaa !7
  %or81.i201 = or i8 %46, 4
  store i8 %or81.i201, i8* %arrayidx79.i200, align 1, !tbaa !7
  %47 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %48 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx45 = getelementptr inbounds i64, i64* %48, i64 %i.1248
  %49 = load i64, i64* %arrayidx45, align 8, !tbaa !1
  %arrayidx46 = getelementptr inbounds i64, i64* %47, i64 %49
  %50 = load i64, i64* %arrayidx46, align 8, !tbaa !1
  %51 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i178 = mul i64 %51, %50
  %add.i179 = add i64 %mul.i178, %i.1248
  %arrayidx.i180 = getelementptr inbounds i8, i8* %6, i64 %add.i179
  store i8 1, i8* %arrayidx.i180, align 1, !tbaa !7
  br label %for.inc110

if.else47:                                        ; preds = %for.body27
  br i1 %cmp32, label %if.then55, label %if.else61

if.then55:                                        ; preds = %if.else47
  %52 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx57 = getelementptr inbounds i64, i64* %52, i64 %28
  %53 = load i64, i64* %arrayidx57, align 8, !tbaa !1
  %54 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add58 = add i64 %54, 1
  %cmp1.i154 = icmp ult i64 %53, %add58
  %cond.i155 = select i1 %cmp1.i154, i64 %53, i64 %add58
  %mul.i156 = mul i64 %cond.i155, %26
  %add19.i161213 = add i64 %mul.i156, %i.1248
  %arrayidx20.i162214 = getelementptr inbounds i8, i8* %3, i64 %add19.i161213
  %55 = load i8, i8* %arrayidx20.i162214, align 1, !tbaa !7
  %or22.i163215 = or i8 %55, 8
  store i8 %or22.i163215, i8* %arrayidx20.i162214, align 1, !tbaa !7
  %y.0.i164216 = add i64 %cond.i155, 1
  %cond15.i165 = select i1 %cmp1.i154, i64 %add58, i64 %53
  %cmp16.i166217 = icmp ult i64 %y.0.i164216, %cond15.i165
  %56 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i166217, label %for.body.i169.preheader, label %DrawSegment.exit177

for.body.i169.preheader:                          ; preds = %if.then55
  br label %for.body.i169

for.body.i169:                                    ; preds = %for.body.i169.preheader, %for.body.i169
  %57 = phi i64 [ %59, %for.body.i169 ], [ %56, %for.body.i169.preheader ]
  %y.0.i164218 = phi i64 [ %y.0.i164, %for.body.i169 ], [ %y.0.i164216, %for.body.i169.preheader ]
  %mul18.i168 = mul i64 %57, %y.0.i164218
  %add19.i161 = add i64 %mul18.i168, %i.1248
  %arrayidx20.i162 = getelementptr inbounds i8, i8* %3, i64 %add19.i161
  %58 = load i8, i8* %arrayidx20.i162, align 1, !tbaa !7
  %or22.i163 = or i8 %58, 12
  store i8 %or22.i163, i8* %arrayidx20.i162, align 1, !tbaa !7
  %y.0.i164 = add nuw i64 %y.0.i164218, 1
  %cmp16.i166 = icmp ult i64 %y.0.i164, %cond15.i165
  %59 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i166, label %for.body.i169, label %DrawSegment.exit177.loopexit

DrawSegment.exit177.loopexit:                     ; preds = %for.body.i169
  br label %DrawSegment.exit177

DrawSegment.exit177:                              ; preds = %DrawSegment.exit177.loopexit, %if.then55
  %.lcssa203 = phi i64 [ %56, %if.then55 ], [ %59, %DrawSegment.exit177.loopexit ]
  %mul30.i170 = mul i64 %.lcssa203, %cond15.i165
  %add31.i171 = add i64 %mul30.i170, %i.1248
  %arrayidx79.i175 = getelementptr inbounds i8, i8* %3, i64 %add31.i171
  %60 = load i8, i8* %arrayidx79.i175, align 1, !tbaa !7
  %or81.i176 = or i8 %60, 4
  store i8 %or81.i176, i8* %arrayidx79.i175, align 1, !tbaa !7
  %61 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %62 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx59 = getelementptr inbounds i64, i64* %62, i64 %i.1248
  %63 = load i64, i64* %arrayidx59, align 8, !tbaa !1
  %arrayidx60 = getelementptr inbounds i64, i64* %61, i64 %63
  %64 = load i64, i64* %arrayidx60, align 8, !tbaa !1
  %65 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i151 = mul i64 %65, %64
  %add.i152 = add i64 %mul.i151, %i.1248
  %arrayidx.i153 = getelementptr inbounds i8, i8* %6, i64 %add.i152
  store i8 1, i8* %arrayidx.i153, align 1, !tbaa !7
  br label %for.inc110

if.else61:                                        ; preds = %land.lhs.true38, %if.else47
  %66 = phi i64 [ %30, %if.else47 ], [ 0, %land.lhs.true38 ]
  %cmp64 = icmp eq i64 %66, %28
  br i1 %cmp64, label %land.lhs.true66, label %if.else84

land.lhs.true66:                                  ; preds = %if.else61
  %67 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx68 = getelementptr inbounds i64, i64* %67, i64 %28
  %68 = load i64, i64* %arrayidx68, align 8, !tbaa !1
  %69 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx70 = getelementptr inbounds i64, i64* %69, i64 %28
  %70 = load i64, i64* %arrayidx70, align 8, !tbaa !1
  %cmp71 = icmp eq i64 %68, %70
  %71 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add74 = add i64 %71, 1
  %arrayidx20.i135242 = getelementptr inbounds i8, i8* %3, i64 %i.1248
  %72 = load i8, i8* %arrayidx20.i135242, align 1, !tbaa !7
  %or22.i136243 = or i8 %72, 8
  store i8 %or22.i136243, i8* %arrayidx20.i135242, align 1, !tbaa !7
  %cmp16.i139244 = icmp ugt i64 %add74, 1
  %73 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp71, label %if.then73, label %if.then80

if.then73:                                        ; preds = %land.lhs.true66
  br i1 %cmp16.i139244, label %for.body.i142.preheader, label %DrawSegment.exit150

for.body.i142.preheader:                          ; preds = %if.then73
  %xtraiter302 = and i64 %71, 1
  %lcmp.mod303 = icmp eq i64 %xtraiter302, 0
  br i1 %lcmp.mod303, label %for.body.i142.prol.loopexit.unr-lcssa, label %for.body.i142.prol.preheader

for.body.i142.prol.preheader:                     ; preds = %for.body.i142.preheader
  br label %for.body.i142.prol

for.body.i142.prol:                               ; preds = %for.body.i142.prol.preheader
  %add19.i134.prol = add i64 %73, %i.1248
  %arrayidx20.i135.prol = getelementptr inbounds i8, i8* %3, i64 %add19.i134.prol
  %74 = load i8, i8* %arrayidx20.i135.prol, align 1, !tbaa !7
  %or22.i136.prol = or i8 %74, 12
  store i8 %or22.i136.prol, i8* %arrayidx20.i135.prol, align 1, !tbaa !7
  %75 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i142.prol.loopexit.unr-lcssa

for.body.i142.prol.loopexit.unr-lcssa:            ; preds = %for.body.i142.preheader, %for.body.i142.prol
  %.lcssa294.unr.ph = phi i64 [ %75, %for.body.i142.prol ], [ undef, %for.body.i142.preheader ]
  %.unr304.ph = phi i64 [ %75, %for.body.i142.prol ], [ %73, %for.body.i142.preheader ]
  %y.0.i137245.unr.ph = phi i64 [ 2, %for.body.i142.prol ], [ 1, %for.body.i142.preheader ]
  br label %for.body.i142.prol.loopexit

for.body.i142.prol.loopexit:                      ; preds = %for.body.i142.prol.loopexit.unr-lcssa
  %76 = icmp eq i64 %71, 1
  br i1 %76, label %DrawSegment.exit150.loopexit, label %for.body.i142.preheader.new

for.body.i142.preheader.new:                      ; preds = %for.body.i142.prol.loopexit
  br label %for.body.i142

for.body.i142:                                    ; preds = %for.body.i142, %for.body.i142.preheader.new
  %77 = phi i64 [ %.unr304.ph, %for.body.i142.preheader.new ], [ %81, %for.body.i142 ]
  %y.0.i137245 = phi i64 [ %y.0.i137245.unr.ph, %for.body.i142.preheader.new ], [ %y.0.i137.1, %for.body.i142 ]
  %mul18.i141 = mul i64 %77, %y.0.i137245
  %add19.i134 = add i64 %mul18.i141, %i.1248
  %arrayidx20.i135 = getelementptr inbounds i8, i8* %3, i64 %add19.i134
  %78 = load i8, i8* %arrayidx20.i135, align 1, !tbaa !7
  %or22.i136 = or i8 %78, 12
  store i8 %or22.i136, i8* %arrayidx20.i135, align 1, !tbaa !7
  %y.0.i137 = add nuw i64 %y.0.i137245, 1
  %79 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i141.1 = mul i64 %79, %y.0.i137
  %add19.i134.1 = add i64 %mul18.i141.1, %i.1248
  %arrayidx20.i135.1 = getelementptr inbounds i8, i8* %3, i64 %add19.i134.1
  %80 = load i8, i8* %arrayidx20.i135.1, align 1, !tbaa !7
  %or22.i136.1 = or i8 %80, 12
  store i8 %or22.i136.1, i8* %arrayidx20.i135.1, align 1, !tbaa !7
  %y.0.i137.1 = add i64 %y.0.i137245, 2
  %81 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %exitcond270.1 = icmp eq i64 %y.0.i137.1, %add74
  br i1 %exitcond270.1, label %DrawSegment.exit150.loopexit.unr-lcssa, label %for.body.i142

DrawSegment.exit150.loopexit.unr-lcssa:           ; preds = %for.body.i142
  br label %DrawSegment.exit150.loopexit

DrawSegment.exit150.loopexit:                     ; preds = %for.body.i142.prol.loopexit, %DrawSegment.exit150.loopexit.unr-lcssa
  %.lcssa294 = phi i64 [ %.lcssa294.unr.ph, %for.body.i142.prol.loopexit ], [ %81, %DrawSegment.exit150.loopexit.unr-lcssa ]
  br label %DrawSegment.exit150

DrawSegment.exit150:                              ; preds = %DrawSegment.exit150.loopexit, %if.then73
  %.lcssa207 = phi i64 [ %73, %if.then73 ], [ %.lcssa294, %DrawSegment.exit150.loopexit ]
  %mul30.i143 = mul i64 %.lcssa207, %add74
  %add31.i144 = add i64 %mul30.i143, %i.1248
  %arrayidx79.i148 = getelementptr inbounds i8, i8* %3, i64 %add31.i144
  %82 = load i8, i8* %arrayidx79.i148, align 1, !tbaa !7
  %or81.i149 = or i8 %82, 4
  store i8 %or81.i149, i8* %arrayidx79.i148, align 1, !tbaa !7
  br label %for.inc110

if.then80:                                        ; preds = %land.lhs.true66
  br i1 %cmp16.i139244, label %for.body.i120.preheader, label %DrawSegment.exit128

for.body.i120.preheader:                          ; preds = %if.then80
  %xtraiter299 = and i64 %71, 1
  %lcmp.mod300 = icmp eq i64 %xtraiter299, 0
  br i1 %lcmp.mod300, label %for.body.i120.prol.loopexit.unr-lcssa, label %for.body.i120.prol.preheader

for.body.i120.prol.preheader:                     ; preds = %for.body.i120.preheader
  br label %for.body.i120.prol

for.body.i120.prol:                               ; preds = %for.body.i120.prol.preheader
  %add19.i112.prol = add i64 %73, %i.1248
  %arrayidx20.i113.prol = getelementptr inbounds i8, i8* %3, i64 %add19.i112.prol
  %83 = load i8, i8* %arrayidx20.i113.prol, align 1, !tbaa !7
  %or22.i114.prol = or i8 %83, 12
  store i8 %or22.i114.prol, i8* %arrayidx20.i113.prol, align 1, !tbaa !7
  %84 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i120.prol.loopexit.unr-lcssa

for.body.i120.prol.loopexit.unr-lcssa:            ; preds = %for.body.i120.preheader, %for.body.i120.prol
  %.lcssa293.unr.ph = phi i64 [ %84, %for.body.i120.prol ], [ undef, %for.body.i120.preheader ]
  %.unr301.ph = phi i64 [ %84, %for.body.i120.prol ], [ %73, %for.body.i120.preheader ]
  %y.0.i115239.unr.ph = phi i64 [ 2, %for.body.i120.prol ], [ 1, %for.body.i120.preheader ]
  br label %for.body.i120.prol.loopexit

for.body.i120.prol.loopexit:                      ; preds = %for.body.i120.prol.loopexit.unr-lcssa
  %85 = icmp eq i64 %71, 1
  br i1 %85, label %DrawSegment.exit128.loopexit, label %for.body.i120.preheader.new

for.body.i120.preheader.new:                      ; preds = %for.body.i120.prol.loopexit
  br label %for.body.i120

for.body.i120:                                    ; preds = %for.body.i120, %for.body.i120.preheader.new
  %86 = phi i64 [ %.unr301.ph, %for.body.i120.preheader.new ], [ %90, %for.body.i120 ]
  %y.0.i115239 = phi i64 [ %y.0.i115239.unr.ph, %for.body.i120.preheader.new ], [ %y.0.i115.1, %for.body.i120 ]
  %mul18.i119 = mul i64 %86, %y.0.i115239
  %add19.i112 = add i64 %mul18.i119, %i.1248
  %arrayidx20.i113 = getelementptr inbounds i8, i8* %3, i64 %add19.i112
  %87 = load i8, i8* %arrayidx20.i113, align 1, !tbaa !7
  %or22.i114 = or i8 %87, 12
  store i8 %or22.i114, i8* %arrayidx20.i113, align 1, !tbaa !7
  %y.0.i115 = add nuw i64 %y.0.i115239, 1
  %88 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i119.1 = mul i64 %88, %y.0.i115
  %add19.i112.1 = add i64 %mul18.i119.1, %i.1248
  %arrayidx20.i113.1 = getelementptr inbounds i8, i8* %3, i64 %add19.i112.1
  %89 = load i8, i8* %arrayidx20.i113.1, align 1, !tbaa !7
  %or22.i114.1 = or i8 %89, 12
  store i8 %or22.i114.1, i8* %arrayidx20.i113.1, align 1, !tbaa !7
  %y.0.i115.1 = add i64 %y.0.i115239, 2
  %90 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %exitcond269.1 = icmp eq i64 %y.0.i115.1, %add74
  br i1 %exitcond269.1, label %DrawSegment.exit128.loopexit.unr-lcssa, label %for.body.i120

DrawSegment.exit128.loopexit.unr-lcssa:           ; preds = %for.body.i120
  br label %DrawSegment.exit128.loopexit

DrawSegment.exit128.loopexit:                     ; preds = %for.body.i120.prol.loopexit, %DrawSegment.exit128.loopexit.unr-lcssa
  %.lcssa293 = phi i64 [ %.lcssa293.unr.ph, %for.body.i120.prol.loopexit ], [ %90, %DrawSegment.exit128.loopexit.unr-lcssa ]
  br label %DrawSegment.exit128

DrawSegment.exit128:                              ; preds = %DrawSegment.exit128.loopexit, %if.then80
  %.lcssa206 = phi i64 [ %73, %if.then80 ], [ %.lcssa293, %DrawSegment.exit128.loopexit ]
  %mul30.i121 = mul i64 %.lcssa206, %add74
  %add31.i122 = add i64 %mul30.i121, %i.1248
  %arrayidx79.i126 = getelementptr inbounds i8, i8* %3, i64 %add31.i122
  %91 = load i8, i8* %arrayidx79.i126, align 1, !tbaa !7
  %or81.i127 = or i8 %91, 4
  store i8 %or81.i127, i8* %arrayidx79.i126, align 1, !tbaa !7
  %92 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %93 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx82 = getelementptr inbounds i64, i64* %93, i64 %i.1248
  %94 = load i64, i64* %arrayidx82, align 8, !tbaa !1
  %arrayidx83 = getelementptr inbounds i64, i64* %92, i64 %94
  %95 = load i64, i64* %arrayidx83, align 8, !tbaa !1
  %96 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i104 = mul i64 %96, %95
  %add.i105 = add i64 %mul.i104, %i.1248
  %arrayidx.i106 = getelementptr inbounds i8, i8* %6, i64 %add.i105
  store i8 1, i8* %arrayidx.i106, align 1, !tbaa !7
  br label %for.inc110

if.else84:                                        ; preds = %if.else61
  %97 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx86 = getelementptr inbounds i64, i64* %97, i64 %66
  %98 = load i64, i64* %arrayidx86, align 8, !tbaa !1
  %arrayidx88 = getelementptr inbounds i64, i64* %97, i64 %28
  %99 = load i64, i64* %arrayidx88, align 8, !tbaa !1
  %cmp89 = icmp ult i64 %98, %99
  br i1 %cmp89, label %if.then91, label %if.else101

if.then91:                                        ; preds = %if.else84
  %arrayidx20.i88222 = getelementptr inbounds i8, i8* %3, i64 %i.1248
  %100 = load i8, i8* %arrayidx20.i88222, align 1, !tbaa !7
  %or22.i89223 = or i8 %100, 8
  store i8 %or22.i89223, i8* %arrayidx20.i88222, align 1, !tbaa !7
  %cmp16.i92224 = icmp ugt i64 %98, 1
  %101 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i92224, label %for.body.i95.preheader, label %DrawSegment.exit103

for.body.i95.preheader:                           ; preds = %if.then91
  %102 = and i64 %98, 1
  %lcmp.mod297 = icmp eq i64 %102, 0
  br i1 %lcmp.mod297, label %for.body.i95.prol.preheader, label %for.body.i95.prol.loopexit.unr-lcssa

for.body.i95.prol.preheader:                      ; preds = %for.body.i95.preheader
  br label %for.body.i95.prol

for.body.i95.prol:                                ; preds = %for.body.i95.prol.preheader
  %add19.i87.prol = add i64 %101, %i.1248
  %arrayidx20.i88.prol = getelementptr inbounds i8, i8* %3, i64 %add19.i87.prol
  %103 = load i8, i8* %arrayidx20.i88.prol, align 1, !tbaa !7
  %or22.i89.prol = or i8 %103, 12
  store i8 %or22.i89.prol, i8* %arrayidx20.i88.prol, align 1, !tbaa !7
  %104 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i95.prol.loopexit.unr-lcssa

for.body.i95.prol.loopexit.unr-lcssa:             ; preds = %for.body.i95.preheader, %for.body.i95.prol
  %.lcssa291.unr.ph = phi i64 [ %104, %for.body.i95.prol ], [ undef, %for.body.i95.preheader ]
  %.unr298.ph = phi i64 [ %104, %for.body.i95.prol ], [ %101, %for.body.i95.preheader ]
  %y.0.i90225.unr.ph = phi i64 [ 2, %for.body.i95.prol ], [ 1, %for.body.i95.preheader ]
  br label %for.body.i95.prol.loopexit

for.body.i95.prol.loopexit:                       ; preds = %for.body.i95.prol.loopexit.unr-lcssa
  %105 = icmp eq i64 %98, 2
  br i1 %105, label %DrawSegment.exit103.loopexit, label %for.body.i95.preheader.new

for.body.i95.preheader.new:                       ; preds = %for.body.i95.prol.loopexit
  br label %for.body.i95

for.body.i95:                                     ; preds = %for.body.i95, %for.body.i95.preheader.new
  %106 = phi i64 [ %.unr298.ph, %for.body.i95.preheader.new ], [ %110, %for.body.i95 ]
  %y.0.i90225 = phi i64 [ %y.0.i90225.unr.ph, %for.body.i95.preheader.new ], [ %y.0.i90.1, %for.body.i95 ]
  %mul18.i94 = mul i64 %106, %y.0.i90225
  %add19.i87 = add i64 %mul18.i94, %i.1248
  %arrayidx20.i88 = getelementptr inbounds i8, i8* %3, i64 %add19.i87
  %107 = load i8, i8* %arrayidx20.i88, align 1, !tbaa !7
  %or22.i89 = or i8 %107, 12
  store i8 %or22.i89, i8* %arrayidx20.i88, align 1, !tbaa !7
  %y.0.i90 = add nuw i64 %y.0.i90225, 1
  %108 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i94.1 = mul i64 %108, %y.0.i90
  %add19.i87.1 = add i64 %mul18.i94.1, %i.1248
  %arrayidx20.i88.1 = getelementptr inbounds i8, i8* %3, i64 %add19.i87.1
  %109 = load i8, i8* %arrayidx20.i88.1, align 1, !tbaa !7
  %or22.i89.1 = or i8 %109, 12
  store i8 %or22.i89.1, i8* %arrayidx20.i88.1, align 1, !tbaa !7
  %y.0.i90.1 = add i64 %y.0.i90225, 2
  %110 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %exitcond268.1 = icmp eq i64 %y.0.i90.1, %98
  br i1 %exitcond268.1, label %DrawSegment.exit103.loopexit.unr-lcssa, label %for.body.i95

DrawSegment.exit103.loopexit.unr-lcssa:           ; preds = %for.body.i95
  br label %DrawSegment.exit103.loopexit

DrawSegment.exit103.loopexit:                     ; preds = %for.body.i95.prol.loopexit, %DrawSegment.exit103.loopexit.unr-lcssa
  %.lcssa291 = phi i64 [ %.lcssa291.unr.ph, %for.body.i95.prol.loopexit ], [ %110, %DrawSegment.exit103.loopexit.unr-lcssa ]
  br label %DrawSegment.exit103

DrawSegment.exit103:                              ; preds = %DrawSegment.exit103.loopexit, %if.then91
  %.lcssa204 = phi i64 [ %101, %if.then91 ], [ %.lcssa291, %DrawSegment.exit103.loopexit ]
  %mul30.i96 = mul i64 %.lcssa204, %98
  %add31.i97 = add i64 %mul30.i96, %i.1248
  %arrayidx79.i101 = getelementptr inbounds i8, i8* %3, i64 %add31.i97
  %111 = load i8, i8* %arrayidx79.i101, align 1, !tbaa !7
  %or81.i102 = or i8 %111, 4
  store i8 %or81.i102, i8* %arrayidx79.i101, align 1, !tbaa !7
  %112 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %113 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx94 = getelementptr inbounds i64, i64* %113, i64 %i.1248
  %114 = load i64, i64* %arrayidx94, align 8, !tbaa !1
  %arrayidx95 = getelementptr inbounds i64, i64* %112, i64 %114
  %115 = load i64, i64* %arrayidx95, align 8, !tbaa !1
  %116 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i79 = mul i64 %116, %115
  %add.i80 = add i64 %mul.i79, %i.1248
  %arrayidx.i81 = getelementptr inbounds i8, i8* %6, i64 %add.i80
  store i8 1, i8* %arrayidx.i81, align 1, !tbaa !7
  %117 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %118 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx96 = getelementptr inbounds i64, i64* %118, i64 %i.1248
  %119 = load i64, i64* %arrayidx96, align 8, !tbaa !1
  %arrayidx97 = getelementptr inbounds i64, i64* %117, i64 %119
  %120 = load i64, i64* %arrayidx97, align 8, !tbaa !1
  %121 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add98 = add i64 %121, 1
  %cmp1.i = icmp ult i64 %120, %add98
  %cond.i = select i1 %cmp1.i, i64 %120, i64 %add98
  %122 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i58 = mul i64 %cond.i, %122
  %add19.i63227 = add i64 %mul.i58, %i.1248
  %arrayidx20.i64228 = getelementptr inbounds i8, i8* %3, i64 %add19.i63227
  %123 = load i8, i8* %arrayidx20.i64228, align 1, !tbaa !7
  %or22.i65229 = or i8 %123, 8
  store i8 %or22.i65229, i8* %arrayidx20.i64228, align 1, !tbaa !7
  %y.0.i66230 = add i64 %cond.i, 1
  %cond15.i = select i1 %cmp1.i, i64 %add98, i64 %120
  %cmp16.i67231 = icmp ult i64 %y.0.i66230, %cond15.i
  %124 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i67231, label %for.body.i70.preheader, label %DrawSegment.exit78

for.body.i70.preheader:                           ; preds = %DrawSegment.exit103
  br label %for.body.i70

for.body.i70:                                     ; preds = %for.body.i70.preheader, %for.body.i70
  %125 = phi i64 [ %127, %for.body.i70 ], [ %124, %for.body.i70.preheader ]
  %y.0.i66232 = phi i64 [ %y.0.i66, %for.body.i70 ], [ %y.0.i66230, %for.body.i70.preheader ]
  %mul18.i69 = mul i64 %125, %y.0.i66232
  %add19.i63 = add i64 %mul18.i69, %i.1248
  %arrayidx20.i64 = getelementptr inbounds i8, i8* %3, i64 %add19.i63
  %126 = load i8, i8* %arrayidx20.i64, align 1, !tbaa !7
  %or22.i65 = or i8 %126, 12
  store i8 %or22.i65, i8* %arrayidx20.i64, align 1, !tbaa !7
  %y.0.i66 = add nuw i64 %y.0.i66232, 1
  %cmp16.i67 = icmp ult i64 %y.0.i66, %cond15.i
  %127 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i67, label %for.body.i70, label %DrawSegment.exit78.loopexit

DrawSegment.exit78.loopexit:                      ; preds = %for.body.i70
  br label %DrawSegment.exit78

DrawSegment.exit78:                               ; preds = %DrawSegment.exit78.loopexit, %DrawSegment.exit103
  %.lcssa205 = phi i64 [ %124, %DrawSegment.exit103 ], [ %127, %DrawSegment.exit78.loopexit ]
  %mul30.i71 = mul i64 %.lcssa205, %cond15.i
  %add31.i72 = add i64 %mul30.i71, %i.1248
  %arrayidx79.i76 = getelementptr inbounds i8, i8* %3, i64 %add31.i72
  %128 = load i8, i8* %arrayidx79.i76, align 1, !tbaa !7
  %or81.i77 = or i8 %128, 4
  store i8 %or81.i77, i8* %arrayidx79.i76, align 1, !tbaa !7
  %129 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %130 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx99 = getelementptr inbounds i64, i64* %130, i64 %i.1248
  %131 = load i64, i64* %arrayidx99, align 8, !tbaa !1
  %arrayidx100 = getelementptr inbounds i64, i64* %129, i64 %131
  %132 = load i64, i64* %arrayidx100, align 8, !tbaa !1
  %133 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i57 = mul i64 %133, %132
  %add.i = add i64 %mul.i57, %i.1248
  %arrayidx.i = getelementptr inbounds i8, i8* %6, i64 %add.i
  store i8 1, i8* %arrayidx.i, align 1, !tbaa !7
  br label %for.inc110

if.else101:                                       ; preds = %if.else84
  %arrayidx102 = getelementptr inbounds i8, i8* %9, i64 %i.1248
  store i8 1, i8* %arrayidx102, align 1, !tbaa !7
  %inc103 = add nsw i32 %numLeft.0250, 1
  br label %for.inc110

for.inc110:                                       ; preds = %land.lhs.true, %DrawSegment.exit177, %DrawSegment.exit128, %if.else101, %DrawSegment.exit78, %DrawSegment.exit150, %DrawSegment.exit202
  %numLeft.1 = phi i32 [ %numLeft.0250, %land.lhs.true ], [ %numLeft.0250, %DrawSegment.exit202 ], [ %numLeft.0250, %DrawSegment.exit177 ], [ %numLeft.0250, %DrawSegment.exit150 ], [ %numLeft.0250, %DrawSegment.exit128 ], [ %numLeft.0250, %DrawSegment.exit78 ], [ %inc103, %if.else101 ]
  %inc111 = add i64 %i.1248, 1
  %134 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp25 = icmp ugt i64 %inc111, %134
  br i1 %cmp25, label %for.end112.loopexit, label %for.body27

for.end112.loopexit:                              ; preds = %for.inc110
  br label %for.end112

for.end112:                                       ; preds = %for.end112.loopexit, %for.cond24.preheader
  %numLeft.0.lcssa = phi i32 [ 0, %for.cond24.preheader ], [ %numLeft.1, %for.end112.loopexit ]
  ret i32 %numLeft.0.lcssa
}

; Function Attrs: argmemonly nounwind
declare void @llvm.memset.p0i8.i64(i8* nocapture writeonly, i8, i64, i32, i1) #5

; Function Attrs: norecurse nounwind uwtable
define i32 @Maze1() local_unnamed_addr #3 {
entry:
  %0 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp41 = icmp eq i64 %0, 0
  br i1 %cmp41, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %1 = phi i64 [ %37, %for.inc ], [ %0, %for.body.preheader ]
  %i.043 = phi i64 [ %inc53, %for.inc ], [ 1, %for.body.preheader ]
  %numLeft.042 = phi i32 [ %numLeft.1, %for.inc ], [ 0, %for.body.preheader ]
  %2 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i8, i8* %2, i64 %i.043
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !7
  %tobool = icmp eq i8 %3, 0
  br i1 %tobool, label %for.inc, label %if.then

if.then:                                          ; preds = %for.body
  %4 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %5 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx1 = getelementptr inbounds i64, i64* %5, i64 %i.043
  %6 = load i64, i64* %arrayidx1, align 8, !tbaa !1
  %arrayidx2 = getelementptr inbounds i64, i64* %4, i64 %6
  %7 = load i64, i64* %arrayidx2, align 8, !tbaa !1
  %8 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx3 = getelementptr inbounds i64, i64* %8, i64 %i.043
  %9 = load i64, i64* %arrayidx3, align 8, !tbaa !1
  %arrayidx4 = getelementptr inbounds i64, i64* %4, i64 %9
  %10 = load i64, i64* %arrayidx4, align 8, !tbaa !1
  %cmp5 = icmp ugt i64 %i.043, 1
  %cmp6 = icmp ugt i64 %10, 1
  %or.cond = and i1 %cmp5, %cmp6
  br i1 %or.cond, label %land.lhs.true7, label %if.else

land.lhs.true7:                                   ; preds = %if.then
  %11 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add = add i64 %11, 1
  %call = tail call fastcc i32 @Maze1Mech(i64 %i.043, i64 %add, i64 %10, i64 0, i64 %7, i32 -1, i32 -1)
  %tobool8 = icmp eq i32 %call, 0
  br i1 %tobool8, label %land.lhs.true7.if.else_crit_edge, label %if.then9

land.lhs.true7.if.else_crit_edge:                 ; preds = %land.lhs.true7
  %.pre = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %if.else

if.then9:                                         ; preds = %land.lhs.true7
  %12 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx10 = getelementptr inbounds i8, i8* %12, i64 %i.043
  store i8 0, i8* %arrayidx10, align 1, !tbaa !7
  %13 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx11 = getelementptr inbounds i64, i64* %13, i64 %i.043
  %14 = load i64, i64* %arrayidx11, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %14)
  %15 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx12 = getelementptr inbounds i64, i64* %15, i64 %i.043
  %16 = load i64, i64* %arrayidx12, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %16)
  br label %for.inc

if.else:                                          ; preds = %land.lhs.true7.if.else_crit_edge, %if.then
  %17 = phi i64 [ %.pre, %land.lhs.true7.if.else_crit_edge ], [ %1, %if.then ]
  %cmp13 = icmp ult i64 %i.043, %17
  %or.cond1 = and i1 %cmp6, %cmp13
  br i1 %or.cond1, label %land.lhs.true16, label %if.else24

land.lhs.true16:                                  ; preds = %if.else
  %18 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add17 = add i64 %18, 1
  %call18 = tail call fastcc i32 @Maze1Mech(i64 %i.043, i64 %add17, i64 %10, i64 0, i64 %7, i32 1, i32 -1)
  %tobool19 = icmp eq i32 %call18, 0
  br i1 %tobool19, label %if.else24, label %if.then20

if.then20:                                        ; preds = %land.lhs.true16
  %19 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx21 = getelementptr inbounds i8, i8* %19, i64 %i.043
  store i8 0, i8* %arrayidx21, align 1, !tbaa !7
  %20 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx22 = getelementptr inbounds i64, i64* %20, i64 %i.043
  %21 = load i64, i64* %arrayidx22, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %21)
  %22 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx23 = getelementptr inbounds i64, i64* %22, i64 %i.043
  %23 = load i64, i64* %arrayidx23, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %23)
  br label %for.inc

if.else24:                                        ; preds = %land.lhs.true16, %if.else
  br i1 %cmp5, label %land.lhs.true26, label %if.else36

land.lhs.true26:                                  ; preds = %if.else24
  %24 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp27 = icmp ult i64 %7, %24
  br i1 %cmp27, label %land.lhs.true28, label %if.else36

land.lhs.true28:                                  ; preds = %land.lhs.true26
  %add29 = add i64 %24, 1
  %call30 = tail call fastcc i32 @Maze1Mech(i64 %i.043, i64 0, i64 %7, i64 %add29, i64 %10, i32 -1, i32 1)
  %tobool31 = icmp eq i32 %call30, 0
  br i1 %tobool31, label %if.else36, label %if.then32

if.then32:                                        ; preds = %land.lhs.true28
  %25 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx33 = getelementptr inbounds i8, i8* %25, i64 %i.043
  store i8 0, i8* %arrayidx33, align 1, !tbaa !7
  %26 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx34 = getelementptr inbounds i64, i64* %26, i64 %i.043
  %27 = load i64, i64* %arrayidx34, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %27)
  %28 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx35 = getelementptr inbounds i64, i64* %28, i64 %i.043
  %29 = load i64, i64* %arrayidx35, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %29)
  br label %for.inc

if.else36:                                        ; preds = %land.lhs.true28, %land.lhs.true26, %if.else24
  %30 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp37 = icmp ult i64 %i.043, %30
  br i1 %cmp37, label %land.lhs.true38, label %if.else48

land.lhs.true38:                                  ; preds = %if.else36
  %31 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp39 = icmp ult i64 %7, %31
  br i1 %cmp39, label %land.lhs.true40, label %if.else48

land.lhs.true40:                                  ; preds = %land.lhs.true38
  %add41 = add i64 %31, 1
  %call42 = tail call fastcc i32 @Maze1Mech(i64 %i.043, i64 0, i64 %7, i64 %add41, i64 %10, i32 1, i32 1)
  %tobool43 = icmp eq i32 %call42, 0
  br i1 %tobool43, label %if.else48, label %if.then44

if.then44:                                        ; preds = %land.lhs.true40
  %32 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx45 = getelementptr inbounds i8, i8* %32, i64 %i.043
  store i8 0, i8* %arrayidx45, align 1, !tbaa !7
  %33 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx46 = getelementptr inbounds i64, i64* %33, i64 %i.043
  %34 = load i64, i64* %arrayidx46, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %34)
  %35 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx47 = getelementptr inbounds i64, i64* %35, i64 %i.043
  %36 = load i64, i64* %arrayidx47, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %36)
  br label %for.inc

if.else48:                                        ; preds = %land.lhs.true40, %land.lhs.true38, %if.else36
  %inc = add nsw i32 %numLeft.042, 1
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then20, %if.then44, %if.else48, %if.then32, %if.then9
  %numLeft.1 = phi i32 [ %numLeft.042, %if.then9 ], [ %numLeft.042, %if.then20 ], [ %numLeft.042, %if.then32 ], [ %numLeft.042, %if.then44 ], [ %inc, %if.else48 ], [ %numLeft.042, %for.body ]
  %inc53 = add i64 %i.043, 1
  %37 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc53, %37
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %numLeft.0.lcssa = phi i32 [ 0, %entry ], [ %numLeft.1, %for.end.loopexit ]
  ret i32 %numLeft.0.lcssa
}

; Function Attrs: norecurse nounwind uwtable
define internal fastcc i32 @Maze1Mech(i64 %i, i64 %s1, i64 %s2, i64 %b1, i64 %b2, i32 %bXdelta, i32 %bYdelta) unnamed_addr #3 {
entry:
  %0 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %cmp1.i = icmp ult i64 %s1, %s2
  %cond.i = select i1 %cmp1.i, i64 %s1, i64 %s2
  %1 = load i64, i64* @channelColumns, align 8
  %mul.i = mul i64 %1, %cond.i
  %add.i = add i64 %mul.i, %i
  %cond11.i = select i1 %cmp1.i, i64 %s2, i64 %s1
  br label %for.body.i

for.body.i:                                       ; preds = %entry, %for.inc.i
  %index.0.i305 = phi i64 [ %add.i, %entry ], [ %add14.i, %for.inc.i ]
  %y.0.i304 = phi i64 [ %cond.i, %entry ], [ %inc.i, %for.inc.i ]
  %arrayidx.i = getelementptr inbounds i8, i8* %0, i64 %index.0.i305
  %2 = load i8, i8* %arrayidx.i, align 1, !tbaa !7
  %tobool.i = icmp eq i8 %2, 0
  br i1 %tobool.i, label %for.inc.i, label %return.loopexit346

for.inc.i:                                        ; preds = %for.body.i
  %inc.i = add i64 %y.0.i304, 1
  %add14.i = add i64 %1, %index.0.i305
  %cmp12.i = icmp ugt i64 %inc.i, %cond11.i
  br i1 %cmp12.i, label %land.lhs.true, label %for.body.i

land.lhs.true:                                    ; preds = %for.inc.i
  %conv = sext i32 %bYdelta to i64
  %add = add i64 %conv, %s2
  %cmp1.i212 = icmp ugt i64 %add, %b1
  %cond.i213 = select i1 %cmp1.i212, i64 %b1, i64 %add
  %mul.i214 = mul i64 %1, %cond.i213
  %add.i215 = add i64 %mul.i214, %i
  %cond11.i219 = select i1 %cmp1.i212, i64 %add, i64 %b1
  br label %for.body.i224

for.body.i224:                                    ; preds = %land.lhs.true, %for.inc.i227
  %index.0.i218303 = phi i64 [ %add.i215, %land.lhs.true ], [ %add14.i226, %for.inc.i227 ]
  %y.0.i217302 = phi i64 [ %cond.i213, %land.lhs.true ], [ %inc.i225, %for.inc.i227 ]
  %arrayidx.i222 = getelementptr inbounds i8, i8* %0, i64 %index.0.i218303
  %3 = load i8, i8* %arrayidx.i222, align 1, !tbaa !7
  %tobool.i223 = icmp eq i8 %3, 0
  br i1 %tobool.i223, label %for.inc.i227, label %return.loopexit345

for.inc.i227:                                     ; preds = %for.body.i224
  %inc.i225 = add i64 %y.0.i217302, 1
  %add14.i226 = add i64 %1, %index.0.i218303
  %cmp12.i220 = icmp ugt i64 %inc.i225, %cond11.i219
  br i1 %cmp12.i220, label %land.lhs.true3, label %for.body.i224

land.lhs.true3:                                   ; preds = %for.inc.i227
  %conv6 = sext i32 %bXdelta to i64
  %add7 = add i64 %conv6, %i
  %cmp.i193 = icmp eq i32 %bXdelta, 0
  %mul.i194 = mul i64 %1, %add
  br i1 %cmp.i193, label %if.then.i196, label %if.else.i208

if.then.i196:                                     ; preds = %land.lhs.true3
  %add.i195 = add i64 %mul.i194, %i
  br label %for.body.i203

for.body.i203:                                    ; preds = %if.then.i196, %for.inc.i206
  %index.0.i198299 = phi i64 [ %add.i195, %if.then.i196 ], [ %add14.i205, %for.inc.i206 ]
  %y.0.i197298 = phi i64 [ %add, %if.then.i196 ], [ %inc.i204, %for.inc.i206 ]
  %arrayidx.i201 = getelementptr inbounds i8, i8* %0, i64 %index.0.i198299
  %4 = load i8, i8* %arrayidx.i201, align 1, !tbaa !7
  %tobool.i202 = icmp eq i8 %4, 0
  br i1 %tobool.i202, label %for.inc.i206, label %return.loopexit342

for.inc.i206:                                     ; preds = %for.body.i203
  %inc.i204 = add i64 %y.0.i197298, 1
  %add14.i205 = add i64 %1, %index.0.i198299
  %cmp12.i199 = icmp ugt i64 %inc.i204, %add
  br i1 %cmp12.i199, label %land.lhs.true12.loopexit, label %for.body.i203

if.else.i208:                                     ; preds = %land.lhs.true3
  %cmp16.i207 = icmp ugt i64 %add7, %i
  %cond20.i = select i1 %cmp16.i207, i64 %i, i64 %add7
  %add21.i = add i64 %mul.i194, %cond20.i
  %cond32.i = select i1 %cmp16.i207, i64 %add7, i64 %i
  br label %for.body34.i

for.body34.i:                                     ; preds = %if.else.i208, %for.inc39.i
  %index.1.i301 = phi i64 [ %add21.i, %if.else.i208 ], [ %inc41.i, %for.inc39.i ]
  %x.0.i209300 = phi i64 [ %cond20.i, %if.else.i208 ], [ %inc40.i, %for.inc39.i ]
  %arrayidx35.i = getelementptr inbounds i8, i8* %0, i64 %index.1.i301
  %5 = load i8, i8* %arrayidx35.i, align 1, !tbaa !7
  %tobool36.i = icmp eq i8 %5, 0
  br i1 %tobool36.i, label %for.inc39.i, label %return.loopexit343

for.inc39.i:                                      ; preds = %for.body34.i
  %inc40.i = add i64 %x.0.i209300, 1
  %inc41.i = add i64 %index.1.i301, 1
  %cmp33.i = icmp ugt i64 %inc40.i, %cond32.i
  br i1 %cmp33.i, label %land.lhs.true12.loopexit344, label %for.body34.i

land.lhs.true12.loopexit:                         ; preds = %for.inc.i206
  br label %land.lhs.true12

land.lhs.true12.loopexit344:                      ; preds = %for.inc39.i
  br label %land.lhs.true12

land.lhs.true12:                                  ; preds = %land.lhs.true12.loopexit344, %land.lhs.true12.loopexit
  %cmp1.i175 = icmp ult i64 %add, %b2
  %cond.i176 = select i1 %cmp1.i175, i64 %add, i64 %b2
  %mul.i177 = mul i64 %1, %cond.i176
  %add.i178 = add i64 %mul.i177, %add7
  %cond11.i182 = select i1 %cmp1.i175, i64 %b2, i64 %add
  br label %for.body.i187

for.body.i187:                                    ; preds = %land.lhs.true12, %for.inc.i190
  %index.0.i181297 = phi i64 [ %add.i178, %land.lhs.true12 ], [ %add14.i189, %for.inc.i190 ]
  %y.0.i180296 = phi i64 [ %cond.i176, %land.lhs.true12 ], [ %inc.i188, %for.inc.i190 ]
  %arrayidx.i185 = getelementptr inbounds i8, i8* %0, i64 %index.0.i181297
  %6 = load i8, i8* %arrayidx.i185, align 1, !tbaa !7
  %tobool.i186 = icmp eq i8 %6, 0
  br i1 %tobool.i186, label %for.inc.i190, label %return.loopexit

for.inc.i190:                                     ; preds = %for.body.i187
  %inc.i188 = add i64 %y.0.i180296, 1
  %add14.i189 = add i64 %1, %index.0.i181297
  %cmp12.i183 = icmp ugt i64 %inc.i188, %cond11.i182
  br i1 %cmp12.i183, label %land.lhs.true21, label %for.body.i187

land.lhs.true21:                                  ; preds = %for.inc.i190
  %7 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx.i173 = getelementptr inbounds i64, i64* %7, i64 %add7
  %8 = load i64, i64* %arrayidx.i173, align 8, !tbaa !1
  %cmp.i174 = icmp eq i64 %8, 0
  br i1 %cmp.i174, label %if.then, label %land.lhs.true.i

land.lhs.true.i:                                  ; preds = %land.lhs.true21
  %9 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx1.i = getelementptr inbounds i64, i64* %9, i64 %add7
  %10 = load i64, i64* %arrayidx1.i, align 8, !tbaa !1
  %cmp2.i = icmp eq i64 %10, 0
  %cmp6.i = icmp eq i64 %8, %10
  %or.cond.i = or i1 %cmp2.i, %cmp6.i
  br i1 %or.cond.i, label %if.then, label %HasVCV.exit

HasVCV.exit:                                      ; preds = %land.lhs.true.i
  %11 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx8.i = getelementptr inbounds i64, i64* %11, i64 %8
  %12 = load i64, i64* %arrayidx8.i, align 8, !tbaa !1
  %arrayidx10.i = getelementptr inbounds i64, i64* %11, i64 %10
  %13 = load i64, i64* %arrayidx10.i, align 8, !tbaa !1
  %cmp11.i = icmp ugt i64 %12, %13
  br i1 %cmp11.i, label %return, label %if.then

if.then:                                          ; preds = %HasVCV.exit, %land.lhs.true21, %land.lhs.true.i
  %arrayidx20.i157290 = getelementptr inbounds i8, i8* %0, i64 %add.i
  %14 = load i8, i8* %arrayidx20.i157290, align 1, !tbaa !7
  %or22.i158291 = or i8 %14, 8
  store i8 %or22.i158291, i8* %arrayidx20.i157290, align 1, !tbaa !7
  %y.0.i159292 = add i64 %cond.i, 1
  %cmp16.i161293 = icmp ult i64 %y.0.i159292, %cond11.i
  %15 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i161293, label %for.body.i164.preheader, label %DrawSegment.exit172

for.body.i164.preheader:                          ; preds = %if.then
  br label %for.body.i164

for.body.i164:                                    ; preds = %for.body.i164.preheader, %for.body.i164
  %16 = phi i64 [ %18, %for.body.i164 ], [ %15, %for.body.i164.preheader ]
  %y.0.i159294 = phi i64 [ %y.0.i159, %for.body.i164 ], [ %y.0.i159292, %for.body.i164.preheader ]
  %mul18.i163 = mul i64 %16, %y.0.i159294
  %add19.i156 = add i64 %mul18.i163, %i
  %arrayidx20.i157 = getelementptr inbounds i8, i8* %0, i64 %add19.i156
  %17 = load i8, i8* %arrayidx20.i157, align 1, !tbaa !7
  %or22.i158 = or i8 %17, 12
  store i8 %or22.i158, i8* %arrayidx20.i157, align 1, !tbaa !7
  %y.0.i159 = add nuw i64 %y.0.i159294, 1
  %cmp16.i161 = icmp ult i64 %y.0.i159, %cond11.i
  %18 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i161, label %for.body.i164, label %DrawSegment.exit172.loopexit

DrawSegment.exit172.loopexit:                     ; preds = %for.body.i164
  br label %DrawSegment.exit172

DrawSegment.exit172:                              ; preds = %DrawSegment.exit172.loopexit, %if.then
  %.lcssa244 = phi i64 [ %15, %if.then ], [ %18, %DrawSegment.exit172.loopexit ]
  %mul30.i165 = mul i64 %.lcssa244, %cond11.i
  %add31.i166 = add i64 %mul30.i165, %i
  %arrayidx79.i170 = getelementptr inbounds i8, i8* %0, i64 %add31.i166
  %19 = load i8, i8* %arrayidx79.i170, align 1, !tbaa !7
  %or81.i171 = or i8 %19, 4
  store i8 %or81.i171, i8* %arrayidx79.i170, align 1, !tbaa !7
  %20 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  %21 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i146 = mul i64 %21, %s2
  %add.i147 = add i64 %mul.i146, %i
  %arrayidx.i148 = getelementptr inbounds i8, i8* %20, i64 %add.i147
  store i8 1, i8* %arrayidx.i148, align 1, !tbaa !7
  %22 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i124 = mul i64 %22, %cond.i213
  %add19.i129282 = add i64 %mul.i124, %i
  %arrayidx20.i130283 = getelementptr inbounds i8, i8* %0, i64 %add19.i129282
  %23 = load i8, i8* %arrayidx20.i130283, align 1, !tbaa !7
  %or22.i131284 = or i8 %23, 8
  store i8 %or22.i131284, i8* %arrayidx20.i130283, align 1, !tbaa !7
  %y.0.i132285 = add i64 %cond.i213, 1
  %cmp16.i134286 = icmp ult i64 %y.0.i132285, %cond11.i219
  %24 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i134286, label %for.body.i137.preheader, label %DrawSegment.exit145

for.body.i137.preheader:                          ; preds = %DrawSegment.exit172
  br label %for.body.i137

for.body.i137:                                    ; preds = %for.body.i137.preheader, %for.body.i137
  %25 = phi i64 [ %27, %for.body.i137 ], [ %24, %for.body.i137.preheader ]
  %y.0.i132287 = phi i64 [ %y.0.i132, %for.body.i137 ], [ %y.0.i132285, %for.body.i137.preheader ]
  %mul18.i136 = mul i64 %25, %y.0.i132287
  %add19.i129 = add i64 %mul18.i136, %i
  %arrayidx20.i130 = getelementptr inbounds i8, i8* %0, i64 %add19.i129
  %26 = load i8, i8* %arrayidx20.i130, align 1, !tbaa !7
  %or22.i131 = or i8 %26, 12
  store i8 %or22.i131, i8* %arrayidx20.i130, align 1, !tbaa !7
  %y.0.i132 = add nuw i64 %y.0.i132287, 1
  %cmp16.i134 = icmp ult i64 %y.0.i132, %cond11.i219
  %27 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i134, label %for.body.i137, label %DrawSegment.exit145.loopexit

DrawSegment.exit145.loopexit:                     ; preds = %for.body.i137
  br label %DrawSegment.exit145

DrawSegment.exit145:                              ; preds = %DrawSegment.exit145.loopexit, %DrawSegment.exit172
  %.lcssa243 = phi i64 [ %24, %DrawSegment.exit172 ], [ %27, %DrawSegment.exit145.loopexit ]
  %mul30.i138 = mul i64 %.lcssa243, %cond11.i219
  %add31.i139 = add i64 %mul30.i138, %i
  %arrayidx79.i143 = getelementptr inbounds i8, i8* %0, i64 %add31.i139
  %28 = load i8, i8* %arrayidx79.i143, align 1, !tbaa !7
  %or81.i144 = or i8 %28, 4
  store i8 %or81.i144, i8* %arrayidx79.i143, align 1, !tbaa !7
  %29 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i83 = mul i64 %29, %add
  br i1 %cmp.i193, label %if.then.i84, label %if.else.i105

if.then.i84:                                      ; preds = %DrawSegment.exit145
  %add19.i88268 = add i64 %mul.i83, %i
  %arrayidx20.i89269 = getelementptr inbounds i8, i8* %0, i64 %add19.i88268
  %30 = load i8, i8* %arrayidx20.i89269, align 1, !tbaa !7
  %or22.i90270 = or i8 %30, 8
  store i8 %or22.i90270, i8* %arrayidx20.i89269, align 1, !tbaa !7
  %cmp16.i92272 = icmp eq i64 %add, -1
  %31 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i92272, label %for.body.i95.preheader, label %for.end.i98

for.body.i95.preheader:                           ; preds = %if.then.i84
  br i1 true, label %for.body.i95.prol.preheader, label %for.body.i95.prol.loopexit

for.body.i95.prol.preheader:                      ; preds = %for.body.i95.preheader
  br label %for.body.i95.prol

for.body.i95.prol:                                ; preds = %for.body.i95.prol.preheader
  %arrayidx20.i89.prol = getelementptr inbounds i8, i8* %0, i64 %i
  %32 = load i8, i8* %arrayidx20.i89.prol, align 1, !tbaa !7
  %or22.i90.prol = or i8 %32, 12
  store i8 %or22.i90.prol, i8* %arrayidx20.i89.prol, align 1, !tbaa !7
  %33 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i95.prol.loopexit

for.body.i95.prol.loopexit:                       ; preds = %for.body.i95.prol, %for.body.i95.preheader
  %.lcssa350.unr = phi i64 [ undef, %for.body.i95.preheader ], [ %33, %for.body.i95.prol ]
  %.unr354 = phi i64 [ %31, %for.body.i95.preheader ], [ %33, %for.body.i95.prol ]
  %y.0.i91273.unr = phi i64 [ 0, %for.body.i95.preheader ], [ 1, %for.body.i95.prol ]
  br i1 false, label %for.end.i98.loopexit, label %for.body.i95.preheader.new

for.body.i95.preheader.new:                       ; preds = %for.body.i95.prol.loopexit
  br label %for.body.i95

for.body.i95:                                     ; preds = %for.body.i95, %for.body.i95.preheader.new
  %34 = phi i64 [ %.unr354, %for.body.i95.preheader.new ], [ %38, %for.body.i95 ]
  %y.0.i91273 = phi i64 [ %y.0.i91273.unr, %for.body.i95.preheader.new ], [ %y.0.i91.1, %for.body.i95 ]
  %mul18.i94 = mul i64 %34, %y.0.i91273
  %add19.i88 = add i64 %mul18.i94, %i
  %arrayidx20.i89 = getelementptr inbounds i8, i8* %0, i64 %add19.i88
  %35 = load i8, i8* %arrayidx20.i89, align 1, !tbaa !7
  %or22.i90 = or i8 %35, 12
  store i8 %or22.i90, i8* %arrayidx20.i89, align 1, !tbaa !7
  %y.0.i91 = add nuw i64 %y.0.i91273, 1
  %36 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i94.1 = mul i64 %36, %y.0.i91
  %add19.i88.1 = add i64 %mul18.i94.1, %i
  %arrayidx20.i89.1 = getelementptr inbounds i8, i8* %0, i64 %add19.i88.1
  %37 = load i8, i8* %arrayidx20.i89.1, align 1, !tbaa !7
  %or22.i90.1 = or i8 %37, 12
  store i8 %or22.i90.1, i8* %arrayidx20.i89.1, align 1, !tbaa !7
  %y.0.i91.1 = add i64 %y.0.i91273, 2
  %cmp16.i92.1 = icmp eq i64 %y.0.i91.1, -1
  %38 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i92.1, label %for.end.i98.loopexit.unr-lcssa, label %for.body.i95

for.end.i98.loopexit.unr-lcssa:                   ; preds = %for.body.i95
  br label %for.end.i98.loopexit

for.end.i98.loopexit:                             ; preds = %for.body.i95.prol.loopexit, %for.end.i98.loopexit.unr-lcssa
  %.lcssa350 = phi i64 [ %.lcssa350.unr, %for.body.i95.prol.loopexit ], [ %38, %for.end.i98.loopexit.unr-lcssa ]
  br label %for.end.i98

for.end.i98:                                      ; preds = %for.end.i98.loopexit, %if.then.i84
  %.lcssa242 = phi i64 [ %31, %if.then.i84 ], [ %.lcssa350, %for.end.i98.loopexit ]
  %mul30.i96 = mul i64 %.lcssa242, %add
  %add31.i97 = add i64 %mul30.i96, %i
  br label %DrawSegment.exit121

if.else.i105:                                     ; preds = %DrawSegment.exit145
  %cmp37.i100 = icmp ugt i64 %add7, %i
  %cond42.i101 = select i1 %cmp37.i100, i64 %i, i64 %add7
  %add43.i102 = add i64 %mul.i83, %cond42.i101
  %arrayidx44.i103 = getelementptr inbounds i8, i8* %0, i64 %add43.i102
  %39 = load i8, i8* %arrayidx44.i103, align 1, !tbaa !7
  %or46.i104 = or i8 %39, 2
  store i8 %or46.i104, i8* %arrayidx44.i103, align 1, !tbaa !7
  %x.0.i107275 = add i64 %cond42.i101, 1
  %cond61.i108 = select i1 %cmp37.i100, i64 %add7, i64 %i
  %cmp62.i109276 = icmp ult i64 %x.0.i107275, %cond61.i108
  %40 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i110277 = mul i64 %40, %add
  br i1 %cmp62.i109276, label %for.body64.i114.preheader, label %for.end70.i116

for.body64.i114.preheader:                        ; preds = %if.else.i105
  br label %for.body64.i114

for.body64.i114:                                  ; preds = %for.body64.i114.preheader, %for.body64.i114
  %mul65.i110279 = phi i64 [ %mul65.i110, %for.body64.i114 ], [ %mul65.i110277, %for.body64.i114.preheader ]
  %x.0.i107278 = phi i64 [ %x.0.i107, %for.body64.i114 ], [ %x.0.i107275, %for.body64.i114.preheader ]
  %add66.i112 = add i64 %mul65.i110279, %x.0.i107278
  %arrayidx67.i113 = getelementptr inbounds i8, i8* %0, i64 %add66.i112
  store i8 3, i8* %arrayidx67.i113, align 1, !tbaa !7
  %x.0.i107 = add nuw i64 %x.0.i107278, 1
  %cmp62.i109 = icmp ult i64 %x.0.i107, %cond61.i108
  %41 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i110 = mul i64 %41, %add
  br i1 %cmp62.i109, label %for.body64.i114, label %for.end70.i116.loopexit

for.end70.i116.loopexit:                          ; preds = %for.body64.i114
  br label %for.end70.i116

for.end70.i116:                                   ; preds = %for.end70.i116.loopexit, %if.else.i105
  %mul65.i110.lcssa = phi i64 [ %mul65.i110277, %if.else.i105 ], [ %mul65.i110, %for.end70.i116.loopexit ]
  %add78.i115 = add i64 %mul65.i110.lcssa, %cond61.i108
  br label %DrawSegment.exit121

DrawSegment.exit121:                              ; preds = %for.end.i98, %for.end70.i116
  %add78.sink.i117 = phi i64 [ %add78.i115, %for.end70.i116 ], [ %add31.i97, %for.end.i98 ]
  %.sink.i118 = phi i8 [ 1, %for.end70.i116 ], [ 4, %for.end.i98 ]
  %arrayidx79.i119 = getelementptr inbounds i8, i8* %0, i64 %add78.sink.i117
  %42 = load i8, i8* %arrayidx79.i119, align 1, !tbaa !7
  %or81.i120 = or i8 %42, %.sink.i118
  store i8 %or81.i120, i8* %arrayidx79.i119, align 1, !tbaa !7
  %43 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i61 = mul i64 %43, %cond.i176
  %add19.i66261 = add i64 %mul.i61, %add7
  %arrayidx20.i67262 = getelementptr inbounds i8, i8* %0, i64 %add19.i66261
  %44 = load i8, i8* %arrayidx20.i67262, align 1, !tbaa !7
  %or22.i68263 = or i8 %44, 8
  store i8 %or22.i68263, i8* %arrayidx20.i67262, align 1, !tbaa !7
  %y.0.i69264 = add i64 %cond.i176, 1
  %cmp16.i70265 = icmp ult i64 %y.0.i69264, %cond11.i182
  %45 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i70265, label %for.body.i73.preheader, label %DrawSegment.exit81

for.body.i73.preheader:                           ; preds = %DrawSegment.exit121
  br label %for.body.i73

for.body.i73:                                     ; preds = %for.body.i73.preheader, %for.body.i73
  %46 = phi i64 [ %48, %for.body.i73 ], [ %45, %for.body.i73.preheader ]
  %y.0.i69266 = phi i64 [ %y.0.i69, %for.body.i73 ], [ %y.0.i69264, %for.body.i73.preheader ]
  %mul18.i72 = mul i64 %46, %y.0.i69266
  %add19.i66 = add i64 %mul18.i72, %add7
  %arrayidx20.i67 = getelementptr inbounds i8, i8* %0, i64 %add19.i66
  %47 = load i8, i8* %arrayidx20.i67, align 1, !tbaa !7
  %or22.i68 = or i8 %47, 12
  store i8 %or22.i68, i8* %arrayidx20.i67, align 1, !tbaa !7
  %y.0.i69 = add nuw i64 %y.0.i69266, 1
  %cmp16.i70 = icmp ult i64 %y.0.i69, %cond11.i182
  %48 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i70, label %for.body.i73, label %DrawSegment.exit81.loopexit

DrawSegment.exit81.loopexit:                      ; preds = %for.body.i73
  br label %DrawSegment.exit81

DrawSegment.exit81:                               ; preds = %DrawSegment.exit81.loopexit, %DrawSegment.exit121
  %.lcssa241 = phi i64 [ %45, %DrawSegment.exit121 ], [ %48, %DrawSegment.exit81.loopexit ]
  %mul30.i74 = mul i64 %.lcssa241, %cond11.i182
  %add31.i75 = add i64 %mul30.i74, %add7
  %arrayidx79.i79 = getelementptr inbounds i8, i8* %0, i64 %add31.i75
  %49 = load i8, i8* %arrayidx79.i79, align 1, !tbaa !7
  %or81.i80 = or i8 %49, 4
  store i8 %or81.i80, i8* %arrayidx79.i79, align 1, !tbaa !7
  %50 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i56 = mul i64 %50, %b2
  %add.i57 = add i64 %mul.i56, %add7
  %arrayidx.i58 = getelementptr inbounds i8, i8* %20, i64 %add.i57
  store i8 1, i8* %arrayidx.i58, align 1, !tbaa !7
  %51 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %52 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i52 = mul i64 %52, %b2
  br i1 %cmp.i193, label %for.cond.i54.preheader, label %if.else.i

for.cond.i54.preheader:                           ; preds = %DrawSegment.exit81
  %add19.i248 = add i64 %mul.i52, %add7
  %arrayidx20.i249 = getelementptr inbounds i8, i8* %51, i64 %add19.i248
  %53 = load i8, i8* %arrayidx20.i249, align 1, !tbaa !7
  %or22.i250 = or i8 %53, 8
  store i8 %or22.i250, i8* %arrayidx20.i249, align 1, !tbaa !7
  %cmp16.i252 = icmp eq i64 %b2, -1
  %54 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i252, label %for.body.i55.preheader, label %for.end.i

for.body.i55.preheader:                           ; preds = %for.cond.i54.preheader
  br i1 true, label %for.body.i55.prol.preheader, label %for.body.i55.prol.loopexit

for.body.i55.prol.preheader:                      ; preds = %for.body.i55.preheader
  br label %for.body.i55.prol

for.body.i55.prol:                                ; preds = %for.body.i55.prol.preheader
  %add19.i.prol = add i64 %conv6, %i
  %arrayidx20.i.prol = getelementptr inbounds i8, i8* %51, i64 %add19.i.prol
  %55 = load i8, i8* %arrayidx20.i.prol, align 1, !tbaa !7
  %or22.i.prol = or i8 %55, 12
  store i8 %or22.i.prol, i8* %arrayidx20.i.prol, align 1, !tbaa !7
  %56 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i55.prol.loopexit

for.body.i55.prol.loopexit:                       ; preds = %for.body.i55.prol, %for.body.i55.preheader
  %.lcssa347.unr = phi i64 [ undef, %for.body.i55.preheader ], [ %56, %for.body.i55.prol ]
  %.unr = phi i64 [ %54, %for.body.i55.preheader ], [ %56, %for.body.i55.prol ]
  %y.0.i53253.unr = phi i64 [ 0, %for.body.i55.preheader ], [ 1, %for.body.i55.prol ]
  br i1 false, label %for.end.i.loopexit, label %for.body.i55.preheader.new

for.body.i55.preheader.new:                       ; preds = %for.body.i55.prol.loopexit
  br label %for.body.i55

for.body.i55:                                     ; preds = %for.body.i55, %for.body.i55.preheader.new
  %57 = phi i64 [ %.unr, %for.body.i55.preheader.new ], [ %61, %for.body.i55 ]
  %y.0.i53253 = phi i64 [ %y.0.i53253.unr, %for.body.i55.preheader.new ], [ %y.0.i53.1, %for.body.i55 ]
  %mul18.i = mul i64 %57, %y.0.i53253
  %add19.i = add i64 %mul18.i, %add7
  %arrayidx20.i = getelementptr inbounds i8, i8* %51, i64 %add19.i
  %58 = load i8, i8* %arrayidx20.i, align 1, !tbaa !7
  %or22.i = or i8 %58, 12
  store i8 %or22.i, i8* %arrayidx20.i, align 1, !tbaa !7
  %y.0.i53 = add nuw i64 %y.0.i53253, 1
  %59 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i.1 = mul i64 %59, %y.0.i53
  %add19.i.1 = add i64 %mul18.i.1, %add7
  %arrayidx20.i.1 = getelementptr inbounds i8, i8* %51, i64 %add19.i.1
  %60 = load i8, i8* %arrayidx20.i.1, align 1, !tbaa !7
  %or22.i.1 = or i8 %60, 12
  store i8 %or22.i.1, i8* %arrayidx20.i.1, align 1, !tbaa !7
  %y.0.i53.1 = add i64 %y.0.i53253, 2
  %cmp16.i.1 = icmp eq i64 %y.0.i53.1, -1
  %61 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i.1, label %for.end.i.loopexit.unr-lcssa, label %for.body.i55

for.end.i.loopexit.unr-lcssa:                     ; preds = %for.body.i55
  br label %for.end.i.loopexit

for.end.i.loopexit:                               ; preds = %for.body.i55.prol.loopexit, %for.end.i.loopexit.unr-lcssa
  %.lcssa347 = phi i64 [ %.lcssa347.unr, %for.body.i55.prol.loopexit ], [ %61, %for.end.i.loopexit.unr-lcssa ]
  br label %for.end.i

for.end.i:                                        ; preds = %for.end.i.loopexit, %for.cond.i54.preheader
  %.lcssa = phi i64 [ %54, %for.cond.i54.preheader ], [ %.lcssa347, %for.end.i.loopexit ]
  %mul30.i = mul i64 %.lcssa, %b2
  %add31.i = add i64 %mul30.i, %add7
  br label %DrawSegment.exit

if.else.i:                                        ; preds = %DrawSegment.exit81
  %cmp37.i = icmp ult i64 %add7, %i
  %cond42.i = select i1 %cmp37.i, i64 %add7, i64 %i
  %add43.i = add i64 %mul.i52, %cond42.i
  %arrayidx44.i = getelementptr inbounds i8, i8* %51, i64 %add43.i
  %62 = load i8, i8* %arrayidx44.i, align 1, !tbaa !7
  %or46.i = or i8 %62, 2
  store i8 %or46.i, i8* %arrayidx44.i, align 1, !tbaa !7
  %x.0.i254 = add i64 %cond42.i, 1
  %cond61.i = select i1 %cmp37.i, i64 %i, i64 %add7
  %cmp62.i255 = icmp ult i64 %x.0.i254, %cond61.i
  %63 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i256 = mul i64 %63, %b2
  br i1 %cmp62.i255, label %for.body64.i.preheader, label %for.end70.i

for.body64.i.preheader:                           ; preds = %if.else.i
  br label %for.body64.i

for.body64.i:                                     ; preds = %for.body64.i.preheader, %for.body64.i
  %mul65.i258 = phi i64 [ %mul65.i, %for.body64.i ], [ %mul65.i256, %for.body64.i.preheader ]
  %x.0.i257 = phi i64 [ %x.0.i, %for.body64.i ], [ %x.0.i254, %for.body64.i.preheader ]
  %add66.i = add i64 %mul65.i258, %x.0.i257
  %arrayidx67.i = getelementptr inbounds i8, i8* %51, i64 %add66.i
  store i8 3, i8* %arrayidx67.i, align 1, !tbaa !7
  %x.0.i = add nuw i64 %x.0.i257, 1
  %cmp62.i = icmp ult i64 %x.0.i, %cond61.i
  %64 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i = mul i64 %64, %b2
  br i1 %cmp62.i, label %for.body64.i, label %for.end70.i.loopexit

for.end70.i.loopexit:                             ; preds = %for.body64.i
  br label %for.end70.i

for.end70.i:                                      ; preds = %for.end70.i.loopexit, %if.else.i
  %mul65.i.lcssa = phi i64 [ %mul65.i256, %if.else.i ], [ %mul65.i, %for.end70.i.loopexit ]
  %add78.i = add i64 %mul65.i.lcssa, %cond61.i
  br label %DrawSegment.exit

DrawSegment.exit:                                 ; preds = %for.end.i, %for.end70.i
  %add78.sink.i = phi i64 [ %add78.i, %for.end70.i ], [ %add31.i, %for.end.i ]
  %.sink.i = phi i8 [ 1, %for.end70.i ], [ 4, %for.end.i ]
  %arrayidx79.i = getelementptr inbounds i8, i8* %51, i64 %add78.sink.i
  %65 = load i8, i8* %arrayidx79.i, align 1, !tbaa !7
  %or81.i = or i8 %65, %.sink.i
  store i8 %or81.i, i8* %arrayidx79.i, align 1, !tbaa !7
  br label %return

return.loopexit:                                  ; preds = %for.body.i187
  br label %return

return.loopexit342:                               ; preds = %for.body.i203
  br label %return

return.loopexit343:                               ; preds = %for.body34.i
  br label %return

return.loopexit345:                               ; preds = %for.body.i224
  br label %return

return.loopexit346:                               ; preds = %for.body.i
  br label %return

return:                                           ; preds = %return.loopexit346, %return.loopexit345, %return.loopexit343, %return.loopexit342, %return.loopexit, %HasVCV.exit, %DrawSegment.exit
  %retval.0 = phi i32 [ 1, %DrawSegment.exit ], [ 0, %HasVCV.exit ], [ 0, %return.loopexit ], [ 0, %return.loopexit342 ], [ 0, %return.loopexit343 ], [ 0, %return.loopexit345 ], [ 0, %return.loopexit346 ]
  ret i32 %retval.0
}

; Function Attrs: norecurse nounwind uwtable
define internal fastcc void @CleanNet(i64 %net) unnamed_addr #3 {
entry:
  %0 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i64, i64* %0, i64 %net
  %1 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %2 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx168 = getelementptr inbounds i64, i64* %2, i64 %net
  %3 = load i64, i64* %arrayidx168, align 8, !tbaa !1
  %cmp69 = icmp ugt i64 %1, %3
  br i1 %cmp69, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %4 = load i64*, i64** @TOP, align 8, !tbaa !5
  %5 = load i8*, i8** @mazeRoute, align 8
  %6 = load i64*, i64** @BOT, align 8
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %i.070 = phi i64 [ %1, %for.body.lr.ph ], [ %inc, %for.inc ]
  %arrayidx2 = getelementptr inbounds i64, i64* %4, i64 %i.070
  %7 = load i64, i64* %arrayidx2, align 8, !tbaa !1
  %cmp3 = icmp eq i64 %7, %net
  br i1 %cmp3, label %land.lhs.true, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %for.body
  %arrayidx4 = getelementptr inbounds i64, i64* %6, i64 %i.070
  %8 = load i64, i64* %arrayidx4, align 8, !tbaa !1
  %cmp5 = icmp eq i64 %8, %net
  br i1 %cmp5, label %land.lhs.true, label %for.inc

land.lhs.true:                                    ; preds = %lor.lhs.false, %for.body
  %arrayidx6 = getelementptr inbounds i8, i8* %5, i64 %i.070
  %9 = load i8, i8* %arrayidx6, align 1, !tbaa !7
  %tobool = icmp eq i8 %9, 0
  br i1 %tobool, label %for.inc, label %cleanup.loopexit74

for.inc:                                          ; preds = %land.lhs.true, %lor.lhs.false
  %inc = add i64 %i.070, 1
  %cmp = icmp ugt i64 %inc, %3
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %10 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx7 = getelementptr inbounds i64, i64* %10, i64 %net
  %11 = load i64, i64* %arrayidx7, align 8, !tbaa !1
  %12 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %13 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul = mul i64 %13, %11
  br label %while.cond

while.cond:                                       ; preds = %while.cond, %for.end
  %effFIRST.0 = phi i64 [ %1, %for.end ], [ %dec, %while.cond ]
  %add = add i64 %mul, %effFIRST.0
  %arrayidx9 = getelementptr inbounds i8, i8* %12, i64 %add
  %14 = load i8, i8* %arrayidx9, align 1, !tbaa !7
  %15 = and i8 %14, 1
  %tobool11 = icmp eq i8 %15, 0
  %dec = add i64 %effFIRST.0, -1
  br i1 %tobool11, label %while.cond13.preheader, label %while.cond

while.cond13.preheader:                           ; preds = %while.cond
  br label %while.cond13

while.cond13:                                     ; preds = %while.cond13.preheader, %while.cond13
  %effLAST.0 = phi i64 [ %inc21, %while.cond13 ], [ %3, %while.cond13.preheader ]
  %add15 = add i64 %mul, %effLAST.0
  %arrayidx16 = getelementptr inbounds i8, i8* %12, i64 %add15
  %16 = load i8, i8* %arrayidx16, align 1, !tbaa !7
  %17 = and i8 %16, 2
  %tobool19 = icmp eq i8 %17, 0
  %inc21 = add i64 %effLAST.0, 1
  br i1 %tobool19, label %for.cond23.preheader, label %while.cond13

for.cond23.preheader:                             ; preds = %while.cond13
  %cmp2462 = icmp ugt i64 %effFIRST.0, %effLAST.0
  br i1 %cmp2462, label %for.end40, label %for.body26.lr.ph

for.body26.lr.ph:                                 ; preds = %for.cond23.preheader
  %18 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  br label %for.body26

for.body26:                                       ; preds = %for.body26.lr.ph, %for.inc38
  %i.165 = phi i64 [ %effFIRST.0, %for.body26.lr.ph ], [ %inc39, %for.inc38 ]
  %lastVia.064 = phi i64 [ 0, %for.body26.lr.ph ], [ %lastVia.1, %for.inc38 ]
  %firstVia.063 = phi i64 [ 9999999, %for.body26.lr.ph ], [ %firstVia.2, %for.inc38 ]
  %add.i = add i64 %mul, %i.165
  %arrayidx.i = getelementptr inbounds i8, i8* %18, i64 %add.i
  %19 = load i8, i8* %arrayidx.i, align 1, !tbaa !7
  %tobool27 = icmp eq i8 %19, 0
  br i1 %tobool27, label %for.inc38, label %if.then28

if.then28:                                        ; preds = %for.body26
  %cmp29 = icmp ult i64 %i.165, %firstVia.063
  %i.1.firstVia.0 = select i1 %cmp29, i64 %i.165, i64 %firstVia.063
  %cmp33 = icmp ugt i64 %i.165, %lastVia.064
  %i.1.lastVia.0 = select i1 %cmp33, i64 %i.165, i64 %lastVia.064
  br label %for.inc38

for.inc38:                                        ; preds = %if.then28, %for.body26
  %firstVia.2 = phi i64 [ %firstVia.063, %for.body26 ], [ %i.1.firstVia.0, %if.then28 ]
  %lastVia.1 = phi i64 [ %lastVia.064, %for.body26 ], [ %i.1.lastVia.0, %if.then28 ]
  %inc39 = add i64 %i.165, 1
  %cmp24 = icmp ugt i64 %inc39, %effLAST.0
  br i1 %cmp24, label %for.end40.loopexit, label %for.body26

for.end40.loopexit:                               ; preds = %for.inc38
  br label %for.end40

for.end40:                                        ; preds = %for.end40.loopexit, %for.cond23.preheader
  %firstVia.0.lcssa = phi i64 [ 9999999, %for.cond23.preheader ], [ %firstVia.2, %for.end40.loopexit ]
  %lastVia.0.lcssa = phi i64 [ 0, %for.cond23.preheader ], [ %lastVia.1, %for.end40.loopexit ]
  %cmp41 = icmp ugt i64 %firstVia.0.lcssa, %effFIRST.0
  br i1 %cmp41, label %for.body47.preheader, label %if.end60

for.body47.preheader:                             ; preds = %for.end40
  %20 = sub i64 %firstVia.0.lcssa, %effFIRST.0
  %21 = add i64 %firstVia.0.lcssa, -1
  %22 = sub i64 %21, %effFIRST.0
  %xtraiter = and i64 %20, 3
  %lcmp.mod = icmp eq i64 %xtraiter, 0
  br i1 %lcmp.mod, label %for.body47.prol.loopexit, label %for.body47.prol.preheader

for.body47.prol.preheader:                        ; preds = %for.body47.preheader
  br label %for.body47.prol

for.body47.prol:                                  ; preds = %for.body47.prol, %for.body47.prol.preheader
  %mul4860.prol = phi i64 [ %mul48.prol, %for.body47.prol ], [ %mul, %for.body47.prol.preheader ]
  %i.259.prol = phi i64 [ %inc52.prol, %for.body47.prol ], [ %effFIRST.0, %for.body47.prol.preheader ]
  %prol.iter = phi i64 [ %prol.iter.sub, %for.body47.prol ], [ %xtraiter, %for.body47.prol.preheader ]
  %add49.prol = add i64 %mul4860.prol, %i.259.prol
  %arrayidx50.prol = getelementptr inbounds i8, i8* %12, i64 %add49.prol
  store i8 0, i8* %arrayidx50.prol, align 1, !tbaa !7
  %inc52.prol = add nuw i64 %i.259.prol, 1
  %23 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul48.prol = mul i64 %23, %11
  %prol.iter.sub = add i64 %prol.iter, -1
  %prol.iter.cmp = icmp eq i64 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.body47.prol.loopexit.unr-lcssa, label %for.body47.prol, !llvm.loop !8

for.body47.prol.loopexit.unr-lcssa:               ; preds = %for.body47.prol
  br label %for.body47.prol.loopexit

for.body47.prol.loopexit:                         ; preds = %for.body47.preheader, %for.body47.prol.loopexit.unr-lcssa
  %mul48.lcssa.unr = phi i64 [ undef, %for.body47.preheader ], [ %mul48.prol, %for.body47.prol.loopexit.unr-lcssa ]
  %mul4860.unr = phi i64 [ %mul, %for.body47.preheader ], [ %mul48.prol, %for.body47.prol.loopexit.unr-lcssa ]
  %i.259.unr = phi i64 [ %effFIRST.0, %for.body47.preheader ], [ %inc52.prol, %for.body47.prol.loopexit.unr-lcssa ]
  %24 = icmp ult i64 %22, 3
  br i1 %24, label %for.end53, label %for.body47.preheader.new

for.body47.preheader.new:                         ; preds = %for.body47.prol.loopexit
  br label %for.body47

for.body47:                                       ; preds = %for.body47, %for.body47.preheader.new
  %mul4860 = phi i64 [ %mul4860.unr, %for.body47.preheader.new ], [ %mul48.3, %for.body47 ]
  %i.259 = phi i64 [ %i.259.unr, %for.body47.preheader.new ], [ %inc52.3, %for.body47 ]
  %add49 = add i64 %mul4860, %i.259
  %arrayidx50 = getelementptr inbounds i8, i8* %12, i64 %add49
  store i8 0, i8* %arrayidx50, align 1, !tbaa !7
  %inc52 = add nuw i64 %i.259, 1
  %25 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul48 = mul i64 %25, %11
  %add49.1 = add i64 %mul48, %inc52
  %arrayidx50.1 = getelementptr inbounds i8, i8* %12, i64 %add49.1
  store i8 0, i8* %arrayidx50.1, align 1, !tbaa !7
  %inc52.1 = add i64 %i.259, 2
  %26 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul48.1 = mul i64 %26, %11
  %add49.2 = add i64 %mul48.1, %inc52.1
  %arrayidx50.2 = getelementptr inbounds i8, i8* %12, i64 %add49.2
  store i8 0, i8* %arrayidx50.2, align 1, !tbaa !7
  %inc52.2 = add i64 %i.259, 3
  %27 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul48.2 = mul i64 %27, %11
  %add49.3 = add i64 %mul48.2, %inc52.2
  %arrayidx50.3 = getelementptr inbounds i8, i8* %12, i64 %add49.3
  store i8 0, i8* %arrayidx50.3, align 1, !tbaa !7
  %inc52.3 = add i64 %i.259, 4
  %28 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul48.3 = mul i64 %28, %11
  %exitcond.3 = icmp eq i64 %inc52.3, %firstVia.0.lcssa
  br i1 %exitcond.3, label %for.end53.unr-lcssa, label %for.body47

for.end53.unr-lcssa:                              ; preds = %for.body47
  br label %for.end53

for.end53:                                        ; preds = %for.body47.prol.loopexit, %for.end53.unr-lcssa
  %mul48.lcssa = phi i64 [ %mul48.lcssa.unr, %for.body47.prol.loopexit ], [ %mul48.3, %for.end53.unr-lcssa ]
  %add55 = add i64 %mul48.lcssa, %firstVia.0.lcssa
  %arrayidx56 = getelementptr inbounds i8, i8* %12, i64 %add55
  %29 = load i8, i8* %arrayidx56, align 1, !tbaa !7
  %and58 = and i8 %29, -2
  store i8 %and58, i8* %arrayidx56, align 1, !tbaa !7
  br label %if.end60

if.end60:                                         ; preds = %for.end53, %for.end40
  %cmp61 = icmp ult i64 %lastVia.0.lcssa, %effLAST.0
  br i1 %cmp61, label %if.then63, label %cleanup

if.then63:                                        ; preds = %if.end60
  %30 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul64 = mul i64 %30, %11
  %add65 = add i64 %mul64, %lastVia.0.lcssa
  %arrayidx66 = getelementptr inbounds i8, i8* %12, i64 %add65
  %31 = load i8, i8* %arrayidx66, align 1, !tbaa !7
  %and68 = and i8 %31, -3
  store i8 %and68, i8* %arrayidx66, align 1, !tbaa !7
  %i.354 = add i64 %lastVia.0.lcssa, 1
  %cmp7255 = icmp ugt i64 %i.354, %effLAST.0
  br i1 %cmp7255, label %cleanup, label %for.body74.preheader

for.body74.preheader:                             ; preds = %if.then63
  br label %for.body74

for.body74:                                       ; preds = %for.body74.preheader, %for.body74
  %i.356 = phi i64 [ %i.3, %for.body74 ], [ %i.354, %for.body74.preheader ]
  %32 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul75 = mul i64 %32, %11
  %add76 = add i64 %mul75, %i.356
  %arrayidx77 = getelementptr inbounds i8, i8* %12, i64 %add76
  store i8 0, i8* %arrayidx77, align 1, !tbaa !7
  %i.3 = add i64 %i.356, 1
  %cmp72 = icmp ugt i64 %i.3, %effLAST.0
  br i1 %cmp72, label %cleanup.loopexit, label %for.body74

cleanup.loopexit:                                 ; preds = %for.body74
  br label %cleanup

cleanup.loopexit74:                               ; preds = %land.lhs.true
  br label %cleanup

cleanup:                                          ; preds = %cleanup.loopexit74, %cleanup.loopexit, %if.then63, %if.end60
  ret void
}

; Function Attrs: nounwind uwtable
define i32 @ExtendOK(i64 %net, i8* nocapture readonly %plane, i64 %_x1, i64 %_y1, i64 %_x2, i64 %_y2) local_unnamed_addr #0 {
entry:
  %cmp = icmp ult i64 %_x1, %_x2
  %cond = select i1 %cmp, i64 %_x1, i64 %_x2
  %cmp1 = icmp ult i64 %_y1, %_y2
  %cond5 = select i1 %cmp1, i64 %_y1, i64 %_y2
  %cond10 = select i1 %cmp, i64 %_x2, i64 %_x1
  %0 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i64, i64* %0, i64 %net
  %1 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %cmp16 = icmp ult i64 %cond, %1
  %2 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx22 = getelementptr inbounds i64, i64* %2, i64 %net
  %3 = load i64, i64* %arrayidx22, align 8, !tbaa !1
  %cmp23 = icmp ugt i64 %cond10, %3
  br i1 %cmp16, label %land.lhs.true21, label %land.lhs.true

land.lhs.true:                                    ; preds = %entry
  br i1 %cmp23, label %if.else35, label %cleanup

land.lhs.true21:                                  ; preds = %entry
  %sub = add i64 %1, -1
  %cmp.i = icmp eq i64 %cond, %sub
  %4 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i = mul i64 %4, %cond5
  br i1 %cmp23, label %if.then24, label %if.then31

if.then24:                                        ; preds = %land.lhs.true21
  br i1 %cmp.i, label %if.then.i, label %if.else.i

if.then.i:                                        ; preds = %if.then24
  %add.i = add i64 %mul.i, %cond
  br label %for.body.i

for.body.i:                                       ; preds = %if.then.i, %for.inc.i
  %index.0.i157 = phi i64 [ %add.i, %if.then.i ], [ %add14.i, %for.inc.i ]
  %y.0.i156 = phi i64 [ %cond5, %if.then.i ], [ %inc.i, %for.inc.i ]
  %arrayidx.i = getelementptr inbounds i8, i8* %plane, i64 %index.0.i157
  %5 = load i8, i8* %arrayidx.i, align 1, !tbaa !7
  %tobool.i = icmp eq i8 %5, 0
  br i1 %tobool.i, label %for.inc.i, label %cleanup.loopexit209

for.inc.i:                                        ; preds = %for.body.i
  %inc.i = add i64 %y.0.i156, 1
  %add14.i = add i64 %4, %index.0.i157
  %cmp12.i = icmp ugt i64 %inc.i, %cond5
  br i1 %cmp12.i, label %land.rhs.loopexit, label %for.body.i

if.else.i:                                        ; preds = %if.then24
  %cmp16.i = icmp ult i64 %cond, %sub
  %cond20.i = select i1 %cmp16.i, i64 %cond, i64 %sub
  %add21.i = add i64 %mul.i, %cond20.i
  %cond32.i = select i1 %cmp16.i, i64 %sub, i64 %cond
  br label %for.body34.i

for.body34.i:                                     ; preds = %if.else.i, %for.inc39.i
  %index.1.i159 = phi i64 [ %add21.i, %if.else.i ], [ %inc41.i, %for.inc39.i ]
  %x.0.i158 = phi i64 [ %cond20.i, %if.else.i ], [ %inc40.i, %for.inc39.i ]
  %arrayidx35.i = getelementptr inbounds i8, i8* %plane, i64 %index.1.i159
  %6 = load i8, i8* %arrayidx35.i, align 1, !tbaa !7
  %tobool36.i = icmp eq i8 %6, 0
  br i1 %tobool36.i, label %for.inc39.i, label %cleanup.loopexit210

for.inc39.i:                                      ; preds = %for.body34.i
  %inc40.i = add i64 %x.0.i158, 1
  %inc41.i = add i64 %index.1.i159, 1
  %cmp33.i = icmp ugt i64 %inc40.i, %cond32.i
  br i1 %cmp33.i, label %land.rhs.loopexit211, label %for.body34.i

land.rhs.loopexit:                                ; preds = %for.inc.i
  br label %land.rhs

land.rhs.loopexit211:                             ; preds = %for.inc39.i
  br label %land.rhs

land.rhs:                                         ; preds = %land.rhs.loopexit211, %land.rhs.loopexit
  %add = add i64 %3, 1
  %cmp.i108 = icmp eq i64 %add, %cond10
  br i1 %cmp.i108, label %if.then.i111, label %if.else.i126

if.then.i111:                                     ; preds = %land.rhs
  %add.i110 = add i64 %mul.i, %cond10
  br label %for.body.i118

for.body.i118:                                    ; preds = %if.then.i111, %for.inc.i121
  %index.0.i113153 = phi i64 [ %add.i110, %if.then.i111 ], [ %add14.i120, %for.inc.i121 ]
  %y.0.i112152 = phi i64 [ %cond5, %if.then.i111 ], [ %inc.i119, %for.inc.i121 ]
  %arrayidx.i116 = getelementptr inbounds i8, i8* %plane, i64 %index.0.i113153
  %7 = load i8, i8* %arrayidx.i116, align 1, !tbaa !7
  %tobool.i117 = icmp eq i8 %7, 0
  br i1 %tobool.i117, label %for.inc.i121, label %cleanup.loopexit

for.inc.i121:                                     ; preds = %for.body.i118
  %inc.i119 = add i64 %y.0.i112152, 1
  %add14.i120 = add i64 %4, %index.0.i113153
  %cmp12.i114 = icmp ugt i64 %inc.i119, %cond5
  br i1 %cmp12.i114, label %cleanup.loopexit, label %for.body.i118

if.else.i126:                                     ; preds = %land.rhs
  %cmp16.i123 = icmp ult i64 %add, %cond10
  %cond20.i124 = select i1 %cmp16.i123, i64 %add, i64 %cond10
  %add21.i125 = add i64 %cond20.i124, %mul.i
  %cond32.i129 = select i1 %cmp16.i123, i64 %cond10, i64 %add
  br label %for.body34.i134

for.body34.i134:                                  ; preds = %if.else.i126, %for.inc39.i137
  %index.1.i128155 = phi i64 [ %add21.i125, %if.else.i126 ], [ %inc41.i136, %for.inc39.i137 ]
  %x.0.i127154 = phi i64 [ %cond20.i124, %if.else.i126 ], [ %inc40.i135, %for.inc39.i137 ]
  %arrayidx35.i132 = getelementptr inbounds i8, i8* %plane, i64 %index.1.i128155
  %8 = load i8, i8* %arrayidx35.i132, align 1, !tbaa !7
  %tobool36.i133 = icmp eq i8 %8, 0
  br i1 %tobool36.i133, label %for.inc39.i137, label %cleanup.loopexit207

for.inc39.i137:                                   ; preds = %for.body34.i134
  %inc40.i135 = add i64 %x.0.i127154, 1
  %inc41.i136 = add i64 %index.1.i128155, 1
  %cmp33.i130 = icmp ugt i64 %inc40.i135, %cond32.i129
  br i1 %cmp33.i130, label %cleanup.loopexit207, label %for.body34.i134

if.then31:                                        ; preds = %land.lhs.true21
  br i1 %cmp.i, label %if.then.i79, label %if.else.i94

if.then.i79:                                      ; preds = %if.then31
  %add.i78 = add i64 %mul.i, %cond
  br label %for.body.i86

for.body.i86:                                     ; preds = %if.then.i79, %for.inc.i89
  %index.0.i81161 = phi i64 [ %add.i78, %if.then.i79 ], [ %add14.i88, %for.inc.i89 ]
  %y.0.i80160 = phi i64 [ %cond5, %if.then.i79 ], [ %inc.i87, %for.inc.i89 ]
  %arrayidx.i84 = getelementptr inbounds i8, i8* %plane, i64 %index.0.i81161
  %9 = load i8, i8* %arrayidx.i84, align 1, !tbaa !7
  %tobool.i85 = icmp eq i8 %9, 0
  br i1 %tobool.i85, label %for.inc.i89, label %cleanup.loopexit212

for.inc.i89:                                      ; preds = %for.body.i86
  %inc.i87 = add i64 %y.0.i80160, 1
  %add14.i88 = add i64 %4, %index.0.i81161
  %cmp12.i82 = icmp ugt i64 %inc.i87, %cond5
  br i1 %cmp12.i82, label %cleanup.loopexit212, label %for.body.i86

if.else.i94:                                      ; preds = %if.then31
  %cmp16.i91 = icmp ult i64 %cond, %sub
  %cond20.i92 = select i1 %cmp16.i91, i64 %cond, i64 %sub
  %add21.i93 = add i64 %mul.i, %cond20.i92
  %cond32.i97 = select i1 %cmp16.i91, i64 %sub, i64 %cond
  br label %for.body34.i102

for.body34.i102:                                  ; preds = %if.else.i94, %for.inc39.i105
  %index.1.i96163 = phi i64 [ %add21.i93, %if.else.i94 ], [ %inc41.i104, %for.inc39.i105 ]
  %x.0.i95162 = phi i64 [ %cond20.i92, %if.else.i94 ], [ %inc40.i103, %for.inc39.i105 ]
  %arrayidx35.i100 = getelementptr inbounds i8, i8* %plane, i64 %index.1.i96163
  %10 = load i8, i8* %arrayidx35.i100, align 1, !tbaa !7
  %tobool36.i101 = icmp eq i8 %10, 0
  br i1 %tobool36.i101, label %for.inc39.i105, label %cleanup.loopexit214

for.inc39.i105:                                   ; preds = %for.body34.i102
  %inc40.i103 = add i64 %x.0.i95162, 1
  %inc41.i104 = add i64 %index.1.i96163, 1
  %cmp33.i98 = icmp ugt i64 %inc40.i103, %cond32.i97
  br i1 %cmp33.i98, label %cleanup.loopexit214, label %for.body34.i102

if.else35:                                        ; preds = %land.lhs.true
  %11 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx36 = getelementptr inbounds i64, i64* %11, i64 %net
  %12 = load i64, i64* %arrayidx36, align 8, !tbaa !1
  %cmp37 = icmp ugt i64 %cond10, %12
  br i1 %cmp37, label %if.then38, label %if.end44

if.then38:                                        ; preds = %if.else35
  %add40 = add i64 %12, 1
  %cmp.i44 = icmp eq i64 %add40, %cond10
  %13 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i45 = mul i64 %13, %cond5
  br i1 %cmp.i44, label %if.then.i47, label %if.else.i62

if.then.i47:                                      ; preds = %if.then38
  %add.i46 = add i64 %mul.i45, %cond10
  br label %for.body.i54

for.body.i54:                                     ; preds = %if.then.i47, %for.inc.i57
  %index.0.i49165 = phi i64 [ %add.i46, %if.then.i47 ], [ %add14.i56, %for.inc.i57 ]
  %y.0.i48164 = phi i64 [ %cond5, %if.then.i47 ], [ %inc.i55, %for.inc.i57 ]
  %arrayidx.i52 = getelementptr inbounds i8, i8* %plane, i64 %index.0.i49165
  %14 = load i8, i8* %arrayidx.i52, align 1, !tbaa !7
  %tobool.i53 = icmp eq i8 %14, 0
  br i1 %tobool.i53, label %for.inc.i57, label %cleanup.loopexit216

for.inc.i57:                                      ; preds = %for.body.i54
  %inc.i55 = add i64 %y.0.i48164, 1
  %add14.i56 = add i64 %13, %index.0.i49165
  %cmp12.i50 = icmp ugt i64 %inc.i55, %cond5
  br i1 %cmp12.i50, label %cleanup.loopexit216, label %for.body.i54

if.else.i62:                                      ; preds = %if.then38
  %cmp16.i59 = icmp ult i64 %add40, %cond10
  %cond20.i60 = select i1 %cmp16.i59, i64 %add40, i64 %cond10
  %add21.i61 = add i64 %mul.i45, %cond20.i60
  %cond32.i65 = select i1 %cmp16.i59, i64 %cond10, i64 %add40
  br label %for.body34.i70

for.body34.i70:                                   ; preds = %if.else.i62, %for.inc39.i73
  %index.1.i64167 = phi i64 [ %add21.i61, %if.else.i62 ], [ %inc41.i72, %for.inc39.i73 ]
  %x.0.i63166 = phi i64 [ %cond20.i60, %if.else.i62 ], [ %inc40.i71, %for.inc39.i73 ]
  %arrayidx35.i68 = getelementptr inbounds i8, i8* %plane, i64 %index.1.i64167
  %15 = load i8, i8* %arrayidx35.i68, align 1, !tbaa !7
  %tobool36.i69 = icmp eq i8 %15, 0
  br i1 %tobool36.i69, label %for.inc39.i73, label %cleanup.loopexit218

for.inc39.i73:                                    ; preds = %for.body34.i70
  %inc40.i71 = add i64 %x.0.i63166, 1
  %inc41.i72 = add i64 %index.1.i64167, 1
  %cmp33.i66 = icmp ugt i64 %inc40.i71, %cond32.i65
  br i1 %cmp33.i66, label %cleanup.loopexit218, label %for.body34.i70

if.end44:                                         ; preds = %if.else35
  tail call void @abort() #8
  unreachable

cleanup.loopexit:                                 ; preds = %for.body.i118, %for.inc.i121
  %retval.0.ph = phi i32 [ 1, %for.inc.i121 ], [ 0, %for.body.i118 ]
  br label %cleanup

cleanup.loopexit207:                              ; preds = %for.body34.i134, %for.inc39.i137
  %retval.0.ph208 = phi i32 [ 1, %for.inc39.i137 ], [ 0, %for.body34.i134 ]
  br label %cleanup

cleanup.loopexit209:                              ; preds = %for.body.i
  br label %cleanup

cleanup.loopexit210:                              ; preds = %for.body34.i
  br label %cleanup

cleanup.loopexit212:                              ; preds = %for.inc.i89, %for.body.i86
  %retval.0.ph213 = phi i32 [ 1, %for.inc.i89 ], [ 0, %for.body.i86 ]
  br label %cleanup

cleanup.loopexit214:                              ; preds = %for.inc39.i105, %for.body34.i102
  %retval.0.ph215 = phi i32 [ 1, %for.inc39.i105 ], [ 0, %for.body34.i102 ]
  br label %cleanup

cleanup.loopexit216:                              ; preds = %for.inc.i57, %for.body.i54
  %retval.0.ph217 = phi i32 [ 1, %for.inc.i57 ], [ 0, %for.body.i54 ]
  br label %cleanup

cleanup.loopexit218:                              ; preds = %for.inc39.i73, %for.body34.i70
  %retval.0.ph219 = phi i32 [ 1, %for.inc39.i73 ], [ 0, %for.body34.i70 ]
  br label %cleanup

cleanup:                                          ; preds = %cleanup.loopexit218, %cleanup.loopexit216, %cleanup.loopexit214, %cleanup.loopexit212, %cleanup.loopexit210, %cleanup.loopexit209, %cleanup.loopexit207, %cleanup.loopexit, %land.lhs.true
  %retval.0 = phi i32 [ 1, %land.lhs.true ], [ %retval.0.ph, %cleanup.loopexit ], [ %retval.0.ph208, %cleanup.loopexit207 ], [ 0, %cleanup.loopexit209 ], [ 0, %cleanup.loopexit210 ], [ %retval.0.ph213, %cleanup.loopexit212 ], [ %retval.0.ph215, %cleanup.loopexit214 ], [ %retval.0.ph217, %cleanup.loopexit216 ], [ %retval.0.ph219, %cleanup.loopexit218 ]
  ret i32 %retval.0
}

; Function Attrs: noreturn nounwind
declare void @abort() local_unnamed_addr #2

; Function Attrs: nounwind uwtable
define i32 @Maze2() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp53 = icmp eq i64 %0, 0
  br i1 %cmp53, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %1 = phi i64 [ %43, %for.inc ], [ %0, %for.body.preheader ]
  %i.055 = phi i64 [ %inc64, %for.inc ], [ 1, %for.body.preheader ]
  %numLeft.054 = phi i32 [ %numLeft.1, %for.inc ], [ 0, %for.body.preheader ]
  %2 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i8, i8* %2, i64 %i.055
  %3 = load i8, i8* %arrayidx, align 1, !tbaa !7
  %tobool = icmp eq i8 %3, 0
  br i1 %tobool, label %for.inc, label %if.then

if.then:                                          ; preds = %for.body
  %4 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %5 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx1 = getelementptr inbounds i64, i64* %5, i64 %i.055
  %6 = load i64, i64* %arrayidx1, align 8, !tbaa !1
  %arrayidx2 = getelementptr inbounds i64, i64* %4, i64 %6
  %7 = load i64, i64* %arrayidx2, align 8, !tbaa !1
  %8 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx3 = getelementptr inbounds i64, i64* %8, i64 %i.055
  %9 = load i64, i64* %arrayidx3, align 8, !tbaa !1
  %arrayidx4 = getelementptr inbounds i64, i64* %4, i64 %9
  %10 = load i64, i64* %arrayidx4, align 8, !tbaa !1
  %cmp5 = icmp ugt i64 %i.055, 1
  %cmp6 = icmp ugt i64 %10, 1
  %or.cond = and i1 %cmp5, %cmp6
  br i1 %or.cond, label %land.lhs.true7, label %if.else

land.lhs.true7:                                   ; preds = %if.then
  %11 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add = add i64 %11, 1
  %sub = add i64 %i.055, -1
  %sub9 = add i64 %10, -1
  %call = tail call fastcc i32 @Maze2Mech(i64 %6, i64 %i.055, i64 %add, i64 %10, i64 0, i64 %7, i64 %sub, i64 1, i32 -1, i64 1, i64 %sub9)
  %tobool10 = icmp eq i32 %call, 0
  br i1 %tobool10, label %land.lhs.true7.if.else_crit_edge, label %if.then11

land.lhs.true7.if.else_crit_edge:                 ; preds = %land.lhs.true7
  %.pre = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %if.else

if.then11:                                        ; preds = %land.lhs.true7
  %12 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx12 = getelementptr inbounds i8, i8* %12, i64 %i.055
  store i8 0, i8* %arrayidx12, align 1, !tbaa !7
  %13 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx13 = getelementptr inbounds i64, i64* %13, i64 %i.055
  %14 = load i64, i64* %arrayidx13, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %14)
  %15 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx14 = getelementptr inbounds i64, i64* %15, i64 %i.055
  %16 = load i64, i64* %arrayidx14, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %16)
  br label %for.inc

if.else:                                          ; preds = %land.lhs.true7.if.else_crit_edge, %if.then
  %17 = phi i64 [ %.pre, %land.lhs.true7.if.else_crit_edge ], [ %1, %if.then ]
  %cmp15 = icmp ult i64 %i.055, %17
  %or.cond1 = and i1 %cmp6, %cmp15
  br i1 %or.cond1, label %land.lhs.true18, label %if.else29

land.lhs.true18:                                  ; preds = %if.else
  %18 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx19 = getelementptr inbounds i64, i64* %18, i64 %i.055
  %19 = load i64, i64* %arrayidx19, align 8, !tbaa !1
  %20 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add20 = add i64 %20, 1
  %add21 = add i64 %i.055, 1
  %sub22 = add i64 %10, -1
  %call23 = tail call fastcc i32 @Maze2Mech(i64 %19, i64 %i.055, i64 %add20, i64 %10, i64 0, i64 %7, i64 %add21, i64 %17, i32 1, i64 1, i64 %sub22)
  %tobool24 = icmp eq i32 %call23, 0
  br i1 %tobool24, label %if.else29, label %if.then25

if.then25:                                        ; preds = %land.lhs.true18
  %21 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx26 = getelementptr inbounds i8, i8* %21, i64 %i.055
  store i8 0, i8* %arrayidx26, align 1, !tbaa !7
  %22 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx27 = getelementptr inbounds i64, i64* %22, i64 %i.055
  %23 = load i64, i64* %arrayidx27, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %23)
  %24 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx28 = getelementptr inbounds i64, i64* %24, i64 %i.055
  %25 = load i64, i64* %arrayidx28, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %25)
  br label %for.inc

if.else29:                                        ; preds = %land.lhs.true18, %if.else
  br i1 %cmp5, label %land.lhs.true31, label %if.else44

land.lhs.true31:                                  ; preds = %if.else29
  %26 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp32 = icmp ult i64 %7, %26
  br i1 %cmp32, label %land.lhs.true33, label %if.else44

land.lhs.true33:                                  ; preds = %land.lhs.true31
  %27 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx34 = getelementptr inbounds i64, i64* %27, i64 %i.055
  %28 = load i64, i64* %arrayidx34, align 8, !tbaa !1
  %add35 = add i64 %26, 1
  %sub36 = add i64 %i.055, -1
  %add37 = add i64 %7, 1
  %call38 = tail call fastcc i32 @Maze2Mech(i64 %28, i64 %i.055, i64 0, i64 %7, i64 %add35, i64 %10, i64 %sub36, i64 1, i32 -1, i64 %add37, i64 %26)
  %tobool39 = icmp eq i32 %call38, 0
  br i1 %tobool39, label %if.else44, label %if.then40

if.then40:                                        ; preds = %land.lhs.true33
  %29 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx41 = getelementptr inbounds i8, i8* %29, i64 %i.055
  store i8 0, i8* %arrayidx41, align 1, !tbaa !7
  %30 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx42 = getelementptr inbounds i64, i64* %30, i64 %i.055
  %31 = load i64, i64* %arrayidx42, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %31)
  %32 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx43 = getelementptr inbounds i64, i64* %32, i64 %i.055
  %33 = load i64, i64* %arrayidx43, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %33)
  br label %for.inc

if.else44:                                        ; preds = %land.lhs.true33, %land.lhs.true31, %if.else29
  %34 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp45 = icmp ult i64 %i.055, %34
  br i1 %cmp45, label %land.lhs.true46, label %if.else59

land.lhs.true46:                                  ; preds = %if.else44
  %35 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp47 = icmp ult i64 %7, %35
  br i1 %cmp47, label %land.lhs.true48, label %if.else59

land.lhs.true48:                                  ; preds = %land.lhs.true46
  %36 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx49 = getelementptr inbounds i64, i64* %36, i64 %i.055
  %37 = load i64, i64* %arrayidx49, align 8, !tbaa !1
  %add50 = add i64 %35, 1
  %add51 = add i64 %i.055, 1
  %add52 = add i64 %7, 1
  %call53 = tail call fastcc i32 @Maze2Mech(i64 %37, i64 %i.055, i64 0, i64 %7, i64 %add50, i64 %10, i64 %add51, i64 %34, i32 1, i64 %add52, i64 %35)
  %tobool54 = icmp eq i32 %call53, 0
  br i1 %tobool54, label %if.else59, label %if.then55

if.then55:                                        ; preds = %land.lhs.true48
  %38 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx56 = getelementptr inbounds i8, i8* %38, i64 %i.055
  store i8 0, i8* %arrayidx56, align 1, !tbaa !7
  %39 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx57 = getelementptr inbounds i64, i64* %39, i64 %i.055
  %40 = load i64, i64* %arrayidx57, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %40)
  %41 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx58 = getelementptr inbounds i64, i64* %41, i64 %i.055
  %42 = load i64, i64* %arrayidx58, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %42)
  br label %for.inc

if.else59:                                        ; preds = %land.lhs.true48, %land.lhs.true46, %if.else44
  %inc = add nsw i32 %numLeft.054, 1
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.then25, %if.then55, %if.else59, %if.then40, %if.then11
  %numLeft.1 = phi i32 [ %numLeft.054, %if.then11 ], [ %numLeft.054, %if.then25 ], [ %numLeft.054, %if.then40 ], [ %numLeft.054, %if.then55 ], [ %inc, %if.else59 ], [ %numLeft.054, %for.body ]
  %inc64 = add i64 %i.055, 1
  %43 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc64, %43
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %numLeft.0.lcssa = phi i32 [ 0, %entry ], [ %numLeft.1, %for.end.loopexit ]
  ret i32 %numLeft.0.lcssa
}

; Function Attrs: nounwind uwtable
define internal fastcc i32 @Maze2Mech(i64 %bentNet, i64 %i, i64 %s1, i64 %s2, i64 %b1, i64 %b2, i64 %xStart, i64 %xEnd, i32 %bXdelta, i64 %yStart, i64 %yEnd) unnamed_addr #0 {
entry:
  %conv = sext i32 %bXdelta to i64
  %add = add i64 %conv, %xEnd
  %add2 = add i64 %yEnd, 1
  %cmp269 = icmp eq i64 %add2, %yStart
  br i1 %cmp269, label %cleanup, label %for.cond4.preheader.lr.ph

for.cond4.preheader.lr.ph:                        ; preds = %entry
  %cmp5265 = icmp eq i64 %add, %xStart
  %cmp1.i131 = icmp ult i64 %s1, %s2
  %cond.i132 = select i1 %cmp1.i131, i64 %s1, i64 %s2
  %cond11.i = select i1 %cmp1.i131, i64 %s2, i64 %s1
  %sub = add i64 %b2, -1
  br label %for.cond4.preheader

for.cond4.preheader:                              ; preds = %for.cond4.preheader.lr.ph, %for.inc25
  %row.0270 = phi i64 [ %yStart, %for.cond4.preheader.lr.ph ], [ %add27, %for.inc25 ]
  br i1 %cmp5265, label %for.inc25, label %for.body7.lr.ph

for.body7.lr.ph:                                  ; preds = %for.cond4.preheader
  %cmp1.i168 = icmp ugt i64 %row.0270, %b1
  %cond.i169 = select i1 %cmp1.i168, i64 %b1, i64 %row.0270
  %cond11.i175 = select i1 %cmp1.i168, i64 %row.0270, i64 %b1
  %cmp1.i150 = icmp ult i64 %row.0270, %sub
  %cond.i151 = select i1 %cmp1.i150, i64 %row.0270, i64 %sub
  %cond11.i157 = select i1 %cmp1.i150, i64 %sub, i64 %row.0270
  br label %for.body7

for.body7:                                        ; preds = %for.body7.lr.ph, %for.inc
  %col.0267 = phi i64 [ %xStart, %for.body7.lr.ph ], [ %add24, %for.inc ]
  %0 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %cmp.i = icmp eq i64 %col.0267, %i
  %1 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i = mul i64 %1, %row.0270
  br i1 %cmp.i, label %if.then.i, label %if.else.i

if.then.i:                                        ; preds = %for.body7
  %add.i = add i64 %mul.i, %i
  br label %for.body.i

for.body.i:                                       ; preds = %if.then.i, %for.inc.i
  %index.0.i258 = phi i64 [ %add.i, %if.then.i ], [ %add14.i, %for.inc.i ]
  %y.0.i257 = phi i64 [ %row.0270, %if.then.i ], [ %inc.i, %for.inc.i ]
  %arrayidx.i = getelementptr inbounds i8, i8* %0, i64 %index.0.i258
  %2 = load i8, i8* %arrayidx.i, align 1, !tbaa !7
  %tobool.i = icmp eq i8 %2, 0
  br i1 %tobool.i, label %for.inc.i, label %for.inc25.loopexit

for.inc.i:                                        ; preds = %for.body.i
  %inc.i = add i64 %y.0.i257, 1
  %add14.i = add i64 %1, %index.0.i258
  %cmp12.i = icmp ugt i64 %inc.i, %row.0270
  br i1 %cmp12.i, label %land.lhs.true.loopexit, label %for.body.i

if.else.i:                                        ; preds = %for.body7
  %cmp16.i = icmp ugt i64 %col.0267, %i
  %cond20.i = select i1 %cmp16.i, i64 %i, i64 %col.0267
  %add21.i = add i64 %mul.i, %cond20.i
  %cond32.i = select i1 %cmp16.i, i64 %col.0267, i64 %i
  br label %for.body34.i

for.body34.i:                                     ; preds = %if.else.i, %for.inc39.i
  %index.1.i256 = phi i64 [ %add21.i, %if.else.i ], [ %inc41.i, %for.inc39.i ]
  %x.0.i255 = phi i64 [ %cond20.i, %if.else.i ], [ %inc40.i, %for.inc39.i ]
  %arrayidx35.i = getelementptr inbounds i8, i8* %0, i64 %index.1.i256
  %3 = load i8, i8* %arrayidx35.i, align 1, !tbaa !7
  %tobool36.i = icmp eq i8 %3, 0
  br i1 %tobool36.i, label %for.inc39.i, label %for.inc25.loopexit331

for.inc39.i:                                      ; preds = %for.body34.i
  %inc40.i = add i64 %x.0.i255, 1
  %inc41.i = add i64 %index.1.i256, 1
  %cmp33.i = icmp ugt i64 %inc40.i, %cond32.i
  br i1 %cmp33.i, label %land.lhs.true.loopexit332, label %for.body34.i

land.lhs.true.loopexit:                           ; preds = %for.inc.i
  br label %land.lhs.true

land.lhs.true.loopexit332:                        ; preds = %for.inc39.i
  br label %land.lhs.true

land.lhs.true:                                    ; preds = %land.lhs.true.loopexit332, %land.lhs.true.loopexit
  %4 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %mul.i133 = mul i64 %1, %cond.i132
  %add.i134 = add i64 %mul.i133, %i
  br label %for.body.i142

for.body.i142:                                    ; preds = %land.lhs.true, %for.inc.i145
  %index.0.i137260 = phi i64 [ %add.i134, %land.lhs.true ], [ %add14.i144, %for.inc.i145 ]
  %y.0.i136259 = phi i64 [ %cond.i132, %land.lhs.true ], [ %inc.i143, %for.inc.i145 ]
  %arrayidx.i140 = getelementptr inbounds i8, i8* %4, i64 %index.0.i137260
  %5 = load i8, i8* %arrayidx.i140, align 1, !tbaa !7
  %tobool.i141 = icmp eq i8 %5, 0
  br i1 %tobool.i141, label %for.inc.i145, label %for.inc.loopexit330

for.inc.i145:                                     ; preds = %for.body.i142
  %inc.i143 = add i64 %y.0.i136259, 1
  %add14.i144 = add i64 %1, %index.0.i137260
  %cmp12.i138 = icmp ugt i64 %inc.i143, %cond11.i
  br i1 %cmp12.i138, label %land.lhs.true11, label %for.body.i142

land.lhs.true11:                                  ; preds = %for.inc.i145
  %mul.i170 = mul i64 %1, %cond.i169
  %add.i171 = add i64 %mul.i170, %i
  br label %for.body.i180

for.body.i180:                                    ; preds = %land.lhs.true11, %for.inc.i183
  %index.0.i174262 = phi i64 [ %add.i171, %land.lhs.true11 ], [ %add14.i182, %for.inc.i183 ]
  %y.0.i173261 = phi i64 [ %cond.i169, %land.lhs.true11 ], [ %inc.i181, %for.inc.i183 ]
  %arrayidx.i178 = getelementptr inbounds i8, i8* %4, i64 %index.0.i174262
  %6 = load i8, i8* %arrayidx.i178, align 1, !tbaa !7
  %tobool.i179 = icmp eq i8 %6, 0
  br i1 %tobool.i179, label %for.inc.i183, label %for.inc.loopexit329

for.inc.i183:                                     ; preds = %for.body.i180
  %inc.i181 = add i64 %y.0.i173261, 1
  %add14.i182 = add i64 %1, %index.0.i174262
  %cmp12.i176 = icmp ugt i64 %inc.i181, %cond11.i175
  br i1 %cmp12.i176, label %land.lhs.true14, label %for.body.i180

land.lhs.true14:                                  ; preds = %for.inc.i183
  %mul.i152 = mul i64 %1, %cond.i151
  %add.i153 = add i64 %mul.i152, %col.0267
  br label %for.body.i162

for.body.i162:                                    ; preds = %land.lhs.true14, %for.inc.i165
  %index.0.i156264 = phi i64 [ %add.i153, %land.lhs.true14 ], [ %add14.i164, %for.inc.i165 ]
  %y.0.i155263 = phi i64 [ %cond.i151, %land.lhs.true14 ], [ %inc.i163, %for.inc.i165 ]
  %arrayidx.i160 = getelementptr inbounds i8, i8* %4, i64 %index.0.i156264
  %7 = load i8, i8* %arrayidx.i160, align 1, !tbaa !7
  %tobool.i161 = icmp eq i8 %7, 0
  br i1 %tobool.i161, label %for.inc.i165, label %for.inc.loopexit

for.inc.i165:                                     ; preds = %for.body.i162
  %inc.i163 = add i64 %y.0.i155263, 1
  %add14.i164 = add i64 %1, %index.0.i156264
  %cmp12.i158 = icmp ugt i64 %inc.i163, %cond11.i157
  br i1 %cmp12.i158, label %land.lhs.true17, label %for.body.i162

land.lhs.true17:                                  ; preds = %for.inc.i165
  %8 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx.i148 = getelementptr inbounds i64, i64* %8, i64 %col.0267
  %9 = load i64, i64* %arrayidx.i148, align 8, !tbaa !1
  %cmp.i149 = icmp eq i64 %9, 0
  br i1 %cmp.i149, label %land.lhs.true20, label %land.lhs.true.i

land.lhs.true.i:                                  ; preds = %land.lhs.true17
  %10 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx1.i = getelementptr inbounds i64, i64* %10, i64 %col.0267
  %11 = load i64, i64* %arrayidx1.i, align 8, !tbaa !1
  %cmp2.i = icmp eq i64 %11, 0
  %cmp6.i = icmp eq i64 %9, %11
  %or.cond.i = or i1 %cmp2.i, %cmp6.i
  br i1 %or.cond.i, label %land.lhs.true20, label %HasVCV.exit

HasVCV.exit:                                      ; preds = %land.lhs.true.i
  %12 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx8.i = getelementptr inbounds i64, i64* %12, i64 %9
  %13 = load i64, i64* %arrayidx8.i, align 8, !tbaa !1
  %arrayidx10.i = getelementptr inbounds i64, i64* %12, i64 %11
  %14 = load i64, i64* %arrayidx10.i, align 8, !tbaa !1
  %cmp11.i = icmp ugt i64 %13, %14
  br i1 %cmp11.i, label %for.inc, label %land.lhs.true20

land.lhs.true20:                                  ; preds = %HasVCV.exit, %land.lhs.true17, %land.lhs.true.i
  %call21 = tail call i32 @ExtendOK(i64 %bentNet, i8* %0, i64 %col.0267, i64 %b2, i64 %i, i64 %b2)
  %tobool22 = icmp eq i32 %call21, 0
  br i1 %tobool22, label %for.inc, label %if.then

if.then:                                          ; preds = %land.lhs.true20
  %15 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %16 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i109 = mul i64 %16, %cond.i132
  %add19.i114248 = add i64 %mul.i109, %i
  %arrayidx20.i115249 = getelementptr inbounds i8, i8* %15, i64 %add19.i114248
  %17 = load i8, i8* %arrayidx20.i115249, align 1, !tbaa !7
  %or22.i116250 = or i8 %17, 8
  store i8 %or22.i116250, i8* %arrayidx20.i115249, align 1, !tbaa !7
  %y.0.i117251 = add i64 %cond.i132, 1
  %cmp16.i119252 = icmp ult i64 %y.0.i117251, %cond11.i
  %18 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i119252, label %for.body.i122.preheader, label %DrawSegment.exit130

for.body.i122.preheader:                          ; preds = %if.then
  br label %for.body.i122

for.body.i122:                                    ; preds = %for.body.i122.preheader, %for.body.i122
  %19 = phi i64 [ %21, %for.body.i122 ], [ %18, %for.body.i122.preheader ]
  %y.0.i117253 = phi i64 [ %y.0.i117, %for.body.i122 ], [ %y.0.i117251, %for.body.i122.preheader ]
  %mul18.i121 = mul i64 %19, %y.0.i117253
  %add19.i114 = add i64 %mul18.i121, %i
  %arrayidx20.i115 = getelementptr inbounds i8, i8* %15, i64 %add19.i114
  %20 = load i8, i8* %arrayidx20.i115, align 1, !tbaa !7
  %or22.i116 = or i8 %20, 12
  store i8 %or22.i116, i8* %arrayidx20.i115, align 1, !tbaa !7
  %y.0.i117 = add nuw i64 %y.0.i117253, 1
  %cmp16.i119 = icmp ult i64 %y.0.i117, %cond11.i
  %21 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i119, label %for.body.i122, label %DrawSegment.exit130.loopexit

DrawSegment.exit130.loopexit:                     ; preds = %for.body.i122
  br label %DrawSegment.exit130

DrawSegment.exit130:                              ; preds = %DrawSegment.exit130.loopexit, %if.then
  %.lcssa201 = phi i64 [ %18, %if.then ], [ %21, %DrawSegment.exit130.loopexit ]
  %mul30.i123 = mul i64 %.lcssa201, %cond11.i
  %add31.i124 = add i64 %mul30.i123, %i
  %arrayidx79.i128 = getelementptr inbounds i8, i8* %15, i64 %add31.i124
  %22 = load i8, i8* %arrayidx79.i128, align 1, !tbaa !7
  %or81.i129 = or i8 %22, 4
  store i8 %or81.i129, i8* %arrayidx79.i128, align 1, !tbaa !7
  %23 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  %24 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i104 = mul i64 %24, %s2
  %add.i105 = add i64 %mul.i104, %i
  %arrayidx.i106 = getelementptr inbounds i8, i8* %23, i64 %add.i105
  store i8 1, i8* %arrayidx.i106, align 1, !tbaa !7
  %25 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i82 = mul i64 %25, %cond.i169
  %add19.i87241 = add i64 %mul.i82, %i
  %arrayidx20.i88242 = getelementptr inbounds i8, i8* %15, i64 %add19.i87241
  %26 = load i8, i8* %arrayidx20.i88242, align 1, !tbaa !7
  %or22.i89243 = or i8 %26, 8
  store i8 %or22.i89243, i8* %arrayidx20.i88242, align 1, !tbaa !7
  %y.0.i90244 = add i64 %cond.i169, 1
  %cmp16.i92245 = icmp ult i64 %y.0.i90244, %cond11.i175
  %27 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i92245, label %for.body.i95.preheader, label %DrawSegment.exit103

for.body.i95.preheader:                           ; preds = %DrawSegment.exit130
  %28 = add i64 %cond11.i175, 1
  %29 = sub i64 %28, %cond.i169
  %30 = add i64 %cond11.i175, -2
  %xtraiter = and i64 %29, 1
  %lcmp.mod = icmp eq i64 %xtraiter, 0
  br i1 %lcmp.mod, label %for.body.i95.prol.loopexit, label %for.body.i95.prol.preheader

for.body.i95.prol.preheader:                      ; preds = %for.body.i95.preheader
  br label %for.body.i95.prol

for.body.i95.prol:                                ; preds = %for.body.i95.prol.preheader
  %mul18.i94.prol = mul i64 %27, %y.0.i90244
  %add19.i87.prol = add i64 %mul18.i94.prol, %i
  %arrayidx20.i88.prol = getelementptr inbounds i8, i8* %15, i64 %add19.i87.prol
  %31 = load i8, i8* %arrayidx20.i88.prol, align 1, !tbaa !7
  %or22.i89.prol = or i8 %31, 12
  store i8 %or22.i89.prol, i8* %arrayidx20.i88.prol, align 1, !tbaa !7
  %y.0.i90.prol = add i64 %cond.i169, 2
  %32 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i95.prol.loopexit

for.body.i95.prol.loopexit:                       ; preds = %for.body.i95.preheader, %for.body.i95.prol
  %.lcssa339.unr = phi i64 [ undef, %for.body.i95.preheader ], [ %32, %for.body.i95.prol ]
  %.unr349 = phi i64 [ %27, %for.body.i95.preheader ], [ %32, %for.body.i95.prol ]
  %y.0.i90246.unr = phi i64 [ %y.0.i90244, %for.body.i95.preheader ], [ %y.0.i90.prol, %for.body.i95.prol ]
  %33 = icmp eq i64 %30, %cond.i169
  br i1 %33, label %DrawSegment.exit103.loopexit, label %for.body.i95.preheader.new

for.body.i95.preheader.new:                       ; preds = %for.body.i95.prol.loopexit
  br label %for.body.i95

for.body.i95:                                     ; preds = %for.body.i95, %for.body.i95.preheader.new
  %34 = phi i64 [ %.unr349, %for.body.i95.preheader.new ], [ %38, %for.body.i95 ]
  %y.0.i90246 = phi i64 [ %y.0.i90246.unr, %for.body.i95.preheader.new ], [ %y.0.i90.1, %for.body.i95 ]
  %mul18.i94 = mul i64 %34, %y.0.i90246
  %add19.i87 = add i64 %mul18.i94, %i
  %arrayidx20.i88 = getelementptr inbounds i8, i8* %15, i64 %add19.i87
  %35 = load i8, i8* %arrayidx20.i88, align 1, !tbaa !7
  %or22.i89 = or i8 %35, 12
  store i8 %or22.i89, i8* %arrayidx20.i88, align 1, !tbaa !7
  %y.0.i90 = add i64 %y.0.i90246, 1
  %36 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i94.1 = mul i64 %36, %y.0.i90
  %add19.i87.1 = add i64 %mul18.i94.1, %i
  %arrayidx20.i88.1 = getelementptr inbounds i8, i8* %15, i64 %add19.i87.1
  %37 = load i8, i8* %arrayidx20.i88.1, align 1, !tbaa !7
  %or22.i89.1 = or i8 %37, 12
  store i8 %or22.i89.1, i8* %arrayidx20.i88.1, align 1, !tbaa !7
  %y.0.i90.1 = add i64 %y.0.i90246, 2
  %38 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %exitcond.1 = icmp eq i64 %y.0.i90.1, %cond11.i175
  br i1 %exitcond.1, label %DrawSegment.exit103.loopexit.unr-lcssa, label %for.body.i95

DrawSegment.exit103.loopexit.unr-lcssa:           ; preds = %for.body.i95
  br label %DrawSegment.exit103.loopexit

DrawSegment.exit103.loopexit:                     ; preds = %for.body.i95.prol.loopexit, %DrawSegment.exit103.loopexit.unr-lcssa
  %.lcssa339 = phi i64 [ %.lcssa339.unr, %for.body.i95.prol.loopexit ], [ %38, %DrawSegment.exit103.loopexit.unr-lcssa ]
  br label %DrawSegment.exit103

DrawSegment.exit103:                              ; preds = %DrawSegment.exit103.loopexit, %DrawSegment.exit130
  %.lcssa200 = phi i64 [ %27, %DrawSegment.exit130 ], [ %.lcssa339, %DrawSegment.exit103.loopexit ]
  %mul30.i96 = mul i64 %.lcssa200, %cond11.i175
  %add31.i97 = add i64 %mul30.i96, %i
  %arrayidx79.i101 = getelementptr inbounds i8, i8* %15, i64 %add31.i97
  %39 = load i8, i8* %arrayidx79.i101, align 1, !tbaa !7
  %or81.i102 = or i8 %39, 4
  store i8 %or81.i102, i8* %arrayidx79.i101, align 1, !tbaa !7
  %40 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i77 = mul i64 %40, %row.0270
  %add.i78 = add i64 %mul.i77, %i
  %arrayidx.i79 = getelementptr inbounds i8, i8* %23, i64 %add.i78
  store i8 1, i8* %arrayidx.i79, align 1, !tbaa !7
  %41 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %42 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i38 = mul i64 %42, %row.0270
  br i1 %cmp.i, label %if.then.i39, label %if.else.i60

if.then.i39:                                      ; preds = %DrawSegment.exit103
  %add19.i43227 = add i64 %mul.i38, %i
  %arrayidx20.i44228 = getelementptr inbounds i8, i8* %41, i64 %add19.i43227
  %43 = load i8, i8* %arrayidx20.i44228, align 1, !tbaa !7
  %or22.i45229 = or i8 %43, 8
  store i8 %or22.i45229, i8* %arrayidx20.i44228, align 1, !tbaa !7
  %cmp16.i47231 = icmp eq i64 %row.0270, -1
  %44 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i47231, label %for.body.i50.preheader, label %for.end.i53

for.body.i50.preheader:                           ; preds = %if.then.i39
  br i1 true, label %for.body.i50.prol.preheader, label %for.body.i50.prol.loopexit

for.body.i50.prol.preheader:                      ; preds = %for.body.i50.preheader
  br label %for.body.i50.prol

for.body.i50.prol:                                ; preds = %for.body.i50.prol.preheader
  %arrayidx20.i44.prol = getelementptr inbounds i8, i8* %41, i64 %i
  %45 = load i8, i8* %arrayidx20.i44.prol, align 1, !tbaa !7
  %or22.i45.prol = or i8 %45, 12
  store i8 %or22.i45.prol, i8* %arrayidx20.i44.prol, align 1, !tbaa !7
  %46 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i50.prol.loopexit

for.body.i50.prol.loopexit:                       ; preds = %for.body.i50.prol, %for.body.i50.preheader
  %.lcssa337.unr = phi i64 [ undef, %for.body.i50.preheader ], [ %46, %for.body.i50.prol ]
  %.unr348 = phi i64 [ %44, %for.body.i50.preheader ], [ %46, %for.body.i50.prol ]
  %y.0.i46232.unr = phi i64 [ 0, %for.body.i50.preheader ], [ 1, %for.body.i50.prol ]
  br i1 false, label %for.end.i53.loopexit, label %for.body.i50.preheader.new

for.body.i50.preheader.new:                       ; preds = %for.body.i50.prol.loopexit
  br label %for.body.i50

for.body.i50:                                     ; preds = %for.body.i50, %for.body.i50.preheader.new
  %47 = phi i64 [ %.unr348, %for.body.i50.preheader.new ], [ %51, %for.body.i50 ]
  %y.0.i46232 = phi i64 [ %y.0.i46232.unr, %for.body.i50.preheader.new ], [ %y.0.i46.1, %for.body.i50 ]
  %mul18.i49 = mul i64 %47, %y.0.i46232
  %add19.i43 = add i64 %mul18.i49, %i
  %arrayidx20.i44 = getelementptr inbounds i8, i8* %41, i64 %add19.i43
  %48 = load i8, i8* %arrayidx20.i44, align 1, !tbaa !7
  %or22.i45 = or i8 %48, 12
  store i8 %or22.i45, i8* %arrayidx20.i44, align 1, !tbaa !7
  %y.0.i46 = add nuw i64 %y.0.i46232, 1
  %49 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i49.1 = mul i64 %49, %y.0.i46
  %add19.i43.1 = add i64 %mul18.i49.1, %i
  %arrayidx20.i44.1 = getelementptr inbounds i8, i8* %41, i64 %add19.i43.1
  %50 = load i8, i8* %arrayidx20.i44.1, align 1, !tbaa !7
  %or22.i45.1 = or i8 %50, 12
  store i8 %or22.i45.1, i8* %arrayidx20.i44.1, align 1, !tbaa !7
  %y.0.i46.1 = add i64 %y.0.i46232, 2
  %cmp16.i47.1 = icmp eq i64 %y.0.i46.1, -1
  %51 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i47.1, label %for.end.i53.loopexit.unr-lcssa, label %for.body.i50

for.end.i53.loopexit.unr-lcssa:                   ; preds = %for.body.i50
  br label %for.end.i53.loopexit

for.end.i53.loopexit:                             ; preds = %for.body.i50.prol.loopexit, %for.end.i53.loopexit.unr-lcssa
  %.lcssa337 = phi i64 [ %.lcssa337.unr, %for.body.i50.prol.loopexit ], [ %51, %for.end.i53.loopexit.unr-lcssa ]
  br label %for.end.i53

for.end.i53:                                      ; preds = %for.end.i53.loopexit, %if.then.i39
  %.lcssa199 = phi i64 [ %44, %if.then.i39 ], [ %.lcssa337, %for.end.i53.loopexit ]
  %mul30.i51 = mul i64 %.lcssa199, %row.0270
  %add31.i52 = add i64 %mul30.i51, %i
  br label %DrawSegment.exit76

if.else.i60:                                      ; preds = %DrawSegment.exit103
  %cmp37.i55 = icmp ugt i64 %col.0267, %i
  %cond42.i56 = select i1 %cmp37.i55, i64 %i, i64 %col.0267
  %add43.i57 = add i64 %mul.i38, %cond42.i56
  %arrayidx44.i58 = getelementptr inbounds i8, i8* %41, i64 %add43.i57
  %52 = load i8, i8* %arrayidx44.i58, align 1, !tbaa !7
  %or46.i59 = or i8 %52, 2
  store i8 %or46.i59, i8* %arrayidx44.i58, align 1, !tbaa !7
  %x.0.i62234 = add i64 %cond42.i56, 1
  %cond61.i63 = select i1 %cmp37.i55, i64 %col.0267, i64 %i
  %cmp62.i64235 = icmp ult i64 %x.0.i62234, %cond61.i63
  %53 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i65236 = mul i64 %53, %row.0270
  br i1 %cmp62.i64235, label %for.body64.i69.preheader, label %for.end70.i71

for.body64.i69.preheader:                         ; preds = %if.else.i60
  br label %for.body64.i69

for.body64.i69:                                   ; preds = %for.body64.i69.preheader, %for.body64.i69
  %mul65.i65238 = phi i64 [ %mul65.i65, %for.body64.i69 ], [ %mul65.i65236, %for.body64.i69.preheader ]
  %x.0.i62237 = phi i64 [ %x.0.i62, %for.body64.i69 ], [ %x.0.i62234, %for.body64.i69.preheader ]
  %add66.i67 = add i64 %mul65.i65238, %x.0.i62237
  %arrayidx67.i68 = getelementptr inbounds i8, i8* %41, i64 %add66.i67
  store i8 3, i8* %arrayidx67.i68, align 1, !tbaa !7
  %x.0.i62 = add nuw i64 %x.0.i62237, 1
  %cmp62.i64 = icmp ult i64 %x.0.i62, %cond61.i63
  %54 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i65 = mul i64 %54, %row.0270
  br i1 %cmp62.i64, label %for.body64.i69, label %for.end70.i71.loopexit

for.end70.i71.loopexit:                           ; preds = %for.body64.i69
  br label %for.end70.i71

for.end70.i71:                                    ; preds = %for.end70.i71.loopexit, %if.else.i60
  %mul65.i65.lcssa = phi i64 [ %mul65.i65236, %if.else.i60 ], [ %mul65.i65, %for.end70.i71.loopexit ]
  %add78.i70 = add i64 %mul65.i65.lcssa, %cond61.i63
  br label %DrawSegment.exit76

DrawSegment.exit76:                               ; preds = %for.end.i53, %for.end70.i71
  %add78.sink.i72 = phi i64 [ %add78.i70, %for.end70.i71 ], [ %add31.i52, %for.end.i53 ]
  %.sink.i73 = phi i8 [ 1, %for.end70.i71 ], [ 4, %for.end.i53 ]
  %arrayidx79.i74 = getelementptr inbounds i8, i8* %41, i64 %add78.sink.i72
  %55 = load i8, i8* %arrayidx79.i74, align 1, !tbaa !7
  %or81.i75 = or i8 %55, %.sink.i73
  store i8 %or81.i75, i8* %arrayidx79.i74, align 1, !tbaa !7
  %56 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i34 = mul i64 %56, %row.0270
  %add.i35 = add i64 %mul.i34, %col.0267
  %arrayidx.i36 = getelementptr inbounds i8, i8* %23, i64 %add.i35
  store i8 1, i8* %arrayidx.i36, align 1, !tbaa !7
  %cmp1.i = icmp ult i64 %row.0270, %b2
  %cond.i = select i1 %cmp1.i, i64 %row.0270, i64 %b2
  %57 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i13 = mul i64 %57, %cond.i
  %add19.i18219 = add i64 %mul.i13, %col.0267
  %arrayidx20.i19220 = getelementptr inbounds i8, i8* %15, i64 %add19.i18219
  %58 = load i8, i8* %arrayidx20.i19220, align 1, !tbaa !7
  %or22.i20221 = or i8 %58, 8
  store i8 %or22.i20221, i8* %arrayidx20.i19220, align 1, !tbaa !7
  %y.0.i21222 = add i64 %cond.i, 1
  %cond15.i = select i1 %cmp1.i, i64 %b2, i64 %row.0270
  %cmp16.i22223 = icmp ult i64 %y.0.i21222, %cond15.i
  %59 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i22223, label %for.body.i25.preheader, label %DrawSegment.exit33

for.body.i25.preheader:                           ; preds = %DrawSegment.exit76
  br label %for.body.i25

for.body.i25:                                     ; preds = %for.body.i25.preheader, %for.body.i25
  %60 = phi i64 [ %62, %for.body.i25 ], [ %59, %for.body.i25.preheader ]
  %y.0.i21224 = phi i64 [ %y.0.i21, %for.body.i25 ], [ %y.0.i21222, %for.body.i25.preheader ]
  %mul18.i24 = mul i64 %60, %y.0.i21224
  %add19.i18 = add i64 %mul18.i24, %col.0267
  %arrayidx20.i19 = getelementptr inbounds i8, i8* %15, i64 %add19.i18
  %61 = load i8, i8* %arrayidx20.i19, align 1, !tbaa !7
  %or22.i20 = or i8 %61, 12
  store i8 %or22.i20, i8* %arrayidx20.i19, align 1, !tbaa !7
  %y.0.i21 = add nuw i64 %y.0.i21224, 1
  %cmp16.i22 = icmp ult i64 %y.0.i21, %cond15.i
  %62 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i22, label %for.body.i25, label %DrawSegment.exit33.loopexit

DrawSegment.exit33.loopexit:                      ; preds = %for.body.i25
  br label %DrawSegment.exit33

DrawSegment.exit33:                               ; preds = %DrawSegment.exit33.loopexit, %DrawSegment.exit76
  %.lcssa198 = phi i64 [ %59, %DrawSegment.exit76 ], [ %62, %DrawSegment.exit33.loopexit ]
  %mul30.i26 = mul i64 %.lcssa198, %cond15.i
  %add31.i27 = add i64 %mul30.i26, %col.0267
  %arrayidx79.i31 = getelementptr inbounds i8, i8* %15, i64 %add31.i27
  %63 = load i8, i8* %arrayidx79.i31, align 1, !tbaa !7
  %or81.i32 = or i8 %63, 4
  store i8 %or81.i32, i8* %arrayidx79.i31, align 1, !tbaa !7
  %64 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i10 = mul i64 %64, %b2
  %add.i11 = add i64 %mul.i10, %col.0267
  %arrayidx.i12 = getelementptr inbounds i8, i8* %23, i64 %add.i11
  store i8 1, i8* %arrayidx.i12, align 1, !tbaa !7
  %65 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i2 = mul i64 %65, %b2
  br i1 %cmp.i, label %for.cond.i6.preheader, label %if.else.i8

for.cond.i6.preheader:                            ; preds = %DrawSegment.exit33
  %add19.i206 = add i64 %mul.i2, %i
  %arrayidx20.i207 = getelementptr inbounds i8, i8* %41, i64 %add19.i206
  %66 = load i8, i8* %arrayidx20.i207, align 1, !tbaa !7
  %or22.i208 = or i8 %66, 8
  store i8 %or22.i208, i8* %arrayidx20.i207, align 1, !tbaa !7
  %cmp16.i5210 = icmp eq i64 %b2, -1
  %67 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i5210, label %for.body.i7.preheader, label %for.end.i

for.body.i7.preheader:                            ; preds = %for.cond.i6.preheader
  br i1 true, label %for.body.i7.prol.preheader, label %for.body.i7.prol.loopexit

for.body.i7.prol.preheader:                       ; preds = %for.body.i7.preheader
  br label %for.body.i7.prol

for.body.i7.prol:                                 ; preds = %for.body.i7.prol.preheader
  %arrayidx20.i.prol = getelementptr inbounds i8, i8* %41, i64 %i
  %68 = load i8, i8* %arrayidx20.i.prol, align 1, !tbaa !7
  %or22.i.prol = or i8 %68, 12
  store i8 %or22.i.prol, i8* %arrayidx20.i.prol, align 1, !tbaa !7
  %69 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i7.prol.loopexit

for.body.i7.prol.loopexit:                        ; preds = %for.body.i7.prol, %for.body.i7.preheader
  %.lcssa334.unr = phi i64 [ undef, %for.body.i7.preheader ], [ %69, %for.body.i7.prol ]
  %.unr = phi i64 [ %67, %for.body.i7.preheader ], [ %69, %for.body.i7.prol ]
  %y.0.i4211.unr = phi i64 [ 0, %for.body.i7.preheader ], [ 1, %for.body.i7.prol ]
  br i1 false, label %for.end.i.loopexit, label %for.body.i7.preheader.new

for.body.i7.preheader.new:                        ; preds = %for.body.i7.prol.loopexit
  br label %for.body.i7

for.body.i7:                                      ; preds = %for.body.i7, %for.body.i7.preheader.new
  %70 = phi i64 [ %.unr, %for.body.i7.preheader.new ], [ %74, %for.body.i7 ]
  %y.0.i4211 = phi i64 [ %y.0.i4211.unr, %for.body.i7.preheader.new ], [ %y.0.i4.1, %for.body.i7 ]
  %mul18.i = mul i64 %70, %y.0.i4211
  %add19.i = add i64 %mul18.i, %i
  %arrayidx20.i = getelementptr inbounds i8, i8* %41, i64 %add19.i
  %71 = load i8, i8* %arrayidx20.i, align 1, !tbaa !7
  %or22.i = or i8 %71, 12
  store i8 %or22.i, i8* %arrayidx20.i, align 1, !tbaa !7
  %y.0.i4 = add nuw i64 %y.0.i4211, 1
  %72 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i.1 = mul i64 %72, %y.0.i4
  %add19.i.1 = add i64 %mul18.i.1, %i
  %arrayidx20.i.1 = getelementptr inbounds i8, i8* %41, i64 %add19.i.1
  %73 = load i8, i8* %arrayidx20.i.1, align 1, !tbaa !7
  %or22.i.1 = or i8 %73, 12
  store i8 %or22.i.1, i8* %arrayidx20.i.1, align 1, !tbaa !7
  %y.0.i4.1 = add i64 %y.0.i4211, 2
  %cmp16.i5.1 = icmp eq i64 %y.0.i4.1, -1
  %74 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i5.1, label %for.end.i.loopexit.unr-lcssa, label %for.body.i7

for.end.i.loopexit.unr-lcssa:                     ; preds = %for.body.i7
  br label %for.end.i.loopexit

for.end.i.loopexit:                               ; preds = %for.body.i7.prol.loopexit, %for.end.i.loopexit.unr-lcssa
  %.lcssa334 = phi i64 [ %.lcssa334.unr, %for.body.i7.prol.loopexit ], [ %74, %for.end.i.loopexit.unr-lcssa ]
  br label %for.end.i

for.end.i:                                        ; preds = %for.end.i.loopexit, %for.cond.i6.preheader
  %.lcssa = phi i64 [ %67, %for.cond.i6.preheader ], [ %.lcssa334, %for.end.i.loopexit ]
  %mul30.i = mul i64 %.lcssa, %b2
  %add31.i = add i64 %mul30.i, %i
  br label %DrawSegment.exit

if.else.i8:                                       ; preds = %DrawSegment.exit33
  %cmp37.i = icmp ult i64 %col.0267, %i
  %cond42.i = select i1 %cmp37.i, i64 %col.0267, i64 %i
  %add43.i = add i64 %mul.i2, %cond42.i
  %arrayidx44.i = getelementptr inbounds i8, i8* %41, i64 %add43.i
  %75 = load i8, i8* %arrayidx44.i, align 1, !tbaa !7
  %or46.i = or i8 %75, 2
  store i8 %or46.i, i8* %arrayidx44.i, align 1, !tbaa !7
  %x.0.i9212 = add i64 %cond42.i, 1
  %cond61.i = select i1 %cmp37.i, i64 %i, i64 %col.0267
  %cmp62.i213 = icmp ult i64 %x.0.i9212, %cond61.i
  %76 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i214 = mul i64 %76, %b2
  br i1 %cmp62.i213, label %for.body64.i.preheader, label %for.end70.i

for.body64.i.preheader:                           ; preds = %if.else.i8
  br label %for.body64.i

for.body64.i:                                     ; preds = %for.body64.i.preheader, %for.body64.i
  %mul65.i216 = phi i64 [ %mul65.i, %for.body64.i ], [ %mul65.i214, %for.body64.i.preheader ]
  %x.0.i9215 = phi i64 [ %x.0.i9, %for.body64.i ], [ %x.0.i9212, %for.body64.i.preheader ]
  %add66.i = add i64 %mul65.i216, %x.0.i9215
  %arrayidx67.i = getelementptr inbounds i8, i8* %41, i64 %add66.i
  store i8 3, i8* %arrayidx67.i, align 1, !tbaa !7
  %x.0.i9 = add nuw i64 %x.0.i9215, 1
  %cmp62.i = icmp ult i64 %x.0.i9, %cond61.i
  %77 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i = mul i64 %77, %b2
  br i1 %cmp62.i, label %for.body64.i, label %for.end70.i.loopexit

for.end70.i.loopexit:                             ; preds = %for.body64.i
  br label %for.end70.i

for.end70.i:                                      ; preds = %for.end70.i.loopexit, %if.else.i8
  %mul65.i.lcssa = phi i64 [ %mul65.i214, %if.else.i8 ], [ %mul65.i, %for.end70.i.loopexit ]
  %add78.i = add i64 %mul65.i.lcssa, %cond61.i
  br label %DrawSegment.exit

DrawSegment.exit:                                 ; preds = %for.end.i, %for.end70.i
  %add78.sink.i = phi i64 [ %add78.i, %for.end70.i ], [ %add31.i, %for.end.i ]
  %.sink.i = phi i8 [ 1, %for.end70.i ], [ 4, %for.end.i ]
  %arrayidx79.i = getelementptr inbounds i8, i8* %41, i64 %add78.sink.i
  %78 = load i8, i8* %arrayidx79.i, align 1, !tbaa !7
  %or81.i = or i8 %78, %.sink.i
  store i8 %or81.i, i8* %arrayidx79.i, align 1, !tbaa !7
  br label %cleanup

for.inc.loopexit:                                 ; preds = %for.body.i162
  br label %for.inc

for.inc.loopexit329:                              ; preds = %for.body.i180
  br label %for.inc

for.inc.loopexit330:                              ; preds = %for.body.i142
  br label %for.inc

for.inc:                                          ; preds = %for.inc.loopexit330, %for.inc.loopexit329, %for.inc.loopexit, %HasVCV.exit, %land.lhs.true20
  %add24 = add i64 %col.0267, %conv
  %cmp5 = icmp eq i64 %add24, %add
  br i1 %cmp5, label %for.inc25.loopexit333, label %for.body7

for.inc25.loopexit:                               ; preds = %for.body.i
  br label %for.inc25

for.inc25.loopexit331:                            ; preds = %for.body34.i
  br label %for.inc25

for.inc25.loopexit333:                            ; preds = %for.inc
  br label %for.inc25

for.inc25:                                        ; preds = %for.inc25.loopexit333, %for.inc25.loopexit331, %for.inc25.loopexit, %for.cond4.preheader
  %add27 = add i64 %row.0270, 1
  %cmp = icmp eq i64 %add27, %add2
  br i1 %cmp, label %cleanup.loopexit, label %for.cond4.preheader

cleanup.loopexit:                                 ; preds = %for.inc25
  br label %cleanup

cleanup:                                          ; preds = %cleanup.loopexit, %entry, %DrawSegment.exit
  %retval.0 = phi i32 [ 1, %DrawSegment.exit ], [ 0, %entry ], [ 0, %cleanup.loopexit ]
  ret i32 %retval.0
}

; Function Attrs: norecurse nounwind uwtable
define void @FindFreeHorzSeg(i64 %startCol, i64 %row, i64* nocapture %rowStart, i64* nocapture %rowEnd) local_unnamed_addr #3 {
entry:
  %cmp14 = icmp eq i64 %startCol, 0
  br i1 %cmp14, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %0 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %1 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul = mul i64 %1, %row
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %i.015 = phi i64 [ %startCol, %for.body.lr.ph ], [ %dec, %for.inc ]
  %add = add i64 %mul, %i.015
  %arrayidx = getelementptr inbounds i8, i8* %0, i64 %add
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !7
  %tobool = icmp eq i8 %2, 0
  br i1 %tobool, label %for.inc, label %for.end.loopexit

for.inc:                                          ; preds = %for.body
  %dec = add i64 %i.015, -1
  %cmp = icmp eq i64 %dec, 0
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body, %for.inc
  %i.0.lcssa.ph = phi i64 [ %i.015, %for.body ], [ 0, %for.inc ]
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %i.0.lcssa = phi i64 [ 0, %entry ], [ %i.0.lcssa.ph, %for.end.loopexit ]
  %add1 = add i64 %i.0.lcssa, 1
  store i64 %add1, i64* %rowStart, align 8, !tbaa !1
  %3 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp311 = icmp ult i64 %3, %startCol
  br i1 %cmp311, label %for.end12, label %for.body4.lr.ph

for.body4.lr.ph:                                  ; preds = %for.end
  %4 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %mul5 = mul i64 %3, %row
  br label %for.body4

for.body4:                                        ; preds = %for.body4.lr.ph, %for.inc11
  %i.112 = phi i64 [ %startCol, %for.body4.lr.ph ], [ %inc, %for.inc11 ]
  %add6 = add i64 %mul5, %i.112
  %arrayidx7 = getelementptr inbounds i8, i8* %4, i64 %add6
  %5 = load i8, i8* %arrayidx7, align 1, !tbaa !7
  %tobool8 = icmp eq i8 %5, 0
  br i1 %tobool8, label %for.inc11, label %for.end12.loopexit

for.inc11:                                        ; preds = %for.body4
  %inc = add i64 %i.112, 1
  %cmp3 = icmp ugt i64 %inc, %3
  br i1 %cmp3, label %for.end12.loopexit, label %for.body4

for.end12.loopexit:                               ; preds = %for.body4, %for.inc11
  %i.1.lcssa.ph = phi i64 [ %i.112, %for.body4 ], [ %inc, %for.inc11 ]
  br label %for.end12

for.end12:                                        ; preds = %for.end12.loopexit, %for.end
  %i.1.lcssa = phi i64 [ %startCol, %for.end ], [ %i.1.lcssa.ph, %for.end12.loopexit ]
  %sub = add i64 %i.1.lcssa, -1
  store i64 %sub, i64* %rowEnd, align 8, !tbaa !1
  ret void
}

; Function Attrs: nounwind uwtable
define i32 @Maze3() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp186 = icmp eq i64 %0, 0
  br i1 %cmp186, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.inc
  %i.0188 = phi i64 [ %inc13, %for.inc ], [ 1, %for.body.preheader ]
  %numLeft.0187 = phi i32 [ %numLeft.1, %for.inc ], [ 0, %for.body.preheader ]
  %1 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i8, i8* %1, i64 %i.0188
  %2 = load i8, i8* %arrayidx, align 1, !tbaa !7
  %tobool = icmp eq i8 %2, 0
  br i1 %tobool, label %for.inc, label %if.then

if.then:                                          ; preds = %for.body
  %3 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %4 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx1 = getelementptr inbounds i64, i64* %4, i64 %i.0188
  %5 = load i64, i64* %arrayidx1, align 8, !tbaa !1
  %arrayidx2 = getelementptr inbounds i64, i64* %3, i64 %5
  %6 = load i64, i64* %arrayidx2, align 8, !tbaa !1
  %7 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx3 = getelementptr inbounds i64, i64* %7, i64 %i.0188
  %8 = load i64, i64* %arrayidx3, align 8, !tbaa !1
  %arrayidx4 = getelementptr inbounds i64, i64* %3, i64 %8
  %9 = load i64, i64* %arrayidx4, align 8, !tbaa !1
  %10 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add = add i64 %10, 1
  %topRow.0.i97 = add i64 %9, 1
  %sub.i = add i64 %6, -1
  %cmp.i98 = icmp ult i64 %topRow.0.i97, %sub.i
  br i1 %cmp.i98, label %for.cond.i.i.preheader.lr.ph, label %if.else

for.cond.i.i.preheader.lr.ph:                     ; preds = %if.then
  %cmp.i.i56 = icmp eq i64 %i.0188, 0
  %add26.i = add i64 %6, 1
  %sub42.i = add i64 %9, -1
  br label %for.cond.i.i.preheader

for.cond.i.i.preheader:                           ; preds = %for.cond.i.i.preheader.lr.ph, %for.cond.i.backedge
  %topRow.0.i100 = phi i64 [ %topRow.0.i97, %for.cond.i.i.preheader.lr.ph ], [ %topRow.0.i, %for.cond.i.backedge ]
  %topRow.0.in.i99 = phi i64 [ %9, %for.cond.i.i.preheader.lr.ph ], [ %topRow.0.i100, %for.cond.i.backedge ]
  br i1 %cmp.i.i56, label %for.cond.i.i.preheader.for.end.i.i_crit_edge, label %for.body.i.i.lr.ph

for.cond.i.i.preheader.for.end.i.i_crit_edge:     ; preds = %for.cond.i.i.preheader
  %.pre = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.end.i.i

for.body.i.i.lr.ph:                               ; preds = %for.cond.i.i.preheader
  %11 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %12 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i.i = mul i64 %12, %topRow.0.i100
  br label %for.body.i.i

for.body.i.i:                                     ; preds = %for.body.i.i.lr.ph, %for.inc.i.i
  %i.0.i.i57 = phi i64 [ %i.0188, %for.body.i.i.lr.ph ], [ %dec.i.i, %for.inc.i.i ]
  %add.i.i = add i64 %mul.i.i, %i.0.i.i57
  %arrayidx.i.i = getelementptr inbounds i8, i8* %11, i64 %add.i.i
  %13 = load i8, i8* %arrayidx.i.i, align 1, !tbaa !7
  %tobool.i.i = icmp eq i8 %13, 0
  br i1 %tobool.i.i, label %for.inc.i.i, label %for.end.i.i.loopexit

for.inc.i.i:                                      ; preds = %for.body.i.i
  %dec.i.i = add i64 %i.0.i.i57, -1
  %cmp.i.i = icmp eq i64 %dec.i.i, 0
  br i1 %cmp.i.i, label %for.end.i.i.loopexit, label %for.body.i.i

for.end.i.i.loopexit:                             ; preds = %for.body.i.i, %for.inc.i.i
  %i.0.i.i.lcssa.ph = phi i64 [ 0, %for.inc.i.i ], [ %i.0.i.i57, %for.body.i.i ]
  br label %for.end.i.i

for.end.i.i:                                      ; preds = %for.end.i.i.loopexit, %for.cond.i.i.preheader.for.end.i.i_crit_edge
  %14 = phi i64 [ %.pre, %for.cond.i.i.preheader.for.end.i.i_crit_edge ], [ %12, %for.end.i.i.loopexit ]
  %i.0.i.i.lcssa = phi i64 [ 0, %for.cond.i.i.preheader.for.end.i.i_crit_edge ], [ %i.0.i.i.lcssa.ph, %for.end.i.i.loopexit ]
  %add1.i.i = add i64 %i.0.i.i.lcssa, 1
  %cmp3.i.i59 = icmp ugt i64 %i.0188, %14
  br i1 %cmp3.i.i59, label %FindFreeHorzSeg.exit.i, label %for.body4.i.i.lr.ph

for.body4.i.i.lr.ph:                              ; preds = %for.end.i.i
  %15 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %mul5.i.i = mul i64 %14, %topRow.0.i100
  br label %for.body4.i.i

for.body4.i.i:                                    ; preds = %for.body4.i.i.lr.ph, %for.inc11.i.i
  %i.1.i.i60 = phi i64 [ %i.0188, %for.body4.i.i.lr.ph ], [ %inc.i.i, %for.inc11.i.i ]
  %add6.i.i = add i64 %mul5.i.i, %i.1.i.i60
  %arrayidx7.i.i = getelementptr inbounds i8, i8* %15, i64 %add6.i.i
  %16 = load i8, i8* %arrayidx7.i.i, align 1, !tbaa !7
  %tobool8.i.i = icmp eq i8 %16, 0
  br i1 %tobool8.i.i, label %for.inc11.i.i, label %FindFreeHorzSeg.exit.i.loopexit

for.inc11.i.i:                                    ; preds = %for.body4.i.i
  %inc.i.i = add i64 %i.1.i.i60, 1
  %cmp3.i.i = icmp ugt i64 %inc.i.i, %14
  br i1 %cmp3.i.i, label %FindFreeHorzSeg.exit.i.loopexit, label %for.body4.i.i

FindFreeHorzSeg.exit.i.loopexit:                  ; preds = %for.body4.i.i, %for.inc11.i.i
  %i.1.i.i.lcssa.ph = phi i64 [ %i.1.i.i60, %for.body4.i.i ], [ %inc.i.i, %for.inc11.i.i ]
  br label %FindFreeHorzSeg.exit.i

FindFreeHorzSeg.exit.i:                           ; preds = %FindFreeHorzSeg.exit.i.loopexit, %for.end.i.i
  %i.1.i.i.lcssa = phi i64 [ %i.0188, %for.end.i.i ], [ %i.1.i.i.lcssa.ph, %FindFreeHorzSeg.exit.i.loopexit ]
  %sub.i.i = add i64 %i.1.i.i.lcssa, -1
  %cmp1.i = icmp ugt i64 %sub.i.i, %add1.i.i
  br i1 %cmp1.i, label %if.end.i, label %for.cond.i.backedge

for.cond.i.backedge.loopexit:                     ; preds = %for.inc56.i
  br label %for.cond.i.backedge

for.cond.i.backedge:                              ; preds = %for.cond.i.backedge.loopexit, %if.end.i, %FindFreeHorzSeg.exit.i
  %topRow.0.i = add i64 %topRow.0.i100, 1
  %cmp.i = icmp ult i64 %topRow.0.i, %sub.i
  br i1 %cmp.i, label %for.cond.i.i.preheader, label %if.else.loopexit

if.end.i:                                         ; preds = %FindFreeHorzSeg.exit.i
  %add2.i = add i64 %topRow.0.in.i99, 2
  %cmp4.i93 = icmp ult i64 %add2.i, %6
  br i1 %cmp4.i93, label %for.cond.i239.i.preheader.lr.ph, label %for.cond.i.backedge

for.cond.i239.i.preheader.lr.ph:                  ; preds = %if.end.i
  %cmp10.i90 = icmp ugt i64 %add1.i.i, %sub.i.i
  %cmp1.i340.i = icmp ult i64 %topRow.0.i100, %add26.i
  %cond.i341.i = select i1 %cmp1.i340.i, i64 %topRow.0.i100, i64 %add26.i
  %cond11.i347.i = select i1 %cmp1.i340.i, i64 %add26.i, i64 %topRow.0.i100
  br label %for.cond.i239.i.preheader

for.cond.i239.i.preheader:                        ; preds = %for.cond.i239.i.preheader.lr.ph, %for.inc56.i
  %botRow.0.i94 = phi i64 [ %add2.i, %for.cond.i239.i.preheader.lr.ph ], [ %inc57.i, %for.inc56.i ]
  br i1 %cmp.i.i56, label %for.cond.i239.i.preheader.for.end.i248.i_crit_edge, label %for.body.i244.i.lr.ph

for.cond.i239.i.preheader.for.end.i248.i_crit_edge: ; preds = %for.cond.i239.i.preheader
  %.pre221 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.end.i248.i

for.body.i244.i.lr.ph:                            ; preds = %for.cond.i239.i.preheader
  %17 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %18 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i240.i = mul i64 %18, %botRow.0.i94
  br label %for.body.i244.i

for.body.i244.i:                                  ; preds = %for.body.i244.i.lr.ph, %for.inc.i246.i
  %i.0.i237.i64 = phi i64 [ %i.0188, %for.body.i244.i.lr.ph ], [ %dec.i245.i, %for.inc.i246.i ]
  %add.i241.i = add i64 %mul.i240.i, %i.0.i237.i64
  %arrayidx.i242.i = getelementptr inbounds i8, i8* %17, i64 %add.i241.i
  %19 = load i8, i8* %arrayidx.i242.i, align 1, !tbaa !7
  %tobool.i243.i = icmp eq i8 %19, 0
  br i1 %tobool.i243.i, label %for.inc.i246.i, label %for.end.i248.i.loopexit

for.inc.i246.i:                                   ; preds = %for.body.i244.i
  %dec.i245.i = add i64 %i.0.i237.i64, -1
  %cmp.i238.i = icmp eq i64 %dec.i245.i, 0
  br i1 %cmp.i238.i, label %for.end.i248.i.loopexit, label %for.body.i244.i

for.end.i248.i.loopexit:                          ; preds = %for.body.i244.i, %for.inc.i246.i
  %i.0.i237.i.lcssa.ph = phi i64 [ 0, %for.inc.i246.i ], [ %i.0.i237.i64, %for.body.i244.i ]
  br label %for.end.i248.i

for.end.i248.i:                                   ; preds = %for.end.i248.i.loopexit, %for.cond.i239.i.preheader.for.end.i248.i_crit_edge
  %20 = phi i64 [ %.pre221, %for.cond.i239.i.preheader.for.end.i248.i_crit_edge ], [ %18, %for.end.i248.i.loopexit ]
  %i.0.i237.i.lcssa = phi i64 [ 0, %for.cond.i239.i.preheader.for.end.i248.i_crit_edge ], [ %i.0.i237.i.lcssa.ph, %for.end.i248.i.loopexit ]
  %add1.i247.i = add i64 %i.0.i237.i.lcssa, 1
  %cmp3.i250.i67 = icmp ugt i64 %i.0188, %20
  br i1 %cmp3.i250.i67, label %FindFreeHorzSeg.exit260.i, label %for.body4.i256.i.lr.ph

for.body4.i256.i.lr.ph:                           ; preds = %for.end.i248.i
  %21 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %mul5.i252.i = mul i64 %20, %botRow.0.i94
  br label %for.body4.i256.i

for.body4.i256.i:                                 ; preds = %for.body4.i256.i.lr.ph, %for.inc11.i258.i
  %i.1.i249.i68 = phi i64 [ %i.0188, %for.body4.i256.i.lr.ph ], [ %inc.i257.i, %for.inc11.i258.i ]
  %add6.i253.i = add i64 %mul5.i252.i, %i.1.i249.i68
  %arrayidx7.i254.i = getelementptr inbounds i8, i8* %21, i64 %add6.i253.i
  %22 = load i8, i8* %arrayidx7.i254.i, align 1, !tbaa !7
  %tobool8.i255.i = icmp eq i8 %22, 0
  br i1 %tobool8.i255.i, label %for.inc11.i258.i, label %FindFreeHorzSeg.exit260.i.loopexit

for.inc11.i258.i:                                 ; preds = %for.body4.i256.i
  %inc.i257.i = add i64 %i.1.i249.i68, 1
  %cmp3.i250.i = icmp ugt i64 %inc.i257.i, %20
  br i1 %cmp3.i250.i, label %FindFreeHorzSeg.exit260.i.loopexit, label %for.body4.i256.i

FindFreeHorzSeg.exit260.i.loopexit:               ; preds = %for.body4.i256.i, %for.inc11.i258.i
  %i.1.i249.i.lcssa.ph = phi i64 [ %i.1.i249.i68, %for.body4.i256.i ], [ %inc.i257.i, %for.inc11.i258.i ]
  br label %FindFreeHorzSeg.exit260.i

FindFreeHorzSeg.exit260.i:                        ; preds = %FindFreeHorzSeg.exit260.i.loopexit, %for.end.i248.i
  %i.1.i249.i.lcssa = phi i64 [ %i.0188, %for.end.i248.i ], [ %i.1.i249.i.lcssa.ph, %FindFreeHorzSeg.exit260.i.loopexit ]
  %sub.i259.i = add i64 %i.1.i249.i.lcssa, -1
  %cmp6.i.not = icmp ule i64 %sub.i259.i, %add1.i247.i
  %brmerge = or i1 %cmp6.i.not, %cmp10.i90
  br i1 %brmerge, label %for.inc56.i, label %for.cond12.i.preheader.lr.ph

for.cond12.i.preheader.lr.ph:                     ; preds = %FindFreeHorzSeg.exit260.i
  %cmp13.i87 = icmp ugt i64 %add1.i247.i, %sub.i259.i
  %cmp18.i = icmp eq i64 %topRow.0.i100, %botRow.0.i94
  %cmp1.i294.i = icmp ult i64 %add, %botRow.0.i94
  %cond.i295.i = select i1 %cmp1.i294.i, i64 %add, i64 %botRow.0.i94
  %cond11.i301.i = select i1 %cmp1.i294.i, i64 %botRow.0.i94, i64 %add
  %cmp1.i263.i = icmp ult i64 %botRow.0.i94, %sub42.i
  %cond.i264.i = select i1 %cmp1.i263.i, i64 %botRow.0.i94, i64 %sub42.i
  %cond11.i.i = select i1 %cmp1.i263.i, i64 %sub42.i, i64 %botRow.0.i94
  br label %for.cond12.i.preheader

for.cond12.i.preheader:                           ; preds = %for.cond12.i.preheader.lr.ph, %for.inc53.i
  %topCol.0.i91 = phi i64 [ %add1.i.i, %for.cond12.i.preheader.lr.ph ], [ %inc54.i, %for.inc53.i ]
  br i1 %cmp13.i87, label %for.inc53.i, label %for.body14.i.lr.ph

for.body14.i.lr.ph:                               ; preds = %for.cond12.i.preheader
  %cmp15.i = icmp eq i64 %topCol.0.i91, %i.0188
  %cmp16.i373.i = icmp ult i64 %i.0188, %topCol.0.i91
  %cond20.i374.i = select i1 %cmp16.i373.i, i64 %i.0188, i64 %topCol.0.i91
  %cond32.i379.i = select i1 %cmp16.i373.i, i64 %topCol.0.i91, i64 %i.0188
  br label %for.body14.i

for.body14.i:                                     ; preds = %for.body14.i.lr.ph, %for.inc.i
  %botCol.0.i88 = phi i64 [ %add1.i247.i, %for.body14.i.lr.ph ], [ %inc.i, %for.inc.i ]
  %cmp16.i = icmp eq i64 %botCol.0.i88, %i.0188
  %or.cond.i = or i1 %cmp15.i, %cmp16.i
  %or.cond1.i = or i1 %cmp18.i, %or.cond.i
  %cmp20.i = icmp eq i64 %topCol.0.i91, %botCol.0.i88
  %or.cond2.i = or i1 %cmp20.i, %or.cond1.i
  br i1 %or.cond2.i, label %for.inc.i, label %land.lhs.true21.i

land.lhs.true21.i:                                ; preds = %for.body14.i
  %23 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %24 = load i64, i64* @channelColumns, align 8
  br label %for.body.i321.i

for.body.i321.i:                                  ; preds = %land.lhs.true21.i, %for.inc.i324.i
  %index.0.i315.i72 = phi i64 [ %i.0188, %land.lhs.true21.i ], [ %add14.i323.i, %for.inc.i324.i ]
  %y.0.i314.i71 = phi i64 [ 0, %land.lhs.true21.i ], [ %inc.i322.i, %for.inc.i324.i ]
  %arrayidx.i319.i = getelementptr inbounds i8, i8* %23, i64 %index.0.i315.i72
  %25 = load i8, i8* %arrayidx.i319.i, align 1, !tbaa !7
  %tobool.i320.i = icmp eq i8 %25, 0
  br i1 %tobool.i320.i, label %for.inc.i324.i, label %for.inc.i.loopexit265

for.inc.i324.i:                                   ; preds = %for.body.i321.i
  %inc.i322.i = add i64 %y.0.i314.i71, 1
  %add14.i323.i = add i64 %24, %index.0.i315.i72
  %cmp12.i317.i = icmp ugt i64 %inc.i322.i, %topRow.0.i100
  br i1 %cmp12.i317.i, label %if.else.i376.i, label %for.body.i321.i

if.else.i376.i:                                   ; preds = %for.inc.i324.i
  %26 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %mul.i359.i = mul i64 %24, %topRow.0.i100
  %add21.i375.i = add i64 %mul.i359.i, %cond20.i374.i
  br label %for.body34.i384.i

for.body34.i384.i:                                ; preds = %if.else.i376.i, %for.inc39.i387.i
  %index.1.i378.i74 = phi i64 [ %add21.i375.i, %if.else.i376.i ], [ %inc41.i386.i, %for.inc39.i387.i ]
  %x.0.i377.i73 = phi i64 [ %cond20.i374.i, %if.else.i376.i ], [ %inc40.i385.i, %for.inc39.i387.i ]
  %arrayidx35.i382.i = getelementptr inbounds i8, i8* %26, i64 %index.1.i378.i74
  %27 = load i8, i8* %arrayidx35.i382.i, align 1, !tbaa !7
  %tobool36.i383.i = icmp eq i8 %27, 0
  br i1 %tobool36.i383.i, label %for.inc39.i387.i, label %for.inc.i.loopexit264

for.inc39.i387.i:                                 ; preds = %for.body34.i384.i
  %inc40.i385.i = add i64 %x.0.i377.i73, 1
  %inc41.i386.i = add i64 %index.1.i378.i74, 1
  %cmp33.i380.i = icmp ugt i64 %inc40.i385.i, %cond32.i379.i
  br i1 %cmp33.i380.i, label %land.lhs.true25.i, label %for.body34.i384.i

land.lhs.true25.i:                                ; preds = %for.inc39.i387.i
  %mul.i342.i = mul i64 %24, %cond.i341.i
  %add.i343.i = add i64 %mul.i342.i, %topCol.0.i91
  br label %for.body.i352.i

for.body.i352.i:                                  ; preds = %land.lhs.true25.i, %for.inc.i355.i
  %index.0.i346.i78 = phi i64 [ %add.i343.i, %land.lhs.true25.i ], [ %add14.i354.i, %for.inc.i355.i ]
  %y.0.i345.i77 = phi i64 [ %cond.i341.i, %land.lhs.true25.i ], [ %inc.i353.i, %for.inc.i355.i ]
  %arrayidx.i350.i = getelementptr inbounds i8, i8* %23, i64 %index.0.i346.i78
  %28 = load i8, i8* %arrayidx.i350.i, align 1, !tbaa !7
  %tobool.i351.i = icmp eq i8 %28, 0
  br i1 %tobool.i351.i, label %for.inc.i355.i, label %for.inc.i.loopexit263

for.inc.i355.i:                                   ; preds = %for.body.i352.i
  %inc.i353.i = add i64 %y.0.i345.i77, 1
  %add14.i354.i = add i64 %24, %index.0.i346.i78
  %cmp12.i348.i = icmp ugt i64 %inc.i353.i, %cond11.i347.i
  br i1 %cmp12.i348.i, label %land.lhs.true29.i, label %for.body.i352.i

land.lhs.true29.i:                                ; preds = %for.inc.i355.i
  %29 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx.i327.i = getelementptr inbounds i64, i64* %29, i64 %topCol.0.i91
  %30 = load i64, i64* %arrayidx.i327.i, align 8, !tbaa !1
  %cmp.i328.i = icmp eq i64 %30, 0
  br i1 %cmp.i328.i, label %land.lhs.true32.i, label %land.lhs.true.i333.i

land.lhs.true.i333.i:                             ; preds = %land.lhs.true29.i
  %31 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx1.i329.i = getelementptr inbounds i64, i64* %31, i64 %topCol.0.i91
  %32 = load i64, i64* %arrayidx1.i329.i, align 8, !tbaa !1
  %cmp2.i330.i = icmp eq i64 %32, 0
  %cmp6.i331.i = icmp eq i64 %30, %32
  %or.cond.i332.i = or i1 %cmp2.i330.i, %cmp6.i331.i
  br i1 %or.cond.i332.i, label %land.lhs.true32.i, label %HasVCV.exit339.i

HasVCV.exit339.i:                                 ; preds = %land.lhs.true.i333.i
  %33 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx8.i334.i = getelementptr inbounds i64, i64* %33, i64 %30
  %34 = load i64, i64* %arrayidx8.i334.i, align 8, !tbaa !1
  %arrayidx10.i335.i = getelementptr inbounds i64, i64* %33, i64 %32
  %35 = load i64, i64* %arrayidx10.i335.i, align 8, !tbaa !1
  %cmp11.i336.i = icmp ugt i64 %34, %35
  br i1 %cmp11.i336.i, label %for.inc.i, label %land.lhs.true32.i

land.lhs.true32.i:                                ; preds = %HasVCV.exit339.i, %land.lhs.true29.i, %land.lhs.true.i333.i
  %call33.i = tail call i32 @ExtendOK(i64 %5, i8* %26, i64 %topCol.0.i91, i64 %6, i64 %i.0188, i64 %6) #6
  %tobool34.i = icmp eq i32 %call33.i, 0
  br i1 %tobool34.i, label %for.inc.i, label %land.lhs.true35.i

land.lhs.true35.i:                                ; preds = %land.lhs.true32.i
  %36 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %37 = load i64, i64* @channelColumns, align 8
  %mul.i296.i = mul i64 %37, %cond.i295.i
  %add.i297.i = add i64 %mul.i296.i, %i.0188
  br label %for.body.i306.i

for.body.i306.i:                                  ; preds = %land.lhs.true35.i, %for.inc.i309.i
  %index.0.i300.i80 = phi i64 [ %add.i297.i, %land.lhs.true35.i ], [ %add14.i308.i, %for.inc.i309.i ]
  %y.0.i299.i79 = phi i64 [ %cond.i295.i, %land.lhs.true35.i ], [ %inc.i307.i, %for.inc.i309.i ]
  %arrayidx.i304.i = getelementptr inbounds i8, i8* %36, i64 %index.0.i300.i80
  %38 = load i8, i8* %arrayidx.i304.i, align 1, !tbaa !7
  %tobool.i305.i = icmp eq i8 %38, 0
  br i1 %tobool.i305.i, label %for.inc.i309.i, label %for.inc.i.loopexit262

for.inc.i309.i:                                   ; preds = %for.body.i306.i
  %inc.i307.i = add i64 %y.0.i299.i79, 1
  %add14.i308.i = add i64 %37, %index.0.i300.i80
  %cmp12.i302.i = icmp ugt i64 %inc.i307.i, %cond11.i301.i
  br i1 %cmp12.i302.i, label %if.else.i290.i, label %for.body.i306.i

if.else.i290.i:                                   ; preds = %for.inc.i309.i
  %39 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %mul.i276.i = mul i64 %37, %botRow.0.i94
  %cmp16.i289.i = icmp ult i64 %i.0188, %botCol.0.i88
  %cond20.i.i = select i1 %cmp16.i289.i, i64 %i.0188, i64 %botCol.0.i88
  %add21.i.i = add i64 %mul.i276.i, %cond20.i.i
  %cond32.i.i = select i1 %cmp16.i289.i, i64 %botCol.0.i88, i64 %i.0188
  br label %for.body34.i.i

for.body34.i.i:                                   ; preds = %if.else.i290.i, %for.inc39.i.i
  %index.1.i.i82 = phi i64 [ %add21.i.i, %if.else.i290.i ], [ %inc41.i.i, %for.inc39.i.i ]
  %x.0.i291.i81 = phi i64 [ %cond20.i.i, %if.else.i290.i ], [ %inc40.i.i, %for.inc39.i.i ]
  %arrayidx35.i.i = getelementptr inbounds i8, i8* %39, i64 %index.1.i.i82
  %40 = load i8, i8* %arrayidx35.i.i, align 1, !tbaa !7
  %tobool36.i.i = icmp eq i8 %40, 0
  br i1 %tobool36.i.i, label %for.inc39.i.i, label %for.inc.i.loopexit261

for.inc39.i.i:                                    ; preds = %for.body34.i.i
  %inc40.i.i = add i64 %x.0.i291.i81, 1
  %inc41.i.i = add i64 %index.1.i.i82, 1
  %cmp33.i.i = icmp ugt i64 %inc40.i.i, %cond32.i.i
  br i1 %cmp33.i.i, label %land.lhs.true41.i, label %for.body34.i.i

land.lhs.true41.i:                                ; preds = %for.inc39.i.i
  %mul.i265.i = mul i64 %37, %cond.i264.i
  %add.i266.i = add i64 %mul.i265.i, %botCol.0.i88
  br label %for.body.i272.i

for.body.i272.i:                                  ; preds = %land.lhs.true41.i, %for.inc.i274.i
  %index.0.i.i86 = phi i64 [ %add.i266.i, %land.lhs.true41.i ], [ %add14.i.i, %for.inc.i274.i ]
  %y.0.i268.i85 = phi i64 [ %cond.i264.i, %land.lhs.true41.i ], [ %inc.i273.i, %for.inc.i274.i ]
  %arrayidx.i270.i = getelementptr inbounds i8, i8* %36, i64 %index.0.i.i86
  %41 = load i8, i8* %arrayidx.i270.i, align 1, !tbaa !7
  %tobool.i271.i = icmp eq i8 %41, 0
  br i1 %tobool.i271.i, label %for.inc.i274.i, label %for.inc.i.loopexit

for.inc.i274.i:                                   ; preds = %for.body.i272.i
  %inc.i273.i = add i64 %y.0.i268.i85, 1
  %add14.i.i = add i64 %37, %index.0.i.i86
  %cmp12.i.i = icmp ugt i64 %inc.i273.i, %cond11.i.i
  br i1 %cmp12.i.i, label %land.lhs.true45.i, label %for.body.i272.i

land.lhs.true45.i:                                ; preds = %for.inc.i274.i
  %42 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx.i261.i = getelementptr inbounds i64, i64* %42, i64 %botCol.0.i88
  %43 = load i64, i64* %arrayidx.i261.i, align 8, !tbaa !1
  %cmp.i262.i = icmp eq i64 %43, 0
  br i1 %cmp.i262.i, label %land.lhs.true48.i, label %land.lhs.true.i.i

land.lhs.true.i.i:                                ; preds = %land.lhs.true45.i
  %44 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx1.i.i = getelementptr inbounds i64, i64* %44, i64 %botCol.0.i88
  %45 = load i64, i64* %arrayidx1.i.i, align 8, !tbaa !1
  %cmp2.i.i = icmp eq i64 %45, 0
  %cmp6.i.i = icmp eq i64 %43, %45
  %or.cond.i.i = or i1 %cmp2.i.i, %cmp6.i.i
  br i1 %or.cond.i.i, label %land.lhs.true48.i, label %HasVCV.exit.i

HasVCV.exit.i:                                    ; preds = %land.lhs.true.i.i
  %46 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %arrayidx8.i.i = getelementptr inbounds i64, i64* %46, i64 %43
  %47 = load i64, i64* %arrayidx8.i.i, align 8, !tbaa !1
  %arrayidx10.i.i = getelementptr inbounds i64, i64* %46, i64 %45
  %48 = load i64, i64* %arrayidx10.i.i, align 8, !tbaa !1
  %cmp11.i.i = icmp ugt i64 %47, %48
  br i1 %cmp11.i.i, label %for.inc.i, label %land.lhs.true48.i

land.lhs.true48.i:                                ; preds = %HasVCV.exit.i, %land.lhs.true45.i, %land.lhs.true.i.i
  %call49.i = tail call i32 @ExtendOK(i64 %8, i8* %39, i64 %botCol.0.i88, i64 %9, i64 %i.0188, i64 %9) #6
  %tobool50.i = icmp eq i32 %call49.i, 0
  br i1 %tobool50.i, label %for.inc.i, label %if.then51.i

if.then51.i:                                      ; preds = %land.lhs.true48.i
  %49 = load i8*, i8** @vertPlane, align 8, !tbaa !5
  %arrayidx20.i221.i102 = getelementptr inbounds i8, i8* %49, i64 %i.0188
  %50 = load i8, i8* %arrayidx20.i221.i102, align 1, !tbaa !7
  %or22.i222.i103 = or i8 %50, 8
  store i8 %or22.i222.i103, i8* %arrayidx20.i221.i102, align 1, !tbaa !7
  %cmp16.i225.i104 = icmp ugt i64 %topRow.0.i100, 1
  %51 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i225.i104, label %for.body.i228.i.preheader, label %if.else.i195.i

for.body.i228.i.preheader:                        ; preds = %if.then51.i
  %52 = and i64 %topRow.0.i100, 1
  %lcmp.mod = icmp eq i64 %52, 0
  br i1 %lcmp.mod, label %for.body.i228.i.prol.preheader, label %for.body.i228.i.prol.loopexit.unr-lcssa

for.body.i228.i.prol.preheader:                   ; preds = %for.body.i228.i.preheader
  br label %for.body.i228.i.prol

for.body.i228.i.prol:                             ; preds = %for.body.i228.i.prol.preheader
  %add19.i220.i.prol = add i64 %51, %i.0188
  %arrayidx20.i221.i.prol = getelementptr inbounds i8, i8* %49, i64 %add19.i220.i.prol
  %53 = load i8, i8* %arrayidx20.i221.i.prol, align 1, !tbaa !7
  %or22.i222.i.prol = or i8 %53, 12
  store i8 %or22.i222.i.prol, i8* %arrayidx20.i221.i.prol, align 1, !tbaa !7
  %54 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i228.i.prol.loopexit.unr-lcssa

for.body.i228.i.prol.loopexit.unr-lcssa:          ; preds = %for.body.i228.i.preheader, %for.body.i228.i.prol
  %.lcssa270.unr.ph = phi i64 [ %54, %for.body.i228.i.prol ], [ undef, %for.body.i228.i.preheader ]
  %.unr.ph = phi i64 [ %54, %for.body.i228.i.prol ], [ %51, %for.body.i228.i.preheader ]
  %y.0.i223.i105.unr.ph = phi i64 [ 2, %for.body.i228.i.prol ], [ 1, %for.body.i228.i.preheader ]
  br label %for.body.i228.i.prol.loopexit

for.body.i228.i.prol.loopexit:                    ; preds = %for.body.i228.i.prol.loopexit.unr-lcssa
  %55 = icmp eq i64 %topRow.0.i100, 2
  br i1 %55, label %if.else.i195.i.loopexit, label %for.body.i228.i.preheader.new

for.body.i228.i.preheader.new:                    ; preds = %for.body.i228.i.prol.loopexit
  br label %for.body.i228.i

for.body.i228.i:                                  ; preds = %for.body.i228.i, %for.body.i228.i.preheader.new
  %56 = phi i64 [ %.unr.ph, %for.body.i228.i.preheader.new ], [ %60, %for.body.i228.i ]
  %y.0.i223.i105 = phi i64 [ %y.0.i223.i105.unr.ph, %for.body.i228.i.preheader.new ], [ %y.0.i223.i.1, %for.body.i228.i ]
  %mul18.i227.i = mul i64 %56, %y.0.i223.i105
  %add19.i220.i = add i64 %mul18.i227.i, %i.0188
  %arrayidx20.i221.i = getelementptr inbounds i8, i8* %49, i64 %add19.i220.i
  %57 = load i8, i8* %arrayidx20.i221.i, align 1, !tbaa !7
  %or22.i222.i = or i8 %57, 12
  store i8 %or22.i222.i, i8* %arrayidx20.i221.i, align 1, !tbaa !7
  %y.0.i223.i = add nuw i64 %y.0.i223.i105, 1
  %58 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i227.i.1 = mul i64 %58, %y.0.i223.i
  %add19.i220.i.1 = add i64 %mul18.i227.i.1, %i.0188
  %arrayidx20.i221.i.1 = getelementptr inbounds i8, i8* %49, i64 %add19.i220.i.1
  %59 = load i8, i8* %arrayidx20.i221.i.1, align 1, !tbaa !7
  %or22.i222.i.1 = or i8 %59, 12
  store i8 %or22.i222.i.1, i8* %arrayidx20.i221.i.1, align 1, !tbaa !7
  %y.0.i223.i.1 = add i64 %y.0.i223.i105, 2
  %60 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %exitcond.1 = icmp eq i64 %y.0.i223.i.1, %topRow.0.i100
  br i1 %exitcond.1, label %if.else.i195.i.loopexit.unr-lcssa, label %for.body.i228.i

if.else.i195.i.loopexit.unr-lcssa:                ; preds = %for.body.i228.i
  br label %if.else.i195.i.loopexit

if.else.i195.i.loopexit:                          ; preds = %for.body.i228.i.prol.loopexit, %if.else.i195.i.loopexit.unr-lcssa
  %.lcssa270 = phi i64 [ %.lcssa270.unr.ph, %for.body.i228.i.prol.loopexit ], [ %60, %if.else.i195.i.loopexit.unr-lcssa ]
  br label %if.else.i195.i

if.else.i195.i:                                   ; preds = %if.else.i195.i.loopexit, %if.then51.i
  %.lcssa = phi i64 [ %51, %if.then51.i ], [ %.lcssa270, %if.else.i195.i.loopexit ]
  %mul30.i229.i = mul i64 %.lcssa, %topRow.0.i100
  %add31.i230.i = add i64 %mul30.i229.i, %i.0188
  %arrayidx79.i234.i = getelementptr inbounds i8, i8* %49, i64 %add31.i230.i
  %61 = load i8, i8* %arrayidx79.i234.i, align 1, !tbaa !7
  %or81.i235.i = or i8 %61, 4
  store i8 %or81.i235.i, i8* %arrayidx79.i234.i, align 1, !tbaa !7
  %62 = load i8*, i8** @viaPlane, align 8, !tbaa !5
  %63 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i212.i = mul i64 %63, %topRow.0.i100
  %add.i213.i = add i64 %mul.i212.i, %i.0188
  %arrayidx.i214.i = getelementptr inbounds i8, i8* %62, i64 %add.i213.i
  store i8 1, i8* %arrayidx.i214.i, align 1, !tbaa !7
  %64 = load i8*, i8** @horzPlane, align 8, !tbaa !5
  %65 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i173.i = mul i64 %65, %topRow.0.i100
  %add43.i192.i = add i64 %mul.i173.i, %cond20.i374.i
  %arrayidx44.i193.i = getelementptr inbounds i8, i8* %64, i64 %add43.i192.i
  %66 = load i8, i8* %arrayidx44.i193.i, align 1, !tbaa !7
  %or46.i194.i = or i8 %66, 2
  store i8 %or46.i194.i, i8* %arrayidx44.i193.i, align 1, !tbaa !7
  %x.0.i197.i107 = add i64 %cond20.i374.i, 1
  %cmp62.i199.i108 = icmp ult i64 %x.0.i197.i107, %cond32.i379.i
  %67 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i200.i109 = mul i64 %67, %topRow.0.i100
  br i1 %cmp62.i199.i108, label %for.body64.i204.i.preheader, label %DrawSegment.exit211.i

for.body64.i204.i.preheader:                      ; preds = %if.else.i195.i
  %68 = add i64 %cond32.i379.i, 3
  %69 = sub i64 %68, %cond20.i374.i
  %70 = add i64 %cond32.i379.i, -2
  %71 = sub i64 %70, %cond20.i374.i
  %xtraiter278 = and i64 %69, 3
  %lcmp.mod279 = icmp eq i64 %xtraiter278, 0
  br i1 %lcmp.mod279, label %for.body64.i204.i.prol.loopexit, label %for.body64.i204.i.prol.preheader

for.body64.i204.i.prol.preheader:                 ; preds = %for.body64.i204.i.preheader
  br label %for.body64.i204.i.prol

for.body64.i204.i.prol:                           ; preds = %for.body64.i204.i.prol, %for.body64.i204.i.prol.preheader
  %mul65.i200.i111.prol = phi i64 [ %mul65.i200.i.prol, %for.body64.i204.i.prol ], [ %mul65.i200.i109, %for.body64.i204.i.prol.preheader ]
  %x.0.i197.i110.prol = phi i64 [ %x.0.i197.i.prol, %for.body64.i204.i.prol ], [ %x.0.i197.i107, %for.body64.i204.i.prol.preheader ]
  %prol.iter = phi i64 [ %prol.iter.sub, %for.body64.i204.i.prol ], [ %xtraiter278, %for.body64.i204.i.prol.preheader ]
  %add66.i202.i.prol = add i64 %mul65.i200.i111.prol, %x.0.i197.i110.prol
  %arrayidx67.i203.i.prol = getelementptr inbounds i8, i8* %64, i64 %add66.i202.i.prol
  store i8 3, i8* %arrayidx67.i203.i.prol, align 1, !tbaa !7
  %x.0.i197.i.prol = add nuw i64 %x.0.i197.i110.prol, 1
  %72 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i200.i.prol = mul i64 %72, %topRow.0.i100
  %prol.iter.sub = add i64 %prol.iter, -1
  %prol.iter.cmp = icmp eq i64 %prol.iter.sub, 0
  br i1 %prol.iter.cmp, label %for.body64.i204.i.prol.loopexit.unr-lcssa, label %for.body64.i204.i.prol, !llvm.loop !10

for.body64.i204.i.prol.loopexit.unr-lcssa:        ; preds = %for.body64.i204.i.prol
  br label %for.body64.i204.i.prol.loopexit

for.body64.i204.i.prol.loopexit:                  ; preds = %for.body64.i204.i.preheader, %for.body64.i204.i.prol.loopexit.unr-lcssa
  %mul65.i200.i111.unr = phi i64 [ %mul65.i200.i109, %for.body64.i204.i.preheader ], [ %mul65.i200.i.prol, %for.body64.i204.i.prol.loopexit.unr-lcssa ]
  %x.0.i197.i110.unr = phi i64 [ %x.0.i197.i107, %for.body64.i204.i.preheader ], [ %x.0.i197.i.prol, %for.body64.i204.i.prol.loopexit.unr-lcssa ]
  %mul65.i200.i.lcssa271.unr = phi i64 [ undef, %for.body64.i204.i.preheader ], [ %mul65.i200.i.prol, %for.body64.i204.i.prol.loopexit.unr-lcssa ]
  %73 = icmp ult i64 %71, 3
  br i1 %73, label %DrawSegment.exit211.i.loopexit, label %for.body64.i204.i.preheader.new

for.body64.i204.i.preheader.new:                  ; preds = %for.body64.i204.i.prol.loopexit
  br label %for.body64.i204.i

for.body64.i204.i:                                ; preds = %for.body64.i204.i, %for.body64.i204.i.preheader.new
  %mul65.i200.i111 = phi i64 [ %mul65.i200.i111.unr, %for.body64.i204.i.preheader.new ], [ %mul65.i200.i.3, %for.body64.i204.i ]
  %x.0.i197.i110 = phi i64 [ %x.0.i197.i110.unr, %for.body64.i204.i.preheader.new ], [ %x.0.i197.i.3, %for.body64.i204.i ]
  %add66.i202.i = add i64 %mul65.i200.i111, %x.0.i197.i110
  %arrayidx67.i203.i = getelementptr inbounds i8, i8* %64, i64 %add66.i202.i
  store i8 3, i8* %arrayidx67.i203.i, align 1, !tbaa !7
  %x.0.i197.i = add nuw i64 %x.0.i197.i110, 1
  %74 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i200.i = mul i64 %74, %topRow.0.i100
  %add66.i202.i.1 = add i64 %mul65.i200.i, %x.0.i197.i
  %arrayidx67.i203.i.1 = getelementptr inbounds i8, i8* %64, i64 %add66.i202.i.1
  store i8 3, i8* %arrayidx67.i203.i.1, align 1, !tbaa !7
  %x.0.i197.i.1 = add i64 %x.0.i197.i110, 2
  %75 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i200.i.1 = mul i64 %75, %topRow.0.i100
  %add66.i202.i.2 = add i64 %mul65.i200.i.1, %x.0.i197.i.1
  %arrayidx67.i203.i.2 = getelementptr inbounds i8, i8* %64, i64 %add66.i202.i.2
  store i8 3, i8* %arrayidx67.i203.i.2, align 1, !tbaa !7
  %x.0.i197.i.2 = add i64 %x.0.i197.i110, 3
  %76 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i200.i.2 = mul i64 %76, %topRow.0.i100
  %add66.i202.i.3 = add i64 %mul65.i200.i.2, %x.0.i197.i.2
  %arrayidx67.i203.i.3 = getelementptr inbounds i8, i8* %64, i64 %add66.i202.i.3
  store i8 3, i8* %arrayidx67.i203.i.3, align 1, !tbaa !7
  %x.0.i197.i.3 = add i64 %x.0.i197.i110, 4
  %cmp62.i199.i.3 = icmp ult i64 %x.0.i197.i.3, %cond32.i379.i
  %77 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i200.i.3 = mul i64 %77, %topRow.0.i100
  br i1 %cmp62.i199.i.3, label %for.body64.i204.i, label %DrawSegment.exit211.i.loopexit.unr-lcssa

DrawSegment.exit211.i.loopexit.unr-lcssa:         ; preds = %for.body64.i204.i
  br label %DrawSegment.exit211.i.loopexit

DrawSegment.exit211.i.loopexit:                   ; preds = %for.body64.i204.i.prol.loopexit, %DrawSegment.exit211.i.loopexit.unr-lcssa
  %mul65.i200.i.lcssa271 = phi i64 [ %mul65.i200.i.lcssa271.unr, %for.body64.i204.i.prol.loopexit ], [ %mul65.i200.i.3, %DrawSegment.exit211.i.loopexit.unr-lcssa ]
  br label %DrawSegment.exit211.i

DrawSegment.exit211.i:                            ; preds = %DrawSegment.exit211.i.loopexit, %if.else.i195.i
  %mul65.i200.i.lcssa = phi i64 [ %mul65.i200.i109, %if.else.i195.i ], [ %mul65.i200.i.lcssa271, %DrawSegment.exit211.i.loopexit ]
  %add78.i205.i = add i64 %mul65.i200.i.lcssa, %cond32.i379.i
  %arrayidx79.i209.i.phi.trans.insert = getelementptr inbounds i8, i8* %64, i64 %add78.i205.i
  %.pre222 = load i8, i8* %arrayidx79.i209.i.phi.trans.insert, align 1, !tbaa !7
  %arrayidx79.i209.i = getelementptr inbounds i8, i8* %64, i64 %add78.i205.i
  %or81.i210.i = or i8 %.pre222, 1
  store i8 %or81.i210.i, i8* %arrayidx79.i209.i, align 1, !tbaa !7
  %78 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i169.i = mul i64 %78, %topRow.0.i100
  %add.i170.i = add i64 %mul.i169.i, %topCol.0.i91
  %arrayidx.i171.i = getelementptr inbounds i8, i8* %62, i64 %add.i170.i
  store i8 1, i8* %arrayidx.i171.i, align 1, !tbaa !7
  %cmp1.i145.i = icmp ult i64 %topRow.0.i100, %6
  %cond.i146.i = select i1 %cmp1.i145.i, i64 %topRow.0.i100, i64 %6
  %79 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i147.i = mul i64 %79, %cond.i146.i
  %add19.i152.i121 = add i64 %mul.i147.i, %topCol.0.i91
  %arrayidx20.i153.i122 = getelementptr inbounds i8, i8* %49, i64 %add19.i152.i121
  %80 = load i8, i8* %arrayidx20.i153.i122, align 1, !tbaa !7
  %or22.i154.i123 = or i8 %80, 8
  store i8 %or22.i154.i123, i8* %arrayidx20.i153.i122, align 1, !tbaa !7
  %y.0.i155.i124 = add i64 %cond.i146.i, 1
  %cond15.i156.i = select i1 %cmp1.i145.i, i64 %6, i64 %topRow.0.i100
  %cmp16.i157.i125 = icmp ult i64 %y.0.i155.i124, %cond15.i156.i
  %81 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i157.i125, label %for.body.i160.i.preheader, label %if.else.i125.i

for.body.i160.i.preheader:                        ; preds = %DrawSegment.exit211.i
  br label %for.body.i160.i

for.body.i160.i:                                  ; preds = %for.body.i160.i.preheader, %for.body.i160.i
  %82 = phi i64 [ %84, %for.body.i160.i ], [ %81, %for.body.i160.i.preheader ]
  %y.0.i155.i126 = phi i64 [ %y.0.i155.i, %for.body.i160.i ], [ %y.0.i155.i124, %for.body.i160.i.preheader ]
  %mul18.i159.i = mul i64 %82, %y.0.i155.i126
  %add19.i152.i = add i64 %mul18.i159.i, %topCol.0.i91
  %arrayidx20.i153.i = getelementptr inbounds i8, i8* %49, i64 %add19.i152.i
  %83 = load i8, i8* %arrayidx20.i153.i, align 1, !tbaa !7
  %or22.i154.i = or i8 %83, 12
  store i8 %or22.i154.i, i8* %arrayidx20.i153.i, align 1, !tbaa !7
  %y.0.i155.i = add nuw i64 %y.0.i155.i126, 1
  %cmp16.i157.i = icmp ult i64 %y.0.i155.i, %cond15.i156.i
  %84 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i157.i, label %for.body.i160.i, label %if.else.i125.i.loopexit

if.else.i125.i.loopexit:                          ; preds = %for.body.i160.i
  br label %if.else.i125.i

if.else.i125.i:                                   ; preds = %if.else.i125.i.loopexit, %DrawSegment.exit211.i
  %.lcssa50 = phi i64 [ %81, %DrawSegment.exit211.i ], [ %84, %if.else.i125.i.loopexit ]
  %mul30.i161.i = mul i64 %.lcssa50, %cond15.i156.i
  %add31.i162.i = add i64 %mul30.i161.i, %topCol.0.i91
  %arrayidx79.i166.i = getelementptr inbounds i8, i8* %49, i64 %add31.i162.i
  %85 = load i8, i8* %arrayidx79.i166.i, align 1, !tbaa !7
  %or81.i167.i = or i8 %85, 4
  store i8 %or81.i167.i, i8* %arrayidx79.i166.i, align 1, !tbaa !7
  %86 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i142.i = mul i64 %86, %6
  %add.i143.i = add i64 %mul.i142.i, %topCol.0.i91
  %arrayidx.i144.i = getelementptr inbounds i8, i8* %62, i64 %add.i143.i
  store i8 1, i8* %arrayidx.i144.i, align 1, !tbaa !7
  %87 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i103.i = mul i64 %87, %6
  %cmp37.i120.i = icmp ult i64 %topCol.0.i91, %i.0188
  %cond42.i121.i = select i1 %cmp37.i120.i, i64 %topCol.0.i91, i64 %i.0188
  %add43.i122.i = add i64 %mul.i103.i, %cond42.i121.i
  %arrayidx44.i123.i = getelementptr inbounds i8, i8* %64, i64 %add43.i122.i
  %88 = load i8, i8* %arrayidx44.i123.i, align 1, !tbaa !7
  %or46.i124.i = or i8 %88, 2
  store i8 %or46.i124.i, i8* %arrayidx44.i123.i, align 1, !tbaa !7
  %x.0.i127.i129 = add i64 %cond42.i121.i, 1
  %cond61.i128.i = select i1 %cmp37.i120.i, i64 %i.0188, i64 %topCol.0.i91
  %cmp62.i129.i130 = icmp ult i64 %x.0.i127.i129, %cond61.i128.i
  %89 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i130.i131 = mul i64 %89, %6
  br i1 %cmp62.i129.i130, label %for.body64.i134.i.preheader, label %DrawSegment.exit141.i

for.body64.i134.i.preheader:                      ; preds = %if.else.i125.i
  br label %for.body64.i134.i

for.body64.i134.i:                                ; preds = %for.body64.i134.i.preheader, %for.body64.i134.i
  %mul65.i130.i133 = phi i64 [ %mul65.i130.i, %for.body64.i134.i ], [ %mul65.i130.i131, %for.body64.i134.i.preheader ]
  %x.0.i127.i132 = phi i64 [ %x.0.i127.i, %for.body64.i134.i ], [ %x.0.i127.i129, %for.body64.i134.i.preheader ]
  %add66.i132.i = add i64 %mul65.i130.i133, %x.0.i127.i132
  %arrayidx67.i133.i = getelementptr inbounds i8, i8* %64, i64 %add66.i132.i
  store i8 3, i8* %arrayidx67.i133.i, align 1, !tbaa !7
  %x.0.i127.i = add nuw i64 %x.0.i127.i132, 1
  %cmp62.i129.i = icmp ult i64 %x.0.i127.i, %cond61.i128.i
  %90 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i130.i = mul i64 %90, %6
  br i1 %cmp62.i129.i, label %for.body64.i134.i, label %DrawSegment.exit141.i.loopexit

DrawSegment.exit141.i.loopexit:                   ; preds = %for.body64.i134.i
  br label %DrawSegment.exit141.i

DrawSegment.exit141.i:                            ; preds = %DrawSegment.exit141.i.loopexit, %if.else.i125.i
  %mul65.i130.i.lcssa = phi i64 [ %mul65.i130.i131, %if.else.i125.i ], [ %mul65.i130.i, %DrawSegment.exit141.i.loopexit ]
  %add78.i135.i = add i64 %mul65.i130.i.lcssa, %cond61.i128.i
  %arrayidx79.i139.i.phi.trans.insert = getelementptr inbounds i8, i8* %64, i64 %add78.i135.i
  %.pre223 = load i8, i8* %arrayidx79.i139.i.phi.trans.insert, align 1, !tbaa !7
  %arrayidx79.i139.i = getelementptr inbounds i8, i8* %64, i64 %add78.i135.i
  %or81.i140.i = or i8 %.pre223, 1
  store i8 %or81.i140.i, i8* %arrayidx79.i139.i, align 1, !tbaa !7
  %91 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i80.i = mul i64 %91, %cond.i295.i
  %add19.i85.i143 = add i64 %mul.i80.i, %i.0188
  %arrayidx20.i86.i144 = getelementptr inbounds i8, i8* %49, i64 %add19.i85.i143
  %92 = load i8, i8* %arrayidx20.i86.i144, align 1, !tbaa !7
  %or22.i87.i145 = or i8 %92, 8
  store i8 %or22.i87.i145, i8* %arrayidx20.i86.i144, align 1, !tbaa !7
  %y.0.i88.i146 = add i64 %cond.i295.i, 1
  %cmp16.i90.i147 = icmp ult i64 %y.0.i88.i146, %cond11.i301.i
  %93 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i90.i147, label %for.body.i93.i.preheader, label %if.else.i58.i

for.body.i93.i.preheader:                         ; preds = %DrawSegment.exit141.i
  %94 = add i64 %cond11.i301.i, 1
  %95 = sub i64 %94, %cond.i295.i
  %96 = add i64 %cond11.i301.i, -2
  %xtraiter280 = and i64 %95, 1
  %lcmp.mod281 = icmp eq i64 %xtraiter280, 0
  br i1 %lcmp.mod281, label %for.body.i93.i.prol.loopexit.unr-lcssa, label %for.body.i93.i.prol.preheader

for.body.i93.i.prol.preheader:                    ; preds = %for.body.i93.i.preheader
  br label %for.body.i93.i.prol

for.body.i93.i.prol:                              ; preds = %for.body.i93.i.prol.preheader
  %mul18.i92.i.prol = mul i64 %93, %y.0.i88.i146
  %add19.i85.i.prol = add i64 %mul18.i92.i.prol, %i.0188
  %arrayidx20.i86.i.prol = getelementptr inbounds i8, i8* %49, i64 %add19.i85.i.prol
  %97 = load i8, i8* %arrayidx20.i86.i.prol, align 1, !tbaa !7
  %or22.i87.i.prol = or i8 %97, 12
  store i8 %or22.i87.i.prol, i8* %arrayidx20.i86.i.prol, align 1, !tbaa !7
  %y.0.i88.i.prol = add i64 %cond.i295.i, 2
  %98 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br label %for.body.i93.i.prol.loopexit.unr-lcssa

for.body.i93.i.prol.loopexit.unr-lcssa:           ; preds = %for.body.i93.i.preheader, %for.body.i93.i.prol
  %.lcssa274.unr.ph = phi i64 [ %98, %for.body.i93.i.prol ], [ undef, %for.body.i93.i.preheader ]
  %.unr282.ph = phi i64 [ %98, %for.body.i93.i.prol ], [ %93, %for.body.i93.i.preheader ]
  %y.0.i88.i148.unr.ph = phi i64 [ %y.0.i88.i.prol, %for.body.i93.i.prol ], [ %y.0.i88.i146, %for.body.i93.i.preheader ]
  br label %for.body.i93.i.prol.loopexit

for.body.i93.i.prol.loopexit:                     ; preds = %for.body.i93.i.prol.loopexit.unr-lcssa
  %99 = icmp eq i64 %96, %cond.i295.i
  br i1 %99, label %if.else.i58.i.loopexit, label %for.body.i93.i.preheader.new

for.body.i93.i.preheader.new:                     ; preds = %for.body.i93.i.prol.loopexit
  br label %for.body.i93.i

for.body.i93.i:                                   ; preds = %for.body.i93.i, %for.body.i93.i.preheader.new
  %100 = phi i64 [ %.unr282.ph, %for.body.i93.i.preheader.new ], [ %104, %for.body.i93.i ]
  %y.0.i88.i148 = phi i64 [ %y.0.i88.i148.unr.ph, %for.body.i93.i.preheader.new ], [ %y.0.i88.i.1, %for.body.i93.i ]
  %mul18.i92.i = mul i64 %100, %y.0.i88.i148
  %add19.i85.i = add i64 %mul18.i92.i, %i.0188
  %arrayidx20.i86.i = getelementptr inbounds i8, i8* %49, i64 %add19.i85.i
  %101 = load i8, i8* %arrayidx20.i86.i, align 1, !tbaa !7
  %or22.i87.i = or i8 %101, 12
  store i8 %or22.i87.i, i8* %arrayidx20.i86.i, align 1, !tbaa !7
  %y.0.i88.i = add i64 %y.0.i88.i148, 1
  %102 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul18.i92.i.1 = mul i64 %102, %y.0.i88.i
  %add19.i85.i.1 = add i64 %mul18.i92.i.1, %i.0188
  %arrayidx20.i86.i.1 = getelementptr inbounds i8, i8* %49, i64 %add19.i85.i.1
  %103 = load i8, i8* %arrayidx20.i86.i.1, align 1, !tbaa !7
  %or22.i87.i.1 = or i8 %103, 12
  store i8 %or22.i87.i.1, i8* %arrayidx20.i86.i.1, align 1, !tbaa !7
  %y.0.i88.i.1 = add i64 %y.0.i88.i148, 2
  %104 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %exitcond220.1 = icmp eq i64 %y.0.i88.i.1, %cond11.i301.i
  br i1 %exitcond220.1, label %if.else.i58.i.loopexit.unr-lcssa, label %for.body.i93.i

if.else.i58.i.loopexit.unr-lcssa:                 ; preds = %for.body.i93.i
  br label %if.else.i58.i.loopexit

if.else.i58.i.loopexit:                           ; preds = %for.body.i93.i.prol.loopexit, %if.else.i58.i.loopexit.unr-lcssa
  %.lcssa274 = phi i64 [ %.lcssa274.unr.ph, %for.body.i93.i.prol.loopexit ], [ %104, %if.else.i58.i.loopexit.unr-lcssa ]
  br label %if.else.i58.i

if.else.i58.i:                                    ; preds = %if.else.i58.i.loopexit, %DrawSegment.exit141.i
  %.lcssa52 = phi i64 [ %93, %DrawSegment.exit141.i ], [ %.lcssa274, %if.else.i58.i.loopexit ]
  %mul30.i94.i = mul i64 %.lcssa52, %cond11.i301.i
  %add31.i95.i = add i64 %mul30.i94.i, %i.0188
  %arrayidx79.i99.i = getelementptr inbounds i8, i8* %49, i64 %add31.i95.i
  %105 = load i8, i8* %arrayidx79.i99.i, align 1, !tbaa !7
  %or81.i100.i = or i8 %105, 4
  store i8 %or81.i100.i, i8* %arrayidx79.i99.i, align 1, !tbaa !7
  %106 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i75.i = mul i64 %106, %botRow.0.i94
  %add.i76.i = add i64 %mul.i75.i, %i.0188
  %arrayidx.i77.i = getelementptr inbounds i8, i8* %62, i64 %add.i76.i
  store i8 1, i8* %arrayidx.i77.i, align 1, !tbaa !7
  %107 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i36.i = mul i64 %107, %botRow.0.i94
  %cmp37.i53.i = icmp ult i64 %i.0188, %botCol.0.i88
  %cond42.i54.i = select i1 %cmp37.i53.i, i64 %i.0188, i64 %botCol.0.i88
  %add43.i55.i = add i64 %mul.i36.i, %cond42.i54.i
  %arrayidx44.i56.i = getelementptr inbounds i8, i8* %64, i64 %add43.i55.i
  %108 = load i8, i8* %arrayidx44.i56.i, align 1, !tbaa !7
  %or46.i57.i = or i8 %108, 2
  store i8 %or46.i57.i, i8* %arrayidx44.i56.i, align 1, !tbaa !7
  %x.0.i60.i150 = add i64 %cond42.i54.i, 1
  %cond61.i61.i = select i1 %cmp37.i53.i, i64 %botCol.0.i88, i64 %i.0188
  %cmp62.i62.i151 = icmp ult i64 %x.0.i60.i150, %cond61.i61.i
  %109 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i63.i152 = mul i64 %109, %botRow.0.i94
  br i1 %cmp62.i62.i151, label %for.body64.i67.i.preheader, label %DrawSegment.exit74.i

for.body64.i67.i.preheader:                       ; preds = %if.else.i58.i
  br label %for.body64.i67.i

for.body64.i67.i:                                 ; preds = %for.body64.i67.i.preheader, %for.body64.i67.i
  %mul65.i63.i154 = phi i64 [ %mul65.i63.i, %for.body64.i67.i ], [ %mul65.i63.i152, %for.body64.i67.i.preheader ]
  %x.0.i60.i153 = phi i64 [ %x.0.i60.i, %for.body64.i67.i ], [ %x.0.i60.i150, %for.body64.i67.i.preheader ]
  %add66.i65.i = add i64 %mul65.i63.i154, %x.0.i60.i153
  %arrayidx67.i66.i = getelementptr inbounds i8, i8* %64, i64 %add66.i65.i
  store i8 3, i8* %arrayidx67.i66.i, align 1, !tbaa !7
  %x.0.i60.i = add nuw i64 %x.0.i60.i153, 1
  %cmp62.i62.i = icmp ult i64 %x.0.i60.i, %cond61.i61.i
  %110 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i63.i = mul i64 %110, %botRow.0.i94
  br i1 %cmp62.i62.i, label %for.body64.i67.i, label %DrawSegment.exit74.i.loopexit

DrawSegment.exit74.i.loopexit:                    ; preds = %for.body64.i67.i
  br label %DrawSegment.exit74.i

DrawSegment.exit74.i:                             ; preds = %DrawSegment.exit74.i.loopexit, %if.else.i58.i
  %mul65.i63.i.lcssa = phi i64 [ %mul65.i63.i152, %if.else.i58.i ], [ %mul65.i63.i, %DrawSegment.exit74.i.loopexit ]
  %add78.i68.i = add i64 %mul65.i63.i.lcssa, %cond61.i61.i
  %arrayidx79.i72.i.phi.trans.insert = getelementptr inbounds i8, i8* %64, i64 %add78.i68.i
  %.pre224 = load i8, i8* %arrayidx79.i72.i.phi.trans.insert, align 1, !tbaa !7
  %arrayidx79.i72.i = getelementptr inbounds i8, i8* %64, i64 %add78.i68.i
  %or81.i73.i = or i8 %.pre224, 1
  store i8 %or81.i73.i, i8* %arrayidx79.i72.i, align 1, !tbaa !7
  %111 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i32.i = mul i64 %111, %botRow.0.i94
  %add.i33.i = add i64 %mul.i32.i, %botCol.0.i88
  %arrayidx.i34.i = getelementptr inbounds i8, i8* %62, i64 %add.i33.i
  store i8 1, i8* %arrayidx.i34.i, align 1, !tbaa !7
  %cmp1.i.i = icmp ult i64 %botRow.0.i94, %9
  %cond.i.i = select i1 %cmp1.i.i, i64 %botRow.0.i94, i64 %9
  %112 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i11.i = mul i64 %112, %cond.i.i
  %add19.i16.i164 = add i64 %mul.i11.i, %botCol.0.i88
  %arrayidx20.i17.i165 = getelementptr inbounds i8, i8* %49, i64 %add19.i16.i164
  %113 = load i8, i8* %arrayidx20.i17.i165, align 1, !tbaa !7
  %or22.i18.i166 = or i8 %113, 8
  store i8 %or22.i18.i166, i8* %arrayidx20.i17.i165, align 1, !tbaa !7
  %y.0.i19.i167 = add i64 %cond.i.i, 1
  %cond15.i.i = select i1 %cmp1.i.i, i64 %9, i64 %botRow.0.i94
  %cmp16.i20.i168 = icmp ult i64 %y.0.i19.i167, %cond15.i.i
  %114 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i20.i168, label %for.body.i23.i.preheader, label %if.else.i.i

for.body.i23.i.preheader:                         ; preds = %DrawSegment.exit74.i
  br label %for.body.i23.i

for.body.i23.i:                                   ; preds = %for.body.i23.i.preheader, %for.body.i23.i
  %115 = phi i64 [ %117, %for.body.i23.i ], [ %114, %for.body.i23.i.preheader ]
  %y.0.i19.i169 = phi i64 [ %y.0.i19.i, %for.body.i23.i ], [ %y.0.i19.i167, %for.body.i23.i.preheader ]
  %mul18.i22.i = mul i64 %115, %y.0.i19.i169
  %add19.i16.i = add i64 %mul18.i22.i, %botCol.0.i88
  %arrayidx20.i17.i = getelementptr inbounds i8, i8* %49, i64 %add19.i16.i
  %116 = load i8, i8* %arrayidx20.i17.i, align 1, !tbaa !7
  %or22.i18.i = or i8 %116, 12
  store i8 %or22.i18.i, i8* %arrayidx20.i17.i, align 1, !tbaa !7
  %y.0.i19.i = add nuw i64 %y.0.i19.i169, 1
  %cmp16.i20.i = icmp ult i64 %y.0.i19.i, %cond15.i.i
  %117 = load i64, i64* @channelColumns, align 8, !tbaa !1
  br i1 %cmp16.i20.i, label %for.body.i23.i, label %if.else.i.i.loopexit

if.else.i.i.loopexit:                             ; preds = %for.body.i23.i
  br label %if.else.i.i

if.else.i.i:                                      ; preds = %if.else.i.i.loopexit, %DrawSegment.exit74.i
  %.lcssa54 = phi i64 [ %114, %DrawSegment.exit74.i ], [ %117, %if.else.i.i.loopexit ]
  %mul30.i24.i = mul i64 %.lcssa54, %cond15.i.i
  %add31.i25.i = add i64 %mul30.i24.i, %botCol.0.i88
  %arrayidx79.i29.i = getelementptr inbounds i8, i8* %49, i64 %add31.i25.i
  %118 = load i8, i8* %arrayidx79.i29.i, align 1, !tbaa !7
  %or81.i30.i = or i8 %118, 4
  store i8 %or81.i30.i, i8* %arrayidx79.i29.i, align 1, !tbaa !7
  %119 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i8.i = mul i64 %119, %9
  %add.i9.i = add i64 %mul.i8.i, %botCol.0.i88
  %arrayidx.i10.i = getelementptr inbounds i8, i8* %62, i64 %add.i9.i
  store i8 1, i8* %arrayidx.i10.i, align 1, !tbaa !7
  %120 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul.i4.i = mul i64 %120, %9
  %cmp37.i.i = icmp ult i64 %botCol.0.i88, %i.0188
  %cond42.i.i = select i1 %cmp37.i.i, i64 %botCol.0.i88, i64 %i.0188
  %add43.i.i = add i64 %mul.i4.i, %cond42.i.i
  %arrayidx44.i.i = getelementptr inbounds i8, i8* %64, i64 %add43.i.i
  %121 = load i8, i8* %arrayidx44.i.i, align 1, !tbaa !7
  %or46.i.i = or i8 %121, 2
  store i8 %or46.i.i, i8* %arrayidx44.i.i, align 1, !tbaa !7
  %x.0.i.i172 = add i64 %cond42.i.i, 1
  %cond61.i.i = select i1 %cmp37.i.i, i64 %i.0188, i64 %botCol.0.i88
  %cmp62.i.i173 = icmp ult i64 %x.0.i.i172, %cond61.i.i
  %122 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i.i174 = mul i64 %122, %9
  br i1 %cmp62.i.i173, label %for.body64.i.i.preheader, label %if.then8

for.body64.i.i.preheader:                         ; preds = %if.else.i.i
  br label %for.body64.i.i

for.body64.i.i:                                   ; preds = %for.body64.i.i.preheader, %for.body64.i.i
  %mul65.i.i176 = phi i64 [ %mul65.i.i, %for.body64.i.i ], [ %mul65.i.i174, %for.body64.i.i.preheader ]
  %x.0.i.i175 = phi i64 [ %x.0.i.i, %for.body64.i.i ], [ %x.0.i.i172, %for.body64.i.i.preheader ]
  %add66.i.i = add i64 %mul65.i.i176, %x.0.i.i175
  %arrayidx67.i.i = getelementptr inbounds i8, i8* %64, i64 %add66.i.i
  store i8 3, i8* %arrayidx67.i.i, align 1, !tbaa !7
  %x.0.i.i = add nuw i64 %x.0.i.i175, 1
  %cmp62.i.i = icmp ult i64 %x.0.i.i, %cond61.i.i
  %123 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %mul65.i.i = mul i64 %123, %9
  br i1 %cmp62.i.i, label %for.body64.i.i, label %if.then8.loopexit

for.inc.i.loopexit:                               ; preds = %for.body.i272.i
  br label %for.inc.i

for.inc.i.loopexit261:                            ; preds = %for.body34.i.i
  br label %for.inc.i

for.inc.i.loopexit262:                            ; preds = %for.body.i306.i
  br label %for.inc.i

for.inc.i.loopexit263:                            ; preds = %for.body.i352.i
  br label %for.inc.i

for.inc.i.loopexit264:                            ; preds = %for.body34.i384.i
  br label %for.inc.i

for.inc.i.loopexit265:                            ; preds = %for.body.i321.i
  br label %for.inc.i

for.inc.i:                                        ; preds = %for.inc.i.loopexit265, %for.inc.i.loopexit264, %for.inc.i.loopexit263, %for.inc.i.loopexit262, %for.inc.i.loopexit261, %for.inc.i.loopexit, %HasVCV.exit.i, %HasVCV.exit339.i, %land.lhs.true48.i, %land.lhs.true32.i, %for.body14.i
  %inc.i = add i64 %botCol.0.i88, 1
  %cmp13.i = icmp ugt i64 %inc.i, %sub.i259.i
  br i1 %cmp13.i, label %for.inc53.i.loopexit, label %for.body14.i

for.inc53.i.loopexit:                             ; preds = %for.inc.i
  br label %for.inc53.i

for.inc53.i:                                      ; preds = %for.inc53.i.loopexit, %for.cond12.i.preheader
  %inc54.i = add i64 %topCol.0.i91, 1
  %cmp10.i = icmp ugt i64 %inc54.i, %sub.i.i
  br i1 %cmp10.i, label %for.inc56.i.loopexit, label %for.cond12.i.preheader

for.inc56.i.loopexit:                             ; preds = %for.inc53.i
  br label %for.inc56.i

for.inc56.i:                                      ; preds = %for.inc56.i.loopexit, %FindFreeHorzSeg.exit260.i
  %inc57.i = add i64 %botRow.0.i94, 1
  %cmp4.i = icmp ult i64 %inc57.i, %6
  br i1 %cmp4.i, label %for.cond.i239.i.preheader, label %for.cond.i.backedge.loopexit

if.then8.loopexit:                                ; preds = %for.body64.i.i
  br label %if.then8

if.then8:                                         ; preds = %if.then8.loopexit, %if.else.i.i
  %mul65.i.i.lcssa = phi i64 [ %mul65.i.i174, %if.else.i.i ], [ %mul65.i.i, %if.then8.loopexit ]
  %add78.i.i = add i64 %mul65.i.i.lcssa, %cond61.i.i
  %arrayidx79.i.i.phi.trans.insert = getelementptr inbounds i8, i8* %64, i64 %add78.i.i
  %.pre225 = load i8, i8* %arrayidx79.i.i.phi.trans.insert, align 1, !tbaa !7
  %arrayidx79.i.i = getelementptr inbounds i8, i8* %64, i64 %add78.i.i
  %or81.i.i = or i8 %.pre225, 1
  store i8 %or81.i.i, i8* %arrayidx79.i.i, align 1, !tbaa !7
  %124 = load i8*, i8** @mazeRoute, align 8, !tbaa !5
  %arrayidx9 = getelementptr inbounds i8, i8* %124, i64 %i.0188
  store i8 0, i8* %arrayidx9, align 1, !tbaa !7
  %125 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx10 = getelementptr inbounds i64, i64* %125, i64 %i.0188
  %126 = load i64, i64* %arrayidx10, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %126)
  %127 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx11 = getelementptr inbounds i64, i64* %127, i64 %i.0188
  %128 = load i64, i64* %arrayidx11, align 8, !tbaa !1
  tail call fastcc void @CleanNet(i64 %128)
  br label %for.inc

if.else.loopexit:                                 ; preds = %for.cond.i.backedge
  br label %if.else

if.else:                                          ; preds = %if.else.loopexit, %if.then
  %inc = add nsw i32 %numLeft.0187, 1
  br label %for.inc

for.inc:                                          ; preds = %for.body, %if.else, %if.then8
  %numLeft.1 = phi i32 [ %numLeft.0187, %if.then8 ], [ %inc, %if.else ], [ %numLeft.0187, %for.body ]
  %inc13 = add i64 %i.0188, 1
  %129 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc13, %129
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.inc
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %numLeft.0.lcssa = phi i32 [ 0, %entry ], [ %numLeft.1, %for.end.loopexit ]
  ret i32 %numLeft.0.lcssa
}

; Function Attrs: nounwind
declare i64 @fwrite(i8* nocapture, i64, i64, %struct._IO_FILE* nocapture) #6

declare i32 @putchar(i32)

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { noreturn nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { norecurse nounwind readonly uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #5 = { argmemonly nounwind }
attributes #6 = { nounwind }
attributes #7 = { cold }
attributes #8 = { noreturn nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"long", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
!7 = !{!3, !3, i64 0}
!8 = distinct !{!8, !9}
!9 = !{!"llvm.loop.unroll.disable"}
!10 = distinct !{!10, !9}
