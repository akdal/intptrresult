	.text
	.file	"gdevmem.bc"
	.globl	gx_device_memory_bitmap_size
	.p2align	4, 0x90
	.type	gx_device_memory_bitmap_size,@function
gx_device_memory_bitmap_size:           # @gx_device_memory_bitmap_size
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	imull	24(%rdi), %eax
	addl	$31, %eax
	sarl	$5, %eax
	shll	$2, %eax
	movl	%eax, 152(%rdi)
	movslq	28(%rdi), %rcx
	addq	$8, %rax
	imulq	%rcx, %rax
	retq
.Lfunc_end0:
	.size	gx_device_memory_bitmap_size, .Lfunc_end0-gx_device_memory_bitmap_size
	.cfi_endproc

	.globl	mem_open
	.p2align	4, 0x90
	.type	mem_open,@function
mem_open:                               # @mem_open
	.cfi_startproc
# BB#0:
	movq	160(%rdi), %rax
	movslq	28(%rdi), %r8
	movslq	152(%rdi), %rcx
	movq	%rcx, %rsi
	imulq	%r8, %rsi
	addq	%rax, %rsi
	leaq	(,%r8,8), %rdx
	movq	%rsi, 168(%rdi)
	testq	%rdx, %rdx
	je	.LBB1_8
# BB#1:                                 # %.lr.ph.preheader
	leaq	-8(,%r8,8), %r9
	movl	%r9d, %edi
	shrl	$3, %edi
	incl	%edi
	andq	$7, %rdi
	je	.LBB1_2
# BB#3:                                 # %.lr.ph.prol.preheader
	negq	%rdi
	movq	%rsi, %rdx
	.p2align	4, 0x90
.LBB1_4:                                # %.lr.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rdx)
	addq	$8, %rdx
	addq	%rcx, %rax
	incq	%rdi
	jne	.LBB1_4
	jmp	.LBB1_5
.LBB1_2:
	movq	%rsi, %rdx
.LBB1_5:                                # %.lr.ph.prol.loopexit
	cmpq	$56, %r9
	jb	.LBB1_8
# BB#6:                                 # %.lr.ph.preheader.new
	leaq	(%rsi,%r8,8), %rsi
	.p2align	4, 0x90
.LBB1_7:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rax, (%rdx)
	addq	%rcx, %rax
	movq	%rax, 8(%rdx)
	addq	%rcx, %rax
	movq	%rax, 16(%rdx)
	addq	%rcx, %rax
	movq	%rax, 24(%rdx)
	addq	%rcx, %rax
	movq	%rax, 32(%rdx)
	addq	%rcx, %rax
	movq	%rax, 40(%rdx)
	addq	%rcx, %rax
	movq	%rax, 48(%rdx)
	addq	%rcx, %rax
	movq	%rax, 56(%rdx)
	addq	$64, %rdx
	addq	%rcx, %rax
	cmpq	%rsi, %rdx
	jne	.LBB1_7
.LBB1_8:                                # %._crit_edge
	xorl	%eax, %eax
	retq
.Lfunc_end1:
	.size	mem_open, .Lfunc_end1-mem_open
	.cfi_endproc

	.globl	mem_get_initial_matrix
	.p2align	4, 0x90
	.type	mem_get_initial_matrix,@function
mem_get_initial_matrix:                 # @mem_get_initial_matrix
	.cfi_startproc
# BB#0:
	movups	136(%rdi), %xmm0
	movups	%xmm0, 80(%rsi)
	movups	120(%rdi), %xmm0
	movups	%xmm0, 64(%rsi)
	movups	56(%rdi), %xmm0
	movups	72(%rdi), %xmm1
	movups	88(%rdi), %xmm2
	movups	104(%rdi), %xmm3
	movups	%xmm3, 48(%rsi)
	movups	%xmm2, 32(%rsi)
	movups	%xmm1, 16(%rsi)
	movups	%xmm0, (%rsi)
	retq
.Lfunc_end2:
	.size	mem_get_initial_matrix, .Lfunc_end2-mem_get_initial_matrix
	.cfi_endproc

	.globl	gs_device_is_memory
	.p2align	4, 0x90
	.type	gs_device_is_memory,@function
gs_device_is_memory:                    # @gs_device_is_memory
	.cfi_startproc
# BB#0:
	movq	16(%rdi), %rcx
	cmpb	$105, (%rcx)
	jne	.LBB3_6
# BB#1:
	cmpb	$109, 1(%rcx)
	jne	.LBB3_6
# BB#2:
	cmpb	$97, 2(%rcx)
	jne	.LBB3_6
# BB#3:
	cmpb	$103, 3(%rcx)
	jne	.LBB3_6
# BB#4:
	cmpb	$101, 4(%rcx)
	jne	.LBB3_6
# BB#5:
	xorl	%eax, %eax
	cmpb	$40, 5(%rcx)
	sete	%al
	retq
.LBB3_6:
	xorl	%eax, %eax
	retq
.Lfunc_end3:
	.size	gs_device_is_memory, .Lfunc_end3-gs_device_is_memory
	.cfi_endproc

	.globl	mem_bytes_per_scan_line
	.p2align	4, 0x90
	.type	mem_bytes_per_scan_line,@function
mem_bytes_per_scan_line:                # @mem_bytes_per_scan_line
	.cfi_startproc
# BB#0:
	movl	48(%rdi), %eax
	imull	24(%rdi), %eax
	addl	$7, %eax
	sarl	$3, %eax
	retq
.Lfunc_end4:
	.size	mem_bytes_per_scan_line, .Lfunc_end4-mem_bytes_per_scan_line
	.cfi_endproc

	.globl	mem_copy_scan_lines
	.p2align	4, 0x90
	.type	mem_copy_scan_lines,@function
mem_copy_scan_lines:                    # @mem_copy_scan_lines
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movl	%esi, %r13d
	movq	%rdi, %r12
	movl	28(%r12), %ebx
	movl	48(%r12), %r15d
	imull	24(%r12), %r15d
	addl	$7, %r15d
	sarl	$3, %r15d
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%r15d
	subl	%r13d, %ebx
	cmpl	%ebx, %eax
	cmovbl	%eax, %ebx
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	movl	%r13d, %edx
	movl	%r15d, %ecx
	movl	%ebx, %r8d
	callq	*176(%r12)
	testl	%eax, %eax
	movl	%r13d, 4(%rsp)          # 4-byte Spill
	movl	%r15d, (%rsp)           # 4-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
	jns	.LBB5_3
# BB#1:                                 # %.lr.ph73.preheader
	movq	8(%rsp), %r12           # 8-byte Reload
	.p2align	4, 0x90
.LBB5_2:                                # %.lr.ph73
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbx, %r15
	shrl	%ebx
	movl	%ebx, %r14d
	imull	(%rsp), %r14d           # 4-byte Folded Reload
	movq	%r12, %rdi
	movl	%r13d, %esi
	movq	%rbp, %rdx
	movl	%r14d, %ecx
	callq	mem_copy_scan_lines
	addq	%r14, %rbp
	addl	%ebx, %r13d
	subl	%ebx, %r15d
	movq	%r15, %rbx
	xorl	%esi, %esi
	xorl	%r9d, %r9d
	movq	%r12, %rdi
	movl	%r13d, %edx
	movl	(%rsp), %ecx            # 4-byte Reload
	movl	%ebx, %r8d
	callq	*176(%r12)
	testl	%eax, %eax
	js	.LBB5_2
.LBB5_3:                                # %._crit_edge74
	movl	%r13d, %edx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movl	(%rsp), %r13d           # 4-byte Reload
	testl	%ebx, %ebx
	je	.LBB5_4
# BB#5:                                 # %.lr.ph
	movq	%rbx, %rax
	movl	%edx, %r12d
	movslq	%r12d, %rbx
	shlq	$3, %rbx
	addq	168(%rcx), %rbx
	movl	%r13d, %r14d
	leal	-1(%rax), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	movl	%eax, %r15d
	andl	$7, %r15d
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB5_6
# BB#7:                                 # %.prol.preheader
	negl	%r15d
	movl	%eax, %r13d
	.p2align	4, 0x90
.LBB5_8:                                # =>This Inner Loop Header: Depth=1
	decl	%r13d
	movq	(%rbx), %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	addq	$8, %rbx
	addq	%r14, %rbp
	incl	%r15d
	jne	.LBB5_8
	jmp	.LBB5_9
.LBB5_4:
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%edx, %eax
	jmp	.LBB5_12
.LBB5_6:
	movl	%eax, %r13d
.LBB5_9:                                # %.prol.loopexit
	cmpl	$7, (%rsp)              # 4-byte Folded Reload
	jb	.LBB5_11
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rsi
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	8(%rbx), %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	16(%rbx), %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	24(%rbx), %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	32(%rbx), %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	40(%rbx), %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	48(%rbx), %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	movq	56(%rbx), %rsi
	addq	%r14, %rbp
	movq	%rbp, %rdi
	movq	%r14, %rdx
	callq	memcpy
	addq	$64, %rbx
	addq	%r14, %rbp
	addl	$-8, %r13d
	jne	.LBB5_10
.LBB5_11:                               # %._crit_edge.loopexit
	movq	16(%rsp), %rax          # 8-byte Reload
	addl	%r12d, %eax
                                        # kill: %EAX<def> %EAX<kill> %RAX<kill>
	movl	4(%rsp), %ecx           # 4-byte Reload
.LBB5_12:                               # %._crit_edge
	subl	%ecx, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	mem_copy_scan_lines, .Lfunc_end5-mem_copy_scan_lines
	.cfi_endproc

	.globl	mem_no_fault_proc
	.p2align	4, 0x90
	.type	mem_no_fault_proc,@function
mem_no_fault_proc:                      # @mem_no_fault_proc
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	mem_no_fault_proc, .Lfunc_end6-mem_no_fault_proc
	.cfi_endproc

	.globl	mem_fill_recover
	.p2align	4, 0x90
	.type	mem_fill_recover,@function
mem_fill_recover:                       # @mem_fill_recover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%r9, %r14
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %rbx
	movl	64(%rsp), %eax
	cmpl	$-2, %eax
	je	.LBB7_3
# BB#1:
	cmpl	$-1, %eax
	jne	.LBB7_5
# BB#2:
	movl	%ecx, %r15d
	sarl	%r15d
	leal	(%r15,%rsi), %r13d
	subl	%r15d, %ecx
	movl	%edx, %ebp
	movl	%r8d, %r12d
	jmp	.LBB7_4
.LBB7_3:
	movl	%r8d, %r12d
	sarl	%r12d
	leal	(%r12,%rdx), %ebp
	subl	%r12d, %r8d
	movl	%esi, %r13d
	movl	%ecx, %r15d
.LBB7_4:
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movq	%r14, %r9
	callq	*56(%rax)
	movq	8(%rbx), %rax
	movq	56(%rax), %rax
	movq	%rbx, %rdi
	movl	%r13d, %esi
	movl	%ebp, %edx
	movl	%r15d, %ecx
	movl	%r12d, %r8d
	movq	%r14, %r9
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB7_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	mem_fill_recover, .Lfunc_end7-mem_fill_recover
	.cfi_endproc

	.globl	mem_copy_mono_recover
	.p2align	4, 0x90
	.type	mem_copy_mono_recover,@function
mem_copy_mono_recover:                  # @mem_copy_mono_recover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi32:
	.cfi_def_cfa_offset 80
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rdi, %rbp
	movl	112(%rsp), %eax
	movl	88(%rsp), %ebx
	movl	80(%rsp), %r10d
	cmpl	$-2, %eax
	je	.LBB8_3
# BB#1:
	cmpl	$-1, %eax
	jne	.LBB8_5
# BB#2:
	movl	%r10d, %eax
	sarl	%eax
	leal	(%rax,%r8), %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	subl	%eax, %r10d
	movl	%r9d, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%ebx, %r14d
	jmp	.LBB8_4
.LBB8_3:
	movl	%ebx, %r14d
	sarl	%r14d
	leal	(%r14,%r9), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	subl	%r14d, %ebx
	movl	%r8d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%r10d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB8_4:
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rsi, %r13
	movl	%edx, %r12d
	movl	%ecx, %r15d
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	104(%rsp)
.Lcfi39:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
.Lcfi40:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi41:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi42:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$32, %rsp
.Lcfi43:
	.cfi_adjust_cfa_offset -32
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%r12d, %edx
	movl	%r15d, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	pushq	104(%rsp)
.Lcfi44:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)
.Lcfi45:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi46:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi47:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$56, %rsp
.Lcfi48:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB8_6
.LBB8_5:
	addq	$24, %rsp
.LBB8_6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	mem_copy_mono_recover, .Lfunc_end8-mem_copy_mono_recover
	.cfi_endproc

	.globl	mem_copy_color_recover
	.p2align	4, 0x90
	.type	mem_copy_color_recover,@function
mem_copy_color_recover:                 # @mem_copy_color_recover
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi51:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi52:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi53:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi54:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi55:
	.cfi_def_cfa_offset 80
.Lcfi56:
	.cfi_offset %rbx, -56
.Lcfi57:
	.cfi_offset %r12, -48
.Lcfi58:
	.cfi_offset %r13, -40
.Lcfi59:
	.cfi_offset %r14, -32
.Lcfi60:
	.cfi_offset %r15, -24
.Lcfi61:
	.cfi_offset %rbp, -16
                                        # kill: %R9D<def> %R9D<kill> %R9<def>
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movq	%rdi, %rbp
	movl	96(%rsp), %eax
	movl	88(%rsp), %ebx
	movl	80(%rsp), %r10d
	cmpl	$-2, %eax
	je	.LBB9_3
# BB#1:
	cmpl	$-1, %eax
	jne	.LBB9_5
# BB#2:
	movl	%r10d, %eax
	sarl	%eax
	leal	(%rax,%r8), %edi
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	subl	%eax, %r10d
	movl	%r9d, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%ebx, %r14d
	jmp	.LBB9_4
.LBB9_3:
	movl	%ebx, %r14d
	sarl	%r14d
	leal	(%r14,%r9), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	subl	%r14d, %ebx
	movl	%r8d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%r10d, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
.LBB9_4:
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rsi, %r13
	movl	%edx, %r12d
	movl	%ecx, %r15d
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%rbx
.Lcfi62:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi63:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$16, %rsp
.Lcfi64:
	.cfi_adjust_cfa_offset -16
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%r13, %rsi
	movl	%r12d, %edx
	movl	%r15d, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	pushq	%r14
.Lcfi65:
	.cfi_adjust_cfa_offset 8
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi66:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$40, %rsp
.Lcfi67:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB9_6
.LBB9_5:
	addq	$24, %rsp
.LBB9_6:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	mem_copy_color_recover, .Lfunc_end9-mem_copy_color_recover
	.cfi_endproc

	.globl	mem_mono_fill_rectangle
	.p2align	4, 0x90
	.type	mem_mono_fill_rectangle,@function
mem_mono_fill_rectangle:                # @mem_mono_fill_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi68:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi69:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi70:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi71:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi72:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi74:
	.cfi_def_cfa_offset 112
.Lcfi75:
	.cfi_offset %rbx, -56
.Lcfi76:
	.cfi_offset %r12, -48
.Lcfi77:
	.cfi_offset %r13, -40
.Lcfi78:
	.cfi_offset %r14, -32
.Lcfi79:
	.cfi_offset %r15, -24
.Lcfi80:
	.cfi_offset %rbp, -16
	movq	%r9, %r12
	movl	%r8d, %r13d
	movl	%ecx, %r14d
	movl	%edx, %ebp
	movl	%esi, %r15d
	movq	%rdi, %rbx
	movq	176(%rbx), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	je	.LBB10_7
# BB#1:
	movl	%r15d, %esi
	sarl	$3, %esi
	leal	7(%r15,%r14), %ecx
	sarl	$3, %ecx
	subl	%esi, %ecx
	movl	$1, %r9d
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movl	%r13d, %r8d
	callq	*%rax
	testl	%eax, %eax
	js	.LBB10_2
.LBB10_7:                               # %mem_fill_recover.exit
	testl	%r14d, %r14d
	jle	.LBB10_54
# BB#8:                                 # %mem_fill_recover.exit
	testl	%r13d, %r13d
	jle	.LBB10_54
# BB#9:
	movl	$-1, %eax
	testl	%r15d, %r15d
	js	.LBB10_55
# BB#10:
	testl	%ebp, %ebp
	js	.LBB10_55
# BB#11:
	movl	24(%rbx), %ecx
	subl	%r14d, %ecx
	cmpl	%r15d, %ecx
	jl	.LBB10_55
# BB#12:
	movl	28(%rbx), %ecx
	subl	%r13d, %ecx
	cmpl	%ebp, %ecx
	jl	.LBB10_55
# BB#13:
	cmpq	$-1, %r12
	je	.LBB10_54
# BB#14:
	movq	%r12, %rcx
	movslq	%ebp, %r12
	shlq	$3, %r12
	addq	168(%rbx), %r12
	cmpq	$1, %rcx
	je	.LBB10_17
# BB#15:
	testq	%rcx, %rcx
	jne	.LBB10_55
# BB#16:
	movb	184(%rbx), %bl
	jmp	.LBB10_18
.LBB10_2:
	cmpl	$-2, %eax
	je	.LBB10_5
# BB#3:
	cmpl	$-1, %eax
	jne	.LBB10_55
# BB#4:
	movq	%r12, %r9
	movq	%r14, %rcx
	sarl	%r14d
	leal	(%r14,%r15), %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	subl	%r14d, %ecx
	movl	%ebp, %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movl	%r13d, %r12d
	jmp	.LBB10_6
.LBB10_5:
	movq	%r12, %r9
	movl	%r13d, %r12d
	sarl	%r12d
	leal	(%r12,%rbp), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	subl	%r12d, %r13d
	movl	%r15d, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movq	%r14, %rcx
.LBB10_6:
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	movl	%r15d, %esi
	movl	%ebp, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	movl	%r13d, %r8d
	movq	%r9, %rbp
	callq	*56(%rax)
	movq	8(%rbx), %rax
	movq	56(%rax), %rax
	movq	%rbx, %rdi
	movl	16(%rsp), %esi          # 4-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	%r14d, %ecx
	movl	%r12d, %r8d
	movq	%rbp, %r9
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.LBB10_17:
	movb	184(%rbx), %bl
	notb	%bl
.LBB10_18:
	movl	%r15d, %edx
	shrl	$3, %edx
	movl	%r15d, %eax
	andl	$7, %eax
	leal	(%rax,%r14), %ecx
	cmpl	$8, %ecx
	ja	.LBB10_20
# BB#19:
	movl	$255, %edi
	movl	$255, %esi
	movl	%r14d, %ecx
	shrl	%cl, %esi
	subl	%esi, %edi
	movl	%eax, %ecx
	sarl	%cl, %edi
	testb	%dil, %dil
	jne	.LBB10_43
	jmp	.LBB10_54
.LBB10_20:
	testl	%eax, %eax
	movb	%bl, 15(%rsp)           # 1-byte Spill
	je	.LBB10_33
# BB#21:
	movl	$255, %r8d
	movl	%eax, %ecx
	shrl	%cl, %r8d
	testb	%bl, %bl
	je	.LBB10_27
# BB#22:                                # %.preheader176.preheader
	leal	-1(%r13), %esi
	movl	%r13d, %edi
	andl	$3, %edi
	movl	%r13d, %eax
	movq	%r12, %rcx
	je	.LBB10_25
# BB#23:                                # %.preheader176.prol.preheader
	negl	%edi
	movl	%r13d, %eax
	movq	%r12, %rcx
.LBB10_24:                              # %.preheader176.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbp
	movzbl	(%rbp,%rdx), %ebx
	orl	%r8d, %ebx
	movb	%bl, (%rbp,%rdx)
	addq	$8, %rcx
	decl	%eax
	incl	%edi
	jne	.LBB10_24
.LBB10_25:                              # %.preheader176.prol.loopexit
	cmpl	$3, %esi
	jb	.LBB10_32
.LBB10_26:                              # %.preheader176
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	orl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	movq	8(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	orl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	movq	16(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	orl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	movq	24(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	orl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	addq	$32, %rcx
	addl	$-4, %eax
	jne	.LBB10_26
	jmp	.LBB10_32
.LBB10_27:                              # %.preheader175
	xorl	$255, %r8d
	leal	-1(%r13), %esi
	movl	%r13d, %edi
	andl	$3, %edi
	movl	%r13d, %eax
	movq	%r12, %rcx
	je	.LBB10_30
# BB#28:                                # %.prol.preheader194
	negl	%edi
	movl	%r13d, %eax
	movq	%r12, %rcx
.LBB10_29:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rbp
	movzbl	(%rbp,%rdx), %ebx
	andl	%r8d, %ebx
	movb	%bl, (%rbp,%rdx)
	addq	$8, %rcx
	decl	%eax
	incl	%edi
	jne	.LBB10_29
.LBB10_30:                              # %.prol.loopexit195
	cmpl	$3, %esi
	jb	.LBB10_32
.LBB10_31:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	andl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	movq	8(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	andl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	movq	16(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	andl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	movq	24(%rcx), %rsi
	movzbl	(%rsi,%rdx), %edi
	andl	%r8d, %edi
	movb	%dil, (%rsi,%rdx)
	addq	$32, %rcx
	addl	$-4, %eax
	jne	.LBB10_31
.LBB10_32:                              # %.loopexit
	incl	%edx
	orl	$-8, %r15d
	addl	%r14d, %r15d
	movl	%r15d, %r14d
.LBB10_33:
	movl	%r14d, %ecx
	andb	$7, %cl
	movl	$255, %edi
	shrl	%cl, %edi
	notb	%dil
	sarl	$3, %r14d
	je	.LBB10_34
# BB#35:
	movl	%edi, 36(%rsp)          # 4-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movslq	%edx, %rax
	movq	%r14, 48(%rsp)          # 8-byte Spill
	movslq	%r14d, %r15
	leal	-1(%r13), %ecx
	movl	%ecx, 32(%rsp)          # 4-byte Spill
	movl	%r13d, %ebx
	andl	$7, %ebx
	movl	%r13d, %r14d
	movq	%r12, %rbp
	movq	%rax, 16(%rsp)          # 8-byte Spill
	je	.LBB10_38
# BB#36:                                # %.prol.preheader189
	negl	%ebx
	movzbl	15(%rsp), %ecx          # 1-byte Folded Reload
	movl	%ecx, 28(%rsp)          # 4-byte Spill
	movl	%r13d, %r14d
	movq	%r12, %rbp
.LBB10_37:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	addq	%rax, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	movq	%r15, %rdx
	callq	memset
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$8, %rbp
	decl	%r14d
	incl	%ebx
	jne	.LBB10_37
.LBB10_38:                              # %.prol.loopexit190
	cmpl	$7, 32(%rsp)            # 4-byte Folded Reload
	jb	.LBB10_41
# BB#39:                                # %.new
	movzbl	15(%rsp), %ebx          # 1-byte Folded Reload
.LBB10_40:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	addq	%rax, %rdi
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	8(%rbp), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rbp), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	24(%rbp), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	32(%rbp), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	40(%rbp), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	48(%rbp), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	56(%rbp), %rdi
	addq	16(%rsp), %rdi          # 8-byte Folded Reload
	movl	%ebx, %esi
	movq	%r15, %rdx
	callq	memset
	movq	16(%rsp), %rax          # 8-byte Reload
	addq	$64, %rbp
	addl	$-8, %r14d
	jne	.LBB10_40
.LBB10_41:
	movq	48(%rsp), %rax          # 8-byte Reload
	addl	40(%rsp), %eax          # 4-byte Folded Reload
	movl	%eax, %edx
	movb	15(%rsp), %bl           # 1-byte Reload
	movl	36(%rsp), %edi          # 4-byte Reload
	testb	%dil, %dil
	jne	.LBB10_43
	jmp	.LBB10_54
.LBB10_34:
	movb	15(%rsp), %bl           # 1-byte Reload
	testb	%dil, %dil
	je	.LBB10_54
.LBB10_43:
	testb	%bl, %bl
	movslq	%edx, %rax
	je	.LBB10_49
# BB#44:                                # %.preheader173.preheader
	leal	-1(%r13), %ecx
	movl	%r13d, %edx
	andl	$3, %edx
	je	.LBB10_47
# BB#45:                                # %.preheader173.prol.preheader
	negl	%edx
	.p2align	4, 0x90
.LBB10_46:                              # %.preheader173.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rsi
	orb	%dil, (%rsi,%rax)
	addq	$8, %r12
	decl	%r13d
	incl	%edx
	jne	.LBB10_46
.LBB10_47:                              # %.preheader173.prol.loopexit
	cmpl	$3, %ecx
	jb	.LBB10_54
.LBB10_48:                              # %.preheader173
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	orb	%dil, (%rcx,%rax)
	movq	8(%r12), %rcx
	orb	%dil, (%rcx,%rax)
	movq	16(%r12), %rcx
	orb	%dil, (%rcx,%rax)
	movq	24(%r12), %rcx
	orb	%dil, (%rcx,%rax)
	addq	$32, %r12
	addl	$-4, %r13d
	jne	.LBB10_48
	jmp	.LBB10_54
.LBB10_49:                              # %.preheader
	notb	%dil
	leal	-1(%r13), %ecx
	movl	%r13d, %edx
	andl	$3, %edx
	je	.LBB10_52
# BB#50:                                # %.prol.preheader
	negl	%edx
.LBB10_51:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rsi
	andb	%dil, (%rsi,%rax)
	addq	$8, %r12
	decl	%r13d
	incl	%edx
	jne	.LBB10_51
.LBB10_52:                              # %.prol.loopexit
	cmpl	$3, %ecx
	jb	.LBB10_54
.LBB10_53:                              # =>This Inner Loop Header: Depth=1
	movq	(%r12), %rcx
	andb	%dil, (%rcx,%rax)
	movq	8(%r12), %rcx
	andb	%dil, (%rcx,%rax)
	movq	16(%r12), %rcx
	andb	%dil, (%rcx,%rax)
	movq	24(%r12), %rcx
	andb	%dil, (%rcx,%rax)
	addq	$32, %r12
	addl	$-4, %r13d
	jne	.LBB10_53
.LBB10_54:
	xorl	%eax, %eax
.LBB10_55:                              # %mem_fill_recover.exit.thread
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	mem_mono_fill_rectangle, .Lfunc_end10-mem_mono_fill_rectangle
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_0:
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	255                     # 0xff
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.byte	0                       # 0x0
	.text
	.globl	mem_mono_copy_mono
	.p2align	4, 0x90
	.type	mem_mono_copy_mono,@function
mem_mono_copy_mono:                     # @mem_mono_copy_mono
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi81:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi82:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi84:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi85:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi86:
	.cfi_def_cfa_offset 56
	subq	$248, %rsp
.Lcfi87:
	.cfi_def_cfa_offset 304
.Lcfi88:
	.cfi_offset %rbx, -56
.Lcfi89:
	.cfi_offset %r12, -48
.Lcfi90:
	.cfi_offset %r13, -40
.Lcfi91:
	.cfi_offset %r14, -32
.Lcfi92:
	.cfi_offset %r15, -24
.Lcfi93:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdi, %r15
	movq	328(%rsp), %rax
	movq	320(%rsp), %r9
	movl	312(%rsp), %ebp
	movl	304(%rsp), %ebx
	cmpl	%r9d, %eax
	jne	.LBB11_1
# BB#148:
	movq	%r15, %rdi
	movl	%r8d, %esi
	movl	%r12d, %edx
	movl	%ebx, %ecx
	movl	%ebp, %r8d
	addq	$248, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	mem_mono_fill_rectangle # TAILCALL
.LBB11_1:
	movq	%r9, 72(%rsp)           # 8-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movl	%ecx, 56(%rsp)          # 4-byte Spill
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	176(%r15), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	je	.LBB11_8
# BB#2:
	movl	%r8d, %esi
	sarl	$3, %esi
	leal	7(%r8,%rbx), %ecx
	sarl	$3, %ecx
	subl	%esi, %ecx
	movl	$1, %r9d
	movq	%r15, %rdi
	movl	%r12d, %edx
	movq	%r8, %r13
	movl	%ebp, %r8d
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbx, %r14
	callq	*%rax
	movq	%r13, %r8
	movq	24(%rsp), %rbp          # 8-byte Reload
	testl	%eax, %eax
	js	.LBB11_3
.LBB11_8:                               # %mem_copy_mono_recover.exit
	xorl	%eax, %eax
	testl	%ebx, %ebx
	jle	.LBB11_146
# BB#9:                                 # %mem_copy_mono_recover.exit
	testl	%ebp, %ebp
	jle	.LBB11_146
# BB#10:
	movl	$-1, %eax
	testl	%r8d, %r8d
	js	.LBB11_146
# BB#11:
	testl	%r12d, %r12d
	js	.LBB11_146
# BB#12:
	movl	24(%r15), %ecx
	subl	%ebx, %ecx
	cmpl	%r8d, %ecx
	jl	.LBB11_146
# BB#13:
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	movl	28(%r15), %ecx
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	subl	%ebp, %ecx
	cmpl	%r12d, %ecx
	jl	.LBB11_146
# BB#14:
	movq	%r8, %rbp
	movl	%ebp, %eax
	shrl	$3, %eax
	movq	168(%r15), %r8
	movslq	%r12d, %r11
	movq	(%r8,%r11,8), %r10
	movq	%rax, 152(%rsp)         # 8-byte Spill
	addq	%rax, %r10
	movq	40(%rsp), %rbx          # 8-byte Reload
	movl	%ebx, %eax
	sarl	$3, %eax
	andl	$7, %ebx
	movl	$8, %r9d
	andl	$7, %ebp
	movl	$8, %r12d
	subl	%ebp, %r12d
	movl	$255, %esi
	movl	$255, %edx
	movl	%ebp, %ecx
	shrl	%cl, %edx
	movq	112(%rsp), %rcx         # 8-byte Reload
	cmpl	%ecx, %r12d
	jle	.LBB11_16
# BB#15:
	movl	%edx, %esi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shrl	%cl, %esi
	subl	%esi, %edx
                                        # implicit-def: %ESI
	jmp	.LBB11_17
.LBB11_3:
	cmpl	$-2, %eax
	je	.LBB11_6
# BB#4:
	cmpl	$-1, %eax
	jne	.LBB11_146
# BB#5:
	movq	%r14, %rbx
	movl	%ebx, %eax
	sarl	%eax
	movq	%r13, %r8
	leal	(%rax,%r8), %ecx
	movl	%ecx, 64(%rsp)          # 4-byte Spill
	movq	%rax, 152(%rsp)         # 8-byte Spill
	subl	%eax, %ebx
	movl	%r12d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	24(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %eax
	movq	%rax, 88(%rsp)          # 8-byte Spill
	jmp	.LBB11_7
.LBB11_6:
	movl	%ebp, %eax
	sarl	%eax
	leal	(%rax,%r12), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rax, 88(%rsp)          # 8-byte Spill
	subl	%eax, %ebp
	movl	%r8d, %eax
	movl	%eax, 64(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
	movq	%rax, 152(%rsp)         # 8-byte Spill
.LBB11_7:
	movq	8(%r15), %r10
	movq	%r15, %rdi
	movq	80(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	56(%rsp), %r14d         # 4-byte Reload
	movl	%r14d, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movl	%r12d, %r9d
	movq	16(%rsp), %r12          # 8-byte Reload
	pushq	%r12
.Lcfi94:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi95:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi96:
	.cfi_adjust_cfa_offset 8
	pushq	%rbx
.Lcfi97:
	.cfi_adjust_cfa_offset 8
	callq	*72(%r10)
	addq	$32, %rsp
.Lcfi98:
	.cfi_adjust_cfa_offset -32
	movq	8(%r15), %rax
	movq	%r15, %rdi
	movq	%r13, %rsi
	movq	40(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r14d, %ecx
	movl	64(%rsp), %r8d          # 4-byte Reload
	movl	12(%rsp), %r9d          # 4-byte Reload
	pushq	%r12
.Lcfi99:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi100:
	.cfi_adjust_cfa_offset 8
	pushq	104(%rsp)               # 8-byte Folded Reload
.Lcfi101:
	.cfi_adjust_cfa_offset 8
	pushq	176(%rsp)               # 8-byte Folded Reload
.Lcfi102:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$280, %rsp              # imm = 0x118
.Lcfi103:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB11_147
.LBB11_16:
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	subl	%r12d, %ecx
	andb	$7, %cl
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shrl	%cl, %esi
	xorl	$255, %esi
.LBB11_17:
	movq	16(%rsp), %r13          # 8-byte Reload
	movq	72(%rsp), %rdi          # 8-byte Reload
	movl	%edx, 72(%rsp)          # 4-byte Spill
	movslq	%eax, %rcx
	subl	%ebx, %r9d
	cmpl	$0, 184(%r15)
	movl	%esi, 88(%rsp)          # 4-byte Spill
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rbp, 160(%rsp)         # 8-byte Spill
	je	.LBB11_19
# BB#18:
	xorl	%eax, %eax
	cmpl	$-1, %edi
	setne	%al
	xorq	%rax, %rdi
	xorl	%eax, %eax
	cmpl	$-1, %r13d
	setne	%al
	xorq	%rax, %r13
.LBB11_19:
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%r8, %rbp
	leaq	(%r8,%r11,8), %r8
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	leaq	(%rax,%rcx), %r14
	cmpl	$1, %edi
	sete	%al
	testl	%r13d, %r13d
	setne	%sil
	sete	%cl
	movl	%ecx, %edx
	orb	%al, %dl
	movzbl	%dl, %r15d
	negl	%r15d
	testl	%edi, %edi
	setne	%dl
	sete	%bl
	orb	%cl, %bl
	movb	%bl, 10(%rsp)           # 1-byte Spill
	andb	%sil, %dl
	movzbl	%dl, %ecx
	negl	%ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	cmpl	$1, %r13d
	sete	%cl
	orb	%al, %cl
	movl	%r9d, 104(%rsp)         # 4-byte Spill
	subl	%r12d, %r9d
	movl	%ecx, %r13d
	movb	%cl, 11(%rsp)           # 1-byte Spill
	jne	.LBB11_71
# BB#20:
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	movq	%r8, %rsi
	movl	12(%rsp), %r8d          # 4-byte Reload
	jle	.LBB11_146
# BB#21:                                # %.lr.ph266
	movq	112(%rsp), %r12         # 8-byte Reload
	movl	%r12d, %r9d
	subl	104(%rsp), %r9d         # 4-byte Folded Reload
	movl	72(%rsp), %eax          # 4-byte Reload
	xorl	$255, %eax
	orl	%r8d, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	88(%rsp), %eax          # 4-byte Reload
	xorl	$255, %eax
	orl	%r8d, %eax
	movl	%eax, 208(%rsp)         # 4-byte Spill
	movl	%r9d, %ecx
	addl	$-8, %ecx
	movslq	56(%rsp), %rbx          # 4-byte Folded Reload
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	js	.LBB11_22
# BB#26:                                # %.lr.ph266.split.us.preheader
	movq	24(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	160(%rsp), %rax         # 8-byte Reload
	leal	-16(%rax,%r12), %edx
	movl	%ecx, 52(%rsp)          # 4-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	shrl	$3, %ecx
	leal	(,%rcx,8), %eax
	subl	%eax, %edx
	movl	%edx, 104(%rsp)         # 4-byte Spill
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx), %r9
	addq	80(%rsp), %r9           # 8-byte Folded Reload
	leaq	1(%rcx), %r11
	leaq	2(%rcx), %rdx
	movq	%rdx, 224(%rsp)         # 8-byte Spill
	movq	%rcx, 160(%rsp)         # 8-byte Spill
	leaq	2(%rax,%rcx), %rcx
	movq	%rcx, 200(%rsp)         # 8-byte Spill
	incq	%rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rax          # 8-byte Reload
	leal	-16(%rax,%r12), %ecx
	movq	%r11, 40(%rsp)          # 8-byte Spill
                                        # kill: %R11D<def> %R11D<kill> %R11<kill> %R11<def>
	andl	$1073741808, %r11d      # imm = 0x3FFFFFF0
	leaq	-16(%r11), %rax
	shrq	$4, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	leal	1(%rax), %edx
	movq	%r11, 112(%rsp)         # 8-byte Spill
	leal	(,%r11,8), %eax
	subl	%eax, %ecx
	movl	%ecx, 144(%rsp)         # 4-byte Spill
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm12       # xmm12 = xmm0[0,0,0,0]
	andl	$1, %edx
	movq	%rdx, 96(%rsp)          # 8-byte Spill
	movd	%r8d, %xmm0
	pshufd	$0, %xmm0, %xmm8        # xmm8 = xmm0[0,0,0,0]
	xorl	%r11d, %r11d
	movdqa	.LCPI11_0(%rip), %xmm9  # xmm9 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pxor	%xmm10, %xmm10
	xorl	%r12d, %r12d
.LBB11_27:                              # %.lr.ph266.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_44 Depth 2
                                        #     Child Loop BB11_33 Depth 2
                                        #     Child Loop BB11_63 Depth 2
                                        #     Child Loop BB11_52 Depth 2
	movq	%rbx, %rax
	movq	%rax, %rbp
	imulq	%r12, %rbp
	movzbl	(%r14), %eax
	xorl	%r15d, %eax
	movl	16(%rsp), %ecx          # 4-byte Reload
	orl	%eax, %ecx
	movzbl	(%r10), %edx
	andl	%ecx, %edx
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rbp), %r8
	movq	80(%rsp), %rcx          # 8-byte Reload
	addq	%rcx, %r8
	addq	200(%rsp), %rbp         # 8-byte Folded Reload
	addq	%rcx, %rbp
	andl	72(%rsp), %eax          # 4-byte Folded Reload
	testb	%r13b, %r13b
	cmovel	%r11d, %eax
	orl	%edx, %eax
	movb	%al, (%r10)
	testb	%r13b, %r13b
	je	.LBB11_28
# BB#47:                                # %.lr.ph.split.us.us.preheader
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	$15, 40(%rsp)           # 8-byte Folded Reload
	movl	52(%rsp), %edi          # 4-byte Reload
	movl	%edi, %eax
	movq	%r14, %rcx
	movq	%r10, %rdx
	movb	10(%rsp), %bl           # 1-byte Reload
	jbe	.LBB11_48
# BB#53:                                # %min.iters.checked500
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	movl	%edi, %eax
	movq	%r14, %rcx
	movq	%r10, %rdx
	je	.LBB11_48
# BB#54:                                # %vector.memcheck515
                                        #   in Loop: Header=BB11_27 Depth=1
	leaq	1(%r10), %rax
	cmpq	%rbp, %rax
	jae	.LBB11_56
# BB#55:                                # %vector.memcheck515
                                        #   in Loop: Header=BB11_27 Depth=1
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%r10,%rax), %rax
	cmpq	%rax, %r8
	movl	%edi, %eax
	movq	%r14, %rcx
	movq	%r10, %rdx
	jb	.LBB11_48
.LBB11_56:                              # %vector.ph516
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	jne	.LBB11_58
# BB#57:                                #   in Loop: Header=BB11_27 Depth=1
	movq	%rsi, %r8
	xorl	%eax, %eax
	movq	112(%rsp), %rcx         # 8-byte Reload
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	jne	.LBB11_63
	jmp	.LBB11_69
.LBB11_28:                              # %.lr.ph..lr.ph.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	$15, 40(%rsp)           # 8-byte Folded Reload
	movl	52(%rsp), %eax          # 4-byte Reload
	movq	%r14, %rcx
	movq	%r10, %rdx
	movl	12(%rsp), %ebx          # 4-byte Reload
	jbe	.LBB11_29
# BB#37:                                # %min.iters.checked543
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	$0, 112(%rsp)           # 8-byte Folded Reload
	movl	52(%rsp), %eax          # 4-byte Reload
	movq	%r14, %rcx
	movq	%r10, %rdx
	je	.LBB11_29
# BB#38:                                # %vector.memcheck556
                                        #   in Loop: Header=BB11_27 Depth=1
	leaq	1(%r10), %rax
	cmpq	%rbp, %rax
	jae	.LBB11_40
# BB#39:                                # %vector.memcheck556
                                        #   in Loop: Header=BB11_27 Depth=1
	movq	224(%rsp), %rax         # 8-byte Reload
	leaq	(%r10,%rax), %rax
	cmpq	%rax, %r8
	movl	52(%rsp), %eax          # 4-byte Reload
	movq	%r14, %rcx
	movq	%r10, %rdx
	jb	.LBB11_29
.LBB11_40:                              # %vector.ph557
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	jne	.LBB11_42
# BB#41:                                #   in Loop: Header=BB11_27 Depth=1
	movq	%rsi, %r8
	xorl	%eax, %eax
	jmp	.LBB11_43
.LBB11_58:                              # %vector.body496.prol
                                        #   in Loop: Header=BB11_27 Depth=1
	movdqu	1(%r14), %xmm6
	pshufd	$78, %xmm6, %xmm4       # xmm4 = xmm6[2,3,0,1]
	pxor	%xmm1, %xmm1
	punpcklbw	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3],xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	movdqa	%xmm4, %xmm5
	punpcklwd	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3]
	punpckhwd	%xmm1, %xmm4    # xmm4 = xmm4[4],xmm1[4],xmm4[5],xmm1[5],xmm4[6],xmm1[6],xmm4[7],xmm1[7]
	punpcklbw	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1],xmm6[2],xmm1[2],xmm6[3],xmm1[3],xmm6[4],xmm1[4],xmm6[5],xmm1[5],xmm6[6],xmm1[6],xmm6[7],xmm1[7]
	movdqa	%xmm6, %xmm7
	punpcklwd	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1],xmm7[2],xmm1[2],xmm7[3],xmm1[3]
	punpckhwd	%xmm1, %xmm6    # xmm6 = xmm6[4],xmm1[4],xmm6[5],xmm1[5],xmm6[6],xmm1[6],xmm6[7],xmm1[7]
	pxor	%xmm12, %xmm6
	pxor	%xmm12, %xmm7
	pxor	%xmm12, %xmm4
	pxor	%xmm12, %xmm5
	movq	%rsi, %r8
	testb	%bl, %bl
	jne	.LBB11_59
# BB#60:                                # %vector.body496.prol
                                        #   in Loop: Header=BB11_27 Depth=1
	movdqu	1(%r10), %xmm0
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	punpcklbw	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1],xmm2[2],xmm1[2],xmm2[3],xmm1[3],xmm2[4],xmm1[4],xmm2[5],xmm1[5],xmm2[6],xmm1[6],xmm2[7],xmm1[7]
	movdqa	%xmm2, %xmm11
	punpcklwd	%xmm1, %xmm11   # xmm11 = xmm11[0],xmm1[0],xmm11[1],xmm1[1],xmm11[2],xmm1[2],xmm11[3],xmm1[3]
	punpckhwd	%xmm1, %xmm2    # xmm2 = xmm2[4],xmm1[4],xmm2[5],xmm1[5],xmm2[6],xmm1[6],xmm2[7],xmm1[7]
	punpcklbw	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3],xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3]
	punpckhwd	%xmm1, %xmm0    # xmm0 = xmm0[4],xmm1[4],xmm0[5],xmm1[5],xmm0[6],xmm1[6],xmm0[7],xmm1[7]
	movdqa	%xmm0, %xmm1
	jmp	.LBB11_61
.LBB11_42:                              # %vector.body539.prol
                                        #   in Loop: Header=BB11_27 Depth=1
	movq	%rsi, %r8
	movdqu	1(%r14), %xmm4
	pshufd	$78, %xmm4, %xmm1       # xmm1 = xmm4[2,3,0,1]
	punpcklbw	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3],xmm4[4],xmm10[4],xmm4[5],xmm10[5],xmm4[6],xmm10[6],xmm4[7],xmm10[7]
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm10, %xmm6   # xmm6 = xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	punpcklwd	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3]
	punpcklbw	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3],xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	movdqa	%xmm1, %xmm5
	punpckhwd	%xmm10, %xmm5   # xmm5 = xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	punpcklwd	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3]
	pxor	%xmm12, %xmm1
	pxor	%xmm12, %xmm5
	pxor	%xmm12, %xmm4
	pxor	%xmm12, %xmm6
	por	%xmm8, %xmm6
	por	%xmm8, %xmm4
	por	%xmm8, %xmm5
	por	%xmm8, %xmm1
	movdqu	1(%r10), %xmm7
	pshufd	$78, %xmm7, %xmm2       # xmm2 = xmm7[2,3,0,1]
	punpcklbw	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3],xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	movdqa	%xmm7, %xmm0
	punpckhwd	%xmm10, %xmm0   # xmm0 = xmm0[4],xmm10[4],xmm0[5],xmm10[5],xmm0[6],xmm10[6],xmm0[7],xmm10[7]
	punpcklwd	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3]
	punpcklbw	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3],xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	movdqa	%xmm2, %xmm3
	punpckhwd	%xmm10, %xmm3   # xmm3 = xmm3[4],xmm10[4],xmm3[5],xmm10[5],xmm3[6],xmm10[6],xmm3[7],xmm10[7]
	punpcklwd	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3]
	pand	%xmm9, %xmm0
	pand	%xmm6, %xmm0
	pand	%xmm9, %xmm7
	pand	%xmm4, %xmm7
	packuswb	%xmm0, %xmm7
	pand	%xmm9, %xmm3
	pand	%xmm5, %xmm3
	pand	%xmm9, %xmm2
	pand	%xmm1, %xmm2
	packuswb	%xmm3, %xmm2
	packuswb	%xmm2, %xmm7
	movdqu	%xmm7, 1(%r10)
	movl	$16, %eax
.LBB11_43:                              # %vector.body539.prol.loopexit
                                        #   in Loop: Header=BB11_27 Depth=1
	movq	112(%rsp), %rcx         # 8-byte Reload
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB11_45
	.p2align	4, 0x90
.LBB11_44:                              # %vector.body539
                                        #   Parent Loop BB11_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	1(%r14,%rax), %xmm4
	pshufd	$78, %xmm4, %xmm1       # xmm1 = xmm4[2,3,0,1]
	punpcklbw	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3],xmm4[4],xmm10[4],xmm4[5],xmm10[5],xmm4[6],xmm10[6],xmm4[7],xmm10[7]
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm10, %xmm6   # xmm6 = xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	punpcklwd	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3]
	punpcklbw	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3],xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	movdqa	%xmm1, %xmm5
	punpckhwd	%xmm10, %xmm5   # xmm5 = xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	punpcklwd	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3]
	pxor	%xmm12, %xmm1
	pxor	%xmm12, %xmm5
	pxor	%xmm12, %xmm4
	pxor	%xmm12, %xmm6
	por	%xmm8, %xmm6
	por	%xmm8, %xmm4
	por	%xmm8, %xmm5
	por	%xmm8, %xmm1
	movdqu	1(%r10,%rax), %xmm7
	pshufd	$78, %xmm7, %xmm2       # xmm2 = xmm7[2,3,0,1]
	punpcklbw	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3],xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	movdqa	%xmm7, %xmm0
	punpckhwd	%xmm10, %xmm0   # xmm0 = xmm0[4],xmm10[4],xmm0[5],xmm10[5],xmm0[6],xmm10[6],xmm0[7],xmm10[7]
	punpcklwd	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3]
	punpcklbw	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3],xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	movdqa	%xmm2, %xmm3
	punpckhwd	%xmm10, %xmm3   # xmm3 = xmm3[4],xmm10[4],xmm3[5],xmm10[5],xmm3[6],xmm10[6],xmm3[7],xmm10[7]
	punpcklwd	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3]
	pand	%xmm9, %xmm0
	pand	%xmm6, %xmm0
	pand	%xmm9, %xmm7
	pand	%xmm4, %xmm7
	packuswb	%xmm0, %xmm7
	pand	%xmm9, %xmm3
	pand	%xmm5, %xmm3
	pand	%xmm9, %xmm2
	pand	%xmm1, %xmm2
	packuswb	%xmm3, %xmm2
	packuswb	%xmm2, %xmm7
	movdqu	%xmm7, 1(%r10,%rax)
	movdqu	17(%r14,%rax), %xmm4
	pshufd	$78, %xmm4, %xmm1       # xmm1 = xmm4[2,3,0,1]
	punpcklbw	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3],xmm4[4],xmm10[4],xmm4[5],xmm10[5],xmm4[6],xmm10[6],xmm4[7],xmm10[7]
	movdqa	%xmm4, %xmm6
	punpckhwd	%xmm10, %xmm6   # xmm6 = xmm6[4],xmm10[4],xmm6[5],xmm10[5],xmm6[6],xmm10[6],xmm6[7],xmm10[7]
	punpcklwd	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1],xmm4[2],xmm10[2],xmm4[3],xmm10[3]
	punpcklbw	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3],xmm1[4],xmm10[4],xmm1[5],xmm10[5],xmm1[6],xmm10[6],xmm1[7],xmm10[7]
	movdqa	%xmm1, %xmm5
	punpckhwd	%xmm10, %xmm5   # xmm5 = xmm5[4],xmm10[4],xmm5[5],xmm10[5],xmm5[6],xmm10[6],xmm5[7],xmm10[7]
	punpcklwd	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1],xmm1[2],xmm10[2],xmm1[3],xmm10[3]
	pxor	%xmm12, %xmm1
	pxor	%xmm12, %xmm5
	pxor	%xmm12, %xmm4
	pxor	%xmm12, %xmm6
	por	%xmm8, %xmm6
	por	%xmm8, %xmm4
	por	%xmm8, %xmm5
	por	%xmm8, %xmm1
	movdqu	17(%r10,%rax), %xmm7
	pshufd	$78, %xmm7, %xmm2       # xmm2 = xmm7[2,3,0,1]
	punpcklbw	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3],xmm7[4],xmm10[4],xmm7[5],xmm10[5],xmm7[6],xmm10[6],xmm7[7],xmm10[7]
	movdqa	%xmm7, %xmm0
	punpckhwd	%xmm10, %xmm0   # xmm0 = xmm0[4],xmm10[4],xmm0[5],xmm10[5],xmm0[6],xmm10[6],xmm0[7],xmm10[7]
	punpcklwd	%xmm10, %xmm7   # xmm7 = xmm7[0],xmm10[0],xmm7[1],xmm10[1],xmm7[2],xmm10[2],xmm7[3],xmm10[3]
	punpcklbw	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3],xmm2[4],xmm10[4],xmm2[5],xmm10[5],xmm2[6],xmm10[6],xmm2[7],xmm10[7]
	movdqa	%xmm2, %xmm3
	punpckhwd	%xmm10, %xmm3   # xmm3 = xmm3[4],xmm10[4],xmm3[5],xmm10[5],xmm3[6],xmm10[6],xmm3[7],xmm10[7]
	punpcklwd	%xmm10, %xmm2   # xmm2 = xmm2[0],xmm10[0],xmm2[1],xmm10[1],xmm2[2],xmm10[2],xmm2[3],xmm10[3]
	pand	%xmm9, %xmm0
	pand	%xmm6, %xmm0
	pand	%xmm9, %xmm7
	pand	%xmm4, %xmm7
	packuswb	%xmm0, %xmm7
	pand	%xmm9, %xmm3
	pand	%xmm5, %xmm3
	pand	%xmm9, %xmm2
	pand	%xmm1, %xmm2
	packuswb	%xmm3, %xmm2
	packuswb	%xmm2, %xmm7
	movdqu	%xmm7, 17(%r10,%rax)
	addq	$32, %rax
	cmpq	%rax, %rcx
	jne	.LBB11_44
.LBB11_45:                              # %middle.block540
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	%rcx, 40(%rsp)          # 8-byte Folded Reload
	je	.LBB11_34
# BB#46:                                #   in Loop: Header=BB11_27 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%r14,%rax), %rcx
	leaq	(%r10,%rax), %rdx
	movl	144(%rsp), %eax         # 4-byte Reload
	movq	%r8, %rsi
.LBB11_29:                              # %.lr.ph..lr.ph.split_crit_edge.us.preheader581
                                        #   in Loop: Header=BB11_27 Depth=1
	movq	%rsi, %r8
	movl	$7, %esi
	subl	%eax, %esi
	movl	%eax, %ebp
	shrl	$3, %ebp
	testl	%esi, %esi
	cmovgl	%r11d, %ebp
	testb	$1, %bpl
	jne	.LBB11_31
# BB#30:                                # %.lr.ph..lr.ph.split_crit_edge.us.prol
                                        #   in Loop: Header=BB11_27 Depth=1
	movzbl	1(%rcx), %esi
	incq	%rcx
	xorl	%r15d, %esi
	orl	%ebx, %esi
	movzbl	1(%rdx), %ebx
	andl	%esi, %ebx
	movb	%bl, 1(%rdx)
	movl	12(%rsp), %ebx          # 4-byte Reload
	incq	%rdx
	addl	$-8, %eax
.LBB11_31:                              # %.lr.ph..lr.ph.split_crit_edge.us.prol.loopexit
                                        #   in Loop: Header=BB11_27 Depth=1
	testl	%ebp, %ebp
	je	.LBB11_34
# BB#32:                                # %.lr.ph..lr.ph.split_crit_edge.us.preheader581.new
                                        #   in Loop: Header=BB11_27 Depth=1
	addq	$2, %rcx
	addq	$2, %rdx
	.p2align	4, 0x90
.LBB11_33:                              # %.lr.ph..lr.ph.split_crit_edge.us
                                        #   Parent Loop BB11_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rcx), %esi
	xorl	%r15d, %esi
	orl	%ebx, %esi
	movzbl	-1(%rdx), %ebp
	andl	%esi, %ebp
	movb	%bpl, -1(%rdx)
	movzbl	(%rcx), %esi
	xorl	%r15d, %esi
	orl	%ebx, %esi
	movzbl	(%rdx), %ebp
	andl	%esi, %ebp
	movb	%bpl, (%rdx)
	addq	$2, %rcx
	addq	$2, %rdx
	addl	$-16, %eax
	jns	.LBB11_33
	jmp	.LBB11_34
.LBB11_59:                              #   in Loop: Header=BB11_27 Depth=1
	pxor	%xmm3, %xmm3
	pxor	%xmm2, %xmm2
	pxor	%xmm11, %xmm11
.LBB11_61:                              # %vector.body496.prol
                                        #   in Loop: Header=BB11_27 Depth=1
	movq	112(%rsp), %rcx         # 8-byte Reload
	por	%xmm11, %xmm5
	por	%xmm2, %xmm4
	por	%xmm3, %xmm7
	por	%xmm1, %xmm6
	pand	%xmm9, %xmm6
	pand	%xmm9, %xmm7
	packuswb	%xmm6, %xmm7
	pand	%xmm9, %xmm4
	pand	%xmm9, %xmm5
	packuswb	%xmm4, %xmm5
	packuswb	%xmm5, %xmm7
	movdqu	%xmm7, 1(%r10)
	movl	$16, %eax
	cmpq	$0, 32(%rsp)            # 8-byte Folded Reload
	je	.LBB11_69
	.p2align	4, 0x90
.LBB11_63:                              # %vector.body496
                                        #   Parent Loop BB11_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	1(%r14,%rax), %xmm7
	pshufd	$78, %xmm7, %xmm5       # xmm5 = xmm7[2,3,0,1]
	pxor	%xmm4, %xmm4
	punpcklbw	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1],xmm5[2],xmm4[2],xmm5[3],xmm4[3],xmm5[4],xmm4[4],xmm5[5],xmm4[5],xmm5[6],xmm4[6],xmm5[7],xmm4[7]
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1],xmm6[2],xmm4[2],xmm6[3],xmm4[3]
	punpckhwd	%xmm4, %xmm5    # xmm5 = xmm5[4],xmm4[4],xmm5[5],xmm4[5],xmm5[6],xmm4[6],xmm5[7],xmm4[7]
	punpcklbw	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1],xmm7[2],xmm4[2],xmm7[3],xmm4[3],xmm7[4],xmm4[4],xmm7[5],xmm4[5],xmm7[6],xmm4[6],xmm7[7],xmm4[7]
	movdqa	%xmm7, %xmm1
	punpcklwd	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3]
	punpckhwd	%xmm4, %xmm7    # xmm7 = xmm7[4],xmm4[4],xmm7[5],xmm4[5],xmm7[6],xmm4[6],xmm7[7],xmm4[7]
	pxor	%xmm12, %xmm7
	pxor	%xmm12, %xmm1
	pxor	%xmm12, %xmm5
	pxor	%xmm12, %xmm6
	testb	%bl, %bl
	pxor	%xmm2, %xmm2
	pxor	%xmm0, %xmm0
	pxor	%xmm3, %xmm3
	pxor	%xmm11, %xmm11
	jne	.LBB11_65
# BB#64:                                # %vector.body496
                                        #   in Loop: Header=BB11_63 Depth=2
	movdqu	1(%r10,%rax), %xmm2
	pshufd	$78, %xmm2, %xmm3       # xmm3 = xmm2[2,3,0,1]
	punpcklbw	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3],xmm3[4],xmm4[4],xmm3[5],xmm4[5],xmm3[6],xmm4[6],xmm3[7],xmm4[7]
	movdqa	%xmm3, %xmm11
	punpcklwd	%xmm4, %xmm11   # xmm11 = xmm11[0],xmm4[0],xmm11[1],xmm4[1],xmm11[2],xmm4[2],xmm11[3],xmm4[3]
	punpckhwd	%xmm4, %xmm3    # xmm3 = xmm3[4],xmm4[4],xmm3[5],xmm4[5],xmm3[6],xmm4[6],xmm3[7],xmm4[7]
	punpcklbw	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3],xmm2[4],xmm4[4],xmm2[5],xmm4[5],xmm2[6],xmm4[6],xmm2[7],xmm4[7]
	movdqa	%xmm2, %xmm0
	punpcklwd	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3]
	punpckhwd	%xmm4, %xmm2    # xmm2 = xmm2[4],xmm4[4],xmm2[5],xmm4[5],xmm2[6],xmm4[6],xmm2[7],xmm4[7]
.LBB11_65:                              # %vector.body496
                                        #   in Loop: Header=BB11_63 Depth=2
	por	%xmm11, %xmm6
	por	%xmm3, %xmm5
	por	%xmm0, %xmm1
	por	%xmm2, %xmm7
	pand	%xmm9, %xmm7
	pand	%xmm9, %xmm1
	packuswb	%xmm7, %xmm1
	pand	%xmm9, %xmm5
	pand	%xmm9, %xmm6
	packuswb	%xmm5, %xmm6
	packuswb	%xmm6, %xmm1
	movdqu	%xmm1, 1(%r10,%rax)
	movdqu	17(%r14,%rax), %xmm1
	pshufd	$78, %xmm1, %xmm5       # xmm5 = xmm1[2,3,0,1]
	punpcklbw	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1],xmm5[2],xmm4[2],xmm5[3],xmm4[3],xmm5[4],xmm4[4],xmm5[5],xmm4[5],xmm5[6],xmm4[6],xmm5[7],xmm4[7]
	movdqa	%xmm5, %xmm6
	punpcklwd	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1],xmm6[2],xmm4[2],xmm6[3],xmm4[3]
	punpckhwd	%xmm4, %xmm5    # xmm5 = xmm5[4],xmm4[4],xmm5[5],xmm4[5],xmm5[6],xmm4[6],xmm5[7],xmm4[7]
	punpcklbw	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1],xmm1[2],xmm4[2],xmm1[3],xmm4[3],xmm1[4],xmm4[4],xmm1[5],xmm4[5],xmm1[6],xmm4[6],xmm1[7],xmm4[7]
	movdqa	%xmm1, %xmm7
	punpcklwd	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1],xmm7[2],xmm4[2],xmm7[3],xmm4[3]
	punpckhwd	%xmm4, %xmm1    # xmm1 = xmm1[4],xmm4[4],xmm1[5],xmm4[5],xmm1[6],xmm4[6],xmm1[7],xmm4[7]
	pxor	%xmm12, %xmm1
	pxor	%xmm12, %xmm7
	pxor	%xmm12, %xmm5
	pxor	%xmm12, %xmm6
	jne	.LBB11_66
# BB#67:                                # %vector.body496
                                        #   in Loop: Header=BB11_63 Depth=2
	movdqu	17(%r10,%rax), %xmm0
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	punpcklbw	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1],xmm2[2],xmm4[2],xmm2[3],xmm4[3],xmm2[4],xmm4[4],xmm2[5],xmm4[5],xmm2[6],xmm4[6],xmm2[7],xmm4[7]
	movdqa	%xmm2, %xmm3
	punpcklwd	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1],xmm3[2],xmm4[2],xmm3[3],xmm4[3]
	punpckhwd	%xmm4, %xmm2    # xmm2 = xmm2[4],xmm4[4],xmm2[5],xmm4[5],xmm2[6],xmm4[6],xmm2[7],xmm4[7]
	punpcklbw	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1],xmm0[2],xmm4[2],xmm0[3],xmm4[3],xmm0[4],xmm4[4],xmm0[5],xmm4[5],xmm0[6],xmm4[6],xmm0[7],xmm4[7]
	movdqa	%xmm0, %xmm11
	punpcklwd	%xmm4, %xmm11   # xmm11 = xmm11[0],xmm4[0],xmm11[1],xmm4[1],xmm11[2],xmm4[2],xmm11[3],xmm4[3]
	punpckhwd	%xmm4, %xmm0    # xmm0 = xmm0[4],xmm4[4],xmm0[5],xmm4[5],xmm0[6],xmm4[6],xmm0[7],xmm4[7]
	movdqa	%xmm0, %xmm4
	jmp	.LBB11_68
.LBB11_66:                              #   in Loop: Header=BB11_63 Depth=2
	pxor	%xmm11, %xmm11
	pxor	%xmm2, %xmm2
	pxor	%xmm3, %xmm3
.LBB11_68:                              # %vector.body496
                                        #   in Loop: Header=BB11_63 Depth=2
	por	%xmm3, %xmm6
	por	%xmm2, %xmm5
	por	%xmm11, %xmm7
	por	%xmm4, %xmm1
	pand	%xmm9, %xmm1
	pand	%xmm9, %xmm7
	packuswb	%xmm1, %xmm7
	pand	%xmm9, %xmm5
	pand	%xmm9, %xmm6
	packuswb	%xmm5, %xmm6
	packuswb	%xmm6, %xmm7
	movdqu	%xmm7, 17(%r10,%rax)
	addq	$32, %rax
	cmpq	%rax, %rcx
	jne	.LBB11_63
.LBB11_69:                              # %middle.block497
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpq	%rcx, 40(%rsp)          # 8-byte Folded Reload
	je	.LBB11_34
# BB#70:                                #   in Loop: Header=BB11_27 Depth=1
	movq	112(%rsp), %rax         # 8-byte Reload
	leaq	(%r14,%rax), %rcx
	leaq	(%r10,%rax), %rdx
	movl	144(%rsp), %eax         # 4-byte Reload
	movq	%r8, %rsi
.LBB11_48:                              # %.lr.ph.split.us.us.preheader580
                                        #   in Loop: Header=BB11_27 Depth=1
	movq	%rsi, %r8
	movl	$7, %esi
	subl	%eax, %esi
	movl	%eax, %ebp
	shrl	$3, %ebp
	testl	%esi, %esi
	cmovgl	%r11d, %ebp
	testb	$1, %bpl
	jne	.LBB11_50
# BB#49:                                # %.lr.ph.split.us.us.prol
                                        #   in Loop: Header=BB11_27 Depth=1
	movzbl	1(%rcx), %esi
	incq	%rcx
	xorl	%r15d, %esi
	movzbl	1(%rdx), %edi
	testb	%bl, %bl
	cmovnel	%r11d, %edi
	orl	%esi, %edi
	movb	%dil, 1(%rdx)
	incq	%rdx
	addl	$-8, %eax
.LBB11_50:                              # %.lr.ph.split.us.us.prol.loopexit
                                        #   in Loop: Header=BB11_27 Depth=1
	testl	%ebp, %ebp
	je	.LBB11_34
# BB#51:                                # %.lr.ph.split.us.us.preheader580.new
                                        #   in Loop: Header=BB11_27 Depth=1
	addq	$2, %rcx
	addq	$2, %rdx
	.p2align	4, 0x90
.LBB11_52:                              # %.lr.ph.split.us.us
                                        #   Parent Loop BB11_27 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rcx), %esi
	xorl	%r15d, %esi
	movzbl	-1(%rdx), %ebp
	testb	%bl, %bl
	cmovnel	%r11d, %ebp
	orl	%esi, %ebp
	testb	%bl, %bl
	movb	%bpl, -1(%rdx)
	movzbl	(%rcx), %esi
	movzbl	(%rdx), %ebp
	cmovnel	%r11d, %ebp
	xorl	%r15d, %esi
	orl	%ebp, %esi
	movb	%sil, (%rdx)
	addq	$2, %rcx
	addq	$2, %rdx
	addl	$-16, %eax
	jns	.LBB11_52
.LBB11_34:                              # %._crit_edge.us
                                        #   in Loop: Header=BB11_27 Depth=1
	cmpl	$0, 104(%rsp)           # 4-byte Folded Reload
	movb	11(%rsp), %r13b         # 1-byte Reload
	jle	.LBB11_36
# BB#35:                                #   in Loop: Header=BB11_27 Depth=1
	movzbl	2(%r9), %eax
	xorl	%r15d, %eax
	movl	208(%rsp), %ecx         # 4-byte Reload
	orl	%eax, %ecx
	movq	160(%rsp), %rsi         # 8-byte Reload
	movzbl	2(%r10,%rsi), %edx
	andl	%ecx, %edx
	andl	88(%rsp), %eax          # 4-byte Folded Reload
	testb	%r13b, %r13b
	cmovel	%r11d, %eax
	orl	%edx, %eax
	movb	%al, 2(%r10,%rsi)
.LBB11_36:                              #   in Loop: Header=BB11_27 Depth=1
	movq	%r8, %rsi
	movq	8(%rsi), %r10
	addq	152(%rsp), %r10         # 8-byte Folded Reload
	addq	$8, %rsi
	movq	56(%rsp), %rbx          # 8-byte Reload
	addq	%rbx, %r14
	addq	%rbx, %r9
	incq	%r12
	xorl	%eax, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	leal	-1(%rcx), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	jg	.LBB11_27
	jmp	.LBB11_146
.LBB11_71:
	cmpl	$0, 24(%rsp)            # 4-byte Folded Reload
	movl	$0, %eax
	movq	%r8, %rsi
	movl	12(%rsp), %ebx          # 4-byte Reload
	jle	.LBB11_146
# BB#72:                                # %.lr.ph288
	andl	$7, %r9d
	movl	$8, %r8d
	subl	%r9d, %r8d
	movq	24(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	72(%rsp), %eax          # 4-byte Reload
	xorl	$255, %eax
	orl	%ebx, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movq	112(%rsp), %rbp         # 8-byte Reload
	movl	%ebp, %eax
	subl	%r12d, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	88(%rsp), %eax          # 4-byte Reload
	xorl	$255, %eax
	orl	%ebx, %eax
	movl	%eax, 200(%rsp)         # 4-byte Spill
	movq	160(%rsp), %rdi         # 8-byte Reload
	leal	-16(%rdi,%rbp), %eax
	movl	%eax, 32(%rsp)          # 4-byte Spill
	movl	%eax, %r11d
	shrl	$3, %r11d
	cmpl	%r12d, 104(%rsp)        # 4-byte Folded Reload
	movq	64(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%r11), %rax
	movslq	56(%rsp), %rcx          # 4-byte Folded Reload
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	jae	.LBB11_73
# BB#76:                                # %.lr.ph288.split.us.preheader
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	2(%rcx,%rax), %rcx
	leal	(,%r11,8), %eax
	subl	%eax, 32(%rsp)          # 4-byte Folded Spill
	movq	%rbp, %r12
	movq	%rcx, %rbp
	addq	$2, %r11
	movq	%r11, 96(%rsp)          # 8-byte Spill
	leal	-16(%rdi,%r12), %eax
	shrl	$3, %eax
	leaq	3(%rdx,%rax), %rcx
	movq	%rcx, 192(%rsp)         # 8-byte Spill
	incq	%rdx
	movq	%rdx, 64(%rsp)          # 8-byte Spill
	leaq	1(%rax), %rcx
	leaq	2(%rax), %rax
	movq	%rax, 240(%rsp)         # 8-byte Spill
	leal	-8(%rdi,%r12), %edx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1073741808, %ecx       # imm = 0x3FFFFFF0
	movq	%rcx, 176(%rsp)         # 8-byte Spill
	leal	(,%rcx,8), %eax
	subl	%eax, %edx
	movl	%edx, 188(%rsp)         # 4-byte Spill
	movd	%r8d, %xmm1
	movd	%r9d, %xmm2
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm14       # xmm14 = xmm0[0,0,0,0]
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm10       # xmm10 = xmm0[0,0,0,0]
	xorl	%edi, %edi
	movdqa	.LCPI11_0(%rip), %xmm11 # xmm11 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pxor	%xmm15, %xmm15
	xorps	%xmm12, %xmm12
	movdqa	%xmm2, 224(%rsp)        # 16-byte Spill
	movss	%xmm2, %xmm12           # xmm12 = xmm2[0],xmm12[1,2,3]
	xorps	%xmm13, %xmm13
	movdqa	%xmm1, 160(%rsp)        # 16-byte Spill
	movss	%xmm1, %xmm13           # xmm13 = xmm1[0],xmm13[1,2,3]
	xorl	%eax, %eax
	movq	%rax, 208(%rsp)         # 8-byte Spill
	jmp	.LBB11_77
.LBB11_103:                             # %vector.ph423
                                        #   in Loop: Header=BB11_77 Depth=1
	leaq	(%r12,%rsi), %r11
	addq	%rsi, %rax
	xorl	%ecx, %ecx
	movb	10(%rsp), %dl           # 1-byte Reload
.LBB11_104:                             # %vector.body404
                                        #   Parent Loop BB11_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pxor	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	xorps	%xmm0, %xmm0
	movaps	160(%rsp), %xmm3        # 16-byte Reload
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movdqu	1(%r14,%rcx), %xmm7
	movdqu	2(%r14,%rcx), %xmm6
	pshufd	$78, %xmm7, %xmm5       # xmm5 = xmm7[2,3,0,1]
	punpcklbw	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3],xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm1, %xmm8    # xmm8 = xmm8[0],xmm1[0],xmm8[1],xmm1[1],xmm8[2],xmm1[2],xmm8[3],xmm1[3]
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklbw	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1],xmm7[2],xmm1[2],xmm7[3],xmm1[3],xmm7[4],xmm1[4],xmm7[5],xmm1[5],xmm7[6],xmm1[6],xmm7[7],xmm1[7]
	movdqa	%xmm7, %xmm9
	punpcklwd	%xmm1, %xmm9    # xmm9 = xmm9[0],xmm1[0],xmm9[1],xmm1[1],xmm9[2],xmm1[2],xmm9[3],xmm1[3]
	punpckhwd	%xmm1, %xmm7    # xmm7 = xmm7[4],xmm1[4],xmm7[5],xmm1[5],xmm7[6],xmm1[6],xmm7[7],xmm1[7]
	pslld	%xmm0, %xmm7
	pslld	%xmm0, %xmm9
	pslld	%xmm0, %xmm5
	pslld	%xmm0, %xmm8
	pshufd	$78, %xmm6, %xmm3       # xmm3 = xmm6[2,3,0,1]
	punpcklbw	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3],xmm3[4],xmm1[4],xmm3[5],xmm1[5],xmm3[6],xmm1[6],xmm3[7],xmm1[7]
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	punpckhwd	%xmm1, %xmm3    # xmm3 = xmm3[4],xmm1[4],xmm3[5],xmm1[5],xmm3[6],xmm1[6],xmm3[7],xmm1[7]
	punpcklbw	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1],xmm6[2],xmm1[2],xmm6[3],xmm1[3],xmm6[4],xmm1[4],xmm6[5],xmm1[5],xmm6[6],xmm1[6],xmm6[7],xmm1[7]
	movdqa	%xmm6, %xmm0
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	punpckhwd	%xmm1, %xmm6    # xmm6 = xmm6[4],xmm1[4],xmm6[5],xmm1[5],xmm6[6],xmm1[6],xmm6[7],xmm1[7]
	psrld	%xmm2, %xmm6
	psrld	%xmm2, %xmm0
	psrld	%xmm2, %xmm3
	psrld	%xmm2, %xmm4
	paddd	%xmm8, %xmm4
	paddd	%xmm5, %xmm3
	paddd	%xmm9, %xmm0
	paddd	%xmm7, %xmm6
	pxor	%xmm14, %xmm6
	pxor	%xmm14, %xmm0
	pxor	%xmm14, %xmm3
	pxor	%xmm14, %xmm4
	testb	%dl, %dl
	jne	.LBB11_105
# BB#106:                               # %vector.body404
                                        #   in Loop: Header=BB11_104 Depth=2
	movdqu	(%r12,%rcx), %xmm5
	pshufd	$78, %xmm5, %xmm2       # xmm2 = xmm5[2,3,0,1]
	punpcklbw	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1],xmm2[2],xmm1[2],xmm2[3],xmm1[3],xmm2[4],xmm1[4],xmm2[5],xmm1[5],xmm2[6],xmm1[6],xmm2[7],xmm1[7]
	movdqa	%xmm2, %xmm7
	punpcklwd	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1],xmm7[2],xmm1[2],xmm7[3],xmm1[3]
	punpckhwd	%xmm1, %xmm2    # xmm2 = xmm2[4],xmm1[4],xmm2[5],xmm1[5],xmm2[6],xmm1[6],xmm2[7],xmm1[7]
	punpcklbw	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3],xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm1, %xmm8    # xmm8 = xmm8[0],xmm1[0],xmm8[1],xmm1[1],xmm8[2],xmm1[2],xmm8[3],xmm1[3]
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	movdqa	%xmm5, %xmm1
	jmp	.LBB11_107
.LBB11_105:                             #   in Loop: Header=BB11_104 Depth=2
	pxor	%xmm8, %xmm8
	pxor	%xmm2, %xmm2
	pxor	%xmm7, %xmm7
.LBB11_107:                             # %vector.body404
                                        #   in Loop: Header=BB11_104 Depth=2
	por	%xmm7, %xmm4
	por	%xmm2, %xmm3
	por	%xmm8, %xmm0
	por	%xmm1, %xmm6
	pand	%xmm11, %xmm6
	pand	%xmm11, %xmm0
	packuswb	%xmm6, %xmm0
	pand	%xmm11, %xmm3
	pand	%xmm11, %xmm4
	packuswb	%xmm3, %xmm4
	packuswb	%xmm4, %xmm0
	movdqu	%xmm0, (%r12,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rsi
	jne	.LBB11_104
# BB#108:                               # %middle.block405
                                        #   in Loop: Header=BB11_77 Depth=1
	cmpq	%rsi, 136(%rsp)         # 8-byte Folded Reload
	movl	188(%rsp), %ebx         # 4-byte Reload
	movl	%edx, %ebp
	movl	$0, %edi
	jne	.LBB11_109
	jmp	.LBB11_111
.LBB11_95:                              # %vector.ph468
                                        #   in Loop: Header=BB11_77 Depth=1
	leaq	(%r12,%rsi), %rbx
	addq	%rsi, %rax
	xorl	%ecx, %ecx
.LBB11_96:                              # %vector.body451
                                        #   Parent Loop BB11_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	1(%r14,%rcx), %xmm7
	movdqu	2(%r14,%rcx), %xmm0
	pshufd	$78, %xmm7, %xmm6       # xmm6 = xmm7[2,3,0,1]
	punpcklbw	%xmm15, %xmm7   # xmm7 = xmm7[0],xmm15[0],xmm7[1],xmm15[1],xmm7[2],xmm15[2],xmm7[3],xmm15[3],xmm7[4],xmm15[4],xmm7[5],xmm15[5],xmm7[6],xmm15[6],xmm7[7],xmm15[7]
	movdqa	%xmm7, %xmm2
	punpckhwd	%xmm15, %xmm2   # xmm2 = xmm2[4],xmm15[4],xmm2[5],xmm15[5],xmm2[6],xmm15[6],xmm2[7],xmm15[7]
	punpcklwd	%xmm15, %xmm7   # xmm7 = xmm7[0],xmm15[0],xmm7[1],xmm15[1],xmm7[2],xmm15[2],xmm7[3],xmm15[3]
	punpcklbw	%xmm15, %xmm6   # xmm6 = xmm6[0],xmm15[0],xmm6[1],xmm15[1],xmm6[2],xmm15[2],xmm6[3],xmm15[3],xmm6[4],xmm15[4],xmm6[5],xmm15[5],xmm6[6],xmm15[6],xmm6[7],xmm15[7]
	movdqa	%xmm6, %xmm5
	punpckhwd	%xmm15, %xmm5   # xmm5 = xmm5[4],xmm15[4],xmm5[5],xmm15[5],xmm5[6],xmm15[6],xmm5[7],xmm15[7]
	punpcklwd	%xmm15, %xmm6   # xmm6 = xmm6[0],xmm15[0],xmm6[1],xmm15[1],xmm6[2],xmm15[2],xmm6[3],xmm15[3]
	pslld	%xmm13, %xmm6
	pslld	%xmm13, %xmm5
	pslld	%xmm13, %xmm7
	pslld	%xmm13, %xmm2
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	punpcklbw	%xmm15, %xmm0   # xmm0 = xmm0[0],xmm15[0],xmm0[1],xmm15[1],xmm0[2],xmm15[2],xmm0[3],xmm15[3],xmm0[4],xmm15[4],xmm0[5],xmm15[5],xmm0[6],xmm15[6],xmm0[7],xmm15[7]
	movdqa	%xmm0, %xmm4
	punpckhwd	%xmm15, %xmm4   # xmm4 = xmm4[4],xmm15[4],xmm4[5],xmm15[5],xmm4[6],xmm15[6],xmm4[7],xmm15[7]
	punpcklwd	%xmm15, %xmm0   # xmm0 = xmm0[0],xmm15[0],xmm0[1],xmm15[1],xmm0[2],xmm15[2],xmm0[3],xmm15[3]
	punpcklbw	%xmm15, %xmm1   # xmm1 = xmm1[0],xmm15[0],xmm1[1],xmm15[1],xmm1[2],xmm15[2],xmm1[3],xmm15[3],xmm1[4],xmm15[4],xmm1[5],xmm15[5],xmm1[6],xmm15[6],xmm1[7],xmm15[7]
	movdqa	%xmm1, %xmm3
	punpckhwd	%xmm15, %xmm3   # xmm3 = xmm3[4],xmm15[4],xmm3[5],xmm15[5],xmm3[6],xmm15[6],xmm3[7],xmm15[7]
	punpcklwd	%xmm15, %xmm1   # xmm1 = xmm1[0],xmm15[0],xmm1[1],xmm15[1],xmm1[2],xmm15[2],xmm1[3],xmm15[3]
	psrld	%xmm12, %xmm1
	psrld	%xmm12, %xmm3
	psrld	%xmm12, %xmm0
	psrld	%xmm12, %xmm4
	paddd	%xmm2, %xmm4
	paddd	%xmm7, %xmm0
	paddd	%xmm5, %xmm3
	paddd	%xmm6, %xmm1
	pxor	%xmm14, %xmm1
	pxor	%xmm14, %xmm3
	pxor	%xmm14, %xmm0
	pxor	%xmm14, %xmm4
	por	%xmm10, %xmm4
	por	%xmm10, %xmm0
	por	%xmm10, %xmm3
	por	%xmm10, %xmm1
	movdqu	(%r12,%rcx), %xmm2
	pshufd	$78, %xmm2, %xmm5       # xmm5 = xmm2[2,3,0,1]
	punpcklbw	%xmm15, %xmm2   # xmm2 = xmm2[0],xmm15[0],xmm2[1],xmm15[1],xmm2[2],xmm15[2],xmm2[3],xmm15[3],xmm2[4],xmm15[4],xmm2[5],xmm15[5],xmm2[6],xmm15[6],xmm2[7],xmm15[7]
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm15, %xmm6   # xmm6 = xmm6[4],xmm15[4],xmm6[5],xmm15[5],xmm6[6],xmm15[6],xmm6[7],xmm15[7]
	punpcklwd	%xmm15, %xmm2   # xmm2 = xmm2[0],xmm15[0],xmm2[1],xmm15[1],xmm2[2],xmm15[2],xmm2[3],xmm15[3]
	punpcklbw	%xmm15, %xmm5   # xmm5 = xmm5[0],xmm15[0],xmm5[1],xmm15[1],xmm5[2],xmm15[2],xmm5[3],xmm15[3],xmm5[4],xmm15[4],xmm5[5],xmm15[5],xmm5[6],xmm15[6],xmm5[7],xmm15[7]
	movdqa	%xmm5, %xmm7
	punpckhwd	%xmm15, %xmm7   # xmm7 = xmm7[4],xmm15[4],xmm7[5],xmm15[5],xmm7[6],xmm15[6],xmm7[7],xmm15[7]
	punpcklwd	%xmm15, %xmm5   # xmm5 = xmm5[0],xmm15[0],xmm5[1],xmm15[1],xmm5[2],xmm15[2],xmm5[3],xmm15[3]
	pand	%xmm11, %xmm6
	pand	%xmm4, %xmm6
	pand	%xmm11, %xmm2
	pand	%xmm0, %xmm2
	packuswb	%xmm6, %xmm2
	pand	%xmm11, %xmm7
	pand	%xmm3, %xmm7
	pand	%xmm11, %xmm5
	pand	%xmm1, %xmm5
	packuswb	%xmm7, %xmm5
	packuswb	%xmm5, %xmm2
	movdqu	%xmm2, (%r12,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rsi
	jne	.LBB11_96
# BB#97:                                # %middle.block452
                                        #   in Loop: Header=BB11_77 Depth=1
	cmpq	%rsi, 136(%rsp)         # 8-byte Folded Reload
	movl	188(%rsp), %edx         # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	jne	.LBB11_90
	jmp	.LBB11_81
.LBB11_77:                              # %.lr.ph288.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_96 Depth 2
                                        #     Child Loop BB11_91 Depth 2
                                        #     Child Loop BB11_104 Depth 2
                                        #     Child Loop BB11_110 Depth 2
	leaq	1(%r14), %rax
	movzbl	(%r14), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movl	104(%rsp), %ecx         # 4-byte Reload
	cmpl	112(%rsp), %ecx         # 4-byte Folded Reload
	jge	.LBB11_79
# BB#78:                                #   in Loop: Header=BB11_77 Depth=1
	movzbl	(%rax), %ebx
	movl	%r9d, %ecx
	shrl	%cl, %ebx
	addl	%ebx, %edx
.LBB11_79:                              #   in Loop: Header=BB11_77 Depth=1
	xorl	%r15d, %edx
	movl	52(%rsp), %ecx          # 4-byte Reload
	orl	%edx, %ecx
	movzbl	(%r10), %ebx
	andl	%ecx, %ebx
	andl	72(%rsp), %edx          # 4-byte Folded Reload
	movl	%r13d, %r11d
	testb	%r11b, %r11b
	cmovel	%edi, %edx
	orl	%ebx, %edx
	leaq	1(%r10), %r12
	movl	16(%rsp), %ecx          # 4-byte Reload
	cmpl	$7, %ecx
	movb	%dl, (%r10)
	jle	.LBB11_80
# BB#87:                                # %.lr.ph279.us
                                        #   in Loop: Header=BB11_77 Depth=1
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	40(%rsp), %rdx          # 8-byte Reload
	imulq	208(%rsp), %rdx         # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rdx), %rcx
	addq	192(%rsp), %rdx         # 8-byte Folded Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	addq	%rsi, %rcx
	addq	%rsi, %rdx
	testb	%r11b, %r11b
	movq	%rbp, 144(%rsp)         # 8-byte Spill
	je	.LBB11_88
# BB#98:                                # %.lr.ph279.split.us.us.preheader
                                        #   in Loop: Header=BB11_77 Depth=1
	cmpq	$16, 136(%rsp)          # 8-byte Folded Reload
	jb	.LBB11_99
# BB#100:                               # %min.iters.checked408
                                        #   in Loop: Header=BB11_77 Depth=1
	movq	176(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB11_99
# BB#101:                               # %vector.memcheck422
                                        #   in Loop: Header=BB11_77 Depth=1
	cmpq	%rdx, %r12
	jae	.LBB11_103
# BB#102:                               # %vector.memcheck422
                                        #   in Loop: Header=BB11_77 Depth=1
	movq	240(%rsp), %rdx         # 8-byte Reload
	leaq	(%r10,%rdx), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB11_103
.LBB11_99:                              #   in Loop: Header=BB11_77 Depth=1
	movq	%r12, %r11
	movl	16(%rsp), %ebx          # 4-byte Reload
	xorl	%edi, %edi
	movb	10(%rsp), %bpl          # 1-byte Reload
.LBB11_109:                             # %.lr.ph279.split.us.us.preheader585
                                        #   in Loop: Header=BB11_77 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB11_110:                             # %.lr.ph279.split.us.us
                                        #   Parent Loop BB11_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movzbl	(%rax), %esi
	movl	%r9d, %ecx
	shrl	%cl, %esi
	addl	%edx, %esi
	xorl	%r15d, %esi
	movzbl	(%r11), %ecx
	testb	%bpl, %bpl
	cmovnel	%edi, %ecx
	orl	%esi, %ecx
	movb	%cl, (%r11)
	addl	$-8, %ebx
	incq	%r11
	incq	%rax
	cmpl	$7, %ebx
	jg	.LBB11_110
.LBB11_111:                             # %._crit_edge280.us.loopexit
                                        #   in Loop: Header=BB11_77 Depth=1
	addq	96(%rsp), %r10          # 8-byte Folded Reload
	movq	144(%rsp), %rbp         # 8-byte Reload
	movq	%rbp, %rax
	movl	32(%rsp), %ebx          # 4-byte Reload
	movq	%r10, %r12
	movq	56(%rsp), %rsi          # 8-byte Reload
	testl	%ebx, %ebx
	jg	.LBB11_83
	jmp	.LBB11_86
.LBB11_80:                              #   in Loop: Header=BB11_77 Depth=1
	movl	%ecx, %ebx
	testl	%ebx, %ebx
	jg	.LBB11_83
	jmp	.LBB11_86
.LBB11_88:                              # %.lr.ph279..lr.ph279.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB11_77 Depth=1
	cmpq	$15, 136(%rsp)          # 8-byte Folded Reload
	jbe	.LBB11_89
# BB#92:                                # %min.iters.checked455
                                        #   in Loop: Header=BB11_77 Depth=1
	movq	176(%rsp), %rsi         # 8-byte Reload
	testq	%rsi, %rsi
	je	.LBB11_89
# BB#93:                                # %vector.memcheck467
                                        #   in Loop: Header=BB11_77 Depth=1
	cmpq	%rdx, %r12
	jae	.LBB11_95
# BB#94:                                # %vector.memcheck467
                                        #   in Loop: Header=BB11_77 Depth=1
	movq	240(%rsp), %rdx         # 8-byte Reload
	leaq	(%r10,%rdx), %rdx
	cmpq	%rdx, %rcx
	jae	.LBB11_95
.LBB11_89:                              #   in Loop: Header=BB11_77 Depth=1
	movq	%r12, %rbx
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
.LBB11_90:                              # %.lr.ph279..lr.ph279.split_crit_edge.us.preheader586
                                        #   in Loop: Header=BB11_77 Depth=1
	incq	%rax
	.p2align	4, 0x90
.LBB11_91:                              # %.lr.ph279..lr.ph279.split_crit_edge.us
                                        #   Parent Loop BB11_77 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rax), %ebp
	movl	%r8d, %ecx
	shll	%cl, %ebp
	movzbl	(%rax), %esi
	movl	%r9d, %ecx
	shrl	%cl, %esi
	addl	%ebp, %esi
	xorl	%r15d, %esi
	orl	%edi, %esi
	movzbl	(%rbx), %ecx
	andl	%esi, %ecx
	movb	%cl, (%rbx)
	addl	$-8, %edx
	incq	%rbx
	incq	%rax
	cmpl	$7, %edx
	jg	.LBB11_91
.LBB11_81:                              # %._crit_edge280.us.loopexit300
                                        #   in Loop: Header=BB11_77 Depth=1
	addq	96(%rsp), %r10          # 8-byte Folded Reload
	movq	144(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %rax
	movl	32(%rsp), %ebx          # 4-byte Reload
	movq	%r10, %r12
	movb	11(%rsp), %r13b         # 1-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%rcx, %rbp
	testl	%ebx, %ebx
	jle	.LBB11_86
.LBB11_83:                              #   in Loop: Header=BB11_77 Depth=1
	movzbl	(%rax), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	cmpl	%r9d, %ebx
	jle	.LBB11_85
# BB#84:                                #   in Loop: Header=BB11_77 Depth=1
	movzbl	1(%rax), %eax
	movl	%r9d, %ecx
	shrl	%cl, %eax
	addl	%eax, %edx
.LBB11_85:                              #   in Loop: Header=BB11_77 Depth=1
	xorl	%r15d, %edx
	movl	200(%rsp), %eax         # 4-byte Reload
	orl	%edx, %eax
	movzbl	(%r12), %ecx
	andl	%eax, %ecx
	andl	88(%rsp), %edx          # 4-byte Folded Reload
	testb	%r13b, %r13b
	movl	$0, %eax
	cmovel	%eax, %edx
	orl	%ecx, %edx
	movb	%dl, (%r12)
.LBB11_86:                              #   in Loop: Header=BB11_77 Depth=1
	movq	8(%rsi), %r10
	addq	152(%rsp), %r10         # 8-byte Folded Reload
	addq	$8, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rax, %r14
	addq	%rax, %rbp
	incq	208(%rsp)               # 8-byte Folded Spill
	xorl	%eax, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	leal	-1(%rcx), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	$0, %edi
	jg	.LBB11_77
	jmp	.LBB11_146
.LBB11_73:                              # %.lr.ph288.split.preheader
	movq	80(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rcx,%rax), %r12
	leal	(,%r11,8), %eax
	subl	%eax, 32(%rsp)          # 4-byte Folded Spill
	addq	$2, %r11
	movq	%r11, 96(%rsp)          # 8-byte Spill
	leal	-16(%rdi,%rbp), %eax
	shrl	$3, %eax
	leaq	2(%rdx,%rax), %rcx
	movq	%rcx, 224(%rsp)         # 8-byte Spill
	leaq	1(%rax), %rcx
	leaq	2(%rax), %rax
	movq	%rax, 192(%rsp)         # 8-byte Spill
	leal	-8(%rdi,%rbp), %edx
	movq	%rcx, 160(%rsp)         # 8-byte Spill
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill> %RCX<def>
	andl	$1073741808, %ecx       # imm = 0x3FFFFFF0
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	leal	(,%rcx,8), %eax
	subl	%eax, %edx
	movl	%edx, 176(%rsp)         # 4-byte Spill
	movd	%r8d, %xmm1
	movd	%r9d, %xmm2
	movd	%r15d, %xmm0
	pshufd	$0, %xmm0, %xmm14       # xmm14 = xmm0[0,0,0,0]
	movd	%ebx, %xmm0
	pshufd	$0, %xmm0, %xmm10       # xmm10 = xmm0[0,0,0,0]
	movdqa	.LCPI11_0(%rip), %xmm11 # xmm11 = [255,0,0,0,255,0,0,0,255,0,0,0,255,0,0,0]
	pxor	%xmm15, %xmm15
	xorps	%xmm12, %xmm12
	movdqa	%xmm2, 208(%rsp)        # 16-byte Spill
	movss	%xmm2, %xmm12           # xmm12 = xmm2[0],xmm12[1,2,3]
	xorps	%xmm13, %xmm13
	movdqa	%xmm1, 112(%rsp)        # 16-byte Spill
	movss	%xmm1, %xmm13           # xmm13 = xmm1[0],xmm13[1,2,3]
	xorl	%ebp, %ebp
	jmp	.LBB11_74
.LBB11_131:                             # %vector.ph
                                        #   in Loop: Header=BB11_74 Depth=1
	leaq	(%r13,%rdx), %rax
	leaq	(%r14,%rdx), %r12
	xorl	%ecx, %ecx
	xorl	%ebp, %ebp
.LBB11_132:                             # %vector.body
                                        #   Parent Loop BB11_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	pxor	%xmm1, %xmm1
	xorps	%xmm2, %xmm2
	movaps	208(%rsp), %xmm0        # 16-byte Reload
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	xorps	%xmm0, %xmm0
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movdqu	(%r14,%rcx), %xmm7
	movdqu	1(%r14,%rcx), %xmm6
	pshufd	$78, %xmm7, %xmm5       # xmm5 = xmm7[2,3,0,1]
	punpcklbw	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3],xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm1, %xmm8    # xmm8 = xmm8[0],xmm1[0],xmm8[1],xmm1[1],xmm8[2],xmm1[2],xmm8[3],xmm1[3]
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	punpcklbw	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1],xmm7[2],xmm1[2],xmm7[3],xmm1[3],xmm7[4],xmm1[4],xmm7[5],xmm1[5],xmm7[6],xmm1[6],xmm7[7],xmm1[7]
	movdqa	%xmm7, %xmm9
	punpcklwd	%xmm1, %xmm9    # xmm9 = xmm9[0],xmm1[0],xmm9[1],xmm1[1],xmm9[2],xmm1[2],xmm9[3],xmm1[3]
	punpckhwd	%xmm1, %xmm7    # xmm7 = xmm7[4],xmm1[4],xmm7[5],xmm1[5],xmm7[6],xmm1[6],xmm7[7],xmm1[7]
	pslld	%xmm0, %xmm7
	pslld	%xmm0, %xmm9
	pslld	%xmm0, %xmm5
	pslld	%xmm0, %xmm8
	pshufd	$78, %xmm6, %xmm3       # xmm3 = xmm6[2,3,0,1]
	punpcklbw	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1],xmm3[2],xmm1[2],xmm3[3],xmm1[3],xmm3[4],xmm1[4],xmm3[5],xmm1[5],xmm3[6],xmm1[6],xmm3[7],xmm1[7]
	movdqa	%xmm3, %xmm4
	punpcklwd	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1],xmm4[2],xmm1[2],xmm4[3],xmm1[3]
	punpckhwd	%xmm1, %xmm3    # xmm3 = xmm3[4],xmm1[4],xmm3[5],xmm1[5],xmm3[6],xmm1[6],xmm3[7],xmm1[7]
	punpcklbw	%xmm1, %xmm6    # xmm6 = xmm6[0],xmm1[0],xmm6[1],xmm1[1],xmm6[2],xmm1[2],xmm6[3],xmm1[3],xmm6[4],xmm1[4],xmm6[5],xmm1[5],xmm6[6],xmm1[6],xmm6[7],xmm1[7]
	movdqa	%xmm6, %xmm0
	punpcklwd	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1],xmm0[2],xmm1[2],xmm0[3],xmm1[3]
	punpckhwd	%xmm1, %xmm6    # xmm6 = xmm6[4],xmm1[4],xmm6[5],xmm1[5],xmm6[6],xmm1[6],xmm6[7],xmm1[7]
	psrld	%xmm2, %xmm6
	psrld	%xmm2, %xmm0
	psrld	%xmm2, %xmm3
	psrld	%xmm2, %xmm4
	paddd	%xmm8, %xmm4
	paddd	%xmm5, %xmm3
	paddd	%xmm9, %xmm0
	paddd	%xmm7, %xmm6
	pxor	%xmm14, %xmm6
	pxor	%xmm14, %xmm0
	pxor	%xmm14, %xmm3
	pxor	%xmm14, %xmm4
	testb	%dil, %dil
	jne	.LBB11_133
# BB#134:                               # %vector.body
                                        #   in Loop: Header=BB11_132 Depth=2
	movdqu	(%r13,%rcx), %xmm5
	pshufd	$78, %xmm5, %xmm2       # xmm2 = xmm5[2,3,0,1]
	punpcklbw	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1],xmm2[2],xmm1[2],xmm2[3],xmm1[3],xmm2[4],xmm1[4],xmm2[5],xmm1[5],xmm2[6],xmm1[6],xmm2[7],xmm1[7]
	movdqa	%xmm2, %xmm7
	punpcklwd	%xmm1, %xmm7    # xmm7 = xmm7[0],xmm1[0],xmm7[1],xmm1[1],xmm7[2],xmm1[2],xmm7[3],xmm1[3]
	punpckhwd	%xmm1, %xmm2    # xmm2 = xmm2[4],xmm1[4],xmm2[5],xmm1[5],xmm2[6],xmm1[6],xmm2[7],xmm1[7]
	punpcklbw	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1],xmm5[2],xmm1[2],xmm5[3],xmm1[3],xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	movdqa	%xmm5, %xmm8
	punpcklwd	%xmm1, %xmm8    # xmm8 = xmm8[0],xmm1[0],xmm8[1],xmm1[1],xmm8[2],xmm1[2],xmm8[3],xmm1[3]
	punpckhwd	%xmm1, %xmm5    # xmm5 = xmm5[4],xmm1[4],xmm5[5],xmm1[5],xmm5[6],xmm1[6],xmm5[7],xmm1[7]
	movdqa	%xmm5, %xmm1
	jmp	.LBB11_135
.LBB11_133:                             #   in Loop: Header=BB11_132 Depth=2
	pxor	%xmm8, %xmm8
	pxor	%xmm2, %xmm2
	pxor	%xmm7, %xmm7
.LBB11_135:                             # %vector.body
                                        #   in Loop: Header=BB11_132 Depth=2
	por	%xmm7, %xmm4
	por	%xmm2, %xmm3
	por	%xmm8, %xmm0
	por	%xmm1, %xmm6
	pand	%xmm11, %xmm6
	pand	%xmm11, %xmm0
	packuswb	%xmm6, %xmm0
	pand	%xmm11, %xmm3
	pand	%xmm11, %xmm4
	packuswb	%xmm3, %xmm4
	packuswb	%xmm4, %xmm0
	movdqu	%xmm0, (%r13,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB11_132
# BB#136:                               # %middle.block
                                        #   in Loop: Header=BB11_74 Depth=1
	cmpq	%rdx, 160(%rsp)         # 8-byte Folded Reload
	movl	176(%rsp), %ebx         # 4-byte Reload
	movl	32(%rsp), %r13d         # 4-byte Reload
	jne	.LBB11_137
	jmp	.LBB11_139
.LBB11_122:                             # %vector.ph376
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	%r12, %r11
	leaq	(%r13,%rdx), %rax
	leaq	(%r14,%rdx), %rbx
	xorl	%ecx, %ecx
.LBB11_123:                             # %vector.body359
                                        #   Parent Loop BB11_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	(%r14,%rcx), %xmm7
	movdqu	1(%r14,%rcx), %xmm0
	pshufd	$78, %xmm7, %xmm6       # xmm6 = xmm7[2,3,0,1]
	punpcklbw	%xmm15, %xmm7   # xmm7 = xmm7[0],xmm15[0],xmm7[1],xmm15[1],xmm7[2],xmm15[2],xmm7[3],xmm15[3],xmm7[4],xmm15[4],xmm7[5],xmm15[5],xmm7[6],xmm15[6],xmm7[7],xmm15[7]
	movdqa	%xmm7, %xmm2
	punpckhwd	%xmm15, %xmm2   # xmm2 = xmm2[4],xmm15[4],xmm2[5],xmm15[5],xmm2[6],xmm15[6],xmm2[7],xmm15[7]
	punpcklwd	%xmm15, %xmm7   # xmm7 = xmm7[0],xmm15[0],xmm7[1],xmm15[1],xmm7[2],xmm15[2],xmm7[3],xmm15[3]
	punpcklbw	%xmm15, %xmm6   # xmm6 = xmm6[0],xmm15[0],xmm6[1],xmm15[1],xmm6[2],xmm15[2],xmm6[3],xmm15[3],xmm6[4],xmm15[4],xmm6[5],xmm15[5],xmm6[6],xmm15[6],xmm6[7],xmm15[7]
	movdqa	%xmm6, %xmm5
	punpckhwd	%xmm15, %xmm5   # xmm5 = xmm5[4],xmm15[4],xmm5[5],xmm15[5],xmm5[6],xmm15[6],xmm5[7],xmm15[7]
	punpcklwd	%xmm15, %xmm6   # xmm6 = xmm6[0],xmm15[0],xmm6[1],xmm15[1],xmm6[2],xmm15[2],xmm6[3],xmm15[3]
	pslld	%xmm13, %xmm6
	pslld	%xmm13, %xmm5
	pslld	%xmm13, %xmm7
	pslld	%xmm13, %xmm2
	pshufd	$78, %xmm0, %xmm1       # xmm1 = xmm0[2,3,0,1]
	punpcklbw	%xmm15, %xmm0   # xmm0 = xmm0[0],xmm15[0],xmm0[1],xmm15[1],xmm0[2],xmm15[2],xmm0[3],xmm15[3],xmm0[4],xmm15[4],xmm0[5],xmm15[5],xmm0[6],xmm15[6],xmm0[7],xmm15[7]
	movdqa	%xmm0, %xmm4
	punpckhwd	%xmm15, %xmm4   # xmm4 = xmm4[4],xmm15[4],xmm4[5],xmm15[5],xmm4[6],xmm15[6],xmm4[7],xmm15[7]
	punpcklwd	%xmm15, %xmm0   # xmm0 = xmm0[0],xmm15[0],xmm0[1],xmm15[1],xmm0[2],xmm15[2],xmm0[3],xmm15[3]
	punpcklbw	%xmm15, %xmm1   # xmm1 = xmm1[0],xmm15[0],xmm1[1],xmm15[1],xmm1[2],xmm15[2],xmm1[3],xmm15[3],xmm1[4],xmm15[4],xmm1[5],xmm15[5],xmm1[6],xmm15[6],xmm1[7],xmm15[7]
	movdqa	%xmm1, %xmm3
	punpckhwd	%xmm15, %xmm3   # xmm3 = xmm3[4],xmm15[4],xmm3[5],xmm15[5],xmm3[6],xmm15[6],xmm3[7],xmm15[7]
	punpcklwd	%xmm15, %xmm1   # xmm1 = xmm1[0],xmm15[0],xmm1[1],xmm15[1],xmm1[2],xmm15[2],xmm1[3],xmm15[3]
	psrld	%xmm12, %xmm1
	psrld	%xmm12, %xmm3
	psrld	%xmm12, %xmm0
	psrld	%xmm12, %xmm4
	paddd	%xmm2, %xmm4
	paddd	%xmm7, %xmm0
	paddd	%xmm5, %xmm3
	paddd	%xmm6, %xmm1
	pxor	%xmm14, %xmm1
	pxor	%xmm14, %xmm3
	pxor	%xmm14, %xmm0
	pxor	%xmm14, %xmm4
	por	%xmm10, %xmm4
	por	%xmm10, %xmm0
	por	%xmm10, %xmm3
	por	%xmm10, %xmm1
	movdqu	(%r13,%rcx), %xmm2
	pshufd	$78, %xmm2, %xmm5       # xmm5 = xmm2[2,3,0,1]
	punpcklbw	%xmm15, %xmm2   # xmm2 = xmm2[0],xmm15[0],xmm2[1],xmm15[1],xmm2[2],xmm15[2],xmm2[3],xmm15[3],xmm2[4],xmm15[4],xmm2[5],xmm15[5],xmm2[6],xmm15[6],xmm2[7],xmm15[7]
	movdqa	%xmm2, %xmm6
	punpckhwd	%xmm15, %xmm6   # xmm6 = xmm6[4],xmm15[4],xmm6[5],xmm15[5],xmm6[6],xmm15[6],xmm6[7],xmm15[7]
	punpcklwd	%xmm15, %xmm2   # xmm2 = xmm2[0],xmm15[0],xmm2[1],xmm15[1],xmm2[2],xmm15[2],xmm2[3],xmm15[3]
	punpcklbw	%xmm15, %xmm5   # xmm5 = xmm5[0],xmm15[0],xmm5[1],xmm15[1],xmm5[2],xmm15[2],xmm5[3],xmm15[3],xmm5[4],xmm15[4],xmm5[5],xmm15[5],xmm5[6],xmm15[6],xmm5[7],xmm15[7]
	movdqa	%xmm5, %xmm7
	punpckhwd	%xmm15, %xmm7   # xmm7 = xmm7[4],xmm15[4],xmm7[5],xmm15[5],xmm7[6],xmm15[6],xmm7[7],xmm15[7]
	punpcklwd	%xmm15, %xmm5   # xmm5 = xmm5[0],xmm15[0],xmm5[1],xmm15[1],xmm5[2],xmm15[2],xmm5[3],xmm15[3]
	pand	%xmm11, %xmm6
	pand	%xmm4, %xmm6
	pand	%xmm11, %xmm2
	pand	%xmm0, %xmm2
	packuswb	%xmm6, %xmm2
	pand	%xmm11, %xmm7
	pand	%xmm3, %xmm7
	pand	%xmm11, %xmm5
	pand	%xmm1, %xmm5
	packuswb	%xmm7, %xmm5
	packuswb	%xmm5, %xmm2
	movdqu	%xmm2, (%r13,%rcx)
	addq	$16, %rcx
	cmpq	%rcx, %rdx
	jne	.LBB11_123
# BB#124:                               # %middle.block360
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	%r14, %r12
	cmpq	%rdx, 160(%rsp)         # 8-byte Folded Reload
	movl	176(%rsp), %edx         # 4-byte Reload
	movl	12(%rsp), %edi          # 4-byte Reload
	movl	32(%rsp), %r13d         # 4-byte Reload
	jne	.LBB11_116
	jmp	.LBB11_125
.LBB11_74:                              # %.lr.ph288.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_123 Depth 2
                                        #     Child Loop BB11_117 Depth 2
                                        #     Child Loop BB11_132 Depth 2
                                        #     Child Loop BB11_138 Depth 2
	movzbl	(%r14), %eax
	movl	%r9d, %ecx
	shrl	%cl, %eax
	xorl	%r15d, %eax
	movl	52(%rsp), %ecx          # 4-byte Reload
	orl	%eax, %ecx
	movzbl	(%r10), %edx
	andl	%ecx, %edx
	andl	72(%rsp), %eax          # 4-byte Folded Reload
	movl	%r13d, %r11d
	testb	%r11b, %r11b
	movl	$0, %ecx
	cmovel	%ecx, %eax
	orl	%edx, %eax
	movb	%al, (%r10)
	leaq	1(%r10), %r13
	movl	16(%rsp), %eax          # 4-byte Reload
	cmpl	$8, %eax
	jl	.LBB11_75
# BB#112:                               # %.lr.ph279
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	imulq	%rbp, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rcx), %rax
	movq	80(%rsp), %rdx          # 8-byte Reload
	addq	%rdx, %rax
	addq	224(%rsp), %rcx         # 8-byte Folded Reload
	addq	%rdx, %rcx
	testb	%r11b, %r11b
	movq	%rsi, 56(%rsp)          # 8-byte Spill
	movq	%rbp, 104(%rsp)         # 8-byte Spill
	je	.LBB11_113
# BB#126:                               # %.lr.ph279.split.us.preheader
                                        #   in Loop: Header=BB11_74 Depth=1
	cmpq	$16, 160(%rsp)          # 8-byte Folded Reload
	movb	10(%rsp), %dil          # 1-byte Reload
	movq	%r12, 144(%rsp)         # 8-byte Spill
	jb	.LBB11_127
# BB#128:                               # %min.iters.checked
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB11_127
# BB#129:                               # %vector.memcheck
                                        #   in Loop: Header=BB11_74 Depth=1
	cmpq	%rcx, %r13
	jae	.LBB11_131
# BB#130:                               # %vector.memcheck
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	192(%rsp), %rcx         # 8-byte Reload
	leaq	(%r10,%rcx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB11_131
.LBB11_127:                             #   in Loop: Header=BB11_74 Depth=1
	movq	%r13, %rax
	movl	16(%rsp), %ebx          # 4-byte Reload
	movq	%r14, %r12
	movl	32(%rsp), %r13d         # 4-byte Reload
	xorl	%ebp, %ebp
.LBB11_137:                             # %.lr.ph279.split.us.preheader588
                                        #   in Loop: Header=BB11_74 Depth=1
	incq	%r12
	.p2align	4, 0x90
.LBB11_138:                             # %.lr.ph279.split.us
                                        #   Parent Loop BB11_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%r12), %edx
	movl	%r8d, %ecx
	shll	%cl, %edx
	movzbl	(%r12), %esi
	movl	%r9d, %ecx
	shrl	%cl, %esi
	addl	%edx, %esi
	xorl	%r15d, %esi
	movzbl	(%rax), %ecx
	testb	%dil, %dil
	cmovnel	%ebp, %ecx
	orl	%esi, %ecx
	movb	%cl, (%rax)
	addl	$-8, %ebx
	incq	%rax
	incq	%r12
	cmpl	$7, %ebx
	jg	.LBB11_138
.LBB11_139:                             # %._crit_edge280.loopexit
                                        #   in Loop: Header=BB11_74 Depth=1
	addq	96(%rsp), %r10          # 8-byte Folded Reload
	movq	144(%rsp), %r12         # 8-byte Reload
	movq	%r12, %rdx
	movl	%r13d, %ebx
	movq	%r10, %r13
	movq	56(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB11_140
.LBB11_75:                              #   in Loop: Header=BB11_74 Depth=1
	movq	%r14, %rdx
	movl	%eax, %ebx
	testl	%ebx, %ebx
	jg	.LBB11_142
	jmp	.LBB11_145
.LBB11_113:                             # %.lr.ph279.split.preheader
                                        #   in Loop: Header=BB11_74 Depth=1
	cmpq	$15, 160(%rsp)          # 8-byte Folded Reload
	jbe	.LBB11_114
# BB#118:                               # %min.iters.checked363
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	136(%rsp), %rdx         # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB11_114
# BB#119:                               # %vector.memcheck375
                                        #   in Loop: Header=BB11_74 Depth=1
	cmpq	%rcx, %r13
	jae	.LBB11_122
# BB#120:                               # %vector.memcheck375
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	192(%rsp), %rcx         # 8-byte Reload
	leaq	(%r10,%rcx), %rcx
	cmpq	%rcx, %rax
	jae	.LBB11_122
# BB#121:                               #   in Loop: Header=BB11_74 Depth=1
	movq	%r12, %r11
	movq	%r13, %rax
	movl	16(%rsp), %edx          # 4-byte Reload
	movq	%r14, %r12
	movq	%r14, %rbx
	movl	12(%rsp), %edi          # 4-byte Reload
	jmp	.LBB11_115
.LBB11_114:                             #   in Loop: Header=BB11_74 Depth=1
	movq	%r12, %r11
	movq	%r13, %rax
	movl	16(%rsp), %edx          # 4-byte Reload
	movl	%ebx, %edi
	movq	%r14, %r12
	movq	%r14, %rbx
.LBB11_115:                             # %.lr.ph279.split.preheader589
                                        #   in Loop: Header=BB11_74 Depth=1
	movl	32(%rsp), %r13d         # 4-byte Reload
.LBB11_116:                             # %.lr.ph279.split.preheader589
                                        #   in Loop: Header=BB11_74 Depth=1
	incq	%rbx
	.p2align	4, 0x90
.LBB11_117:                             # %.lr.ph279.split
                                        #   Parent Loop BB11_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-1(%rbx), %esi
	movl	%r8d, %ecx
	shll	%cl, %esi
	movzbl	(%rbx), %ebp
	movl	%r9d, %ecx
	shrl	%cl, %ebp
	addl	%esi, %ebp
	xorl	%r15d, %ebp
	orl	%edi, %ebp
	movzbl	(%rax), %ecx
	andl	%ebp, %ecx
	movb	%cl, (%rax)
	addl	$-8, %edx
	incq	%rax
	incq	%rbx
	cmpl	$7, %edx
	jg	.LBB11_117
.LBB11_125:                             # %._crit_edge280.loopexit302
                                        #   in Loop: Header=BB11_74 Depth=1
	addq	96(%rsp), %r10          # 8-byte Folded Reload
	movq	%r11, %rax
	movq	%rax, %rdx
	movl	%r13d, %ebx
	movq	%r10, %r13
	movb	11(%rsp), %r11b         # 1-byte Reload
	movq	56(%rsp), %rsi          # 8-byte Reload
	movq	%r12, %r14
	movq	%rax, %r12
.LBB11_140:                             # %._crit_edge280
                                        #   in Loop: Header=BB11_74 Depth=1
	movq	104(%rsp), %rbp         # 8-byte Reload
	testl	%ebx, %ebx
	jle	.LBB11_145
.LBB11_142:                             #   in Loop: Header=BB11_74 Depth=1
	movzbl	(%rdx), %eax
	movl	%r8d, %ecx
	shll	%cl, %eax
	cmpl	%r9d, %ebx
	jle	.LBB11_144
# BB#143:                               #   in Loop: Header=BB11_74 Depth=1
	movzbl	1(%rdx), %edx
	movl	%r9d, %ecx
	shrl	%cl, %edx
	addl	%edx, %eax
.LBB11_144:                             #   in Loop: Header=BB11_74 Depth=1
	xorl	%r15d, %eax
	movl	200(%rsp), %ecx         # 4-byte Reload
	orl	%eax, %ecx
	movzbl	(%r13), %edx
	andl	%ecx, %edx
	andl	88(%rsp), %eax          # 4-byte Folded Reload
	testb	%r11b, %r11b
	movl	$0, %ecx
	cmovel	%ecx, %eax
	orl	%edx, %eax
	movb	%al, (%r13)
.LBB11_145:                             #   in Loop: Header=BB11_74 Depth=1
	movl	%r11d, %r13d
	movq	8(%rsi), %r10
	addq	152(%rsp), %r10         # 8-byte Folded Reload
	addq	$8, %rsi
	movq	40(%rsp), %rax          # 8-byte Reload
	addq	%rax, %r14
	addq	%rax, %r12
	incq	%rbp
	xorl	%eax, %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	leal	-1(%rcx), %ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movl	12(%rsp), %ebx          # 4-byte Reload
	jg	.LBB11_74
	jmp	.LBB11_146
.LBB11_22:                              # %.lr.ph266.split.preheader
	leaq	8(%rbp,%r11,8), %rdx
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	leaq	1(%rax,%rcx), %rsi
	xorl	%r8d, %r8d
.LBB11_23:                              # %.lr.ph266.split
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-1(%rsi), %eax
	xorl	%r15d, %eax
	movl	16(%rsp), %ebx          # 4-byte Reload
	orl	%eax, %ebx
	movzbl	(%r10), %ebp
	andl	%ebx, %ebp
	andl	72(%rsp), %eax          # 4-byte Folded Reload
	testb	%r13b, %r13b
	cmovel	%r8d, %eax
	orl	%ebp, %eax
	movb	%al, (%r10)
	testl	%r9d, %r9d
	jle	.LBB11_25
# BB#24:                                #   in Loop: Header=BB11_23 Depth=1
	movzbl	(%rsi), %eax
	xorl	%r15d, %eax
	movl	208(%rsp), %ebp         # 4-byte Reload
	orl	%eax, %ebp
	movzbl	1(%r10), %ebx
	andl	%ebp, %ebx
	andl	88(%rsp), %eax          # 4-byte Folded Reload
	testb	%r13b, %r13b
	cmovel	%r8d, %eax
	orl	%ebx, %eax
	movb	%al, 1(%r10)
.LBB11_25:                              #   in Loop: Header=BB11_23 Depth=1
	movq	(%rdx), %r10
	addq	152(%rsp), %r10         # 8-byte Folded Reload
	addq	$8, %rdx
	addq	56(%rsp), %rsi          # 8-byte Folded Reload
	xorl	%eax, %eax
	movq	24(%rsp), %rbp          # 8-byte Reload
	decl	%ebp
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	jg	.LBB11_23
.LBB11_146:                             # %mem_copy_mono_recover.exit.thread
	addq	$248, %rsp
.LBB11_147:                             # %mem_copy_mono_recover.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	mem_mono_copy_mono, .Lfunc_end11-mem_mono_copy_mono
	.cfi_endproc

	.globl	mem_mono_copy_color
	.p2align	4, 0x90
	.type	mem_mono_copy_color,@function
mem_mono_copy_color:                    # @mem_mono_copy_color
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi104:
	.cfi_def_cfa_offset 16
	movl	16(%rsp), %r10d
	movl	24(%rsp), %eax
	pushq	$1
.Lcfi105:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi106:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi107:
	.cfi_adjust_cfa_offset 8
	pushq	%r10
.Lcfi108:
	.cfi_adjust_cfa_offset 8
	callq	mem_mono_copy_mono
	addq	$32, %rsp
.Lcfi109:
	.cfi_adjust_cfa_offset -32
	popq	%rcx
	retq
.Lfunc_end12:
	.size	mem_mono_copy_color, .Lfunc_end12-mem_mono_copy_color
	.cfi_endproc

	.globl	copy_byte_rect
	.p2align	4, 0x90
	.type	copy_byte_rect,@function
copy_byte_rect:                         # @copy_byte_rect
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi110:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi111:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi112:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi113:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi114:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi116:
	.cfi_def_cfa_offset 64
.Lcfi117:
	.cfi_offset %rbx, -56
.Lcfi118:
	.cfi_offset %r12, -48
.Lcfi119:
	.cfi_offset %r13, -40
.Lcfi120:
	.cfi_offset %r14, -32
.Lcfi121:
	.cfi_offset %r15, -24
.Lcfi122:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movl	64(%rsp), %ebp
	testl	%ebp, %ebp
	jle	.LBB13_3
# BB#1:                                 # %.lr.ph
	movslq	%ecx, %r15
	movslq	%r8d, %rbx
	shlq	$3, %rbx
	addq	168(%rdi), %rbx
	movslq	%r9d, %r14
	movslq	%edx, %r13
	incl	%ebp
	.p2align	4, 0x90
.LBB13_2:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	addq	%r15, %rdi
	movq	%r12, %rsi
	movq	%r14, %rdx
	callq	memcpy
	addq	%r13, %r12
	addq	$8, %rbx
	decl	%ebp
	cmpl	$1, %ebp
	jg	.LBB13_2
.LBB13_3:                               # %._crit_edge
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end13:
	.size	copy_byte_rect, .Lfunc_end13-copy_byte_rect
	.cfi_endproc

	.globl	mem_mapped_map_rgb_color
	.p2align	4, 0x90
	.type	mem_mapped_map_rgb_color,@function
mem_mapped_map_rgb_color:               # @mem_mapped_map_rgb_color
	.cfi_startproc
# BB#0:
	movl	188(%rdi), %r8d
	testl	%r8d, %r8d
	jle	.LBB14_1
# BB#2:                                 # %.lr.ph
	pushq	%rbp
.Lcfi123:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 24
.Lcfi125:
	.cfi_offset %rbx, -24
.Lcfi126:
	.cfi_offset %rbp, -16
	movq	192(%rdi), %r9
	movzwl	%si, %esi
	movzwl	%dx, %r11d
	movzwl	%cx, %r10d
	incl	%r8d
	movl	$768, %edi              # imm = 0x300
                                        # implicit-def: %RAX
	movq	%r9, %rcx
	.p2align	4, 0x90
.LBB14_3:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rcx), %edx
	subl	%esi, %edx
	movl	%edx, %ebx
	negl	%ebx
	cmovll	%edx, %ebx
	cmpl	%edi, %ebx
	jge	.LBB14_6
# BB#4:                                 #   in Loop: Header=BB14_3 Depth=1
	movzbl	1(%rcx), %ebp
	subl	%r11d, %ebp
	movl	%ebp, %edx
	negl	%edx
	cmovll	%ebp, %edx
	addl	%ebx, %edx
	cmpl	%edi, %edx
	jge	.LBB14_6
# BB#5:                                 #   in Loop: Header=BB14_3 Depth=1
	movzbl	2(%rcx), %ebx
	subl	%r10d, %ebx
	movl	%ebx, %ebp
	negl	%ebp
	cmovll	%ebx, %ebp
	addl	%edx, %ebp
	cmpl	%edi, %ebp
	cmovlq	%rcx, %rax
	cmovlel	%ebp, %edi
.LBB14_6:                               #   in Loop: Header=BB14_3 Depth=1
	addq	$3, %rcx
	decl	%r8d
	cmpl	$1, %r8d
	jg	.LBB14_3
# BB#7:
	popq	%rbx
	popq	%rbp
	jmp	.LBB14_8
.LBB14_1:                               # %.._crit_edge_crit_edge
	movq	192(%rdi), %r9
                                        # implicit-def: %RAX
.LBB14_8:                               # %._crit_edge
	subq	%r9, %rax
	movabsq	$6148914691236517206, %rcx # imm = 0x5555555555555556
	imulq	%rcx
	movq	%rdx, %rax
	shrq	$63, %rax
	leaq	(%rax,%rdx), %rax
	retq
.Lfunc_end14:
	.size	mem_mapped_map_rgb_color, .Lfunc_end14-mem_mapped_map_rgb_color
	.cfi_endproc

	.globl	mem_mapped_map_color_rgb
	.p2align	4, 0x90
	.type	mem_mapped_map_color_rgb,@function
mem_mapped_map_color_rgb:               # @mem_mapped_map_color_rgb
	.cfi_startproc
# BB#0:
	movq	192(%rdi), %rax
	shlq	$32, %rsi
	leaq	(%rsi,%rsi,2), %rcx
	sarq	$32, %rcx
	movzbl	(%rax,%rcx), %esi
	movw	%si, (%rdx)
	movzbl	1(%rax,%rcx), %esi
	movw	%si, 2(%rdx)
	movzbl	2(%rax,%rcx), %eax
	movw	%ax, 4(%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end15:
	.size	mem_mapped_map_color_rgb, .Lfunc_end15-mem_mapped_map_color_rgb
	.cfi_endproc

	.globl	mem_mapped_fill_rectangle
	.p2align	4, 0x90
	.type	mem_mapped_fill_rectangle,@function
mem_mapped_fill_rectangle:              # @mem_mapped_fill_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi127:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi128:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi130:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi131:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi132:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi133:
	.cfi_def_cfa_offset 80
.Lcfi134:
	.cfi_offset %rbx, -56
.Lcfi135:
	.cfi_offset %r12, -48
.Lcfi136:
	.cfi_offset %r13, -40
.Lcfi137:
	.cfi_offset %r14, -32
.Lcfi138:
	.cfi_offset %r15, -24
.Lcfi139:
	.cfi_offset %rbp, -16
	movq	%r9, 16(%rsp)           # 8-byte Spill
	movl	%r8d, %ebx
	movl	%edx, %r13d
	movl	%esi, %r12d
	movq	%rdi, %r15
	movq	176(%r15), %rax
	movl	$mem_no_fault_proc, %edx
	cmpq	%rdx, %rax
	je	.LBB16_7
# BB#1:
	movl	$1, %r9d
	movq	%r15, %rdi
	movl	%r12d, %esi
	movl	%r13d, %edx
	movl	%ecx, %ebp
	movl	%ebx, %r8d
	callq	*%rax
	movl	%ebp, %ecx
	movl	%eax, %r14d
	testl	%r14d, %r14d
	js	.LBB16_2
.LBB16_7:                               # %mem_fill_recover.exit
	xorl	%r14d, %r14d
	testl	%ebx, %ebx
	jle	.LBB16_10
# BB#8:                                 # %.lr.ph
	movslq	%r12d, %r12
	movslq	%r13d, %rbp
	shlq	$3, %rbp
	addq	168(%r15), %rbp
	movslq	%ecx, %r15
	incl	%ebx
	movzbl	16(%rsp), %r13d         # 1-byte Folded Reload
	.p2align	4, 0x90
.LBB16_9:                               # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	addq	%r12, %rdi
	movl	%r13d, %esi
	movq	%r15, %rdx
	callq	memset
	addq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB16_9
.LBB16_10:                              # %mem_fill_recover.exit.thread
	movl	%r14d, %eax
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB16_2:
	cmpl	$-2, %r14d
	je	.LBB16_5
# BB#3:
	cmpl	$-1, %r14d
	jne	.LBB16_10
# BB#4:
	movl	%ecx, %ebp
	sarl	%ebp
	leal	(%rbp,%r12), %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	subl	%ebp, %ecx
	movl	%r13d, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%ebx, %r14d
	jmp	.LBB16_6
.LBB16_5:
	movl	%ebx, %r14d
	sarl	%r14d
	leal	(%r14,%r13), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	subl	%r14d, %ebx
	movl	%r12d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%ecx, %ebp
.LBB16_6:
	movq	8(%r15), %rax
	movq	%r15, %rdi
	movl	%r12d, %esi
	movl	%r13d, %edx
	movl	%ebx, %r8d
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	%rbx, %r9
	callq	*56(%rax)
	movq	8(%r15), %rax
	movq	56(%rax), %rax
	movq	%r15, %rdi
	movl	12(%rsp), %esi          # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%ebp, %ecx
	movl	%r14d, %r8d
	movq	%rbx, %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end16:
	.size	mem_mapped_fill_rectangle, .Lfunc_end16-mem_mapped_fill_rectangle
	.cfi_endproc

	.globl	mem_mapped_copy_mono
	.p2align	4, 0x90
	.type	mem_mapped_copy_mono,@function
mem_mapped_copy_mono:                   # @mem_mapped_copy_mono
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi142:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi143:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi144:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi145:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi146:
	.cfi_def_cfa_offset 112
.Lcfi147:
	.cfi_offset %rbx, -56
.Lcfi148:
	.cfi_offset %r12, -48
.Lcfi149:
	.cfi_offset %r13, -40
.Lcfi150:
	.cfi_offset %r14, -32
.Lcfi151:
	.cfi_offset %r15, -24
.Lcfi152:
	.cfi_offset %rbp, -16
	movl	%r9d, %r11d
	movl	%r8d, %ebx
	movl	%ecx, %r10d
	movq	%rsi, %r14
	movq	%rdi, %rbp
	movq	136(%rsp), %r13
	movq	128(%rsp), %r12
	movl	120(%rsp), %r15d
	movl	112(%rsp), %r8d
	movq	176(%rbp), %rax
	movl	$mem_no_fault_proc, %esi
	cmpq	%rsi, %rax
	je	.LBB17_7
# BB#1:
	movl	$1, %r9d
	movq	%rbp, %rdi
	movl	%ebx, %esi
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movl	%r11d, %edx
	movl	%r8d, %ecx
	movl	%r15d, %r8d
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movl	%r10d, %ebx
	movq	%r11, 48(%rsp)          # 8-byte Spill
	callq	*%rax
	movl	112(%rsp), %r8d
	movq	48(%rsp), %r11          # 8-byte Reload
	movl	28(%rsp), %edx          # 4-byte Reload
	movl	%ebx, %r10d
	movq	16(%rsp), %rbx          # 8-byte Reload
	testl	%eax, %eax
	js	.LBB17_2
.LBB17_7:                               # %mem_copy_mono_recover.exit
	movl	%edx, %ecx
	andb	$7, %cl
	movl	$128, %r9d
	shrl	%cl, %r9d
	xorl	%eax, %eax
	testl	%r15d, %r15d
	jle	.LBB17_32
# BB#8:                                 # %.lr.ph
	movslq	%ebx, %rsi
	movslq	%r11d, %r11
	shlq	$3, %r11
	addq	168(%rbp), %r11
	sarl	$3, %edx
	movslq	%edx, %rcx
	addq	%rcx, %r14
	decl	%r15d
	cmpq	$-1, %r13
	movslq	%r10d, %r10
	je	.LBB17_16
# BB#9:                                 # %.lr.ph.split.preheader
	incl	%r8d
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB17_10:                              # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_11 Depth 2
                                        #     Child Loop BB17_25 Depth 2
	movq	(%r11), %rbp
	addq	%rax, %rbp
	leaq	1(%r14), %rsi
	cmpq	$-1, %r12
	movzbl	(%r14), %edx
	movl	%r8d, %edi
	movl	%r9d, %ebx
	je	.LBB17_25
	.p2align	4, 0x90
.LBB17_11:                              # %.split.split
                                        #   Parent Loop BB17_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %ebx
	movl	%r12d, %ecx
	je	.LBB17_13
# BB#12:                                # %.split.split
                                        #   in Loop: Header=BB17_11 Depth=2
	movl	%r13d, %ecx
.LBB17_13:                              # %.split.split
                                        #   in Loop: Header=BB17_11 Depth=2
	movb	%cl, (%rbp)
	sarl	%ebx
	jne	.LBB17_15
# BB#14:                                #   in Loop: Header=BB17_11 Depth=2
	movzbl	(%rsi), %edx
	incq	%rsi
	movl	$128, %ebx
.LBB17_15:                              #   in Loop: Header=BB17_11 Depth=2
	incq	%rbp
	decl	%edi
	cmpl	$1, %edi
	jg	.LBB17_11
	jmp	.LBB17_30
	.p2align	4, 0x90
.LBB17_25:                              # %.split.split.us
                                        #   Parent Loop BB17_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %ebx
	je	.LBB17_27
# BB#26:                                #   in Loop: Header=BB17_25 Depth=2
	movb	%r13b, (%rbp)
.LBB17_27:                              #   in Loop: Header=BB17_25 Depth=2
	sarl	%ebx
	jne	.LBB17_29
# BB#28:                                #   in Loop: Header=BB17_25 Depth=2
	movzbl	(%rsi), %edx
	incq	%rsi
	movl	$128, %ebx
.LBB17_29:                              #   in Loop: Header=BB17_25 Depth=2
	incq	%rbp
	decl	%edi
	cmpl	$1, %edi
	jg	.LBB17_25
.LBB17_30:                              # %.us-lcssa
                                        #   in Loop: Header=BB17_10 Depth=1
	addq	%r10, %r14
	addq	$8, %r11
	testl	%r15d, %r15d
	leal	-1(%r15), %ecx
	movl	%ecx, %r15d
	jg	.LBB17_10
# BB#31:
	xorl	%eax, %eax
	jmp	.LBB17_32
.LBB17_2:
	movq	%r8, %rbx
	cmpl	$-2, %eax
	je	.LBB17_5
# BB#3:
	cmpl	$-1, %eax
	jne	.LBB17_32
# BB#4:
	movq	%rbx, %rax
	movl	%eax, %ecx
	sarl	%ecx
	movq	16(%rsp), %r8           # 8-byte Reload
	leal	(%rcx,%r8), %esi
	movl	%esi, 12(%rsp)          # 4-byte Spill
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	subl	%ecx, %eax
	movl	%r11d, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movl	%r15d, %ecx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	jmp	.LBB17_6
.LBB17_16:                              # %.lr.ph.split.us.preheader
	incl	%r8d
	.p2align	4, 0x90
.LBB17_17:                              # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_19 Depth 2
	cmpq	$-1, %r12
	je	.LBB17_24
# BB#18:                                # %.split.us..split.us.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB17_17 Depth=1
	movzbl	(%r14), %edx
	leaq	1(%r14), %rcx
	movq	(%r11), %rbp
	addq	%rsi, %rbp
	movl	%r8d, %edi
	movl	%r9d, %ebx
	.p2align	4, 0x90
.LBB17_19:                              # %.split.us..split.us.split_crit_edge.us
                                        #   Parent Loop BB17_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %ebx
	jne	.LBB17_21
# BB#20:                                #   in Loop: Header=BB17_19 Depth=2
	movb	%r12b, (%rbp)
.LBB17_21:                              #   in Loop: Header=BB17_19 Depth=2
	sarl	%ebx
	jne	.LBB17_23
# BB#22:                                #   in Loop: Header=BB17_19 Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movl	$128, %ebx
.LBB17_23:                              #   in Loop: Header=BB17_19 Depth=2
	incq	%rbp
	decl	%edi
	cmpl	$1, %edi
	jg	.LBB17_19
.LBB17_24:                              # %.us-lcssa.us.us
                                        #   in Loop: Header=BB17_17 Depth=1
	addq	%r10, %r14
	addq	$8, %r11
	testl	%r15d, %r15d
	leal	-1(%r15), %ecx
	movl	%ecx, %r15d
	jg	.LBB17_17
.LBB17_32:                              # %mem_copy_mono_recover.exit.thread
	addq	$56, %rsp
.LBB17_33:                              # %mem_copy_mono_recover.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB17_5:
	movl	%r15d, %eax
	sarl	%eax
	leal	(%rax,%r11), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subl	%eax, %r15d
	movq	16(%rsp), %r8           # 8-byte Reload
	movl	%r8d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movq	%rbx, %rax
	movl	%eax, %ecx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
.LBB17_6:
	movq	%r11, %r9
	movq	8(%rbp), %r11
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%edx, %ebx
	movl	%r10d, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%r13
.Lcfi153:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi154:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi155:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi156:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %r15d
	callq	*72(%r11)
	addq	$32, %rsp
.Lcfi157:
	.cfi_adjust_cfa_offset -32
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
	movl	%r15d, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	pushq	%r13
.Lcfi158:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi159:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi160:
	.cfi_adjust_cfa_offset 8
	pushq	64(%rsp)                # 8-byte Folded Reload
.Lcfi161:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$88, %rsp
.Lcfi162:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB17_33
.Lfunc_end17:
	.size	mem_mapped_copy_mono, .Lfunc_end17-mem_mapped_copy_mono
	.cfi_endproc

	.globl	mem_mapped_copy_color
	.p2align	4, 0x90
	.type	mem_mapped_copy_color,@function
mem_mapped_copy_color:                  # @mem_mapped_copy_color
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi163:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi164:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi165:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi166:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi167:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi168:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi169:
	.cfi_def_cfa_offset 96
.Lcfi170:
	.cfi_offset %rbx, -56
.Lcfi171:
	.cfi_offset %r12, -48
.Lcfi172:
	.cfi_offset %r13, -40
.Lcfi173:
	.cfi_offset %r14, -32
.Lcfi174:
	.cfi_offset %r15, -24
.Lcfi175:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r12d
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	104(%rsp), %ebx
	movl	96(%rsp), %r13d
	movq	176(%r15), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	je	.LBB18_7
# BB#1:
	movl	$1, %r9d
	movq	%r15, %rdi
	movl	%r12d, %esi
	movl	%ebp, %edx
	movl	%r13d, %ecx
	movl	%ebx, %r8d
	callq	*%rax
	testl	%eax, %eax
	js	.LBB18_2
.LBB18_7:                               # %mem_copy_color_recover.exit
	testl	%r13d, %r13d
	jle	.LBB18_16
# BB#8:                                 # %mem_copy_color_recover.exit
	testl	%ebx, %ebx
	jle	.LBB18_16
# BB#9:
	movl	$-1, %eax
	testl	%r12d, %r12d
	js	.LBB18_17
# BB#10:
	testl	%ebp, %ebp
	js	.LBB18_17
# BB#11:
	movl	24(%r15), %ecx
	subl	%r13d, %ecx
	cmpl	%r12d, %ecx
	jl	.LBB18_17
# BB#12:
	movl	28(%r15), %ecx
	subl	%ebx, %ecx
	cmpl	%ebp, %ecx
	jl	.LBB18_17
# BB#13:
	testl	%ebx, %ebx
	jle	.LBB18_16
# BB#14:                                # %.lr.ph.i
	movslq	%r12d, %r12
	movslq	8(%rsp), %rax           # 4-byte Folded Reload
	addq	%rax, %r14
	movslq	%ebp, %rbp
	shlq	$3, %rbp
	addq	168(%r15), %rbp
	movslq	%r13d, %r15
	movslq	12(%rsp), %r13          # 4-byte Folded Reload
	incl	%ebx
	.p2align	4, 0x90
.LBB18_15:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	addq	%r12, %rdi
	movq	%r14, %rsi
	movq	%r15, %rdx
	callq	memcpy
	addq	%r13, %r14
	addq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB18_15
.LBB18_16:
	xorl	%eax, %eax
.LBB18_17:                              # %copy_byte_rect.exit
	addq	$40, %rsp
.LBB18_18:                              # %copy_byte_rect.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB18_2:
	cmpl	$-2, %eax
	je	.LBB18_5
# BB#3:
	cmpl	$-1, %eax
	jne	.LBB18_17
# BB#4:
	movl	%r13d, %eax
	sarl	%eax
	leal	(%rax,%r12), %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subl	%eax, %r13d
	movl	%ebp, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB18_6
.LBB18_5:
	movl	%ebx, %eax
	sarl	%eax
	leal	(%rax,%rbp), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subl	%eax, %ebx
	movl	%r12d, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%r13d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB18_6:
	movq	8(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r12d, %r8d
	movl	%ebp, %r9d
	pushq	%rbx
.Lcfi176:
	.cfi_adjust_cfa_offset 8
	pushq	%r13
.Lcfi177:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$16, %rsp
.Lcfi178:
	.cfi_adjust_cfa_offset -16
	movq	8(%r15), %rax
	movq	%r15, %rdi
	movq	%r14, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	20(%rsp), %r8d          # 4-byte Reload
	movl	16(%rsp), %r9d          # 4-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi179:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi180:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$56, %rsp
.Lcfi181:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB18_18
.Lfunc_end18:
	.size	mem_mapped_copy_color, .Lfunc_end18-mem_mapped_copy_color
	.cfi_endproc

	.globl	mem_true_map_rgb_color
	.p2align	4, 0x90
	.type	mem_true_map_rgb_color,@function
mem_true_map_rgb_color:                 # @mem_true_map_rgb_color
	.cfi_startproc
# BB#0:
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movzbl	%cl, %ecx
	shlq	$24, %rcx
	movzbl	%dl, %edx
	shlq	$16, %rdx
	shll	$8, %esi
	movzwl	%si, %eax
	orq	%rdx, %rax
	orq	%rcx, %rax
	retq
.Lfunc_end19:
	.size	mem_true_map_rgb_color, .Lfunc_end19-mem_true_map_rgb_color
	.cfi_endproc

	.globl	mem_true_map_color_rgb
	.p2align	4, 0x90
	.type	mem_true_map_color_rgb,@function
mem_true_map_color_rgb:                 # @mem_true_map_color_rgb
	.cfi_startproc
# BB#0:
	movq	%rsi, %rax
	movzbl	%ah, %ecx  # NOREX
	shrl	$16, %esi
	shrl	$24, %eax
	movw	%cx, (%rdx)
	movzbl	%sil, %ecx
	movw	%cx, 2(%rdx)
	movw	%ax, 4(%rdx)
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	mem_true_map_color_rgb, .Lfunc_end20-mem_true_map_color_rgb
	.cfi_endproc

	.globl	mem_true24_fill_rectangle
	.p2align	4, 0x90
	.type	mem_true24_fill_rectangle,@function
mem_true24_fill_rectangle:              # @mem_true24_fill_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi182:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi183:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi184:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi185:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi186:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi187:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi188:
	.cfi_def_cfa_offset 80
.Lcfi189:
	.cfi_offset %rbx, -56
.Lcfi190:
	.cfi_offset %r12, -48
.Lcfi191:
	.cfi_offset %r13, -40
.Lcfi192:
	.cfi_offset %r14, -32
.Lcfi193:
	.cfi_offset %r15, -24
.Lcfi194:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%r8d, %r14d
	movl	%ecx, %r15d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	movq	176(%r12), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	je	.LBB21_1
# BB#8:
	leal	(%rsi,%rsi,2), %r13d
	leal	(%r15,%r15,2), %ecx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%r13d, %esi
	movq	%rdx, %rbp
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r14d, %r8d
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB21_2
# BB#9:
	movq	16(%rsp), %rsi          # 8-byte Reload
	cmpl	$-2, %eax
	je	.LBB21_12
# BB#10:
	cmpl	$-1, %eax
	movq	%rbp, %rdx
	jne	.LBB21_7
# BB#11:
	movl	%r15d, %ebp
	sarl	%ebp
	leal	(%rbp,%rsi), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	subl	%ebp, %r15d
	movl	%edx, %r13d
	movl	%r14d, %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB21_13
.LBB21_1:                               # %.mem_fill_recover.exit_crit_edge
	movq	%rdx, %rbp
	leal	(%rsi,%rsi,2), %r13d
.LBB21_2:                               # %mem_fill_recover.exit
	xorl	%eax, %eax
	testl	%r14d, %r14d
	jle	.LBB21_7
# BB#3:                                 # %.lr.ph.preheader
	movq	%rbx, %rcx
	shrq	$8, %rcx
	movq	%rbx, %rdx
	shrq	$16, %rdx
	shrq	$24, %rbx
	movslq	%r13d, %r8
	movslq	%ebp, %rdi
	shlq	$3, %rdi
	addq	168(%r12), %rdi
	incl	%r15d
	.p2align	4, 0x90
.LBB21_4:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB21_5 Depth 2
	movq	(%rdi), %rbp
	addq	%r8, %rbp
	movl	%r15d, %esi
	.p2align	4, 0x90
.LBB21_5:                               #   Parent Loop BB21_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movb	%cl, (%rbp)
	movb	%dl, 1(%rbp)
	movb	%bl, 2(%rbp)
	decl	%esi
	addq	$3, %rbp
	cmpl	$1, %esi
	jg	.LBB21_5
# BB#6:                                 #   in Loop: Header=BB21_4 Depth=1
	addq	$8, %rdi
	cmpl	$1, %r14d
	leal	-1(%r14), %esi
	movl	%esi, %r14d
	jg	.LBB21_4
.LBB21_7:                               # %mem_fill_recover.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB21_12:
	movl	%r14d, %eax
	sarl	%eax
	movq	%rbp, %rdx
	leal	(%rax,%rdx), %r13d
	movq	%rax, 8(%rsp)           # 8-byte Spill
	subl	%eax, %r14d
	movl	%esi, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%r15d, %ebp
.LBB21_13:
	movq	8(%r12), %rax
	movq	%r12, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r15d, %ecx
	movl	%r14d, %r8d
	movq	%rbx, %r9
	callq	*56(%rax)
	movq	8(%r12), %rax
	movq	56(%rax), %rax
	movq	%r12, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	%r13d, %edx
	movl	%ebp, %ecx
	movq	8(%rsp), %r8            # 8-byte Reload
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
	movq	%rbx, %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end21:
	.size	mem_true24_fill_rectangle, .Lfunc_end21-mem_true24_fill_rectangle
	.cfi_endproc

	.globl	mem_true24_copy_mono
	.p2align	4, 0x90
	.type	mem_true24_copy_mono,@function
mem_true24_copy_mono:                   # @mem_true24_copy_mono
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi195:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi196:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi197:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi198:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi199:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi200:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi201:
	.cfi_def_cfa_offset 144
.Lcfi202:
	.cfi_offset %rbx, -56
.Lcfi203:
	.cfi_offset %r12, -48
.Lcfi204:
	.cfi_offset %r13, -40
.Lcfi205:
	.cfi_offset %r14, -32
.Lcfi206:
	.cfi_offset %r15, -24
.Lcfi207:
	.cfi_offset %rbp, -16
	movl	%r9d, %r10d
	movl	%r8d, %ebp
	movl	%ecx, %r9d
	movq	%rsi, %rbx
	movq	168(%rsp), %r12
	movq	160(%rsp), %r14
	movl	152(%rsp), %r8d
	movl	144(%rsp), %r11d
	movq	%r14, %r15
	shrq	$32, %r15
	movq	%r12, %r13
	shrq	$32, %r13
	movq	176(%rdi), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	movq	%rdi, 64(%rsp)          # 8-byte Spill
	movq	%r10, 40(%rsp)          # 8-byte Spill
	movq	%r11, 16(%rsp)          # 8-byte Spill
	je	.LBB22_1
# BB#12:
	leal	(%rbp,%rbp,2), %esi
	leal	(%r11,%r11,2), %ecx
	movl	%r9d, 80(%rsp)          # 4-byte Spill
	movl	$1, %r9d
	movl	%esi, 24(%rsp)          # 4-byte Spill
	movq	%r15, %r12
	movq	%r13, %r15
	movl	%edx, %r13d
	movl	%r10d, %edx
	movq	%r8, %r14
	movq	%rbp, 72(%rsp)          # 8-byte Spill
	callq	*%rax
	movl	%r13d, %edx
	movq	%r15, %r13
	movq	%r12, %r15
	movl	80(%rsp), %r9d          # 4-byte Reload
	movq	168(%rsp), %rbp
	movq	%rbp, %r12
	movq	%r14, %r8
	movq	160(%rsp), %rbp
	movq	%rbp, %r14
	testl	%eax, %eax
	jns	.LBB22_2
# BB#13:
	movq	72(%rsp), %rdi          # 8-byte Reload
	cmpl	$-2, %eax
	je	.LBB22_16
# BB#14:
	cmpl	$-1, %eax
	movq	40(%rsp), %rcx          # 8-byte Reload
	movl	144(%rsp), %esi
	jne	.LBB22_35
# BB#15:
	movl	%esi, %eax
	sarl	%eax
	leal	(%rax,%rdi), %ebp
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movq	%rax, 56(%rsp)          # 8-byte Spill
	subl	%eax, %esi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	movl	%eax, 52(%rsp)          # 4-byte Spill
	movl	%r8d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rcx, %r10
	movq	%rdi, %r11
	movq	%r8, 24(%rsp)           # 8-byte Spill
	jmp	.LBB22_17
.LBB22_1:                               # %.mem_copy_mono_recover.exit_crit_edge
	leal	(%rbp,%rbp,2), %eax
	movl	%eax, 24(%rsp)          # 4-byte Spill
.LBB22_2:                               # %mem_copy_mono_recover.exit
	movl	%edx, %ecx
	andb	$7, %cl
	movl	$128, %eax
	shrl	%cl, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	xorl	%eax, %eax
	testl	%r8d, %r8d
	jle	.LBB22_35
# BB#3:                                 # %.lr.ph
	movq	%r14, %r11
	shrq	$8, %r11
	movq	%r14, %rsi
	shrq	$16, %rsi
	sarl	$3, %edx
	movslq	%edx, %rax
	addq	%rax, %rbx
	movq	%r14, %rdi
	shrq	$24, %rdi
	movslq	40(%rsp), %rcx          # 4-byte Folded Reload
	shlq	$3, %rcx
	movq	64(%rsp), %rax          # 8-byte Reload
	addq	168(%rax), %rcx
	movq	%r12, %r10
	shrq	$8, %r10
	shlq	$32, %r15
	movl	%r14d, %eax
	orq	%r15, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	shlq	$32, %r13
	movl	%r12d, %eax
	orq	%r13, %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	%r12, %r14
	shrq	$16, %r14
	movq	%r12, %rbp
	shrq	$24, %rbp
	movslq	24(%rsp), %rdx          # 4-byte Folded Reload
	movq	%rdx, 72(%rsp)          # 8-byte Spill
	movslq	%r9d, %rdx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	incl	%eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB22_4:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB22_20 Depth 2
                                        #     Child Loop BB22_28 Depth 2
                                        #     Child Loop BB22_7 Depth 2
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	(%rcx), %r8
	addq	72(%rsp), %r8           # 8-byte Folded Reload
	leaq	1(%rbx), %rax
	movzbl	(%rbx), %r13d
	cmpq	$-1, 80(%rsp)           # 8-byte Folded Reload
	movq	%rbx, 40(%rsp)          # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	je	.LBB22_5
# BB#18:                                # %.split
                                        #   in Loop: Header=BB22_4 Depth=1
	cmpq	$-1, 32(%rsp)           # 8-byte Folded Reload
	je	.LBB22_27
# BB#19:                                #   in Loop: Header=BB22_4 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %r15d
	movl	12(%rsp), %r12d         # 4-byte Reload
	.p2align	4, 0x90
.LBB22_20:                              # %.split.split
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%r12d, %r9d
	andl	%r13d, %r9d
	movl	%r11d, %edx
	movl	%esi, %ebx
	je	.LBB22_22
# BB#21:                                # %.split.split
                                        #   in Loop: Header=BB22_20 Depth=2
	movl	%r10d, %edx
	movl	%r14d, %ebx
.LBB22_22:                              # %.split.split
                                        #   in Loop: Header=BB22_20 Depth=2
	testl	%r9d, %r9d
	movl	%edi, %ecx
	je	.LBB22_24
# BB#23:                                # %.split.split
                                        #   in Loop: Header=BB22_20 Depth=2
	movl	%ebp, %ecx
.LBB22_24:                              # %.split.split
                                        #   in Loop: Header=BB22_20 Depth=2
	movb	%dl, (%r8)
	movb	%bl, 1(%r8)
	movb	%cl, 2(%r8)
	sarl	%r12d
	jne	.LBB22_26
# BB#25:                                #   in Loop: Header=BB22_20 Depth=2
	movzbl	(%rax), %r13d
	incq	%rax
	movl	$128, %r12d
.LBB22_26:                              #   in Loop: Header=BB22_20 Depth=2
	decl	%r15d
	addq	$3, %r8
	cmpl	$1, %r15d
	jg	.LBB22_20
	jmp	.LBB22_33
	.p2align	4, 0x90
.LBB22_5:                               # %.split.us
                                        #   in Loop: Header=BB22_4 Depth=1
	cmpq	$-1, 32(%rsp)           # 8-byte Folded Reload
	je	.LBB22_33
# BB#6:                                 # %.split.us.split.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	movl	12(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB22_7:                               # %.split.us.split
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%r13d, %ebx
	jne	.LBB22_9
# BB#8:                                 #   in Loop: Header=BB22_7 Depth=2
	movb	%r11b, (%r8)
	movb	%sil, 1(%r8)
	movb	%dil, 2(%r8)
	addq	$3, %r8
.LBB22_9:                               #   in Loop: Header=BB22_7 Depth=2
	sarl	%ebx
	jne	.LBB22_11
# BB#10:                                #   in Loop: Header=BB22_7 Depth=2
	movzbl	(%rax), %r13d
	incq	%rax
	movl	$128, %ebx
.LBB22_11:                              #   in Loop: Header=BB22_7 Depth=2
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB22_7
	jmp	.LBB22_33
	.p2align	4, 0x90
.LBB22_27:                              # %.split.split.us.preheader
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%ecx, %edx
	movl	12(%rsp), %ebx          # 4-byte Reload
	.p2align	4, 0x90
.LBB22_28:                              # %.split.split.us
                                        #   Parent Loop BB22_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%r13d, %ebx
	je	.LBB22_30
# BB#29:                                #   in Loop: Header=BB22_28 Depth=2
	movb	%r10b, (%r8)
	movb	%r14b, 1(%r8)
	movb	%bpl, 2(%r8)
	addq	$3, %r8
.LBB22_30:                              #   in Loop: Header=BB22_28 Depth=2
	sarl	%ebx
	jne	.LBB22_32
# BB#31:                                #   in Loop: Header=BB22_28 Depth=2
	movzbl	(%rax), %r13d
	incq	%rax
	movl	$128, %ebx
.LBB22_32:                              #   in Loop: Header=BB22_28 Depth=2
	decl	%edx
	cmpl	$1, %edx
	jg	.LBB22_28
.LBB22_33:                              # %.us-lcssa.us
                                        #   in Loop: Header=BB22_4 Depth=1
	movq	40(%rsp), %rbx          # 8-byte Reload
	addq	56(%rsp), %rbx          # 8-byte Folded Reload
	movq	64(%rsp), %rcx          # 8-byte Reload
	addq	$8, %rcx
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpl	$1, %eax
	leal	-1(%rax), %eax
	movl	%eax, %r8d
	jg	.LBB22_4
# BB#34:
	xorl	%eax, %eax
.LBB22_35:                              # %mem_copy_mono_recover.exit.thread
	addq	$88, %rsp
.LBB22_36:                              # %mem_copy_mono_recover.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB22_16:
	movl	%r8d, %eax
	sarl	%eax
	movq	40(%rsp), %r10          # 8-byte Reload
	leal	(%rax,%r10), %ecx
	movl	%ecx, 52(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subl	%eax, %r8d
	movq	%r8, 24(%rsp)           # 8-byte Spill
	movq	%rdi, %r11
	movl	%r11d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	144(%rsp), %eax
	movl	%eax, %ecx
	movq	%rcx, 56(%rsp)          # 8-byte Spill
.LBB22_17:
	shlq	$32, %r15
	movl	%r14d, %r14d
	orq	%r15, %r14
	shlq	$32, %r13
	movl	%r12d, %r12d
	orq	%r13, %r12
	movq	64(%rsp), %rbp          # 8-byte Reload
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movl	%edx, %r13d
	movl	%r9d, %ecx
	movl	%r11d, %r8d
	movl	%r9d, %r15d
	movl	%r10d, %r9d
	pushq	%r12
.Lcfi208:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi209:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi210:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi211:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$32, %rsp
.Lcfi212:
	.cfi_adjust_cfa_offset -32
	movq	8(%rbp), %rax
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	movl	%r13d, %edx
	movl	%r15d, %ecx
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	52(%rsp), %r9d          # 4-byte Reload
	pushq	%r12
.Lcfi213:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi214:
	.cfi_adjust_cfa_offset 8
	pushq	48(%rsp)                # 8-byte Folded Reload
.Lcfi215:
	.cfi_adjust_cfa_offset 8
	pushq	80(%rsp)                # 8-byte Folded Reload
.Lcfi216:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$120, %rsp
.Lcfi217:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB22_36
.Lfunc_end22:
	.size	mem_true24_copy_mono, .Lfunc_end22-mem_true24_copy_mono
	.cfi_endproc

	.globl	mem_true24_copy_color
	.p2align	4, 0x90
	.type	mem_true24_copy_color,@function
mem_true24_copy_color:                  # @mem_true24_copy_color
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi218:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi219:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi220:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi221:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi222:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi223:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi224:
	.cfi_def_cfa_offset 96
.Lcfi225:
	.cfi_offset %rbx, -56
.Lcfi226:
	.cfi_offset %r12, -48
.Lcfi227:
	.cfi_offset %r13, -40
.Lcfi228:
	.cfi_offset %r14, -32
.Lcfi229:
	.cfi_offset %r15, -24
.Lcfi230:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r15d
	movl	%ecx, 4(%rsp)           # 4-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	104(%rsp), %ebx
	movl	96(%rsp), %r12d
	movq	176(%r13), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	je	.LBB23_7
# BB#1:
	leal	(%r15,%r15,2), %esi
	leal	(%r12,%r12,2), %ecx
	movl	$1, %r9d
	movq	%r13, %rdi
	movl	%ebp, %edx
	movl	%ebx, %r8d
	callq	*%rax
	testl	%eax, %eax
	js	.LBB23_2
.LBB23_7:                               # %mem_copy_color_recover.exit
	testl	%r12d, %r12d
	jle	.LBB23_16
# BB#8:                                 # %mem_copy_color_recover.exit
	testl	%ebx, %ebx
	jle	.LBB23_16
# BB#9:
	movl	$-1, %eax
	testl	%r15d, %r15d
	js	.LBB23_17
# BB#10:
	testl	%ebp, %ebp
	js	.LBB23_17
# BB#11:
	movl	24(%r13), %ecx
	subl	%r12d, %ecx
	cmpl	%r15d, %ecx
	jl	.LBB23_17
# BB#12:
	movl	28(%r13), %ecx
	subl	%ebx, %ecx
	cmpl	%ebp, %ecx
	jl	.LBB23_17
# BB#13:
	testl	%ebx, %ebx
	jle	.LBB23_16
# BB#14:                                # %.lr.ph.i
	leal	(%r15,%r15,2), %eax
	movslq	%eax, %r15
	leal	(%r12,%r12,2), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	leal	(%rcx,%rcx,2), %ecx
	movslq	%ecx, %rcx
	addq	%rcx, %r14
	movslq	%ebp, %rbp
	shlq	$3, %rbp
	addq	168(%r13), %rbp
	movslq	%eax, %r12
	movslq	4(%rsp), %r13           # 4-byte Folded Reload
	incl	%ebx
	.p2align	4, 0x90
.LBB23_15:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	addq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
	addq	%r13, %r14
	addq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB23_15
.LBB23_16:
	xorl	%eax, %eax
.LBB23_17:                              # %copy_byte_rect.exit
	addq	$40, %rsp
.LBB23_18:                              # %copy_byte_rect.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB23_2:
	cmpl	$-2, %eax
	je	.LBB23_5
# BB#3:
	cmpl	$-1, %eax
	jne	.LBB23_17
# BB#4:
	movl	%r12d, %eax
	sarl	%eax
	leal	(%rax,%r15), %ecx
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subl	%eax, %r12d
	movl	%ebp, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%ebx, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB23_6
.LBB23_5:
	movl	%ebx, %eax
	sarl	%eax
	leal	(%rax,%rbp), %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subl	%eax, %ebx
	movl	%r15d, %eax
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%r12d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB23_6:
	movq	8(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, %rdx
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	%r15d, %r8d
	movl	%ebp, %r9d
	pushq	%rbx
.Lcfi231:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi232:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$16, %rsp
.Lcfi233:
	.cfi_adjust_cfa_offset -16
	movq	8(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	16(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	4(%rsp), %ecx           # 4-byte Reload
	movl	12(%rsp), %r8d          # 4-byte Reload
	movl	8(%rsp), %r9d           # 4-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi234:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi235:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$56, %rsp
.Lcfi236:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB23_18
.Lfunc_end23:
	.size	mem_true24_copy_color, .Lfunc_end23-mem_true24_copy_color
	.cfi_endproc

	.globl	mem_true32_fill_rectangle
	.p2align	4, 0x90
	.type	mem_true32_fill_rectangle,@function
mem_true32_fill_rectangle:              # @mem_true32_fill_rectangle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi237:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi238:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi239:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi240:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi241:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi242:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi243:
	.cfi_def_cfa_offset 80
.Lcfi244:
	.cfi_offset %rbx, -56
.Lcfi245:
	.cfi_offset %r12, -48
.Lcfi246:
	.cfi_offset %r13, -40
.Lcfi247:
	.cfi_offset %r14, -32
.Lcfi248:
	.cfi_offset %r15, -24
.Lcfi249:
	.cfi_offset %rbp, -16
	movq	%r9, %rbx
	movl	%r8d, %r14d
	movl	%ecx, %r15d
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	movq	176(%r12), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	je	.LBB24_1
# BB#6:
	leal	(,%rsi,4), %r13d
	leal	(,%r15,4), %ecx
	movl	$1, %r9d
	movq	%r12, %rdi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movl	%r13d, %esi
	movq	%rdx, %rbp
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r14d, %r8d
	callq	*%rax
	testl	%eax, %eax
	jns	.LBB24_2
# BB#7:
	movq	16(%rsp), %rsi          # 8-byte Reload
	cmpl	$-2, %eax
	je	.LBB24_10
# BB#8:
	cmpl	$-1, %eax
	movq	%rbp, %rdx
	jne	.LBB24_27
# BB#9:
	movl	%r15d, %ebp
	sarl	%ebp
	leal	(%rbp,%rsi), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	subl	%ebp, %r15d
	movl	%edx, %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movl	%r14d, %r13d
	jmp	.LBB24_11
.LBB24_1:                               # %.mem_fill_recover.exit_crit_edge
	movq	%rdx, %rbp
	shll	$2, %esi
	movl	%esi, %r13d
.LBB24_2:                               # %mem_fill_recover.exit
	testl	%r14d, %r14d
	jle	.LBB24_26
# BB#3:                                 # %.lr.ph.preheader
	movslq	%r13d, %rax
	movslq	%ebp, %rcx
	shlq	$3, %rcx
	addq	168(%r12), %rcx
	movl	%r15d, %edx
	notl	%edx
	cmpl	$-3, %edx
	movl	$-2, %esi
	cmovgl	%edx, %esi
	leal	1(%rsi,%r15), %edx
	incq	%rdx
	movabsq	$8589934588, %r11       # imm = 0x1FFFFFFFC
	andq	%rdx, %r11
	leaq	-4(%r11), %r12
	movl	%r12d, %r13d
	shrl	$2, %r13d
	incl	%r13d
	movl	%r15d, %esi
	subl	%r11d, %esi
	movl	%esi, 4(%rsp)           # 4-byte Spill
	movd	%rbx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	andl	$7, %r13d
	leaq	16(%rax), %rsi
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%r13, %r8
	negq	%r8
	leaq	240(%rax), %rsi
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB24_4:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB24_17 Depth 2
                                        #     Child Loop BB24_20 Depth 2
                                        #     Child Loop BB24_24 Depth 2
	movq	(%rcx), %rdi
	leaq	(%rdi,%rax), %rbp
	cmpq	$4, %rdx
	jae	.LBB24_12
# BB#5:                                 #   in Loop: Header=BB24_4 Depth=1
	movl	%r15d, %esi
	jmp	.LBB24_23
	.p2align	4, 0x90
.LBB24_12:                              # %min.iters.checked
                                        #   in Loop: Header=BB24_4 Depth=1
	testq	%r11, %r11
	je	.LBB24_13
# BB#14:                                # %vector.ph
                                        #   in Loop: Header=BB24_4 Depth=1
	testq	%r13, %r13
	je	.LBB24_15
# BB#16:                                # %vector.body.prol.preheader
                                        #   in Loop: Header=BB24_4 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	leaq	(%rdi,%rsi), %rsi
	movq	%r8, %r10
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB24_17:                              # %vector.body.prol
                                        #   Parent Loop BB24_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -16(%rsi,%r9,8)
	movdqu	%xmm0, (%rsi,%r9,8)
	addq	$4, %r9
	incq	%r10
	jne	.LBB24_17
	jmp	.LBB24_18
.LBB24_13:                              #   in Loop: Header=BB24_4 Depth=1
	movl	%r15d, %esi
	jmp	.LBB24_23
.LBB24_15:                              #   in Loop: Header=BB24_4 Depth=1
	xorl	%r9d, %r9d
.LBB24_18:                              # %vector.body.prol.loopexit
                                        #   in Loop: Header=BB24_4 Depth=1
	cmpq	$28, %r12
	jb	.LBB24_21
# BB#19:                                # %vector.ph.new
                                        #   in Loop: Header=BB24_4 Depth=1
	movq	%r11, %rsi
	subq	%r9, %rsi
	addq	8(%rsp), %rdi           # 8-byte Folded Reload
	leaq	(%rdi,%r9,8), %rdi
	.p2align	4, 0x90
.LBB24_20:                              # %vector.body
                                        #   Parent Loop BB24_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movdqu	%xmm0, -240(%rdi)
	movdqu	%xmm0, -224(%rdi)
	movdqu	%xmm0, -208(%rdi)
	movdqu	%xmm0, -192(%rdi)
	movdqu	%xmm0, -176(%rdi)
	movdqu	%xmm0, -160(%rdi)
	movdqu	%xmm0, -144(%rdi)
	movdqu	%xmm0, -128(%rdi)
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rsi
	jne	.LBB24_20
.LBB24_21:                              # %middle.block
                                        #   in Loop: Header=BB24_4 Depth=1
	cmpq	%r11, %rdx
	je	.LBB24_25
# BB#22:                                #   in Loop: Header=BB24_4 Depth=1
	leaq	(%rbp,%r11,8), %rbp
	movl	4(%rsp), %esi           # 4-byte Reload
	.p2align	4, 0x90
.LBB24_23:                              # %scalar.ph.preheader
                                        #   in Loop: Header=BB24_4 Depth=1
	incl	%esi
	.p2align	4, 0x90
.LBB24_24:                              # %scalar.ph
                                        #   Parent Loop BB24_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, (%rbp)
	addq	$8, %rbp
	decl	%esi
	cmpl	$1, %esi
	jg	.LBB24_24
.LBB24_25:                              # %.loopexit
                                        #   in Loop: Header=BB24_4 Depth=1
	addq	$8, %rcx
	cmpl	$1, %r14d
	leal	-1(%r14), %esi
	movl	%esi, %r14d
	jg	.LBB24_4
.LBB24_26:
	xorl	%eax, %eax
.LBB24_27:                              # %mem_fill_recover.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_10:
	movl	%r14d, %r13d
	sarl	%r13d
	movq	%rbp, %rdx
	leal	(%r13,%rdx), %eax
	movl	%eax, 8(%rsp)           # 4-byte Spill
	subl	%r13d, %r14d
	movl	%esi, %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movl	%r15d, %ebp
.LBB24_11:
	movq	8(%r12), %rax
	movq	%r12, %rdi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	movl	%r15d, %ecx
	movl	%r14d, %r8d
	movq	%rbx, %r9
	callq	*56(%rax)
	movq	8(%r12), %rax
	movq	56(%rax), %rax
	movq	%r12, %rdi
	movl	4(%rsp), %esi           # 4-byte Reload
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	%ebp, %ecx
	movl	%r13d, %r8d
	movq	%rbx, %r9
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*%rax                   # TAILCALL
.Lfunc_end24:
	.size	mem_true32_fill_rectangle, .Lfunc_end24-mem_true32_fill_rectangle
	.cfi_endproc

	.globl	mem_true32_copy_mono
	.p2align	4, 0x90
	.type	mem_true32_copy_mono,@function
mem_true32_copy_mono:                   # @mem_true32_copy_mono
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi250:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi251:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi252:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi253:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi254:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi255:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi256:
	.cfi_def_cfa_offset 128
.Lcfi257:
	.cfi_offset %rbx, -56
.Lcfi258:
	.cfi_offset %r12, -48
.Lcfi259:
	.cfi_offset %r13, -40
.Lcfi260:
	.cfi_offset %r14, -32
.Lcfi261:
	.cfi_offset %r15, -24
.Lcfi262:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebx
                                        # kill: %R8D<def> %R8D<kill> %R8<def>
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, %r10d
	movq	%rsi, %r13
	movq	152(%rsp), %r15
	movq	144(%rsp), %r12
	movl	136(%rsp), %r14d
	movl	128(%rsp), %edx
	movq	176(%rdi), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	je	.LBB25_1
# BB#10:
	leal	(,%r8,4), %ebp
	leal	(,%rdx,4), %ecx
	movl	$1, %r9d
	movl	%ebp, %esi
	movl	%ebx, %edx
	movq	%r8, 64(%rsp)           # 8-byte Spill
	movl	%r14d, %r8d
	movl	%r10d, %ebx
	callq	*%rax
	movl	%ebx, %r10d
	testl	%eax, %eax
	jns	.LBB25_2
# BB#11:
	movq	64(%rsp), %r8           # 8-byte Reload
	cmpl	$-2, %eax
	je	.LBB25_14
# BB#12:
	cmpl	$-1, %eax
	jne	.LBB25_32
# BB#13:
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	128(%rsp), %ebx
	movq	%rbx, %rax
	movl	%eax, %edx
	sarl	%edx
	leal	(%rdx,%r8), %esi
	movl	%esi, 20(%rsp)          # 4-byte Spill
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	subl	%edx, %eax
	movq	32(%rsp), %r9           # 8-byte Reload
	movl	%r9d, %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movl	%r14d, %edx
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	jmp	.LBB25_15
.LBB25_1:                               # %.mem_copy_mono_recover.exit_crit_edge
	shll	$2, %r8d
	movl	%r8d, %ebp
.LBB25_2:                               # %mem_copy_mono_recover.exit
	movl	%r10d, %ecx
	andb	$7, %cl
	movl	$128, %r9d
	shrl	%cl, %r9d
	xorl	%eax, %eax
	testl	%r14d, %r14d
	jle	.LBB25_32
# BB#3:                                 # %.lr.ph
	movslq	%ebp, %r8
	movslq	32(%rsp), %r11          # 4-byte Folded Reload
	shlq	$3, %r11
	movq	56(%rsp), %rcx          # 8-byte Reload
	addq	168(%rcx), %r11
	sarl	$3, %r10d
	movslq	%r10d, %rcx
	addq	%rcx, %r13
	decl	%r14d
	cmpq	$-1, %r15
	movslq	12(%rsp), %r10          # 4-byte Folded Reload
	je	.LBB25_16
# BB#4:                                 # %.lr.ph.split.preheader
	movq	24(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB25_5:                               # %.lr.ph.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_7 Depth 2
                                        #     Child Loop BB25_26 Depth 2
	movq	(%r11), %rbp
	addq	%r8, %rbp
	leaq	1(%r13), %rcx
	cmpq	$-1, %r12
	movzbl	(%r13), %edx
	je	.LBB25_25
# BB#6:                                 # %.split.split.preheader
                                        #   in Loop: Header=BB25_5 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %edi
	movl	%r9d, %ebx
	.p2align	4, 0x90
.LBB25_7:                               # %.split.split
                                        #   Parent Loop BB25_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %ebx
	movq	%r15, %rsi
	cmoveq	%r12, %rsi
	movq	%rsi, (%rbp)
	sarl	%ebx
	jne	.LBB25_9
# BB#8:                                 #   in Loop: Header=BB25_7 Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movl	$128, %ebx
.LBB25_9:                               #   in Loop: Header=BB25_7 Depth=2
	addq	$8, %rbp
	decl	%edi
	cmpl	$1, %edi
	jg	.LBB25_7
	jmp	.LBB25_31
	.p2align	4, 0x90
.LBB25_25:                              # %.split.split.us.preheader
                                        #   in Loop: Header=BB25_5 Depth=1
	movq	24(%rsp), %rsi          # 8-byte Reload
	movl	%esi, %edi
	movl	%r9d, %ebx
	.p2align	4, 0x90
.LBB25_26:                              # %.split.split.us
                                        #   Parent Loop BB25_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %ebx
	je	.LBB25_28
# BB#27:                                #   in Loop: Header=BB25_26 Depth=2
	movq	%r15, (%rbp)
.LBB25_28:                              #   in Loop: Header=BB25_26 Depth=2
	sarl	%ebx
	jne	.LBB25_30
# BB#29:                                #   in Loop: Header=BB25_26 Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movl	$128, %ebx
.LBB25_30:                              #   in Loop: Header=BB25_26 Depth=2
	addq	$8, %rbp
	decl	%edi
	cmpl	$1, %edi
	jg	.LBB25_26
.LBB25_31:                              # %.us-lcssa
                                        #   in Loop: Header=BB25_5 Depth=1
	addq	%r10, %r13
	addq	$8, %r11
	testl	%r14d, %r14d
	leal	-1(%r14), %ecx
	movl	%ecx, %r14d
	jg	.LBB25_5
	jmp	.LBB25_32
.LBB25_16:                              # %.lr.ph.split.us.preheader
	movl	128(%rsp), %esi
	incl	%esi
	.p2align	4, 0x90
.LBB25_17:                              # %.lr.ph.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB25_19 Depth 2
	cmpq	$-1, %r12
	je	.LBB25_24
# BB#18:                                # %.split.us..split.us.split_crit_edge.us.preheader
                                        #   in Loop: Header=BB25_17 Depth=1
	movzbl	(%r13), %edx
	leaq	1(%r13), %rcx
	movq	(%r11), %rbp
	addq	%r8, %rbp
	movl	%esi, %edi
	movl	%r9d, %ebx
	.p2align	4, 0x90
.LBB25_19:                              # %.split.us..split.us.split_crit_edge.us
                                        #   Parent Loop BB25_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%edx, %ebx
	jne	.LBB25_21
# BB#20:                                #   in Loop: Header=BB25_19 Depth=2
	movq	%r12, (%rbp)
.LBB25_21:                              #   in Loop: Header=BB25_19 Depth=2
	sarl	%ebx
	jne	.LBB25_23
# BB#22:                                #   in Loop: Header=BB25_19 Depth=2
	movzbl	(%rcx), %edx
	incq	%rcx
	movl	$128, %ebx
.LBB25_23:                              #   in Loop: Header=BB25_19 Depth=2
	addq	$8, %rbp
	decl	%edi
	cmpl	$1, %edi
	jg	.LBB25_19
.LBB25_24:                              # %.us-lcssa.us.us
                                        #   in Loop: Header=BB25_17 Depth=1
	addq	%r10, %r13
	addq	$8, %r11
	testl	%r14d, %r14d
	leal	-1(%r14), %ecx
	movl	%ecx, %r14d
	jg	.LBB25_17
.LBB25_32:                              # %mem_copy_mono_recover.exit.thread
	addq	$72, %rsp
.LBB25_33:                              # %mem_copy_mono_recover.exit.thread
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB25_14:
	movl	12(%rsp), %ebp          # 4-byte Reload
	movl	%r14d, %eax
	sarl	%eax
	movq	32(%rsp), %r9           # 8-byte Reload
	leal	(%rax,%r9), %edx
	movl	%edx, 16(%rsp)          # 4-byte Spill
	movq	%rax, 40(%rsp)          # 8-byte Spill
	subl	%eax, %r14d
	movl	%r8d, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	128(%rsp), %ebx
	movq	%rbx, %rax
	movl	%eax, %edx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
.LBB25_15:
	movq	56(%rsp), %rbx          # 8-byte Reload
	movq	8(%rbx), %r11
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r10d, %edx
	movl	%ebp, %ecx
                                        # kill: %R8D<def> %R8D<kill> %R8<kill>
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	pushq	%r15
.Lcfi263:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi264:
	.cfi_adjust_cfa_offset 8
	pushq	%r14
.Lcfi265:
	.cfi_adjust_cfa_offset 8
	pushq	%rax
.Lcfi266:
	.cfi_adjust_cfa_offset 8
	movl	%r10d, %r14d
	callq	*72(%r11)
	addq	$32, %rsp
.Lcfi267:
	.cfi_adjust_cfa_offset -32
	movq	8(%rbx), %rax
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movl	%r14d, %edx
	movl	%ebp, %ecx
	movl	20(%rsp), %r8d          # 4-byte Reload
	movl	16(%rsp), %r9d          # 4-byte Reload
	pushq	%r15
.Lcfi268:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi269:
	.cfi_adjust_cfa_offset 8
	pushq	56(%rsp)                # 8-byte Folded Reload
.Lcfi270:
	.cfi_adjust_cfa_offset 8
	pushq	72(%rsp)                # 8-byte Folded Reload
.Lcfi271:
	.cfi_adjust_cfa_offset 8
	callq	*72(%rax)
	addq	$104, %rsp
.Lcfi272:
	.cfi_adjust_cfa_offset -32
	jmp	.LBB25_33
.Lfunc_end25:
	.size	mem_true32_copy_mono, .Lfunc_end25-mem_true32_copy_mono
	.cfi_endproc

	.globl	mem_true32_copy_color
	.p2align	4, 0x90
	.type	mem_true32_copy_color,@function
mem_true32_copy_color:                  # @mem_true32_copy_color
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi273:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi274:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi275:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi276:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi277:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi278:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi279:
	.cfi_def_cfa_offset 96
.Lcfi280:
	.cfi_offset %rbx, -56
.Lcfi281:
	.cfi_offset %r12, -48
.Lcfi282:
	.cfi_offset %r13, -40
.Lcfi283:
	.cfi_offset %r14, -32
.Lcfi284:
	.cfi_offset %r15, -24
.Lcfi285:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movl	%r8d, %r15d
	movl	%ecx, 12(%rsp)          # 4-byte Spill
	movl	%edx, 8(%rsp)           # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r13
	movl	104(%rsp), %ebx
	movl	96(%rsp), %r12d
	movq	176(%r13), %rax
	movl	$mem_no_fault_proc, %ecx
	cmpq	%rcx, %rax
	je	.LBB26_7
# BB#1:
	leal	(,%r15,4), %esi
	leal	(,%r12,4), %ecx
	movl	$1, %r9d
	movq	%r13, %rdi
	movl	%ebp, %edx
	movl	%ebx, %r8d
	callq	*%rax
	testl	%eax, %eax
	js	.LBB26_2
.LBB26_7:                               # %mem_copy_color_recover.exit
	testl	%r12d, %r12d
	jle	.LBB26_16
# BB#8:                                 # %mem_copy_color_recover.exit
	testl	%ebx, %ebx
	jle	.LBB26_16
# BB#9:
	movl	$-1, %eax
	testl	%r15d, %r15d
	js	.LBB26_17
# BB#10:
	testl	%ebp, %ebp
	js	.LBB26_17
# BB#11:
	movl	24(%r13), %ecx
	subl	%r12d, %ecx
	cmpl	%r15d, %ecx
	jl	.LBB26_17
# BB#12:
	movl	28(%r13), %ecx
	subl	%ebx, %ecx
	cmpl	%ebp, %ecx
	jl	.LBB26_17
# BB#13:
	testl	%ebx, %ebx
	jle	.LBB26_16
# BB#14:                                # %.lr.ph.i
	shll	$2, %r15d
	movslq	%r15d, %r15
	shll	$2, %r12d
	movl	8(%rsp), %eax           # 4-byte Reload
	shll	$2, %eax
	cltq
	addq	%rax, %r14
	movslq	%ebp, %rbp
	shlq	$3, %rbp
	addq	168(%r13), %rbp
	movslq	%r12d, %r12
	movslq	12(%rsp), %r13          # 4-byte Folded Reload
	incl	%ebx
	.p2align	4, 0x90
.LBB26_15:                              # =>This Inner Loop Header: Depth=1
	movq	(%rbp), %rdi
	addq	%r15, %rdi
	movq	%r14, %rsi
	movq	%r12, %rdx
	callq	memcpy
	addq	%r13, %r14
	addq	$8, %rbp
	decl	%ebx
	cmpl	$1, %ebx
	jg	.LBB26_15
.LBB26_16:
	xorl	%eax, %eax
.LBB26_17:                              # %copy_byte_rect.exit
	addq	$40, %rsp
.LBB26_18:                              # %copy_byte_rect.exit
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB26_2:
	cmpl	$-2, %eax
	je	.LBB26_5
# BB#3:
	cmpl	$-1, %eax
	jne	.LBB26_17
# BB#4:
	movl	%r12d, %eax
	sarl	%eax
	leal	(%rax,%r15), %ecx
	movl	%ecx, 20(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	subl	%eax, %r12d
	movl	%ebp, %eax
	movl	%eax, 16(%rsp)          # 4-byte Spill
	movl	%ebx, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	jmp	.LBB26_6
.LBB26_5:
	movl	%ebx, %eax
	sarl	%eax
	leal	(%rax,%rbp), %ecx
	movl	%ecx, 16(%rsp)          # 4-byte Spill
	movq	%rax, 24(%rsp)          # 8-byte Spill
	subl	%eax, %ebx
	movl	%r15d, %eax
	movl	%eax, 20(%rsp)          # 4-byte Spill
	movl	%r12d, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
.LBB26_6:
	movq	8(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	%r15d, %r8d
	movl	%ebp, %r9d
	pushq	%rbx
.Lcfi286:
	.cfi_adjust_cfa_offset 8
	pushq	%r12
.Lcfi287:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$16, %rsp
.Lcfi288:
	.cfi_adjust_cfa_offset -16
	movq	8(%r13), %rax
	movq	%r13, %rdi
	movq	%r14, %rsi
	movl	8(%rsp), %edx           # 4-byte Reload
	movl	12(%rsp), %ecx          # 4-byte Reload
	movl	20(%rsp), %r8d          # 4-byte Reload
	movl	16(%rsp), %r9d          # 4-byte Reload
	pushq	24(%rsp)                # 8-byte Folded Reload
.Lcfi289:
	.cfi_adjust_cfa_offset 8
	pushq	40(%rsp)                # 8-byte Folded Reload
.Lcfi290:
	.cfi_adjust_cfa_offset 8
	callq	*80(%rax)
	addq	$56, %rsp
.Lcfi291:
	.cfi_adjust_cfa_offset -16
	jmp	.LBB26_18
.Lfunc_end26:
	.size	mem_true32_copy_color, .Lfunc_end26-mem_true32_copy_color
	.cfi_endproc

	.type	mem_mono_procs,@object  # @mem_mono_procs
	.data
	.globl	mem_mono_procs
	.p2align	3
mem_mono_procs:
	.quad	mem_open
	.quad	mem_get_initial_matrix
	.quad	gx_default_sync_output
	.quad	gx_default_output_page
	.quad	gx_default_close_device
	.quad	gx_default_map_rgb_color
	.quad	gx_default_map_color_rgb
	.quad	mem_mono_fill_rectangle
	.quad	gx_default_tile_rectangle
	.quad	mem_mono_copy_mono
	.quad	mem_mono_copy_color
	.quad	gx_default_draw_line
	.quad	gx_default_fill_trapezoid
	.quad	gx_default_tile_trapezoid
	.size	mem_mono_procs, 112

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"image(mono)"
	.size	.L.str.1, 12

	.type	mem_mono_device,@object # @mem_mono_device
	.data
	.globl	mem_mono_device
	.p2align	3
mem_mono_device:
	.long	200                     # 0xc8
	.zero	4
	.quad	mem_mono_procs
	.quad	.L.str.1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	0                       # 0x0
	.short	1                       # 0x1
	.zero	2
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.quad	0
	.quad	mem_no_fault_proc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	0
	.size	mem_mono_device, 200

	.type	mem_mapped_procs,@object # @mem_mapped_procs
	.globl	mem_mapped_procs
	.p2align	3
mem_mapped_procs:
	.quad	mem_open
	.quad	mem_get_initial_matrix
	.quad	gx_default_sync_output
	.quad	gx_default_output_page
	.quad	gx_default_close_device
	.quad	mem_mapped_map_rgb_color
	.quad	mem_mapped_map_color_rgb
	.quad	mem_mapped_fill_rectangle
	.quad	gx_default_tile_rectangle
	.quad	mem_mapped_copy_mono
	.quad	mem_mapped_copy_color
	.quad	gx_default_draw_line
	.quad	gx_default_fill_trapezoid
	.quad	gx_default_tile_trapezoid
	.size	mem_mapped_procs, 112

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"image(8)"
	.size	.L.str.2, 9

	.type	mem_mapped_color_device,@object # @mem_mapped_color_device
	.data
	.globl	mem_mapped_color_device
	.p2align	3
mem_mapped_color_device:
	.long	200                     # 0xc8
	.zero	4
	.quad	mem_mapped_procs
	.quad	.L.str.2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1                       # 0x1
	.short	255                     # 0xff
	.zero	2
	.long	8                       # 0x8
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.quad	0
	.quad	mem_no_fault_proc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	0
	.size	mem_mapped_color_device, 200

	.type	mem_true24_procs,@object # @mem_true24_procs
	.globl	mem_true24_procs
	.p2align	3
mem_true24_procs:
	.quad	mem_open
	.quad	mem_get_initial_matrix
	.quad	gx_default_sync_output
	.quad	gx_default_output_page
	.quad	gx_default_close_device
	.quad	mem_true_map_rgb_color
	.quad	mem_true_map_color_rgb
	.quad	mem_true24_fill_rectangle
	.quad	gx_default_tile_rectangle
	.quad	mem_true24_copy_mono
	.quad	mem_true24_copy_color
	.quad	gx_default_draw_line
	.quad	gx_default_fill_trapezoid
	.quad	gx_default_tile_trapezoid
	.size	mem_true24_procs, 112

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"image(24)"
	.size	.L.str.3, 10

	.type	mem_true24_color_device,@object # @mem_true24_color_device
	.data
	.globl	mem_true24_color_device
	.p2align	3
mem_true24_color_device:
	.long	200                     # 0xc8
	.zero	4
	.quad	mem_true24_procs
	.quad	.L.str.3
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1                       # 0x1
	.short	255                     # 0xff
	.zero	2
	.long	24                      # 0x18
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.quad	0
	.quad	mem_no_fault_proc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	0
	.size	mem_true24_color_device, 200

	.type	mem_true32_procs,@object # @mem_true32_procs
	.globl	mem_true32_procs
	.p2align	3
mem_true32_procs:
	.quad	mem_open
	.quad	mem_get_initial_matrix
	.quad	gx_default_sync_output
	.quad	gx_default_output_page
	.quad	gx_default_close_device
	.quad	mem_true_map_rgb_color
	.quad	mem_true_map_color_rgb
	.quad	mem_true32_fill_rectangle
	.quad	gx_default_tile_rectangle
	.quad	mem_true32_copy_mono
	.quad	mem_true32_copy_color
	.quad	gx_default_draw_line
	.quad	gx_default_fill_trapezoid
	.quad	gx_default_tile_trapezoid
	.size	mem_true32_procs, 112

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"image(32)"
	.size	.L.str.4, 10

	.type	mem_true32_color_device,@object # @mem_true32_color_device
	.data
	.globl	mem_true32_color_device
	.p2align	3
mem_true32_color_device:
	.long	200                     # 0xc8
	.zero	4
	.quad	mem_true32_procs
	.quad	.L.str.4
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.long	1                       # 0x1
	.short	255                     # 0xff
	.zero	2
	.long	32                      # 0x20
	.long	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	1065353216              # float 1
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # float 0
	.zero	4
	.quad	0                       # 0x0
	.long	0                       # 0x0
	.zero	4
	.quad	0
	.quad	0
	.quad	mem_no_fault_proc
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	0
	.size	mem_true32_color_device, 200


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
