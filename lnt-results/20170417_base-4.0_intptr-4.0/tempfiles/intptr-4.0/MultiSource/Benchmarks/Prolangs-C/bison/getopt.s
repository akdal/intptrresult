	.text
	.file	"getopt.bc"
	.globl	getopt
	.p2align	4, 0x90
	.type	getopt,@function
getopt:                                 # @getopt
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movl	optind(%rip), %ebx
	testl	%ebx, %ebx
	je	.LBB0_1
# BB#4:
	movq	nextchar(%rip), %rbp
	testq	%rbp, %rbp
	je	.LBB0_6
# BB#5:
	cmpb	$0, (%rbp)
	je	.LBB0_6
.LBB0_38:
	leaq	1(%rbp), %r12
	movq	%r12, nextchar(%rip)
	movsbl	(%rbp), %r13d
	movq	%rdx, %rdi
	movl	%r13d, %esi
	callq	strchr
	cmpb	$0, 1(%rbp)
	jne	.LBB0_40
# BB#39:
	incl	%ebx
	movl	%ebx, optind(%rip)
.LBB0_40:
	cmpb	$58, %r13b
	je	.LBB0_42
# BB#41:
	testq	%rax, %rax
	je	.LBB0_42
# BB#49:
	cmpb	$58, 1(%rax)
	jne	.LBB0_62
# BB#50:
	movb	(%r12), %cl
	cmpb	$58, 2(%rax)
	jne	.LBB0_54
# BB#51:
	testb	%cl, %cl
	jne	.LBB0_52
# BB#53:
	movq	$0, optarg(%rip)
	jmp	.LBB0_61
.LBB0_6:                                # %thread-pre-split
	movl	ordering(%rip), %esi
	cmpl	$1, %esi
	je	.LBB0_8
	jmp	.LBB0_19
.LBB0_1:
	movl	$1, optind(%rip)
	movl	$1, last_nonopt(%rip)
	movl	$1, first_nonopt(%rip)
	movq	$0, nextchar(%rip)
	movl	$2, %esi
	cmpb	$45, (%rdx)
	je	.LBB0_3
# BB#2:
	movl	$.L.str, %edi
	movq	%rdx, %rbx
	callq	getenv
	movq	%rbx, %rdx
	xorl	%esi, %esi
	testq	%rax, %rax
	sete	%sil
.LBB0_3:                                # %.thread69
	movl	%esi, ordering(%rip)
	movl	$1, %ebx
	cmpl	$1, %esi
	jne	.LBB0_19
.LBB0_8:
	movslq	first_nonopt(%rip), %r12
	movl	last_nonopt(%rip), %ebp
	cmpl	%ebp, %r12d
	je	.LBB0_11
# BB#9:
	movl	%ebx, %r13d
	subl	%ebp, %r13d
	je	.LBB0_11
# BB#10:
	movslq	%ebp, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	%ebp, %eax
	subl	%r12d, %eax
	shll	$3, %eax
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	movl	%esi, 8(%rsp)           # 4-byte Spill
	callq	malloc
	movq	%rax, (%rsp)            # 8-byte Spill
	leaq	(%r14,%r12,8), %rsi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	(%r14,%rax,8), %rsi
	movslq	%r13d, %rdx
	shlq	$3, %rdx
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	memcpy
	subl	%ebp, %r12d
	movl	optind(%rip), %eax
	addl	%r12d, %eax
	cltq
	leaq	(%r14,%rax,8), %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
	movq	%rbx, %rdx
	callq	memcpy
	movl	8(%rsp), %esi           # 4-byte Reload
	movq	16(%rsp), %rdx          # 8-byte Reload
	movl	optind(%rip), %ebx
	addl	%ebx, %r12d
	movl	%r12d, first_nonopt(%rip)
	movl	%ebx, last_nonopt(%rip)
	cmpl	%r15d, %ebx
	jl	.LBB0_14
	jmp	.LBB0_18
.LBB0_11:
	cmpl	%ebp, %ebx
	je	.LBB0_13
# BB#12:
	movl	%ebx, first_nonopt(%rip)
.LBB0_13:                               # %.preheader
	cmpl	%r15d, %ebx
	jge	.LBB0_18
.LBB0_14:                               # %.lr.ph.preheader
	movslq	%ebx, %rbx
	.p2align	4, 0x90
.LBB0_15:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r14,%rbx,8), %rax
	cmpb	$45, (%rax)
	jne	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=1
	cmpb	$0, 1(%rax)
	jne	.LBB0_18
.LBB0_17:                               # %.thread
                                        #   in Loop: Header=BB0_15 Depth=1
	incq	%rbx
	movl	%ebx, optind(%rip)
	cmpl	%r15d, %ebx
	jl	.LBB0_15
.LBB0_18:                               # %.loopexit
	movl	%ebx, last_nonopt(%rip)
.LBB0_19:
	cmpl	%r15d, %ebx
	je	.LBB0_30
# BB#20:
	movslq	%ebx, %rax
	movq	(%r14,%rax,8), %rcx
	cmpb	$45, (%rcx)
	jne	.LBB0_29
# BB#21:
	cmpb	$45, 1(%rcx)
	jne	.LBB0_29
# BB#22:
	cmpb	$0, 2(%rcx)
	je	.LBB0_23
.LBB0_29:                               # %.thread70
	cmpl	%r15d, %ebx
	jne	.LBB0_32
.LBB0_30:                               # %.thread72
	movl	first_nonopt(%rip), %eax
	movl	$-1, %r13d
	cmpl	last_nonopt(%rip), %eax
	je	.LBB0_62
# BB#31:
	movl	%eax, optind(%rip)
	jmp	.LBB0_62
.LBB0_32:
	movq	(%r14,%rax,8), %rbp
	cmpb	$45, (%rbp)
	jne	.LBB0_34
# BB#33:
	cmpb	$0, 1(%rbp)
	je	.LBB0_34
# BB#37:
	incq	%rbp
	movq	%rbp, nextchar(%rip)
	jmp	.LBB0_38
.LBB0_42:
	cmpl	$0, opterr(%rip)
	jne	.LBB0_44
# BB#43:
	movl	$63, %r13d
	jmp	.LBB0_62
.LBB0_34:
	testl	%esi, %esi
	je	.LBB0_35
# BB#36:
	incl	%ebx
	movl	%ebx, optind(%rip)
	movq	%rbp, optarg(%rip)
	xorl	%r13d, %r13d
	jmp	.LBB0_62
.LBB0_35:
	movl	$-1, %r13d
	jmp	.LBB0_62
.LBB0_54:
	testb	%cl, %cl
	je	.LBB0_55
.LBB0_52:
	movq	%r12, optarg(%rip)
	incl	%ebx
	movl	%ebx, optind(%rip)
	jmp	.LBB0_61
.LBB0_23:
	incl	%ebx
	movl	%ebx, optind(%rip)
	movslq	first_nonopt(%rip), %r13
	movl	last_nonopt(%rip), %ebp
	cmpl	%ebp, %r13d
	je	.LBB0_26
# BB#24:
	movl	%ebx, %r12d
	subl	%ebp, %r12d
	je	.LBB0_26
# BB#25:
	movslq	%ebp, %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movl	%ebp, %eax
	subl	%r13d, %eax
	shll	$3, %eax
	movslq	%eax, %rbx
	movq	%rbx, %rdi
	callq	malloc
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leaq	(%r14,%r13,8), %rsi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movq	%rax, %rdi
	movq	%rbx, %rdx
	callq	memcpy
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	(%r14,%rax,8), %rsi
	movslq	%r12d, %rdx
	shlq	$3, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	callq	memcpy
	subl	%ebp, %r13d
	movl	optind(%rip), %eax
	addl	%r13d, %eax
	cltq
	leaq	(%r14,%rax,8), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	%rbx, %rdx
	callq	memcpy
	movl	optind(%rip), %eax
	addl	%eax, %r13d
	movl	%r13d, first_nonopt(%rip)
	movl	%eax, last_nonopt(%rip)
	jmp	.LBB0_28
.LBB0_26:
	cmpl	%ebp, %r13d
	jne	.LBB0_28
# BB#27:
	movl	%ebx, first_nonopt(%rip)
.LBB0_28:
	movl	%r15d, last_nonopt(%rip)
	movl	%r15d, optind(%rip)
	jmp	.LBB0_30
.LBB0_55:
	cmpl	%r15d, %ebx
	jne	.LBB0_59
# BB#56:
	cmpl	$0, opterr(%rip)
	jne	.LBB0_58
# BB#57:
	xorl	%ebx, %ebx
	jmp	.LBB0_60
.LBB0_44:
	cmpb	$32, %r13b
	movq	stderr(%rip), %rdi
	movq	(%r14), %rdx
	jl	.LBB0_46
# BB#45:
	cmpb	$127, %r13b
	je	.LBB0_46
# BB#48:
	movl	$.L.str.3, %esi
	jmp	.LBB0_47
.LBB0_59:
	leal	1(%rbx), %eax
	movl	%eax, optind(%rip)
	movslq	%ebx, %rax
	movq	(%r14,%rax,8), %rbx
.LBB0_60:
	movq	%rbx, optarg(%rip)
.LBB0_61:
	movq	$0, nextchar(%rip)
.LBB0_62:
	movl	%r13d, %eax
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_46:
	movl	$.L.str.2, %esi
.LBB0_47:
	xorl	%eax, %eax
	movl	%r13d, %ecx
	callq	fprintf
	movl	$63, %r13d
	jmp	.LBB0_62
.LBB0_58:
	movq	stderr(%rip), %rdi
	movq	(%r14), %rdx
	xorl	%ebx, %ebx
	movl	$.L.str.4, %esi
	xorl	%eax, %eax
	movl	%r13d, %ecx
	callq	fprintf
	jmp	.LBB0_60
.Lfunc_end0:
	.size	getopt, .Lfunc_end0-getopt
	.cfi_endproc

	.type	optarg,@object          # @optarg
	.bss
	.globl	optarg
	.p2align	3
optarg:
	.quad	0
	.size	optarg, 8

	.type	optind,@object          # @optind
	.globl	optind
	.p2align	2
optind:
	.long	0                       # 0x0
	.size	optind, 4

	.type	opterr,@object          # @opterr
	.data
	.globl	opterr
	.p2align	2
opterr:
	.long	1                       # 0x1
	.size	opterr, 4

	.type	last_nonopt,@object     # @last_nonopt
	.local	last_nonopt
	.comm	last_nonopt,4,4
	.type	first_nonopt,@object    # @first_nonopt
	.local	first_nonopt
	.comm	first_nonopt,4,4
	.type	nextchar,@object        # @nextchar
	.local	nextchar
	.comm	nextchar,8,8
	.type	ordering,@object        # @ordering
	.local	ordering
	.comm	ordering,4,4
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"_POSIX_OPTION_ORDER"
	.size	.L.str, 20

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"--"
	.size	.L.str.1, 3

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"%s: unrecognized option, character code 0%o\n"
	.size	.L.str.2, 45

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"%s: unrecognized option `-%c'\n"
	.size	.L.str.3, 31

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"%s: no argument for `-%c' option\n"
	.size	.L.str.4, 34


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
