	.text
	.file	"z28.bc"
	.globl	ErrorInit
	.p2align	4, 0x90
	.type	ErrorInit,@function
ErrorInit:                              # @ErrorInit
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	cmpq	$0, fp(%rip)
	je	.LBB0_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$28, %edi
	movl	$1, %esi
	movl	$.L.str, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	callq	Error
.LBB0_2:
	movl	$.L.str.1, %esi
	movq	%rbx, %rdi
	callq	fopen
	movq	%rax, fp(%rip)
	testq	%rax, %rax
	je	.LBB0_4
# BB#3:
	popq	%rbx
	retq
.LBB0_4:
	movq	no_fpos(%rip), %r8
	movl	$28, %edi
	movl	$2, %esi
	movl	$.L.str.2, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%rbx, %r9
	popq	%rbx
	jmp	Error                   # TAILCALL
.Lfunc_end0:
	.size	ErrorInit, .Lfunc_end0-ErrorInit
	.cfi_endproc

	.globl	Error
	.p2align	4, 0x90
	.type	Error,@function
Error:                                  # @Error
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
	subq	$720, %rsp              # imm = 0x2D0
.Lcfi5:
	.cfi_def_cfa_offset 752
.Lcfi6:
	.cfi_offset %rbx, -32
.Lcfi7:
	.cfi_offset %r14, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movq	%r8, %r14
	movl	%ecx, %ebp
	movq	%rdx, %rbx
	movl	%esi, %ecx
	movl	%edi, %edx
	testb	%al, %al
	je	.LBB1_2
# BB#1:
	movaps	%xmm0, 80(%rsp)
	movaps	%xmm1, 96(%rsp)
	movaps	%xmm2, 112(%rsp)
	movaps	%xmm3, 128(%rsp)
	movaps	%xmm4, 144(%rsp)
	movaps	%xmm5, 160(%rsp)
	movaps	%xmm6, 176(%rsp)
	movaps	%xmm7, 192(%rsp)
.LBB1_2:
	movq	%r9, 72(%rsp)
	leaq	32(%rsp), %rax
	movq	%rax, 16(%rsp)
	leaq	752(%rsp), %rax
	movq	%rax, 8(%rsp)
	movl	$48, 4(%rsp)
	movl	$40, (%rsp)
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB1_4
# BB#3:
	movl	%edx, %esi
	movl	%ecx, %edx
	movq	%rbx, %rcx
	callq	catgets
	movq	%rax, %rbx
.LBB1_4:
	leaq	208(%rsp), %rdi
	movq	%rsp, %rdx
	movq	%rbx, %rsi
	callq	vsprintf
	movq	fp(%rip), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_6
# BB#5:
	movq	stderr(%rip), %rbx
	movq	%rbx, fp(%rip)
.LBB1_6:
	testl	%ebp, %ebp
	je	.LBB1_16
# BB#7:
	cmpl	$2, %ebp
	je	.LBB1_37
# BB#8:
	cmpl	$1, %ebp
	je	.LBB1_11
# BB#9:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.15, %r9d
	xorl	%eax, %eax
	callq	Error
	jmp	.LBB1_52
	.p2align	4, 0x90
.LBB1_15:                               # %.lr.ph
                                        #   in Loop: Header=BB1_16 Depth=1
	movl	$1, %edi
	callq	LeaveErrorBlock
.LBB1_16:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, block_top(%rip)
	jg	.LBB1_15
# BB#17:                                # %._crit_edge
	cmpl	$0, AltErrorFormat(%rip)
	jne	.LBB1_18
# BB#23:
	movzwl	2(%r14), %ebx
	cmpl	%ebx, PrintFileBanner.CurrentFileNum(%rip)
	je	.LBB1_25
# BB#24:
	movq	fp(%rip), %rbp
	movl	%ebx, %edi
	callq	EchoFileSource
	movq	%rax, %rcx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movl	%ebx, PrintFileBanner.CurrentFileNum(%rip)
.LBB1_25:                               # %PrintFileBanner.exit18
	movq	fp(%rip), %rbx
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_27
# BB#26:
	movl	$.L.str.9, %ebp
	jmp	.LBB1_29
	.p2align	4, 0x90
.LBB1_10:                               # %.lr.ph20
                                        #   in Loop: Header=BB1_11 Depth=1
	movl	$1, %edi
	callq	LeaveErrorBlock
.LBB1_11:                               # %.lr.ph20
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, block_top(%rip)
	jg	.LBB1_10
# BB#12:                                # %._crit_edge21
	cmpl	$0, AltErrorFormat(%rip)
	jne	.LBB1_13
# BB#32:
	movzwl	2(%r14), %ebx
	cmpl	%ebx, PrintFileBanner.CurrentFileNum(%rip)
	je	.LBB1_34
# BB#33:
	movq	fp(%rip), %rbp
	movl	%ebx, %edi
	callq	EchoFileSource
	movq	%rax, %rcx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbp, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movl	%ebx, PrintFileBanner.CurrentFileNum(%rip)
.LBB1_34:                               # %PrintFileBanner.exit17
	movq	fp(%rip), %rbx
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_36
# BB#35:
	movl	$.L.str.11, %ebp
	jmp	.LBB1_29
.LBB1_37:
	movslq	block_top(%rip), %rax
	testq	%rax, %rax
	je	.LBB1_39
# BB#38:
	cmpl	$0, print_block-4(,%rax,4)
	je	.LBB1_45
.LBB1_39:
	cmpl	$0, AltErrorFormat(%rip)
	je	.LBB1_42
# BB#40:
	movq	%r14, %rdi
	callq	EchoAltFilePos
	movq	%rax, %rdx
	leaq	208(%rsp), %rcx
	movl	$.L.str.12, %esi
	jmp	.LBB1_41
.LBB1_18:
	movq	fp(%rip), %rbx
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_20
# BB#19:
	movl	$.L.str.8, %ebp
	jmp	.LBB1_22
.LBB1_13:
	movq	fp(%rip), %rbx
	movq	MsgCat(%rip), %rdi
	testq	%rdi, %rdi
	jne	.LBB1_31
# BB#14:
	movl	$.L.str.10, %ebp
	jmp	.LBB1_22
.LBB1_42:
	movzwl	2(%r14), %ebp
	cmpl	%ebp, PrintFileBanner.CurrentFileNum(%rip)
	je	.LBB1_44
# BB#43:
	movl	%ebp, %edi
	callq	EchoFileSource
	movq	%rax, %rcx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movl	%ebp, PrintFileBanner.CurrentFileNum(%rip)
	movq	fp(%rip), %rbx
.LBB1_44:                               # %PrintFileBanner.exit
	movq	%r14, %rdi
	callq	EchoFileLine
	movq	%rax, %rdx
	leaq	208(%rsp), %rcx
	movl	$.L.str.13, %esi
.LBB1_41:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
.LBB1_51:
	movb	$1, error_seen(%rip)
.LBB1_52:
	xorl	%eax, %eax
	addq	$720, %rsp              # imm = 0x2D0
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB1_27:
	movl	$28, %esi
	movl	$4, %edx
	movl	$.L.str.9, %ecx
	jmp	.LBB1_28
.LBB1_36:
	movl	$28, %esi
	movl	$5, %edx
	movl	$.L.str.11, %ecx
.LBB1_28:
	callq	catgets
	movq	%rax, %rbp
.LBB1_29:
	movq	%r14, %rdi
	callq	EchoFileLine
	jmp	.LBB1_30
.LBB1_20:
	movl	$28, %esi
	movl	$7, %edx
	movl	$.L.str.8, %ecx
	jmp	.LBB1_21
.LBB1_31:
	movl	$28, %esi
	movl	$8, %edx
	movl	$.L.str.10, %ecx
.LBB1_21:
	callq	catgets
	movq	%rax, %rbp
.LBB1_22:
	movq	%r14, %rdi
	callq	EchoAltFilePos
.LBB1_30:
	movq	%rax, %rdx
	leaq	208(%rsp), %rcx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB1_45:
	movslq	mess_top(%rip), %rax
	cmpq	$19, %rax
	jg	.LBB1_50
# BB#46:
	cmpl	$0, AltErrorFormat(%rip)
	je	.LBB1_49
# BB#47:
	leal	1(%rax), %ecx
	movl	%ecx, mess_top(%rip)
	shlq	$9, %rax
	leaq	message(%rax), %rbx
	movq	%r14, %rdi
	callq	EchoAltFilePos
	movq	%rax, %rdx
	leaq	208(%rsp), %rcx
	movl	$.L.str.12, %esi
	jmp	.LBB1_48
.LBB1_50:
	movl	$28, %edi
	movl	$6, %esi
	movl	$.L.str.14, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	movq	%r14, %r8
	callq	Error
	jmp	.LBB1_51
.LBB1_49:
	movzwl	2(%r14), %ecx
	movl	%ecx, message_fnum(,%rax,4)
	leal	1(%rax), %ecx
	movl	%ecx, mess_top(%rip)
	shlq	$9, %rax
	leaq	message(%rax), %rbx
	movq	%r14, %rdi
	callq	EchoFileLine
	movq	%rax, %rdx
	leaq	208(%rsp), %rcx
	movl	$.L.str.13, %esi
.LBB1_48:
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	sprintf
	jmp	.LBB1_51
.Lfunc_end1:
	.size	Error, .Lfunc_end1-Error
	.cfi_endproc

	.globl	ErrorSeen
	.p2align	4, 0x90
	.type	ErrorSeen,@function
ErrorSeen:                              # @ErrorSeen
	.cfi_startproc
# BB#0:
	movzbl	error_seen(%rip), %eax
	retq
.Lfunc_end2:
	.size	ErrorSeen, .Lfunc_end2-ErrorSeen
	.cfi_endproc

	.globl	EnterErrorBlock
	.p2align	4, 0x90
	.type	EnterErrorBlock,@function
EnterErrorBlock:                        # @EnterErrorBlock
	.cfi_startproc
# BB#0:
	movslq	block_top(%rip), %rax
	cmpq	$20, %rax
	jge	.LBB3_2
# BB#1:
	movl	%edi, print_block(,%rax,4)
	movl	mess_top(%rip), %ecx
	movl	%ecx, start_block(,%rax,4)
	leal	1(%rax), %eax
	movl	%eax, block_top(%rip)
	retq
.LBB3_2:
	movq	no_fpos(%rip), %r8
	movl	$28, %edi
	movl	$3, %esi
	movl	$.L.str.3, %edx
	movl	$1, %ecx
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.Lfunc_end3:
	.size	EnterErrorBlock, .Lfunc_end3-EnterErrorBlock
	.cfi_endproc

	.globl	LeaveErrorBlock
	.p2align	4, 0x90
	.type	LeaveErrorBlock,@function
LeaveErrorBlock:                        # @LeaveErrorBlock
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 48
.Lcfi14:
	.cfi_offset %rbx, -40
.Lcfi15:
	.cfi_offset %r14, -32
.Lcfi16:
	.cfi_offset %r15, -24
.Lcfi17:
	.cfi_offset %rbp, -16
	movl	%edi, %ebx
	cmpl	$0, block_top(%rip)
	jg	.LBB4_2
# BB#1:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.5, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_2:
	testl	%ebx, %ebx
	jne	.LBB4_5
# BB#3:
	movslq	block_top(%rip), %rax
	cmpl	$0, print_block-4(,%rax,4)
	je	.LBB4_5
# BB#4:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.6, %r9d
	xorl	%eax, %eax
	callq	Error
.LBB4_5:
	cmpq	$0, fp(%rip)
	jne	.LBB4_7
# BB#6:
	movq	stderr(%rip), %rax
	movq	%rax, fp(%rip)
.LBB4_7:
	testl	%ebx, %ebx
	je	.LBB4_14
# BB#8:
	movslq	block_top(%rip), %rax
	movslq	start_block-4(,%rax,4), %r15
	cmpl	mess_top(%rip), %r15d
	jge	.LBB4_14
# BB#9:                                 # %.lr.ph.preheader
	movq	%r15, %rax
	shlq	$9, %rax
	leaq	message(%rax), %rbx
	.p2align	4, 0x90
.LBB4_10:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpl	$0, AltErrorFormat(%rip)
	jne	.LBB4_13
# BB#11:                                #   in Loop: Header=BB4_10 Depth=1
	movl	message_fnum(,%r15,4), %ebp
	cmpl	%ebp, PrintFileBanner.CurrentFileNum(%rip)
	je	.LBB4_13
# BB#12:                                #   in Loop: Header=BB4_10 Depth=1
	movq	fp(%rip), %r14
	movzwl	%bp, %edi
	callq	EchoFileSource
	movq	%rax, %rcx
	movl	$.L.str.16, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rcx, %rdx
	callq	fprintf
	movl	%ebp, PrintFileBanner.CurrentFileNum(%rip)
	.p2align	4, 0x90
.LBB4_13:                               # %PrintFileBanner.exit
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	fp(%rip), %rsi
	movq	%rbx, %rdi
	callq	fputs
	incq	%r15
	movslq	mess_top(%rip), %rax
	addq	$512, %rbx              # imm = 0x200
	cmpq	%rax, %r15
	jl	.LBB4_10
.LBB4_14:                               # %.loopexit
	movslq	block_top(%rip), %rax
	leaq	-1(%rax), %rcx
	movl	%ecx, block_top(%rip)
	movl	start_block-4(,%rax,4), %eax
	movl	%eax, mess_top(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end4:
	.size	LeaveErrorBlock, .Lfunc_end4-LeaveErrorBlock
	.cfi_endproc

	.globl	CheckErrorBlocks
	.p2align	4, 0x90
	.type	CheckErrorBlocks,@function
CheckErrorBlocks:                       # @CheckErrorBlocks
	.cfi_startproc
# BB#0:
	cmpl	$0, block_top(%rip)
	je	.LBB5_1
# BB#2:
	movq	no_fpos(%rip), %r8
	movl	$1, %edi
	movl	$2, %esi
	movl	$.L.str.4, %edx
	xorl	%ecx, %ecx
	movl	$.L.str.7, %r9d
	xorl	%eax, %eax
	jmp	Error                   # TAILCALL
.LBB5_1:
	retq
.Lfunc_end5:
	.size	CheckErrorBlocks, .Lfunc_end5-CheckErrorBlocks
	.cfi_endproc

	.type	fp,@object              # @fp
	.local	fp
	.comm	fp,8,8
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"-e argument appears twice in command line"
	.size	.L.str, 42

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"w"
	.size	.L.str.1, 2

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"cannot open error file %s"
	.size	.L.str.2, 26

	.type	error_seen,@object      # @error_seen
	.local	error_seen
	.comm	error_seen,1,4
	.type	block_top,@object       # @block_top
	.local	block_top
	.comm	block_top,4,4
	.type	print_block,@object     # @print_block
	.local	print_block
	.comm	print_block,80,16
	.type	mess_top,@object        # @mess_top
	.local	mess_top
	.comm	mess_top,4,4
	.type	start_block,@object     # @start_block
	.local	start_block
	.comm	start_block,80,16
	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"too many levels of error messages"
	.size	.L.str.3, 34

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"assert failed in %s"
	.size	.L.str.4, 20

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"LeaveErrorBlock: no matching EnterErrorBlock!"
	.size	.L.str.5, 46

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"LeaveErrorBlock: commit!"
	.size	.L.str.6, 25

	.type	message,@object         # @message
	.local	message
	.comm	message,10240,16
	.type	message_fnum,@object    # @message_fnum
	.local	message_fnum
	.comm	message_fnum,80,16
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"CheckErrorBlocks: block_top != 0!"
	.size	.L.str.7, 34

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"%s internal error: %s\n"
	.size	.L.str.8, 23

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"  %6s internal error: %s\n"
	.size	.L.str.9, 26

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"%s: fatal error: %s\n"
	.size	.L.str.10, 21

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"  %6s: fatal error: %s\n"
	.size	.L.str.11, 24

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"%s: %s\n"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"  %6s: %s\n"
	.size	.L.str.13, 11

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"too many error messages"
	.size	.L.str.14, 24

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"Error: invalid error type"
	.size	.L.str.15, 26

	.type	PrintFileBanner.CurrentFileNum,@object # @PrintFileBanner.CurrentFileNum
	.data
	.p2align	2
PrintFileBanner.CurrentFileNum:
	.long	4294967295              # 0xffffffff
	.size	PrintFileBanner.CurrentFileNum, 4

	.type	.L.str.16,@object       # @.str.16
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.16:
	.asciz	"lout%s:\n"
	.size	.L.str.16, 9


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
