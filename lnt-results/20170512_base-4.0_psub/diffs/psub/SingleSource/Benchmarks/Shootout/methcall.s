	.text
	.file	"methcall.bc"
	.globl	toggle_value
	.p2align	4, 0x90
	.type	toggle_value,@function
toggle_value:                           # @toggle_value
	.cfi_startproc
# BB#0:
	movb	(%rdi), %al
	retq
.Lfunc_end0:
	.size	toggle_value, .Lfunc_end0-toggle_value
	.cfi_endproc

	.globl	toggle_activate
	.p2align	4, 0x90
	.type	toggle_activate,@function
toggle_activate:                        # @toggle_activate
	.cfi_startproc
# BB#0:
	cmpb	$0, (%rdi)
	sete	(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end1:
	.size	toggle_activate, .Lfunc_end1-toggle_activate
	.cfi_endproc

	.globl	init_Toggle
	.p2align	4, 0x90
	.type	init_Toggle,@function
init_Toggle:                            # @init_Toggle
	.cfi_startproc
# BB#0:
	movb	%sil, (%rdi)
	movq	$toggle_value, 8(%rdi)
	movq	$toggle_activate, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end2:
	.size	init_Toggle, .Lfunc_end2-init_Toggle
	.cfi_endproc

	.globl	new_Toggle
	.p2align	4, 0x90
	.type	new_Toggle,@function
new_Toggle:                             # @new_Toggle
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	%edi, %ebx
	movl	$24, %edi
	callq	malloc
	movb	%bl, (%rax)
	movq	$toggle_value, 8(%rax)
	movq	$toggle_activate, 16(%rax)
	popq	%rbx
	retq
.Lfunc_end3:
	.size	new_Toggle, .Lfunc_end3-new_Toggle
	.cfi_endproc

	.globl	nth_toggle_activate
	.p2align	4, 0x90
	.type	nth_toggle_activate,@function
nth_toggle_activate:                    # @nth_toggle_activate
	.cfi_startproc
# BB#0:
	movl	28(%rdi), %eax
	incl	%eax
	movl	%eax, 28(%rdi)
	cmpl	24(%rdi), %eax
	jl	.LBB4_2
# BB#1:
	cmpb	$0, (%rdi)
	sete	(%rdi)
	movl	$0, 28(%rdi)
.LBB4_2:
	movq	%rdi, %rax
	retq
.Lfunc_end4:
	.size	nth_toggle_activate, .Lfunc_end4-nth_toggle_activate
	.cfi_endproc

	.globl	init_NthToggle
	.p2align	4, 0x90
	.type	init_NthToggle,@function
init_NthToggle:                         # @init_NthToggle
	.cfi_startproc
# BB#0:
	movl	%esi, 24(%rdi)
	movl	$0, 28(%rdi)
	movq	$nth_toggle_activate, 16(%rdi)
	movq	%rdi, %rax
	retq
.Lfunc_end5:
	.size	init_NthToggle, .Lfunc_end5-init_NthToggle
	.cfi_endproc

	.globl	new_NthToggle
	.p2align	4, 0x90
	.type	new_NthToggle,@function
new_NthToggle:                          # @new_NthToggle
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -24
.Lcfi6:
	.cfi_offset %rbp, -16
	movl	%esi, %ebx
	movl	%edi, %ebp
	movl	$32, %edi
	callq	malloc
	movb	%bpl, (%rax)
	movq	$toggle_value, 8(%rax)
	movl	%ebx, 24(%rax)
	movl	$0, 28(%rax)
	movq	$nth_toggle_activate, 16(%rax)
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end6:
	.size	new_NthToggle, .Lfunc_end6-new_NthToggle
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi7:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 32
.Lcfi10:
	.cfi_offset %rbx, -32
.Lcfi11:
	.cfi_offset %r14, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	$500000000, %ebx        # imm = 0x1DCD6500
	cmpl	$2, %edi
	jne	.LBB7_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
.LBB7_2:
	movl	$24, %edi
	callq	malloc
	movq	%rax, %r14
	movb	$1, (%r14)
	movq	$toggle_value, 8(%r14)
	movq	$toggle_activate, 16(%r14)
	testl	%ebx, %ebx
	jle	.LBB7_3
# BB#4:                                 # %.lr.ph31.preheader
	movq	%r14, %rdi
	callq	toggle_activate
	movq	%r14, %rdi
	callq	*8(%rax)
	cmpl	$1, %ebx
	je	.LBB7_7
# BB#5:                                 # %._crit_edge39.preheader
	leal	-1(%rbx), %ebp
	.p2align	4, 0x90
.LBB7_6:                                # %._crit_edge39
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	*16(%r14)
	movq	%r14, %rdi
	callq	*8(%rax)
	decl	%ebp
	jne	.LBB7_6
.LBB7_7:                                # %._crit_edge32
	testb	%al, %al
	movl	$.L.str, %eax
	movl	$.L.str.1, %edi
	cmovneq	%rax, %rdi
	jmp	.LBB7_8
.LBB7_3:
	movl	$.L.str, %edi
.LBB7_8:
	callq	puts
	movq	%r14, %rdi
	callq	free
	movl	$32, %edi
	callq	malloc
	movq	%rax, %r14
	movb	$1, (%r14)
	movq	$toggle_value, 8(%r14)
	movl	$3, 24(%r14)
	movl	$0, 28(%r14)
	movq	$nth_toggle_activate, 16(%r14)
	testl	%ebx, %ebx
	jle	.LBB7_9
# BB#10:                                # %.lr.ph
	movq	%r14, %rdi
	callq	nth_toggle_activate
	movq	%r14, %rdi
	callq	*8(%rax)
	cmpl	$1, %ebx
	je	.LBB7_13
# BB#11:                                # %._crit_edge40.preheader
	decl	%ebx
	.p2align	4, 0x90
.LBB7_12:                               # %._crit_edge40
                                        # =>This Inner Loop Header: Depth=1
	movq	%r14, %rdi
	callq	*16(%r14)
	movq	%r14, %rdi
	callq	*8(%rax)
	decl	%ebx
	jne	.LBB7_12
.LBB7_13:                               # %._crit_edge
	testb	%al, %al
	movl	$.L.str, %eax
	movl	$.L.str.1, %edi
	cmovneq	%rax, %rdi
	jmp	.LBB7_14
.LBB7_9:
	movl	$.L.str, %edi
.LBB7_14:
	callq	puts
	movq	%r14, %rdi
	callq	free
	xorl	%eax, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end7:
	.size	main, .Lfunc_end7-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"true\n"
	.size	.L.str, 6

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"false\n"
	.size	.L.str.1, 7


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
