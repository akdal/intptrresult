	.text
	.file	"zpaint.bc"
	.globl	zerasepage
	.p2align	4, 0x90
	.type	zerasepage,@function
zerasepage:                             # @zerasepage
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_erasepage            # TAILCALL
.Lfunc_end0:
	.size	zerasepage, .Lfunc_end0-zerasepage
	.cfi_endproc

	.globl	zfill
	.p2align	4, 0x90
	.type	zfill,@function
zfill:                                  # @zfill
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_fill                 # TAILCALL
.Lfunc_end1:
	.size	zfill, .Lfunc_end1-zfill
	.cfi_endproc

	.globl	zeofill
	.p2align	4, 0x90
	.type	zeofill,@function
zeofill:                                # @zeofill
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_eofill               # TAILCALL
.Lfunc_end2:
	.size	zeofill, .Lfunc_end2-zeofill
	.cfi_endproc

	.globl	zstroke
	.p2align	4, 0x90
	.type	zstroke,@function
zstroke:                                # @zstroke
	.cfi_startproc
# BB#0:
	movq	igs(%rip), %rdi
	jmp	gs_stroke               # TAILCALL
.Lfunc_end3:
	.size	zstroke, .Lfunc_end3-zstroke
	.cfi_endproc

	.globl	zcolorimage
	.p2align	4, 0x90
	.type	zcolorimage,@function
zcolorimage:                            # @zcolorimage
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movzwl	8(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB4_18
# BB#1:
	movzwl	-8(%rdi), %ecx
	andl	$252, %ecx
	cmpl	$4, %ecx
	jne	.LBB4_18
# BB#2:
	movq	(%rdi), %rdx
	cmpq	$4, %rdx
	jbe	.LBB4_4
# BB#3:
	movl	$-15, %eax
	popq	%rbx
	retq
.LBB4_4:
	leal	-3(%rdx), %ecx
	cmpl	$2, %ecx
	jae	.LBB4_5
# BB#11:
	cmpw	$0, -16(%rdi)
	leaq	-32(%rdi), %rdi
	je	.LBB4_12
# BB#13:
	movq	%rdx, %rcx
	shlq	$32, %rcx
	movabsq	$-4294967296, %rsi      # imm = 0xFFFFFFFF00000000
	addq	%rcx, %rsi
	sarq	$28, %rsi
	subq	%rsi, %rdi
	negl	%edx
	movabsq	$25769803776, %rbx      # imm = 0x600000000
	addq	%rcx, %rbx
	sarq	$32, %rbx
	jmp	.LBB4_14
.LBB4_5:
	cmpl	$1, %edx
	jne	.LBB4_6
# BB#7:
	addq	$-32, osp(%rip)
	movzwl	-24(%rdi), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB4_18
# BB#8:
	movq	-32(%rdi), %rsi
	cmpq	$8, %rsi
	movl	$-15, %eax
	ja	.LBB4_18
# BB#9:
	movl	$1, %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	image_setup
	testl	%eax, %eax
	js	.LBB4_18
# BB#10:
	addq	$-80, osp(%rip)
	popq	%rbx
	retq
.LBB4_6:
	movl	$-15, %eax
	popq	%rbx
	retq
.LBB4_12:
	movl	$7, %ebx
.LBB4_14:
	movzwl	-24(%rdi), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB4_18
# BB#15:
	movq	-32(%rdi), %rsi
	cmpq	$8, %rsi
	movl	$-15, %eax
	ja	.LBB4_18
# BB#16:
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	callq	image_setup
	testl	%eax, %eax
	js	.LBB4_18
# BB#17:
	shlq	$4, %rbx
	subq	%rbx, osp(%rip)
.LBB4_18:                               # %zimage.exit
	popq	%rbx
	retq
.Lfunc_end4:
	.size	zcolorimage, .Lfunc_end4-zcolorimage
	.cfi_endproc

	.globl	image_setup
	.p2align	4, 0x90
	.type	image_setup,@function
image_setup:                            # @image_setup
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi8:
	.cfi_def_cfa_offset 160
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movl	%esi, %r14d
	movq	%rdi, %r15
	movl	$144, %ecx
	addq	esp(%rip), %rcx
	movl	$-5, %eax
	cmpq	estop(%rip), %rcx
	ja	.LBB5_32
# BB#1:
	movzwl	-56(%r15), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB5_32
# BB#2:
	movzwl	-40(%r15), %ecx
	andl	$252, %ecx
	cmpl	$20, %ecx
	jne	.LBB5_32
# BB#3:                                 # %.preheader
	movl	%ebp, %ecx
	notl	%ecx
	movl	%ebp, %r13d
	sarl	$31, %r13d
	andl	%ecx, %r13d
	js	.LBB5_11
# BB#4:                                 # %.lr.ph.preheader
	movslq	%r13d, %rcx
	leaq	8(%r15), %rdx
	movq	$-1, %rsi
	.p2align	4, 0x90
.LBB5_5:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	(%rdx), %ebx
	movl	%ebx, %edi
	andl	$252, %edi
	shrl	$2, %edi
	cmpl	$13, %edi
	je	.LBB5_10
# BB#6:                                 #   in Loop: Header=BB5_5 Depth=1
	andb	$63, %dil
	cmpb	$10, %dil
	je	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_5 Depth=1
	testb	%dil, %dil
	jne	.LBB5_32
.LBB5_8:                                #   in Loop: Header=BB5_5 Depth=1
	andl	$3, %ebx
	cmpl	$3, %ebx
	jne	.LBB5_9
.LBB5_10:                               #   in Loop: Header=BB5_5 Depth=1
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rcx, %rsi
	jl	.LBB5_5
.LBB5_11:                               # %._crit_edge
	movl	$-23, %eax
	cmpq	$0, -64(%r15)
	jle	.LBB5_32
# BB#12:
	movq	-48(%r15), %rcx
	testq	%rcx, %rcx
	js	.LBB5_32
# BB#13:
	testq	%rcx, %rcx
	je	.LBB5_14
# BB#15:
	leaq	-16(%r15), %rdi
	leaq	8(%rsp), %r12
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	read_matrix
	testl	%eax, %eax
	js	.LBB5_32
# BB#16:
	movl	gs_image_enum_sizeof(%rip), %esi
	movl	$1, %edi
	movl	$.L.str, %edx
	callq	alloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	je	.LBB5_17
# BB#18:
	movq	igs(%rip), %rsi
	movl	-64(%r15), %edx
	movl	-48(%r15), %ecx
	testl	%ebp, %ebp
	je	.LBB5_19
# BB#20:
	movq	%r12, (%rsp)
	movq	%rbx, %rdi
	movl	%r14d, %r8d
	movl	%ebp, %r9d
	callq	gs_image_init
	testl	%eax, %eax
	jns	.LBB5_22
	jmp	.LBB5_32
.LBB5_9:
	movl	$-7, %eax
	jmp	.LBB5_32
.LBB5_14:
	xorl	%eax, %eax
	jmp	.LBB5_32
.LBB5_17:
	movl	$-25, %eax
	jmp	.LBB5_32
.LBB5_19:
	leaq	8(%rsp), %r9
	movq	%rbx, %rdi
	movl	%r14d, %r8d
	callq	gs_imagemask_init
	testl	%eax, %eax
	js	.LBB5_32
.LBB5_22:
	movq	esp(%rip), %rax
	movw	$0, 16(%rax)
	movw	$33, 24(%rax)
	leaq	32(%rax), %rcx
	movq	%rcx, esp(%rip)
	testl	%r13d, %r13d
	js	.LBB5_23
# BB#24:
	movups	(%r15), %xmm0
	movups	%xmm0, (%rcx)
	leaq	48(%rax), %rcx
	movq	%rcx, esp(%rip)
	testl	%r13d, %r13d
	je	.LBB5_25
# BB#26:
	movups	16(%r15), %xmm0
	movups	%xmm0, (%rcx)
	leaq	64(%rax), %rcx
	movq	%rcx, esp(%rip)
	cmpl	$1, %r13d
	jle	.LBB5_27
# BB#29:
	movups	32(%r15), %xmm0
	movups	%xmm0, (%rcx)
	leaq	80(%rax), %rcx
	movq	%rcx, esp(%rip)
	cmpl	$2, %r13d
	je	.LBB5_28
# BB#30:
	movups	48(%r15), %xmm0
	movups	%xmm0, (%rcx)
	jmp	.LBB5_31
.LBB5_23:                               # %.thread
	movq	$0, 32(%rax)
	movw	$32, 40(%rax)
	leaq	48(%rax), %rcx
	movq	%rcx, esp(%rip)
.LBB5_25:                               # %.thread44
	movq	$0, 48(%rax)
	movw	$32, 56(%rax)
	leaq	64(%rax), %rcx
	movq	%rcx, esp(%rip)
.LBB5_27:                               # %.thread45
	movq	$0, 64(%rax)
	movw	$32, 72(%rax)
	leaq	80(%rax), %rcx
	movq	%rcx, esp(%rip)
.LBB5_28:
	movq	$0, 80(%rax)
	movw	$32, 88(%rax)
.LBB5_31:
	movq	$0, 96(%rax)
	movw	$20, 104(%rax)
	movw	%r13w, 106(%rax)
	leaq	112(%rax), %rcx
	movq	%rcx, esp(%rip)
	movq	%rbx, 112(%rax)
	movw	$52, 120(%rax)
	movzwl	gs_image_enum_sizeof(%rip), %eax
	movq	esp(%rip), %rcx
	movw	%ax, 10(%rcx)
	movq	$image_continue, 16(%rcx)
	movw	$37, 24(%rcx)
	movw	$0, 26(%rcx)
	leaq	32(%rcx), %rax
	movq	%rax, esp(%rip)
	movups	(%r15), %xmm0
	movups	%xmm0, 32(%rcx)
	movl	$1, %eax
.LBB5_32:                               # %.loopexit
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	image_setup, .Lfunc_end5-image_setup
	.cfi_endproc

	.globl	zimage
	.p2align	4, 0x90
	.type	zimage,@function
zimage:                                 # @zimage
	.cfi_startproc
# BB#0:
	movzwl	-24(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$20, %ecx
	jne	.LBB6_4
# BB#1:
	movq	-32(%rdi), %rsi
	movl	$-15, %eax
	cmpq	$8, %rsi
	ja	.LBB6_4
# BB#2:
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 16
	movl	$1, %edx
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	image_setup
	testl	%eax, %eax
	leaq	8(%rsp), %rsp
	js	.LBB6_4
# BB#3:
	addq	$-80, osp(%rip)
.LBB6_4:
	retq
.Lfunc_end6:
	.size	zimage, .Lfunc_end6-zimage
	.cfi_endproc

	.globl	zimagemask
	.p2align	4, 0x90
	.type	zimagemask,@function
zimagemask:                             # @zimagemask
	.cfi_startproc
# BB#0:
	movzwl	-24(%rdi), %ecx
	andl	$252, %ecx
	movl	$-20, %eax
	cmpl	$4, %ecx
	jne	.LBB7_3
# BB#1:
	pushq	%rax
.Lcfi16:
	.cfi_def_cfa_offset 16
	movzwl	-32(%rdi), %esi
	xorl	%edx, %edx
	callq	image_setup
	testl	%eax, %eax
	leaq	8(%rsp), %rsp
	js	.LBB7_3
# BB#2:
	addq	$-80, osp(%rip)
.LBB7_3:
	retq
.Lfunc_end7:
	.size	zimagemask, .Lfunc_end7-zimagemask
	.cfi_endproc

	.globl	image_continue
	.p2align	4, 0x90
	.type	image_continue,@function
image_continue:                         # @image_continue
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -24
.Lcfi21:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	esp(%rip), %rax
	movq	(%rax), %r14
	movzwl	8(%rbx), %ecx
	andl	$252, %ecx
	cmpl	$52, %ecx
	jne	.LBB8_1
# BB#2:
	movq	(%rbx), %rsi
	movzwl	10(%rbx), %edx
	movq	%r14, %rdi
	callq	gs_image_next
	testl	%eax, %eax
	movq	esp(%rip), %rax
	jg	.LBB8_4
# BB#3:
	movzwl	10(%rbx), %ecx
	testw	%cx, %cx
	je	.LBB8_4
# BB#5:
	movq	-16(%rax), %rcx
	incq	%rcx
	movq	%rcx, -16(%rax)
	movzwl	-6(%rax), %edx
	leaq	32(%rax), %rsi
	xorl	%edi, %edi
	cmpl	%edx, %ecx
	movslq	%ecx, %rdx
	cmovgq	%rdi, %rcx
	movq	%rcx, -16(%rax)
	movq	$image_continue, 16(%rax)
	movw	$37, 24(%rax)
	movw	$0, 26(%rax)
	movq	%rsi, esp(%rip)
	cmovgq	%rdi, %rdx
	shlq	$4, %rdx
	movups	-80(%rax,%rdx), %xmm0
	movups	%xmm0, 32(%rax)
	jmp	.LBB8_6
.LBB8_1:
	addq	$-112, %rax
	movq	%rax, esp(%rip)
	movl	gs_image_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.1, %ecx
	movq	%r14, %rdi
	callq	alloc_free
	movl	$-20, %eax
	jmp	.LBB8_7
.LBB8_4:
	addq	$-112, %rax
	movq	%rax, esp(%rip)
	movl	gs_image_enum_sizeof(%rip), %edx
	movl	$1, %esi
	movl	$.L.str.2, %ecx
	movq	%r14, %rdi
	callq	alloc_free
.LBB8_6:
	addq	$-16, osp(%rip)
	movl	$1, %eax
.LBB8_7:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end8:
	.size	image_continue, .Lfunc_end8-image_continue
	.cfi_endproc

	.globl	zpaint_op_init
	.p2align	4, 0x90
	.type	zpaint_op_init,@function
zpaint_op_init:                         # @zpaint_op_init
	.cfi_startproc
# BB#0:
	movl	$zpaint_op_init.my_defs, %edi
	xorl	%eax, %eax
	jmp	z_op_init               # TAILCALL
.Lfunc_end9:
	.size	zpaint_op_init, .Lfunc_end9-zpaint_op_init
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"image_setup"
	.size	.L.str, 12

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"image_continue(quit)"
	.size	.L.str.1, 21

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"image_continue(finished)"
	.size	.L.str.2, 25

	.type	zpaint_op_init.my_defs,@object # @zpaint_op_init.my_defs
	.data
	.p2align	4
zpaint_op_init.my_defs:
	.quad	.L.str.3
	.quad	zeofill
	.quad	.L.str.4
	.quad	zerasepage
	.quad	.L.str.5
	.quad	zfill
	.quad	.L.str.6
	.quad	zcolorimage
	.quad	.L.str.7
	.quad	zimage
	.quad	.L.str.8
	.quad	zimagemask
	.quad	.L.str.9
	.quad	zstroke
	.zero	16
	.size	zpaint_op_init.my_defs, 128

	.type	.L.str.3,@object        # @.str.3
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.3:
	.asciz	"0eofill"
	.size	.L.str.3, 8

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"0erasepage"
	.size	.L.str.4, 11

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"0fill"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"7colorimage"
	.size	.L.str.6, 12

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"5image"
	.size	.L.str.7, 7

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"5imagemask"
	.size	.L.str.8, 11

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"0stroke"
	.size	.L.str.9, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
