	.text
	.file	"weighted_prediction.bc"
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI0_0:
	.quad	4629700416936869888     # double 32
.LCPI0_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	estimate_weighting_factor_P_slice
	.p2align	4, 0x90
	.type	estimate_weighting_factor_P_slice,@function
estimate_weighting_factor_P_slice:      # @estimate_weighting_factor_P_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1448, %rsp             # imm = 0x5A8
.Lcfi6:
	.cfi_def_cfa_offset 1504
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	img(%rip), %r9
	cmpl	$0, 15268(%r9)
	je	.LBB0_1
# BB#2:
	movq	14224(%r9), %rcx
	movslq	12(%r9), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdx)
	je	.LBB0_1
# BB#3:
	andl	$1, %eax
	leal	4(%rax,%rax), %eax
	jmp	.LBB0_4
.LBB0_1:
	movl	$2, %eax
.LBB0_4:                                # %.preheader144.lr.ph
	movq	%rax, -128(%rsp)        # 8-byte Spill
	movl	$5, luma_log_weight_denom(%rip)
	movl	$5, chroma_log_weight_denom(%rip)
	movl	$16, wp_luma_round(%rip)
	movl	$16, wp_chroma_round(%rip)
	movl	$32, -108(%rsp)
	movabsq	$137438953504, %rax     # imm = 0x2000000020
	movq	%rax, -116(%rsp)
	movq	wp_weight(%rip), %rax
	movq	%rax, -104(%rsp)        # 8-byte Spill
	leaq	672(%rsp), %r15
	leaq	-96(%rsp), %r12
	xorl	%r13d, %r13d
	movq	wp_offset(%rip), %r14
	.p2align	4, 0x90
.LBB0_5:                                # %.preheader144
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_7 Depth 2
	cmpl	$0, listXsize(,%r13,4)
	jle	.LBB0_8
# BB#6:                                 # %.preheader143.lr.ph
                                        #   in Loop: Header=BB0_5 Depth=1
	movq	-104(%rsp), %rax        # 8-byte Reload
	movq	(%rax,%r13,8), %r8
	movq	(%r14,%r13,8), %r11
	movq	%r12, %rsi
	movq	%r15, %rdx
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_7:                                # %.preheader143
                                        #   Parent Loop BB0_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r8,%rbx,8), %rbp
	movq	(%r11,%rbx,8), %rax
	movl	$0, 8(%rdx)
	movq	$0, (%rdx)
	movl	-108(%rsp), %r10d
	movl	%r10d, 8(%rsi)
	movq	-116(%rsp), %rcx
	movq	%rcx, (%rsi)
	movl	$32, (%rbp)
	movl	$0, (%rax)
	movl	$32, 4(%rbp)
	movl	$0, 4(%rax)
	movl	$32, 8(%rbp)
	movl	$0, 8(%rax)
	incq	%rbx
	movslq	listXsize(,%r13,4), %rax
	addq	$12, %rdx
	addq	$12, %rsi
	cmpq	%rax, %rbx
	jl	.LBB0_7
.LBB0_8:                                # %._crit_edge169
                                        #   in Loop: Header=BB0_5 Depth=1
	incq	%r13
	addq	$384, %r15              # imm = 0x180
	addq	$384, %r12              # imm = 0x180
	cmpq	-128(%rsp), %r13        # 8-byte Folded Reload
	jl	.LBB0_5
# BB#9:                                 # %.preheader142
	movslq	68(%r9), %r8
	testq	%r8, %r8
	xorps	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	jle	.LBB0_21
# BB#10:                                # %.preheader141.lr.ph
	cmpl	$0, 52(%r9)
	xorpd	%xmm1, %xmm1
	jle	.LBB0_21
# BB#11:                                # %.preheader141.us.preheader
	movq	imgY_org(%rip), %rcx
	movq	img(%rip), %rdx
	movslq	52(%rdx), %rdx
	testb	$1, %r8b
	jne	.LBB0_13
# BB#12:
	xorpd	%xmm1, %xmm1
	xorl	%esi, %esi
	cmpl	$1, %r8d
	jne	.LBB0_16
	jmp	.LBB0_21
.LBB0_13:                               # %.preheader141.us.prol
	movq	(%rcx), %rbx
	xorpd	%xmm1, %xmm1
	xorl	%ebp, %ebp
	movl	$1, %esi
	.p2align	4, 0x90
.LBB0_14:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rbx,%rbp,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm2, %xmm1
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB0_14
# BB#15:                                # %.preheader141.us.prol.loopexit
	cmpl	$1, %r8d
	je	.LBB0_21
	.p2align	4, 0x90
.LBB0_16:                               # %.preheader141.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_17 Depth 2
                                        #     Child Loop BB0_19 Depth 2
	movq	(%rcx,%rsi,8), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_17:                               #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%rbp,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm2, %xmm1
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB0_17
# BB#18:                                # %._crit_edge162.us
                                        #   in Loop: Header=BB0_16 Depth=1
	movq	8(%rcx,%rsi,8), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_19:                               #   Parent Loop BB0_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbx,%rbp,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm2, %xmm1
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB0_19
# BB#20:                                # %._crit_edge162.us.1
                                        #   in Loop: Header=BB0_16 Depth=1
	addq	$2, %rsi
	cmpq	%r8, %rsi
	jl	.LBB0_16
.LBB0_21:                               # %.preheader139.lr.ph
	movsd	.LCPI0_0(%rip), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	xorl	%r13d, %r13d
	movsd	.LCPI0_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movl	$-128, %r10d
	.p2align	4, 0x90
.LBB0_22:                               # %.preheader139
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_24 Depth 2
                                        #       Child Loop BB0_27 Depth 3
                                        #         Child Loop BB0_28 Depth 4
	movslq	listXsize(,%r13,4), %r14
	testq	%r14, %r14
	jle	.LBB0_38
# BB#23:                                # %.lr.ph156
                                        #   in Loop: Header=BB0_22 Depth=1
	movq	listX(,%r13,8), %r11
	movq	img(%rip), %r12
	movslq	68(%r12), %r15
	movq	%r15, %rbx
	addq	$20, %rbx
	xorl	%eax, %eax
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_31:                               #   in Loop: Header=BB0_24 Depth=2
	ucomisd	%xmm0, %xmm4
	jne	.LBB0_33
	jnp	.LBB0_32
.LBB0_33:                               #   in Loop: Header=BB0_24 Depth=2
	movapd	%xmm2, %xmm5
	divsd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	cvttsd2si	%xmm5, %esi
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r13,%r13,2), %rdx
	shlq	$7, %rdx
	leaq	-96(%rsp,%rdx), %rdx
	movl	%esi, (%rdx,%rcx,4)
	addl	$64, %esi
	cmpl	$191, %esi
	jbe	.LBB0_37
# BB#34:                                #   in Loop: Header=BB0_24 Depth=2
	leaq	(%rdx,%rcx,4), %rcx
	jmp	.LBB0_36
.LBB0_32:                               # %.thread
                                        #   in Loop: Header=BB0_24 Depth=2
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r13,%r13,2), %rdx
	shlq	$7, %rdx
	leaq	-96(%rsp,%rdx), %rdx
	leaq	(%rdx,%rcx,4), %rcx
	jmp	.LBB0_36
	.p2align	4, 0x90
.LBB0_24:                               #   Parent Loop BB0_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_27 Depth 3
                                        #         Child Loop BB0_28 Depth 4
	movq	(%r11,%rax,8), %rcx
	movq	6448(%rcx), %rdx
	movq	%rdx, ref_pic_sub(%rip)
	xorpd	%xmm4, %xmm4
	testl	%r15d, %r15d
	jle	.LBB0_30
# BB#25:                                # %.preheader138.lr.ph
                                        #   in Loop: Header=BB0_24 Depth=2
	movslq	52(%r12), %rcx
	testq	%rcx, %rcx
	jle	.LBB0_30
# BB#26:                                # %.preheader138.us.preheader
                                        #   in Loop: Header=BB0_24 Depth=2
	movq	(%rdx), %rdx
	movq	(%rdx), %rsi
	addq	$19, %rcx
	xorpd	%xmm4, %xmm4
	movl	$20, %edx
	.p2align	4, 0x90
.LBB0_27:                               # %.preheader138.us
                                        #   Parent Loop BB0_22 Depth=1
                                        #     Parent Loop BB0_24 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_28 Depth 4
	movq	(%rsi,%rdx,8), %rbp
	movl	$19, %r8d
	.p2align	4, 0x90
.LBB0_28:                               #   Parent Loop BB0_22 Depth=1
                                        #     Parent Loop BB0_24 Depth=2
                                        #       Parent Loop BB0_27 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	2(%rbp,%r8,2), %r9d
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%r9d, %xmm5
	addsd	%xmm5, %xmm4
	incq	%r8
	cmpq	%rcx, %r8
	jl	.LBB0_28
# BB#29:                                # %._crit_edge151.us
                                        #   in Loop: Header=BB0_27 Depth=3
	incq	%rdx
	cmpq	%rbx, %rdx
	jl	.LBB0_27
.LBB0_30:                               # %._crit_edge154
                                        #   in Loop: Header=BB0_24 Depth=2
	testl	%edi, %edi
	je	.LBB0_31
# BB#35:                                #   in Loop: Header=BB0_24 Depth=2
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	xorps	%xmm4, %xmm4
	cvtsi2sdl	88(%r12), %xmm4
	divsd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	cvttsd2si	%xmm5, %ecx
	leaq	(,%rax,4), %rdx
	leaq	(%rdx,%rdx,2), %rdx
	movq	%r13, %rsi
	shlq	$7, %rsi
	leaq	(%rsi,%rsi,2), %rsi
	leaq	672(%rsp,%rsi), %r8
	cmpl	$128, %ecx
	movl	$127, %ebp
	cmovll	%ecx, %ebp
	cmpl	$-128, %ecx
	cmovll	%r10d, %ebp
	movl	%ebp, (%r8,%rdx)
	leaq	-96(%rsp,%rsi), %rcx
	addq	%rdx, %rcx
.LBB0_36:                               # %.sink.split208
                                        #   in Loop: Header=BB0_24 Depth=2
	movl	$32, (%rcx)
.LBB0_37:                               #   in Loop: Header=BB0_24 Depth=2
	leaq	(%rax,%rax,2), %rcx
	leaq	(%r13,%r13,2), %rdx
	shlq	$7, %rdx
	leaq	-96(%rsp,%rdx), %rdx
	movabsq	$137438953504, %rsi     # imm = 0x2000000020
	movq	%rsi, 4(%rdx,%rcx,4)
	incq	%rax
	cmpq	%r14, %rax
	jl	.LBB0_24
.LBB0_38:                               # %._crit_edge157
                                        #   in Loop: Header=BB0_22 Depth=1
	incq	%r13
	cmpq	-128(%rsp), %r13        # 8-byte Folded Reload
	jl	.LBB0_22
# BB#39:                                # %.preheader136.lr.ph
	movq	wp_weight(%rip), %r9
	movq	wp_offset(%rip), %r10
	leaq	-88(%rsp), %r11
	leaq	680(%rsp), %r14
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB0_40:                               # %.preheader136
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_42 Depth 2
	cmpl	$0, listXsize(,%r8,4)
	jle	.LBB0_43
# BB#41:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB0_40 Depth=1
	movq	(%r9,%r8,8), %r15
	movq	(%r10,%r8,8), %rax
	movq	%r14, %rsi
	movq	%r11, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB0_42:                               # %.preheader
                                        #   Parent Loop BB0_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r15,%rcx,8), %rbp
	movq	(%rax,%rcx,8), %rdi
	movl	-8(%rdx), %ebx
	movl	%ebx, (%rbp)
	movl	-8(%rsi), %ebx
	movl	%ebx, (%rdi)
	movl	-4(%rdx), %ebx
	movl	%ebx, 4(%rbp)
	movl	-4(%rsi), %ebx
	movl	%ebx, 4(%rdi)
	movl	(%rdx), %ebx
	movl	%ebx, 8(%rbp)
	movl	(%rsi), %ebx
	movl	%ebx, 8(%rdi)
	incq	%rcx
	movslq	listXsize(,%r8,4), %rdi
	addq	$12, %rdx
	addq	$12, %rsi
	cmpq	%rdi, %rcx
	jl	.LBB0_42
.LBB0_43:                               # %._crit_edge
                                        #   in Loop: Header=BB0_40 Depth=1
	incq	%r8
	addq	$384, %r11              # imm = 0x180
	addq	$384, %r14              # imm = 0x180
	cmpq	-128(%rsp), %r8         # 8-byte Folded Reload
	jl	.LBB0_40
# BB#44:                                # %._crit_edge149
	addq	$1448, %rsp             # imm = 0x5A8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	estimate_weighting_factor_P_slice, .Lfunc_end0-estimate_weighting_factor_P_slice
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4629700416936869888     # double 32
.LCPI1_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	estimate_weighting_factor_B_slice
	.p2align	4, 0x90
	.type	estimate_weighting_factor_B_slice,@function
estimate_weighting_factor_B_slice:      # @estimate_weighting_factor_B_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$78408, %rsp            # imm = 0x13248
.Lcfi19:
	.cfi_def_cfa_offset 78464
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rcx
	xorl	%eax, %eax
	cmpl	$0, 15268(%rcx)
	movq	%rcx, 56(%rsp)          # 8-byte Spill
	je	.LBB1_3
# BB#1:
	movq	14224(%rcx), %rdx
	movslq	12(%rcx), %rcx
	imulq	$536, %rcx, %rsi        # imm = 0x218
	cmpl	$0, 424(%rdx,%rsi)
	je	.LBB1_3
# BB#2:
	andl	$1, %ecx
	leal	2(%rcx,%rcx), %eax
.LBB1_3:                                # %.preheader294.lr.ph
	movl	$5, luma_log_weight_denom(%rip)
	movl	$5, chroma_log_weight_denom(%rip)
	movl	$16, wp_luma_round(%rip)
	movl	$16, wp_chroma_round(%rip)
	movabsq	$137438953504, %rcx     # imm = 0x2000000020
	movq	%rcx, 44(%rsp)
	movl	$32, 52(%rsp)
	addl	$2, %eax
	movq	wp_weight(%rip), %r8
	movq	wp_offset(%rip), %r9
	movslq	%eax, %r12
	leaq	64(%rsp), %r10
	leaq	2368(%rsp), %r11
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_4:                                # %.preheader294
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
	cmpl	$0, listXsize(,%r15,4)
	jle	.LBB1_7
# BB#5:                                 # %.preheader293.lr.ph
                                        #   in Loop: Header=BB1_4 Depth=1
	movq	(%r8,%r15,8), %r14
	movq	(%r9,%r15,8), %rbx
	movq	%r11, %rsi
	movq	%r10, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_6:                                # %.preheader293
                                        #   Parent Loop BB1_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rcx,8), %rax
	movq	(%rbx,%rcx,8), %rbp
	movl	$0, 8(%rdx)
	movq	$0, (%rdx)
	movl	52(%rsp), %edi
	movl	%edi, 8(%rsi)
	movq	44(%rsp), %rdi
	movq	%rdi, (%rsi)
	movl	$32, (%rax)
	movl	$0, (%rbp)
	movl	$32, 4(%rax)
	movl	$0, 4(%rbp)
	movl	$32, 8(%rax)
	movl	$0, 8(%rbp)
	incq	%rcx
	movslq	listXsize(,%r15,4), %rax
	addq	$12, %rdx
	addq	$12, %rsi
	cmpq	%rax, %rcx
	jl	.LBB1_6
.LBB1_7:                                # %._crit_edge339
                                        #   in Loop: Header=BB1_4 Depth=1
	incq	%r15
	addq	$384, %r10              # imm = 0x180
	addq	$384, %r11              # imm = 0x180
	cmpq	%r12, %r15
	jl	.LBB1_4
# BB#8:                                 # %.preheader292
	movq	%r12, 24(%rsp)          # 8-byte Spill
	movslq	listXsize(%rip), %r15
	testq	%r15, %r15
	movq	%r15, 32(%rsp)          # 8-byte Spill
	jle	.LBB1_18
# BB#9:                                 # %.preheader291.lr.ph
	movq	listX+8(%rip), %rdi
	movq	listX(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	enc_picture(%rip), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movslq	listXsize+4(%rip), %rbp
	leaq	16968(%rsp), %r14
	xorl	%ebx, %ebx
	movl	$-128, %esi
	movl	$127, %ecx
	movl	$-1024, %r13d           # imm = 0xFC00
	movl	$1023, %r8d             # imm = 0x3FF
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader291
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_12 Depth 2
	testl	%ebp, %ebp
	jle	.LBB1_17
# BB#11:                                # %.lr.ph333
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbx,8), %rax
	movl	4(%rax), %r9d
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	4(%rax), %r10d
	subl	%r9d, %r10d
	cmpl	$-129, %r10d
	cmovlel	%esi, %r10d
	cmpl	$128, %r10d
	cmovgel	%ecx, %r10d
	movq	%r14, %r12
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB1_12:                               #   Parent Loop BB1_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%r15,8), %rax
	movl	4(%rax), %r11d
	subl	%r9d, %r11d
	cmpl	$-129, %r11d
	cmovlel	%esi, %r11d
	cmpl	$128, %r11d
	cmovgel	%ecx, %r11d
	testl	%r11d, %r11d
	je	.LBB1_15
# BB#13:                                # %.split.preheader
                                        #   in Loop: Header=BB1_12 Depth=2
	movl	%r11d, %edx
	shrl	$31, %edx
	addl	%r11d, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %r11d
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%r11d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	imull	%r10d, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	cmovlel	%r13d, %eax
	cmpl	$1024, %eax             # imm = 0x400
	cmovgel	%r8d, %eax
	sarl	$2, %eax
	leal	64(%rax), %edx
	cmpl	$192, %edx
	jbe	.LBB1_14
# BB#81:                                #   in Loop: Header=BB1_12 Depth=2
	movl	$32, -8(%r12)
	movl	$32, -12296(%r12)
	movl	$32, -4(%r12)
	movl	$32, %edx
	movl	$32, %eax
	jmp	.LBB1_82
	.p2align	4, 0x90
.LBB1_15:                               # %.split.us.preheader
                                        #   in Loop: Header=BB1_12 Depth=2
	movl	$32, -8(%r12)
	movl	$32, -4(%r12)
	movabsq	$137438953504, %rax     # imm = 0x2000000020
	movq	%rax, -12296(%r12)
	movl	$32, (%r12)
	movl	$32, %edx
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_14:                               # %.split.2389
                                        #   in Loop: Header=BB1_12 Depth=2
	movl	%eax, -8(%r12)
	movl	$64, %edx
	subl	%eax, %edx
	movl	%edx, -12296(%r12)
	movl	%eax, -4(%r12)
.LBB1_82:                               # %.us-lcssa.us.loopexit346390
                                        #   in Loop: Header=BB1_12 Depth=2
	movl	%edx, -12292(%r12)
	movl	%eax, (%r12)
	movl	$64, %edx
	subl	%eax, %edx
.LBB1_16:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB1_12 Depth=2
	movl	%edx, -12288(%r12)
	incq	%r15
	addq	$12, %r12
	cmpq	%rbp, %r15
	jl	.LBB1_12
.LBB1_17:                               # %._crit_edge334
                                        #   in Loop: Header=BB1_10 Depth=1
	incq	%rbx
	addq	$384, %r14              # imm = 0x180
	movq	32(%rsp), %r15          # 8-byte Reload
	cmpq	%r15, %rbx
	jl	.LBB1_10
.LBB1_18:                               # %._crit_edge336
	movq	active_pps(%rip), %rax
	movl	196(%rax), %eax
	cmpl	$2, %eax
	jne	.LBB1_19
# BB#46:                                # %.preheader275
	testl	%r15d, %r15d
	jle	.LBB1_47
# BB#48:                                # %.preheader274.lr.ph
	movq	wbp_weight(%rip), %r8
	movl	listXsize+4(%rip), %esi
	leaq	16968(%rsp), %r9
	xorl	%r10d, %r10d
	movq	24(%rsp), %r11          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_49:                               # %.preheader274
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_51 Depth 2
	testl	%esi, %esi
	jle	.LBB1_53
# BB#50:                                # %.preheader273.lr.ph
                                        #   in Loop: Header=BB1_49 Depth=1
	movq	(%r8), %rax
	movq	8(%r8), %rcx
	movq	(%rcx,%r10,8), %rdi
	movq	(%rax,%r10,8), %rbp
	movq	%r9, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_51:                               # %.preheader273
                                        #   Parent Loop BB1_49 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rax,8), %rsi
	movq	(%rbp,%rax,8), %rbx
	movl	-8(%rcx), %edx
	movl	%edx, (%rsi)
	movl	-12296(%rcx), %edx
	movl	%edx, (%rbx)
	movl	-4(%rcx), %edx
	movl	%edx, 4(%rsi)
	movl	-12292(%rcx), %edx
	movl	%edx, 4(%rbx)
	movl	(%rcx), %edx
	movl	%edx, 8(%rsi)
	movl	-12288(%rcx), %edx
	movl	%edx, 8(%rbx)
	incq	%rax
	movslq	listXsize+4(%rip), %rsi
	addq	$12, %rcx
	cmpq	%rsi, %rax
	jl	.LBB1_51
# BB#52:                                # %._crit_edge299.loopexit
                                        #   in Loop: Header=BB1_49 Depth=1
	movl	listXsize(%rip), %r15d
.LBB1_53:                               # %._crit_edge299
                                        #   in Loop: Header=BB1_49 Depth=1
	incq	%r10
	movslq	%r15d, %rax
	addq	$384, %r9               # imm = 0x180
	cmpq	%rax, %r10
	jl	.LBB1_49
	jmp	.LBB1_54
.LBB1_19:                               # %.preheader290
	movl	%eax, 8(%rsp)           # 4-byte Spill
	movq	56(%rsp), %rcx          # 8-byte Reload
	movslq	68(%rcx), %rax
	testq	%rax, %rax
	xorpd	%xmm3, %xmm3
	jle	.LBB1_32
# BB#20:                                # %.preheader289.lr.ph
	cmpl	$0, 52(%rcx)
	xorpd	%xmm3, %xmm3
	jle	.LBB1_32
# BB#21:                                # %.preheader289.us.preheader
	movq	imgY_org(%rip), %rcx
	movq	img(%rip), %rdx
	movslq	52(%rdx), %rdx
	testb	$1, %al
	jne	.LBB1_23
# BB#22:
	xorpd	%xmm3, %xmm3
	xorl	%esi, %esi
	cmpl	$1, %eax
	jne	.LBB1_26
	jmp	.LBB1_31
.LBB1_47:
	movq	24(%rsp), %r11          # 8-byte Reload
.LBB1_54:                               # %.preheader.lr.ph
	movq	wp_weight(%rip), %rax
	movq	wp_offset(%rip), %rcx
	xorl	%edx, %edx
	testl	%r15d, %r15d
	jg	.LBB1_56
	jmp	.LBB1_58
	.p2align	4, 0x90
.LBB1_59:                               # %._crit_edge..preheader_crit_edge
                                        #   in Loop: Header=BB1_58 Depth=1
	movl	listXsize+4(,%rdx,4), %r15d
	movq	%rsi, %rdx
	testl	%r15d, %r15d
	jle	.LBB1_58
.LBB1_56:                               # %.lr.ph
	movq	(%rax,%rdx,8), %rsi
	movq	(%rcx,%rdx,8), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_57:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbp,8), %rbx
	movl	$32, (%rbx)
	movl	$32, 4(%rbx)
	movl	$32, 8(%rbx)
	movq	(%rdi,%rbp,8), %rbx
	movl	$0, (%rbx)
	movl	$0, 4(%rbx)
	movl	$0, 8(%rbx)
	incq	%rbp
	movslq	listXsize(,%rdx,4), %rbx
	cmpq	%rbx, %rbp
	jl	.LBB1_57
.LBB1_58:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rsi
	cmpq	%r11, %rsi
	jl	.LBB1_59
	jmp	.LBB1_79
.LBB1_23:                               # %.preheader289.us.prol
	movq	(%rcx), %rdi
	xorpd	%xmm3, %xmm3
	xorl	%ebp, %ebp
	movl	$1, %esi
	.p2align	4, 0x90
.LBB1_24:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi,%rbp,2), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addsd	%xmm0, %xmm3
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB1_24
# BB#25:                                # %.preheader289.us.prol.loopexit
	cmpl	$1, %eax
	je	.LBB1_31
	.p2align	4, 0x90
.LBB1_26:                               # %.preheader289.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_27 Depth 2
                                        #     Child Loop BB1_29 Depth 2
	movq	(%rcx,%rsi,8), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_27:                               #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdi,%rbp,2), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addsd	%xmm0, %xmm3
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB1_27
# BB#28:                                # %._crit_edge327.us
                                        #   in Loop: Header=BB1_26 Depth=1
	movq	8(%rcx,%rsi,8), %rdi
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_29:                               #   Parent Loop BB1_26 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdi,%rbp,2), %ebx
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ebx, %xmm0
	addsd	%xmm0, %xmm3
	incq	%rbp
	cmpq	%rdx, %rbp
	jl	.LBB1_29
# BB#30:                                # %._crit_edge327.us.1
                                        #   in Loop: Header=BB1_26 Depth=1
	addq	$2, %rsi
	cmpq	%rax, %rsi
	jl	.LBB1_26
.LBB1_31:                               # %.preheader287.lr.ph.loopexit
	mulsd	.LCPI1_0(%rip), %xmm3
.LBB1_32:                               # %.preheader287.lr.ph
	xorl	%r13d, %r13d
	movl	$32, %r14d
	movl	%r15d, %eax
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	jg	.LBB1_34
	.p2align	4, 0x90
.LBB1_44:                               # %._crit_edge321
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r13), %rcx
	movq	24(%rsp), %r12          # 8-byte Reload
	cmpq	%r12, %rcx
	jge	.LBB1_60
# BB#45:                                # %._crit_edge321..preheader287_crit_edge
                                        #   in Loop: Header=BB1_44 Depth=1
	movl	listXsize+4(,%r13,4), %eax
	movq	%rcx, %r13
	testl	%eax, %eax
	jle	.LBB1_44
.LBB1_34:                               # %.lr.ph320
	leaq	(%r13,%r13,2), %r15
	shlq	$7, %r15
	leaq	64(%rsp,%r15), %rdi
	movslq	%eax, %r12
	decl	%eax
	leaq	(%rax,%rax,2), %rax
	leaq	12(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movsd	.LCPI1_1(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	xorps	%xmm2, %xmm2
	movq	listX(,%r13,8), %r8
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB1_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_38 Depth 2
                                        #       Child Loop BB1_39 Depth 3
	movq	(%r8,%r10,8), %rax
	movq	6448(%rax), %rax
	movq	%rax, ref_qpic_sub(%rip)
	movq	img(%rip), %rdx
	movslq	68(%rdx), %rsi
	testq	%rsi, %rsi
	movl	$32, %r9d
	jle	.LBB1_43
# BB#36:                                # %.preheader286.lr.ph
                                        #   in Loop: Header=BB1_35 Depth=1
	movslq	52(%rdx), %rdi
	testq	%rdi, %rdi
	jle	.LBB1_43
# BB#37:                                # %.preheader286.us.preheader
                                        #   in Loop: Header=BB1_35 Depth=1
	movq	(%rax), %rax
	movq	(%rax), %rbp
	addq	$20, %rsi
	addq	$19, %rdi
	xorpd	%xmm0, %xmm0
	movl	$20, %ebx
	.p2align	4, 0x90
.LBB1_38:                               # %.preheader286.us
                                        #   Parent Loop BB1_35 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_39 Depth 3
	movq	(%rbp,%rbx,8), %rax
	movl	$19, %edx
	.p2align	4, 0x90
.LBB1_39:                               #   Parent Loop BB1_35 Depth=1
                                        #     Parent Loop BB1_38 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	2(%rax,%rdx,2), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rdx
	cmpq	%rdi, %rdx
	jl	.LBB1_39
# BB#40:                                # %._crit_edge315.us
                                        #   in Loop: Header=BB1_38 Depth=2
	incq	%rbx
	cmpq	%rsi, %rbx
	jl	.LBB1_38
# BB#41:                                # %._crit_edge318
                                        #   in Loop: Header=BB1_35 Depth=1
	ucomisd	%xmm2, %xmm0
	jne	.LBB1_42
	jnp	.LBB1_43
.LBB1_42:                               #   in Loop: Header=BB1_35 Depth=1
	movapd	%xmm3, %xmm1
	divsd	%xmm0, %xmm1
	addsd	%xmm4, %xmm1
	cvttsd2si	%xmm1, %r9d
.LBB1_43:                               # %._crit_edge318.thread
                                        #   in Loop: Header=BB1_35 Depth=1
	movl	%r9d, %eax
	subl	$-128, %eax
	cmpl	$255, %eax
	cmoval	%r14d, %r9d
	leaq	(%r10,%r10,2), %rax
	leaq	2368(%rsp,%r15), %rcx
	movl	%r9d, (%rcx,%rax,4)
	movl	$32, 4(%rcx,%rax,4)
	movl	$32, 8(%rcx,%rax,4)
	incq	%r10
	cmpq	%r12, %r10
	jl	.LBB1_35
	jmp	.LBB1_44
.LBB1_60:                               # %._crit_edge323
	cmpl	$1, 8(%rsp)             # 4-byte Folded Reload
	movq	wp_weight(%rip), %r8
	movq	wp_offset(%rip), %r9
	jne	.LBB1_61
# BB#67:                                # %.preheader281.preheader
	leaq	2376(%rsp), %r10
	leaq	72(%rsp), %r11
	xorl	%r14d, %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
	testl	%eax, %eax
	jg	.LBB1_69
	jmp	.LBB1_71
	.p2align	4, 0x90
.LBB1_80:                               # %._crit_edge307..preheader281_crit_edge
                                        #   in Loop: Header=BB1_71 Depth=1
	movl	listXsize+4(,%r14,4), %eax
	addq	$384, %r10              # imm = 0x180
	addq	$384, %r11              # imm = 0x180
	movq	%rcx, %r14
	testl	%eax, %eax
	jle	.LBB1_71
.LBB1_69:                               # %.preheader280.lr.ph
	movq	(%r8,%r14,8), %r15
	movq	(%r9,%r14,8), %rbx
	movq	%r11, %rsi
	movq	%r10, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_70:                               # %.preheader280
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rcx,8), %rax
	movq	(%rbx,%rcx,8), %rdi
	movl	-8(%rdx), %ebp
	movl	%ebp, (%rax)
	movl	-8(%rsi), %ebp
	movl	%ebp, (%rdi)
	movl	-4(%rdx), %ebp
	movl	%ebp, 4(%rax)
	movl	-4(%rsi), %ebp
	movl	%ebp, 4(%rdi)
	movl	(%rdx), %ebp
	movl	%ebp, 8(%rax)
	movl	(%rsi), %eax
	movl	%eax, 8(%rdi)
	incq	%rcx
	movslq	listXsize(,%r14,4), %rax
	addq	$12, %rdx
	addq	$12, %rsi
	cmpq	%rax, %rcx
	jl	.LBB1_70
.LBB1_71:                               # %._crit_edge307
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r14), %rcx
	cmpq	%r12, %rcx
	jl	.LBB1_80
	jmp	.LBB1_72
.LBB1_61:                               # %.preheader283.preheader
	xorl	%edx, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	jg	.LBB1_63
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_66:                               # %._crit_edge311..preheader283_crit_edge
                                        #   in Loop: Header=BB1_65 Depth=1
	movl	listXsize+4(,%rdx,4), %ecx
	movq	%rax, %rdx
	testl	%ecx, %ecx
	jle	.LBB1_65
.LBB1_63:                               # %.lr.ph310
	movq	(%r8,%rdx,8), %rax
	movq	(%r9,%rdx,8), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_64:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rsi,8), %rdi
	movl	$32, (%rdi)
	movl	$32, 4(%rdi)
	movl	$32, 8(%rdi)
	movq	(%rcx,%rsi,8), %rdi
	movl	$0, (%rdi)
	movl	$0, 4(%rdi)
	movl	$0, 8(%rdi)
	incq	%rsi
	movslq	listXsize(,%rdx,4), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB1_64
.LBB1_65:                               # %._crit_edge311
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rax
	cmpq	%r12, %rax
	jl	.LBB1_66
.LBB1_72:                               # %.preheader278
	movl	listXsize(%rip), %edi
	testl	%edi, %edi
	jle	.LBB1_79
# BB#73:                                # %.preheader277.lr.ph
	movq	wp_weight(%rip), %r8
	movq	wbp_weight(%rip), %r9
	movl	listXsize+4(%rip), %esi
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB1_74:                               # %.preheader277
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_76 Depth 2
	testl	%esi, %esi
	jle	.LBB1_78
# BB#75:                                # %.preheader276.lr.ph
                                        #   in Loop: Header=BB1_74 Depth=1
	movq	(%r8), %rax
	movq	8(%r8), %r11
	movq	(%rax,%r10,8), %rbp
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	(%rax,%r10,8), %r14
	movq	(%rcx,%r10,8), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_76:                               # %.preheader276
                                        #   Parent Loop BB1_74 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rcx,8), %rsi
	movq	(%r11,%rcx,8), %rdx
	movq	(%rax,%rcx,8), %rdi
	movl	(%rbp), %ebx
	movl	%ebx, (%rsi)
	movl	(%rdx), %ebx
	movl	%ebx, (%rdi)
	movl	4(%rbp), %ebx
	movl	%ebx, 4(%rsi)
	movl	4(%rdx), %ebx
	movl	%ebx, 4(%rdi)
	movl	8(%rbp), %ebx
	movl	%ebx, 8(%rsi)
	movl	8(%rdx), %edx
	movl	%edx, 8(%rdi)
	incq	%rcx
	movslq	listXsize+4(%rip), %rsi
	cmpq	%rsi, %rcx
	jl	.LBB1_76
# BB#77:                                # %._crit_edge303.loopexit
                                        #   in Loop: Header=BB1_74 Depth=1
	movl	listXsize(%rip), %edi
.LBB1_78:                               # %._crit_edge303
                                        #   in Loop: Header=BB1_74 Depth=1
	incq	%r10
	movslq	%edi, %rax
	cmpq	%rax, %r10
	jl	.LBB1_74
.LBB1_79:                               # %.loopexit
	addq	$78408, %rsp            # imm = 0x13248
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	estimate_weighting_factor_B_slice, .Lfunc_end1-estimate_weighting_factor_B_slice
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI2_0:
	.quad	4629700416936869888     # double 32
.LCPI2_1:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	test_wp_P_slice
	.p2align	4, 0x90
	.type	test_wp_P_slice,@function
test_wp_P_slice:                        # @test_wp_P_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi30:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi31:
	.cfi_def_cfa_offset 56
	subq	$1432, %rsp             # imm = 0x598
.Lcfi32:
	.cfi_def_cfa_offset 1488
.Lcfi33:
	.cfi_offset %rbx, -56
.Lcfi34:
	.cfi_offset %r12, -48
.Lcfi35:
	.cfi_offset %r13, -40
.Lcfi36:
	.cfi_offset %r14, -32
.Lcfi37:
	.cfi_offset %r15, -24
.Lcfi38:
	.cfi_offset %rbp, -16
	movq	img(%rip), %r8
	cmpl	$0, 15268(%r8)
	je	.LBB2_3
# BB#1:
	movq	14224(%r8), %rcx
	movslq	12(%r8), %rax
	imulq	$536, %rax, %rdx        # imm = 0x218
	cmpl	$0, 424(%rcx,%rdx)
	je	.LBB2_3
# BB#2:
	andl	$1, %eax
	leal	4(%rax,%rax), %eax
	jmp	.LBB2_4
.LBB2_3:
	movl	$2, %eax
.LBB2_4:                                # %.preheader168.lr.ph
	movq	%rax, -120(%rsp)        # 8-byte Spill
	movl	$5, luma_log_weight_denom(%rip)
	movl	$5, chroma_log_weight_denom(%rip)
	movl	$16, wp_luma_round(%rip)
	movl	$16, wp_chroma_round(%rip)
	movq	wp_weight(%rip), %r9
	movq	wp_offset(%rip), %r10
	leaq	-104(%rsp), %r14
	leaq	656(%rsp), %r15
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB2_5:                                # %.preheader168
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_7 Depth 2
	cmpl	$0, listXsize(,%r11,4)
	jle	.LBB2_8
# BB#6:                                 # %.preheader167.lr.ph
                                        #   in Loop: Header=BB2_5 Depth=1
	movq	(%r9,%r11,8), %r12
	movq	(%r10,%r11,8), %rax
	movq	%r14, %rsi
	movq	%r15, %rbp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_7:                                # %.preheader167
                                        #   Parent Loop BB2_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r12,%rdx,8), %rbx
	movq	(%rax,%rdx,8), %rcx
	movl	$0, 8(%rbp)
	movq	$0, (%rbp)
	movl	$32, -8(%rsi)
	movl	$32, (%rbx)
	movl	$0, (%rcx)
	movl	$32, -4(%rsi)
	movl	$32, 4(%rbx)
	movl	$0, 4(%rcx)
	movl	$32, (%rsi)
	movl	$32, 8(%rbx)
	movl	$0, 8(%rcx)
	incq	%rdx
	movslq	listXsize(,%r11,4), %rcx
	addq	$12, %rbp
	addq	$12, %rsi
	cmpq	%rcx, %rdx
	jl	.LBB2_7
.LBB2_8:                                # %._crit_edge211
                                        #   in Loop: Header=BB2_5 Depth=1
	incq	%r11
	addq	$384, %r15              # imm = 0x180
	addq	$384, %r14              # imm = 0x180
	cmpq	-120(%rsp), %r11        # 8-byte Folded Reload
	jl	.LBB2_5
# BB#9:                                 # %.preheader166
	movslq	68(%r8), %r9
	testq	%r9, %r9
	xorps	%xmm0, %xmm0
	xorpd	%xmm1, %xmm1
	jle	.LBB2_21
# BB#10:                                # %.preheader165.lr.ph
	cmpl	$0, 52(%r8)
	xorpd	%xmm1, %xmm1
	jle	.LBB2_21
# BB#11:                                # %.preheader165.us.preheader
	movq	imgY_org(%rip), %rcx
	movq	img(%rip), %rdx
	movslq	52(%rdx), %rdx
	testb	$1, %r9b
	jne	.LBB2_13
# BB#12:
	xorpd	%xmm1, %xmm1
	xorl	%esi, %esi
	cmpl	$1, %r9d
	jne	.LBB2_16
	jmp	.LBB2_21
.LBB2_13:                               # %.preheader165.us.prol
	movq	(%rcx), %rbp
	xorpd	%xmm1, %xmm1
	xorl	%ebx, %ebx
	movl	$1, %esi
	.p2align	4, 0x90
.LBB2_14:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rbp,%rbx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm2, %xmm1
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB2_14
# BB#15:                                # %.preheader165.us.prol.loopexit
	cmpl	$1, %r9d
	je	.LBB2_21
	.p2align	4, 0x90
.LBB2_16:                               # %.preheader165.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_17 Depth 2
                                        #     Child Loop BB2_19 Depth 2
	movq	(%rcx,%rsi,8), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_17:                               #   Parent Loop BB2_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbp,%rbx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm2, %xmm1
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB2_17
# BB#18:                                # %._crit_edge204.us
                                        #   in Loop: Header=BB2_16 Depth=1
	movq	8(%rcx,%rsi,8), %rbp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB2_19:                               #   Parent Loop BB2_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rbp,%rbx,2), %eax
	xorps	%xmm2, %xmm2
	cvtsi2sdl	%eax, %xmm2
	addsd	%xmm2, %xmm1
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB2_19
# BB#20:                                # %._crit_edge204.us.1
                                        #   in Loop: Header=BB2_16 Depth=1
	addq	$2, %rsi
	cmpq	%r9, %rsi
	jl	.LBB2_16
.LBB2_21:                               # %.preheader163.lr.ph
	movsd	.LCPI2_0(%rip), %xmm2   # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	xorl	%r13d, %r13d
	movsd	.LCPI2_1(%rip), %xmm3   # xmm3 = mem[0],zero
	movabsq	$137438953504, %r9      # imm = 0x2000000020
	.p2align	4, 0x90
.LBB2_22:                               # %.preheader163
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_28 Depth 2
                                        #       Child Loop BB2_31 Depth 3
                                        #         Child Loop BB2_32 Depth 4
	movslq	listXsize(,%r13,4), %r14
	testq	%r14, %r14
	jle	.LBB2_38
# BB#23:                                # %.lr.ph198
                                        #   in Loop: Header=BB2_22 Depth=1
	movq	listX(,%r13,8), %r10
	movq	img(%rip), %r12
	movslq	68(%r12), %r15
	movq	%r15, %rbx
	addq	$20, %rbx
	xorl	%esi, %esi
	jmp	.LBB2_28
	.p2align	4, 0x90
.LBB2_24:                               #   in Loop: Header=BB2_28 Depth=2
	ucomisd	%xmm0, %xmm4
	jne	.LBB2_25
	jnp	.LBB2_27
.LBB2_25:                               #   in Loop: Header=BB2_28 Depth=2
	movapd	%xmm2, %xmm5
	divsd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	cvttsd2si	%xmm5, %edx
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r13,%r13,2), %rcx
	shlq	$7, %rcx
	leaq	-112(%rsp,%rcx), %rcx
	movl	%edx, (%rcx,%rax,4)
	addl	$64, %edx
	cmpl	$191, %edx
	jbe	.LBB2_37
# BB#26:                                #   in Loop: Header=BB2_28 Depth=2
	leaq	(%rcx,%rax,4), %rax
	jmp	.LBB2_36
.LBB2_27:                               # %.thread261
                                        #   in Loop: Header=BB2_28 Depth=2
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r13,%r13,2), %rcx
	shlq	$7, %rcx
	leaq	-112(%rsp,%rcx), %rcx
	leaq	(%rcx,%rax,4), %rax
	jmp	.LBB2_36
	.p2align	4, 0x90
.LBB2_28:                               #   Parent Loop BB2_22 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB2_31 Depth 3
                                        #         Child Loop BB2_32 Depth 4
	movq	(%r10,%rsi,8), %rax
	movq	6448(%rax), %rax
	movq	%rax, ref_pic_sub(%rip)
	xorpd	%xmm4, %xmm4
	testl	%r15d, %r15d
	jle	.LBB2_34
# BB#29:                                # %.preheader162.lr.ph
                                        #   in Loop: Header=BB2_28 Depth=2
	movslq	52(%r12), %rbp
	testq	%rbp, %rbp
	jle	.LBB2_34
# BB#30:                                # %.preheader162.us.preheader
                                        #   in Loop: Header=BB2_28 Depth=2
	movq	(%rax), %rax
	movq	(%rax), %rdx
	addq	$19, %rbp
	xorpd	%xmm4, %xmm4
	movl	$20, %ecx
	.p2align	4, 0x90
.LBB2_31:                               # %.preheader162.us
                                        #   Parent Loop BB2_22 Depth=1
                                        #     Parent Loop BB2_28 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB2_32 Depth 4
	movq	(%rdx,%rcx,8), %rax
	movl	$19, %r11d
	.p2align	4, 0x90
.LBB2_32:                               #   Parent Loop BB2_22 Depth=1
                                        #     Parent Loop BB2_28 Depth=2
                                        #       Parent Loop BB2_31 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movzwl	2(%rax,%r11,2), %r8d
	xorps	%xmm5, %xmm5
	cvtsi2sdl	%r8d, %xmm5
	addsd	%xmm5, %xmm4
	incq	%r11
	cmpq	%rbp, %r11
	jl	.LBB2_32
# BB#33:                                # %._crit_edge192.us
                                        #   in Loop: Header=BB2_31 Depth=3
	incq	%rcx
	cmpq	%rbx, %rcx
	jl	.LBB2_31
.LBB2_34:                               # %._crit_edge195
                                        #   in Loop: Header=BB2_28 Depth=2
	testl	%edi, %edi
	je	.LBB2_24
# BB#35:                                #   in Loop: Header=BB2_28 Depth=2
	movapd	%xmm1, %xmm5
	subsd	%xmm4, %xmm5
	xorps	%xmm4, %xmm4
	cvtsi2sdl	88(%r12), %xmm4
	divsd	%xmm4, %xmm5
	addsd	%xmm3, %xmm5
	cvttsd2si	%xmm5, %eax
	leaq	(,%rsi,4), %rcx
	leaq	(%rcx,%rcx,2), %rcx
	movq	%r13, %rdx
	shlq	$7, %rdx
	leaq	(%rdx,%rdx,2), %rdx
	leaq	656(%rsp,%rdx), %r8
	cmpl	$128, %eax
	movl	$127, %ebp
	cmovll	%eax, %ebp
	cmpl	$-128, %eax
	movl	$-128, %eax
	cmovll	%eax, %ebp
	movl	%ebp, (%r8,%rcx)
	leaq	-112(%rsp,%rdx), %rax
	addq	%rcx, %rax
.LBB2_36:                               # %.sink.split269
                                        #   in Loop: Header=BB2_28 Depth=2
	movl	$32, (%rax)
.LBB2_37:                               #   in Loop: Header=BB2_28 Depth=2
	leaq	(%rsi,%rsi,2), %rax
	leaq	(%r13,%r13,2), %rcx
	shlq	$7, %rcx
	leaq	-112(%rsp,%rcx), %rcx
	movq	%r9, 4(%rcx,%rax,4)
	incq	%rsi
	cmpq	%r14, %rsi
	jl	.LBB2_28
.LBB2_38:                               # %._crit_edge199
                                        #   in Loop: Header=BB2_22 Depth=1
	incq	%r13
	cmpq	-120(%rsp), %r13        # 8-byte Folded Reload
	jl	.LBB2_22
# BB#39:                                # %.preheader159.lr.ph
	movq	input(%rip), %r9
	movq	active_sps(%rip), %r8
	leaq	-104(%rsp), %r14
	leaq	664(%rsp), %r15
	xorl	%r10d, %r10d
	movl	$1, %r11d
	xorl	%eax, %eax
.LBB2_40:                               # %.preheader159
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB2_43 Depth 2
                                        #     Child Loop BB2_58 Depth 2
	movslq	listXsize(,%r10,4), %r12
	testq	%r12, %r12
	jle	.LBB2_65
# BB#41:                                # %.preheader.lr.ph
                                        #   in Loop: Header=BB2_40 Depth=1
	cmpl	$0, 2952(%r9)
	je	.LBB2_57
# BB#42:                                # %.preheader.preheader
                                        #   in Loop: Header=BB2_40 Depth=1
	movl	4(%r8), %r13d
	movq	%r15, %rsi
	movq	%r14, %rdi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB2_43:                               #   Parent Loop BB2_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	$66, %r13d
	movl	-8(%rsi), %ebp
	jne	.LBB2_45
# BB#44:                                #   in Loop: Header=BB2_43 Depth=2
	testl	%ebp, %ebp
	setne	%cl
	jmp	.LBB2_46
	.p2align	4, 0x90
.LBB2_45:                               #   in Loop: Header=BB2_43 Depth=2
	movl	%ebp, %ecx
	negl	%ecx
	cmovll	%ebp, %ecx
	cmpl	$2, %ecx
	setg	%cl
.LBB2_46:                               #   in Loop: Header=BB2_43 Depth=2
	cmpl	$32, -8(%rdi)
	setne	%bl
	orb	%cl, %bl
	cmovnel	%r11d, %eax
	cmpb	$1, %bl
	je	.LBB2_55
# BB#47:                                #   in Loop: Header=BB2_43 Depth=2
	cmpl	$66, %r13d
	movl	-4(%rsi), %ecx
	jne	.LBB2_49
# BB#48:                                #   in Loop: Header=BB2_43 Depth=2
	testl	%ecx, %ecx
	setne	%cl
	jmp	.LBB2_50
	.p2align	4, 0x90
.LBB2_49:                               #   in Loop: Header=BB2_43 Depth=2
	movl	%ecx, %ebx
	negl	%ebx
	cmovll	%ecx, %ebx
	cmpl	$2, %ebx
	setg	%cl
.LBB2_50:                               #   in Loop: Header=BB2_43 Depth=2
	cmpl	$32, -4(%rdi)
	setne	%bl
	orb	%cl, %bl
	cmovnel	%r11d, %eax
	jne	.LBB2_55
# BB#51:                                #   in Loop: Header=BB2_43 Depth=2
	cmpl	$66, %r13d
	movl	(%rsi), %ecx
	jne	.LBB2_53
# BB#52:                                #   in Loop: Header=BB2_43 Depth=2
	testl	%ecx, %ecx
	setne	%cl
	jmp	.LBB2_54
.LBB2_53:                               #   in Loop: Header=BB2_43 Depth=2
	movl	%ecx, %ebx
	negl	%ebx
	cmovll	%ecx, %ebx
	cmpl	$2, %ebx
	setg	%cl
.LBB2_54:                               #   in Loop: Header=BB2_43 Depth=2
	cmpl	$32, (%rdi)
	cmovnel	%r11d, %eax
	testb	%cl, %cl
	cmovnel	%r11d, %eax
.LBB2_55:                               # %.us-lcssa
                                        #   in Loop: Header=BB2_43 Depth=2
	cmpl	$1, %eax
	je	.LBB2_67
# BB#56:                                #   in Loop: Header=BB2_43 Depth=2
	incq	%rdx
	addq	$12, %rdi
	addq	$12, %rsi
	cmpq	%r12, %rdx
	jl	.LBB2_43
	jmp	.LBB2_65
	.p2align	4, 0x90
.LBB2_57:                               # %.preheader.us.preheader
                                        #   in Loop: Header=BB2_40 Depth=1
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB2_58:                               #   Parent Loop BB2_40 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	-8(%r14,%rdx), %ecx
	cmpl	$32, %ecx
	cmovnel	%r11d, %eax
	cmpl	$0, -8(%r15,%rdx)
	cmovnel	%r11d, %eax
	jne	.LBB2_63
# BB#59:                                #   in Loop: Header=BB2_58 Depth=2
	cmpl	$32, %ecx
	jne	.LBB2_63
# BB#60:                                #   in Loop: Header=BB2_58 Depth=2
	movl	-4(%r14,%rdx), %ecx
	cmpl	$32, %ecx
	cmovnel	%r11d, %eax
	cmpl	$0, -4(%r15,%rdx)
	cmovnel	%r11d, %eax
	jne	.LBB2_63
# BB#61:                                #   in Loop: Header=BB2_58 Depth=2
	cmpl	$32, %ecx
	jne	.LBB2_63
# BB#62:                                #   in Loop: Header=BB2_58 Depth=2
	cmpl	$32, (%r14,%rdx)
	cmovnel	%r11d, %eax
	cmpl	$0, (%r15,%rdx)
	cmovnel	%r11d, %eax
.LBB2_63:                               # %.us-lcssa.us.us
                                        #   in Loop: Header=BB2_58 Depth=2
	cmpl	$1, %eax
	je	.LBB2_67
# BB#64:                                #   in Loop: Header=BB2_58 Depth=2
	incq	%rsi
	addq	$12, %rdx
	cmpq	%r12, %rsi
	jl	.LBB2_58
	.p2align	4, 0x90
.LBB2_65:                               # %._crit_edge
                                        #   in Loop: Header=BB2_40 Depth=1
	incq	%r10
	addq	$384, %r14              # imm = 0x180
	addq	$384, %r15              # imm = 0x180
	cmpq	-120(%rsp), %r10        # 8-byte Folded Reload
	jl	.LBB2_40
	jmp	.LBB2_68
.LBB2_67:
	movl	$1, %eax
.LBB2_68:                               # %.thread
	addq	$1432, %rsp             # imm = 0x598
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	test_wp_P_slice, .Lfunc_end2-test_wp_P_slice
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4602678819172646912     # double 0.5
	.text
	.globl	test_wp_B_slice
	.p2align	4, 0x90
	.type	test_wp_B_slice,@function
test_wp_B_slice:                        # @test_wp_B_slice
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$78424, %rsp            # imm = 0x13258
.Lcfi45:
	.cfi_def_cfa_offset 78480
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movl	%edi, 12(%rsp)          # 4-byte Spill
	movq	img(%rip), %rax
	xorl	%edx, %edx
	cmpl	$0, 15268(%rax)
	movq	%rax, 64(%rsp)          # 8-byte Spill
	je	.LBB3_3
# BB#1:
	movq	14224(%rax), %rcx
	movslq	12(%rax), %rax
	imulq	$536, %rax, %rsi        # imm = 0x218
	cmpl	$0, 424(%rcx,%rsi)
	je	.LBB3_3
# BB#2:
	andl	$1, %eax
	leal	2(%rax,%rax), %edx
.LBB3_3:                                # %.preheader328.lr.ph
	xorl	%ecx, %ecx
	cmpl	$1, 12(%rsp)            # 4-byte Folded Reload
	setne	%cl
	leal	5(%rcx), %eax
	movl	%eax, luma_log_weight_denom(%rip)
	movl	%eax, chroma_log_weight_denom(%rip)
	orl	$4, %ecx
	movl	$1, %r12d
	movl	$1, %esi
                                        # kill: %CL<def> %CL<kill> %RCX<kill>
	shll	%cl, %esi
	movl	%esi, wp_luma_round(%rip)
	movl	%esi, wp_chroma_round(%rip)
	movl	%eax, %ecx
	shll	%cl, %r12d
	movl	%r12d, 44(%rsp)
	movl	%r12d, 48(%rsp)
	movl	%r12d, 52(%rsp)
	addl	$2, %edx
	movq	wp_weight(%rip), %r8
	movq	wp_offset(%rip), %r9
	movslq	%edx, %r13
	leaq	80(%rsp), %r10
	leaq	2384(%rsp), %r11
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB3_4:                                # %.preheader328
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_6 Depth 2
	cmpl	$0, listXsize(,%r15,4)
	jle	.LBB3_7
# BB#5:                                 # %.preheader327.lr.ph
                                        #   in Loop: Header=BB3_4 Depth=1
	movq	(%r8,%r15,8), %r14
	movq	(%r9,%r15,8), %rax
	movq	%r11, %rsi
	movq	%r10, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_6:                                # %.preheader327
                                        #   Parent Loop BB3_4 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rcx,8), %rbp
	movq	(%rax,%rcx,8), %rbx
	movl	$0, 8(%rdx)
	movq	$0, (%rdx)
	movl	52(%rsp), %edi
	movl	%edi, 8(%rsi)
	movq	44(%rsp), %rdi
	movq	%rdi, (%rsi)
	movl	%r12d, (%rbp)
	movl	$0, (%rbx)
	movl	%r12d, 4(%rbp)
	movl	$0, 4(%rbx)
	movl	%r12d, 8(%rbp)
	movl	$0, 8(%rbx)
	incq	%rcx
	movslq	listXsize(,%r15,4), %rdi
	addq	$12, %rdx
	addq	$12, %rsi
	cmpq	%rdi, %rcx
	jl	.LBB3_6
.LBB3_7:                                # %._crit_edge377
                                        #   in Loop: Header=BB3_4 Depth=1
	incq	%r15
	addq	$384, %r10              # imm = 0x180
	addq	$384, %r11              # imm = 0x180
	cmpq	%r13, %r15
	jl	.LBB3_4
# BB#8:                                 # %.preheader326
	movq	%r13, 24(%rsp)          # 8-byte Spill
	movslq	listXsize(%rip), %r11
	testq	%r11, %r11
	movq	%r11, 32(%rsp)          # 8-byte Spill
	jle	.LBB3_17
# BB#9:                                 # %.preheader325.lr.ph
	movq	listX+8(%rip), %rcx
	movq	listX(%rip), %rax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	enc_picture(%rip), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movslq	listXsize+4(%rip), %rsi
	leaq	16984(%rsp), %rbp
	xorl	%r13d, %r13d
	movl	$-128, %edi
	movl	$127, %ebx
	movl	$32, %r9d
	.p2align	4, 0x90
.LBB3_10:                               # %.preheader325
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_12 Depth 2
	testl	%esi, %esi
	jle	.LBB3_16
# BB#11:                                # %.lr.ph371
                                        #   in Loop: Header=BB3_10 Depth=1
	movq	16(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rax
	movl	4(%rax), %r10d
	movq	72(%rsp), %rax          # 8-byte Reload
	movl	4(%rax), %r15d
	subl	%r10d, %r15d
	cmpl	$-129, %r15d
	cmovlel	%edi, %r15d
	cmpl	$128, %r15d
	cmovgel	%ebx, %r15d
	movq	%rbp, %r11
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB3_12:                               #   Parent Loop BB3_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rcx,%r14,8), %rax
	movl	4(%rax), %r8d
	subl	%r10d, %r8d
	cmpl	$-129, %r8d
	cmovlel	%edi, %r8d
	cmpl	$128, %r8d
	cmovgel	%ebx, %r8d
	testl	%r8d, %r8d
	je	.LBB3_14
# BB#13:                                # %.split.preheader
                                        #   in Loop: Header=BB3_12 Depth=2
	movl	%r8d, %edx
	shrl	$31, %edx
	addl	%r8d, %edx
	sarl	%edx
	movl	%edx, %eax
	negl	%eax
	cmpl	$-1, %r8d
	cmovgel	%edx, %eax
	addl	$16384, %eax            # imm = 0x4000
	cltd
	idivl	%r8d
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	imull	%r15d, %eax
	addl	$32, %eax
	sarl	$6, %eax
	cmpl	$-1025, %eax            # imm = 0xFBFF
	movl	$-1024, %edx            # imm = 0xFC00
	cmovlel	%edx, %eax
	cmpl	$1024, %eax             # imm = 0x400
	movl	$1023, %edx             # imm = 0x3FF
	cmovgel	%edx, %eax
	sarl	$2, %eax
	leal	64(%rax), %edx
	cmpl	$192, %edx
	cmoval	%r9d, %eax
	movl	%eax, -8(%r11)
	movl	$64, %edx
	subl	%eax, %edx
	jmp	.LBB3_15
	.p2align	4, 0x90
.LBB3_14:                               # %.split.us.preheader
                                        #   in Loop: Header=BB3_12 Depth=2
	movl	%r12d, -8(%r11)
	movl	%r12d, %edx
	movl	%r12d, %eax
.LBB3_15:                               # %.us-lcssa.us
                                        #   in Loop: Header=BB3_12 Depth=2
	movl	%edx, -12296(%r11)
	movl	%eax, -4(%r11)
	movl	%edx, -12292(%r11)
	movl	%eax, (%r11)
	movl	%edx, -12288(%r11)
	incq	%r14
	addq	$12, %r11
	cmpq	%rsi, %r14
	jl	.LBB3_12
.LBB3_16:                               # %._crit_edge372
                                        #   in Loop: Header=BB3_10 Depth=1
	incq	%r13
	addq	$384, %rbp              # imm = 0x180
	movq	32(%rsp), %r11          # 8-byte Reload
	cmpq	%r11, %r13
	jl	.LBB3_10
.LBB3_17:                               # %._crit_edge374
	cmpl	$1, 12(%rsp)            # 4-byte Folded Reload
	jne	.LBB3_18
# BB#44:                                # %.preheader309
	testl	%r11d, %r11d
	jle	.LBB3_45
# BB#46:                                # %.preheader308.lr.ph
	movq	wbp_weight(%rip), %r8
	movl	listXsize+4(%rip), %esi
	leaq	16984(%rsp), %r9
	xorl	%r10d, %r10d
	movq	24(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB3_47:                               # %.preheader308
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_49 Depth 2
	testl	%esi, %esi
	jle	.LBB3_51
# BB#48:                                # %.preheader307.lr.ph
                                        #   in Loop: Header=BB3_47 Depth=1
	movq	(%r8), %rax
	movq	8(%r8), %rcx
	movq	(%rcx,%r10,8), %rdi
	movq	(%rax,%r10,8), %rbx
	movq	%r9, %rcx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB3_49:                               # %.preheader307
                                        #   Parent Loop BB3_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rdi,%rax,8), %rsi
	movq	(%rbx,%rax,8), %rbp
	movl	-8(%rcx), %edx
	movl	%edx, (%rsi)
	movl	-12296(%rcx), %edx
	movl	%edx, (%rbp)
	movl	-4(%rcx), %edx
	movl	%edx, 4(%rsi)
	movl	-12292(%rcx), %edx
	movl	%edx, 4(%rbp)
	movl	(%rcx), %edx
	movl	%edx, 8(%rsi)
	movl	-12288(%rcx), %edx
	movl	%edx, 8(%rbp)
	incq	%rax
	movslq	listXsize+4(%rip), %rsi
	addq	$12, %rcx
	cmpq	%rsi, %rax
	jl	.LBB3_49
# BB#50:                                # %._crit_edge337.loopexit
                                        #   in Loop: Header=BB3_47 Depth=1
	movl	listXsize(%rip), %r11d
.LBB3_51:                               # %._crit_edge337
                                        #   in Loop: Header=BB3_47 Depth=1
	incq	%r10
	movslq	%r11d, %rax
	addq	$384, %r9               # imm = 0x180
	cmpq	%rax, %r10
	jl	.LBB3_47
	jmp	.LBB3_52
.LBB3_18:                               # %.preheader324
	movq	64(%rsp), %rcx          # 8-byte Reload
	movslq	68(%rcx), %rax
	testq	%rax, %rax
	xorpd	%xmm0, %xmm0
	jle	.LBB3_30
# BB#19:                                # %.preheader323.lr.ph
	cmpl	$0, 52(%rcx)
	xorpd	%xmm0, %xmm0
	jle	.LBB3_30
# BB#20:                                # %.preheader323.us.preheader
	movq	imgY_org(%rip), %rcx
	movq	img(%rip), %rdx
	movslq	52(%rdx), %rdx
	testb	$1, %al
	jne	.LBB3_22
# BB#21:
	xorpd	%xmm0, %xmm0
	xorl	%esi, %esi
	cmpl	$1, %eax
	jne	.LBB3_25
	jmp	.LBB3_30
.LBB3_45:
	movq	24(%rsp), %r13          # 8-byte Reload
.LBB3_52:                               # %.preheader305.lr.ph
	movq	wp_weight(%rip), %rax
	movq	wp_offset(%rip), %rcx
	xorl	%edx, %edx
	testl	%r11d, %r11d
	jg	.LBB3_54
	jmp	.LBB3_56
	.p2align	4, 0x90
.LBB3_57:                               # %._crit_edge333..preheader305_crit_edge
                                        #   in Loop: Header=BB3_56 Depth=1
	movl	listXsize+4(,%rdx,4), %r11d
	movq	%rsi, %rdx
	testl	%r11d, %r11d
	jle	.LBB3_56
.LBB3_54:                               # %.lr.ph
	movq	(%rax,%rdx,8), %rsi
	movq	(%rcx,%rdx,8), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_55:                               # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movl	%r12d, (%rbp)
	movl	%r12d, 4(%rbp)
	movl	%r12d, 8(%rbp)
	movq	(%rdi,%rbx,8), %rbp
	movl	$0, (%rbp)
	movl	$0, 4(%rbp)
	movl	$0, 8(%rbp)
	incq	%rbx
	movslq	listXsize(,%rdx,4), %rbp
	cmpq	%rbp, %rbx
	jl	.LBB3_55
.LBB3_56:                               # %._crit_edge333
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rsi
	cmpq	%r13, %rsi
	jl	.LBB3_57
	jmp	.LBB3_77
.LBB3_22:                               # %.preheader323.us.prol
	movq	(%rcx), %rdi
	xorpd	%xmm0, %xmm0
	xorl	%ebx, %ebx
	movl	$1, %esi
	.p2align	4, 0x90
.LBB3_23:                               # =>This Inner Loop Header: Depth=1
	movzwl	(%rdi,%rbx,2), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB3_23
# BB#24:                                # %.preheader323.us.prol.loopexit
	cmpl	$1, %eax
	je	.LBB3_30
	.p2align	4, 0x90
.LBB3_25:                               # %.preheader323.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_26 Depth 2
                                        #     Child Loop BB3_28 Depth 2
	movq	(%rcx,%rsi,8), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_26:                               #   Parent Loop BB3_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdi,%rbx,2), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB3_26
# BB#27:                                # %._crit_edge365.us
                                        #   in Loop: Header=BB3_25 Depth=1
	movq	8(%rcx,%rsi,8), %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_28:                               #   Parent Loop BB3_25 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzwl	(%rdi,%rbx,2), %ebp
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ebp, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB3_28
# BB#29:                                # %._crit_edge365.us.1
                                        #   in Loop: Header=BB3_25 Depth=1
	addq	$2, %rsi
	cmpq	%rax, %rsi
	jl	.LBB3_25
.LBB3_30:                               # %.preheader321.lr.ph
	cvtsi2sdl	%r12d, %xmm3
	mulsd	%xmm0, %xmm3
	xorl	%r14d, %r14d
	movl	%r11d, %eax
	movsd	%xmm3, 16(%rsp)         # 8-byte Spill
	testl	%eax, %eax
	jg	.LBB3_32
	.p2align	4, 0x90
.LBB3_42:                               # %._crit_edge359
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r14), %rcx
	movq	24(%rsp), %r13          # 8-byte Reload
	cmpq	%r13, %rcx
	jge	.LBB3_58
# BB#43:                                # %._crit_edge359..preheader321_crit_edge
                                        #   in Loop: Header=BB3_42 Depth=1
	movl	listXsize+4(,%r14,4), %eax
	movq	%rcx, %r14
	testl	%eax, %eax
	jle	.LBB3_42
.LBB3_32:                               # %.lr.ph358
	leaq	(%r14,%r14,2), %r15
	shlq	$7, %r15
	leaq	80(%rsp,%r15), %rdi
	movslq	%eax, %r13
	decl	%eax
	leaq	(%rax,%rax,2), %rax
	leaq	12(,%rax,4), %rdx
	xorl	%esi, %esi
	callq	memset
	movsd	.LCPI3_0(%rip), %xmm4   # xmm4 = mem[0],zero
	movsd	16(%rsp), %xmm3         # 8-byte Reload
                                        # xmm3 = mem[0],zero
	xorps	%xmm2, %xmm2
	movq	listX(,%r14,8), %r8
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB3_33:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_36 Depth 2
                                        #       Child Loop BB3_37 Depth 3
	movq	(%r8,%r9,8), %rax
	movq	6448(%rax), %rax
	movq	%rax, ref_pic_sub(%rip)
	movq	img(%rip), %rsi
	movslq	68(%rsi), %rdx
	testq	%rdx, %rdx
	movl	%r12d, %edi
	jle	.LBB3_41
# BB#34:                                # %.preheader320.lr.ph
                                        #   in Loop: Header=BB3_33 Depth=1
	movslq	52(%rsi), %rsi
	testq	%rsi, %rsi
	movl	%r12d, %edi
	jle	.LBB3_41
# BB#35:                                # %.preheader320.us.preheader
                                        #   in Loop: Header=BB3_33 Depth=1
	movq	(%rax), %rax
	movq	(%rax), %rdi
	addq	$20, %rdx
	addq	$19, %rsi
	xorpd	%xmm0, %xmm0
	movl	$20, %ebx
	.p2align	4, 0x90
.LBB3_36:                               # %.preheader320.us
                                        #   Parent Loop BB3_33 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB3_37 Depth 3
	movq	(%rdi,%rbx,8), %rax
	movl	$19, %ebp
	.p2align	4, 0x90
.LBB3_37:                               #   Parent Loop BB3_33 Depth=1
                                        #     Parent Loop BB3_36 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzwl	2(%rax,%rbp,2), %ecx
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	addsd	%xmm1, %xmm0
	incq	%rbp
	cmpq	%rsi, %rbp
	jl	.LBB3_37
# BB#38:                                # %._crit_edge353.us
                                        #   in Loop: Header=BB3_36 Depth=2
	incq	%rbx
	cmpq	%rdx, %rbx
	jl	.LBB3_36
# BB#39:                                # %._crit_edge356
                                        #   in Loop: Header=BB3_33 Depth=1
	ucomisd	%xmm2, %xmm0
	movl	%r12d, %edi
	jne	.LBB3_40
	jnp	.LBB3_41
.LBB3_40:                               #   in Loop: Header=BB3_33 Depth=1
	movapd	%xmm3, %xmm1
	divsd	%xmm0, %xmm1
	addsd	%xmm4, %xmm1
	cvttsd2si	%xmm1, %edi
.LBB3_41:                               # %._crit_edge356.thread
                                        #   in Loop: Header=BB3_33 Depth=1
	leal	64(%rdi), %eax
	cmpl	$191, %eax
	cmoval	%r12d, %edi
	leaq	(%r9,%r9,2), %rax
	leaq	2384(%rsp,%r15), %rcx
	movl	%edi, (%rcx,%rax,4)
	movl	%r12d, 4(%rcx,%rax,4)
	movl	%r12d, 8(%rcx,%rax,4)
	incq	%r9
	cmpq	%r13, %r9
	jl	.LBB3_33
	jmp	.LBB3_42
.LBB3_58:                               # %._crit_edge361
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	movq	wp_weight(%rip), %r8
	movq	wp_offset(%rip), %r9
	je	.LBB3_65
# BB#59:                                # %.preheader317.preheader
	xorl	%edx, %edx
	movq	32(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	jg	.LBB3_61
	jmp	.LBB3_63
	.p2align	4, 0x90
.LBB3_64:                               # %._crit_edge349..preheader317_crit_edge
                                        #   in Loop: Header=BB3_63 Depth=1
	movl	listXsize+4(,%rdx,4), %ecx
	movq	%rax, %rdx
	testl	%ecx, %ecx
	jle	.LBB3_63
.LBB3_61:                               # %.lr.ph348
	movq	(%r8,%rdx,8), %rax
	movq	(%r9,%rdx,8), %rcx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_62:                               # =>This Inner Loop Header: Depth=1
	movq	(%rax,%rsi,8), %rdi
	movl	%r12d, (%rdi)
	movl	%r12d, 4(%rdi)
	movl	%r12d, 8(%rdi)
	movq	(%rcx,%rsi,8), %rdi
	movl	$0, (%rdi)
	movl	$0, 4(%rdi)
	movl	$0, 8(%rdi)
	incq	%rsi
	movslq	listXsize(,%rdx,4), %rdi
	cmpq	%rdi, %rsi
	jl	.LBB3_62
.LBB3_63:                               # %._crit_edge349
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rax
	cmpq	%r13, %rax
	jl	.LBB3_64
	jmp	.LBB3_70
.LBB3_65:                               # %.preheader315.preheader
	leaq	2392(%rsp), %r10
	leaq	88(%rsp), %r11
	xorl	%r14d, %r14d
	movq	32(%rsp), %rcx          # 8-byte Reload
	testl	%ecx, %ecx
	jg	.LBB3_67
	jmp	.LBB3_69
	.p2align	4, 0x90
.LBB3_89:                               # %._crit_edge345..preheader315_crit_edge
                                        #   in Loop: Header=BB3_69 Depth=1
	movl	listXsize+4(,%r14,4), %ecx
	addq	$384, %r10              # imm = 0x180
	addq	$384, %r11              # imm = 0x180
	movq	%rax, %r14
	testl	%ecx, %ecx
	jle	.LBB3_69
.LBB3_67:                               # %.preheader314.lr.ph
	movq	(%r8,%r14,8), %r15
	movq	(%r9,%r14,8), %rax
	movq	%r11, %rsi
	movq	%r10, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB3_68:                               # %.preheader314
                                        # =>This Inner Loop Header: Depth=1
	movq	(%r15,%rcx,8), %rbp
	movq	(%rax,%rcx,8), %rdi
	movl	-8(%rdx), %ebx
	movl	%ebx, (%rbp)
	movl	-8(%rsi), %ebx
	movl	%ebx, (%rdi)
	movl	-4(%rdx), %ebx
	movl	%ebx, 4(%rbp)
	movl	-4(%rsi), %ebx
	movl	%ebx, 4(%rdi)
	movl	(%rdx), %ebx
	movl	%ebx, 8(%rbp)
	movl	(%rsi), %ebx
	movl	%ebx, 8(%rdi)
	incq	%rcx
	movslq	listXsize(,%r14,4), %rdi
	addq	$12, %rdx
	addq	$12, %rsi
	cmpq	%rdi, %rcx
	jl	.LBB3_68
.LBB3_69:                               # %._crit_edge345
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%r14), %rax
	cmpq	%r13, %rax
	jl	.LBB3_89
.LBB3_70:                               # %.preheader312
	movl	listXsize(%rip), %edi
	testl	%edi, %edi
	jle	.LBB3_77
# BB#71:                                # %.preheader311.lr.ph
	movq	wp_weight(%rip), %r8
	movq	wbp_weight(%rip), %r9
	movl	listXsize+4(%rip), %esi
	xorl	%r10d, %r10d
	.p2align	4, 0x90
.LBB3_72:                               # %.preheader311
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_74 Depth 2
	testl	%esi, %esi
	jle	.LBB3_76
# BB#73:                                # %.preheader310.lr.ph
                                        #   in Loop: Header=BB3_72 Depth=1
	movq	(%r8), %rax
	movq	8(%r8), %r11
	movq	(%rax,%r10,8), %rbx
	movq	(%r9), %rax
	movq	8(%r9), %rcx
	movq	(%rax,%r10,8), %r14
	movq	(%rcx,%r10,8), %rcx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_74:                               # %.preheader310
                                        #   Parent Loop BB3_72 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r14,%rdx,8), %rsi
	movq	(%r11,%rdx,8), %rbp
	movq	(%rcx,%rdx,8), %rdi
	movl	(%rbx), %eax
	movl	%eax, (%rsi)
	movl	(%rbp), %eax
	movl	%eax, (%rdi)
	movl	4(%rbx), %eax
	movl	%eax, 4(%rsi)
	movl	4(%rbp), %eax
	movl	%eax, 4(%rdi)
	movl	8(%rbx), %eax
	movl	%eax, 8(%rsi)
	movl	8(%rbp), %eax
	movl	%eax, 8(%rdi)
	incq	%rdx
	movslq	listXsize+4(%rip), %rsi
	cmpq	%rsi, %rdx
	jl	.LBB3_74
# BB#75:                                # %._crit_edge341.loopexit
                                        #   in Loop: Header=BB3_72 Depth=1
	movl	listXsize(%rip), %edi
.LBB3_76:                               # %._crit_edge341
                                        #   in Loop: Header=BB3_72 Depth=1
	incq	%r10
	movslq	%edi, %rax
	cmpq	%rax, %r10
	jl	.LBB3_72
.LBB3_77:                               # %.loopexit
	xorl	%eax, %eax
	cmpl	$0, 12(%rsp)            # 4-byte Folded Reload
	je	.LBB3_78
.LBB3_87:
	addq	$78424, %rsp            # imm = 0x13258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_78:                               # %.preheader303.lr.ph
	movq	input(%rip), %rcx
	movl	40(%rcx), %edx
	movl	listXsize(%rip), %esi
	cmpl	%esi, %edx
	movl	%esi, %eax
	cmovlel	%edx, %eax
	testl	%edx, %edx
	cmovel	%esi, %eax
	movl	%eax, 56(%rsp)
	movl	listXsize+4(%rip), %esi
	cmpl	%esi, %edx
	cmovgl	%esi, %edx
	cmpl	$0, 44(%rcx)
	cmovel	%esi, %edx
	movl	%edx, 60(%rsp)
	movq	wp_weight(%rip), %rcx
	xorl	%edx, %edx
	testl	%eax, %eax
	jg	.LBB3_80
.LBB3_85:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	leaq	1(%rdx), %rsi
	cmpq	%r13, %rsi
	jge	.LBB3_86
# BB#88:                                # %._crit_edge..preheader303_crit_edge
                                        #   in Loop: Header=BB3_85 Depth=1
	movl	60(%rsp,%rdx,4), %eax
	movq	%rsi, %rdx
	testl	%eax, %eax
	jle	.LBB3_85
.LBB3_80:                               # %.preheader.lr.ph
	movq	(%rcx,%rdx,8), %rsi
	movslq	%eax, %rdi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_81:                               # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsi,%rbx,8), %rbp
	movl	$1, %eax
	cmpl	%r12d, (%rbp)
	jne	.LBB3_87
# BB#82:                                #   in Loop: Header=BB3_81 Depth=1
	cmpl	%r12d, 4(%rbp)
	jne	.LBB3_87
# BB#83:                                #   in Loop: Header=BB3_81 Depth=1
	cmpl	%r12d, 8(%rbp)
	jne	.LBB3_87
# BB#84:                                #   in Loop: Header=BB3_81 Depth=1
	incq	%rbx
	cmpq	%rdi, %rbx
	jl	.LBB3_81
	jmp	.LBB3_85
.LBB3_86:
	xorl	%eax, %eax
	jmp	.LBB3_87
.Lfunc_end3:
	.size	test_wp_B_slice, .Lfunc_end3-test_wp_B_slice
	.cfi_endproc

	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	ref_pic_sub,@object     # @ref_pic_sub
	.local	ref_pic_sub
	.comm	ref_pic_sub,24,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	ref_qpic_sub,@object    # @ref_qpic_sub
	.local	ref_qpic_sub
	.comm	ref_qpic_sub,24,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
