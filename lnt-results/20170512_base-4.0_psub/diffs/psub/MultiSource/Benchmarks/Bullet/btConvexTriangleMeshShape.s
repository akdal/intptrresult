	.text
	.file	"btConvexTriangleMeshShape.bc"
	.globl	_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb
	.p2align	4, 0x90
	.type	_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb,@function
_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb: # @_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edx, %r14d
	movq	%rsi, %rbp
	movq	%rdi, %rbx
	callq	_ZN34btPolyhedralConvexAabbCachingShapeC2Ev
	movq	$_ZTV25btConvexTriangleMeshShape+16, (%rbx)
	movq	%rbp, 104(%rbx)
	movl	$3, 8(%rbx)
	testb	%r14b, %r14b
	je	.LBB0_2
# BB#1:
.Ltmp0:
	movq	%rbx, %rdi
	callq	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv
.Ltmp1:
.LBB0_2:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB0_3:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN13btConvexShapeD2Ev
.Ltmp4:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_5:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end0:
	.size	_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb, .Lfunc_end0-_ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end0-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end1:
	.size	__clang_call_terminate, .Lfunc_end1-__clang_call_terminate

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI2_0:
	.long	953267991               # float 9.99999974E-5
.LCPI2_1:
	.long	1065353216              # float 1
.LCPI2_3:
	.long	3713928043              # float -9.99999984E+17
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_2:
	.long	3713928043              # float -9.99999984E+17
	.long	3713928043              # float -9.99999984E+17
	.zero	4
	.zero	4
	.text
	.globl	_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3,@function
_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3: # @_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
	subq	$128, %rsp
.Lcfi7:
	.cfi_def_cfa_offset 144
.Lcfi8:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movss	(%rsi), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI2_0(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB2_2
# BB#1:
	xorps	%xmm2, %xmm2
	xorl	%ecx, %ecx
	movl	$1065353216, %eax       # imm = 0x3F800000
	xorl	%edx, %edx
	jmp	.LBB2_5
.LBB2_2:
	movss	12(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB2_4
# BB#3:                                 # %call.sqrt
	movss	%xmm2, 32(%rsp)         # 4-byte Spill
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm4, 12(%rsp)         # 4-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB2_4:                                # %.split
	movss	.LCPI2_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm5
	movd	%xmm5, %eax
	mulss	%xmm0, %xmm4
	movd	%xmm4, %ecx
	mulss	%xmm0, %xmm3
	movd	%xmm3, %edx
.LBB2_5:
	movq	$_ZTV26LocalSupportVertexCallback+16, 48(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 56(%rsp)
	movl	$-581039253, 72(%rsp)   # imm = 0xDD5E0B6B
	movl	%eax, 76(%rsp)
	movl	%ecx, 80(%rsp)
	movl	%edx, 84(%rsp)
	movss	%xmm2, 88(%rsp)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, 112(%rsp)
	movq	$1566444395, 120(%rsp)  # imm = 0x5D5E0B6B
	movq	104(%rbx), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	.LCPI2_2(%rip), %rcx
	movq	%rcx, 96(%rsp)
	movss	.LCPI2_3(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movlps	%xmm0, 104(%rsp)
.Ltmp6:
	leaq	48(%rsp), %rsi
	leaq	96(%rsp), %rdx
	leaq	112(%rsp), %rcx
	callq	*%rax
.Ltmp7:
# BB#6:
	movsd	56(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	movsd	64(%rsp), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	leaq	48(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	addq	$128, %rsp
	popq	%rbx
	retq
.LBB2_7:
.Ltmp8:
	movq	%rax, %rbx
.Ltmp9:
	leaq	48(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp10:
# BB#8:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB2_9:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end2:
	.size	_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3, .Lfunc_end2-_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp7-.Lfunc_begin1    # >> Call Site 2 <<
	.long	.Ltmp9-.Ltmp7           #   Call between .Ltmp7 and .Ltmp9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end2-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	3713928043              # float -9.99999984E+17
	.long	3713928043              # float -9.99999984E+17
	.zero	4
	.zero	4
.LCPI3_2:
	.zero	16
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_1:
	.long	3713928043              # float -9.99999984E+17
	.text
	.globl	_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i,@function
_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i: # @_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi9:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi10:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi12:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi13:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi15:
	.cfi_def_cfa_offset 144
.Lcfi16:
	.cfi_offset %rbx, -56
.Lcfi17:
	.cfi_offset %r12, -48
.Lcfi18:
	.cfi_offset %r13, -40
.Lcfi19:
	.cfi_offset %r14, -32
.Lcfi20:
	.cfi_offset %r15, -24
.Lcfi21:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, %r14
	movq	%rdi, (%rsp)            # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB3_12
# BB#1:                                 # %.lr.ph41.preheader
	movl	%ecx, %eax
	leaq	-1(%rax), %rsi
	movq	%rax, %rdi
	andq	$7, %rdi
	je	.LBB3_2
# BB#3:                                 # %.lr.ph41.prol.preheader
	leaq	12(%rbx), %rbp
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_4:                                # %.lr.ph41.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, (%rbp)     # imm = 0xDD5E0B6B
	incq	%rdx
	addq	$16, %rbp
	cmpq	%rdx, %rdi
	jne	.LBB3_4
	jmp	.LBB3_5
.LBB3_2:
	xorl	%edx, %edx
.LBB3_5:                                # %.lr.ph41.prol.loopexit
	cmpq	$7, %rsi
	jb	.LBB3_8
# BB#6:                                 # %.lr.ph41.preheader.new
	subq	%rdx, %rax
	shlq	$4, %rdx
	leaq	124(%rbx,%rdx), %rdx
	.p2align	4, 0x90
.LBB3_7:                                # %.lr.ph41
                                        # =>This Inner Loop Header: Depth=1
	movl	$-581039253, -112(%rdx) # imm = 0xDD5E0B6B
	movl	$-581039253, -96(%rdx)  # imm = 0xDD5E0B6B
	movl	$-581039253, -80(%rdx)  # imm = 0xDD5E0B6B
	movl	$-581039253, -64(%rdx)  # imm = 0xDD5E0B6B
	movl	$-581039253, -48(%rdx)  # imm = 0xDD5E0B6B
	movl	$-581039253, -32(%rdx)  # imm = 0xDD5E0B6B
	movl	$-581039253, -16(%rdx)  # imm = 0xDD5E0B6B
	movl	$-581039253, (%rdx)     # imm = 0xDD5E0B6B
	subq	$-128, %rdx
	addq	$-8, %rax
	jne	.LBB3_7
.LBB3_8:                                # %.preheader
	testl	%ecx, %ecx
	jle	.LBB3_12
# BB#9:                                 # %.lr.ph
	leaq	48(%rsp), %r15
	movslq	%ecx, %r13
	xorl	%ebp, %ebp
	leaq	40(%rsp), %r12
	.p2align	4, 0x90
.LBB3_10:                               # =>This Inner Loop Header: Depth=1
	movq	$_ZTV26LocalSupportVertexCallback+16, 40(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r15)
	movl	$-581039253, 64(%rsp)   # imm = 0xDD5E0B6B
	movups	(%r14), %xmm0
	movups	%xmm0, 20(%r15)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, 24(%rsp)
	movq	$1566444395, 32(%rsp)   # imm = 0x5D5E0B6B
	movq	(%rsp), %rax            # 8-byte Reload
	movq	104(%rax), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	.LCPI3_0(%rip), %rcx
	movq	%rcx, 8(%rsp)
	movss	.LCPI3_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movlps	%xmm0, 16(%rsp)
.Ltmp12:
	movq	%r12, %rsi
	leaq	8(%rsp), %rdx
	leaq	24(%rsp), %rcx
	callq	*%rax
.Ltmp13:
# BB#11:                                #   in Loop: Header=BB3_10 Depth=1
	movups	48(%rsp), %xmm0
	movups	%xmm0, (%rbx)
	movq	%r12, %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
	incq	%rbp
	addq	$16, %r14
	addq	$16, %rbx
	cmpq	%r13, %rbp
	jl	.LBB3_10
.LBB3_12:                               # %._crit_edge
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_13:
.Ltmp14:
	movq	%rax, %rbx
.Ltmp15:
	leaq	40(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp16:
# BB#14:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB3_15:
.Ltmp17:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i, .Lfunc_end3-_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp12-.Lfunc_begin2   # >> Call Site 1 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin2   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp13-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp15-.Ltmp13         #   Call between .Ltmp13 and .Ltmp15
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp15-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp16-.Ltmp15         #   Call between .Ltmp15 and .Ltmp16
	.long	.Ltmp17-.Lfunc_begin2   #     jumps to .Ltmp17
	.byte	1                       #   On action: 1
	.long	.Ltmp16-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp16     #   Call between .Ltmp16 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	679477248               # float 1.42108547E-14
.LCPI4_2:
	.long	3212836864              # float -1
.LCPI4_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI4_1:
	.long	3212836864              # float -1
	.long	3212836864              # float -1
	.zero	4
	.zero	4
	.text
	.globl	_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3,@function
_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3: # @_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 24
	subq	$72, %rsp
.Lcfi24:
	.cfi_def_cfa_offset 96
.Lcfi25:
	.cfi_offset %rbx, -24
.Lcfi26:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	(%r14), %rax
	callq	*104(%rax)
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jne	.LBB4_2
	jnp	.LBB4_1
.LBB4_2:
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movss	8(%rbx), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI4_0(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	ucomiss	%xmm1, %xmm2
	seta	%al
	movd	%eax, %xmm1
	pshufd	$80, %xmm1, %xmm2       # xmm2 = xmm1[0,0,1,1]
	pslld	$31, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm2, %xmm1
	pandn	%xmm0, %xmm1
	pand	.LCPI4_1(%rip), %xmm2
	por	%xmm1, %xmm2
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	movdqa	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	jbe	.LBB4_4
# BB#3:
	movss	.LCPI4_2(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
.LBB4_4:
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB4_6
# BB#5:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movdqa	%xmm2, (%rsp)           # 16-byte Spill
	callq	sqrtf
	movdqa	(%rsp), %xmm2           # 16-byte Reload
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
.LBB4_6:                                # %.split
	movss	.LCPI4_3(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm2, %xmm0
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movq	(%r14), %rax
	movq	%r14, %rdi
	callq	*88(%rax)
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm0, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	16(%rsp), %xmm0         # 16-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	addps	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	addss	%xmm2, %xmm1
	jmp	.LBB4_7
.LBB4_1:
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
.LBB4_7:
	addq	$72, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3, .Lfunc_end4-_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape14getNumVerticesEv
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape14getNumVerticesEv,@function
_ZNK25btConvexTriangleMeshShape14getNumVerticesEv: # @_ZNK25btConvexTriangleMeshShape14getNumVerticesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end5:
	.size	_ZNK25btConvexTriangleMeshShape14getNumVerticesEv, .Lfunc_end5-_ZNK25btConvexTriangleMeshShape14getNumVerticesEv
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape11getNumEdgesEv
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape11getNumEdgesEv,@function
_ZNK25btConvexTriangleMeshShape11getNumEdgesEv: # @_ZNK25btConvexTriangleMeshShape11getNumEdgesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end6:
	.size	_ZNK25btConvexTriangleMeshShape11getNumEdgesEv, .Lfunc_end6-_ZNK25btConvexTriangleMeshShape11getNumEdgesEv
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_,@function
_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_: # @_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end7:
	.size	_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_, .Lfunc_end7-_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3,@function
_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3: # @_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end8:
	.size	_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3, .Lfunc_end8-_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape12getNumPlanesEv
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape12getNumPlanesEv,@function
_ZNK25btConvexTriangleMeshShape12getNumPlanesEv: # @_ZNK25btConvexTriangleMeshShape12getNumPlanesEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end9:
	.size	_ZNK25btConvexTriangleMeshShape12getNumPlanesEv, .Lfunc_end9-_ZNK25btConvexTriangleMeshShape12getNumPlanesEv
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i,@function
_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i: # @_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end10:
	.size	_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i, .Lfunc_end10-_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f,@function
_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f: # @_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end11:
	.size	_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f, .Lfunc_end11-_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f
	.cfi_endproc

	.globl	_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3
	.p2align	4, 0x90
	.type	_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3,@function
_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3: # @_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rax
	movups	(%rsi), %xmm0
	movups	%xmm0, 8(%rax)
	jmp	_ZN34btPolyhedralConvexAabbCachingShape15recalcLocalAabbEv # TAILCALL
.Lfunc_end12:
	.size	_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3, .Lfunc_end12-_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3
	.cfi_endproc

	.globl	_ZNK25btConvexTriangleMeshShape15getLocalScalingEv
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape15getLocalScalingEv,@function
_ZNK25btConvexTriangleMeshShape15getLocalScalingEv: # @_ZNK25btConvexTriangleMeshShape15getLocalScalingEv
	.cfi_startproc
# BB#0:
	movq	104(%rdi), %rax
	addq	$8, %rax
	retq
.Lfunc_end13:
	.size	_ZNK25btConvexTriangleMeshShape15getLocalScalingEv, .Lfunc_end13-_ZNK25btConvexTriangleMeshShape15getLocalScalingEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_0:
	.long	3713928043              # float -9.99999984E+17
	.long	3713928043              # float -9.99999984E+17
	.zero	4
	.zero	4
.LCPI14_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_1:
	.long	3713928043              # float -9.99999984E+17
.LCPI14_2:
	.long	1065353216              # float 1
.LCPI14_3:
	.long	1042983595              # float 0.166666672
.LCPI14_5:
	.long	925353388               # float 9.99999974E-6
	.text
	.globl	_ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3Rf
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3Rf,@function
_ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3Rf: # @_ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3Rf
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%r15
.Lcfi27:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi28:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi29:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 40
	subq	$152, %rsp
.Lcfi31:
	.cfi_def_cfa_offset 192
.Lcfi32:
	.cfi_offset %rbx, -40
.Lcfi33:
	.cfi_offset %r12, -32
.Lcfi34:
	.cfi_offset %r14, -24
.Lcfi35:
	.cfi_offset %r15, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movq	%rsi, %r15
	movq	%rdi, %r12
	movq	$_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback+16, 16(%rsp)
	movb	$1, 24(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 44(%rsp)
	movups	%xmm0, 28(%rsp)
	movl	$0, 60(%rsp)
	movabsq	$6727827449093950315, %rax # imm = 0x5D5E0B6B5D5E0B6B
	movq	%rax, (%rsp)
	movq	$1566444395, 8(%rsp)    # imm = 0x5D5E0B6B
	movq	104(%r12), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movq	.LCPI14_0(%rip), %rcx
	movq	%rcx, 64(%rsp)
	movss	.LCPI14_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movlps	%xmm0, 72(%rsp)
.Ltmp18:
	leaq	16(%rsp), %rsi
	leaq	64(%rsp), %rdx
	movq	%rsp, %rcx
	callq	*%rax
.Ltmp19:
# BB#1:
	movss	60(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jbe	.LBB14_3
# BB#2:
	movss	.LCPI14_2(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm3
	movsd	44(%rsp), %xmm2         # xmm2 = mem[0],zero
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm2, %xmm1
	mulss	52(%rsp), %xmm3
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	jmp	.LBB14_4
.LBB14_3:
	movsd	28(%rsp), %xmm1         # xmm1 = mem[0],zero
	movsd	36(%rsp), %xmm2         # xmm2 = mem[0],zero
.LBB14_4:
	movlps	%xmm1, 48(%r15)
	movlps	%xmm2, 56(%r15)
	mulss	.LCPI14_3(%rip), %xmm0
	movss	%xmm0, (%r14)
	movq	$_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback+16, 64(%rsp)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 104(%rsp)
	movups	%xmm0, 88(%rsp)
	movups	%xmm0, 72(%rsp)
	unpcklpd	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0]
	movupd	%xmm1, 120(%rsp)
	movq	104(%r12), %rdi
	movq	(%rdi), %rax
	movq	16(%rax), %rax
	movsd	(%rsp), %xmm1           # xmm1 = mem[0],zero
	movaps	.LCPI14_4(%rip), %xmm2  # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm2, %xmm1
	movss	8(%rsp), %xmm3          # xmm3 = mem[0],zero,zero,zero
	xorps	%xmm2, %xmm3
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm1, 136(%rsp)
	movlps	%xmm0, 144(%rsp)
.Ltmp21:
	leaq	64(%rsp), %rsi
	leaq	136(%rsp), %rdx
	movq	%rsp, %rcx
	callq	*%rax
.Ltmp22:
# BB#5:
	leaq	72(%rsp), %rdi
.Ltmp24:
	movss	.LCPI14_5(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movl	$20, %edx
	movq	%r15, %rsi
	callq	_ZN11btMatrix3x311diagonalizeERS_fi
.Ltmp25:
# BB#6:
	movl	72(%rsp), %eax
	movl	%eax, (%rbx)
	movl	92(%rsp), %ecx
	movl	%ecx, 4(%rbx)
	movl	112(%rsp), %edx
	movl	%edx, 8(%rbx)
	movl	$0, 12(%rbx)
	movss	.LCPI14_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	(%r14), %xmm0
	movd	%eax, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movd	%ecx, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbx)
	movd	%edx, %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rbx)
.Ltmp29:
	leaq	64(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp30:
# BB#7:
	leaq	16(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
	addq	$152, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB14_8:
.Ltmp31:
	jmp	.LBB14_13
.LBB14_9:
.Ltmp26:
	jmp	.LBB14_11
.LBB14_10:
.Ltmp23:
.LBB14_11:
	movq	%rax, %rbx
.Ltmp27:
	leaq	64(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp28:
	jmp	.LBB14_14
.LBB14_12:
.Ltmp20:
.LBB14_13:
	movq	%rax, %rbx
.LBB14_14:
.Ltmp32:
	leaq	16(%rsp), %rdi
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp33:
# BB#15:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.LBB14_16:
.Ltmp34:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3Rf, .Lfunc_end14-_ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3Rf
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\343\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Ltmp18-.Lfunc_begin3   # >> Call Site 1 <<
	.long	.Ltmp19-.Ltmp18         #   Call between .Ltmp18 and .Ltmp19
	.long	.Ltmp20-.Lfunc_begin3   #     jumps to .Ltmp20
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp22-.Ltmp21         #   Call between .Ltmp21 and .Ltmp22
	.long	.Ltmp23-.Lfunc_begin3   #     jumps to .Ltmp23
	.byte	0                       #   On action: cleanup
	.long	.Ltmp24-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp25-.Ltmp24         #   Call between .Ltmp24 and .Ltmp25
	.long	.Ltmp26-.Lfunc_begin3   #     jumps to .Ltmp26
	.byte	0                       #   On action: cleanup
	.long	.Ltmp29-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp30-.Ltmp29         #   Call between .Ltmp29 and .Ltmp30
	.long	.Ltmp31-.Lfunc_begin3   #     jumps to .Ltmp31
	.byte	0                       #   On action: cleanup
	.long	.Ltmp30-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp27-.Ltmp30         #   Call between .Ltmp30 and .Ltmp27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp33-.Ltmp27         #   Call between .Ltmp27 and .Ltmp33
	.long	.Ltmp34-.Lfunc_begin3   #     jumps to .Ltmp34
	.byte	1                       #   On action: 1
	.long	.Ltmp33-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Lfunc_end14-.Ltmp33    #   Call between .Ltmp33 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI15_6:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI15_1:
	.long	872415232               # float 1.1920929E-7
.LCPI15_2:
	.long	1285554176              # float 83886080
.LCPI15_3:
	.long	1056964608              # float 0.5
.LCPI15_4:
	.long	1073741824              # float 2
.LCPI15_5:
	.long	1065353216              # float 1
	.section	.text._ZN11btMatrix3x311diagonalizeERS_fi,"axG",@progbits,_ZN11btMatrix3x311diagonalizeERS_fi,comdat
	.weak	_ZN11btMatrix3x311diagonalizeERS_fi
	.p2align	4, 0x90
	.type	_ZN11btMatrix3x311diagonalizeERS_fi,@function
_ZN11btMatrix3x311diagonalizeERS_fi:    # @_ZN11btMatrix3x311diagonalizeERS_fi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi39:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi40:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi41:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi42:
	.cfi_def_cfa_offset 112
.Lcfi43:
	.cfi_offset %rbx, -56
.Lcfi44:
	.cfi_offset %r12, -48
.Lcfi45:
	.cfi_offset %r13, -40
.Lcfi46:
	.cfi_offset %r14, -32
.Lcfi47:
	.cfi_offset %r15, -24
.Lcfi48:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
	movaps	%xmm0, %xmm8
	movq	%rsi, %r12
	movl	$1065353216, (%r12)     # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 4(%r12)
	movl	$1065353216, 20(%r12)   # imm = 0x3F800000
	movups	%xmm0, 24(%r12)
	movq	$1065353216, 40(%r12)   # imm = 0x3F800000
	testl	%edx, %edx
	jle	.LBB15_15
# BB#1:                                 # %.lr.ph
	movdqa	.LCPI15_0(%rip), %xmm7  # xmm7 = [nan,nan,nan,nan]
	movss	.LCPI15_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movss	.LCPI15_5(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movss	%xmm8, 12(%rsp)         # 4-byte Spill
	movq	%rdi, 48(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	4(%rdi), %xmm1          # xmm1 = mem[0],zero
	pand	%xmm7, %xmm1
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
	ucomiss	%xmm1, %xmm0
	seta	%al
	setbe	%cl
	maxss	%xmm1, %xmm0
	movd	24(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	pand	%xmm7, %xmm1
	ucomiss	%xmm0, %xmm1
	jbe	.LBB15_3
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	xorl	%ebp, %ebp
	movdqa	%xmm1, %xmm0
	movl	$2, %ebx
	movl	$1, %r14d
	jmp	.LBB15_5
	.p2align	4, 0x90
.LBB15_3:                               #   in Loop: Header=BB15_2 Depth=1
	movb	%cl, %bpl
	incq	%rbp
	movb	%al, %bl
	incq	%rbx
	xorl	%r14d, %r14d
.LBB15_5:                               #   in Loop: Header=BB15_2 Depth=1
	movd	(%rdi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movd	20(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	pand	%xmm7, %xmm1
	pand	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	movd	40(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	pand	%xmm7, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm8, %xmm1
	ucomiss	%xmm0, %xmm1
	jb	.LBB15_7
# BB#6:                                 #   in Loop: Header=BB15_2 Depth=1
	mulss	.LCPI15_1(%rip), %xmm1
	movl	$1, %edx
	ucomiss	%xmm0, %xmm1
	jae	.LBB15_15
.LBB15_7:                               #   in Loop: Header=BB15_2 Depth=1
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rdi, %r13
	leaq	(%r13,%rbx,4), %rcx
	movss	(%r13,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movq	%rbx, %r15
	shlq	$4, %r15
	addq	%rdi, %r15
	leaq	(%r15,%rbx,4), %rax
	movss	(%r15,%rbx,4), %xmm4    # xmm4 = mem[0],zero,zero,zero
	leaq	(%r13,%r14,4), %rsi
	subss	(%r13,%r14,4), %xmm4
	movaps	%xmm3, %xmm0
	addss	%xmm0, %xmm0
	divss	%xmm0, %xmm4
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm6
	jbe	.LBB15_13
# BB#8:                                 #   in Loop: Header=BB15_2 Depth=1
	addss	%xmm9, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB15_10
# BB#9:                                 # %call.sqrt
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movss	%xmm4, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm4          # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI15_5(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movss	.LCPI15_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movdqa	.LCPI15_0(%rip), %xmm7  # xmm7 = [nan,nan,nan,nan]
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movss	12(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB15_10:                              # %.split
                                        #   in Loop: Header=BB15_2 Depth=1
	xorps	%xmm0, %xmm0
	cmpless	%xmm4, %xmm0
	movaps	%xmm0, %xmm2
	andps	%xmm1, %xmm2
	xorps	.LCPI15_6(%rip), %xmm1
	andnps	%xmm1, %xmm0
	orps	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm9, %xmm5
	divss	%xmm0, %xmm5
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm9, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB15_12
# BB#11:                                # %call.sqrt138
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movss	%xmm3, 8(%rsp)          # 4-byte Spill
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movq	%rcx, 24(%rsp)          # 8-byte Spill
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movss	%xmm5, 4(%rsp)          # 4-byte Spill
	callq	sqrtf
	movss	4(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movq	16(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rcx          # 8-byte Reload
	movq	32(%rsp), %rax          # 8-byte Reload
	movss	8(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI15_5(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movss	.LCPI15_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	movdqa	.LCPI15_0(%rip), %xmm7  # xmm7 = [nan,nan,nan,nan]
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	48(%rsp), %rdi          # 8-byte Reload
	movss	12(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB15_12:                              # %.split.split
                                        #   in Loop: Header=BB15_2 Depth=1
	movaps	%xmm9, %xmm0
	divss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm5, %xmm1
	jmp	.LBB15_14
	.p2align	4, 0x90
.LBB15_13:                              #   in Loop: Header=BB15_2 Depth=1
	movss	.LCPI15_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	divss	%xmm0, %xmm1
	addss	.LCPI15_4(%rip), %xmm1
	mulss	%xmm1, %xmm4
	movaps	%xmm9, %xmm5
	divss	%xmm4, %xmm5
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	mulss	%xmm5, %xmm1
	movaps	%xmm9, %xmm0
	subss	%xmm1, %xmm0
	movaps	%xmm5, %xmm1
	mulss	%xmm0, %xmm1
.LBB15_14:                              #   in Loop: Header=BB15_2 Depth=1
	movl	$0, (%r15,%r14,4)
	movl	$0, (%rcx)
	mulss	%xmm5, %xmm3
	movss	(%rsi), %xmm2           # xmm2 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm2
	movss	%xmm2, (%rsi)
	addss	(%rax), %xmm3
	movss	%xmm3, (%rax)
	movq	%rbp, %rax
	shlq	$4, %rax
	addq	%rdi, %rax
	movss	(%rax,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	(%rax,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, (%r13,%rbp,4)
	movss	%xmm4, (%rax,%r14,4)
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, (%r15,%rbp,4)
	movss	%xmm2, (%rax,%rbx,4)
	movss	(%r12,%r14,4), %xmm2    # xmm2 = mem[0],zero,zero,zero
	movss	(%r12,%rbx,4), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, (%r12,%r14,4)
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, (%r12,%rbx,4)
	movss	16(%r12,%r14,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movss	16(%r12,%rbx,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, 16(%r12,%r14,4)
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	%xmm2, 16(%r12,%rbx,4)
	movss	32(%r12,%r14,4), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movss	32(%r12,%rbx,4), %xmm3  # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm3, %xmm5
	subss	%xmm5, %xmm4
	movss	%xmm4, 32(%r12,%r14,4)
	mulss	%xmm3, %xmm0
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movss	%xmm1, 32(%r12,%rbx,4)
	cmpl	$1, %edx
	leal	-1(%rdx), %eax
	movl	%eax, %edx
	jg	.LBB15_2
.LBB15_15:                              # %._crit_edge
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	_ZN11btMatrix3x311diagonalizeERS_fi, .Lfunc_end15-_ZN11btMatrix3x311diagonalizeERS_fi
	.cfi_endproc

	.section	.text._ZN25btConvexTriangleMeshShapeD0Ev,"axG",@progbits,_ZN25btConvexTriangleMeshShapeD0Ev,comdat
	.weak	_ZN25btConvexTriangleMeshShapeD0Ev
	.p2align	4, 0x90
	.type	_ZN25btConvexTriangleMeshShapeD0Ev,@function
_ZN25btConvexTriangleMeshShapeD0Ev:     # @_ZN25btConvexTriangleMeshShapeD0Ev
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -24
.Lcfi53:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp35:
	callq	_ZN13btConvexShapeD2Ev
.Ltmp36:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_Z21btAlignedFreeInternalPv # TAILCALL
.LBB16_2:
.Ltmp37:
	movq	%rax, %r14
.Ltmp38:
	movq	%rbx, %rdi
	callq	_Z21btAlignedFreeInternalPv
.Ltmp39:
# BB#3:                                 # %_ZN13btConvexShapedlEPv.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_4:
.Ltmp40:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN25btConvexTriangleMeshShapeD0Ev, .Lfunc_end16-_ZN25btConvexTriangleMeshShapeD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp35-.Lfunc_begin4   # >> Call Site 1 <<
	.long	.Ltmp36-.Ltmp35         #   Call between .Ltmp35 and .Ltmp36
	.long	.Ltmp37-.Lfunc_begin4   #     jumps to .Ltmp37
	.byte	0                       #   On action: cleanup
	.long	.Ltmp36-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp38-.Ltmp36         #   Call between .Ltmp36 and .Ltmp38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp38-.Lfunc_begin4   # >> Call Site 3 <<
	.long	.Ltmp39-.Ltmp38         #   Call between .Ltmp38 and .Ltmp39
	.long	.Ltmp40-.Lfunc_begin4   #     jumps to .Ltmp40
	.byte	1                       #   On action: 1
	.long	.Ltmp39-.Lfunc_begin4   # >> Call Site 4 <<
	.long	.Lfunc_end16-.Ltmp39    #   Call between .Ltmp39 and .Lfunc_end16
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK25btConvexTriangleMeshShape7getNameEv,"axG",@progbits,_ZNK25btConvexTriangleMeshShape7getNameEv,comdat
	.weak	_ZNK25btConvexTriangleMeshShape7getNameEv
	.p2align	4, 0x90
	.type	_ZNK25btConvexTriangleMeshShape7getNameEv,@function
_ZNK25btConvexTriangleMeshShape7getNameEv: # @_ZNK25btConvexTriangleMeshShape7getNameEv
	.cfi_startproc
# BB#0:
	movl	$.L.str, %eax
	retq
.Lfunc_end17:
	.size	_ZNK25btConvexTriangleMeshShape7getNameEv, .Lfunc_end17-_ZNK25btConvexTriangleMeshShape7getNameEv
	.cfi_endproc

	.section	.text._ZN21btConvexInternalShape9setMarginEf,"axG",@progbits,_ZN21btConvexInternalShape9setMarginEf,comdat
	.weak	_ZN21btConvexInternalShape9setMarginEf
	.p2align	4, 0x90
	.type	_ZN21btConvexInternalShape9setMarginEf,@function
_ZN21btConvexInternalShape9setMarginEf: # @_ZN21btConvexInternalShape9setMarginEf
	.cfi_startproc
# BB#0:
	movss	%xmm0, 56(%rdi)
	retq
.Lfunc_end18:
	.size	_ZN21btConvexInternalShape9setMarginEf, .Lfunc_end18-_ZN21btConvexInternalShape9setMarginEf
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape9getMarginEv,"axG",@progbits,_ZNK21btConvexInternalShape9getMarginEv,comdat
	.weak	_ZNK21btConvexInternalShape9getMarginEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape9getMarginEv,@function
_ZNK21btConvexInternalShape9getMarginEv: # @_ZNK21btConvexInternalShape9getMarginEv
	.cfi_startproc
# BB#0:
	movss	56(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	retq
.Lfunc_end19:
	.size	_ZNK21btConvexInternalShape9getMarginEv, .Lfunc_end19-_ZNK21btConvexInternalShape9getMarginEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,"axG",@progbits,_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,comdat
	.weak	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv,@function
_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv: # @_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end20:
	.size	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv, .Lfunc_end20-_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.cfi_endproc

	.section	.text._ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,"axG",@progbits,_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,comdat
	.weak	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3,@function
_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3: # @_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3, .Lfunc_end21-_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.cfi_endproc

	.section	.text._ZN26LocalSupportVertexCallbackD0Ev,"axG",@progbits,_ZN26LocalSupportVertexCallbackD0Ev,comdat
	.weak	_ZN26LocalSupportVertexCallbackD0Ev
	.p2align	4, 0x90
	.type	_ZN26LocalSupportVertexCallbackD0Ev,@function
_ZN26LocalSupportVertexCallbackD0Ev:    # @_ZN26LocalSupportVertexCallbackD0Ev
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 32
.Lcfi57:
	.cfi_offset %rbx, -24
.Lcfi58:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp41:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp42:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_2:
.Ltmp43:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN26LocalSupportVertexCallbackD0Ev, .Lfunc_end22-_ZN26LocalSupportVertexCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp41-.Lfunc_begin5   # >> Call Site 1 <<
	.long	.Ltmp42-.Ltmp41         #   Call between .Ltmp41 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin5   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp42-.Lfunc_begin5   # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp42    #   Call between .Ltmp42 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii,"axG",@progbits,_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii,comdat
	.weak	_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii
	.p2align	4, 0x90
	.type	_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii,@function
_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii: # @_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii
	.cfi_startproc
# BB#0:
	leaq	8(%rdi), %rax
	movss	24(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	28(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	32(%rdi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	movss	36(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	ucomiss	%xmm3, %xmm4
	jbe	.LBB23_2
# BB#1:
	movss	%xmm4, 24(%rdi)
	movups	(%rsi), %xmm3
	movups	%xmm3, (%rax)
	movaps	%xmm4, %xmm3
.LBB23_2:
	movss	16(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	20(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movss	24(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	ucomiss	%xmm3, %xmm4
	jbe	.LBB23_4
# BB#3:
	leaq	16(%rsi), %rcx
	movss	%xmm4, 24(%rdi)
	movups	(%rcx), %xmm3
	movups	%xmm3, (%rax)
	movaps	%xmm4, %xmm3
.LBB23_4:
	mulss	32(%rsi), %xmm1
	mulss	36(%rsi), %xmm2
	addss	%xmm1, %xmm2
	mulss	40(%rsi), %xmm0
	addss	%xmm2, %xmm0
	ucomiss	%xmm3, %xmm0
	jbe	.LBB23_6
# BB#5:
	addq	$32, %rsi
	movss	%xmm0, 24(%rdi)
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rax)
.LBB23_6:
	retq
.Lfunc_end23:
	.size	_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii, .Lfunc_end23-_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii
	.cfi_endproc

	.text
	.p2align	4, 0x90
	.type	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev,@function
_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev: # @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%r14
.Lcfi59:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi61:
	.cfi_def_cfa_offset 32
.Lcfi62:
	.cfi_offset %rbx, -24
.Lcfi63:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp44:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp45:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB24_2:
.Ltmp46:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev, .Lfunc_end24-_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp44-.Lfunc_begin6   # >> Call Site 1 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin6   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp45-.Lfunc_begin6   # >> Call Site 2 <<
	.long	.Lfunc_end24-.Ltmp45    #   Call between .Ltmp45 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI25_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_1:
	.long	1048576000              # float 0.25
	.text
	.p2align	4, 0x90
	.type	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii,@function
_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii: # @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_startproc
# BB#0:
	cmpb	$0, 8(%rdi)
	je	.LBB25_2
# BB#1:
	leaq	12(%rdi), %rax
	movups	(%rsi), %xmm0
	movups	%xmm0, (%rax)
	movb	$0, 8(%rdi)
	retq
.LBB25_2:
	movss	(%rsi), %xmm1           # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm15         # xmm15 = mem[0],zero,zero,zero
	movss	12(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	16(%rdi), %xmm13        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm10
	movaps	%xmm15, %xmm8
	movss	8(%rsi), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	20(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm11
	movss	16(%rsi), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	20(%rsi), %xmm7         # xmm7 = mem[0],zero,zero,zero
	addss	%xmm7, %xmm15
	subss	%xmm13, %xmm7
	movss	24(%rsi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	addss	%xmm6, %xmm2
	subss	%xmm14, %xmm6
	movss	32(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm9, %xmm1
	addss	%xmm3, %xmm1
	subss	%xmm12, %xmm3
	movss	36(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm15
	subss	%xmm13, %xmm4
	movss	40(%rsi), %xmm5         # xmm5 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm2
	subss	%xmm14, %xmm5
	movaps	%xmm6, %xmm0
	mulss	%xmm3, %xmm6
	mulss	%xmm7, %xmm3
	mulss	%xmm5, %xmm7
	mulss	%xmm4, %xmm0
	subss	%xmm0, %xmm7
	subss	%xmm12, %xmm10
	mulss	%xmm10, %xmm7
	subss	%xmm12, %xmm9
	mulss	%xmm9, %xmm5
	subss	%xmm5, %xmm6
	subss	%xmm13, %xmm8
	mulss	%xmm8, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	subss	%xmm14, %xmm11
	mulss	%xmm11, %xmm4
	addss	%xmm6, %xmm4
	addss	%xmm12, %xmm1
	addss	%xmm13, %xmm15
	andps	.LCPI25_0(%rip), %xmm4
	addss	%xmm14, %xmm2
	movaps	%xmm4, %xmm0
	mulss	.LCPI25_1(%rip), %xmm0
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm15
	mulss	%xmm0, %xmm2
	addss	28(%rdi), %xmm1
	movss	%xmm1, 28(%rdi)
	addss	32(%rdi), %xmm15
	movss	%xmm15, 32(%rdi)
	addss	36(%rdi), %xmm2
	movss	%xmm2, 36(%rdi)
	movss	44(%rdi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm0
	movss	%xmm0, 44(%rdi)
	retq
.Lfunc_end25:
	.size	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii, .Lfunc_end25-_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_endproc

	.p2align	4, 0x90
	.type	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev,@function
_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev: # @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi64:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi66:
	.cfi_def_cfa_offset 32
.Lcfi67:
	.cfi_offset %rbx, -24
.Lcfi68:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp47:
	callq	_ZN31btInternalTriangleIndexCallbackD2Ev
.Ltmp48:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_2:
.Ltmp49:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end26:
	.size	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev, .Lfunc_end26-_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp47-.Lfunc_begin7   # >> Call Site 1 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin7   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp48-.Lfunc_begin7   # >> Call Site 2 <<
	.long	.Lfunc_end26-.Ltmp48    #   Call between .Ltmp48 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI27_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI27_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI27_1:
	.long	3190467243              # float -0.166666672
.LCPI27_2:
	.long	1036831949              # float 0.100000001
.LCPI27_3:
	.long	1028443341              # float 0.0500000007
	.text
	.p2align	4, 0x90
	.type	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii,@function
_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii: # @_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_startproc
# BB#0:                                 # %.lr.ph.1
	movsd	(%rsi), %xmm14          # xmm14 = mem[0],zero
	movsd	56(%rdi), %xmm5         # xmm5 = mem[0],zero
	subps	%xmm5, %xmm14
	movss	8(%rsi), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	64(%rdi), %xmm6         # xmm6 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm7
	movaps	%xmm14, %xmm8
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	xorps	%xmm3, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm7, %xmm1            # xmm1 = xmm7[0],xmm1[1,2,3]
	movlps	%xmm14, -96(%rsp)
	movlps	%xmm1, -88(%rsp)
	movsd	16(%rsi), %xmm4         # xmm4 = mem[0],zero
	subps	%xmm5, %xmm4
	movss	24(%rsi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm1
	movaps	%xmm4, %xmm12
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movlps	%xmm4, -32(%rsp)
	movlps	%xmm2, -24(%rsp)
	movsd	32(%rsi), %xmm2         # xmm2 = mem[0],zero
	subps	%xmm5, %xmm2
	movss	40(%rsi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	subss	%xmm6, %xmm11
	movaps	%xmm2, %xmm13
	shufps	$229, %xmm13, %xmm13    # xmm13 = xmm13[1,1,2,3]
	movss	%xmm11, %xmm3           # xmm3 = xmm11[0],xmm3[1,2,3]
	movlps	%xmm2, -16(%rsp)
	movlps	%xmm3, -8(%rsp)
	movaps	%xmm12, %xmm3
	mulss	%xmm11, %xmm3
	movaps	%xmm1, %xmm5
	mulss	%xmm13, %xmm5
	subss	%xmm5, %xmm3
	mulss	%xmm14, %xmm3
	mulss	%xmm2, %xmm1
	movaps	%xmm11, %xmm5
	mulss	%xmm4, %xmm5
	subss	%xmm5, %xmm1
	mulss	%xmm8, %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm13, %xmm10
	mulss	%xmm4, %xmm10
	movaps	%xmm12, %xmm3
	mulss	%xmm2, %xmm3
	subss	%xmm3, %xmm10
	mulss	%xmm7, %xmm10
	addss	%xmm1, %xmm10
	andps	.LCPI27_0(%rip), %xmm10
	mulss	.LCPI27_1(%rip), %xmm10
	movaps	%xmm14, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm4, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	.LCPI27_2(%rip), %xmm1
	movaps	%xmm14, %xmm3
	mulss	%xmm4, %xmm3
	addss	%xmm3, %xmm3
	movaps	%xmm14, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm5, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm2, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm5, %xmm3
	addss	%xmm5, %xmm3
	mulss	.LCPI27_3(%rip), %xmm3
	addss	%xmm1, %xmm3
	mulss	%xmm10, %xmm3
	movss	%xmm3, -80(%rsp)
	xorl	%eax, %eax
	movss	-92(%rsp), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movss	.LCPI27_2(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movss	.LCPI27_3(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	movaps	%xmm4, %xmm1
	movaps	%xmm14, %xmm6
	jmp	.LBB27_1
	.p2align	4, 0x90
.LBB27_2:                               # %._crit_edge77
                                        #   in Loop: Header=BB27_1 Depth=1
	movss	-92(%rsp,%rax), %xmm6   # xmm6 = mem[0],zero,zero,zero
	movss	-28(%rsp,%rax), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movss	-12(%rsp,%rax), %xmm3   # xmm3 = mem[0],zero,zero,zero
	addq	$4, %rax
.LBB27_1:                               # =>This Inner Loop Header: Depth=1
	movaps	%xmm15, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm12, %xmm0
	mulss	%xmm1, %xmm0
	addss	%xmm7, %xmm0
	movaps	%xmm13, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm0, %xmm7
	mulss	%xmm8, %xmm7
	movaps	%xmm15, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm6, %xmm5
	mulss	%xmm12, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm15, %xmm0
	mulss	%xmm3, %xmm0
	addss	%xmm5, %xmm0
	mulss	%xmm13, %xmm6
	addss	%xmm0, %xmm6
	mulss	%xmm12, %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm13, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm9, %xmm1
	addss	%xmm7, %xmm1
	mulss	%xmm10, %xmm1
	movss	%xmm1, -76(%rsp,%rax,4)
	movss	%xmm1, -64(%rsp,%rax)
	cmpq	$4, %rax
	jne	.LBB27_2
# BB#3:                                 # %.lr.ph.2
	movss	-88(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	-24(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	xorl	%eax, %eax
	jmp	.LBB27_4
	.p2align	4, 0x90
.LBB27_5:                               # %._crit_edge84
                                        #   in Loop: Header=BB27_4 Depth=1
	movss	-92(%rsp,%rax), %xmm14  # xmm14 = mem[0],zero,zero,zero
	movss	-28(%rsp,%rax), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movss	-12(%rsp,%rax), %xmm2   # xmm2 = mem[0],zero,zero,zero
	addq	$4, %rax
.LBB27_4:                               # =>This Inner Loop Header: Depth=1
	movaps	%xmm5, %xmm0
	mulss	%xmm14, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm4, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm11, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm5, %xmm1
	mulss	%xmm4, %xmm1
	movaps	%xmm14, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm1, %xmm3
	movaps	%xmm5, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm11, %xmm14
	addss	%xmm1, %xmm14
	mulss	%xmm6, %xmm2
	addss	%xmm14, %xmm2
	mulss	%xmm11, %xmm4
	addss	%xmm2, %xmm4
	mulss	%xmm9, %xmm4
	addss	%xmm0, %xmm4
	mulss	%xmm10, %xmm4
	movss	%xmm4, -72(%rsp,%rax,4)
	movss	%xmm4, -48(%rsp,%rax)
	cmpq	$8, %rax
	jne	.LBB27_5
# BB#6:                                 # %._crit_edge.2
	movss	-80(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	-60(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	.LCPI27_4(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm2, %xmm0
	xorps	%xmm3, %xmm0
	subss	%xmm1, %xmm0
	xorps	%xmm3, %xmm1
	movss	-40(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm3
	subss	%xmm2, %xmm3
	movss	%xmm3, -80(%rsp)
	subss	%xmm4, %xmm1
	movss	%xmm1, -60(%rsp)
	movss	%xmm0, -40(%rsp)
	addss	8(%rdi), %xmm3
	movss	%xmm3, 8(%rdi)
	movss	-76(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	12(%rdi), %xmm2
	movss	%xmm2, 12(%rdi)
	movss	-72(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	16(%rdi), %xmm2
	movss	%xmm2, 16(%rdi)
	movss	-64(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	24(%rdi), %xmm2
	movss	%xmm2, 24(%rdi)
	addss	28(%rdi), %xmm1
	movss	%xmm1, 28(%rdi)
	movss	-56(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	32(%rdi), %xmm1
	movss	%xmm1, 32(%rdi)
	movss	-48(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	40(%rdi), %xmm1
	movss	%xmm1, 40(%rdi)
	movss	-44(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addss	44(%rdi), %xmm1
	movss	%xmm1, 44(%rdi)
	addss	48(%rdi), %xmm0
	movss	%xmm0, 48(%rdi)
	retq
.Lfunc_end27:
	.size	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii, .Lfunc_end27-_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii
	.cfi_endproc

	.type	_ZTV25btConvexTriangleMeshShape,@object # @_ZTV25btConvexTriangleMeshShape
	.section	.rodata,"a",@progbits
	.globl	_ZTV25btConvexTriangleMeshShape
	.p2align	3
_ZTV25btConvexTriangleMeshShape:
	.quad	0
	.quad	_ZTI25btConvexTriangleMeshShape
	.quad	_ZN13btConvexShapeD2Ev
	.quad	_ZN25btConvexTriangleMeshShapeD0Ev
	.quad	_ZNK34btPolyhedralConvexAabbCachingShape7getAabbERK11btTransformR9btVector3S4_
	.quad	_ZNK16btCollisionShape17getBoundingSphereER9btVector3Rf
	.quad	_ZNK16btCollisionShape20getAngularMotionDiscEv
	.quad	_ZNK16btCollisionShape27getContactBreakingThresholdEv
	.quad	_ZN25btConvexTriangleMeshShape15setLocalScalingERK9btVector3
	.quad	_ZNK25btConvexTriangleMeshShape15getLocalScalingEv
	.quad	_ZNK23btPolyhedralConvexShape21calculateLocalInertiaEfR9btVector3
	.quad	_ZNK25btConvexTriangleMeshShape7getNameEv
	.quad	_ZN21btConvexInternalShape9setMarginEf
	.quad	_ZNK21btConvexInternalShape9getMarginEv
	.quad	_ZNK25btConvexTriangleMeshShape24localGetSupportingVertexERK9btVector3
	.quad	_ZNK25btConvexTriangleMeshShape37localGetSupportingVertexWithoutMarginERK9btVector3
	.quad	_ZNK25btConvexTriangleMeshShape49batchedUnitVectorGetSupportingVertexWithoutMarginEPK9btVector3PS0_i
	.quad	_ZNK21btConvexInternalShape11getAabbSlowERK11btTransformR9btVector3S4_
	.quad	_ZNK21btConvexInternalShape36getNumPreferredPenetrationDirectionsEv
	.quad	_ZNK21btConvexInternalShape32getPreferredPenetrationDirectionEiR9btVector3
	.quad	_ZNK25btConvexTriangleMeshShape14getNumVerticesEv
	.quad	_ZNK25btConvexTriangleMeshShape11getNumEdgesEv
	.quad	_ZNK25btConvexTriangleMeshShape7getEdgeEiR9btVector3S1_
	.quad	_ZNK25btConvexTriangleMeshShape9getVertexEiR9btVector3
	.quad	_ZNK25btConvexTriangleMeshShape12getNumPlanesEv
	.quad	_ZNK25btConvexTriangleMeshShape8getPlaneER9btVector3S1_i
	.quad	_ZNK25btConvexTriangleMeshShape8isInsideERK9btVector3f
	.size	_ZTV25btConvexTriangleMeshShape, 216

	.type	_ZTS25btConvexTriangleMeshShape,@object # @_ZTS25btConvexTriangleMeshShape
	.globl	_ZTS25btConvexTriangleMeshShape
	.p2align	4
_ZTS25btConvexTriangleMeshShape:
	.asciz	"25btConvexTriangleMeshShape"
	.size	_ZTS25btConvexTriangleMeshShape, 28

	.type	_ZTI25btConvexTriangleMeshShape,@object # @_ZTI25btConvexTriangleMeshShape
	.globl	_ZTI25btConvexTriangleMeshShape
	.p2align	4
_ZTI25btConvexTriangleMeshShape:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS25btConvexTriangleMeshShape
	.quad	_ZTI34btPolyhedralConvexAabbCachingShape
	.size	_ZTI25btConvexTriangleMeshShape, 24

	.type	_ZTV26LocalSupportVertexCallback,@object # @_ZTV26LocalSupportVertexCallback
	.section	.rodata._ZTV26LocalSupportVertexCallback,"aG",@progbits,_ZTV26LocalSupportVertexCallback,comdat
	.weak	_ZTV26LocalSupportVertexCallback
	.p2align	3
_ZTV26LocalSupportVertexCallback:
	.quad	0
	.quad	_ZTI26LocalSupportVertexCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZN26LocalSupportVertexCallbackD0Ev
	.quad	_ZN26LocalSupportVertexCallback28internalProcessTriangleIndexEP9btVector3ii
	.size	_ZTV26LocalSupportVertexCallback, 40

	.type	_ZTS26LocalSupportVertexCallback,@object # @_ZTS26LocalSupportVertexCallback
	.section	.rodata._ZTS26LocalSupportVertexCallback,"aG",@progbits,_ZTS26LocalSupportVertexCallback,comdat
	.weak	_ZTS26LocalSupportVertexCallback
	.p2align	4
_ZTS26LocalSupportVertexCallback:
	.asciz	"26LocalSupportVertexCallback"
	.size	_ZTS26LocalSupportVertexCallback, 29

	.type	_ZTI26LocalSupportVertexCallback,@object # @_ZTI26LocalSupportVertexCallback
	.section	.rodata._ZTI26LocalSupportVertexCallback,"aG",@progbits,_ZTI26LocalSupportVertexCallback,comdat
	.weak	_ZTI26LocalSupportVertexCallback
	.p2align	4
_ZTI26LocalSupportVertexCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS26LocalSupportVertexCallback
	.quad	_ZTI31btInternalTriangleIndexCallback
	.size	_ZTI26LocalSupportVertexCallback, 24

	.type	_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback,@object # @_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback
	.section	.rodata,"a",@progbits
	.p2align	3
_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback:
	.quad	0
	.quad	_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallbackD0Ev
	.quad	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN14CenterCallback28internalProcessTriangleIndexEPS2_ii
	.size	_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback, 40

	.type	_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback,@object # @_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback
	.p2align	4
_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback:
	.asciz	"ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback"
	.size	_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback, 109

	.type	_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback,@object # @_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback
	.p2align	4
_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback
	.quad	_ZTI31btInternalTriangleIndexCallback
	.size	_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE14CenterCallback, 24

	.type	_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback,@object # @_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback
	.p2align	3
_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback:
	.quad	0
	.quad	_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback
	.quad	_ZN31btInternalTriangleIndexCallbackD2Ev
	.quad	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallbackD0Ev
	.quad	_ZZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfEN15InertiaCallback28internalProcessTriangleIndexEPS2_ii
	.size	_ZTVZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback, 40

	.type	_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback,@object # @_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback
	.p2align	4
_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback:
	.asciz	"ZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback"
	.size	_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback, 110

	.type	_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback,@object # @_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback
	.p2align	4
_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback
	.quad	_ZTI31btInternalTriangleIndexCallback
	.size	_ZTIZNK25btConvexTriangleMeshShape31calculatePrincipalAxisTransformER11btTransformR9btVector3RfE15InertiaCallback, 24

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ConvexTrimesh"
	.size	.L.str, 14


	.globl	_ZN25btConvexTriangleMeshShapeC1EP23btStridingMeshInterfaceb
	.type	_ZN25btConvexTriangleMeshShapeC1EP23btStridingMeshInterfaceb,@function
_ZN25btConvexTriangleMeshShapeC1EP23btStridingMeshInterfaceb = _ZN25btConvexTriangleMeshShapeC2EP23btStridingMeshInterfaceb
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
