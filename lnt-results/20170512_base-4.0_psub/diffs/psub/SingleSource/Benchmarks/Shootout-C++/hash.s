	.text
	.file	"hash.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$80, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 128
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	$500000, %ebx           # imm = 0x7A120
	cmpl	$2, %edi
	jne	.LBB0_2
# BB#1:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movq	%rax, %rbx
.LBB0_2:
.Ltmp0:
	leaq	24(%rsp), %rdi
	leaq	16(%rsp), %rdx
	leaq	8(%rsp), %rcx
	movq	%rsp, %r8
	movl	$100, %esi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
.Ltmp1:
# BB#3:                                 # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEC2Ev.exit
	testl	%ebx, %ebx
	jle	.LBB0_12
# BB#4:                                 # %.lr.ph61
	leaq	64(%rsp), %r12
	leaq	24(%rsp), %r14
	leaq	16(%rsp), %r15
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_5:                                # =>This Inner Loop Header: Depth=1
	incl	%ebp
	movl	$.L.str, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	movl	%ebp, %edx
	callq	sprintf
	movq	%r12, %rdi
	callq	strdup
	movq	%rax, 16(%rsp)
.Ltmp3:
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp4:
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=1
	movl	%ebp, (%rax)
	cmpl	%ebx, %ebp
	jl	.LBB0_5
# BB#7:                                 # %.preheader
	testl	%ebx, %ebx
	jle	.LBB0_12
# BB#8:                                 # %.lr.ph
	leaq	64(%rsp), %r14
	leaq	24(%rsp), %r15
	leaq	16(%rsp), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_9:                                # =>This Inner Loop Header: Depth=1
	movl	$.L.str.1, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebx, %edx
	callq	sprintf
	movq	%r14, %rdi
	callq	strdup
	movq	%rax, 16(%rsp)
.Ltmp6:
	movq	%r15, %rdi
	movq	%r12, %rsi
	callq	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
.Ltmp7:
# BB#10:                                #   in Loop: Header=BB0_9 Depth=1
	cmpl	$1, (%rax)
	sbbl	$-1, %ebp
	cmpl	$1, %ebx
	leal	-1(%rbx), %eax
	movl	%eax, %ebx
	jg	.LBB0_9
	jmp	.LBB0_13
.LBB0_12:
	xorl	%ebp, %ebp
.LBB0_13:                               # %._crit_edge
.Ltmp9:
	movl	$_ZSt4cout, %edi
	movl	%ebp, %esi
	callq	_ZNSolsEi
	movq	%rax, %rbx
.Ltmp10:
# BB#14:
	movq	(%rbx), %rax
	movq	-24(%rax), %rax
	movq	240(%rbx,%rax), %rbp
	testq	%rbp, %rbp
	je	.LBB0_32
# BB#15:                                # %.noexc49
	cmpb	$0, 56(%rbp)
	je	.LBB0_17
# BB#16:
	movb	67(%rbp), %al
	jmp	.LBB0_19
.LBB0_17:
.Ltmp11:
	movq	%rbp, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
.Ltmp12:
# BB#18:                                # %.noexc51
	movq	(%rbp), %rax
.Ltmp13:
	movl	$10, %esi
	movq	%rbp, %rdi
	callq	*48(%rax)
.Ltmp14:
.LBB0_19:                               # %.noexc
.Ltmp15:
	movsbl	%al, %esi
	movq	%rbx, %rdi
	callq	_ZNSo3putEc
.Ltmp16:
# BB#20:                                # %.noexc32
.Ltmp17:
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
.Ltmp18:
# BB#21:                                # %_ZNSolsEPFRSoS_E.exit
	cmpq	$0, 56(%rsp)
	je	.LBB0_29
# BB#22:                                # %.preheader.i.i.i
	movq	32(%rsp), %rdi
	cmpq	%rdi, 40(%rsp)
	je	.LBB0_28
# BB#23:                                # %.lr.ph16.i.i.i.preheader
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_24:                               # %.lr.ph16.i.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_25 Depth 2
	movq	(%rdi,%rbx,8), %rax
	testq	%rax, %rax
	je	.LBB0_27
	.p2align	4, 0x90
.LBB0_25:                               # %.lr.ph.i.i.i
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbp
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbp, %rbp
	movq	%rbp, %rax
	jne	.LBB0_25
# BB#26:                                # %._crit_edge.loopexit.i.i.i
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	32(%rsp), %rdi
.LBB0_27:                               # %._crit_edge.i.i.i
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	$0, (%rdi,%rbx,8)
	incq	%rbx
	movq	32(%rsp), %rdi
	movq	40(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbx
	jb	.LBB0_24
.LBB0_28:                               # %._crit_edge17.i.i.i
	movq	$0, 56(%rsp)
	testq	%rdi, %rdi
	jne	.LBB0_30
	jmp	.LBB0_31
.LBB0_29:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_31
.LBB0_30:
	callq	_ZdlPv
.LBB0_31:                               # %_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEED2Ev.exit
	xorl	%eax, %eax
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB0_32:
.Ltmp19:
	callq	_ZSt16__throw_bad_castv
.Ltmp20:
# BB#33:                                # %.noexc53
.LBB0_34:
.Ltmp2:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_35:
.Ltmp21:
	jmp	.LBB0_38
.LBB0_36:
.Ltmp8:
	jmp	.LBB0_38
.LBB0_37:
.Ltmp5:
.LBB0_38:
	movq	%rax, %r14
	cmpq	$0, 56(%rsp)
	je	.LBB0_48
# BB#39:                                # %.preheader.i.i.i38
	movq	32(%rsp), %rdi
	cmpq	%rdi, 40(%rsp)
	je	.LBB0_45
# BB#40:                                # %.lr.ph16.i.i.i41.preheader
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB0_41:                               # %.lr.ph16.i.i.i41
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_42 Depth 2
	movq	(%rdi,%rbp,8), %rax
	testq	%rax, %rax
	je	.LBB0_44
	.p2align	4, 0x90
.LBB0_42:                               # %.lr.ph.i.i.i43
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rbx
	movq	%rax, %rdi
	callq	_ZdlPv
	testq	%rbx, %rbx
	movq	%rbx, %rax
	jne	.LBB0_42
# BB#43:                                # %._crit_edge.loopexit.i.i.i45
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	32(%rsp), %rdi
.LBB0_44:                               # %._crit_edge.i.i.i46
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	$0, (%rdi,%rbp,8)
	incq	%rbp
	movq	32(%rsp), %rdi
	movq	40(%rsp), %rax
	subq	%rdi, %rax
	sarq	$3, %rax
	cmpq	%rax, %rbp
	jb	.LBB0_41
.LBB0_45:                               # %._crit_edge17.i.i.i39
	movq	$0, 56(%rsp)
	testq	%rdi, %rdi
	je	.LBB0_47
.LBB0_46:
	callq	_ZdlPv
.LBB0_47:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB0_48:                               # %._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE5clearEv.exit_crit_edge.i.i37
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB0_46
	jmp	.LBB0_47
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table0:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\303\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	65                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	0                       #   On action: cleanup
	.long	.Ltmp6-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin0    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp20-.Ltmp9          #   Call between .Ltmp9 and .Ltmp20
	.long	.Ltmp21-.Lfunc_begin0   #     jumps to .Ltmp21
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Lfunc_end0-.Ltmp20     #   Call between .Ltmp20 and .Lfunc_end0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_,"axG",@progbits,_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_,comdat
	.weak	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_,@function
_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_: # @_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi13:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi14:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 48
.Lcfi16:
	.cfi_offset %rbx, -48
.Lcfi17:
	.cfi_offset %r12, -40
.Lcfi18:
	.cfi_offset %r13, -32
.Lcfi19:
	.cfi_offset %r14, -24
.Lcfi20:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	(%rsi), %r12
	movq	32(%r14), %rsi
	incq	%rsi
	callq	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	movq	8(%r14), %rcx
	movq	16(%r14), %rsi
	subq	%rcx, %rsi
	sarq	$3, %rsi
	movb	(%r12), %dl
	testb	%dl, %dl
	je	.LBB1_1
# BB#2:                                 # %.lr.ph.i.i.i.i.i.i.preheader
	leaq	1(%r12), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_3:                                # %.lr.ph.i.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	leaq	(%rax,%rax,4), %rbx
	movsbq	%dl, %rax
	addq	%rbx, %rax
	movzbl	(%rdi), %edx
	incq	%rdi
	testb	%dl, %dl
	jne	.LBB1_3
	jmp	.LBB1_4
.LBB1_1:
	xorl	%eax, %eax
.LBB1_4:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE10_M_bkt_numERKS5_.exit.i
	xorl	%edx, %edx
	divq	%rsi
	movq	%rdx, %r15
	movq	(%rcx,%r15,8), %r13
	testq	%r13, %r13
	je	.LBB1_9
# BB#5:                                 # %.lr.ph.i.preheader
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rdi
	movq	%r12, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB1_7
# BB#8:                                 #   in Loop: Header=BB1_6 Depth=1
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_6
.LBB1_9:                                # %._crit_edge.i
	movl	$24, %edi
	callq	_Znwm
	movq	%r12, 8(%rax)
	movl	$0, 16(%rax)
	movq	%r13, (%rax)
	movq	8(%r14), %rcx
	movq	%rax, (%rcx,%r15,8)
	leaq	8(%rax), %rbx
	incq	32(%r14)
	jmp	.LBB1_10
.LBB1_7:
	addq	$8, %rbx
.LBB1_10:                               # %_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE14find_or_insertERKS5_.exit
	addq	$8, %rbx
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_, .Lfunc_end1-_ZN9__gnu_cxx8hash_mapIPKciNS_4hashIS2_EE5eqstrSaIiEEixERKS2_
	.cfi_endproc

	.section	.text._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,"axG",@progbits,_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,comdat
	.weak	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E,@function
_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E: # @_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi23:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 48
.Lcfi26:
	.cfi_offset %rbx, -40
.Lcfi27:
	.cfi_offset %r12, -32
.Lcfi28:
	.cfi_offset %r14, -24
.Lcfi29:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	8(%r15), %r14
	xorps	%xmm0, %xmm0
	movups	%xmm0, 24(%r15)
	movups	%xmm0, 8(%r15)
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, %eax
	movl	$29, %ecx
	jmp	.LBB2_1
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	leaq	(%rax,%rcx,8), %rax
	addq	$8, %rax
	decq	%rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB2_1:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	testq	%rdx, %rdx
	jle	.LBB2_4
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	%rdx, %rcx
	shrq	%rcx
	cmpq	%rsi, (%rax,%rcx,8)
	jae	.LBB2_1
	jmp	.LBB2_3
.LBB2_4:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE12_M_next_sizeEm.exit.i
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+232, %ecx
	cmpq	%rcx, %rax
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+224, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %rbx
	movq	%rbx, %rax
	shrq	$61, %rax
	jne	.LBB2_5
# BB#7:
	testq	%rbx, %rbx
	je	.LBB2_8
# BB#9:                                 # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE11_M_allocateEm.exit.i.i
	leaq	(,%rbx,8), %rdi
.Ltmp22:
	callq	_Znwm
	movq	%rax, %r12
.Ltmp23:
# BB#10:                                # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE20_M_allocate_and_copyIPS8_EESC_mT_SD_.exit.i
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_12
# BB#11:
	callq	_ZdlPv
.LBB2_12:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE13_M_deallocateEPS8_m.exit.i
	movq	%r12, 8(%r15)
	movq	%r12, 16(%r15)
	leaq	(%r12,%rbx,8), %rax
	movq	%rax, 24(%r15)
	jmp	.LBB2_13
.LBB2_8:
	xorl	%r12d, %r12d
.LBB2_13:                               # %.noexc
	movq	$0, (%rsp)
.Ltmp24:
	movq	%rsp, %rcx
	movq	%r14, %rdi
	movq	%r12, %rsi
	movq	%rbx, %rdx
	callq	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
.Ltmp25:
# BB#14:
	movq	$0, 32(%r15)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.LBB2_5:
.Ltmp26:
	movl	$.L.str.2, %edi
	callq	_ZSt20__throw_length_errorPKc
.Ltmp27:
# BB#6:                                 # %.noexc9
.LBB2_15:
.Ltmp28:
	movq	%rax, %rbx
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB2_17
# BB#16:
	callq	_ZdlPv
.LBB2_17:                               # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E, .Lfunc_end2-_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEEC2EmRKS7_RKSA_RKSaIS5_E
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp22-.Lfunc_begin1   # >> Call Site 1 <<
	.long	.Ltmp27-.Ltmp22         #   Call between .Ltmp22 and .Ltmp27
	.long	.Ltmp28-.Lfunc_begin1   #     jumps to .Ltmp28
	.byte	0                       #   On action: cleanup
	.long	.Ltmp27-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Lfunc_end2-.Ltmp27     #   Call between .Ltmp27 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,"axG",@progbits,_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,comdat
	.weak	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.p2align	4, 0x90
	.type	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_,@function
_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_: # @_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi30:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi31:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi33:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 64
.Lcfi37:
	.cfi_offset %rbx, -56
.Lcfi38:
	.cfi_offset %r12, -48
.Lcfi39:
	.cfi_offset %r13, -40
.Lcfi40:
	.cfi_offset %r14, -32
.Lcfi41:
	.cfi_offset %r15, -24
.Lcfi42:
	.cfi_offset %rbp, -16
	movq	%rcx, %r13
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %r15
	testq	%r14, %r14
	je	.LBB3_75
# BB#1:
	movq	8(%r15), %r12
	movq	16(%r15), %rax
	subq	%r12, %rax
	sarq	$3, %rax
	cmpq	%r14, %rax
	jae	.LBB3_2
# BB#49:
	movabsq	$2305843009213693951, %rax # imm = 0x1FFFFFFFFFFFFFFF
	movq	%r15, %rbp
	movq	(%r15), %r15
	subq	%r15, %r12
	sarq	$3, %r12
	movq	%rax, %rcx
	subq	%r12, %rcx
	cmpq	%r14, %rcx
	jb	.LBB3_76
# BB#50:                                # %_ZNKSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE12_M_check_lenEmS4_.exit
	cmpq	%r14, %r12
	movq	%r12, %rcx
	cmovbq	%r14, %rcx
	leaq	(%rcx,%r12), %rdx
	cmpq	%rax, %rdx
	cmovaq	%rax, %rdx
	addq	%r12, %rcx
	cmovbq	%rax, %rdx
	testq	%rdx, %rdx
	movq	%rdx, (%rsp)            # 8-byte Spill
	je	.LBB3_51
# BB#52:
	cmpq	%rax, %rdx
	ja	.LBB3_77
# BB#53:                                # %_ZN9__gnu_cxx14__alloc_traitsISaIPNS_15_Hashtable_nodeISt4pairIKPKciEEEEE8allocateERS9_m.exit.i
	leaq	(,%rdx,8), %rdi
	callq	_Znwm
	movq	%rax, %r12
	jmp	.LBB3_54
.LBB3_2:
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	(%r13), %rbp
	movq	%r12, %rdx
	subq	%rbx, %rdx
	movq	%rdx, %r13
	sarq	$3, %r13
	cmpq	%r14, %r13
	jbe	.LBB3_20
# BB#3:
	leaq	(,%r14,8), %rdx
	movq	%r12, %r13
	subq	%rdx, %r13
	testq	%rdx, %rdx
	movq	%r12, %rax
	je	.LBB3_5
# BB#4:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
	movabsq	$4611686018427387900, %r9 # imm = 0x3FFFFFFFFFFFFFFC
	movq	8(%r15), %rax
.LBB3_5:                                # %_ZSt22__uninitialized_move_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES9_SaIS8_EET0_T_SC_SB_RT1_.exit
	leaq	(%rax,%r14,8), %rax
	movq	%rax, 8(%r15)
	subq	%rbx, %r13
	movq	%r13, %rax
	sarq	$3, %rax
	je	.LBB3_7
# BB#6:
	shlq	$3, %rax
	subq	%rax, %r12
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r13, %rdx
	movq	%r9, %r15
	callq	memmove
	movq	%r15, %r9
.LBB3_7:                                # %.lr.ph.i.i68.preheader
	leaq	-8(,%r14,8), %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	movq	%rbx, %rcx
	jb	.LBB3_18
# BB#8:                                 # %min.iters.checked121
	andq	%rax, %r9
	movq	%rbx, %rcx
	je	.LBB3_18
# BB#9:                                 # %vector.ph125
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB3_10
# BB#11:                                # %vector.body117.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_12:                               # %vector.body117.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB3_12
	jmp	.LBB3_13
.LBB3_20:
	subq	%r13, %r14
	movq	%r12, %rdi
	je	.LBB3_34
# BB#21:                                # %.lr.ph.i.i.i.i.i64.preheader
	cmpq	$3, %r14
	movq	%r14, %rsi
	movq	%r12, %rax
	jbe	.LBB3_32
# BB#22:                                # %min.iters.checked
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %rcx
	andq	$-4, %rcx
	movq	%r14, %rsi
	movq	%r12, %rax
	je	.LBB3_32
# BB#23:                                # %vector.ph
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%rcx), %rax
	movl	%eax, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB3_24
# BB#25:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB3_26:                               # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%r12,%rdi,8)
	movdqu	%xmm0, 16(%r12,%rdi,8)
	addq	$4, %rdi
	incq	%rsi
	jne	.LBB3_26
	jmp	.LBB3_27
.LBB3_51:
	xorl	%r12d, %r12d
.LBB3_54:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE11_M_allocateEm.exit
	movq	%rbx, %rdi
	subq	%r15, %rdi
	sarq	$3, %rdi
	leaq	(%r12,%rdi,8), %rax
	movq	(%r13), %rcx
	cmpq	$4, %r14
	jae	.LBB3_56
# BB#55:
	movq	%r14, %rdx
	movq	%rbp, %r15
	jmp	.LBB3_67
.LBB3_56:                               # %min.iters.checked139
	movq	%r14, %r8
	andq	$-4, %r8
	movq	%r14, %r9
	andq	$-4, %r9
	movq	%rbp, %r15
	je	.LBB3_57
# BB#58:                                # %vector.ph143
	movd	%rcx, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %r10
	movl	%r10d, %edx
	shrl	$2, %edx
	incl	%edx
	andq	$7, %rdx
	je	.LBB3_59
# BB#60:                                # %vector.body135.prol.preheader
	leaq	16(%r12,%rdi,8), %rbp
	negq	%rdx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB3_61:                               # %vector.body135.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -16(%rbp,%rsi,8)
	movdqu	%xmm0, (%rbp,%rsi,8)
	addq	$4, %rsi
	incq	%rdx
	jne	.LBB3_61
	jmp	.LBB3_62
.LBB3_57:
	movq	%r14, %rdx
	jmp	.LBB3_67
.LBB3_59:
	xorl	%esi, %esi
.LBB3_62:                               # %vector.body135.prol.loopexit
	cmpq	$28, %r10
	jb	.LBB3_65
# BB#63:                                # %vector.ph143.new
	movq	%r9, %rdx
	subq	%rsi, %rdx
	addq	%rsi, %rdi
	leaq	240(%r12,%rdi,8), %rsi
	.p2align	4, 0x90
.LBB3_64:                               # %vector.body135
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rsi)
	movdqu	%xmm0, -224(%rsi)
	movdqu	%xmm0, -208(%rsi)
	movdqu	%xmm0, -192(%rsi)
	movdqu	%xmm0, -176(%rsi)
	movdqu	%xmm0, -160(%rsi)
	movdqu	%xmm0, -144(%rsi)
	movdqu	%xmm0, -128(%rsi)
	movdqu	%xmm0, -112(%rsi)
	movdqu	%xmm0, -96(%rsi)
	movdqu	%xmm0, -80(%rsi)
	movdqu	%xmm0, -64(%rsi)
	movdqu	%xmm0, -48(%rsi)
	movdqu	%xmm0, -32(%rsi)
	movdqu	%xmm0, -16(%rsi)
	movdqu	%xmm0, (%rsi)
	addq	$256, %rsi              # imm = 0x100
	addq	$-32, %rdx
	jne	.LBB3_64
.LBB3_65:                               # %middle.block136
	cmpq	%r14, %r9
	je	.LBB3_68
# BB#66:
	movq	%r14, %rdx
	subq	%r8, %rdx
	leaq	(%rax,%r9,8), %rax
	.p2align	4, 0x90
.LBB3_67:                               # %.lr.ph.i.i.i.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, (%rax)
	addq	$8, %rax
	decq	%rdx
	jne	.LBB3_67
.LBB3_68:                               # %_ZSt24__uninitialized_fill_n_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEEmS8_S8_ET_SA_T0_RKT1_RSaIT2_E.exit
	movq	(%r15), %r13
	movq	%rbx, %rdx
	subq	%r13, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB3_70
# BB#69:
	movq	%r12, %rdi
	movq	%r13, %rsi
	callq	memmove
.LBB3_70:
	leaq	(%r12,%rbp,8), %rax
	leaq	(%rax,%r14,8), %r14
	movq	8(%r15), %rdx
	subq	%rbx, %rdx
	movq	%rdx, %rbp
	sarq	$3, %rbp
	je	.LBB3_72
# BB#71:
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	memmove
.LBB3_72:
	leaq	(%r14,%rbp,8), %rbx
	testq	%r13, %r13
	je	.LBB3_74
# BB#73:
	movq	%r13, %rdi
	callq	_ZdlPv
.LBB3_74:                               # %_ZNSt12_Vector_baseIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE13_M_deallocateEPS8_m.exit57
	movq	%r12, (%r15)
	movq	%rbx, 8(%r15)
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	(%r12,%rax,8), %rax
	movq	%rax, 16(%r15)
	jmp	.LBB3_75
.LBB3_10:
	xorl	%edx, %edx
.LBB3_13:                               # %vector.body117.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB3_16
# BB#14:                                # %vector.ph125.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB3_15:                               # %vector.body117
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB3_15
.LBB3_16:                               # %middle.block118
	cmpq	%r9, %rax
	je	.LBB3_75
# BB#17:
	leaq	(%rbx,%r9,8), %rcx
.LBB3_18:                               # %.lr.ph.i.i68.preheader160
	leaq	(%rbx,%r14,8), %rax
	.p2align	4, 0x90
.LBB3_19:                               # %.lr.ph.i.i68
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rcx)
	addq	$8, %rcx
	cmpq	%rcx, %rax
	jne	.LBB3_19
	jmp	.LBB3_75
.LBB3_24:
	xorl	%edi, %edi
.LBB3_27:                               # %vector.body.prol.loopexit
	cmpq	$28, %rax
	jb	.LBB3_30
# BB#28:                                # %vector.ph.new
	movq	%rcx, %rsi
	subq	%rdi, %rsi
	leaq	240(%r12,%rdi,8), %rdi
	.p2align	4, 0x90
.LBB3_29:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdi)
	movdqu	%xmm0, -224(%rdi)
	movdqu	%xmm0, -208(%rdi)
	movdqu	%xmm0, -192(%rdi)
	movdqu	%xmm0, -176(%rdi)
	movdqu	%xmm0, -160(%rdi)
	movdqu	%xmm0, -144(%rdi)
	movdqu	%xmm0, -128(%rdi)
	movdqu	%xmm0, -112(%rdi)
	movdqu	%xmm0, -96(%rdi)
	movdqu	%xmm0, -80(%rdi)
	movdqu	%xmm0, -64(%rdi)
	movdqu	%xmm0, -48(%rdi)
	movdqu	%xmm0, -32(%rdi)
	movdqu	%xmm0, -16(%rdi)
	movdqu	%xmm0, (%rdi)
	addq	$256, %rdi              # imm = 0x100
	addq	$-32, %rsi
	jne	.LBB3_29
.LBB3_30:                               # %middle.block
	cmpq	%rcx, %r14
	je	.LBB3_33
# BB#31:
	movq	%r14, %rsi
	subq	%r8, %rsi
	leaq	(%r12,%rcx,8), %rax
	.p2align	4, 0x90
.LBB3_32:                               # %.lr.ph.i.i.i.i.i64
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rax)
	addq	$8, %rax
	decq	%rsi
	jne	.LBB3_32
.LBB3_33:                               # %._crit_edge.loopexit.i.i.i.i.i61
	leaq	(%r12,%r14,8), %rdi
.LBB3_34:                               # %_ZSt24__uninitialized_fill_n_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEEmS8_S8_ET_SA_T0_RKT1_RSaIT2_E.exit66
	movq	%rdi, 8(%r15)
	testq	%rdx, %rdx
	je	.LBB3_36
# BB#35:
	movq	%rbx, %rsi
	movq	%r9, %r14
	callq	memmove
	movq	%r14, %r9
	movq	8(%r15), %rdi
.LBB3_36:                               # %_ZSt22__uninitialized_move_aIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES9_SaIS8_EET0_T_SC_SB_RT1_.exit59
	leaq	(%rdi,%r13,8), %rax
	movq	%rax, 8(%r15)
	cmpq	%rbx, %r12
	je	.LBB3_75
# BB#37:                                # %.lr.ph.i.i.preheader
	leaq	-8(%r12), %rax
	subq	%rbx, %rax
	shrq	$3, %rax
	incq	%rax
	cmpq	$4, %rax
	jb	.LBB3_48
# BB#38:                                # %min.iters.checked103
	andq	%rax, %r9
	je	.LBB3_48
# BB#39:                                # %vector.ph107
	movd	%rbp, %xmm0
	pshufd	$68, %xmm0, %xmm0       # xmm0 = xmm0[0,1,0,1]
	leaq	-4(%r9), %rcx
	movl	%ecx, %esi
	shrl	$2, %esi
	incl	%esi
	andq	$7, %rsi
	je	.LBB3_40
# BB#41:                                # %vector.body97.prol.preheader
	negq	%rsi
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB3_42:                               # %vector.body97.prol
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, (%rbx,%rdx,8)
	movdqu	%xmm0, 16(%rbx,%rdx,8)
	addq	$4, %rdx
	incq	%rsi
	jne	.LBB3_42
	jmp	.LBB3_43
.LBB3_40:
	xorl	%edx, %edx
.LBB3_43:                               # %vector.body97.prol.loopexit
	cmpq	$28, %rcx
	jb	.LBB3_46
# BB#44:                                # %vector.ph107.new
	movq	%r9, %rcx
	subq	%rdx, %rcx
	leaq	240(%rbx,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB3_45:                               # %vector.body97
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm0, -240(%rdx)
	movdqu	%xmm0, -224(%rdx)
	movdqu	%xmm0, -208(%rdx)
	movdqu	%xmm0, -192(%rdx)
	movdqu	%xmm0, -176(%rdx)
	movdqu	%xmm0, -160(%rdx)
	movdqu	%xmm0, -144(%rdx)
	movdqu	%xmm0, -128(%rdx)
	movdqu	%xmm0, -112(%rdx)
	movdqu	%xmm0, -96(%rdx)
	movdqu	%xmm0, -80(%rdx)
	movdqu	%xmm0, -64(%rdx)
	movdqu	%xmm0, -48(%rdx)
	movdqu	%xmm0, -32(%rdx)
	movdqu	%xmm0, -16(%rdx)
	movdqu	%xmm0, (%rdx)
	addq	$256, %rdx              # imm = 0x100
	addq	$-32, %rcx
	jne	.LBB3_45
.LBB3_46:                               # %middle.block98
	cmpq	%r9, %rax
	je	.LBB3_75
# BB#47:
	leaq	(%rbx,%r9,8), %rbx
	.p2align	4, 0x90
.LBB3_48:                               # %.lr.ph.i.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rbp, (%rbx)
	addq	$8, %rbx
	cmpq	%rbx, %r12
	jne	.LBB3_48
.LBB3_75:                               # %_ZSt4fillIPPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEES8_EvT_SA_RKT0_.exit69
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB3_76:
	movl	$.L.str.3, %edi
	callq	_ZSt20__throw_length_errorPKc
.LBB3_77:
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end3:
	.size	_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_, .Lfunc_end3-_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EE14_M_fill_insertENS0_17__normal_iteratorIPS8_SA_EEmRKS8_
	.cfi_endproc

	.section	.text._ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,"axG",@progbits,_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,comdat
	.weak	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.p2align	4, 0x90
	.type	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm,@function
_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm: # @_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi43:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi44:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi45:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi46:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi47:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi48:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi49:
	.cfi_def_cfa_offset 64
.Lcfi50:
	.cfi_offset %rbx, -56
.Lcfi51:
	.cfi_offset %r12, -48
.Lcfi52:
	.cfi_offset %r13, -40
.Lcfi53:
	.cfi_offset %r14, -32
.Lcfi54:
	.cfi_offset %r15, -24
.Lcfi55:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	16(%r14), %r12
	subq	8(%r14), %r12
	sarq	$3, %r12
	cmpq	%rsi, %r12
	jae	.LBB4_18
# BB#1:                                 # %.outer.i.i.i.i.preheader
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, %eax
	movl	$29, %ecx
	jmp	.LBB4_2
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	leaq	(%rax,%rcx,8), %rax
	addq	$8, %rax
	decq	%rdx
	subq	%rcx, %rdx
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	%rcx, %rdx
	testq	%rdx, %rdx
	jle	.LBB4_5
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movq	%rdx, %rcx
	shrq	%rcx
	cmpq	%rsi, (%rax,%rcx,8)
	jae	.LBB4_2
	jmp	.LBB4_4
.LBB4_5:                                # %_ZNK9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE12_M_next_sizeEm.exit
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+232, %ecx
	cmpq	%rcx, %rax
	movl	$_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE+224, %ecx
	cmovneq	%rax, %rcx
	movq	(%rcx), %r13
	cmpq	%r12, %r13
	jbe	.LBB4_18
# BB#6:
	movq	%r13, %rax
	shrq	$61, %rax
	jne	.LBB4_19
# BB#7:                                 # %_ZN9__gnu_cxx14__alloc_traitsISaIPNS_15_Hashtable_nodeISt4pairIKPKciEEEEE8allocateERS9_m.exit.i.i.i.i
	leaq	(,%r13,8), %rbx
	movq	%rbx, %rdi
	callq	_Znwm
	movq	%rax, %r15
	leaq	(%r15,%r13,8), %rbp
	xorl	%esi, %esi
	movq	%r15, %rdi
	movq	%rbx, %rdx
	callq	memset
	testq	%r12, %r12
	movq	8(%r14), %rdi
	je	.LBB4_8
# BB#9:                                 # %.preheader.preheader
	xorl	%ecx, %ecx
	movq	%rbp, %r8
	jmp	.LBB4_10
	.p2align	4, 0x90
.LBB4_15:                               # %.loopexit
                                        #   in Loop: Header=BB4_10 Depth=1
	xorl	%edx, %edx
	divq	%r13
	movq	(%rsi), %rax
	movq	%rax, (%rdi,%rcx,8)
	movq	(%r15,%rdx,8), %rax
	movq	%rax, (%rsi)
	movq	%rsi, (%r15,%rdx,8)
	movq	8(%r14), %rdi
.LBB4_10:                               # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_14 Depth 2
	movq	(%rdi,%rcx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB4_16
# BB#11:                                # %.lr.ph
                                        #   in Loop: Header=BB4_10 Depth=1
	movq	8(%rsi), %rdx
	movb	(%rdx), %bl
	testb	%bl, %bl
	je	.LBB4_12
# BB#13:                                # %.lr.ph.i.i.i.i.preheader
                                        #   in Loop: Header=BB4_10 Depth=1
	incq	%rdx
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB4_14:                               # %.lr.ph.i.i.i.i
                                        #   Parent Loop BB4_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leaq	(%rax,%rax,4), %rbp
	movsbq	%bl, %rax
	addq	%rbp, %rax
	movzbl	(%rdx), %ebx
	incq	%rdx
	testb	%bl, %bl
	jne	.LBB4_14
	jmp	.LBB4_15
	.p2align	4, 0x90
.LBB4_12:                               #   in Loop: Header=BB4_10 Depth=1
	xorl	%eax, %eax
	jmp	.LBB4_15
.LBB4_16:                               # %._crit_edge
                                        #   in Loop: Header=BB4_10 Depth=1
	incq	%rcx
	cmpq	%r12, %rcx
	jne	.LBB4_10
	jmp	.LBB4_17
.LBB4_8:
	movq	%rbp, %r8
.LBB4_17:                               # %._crit_edge85
	movq	%r15, 8(%r14)
	movq	%r8, 16(%r14)
	movq	%r8, 24(%r14)
	testq	%rdi, %rdi
	je	.LBB4_18
# BB#20:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZdlPv                  # TAILCALL
.LBB4_18:                               # %_ZNSt6vectorIPN9__gnu_cxx15_Hashtable_nodeISt4pairIKPKciEEESaIS8_EED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_19:                               # %.noexc.i.i
	callq	_ZSt17__throw_bad_allocv
.Lfunc_end4:
	.size	_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm, .Lfunc_end4-_ZN9__gnu_cxx9hashtableISt4pairIKPKciES3_NS_4hashIS3_EESt10_Select1stIS5_E5eqstrSaIiEE6resizeEm
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_hash.ii,@function
_GLOBAL__sub_I_hash.ii:                 # @_GLOBAL__sub_I_hash.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi56:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end5:
	.size	_GLOBAL__sub_I_hash.ii, .Lfunc_end5-_GLOBAL__sub_I_hash.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"%x"
	.size	.L.str, 3

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"%d"
	.size	.L.str.1, 3

	.type	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,@object # @_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE
	.section	.rodata._ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,"aG",@progbits,_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE,comdat
	.weak	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE
	.p2align	4
_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE:
	.quad	5                       # 0x5
	.quad	53                      # 0x35
	.quad	97                      # 0x61
	.quad	193                     # 0xc1
	.quad	389                     # 0x185
	.quad	769                     # 0x301
	.quad	1543                    # 0x607
	.quad	3079                    # 0xc07
	.quad	6151                    # 0x1807
	.quad	12289                   # 0x3001
	.quad	24593                   # 0x6011
	.quad	49157                   # 0xc005
	.quad	98317                   # 0x1800d
	.quad	196613                  # 0x30005
	.quad	393241                  # 0x60019
	.quad	786433                  # 0xc0001
	.quad	1572869                 # 0x180005
	.quad	3145739                 # 0x30000b
	.quad	6291469                 # 0x60000d
	.quad	12582917                # 0xc00005
	.quad	25165843                # 0x1800013
	.quad	50331653                # 0x3000005
	.quad	100663319               # 0x6000017
	.quad	201326611               # 0xc000013
	.quad	402653189               # 0x18000005
	.quad	805306457               # 0x30000059
	.quad	1610612741              # 0x60000005
	.quad	3221225473              # 0xc0000001
	.quad	4294967291              # 0xfffffffb
	.size	_ZN9__gnu_cxx21_Hashtable_prime_listImE16__stl_prime_listE, 232

	.type	.L.str.2,@object        # @.str.2
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.2:
	.asciz	"vector::reserve"
	.size	.L.str.2, 16

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"vector::_M_fill_insert"
	.size	.L.str.3, 23

	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_hash.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
