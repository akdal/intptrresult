	.text
	.file	"tableau.bc"
	.globl	tab_PathCreate
	.p2align	4, 0x90
	.type	tab_PathCreate,@function
tab_PathCreate:                         # @tab_PathCreate
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movl	%edi, %r15d
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rax, %rbx
	leal	8(,%r15,8), %edi
	callq	memory_Malloc
	movq	%rax, (%rbx)
	movq	%r14, (%rax)
	movl	%r15d, 12(%rbx)
	movl	$0, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end0:
	.size	tab_PathCreate, .Lfunc_end0-tab_PathCreate
	.cfi_endproc

	.globl	tab_PathDelete
	.p2align	4, 0x90
	.type	tab_PathDelete,@function
tab_PathDelete:                         # @tab_PathDelete
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 16
.Lcfi7:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	(%rbx), %rdi
	movl	12(%rbx), %eax
	leal	8(,%rax,8), %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB1_1
# BB#6:
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB1_7
.LBB1_1:
	movl	memory_ALIGN(%rip), %esi
	xorl	%edx, %edx
	movl	%ecx, %eax
	divl	%esi
	subl	%edx, %esi
	testl	%edx, %edx
	cmovel	%edx, %esi
	addl	%ecx, %esi
	movl	memory_OFFSET(%rip), %eax
	movq	%rdi, %rcx
	subq	%rax, %rcx
	movq	-16(%rcx), %rdx
	movq	-8(%rcx), %r8
	testq	%rdx, %rdx
	leaq	8(%rdx), %r9
	movl	$memory_BIGBLOCKS, %edx
	cmovneq	%r9, %rdx
	movq	%r8, (%rdx)
	movq	-8(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB1_3
# BB#2:
	negq	%rax
	movq	-16(%rdi,%rax), %rax
	movq	%rax, (%rcx)
.LBB1_3:
	addl	memory_MARKSIZE(%rip), %esi
	movq	memory_FREEDBYTES(%rip), %rax
	leaq	16(%rax,%rsi), %rax
	movq	%rax, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rax
	testq	%rax, %rax
	js	.LBB1_5
# BB#4:
	leaq	16(%rsi,%rax), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB1_5:
	addq	$-16, %rdi
	callq	free
.LBB1_7:                                # %memory_Free.exit
	movq	memory_ARRAY+128(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+128(%rip), %rax
	movq	%rbx, (%rax)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	tab_PathDelete, .Lfunc_end1-tab_PathDelete
	.cfi_endproc

	.globl	tab_PathContainsClause
	.p2align	4, 0x90
	.type	tab_PathContainsClause,@function
tab_PathContainsClause:                 # @tab_PathContainsClause
	.cfi_startproc
# BB#0:
	movslq	12(%rsi), %rcx
	xorl	%eax, %eax
	cmpl	8(%rdi), %ecx
	ja	.LBB2_5
# BB#1:
	movq	(%rdi), %rdx
	movq	(%rdx,%rcx,8), %rcx
	.p2align	4, 0x90
.LBB2_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rcx
	testq	%rcx, %rcx
	je	.LBB2_5
# BB#3:                                 #   in Loop: Header=BB2_2 Depth=1
	cmpq	%rsi, 8(%rcx)
	jne	.LBB2_2
# BB#4:
	movl	$1, %eax
.LBB2_5:                                # %.loopexit
	retq
.Lfunc_end2:
	.size	tab_PathContainsClause, .Lfunc_end2-tab_PathContainsClause
	.cfi_endproc

	.globl	tab_PathContainsClauseRobust
	.p2align	4, 0x90
	.type	tab_PathContainsClauseRobust,@function
tab_PathContainsClauseRobust:           # @tab_PathContainsClauseRobust
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi8:
	.cfi_def_cfa_offset 16
.Lcfi9:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movslq	12(%rbx), %rdx
	movslq	8(%rdi), %rcx
	xorl	%eax, %eax
	cmpl	%ecx, %edx
	jbe	.LBB3_1
.LBB3_13:                               # %tab_PathContainsClause.exit
	popq	%rbx
	retq
.LBB3_1:
	movq	(%rdi), %rsi
	movq	(%rsi,%rdx,8), %rsi
	.p2align	4, 0x90
.LBB3_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rsi), %rsi
	testq	%rsi, %rsi
	je	.LBB3_3
# BB#11:                                #   in Loop: Header=BB3_2 Depth=1
	cmpq	%rbx, 8(%rsi)
	jne	.LBB3_2
	jmp	.LBB3_10
.LBB3_3:                                # %.loopexit
	cmpl	%ecx, %edx
	ja	.LBB3_13
# BB#4:                                 # %.loopexit
	testl	%ecx, %ecx
	js	.LBB3_13
# BB#5:                                 # %.lr.ph26.i
	movq	(%rdi), %rdx
	xorl	%esi, %esi
.LBB3_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB3_7 Depth 2
	movq	(%rdx,%rsi,8), %rax
	.p2align	4, 0x90
.LBB3_7:                                #   Parent Loop BB3_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB3_12
# BB#8:                                 # %.lr.ph.i5
                                        #   in Loop: Header=BB3_7 Depth=2
	cmpq	%rbx, 8(%rax)
	jne	.LBB3_7
	jmp	.LBB3_9
	.p2align	4, 0x90
.LBB3_12:                               # %._crit_edge.i
                                        #   in Loop: Header=BB3_6 Depth=1
	xorl	%eax, %eax
	cmpq	%rcx, %rsi
	leaq	1(%rsi), %rsi
	jl	.LBB3_6
	jmp	.LBB3_13
.LBB3_9:                                # %tab_PathContainsClauseSoft.exit
	movq	stderr(%rip), %rcx
	movl	$.L.str, %edi
	movl	$57, %esi
	movl	$1, %edx
	callq	fwrite
	movq	stderr(%rip), %rdi
	movq	%rbx, %rsi
	callq	clause_PParentsFPrint
	movq	stderr(%rip), %rdi
	callq	fflush
.LBB3_10:                               # %tab_PathContainsClause.exit
	movl	$1, %eax
	popq	%rbx
	retq
.Lfunc_end3:
	.size	tab_PathContainsClauseRobust, .Lfunc_end3-tab_PathContainsClauseRobust
	.cfi_endproc

	.globl	tab_AddSplitAtCursor
	.p2align	4, 0x90
	.type	tab_AddSplitAtCursor,@function
tab_AddSplitAtCursor:                   # @tab_AddSplitAtCursor
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -32
.Lcfi14:
	.cfi_offset %r14, -24
.Lcfi15:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	movq	(%rbx), %rax
	movslq	8(%rbx), %rcx
	movq	(%rax,%rcx,8), %rbp
	movl	$56, %edi
	callq	memory_Malloc
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rax)
	movups	%xmm0, 16(%rax)
	movups	%xmm0, (%rax)
	leaq	40(%rbp), %rcx
	addq	$32, %rbp
	testl	%r14d, %r14d
	cmoveq	%rcx, %rbp
	movq	%rax, (%rbp)
	movslq	8(%rbx), %rcx
	leaq	1(%rcx), %rdx
	movl	%edx, 8(%rbx)
	movq	(%rbx), %rdx
	movq	%rax, 8(%rdx,%rcx,8)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end4:
	.size	tab_AddSplitAtCursor, .Lfunc_end4-tab_AddSplitAtCursor
	.cfi_endproc

	.globl	tab_AddClauseOnItsLevel
	.p2align	4, 0x90
	.type	tab_AddClauseOnItsLevel,@function
tab_AddClauseOnItsLevel:                # @tab_AddClauseOnItsLevel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 32
.Lcfi19:
	.cfi_offset %rbx, -32
.Lcfi20:
	.cfi_offset %r14, -24
.Lcfi21:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movslq	12(%r14), %rax
	cmpl	8(%rsi), %eax
	jg	.LBB5_2
# BB#1:
	movq	(%rsi), %rcx
	movq	(%rcx,%rax,8), %rbx
	movq	(%rbx), %r15
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r14, 8(%rax)
	movq	%r15, (%rax)
	movq	%rax, (%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.LBB5_2:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.2, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	movl	$.L.str.3, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end5:
	.size	tab_AddClauseOnItsLevel, .Lfunc_end5-tab_AddClauseOnItsLevel
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi22:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end6:
	.size	misc_Error, .Lfunc_end6-misc_Error
	.cfi_endproc

	.globl	tab_Depth
	.p2align	4, 0x90
	.type	tab_Depth,@function
tab_Depth:                              # @tab_Depth
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi25:
	.cfi_def_cfa_offset 32
.Lcfi26:
	.cfi_offset %rbx, -24
.Lcfi27:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB7_4
# BB#1:
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	jne	.LBB7_3
# BB#2:                                 # %tab_IsLeaf.exit
	cmpq	$0, 32(%rbx)
	je	.LBB7_4
.LBB7_3:                                # %tab_IsLeaf.exit.thread
	callq	tab_Depth
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	1(%rax), %ebp
	movq	32(%rbx), %rdi
	callq	tab_Depth
	movl	%ebp, %edi
	movl	%eax, %esi
	callq	misc_Max
	incl	%eax
	jmp	.LBB7_5
.LBB7_4:
	xorl	%eax, %eax
.LBB7_5:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end7:
	.size	tab_Depth, .Lfunc_end7-tab_Depth
	.cfi_endproc

	.globl	tab_IsClosed
	.p2align	4, 0x90
	.type	tab_IsClosed,@function
tab_IsClosed:                           # @tab_IsClosed
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 16
.Lcfi29:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB8_1
# BB#2:
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB8_4
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_9:                                # %clause_IsEmptyClause.exit.thread.i
                                        #   in Loop: Header=BB8_4 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB8_10
.LBB8_4:                                # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB8_9
# BB#5:                                 #   in Loop: Header=BB8_4 Depth=1
	cmpl	$0, 68(%rcx)
	jne	.LBB8_9
# BB#6:                                 #   in Loop: Header=BB8_4 Depth=1
	cmpl	$0, 72(%rcx)
	jne	.LBB8_9
# BB#7:                                 # %clause_IsEmptyClause.exit.i
                                        #   in Loop: Header=BB8_4 Depth=1
	cmpl	$0, 64(%rcx)
	jne	.LBB8_9
# BB#8:
	movl	$1, %ebx
	jmp	.LBB8_17
.LBB8_10:                               # %.loopexit
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB8_12
# BB#11:
	cmpq	$0, 32(%rbx)
	je	.LBB8_12
# BB#13:
	callq	tab_IsClosed
	testl	%eax, %eax
	je	.LBB8_14
# BB#15:
	movq	32(%rbx), %rdi
	callq	tab_IsClosed
	testl	%eax, %eax
	setne	%al
	jmp	.LBB8_16
.LBB8_1:
	xorl	%ebx, %ebx
	jmp	.LBB8_17
.LBB8_12:
	movl	48(%rbx), %esi
	xorl	%ebx, %ebx
	movl	$.L.str.4, %edi
	xorl	%eax, %eax
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	jmp	.LBB8_17
.LBB8_14:
	xorl	%eax, %eax
.LBB8_16:
	movzbl	%al, %ebx
.LBB8_17:                               # %tab_HasEmptyClause.exit
	movl	%ebx, %eax
	popq	%rbx
	retq
.Lfunc_end8:
	.size	tab_IsClosed, .Lfunc_end8-tab_IsClosed
	.cfi_endproc

	.globl	tab_Delete
	.p2align	4, 0x90
	.type	tab_Delete,@function
tab_Delete:                             # @tab_Delete
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 16
	movq	$0, (%rsp)
	movq	%rsp, %rsi
	movl	$1, %edx
	callq	tab_DeleteGen
	popq	%rax
	retq
.Lfunc_end9:
	.size	tab_Delete, .Lfunc_end9-tab_Delete
	.cfi_endproc

	.p2align	4, 0x90
	.type	tab_DeleteGen,@function
tab_DeleteGen:                          # @tab_DeleteGen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi31:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi32:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 32
.Lcfi34:
	.cfi_offset %rbx, -32
.Lcfi35:
	.cfi_offset %r14, -24
.Lcfi36:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB10_14
# BB#1:
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	tab_DeleteGen
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	tab_DeleteGen
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.LBB10_3
	.p2align	4, 0x90
.LBB10_2:                               # %.lr.ph.i18
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB10_2
.LBB10_3:                               # %list_Delete.exit19
	testl	%ebp, %ebp
	movq	(%rbx), %rax
	je	.LBB10_6
# BB#4:
	testq	%rax, %rax
	je	.LBB10_13
	.p2align	4, 0x90
.LBB10_5:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB10_5
	jmp	.LBB10_13
.LBB10_6:
	movq	(%r14), %rcx
	testq	%rax, %rax
	je	.LBB10_7
# BB#8:
	testq	%rcx, %rcx
	je	.LBB10_12
# BB#9:                                 # %.preheader.i.preheader
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB10_10:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB10_10
# BB#11:
	movq	%rcx, (%rdx)
	jmp	.LBB10_12
.LBB10_7:
	movq	%rcx, %rax
.LBB10_12:                              # %list_Nconc.exit
	movq	%rax, (%r14)
.LBB10_13:                              # %list_Delete.exit
	movq	memory_ARRAY+448(%rip), %rax
	movslq	32(%rax), %rcx
	addq	%rcx, memory_FREEDBYTES(%rip)
	movq	(%rax), %rax
	movq	%rax, (%rbx)
	movq	memory_ARRAY+448(%rip), %rax
	movq	%rbx, (%rax)
.LBB10_14:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end10:
	.size	tab_DeleteGen, .Lfunc_end10-tab_DeleteGen
	.cfi_endproc

	.globl	tab_SetSplitLevels
	.p2align	4, 0x90
	.type	tab_SetSplitLevels,@function
tab_SetSplitLevels:                     # @tab_SetSplitLevels
	.cfi_startproc
# BB#0:
	xorl	%esi, %esi
	jmp	tab_SetSplitLevelsRec   # TAILCALL
.Lfunc_end11:
	.size	tab_SetSplitLevels, .Lfunc_end11-tab_SetSplitLevels
	.cfi_endproc

	.p2align	4, 0x90
	.type	tab_SetSplitLevelsRec,@function
tab_SetSplitLevelsRec:                  # @tab_SetSplitLevelsRec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi37:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi38:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi39:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi40:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi41:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi43:
	.cfi_def_cfa_offset 80
.Lcfi44:
	.cfi_offset %rbx, -56
.Lcfi45:
	.cfi_offset %r12, -48
.Lcfi46:
	.cfi_offset %r13, -40
.Lcfi47:
	.cfi_offset %r14, -32
.Lcfi48:
	.cfi_offset %r15, -24
.Lcfi49:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB12_2
	jmp	.LBB12_47
	.p2align	4, 0x90
.LBB12_46:                              # %tailrecurse
                                        #   in Loop: Header=BB12_2 Depth=1
	movq	16(%rsp), %rbx          # 8-byte Reload
	movq	40(%rbx), %rdi
	incl	%r14d
	movl	%r14d, %esi
	callq	tab_SetSplitLevelsRec
	movq	32(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB12_47
.LBB12_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB12_7 Depth 2
                                        #       Child Loop BB12_12 Depth 3
                                        #       Child Loop BB12_15 Depth 3
                                        #       Child Loop BB12_31 Depth 3
	movq	%rbx, 16(%rsp)          # 8-byte Spill
	movq	(%rbx), %r13
	testq	%r13, %r13
	je	.LBB12_46
# BB#3:                                 # %.lr.ph
                                        #   in Loop: Header=BB12_2 Depth=1
	leal	-64(%r14), %eax
	shrl	$6, %eax
	leal	1(%rax), %r12d
	cmpl	$63, %r14d
	movl	$0, %ecx
	cmovbel	%ecx, %r12d
	ja	.LBB12_4
# BB#5:                                 # %.lr.ph
                                        #   in Loop: Header=BB12_2 Depth=1
	movb	%r14b, %cl
	jmp	.LBB12_6
	.p2align	4, 0x90
.LBB12_4:                               #   in Loop: Header=BB12_2 Depth=1
	shll	$6, %eax
	negl	%eax
	leal	-64(%r14,%rax), %ecx
.LBB12_6:                               # %.lr.ph
                                        #   in Loop: Header=BB12_2 Depth=1
	movl	$1, %ebx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shlq	%cl, %rbx
	movl	%r12d, %eax
	incl	%eax
	movl	%eax, (%rsp)            # 4-byte Spill
	leal	8(,%r12,8), %eax
	movl	%eax, 4(%rsp)           # 4-byte Spill
	movq	%rbx, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB12_7:                               #   Parent Loop BB12_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB12_12 Depth 3
                                        #       Child Loop BB12_15 Depth 3
                                        #       Child Loop BB12_31 Depth 3
	movq	8(%r13), %rax
	movl	%r14d, 12(%rax)
	movq	8(%r13), %r15
	movl	24(%r15), %ecx
	testl	%r14d, %r14d
	jle	.LBB12_35
# BB#8:                                 #   in Loop: Header=BB12_7 Depth=2
	movl	%ecx, %edx
	decl	%edx
	js	.LBB12_17
# BB#9:                                 # %.lr.ph.i
                                        #   in Loop: Header=BB12_7 Depth=2
	movslq	%edx, %rax
	andl	$3, %ecx
	je	.LBB12_10
# BB#11:                                # %.prol.preheader
                                        #   in Loop: Header=BB12_7 Depth=2
	negl	%ecx
	.p2align	4, 0x90
.LBB12_12:                              #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r15), %rsi
	movq	$0, (%rsi,%rax,8)
	decq	%rax
	incl	%ecx
	jne	.LBB12_12
# BB#13:                                #   in Loop: Header=BB12_7 Depth=2
	movl	%eax, %ecx
	cmpl	$3, %edx
	jae	.LBB12_15
	jmp	.LBB12_16
	.p2align	4, 0x90
.LBB12_35:                              #   in Loop: Header=BB12_7 Depth=2
	testl	%ecx, %ecx
	je	.LBB12_45
# BB#36:                                #   in Loop: Header=BB12_7 Depth=2
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB12_44
# BB#37:                                #   in Loop: Header=BB12_7 Depth=2
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB12_38
# BB#43:                                #   in Loop: Header=BB12_7 Depth=2
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB12_44
.LBB12_10:                              #   in Loop: Header=BB12_7 Depth=2
	movl	%edx, %ecx
	cmpl	$3, %edx
	jb	.LBB12_16
	.p2align	4, 0x90
.LBB12_15:                              #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r15), %rdx
	movq	$0, (%rdx,%rax,8)
	movq	16(%r15), %rdx
	movq	$0, -8(%rdx,%rax,8)
	movq	16(%r15), %rdx
	movq	$0, -16(%rdx,%rax,8)
	movq	16(%r15), %rdx
	movq	$0, -24(%rdx,%rax,8)
	addq	$-4, %rax
	addl	$-4, %ecx
	jns	.LBB12_15
.LBB12_16:                              # %clause_ClearSplitField.exit.loopexit
                                        #   in Loop: Header=BB12_7 Depth=2
	movq	8(%r13), %r15
	movl	24(%r15), %ecx
.LBB12_17:                              # %clause_ClearSplitField.exit
                                        #   in Loop: Header=BB12_7 Depth=2
	cmpl	%ecx, %r12d
	jb	.LBB12_34
# BB#18:                                # %clause_ClearSplitField.exit
                                        #   in Loop: Header=BB12_7 Depth=2
	cmpl	(%rsp), %ecx            # 4-byte Folded Reload
	jae	.LBB12_34
# BB#19:                                #   in Loop: Header=BB12_7 Depth=2
	movl	4(%rsp), %edi           # 4-byte Reload
	callq	memory_Malloc
	movq	%rax, %rbp
	cmpl	$0, 24(%r15)
	je	.LBB12_20
# BB#30:                                # %.lr.ph27.i.i
                                        #   in Loop: Header=BB12_7 Depth=2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB12_31:                              #   Parent Loop BB12_2 Depth=1
                                        #     Parent Loop BB12_7 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movq	16(%r15), %rax
	movl	%ecx, %edx
	movq	(%rax,%rdx,8), %rax
	movq	%rax, (%rbp,%rdx,8)
	incl	%ecx
	movl	24(%r15), %eax
	cmpl	%eax, %ecx
	jb	.LBB12_31
# BB#21:                                # %.preheader.i.i
                                        #   in Loop: Header=BB12_7 Depth=2
	cmpl	(%rsp), %eax            # 4-byte Folded Reload
	jb	.LBB12_22
	jmp	.LBB12_23
.LBB12_20:                              #   in Loop: Header=BB12_7 Depth=2
	xorl	%eax, %eax
.LBB12_22:                              # %.lr.ph.preheader.i.i
                                        #   in Loop: Header=BB12_7 Depth=2
	movl	%eax, %ecx
	leaq	(%rbp,%rcx,8), %rdi
	movl	%r12d, %ecx
	subl	%eax, %ecx
	leaq	8(,%rcx,8), %rdx
	xorl	%esi, %esi
	callq	memset
.LBB12_23:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB12_7 Depth=2
	movq	16(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB12_33
# BB#24:                                #   in Loop: Header=BB12_7 Depth=2
	movl	24(%r15), %ecx
	shll	$3, %ecx
	cmpl	$1024, %ecx             # imm = 0x400
	jae	.LBB12_25
# BB#32:                                #   in Loop: Header=BB12_7 Depth=2
	movl	%ecx, %eax
	movq	memory_ARRAY(,%rax,8), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%rdi)
	movq	memory_ARRAY(,%rax,8), %rax
	movq	%rdi, (%rax)
	jmp	.LBB12_33
.LBB12_25:                              #   in Loop: Header=BB12_7 Depth=2
	xorl	%edx, %edx
	movl	%ecx, %eax
	movl	memory_ALIGN(%rip), %esi
	divl	%esi
	movl	%esi, %eax
	subl	%edx, %eax
	testl	%edx, %edx
	cmovel	%edx, %eax
	addl	%ecx, %eax
	movl	memory_OFFSET(%rip), %ecx
	movq	%rdi, %rdx
	subq	%rcx, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %r8
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebx
	cmoveq	%rbx, %rsi
	movq	%r8, (%rsi)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB12_27
# BB#26:                                #   in Loop: Header=BB12_7 Depth=2
	negq	%rcx
	movq	-16(%rdi,%rcx), %rcx
	movq	%rcx, (%rdx)
.LBB12_27:                              #   in Loop: Header=BB12_7 Depth=2
	addl	memory_MARKSIZE(%rip), %eax
	movq	memory_FREEDBYTES(%rip), %rcx
	leaq	16(%rcx,%rax), %rcx
	movq	%rcx, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rcx
	testq	%rcx, %rcx
	movq	8(%rsp), %rbx           # 8-byte Reload
	js	.LBB12_29
# BB#28:                                #   in Loop: Header=BB12_7 Depth=2
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB12_29:                              #   in Loop: Header=BB12_7 Depth=2
	addq	$-16, %rdi
	callq	free
.LBB12_33:                              # %memory_Free.exit.i.i
                                        #   in Loop: Header=BB12_7 Depth=2
	movq	%rbp, 16(%r15)
	movl	(%rsp), %eax            # 4-byte Reload
	movl	%eax, 24(%r15)
.LBB12_34:                              # %clause_SetSplitFieldBit.exit
                                        #   in Loop: Header=BB12_7 Depth=2
	movq	16(%r15), %rax
	orq	%rbx, (%rax,%r12,8)
	jmp	.LBB12_45
.LBB12_38:                              #   in Loop: Header=BB12_7 Depth=2
	xorl	%edx, %edx
	movl	%ecx, %eax
	movl	memory_ALIGN(%rip), %esi
	divl	%esi
	movl	%esi, %eax
	subl	%edx, %eax
	testl	%edx, %edx
	cmovel	%edx, %eax
	addl	%ecx, %eax
	movl	memory_OFFSET(%rip), %ecx
	movq	%rdi, %rdx
	subq	%rcx, %rdx
	movq	-16(%rdx), %rsi
	movq	-8(%rdx), %rbp
	testq	%rsi, %rsi
	leaq	8(%rsi), %rsi
	movl	$memory_BIGBLOCKS, %ebx
	cmovneq	%rsi, %rbx
	movq	%rbp, (%rbx)
	movq	-8(%rdx), %rdx
	testq	%rdx, %rdx
	je	.LBB12_40
# BB#39:                                #   in Loop: Header=BB12_7 Depth=2
	negq	%rcx
	movq	-16(%rdi,%rcx), %rcx
	movq	%rcx, (%rdx)
.LBB12_40:                              #   in Loop: Header=BB12_7 Depth=2
	addl	memory_MARKSIZE(%rip), %eax
	movq	memory_FREEDBYTES(%rip), %rcx
	leaq	16(%rcx,%rax), %rcx
	movq	%rcx, memory_FREEDBYTES(%rip)
	movq	memory_MAXMEM(%rip), %rcx
	testq	%rcx, %rcx
	movq	8(%rsp), %rbx           # 8-byte Reload
	js	.LBB12_42
# BB#41:                                #   in Loop: Header=BB12_7 Depth=2
	leaq	16(%rax,%rcx), %rax
	movq	%rax, memory_MAXMEM(%rip)
.LBB12_42:                              #   in Loop: Header=BB12_7 Depth=2
	addq	$-16, %rdi
	callq	free
.LBB12_44:                              # %memory_Free.exit.i
                                        #   in Loop: Header=BB12_7 Depth=2
	movq	$0, 16(%r15)
	movl	$0, 24(%r15)
.LBB12_45:                              # %clause_SetSplitField.exit
                                        #   in Loop: Header=BB12_7 Depth=2
	movq	(%r13), %r13
	testq	%r13, %r13
	jne	.LBB12_7
	jmp	.LBB12_46
.LBB12_47:                              # %tailrecurse._crit_edge
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	tab_SetSplitLevelsRec, .Lfunc_end12-tab_SetSplitLevelsRec
	.cfi_endproc

	.globl	tab_PruneClosedBranches
	.p2align	4, 0x90
	.type	tab_PruneClosedBranches,@function
tab_PruneClosedBranches:                # @tab_PruneClosedBranches
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi52:
	.cfi_def_cfa_offset 32
.Lcfi53:
	.cfi_offset %rbx, -24
.Lcfi54:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB13_12
# BB#1:
	movq	(%rbx), %rax
	testq	%rax, %rax
	jne	.LBB13_3
	jmp	.LBB13_8
	.p2align	4, 0x90
.LBB13_7:                               # %clause_IsEmptyClause.exit.thread.i
                                        #   in Loop: Header=BB13_3 Depth=1
	movq	(%rax), %rax
	testq	%rax, %rax
	je	.LBB13_8
.LBB13_3:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rax), %rcx
	testq	%rcx, %rcx
	je	.LBB13_7
# BB#4:                                 #   in Loop: Header=BB13_3 Depth=1
	cmpl	$0, 68(%rcx)
	jne	.LBB13_7
# BB#5:                                 #   in Loop: Header=BB13_3 Depth=1
	cmpl	$0, 72(%rcx)
	jne	.LBB13_7
# BB#6:                                 # %clause_IsEmptyClause.exit.i
                                        #   in Loop: Header=BB13_3 Depth=1
	cmpl	$0, 64(%rcx)
	jne	.LBB13_7
# BB#9:
	movq	40(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	callq	tab_DeleteGen
	movq	32(%rbx), %rdi
	xorl	%edx, %edx
	movq	%r14, %rsi
	callq	tab_DeleteGen
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
	movq	24(%rbx), %rax
	testq	%rax, %rax
	je	.LBB13_11
	.p2align	4, 0x90
.LBB13_10:                              # %.lr.ph.i23
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rcx
	movq	memory_ARRAY+128(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+128(%rip), %rdx
	movq	%rax, (%rdx)
	testq	%rcx, %rcx
	movq	%rcx, %rax
	jne	.LBB13_10
.LBB13_11:                              # %list_Delete.exit
	movups	%xmm0, 8(%rbx)
	movq	$0, 24(%rbx)
	jmp	.LBB13_12
.LBB13_8:                               # %.loopexit
	movq	40(%rbx), %rdi
	movq	%r14, %rsi
	callq	tab_PruneClosedBranches
	movq	%rax, 40(%rbx)
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	callq	tab_PruneClosedBranches
	movq	%rax, 32(%rbx)
.LBB13_12:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	tab_PruneClosedBranches, .Lfunc_end13-tab_PruneClosedBranches
	.cfi_endproc

	.globl	tab_RemoveIncompleteSplits
	.p2align	4, 0x90
	.type	tab_RemoveIncompleteSplits,@function
tab_RemoveIncompleteSplits:             # @tab_RemoveIncompleteSplits
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -32
.Lcfi59:
	.cfi_offset %r14, -24
.Lcfi60:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB14_14
# BB#1:
	movq	32(%rbx), %rax
	movq	40(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB14_2
# BB#4:                                 # %tab_IsLeaf.exit.thread
	testq	%rax, %rax
	je	.LBB14_6
# BB#5:
	movq	%r14, %rsi
	callq	tab_RemoveIncompleteSplits
	movq	%rax, 40(%rbx)
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	callq	tab_RemoveIncompleteSplits
	movq	%rax, 32(%rbx)
	jmp	.LBB14_14
.LBB14_2:                               # %tab_IsLeaf.exit
	testq	%rax, %rax
	je	.LBB14_14
# BB#3:
	leaq	32(%rbx), %r15
	movq	32(%rbx), %rdi
	jmp	.LBB14_7
.LBB14_6:                               # %._crit_edge
	leaq	32(%rbx), %r15
.LBB14_7:
	movq	%r14, %rsi
	callq	tab_RemoveIncompleteSplits
	movq	32(%rax), %rcx
	movq	%rcx, (%r15)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rbx)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rbx)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rbx)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rbx)
	movq	(%rax), %rcx
	movq	memory_ARRAY+448(%rip), %rdx
	movslq	32(%rdx), %rsi
	addq	%rsi, memory_FREEDBYTES(%rip)
	movq	(%rdx), %rdx
	movq	%rdx, (%rax)
	movq	memory_ARRAY+448(%rip), %rdx
	movq	%rax, (%rdx)
	movq	(%r14), %rax
	testq	%rcx, %rcx
	je	.LBB14_8
# BB#9:
	testq	%rax, %rax
	je	.LBB14_13
# BB#10:                                # %.preheader.i.preheader
	movq	%rcx, %rsi
	.p2align	4, 0x90
.LBB14_11:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB14_11
# BB#12:
	movq	%rax, (%rdx)
	jmp	.LBB14_13
.LBB14_8:
	movq	%rax, %rcx
.LBB14_13:                              # %list_Nconc.exit
	movq	%rcx, (%r14)
.LBB14_14:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	tab_RemoveIncompleteSplits, .Lfunc_end14-tab_RemoveIncompleteSplits
	.cfi_endproc

	.globl	tab_CheckEmpties
	.p2align	4, 0x90
	.type	tab_CheckEmpties,@function
tab_CheckEmpties:                       # @tab_CheckEmpties
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi61:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi62:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi63:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi64:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi65:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi66:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi67:
	.cfi_def_cfa_offset 64
.Lcfi68:
	.cfi_offset %rbx, -56
.Lcfi69:
	.cfi_offset %r12, -48
.Lcfi70:
	.cfi_offset %r13, -40
.Lcfi71:
	.cfi_offset %r14, -32
.Lcfi72:
	.cfi_offset %r15, -24
.Lcfi73:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	testq	%r14, %r14
	jne	.LBB15_2
	jmp	.LBB15_24
.LBB15_19:                              #   in Loop: Header=BB15_2 Depth=1
	xorl	%r12d, %r12d
	jmp	.LBB15_15
	.p2align	4, 0x90
.LBB15_2:                               # %.lr.ph42
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB15_4 Depth 2
                                        #     Child Loop BB15_23 Depth 2
	movq	(%r14), %rbp
	testq	%rbp, %rbp
	je	.LBB15_14
# BB#3:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB15_2 Depth=1
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB15_4:                               # %.lr.ph
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rbp), %rbx
	testq	%rbx, %rbx
	je	.LBB15_9
# BB#5:                                 #   in Loop: Header=BB15_4 Depth=2
	cmpl	$0, 68(%rbx)
	jne	.LBB15_9
# BB#6:                                 #   in Loop: Header=BB15_4 Depth=2
	cmpl	$0, 72(%rbx)
	jne	.LBB15_9
# BB#7:                                 # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB15_4 Depth=2
	cmpl	$0, 64(%rbx)
	jne	.LBB15_9
# BB#8:                                 #   in Loop: Header=BB15_4 Depth=2
	movl	$16, %edi
	callq	memory_Malloc
	movq	%rbx, 8(%rax)
	movq	%r13, (%rax)
	movq	%rax, %r13
	.p2align	4, 0x90
.LBB15_9:                               # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB15_4 Depth=2
	movq	(%rbp), %rbp
	testq	%rbp, %rbp
	jne	.LBB15_4
# BB#10:                                # %._crit_edge
                                        #   in Loop: Header=BB15_2 Depth=1
	testq	%r13, %r13
	sete	%r12b
	je	.LBB15_14
# BB#11:                                #   in Loop: Header=BB15_2 Depth=1
	leaq	40(%r14), %r15
	cmpq	$0, 40(%r14)
	jne	.LBB15_13
# BB#12:                                # %tab_IsLeaf.exit32
                                        #   in Loop: Header=BB15_2 Depth=1
	cmpq	$0, 32(%r14)
	je	.LBB15_19
.LBB15_13:                              # %tab_IsLeaf.exit32.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	movl	$.L.str.5, %edi
	callq	puts
	movl	$1, %ebp
	cmpq	$0, (%r15)
	jne	.LBB15_20
	jmp	.LBB15_16
	.p2align	4, 0x90
.LBB15_14:                              # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB15_2 Depth=1
	leaq	40(%r14), %r15
	xorl	%r13d, %r13d
	movb	$1, %r12b
.LBB15_15:                              # %._crit_edge._crit_edge
                                        #   in Loop: Header=BB15_2 Depth=1
	xorl	%ebp, %ebp
	cmpq	$0, (%r15)
	jne	.LBB15_20
.LBB15_16:                              # %tab_IsLeaf.exit
                                        #   in Loop: Header=BB15_2 Depth=1
	cmpq	$0, 32(%r14)
	jne	.LBB15_20
# BB#17:                                #   in Loop: Header=BB15_2 Depth=1
	movq	%r13, %rdi
	callq	list_Length
	cmpl	$2, %eax
	jb	.LBB15_20
# BB#18:                                # %.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	movl	$.L.str.6, %edi
	callq	puts
	jmp	.LBB15_21
	.p2align	4, 0x90
.LBB15_20:                              # %tab_IsLeaf.exit.thread
                                        #   in Loop: Header=BB15_2 Depth=1
	testl	%ebp, %ebp
	je	.LBB15_22
.LBB15_21:                              #   in Loop: Header=BB15_2 Depth=1
	movl	$.L.str.7, %edi
	callq	puts
	movq	(%r14), %rdi
	callq	clause_PParentsListPrint
.LBB15_22:                              #   in Loop: Header=BB15_2 Depth=1
	testb	%r12b, %r12b
	jne	.LBB15_1
	.p2align	4, 0x90
.LBB15_23:                              # %.lr.ph.i
                                        #   Parent Loop BB15_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	(%r13), %rax
	movq	memory_ARRAY+128(%rip), %rcx
	movslq	32(%rcx), %rdx
	addq	%rdx, memory_FREEDBYTES(%rip)
	movq	(%rcx), %rcx
	movq	%rcx, (%r13)
	movq	memory_ARRAY+128(%rip), %rcx
	movq	%r13, (%rcx)
	testq	%rax, %rax
	movq	%rax, %r13
	jne	.LBB15_23
.LBB15_1:                               # %list_Delete.exit
                                        #   in Loop: Header=BB15_2 Depth=1
	movq	32(%r14), %rdi
	callq	tab_CheckEmpties
	movq	(%r15), %r14
	testq	%r14, %r14
	jne	.LBB15_2
.LBB15_24:                              # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end15:
	.size	tab_CheckEmpties, .Lfunc_end15-tab_CheckEmpties
	.cfi_endproc

	.globl	tab_GetAllEmptyClauses
	.p2align	4, 0x90
	.type	tab_GetAllEmptyClauses,@function
tab_GetAllEmptyClauses:                 # @tab_GetAllEmptyClauses
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end16:
	.size	tab_GetAllEmptyClauses, .Lfunc_end16-tab_GetAllEmptyClauses
	.cfi_endproc

	.globl	tab_GetEarliestEmptyClauses
	.p2align	4, 0x90
	.type	tab_GetEarliestEmptyClauses,@function
tab_GetEarliestEmptyClauses:            # @tab_GetEarliestEmptyClauses
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi74:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi75:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 48
.Lcfi79:
	.cfi_offset %rbx, -40
.Lcfi80:
	.cfi_offset %r12, -32
.Lcfi81:
	.cfi_offset %r14, -24
.Lcfi82:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB17_2
	jmp	.LBB17_17
.LBB17_14:                              # %._crit_edge
                                        #   in Loop: Header=BB17_2 Depth=1
	testq	%rcx, %rcx
	je	.LBB17_16
# BB#15:                                #   in Loop: Header=BB17_2 Depth=1
	movq	(%r14), %r12
	movl	$16, %edi
	callq	memory_Malloc
	movq	%r15, 8(%rax)
	movq	%r12, (%rax)
	movq	%rax, (%r14)
	.p2align	4, 0x90
.LBB17_16:                              # %tab_IsLeaf.exit.thread
                                        #   in Loop: Header=BB17_2 Depth=1
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	callq	tab_GetEarliestEmptyClauses
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB17_17
.LBB17_2:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB17_6 Depth 2
	cmpq	$0, 40(%rbx)
	jne	.LBB17_16
# BB#3:                                 # %tab_IsLeaf.exit
                                        #   in Loop: Header=BB17_2 Depth=1
	cmpq	$0, 32(%rbx)
	jne	.LBB17_16
# BB#4:                                 #   in Loop: Header=BB17_2 Depth=1
	movq	(%rbx), %rax
	testq	%rax, %rax
	je	.LBB17_16
# BB#5:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB17_2 Depth=1
	xorl	%ecx, %ecx
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB17_6:                               # %.lr.ph
                                        #   Parent Loop BB17_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	8(%rax), %rdx
	testq	%rdx, %rdx
	je	.LBB17_13
# BB#7:                                 #   in Loop: Header=BB17_6 Depth=2
	cmpl	$0, 68(%rdx)
	jne	.LBB17_13
# BB#8:                                 #   in Loop: Header=BB17_6 Depth=2
	cmpl	$0, 72(%rdx)
	jne	.LBB17_13
# BB#9:                                 # %clause_IsEmptyClause.exit
                                        #   in Loop: Header=BB17_6 Depth=2
	cmpl	$0, 64(%rdx)
	jne	.LBB17_13
# BB#10:                                #   in Loop: Header=BB17_6 Depth=2
	testq	%rcx, %rcx
	je	.LBB17_11
# BB#12:                                #   in Loop: Header=BB17_6 Depth=2
	movl	(%rcx), %esi
	cmpl	(%rdx), %esi
	cmovgq	%rdx, %r15
	cmovgq	%rdx, %rcx
	jmp	.LBB17_13
.LBB17_11:                              #   in Loop: Header=BB17_6 Depth=2
	movq	%rdx, %r15
	movq	%rdx, %rcx
	.p2align	4, 0x90
.LBB17_13:                              # %clause_IsEmptyClause.exit.thread
                                        #   in Loop: Header=BB17_6 Depth=2
	movq	(%rax), %rax
	testq	%rax, %rax
	jne	.LBB17_6
	jmp	.LBB17_14
.LBB17_17:                              # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	tab_GetEarliestEmptyClauses, .Lfunc_end17-tab_GetEarliestEmptyClauses
	.cfi_endproc

	.globl	tab_ToClauseList
	.p2align	4, 0x90
	.type	tab_ToClauseList,@function
tab_ToClauseList:                       # @tab_ToClauseList
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi83:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi84:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi85:
	.cfi_def_cfa_offset 32
.Lcfi86:
	.cfi_offset %rbx, -24
.Lcfi87:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	jne	.LBB18_2
	jmp	.LBB18_9
	.p2align	4, 0x90
.LBB18_8:                               # %list_Nconc.exit
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rax, (%r14)
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	callq	tab_ToClauseList
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB18_9
.LBB18_2:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB18_6 Depth 2
	movq	(%rbx), %rdi
	callq	list_Copy
	movq	(%r14), %rcx
	testq	%rax, %rax
	je	.LBB18_3
# BB#4:                                 #   in Loop: Header=BB18_2 Depth=1
	testq	%rcx, %rcx
	je	.LBB18_8
# BB#5:                                 # %.preheader.i.preheader
                                        #   in Loop: Header=BB18_2 Depth=1
	movq	%rax, %rsi
	.p2align	4, 0x90
.LBB18_6:                               # %.preheader.i
                                        #   Parent Loop BB18_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rsi, %rdx
	movq	(%rdx), %rsi
	testq	%rsi, %rsi
	jne	.LBB18_6
# BB#7:                                 #   in Loop: Header=BB18_2 Depth=1
	movq	%rcx, (%rdx)
	jmp	.LBB18_8
	.p2align	4, 0x90
.LBB18_3:                               #   in Loop: Header=BB18_2 Depth=1
	movq	%rcx, %rax
	jmp	.LBB18_8
.LBB18_9:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end18:
	.size	tab_ToClauseList, .Lfunc_end18-tab_ToClauseList
	.cfi_endproc

	.globl	tab_LabelNodes
	.p2align	4, 0x90
	.type	tab_LabelNodes,@function
tab_LabelNodes:                         # @tab_LabelNodes
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi88:
	.cfi_def_cfa_offset 16
	movl	$0, 4(%rsp)
	leaq	4(%rsp), %rsi
	callq	tab_LabelNodesRec
	popq	%rax
	retq
.Lfunc_end19:
	.size	tab_LabelNodes, .Lfunc_end19-tab_LabelNodes
	.cfi_endproc

	.p2align	4, 0x90
	.type	tab_LabelNodesRec,@function
tab_LabelNodesRec:                      # @tab_LabelNodesRec
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi91:
	.cfi_def_cfa_offset 32
.Lcfi92:
	.cfi_offset %rbx, -24
.Lcfi93:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	testq	%rbx, %rbx
	je	.LBB20_3
	.p2align	4, 0x90
.LBB20_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r14), %eax
	movl	%eax, 48(%rbx)
	incl	(%r14)
	movq	32(%rbx), %rdi
	movq	%r14, %rsi
	callq	tab_LabelNodesRec
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB20_1
.LBB20_3:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end20:
	.size	tab_LabelNodesRec, .Lfunc_end20-tab_LabelNodesRec
	.cfi_endproc

	.globl	tab_PrintCgFormat
	.p2align	4, 0x90
	.type	tab_PrintCgFormat,@function
tab_PrintCgFormat:                      # @tab_PrintCgFormat
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi96:
	.cfi_def_cfa_offset 32
.Lcfi97:
	.cfi_offset %rbx, -24
.Lcfi98:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movq	stdout(%rip), %rbx
	movl	$.L.str.10, %edi
	movl	$35, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	tab_FPrintNodesCgFormat
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	tab_FPrintEdgesCgFormat
	movl	$.L.str.11, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	fwrite                  # TAILCALL
.Lfunc_end21:
	.size	tab_PrintCgFormat, .Lfunc_end21-tab_PrintCgFormat
	.cfi_endproc

	.globl	tab_WriteTableau
	.p2align	4, 0x90
	.type	tab_WriteTableau,@function
tab_WriteTableau:                       # @tab_WriteTableau
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 48
.Lcfi104:
	.cfi_offset %rbx, -40
.Lcfi105:
	.cfi_offset %r14, -32
.Lcfi106:
	.cfi_offset %r15, -24
.Lcfi107:
	.cfi_offset %rbp, -16
	movl	%edx, %ebp
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpl	$2, %ebp
	jae	.LBB22_7
# BB#1:
	movl	$.L.str.9, %esi
	movq	%r14, %rdi
	callq	misc_OpenFile
	movq	%rax, %rbx
	cmpl	$1, %ebp
	je	.LBB22_4
# BB#2:
	testl	%ebp, %ebp
	jne	.LBB22_6
# BB#3:
	movl	$.L.str.28, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	tab_FPrintDaVinciFormatRec
	movl	$.L.str.29, %edi
	jmp	.LBB22_5
.LBB22_4:
	movl	$.L.str.10, %edi
	movl	$35, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	tab_FPrintNodesCgFormat
	movq	%rbx, %rdi
	movq	%r15, %rsi
	callq	tab_FPrintEdgesCgFormat
	movl	$.L.str.11, %edi
.LBB22_5:
	movl	$2, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
.LBB22_6:
	movq	%rbx, %rdi
	movq	%r14, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	misc_CloseFile          # TAILCALL
.LBB22_7:
	movq	stdout(%rip), %rdi
	callq	fflush
	movl	$.L.str.8, %edi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end22:
	.size	tab_WriteTableau, .Lfunc_end22-tab_WriteTableau
	.cfi_endproc

	.p2align	4, 0x90
	.type	tab_FPrintNodesCgFormat,@function
tab_FPrintNodesCgFormat:                # @tab_FPrintNodesCgFormat
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi108:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi109:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi110:
	.cfi_def_cfa_offset 32
.Lcfi111:
	.cfi_offset %rbx, -24
.Lcfi112:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	je	.LBB23_3
	.p2align	4, 0x90
.LBB23_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.12, %edi
	movl	$17, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	tab_FPrintNodeLabel
	movl	$10, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movl	48(%rbx), %edx
	movl	$.L.str.13, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	fprintf
	movl	$.L.str.14, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movq	32(%rbx), %rsi
	movq	%r14, %rdi
	callq	tab_FPrintNodesCgFormat
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB23_1
.LBB23_3:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end23:
	.size	tab_FPrintNodesCgFormat, .Lfunc_end23-tab_FPrintNodesCgFormat
	.cfi_endproc

	.p2align	4, 0x90
	.type	tab_FPrintEdgesCgFormat,@function
tab_FPrintEdgesCgFormat:                # @tab_FPrintEdgesCgFormat
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi113:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi114:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi115:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi116:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 48
.Lcfi118:
	.cfi_offset %rbx, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	testq	%rbx, %rbx
	jne	.LBB24_2
	jmp	.LBB24_7
	.p2align	4, 0x90
.LBB24_6:                               # %tailrecurse
                                        #   in Loop: Header=BB24_2 Depth=1
	movq	32(%rbx), %rsi
	movq	%r14, %rdi
	callq	tab_FPrintEdgesCgFormat
	movq	40(%rbx), %rbx
	testq	%rbx, %rbx
	je	.LBB24_7
.LBB24_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	32(%rbx), %rax
	testq	%rax, %rax
	je	.LBB24_4
# BB#3:                                 #   in Loop: Header=BB24_2 Depth=1
	movl	48(%rbx), %ebp
	movl	48(%rax), %r15d
	movl	$.L.str.23, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movl	$.L.str.26, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$48, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movl	$.L.str.27, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
.LBB24_4:                               #   in Loop: Header=BB24_2 Depth=1
	movq	40(%rbx), %rax
	testq	%rax, %rax
	je	.LBB24_6
# BB#5:                                 #   in Loop: Header=BB24_2 Depth=1
	movl	48(%rbx), %ebp
	movl	48(%rax), %r15d
	movl	$.L.str.23, %edi
	movl	$8, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$.L.str.24, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%ebp, %edx
	callq	fprintf
	movl	$.L.str.25, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movl	%r15d, %edx
	callq	fprintf
	movl	$.L.str.26, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$49, %edi
	movq	%r14, %rsi
	callq	_IO_putc
	movl	$.L.str.27, %edi
	movl	$5, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	jmp	.LBB24_6
.LBB24_7:                               # %tailrecurse._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end24:
	.size	tab_FPrintEdgesCgFormat, .Lfunc_end24-tab_FPrintEdgesCgFormat
	.cfi_endproc

	.p2align	4, 0x90
	.type	tab_FPrintNodeLabel,@function
tab_FPrintNodeLabel:                    # @tab_FPrintNodeLabel
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi124:
	.cfi_def_cfa_offset 32
.Lcfi125:
	.cfi_offset %rbx, -32
.Lcfi126:
	.cfi_offset %r14, -24
.Lcfi127:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movl	48(%r14), %edx
	movl	$.L.str.15, %esi
	xorl	%eax, %eax
	callq	fprintf
	movl	$.L.str.16, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	8(%r14), %rsi
	movq	%r15, %rdi
	callq	clause_PParentsFPrint
	movl	$.L.str.17, %edi
	movl	$16, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	16(%r14), %rsi
	movq	%r15, %rdi
	callq	clause_PParentsFPrint
	movl	$.L.str.18, %edi
	movl	$16, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	24(%r14), %rax
	testq	%rax, %rax
	je	.LBB25_1
# BB#2:
	movq	8(%rax), %rsi
	movq	%r15, %rdi
	callq	clause_PParentsFPrint
	movl	$.L.str.20, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	24(%r14), %rax
	movq	(%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB25_5
	.p2align	4, 0x90
.LBB25_3:                               # %.lr.ph47
                                        # =>This Inner Loop Header: Depth=1
	movl	$.L.str.21, %edi
	movl	$14, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	clause_PParentsFPrint
	movl	$.L.str.20, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB25_3
	jmp	.LBB25_5
.LBB25_1:
	movl	$.L.str.19, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB25_5:                               # %.loopexit42
	cmpl	$0, pcheck_ClauseCg(%rip)
	je	.LBB25_9
# BB#6:
	movq	(%r14), %rbx
	testq	%rbx, %rbx
	je	.LBB25_7
	.p2align	4, 0x90
.LBB25_8:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbx), %rsi
	movq	%r15, %rdi
	callq	clause_PParentsFPrint
	movl	$.L.str.20, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
	movq	(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB25_8
	jmp	.LBB25_9
.LBB25_7:
	movl	$.L.str.22, %edi
	movl	$2, %esi
	movl	$1, %edx
	movq	%r15, %rcx
	callq	fwrite
.LBB25_9:                               # %.loopexit
	movl	$34, %edi
	movq	%r15, %rsi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_IO_putc                # TAILCALL
.Lfunc_end25:
	.size	tab_FPrintNodeLabel, .Lfunc_end25-tab_FPrintNodeLabel
	.cfi_endproc

	.p2align	4, 0x90
	.type	tab_FPrintDaVinciFormatRec,@function
tab_FPrintDaVinciFormatRec:             # @tab_FPrintDaVinciFormatRec
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi128:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi129:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi130:
	.cfi_def_cfa_offset 32
.Lcfi131:
	.cfi_offset %rbx, -32
.Lcfi132:
	.cfi_offset %r14, -24
.Lcfi133:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	jmp	.LBB26_1
	.p2align	4, 0x90
.LBB26_10:                              #   in Loop: Header=BB26_1 Depth=1
	movl	$44, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	40(%r14), %r14
.LBB26_1:                               # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	movl	48(%r14), %edx
	movl	$.L.str.30, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	fprintf
	movl	$.L.str.31, %edi
	movl	$18, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	tab_FPrintNodeLabel
	movl	$.L.str.32, %edi
	movl	$4, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$91, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	32(%r14), %rax
	testq	%rax, %rax
	je	.LBB26_3
# BB#2:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	48(%r14), %edx
	movl	48(%rax), %ebp
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	fprintf
	movl	$.L.str.35, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	fprintf
.LBB26_3:                               #   in Loop: Header=BB26_1 Depth=1
	movq	40(%r14), %rax
	testq	%rax, %rax
	je	.LBB26_7
# BB#4:                                 #   in Loop: Header=BB26_1 Depth=1
	cmpq	$0, 32(%r14)
	je	.LBB26_6
# BB#5:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	$44, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	40(%r14), %rax
.LBB26_6:                               #   in Loop: Header=BB26_1 Depth=1
	movl	48(%r14), %edx
	movl	48(%rax), %ebp
	movl	$.L.str.34, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %ecx
	callq	fprintf
	movl	$.L.str.35, %edi
	movl	$9, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	movl	$.L.str.36, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	callq	fprintf
.LBB26_7:                               #   in Loop: Header=BB26_1 Depth=1
	movl	$.L.str.33, %edi
	movl	$3, %esi
	movl	$1, %edx
	movq	%rbx, %rcx
	callq	fwrite
	cmpq	$0, 32(%r14)
	je	.LBB26_9
# BB#8:                                 #   in Loop: Header=BB26_1 Depth=1
	movl	$44, %edi
	movq	%rbx, %rsi
	callq	_IO_putc
	movq	32(%r14), %rsi
	movq	%rbx, %rdi
	callq	tab_FPrintDaVinciFormatRec
.LBB26_9:                               #   in Loop: Header=BB26_1 Depth=1
	cmpq	$0, 40(%r14)
	jne	.LBB26_10
# BB#11:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end26:
	.size	tab_FPrintDaVinciFormatRec, .Lfunc_end26-tab_FPrintDaVinciFormatRec
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"NOTE: Clause is found on path, but not indexed by level.\n"
	.size	.L.str, 58

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"\nError: Split level of some clause "
	.size	.L.str.1, 36

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"\nis higher than existing level."
	.size	.L.str.2, 32

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"\nThis may be a bug in the proof file."
	.size	.L.str.3, 38

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"\nopen node label: %d"
	.size	.L.str.4, 21

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"\nNOTE: non-leaf node contains empty clauses."
	.size	.L.str.5, 45

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"\nNOTE: Leaf contains more than one empty clauses."
	.size	.L.str.6, 50

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Clauses:"
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"\nError: unknown output format for tableau.\n"
	.size	.L.str.8, 44

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"w"
	.size	.L.str.9, 2

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"graph: \n{\ndisplay_edge_labels: yes\n"
	.size	.L.str.10, 36

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"}\n"
	.size	.L.str.11, 3

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"\nnode: {\n\nlabel: "
	.size	.L.str.12, 18

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"title: \"%d\"\n"
	.size	.L.str.13, 13

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"  }\n"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"\"label: %d\\n"
	.size	.L.str.15, 13

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"SplitClause : "
	.size	.L.str.16, 15

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"\\nLeft Clause : "
	.size	.L.str.17, 17

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"\\nRightClauses: "
	.size	.L.str.18, 17

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"[]\\n"
	.size	.L.str.19, 5

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"\\n"
	.size	.L.str.20, 3

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"              "
	.size	.L.str.21, 15

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"[]"
	.size	.L.str.22, 3

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"\nedge: {"
	.size	.L.str.23, 9

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"\nsourcename: \"%d\""
	.size	.L.str.24, 18

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"\ntargetname: \"%d\"\n"
	.size	.L.str.25, 19

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"\nlabel: \""
	.size	.L.str.26, 10

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"\"  }\n"
	.size	.L.str.27, 6

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"[\n"
	.size	.L.str.28, 3

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"]\n"
	.size	.L.str.29, 3

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"l(\"%d\","
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"n(\"\", [a(\"OBJECT\","
	.size	.L.str.31, 19

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	")],\n"
	.size	.L.str.32, 5

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"]))"
	.size	.L.str.33, 4

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"l(\"%d->%d\","
	.size	.L.str.34, 12

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"e(\"\",[],\n"
	.size	.L.str.35, 10

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"r(\"%d\")))\n"
	.size	.L.str.36, 11


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
