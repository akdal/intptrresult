	.text
	.file	"btConeTwistConstraint.bc"
	.globl	_ZN21btConeTwistConstraintC2Ev
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraintC2Ev,@function
_ZN21btConeTwistConstraintC2Ev:         # @_ZN21btConeTwistConstraintC2Ev
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$5, %esi
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintType
	movq	$_ZTV21btConeTwistConstraint+16, (%rbx)
	movb	$0, 575(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	_ZN21btConeTwistConstraintC2Ev, .Lfunc_end0-_ZN21btConeTwistConstraintC2Ev
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1065353216              # float 1
	.long	1050253722              # float 0.300000012
	.long	1065353216              # float 1
	.long	1008981770              # float 0.00999999977
.LCPI1_1:
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.long	1028443341              # float 0.0500000007
	.text
	.globl	_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_,@function
_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_: # @_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 32
.Lcfi5:
	.cfi_offset %rbx, -32
.Lcfi6:
	.cfi_offset %r14, -24
.Lcfi7:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %rax
	movq	%rsi, %rcx
	movq	%rdi, %rbx
	movl	$5, %esi
	movq	%rcx, %rdx
	movq	%rax, %rcx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBodyS2_
	movq	$_ZTV21btConeTwistConstraint+16, (%rbx)
	movups	(%r15), %xmm0
	movups	%xmm0, 348(%rbx)
	movups	16(%r15), %xmm0
	movups	%xmm0, 364(%rbx)
	movups	32(%r15), %xmm0
	movups	%xmm0, 380(%rbx)
	movups	48(%r15), %xmm0
	movups	%xmm0, 396(%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, 412(%rbx)
	movups	16(%r14), %xmm0
	movups	%xmm0, 428(%rbx)
	movups	32(%r14), %xmm0
	movups	%xmm0, 444(%rbx)
	movups	48(%r14), %xmm0
	movups	%xmm0, 460(%rbx)
	movb	$0, 600(%rbx)
	movl	$0, 572(%rbx)
	movl	$-1082130432, 620(%rbx) # imm = 0xBF800000
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [1.000000e+00,3.000000e-01,1.000000e+00,1.000000e-02]
	movups	%xmm0, 476(%rbx)
	movaps	.LCPI1_1(%rip), %xmm0   # xmm0 = [1.000000e+18,1.000000e+18,1.000000e+18,5.000000e-02]
	movups	%xmm0, 492(%rbx)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end1:
	.size	_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_, .Lfunc_end1-_ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.long	1065353216              # float 1
	.long	1050253722              # float 0.300000012
	.long	1065353216              # float 1
	.long	1008981770              # float 0.00999999977
.LCPI2_1:
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.long	1028443341              # float 0.0500000007
	.text
	.globl	_ZN21btConeTwistConstraint4initEv
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint4initEv,@function
_ZN21btConeTwistConstraint4initEv:      # @_ZN21btConeTwistConstraint4initEv
	.cfi_startproc
# BB#0:
	movb	$0, 572(%rdi)
	movb	$0, 573(%rdi)
	movb	$0, 574(%rdi)
	movb	$0, 600(%rdi)
	movl	$-1082130432, 620(%rdi) # imm = 0xBF800000
	movaps	.LCPI2_0(%rip), %xmm0   # xmm0 = [1.000000e+00,3.000000e-01,1.000000e+00,1.000000e-02]
	movups	%xmm0, 476(%rdi)
	movaps	.LCPI2_1(%rip), %xmm0   # xmm0 = [1.000000e+18,1.000000e+18,1.000000e+18,5.000000e-02]
	movups	%xmm0, 492(%rdi)
	retq
.Lfunc_end2:
	.size	_ZN21btConeTwistConstraint4initEv, .Lfunc_end2-_ZN21btConeTwistConstraint4initEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI3_0:
	.long	1065353216              # float 1
	.long	1050253722              # float 0.300000012
	.long	1065353216              # float 1
	.long	1008981770              # float 0.00999999977
.LCPI3_1:
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.long	1566444395              # float 9.99999984E+17
	.long	1028443341              # float 0.0500000007
	.text
	.globl	_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform,@function
_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform: # @_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 32
.Lcfi11:
	.cfi_offset %rbx, -24
.Lcfi12:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rax
	movq	%rdi, %rbx
	movl	$5, %esi
	movq	%rax, %rdx
	callq	_ZN17btTypedConstraintC2E21btTypedConstraintTypeR11btRigidBody
	movq	$_ZTV21btConeTwistConstraint+16, (%rbx)
	movups	(%r14), %xmm0
	movups	%xmm0, 348(%rbx)
	movups	16(%r14), %xmm0
	movups	%xmm0, 364(%rbx)
	movups	32(%r14), %xmm0
	movups	%xmm0, 380(%rbx)
	movups	48(%r14), %xmm0
	movups	%xmm0, 396(%rbx)
	movb	$0, 575(%rbx)
	movups	348(%rbx), %xmm0
	movups	%xmm0, 412(%rbx)
	movups	364(%rbx), %xmm0
	movups	%xmm0, 428(%rbx)
	movups	380(%rbx), %xmm0
	movups	%xmm0, 444(%rbx)
	movups	396(%rbx), %xmm0
	movups	%xmm0, 460(%rbx)
	movb	$0, 572(%rbx)
	movb	$0, 573(%rbx)
	movb	$0, 574(%rbx)
	movb	$0, 600(%rbx)
	movl	$-1082130432, 620(%rbx) # imm = 0xBF800000
	movaps	.LCPI3_0(%rip), %xmm0   # xmm0 = [1.000000e+00,3.000000e-01,1.000000e+00,1.000000e-02]
	movups	%xmm0, 476(%rbx)
	movaps	.LCPI3_1(%rip), %xmm0   # xmm0 = [1.000000e+18,1.000000e+18,1.000000e+18,5.000000e-02]
	movups	%xmm0, 492(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end3:
	.size	_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform, .Lfunc_end3-_ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform
	.cfi_endproc

	.globl	_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E: # @_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi15:
	.cfi_def_cfa_offset 32
.Lcfi16:
	.cfi_offset %rbx, -24
.Lcfi17:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 575(%rbx)
	je	.LBB4_2
# BB#1:
	movq	$0, (%r14)
	jmp	.LBB4_8
.LBB4_2:
	movabsq	$12884901891, %rax      # imm = 0x300000003
	movq	%rax, (%r14)
	movq	24(%rbx), %rcx
	movq	32(%rbx), %r8
	leaq	8(%rcx), %rsi
	addq	$280, %rcx              # imm = 0x118
	leaq	8(%r8), %rdx
	addq	$280, %r8               # imm = 0x118
	movq	%rbx, %rdi
	callq	_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_
	cmpb	$0, 574(%rbx)
	je	.LBB4_6
# BB#3:
	movl	(%r14), %eax
	leal	1(%rax), %ecx
	movl	%ecx, (%r14)
	movl	4(%r14), %ecx
	leal	-1(%rcx), %edx
	movl	%edx, 4(%r14)
	movss	504(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	492(%rbx), %xmm0
	jbe	.LBB4_6
# BB#4:
	ucomiss	496(%rbx), %xmm0
	jbe	.LBB4_6
# BB#5:
	addl	$2, %eax
	movl	%eax, (%r14)
	addl	$-2, %ecx
	movl	%ecx, 4(%r14)
.LBB4_6:
	cmpb	$0, 573(%rbx)
	je	.LBB4_8
# BB#7:
	incl	(%r14)
	decl	4(%r14)
.LBB4_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end4:
	.size	_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end4-_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI5_0:
	.long	1073741824              # float 2
.LCPI5_1:
	.long	1065353216              # float 1
.LCPI5_4:
	.long	872415232               # float 1.1920929E-7
.LCPI5_5:
	.long	2147483648              # float -0
.LCPI5_6:
	.long	3212836862              # float -0.99999988
.LCPI5_7:
	.long	1056964608              # float 0.5
.LCPI5_8:
	.long	1060439283              # float 0.707106769
.LCPI5_9:
	.long	1065353214              # float 0.99999988
.LCPI5_10:
	.long	1078530011              # float 3.14159274
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_2:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI5_3:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_,@function
_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_: # @_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi18:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi19:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi20:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi21:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi22:
	.cfi_def_cfa_offset 48
	subq	$608, %rsp              # imm = 0x260
.Lcfi23:
	.cfi_def_cfa_offset 656
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r13, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rsi, %r13
	movq	%rdi, %rbx
	movl	$0, 552(%rbx)
	movl	$0, 548(%rbx)
	movw	$0, 573(%rbx)
	cmpb	$0, 600(%rbx)
	je	.LBB5_2
# BB#1:
	cmpb	$0, 575(%rbx)
	je	.LBB5_12
.LBB5_2:
	leaq	400(%rsp), %rsi
	movq	%r13, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	400(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movsd	408(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	leaq	348(%rbx), %rdi
	leaq	400(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movq	400(%rsp), %xmm0        # xmm0 = mem[0],zero
	movq	408(%rsp), %xmm4        # xmm4 = mem[0],zero
	movdqa	224(%rsp), %xmm2        # 16-byte Reload
	pshufd	$229, %xmm2, %xmm11     # xmm11 = xmm2[1,1,2,3]
	pshufd	$229, %xmm4, %xmm9      # xmm9 = xmm4[1,1,2,3]
	pshufd	$229, %xmm0, %xmm8      # xmm8 = xmm0[1,1,2,3]
	movdqa	%xmm11, %xmm5
	mulss	%xmm8, %xmm5
	movdqa	64(%rsp), %xmm7         # 16-byte Reload
	pshufd	$229, %xmm7, %xmm3      # xmm3 = xmm7[1,1,2,3]
	movdqa	%xmm3, %xmm6
	mulss	%xmm9, %xmm6
	addss	%xmm5, %xmm6
	movdqa	%xmm2, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm6, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movdqa	%xmm7, %xmm1
	movdqa	%xmm7, %xmm10
	mulss	%xmm4, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	movdqa	%xmm2, %xmm5
	movaps	%xmm4, %xmm1
	movdqa	%xmm2, %xmm6
	mulss	%xmm4, %xmm2
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	mulps	%xmm11, %xmm4
	unpcklps	%xmm10, %xmm5   # xmm5 = xmm5[0],xmm10[0],xmm5[1],xmm10[1]
	mulps	%xmm9, %xmm5
	addps	%xmm4, %xmm5
	shufps	$16, %xmm0, %xmm1       # xmm1 = xmm1[0,0],xmm0[1,0]
	shufps	$226, %xmm0, %xmm1      # xmm1 = xmm1[2,0],xmm0[2,3]
	movaps	%xmm10, %xmm4
	mulps	%xmm4, %xmm1
	addps	%xmm5, %xmm1
	shufps	$16, %xmm4, %xmm6       # xmm6 = xmm6[0,0],xmm4[1,0]
	shufps	$226, %xmm4, %xmm6      # xmm6 = xmm6[2,0],xmm4[2,3]
	mulps	%xmm0, %xmm6
	subps	%xmm6, %xmm1
	movaps	%xmm1, 336(%rsp)        # 16-byte Spill
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm1, 384(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm7
	mulss	%xmm9, %xmm7
	mulss	%xmm0, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm7
	subss	%xmm2, %xmm7
	movaps	%xmm7, 224(%rsp)        # 16-byte Spill
	leaq	400(%rsp), %rsi
	movq	%r12, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	400(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movsd	408(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	leaq	412(%rbx), %rdi
	leaq	400(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movq	400(%rsp), %xmm0        # xmm0 = mem[0],zero
	movq	408(%rsp), %xmm4        # xmm4 = mem[0],zero
	movdqa	16(%rsp), %xmm2         # 16-byte Reload
	pshufd	$229, %xmm2, %xmm7      # xmm7 = xmm2[1,1,2,3]
	pshufd	$229, %xmm4, %xmm9      # xmm9 = xmm4[1,1,2,3]
	pshufd	$229, %xmm0, %xmm8      # xmm8 = xmm0[1,1,2,3]
	movdqa	%xmm7, %xmm5
	mulss	%xmm8, %xmm5
	movdqa	64(%rsp), %xmm3         # 16-byte Reload
	pshufd	$229, %xmm3, %xmm10     # xmm10 = xmm3[1,1,2,3]
	movdqa	%xmm10, %xmm6
	mulss	%xmm9, %xmm6
	addss	%xmm5, %xmm6
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm12
	mulss	%xmm0, %xmm1
	addss	%xmm6, %xmm1
	movdqa	%xmm3, %xmm5
	movdqa	%xmm3, %xmm11
	mulss	%xmm4, %xmm5
	subss	%xmm5, %xmm1
	movdqa	%xmm12, %xmm5
	movaps	%xmm4, %xmm2
	movdqa	%xmm12, %xmm6
	mulss	%xmm4, %xmm12
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	mulps	%xmm7, %xmm4
	unpcklps	%xmm11, %xmm5   # xmm5 = xmm5[0],xmm11[0],xmm5[1],xmm11[1]
	mulps	%xmm9, %xmm5
	addps	%xmm4, %xmm5
	shufps	$16, %xmm0, %xmm2       # xmm2 = xmm2[0,0],xmm0[1,0]
	shufps	$226, %xmm0, %xmm2      # xmm2 = xmm2[2,0],xmm0[2,3]
	movaps	%xmm11, %xmm4
	mulps	%xmm4, %xmm2
	addps	%xmm5, %xmm2
	shufps	$16, %xmm4, %xmm6       # xmm6 = xmm6[0,0],xmm4[1,0]
	shufps	$226, %xmm4, %xmm6      # xmm6 = xmm6[2,0],xmm4[2,3]
	mulps	%xmm0, %xmm6
	subps	%xmm6, %xmm2
	mulss	%xmm9, %xmm7
	mulss	%xmm0, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm8, %xmm10
	subss	%xmm10, %xmm7
	subss	%xmm12, %xmm7
	movss	.LCPI5_5(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm4        # 16-byte Reload
	unpcklps	%xmm0, %xmm4    # xmm4 = xmm4[0],xmm0[0],xmm4[1],xmm0[1]
	movaps	%xmm1, 528(%rsp)        # 16-byte Spill
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	unpcklps	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	subps	%xmm0, %xmm4
	movaps	%xmm4, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	.LCPI5_2(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm2, %xmm5
	xorps	%xmm8, %xmm5
	pshufd	$229, %xmm5, %xmm12     # xmm12 = xmm5[1,1,2,3]
	movaps	%xmm4, %xmm0
	movaps	%xmm4, %xmm10
	mulss	%xmm7, %xmm0
	movaps	224(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm0, %xmm1
	movaps	384(%rsp), %xmm13       # 16-byte Reload
	movaps	%xmm13, %xmm9
	mulss	%xmm5, %xmm9
	addss	%xmm1, %xmm9
	movaps	336(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm3, %xmm0
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm9
	movaps	%xmm7, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm3, %xmm0
	movaps	%xmm1, 352(%rsp)        # 16-byte Spill
	mulps	%xmm1, %xmm0
	movaps	%xmm4, %xmm1
	movaps	%xmm4, %xmm11
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm1, 464(%rsp)        # 16-byte Spill
	movaps	%xmm2, 512(%rsp)        # 16-byte Spill
	mulps	%xmm2, %xmm1
	subps	%xmm1, %xmm0
	movaps	%xmm12, %xmm4
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm10, %xmm4
	addps	%xmm0, %xmm4
	movaps	%xmm13, %xmm2
	movaps	%xmm2, %xmm0
	unpcklps	%xmm5, %xmm0    # xmm0 = xmm0[0],xmm5[0],xmm0[1],xmm5[1]
	movaps	%xmm6, %xmm1
	unpcklps	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1]
	mulps	%xmm0, %xmm1
	subps	%xmm1, %xmm4
	movaps	%xmm11, %xmm0
	movaps	%xmm7, 544(%rsp)        # 16-byte Spill
	mulss	%xmm7, %xmm11
	movaps	%xmm2, %xmm0
	movaps	%xmm12, 320(%rsp)       # 16-byte Spill
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm11
	movaps	%xmm10, 128(%rsp)       # 16-byte Spill
	movaps	%xmm10, %xmm0
	movaps	%xmm6, 496(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm0
	subss	%xmm0, %xmm11
	movaps	%xmm3, %xmm0
	movaps	%xmm5, 480(%rsp)        # 16-byte Spill
	mulss	%xmm5, %xmm0
	subss	%xmm0, %xmm11
	movss	_ZL6vTwist(%rip), %xmm1 # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm11, %xmm2
	movss	_ZL6vTwist+8(%rip), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm12
	mulss	%xmm9, %xmm12
	addss	%xmm2, %xmm12
	movss	_ZL6vTwist+4(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm2
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm12
	movaps	%xmm1, %xmm2
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	movaps	%xmm11, %xmm5
	shufps	$0, %xmm4, %xmm5        # xmm5 = xmm5[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm5      # xmm5 = xmm5[2,0],xmm4[2,3]
	mulps	%xmm2, %xmm5
	movaps	%xmm10, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movaps	%xmm4, %xmm0
	shufps	$1, %xmm11, %xmm0       # xmm0 = xmm0[1,0],xmm11[0,0]
	shufps	$226, %xmm11, %xmm0     # xmm0 = xmm0[2,0],xmm11[2,3]
	mulps	%xmm6, %xmm0
	addps	%xmm5, %xmm0
	movaps	%xmm4, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movaps	%xmm3, %xmm5
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	movaps	%xmm9, %xmm6
	shufps	$0, %xmm7, %xmm6        # xmm6 = xmm6[0,0],xmm7[0,0]
	shufps	$226, %xmm7, %xmm6      # xmm6 = xmm6[2,0],xmm7[2,3]
	mulps	%xmm5, %xmm6
	subps	%xmm6, %xmm0
	movaps	%xmm7, 80(%rsp)         # 16-byte Spill
	mulss	%xmm7, %xmm1
	xorps	%xmm8, %xmm1
	mulss	%xmm9, %xmm10
	subss	%xmm10, %xmm1
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm1
	movaps	%xmm9, %xmm3
	xorps	%xmm8, %xmm3
	movaps	%xmm4, %xmm7
	xorps	%xmm8, %xmm7
	movaps	%xmm3, %xmm5
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	pshufd	$229, %xmm7, %xmm8      # xmm8 = xmm7[1,1,2,3]
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	shufps	$0, %xmm8, %xmm5        # xmm5 = xmm5[0,0],xmm8[0,0]
	shufps	$226, %xmm8, %xmm5      # xmm5 = xmm5[2,0],xmm8[2,3]
	mulps	%xmm1, %xmm5
	movaps	%xmm11, %xmm1
	unpcklps	%xmm1, %xmm1    # xmm1 = xmm1[0,0,1,1]
	movaps	%xmm12, %xmm6
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm1, %xmm6
	addps	%xmm5, %xmm6
	movaps	%xmm0, %xmm1
	movaps	%xmm4, 368(%rsp)        # 16-byte Spill
	mulps	%xmm4, %xmm1
	subps	%xmm1, %xmm6
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm12, %xmm1
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm1, %xmm3
	subps	%xmm3, %xmm6
	movaps	%xmm11, 240(%rsp)       # 16-byte Spill
	mulss	%xmm11, %xmm2
	addss	%xmm7, %xmm2
	movaps	%xmm9, 64(%rsp)         # 16-byte Spill
	mulss	%xmm9, %xmm12
	subss	%xmm12, %xmm2
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm2
	movaps	%xmm6, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_4
# BB#3:                                 # %call.sqrt785
	movaps	%xmm1, %xmm0
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	movaps	%xmm6, 32(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	32(%rsp), %xmm6         # 16-byte Reload
	movaps	16(%rsp), %xmm2         # 16-byte Reload
.LBB5_4:                                # %.split784
	movss	.LCPI5_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm7
	divss	%xmm0, %xmm7
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm6, %xmm4
	movaps	%xmm4, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	mulss	%xmm2, %xmm7
	movss	_ZL6vTwist+4(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movss	_ZL6vTwist+8(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movss	_ZL6vTwist(%rip), %xmm3 # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm4, %xmm1
	movaps	%xmm0, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm10, %xmm1
	mulss	%xmm7, %xmm1
	addss	%xmm6, %xmm1
	movss	.LCPI5_6(%rip), %xmm6   # xmm6 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm6
	jbe	.LBB5_9
# BB#5:
	movaps	.LCPI5_3(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm10, %xmm1
	ucomiss	.LCPI5_8(%rip), %xmm1
	jbe	.LBB5_16
# BB#6:
	mulss	%xmm0, %xmm0
	mulss	%xmm10, %xmm10
	addss	%xmm0, %xmm10
	xorps	%xmm0, %xmm0
	sqrtss	%xmm10, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_8
# BB#7:                                 # %call.sqrt787
	movaps	%xmm10, %xmm0
	callq	sqrtf
	movss	.LCPI5_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm8
.LBB5_8:                                # %.split786
	movss	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	_ZL6vTwist+8(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm10
	xorps	.LCPI5_2(%rip), %xmm10
	mulss	_ZL6vTwist+4(%rip), %xmm1
	unpcklps	%xmm1, %xmm10   # xmm10 = xmm10[0],xmm1[0],xmm10[1],xmm1[1]
	xorps	%xmm11, %xmm11
	jmp	.LBB5_19
.LBB5_9:
	movaps	%xmm0, %xmm11
	mulss	%xmm7, %xmm11
	mulss	%xmm10, %xmm5
	subss	%xmm5, %xmm11
	unpcklps	%xmm3, %xmm10   # xmm10 = xmm10[0],xmm3[0],xmm10[1],xmm3[1]
	mulps	%xmm4, %xmm10
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	unpcklps	%xmm4, %xmm7    # xmm7 = xmm7[0],xmm4[0],xmm7[1],xmm4[1]
	mulps	%xmm3, %xmm7
	subps	%xmm7, %xmm10
	addss	.LCPI5_1(%rip), %xmm1
	addss	%xmm1, %xmm1
	xorps	%xmm12, %xmm12
	sqrtss	%xmm1, %xmm12
	ucomiss	%xmm12, %xmm12
	jnp	.LBB5_11
# BB#10:                                # %call.sqrt791
	movaps	%xmm1, %xmm0
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm11, 32(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movss	.LCPI5_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm8
	movaps	32(%rsp), %xmm11        # 16-byte Reload
	movaps	%xmm0, %xmm12
.LBB5_11:                               # %.split790
	movaps	%xmm8, %xmm0
	divss	%xmm12, %xmm0
	mulss	%xmm0, %xmm11
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm10
	mulss	.LCPI5_7(%rip), %xmm12
	jmp	.LBB5_20
.LBB5_12:
	movss	604(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movss	608(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	612(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	616(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	.LCPI5_0(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm3
	movaps	%xmm5, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm11, %xmm1
	mulss	%xmm3, %xmm1
	mulss	%xmm8, %xmm3
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm2, %xmm10
	mulss	%xmm1, %xmm10
	mulss	%xmm3, %xmm2
	mulss	%xmm5, %xmm0
	movaps	%xmm5, %xmm4
	mulss	%xmm1, %xmm4
	mulss	%xmm3, %xmm5
	movaps	%xmm5, %xmm6
	mulss	%xmm11, %xmm1
	mulss	%xmm3, %xmm11
	mulss	%xmm8, %xmm3
	movaps	%xmm1, %xmm5
	addss	%xmm3, %xmm5
	movss	.LCPI5_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm7
	subss	%xmm5, %xmm7
	movaps	%xmm7, 240(%rsp)        # 16-byte Spill
	movaps	%xmm4, %xmm5
	subss	%xmm2, %xmm5
	movaps	%xmm5, 464(%rsp)        # 16-byte Spill
	movaps	%xmm6, %xmm5
	addss	%xmm10, %xmm5
	movaps	%xmm5, 352(%rsp)        # 16-byte Spill
	addss	%xmm2, %xmm4
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	addss	%xmm0, %xmm3
	movaps	%xmm8, %xmm2
	subss	%xmm3, %xmm2
	movaps	%xmm2, 320(%rsp)        # 16-byte Spill
	movaps	%xmm11, %xmm2
	subss	%xmm9, %xmm2
	movaps	%xmm2, 112(%rsp)        # 16-byte Spill
	subss	%xmm10, %xmm6
	movaps	%xmm6, 224(%rsp)        # 16-byte Spill
	addss	%xmm9, %xmm11
	movaps	%xmm11, 16(%rsp)        # 16-byte Spill
	addss	%xmm0, %xmm1
	subss	%xmm1, %xmm8
	movaps	%xmm8, 384(%rsp)        # 16-byte Spill
	movss	(%r13), %xmm8           # xmm8 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	348(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	mulss	%xmm8, %xmm1
	movss	364(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movss	380(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm3          # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm3, 128(%rsp)        # 16-byte Spill
	addss	%xmm2, %xmm1
	movss	%xmm1, 32(%rsp)         # 4-byte Spill
	movss	352(%rbx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm12, %xmm1
	movss	368(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm7, %xmm2
	addss	%xmm1, %xmm2
	movss	384(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm14, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, 368(%rsp)        # 4-byte Spill
	movss	356(%rbx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm1
	mulss	%xmm13, %xmm1
	movss	372(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movss	388(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm6, %xmm9
	movss	%xmm9, 336(%rsp)        # 4-byte Spill
	addss	%xmm2, %xmm1
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	movss	16(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm1
	mulss	%xmm3, %xmm1
	movss	20(%r13), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm1, %xmm6
	movss	24(%r13), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	mulss	%xmm15, %xmm1
	addss	%xmm6, %xmm1
	movss	%xmm1, 528(%rsp)        # 4-byte Spill
	movaps	%xmm12, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm7, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm14, %xmm1
	mulss	%xmm15, %xmm1
	addss	%xmm6, %xmm1
	movss	%xmm1, 512(%rsp)        # 4-byte Spill
	movaps	%xmm13, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm5, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm9, %xmm1
	mulss	%xmm15, %xmm1
	addss	%xmm6, %xmm1
	movss	%xmm1, 544(%rsp)        # 4-byte Spill
	movss	32(%r13), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm10
	movss	36(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm10, %xmm4
	movss	40(%r13), %xmm10        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm11
	addss	%xmm4, %xmm11
	movss	%xmm11, 496(%rsp)       # 4-byte Spill
	mulss	%xmm9, %xmm12
	mulss	%xmm1, %xmm7
	addss	%xmm12, %xmm7
	mulss	%xmm10, %xmm14
	addss	%xmm7, %xmm14
	movss	%xmm14, 480(%rsp)       # 4-byte Spill
	mulss	%xmm9, %xmm13
	mulss	%xmm1, %xmm5
	addss	%xmm13, %xmm5
	movss	336(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm4
	addss	%xmm5, %xmm4
	movss	%xmm4, 336(%rsp)        # 4-byte Spill
	movss	396(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm8
	movss	400(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	addss	%xmm8, %xmm0
	movss	404(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm7        # 16-byte Reload
	mulss	%xmm6, %xmm7
	addss	%xmm0, %xmm7
	movaps	%xmm7, 128(%rsp)        # 16-byte Spill
	mulss	%xmm4, %xmm3
	mulss	%xmm5, %xmm2
	addss	%xmm3, %xmm2
	mulss	%xmm6, %xmm15
	addss	%xmm2, %xmm15
	movss	%xmm15, 12(%rsp)        # 4-byte Spill
	mulss	%xmm4, %xmm9
	mulss	%xmm5, %xmm1
	addss	%xmm9, %xmm1
	mulss	%xmm6, %xmm10
	addss	%xmm1, %xmm10
	movss	%xmm10, 96(%rsp)        # 4-byte Spill
	movss	16(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	(%r12), %xmm12          # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm12   # xmm12 = xmm12[0],xmm0[0],xmm12[1],xmm0[1]
	movss	4(%r12), %xmm14         # xmm14 = mem[0],zero,zero,zero
	movss	20(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm14   # xmm14 = xmm14[0],xmm0[0],xmm14[1],xmm0[1]
	movss	412(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	movss	428(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm14, %xmm1
	addps	%xmm0, %xmm1
	movss	24(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm13         # xmm13 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm13   # xmm13 = xmm13[0],xmm0[0],xmm13[1],xmm0[1]
	movss	444(%rbx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm11
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulps	%xmm13, %xmm11
	addps	%xmm1, %xmm11
	movss	416(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	movss	432(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm14, %xmm4
	addps	%xmm0, %xmm4
	movss	448(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm13, %xmm6
	addps	%xmm4, %xmm6
	movss	420(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm12, %xmm5
	movss	436(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm14, %xmm7
	addps	%xmm5, %xmm7
	movss	452(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, 80(%rsp)        # 16-byte Spill
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm13, %xmm10
	addps	%xmm7, %xmm10
	movss	32(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	movss	36(%r12), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm3, %xmm2
	movss	40(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	addss	%xmm2, %xmm15
	movaps	%xmm15, %xmm2
	mulss	%xmm5, %xmm8
	mulss	%xmm7, %xmm1
	addss	%xmm8, %xmm1
	mulss	%xmm3, %xmm9
	addss	%xmm1, %xmm9
	mulss	%xmm5, %xmm4
	mulss	%xmm7, %xmm0
	addss	%xmm4, %xmm0
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm3, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm1, 80(%rsp)         # 16-byte Spill
	movss	460(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm12, %xmm0
	movss	464(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm14, %xmm1
	addps	%xmm0, %xmm1
	movss	468(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm13, %xmm0
	addps	%xmm1, %xmm0
	movsd	48(%r12), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	addss	%xmm5, %xmm7
	addss	%xmm3, %xmm7
	movaps	240(%rsp), %xmm0        # 16-byte Reload
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm11, %xmm0
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	addps	%xmm0, %xmm1
	movaps	224(%rsp), %xmm13       # 16-byte Reload
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	%xmm10, %xmm13
	addps	%xmm1, %xmm13
	movaps	464(%rsp), %xmm12       # 16-byte Reload
	movaps	%xmm12, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm11, %xmm0
	movaps	320(%rsp), %xmm1        # 16-byte Reload
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	addps	%xmm0, %xmm1
	movaps	16(%rsp), %xmm15        # 16-byte Reload
	movaps	%xmm15, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm10, %xmm0
	addps	%xmm1, %xmm0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
	movaps	352(%rsp), %xmm14       # 16-byte Reload
	movaps	%xmm14, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm11, %xmm0
	movaps	112(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm6, %xmm1
	addps	%xmm0, %xmm1
	movaps	384(%rsp), %xmm4        # 16-byte Reload
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm10, %xmm5
	addps	%xmm1, %xmm5
	movaps	240(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm2, %xmm1
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm9, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movaps	%xmm12, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, %xmm12
	movaps	320(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm9, %xmm1
	addss	%xmm0, %xmm1
	mulss	%xmm3, %xmm15
	addss	%xmm1, %xmm15
	movaps	%xmm15, 16(%rsp)        # 16-byte Spill
	movaps	%xmm14, %xmm0
	mulss	%xmm12, %xmm0
	mulss	%xmm9, %xmm8
	addss	%xmm0, %xmm8
	mulss	%xmm3, %xmm4
	addss	%xmm8, %xmm4
	movaps	%xmm4, 384(%rsp)        # 16-byte Spill
	xorps	%xmm0, %xmm0
	mulps	%xmm0, %xmm11
	mulps	%xmm0, %xmm6
	addps	%xmm11, %xmm6
	mulps	%xmm0, %xmm10
	addps	%xmm6, %xmm10
	addps	144(%rsp), %xmm10       # 16-byte Folded Reload
	movaps	%xmm10, 240(%rsp)       # 16-byte Spill
	xorps	%xmm0, %xmm0
	movaps	%xmm12, %xmm1
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm9
	addss	%xmm1, %xmm9
	mulss	%xmm0, %xmm3
	addss	%xmm9, %xmm3
	addss	56(%r12), %xmm7
	addss	%xmm7, %xmm3
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	addss	48(%r13), %xmm1
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	52(%r13), %xmm0
	xorps	.LCPI5_2(%rip), %xmm1
	movss	32(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm2
	mulss	%xmm1, %xmm2
	movaps	%xmm1, %xmm7
	movss	528(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm2
	movss	96(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	addss	56(%r13), %xmm3
	movss	496(%rsp), %xmm14       # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm1
	mulss	%xmm3, %xmm1
	movaps	%xmm3, %xmm4
	subss	%xmm1, %xmm2
	movaps	%xmm2, %xmm11
	movaps	%xmm11, 112(%rsp)       # 16-byte Spill
	movss	368(%rsp), %xmm6        # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm1
	mulss	%xmm7, %xmm1
	movss	512(%rsp), %xmm12       # 4-byte Reload
                                        # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm3
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm1
	movss	480(%rsp), %xmm8        # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm3
	mulss	%xmm4, %xmm3
	subss	%xmm3, %xmm1
	movaps	%xmm1, %xmm15
	movaps	%xmm15, 464(%rsp)       # 16-byte Spill
	movss	64(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm2
	mulss	%xmm1, %xmm2
	movss	544(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm2
	movss	336(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	subss	%xmm4, %xmm2
	movaps	%xmm2, 128(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm6, %xmm4
	movaps	160(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm1, %xmm3
	mulss	%xmm5, %xmm3
	addss	%xmm4, %xmm3
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	movaps	%xmm9, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm12, %xmm4
	mulss	%xmm2, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm7, %xmm1
	mulss	%xmm5, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, 320(%rsp)        # 4-byte Spill
	movaps	%xmm14, %xmm4
	mulss	%xmm13, %xmm4
	movaps	%xmm8, %xmm7
	mulss	%xmm2, %xmm7
	movaps	%xmm2, %xmm1
	addss	%xmm4, %xmm7
	mulss	%xmm5, %xmm0
	addss	%xmm7, %xmm0
	movss	%xmm0, 352(%rsp)        # 4-byte Spill
	movaps	%xmm11, %xmm12
	shufps	$224, %xmm12, %xmm12    # xmm12 = xmm12[0,0,2,3]
	mulps	%xmm13, %xmm12
	shufps	$229, %xmm13, %xmm13    # xmm13 = xmm13[1,1,2,3]
	movaps	%xmm10, %xmm6
	mulss	%xmm13, %xmm6
	movaps	%xmm15, %xmm14
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	movaps	%xmm1, %xmm0
	mulps	%xmm0, %xmm14
	movaps	%xmm0, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movss	368(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm2, %xmm3
	addss	%xmm6, %xmm3
	movaps	128(%rsp), %xmm6        # 16-byte Reload
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm5, %xmm6
	movaps	%xmm5, %xmm10
	shufps	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	movss	64(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm8
	mulss	%xmm10, %xmm8
	addss	%xmm3, %xmm8
	movaps	%xmm9, %xmm5
	mulss	%xmm13, %xmm5
	movss	512(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm5, %xmm0
	movss	544(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm7
	mulss	%xmm10, %xmm7
	addss	%xmm0, %xmm7
	movss	496(%rsp), %xmm15       # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm13
	movss	480(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	addss	%xmm13, %xmm2
	movss	336(%rsp), %xmm13       # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm10
	addss	%xmm2, %xmm10
	movaps	224(%rsp), %xmm0        # 16-byte Reload
	movss	32(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm2, %xmm4
	addss	%xmm5, %xmm4
	movaps	384(%rsp), %xmm5        # 16-byte Reload
	mulss	%xmm5, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	movss	528(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	mulss	%xmm2, %xmm3
	addss	%xmm1, %xmm3
	mulss	%xmm5, %xmm11
	addss	%xmm3, %xmm11
	mulss	%xmm0, %xmm15
	movaps	%xmm0, %xmm1
	mulss	%xmm2, %xmm9
	addss	%xmm15, %xmm9
	mulss	%xmm5, %xmm13
	addss	%xmm9, %xmm13
	addps	%xmm12, %xmm14
	addps	%xmm14, %xmm6
	addps	240(%rsp), %xmm6        # 16-byte Folded Reload
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm1, %xmm0
	movaps	464(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm2, %xmm1
	addss	%xmm0, %xmm1
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm5, %xmm0
	addss	%xmm1, %xmm0
	addss	80(%rsp), %xmm0         # 16-byte Folded Reload
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movss	48(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 400(%rsp)
	movss	320(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 404(%rsp)
	movss	352(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 408(%rsp)
	movl	$0, 412(%rsp)
	movss	%xmm8, 416(%rsp)
	movss	%xmm7, 420(%rsp)
	movss	%xmm10, 424(%rsp)
	movl	$0, 428(%rsp)
	movss	64(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 432(%rsp)
	movss	%xmm11, 436(%rsp)
	movss	%xmm13, 440(%rsp)
	movl	$0, 444(%rsp)
	movlps	%xmm6, 448(%rsp)
	movlps	%xmm1, 456(%rsp)
	leaq	400(%rsp), %rdi
	leaq	560(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movq	560(%rsp), %xmm0        # xmm0 = mem[0],zero
	pshufd	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movq	568(%rsp), %xmm3        # xmm3 = mem[0],zero
	pshufd	$212, %xmm3, %xmm1      # xmm1 = xmm3[0,1,1,3]
	movd	%xmm0, %eax
	pshufd	$78, %xmm0, %xmm2       # xmm2 = xmm0[2,3,0,1]
	movd	%xmm2, %ecx
	movd	%xmm1, %edx
	movd	%xmm0, 508(%rbx)
	movd	%xmm2, 512(%rbx)
	movd	%xmm1, 516(%rbx)
	movl	$0, 520(%rbx)
	movd	%eax, %xmm0
	mulss	%xmm0, %xmm0
	movd	%ecx, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%edx, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_14
# BB#13:                                # %call.sqrt
	movdqa	%xmm3, 128(%rsp)        # 16-byte Spill
	callq	sqrtf
	movdqa	128(%rsp), %xmm3        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB5_14:                               # %.split
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	movss	508(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 508(%rbx)
	movss	512(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 512(%rbx)
	mulss	516(%rbx), %xmm2
	movss	%xmm2, 516(%rbx)
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm3, %xmm0
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, 552(%rbx)
	andps	.LCPI5_3(%rip), %xmm0
	movss	.LCPI5_4(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	ja	.LBB5_69
# BB#15:
	movb	$1, 574(%rbx)
	jmp	.LBB5_69
.LBB5_16:
	mulss	%xmm3, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_18
# BB#17:                                # %call.sqrt789
	callq	sqrtf
	movss	.LCPI5_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm8
	movaps	%xmm0, %xmm1
.LBB5_18:                               # %.split788
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	_ZL6vTwist+4(%rip), %xmm11 # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm11
	xorps	.LCPI5_2(%rip), %xmm11
	mulss	_ZL6vTwist(%rip), %xmm0
	xorps	%xmm10, %xmm10
	movss	%xmm0, %xmm10           # xmm10 = xmm0[0],xmm10[1,2,3]
.LBB5_19:                               # %_Z15shortestArcQuatRK9btVector3S1_.exit
	xorps	%xmm12, %xmm12
.LBB5_20:                               # %_Z15shortestArcQuatRK9btVector3S1_.exit
	movaps	%xmm11, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_22
# BB#21:                                # %call.sqrt792
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm11, 32(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movss	.LCPI5_1(%rip), %xmm11  # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm8
	movaps	32(%rsp), %xmm11        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB5_22:                               # %_Z15shortestArcQuatRK9btVector3S1_.exit.split
	movaps	%xmm8, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm11
	mulss	%xmm0, %xmm12
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm10
	movaps	%xmm11, %xmm1
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm2
	xorps	%xmm2, %xmm1
	movaps	%xmm10, %xmm0
	xorps	%xmm2, %xmm0
	movaps	64(%rsp), %xmm8         # 16-byte Reload
	movaps	%xmm8, %xmm2
	movaps	240(%rsp), %xmm7        # 16-byte Reload
	shufps	$0, %xmm7, %xmm2        # xmm2 = xmm2[0,0],xmm7[0,0]
	shufps	$226, %xmm7, %xmm2      # xmm2 = xmm2[2,0],xmm7[2,3]
	movaps	%xmm1, %xmm3
	unpcklps	%xmm12, %xmm3   # xmm3 = xmm3[0],xmm12[0],xmm3[1],xmm12[1]
	mulps	%xmm2, %xmm3
	movaps	%xmm7, %xmm2
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	shufps	$0, %xmm4, %xmm2        # xmm2 = xmm2[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm2      # xmm2 = xmm2[2,0],xmm4[2,3]
	movaps	%xmm12, %xmm6
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	mulps	%xmm2, %xmm6
	addps	%xmm3, %xmm6
	movaps	%xmm8, %xmm3
	movaps	368(%rsp), %xmm9        # 16-byte Reload
	unpcklps	%xmm9, %xmm3    # xmm3 = xmm3[0],xmm9[0],xmm3[1],xmm9[1]
	movaps	%xmm9, %xmm2
	mulps	%xmm10, %xmm2
	subps	%xmm2, %xmm6
	pshufd	$229, %xmm0, %xmm2      # xmm2 = xmm0[1,1,2,3]
	shufps	$0, %xmm2, %xmm1        # xmm1 = xmm1[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm1      # xmm1 = xmm1[2,0],xmm2[2,3]
	mulps	%xmm3, %xmm1
	subps	%xmm1, %xmm6
	movaps	%xmm6, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm9, %xmm3
	mulss	%xmm12, %xmm3
	movaps	%xmm7, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movaps	%xmm8, %xmm3
	mulss	%xmm11, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm4, %xmm3
	mulss	%xmm0, %xmm3
	subss	%xmm3, %xmm5
	mulss	%xmm12, %xmm7
	mulss	%xmm11, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm4
	mulss	%xmm9, %xmm2
	subss	%xmm2, %xmm4
	movaps	%xmm6, 368(%rsp)        # 16-byte Spill
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, 64(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm2, %xmm2
	sqrtss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm2
	jnp	.LBB5_24
# BB#23:                                # %call.sqrt793
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm11, 32(%rsp)        # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	32(%rsp), %xmm11        # 16-byte Reload
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movaps	%xmm0, %xmm2
.LBB5_24:                               # %_Z15shortestArcQuatRK9btVector3S1_.exit.split.split
	movss	492(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	504(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	movss	%xmm2, 240(%rsp)        # 4-byte Spill
	jb	.LBB5_26
# BB#25:
	movss	496(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jae	.LBB5_34
.LBB5_26:
	movss	364(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	380(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	(%r13), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%r13), %xmm2          # xmm2 = mem[0],zero,zero,zero
	movss	8(%r13), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	16(%r13), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	348(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	352(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm3
	mulss	%xmm8, %xmm3
	movss	20(%r13), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	mulss	%xmm1, %xmm0
	addss	%xmm3, %xmm0
	movss	24(%r13), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm11
	mulss	%xmm14, %xmm11
	addss	%xmm0, %xmm11
	movss	32(%r13), %xmm10        # xmm10 = mem[0],zero,zero,zero
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	movaps	%xmm10, %xmm0
	unpcklps	%xmm9, %xmm0    # xmm0 = xmm0[0],xmm9[0],xmm0[1],xmm9[1]
	mulps	%xmm5, %xmm0
	movss	36(%r13), %xmm3         # xmm3 = mem[0],zero,zero,zero
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movaps	%xmm3, %xmm5
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	mulps	%xmm4, %xmm5
	addps	%xmm0, %xmm5
	movss	40(%r13), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, 16(%rsp)        # 16-byte Spill
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	unpcklps	%xmm12, %xmm15  # xmm15 = xmm15[0],xmm12[0],xmm15[1],xmm12[1]
	mulps	%xmm7, %xmm15
	addps	%xmm5, %xmm15
	movss	368(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	384(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm4
	mulss	%xmm6, %xmm4
	movaps	%xmm2, %xmm5
	mulss	%xmm0, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm12, %xmm13
	mulss	%xmm7, %xmm13
	addss	%xmm5, %xmm13
	movaps	%xmm8, %xmm4
	mulss	%xmm6, %xmm4
	movaps	%xmm1, %xmm5
	mulss	%xmm0, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm14, %xmm4
	mulss	%xmm7, %xmm4
	addss	%xmm5, %xmm4
	movss	%xmm4, 32(%rsp)         # 4-byte Spill
	mulss	%xmm10, %xmm6
	mulss	%xmm3, %xmm0
	addss	%xmm6, %xmm0
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	mulss	%xmm6, %xmm7
	addss	%xmm0, %xmm7
	movss	356(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	372(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm9
	mulss	%xmm4, %xmm2
	addss	%xmm9, %xmm2
	movaps	%xmm12, %xmm9
	movss	388(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm9
	addss	%xmm2, %xmm9
	movaps	%xmm15, %xmm12
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	mulss	%xmm0, %xmm8
	mulss	%xmm4, %xmm1
	addss	%xmm8, %xmm1
	mulss	%xmm5, %xmm14
	addss	%xmm1, %xmm14
	mulss	%xmm0, %xmm10
	mulss	%xmm4, %xmm3
	addss	%xmm10, %xmm3
	movaps	%xmm6, %xmm10
	mulss	%xmm5, %xmm10
	addss	%xmm3, %xmm10
	movss	412(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	428(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	444(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	(%r12), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	4(%r12), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	addss	%xmm3, %xmm4
	movss	8(%r12), %xmm8          # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm8
	addss	%xmm4, %xmm8
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	32(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm1, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	36(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm5    # xmm5 = xmm5[0],xmm1[0],xmm5[1],xmm1[1]
	mulps	%xmm2, %xmm5
	addps	%xmm4, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	40(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	mulps	%xmm0, %xmm4
	addps	%xmm5, %xmm4
	movaps	%xmm4, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm12, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm11, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm15, %xmm1
	mulss	%xmm6, %xmm1
	addss	%xmm2, %xmm1
	movaps	%xmm13, %xmm0
	mulss	%xmm8, %xmm0
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movss	%xmm7, 12(%rsp)         # 4-byte Spill
	movaps	%xmm7, %xmm5
	mulss	%xmm6, %xmm5
	addss	%xmm2, %xmm5
	movaps	%xmm9, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm14, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm6, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm2, %xmm3
	movss	492(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	504(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm7, %xmm0
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	jbe	.LBB5_31
# BB#27:
	movaps	.LCPI5_3(%rip), %xmm3   # xmm3 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm3
	ucomiss	496(%rbx), %xmm0
	movaps	%xmm5, %xmm2
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	jbe	.LBB5_40
# BB#28:
	movss	.LCPI5_4(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm0
	jbe	.LBB5_30
# BB#29:
	movaps	112(%rsp), %xmm1        # 16-byte Reload
	andps	.LCPI5_3(%rip), %xmm1
	ucomiss	%xmm1, %xmm0
	ja	.LBB5_52
.LBB5_30:
	movb	$1, 574(%rbx)
	movaps	%xmm4, %xmm0
	mulps	%xmm15, %xmm0
	movaps	%xmm11, %xmm1
	unpcklps	%xmm15, %xmm1   # xmm1 = xmm1[0],xmm15[0],xmm1[1],xmm15[1]
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm1, %xmm6
	subps	%xmm6, %xmm0
	mulss	%xmm8, %xmm11
	mulss	%xmm4, %xmm12
	subss	%xmm12, %xmm11
	movaps	.LCPI5_2(%rip), %xmm1   # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm0
	xorps	%xmm1, %xmm11
	xorps	%xmm1, %xmm1
	movss	%xmm11, %xmm1           # xmm1 = xmm11[0],xmm1[1,2,3]
	movlps	%xmm0, 508(%rbx)
	movlps	%xmm1, 516(%rbx)
	jmp	.LBB5_52
.LBB5_31:
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	movaps	%xmm1, 96(%rsp)         # 16-byte Spill
	movaps	.LCPI5_3(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm2
	movss	.LCPI5_4(%rip), %xmm3   # xmm3 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm3
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	ja	.LBB5_33
# BB#32:
	movb	$1, 574(%rbx)
	ucomiss	%xmm0, %xmm7
	jae	.LBB5_79
.LBB5_33:
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	jmp	.LBB5_43
.LBB5_34:
	movaps	%xmm11, 32(%rsp)        # 16-byte Spill
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm12, %xmm0
	callq	acosf
	movaps	%xmm0, %xmm7
	addss	%xmm7, %xmm7
	ucomiss	.LCPI5_4(%rip), %xmm7
	jbe	.LBB5_70
# BB#35:
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm0
	mulss	%xmm0, %xmm0
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm3, %xmm12
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	movaps	%xmm12, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_37
# BB#36:                                # %call.sqrt795
	movaps	%xmm1, %xmm0
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movss	%xmm7, 112(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	112(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm4         # 16-byte Reload
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	16(%rsp), %xmm3         # 16-byte Reload
.LBB5_37:                               # %.split794
	movss	.LCPI5_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	divss	%xmm0, %xmm6
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulss	%xmm6, %xmm12
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm4, %xmm6
	movaps	%xmm6, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movss	492(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	.LCPI5_3(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm1
	ucomiss	.LCPI5_4(%rip), %xmm1
	jbe	.LBB5_71
# BB#38:
	movaps	%xmm12, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm3, %xmm3
	divss	%xmm3, %xmm1
	movss	496(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm3
	movaps	%xmm2, %xmm4
	divss	%xmm3, %xmm4
	mulss	%xmm0, %xmm0
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	addss	%xmm2, %xmm1
	divss	%xmm3, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_71
# BB#39:                                # %call.sqrt797
	movaps	%xmm1, %xmm0
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	movss	%xmm7, 112(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	112(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	jmp	.LBB5_71
.LBB5_40:
	movaps	%xmm2, 48(%rsp)         # 16-byte Spill
	movss	.LCPI5_4(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	ja	.LBB5_42
# BB#41:
	movb	$1, 574(%rbx)
	movss	496(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	jae	.LBB5_83
.LBB5_42:
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
.LBB5_43:
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
.LBB5_44:
	movaps	112(%rsp), %xmm3        # 16-byte Reload
.LBB5_45:
	mulss	%xmm1, %xmm12
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	mulss	%xmm7, %xmm13
	addss	%xmm12, %xmm13
	mulss	%xmm3, %xmm9
	addss	%xmm13, %xmm9
	mulss	%xmm1, %xmm11
	mulss	%xmm7, %xmm2
	addss	%xmm11, %xmm2
	mulss	%xmm3, %xmm14
	addss	%xmm2, %xmm14
	mulss	%xmm1, %xmm15
	mulss	%xmm7, %xmm0
	addss	%xmm15, %xmm0
	mulss	%xmm3, %xmm10
	addss	%xmm0, %xmm10
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm14, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_47
# BB#46:                                # %call.sqrt801
	movaps	%xmm1, %xmm0
	movaps	%xmm14, 160(%rsp)       # 16-byte Spill
	movaps	%xmm9, 144(%rsp)        # 16-byte Spill
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	movaps	%xmm6, 192(%rsp)        # 16-byte Spill
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	176(%rsp), %xmm4        # 16-byte Reload
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	movaps	208(%rsp), %xmm8        # 16-byte Reload
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movaps	144(%rsp), %xmm9        # 16-byte Reload
	movaps	160(%rsp), %xmm14       # 16-byte Reload
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
.LBB5_47:                               # %.split800
	movss	.LCPI5_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm9
	mulss	%xmm1, %xmm14
	mulss	%xmm10, %xmm1
	movaps	%xmm4, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm6, %xmm2
	mulss	%xmm14, %xmm2
	subss	%xmm2, %xmm0
	mulss	%xmm9, %xmm6
	mulss	%xmm8, %xmm1
	subss	%xmm1, %xmm6
	mulss	%xmm8, %xmm14
	mulss	%xmm4, %xmm9
	subss	%xmm9, %xmm14
	movaps	%xmm0, %xmm1
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm2, %xmm4
	xorps	%xmm4, %xmm1
	movaps	%xmm6, %xmm2
	xorps	%xmm4, %xmm2
	movaps	%xmm14, %xmm3
	xorps	%xmm4, %xmm3
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	xorps	%xmm2, %xmm2
	movss	%xmm3, %xmm2            # xmm2 = xmm3[0],xmm2[1,2,3]
	movlps	%xmm1, 508(%rbx)
	movlps	%xmm2, 516(%rbx)
	mulss	%xmm0, %xmm0
	mulss	%xmm6, %xmm6
	addss	%xmm0, %xmm6
	mulss	%xmm14, %xmm14
	addss	%xmm6, %xmm14
	xorps	%xmm0, %xmm0
	sqrtss	%xmm14, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_49
# BB#48:                                # %call.sqrt802
	movaps	%xmm14, %xmm0
	callq	sqrtf
	movss	.LCPI5_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
.LBB5_49:                               # %.split800.split
	movss	%xmm0, 552(%rbx)
	movss	508(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	512(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	516(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_51
# BB#50:                                # %call.sqrt803
	callq	sqrtf
	movss	.LCPI5_1(%rip), %xmm7   # xmm7 = mem[0],zero,zero,zero
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB5_51:                               # %.split800.split.split
	divss	%xmm1, %xmm7
	movss	508(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	%xmm0, 508(%rbx)
	movss	512(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	%xmm0, 512(%rbx)
	mulss	516(%rbx), %xmm7
	movss	%xmm7, 516(%rbx)
.LBB5_52:
	movss	500(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jae	.LBB5_54
# BB#53:
	movl	$0, 560(%rbx)
	jmp	.LBB5_69
.LBB5_54:
	divss	240(%rsp), %xmm5        # 4-byte Folded Reload
	movaps	%xmm5, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm5, %xmm1
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	mulss	80(%rsp), %xmm5         # 16-byte Folded Reload
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	mulps	368(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm0
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, 560(%rbx)
	ucomiss	.LCPI5_10(%rip), %xmm0
	jbe	.LBB5_56
# BB#55:
	movaps	.LCPI5_2(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm1
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	xorps	%xmm1, %xmm0
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	xorps	%xmm1, %xmm2
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	xorps	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, 560(%rbx)
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	jmp	.LBB5_57
.LBB5_56:
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	unpcklps	16(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
.LBB5_57:
	movaps	80(%rsp), %xmm13        # 16-byte Reload
	shufps	$212, %xmm13, %xmm13    # xmm13 = xmm13[0,1,1,3]
	movd	%xmm13, %eax
	shufps	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	ucomiss	.LCPI5_4(%rip), %xmm0
	jbe	.LBB5_61
# BB#58:
	movd	%eax, %xmm2
	pshufd	$78, %xmm13, %xmm13     # xmm13 = xmm13[2,3,0,1]
	punpckldq	%xmm1, %xmm13   # xmm13 = xmm13[0],xmm1[0],xmm13[1],xmm1[1]
	movdqa	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	movdqa	%xmm13, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm13, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB5_60
# BB#59:                                # %call.sqrt805
	movdqa	%xmm13, 80(%rsp)        # 16-byte Spill
	movd	%xmm2, 64(%rsp)         # 4-byte Folded Spill
	callq	sqrtf
	movd	64(%rsp), %xmm2         # 4-byte Folded Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movdqa	80(%rsp), %xmm13        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB5_60:                               # %.split804
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm2
	movd	%xmm2, %eax
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm13
	shufps	$212, %xmm13, %xmm13    # xmm13 = xmm13[0,1,1,3]
	movss	560(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB5_62
.LBB5_61:
	shufpd	$1, %xmm1, %xmm13       # xmm13 = xmm13[1],xmm1[0]
.LBB5_62:                               # %_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3.exit
	movss	476(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	500(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm4, %xmm2
	ucomiss	%xmm2, %xmm0
	jbe	.LBB5_67
# BB#63:
	movb	$1, 573(%rbx)
	movss	.LCPI5_1(%rip), %xmm8   # xmm8 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm3
	movaps	%xmm8, %xmm5
	jbe	.LBB5_66
# BB#64:
	movss	.LCPI5_9(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	ucomiss	%xmm4, %xmm5
	movaps	%xmm8, %xmm5
	jbe	.LBB5_66
# BB#65:
	movaps	%xmm0, %xmm5
	subss	%xmm2, %xmm5
	subss	%xmm2, %xmm3
	divss	%xmm3, %xmm5
.LBB5_66:
	movss	%xmm5, 580(%rbx)
	subss	%xmm2, %xmm0
	movss	%xmm0, 556(%rbx)
	movd	%eax, %xmm0
	movaps	544(%rsp), %xmm9        # 16-byte Reload
	movaps	%xmm9, %xmm4
	mulss	%xmm0, %xmm4
	movaps	512(%rsp), %xmm11       # 16-byte Reload
	movaps	%xmm11, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	528(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm5
	shufps	$0, %xmm3, %xmm5        # xmm5 = xmm5[0,0],xmm3[0,0]
	shufps	$226, %xmm3, %xmm5      # xmm5 = xmm5[2,0],xmm3[2,3]
	mulss	%xmm0, %xmm3
	movdqa	%xmm0, %xmm6
	movdqa	.LCPI5_2(%rip), %xmm0   # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm6
	pshufd	$232, %xmm13, %xmm7     # xmm7 = xmm13[0,2,2,3]
	movaps	352(%rsp), %xmm12       # 16-byte Reload
	movaps	%xmm12, %xmm1
	mulps	%xmm7, %xmm1
	pxor	%xmm0, %xmm7
	pshufd	$229, %xmm7, %xmm2      # xmm2 = xmm7[1,1,2,3]
	movaps	%xmm10, %xmm0
	mulss	%xmm2, %xmm0
	subss	%xmm4, %xmm0
	movaps	%xmm11, %xmm4
	mulss	%xmm7, %xmm4
	subss	%xmm4, %xmm0
	movdqa	%xmm6, %xmm4
	unpcklps	%xmm7, %xmm4    # xmm4 = xmm4[0],xmm7[0],xmm4[1],xmm7[1]
	mulps	%xmm11, %xmm4
	subps	%xmm1, %xmm4
	shufps	$0, %xmm2, %xmm6        # xmm6 = xmm6[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm6      # xmm6 = xmm6[2,0],xmm2[2,3]
	mulps	%xmm5, %xmm6
	subps	%xmm6, %xmm4
	mulss	%xmm10, %xmm7
	subss	%xmm7, %xmm3
	mulss	%xmm2, %xmm11
	subss	%xmm11, %xmm3
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	128(%rsp), %xmm2        # 16-byte Reload
	movaps	320(%rsp), %xmm10       # 16-byte Reload
	shufps	$1, %xmm10, %xmm2       # xmm2 = xmm2[1,0],xmm10[0,0]
	shufps	$226, %xmm10, %xmm2     # xmm2 = xmm2[2,0],xmm10[2,3]
	mulps	%xmm1, %xmm2
	movaps	%xmm0, %xmm1
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	mulps	%xmm12, %xmm1
	addps	%xmm2, %xmm1
	movaps	%xmm4, %xmm2
	movaps	480(%rsp), %xmm7        # 16-byte Reload
	mulps	%xmm7, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm4, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm0, %xmm5
	shufps	$0, %xmm1, %xmm5        # xmm5 = xmm5[0,0],xmm1[0,0]
	shufps	$226, %xmm1, %xmm5      # xmm5 = xmm5[2,0],xmm1[2,3]
	movaps	496(%rsp), %xmm11       # 16-byte Reload
	movaps	%xmm11, %xmm6
	unpcklps	%xmm7, %xmm6    # xmm6 = xmm6[0],xmm7[0],xmm6[1],xmm7[1]
	mulps	%xmm5, %xmm6
	subps	%xmm6, %xmm2
	mulss	%xmm7, %xmm3
	mulss	%xmm9, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm11, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm0
	movaps	%xmm2, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 524(%rbx)
	movlps	%xmm1, 532(%rbx)
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	16(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	movss	32(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm4, %xmm5
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	20(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	movss	36(%r15), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm4, %xmm1
	movss	8(%r15), %xmm4          # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	24(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm4, %xmm6
	movss	40(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm6, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm0, %xmm4
	addss	%xmm1, %xmm4
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	16(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm1, %xmm6
	movss	32(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm6, %xmm5
	movss	4(%r14), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movss	20(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm1, %xmm6
	movss	36(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	addss	%xmm6, %xmm1
	movss	8(%r14), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	movss	24(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	addss	%xmm6, %xmm7
	movss	40(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm0, %xmm6
	addss	%xmm1, %xmm6
	addss	%xmm4, %xmm6
	divss	%xmm6, %xmm8
	movss	%xmm8, 544(%rbx)
.LBB5_67:
	cmpb	$0, 574(%rbx)
	je	.LBB5_69
# BB#68:
	movd	%eax, %xmm1
	movdqa	%xmm1, %xmm5
	movdqa	.LCPI5_2(%rip), %xmm7   # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm7, %xmm5
	pshufd	$232, %xmm13, %xmm3     # xmm3 = xmm13[0,2,2,3]
	movaps	464(%rsp), %xmm12       # 16-byte Reload
	movaps	%xmm12, %xmm6
	mulps	%xmm3, %xmm6
	pxor	%xmm7, %xmm3
	movaps	224(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm2
	mulss	%xmm1, %xmm2
	pshufd	$229, %xmm3, %xmm4      # xmm4 = xmm3[1,1,2,3]
	movaps	128(%rsp), %xmm9        # 16-byte Reload
	movaps	%xmm9, %xmm0
	mulss	%xmm4, %xmm0
	subss	%xmm2, %xmm0
	movaps	336(%rsp), %xmm11       # 16-byte Reload
	movaps	%xmm11, %xmm2
	mulss	%xmm3, %xmm2
	subss	%xmm2, %xmm0
	movdqa	%xmm5, %xmm2
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	mulps	%xmm11, %xmm2
	subps	%xmm6, %xmm2
	movaps	384(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm6
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	shufps	$0, %xmm4, %xmm5        # xmm5 = xmm5[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm5      # xmm5 = xmm5[2,0],xmm4[2,3]
	mulps	%xmm6, %xmm5
	subps	%xmm5, %xmm2
	mulss	%xmm10, %xmm1
	mulss	%xmm9, %xmm3
	subss	%xmm3, %xmm1
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm1
	movaps	%xmm9, %xmm3
	xorps	%xmm7, %xmm3
	xorps	%xmm11, %xmm7
	pshufd	$229, %xmm7, %xmm4      # xmm4 = xmm7[1,1,2,3]
	movaps	%xmm3, %xmm5
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	mulss	%xmm1, %xmm7
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	shufps	$0, %xmm4, %xmm5        # xmm5 = xmm5[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm5      # xmm5 = xmm5[2,0],xmm4[2,3]
	mulps	%xmm1, %xmm5
	movaps	%xmm0, %xmm1
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm12, %xmm1
	addps	%xmm5, %xmm1
	mulps	%xmm2, %xmm11
	subps	%xmm11, %xmm1
	movaps	%xmm2, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm0, %xmm6
	shufps	$0, %xmm5, %xmm6        # xmm6 = xmm6[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm6      # xmm6 = xmm6[2,0],xmm5[2,3]
	mulps	%xmm6, %xmm3
	subps	%xmm3, %xmm1
	mulss	%xmm8, %xmm5
	addss	%xmm7, %xmm5
	mulss	%xmm9, %xmm0
	subss	%xmm0, %xmm5
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm1, 584(%rbx)
	movlps	%xmm0, 592(%rbx)
.LBB5_69:
	addq	$608, %rsp              # imm = 0x260
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB5_70:
	xorps	%xmm0, %xmm0
                                        # implicit-def: %XMM12
                                        # implicit-def: %XMM6
.LBB5_71:                               # %_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_.exit
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movss	476(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm3, %xmm1
	ucomiss	%xmm1, %xmm7
	jbe	.LBB5_52
# BB#72:
	movb	$1, 574(%rbx)
	ucomiss	%xmm7, %xmm0
	movaps	%xmm5, %xmm2
	jbe	.LBB5_75
# BB#73:
	movss	.LCPI5_9(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	ucomiss	%xmm3, %xmm2
	movaps	%xmm5, %xmm2
	jbe	.LBB5_75
# BB#74:
	movaps	%xmm7, %xmm2
	subss	%xmm1, %xmm2
	subss	%xmm1, %xmm0
	divss	%xmm0, %xmm2
.LBB5_75:
	movss	%xmm2, 576(%rbx)
	subss	%xmm1, %xmm7
	movss	%xmm7, 552(%rbx)
	movaps	%xmm6, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	.LCPI5_3(%rip), %xmm1   # xmm1 = [nan,nan,nan,nan]
	andps	%xmm0, %xmm1
	ucomiss	.LCPI5_4(%rip), %xmm1
	jbe	.LBB5_81
# BB#76:
	movss	.LCPI5_5(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	shufps	$0, %xmm1, %xmm1        # xmm1 = xmm1[0,0,0,0]
	movaps	%xmm12, %xmm2
	xorps	%xmm1, %xmm2
	divss	%xmm0, %xmm2
	movss	496(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	divss	492(%rbx), %xmm3
	mulss	%xmm2, %xmm3
	mulss	%xmm0, %xmm3
	movaps	.LCPI5_3(%rip), %xmm2   # xmm2 = [nan,nan,nan,nan]
	andps	%xmm3, %xmm2
	orps	%xmm1, %xmm3
	cmpltss	%xmm4, %xmm12
	andps	%xmm12, %xmm2
	andnps	%xmm3, %xmm12
	orps	%xmm2, %xmm12
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm12, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB5_78
# BB#77:                                # %call.sqrt799
	movaps	%xmm1, %xmm0
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	movaps	%xmm6, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
.LBB5_78:                               # %.split798
	movaps	%xmm5, %xmm11
	movss	.LCPI5_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm12
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm6
	xorps	.LCPI5_2(%rip), %xmm12
	jmp	.LBB5_82
.LBB5_79:
	movaps	%xmm12, 256(%rsp)       # 16-byte Spill
	movaps	%xmm13, 272(%rsp)       # 16-byte Spill
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	movaps	%xmm6, 192(%rsp)        # 16-byte Spill
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	movaps	%xmm15, 288(%rsp)       # 16-byte Spill
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm11, 304(%rsp)       # 16-byte Spill
	movaps	%xmm9, 144(%rsp)        # 16-byte Spill
	movaps	%xmm14, 160(%rsp)       # 16-byte Spill
	movaps	48(%rsp), %xmm0         # 16-byte Reload
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	callq	atan2f
	movaps	%xmm0, %xmm1
	movss	492(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB5_85
# BB#80:
	callq	cosf
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	492(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	xorps	%xmm3, %xmm3
	jmp	.LBB5_90
.LBB5_81:
	movaps	%xmm5, %xmm11
.LBB5_82:                               # %_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3.exit
	movaps	%xmm6, %xmm1
	movaps	.LCPI5_2(%rip), %xmm7   # xmm7 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm7, %xmm8
	xorps	%xmm8, %xmm1
	movaps	%xmm12, %xmm3
	xorps	%xmm8, %xmm3
	movaps	544(%rsp), %xmm9        # 16-byte Reload
	movaps	%xmm9, %xmm0
	mulss	%xmm1, %xmm0
	movaps	528(%rsp), %xmm10       # 16-byte Reload
	movaps	%xmm10, %xmm2
	mulss	%xmm12, %xmm2
	subss	%xmm2, %xmm0
	pshufd	$229, %xmm1, %xmm4      # xmm4 = xmm1[1,1,2,3]
	movaps	512(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm2
	mulss	%xmm4, %xmm2
	subss	%xmm2, %xmm0
	movaps	%xmm7, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm3, %xmm2
	shufps	$0, %xmm4, %xmm2        # xmm2 = xmm2[0,0],xmm4[0,0]
	shufps	$226, %xmm4, %xmm2      # xmm2 = xmm2[2,0],xmm4[2,3]
	mulps	352(%rsp), %xmm2        # 16-byte Folded Reload
	mulps	%xmm7, %xmm6
	subps	%xmm6, %xmm2
	movaps	%xmm10, %xmm6
	shufps	$0, %xmm5, %xmm6        # xmm6 = xmm6[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm6      # xmm6 = xmm6[2,0],xmm5[2,3]
	unpcklps	%xmm1, %xmm3    # xmm3 = xmm3[0],xmm1[0],xmm3[1],xmm1[1]
	mulps	%xmm6, %xmm3
	subps	%xmm3, %xmm2
	mulss	%xmm1, %xmm5
	xorps	%xmm8, %xmm5
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm7, %xmm12
	addss	%xmm5, %xmm12
	movaps	%xmm12, %xmm1
	shufps	$0, %xmm9, %xmm1        # xmm1 = xmm1[0,0],xmm9[0,0]
	shufps	$226, %xmm9, %xmm1      # xmm1 = xmm1[2,0],xmm9[2,3]
	movaps	128(%rsp), %xmm3        # 16-byte Reload
	shufps	$1, %xmm0, %xmm3        # xmm3 = xmm3[1,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm3      # xmm3 = xmm3[2,0],xmm0[2,3]
	mulps	%xmm1, %xmm3
	movaps	%xmm12, %xmm1
	unpcklps	%xmm9, %xmm1    # xmm1 = xmm1[0],xmm9[0],xmm1[1],xmm9[1]
	movaps	320(%rsp), %xmm8        # 16-byte Reload
	movaps	%xmm8, %xmm4
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	mulps	%xmm1, %xmm4
	addps	%xmm3, %xmm4
	movaps	%xmm2, %xmm1
	movaps	480(%rsp), %xmm6        # 16-byte Reload
	mulps	%xmm6, %xmm1
	addps	%xmm4, %xmm1
	movaps	%xmm2, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm0, %xmm4
	shufps	$0, %xmm3, %xmm4        # xmm4 = xmm4[0,0],xmm3[0,0]
	shufps	$226, %xmm3, %xmm4      # xmm4 = xmm4[2,0],xmm3[2,3]
	movaps	496(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm5
	unpcklps	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0],xmm5[1],xmm6[1]
	mulps	%xmm4, %xmm5
	subps	%xmm5, %xmm1
	mulss	%xmm6, %xmm12
	mulss	%xmm9, %xmm3
	addss	%xmm12, %xmm3
	mulss	%xmm7, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm8, %xmm2
	subss	%xmm2, %xmm0
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	xorps	%xmm3, %xmm3
	xorps	%xmm4, %xmm4
	movss	%xmm0, %xmm4            # xmm4 = xmm0[0],xmm4[1,2,3]
	movlps	%xmm1, 508(%rbx)
	movlps	%xmm4, 516(%rbx)
	movups	%xmm3, 584(%rbx)
	movss	(%r15), %xmm3           # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	16(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	20(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	36(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	movss	8(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	24(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm0, %xmm3
	addss	%xmm6, %xmm3
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	16(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movss	32(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	movss	20(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	movss	36(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm6, %xmm5
	movss	8(%r14), %xmm6          # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	movss	24(%r14), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	movss	40(%r14), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	movss	.LCPI5_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm6, %xmm0
	movss	%xmm0, 540(%rbx)
	movaps	%xmm11, %xmm5
	jmp	.LBB5_52
.LBB5_83:
	movaps	%xmm12, 256(%rsp)       # 16-byte Spill
	movaps	%xmm13, 272(%rsp)       # 16-byte Spill
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	movaps	%xmm6, 192(%rsp)        # 16-byte Spill
	movaps	%xmm8, 208(%rsp)        # 16-byte Spill
	movaps	%xmm15, 288(%rsp)       # 16-byte Spill
	movaps	%xmm10, 16(%rsp)        # 16-byte Spill
	movaps	%xmm11, 304(%rsp)       # 16-byte Spill
	movaps	%xmm9, 144(%rsp)        # 16-byte Spill
	movaps	%xmm14, 160(%rsp)       # 16-byte Spill
	movaps	112(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm1, 96(%rsp)         # 16-byte Spill
	callq	atan2f
	movaps	%xmm0, %xmm1
	movss	496(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB5_88
# BB#84:
	callq	cosf
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	496(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm0, %xmm3
	xorps	%xmm0, %xmm0
	jmp	.LBB5_87
.LBB5_85:
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm2
	xorps	%xmm3, %xmm3
	ucomiss	%xmm1, %xmm2
	jbe	.LBB5_91
# BB#86:
	callq	cosf
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	492(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	xorps	%xmm3, %xmm3
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	xorps	.LCPI5_2(%rip), %xmm0
.LBB5_87:
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	jmp	.LBB5_90
.LBB5_88:
	movaps	96(%rsp), %xmm3         # 16-byte Reload
	movaps	.LCPI5_2(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm2
	xorps	%xmm4, %xmm4
	movaps	%xmm4, 48(%rsp)         # 16-byte Spill
	ucomiss	%xmm1, %xmm2
	jbe	.LBB5_92
# BB#89:
	callq	cosf
	movaps	%xmm0, 96(%rsp)         # 16-byte Spill
	movss	496(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm0, %xmm3
	xorps	.LCPI5_2(%rip), %xmm3
.LBB5_90:
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm14       # 16-byte Reload
	movaps	144(%rsp), %xmm9        # 16-byte Reload
	movaps	304(%rsp), %xmm11       # 16-byte Reload
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movaps	288(%rsp), %xmm15       # 16-byte Reload
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	208(%rsp), %xmm8        # 16-byte Reload
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	movaps	176(%rsp), %xmm4        # 16-byte Reload
	movaps	272(%rsp), %xmm13       # 16-byte Reload
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	256(%rsp), %xmm12       # 16-byte Reload
	jmp	.LBB5_45
.LBB5_91:
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm14       # 16-byte Reload
	movaps	144(%rsp), %xmm9        # 16-byte Reload
	movaps	304(%rsp), %xmm11       # 16-byte Reload
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movaps	288(%rsp), %xmm15       # 16-byte Reload
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	208(%rsp), %xmm8        # 16-byte Reload
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	movaps	176(%rsp), %xmm4        # 16-byte Reload
	movaps	272(%rsp), %xmm13       # 16-byte Reload
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	256(%rsp), %xmm12       # 16-byte Reload
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	jmp	.LBB5_45
.LBB5_92:
	movss	.LCPI5_1(%rip), %xmm5   # xmm5 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm14       # 16-byte Reload
	movaps	144(%rsp), %xmm9        # 16-byte Reload
	movaps	304(%rsp), %xmm11       # 16-byte Reload
	movaps	16(%rsp), %xmm10        # 16-byte Reload
	movaps	288(%rsp), %xmm15       # 16-byte Reload
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	208(%rsp), %xmm8        # 16-byte Reload
	movaps	192(%rsp), %xmm6        # 16-byte Reload
	movaps	176(%rsp), %xmm4        # 16-byte Reload
	movaps	272(%rsp), %xmm13       # 16-byte Reload
	movss	32(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	256(%rsp), %xmm12       # 16-byte Reload
	movaps	%xmm3, %xmm1
	jmp	.LBB5_44
.Lfunc_end5:
	.size	_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_, .Lfunc_end5-_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_
	.cfi_endproc

	.globl	_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E,@function
_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E: # @_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_startproc
# BB#0:
	movq	$6, (%rsi)
	retq
.Lfunc_end6:
	.size	_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E, .Lfunc_end6-_ZN21btConeTwistConstraint18getInfo1NonVirtualEPN17btTypedConstraint17btConstraintInfo1E
	.cfi_endproc

	.globl	_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E,@function
_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E: # @_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_startproc
# BB#0:
	movq	24(%rdi), %r8
	movq	32(%rdi), %r9
	leaq	8(%r8), %rdx
	addq	$280, %r8               # imm = 0x118
	leaq	8(%r9), %rcx
	addq	$280, %r9               # imm = 0x118
	jmp	_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_ # TAILCALL
.Lfunc_end7:
	.size	_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E, .Lfunc_end7-_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI8_1:
	.long	2139095039              # float 3.40282347E+38
	.text
	.globl	_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_,@function
_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_: # @_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi32:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi33:
	.cfi_def_cfa_offset 80
.Lcfi34:
	.cfi_offset %rbx, -40
.Lcfi35:
	.cfi_offset %r12, -32
.Lcfi36:
	.cfi_offset %r14, -24
.Lcfi37:
	.cfi_offset %r15, -16
	movq	%rcx, %rbx
	movq	%rdx, %r12
	movq	%rsi, %r14
	movq	%rdi, %r15
	movq	%r12, %rsi
	movq	%rbx, %rdx
	movq	%r8, %rcx
	movq	%r9, %r8
	callq	_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_
	movq	8(%r14), %rdx
	movl	$1065353216, (%rdx)     # imm = 0x3F800000
	movslq	40(%r14), %rax
	movl	$1065353216, 4(%rdx,%rax,4) # imm = 0x3F800000
	leal	(%rax,%rax), %ecx
	movslq	%ecx, %rcx
	movl	$1065353216, 8(%rdx,%rcx,4) # imm = 0x3F800000
	movss	396(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	400(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	404(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	16(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%r12), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm1, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm4, %xmm3
	movss	20(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm0, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm5, %xmm4
	addps	%xmm3, %xmm4
	movss	24(%r12), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	addps	%xmm4, %xmm3
	mulss	32(%r12), %xmm1
	mulss	36(%r12), %xmm0
	addss	%xmm1, %xmm0
	mulss	40(%r12), %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm3, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movlps	%xmm3, 8(%rsp)
	xorps	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	movss	%xmm2, %xmm0            # xmm0 = xmm2[0],xmm0[1,2,3]
	movlps	%xmm0, 16(%rsp)
	movq	16(%r14), %rdx
	movaps	.LCPI8_0(%rip), %xmm12  # xmm12 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm4, %xmm5
	xorps	%xmm12, %xmm5
	movl	$0, (%rdx)
	movss	%xmm2, 4(%rdx)
	xorps	%xmm12, %xmm2
	movss	%xmm5, 8(%rdx)
	movl	$0, 12(%rdx)
	movss	%xmm2, (%rdx,%rax,4)
	movl	$0, 4(%rdx,%rax,4)
	movss	%xmm3, 8(%rdx,%rax,4)
	xorps	%xmm12, %xmm3
	movl	$0, 12(%rdx,%rax,4)
	movss	%xmm4, (%rdx,%rcx,4)
	movss	%xmm3, 4(%rdx,%rcx,4)
	movq	$0, 8(%rdx,%rcx,4)
	movss	460(%r15), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	464(%r15), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	468(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	16(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rbx), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0],xmm6[1],xmm3[1]
	movaps	%xmm5, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm6, %xmm3
	movss	20(%rbx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm6, %xmm7    # xmm7 = xmm7[0],xmm6[0],xmm7[1],xmm6[1]
	movaps	%xmm4, %xmm6
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm7, %xmm6
	addps	%xmm3, %xmm6
	movss	24(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm7          # xmm7 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm7    # xmm7 = xmm7[0],xmm3[0],xmm7[1],xmm3[1]
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm7, %xmm3
	addps	%xmm6, %xmm3
	mulss	32(%rbx), %xmm5
	mulss	36(%rbx), %xmm4
	addss	%xmm5, %xmm4
	mulss	40(%rbx), %xmm2
	addss	%xmm4, %xmm2
	movaps	%xmm3, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movlps	%xmm3, 24(%rsp)
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm1, 32(%rsp)
	movq	32(%r14), %rcx
	movslq	40(%r14), %rax
	movaps	%xmm2, %xmm1
	xorps	%xmm12, %xmm1
	movl	$0, (%rcx)
	movss	%xmm1, 4(%rcx)
	movss	%xmm4, 8(%rcx)
	movl	$0, 12(%rcx)
	movaps	%xmm3, %xmm1
	xorps	%xmm12, %xmm1
	movss	%xmm2, (%rcx,%rax,4)
	movl	$0, 4(%rcx,%rax,4)
	movss	%xmm1, 8(%rcx,%rax,4)
	movl	$0, 12(%rcx,%rax,4)
	movaps	%xmm4, %xmm1
	xorps	%xmm12, %xmm1
	movss	%xmm1, (%rcx,%rax,8)
	movss	%xmm3, 4(%rcx,%rax,8)
	movq	$0, 8(%rcx,%rax,8)
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	4(%r14), %xmm1
	movq	48(%r14), %rsi
	movq	64(%r14), %rdx
	movq	72(%r14), %rcx
	movss	48(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm2
	subss	8(%rsp), %xmm2
	subss	48(%r12), %xmm2
	mulss	%xmm1, %xmm2
	movss	%xmm2, (%rsi)
	movl	$-8388609, (%rdx)       # imm = 0xFF7FFFFF
	movl	$2139095039, (%rcx)     # imm = 0x7F7FFFFF
	movss	52(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm2
	subss	12(%rsp), %xmm2
	subss	52(%r12), %xmm2
	mulss	%xmm1, %xmm2
	movss	%xmm2, (%rsi,%rax,4)
	movl	$-8388609, (%rdx,%rax,4) # imm = 0xFF7FFFFF
	movl	$2139095039, (%rcx,%rax,4) # imm = 0x7F7FFFFF
	movss	32(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	addss	56(%rbx), %xmm2
	subss	16(%rsp), %xmm2
	subss	56(%r12), %xmm2
	mulss	%xmm1, %xmm2
	movss	%xmm2, (%rsi,%rax,8)
	movl	$-8388609, (%rdx,%rax,8) # imm = 0xFF7FFFFF
	movl	$2139095039, (%rcx,%rax,8) # imm = 0x7F7FFFFF
	movl	40(%r14), %edi
	movslq	%edi, %r10
	leaq	(%r10,%r10,2), %r8
	cmpb	$0, 574(%r15)
	je	.LBB8_6
# BB#1:
	movq	16(%r14), %rax
	movq	32(%r14), %r9
	movss	504(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	492(%r15), %xmm1
	jbe	.LBB8_4
# BB#2:
	ucomiss	496(%r15), %xmm1
	jbe	.LBB8_4
# BB#3:
	movss	(%r12), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%r12), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	8(%r12), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movss	352(%r15), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	356(%r15), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	mulss	%xmm10, %xmm3
	movss	368(%r15), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm6
	mulss	%xmm13, %xmm6
	addss	%xmm3, %xmm6
	movss	384(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm11
	mulss	%xmm3, %xmm11
	addss	%xmm6, %xmm11
	mulss	%xmm8, %xmm2
	movss	372(%r15), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm5
	addss	%xmm2, %xmm5
	movss	388(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm9
	addss	%xmm5, %xmm9
	movss	16(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	20(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	24(%r12), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm6
	mulss	%xmm4, %xmm6
	movaps	%xmm13, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm6, %xmm1
	movaps	%xmm3, %xmm6
	mulss	%xmm5, %xmm6
	addss	%xmm1, %xmm6
	mulss	%xmm8, %xmm4
	mulss	%xmm7, %xmm0
	addss	%xmm4, %xmm0
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	movss	32(%r12), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	36(%r12), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	40(%r12), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm10
	mulss	%xmm1, %xmm13
	addss	%xmm10, %xmm13
	mulss	%xmm4, %xmm3
	addss	%xmm13, %xmm3
	mulss	%xmm0, %xmm8
	mulss	%xmm1, %xmm7
	addss	%xmm8, %xmm7
	mulss	%xmm4, %xmm2
	addss	%xmm7, %xmm2
	leaq	(,%r10,4), %rbx
	movss	%xmm11, (%rax,%r8,4)
	movslq	%r8d, %rcx
	movss	%xmm6, 4(%rax,%rcx,4)
	movss	%xmm3, 8(%rax,%rcx,4)
	shlq	$4, %r10
	movss	%xmm9, (%rax,%r10)
	leal	1(%rbx), %edx
	movslq	%edx, %rdx
	movss	%xmm5, (%rax,%rdx,4)
	leal	2(%rbx), %esi
	movslq	%esi, %rsi
	movss	%xmm2, (%rax,%rsi,4)
	movaps	%xmm11, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, (%r9,%r8,4)
	movaps	%xmm6, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, 4(%r9,%rcx,4)
	movaps	%xmm3, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, 8(%r9,%rcx,4)
	movaps	%xmm9, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, (%r9,%r10)
	movaps	%xmm5, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, (%r9,%rdx,4)
	movaps	%xmm2, %xmm0
	xorps	%xmm12, %xmm0
	movss	%xmm0, (%r9,%rsi,4)
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	484(%r15), %xmm0
	mulss	508(%r15), %xmm11
	mulss	512(%r15), %xmm6
	addss	%xmm11, %xmm6
	mulss	516(%r15), %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm0, %xmm3
	movq	48(%r14), %rsi
	movss	%xmm3, (%rsi,%r8,4)
	mulss	508(%r15), %xmm9
	mulss	512(%r15), %xmm5
	addss	%xmm9, %xmm5
	mulss	516(%r15), %xmm2
	addss	%xmm5, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsi,%r10)
	movq	64(%r14), %rdx
	movl	$-8388609, (%rdx,%r8,4) # imm = 0xFF7FFFFF
	movq	72(%r14), %rcx
	movl	$2139095039, (%rcx,%r8,4) # imm = 0x7F7FFFFF
	movl	$-8388609, (%rdx,%r10)  # imm = 0xFF7FFFFF
	movl	40(%r14), %edi
	movl	%ebx, %r8d
	jmp	.LBB8_5
.LBB8_4:
	movss	484(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	508(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	512(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	516(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	movslq	%r8d, %rbx
	movss	%xmm1, (%rax,%rbx,4)
	movss	%xmm2, 4(%rax,%rbx,4)
	movss	%xmm3, 8(%rax,%rbx,4)
	xorps	%xmm12, %xmm1
	movss	%xmm1, (%r9,%rbx,4)
	xorps	%xmm12, %xmm2
	movss	%xmm2, 4(%r9,%rbx,4)
	xorps	%xmm12, %xmm3
	movss	%xmm3, 8(%r9,%rbx,4)
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	480(%r15), %xmm0
	mulss	552(%r15), %xmm0
	movss	%xmm0, (%rsi,%rbx,4)
	movq	56(%r14), %rax
	movl	$0, (%rax,%rbx,4)
	movl	$0, (%rdx,%rbx,4)
.LBB8_5:                                # %.sink.split
	movl	$2139095039, (%rcx,%rbx,4) # imm = 0x7F7FFFFF
	addl	%r8d, %edi
	movl	%edi, %r8d
.LBB8_6:
	cmpb	$0, 573(%r15)
	je	.LBB8_14
# BB#7:
	movss	484(%r15), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	524(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	528(%r15), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	532(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm2
	mulss	%xmm0, %xmm3
	movq	16(%r14), %rdi
	movq	32(%r14), %rbx
	movslq	%r8d, %rax
	movss	%xmm1, (%rdi,%rax,4)
	movss	%xmm2, 4(%rdi,%rax,4)
	movss	%xmm3, 8(%rdi,%rax,4)
	xorps	%xmm12, %xmm1
	movss	%xmm1, (%rbx,%rax,4)
	xorps	%xmm12, %xmm2
	movss	%xmm2, 4(%rbx,%rax,4)
	xorps	%xmm12, %xmm3
	movss	%xmm3, 8(%rbx,%rax,4)
	movss	(%r14), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	480(%r15), %xmm0
	mulss	556(%r15), %xmm0
	movss	%xmm0, (%rsi,%rax,4)
	movq	56(%r14), %rsi
	movl	$0, (%rsi,%rax,4)
	movss	500(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm1
	jbe	.LBB8_11
# BB#8:
	movss	556(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB8_10
# BB#9:
	movl	$0, (%rdx,%rax,4)
	jmp	.LBB8_12
.LBB8_11:
	movl	$-8388609, (%rdx,%rax,4) # imm = 0xFF7FFFFF
.LBB8_12:
	movss	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	jmp	.LBB8_13
.LBB8_10:
	movl	$-8388609, (%rdx,%rax,4) # imm = 0xFF7FFFFF
	xorps	%xmm0, %xmm0
.LBB8_13:
	movss	%xmm0, (%rcx,%rax,4)
.LBB8_14:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_, .Lfunc_end8-_ZN21btConeTwistConstraint18getInfo2NonVirtualEPN17btTypedConstraint17btConstraintInfo2ERK11btTransformS5_RK11btMatrix3x3S8_
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI9_0:
	.long	872415232               # float 1.1920929E-7
.LCPI9_1:
	.long	1065353216              # float 1
.LCPI9_3:
	.long	1060439283              # float 0.707106769
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI9_4:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN21btConeTwistConstraint13buildJacobianEv
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint13buildJacobianEv,@function
_ZN21btConeTwistConstraint13buildJacobianEv: # @_ZN21btConeTwistConstraint13buildJacobianEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi38:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi39:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi40:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi41:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 56
	subq	$280, %rsp              # imm = 0x118
.Lcfi44:
	.cfi_def_cfa_offset 336
.Lcfi45:
	.cfi_offset %rbx, -56
.Lcfi46:
	.cfi_offset %r12, -48
.Lcfi47:
	.cfi_offset %r13, -40
.Lcfi48:
	.cfi_offset %r14, -32
.Lcfi49:
	.cfi_offset %r15, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	cmpb	$0, 575(%r14)
	je	.LBB9_17
# BB#1:
	movl	$0, 40(%r14)
	movl	$0, 568(%r14)
	movl	$0, 564(%r14)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 624(%r14)
	cmpb	$0, 572(%r14)
	jne	.LBB9_16
# BB#2:
	movss	396(%r14), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	400(%r14), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	404(%r14), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movq	24(%r14), %rax
	movq	32(%r14), %rcx
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	movaps	%xmm11, %xmm14
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	%xmm4, %xmm14
	movss	32(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm5    # xmm5 = xmm5[0],xmm4[0],xmm5[1],xmm4[1]
	movaps	%xmm10, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	movsd	56(%rax), %xmm8         # xmm8 = mem[0],zero
	mulss	44(%rax), %xmm11
	mulss	48(%rax), %xmm10
	movss	460(%r14), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	464(%r14), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movss	468(%r14), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	28(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movss	12(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movaps	%xmm7, %xmm0
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm5, %xmm0
	movss	32(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	movaps	%xmm4, %xmm5
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm1, %xmm5
	movsd	56(%rcx), %xmm9         # xmm9 = mem[0],zero
	movss	40(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	mulss	44(%rcx), %xmm7
	addss	%xmm1, %xmm7
	mulss	48(%rcx), %xmm4
	addss	%xmm7, %xmm4
	addss	64(%rcx), %xmm4
	movss	24(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm12   # xmm12 = xmm12[0],xmm1[0],xmm12[1],xmm1[1]
	movaps	%xmm6, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm12, %xmm7
	addps	%xmm14, %xmm7
	addps	%xmm2, %xmm7
	addps	%xmm8, %xmm7
	movss	24(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm13   # xmm13 = xmm13[0],xmm1[0],xmm13[1],xmm1[1]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm13, %xmm3
	addps	%xmm0, %xmm3
	addps	%xmm5, %xmm3
	addps	%xmm9, %xmm3
	mulss	40(%rax), %xmm6
	addss	%xmm11, %xmm6
	addss	%xmm10, %xmm6
	addss	64(%rax), %xmm6
	movaps	%xmm3, %xmm2
	subps	%xmm7, %xmm2
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm4, %xmm5
	subss	%xmm6, %xmm5
	movaps	%xmm2, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI9_0(%rip), %xmm0
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	movaps	%xmm4, 80(%rsp)         # 16-byte Spill
	movaps	%xmm7, 64(%rsp)         # 16-byte Spill
	jbe	.LBB9_6
# BB#3:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB9_5
# BB#4:                                 # %call.sqrt
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	movaps	%xmm2, 256(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	256(%rsp), %xmm2        # 16-byte Reload
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	movaps	96(%rsp), %xmm3         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB9_5:                                # %.split
	movss	.LCPI9_1(%rip), %xmm0   # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm5
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	%xmm0, %xmm2
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movlps	%xmm2, (%rsp)
	movaps	%xmm2, %xmm9
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	movlps	%xmm0, 8(%rsp)
	jmp	.LBB9_7
.LBB9_6:
	movq	$1065353216, (%rsp)     # imm = 0x3F800000
	movq	$0, 8(%rsp)
	movss	.LCPI9_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm9, %xmm9
	xorps	%xmm5, %xmm5
.LBB9_7:
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm0
	ucomiss	.LCPI9_3(%rip), %xmm0
	jbe	.LBB9_11
# BB#8:
	mulss	%xmm9, %xmm9
	mulss	%xmm5, %xmm5
	addss	%xmm9, %xmm5
	xorps	%xmm0, %xmm0
	sqrtss	%xmm5, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB9_10
# BB#9:                                 # %call.sqrt94
	movaps	%xmm5, %xmm0
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	movaps	96(%rsp), %xmm3         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
.LBB9_10:                               # %.split93
	movss	.LCPI9_1(%rip), %xmm1   # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movss	8(%rsp), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movaps	.LCPI9_4(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm2
	mulss	%xmm1, %xmm5
	mulss	4(%rsp), %xmm1
	movl	$0, 16(%rsp)
	movss	%xmm1, 24(%rsp)
	movss	(%rsp), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	xorps	%xmm8, %xmm1
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	jmp	.LBB9_14
.LBB9_11:
	mulss	%xmm2, %xmm2
	mulss	%xmm9, %xmm9
	addss	%xmm2, %xmm9
	xorps	%xmm0, %xmm0
	sqrtss	%xmm9, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB9_13
# BB#12:                                # %call.sqrt96
	movaps	%xmm9, %xmm0
	movaps	%xmm9, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm9         # 16-byte Reload
	movaps	64(%rsp), %xmm7         # 16-byte Reload
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	movaps	96(%rsp), %xmm3         # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
.LBB9_13:                               # %.split95
	movss	.LCPI9_1(%rip), %xmm2   # xmm2 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm2
	movss	4(%rsp), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm1
	movaps	.LCPI9_4(%rip), %xmm8   # xmm8 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm8, %xmm1
	mulss	%xmm2, %xmm9
	mulss	(%rsp), %xmm2
	movss	%xmm1, 16(%rsp)
	movl	$0, 24(%rsp)
	movss	8(%rsp), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm0, %xmm5
	xorps	%xmm8, %xmm5
	mulss	%xmm0, %xmm1
.LBB9_14:                               # %_Z13btPlaneSpace1RK9btVector3RS_S2_.exit
	movss	%xmm2, 20(%rsp)
	movl	$0, 28(%rsp)
	movss	%xmm5, 32(%rsp)
	movss	%xmm1, 36(%rsp)
	movss	%xmm9, 40(%rsp)
	movl	$0, 44(%rsp)
	leaq	96(%r14), %rbx
	xorl	%r15d, %r15d
	leaq	240(%rsp), %r13
	leaq	224(%rsp), %r12
	.p2align	4, 0x90
.LBB9_15:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rcx
	movl	8(%rcx), %eax
	movl	%eax, 176(%rsp)
	movl	24(%rcx), %eax
	movl	%eax, 180(%rsp)
	movl	40(%rcx), %eax
	movl	%eax, 184(%rsp)
	movl	$0, 188(%rsp)
	movl	12(%rcx), %eax
	movl	%eax, 192(%rsp)
	movl	28(%rcx), %eax
	movl	%eax, 196(%rsp)
	movl	44(%rcx), %eax
	movl	%eax, 200(%rsp)
	movl	$0, 204(%rsp)
	movl	16(%rcx), %eax
	movl	%eax, 208(%rsp)
	movl	32(%rcx), %eax
	movl	%eax, 212(%rsp)
	movl	48(%rcx), %eax
	movl	%eax, 216(%rsp)
	movl	$0, 220(%rsp)
	movq	32(%r14), %rax
	movl	8(%rax), %edx
	movl	%edx, 128(%rsp)
	movl	24(%rax), %edx
	movl	%edx, 132(%rsp)
	movl	40(%rax), %edx
	movl	%edx, 136(%rsp)
	movl	$0, 140(%rsp)
	movl	12(%rax), %edx
	movl	%edx, 144(%rsp)
	movl	28(%rax), %edx
	movl	%edx, 148(%rsp)
	movl	44(%rax), %edx
	movl	%edx, 152(%rsp)
	movl	$0, 156(%rsp)
	movl	16(%rax), %edx
	movl	%edx, 160(%rsp)
	movl	32(%rax), %edx
	movl	%edx, 164(%rsp)
	movl	48(%rax), %edx
	movl	%edx, 168(%rsp)
	movl	$0, 172(%rsp)
	movsd	56(%rcx), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm7, %xmm1
	subps	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	subss	64(%rcx), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 240(%rsp)
	movlps	%xmm2, 248(%rsp)
	movsd	56(%rax), %xmm0         # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm1
	subps	%xmm0, %xmm1
	movaps	%xmm4, %xmm0
	subss	64(%rax), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 224(%rsp)
	movlps	%xmm2, 232(%rsp)
	leaq	(%rsp,%r15), %r9
	movq	24(%r14), %rbp
	movss	360(%rbp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addq	$428, %rbp              # imm = 0x1AC
	movss	360(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	addq	$428, %rax              # imm = 0x1AC
	movq	%rbx, %rdi
	leaq	176(%rsp), %rsi
	leaq	128(%rsp), %rdx
	movq	%r13, %rcx
	movq	%r12, %r8
	pushq	%rax
.Lcfi51:
	.cfi_adjust_cfa_offset 8
	pushq	%rbp
.Lcfi52:
	.cfi_adjust_cfa_offset 8
	callq	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	movaps	80(%rsp), %xmm7         # 16-byte Reload
	movaps	96(%rsp), %xmm4         # 16-byte Reload
	movaps	112(%rsp), %xmm3        # 16-byte Reload
	movaps	128(%rsp), %xmm6        # 16-byte Reload
	addq	$16, %rsp
.Lcfi53:
	.cfi_adjust_cfa_offset -16
	addq	$16, %r15
	addq	$84, %rbx
	cmpq	$48, %r15
	jne	.LBB9_15
.LBB9_16:
	movq	24(%r14), %rcx
	movq	32(%r14), %r8
	leaq	8(%rcx), %rsi
	addq	$280, %rcx              # imm = 0x118
	leaq	8(%r8), %rdx
	addq	$280, %r8               # imm = 0x118
	movq	%r14, %rdi
	callq	_ZN21btConeTwistConstraint14calcAngleInfo2ERK11btTransformS2_RK11btMatrix3x3S5_
.LBB9_17:
	addq	$280, %rsp              # imm = 0x118
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	_ZN21btConeTwistConstraint13buildJacobianEv, .Lfunc_end9-_ZN21btConeTwistConstraint13buildJacobianEv
	.cfi_endproc

	.section	.text._ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,"axG",@progbits,_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,comdat
	.weak	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.p2align	4, 0x90
	.type	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f,@function
_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f: # @_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_startproc
# BB#0:
	movq	16(%rsp), %r10
	movq	8(%rsp), %rax
	movups	(%r9), %xmm2
	movups	%xmm2, (%rdi)
	movss	(%rcx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	movss	4(%rcx), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movss	8(%rdi), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm11, %xmm7
	movss	8(%rcx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	movss	(%rdi), %xmm9           # xmm9 = mem[0],zero,zero,zero
	movss	4(%rdi), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm7
	mulss	%xmm9, %xmm5
	movaps	%xmm11, %xmm4
	mulss	%xmm2, %xmm4
	subss	%xmm4, %xmm5
	mulss	%xmm10, %xmm2
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm2
	movaps	%xmm7, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	16(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	(%rsi), %xmm6           # xmm6 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm3          # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm6    # xmm6 = xmm6[0],xmm8[0],xmm6[1],xmm8[1]
	mulps	%xmm4, %xmm6
	movss	20(%rsi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm6, %xmm4
	movaps	%xmm2, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rsi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	8(%rsi), %xmm12         # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	mulps	%xmm3, %xmm12
	addps	%xmm4, %xmm12
	mulss	32(%rsi), %xmm7
	mulss	36(%rsi), %xmm5
	addss	%xmm7, %xmm5
	mulss	40(%rsi), %xmm2
	addss	%xmm5, %xmm2
	xorps	%xmm8, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm2, %xmm3            # xmm3 = xmm2[0],xmm3[1,2,3]
	movlps	%xmm12, 16(%rdi)
	movlps	%xmm3, 24(%rdi)
	movss	(%r8), %xmm3            # xmm3 = mem[0],zero,zero,zero
	movss	4(%r8), %xmm4           # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm5
	mulss	%xmm11, %xmm5
	movss	8(%r8), %xmm7           # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm6
	mulss	%xmm10, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm9, %xmm7
	mulss	%xmm3, %xmm11
	subss	%xmm7, %xmm11
	mulss	%xmm10, %xmm3
	mulss	%xmm9, %xmm4
	subss	%xmm3, %xmm4
	movss	16(%rdx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	(%rdx), %xmm5           # xmm5 = mem[0],zero,zero,zero
	movss	4(%rdx), %xmm9          # xmm9 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	movaps	%xmm6, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm5, %xmm3
	movss	20(%rdx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm9    # xmm9 = xmm9[0],xmm5[0],xmm9[1],xmm5[1]
	movaps	%xmm11, %xmm7
	shufps	$224, %xmm7, %xmm7      # xmm7 = xmm7[0,0,2,3]
	mulps	%xmm9, %xmm7
	addps	%xmm3, %xmm7
	movaps	%xmm4, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	24(%rdx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movss	8(%rdx), %xmm5          # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	mulps	%xmm3, %xmm5
	addps	%xmm7, %xmm5
	mulss	32(%rdx), %xmm6
	mulss	36(%rdx), %xmm11
	addss	%xmm6, %xmm11
	mulss	40(%rdx), %xmm4
	addss	%xmm11, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	movlps	%xmm5, 32(%rdi)
	movlps	%xmm3, 40(%rdi)
	movsd	(%rax), %xmm9           # xmm9 = mem[0],zero
	mulps	%xmm12, %xmm9
	mulss	8(%rax), %xmm2
	xorps	%xmm6, %xmm6
	movss	%xmm2, %xmm6            # xmm6 = xmm2[0],xmm6[1,2,3]
	movlps	%xmm9, 48(%rdi)
	movlps	%xmm6, 56(%rdi)
	movsd	(%r10), %xmm6           # xmm6 = mem[0],zero
	mulps	%xmm5, %xmm6
	mulss	8(%r10), %xmm4
	movaps	%xmm6, %xmm7
	movlps	%xmm6, 64(%rdi)
	mulss	%xmm5, %xmm6
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movss	%xmm4, %xmm8            # xmm8 = xmm4[0],xmm8[1,2,3]
	movlps	%xmm8, 72(%rdi)
	movss	16(%rdi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	20(%rdi), %xmm9
	addss	%xmm3, %xmm9
	mulss	24(%rdi), %xmm2
	addss	%xmm9, %xmm2
	addss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm5, %xmm7
	addss	%xmm6, %xmm7
	mulss	40(%rdi), %xmm4
	addss	%xmm7, %xmm4
	addss	%xmm2, %xmm4
	movss	%xmm4, 80(%rdi)
	retq
.Lfunc_end10:
	.size	_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f, .Lfunc_end10-_ZN15btJacobianEntryC2ERK11btMatrix3x3S2_RK9btVector3S5_S5_S5_fS5_f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI11_0:
	.long	1065353216              # float 1
.LCPI11_1:
	.long	3197737370              # float -0.300000012
.LCPI11_2:
	.long	1073741824              # float 2
.LCPI11_4:
	.long	872415232               # float 1.1920929E-7
.LCPI11_5:
	.long	2147483648              # float -0
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI11_3:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f,@function
_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f: # @_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi54:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi55:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi56:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi57:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi58:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi59:
	.cfi_def_cfa_offset 56
	subq	$744, %rsp              # imm = 0x2E8
.Lcfi60:
	.cfi_def_cfa_offset 800
.Lcfi61:
	.cfi_offset %rbx, -56
.Lcfi62:
	.cfi_offset %r12, -48
.Lcfi63:
	.cfi_offset %r13, -40
.Lcfi64:
	.cfi_offset %r14, -32
.Lcfi65:
	.cfi_offset %r15, -24
.Lcfi66:
	.cfi_offset %rbp, -16
	movaps	%xmm0, %xmm11
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	cmpb	$0, 575(%rbx)
	je	.LBB11_68
# BB#1:
	cmpb	$0, 572(%rbx)
	movss	%xmm11, 12(%rsp)        # 4-byte Spill
	jne	.LBB11_10
# BB#2:
	movss	396(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	400(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	404(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rax
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm3
	movss	28(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	32(%rcx), %xmm13        # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm13
	addss	%xmm4, %xmm13
	movss	60(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	addss	%xmm9, %xmm13
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	8(%rcx), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	movss	40(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm2, %xmm6
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	44(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0],xmm2[1],xmm5[1]
	mulps	%xmm1, %xmm2
	addps	%xmm6, %xmm2
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	movss	16(%rcx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movss	48(%rcx), %xmm14        # xmm14 = mem[0],zero,zero,zero
	unpcklps	%xmm1, %xmm14   # xmm14 = xmm14[0],xmm1[0],xmm14[1],xmm1[1]
	mulps	%xmm0, %xmm14
	addps	%xmm2, %xmm14
	movss	56(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	64(%rcx), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	addps	%xmm5, %xmm14
	movss	460(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	464(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	468(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	movss	28(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm0, %xmm4
	movss	32(%rax), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	addss	%xmm4, %xmm0
	movss	60(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	addss	%xmm8, %xmm0
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	movss	8(%rax), %xmm7          # xmm7 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movss	40(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm7, %xmm3    # xmm3 = xmm3[0],xmm7[0],xmm3[1],xmm7[1]
	mulps	%xmm6, %xmm3
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movss	44(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	mulps	%xmm1, %xmm6
	addps	%xmm3, %xmm6
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	16(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	48(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm1    # xmm1 = xmm1[0],xmm3[0],xmm1[1],xmm3[1]
	mulps	%xmm2, %xmm1
	addps	%xmm6, %xmm1
	movss	56(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	64(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm2    # xmm2 = xmm2[0],xmm3[0],xmm2[1],xmm3[1]
	addps	%xmm2, %xmm1
	movaps	%xmm13, %xmm12
	subss	%xmm9, %xmm13
	movaps	%xmm14, %xmm15
	subps	%xmm5, %xmm15
	movq	72(%r15), %rax
	testq	%rax, %rax
	je	.LBB11_3
# BB#4:
	movsd	328(%rax), %xmm4        # xmm4 = mem[0],zero
	movsd	(%r15), %xmm9           # xmm9 = mem[0],zero
	addps	%xmm4, %xmm9
	movss	336(%rax), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	344(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	addss	8(%r15), %xmm10
	addss	16(%r15), %xmm4
	movsd	348(%rax), %xmm6        # xmm6 = mem[0],zero
	movsd	20(%r15), %xmm7         # xmm7 = mem[0],zero
	addps	%xmm6, %xmm7
	movss	%xmm8, 16(%rsp)         # 4-byte Spill
	movaps	%xmm15, %xmm8
	mulps	%xmm7, %xmm8
	movaps	%xmm13, %xmm6
	unpcklps	%xmm15, %xmm6   # xmm6 = xmm6[0],xmm15[0],xmm6[1],xmm15[1]
	movaps	%xmm15, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm7, %xmm3
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	movaps	%xmm13, %xmm5
	mulss	%xmm4, %xmm5
	shufps	$0, %xmm7, %xmm4        # xmm4 = xmm4[0,0],xmm7[0,0]
	shufps	$226, %xmm7, %xmm4      # xmm4 = xmm4[2,0],xmm7[2,3]
	mulps	%xmm6, %xmm4
	movaps	%xmm8, %xmm6
	movss	16(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	subps	%xmm4, %xmm6
	subss	%xmm3, %xmm5
	addps	%xmm9, %xmm6
	addss	%xmm10, %xmm5
	xorps	%xmm10, %xmm10
	movss	%xmm5, %xmm10           # xmm10 = xmm5[0],xmm10[1,2,3]
	jmp	.LBB11_5
.LBB11_3:
	xorps	%xmm6, %xmm6
	xorps	%xmm10, %xmm10
.LBB11_5:                               # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit
	movaps	%xmm0, %xmm4
	subss	%xmm8, %xmm4
	movaps	%xmm1, %xmm3
	subps	%xmm2, %xmm3
	movq	72(%r14), %rax
	testq	%rax, %rax
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movaps	%xmm4, 128(%rsp)        # 16-byte Spill
	je	.LBB11_6
# BB#7:
	movsd	328(%rax), %xmm2        # xmm2 = mem[0],zero
	movsd	(%r14), %xmm8           # xmm8 = mem[0],zero
	addps	%xmm2, %xmm8
	movss	336(%rax), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	344(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	addss	8(%r14), %xmm9
	addss	16(%r14), %xmm5
	movsd	348(%rax), %xmm2        # xmm2 = mem[0],zero
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	movsd	20(%r14), %xmm6         # xmm6 = mem[0],zero
	addps	%xmm2, %xmm6
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	mulps	%xmm6, %xmm2
	movaps	%xmm4, %xmm7
	unpcklps	16(%rsp), %xmm7 # 16-byte Folded Reload
                                        # xmm7 = xmm7[0],mem[0],xmm7[1],mem[1]
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	mulss	%xmm6, %xmm3
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm5, %xmm4
	shufps	$0, %xmm6, %xmm5        # xmm5 = xmm5[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm5      # xmm5 = xmm5[2,0],xmm6[2,3]
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	mulps	%xmm7, %xmm5
	subps	%xmm5, %xmm2
	subss	%xmm3, %xmm4
	addps	%xmm8, %xmm2
	addss	%xmm9, %xmm4
	xorps	%xmm3, %xmm3
	movss	%xmm4, %xmm3            # xmm3 = xmm4[0],xmm3[1,2,3]
	jmp	.LBB11_8
.LBB11_6:                               # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit._ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit515_crit_edge
	xorps	%xmm2, %xmm2
	xorps	%xmm3, %xmm3
.LBB11_8:                               # %_ZNK12btSolverBody31getVelocityInLocalPointObsoleteERK9btVector3RS0_.exit515
	movaps	%xmm6, %xmm4
	subss	%xmm2, %xmm4
	movaps	%xmm4, 160(%rsp)        # 16-byte Spill
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	subss	%xmm2, %xmm6
	subss	%xmm3, %xmm10
	movaps	%xmm1, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm14, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	subss	%xmm2, %xmm3
	movaps	%xmm3, 240(%rsp)        # 16-byte Spill
	subss	%xmm0, %xmm12
	subss	%xmm1, %xmm14
	movaps	%xmm15, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 224(%rsp)        # 16-byte Spill
	movaps	16(%rsp), %xmm0         # 16-byte Reload
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm0, 192(%rsp)        # 16-byte Spill
	xorl	%eax, %eax
	movaps	%xmm13, 48(%rsp)        # 16-byte Spill
	movaps	%xmm14, 96(%rsp)        # 16-byte Spill
	movaps	%xmm6, 80(%rsp)         # 16-byte Spill
	movaps	%xmm10, 112(%rsp)       # 16-byte Spill
	.p2align	4, 0x90
.LBB11_9:                               # =>This Inner Loop Header: Depth=1
	movss	.LCPI11_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	176(%rbx,%rax), %xmm2
	movss	96(%rbx,%rax), %xmm3    # xmm3 = mem[0],zero,zero,zero
	movss	100(%rbx,%rax), %xmm4   # xmm4 = mem[0],zero,zero,zero
	movaps	160(%rsp), %xmm5        # 16-byte Reload
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm6
	movss	104(%rbx,%rax), %xmm0   # xmm0 = mem[0],zero,zero,zero
	movaps	112(%rsp), %xmm7        # 16-byte Reload
	mulss	%xmm0, %xmm7
	addss	%xmm5, %xmm6
	mulss	240(%rsp), %xmm3        # 16-byte Folded Reload
	movaps	%xmm12, %xmm8
	mulss	%xmm8, %xmm4
	mulss	%xmm14, %xmm0
	addss	%xmm3, %xmm4
	addss	%xmm4, %xmm0
	mulss	.LCPI11_1(%rip), %xmm0
	divss	%xmm11, %xmm0
	addss	%xmm6, %xmm7
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm7
	movss	40(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	subss	%xmm7, %xmm0
	addss	%xmm0, %xmm2
	movss	%xmm2, 40(%rbx)
	movss	104(%rbx,%rax), %xmm9   # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm2
	mulss	%xmm9, %xmm2
	movss	96(%rbx,%rax), %xmm11   # xmm11 = mem[0],zero,zero,zero
	movss	100(%rbx,%rax), %xmm10  # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm4
	mulss	%xmm10, %xmm4
	movaps	%xmm15, %xmm3
	subss	%xmm4, %xmm2
	mulss	%xmm11, %xmm3
	movaps	224(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm4
	mulss	%xmm9, %xmm4
	mulss	%xmm10, %xmm5
	movaps	%xmm13, %xmm1
	subss	%xmm4, %xmm3
	mulss	%xmm11, %xmm1
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movaps	%xmm12, %xmm6
	mulss	%xmm9, %xmm6
	movaps	16(%rsp), %xmm7         # 16-byte Reload
	movaps	%xmm7, %xmm14
	mulss	%xmm10, %xmm14
	subss	%xmm1, %xmm5
	mulss	%xmm11, %xmm7
	movaps	192(%rsp), %xmm1        # 16-byte Reload
	movaps	%xmm1, %xmm4
	mulss	%xmm9, %xmm4
	mulss	%xmm10, %xmm1
	movaps	%xmm15, %xmm13
	subss	%xmm14, %xmm6
	mulss	%xmm11, %xmm12
	movq	24(%rbx), %rcx
	movss	360(%rcx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm7
	mulss	%xmm14, %xmm11
	mulss	%xmm14, %xmm10
	mulss	%xmm14, %xmm9
	subss	%xmm12, %xmm1
	movss	280(%rcx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm14
	movss	284(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm14, %xmm4
	movss	288(%rcx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm15
	addss	%xmm4, %xmm15
	movss	296(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	movss	300(%rcx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm12
	addss	%xmm4, %xmm12
	movss	304(%rcx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm14
	addss	%xmm12, %xmm14
	movaps	%xmm8, %xmm12
	mulss	312(%rcx), %xmm2
	mulss	316(%rcx), %xmm3
	addss	%xmm2, %xmm3
	mulss	320(%rcx), %xmm5
	mulss	%xmm0, %xmm11
	addss	(%r15), %xmm11
	movss	%xmm11, (%r15)
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm5
	mulss	%xmm0, %xmm10
	addss	4(%r15), %xmm10
	movss	%xmm10, 4(%r15)
	mulss	%xmm0, %xmm9
	addss	8(%r15), %xmm9
	movss	%xmm9, 8(%r15)
	movss	32(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm15, %xmm2
	movaps	%xmm13, %xmm15
	movss	36(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	mulss	%xmm14, %xmm3
	movaps	96(%rsp), %xmm14        # 16-byte Reload
	movaps	48(%rsp), %xmm13        # 16-byte Reload
	movss	40(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	%xmm5, %xmm4
	addss	16(%r15), %xmm2
	movss	%xmm2, 16(%r15)
	addss	20(%r15), %xmm3
	movss	%xmm3, 20(%r15)
	addss	24(%r15), %xmm4
	movss	%xmm4, 24(%r15)
	movq	32(%rbx), %rcx
	movss	280(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	movss	284(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	movss	288(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	mulss	%xmm1, %xmm2
	movss	296(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm4, %xmm2
	movss	300(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm4
	addss	%xmm3, %xmm4
	movss	304(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	movss	360(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	312(%rcx), %xmm6
	mulss	316(%rcx), %xmm7
	addss	%xmm6, %xmm7
	movss	96(%rbx,%rax), %xmm5    # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	mulss	320(%rcx), %xmm1
	addss	%xmm7, %xmm1
	movss	(%r14), %xmm6           # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	subss	%xmm5, %xmm6
	movss	100(%rbx,%rax), %xmm5   # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	mulss	104(%rbx,%rax), %xmm4
	mulss	%xmm0, %xmm5
	movss	%xmm6, (%r14)
	movss	4(%r14), %xmm6          # xmm6 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm6
	mulss	%xmm0, %xmm4
	movss	%xmm6, 4(%r14)
	movss	8(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm5
	movss	%xmm5, 8(%r14)
	movss	32(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	%xmm2, %xmm4
	movss	36(%r14), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	%xmm3, %xmm2
	mulss	40(%r14), %xmm0
	mulss	%xmm1, %xmm0
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 16(%r14)
	movss	20(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm2, %xmm1
	movss	%xmm1, 20(%r14)
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r14)
	addq	$84, %rax
	cmpq	$252, %rax
	jne	.LBB11_9
.LBB11_10:                              # %.loopexit
	cmpb	$0, 600(%rbx)
	je	.LBB11_43
# BB#11:
	movq	24(%rbx), %rax
	movups	8(%rax), %xmm0
	movaps	%xmm0, 672(%rsp)
	movups	24(%rax), %xmm0
	movaps	%xmm0, 688(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, 704(%rsp)
	movups	56(%rax), %xmm0
	movaps	%xmm0, 720(%rsp)
	movq	32(%rbx), %rax
	movups	8(%rax), %xmm0
	movaps	%xmm0, 608(%rsp)
	movups	24(%rax), %xmm0
	movaps	%xmm0, 624(%rsp)
	movups	40(%rax), %xmm0
	movaps	%xmm0, 640(%rsp)
	movups	56(%rax), %xmm0
	movaps	%xmm0, 656(%rsp)
	movq	72(%r15), %rax
	testq	%rax, %rax
	je	.LBB11_13
# BB#12:
	movsd	344(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	16(%r15), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	352(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	24(%r15), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 288(%rsp)
	movlps	%xmm2, 296(%rsp)
	jmp	.LBB11_14
.LBB11_43:
	movss	488(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	leaq	72(%r15), %rbp
	ucomiss	.LCPI11_4(%rip), %xmm0
	jbe	.LBB11_54
# BB#44:
	movq	(%rbp), %rax
	xorps	%xmm8, %xmm8
	testq	%rax, %rax
	xorps	%xmm0, %xmm0
	xorps	%xmm1, %xmm1
	je	.LBB11_46
# BB#45:
	movsd	344(%rax), %xmm1        # xmm1 = mem[0],zero
	movsd	16(%r15), %xmm0         # xmm0 = mem[0],zero
	addps	%xmm1, %xmm0
	movss	352(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	24(%r15), %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
.LBB11_46:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit266
	movq	72(%r14), %rax
	testq	%rax, %rax
	xorps	%xmm12, %xmm12
	je	.LBB11_48
# BB#47:
	movsd	344(%rax), %xmm2        # xmm2 = mem[0],zero
	movsd	16(%r14), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm2, %xmm8
	movss	352(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	24(%r14), %xmm2
	xorps	%xmm12, %xmm12
	movss	%xmm2, %xmm12           # xmm12 = xmm2[0],xmm12[1,2,3]
.LBB11_48:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit258
	movaps	%xmm8, %xmm9
	subss	%xmm0, %xmm9
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	subss	%xmm0, %xmm8
	subss	%xmm1, %xmm12
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm8, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI11_4(%rip), %xmm0
	jbe	.LBB11_54
# BB#49:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB11_51
# BB#50:                                # %call.sqrt1355
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	movaps	%xmm12, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm12        # 16-byte Reload
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB11_51:                              # %.split1354
	movss	.LCPI11_0(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm0
	divss	%xmm1, %xmm0
	movaps	%xmm9, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm8, %xmm2
	mulss	%xmm0, %xmm2
	mulss	%xmm12, %xmm0
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rax
	movss	280(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	296(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	312(%rcx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	300(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	316(%rcx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	movss	288(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	304(%rcx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	320(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm5, %xmm3
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm0, %xmm3
	addss	%xmm6, %xmm3
	movss	280(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	movss	296(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	movss	312(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	movss	284(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	movss	300(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm6
	addss	%xmm5, %xmm6
	movss	316(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	addss	%xmm6, %xmm5
	movss	288(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	movss	304(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm7
	addss	%xmm6, %xmm7
	movss	320(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	addss	%xmm7, %xmm6
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	addss	%xmm4, %xmm5
	mulss	%xmm0, %xmm6
	addss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	movaps	%xmm10, %xmm3
	divss	%xmm6, %xmm3
	mulss	488(%rbx), %xmm3
	mulss	%xmm3, %xmm9
	mulss	%xmm3, %xmm8
	mulss	%xmm12, %xmm3
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm8, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB11_53
# BB#52:                                # %call.sqrt1356
	movaps	%xmm1, %xmm0
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	movss	%xmm3, 48(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	48(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	.LCPI11_0(%rip), %xmm10 # xmm10 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
.LBB11_53:                              # %.split1354.split
	divss	%xmm0, %xmm10
	mulss	%xmm10, %xmm9
	mulss	%xmm10, %xmm8
	mulss	%xmm3, %xmm10
	movq	24(%rbx), %rax
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm3, %xmm2
	movss	312(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	movss	316(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm3
	addss	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	mulss	%xmm0, %xmm4
	movss	(%r15), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r15)
	movss	4(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r15)
	addss	8(%r15), %xmm4
	movss	%xmm4, 8(%r15)
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	40(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm6
	addss	16(%r15), %xmm4
	movss	%xmm4, 16(%r15)
	addss	20(%r15), %xmm5
	movss	%xmm5, 20(%r15)
	addss	24(%r15), %xmm6
	movss	%xmm6, 24(%r15)
	movq	32(%rbx), %rax
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm2
	addss	%xmm3, %xmm2
	mulss	312(%rax), %xmm9
	mulss	316(%rax), %xmm8
	addss	%xmm9, %xmm8
	mulss	320(%rax), %xmm10
	addss	%xmm8, %xmm10
	movss	.LCPI11_5(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, (%r14)
	movss	4(%r14), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, 4(%r14)
	addss	8(%r14), %xmm3
	movss	%xmm3, 8(%r14)
	movss	32(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	40(%r14), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm4
	mulss	%xmm10, %xmm0
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	movss	%xmm1, 16(%r14)
	movss	20(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 20(%r14)
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r14)
	jmp	.LBB11_54
.LBB11_13:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 288(%rsp)
.LBB11_14:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit531
	movq	72(%r14), %rax
	testq	%rax, %rax
	je	.LBB11_16
# BB#15:
	movsd	344(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	16(%r14), %xmm1         # xmm1 = mem[0],zero
	addps	%xmm0, %xmm1
	movss	352(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	24(%r14), %xmm0
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm1, 272(%rsp)
	movlps	%xmm2, 280(%rsp)
	jmp	.LBB11_17
.LBB11_16:
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 272(%rsp)
.LBB11_17:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit523
	movl	$1065353216, 416(%rsp)  # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 420(%rsp)
	movl	$1065353216, 436(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 440(%rsp)
	movl	$1065353216, 456(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 460(%rsp)
	movl	$0, 476(%rsp)
	movaps	%xmm0, 256(%rsp)
	leaq	672(%rsp), %r13
	leaq	256(%rsp), %rbp
	leaq	288(%rsp), %rdx
	leaq	416(%rsp), %rcx
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movaps	%xmm11, %xmm0
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	movl	$1065353216, 352(%rsp)  # imm = 0x3F800000
	xorps	%xmm0, %xmm0
	movups	%xmm0, 356(%rsp)
	movl	$1065353216, 372(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 376(%rsp)
	movl	$1065353216, 392(%rsp)  # imm = 0x3F800000
	movups	%xmm0, 396(%rsp)
	movl	$0, 412(%rsp)
	leaq	608(%rsp), %r12
	leaq	272(%rsp), %rdx
	leaq	352(%rsp), %rcx
	movq	%r12, %rdi
	movq	%rbp, %rsi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	movss	604(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	movss	608(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	612(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	616(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	.LCPI11_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm6
	movaps	%xmm3, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm11, %xmm4
	mulss	%xmm6, %xmm4
	mulss	%xmm9, %xmm6
	movaps	%xmm2, %xmm0
	mulss	%xmm1, %xmm0
	movaps	%xmm2, %xmm7
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm2
	mulss	%xmm3, %xmm1
	movaps	%xmm3, %xmm8
	mulss	%xmm4, %xmm8
	mulss	%xmm6, %xmm3
	movaps	%xmm3, %xmm5
	mulss	%xmm11, %xmm4
	mulss	%xmm6, %xmm11
	mulss	%xmm9, %xmm6
	movaps	%xmm4, %xmm3
	addss	%xmm6, %xmm3
	movss	.LCPI11_0(%rip), %xmm9  # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm10
	subss	%xmm3, %xmm10
	movaps	%xmm8, %xmm15
	subss	%xmm2, %xmm15
	movaps	%xmm5, %xmm14
	addss	%xmm7, %xmm14
	addss	%xmm2, %xmm8
	addss	%xmm1, %xmm6
	movaps	%xmm9, %xmm3
	subss	%xmm6, %xmm3
	movaps	%xmm11, %xmm2
	subss	%xmm0, %xmm2
	subss	%xmm7, %xmm5
	addss	%xmm0, %xmm11
	addss	%xmm1, %xmm4
	movaps	%xmm9, %xmm12
	subss	%xmm4, %xmm12
	movss	412(%rbx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm1
	mulss	%xmm10, %xmm1
	movss	416(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm6
	mulss	%xmm8, %xmm6
	addss	%xmm1, %xmm6
	movss	420(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	mulss	%xmm5, %xmm1
	addss	%xmm6, %xmm1
	movss	%xmm1, 76(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm1
	mulss	%xmm15, %xmm1
	movaps	%xmm4, %xmm6
	mulss	%xmm3, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm0, %xmm1
	mulss	%xmm11, %xmm1
	addss	%xmm6, %xmm1
	movss	%xmm1, 336(%rsp)        # 4-byte Spill
	movaps	%xmm13, %xmm1
	mulss	%xmm14, %xmm1
	movaps	%xmm4, %xmm6
	mulss	%xmm2, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm0, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm6, %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movss	428(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm1
	mulss	%xmm10, %xmm1
	movss	432(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm7
	mulss	%xmm8, %xmm7
	addss	%xmm1, %xmm7
	movss	436(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movaps	%xmm5, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm7, %xmm1
	movss	%xmm1, 72(%rsp)         # 4-byte Spill
	movaps	%xmm9, %xmm1
	mulss	%xmm15, %xmm1
	movaps	%xmm6, %xmm7
	mulss	%xmm3, %xmm7
	addss	%xmm1, %xmm7
	movaps	%xmm11, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm7, %xmm1
	movss	%xmm1, 320(%rsp)        # 4-byte Spill
	movaps	%xmm9, %xmm1
	mulss	%xmm14, %xmm1
	movaps	%xmm6, %xmm7
	mulss	%xmm2, %xmm7
	addss	%xmm1, %xmm7
	mulss	%xmm12, %xmm0
	addss	%xmm7, %xmm0
	movaps	%xmm0, 128(%rsp)        # 16-byte Spill
	movss	444(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm10
	movss	448(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm8
	addss	%xmm10, %xmm8
	movss	452(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm5
	addss	%xmm8, %xmm5
	movss	%xmm5, 216(%rsp)        # 4-byte Spill
	mulss	%xmm1, %xmm15
	mulss	%xmm7, %xmm3
	addss	%xmm15, %xmm3
	mulss	%xmm10, %xmm11
	addss	%xmm3, %xmm11
	movss	%xmm11, 212(%rsp)       # 4-byte Spill
	mulss	%xmm1, %xmm14
	mulss	%xmm7, %xmm2
	addss	%xmm14, %xmm2
	mulss	%xmm10, %xmm12
	addss	%xmm2, %xmm12
	movss	%xmm12, 208(%rsp)       # 4-byte Spill
	xorps	%xmm0, %xmm0
	mulss	%xmm0, %xmm13
	mulss	%xmm0, %xmm4
	addss	%xmm13, %xmm4
	movss	192(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm4, %xmm2
	movss	%xmm2, 192(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm9
	mulss	%xmm0, %xmm6
	addss	%xmm9, %xmm6
	movss	44(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm6, %xmm2
	movss	%xmm2, 44(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm1
	mulss	%xmm0, %xmm7
	addss	%xmm1, %xmm7
	mulss	%xmm0, %xmm10
	addss	%xmm7, %xmm10
	movss	%xmm10, 220(%rsp)       # 4-byte Spill
	movss	396(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	xorps	.LCPI11_3(%rip), %xmm10
	movss	348(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm15
	mulss	%xmm10, %xmm15
	movss	364(%rbx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movss	400(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm15
	movss	380(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 188(%rsp)        # 4-byte Spill
	movss	404(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm0
	subss	%xmm0, %xmm15
	movss	352(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm5
	mulss	%xmm10, %xmm5
	movss	368(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm3
	mulss	%xmm7, %xmm3
	subss	%xmm3, %xmm5
	movss	384(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm3
	mulss	%xmm6, %xmm3
	subss	%xmm3, %xmm5
	movss	356(%rbx), %xmm12       # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm10
	movss	372(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 48(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm7
	subss	%xmm7, %xmm10
	movss	388(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 160(%rsp)        # 4-byte Spill
	mulss	%xmm0, %xmm6
	subss	%xmm6, %xmm10
	movss	76(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm3
	mulss	%xmm14, %xmm3
	movss	336(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm8, %xmm6
	addss	%xmm3, %xmm6
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm6, %xmm3
	movss	%xmm3, 148(%rsp)        # 4-byte Spill
	movaps	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm2, %xmm0
	movaps	%xmm0, %xmm6
	mulss	%xmm9, %xmm6
	addss	%xmm3, %xmm6
	movaps	%xmm1, %xmm2
	movss	48(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm6, %xmm2
	movss	%xmm2, 152(%rsp)        # 4-byte Spill
	mulss	188(%rsp), %xmm4        # 4-byte Folded Reload
	movaps	%xmm0, %xmm6
	mulss	%xmm11, %xmm6
	addss	%xmm4, %xmm6
	movss	160(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	addss	%xmm6, %xmm1
	movss	%xmm1, 80(%rsp)         # 4-byte Spill
	movss	72(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm14, %xmm3
	movss	320(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm6
	mulss	%xmm8, %xmm6
	addss	%xmm3, %xmm6
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm3
	mulss	%xmm12, %xmm3
	addss	%xmm6, %xmm3
	movss	%xmm3, 96(%rsp)         # 4-byte Spill
	movaps	%xmm1, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm2, %xmm6
	mulss	%xmm9, %xmm6
	addss	%xmm3, %xmm6
	movaps	%xmm0, %xmm3
	mulss	%xmm7, %xmm3
	addss	%xmm6, %xmm3
	movss	%xmm3, 240(%rsp)        # 4-byte Spill
	movss	188(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	mulss	%xmm11, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm0, %xmm1
	mulss	%xmm4, %xmm1
	movaps	%xmm4, %xmm0
	addss	%xmm2, %xmm1
	movss	%xmm1, 112(%rsp)        # 4-byte Spill
	movss	216(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm14
	movss	212(%rsp), %xmm6        # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm8
	addss	%xmm14, %xmm8
	movss	208(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm12
	addss	%xmm8, %xmm12
	movss	%xmm12, 224(%rsp)       # 4-byte Spill
	mulss	%xmm2, %xmm13
	mulss	%xmm6, %xmm9
	addss	%xmm13, %xmm9
	movss	48(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	addss	%xmm9, %xmm1
	movaps	%xmm1, %xmm8
	mulss	%xmm2, %xmm7
	movaps	%xmm2, %xmm12
	mulss	%xmm6, %xmm11
	movaps	%xmm6, %xmm4
	addss	%xmm7, %xmm11
	mulss	%xmm3, %xmm0
	movaps	%xmm3, %xmm6
	addss	%xmm11, %xmm0
	movaps	%xmm0, %xmm9
	movss	76(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm1
	movss	336(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm10, %xmm1
	addss	%xmm2, %xmm1
	movss	192(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	addss	460(%rbx), %xmm2
	addss	%xmm2, %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movss	72(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm15, %xmm0
	movss	320(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm1, %xmm0
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm10, %xmm1
	addss	%xmm0, %xmm1
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	464(%rbx), %xmm0
	addss	%xmm0, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	mulss	%xmm12, %xmm15
	mulss	%xmm4, %xmm5
	addss	%xmm15, %xmm5
	mulss	%xmm6, %xmm10
	addss	%xmm5, %xmm10
	movss	220(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	addss	468(%rbx), %xmm0
	addss	%xmm0, %xmm10
	movss	352(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	148(%rsp), %xmm14       # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	mulss	%xmm5, %xmm0
	movss	356(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm0, %xmm1
	movss	360(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	224(%rsp), %xmm6        # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	movss	152(%rsp), %xmm13       # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	mulss	%xmm5, %xmm0
	movss	240(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm8, %xmm7
	movss	%xmm7, 48(%rsp)         # 4-byte Spill
	movaps	%xmm7, %xmm0
	mulss	%xmm2, %xmm0
	movaps	%xmm2, 336(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm0
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movss	80(%rsp), %xmm8         # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm5, %xmm0
	movss	112(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm9, %xmm15
	movss	%xmm15, 160(%rsp)       # 4-byte Spill
	movaps	%xmm15, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movss	368(%rsp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	movaps	%xmm14, %xmm2
	mulss	%xmm9, %xmm0
	movss	372(%rsp), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	movss	376(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, 320(%rsp)        # 16-byte Spill
	mulss	%xmm3, %xmm6
	addss	%xmm1, %xmm6
	movss	%xmm6, 72(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm0
	mulss	%xmm9, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm7, %xmm13
	mulss	%xmm3, %xmm13
	addss	%xmm1, %xmm13
	movaps	%xmm8, %xmm0
	mulss	%xmm9, %xmm0
	movaps	%xmm11, %xmm6
	movaps	%xmm6, %xmm1
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm15, %xmm11
	mulss	%xmm3, %xmm11
	addss	%xmm1, %xmm11
	movss	384(%rsp), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm7, %xmm0
	movss	388(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	96(%rsp), %xmm2         # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	addss	%xmm0, %xmm2
	movss	392(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	224(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm2, %xmm4
	movss	152(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	240(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	movss	48(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm15
	addss	%xmm0, %xmm15
	mulss	%xmm7, %xmm8
	movaps	%xmm6, %xmm2
	mulss	%xmm3, %xmm2
	addss	%xmm8, %xmm2
	movss	160(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm2, %xmm0
	unpcklps	%xmm9, %xmm5    # xmm5 = xmm5[0],xmm9[0],xmm5[1],xmm9[1]
	movaps	16(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm6, %xmm8
	shufps	$224, %xmm8, %xmm8      # xmm8 = xmm8[0,0,2,3]
	mulps	%xmm5, %xmm8
	unpcklps	%xmm14, %xmm12  # xmm12 = xmm12[0],xmm14[0],xmm12[1],xmm14[1]
	movaps	128(%rsp), %xmm14       # 16-byte Reload
	movaps	%xmm14, %xmm9
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm12, %xmm9
	addps	%xmm8, %xmm9
	movaps	336(%rsp), %xmm5        # 16-byte Reload
	unpcklps	320(%rsp), %xmm5 # 16-byte Folded Reload
                                        # xmm5 = xmm5[0],mem[0],xmm5[1],mem[1]
	movaps	%xmm10, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm5, %xmm2
	addps	%xmm9, %xmm2
	movsd	400(%rsp), %xmm8        # xmm8 = mem[0],zero
	addps	%xmm2, %xmm8
	mulss	%xmm6, %xmm7
	mulss	%xmm14, %xmm3
	movaps	%xmm14, %xmm5
	addss	%xmm7, %xmm3
	mulss	%xmm10, %xmm1
	addss	%xmm3, %xmm1
	addss	408(%rsp), %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	movss	192(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 480(%rsp)
	movss	44(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 484(%rsp)
	movss	76(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 488(%rsp)
	movl	$0, 492(%rsp)
	movss	72(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 496(%rsp)
	movss	%xmm13, 500(%rsp)
	movss	%xmm11, 504(%rsp)
	movl	$0, 508(%rsp)
	movss	%xmm4, 512(%rsp)
	movss	%xmm15, 516(%rsp)
	movss	%xmm0, 520(%rsp)
	movl	$0, 524(%rsp)
	movlps	%xmm8, 528(%rsp)
	movlps	%xmm2, 536(%rsp)
	movaps	%xmm6, %xmm2
	xorps	.LCPI11_3(%rip), %xmm2
	movss	148(%rsp), %xmm4        # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm15
	mulss	%xmm2, %xmm15
	movss	96(%rsp), %xmm14        # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm0
	movaps	%xmm5, %xmm3
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm15
	movss	224(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	mulss	%xmm10, %xmm0
	subss	%xmm0, %xmm15
	movss	152(%rsp), %xmm6        # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm8
	mulss	%xmm2, %xmm8
	movss	240(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm3, %xmm0
	subss	%xmm0, %xmm8
	movss	48(%rsp), %xmm13        # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm13, %xmm0
	mulss	%xmm10, %xmm0
	subss	%xmm0, %xmm8
	movss	80(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm2
	movss	112(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm5
	subss	%xmm5, %xmm2
	movss	160(%rsp), %xmm11       # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm10
	subss	%xmm10, %xmm2
	movaps	%xmm2, 16(%rsp)         # 16-byte Spill
	movss	416(%rsp), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm0
	movaps	%xmm4, %xmm2
	mulss	%xmm12, %xmm0
	movss	420(%rsp), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	movaps	%xmm6, %xmm10
	mulss	%xmm4, %xmm5
	addss	%xmm0, %xmm5
	movss	424(%rsp), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm0
	mulss	%xmm6, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 128(%rsp)        # 4-byte Spill
	movaps	%xmm14, %xmm0
	mulss	%xmm12, %xmm0
	movaps	%xmm9, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm7, %xmm0
	movaps	%xmm7, %xmm14
	mulss	%xmm6, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	movaps	%xmm1, %xmm3
	movaps	%xmm3, %xmm0
	mulss	%xmm12, %xmm0
	movaps	%xmm13, %xmm7
	movaps	%xmm7, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm11, %xmm0
	movaps	%xmm11, %xmm13
	mulss	%xmm6, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 44(%rsp)         # 4-byte Spill
	movss	432(%rsp), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm11, %xmm0
	movss	436(%rsp), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	movss	440(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	addss	%xmm5, %xmm0
	movss	%xmm0, 76(%rsp)         # 4-byte Spill
	movss	96(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm11, %xmm0
	movaps	%xmm9, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	mulss	%xmm1, %xmm14
	addss	%xmm5, %xmm14
	movss	%xmm14, 72(%rsp)        # 4-byte Spill
	movaps	%xmm3, %xmm0
	mulss	%xmm11, %xmm0
	movaps	%xmm7, %xmm5
	mulss	%xmm2, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm13, %xmm9
	mulss	%xmm1, %xmm9
	movaps	%xmm1, %xmm14
	addss	%xmm5, %xmm9
	movss	448(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	148(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm0
	movss	452(%rsp), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm10
	addss	%xmm0, %xmm10
	movss	456(%rsp), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	80(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm10, %xmm3
	movss	%xmm3, 80(%rsp)         # 4-byte Spill
	movss	96(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	240(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	addss	%xmm3, %xmm7
	movss	112(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm7, %xmm3
	movss	%xmm3, 112(%rsp)        # 4-byte Spill
	movss	224(%rsp), %xmm3        # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	movss	48(%rsp), %xmm7         # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm7
	addss	%xmm3, %xmm7
	mulss	%xmm0, %xmm13
	addss	%xmm7, %xmm13
	unpcklps	%xmm11, %xmm12  # xmm12 = xmm12[0],xmm11[0],xmm12[1],xmm11[1]
	movaps	%xmm15, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm12
	unpcklps	%xmm2, %xmm4    # xmm4 = xmm4[0],xmm2[0],xmm4[1],xmm2[1]
	movaps	%xmm8, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm4
	addps	%xmm12, %xmm4
	unpcklps	%xmm14, %xmm6   # xmm6 = xmm6[0],xmm14[0],xmm6[1],xmm14[1]
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm2, %xmm6
	addps	%xmm4, %xmm6
	movsd	464(%rsp), %xmm2        # xmm2 = mem[0],zero
	addps	%xmm6, %xmm2
	movaps	%xmm2, 160(%rsp)        # 16-byte Spill
	mulss	%xmm1, %xmm15
	mulss	%xmm5, %xmm8
	addss	%xmm15, %xmm8
	movaps	%xmm3, %xmm1
	mulss	%xmm0, %xmm1
	addss	%xmm8, %xmm1
	movss	128(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 544(%rsp)
	movss	192(%rsp), %xmm0        # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 548(%rsp)
	movss	44(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 552(%rsp)
	movl	$0, 556(%rsp)
	movss	76(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 560(%rsp)
	movss	72(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 564(%rsp)
	addss	472(%rsp), %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movss	%xmm9, 568(%rsp)
	xorps	%xmm0, %xmm0
	movss	%xmm1, %xmm0            # xmm0 = xmm1[0],xmm0[1,2,3]
	movl	$0, 572(%rsp)
	movss	80(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 576(%rsp)
	movss	112(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 580(%rsp)
	movss	%xmm13, 584(%rsp)
	movl	$0, 588(%rsp)
	movlps	%xmm2, 592(%rsp)
	movlps	%xmm0, 600(%rsp)
	movsd	528(%rsp), %xmm0        # xmm0 = mem[0],zero
	subps	720(%rsp), %xmm0
	movss	536(%rsp), %xmm1        # xmm1 = mem[0],zero,zero,zero
	subss	728(%rsp), %xmm1
	movss	.LCPI11_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm3
	divss	12(%rsp), %xmm3         # 4-byte Folded Reload
	movaps	%xmm3, 112(%rsp)        # 16-byte Spill
	mulss	%xmm3, %xmm1
	xorps	%xmm2, %xmm2
	movss	%xmm1, %xmm2            # xmm2 = xmm1[0],xmm2[1,2,3]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movaps	%xmm3, 96(%rsp)         # 16-byte Spill
	mulps	%xmm3, %xmm0
	movlps	%xmm0, 256(%rsp)
	movlps	%xmm2, 264(%rsp)
	leaq	480(%rsp), %rsi
	leaq	304(%rsp), %rdx
	leaq	156(%rsp), %rcx
	movq	%r13, %rdi
	callq	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	movss	156(%rsp), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movsd	304(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	mulss	312(%rsp), %xmm3
	movaps	96(%rsp), %xmm1         # 16-byte Reload
	mulps	%xmm1, %xmm2
	movaps	%xmm2, 80(%rsp)         # 16-byte Spill
	movaps	112(%rsp), %xmm2        # 16-byte Reload
	mulss	%xmm2, %xmm3
	movaps	%xmm3, 48(%rsp)         # 16-byte Spill
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	subps	656(%rsp), %xmm0
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	subss	664(%rsp), %xmm3
	mulps	%xmm1, %xmm0
	movaps	%xmm0, %xmm1
	mulss	%xmm2, %xmm3
	xorps	%xmm0, %xmm0
	movss	%xmm3, %xmm0            # xmm0 = xmm3[0],xmm0[1,2,3]
	movlps	%xmm1, 256(%rsp)
	movlps	%xmm0, 264(%rsp)
	leaq	544(%rsp), %rsi
	leaq	304(%rsp), %rdx
	leaq	156(%rsp), %rcx
	movq	%r12, %rdi
	callq	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movss	156(%rsp), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movsd	304(%rsp), %xmm0        # xmm0 = mem[0],zero
	movaps	%xmm9, %xmm10
	shufps	$224, %xmm10, %xmm10    # xmm10 = xmm10[0,0,2,3]
	mulps	%xmm0, %xmm10
	mulss	312(%rsp), %xmm9
	mulps	96(%rsp), %xmm10        # 16-byte Folded Reload
	mulss	112(%rsp), %xmm9        # 16-byte Folded Reload
	movaps	80(%rsp), %xmm0         # 16-byte Reload
	subps	288(%rsp), %xmm0
	movaps	%xmm0, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	subss	296(%rsp), %xmm8
	subps	272(%rsp), %xmm10
	subss	280(%rsp), %xmm9
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI11_4(%rip), %xmm0
	xorps	%xmm7, %xmm7
                                        # implicit-def: %XMM14
                                        # implicit-def: %XMM13
	jbe	.LBB11_21
# BB#18:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB11_20
# BB#19:                                # %call.sqrt
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movaps	%xmm10, 96(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm10        # 16-byte Reload
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB11_20:                              # %.split
	movss	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm14
	divss	%xmm1, %xmm14
	movaps	%xmm8, %xmm0
	mulss	%xmm14, %xmm0
	shufps	$224, %xmm14, %xmm14    # xmm14 = xmm14[0,0,2,3]
	mulps	80(%rsp), %xmm14        # 16-byte Folded Reload
	movaps	%xmm14, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm13, %xmm13
	movss	%xmm0, %xmm13           # xmm13 = xmm0[0],xmm13[1,2,3]
	movq	24(%rbx), %rax
	movss	280(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm2
	movss	296(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	312(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm3, %xmm2
	movss	284(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm3
	movss	300(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	316(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	movss	288(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm4
	movss	304(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	320(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	addss	%xmm5, %xmm7
	mulss	%xmm14, %xmm2
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm0, %xmm7
	addss	%xmm3, %xmm7
.LBB11_21:
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm10, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI11_4(%rip), %xmm0
                                        # implicit-def: %XMM6
                                        # implicit-def: %XMM12
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	xorps	%xmm4, %xmm4
	jbe	.LBB11_25
# BB#22:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB11_24
# BB#23:                                # %call.sqrt1345
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movaps	%xmm10, 96(%rsp)        # 16-byte Spill
	movaps	%xmm14, 112(%rsp)       # 16-byte Spill
	movaps	%xmm13, 128(%rsp)       # 16-byte Spill
	movss	%xmm7, 160(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	160(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm13       # 16-byte Reload
	movaps	112(%rsp), %xmm14       # 16-byte Reload
	movaps	96(%rsp), %xmm10        # 16-byte Reload
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB11_24:                              # %.split1344
	movss	.LCPI11_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm6
	divss	%xmm1, %xmm6
	movaps	%xmm9, %xmm0
	mulss	%xmm6, %xmm0
	shufps	$224, %xmm6, %xmm6      # xmm6 = xmm6[0,0,2,3]
	mulps	%xmm10, %xmm6
	movaps	%xmm6, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm12, %xmm12
	movss	%xmm0, %xmm12           # xmm12 = xmm0[0],xmm12[1,2,3]
	movq	32(%rbx), %rax
	movss	280(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	movss	296(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	312(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	addss	%xmm3, %xmm2
	movss	284(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	movss	300(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	movss	316(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm4, %xmm3
	movss	288(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	movss	304(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm4, %xmm5
	movss	320(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	addss	%xmm5, %xmm4
	mulss	%xmm6, %xmm2
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm0, %xmm4
	addss	%xmm3, %xmm4
.LBB11_25:
	movaps	%xmm7, %xmm0
	mulss	%xmm14, %xmm0
	shufps	$229, %xmm14, %xmm14    # xmm14 = xmm14[1,1,2,3]
	mulss	%xmm7, %xmm14
	mulss	%xmm7, %xmm13
	movaps	%xmm4, %xmm7
	mulss	%xmm6, %xmm7
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	mulss	%xmm4, %xmm6
	mulss	%xmm4, %xmm12
	addss	%xmm0, %xmm7
	addss	%xmm14, %xmm6
	addss	%xmm13, %xmm12
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	ucomiss	.LCPI11_4(%rip), %xmm0
	jbe	.LBB11_42
# BB#26:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB11_28
# BB#27:                                # %call.sqrt1347
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movaps	%xmm10, 96(%rsp)        # 16-byte Spill
	movaps	%xmm6, 112(%rsp)        # 16-byte Spill
	movaps	%xmm12, 128(%rsp)       # 16-byte Spill
	movss	%xmm7, 160(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	160(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movaps	112(%rsp), %xmm6        # 16-byte Reload
	movaps	96(%rsp), %xmm10        # 16-byte Reload
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
.LBB11_28:                              # %.split1346
	movss	.LCPI11_0(%rip), %xmm5  # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm7
	mulss	%xmm0, %xmm6
	mulss	%xmm0, %xmm12
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rax
	movss	280(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	296(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm0, %xmm2
	movss	312(%rcx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	addss	%xmm2, %xmm1
	movss	284(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	300(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm0, %xmm2
	movss	316(%rcx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movss	288(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movss	304(%rcx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm0, %xmm2
	movss	320(%rcx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm7, %xmm1
	mulss	%xmm6, %xmm3
	addss	%xmm1, %xmm3
	mulss	%xmm12, %xmm0
	addss	%xmm3, %xmm0
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm2
	addss	%xmm1, %xmm2
	movss	312(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm1
	addss	%xmm2, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm3
	addss	%xmm2, %xmm3
	movss	316(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm2
	addss	%xmm3, %xmm2
	movss	288(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	movss	304(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm3
	addss	%xmm4, %xmm3
	mulss	%xmm7, %xmm1
	mulss	%xmm6, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm12, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm0, %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	mulps	%xmm2, %xmm4
	mulss	%xmm0, %xmm8
	mulss	%xmm3, %xmm9
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm3, %xmm10
	subps	%xmm10, %xmm4
	subss	%xmm9, %xmm8
	mulss	%xmm1, %xmm1
	movaps	%xmm5, %xmm9
	divss	%xmm1, %xmm9
	mulss	%xmm9, %xmm8
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm4, %xmm9
	xorps	%xmm3, %xmm3
	movss	%xmm8, %xmm3            # xmm3 = xmm8[0],xmm3[1,2,3]
	movss	620(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm2
	jb	.LBB11_39
# BB#29:
	cmpb	$0, 601(%rbx)
	je	.LBB11_31
# BB#30:                                # %select.false.sink
	divss	%xmm0, %xmm2
.LBB11_31:                              # %select.end
	movsd	624(%rbx), %xmm4        # xmm4 = mem[0],zero
	addps	%xmm9, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	addss	632(%rbx), %xmm8
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm8, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB11_33
# BB#32:                                # %call.sqrt1349
	movaps	%xmm1, %xmm0
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movaps	%xmm4, 112(%rsp)        # 16-byte Spill
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	movaps	112(%rsp), %xmm4        # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
.LBB11_33:                              # %.split1348
	ucomiss	%xmm2, %xmm0
	jbe	.LBB11_34
# BB#35:
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB11_37
# BB#36:                                # %call.sqrt1351
	movaps	%xmm1, %xmm0
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm2, 96(%rsp)         # 16-byte Spill
	movaps	%xmm4, 112(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	112(%rsp), %xmm4        # 16-byte Reload
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
.LBB11_37:                              # %.split1350
	movss	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm8
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm1, %xmm4
	movaps	%xmm2, %xmm9
	shufps	$224, %xmm9, %xmm9      # xmm9 = xmm9[0,0,2,3]
	mulps	%xmm4, %xmm9
	mulss	%xmm2, %xmm8
	movsd	624(%rbx), %xmm1        # xmm1 = mem[0],zero
	subps	%xmm1, %xmm9
	movss	632(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm8
	xorps	%xmm3, %xmm3
	movss	%xmm8, %xmm3            # xmm3 = xmm8[0],xmm3[1,2,3]
	pshufd	$229, %xmm1, %xmm2      # xmm2 = xmm1[1,1,2,3]
	jmp	.LBB11_38
.LBB11_34:                              # %._crit_edge
	movss	624(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movd	628(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	632(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
.LBB11_38:
	addss	%xmm9, %xmm1
	movss	%xmm1, 624(%rbx)
	movaps	%xmm9, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	addss	%xmm2, %xmm1
	movss	%xmm1, 628(%rbx)
	addss	%xmm3, %xmm0
	movss	%xmm0, 632(%rbx)
.LBB11_39:
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm9, %xmm8
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	movaps	%xmm8, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB11_41
# BB#40:                                # %call.sqrt1353
	movaps	%xmm1, %xmm0
	movaps	%xmm9, 16(%rsp)         # 16-byte Spill
	movaps	%xmm8, 48(%rsp)         # 16-byte Spill
	movaps	%xmm3, 80(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	movaps	48(%rsp), %xmm8         # 16-byte Reload
	movaps	16(%rsp), %xmm9         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
.LBB11_41:                              # %.split1352
	movss	.LCPI11_0(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm7
	mulss	%xmm7, %xmm9
	mulss	%xmm7, %xmm8
	mulss	%xmm3, %xmm7
	movq	24(%rbx), %rax
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm3, %xmm2
	movss	312(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm3
	movss	316(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	movss	320(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm3
	addss	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	mulss	%xmm0, %xmm4
	movss	(%r15), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r15)
	movss	4(%r15), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r15)
	addss	8(%r15), %xmm4
	movss	%xmm4, 8(%r15)
	movss	32(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	40(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	mulss	%xmm1, %xmm4
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm6
	addss	16(%r15), %xmm4
	movss	%xmm4, 16(%r15)
	addss	20(%r15), %xmm5
	movss	%xmm5, 20(%r15)
	addss	24(%r15), %xmm6
	movss	%xmm6, 24(%r15)
	movq	32(%rbx), %rax
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm1
	movss	284(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm2
	addss	%xmm1, %xmm2
	movss	288(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm1
	addss	%xmm2, %xmm1
	movss	296(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm2
	movss	300(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm3
	addss	%xmm2, %xmm3
	movss	304(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm2
	addss	%xmm3, %xmm2
	mulss	312(%rax), %xmm9
	mulss	316(%rax), %xmm8
	addss	%xmm9, %xmm8
	mulss	320(%rax), %xmm7
	addss	%xmm8, %xmm7
	movss	.LCPI11_5(%rip), %xmm3  # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	(%r14), %xmm4           # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, (%r14)
	movss	4(%r14), %xmm4          # xmm4 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm4
	movss	%xmm4, 4(%r14)
	addss	8(%r14), %xmm3
	movss	%xmm3, 8(%r14)
	movss	32(%r14), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	40(%r14), %xmm0
	mulss	%xmm1, %xmm3
	mulss	%xmm2, %xmm4
	mulss	%xmm7, %xmm0
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm3, %xmm1
	movss	%xmm1, 16(%r14)
	movss	20(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 20(%r14)
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r14)
.LBB11_42:
	leaq	72(%r15), %rbp
.LBB11_54:                              # %._crit_edge1319
	movq	(%rbp), %rax
	xorps	%xmm8, %xmm8
	testq	%rax, %rax
	xorps	%xmm9, %xmm9
	xorps	%xmm10, %xmm10
	je	.LBB11_56
# BB#55:
	movsd	344(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	16(%r15), %xmm9         # xmm9 = mem[0],zero
	addps	%xmm0, %xmm9
	movss	352(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	24(%r15), %xmm0
	xorps	%xmm10, %xmm10
	movss	%xmm0, %xmm10           # xmm10 = xmm0[0],xmm10[1,2,3]
.LBB11_56:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit216
	movq	72(%r14), %rax
	testq	%rax, %rax
	xorps	%xmm12, %xmm12
	je	.LBB11_58
# BB#57:
	movsd	344(%rax), %xmm0        # xmm0 = mem[0],zero
	movsd	16(%r14), %xmm8         # xmm8 = mem[0],zero
	addps	%xmm0, %xmm8
	movss	352(%rax), %xmm0        # xmm0 = mem[0],zero,zero,zero
	addss	24(%r14), %xmm0
	xorps	%xmm12, %xmm12
	movss	%xmm0, %xmm12           # xmm12 = xmm0[0],xmm12[1,2,3]
.LBB11_58:                              # %_ZNK12btSolverBody18getAngularVelocityER9btVector3.exit
	cmpb	$0, 574(%rbx)
	je	.LBB11_64
# BB#59:
	movss	576(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	552(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	480(%rbx), %xmm0
	divss	%xmm11, %xmm0
	movaps	%xmm8, %xmm1
	subss	%xmm9, %xmm1
	movaps	%xmm8, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm9, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	subss	%xmm4, %xmm3
	movaps	%xmm12, %xmm4
	subss	%xmm10, %xmm4
	movss	508(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	512(%rbx), %xmm13       # xmm13 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm1
	mulss	%xmm13, %xmm3
	addss	%xmm1, %xmm3
	movss	516(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm3, %xmm4
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm4
	jbe	.LBB11_61
# BB#60:
	mulss	%xmm4, %xmm2
	mulss	484(%rbx), %xmm2
	addss	%xmm2, %xmm0
.LBB11_61:
	mulss	540(%rbx), %xmm0
	movss	564(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm0
	movss	%xmm0, 672(%rsp)
	movl	$0, 608(%rsp)
	ucomiss	%xmm3, %xmm0
	leaq	672(%rsp), %rax
	leaq	608(%rsp), %rcx
	cmovaq	%rax, %rcx
	movl	(%rcx), %eax
	movl	%eax, 564(%rbx)
	movd	%eax, %xmm5
	subss	%xmm2, %xmm5
	mulss	%xmm5, %xmm14
	mulss	%xmm5, %xmm13
	mulss	%xmm1, %xmm5
	movss	584(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	588(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm14, %xmm2
	mulss	%xmm0, %xmm2
	movaps	%xmm13, %xmm3
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movss	592(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm4
	mulss	%xmm2, %xmm4
	addss	%xmm3, %xmm4
	mulss	%xmm4, %xmm0
	mulss	%xmm4, %xmm1
	mulss	%xmm2, %xmm4
	subss	%xmm0, %xmm14
	subss	%xmm1, %xmm13
	subss	%xmm4, %xmm5
	movaps	%xmm14, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm13, %xmm2
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm5, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB11_63
# BB#62:                                # %call.sqrt1358
	movaps	%xmm1, %xmm0
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	movaps	%xmm12, 96(%rsp)        # 16-byte Spill
	movss	%xmm13, 112(%rsp)       # 4-byte Spill
	movss	%xmm14, 128(%rsp)       # 4-byte Spill
	movss	%xmm5, 160(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	160(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	128(%rsp), %xmm14       # 4-byte Reload
                                        # xmm14 = mem[0],zero,zero,zero
	movss	112(%rsp), %xmm13       # 4-byte Reload
                                        # xmm13 = mem[0],zero,zero,zero
	movaps	96(%rsp), %xmm12        # 16-byte Reload
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movss	12(%rsp), %xmm11        # 4-byte Reload
                                        # xmm11 = mem[0],zero,zero,zero
.LBB11_63:                              # %.split1357
	movss	.LCPI11_0(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm14
	mulss	%xmm1, %xmm13
	mulss	%xmm5, %xmm1
	movq	24(%rbx), %rax
	movss	280(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm2
	movss	284(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	addss	%xmm2, %xmm3
	movss	288(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	296(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm3
	movss	300(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm4
	addss	%xmm3, %xmm4
	movss	304(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	movss	312(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm4
	movss	316(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm5
	addss	%xmm4, %xmm5
	movss	320(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	addss	%xmm5, %xmm4
	xorps	%xmm5, %xmm5
	mulss	%xmm0, %xmm5
	movss	(%r15), %xmm6           # xmm6 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm6
	movss	%xmm6, (%r15)
	movss	4(%r15), %xmm6          # xmm6 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm6
	movss	%xmm6, 4(%r15)
	addss	8(%r15), %xmm5
	movss	%xmm5, 8(%r15)
	movss	32(%r15), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	movss	36(%r15), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm6
	movss	40(%r15), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm5
	mulss	%xmm3, %xmm6
	mulss	%xmm4, %xmm7
	addss	16(%r15), %xmm5
	movss	%xmm5, 16(%r15)
	addss	20(%r15), %xmm6
	movss	%xmm6, 20(%r15)
	addss	24(%r15), %xmm7
	movss	%xmm7, 24(%r15)
	movq	32(%rbx), %rax
	movss	280(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm2
	movss	284(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	addss	%xmm2, %xmm3
	movss	288(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	296(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm14, %xmm3
	movss	300(%rax), %xmm4        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm4
	addss	%xmm3, %xmm4
	movss	304(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	mulss	312(%rax), %xmm14
	mulss	316(%rax), %xmm13
	addss	%xmm14, %xmm13
	mulss	320(%rax), %xmm1
	addss	%xmm13, %xmm1
	movss	.LCPI11_5(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	(%r14), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r14)
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r14)
	addss	8(%r14), %xmm4
	movss	%xmm4, 8(%r14)
	movss	32(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	mulss	40(%r14), %xmm0
	mulss	%xmm2, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm0
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 16(%r14)
	movss	20(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	movss	%xmm1, 20(%r14)
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r14)
.LBB11_64:
	cmpb	$0, 573(%rbx)
	je	.LBB11_68
# BB#65:
	movss	580(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	556(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm0
	mulss	480(%rbx), %xmm0
	divss	%xmm11, %xmm0
	movaps	%xmm8, %xmm1
	subss	%xmm9, %xmm1
	shufps	$229, %xmm8, %xmm8      # xmm8 = xmm8[1,1,2,3]
	shufps	$229, %xmm9, %xmm9      # xmm9 = xmm9[1,1,2,3]
	subss	%xmm9, %xmm8
	subss	%xmm10, %xmm12
	movss	524(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	528(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	mulss	%xmm3, %xmm8
	addss	%xmm1, %xmm8
	movss	532(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm12
	addss	%xmm8, %xmm12
	xorps	%xmm2, %xmm2
	ucomiss	%xmm2, %xmm12
	jbe	.LBB11_67
# BB#66:
	mulss	%xmm12, %xmm5
	mulss	484(%rbx), %xmm5
	addss	%xmm5, %xmm0
.LBB11_67:
	mulss	544(%rbx), %xmm0
	movss	568(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	addss	%xmm5, %xmm0
	movss	%xmm0, 672(%rsp)
	movl	$0, 608(%rsp)
	ucomiss	%xmm2, %xmm0
	leaq	672(%rsp), %rax
	leaq	608(%rsp), %rcx
	cmovaq	%rax, %rcx
	movl	(%rcx), %eax
	movl	%eax, 568(%rbx)
	movd	%eax, %xmm0
	subss	%xmm5, %xmm0
	movq	24(%rbx), %rax
	movss	280(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm5
	movss	284(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm6
	addss	%xmm5, %xmm6
	movss	288(%rax), %xmm5        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm6, %xmm5
	movss	296(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm6
	movss	300(%rax), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm7
	addss	%xmm6, %xmm7
	movss	304(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm7, %xmm6
	mulss	312(%rax), %xmm4
	mulss	316(%rax), %xmm3
	addss	%xmm4, %xmm3
	mulss	320(%rax), %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm2
	movss	(%r15), %xmm3           # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, (%r15)
	movss	4(%r15), %xmm3          # xmm3 = mem[0],zero,zero,zero
	addss	%xmm2, %xmm3
	movss	%xmm3, 4(%r15)
	addss	8(%r15), %xmm2
	movss	%xmm2, 8(%r15)
	movss	32(%r15), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	movss	36(%r15), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	40(%r15), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	mulss	%xmm5, %xmm2
	mulss	%xmm6, %xmm3
	mulss	%xmm1, %xmm4
	addss	16(%r15), %xmm2
	movss	%xmm2, 16(%r15)
	addss	20(%r15), %xmm3
	movss	%xmm3, 20(%r15)
	addss	24(%r15), %xmm4
	movss	%xmm4, 24(%r15)
	movq	32(%rbx), %rax
	movss	524(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movss	280(%rax), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	movss	528(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	284(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm3
	addss	%xmm1, %xmm3
	movss	532(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	288(%rax), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm3, %xmm2
	movss	296(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm3
	movss	300(%rax), %xmm6        # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm6
	addss	%xmm3, %xmm6
	movss	304(%rax), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm6, %xmm3
	mulss	312(%rax), %xmm4
	mulss	316(%rax), %xmm5
	addss	%xmm4, %xmm5
	mulss	320(%rax), %xmm1
	addss	%xmm5, %xmm1
	movss	.LCPI11_5(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	(%r14), %xmm5           # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, (%r14)
	movss	4(%r14), %xmm5          # xmm5 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm5
	movss	%xmm5, 4(%r14)
	addss	8(%r14), %xmm4
	movss	%xmm4, 8(%r14)
	movss	32(%r14), %xmm4         # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	movss	36(%r14), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm5
	mulss	40(%r14), %xmm0
	mulss	%xmm2, %xmm4
	mulss	%xmm3, %xmm5
	mulss	%xmm1, %xmm0
	movss	16(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm4, %xmm1
	movss	%xmm1, 16(%r14)
	movss	20(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm5, %xmm1
	movss	%xmm1, 20(%r14)
	movss	24(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	%xmm0, %xmm1
	movss	%xmm1, 24(%r14)
.LBB11_68:
	addq	$744, %rsp              # imm = 0x2E8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f, .Lfunc_end11-_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI12_0:
	.long	1061752795              # float 0.785398185
.LCPI12_1:
	.long	981668463               # float 0.00100000005
.LCPI12_2:
	.long	1056964608              # float 0.5
.LCPI12_3:
	.long	3165301419              # float -0.020833334
.LCPI12_4:
	.long	1065353216              # float 1
.LCPI12_5:
	.long	1073741824              # float 2
	.section	.text._ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,"axG",@progbits,_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,comdat
	.weak	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_,@function
_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_: # @_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi67:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi68:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi69:
	.cfi_def_cfa_offset 32
	subq	$64, %rsp
.Lcfi70:
	.cfi_def_cfa_offset 96
.Lcfi71:
	.cfi_offset %rbx, -32
.Lcfi72:
	.cfi_offset %r14, -24
.Lcfi73:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movaps	%xmm0, %xmm3
	movq	%rdx, %rbx
	movq	%rdi, %r14
	movsd	(%rsi), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm3, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm0, %xmm1
	movss	8(%rsi), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm0
	movsd	48(%r14), %xmm2         # xmm2 = mem[0],zero
	addps	%xmm1, %xmm2
	addss	56(%r14), %xmm0
	xorps	%xmm1, %xmm1
	movss	%xmm0, %xmm1            # xmm1 = xmm0[0],xmm1[1,2,3]
	movlps	%xmm2, 48(%r15)
	movlps	%xmm1, 56(%r15)
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm2, %xmm2
	sqrtss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm2
	jnp	.LBB12_2
# BB#1:                                 # %call.sqrt
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm0, %xmm2
.LBB12_2:                               # %.split
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	ucomiss	.LCPI12_0(%rip), %xmm0
	jbe	.LBB12_4
# BB#3:                                 # %select.true.sink
	movss	.LCPI12_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm2
.LBB12_4:                               # %select.end
	movss	.LCPI12_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm2, %xmm0
	jbe	.LBB12_6
# BB#5:
	movss	.LCPI12_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movaps	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm3, %xmm0
	mulss	.LCPI12_3(%rip), %xmm0
	mulss	%xmm2, %xmm0
	mulss	%xmm2, %xmm0
	addss	%xmm1, %xmm0
	jmp	.LBB12_7
.LBB12_6:
	movss	.LCPI12_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	movaps	%xmm3, 16(%rsp)         # 16-byte Spill
	movss	%xmm2, 8(%rsp)          # 4-byte Spill
	callq	sinf
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm3         # 16-byte Reload
	divss	%xmm2, %xmm0
.LBB12_7:
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)         # 4-byte Spill
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 8(%rsp)          # 4-byte Spill
	mulss	8(%rbx), %xmm0
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	mulss	%xmm3, %xmm2
	mulss	.LCPI12_2(%rip), %xmm2
	movaps	%xmm2, %xmm0
	callq	cosf
	movss	%xmm0, 16(%rsp)         # 4-byte Spill
	leaq	48(%rsp), %rsi
	movq	%r14, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	48(%rsp), %xmm12        # xmm12 = mem[0],zero
	movq	56(%rsp), %xmm9         # xmm9 = mem[0],zero
	movss	16(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	pshufd	$229, %xmm9, %xmm8      # xmm8 = xmm9[1,1,2,3]
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm4
	mulss	%xmm8, %xmm4
	addss	%xmm3, %xmm4
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm4, %xmm7
	pshufd	$229, %xmm12, %xmm11    # xmm11 = xmm12[1,1,2,3]
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm4
	mulss	%xmm11, %xmm4
	subss	%xmm4, %xmm7
	movaps	%xmm1, %xmm4
	movaps	%xmm1, %xmm13
	mulss	%xmm11, %xmm4
	movaps	%xmm2, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm3, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm0, %xmm4
	mulss	%xmm9, %xmm4
	subss	%xmm4, %xmm6
	movaps	%xmm13, %xmm4
	mulss	%xmm9, %xmm4
	movaps	%xmm3, %xmm5
	movaps	%xmm3, %xmm1
	mulss	%xmm8, %xmm5
	addss	%xmm4, %xmm5
	movaps	%xmm0, %xmm10
	mulss	%xmm11, %xmm10
	addss	%xmm5, %xmm10
	movaps	%xmm2, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm10
	movaps	%xmm13, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm0, %xmm4
	mulss	%xmm12, %xmm4
	subss	%xmm4, %xmm3
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm3
	mulss	%xmm9, %xmm1
	subss	%xmm1, %xmm3
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm3, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB12_9
# BB#8:                                 # %call.sqrt113
	movaps	%xmm1, %xmm0
	movss	%xmm7, 8(%rsp)          # 4-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm3, 16(%rsp)         # 4-byte Spill
	movss	%xmm10, 32(%rsp)        # 4-byte Spill
	callq	sqrtf
	movss	32(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	movss	16(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm7          # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
.LBB12_9:                               # %.split112
	movss	.LCPI12_4(%rip), %xmm8  # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	divss	%xmm0, %xmm2
	mulss	%xmm2, %xmm7
	mulss	%xmm2, %xmm6
	mulss	%xmm2, %xmm10
	mulss	%xmm3, %xmm2
	movaps	%xmm7, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm2, %xmm3
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movss	.LCPI12_5(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	divss	%xmm3, %xmm4
	movaps	%xmm7, %xmm0
	mulss	%xmm4, %xmm0
	movaps	%xmm6, %xmm3
	mulss	%xmm4, %xmm3
	mulss	%xmm10, %xmm4
	movaps	%xmm2, %xmm9
	mulss	%xmm0, %xmm9
	movaps	%xmm2, %xmm11
	mulss	%xmm3, %xmm11
	mulss	%xmm4, %xmm2
	mulss	%xmm7, %xmm0
	movaps	%xmm7, %xmm5
	mulss	%xmm3, %xmm5
	mulss	%xmm4, %xmm7
	mulss	%xmm6, %xmm3
	mulss	%xmm4, %xmm6
	mulss	%xmm10, %xmm4
	movaps	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	movaps	%xmm8, %xmm10
	subss	%xmm1, %xmm10
	movaps	%xmm5, %xmm12
	subss	%xmm2, %xmm12
	movaps	%xmm7, %xmm1
	addss	%xmm11, %xmm1
	addss	%xmm2, %xmm5
	addss	%xmm0, %xmm4
	movaps	%xmm8, %xmm2
	subss	%xmm4, %xmm2
	movaps	%xmm6, %xmm4
	subss	%xmm9, %xmm4
	subss	%xmm11, %xmm7
	addss	%xmm9, %xmm6
	addss	%xmm0, %xmm3
	subss	%xmm3, %xmm8
	movss	%xmm10, (%r15)
	movss	%xmm12, 4(%r15)
	movss	%xmm1, 8(%r15)
	movl	$0, 12(%r15)
	movss	%xmm5, 16(%r15)
	movss	%xmm2, 20(%r15)
	movss	%xmm4, 24(%r15)
	movl	$0, 28(%r15)
	movss	%xmm7, 32(%r15)
	movss	%xmm6, 36(%r15)
	movss	%xmm8, 40(%r15)
	movl	$0, 44(%r15)
	addq	$64, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end12:
	.size	_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_, .Lfunc_end12-_ZN15btTransformUtil18integrateTransformERK11btTransformRK9btVector3S5_fRS0_
	.cfi_endproc

	.text
	.globl	_ZN21btConeTwistConstraint9updateRHSEf
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint9updateRHSEf,@function
_ZN21btConeTwistConstraint9updateRHSEf: # @_ZN21btConeTwistConstraint9updateRHSEf
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end13:
	.size	_ZN21btConeTwistConstraint9updateRHSEf, .Lfunc_end13-_ZN21btConeTwistConstraint9updateRHSEf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI14_0:
	.long	1028443341              # float 0.0500000007
.LCPI14_2:
	.long	1061752795              # float 0.785398185
.LCPI14_3:
	.long	1075235812              # float 2.3561945
.LCPI14_4:
	.long	3209236443              # float -0.785398185
.LCPI14_6:
	.long	1092616192              # float 10
.LCPI14_7:
	.long	1065353216              # float 1
.LCPI14_9:
	.long	3212836864              # float -1
.LCPI14_11:
	.long	3212836862              # float -0.99999988
.LCPI14_12:
	.long	1056964608              # float 0.5
.LCPI14_13:
	.long	1060439283              # float 0.707106769
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI14_1:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
.LCPI14_5:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI14_8:
	.long	1065353216              # float 1
	.long	1065353216              # float 1
	.zero	4
	.zero	4
.LCPI14_14:
	.long	1056964608              # float 0.5
	.long	1056964608              # float 0.5
	.zero	4
	.zero	4
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	2
.LCPI14_10:
	.long	3212836864              # float -1
	.long	1065353216              # float 1
	.text
	.globl	_ZN21btConeTwistConstraint13calcAngleInfoEv
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint13calcAngleInfoEv,@function
_ZN21btConeTwistConstraint13calcAngleInfoEv: # @_ZN21btConeTwistConstraint13calcAngleInfoEv
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi74:
	.cfi_def_cfa_offset 16
	subq	$208, %rsp
.Lcfi75:
	.cfi_def_cfa_offset 224
.Lcfi76:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movl	$0, 552(%rbx)
	movl	$0, 548(%rbx)
	movw	$0, 573(%rbx)
	movss	348(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	364(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	380(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movq	24(%rbx), %rcx
	movq	32(%rbx), %rax
	movss	24(%rcx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rcx), %xmm11         # xmm11 = mem[0],zero,zero,zero
	movss	12(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm11   # xmm11 = xmm11[0],xmm3[0],xmm11[1],xmm3[1]
	mulps	%xmm11, %xmm2
	movaps	%xmm0, %xmm3
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	movss	28(%rcx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	unpcklps	%xmm4, %xmm6    # xmm6 = xmm6[0],xmm4[0],xmm6[1],xmm4[1]
	movaps	%xmm6, 96(%rsp)         # 16-byte Spill
	mulps	%xmm6, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm5, %xmm4
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	movss	32(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	16(%rcx), %xmm12        # xmm12 = mem[0],zero,zero,zero
	unpcklps	%xmm2, %xmm12   # xmm12 = xmm12[0],xmm2[0],xmm12[1],xmm2[1]
	mulps	%xmm12, %xmm4
	addps	%xmm3, %xmm4
	movaps	%xmm4, 64(%rsp)         # 16-byte Spill
	movss	40(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movss	%xmm2, 32(%rsp)         # 4-byte Spill
	mulss	%xmm2, %xmm1
	movss	44(%rcx), %xmm9         # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	addss	%xmm1, %xmm0
	movss	48(%rcx), %xmm6         # xmm6 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm5
	addss	%xmm0, %xmm5
	movaps	%xmm5, 80(%rsp)         # 16-byte Spill
	movss	412(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	428(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	444(%rbx), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	24(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm4          # xmm4 = mem[0],zero,zero,zero
	movss	12(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	mulps	%xmm2, %xmm4
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm5    # xmm5 = xmm5[0],xmm3[0],xmm5[1],xmm3[1]
	mulps	%xmm2, %xmm5
	addps	%xmm4, %xmm5
	movaps	%xmm7, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movss	32(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movss	16(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	unpcklps	%xmm3, %xmm8    # xmm8 = xmm8[0],xmm3[0],xmm8[1],xmm3[1]
	mulps	%xmm2, %xmm8
	addps	%xmm5, %xmm8
	mulss	40(%rax), %xmm1
	mulss	44(%rax), %xmm0
	addss	%xmm1, %xmm0
	mulss	48(%rax), %xmm7
	addss	%xmm0, %xmm7
	movss	492(%rbx), %xmm15       # xmm15 = mem[0],zero,zero,zero
	xorps	%xmm10, %xmm10
	ucomiss	.LCPI14_0(%rip), %xmm15
                                        # implicit-def: %XMM0
	movaps	%xmm0, 160(%rsp)        # 16-byte Spill
                                        # implicit-def: %XMM0
	xorps	%xmm4, %xmm4
	jb	.LBB14_5
# BB#1:
	movss	352(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	368(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	384(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm4
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm11, %xmm2
	movaps	%xmm9, %xmm13
	mulss	%xmm3, %xmm13
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	96(%rsp), %xmm3         # 16-byte Folded Reload
	addps	%xmm2, %xmm3
	movaps	%xmm6, %xmm5
	movaps	%xmm6, %xmm2
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm12, %xmm1
	addps	%xmm3, %xmm1
	movaps	%xmm1, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	addss	%xmm4, %xmm13
	addss	%xmm2, %xmm13
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm2
	mulss	%xmm8, %xmm2
	movaps	%xmm4, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm8, %xmm0
	movaps	%xmm7, %xmm8
	movaps	%xmm0, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	%xmm7, %xmm6
	addss	%xmm2, %xmm6
	movaps	80(%rsp), %xmm4         # 16-byte Reload
	mulss	%xmm8, %xmm4
	addss	%xmm6, %xmm4
	mulss	%xmm3, %xmm7
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movaps	%xmm0, %xmm2
	movaps	%xmm1, 160(%rsp)        # 16-byte Spill
	mulss	%xmm1, %xmm2
	addss	%xmm2, %xmm7
	movaps	%xmm8, %xmm14
	movaps	%xmm8, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm7, %xmm2
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm4
	movaps	.LCPI14_1(%rip), %xmm6  # xmm6 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm6
	movaps	%xmm4, %xmm3
	jae	.LBB14_2
# BB#3:
	addss	%xmm6, %xmm3
	subss	%xmm4, %xmm6
	divss	%xmm6, %xmm3
	movss	.LCPI14_3(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	jmp	.LBB14_4
.LBB14_2:
	subss	%xmm6, %xmm3
	addss	%xmm4, %xmm6
	divss	%xmm6, %xmm3
	movss	.LCPI14_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
.LBB14_4:                               # %_Z11btAtan2Fastff.exit192
	movaps	%xmm14, %xmm7
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	xorps	%xmm0, %xmm0
	movss	%xmm13, %xmm0           # xmm0 = xmm13[0],xmm0[1,2,3]
	mulss	.LCPI14_4(%rip), %xmm3
	addss	%xmm6, %xmm3
	xorps	%xmm1, %xmm1
	movaps	%xmm2, %xmm6
	cmpltss	%xmm1, %xmm6
	movaps	%xmm6, %xmm1
	andnps	%xmm3, %xmm1
	xorps	.LCPI14_5(%rip), %xmm3
	andps	%xmm6, %xmm3
	orps	%xmm1, %xmm3
	mulss	%xmm2, %xmm2
	mulss	%xmm4, %xmm4
	addss	%xmm2, %xmm4
	movss	.LCPI14_6(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm4
	mulss	%xmm1, %xmm4
	movss	.LCPI14_7(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	addss	%xmm4, %xmm1
	divss	%xmm1, %xmm4
	mulss	%xmm3, %xmm4
	movaps	%xmm5, %xmm6
.LBB14_5:
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movss	496(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	ucomiss	.LCPI14_0(%rip), %xmm14
                                        # implicit-def: %XMM13
                                        # implicit-def: %XMM0
	movaps	%xmm0, 112(%rsp)        # 16-byte Spill
	jb	.LBB14_10
# BB#6:
	movss	356(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	372(%rbx), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movss	388(%rbx), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm11, %xmm2
	mulss	%xmm3, %xmm9
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	96(%rsp), %xmm3         # 16-byte Folded Reload
	addps	%xmm2, %xmm3
	mulss	%xmm5, %xmm6
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	%xmm12, %xmm5
	addps	%xmm3, %xmm5
	movaps	%xmm5, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	addss	%xmm0, %xmm9
	addss	%xmm9, %xmm6
	xorps	%xmm9, %xmm9
	movaps	64(%rsp), %xmm0         # 16-byte Reload
	movaps	%xmm0, %xmm3
	mulss	%xmm8, %xmm3
	movaps	%xmm6, %xmm12
	movaps	%xmm0, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm7, %xmm1
	movaps	%xmm8, %xmm7
	shufps	$229, %xmm7, %xmm7      # xmm7 = xmm7[1,1,2,3]
	mulss	%xmm7, %xmm6
	addss	%xmm3, %xmm6
	movaps	80(%rsp), %xmm10        # 16-byte Reload
	mulss	%xmm1, %xmm10
	addss	%xmm6, %xmm10
	movaps	%xmm8, %xmm11
	movaps	%xmm8, %xmm3
	movaps	%xmm5, %xmm13
	mulss	%xmm5, %xmm3
	mulss	%xmm2, %xmm7
	addss	%xmm3, %xmm7
	movaps	%xmm1, %xmm8
	movaps	%xmm1, %xmm2
	movaps	%xmm12, %xmm1
	mulss	%xmm12, %xmm2
	addss	%xmm7, %xmm2
	movaps	.LCPI14_1(%rip), %xmm6  # xmm6 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm6
	xorps	%xmm3, %xmm3
	ucomiss	%xmm3, %xmm10
	jae	.LBB14_7
# BB#8:
	movaps	%xmm10, %xmm3
	addss	%xmm6, %xmm3
	subss	%xmm10, %xmm6
	divss	%xmm6, %xmm3
	movss	.LCPI14_3(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
	jmp	.LBB14_9
.LBB14_7:
	movaps	%xmm10, %xmm3
	subss	%xmm6, %xmm3
	addss	%xmm10, %xmm6
	divss	%xmm6, %xmm3
	movss	.LCPI14_2(%rip), %xmm6  # xmm6 = mem[0],zero,zero,zero
.LBB14_9:                               # %_Z11btAtan2Fastff.exit177
	movss	%xmm1, %xmm9            # xmm9 = xmm1[0],xmm9[1,2,3]
	movaps	%xmm9, 112(%rsp)        # 16-byte Spill
	mulss	.LCPI14_4(%rip), %xmm3
	addss	%xmm6, %xmm3
	xorps	%xmm6, %xmm6
	movaps	%xmm2, %xmm7
	cmpltss	%xmm6, %xmm7
	movaps	%xmm7, %xmm6
	andnps	%xmm3, %xmm6
	xorps	.LCPI14_5(%rip), %xmm3
	andps	%xmm7, %xmm3
	orps	%xmm6, %xmm3
	mulss	%xmm2, %xmm2
	mulss	%xmm10, %xmm10
	addss	%xmm2, %xmm10
	movss	.LCPI14_6(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm10
	mulss	%xmm2, %xmm10
	movss	.LCPI14_7(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	addss	%xmm10, %xmm2
	divss	%xmm2, %xmm10
	mulss	%xmm3, %xmm10
	movaps	%xmm8, %xmm7
	movaps	%xmm11, %xmm8
.LBB14_10:
	unpcklps	%xmm14, %xmm15  # xmm15 = xmm15[0],xmm14[0],xmm15[1],xmm14[1]
	mulps	%xmm15, %xmm15
	movaps	.LCPI14_8(%rip), %xmm1  # xmm1 = <1,1,u,u>
	divps	%xmm15, %xmm1
	unpcklps	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1]
	mulps	%xmm4, %xmm4
	andps	.LCPI14_1(%rip), %xmm4
	mulps	%xmm1, %xmm4
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	addss	%xmm4, %xmm0
	ucomiss	.LCPI14_7(%rip), %xmm0
	movaps	112(%rsp), %xmm9        # 16-byte Reload
	movaps	%xmm7, %xmm10
	movaps	144(%rsp), %xmm11       # 16-byte Reload
	jbe	.LBB14_14
# BB#11:
	addss	.LCPI14_9(%rip), %xmm0
	movss	%xmm0, 552(%rbx)
	movb	$1, 574(%rbx)
	movaps	%xmm8, %xmm1
	movaps	160(%rsp), %xmm4        # 16-byte Reload
	mulss	%xmm4, %xmm1
	movaps	%xmm4, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	%xmm8, %xmm5
	shufps	$229, %xmm5, %xmm5      # xmm5 = xmm5[1,1,2,3]
	movaps	%xmm5, %xmm2
	mulss	%xmm0, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm10, %xmm3
	mulss	%xmm11, %xmm3
	addss	%xmm2, %xmm3
	mulss	%xmm3, %xmm0
	movaps	%xmm11, %xmm1
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	shufps	$224, %xmm3, %xmm3      # xmm3 = xmm3[0,0,2,3]
	mulps	%xmm1, %xmm3
	movaps	%xmm8, %xmm1
	mulss	%xmm13, %xmm1
	movaps	%xmm13, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm5, %xmm4
	mulss	%xmm2, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm10, %xmm1
	mulss	%xmm9, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm1, %xmm2
	movaps	%xmm9, %xmm4
	unpcklps	%xmm13, %xmm4   # xmm4 = xmm4[0],xmm13[0],xmm4[1],xmm13[1]
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addss	%xmm0, %xmm2
	addps	%xmm3, %xmm1
	movaps	%xmm10, %xmm3
	shufps	$0, %xmm5, %xmm3        # xmm3 = xmm3[0,0],xmm5[0,0]
	shufps	$226, %xmm5, %xmm3      # xmm3 = xmm3[2,0],xmm5[2,3]
	mulps	%xmm1, %xmm3
	movaps	%xmm10, %xmm4
	unpcklps	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1]
	movaps	%xmm8, %xmm0
	mulss	%xmm2, %xmm0
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	mulps	%xmm4, %xmm2
	subps	%xmm2, %xmm3
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm0
	movaps	%xmm3, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	xorps	%xmm2, %xmm2
	movss	%xmm0, %xmm2            # xmm2 = xmm0[0],xmm2[1,2,3]
	movlps	%xmm3, 508(%rbx)
	movlps	%xmm2, 516(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm1, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB14_13
# BB#12:                                # %call.sqrt
	movaps	%xmm10, 32(%rsp)        # 16-byte Spill
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm13, 96(%rsp)        # 16-byte Spill
	movaps	%xmm5, 48(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movaps	96(%rsp), %xmm13        # 16-byte Reload
	movaps	112(%rsp), %xmm9        # 16-byte Reload
	movaps	144(%rsp), %xmm11       # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movaps	32(%rsp), %xmm10        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB14_13:                              # %.split
	movss	.LCPI14_7(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	508(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	512(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm2
	mulss	516(%rbx), %xmm0
	movaps	64(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm4, %xmm3
	mulss	%xmm8, %xmm3
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	mulss	%xmm5, %xmm4
	addss	%xmm3, %xmm4
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm10, %xmm3
	addss	%xmm4, %xmm3
	xorps	%xmm4, %xmm4
	xorl	%eax, %eax
	ucomiss	%xmm4, %xmm3
	setae	%al
	movss	.LCPI14_10(,%rax,4), %xmm3 # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm1
	movss	%xmm1, 508(%rbx)
	mulss	%xmm3, %xmm2
	movss	%xmm2, 512(%rbx)
	mulss	%xmm3, %xmm0
	movss	%xmm0, 516(%rbx)
.LBB14_14:
	movss	500(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	xorps	%xmm1, %xmm1
	ucomiss	%xmm1, %xmm0
	jb	.LBB14_38
# BB#15:
	movq	32(%rbx), %rax
	movss	416(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	432(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	448(%rbx), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movss	8(%rax), %xmm2          # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	12(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	16(%rax), %xmm7         # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm7
	addss	%xmm3, %xmm7
	movss	24(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	addss	%xmm2, %xmm3
	movss	32(%rax), %xmm5         # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm6, %xmm5
	addss	%xmm3, %xmm5
	mulss	40(%rax), %xmm1
	mulss	44(%rax), %xmm0
	addss	%xmm1, %xmm0
	mulss	48(%rax), %xmm6
	addss	%xmm0, %xmm6
	movaps	64(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm0
	mulss	%xmm8, %xmm0
	movaps	%xmm1, %xmm4
	shufps	$229, %xmm4, %xmm4      # xmm4 = xmm4[1,1,2,3]
	movaps	%xmm8, %xmm12
	shufps	$229, %xmm12, %xmm12    # xmm12 = xmm12[1,1,2,3]
	movaps	%xmm4, %xmm2
	mulss	%xmm12, %xmm2
	addss	%xmm0, %xmm2
	movaps	80(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm10, %xmm1
	addss	%xmm2, %xmm1
	movss	.LCPI14_11(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm0
	movaps	%xmm13, 96(%rsp)        # 16-byte Spill
	jbe	.LBB14_23
# BB#16:
	movaps	.LCPI14_1(%rip), %xmm0  # xmm0 = [nan,nan,nan,nan]
	andps	%xmm10, %xmm0
	ucomiss	.LCPI14_13(%rip), %xmm0
	jbe	.LBB14_20
# BB#17:
	movaps	%xmm12, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB14_19
# BB#18:                                # %call.sqrt442
	movaps	%xmm1, %xmm0
	movaps	%xmm10, 32(%rsp)        # 16-byte Spill
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm7, 48(%rsp)         # 16-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	movaps	%xmm12, 128(%rsp)       # 16-byte Spill
	callq	sqrtf
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movaps	112(%rsp), %xmm9        # 16-byte Reload
	movaps	144(%rsp), %xmm11       # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movaps	32(%rsp), %xmm10        # 16-byte Reload
.LBB14_19:                              # %.split441
	movss	.LCPI14_7(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	movaps	%xmm10, %xmm13
	mulss	%xmm1, %xmm13
	xorps	.LCPI14_5(%rip), %xmm13
	mulss	%xmm1, %xmm12
	xorps	%xmm14, %xmm14
	xorps	%xmm0, %xmm0
	jmp	.LBB14_26
.LBB14_23:
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm14
	mulss	%xmm12, %xmm14
	movaps	%xmm4, %xmm0
	mulss	%xmm10, %xmm0
	subss	%xmm0, %xmm14
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	movaps	%xmm3, %xmm13
	mulss	%xmm10, %xmm13
	movaps	%xmm2, %xmm0
	mulss	%xmm8, %xmm0
	subss	%xmm0, %xmm13
	mulss	%xmm8, %xmm4
	mulss	%xmm3, %xmm12
	subss	%xmm12, %xmm4
	addss	.LCPI14_7(%rip), %xmm1
	addss	%xmm1, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB14_25
# BB#24:                                # %call.sqrt446
	movaps	%xmm1, %xmm0
	movaps	%xmm10, 32(%rsp)        # 16-byte Spill
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm7, 48(%rsp)         # 16-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	movaps	%xmm13, 128(%rsp)       # 16-byte Spill
	movaps	%xmm14, 192(%rsp)       # 16-byte Spill
	movaps	%xmm4, 176(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	176(%rsp), %xmm4        # 16-byte Reload
	movaps	192(%rsp), %xmm14       # 16-byte Reload
	movaps	128(%rsp), %xmm13       # 16-byte Reload
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movaps	112(%rsp), %xmm9        # 16-byte Reload
	movaps	144(%rsp), %xmm11       # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movaps	32(%rsp), %xmm10        # 16-byte Reload
.LBB14_25:                              # %.split445
	movss	.LCPI14_7(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm14
	mulss	%xmm1, %xmm13
	mulss	%xmm1, %xmm4
	mulss	.LCPI14_12(%rip), %xmm0
	movaps	%xmm4, %xmm12
	jmp	.LBB14_26
.LBB14_20:
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm12, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB14_22
# BB#21:                                # %call.sqrt444
	movaps	%xmm1, %xmm0
	movaps	%xmm10, 32(%rsp)        # 16-byte Spill
	movaps	%xmm8, 16(%rsp)         # 16-byte Spill
	movaps	%xmm7, 48(%rsp)         # 16-byte Spill
	movss	%xmm6, 12(%rsp)         # 4-byte Spill
	movss	%xmm5, 8(%rsp)          # 4-byte Spill
	movaps	%xmm12, 128(%rsp)       # 16-byte Spill
	callq	sqrtf
	movaps	128(%rsp), %xmm12       # 16-byte Reload
	movss	8(%rsp), %xmm5          # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm6         # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	48(%rsp), %xmm7         # 16-byte Reload
	movaps	112(%rsp), %xmm9        # 16-byte Reload
	movaps	144(%rsp), %xmm11       # 16-byte Reload
	movaps	16(%rsp), %xmm8         # 16-byte Reload
	movaps	32(%rsp), %xmm10        # 16-byte Reload
.LBB14_22:                              # %.split443
	movss	.LCPI14_7(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm13
	mulss	%xmm13, %xmm12
	xorps	.LCPI14_5(%rip), %xmm12
	mulss	%xmm8, %xmm13
	xorps	%xmm0, %xmm0
	movaps	%xmm12, %xmm14
	xorps	%xmm12, %xmm12
.LBB14_26:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit
	movaps	%xmm7, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm6, %xmm2
	mulss	%xmm13, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm5, %xmm1
	mulss	%xmm12, %xmm1
	subss	%xmm1, %xmm2
	movaps	%xmm5, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm7, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm3, %xmm1
	movaps	%xmm6, %xmm3
	mulss	%xmm14, %xmm3
	subss	%xmm3, %xmm1
	movaps	%xmm6, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm5, %xmm4
	mulss	%xmm14, %xmm4
	addss	%xmm3, %xmm4
	movaps	%xmm7, %xmm3
	mulss	%xmm13, %xmm3
	subss	%xmm3, %xmm4
	mulss	%xmm14, %xmm7
	xorps	.LCPI14_5(%rip), %xmm7
	mulss	%xmm13, %xmm5
	subss	%xmm5, %xmm7
	mulss	%xmm12, %xmm6
	subss	%xmm6, %xmm7
	movaps	%xmm7, %xmm3
	mulss	%xmm14, %xmm3
	movaps	%xmm0, %xmm5
	mulss	%xmm2, %xmm5
	subss	%xmm3, %xmm5
	movaps	%xmm1, %xmm3
	mulss	%xmm12, %xmm3
	subss	%xmm3, %xmm5
	movaps	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	addss	%xmm5, %xmm3
	movaps	%xmm7, %xmm5
	mulss	%xmm13, %xmm5
	movaps	%xmm0, %xmm6
	mulss	%xmm1, %xmm6
	subss	%xmm5, %xmm6
	mulss	%xmm4, %xmm0
	mulss	%xmm14, %xmm4
	subss	%xmm4, %xmm6
	movaps	%xmm2, %xmm4
	mulss	%xmm12, %xmm4
	addss	%xmm6, %xmm4
	mulss	%xmm12, %xmm7
	subss	%xmm7, %xmm0
	mulss	%xmm13, %xmm2
	subss	%xmm2, %xmm0
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	movaps	96(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm0
	mulss	%xmm3, %xmm0
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	mulss	%xmm4, %xmm2
	addss	%xmm0, %xmm2
	mulss	%xmm1, %xmm9
	addss	%xmm2, %xmm9
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm3
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	mulss	%xmm4, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm1, %xmm11
	addss	%xmm0, %xmm11
	movaps	.LCPI14_1(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm9, %xmm1
	xorps	%xmm0, %xmm0
	ucomiss	%xmm0, %xmm11
	jae	.LBB14_27
# BB#28:
	movaps	%xmm1, %xmm0
	addss	%xmm11, %xmm0
	subss	%xmm11, %xmm1
	divss	%xmm1, %xmm0
	movss	.LCPI14_3(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	jmp	.LBB14_29
.LBB14_27:
	movaps	%xmm11, %xmm0
	subss	%xmm1, %xmm0
	addss	%xmm11, %xmm1
	divss	%xmm1, %xmm0
	movss	.LCPI14_2(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
.LBB14_29:                              # %_Z11btAtan2Fastff.exit
	mulss	.LCPI14_4(%rip), %xmm0
	addss	%xmm1, %xmm0
	movaps	.LCPI14_5(%rip), %xmm5  # xmm5 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm1
	cmpltss	%xmm1, %xmm9
	movaps	%xmm9, %xmm1
	andnps	%xmm0, %xmm1
	xorps	%xmm5, %xmm0
	andps	%xmm9, %xmm0
	orps	%xmm1, %xmm0
	movss	%xmm0, 560(%rbx)
	movss	500(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	.LCPI14_0(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	cmpltss	%xmm1, %xmm2
	movss	.LCPI14_7(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	andps	%xmm4, %xmm2
	mulss	%xmm1, %xmm2
	movaps	%xmm2, %xmm3
	xorps	%xmm5, %xmm3
	ucomiss	%xmm0, %xmm3
	jae	.LBB14_30
# BB#33:
	ucomiss	%xmm2, %xmm0
	jbe	.LBB14_38
# BB#34:
	subss	%xmm1, %xmm0
	movss	%xmm0, 556(%rbx)
	movb	$1, 573(%rbx)
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	addps	%xmm8, %xmm3
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	addss	%xmm10, %xmm2
	mulps	.LCPI14_14(%rip), %xmm3
	mulss	.LCPI14_12(%rip), %xmm2
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm3, 524(%rbx)
	movlps	%xmm1, 532(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB14_36
# BB#35:                                # %call.sqrt450
	movaps	%xmm1, %xmm0
	callq	sqrtf
	movss	.LCPI14_7(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
.LBB14_36:                              # %.split449
	divss	%xmm0, %xmm4
	movss	524(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	%xmm0, 524(%rbx)
	movss	528(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	%xmm0, 528(%rbx)
	mulss	532(%rbx), %xmm4
	jmp	.LBB14_37
.LBB14_30:
	addss	%xmm0, %xmm1
	xorps	.LCPI14_5(%rip), %xmm1
	movss	%xmm1, 556(%rbx)
	movb	$1, 573(%rbx)
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	addps	%xmm8, %xmm3
	movaps	80(%rsp), %xmm2         # 16-byte Reload
	addss	%xmm10, %xmm2
	mulps	.LCPI14_14(%rip), %xmm3
	mulss	.LCPI14_12(%rip), %xmm2
	movaps	%xmm3, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movlps	%xmm3, 524(%rbx)
	movlps	%xmm1, 532(%rbx)
	mulss	%xmm3, %xmm3
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	mulss	%xmm2, %xmm2
	addss	%xmm0, %xmm2
	movaps	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm2, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB14_32
# BB#31:                                # %call.sqrt448
	movaps	%xmm1, %xmm0
	callq	sqrtf
	movaps	.LCPI14_5(%rip), %xmm5  # xmm5 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movss	.LCPI14_7(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
.LBB14_32:                              # %.split447
	divss	%xmm0, %xmm4
	movss	524(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	movss	528(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm1
	mulss	532(%rbx), %xmm4
	xorps	%xmm5, %xmm0
	movss	%xmm0, 524(%rbx)
	xorps	%xmm5, %xmm1
	movss	%xmm1, 528(%rbx)
	xorps	%xmm5, %xmm4
.LBB14_37:
	movss	%xmm4, 532(%rbx)
.LBB14_38:
	addq	$208, %rsp
	popq	%rbx
	retq
.Lfunc_end14:
	.size	_ZN21btConeTwistConstraint13calcAngleInfoEv, .Lfunc_end14-_ZN21btConeTwistConstraint13calcAngleInfoEv
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI15_0:
	.long	872415232               # float 1.1920929E-7
.LCPI15_1:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI15_2:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.text
	.globl	_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_,@function
_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_: # @_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi77:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi78:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi79:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi80:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 48
.Lcfi82:
	.cfi_offset %rbx, -48
.Lcfi83:
	.cfi_offset %r12, -40
.Lcfi84:
	.cfi_offset %r13, -32
.Lcfi85:
	.cfi_offset %r14, -24
.Lcfi86:
	.cfi_offset %r15, -16
	movq	%r8, %r14
	movq	%rcx, %r12
	movq	%rdx, %r13
	movq	%rsi, %rbx
	movq	%rdi, %r15
	movss	12(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r13)
	ucomiss	.LCPI15_0(%rip), %xmm0
	jbe	.LBB15_7
# BB#1:
	movl	(%rbx), %eax
	movl	4(%rbx), %ecx
	movl	8(%rbx), %edx
	movl	%eax, (%r12)
	movl	%ecx, 4(%r12)
	movl	%edx, 8(%r12)
	movl	$0, 12(%r12)
	movd	%eax, %xmm0
	mulss	%xmm0, %xmm0
	movd	%ecx, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%edx, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB15_3
# BB#2:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB15_3:                               # %.split
	movss	.LCPI15_1(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	divss	%xmm1, %xmm0
	movss	(%r12), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r12)
	movss	4(%r12), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%r12)
	mulss	8(%r12), %xmm0
	movss	%xmm0, 8(%r12)
	movl	492(%r15), %eax
	movl	%eax, (%r14)
	movaps	.LCPI15_2(%rip), %xmm3  # xmm3 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm3
	ucomiss	.LCPI15_0(%rip), %xmm3
	jbe	.LBB15_7
# BB#4:
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	divss	%xmm1, %xmm0
	movss	492(%r15), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	496(%r15), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm3
	movaps	%xmm2, %xmm4
	divss	%xmm3, %xmm4
	mulss	%xmm1, %xmm1
	movaps	%xmm0, %xmm3
	divss	%xmm1, %xmm3
	addss	%xmm4, %xmm3
	addss	%xmm2, %xmm0
	divss	%xmm3, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB15_6
# BB#5:                                 # %call.sqrt48
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB15_6:                               # %.split47
	movss	%xmm1, (%r14)
.LBB15_7:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_, .Lfunc_end15-_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI16_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI16_1:
	.long	872415232               # float 1.1920929E-7
.LCPI16_2:
	.long	2147483648              # float -0
.LCPI16_3:
	.long	1065353216              # float 1
	.text
	.globl	_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3
	.p2align	4, 0x90
	.type	_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3,@function
_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3: # @_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi87:
	.cfi_def_cfa_offset 16
.Lcfi88:
	.cfi_offset %rbx, -16
	movq	%rsi, %rbx
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	movaps	.LCPI16_0(%rip), %xmm0  # xmm0 = [nan,nan,nan,nan]
	andps	%xmm1, %xmm0
	ucomiss	.LCPI16_1(%rip), %xmm0
	jbe	.LBB16_4
# BB#1:
	movss	8(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	movss	.LCPI16_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	cmpltss	%xmm2, %xmm0
	shufps	$0, %xmm2, %xmm2        # xmm2 = xmm2[0,0,0,0]
	xorps	%xmm2, %xmm3
	divss	%xmm1, %xmm3
	movss	496(%rdi), %xmm4        # xmm4 = mem[0],zero,zero,zero
	divss	492(%rdi), %xmm4
	mulss	%xmm3, %xmm4
	mulss	%xmm1, %xmm4
	movaps	.LCPI16_0(%rip), %xmm3  # xmm3 = [nan,nan,nan,nan]
	andps	%xmm4, %xmm3
	orps	%xmm2, %xmm4
	andps	%xmm0, %xmm3
	andnps	%xmm4, %xmm0
	orps	%xmm3, %xmm0
	xorps	%xmm0, %xmm2
	movss	%xmm2, 8(%rbx)
	movss	(%rbx), %xmm2           # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	mulss	%xmm1, %xmm1
	addss	%xmm2, %xmm1
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB16_3
# BB#2:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB16_3:                               # %.split
	movss	.LCPI16_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%rbx), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rbx)
	movss	4(%rbx), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%rbx)
	mulss	8(%rbx), %xmm0
	movss	%xmm0, 8(%rbx)
.LBB16_4:
	popq	%rbx
	retq
.Lfunc_end16:
	.size	_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3, .Lfunc_end16-_ZNK21btConeTwistConstraint33adjustSwingAxisToUseEllipseNormalER9btVector3
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI17_0:
	.long	1078530011              # float 3.14159274
.LCPI17_2:
	.long	872415232               # float 1.1920929E-7
.LCPI17_3:
	.long	1065353216              # float 1
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI17_1:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3,@function
_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3: # @_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi89:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi90:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi91:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi92:
	.cfi_def_cfa_offset 80
.Lcfi93:
	.cfi_offset %rbx, -32
.Lcfi94:
	.cfi_offset %r14, -24
.Lcfi95:
	.cfi_offset %r15, -16
	movq	%rcx, %r15
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movsd	(%rbx), %xmm0           # xmm0 = mem[0],zero
	movaps	%xmm0, 16(%rsp)         # 16-byte Spill
	movsd	8(%rbx), %xmm0          # xmm0 = mem[0],zero
	movaps	%xmm0, (%rsp)           # 16-byte Spill
	movss	12(%rbx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	ucomiss	.LCPI17_0(%rip), %xmm0
	jbe	.LBB17_2
# BB#1:
	movq	(%rbx), %xmm1           # xmm1 = mem[0],zero
	movdqa	.LCPI17_1(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, 16(%rsp)         # 16-byte Spill
	movq	8(%rbx), %xmm1          # xmm1 = mem[0],zero
	pxor	%xmm0, %xmm1
	movdqa	%xmm1, (%rsp)           # 16-byte Spill
	pshufd	$229, %xmm1, %xmm0      # xmm0 = xmm1[1,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
.LBB17_2:
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	shufps	$212, %xmm2, %xmm2      # xmm2 = xmm2[0,1,1,3]
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	movaps	(%rsp), %xmm3           # 16-byte Reload
	shufps	$212, %xmm3, %xmm3      # xmm3 = xmm3[0,1,1,3]
	movd	%xmm2, (%r15)
	movd	%xmm0, 4(%r15)
	movss	%xmm3, 8(%r15)
	movl	$0, 12(%r15)
	movss	(%r14), %xmm1           # xmm1 = mem[0],zero,zero,zero
	ucomiss	.LCPI17_2(%rip), %xmm1
	jbe	.LBB17_6
# BB#3:
	movd	%xmm2, %eax
	movd	%xmm0, %ecx
	movd	%xmm3, %edx
	movd	%eax, %xmm1
	movd	%ecx, %xmm2
	movd	%edx, %xmm0
	mulss	%xmm1, %xmm1
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB17_5
# BB#4:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB17_5:                               # %.split
	movss	.LCPI17_3(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	movss	(%r15), %xmm1           # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%r15)
	movss	4(%r15), %xmm1          # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm1
	movss	%xmm1, 4(%r15)
	mulss	8(%r15), %xmm0
	movss	%xmm0, 8(%r15)
.LBB17_6:
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end17:
	.size	_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3, .Lfunc_end17-_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI18_0:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI18_1:
	.long	872415232               # float 1.1920929E-7
.LCPI18_2:
	.long	1065353216              # float 1
.LCPI18_3:
	.long	1056964608              # float 0.5
.LCPI18_4:
	.long	2147483648              # float -0
	.text
	.globl	_ZNK21btConeTwistConstraint16GetPointForAngleEff
	.p2align	4, 0x90
	.type	_ZNK21btConeTwistConstraint16GetPointForAngleEff,@function
_ZNK21btConeTwistConstraint16GetPointForAngleEff: # @_ZNK21btConeTwistConstraint16GetPointForAngleEff
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 16
	subq	$80, %rsp
.Lcfi97:
	.cfi_def_cfa_offset 96
.Lcfi98:
	.cfi_offset %rbx, -16
	movaps	%xmm1, 64(%rsp)         # 16-byte Spill
	movss	%xmm0, 32(%rsp)         # 4-byte Spill
	movq	%rdi, %rbx
	callq	cosf
	movaps	%xmm0, 48(%rsp)         # 16-byte Spill
	movss	32(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	sinf
	movaps	48(%rsp), %xmm5         # 16-byte Reload
	movss	492(%rbx), %xmm4        # xmm4 = mem[0],zero,zero,zero
	movaps	.LCPI18_0(%rip), %xmm2  # xmm2 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm2
	ucomiss	.LCPI18_1(%rip), %xmm2
	movaps	%xmm0, 32(%rsp)         # 16-byte Spill
	jbe	.LBB18_1
# BB#2:
	movaps	%xmm0, %xmm6
	mulss	%xmm6, %xmm6
	mulss	%xmm5, %xmm5
	movaps	%xmm6, %xmm0
	divss	%xmm5, %xmm0
	movss	496(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm1
	movss	.LCPI18_2(%rip), %xmm2  # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm3
	addss	%xmm2, %xmm0
	divss	%xmm1, %xmm2
	mulss	%xmm4, %xmm4
	divss	%xmm4, %xmm3
	addss	%xmm2, %xmm3
	divss	%xmm3, %xmm0
	xorps	%xmm4, %xmm4
	sqrtss	%xmm0, %xmm4
	ucomiss	%xmm4, %xmm4
	jnp	.LBB18_4
# BB#3:                                 # %call.sqrt
	movaps	%xmm5, 16(%rsp)         # 16-byte Spill
	movss	%xmm6, (%rsp)           # 4-byte Spill
	callq	sqrtf
	movss	(%rsp), %xmm6           # 4-byte Reload
                                        # xmm6 = mem[0],zero,zero,zero
	movaps	16(%rsp), %xmm5         # 16-byte Reload
	movaps	%xmm0, %xmm4
	jmp	.LBB18_4
.LBB18_1:                               # %._crit_edge
	mulss	%xmm5, %xmm5
	movaps	%xmm0, %xmm6
	mulss	%xmm6, %xmm6
.LBB18_4:
	xorps	%xmm0, %xmm0
	addss	%xmm0, %xmm5
	addss	%xmm6, %xmm5
	xorps	%xmm0, %xmm0
	sqrtss	%xmm5, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB18_6
# BB#5:                                 # %call.sqrt56
	movaps	%xmm5, %xmm0
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	callq	sqrtf
	movss	16(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
.LBB18_6:                               # %.split55
	movss	%xmm0, (%rsp)           # 4-byte Spill
	mulss	.LCPI18_3(%rip), %xmm4
	movss	%xmm4, 16(%rsp)         # 4-byte Spill
	movaps	%xmm4, %xmm0
	callq	sinf
	divss	(%rsp), %xmm0           # 4-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movaps	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	mulss	%xmm0, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movss	16(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	cosf
	movaps	%xmm0, %xmm8
	movaps	64(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm3, %xmm8
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	%xmm10, %xmm12
	xorps	%xmm1, %xmm1
	mulss	%xmm1, %xmm12
	addss	%xmm12, %xmm8
	movss	.LCPI18_4(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm6
	mulss	%xmm4, %xmm6
	subss	%xmm6, %xmm8
	movaps	%xmm0, %xmm5
	mulss	%xmm1, %xmm5
	movaps	%xmm2, %xmm7
	movaps	%xmm2, %xmm11
	mulss	%xmm3, %xmm7
	movaps	%xmm5, %xmm9
	subss	%xmm7, %xmm9
	movaps	(%rsp), %xmm2           # 16-byte Reload
	mulss	%xmm2, %xmm1
	addss	%xmm1, %xmm5
	movaps	%xmm10, %xmm7
	mulss	%xmm3, %xmm7
	subss	%xmm7, %xmm5
	mulss	%xmm2, %xmm3
	unpcklps	%xmm4, %xmm9    # xmm9 = xmm9[0],xmm4[0],xmm9[1],xmm4[1]
	shufps	$0, %xmm4, %xmm4        # xmm4 = xmm4[0,0,0,0]
	xorps	%xmm4, %xmm3
	subss	%xmm12, %xmm3
	subss	%xmm6, %xmm3
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	subps	%xmm1, %xmm9
	xorps	%xmm10, %xmm4
	movaps	%xmm3, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	movaps	%xmm9, %xmm6
	shufps	$229, %xmm6, %xmm6      # xmm6 = xmm6[1,1,2,3]
	movaps	%xmm4, %xmm7
	shufps	$0, %xmm6, %xmm7        # xmm7 = xmm7[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm7      # xmm7 = xmm7[2,0],xmm6[2,3]
	mulps	%xmm2, %xmm7
	movaps	%xmm0, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm8, %xmm2
	unpcklps	%xmm9, %xmm2    # xmm2 = xmm2[0],xmm9[0],xmm2[1],xmm9[1]
	mulps	%xmm1, %xmm2
	addps	%xmm7, %xmm2
	movaps	%xmm11, %xmm1
	unpcklps	%xmm5, %xmm1    # xmm1 = xmm1[0],xmm5[0],xmm1[1],xmm5[1]
	mulps	%xmm9, %xmm1
	addps	%xmm1, %xmm2
	mulss	%xmm5, %xmm0
	unpcklps	%xmm11, %xmm5   # xmm5 = xmm5[0],xmm11[0],xmm5[1],xmm11[1]
	unpcklps	%xmm8, %xmm4    # xmm4 = xmm4[0],xmm8[0],xmm4[1],xmm8[1]
	mulps	%xmm5, %xmm4
	subps	%xmm4, %xmm2
	mulss	%xmm11, %xmm3
	addss	%xmm0, %xmm3
	mulss	%xmm10, %xmm8
	subss	%xmm8, %xmm3
	mulss	%xmm9, %xmm6
	subss	%xmm6, %xmm3
	xorps	%xmm1, %xmm1
	movss	%xmm3, %xmm1            # xmm1 = xmm3[0],xmm1[1,2,3]
	movaps	%xmm2, %xmm0
	addq	$80, %rsp
	popq	%rbx
	retq
.Lfunc_end18:
	.size	_ZNK21btConeTwistConstraint16GetPointForAngleEff, .Lfunc_end18-_ZNK21btConeTwistConstraint16GetPointForAngleEff
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI19_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.text
	.globl	_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion,@function
_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion: # @_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi99:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi100:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi101:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi102:
	.cfi_def_cfa_offset 40
	subq	$568, %rsp              # imm = 0x238
.Lcfi103:
	.cfi_def_cfa_offset 608
.Lcfi104:
	.cfi_offset %rbx, -40
.Lcfi105:
	.cfi_offset %r12, -32
.Lcfi106:
	.cfi_offset %r14, -24
.Lcfi107:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	24(%r12), %rax
	movq	32(%r12), %rcx
	movss	8(%rax), %xmm12         # xmm12 = mem[0],zero,zero,zero
	movss	24(%rax), %xmm9         # xmm9 = mem[0],zero,zero,zero
	movsd	8(%rcx), %xmm6          # xmm6 = mem[0],zero
	movsd	24(%rcx), %xmm4         # xmm4 = mem[0],zero
	movss	16(%rcx), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movss	32(%rcx), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm11, 192(%rsp)       # 16-byte Spill
	movsd	40(%rcx), %xmm7         # xmm7 = mem[0],zero
	movss	48(%rcx), %xmm10        # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, 208(%rsp)       # 16-byte Spill
	movd	56(%rcx), %xmm15        # xmm15 = mem[0],zero,zero,zero
	movdqa	%xmm15, 320(%rsp)       # 16-byte Spill
	movd	60(%rcx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movd	64(%rcx), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movdqa	.LCPI19_0(%rip), %xmm3  # xmm3 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	pxor	%xmm3, %xmm15
	movdqa	%xmm2, %xmm1
	movdqa	%xmm2, %xmm5
	movdqa	%xmm5, 496(%rsp)        # 16-byte Spill
	pxor	%xmm3, %xmm1
	movdqa	%xmm0, %xmm2
	movdqa	%xmm0, 304(%rsp)        # 16-byte Spill
	pxor	%xmm3, %xmm2
	pshufd	$224, %xmm15, %xmm3     # xmm3 = xmm15[0,0,2,3]
	mulps	%xmm6, %xmm3
	pshufd	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	mulps	%xmm4, %xmm1
	addps	%xmm3, %xmm1
	pshufd	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm7, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm2, 128(%rsp)        # 16-byte Spill
	mulss	%xmm8, %xmm15
	movaps	%xmm11, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm1, %xmm15
	movaps	%xmm10, %xmm1
	mulss	%xmm0, %xmm1
	subss	%xmm1, %xmm15
	movaps	%xmm12, %xmm1
	mulss	%xmm6, %xmm1
	movaps	%xmm9, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm2
	movss	40(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm0
	movaps	%xmm1, %xmm10
	mulss	%xmm7, %xmm0
	movaps	%xmm7, 112(%rsp)        # 16-byte Spill
	addss	%xmm2, %xmm0
	movss	%xmm0, 256(%rsp)        # 4-byte Spill
	movss	12(%rax), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm11
	mulss	%xmm6, %xmm1
	movaps	%xmm6, %xmm0
	movaps	%xmm0, 64(%rsp)         # 16-byte Spill
	movss	28(%rax), %xmm3         # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	movaps	%xmm3, %xmm13
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm2
	movss	44(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	movaps	%xmm1, %xmm6
	movss	%xmm6, 272(%rsp)        # 4-byte Spill
	mulss	%xmm7, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 240(%rsp)        # 4-byte Spill
	movss	16(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm1
	movss	32(%rax), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm2
	movaps	%xmm4, 96(%rsp)         # 16-byte Spill
	mulss	%xmm4, %xmm2
	addss	%xmm1, %xmm2
	movss	48(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm3
	movaps	%xmm1, %xmm14
	mulss	%xmm7, %xmm3
	addss	%xmm2, %xmm3
	movss	%xmm3, 224(%rsp)        # 4-byte Spill
	pshufd	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movss	%xmm12, 8(%rsp)         # 4-byte Spill
	movaps	%xmm12, %xmm1
	mulss	%xmm0, %xmm1
	pshufd	$229, %xmm4, %xmm5      # xmm5 = xmm4[1,1,2,3]
	movss	%xmm9, 56(%rsp)         # 4-byte Spill
	movaps	%xmm9, %xmm2
	mulss	%xmm5, %xmm2
	movaps	%xmm5, 464(%rsp)        # 16-byte Spill
	addss	%xmm1, %xmm2
	pshufd	$229, %xmm7, %xmm4      # xmm4 = xmm7[1,1,2,3]
	movaps	%xmm10, %xmm3
	movss	%xmm3, 60(%rsp)         # 4-byte Spill
	movaps	%xmm3, %xmm7
	mulss	%xmm4, %xmm7
	addss	%xmm2, %xmm7
	movss	%xmm7, 288(%rsp)        # 4-byte Spill
	movaps	%xmm11, %xmm1
	movaps	%xmm11, %xmm10
	movss	%xmm10, 4(%rsp)         # 4-byte Spill
	mulss	%xmm0, %xmm1
	movaps	%xmm0, 80(%rsp)         # 16-byte Spill
	movaps	%xmm13, %xmm2
	movaps	%xmm13, %xmm11
	movss	%xmm11, 12(%rsp)        # 4-byte Spill
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm6, %xmm13
	mulss	%xmm4, %xmm13
	movaps	%xmm4, 480(%rsp)        # 16-byte Spill
	addss	%xmm2, %xmm13
	movaps	144(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm1
	mulss	%xmm0, %xmm1
	movaps	%xmm8, 160(%rsp)        # 16-byte Spill
	movaps	%xmm8, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm14, %xmm6
	movaps	%xmm6, 176(%rsp)        # 16-byte Spill
	mulss	%xmm4, %xmm14
	addss	%xmm2, %xmm14
	movaps	%xmm12, %xmm1
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm9, %xmm2
	movaps	192(%rsp), %xmm12       # 16-byte Reload
	mulss	%xmm12, %xmm2
	addss	%xmm1, %xmm2
	movaps	%xmm3, %xmm9
	movaps	208(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm1, %xmm9
	addss	%xmm2, %xmm9
	mulss	%xmm0, %xmm10
	mulss	%xmm12, %xmm11
	addss	%xmm10, %xmm11
	movss	272(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm2
	addss	%xmm11, %xmm2
	mulss	%xmm0, %xmm7
	movaps	%xmm0, %xmm10
	movaps	%xmm8, %xmm4
	mulss	%xmm12, %xmm4
	addss	%xmm7, %xmm4
	movaps	%xmm6, %xmm3
	mulss	%xmm1, %xmm3
	movaps	%xmm1, %xmm8
	addss	%xmm4, %xmm3
	movss	56(%rax), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	movaps	%xmm1, 512(%rsp)        # 16-byte Spill
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	64(%rsp), %xmm4         # 16-byte Folded Reload
	movss	60(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm5
	movaps	%xmm6, %xmm0
	movaps	%xmm0, 528(%rsp)        # 16-byte Spill
	shufps	$224, %xmm5, %xmm5      # xmm5 = xmm5[0,0,2,3]
	mulps	96(%rsp), %xmm5         # 16-byte Folded Reload
	addps	%xmm4, %xmm5
	movss	64(%rax), %xmm6         # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm6, %xmm4
	movaps	%xmm6, %xmm7
	movaps	%xmm7, 544(%rsp)        # 16-byte Spill
	shufps	$224, %xmm4, %xmm4      # xmm4 = xmm4[0,0,2,3]
	mulps	112(%rsp), %xmm4        # 16-byte Folded Reload
	addps	%xmm5, %xmm4
	addps	128(%rsp), %xmm4        # 16-byte Folded Reload
	mulss	%xmm10, %xmm1
	movaps	%xmm0, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm1, %xmm6
	movaps	%xmm7, %xmm5
	mulss	%xmm8, %xmm5
	addss	%xmm6, %xmm5
	addss	%xmm15, %xmm5
	xorps	%xmm0, %xmm0
	movss	%xmm5, %xmm0            # xmm0 = xmm5[0],xmm0[1,2,3]
	movss	256(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 400(%rsp)
	movss	240(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 404(%rsp)
	movss	224(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 408(%rsp)
	movl	$0, 412(%rsp)
	movss	288(%rsp), %xmm1        # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movss	%xmm1, 416(%rsp)
	movss	%xmm13, 420(%rsp)
	movss	%xmm14, 424(%rsp)
	movl	$0, 428(%rsp)
	movss	%xmm9, 432(%rsp)
	movss	%xmm2, 436(%rsp)
	movss	%xmm3, 440(%rsp)
	movl	$0, 444(%rsp)
	movlps	%xmm4, 448(%rsp)
	movlps	%xmm0, 456(%rsp)
	leaq	400(%rsp), %rdi
	leaq	336(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	412(%r12), %xmm11       # xmm11 = mem[0],zero
	movdqa	64(%rsp), %xmm4         # 16-byte Reload
	pshufd	$224, %xmm4, %xmm1      # xmm1 = xmm4[0,0,2,3]
	mulps	%xmm11, %xmm1
	movsd	428(%r12), %xmm0        # xmm0 = mem[0],zero
	movaps	80(%rsp), %xmm6         # 16-byte Reload
	movaps	%xmm6, %xmm3
	mulps	%xmm0, %xmm3
	addps	%xmm1, %xmm3
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm2, %xmm1
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movsd	444(%r12), %xmm12       # xmm12 = mem[0],zero
	mulps	%xmm12, %xmm1
	addps	%xmm3, %xmm1
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movss	420(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movdqa	%xmm4, %xmm5
	mulss	%xmm3, %xmm5
	movss	436(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm6
	addss	%xmm5, %xmm6
	movss	452(%r12), %xmm7        # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	mulss	%xmm7, %xmm5
	addss	%xmm6, %xmm5
	movaps	%xmm5, 256(%rsp)        # 16-byte Spill
	movdqa	96(%rsp), %xmm8         # 16-byte Reload
	pshufd	$224, %xmm8, %xmm5      # xmm5 = xmm8[0,0,2,3]
	mulps	%xmm11, %xmm5
	movaps	464(%rsp), %xmm13       # 16-byte Reload
	movaps	%xmm13, %xmm6
	mulps	%xmm0, %xmm6
	addps	%xmm5, %xmm6
	movaps	192(%rsp), %xmm14       # 16-byte Reload
	movaps	%xmm14, %xmm15
	shufps	$224, %xmm15, %xmm15    # xmm15 = xmm15[0,0,2,3]
	mulps	%xmm12, %xmm15
	addps	%xmm6, %xmm15
	movaps	%xmm15, 240(%rsp)       # 16-byte Spill
	movdqa	%xmm8, %xmm5
	movdqa	%xmm8, %xmm10
	mulss	%xmm3, %xmm5
	movaps	%xmm13, %xmm6
	mulss	%xmm1, %xmm6
	addss	%xmm5, %xmm6
	movaps	%xmm14, %xmm5
	mulss	%xmm7, %xmm5
	addss	%xmm6, %xmm5
	movaps	%xmm5, %xmm9
	movaps	%xmm9, 288(%rsp)        # 16-byte Spill
	movdqa	112(%rsp), %xmm8        # 16-byte Reload
	pshufd	$224, %xmm8, %xmm5      # xmm5 = xmm8[0,0,2,3]
	mulps	%xmm11, %xmm5
	movaps	480(%rsp), %xmm6        # 16-byte Reload
	mulps	%xmm6, %xmm0
	addps	%xmm5, %xmm0
	movaps	208(%rsp), %xmm11       # 16-byte Reload
	movaps	%xmm11, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm12, %xmm2
	addps	%xmm0, %xmm2
	movaps	%xmm2, %xmm12
	movaps	%xmm12, 224(%rsp)       # 16-byte Spill
	mulss	%xmm8, %xmm3
	movaps	%xmm8, %xmm5
	mulss	%xmm6, %xmm1
	movaps	%xmm6, %xmm8
	addss	%xmm3, %xmm1
	mulss	%xmm11, %xmm7
	addss	%xmm1, %xmm7
	movss	%xmm7, 52(%rsp)         # 4-byte Spill
	movss	460(%r12), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	mulss	%xmm0, %xmm2
	movss	464(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movaps	80(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm1, %xmm3
	addss	%xmm2, %xmm3
	movaps	%xmm3, %xmm4
	movss	468(%r12), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm3         # 16-byte Reload
	mulss	%xmm2, %xmm3
	addss	%xmm4, %xmm3
	addss	320(%rsp), %xmm3        # 16-byte Folded Reload
	movaps	%xmm3, %xmm4
	mulss	%xmm0, %xmm10
	mulss	%xmm1, %xmm13
	addss	%xmm10, %xmm13
	mulss	%xmm2, %xmm14
	addss	%xmm13, %xmm14
	addss	496(%rsp), %xmm14       # 16-byte Folded Reload
	movaps	%xmm5, %xmm3
	mulss	%xmm0, %xmm3
	mulss	%xmm1, %xmm8
	addss	%xmm3, %xmm8
	mulss	%xmm2, %xmm11
	addss	%xmm8, %xmm11
	addss	304(%rsp), %xmm11       # 16-byte Folded Reload
	movaps	.LCPI19_0(%rip), %xmm1  # xmm1 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm1, %xmm4
	movaps	%xmm9, %xmm0
	mulss	%xmm14, %xmm0
	xorps	%xmm1, %xmm14
	movaps	%xmm1, %xmm3
	pshufd	$224, %xmm4, %xmm1      # xmm1 = xmm4[0,0,2,3]
	mulps	128(%rsp), %xmm1        # 16-byte Folded Reload
	pshufd	$224, %xmm14, %xmm2     # xmm2 = xmm14[0,0,2,3]
	mulps	%xmm15, %xmm2
	addps	%xmm1, %xmm2
	movaps	%xmm7, %xmm1
	mulss	%xmm11, %xmm1
	xorps	%xmm3, %xmm11
	pshufd	$224, %xmm11, %xmm3     # xmm3 = xmm11[0,0,2,3]
	mulps	%xmm12, %xmm3
	addps	%xmm2, %xmm3
	movaps	%xmm3, 208(%rsp)        # 16-byte Spill
	mulss	256(%rsp), %xmm4        # 16-byte Folded Reload
	subss	%xmm0, %xmm4
	subss	%xmm1, %xmm4
	movaps	%xmm4, 32(%rsp)         # 16-byte Spill
	movss	348(%r12), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm2          # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm0
	mulss	%xmm9, %xmm0
	movss	364(%r12), %xmm12       # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm1
	mulss	%xmm12, %xmm1
	addss	%xmm0, %xmm1
	movss	380(%r12), %xmm6        # xmm6 = mem[0],zero,zero,zero
	movaps	144(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm4
	mulss	%xmm6, %xmm4
	addss	%xmm1, %xmm4
	movss	%xmm4, 64(%rsp)         # 4-byte Spill
	movss	352(%r12), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm1
	movaps	%xmm2, %xmm7
	mulss	%xmm10, %xmm1
	movss	368(%r12), %xmm5        # xmm5 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	movaps	%xmm3, %xmm4
	mulss	%xmm5, %xmm2
	addss	%xmm1, %xmm2
	movss	384(%r12), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	mulss	%xmm8, %xmm1
	addss	%xmm2, %xmm1
	movss	%xmm1, 112(%rsp)        # 4-byte Spill
	movss	356(%r12), %xmm13       # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm2
	mulss	%xmm13, %xmm2
	movss	372(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	addss	%xmm2, %xmm4
	movss	388(%r12), %xmm7        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	addss	%xmm4, %xmm0
	movss	%xmm0, 192(%rsp)        # 4-byte Spill
	movss	56(%rsp), %xmm15        # 4-byte Reload
                                        # xmm15 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	mulss	%xmm9, %xmm2
	movss	12(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm4
	mulss	%xmm12, %xmm4
	addss	%xmm2, %xmm4
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	movaps	%xmm0, %xmm14
	mulss	%xmm6, %xmm14
	addss	%xmm4, %xmm14
	movaps	%xmm15, %xmm2
	mulss	%xmm10, %xmm2
	movaps	%xmm1, %xmm4
	movaps	%xmm1, %xmm11
	mulss	%xmm5, %xmm4
	addss	%xmm2, %xmm4
	movaps	%xmm0, %xmm1
	movaps	%xmm0, %xmm2
	mulss	%xmm8, %xmm1
	addss	%xmm4, %xmm1
	movss	%xmm1, 96(%rsp)         # 4-byte Spill
	movaps	%xmm15, %xmm4
	mulss	%xmm13, %xmm4
	movaps	%xmm11, %xmm0
	mulss	%xmm3, %xmm0
	addss	%xmm4, %xmm0
	movaps	%xmm2, %xmm11
	movaps	%xmm2, %xmm4
	mulss	%xmm7, %xmm11
	addss	%xmm0, %xmm11
	movss	60(%rsp), %xmm1         # 4-byte Reload
                                        # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm9
	movss	272(%rsp), %xmm2        # 4-byte Reload
                                        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm12
	addss	%xmm9, %xmm12
	movaps	176(%rsp), %xmm9        # 16-byte Reload
	mulss	%xmm9, %xmm6
	addss	%xmm12, %xmm6
	movaps	%xmm6, %xmm12
	movss	%xmm12, 304(%rsp)       # 4-byte Spill
	mulss	%xmm1, %xmm10
	mulss	%xmm2, %xmm5
	addss	%xmm10, %xmm5
	mulss	%xmm9, %xmm8
	movaps	%xmm9, %xmm10
	addss	%xmm5, %xmm8
	movss	%xmm8, 80(%rsp)         # 4-byte Spill
	mulss	%xmm1, %xmm13
	movaps	%xmm1, %xmm9
	mulss	%xmm2, %xmm3
	addss	%xmm13, %xmm3
	mulss	%xmm10, %xmm7
	addss	%xmm3, %xmm7
	movss	%xmm7, 320(%rsp)        # 4-byte Spill
	movss	396(%r12), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movss	8(%rsp), %xmm8          # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm8
	movss	400(%r12), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	4(%rsp), %xmm3          # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm3
	addss	%xmm8, %xmm3
	movaps	%xmm3, %xmm5
	movss	404(%r12), %xmm3        # xmm3 = mem[0],zero,zero,zero
	movaps	144(%rsp), %xmm13       # 16-byte Reload
	mulss	%xmm3, %xmm13
	addss	%xmm5, %xmm13
	addss	512(%rsp), %xmm13       # 16-byte Folded Reload
	mulss	%xmm0, %xmm15
	movss	12(%rsp), %xmm5         # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm1, %xmm5
	addss	%xmm15, %xmm5
	mulss	%xmm3, %xmm4
	addss	%xmm5, %xmm4
	addss	528(%rsp), %xmm4        # 16-byte Folded Reload
	movaps	%xmm4, 160(%rsp)        # 16-byte Spill
	mulss	%xmm0, %xmm9
	mulss	%xmm1, %xmm2
	addss	%xmm9, %xmm2
	movaps	%xmm10, %xmm0
	mulss	%xmm3, %xmm0
	addss	%xmm2, %xmm0
	addss	544(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm0, 176(%rsp)        # 16-byte Spill
	movaps	128(%rsp), %xmm5        # 16-byte Reload
	movaps	%xmm5, %xmm0
	movss	64(%rsp), %xmm10        # 4-byte Reload
                                        # xmm10 = mem[0],zero,zero,zero
	mulss	%xmm10, %xmm0
	movaps	240(%rsp), %xmm2        # 16-byte Reload
	movaps	%xmm2, %xmm1
	mulss	%xmm14, %xmm1
	addss	%xmm0, %xmm1
	movaps	224(%rsp), %xmm6        # 16-byte Reload
	movaps	%xmm6, %xmm0
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 144(%rsp)        # 16-byte Spill
	movaps	%xmm5, %xmm0
	movss	112(%rsp), %xmm9        # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	movaps	%xmm2, %xmm1
	mulss	96(%rsp), %xmm1         # 4-byte Folded Reload
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm0
	movss	80(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm4, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm0, 272(%rsp)        # 16-byte Spill
	movaps	%xmm5, %xmm0
	movss	192(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm7, %xmm0
	movaps	%xmm2, %xmm1
	mulss	%xmm11, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm6, %xmm12
	movaps	%xmm6, %xmm15
	movss	320(%rsp), %xmm8        # 4-byte Reload
                                        # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm8, %xmm12
	addss	%xmm1, %xmm12
	movaps	%xmm5, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm3, %xmm6
	mulss	%xmm10, %xmm6
	movaps	%xmm2, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm1, %xmm0
	mulss	%xmm14, %xmm0
	addss	%xmm6, %xmm0
	movaps	%xmm15, %xmm10
	shufps	$229, %xmm10, %xmm10    # xmm10 = xmm10[1,1,2,3]
	movaps	%xmm10, %xmm15
	movss	304(%rsp), %xmm5        # 4-byte Reload
                                        # xmm5 = mem[0],zero,zero,zero
	mulss	%xmm5, %xmm15
	addss	%xmm0, %xmm15
	movaps	%xmm3, %xmm6
	mulss	%xmm9, %xmm6
	movaps	%xmm1, %xmm0
	movss	96(%rsp), %xmm9         # 4-byte Reload
                                        # xmm9 = mem[0],zero,zero,zero
	mulss	%xmm9, %xmm0
	addss	%xmm6, %xmm0
	movaps	%xmm10, %xmm6
	mulss	%xmm4, %xmm6
	addss	%xmm0, %xmm6
	mulss	%xmm7, %xmm3
	movaps	%xmm7, %xmm2
	mulss	%xmm11, %xmm1
	addss	%xmm3, %xmm1
	mulss	%xmm8, %xmm10
	addss	%xmm1, %xmm10
	movaps	256(%rsp), %xmm0        # 16-byte Reload
	movss	64(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movaps	288(%rsp), %xmm1        # 16-byte Reload
	mulss	%xmm1, %xmm14
	addss	%xmm3, %xmm14
	movss	52(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm5
	addss	%xmm14, %xmm5
	movss	112(%rsp), %xmm7        # 4-byte Reload
                                        # xmm7 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm7
	mulss	%xmm1, %xmm9
	addss	%xmm7, %xmm9
	movss	80(%rsp), %xmm4         # 4-byte Reload
                                        # xmm4 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm4
	movaps	%xmm3, %xmm7
	addss	%xmm9, %xmm4
	mulss	%xmm0, %xmm2
	mulss	%xmm1, %xmm11
	movaps	%xmm1, %xmm3
	addss	%xmm2, %xmm11
	mulss	%xmm7, %xmm8
	addss	%xmm11, %xmm8
	mulss	%xmm13, %xmm0
	movaps	%xmm0, %xmm2
	shufps	$224, %xmm13, %xmm13    # xmm13 = xmm13[0,0,2,3]
	mulps	128(%rsp), %xmm13       # 16-byte Folded Reload
	movaps	160(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm3
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	240(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm13, %xmm0
	movaps	%xmm0, %xmm1
	movaps	176(%rsp), %xmm0        # 16-byte Reload
	mulss	%xmm0, %xmm7
	shufps	$224, %xmm0, %xmm0      # xmm0 = xmm0[0,0,2,3]
	mulps	224(%rsp), %xmm0        # 16-byte Folded Reload
	addps	%xmm1, %xmm0
	addps	208(%rsp), %xmm0        # 16-byte Folded Reload
	movaps	%xmm3, %xmm1
	addss	%xmm2, %xmm1
	addss	%xmm7, %xmm1
	addss	32(%rsp), %xmm1         # 16-byte Folded Reload
	movaps	%xmm1, %xmm2
	xorps	%xmm1, %xmm1
	movss	%xmm2, %xmm1            # xmm1 = xmm2[0],xmm1[1,2,3]
	movaps	144(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 336(%rsp)
	movaps	272(%rsp), %xmm2        # 16-byte Reload
	movss	%xmm2, 340(%rsp)
	movss	%xmm12, 344(%rsp)
	movl	$0, 348(%rsp)
	movss	%xmm15, 352(%rsp)
	movss	%xmm6, 356(%rsp)
	movss	%xmm10, 360(%rsp)
	movl	$0, 364(%rsp)
	movss	%xmm5, 368(%rsp)
	movss	%xmm4, 372(%rsp)
	movss	%xmm8, 376(%rsp)
	movl	$0, 380(%rsp)
	movlps	%xmm0, 384(%rsp)
	movlps	%xmm1, 392(%rsp)
	leaq	412(%r12), %rbx
	leaq	348(%r12), %r15
	leaq	336(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	leaq	16(%rsp), %rsi
	movq	%rbx, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movq	16(%rsp), %xmm8         # xmm8 = mem[0],zero
	movq	24(%rsp), %xmm12        # xmm12 = mem[0],zero
	pshufd	$229, %xmm8, %xmm4      # xmm4 = xmm8[1,1,2,3]
	movq	(%r14), %xmm14          # xmm14 = mem[0],zero
	movss	8(%r14), %xmm10         # xmm10 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm9
	mulss	%xmm4, %xmm9
	pshufd	$229, %xmm14, %xmm0     # xmm0 = xmm14[1,1,2,3]
	movdqa	%xmm0, %xmm11
	movaps	%xmm10, %xmm7
	shufps	$0, %xmm0, %xmm7        # xmm7 = xmm7[0,0],xmm0[0,0]
	shufps	$226, %xmm0, %xmm7      # xmm7 = xmm7[2,0],xmm0[2,3]
	movaps	%xmm0, %xmm13
	mulss	%xmm4, %xmm13
	movaps	.LCPI19_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	xorps	%xmm0, %xmm4
	pshufd	$229, %xmm12, %xmm2     # xmm2 = xmm12[1,1,2,3]
	unpcklps	%xmm8, %xmm12   # xmm12 = xmm12[0],xmm8[0],xmm12[1],xmm8[1]
	xorps	%xmm12, %xmm0
	movdqa	%xmm2, %xmm5
	mulss	%xmm14, %xmm5
	movss	12(%r14), %xmm1         # xmm1 = mem[0],zero,zero,zero
	pshufd	$229, %xmm0, %xmm6      # xmm6 = xmm0[1,1,2,3]
	movaps	%xmm1, %xmm3
	mulss	%xmm6, %xmm3
	addss	%xmm5, %xmm3
	subss	%xmm9, %xmm3
	mulss	%xmm0, %xmm11
	subss	%xmm11, %xmm3
	movaps	%xmm3, 32(%rsp)         # 16-byte Spill
	mulps	%xmm2, %xmm7
	mulss	%xmm1, %xmm2
	shufps	$224, %xmm1, %xmm1      # xmm1 = xmm1[0,0,2,3]
	movaps	%xmm4, %xmm3
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	mulps	%xmm1, %xmm3
	addps	%xmm7, %xmm3
	mulps	%xmm14, %xmm12
	subps	%xmm12, %xmm3
	mulss	%xmm10, %xmm0
	unpcklps	%xmm14, %xmm10  # xmm10 = xmm10[0],xmm14[0],xmm10[1],xmm14[1]
	shufps	$0, %xmm6, %xmm4        # xmm4 = xmm4[0,0],xmm6[0,0]
	shufps	$226, %xmm6, %xmm4      # xmm4 = xmm4[2,0],xmm6[2,3]
	mulps	%xmm10, %xmm4
	subps	%xmm4, %xmm3
	movaps	%xmm3, 176(%rsp)        # 16-byte Spill
	mulss	%xmm6, %xmm14
	subss	%xmm14, %xmm2
	addss	%xmm2, %xmm13
	subss	%xmm0, %xmm13
	movaps	%xmm13, 160(%rsp)       # 16-byte Spill
	leaq	16(%rsp), %rsi
	movq	%r15, %rdi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movsd	16(%rsp), %xmm0         # xmm0 = mem[0],zero
	movq	24(%rsp), %xmm3         # xmm3 = mem[0],zero
	pshufd	$229, %xmm3, %xmm8      # xmm8 = xmm3[1,1,2,3]
	movaps	160(%rsp), %xmm7        # 16-byte Reload
	movaps	%xmm7, %xmm2
	shufps	$224, %xmm2, %xmm2      # xmm2 = xmm2[0,0,2,3]
	mulps	%xmm0, %xmm2
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	movaps	%xmm1, %xmm4
	movaps	176(%rsp), %xmm5        # 16-byte Reload
	unpcklps	%xmm5, %xmm4    # xmm4 = xmm4[0],xmm5[0],xmm4[1],xmm5[1]
	mulps	%xmm8, %xmm4
	addps	%xmm2, %xmm4
	movaps	%xmm5, %xmm2
	movaps	%xmm5, %xmm12
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm1, %xmm5
	movaps	%xmm1, %xmm9
	shufps	$0, %xmm2, %xmm5        # xmm5 = xmm5[0,0],xmm2[0,0]
	shufps	$226, %xmm2, %xmm5      # xmm5 = xmm5[2,0],xmm2[2,3]
	movdqa	%xmm3, %xmm6
	movaps	%xmm7, %xmm10
	movaps	%xmm7, %xmm11
	mulss	%xmm3, %xmm10
	movaps	%xmm2, %xmm1
	mulss	%xmm3, %xmm2
	unpcklps	%xmm0, %xmm3    # xmm3 = xmm3[0],xmm0[0],xmm3[1],xmm0[1]
	mulps	%xmm12, %xmm3
	addps	%xmm4, %xmm3
	shufps	$16, %xmm0, %xmm6       # xmm6 = xmm6[0,0],xmm0[1,0]
	shufps	$226, %xmm0, %xmm6      # xmm6 = xmm6[2,0],xmm0[2,3]
	mulps	%xmm5, %xmm6
	pshufd	$229, %xmm0, %xmm4      # xmm4 = xmm0[1,1,2,3]
	subps	%xmm6, %xmm3
	mulss	%xmm8, %xmm1
	addss	%xmm10, %xmm1
	movaps	%xmm9, %xmm6
	movaps	%xmm6, %xmm5
	mulss	%xmm4, %xmm5
	addss	%xmm1, %xmm5
	mulss	%xmm0, %xmm6
	mulss	%xmm12, %xmm0
	subss	%xmm0, %xmm5
	movaps	%xmm11, %xmm0
	mulss	%xmm8, %xmm0
	subss	%xmm6, %xmm0
	mulss	%xmm4, %xmm12
	subss	%xmm12, %xmm0
	subss	%xmm2, %xmm0
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movlps	%xmm3, 16(%rsp)
	movlps	%xmm5, 24(%rsp)
	leaq	16(%rsp), %rsi
	movq	%r12, %rdi
	callq	_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion
	addq	$568, %rsp              # imm = 0x238
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end19:
	.size	_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion, .Lfunc_end19-_ZN21btConeTwistConstraint14setMotorTargetERK12btQuaternion
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI20_0:
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
	.long	2147483648              # float -0
.LCPI20_4:
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.long	2147483647              # float NaN
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI20_1:
	.long	3212836862              # float -0.99999988
.LCPI20_2:
	.long	1065353216              # float 1
.LCPI20_3:
	.long	1056964608              # float 0.5
.LCPI20_5:
	.long	1060439283              # float 0.707106769
.LCPI20_6:
	.long	1028443341              # float 0.0500000007
.LCPI20_7:
	.long	872415232               # float 1.1920929E-7
.LCPI20_8:
	.long	1078530011              # float 3.14159274
	.text
	.globl	_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion,@function
_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion: # @_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi108:
	.cfi_def_cfa_offset 16
	subq	$176, %rsp
.Lcfi109:
	.cfi_def_cfa_offset 192
.Lcfi110:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movups	(%rsi), %xmm0
	movups	%xmm0, 604(%rbx)
	movss	616(%rbx), %xmm11       # xmm11 = mem[0],zero,zero,zero
	movss	_ZL6vTwist(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm11, %xmm0
	mulss	%xmm13, %xmm0
	movss	604(%rbx), %xmm8        # xmm8 = mem[0],zero,zero,zero
	movss	608(%rbx), %xmm9        # xmm9 = mem[0],zero,zero,zero
	movss	_ZL6vTwist+8(%rip), %xmm12 # xmm12 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm5
	mulss	%xmm12, %xmm5
	addss	%xmm0, %xmm5
	movss	612(%rbx), %xmm10       # xmm10 = mem[0],zero,zero,zero
	movss	_ZL6vTwist+4(%rip), %xmm0 # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm10, %xmm2
	mulss	%xmm0, %xmm2
	subss	%xmm2, %xmm5
	movaps	%xmm11, %xmm3
	mulss	%xmm0, %xmm3
	movaps	%xmm13, %xmm2
	mulss	%xmm10, %xmm2
	addss	%xmm3, %xmm2
	movaps	%xmm12, %xmm3
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm2
	movaps	%xmm11, %xmm4
	mulss	%xmm12, %xmm4
	movaps	%xmm0, %xmm3
	mulss	%xmm8, %xmm3
	addss	%xmm4, %xmm3
	movaps	%xmm13, %xmm4
	mulss	%xmm9, %xmm4
	subss	%xmm4, %xmm3
	movaps	%xmm13, %xmm4
	mulss	%xmm8, %xmm4
	xorps	.LCPI20_0(%rip), %xmm4
	movaps	%xmm9, %xmm7
	mulss	%xmm0, %xmm7
	subss	%xmm7, %xmm4
	movaps	%xmm12, %xmm7
	mulss	%xmm10, %xmm7
	subss	%xmm7, %xmm4
	movaps	%xmm4, %xmm7
	mulss	%xmm8, %xmm7
	movaps	%xmm11, %xmm1
	mulss	%xmm5, %xmm1
	subss	%xmm7, %xmm1
	movaps	%xmm2, %xmm7
	mulss	%xmm10, %xmm7
	subss	%xmm7, %xmm1
	movaps	%xmm3, %xmm7
	mulss	%xmm9, %xmm7
	addss	%xmm1, %xmm7
	movaps	%xmm4, %xmm1
	mulss	%xmm9, %xmm1
	movaps	%xmm11, %xmm6
	mulss	%xmm2, %xmm6
	subss	%xmm1, %xmm6
	mulss	%xmm3, %xmm11
	mulss	%xmm8, %xmm3
	subss	%xmm3, %xmm6
	movaps	%xmm5, %xmm3
	mulss	%xmm10, %xmm3
	addss	%xmm6, %xmm3
	mulss	%xmm10, %xmm4
	subss	%xmm4, %xmm11
	mulss	%xmm9, %xmm5
	subss	%xmm5, %xmm11
	mulss	%xmm8, %xmm2
	addss	%xmm11, %xmm2
	movaps	%xmm13, %xmm1
	mulss	%xmm7, %xmm1
	movaps	%xmm0, %xmm4
	mulss	%xmm3, %xmm4
	addss	%xmm1, %xmm4
	movaps	%xmm12, %xmm1
	mulss	%xmm2, %xmm1
	addss	%xmm4, %xmm1
	movss	.LCPI20_1(%rip), %xmm4  # xmm4 = mem[0],zero,zero,zero
	ucomiss	%xmm1, %xmm4
	jbe	.LBB20_5
# BB#1:
	movaps	.LCPI20_4(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm12, %xmm1
	ucomiss	.LCPI20_5(%rip), %xmm1
	jbe	.LBB20_8
# BB#2:
	mulss	%xmm0, %xmm0
	mulss	%xmm12, %xmm12
	addss	%xmm0, %xmm12
	xorps	%xmm0, %xmm0
	sqrtss	%xmm12, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB20_4
# BB#3:                                 # %call.sqrt
	movaps	%xmm12, %xmm0
	callq	sqrtf
.LBB20_4:                               # %.split
	movss	.LCPI20_2(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	divss	%xmm0, %xmm13
	movss	_ZL6vTwist+8(%rip), %xmm12 # xmm12 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm12
	xorps	.LCPI20_0(%rip), %xmm12
	mulss	_ZL6vTwist+4(%rip), %xmm13
	xorps	%xmm8, %xmm8
	jmp	.LBB20_11
.LBB20_5:
	movaps	%xmm0, %xmm8
	mulss	%xmm2, %xmm8
	movaps	%xmm12, %xmm4
	mulss	%xmm3, %xmm4
	subss	%xmm4, %xmm8
	mulss	%xmm7, %xmm12
	mulss	%xmm13, %xmm2
	subss	%xmm2, %xmm12
	mulss	%xmm3, %xmm13
	mulss	%xmm7, %xmm0
	subss	%xmm0, %xmm13
	addss	.LCPI20_2(%rip), %xmm1
	addss	%xmm1, %xmm1
	sqrtss	%xmm1, %xmm10
	ucomiss	%xmm10, %xmm10
	jnp	.LBB20_7
# BB#6:                                 # %call.sqrt193
	movaps	%xmm1, %xmm0
	movaps	%xmm12, 16(%rsp)        # 16-byte Spill
	movaps	%xmm13, (%rsp)          # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	(%rsp), %xmm13          # 16-byte Reload
	movaps	16(%rsp), %xmm12        # 16-byte Reload
	movaps	%xmm0, %xmm10
.LBB20_7:                               # %.split192
	movss	.LCPI20_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm10, %xmm0
	mulss	%xmm0, %xmm8
	mulss	%xmm0, %xmm12
	mulss	%xmm0, %xmm13
	mulss	.LCPI20_3(%rip), %xmm10
	jmp	.LBB20_12
.LBB20_8:
	mulss	%xmm13, %xmm13
	mulss	%xmm0, %xmm0
	addss	%xmm13, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB20_10
# BB#9:                                 # %call.sqrt191
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB20_10:                              # %.split190
	movss	.LCPI20_2(%rip), %xmm12 # xmm12 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm12
	movss	_ZL6vTwist+4(%rip), %xmm8 # xmm8 = mem[0],zero,zero,zero
	mulss	%xmm12, %xmm8
	xorps	.LCPI20_0(%rip), %xmm8
	mulss	_ZL6vTwist(%rip), %xmm12
	xorps	%xmm13, %xmm13
.LBB20_11:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit
	xorps	%xmm10, %xmm10
.LBB20_12:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm12, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm13, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB20_14
# BB#13:                                # %call.sqrt194
	movaps	%xmm1, %xmm0
	movaps	%xmm12, 16(%rsp)        # 16-byte Spill
	movaps	%xmm13, (%rsp)          # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	callq	sqrtf
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	(%rsp), %xmm13          # 16-byte Reload
	movaps	16(%rsp), %xmm12        # 16-byte Reload
.LBB20_14:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit.split
	movss	.LCPI20_2(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm1
	divss	%xmm0, %xmm1
	mulss	%xmm1, %xmm8
	mulss	%xmm1, %xmm12
	mulss	%xmm1, %xmm13
	mulss	%xmm1, %xmm10
	movaps	.LCPI20_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm8, %xmm4
	xorps	%xmm0, %xmm4
	movaps	%xmm12, %xmm9
	xorps	%xmm0, %xmm9
	movaps	%xmm13, %xmm3
	xorps	%xmm0, %xmm3
	movss	616(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	movss	612(%rbx), %xmm14       # xmm14 = mem[0],zero,zero,zero
	movss	604(%rbx), %xmm1        # xmm1 = mem[0],zero,zero,zero
	movss	608(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm2, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	movaps	%xmm10, %xmm6
	unpcklps	%xmm9, %xmm6    # xmm6 = xmm6[0],xmm9[0],xmm6[1],xmm9[1]
	unpcklps	%xmm3, %xmm9    # xmm9 = xmm9[0],xmm3[0],xmm9[1],xmm3[1]
	unpcklps	%xmm4, %xmm3    # xmm3 = xmm3[0],xmm4[0],xmm3[1],xmm4[1]
	unpcklps	%xmm10, %xmm4   # xmm4 = xmm4[0],xmm10[0],xmm4[1],xmm10[1]
	mulps	%xmm5, %xmm4
	movaps	%xmm1, %xmm5
	unpcklps	%xmm2, %xmm5    # xmm5 = xmm5[0],xmm2[0],xmm5[1],xmm2[1]
	mulps	%xmm5, %xmm6
	addps	%xmm4, %xmm6
	movaps	%xmm14, %xmm4
	unpcklps	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	mulps	%xmm4, %xmm9
	addps	%xmm6, %xmm9
	movsd	608(%rbx), %xmm4        # xmm4 = mem[0],zero
	mulps	%xmm4, %xmm3
	subps	%xmm3, %xmm9
	movaps	%xmm9, %xmm3
	shufps	$229, %xmm3, %xmm3      # xmm3 = xmm3[1,1,2,3]
	movaps	%xmm10, %xmm4
	mulss	%xmm14, %xmm4
	movaps	%xmm10, %xmm5
	mulss	%xmm2, %xmm5
	mulss	%xmm13, %xmm2
	subss	%xmm2, %xmm4
	movaps	%xmm0, %xmm2
	mulss	%xmm8, %xmm2
	subss	%xmm2, %xmm4
	movaps	%xmm1, %xmm6
	mulss	%xmm12, %xmm6
	addss	%xmm4, %xmm6
	mulss	%xmm8, %xmm1
	addss	%xmm5, %xmm1
	mulss	%xmm12, %xmm0
	addss	%xmm1, %xmm0
	mulss	%xmm13, %xmm14
	addss	%xmm0, %xmm14
	movaps	%xmm9, %xmm0
	mulss	%xmm0, %xmm0
	mulss	%xmm3, %xmm3
	addss	%xmm0, %xmm3
	movaps	%xmm6, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm3, %xmm0
	movaps	%xmm14, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB20_16
# BB#15:                                # %call.sqrt195
	movaps	%xmm1, %xmm0
	movaps	%xmm12, 16(%rsp)        # 16-byte Spill
	movaps	%xmm13, (%rsp)          # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm10, 48(%rsp)        # 16-byte Spill
	movaps	%xmm14, 112(%rsp)       # 16-byte Spill
	callq	sqrtf
	movaps	112(%rsp), %xmm14       # 16-byte Reload
	movaps	48(%rsp), %xmm10        # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movss	.LCPI20_2(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	(%rsp), %xmm13          # 16-byte Reload
	movaps	16(%rsp), %xmm12        # 16-byte Reload
.LBB20_16:                              # %_Z15shortestArcQuatRK9btVector3S1_.exit.split.split
	movaps	%xmm8, %xmm1
	unpcklps	%xmm12, %xmm1   # xmm1 = xmm1[0],xmm12[0],xmm1[1],xmm12[1]
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	movaps	%xmm13, %xmm1
	unpcklps	%xmm10, %xmm1   # xmm1 = xmm1[0],xmm10[0],xmm1[1],xmm10[1]
	movaps	%xmm1, 144(%rsp)        # 16-byte Spill
	movaps	%xmm7, %xmm1
	divss	%xmm0, %xmm1
	movaps	%xmm1, %xmm11
	shufps	$224, %xmm11, %xmm11    # xmm11 = xmm11[0,0,2,3]
	mulss	%xmm1, %xmm6
	mulss	%xmm14, %xmm1
	movss	492(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI20_6(%rip), %xmm0
	jb	.LBB20_31
# BB#17:
	movss	496(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI20_6(%rip), %xmm0
	jb	.LBB20_31
# BB#18:
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movaps	%xmm10, %xmm0
	movaps	%xmm12, 16(%rsp)        # 16-byte Spill
	movaps	%xmm13, (%rsp)          # 16-byte Spill
	movaps	%xmm11, 48(%rsp)        # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movaps	%xmm9, 80(%rsp)         # 16-byte Spill
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	callq	acosf
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movaps	16(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm0, %xmm5
	addss	%xmm5, %xmm5
	ucomiss	.LCPI20_7(%rip), %xmm5
                                        # implicit-def: %XMM0
	jbe	.LBB20_24
# BB#19:
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB20_21
# BB#20:                                # %call.sqrt197
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movaps	16(%rsp), %xmm4         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB20_21:                              # %.split196
	movss	.LCPI20_2(%rip), %xmm7  # xmm7 = mem[0],zero,zero,zero
	movaps	%xmm7, %xmm0
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm8
	mulss	%xmm0, %xmm4
	mulss	%xmm0, %xmm10
	movss	492(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	movaps	.LCPI20_4(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm4, %xmm1
	ucomiss	.LCPI20_7(%rip), %xmm1
	jbe	.LBB20_24
# BB#22:
	movaps	%xmm10, %xmm1
	mulss	%xmm1, %xmm1
	movaps	%xmm4, %xmm2
	mulss	%xmm2, %xmm2
	divss	%xmm2, %xmm1
	movss	496(%rbx), %xmm2        # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	movaps	%xmm7, %xmm3
	divss	%xmm2, %xmm3
	mulss	%xmm0, %xmm0
	movaps	%xmm1, %xmm2
	divss	%xmm0, %xmm2
	addss	%xmm3, %xmm2
	addss	%xmm7, %xmm1
	divss	%xmm2, %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB20_24
# BB#23:                                # %call.sqrt199
	movaps	%xmm1, %xmm0
	movaps	%xmm4, 16(%rsp)         # 16-byte Spill
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movaps	32(%rsp), %xmm8         # 16-byte Reload
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	(%rsp), %xmm10          # 16-byte Reload
	movaps	16(%rsp), %xmm4         # 16-byte Reload
.LBB20_24:                              # %_ZN21btConeTwistConstraint20computeConeLimitInfoERK12btQuaternionRfR9btVector3S3_.exit
	movaps	.LCPI20_4(%rip), %xmm1  # xmm1 = [nan,nan,nan,nan]
	andps	%xmm5, %xmm1
	ucomiss	.LCPI20_7(%rip), %xmm1
	movaps	128(%rsp), %xmm1        # 16-byte Reload
	jbe	.LBB20_31
# BB#25:
	ucomiss	%xmm0, %xmm5
	ja	.LBB20_27
# BB#26:
	xorps	.LCPI20_0(%rip), %xmm0
	ucomiss	%xmm5, %xmm0
	jbe	.LBB20_28
.LBB20_27:
	movaps	%xmm0, %xmm5
.LBB20_28:
	movaps	%xmm8, %xmm0
	mulss	%xmm0, %xmm0
	movaps	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movaps	%xmm10, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	movaps	%xmm4, 16(%rsp)         # 16-byte Spill
	movaps	%xmm10, (%rsp)          # 16-byte Spill
	movaps	%xmm8, 32(%rsp)         # 16-byte Spill
	jnp	.LBB20_30
# BB#29:                                # %call.sqrt201
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	96(%rsp), %xmm5         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB20_30:                              # %.split200
	movss	%xmm1, 112(%rsp)        # 4-byte Spill
	mulss	.LCPI20_3(%rip), %xmm5
	movaps	%xmm5, 96(%rsp)         # 16-byte Spill
	movaps	%xmm5, %xmm0
	callq	sinf
	divss	112(%rsp), %xmm0        # 4-byte Folded Reload
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 32(%rsp)         # 16-byte Spill
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movaps	(%rsp), %xmm1           # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movaps	96(%rsp), %xmm0         # 16-byte Reload
	callq	cosf
	movaps	32(%rsp), %xmm1         # 16-byte Reload
	unpcklps	16(%rsp), %xmm1 # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	(%rsp), %xmm2           # 16-byte Reload
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm1, 112(%rsp)        # 16-byte Spill
	movaps	%xmm2, 144(%rsp)        # 16-byte Spill
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	80(%rsp), %xmm9         # 16-byte Reload
	movaps	64(%rsp), %xmm6         # 16-byte Reload
	movaps	128(%rsp), %xmm1        # 16-byte Reload
.LBB20_31:
	mulps	%xmm9, %xmm11
	movaps	%xmm6, %xmm14
	unpcklps	%xmm1, %xmm14   # xmm14 = xmm14[0],xmm1[0],xmm14[1],xmm1[1]
	movss	500(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	.LCPI20_6(%rip), %xmm0
	jb	.LBB20_45
# BB#32:
	movaps	%xmm6, 64(%rsp)         # 16-byte Spill
	movaps	%xmm1, %xmm0
	movaps	%xmm11, 48(%rsp)        # 16-byte Spill
	movaps	%xmm1, 128(%rsp)        # 16-byte Spill
	movaps	%xmm14, 16(%rsp)        # 16-byte Spill
	callq	acosf
	movaps	16(%rsp), %xmm14        # 16-byte Reload
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	%xmm0, %xmm2
	addss	%xmm2, %xmm2
	ucomiss	.LCPI20_8(%rip), %xmm2
	movaps	%xmm11, %xmm1
	movaps	%xmm14, %xmm0
	jbe	.LBB20_34
# BB#33:
	movaps	.LCPI20_0(%rip), %xmm0  # xmm0 = [-0.000000e+00,-0.000000e+00,-0.000000e+00,-0.000000e+00]
	movaps	%xmm0, %xmm1
	xorps	%xmm1, %xmm11
	movaps	%xmm11, (%rsp)          # 16-byte Spill
	movaps	64(%rsp), %xmm2         # 16-byte Reload
	xorps	%xmm1, %xmm2
	movaps	128(%rsp), %xmm0        # 16-byte Reload
	xorps	%xmm1, %xmm0
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm2, 64(%rsp)         # 16-byte Spill
	callq	acosf
	movaps	(%rsp), %xmm1           # 16-byte Reload
	movaps	16(%rsp), %xmm14        # 16-byte Reload
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	%xmm0, %xmm2
	addss	%xmm2, %xmm2
	movaps	64(%rsp), %xmm0         # 16-byte Reload
.LBB20_34:
	shufps	$212, %xmm1, %xmm1      # xmm1 = xmm1[0,1,1,3]
	movd	%xmm1, %eax
	pshufd	$78, %xmm1, %xmm1       # xmm1 = xmm1[2,3,0,1]
	movd	%xmm1, %edx
	shufps	$212, %xmm0, %xmm0      # xmm0 = xmm0[0,1,1,3]
	movd	%xmm0, %ecx
	ucomiss	.LCPI20_7(%rip), %xmm2
	jbe	.LBB20_38
# BB#35:
	movd	%eax, %xmm5
	movd	%edx, %xmm4
	movd	%ecx, %xmm3
	movdqa	%xmm5, %xmm0
	mulss	%xmm0, %xmm0
	movdqa	%xmm4, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movdqa	%xmm3, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB20_37
# BB#36:                                # %call.sqrt203
	movaps	%xmm2, 32(%rsp)         # 16-byte Spill
	movd	%xmm3, (%rsp)           # 4-byte Folded Spill
	movd	%xmm4, 64(%rsp)         # 4-byte Folded Spill
	movd	%xmm5, 80(%rsp)         # 4-byte Folded Spill
	callq	sqrtf
	movd	80(%rsp), %xmm5         # 4-byte Folded Reload
                                        # xmm5 = mem[0],zero,zero,zero
	movd	64(%rsp), %xmm4         # 4-byte Folded Reload
                                        # xmm4 = mem[0],zero,zero,zero
	movd	(%rsp), %xmm3           # 4-byte Folded Reload
                                        # xmm3 = mem[0],zero,zero,zero
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movaps	16(%rsp), %xmm14        # 16-byte Reload
	movaps	48(%rsp), %xmm11        # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB20_37:                              # %.split202
	movss	.LCPI20_2(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	mulss	%xmm0, %xmm5
	movd	%xmm5, %eax
	mulss	%xmm0, %xmm4
	movd	%xmm4, %edx
	mulss	%xmm0, %xmm3
	movd	%xmm3, %ecx
.LBB20_38:                              # %_ZN21btConeTwistConstraint21computeTwistLimitInfoERK12btQuaternionRfR9btVector3.exit
	movaps	.LCPI20_4(%rip), %xmm0  # xmm0 = [nan,nan,nan,nan]
	andps	%xmm2, %xmm0
	ucomiss	.LCPI20_7(%rip), %xmm0
	jbe	.LBB20_45
# BB#39:
	movss	500(%rbx), %xmm0        # xmm0 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm2
	ja	.LBB20_41
# BB#40:
	xorps	.LCPI20_0(%rip), %xmm0
	ucomiss	%xmm2, %xmm0
	jbe	.LBB20_42
.LBB20_41:
	movaps	%xmm0, %xmm2
.LBB20_42:
	movd	%eax, %xmm0
	movdqa	%xmm0, 48(%rsp)         # 16-byte Spill
	mulss	%xmm0, %xmm0
	movd	%edx, %xmm1
	movdqa	%xmm1, (%rsp)           # 16-byte Spill
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%ecx, %xmm0
	movdqa	%xmm0, 16(%rsp)         # 16-byte Spill
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB20_44
# BB#43:                                # %call.sqrt205
	movaps	%xmm2, 32(%rsp)         # 16-byte Spill
	callq	sqrtf
	movaps	32(%rsp), %xmm2         # 16-byte Reload
	movaps	%xmm0, %xmm1
.LBB20_44:                              # %.split204
	movss	%xmm1, 64(%rsp)         # 4-byte Spill
	mulss	.LCPI20_3(%rip), %xmm2
	movaps	%xmm2, 32(%rsp)         # 16-byte Spill
	movaps	%xmm2, %xmm0
	callq	sinf
	divss	64(%rsp), %xmm0         # 4-byte Folded Reload
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 48(%rsp)         # 16-byte Spill
	movaps	(%rsp), %xmm1           # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, (%rsp)           # 16-byte Spill
	movaps	16(%rsp), %xmm1         # 16-byte Reload
	mulss	%xmm0, %xmm1
	movaps	%xmm1, 16(%rsp)         # 16-byte Spill
	movaps	32(%rsp), %xmm0         # 16-byte Reload
	callq	cosf
	movaps	48(%rsp), %xmm1         # 16-byte Reload
	unpcklps	(%rsp), %xmm1   # 16-byte Folded Reload
                                        # xmm1 = xmm1[0],mem[0],xmm1[1],mem[1]
	movaps	16(%rsp), %xmm2         # 16-byte Reload
	unpcklps	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	movaps	%xmm1, %xmm11
	movaps	%xmm2, %xmm14
.LBB20_45:
	movaps	144(%rsp), %xmm13       # 16-byte Reload
	movaps	%xmm13, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm14, %xmm0
	shufps	$229, %xmm0, %xmm0      # xmm0 = xmm0[1,1,2,3]
	movaps	112(%rsp), %xmm12       # 16-byte Reload
	movaps	%xmm12, %xmm10
	mulss	%xmm0, %xmm10
	movaps	%xmm12, %xmm1
	shufps	$229, %xmm1, %xmm1      # xmm1 = xmm1[1,1,2,3]
	movaps	%xmm2, %xmm8
	movaps	%xmm1, %xmm5
	mulss	%xmm0, %xmm5
	movaps	%xmm2, %xmm9
	movaps	%xmm13, %xmm7
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm0
	mulss	%xmm11, %xmm2
	addss	%xmm2, %xmm10
	movaps	%xmm11, %xmm2
	shufps	$229, %xmm2, %xmm2      # xmm2 = xmm2[1,1,2,3]
	movaps	%xmm13, %xmm4
	mulss	%xmm2, %xmm4
	mulss	%xmm2, %xmm8
	movaps	%xmm12, %xmm6
	mulss	%xmm2, %xmm6
	movaps	%xmm1, %xmm3
	mulss	%xmm1, %xmm2
	mulss	%xmm14, %xmm1
	addss	%xmm10, %xmm1
	subss	%xmm4, %xmm1
	addss	%xmm8, %xmm5
	movaps	%xmm13, %xmm4
	mulss	%xmm11, %xmm4
	addss	%xmm5, %xmm4
	movaps	%xmm12, %xmm5
	mulss	%xmm14, %xmm5
	subss	%xmm5, %xmm4
	mulss	%xmm14, %xmm9
	addss	%xmm9, %xmm7
	addss	%xmm7, %xmm6
	mulss	%xmm11, %xmm3
	subss	%xmm3, %xmm6
	mulss	%xmm11, %xmm12
	subss	%xmm12, %xmm0
	subss	%xmm2, %xmm0
	mulss	%xmm14, %xmm13
	subss	%xmm13, %xmm0
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	movlps	%xmm1, 604(%rbx)
	movlps	%xmm6, 612(%rbx)
	addq	$176, %rsp
	popq	%rbx
	retq
.Lfunc_end20:
	.size	_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion, .Lfunc_end20-_ZN21btConeTwistConstraint31setMotorTargetInConstraintSpaceERK12btQuaternion
	.cfi_endproc

	.section	.text._ZN17btTypedConstraintD2Ev,"axG",@progbits,_ZN17btTypedConstraintD2Ev,comdat
	.weak	_ZN17btTypedConstraintD2Ev
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraintD2Ev,@function
_ZN17btTypedConstraintD2Ev:             # @_ZN17btTypedConstraintD2Ev
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZN17btTypedConstraintD2Ev, .Lfunc_end21-_ZN17btTypedConstraintD2Ev
	.cfi_endproc

	.section	.text._ZN21btConeTwistConstraintD0Ev,"axG",@progbits,_ZN21btConeTwistConstraintD0Ev,comdat
	.weak	_ZN21btConeTwistConstraintD0Ev
	.p2align	4, 0x90
	.type	_ZN21btConeTwistConstraintD0Ev,@function
_ZN21btConeTwistConstraintD0Ev:         # @_ZN21btConeTwistConstraintD0Ev
	.cfi_startproc
# BB#0:
	jmp	_ZdlPv                  # TAILCALL
.Lfunc_end22:
	.size	_ZN21btConeTwistConstraintD0Ev, .Lfunc_end22-_ZN21btConeTwistConstraintD0Ev
	.cfi_endproc

	.section	.text._ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,"axG",@progbits,_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,comdat
	.weak	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.p2align	4, 0x90
	.type	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif,@function
_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif: # @_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end23:
	.size	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif, .Lfunc_end23-_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI24_0:
	.long	1065353216              # float 1
.LCPI24_1:
	.long	679477248               # float 1.42108547E-14
	.section	.text._ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,"axG",@progbits,_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,comdat
	.weak	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.p2align	4, 0x90
	.type	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf,@function
_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf: # @_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 24
	subq	$88, %rsp
.Lcfi113:
	.cfi_def_cfa_offset 112
.Lcfi114:
	.cfi_offset %rbx, -24
.Lcfi115:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdx, %rbx
	movss	20(%rdi), %xmm8         # xmm8 = mem[0],zero,zero,zero
	movss	40(%rdi), %xmm4         # xmm4 = mem[0],zero,zero,zero
	movaps	%xmm8, %xmm0
	mulss	%xmm4, %xmm0
	movss	24(%rdi), %xmm12        # xmm12 = mem[0],zero,zero,zero
	movss	36(%rdi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm1
	mulss	%xmm14, %xmm1
	subss	%xmm1, %xmm0
	movaps	%xmm0, %xmm3
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movss	32(%rdi), %xmm1         # xmm1 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm5
	mulss	%xmm1, %xmm5
	movss	16(%rdi), %xmm11        # xmm11 = mem[0],zero,zero,zero
	movaps	%xmm4, %xmm2
	mulss	%xmm11, %xmm2
	subss	%xmm2, %xmm5
	movaps	%xmm14, %xmm15
	mulss	%xmm11, %xmm15
	movaps	%xmm8, %xmm2
	mulss	%xmm1, %xmm2
	subss	%xmm2, %xmm15
	movss	(%rdi), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movss	4(%rdi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, 36(%rsp)         # 4-byte Spill
	movss	8(%rdi), %xmm6          # xmm6 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm2
	mulss	%xmm6, %xmm2
	movaps	%xmm14, %xmm7
	mulss	%xmm6, %xmm7
	movaps	%xmm4, %xmm3
	mulss	%xmm13, %xmm3
	movaps	%xmm12, %xmm10
	mulss	%xmm13, %xmm10
	movaps	%xmm8, %xmm0
	mulss	%xmm6, %xmm0
	movaps	%xmm1, %xmm9
	mulss	%xmm6, %xmm9
	mulss	%xmm11, %xmm6
	mulss	%xmm13, %xmm1
	mulss	%xmm13, %xmm11
	mulss	%xmm5, %xmm13
	addss	36(%rsp), %xmm13        # 4-byte Folded Reload
	addss	%xmm13, %xmm2
	movss	.LCPI24_0(%rip), %xmm13 # xmm13 = mem[0],zero,zero,zero
	divss	%xmm2, %xmm13
	subss	%xmm3, %xmm7
	subss	%xmm0, %xmm10
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm13, %xmm3
	mulss	%xmm13, %xmm7
	mulss	%xmm13, %xmm10
	mulss	%xmm13, %xmm5
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm4
	subss	%xmm9, %xmm4
	mulss	%xmm13, %xmm4
	mulss	%xmm0, %xmm12
	subss	%xmm12, %xmm6
	mulss	%xmm13, %xmm6
	mulss	%xmm13, %xmm15
	mulss	%xmm0, %xmm14
	subss	%xmm14, %xmm1
	mulss	%xmm13, %xmm1
	mulss	%xmm0, %xmm8
	subss	%xmm11, %xmm8
	mulss	%xmm13, %xmm8
	movss	(%rsi), %xmm12          # xmm12 = mem[0],zero,zero,zero
	movss	4(%rsi), %xmm13         # xmm13 = mem[0],zero,zero,zero
	movaps	%xmm12, %xmm9
	mulss	%xmm3, %xmm9
	movss	%xmm3, 12(%rsp)         # 4-byte Spill
	movaps	%xmm13, %xmm2
	mulss	%xmm5, %xmm2
	addss	%xmm9, %xmm2
	movss	8(%rsi), %xmm9          # xmm9 = mem[0],zero,zero,zero
	movaps	%xmm9, %xmm0
	mulss	%xmm15, %xmm0
	addss	%xmm2, %xmm0
	movss	%xmm0, 8(%rsp)          # 4-byte Spill
	movaps	%xmm12, %xmm11
	mulss	%xmm7, %xmm11
	movaps	%xmm13, %xmm2
	mulss	%xmm4, %xmm2
	addss	%xmm11, %xmm2
	movaps	%xmm9, %xmm11
	mulss	%xmm1, %xmm11
	addss	%xmm2, %xmm11
	mulss	%xmm10, %xmm12
	mulss	%xmm6, %xmm13
	addss	%xmm12, %xmm13
	mulss	%xmm8, %xmm9
	addss	%xmm13, %xmm9
	movss	16(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm3, %xmm12
	mulss	%xmm2, %xmm12
	movss	20(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm5, %xmm13
	mulss	%xmm0, %xmm13
	addss	%xmm12, %xmm13
	movss	24(%rsi), %xmm14        # xmm14 = mem[0],zero,zero,zero
	movaps	%xmm15, %xmm12
	mulss	%xmm14, %xmm12
	addss	%xmm13, %xmm12
	movaps	%xmm7, %xmm13
	mulss	%xmm2, %xmm13
	movaps	%xmm4, %xmm3
	mulss	%xmm0, %xmm3
	addss	%xmm13, %xmm3
	movaps	%xmm1, %xmm13
	mulss	%xmm14, %xmm13
	addss	%xmm3, %xmm13
	mulss	%xmm10, %xmm2
	mulss	%xmm6, %xmm0
	addss	%xmm2, %xmm0
	mulss	%xmm8, %xmm14
	addss	%xmm0, %xmm14
	movss	32(%rsi), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	12(%rsp), %xmm3         # 4-byte Reload
                                        # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm3
	movss	36(%rsi), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm5
	addss	%xmm3, %xmm5
	movss	40(%rsi), %xmm3         # xmm3 = mem[0],zero,zero,zero
	mulss	%xmm3, %xmm15
	addss	%xmm5, %xmm15
	mulss	%xmm0, %xmm7
	mulss	%xmm2, %xmm4
	addss	%xmm7, %xmm4
	mulss	%xmm3, %xmm1
	addss	%xmm4, %xmm1
	mulss	%xmm0, %xmm10
	mulss	%xmm2, %xmm6
	addss	%xmm10, %xmm6
	mulss	%xmm3, %xmm8
	addss	%xmm6, %xmm8
	movss	8(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 40(%rsp)
	movss	%xmm11, 44(%rsp)
	movss	%xmm9, 48(%rsp)
	movl	$0, 52(%rsp)
	movss	%xmm12, 56(%rsp)
	movss	%xmm13, 60(%rsp)
	movss	%xmm14, 64(%rsp)
	movl	$0, 68(%rsp)
	movss	%xmm15, 72(%rsp)
	movss	%xmm1, 76(%rsp)
	movss	%xmm8, 80(%rsp)
	movl	$0, 84(%rsp)
	leaq	40(%rsp), %rdi
	leaq	16(%rsp), %rsi
	callq	_ZNK11btMatrix3x311getRotationER12btQuaternion
	movss	16(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	movss	20(%rsp), %xmm1         # xmm1 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movss	24(%rsp), %xmm2         # xmm2 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm2
	addss	%xmm1, %xmm2
	movss	28(%rsp), %xmm0         # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm0, %xmm0
	addss	%xmm2, %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB24_2
# BB#1:                                 # %call.sqrt
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB24_2:                               # %.split
	movss	.LCPI24_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	divss	%xmm1, %xmm0
	shufps	$0, %xmm0, %xmm0        # xmm0 = xmm0[0,0,0,0]
	mulps	16(%rsp), %xmm0
	movaps	%xmm0, 16(%rsp)
	shufps	$231, %xmm0, %xmm0      # xmm0 = xmm0[3,1,2,3]
	callq	acosf
	addss	%xmm0, %xmm0
	movss	%xmm0, (%r14)
	movl	16(%rsp), %eax
	movl	20(%rsp), %ecx
	movl	24(%rsp), %edx
	movl	%eax, (%rbx)
	movl	%ecx, 4(%rbx)
	movl	%edx, 8(%rbx)
	movl	$0, 12(%rbx)
	movd	%eax, %xmm0
	mulss	%xmm0, %xmm0
	movd	%ecx, %xmm1
	mulss	%xmm1, %xmm1
	addss	%xmm0, %xmm1
	movd	%edx, %xmm0
	mulss	%xmm0, %xmm0
	addss	%xmm1, %xmm0
	movss	.LCPI24_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	ucomiss	%xmm0, %xmm1
	jbe	.LBB24_4
# BB#3:
	movq	$1065353216, (%rbx)     # imm = 0x3F800000
	movq	$0, 8(%rbx)
	jmp	.LBB24_7
.LBB24_4:
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB24_6
# BB#5:                                 # %call.sqrt52
	callq	sqrtf
	movaps	%xmm0, %xmm1
.LBB24_6:                               # %.split51
	movss	.LCPI24_0(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm2
	divss	%xmm1, %xmm2
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, (%rbx)
	movss	4(%rbx), %xmm0          # xmm0 = mem[0],zero,zero,zero
	mulss	%xmm2, %xmm0
	movss	%xmm0, 4(%rbx)
	mulss	8(%rbx), %xmm2
	movss	%xmm2, 8(%rbx)
.LBB24_7:
	addq	$88, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end24:
	.size	_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf, .Lfunc_end24-_ZN15btTransformUtil22calculateDiffAxisAngleERK11btTransformS2_R9btVector3Rf
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI25_0:
	.long	1065353216              # float 1
.LCPI25_1:
	.long	1056964608              # float 0.5
	.section	.text._ZNK11btMatrix3x311getRotationER12btQuaternion,"axG",@progbits,_ZNK11btMatrix3x311getRotationER12btQuaternion,comdat
	.weak	_ZNK11btMatrix3x311getRotationER12btQuaternion
	.p2align	4, 0x90
	.type	_ZNK11btMatrix3x311getRotationER12btQuaternion,@function
_ZNK11btMatrix3x311getRotationER12btQuaternion: # @_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi116:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi117:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi118:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi119:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi120:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi121:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi122:
	.cfi_def_cfa_offset 80
.Lcfi123:
	.cfi_offset %rbx, -56
.Lcfi124:
	.cfi_offset %r12, -48
.Lcfi125:
	.cfi_offset %r13, -40
.Lcfi126:
	.cfi_offset %r14, -32
.Lcfi127:
	.cfi_offset %r15, -24
.Lcfi128:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movss	(%rbx), %xmm0           # xmm0 = mem[0],zero,zero,zero
	movss	20(%rbx), %xmm2         # xmm2 = mem[0],zero,zero,zero
	movaps	%xmm0, %xmm1
	addss	%xmm2, %xmm1
	movss	40(%rbx), %xmm3         # xmm3 = mem[0],zero,zero,zero
	addss	%xmm3, %xmm1
	xorps	%xmm4, %xmm4
	ucomiss	%xmm4, %xmm1
	jbe	.LBB25_4
# BB#1:
	addss	.LCPI25_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtss	%xmm1, %xmm0
	ucomiss	%xmm0, %xmm0
	jnp	.LBB25_3
# BB#2:                                 # %call.sqrt
	movaps	%xmm1, %xmm0
	movq	%rsi, %rbp
	callq	sqrtf
	movq	%rbp, %rsi
.LBB25_3:                               # %.split
	movss	.LCPI25_1(%rip), %xmm1  # xmm1 = mem[0],zero,zero,zero
	movss	8(%rbx), %xmm2          # xmm2 = mem[0],zero,zero,zero
	subss	32(%rbx), %xmm2
	unpcklps	%xmm1, %xmm2    # xmm2 = xmm2[0],xmm1[0],xmm2[1],xmm1[1]
	movaps	%xmm1, %xmm3
	divss	%xmm0, %xmm3
	movss	36(%rbx), %xmm1         # xmm1 = mem[0],zero,zero,zero
	subss	24(%rbx), %xmm1
	movss	16(%rbx), %xmm4         # xmm4 = mem[0],zero,zero,zero
	subss	4(%rbx), %xmm4
	movaps	%xmm3, %xmm5
	unpcklps	%xmm0, %xmm5    # xmm5 = xmm5[0],xmm0[0],xmm5[1],xmm0[1]
	unpcklps	%xmm3, %xmm3    # xmm3 = xmm3[0,0,1,1]
	unpcklps	%xmm5, %xmm3    # xmm3 = xmm3[0],xmm5[0],xmm3[1],xmm5[1]
	unpcklps	%xmm4, %xmm1    # xmm1 = xmm1[0],xmm4[0],xmm1[1],xmm4[1]
	unpcklps	%xmm2, %xmm1    # xmm1 = xmm1[0],xmm2[0],xmm1[1],xmm2[1]
	mulps	%xmm3, %xmm1
	movaps	%xmm1, (%rsp)
	jmp	.LBB25_7
.LBB25_4:
	xorl	%eax, %eax
	ucomiss	%xmm0, %xmm2
	seta	%al
	maxss	%xmm0, %xmm2
	ucomiss	%xmm2, %xmm3
	movl	$2, %r15d
	cmovbel	%eax, %r15d
	leal	1(%r15), %eax
	movl	$2863311531, %ecx       # imm = 0xAAAAAAAB
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	1(%r15,%rax), %edx
	movl	%r15d, %eax
	addl	$2, %eax
	imulq	%rcx, %rax
	shrq	$33, %rax
	leal	(%rax,%rax,2), %eax
	negl	%eax
	leal	2(%r15,%rax), %r14d
	movq	%r15, %rbp
	shlq	$4, %rbp
	addq	%rbx, %rbp
	movss	(%rbp,%r15,4), %xmm0    # xmm0 = mem[0],zero,zero,zero
	movq	%rdx, %r12
	shlq	$4, %r12
	addq	%rbx, %r12
	subss	(%r12,%rdx,4), %xmm0
	movq	%r14, %r13
	shlq	$4, %r13
	addq	%rbx, %r13
	subss	(%r13,%r14,4), %xmm0
	addss	.LCPI25_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	sqrtss	%xmm0, %xmm1
	ucomiss	%xmm1, %xmm1
	jnp	.LBB25_6
# BB#5:                                 # %call.sqrt72
	movq	%rsi, 16(%rsp)          # 8-byte Spill
	movq	%rdx, %rbx
	callq	sqrtf
	movq	%rbx, %rdx
	movq	16(%rsp), %rsi          # 8-byte Reload
	movaps	%xmm0, %xmm1
.LBB25_6:                               # %.split71
	movss	.LCPI25_1(%rip), %xmm0  # xmm0 = mem[0],zero,zero,zero
	movaps	%xmm1, %xmm2
	mulss	%xmm0, %xmm2
	movss	%xmm2, (%rsp,%r15,4)
	divss	%xmm1, %xmm0
	movss	(%r13,%rdx,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	subss	(%r12,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, 12(%rsp)
	movss	(%r12,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%rdx,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%rdx,4)
	movss	(%r13,%r15,4), %xmm1    # xmm1 = mem[0],zero,zero,zero
	addss	(%rbp,%r14,4), %xmm1
	mulss	%xmm0, %xmm1
	movss	%xmm1, (%rsp,%r14,4)
	movaps	(%rsp), %xmm1
.LBB25_7:
	movups	%xmm1, (%rsi)
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end25:
	.size	_ZNK11btMatrix3x311getRotationER12btQuaternion, .Lfunc_end25-_ZNK11btMatrix3x311getRotationER12btQuaternion
	.cfi_endproc

	.type	_ZTV21btConeTwistConstraint,@object # @_ZTV21btConeTwistConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTV21btConeTwistConstraint
	.p2align	3
_ZTV21btConeTwistConstraint:
	.quad	0
	.quad	_ZTI21btConeTwistConstraint
	.quad	_ZN17btTypedConstraintD2Ev
	.quad	_ZN21btConeTwistConstraintD0Ev
	.quad	_ZN21btConeTwistConstraint13buildJacobianEv
	.quad	_ZN17btTypedConstraint21setupSolverConstraintER20btAlignedObjectArrayI18btSolverConstraintEiif
	.quad	_ZN21btConeTwistConstraint8getInfo1EPN17btTypedConstraint17btConstraintInfo1E
	.quad	_ZN21btConeTwistConstraint8getInfo2EPN17btTypedConstraint17btConstraintInfo2E
	.quad	_ZN21btConeTwistConstraint23solveConstraintObsoleteER12btSolverBodyS1_f
	.size	_ZTV21btConeTwistConstraint, 72

	.type	_ZL6vTwist,@object      # @_ZL6vTwist
	.data
	.p2align	2
_ZL6vTwist:
	.long	1065353216              # float 1
	.long	0                       # float 0
	.long	0                       # float 0
	.long	0                       # float 0
	.size	_ZL6vTwist, 16

	.type	_ZTS21btConeTwistConstraint,@object # @_ZTS21btConeTwistConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTS21btConeTwistConstraint
	.p2align	4
_ZTS21btConeTwistConstraint:
	.asciz	"21btConeTwistConstraint"
	.size	_ZTS21btConeTwistConstraint, 24

	.type	_ZTS17btTypedConstraint,@object # @_ZTS17btTypedConstraint
	.section	.rodata._ZTS17btTypedConstraint,"aG",@progbits,_ZTS17btTypedConstraint,comdat
	.weak	_ZTS17btTypedConstraint
	.p2align	4
_ZTS17btTypedConstraint:
	.asciz	"17btTypedConstraint"
	.size	_ZTS17btTypedConstraint, 20

	.type	_ZTS13btTypedObject,@object # @_ZTS13btTypedObject
	.section	.rodata._ZTS13btTypedObject,"aG",@progbits,_ZTS13btTypedObject,comdat
	.weak	_ZTS13btTypedObject
_ZTS13btTypedObject:
	.asciz	"13btTypedObject"
	.size	_ZTS13btTypedObject, 16

	.type	_ZTI13btTypedObject,@object # @_ZTI13btTypedObject
	.section	.rodata._ZTI13btTypedObject,"aG",@progbits,_ZTI13btTypedObject,comdat
	.weak	_ZTI13btTypedObject
	.p2align	3
_ZTI13btTypedObject:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13btTypedObject
	.size	_ZTI13btTypedObject, 16

	.type	_ZTI17btTypedConstraint,@object # @_ZTI17btTypedConstraint
	.section	.rodata._ZTI17btTypedConstraint,"aG",@progbits,_ZTI17btTypedConstraint,comdat
	.weak	_ZTI17btTypedConstraint
	.p2align	4
_ZTI17btTypedConstraint:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS17btTypedConstraint
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	_ZTI13btTypedObject
	.quad	2050                    # 0x802
	.size	_ZTI17btTypedConstraint, 40

	.type	_ZTI21btConeTwistConstraint,@object # @_ZTI21btConeTwistConstraint
	.section	.rodata,"a",@progbits
	.globl	_ZTI21btConeTwistConstraint
	.p2align	4
_ZTI21btConeTwistConstraint:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21btConeTwistConstraint
	.quad	_ZTI17btTypedConstraint
	.size	_ZTI21btConeTwistConstraint, 24


	.globl	_ZN21btConeTwistConstraintC1Ev
	.type	_ZN21btConeTwistConstraintC1Ev,@function
_ZN21btConeTwistConstraintC1Ev = _ZN21btConeTwistConstraintC2Ev
	.globl	_ZN21btConeTwistConstraintC1ER11btRigidBodyS1_RK11btTransformS4_
	.type	_ZN21btConeTwistConstraintC1ER11btRigidBodyS1_RK11btTransformS4_,@function
_ZN21btConeTwistConstraintC1ER11btRigidBodyS1_RK11btTransformS4_ = _ZN21btConeTwistConstraintC2ER11btRigidBodyS1_RK11btTransformS4_
	.globl	_ZN21btConeTwistConstraintC1ER11btRigidBodyRK11btTransform
	.type	_ZN21btConeTwistConstraintC1ER11btRigidBodyRK11btTransform,@function
_ZN21btConeTwistConstraintC1ER11btRigidBodyRK11btTransform = _ZN21btConeTwistConstraintC2ER11btRigidBodyRK11btTransform
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
