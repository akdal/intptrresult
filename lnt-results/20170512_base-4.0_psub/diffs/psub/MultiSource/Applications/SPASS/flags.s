	.text
	.file	"flags.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_1:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	0                       # 0x0
.LCPI0_2:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
.LCPI0_3:
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	100                     # 0x64
	.text
	.globl	flag_Init
	.p2align	4, 0x90
	.type	flag_Init,@function
flag_Init:                              # @flag_Init
	.cfi_startproc
# BB#0:
	movl	$3, flag_PROPERTIES+8(%rip)
	movq	$.L.str, flag_PROPERTIES+16(%rip)
	movl	$-1, flag_PROPERTIES(%rip)
	movl	$2, flag_PROPERTIES+4(%rip)
	movl	$3, flag_PROPERTIES+104(%rip)
	movq	$.L.str.1, flag_PROPERTIES+112(%rip)
	movl	$-1, flag_PROPERTIES+96(%rip)
	movl	$2, flag_PROPERTIES+100(%rip)
	movl	$0, flag_DEFAULTSTORE+16(%rip)
	movl	$3, flag_PROPERTIES+32(%rip)
	movq	$.L.str.2, flag_PROPERTIES+40(%rip)
	movl	$-1, flag_PROPERTIES+24(%rip)
	movl	$2, flag_PROPERTIES+28(%rip)
	movl	$3, flag_PROPERTIES+56(%rip)
	movq	$.L.str.3, flag_PROPERTIES+64(%rip)
	movl	$-1, flag_PROPERTIES+48(%rip)
	movl	$2, flag_PROPERTIES+52(%rip)
	movl	$3, flag_PROPERTIES+80(%rip)
	movq	$.L.str.4, flag_PROPERTIES+88(%rip)
	movl	$-1, flag_PROPERTIES+72(%rip)
	movl	$2, flag_PROPERTIES+76(%rip)
	movl	$1, %eax
	movd	%eax, %xmm0
	movdqa	%xmm0, flag_DEFAULTSTORE(%rip)
	movl	$3, flag_PROPERTIES+272(%rip)
	movq	$.L.str.5, flag_PROPERTIES+280(%rip)
	movl	$-2, flag_PROPERTIES+264(%rip)
	movl	$2147483647, flag_PROPERTIES+268(%rip) # imm = 0x7FFFFFFF
	movl	$3, flag_PROPERTIES+128(%rip)
	movq	$.L.str.6, flag_PROPERTIES+136(%rip)
	movl	$-2, flag_PROPERTIES+120(%rip)
	movl	$2147483647, flag_PROPERTIES+124(%rip) # imm = 0x7FFFFFFF
	movl	$0, flag_DEFAULTSTORE+20(%rip)
	movl	$3, flag_PROPERTIES+968(%rip)
	movq	$.L.str.7, flag_PROPERTIES+976(%rip)
	movl	$-1, flag_PROPERTIES+960(%rip)
	movl	$3, flag_PROPERTIES+964(%rip)
	movl	$0, flag_DEFAULTSTORE+160(%rip)
	movl	$1, flag_PROPERTIES+296(%rip)
	movq	$.L.str.8, flag_PROPERTIES+304(%rip)
	movl	$-1, flag_PROPERTIES+288(%rip)
	movl	$2, flag_PROPERTIES+292(%rip)
	movl	$3, flag_PROPERTIES+152(%rip)
	movq	$.L.str.9, flag_PROPERTIES+160(%rip)
	movl	$-2, flag_PROPERTIES+144(%rip)
	movl	$2147483647, flag_PROPERTIES+148(%rip) # imm = 0x7FFFFFFF
	movl	$-1, flag_DEFAULTSTORE+24(%rip)
	movl	$1, flag_PROPERTIES+200(%rip)
	movq	$.L.str.10, flag_PROPERTIES+208(%rip)
	movl	$-1, flag_PROPERTIES+192(%rip)
	movl	$2, flag_PROPERTIES+196(%rip)
	movl	$0, flag_DEFAULTSTORE+32(%rip)
	movl	$1, flag_PROPERTIES+320(%rip)
	movq	$.L.str.11, flag_PROPERTIES+328(%rip)
	movl	$-1, flag_PROPERTIES+312(%rip)
	movl	$2, flag_PROPERTIES+316(%rip)
	movl	$1, flag_PROPERTIES+344(%rip)
	movq	$.L.str.12, flag_PROPERTIES+352(%rip)
	movl	$-1, flag_PROPERTIES+336(%rip)
	movl	$2, flag_PROPERTIES+340(%rip)
	movl	$-1, %eax
	movd	%eax, %xmm0
	movdqu	%xmm0, flag_DEFAULTSTORE+44(%rip)
	movl	$1, flag_PROPERTIES+368(%rip)
	movq	$.L.str.13, flag_PROPERTIES+376(%rip)
	movl	$-1, flag_PROPERTIES+360(%rip)
	movl	$2, flag_PROPERTIES+364(%rip)
	movl	$0, flag_DEFAULTSTORE+60(%rip)
	movl	$1, flag_PROPERTIES+536(%rip)
	movq	$.L.str.14, flag_PROPERTIES+544(%rip)
	movl	$-1, flag_PROPERTIES+528(%rip)
	movl	$2, flag_PROPERTIES+532(%rip)
	movl	$0, flag_DEFAULTSTORE+88(%rip)
	movl	$1, flag_PROPERTIES+392(%rip)
	movq	$.L.str.15, flag_PROPERTIES+400(%rip)
	movl	$-1, flag_PROPERTIES+384(%rip)
	movl	$2, flag_PROPERTIES+388(%rip)
	movl	$0, flag_DEFAULTSTORE+64(%rip)
	movl	$1, flag_PROPERTIES+416(%rip)
	movq	$.L.str.16, flag_PROPERTIES+424(%rip)
	movl	$-1, flag_PROPERTIES+408(%rip)
	movl	$2, flag_PROPERTIES+412(%rip)
	movl	$1, flag_PROPERTIES+440(%rip)
	movq	$.L.str.17, flag_PROPERTIES+448(%rip)
	movl	$-1, flag_PROPERTIES+432(%rip)
	movl	$2, flag_PROPERTIES+436(%rip)
	movl	$1, flag_PROPERTIES+464(%rip)
	movq	$.L.str.18, flag_PROPERTIES+472(%rip)
	movl	$-1, flag_PROPERTIES+456(%rip)
	movl	$2, flag_PROPERTIES+460(%rip)
	movl	$3, flag_PROPERTIES+224(%rip)
	movq	$.L.str.19, flag_PROPERTIES+232(%rip)
	movl	$-1, flag_PROPERTIES+216(%rip)
	movl	$2, flag_PROPERTIES+220(%rip)
	movl	$0, flag_DEFAULTSTORE+36(%rip)
	movl	$1, flag_PROPERTIES+488(%rip)
	movq	$.L.str.20, flag_PROPERTIES+496(%rip)
	movl	$-1, flag_PROPERTIES+480(%rip)
	movl	$2, flag_PROPERTIES+484(%rip)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, flag_DEFAULTSTORE+68(%rip)
	movl	$1, flag_PROPERTIES+512(%rip)
	movq	$.L.str.21, flag_PROPERTIES+520(%rip)
	movl	$-1, flag_PROPERTIES+504(%rip)
	movl	$2, flag_PROPERTIES+508(%rip)
	movl	$0, flag_DEFAULTSTORE+84(%rip)
	movl	$1, flag_PROPERTIES+560(%rip)
	movq	$.L.str.22, flag_PROPERTIES+568(%rip)
	movl	$-1, flag_PROPERTIES+552(%rip)
	movl	$2, flag_PROPERTIES+556(%rip)
	movl	$1, flag_PROPERTIES+584(%rip)
	movq	$.L.str.23, flag_PROPERTIES+592(%rip)
	movl	$-1, flag_PROPERTIES+576(%rip)
	movl	$2, flag_PROPERTIES+580(%rip)
	movl	$1, flag_PROPERTIES+608(%rip)
	movq	$.L.str.24, flag_PROPERTIES+616(%rip)
	movl	$-1, flag_PROPERTIES+600(%rip)
	movl	$2, flag_PROPERTIES+604(%rip)
	movl	$1, flag_PROPERTIES+632(%rip)
	movq	$.L.str.25, flag_PROPERTIES+640(%rip)
	movl	$-1, flag_PROPERTIES+624(%rip)
	movl	$2, flag_PROPERTIES+628(%rip)
	movaps	.LCPI0_0(%rip), %xmm1   # xmm1 = [0,1,0,0]
	movups	%xmm1, flag_DEFAULTSTORE+92(%rip)
	movl	$1, flag_PROPERTIES+248(%rip)
	movq	$.L.str.26, flag_PROPERTIES+256(%rip)
	movl	$-1, flag_PROPERTIES+240(%rip)
	movl	$2, flag_PROPERTIES+244(%rip)
	movl	$0, flag_DEFAULTSTORE+40(%rip)
	movl	$1, flag_PROPERTIES+656(%rip)
	movq	$.L.str.27, flag_PROPERTIES+664(%rip)
	movl	$-1, flag_PROPERTIES+648(%rip)
	movl	$2, flag_PROPERTIES+652(%rip)
	movl	$1, flag_PROPERTIES+680(%rip)
	movq	$.L.str.28, flag_PROPERTIES+688(%rip)
	movl	$-1, flag_PROPERTIES+672(%rip)
	movl	$2, flag_PROPERTIES+676(%rip)
	movl	$1, flag_PROPERTIES+704(%rip)
	movq	$.L.str.29, flag_PROPERTIES+712(%rip)
	movl	$-1, flag_PROPERTIES+696(%rip)
	movl	$2, flag_PROPERTIES+700(%rip)
	movl	$1, flag_PROPERTIES+728(%rip)
	movq	$.L.str.30, flag_PROPERTIES+736(%rip)
	movl	$-1, flag_PROPERTIES+720(%rip)
	movl	$3, flag_PROPERTIES+724(%rip)
	movaps	.LCPI0_1(%rip), %xmm1   # xmm1 = [1,0,1,0]
	movups	%xmm1, flag_DEFAULTSTORE+108(%rip)
	movl	$1, flag_PROPERTIES+752(%rip)
	movq	$.L.str.31, flag_PROPERTIES+760(%rip)
	movl	$-1, flag_PROPERTIES+744(%rip)
	movl	$2, flag_PROPERTIES+748(%rip)
	movl	$0, flag_DEFAULTSTORE+124(%rip)
	movl	$1, flag_PROPERTIES+776(%rip)
	movq	$.L.str.32, flag_PROPERTIES+784(%rip)
	movl	$-1, flag_PROPERTIES+768(%rip)
	movl	$2, flag_PROPERTIES+772(%rip)
	movl	$1, flag_PROPERTIES+800(%rip)
	movq	$.L.str.33, flag_PROPERTIES+808(%rip)
	movl	$-1, flag_PROPERTIES+792(%rip)
	movl	$2, flag_PROPERTIES+796(%rip)
	movl	$1, flag_PROPERTIES+824(%rip)
	movq	$.L.str.34, flag_PROPERTIES+832(%rip)
	movl	$-1, flag_PROPERTIES+816(%rip)
	movl	$2, flag_PROPERTIES+820(%rip)
	movl	$1, flag_PROPERTIES+848(%rip)
	movq	$.L.str.35, flag_PROPERTIES+856(%rip)
	movl	$-1, flag_PROPERTIES+840(%rip)
	movl	$2, flag_PROPERTIES+844(%rip)
	movdqa	%xmm0, flag_DEFAULTSTORE+128(%rip)
	movl	$1, flag_PROPERTIES+872(%rip)
	movq	$.L.str.36, flag_PROPERTIES+880(%rip)
	movl	$-1, flag_PROPERTIES+864(%rip)
	movl	$2, flag_PROPERTIES+868(%rip)
	movl	$0, flag_DEFAULTSTORE+144(%rip)
	movl	$1, flag_PROPERTIES+896(%rip)
	movq	$.L.str.37, flag_PROPERTIES+904(%rip)
	movl	$-1, flag_PROPERTIES+888(%rip)
	movl	$2, flag_PROPERTIES+892(%rip)
	movl	$0, flag_DEFAULTSTORE+148(%rip)
	movl	$3, flag_PROPERTIES+176(%rip)
	movq	$.L.str.38, flag_PROPERTIES+184(%rip)
	movl	$-2, flag_PROPERTIES+168(%rip)
	movl	$2147483647, flag_PROPERTIES+172(%rip) # imm = 0x7FFFFFFF
	movl	$-1, flag_DEFAULTSTORE+28(%rip)
	movl	$3, flag_PROPERTIES+920(%rip)
	movq	$.L.str.39, flag_PROPERTIES+928(%rip)
	movl	$-1, flag_PROPERTIES+912(%rip)
	movl	$3, flag_PROPERTIES+916(%rip)
	movl	$1, flag_DEFAULTSTORE+152(%rip)
	movl	$0, flag_PROPERTIES+1424(%rip)
	movq	$.L.str.40, flag_PROPERTIES+1432(%rip)
	movl	$-1, flag_PROPERTIES+1416(%rip)
	movl	$2, flag_PROPERTIES+1420(%rip)
	movl	$0, flag_PROPERTIES+1448(%rip)
	movq	$.L.str.41, flag_PROPERTIES+1456(%rip)
	movl	$-1, flag_PROPERTIES+1440(%rip)
	movl	$2, flag_PROPERTIES+1444(%rip)
	movl	$0, flag_PROPERTIES+1472(%rip)
	movq	$.L.str.42, flag_PROPERTIES+1480(%rip)
	movl	$-1, flag_PROPERTIES+1464(%rip)
	movl	$2, flag_PROPERTIES+1468(%rip)
	movl	$0, flag_PROPERTIES+1496(%rip)
	movq	$.L.str.43, flag_PROPERTIES+1504(%rip)
	movl	$-1, flag_PROPERTIES+1488(%rip)
	movl	$2, flag_PROPERTIES+1492(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+236(%rip)
	movl	$0, flag_PROPERTIES+1520(%rip)
	movq	$.L.str.44, flag_PROPERTIES+1528(%rip)
	movl	$-1, flag_PROPERTIES+1512(%rip)
	movl	$2, flag_PROPERTIES+1516(%rip)
	movl	$0, flag_PROPERTIES+1544(%rip)
	movq	$.L.str.45, flag_PROPERTIES+1552(%rip)
	movl	$-1, flag_PROPERTIES+1536(%rip)
	movl	$2, flag_PROPERTIES+1540(%rip)
	movl	$0, flag_PROPERTIES+1568(%rip)
	movq	$.L.str.46, flag_PROPERTIES+1576(%rip)
	movl	$-1, flag_PROPERTIES+1560(%rip)
	movl	$2, flag_PROPERTIES+1564(%rip)
	movl	$0, flag_PROPERTIES+1592(%rip)
	movq	$.L.str.47, flag_PROPERTIES+1600(%rip)
	movl	$-1, flag_PROPERTIES+1584(%rip)
	movl	$2, flag_PROPERTIES+1588(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+252(%rip)
	movl	$0, flag_PROPERTIES+1616(%rip)
	movq	$.L.str.48, flag_PROPERTIES+1624(%rip)
	movl	$-1, flag_PROPERTIES+1608(%rip)
	movl	$2, flag_PROPERTIES+1612(%rip)
	movl	$0, flag_PROPERTIES+1640(%rip)
	movq	$.L.str.49, flag_PROPERTIES+1648(%rip)
	movl	$-1, flag_PROPERTIES+1632(%rip)
	movl	$2, flag_PROPERTIES+1636(%rip)
	movl	$0, flag_PROPERTIES+1664(%rip)
	movq	$.L.str.50, flag_PROPERTIES+1672(%rip)
	movl	$-1, flag_PROPERTIES+1656(%rip)
	movl	$3, flag_PROPERTIES+1660(%rip)
	movl	$0, flag_PROPERTIES+1688(%rip)
	movq	$.L.str.51, flag_PROPERTIES+1696(%rip)
	movl	$-1, flag_PROPERTIES+1680(%rip)
	movl	$3, flag_PROPERTIES+1684(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+268(%rip)
	movl	$0, flag_PROPERTIES+1712(%rip)
	movq	$.L.str.52, flag_PROPERTIES+1720(%rip)
	movl	$-1, flag_PROPERTIES+1704(%rip)
	movl	$2, flag_PROPERTIES+1708(%rip)
	movl	$0, flag_PROPERTIES+1736(%rip)
	movq	$.L.str.53, flag_PROPERTIES+1744(%rip)
	movl	$-1, flag_PROPERTIES+1728(%rip)
	movl	$2, flag_PROPERTIES+1732(%rip)
	movl	$0, flag_PROPERTIES+1760(%rip)
	movq	$.L.str.54, flag_PROPERTIES+1768(%rip)
	movl	$-1, flag_PROPERTIES+1752(%rip)
	movl	$2, flag_PROPERTIES+1756(%rip)
	movl	$0, flag_PROPERTIES+1784(%rip)
	movq	$.L.str.55, flag_PROPERTIES+1792(%rip)
	movl	$-1, flag_PROPERTIES+1776(%rip)
	movl	$3, flag_PROPERTIES+1780(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+284(%rip)
	movl	$0, flag_PROPERTIES+1808(%rip)
	movq	$.L.str.56, flag_PROPERTIES+1816(%rip)
	movl	$-1, flag_PROPERTIES+1800(%rip)
	movl	$2, flag_PROPERTIES+1804(%rip)
	movl	$0, flag_PROPERTIES+1856(%rip)
	movq	$.L.str.57, flag_PROPERTIES+1864(%rip)
	movl	$-1, flag_PROPERTIES+1848(%rip)
	movl	$2, flag_PROPERTIES+1852(%rip)
	movl	$0, flag_PROPERTIES+1880(%rip)
	movq	$.L.str.58, flag_PROPERTIES+1888(%rip)
	movl	$-1, flag_PROPERTIES+1872(%rip)
	movl	$2, flag_PROPERTIES+1876(%rip)
	movl	$0, flag_PROPERTIES+1832(%rip)
	movq	$.L.str.59, flag_PROPERTIES+1840(%rip)
	movl	$-1, flag_PROPERTIES+1824(%rip)
	movl	$2, flag_PROPERTIES+1828(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+300(%rip)
	movl	$2, flag_PROPERTIES+1904(%rip)
	movq	$.L.str.60, flag_PROPERTIES+1912(%rip)
	movl	$-1, flag_PROPERTIES+1896(%rip)
	movl	$2, flag_PROPERTIES+1900(%rip)
	movl	$2, flag_PROPERTIES+1928(%rip)
	movq	$.L.str.61, flag_PROPERTIES+1936(%rip)
	movl	$-1, flag_PROPERTIES+1920(%rip)
	movl	$2, flag_PROPERTIES+1924(%rip)
	movl	$2, flag_PROPERTIES+1952(%rip)
	movq	$.L.str.62, flag_PROPERTIES+1960(%rip)
	movl	$-1, flag_PROPERTIES+1944(%rip)
	movl	$2, flag_PROPERTIES+1948(%rip)
	movl	$2, flag_PROPERTIES+1976(%rip)
	movq	$.L.str.63, flag_PROPERTIES+1984(%rip)
	movl	$-1, flag_PROPERTIES+1968(%rip)
	movl	$2, flag_PROPERTIES+1972(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+316(%rip)
	movl	$2, flag_PROPERTIES+2072(%rip)
	movq	$.L.str.64, flag_PROPERTIES+2080(%rip)
	movl	$-1, flag_PROPERTIES+2064(%rip)
	movl	$2, flag_PROPERTIES+2068(%rip)
	movl	$2, flag_PROPERTIES+2096(%rip)
	movq	$.L.str.65, flag_PROPERTIES+2104(%rip)
	movl	$-1, flag_PROPERTIES+2088(%rip)
	movl	$2147483647, flag_PROPERTIES+2092(%rip) # imm = 0x7FFFFFFF
	movl	$2, flag_PROPERTIES+2192(%rip)
	movq	$.L.str.66, flag_PROPERTIES+2200(%rip)
	movl	$-1, flag_PROPERTIES+2184(%rip)
	movl	$2, flag_PROPERTIES+2188(%rip)
	movl	$2, flag_PROPERTIES+2216(%rip)
	movq	$.L.str.67, flag_PROPERTIES+2224(%rip)
	movl	$-1, flag_PROPERTIES+2208(%rip)
	movl	$2, flag_PROPERTIES+2212(%rip)
	movl	$2, flag_PROPERTIES+2000(%rip)
	movq	$.L.str.68, flag_PROPERTIES+2008(%rip)
	movl	$-1, flag_PROPERTIES+1992(%rip)
	movl	$2, flag_PROPERTIES+1996(%rip)
	movl	$2, flag_PROPERTIES+2024(%rip)
	movq	$.L.str.69, flag_PROPERTIES+2032(%rip)
	movl	$-1, flag_PROPERTIES+2016(%rip)
	movl	$2, flag_PROPERTIES+2020(%rip)
	movl	$2, flag_PROPERTIES+2048(%rip)
	movq	$.L.str.70, flag_PROPERTIES+2056(%rip)
	movl	$-1, flag_PROPERTIES+2040(%rip)
	movl	$2, flag_PROPERTIES+2044(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+332(%rip)
	movl	$2, flag_PROPERTIES+2120(%rip)
	movq	$.L.str.71, flag_PROPERTIES+2128(%rip)
	movl	$-1, flag_PROPERTIES+2112(%rip)
	movl	$3, flag_PROPERTIES+2116(%rip)
	movl	$2, flag_PROPERTIES+2168(%rip)
	movq	$.L.str.72, flag_PROPERTIES+2176(%rip)
	movl	$-1, flag_PROPERTIES+2160(%rip)
	movl	$2, flag_PROPERTIES+2164(%rip)
	movl	$2, flag_PROPERTIES+2144(%rip)
	movq	$.L.str.73, flag_PROPERTIES+2152(%rip)
	movl	$-1, flag_PROPERTIES+2136(%rip)
	movl	$2, flag_PROPERTIES+2140(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+348(%rip)
	movl	$2, flag_PROPERTIES+2240(%rip)
	movq	$.L.str.74, flag_PROPERTIES+2248(%rip)
	movl	$-1, flag_PROPERTIES+2232(%rip)
	movl	$3, flag_PROPERTIES+2236(%rip)
	movl	$2, flag_PROPERTIES+2264(%rip)
	movq	$.L.str.75, flag_PROPERTIES+2272(%rip)
	movl	$-1, flag_PROPERTIES+2256(%rip)
	movl	$2, flag_PROPERTIES+2260(%rip)
	movdqu	%xmm0, flag_DEFAULTSTORE+364(%rip)
	movl	$3, flag_PROPERTIES+944(%rip)
	movq	$.L.str.76, flag_PROPERTIES+952(%rip)
	movl	$-1, flag_PROPERTIES+936(%rip)
	movl	$2, flag_PROPERTIES+940(%rip)
	movl	$1, flag_DEFAULTSTORE+156(%rip)
	movl	$3, flag_PROPERTIES+1232(%rip)
	movq	$.L.str.77, flag_PROPERTIES+1240(%rip)
	movl	$-1, flag_PROPERTIES+1224(%rip)
	movl	$2147483647, flag_PROPERTIES+1228(%rip) # imm = 0x7FFFFFFF
	movl	$0, flag_DEFAULTSTORE+204(%rip)
	movl	$3, flag_PROPERTIES+1064(%rip)
	movq	$.L.str.78, flag_PROPERTIES+1072(%rip)
	movl	$-1, flag_PROPERTIES+1056(%rip)
	movl	$2, flag_PROPERTIES+1060(%rip)
	movl	$1, flag_DEFAULTSTORE+176(%rip)
	movl	$3, flag_PROPERTIES+992(%rip)
	movq	$.L.str.79, flag_PROPERTIES+1000(%rip)
	movl	$-1, flag_PROPERTIES+984(%rip)
	movl	$2, flag_PROPERTIES+988(%rip)
	movl	$0, flag_DEFAULTSTORE+164(%rip)
	movl	$3, flag_PROPERTIES+1016(%rip)
	movq	$.L.str.80, flag_PROPERTIES+1024(%rip)
	movl	$0, flag_PROPERTIES+1008(%rip)
	movl	$2147483647, flag_PROPERTIES+1012(%rip) # imm = 0x7FFFFFFF
	movl	$5, flag_DEFAULTSTORE+168(%rip)
	movl	$3, flag_PROPERTIES+1040(%rip)
	movq	$.L.str.81, flag_PROPERTIES+1048(%rip)
	movl	$0, flag_PROPERTIES+1032(%rip)
	movl	$2147483647, flag_PROPERTIES+1036(%rip) # imm = 0x7FFFFFFF
	movl	$1, flag_DEFAULTSTORE+172(%rip)
	movl	$3, flag_PROPERTIES+1088(%rip)
	movq	$.L.str.82, flag_PROPERTIES+1096(%rip)
	movl	$0, flag_PROPERTIES+1080(%rip)
	movl	$2147483647, flag_PROPERTIES+1084(%rip) # imm = 0x7FFFFFFF
	movl	$3, flag_PROPERTIES+1112(%rip)
	movq	$.L.str.83, flag_PROPERTIES+1120(%rip)
	movl	$0, flag_PROPERTIES+1104(%rip)
	movl	$2147483647, flag_PROPERTIES+1108(%rip) # imm = 0x7FFFFFFF
	movl	$3, flag_PROPERTIES+1136(%rip)
	movq	$.L.str.84, flag_PROPERTIES+1144(%rip)
	movl	$-1, flag_PROPERTIES+1128(%rip)
	movl	$2, flag_PROPERTIES+1132(%rip)
	movl	$3, flag_PROPERTIES+1160(%rip)
	movq	$.L.str.85, flag_PROPERTIES+1168(%rip)
	movl	$-1, flag_PROPERTIES+1152(%rip)
	movl	$3, flag_PROPERTIES+1156(%rip)
	movaps	.LCPI0_2(%rip), %xmm0   # xmm0 = [1,1,0,0]
	movups	%xmm0, flag_DEFAULTSTORE+180(%rip)
	movl	$3, flag_PROPERTIES+1184(%rip)
	movq	$.L.str.86, flag_PROPERTIES+1192(%rip)
	movl	$-2, flag_PROPERTIES+1176(%rip)
	movl	$2147483647, flag_PROPERTIES+1180(%rip) # imm = 0x7FFFFFFF
	movl	$-1, flag_DEFAULTSTORE+196(%rip)
	movl	$3, flag_PROPERTIES+1208(%rip)
	movq	$.L.str.87, flag_PROPERTIES+1216(%rip)
	movl	$0, flag_PROPERTIES+1200(%rip)
	movl	$2147483647, flag_PROPERTIES+1204(%rip) # imm = 0x7FFFFFFF
	movl	$1, flag_DEFAULTSTORE+200(%rip)
	movl	$3, flag_PROPERTIES+1256(%rip)
	movq	$.L.str.88, flag_PROPERTIES+1264(%rip)
	movl	$-1, flag_PROPERTIES+1248(%rip)
	movl	$2, flag_PROPERTIES+1252(%rip)
	movl	$3, flag_PROPERTIES+1280(%rip)
	movq	$.L.str.89, flag_PROPERTIES+1288(%rip)
	movl	$-1, flag_PROPERTIES+1272(%rip)
	movl	$2, flag_PROPERTIES+1276(%rip)
	movl	$3, flag_PROPERTIES+1328(%rip)
	movq	$.L.str.90, flag_PROPERTIES+1336(%rip)
	movl	$0, flag_PROPERTIES+1320(%rip)
	movl	$2147483647, flag_PROPERTIES+1324(%rip) # imm = 0x7FFFFFFF
	movl	$3, flag_PROPERTIES+1352(%rip)
	movq	$.L.str.91, flag_PROPERTIES+1360(%rip)
	movl	$-1, flag_PROPERTIES+1344(%rip)
	movl	$2, flag_PROPERTIES+1348(%rip)
	movl	$1, flag_DEFAULTSTORE+224(%rip)
	movl	$3, flag_PROPERTIES+1376(%rip)
	movq	$.L.str.92, flag_PROPERTIES+1384(%rip)
	movl	$-1, flag_PROPERTIES+1368(%rip)
	movl	$2, flag_PROPERTIES+1372(%rip)
	movl	$0, flag_DEFAULTSTORE+228(%rip)
	movl	$3, flag_PROPERTIES+1304(%rip)
	movq	$.L.str.93, flag_PROPERTIES+1312(%rip)
	movl	$-1, flag_PROPERTIES+1296(%rip)
	movl	$2, flag_PROPERTIES+1300(%rip)
	movaps	.LCPI0_3(%rip), %xmm0   # xmm0 = [0,1,1,100]
	movaps	%xmm0, flag_DEFAULTSTORE+208(%rip)
	movl	$3, flag_PROPERTIES+1400(%rip)
	movq	$.L.str.94, flag_PROPERTIES+1408(%rip)
	movl	$-1, flag_PROPERTIES+1392(%rip)
	movl	$2, flag_PROPERTIES+1396(%rip)
	movl	$1, flag_DEFAULTSTORE+232(%rip)
	movl	$3, flag_PROPERTIES+2288(%rip)
	movq	$.L.str.95, flag_PROPERTIES+2296(%rip)
	movl	$-1, flag_PROPERTIES+2280(%rip)
	movl	$4, flag_PROPERTIES+2284(%rip)
	movl	$0, flag_DEFAULTSTORE+380(%rip)
	retq
.Lfunc_end0:
	.size	flag_Init, .Lfunc_end0-flag_Init
	.cfi_endproc

	.globl	flag_DefaultStore
	.p2align	4, 0x90
	.type	flag_DefaultStore,@function
flag_DefaultStore:                      # @flag_DefaultStore
	.cfi_startproc
# BB#0:
	movl	$flag_DEFAULTSTORE, %eax
	retq
.Lfunc_end1:
	.size	flag_DefaultStore, .Lfunc_end1-flag_DefaultStore
	.cfi_endproc

	.globl	flag_Print
	.p2align	4, 0x90
	.type	flag_Print,@function
flag_Print:                             # @flag_Print
	.cfi_startproc
# BB#0:
	movq	%rdi, %rax
	movq	stdout(%rip), %rdi
	movq	%rax, %rsi
	jmp	flag_FPrint             # TAILCALL
.Lfunc_end2:
	.size	flag_Print, .Lfunc_end2-flag_Print
	.cfi_endproc

	.globl	flag_FPrint
	.p2align	4, 0x90
	.type	flag_FPrint,@function
flag_FPrint:                            # @flag_FPrint
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 80
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r13, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movq	%rsi, %r15
	movq	%rdi, %r14
	movl	$.L.str.96, %edi
	movl	$26, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	movl	$flag_PROPERTIES+64, %r13d
	movq	%rsp, %r12
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB3_1:                                # =>This Inner Loop Header: Depth=1
	movq	-48(%r13), %rdx
	movl	(%r15,%rbx,4), %ecx
	movl	$.L.str.97, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sprintf
	movl	$.L.str.98, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movq	-24(%r13), %rdx
	movl	4(%r15,%rbx,4), %ecx
	movl	$.L.str.97, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sprintf
	movl	$.L.str.99, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fprintf
	movq	(%r13), %rdx
	movl	8(%r15,%rbx,4), %ecx
	movl	$.L.str.100, %esi
	xorl	%eax, %eax
	movq	%r12, %rdi
	callq	sprintf
	movl	$.L.str.99, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%r12, %rdx
	callq	fprintf
	addq	$3, %rbx
	addq	$72, %r13
	cmpq	$96, %rbx
	jb	.LBB3_1
# BB#2:
	movl	$.L.str.101, %edi
	movl	$16, %esi
	movl	$1, %edx
	movq	%r14, %rcx
	callq	fwrite
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end3:
	.size	flag_FPrint, .Lfunc_end3-flag_FPrint
	.cfi_endproc

	.globl	flag_Name
	.p2align	4, 0x90
	.type	flag_Name,@function
flag_Name:                              # @flag_Name
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	leaq	(%rax,%rax,2), %rax
	movq	flag_PROPERTIES+16(,%rax,8), %rax
	retq
.Lfunc_end4:
	.size	flag_Name, .Lfunc_end4-flag_Name
	.cfi_endproc

	.globl	flag_Lookup
	.p2align	4, 0x90
	.type	flag_Lookup,@function
flag_Lookup:                            # @flag_Lookup
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	movl	$flag_PROPERTIES+16, %ebx
	.p2align	4, 0x90
.LBB5_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB5_4
# BB#2:                                 #   in Loop: Header=BB5_1 Depth=1
	incq	%r15
	addq	$24, %rbx
	cmpq	$96, %r15
	jb	.LBB5_1
# BB#3:
	movl	$-1, %r15d
.LBB5_4:                                # %flag_Id.exit
	xorl	%eax, %eax
	cmpl	$-1, %r15d
	setne	%al
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	flag_Lookup, .Lfunc_end5-flag_Lookup
	.cfi_endproc

	.globl	flag_Id
	.p2align	4, 0x90
	.type	flag_Id,@function
flag_Id:                                # @flag_Id
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi17:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi18:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 32
.Lcfi20:
	.cfi_offset %rbx, -32
.Lcfi21:
	.cfi_offset %r14, -24
.Lcfi22:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	xorl	%r15d, %r15d
	movl	$flag_PROPERTIES+16, %ebx
	.p2align	4, 0x90
.LBB6_1:                                # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r14, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_4
# BB#2:                                 #   in Loop: Header=BB6_1 Depth=1
	incq	%r15
	addq	$24, %rbx
	cmpq	$96, %r15
	jb	.LBB6_1
# BB#3:
	movl	$-1, %r15d
.LBB6_4:
	movl	%r15d, %eax
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	flag_Id, .Lfunc_end6-flag_Id
	.cfi_endproc

	.globl	flag_Minimum
	.p2align	4, 0x90
	.type	flag_Minimum,@function
flag_Minimum:                           # @flag_Minimum
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	leaq	(%rax,%rax,2), %rax
	movl	flag_PROPERTIES(,%rax,8), %eax
	retq
.Lfunc_end7:
	.size	flag_Minimum, .Lfunc_end7-flag_Minimum
	.cfi_endproc

	.globl	flag_Maximum
	.p2align	4, 0x90
	.type	flag_Maximum,@function
flag_Maximum:                           # @flag_Maximum
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	leaq	(%rax,%rax,2), %rax
	movl	flag_PROPERTIES+4(,%rax,8), %eax
	retq
.Lfunc_end8:
	.size	flag_Maximum, .Lfunc_end8-flag_Maximum
	.cfi_endproc

	.globl	flag_Type
	.p2align	4, 0x90
	.type	flag_Type,@function
flag_Type:                              # @flag_Type
	.cfi_startproc
# BB#0:
	movl	%edi, %eax
	leaq	(%rax,%rax,2), %rax
	movl	flag_PROPERTIES+8(,%rax,8), %eax
	retq
.Lfunc_end9:
	.size	flag_Type, .Lfunc_end9-flag_Type
	.cfi_endproc

	.globl	flag_ClearInferenceRules
	.p2align	4, 0x90
	.type	flag_ClearInferenceRules,@function
flag_ClearInferenceRules:               # @flag_ClearInferenceRules
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 16
.Lcfi24:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	movl	$flag_PROPERTIES+16, %ebx
	.p2align	4, 0x90
.LBB10_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$0, -8(%rbx)
	jne	.LBB10_8
# BB#2:                                 #   in Loop: Header=BB10_1 Depth=1
	cmpl	$0, -16(%rbx)
	jns	.LBB10_3
# BB#5:                                 #   in Loop: Header=BB10_1 Depth=1
	cmpl	$0, -12(%rbx)
	jle	.LBB10_6
# BB#7:                                 # %flag_SetFlagValue.exit
                                        #   in Loop: Header=BB10_1 Depth=1
	movl	$0, (%rdi,%rax,4)
.LBB10_8:                               #   in Loop: Header=BB10_1 Depth=1
	incq	%rax
	addq	$24, %rbx
	cmpq	$96, %rax
	jb	.LBB10_1
# BB#9:
	popq	%rbx
	retq
.LBB10_3:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.102, %edi
	jmp	.LBB10_4
.LBB10_6:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.103, %edi
.LBB10_4:
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end10:
	.size	flag_ClearInferenceRules, .Lfunc_end10-flag_ClearInferenceRules
	.cfi_endproc

	.globl	flag_ClearReductionRules
	.p2align	4, 0x90
	.type	flag_ClearReductionRules,@function
flag_ClearReductionRules:               # @flag_ClearReductionRules
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 16
.Lcfi26:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	movl	$flag_PROPERTIES+16, %ebx
	.p2align	4, 0x90
.LBB11_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$2, -8(%rbx)
	jne	.LBB11_8
# BB#2:                                 #   in Loop: Header=BB11_1 Depth=1
	cmpl	$0, -16(%rbx)
	jns	.LBB11_3
# BB#5:                                 #   in Loop: Header=BB11_1 Depth=1
	cmpl	$0, -12(%rbx)
	jle	.LBB11_6
# BB#7:                                 # %flag_SetFlagValue.exit
                                        #   in Loop: Header=BB11_1 Depth=1
	movl	$0, (%rdi,%rax,4)
.LBB11_8:                               #   in Loop: Header=BB11_1 Depth=1
	incq	%rax
	addq	$24, %rbx
	cmpq	$96, %rax
	jb	.LBB11_1
# BB#9:
	popq	%rbx
	retq
.LBB11_3:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.102, %edi
	jmp	.LBB11_4
.LBB11_6:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.103, %edi
.LBB11_4:
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end11:
	.size	flag_ClearReductionRules, .Lfunc_end11-flag_ClearReductionRules
	.cfi_endproc

	.globl	flag_ClearPrinting
	.p2align	4, 0x90
	.type	flag_ClearPrinting,@function
flag_ClearPrinting:                     # @flag_ClearPrinting
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi27:
	.cfi_def_cfa_offset 16
.Lcfi28:
	.cfi_offset %rbx, -16
	xorl	%eax, %eax
	movl	$flag_PROPERTIES+16, %ebx
	.p2align	4, 0x90
.LBB12_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$1, -8(%rbx)
	jne	.LBB12_8
# BB#2:                                 #   in Loop: Header=BB12_1 Depth=1
	cmpl	$0, -16(%rbx)
	jns	.LBB12_3
# BB#5:                                 #   in Loop: Header=BB12_1 Depth=1
	cmpl	$0, -12(%rbx)
	jle	.LBB12_6
# BB#7:                                 # %flag_SetFlagValue.exit
                                        #   in Loop: Header=BB12_1 Depth=1
	movl	$0, (%rdi,%rax,4)
.LBB12_8:                               #   in Loop: Header=BB12_1 Depth=1
	incq	%rax
	addq	$24, %rbx
	cmpq	$96, %rax
	jb	.LBB12_1
# BB#9:
	popq	%rbx
	retq
.LBB12_3:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.102, %edi
	jmp	.LBB12_4
.LBB12_6:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.103, %edi
.LBB12_4:
	xorl	%esi, %esi
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end12:
	.size	flag_ClearPrinting, .Lfunc_end12-flag_ClearPrinting
	.cfi_endproc

	.globl	flag_SetReductionsToDefaults
	.p2align	4, 0x90
	.type	flag_SetReductionsToDefaults,@function
flag_SetReductionsToDefaults:           # @flag_SetReductionsToDefaults
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi31:
	.cfi_def_cfa_offset 32
.Lcfi32:
	.cfi_offset %rbx, -24
.Lcfi33:
	.cfi_offset %rbp, -16
	xorl	%eax, %eax
	movl	$flag_PROPERTIES+16, %ebx
	.p2align	4, 0x90
.LBB13_1:                               # =>This Inner Loop Header: Depth=1
	cmpl	$2, -8(%rbx)
	jne	.LBB13_8
# BB#2:                                 #   in Loop: Header=BB13_1 Depth=1
	movl	flag_DEFAULTSTORE(,%rax,4), %ebp
	cmpl	%ebp, -16(%rbx)
	jge	.LBB13_3
# BB#5:                                 #   in Loop: Header=BB13_1 Depth=1
	cmpl	%ebp, -12(%rbx)
	jle	.LBB13_6
# BB#7:                                 # %flag_SetFlagToDefault.exit
                                        #   in Loop: Header=BB13_1 Depth=1
	movl	%ebp, (%rdi,%rax,4)
.LBB13_8:                               #   in Loop: Header=BB13_1 Depth=1
	incq	%rax
	addq	$24, %rbx
	cmpq	$96, %rax
	jb	.LBB13_1
# BB#9:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB13_3:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.102, %edi
	jmp	.LBB13_4
.LBB13_6:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	(%rbx), %rdx
	movl	$.L.str.103, %edi
.LBB13_4:
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end13:
	.size	flag_SetReductionsToDefaults, .Lfunc_end13-flag_SetReductionsToDefaults
	.cfi_endproc

	.globl	flag_InitFlotterSubproofFlags
	.p2align	4, 0x90
	.type	flag_InitFlotterSubproofFlags,@function
flag_InitFlotterSubproofFlags:          # @flag_InitFlotterSubproofFlags
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi34:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi35:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi36:
	.cfi_def_cfa_offset 32
.Lcfi37:
	.cfi_offset %rbx, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	movq	%rbx, %rdi
	callq	flag_ClearPrinting
	movq	%rbx, %rdi
	callq	flag_ClearInferenceRules
	movq	%rbx, %rdi
	callq	flag_SetReductionsToDefaults
	movl	flag_DEFAULTSTORE+232(%rip), %ebp
	cmpl	%ebp, flag_PROPERTIES+1392(%rip)
	jge	.LBB14_19
# BB#1:
	cmpl	%ebp, flag_PROPERTIES+1396(%rip)
	jle	.LBB14_20
# BB#2:                                 # %flag_SetFlagToDefault.exit
	movl	%ebp, 232(%rbx)
	movl	flag_DEFAULTSTORE+156(%rip), %ebp
	cmpl	%ebp, flag_PROPERTIES+936(%rip)
	jge	.LBB14_21
# BB#3:
	cmpl	%ebp, flag_PROPERTIES+940(%rip)
	jle	.LBB14_22
# BB#4:                                 # %flag_SetFlagToDefault.exit17
	movl	%ebp, 156(%rbx)
	movl	36(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+216(%rip)
	jge	.LBB14_23
# BB#5:
	cmpl	%ebp, flag_PROPERTIES+220(%rip)
	jle	.LBB14_24
# BB#6:                                 # %flag_TransferFlag.exit
	movl	%ebp, 36(%rbx)
	movl	220(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+1320(%rip)
	jge	.LBB14_25
# BB#7:
	cmpl	%ebp, flag_PROPERTIES+1324(%rip)
	jle	.LBB14_26
# BB#8:                                 # %flag_TransferFlag.exit18
	movl	%ebp, 220(%rbx)
	cmpl	$0, flag_PROPERTIES+1848(%rip)
	jg	.LBB14_27
# BB#9:
	cmpl	$1, flag_PROPERTIES+1852(%rip)
	jle	.LBB14_28
# BB#10:                                # %flag_SetFlagValue.exit
	movl	$1, 308(%rbx)
	cmpl	$0, flag_PROPERTIES+1248(%rip)
	jns	.LBB14_30
# BB#11:
	cmpl	$0, flag_PROPERTIES+1252(%rip)
	jle	.LBB14_31
# BB#12:                                # %flag_SetFlagValue.exit19
	movl	$0, 208(%rbx)
	movl	180(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+1080(%rip)
	jge	.LBB14_34
# BB#13:
	cmpl	%ebp, flag_PROPERTIES+1084(%rip)
	jle	.LBB14_35
# BB#14:                                # %flag_TransferFlag.exit20
	movl	%ebp, 180(%rbx)
	movl	184(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+1104(%rip)
	jge	.LBB14_36
# BB#15:
	cmpl	%ebp, flag_PROPERTIES+1108(%rip)
	jle	.LBB14_37
# BB#16:                                # %flag_TransferFlag.exit21
	movl	%ebp, 184(%rbx)
	movl	152(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+912(%rip)
	jge	.LBB14_38
# BB#17:
	cmpl	%ebp, flag_PROPERTIES+916(%rip)
	jle	.LBB14_41
# BB#18:                                # %flag_TransferFlag.exit22
	movl	%ebp, 152(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB14_19:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1408(%rip), %rdx
	jmp	.LBB14_39
.LBB14_20:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1408(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB14_40
.LBB14_21:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+952(%rip), %rdx
	jmp	.LBB14_39
.LBB14_22:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+952(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB14_40
.LBB14_23:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+232(%rip), %rdx
	jmp	.LBB14_39
.LBB14_24:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+232(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB14_40
.LBB14_25:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1336(%rip), %rdx
	jmp	.LBB14_39
.LBB14_26:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1336(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB14_40
.LBB14_27:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1864(%rip), %rdx
	movl	$.L.str.102, %edi
	jmp	.LBB14_29
.LBB14_28:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1864(%rip), %rdx
	movl	$.L.str.103, %edi
.LBB14_29:
	movl	$1, %esi
	jmp	.LBB14_33
.LBB14_30:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1264(%rip), %rdx
	movl	$.L.str.102, %edi
	jmp	.LBB14_32
.LBB14_31:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1264(%rip), %rdx
	movl	$.L.str.103, %edi
.LBB14_32:
	xorl	%esi, %esi
.LBB14_33:
	xorl	%eax, %eax
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_34:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1096(%rip), %rdx
	jmp	.LBB14_39
.LBB14_35:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1096(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB14_40
.LBB14_36:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1120(%rip), %rdx
	jmp	.LBB14_39
.LBB14_37:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1120(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB14_40
.LBB14_38:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+928(%rip), %rdx
.LBB14_39:
	movl	$.L.str.102, %edi
.LBB14_40:
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB14_41:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+928(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB14_40
.Lfunc_end14:
	.size	flag_InitFlotterSubproofFlags, .Lfunc_end14-flag_InitFlotterSubproofFlags
	.cfi_endproc

	.globl	flag_InitFlotterFlags
	.p2align	4, 0x90
	.type	flag_InitFlotterFlags,@function
flag_InitFlotterFlags:                  # @flag_InitFlotterFlags
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi42:
	.cfi_def_cfa_offset 32
.Lcfi43:
	.cfi_offset %rbx, -32
.Lcfi44:
	.cfi_offset %r14, -24
.Lcfi45:
	.cfi_offset %rbp, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	callq	flag_InitFlotterSubproofFlags
	movl	flag_DEFAULTSTORE+208(%rip), %ebp
	cmpl	%ebp, flag_PROPERTIES+1248(%rip)
	jge	.LBB15_1
# BB#4:
	cmpl	%ebp, flag_PROPERTIES+1252(%rip)
	jle	.LBB15_5
# BB#7:                                 # %flag_SetFlagToDefault.exit
	movl	%ebp, 208(%rbx)
	movl	flag_DEFAULTSTORE+180(%rip), %ebp
	cmpl	%ebp, flag_PROPERTIES+1080(%rip)
	jge	.LBB15_8
# BB#9:
	cmpl	%ebp, flag_PROPERTIES+1084(%rip)
	jle	.LBB15_10
# BB#11:                                # %flag_SetFlagToDefault.exit25
	movl	%ebp, 180(%rbx)
	movl	flag_DEFAULTSTORE+184(%rip), %ebp
	cmpl	%ebp, flag_PROPERTIES+1104(%rip)
	jge	.LBB15_12
# BB#13:
	cmpl	%ebp, flag_PROPERTIES+1108(%rip)
	jle	.LBB15_14
# BB#15:                                # %flag_SetFlagToDefault.exit26
	movl	%ebp, 184(%rbx)
	movl	224(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+1344(%rip)
	jge	.LBB15_16
# BB#17:
	cmpl	%ebp, flag_PROPERTIES+1348(%rip)
	jle	.LBB15_18
# BB#19:                                # %flag_TransferFlag.exit
	movl	%ebp, 224(%rbx)
	movl	212(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+1272(%rip)
	jge	.LBB15_20
# BB#21:
	cmpl	%ebp, flag_PROPERTIES+1276(%rip)
	jle	.LBB15_22
# BB#23:                                # %flag_TransferFlag.exit27
	movl	%ebp, 212(%rbx)
	movl	216(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+1296(%rip)
	jge	.LBB15_24
# BB#25:
	cmpl	%ebp, flag_PROPERTIES+1300(%rip)
	jle	.LBB15_26
# BB#27:                                # %flag_TransferFlag.exit28
	movl	%ebp, 216(%rbx)
	movl	148(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+888(%rip)
	jge	.LBB15_28
# BB#29:
	cmpl	%ebp, flag_PROPERTIES+892(%rip)
	jle	.LBB15_30
# BB#31:                                # %flag_TransferFlag.exit29
	movl	%ebp, 148(%rbx)
	movl	140(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+840(%rip)
	jge	.LBB15_32
# BB#33:
	cmpl	%ebp, flag_PROPERTIES+844(%rip)
	jle	.LBB15_34
# BB#35:                                # %flag_TransferFlag.exit30
	movl	%ebp, 140(%rbx)
	movl	144(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+864(%rip)
	jge	.LBB15_36
# BB#37:
	cmpl	%ebp, flag_PROPERTIES+868(%rip)
	jle	.LBB15_38
# BB#39:                                # %flag_TransferFlag.exit31
	movl	%ebp, 144(%rbx)
	movl	228(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+1368(%rip)
	jge	.LBB15_40
# BB#41:
	cmpl	%ebp, flag_PROPERTIES+1372(%rip)
	jle	.LBB15_42
# BB#43:                                # %flag_TransferFlag.exit32
	movl	%ebp, 228(%rbx)
	movl	132(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+792(%rip)
	jge	.LBB15_44
# BB#45:
	cmpl	%ebp, flag_PROPERTIES+796(%rip)
	jle	.LBB15_46
# BB#47:                                # %flag_TransferFlag.exit33
	movl	%ebp, 132(%rbx)
	movl	136(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+816(%rip)
	jge	.LBB15_48
# BB#49:
	cmpl	%ebp, flag_PROPERTIES+820(%rip)
	jle	.LBB15_50
# BB#51:                                # %flag_TransferFlag.exit34
	movl	%ebp, 136(%rbx)
	movl	8(%r14), %ebp
	cmpl	%ebp, flag_PROPERTIES+48(%rip)
	jge	.LBB15_52
# BB#53:
	cmpl	%ebp, flag_PROPERTIES+52(%rip)
	jle	.LBB15_54
# BB#55:                                # %flag_TransferFlag.exit35
	movl	%ebp, 8(%rbx)
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.LBB15_1:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1264(%rip), %rdx
	jmp	.LBB15_2
.LBB15_5:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1264(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_8:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1096(%rip), %rdx
	jmp	.LBB15_2
.LBB15_10:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1096(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_12:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1120(%rip), %rdx
	jmp	.LBB15_2
.LBB15_14:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1120(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_16:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1360(%rip), %rdx
	jmp	.LBB15_2
.LBB15_18:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1360(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_20:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1288(%rip), %rdx
	jmp	.LBB15_2
.LBB15_22:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1288(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_24:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1312(%rip), %rdx
	jmp	.LBB15_2
.LBB15_26:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1312(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_28:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+904(%rip), %rdx
	jmp	.LBB15_2
.LBB15_30:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+904(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_32:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+856(%rip), %rdx
	jmp	.LBB15_2
.LBB15_34:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+856(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_36:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+880(%rip), %rdx
	jmp	.LBB15_2
.LBB15_38:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+880(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_40:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1384(%rip), %rdx
	jmp	.LBB15_2
.LBB15_42:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+1384(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_44:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+808(%rip), %rdx
	jmp	.LBB15_2
.LBB15_46:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+808(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_48:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+832(%rip), %rdx
	jmp	.LBB15_2
.LBB15_50:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+832(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.LBB15_52:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+64(%rip), %rdx
.LBB15_2:
	movl	$.L.str.102, %edi
.LBB15_3:
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	misc_UserErrorReport
	callq	misc_Error
.LBB15_54:
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	flag_PROPERTIES+64(%rip), %rdx
	movl	$.L.str.103, %edi
	jmp	.LBB15_3
.Lfunc_end15:
	.size	flag_InitFlotterFlags, .Lfunc_end15-flag_InitFlotterFlags
	.cfi_endproc

	.globl	flag_CheckStore
	.p2align	4, 0x90
	.type	flag_CheckStore,@function
flag_CheckStore:                        # @flag_CheckStore
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi46:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi48:
	.cfi_def_cfa_offset 32
.Lcfi49:
	.cfi_offset %rbx, -24
.Lcfi50:
	.cfi_offset %rbp, -16
	movl	$1, %ebx
	movl	$flag_PROPERTIES+28, %eax
	.p2align	4, 0x90
.LBB16_1:                               # =>This Inner Loop Header: Depth=1
	movl	-4(%rdi,%rbx,4), %ebp
	cmpl	$-5, %ebp
	je	.LBB16_11
# BB#2:                                 #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ebp, -28(%rax)
	jge	.LBB16_3
# BB#6:                                 #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ebp, -24(%rax)
	jle	.LBB16_7
.LBB16_11:                              # %flag_CheckFlagValueInRange.exit
                                        #   in Loop: Header=BB16_1 Depth=1
	movl	(%rdi,%rbx,4), %ebp
	cmpl	$-5, %ebp
	je	.LBB16_12
# BB#9:                                 #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ebp, -4(%rax)
	jge	.LBB16_4
# BB#10:                                #   in Loop: Header=BB16_1 Depth=1
	cmpl	%ebp, (%rax)
	jle	.LBB16_8
.LBB16_12:                              # %flag_CheckFlagValueInRange.exit.1
                                        #   in Loop: Header=BB16_1 Depth=1
	addq	$48, %rax
	leaq	2(%rbx), %rcx
	incq	%rbx
	cmpq	$96, %rbx
	movq	%rcx, %rbx
	jb	.LBB16_1
# BB#13:
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.LBB16_3:                               # %split
	decq	%rbx
.LBB16_4:
	movq	stdout(%rip), %rdi
	callq	fflush
	leaq	(%rbx,%rbx,2), %rax
	movq	flag_PROPERTIES+16(,%rax,8), %rdx
	movl	$.L.str.102, %edi
	jmp	.LBB16_5
.LBB16_7:                               # %split31
	decq	%rbx
.LBB16_8:
	movq	stdout(%rip), %rdi
	callq	fflush
	leaq	(%rbx,%rbx,2), %rax
	movq	flag_PROPERTIES+16(,%rax,8), %rdx
	movl	$.L.str.103, %edi
.LBB16_5:
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	misc_UserErrorReport
	callq	misc_Error
.Lfunc_end16:
	.size	flag_CheckStore, .Lfunc_end16-flag_CheckStore
	.cfi_endproc

	.p2align	4, 0x90
	.type	misc_Error,@function
misc_Error:                             # @misc_Error
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi51:
	.cfi_def_cfa_offset 16
	movq	stderr(%rip), %rdi
	callq	fflush
	movq	stdout(%rip), %rdi
	callq	fflush
	movq	stderr(%rip), %rdi
	callq	fflush
	movl	$1, %edi
	callq	exit
.Lfunc_end17:
	.size	misc_Error, .Lfunc_end17-misc_Error
	.cfi_endproc

	.type	flag_CLEAN,@object      # @flag_CLEAN
	.section	.rodata,"a",@progbits
	.globl	flag_CLEAN
	.p2align	2
flag_CLEAN:
	.long	4294967291              # 0xfffffffb
	.size	flag_CLEAN, 4

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Auto"
	.size	.L.str, 5

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"SOS"
	.size	.L.str.1, 4

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"Stdin"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"Interactive"
	.size	.L.str.3, 12

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"Flotter"
	.size	.L.str.4, 8

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"Loops"
	.size	.L.str.5, 6

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"Splits"
	.size	.L.str.6, 7

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"Sorts"
	.size	.L.str.7, 6

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"PSub"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"Memory"
	.size	.L.str.9, 7

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"DocSST"
	.size	.L.str.10, 7

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"PRew"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"PCRw"
	.size	.L.str.12, 5

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"PCon"
	.size	.L.str.13, 5

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"PAED"
	.size	.L.str.14, 5

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"PTaut"
	.size	.L.str.15, 6

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"PObv"
	.size	.L.str.16, 5

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"PSSi"
	.size	.L.str.17, 5

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"PSST"
	.size	.L.str.18, 5

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"DocProof"
	.size	.L.str.19, 9

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"PMRR"
	.size	.L.str.20, 5

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"PUnC"
	.size	.L.str.21, 5

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"PDer"
	.size	.L.str.22, 5

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"PGiven"
	.size	.L.str.23, 7

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"PLabels"
	.size	.L.str.24, 8

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"PKept"
	.size	.L.str.25, 6

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"DocSplit"
	.size	.L.str.26, 9

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"PProblem"
	.size	.L.str.27, 9

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"PEmptyClause"
	.size	.L.str.28, 13

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"PStatistic"
	.size	.L.str.29, 11

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"FPModel"
	.size	.L.str.30, 8

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"FPDFGProof"
	.size	.L.str.31, 11

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"PFlags"
	.size	.L.str.32, 7

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"POptSkolem"
	.size	.L.str.33, 11

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"PStrSkolem"
	.size	.L.str.34, 11

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"PBDC"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"PBInc"
	.size	.L.str.36, 6

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"PApplyDefs"
	.size	.L.str.37, 11

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"TimeLimit"
	.size	.L.str.38, 10

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"Select"
	.size	.L.str.39, 7

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"IEmS"
	.size	.L.str.40, 5

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"ISoR"
	.size	.L.str.41, 5

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"IEqR"
	.size	.L.str.42, 5

	.type	.L.str.43,@object       # @.str.43
.L.str.43:
	.asciz	"IERR"
	.size	.L.str.43, 5

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"IEqF"
	.size	.L.str.44, 5

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"IMPm"
	.size	.L.str.45, 5

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"ISpR"
	.size	.L.str.46, 5

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"IOPm"
	.size	.L.str.47, 5

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"ISPm"
	.size	.L.str.48, 5

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"ISpL"
	.size	.L.str.49, 5

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"IORe"
	.size	.L.str.50, 5

	.type	.L.str.51,@object       # @.str.51
.L.str.51:
	.asciz	"ISRe"
	.size	.L.str.51, 5

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"ISHy"
	.size	.L.str.52, 5

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"IOHy"
	.size	.L.str.53, 5

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"IURR"
	.size	.L.str.54, 5

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"IOFc"
	.size	.L.str.55, 5

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"ISFc"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"IBUR"
	.size	.L.str.57, 5

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"IDEF"
	.size	.L.str.58, 5

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"IUnR"
	.size	.L.str.59, 5

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"RFRew"
	.size	.L.str.60, 6

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"RBRew"
	.size	.L.str.61, 6

	.type	.L.str.62,@object       # @.str.62
.L.str.62:
	.asciz	"RFCRw"
	.size	.L.str.62, 6

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"RBCRw"
	.size	.L.str.63, 6

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"RUnC"
	.size	.L.str.64, 5

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"RTer"
	.size	.L.str.65, 5

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"RFSub"
	.size	.L.str.66, 6

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"RBSub"
	.size	.L.str.67, 6

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"RFMRR"
	.size	.L.str.68, 6

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"RBMRR"
	.size	.L.str.69, 6

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	"RObv"
	.size	.L.str.70, 5

	.type	.L.str.71,@object       # @.str.71
.L.str.71:
	.asciz	"RTaut"
	.size	.L.str.71, 6

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"RSSi"
	.size	.L.str.72, 5

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"RSST"
	.size	.L.str.73, 5

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"RAED"
	.size	.L.str.74, 5

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"RCon"
	.size	.L.str.75, 5

	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"RInput"
	.size	.L.str.76, 7

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"ApplyDefs"
	.size	.L.str.77, 10

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"FullRed"
	.size	.L.str.78, 8

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	"SatInput"
	.size	.L.str.79, 9

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	"WDRatio"
	.size	.L.str.80, 8

	.type	.L.str.81,@object       # @.str.81
.L.str.81:
	.asciz	"PrefCon"
	.size	.L.str.81, 8

	.type	.L.str.82,@object       # @.str.82
.L.str.82:
	.asciz	"FuncWeight"
	.size	.L.str.82, 11

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"VarWeight"
	.size	.L.str.83, 10

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"PrefVar"
	.size	.L.str.84, 8

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"BoundMode"
	.size	.L.str.85, 10

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"BoundStart"
	.size	.L.str.86, 11

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"BoundLoops"
	.size	.L.str.87, 11

	.type	.L.str.88,@object       # @.str.88
.L.str.88:
	.asciz	"Ordering"
	.size	.L.str.88, 9

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"CNFOptSkolem"
	.size	.L.str.89, 13

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"CNFProofSteps"
	.size	.L.str.90, 14

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"CNFRenaming"
	.size	.L.str.91, 12

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"CNFPRenaming"
	.size	.L.str.92, 13

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"CNFStrSkolem"
	.size	.L.str.93, 13

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"CNFFEqR"
	.size	.L.str.94, 8

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"TDfg2OtterOptions"
	.size	.L.str.95, 18

	.type	flag_DEFAULTSTORE,@object # @flag_DEFAULTSTORE
	.local	flag_DEFAULTSTORE
	.comm	flag_DEFAULTSTORE,384,16
	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"list_of_settings(SPASS).{*"
	.size	.L.str.96, 27

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"set_flag(%s,%d)."
	.size	.L.str.97, 17

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"\n %-30s"
	.size	.L.str.98, 8

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	" %-30s"
	.size	.L.str.99, 7

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	" set_flag(%s,%d)."
	.size	.L.str.100, 18

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"*}\nend_of_list.\n"
	.size	.L.str.101, 17

	.type	flag_PROPERTIES,@object # @flag_PROPERTIES
	.local	flag_PROPERTIES
	.comm	flag_PROPERTIES,2304,16
	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"\n Error: Flag value %d is too small for flag %s.\n"
	.size	.L.str.102, 50

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"\n Error: Flag value %d is too large for flag %s.\n"
	.size	.L.str.103, 50


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
