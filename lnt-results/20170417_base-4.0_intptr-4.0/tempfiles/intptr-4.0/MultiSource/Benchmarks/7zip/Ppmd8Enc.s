	.text
	.file	"Ppmd8Enc.bc"
	.globl	Ppmd8_RangeEnc_FlushData
	.p2align	4, 0x90
	.type	Ppmd8_RangeEnc_FlushData,@function
Ppmd8_RangeEnc_FlushData:               # @Ppmd8_RangeEnc_FlushData
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movq	%rdi, %rbx
	movq	120(%rbx), %rdi
	movzbl	115(%rbx), %esi
	callq	*(%rdi)
	movl	112(%rbx), %eax
	movl	%eax, %ecx
	shll	$8, %ecx
	movl	%ecx, 112(%rbx)
	movq	120(%rbx), %rdi
	shrl	$16, %eax
	movzbl	%al, %esi
	callq	*(%rdi)
	movl	112(%rbx), %eax
	movl	%eax, %ecx
	shll	$8, %ecx
	movl	%ecx, 112(%rbx)
	movq	120(%rbx), %rdi
	shrl	$16, %eax
	movzbl	%al, %esi
	callq	*(%rdi)
	movl	112(%rbx), %eax
	movl	%eax, %ecx
	shll	$8, %ecx
	movl	%ecx, 112(%rbx)
	movq	120(%rbx), %rdi
	shrl	$16, %eax
	movzbl	%al, %esi
	callq	*(%rdi)
	shll	$8, 112(%rbx)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	Ppmd8_RangeEnc_FlushData, .Lfunc_end0-Ppmd8_RangeEnc_FlushData
	.cfi_endproc

	.globl	Ppmd8_EncodeSymbol
	.p2align	4, 0x90
	.type	Ppmd8_EncodeSymbol,@function
Ppmd8_EncodeSymbol:                     # @Ppmd8_EncodeSymbol
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi2:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi3:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi4:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi5:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi6:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 56
	subq	$296, %rsp              # imm = 0x128
.Lcfi8:
	.cfi_def_cfa_offset 352
.Lcfi9:
	.cfi_offset %rbx, -56
.Lcfi10:
	.cfi_offset %r12, -48
.Lcfi11:
	.cfi_offset %r13, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movl	%esi, %r9d
	movq	%rdi, %rbx
	movq	(%rbx), %r8
	movzbl	(%r8), %ecx
	testl	%ecx, %ecx
	je	.LBB1_27
# BB#1:
	movq	56(%rbx), %rbp
	movl	4(%r8), %edi
	movzbl	(%rbp,%rdi), %edx
	cmpl	%r9d, %edx
	jne	.LBB1_7
# BB#2:                                 # %.critedge
	addq	%rdi, %rbp
	movzbl	1(%rbp), %ecx
	movzwl	2(%r8), %edi
	movl	104(%rbx), %eax
	movl	112(%rbx), %esi
	xorl	%edx, %edx
	divl	%edi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	imull	%ecx, %eax
	movl	%eax, 104(%rbx)
	jmp	.LBB1_3
	.p2align	4, 0x90
.LBB1_6:                                # %.critedge.i.i
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	120(%rbx), %rdi
	shrl	$24, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*(%rdi)
	movl	104(%rbx), %eax
	shll	$8, %eax
	movl	%eax, 104(%rbx)
	movl	112(%rbx), %esi
	shll	$8, %esi
	movl	%esi, 112(%rbx)
.LBB1_3:                                # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rax), %ecx
	xorl	%esi, %ecx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB1_6
# BB#4:                                 #   in Loop: Header=BB1_3 Depth=1
	cmpl	$32767, %eax            # imm = 0x7FFF
	ja	.LBB1_71
# BB#5:                                 #   in Loop: Header=BB1_3 Depth=1
	movl	%esi, %eax
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, 104(%rbx)
	jmp	.LBB1_6
.LBB1_27:
	leaq	2(%r8), %r12
	movzbl	3(%r8), %eax
	movzbl	855(%rbx,%rax), %eax
	movq	56(%rbx), %rcx
	movl	8(%r8), %edx
	movzbl	(%rcx,%rdx), %ecx
	movzbl	600(%rbx,%rcx), %ecx
	addl	32(%rbx), %ecx
	movzbl	1(%r8), %edx
	addl	%ecx, %edx
	movl	40(%rbx), %ecx
	shrl	$26, %ecx
	andl	$32, %ecx
	addl	%edx, %ecx
	shlq	$7, %rax
	addq	%rbx, %rax
	leaq	4192(%rax,%rcx,2), %r14
	movzbl	2(%r8), %edx
	movzwl	4192(%rax,%rcx,2), %ecx
	leaq	104(%rbx), %r15
	movl	104(%rbx), %eax
	shrl	$14, %eax
	movl	%eax, %esi
	imull	%ecx, %esi
	cmpl	%r9d, %edx
	jne	.LBB1_34
# BB#28:                                # %.critedge180
	movl	%esi, 104(%rbx)
	movl	112(%rbx), %eax
	jmp	.LBB1_29
	.p2align	4, 0x90
.LBB1_32:                               # %.critedge.i.i185
                                        #   in Loop: Header=BB1_29 Depth=1
	movq	120(%rbx), %rdi
	shrl	$24, %eax
	movl	%eax, %esi
	callq	*(%rdi)
	movl	104(%rbx), %esi
	shll	$8, %esi
	movl	%esi, 104(%rbx)
	movl	112(%rbx), %eax
	shll	$8, %eax
	movl	%eax, 112(%rbx)
.LBB1_29:                               # =>This Inner Loop Header: Depth=1
	leal	(%rax,%rsi), %ecx
	xorl	%eax, %ecx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB1_32
# BB#30:                                #   in Loop: Header=BB1_29 Depth=1
	cmpl	$32767, %esi            # imm = 0x7FFF
	ja	.LBB1_33
# BB#31:                                #   in Loop: Header=BB1_29 Depth=1
	movl	%eax, %ecx
	negl	%ecx
	andl	$32767, %ecx            # imm = 0x7FFF
	movl	%ecx, (%r15)
	jmp	.LBB1_32
.LBB1_7:
	movl	$0, 32(%rbx)
	movzbl	1(%rbp,%rdi), %esi
	movl	%ecx, %edx
	negl	%edx
	leaq	6(%rbp,%rdi), %rbp
	.p2align	4, 0x90
.LBB1_8:                                # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	movzbl	1(%rbp), %edi
	cmpl	%r9d, %eax
	je	.LBB1_9
# BB#15:                                #   in Loop: Header=BB1_8 Depth=1
	addl	%edi, %esi
	addq	$6, %rbp
	incl	%edx
	jne	.LBB1_8
# BB#16:                                # %.preheader196
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	movzbl	-6(%rbp), %eax
	addq	$-6, %rbp
	movb	$0, 32(%rsp,%rax)
	leal	-1(%rcx), %edx
	movl	%ecx, %edi
	andl	$3, %edi
	je	.LBB1_19
# BB#17:                                # %.prol.preheader
	negl	%edi
	.p2align	4, 0x90
.LBB1_18:                               # =>This Inner Loop Header: Depth=1
	movzbl	-6(%rbp), %eax
	addq	$-6, %rbp
	movb	$0, 32(%rsp,%rax)
	decl	%ecx
	incl	%edi
	jne	.LBB1_18
.LBB1_19:                               # %.prol.loopexit
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	cmpl	$3, %edx
	jb	.LBB1_22
# BB#20:                                # %.preheader196.new
	addq	$-6, %rbp
	.p2align	4, 0x90
.LBB1_21:                               # =>This Inner Loop Header: Depth=1
	movzbl	(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	movzbl	-6(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	movzbl	-12(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	movzbl	-18(%rbp), %eax
	movb	$0, 32(%rsp,%rax)
	addq	$-24, %rbp
	addl	$-4, %ecx
	jne	.LBB1_21
.LBB1_22:
	movzwl	2(%r8), %ecx
	movl	104(%rbx), %eax
	xorl	%edx, %edx
	divl	%ecx
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	subl	%esi, %ecx
	leaq	104(%rbx), %r15
	imull	%eax, %esi
	leaq	112(%rbx), %r13
	addl	112(%rbx), %esi
	movl	%esi, 112(%rbx)
	imull	%eax, %ecx
	movl	%ecx, 104(%rbx)
	leaq	120(%rbx), %rbp
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_26:                               # %.critedge.i.i183
                                        #   in Loop: Header=BB1_23 Depth=1
	movq	(%rbp), %rdi
	shrl	$24, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*(%rdi)
	movl	(%r15), %ecx
	shll	$8, %ecx
	movl	%ecx, (%r15)
	movl	(%r13), %esi
	shll	$8, %esi
	movl	%esi, (%r13)
.LBB1_23:                               # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rcx), %eax
	xorl	%esi, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB1_26
# BB#24:                                #   in Loop: Header=BB1_23 Depth=1
	cmpl	$32767, %ecx            # imm = 0x7FFF
	ja	.LBB1_40
# BB#25:                                #   in Loop: Header=BB1_23 Depth=1
	movl	%esi, %eax
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, (%r15)
	jmp	.LBB1_26
.LBB1_71:                               # %RangeEnc_Encode.exit
	movq	%rbp, 16(%rbx)
	movq	%rbx, %rdi
	callq	Ppmd8_Update1_0
	jmp	.LBB1_72
.LBB1_34:
	movl	%r9d, 12(%rsp)          # 4-byte Spill
	leaq	112(%rbx), %r13
	addl	112(%rbx), %esi
	movl	%esi, 112(%rbx)
	movl	$16384, %edx            # imm = 0x4000
	subl	%ecx, %edx
	imull	%edx, %eax
	movl	%eax, 104(%rbx)
	leaq	120(%rbx), %rbp
	jmp	.LBB1_35
	.p2align	4, 0x90
.LBB1_38:                               # %.critedge.i.i186
                                        #   in Loop: Header=BB1_35 Depth=1
	movq	(%rbp), %rdi
	shrl	$24, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*(%rdi)
	movl	(%r15), %eax
	shll	$8, %eax
	movl	%eax, (%r15)
	movl	(%r13), %esi
	shll	$8, %esi
	movl	%esi, (%r13)
.LBB1_35:                               # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rax), %ecx
	xorl	%esi, %ecx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB1_38
# BB#36:                                #   in Loop: Header=BB1_35 Depth=1
	cmpl	$32767, %eax            # imm = 0x7FFF
	ja	.LBB1_39
# BB#37:                                #   in Loop: Header=BB1_35 Depth=1
	movl	%esi, %eax
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, (%r15)
	jmp	.LBB1_38
.LBB1_33:                               # %RangeEnc_EncodeBit_0.exit
	movzwl	(%r14), %eax
	leal	32(%rax), %ecx
	shrl	$7, %ecx
	negl	%ecx
	leal	128(%rax,%rcx), %eax
	movw	%ax, (%r14)
	movq	%r12, 16(%rbx)
	movq	%rbx, %rdi
	callq	Ppmd8_UpdateBin
	jmp	.LBB1_72
.LBB1_39:                               # %RangeEnc_EncodeBit_1.exit
	movzwl	(%r14), %eax
	leal	32(%rax), %ecx
	shrl	$7, %ecx
	subl	%ecx, %eax
	movw	%ax, (%r14)
	shrl	$10, %eax
	andl	$63, %eax
	movzbl	PPMD8_kExpEscape(%rax), %eax
	movl	%eax, 28(%rbx)
	pcmpeqd	%xmm0, %xmm0
	movdqa	%xmm0, 272(%rsp)
	movdqa	%xmm0, 256(%rsp)
	movdqa	%xmm0, 240(%rsp)
	movdqa	%xmm0, 224(%rsp)
	movdqa	%xmm0, 208(%rsp)
	movdqa	%xmm0, 192(%rsp)
	movdqa	%xmm0, 176(%rsp)
	movdqa	%xmm0, 160(%rsp)
	movdqa	%xmm0, 144(%rsp)
	movdqa	%xmm0, 128(%rsp)
	movdqa	%xmm0, 112(%rsp)
	movdqa	%xmm0, 96(%rsp)
	movdqa	%xmm0, 80(%rsp)
	movdqa	%xmm0, 64(%rsp)
	movdqa	%xmm0, 48(%rsp)
	movdqa	%xmm0, 32(%rsp)
	movzbl	(%r12), %eax
	movb	$0, 32(%rsp,%rax)
	movl	$0, 32(%rbx)
.LBB1_40:                               # %RangeEnc_Encode.exit184.preheader
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	jmp	.LBB1_41
.LBB1_9:                                # %.critedge179
	movzwl	2(%r8), %ecx
	movl	104(%rbx), %eax
	xorl	%edx, %edx
	divl	%ecx
	imull	%eax, %esi
	addl	112(%rbx), %esi
	movl	%esi, 112(%rbx)
	imull	%eax, %edi
	movl	%edi, 104(%rbx)
	jmp	.LBB1_10
	.p2align	4, 0x90
.LBB1_13:                               # %.critedge.i.i181
                                        #   in Loop: Header=BB1_10 Depth=1
	movq	120(%rbx), %rdi
	shrl	$24, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*(%rdi)
	movl	104(%rbx), %edi
	shll	$8, %edi
	movl	%edi, 104(%rbx)
	movl	112(%rbx), %esi
	shll	$8, %esi
	movl	%esi, 112(%rbx)
.LBB1_10:                               # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rdi), %eax
	xorl	%esi, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB1_13
# BB#11:                                #   in Loop: Header=BB1_10 Depth=1
	cmpl	$32767, %edi            # imm = 0x7FFF
	ja	.LBB1_14
# BB#12:                                #   in Loop: Header=BB1_10 Depth=1
	movl	%esi, %eax
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, 104(%rbx)
	jmp	.LBB1_13
.LBB1_14:                               # %RangeEnc_Encode.exit182
	movq	%rbp, 16(%rbx)
	movq	%rbx, %rdi
	callq	Ppmd8_Update1
	jmp	.LBB1_72
.LBB1_64:                               #   in Loop: Header=BB1_41 Depth=1
	movl	8(%rsp), %ecx
	leal	(%rcx,%r12), %esi
	movl	(%r15), %eax
	xorl	%edx, %edx
	divl	%esi
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	movl	%eax, %esi
	imull	%r12d, %esi
	addl	(%r13), %esi
	movl	%esi, (%r13)
	imull	%ecx, %eax
	movl	%eax, (%r15)
	jmp	.LBB1_65
	.p2align	4, 0x90
.LBB1_68:                               # %.critedge.i.i189
                                        #   in Loop: Header=BB1_65 Depth=2
	movq	(%rbp), %rdi
	shrl	$24, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*(%rdi)
	movl	(%r15), %eax
	shll	$8, %eax
	movl	%eax, (%r15)
	movl	(%r13), %esi
	shll	$8, %esi
	movl	%esi, (%r13)
.LBB1_65:                               #   Parent Loop BB1_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	leal	(%rsi,%rax), %ecx
	xorl	%esi, %ecx
	cmpl	$16777216, %ecx         # imm = 0x1000000
	jb	.LBB1_68
# BB#66:                                #   in Loop: Header=BB1_65 Depth=2
	cmpl	$32767, %eax            # imm = 0x7FFF
	ja	.LBB1_70
# BB#67:                                #   in Loop: Header=BB1_65 Depth=2
	movl	%esi, %eax
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, (%r15)
	jmp	.LBB1_68
.LBB1_70:                               #   in Loop: Header=BB1_41 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movzwl	(%rcx), %eax
	addl	%r12d, %eax
	addl	8(%rsp), %eax
	movw	%ax, (%rcx)
.LBB1_41:                               # %RangeEnc_Encode.exit184
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_42 Depth 2
                                        #     Child Loop BB1_45 Depth 2
                                        #     Child Loop BB1_65 Depth 2
	leaq	8(%rsp), %rdx
	movq	(%rbx), %rcx
	movzbl	(%rcx), %esi
	movl	24(%rbx), %eax
	.p2align	4, 0x90
.LBB1_42:                               #   Parent Loop BB1_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	8(%rcx), %ebp
	incl	%eax
	testq	%rbp, %rbp
	je	.LBB1_69
# BB#43:                                #   in Loop: Header=BB1_42 Depth=2
	movq	56(%rbx), %rdi
	leaq	(%rdi,%rbp), %rcx
	movq	%rcx, (%rbx)
	cmpb	%sil, (%rdi,%rbp)
	je	.LBB1_42
# BB#44:                                #   in Loop: Header=BB1_41 Depth=1
	movl	%eax, 24(%rbx)
	movq	%rbx, %rdi
	callq	Ppmd8_MakeEscFreq
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movq	(%rbx), %rax
	movl	4(%rax), %r14d
	addq	56(%rbx), %r14
	movzbl	(%rax), %eax
	decl	%eax
	xorl	%esi, %esi
	movl	12(%rsp), %edi          # 4-byte Reload
	movq	24(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_45:                               #   Parent Loop BB1_41 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r14), %ecx
	movzbl	32(%rsp,%rcx), %edx
	andb	1(%r14), %dl
	movzbl	%dl, %r12d
	addl	%esi, %r12d
	cmpl	%edi, %ecx
	je	.LBB1_46
# BB#63:                                #   in Loop: Header=BB1_45 Depth=2
	movb	$0, 32(%rsp,%rcx)
	decl	%eax
	addq	$6, %r14
	cmpl	$-2, %eax
	movl	%r12d, %esi
	jne	.LBB1_45
	jmp	.LBB1_64
.LBB1_69:                               # %.thread.loopexit
	movl	%eax, 24(%rbx)
	jmp	.LBB1_72
.LBB1_46:                               # %.preheader.preheader
	cmpl	$-1, %eax
	je	.LBB1_47
# BB#48:                                # %._crit_edge.preheader
	leal	1(%rax), %r8d
	testb	$1, %r8b
	jne	.LBB1_50
# BB#49:
                                        # implicit-def: %EDI
	movq	%r14, %rcx
	testl	%eax, %eax
	jne	.LBB1_52
	jmp	.LBB1_54
.LBB1_47:
	movl	%r12d, %edi
	jmp	.LBB1_54
.LBB1_50:                               # %._crit_edge.prol
	leaq	6(%r14), %rcx
	movzbl	6(%r14), %edx
	movb	32(%rsp,%rdx), %dl
	andb	7(%r14), %dl
	movzbl	%dl, %edi
	addl	%r12d, %edi
	movl	%eax, %r8d
	movl	%edi, %r12d
	testl	%eax, %eax
	je	.LBB1_54
.LBB1_52:                               # %._crit_edge.preheader.new
	addq	$13, %rcx
	movl	%r12d, %edi
	.p2align	4, 0x90
.LBB1_53:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movzbl	-7(%rcx), %eax
	movzbl	32(%rsp,%rax), %eax
	andb	-6(%rcx), %al
	movzbl	%al, %eax
	addl	%edi, %eax
	movzbl	-1(%rcx), %edi
	movzbl	32(%rsp,%rdi), %edx
	andb	(%rcx), %dl
	movzbl	%dl, %edi
	addl	%eax, %edi
	addq	$12, %rcx
	addl	$-2, %r8d
	jne	.LBB1_53
.LBB1_54:                               # %.preheader._crit_edge
	movzbl	1(%r14), %ecx
	addl	8(%rsp), %edi
	movl	(%r15), %eax
	xorl	%edx, %edx
	divl	%edi
	imull	%eax, %esi
	addl	(%r13), %esi
	movl	%esi, (%r13)
	imull	%eax, %ecx
	movl	%ecx, (%r15)
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_58:                               # %.critedge.i.i187
                                        #   in Loop: Header=BB1_55 Depth=1
	movq	(%rbp), %rdi
	shrl	$24, %esi
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	callq	*(%rdi)
	movl	(%r15), %ecx
	shll	$8, %ecx
	movl	%ecx, (%r15)
	movl	(%r13), %esi
	shll	$8, %esi
	movl	%esi, (%r13)
.LBB1_55:                               # =>This Inner Loop Header: Depth=1
	leal	(%rsi,%rcx), %eax
	xorl	%esi, %eax
	cmpl	$16777216, %eax         # imm = 0x1000000
	jb	.LBB1_58
# BB#56:                                #   in Loop: Header=BB1_55 Depth=1
	cmpl	$32767, %ecx            # imm = 0x7FFF
	ja	.LBB1_59
# BB#57:                                #   in Loop: Header=BB1_55 Depth=1
	movl	%esi, %eax
	negl	%eax
	andl	$32767, %eax            # imm = 0x7FFF
	movl	%eax, (%r15)
	jmp	.LBB1_58
.LBB1_59:                               # %RangeEnc_Encode.exit188
	movq	16(%rsp), %rdx          # 8-byte Reload
	movzbl	2(%rdx), %ecx
	cmpl	$6, %ecx
	ja	.LBB1_62
# BB#60:
	decb	3(%rdx)
	jne	.LBB1_62
# BB#61:
	shlw	(%rdx)
	movl	%ecx, %eax
	incb	%al
	movb	%al, 2(%rdx)
	movl	$3, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 3(%rdx)
.LBB1_62:
	movq	%r14, 16(%rbx)
	movq	%rbx, %rdi
	callq	Ppmd8_Update2
.LBB1_72:
	addq	$296, %rsp              # imm = 0x128
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	Ppmd8_EncodeSymbol, .Lfunc_end1-Ppmd8_EncodeSymbol
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
