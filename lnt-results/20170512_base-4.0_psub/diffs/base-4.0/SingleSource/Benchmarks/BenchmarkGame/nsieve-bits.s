	.text
	.file	"nsieve-bits.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 48
.Lcfi5:
	.cfi_offset %rbx, -40
.Lcfi6:
	.cfi_offset %r14, -32
.Lcfi7:
	.cfi_offset %r15, -24
.Lcfi8:
	.cfi_offset %rbp, -16
	movl	$5120004, %edi          # imm = 0x4E2004
	callq	malloc
	movq	%rax, %r15
	testq	%r15, %r15
	je	.LBB0_1
# BB#2:                                 # %.lr.ph53.preheader
	movl	$255, %esi
	movl	$5120004, %edx          # imm = 0x4E2004
	movq	%r15, %rdi
	callq	memset
	movl	$2, %eax
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB0_3:                                # %.lr.ph53
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_5 Depth 2
	movl	%eax, %ecx
	shrl	$5, %ecx
	movl	(%r15,%rcx,4), %ecx
	btl	%eax, %ecx
	jae	.LBB0_8
# BB#4:                                 #   in Loop: Header=BB0_3 Depth=1
	incl	%edx
	leal	(%rax,%rax), %ecx
	cmpl	$40960000, %ecx         # imm = 0x2710000
	ja	.LBB0_8
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph
                                        #   Parent Loop BB0_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %esi
	shrl	$5, %esi
	movl	(%r15,%rsi,4), %edi
	movl	$1, %ebp
	shll	%cl, %ebp
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %ebx
	btl	%ebx, %edi
	jae	.LBB0_7
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	xorl	%ebp, %edi
	movl	%edi, (%r15,%rsi,4)
.LBB0_7:                                #   in Loop: Header=BB0_5 Depth=2
	addl	%eax, %ecx
	cmpl	$40960001, %ecx         # imm = 0x2710001
	jb	.LBB0_5
.LBB0_8:                                # %.loopexit
                                        #   in Loop: Header=BB0_3 Depth=1
	incl	%eax
	cmpl	$40960000, %eax         # imm = 0x2710000
	jbe	.LBB0_3
# BB#9:                                 # %.lr.ph53.preheader.1
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	movl	$40960000, %esi         # imm = 0x2710000
	xorl	%eax, %eax
	callq	printf
	movl	$255, %esi
	movl	$2560004, %edx          # imm = 0x271004
	movq	%r15, %rdi
	callq	memset
	movl	$2, %eax
	.p2align	4, 0x90
.LBB0_10:                               # %.lr.ph53.1
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_12 Depth 2
	movl	%eax, %ecx
	shrl	$5, %ecx
	movl	(%r15,%rcx,4), %ecx
	btl	%eax, %ecx
	jae	.LBB0_15
# BB#11:                                #   in Loop: Header=BB0_10 Depth=1
	incl	%ebp
	leal	(%rax,%rax), %ecx
	cmpl	$20480000, %ecx         # imm = 0x1388000
	ja	.LBB0_15
	.p2align	4, 0x90
.LBB0_12:                               # %.lr.ph.1
                                        #   Parent Loop BB0_10 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %edx
	shrl	$5, %edx
	movl	(%r15,%rdx,4), %esi
	movl	$1, %edi
	shll	%cl, %edi
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %ebx
	btl	%ebx, %esi
	jae	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_12 Depth=2
	xorl	%edi, %esi
	movl	%esi, (%r15,%rdx,4)
.LBB0_14:                               #   in Loop: Header=BB0_12 Depth=2
	addl	%eax, %ecx
	cmpl	$20480001, %ecx         # imm = 0x1388001
	jb	.LBB0_12
.LBB0_15:                               # %.loopexit.1
                                        #   in Loop: Header=BB0_10 Depth=1
	incl	%eax
	cmpl	$20480001, %eax         # imm = 0x1388001
	jb	.LBB0_10
# BB#16:                                # %.lr.ph53.preheader.2
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
	movl	$20480000, %esi         # imm = 0x1388000
	xorl	%eax, %eax
	movl	%ebp, %edx
	callq	printf
	movl	$255, %esi
	movl	$1280004, %edx          # imm = 0x138804
	movq	%r15, %rdi
	callq	memset
	movl	$2, %eax
	.p2align	4, 0x90
.LBB0_17:                               # %.lr.ph53.2
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_19 Depth 2
	movl	%eax, %ecx
	shrl	$5, %ecx
	movl	(%r15,%rcx,4), %ecx
	btl	%eax, %ecx
	jae	.LBB0_22
# BB#18:                                #   in Loop: Header=BB0_17 Depth=1
	incl	%r14d
	leal	(%rax,%rax), %ecx
	cmpl	$10240000, %ecx         # imm = 0x9C4000
	ja	.LBB0_22
	.p2align	4, 0x90
.LBB0_19:                               # %.lr.ph.2
                                        #   Parent Loop BB0_17 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	%ecx, %edx
	shrl	$5, %edx
	movl	(%r15,%rdx,4), %esi
	movl	$1, %edi
	shll	%cl, %edi
	movl	%ecx, %ebx
	andb	$31, %bl
	movzbl	%bl, %ebp
	btl	%ebp, %esi
	jae	.LBB0_21
# BB#20:                                #   in Loop: Header=BB0_19 Depth=2
	xorl	%edi, %esi
	movl	%esi, (%r15,%rdx,4)
.LBB0_21:                               #   in Loop: Header=BB0_19 Depth=2
	addl	%eax, %ecx
	cmpl	$10240001, %ecx         # imm = 0x9C4001
	jb	.LBB0_19
.LBB0_22:                               # %.loopexit.2
                                        #   in Loop: Header=BB0_17 Depth=1
	incl	%eax
	cmpl	$10240001, %eax         # imm = 0x9C4001
	jb	.LBB0_17
# BB#23:                                # %._crit_edge.2
	xorl	%ebp, %ebp
	movl	$.L.str, %edi
	movl	$10240000, %esi         # imm = 0x9C4000
	xorl	%eax, %eax
	movl	%r14d, %edx
	callq	printf
	movq	%r15, %rdi
	callq	free
	jmp	.LBB0_24
.LBB0_1:
	movl	$1, %ebp
.LBB0_24:
	movl	%ebp, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Primes up to %8d %8d\n"
	.size	.L.str, 22


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
