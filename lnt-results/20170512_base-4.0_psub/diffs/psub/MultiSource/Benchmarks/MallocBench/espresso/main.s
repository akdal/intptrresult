	.text
	.file	"main.bc"
	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 144
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rsi, %r13
	movl	%edi, 44(%rsp)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	$.L.str.91, total_name+120(%rip)
	movl	$.L.str.92, %eax
	movd	%rax, %xmm0
	movl	$.L.str.90, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, total_name(%rip)
	movl	$.L.str.94, %eax
	movd	%rax, %xmm0
	movl	$.L.str.95, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+24(%rip)
	movl	$.L.str.93, %eax
	movd	%rax, %xmm0
	movl	$.L.str.96, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+40(%rip)
	movl	$.L.str.99, %eax
	movd	%rax, %xmm0
	movl	$.L.str.98, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+56(%rip)
	movl	$.L.str.102, %eax
	movd	%rax, %xmm0
	movl	$.L.str.101, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+104(%rip)
	movl	$.L.str.77, %eax
	movd	%rax, %xmm0
	movl	$.L.str.97, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+72(%rip)
	movl	$.L.str.100, %eax
	movd	%rax, %xmm0
	movl	$.L.str.103, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+88(%rip)
	movl	$0, 40(%rsp)
	movl	$1, 24(%rsp)
	movl	$0, debug(%rip)
	movl	$0, verbose_debug(%rip)
	movl	$1, print_solution(%rip)
	movl	$0, summary(%rip)
	movl	$0, trace(%rip)
	movl	$-1, 28(%rsp)
	movl	$-1, 16(%rsp)
	movl	$1, remove_essential(%rip)
	movl	$1, force_irredundant(%rip)
	movl	$1, unwrap_onset(%rip)
	movl	$0, single_expand(%rip)
	movl	$0, pos(%rip)
	movl	$0, recompute_onset(%rip)
	movl	$0, use_super_gasp(%rip)
	movl	$0, use_random_order(%rip)
	movl	$0, kiss(%rip)
	movl	$1, echo_comments(%rip)
	movl	$1, echo_unknown_commands(%rip)
	leaq	44(%rsp), %rdi
	leaq	40(%rsp), %rdx
	leaq	24(%rsp), %rcx
	movq	%r13, %rsi
	callq	backward_compatibility_hack
	movslq	44(%rsp), %r15
	leaq	28(%rsp), %r14
	leaq	16(%rsp), %r12
	movl	$0, 20(%rsp)            # 4-byte Folded Spill
	jmp	.LBB0_24
	.p2align	4, 0x90
.LBB0_1:                                #   in Loop: Header=BB0_28 Depth=2
	movq	(%rbp), %rsi
	incl	%ebx
	addq	$24, %rbp
	testq	%rsi, %rsi
	jne	.LBB0_28
	jmp	.LBB0_158
.LBB0_11:                               #   in Loop: Header=BB0_24 Depth=1
	movq	optarg(%rip), %rdi
	movl	$.L.str.67, %esi
	xorl	%eax, %eax
	movq	%r14, %rdx
	movq	%r12, %rcx
	callq	sscanf
	cmpl	$1, %eax
	jg	.LBB0_24
	jmp	.LBB0_161
.LBB0_23:                               #   in Loop: Header=BB0_24 Depth=1
	movq	optarg(%rip), %rdi
	callq	atoi
	movl	%eax, 20(%rsp)          # 4-byte Spill
	jmp	.LBB0_24
.LBB0_2:                                #   in Loop: Header=BB0_24 Depth=1
	movl	debug_table+8(%rip), %eax
	movl	%eax, debug(%rip)
	movl	$1, trace(%rip)
	movl	$1, summary(%rip)
	jmp	.LBB0_24
.LBB0_3:                                # %.preheader215
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	esp_opt_table(%rip), %rsi
	testq	%rsi, %rsi
	movq	optarg(%rip), %rbp
	je	.LBB0_154
# BB#4:                                 # %.lr.ph256.preheader
                                        #   in Loop: Header=BB0_24 Depth=1
	movl	$esp_opt_table+24, %ebx
	.p2align	4, 0x90
.LBB0_5:                                # %.lr.ph256
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_20
# BB#6:                                 #   in Loop: Header=BB0_5 Depth=2
	movq	(%rbx), %rsi
	addq	$24, %rbx
	testq	%rsi, %rsi
	jne	.LBB0_5
	jmp	.LBB0_154
.LBB0_20:                               #   in Loop: Header=BB0_24 Depth=1
	movl	-8(%rbx), %eax
	movq	-16(%rbx), %rcx
	movl	%eax, (%rcx)
	jmp	.LBB0_24
.LBB0_7:                                # %.preheader214
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	pla_types(%rip), %rsi
	testq	%rsi, %rsi
	movq	optarg(%rip), %rbp
	je	.LBB0_155
# BB#8:                                 # %.lr.ph258.preheader
                                        #   in Loop: Header=BB0_24 Depth=1
	movl	$pla_types+16, %ebx
	.p2align	4, 0x90
.LBB0_9:                                # %.lr.ph258
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rsi
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_21
# BB#10:                                #   in Loop: Header=BB0_9 Depth=2
	movq	(%rbx), %rsi
	addq	$16, %rbx
	testq	%rsi, %rsi
	jne	.LBB0_9
	jmp	.LBB0_155
.LBB0_21:                               #   in Loop: Header=BB0_24 Depth=1
	movl	-8(%rbx), %eax
	movl	%eax, 24(%rsp)
	jmp	.LBB0_24
.LBB0_12:                               #   in Loop: Header=BB0_24 Depth=1
	movl	$1, summary(%rip)
	jmp	.LBB0_24
.LBB0_13:                               #   in Loop: Header=BB0_24 Depth=1
	movl	$1, trace(%rip)
	jmp	.LBB0_24
.LBB0_14:                               #   in Loop: Header=BB0_24 Depth=1
	movl	$1, verbose_debug(%rip)
	movq	debug_table(%rip), %rsi
	testq	%rsi, %rsi
	movq	optarg(%rip), %rbp
	je	.LBB0_156
# BB#15:                                # %.lr.ph253.preheader
                                        #   in Loop: Header=BB0_24 Depth=1
	movl	$debug_table+16, %ebx
	.p2align	4, 0x90
.LBB0_16:                               # %.lr.ph253
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbp, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB0_22
# BB#17:                                #   in Loop: Header=BB0_16 Depth=2
	movq	(%rbx), %rsi
	addq	$16, %rbx
	testq	%rsi, %rsi
	jne	.LBB0_16
	jmp	.LBB0_156
.LBB0_22:                               #   in Loop: Header=BB0_24 Depth=1
	movl	-8(%rbx), %eax
	orl	%eax, debug(%rip)
	jmp	.LBB0_24
.LBB0_18:                               #   in Loop: Header=BB0_24 Depth=1
	movl	$0, print_solution(%rip)
	jmp	.LBB0_24
.LBB0_19:                               #   in Loop: Header=BB0_24 Depth=1
	movl	%ebx, 40(%rsp)
	movq	%r12, %r14
	movq	%r13, %r12
	movq	72(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB0_24:                               # %.backedge
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_16 Depth 2
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_5 Depth 2
                                        #     Child Loop BB0_28 Depth 2
	movl	$.L.str.62, %edx
	xorl	%eax, %eax
	movl	%r15d, %edi
	movq	%r13, %rsi
	callq	espresso_getopt
                                        # kill: %EAX<def> %EAX<kill> %RAX<def>
	leal	-68(%rax), %ecx
	cmpl	$52, %ecx
	ja	.LBB0_29
# BB#25:                                # %.backedge
                                        #   in Loop: Header=BB0_24 Depth=1
	jmpq	*.LJTI0_0(,%rcx,8)
.LBB0_26:                               # %.preheader213
                                        #   in Loop: Header=BB0_24 Depth=1
	movq	%r13, 72(%rsp)          # 8-byte Spill
	movq	%r12, %r13
	movq	%r14, %r12
	movq	option_table(%rip), %rsi
	testq	%rsi, %rsi
	movq	optarg(%rip), %r14
	je	.LBB0_158
# BB#27:                                # %.lr.ph260.preheader
                                        #   in Loop: Header=BB0_24 Depth=1
	xorl	%ebx, %ebx
	movl	$option_table+24, %ebp
	.p2align	4, 0x90
.LBB0_28:                               # %.lr.ph260
                                        #   Parent Loop BB0_24 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rdi
	callq	strcmp
	testl	%eax, %eax
	jne	.LBB0_1
	jmp	.LBB0_19
.LBB0_29:                               # %.backedge
	cmpl	$-1, %eax
	jne	.LBB0_160
# BB#30:
	movl	trace(%rip), %eax
	orl	summary(%rip), %eax
	je	.LBB0_35
# BB#31:
	movl	$.L.str.69, %edi
	xorl	%eax, %eax
	callq	printf
	cmpl	$2, %r15d
	jl	.LBB0_34
# BB#32:                                # %.lr.ph249
	movl	$1, %ebx
	.p2align	4, 0x90
.LBB0_33:                               # =>This Inner Loop Header: Depth=1
	movq	(%r13,%rbx,8), %rbp
	movl	$47, %esi
	movq	%rbp, %rdi
	callq	strrchr
	testq	%rax, %rax
	leaq	1(%rax), %rsi
	cmoveq	%rbp, %rsi
	movl	$.L.str.70, %edi
	xorl	%eax, %eax
	callq	printf
	incq	%rbx
	cmpq	%r15, %rbx
	jl	.LBB0_33
.LBB0_34:                               # %._crit_edge250
	movl	$10, %edi
	callq	putchar
	movl	$.L.str.72, %edi
	movl	$.L.str.73, %esi
	xorl	%eax, %eax
	callq	printf
.LBB0_35:
	movq	$0, 32(%rsp)
	movq	$0, 8(%rsp)
	movslq	40(%rsp), %r12
	leaq	(%r12,%r12,2), %r14
	movl	option_table+12(,%r14,8), %eax
	cmpl	$1, %eax
	je	.LBB0_40
# BB#36:
	cmpl	$2, %eax
	jne	.LBB0_44
# BB#37:
	movl	optind(%rip), %edi
	leal	2(%rdi), %eax
	cmpl	%r15d, %eax
	jge	.LBB0_39
# BB#38:
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	optind(%rip), %edi
.LBB0_39:
	leal	1(%rdi), %eax
	movl	%eax, optind(%rip)
	movl	24(%rsp), %ebp
	leaq	8(%rsp), %r8
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r15d, %esi
	movq	%r13, %rdx
	movl	%r12d, %ecx
	movl	%ebp, %r9d
	callq	getPLA
	movl	optind(%rip), %edi
	leal	1(%rdi), %eax
	movl	%eax, optind(%rip)
	leaq	32(%rsp), %r8
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r15d, %esi
	movq	%r13, %rdx
	movl	%r12d, %ecx
	movl	%ebp, %r9d
	jmp	.LBB0_43
.LBB0_40:
	movl	optind(%rip), %edi
	leal	1(%rdi), %eax
	cmpl	%r15d, %eax
	jge	.LBB0_42
# BB#41:
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	callq	fatal
	movl	optind(%rip), %edi
.LBB0_42:
	leal	1(%rdi), %eax
	movl	%eax, optind(%rip)
	movl	24(%rsp), %r9d
	leaq	8(%rsp), %r8
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r15d, %esi
	movq	%r13, %rdx
	movl	%r12d, %ecx
.LBB0_43:
	callq	getPLA
.LBB0_44:
	cmpl	%r15d, optind(%rip)
	jge	.LBB0_46
# BB#45:
	movl	$.L.str.74, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_46:
	movl	trace(%rip), %eax
	orl	summary(%rip), %eax
	je	.LBB0_51
# BB#47:
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_49
# BB#48:
	xorl	%eax, %eax
	callq	PLA_summary
.LBB0_49:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB0_51
# BB#50:
	xorl	%eax, %eax
	callq	PLA_summary
.LBB0_51:
	movl	option_table+8(,%r14,8), %eax
	cmpq	$41, %rax
	movl	$0, %r13d
	ja	.LBB0_134
# BB#52:
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	jmpq	*.LJTI0_1(,%rax,8)
.LBB0_53:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %r14
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	xorl	%eax, %eax
	callq	espresso
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	verify
	movl	%eax, %ebx
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$14, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	totals
	testl	%ebx, %ebx
	je	.LBB0_76
# BB#54:
	movl	$0, print_solution(%rip)
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	check_consistency
	movl	%ebx, %r13d
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_55:
	movq	8(%rsp), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	check_consistency
	jmp	.LBB0_56
.LBB0_57:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	sf_contain
	jmp	.LBB0_130
.LBB0_58:
	movl	28(%rsp), %ebx
	movl	cube+4(%rip), %eax
	testl	%ebx, %ebx
	js	.LBB0_60
# BB#59:
	cmpl	%eax, %ebx
	jl	.LBB0_61
.LBB0_60:
	movl	$0, 28(%rsp)
	xorl	%ebx, %ebx
.LBB0_61:
	movl	16(%rsp), %ecx
	testl	%ecx, %ecx
	js	.LBB0_63
# BB#62:
	cmpl	%eax, %ecx
	jl	.LBB0_64
.LBB0_63:
	decl	%eax
	movl	%eax, 16(%rsp)
	movl	%eax, %ecx
.LBB0_64:
	cmpl	%ecx, %ebx
	movl	$0, %r13d
	jg	.LBB0_134
# BB#65:                                # %.lr.ph244.preheader
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB0_66:                               # %.lr.ph244
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	d1merge
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	cmpl	16(%rsp), %ebx
	leal	1(%rbx), %eax
	movl	%eax, %ebx
	jl	.LBB0_66
	jmp	.LBB0_134
.LBB0_67:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	make_disjoint
	jmp	.LBB0_130
.LBB0_68:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	32(%rsp), %rax
	movq	(%rax), %rsi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	cv_dsharp
	jmp	.LBB0_130
.LBB0_69:
	movq	8(%rsp), %rax
	movq	(%rax), %rax
	movslq	(%rax), %rcx
	movslq	12(%rax), %rdx
	imulq	%rcx, %rdx
	testl	%edx, %edx
	jle	.LBB0_72
# BB#70:                                # %.lr.ph.preheader
	movq	24(%rax), %rcx
	leaq	(%rcx,%rdx,4), %rdx
	movl	$-17409, %esi           # imm = 0xBBFF
	.p2align	4, 0x90
.LBB0_71:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edi
	andl	%esi, %edi
	orl	$1024, %edi             # imm = 0x400
	movl	%edi, (%rcx)
	movslq	(%rax), %rdi
	leaq	(%rcx,%rdi,4), %rcx
	cmpq	%rdx, %rcx
	jb	.LBB0_71
.LBB0_72:                               # %._crit_edge
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	8(%rsp), %rdi
	leaq	8(%rdi), %rsi
	xorl	%eax, %eax
	callq	essential
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$3, %esi
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	totals
	jmp	.LBB0_132
.LBB0_73:
	movl	$1, %ebx
.LBB0_74:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_save
	movq	%rax, %r14
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	xorl	%eax, %eax
	movl	%ebx, %ecx
	callq	minimize_exact
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rdx
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	verify
	movl	%eax, %r13d
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	totals
	testl	%r13d, %r13d
	je	.LBB0_76
# BB#75:
	movl	$0, print_solution(%rip)
	movq	8(%rsp), %rax
	movq	%r14, (%rax)
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	check_consistency
.LBB0_76:
	xorl	%eax, %eax
	movq	%r14, %rdi
	jmp	.LBB0_133
.LBB0_77:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	16(%rax), %rsi
	xorl	%edx, %edx
	xorl	%eax, %eax
	callq	expand
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$4, %esi
	jmp	.LBB0_105
.LBB0_78:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	xorl	%r13d, %r13d
	leaq	48(%rsp), %rcx
	xorl	%eax, %eax
	callq	last_gasp
	jmp	.LBB0_130
.LBB0_79:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	32(%rsp), %rax
	movq	(%rax), %rsi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	cv_intersect
	jmp	.LBB0_130
.LBB0_80:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	callq	irredundant
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$5, %esi
	jmp	.LBB0_105
.LBB0_81:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	lex_sort
	jmp	.LBB0_130
.LBB0_82:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	make_sparse
	jmp	.LBB0_130
.LBB0_83:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	map
	jmp	.LBB0_56
.LBB0_84:
	movq	8(%rsp), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	map_dcset
	movl	$3, 24(%rsp)
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_85:
	movl	28(%rsp), %esi
	movl	cube+4(%rip), %eax
	testl	%esi, %esi
	js	.LBB0_87
# BB#86:
	cmpl	%eax, %esi
	jl	.LBB0_88
.LBB0_87:
	movl	$0, 28(%rsp)
	xorl	%esi, %esi
.LBB0_88:
	movl	16(%rsp), %edx
	testl	%edx, %edx
	js	.LBB0_90
# BB#89:
	cmpl	%eax, %edx
	jl	.LBB0_91
.LBB0_90:
	decl	%eax
	movl	%eax, 16(%rsp)
	movl	%eax, %edx
.LBB0_91:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	unravel_range
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_dupl
	jmp	.LBB0_130
.LBB0_92:
	movq	8(%rsp), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movl	20(%rsp), %esi          # 4-byte Reload
	callq	phase_assignment
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_93:
	movl	28(%rsp), %esi
	testl	%esi, %esi
	js	.LBB0_95
# BB#94:
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	cmpl	(%rax,%rcx,4), %esi
	jl	.LBB0_96
.LBB0_95:
	movl	$0, 28(%rsp)
	xorl	%esi, %esi
.LBB0_96:
	movl	16(%rsp), %edx
	movq	cube+32(%rip), %rax
	movslq	cube+124(%rip), %rcx
	movl	(%rax,%rcx,4), %eax
	testl	%edx, %edx
	js	.LBB0_98
# BB#97:
	cmpl	%eax, %edx
	jl	.LBB0_99
.LBB0_98:                               # %._crit_edge304
	decl	%eax
	movl	%eax, 16(%rsp)
	movl	%eax, %edx
.LBB0_99:
	movq	8(%rsp), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movl	20(%rsp), %ecx          # 4-byte Reload
	callq	opoall
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_100:
	movq	8(%rsp), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movl	20(%rsp), %esi          # 4-byte Reload
	callq	find_optimal_pairing
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_101:
	movq	8(%rsp), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	movl	20(%rsp), %esi          # 4-byte Reload
	callq	pair_all
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_102:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	callq	cube2list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	primes_consensus
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	cmpl	$0, trace(%rip)
	movl	$0, %r13d
	je	.LBB0_140
# BB#103:
	movq	8(%rsp), %rax
	movq	(%rax), %rbx
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movl	$.L.str.77, %esi
	jmp	.LBB0_109
.LBB0_104:
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	xorl	%eax, %eax
	callq	reduce
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$6, %esi
.LBB0_105:                              # %.loopexit
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	totals
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_106:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	32(%rsp), %rax
	movq	(%rax), %rsi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	cv_sharp
	jmp	.LBB0_130
.LBB0_107:
	xorl	%ebx, %ebx
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r14
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	simplify
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	cmpl	$0, trace(%rip)
	movl	$0, %r13d
	je	.LBB0_140
# BB#108:
	movq	8(%rsp), %rax
	movq	(%rax), %rbx
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%r14, %rcx
	movl	$.L.str.76, %esi
.LBB0_109:                              # %.loopexit
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	print_trace
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_110:
	xorl	%r13d, %r13d
	movl	20(%rsp), %esi          # 4-byte Reload
	cmpl	$1, %esi
	cmoval	%r13d, %esi
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	so_espresso
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_111:
	xorl	%r13d, %r13d
	movl	20(%rsp), %esi          # 4-byte Reload
	cmpl	$1, %esi
	cmoval	%r13d, %esi
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	so_both_espresso
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_112:
	cmpl	$0, summary(%rip)
	jne	.LBB0_114
# BB#113:
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	PLA_summary
.LBB0_114:
	movl	$0, print_solution(%rip)
	xorl	%r13d, %r13d
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_115:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	xorl	%r13d, %r13d
	leaq	48(%rsp), %rcx
	xorl	%eax, %eax
	callq	super_gasp
	jmp	.LBB0_130
.LBB0_116:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	tautology
	testl	%eax, %eax
	movl	$.L.str.79, %eax
	movl	$.L.str.80, %esi
	cmovneq	%rax, %rsi
	movl	$.L.str.78, %edi
	xorl	%eax, %eax
	callq	printf
	jmp	.LBB0_56
.LBB0_117:
	movq	8(%rsp), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %rsi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	sf_join
	movq	%rax, %r14
	movl	cube(%rip), %esi
	movl	$10, %edi
	xorl	%eax, %eax
	callq	sf_new
	movq	%rax, %r12
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_free
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	cube1list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %r15
	movl	$1, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	expand
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	movl	$4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	irredundant
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	movl	$5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r15, %rcx
	callq	totals
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	16(%rax), %rsi
	xorl	%eax, %eax
	callq	sf_join
	movq	%rax, %r14
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	8(%rax), %rdi
	xorl	%edx, %edx
	xorl	%eax, %eax
	movq	%r14, %rsi
	callq	expand
	movq	%rax, %rcx
	movq	8(%rsp), %rax
	movq	%rcx, 8(%rax)
	movl	$4, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%r15, %rcx
	callq	totals
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	8(%rax), %rdi
	xorl	%eax, %eax
	movq	%r12, %rsi
	callq	irredundant
	movq	%rax, %rcx
	movq	8(%rsp), %rax
	movq	%rcx, 8(%rax)
	movl	$5, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	movq	%r15, %rcx
	callq	totals
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
	xorl	%eax, %eax
	movq	%r12, %rdi
	jmp	.LBB0_133
.LBB0_118:
	movq	8(%rsp), %rdi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	find_equiv_outputs
	jmp	.LBB0_56
.LBB0_119:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	32(%rsp), %rax
	movq	(%rax), %rsi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	sf_union
	jmp	.LBB0_130
.LBB0_120:                              # %.preheader
	movl	24(%rsp), %r15d
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB0_121:                              # =>This Inner Loop Header: Depth=1
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbp
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rax), %rsi
	movq	16(%rax), %rdx
	xorl	%eax, %eax
	callq	espresso
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	cmpl	$0, trace(%rip)
	je	.LBB0_123
# BB#122:                               #   in Loop: Header=BB0_121 Depth=1
	movq	8(%rsp), %rax
	movq	(%rax), %rbx
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	%rbp, %rcx
	movl	$.L.str.75, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_123:                              #   in Loop: Header=BB0_121 Depth=1
	cmpl	$0, print_solution(%rip)
	je	.LBB0_125
# BB#124:                               #   in Loop: Header=BB0_121 Depth=1
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %rsi
	xorl	%eax, %eax
	movl	%r15d, %edx
	callq	fprint_pla
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB0_125:                              #   in Loop: Header=BB0_121 Depth=1
	movq	8(%rsp), %rdi
	movl	32(%rdi), %ebp
	xorl	%eax, %eax
	callq	free_PLA
	xorl	%eax, %eax
	callq	setdown_cube
	movq	cube+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_127
# BB#126:                               #   in Loop: Header=BB0_121 Depth=1
	callq	free
	movq	$0, cube+32(%rip)
.LBB0_127:                              #   in Loop: Header=BB0_121 Depth=1
	movq	last_fp(%rip), %rdi
	movl	$1, %esi
	movl	$1, %edx
	xorl	%eax, %eax
	movl	%ebp, %ecx
	movq	%r14, %r8
	callq	read_pla
	cmpl	$-1, %eax
	jne	.LBB0_121
# BB#128:
	xorl	%edi, %edi
	callq	exit
.LBB0_129:
	movq	8(%rsp), %rax
	movq	8(%rax), %rdi
	movq	16(%rax), %rsi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	cube2list
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	complement
.LBB0_130:                              # %.loopexit
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
	jmp	.LBB0_153
.LBB0_131:
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	movq	32(%rsp), %rax
	movq	16(%rax), %rsi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %r14
	movq	32(%rsp), %rax
	movq	(%rax), %rdi
	movq	8(%rsp), %rax
	movq	16(%rax), %rsi
	xorl	%eax, %eax
	callq	cv_intersect
	movq	%rax, %rbx
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	callq	sf_free
	xorl	%eax, %eax
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	sf_join
	movq	%rax, %rcx
	xorl	%eax, %eax
	movq	%rcx, %rdi
	callq	sf_contain
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	xorl	%eax, %eax
	movq	%r14, %rdi
	callq	sf_free
.LBB0_132:                              # %.loopexit
	xorl	%eax, %eax
	movq	%rbx, %rdi
.LBB0_133:                              # %.loopexit
	callq	sf_free
.LBB0_134:                              # %.loopexit
	cmpl	$0, trace(%rip)
	je	.LBB0_153
.LBB0_135:                              # %.preheader.i.preheader
	movq	$-64, %rbp
	.p2align	4, 0x90
.LBB0_136:                              # %.preheader.i
                                        # =>This Inner Loop Header: Depth=1
	movl	total_calls+64(%rbp), %ebx
	testl	%ebx, %ebx
	je	.LBB0_138
# BB#137:                               #   in Loop: Header=BB0_136 Depth=1
	movq	total_time+128(%rbp,%rbp), %rdi
	movq	total_name+128(%rbp,%rbp), %r14
	callq	util_print_time
	movq	%rax, %rcx
	movl	$.L.str.89, %edi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	printf
.LBB0_138:                              #   in Loop: Header=BB0_136 Depth=1
	addq	$4, %rbp
	jne	.LBB0_136
# BB#139:                               # %runtime.exit.loopexit
	movl	trace(%rip), %ebx
.LBB0_140:                              # %runtime.exit
	orl	summary(%rip), %ebx
	je	.LBB0_142
.LBB0_141:
	movq	8(%rsp), %rax
	movq	(%rax), %rbx
	movslq	40(%rsp), %rax
	leaq	(%rax,%rax,2), %rax
	movq	option_table(,%rax,8), %rbp
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rcx
	subq	80(%rsp), %rcx          # 8-byte Folded Reload
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%rcx, %rdx
	callq	print_trace
.LBB0_142:
	cmpl	$0, print_solution(%rip)
	je	.LBB0_144
# BB#143:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	stdout(%rip), %rdi
	movq	8(%rsp), %rsi
	movl	24(%rsp), %edx
	xorl	%eax, %eax
	callq	fprint_pla
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$15, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	totals
.LBB0_144:
	testl	%r13d, %r13d
	je	.LBB0_146
# BB#145:
	movl	$.L.str.83, %edi
	xorl	%eax, %eax
	callq	fatal
.LBB0_146:
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	callq	free_PLA
	movq	cube+32(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB0_148
# BB#147:
	callq	free
	movq	$0, cube+32(%rip)
.LBB0_148:
	xorl	%eax, %eax
	callq	setdown_cube
	xorl	%eax, %eax
	callq	sf_cleanup
	xorl	%eax, %eax
	callq	sm_cleanup
	xorl	%edi, %edi
	callq	exit
.LBB0_149:                              # %.preheader211
	cmpl	$0, cube+8(%rip)
	movl	$0, %r13d
	jle	.LBB0_134
# BB#150:                               # %.lr.ph246.preheader
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_151:                              # %.lr.ph246
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsp), %rax
	movq	(%rax), %rdi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	d1merge
	movq	8(%rsp), %rcx
	movq	%rax, (%rcx)
	incl	%ebx
	cmpl	cube+8(%rip), %ebx
	jl	.LBB0_151
	jmp	.LBB0_134
.LBB0_152:
	movq	8(%rsp), %rdi
	movl	summary(%rip), %esi
	xorl	%r13d, %r13d
	xorl	%eax, %eax
	callq	disassemble_fsm
.LBB0_56:
	movl	$0, print_solution(%rip)
	cmpl	$0, trace(%rip)
	jne	.LBB0_135
.LBB0_153:
	xorl	%ebx, %ebx
	orl	summary(%rip), %ebx
	jne	.LBB0_141
	jmp	.LBB0_142
.LBB0_154:                              # %.thread205
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movl	$.L.str.65, %esi
	jmp	.LBB0_157
.LBB0_155:                              # %.thread203
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movl	$.L.str.64, %esi
	jmp	.LBB0_157
.LBB0_156:                              # %.thread207
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movl	$.L.str.66, %esi
.LBB0_157:                              # %.thread203
	xorl	%eax, %eax
	movq	%rbp, %rcx
	jmp	.LBB0_159
.LBB0_158:                              # %.thread
	movq	stderr(%rip), %rdi
	movq	72(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movl	$.L.str.63, %esi
	xorl	%eax, %eax
	movq	%r14, %rcx
.LBB0_159:                              # %.thread203
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB0_160:
	callq	usage
	movl	$1, %edi
	callq	exit
.LBB0_161:
	movq	stderr(%rip), %rdi
	movq	(%r13), %rdx
	movq	optarg(%rip), %rcx
	movl	$.L.str.68, %esi
	xorl	%eax, %eax
	jmp	.LBB0_159
.LBB0_162:
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	movq	32(%rsp), %rsi
	xorl	%eax, %eax
	callq	PLA_verify
	movl	%eax, %ebp
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$14, %esi
	xorl	%eax, %eax
	movq	%rbx, %rdi
	callq	totals
	testl	%ebp, %ebp
	je	.LBB0_164
	jmp	.LBB0_165
.LBB0_163:
	movq	8(%rsp), %rax
	movq	(%rax), %r14
	movq	8(%rax), %rbp
	movq	32(%rsp), %rax
	movq	(%rax), %rbx
	xorl	%eax, %eax
	callq	util_cpu_time
	movq	%rax, %r15
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	verify
	movl	%eax, %ebx
	movq	8(%rsp), %rax
	movq	(%rax), %rdx
	leaq	48(%rsp), %rcx
	movl	$14, %esi
	xorl	%eax, %eax
	movq	%r15, %rdi
	callq	totals
	testl	%ebx, %ebx
	jne	.LBB0_165
.LBB0_164:
	movl	$.Lstr.2, %edi
	callq	puts
	xorl	%edi, %edi
	callq	exit
.LBB0_165:
	movl	$.Lstr.3, %edi
	callq	puts
	movl	$1, %edi
	callq	exit
.Lfunc_end0:
	.size	main, .Lfunc_end0-main
	.cfi_endproc
	.section	.rodata,"a",@progbits
	.p2align	3
.LJTI0_0:
	.quad	.LBB0_26
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_23
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_2
	.quad	.LBB0_3
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_7
	.quad	.LBB0_160
	.quad	.LBB0_160
	.quad	.LBB0_11
	.quad	.LBB0_12
	.quad	.LBB0_13
	.quad	.LBB0_160
	.quad	.LBB0_14
	.quad	.LBB0_160
	.quad	.LBB0_18
.LJTI0_1:
	.quad	.LBB0_53
	.quad	.LBB0_162
	.quad	.LBB0_55
	.quad	.LBB0_57
	.quad	.LBB0_58
	.quad	.LBB0_67
	.quad	.LBB0_68
	.quad	.LBB0_134
	.quad	.LBB0_69
	.quad	.LBB0_73
	.quad	.LBB0_77
	.quad	.LBB0_78
	.quad	.LBB0_79
	.quad	.LBB0_80
	.quad	.LBB0_81
	.quad	.LBB0_82
	.quad	.LBB0_83
	.quad	.LBB0_84
	.quad	.LBB0_85
	.quad	.LBB0_92
	.quad	.LBB0_93
	.quad	.LBB0_100
	.quad	.LBB0_101
	.quad	.LBB0_102
	.quad	.LBB0_74
	.quad	.LBB0_104
	.quad	.LBB0_106
	.quad	.LBB0_107
	.quad	.LBB0_110
	.quad	.LBB0_111
	.quad	.LBB0_112
	.quad	.LBB0_115
	.quad	.LBB0_116
	.quad	.LBB0_117
	.quad	.LBB0_118
	.quad	.LBB0_119
	.quad	.LBB0_163
	.quad	.LBB0_120
	.quad	.LBB0_129
	.quad	.LBB0_131
	.quad	.LBB0_149
	.quad	.LBB0_152

	.text
	.globl	getPLA
	.p2align	4, 0x90
	.type	getPLA,@function
getPLA:                                 # @getPLA
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movl	%r9d, %ebp
	movq	%r8, %r12
	movl	%ecx, %r13d
	movq	%rdx, %r15
	cmpl	%esi, %edi
	jge	.LBB1_1
# BB#5:
	movslq	%edi, %rax
	movq	(%r15,%rax,8), %r14
	cmpb	$45, (%r14)
	jne	.LBB1_7
# BB#6:
	cmpb	$0, 1(%r14)
	je	.LBB1_2
.LBB1_7:                                # %.thread
	movl	$.L.str.86, %esi
	movq	%r14, %rdi
	callq	fopen
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_3
# BB#8:
	movq	stderr(%rip), %rdi
	movq	(%r15), %rdx
	movl	$.L.str.87, %esi
	jmp	.LBB1_9
.LBB1_1:
	movl	$.L.str.84, %r14d
.LBB1_2:
	movq	stdin(%rip), %rbx
.LBB1_3:
	movslq	%r13d, %rax
	leaq	(%rax,%rax,2), %rcx
	cmpl	$7, option_table+8(,%rcx,8)
	jne	.LBB1_10
# BB#4:
	movl	%ebp, %esi
	shrl	%esi
	andl	$1, %esi
	shrl	$2, %ebp
	andl	$1, %ebp
	jmp	.LBB1_11
.LBB1_10:
	shlq	$3, %rax
	movl	option_table+20(%rax,%rax,2), %esi
	movl	option_table+16(%rax,%rax,2), %ebp
.LBB1_11:
	movl	input_type(%rip), %ecx
	xorl	%eax, %eax
	movq	%rbx, %rdi
	movl	%ebp, %edx
	movq	%r12, %r8
	callq	read_pla
	cmpl	$-1, %eax
	je	.LBB1_12
# BB#13:
	movq	%r14, %rdi
	callq	util_strsav
	movq	(%r12), %rcx
	movq	%rax, 24(%rcx)
	movq	(%r12), %rax
	movq	24(%rax), %rax
	movq	%rax, filename(%rip)
	movq	%rbx, last_fp(%rip)
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_12:
	movq	stderr(%rip), %rdi
	movq	(%r15), %rdx
	movl	$.L.str.88, %esi
.LBB1_9:
	xorl	%eax, %eax
	movq	%r14, %rcx
	callq	fprintf
	movl	$1, %edi
	callq	exit
.Lfunc_end1:
	.size	getPLA, .Lfunc_end1-getPLA
	.cfi_endproc

	.globl	runtime
	.p2align	4, 0x90
	.type	runtime,@function
runtime:                                # @runtime
	.cfi_startproc
# BB#0:                                 # %.preheader.preheader
	pushq	%rbp
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 32
.Lcfi29:
	.cfi_offset %rbx, -32
.Lcfi30:
	.cfi_offset %r14, -24
.Lcfi31:
	.cfi_offset %rbp, -16
	movq	$-64, %rbx
	.p2align	4, 0x90
.LBB2_1:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movl	total_calls+64(%rbx), %ebp
	testl	%ebp, %ebp
	je	.LBB2_3
# BB#2:                                 #   in Loop: Header=BB2_1 Depth=1
	movq	total_time+128(%rbx,%rbx), %rdi
	movq	total_name+128(%rbx,%rbx), %r14
	callq	util_print_time
	movq	%rax, %rcx
	movl	$.L.str.89, %edi
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	xorl	%eax, %eax
	movq	%r14, %rsi
	movl	%ebp, %edx
	callq	printf
.LBB2_3:                                #   in Loop: Header=BB2_1 Depth=1
	addq	$4, %rbx
	jne	.LBB2_1
# BB#4:
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end2:
	.size	runtime, .Lfunc_end2-runtime
	.cfi_endproc

	.globl	init_runtime
	.p2align	4, 0x90
	.type	init_runtime,@function
init_runtime:                           # @init_runtime
	.cfi_startproc
# BB#0:
	movq	$.L.str.91, total_name+120(%rip)
	movl	$.L.str.92, %eax
	movd	%rax, %xmm0
	movl	$.L.str.90, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, total_name(%rip)
	movl	$.L.str.94, %eax
	movd	%rax, %xmm0
	movl	$.L.str.95, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+24(%rip)
	movl	$.L.str.93, %eax
	movd	%rax, %xmm0
	movl	$.L.str.96, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+40(%rip)
	movl	$.L.str.99, %eax
	movd	%rax, %xmm0
	movl	$.L.str.98, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+56(%rip)
	movl	$.L.str.102, %eax
	movd	%rax, %xmm0
	movl	$.L.str.101, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+104(%rip)
	movl	$.L.str.77, %eax
	movd	%rax, %xmm0
	movl	$.L.str.97, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+72(%rip)
	movl	$.L.str.100, %eax
	movd	%rax, %xmm0
	movl	$.L.str.103, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, total_name+88(%rip)
	retq
.Lfunc_end3:
	.size	init_runtime, .Lfunc_end3-init_runtime
	.cfi_endproc

	.globl	subcommands
	.p2align	4, 0x90
	.type	subcommands,@function
subcommands:                            # @subcommands
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi32:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi33:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi34:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -40
.Lcfi38:
	.cfi_offset %r12, -32
.Lcfi39:
	.cfi_offset %r14, -24
.Lcfi40:
	.cfi_offset %r15, -16
	movl	$.L.str.104, %edi
	xorl	%eax, %eax
	callq	printf
	movq	option_table(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB4_7
# BB#1:                                 # %.lr.ph.preheader
	movl	$option_table, %ebx
	movl	$16, %r14d
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB4_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movslq	%r14d, %r12
	callq	strlen
	leaq	1(%r12,%rax), %rax
	cmpq	$77, %rax
	jb	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str.105, %edi
	xorl	%eax, %eax
	callq	printf
	movl	$16, %r14d
	jmp	.LBB4_6
	.p2align	4, 0x90
.LBB4_4:                                #   in Loop: Header=BB4_2 Depth=1
	testq	%r15, %r15
	je	.LBB4_6
# BB#5:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	$.L.str.106, %edi
	xorl	%eax, %eax
	callq	printf
.LBB4_6:                                #   in Loop: Header=BB4_2 Depth=1
	movq	(%rbx), %rsi
	movl	$.L.str.107, %edi
	xorl	%eax, %eax
	callq	printf
	movq	(%rbx), %rdi
	callq	strlen
	leal	2(%r14,%rax), %r14d
	movq	24(%rbx), %rdi
	addq	$24, %rbx
	decq	%r15
	testq	%rdi, %rdi
	jne	.LBB4_2
.LBB4_7:                                # %._crit_edge
	movl	$10, %edi
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	jmp	putchar                 # TAILCALL
.Lfunc_end4:
	.size	subcommands, .Lfunc_end4-subcommands
	.cfi_endproc

	.globl	usage
	.p2align	4, 0x90
	.type	usage,@function
usage:                                  # @usage
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi41:
	.cfi_def_cfa_offset 16
	movl	$.L.str.108, %edi
	movl	$.L.str.73, %esi
	xorl	%eax, %eax
	callq	printf
	movl	$.Lstr.4, %edi
	callq	puts
	movl	$.Lstr.5, %edi
	callq	puts
	movl	$.Lstr.6, %edi
	callq	puts
	movl	$.Lstr.7, %edi
	callq	puts
	movl	$.Lstr.8, %edi
	callq	puts
	movl	$.Lstr.9, %edi
	callq	puts
	movl	$.Lstr.10, %edi
	callq	puts
	movl	$.Lstr.11, %edi
	callq	puts
	movl	$.Lstr.12, %edi
	callq	puts
	movl	$.Lstr.13, %edi
	callq	puts
	movl	$.Lstr.14, %edi
	callq	puts
	movl	$.Lstr.15, %edi
	callq	puts
	movl	$.Lstr.16, %edi
	callq	puts
	movl	$.Lstr.17, %edi
	callq	puts
	movl	$.Lstr.18, %edi
	callq	puts
	movl	$.Lstr.19, %edi
	callq	puts
	callq	subcommands
	movl	$.Lstr.20, %edi
	callq	puts
	movl	$.Lstr.21, %edi
	callq	puts
	movl	$.Lstr.22, %edi
	callq	puts
	movl	$.Lstr.23, %edi
	callq	puts
	movl	$.Lstr.24, %edi
	callq	puts
	movl	$.Lstr.25, %edi
	callq	puts
	movl	$.Lstr.26, %edi
	popq	%rax
	jmp	puts                    # TAILCALL
.Lfunc_end5:
	.size	usage, .Lfunc_end5-usage
	.cfi_endproc

	.globl	backward_compatibility_hack
	.p2align	4, 0x90
	.type	backward_compatibility_hack,@function
backward_compatibility_hack:            # @backward_compatibility_hack
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi42:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi43:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi44:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi45:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi46:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 56
	subq	$72, %rsp
.Lcfi48:
	.cfi_def_cfa_offset 128
.Lcfi49:
	.cfi_offset %rbx, -56
.Lcfi50:
	.cfi_offset %r12, -48
.Lcfi51:
	.cfi_offset %r13, -40
.Lcfi52:
	.cfi_offset %r14, -32
.Lcfi53:
	.cfi_offset %r15, -24
.Lcfi54:
	.cfi_offset %rbp, -16
	movq	%rcx, %r9
	movq	%rdx, %r11
	movq	%rsi, %r13
	movabsq	$8589934592, %r8        # imm = 0x200000000
	movabsq	$4294967296, %rsi       # imm = 0x100000000
	movl	$0, (%r11)
	movslq	(%rdi), %r10
	movq	%r10, %rax
	decq	%rax
	cmpl	$2, %eax
	movq	%r13, (%rsp)            # 8-byte Spill
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	jl	.LBB6_8
# BB#1:                                 # %.lr.ph218.preheader
	movq	$-1, %r12
	movl	$2, %r13d
	xorl	%ebx, %ebx
	movq	%r8, %rcx
	xorl	%ebp, %ebp
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB6_2:                                # %.lr.ph218
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rsp), %rdx            # 8-byte Reload
	movq	8(%rdx,%rbp,8), %rdx
	cmpb	$45, (%rdx)
	jne	.LBB6_45
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpb	$100, 1(%rdx)
	jne	.LBB6_45
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpb	$111, 2(%rdx)
	jne	.LBB6_45
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpb	$0, 3(%rdx)
	je	.LBB6_6
	.p2align	4, 0x90
.LBB6_45:                               # %.thread
                                        #   in Loop: Header=BB6_2 Depth=1
	incq	%r13
	decq	%r12
	decq	%r15
	addq	$-8, %rbx
	leaq	1(%rbp), %rdx
	addq	$2, %rbp
	addq	%rsi, %rcx
	cmpq	%rax, %rbp
	movq	%rdx, %rbp
	jl	.LBB6_2
# BB#46:
	movq	(%rsp), %r13            # 8-byte Reload
.LBB6_8:                                # %.preheader185
	leal	-1(%r10), %eax
	movl	$1, %r14d
	cmpl	$2, %eax
	jl	.LBB6_80
# BB#9:                                 # %.lr.ph214.preheader
	movq	%r9, 56(%rsp)           # 8-byte Spill
	movq	%r10, 8(%rsp)           # 8-byte Spill
	cltq
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	$-1, %rcx
	movl	$2, %r15d
	xorl	%eax, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	xorl	%r13d, %r13d
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph214
                                        # =>This Inner Loop Header: Depth=1
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rsi, %rbp
	movq	%r8, %r12
	movq	(%rsp), %rax            # 8-byte Reload
	movq	8(%rax,%r13,8), %rdi
	movl	$.L.str.134, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_11
# BB#78:                                #   in Loop: Header=BB6_10 Depth=1
	incq	%r15
	movq	32(%rsp), %rcx          # 8-byte Reload
	decq	%rcx
	decq	%rbx
	addq	$-8, 24(%rsp)           # 8-byte Folded Spill
	leaq	1(%r13), %rax
	addq	$2, %r13
	movq	%r12, %r8
	movq	%rbp, %rsi
	addq	%rsi, %r8
	cmpq	40(%rsp), %r13          # 8-byte Folded Reload
	movq	%rax, %r13
	jl	.LBB6_10
# BB#79:
	movq	(%rsp), %r13            # 8-byte Reload
	movq	8(%rsp), %r10           # 8-byte Reload
	cmpl	%r10d, %r14d
	jl	.LBB6_81
	jmp	.LBB6_102
.LBB6_11:                               # %.preheader184
	movq	pla_types(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB6_12
# BB#48:                                # %.lr.ph212
	leaq	1(%r13), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	2(%r13), %rbp
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	subq	24(%rsp), %rcx          # 8-byte Folded Reload
	addq	$16, %rcx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	movq	16(%rax,%r13,8), %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movl	$pla_types+16, %r12d
	.p2align	4, 0x90
.LBB6_49:                               # =>This Inner Loop Header: Depth=1
	incq	%rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_50
# BB#47:                                #   in Loop: Header=BB6_49 Depth=1
	movq	(%r12), %rdi
	addq	$16, %r12
	testq	%rdi, %rdi
	jne	.LBB6_49
.LBB6_77:                               # %delete_arg.exit176.thread
	movq	stderr(%rip), %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movl	$.L.str.135, %esi
.LBB6_44:                               # %delete_arg.exit150.thread
	xorl	%eax, %eax
	callq	fprintf
	movl	$1, %edi
	callq	exit
.LBB6_50:
	movl	-8(%r12), %eax
	movq	56(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %r10
	leaq	-1(%r10), %rax
	movl	%eax, (%rcx)
	movq	%rbp, %rsi
	cmpl	%esi, %eax
	movq	8(%rsp), %rbp           # 8-byte Reload
	jle	.LBB6_63
# BB#51:                                # %.lr.ph.i160
	movq	%rax, %rcx
	subq	%r13, %rcx
	addq	$-2, %rcx
	cmpq	$4, %rcx
	jb	.LBB6_61
# BB#52:                                # %min.iters.checked318
	movq	%rcx, %rdx
	andq	$-4, %rdx
	je	.LBB6_61
# BB#53:                                # %vector.body314.preheader
	leaq	-4(%rdx), %rdi
	movq	%rdi, %rsi
	shrq	$2, %rsi
	btl	$2, %edi
	jb	.LBB6_54
# BB#55:                                # %vector.body314.prol
	movq	(%rsp), %rdi            # 8-byte Reload
	movups	24(%rdi,%r13,8), %xmm0
	movups	40(%rdi,%r13,8), %xmm1
	movups	%xmm0, 16(%rdi,%r13,8)
	movups	%xmm1, 32(%rdi,%r13,8)
	movl	$4, %r8d
	testq	%rsi, %rsi
	jne	.LBB6_57
	jmp	.LBB6_59
.LBB6_6:                                # %.preheader187
	movq	%r11, 40(%rsp)          # 8-byte Spill
	movq	option_table(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB6_7
# BB#13:                                # %.lr.ph216
	movq	%r9, 56(%rsp)           # 8-byte Spill
	leaq	1(%rbp), %rax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	leaq	2(%rbp), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rax, %rcx
	subq	%rbx, %rcx
	addq	$16, %rcx
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	16(%rax,%rbp,8), %rax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$option_table, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB6_14:                               # =>This Inner Loop Header: Depth=1
	movq	24(%rsp), %rdi          # 8-byte Reload
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_15
# BB#42:                                #   in Loop: Header=BB6_14 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	24(%rax), %rsi
	addq	$24, %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	incl	%r14d
	testq	%rsi, %rsi
	jne	.LBB6_14
.LBB6_43:                               # %delete_arg.exit150.thread
	movq	stderr(%rip), %rdi
	movq	64(%rsp), %rax          # 8-byte Reload
	movq	(%rax), %rdx
	movl	$.L.str.133, %esi
	jmp	.LBB6_44
.LBB6_15:
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%r14d, (%rax)
	movq	16(%rsp), %rcx          # 8-byte Reload
	movslq	(%rcx), %r10
	leaq	-1(%r10), %rax
	movl	%eax, (%rcx)
	movq	48(%rsp), %rdx          # 8-byte Reload
	cmpl	%edx, %eax
	movq	56(%rsp), %r9           # 8-byte Reload
	movabsq	$8589934592, %r8        # imm = 0x200000000
	jle	.LBB6_28
# BB#16:                                # %.lr.ph.i
	movq	%rax, %rcx
	subq	%rbp, %rcx
	addq	$-2, %rcx
	cmpq	$4, %rcx
	jb	.LBB6_26
# BB#17:                                # %min.iters.checked
	movq	%rcx, %r11
	andq	$-4, %r11
	je	.LBB6_26
# BB#18:                                # %vector.body.preheader
	leaq	-4(%r11), %rdi
	movq	%rdi, %rdx
	shrq	$2, %rdx
	btl	$2, %edi
	jb	.LBB6_19
# BB#20:                                # %vector.body.prol
	movq	(%rsp), %rdi            # 8-byte Reload
	movups	24(%rdi,%rbp,8), %xmm0
	movups	40(%rdi,%rbp,8), %xmm1
	movups	%xmm0, 16(%rdi,%rbp,8)
	movups	%xmm1, 32(%rdi,%rbp,8)
	movl	$4, %esi
	testq	%rdx, %rdx
	jne	.LBB6_22
	jmp	.LBB6_24
.LBB6_54:
	xorl	%r8d, %r8d
	testq	%rsi, %rsi
	je	.LBB6_59
.LBB6_57:                               # %vector.body314.preheader.new
	movq	%rdx, %rsi
	subq	%r8, %rsi
	movq	(%rsp), %rdi            # 8-byte Reload
	leaq	72(%rdi,%r8,8), %rdi
	subq	24(%rsp), %rdi          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB6_58:                               # %vector.body314
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -56(%rdi)
	movups	%xmm1, -40(%rdi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -24(%rdi)
	movups	%xmm1, -8(%rdi)
	addq	$64, %rdi
	addq	$-8, %rsi
	jne	.LBB6_58
.LBB6_59:                               # %middle.block315
	cmpq	%rdx, %rcx
	je	.LBB6_63
# BB#60:
	leaq	-2(%rbx,%rax), %rcx
	andq	$-4, %rcx
	leaq	2(%rcx,%r13), %rsi
.LBB6_61:                               # %scalar.ph316.preheader
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	8(%rcx,%rsi,8), %rcx
	subq	%rsi, %rax
	.p2align	4, 0x90
.LBB6_62:                               # %scalar.ph316
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, -8(%rcx)
	addq	$8, %rcx
	decq	%rax
	jne	.LBB6_62
.LBB6_63:                               # %delete_arg.exit163
	addq	$-2, %r10
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r10d, (%rax)
	cmpl	%ebp, %r10d
	movq	(%rsp), %rdx            # 8-byte Reload
	jle	.LBB6_76
# BB#64:                                # %.lr.ph.i173
	cmpq	%r10, %r15
	movq	%r10, %rax
	cmovgeq	%r15, %rax
	subq	%r13, %rax
	decq	%rax
	cmpq	$4, %rax
	jb	.LBB6_74
# BB#65:                                # %min.iters.checked340
	cmpq	%r10, %r15
	movq	%r10, %rcx
	cmovgeq	%r15, %rcx
	movq	%rax, %rdx
	andq	$-4, %rdx
	je	.LBB6_74
# BB#66:                                # %vector.body335.preheader
	leaq	-4(%rdx), %rsi
	movq	%rsi, %rdi
	shrq	$2, %rdi
	btl	$2, %esi
	jb	.LBB6_67
# BB#68:                                # %vector.body335.prol
	movq	(%rsp), %rsi            # 8-byte Reload
	movups	16(%rsi,%r13,8), %xmm0
	movups	32(%rsi,%r13,8), %xmm1
	movups	%xmm0, 8(%rsi,%r13,8)
	movups	%xmm1, 24(%rsi,%r13,8)
	movl	$4, %esi
	testq	%rdi, %rdi
	jne	.LBB6_70
	jmp	.LBB6_72
.LBB6_67:
	xorl	%esi, %esi
	testq	%rdi, %rdi
	je	.LBB6_72
.LBB6_70:                               # %vector.body335.preheader.new
	cmpq	%r10, %r15
	cmovlq	%r10, %r15
	subq	%r13, %r15
	decq	%r15
	andq	$-4, %r15
	subq	%rsi, %r15
	movq	(%rsp), %rdi            # 8-byte Reload
	leaq	64(%rdi,%rsi,8), %rsi
	subq	24(%rsp), %rsi          # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB6_71:                               # %vector.body335
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -56(%rsi)
	movups	%xmm1, -40(%rsi)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -24(%rsi)
	movups	%xmm1, -8(%rsi)
	addq	$64, %rsi
	addq	$-8, %r15
	jne	.LBB6_71
.LBB6_72:                               # %middle.block336
	cmpq	%rdx, %rax
	movq	(%rsp), %rdx            # 8-byte Reload
	je	.LBB6_76
# BB#73:
	addq	32(%rsp), %rcx          # 8-byte Folded Reload
	andq	$-4, %rcx
	leaq	1(%rcx,%r13), %rbp
.LBB6_74:                               # %scalar.ph337.preheader
	movq	(%rsp), %rdx            # 8-byte Reload
	.p2align	4, 0x90
.LBB6_75:                               # %scalar.ph337
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rbp,8), %rax
	movq	%rax, (%rdx,%rbp,8)
	incq	%rbp
	cmpq	%r10, %rbp
	jl	.LBB6_75
.LBB6_76:                               # %delete_arg.exit176
	cmpq	$0, -16(%r12)
	movq	%rdx, %r13
	jne	.LBB6_80
	jmp	.LBB6_77
	.p2align	4, 0x90
.LBB6_101:                              # %.loopexit
	incl	%r14d
.LBB6_80:                               # %.loopexit186
	cmpl	%r10d, %r14d
	jge	.LBB6_102
.LBB6_81:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_84 Depth 2
	movslq	%r14d, %r15
	movq	(%r13,%r15,8), %rbx
	cmpb	$45, (%rbx)
	jne	.LBB6_101
# BB#82:                                # %.preheader
                                        #   in Loop: Header=BB6_81 Depth=1
	movq	esp_opt_table(%rip), %rsi
	testq	%rsi, %rsi
	je	.LBB6_101
# BB#83:                                # %.lr.ph
                                        #   in Loop: Header=BB6_81 Depth=1
	movq	%r10, %r12
	incq	%rbx
	movl	$esp_opt_table+24, %ebp
	.p2align	4, 0x90
.LBB6_84:                               #   Parent Loop BB6_81 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%rbx, %rdi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_87
# BB#85:                                #   in Loop: Header=BB6_84 Depth=2
	movq	(%rbp), %rsi
	addq	$24, %rbp
	testq	%rsi, %rsi
	jne	.LBB6_84
# BB#86:                                #   in Loop: Header=BB6_81 Depth=1
	movq	%r12, %r10
	incl	%r14d
	cmpl	%r10d, %r14d
	jl	.LBB6_81
	jmp	.LBB6_102
.LBB6_87:
	decl	%r12d
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r12d, (%rax)
	cmpl	%r14d, %r12d
	jle	.LBB6_100
# BB#88:                                # %.lr.ph.i177
	movslq	%r12d, %rax
	movq	%rax, %rcx
	subq	%r15, %rcx
	cmpq	$4, %rcx
	jb	.LBB6_98
# BB#89:                                # %min.iters.checked427
	movq	%rcx, %r8
	andq	$-4, %r8
	movq	%rcx, %rsi
	andq	$-4, %rsi
	je	.LBB6_98
# BB#90:                                # %vector.body423.preheader
	leaq	-4(%rsi), %rdi
	movq	%rdi, %rdx
	shrq	$2, %rdx
	btl	$2, %edi
	jb	.LBB6_91
# BB#92:                                # %vector.body423.prol
	movups	8(%r13,%r15,8), %xmm0
	movups	24(%r13,%r15,8), %xmm1
	movups	%xmm0, (%r13,%r15,8)
	movups	%xmm1, 16(%r13,%r15,8)
	movl	$4, %ebx
	testq	%rdx, %rdx
	jne	.LBB6_94
	jmp	.LBB6_96
.LBB6_91:
	xorl	%ebx, %ebx
	testq	%rdx, %rdx
	je	.LBB6_96
.LBB6_94:                               # %vector.body423.preheader.new
	movq	%rsi, %rdi
	subq	%rbx, %rdi
	addq	%r15, %rbx
	leaq	56(%r13,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB6_95:                               # %vector.body423
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -56(%rbx)
	movups	%xmm1, -40(%rbx)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -24(%rbx)
	movups	%xmm1, -8(%rbx)
	addq	$64, %rbx
	addq	$-8, %rdi
	jne	.LBB6_95
.LBB6_96:                               # %middle.block424
	cmpq	%rsi, %rcx
	je	.LBB6_100
# BB#97:
	addq	%r8, %r15
.LBB6_98:                               # %scalar.ph425.preheader
	leaq	8(%r13,%r15,8), %rcx
	subq	%r15, %rax
	.p2align	4, 0x90
.LBB6_99:                               # %scalar.ph425
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, -8(%rcx)
	addq	$8, %rcx
	decq	%rax
	jne	.LBB6_99
.LBB6_100:                              # %delete_arg.exit180
	movl	-8(%rbp), %eax
	movq	-16(%rbp), %rcx
	movl	%eax, (%rcx)
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	(%rax), %r10d
	jmp	.LBB6_101
.LBB6_102:
	cmpl	$2, %r10d
	jl	.LBB6_120
# BB#103:                               # %.lr.ph.preheader.i164
	movq	%r10, 8(%rsp)           # 8-byte Spill
	movslq	%r10d, %r12
	movq	$-1, %r14
	movl	$2, %r15d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_104:                              # %.lr.ph.i166
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13,%rbp,8), %rdi
	movl	$.L.str.136, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_105
# BB#118:                               #   in Loop: Header=BB6_104 Depth=1
	incq	%r15
	decq	%r14
	addq	$-8, %rbx
	leaq	1(%rbp), %rax
	addq	$2, %rbp
	cmpq	%r12, %rbp
	movq	%rax, %rbp
	jl	.LBB6_104
# BB#119:
	movq	8(%rsp), %r10           # 8-byte Reload
	jmp	.LBB6_120
.LBB6_105:
	leaq	1(%rbp), %rdi
	movq	8(%rsp), %r10           # 8-byte Reload
	decl	%r10d
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r10d, (%rax)
	cmpl	%edi, %r10d
	jle	.LBB6_117
# BB#106:                               # %.lr.ph.i.i167
	movslq	%r10d, %rax
	cmpq	%rax, %r15
	movq	%rax, %rcx
	cmovgeq	%r15, %rcx
	decq	%rcx
	subq	%rbp, %rcx
	cmpq	$4, %rcx
	jb	.LBB6_116
# BB#107:                               # %min.iters.checked362
	cmpq	%rax, %r15
	movq	%rax, %r9
	cmovgeq	%r15, %r9
	movq	%rcx, %rsi
	andq	$-4, %rsi
	je	.LBB6_116
# BB#108:                               # %vector.body357.preheader
	leaq	-4(%rsi), %rdi
	movq	%rdi, %rdx
	shrq	$2, %rdx
	btl	$2, %edi
	jb	.LBB6_109
# BB#110:                               # %vector.body357.prol
	movups	16(%r13,%rbp,8), %xmm0
	movups	32(%r13,%rbp,8), %xmm1
	movups	%xmm0, 8(%r13,%rbp,8)
	movups	%xmm1, 24(%r13,%rbp,8)
	movl	$4, %r8d
	testq	%rdx, %rdx
	jne	.LBB6_112
	jmp	.LBB6_114
.LBB6_109:
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.LBB6_114
.LBB6_112:                              # %vector.body357.preheader.new
	cmpq	%rax, %r15
	cmovlq	%rax, %r15
	decq	%r15
	subq	%rbp, %r15
	andq	$-4, %r15
	subq	%r8, %r15
	leaq	64(%r13,%r8,8), %rdi
	subq	%rbx, %rdi
	.p2align	4, 0x90
.LBB6_113:                              # %vector.body357
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -56(%rdi)
	movups	%xmm1, -40(%rdi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -24(%rdi)
	movups	%xmm1, -8(%rdi)
	addq	$64, %rdi
	addq	$-8, %r15
	jne	.LBB6_113
.LBB6_114:                              # %middle.block358
	cmpq	%rsi, %rcx
	je	.LBB6_117
# BB#115:
	addq	%r14, %r9
	andq	$-4, %r9
	leaq	1(%r9,%rbp), %rdi
	.p2align	4, 0x90
.LBB6_116:                              # %scalar.ph359
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13,%rdi,8), %rcx
	movq	%rcx, (%r13,%rdi,8)
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB6_116
.LBB6_117:                              # %check_arg.exit172
	movl	$7, input_type(%rip)
.LBB6_120:                              # %check_arg.exit172.thread
	movq	%r10, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %r10d
	jl	.LBB6_154
# BB#121:                               # %.lr.ph.preheader.i151
	movslq	8(%rsp), %r12           # 4-byte Folded Reload
	movq	$-1, %r14
	movl	$2, %r15d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_122:                              # %.lr.ph.i153
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13,%rbp,8), %rdi
	movl	$.L.str.137, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_123
# BB#136:                               #   in Loop: Header=BB6_122 Depth=1
	incq	%r15
	decq	%r14
	addq	$-8, %rbx
	leaq	1(%rbp), %rax
	addq	$2, %rbp
	cmpq	%r12, %rbp
	movq	%rax, %rbp
	jl	.LBB6_122
# BB#137:
	movq	8(%rsp), %rsi           # 8-byte Reload
	jmp	.LBB6_138
.LBB6_123:
	leaq	1(%rbp), %rdi
	movq	8(%rsp), %rsi           # 8-byte Reload
	decl	%esi
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%esi, (%rax)
	cmpl	%edi, %esi
	jle	.LBB6_135
# BB#124:                               # %.lr.ph.i.i154
	movslq	%esi, %rax
	cmpq	%rax, %r15
	movq	%rax, %rcx
	cmovgeq	%r15, %rcx
	decq	%rcx
	subq	%rbp, %rcx
	cmpq	$4, %rcx
	jb	.LBB6_134
# BB#125:                               # %min.iters.checked384
	cmpq	%rax, %r15
	movq	%rax, %r9
	cmovgeq	%r15, %r9
	movq	%rcx, %r10
	andq	$-4, %r10
	je	.LBB6_134
# BB#126:                               # %vector.body379.preheader
	leaq	-4(%r10), %rdi
	movq	%rdi, %rdx
	shrq	$2, %rdx
	btl	$2, %edi
	jb	.LBB6_127
# BB#128:                               # %vector.body379.prol
	movups	16(%r13,%rbp,8), %xmm0
	movups	32(%r13,%rbp,8), %xmm1
	movups	%xmm0, 8(%r13,%rbp,8)
	movups	%xmm1, 24(%r13,%rbp,8)
	movl	$4, %r8d
	testq	%rdx, %rdx
	jne	.LBB6_130
	jmp	.LBB6_132
.LBB6_127:
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.LBB6_132
.LBB6_130:                              # %vector.body379.preheader.new
	cmpq	%rax, %r15
	cmovlq	%rax, %r15
	decq	%r15
	subq	%rbp, %r15
	andq	$-4, %r15
	subq	%r8, %r15
	leaq	64(%r13,%r8,8), %rdi
	subq	%rbx, %rdi
	.p2align	4, 0x90
.LBB6_131:                              # %vector.body379
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -56(%rdi)
	movups	%xmm1, -40(%rdi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -24(%rdi)
	movups	%xmm1, -8(%rdi)
	addq	$64, %rdi
	addq	$-8, %r15
	jne	.LBB6_131
.LBB6_132:                              # %middle.block380
	cmpq	%r10, %rcx
	je	.LBB6_135
# BB#133:
	addq	%r14, %r9
	andq	$-4, %r9
	leaq	1(%r9,%rbp), %rdi
	.p2align	4, 0x90
.LBB6_134:                              # %scalar.ph381
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13,%rdi,8), %rcx
	movq	%rcx, (%r13,%rdi,8)
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB6_134
.LBB6_135:                              # %check_arg.exit159
	movl	$5, input_type(%rip)
.LBB6_138:                              # %check_arg.exit159.thread
	movq	%rsi, 8(%rsp)           # 8-byte Spill
	cmpl	$2, %esi
	jl	.LBB6_154
# BB#139:                               # %.lr.ph.preheader.i
	movslq	8(%rsp), %r12           # 4-byte Folded Reload
	movq	$-1, %r14
	movl	$2, %r15d
	xorl	%ebp, %ebp
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_140:                              # %.lr.ph.i145
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13,%rbp,8), %rdi
	movl	$.L.str.138, %esi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB6_141
# BB#155:                               #   in Loop: Header=BB6_140 Depth=1
	incq	%r15
	decq	%r14
	addq	$-8, %rbx
	leaq	1(%rbp), %rax
	addq	$2, %rbp
	cmpq	%r12, %rbp
	movq	%rax, %rbp
	jl	.LBB6_140
	jmp	.LBB6_154
.LBB6_141:
	leaq	1(%rbp), %rdi
	movq	8(%rsp), %rcx           # 8-byte Reload
	decl	%ecx
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%ecx, (%rax)
	cmpl	%edi, %ecx
	jle	.LBB6_153
# BB#142:                               # %.lr.ph.i.i
	movslq	%ecx, %rax
	cmpq	%rax, %r15
	movq	%rax, %rcx
	cmovgeq	%r15, %rcx
	decq	%rcx
	subq	%rbp, %rcx
	cmpq	$4, %rcx
	jb	.LBB6_152
# BB#143:                               # %min.iters.checked406
	cmpq	%rax, %r15
	movq	%rax, %r9
	cmovgeq	%r15, %r9
	movq	%rcx, %rsi
	andq	$-4, %rsi
	je	.LBB6_152
# BB#144:                               # %vector.body401.preheader
	leaq	-4(%rsi), %rdi
	movq	%rdi, %rdx
	shrq	$2, %rdx
	btl	$2, %edi
	jb	.LBB6_145
# BB#146:                               # %vector.body401.prol
	movups	16(%r13,%rbp,8), %xmm0
	movups	32(%r13,%rbp,8), %xmm1
	movups	%xmm0, 8(%r13,%rbp,8)
	movups	%xmm1, 24(%r13,%rbp,8)
	movl	$4, %r8d
	testq	%rdx, %rdx
	jne	.LBB6_148
	jmp	.LBB6_150
.LBB6_145:
	xorl	%r8d, %r8d
	testq	%rdx, %rdx
	je	.LBB6_150
.LBB6_148:                              # %vector.body401.preheader.new
	cmpq	%rax, %r15
	cmovlq	%rax, %r15
	decq	%r15
	subq	%rbp, %r15
	andq	$-4, %r15
	subq	%r8, %r15
	leaq	64(%r13,%r8,8), %rdi
	subq	%rbx, %rdi
	.p2align	4, 0x90
.LBB6_149:                              # %vector.body401
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -56(%rdi)
	movups	%xmm1, -40(%rdi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -24(%rdi)
	movups	%xmm1, -8(%rdi)
	addq	$64, %rdi
	addq	$-8, %r15
	jne	.LBB6_149
.LBB6_150:                              # %middle.block402
	cmpq	%rsi, %rcx
	je	.LBB6_153
# BB#151:
	addq	%r14, %r9
	andq	$-4, %r9
	leaq	1(%r9,%rbp), %rdi
	.p2align	4, 0x90
.LBB6_152:                              # %scalar.ph403
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r13,%rdi,8), %rcx
	movq	%rcx, (%r13,%rdi,8)
	incq	%rdi
	cmpq	%rax, %rdi
	jl	.LBB6_152
.LBB6_153:                              # %check_arg.exit
	movl	$1, input_type(%rip)
.LBB6_154:                              # %check_arg.exit.thread
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_19:
	xorl	%esi, %esi
	testq	%rdx, %rdx
	je	.LBB6_24
.LBB6_22:                               # %vector.body.preheader.new
	movq	%r11, %rdx
	subq	%rsi, %rdx
	movq	(%rsp), %rdi            # 8-byte Reload
	leaq	72(%rdi,%rsi,8), %rdi
	subq	%rbx, %rdi
.LBB6_23:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdi), %xmm0
	movups	-32(%rdi), %xmm1
	movups	%xmm0, -56(%rdi)
	movups	%xmm1, -40(%rdi)
	movups	-16(%rdi), %xmm0
	movups	(%rdi), %xmm1
	movups	%xmm0, -24(%rdi)
	movups	%xmm1, -8(%rdi)
	addq	$64, %rdi
	addq	$-8, %rdx
	jne	.LBB6_23
.LBB6_24:                               # %middle.block
	cmpq	%r11, %rcx
	je	.LBB6_28
# BB#25:
	leaq	-2(%r15,%rax), %rcx
	andq	$-4, %rcx
	leaq	2(%rcx,%rbp), %rdx
.LBB6_26:                               # %scalar.ph.preheader
	movq	(%rsp), %rcx            # 8-byte Reload
	leaq	8(%rcx,%rdx,8), %rcx
	subq	%rdx, %rax
	.p2align	4, 0x90
.LBB6_27:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rcx), %rdx
	movq	%rdx, -8(%rcx)
	addq	$8, %rcx
	decq	%rax
	jne	.LBB6_27
.LBB6_28:                               # %delete_arg.exit
	addq	$-2, %r10
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r10d, (%rax)
	movq	8(%rsp), %rcx           # 8-byte Reload
	cmpl	%ecx, %r10d
	movq	(%rsp), %rdx            # 8-byte Reload
	jle	.LBB6_41
# BB#29:                                # %.lr.ph.i147
	cmpq	%r10, %r13
	movq	%r10, %rax
	cmovgeq	%r13, %rax
	subq	%rbp, %rax
	decq	%rax
	cmpq	$4, %rax
	jb	.LBB6_39
# BB#30:                                # %min.iters.checked297
	cmpq	%r10, %r13
	movq	%r10, %rsi
	cmovgeq	%r13, %rsi
	movq	%rax, %rdx
	andq	$-4, %rdx
	je	.LBB6_39
# BB#31:                                # %vector.body293.preheader
	leaq	-4(%rdx), %rdi
	movq	%rdi, %rcx
	shrq	$2, %rcx
	btl	$2, %edi
	jb	.LBB6_32
# BB#33:                                # %vector.body293.prol
	movq	(%rsp), %rdi            # 8-byte Reload
	movups	16(%rdi,%rbp,8), %xmm0
	movups	32(%rdi,%rbp,8), %xmm1
	movups	%xmm0, 8(%rdi,%rbp,8)
	movups	%xmm1, 24(%rdi,%rbp,8)
	movl	$4, %r11d
	testq	%rcx, %rcx
	jne	.LBB6_35
	jmp	.LBB6_37
.LBB6_32:
	xorl	%r11d, %r11d
	testq	%rcx, %rcx
	je	.LBB6_37
.LBB6_35:                               # %vector.body293.preheader.new
	cmpq	%r10, %r13
	cmovlq	%r10, %r13
	subq	%rbp, %r13
	decq	%r13
	andq	$-4, %r13
	subq	%r11, %r13
	movq	(%rsp), %rdi            # 8-byte Reload
	leaq	64(%rdi,%r11,8), %rcx
	subq	%rbx, %rcx
.LBB6_36:                               # %vector.body293
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -56(%rcx)
	movups	%xmm1, -40(%rcx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -24(%rcx)
	movups	%xmm1, -8(%rcx)
	addq	$64, %rcx
	addq	$-8, %r13
	jne	.LBB6_36
.LBB6_37:                               # %middle.block294
	cmpq	%rdx, %rax
	movq	(%rsp), %rdx            # 8-byte Reload
	je	.LBB6_41
# BB#38:
	addq	%r12, %rsi
	andq	$-4, %rsi
	leaq	1(%rsi,%rbp), %rcx
.LBB6_39:                               # %scalar.ph295.preheader
	movq	(%rsp), %rdx            # 8-byte Reload
	.p2align	4, 0x90
.LBB6_40:                               # %scalar.ph295
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rdx,%rcx,8), %rax
	movq	%rax, (%rdx,%rcx,8)
	incq	%rcx
	cmpq	%r10, %rcx
	jl	.LBB6_40
.LBB6_41:                               # %delete_arg.exit150
	movq	32(%rsp), %rax          # 8-byte Reload
	cmpq	$0, (%rax)
	movq	%rdx, %r13
	movabsq	$4294967296, %rdi       # imm = 0x100000000
	movq	%rdi, %rsi
	jne	.LBB6_8
	jmp	.LBB6_43
.LBB6_12:                               # %.preheader184.delete_arg.exit176.thread_crit_edge
	sarq	$29, %r12
	movq	(%rsp), %rax            # 8-byte Reload
	addq	%r12, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	jmp	.LBB6_77
.LBB6_7:                                # %.preheader187.delete_arg.exit150.thread_crit_edge
	sarq	$29, %rcx
	movq	(%rsp), %rax            # 8-byte Reload
	addq	%rcx, %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	jmp	.LBB6_43
.Lfunc_end6:
	.size	backward_compatibility_hack, .Lfunc_end6-backward_compatibility_hack
	.cfi_endproc

	.globl	delete_arg
	.p2align	4, 0x90
	.type	delete_arg,@function
delete_arg:                             # @delete_arg
	.cfi_startproc
# BB#0:
	movslq	(%rdi), %rax
	decq	%rax
	movl	%eax, (%rdi)
	cmpl	%edx, %eax
	jle	.LBB7_12
# BB#1:                                 # %.lr.ph
	movslq	%edx, %rcx
	movq	%rax, %r10
	subq	%rcx, %r10
	cmpq	$4, %r10
	jb	.LBB7_11
# BB#2:                                 # %min.iters.checked
	movq	%r10, %r8
	andq	$-4, %r8
	movq	%r10, %r9
	andq	$-4, %r9
	je	.LBB7_11
# BB#3:                                 # %vector.body.preheader
	leaq	-4(%r9), %rdi
	movq	%rdi, %rdx
	shrq	$2, %rdx
	btl	$2, %edi
	jb	.LBB7_4
# BB#5:                                 # %vector.body.prol
	movups	8(%rsi,%rcx,8), %xmm0
	movups	24(%rsi,%rcx,8), %xmm1
	movups	%xmm0, (%rsi,%rcx,8)
	movups	%xmm1, 16(%rsi,%rcx,8)
	movl	$4, %r11d
	testq	%rdx, %rdx
	jne	.LBB7_7
	jmp	.LBB7_9
.LBB7_4:
	xorl	%r11d, %r11d
	testq	%rdx, %rdx
	je	.LBB7_9
.LBB7_7:                                # %vector.body.preheader.new
	movq	%r9, %rdi
	subq	%r11, %rdi
	addq	%rcx, %r11
	leaq	56(%rsi,%r11,8), %rdx
	.p2align	4, 0x90
.LBB7_8:                                # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rdx), %xmm0
	movups	-32(%rdx), %xmm1
	movups	%xmm0, -56(%rdx)
	movups	%xmm1, -40(%rdx)
	movups	-16(%rdx), %xmm0
	movups	(%rdx), %xmm1
	movups	%xmm0, -24(%rdx)
	movups	%xmm1, -8(%rdx)
	addq	$64, %rdx
	addq	$-8, %rdi
	jne	.LBB7_8
.LBB7_9:                                # %middle.block
	cmpq	%r9, %r10
	je	.LBB7_12
# BB#10:
	addq	%r8, %rcx
	.p2align	4, 0x90
.LBB7_11:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rsi,%rcx,8), %rdx
	movq	%rdx, (%rsi,%rcx,8)
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB7_11
.LBB7_12:                               # %._crit_edge
	retq
.Lfunc_end7:
	.size	delete_arg, .Lfunc_end7-delete_arg
	.cfi_endproc

	.globl	check_arg
	.p2align	4, 0x90
	.type	check_arg,@function
check_arg:                              # @check_arg
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi57:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi58:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi59:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi61:
	.cfi_def_cfa_offset 80
.Lcfi62:
	.cfi_offset %rbx, -56
.Lcfi63:
	.cfi_offset %r12, -48
.Lcfi64:
	.cfi_offset %r13, -40
.Lcfi65:
	.cfi_offset %r14, -32
.Lcfi66:
	.cfi_offset %r15, -24
.Lcfi67:
	.cfi_offset %rbp, -16
	movq	%rdx, %rcx
	movslq	(%rdi), %r13
	xorl	%eax, %eax
	cmpq	$2, %r13
	jl	.LBB8_17
# BB#1:                                 # %.lr.ph.preheader
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	$-1, %rdx
	movl	$2, %ebp
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_2:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movq	%rsi, %r12
	movq	8(%rsi,%rbx,8), %rdi
	movq	%rcx, %r15
	movq	%rcx, %rsi
	callq	strcmp
	testl	%eax, %eax
	je	.LBB8_3
# BB#15:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%rbp
	movq	8(%rsp), %rdx           # 8-byte Reload
	decq	%rdx
	addq	$-8, %r14
	leaq	1(%rbx), %rax
	addq	$2, %rbx
	cmpq	%r13, %rbx
	movq	%rax, %rbx
	movq	%r12, %rsi
	movq	%r15, %rcx
	jl	.LBB8_2
# BB#16:
	xorl	%eax, %eax
	jmp	.LBB8_17
.LBB8_3:
	leaq	1(%rbx), %rsi
	decl	%r13d
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	%r13d, (%rax)
	movl	$1, %eax
	cmpl	%esi, %r13d
	jle	.LBB8_17
# BB#4:                                 # %.lr.ph.i
	movslq	%r13d, %rcx
	cmpq	%rcx, %rbp
	movq	%rcx, %rdx
	cmovgeq	%rbp, %rdx
	decq	%rdx
	subq	%rbx, %rdx
	cmpq	$4, %rdx
	jb	.LBB8_14
# BB#5:                                 # %min.iters.checked
	cmpq	%rcx, %rbp
	movq	%rcx, %r8
	cmovgeq	%rbp, %r8
	movq	%rdx, %r10
	andq	$-4, %r10
	je	.LBB8_14
# BB#6:                                 # %vector.body.preheader
	leaq	-4(%r10), %rsi
	movq	%rsi, %r11
	shrq	$2, %r11
	btl	$2, %esi
	jb	.LBB8_7
# BB#8:                                 # %vector.body.prol
	movups	16(%r12,%rbx,8), %xmm0
	movups	32(%r12,%rbx,8), %xmm1
	movups	%xmm0, 8(%r12,%rbx,8)
	movups	%xmm1, 24(%r12,%rbx,8)
	movl	$4, %r9d
	testq	%r11, %r11
	jne	.LBB8_10
	jmp	.LBB8_12
.LBB8_7:
	xorl	%r9d, %r9d
	testq	%r11, %r11
	je	.LBB8_12
.LBB8_10:                               # %vector.body.preheader.new
	cmpq	%rcx, %rbp
	cmovlq	%rcx, %rbp
	decq	%rbp
	subq	%rbx, %rbp
	andq	$-4, %rbp
	subq	%r9, %rbp
	leaq	64(%r12,%r9,8), %rsi
	subq	%r14, %rsi
	.p2align	4, 0x90
.LBB8_11:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-48(%rsi), %xmm0
	movups	-32(%rsi), %xmm1
	movups	%xmm0, -56(%rsi)
	movups	%xmm1, -40(%rsi)
	movups	-16(%rsi), %xmm0
	movups	(%rsi), %xmm1
	movups	%xmm0, -24(%rsi)
	movups	%xmm1, -8(%rsi)
	addq	$64, %rsi
	addq	$-8, %rbp
	jne	.LBB8_11
.LBB8_12:                               # %middle.block
	cmpq	%r10, %rdx
	je	.LBB8_17
# BB#13:
	addq	8(%rsp), %r8            # 8-byte Folded Reload
	andq	$-4, %r8
	leaq	1(%r8,%rbx), %rsi
	.p2align	4, 0x90
.LBB8_14:                               # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%r12,%rsi,8), %rdx
	movq	%rdx, (%r12,%rsi,8)
	incq	%rsi
	cmpq	%rcx, %rsi
	jl	.LBB8_14
.LBB8_17:                               # %delete_arg.exit
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	check_arg, .Lfunc_end8-check_arg
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"ESPRESSO"
	.size	.L.str, 9

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"many"
	.size	.L.str.1, 5

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"exact"
	.size	.L.str.2, 6

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"qm"
	.size	.L.str.3, 3

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"single_output"
	.size	.L.str.4, 14

	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"so"
	.size	.L.str.5, 3

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"so_both"
	.size	.L.str.6, 8

	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"simplify"
	.size	.L.str.7, 9

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"echo"
	.size	.L.str.8, 5

	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"opo"
	.size	.L.str.9, 4

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"opoall"
	.size	.L.str.10, 7

	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"pair"
	.size	.L.str.11, 5

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"pairall"
	.size	.L.str.12, 8

	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"check"
	.size	.L.str.13, 6

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"stats"
	.size	.L.str.14, 6

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"verify"
	.size	.L.str.15, 7

	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"PLAverify"
	.size	.L.str.16, 10

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"equiv"
	.size	.L.str.17, 6

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"map"
	.size	.L.str.18, 4

	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"mapdc"
	.size	.L.str.19, 6

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"fsm"
	.size	.L.str.20, 4

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"contain"
	.size	.L.str.21, 8

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"d1merge"
	.size	.L.str.22, 8

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"d1merge_in"
	.size	.L.str.23, 11

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"disjoint"
	.size	.L.str.24, 9

	.type	.L.str.25,@object       # @.str.25
.L.str.25:
	.asciz	"dsharp"
	.size	.L.str.25, 7

	.type	.L.str.26,@object       # @.str.26
.L.str.26:
	.asciz	"intersect"
	.size	.L.str.26, 10

	.type	.L.str.27,@object       # @.str.27
.L.str.27:
	.asciz	"minterms"
	.size	.L.str.27, 9

	.type	.L.str.28,@object       # @.str.28
.L.str.28:
	.asciz	"primes"
	.size	.L.str.28, 7

	.type	.L.str.29,@object       # @.str.29
.L.str.29:
	.asciz	"separate"
	.size	.L.str.29, 9

	.type	.L.str.30,@object       # @.str.30
.L.str.30:
	.asciz	"sharp"
	.size	.L.str.30, 6

	.type	.L.str.31,@object       # @.str.31
.L.str.31:
	.asciz	"union"
	.size	.L.str.31, 6

	.type	.L.str.32,@object       # @.str.32
.L.str.32:
	.asciz	"xor"
	.size	.L.str.32, 4

	.type	.L.str.33,@object       # @.str.33
.L.str.33:
	.asciz	"essen"
	.size	.L.str.33, 6

	.type	.L.str.34,@object       # @.str.34
.L.str.34:
	.asciz	"expand"
	.size	.L.str.34, 7

	.type	.L.str.35,@object       # @.str.35
.L.str.35:
	.asciz	"gasp"
	.size	.L.str.35, 5

	.type	.L.str.36,@object       # @.str.36
.L.str.36:
	.asciz	"irred"
	.size	.L.str.36, 6

	.type	.L.str.37,@object       # @.str.37
.L.str.37:
	.asciz	"make_sparse"
	.size	.L.str.37, 12

	.type	.L.str.38,@object       # @.str.38
.L.str.38:
	.asciz	"reduce"
	.size	.L.str.38, 7

	.type	.L.str.39,@object       # @.str.39
.L.str.39:
	.asciz	"taut"
	.size	.L.str.39, 5

	.type	.L.str.40,@object       # @.str.40
.L.str.40:
	.asciz	"super_gasp"
	.size	.L.str.40, 11

	.type	.L.str.41,@object       # @.str.41
.L.str.41:
	.asciz	"lexsort"
	.size	.L.str.41, 8

	.type	.L.str.42,@object       # @.str.42
.L.str.42:
	.asciz	"test"
	.size	.L.str.42, 5

	.type	option_table,@object    # @option_table
	.data
	.globl	option_table
	.p2align	4
option_table:
	.quad	.L.str
	.long	0                       # 0x0
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.1
	.long	37                      # 0x25
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.2
	.long	9                       # 0x9
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.3
	.long	24                      # 0x18
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.4
	.long	28                      # 0x1c
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.5
	.long	28                      # 0x1c
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.6
	.long	29                      # 0x1d
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.7
	.long	27                      # 0x1b
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.8
	.long	7                       # 0x7
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.9
	.long	19                      # 0x13
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.10
	.long	20                      # 0x14
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.11
	.long	21                      # 0x15
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.12
	.long	22                      # 0x16
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.13
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.14
	.long	30                      # 0x1e
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.15
	.long	36                      # 0x24
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	.L.str.16
	.long	1                       # 0x1
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	.L.str.17
	.long	34                      # 0x22
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.18
	.long	16                      # 0x10
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.19
	.long	17                      # 0x11
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.20
	.long	41                      # 0x29
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	.L.str.21
	.long	3                       # 0x3
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.22
	.long	4                       # 0x4
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.23
	.long	40                      # 0x28
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.24
	.long	5                       # 0x5
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.quad	.L.str.25
	.long	6                       # 0x6
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.26
	.long	12                      # 0xc
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.27
	.long	18                      # 0x12
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.28
	.long	23                      # 0x17
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	.L.str.29
	.long	38                      # 0x26
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.30
	.long	26                      # 0x1a
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.31
	.long	35                      # 0x23
	.long	2                       # 0x2
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.32
	.long	39                      # 0x27
	.long	2                       # 0x2
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.33
	.long	8                       # 0x8
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	.L.str.34
	.long	10                      # 0xa
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	0                       # 0x0
	.quad	.L.str.35
	.long	11                      # 0xb
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.36
	.long	13                      # 0xd
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	.L.str.37
	.long	15                      # 0xf
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.38
	.long	25                      # 0x19
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	1                       # 0x1
	.quad	.L.str.39
	.long	32                      # 0x20
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.40
	.long	31                      # 0x1f
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	.L.str.41
	.long	14                      # 0xe
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	0                       # 0x0
	.quad	.L.str.42
	.long	33                      # 0x21
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.quad	0
	.long	42                      # 0x2a
	.long	0                       # 0x0
	.long	0                       # 0x0
	.long	0                       # 0x0
	.size	option_table, 1056

	.type	.L.str.43,@object       # @.str.43
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.43:
	.zero	1
	.size	.L.str.43, 1

	.type	.L.str.44,@object       # @.str.44
.L.str.44:
	.asciz	"compl"
	.size	.L.str.44, 6

	.type	.L.str.45,@object       # @.str.45
.L.str.45:
	.asciz	"expand1"
	.size	.L.str.45, 8

	.type	.L.str.46,@object       # @.str.46
.L.str.46:
	.asciz	"irred1"
	.size	.L.str.46, 7

	.type	.L.str.47,@object       # @.str.47
.L.str.47:
	.asciz	"reduce1"
	.size	.L.str.47, 8

	.type	.L.str.48,@object       # @.str.48
.L.str.48:
	.asciz	"mincov"
	.size	.L.str.48, 7

	.type	.L.str.49,@object       # @.str.49
.L.str.49:
	.asciz	"mincov1"
	.size	.L.str.49, 8

	.type	.L.str.50,@object       # @.str.50
.L.str.50:
	.asciz	"sparse"
	.size	.L.str.50, 7

	.type	debug_table,@object     # @debug_table
	.data
	.globl	debug_table
	.p2align	4
debug_table:
	.quad	.L.str.43
	.long	10614                   # 0x2976
	.zero	4
	.quad	.L.str.44
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.33
	.long	2                       # 0x2
	.zero	4
	.quad	.L.str.34
	.long	4                       # 0x4
	.zero	4
	.quad	.L.str.45
	.long	12                      # 0xc
	.zero	4
	.quad	.L.str.36
	.long	32                      # 0x20
	.zero	4
	.quad	.L.str.46
	.long	16416                   # 0x4020
	.zero	4
	.quad	.L.str.38
	.long	64                      # 0x40
	.zero	4
	.quad	.L.str.47
	.long	192                     # 0xc0
	.zero	4
	.quad	.L.str.48
	.long	2048                    # 0x800
	.zero	4
	.quad	.L.str.49
	.long	6144                    # 0x1800
	.zero	4
	.quad	.L.str.50
	.long	256                     # 0x100
	.zero	4
	.quad	.L.str.30
	.long	8192                    # 0x2000
	.zero	4
	.quad	.L.str.39
	.long	512                     # 0x200
	.zero	4
	.quad	.L.str.35
	.long	16                      # 0x10
	.zero	4
	.quad	.L.str.2
	.long	1024                    # 0x400
	.zero	4
	.zero	16
	.size	debug_table, 272

	.type	.L.str.51,@object       # @.str.51
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.51:
	.asciz	"eat"
	.size	.L.str.51, 4

	.type	.L.str.52,@object       # @.str.52
.L.str.52:
	.asciz	"eatdots"
	.size	.L.str.52, 8

	.type	.L.str.53,@object       # @.str.53
.L.str.53:
	.asciz	"fast"
	.size	.L.str.53, 5

	.type	.L.str.54,@object       # @.str.54
.L.str.54:
	.asciz	"kiss"
	.size	.L.str.54, 5

	.type	.L.str.55,@object       # @.str.55
.L.str.55:
	.asciz	"ness"
	.size	.L.str.55, 5

	.type	.L.str.56,@object       # @.str.56
.L.str.56:
	.asciz	"nirr"
	.size	.L.str.56, 5

	.type	.L.str.57,@object       # @.str.57
.L.str.57:
	.asciz	"nunwrap"
	.size	.L.str.57, 8

	.type	.L.str.58,@object       # @.str.58
.L.str.58:
	.asciz	"onset"
	.size	.L.str.58, 6

	.type	.L.str.59,@object       # @.str.59
.L.str.59:
	.asciz	"pos"
	.size	.L.str.59, 4

	.type	.L.str.60,@object       # @.str.60
.L.str.60:
	.asciz	"random"
	.size	.L.str.60, 7

	.type	.L.str.61,@object       # @.str.61
.L.str.61:
	.asciz	"strong"
	.size	.L.str.61, 7

	.type	esp_opt_table,@object   # @esp_opt_table
	.data
	.globl	esp_opt_table
	.p2align	4
esp_opt_table:
	.quad	.L.str.51
	.quad	echo_comments
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.52
	.quad	echo_unknown_commands
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.53
	.quad	single_expand
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.54
	.quad	kiss
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.55
	.quad	remove_essential
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.56
	.quad	force_irredundant
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.57
	.quad	unwrap_onset
	.long	0                       # 0x0
	.zero	4
	.quad	.L.str.58
	.quad	recompute_onset
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.59
	.quad	pos
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.60
	.quad	use_random_order
	.long	1                       # 0x1
	.zero	4
	.quad	.L.str.61
	.quad	use_super_gasp
	.long	1                       # 0x1
	.zero	4
	.zero	24
	.size	esp_opt_table, 288

	.type	.L.str.62,@object       # @.str.62
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.62:
	.asciz	"D:S:de:o:r:stv:x"
	.size	.L.str.62, 17

	.type	.L.str.63,@object       # @.str.63
.L.str.63:
	.asciz	"%s: bad subcommand \"%s\"\n"
	.size	.L.str.63, 25

	.type	.L.str.64,@object       # @.str.64
.L.str.64:
	.asciz	"%s: bad output type \"%s\"\n"
	.size	.L.str.64, 26

	.type	.L.str.65,@object       # @.str.65
.L.str.65:
	.asciz	"%s: bad espresso option \"%s\"\n"
	.size	.L.str.65, 30

	.type	.L.str.66,@object       # @.str.66
.L.str.66:
	.asciz	"%s: bad debug type \"%s\"\n"
	.size	.L.str.66, 25

	.type	.L.str.67,@object       # @.str.67
.L.str.67:
	.asciz	"%d-%d"
	.size	.L.str.67, 6

	.type	.L.str.68,@object       # @.str.68
.L.str.68:
	.asciz	"%s: bad output range \"%s\"\n"
	.size	.L.str.68, 27

	.type	.L.str.69,@object       # @.str.69
.L.str.69:
	.asciz	"# espresso"
	.size	.L.str.69, 11

	.type	.L.str.70,@object       # @.str.70
.L.str.70:
	.asciz	" %s"
	.size	.L.str.70, 4

	.type	.L.str.72,@object       # @.str.72
.L.str.72:
	.asciz	"# %s\n"
	.size	.L.str.72, 6

	.type	.L.str.73,@object       # @.str.73
.L.str.73:
	.asciz	"UC Berkeley, Espresso Version #2.3, Release date 01/31/88"
	.size	.L.str.73, 58

	.type	.L.str.74,@object       # @.str.74
.L.str.74:
	.asciz	"trailing arguments on command line"
	.size	.L.str.74, 35

	.type	.L.str.75,@object       # @.str.75
.L.str.75:
	.asciz	"ESPRESSO   "
	.size	.L.str.75, 12

	.type	last_fp,@object         # @last_fp
	.local	last_fp
	.comm	last_fp,8,8
	.type	.L.str.76,@object       # @.str.76
.L.str.76:
	.asciz	"SIMPLIFY  "
	.size	.L.str.76, 11

	.type	.L.str.77,@object       # @.str.77
.L.str.77:
	.asciz	"PRIMES     "
	.size	.L.str.77, 12

	.type	.L.str.78,@object       # @.str.78
.L.str.78:
	.asciz	"ON-set is%sa tautology\n"
	.size	.L.str.78, 24

	.type	.L.str.79,@object       # @.str.79
.L.str.79:
	.asciz	" "
	.size	.L.str.79, 2

	.type	.L.str.80,@object       # @.str.80
.L.str.80:
	.asciz	" not "
	.size	.L.str.80, 6

	.type	.L.str.83,@object       # @.str.83
.L.str.83:
	.asciz	"cover verification failed"
	.size	.L.str.83, 26

	.type	.L.str.84,@object       # @.str.84
.L.str.84:
	.asciz	"(stdin)"
	.size	.L.str.84, 8

	.type	.L.str.85,@object       # @.str.85
.L.str.85:
	.asciz	"-"
	.size	.L.str.85, 2

	.type	.L.str.86,@object       # @.str.86
.L.str.86:
	.asciz	"r"
	.size	.L.str.86, 2

	.type	.L.str.87,@object       # @.str.87
.L.str.87:
	.asciz	"%s: Unable to open %s\n"
	.size	.L.str.87, 23

	.type	input_type,@object      # @input_type
	.data
	.p2align	2
input_type:
	.long	3                       # 0x3
	.size	input_type, 4

	.type	.L.str.88,@object       # @.str.88
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.88:
	.asciz	"%s: Unable to find PLA on file %s\n"
	.size	.L.str.88, 35

	.type	.L.str.89,@object       # @.str.89
.L.str.89:
	.asciz	"# %s\t%2d call(s) for %s (%2ld.%01ld%%)\n"
	.size	.L.str.89, 40

	.type	.L.str.90,@object       # @.str.90
.L.str.90:
	.asciz	"READ       "
	.size	.L.str.90, 12

	.type	.L.str.91,@object       # @.str.91
.L.str.91:
	.asciz	"WRITE      "
	.size	.L.str.91, 12

	.type	.L.str.92,@object       # @.str.92
.L.str.92:
	.asciz	"COMPL      "
	.size	.L.str.92, 12

	.type	.L.str.93,@object       # @.str.93
.L.str.93:
	.asciz	"REDUCE     "
	.size	.L.str.93, 12

	.type	.L.str.94,@object       # @.str.94
.L.str.94:
	.asciz	"EXPAND     "
	.size	.L.str.94, 12

	.type	.L.str.95,@object       # @.str.95
.L.str.95:
	.asciz	"ESSEN      "
	.size	.L.str.95, 12

	.type	.L.str.96,@object       # @.str.96
.L.str.96:
	.asciz	"IRRED      "
	.size	.L.str.96, 12

	.type	.L.str.97,@object       # @.str.97
.L.str.97:
	.asciz	"REDUCE_GASP"
	.size	.L.str.97, 12

	.type	.L.str.98,@object       # @.str.98
.L.str.98:
	.asciz	"EXPAND_GASP"
	.size	.L.str.98, 12

	.type	.L.str.99,@object       # @.str.99
.L.str.99:
	.asciz	"IRRED_GASP "
	.size	.L.str.99, 12

	.type	.L.str.100,@object      # @.str.100
.L.str.100:
	.asciz	"MV_REDUCE  "
	.size	.L.str.100, 12

	.type	.L.str.101,@object      # @.str.101
.L.str.101:
	.asciz	"RAISE_IN   "
	.size	.L.str.101, 12

	.type	.L.str.102,@object      # @.str.102
.L.str.102:
	.asciz	"VERIFY     "
	.size	.L.str.102, 12

	.type	.L.str.103,@object      # @.str.103
.L.str.103:
	.asciz	"MINCOV     "
	.size	.L.str.103, 12

	.type	.L.str.104,@object      # @.str.104
.L.str.104:
	.asciz	"                "
	.size	.L.str.104, 17

	.type	.L.str.105,@object      # @.str.105
.L.str.105:
	.asciz	",\n                "
	.size	.L.str.105, 19

	.type	.L.str.106,@object      # @.str.106
.L.str.106:
	.asciz	", "
	.size	.L.str.106, 3

	.type	.L.str.107,@object      # @.str.107
.L.str.107:
	.asciz	"%s"
	.size	.L.str.107, 3

	.type	.L.str.108,@object      # @.str.108
.L.str.108:
	.asciz	"%s\n\n"
	.size	.L.str.108, 5

	.type	.L.str.132,@object      # @.str.132
.L.str.132:
	.asciz	"-do"
	.size	.L.str.132, 4

	.type	.L.str.133,@object      # @.str.133
.L.str.133:
	.asciz	"espresso: bad keyword \"%s\" following -do\n"
	.size	.L.str.133, 42

	.type	.L.str.134,@object      # @.str.134
.L.str.134:
	.asciz	"-out"
	.size	.L.str.134, 5

	.type	.L.str.135,@object      # @.str.135
.L.str.135:
	.asciz	"espresso: bad keyword \"%s\" following -out\n"
	.size	.L.str.135, 43

	.type	.L.str.136,@object      # @.str.136
.L.str.136:
	.asciz	"-fdr"
	.size	.L.str.136, 5

	.type	.L.str.137,@object      # @.str.137
.L.str.137:
	.asciz	"-fr"
	.size	.L.str.137, 4

	.type	.L.str.138,@object      # @.str.138
.L.str.138:
	.asciz	"-f"
	.size	.L.str.138, 3

	.type	.Lstr.2,@object         # @str.2
	.section	.rodata.str1.16,"aMS",@progbits,1
	.p2align	4
.Lstr.2:
	.asciz	"PLA's compared equal"
	.size	.Lstr.2, 21

	.type	.Lstr.3,@object         # @str.3
	.p2align	4
.Lstr.3:
	.asciz	"PLA comparison failed; the PLA's are not equivalent"
	.size	.Lstr.3, 52

	.type	.Lstr.4,@object         # @str.4
	.p2align	4
.Lstr.4:
	.asciz	"SYNOPSIS: espresso [options] [file]\n"
	.size	.Lstr.4, 37

	.type	.Lstr.5,@object         # @str.5
	.p2align	4
.Lstr.5:
	.asciz	"  -d        Enable debugging"
	.size	.Lstr.5, 29

	.type	.Lstr.6,@object         # @str.6
	.p2align	4
.Lstr.6:
	.asciz	"  -e[opt]   Select espresso option:"
	.size	.Lstr.6, 36

	.type	.Lstr.7,@object         # @str.7
	.p2align	4
.Lstr.7:
	.asciz	"                fast, ness, nirr, nunwrap, onset, pos, strong,"
	.size	.Lstr.7, 63

	.type	.Lstr.8,@object         # @str.8
	.p2align	4
.Lstr.8:
	.asciz	"                eat, eatdots, kiss, random"
	.size	.Lstr.8, 43

	.type	.Lstr.9,@object         # @str.9
	.p2align	4
.Lstr.9:
	.asciz	"  -o[type]  Select output format:"
	.size	.Lstr.9, 34

	.type	.Lstr.10,@object        # @str.10
	.p2align	4
.Lstr.10:
	.asciz	"                f, fd, fr, fdr, pleasure, eqntott, kiss, cons"
	.size	.Lstr.10, 62

	.type	.Lstr.11,@object        # @str.11
	.p2align	4
.Lstr.11:
	.asciz	"  -rn-m     Select range for subcommands:"
	.size	.Lstr.11, 42

	.type	.Lstr.12,@object        # @str.12
	.p2align	4
.Lstr.12:
	.asciz	"                d1merge: first and last variables (0 ... m-1)"
	.size	.Lstr.12, 62

	.type	.Lstr.13,@object        # @str.13
	.p2align	4
.Lstr.13:
	.asciz	"                minterms: first and last variables (0 ... m-1)"
	.size	.Lstr.13, 63

	.type	.Lstr.14,@object        # @str.14
	.p2align	4
.Lstr.14:
	.asciz	"                opoall: first and last outputs (0 ... m-1)"
	.size	.Lstr.14, 59

	.type	.Lstr.15,@object        # @str.15
	.p2align	4
.Lstr.15:
	.asciz	"  -s        Provide short execution summary"
	.size	.Lstr.15, 44

	.type	.Lstr.16,@object        # @str.16
	.p2align	4
.Lstr.16:
	.asciz	"  -t        Provide longer execution trace"
	.size	.Lstr.16, 43

	.type	.Lstr.17,@object        # @str.17
	.p2align	4
.Lstr.17:
	.asciz	"  -x        Suppress printing of solution"
	.size	.Lstr.17, 42

	.type	.Lstr.18,@object        # @str.18
	.p2align	4
.Lstr.18:
	.asciz	"  -v[type]  Verbose debugging detail (-v '' for all)"
	.size	.Lstr.18, 53

	.type	.Lstr.19,@object        # @str.19
	.p2align	4
.Lstr.19:
	.asciz	"  -D[cmd]   Execute subcommand 'cmd':"
	.size	.Lstr.19, 38

	.type	.Lstr.20,@object        # @str.20
	.p2align	4
.Lstr.20:
	.asciz	"  -Sn       Select strategy for subcommands:"
	.size	.Lstr.20, 45

	.type	.Lstr.21,@object        # @str.21
	.p2align	4
.Lstr.21:
	.asciz	"                opo: bit2=exact bit1=repeated bit0=skip sparse"
	.size	.Lstr.21, 63

	.type	.Lstr.22,@object        # @str.22
	.p2align	4
.Lstr.22:
	.asciz	"                opoall: 0=minimize, 1=exact"
	.size	.Lstr.22, 44

	.type	.Lstr.23,@object        # @str.23
	.p2align	4
.Lstr.23:
	.asciz	"                pair: 0=algebraic, 1=strongd, 2=espresso, 3=exact"
	.size	.Lstr.23, 66

	.type	.Lstr.24,@object        # @str.24
	.p2align	4
.Lstr.24:
	.asciz	"                pairall: 0=minimize, 1=exact, 2=opo"
	.size	.Lstr.24, 52

	.type	.Lstr.25,@object        # @str.25
	.p2align	4
.Lstr.25:
	.asciz	"                so_espresso: 0=minimize, 1=exact"
	.size	.Lstr.25, 49

	.type	.Lstr.26,@object        # @str.26
	.p2align	4
.Lstr.26:
	.asciz	"                so_both: 0=minimize, 1=exact"
	.size	.Lstr.26, 45


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
