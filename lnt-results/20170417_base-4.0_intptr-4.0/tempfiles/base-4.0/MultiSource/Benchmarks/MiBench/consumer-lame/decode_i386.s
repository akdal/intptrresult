	.text
	.file	"decode_i386.bc"
	.globl	synth_1to1_mono
	.p2align	4, 0x90
	.type	synth_1to1_mono,@function
synth_1to1_mono:                        # @synth_1to1_mono
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	subq	$152, %rsp
.Lcfi2:
	.cfi_def_cfa_offset 176
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdx, %r14
	movq	%rsi, %rbx
	movl	$0, 12(%rsp)
	leaq	16(%rsp), %rdx
	leaq	12(%rsp), %rcx
	xorl	%esi, %esi
	callq	synth_1to1
	movslq	(%r14), %rcx
	movzwl	16(%rsp), %edx
	movw	%dx, (%rbx,%rcx)
	movzwl	20(%rsp), %edx
	movw	%dx, 2(%rbx,%rcx)
	movzwl	24(%rsp), %edx
	movw	%dx, 4(%rbx,%rcx)
	movzwl	28(%rsp), %edx
	movw	%dx, 6(%rbx,%rcx)
	movzwl	32(%rsp), %edx
	movw	%dx, 8(%rbx,%rcx)
	movzwl	36(%rsp), %edx
	movw	%dx, 10(%rbx,%rcx)
	movzwl	40(%rsp), %edx
	movw	%dx, 12(%rbx,%rcx)
	movzwl	44(%rsp), %edx
	movw	%dx, 14(%rbx,%rcx)
	movzwl	48(%rsp), %edx
	movw	%dx, 16(%rbx,%rcx)
	movzwl	52(%rsp), %edx
	movw	%dx, 18(%rbx,%rcx)
	movzwl	56(%rsp), %edx
	movw	%dx, 20(%rbx,%rcx)
	movzwl	60(%rsp), %edx
	movw	%dx, 22(%rbx,%rcx)
	movzwl	64(%rsp), %edx
	movw	%dx, 24(%rbx,%rcx)
	movzwl	68(%rsp), %edx
	movw	%dx, 26(%rbx,%rcx)
	movzwl	72(%rsp), %edx
	movw	%dx, 28(%rbx,%rcx)
	movzwl	76(%rsp), %edx
	movw	%dx, 30(%rbx,%rcx)
	movzwl	80(%rsp), %edx
	movw	%dx, 32(%rbx,%rcx)
	movzwl	84(%rsp), %edx
	movw	%dx, 34(%rbx,%rcx)
	movzwl	88(%rsp), %edx
	movw	%dx, 36(%rbx,%rcx)
	movzwl	92(%rsp), %edx
	movw	%dx, 38(%rbx,%rcx)
	movzwl	96(%rsp), %edx
	movw	%dx, 40(%rbx,%rcx)
	movzwl	100(%rsp), %edx
	movw	%dx, 42(%rbx,%rcx)
	movzwl	104(%rsp), %edx
	movw	%dx, 44(%rbx,%rcx)
	movzwl	108(%rsp), %edx
	movw	%dx, 46(%rbx,%rcx)
	movzwl	112(%rsp), %edx
	movw	%dx, 48(%rbx,%rcx)
	movzwl	116(%rsp), %edx
	movw	%dx, 50(%rbx,%rcx)
	movzwl	120(%rsp), %edx
	movw	%dx, 52(%rbx,%rcx)
	movzwl	124(%rsp), %edx
	movw	%dx, 54(%rbx,%rcx)
	movzwl	128(%rsp), %edx
	movw	%dx, 56(%rbx,%rcx)
	movzwl	132(%rsp), %edx
	movw	%dx, 58(%rbx,%rcx)
	movzwl	136(%rsp), %edx
	movw	%dx, 60(%rbx,%rcx)
	movzwl	140(%rsp), %edx
	movw	%dx, 62(%rbx,%rcx)
	addl	$64, (%r14)
	addq	$152, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	synth_1to1_mono, .Lfunc_end0-synth_1to1_mono
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4674736138332667904     # double 32767
.LCPI1_1:
	.quad	-4548635623644200960    # double -32768
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_2:
	.quad	-9223372036854775808    # double -0
	.quad	-9223372036854775808    # double -0
	.text
	.globl	synth_1to1
	.p2align	4, 0x90
	.type	synth_1to1,@function
synth_1to1:                             # @synth_1to1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi7:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi8:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi9:
	.cfi_def_cfa_offset 48
.Lcfi10:
	.cfi_offset %rbx, -48
.Lcfi11:
	.cfi_offset %r12, -40
.Lcfi12:
	.cfi_offset %r14, -32
.Lcfi13:
	.cfi_offset %r15, -24
.Lcfi14:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rdi, %r8
	movslq	(%r14), %rbp
	leaq	(%rdx,%rbp), %rbx
	movq	gmp(%rip), %rcx
	movl	31872(%rcx), %eax
	leal	15(%rax), %r12d
	andl	$15, %r12d
	xorl	%edi, %edi
	testl	%esi, %esi
	leaq	2(%rdx,%rbp), %r15
	setne	%dil
	cmovnel	%eax, %r12d
	cmoveq	%rbx, %r15
	imulq	$4352, %rdi, %rax       # imm = 0x1100
	testb	$1, %r12b
	jne	.LBB1_1
# BB#2:
	leaq	25344(%rcx,%rax), %rbx
	addq	%rax, %rcx
	leal	1(%r12), %ebp
	movslq	%r12d, %rax
	leaq	23168(%rcx,%rax,8), %rdi
	leaq	25352(%rcx,%rax,8), %rsi
	movq	%r8, %rdx
	callq	dct64
	jmp	.LBB1_3
.LBB1_1:
	leaq	23168(%rcx,%rax), %rbx
	addq	%rax, %rcx
	leal	1(%r12), %eax
	andl	$15, %eax
	leaq	25344(%rcx,%rax,8), %rdi
	movslq	%r12d, %rax
	leaq	23168(%rcx,%rax,8), %rsi
	movq	%r8, %rdx
	callq	dct64
	movl	%r12d, %ebp
.LBB1_3:
	movq	gmp(%rip), %rax
	movl	%r12d, 31872(%rax)
	movslq	%ebp, %r8
	leaq	(,%r8,8), %rax
	negq	%rax
	movsd	decwin+128(,%rax), %xmm2 # xmm2 = mem[0],zero
	mulsd	(%rbx), %xmm2
	movl	$496, %edx              # imm = 0x1F0
	subq	%r8, %rdx
	movl	$17, %esi
	subq	%r8, %rsi
	xorl	%edi, %edi
	shlq	$3, %rsi
	movsd	.LCPI1_0(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI1_1(%rip), %xmm1   # xmm1 = mem[0],zero
	xorl	%eax, %eax
	movq	%r15, %rcx
	.p2align	4, 0x90
.LBB1_4:                                # =>This Inner Loop Header: Depth=1
	movsd	decwin(%rsi,%rdi,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	8(%rbx,%rdi,2), %xmm3
	subsd	%xmm3, %xmm2
	movsd	decwin+8(%rsi,%rdi,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	16(%rbx,%rdi,2), %xmm3
	addsd	%xmm2, %xmm3
	movsd	decwin+16(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	24(%rbx,%rdi,2), %xmm2
	subsd	%xmm2, %xmm3
	movsd	decwin+24(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	32(%rbx,%rdi,2), %xmm2
	addsd	%xmm3, %xmm2
	movsd	decwin+32(%rsi,%rdi,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	40(%rbx,%rdi,2), %xmm3
	subsd	%xmm3, %xmm2
	movsd	decwin+40(%rsi,%rdi,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	48(%rbx,%rdi,2), %xmm3
	addsd	%xmm2, %xmm3
	movsd	decwin+48(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	56(%rbx,%rdi,2), %xmm2
	subsd	%xmm2, %xmm3
	movsd	decwin+56(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	64(%rbx,%rdi,2), %xmm2
	addsd	%xmm3, %xmm2
	movsd	decwin+64(%rsi,%rdi,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	72(%rbx,%rdi,2), %xmm3
	subsd	%xmm3, %xmm2
	movsd	decwin+72(%rsi,%rdi,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	80(%rbx,%rdi,2), %xmm3
	addsd	%xmm2, %xmm3
	movsd	decwin+80(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	88(%rbx,%rdi,2), %xmm2
	subsd	%xmm2, %xmm3
	movsd	decwin+88(%rsi,%rdi,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	96(%rbx,%rdi,2), %xmm4
	addsd	%xmm3, %xmm4
	movsd	decwin+96(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	104(%rbx,%rdi,2), %xmm2
	subsd	%xmm2, %xmm4
	movsd	decwin+104(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	112(%rbx,%rdi,2), %xmm2
	addsd	%xmm4, %xmm2
	movsd	decwin+112(%rsi,%rdi,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	120(%rbx,%rdi,2), %xmm3
	subsd	%xmm3, %xmm2
	ucomisd	%xmm0, %xmm2
	jbe	.LBB1_6
# BB#5:                                 #   in Loop: Header=BB1_4 Depth=1
	movw	$32767, (%rcx)          # imm = 0x7FFF
	incl	%eax
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_6:                                #   in Loop: Header=BB1_4 Depth=1
	ucomisd	%xmm2, %xmm1
	jbe	.LBB1_8
# BB#7:                                 #   in Loop: Header=BB1_4 Depth=1
	movw	$-32768, (%rcx)         # imm = 0x8000
	incl	%eax
	jmp	.LBB1_9
	.p2align	4, 0x90
.LBB1_8:                                #   in Loop: Header=BB1_4 Depth=1
	cvttsd2si	%xmm2, %ebp
	movw	%bp, (%rcx)
.LBB1_9:                                #   in Loop: Header=BB1_4 Depth=1
	addq	$4, %rcx
	movsd	decwin+248(%rsi,%rdi,4), %xmm2 # xmm2 = mem[0],zero
	mulsd	128(%rbx,%rdi,2), %xmm2
	addq	$64, %rdi
	cmpl	$1024, %edi             # imm = 0x400
	jne	.LBB1_4
# BB#10:
	movsd	decwin+272(,%rdx,8), %xmm3 # xmm3 = mem[0],zero
	mulsd	2064(%rbx), %xmm3
	addsd	%xmm3, %xmm2
	movsd	decwin+288(,%rdx,8), %xmm3 # xmm3 = mem[0],zero
	mulsd	2080(%rbx), %xmm3
	addsd	%xmm2, %xmm3
	movsd	decwin+304(,%rdx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	2096(%rbx), %xmm2
	addsd	%xmm3, %xmm2
	movsd	decwin+320(,%rdx,8), %xmm3 # xmm3 = mem[0],zero
	mulsd	2112(%rbx), %xmm3
	addsd	%xmm2, %xmm3
	movsd	decwin+336(,%rdx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	2128(%rbx), %xmm2
	addsd	%xmm3, %xmm2
	movsd	decwin+352(,%rdx,8), %xmm3 # xmm3 = mem[0],zero
	mulsd	2144(%rbx), %xmm3
	addsd	%xmm2, %xmm3
	movsd	decwin+368(,%rdx,8), %xmm2 # xmm2 = mem[0],zero
	mulsd	2160(%rbx), %xmm2
	addsd	%xmm3, %xmm2
	ucomisd	.LCPI1_0(%rip), %xmm2
	jbe	.LBB1_12
# BB#11:
	movw	$32767, 64(%r15)        # imm = 0x7FFF
	incl	%eax
	jmp	.LBB1_15
.LBB1_12:
	ucomisd	%xmm2, %xmm1
	jbe	.LBB1_14
# BB#13:
	movw	$-32768, 64(%r15)       # imm = 0x8000
	incl	%eax
	jmp	.LBB1_15
.LBB1_14:
	cvttsd2si	%xmm2, %ecx
	movw	%cx, 64(%r15)
.LBB1_15:
	leal	(%r8,%r8), %ecx
	movslq	%ecx, %rdx
	addq	$68, %r15
	addq	$496, %rdx              # imm = 0x1F0
	subq	%r8, %rdx
	xorl	%ecx, %ecx
	shlq	$3, %rdx
	movapd	.LCPI1_2(%rip), %xmm2   # xmm2 = [-0.000000e+00,-0.000000e+00]
	.p2align	4, 0x90
.LBB1_16:                               # =>This Inner Loop Header: Depth=1
	movsd	decwin-8(%rdx,%rcx,4), %xmm3 # xmm3 = mem[0],zero
	mulsd	1920(%rbx,%rcx,2), %xmm3
	xorpd	%xmm2, %xmm3
	movsd	decwin-16(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1928(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-24(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1936(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-32(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1944(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-40(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1952(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-48(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1960(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-56(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1968(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-64(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1976(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-72(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1984(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-80(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	1992(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-88(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	2000(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-96(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	2008(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-104(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	2016(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-112(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	2024(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin-120(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	2032(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	movsd	decwin(%rdx,%rcx,4), %xmm4 # xmm4 = mem[0],zero
	mulsd	2040(%rbx,%rcx,2), %xmm4
	subsd	%xmm4, %xmm3
	ucomisd	%xmm0, %xmm3
	jbe	.LBB1_18
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	movw	$32767, (%r15)          # imm = 0x7FFF
	incl	%eax
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_18:                               #   in Loop: Header=BB1_16 Depth=1
	ucomisd	%xmm3, %xmm1
	jbe	.LBB1_20
# BB#19:                                #   in Loop: Header=BB1_16 Depth=1
	movw	$-32768, (%r15)         # imm = 0x8000
	incl	%eax
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_16 Depth=1
	cvttsd2si	%xmm3, %esi
	movw	%si, (%r15)
.LBB1_21:                               #   in Loop: Header=BB1_16 Depth=1
	addq	$4, %r15
	addq	$-64, %rcx
	cmpl	$-960, %ecx             # imm = 0xFC40
	jne	.LBB1_16
# BB#22:
	subl	$-128, (%r14)
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	synth_1to1, .Lfunc_end1-synth_1to1
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
