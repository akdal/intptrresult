	.text
	.file	"part.bc"
	.globl	visit_col
	.p2align	4, 0x90
	.type	visit_col,@function
visit_col:                              # @visit_col
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi6:
	.cfi_def_cfa_offset 64
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rcx, %r15
	movq	%rdx, %r12
	movq	%rdi, %rbx
	xorl	%r14d, %r14d
	cmpl	$0, 8(%rsi)
	jne	.LBB0_3
# BB#1:
	movl	$1, 8(%rsi)
	movl	(%r15), %eax
	incl	%eax
	movl	%eax, (%r15)
	cmpl	72(%rbx), %eax
	je	.LBB0_2
# BB#4:
	movq	16(%rsi), %r13
	testq	%r13, %r13
	jne	.LBB0_6
	jmp	.LBB0_3
	.p2align	4, 0x90
.LBB0_13:                               # %visit_row.exit
                                        #   in Loop: Header=BB0_6 Depth=1
	movq	8(%r13), %r13
	testq	%r13, %r13
	je	.LBB0_3
.LBB0_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_10 Depth 2
	movq	(%rbx), %rax
	movslq	(%r13), %rcx
	movq	(%rax,%rcx,8), %rax
	cmpl	$0, 8(%rax)
	jne	.LBB0_13
# BB#7:                                 #   in Loop: Header=BB0_6 Depth=1
	movl	$1, 8(%rax)
	movl	(%r12), %ecx
	incl	%ecx
	movl	%ecx, (%r12)
	cmpl	48(%rbx), %ecx
	je	.LBB0_2
# BB#8:                                 #   in Loop: Header=BB0_6 Depth=1
	movq	16(%rax), %rbp
	testq	%rbp, %rbp
	jne	.LBB0_10
	jmp	.LBB0_13
	.p2align	4, 0x90
.LBB0_12:                               #   in Loop: Header=BB0_10 Depth=2
	movq	24(%rbp), %rbp
	testq	%rbp, %rbp
	je	.LBB0_13
.LBB0_10:                               # %.lr.ph
                                        #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	16(%rbx), %rax
	movslq	4(%rbp), %rcx
	movq	(%rax,%rcx,8), %rsi
	cmpl	$0, 8(%rsi)
	jne	.LBB0_12
# BB#11:                                #   in Loop: Header=BB0_10 Depth=2
	movq	%rbx, %rdi
	movq	%r12, %rdx
	movq	%r15, %rcx
	callq	visit_col
	testl	%eax, %eax
	je	.LBB0_12
.LBB0_2:
	movl	$1, %r14d
.LBB0_3:                                # %visit_row.exit.thread
	movl	%r14d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	visit_col, .Lfunc_end0-visit_col
	.cfi_endproc

	.globl	sm_block_partition
	.p2align	4, 0x90
	.type	sm_block_partition,@function
sm_block_partition:                     # @sm_block_partition
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi19:
	.cfi_def_cfa_offset 64
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movq	%rsi, %r12
	movq	%rdi, %rbp
	movl	48(%rbp), %eax
	xorl	%r15d, %r15d
	testl	%eax, %eax
	je	.LBB1_24
# BB#1:
	movq	32(%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB1_4
	.p2align	4, 0x90
.LBB1_2:                                # %.lr.ph43
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rcx)
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_2
.LBB1_4:                                # %._crit_edge44
	movq	56(%rbp), %rcx
	testq	%rcx, %rcx
	je	.LBB1_7
	.p2align	4, 0x90
.LBB1_5:                                # %.lr.ph39
                                        # =>This Inner Loop Header: Depth=1
	movl	$0, 8(%rcx)
	movq	32(%rcx), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_5
.LBB1_7:                                # %._crit_edge
	movl	$0, (%rsp)
	movl	$0, 4(%rsp)
	movq	32(%rbp), %rcx
	cmpl	$0, 8(%rcx)
	je	.LBB1_8
.LBB1_14:                               # %.loopexit
	xorl	%eax, %eax
	callq	sm_alloc
	movq	%rax, (%r12)
	xorl	%eax, %eax
	callq	sm_alloc
	movq	%rax, (%r14)
	movq	32(%rbp), %r13
	movl	$1, %r15d
	testq	%r13, %r13
	jne	.LBB1_16
	jmp	.LBB1_24
	.p2align	4, 0x90
.LBB1_23:                               # %copy_row.exit
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	32(%r13), %r13
	testq	%r13, %r13
	je	.LBB1_24
.LBB1_16:                               # %.lr.ph
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_19 Depth 2
                                        #     Child Loop BB1_22 Depth 2
	cmpl	$0, 8(%r13)
	je	.LBB1_20
# BB#17:                                #   in Loop: Header=BB1_16 Depth=1
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB1_23
# BB#18:                                # %.lr.ph.i27.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	(%r12), %rbp
	.p2align	4, 0x90
.LBB1_19:                               # %.lr.ph.i27
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movl	4(%rbx), %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sm_insert
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_19
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_20:                               #   in Loop: Header=BB1_16 Depth=1
	movq	16(%r13), %rbx
	testq	%rbx, %rbx
	je	.LBB1_23
# BB#21:                                # %.lr.ph.i31.preheader
                                        #   in Loop: Header=BB1_16 Depth=1
	movq	(%r14), %rbp
	.p2align	4, 0x90
.LBB1_22:                               # %.lr.ph.i31
                                        #   Parent Loop BB1_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx), %esi
	movl	4(%rbx), %edx
	xorl	%eax, %eax
	movq	%rbp, %rdi
	callq	sm_insert
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_22
	jmp	.LBB1_23
.LBB1_8:
	movl	$1, 8(%rcx)
	movl	$1, (%rsp)
	cmpl	$1, %eax
	jne	.LBB1_9
.LBB1_24:                               # %visit_row.exit
	movl	%r15d, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_9:
	movq	16(%rcx), %rbx
	testq	%rbx, %rbx
	je	.LBB1_14
# BB#10:                                # %.lr.ph.i
	leaq	4(%rsp), %r13
	.p2align	4, 0x90
.LBB1_11:                               # =>This Inner Loop Header: Depth=1
	movq	16(%rbp), %rax
	movslq	4(%rbx), %rcx
	movq	(%rax,%rcx,8), %rsi
	cmpl	$0, 8(%rsi)
	jne	.LBB1_13
# BB#12:                                #   in Loop: Header=BB1_11 Depth=1
	movq	%rbp, %rdi
	movq	%rsp, %rdx
	movq	%r13, %rcx
	callq	visit_col
	testl	%eax, %eax
	jne	.LBB1_24
.LBB1_13:                               #   in Loop: Header=BB1_11 Depth=1
	movq	24(%rbx), %rbx
	testq	%rbx, %rbx
	jne	.LBB1_11
	jmp	.LBB1_14
.Lfunc_end1:
	.size	sm_block_partition, .Lfunc_end1-sm_block_partition
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
