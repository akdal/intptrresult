	.text
	.file	"findopen.bc"
	.globl	findopen
	.p2align	4, 0x90
	.type	findopen,@function
findopen:                               # @findopen
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 80
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r8d, %r13d
	movq	%rdx, %r8
	movl	%esi, %ebx
	movl	%edi, %ebp
	movq	80(%rsp), %rdx
	movslq	%ebp, %rax
	movslq	%ebx, %r14
	imulq	$19, %rax, %r15
	movb	$1, ma(%r15,%r14)
	testl	%eax, %eax
	je	.LBB0_10
# BB#1:
	leal	-1(%rbp), %edi
	movslq	%edi, %rax
	imulq	$19, %rax, %rax
	movzbl	p(%rax,%r14), %esi
	testl	%esi, %esi
	jne	.LBB0_5
# BB#2:
	cmpl	mik(%rip), %edi
	jne	.LBB0_4
# BB#3:
	cmpl	%ebx, mjk(%rip)
	jne	.LBB0_4
.LBB0_5:
	cmpl	%r13d, %esi
	jne	.LBB0_9
# BB#6:
	cmpb	$0, ma(%rax,%r14)
	jne	.LBB0_9
# BB#7:
	movq	%rdx, (%rsp)
	movl	%ebx, %esi
	movq	%r8, %rdx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movl	%r13d, %r8d
	movl	%r9d, %r12d
	callq	findopen
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	%r12d, %r9d
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %rdx
	testl	%eax, %eax
	je	.LBB0_9
# BB#8:
	movl	$1, %eax
	cmpl	%r9d, (%rdx)
	jne	.LBB0_9
	jmp	.LBB0_37
.LBB0_4:
	movslq	(%rdx), %rax
	movl	%edi, (%r8,%rax,4)
	movslq	(%rdx), %rax
	movl	%ebx, (%rcx,%rax,4)
	movl	(%rdx), %esi
	incl	%esi
	movl	%esi, (%rdx)
	movl	$1, %eax
	cmpl	%r9d, %esi
	je	.LBB0_37
.LBB0_9:
	cmpl	$18, %ebp
	je	.LBB0_18
.LBB0_10:
	leal	1(%rbp), %edi
	movslq	%edi, %rax
	imulq	$19, %rax, %rax
	movzbl	p(%rax,%r14), %esi
	testl	%esi, %esi
	jne	.LBB0_14
# BB#11:
	cmpl	mik(%rip), %edi
	jne	.LBB0_13
# BB#12:
	cmpl	%ebx, mjk(%rip)
	jne	.LBB0_13
.LBB0_14:
	cmpl	%r13d, %esi
	jne	.LBB0_18
# BB#15:
	cmpb	$0, ma(%rax,%r14)
	jne	.LBB0_18
# BB#16:
	movq	%rdx, (%rsp)
	movl	%ebx, %esi
	movq	%r8, %rdx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movl	%r13d, %r8d
	movl	%r9d, %r12d
	callq	findopen
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	%r12d, %r9d
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %rdx
	testl	%eax, %eax
	je	.LBB0_18
# BB#17:
	movl	$1, %eax
	cmpl	%r9d, (%rdx)
	jne	.LBB0_18
	jmp	.LBB0_37
.LBB0_13:
	movslq	(%rdx), %rax
	movl	%edi, (%r8,%rax,4)
	movslq	(%rdx), %rax
	movl	%ebx, (%rcx,%rax,4)
	movl	(%rdx), %esi
	incl	%esi
	movl	%esi, (%rdx)
	movl	$1, %eax
	cmpl	%r9d, %esi
	je	.LBB0_37
.LBB0_18:
	testl	%ebx, %ebx
	je	.LBB0_28
# BB#19:
	leal	-1(%rbx), %esi
	movzbl	p-1(%r15,%r14), %eax
	testl	%eax, %eax
	jne	.LBB0_23
# BB#20:
	cmpl	%ebp, mik(%rip)
	jne	.LBB0_22
# BB#21:
	cmpl	mjk(%rip), %esi
	jne	.LBB0_22
.LBB0_23:
	cmpl	%r13d, %eax
	jne	.LBB0_27
# BB#24:
	leaq	-1(%r14), %rax
	cmpb	$0, ma(%r15,%rax)
	jne	.LBB0_27
# BB#25:
	movq	%rdx, (%rsp)
	movl	%ebp, %edi
	movq	%r8, %rdx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movq	%r8, 8(%rsp)            # 8-byte Spill
	movl	%r13d, %r8d
	movl	%r9d, %r12d
	callq	findopen
	movq	8(%rsp), %r8            # 8-byte Reload
	movl	%r12d, %r9d
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	80(%rsp), %rdx
	testl	%eax, %eax
	je	.LBB0_27
# BB#26:
	movl	$1, %eax
	cmpl	%r9d, (%rdx)
	jne	.LBB0_27
	jmp	.LBB0_37
.LBB0_22:
	movslq	(%rdx), %rax
	movl	%ebp, (%r8,%rax,4)
	movslq	(%rdx), %rax
	movl	%esi, (%rcx,%rax,4)
	movl	(%rdx), %esi
	incl	%esi
	movl	%esi, (%rdx)
	movl	$1, %eax
	cmpl	%r9d, %esi
	je	.LBB0_37
.LBB0_27:
	cmpl	$18, %ebx
	je	.LBB0_36
.LBB0_28:
	incl	%ebx
	movzbl	p+1(%r15,%r14), %eax
	testl	%eax, %eax
	jne	.LBB0_32
# BB#29:
	cmpl	%ebp, mik(%rip)
	jne	.LBB0_31
# BB#30:
	cmpl	mjk(%rip), %ebx
	jne	.LBB0_31
.LBB0_32:
	cmpl	%r13d, %eax
	jne	.LBB0_36
# BB#33:
	incq	%r14
	cmpb	$0, ma(%r15,%r14)
	jne	.LBB0_36
# BB#34:
	movq	%rdx, (%rsp)
	movl	%ebp, %edi
	movl	%ebx, %esi
	movq	%rdx, %rbx
	movq	%r8, %rdx
	movl	%r13d, %r8d
	movl	%r9d, %ebp
	callq	findopen
	movq	%rbx, %rcx
	testl	%eax, %eax
	je	.LBB0_36
# BB#35:
	movl	$1, %eax
	cmpl	%ebp, (%rcx)
	jne	.LBB0_36
	jmp	.LBB0_37
.LBB0_31:
	movslq	(%rdx), %rax
	movl	%ebp, (%r8,%rax,4)
	movslq	(%rdx), %rax
	movl	%ebx, (%rcx,%rax,4)
	movl	(%rdx), %ecx
	incl	%ecx
	movl	%ecx, (%rdx)
	movl	$1, %eax
	cmpl	%r9d, %ecx
	je	.LBB0_37
.LBB0_36:
	xorl	%eax, %eax
.LBB0_37:
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	findopen, .Lfunc_end0-findopen
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
