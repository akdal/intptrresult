	.text
	.file	"btSimpleDynamicsWorld.bc"
	.globl	btBulletDynamicsProbe
	.p2align	4, 0x90
	.type	btBulletDynamicsProbe,@function
btBulletDynamicsProbe:                  # @btBulletDynamicsProbe
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end0:
	.size	btBulletDynamicsProbe, .Lfunc_end0-btBulletDynamicsProbe
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	1101004800              # float 20
	.long	1065353216              # float 1
	.long	1045220557              # float 0.200000003
	.long	1036831949              # float 0.100000001
	.text
	.globl	_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration,@function
_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration: # @_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rcx, %r14
	movq	%rdi, %rbx
	movq	%r8, %rcx
	callq	_ZN16btCollisionWorldC2EP12btDispatcherP21btBroadphaseInterfaceP24btCollisionConfiguration
	xorps	%xmm0, %xmm0
	movups	%xmm0, 136(%rbx)
	movq	$0, 152(%rbx)
	movl	$1058642330, 160(%rbx)  # imm = 0x3F19999A
	movl	$1065353216, 164(%rbx)  # imm = 0x3F800000
	movl	$1050253722, 168(%rbx)  # imm = 0x3E99999A
	movl	$0, 176(%rbx)
	movl	$10, 180(%rbx)
	movl	$0, 200(%rbx)
	movaps	.LCPI1_0(%rip), %xmm0   # xmm0 = [2.000000e+01,1.000000e+00,2.000000e-01,1.000000e-01]
	movups	%xmm0, 184(%rbx)
	movl	$0, 204(%rbx)
	movl	$-1130113270, 208(%rbx) # imm = 0xBCA3D70A
	movl	$0, 212(%rbx)
	movl	$1062836634, 216(%rbx)  # imm = 0x3F59999A
	movl	$260, 220(%rbx)         # imm = 0x104
	movl	$2, 224(%rbx)
	movq	$_ZTV21btSimpleDynamicsWorld+16, (%rbx)
	movq	%r14, 232(%rbx)
	movb	$0, 240(%rbx)
	movq	$0, 244(%rbx)
	movl	$3240099840, %eax       # imm = 0xC1200000
	movq	%rax, 252(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end1:
	.size	_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration, .Lfunc_end1-_ZN21btSimpleDynamicsWorldC2EP12btDispatcherP21btBroadphaseInterfaceP18btConstraintSolverP24btCollisionConfiguration
	.cfi_endproc

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end2:
	.size	__clang_call_terminate, .Lfunc_end2-__clang_call_terminate

	.text
	.globl	_ZN21btSimpleDynamicsWorldD2Ev
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorldD2Ev,@function
_ZN21btSimpleDynamicsWorldD2Ev:         # @_ZN21btSimpleDynamicsWorldD2Ev
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV21btSimpleDynamicsWorld+16, (%rbx)
	cmpb	$0, 240(%rbx)
	je	.LBB3_2
# BB#1:
	movq	232(%rbx), %rdi
.Ltmp0:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp1:
.LBB3_2:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN16btCollisionWorldD2Ev # TAILCALL
.LBB3_3:
.Ltmp2:
	movq	%rax, %r14
.Ltmp3:
	movq	%rbx, %rdi
	callq	_ZN16btCollisionWorldD2Ev
.Ltmp4:
# BB#4:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB3_5:
.Ltmp5:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end3:
	.size	_ZN21btSimpleDynamicsWorldD2Ev, .Lfunc_end3-_ZN21btSimpleDynamicsWorldD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table3:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 1 <<
	.long	.Ltmp1-.Ltmp0           #   Call between .Ltmp0 and .Ltmp1
	.long	.Ltmp2-.Lfunc_begin0    #     jumps to .Ltmp2
	.byte	0                       #   On action: cleanup
	.long	.Ltmp1-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp1           #   Call between .Ltmp1 and .Ltmp3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp3-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp4-.Ltmp3           #   Call between .Ltmp3 and .Ltmp4
	.long	.Ltmp5-.Lfunc_begin0    #     jumps to .Ltmp5
	.byte	1                       #   On action: 1
	.long	.Ltmp4-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Lfunc_end3-.Ltmp4      #   Call between .Ltmp4 and .Lfunc_end3
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.text
	.globl	_ZN21btSimpleDynamicsWorldD0Ev
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorldD0Ev,@function
_ZN21btSimpleDynamicsWorldD0Ev:         # @_ZN21btSimpleDynamicsWorldD0Ev
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r14
.Lcfi10:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi12:
	.cfi_def_cfa_offset 32
.Lcfi13:
	.cfi_offset %rbx, -24
.Lcfi14:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV21btSimpleDynamicsWorld+16, (%rbx)
	cmpb	$0, 240(%rbx)
	je	.LBB4_2
# BB#1:
	movq	232(%rbx), %rdi
.Ltmp6:
	callq	_Z21btAlignedFreeInternalPv
.Ltmp7:
.LBB4_2:
.Ltmp12:
	movq	%rbx, %rdi
	callq	_ZN16btCollisionWorldD2Ev
.Ltmp13:
# BB#3:                                 # %_ZN21btSimpleDynamicsWorldD2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB4_4:
.Ltmp8:
	movq	%rax, %r14
.Ltmp9:
	movq	%rbx, %rdi
	callq	_ZN16btCollisionWorldD2Ev
.Ltmp10:
	jmp	.LBB4_7
.LBB4_5:
.Ltmp11:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB4_6:
.Ltmp14:
	movq	%rax, %r14
.LBB4_7:                                # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end4:
	.size	_ZN21btSimpleDynamicsWorldD0Ev, .Lfunc_end4-_ZN21btSimpleDynamicsWorldD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table4:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp6-.Lfunc_begin1    # >> Call Site 1 <<
	.long	.Ltmp7-.Ltmp6           #   Call between .Ltmp6 and .Ltmp7
	.long	.Ltmp8-.Lfunc_begin1    #     jumps to .Ltmp8
	.byte	0                       #   On action: cleanup
	.long	.Ltmp12-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp13-.Ltmp12         #   Call between .Ltmp12 and .Ltmp13
	.long	.Ltmp14-.Lfunc_begin1   #     jumps to .Ltmp14
	.byte	0                       #   On action: cleanup
	.long	.Ltmp9-.Lfunc_begin1    # >> Call Site 3 <<
	.long	.Ltmp10-.Ltmp9          #   Call between .Ltmp9 and .Ltmp10
	.long	.Ltmp11-.Lfunc_begin1   #     jumps to .Ltmp11
	.byte	1                       #   On action: 1
	.long	.Ltmp10-.Lfunc_begin1   # >> Call Site 4 <<
	.long	.Lfunc_end4-.Ltmp10     #   Call between .Ltmp10 and .Lfunc_end4
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI5_0:
	.long	1101004800              # float 20
	.long	1065353216              # float 1
	.long	1045220557              # float 0.200000003
	.long	1036831949              # float 0.100000001
	.text
	.globl	_ZN21btSimpleDynamicsWorld14stepSimulationEfif
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld14stepSimulationEfif,@function
_ZN21btSimpleDynamicsWorld14stepSimulationEfif: # @_ZN21btSimpleDynamicsWorld14stepSimulationEfif
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi15:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi16:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi17:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 48
	subq	$80, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 128
.Lcfi21:
	.cfi_offset %rbx, -48
.Lcfi22:
	.cfi_offset %r12, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r12
	movl	12(%r12), %eax
	testl	%eax, %eax
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	jle	.LBB5_9
# BB#1:                                 # %.lr.ph.i
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB5_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rcx
	movq	(%rcx,%rbx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB5_8
# BB#3:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpl	$2, 256(%rbp)
	jne	.LBB5_8
# BB#4:                                 #   in Loop: Header=BB5_2 Depth=1
	testb	$1, 216(%rbp)
	jne	.LBB5_8
# BB#5:                                 #   in Loop: Header=BB5_2 Depth=1
	movl	228(%rbp), %ecx
	cmpl	$2, %ecx
	je	.LBB5_8
# BB#6:                                 #   in Loop: Header=BB5_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB5_8
# BB#7:                                 #   in Loop: Header=BB5_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZN11btRigidBody12applyGravityEv
	movq	%rbp, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody19integrateVelocitiesEf
	movq	%rbp, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody12applyDampingEf
	leaq	72(%rbp), %rsi
	movq	%rbp, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movl	12(%r12), %eax
	.p2align	4, 0x90
.LBB5_8:                                #   in Loop: Header=BB5_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB5_2
.LBB5_9:                                # %_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf.exit
	movss	%xmm0, 48(%r12)
	movl	$0, 52(%r12)
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*32(%rax)
	movq	%rax, 72(%r12)
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*56(%rax)
	movq	40(%r12), %rdi
	movq	(%rdi), %rax
	callq	*72(%rax)
	movl	%eax, %ebp
	testl	%ebp, %ebp
	je	.LBB5_11
# BB#10:
	movq	40(%r12), %rdi
	movq	(%rdi), %rax
	callq	*88(%rax)
	movq	%rax, %r14
	movl	$1058642330, 8(%rsp)    # imm = 0x3F19999A
	movl	$1065353216, 12(%rsp)   # imm = 0x3F800000
	movl	$1050253722, 16(%rsp)   # imm = 0x3E99999A
	movl	$0, 24(%rsp)
	movl	$10, 28(%rsp)
	movl	$0, 48(%rsp)
	movaps	.LCPI5_0(%rip), %xmm0   # xmm0 = [2.000000e+01,1.000000e+00,2.000000e-01,1.000000e-01]
	movups	%xmm0, 32(%rsp)
	movl	$0, 52(%rsp)
	movl	$-1130113270, 56(%rsp)  # imm = 0xBCA3D70A
	movl	$0, 60(%rsp)
	movl	$1062836634, 64(%rsp)   # imm = 0x3F59999A
	movl	$260, 68(%rsp)          # imm = 0x104
	movl	$2, 72(%rsp)
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movss	%xmm0, 20(%rsp)
	movq	232(%r12), %rdi
	movq	(%rdi), %rax
	xorl	%esi, %esi
	movl	%ebp, %edx
	callq	*16(%rax)
	movq	232(%r12), %rdi
	movq	(%rdi), %rax
	subq	$8, %rsp
.Lcfi26:
	.cfi_adjust_cfa_offset 8
	leaq	16(%rsp), %r15
	xorl	%esi, %esi
	xorl	%edx, %edx
	xorl	%r9d, %r9d
	movq	%r14, %rcx
	movl	%ebp, %r8d
	pushq	40(%r12)
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	104(%r12)
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	pushq	120(%r12)
.Lcfi29:
	.cfi_adjust_cfa_offset 8
	pushq	%r15
.Lcfi30:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi31:
	.cfi_adjust_cfa_offset 8
	callq	*24(%rax)
	addq	$48, %rsp
.Lcfi32:
	.cfi_adjust_cfa_offset -48
	movq	232(%r12), %rdi
	movq	(%rdi), %rax
	movq	104(%r12), %rcx
	movq	120(%r12), %rdx
	movq	%r15, %rsi
	callq	*32(%rax)
.LBB5_11:
	movl	12(%r12), %eax
	testl	%eax, %eax
	jle	.LBB5_20
# BB#12:                                # %.lr.ph.i16
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB5_13:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rcx
	movq	(%rcx,%rbx,8), %rbp
	testq	%rbp, %rbp
	je	.LBB5_19
# BB#14:                                #   in Loop: Header=BB5_13 Depth=1
	cmpl	$2, 256(%rbp)
	jne	.LBB5_19
# BB#15:                                #   in Loop: Header=BB5_13 Depth=1
	movl	228(%rbp), %ecx
	cmpl	$2, %ecx
	je	.LBB5_19
# BB#16:                                #   in Loop: Header=BB5_13 Depth=1
	cmpl	$5, %ecx
	je	.LBB5_19
# BB#17:                                #   in Loop: Header=BB5_13 Depth=1
	testb	$1, 216(%rbp)
	jne	.LBB5_19
# BB#18:                                #   in Loop: Header=BB5_13 Depth=1
	movq	%rbp, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%r14, %rsi
	callq	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
	movq	%rbp, %rdi
	movq	%r14, %rsi
	callq	_ZN11btRigidBody18proceedToTransformERK11btTransform
	movl	12(%r12), %eax
	.p2align	4, 0x90
.LBB5_19:                               #   in Loop: Header=BB5_13 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB5_13
.LBB5_20:                               # %_ZN21btSimpleDynamicsWorld19integrateTransformsEf.exit
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*16(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*128(%rax)
	movq	(%r12), %rax
	movq	%r12, %rdi
	callq	*200(%rax)
	movl	$1, %eax
	addq	$80, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	_ZN21btSimpleDynamicsWorld14stepSimulationEfif, .Lfunc_end5-_ZN21btSimpleDynamicsWorld14stepSimulationEfif
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf,@function
_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf: # @_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi33:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi34:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi35:
	.cfi_def_cfa_offset 32
	subq	$16, %rsp
.Lcfi36:
	.cfi_def_cfa_offset 48
.Lcfi37:
	.cfi_offset %rbx, -32
.Lcfi38:
	.cfi_offset %r14, -24
.Lcfi39:
	.cfi_offset %r15, -16
	movss	%xmm0, 12(%rsp)         # 4-byte Spill
	movq	%rdi, %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.LBB6_9
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB6_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rcx
	movq	(%rcx,%rbx,8), %r15
	testq	%r15, %r15
	je	.LBB6_8
# BB#3:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpl	$2, 256(%r15)
	jne	.LBB6_8
# BB#4:                                 #   in Loop: Header=BB6_2 Depth=1
	testb	$1, 216(%r15)
	jne	.LBB6_8
# BB#5:                                 #   in Loop: Header=BB6_2 Depth=1
	movl	228(%r15), %ecx
	cmpl	$2, %ecx
	je	.LBB6_8
# BB#6:                                 #   in Loop: Header=BB6_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_2 Depth=1
	movq	%r15, %rdi
	callq	_ZN11btRigidBody12applyGravityEv
	movq	%r15, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody19integrateVelocitiesEf
	movq	%r15, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody12applyDampingEf
	leaq	72(%r15), %rsi
	movq	%r15, %rdi
	movss	12(%rsp), %xmm0         # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	callq	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
	movl	12(%r14), %eax
	.p2align	4, 0x90
.LBB6_8:                                #   in Loop: Header=BB6_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB6_2
.LBB6_9:                                # %._crit_edge
	addq	$16, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end6:
	.size	_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf, .Lfunc_end6-_ZN21btSimpleDynamicsWorld25predictUnconstraintMotionEf
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld19integrateTransformsEf
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld19integrateTransformsEf,@function
_ZN21btSimpleDynamicsWorld19integrateTransformsEf: # @_ZN21btSimpleDynamicsWorld19integrateTransformsEf
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi42:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi43:
	.cfi_def_cfa_offset 40
	subq	$72, %rsp
.Lcfi44:
	.cfi_def_cfa_offset 112
.Lcfi45:
	.cfi_offset %rbx, -40
.Lcfi46:
	.cfi_offset %r12, -32
.Lcfi47:
	.cfi_offset %r14, -24
.Lcfi48:
	.cfi_offset %r15, -16
	movss	%xmm0, 4(%rsp)          # 4-byte Spill
	movq	%rdi, %r15
	movl	12(%r15), %eax
	testl	%eax, %eax
	jle	.LBB7_9
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB7_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rcx
	movq	(%rcx,%rbx,8), %r12
	testq	%r12, %r12
	je	.LBB7_8
# BB#3:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	$2, 256(%r12)
	jne	.LBB7_8
# BB#4:                                 #   in Loop: Header=BB7_2 Depth=1
	movl	228(%r12), %ecx
	cmpl	$2, %ecx
	je	.LBB7_8
# BB#5:                                 #   in Loop: Header=BB7_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB7_8
# BB#6:                                 #   in Loop: Header=BB7_2 Depth=1
	testb	$1, 216(%r12)
	jne	.LBB7_8
# BB#7:                                 #   in Loop: Header=BB7_2 Depth=1
	movq	%r12, %rdi
	movss	4(%rsp), %xmm0          # 4-byte Reload
                                        # xmm0 = mem[0],zero,zero,zero
	movq	%r14, %rsi
	callq	_ZN11btRigidBody26predictIntegratedTransformEfR11btTransform
	movq	%r12, %rdi
	movq	%r14, %rsi
	callq	_ZN11btRigidBody18proceedToTransformERK11btTransform
	movl	12(%r15), %eax
	.p2align	4, 0x90
.LBB7_8:                                #   in Loop: Header=BB7_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB7_2
.LBB7_9:                                # %._crit_edge
	addq	$72, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end7:
	.size	_ZN21btSimpleDynamicsWorld19integrateTransformsEf, .Lfunc_end7-_ZN21btSimpleDynamicsWorld19integrateTransformsEf
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld11clearForcesEv
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld11clearForcesEv,@function
_ZN21btSimpleDynamicsWorld11clearForcesEv: # @_ZN21btSimpleDynamicsWorld11clearForcesEv
	.cfi_startproc
# BB#0:
	movl	12(%rdi), %eax
	testl	%eax, %eax
	jle	.LBB8_6
# BB#1:                                 # %.lr.ph
	xorl	%ecx, %ecx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB8_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%rdi), %rdx
	movq	(%rdx,%rcx,8), %rdx
	testq	%rdx, %rdx
	je	.LBB8_5
# BB#3:                                 #   in Loop: Header=BB8_2 Depth=1
	cmpl	$2, 256(%rdx)
	jne	.LBB8_5
# BB#4:                                 #   in Loop: Header=BB8_2 Depth=1
	movups	%xmm0, 460(%rdx)
	movups	%xmm0, 444(%rdx)
	movl	12(%rdi), %eax
.LBB8_5:                                #   in Loop: Header=BB8_2 Depth=1
	incq	%rcx
	movslq	%eax, %rdx
	cmpq	%rdx, %rcx
	jl	.LBB8_2
.LBB8_6:                                # %._crit_edge
	retq
.Lfunc_end8:
	.size	_ZN21btSimpleDynamicsWorld11clearForcesEv, .Lfunc_end8-_ZN21btSimpleDynamicsWorld11clearForcesEv
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3,@function
_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3: # @_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi49:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi50:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi51:
	.cfi_def_cfa_offset 32
.Lcfi52:
	.cfi_offset %rbx, -32
.Lcfi53:
	.cfi_offset %r14, -24
.Lcfi54:
	.cfi_offset %r15, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	movups	(%r14), %xmm0
	movups	%xmm0, 244(%r15)
	movl	12(%r15), %eax
	testl	%eax, %eax
	jle	.LBB9_6
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB9_2:                                # =>This Inner Loop Header: Depth=1
	movq	24(%r15), %rcx
	movq	(%rcx,%rbx,8), %rdi
	testq	%rdi, %rdi
	je	.LBB9_5
# BB#3:                                 #   in Loop: Header=BB9_2 Depth=1
	cmpl	$2, 256(%rdi)
	jne	.LBB9_5
# BB#4:                                 #   in Loop: Header=BB9_2 Depth=1
	movq	%r14, %rsi
	callq	_ZN11btRigidBody10setGravityERK9btVector3
	movl	12(%r15), %eax
.LBB9_5:                                #   in Loop: Header=BB9_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB9_2
.LBB9_6:                                # %._crit_edge
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end9:
	.size	_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3, .Lfunc_end9-_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3
	.cfi_endproc

	.globl	_ZNK21btSimpleDynamicsWorld10getGravityEv
	.p2align	4, 0x90
	.type	_ZNK21btSimpleDynamicsWorld10getGravityEv,@function
_ZNK21btSimpleDynamicsWorld10getGravityEv: # @_ZNK21btSimpleDynamicsWorld10getGravityEv
	.cfi_startproc
# BB#0:
	movsd	244(%rdi), %xmm0        # xmm0 = mem[0],zero
	movsd	252(%rdi), %xmm1        # xmm1 = mem[0],zero
	retq
.Lfunc_end10:
	.size	_ZNK21btSimpleDynamicsWorld10getGravityEv, .Lfunc_end10-_ZNK21btSimpleDynamicsWorld10getGravityEv
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody,@function
_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody: # @_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody
	.cfi_startproc
# BB#0:
	jmp	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject # TAILCALL
.Lfunc_end11:
	.size	_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody, .Lfunc_end11-_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject,@function
_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject: # @_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_startproc
# BB#0:
	testq	%rsi, %rsi
	je	.LBB12_3
# BB#1:
	cmpl	$2, 256(%rsi)
	jne	.LBB12_3
# BB#2:
	movq	(%rdi), %rax
	movq	144(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.LBB12_3:
	jmp	_ZN16btCollisionWorld21removeCollisionObjectEP17btCollisionObject # TAILCALL
.Lfunc_end12:
	.size	_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject, .Lfunc_end12-_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody,@function
_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody: # @_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi55:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi56:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi57:
	.cfi_def_cfa_offset 32
.Lcfi58:
	.cfi_offset %rbx, -24
.Lcfi59:
	.cfi_offset %r14, -16
	movq	%rsi, %rbx
	movq	%rdi, %r14
	leaq	244(%r14), %rsi
	movq	%rbx, %rdi
	callq	_ZN11btRigidBody10setGravityERK9btVector3
	cmpq	$0, 200(%rbx)
	je	.LBB13_1
# BB#2:
	movq	(%r14), %rax
	movq	40(%rax), %rax
	movl	$1, %edx
	movl	$-1, %ecx
	movq	%r14, %rdi
	movq	%rbx, %rsi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmpq	*%rax                   # TAILCALL
.LBB13_1:
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end13:
	.size	_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody, .Lfunc_end13-_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld11updateAabbsEv
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld11updateAabbsEv,@function
_ZN21btSimpleDynamicsWorld11updateAabbsEv: # @_ZN21btSimpleDynamicsWorld11updateAabbsEv
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi64:
	.cfi_def_cfa_offset 48
	subq	$32, %rsp
.Lcfi65:
	.cfi_def_cfa_offset 80
.Lcfi66:
	.cfi_offset %rbx, -48
.Lcfi67:
	.cfi_offset %r12, -40
.Lcfi68:
	.cfi_offset %r13, -32
.Lcfi69:
	.cfi_offset %r14, -24
.Lcfi70:
	.cfi_offset %r15, -16
	movq	%rdi, %r12
	movl	12(%r12), %eax
	testl	%eax, %eax
	jle	.LBB14_9
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	leaq	16(%rsp), %r14
	movq	%rsp, %r15
	.p2align	4, 0x90
.LBB14_2:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r12), %rcx
	movq	(%rcx,%rbx,8), %r13
	testq	%r13, %r13
	je	.LBB14_8
# BB#3:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	$2, 256(%r13)
	jne	.LBB14_8
# BB#4:                                 #   in Loop: Header=BB14_2 Depth=1
	movl	228(%r13), %ecx
	cmpl	$2, %ecx
	je	.LBB14_8
# BB#5:                                 #   in Loop: Header=BB14_2 Depth=1
	cmpl	$5, %ecx
	je	.LBB14_8
# BB#6:                                 #   in Loop: Header=BB14_2 Depth=1
	testb	$1, 216(%r13)
	jne	.LBB14_8
# BB#7:                                 #   in Loop: Header=BB14_2 Depth=1
	movq	200(%r13), %rdi
	movq	(%rdi), %rax
	leaq	8(%r13), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	*16(%rax)
	movq	40(%r12), %r8
	movq	112(%r12), %rdi
	movq	(%rdi), %rax
	movq	192(%r13), %rsi
	movq	%r14, %rdx
	movq	%r15, %rcx
	callq	*32(%rax)
	movl	12(%r12), %eax
	.p2align	4, 0x90
.LBB14_8:                               #   in Loop: Header=BB14_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB14_2
.LBB14_9:                               # %._crit_edge
	addq	$32, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end14:
	.size	_ZN21btSimpleDynamicsWorld11updateAabbsEv, .Lfunc_end14-_ZN21btSimpleDynamicsWorld11updateAabbsEv
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv,@function
_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv: # @_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi71:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi72:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi73:
	.cfi_def_cfa_offset 32
.Lcfi74:
	.cfi_offset %rbx, -24
.Lcfi75:
	.cfi_offset %r14, -16
	movq	%rdi, %r14
	movl	12(%r14), %eax
	testl	%eax, %eax
	jle	.LBB15_8
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB15_2:                               # =>This Inner Loop Header: Depth=1
	movq	24(%r14), %rcx
	movq	(%rcx,%rbx,8), %rsi
	testq	%rsi, %rsi
	je	.LBB15_7
# BB#3:                                 #   in Loop: Header=BB15_2 Depth=1
	cmpl	$2, 256(%rsi)
	jne	.LBB15_7
# BB#4:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	512(%rsi), %rdi
	testq	%rdi, %rdi
	je	.LBB15_7
# BB#5:                                 #   in Loop: Header=BB15_2 Depth=1
	cmpl	$2, 228(%rsi)
	je	.LBB15_7
# BB#6:                                 #   in Loop: Header=BB15_2 Depth=1
	movq	(%rdi), %rax
	addq	$8, %rsi
	callq	*24(%rax)
	movl	12(%r14), %eax
	.p2align	4, 0x90
.LBB15_7:                               #   in Loop: Header=BB15_2 Depth=1
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB15_2
.LBB15_8:                               # %._crit_edge
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end15:
	.size	_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv, .Lfunc_end15-_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver,@function
_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver: # @_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi78:
	.cfi_def_cfa_offset 32
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movq	%rsi, %r14
	movq	%rdi, %rbx
	cmpb	$0, 240(%rbx)
	je	.LBB16_2
# BB#1:
	movq	232(%rbx), %rdi
	callq	_Z21btAlignedFreeInternalPv
.LBB16_2:                               # %._crit_edge
	movb	$0, 240(%rbx)
	movq	%r14, 232(%rbx)
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end16:
	.size	_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver, .Lfunc_end16-_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.cfi_endproc

	.globl	_ZN21btSimpleDynamicsWorld19getConstraintSolverEv
	.p2align	4, 0x90
	.type	_ZN21btSimpleDynamicsWorld19getConstraintSolverEv,@function
_ZN21btSimpleDynamicsWorld19getConstraintSolverEv: # @_ZN21btSimpleDynamicsWorld19getConstraintSolverEv
	.cfi_startproc
# BB#0:
	movq	232(%rdi), %rax
	retq
.Lfunc_end17:
	.size	_ZN21btSimpleDynamicsWorld19getConstraintSolverEv, .Lfunc_end17-_ZN21btSimpleDynamicsWorld19getConstraintSolverEv
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,"axG",@progbits,_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,comdat
	.weak	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw,@function
_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw: # @_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_startproc
# BB#0:
	movq	%rsi, 120(%rdi)
	retq
.Lfunc_end18:
	.size	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw, .Lfunc_end18-_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.cfi_endproc

	.section	.text._ZN16btCollisionWorld14getDebugDrawerEv,"axG",@progbits,_ZN16btCollisionWorld14getDebugDrawerEv,comdat
	.weak	_ZN16btCollisionWorld14getDebugDrawerEv
	.p2align	4, 0x90
	.type	_ZN16btCollisionWorld14getDebugDrawerEv,@function
_ZN16btCollisionWorld14getDebugDrawerEv: # @_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_startproc
# BB#0:
	movq	120(%rdi), %rax
	retq
.Lfunc_end19:
	.size	_ZN16btCollisionWorld14getDebugDrawerEv, .Lfunc_end19-_ZN16btCollisionWorld14getDebugDrawerEv
	.cfi_endproc

	.section	.text._ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb,"axG",@progbits,_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb,comdat
	.weak	_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb
	.p2align	4, 0x90
	.type	_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb,@function
_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb: # @_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end20:
	.size	_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb, .Lfunc_end20-_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb
	.cfi_endproc

	.section	.text._ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint,"axG",@progbits,_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint,comdat
	.weak	_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint
	.p2align	4, 0x90
	.type	_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint,@function
_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint: # @_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end21:
	.size	_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint, .Lfunc_end21-_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint
	.cfi_endproc

	.section	.text._ZNK15btDynamicsWorld17getNumConstraintsEv,"axG",@progbits,_ZNK15btDynamicsWorld17getNumConstraintsEv,comdat
	.weak	_ZNK15btDynamicsWorld17getNumConstraintsEv
	.p2align	4, 0x90
	.type	_ZNK15btDynamicsWorld17getNumConstraintsEv,@function
_ZNK15btDynamicsWorld17getNumConstraintsEv: # @_ZNK15btDynamicsWorld17getNumConstraintsEv
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end22:
	.size	_ZNK15btDynamicsWorld17getNumConstraintsEv, .Lfunc_end22-_ZNK15btDynamicsWorld17getNumConstraintsEv
	.cfi_endproc

	.section	.text._ZN15btDynamicsWorld13getConstraintEi,"axG",@progbits,_ZN15btDynamicsWorld13getConstraintEi,comdat
	.weak	_ZN15btDynamicsWorld13getConstraintEi
	.p2align	4, 0x90
	.type	_ZN15btDynamicsWorld13getConstraintEi,@function
_ZN15btDynamicsWorld13getConstraintEi:  # @_ZN15btDynamicsWorld13getConstraintEi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end23:
	.size	_ZN15btDynamicsWorld13getConstraintEi, .Lfunc_end23-_ZN15btDynamicsWorld13getConstraintEi
	.cfi_endproc

	.section	.text._ZNK15btDynamicsWorld13getConstraintEi,"axG",@progbits,_ZNK15btDynamicsWorld13getConstraintEi,comdat
	.weak	_ZNK15btDynamicsWorld13getConstraintEi
	.p2align	4, 0x90
	.type	_ZNK15btDynamicsWorld13getConstraintEi,@function
_ZNK15btDynamicsWorld13getConstraintEi: # @_ZNK15btDynamicsWorld13getConstraintEi
	.cfi_startproc
# BB#0:
	xorl	%eax, %eax
	retq
.Lfunc_end24:
	.size	_ZNK15btDynamicsWorld13getConstraintEi, .Lfunc_end24-_ZNK15btDynamicsWorld13getConstraintEi
	.cfi_endproc

	.section	.text._ZNK21btSimpleDynamicsWorld12getWorldTypeEv,"axG",@progbits,_ZNK21btSimpleDynamicsWorld12getWorldTypeEv,comdat
	.weak	_ZNK21btSimpleDynamicsWorld12getWorldTypeEv
	.p2align	4, 0x90
	.type	_ZNK21btSimpleDynamicsWorld12getWorldTypeEv,@function
_ZNK21btSimpleDynamicsWorld12getWorldTypeEv: # @_ZNK21btSimpleDynamicsWorld12getWorldTypeEv
	.cfi_startproc
# BB#0:
	movl	$1, %eax
	retq
.Lfunc_end25:
	.size	_ZNK21btSimpleDynamicsWorld12getWorldTypeEv, .Lfunc_end25-_ZNK21btSimpleDynamicsWorld12getWorldTypeEv
	.cfi_endproc

	.section	.text._ZN15btDynamicsWorld10addVehicleEP17btActionInterface,"axG",@progbits,_ZN15btDynamicsWorld10addVehicleEP17btActionInterface,comdat
	.weak	_ZN15btDynamicsWorld10addVehicleEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN15btDynamicsWorld10addVehicleEP17btActionInterface,@function
_ZN15btDynamicsWorld10addVehicleEP17btActionInterface: # @_ZN15btDynamicsWorld10addVehicleEP17btActionInterface
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end26:
	.size	_ZN15btDynamicsWorld10addVehicleEP17btActionInterface, .Lfunc_end26-_ZN15btDynamicsWorld10addVehicleEP17btActionInterface
	.cfi_endproc

	.section	.text._ZN15btDynamicsWorld13removeVehicleEP17btActionInterface,"axG",@progbits,_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface,comdat
	.weak	_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface,@function
_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface: # @_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end27:
	.size	_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface, .Lfunc_end27-_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface
	.cfi_endproc

	.section	.text._ZN15btDynamicsWorld12addCharacterEP17btActionInterface,"axG",@progbits,_ZN15btDynamicsWorld12addCharacterEP17btActionInterface,comdat
	.weak	_ZN15btDynamicsWorld12addCharacterEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN15btDynamicsWorld12addCharacterEP17btActionInterface,@function
_ZN15btDynamicsWorld12addCharacterEP17btActionInterface: # @_ZN15btDynamicsWorld12addCharacterEP17btActionInterface
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end28:
	.size	_ZN15btDynamicsWorld12addCharacterEP17btActionInterface, .Lfunc_end28-_ZN15btDynamicsWorld12addCharacterEP17btActionInterface
	.cfi_endproc

	.section	.text._ZN15btDynamicsWorld15removeCharacterEP17btActionInterface,"axG",@progbits,_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface,comdat
	.weak	_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface
	.p2align	4, 0x90
	.type	_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface,@function
_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface: # @_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end29:
	.size	_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface, .Lfunc_end29-_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface
	.cfi_endproc

	.type	_ZTV21btSimpleDynamicsWorld,@object # @_ZTV21btSimpleDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTV21btSimpleDynamicsWorld
	.p2align	3
_ZTV21btSimpleDynamicsWorld:
	.quad	0
	.quad	_ZTI21btSimpleDynamicsWorld
	.quad	_ZN21btSimpleDynamicsWorldD2Ev
	.quad	_ZN21btSimpleDynamicsWorldD0Ev
	.quad	_ZN21btSimpleDynamicsWorld11updateAabbsEv
	.quad	_ZN16btCollisionWorld14setDebugDrawerEP12btIDebugDraw
	.quad	_ZN16btCollisionWorld14getDebugDrawerEv
	.quad	_ZN16btCollisionWorld18addCollisionObjectEP17btCollisionObjectss
	.quad	_ZN21btSimpleDynamicsWorld21removeCollisionObjectEP17btCollisionObject
	.quad	_ZN16btCollisionWorld33performDiscreteCollisionDetectionEv
	.quad	_ZN21btSimpleDynamicsWorld14stepSimulationEfif
	.quad	__cxa_pure_virtual
	.quad	_ZN15btDynamicsWorld13addConstraintEP17btTypedConstraintb
	.quad	_ZN15btDynamicsWorld16removeConstraintEP17btTypedConstraint
	.quad	__cxa_pure_virtual
	.quad	__cxa_pure_virtual
	.quad	_ZN21btSimpleDynamicsWorld10setGravityERK9btVector3
	.quad	_ZNK21btSimpleDynamicsWorld10getGravityEv
	.quad	_ZN21btSimpleDynamicsWorld23synchronizeMotionStatesEv
	.quad	_ZN21btSimpleDynamicsWorld12addRigidBodyEP11btRigidBody
	.quad	_ZN21btSimpleDynamicsWorld15removeRigidBodyEP11btRigidBody
	.quad	_ZN21btSimpleDynamicsWorld19setConstraintSolverEP18btConstraintSolver
	.quad	_ZN21btSimpleDynamicsWorld19getConstraintSolverEv
	.quad	_ZNK15btDynamicsWorld17getNumConstraintsEv
	.quad	_ZN15btDynamicsWorld13getConstraintEi
	.quad	_ZNK15btDynamicsWorld13getConstraintEi
	.quad	_ZNK21btSimpleDynamicsWorld12getWorldTypeEv
	.quad	_ZN21btSimpleDynamicsWorld11clearForcesEv
	.quad	_ZN15btDynamicsWorld10addVehicleEP17btActionInterface
	.quad	_ZN15btDynamicsWorld13removeVehicleEP17btActionInterface
	.quad	_ZN15btDynamicsWorld12addCharacterEP17btActionInterface
	.quad	_ZN15btDynamicsWorld15removeCharacterEP17btActionInterface
	.size	_ZTV21btSimpleDynamicsWorld, 256

	.type	_ZTS21btSimpleDynamicsWorld,@object # @_ZTS21btSimpleDynamicsWorld
	.globl	_ZTS21btSimpleDynamicsWorld
	.p2align	4
_ZTS21btSimpleDynamicsWorld:
	.asciz	"21btSimpleDynamicsWorld"
	.size	_ZTS21btSimpleDynamicsWorld, 24

	.type	_ZTS15btDynamicsWorld,@object # @_ZTS15btDynamicsWorld
	.section	.rodata._ZTS15btDynamicsWorld,"aG",@progbits,_ZTS15btDynamicsWorld,comdat
	.weak	_ZTS15btDynamicsWorld
	.p2align	4
_ZTS15btDynamicsWorld:
	.asciz	"15btDynamicsWorld"
	.size	_ZTS15btDynamicsWorld, 18

	.type	_ZTI15btDynamicsWorld,@object # @_ZTI15btDynamicsWorld
	.section	.rodata._ZTI15btDynamicsWorld,"aG",@progbits,_ZTI15btDynamicsWorld,comdat
	.weak	_ZTI15btDynamicsWorld
	.p2align	4
_ZTI15btDynamicsWorld:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS15btDynamicsWorld
	.quad	_ZTI16btCollisionWorld
	.size	_ZTI15btDynamicsWorld, 24

	.type	_ZTI21btSimpleDynamicsWorld,@object # @_ZTI21btSimpleDynamicsWorld
	.section	.rodata,"a",@progbits
	.globl	_ZTI21btSimpleDynamicsWorld
	.p2align	4
_ZTI21btSimpleDynamicsWorld:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS21btSimpleDynamicsWorld
	.quad	_ZTI15btDynamicsWorld
	.size	_ZTI21btSimpleDynamicsWorld, 24


	.globl	_ZN21btSimpleDynamicsWorldD1Ev
	.type	_ZN21btSimpleDynamicsWorldD1Ev,@function
_ZN21btSimpleDynamicsWorldD1Ev = _ZN21btSimpleDynamicsWorldD2Ev
	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
