	.text
	.file	"memory.bc"
	.globl	hypre_OutOfMemory
	.p2align	4, 0x90
	.type	hypre_OutOfMemory,@function
hypre_OutOfMemory:                      # @hypre_OutOfMemory
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi0:
	.cfi_def_cfa_offset 16
	movl	%edi, %ecx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ecx, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	xorl	%eax, %eax
	popq	%rcx
	retq
.Lfunc_end0:
	.size	hypre_OutOfMemory, .Lfunc_end0-hypre_OutOfMemory
	.cfi_endproc

	.globl	hypre_MAlloc
	.p2align	4, 0x90
	.type	hypre_MAlloc,@function
hypre_MAlloc:                           # @hypre_MAlloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi1:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi3:
	.cfi_def_cfa_offset 32
.Lcfi4:
	.cfi_offset %rbx, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movl	%edi, %ebp
	testl	%ebp, %ebp
	jle	.LBB1_1
# BB#2:
	movslq	%ebp, %rdi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB1_4
# BB#3:
	xorl	%ebx, %ebx
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebp, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	jmp	.LBB1_4
.LBB1_1:
	xorl	%ebx, %ebx
.LBB1_4:
	movq	%rbx, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%rbp
	retq
.Lfunc_end1:
	.size	hypre_MAlloc, .Lfunc_end1-hypre_MAlloc
	.cfi_endproc

	.globl	hypre_CAlloc
	.p2align	4, 0x90
	.type	hypre_CAlloc,@function
hypre_CAlloc:                           # @hypre_CAlloc
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi8:
	.cfi_def_cfa_offset 32
.Lcfi9:
	.cfi_offset %rbx, -24
.Lcfi10:
	.cfi_offset %r14, -16
	movl	%esi, %ebx
	imull	%edi, %ebx
	testl	%ebx, %ebx
	jle	.LBB2_1
# BB#2:
	movslq	%edi, %rdi
	movslq	%esi, %rsi
	callq	calloc
	movq	%rax, %r14
	testq	%r14, %r14
	jne	.LBB2_4
# BB#3:
	xorl	%r14d, %r14d
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%ebx, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
	jmp	.LBB2_4
.LBB2_1:
	xorl	%r14d, %r14d
.LBB2_4:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end2:
	.size	hypre_CAlloc, .Lfunc_end2-hypre_CAlloc
	.cfi_endproc

	.globl	hypre_ReAlloc
	.p2align	4, 0x90
	.type	hypre_ReAlloc,@function
hypre_ReAlloc:                          # @hypre_ReAlloc
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi11:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi12:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi13:
	.cfi_def_cfa_offset 32
.Lcfi14:
	.cfi_offset %rbx, -32
.Lcfi15:
	.cfi_offset %r14, -24
.Lcfi16:
	.cfi_offset %rbp, -16
	movl	%esi, %r14d
	movslq	%r14d, %rbp
	movq	%rbp, %rsi
	callq	realloc
	movq	%rax, %rbx
	testl	%ebp, %ebp
	jle	.LBB3_3
# BB#1:
	testq	%rbx, %rbx
	jne	.LBB3_3
# BB#2:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	movl	%r14d, %esi
	callq	printf
	movq	stdout(%rip), %rdi
	callq	fflush
.LBB3_3:
	movq	%rbx, %rax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end3:
	.size	hypre_ReAlloc, .Lfunc_end3-hypre_ReAlloc
	.cfi_endproc

	.globl	hypre_Free
	.p2align	4, 0x90
	.type	hypre_Free,@function
hypre_Free:                             # @hypre_Free
	.cfi_startproc
# BB#0:
	testq	%rdi, %rdi
	je	.LBB4_1
# BB#2:
	jmp	free                    # TAILCALL
.LBB4_1:
	retq
.Lfunc_end4:
	.size	hypre_Free, .Lfunc_end4-hypre_Free
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"Out of memory trying to allocate %d bytes\n"
	.size	.L.str, 43


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 8fbdf869211d89080ad5ef79f0278dd0469ce012)"
	.section	".note.GNU-stack","",@progbits
