	.text
	.file	"sei.bc"
	.globl	InitSEIMessages
	.p2align	4, 0x90
	.type	InitSEIMessages,@function
InitSEIMessages:                        # @InitSEIMessages
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi0:
	.cfi_def_cfa_offset 16
.Lcfi1:
	.cfi_offset %rbx, -16
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, sei_message+16(%rip)
	testq	%rax, %rax
	jne	.LBB0_2
# BB#1:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	sei_message+16(%rip), %rax
.LBB0_2:
	movb	$5, sei_message+8(%rip)
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	movq	%rax, %rdi
	callq	memset
	movq	$0, sei_message(%rip)
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, sei_message+40(%rip)
	testq	%rax, %rax
	jne	.LBB0_4
# BB#3:
	movl	$.L.str, %edi
	callq	no_mem_exit
	movq	sei_message+40(%rip), %rax
.LBB0_4:
	movb	$5, sei_message+32(%rip)
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	movq	%rax, %rdi
	callq	memset
	movl	$0, sei_message+28(%rip)
	movl	$0, sei_message+24(%rip)
	movq	$0, seiSparePicturePayload+16(%rip)
	callq	InitSparePicture
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiSubseqChar+72(%rip)
	testq	%rbx, %rbx
	jne	.LBB0_6
# BB#5:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
	movq	seiSubseqChar+72(%rip), %rbx
.LBB0_6:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB0_8
# BB#7:
	movl	$.L.str.8, %edi
	callq	no_mem_exit
	movq	seiSubseqChar+72(%rip), %rax
	movq	32(%rax), %rax
.LBB0_8:                                # %InitSubseqChar.exit
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	movq	%rax, %rdi
	callq	memset
	movq	seiSubseqChar+72(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiSubseqChar+80(%rip)
	movl	$0, seiHasSubseqChar(%rip)
	movq	img(%rip), %rax
	movslq	15248(%rax), %rax
	movl	%eax, seiSubseqChar(%rip)
	shlq	$5, %rax
	movl	seiSubseqInfo+4(%rax), %eax
	movl	%eax, seiSubseqChar+4(%rip)
	movl	$0, seiSubseqChar+8(%rip)
	movl	$0, seiSubseqChar+16(%rip)
	movl	$0, seiSubseqChar+28(%rip)
	movq	input(%rip), %rax
	cmpl	$0, 4736(%rax)
	je	.LBB0_10
# BB#9:
	movl	$1, seiHasSubseqLayerInfo(%rip)
	movq	$0, seiSubseqLayerInfo(%rip)
	movl	$2, seiSubseqLayerInfo+16(%rip)
.LBB0_10:
	movl	$1, seiHasSceneInformation(%rip)
	movq	$0, seiSceneInformation(%rip)
	movl	$-1, seiSceneInformation+8(%rip)
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiSceneInformation+16(%rip)
	testq	%rbx, %rbx
	jne	.LBB0_12
# BB#11:
	movl	$.L.str.9, %edi
	callq	no_mem_exit
	movq	seiSceneInformation+16(%rip), %rbx
.LBB0_12:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB0_14
# BB#13:
	movl	$.L.str.10, %edi
	callq	no_mem_exit
	movq	seiSceneInformation+16(%rip), %rbx
	movq	32(%rbx), %rax
.LBB0_14:                               # %InitSceneInformation.exit
	movl	$8, 4(%rbx)
	movl	$0, (%rbx)
	movb	$0, 8(%rbx)
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	movq	%rax, %rdi
	callq	memset
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiPanScanRectInfo+24(%rip)
	testq	%rbx, %rbx
	jne	.LBB0_16
# BB#15:
	movl	$.L.str.11, %edi
	callq	no_mem_exit
	movq	seiPanScanRectInfo+24(%rip), %rbx
.LBB0_16:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB0_18
# BB#17:
	movl	$.L.str.12, %edi
	callq	no_mem_exit
	movq	seiPanScanRectInfo+24(%rip), %rax
	movq	32(%rax), %rax
.LBB0_18:                               # %InitPanScanRectInfo.exit
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	movq	%rax, %rdi
	callq	memset
	movq	seiPanScanRectInfo+24(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiPanScanRectInfo+32(%rip)
	movl	$1, seiHasPanScanRectInfo(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, seiPanScanRectInfo+4(%rip)
	callq	InitUser_data_unregistered
	callq	InitUser_data_registered_itu_t_t35
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiRecoveryPoint+8(%rip)
	testq	%rbx, %rbx
	jne	.LBB0_20
# BB#19:
	movl	$.L.str.19, %edi
	callq	no_mem_exit
	movq	seiRecoveryPoint+8(%rip), %rbx
.LBB0_20:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB0_22
# BB#21:
	movl	$.L.str.20, %edi
	callq	no_mem_exit
	movq	seiRecoveryPoint+8(%rip), %rax
	movq	32(%rax), %rax
.LBB0_22:                               # %InitRandomAccess.exit
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	movq	%rax, %rdi
	callq	memset
	movq	seiRecoveryPoint+8(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiRecoveryPoint+16(%rip)
	movl	$0, seiRecoveryPoint(%rip)
	movb	$0, seiRecoveryPoint+5(%rip)
	movb	$0, seiRecoveryPoint+4(%rip)
	movl	$0, seiHasRecoveryPoint_info(%rip)
	popq	%rbx
	retq
.Lfunc_end0:
	.size	InitSEIMessages, .Lfunc_end0-InitSEIMessages
	.cfi_endproc

	.globl	clear_sei_message
	.p2align	4, 0x90
	.type	clear_sei_message,@function
clear_sei_message:                      # @clear_sei_message
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 16
.Lcfi3:
	.cfi_offset %rbx, -16
	movslq	%edi, %rbx
	shlq	$3, %rbx
	movq	sei_message+16(%rbx,%rbx,2), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	$0, sei_message(%rbx,%rbx,2)
	popq	%rbx
	retq
.Lfunc_end1:
	.size	clear_sei_message, .Lfunc_end1-clear_sei_message
	.cfi_endproc

	.globl	CloseSEIMessages
	.p2align	4, 0x90
	.type	CloseSEIMessages,@function
CloseSEIMessages:                       # @CloseSEIMessages
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi4:
	.cfi_def_cfa_offset 16
	movq	seiSubseqChar+72(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_2
# BB#1:
	movq	32(%rax), %rdi
	callq	free
	movq	seiSubseqChar+72(%rip), %rdi
	callq	free
.LBB2_2:                                # %CloseSubseqChar.exit
	movq	$0, seiSubseqChar+72(%rip)
	movq	seiSparePicturePayload+16(%rip), %rdi
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.LBB2_4
# BB#3:
	movq	%rax, %rdi
	callq	free
	movq	seiSparePicturePayload+16(%rip), %rdi
.LBB2_4:
	movq	$0, 32(%rdi)
	testq	%rdi, %rdi
	je	.LBB2_6
# BB#5:
	callq	free
.LBB2_6:                                # %CloseSparePicture.exit
	movq	$0, seiSparePicturePayload+16(%rip)
	movq	$0, seiSparePicturePayload(%rip)
	movq	seiSceneInformation+16(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_8
# BB#7:
	movq	32(%rax), %rdi
	callq	free
	movq	seiSceneInformation+16(%rip), %rdi
	callq	free
.LBB2_8:                                # %CloseSceneInformation.exit
	movq	$0, seiSceneInformation+16(%rip)
	movq	seiPanScanRectInfo+24(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_10
# BB#9:
	movq	32(%rax), %rdi
	callq	free
	movq	seiPanScanRectInfo+24(%rip), %rdi
	callq	free
.LBB2_10:                               # %ClosePanScanRectInfo.exit
	movq	$0, seiPanScanRectInfo+24(%rip)
	movq	seiUser_data_unregistered+16(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_12
# BB#11:
	movq	32(%rax), %rdi
	callq	free
	movq	seiUser_data_unregistered+16(%rip), %rdi
	callq	free
.LBB2_12:
	movq	$0, seiUser_data_unregistered+16(%rip)
	movq	seiUser_data_unregistered(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_14
# BB#13:
	callq	free
.LBB2_14:                               # %CloseUser_data_unregistered.exit
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_16
# BB#15:
	movq	32(%rax), %rdi
	callq	free
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rdi
	callq	free
.LBB2_16:
	movq	$0, seiUser_data_registered_itu_t_t35+24(%rip)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_18
# BB#17:
	callq	free
.LBB2_18:                               # %CloseUser_data_registered_itu_t_t35.exit
	movq	seiRecoveryPoint+8(%rip), %rax
	testq	%rax, %rax
	je	.LBB2_20
# BB#19:
	movq	32(%rax), %rdi
	callq	free
	movq	seiRecoveryPoint+8(%rip), %rdi
	callq	free
.LBB2_20:                               # %CloseRandomAccess.exit
	movq	$0, seiRecoveryPoint+8(%rip)
	movq	sei_message+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_22
# BB#21:
	callq	free
.LBB2_22:
	movq	$0, sei_message+16(%rip)
	movq	sei_message+40(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB2_24
# BB#23:
	callq	free
.LBB2_24:
	movq	$0, sei_message+40(%rip)
	popq	%rax
	retq
.Lfunc_end2:
	.size	CloseSEIMessages, .Lfunc_end2-CloseSEIMessages
	.cfi_endproc

	.globl	HaveAggregationSEI
	.p2align	4, 0x90
	.type	HaveAggregationSEI,@function
HaveAggregationSEI:                     # @HaveAggregationSEI
	.cfi_startproc
# BB#0:
	cmpl	$0, sei_message+24(%rip)
	je	.LBB3_3
# BB#1:
	movq	img(%rip), %rcx
	movl	$1, %eax
	cmpl	$1, 20(%rcx)
	jne	.LBB3_10
# BB#2:
	movl	seiHasSubseqInfo(%rip), %ecx
	testl	%ecx, %ecx
	je	.LBB3_4
	jmp	.LBB3_10
.LBB3_3:
	movl	$1, %eax
	cmpl	$0, seiHasSubseqInfo(%rip)
	jne	.LBB3_10
.LBB3_4:
	cmpl	$0, seiHasSubseqLayerInfo(%rip)
	je	.LBB3_7
# BB#5:
	movq	img(%rip), %rcx
	movl	$1, %eax
	cmpl	$0, (%rcx)
	je	.LBB3_10
# BB#6:
	movl	seiHasSceneInformation(%rip), %ecx
	orl	seiHasSubseqChar(%rip), %ecx
	orl	seiHasPanScanRectInfo(%rip), %ecx
	orl	seiHasUser_data_unregistered_info(%rip), %ecx
	orl	seiHasUser_data_registered_itu_t_t35_info(%rip), %ecx
	je	.LBB3_9
	jmp	.LBB3_10
.LBB3_7:
	movl	seiHasSceneInformation(%rip), %ecx
	orl	seiHasSubseqChar(%rip), %ecx
	orl	seiHasPanScanRectInfo(%rip), %ecx
	orl	seiHasUser_data_unregistered_info(%rip), %ecx
	movl	$1, %eax
	orl	seiHasUser_data_registered_itu_t_t35_info(%rip), %ecx
	jne	.LBB3_10
.LBB3_9:
	xorl	%eax, %eax
	cmpl	$0, seiHasRecoveryPoint_info(%rip)
	setne	%al
.LBB3_10:
	retq
.Lfunc_end3:
	.size	HaveAggregationSEI, .Lfunc_end3-HaveAggregationSEI
	.cfi_endproc

	.globl	write_sei_message
	.p2align	4, 0x90
	.type	write_sei_message,@function
write_sei_message:                      # @write_sei_message
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -32
.Lcfi9:
	.cfi_offset %r14, -24
.Lcfi10:
	.cfi_offset %r15, -16
	movl	%edx, %r14d
	movslq	%edi, %rdx
	shlq	$3, %rdx
	movl	sei_message+4(%rdx,%rdx,2), %ebx
	leaq	sei_message+16(%rdx,%rdx,2), %rax
	cmpl	$256, %ecx              # imm = 0x100
	jl	.LBB4_3
# BB#1:                                 # %.lr.ph39
	movslq	%ebx, %rbx
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	movb	$-1, (%rdi,%rbx)
	incq	%rbx
	addl	$-255, %ecx
	cmpl	$255, %ecx
	jg	.LBB4_2
.LBB4_3:                                # %._crit_edge40
	leaq	sei_message+4(%rdx,%rdx,2), %r15
	movq	(%rax), %rdi
	movslq	%ebx, %rdx
	movb	%cl, (%rdi,%rdx)
	incl	%edx
	cmpl	$256, %r14d             # imm = 0x100
	jl	.LBB4_4
# BB#5:                                 # %.lr.ph.preheader
	movslq	%edx, %rdx
	movl	%r14d, %ecx
	.p2align	4, 0x90
.LBB4_6:                                # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rax), %rdi
	movb	$-1, (%rdi,%rdx)
	addl	$-255, %ecx
	incq	%rdx
	incl	%ebx
	cmpl	$255, %ecx
	jg	.LBB4_6
# BB#7:                                 # %._crit_edge.loopexit
	leal	1(%rbx), %edx
	jmp	.LBB4_8
.LBB4_4:
	movl	%r14d, %ecx
.LBB4_8:                                # %._crit_edge
	movq	(%rax), %rdi
	movslq	%edx, %rdx
	movb	%cl, (%rdi,%rdx)
	movq	(%rax), %rax
	movslq	%ebx, %rcx
	leaq	2(%rax,%rcx), %rdi
	movslq	%r14d, %rdx
	callq	memcpy
	leal	2(%rbx,%r14), %eax
	movl	%eax, (%r15)
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	write_sei_message, .Lfunc_end4-write_sei_message
	.cfi_endproc

	.globl	finalize_sei_message
	.p2align	4, 0x90
	.type	finalize_sei_message,@function
finalize_sei_message:                   # @finalize_sei_message
	.cfi_startproc
# BB#0:
	movslq	%edi, %rax
	shlq	$3, %rax
	movq	sei_message+16(%rax,%rax,2), %rcx
	movslq	sei_message+4(%rax,%rax,2), %rdx
	movb	$-128, (%rcx,%rdx)
	incl	sei_message+4(%rax,%rax,2)
	movl	$1, sei_message(%rax,%rax,2)
	retq
.Lfunc_end5:
	.size	finalize_sei_message, .Lfunc_end5-finalize_sei_message
	.cfi_endproc

	.globl	AppendTmpbits2Buf
	.p2align	4, 0x90
	.type	AppendTmpbits2Buf,@function
AppendTmpbits2Buf:                      # @AppendTmpbits2Buf
	.cfi_startproc
# BB#0:
	cmpl	$0, (%rsi)
	jle	.LBB6_9
# BB#1:                                 # %.preheader.lr.ph
	movb	8(%rdi), %r9b
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB6_2:                                # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_3 Depth 2
	movl	$128, %edx
	movl	$8, %eax
	.p2align	4, 0x90
.LBB6_3:                                #   Parent Loop BB6_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addb	%r9b, %r9b
	movb	%r9b, 8(%rdi)
	movq	32(%rsi), %rcx
	movzbl	(%rcx,%r8), %ecx
	movzbl	%dl, %edx
	testl	%edx, %ecx
	je	.LBB6_5
# BB#4:                                 #   in Loop: Header=BB6_3 Depth=2
	orb	$1, %r9b
	movb	%r9b, 8(%rdi)
.LBB6_5:                                #   in Loop: Header=BB6_3 Depth=2
	decl	4(%rdi)
	jne	.LBB6_7
# BB#6:                                 #   in Loop: Header=BB6_3 Depth=2
	movl	$8, 4(%rdi)
	movq	32(%rdi), %r10
	movslq	(%rdi), %r11
	leal	1(%r11), %ecx
	movl	%ecx, (%rdi)
	movb	%r9b, (%r10,%r11)
	movb	$0, 8(%rdi)
	xorl	%r9d, %r9d
.LBB6_7:                                #   in Loop: Header=BB6_3 Depth=2
	shrl	%edx
	decl	%eax
	jne	.LBB6_3
# BB#8:                                 #   in Loop: Header=BB6_2 Depth=1
	incq	%r8
	movslq	(%rsi), %rax
	cmpq	%rax, %r8
	jl	.LBB6_2
.LBB6_9:                                # %._crit_edge
	movl	$8, %r8d
	subl	4(%rsi), %r8d
	jle	.LBB6_16
# BB#10:                                # %.lr.ph
	leal	-1(%r8), %ecx
	movl	$1, %edx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movb	8(%rdi), %cl
	.p2align	4, 0x90
.LBB6_11:                               # =>This Inner Loop Header: Depth=1
	addb	%cl, %cl
	movb	%cl, 8(%rdi)
	movzbl	8(%rsi), %eax
	movzbl	%dl, %edx
	testl	%edx, %eax
	je	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_11 Depth=1
	orb	$1, %cl
	movb	%cl, 8(%rdi)
.LBB6_13:                               #   in Loop: Header=BB6_11 Depth=1
	decl	4(%rdi)
	jne	.LBB6_15
# BB#14:                                #   in Loop: Header=BB6_11 Depth=1
	movl	$8, 4(%rdi)
	movq	32(%rdi), %r9
	movslq	(%rdi), %r10
	leal	1(%r10), %eax
	movl	%eax, (%rdi)
	movb	%cl, (%r9,%r10)
	movb	$0, 8(%rdi)
	xorl	%ecx, %ecx
.LBB6_15:                               #   in Loop: Header=BB6_11 Depth=1
	shrl	%edx
	decl	%r8d
	jne	.LBB6_11
.LBB6_16:                               # %.loopexit
	retq
.Lfunc_end6:
	.size	AppendTmpbits2Buf, .Lfunc_end6-AppendTmpbits2Buf
	.cfi_endproc

	.globl	InitSparePicture
	.p2align	4, 0x90
	.type	InitSparePicture,@function
InitSparePicture:                       # @InitSparePicture
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 16
.Lcfi12:
	.cfi_offset %rbx, -16
	movq	seiSparePicturePayload+16(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB7_6
# BB#1:
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.LBB7_2
# BB#3:
	movq	%rax, %rdi
	callq	free
	movq	seiSparePicturePayload+16(%rip), %rdi
	movq	$0, 32(%rdi)
	testq	%rdi, %rdi
	jne	.LBB7_4
	jmp	.LBB7_5
.LBB7_2:                                # %.thread
	movq	$0, 32(%rdi)
.LBB7_4:
	callq	free
.LBB7_5:                                # %CloseSparePicture.exit
	movq	$0, seiSparePicturePayload+16(%rip)
	movq	$0, seiSparePicturePayload(%rip)
.LBB7_6:
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiSparePicturePayload+16(%rip)
	testq	%rbx, %rbx
	jne	.LBB7_8
# BB#7:
	movl	$.L.str.1, %edi
	callq	no_mem_exit
	movq	seiSparePicturePayload+16(%rip), %rbx
.LBB7_8:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB7_10
# BB#9:
	movl	$.L.str.2, %edi
	callq	no_mem_exit
	movq	seiSparePicturePayload+16(%rip), %rbx
.LBB7_10:
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	$0, seiSparePicturePayload(%rip)
	movq	seiSparePicturePayload+16(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	popq	%rbx
	retq
.Lfunc_end7:
	.size	InitSparePicture, .Lfunc_end7-InitSparePicture
	.cfi_endproc

	.globl	CloseSparePicture
	.p2align	4, 0x90
	.type	CloseSparePicture,@function
CloseSparePicture:                      # @CloseSparePicture
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi13:
	.cfi_def_cfa_offset 16
	movq	seiSparePicturePayload+16(%rip), %rdi
	movq	32(%rdi), %rax
	testq	%rax, %rax
	je	.LBB8_2
# BB#1:
	movq	%rax, %rdi
	callq	free
	movq	seiSparePicturePayload+16(%rip), %rdi
.LBB8_2:
	movq	$0, 32(%rdi)
	testq	%rdi, %rdi
	je	.LBB8_4
# BB#3:
	callq	free
.LBB8_4:
	movq	$0, seiSparePicturePayload+16(%rip)
	movq	$0, seiSparePicturePayload(%rip)
	popq	%rax
	retq
.Lfunc_end8:
	.size	CloseSparePicture, .Lfunc_end8-CloseSparePicture
	.cfi_endproc

	.globl	CalculateSparePicture
	.p2align	4, 0x90
	.type	CalculateSparePicture,@function
CalculateSparePicture:                  # @CalculateSparePicture
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end9:
	.size	CalculateSparePicture, .Lfunc_end9-CalculateSparePicture
	.cfi_endproc

	.globl	ComposeSparePictureMessage
	.p2align	4, 0x90
	.type	ComposeSparePictureMessage,@function
ComposeSparePictureMessage:             # @ComposeSparePictureMessage
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi17:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi18:
	.cfi_def_cfa_offset 80
.Lcfi19:
	.cfi_offset %rbx, -40
.Lcfi20:
	.cfi_offset %r14, -32
.Lcfi21:
	.cfi_offset %r15, -24
.Lcfi22:
	.cfi_offset %rbp, -16
	movq	%rdx, %r14
	movl	%esi, %ebp
	movq	seiSparePicturePayload+16(%rip), %rbx
	movl	$0, (%rsp)
	movq	$ue_linfo, 32(%rsp)
	movl	%edi, 4(%rsp)
	movq	%rsp, %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	%ebp, 4(%rsp)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	AppendTmpbits2Buf
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end10:
	.size	ComposeSparePictureMessage, .Lfunc_end10-ComposeSparePictureMessage
	.cfi_endproc

	.globl	CompressSpareMBMap
	.p2align	4, 0x90
	.type	CompressSpareMBMap,@function
CompressSpareMBMap:                     # @CompressSpareMBMap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi23:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi24:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi25:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi26:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi27:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi28:
	.cfi_def_cfa_offset 56
	subq	$104, %rsp
.Lcfi29:
	.cfi_def_cfa_offset 160
.Lcfi30:
	.cfi_offset %rbx, -56
.Lcfi31:
	.cfi_offset %r12, -48
.Lcfi32:
	.cfi_offset %r13, -40
.Lcfi33:
	.cfi_offset %r14, -32
.Lcfi34:
	.cfi_offset %r15, -24
.Lcfi35:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, 56(%rsp)          # 8-byte Spill
	movq	img(%rip), %rcx
	movl	52(%rcx), %edx
	movl	68(%rcx), %edi
	movl	%edi, %eax
	sarl	$31, %eax
	shrl	$28, %eax
	addl	%edi, %eax
	sarl	$4, %eax
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	movl	%esi, %ebp
	imull	%eax, %ebp
	movl	$0, 64(%rsp)
	movq	$ue_linfo, 96(%rsp)
	xorl	%ebx, %ebx
	cmpl	$16, %edi
	jl	.LBB11_40
# BB#1:                                 # %.preheader155.lr.ph
	movq	%r14, 40(%rsp)          # 8-byte Spill
	movl	%ebp, 36(%rsp)          # 4-byte Spill
	leal	-1(%rsi), %edi
	shrl	$31, %edi
	leal	-1(%rsi,%rdi), %r8d
	sarl	%r8d
	leal	-1(%rax), %esi
	shrl	$31, %esi
	leal	-1(%rax,%rsi), %edi
	sarl	%edi
	movl	$1, %r15d
	xorl	%eax, %eax
	xorl	%ebx, %ebx
	xorl	%r13d, %r13d
	movl	%edi, %esi
	movq	%rsi, (%rsp)            # 8-byte Spill
	movl	%r8d, %esi
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	movl	%edi, %ebp
	movq	%r8, 16(%rsp)           # 8-byte Spill
	movl	%r8d, %r12d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB11_2:                               # %.preheader155
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_4 Depth 2
	movl	%eax, 52(%rsp)          # 4-byte Spill
	cmpl	$16, %edx
	jl	.LBB11_36
# BB#3:                                 # %.lr.ph169.preheader
                                        #   in Loop: Header=BB11_2 Depth=1
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB11_4:                               # %.lr.ph169
                                        #   Parent Loop BB11_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	%ebp, %rcx
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	(%rdx,%rcx,8), %rcx
	movslq	%r12d, %rdx
	cmpb	$0, (%rcx,%rdx)
	je	.LBB11_5
# BB#6:                                 #   in Loop: Header=BB11_4 Depth=2
	movl	%r8d, 68(%rsp)
	leaq	64(%rsp), %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
	callq	writeSyntaxElement2Buf_UVLC
	addl	%eax, %ebx
	xorl	%r8d, %r8d
	cmpl	$-1, %r13d
	je	.LBB11_8
	jmp	.LBB11_14
	.p2align	4, 0x90
.LBB11_5:                               #   in Loop: Header=BB11_4 Depth=2
	incl	%r8d
	cmpl	$-1, %r13d
	jne	.LBB11_14
.LBB11_8:                               #   in Loop: Header=BB11_4 Depth=2
	testl	%r15d, %r15d
	jne	.LBB11_14
# BB#9:                                 #   in Loop: Header=BB11_4 Depth=2
	cmpl	24(%rsp), %r12d         # 4-byte Folded Reload
	jle	.LBB11_11
# BB#10:                                #   in Loop: Header=BB11_4 Depth=2
	decl	%r12d
	xorl	%r15d, %r15d
	movl	$-1, %r13d
	jmp	.LBB11_35
	.p2align	4, 0x90
.LBB11_14:                              #   in Loop: Header=BB11_4 Depth=2
	cmpl	$1, %r13d
	jne	.LBB11_21
# BB#15:                                #   in Loop: Header=BB11_4 Depth=2
	testl	%r15d, %r15d
	jne	.LBB11_21
# BB#16:                                #   in Loop: Header=BB11_4 Depth=2
	cmpl	16(%rsp), %r12d         # 4-byte Folded Reload
	jge	.LBB11_18
# BB#17:                                #   in Loop: Header=BB11_4 Depth=2
	incl	%r12d
	xorl	%r15d, %r15d
	movl	$1, %r13d
	jmp	.LBB11_35
	.p2align	4, 0x90
.LBB11_11:                              #   in Loop: Header=BB11_4 Depth=2
	testl	%r12d, %r12d
	je	.LBB11_12
# BB#13:                                #   in Loop: Header=BB11_4 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	leal	-1(%rax), %ecx
	xorl	%r15d, %r15d
	cmpl	%eax, %r12d
	sete	%r15b
	cmovel	%ecx, %r12d
	cmovel	%ecx, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	$0, %r13d
	movl	$-1, %eax
	cmovnel	%eax, %r13d
	jmp	.LBB11_35
	.p2align	4, 0x90
.LBB11_21:                              #   in Loop: Header=BB11_4 Depth=2
	testl	%r13d, %r13d
	jne	.LBB11_28
# BB#22:                                #   in Loop: Header=BB11_4 Depth=2
	cmpl	$-1, %r15d
	jne	.LBB11_28
# BB#23:                                #   in Loop: Header=BB11_4 Depth=2
	cmpl	8(%rsp), %ebp           # 4-byte Folded Reload
	jle	.LBB11_25
# BB#24:                                #   in Loop: Header=BB11_4 Depth=2
	decl	%ebp
	movl	$-1, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB11_35
.LBB11_18:                              #   in Loop: Header=BB11_4 Depth=2
	movq	img(%rip), %rcx
	movl	52(%rcx), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	decl	%edx
	cmpl	%edx, %r12d
	jne	.LBB11_20
# BB#19:                                #   in Loop: Header=BB11_4 Depth=2
	movq	8(%rsp), %rcx           # 8-byte Reload
	decl	%ecx
	xorl	%r15d, %r15d
	movl	$-1, %r13d
	movq	%rcx, 8(%rsp)           # 8-byte Spill
	movl	%ecx, %ebp
	jmp	.LBB11_35
.LBB11_28:                              #   in Loop: Header=BB11_4 Depth=2
	testl	%r13d, %r13d
	jne	.LBB11_35
# BB#29:                                #   in Loop: Header=BB11_4 Depth=2
	cmpl	$1, %r15d
	jne	.LBB11_35
# BB#30:                                #   in Loop: Header=BB11_4 Depth=2
	cmpl	(%rsp), %ebp            # 4-byte Folded Reload
	jge	.LBB11_32
# BB#31:                                #   in Loop: Header=BB11_4 Depth=2
	incl	%ebp
	movl	$1, %r15d
	xorl	%r13d, %r13d
	jmp	.LBB11_35
.LBB11_12:                              #   in Loop: Header=BB11_4 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	incl	%eax
	movl	$1, %r13d
	xorl	%r12d, %r12d
	movq	%rax, (%rsp)            # 8-byte Spill
	movl	%eax, %ebp
	xorl	%r15d, %r15d
	jmp	.LBB11_35
.LBB11_25:                              #   in Loop: Header=BB11_4 Depth=2
	testl	%ebp, %ebp
	je	.LBB11_26
# BB#27:                                #   in Loop: Header=BB11_4 Depth=2
	movq	8(%rsp), %rdx           # 8-byte Reload
	leal	-1(%rdx), %ecx
	cmpl	%edx, %ebp
	cmovel	%ecx, %ebp
	cmovel	%ecx, %edx
	movq	%rdx, 8(%rsp)           # 8-byte Spill
	movl	$0, %r13d
	movl	$-1, %eax
	cmovel	%eax, %r13d
	movl	$0, %r15d
	cmovnel	%eax, %r15d
	jmp	.LBB11_35
.LBB11_20:                              #   in Loop: Header=BB11_4 Depth=2
	movq	16(%rsp), %rdx          # 8-byte Reload
	leal	1(%rdx), %ecx
	xorl	%r13d, %r13d
	cmpl	%edx, %r12d
	cmovel	%ecx, %r12d
	cmovel	%ecx, %edx
	movq	%rdx, 16(%rsp)          # 8-byte Spill
	setne	%r13b
	movl	$0, %r15d
	movl	$-1, %eax
	cmovel	%eax, %r15d
	jmp	.LBB11_35
.LBB11_32:                              #   in Loop: Header=BB11_4 Depth=2
	movq	img(%rip), %rcx
	movl	68(%rcx), %ecx
	movl	%ecx, %edx
	sarl	$31, %edx
	shrl	$28, %edx
	addl	%ecx, %edx
	sarl	$4, %edx
	decl	%edx
	cmpl	%edx, %ebp
	jne	.LBB11_34
# BB#33:                                #   in Loop: Header=BB11_4 Depth=2
	movq	16(%rsp), %rcx          # 8-byte Reload
	incl	%ecx
	movl	$-1, %r15d
	xorl	%r13d, %r13d
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%ecx, %r12d
	jmp	.LBB11_35
.LBB11_26:                              #   in Loop: Header=BB11_4 Depth=2
	movq	24(%rsp), %rax          # 8-byte Reload
	decl	%eax
	movl	$1, %r15d
	xorl	%ebp, %ebp
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movl	%eax, %r12d
	xorl	%r13d, %r13d
	jmp	.LBB11_35
.LBB11_34:                              #   in Loop: Header=BB11_4 Depth=2
	movq	(%rsp), %rax            # 8-byte Reload
	leal	1(%rax), %ecx
	xorl	%r13d, %r13d
	xorl	%r15d, %r15d
	cmpl	%eax, %ebp
	sete	%r13b
	cmovel	%ecx, %ebp
	cmovel	%ecx, %eax
	movq	%rax, (%rsp)            # 8-byte Spill
	setne	%r15b
	.p2align	4, 0x90
.LBB11_35:                              #   in Loop: Header=BB11_4 Depth=2
	incl	%r14d
	movq	img(%rip), %rcx
	movl	52(%rcx), %edx
	movl	%edx, %esi
	sarl	$31, %esi
	shrl	$28, %esi
	addl	%edx, %esi
	sarl	$4, %esi
	cmpl	%esi, %r14d
	jl	.LBB11_4
.LBB11_36:                              # %._crit_edge170
                                        #   in Loop: Header=BB11_2 Depth=1
	movl	52(%rsp), %eax          # 4-byte Reload
	incl	%eax
	movl	68(%rcx), %esi
	movl	%esi, %edi
	sarl	$31, %edi
	shrl	$28, %edi
	addl	%esi, %edi
	sarl	$4, %edi
	cmpl	%edi, %eax
	jl	.LBB11_2
# BB#37:                                # %._crit_edge191
	testl	%r8d, %r8d
	je	.LBB11_38
# BB#39:
	movl	%r8d, 68(%rsp)
	leaq	64(%rsp), %rdi
	movq	40(%rsp), %r14          # 8-byte Reload
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	addl	%eax, %ebx
	movl	36(%rsp), %ebp          # 4-byte Reload
	jmp	.LBB11_40
.LBB11_38:
	movl	36(%rsp), %ebp          # 4-byte Reload
	movq	40(%rsp), %r14          # 8-byte Reload
.LBB11_40:                              # %._crit_edge191.thread
	xorl	%eax, %eax
	cmpl	%ebp, %ebx
	setl	%al
	jl	.LBB11_51
# BB#41:
	movb	$0, 8(%r14)
	movabsq	$34359738368, %rcx      # imm = 0x800000000
	movq	%rcx, (%r14)
	movq	img(%rip), %rsi
	cmpl	$16, 68(%rsi)
	jl	.LBB11_51
# BB#42:                                # %.preheader.lr.ph
	xorl	%ecx, %ecx
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB11_43:                              # %.preheader
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB11_45 Depth 2
	cmpl	$16, 52(%rsi)
	jl	.LBB11_50
# BB#44:                                # %.lr.ph
                                        #   in Loop: Header=BB11_43 Depth=1
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB11_45:                              #   Parent Loop BB11_43 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addb	%cl, %cl
	movb	%cl, 8(%r14)
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	(%rbp,%rdx,8), %rbp
	cmpb	$0, (%rbp,%rdi)
	je	.LBB11_47
# BB#46:                                #   in Loop: Header=BB11_45 Depth=2
	orb	$1, %cl
	movb	%cl, 8(%r14)
.LBB11_47:                              #   in Loop: Header=BB11_45 Depth=2
	decl	4(%r14)
	jne	.LBB11_49
# BB#48:                                #   in Loop: Header=BB11_45 Depth=2
	movl	$8, 4(%r14)
	movq	32(%r14), %rsi
	movslq	(%r14), %rbp
	leal	1(%rbp), %ebx
	movl	%ebx, (%r14)
	movb	%cl, (%rsi,%rbp)
	movb	$0, 8(%r14)
	xorl	%ecx, %ecx
	movq	img(%rip), %rsi
.LBB11_49:                              #   in Loop: Header=BB11_45 Depth=2
	incq	%rdi
	movl	52(%rsi), %ebx
	movl	%ebx, %ebp
	sarl	$31, %ebp
	shrl	$28, %ebp
	addl	%ebx, %ebp
	sarl	$4, %ebp
	movslq	%ebp, %rbp
	cmpq	%rbp, %rdi
	jl	.LBB11_45
.LBB11_50:                              # %._crit_edge
                                        #   in Loop: Header=BB11_43 Depth=1
	incq	%rdx
	movl	68(%rsi), %ebp
	movl	%ebp, %edi
	sarl	$31, %edi
	shrl	$28, %edi
	addl	%ebp, %edi
	sarl	$4, %edi
	movslq	%edi, %rdi
	cmpq	%rdi, %rdx
	jl	.LBB11_43
.LBB11_51:                              # %.loopexit
	addq	$104, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end11:
	.size	CompressSpareMBMap, .Lfunc_end11-CompressSpareMBMap
	.cfi_endproc

	.globl	FinalizeSpareMBMap
	.p2align	4, 0x90
	.type	FinalizeSpareMBMap,@function
FinalizeSpareMBMap:                     # @FinalizeSpareMBMap
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi36:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi37:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi38:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi39:
	.cfi_def_cfa_offset 40
	subq	$40, %rsp
.Lcfi40:
	.cfi_def_cfa_offset 80
.Lcfi41:
	.cfi_offset %rbx, -40
.Lcfi42:
	.cfi_offset %r14, -32
.Lcfi43:
	.cfi_offset %r15, -24
.Lcfi44:
	.cfi_offset %rbp, -16
	movq	img(%rip), %rax
	movl	(%rax), %r15d
	movl	%r15d, %ebp
	sarl	$31, %ebp
	shrl	$24, %ebp
	addl	%r15d, %ebp
	andl	$-256, %ebp
	movl	$0, (%rsp)
	movq	$ue_linfo, 32(%rsp)
	movq	seiSparePicturePayload+16(%rip), %r14
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	testq	%rbx, %rbx
	jne	.LBB12_2
# BB#1:
	movl	$.L.str.3, %edi
	callq	no_mem_exit
.LBB12_2:
	subl	%ebp, %r15d
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB12_4
# BB#3:
	movl	$.L.str.4, %edi
	callq	no_mem_exit
	movq	32(%rbx), %rax
.LBB12_4:
	movl	$8, 4(%rbx)
	movl	$0, (%rbx)
	movb	$0, 8(%rbx)
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	movq	%rax, %rdi
	callq	memset
	movl	seiSparePicturePayload(%rip), %eax
	movl	%r15d, %ecx
	subl	%eax, %ecx
	addl	$256, %ecx              # imm = 0x100
	subl	%eax, %r15d
	cmovsl	%ecx, %r15d
	movl	%r15d, 4(%rsp)
	movq	%rsp, %r15
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiSparePicturePayload+4(%rip), %eax
	decl	%eax
	movl	%eax, 4(%rsp)
	movq	%r15, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	AppendTmpbits2Buf
	movl	4(%rbx), %ecx
	cmpl	$8, %ecx
	je	.LBB12_8
# BB#5:
	movb	8(%rbx), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%rbx)
	decl	%ecx
	movl	%ecx, 4(%rbx)
	je	.LBB12_7
# BB#6:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%rbx)
.LBB12_7:
	movl	$8, 4(%rbx)
	movq	32(%rbx), %rcx
	movslq	(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%rbx)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%rbx)
.LBB12_8:
	movl	(%rbx), %eax
	movl	%eax, seiSparePicturePayload+8(%rip)
	movq	%rbx, seiSparePicturePayload+16(%rip)
	movq	32(%r14), %rdi
	callq	free
	movq	%r14, %rdi
	callq	free
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	FinalizeSpareMBMap, .Lfunc_end12-FinalizeSpareMBMap
	.cfi_endproc

	.globl	InitSubseqInfo
	.p2align	4, 0x90
	.type	InitSubseqInfo,@function
InitSubseqInfo:                         # @InitSubseqInfo
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi45:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi46:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi47:
	.cfi_def_cfa_offset 32
.Lcfi48:
	.cfi_offset %rbx, -32
.Lcfi49:
	.cfi_offset %r14, -24
.Lcfi50:
	.cfi_offset %r15, -16
	movl	$1, seiHasSubseqInfo(%rip)
	movslq	%edi, %rax
	movq	%rax, %rbx
	shlq	$5, %rbx
	movl	%eax, seiSubseqInfo(%rbx)
	movzwl	InitSubseqInfo.id(%rip), %eax
	leal	1(%rax), %ecx
	movw	%cx, InitSubseqInfo.id(%rip)
	movl	%eax, seiSubseqInfo+4(%rbx)
	movl	$0, seiSubseqInfo+8(%rbx)
	movl	$-1, seiSubseqInfo+12(%rbx)
	movl	$0, seiSubseqInfo+16(%rbx)
	movl	$48, %edi
	callq	malloc
	movq	%rax, %r15
	leaq	seiSubseqInfo+24(%rbx), %r14
	movq	%r15, seiSubseqInfo+24(%rbx)
	testq	%r15, %r15
	jne	.LBB13_2
# BB#1:
	movl	$.L.str.5, %edi
	callq	no_mem_exit
	movq	(%r14), %r15
.LBB13_2:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%r15)
	testq	%rax, %rax
	jne	.LBB13_4
# BB#3:
	movl	$.L.str.6, %edi
	callq	no_mem_exit
	movq	(%r14), %r15
.LBB13_4:
	movl	$8, 4(%r15)
	movl	$0, (%r15)
	movb	$0, 8(%r15)
	movq	32(%r15), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	memset                  # TAILCALL
.Lfunc_end13:
	.size	InitSubseqInfo, .Lfunc_end13-InitSubseqInfo
	.cfi_endproc

	.globl	UpdateSubseqInfo
	.p2align	4, 0x90
	.type	UpdateSubseqInfo,@function
UpdateSubseqInfo:                       # @UpdateSubseqInfo
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	cmpl	$1, 20(%rax)
	je	.LBB14_2
# BB#1:
	movslq	%edi, %rcx
	shlq	$5, %rcx
	movl	seiSubseqInfo+12(%rcx), %edx
	incl	%edx
	movzbl	%dl, %edx
	movl	%edx, seiSubseqInfo+12(%rcx)
.LBB14_2:
	cmpl	$1, %edi
	je	.LBB14_5
# BB#3:
	testl	%edi, %edi
	jne	.LBB14_13
# BB#4:
	movq	input(%rip), %rcx
	movl	8(%rcx), %ecx
	decl	%ecx
	xorl	%edx, %edx
	cmpl	%ecx, (%rax)
	sete	%dl
	movl	%edx, seiSubseqInfo+8(%rip)
	retq
.LBB14_5:
	movl	(%rax), %ecx
	subl	start_frame_no_in_this_IGOP(%rip), %ecx
	movq	input(%rip), %r8
	movl	4736(%r8), %edi
	leal	1(%rdi), %esi
	movl	%ecx, %eax
	cltd
	idivl	%esi
	testl	%edx, %edx
	je	.LBB14_6
.LBB14_8:
	cmpl	%edi, %edx
	jne	.LBB14_11
# BB#9:
	cmpl	$0, 2096(%r8)
	je	.LBB14_10
.LBB14_11:
	xorl	%eax, %eax
	jmp	.LBB14_12
.LBB14_6:
	testl	%ecx, %ecx
	jle	.LBB14_8
# BB#7:
	movl	2096(%r8), %ecx
	movl	$1, %eax
	testl	%ecx, %ecx
	jne	.LBB14_12
	jmp	.LBB14_8
.LBB14_10:
	movl	$1, %eax
.LBB14_12:
	movl	%eax, seiSubseqInfo+40(%rip)
.LBB14_13:
	retq
.Lfunc_end14:
	.size	UpdateSubseqInfo, .Lfunc_end14-UpdateSubseqInfo
	.cfi_endproc

	.globl	FinalizeSubseqInfo
	.p2align	4, 0x90
	.type	FinalizeSubseqInfo,@function
FinalizeSubseqInfo:                     # @FinalizeSubseqInfo
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi51:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi52:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi53:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi54:
	.cfi_def_cfa_offset 80
.Lcfi55:
	.cfi_offset %rbx, -32
.Lcfi56:
	.cfi_offset %r14, -24
.Lcfi57:
	.cfi_offset %r15, -16
	movslq	%edi, %r15
	shlq	$5, %r15
	movq	seiSubseqInfo+24(%r15), %rbx
	movl	$0, 8(%rsp)
	movq	$ue_linfo, 40(%rsp)
	movl	seiSubseqInfo(%r15), %eax
	movl	%eax, 12(%rsp)
	leaq	8(%rsp), %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiSubseqInfo+4(%r15), %eax
	movl	%eax, 12(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiSubseqInfo+8(%r15), %eax
	movl	%eax, 28(%rsp)
	movl	$1, 20(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	movl	seiSubseqInfo+12(%r15), %eax
	movl	%eax, 12(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	4(%rbx), %ecx
	cmpl	$8, %ecx
	je	.LBB15_4
# BB#1:
	movb	8(%rbx), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%rbx)
	decl	%ecx
	movl	%ecx, 4(%rbx)
	je	.LBB15_3
# BB#2:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%rbx)
.LBB15_3:
	movl	$8, 4(%rbx)
	movq	32(%rbx), %rcx
	movslq	(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%rbx)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%rbx)
.LBB15_4:
	movl	(%rbx), %eax
	movl	%eax, seiSubseqInfo+16(%r15)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end15:
	.size	FinalizeSubseqInfo, .Lfunc_end15-FinalizeSubseqInfo
	.cfi_endproc

	.globl	ClearSubseqInfoPayload
	.p2align	4, 0x90
	.type	ClearSubseqInfoPayload,@function
ClearSubseqInfoPayload:                 # @ClearSubseqInfoPayload
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi58:
	.cfi_def_cfa_offset 16
.Lcfi59:
	.cfi_offset %rbx, -16
	movslq	%edi, %rbx
	shlq	$5, %rbx
	movq	seiSubseqInfo+24(%rbx), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movq	32(%rax), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movl	$0, seiSubseqInfo+16(%rbx)
	popq	%rbx
	retq
.Lfunc_end16:
	.size	ClearSubseqInfoPayload, .Lfunc_end16-ClearSubseqInfoPayload
	.cfi_endproc

	.globl	CloseSubseqInfo
	.p2align	4, 0x90
	.type	CloseSubseqInfo,@function
CloseSubseqInfo:                        # @CloseSubseqInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi60:
	.cfi_def_cfa_offset 16
.Lcfi61:
	.cfi_offset %rbx, -16
	movslq	%edi, %rbx
	shlq	$5, %rbx
	movl	$4294967295, %eax       # imm = 0xFFFFFFFF
	movq	%rax, seiSubseqInfo+12(%rbx)
	movq	seiSubseqInfo+24(%rbx), %rax
	movq	32(%rax), %rdi
	callq	free
	movq	seiSubseqInfo+24(%rbx), %rdi
	popq	%rbx
	jmp	free                    # TAILCALL
.Lfunc_end17:
	.size	CloseSubseqInfo, .Lfunc_end17-CloseSubseqInfo
	.cfi_endproc

	.globl	InitSubseqLayerInfo
	.p2align	4, 0x90
	.type	InitSubseqLayerInfo,@function
InitSubseqLayerInfo:                    # @InitSubseqLayerInfo
	.cfi_startproc
# BB#0:
	movl	$1, seiHasSubseqLayerInfo(%rip)
	movq	$0, seiSubseqLayerInfo(%rip)
	movl	$2, seiSubseqLayerInfo+16(%rip)
	retq
.Lfunc_end18:
	.size	InitSubseqLayerInfo, .Lfunc_end18-InitSubseqLayerInfo
	.cfi_endproc

	.globl	CloseSubseqLayerInfo
	.p2align	4, 0x90
	.type	CloseSubseqLayerInfo,@function
CloseSubseqLayerInfo:                   # @CloseSubseqLayerInfo
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end19:
	.size	CloseSubseqLayerInfo, .Lfunc_end19-CloseSubseqLayerInfo
	.cfi_endproc

	.globl	FinalizeSubseqLayerInfo
	.p2align	4, 0x90
	.type	FinalizeSubseqLayerInfo,@function
FinalizeSubseqLayerInfo:                # @FinalizeSubseqLayerInfo
	.cfi_startproc
# BB#0:
	movl	$0, seiSubseqLayerInfo+20(%rip)
	cmpl	$0, seiSubseqLayerInfo+16(%rip)
	jle	.LBB20_3
# BB#1:                                 # %.lr.ph.preheader
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB20_2:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movzwl	seiSubseqLayerInfo(%rax,%rax), %ecx
	movw	%cx, seiSubseqLayerInfo+8(,%rax,4)
	movzwl	seiSubseqLayerInfo+4(%rax,%rax), %ecx
	movw	%cx, seiSubseqLayerInfo+10(,%rax,4)
	addl	$4, seiSubseqLayerInfo+20(%rip)
	incq	%rax
	movslq	seiSubseqLayerInfo+16(%rip), %rcx
	cmpq	%rcx, %rax
	jl	.LBB20_2
.LBB20_3:                               # %._crit_edge
	retq
.Lfunc_end20:
	.size	FinalizeSubseqLayerInfo, .Lfunc_end20-FinalizeSubseqLayerInfo
	.cfi_endproc

	.globl	InitSubseqChar
	.p2align	4, 0x90
	.type	InitSubseqChar,@function
InitSubseqChar:                         # @InitSubseqChar
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi62:
	.cfi_def_cfa_offset 16
.Lcfi63:
	.cfi_offset %rbx, -16
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiSubseqChar+72(%rip)
	testq	%rbx, %rbx
	jne	.LBB21_2
# BB#1:
	movl	$.L.str.7, %edi
	callq	no_mem_exit
	movq	seiSubseqChar+72(%rip), %rbx
.LBB21_2:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB21_4
# BB#3:
	movl	$.L.str.8, %edi
	callq	no_mem_exit
	movq	seiSubseqChar+72(%rip), %rbx
.LBB21_4:
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiSubseqChar+72(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiSubseqChar+80(%rip)
	movl	$0, seiHasSubseqChar(%rip)
	movq	img(%rip), %rax
	movslq	15248(%rax), %rax
	movl	%eax, seiSubseqChar(%rip)
	shlq	$5, %rax
	movl	seiSubseqInfo+4(%rax), %eax
	movl	%eax, seiSubseqChar+4(%rip)
	movl	$0, seiSubseqChar+8(%rip)
	movl	$0, seiSubseqChar+16(%rip)
	movl	$0, seiSubseqChar+28(%rip)
	popq	%rbx
	retq
.Lfunc_end21:
	.size	InitSubseqChar, .Lfunc_end21-InitSubseqChar
	.cfi_endproc

	.globl	ClearSubseqCharPayload
	.p2align	4, 0x90
	.type	ClearSubseqCharPayload,@function
ClearSubseqCharPayload:                 # @ClearSubseqCharPayload
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi64:
	.cfi_def_cfa_offset 16
	movq	seiSubseqChar+72(%rip), %rax
	movq	32(%rax), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiSubseqChar+72(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiSubseqChar+80(%rip)
	movl	$0, seiHasSubseqChar(%rip)
	popq	%rax
	retq
.Lfunc_end22:
	.size	ClearSubseqCharPayload, .Lfunc_end22-ClearSubseqCharPayload
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI23_0:
	.long	0                       # 0x0
	.long	100                     # 0x64
	.long	30                      # 0x1e
	.long	0                       # 0x0
	.text
	.globl	UpdateSubseqChar
	.p2align	4, 0x90
	.type	UpdateSubseqChar,@function
UpdateSubseqChar:                       # @UpdateSubseqChar
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rax
	movslq	15248(%rax), %rax
	movl	%eax, seiSubseqChar(%rip)
	shlq	$5, %rax
	movl	seiSubseqInfo+4(%rax), %eax
	movl	%eax, seiSubseqChar+4(%rip)
	movl	$0, seiSubseqChar+8(%rip)
	movaps	.LCPI23_0(%rip), %xmm0  # xmm0 = [0,100,30,0]
	movups	%xmm0, seiSubseqChar+16(%rip)
	movl	$1, seiSubseqChar+32(%rip)
	movl	$2, seiSubseqChar+52(%rip)
	movl	$3, seiSubseqChar+36(%rip)
	movl	$4, seiSubseqChar+56(%rip)
	movl	$1, seiHasSubseqChar(%rip)
	retq
.Lfunc_end23:
	.size	UpdateSubseqChar, .Lfunc_end23-UpdateSubseqChar
	.cfi_endproc

	.globl	FinalizeSubseqChar
	.p2align	4, 0x90
	.type	FinalizeSubseqChar,@function
FinalizeSubseqChar:                     # @FinalizeSubseqChar
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi67:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi68:
	.cfi_def_cfa_offset 80
.Lcfi69:
	.cfi_offset %rbx, -32
.Lcfi70:
	.cfi_offset %r14, -24
.Lcfi71:
	.cfi_offset %r15, -16
	movq	seiSubseqChar+72(%rip), %r14
	movl	$0, 8(%rsp)
	movq	$ue_linfo, 40(%rsp)
	movl	seiSubseqChar(%rip), %eax
	movl	%eax, 12(%rsp)
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiSubseqChar+4(%rip), %eax
	movl	%eax, 12(%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiSubseqChar+8(%rip), %eax
	movl	%eax, 28(%rsp)
	movl	$1, 20(%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	cmpl	$0, seiSubseqChar+8(%rip)
	je	.LBB24_2
# BB#1:
	movl	seiSubseqChar+12(%rip), %eax
	movl	%eax, 28(%rsp)
	movl	$32, 20(%rsp)
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
.LBB24_2:
	movl	seiSubseqChar+16(%rip), %eax
	movl	%eax, 28(%rsp)
	movl	$1, 20(%rsp)
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	cmpl	$0, seiSubseqChar+16(%rip)
	je	.LBB24_4
# BB#3:
	movl	seiSubseqChar+20(%rip), %eax
	movl	%eax, 28(%rsp)
	movl	$16, 20(%rsp)
	leaq	8(%rsp), %rbx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	movl	seiSubseqChar+24(%rip), %eax
	movl	%eax, 28(%rsp)
	movl	$16, 20(%rsp)
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_Fixed
.LBB24_4:
	movl	seiSubseqChar+28(%rip), %eax
	movl	%eax, 12(%rsp)
	leaq	8(%rsp), %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	cmpl	$0, seiSubseqChar+28(%rip)
	jle	.LBB24_7
# BB#5:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r15
	.p2align	4, 0x90
.LBB24_6:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	seiSubseqChar+32(,%rbx,4), %eax
	movl	%eax, 12(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiSubseqChar+52(,%rbx,4), %eax
	movl	%eax, 12(%rsp)
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	incq	%rbx
	movslq	seiSubseqChar+28(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB24_6
.LBB24_7:                               # %._crit_edge
	movl	4(%r14), %ecx
	cmpl	$8, %ecx
	je	.LBB24_11
# BB#8:
	movb	8(%r14), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%r14)
	decl	%ecx
	movl	%ecx, 4(%r14)
	je	.LBB24_10
# BB#9:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%r14)
.LBB24_10:
	movl	$8, 4(%r14)
	movq	32(%r14), %rcx
	movslq	(%r14), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%r14)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%r14)
.LBB24_11:
	movl	(%r14), %eax
	movl	%eax, seiSubseqChar+80(%rip)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end24:
	.size	FinalizeSubseqChar, .Lfunc_end24-FinalizeSubseqChar
	.cfi_endproc

	.globl	CloseSubseqChar
	.p2align	4, 0x90
	.type	CloseSubseqChar,@function
CloseSubseqChar:                        # @CloseSubseqChar
	.cfi_startproc
# BB#0:
	movq	seiSubseqChar+72(%rip), %rax
	testq	%rax, %rax
	je	.LBB25_2
# BB#1:
	pushq	%rax
.Lcfi72:
	.cfi_def_cfa_offset 16
	movq	32(%rax), %rdi
	callq	free
	movq	seiSubseqChar+72(%rip), %rdi
	callq	free
	addq	$8, %rsp
.LBB25_2:
	movq	$0, seiSubseqChar+72(%rip)
	retq
.Lfunc_end25:
	.size	CloseSubseqChar, .Lfunc_end25-CloseSubseqChar
	.cfi_endproc

	.globl	InitSceneInformation
	.p2align	4, 0x90
	.type	InitSceneInformation,@function
InitSceneInformation:                   # @InitSceneInformation
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi73:
	.cfi_def_cfa_offset 16
.Lcfi74:
	.cfi_offset %rbx, -16
	movl	$1, seiHasSceneInformation(%rip)
	movq	$0, seiSceneInformation(%rip)
	movl	$-1, seiSceneInformation+8(%rip)
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiSceneInformation+16(%rip)
	testq	%rbx, %rbx
	jne	.LBB26_2
# BB#1:
	movl	$.L.str.9, %edi
	callq	no_mem_exit
	movq	seiSceneInformation+16(%rip), %rbx
.LBB26_2:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB26_4
# BB#3:
	movl	$.L.str.10, %edi
	callq	no_mem_exit
	movq	seiSceneInformation+16(%rip), %rbx
.LBB26_4:
	movl	$8, 4(%rbx)
	movl	$0, (%rbx)
	movb	$0, 8(%rbx)
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	popq	%rbx
	jmp	memset                  # TAILCALL
.Lfunc_end26:
	.size	InitSceneInformation, .Lfunc_end26-InitSceneInformation
	.cfi_endproc

	.globl	CloseSceneInformation
	.p2align	4, 0x90
	.type	CloseSceneInformation,@function
CloseSceneInformation:                  # @CloseSceneInformation
	.cfi_startproc
# BB#0:
	movq	seiSceneInformation+16(%rip), %rax
	testq	%rax, %rax
	je	.LBB27_2
# BB#1:
	pushq	%rax
.Lcfi75:
	.cfi_def_cfa_offset 16
	movq	32(%rax), %rdi
	callq	free
	movq	seiSceneInformation+16(%rip), %rdi
	callq	free
	addq	$8, %rsp
.LBB27_2:
	movq	$0, seiSceneInformation+16(%rip)
	retq
.Lfunc_end27:
	.size	CloseSceneInformation, .Lfunc_end27-CloseSceneInformation
	.cfi_endproc

	.globl	FinalizeSceneInformation
	.p2align	4, 0x90
	.type	FinalizeSceneInformation,@function
FinalizeSceneInformation:               # @FinalizeSceneInformation
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi76:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 64
.Lcfi79:
	.cfi_offset %rbx, -24
.Lcfi80:
	.cfi_offset %r14, -16
	movq	seiSceneInformation+16(%rip), %rbx
	movl	$0, (%rsp)
	movq	$ue_linfo, 32(%rsp)
	movl	seiSceneInformation(%rip), %eax
	movl	%eax, 20(%rsp)
	movl	$8, 12(%rsp)
	movq	%rsp, %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	movl	seiSceneInformation+4(%rip), %eax
	movl	%eax, 4(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	cmpl	$4, seiSceneInformation+4(%rip)
	jl	.LBB28_2
# BB#1:
	movl	seiSceneInformation+8(%rip), %eax
	movl	%eax, 20(%rsp)
	movl	$8, 12(%rsp)
	movq	%rsp, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_Fixed
.LBB28_2:
	movl	4(%rbx), %ecx
	cmpl	$8, %ecx
	je	.LBB28_6
# BB#3:
	movb	8(%rbx), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%rbx)
	decl	%ecx
	movl	%ecx, 4(%rbx)
	je	.LBB28_5
# BB#4:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%rbx)
.LBB28_5:
	movl	$8, 4(%rbx)
	movq	32(%rbx), %rcx
	movslq	(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%rbx)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%rbx)
.LBB28_6:
	movl	(%rbx), %eax
	movl	%eax, seiSceneInformation+24(%rip)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end28:
	.size	FinalizeSceneInformation, .Lfunc_end28-FinalizeSceneInformation
	.cfi_endproc

	.globl	UpdateSceneInformation
	.p2align	4, 0x90
	.type	UpdateSceneInformation,@function
UpdateSceneInformation:                 # @UpdateSceneInformation
	.cfi_startproc
# BB#0:
	movl	%edi, seiHasSceneInformation(%rip)
	movl	%esi, seiSceneInformation(%rip)
	movl	%edx, seiSceneInformation+4(%rip)
	cmpl	$4, %edx
	jl	.LBB29_2
# BB#1:
	movl	%ecx, seiSceneInformation+8(%rip)
.LBB29_2:
	retq
.Lfunc_end29:
	.size	UpdateSceneInformation, .Lfunc_end29-UpdateSceneInformation
	.cfi_endproc

	.globl	InitPanScanRectInfo
	.p2align	4, 0x90
	.type	InitPanScanRectInfo,@function
InitPanScanRectInfo:                    # @InitPanScanRectInfo
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi81:
	.cfi_def_cfa_offset 16
.Lcfi82:
	.cfi_offset %rbx, -16
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiPanScanRectInfo+24(%rip)
	testq	%rbx, %rbx
	jne	.LBB30_2
# BB#1:
	movl	$.L.str.11, %edi
	callq	no_mem_exit
	movq	seiPanScanRectInfo+24(%rip), %rbx
.LBB30_2:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB30_4
# BB#3:
	movl	$.L.str.12, %edi
	callq	no_mem_exit
	movq	seiPanScanRectInfo+24(%rip), %rbx
.LBB30_4:
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiPanScanRectInfo+24(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiPanScanRectInfo+32(%rip)
	movl	$1, seiHasPanScanRectInfo(%rip)
	xorps	%xmm0, %xmm0
	movups	%xmm0, seiPanScanRectInfo+4(%rip)
	popq	%rbx
	retq
.Lfunc_end30:
	.size	InitPanScanRectInfo, .Lfunc_end30-InitPanScanRectInfo
	.cfi_endproc

	.globl	ClearPanScanRectInfoPayload
	.p2align	4, 0x90
	.type	ClearPanScanRectInfoPayload,@function
ClearPanScanRectInfoPayload:            # @ClearPanScanRectInfoPayload
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi83:
	.cfi_def_cfa_offset 16
	movq	seiPanScanRectInfo+24(%rip), %rax
	movq	32(%rax), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiPanScanRectInfo+24(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiPanScanRectInfo+32(%rip)
	movl	$1, seiHasPanScanRectInfo(%rip)
	popq	%rax
	retq
.Lfunc_end31:
	.size	ClearPanScanRectInfoPayload, .Lfunc_end31-ClearPanScanRectInfoPayload
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI32_0:
	.long	3                       # 0x3
	.long	10                      # 0xa
	.long	40                      # 0x28
	.long	20                      # 0x14
	.text
	.globl	UpdatePanScanRectInfo
	.p2align	4, 0x90
	.type	UpdatePanScanRectInfo,@function
UpdatePanScanRectInfo:                  # @UpdatePanScanRectInfo
	.cfi_startproc
# BB#0:
	movaps	.LCPI32_0(%rip), %xmm0  # xmm0 = [3,10,40,20]
	movups	%xmm0, seiPanScanRectInfo(%rip)
	movl	$32, seiPanScanRectInfo+16(%rip)
	movl	$1, seiHasPanScanRectInfo(%rip)
	retq
.Lfunc_end32:
	.size	UpdatePanScanRectInfo, .Lfunc_end32-UpdatePanScanRectInfo
	.cfi_endproc

	.globl	FinalizePanScanRectInfo
	.p2align	4, 0x90
	.type	FinalizePanScanRectInfo,@function
FinalizePanScanRectInfo:                # @FinalizePanScanRectInfo
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi84:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi85:
	.cfi_def_cfa_offset 24
	subq	$40, %rsp
.Lcfi86:
	.cfi_def_cfa_offset 64
.Lcfi87:
	.cfi_offset %rbx, -24
.Lcfi88:
	.cfi_offset %r14, -16
	movq	seiPanScanRectInfo+24(%rip), %rbx
	movl	$0, (%rsp)
	movq	$ue_linfo, 32(%rsp)
	movl	seiPanScanRectInfo(%rip), %eax
	movl	%eax, 4(%rsp)
	movq	%rsp, %r14
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiPanScanRectInfo+4(%rip), %eax
	movl	%eax, 4(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiPanScanRectInfo+8(%rip), %eax
	movl	%eax, 4(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiPanScanRectInfo+12(%rip), %eax
	movl	%eax, 4(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	seiPanScanRectInfo+16(%rip), %eax
	movl	%eax, 4(%rsp)
	movq	%r14, %rdi
	movq	%rbx, %rsi
	callq	writeSyntaxElement2Buf_UVLC
	movl	4(%rbx), %ecx
	cmpl	$8, %ecx
	je	.LBB33_4
# BB#1:
	movb	8(%rbx), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%rbx)
	decl	%ecx
	movl	%ecx, 4(%rbx)
	je	.LBB33_3
# BB#2:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%rbx)
.LBB33_3:
	movl	$8, 4(%rbx)
	movq	32(%rbx), %rcx
	movslq	(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%rbx)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%rbx)
.LBB33_4:
	movl	(%rbx), %eax
	movl	%eax, seiPanScanRectInfo+32(%rip)
	addq	$40, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end33:
	.size	FinalizePanScanRectInfo, .Lfunc_end33-FinalizePanScanRectInfo
	.cfi_endproc

	.globl	ClosePanScanRectInfo
	.p2align	4, 0x90
	.type	ClosePanScanRectInfo,@function
ClosePanScanRectInfo:                   # @ClosePanScanRectInfo
	.cfi_startproc
# BB#0:
	movq	seiPanScanRectInfo+24(%rip), %rax
	testq	%rax, %rax
	je	.LBB34_2
# BB#1:
	pushq	%rax
.Lcfi89:
	.cfi_def_cfa_offset 16
	movq	32(%rax), %rdi
	callq	free
	movq	seiPanScanRectInfo+24(%rip), %rdi
	callq	free
	addq	$8, %rsp
.LBB34_2:
	movq	$0, seiPanScanRectInfo+24(%rip)
	retq
.Lfunc_end34:
	.size	ClosePanScanRectInfo, .Lfunc_end34-ClosePanScanRectInfo
	.cfi_endproc

	.globl	InitUser_data_unregistered
	.p2align	4, 0x90
	.type	InitUser_data_unregistered,@function
InitUser_data_unregistered:             # @InitUser_data_unregistered
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 16
.Lcfi91:
	.cfi_offset %rbx, -16
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiUser_data_unregistered+16(%rip)
	testq	%rbx, %rbx
	jne	.LBB35_2
# BB#1:
	movl	$.L.str.13, %edi
	callq	no_mem_exit
	movq	seiUser_data_unregistered+16(%rip), %rbx
.LBB35_2:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB35_4
# BB#3:
	movl	$.L.str.14, %edi
	callq	no_mem_exit
.LBB35_4:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, seiUser_data_unregistered(%rip)
	testq	%rax, %rax
	jne	.LBB35_6
# BB#5:
	movl	$.L.str.15, %edi
	callq	no_mem_exit
.LBB35_6:
	movq	seiUser_data_unregistered+16(%rip), %rax
	movq	32(%rax), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiUser_data_unregistered+16(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiUser_data_unregistered+24(%rip)
	movq	seiUser_data_unregistered(%rip), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movl	$0, seiUser_data_unregistered+8(%rip)
	movl	$1, seiHasUser_data_unregistered_info(%rip)
	popq	%rbx
	retq
.Lfunc_end35:
	.size	InitUser_data_unregistered, .Lfunc_end35-InitUser_data_unregistered
	.cfi_endproc

	.globl	ClearUser_data_unregistered
	.p2align	4, 0x90
	.type	ClearUser_data_unregistered,@function
ClearUser_data_unregistered:            # @ClearUser_data_unregistered
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi92:
	.cfi_def_cfa_offset 16
	movq	seiUser_data_unregistered+16(%rip), %rax
	movq	32(%rax), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiUser_data_unregistered+16(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiUser_data_unregistered+24(%rip)
	movq	seiUser_data_unregistered(%rip), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movl	$0, seiUser_data_unregistered+8(%rip)
	movl	$1, seiHasUser_data_unregistered_info(%rip)
	popq	%rax
	retq
.Lfunc_end36:
	.size	ClearUser_data_unregistered, .Lfunc_end36-ClearUser_data_unregistered
	.cfi_endproc

	.globl	UpdateUser_data_unregistered
	.p2align	4, 0x90
	.type	UpdateUser_data_unregistered,@function
UpdateUser_data_unregistered:           # @UpdateUser_data_unregistered
	.cfi_startproc
# BB#0:
	movq	seiUser_data_unregistered(%rip), %rax
	movb	$0, (%rax)
	movq	seiUser_data_unregistered(%rip), %rax
	movb	$4, 1(%rax)
	movq	seiUser_data_unregistered(%rip), %rax
	movb	$8, 2(%rax)
	movq	seiUser_data_unregistered(%rip), %rax
	movb	$12, 3(%rax)
	movq	seiUser_data_unregistered(%rip), %rax
	movb	$16, 4(%rax)
	movq	seiUser_data_unregistered(%rip), %rax
	movb	$20, 5(%rax)
	movq	seiUser_data_unregistered(%rip), %rax
	movb	$24, 6(%rax)
	movl	$7, seiUser_data_unregistered+8(%rip)
	retq
.Lfunc_end37:
	.size	UpdateUser_data_unregistered, .Lfunc_end37-UpdateUser_data_unregistered
	.cfi_endproc

	.globl	FinalizeUser_data_unregistered
	.p2align	4, 0x90
	.type	FinalizeUser_data_unregistered,@function
FinalizeUser_data_unregistered:         # @FinalizeUser_data_unregistered
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi93:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi94:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi95:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi96:
	.cfi_def_cfa_offset 80
.Lcfi97:
	.cfi_offset %rbx, -32
.Lcfi98:
	.cfi_offset %r14, -24
.Lcfi99:
	.cfi_offset %r15, -16
	movq	seiUser_data_unregistered+16(%rip), %r15
	movl	$0, 8(%rsp)
	movq	$ue_linfo, 40(%rsp)
	cmpl	$0, seiUser_data_unregistered+8(%rip)
	jle	.LBB38_3
# BB#1:                                 # %.lr.ph
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB38_2:                               # =>This Inner Loop Header: Depth=1
	movq	seiUser_data_unregistered(%rip), %rax
	movsbl	(%rax,%rbx), %eax
	movl	%eax, 28(%rsp)
	movl	$8, 20(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	incq	%rbx
	movslq	seiUser_data_unregistered+8(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB38_2
.LBB38_3:                               # %._crit_edge
	movl	4(%r15), %ecx
	cmpl	$8, %ecx
	je	.LBB38_7
# BB#4:
	movb	8(%r15), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%r15)
	decl	%ecx
	movl	%ecx, 4(%r15)
	je	.LBB38_6
# BB#5:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%r15)
.LBB38_6:
	movl	$8, 4(%r15)
	movq	32(%r15), %rcx
	movslq	(%r15), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%r15)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%r15)
.LBB38_7:
	movl	(%r15), %eax
	movl	%eax, seiUser_data_unregistered+24(%rip)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end38:
	.size	FinalizeUser_data_unregistered, .Lfunc_end38-FinalizeUser_data_unregistered
	.cfi_endproc

	.globl	CloseUser_data_unregistered
	.p2align	4, 0x90
	.type	CloseUser_data_unregistered,@function
CloseUser_data_unregistered:            # @CloseUser_data_unregistered
	.cfi_startproc
# BB#0:
	movq	seiUser_data_unregistered+16(%rip), %rax
	testq	%rax, %rax
	je	.LBB39_2
# BB#1:
	pushq	%rax
.Lcfi100:
	.cfi_def_cfa_offset 16
	movq	32(%rax), %rdi
	callq	free
	movq	seiUser_data_unregistered+16(%rip), %rdi
	callq	free
	addq	$8, %rsp
.LBB39_2:
	movq	$0, seiUser_data_unregistered+16(%rip)
	movq	seiUser_data_unregistered(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB39_3
# BB#4:
	jmp	free                    # TAILCALL
.LBB39_3:
	retq
.Lfunc_end39:
	.size	CloseUser_data_unregistered, .Lfunc_end39-CloseUser_data_unregistered
	.cfi_endproc

	.globl	InitUser_data_registered_itu_t_t35
	.p2align	4, 0x90
	.type	InitUser_data_registered_itu_t_t35,@function
InitUser_data_registered_itu_t_t35:     # @InitUser_data_registered_itu_t_t35
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi101:
	.cfi_def_cfa_offset 16
.Lcfi102:
	.cfi_offset %rbx, -16
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiUser_data_registered_itu_t_t35+24(%rip)
	testq	%rbx, %rbx
	jne	.LBB40_2
# BB#1:
	movl	$.L.str.16, %edi
	callq	no_mem_exit
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rbx
.LBB40_2:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB40_4
# BB#3:
	movl	$.L.str.17, %edi
	callq	no_mem_exit
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rbx
.LBB40_4:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, seiUser_data_registered_itu_t_t35(%rip)
	testq	%rbx, %rbx
	jne	.LBB40_6
# BB#5:
	movl	$.L.str.18, %edi
	callq	no_mem_exit
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rbx
.LBB40_6:
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiUser_data_registered_itu_t_t35+32(%rip)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movl	$0, seiUser_data_registered_itu_t_t35+8(%rip)
	movl	$0, seiUser_data_registered_itu_t_t35+12(%rip)
	movl	$0, seiUser_data_registered_itu_t_t35+16(%rip)
	movl	$1, seiHasUser_data_registered_itu_t_t35_info(%rip)
	popq	%rbx
	retq
.Lfunc_end40:
	.size	InitUser_data_registered_itu_t_t35, .Lfunc_end40-InitUser_data_registered_itu_t_t35
	.cfi_endproc

	.globl	ClearUser_data_registered_itu_t_t35
	.p2align	4, 0x90
	.type	ClearUser_data_registered_itu_t_t35,@function
ClearUser_data_registered_itu_t_t35:    # @ClearUser_data_registered_itu_t_t35
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi103:
	.cfi_def_cfa_offset 16
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rax
	movq	32(%rax), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiUser_data_registered_itu_t_t35+32(%rip)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movl	$0, seiUser_data_registered_itu_t_t35+8(%rip)
	movl	$0, seiUser_data_registered_itu_t_t35+12(%rip)
	movl	$0, seiUser_data_registered_itu_t_t35+16(%rip)
	movl	$1, seiHasUser_data_registered_itu_t_t35_info(%rip)
	popq	%rax
	retq
.Lfunc_end41:
	.size	ClearUser_data_registered_itu_t_t35, .Lfunc_end41-ClearUser_data_registered_itu_t_t35
	.cfi_endproc

	.globl	UpdateUser_data_registered_itu_t_t35
	.p2align	4, 0x90
	.type	UpdateUser_data_registered_itu_t_t35,@function
UpdateUser_data_registered_itu_t_t35:   # @UpdateUser_data_registered_itu_t_t35
	.cfi_startproc
# BB#0:
	movl	$82, seiUser_data_registered_itu_t_t35+12(%rip)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movb	$0, (%rax)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movb	$3, 1(%rax)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movb	$6, 2(%rax)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movb	$9, 3(%rax)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movb	$12, 4(%rax)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movb	$15, 5(%rax)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movb	$18, 6(%rax)
	movl	$7, seiUser_data_registered_itu_t_t35+8(%rip)
	retq
.Lfunc_end42:
	.size	UpdateUser_data_registered_itu_t_t35, .Lfunc_end42-UpdateUser_data_registered_itu_t_t35
	.cfi_endproc

	.globl	FinalizeUser_data_registered_itu_t_t35
	.p2align	4, 0x90
	.type	FinalizeUser_data_registered_itu_t_t35,@function
FinalizeUser_data_registered_itu_t_t35: # @FinalizeUser_data_registered_itu_t_t35
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi106:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi107:
	.cfi_def_cfa_offset 80
.Lcfi108:
	.cfi_offset %rbx, -32
.Lcfi109:
	.cfi_offset %r14, -24
.Lcfi110:
	.cfi_offset %r15, -16
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %r15
	movl	$0, 8(%rsp)
	movq	$ue_linfo, 40(%rsp)
	movl	seiUser_data_registered_itu_t_t35+12(%rip), %eax
	movl	%eax, 28(%rsp)
	movl	$8, 20(%rsp)
	leaq	8(%rsp), %rdi
	movq	%r15, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	cmpl	$255, seiUser_data_registered_itu_t_t35+12(%rip)
	jne	.LBB43_2
# BB#1:
	movl	seiUser_data_registered_itu_t_t35+16(%rip), %eax
	movl	%eax, 28(%rsp)
	movl	$8, 20(%rsp)
	leaq	8(%rsp), %rdi
	movq	%r15, %rsi
	callq	writeSyntaxElement2Buf_Fixed
.LBB43_2:                               # %.preheader
	cmpl	$0, seiUser_data_registered_itu_t_t35+8(%rip)
	jle	.LBB43_5
# BB#3:                                 # %.lr.ph.preheader
	xorl	%ebx, %ebx
	leaq	8(%rsp), %r14
	.p2align	4, 0x90
.LBB43_4:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	seiUser_data_registered_itu_t_t35(%rip), %rax
	movsbl	(%rax,%rbx), %eax
	movl	%eax, 28(%rsp)
	movl	$8, 20(%rsp)
	movq	%r14, %rdi
	movq	%r15, %rsi
	callq	writeSyntaxElement2Buf_Fixed
	incq	%rbx
	movslq	seiUser_data_registered_itu_t_t35+8(%rip), %rax
	cmpq	%rax, %rbx
	jl	.LBB43_4
.LBB43_5:                               # %._crit_edge
	movl	4(%r15), %ecx
	cmpl	$8, %ecx
	je	.LBB43_9
# BB#6:
	movb	8(%r15), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%r15)
	decl	%ecx
	movl	%ecx, 4(%r15)
	je	.LBB43_8
# BB#7:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%r15)
.LBB43_8:
	movl	$8, 4(%r15)
	movq	32(%r15), %rcx
	movslq	(%r15), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%r15)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%r15)
.LBB43_9:
	movl	(%r15), %eax
	movl	%eax, seiUser_data_registered_itu_t_t35+32(%rip)
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%r15
	retq
.Lfunc_end43:
	.size	FinalizeUser_data_registered_itu_t_t35, .Lfunc_end43-FinalizeUser_data_registered_itu_t_t35
	.cfi_endproc

	.globl	CloseUser_data_registered_itu_t_t35
	.p2align	4, 0x90
	.type	CloseUser_data_registered_itu_t_t35,@function
CloseUser_data_registered_itu_t_t35:    # @CloseUser_data_registered_itu_t_t35
	.cfi_startproc
# BB#0:
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rax
	testq	%rax, %rax
	je	.LBB44_2
# BB#1:
	pushq	%rax
.Lcfi111:
	.cfi_def_cfa_offset 16
	movq	32(%rax), %rdi
	callq	free
	movq	seiUser_data_registered_itu_t_t35+24(%rip), %rdi
	callq	free
	addq	$8, %rsp
.LBB44_2:
	movq	$0, seiUser_data_registered_itu_t_t35+24(%rip)
	movq	seiUser_data_registered_itu_t_t35(%rip), %rdi
	testq	%rdi, %rdi
	je	.LBB44_3
# BB#4:
	jmp	free                    # TAILCALL
.LBB44_3:
	retq
.Lfunc_end44:
	.size	CloseUser_data_registered_itu_t_t35, .Lfunc_end44-CloseUser_data_registered_itu_t_t35
	.cfi_endproc

	.globl	InitRandomAccess
	.p2align	4, 0x90
	.type	InitRandomAccess,@function
InitRandomAccess:                       # @InitRandomAccess
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi112:
	.cfi_def_cfa_offset 16
.Lcfi113:
	.cfi_offset %rbx, -16
	movl	$48, %edi
	callq	malloc
	movq	%rax, %rbx
	movq	%rbx, seiRecoveryPoint+8(%rip)
	testq	%rbx, %rbx
	jne	.LBB45_2
# BB#1:
	movl	$.L.str.19, %edi
	callq	no_mem_exit
	movq	seiRecoveryPoint+8(%rip), %rbx
.LBB45_2:
	movl	$65496, %edi            # imm = 0xFFD8
	callq	malloc
	movq	%rax, 32(%rbx)
	testq	%rax, %rax
	jne	.LBB45_4
# BB#3:
	movl	$.L.str.20, %edi
	callq	no_mem_exit
	movq	seiRecoveryPoint+8(%rip), %rbx
.LBB45_4:
	movq	32(%rbx), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiRecoveryPoint+8(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiRecoveryPoint+16(%rip)
	movl	$0, seiRecoveryPoint(%rip)
	movb	$0, seiRecoveryPoint+5(%rip)
	movb	$0, seiRecoveryPoint+4(%rip)
	movl	$0, seiHasRecoveryPoint_info(%rip)
	popq	%rbx
	retq
.Lfunc_end45:
	.size	InitRandomAccess, .Lfunc_end45-InitRandomAccess
	.cfi_endproc

	.globl	ClearRandomAccess
	.p2align	4, 0x90
	.type	ClearRandomAccess,@function
ClearRandomAccess:                      # @ClearRandomAccess
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi114:
	.cfi_def_cfa_offset 16
	movq	seiRecoveryPoint+8(%rip), %rax
	movq	32(%rax), %rdi
	xorl	%esi, %esi
	movl	$65496, %edx            # imm = 0xFFD8
	callq	memset
	movq	seiRecoveryPoint+8(%rip), %rax
	movl	$8, 4(%rax)
	movl	$0, (%rax)
	movb	$0, 8(%rax)
	movl	$0, seiRecoveryPoint+16(%rip)
	movl	$0, seiRecoveryPoint(%rip)
	movb	$0, seiRecoveryPoint+5(%rip)
	movb	$0, seiRecoveryPoint+4(%rip)
	movl	$0, seiHasRecoveryPoint_info(%rip)
	popq	%rax
	retq
.Lfunc_end46:
	.size	ClearRandomAccess, .Lfunc_end46-ClearRandomAccess
	.cfi_endproc

	.globl	UpdateRandomAccess
	.p2align	4, 0x90
	.type	UpdateRandomAccess,@function
UpdateRandomAccess:                     # @UpdateRandomAccess
	.cfi_startproc
# BB#0:
	movq	img(%rip), %rcx
	xorl	%eax, %eax
	cmpl	$2, 20(%rcx)
	jne	.LBB47_2
# BB#1:
	movl	$0, seiRecoveryPoint(%rip)
	movw	$1, seiRecoveryPoint+4(%rip)
	movl	$1, %eax
.LBB47_2:
	movl	%eax, seiHasRecoveryPoint_info(%rip)
	retq
.Lfunc_end47:
	.size	UpdateRandomAccess, .Lfunc_end47-UpdateRandomAccess
	.cfi_endproc

	.globl	FinalizeRandomAccess
	.p2align	4, 0x90
	.type	FinalizeRandomAccess,@function
FinalizeRandomAccess:                   # @FinalizeRandomAccess
	.cfi_startproc
# BB#0:
	pushq	%rbx
.Lcfi115:
	.cfi_def_cfa_offset 16
.Lcfi116:
	.cfi_offset %rbx, -16
	movq	seiRecoveryPoint+8(%rip), %rbx
	movl	seiRecoveryPoint(%rip), %esi
	movl	$.L.str.21, %edi
	movq	%rbx, %rdx
	callq	ue_v
	movzbl	seiRecoveryPoint+4(%rip), %esi
	movl	$.L.str.22, %edi
	movq	%rbx, %rdx
	callq	u_1
	movzbl	seiRecoveryPoint+5(%rip), %esi
	movl	$.L.str.23, %edi
	movq	%rbx, %rdx
	callq	u_1
	movzbl	seiRecoveryPoint+6(%rip), %edx
	movl	$2, %edi
	movl	$.L.str.24, %esi
	movq	%rbx, %rcx
	callq	u_v
	movl	4(%rbx), %ecx
	cmpl	$8, %ecx
	je	.LBB48_4
# BB#1:
	movb	8(%rbx), %al
	addb	%al, %al
	orb	$1, %al
	movb	%al, 8(%rbx)
	decl	%ecx
	movl	%ecx, 4(%rbx)
	je	.LBB48_3
# BB#2:
	movzbl	%al, %eax
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %eax
	movb	%al, 8(%rbx)
.LBB48_3:
	movl	$8, 4(%rbx)
	movq	32(%rbx), %rcx
	movslq	(%rbx), %rdx
	leal	1(%rdx), %esi
	movl	%esi, (%rbx)
	movb	%al, (%rcx,%rdx)
	movb	$0, 8(%rbx)
.LBB48_4:
	movl	(%rbx), %eax
	movl	%eax, seiRecoveryPoint+16(%rip)
	popq	%rbx
	retq
.Lfunc_end48:
	.size	FinalizeRandomAccess, .Lfunc_end48-FinalizeRandomAccess
	.cfi_endproc

	.globl	CloseRandomAccess
	.p2align	4, 0x90
	.type	CloseRandomAccess,@function
CloseRandomAccess:                      # @CloseRandomAccess
	.cfi_startproc
# BB#0:
	movq	seiRecoveryPoint+8(%rip), %rax
	testq	%rax, %rax
	je	.LBB49_2
# BB#1:
	pushq	%rax
.Lcfi117:
	.cfi_def_cfa_offset 16
	movq	32(%rax), %rdi
	callq	free
	movq	seiRecoveryPoint+8(%rip), %rdi
	callq	free
	addq	$8, %rsp
.LBB49_2:
	movq	$0, seiRecoveryPoint+8(%rip)
	retq
.Lfunc_end49:
	.size	CloseRandomAccess, .Lfunc_end49-CloseRandomAccess
	.cfi_endproc

	.type	seiHasTemporal_reference,@object # @seiHasTemporal_reference
	.bss
	.globl	seiHasTemporal_reference
	.p2align	2
seiHasTemporal_reference:
	.long	0                       # 0x0
	.size	seiHasTemporal_reference, 4

	.type	seiHasClock_timestamp,@object # @seiHasClock_timestamp
	.globl	seiHasClock_timestamp
	.p2align	2
seiHasClock_timestamp:
	.long	0                       # 0x0
	.size	seiHasClock_timestamp, 4

	.type	seiHasPanscan_rect,@object # @seiHasPanscan_rect
	.globl	seiHasPanscan_rect
	.p2align	2
seiHasPanscan_rect:
	.long	0                       # 0x0
	.size	seiHasPanscan_rect, 4

	.type	seiHasBuffering_period,@object # @seiHasBuffering_period
	.globl	seiHasBuffering_period
	.p2align	2
seiHasBuffering_period:
	.long	0                       # 0x0
	.size	seiHasBuffering_period, 4

	.type	seiHasHrd_picture,@object # @seiHasHrd_picture
	.globl	seiHasHrd_picture
	.p2align	2
seiHasHrd_picture:
	.long	0                       # 0x0
	.size	seiHasHrd_picture, 4

	.type	seiHasFiller_payload,@object # @seiHasFiller_payload
	.globl	seiHasFiller_payload
	.p2align	2
seiHasFiller_payload:
	.long	0                       # 0x0
	.size	seiHasFiller_payload, 4

	.type	seiHasUser_data_registered_itu_t_t35,@object # @seiHasUser_data_registered_itu_t_t35
	.globl	seiHasUser_data_registered_itu_t_t35
	.p2align	2
seiHasUser_data_registered_itu_t_t35:
	.long	0                       # 0x0
	.size	seiHasUser_data_registered_itu_t_t35, 4

	.type	seiHasUser_data_unregistered,@object # @seiHasUser_data_unregistered
	.globl	seiHasUser_data_unregistered
	.p2align	2
seiHasUser_data_unregistered:
	.long	0                       # 0x0
	.size	seiHasUser_data_unregistered, 4

	.type	seiHasRandom_access_point,@object # @seiHasRandom_access_point
	.globl	seiHasRandom_access_point
	.p2align	2
seiHasRandom_access_point:
	.long	0                       # 0x0
	.size	seiHasRandom_access_point, 4

	.type	seiHasRef_pic_buffer_management_repetition,@object # @seiHasRef_pic_buffer_management_repetition
	.globl	seiHasRef_pic_buffer_management_repetition
	.p2align	2
seiHasRef_pic_buffer_management_repetition:
	.long	0                       # 0x0
	.size	seiHasRef_pic_buffer_management_repetition, 4

	.type	seiHasSpare_picture,@object # @seiHasSpare_picture
	.globl	seiHasSpare_picture
	.p2align	2
seiHasSpare_picture:
	.long	0                       # 0x0
	.size	seiHasSpare_picture, 4

	.type	seiHasSceneInformation,@object # @seiHasSceneInformation
	.globl	seiHasSceneInformation
	.p2align	2
seiHasSceneInformation:
	.long	0                       # 0x0
	.size	seiHasSceneInformation, 4

	.type	seiHasSubseq_information,@object # @seiHasSubseq_information
	.globl	seiHasSubseq_information
	.p2align	2
seiHasSubseq_information:
	.long	0                       # 0x0
	.size	seiHasSubseq_information, 4

	.type	seiHasSubseq_layer_characteristics,@object # @seiHasSubseq_layer_characteristics
	.globl	seiHasSubseq_layer_characteristics
	.p2align	2
seiHasSubseq_layer_characteristics:
	.long	0                       # 0x0
	.size	seiHasSubseq_layer_characteristics, 4

	.type	seiHasSubseq_characteristics,@object # @seiHasSubseq_characteristics
	.globl	seiHasSubseq_characteristics
	.p2align	2
seiHasSubseq_characteristics:
	.long	0                       # 0x0
	.size	seiHasSubseq_characteristics, 4

	.type	sei_message,@object     # @sei_message
	.comm	sei_message,48,16
	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"InitSEIMessages: sei_message[i].data"
	.size	.L.str, 37

	.type	seiSparePicturePayload,@object # @seiSparePicturePayload
	.comm	seiSparePicturePayload,24,8
	.type	seiHasSubseqInfo,@object # @seiHasSubseqInfo
	.bss
	.globl	seiHasSubseqInfo
	.p2align	2
seiHasSubseqInfo:
	.long	0                       # 0x0
	.size	seiHasSubseqInfo, 4

	.type	seiHasSubseqLayerInfo,@object # @seiHasSubseqLayerInfo
	.globl	seiHasSubseqLayerInfo
	.p2align	2
seiHasSubseqLayerInfo:
	.long	0                       # 0x0
	.size	seiHasSubseqLayerInfo, 4

	.type	seiHasSubseqChar,@object # @seiHasSubseqChar
	.globl	seiHasSubseqChar
	.p2align	2
seiHasSubseqChar:
	.long	0                       # 0x0
	.size	seiHasSubseqChar, 4

	.type	seiHasPanScanRectInfo,@object # @seiHasPanScanRectInfo
	.globl	seiHasPanScanRectInfo
	.p2align	2
seiHasPanScanRectInfo:
	.long	0                       # 0x0
	.size	seiHasPanScanRectInfo, 4

	.type	seiHasUser_data_unregistered_info,@object # @seiHasUser_data_unregistered_info
	.comm	seiHasUser_data_unregistered_info,4,4
	.type	seiHasUser_data_registered_itu_t_t35_info,@object # @seiHasUser_data_registered_itu_t_t35_info
	.comm	seiHasUser_data_registered_itu_t_t35_info,4,4
	.type	seiHasRecoveryPoint_info,@object # @seiHasRecoveryPoint_info
	.comm	seiHasRecoveryPoint_info,4,4
	.type	seiHasSparePicture,@object # @seiHasSparePicture
	.globl	seiHasSparePicture
	.p2align	2
seiHasSparePicture:
	.long	0                       # 0x0
	.size	seiHasSparePicture, 4

	.type	.L.str.1,@object        # @.str.1
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.1:
	.asciz	"InitSparePicture: seiSparePicturePayload.data"
	.size	.L.str.1, 46

	.type	.L.str.2,@object        # @.str.2
.L.str.2:
	.asciz	"InitSparePicture: seiSparePicturePayload.data->streamBuffer"
	.size	.L.str.2, 60

	.type	.L.str.3,@object        # @.str.3
.L.str.3:
	.asciz	"FinalizeSpareMBMap: dest"
	.size	.L.str.3, 25

	.type	.L.str.4,@object        # @.str.4
.L.str.4:
	.asciz	"FinalizeSpareMBMap: dest->streamBuffer"
	.size	.L.str.4, 39

	.type	InitSubseqInfo.id,@object # @InitSubseqInfo.id
	.local	InitSubseqInfo.id
	.comm	InitSubseqInfo.id,2,2
	.type	seiSubseqInfo,@object   # @seiSubseqInfo
	.comm	seiSubseqInfo,64,16
	.type	.L.str.5,@object        # @.str.5
.L.str.5:
	.asciz	"InitSubseqInfo: seiSubseqInfo[currLayer].data"
	.size	.L.str.5, 46

	.type	.L.str.6,@object        # @.str.6
.L.str.6:
	.asciz	"InitSubseqInfo: seiSubseqInfo[currLayer].data->streamBuffer"
	.size	.L.str.6, 60

	.type	seiSubseqLayerInfo,@object # @seiSubseqLayerInfo
	.comm	seiSubseqLayerInfo,24,4
	.type	seiSubseqChar,@object   # @seiSubseqChar
	.comm	seiSubseqChar,88,8
	.type	.L.str.7,@object        # @.str.7
.L.str.7:
	.asciz	"InitSubseqChar: seiSubseqChar.data"
	.size	.L.str.7, 35

	.type	.L.str.8,@object        # @.str.8
.L.str.8:
	.asciz	"InitSubseqChar: seiSubseqChar.data->streamBuffer"
	.size	.L.str.8, 49

	.type	seiSceneInformation,@object # @seiSceneInformation
	.comm	seiSceneInformation,32,8
	.type	.L.str.9,@object        # @.str.9
.L.str.9:
	.asciz	"InitSceneInformation: seiSceneInformation.data"
	.size	.L.str.9, 47

	.type	.L.str.10,@object       # @.str.10
.L.str.10:
	.asciz	"InitSceneInformation: seiSceneInformation.data->streamBuffer"
	.size	.L.str.10, 61

	.type	seiPanScanRectInfo,@object # @seiPanScanRectInfo
	.comm	seiPanScanRectInfo,40,8
	.type	.L.str.11,@object       # @.str.11
.L.str.11:
	.asciz	"InitPanScanRectInfo: seiPanScanRectInfo.data"
	.size	.L.str.11, 45

	.type	.L.str.12,@object       # @.str.12
.L.str.12:
	.asciz	"InitPanScanRectInfo: seiPanScanRectInfo.data->streamBuffer"
	.size	.L.str.12, 59

	.type	seiUser_data_unregistered,@object # @seiUser_data_unregistered
	.comm	seiUser_data_unregistered,32,8
	.type	.L.str.13,@object       # @.str.13
.L.str.13:
	.asciz	"InitUser_data_unregistered: seiUser_data_unregistered.data"
	.size	.L.str.13, 59

	.type	.L.str.14,@object       # @.str.14
.L.str.14:
	.asciz	"InitUser_data_unregistered: seiUser_data_unregistered.data->streamBuffer"
	.size	.L.str.14, 73

	.type	.L.str.15,@object       # @.str.15
.L.str.15:
	.asciz	"InitUser_data_unregistered: seiUser_data_unregistered.byte"
	.size	.L.str.15, 59

	.type	seiUser_data_registered_itu_t_t35,@object # @seiUser_data_registered_itu_t_t35
	.comm	seiUser_data_registered_itu_t_t35,40,8
	.type	.L.str.16,@object       # @.str.16
.L.str.16:
	.asciz	"InitUser_data_unregistered: seiUser_data_registered_itu_t_t35.data"
	.size	.L.str.16, 67

	.type	.L.str.17,@object       # @.str.17
.L.str.17:
	.asciz	"InitUser_data_unregistered: seiUser_data_registered_itu_t_t35.data->streamBuffer"
	.size	.L.str.17, 81

	.type	.L.str.18,@object       # @.str.18
.L.str.18:
	.asciz	"InitUser_data_unregistered: seiUser_data_registered_itu_t_t35.byte"
	.size	.L.str.18, 67

	.type	seiRecoveryPoint,@object # @seiRecoveryPoint
	.comm	seiRecoveryPoint,24,8
	.type	.L.str.19,@object       # @.str.19
.L.str.19:
	.asciz	"InitRandomAccess: seiRandomAccess.data"
	.size	.L.str.19, 39

	.type	.L.str.20,@object       # @.str.20
.L.str.20:
	.asciz	"InitRandomAccess: seiRandomAccess.data->streamBuffer"
	.size	.L.str.20, 53

	.type	.L.str.21,@object       # @.str.21
.L.str.21:
	.asciz	"SEI: recovery_frame_cnt"
	.size	.L.str.21, 24

	.type	.L.str.22,@object       # @.str.22
.L.str.22:
	.asciz	"SEI: exact_match_flag"
	.size	.L.str.22, 22

	.type	.L.str.23,@object       # @.str.23
.L.str.23:
	.asciz	"SEI: broken_link_flag"
	.size	.L.str.23, 22

	.type	.L.str.24,@object       # @.str.24
.L.str.24:
	.asciz	"SEI: changing_slice_group_idc"
	.size	.L.str.24, 30

	.type	color_formats,@object   # @color_formats
	.comm	color_formats,4,4
	.type	top_pic,@object         # @top_pic
	.comm	top_pic,8,8
	.type	bottom_pic,@object      # @bottom_pic
	.comm	bottom_pic,8,8
	.type	frame_pic,@object       # @frame_pic
	.comm	frame_pic,8,8
	.type	frame_pic_1,@object     # @frame_pic_1
	.comm	frame_pic_1,8,8
	.type	frame_pic_2,@object     # @frame_pic_2
	.comm	frame_pic_2,8,8
	.type	frame_pic_3,@object     # @frame_pic_3
	.comm	frame_pic_3,8,8
	.type	frame_pic_si,@object    # @frame_pic_si
	.comm	frame_pic_si,8,8
	.type	Bit_Buffer,@object      # @Bit_Buffer
	.comm	Bit_Buffer,8,8
	.type	imgY_org,@object        # @imgY_org
	.comm	imgY_org,8,8
	.type	imgUV_org,@object       # @imgUV_org
	.comm	imgUV_org,8,8
	.type	imgY_sub_tmp,@object    # @imgY_sub_tmp
	.comm	imgY_sub_tmp,8,8
	.type	PicPos,@object          # @PicPos
	.comm	PicPos,8,8
	.type	log2_max_frame_num_minus4,@object # @log2_max_frame_num_minus4
	.comm	log2_max_frame_num_minus4,4,4
	.type	log2_max_pic_order_cnt_lsb_minus4,@object # @log2_max_pic_order_cnt_lsb_minus4
	.comm	log2_max_pic_order_cnt_lsb_minus4,4,4
	.type	me_tot_time,@object     # @me_tot_time
	.comm	me_tot_time,8,8
	.type	me_time,@object         # @me_time
	.comm	me_time,8,8
	.type	active_pps,@object      # @active_pps
	.comm	active_pps,8,8
	.type	active_sps,@object      # @active_sps
	.comm	active_sps,8,8
	.type	dsr_new_search_range,@object # @dsr_new_search_range
	.comm	dsr_new_search_range,4,4
	.type	mb_adaptive,@object     # @mb_adaptive
	.comm	mb_adaptive,4,4
	.type	MBPairIsField,@object   # @MBPairIsField
	.comm	MBPairIsField,4,4
	.type	wp_weight,@object       # @wp_weight
	.comm	wp_weight,8,8
	.type	wp_offset,@object       # @wp_offset
	.comm	wp_offset,8,8
	.type	wbp_weight,@object      # @wbp_weight
	.comm	wbp_weight,8,8
	.type	luma_log_weight_denom,@object # @luma_log_weight_denom
	.comm	luma_log_weight_denom,4,4
	.type	chroma_log_weight_denom,@object # @chroma_log_weight_denom
	.comm	chroma_log_weight_denom,4,4
	.type	wp_luma_round,@object   # @wp_luma_round
	.comm	wp_luma_round,4,4
	.type	wp_chroma_round,@object # @wp_chroma_round
	.comm	wp_chroma_round,4,4
	.type	imgY_org_top,@object    # @imgY_org_top
	.comm	imgY_org_top,8,8
	.type	imgY_org_bot,@object    # @imgY_org_bot
	.comm	imgY_org_bot,8,8
	.type	imgUV_org_top,@object   # @imgUV_org_top
	.comm	imgUV_org_top,8,8
	.type	imgUV_org_bot,@object   # @imgUV_org_bot
	.comm	imgUV_org_bot,8,8
	.type	imgY_org_frm,@object    # @imgY_org_frm
	.comm	imgY_org_frm,8,8
	.type	imgUV_org_frm,@object   # @imgUV_org_frm
	.comm	imgUV_org_frm,8,8
	.type	imgY_com,@object        # @imgY_com
	.comm	imgY_com,8,8
	.type	imgUV_com,@object       # @imgUV_com
	.comm	imgUV_com,8,8
	.type	direct_ref_idx,@object  # @direct_ref_idx
	.comm	direct_ref_idx,8,8
	.type	direct_pdir,@object     # @direct_pdir
	.comm	direct_pdir,8,8
	.type	pixel_map,@object       # @pixel_map
	.comm	pixel_map,8,8
	.type	refresh_map,@object     # @refresh_map
	.comm	refresh_map,8,8
	.type	intras,@object          # @intras
	.comm	intras,4,4
	.type	frame_ctr,@object       # @frame_ctr
	.comm	frame_ctr,20,16
	.type	frame_no,@object        # @frame_no
	.comm	frame_no,4,4
	.type	nextP_tr_fld,@object    # @nextP_tr_fld
	.comm	nextP_tr_fld,4,4
	.type	nextP_tr_frm,@object    # @nextP_tr_frm
	.comm	nextP_tr_frm,4,4
	.type	tot_time,@object        # @tot_time
	.comm	tot_time,8,8
	.type	errortext,@object       # @errortext
	.comm	errortext,300,16
	.type	b8_ipredmode8x8,@object # @b8_ipredmode8x8
	.comm	b8_ipredmode8x8,16,16
	.type	b8_intra_pred_modes8x8,@object # @b8_intra_pred_modes8x8
	.comm	b8_intra_pred_modes8x8,16,16
	.type	gop_structure,@object   # @gop_structure
	.comm	gop_structure,8,8
	.type	rdopt,@object           # @rdopt
	.comm	rdopt,8,8
	.type	rddata_top_frame_mb,@object # @rddata_top_frame_mb
	.comm	rddata_top_frame_mb,1752,8
	.type	rddata_bot_frame_mb,@object # @rddata_bot_frame_mb
	.comm	rddata_bot_frame_mb,1752,8
	.type	rddata_top_field_mb,@object # @rddata_top_field_mb
	.comm	rddata_top_field_mb,1752,8
	.type	rddata_bot_field_mb,@object # @rddata_bot_field_mb
	.comm	rddata_bot_field_mb,1752,8
	.type	p_stat,@object          # @p_stat
	.comm	p_stat,8,8
	.type	p_log,@object           # @p_log
	.comm	p_log,8,8
	.type	p_trace,@object         # @p_trace
	.comm	p_trace,8,8
	.type	p_in,@object            # @p_in
	.comm	p_in,4,4
	.type	p_dec,@object           # @p_dec
	.comm	p_dec,4,4
	.type	mb16x16_cost_frame,@object # @mb16x16_cost_frame
	.comm	mb16x16_cost_frame,8,8
	.type	Bytes_After_Header,@object # @Bytes_After_Header
	.comm	Bytes_After_Header,4,4
	.type	encode_one_macroblock,@object # @encode_one_macroblock
	.comm	encode_one_macroblock,8,8
	.type	lrec,@object            # @lrec
	.comm	lrec,8,8
	.type	lrec_uv,@object         # @lrec_uv
	.comm	lrec_uv,8,8
	.type	si_frame_indicator,@object # @si_frame_indicator
	.comm	si_frame_indicator,4,4
	.type	sp2_frame_indicator,@object # @sp2_frame_indicator
	.comm	sp2_frame_indicator,4,4
	.type	number_sp2_frames,@object # @number_sp2_frames
	.comm	number_sp2_frames,4,4
	.type	giRDOpt_B8OnlyFlag,@object # @giRDOpt_B8OnlyFlag
	.comm	giRDOpt_B8OnlyFlag,4,4
	.type	imgY_tmp,@object        # @imgY_tmp
	.comm	imgY_tmp,8,8
	.type	imgUV_tmp,@object       # @imgUV_tmp
	.comm	imgUV_tmp,16,16
	.type	frameNuminGOP,@object   # @frameNuminGOP
	.comm	frameNuminGOP,4,4
	.type	redundant_coding,@object # @redundant_coding
	.comm	redundant_coding,4,4
	.type	key_frame,@object       # @key_frame
	.comm	key_frame,4,4
	.type	redundant_ref_idx,@object # @redundant_ref_idx
	.comm	redundant_ref_idx,4,4
	.type	img_pad_size_uv_x,@object # @img_pad_size_uv_x
	.comm	img_pad_size_uv_x,4,4
	.type	img_pad_size_uv_y,@object # @img_pad_size_uv_y
	.comm	img_pad_size_uv_y,4,4
	.type	chroma_mask_mv_y,@object # @chroma_mask_mv_y
	.comm	chroma_mask_mv_y,1,1
	.type	chroma_mask_mv_x,@object # @chroma_mask_mv_x
	.comm	chroma_mask_mv_x,1,1
	.type	chroma_shift_y,@object  # @chroma_shift_y
	.comm	chroma_shift_y,4,4
	.type	chroma_shift_x,@object  # @chroma_shift_x
	.comm	chroma_shift_x,4,4
	.type	shift_cr_x,@object      # @shift_cr_x
	.comm	shift_cr_x,4,4
	.type	shift_cr_y,@object      # @shift_cr_y
	.comm	shift_cr_y,4,4
	.type	img_padded_size_x,@object # @img_padded_size_x
	.comm	img_padded_size_x,4,4
	.type	img_cr_padded_size_x,@object # @img_cr_padded_size_x
	.comm	img_cr_padded_size_x,4,4
	.type	start_me_refinement_hp,@object # @start_me_refinement_hp
	.comm	start_me_refinement_hp,4,4
	.type	start_me_refinement_qp,@object # @start_me_refinement_qp
	.comm	start_me_refinement_qp,4,4
	.type	WriteNALU,@object       # @WriteNALU
	.comm	WriteNALU,8,8
	.type	seiHasBufferingPeriod_info,@object # @seiHasBufferingPeriod_info
	.comm	seiHasBufferingPeriod_info,4,4
	.type	seiBufferingPeriod,@object # @seiBufferingPeriod
	.comm	seiBufferingPeriod,280,8
	.type	seiHasPicTiming_info,@object # @seiHasPicTiming_info
	.comm	seiHasPicTiming_info,4,4
	.type	seiPicTiming,@object    # @seiPicTiming
	.comm	seiPicTiming,152,8

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
