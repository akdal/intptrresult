	.text
	.file	"Update.bc"
	.globl	_ZN18COutMultiVolStream5CloseEv
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStream5CloseEv,@function
_ZN18COutMultiVolStream5CloseEv:        # @_ZN18COutMultiVolStream5CloseEv
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -32
.Lcfi4:
	.cfi_offset %r14, -24
.Lcfi5:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movl	52(%rbx), %eax
	testl	%eax, %eax
	jle	.LBB0_1
# BB#3:                                 # %.lr.ph
	xorl	%ebp, %ebp
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB0_4:                                # =>This Inner Loop Header: Depth=1
	movq	56(%rbx), %rcx
	movq	(%rcx,%rbp,8), %rcx
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_4 Depth=1
	callq	_ZN14COutFileStream5CloseEv
	testl	%eax, %eax
	cmovnel	%eax, %r14d
	movl	52(%rbx), %eax
.LBB0_6:                                #   in Loop: Header=BB0_4 Depth=1
	incq	%rbp
	movslq	%eax, %rcx
	cmpq	%rcx, %rbp
	jl	.LBB0_4
	jmp	.LBB0_2
.LBB0_1:
	xorl	%r14d, %r14d
.LBB0_2:                                # %._crit_edge
	movl	%r14d, %eax
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end0:
	.size	_ZN18COutMultiVolStream5CloseEv, .Lfunc_end0-_ZN18COutMultiVolStream5CloseEv
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.zero	16
	.text
	.globl	_ZN18COutMultiVolStream5WriteEPKvjPj
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStream5WriteEPKvjPj,@function
_ZN18COutMultiVolStream5WriteEPKvjPj:   # @_ZN18COutMultiVolStream5WriteEPKvjPj
.Lfunc_begin0:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception0
# BB#0:
	pushq	%rbp
.Lcfi6:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi7:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi8:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi9:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi10:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi11:
	.cfi_def_cfa_offset 56
	subq	$184, %rsp
.Lcfi12:
	.cfi_def_cfa_offset 240
.Lcfi13:
	.cfi_offset %rbx, -56
.Lcfi14:
	.cfi_offset %r12, -48
.Lcfi15:
	.cfi_offset %r13, -40
.Lcfi16:
	.cfi_offset %r14, -32
.Lcfi17:
	.cfi_offset %r15, -24
.Lcfi18:
	.cfi_offset %rbp, -16
	movl	%edx, %r13d
	movq	%rsi, 80(%rsp)          # 8-byte Spill
	movq	%rdi, %r15
	testq	%rcx, %rcx
	je	.LBB1_2
# BB#1:
	movl	$0, (%rcx)
.LBB1_2:                                # %.preheader251
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	leaq	40(%r15), %rax
	movq	%rax, 88(%rsp)          # 8-byte Spill
                                        # implicit-def: %R12D
	movq	%r15, 48(%rsp)          # 8-byte Spill
	movl	%r13d, 60(%rsp)         # 4-byte Spill
.LBB1_3:                                # %.outer
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_6 Depth 2
                                        #     Child Loop BB1_10 Depth 2
                                        #     Child Loop BB1_13 Depth 2
                                        #     Child Loop BB1_16 Depth 2
                                        #       Child Loop BB1_19 Depth 3
                                        #       Child Loop BB1_24 Depth 3
                                        #       Child Loop BB1_32 Depth 3
                                        #     Child Loop BB1_41 Depth 2
                                        #     Child Loop BB1_56 Depth 2
                                        #     Child Loop BB1_65 Depth 2
                                        #     Child Loop BB1_71 Depth 2
                                        #     Child Loop BB1_76 Depth 2
	testl	%r13d, %r13d
	je	.LBB1_99
# BB#4:                                 # %.lr.ph
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	12(%r15), %ebx
	movl	52(%r15), %r8d
	cmpl	%r8d, %ebx
	jge	.LBB1_8
# BB#5:                                 # %.lr.ph849.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movslq	%ebx, %rbx
	movl	84(%r15), %ecx
	leal	-1(%rcx), %edx
	movq	88(%r15), %rdi
	.p2align	4, 0x90
.LBB1_6:                                # %.lr.ph849
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	%ecx, %ebx
	movl	%edx, %eax
	cmovll	%ebx, %eax
	cltq
	movq	(%rdi,%rax,8), %rbp
	movq	16(%r15), %rsi
	movq	%rsi, %rax
	subq	%rbp, %rax
	jb	.LBB1_84
# BB#7:                                 # %.thread
                                        #   in Loop: Header=BB1_6 Depth=2
	movq	%rax, 16(%r15)
	incq	%rbx
	movl	%ebx, 12(%r15)
	cmpl	%r8d, %ebx
	jl	.LBB1_6
.LBB1_8:                                # %.us-lcssa.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%r12d, 28(%rsp)         # 4-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	$0, (%rax)
	incl	%ebx
.Ltmp0:
	movl	%ebx, %edi
	leaq	112(%rsp), %rsi
	callq	_Z21ConvertUInt32ToStringjPw
.Ltmp1:
# BB#9:                                 # %.preheader.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	$-1, %ebp
	leaq	112(%rsp), %rax
	movabsq	$-4294967296, %r12      # imm = 0xFFFFFFFF00000000
	movabsq	$4294967296, %rcx       # imm = 0x100000000
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	%rcx, %r12
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB1_10
# BB#11:                                # %_Z11MyStringLenIwEiPKT_.exit.i
                                        #   in Loop: Header=BB1_3 Depth=1
	leal	1(%rbp), %ebx
	movslq	%ebx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp2:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp3:
# BB#12:                                # %.noexc
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	$0, (%r13)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_13:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	112(%rsp,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_13
# BB#14:                                # %_ZN11CStringBaseIwEC2EPKw.exit.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	cmpl	$2, %ebp
	jg	.LBB1_35
# BB#15:                                # %.lr.ph439.preheader
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%ebx, 8(%rsp)           # 4-byte Spill
	sarq	$32, %r12
	movq	%r13, %rax
	.p2align	4, 0x90
.LBB1_16:                               # %.lr.ph439
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_19 Depth 3
                                        #       Child Loop BB1_24 Depth 3
                                        #       Child Loop BB1_32 Depth 3
.Ltmp5:
	movq	%rax, 32(%rsp)          # 8-byte Spill
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp6:
# BB#17:                                # %._crit_edge16.i.i.i
                                        #   in Loop: Header=BB1_16 Depth=2
	movq	$48, (%rbx)
.Ltmp8:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp9:
	movl	8(%rsp), %r15d          # 4-byte Reload
# BB#18:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
                                        #   in Loop: Header=BB1_16 Depth=2
	movl	$48, (%rbp)
	movl	$4, %eax
	.p2align	4, 0x90
.LBB1_19:                               # %._crit_edge
                                        #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbx,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_19
# BB#20:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
                                        #   in Loop: Header=BB1_16 Depth=2
	testq	%r12, %r12
	jle	.LBB1_23
# BB#21:                                #   in Loop: Header=BB1_16 Depth=2
.Ltmp11:
	movl	$28, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp12:
# BB#22:                                # %.lr.ph.i.i
                                        #   in Loop: Header=BB1_16 Depth=2
	movl	(%rbp), %eax
	movl	%eax, (%r14)
	movq	%rbp, %rdi
	callq	_ZdaPv
	movl	$0, 4(%r14)
	movq	%r14, %rbp
.LBB1_23:                               # %.noexc.i
                                        #   in Loop: Header=BB1_16 Depth=2
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_24:                               #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%r13,%rcx), %eax
	movl	%eax, 4(%rbp,%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB1_24
# BB#25:                                #   in Loop: Header=BB1_16 Depth=2
	movl	$0, (%r13)
	leaq	2(%r12), %r14
	cmpl	%r15d, %r14d
	jne	.LBB1_27
# BB#26:                                #   in Loop: Header=BB1_16 Depth=2
	movl	%r15d, %r14d
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB1_31
	.p2align	4, 0x90
.LBB1_27:                               #   in Loop: Header=BB1_16 Depth=2
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp14:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp15:
# BB#28:                                # %.noexc80
                                        #   in Loop: Header=BB1_16 Depth=2
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB1_30
# BB#29:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB1_16 Depth=2
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB1_30:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB1_16 Depth=2
	movl	$0, (%r15)
	movq	%r15, %r13
.LBB1_31:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i77
                                        #   in Loop: Header=BB1_16 Depth=2
	incq	%r12
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_32:                               #   Parent Loop BB1_3 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_32
# BB#33:                                # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        #   in Loop: Header=BB1_16 Depth=2
	movq	%rbp, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_ZdaPv
	cmpq	$3, %r12
	movq	%r15, %rax
	movl	%r14d, %ecx
	movl	%ecx, 8(%rsp)           # 4-byte Spill
	jl	.LBB1_16
# BB#34:                                # %_ZN11CStringBaseIwEC2EPKw.exit._crit_edge.loopexit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%r15, 40(%rsp)          # 8-byte Spill
	movl	%r12d, %ebp
	jmp	.LBB1_36
	.p2align	4, 0x90
.LBB1_35:                               #   in Loop: Header=BB1_3 Depth=1
	movq	%r13, 40(%rsp)          # 8-byte Spill
.LBB1_36:                               # %_ZN11CStringBaseIwEC2EPKw.exit._crit_edge
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	48(%rsp), %r15          # 8-byte Reload
	movslq	112(%r15), %rdi
	leaq	1(%rdi), %rbx
	testl	%ebx, %ebx
	movq	%rdi, 8(%rsp)           # 8-byte Spill
	je	.LBB1_39
# BB#37:                                # %._crit_edge16.i.i.i84
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp17:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp18:
# BB#38:                                # %.noexc93
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	$0, (%r14)
	movq	%r14, %r12
	movq	8(%rsp), %rdi           # 8-byte Reload
	jmp	.LBB1_40
	.p2align	4, 0x90
.LBB1_39:                               #   in Loop: Header=BB1_3 Depth=1
	xorl	%ebx, %ebx
	xorl	%r14d, %r14d
	xorl	%r12d, %r12d
.LBB1_40:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i85
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	104(%r15), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_41:                               #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx), %edx
	movl	%edx, (%r12,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB1_41
# BB#42:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i88
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%ebx, %eax
	subl	%edi, %eax
	cmpl	%ebp, %eax
	jle	.LBB1_44
# BB#43:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%r14, %r15
	jmp	.LBB1_55
	.p2align	4, 0x90
.LBB1_44:                               #   in Loop: Header=BB1_3 Depth=1
	decl	%eax
	cmpl	$8, %ebx
	movl	$4, %ecx
	movl	$16, %edx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB1_46
# BB#45:                                # %select.true.sink
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB1_46:                               # %select.end
                                        #   in Loop: Header=BB1_3 Depth=1
	leal	(%rcx,%rax), %edx
	movl	%ebp, %esi
	subl	%eax, %esi
	cmpl	%ebp, %edx
	cmovgel	%ecx, %esi
	leal	1(%rbx,%rsi), %eax
	cmpl	%ebx, %eax
	jne	.LBB1_48
# BB#47:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%r14, %r15
	jmp	.LBB1_55
.LBB1_48:                               #   in Loop: Header=BB1_3 Depth=1
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp20:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp21:
# BB#49:                                # %.noexc160
                                        #   in Loop: Header=BB1_3 Depth=1
	testl	%ebx, %ebx
	movq	8(%rsp), %rdi           # 8-byte Reload
	jle	.LBB1_54
# BB#50:                                # %.preheader.i.i150
                                        #   in Loop: Header=BB1_3 Depth=1
	testl	%edi, %edi
	jle	.LBB1_52
# BB#51:                                # %.lr.ph.i.i151
                                        #   in Loop: Header=BB1_3 Depth=1
	leaq	(,%rdi,4), %rdx
	movq	%r15, %rdi
	movq	%r14, %rsi
	callq	memcpy
	jmp	.LBB1_53
.LBB1_52:                               # %._crit_edge.i.i152
                                        #   in Loop: Header=BB1_3 Depth=1
	testq	%r12, %r12
	je	.LBB1_54
.LBB1_53:                               # %._crit_edge.thread.i.i157
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%r14, %rdi
	callq	_ZdaPv
	movq	8(%rsp), %rdi           # 8-byte Reload
.LBB1_54:                               # %._crit_edge16.i.i158
                                        #   in Loop: Header=BB1_3 Depth=1
	movl	$0, (%r15,%rdi,4)
	movq	%r15, %r12
.LBB1_55:                               # %.noexc.i89
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%r15, 32(%rsp)          # 8-byte Spill
	leaq	(%r12,%rdi,4), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_56:                               #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r13,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB1_56
# BB#57:                                #   in Loop: Header=BB1_3 Depth=1
.Ltmp23:
	xorl	%r14d, %r14d
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp24:
# BB#58:                                #   in Loop: Header=BB1_3 Depth=1
	movl	$0, 8(%rbx)
	movq	$_ZTV14COutFileStream+16, (%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 16(%rbx)
	movl	$-1, 24(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
.Ltmp25:
	movl	$4, %edi
	callq	_Znam
.Ltmp26:
# BB#59:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%rax, 32(%rbx)
	movb	$0, (%rax)
	movl	$4, 44(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO8COutFileE+16, 16(%rbx)
.Ltmp28:
	xorl	%r14d, %r14d
	movq	%rbx, %rdi
	callq	*_ZTV14COutFileStream+24(%rip)
.Ltmp29:
# BB#60:                                # %_ZN9CMyComPtrI10IOutStreamEaSEPS0_.exit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%rbx, %rdi
	addq	$16, %rdi
	movq	%rbx, %r14
	movq	$0, 1104(%rbx)
.Ltmp30:
	xorl	%edx, %edx
	movq	%r12, %rsi
	callq	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
.Ltmp31:
# BB#61:                                # %_ZN14COutFileStream6CreateEPKwb.exit
                                        #   in Loop: Header=BB1_3 Depth=1
	testb	%al, %al
	je	.LBB1_79
# BB#62:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%rbx, %r14
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	120(%rax), %r15
.Ltmp32:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rsi
.Ltmp33:
# BB#63:                                # %.noexc106
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%r15, 104(%rsp)         # 8-byte Spill
	addl	8(%rsp), %ebp           # 4-byte Folded Reload
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rsi)
	leal	1(%rbp), %r15d
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	%rax, %rcx
	movq	$-1, %rax
	cmovoq	%rax, %rcx
.Ltmp34:
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rsi, %r14
	movq	%rcx, %rdi
	callq	_Znam
.Ltmp35:
# BB#64:                                # %.noexc.i102
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%rax, (%r14)
	movl	$0, (%rax)
	movl	%r15d, 8(%rsp)          # 4-byte Spill
	movl	%r15d, 12(%r14)
	movq	%r12, %rcx
	movq	104(%rsp), %r15         # 8-byte Reload
	movq	%r14, %rdi
	.p2align	4, 0x90
.LBB1_65:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i103
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB1_65
# BB#66:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%rbx, %r14
	movl	%ebp, 8(%rdi)
.Ltmp37:
	movq	%rdi, 96(%rsp)          # 8-byte Spill
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
	movq	96(%rsp), %rdx          # 8-byte Reload
.Ltmp38:
# BB#67:                                #   in Loop: Header=BB1_3 Depth=1
	movq	16(%r15), %rax
	movslq	12(%r15), %rcx
	movq	%rdx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%r15)
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	$0, (%r15)
	cmpl	$4, 8(%rsp)             # 4-byte Folded Reload
	je	.LBB1_70
# BB#68:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%rbx, %r14
.Ltmp39:
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_Znam
	movq	%rax, %r15
.Ltmp40:
# BB#69:                                # %._crit_edge16.i.i114
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movl	$0, (%r15)
.LBB1_70:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i115
                                        #   in Loop: Header=BB1_3 Depth=1
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB1_71:                               #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%r15,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB1_71
# BB#72:                                # %_ZN11CStringBaseIwEaSERKS0_.exit119
                                        #   in Loop: Header=BB1_3 Depth=1
.Ltmp41:
	movq	%r15, 16(%rsp)          # 8-byte Spill
	movq	%rbx, %r14
	movl	$48, %edi
	callq	_Znwm
.Ltmp42:
# BB#73:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%rbx, (%rax)
	movq	%rbx, 8(%rax)
	movq	(%rbx), %rcx
.Ltmp44:
	movq	%rbx, %r14
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	*8(%rcx)
.Ltmp45:
# BB#74:                                # %_ZN9CMyComPtrI10IOutStreamEC2ERKS1_.exit.i
                                        #   in Loop: Header=BB1_3 Depth=1
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%rbx)
.Ltmp47:
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_Znam
.Ltmp48:
	movl	8(%rsp), %ecx           # 4-byte Reload
# BB#75:                                # %.noexc.i133
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	%rax, 16(%rbx)
	movl	$0, (%rax)
	movl	%ecx, 28(%rbx)
	xorl	%ecx, %ecx
	movq	16(%rsp), %rsi          # 8-byte Reload
	.p2align	4, 0x90
.LBB1_76:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i134
                                        #   Parent Loop BB1_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rsi,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB1_76
# BB#77:                                #   in Loop: Header=BB1_3 Depth=1
	movl	%ebp, 24(%rbx)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 32(%rbx)
.Ltmp53:
	movq	88(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp54:
	movq	48(%rsp), %r15          # 8-byte Reload
# BB#78:                                # %_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE3AddERKS1_.exit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	56(%r15), %rax
	movslq	52(%r15), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 52(%r15)
	xorl	%ebp, %ebp
	movq	%r14, %rbx
	testq	%r12, %r12
	jne	.LBB1_80
	jmp	.LBB1_81
	.p2align	4, 0x90
.LBB1_79:                               #   in Loop: Header=BB1_3 Depth=1
	callq	__errno_location
	movl	(%rax), %eax
	movl	%eax, 28(%rsp)          # 4-byte Spill
	movb	$1, %bpl
	movq	48(%rsp), %r15          # 8-byte Reload
	testq	%r12, %r12
	je	.LBB1_81
.LBB1_80:                               #   in Loop: Header=BB1_3 Depth=1
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB1_81:                               # %_ZN11CStringBaseIwED2Ev.exit124
                                        #   in Loop: Header=BB1_3 Depth=1
	testq	%r13, %r13
	movl	28(%rsp), %r12d         # 4-byte Reload
	je	.LBB1_83
# BB#82:                                #   in Loop: Header=BB1_3 Depth=1
	movq	%r13, %rdi
	callq	_ZdaPv
.LBB1_83:                               # %_ZN18COutMultiVolStream14CSubStreamInfoD2Ev.exit
                                        #   in Loop: Header=BB1_3 Depth=1
	movq	16(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	movq	(%rbx), %rax
	movq	%rbx, %rdi
	callq	*16(%rax)
	testb	%bpl, %bpl
	movl	60(%rsp), %r13d         # 4-byte Reload
	je	.LBB1_3
	jmp	.LBB1_98
.LBB1_84:                               # %.us-lcssa435
	movq	56(%r15), %rax
	movq	(%rax,%rbx,8), %r14
	cmpq	32(%r14), %rsi
	je	.LBB1_87
# BB#85:
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	xorl	%edx, %edx
	xorl	%ecx, %ecx
	callq	*48(%rax)
	testl	%eax, %eax
	jne	.LBB1_97
# BB#86:
	movq	16(%r15), %rsi
	movq	%rsi, 32(%r14)
.LBB1_87:                               # %.us-lcssa435._crit_edge
	movl	%r13d, %eax
	movq	%rbp, %rbx
	subq	%rsi, %rbx
	cmpq	%rbx, %rax
	cmovbl	%r13d, %ebx
	movq	8(%r14), %rdi
	movq	(%rdi), %rax
	leaq	112(%rsp), %rcx
	movq	80(%rsp), %rsi          # 8-byte Reload
	movl	%ebx, %edx
	callq	*40(%rax)
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	jne	.LBB1_97
# BB#88:
	movl	112(%rsp), %eax
	movq	32(%r14), %rcx
	addq	%rax, %rcx
	movq	%rcx, 32(%r14)
	movq	16(%r15), %rdx
	addq	%rax, %rdx
	movq	%rdx, 16(%r15)
	movq	24(%r15), %rsi
	addq	%rax, %rsi
	movq	%rsi, 24(%r15)
	cmpq	32(%r15), %rsi
	jbe	.LBB1_90
# BB#89:
	movq	%rsi, 32(%r15)
.LBB1_90:
	cmpq	40(%r14), %rdx
	jbe	.LBB1_92
# BB#91:
	movq	%rdx, 40(%r14)
.LBB1_92:
	movq	72(%rsp), %rdx          # 8-byte Reload
	testq	%rdx, %rdx
	je	.LBB1_94
# BB#93:
	addl	%eax, (%rdx)
.LBB1_94:
	cmpq	%rbp, %rcx
	jne	.LBB1_96
# BB#95:
	incl	12(%r15)
	movq	$0, 16(%r15)
.LBB1_96:
	testl	%eax, %eax
	sete	%al
	testl	%ebx, %ebx
	setne	%cl
	andb	%al, %cl
	movl	$-2147467259, %eax      # imm = 0x80004005
	cmovnel	%eax, %r12d
	cmpb	$1, %cl
	movl	%r12d, %eax
	movl	$0, %r12d
	jne	.LBB1_98
.LBB1_97:                               # %.loopexit256.loopexit587
	movl	%eax, %r12d
.LBB1_98:                               # %.loopexit256
	movl	%r12d, %eax
	addq	$184, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB1_99:
	xorl	%r12d, %r12d
	jmp	.LBB1_98
.LBB1_100:
.Ltmp22:
	movq	%rax, %r15
	testq	%r12, %r12
	je	.LBB1_110
# BB#101:
	movq	%r14, %rdi
	callq	_ZdaPv
	jmp	.LBB1_110
.LBB1_102:
.Ltmp55:
	jmp	.LBB1_114
.LBB1_103:
.Ltmp49:
	movq	%rax, %r15
	movq	%rbx, %rbp
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB1_107
# BB#104:
	movq	(%rdi), %rax
.Ltmp50:
	callq	*16(%rax)
.Ltmp51:
	jmp	.LBB1_107
.LBB1_105:
.Ltmp52:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB1_106:
.Ltmp46:
	movq	%rbx, %rbp
	movq	%rax, %r15
.LBB1_107:                              # %.body138
	movq	%rbp, %rdi
	callq	_ZdlPv
	testq	%r12, %r12
	jne	.LBB1_115
	jmp	.LBB1_123
.LBB1_108:
.Ltmp36:
	movq	%rax, %r15
	movq	%r14, %rdi
	callq	_ZdlPv
	movq	%rbx, %r14
	testq	%r12, %r12
	jne	.LBB1_115
	jmp	.LBB1_123
.LBB1_109:
.Ltmp19:
	movq	%rax, %r15
.LBB1_110:                              # %.body94
	xorl	%r14d, %r14d
	testq	%r13, %r13
	jne	.LBB1_124
	jmp	.LBB1_125
.LBB1_111:
.Ltmp27:
	movq	%rax, %r15
	movq	%rbx, %rdi
	callq	_ZdlPv
	xorl	%r14d, %r14d
	testq	%r12, %r12
	jne	.LBB1_115
	jmp	.LBB1_123
.LBB1_112:                              # %_ZN11CStringBaseIwED2Ev.exit128.thread
.Ltmp4:
	movq	%rax, %r15
	xorl	%r14d, %r14d
	movq	16(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB1_126
.LBB1_113:
.Ltmp43:
.LBB1_114:
	movq	%rax, %r15
	testq	%r12, %r12
	je	.LBB1_123
.LBB1_115:
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	testq	%r13, %r13
	jne	.LBB1_124
	jmp	.LBB1_125
.LBB1_116:
.Ltmp16:
	jmp	.LBB1_118
.LBB1_117:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp13:
.LBB1_118:                              # %_ZN11CStringBaseIwED2Ev.exit83
	movq	%rax, %r15
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB1_121
.LBB1_119:
.Ltmp7:
	movq	%rax, %r15
	jmp	.LBB1_122
.LBB1_120:
.Ltmp10:
	movq	%rax, %r15
.LBB1_121:                              # %_ZN11CStringBaseIwED2Ev.exit83
	movq	%rbx, %rdi
	callq	_ZdaPv
.LBB1_122:                              # %_ZN11CStringBaseIwED2Ev.exit127
	xorl	%r14d, %r14d
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, 40(%rsp)          # 8-byte Spill
.LBB1_123:                              # %_ZN11CStringBaseIwED2Ev.exit127
	testq	%r13, %r13
	je	.LBB1_125
.LBB1_124:
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
.LBB1_125:                              # %_ZN11CStringBaseIwED2Ev.exit128
	movq	16(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB1_127
.LBB1_126:
	callq	_ZdaPv
.LBB1_127:                              # %_ZN11CStringBaseIwED2Ev.exit.i129
	testq	%r14, %r14
	je	.LBB1_129
# BB#128:
	movq	(%r14), %rax
.Ltmp56:
	movq	%r14, %rdi
	callq	*16(%rax)
.Ltmp57:
.LBB1_129:                              # %_ZN18COutMultiVolStream14CSubStreamInfoD2Ev.exit131
	movq	%r15, %rdi
	callq	_Unwind_Resume
.LBB1_130:
.Ltmp58:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end1:
	.size	_ZN18COutMultiVolStream5WriteEPKvjPj, .Lfunc_end1-_ZN18COutMultiVolStream5WriteEPKvjPj
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table1:
.Lexception0:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\232\202\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\221\002"              # Call site table length
	.long	.Lfunc_begin0-.Lfunc_begin0 # >> Call Site 1 <<
	.long	.Ltmp0-.Lfunc_begin0    #   Call between .Lfunc_begin0 and .Ltmp0
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp0-.Lfunc_begin0    # >> Call Site 2 <<
	.long	.Ltmp3-.Ltmp0           #   Call between .Ltmp0 and .Ltmp3
	.long	.Ltmp4-.Lfunc_begin0    #     jumps to .Ltmp4
	.byte	0                       #   On action: cleanup
	.long	.Ltmp5-.Lfunc_begin0    # >> Call Site 3 <<
	.long	.Ltmp6-.Ltmp5           #   Call between .Ltmp5 and .Ltmp6
	.long	.Ltmp7-.Lfunc_begin0    #     jumps to .Ltmp7
	.byte	0                       #   On action: cleanup
	.long	.Ltmp8-.Lfunc_begin0    # >> Call Site 4 <<
	.long	.Ltmp9-.Ltmp8           #   Call between .Ltmp8 and .Ltmp9
	.long	.Ltmp10-.Lfunc_begin0   #     jumps to .Ltmp10
	.byte	0                       #   On action: cleanup
	.long	.Ltmp11-.Lfunc_begin0   # >> Call Site 5 <<
	.long	.Ltmp12-.Ltmp11         #   Call between .Ltmp11 and .Ltmp12
	.long	.Ltmp13-.Lfunc_begin0   #     jumps to .Ltmp13
	.byte	0                       #   On action: cleanup
	.long	.Ltmp14-.Lfunc_begin0   # >> Call Site 6 <<
	.long	.Ltmp15-.Ltmp14         #   Call between .Ltmp14 and .Ltmp15
	.long	.Ltmp16-.Lfunc_begin0   #     jumps to .Ltmp16
	.byte	0                       #   On action: cleanup
	.long	.Ltmp17-.Lfunc_begin0   # >> Call Site 7 <<
	.long	.Ltmp18-.Ltmp17         #   Call between .Ltmp17 and .Ltmp18
	.long	.Ltmp19-.Lfunc_begin0   #     jumps to .Ltmp19
	.byte	0                       #   On action: cleanup
	.long	.Ltmp20-.Lfunc_begin0   # >> Call Site 8 <<
	.long	.Ltmp21-.Ltmp20         #   Call between .Ltmp20 and .Ltmp21
	.long	.Ltmp22-.Lfunc_begin0   #     jumps to .Ltmp22
	.byte	0                       #   On action: cleanup
	.long	.Ltmp21-.Lfunc_begin0   # >> Call Site 9 <<
	.long	.Ltmp23-.Ltmp21         #   Call between .Ltmp21 and .Ltmp23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp23-.Lfunc_begin0   # >> Call Site 10 <<
	.long	.Ltmp24-.Ltmp23         #   Call between .Ltmp23 and .Ltmp24
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp25-.Lfunc_begin0   # >> Call Site 11 <<
	.long	.Ltmp26-.Ltmp25         #   Call between .Ltmp25 and .Ltmp26
	.long	.Ltmp27-.Lfunc_begin0   #     jumps to .Ltmp27
	.byte	0                       #   On action: cleanup
	.long	.Ltmp28-.Lfunc_begin0   # >> Call Site 12 <<
	.long	.Ltmp33-.Ltmp28         #   Call between .Ltmp28 and .Ltmp33
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp34-.Lfunc_begin0   # >> Call Site 13 <<
	.long	.Ltmp35-.Ltmp34         #   Call between .Ltmp34 and .Ltmp35
	.long	.Ltmp36-.Lfunc_begin0   #     jumps to .Ltmp36
	.byte	0                       #   On action: cleanup
	.long	.Ltmp37-.Lfunc_begin0   # >> Call Site 14 <<
	.long	.Ltmp42-.Ltmp37         #   Call between .Ltmp37 and .Ltmp42
	.long	.Ltmp43-.Lfunc_begin0   #     jumps to .Ltmp43
	.byte	0                       #   On action: cleanup
	.long	.Ltmp44-.Lfunc_begin0   # >> Call Site 15 <<
	.long	.Ltmp45-.Ltmp44         #   Call between .Ltmp44 and .Ltmp45
	.long	.Ltmp46-.Lfunc_begin0   #     jumps to .Ltmp46
	.byte	0                       #   On action: cleanup
	.long	.Ltmp47-.Lfunc_begin0   # >> Call Site 16 <<
	.long	.Ltmp48-.Ltmp47         #   Call between .Ltmp47 and .Ltmp48
	.long	.Ltmp49-.Lfunc_begin0   #     jumps to .Ltmp49
	.byte	0                       #   On action: cleanup
	.long	.Ltmp53-.Lfunc_begin0   # >> Call Site 17 <<
	.long	.Ltmp54-.Ltmp53         #   Call between .Ltmp53 and .Ltmp54
	.long	.Ltmp55-.Lfunc_begin0   #     jumps to .Ltmp55
	.byte	0                       #   On action: cleanup
	.long	.Ltmp54-.Lfunc_begin0   # >> Call Site 18 <<
	.long	.Ltmp50-.Ltmp54         #   Call between .Ltmp54 and .Ltmp50
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp50-.Lfunc_begin0   # >> Call Site 19 <<
	.long	.Ltmp51-.Ltmp50         #   Call between .Ltmp50 and .Ltmp51
	.long	.Ltmp52-.Lfunc_begin0   #     jumps to .Ltmp52
	.byte	1                       #   On action: 1
	.long	.Ltmp56-.Lfunc_begin0   # >> Call Site 20 <<
	.long	.Ltmp57-.Ltmp56         #   Call between .Ltmp56 and .Ltmp57
	.long	.Ltmp58-.Lfunc_begin0   #     jumps to .Ltmp58
	.byte	1                       #   On action: 1
	.long	.Ltmp57-.Lfunc_begin0   # >> Call Site 21 <<
	.long	.Lfunc_end1-.Ltmp57     #   Call between .Ltmp57 and .Lfunc_end1
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZplIwE11CStringBaseIT_ERKS2_S4_,"axG",@progbits,_ZplIwE11CStringBaseIT_ERKS2_S4_,comdat
	.weak	_ZplIwE11CStringBaseIT_ERKS2_S4_
	.p2align	4, 0x90
	.type	_ZplIwE11CStringBaseIT_ERKS2_S4_,@function
_ZplIwE11CStringBaseIT_ERKS2_S4_:       # @_ZplIwE11CStringBaseIT_ERKS2_S4_
.Lfunc_begin1:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception1
# BB#0:
	pushq	%r15
.Lcfi19:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi20:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi21:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi22:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi23:
	.cfi_def_cfa_offset 48
.Lcfi24:
	.cfi_offset %rbx, -48
.Lcfi25:
	.cfi_offset %r12, -40
.Lcfi26:
	.cfi_offset %r13, -32
.Lcfi27:
	.cfi_offset %r14, -24
.Lcfi28:
	.cfi_offset %r15, -16
	movq	%rdx, %r14
	movq	%rsi, %r15
	movq	%rdi, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%rbx)
	movslq	8(%r15), %r13
	leaq	1(%r13), %r12
	testl	%r12d, %r12d
	je	.LBB2_1
# BB#2:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%rbx)
	movl	$0, (%rax)
	movl	%r12d, 12(%rbx)
	jmp	.LBB2_3
.LBB2_1:
	xorl	%eax, %eax
.LBB2_3:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB2_4:                                # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB2_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit
	movl	%r13d, 8(%rbx)
	movl	8(%r14), %esi
.Ltmp59:
	movq	%rbx, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp60:
# BB#6:                                 # %.noexc
	movslq	8(%rbx), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%rbx), %rcx
	movq	(%r14), %rsi
	.p2align	4, 0x90
.LBB2_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rsi), %edx
	addq	$4, %rsi
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB2_7
# BB#8:
	movl	8(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%rbx)
	movq	%rbx, %rax
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.LBB2_9:
.Ltmp61:
	movq	%rax, %r14
	movq	(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB2_11
# BB#10:
	callq	_ZdaPv
.LBB2_11:                               # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end2:
	.size	_ZplIwE11CStringBaseIT_ERKS2_S4_, .Lfunc_end2-_ZplIwE11CStringBaseIT_ERKS2_S4_
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table2:
.Lexception1:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin1-.Lfunc_begin1 # >> Call Site 1 <<
	.long	.Ltmp59-.Lfunc_begin1   #   Call between .Lfunc_begin1 and .Ltmp59
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp59-.Lfunc_begin1   # >> Call Site 2 <<
	.long	.Ltmp60-.Ltmp59         #   Call between .Ltmp59 and .Ltmp60
	.long	.Ltmp61-.Lfunc_begin1   #     jumps to .Ltmp61
	.byte	0                       #   On action: cleanup
	.long	.Ltmp60-.Lfunc_begin1   # >> Call Site 3 <<
	.long	.Lfunc_end2-.Ltmp60     #   Call between .Ltmp60 and .Lfunc_end2
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text.__clang_call_terminate,"axG",@progbits,__clang_call_terminate,comdat
	.hidden	__clang_call_terminate
	.weak	__clang_call_terminate
	.p2align	4, 0x90
	.type	__clang_call_terminate,@function
__clang_call_terminate:                 # @__clang_call_terminate
# BB#0:
	pushq	%rax
	callq	__cxa_begin_catch
	callq	_ZSt9terminatev
.Lfunc_end3:
	.size	__clang_call_terminate, .Lfunc_end3-__clang_call_terminate

	.text
	.globl	_ZN18COutMultiVolStream4SeekExjPy
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStream4SeekExjPy,@function
_ZN18COutMultiVolStream4SeekExjPy:      # @_ZN18COutMultiVolStream4SeekExjPy
	.cfi_startproc
# BB#0:
	movl	$-2147287039, %eax      # imm = 0x80030001
	cmpl	$2, %edx
	ja	.LBB4_11
# BB#1:
	testl	%edx, %edx
	je	.LBB4_7
# BB#2:
	cmpl	$1, %edx
	je	.LBB4_5
# BB#3:
	cmpl	$2, %edx
	jne	.LBB4_4
# BB#6:
	addq	32(%rdi), %rsi
	jmp	.LBB4_7
.LBB4_5:
	addq	24(%rdi), %rsi
.LBB4_7:
	movq	%rsi, 24(%rdi)
	jmp	.LBB4_8
.LBB4_4:                                # %._crit_edge
	movq	24(%rdi), %rsi
.LBB4_8:
	movq	%rsi, 16(%rdi)
	testq	%rcx, %rcx
	je	.LBB4_10
# BB#9:
	movq	%rsi, (%rcx)
.LBB4_10:
	movl	$0, 12(%rdi)
	xorl	%eax, %eax
.LBB4_11:
	retq
.Lfunc_end4:
	.size	_ZN18COutMultiVolStream4SeekExjPy, .Lfunc_end4-_ZN18COutMultiVolStream4SeekExjPy
	.cfi_endproc

	.globl	_ZN18COutMultiVolStream7SetSizeEy
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStream7SetSizeEy,@function
_ZN18COutMultiVolStream7SetSizeEy:      # @_ZN18COutMultiVolStream7SetSizeEy
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi29:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi30:
	.cfi_def_cfa_offset 24
	pushq	%r13
.Lcfi31:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi32:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi33:
	.cfi_def_cfa_offset 48
.Lcfi34:
	.cfi_offset %rbx, -48
.Lcfi35:
	.cfi_offset %r12, -40
.Lcfi36:
	.cfi_offset %r13, -32
.Lcfi37:
	.cfi_offset %r14, -24
.Lcfi38:
	.cfi_offset %r15, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	movslq	52(%r14), %rax
	testq	%rax, %rax
	jle	.LBB5_1
# BB#2:                                 # %.lr.ph47
	movq	56(%r14), %rcx
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB5_3:                                # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%r13,8), %rbx
	movq	%r12, %rdx
	subq	40(%rbx), %rdx
	jb	.LBB5_4
# BB#6:                                 # %.thread
                                        #   in Loop: Header=BB5_3 Depth=1
	incq	%r13
	cmpq	%rax, %r13
	movq	%rdx, %r12
	jl	.LBB5_3
# BB#7:                                 # %.preheader.loopexit
	movq	%rdx, %r12
	cmpl	%eax, %r13d
	jl	.LBB5_9
	jmp	.LBB5_13
.LBB5_1:
	xorl	%r13d, %r13d
	cmpl	%eax, %r13d
	jl	.LBB5_9
	jmp	.LBB5_13
.LBB5_4:
	movq	8(%rbx), %rdi
	movq	(%rdi), %rax
	movq	%r12, %rsi
	callq	*56(%rax)
	testl	%eax, %eax
	jne	.LBB5_14
# BB#5:                                 # %.thread34
	incl	%r13d
	movq	%r12, 40(%rbx)
	movl	52(%r14), %eax
	cmpl	%eax, %r13d
	jge	.LBB5_13
.LBB5_9:
	leaq	40(%r14), %r15
	.p2align	4, 0x90
.LBB5_10:                               # =>This Inner Loop Header: Depth=1
	movq	56(%r14), %rcx
	cltq
	movq	-8(%rcx,%rax,8), %rbx
	movq	8(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB5_12
# BB#11:                                #   in Loop: Header=BB5_10 Depth=1
	movq	(%rdi), %rax
	callq	*16(%rax)
	movq	$0, 8(%rbx)
.LBB5_12:                               # %_ZN9CMyComPtrI10IOutStreamE7ReleaseEv.exit
                                        #   in Loop: Header=BB5_10 Depth=1
	movq	16(%rbx), %rdi
	callq	_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector10DeleteBackEv
	movl	52(%r14), %eax
	cmpl	%eax, %r13d
	jl	.LBB5_10
.LBB5_13:                               # %._crit_edge
	movq	24(%r14), %rax
	movq	%rax, 16(%r14)
	movl	$0, 12(%r14)
	movq	%r12, 32(%r14)
	xorl	%eax, %eax
.LBB5_14:
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	retq
.Lfunc_end5:
	.size	_ZN18COutMultiVolStream7SetSizeEy, .Lfunc_end5-_ZN18COutMultiVolStream7SetSizeEy
	.cfi_endproc

	.globl	_ZN14CUpdateOptions4InitEPK7CCodecsRK13CRecordVectorIiERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN14CUpdateOptions4InitEPK7CCodecsRK13CRecordVectorIiERK11CStringBaseIwE,@function
_ZN14CUpdateOptions4InitEPK7CCodecsRK13CRecordVectorIiERK11CStringBaseIwE: # @_ZN14CUpdateOptions4InitEPK7CCodecsRK13CRecordVectorIiERK11CStringBaseIwE
.Lfunc_begin2:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception2
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi45:
	.cfi_def_cfa_offset 112
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%rcx, %r14
	movq	%rsi, %rbx
	movq	%rdi, %rbp
	movl	12(%rdx), %eax
	cmpl	$1, %eax
	jle	.LBB6_2
# BB#1:
	xorl	%eax, %eax
	jmp	.LBB6_73
.LBB6_2:
	testl	%eax, %eax
	je	.LBB6_7
# BB#3:
	movq	16(%rdx), %rax
	movl	(%rax), %ecx
	testl	%ecx, %ecx
	js	.LBB6_7
# BB#4:
	movl	%ecx, (%rbp)
	jmp	.LBB6_5
.LBB6_7:                                # %.thread
	movq	%rbx, %rdi
	movq	%r14, %rsi
	callq	_ZNK7CCodecs24FindFormatForArchiveNameERK11CStringBaseIwE
	movl	%eax, %ecx
	movl	%ecx, (%rbp)
	testl	%ecx, %ecx
	jns	.LBB6_5
# BB#8:                                 # %_Z11MyStringLenIwEiPKT_.exit.i
	movl	$12, %edi
	callq	_Znam
	movq	%rax, (%rsp)
	movl	$3, 12(%rsp)
	movl	$55, (%rax)
	movl	$122, 4(%rax)
	movl	$0, 8(%rax)
	movl	$2, 8(%rsp)
.Ltmp62:
	movq	%rsp, %rsi
	movq	%rbx, %rdi
	callq	_ZNK7CCodecs24FindFormatForArchiveTypeERK11CStringBaseIwE
	movl	%eax, %ecx
.Ltmp63:
# BB#9:
	movl	%ecx, (%rbp)
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_11
# BB#10:
	callq	_ZdaPv
	movl	(%rbp), %ecx
.LBB6_11:
	testl	%ecx, %ecx
	js	.LBB6_12
.LBB6_5:                                # %.thread121
	movq	32(%rbx), %rax
	movslq	%ecx, %rcx
	movq	(%rax,%rcx,8), %rax
	cmpb	$0, (%rax)
	je	.LBB6_6
# BB#14:
	movq	%r14, 48(%rsp)          # 8-byte Spill
	cmpl	$0, 52(%rax)
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	je	.LBB6_15
# BB#16:
	movq	56(%rax), %rax
	movq	(%rax), %r14
	xorps	%xmm0, %xmm0
	movaps	%xmm0, (%rsp)
	movslq	8(%r14), %r15
	leaq	1(%r15), %r12
	testl	%r12d, %r12d
	je	.LBB6_17
# BB#18:                                # %._crit_edge16.i.i.i
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	%r12d, 12(%rsp)
	jmp	.LBB6_19
.LBB6_6:
	xorl	%eax, %eax
	jmp	.LBB6_73
.LBB6_15:
	movq	$0, 8(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
	movq	%rbx, (%rsp)
	movl	$0, (%rbx)
	movl	$4, 12(%rsp)
	xorl	%r15d, %r15d
	jmp	.LBB6_22
.LBB6_17:
	xorl	%ebx, %ebx
.LBB6_19:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	(%r14), %rax
	movq	%rbx, %rcx
	.p2align	4, 0x90
.LBB6_20:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edx
	addq	$4, %rax
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB6_20
# BB#21:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	%r15d, 8(%rsp)
.LBB6_22:                               # %_ZNK10CArcInfoEx10GetMainExtEv.exit
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 32(%rsp)
	movl	%r15d, %r14d
	incl	%r14d
	je	.LBB6_23
# BB#24:                                # %._crit_edge16.i.i48
	movslq	%r14d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp65:
	callq	_Znam
	movq	%rax, %r12
.Ltmp66:
# BB#25:                                # %.noexc53
	movq	%r12, 32(%rsp)
	movl	$0, (%r12)
	movl	%r14d, 44(%rsp)
	movq	%r12, %r13
	jmp	.LBB6_26
.LBB6_23:
	xorl	%r12d, %r12d
	xorl	%r14d, %r14d
	xorl	%r13d, %r13d
.LBB6_26:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i49
	movq	%r12, %rax
	.p2align	4, 0x90
.LBB6_27:                               # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %ecx
	addq	$4, %rbx
	movl	%ecx, (%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB6_27
# BB#28:
	movl	%r15d, 40(%rsp)
	movq	24(%rsp), %rax          # 8-byte Reload
	cmpb	$0, 200(%rax)
	je	.LBB6_36
# BB#29:                                # %_Z11MyStringLenIwEiPKT_.exit.i54
	movl	$0, 40(%rsp)
	movl	$0, (%r12)
	cmpl	$1, %r14d
	je	.LBB6_35
# BB#30:
.Ltmp68:
	movl	$4, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp69:
# BB#31:                                # %.noexc64
	testl	%r14d, %r14d
	jle	.LBB6_32
# BB#33:                                # %._crit_edge.thread.i.i58
	movq	%r13, %rdi
	callq	_ZdaPv
	movslq	40(%rsp), %rax
	jmp	.LBB6_34
.LBB6_12:
	xorl	%eax, %eax
	jmp	.LBB6_73
.LBB6_32:
	xorl	%eax, %eax
.LBB6_34:                               # %._crit_edge16.i.i59
	movq	%r12, 32(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	$1, 44(%rsp)
.LBB6_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i62.preheader
	movl	$0, (%r12)
	movl	$0, 40(%rsp)
	xorl	%r15d, %r15d
.LBB6_36:
	movq	24(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rax
	subq	$-128, %rax
	leaq	32(%rsp), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_49
# BB#37:
	movl	$0, 136(%rbp)
	movq	128(%rbp), %rbx
	movl	$0, (%rbx)
	incl	%r15d
	movl	140(%rbp), %r14d
	cmpl	%r14d, %r15d
	jne	.LBB6_41
# BB#38:
	movq	24(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB6_47
.LBB6_41:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp70:
	callq	_Znam
	movq	%rax, %r13
.Ltmp71:
# BB#42:                                # %.noexc74
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_43
# BB#44:                                # %.noexc74
	testl	%r14d, %r14d
	movq	24(%rsp), %rbp          # 8-byte Reload
	jle	.LBB6_46
# BB#45:                                # %._crit_edge.thread.i.i68
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	136(%rbp), %rax
	movq	32(%rsp), %r12
	jmp	.LBB6_46
.LBB6_43:
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB6_46:                               # %._crit_edge16.i.i69
	movq	%r13, 128(%rbp)
	movl	$0, (%r13,%rax,4)
	movl	%r15d, 140(%rbp)
	movq	%r13, %rbx
	.p2align	4, 0x90
.LBB6_47:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i70
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12), %eax
	addq	$4, %r12
	movl	%eax, (%rbx)
	addq	$4, %rbx
	testl	%eax, %eax
	jne	.LBB6_47
# BB#48:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i73
	movl	40(%rsp), %eax
	movl	%eax, 136(%rbp)
.LBB6_49:                               # %_ZN11CStringBaseIwEaSERKS0_.exit75
	leaq	144(%rbp), %rax
	movq	%rsp, %rcx
	cmpq	%rax, %rcx
	movq	48(%rsp), %rsi          # 8-byte Reload
	je	.LBB6_60
# BB#50:
	movl	$0, 152(%rbp)
	movq	144(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %r12
	incq	%r12
	movl	156(%rbp), %r14d
	cmpl	%r14d, %r12d
	je	.LBB6_57
# BB#51:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp72:
	callq	_Znam
	movq	%rax, %r13
.Ltmp73:
# BB#52:                                # %.noexc85
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB6_53
# BB#54:                                # %.noexc85
	testl	%r14d, %r14d
	movq	24(%rsp), %rbp          # 8-byte Reload
	jle	.LBB6_56
# BB#55:                                # %._crit_edge.thread.i.i79
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	152(%rbp), %rax
	jmp	.LBB6_56
.LBB6_53:
	movq	24(%rsp), %rbp          # 8-byte Reload
.LBB6_56:                               # %._crit_edge16.i.i80
	movq	%r13, 144(%rbp)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 156(%rbp)
	movq	%r13, %rbx
	movq	48(%rsp), %rsi          # 8-byte Reload
.LBB6_57:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i81
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB6_58:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB6_58
# BB#59:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i84
	movl	8(%rsp), %eax
	movl	%eax, 152(%rbp)
.LBB6_60:                               # %_ZN11CStringBaseIwEaSERKS0_.exit86
	leaq	80(%rbp), %rdi
.Ltmp74:
	callq	_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE
.Ltmp75:
# BB#61:                                # %.preheader
	movq	24(%rsp), %rcx          # 8-byte Reload
	cmpl	$0, 52(%rcx)
	jle	.LBB6_68
# BB#62:                                # %.lr.ph
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB6_63:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_78 Depth 2
                                        #     Child Loop BB6_89 Depth 2
	movq	56(%rcx), %rax
	movq	(%rax,%rbp,8), %r14
	leaq	64(%r14), %rax
	leaq	32(%rsp), %rcx
	cmpq	%rax, %rcx
	je	.LBB6_80
# BB#64:                                #   in Loop: Header=BB6_63 Depth=1
	movl	$0, 72(%r14)
	movq	64(%r14), %rbx
	movl	$0, (%rbx)
	movslq	40(%rsp), %r12
	incq	%r12
	movl	76(%r14), %r15d
	cmpl	%r15d, %r12d
	je	.LBB6_77
# BB#65:                                #   in Loop: Header=BB6_63 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp77:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp78:
# BB#66:                                # %.noexc98
                                        #   in Loop: Header=BB6_63 Depth=1
	testq	%rbx, %rbx
	je	.LBB6_67
# BB#74:                                # %.noexc98
                                        #   in Loop: Header=BB6_63 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB6_76
# BB#75:                                # %._crit_edge.thread.i.i92
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	72(%r14), %rax
	jmp	.LBB6_76
.LBB6_67:                               #   in Loop: Header=BB6_63 Depth=1
	xorl	%eax, %eax
.LBB6_76:                               # %._crit_edge16.i.i93
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	%r13, 64(%r14)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 76(%r14)
	movq	%r13, %rbx
.LBB6_77:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i94
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	32(%rsp), %rax
	.p2align	4, 0x90
.LBB6_78:                               #   Parent Loop BB6_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB6_78
# BB#79:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i97
                                        #   in Loop: Header=BB6_63 Depth=1
	movl	40(%rsp), %eax
	movl	%eax, 72(%r14)
.LBB6_80:                               # %_ZN11CStringBaseIwEaSERKS0_.exit99
                                        #   in Loop: Header=BB6_63 Depth=1
	leaq	80(%r14), %rax
	movq	%rsp, %rcx
	cmpq	%rax, %rcx
	je	.LBB6_91
# BB#81:                                #   in Loop: Header=BB6_63 Depth=1
	movl	$0, 88(%r14)
	movq	80(%r14), %rbx
	movl	$0, (%rbx)
	movslq	8(%rsp), %r12
	incq	%r12
	movl	92(%r14), %r15d
	cmpl	%r15d, %r12d
	je	.LBB6_88
# BB#82:                                #   in Loop: Header=BB6_63 Depth=1
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp79:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
.Ltmp80:
# BB#83:                                # %.noexc
                                        #   in Loop: Header=BB6_63 Depth=1
	testq	%rbx, %rbx
	je	.LBB6_84
# BB#85:                                # %.noexc
                                        #   in Loop: Header=BB6_63 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB6_87
# BB#86:                                # %._crit_edge.thread.i.i
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	88(%r14), %rax
	jmp	.LBB6_87
.LBB6_84:                               #   in Loop: Header=BB6_63 Depth=1
	xorl	%eax, %eax
.LBB6_87:                               # %._crit_edge16.i.i
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	%r13, 80(%r14)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 92(%r14)
	movq	%r13, %rbx
.LBB6_88:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i45
                                        #   in Loop: Header=BB6_63 Depth=1
	movq	(%rsp), %rax
	.p2align	4, 0x90
.LBB6_89:                               #   Parent Loop BB6_63 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB6_89
# BB#90:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
                                        #   in Loop: Header=BB6_63 Depth=1
	movl	8(%rsp), %eax
	movl	%eax, 88(%r14)
.LBB6_91:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
                                        #   in Loop: Header=BB6_63 Depth=1
	leaq	16(%r14), %rdi
.Ltmp81:
	movq	%r14, %rsi
	callq	_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE
.Ltmp82:
# BB#92:                                #   in Loop: Header=BB6_63 Depth=1
	incq	%rbp
	movq	24(%rsp), %rcx          # 8-byte Reload
	movslq	52(%rcx), %rax
	cmpq	%rax, %rbp
	jl	.LBB6_63
.LBB6_68:                               # %._crit_edge
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_70
# BB#69:
	callq	_ZdaPv
.LBB6_70:                               # %_ZN11CStringBaseIwED2Ev.exit87
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_72
# BB#71:
	callq	_ZdaPv
.LBB6_72:                               # %_ZN11CStringBaseIwED2Ev.exit88
	movb	$1, %al
.LBB6_73:
                                        # kill: %AL<def> %AL<kill> %EAX<kill>
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB6_13:
.Ltmp64:
	movq	%rax, %r14
	movq	(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB6_99
	jmp	.LBB6_100
.LBB6_39:
.Ltmp67:
	movq	%rax, %r14
	testq	%rbx, %rbx
	jne	.LBB6_98
	jmp	.LBB6_100
.LBB6_40:
.Ltmp76:
	jmp	.LBB6_94
.LBB6_93:
.Ltmp83:
.LBB6_94:
	movq	%rax, %r14
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB6_96
# BB#95:
	callq	_ZdaPv
.LBB6_96:                               # %_ZN11CStringBaseIwED2Ev.exit42
	movq	(%rsp), %rbx
	testq	%rbx, %rbx
	je	.LBB6_100
.LBB6_98:
	movq	%rbx, %rdi
.LBB6_99:
	callq	_ZdaPv
.LBB6_100:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end6:
	.size	_ZN14CUpdateOptions4InitEPK7CCodecsRK13CRecordVectorIiERK11CStringBaseIwE, .Lfunc_end6-_ZN14CUpdateOptions4InitEPK7CCodecsRK13CRecordVectorIiERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table6:
.Lexception2:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin2-.Lfunc_begin2 # >> Call Site 1 <<
	.long	.Ltmp62-.Lfunc_begin2   #   Call between .Lfunc_begin2 and .Ltmp62
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp62-.Lfunc_begin2   # >> Call Site 2 <<
	.long	.Ltmp63-.Ltmp62         #   Call between .Ltmp62 and .Ltmp63
	.long	.Ltmp64-.Lfunc_begin2   #     jumps to .Ltmp64
	.byte	0                       #   On action: cleanup
	.long	.Ltmp63-.Lfunc_begin2   # >> Call Site 3 <<
	.long	.Ltmp65-.Ltmp63         #   Call between .Ltmp63 and .Ltmp65
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp65-.Lfunc_begin2   # >> Call Site 4 <<
	.long	.Ltmp66-.Ltmp65         #   Call between .Ltmp65 and .Ltmp66
	.long	.Ltmp67-.Lfunc_begin2   #     jumps to .Ltmp67
	.byte	0                       #   On action: cleanup
	.long	.Ltmp68-.Lfunc_begin2   # >> Call Site 5 <<
	.long	.Ltmp75-.Ltmp68         #   Call between .Ltmp68 and .Ltmp75
	.long	.Ltmp76-.Lfunc_begin2   #     jumps to .Ltmp76
	.byte	0                       #   On action: cleanup
	.long	.Ltmp77-.Lfunc_begin2   # >> Call Site 6 <<
	.long	.Ltmp82-.Ltmp77         #   Call between .Ltmp77 and .Ltmp82
	.long	.Ltmp83-.Lfunc_begin2   #     jumps to .Ltmp83
	.byte	0                       #   On action: cleanup
	.long	.Ltmp82-.Lfunc_begin2   # >> Call Site 7 <<
	.long	.Lfunc_end6-.Ltmp82     #   Call between .Ltmp82 and .Lfunc_end6
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE,"axG",@progbits,_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE,comdat
	.weak	_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE
	.p2align	4, 0x90
	.type	_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE,@function
_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE: # @_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE
.Lfunc_begin3:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception3
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$24, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 80
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%rsi, %r15
	movq	%rdi, %r13
	cmpq	%r15, %r13
	je	.LBB7_9
# BB#1:
	movl	$0, 8(%r13)
	movq	(%r13), %rbx
	movl	$0, (%rbx)
	movslq	8(%r15), %rbp
	incq	%rbp
	movl	12(%r13), %r12d
	cmpl	%r12d, %ebp
	je	.LBB7_6
# BB#2:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB7_5
# BB#3:
	testl	%r12d, %r12d
	jle	.LBB7_5
# BB#4:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r13), %rax
.LBB7_5:                                # %._crit_edge16.i.i
	movq	%r14, (%r13)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 12(%r13)
	movq	%r14, %rbx
.LBB7_6:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rax
	.p2align	4, 0x90
.LBB7_7:                                # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB7_7
# BB#8:                                 # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movl	8(%r15), %eax
	movl	%eax, 8(%r13)
.LBB7_9:                                # %_ZN11CStringBaseIwEaSERKS0_.exit
	leaq	16(%r13), %rsi
	leaq	32(%r13), %r14
	movq	%r15, %rdi
	movq	%r14, %rdx
	callq	_Z16SplitPathToPartsRK11CStringBaseIwERS0_S3_
	movslq	40(%r13), %rcx
	testq	%rcx, %rcx
	je	.LBB7_13
# BB#10:
	movq	(%r14), %rax
	leaq	(,%rcx,4), %rdx
	.p2align	4, 0x90
.LBB7_11:                               # =>This Inner Loop Header: Depth=1
	cmpl	$46, -4(%rax,%rdx)
	je	.LBB7_14
# BB#12:                                #   in Loop: Header=BB7_11 Depth=1
	addq	$-4, %rdx
	jne	.LBB7_11
	jmp	.LBB7_13
.LBB7_14:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit
	leaq	-4(%rax,%rdx), %r15
	subq	%rax, %r15
	shrq	$2, %r15
	testl	%r15d, %r15d
	js	.LBB7_13
# BB#15:
	leal	-1(%rcx), %eax
	cmpl	%eax, %r15d
	jne	.LBB7_31
# BB#16:
	leaq	8(%rsp), %rbx
	xorl	%edx, %edx
	movq	%rbx, %rdi
	movq	%r14, %rsi
	movl	%r15d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r14, %rbx
	je	.LBB7_17
# BB#18:
	movl	$0, 40(%r13)
	movq	32(%r13), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %r14
	incq	%r14
	movl	44(%r13), %ebp
	cmpl	%ebp, %r14d
	je	.LBB7_24
# BB#19:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp93:
	callq	_Znam
	movq	%rax, %r15
.Ltmp94:
# BB#20:                                # %.noexc
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB7_23
# BB#21:                                # %.noexc
	testl	%ebp, %ebp
	jle	.LBB7_23
# BB#22:                                # %._crit_edge.thread.i.i16
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%r13), %rax
.LBB7_23:                               # %._crit_edge16.i.i17
	movq	%r15, 32(%r13)
	movl	$0, (%r15,%rax,4)
	movl	%r14d, 44(%r13)
	movq	%r15, %rbx
.LBB7_24:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i18
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_25:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_25
# BB#26:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i21
	movl	16(%rsp), %eax
	movl	%eax, 40(%r13)
	testq	%rdi, %rdi
	jne	.LBB7_28
	jmp	.LBB7_29
.LBB7_31:
	leal	1(%r15), %ebx
	subl	%ebx, %ecx
	leaq	8(%rsp), %r12
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
                                        # kill: %ECX<def> %ECX<kill> %RCX<kill>
	callq	_ZNK11CStringBaseIwE3MidEii
	movq	48(%r13), %rdi
	movq	8(%rsp), %rsi
.Ltmp84:
	callq	_Z21MyStringCompareNoCasePKwS0_
	movl	%eax, %ebp
.Ltmp85:
# BB#32:                                # %_ZNK11CStringBaseIwE13CompareNoCaseERKS0_.exit
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_34
# BB#33:
	callq	_ZdaPv
.LBB7_34:                               # %_ZN11CStringBaseIwED2Ev.exit26
	testl	%ebp, %ebp
	jne	.LBB7_29
# BB#35:
	leaq	48(%r13), %rbp
	movl	40(%r13), %ecx
	subl	%ebx, %ecx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%ebx, %edx
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%rbp, %r12
	je	.LBB7_36
# BB#37:
	movl	$0, 56(%r13)
	movq	48(%r13), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %rbp
	incq	%rbp
	movl	60(%r13), %r12d
	cmpl	%r12d, %ebp
	jne	.LBB7_39
# BB#38:
	leaq	8(%rsp), %r12
	jmp	.LBB7_45
.LBB7_17:                               # %._ZN11CStringBaseIwEaSERKS0_.exit22_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_29
.LBB7_28:
	callq	_ZdaPv
.LBB7_29:
	movl	$0, 56(%r13)
	movq	48(%r13), %rax
	movl	$0, (%rax)
.LBB7_13:                               # %_ZNK11CStringBaseIwE11ReverseFindEw.exit.thread
	addq	$24, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB7_36:                               # %._ZN11CStringBaseIwEaSERKS0_.exit37_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB7_49
	jmp	.LBB7_50
.LBB7_39:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp87:
	callq	_Znam
.Ltmp88:
# BB#40:                                # %.noexc36
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB7_41
# BB#42:                                # %.noexc36
	testl	%r12d, %r12d
	leaq	8(%rsp), %r12
	jle	.LBB7_44
# BB#43:                                # %._crit_edge.thread.i.i30
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	56(%r13), %rcx
	jmp	.LBB7_44
.LBB7_41:
	leaq	8(%rsp), %r12
.LBB7_44:                               # %._crit_edge16.i.i31
	movq	%rax, 48(%r13)
	movl	$0, (%rax,%rcx,4)
	movl	%ebp, 60(%r13)
	movq	%rax, %rbx
.LBB7_45:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i32
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_46:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_46
# BB#47:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i35
	movl	16(%rsp), %eax
	movl	%eax, 56(%r13)
	testq	%rdi, %rdi
	je	.LBB7_50
.LBB7_49:
	callq	_ZdaPv
.LBB7_50:                               # %_ZN11CStringBaseIwED2Ev.exit38
	xorl	%edx, %edx
	movq	%r12, %rdi
	movq	%r14, %rsi
	movl	%r15d, %ecx
	callq	_ZNK11CStringBaseIwE3MidEii
	cmpq	%r14, %r12
	je	.LBB7_51
# BB#52:
	movl	$0, 40(%r13)
	movq	32(%r13), %rbx
	movl	$0, (%rbx)
	movslq	16(%rsp), %rbp
	incq	%rbp
	movl	44(%r13), %r15d
	cmpl	%r15d, %ebp
	je	.LBB7_58
# BB#53:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp90:
	callq	_Znam
	movq	%rax, %r14
.Ltmp91:
# BB#54:                                # %.noexc48
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB7_57
# BB#55:                                # %.noexc48
	testl	%r15d, %r15d
	jle	.LBB7_57
# BB#56:                                # %._crit_edge.thread.i.i42
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%r13), %rax
.LBB7_57:                               # %._crit_edge16.i.i43
	movq	%r14, 32(%r13)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 44(%r13)
	movq	%r14, %rbx
.LBB7_58:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i44
	movq	8(%rsp), %rdi
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB7_59:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB7_59
# BB#60:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i47
	movl	16(%rsp), %eax
	movl	%eax, 40(%r13)
	testq	%rdi, %rdi
	jne	.LBB7_62
	jmp	.LBB7_13
.LBB7_51:                               # %_ZN11CStringBaseIwED2Ev.exit38._ZN11CStringBaseIwEaSERKS0_.exit49_crit_edge
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_13
.LBB7_62:
	callq	_ZdaPv
	jmp	.LBB7_13
.LBB7_65:
.Ltmp92:
	jmp	.LBB7_66
.LBB7_64:
.Ltmp89:
	jmp	.LBB7_66
.LBB7_30:
.Ltmp95:
	jmp	.LBB7_66
.LBB7_63:
.Ltmp86:
.LBB7_66:
	movq	%rax, %rbx
	movq	8(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB7_68
# BB#67:
	callq	_ZdaPv
.LBB7_68:
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end7:
	.size	_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE, .Lfunc_end7-_ZN12CArchivePath13ParseFromPathERK11CStringBaseIwE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table7:
.Lexception3:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\367\200"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Lfunc_begin3-.Lfunc_begin3 # >> Call Site 1 <<
	.long	.Ltmp93-.Lfunc_begin3   #   Call between .Lfunc_begin3 and .Ltmp93
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp93-.Lfunc_begin3   # >> Call Site 2 <<
	.long	.Ltmp94-.Ltmp93         #   Call between .Ltmp93 and .Ltmp94
	.long	.Ltmp95-.Lfunc_begin3   #     jumps to .Ltmp95
	.byte	0                       #   On action: cleanup
	.long	.Ltmp94-.Lfunc_begin3   # >> Call Site 3 <<
	.long	.Ltmp84-.Ltmp94         #   Call between .Ltmp94 and .Ltmp84
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp84-.Lfunc_begin3   # >> Call Site 4 <<
	.long	.Ltmp85-.Ltmp84         #   Call between .Ltmp84 and .Ltmp85
	.long	.Ltmp86-.Lfunc_begin3   #     jumps to .Ltmp86
	.byte	0                       #   On action: cleanup
	.long	.Ltmp85-.Lfunc_begin3   # >> Call Site 5 <<
	.long	.Ltmp87-.Ltmp85         #   Call between .Ltmp85 and .Ltmp87
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp87-.Lfunc_begin3   # >> Call Site 6 <<
	.long	.Ltmp88-.Ltmp87         #   Call between .Ltmp87 and .Ltmp88
	.long	.Ltmp89-.Lfunc_begin3   #     jumps to .Ltmp89
	.byte	0                       #   On action: cleanup
	.long	.Ltmp88-.Lfunc_begin3   # >> Call Site 7 <<
	.long	.Ltmp90-.Ltmp88         #   Call between .Ltmp88 and .Ltmp90
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp90-.Lfunc_begin3   # >> Call Site 8 <<
	.long	.Ltmp91-.Ltmp90         #   Call between .Ltmp90 and .Ltmp91
	.long	.Ltmp92-.Lfunc_begin3   #     jumps to .Ltmp92
	.byte	0                       #   On action: cleanup
	.long	.Ltmp91-.Lfunc_begin3   # >> Call Site 9 <<
	.long	.Lfunc_end7-.Ltmp91     #   Call between .Ltmp91 and .Lfunc_end7
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI8_0:
	.zero	16
	.text
	.globl	_Z23EnumerateInArchiveItemsRKN9NWildcard7CCensorERK4CArcR13CObjectVectorI8CArcItemE
	.p2align	4, 0x90
	.type	_Z23EnumerateInArchiveItemsRKN9NWildcard7CCensorERK4CArcR13CObjectVectorI8CArcItemE,@function
_Z23EnumerateInArchiveItemsRKN9NWildcard7CCensorERK4CArcR13CObjectVectorI8CArcItemE: # @_Z23EnumerateInArchiveItemsRKN9NWildcard7CCensorERK4CArcR13CObjectVectorI8CArcItemE
.Lfunc_begin4:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception4
# BB#0:
	pushq	%rbp
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi68:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi69:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi70:
	.cfi_def_cfa_offset 56
	subq	$120, %rsp
.Lcfi71:
	.cfi_def_cfa_offset 176
.Lcfi72:
	.cfi_offset %rbx, -56
.Lcfi73:
	.cfi_offset %r12, -48
.Lcfi74:
	.cfi_offset %r13, -40
.Lcfi75:
	.cfi_offset %r14, -32
.Lcfi76:
	.cfi_offset %r15, -24
.Lcfi77:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbp
	movq	%rsi, %r12
	movq	%rdi, %r14
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
	movq	(%r12), %rbx
	movq	(%rbx), %rax
	leaq	12(%rsp), %rsi
	movq	%rbx, %rdi
	callq	*56(%rax)
	movl	%eax, %r15d
	testl	%r15d, %r15d
	jne	.LBB8_3
# BB#1:
	movl	12(%rsp), %esi
	movq	%rbp, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
	cmpl	$0, 12(%rsp)
	je	.LBB8_2
# BB#4:                                 # %.lr.ph
	movq	%r14, 96(%rsp)          # 8-byte Spill
	movq	%rbp, 64(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB8_5:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_39 Depth 2
	movq	$0, 40(%rsp)
	movl	$16, %edi
	callq	_Znam
	movq	%rax, 32(%rsp)
	movl	$0, (%rax)
	movq	$4, 44(%rsp)
	movl	$-1, 56(%rsp)
.Ltmp96:
	movq	%r12, %rdi
	movl	%ebp, %esi
	leaq	32(%rsp), %rdx
	callq	_ZNK4CArc11GetItemPathEjR11CStringBaseIwE
.Ltmp97:
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r15d
	movl	$1, %r14d
	je	.LBB8_9
# BB#7:                                 #   in Loop: Header=BB8_5 Depth=1
	movl	%eax, %r15d
	jmp	.LBB8_42
	.p2align	4, 0x90
.LBB8_9:                                #   in Loop: Header=BB8_5 Depth=1
.Ltmp98:
	movq	%rbx, %rdi
	movl	%ebp, %esi
	leaq	48(%rsp), %rdx
	callq	_Z19IsArchiveItemFolderP10IInArchivejRb
.Ltmp99:
# BB#10:                                #   in Loop: Header=BB8_5 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r15d
	je	.LBB8_12
# BB#11:                                #   in Loop: Header=BB8_5 Depth=1
	movl	%eax, %r15d
	jmp	.LBB8_42
.LBB8_12:                               #   in Loop: Header=BB8_5 Depth=1
	xorl	%edx, %edx
	cmpb	$0, 48(%rsp)
	sete	%dl
.Ltmp100:
	movq	96(%rsp), %rdi          # 8-byte Reload
	leaq	32(%rsp), %rsi
	callq	_ZNK9NWildcard7CCensor9CheckPathERK11CStringBaseIwEb
.Ltmp101:
# BB#13:                                #   in Loop: Header=BB8_5 Depth=1
	movb	%al, 51(%rsp)
.Ltmp102:
	movq	%r12, %rdi
	movl	%ebp, %esi
	leaq	24(%rsp), %rdx
	leaq	50(%rsp), %rcx
	callq	_ZNK4CArc12GetItemMTimeEjR9_FILETIMERb
.Ltmp103:
# BB#14:                                #   in Loop: Header=BB8_5 Depth=1
	testl	%eax, %eax
	movl	%r15d, %r13d
	cmovnel	%eax, %r13d
	movl	%eax, %r15d
	jne	.LBB8_42
# BB#15:                                #   in Loop: Header=BB8_5 Depth=1
	movl	$0, 104(%rsp)
	movq	(%rbx), %rax
.Ltmp104:
	movl	$7, %edx
	movq	%rbx, %r15
	movq	%rbx, %rdi
	movl	%ebp, %esi
	leaq	104(%rsp), %r14
	movq	%r14, %rcx
	callq	*64(%rax)
.Ltmp105:
# BB#16:                                #   in Loop: Header=BB8_5 Depth=1
	testl	%eax, %eax
	cmovnel	%eax, %r13d
	movl	$1, %ebx
	jne	.LBB8_20
# BB#17:                                #   in Loop: Header=BB8_5 Depth=1
	xorl	%ebx, %ebx
	cmpw	$0, 104(%rsp)
	setne	49(%rsp)
	je	.LBB8_20
# BB#18:                                #   in Loop: Header=BB8_5 Depth=1
.Ltmp106:
	movq	%r14, %rdi
	callq	_Z26ConvertPropVariantToUInt64RK14tagPROPVARIANT
.Ltmp107:
# BB#19:                                #   in Loop: Header=BB8_5 Depth=1
	movq	%rax, 16(%rsp)
.LBB8_20:                               #   in Loop: Header=BB8_5 Depth=1
.Ltmp111:
	movq	%r14, %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp112:
# BB#21:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit91
                                        #   in Loop: Header=BB8_5 Depth=1
	movl	$1, %r14d
	testl	%ebx, %ebx
	movq	%r15, %rbx
	je	.LBB8_24
# BB#22:                                #   in Loop: Header=BB8_5 Depth=1
	movl	%r13d, %r15d
	jmp	.LBB8_42
.LBB8_24:                               #   in Loop: Header=BB8_5 Depth=1
	movl	$0, 72(%rsp)
	movq	(%rbx), %rax
.Ltmp114:
	movl	$40, %edx
	movq	%rbx, %rdi
	movl	%ebp, %esi
	leaq	72(%rsp), %rcx
	callq	*64(%rax)
	movl	%eax, %r15d
.Ltmp115:
# BB#25:                                #   in Loop: Header=BB8_5 Depth=1
	movl	$1, %ecx
	testl	%r15d, %r15d
	jne	.LBB8_29
# BB#26:                                #   in Loop: Header=BB8_5 Depth=1
	movzwl	72(%rsp), %eax
	cmpl	$19, %eax
	jne	.LBB8_28
# BB#27:                                #   in Loop: Header=BB8_5 Depth=1
	movl	80(%rsp), %eax
	movl	%eax, 56(%rsp)
	movl	$-2147467259, %r15d     # imm = 0x80004005
	cmpl	$2, %eax
	ja	.LBB8_29
.LBB8_28:                               #   in Loop: Header=BB8_5 Depth=1
	xorl	%ecx, %ecx
	movl	%r13d, %r15d
.LBB8_29:                               #   in Loop: Header=BB8_5 Depth=1
.Ltmp120:
	movl	%ecx, %r13d
	leaq	72(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp121:
# BB#30:                                # %_ZN8NWindows4NCOM12CPropVariantD2Ev.exit94
                                        #   in Loop: Header=BB8_5 Depth=1
	movl	$1, %r14d
	testl	%r13d, %r13d
	jne	.LBB8_42
# BB#31:                                #   in Loop: Header=BB8_5 Depth=1
	movl	%ebp, 52(%rsp)
.Ltmp123:
	movl	$48, %edi
	callq	_Znwm
	movq	%rax, %r13
.Ltmp124:
# BB#32:                                # %.noexc
                                        #   in Loop: Header=BB8_5 Depth=1
	movups	16(%rsp), %xmm0
	movups	%xmm0, (%r13)
	xorps	%xmm0, %xmm0
	movups	%xmm0, 16(%r13)
	movslq	40(%rsp), %rsi
	leaq	1(%rsi), %rax
	testl	%eax, %eax
	je	.LBB8_33
# BB#36:                                # %._crit_edge16.i.i.i.i
                                        #   in Loop: Header=BB8_5 Depth=1
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	movq	%rax, %r14
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp125:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp126:
# BB#37:                                # %.noexc.i
                                        #   in Loop: Header=BB8_5 Depth=1
	movq	%rax, 16(%r13)
	movl	$0, (%rax)
	movl	%r14d, 28(%r13)
	movq	88(%rsp), %rsi          # 8-byte Reload
	jmp	.LBB8_38
.LBB8_33:                               #   in Loop: Header=BB8_5 Depth=1
	xorl	%eax, %eax
.LBB8_38:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
                                        #   in Loop: Header=BB8_5 Depth=1
	movq	32(%rsp), %rcx
	.p2align	4, 0x90
.LBB8_39:                               #   Parent Loop BB8_5 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB8_39
# BB#40:                                #   in Loop: Header=BB8_5 Depth=1
	movl	%esi, 24(%r13)
	leaq	48(%rsp), %rax
	movq	%rax, %rcx
	movl	8(%rcx), %eax
	movl	%eax, 40(%r13)
	movq	(%rcx), %rax
	movq	%rax, 32(%r13)
.Ltmp128:
	movq	64(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp129:
# BB#41:                                # %_ZN13CObjectVectorI8CArcItemE3AddERKS0_.exit
                                        #   in Loop: Header=BB8_5 Depth=1
	movq	64(%rsp), %rdx          # 8-byte Reload
	movq	16(%rdx), %rax
	movslq	12(%rdx), %rcx
	movq	%r13, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 12(%rdx)
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB8_42:                               #   in Loop: Header=BB8_5 Depth=1
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_44
# BB#43:                                #   in Loop: Header=BB8_5 Depth=1
	callq	_ZdaPv
.LBB8_44:                               # %_ZN8CArcItemD2Ev.exit92
                                        #   in Loop: Header=BB8_5 Depth=1
	testl	%r14d, %r14d
	jne	.LBB8_3
# BB#45:                                #   in Loop: Header=BB8_5 Depth=1
	incl	%ebp
	cmpl	12(%rsp), %ebp
	jb	.LBB8_5
.LBB8_2:
	xorl	%r15d, %r15d
.LBB8_3:                                # %.loopexit
	movl	%r15d, %eax
	addq	$120, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB8_52:
.Ltmp127:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdlPv
	jmp	.LBB8_48
.LBB8_46:
.Ltmp122:
	jmp	.LBB8_47
.LBB8_35:
.Ltmp116:
	movq	%rax, %rbx
.Ltmp117:
	leaq	72(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp118:
	jmp	.LBB8_48
.LBB8_34:
.Ltmp113:
	jmp	.LBB8_47
.LBB8_23:
.Ltmp108:
	movq	%rax, %rbx
.Ltmp109:
	leaq	104(%rsp), %rdi
	callq	_ZN8NWindows4NCOM12CPropVariant5ClearEv
.Ltmp110:
	jmp	.LBB8_48
.LBB8_51:
.Ltmp119:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB8_8:
.Ltmp130:
.LBB8_47:
	movq	%rax, %rbx
.LBB8_48:
	movq	32(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB8_50
# BB#49:
	callq	_ZdaPv
.LBB8_50:                               # %_ZN8CArcItemD2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end8:
	.size	_Z23EnumerateInArchiveItemsRKN9NWildcard7CCensorERK4CArcR13CObjectVectorI8CArcItemE, .Lfunc_end8-_Z23EnumerateInArchiveItemsRKN9NWildcard7CCensorERK4CArcR13CObjectVectorI8CArcItemE
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table8:
.Lexception4:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\230\001"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\217\001"              # Call site table length
	.long	.Lfunc_begin4-.Lfunc_begin4 # >> Call Site 1 <<
	.long	.Ltmp96-.Lfunc_begin4   #   Call between .Lfunc_begin4 and .Ltmp96
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp96-.Lfunc_begin4   # >> Call Site 2 <<
	.long	.Ltmp103-.Ltmp96        #   Call between .Ltmp96 and .Ltmp103
	.long	.Ltmp130-.Lfunc_begin4  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp104-.Lfunc_begin4  # >> Call Site 3 <<
	.long	.Ltmp107-.Ltmp104       #   Call between .Ltmp104 and .Ltmp107
	.long	.Ltmp108-.Lfunc_begin4  #     jumps to .Ltmp108
	.byte	0                       #   On action: cleanup
	.long	.Ltmp111-.Lfunc_begin4  # >> Call Site 4 <<
	.long	.Ltmp112-.Ltmp111       #   Call between .Ltmp111 and .Ltmp112
	.long	.Ltmp113-.Lfunc_begin4  #     jumps to .Ltmp113
	.byte	0                       #   On action: cleanup
	.long	.Ltmp114-.Lfunc_begin4  # >> Call Site 5 <<
	.long	.Ltmp115-.Ltmp114       #   Call between .Ltmp114 and .Ltmp115
	.long	.Ltmp116-.Lfunc_begin4  #     jumps to .Ltmp116
	.byte	0                       #   On action: cleanup
	.long	.Ltmp120-.Lfunc_begin4  # >> Call Site 6 <<
	.long	.Ltmp121-.Ltmp120       #   Call between .Ltmp120 and .Ltmp121
	.long	.Ltmp122-.Lfunc_begin4  #     jumps to .Ltmp122
	.byte	0                       #   On action: cleanup
	.long	.Ltmp123-.Lfunc_begin4  # >> Call Site 7 <<
	.long	.Ltmp124-.Ltmp123       #   Call between .Ltmp123 and .Ltmp124
	.long	.Ltmp130-.Lfunc_begin4  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp125-.Lfunc_begin4  # >> Call Site 8 <<
	.long	.Ltmp126-.Ltmp125       #   Call between .Ltmp125 and .Ltmp126
	.long	.Ltmp127-.Lfunc_begin4  #     jumps to .Ltmp127
	.byte	0                       #   On action: cleanup
	.long	.Ltmp128-.Lfunc_begin4  # >> Call Site 9 <<
	.long	.Ltmp129-.Ltmp128       #   Call between .Ltmp128 and .Ltmp129
	.long	.Ltmp130-.Lfunc_begin4  #     jumps to .Ltmp130
	.byte	0                       #   On action: cleanup
	.long	.Ltmp117-.Lfunc_begin4  # >> Call Site 10 <<
	.long	.Ltmp110-.Ltmp117       #   Call between .Ltmp117 and .Ltmp110
	.long	.Ltmp119-.Lfunc_begin4  #     jumps to .Ltmp119
	.byte	1                       #   On action: 1
	.long	.Ltmp110-.Lfunc_begin4  # >> Call Site 11 <<
	.long	.Lfunc_end8-.Ltmp110    #   Call between .Ltmp110 and .Lfunc_end8
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI9_0:
	.long	114                     # 0x72
	.long	115                     # 0x73
	.long	102                     # 0x66
	.long	120                     # 0x78
.LCPI9_1:
	.long	55                      # 0x37
	.long	45                      # 0x2d
	.long	90                      # 0x5a
	.long	105                     # 0x69
.LCPI9_2:
	.long	112                     # 0x70
	.long	32                      # 0x20
	.long	99                      # 0x63
	.long	97                      # 0x61
.LCPI9_3:
	.long	110                     # 0x6e
	.long	110                     # 0x6e
	.long	111                     # 0x6f
	.long	116                     # 0x74
.LCPI9_4:
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	110                     # 0x6e
.LCPI9_5:
	.long	100                     # 0x64
	.long	32                      # 0x20
	.long	115                     # 0x73
	.long	112                     # 0x70
.LCPI9_6:
	.long	101                     # 0x65
	.long	99                      # 0x63
	.long	105                     # 0x69
	.long	102                     # 0x66
.LCPI9_7:
	.long	105                     # 0x69
	.long	101                     # 0x65
	.long	100                     # 0x64
	.long	32                      # 0x20
.LCPI9_8:
	.long	83                      # 0x53
	.long	70                      # 0x46
	.long	88                      # 0x58
	.long	32                      # 0x20
.LCPI9_9:
	.long	109                     # 0x6d
	.long	111                     # 0x6f
	.long	100                     # 0x64
	.long	117                     # 0x75
.LCPI9_10:
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	108                     # 0x6c
	.long	101                     # 0x65
.LCPI9_11:
	.long	32                      # 0x20
	.long	105                     # 0x69
	.long	115                     # 0x73
	.long	32                      # 0x20
.LCPI9_12:
	.long	110                     # 0x6e
	.long	111                     # 0x6f
	.long	116                     # 0x74
	.long	32                      # 0x20
.LCPI9_13:
	.long	115                     # 0x73
	.long	112                     # 0x70
	.long	101                     # 0x65
	.long	99                      # 0x63
.LCPI9_14:
	.long	105                     # 0x69
	.long	102                     # 0x66
	.long	105                     # 0x69
	.long	101                     # 0x65
.LCPI9_15:
	.long	85                      # 0x55
	.long	112                     # 0x70
	.long	100                     # 0x64
	.long	97                      # 0x61
.LCPI9_16:
	.long	116                     # 0x74
	.long	105                     # 0x69
	.long	110                     # 0x6e
	.long	103                     # 0x67
.LCPI9_17:
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	111                     # 0x6f
	.long	114                     # 0x72
.LCPI9_18:
	.long	32                      # 0x20
	.long	109                     # 0x6d
	.long	117                     # 0x75
	.long	108                     # 0x6c
.LCPI9_19:
	.long	116                     # 0x74
	.long	105                     # 0x69
	.long	118                     # 0x76
	.long	111                     # 0x6f
.LCPI9_20:
	.long	108                     # 0x6c
	.long	117                     # 0x75
	.long	109                     # 0x6d
	.long	101                     # 0x65
.LCPI9_21:
	.long	32                      # 0x20
	.long	97                      # 0x61
	.long	114                     # 0x72
	.long	99                      # 0x63
.LCPI9_22:
	.long	104                     # 0x68
	.long	105                     # 0x69
	.long	118                     # 0x76
	.long	101                     # 0x65
.LCPI9_23:
	.long	115                     # 0x73
	.long	32                      # 0x20
	.long	105                     # 0x69
	.long	115                     # 0x73
.LCPI9_24:
	.long	32                      # 0x20
	.long	110                     # 0x6e
	.long	111                     # 0x6f
	.long	116                     # 0x74
.LCPI9_25:
	.long	32                      # 0x20
	.long	105                     # 0x69
	.long	109                     # 0x6d
	.long	112                     # 0x70
.LCPI9_26:
	.long	108                     # 0x6c
	.long	101                     # 0x65
	.long	109                     # 0x6d
	.long	101                     # 0x65
.LCPI9_27:
	.long	110                     # 0x6e
	.long	116                     # 0x74
	.long	101                     # 0x65
	.long	100                     # 0x64
.LCPI9_28:
	.long	83                      # 0x53
	.long	99                      # 0x63
	.long	97                      # 0x61
	.long	110                     # 0x6e
.LCPI9_29:
	.long	110                     # 0x6e
	.long	105                     # 0x69
	.long	110                     # 0x6e
	.long	103                     # 0x67
.LCPI9_30:
	.long	32                      # 0x20
	.long	101                     # 0x65
	.long	114                     # 0x72
	.long	114                     # 0x72
.LCPI9_31:
	.long	84                      # 0x54
	.long	104                     # 0x68
	.long	101                     # 0x65
	.long	32                      # 0x20
.LCPI9_32:
	.long	32                      # 0x20
	.long	97                      # 0x61
	.long	108                     # 0x6c
	.long	114                     # 0x72
.LCPI9_33:
	.long	101                     # 0x65
	.long	97                      # 0x61
	.long	100                     # 0x64
	.long	121                     # 0x79
.LCPI9_34:
	.long	32                      # 0x20
	.long	101                     # 0x65
	.long	120                     # 0x78
	.long	105                     # 0x69
.LCPI9_35:
	.long	115                     # 0x73
	.long	116                     # 0x74
	.long	115                     # 0x73
	.long	0                       # 0x0
.LCPI9_36:
	.long	32                      # 0x20
	.long	111                     # 0x6f
	.long	112                     # 0x70
	.long	101                     # 0x65
.LCPI9_37:
	.long	110                     # 0x6e
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	105                     # 0x69
.LCPI9_38:
	.long	110                     # 0x6e
	.long	32                      # 0x20
	.long	83                      # 0x53
	.long	70                      # 0x46
.LCPI9_39:
	.long	88                      # 0x58
	.long	32                      # 0x20
	.long	109                     # 0x6d
	.long	111                     # 0x6f
.LCPI9_40:
	.long	100                     # 0x64
	.long	117                     # 0x75
	.long	108                     # 0x6c
	.long	101                     # 0x65
.LCPI9_41:
	.long	32                      # 0x20
	.long	100                     # 0x64
	.long	101                     # 0x65
	.long	108                     # 0x6c
.LCPI9_42:
	.long	101                     # 0x65
	.long	116                     # 0x74
	.long	101                     # 0x65
	.long	32                      # 0x20
.LCPI9_43:
	.long	116                     # 0x74
	.long	104                     # 0x68
	.long	101                     # 0x65
	.long	32                      # 0x20
.LCPI9_44:
	.long	32                      # 0x20
	.long	109                     # 0x6d
	.long	111                     # 0x6f
	.long	118                     # 0x76
.LCPI9_45:
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	116                     # 0x74
	.long	104                     # 0x68
.LCPI9_46:
	.long	101                     # 0x65
	.long	32                      # 0x20
	.long	102                     # 0x66
	.long	105                     # 0x69
.LCPI9_47:
	.zero	16
	.text
	.globl	_Z13UpdateArchiveP7CCodecsRKN9NWildcard7CCensorER14CUpdateOptionsR16CUpdateErrorInfoP15IOpenCallbackUIP18IUpdateCallbackUI2
	.p2align	4, 0x90
	.type	_Z13UpdateArchiveP7CCodecsRKN9NWildcard7CCensorER14CUpdateOptionsR16CUpdateErrorInfoP15IOpenCallbackUIP18IUpdateCallbackUI2,@function
_Z13UpdateArchiveP7CCodecsRKN9NWildcard7CCensorER14CUpdateOptionsR16CUpdateErrorInfoP15IOpenCallbackUIP18IUpdateCallbackUI2: # @_Z13UpdateArchiveP7CCodecsRKN9NWildcard7CCensorER14CUpdateOptionsR16CUpdateErrorInfoP15IOpenCallbackUIP18IUpdateCallbackUI2
.Lfunc_begin5:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception5
# BB#0:
	pushq	%rbp
.Lcfi78:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi79:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi80:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi81:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi82:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi83:
	.cfi_def_cfa_offset 56
	subq	$856, %rsp              # imm = 0x358
.Lcfi84:
	.cfi_def_cfa_offset 912
.Lcfi85:
	.cfi_offset %rbx, -56
.Lcfi86:
	.cfi_offset %r12, -48
.Lcfi87:
	.cfi_offset %r13, -40
.Lcfi88:
	.cfi_offset %r14, -32
.Lcfi89:
	.cfi_offset %r15, -24
.Lcfi90:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, %rbp
	movq	%rcx, %r14
	movq	%rdx, %r12
	movq	%rdi, %r13
	cmpb	$0, 248(%r12)
	je	.LBB9_2
# BB#1:
	movl	$-2147467259, %ebx      # imm = 0x80004005
	cmpb	$0, 249(%r12)
	jne	.LBB9_570
.LBB9_2:
	cmpl	$0, 300(%r12)
	movq	%r14, 16(%rsp)          # 8-byte Spill
	movq	%r12, 8(%rsp)           # 8-byte Spill
	movq	%r15, 80(%rsp)          # 8-byte Spill
	jle	.LBB9_6
# BB#3:
	movl	$-2147467263, %ebx      # imm = 0x80004001
	cmpb	$0, 249(%r12)
	jne	.LBB9_570
# BB#4:
	cmpb	$0, 200(%r12)
	jne	.LBB9_570
# BB#5:
	leaq	200(%r12), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	jmp	.LBB9_54
.LBB9_6:
	leaq	200(%r12), %rax
	movq	%rax, 360(%rsp)         # 8-byte Spill
	cmpb	$0, 200(%r12)
	je	.LBB9_54
# BB#7:
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	movq	%r13, 216(%rsp)         # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r14
	movl	$0, (%r14)
.Ltmp131:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp132:
# BB#8:
	movl	$0, (%r12)
	movl	$0, (%r14)
.Ltmp134:
	movl	$20, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp135:
# BB#9:
	movq	%r14, %rdi
	callq	_ZdaPv
	movdqa	.LCPI9_0(%rip), %xmm0   # xmm0 = [114,115,102,120]
	movdqu	%xmm0, (%r13)
	movl	$0, 16(%r13)
	movq	%r13, %r14
	movl	$0, (%r12)
.Ltmp136:
	movl	$12, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp137:
# BB#10:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i295.preheader
	movq	%r13, %r14
	movq	%r12, %rdi
	callq	_ZdaPv
	movabsq	$472446402671, %rax     # imm = 0x6E0000006F
	movq	%rax, (%r15)
	movl	$0, 8(%r15)
.Ltmp138:
	movq	%r15, %r12
	movl	$32, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp139:
# BB#11:                                # %._crit_edge16.i.i.i
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rbx)
.Ltmp140:
	movl	$20, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp141:
# BB#12:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movq	%rbp, (%rbx)
	movl	$0, (%rbp)
	movl	$5, 12(%rbx)
	xorl	%eax, %eax
	.p2align	4, 0x90
.LBB9_13:                               # =>This Inner Loop Header: Depth=1
	movl	(%r13,%rax), %ecx
	movl	%ecx, (%rbp,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_13
# BB#14:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	movl	$4, 8(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 16(%rbx)
.Ltmp143:
	movl	$12, %edi
	callq	_Znam
.Ltmp144:
# BB#15:                                # %.noexc.i
	movq	%rax, 16(%rbx)
	movl	$0, (%rax)
	movl	$3, 28(%rbx)
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_16:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i5.i
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_16
# BB#17:
	movq	%r13, %r14
	movq	%r15, %r12
	movl	$2, 24(%rbx)
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	8(%rax), %rdi
.Ltmp146:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp147:
# BB#18:
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	24(%r12), %rax
	movslq	20(%r12), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 20(%r12)
	movslq	216(%r12), %rbx
	testq	%rbx, %rbx
	je	.LBB9_22
# BB#19:
	leaq	1(%rbx), %rax
	testl	%eax, %eax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	je	.LBB9_24
# BB#20:                                # %._crit_edge16.i.i313
	movq	%r13, %r14
	movq	%r15, %r12
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp148:
	callq	_Znam
	movq	%rax, %rsi
.Ltmp149:
# BB#21:                                # %.noexc317
	movl	$0, (%rsi)
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB9_25
.LBB9_22:                               # %_Z11MyStringLenIwEiPKT_.exit.i302
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	$0, 48(%r14)
	movq	40(%r14), %rbx
	movl	$0, (%rbx)
	movl	52(%r14), %ebp
	cmpl	$26, %ebp
	jne	.LBB9_46
# BB#23:
	movq	56(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB9_52
.LBB9_24:
	xorl	%esi, %esi
.LBB9_25:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i314
	movq	%rbx, 112(%rsp)         # 8-byte Spill
	leaq	208(%r12), %rcx
	movq	(%rcx), %rax
	movq	%rsi, %rdx
	movq	16(%rsp), %r14          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_26:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %edi
	addq	$4, %rax
	movl	%edi, (%rdx)
	addq	$4, %rdx
	testl	%edi, %edi
	jne	.LBB9_26
# BB#27:
.Ltmp150:
	xorl	%ebx, %ebx
	xorl	%edi, %edi
	xorl	%edx, %edx
	movq	%rsi, 48(%rsp)          # 8-byte Spill
	callq	_ZN8NWindows5NFile10NDirectory12MySearchPathEPKwS3_S3_R11CStringBaseIwE
.Ltmp151:
# BB#28:
	testb	%al, %al
	movq	56(%rsp), %rbp          # 8-byte Reload
	jne	.LBB9_43
# BB#29:                                # %_Z11MyStringLenIwEiPKT_.exit.i320
	callq	__errno_location
	movl	(%rax), %eax
	movl	%eax, (%r14)
	movl	$0, 48(%r14)
	movq	40(%r14), %rbx
	movl	$0, (%rbx)
	movl	52(%r14), %r12d
	cmpl	$39, %r12d
	je	.LBB9_35
# BB#30:
.Ltmp152:
	movl	$156, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp153:
# BB#31:                                # %.noexc329
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_34
# BB#32:                                # %.noexc329
	testl	%r12d, %r12d
	jle	.LBB9_34
# BB#33:                                # %._crit_edge.thread.i.i324
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r14), %rax
.LBB9_34:                               # %._crit_edge16.i.i325
	movq	%rbp, 40(%r14)
	movl	$0, (%rbp,%rax,4)
	movl	$39, 52(%r14)
	movq	%rbp, %rbx
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB9_35:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i328.preheader
	movq	48(%rsp), %rax          # 8-byte Reload
	movq	32(%rsp), %rcx          # 8-byte Reload
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [55,45,90,105]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [112,32,99,97]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [110,110,111,116]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_4(%rip), %xmm0   # xmm0 = [32,102,105,110]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI9_5(%rip), %xmm0   # xmm0 = [100,32,115,112]
	movups	%xmm0, 64(%rbx)
	movaps	.LCPI9_6(%rip), %xmm0   # xmm0 = [101,99,105,102]
	movups	%xmm0, 80(%rbx)
	movaps	.LCPI9_7(%rip), %xmm0   # xmm0 = [105,101,100,32]
	movups	%xmm0, 96(%rbx)
	movaps	.LCPI9_8(%rip), %xmm0   # xmm0 = [83,70,88,32]
	movups	%xmm0, 112(%rbx)
	movdqa	.LCPI9_9(%rip), %xmm0   # xmm0 = [109,111,100,117]
	movdqu	%xmm0, 128(%rbx)
	movl	$108, 144(%rbx)
	movl	$101, 148(%rbx)
	movl	$0, 152(%rbx)
	movl	$38, 48(%r14)
	movl	$0, 16(%r14)
	movq	8(%r14), %rbx
	movl	$0, (%rbx)
	movl	20(%r14), %r12d
	cmpl	%r12d, %ecx
	je	.LBB9_41
# BB#36:
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp154:
	callq	_Znam
	movq	%rax, %rbp
.Ltmp155:
# BB#37:                                # %.noexc339
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_40
# BB#38:                                # %.noexc339
	testl	%r12d, %r12d
	jle	.LBB9_40
# BB#39:                                # %._crit_edge.thread.i.i334
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	16(%r14), %rax
.LBB9_40:                               # %._crit_edge16.i.i335
	movq	%rbp, 8(%r14)
	movl	$0, (%rbp,%rax,4)
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%eax, 20(%r14)
	movq	%rbp, %rbx
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	48(%rsp), %rax          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_41:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i336
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_41
# BB#42:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i
	movq	112(%rsp), %rax         # 8-byte Reload
	movl	%eax, 16(%r14)
	movl	$1, %ebx
.LBB9_43:                               # %_ZN11CStringBaseIwEaSERKS0_.exit
	movq	48(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	je	.LBB9_45
# BB#44:
	callq	_ZdaPv
.LBB9_45:                               # %_ZN9CPropertyD2Ev.exit
	movq	8(%rsp), %r12           # 8-byte Reload
	jmp	.LBB9_53
.LBB9_46:
.Ltmp157:
	movq	%r13, %r14
	movq	%r15, %r12
	movl	$104, %edi
	callq	_Znam
.Ltmp158:
# BB#47:                                # %.noexc311
	xorl	%ecx, %ecx
	testq	%rbx, %rbx
	je	.LBB9_50
# BB#48:                                # %.noexc311
	testl	%ebp, %ebp
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
	jle	.LBB9_51
# BB#49:                                # %._crit_edge.thread.i.i306
	movq	%rbx, %rdi
	movq	%rax, %rbx
	callq	_ZdaPv
	movq	%rbx, %rax
	movslq	48(%r14), %rcx
	jmp	.LBB9_51
.LBB9_50:
	movq	16(%rsp), %r14          # 8-byte Reload
	movq	8(%rsp), %r12           # 8-byte Reload
	movq	56(%rsp), %rbp          # 8-byte Reload
.LBB9_51:                               # %._crit_edge16.i.i307
	movq	%rax, 40(%r14)
	movl	$0, (%rax,%rcx,4)
	movl	$26, 52(%r14)
	movq	%rax, %rbx
.LBB9_52:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i310.preheader
	movaps	.LCPI9_8(%rip), %xmm0   # xmm0 = [83,70,88,32]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_10(%rip), %xmm0  # xmm0 = [102,105,108,101]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_11(%rip), %xmm0  # xmm0 = [32,105,115,32]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_12(%rip), %xmm0  # xmm0 = [110,111,116,32]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI9_13(%rip), %xmm0  # xmm0 = [115,112,101,99]
	movups	%xmm0, 64(%rbx)
	movdqa	.LCPI9_14(%rip), %xmm0  # xmm0 = [105,102,105,101]
	movdqu	%xmm0, 80(%rbx)
	movl	$100, 96(%rbx)
	movl	$0, 100(%rbx)
	movl	$25, 48(%r14)
	movl	$1, %ebx
.LBB9_53:                               # %_ZN9CPropertyD2Ev.exit
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r13, %rdi
	callq	_ZdaPv
	testl	%ebx, %ebx
	movl	$-2147467259, %ebx      # imm = 0x80004005
	movq	80(%rsp), %r15          # 8-byte Reload
	movq	216(%rsp), %r13         # 8-byte Reload
	movq	96(%rsp), %rsi          # 8-byte Reload
	jne	.LBB9_570
.LBB9_54:                               # %.thread1104
	movq	%rsi, 96(%rsp)          # 8-byte Spill
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 536(%rsp)
	movq	$8, 552(%rsp)
	movq	$_ZTV13CObjectVectorI4CArcE+16, 528(%rsp)
	movdqu	%xmm0, 568(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 560(%rsp)
	movl	$8, %eax
	movd	%rax, %xmm0
	movdqu	%xmm0, 584(%rsp)
	movb	$0, 600(%rsp)
	leaq	80(%r12), %rbx
.Ltmp160:
	leaq	128(%rsp), %rdi
	movq	%rbx, %rsi
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp161:
# BB#55:
	cmpl	$0, 88(%r12)
	movq	%r13, 216(%rsp)         # 8-byte Spill
	movq	%rbx, 56(%rsp)          # 8-byte Spill
	je	.LBB9_62
# BB#56:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 504(%rsp)
.Ltmp163:
	movl	$16, %edi
	callq	_Znam
.Ltmp164:
# BB#57:
	movq	%rax, 504(%rsp)
	movl	$0, (%rax)
	movl	$4, 516(%rsp)
	movq	128(%rsp), %rsi
.Ltmp166:
	leaq	464(%rsp), %rdi
	callq	_ZN8NWindows5NFile5NFind10CFileInfoW4FindEPKw
.Ltmp167:
# BB#58:
	movl	$-2147467259, %r13d     # imm = 0x80004005
	testb	%al, %al
	je	.LBB9_84
# BB#59:
	testb	$16, 496(%rsp)
	jne	.LBB9_574
# BB#60:
	cmpl	$0, 300(%r12)
	jle	.LBB9_63
# BB#61:
	movl	$-2147467263, %r13d     # imm = 0x80004001
	movl	$1, %ebx
	jmp	.LBB9_85
.LBB9_62:
	movl	$-2147467259, %ebx      # imm = 0x80004005
	jmp	.LBB9_88
.LBB9_63:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 408(%rsp)
	movq	$4, 424(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 400(%rsp)
	movl	(%r12), %ebx
	testl	%ebx, %ebx
	movq	216(%rsp), %r12         # 8-byte Reload
	js	.LBB9_66
# BB#64:
.Ltmp171:
	leaq	400(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp172:
# BB#65:                                # %_ZN13CRecordVectorIiE3AddEi.exit
	movq	416(%rsp), %rax
	movslq	412(%rsp), %rcx
	movl	%ebx, (%rax,%rcx,4)
	incl	412(%rsp)
.LBB9_66:
.Ltmp173:
	movq	%rbp, (%rsp)
	leaq	528(%rsp), %rdi
	leaq	400(%rsp), %rdx
	leaq	128(%rsp), %r9
	xorl	%ecx, %ecx
	xorl	%r8d, %r8d
	movq	%r12, %rsi
	callq	_ZN12CArchiveLink5Open2EP7CCodecsRK13CRecordVectorIiEbP9IInStreamRK11CStringBaseIwEP15IOpenCallbackUI
	movl	%eax, %ebx
.Ltmp174:
# BB#67:
	movl	$-2147467260, %r13d     # imm = 0x80004004
	movl	$1, %r12d
	cmpl	$-2147467260, %ebx      # imm = 0x80004004
	je	.LBB9_82
# BB#68:
	movq	(%r15), %rax
	movq	128(%rsp), %rsi
.Ltmp176:
	movq	%r15, %rdi
	movl	%ebx, %edx
	callq	*88(%rax)
.Ltmp177:
# BB#69:
	movl	%eax, %r13d
	testl	%eax, %eax
	jne	.LBB9_81
# BB#70:
	testl	%ebx, %ebx
	je	.LBB9_72
# BB#71:
	movl	%ebx, %r13d
	jmp	.LBB9_81
.LBB9_72:
	cmpl	$2, 572(%rsp)
	jl	.LBB9_80
# BB#73:                                # %_Z11MyStringLenIwEiPKT_.exit.i352
	movl	$-2147467263, (%r14)    # imm = 0x80004001
	movl	$0, 48(%r14)
	movq	40(%r14), %rbx
	movl	$0, (%rbx)
	movl	52(%r14), %r15d
	cmpl	$53, %r15d
	je	.LBB9_79
# BB#74:
.Ltmp178:
	movl	$212, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp179:
# BB#75:                                # %.noexc362
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_78
# BB#76:                                # %.noexc362
	testl	%r15d, %r15d
	jle	.LBB9_78
# BB#77:                                # %._crit_edge.thread.i.i356
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r14), %rax
.LBB9_78:                               # %._crit_edge16.i.i357
	movq	%rbp, 40(%r14)
	movl	$0, (%rbp,%rax,4)
	movl	$53, 52(%r14)
	movq	%rbp, %rbx
.LBB9_79:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i360.preheader
	movq	80(%rsp), %r15          # 8-byte Reload
	movaps	.LCPI9_15(%rip), %xmm0  # xmm0 = [85,112,100,97]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_16(%rip), %xmm0  # xmm0 = [116,105,110,103]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_17(%rip), %xmm0  # xmm0 = [32,102,111,114]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_18(%rip), %xmm0  # xmm0 = [32,109,117,108]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI9_19(%rip), %xmm0  # xmm0 = [116,105,118,111]
	movups	%xmm0, 64(%rbx)
	movaps	.LCPI9_20(%rip), %xmm0  # xmm0 = [108,117,109,101]
	movups	%xmm0, 80(%rbx)
	movaps	.LCPI9_21(%rip), %xmm0  # xmm0 = [32,97,114,99]
	movups	%xmm0, 96(%rbx)
	movaps	.LCPI9_22(%rip), %xmm0  # xmm0 = [104,105,118,101]
	movups	%xmm0, 112(%rbx)
	movaps	.LCPI9_23(%rip), %xmm0  # xmm0 = [115,32,105,115]
	movups	%xmm0, 128(%rbx)
	movaps	.LCPI9_24(%rip), %xmm0  # xmm0 = [32,110,111,116]
	movups	%xmm0, 144(%rbx)
	movaps	.LCPI9_25(%rip), %xmm0  # xmm0 = [32,105,109,112]
	movups	%xmm0, 160(%rbx)
	movaps	.LCPI9_26(%rip), %xmm0  # xmm0 = [108,101,109,101]
	movups	%xmm0, 176(%rbx)
	movdqa	.LCPI9_27(%rip), %xmm0  # xmm0 = [110,116,101,100]
	movdqu	%xmm0, 192(%rbx)
	movl	$0, 208(%rbx)
	movl	$52, 48(%r14)
	movl	$-2147467263, %r13d     # imm = 0x80004001
	jmp	.LBB9_82
.LBB9_80:
	movslq	540(%rsp), %rax
	movq	544(%rsp), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movb	500(%rsp), %cl
	xorb	$1, %cl
	movb	%cl, 56(%rax)
	movq	488(%rsp), %rcx
	movq	%rcx, 48(%rax)
	movl	$-2147467259, %r13d     # imm = 0x80004005
	xorl	%r12d, %r12d
.LBB9_81:
	movq	80(%rsp), %r15          # 8-byte Reload
.LBB9_82:
.Ltmp183:
	leaq	400(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp184:
# BB#83:
	movl	$1, %ebx
	testl	%r12d, %r12d
	movq	8(%rsp), %r12           # 8-byte Reload
	jne	.LBB9_85
.LBB9_84:
	xorl	%ebx, %ebx
.LBB9_85:
	movq	504(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_87
# BB#86:
	callq	_ZdaPv
.LBB9_87:                               # %_ZN8NWindows5NFile5NFind10CFileInfoWD2Ev.exit
	testl	%ebx, %ebx
	movl	%r13d, %ebx
	jne	.LBB9_567
.LBB9_88:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 712(%rsp)
	movq	$8, 728(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 704(%rsp)
	movdqu	%xmm0, 744(%rsp)
	movq	$4, 760(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 736(%rsp)
	movdqu	%xmm0, 776(%rsp)
	movq	$4, 792(%rsp)
	movq	$_ZTV13CRecordVectorIiE+16, 768(%rsp)
	movdqu	%xmm0, 808(%rsp)
	movq	$8, 824(%rsp)
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, 800(%rsp)
	cmpb	$0, 225(%r12)
	je	.LBB9_102
# BB#89:
	movl	%ebx, 48(%rsp)          # 4-byte Spill
	movdqu	%xmm0, 176(%rsp)
.Ltmp186:
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp187:
# BB#90:
	leaq	176(%rsp), %rax
	movq	%rbx, 176(%rsp)
	movl	$0, (%rbx)
	movl	$4, 188(%rsp)
	movl	$-1, 196(%rsp)
	movl	$-1, 200(%rsp)
	leaq	232(%r12), %r15
	cmpq	%rax, %r15
	je	.LBB9_97
# BB#91:
	movl	$0, 184(%rsp)
	movl	$0, (%rbx)
	movslq	240(%r12), %rbp
	incq	%rbp
	cmpl	$4, %ebp
	je	.LBB9_94
# BB#92:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp189:
	callq	_Znam
	movq	%rax, %r14
.Ltmp190:
# BB#93:                                # %._crit_edge16.i.i372
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	184(%rsp), %rax
	movq	%r14, 176(%rsp)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 188(%rsp)
	movq	%r14, %rbx
.LBB9_94:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i373
	movq	(%r15), %rax
	.p2align	4, 0x90
.LBB9_95:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_95
# BB#96:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i376
	movl	240(%r12), %eax
	movl	%eax, 184(%rsp)
.LBB9_97:                               # %_ZN11CStringBaseIwEaSERKS0_.exit378
	movq	$-1, 144(%rsp)
	movl	$0, 192(%rsp)
	leaq	168(%rsp), %rdi
.Ltmp191:
	callq	_ZN8NWindows5NTime17GetCurUtcFileTimeER9_FILETIME
.Ltmp192:
# BB#98:
	movq	168(%rsp), %rax
	movq	%rax, 160(%rsp)
	movq	%rax, 152(%rsp)
.Ltmp193:
	movl	$64, %edi
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp194:
# BB#99:                                # %.noexc380
	movdqu	144(%rsp), %xmm0
	movdqu	160(%rsp), %xmm1
	movdqu	%xmm1, 16(%rbx)
	movdqu	%xmm0, (%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rbx)
	movslq	184(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	je	.LBB9_116
# BB#100:                               # %._crit_edge16.i.i.i.i
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp195:
	callq	_Znam
.Ltmp196:
# BB#101:                               # %.noexc.i379
	movq	%rax, 32(%rbx)
	movl	$0, (%rax)
	movl	%ebp, 44(%rbx)
	jmp	.LBB9_117
.LBB9_102:                              # %.preheader601
	movslq	52(%r12), %rax
	testq	%rax, %rax
	jle	.LBB9_123
# BB#103:                               # %.lr.ph847
	movl	%ebx, %r14d
	movq	56(%r12), %rcx
	xorl	%edx, %edx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB9_104:                              # =>This Inner Loop Header: Depth=1
	movq	(%rcx,%rdx,8), %rbx
	cmpl	$2, 136(%rbx)
	je	.LBB9_112
# BB#105:                               #   in Loop: Header=BB9_104 Depth=1
	movl	140(%rbx), %ebp
	cmpl	$2, %ebp
	je	.LBB9_112
# BB#106:                               #   in Loop: Header=BB9_104 Depth=1
	movl	144(%rbx), %edi
	cmpl	$2, %edi
	je	.LBB9_112
# BB#107:                               #   in Loop: Header=BB9_104 Depth=1
	movl	148(%rbx), %r8d
	cmpl	$2, %r8d
	je	.LBB9_112
# BB#108:                               #   in Loop: Header=BB9_104 Depth=1
	movl	152(%rbx), %r9d
	cmpl	$2, %r9d
	je	.LBB9_112
# BB#109:                               #   in Loop: Header=BB9_104 Depth=1
	movl	156(%rbx), %r10d
	cmpl	$2, %r10d
	je	.LBB9_112
# BB#110:                               #   in Loop: Header=BB9_104 Depth=1
	movl	160(%rbx), %ebx
	cmpl	$2, %ebx
	je	.LBB9_112
# BB#111:                               # %_ZNK14NUpdateArchive10CActionSet12NeedScanningEv.exit
                                        #   in Loop: Header=BB9_104 Depth=1
	orl	%ebp, %edi
	orl	%r8d, %edi
	orl	%r9d, %edi
	orl	%r10d, %edi
	orl	%ebx, %edi
	je	.LBB9_113
	.p2align	4, 0x90
.LBB9_112:                              # %_ZNK14NUpdateArchive10CActionSet12NeedScanningEv.exit.thread
                                        #   in Loop: Header=BB9_104 Depth=1
	movb	$1, %sil
.LBB9_113:                              #   in Loop: Header=BB9_104 Depth=1
	incq	%rdx
	cmpq	%rax, %rdx
	jl	.LBB9_104
# BB#114:                               # %._crit_edge848
	testb	$1, %sil
	jne	.LBB9_468
# BB#115:
	movl	%r14d, %ebx
	jmp	.LBB9_123
.LBB9_116:
	xorl	%eax, %eax
.LBB9_117:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i
	leaq	192(%rsp), %rcx
	movq	176(%rsp), %rdx
	.p2align	4, 0x90
.LBB9_118:                              # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rax)
	addq	$4, %rax
	testl	%esi, %esi
	jne	.LBB9_118
# BB#119:
	movl	%r14d, 40(%rbx)
	movl	8(%rcx), %eax
	movl	%eax, 56(%rbx)
	movq	(%rcx), %rax
	movq	%rax, 48(%rbx)
	leaq	800(%rsp), %rdi
.Ltmp198:
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp199:
# BB#120:
	movq	816(%rsp), %rax
	movslq	812(%rsp), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 812(%rsp)
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_122
# BB#121:
	callq	_ZdaPv
.LBB9_122:                              # %_ZN8CDirItemD2Ev.exit
	movl	48(%rsp), %ebx          # 4-byte Reload
.LBB9_123:                              # %.thread
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 440(%rsp)
	movq	$8, 456(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 432(%rsp)
	movb	600(%rsp), %al
	movb	%al, 31(%rsp)           # 1-byte Spill
	cmpb	$0, 248(%r12)
	movl	%ebx, 48(%rsp)          # 4-byte Spill
	je	.LBB9_158
.LBB9_124:
	xorl	%ebp, %ebp
.LBB9_125:
	movq	8(%rsp), %rbx           # 8-byte Reload
	movl	52(%rbx), %eax
	testl	%eax, %eax
	movl	%ebp, 56(%rsp)          # 4-byte Spill
	jle	.LBB9_154
# BB#126:                               # %.lr.ph
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB9_127:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_148 Depth 2
	testb	%bpl, %bpl
	sete	%al
	testq	%r15, %r15
	setg	%cl
	cmpb	$0, 248(%rbx)
	jne	.LBB9_153
# BB#128:                               #   in Loop: Header=BB9_127 Depth=1
	orb	%cl, %al
	je	.LBB9_153
# BB#129:                               #   in Loop: Header=BB9_127 Depth=1
	movq	56(%rbx), %rax
	movq	(%rax,%r15,8), %rsi
	addq	$16, %rsi
.Ltmp244:
	leaq	248(%rsp), %rdi
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp245:
# BB#130:                               #   in Loop: Header=BB9_127 Depth=1
	movq	248(%rsp), %rdi
.Ltmp247:
	callq	_ZN8NWindows5NFile5NFind18DoesFileOrDirExistEPKw
.Ltmp248:
# BB#131:                               #   in Loop: Header=BB9_127 Depth=1
	xorl	%r13d, %r13d
	testb	%al, %al
	movq	16(%rsp), %rbp          # 8-byte Reload
	je	.LBB9_150
# BB#132:                               # %_Z11MyStringLenIwEiPKT_.exit.i424
                                        #   in Loop: Header=BB9_127 Depth=1
	movl	$0, (%rbp)
	movl	$0, 48(%rbp)
	movq	40(%rbp), %rbx
	movl	$0, (%rbx)
	movl	52(%rbp), %r14d
	cmpl	$24, %r14d
	je	.LBB9_139
# BB#133:                               #   in Loop: Header=BB9_127 Depth=1
.Ltmp249:
	movl	$96, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp250:
# BB#134:                               # %.noexc434
                                        #   in Loop: Header=BB9_127 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_137
# BB#135:                               # %.noexc434
                                        #   in Loop: Header=BB9_127 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	jle	.LBB9_138
# BB#136:                               # %._crit_edge.thread.i.i428
                                        #   in Loop: Header=BB9_127 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%rbp), %rax
	jmp	.LBB9_138
.LBB9_137:                              #   in Loop: Header=BB9_127 Depth=1
	xorl	%eax, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB9_138:                              # %._crit_edge16.i.i429
                                        #   in Loop: Header=BB9_127 Depth=1
	movq	%r12, 40(%rbp)
	movl	$0, (%r12,%rax,4)
	movl	$24, 52(%rbp)
	movq	%r12, %rbx
.LBB9_139:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i432.preheader
                                        #   in Loop: Header=BB9_127 Depth=1
	leaq	248(%rsp), %rax
	cmpq	32(%rsp), %rax          # 8-byte Folded Reload
	movaps	.LCPI9_31(%rip), %xmm0  # xmm0 = [84,104,101,32]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_10(%rip), %xmm0  # xmm0 = [102,105,108,101]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_32(%rip), %xmm0  # xmm0 = [32,97,108,114]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_33(%rip), %xmm0  # xmm0 = [101,97,100,121]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI9_34(%rip), %xmm0  # xmm0 = [32,101,120,105]
	movups	%xmm0, 64(%rbx)
	movdqa	.LCPI9_35(%rip), %xmm0  # xmm0 = [115,116,115,0]
	movdqu	%xmm0, 80(%rbx)
	movl	$23, 48(%rbp)
	movl	$-2147467259, 48(%rsp)  # 4-byte Folded Spill
                                        # imm = 0x80004005
	movl	$1, %r13d
	je	.LBB9_150
# BB#140:                               #   in Loop: Header=BB9_127 Depth=1
	movl	$0, 16(%rbp)
	movq	8(%rbp), %rbx
	movl	$0, (%rbx)
	movslq	256(%rsp), %r14
	incq	%r14
	movl	20(%rbp), %ebp
	cmpl	%ebp, %r14d
	je	.LBB9_147
# BB#141:                               #   in Loop: Header=BB9_127 Depth=1
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp251:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp252:
# BB#142:                               # %.noexc445
                                        #   in Loop: Header=BB9_127 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_145
# BB#143:                               # %.noexc445
                                        #   in Loop: Header=BB9_127 Depth=1
	testl	%ebp, %ebp
	movl	$0, %eax
	jle	.LBB9_146
# BB#144:                               # %._crit_edge.thread.i.i439
                                        #   in Loop: Header=BB9_127 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	16(%rax), %rax
	jmp	.LBB9_146
.LBB9_145:                              #   in Loop: Header=BB9_127 Depth=1
	xorl	%eax, %eax
.LBB9_146:                              # %._crit_edge16.i.i440
                                        #   in Loop: Header=BB9_127 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r12, 8(%rcx)
	movl	$0, (%r12,%rax,4)
	movl	%r14d, 20(%rcx)
	movq	%r12, %rbx
.LBB9_147:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i441
                                        #   in Loop: Header=BB9_127 Depth=1
	movq	248(%rsp), %rax
	.p2align	4, 0x90
.LBB9_148:                              #   Parent Loop BB9_127 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_148
# BB#149:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i444
                                        #   in Loop: Header=BB9_127 Depth=1
	movl	256(%rsp), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 16(%rcx)
.LBB9_150:                              # %_ZN11CStringBaseIwEaSERKS0_.exit446
                                        #   in Loop: Header=BB9_127 Depth=1
	movq	248(%rsp), %rdi
	testq	%rdi, %rdi
	movq	8(%rsp), %rbx           # 8-byte Reload
	je	.LBB9_152
# BB#151:                               #   in Loop: Header=BB9_127 Depth=1
	callq	_ZdaPv
.LBB9_152:                              # %_ZN11CStringBaseIwED2Ev.exit448
                                        #   in Loop: Header=BB9_127 Depth=1
	testl	%r13d, %r13d
	movl	56(%rsp), %ebp          # 4-byte Reload
	jne	.LBB9_564
.LBB9_153:                              #   in Loop: Header=BB9_127 Depth=1
	incq	%r15
	movslq	52(%rbx), %rax
	cmpq	%rax, %r15
	jl	.LBB9_127
.LBB9_154:                              # %._crit_edge
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 336(%rsp)
	movq	$8, 352(%rsp)
	movq	$_ZTV13CObjectVectorI8CArcItemE+16, 328(%rsp)
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	movq	80(%rsp), %r14          # 8-byte Reload
	je	.LBB9_163
# BB#155:
	movslq	540(%rsp), %rax
	movq	544(%rsp), %rcx
	movq	-8(%rcx,%rax,8), %rsi
.Ltmp254:
	leaq	328(%rsp), %rdx
	movq	96(%rsp), %rdi          # 8-byte Reload
	callq	_Z23EnumerateInArchiveItemsRKN9NWildcard7CCensorERK4CArcR13CObjectVectorI8CArcItemE
	movl	%eax, %r13d
.Ltmp255:
# BB#156:
	testl	%r13d, %r13d
	jne	.LBB9_561
# BB#157:
	movslq	540(%rsp), %rax
	movq	544(%rsp), %rcx
	movq	-8(%rcx,%rax,8), %rax
	movq	(%rax), %rcx
	movl	52(%rbx), %eax
	testl	%eax, %eax
	jg	.LBB9_164
	jmp	.LBB9_450
.LBB9_158:
	cmpb	$0, 72(%r12)
	je	.LBB9_124
# BB#159:
	movq	56(%r12), %rax
	movq	(%rax), %rbx
	leaq	16(%rbx), %rdi
.Ltmp237:
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	_ZN12CArchivePathaSERKS_
.Ltmp238:
# BB#160:
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	je	.LBB9_471
# BB#161:
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 300(%rax)
	jne	.LBB9_124
# BB#474:
	cmpl	$0, 280(%rax)
	movb	$1, 96(%rbx)
	je	.LBB9_502
# BB#475:
	leaq	280(%rax), %rdx
	jmp	.LBB9_476
.LBB9_163:
	xorl	%ecx, %ecx
	testl	%eax, %eax
	jle	.LBB9_450
.LBB9_164:                              # %.lr.ph.i
	leaq	208(%rbx), %rax
	movq	%rax, 232(%rsp)         # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 224(%rsp)         # 8-byte Spill
	leaq	8(%rbx), %rax
	movq	%rax, 520(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	movl	$_ZTV13CInFileStream+96, %eax
	movd	%rax, %xmm0
	movl	$_ZTV13CInFileStream+16, %eax
	movd	%rax, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqa	%xmm1, 832(%rsp)        # 16-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	jmp	.LBB9_265
	.p2align	4, 0x90
.LBB9_165:                              #   in Loop: Header=BB9_265 Depth=1
	movq	(%r14), %rax
.Ltmp489:
	movq	%r14, %rdi
	callq	*136(%rax)
	movl	%eax, %r12d
.Ltmp490:
# BB#166:                               # %.noexc463
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	jne	.LBB9_449
# BB#167:                               #   in Loop: Header=BB9_265 Depth=1
	incq	%r13
	movq	8(%rsp), %rbx           # 8-byte Reload
	movslq	52(%rbx), %rax
	cmpq	%rax, %r13
	movq	96(%rsp), %rcx          # 8-byte Reload
	jl	.LBB9_265
	jmp	.LBB9_450
.LBB9_168:                              #   in Loop: Header=BB9_265 Depth=1
	cmpb	$0, 88(%rsp)            # 1-byte Folded Reload
	je	.LBB9_253
# BB#169:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp412:
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %rbx
.Ltmp413:
# BB#170:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 16(%rbx)
	movdqa	832(%rsp), %xmm0        # 16-byte Reload
	movdqu	%xmm0, (%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 24(%rbx)
	movl	$-1, 32(%rbx)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 40(%rbx)
.Ltmp414:
	movl	$4, %edi
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	callq	_Znam
.Ltmp415:
# BB#171:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%r15, 104(%rsp)         # 8-byte Spill
	movq	%r14, 88(%rsp)          # 8-byte Spill
	movq	%rax, 40(%rbx)
	movb	$0, (%rax)
	movl	$4, 52(%rbx)
	movq	$_ZTVN8NWindows5NFile3NIO7CInFileE+16, 24(%rbx)
	movb	$0, 20(%rbx)
.Ltmp417:
	movq	%rbx, %rdi
	callq	*_ZTV13CInFileStream+24(%rip)
.Ltmp418:
# BB#172:                               # %_ZN9CMyComPtrI9IInStreamEC2EPS0_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rsi
.Ltmp420:
	movq	%rbx, 32(%rsp)          # 8-byte Spill
	movq	%rbx, %rdi
	callq	_ZN13CInFileStream4OpenEPKw
.Ltmp421:
# BB#173:                               #   in Loop: Header=BB9_265 Depth=1
	testb	%al, %al
	je	.LBB9_187
# BB#174:                               #   in Loop: Header=BB9_265 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 300(%rax)
	je	.LBB9_192
# BB#175:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp427:
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp428:
# BB#176:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 8(%rbp)
	movq	$_ZTV14COutFileStream+16, (%rbp)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 16(%rbp)
	movl	$-1, 24(%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rbp)
.Ltmp429:
	movl	$4, %edi
	callq	_Znam
.Ltmp430:
# BB#177:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rax, 32(%rbp)
	movb	$0, (%rax)
	movl	$4, 44(%rbp)
	movq	$_ZTVN8NWindows5NFile3NIO8COutFileE+16, 16(%rbp)
.Ltmp432:
	movq	%rbp, %rdi
	callq	*_ZTV14COutFileStream+24(%rip)
.Ltmp433:
# BB#178:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit406.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
.Ltmp434:
	leaq	264(%rsp), %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp435:
# BB#179:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, %rdi
	addq	$16, %rdi
	movq	264(%rsp), %rsi
	movq	$0, 1104(%rbp)
.Ltmp436:
	xorl	%edx, %edx
	callq	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
.Ltmp437:
# BB#180:                               # %_ZN14COutFileStream6CreateEPKwb.exit408.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	xorl	%ebx, %ebx
	testb	%al, %al
	movq	88(%rsp), %r14          # 8-byte Reload
	jne	.LBB9_238
# BB#181:                               #   in Loop: Header=BB9_265 Depth=1
	callq	__errno_location
	leaq	264(%rsp), %rcx
	cmpq	224(%rsp), %rcx         # 8-byte Folded Reload
	movl	(%rax), %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	%eax, (%r12)
	je	.LBB9_230
# BB#182:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 16(%r12)
	movq	8(%r12), %rbx
	movl	$0, (%rbx)
	movslq	272(%rsp), %r14
	incq	%r14
	movl	20(%r12), %r15d
	cmpl	%r15d, %r14d
	je	.LBB9_227
# BB#183:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%r14, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp438:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp439:
# BB#184:                               # %.noexc418.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_225
# BB#185:                               # %.noexc418.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB9_226
# BB#186:                               # %._crit_edge.thread.i.i412.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	16(%rax), %rax
	jmp	.LBB9_226
.LBB9_187:                              # %_Z11MyStringLenIwEiPKT_.exit.i377.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	callq	__errno_location
	movl	(%rax), %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	movl	%eax, (%rbp)
	movl	$0, 48(%rbp)
	movq	40(%rbp), %rbx
	movl	$0, (%rbx)
	movl	52(%rbp), %r14d
	cmpl	$29, %r14d
	je	.LBB9_196
# BB#188:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp422:
	movl	$116, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp423:
# BB#189:                               # %.noexc387.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_194
# BB#190:                               # %.noexc387.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
	jle	.LBB9_195
# BB#191:                               # %._crit_edge.thread.i.i381.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%rbp), %rax
	jmp	.LBB9_195
.LBB9_192:                              #   in Loop: Header=BB9_265 Depth=1
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp443:
	movq	32(%rsp), %rbx          # 8-byte Reload
	callq	*8(%rax)
.Ltmp444:
	movq	88(%rsp), %r14          # 8-byte Reload
# BB#193:                               #   in Loop: Header=BB9_265 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	xorl	%r15d, %r15d
	movq	%rbx, %rax
	jmp	.LBB9_243
.LBB9_194:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
	movq	16(%rsp), %rbp          # 8-byte Reload
.LBB9_195:                              # %._crit_edge16.i.i382.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%r15, 40(%rbp)
	movl	$0, (%r15,%rax,4)
	movl	$29, 52(%rbp)
	movq	%r15, %rbx
.LBB9_196:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i385.preheader.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	224(%rsp), %rax         # 8-byte Reload
	cmpq	232(%rsp), %rax         # 8-byte Folded Reload
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [55,45,90,105]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [112,32,99,97]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [110,110,111,116]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_36(%rip), %xmm0  # xmm0 = [32,111,112,101]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI9_38(%rip), %xmm0  # xmm0 = [110,32,83,70]
	movups	%xmm0, 64(%rbx)
	movaps	.LCPI9_39(%rip), %xmm0  # xmm0 = [88,32,109,111]
	movups	%xmm0, 80(%rbx)
	movdqa	.LCPI9_40(%rip), %xmm0  # xmm0 = [100,117,108,101]
	movdqu	%xmm0, 96(%rbx)
	movl	$0, 112(%rbx)
	movl	$28, 48(%rbp)
	movq	%rbp, %rax
	movl	$-2147467259, %r12d     # imm = 0x80004005
	movl	$1, %ebx
	je	.LBB9_202
# BB#197:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 16(%rax)
	movq	8(%rax), %rbx
	movl	$0, (%rbx)
	movq	8(%rsp), %rcx           # 8-byte Reload
	movslq	216(%rcx), %rbp
	incq	%rbp
	movl	20(%rax), %r15d
	cmpl	%r15d, %ebp
	je	.LBB9_222
# BB#198:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp424:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp425:
# BB#199:                               # %.noexc398.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_220
# BB#200:                               # %.noexc398.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB9_221
# BB#201:                               # %._crit_edge.thread.i.i392.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	16(%rax), %rax
	jmp	.LBB9_221
.LBB9_202:                              #   in Loop: Header=BB9_265 Depth=1
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	104(%rsp), %r15         # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	jmp	.LBB9_251
.LBB9_203:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
.LBB9_204:                              # %._crit_edge16.i.i302.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%r14, 8(%r12)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 20(%r12)
	movq	%r14, %rbx
.LBB9_205:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i303.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	64(%rsp), %rax
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB9_206:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_206
# BB#207:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	72(%rsp), %eax
	movl	%eax, 16(%r12)
.LBB9_208:                              # %_ZN11CStringBaseIwEaSERKS0_.exit307.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 48(%r12)
	movq	40(%r12), %rbx
	movl	$0, (%rbx)
	movl	52(%r12), %r14d
	cmpl	$23, %r14d
	je	.LBB9_215
# BB#209:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp394:
	movl	$92, %edi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp395:
# BB#210:                               # %.noexc320.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_213
# BB#211:                               # %.noexc320.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r14d, %r14d
	movl	$0, %eax
	jle	.LBB9_214
# BB#212:                               # %._crit_edge.thread.i.i314.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r12), %rax
	jmp	.LBB9_214
.LBB9_213:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
.LBB9_214:                              # %._crit_edge16.i.i315.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, 40(%r12)
	movl	$0, (%rbp,%rax,4)
	movl	$23, 52(%r12)
	movq	%rbp, %rbx
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB9_215:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i318.preheader.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [55,45,90,105]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [112,32,99,97]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [110,110,111,116]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_36(%rip), %xmm0  # xmm0 = [32,111,112,101]
	movups	%xmm0, 48(%rbx)
	movdqa	.LCPI9_37(%rip), %xmm0  # xmm0 = [110,32,102,105]
	movdqu	%xmm0, 64(%rbx)
	movl	$108, 80(%rbx)
	movl	$101, 84(%rbx)
	movl	$0, 88(%rbx)
	movl	$22, 48(%r12)
	movl	$-2147467259, %r12d     # imm = 0x80004005
	xorl	%ebx, %ebx
.LBB9_216:                              # %.critedge231.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_218
# BB#217:                               #   in Loop: Header=BB9_265 Depth=1
	callq	_ZdaPv
.LBB9_218:                              # %_ZN11CStringBaseIwED2Ev.exit322.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testb	%bl, %bl
	je	.LBB9_443
# BB#219:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, %r15
	xorl	%r14d, %r14d
	jmp	.LBB9_440
.LBB9_220:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
.LBB9_221:                              # %._crit_edge16.i.i393.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r14, 8(%rcx)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 20(%rcx)
	movq	%r14, %rbx
.LBB9_222:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i394.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	88(%rsp), %r14          # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	232(%rsp), %rax         # 8-byte Reload
	movq	(%rax), %rax
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB9_223:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_223
# BB#224:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i397.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	216(%rax), %eax
	movq	16(%rsp), %rcx          # 8-byte Reload
	movl	%eax, 16(%rcx)
	movq	104(%rsp), %r15         # 8-byte Reload
	movl	$1, %ebx
	jmp	.LBB9_251
.LBB9_225:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
.LBB9_226:                              # %._crit_edge16.i.i413.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r12, 8(%rcx)
	movl	$0, (%r12,%rax,4)
	movl	%r14d, 20(%rcx)
	movq	%r12, %rbx
	movq	%rcx, %r12
.LBB9_227:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i414.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	264(%rsp), %rax
.LBB9_228:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_228
# BB#229:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i417.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	272(%rsp), %eax
	movl	%eax, 16(%r12)
.LBB9_230:                              # %_ZN11CStringBaseIwEaSERKS0_.exit419.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 48(%r12)
	movq	40(%r12), %rbx
	movl	$0, (%rbx)
	movl	52(%r12), %r15d
	cmpl	$23, %r15d
	je	.LBB9_237
# BB#231:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp440:
	movl	$92, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp441:
# BB#232:                               # %.noexc432.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_235
# BB#233:                               # %.noexc432.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB9_236
# BB#234:                               # %._crit_edge.thread.i.i426.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r12), %rax
	jmp	.LBB9_236
.LBB9_235:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB9_236:                              # %._crit_edge16.i.i427.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%r14, 40(%r12)
	movl	$0, (%r14,%rax,4)
	movl	$23, 52(%r12)
	movq	%r14, %rbx
.LBB9_237:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i430.preheader.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	88(%rsp), %r14          # 8-byte Reload
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [55,45,90,105]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [112,32,99,97]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [110,110,111,116]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_36(%rip), %xmm0  # xmm0 = [32,111,112,101]
	movups	%xmm0, 48(%rbx)
	movdqa	.LCPI9_37(%rip), %xmm0  # xmm0 = [110,32,102,105]
	movdqu	%xmm0, 64(%rbx)
	movl	$108, 80(%rbx)
	movl	$101, 84(%rbx)
	movl	$0, 88(%rbx)
	movl	$22, 48(%r12)
	movl	$-2147467259, %r12d     # imm = 0x80004005
	movl	$1, %ebx
.LBB9_238:                              #   in Loop: Header=BB9_265 Depth=1
	movq	264(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_240
# BB#239:                               #   in Loop: Header=BB9_265 Depth=1
	callq	_ZdaPv
.LBB9_240:                              # %_ZN11CStringBaseIwED2Ev.exit435.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%ebx, %ebx
	movq	32(%rsp), %rax          # 8-byte Reload
	je	.LBB9_242
# BB#241:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$1, %ebx
	jmp	.LBB9_250
.LBB9_242:                              #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, %r15
.LBB9_243:                              # %_ZN9CMyComPtrI20ISequentialOutStreamEaSERKS1_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
.Ltmp446:
	xorl	%edx, %edx
	movq	%rax, %rdi
	movq	%rbp, %rsi
	callq	_ZN9NCompress10CopyStreamEP19ISequentialInStreamP20ISequentialOutStreamP21ICompressProgressInfo
.Ltmp447:
# BB#244:                               #   in Loop: Header=BB9_265 Depth=1
	testl	%eax, %eax
	je	.LBB9_246
.LBB9_245:                              #   in Loop: Header=BB9_265 Depth=1
	movl	%eax, %r12d
	movl	$1, %ebx
	jmp	.LBB9_250
.LBB9_246:                              #   in Loop: Header=BB9_265 Depth=1
	testq	%r15, %r15
	je	.LBB9_249
# BB#247:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp448:
	movq	%r15, %rdi
	callq	_ZN14COutFileStream5CloseEv
.Ltmp449:
# BB#248:                               #   in Loop: Header=BB9_265 Depth=1
	testl	%eax, %eax
	jne	.LBB9_245
.LBB9_249:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%ebx, %ebx
.LBB9_250:                              #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbp), %rax
.Ltmp453:
	movq	%rbp, %rdi
	movq	40(%rsp), %rbp          # 8-byte Reload
	callq	*16(%rax)
.Ltmp454:
	movq	104(%rsp), %r15         # 8-byte Reload
	movq	32(%rsp), %rdi          # 8-byte Reload
.LBB9_251:                              # %_ZN11CStringBaseIwEaSERKS0_.exit399.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	(%rdi), %rax
.Ltmp458:
	callq	*16(%rax)
.Ltmp459:
# BB#252:                               # %_ZN9CMyComPtrI9IInStreamED2Ev.exit439.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%ebx, %ebx
	jne	.LBB9_443
.LBB9_253:                              #   in Loop: Header=BB9_265 Depth=1
	movq	120(%rsp), %rdi
	movq	(%rdi), %rax
	movl	476(%rsp), %edx
.Ltmp461:
	movq	%rbp, %rsi
	movq	240(%rsp), %rcx         # 8-byte Reload
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp462:
# BB#254:                               #   in Loop: Header=BB9_265 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp464:
	callq	*32(%rax)
.Ltmp465:
# BB#255:                               #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	jne	.LBB9_443
# BB#256:                               #   in Loop: Header=BB9_265 Depth=1
	testq	%r15, %r15
	je	.LBB9_258
# BB#257:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp466:
	movq	%r15, %rdi
	callq	_ZN14COutFileStream5CloseEv
	movl	%eax, %r12d
.Ltmp467:
	jmp	.LBB9_443
.LBB9_258:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%r12d, %r12d
	testq	%r14, %r14
	je	.LBB9_443
# BB#259:                               #   in Loop: Header=BB9_265 Depth=1
	movl	52(%r14), %eax
	testl	%eax, %eax
	jle	.LBB9_443
# BB#260:                               # %.lr.ph.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	xorl	%ebx, %ebx
	xorl	%r12d, %r12d
.LBB9_261:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	56(%r14), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx), %rdi
	testq	%rdi, %rdi
	je	.LBB9_264
# BB#262:                               #   in Loop: Header=BB9_261 Depth=2
.Ltmp469:
	callq	_ZN14COutFileStream5CloseEv
.Ltmp470:
# BB#263:                               # %.noexc254.i.i
                                        #   in Loop: Header=BB9_261 Depth=2
	testl	%eax, %eax
	cmovnel	%eax, %r12d
	movl	52(%r14), %eax
.LBB9_264:                              #   in Loop: Header=BB9_261 Depth=2
	incq	%rbx
	movslq	%eax, %rcx
	cmpq	%rcx, %rbx
	jl	.LBB9_261
	jmp	.LBB9_443
	.p2align	4, 0x90
.LBB9_265:                              # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_298 Depth 2
                                        #     Child Loop BB9_303 Depth 2
                                        #     Child Loop BB9_331 Depth 2
                                        #     Child Loop BB9_399 Depth 2
                                        #     Child Loop BB9_423 Depth 2
                                        #     Child Loop BB9_433 Depth 2
                                        #     Child Loop BB9_347 Depth 2
                                        #       Child Loop BB9_351 Depth 3
                                        #       Child Loop BB9_367 Depth 3
                                        #       Child Loop BB9_379 Depth 3
                                        #       Child Loop BB9_383 Depth 3
                                        #     Child Loop BB9_206 Depth 2
                                        #     Child Loop BB9_416 Depth 2
                                        #     Child Loop BB9_223 Depth 2
                                        #     Child Loop BB9_228 Depth 2
                                        #     Child Loop BB9_261 Depth 2
	movq	56(%rbx), %rax
	movq	(%rax,%r13,8), %rbp
	cmpb	$0, 248(%rbx)
	movq	(%r14), %rax
	movq	128(%rax), %rbx
	je	.LBB9_269
# BB#266:                               #   in Loop: Header=BB9_265 Depth=1
	xorl	%edx, %edx
	testq	%rcx, %rcx
	setne	%dl
.Ltmp256:
	movl	$.L.str.12, %esi
	movq	%r14, %rdi
	callq	*%rbx
	movl	%eax, %r12d
.Ltmp257:
# BB#267:                               # %.noexc458
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	jne	.LBB9_449
# BB#268:                               # %._crit_edge.i
                                        #   in Loop: Header=BB9_265 Depth=1
	leaq	16(%rbp), %rax
	movq	%rax, 112(%rsp)         # 8-byte Spill
	jmp	.LBB9_277
.LBB9_269:                              #   in Loop: Header=BB9_265 Depth=1
	leaq	16(%rbp), %rsi
.Ltmp258:
	leaq	608(%rsp), %rdi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp259:
# BB#270:                               # %.noexc459
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	608(%rsp), %rsi
	testq	%r13, %r13
	je	.LBB9_272
# BB#271:                               #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
	jmp	.LBB9_273
.LBB9_272:                              #   in Loop: Header=BB9_265 Depth=1
	cmpq	$0, 96(%rsp)            # 8-byte Folded Reload
	setne	%cl
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpb	$0, 72(%rax)
	setne	%al
	andb	%cl, %al
.LBB9_273:                              #   in Loop: Header=BB9_265 Depth=1
.Ltmp260:
	movzbl	%al, %edx
	movq	%r14, %rdi
	callq	*%rbx
	movl	%eax, %r12d
.Ltmp261:
# BB#274:                               #   in Loop: Header=BB9_265 Depth=1
	movq	608(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_276
# BB#275:                               #   in Loop: Header=BB9_265 Depth=1
	callq	_ZdaPv
.LBB9_276:                              # %_ZN11CStringBaseIwED2Ev.exit.i452
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	jne	.LBB9_449
.LBB9_277:                              #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	96(%rsp), %rbx          # 8-byte Reload
	testq	%rbx, %rbx
	movq	8(%rsp), %rcx           # 8-byte Reload
	movb	224(%rcx), %r14b
	movb	225(%rcx), %r15b
	movb	248(%rcx), %al
	movb	%al, 40(%rsp)           # 1-byte Spill
	movq	360(%rsp), %rax         # 8-byte Reload
	movb	(%rax), %bpl
	movq	$0, 120(%rsp)
	je	.LBB9_283
# BB#278:                               #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbx), %rax
.Ltmp263:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp264:
# BB#279:                               # %_ZN9CMyComPtrI10IInArchiveEC2EPS0_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbx), %rax
.Ltmp265:
	movl	$IID_IOutArchive, %esi
	movq	%rbx, %rdi
	leaq	120(%rsp), %rdx
	callq	*(%rax)
.Ltmp266:
# BB#280:                               # %_ZNK9CMyComPtrI10IInArchiveE14QueryInterfaceI11IOutArchiveEEiRK4GUIDPPT_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%eax, %eax
	jne	.LBB9_572
# BB#281:                               #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbx), %rax
.Ltmp273:
	movq	%rbx, %rdi
	callq	*16(%rax)
.Ltmp274:
# BB#282:                               # %_ZN9CMyComPtrI10IInArchiveED2Ev.exitthread-pre-split.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	120(%rsp), %rbx
	testq	%rbx, %rbx
	jne	.LBB9_289
	jmp	.LBB9_571
.LBB9_283:                              #   in Loop: Header=BB9_265 Depth=1
	movq	216(%rsp), %rax         # 8-byte Reload
	movq	32(%rax), %rax
	movslq	(%rcx), %rcx
	movq	(%rax,%rcx,8), %rax
.Ltmp276:
	callq	*16(%rax)
	movq	%rax, %rbx
.Ltmp277:
# BB#284:                               # %.noexc242.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_286
# BB#285:                               #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbx), %rax
.Ltmp278:
	movq	%rbx, %rdi
	callq	*8(%rax)
.Ltmp279:
.LBB9_286:                              # %.noexc243.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_288
# BB#287:                               #   in Loop: Header=BB9_265 Depth=1
	movq	(%rdi), %rax
.Ltmp280:
	callq	*16(%rax)
.Ltmp281:
.LBB9_288:                              #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, 120(%rsp)
	testq	%rbx, %rbx
	je	.LBB9_571
.LBB9_289:                              #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbx), %rax
.Ltmp283:
	movq	%rbx, %rdi
	leaq	400(%rsp), %rsi
	callq	*48(%rax)
	movl	%eax, %r12d
.Ltmp284:
# BB#290:                               #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	jne	.LBB9_446
# BB#291:                               #   in Loop: Header=BB9_265 Depth=1
	movl	400(%rsp), %edx
	movl	$-2147467259, %r12d     # imm = 0x80004005
	cmpl	$2, %edx
	ja	.LBB9_446
# BB#292:                               #   in Loop: Header=BB9_265 Depth=1
	leaq	472(%rsp), %rax
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rax)
	movq	$16, 488(%rsp)
	movq	$_ZTV13CRecordVectorI12CUpdatePair2E+16, 464(%rsp)
	leaq	152(%rsp), %rax
	movdqu	%xmm0, (%rax)
	movq	$12, 168(%rsp)
	movq	$_ZTV13CRecordVectorI11CUpdatePairE+16, 144(%rsp)
.Ltmp286:
	leaq	704(%rsp), %rdi
	leaq	328(%rsp), %rsi
	leaq	144(%rsp), %rbx
	movq	%rbx, %rcx
	callq	_Z21GetUpdatePairInfoListRK9CDirItemsRK13CObjectVectorI8CArcItemEN13NFileTimeType5EEnumER13CRecordVectorI11CUpdatePairE
.Ltmp287:
# BB#293:                               #   in Loop: Header=BB9_265 Depth=1
	movq	32(%rsp), %rax          # 8-byte Reload
	leaq	136(%rax), %rsi
.Ltmp288:
	xorl	%ecx, %ecx
	movq	%rbx, %rdi
	leaq	464(%rsp), %rdx
	callq	_Z13UpdateProduceRK13CRecordVectorI11CUpdatePairERKN14NUpdateArchive10CActionSetERS_I12CUpdatePair2EP22IUpdateProduceCallback
.Ltmp289:
# BB#294:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp293:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp294:
# BB#295:                               #   in Loop: Header=BB9_265 Depth=1
	movslq	476(%rsp), %rax
	testq	%rax, %rax
	movb	%bpl, 88(%rsp)          # 1-byte Spill
	jle	.LBB9_299
# BB#296:                               # %.lr.ph.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	480(%rsp), %r8
	leaq	-1(%rax), %rdi
	movq	%rax, %rbx
	xorl	%esi, %esi
	andq	$3, %rbx
	je	.LBB9_300
# BB#297:                               # %.prol.preheader
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%r8, %rdx
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB9_298:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rdx), %ebp
	addl	%ebp, %ecx
	incq	%rsi
	addq	$16, %rdx
	cmpq	%rsi, %rbx
	jne	.LBB9_298
	jmp	.LBB9_301
.LBB9_299:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB9_304
.LBB9_300:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%ecx, %ecx
.LBB9_301:                              # %.prol.loopexit
                                        #   in Loop: Header=BB9_265 Depth=1
	cmpq	$3, %rdi
	jb	.LBB9_304
# BB#302:                               # %.lr.ph.i.i.new
                                        #   in Loop: Header=BB9_265 Depth=1
	subq	%rsi, %rax
	shlq	$4, %rsi
	leaq	48(%r8,%rsi), %rdx
	.p2align	4, 0x90
.LBB9_303:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	-48(%rdx), %esi
	addl	%ecx, %esi
	movzbl	-32(%rdx), %ecx
	addl	%esi, %ecx
	movzbl	-16(%rdx), %esi
	addl	%ecx, %esi
	movzbl	(%rdx), %ecx
	addl	%esi, %ecx
	addq	$64, %rdx
	addq	$-4, %rax
	jne	.LBB9_303
.LBB9_304:                              # %._crit_edge.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	80(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
	movl	%ecx, %esi
.Ltmp296:
	callq	*40(%rax)
	movl	%eax, %r12d
.Ltmp297:
# BB#305:                               #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	jne	.LBB9_445
# BB#306:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp298:
	movl	$160, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp299:
# BB#307:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp301:
	movq	%rbp, %rdi
	callq	_ZN22CArchiveUpdateCallbackC1Ev
.Ltmp302:
# BB#308:                               #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbp), %rax
.Ltmp304:
	movq	%rbp, %rdi
	callq	*8(%rax)
.Ltmp305:
	movq	8(%rsp), %r12           # 8-byte Reload
# BB#309:                               # %_ZN9CMyComPtrI22IArchiveUpdateCallbackEC2EPS0_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movb	40(%rsp), %cl           # 1-byte Reload
	testb	%cl, %cl
	movb	%r14b, 112(%rbp)
	movb	%r15b, 113(%rbp)
	movq	80(%rsp), %rax          # 8-byte Reload
	movq	%rax, 104(%rbp)
	leaq	704(%rsp), %rax
	movq	%rax, 120(%rbp)
	leaq	328(%rsp), %rax
	movq	%rax, 128(%rbp)
	leaq	464(%rsp), %rax
	movq	%rax, 136(%rbp)
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	%rbp, 240(%rsp)         # 8-byte Spill
	jne	.LBB9_323
# BB#310:                               #   in Loop: Header=BB9_265 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 288(%rsp)
.Ltmp307:
	movl	$16, %edi
	callq	_Znam
.Ltmp308:
# BB#311:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rax, 288(%rsp)
	movl	$0, (%rax)
	movl	$4, 300(%rsp)
.Ltmp310:
	leaq	640(%rsp), %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp311:
# BB#312:                               #   in Loop: Header=BB9_265 Depth=1
	movq	640(%rsp), %rdi
.Ltmp312:
	leaq	288(%rsp), %rsi
	leaq	144(%rsp), %rdx
	callq	_ZN8NWindows5NFile10NDirectory17MyGetFullPathNameEPKwR11CStringBaseIwERi
	movl	%eax, %ebx
.Ltmp313:
# BB#313:                               #   in Loop: Header=BB9_265 Depth=1
	movq	640(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_315
# BB#314:                               #   in Loop: Header=BB9_265 Depth=1
	callq	_ZdaPv
.LBB9_315:                              # %_ZN11CStringBaseIwED2Ev.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testb	%bl, %bl
	je	.LBB9_575
# BB#316:                               #   in Loop: Header=BB9_265 Depth=1
	movl	144(%rsp), %ecx
.Ltmp318:
	xorl	%edx, %edx
	leaq	624(%rsp), %rdi
	leaq	288(%rsp), %rsi
	callq	_ZNK11CStringBaseIwE3MidEii
.Ltmp319:
# BB#317:                               # %_ZNK11CStringBaseIwE4LeftEi.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	624(%rsp), %rdi
.Ltmp321:
	callq	_ZN8NWindows5NFile10NDirectory22CreateComplexDirectoryEPKw
.Ltmp322:
# BB#318:                               #   in Loop: Header=BB9_265 Depth=1
	movq	624(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_320
# BB#319:                               #   in Loop: Header=BB9_265 Depth=1
	callq	_ZdaPv
.LBB9_320:                              # %_ZN11CStringBaseIwED2Ev.exit257.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_322
# BB#321:                               #   in Loop: Header=BB9_265 Depth=1
	callq	_ZdaPv
.LBB9_322:                              # %_ZN11CStringBaseIwED2Ev.exit258.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movb	40(%rsp), %cl           # 1-byte Reload
.LBB9_323:                              #   in Loop: Header=BB9_265 Depth=1
	cmpl	$0, 300(%r12)
	je	.LBB9_338
# BB#324:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$-2147467259, %r12d     # imm = 0x80004005
	testb	%cl, %cl
	jne	.LBB9_444
# BB#325:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp324:
	movl	$128, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp325:
# BB#326:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 8(%rbp)
	movq	$_ZTV18COutMultiVolStream+16, (%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 48(%rbp)
	movq	$8, 64(%rbp)
	movq	$_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE+16, 40(%rbp)
	leaq	40(%rbp), %rbx
	movq	%rbp, %r14
	addq	$72, %r14
	movdqu	%xmm0, 80(%rbp)
	movq	$8, 96(%rbp)
	movq	$_ZTV13CRecordVectorIyE+16, 72(%rbp)
	movdqu	%xmm0, 104(%rbp)
.Ltmp326:
	movl	$16, %edi
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	callq	_Znam
.Ltmp327:
# BB#327:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit328.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rax, 104(%rbp)
	movl	$0, (%rax)
	movl	$4, 116(%rbp)
	movl	$1, 8(%rbp)
.Ltmp340:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp341:
# BB#328:                               # %.noexc329.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	300(%rax), %r15d
	movl	84(%rbp), %esi
	addl	%r15d, %esi
.Ltmp342:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector7ReserveEi
.Ltmp343:
# BB#329:                               # %.noexc330.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r15d, %r15d
	movq	8(%rsp), %r12           # 8-byte Reload
	jle	.LBB9_333
# BB#330:                               # %.lr.ph.i.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	xorl	%ebp, %ebp
.LBB9_331:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	304(%r12), %rax
	movq	(%rax,%rbp,8), %rbx
.Ltmp344:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp345:
# BB#332:                               # %.noexc331.i.i
                                        #   in Loop: Header=BB9_331 Depth=2
	movq	40(%rsp), %rdx          # 8-byte Reload
	movq	88(%rdx), %rax
	movslq	84(%rdx), %rcx
	movq	%rbx, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 84(%rdx)
	incq	%rbp
	cmpq	%rbp, %r15
	jne	.LBB9_331
.LBB9_333:                              # %_ZN13CRecordVectorIyEaSERKS0_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
.Ltmp347:
	leaq	144(%rsp), %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp348:
# BB#334:                               # %.preheader.preheader.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
.Ltmp350:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp351:
# BB#335:                               # %.noexc339.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	$46, (%r15)
	movslq	152(%rsp), %r14
	leaq	1(%r14), %rbp
	testl	%ebp, %ebp
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	je	.LBB9_397
# BB#336:                               # %._crit_edge16.i.i.i340.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp353:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbx
.Ltmp354:
# BB#337:                               # %.noexc345.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	$0, (%rbx)
	movl	%ebp, %r12d
	movq	%rbx, %rbp
	jmp	.LBB9_398
.LBB9_338:                              #   in Loop: Header=BB9_265 Depth=1
	testb	%cl, %cl
	movq	16(%rsp), %r12          # 8-byte Reload
	je	.LBB9_342
# BB#339:                               #   in Loop: Header=BB9_265 Depth=1
.Ltmp405:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp406:
# BB#340:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 8(%rbp)
	movq	$_ZTV17CStdOutFileStream+16, (%rbp)
.Ltmp407:
	movq	%rbp, %rdi
	callq	*_ZTV17CStdOutFileStream+24(%rip)
.Ltmp408:
# BB#341:                               #   in Loop: Header=BB9_265 Depth=1
	xorl	%r15d, %r15d
	xorl	%r14d, %r14d
	jmp	.LBB9_439
.LBB9_342:                              #   in Loop: Header=BB9_265 Depth=1
.Ltmp362:
	movl	$1112, %edi             # imm = 0x458
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp363:
# BB#343:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 8(%rbp)
	movq	$_ZTV14COutFileStream+16, (%rbp)
	movq	$_ZTVN8NWindows5NFile3NIO9CFileBaseE+16, 16(%rbp)
	movl	$-1, 24(%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 32(%rbp)
.Ltmp364:
	movl	$4, %edi
	callq	_Znam
.Ltmp365:
# BB#344:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rax, 32(%rbp)
	movb	$0, (%rax)
	movl	$4, 44(%rbp)
	movq	$_ZTVN8NWindows5NFile3NIO8COutFileE+16, 16(%rbp)
.Ltmp367:
	movq	%rbp, %rdi
	callq	*_ZTV14COutFileStream+24(%rip)
.Ltmp368:
# BB#345:                               # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit267.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	pxor	%xmm0, %xmm0
	movdqa	%xmm0, 64(%rsp)
.Ltmp369:
	movl	$16, %edi
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	callq	_Znam
.Ltmp370:
# BB#346:                               # %_ZN11CStringBaseIwEC2Ev.exit269.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, %rcx
	addq	$16, %rcx
	movq	%rcx, 104(%rsp)         # 8-byte Spill
	movq	%rax, 64(%rsp)
	movl	$0, (%rax)
	movl	$4, 76(%rsp)
	movb	96(%r14), %cl
	xorl	%r14d, %r14d
.LBB9_347:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_351 Depth 3
                                        #       Child Loop BB9_367 Depth 3
                                        #       Child Loop BB9_379 Depth 3
                                        #       Child Loop BB9_383 Depth 3
	testb	%cl, %cl
	je	.LBB9_357
# BB#348:                               #   in Loop: Header=BB9_347 Depth=2
	testl	%r14d, %r14d
	leaq	144(%rsp), %rsi
	jle	.LBB9_369
# BB#349:                               #   in Loop: Header=BB9_347 Depth=2
.Ltmp372:
	movl	%r14d, %edi
	callq	_Z21ConvertUInt32ToStringjPw
.Ltmp373:
# BB#350:                               #   in Loop: Header=BB9_347 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	$0, 128(%rcx)
	movq	120(%rcx), %rbx
	movl	$0, (%rbx)
	xorl	%ebp, %ebp
	leaq	144(%rsp), %rax
	.p2align	4, 0x90
.LBB9_351:                              #   Parent Loop BB9_265 Depth=1
                                        #     Parent Loop BB9_347 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incl	%ebp
	cmpl	$0, (%rax)
	leaq	4(%rax), %rax
	jne	.LBB9_351
# BB#352:                               # %_Z11MyStringLenIwEiPKT_.exit.i.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movl	132(%rcx), %r12d
	cmpl	%ebp, %r12d
	je	.LBB9_366
# BB#353:                               #   in Loop: Header=BB9_347 Depth=2
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp374:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r15
.Ltmp375:
# BB#354:                               # %.noexc271.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	testq	%rbx, %rbx
	je	.LBB9_364
# BB#355:                               # %.noexc271.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	testl	%r12d, %r12d
	movl	$0, %eax
	jle	.LBB9_365
# BB#356:                               # %._crit_edge.thread.i.i.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	32(%rsp), %rax          # 8-byte Reload
	movslq	128(%rax), %rax
	jmp	.LBB9_365
.LBB9_357:                              #   in Loop: Header=BB9_347 Depth=2
.Ltmp383:
	leaq	144(%rsp), %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	_ZNK12CArchivePath12GetFinalPathEv
.Ltmp384:
# BB#358:                               #   in Loop: Header=BB9_347 Depth=2
	movl	$0, 72(%rsp)
	movq	64(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	152(%rsp), %rbp
	incq	%rbp
	movl	76(%rsp), %r15d
	cmpl	%r15d, %ebp
	jne	.LBB9_360
# BB#359:                               #   in Loop: Header=BB9_347 Depth=2
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB9_382
.LBB9_360:                              #   in Loop: Header=BB9_347 Depth=2
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp386:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
.Ltmp387:
# BB#361:                               # %.noexc291.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	testq	%rbx, %rbx
	je	.LBB9_380
# BB#362:                               # %.noexc291.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	testl	%r15d, %r15d
	movl	$0, %eax
	movq	32(%rsp), %r15          # 8-byte Reload
	jle	.LBB9_381
# BB#363:                               # %._crit_edge.thread.i.i286.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	72(%rsp), %rax
	jmp	.LBB9_381
.LBB9_364:                              #   in Loop: Header=BB9_347 Depth=2
	xorl	%eax, %eax
.LBB9_365:                              # %._crit_edge16.i.i.i.i453
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	32(%rsp), %rcx          # 8-byte Reload
	movq	%r15, 120(%rcx)
	movl	$0, (%r15,%rax,4)
	movl	%ebp, 132(%rcx)
	movq	%r15, %rbx
.LBB9_366:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i456.preheader
                                        #   in Loop: Header=BB9_347 Depth=2
	leaq	144(%rsp), %rsi
	decl	%ebp
	movq	%rsi, %rax
	.p2align	4, 0x90
.LBB9_367:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i456
                                        #   Parent Loop BB9_265 Depth=1
                                        #     Parent Loop BB9_347 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_367
# BB#368:                               #   in Loop: Header=BB9_347 Depth=2
	movq	32(%rsp), %rax          # 8-byte Reload
	movl	%ebp, 128(%rax)
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB9_369:                              #   in Loop: Header=BB9_347 Depth=2
.Ltmp377:
	movq	%rsi, %rdi
	movq	112(%rsp), %rsi         # 8-byte Reload
	callq	_ZNK12CArchivePath11GetTempPathEv
.Ltmp378:
# BB#370:                               #   in Loop: Header=BB9_347 Depth=2
	movl	$0, 72(%rsp)
	movq	64(%rsp), %rbx
	movl	$0, (%rbx)
	movslq	152(%rsp), %r12
	incq	%r12
	movl	76(%rsp), %r15d
	cmpl	%r15d, %r12d
	jne	.LBB9_372
# BB#371:                               #   in Loop: Header=BB9_347 Depth=2
	movq	32(%rsp), %r15          # 8-byte Reload
	jmp	.LBB9_378
.LBB9_372:                              #   in Loop: Header=BB9_347 Depth=2
	movq	%r12, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp380:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rbp
.Ltmp381:
# BB#373:                               # %.noexc280.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	testq	%rbx, %rbx
	je	.LBB9_376
# BB#374:                               # %.noexc280.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	testl	%r15d, %r15d
	movl	$0, %eax
	movq	32(%rsp), %r15          # 8-byte Reload
	jle	.LBB9_377
# BB#375:                               # %._crit_edge.thread.i.i275.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	72(%rsp), %rax
	jmp	.LBB9_377
.LBB9_376:                              #   in Loop: Header=BB9_347 Depth=2
	xorl	%eax, %eax
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB9_377:                              # %._crit_edge16.i.i276.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	%rbp, 64(%rsp)
	movl	$0, (%rbp,%rax,4)
	movl	%r12d, 76(%rsp)
	movq	%rbp, %rbx
	movq	40(%rsp), %rbp          # 8-byte Reload
.LBB9_378:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i277.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	144(%rsp), %rdi
	xorl	%eax, %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_379:                              #   Parent Loop BB9_265 Depth=1
                                        #     Parent Loop BB9_347 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_379
	jmp	.LBB9_384
.LBB9_380:                              #   in Loop: Header=BB9_347 Depth=2
	xorl	%eax, %eax
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB9_381:                              # %._crit_edge16.i.i287.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	%r12, 64(%rsp)
	movl	$0, (%r12,%rax,4)
	movl	%ebp, 76(%rsp)
	movq	%r12, %rbx
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB9_382:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i288.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	movq	144(%rsp), %rdi
	xorl	%eax, %eax
	movq	40(%rsp), %rbp          # 8-byte Reload
	.p2align	4, 0x90
.LBB9_383:                              #   Parent Loop BB9_265 Depth=1
                                        #     Parent Loop BB9_347 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movl	(%rdi,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_383
.LBB9_384:                              #   in Loop: Header=BB9_347 Depth=2
	movl	152(%rsp), %eax
	movl	%eax, 72(%rsp)
	testq	%rdi, %rdi
	je	.LBB9_386
# BB#385:                               #   in Loop: Header=BB9_347 Depth=2
	callq	_ZdaPv
.LBB9_386:                              #   in Loop: Header=BB9_347 Depth=2
	movq	64(%rsp), %rsi
	movq	$0, 1104(%rbp)
.Ltmp389:
	xorl	%edx, %edx
	movq	104(%rsp), %rdi         # 8-byte Reload
	callq	_ZN8NWindows5NFile3NIO8COutFile6CreateEPKwb
.Ltmp390:
# BB#387:                               # %_ZN14COutFileStream6CreateEPKwb.exit.i.i
                                        #   in Loop: Header=BB9_347 Depth=2
	testb	%al, %al
	jne	.LBB9_406
# BB#388:                               #   in Loop: Header=BB9_347 Depth=2
	callq	__errno_location
	movl	(%rax), %eax
	incl	%r14d
	cmpl	$65535, %r14d           # imm = 0xFFFF
	jg	.LBB9_391
# BB#389:                               #   in Loop: Header=BB9_347 Depth=2
	cmpl	$17, %eax
	jne	.LBB9_391
# BB#390:                               #   in Loop: Header=BB9_347 Depth=2
	movb	96(%r15), %cl
	testb	%cl, %cl
	jne	.LBB9_347
.LBB9_391:                              # %.critedge.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	leaq	64(%rsp), %rcx
	cmpq	224(%rsp), %rcx         # 8-byte Folded Reload
	movl	%eax, (%r12)
	je	.LBB9_208
# BB#392:                               #   in Loop: Header=BB9_265 Depth=1
	movl	$0, 16(%r12)
	movq	8(%r12), %rbx
	movl	$0, (%rbx)
	movslq	72(%rsp), %rbp
	incq	%rbp
	movl	20(%r12), %r15d
	cmpl	%r15d, %ebp
	je	.LBB9_205
# BB#393:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rbp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp392:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp393:
# BB#394:                               # %.noexc306.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_203
# BB#395:                               # %.noexc306.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r15d, %r15d
	movl	$0, %eax
	jle	.LBB9_204
# BB#396:                               # %._crit_edge.thread.i.i301.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	16(%r12), %rax
	jmp	.LBB9_204
.LBB9_397:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%r12d, %r12d
	xorl	%ebx, %ebx
	xorl	%ebp, %ebp
.LBB9_398:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i341.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	144(%rsp), %rax
	xorl	%ecx, %ecx
.LBB9_399:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rax,%rcx), %edx
	movl	%edx, (%rbp,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_399
# BB#400:                               # %_ZN11CStringBaseIwEC2ERKS0_.exit.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	%r12d, %eax
	subl	%r14d, %eax
	cmpl	$1, %eax
	jle	.LBB9_402
# BB#401:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rsi
	jmp	.LBB9_422
.LBB9_402:                              #   in Loop: Header=BB9_265 Depth=1
	leal	-1(%rax), %ecx
	cmpl	$8, %r12d
	movl	$4, %edx
	movl	$16, %esi
	cmovgl	%esi, %edx
	cmpl	$65, %r12d
	jl	.LBB9_404
# BB#403:                               # %select.true.sink
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	%r12d, %edx
	shrl	$31, %edx
	addl	%r12d, %edx
	sarl	%edx
.LBB9_404:                              # %select.end
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	$2, %esi
	subl	%eax, %esi
	addl	%edx, %ecx
	cmovgl	%edx, %esi
	leal	1(%r12,%rsi), %eax
	cmpl	%r12d, %eax
	jne	.LBB9_410
# BB#405:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rsi
	jmp	.LBB9_422
.LBB9_406:                              #   in Loop: Header=BB9_265 Depth=1
.Ltmp397:
	movl	$16, %edi
	callq	_Znwm
	movq	%rax, %rbp
.Ltmp398:
# BB#407:                               # %.noexc296.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, (%rbp)
	movslq	72(%rsp), %r14
	leaq	1(%r14), %rbx
	testl	%ebx, %ebx
	je	.LBB9_414
# BB#408:                               # %._crit_edge16.i.i.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp399:
	movq	%rax, %rdi
	callq	_Znam
.Ltmp400:
# BB#409:                               # %.noexc.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rax, (%rbp)
	movl	$0, (%rax)
	movl	%ebx, 12(%rbp)
	jmp	.LBB9_415
.LBB9_410:                              #   in Loop: Header=BB9_265 Depth=1
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp356:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %rsi
.Ltmp357:
# BB#411:                               # %.noexc372.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	jle	.LBB9_421
# BB#412:                               # %.preheader.i.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r14d, %r14d
	jle	.LBB9_419
# BB#413:                               # %.lr.ph.i.i365.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	leaq	(,%r14,4), %rdx
	movq	%rsi, %rdi
	movq	%rsi, %rbp
	movq	%rbx, %rsi
	callq	memcpy
	movq	%rbp, %rsi
	jmp	.LBB9_420
.LBB9_414:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
.LBB9_415:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	64(%rsp), %rcx
.LBB9_416:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB9_416
# BB#417:                               #   in Loop: Header=BB9_265 Depth=1
	movl	%r14d, 8(%rbp)
.Ltmp402:
	leaq	432(%rsp), %rdi
	callq	_ZN17CBaseRecordVector18ReserveOnePositionEv
.Ltmp403:
# BB#418:                               # %_ZN13CObjectVectorI11CStringBaseIwEE3AddERKS1_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	448(%rsp), %rax
	movslq	444(%rsp), %rcx
	movq	%rbp, (%rax,%rcx,8)
	leal	1(%rcx), %eax
	movl	%eax, 444(%rsp)
	xorl	%r12d, %r12d
	movb	$1, %bl
	movq	40(%rsp), %rbp          # 8-byte Reload
	jmp	.LBB9_216
.LBB9_419:                              # %._crit_edge.i.i.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbp, %rbp
	je	.LBB9_421
.LBB9_420:                              # %._crit_edge.thread.i.i369.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	movq	%rsi, %rbx
	callq	_ZdaPv
	movq	%rbx, %rsi
.LBB9_421:                              # %._crit_edge16.i.i370.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movl	$0, (%rsi,%r14,4)
	movq	%rsi, %rbp
.LBB9_422:                              # %.noexc.i344.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	leaq	(%rbp,%r14,4), %rax
	xorl	%ecx, %ecx
.LBB9_423:                              #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%r15,%rcx), %edx
	movl	%edx, (%rax,%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB9_423
# BB#424:                               #   in Loop: Header=BB9_265 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	$0, 112(%rax)
	movq	104(%rax), %rbx
	movl	$0, (%rbx)
	leal	2(%r14), %ecx
	movl	116(%rax), %r12d
	cmpl	%r12d, %ecx
	jne	.LBB9_426
# BB#425:                               #   in Loop: Header=BB9_265 Depth=1
	movq	32(%rsp), %r12          # 8-byte Reload
	jmp	.LBB9_432
.LBB9_426:                              #   in Loop: Header=BB9_265 Depth=1
	movq	%rsi, 104(%rsp)         # 8-byte Spill
	movl	%ecx, 284(%rsp)         # 4-byte Spill
	movslq	%ecx, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rcx
	cmovoq	%rcx, %rax
.Ltmp359:
	movq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r14
.Ltmp360:
# BB#427:                               # %.noexc357.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testq	%rbx, %rbx
	je	.LBB9_430
# BB#428:                               # %.noexc357.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	movl	$0, %eax
	movq	32(%rsp), %r12          # 8-byte Reload
	jle	.LBB9_431
# BB#429:                               # %._crit_edge.thread.i.i351.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	40(%rsp), %rax          # 8-byte Reload
	movslq	112(%rax), %rax
	jmp	.LBB9_431
.LBB9_430:                              #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
	movq	32(%rsp), %r12          # 8-byte Reload
.LBB9_431:                              # %._crit_edge16.i.i352.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	40(%rsp), %rcx          # 8-byte Reload
	movq	%r14, 104(%rcx)
	movl	$0, (%r14,%rax,4)
	movl	284(%rsp), %eax         # 4-byte Reload
	movl	%eax, 116(%rcx)
	movq	%r14, %rbx
	movq	104(%rsp), %rsi         # 8-byte Reload
.LBB9_432:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i353.i.i.preheader
                                        #   in Loop: Header=BB9_265 Depth=1
	xorl	%eax, %eax
.LBB9_433:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i353.i.i
                                        #   Parent Loop BB9_265 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbp,%rax), %ecx
	movl	%ecx, (%rbx,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB9_433
# BB#434:                               #   in Loop: Header=BB9_265 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%r12d, 112(%rax)
	testq	%rbp, %rbp
	je	.LBB9_436
# BB#435:                               #   in Loop: Header=BB9_265 Depth=1
	movq	%rsi, %rdi
	callq	_ZdaPv
.LBB9_436:                              # %_ZN11CStringBaseIwED2Ev.exit360.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_438
# BB#437:                               #   in Loop: Header=BB9_265 Depth=1
	callq	_ZdaPv
.LBB9_438:                              # %_ZN11CStringBaseIwED2Ev.exit361.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	leaq	432(%rsp), %rax
	movq	40(%rsp), %rbp          # 8-byte Reload
	movq	%rax, 120(%rbp)
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 24(%rbp)
	movdqu	%xmm0, 12(%rbp)
	xorl	%r15d, %r15d
	movq	%rbp, %r14
.LBB9_439:                              # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	xorl	%r12d, %r12d
.LBB9_440:                              # %_ZN9CMyComPtrI20ISequentialOutStreamEaSEPS0_.exit.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	120(%rsp), %rdi
.Ltmp410:
	movq	520(%rsp), %rsi         # 8-byte Reload
	callq	_Z13SetPropertiesP8IUnknownRK13CObjectVectorI9CPropertyE
.Ltmp411:
# BB#441:                               #   in Loop: Header=BB9_265 Depth=1
	testl	%eax, %eax
	je	.LBB9_168
# BB#442:                               #   in Loop: Header=BB9_265 Depth=1
	movl	%eax, %r12d
.LBB9_443:                              # %.loopexit525.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	(%rbp), %rax
.Ltmp474:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp475:
.LBB9_444:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit252.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp479:
	callq	*16(%rax)
.Ltmp480:
.LBB9_445:                              # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit249.i.i
                                        #   in Loop: Header=BB9_265 Depth=1
.Ltmp484:
	leaq	464(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp485:
	.p2align	4, 0x90
.LBB9_446:                              #   in Loop: Header=BB9_265 Depth=1
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	movq	80(%rsp), %r14          # 8-byte Reload
	je	.LBB9_448
# BB#447:                               #   in Loop: Header=BB9_265 Depth=1
	movq	(%rdi), %rax
.Ltmp487:
	callq	*16(%rax)
.Ltmp488:
.LBB9_448:                              # %_ZL8CompressP7CCodecsRKN14NUpdateArchive10CActionSetEP10IInArchiveRK22CCompressionMethodModeR12CArchivePathRK13CObjectVectorI8CArcItemEbbbRK9CDirItemsbRK11CStringBaseIwERK13CRecordVectorIyER10CTempFilesR16CUpdateErrorInfoP17IUpdateCallbackUI.exit.i
                                        #   in Loop: Header=BB9_265 Depth=1
	testl	%r12d, %r12d
	je	.LBB9_165
.LBB9_449:                              # %.thread587
	testl	%r12d, %r12d
	movl	48(%rsp), %r13d         # 4-byte Reload
	cmovnel	%r12d, %r13d
	jmp	.LBB9_561
.LBB9_450:                              # %.loopexit598
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	je	.LBB9_454
# BB#451:
.Ltmp492:
	leaq	528(%rsp), %rdi
	callq	_ZN12CArchiveLink5CloseEv
	movl	%eax, %r13d
.Ltmp493:
# BB#452:
	testl	%r13d, %r13d
	jne	.LBB9_561
# BB#453:
.Ltmp494:
	leaq	528(%rsp), %rdi
	callq	_ZN12CArchiveLink7ReleaseEv
.Ltmp495:
.LBB9_454:
.Ltmp496:
	leaq	432(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp497:
# BB#455:
	cmpb	$0, 56(%rsp)            # 1-byte Folded Reload
	je	.LBB9_560
# BB#456:
	movq	56(%rbx), %rax
	movq	(%rax), %rsi
	addq	$16, %rsi
.Ltmp499:
	leaq	312(%rsp), %r14
	movq	%r14, %rdi
	callq	_ZNK12CArchivePath11GetTempPathEv
.Ltmp500:
# BB#457:
	cmpb	$0, 31(%rsp)            # 1-byte Folded Reload
	je	.LBB9_460
# BB#458:
	movq	128(%rsp), %rdi
.Ltmp502:
	callq	_ZN8NWindows5NFile10NDirectory16DeleteFileAlwaysEPKw
.Ltmp503:
# BB#459:
	testb	%al, %al
	je	.LBB9_497
.LBB9_460:                              # %._crit_edge1099
	movq	312(%rsp), %rdi
	movq	128(%rsp), %rsi
.Ltmp508:
	callq	_ZN8NWindows5NFile10NDirectory10MyMoveFileEPKwS3_
.Ltmp509:
# BB#461:
	movb	$1, %bl
	testb	%al, %al
	movl	48(%rsp), %r13d         # 4-byte Reload
	jne	.LBB9_557
# BB#462:                               # %_Z11MyStringLenIwEiPKT_.exit.i492
	callq	__errno_location
	movl	(%rax), %eax
	movq	16(%rsp), %r12          # 8-byte Reload
	movl	%eax, (%r12)
	movl	$0, 48(%r12)
	movq	40(%r12), %rbp
	movl	$0, (%rbp)
	movl	52(%r12), %ebx
	cmpl	$27, %ebx
	je	.LBB9_519
# BB#463:
.Ltmp510:
	movl	$108, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp511:
# BB#464:                               # %.noexc502
	xorl	%eax, %eax
	testq	%rbp, %rbp
	je	.LBB9_517
# BB#465:                               # %.noexc502
	testl	%ebx, %ebx
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB9_518
# BB#466:                               # %._crit_edge.thread.i.i496
	movq	%rbp, %rdi
	callq	_ZdaPv
	movslq	48(%r12), %rax
	jmp	.LBB9_518
.LBB9_468:
	movq	$_ZTV26CEnumDirItemUpdateCallback+16, 688(%rsp)
	movq	%r15, 696(%rsp)
	movq	(%r15), %rax
.Ltmp201:
	movq	%r15, %rdi
	callq	*96(%rax)
.Ltmp202:
# BB#469:
	testl	%eax, %eax
	je	.LBB9_487
# BB#470:                               # %.thread1106
	movl	%eax, %ebx
	jmp	.LBB9_566
.LBB9_471:
	xorl	%ebp, %ebp
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpl	$0, 280(%rax)
	je	.LBB9_125
# BB#472:
	movl	300(%rax), %eax
	testl	%eax, %eax
	jne	.LBB9_125
# BB#473:                               # %.thread1108
	movq	8(%rsp), %rax           # 8-byte Reload
	leaq	280(%rax), %rdx
	movb	$1, 96(%rbx)
.LBB9_476:
	leaq	272(%rax), %r13
	leaq	104(%rbx), %r14
	cmpq	%r14, %r13
	je	.LBB9_486
# BB#477:
	movl	$0, 112(%rbx)
	movq	104(%rbx), %rbp
	movl	$0, (%rbp)
	movslq	(%rdx), %r15
	incq	%r15
	movl	116(%rbx), %r12d
	cmpl	%r12d, %r15d
	je	.LBB9_483
# BB#478:
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	movl	$4, %ecx
	movq	%r15, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp239:
	callq	_Znam
.Ltmp240:
# BB#479:                               # %.noexc420
	xorl	%ecx, %ecx
	testq	%rbp, %rbp
	je	.LBB9_482
# BB#480:                               # %.noexc420
	testl	%r12d, %r12d
	jle	.LBB9_482
# BB#481:                               # %._crit_edge.thread.i.i414
	movq	%rbp, %rdi
	movq	%rax, %rbp
	callq	_ZdaPv
	movq	%rbp, %rax
	movslq	112(%rbx), %rcx
.LBB9_482:                              # %._crit_edge16.i.i415
	movq	%rax, 104(%rbx)
	movl	$0, (%rax,%rcx,4)
	movl	%r15d, 116(%rbx)
	movq	%rax, %rbp
	movq	56(%rsp), %rdx          # 8-byte Reload
.LBB9_483:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i416
	movq	(%r13), %rax
.LBB9_484:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbp)
	addq	$4, %rbp
	testl	%ecx, %ecx
	jne	.LBB9_484
# BB#485:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i419
	movl	(%rdx), %eax
	movl	%eax, 112(%rbx)
.LBB9_486:                              # %_ZN11CStringBaseIwEaSERKS0_.exit421
	movb	$1, %bpl
.Ltmp241:
	movq	%r14, %rdi
	callq	_ZN8NWindows5NFile5NName22NormalizeDirPathPrefixER11CStringBaseIwE
.Ltmp242:
	jmp	.LBB9_125
.LBB9_487:
	pxor	%xmm0, %xmm0
	movdqu	%xmm0, 376(%rsp)
	movq	$8, 392(%rsp)
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 368(%rsp)
	movdqu	%xmm0, 664(%rsp)
	movq	$4, 680(%rsp)
	movq	$_ZTV13CRecordVectorIjE+16, 656(%rsp)
.Ltmp204:
	leaq	704(%rsp), %rsi
	leaq	688(%rsp), %rdx
	leaq	368(%rsp), %rcx
	leaq	656(%rsp), %r8
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	callq	_Z14EnumerateItemsRKN9NWildcard7CCensorER9CDirItemsP20IEnumDirItemCallbackR13CObjectVectorI11CStringBaseIwEER13CRecordVectorIjE
	movl	%eax, %r12d
.Ltmp205:
# BB#488:                               # %.preheader
	cmpl	$0, 380(%rsp)
	jle	.LBB9_493
# BB#489:                               # %.lr.ph842
	xorl	%ebx, %ebx
.LBB9_490:                              # =>This Inner Loop Header: Depth=1
	movq	(%r15), %rax
	movq	384(%rsp), %rcx
	movq	(%rcx,%rbx,8), %rcx
	movq	(%rcx), %rsi
	movq	672(%rsp), %rcx
	movl	(%rcx,%rbx,4), %edx
.Ltmp207:
	movq	%r15, %rdi
	callq	*112(%rax)
.Ltmp208:
# BB#491:                               #   in Loop: Header=BB9_490 Depth=1
	testl	%eax, %eax
	jne	.LBB9_503
# BB#492:                               #   in Loop: Header=BB9_490 Depth=1
	incq	%rbx
	movslq	380(%rsp), %rax
	cmpq	%rax, %rbx
	jl	.LBB9_490
.LBB9_493:                              # %._crit_edge843
	movl	$1, %ebp
	cmpl	$-2147467260, %r12d     # imm = 0x80004004
	je	.LBB9_504
# BB#494:                               # %._crit_edge843
	testl	%r12d, %r12d
	jne	.LBB9_505
# BB#495:
	movq	(%r15), %rax
.Ltmp210:
	movq	%r15, %rdi
	callq	*120(%rax)
.Ltmp211:
# BB#496:
	xorl	%ebp, %ebp
	testl	%eax, %eax
	setne	%bpl
	movl	%r14d, %ebx
	cmovnel	%eax, %ebx
	jmp	.LBB9_513
.LBB9_497:                              # %_Z11MyStringLenIwEiPKT_.exit.i466
	callq	__errno_location
	movl	(%rax), %eax
	movq	16(%rsp), %r15          # 8-byte Reload
	movl	%eax, (%r15)
	movl	$0, 48(%r15)
	movq	40(%r15), %rbx
	movl	$0, (%rbx)
	movl	52(%r15), %ebp
	cmpl	$29, %ebp
	je	.LBB9_527
# BB#498:
.Ltmp504:
	movl	$116, %edi
	callq	_Znam
	movq	%rax, %r14
.Ltmp505:
# BB#499:                               # %.noexc476
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_525
# BB#500:                               # %.noexc476
	testl	%ebp, %ebp
	movq	16(%rsp), %r15          # 8-byte Reload
	jle	.LBB9_526
# BB#501:                               # %._crit_edge.thread.i.i470
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	48(%r15), %rax
	jmp	.LBB9_526
.LBB9_502:
	movb	$1, %bpl
	jmp	.LBB9_125
.LBB9_503:
	movl	$1, %ebp
	movl	%eax, %ebx
	jmp	.LBB9_513
.LBB9_504:
	movl	$-2147467260, %ebx      # imm = 0x80004004
	jmp	.LBB9_513
.LBB9_505:                              # %_Z11MyStringLenIwEiPKT_.exit.i390
	movq	16(%rsp), %rax          # 8-byte Reload
	movl	$0, 48(%rax)
	movq	40(%rax), %rbx
	movl	$0, (%rbx)
	movl	52(%rax), %r13d
	cmpl	$15, %r13d
	jne	.LBB9_507
# BB#506:
	movq	16(%rsp), %rcx          # 8-byte Reload
	jmp	.LBB9_512
.LBB9_507:
.Ltmp212:
	movl	$60, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp213:
# BB#508:                               # %.noexc400
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_511
# BB#509:                               # %.noexc400
	testl	%r13d, %r13d
	jle	.LBB9_511
# BB#510:                               # %._crit_edge.thread.i.i394
	movq	%rbx, %rdi
	callq	_ZdaPv
	movq	16(%rsp), %rax          # 8-byte Reload
	movslq	48(%rax), %rax
.LBB9_511:                              # %._crit_edge16.i.i395
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	%r15, 40(%rcx)
	movl	$0, (%r15,%rax,4)
	movl	$15, 52(%rcx)
	movq	%r15, %rbx
.LBB9_512:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i398.preheader
	movaps	.LCPI9_28(%rip), %xmm0  # xmm0 = [83,99,97,110]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_29(%rip), %xmm0  # xmm0 = [110,105,110,103]
	movups	%xmm0, 16(%rbx)
	movdqa	.LCPI9_30(%rip), %xmm0  # xmm0 = [32,101,114,114]
	movdqu	%xmm0, 32(%rbx)
	movl	$111, 48(%rbx)
	movl	$114, 52(%rbx)
	movl	$0, 56(%rbx)
	movl	$14, 48(%rcx)
	movl	%r12d, %ebx
.LBB9_513:                              # %.loopexit600
.Ltmp217:
	leaq	656(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp218:
	movq	8(%rsp), %r12           # 8-byte Reload
# BB#514:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 368(%rsp)
.Ltmp228:
	leaq	368(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp229:
# BB#515:
.Ltmp234:
	leaq	368(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp235:
# BB#516:
	testl	%ebp, %ebp
	jne	.LBB9_566
	jmp	.LBB9_123
.LBB9_517:
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB9_518:                              # %._crit_edge16.i.i497
	movq	%r15, 40(%r12)
	movl	$0, (%r15,%rax,4)
	movl	$27, 52(%r12)
	movq	%r15, %rbp
.LBB9_519:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i500.preheader
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [55,45,90,105]
	movups	%xmm0, (%rbp)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [112,32,99,97]
	movups	%xmm0, 16(%rbp)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [110,110,111,116]
	movups	%xmm0, 32(%rbp)
	movaps	.LCPI9_44(%rip), %xmm0  # xmm0 = [32,109,111,118]
	movups	%xmm0, 48(%rbp)
	movaps	.LCPI9_45(%rip), %xmm0  # xmm0 = [101,32,116,104]
	movups	%xmm0, 64(%rbp)
	movdqa	.LCPI9_46(%rip), %xmm0  # xmm0 = [101,32,102,105]
	movdqu	%xmm0, 80(%rbp)
	movl	$108, 96(%rbp)
	movl	$101, 100(%rbp)
	movl	$0, 104(%rbp)
	movl	$26, 48(%r12)
	leaq	8(%r12), %rax
	cmpq	%rax, %r14
	je	.LBB9_539
# BB#520:
	movl	$0, 16(%r12)
	movq	8(%r12), %rbx
	movl	$0, (%rbx)
	movslq	320(%rsp), %rbp
	incq	%rbp
	movl	20(%r12), %r15d
	cmpl	%r15d, %ebp
	je	.LBB9_536
# BB#521:
	movl	$4, %ecx
	movq	%rbp, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp512:
	callq	_Znam
	movq	%rax, %r14
.Ltmp513:
# BB#522:                               # %.noexc513
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_534
# BB#523:                               # %.noexc513
	testl	%r15d, %r15d
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB9_535
# BB#524:                               # %._crit_edge.thread.i.i507
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	16(%r12), %rax
	jmp	.LBB9_535
.LBB9_525:
	movq	16(%rsp), %r15          # 8-byte Reload
.LBB9_526:                              # %._crit_edge16.i.i471
	movq	%r14, 40(%r15)
	movl	$0, (%r14,%rax,4)
	movl	$29, 52(%r15)
	movq	%r14, %rbx
.LBB9_527:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i474.preheader
	movaps	.LCPI9_1(%rip), %xmm0   # xmm0 = [55,45,90,105]
	movups	%xmm0, (%rbx)
	movaps	.LCPI9_2(%rip), %xmm0   # xmm0 = [112,32,99,97]
	movups	%xmm0, 16(%rbx)
	movaps	.LCPI9_3(%rip), %xmm0   # xmm0 = [110,110,111,116]
	movups	%xmm0, 32(%rbx)
	movaps	.LCPI9_41(%rip), %xmm0  # xmm0 = [32,100,101,108]
	movups	%xmm0, 48(%rbx)
	movaps	.LCPI9_42(%rip), %xmm0  # xmm0 = [101,116,101,32]
	movups	%xmm0, 64(%rbx)
	movaps	.LCPI9_43(%rip), %xmm0  # xmm0 = [116,104,101,32]
	movups	%xmm0, 80(%rbx)
	movdqa	.LCPI9_10(%rip), %xmm0  # xmm0 = [102,105,108,101]
	movdqu	%xmm0, 96(%rbx)
	movl	$0, 112(%rbx)
	movl	$28, 48(%r15)
	leaq	8(%r15), %rax
	movl	$-2147467259, %r13d     # imm = 0x80004005
	leaq	128(%rsp), %rcx
	cmpq	%rax, %rcx
	je	.LBB9_556
# BB#528:
	movl	$0, 16(%r15)
	movq	8(%r15), %rbx
	movl	$0, (%rbx)
	movslq	136(%rsp), %r14
	incq	%r14
	movl	20(%r15), %r12d
	cmpl	%r12d, %r14d
	jne	.LBB9_530
# BB#529:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB9_553
.LBB9_530:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp506:
	callq	_Znam
	movq	%rax, %r15
.Ltmp507:
# BB#531:                               # %.noexc487
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_551
# BB#532:                               # %.noexc487
	testl	%r12d, %r12d
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB9_552
# BB#533:                               # %._crit_edge.thread.i.i481
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	16(%r12), %rax
	jmp	.LBB9_552
.LBB9_534:
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB9_535:                              # %._crit_edge16.i.i508
	movq	%r14, 8(%r12)
	movl	$0, (%r14,%rax,4)
	movl	%ebp, 20(%r12)
	movq	%r14, %rbx
.LBB9_536:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i509
	movq	312(%rsp), %rax
.LBB9_537:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_537
# BB#538:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i512
	movl	320(%rsp), %eax
	movl	%eax, 16(%r12)
.LBB9_539:                              # %_ZN11CStringBaseIwEaSERKS0_.exit514
	leaq	24(%r12), %rax
	movl	$-2147467259, %r13d     # imm = 0x80004005
	leaq	128(%rsp), %rcx
	cmpq	%rax, %rcx
	je	.LBB9_556
# BB#540:
	movl	$0, 32(%r12)
	movq	24(%r12), %rbx
	movl	$0, (%rbx)
	movslq	136(%rsp), %r14
	incq	%r14
	movl	36(%r12), %r12d
	cmpl	%r12d, %r14d
	jne	.LBB9_542
# BB#541:
	movq	16(%rsp), %r12          # 8-byte Reload
	jmp	.LBB9_548
.LBB9_542:
	movl	$4, %ecx
	movq	%r14, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp514:
	callq	_Znam
	movq	%rax, %r15
.Ltmp515:
# BB#543:                               # %.noexc524
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB9_546
# BB#544:                               # %.noexc524
	testl	%r12d, %r12d
	movq	16(%rsp), %r12          # 8-byte Reload
	jle	.LBB9_547
# BB#545:                               # %._crit_edge.thread.i.i518
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	32(%r12), %rax
	jmp	.LBB9_547
.LBB9_546:
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB9_547:                              # %._crit_edge16.i.i519
	movq	%r15, 24(%r12)
	movl	$0, (%r15,%rax,4)
	movl	%r14d, 36(%r12)
	movq	%r15, %rbx
.LBB9_548:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i520
	movq	128(%rsp), %rax
.LBB9_549:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_549
# BB#550:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i523
	movl	136(%rsp), %eax
	movl	%eax, 32(%r12)
	jmp	.LBB9_556
.LBB9_551:
	movq	16(%rsp), %r12          # 8-byte Reload
.LBB9_552:                              # %._crit_edge16.i.i482
	movq	%r15, 8(%r12)
	movl	$0, (%r15,%rax,4)
	movl	%r14d, 20(%r12)
	movq	%r15, %rbx
.LBB9_553:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i483
	movq	128(%rsp), %rax
.LBB9_554:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB9_554
# BB#555:                               # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i486
	movl	136(%rsp), %eax
	movl	%eax, 16(%r12)
.LBB9_556:                              # %_ZN11CStringBaseIwEaSERKS0_.exit488
	xorl	%ebx, %ebx
.LBB9_557:                              # %_ZN11CStringBaseIwEaSERKS0_.exit488
	movq	312(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_559
# BB#558:
	callq	_ZdaPv
.LBB9_559:                              # %_ZN11CStringBaseIwED2Ev.exit526
	testb	%bl, %bl
	je	.LBB9_561
.LBB9_560:
	xorl	%r13d, %r13d
.LBB9_561:
	movq	$_ZTV13CObjectVectorI8CArcItemE+16, 328(%rsp)
.Ltmp522:
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp523:
# BB#562:
.Ltmp528:
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp529:
# BB#563:                               # %_ZN13CObjectVectorI8CArcItemED2Ev.exit
	movl	%r13d, 48(%rsp)         # 4-byte Spill
.LBB9_564:                              # %.loopexit599
.Ltmp531:
	leaq	432(%rsp), %rdi
	callq	_ZN10CTempFilesD2Ev
.Ltmp532:
# BB#565:                               # %_ZN11CStringBaseIwED2Ev.exit534
	movl	48(%rsp), %ebx          # 4-byte Reload
.LBB9_566:
.Ltmp534:
	leaq	704(%rsp), %rdi
	callq	_ZN9CDirItemsD2Ev
.Ltmp535:
.LBB9_567:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_569
# BB#568:
	callq	_ZdaPv
.LBB9_569:                              # %_ZN11CStringBaseIwED2Ev.exit449
	leaq	528(%rsp), %rdi
	callq	_ZN12CArchiveLinkD2Ev
.LBB9_570:
	movl	%ebx, %eax
	addq	$856, %rsp              # imm = 0x358
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB9_571:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.16, (%rax)
.Ltmp537:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp538:
	jmp	.LBB9_573
.LBB9_572:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.16, (%rax)
.Ltmp268:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp269:
.LBB9_573:
.LBB9_574:
	movl	$8, %edi
	callq	__cxa_allocate_exception
	movq	$.L.str.4, (%rax)
.Ltmp168:
	movl	$_ZTIPKc, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp169:
	jmp	.LBB9_648
.LBB9_575:
	movl	$4, %edi
	callq	__cxa_allocate_exception
	movl	$1417161, (%rax)        # imm = 0x159FC9
.Ltmp315:
	movl	$_ZTIi, %esi
	xorl	%edx, %edx
	movq	%rax, %rdi
	callq	__cxa_throw
.Ltmp316:
	jmp	.LBB9_573
.LBB9_576:                              # %.loopexit.split-lp79.i
.Ltmp317:
	jmp	.LBB9_636
.LBB9_577:
.Ltmp270:
	jmp	.LBB9_703
.LBB9_578:                              # %.loopexit.split-lp.i
.Ltmp539:
	jmp	.LBB9_710
.LBB9_579:                              # %.thread.i.i
.Ltmp431:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB9_606
.LBB9_580:
.Ltmp401:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB9_660
.LBB9_581:
.Ltmp442:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	264(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_588
# BB#582:
	callq	_ZdaPv
	jmp	.LBB9_588
.LBB9_583:
.Ltmp396:
	jmp	.LBB9_659
.LBB9_584:
.Ltmp358:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB9_602
# BB#585:
	movq	%rbx, %rdi
	jmp	.LBB9_593
.LBB9_586:
.Ltmp455:
	jmp	.LBB9_605
.LBB9_587:                              # %.thread506.i.i
.Ltmp450:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_588:
	movq	(%rbp), %rax
.Ltmp451:
	movq	%rbp, %rdi
	callq	*16(%rax)
.Ltmp452:
	jmp	.LBB9_606
.LBB9_589:
.Ltmp214:
	jmp	.LBB9_666
.LBB9_590:                              # %.thread503.i.i
.Ltmp445:
	jmp	.LBB9_605
.LBB9_591:
.Ltmp361:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testq	%rbp, %rbp
	je	.LBB9_602
# BB#592:
	movq	104(%rsp), %rdi         # 8-byte Reload
.LBB9_593:                              # %_ZN11CStringBaseIwED2Ev.exit363.i.i
	callq	_ZdaPv
	jmp	.LBB9_602
.LBB9_594:
.Ltmp460:
	jmp	.LBB9_678
.LBB9_595:
.Ltmp419:
	jmp	.LBB9_678
.LBB9_596:
.Ltmp416:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB9_679
.LBB9_597:                              # %.loopexit.i.i
.Ltmp471:
	jmp	.LBB9_652
.LBB9_598:
.Ltmp371:
	jmp	.LBB9_678
.LBB9_599:
.Ltmp366:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB9_680
.LBB9_600:                              # %.loopexit.split-lp527.i.i
.Ltmp404:
	jmp	.LBB9_659
.LBB9_601:
.Ltmp355:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_602:                              # %_ZN11CStringBaseIwED2Ev.exit363.i.i
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB9_608
.LBB9_603:                              # %.loopexit.split-lp.i.i
.Ltmp468:
	jmp	.LBB9_652
.LBB9_604:
.Ltmp426:
.LBB9_605:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit441.i.i
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_606:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit441.i.i
	movq	32(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp456:
	callq	*16(%rax)
.Ltmp457:
	jmp	.LBB9_679
.LBB9_607:
.Ltmp352:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_608:
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_679
# BB#609:
	callq	_ZdaPv
	jmp	.LBB9_679
.LBB9_610:
.Ltmp349:
	jmp	.LBB9_678
.LBB9_611:
.Ltmp328:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp329:
	movq	%r14, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp330:
# BB#612:
	movq	$_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE+16, (%rbx)
.Ltmp331:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp332:
# BB#613:
.Ltmp337:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp338:
# BB#614:                               # %.body324.i.i
	movq	40(%rsp), %rdi          # 8-byte Reload
	callq	_ZdlPv
	jmp	.LBB9_680
.LBB9_615:
.Ltmp333:
	movq	%rax, %rbp
.Ltmp334:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp335:
	jmp	.LBB9_618
.LBB9_616:
.Ltmp336:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_617:
.Ltmp339:
	movq	%rax, %rbp
.LBB9_618:                              # %.body.i.i.i
	movq	%rbp, %rdi
	callq	__clang_call_terminate
.LBB9_619:
.Ltmp236:
	jmp	.LBB9_687
.LBB9_620:
.Ltmp230:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp231:
	leaq	368(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp232:
	jmp	.LBB9_728
.LBB9_621:
.Ltmp233:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_622:
.Ltmp219:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_667
.LBB9_623:
.Ltmp206:
	jmp	.LBB9_666
.LBB9_624:
.Ltmp323:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	624(%rsp), %rdi
	testq	%rdi, %rdi
	jne	.LBB9_626
	jmp	.LBB9_637
.LBB9_625:
.Ltmp314:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	640(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_637
.LBB9_626:
	callq	_ZdaPv
	jmp	.LBB9_637
.LBB9_627:
.Ltmp309:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_680
.LBB9_628:
.Ltmp388:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_660
# BB#629:
	callq	_ZdaPv
	jmp	.LBB9_660
.LBB9_630:
.Ltmp501:
	movq	%rax, %rbx
	jmp	.LBB9_647
.LBB9_631:
.Ltmp476:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_680
.LBB9_632:
.Ltmp180:
	jmp	.LBB9_657
.LBB9_633:
.Ltmp382:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	144(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_660
# BB#634:
	callq	_ZdaPv
	jmp	.LBB9_660
.LBB9_635:                              # %.loopexit78.i
.Ltmp320:
.LBB9_636:                              # %_ZN11CStringBaseIwED2Ev.exit255.i.i
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_637:                              # %_ZN11CStringBaseIwED2Ev.exit255.i.i
	movq	288(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_680
# BB#638:
	callq	_ZdaPv
	jmp	.LBB9_680
.LBB9_639:
.Ltmp203:
	jmp	.LBB9_687
.LBB9_640:
.Ltmp385:
	jmp	.LBB9_659
.LBB9_641:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.thread521.i.i
.Ltmp409:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_680
.LBB9_642:
.Ltmp481:
	jmp	.LBB9_672
.LBB9_643:
.Ltmp306:
	jmp	.LBB9_672
.LBB9_644:
.Ltmp303:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	callq	_ZdlPv
	jmp	.LBB9_683
.LBB9_645:
.Ltmp516:
	movq	%rax, %rbx
	movq	312(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_647
# BB#646:
	callq	_ZdaPv
.LBB9_647:
	movq	%rbx, %rdi
	callq	__cxa_begin_catch
.Ltmp517:
	callq	__cxa_rethrow
.Ltmp518:
.LBB9_648:
.LBB9_649:
.Ltmp519:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp520:
	callq	__cxa_end_catch
.Ltmp521:
	jmp	.LBB9_719
.LBB9_650:
.Ltmp185:
	jmp	.LBB9_690
.LBB9_651:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.thread524.loopexit.split-lp.i.i
.Ltmp463:
.LBB9_652:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.thread524.i.i
	movq	%rbp, 40(%rsp)          # 8-byte Spill
	jmp	.LBB9_678
.LBB9_653:
.Ltmp376:
	jmp	.LBB9_659
.LBB9_654:
.Ltmp379:
	jmp	.LBB9_659
.LBB9_655:
.Ltmp243:
	jmp	.LBB9_724
.LBB9_656:
.Ltmp175:
.LBB9_657:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp181:
	leaq	400(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp182:
	jmp	.LBB9_691
.LBB9_658:                              # %.loopexit526.i.i
.Ltmp391:
.LBB9_659:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_660:
	movq	64(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_679
# BB#661:
	callq	_ZdaPv
	jmp	.LBB9_679
.LBB9_662:
.Ltmp197:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB9_706
.LBB9_663:
.Ltmp486:
	jmp	.LBB9_710
.LBB9_664:
.Ltmp295:
	jmp	.LBB9_672
.LBB9_665:
.Ltmp209:
.LBB9_666:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp215:
	leaq	656(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp216:
.LBB9_667:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 368(%rsp)
.Ltmp220:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp221:
# BB#668:
.Ltmp226:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp227:
	jmp	.LBB9_728
.LBB9_669:
.Ltmp222:
	movq	%rax, %rbx
.Ltmp223:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp224:
	jmp	.LBB9_734
.LBB9_670:
.Ltmp225:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_671:
.Ltmp300:
.LBB9_672:                              # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit.i.i
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_683
.LBB9_673:
.Ltmp188:
	jmp	.LBB9_687
.LBB9_674:
.Ltmp530:
	jmp	.LBB9_724
.LBB9_675:
.Ltmp524:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp525:
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp526:
	jmp	.LBB9_727
.LBB9_676:
.Ltmp527:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_677:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.thread524.loopexit.i.i
.Ltmp346:
.LBB9_678:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.thread524.i.i
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_679:                              # %_ZN9CMyComPtrI9IInStreamED2Ev.exit.thread.i.i
	movq	40(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp472:
	callq	*16(%rax)
.Ltmp473:
.LBB9_680:                              # %_ZN9CMyComPtrI20ISequentialOutStreamED2Ev.exit.i.i
	movq	240(%rsp), %rdi         # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp477:
	callq	*16(%rax)
.Ltmp478:
	jmp	.LBB9_683
.LBB9_681:                              # %.loopexit.split-lp
.Ltmp498:
	jmp	.LBB9_718
.LBB9_682:
.Ltmp290:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.Ltmp291:
	leaq	144(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp292:
.LBB9_683:                              # %_ZN9CMyComPtrI22IArchiveUpdateCallbackED2Ev.exit.i.i
.Ltmp482:
	leaq	464(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp483:
	jmp	.LBB9_711
.LBB9_684:
.Ltmp262:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	608(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_719
# BB#685:
	callq	_ZdaPv
	jmp	.LBB9_719
.LBB9_686:
.Ltmp533:
.LBB9_687:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_728
.LBB9_688:
.Ltmp536:
	jmp	.LBB9_694
.LBB9_689:
.Ltmp170:
.LBB9_690:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_691:
	movq	504(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_729
# BB#692:
	callq	_ZdaPv
	jmp	.LBB9_729
.LBB9_693:
.Ltmp165:
.LBB9_694:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_729
.LBB9_695:
.Ltmp156:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	cmpq	$0, 48(%rsp)            # 8-byte Folded Reload
	je	.LBB9_715
# BB#696:
	movq	48(%rsp), %rdi          # 8-byte Reload
	callq	_ZdaPv
	jmp	.LBB9_715
.LBB9_697:
.Ltmp145:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%rbp, %rdi
	callq	_ZdaPv
	jmp	.LBB9_699
.LBB9_698:
.Ltmp142:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_699:                              # %.body347
	movq	%rbx, %rdi
	callq	_ZdlPv
	jmp	.LBB9_715
.LBB9_700:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp133:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r14, %rdi
	jmp	.LBB9_716
.LBB9_701:
.Ltmp275:
	jmp	.LBB9_710
.LBB9_702:
.Ltmp267:
.LBB9_703:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	96(%rsp), %rdi          # 8-byte Reload
	movq	(%rdi), %rax
.Ltmp271:
	callq	*16(%rax)
.Ltmp272:
	jmp	.LBB9_711
.LBB9_704:
.Ltmp162:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_731
.LBB9_705:
.Ltmp200:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_706:                              # %.body382
	movq	176(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_728
# BB#707:
	callq	_ZdaPv
	jmp	.LBB9_728
.LBB9_708:
.Ltmp285:
	jmp	.LBB9_710
.LBB9_709:                              # %.loopexit.i
.Ltmp282:
.LBB9_710:                              # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit241.i.i
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_711:                              # %_ZN9CMyComPtrI10IInArchiveED2Ev.exit241.i.i
	movq	120(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_719
# BB#712:
	movq	(%rdi), %rax
.Ltmp540:
	callq	*16(%rax)
.Ltmp541:
	jmp	.LBB9_719
.LBB9_713:
.Ltmp542:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_714:
.Ltmp159:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	%r12, %r15
	movq	%r14, %r13
.LBB9_715:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%r15, %rdi
	callq	_ZdaPv
	movq	%r13, %rdi
.LBB9_716:                              # %unwind_resume
	callq	_ZdaPv
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_Unwind_Resume
.LBB9_717:                              # %.loopexit
.Ltmp491:
.LBB9_718:
	movq	%rax, 8(%rsp)           # 8-byte Spill
.LBB9_719:
	movq	$_ZTV13CObjectVectorI8CArcItemE+16, 328(%rsp)
.Ltmp543:
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp544:
# BB#720:
.Ltmp549:
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp550:
	jmp	.LBB9_727
.LBB9_721:
.Ltmp545:
	movq	%rax, %rbx
.Ltmp546:
	leaq	328(%rsp), %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp547:
	jmp	.LBB9_734
.LBB9_722:
.Ltmp548:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB9_723:
.Ltmp246:
.LBB9_724:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	jmp	.LBB9_727
.LBB9_725:
.Ltmp253:
	movq	%rax, 8(%rsp)           # 8-byte Spill
	movq	248(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_727
# BB#726:
	callq	_ZdaPv
.LBB9_727:
.Ltmp551:
	leaq	432(%rsp), %rdi
	callq	_ZN10CTempFilesD2Ev
.Ltmp552:
.LBB9_728:
.Ltmp553:
	leaq	704(%rsp), %rdi
	callq	_ZN9CDirItemsD2Ev
.Ltmp554:
.LBB9_729:
	movq	128(%rsp), %rdi
	testq	%rdi, %rdi
	je	.LBB9_731
# BB#730:
	callq	_ZdaPv
.LBB9_731:
.Ltmp555:
	leaq	528(%rsp), %rdi
	callq	_ZN12CArchiveLinkD2Ev
.Ltmp556:
# BB#732:                               # %unwind_resume
	movq	8(%rsp), %rdi           # 8-byte Reload
	callq	_Unwind_Resume
.LBB9_733:
.Ltmp557:
	movq	%rax, %rbx
.LBB9_734:                              # %.body406
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end9:
	.size	_Z13UpdateArchiveP7CCodecsRKN9NWildcard7CCensorER14CUpdateOptionsR16CUpdateErrorInfoP15IOpenCallbackUIP18IUpdateCallbackUI2, .Lfunc_end9-_Z13UpdateArchiveP7CCodecsRKN9NWildcard7CCensorER14CUpdateOptionsR16CUpdateErrorInfoP15IOpenCallbackUIP18IUpdateCallbackUI2
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table9:
.Lexception5:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.ascii	"\344\r"                # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\333\r"                # Call site table length
	.long	.Lfunc_begin5-.Lfunc_begin5 # >> Call Site 1 <<
	.long	.Ltmp131-.Lfunc_begin5  #   Call between .Lfunc_begin5 and .Ltmp131
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp131-.Lfunc_begin5  # >> Call Site 2 <<
	.long	.Ltmp132-.Ltmp131       #   Call between .Ltmp131 and .Ltmp132
	.long	.Ltmp133-.Lfunc_begin5  #     jumps to .Ltmp133
	.byte	0                       #   On action: cleanup
	.long	.Ltmp134-.Lfunc_begin5  # >> Call Site 3 <<
	.long	.Ltmp139-.Ltmp134       #   Call between .Ltmp134 and .Ltmp139
	.long	.Ltmp159-.Lfunc_begin5  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp140-.Lfunc_begin5  # >> Call Site 4 <<
	.long	.Ltmp141-.Ltmp140       #   Call between .Ltmp140 and .Ltmp141
	.long	.Ltmp142-.Lfunc_begin5  #     jumps to .Ltmp142
	.byte	0                       #   On action: cleanup
	.long	.Ltmp143-.Lfunc_begin5  # >> Call Site 5 <<
	.long	.Ltmp144-.Ltmp143       #   Call between .Ltmp143 and .Ltmp144
	.long	.Ltmp145-.Lfunc_begin5  #     jumps to .Ltmp145
	.byte	0                       #   On action: cleanup
	.long	.Ltmp146-.Lfunc_begin5  # >> Call Site 6 <<
	.long	.Ltmp149-.Ltmp146       #   Call between .Ltmp146 and .Ltmp149
	.long	.Ltmp159-.Lfunc_begin5  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp150-.Lfunc_begin5  # >> Call Site 7 <<
	.long	.Ltmp155-.Ltmp150       #   Call between .Ltmp150 and .Ltmp155
	.long	.Ltmp156-.Lfunc_begin5  #     jumps to .Ltmp156
	.byte	0                       #   On action: cleanup
	.long	.Ltmp157-.Lfunc_begin5  # >> Call Site 8 <<
	.long	.Ltmp158-.Ltmp157       #   Call between .Ltmp157 and .Ltmp158
	.long	.Ltmp159-.Lfunc_begin5  #     jumps to .Ltmp159
	.byte	0                       #   On action: cleanup
	.long	.Ltmp160-.Lfunc_begin5  # >> Call Site 9 <<
	.long	.Ltmp161-.Ltmp160       #   Call between .Ltmp160 and .Ltmp161
	.long	.Ltmp162-.Lfunc_begin5  #     jumps to .Ltmp162
	.byte	0                       #   On action: cleanup
	.long	.Ltmp163-.Lfunc_begin5  # >> Call Site 10 <<
	.long	.Ltmp164-.Ltmp163       #   Call between .Ltmp163 and .Ltmp164
	.long	.Ltmp165-.Lfunc_begin5  #     jumps to .Ltmp165
	.byte	0                       #   On action: cleanup
	.long	.Ltmp166-.Lfunc_begin5  # >> Call Site 11 <<
	.long	.Ltmp167-.Ltmp166       #   Call between .Ltmp166 and .Ltmp167
	.long	.Ltmp170-.Lfunc_begin5  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp171-.Lfunc_begin5  # >> Call Site 12 <<
	.long	.Ltmp174-.Ltmp171       #   Call between .Ltmp171 and .Ltmp174
	.long	.Ltmp175-.Lfunc_begin5  #     jumps to .Ltmp175
	.byte	0                       #   On action: cleanup
	.long	.Ltmp176-.Lfunc_begin5  # >> Call Site 13 <<
	.long	.Ltmp179-.Ltmp176       #   Call between .Ltmp176 and .Ltmp179
	.long	.Ltmp180-.Lfunc_begin5  #     jumps to .Ltmp180
	.byte	0                       #   On action: cleanup
	.long	.Ltmp183-.Lfunc_begin5  # >> Call Site 14 <<
	.long	.Ltmp184-.Ltmp183       #   Call between .Ltmp183 and .Ltmp184
	.long	.Ltmp185-.Lfunc_begin5  #     jumps to .Ltmp185
	.byte	0                       #   On action: cleanup
	.long	.Ltmp186-.Lfunc_begin5  # >> Call Site 15 <<
	.long	.Ltmp187-.Ltmp186       #   Call between .Ltmp186 and .Ltmp187
	.long	.Ltmp188-.Lfunc_begin5  #     jumps to .Ltmp188
	.byte	0                       #   On action: cleanup
	.long	.Ltmp189-.Lfunc_begin5  # >> Call Site 16 <<
	.long	.Ltmp194-.Ltmp189       #   Call between .Ltmp189 and .Ltmp194
	.long	.Ltmp200-.Lfunc_begin5  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp195-.Lfunc_begin5  # >> Call Site 17 <<
	.long	.Ltmp196-.Ltmp195       #   Call between .Ltmp195 and .Ltmp196
	.long	.Ltmp197-.Lfunc_begin5  #     jumps to .Ltmp197
	.byte	0                       #   On action: cleanup
	.long	.Ltmp198-.Lfunc_begin5  # >> Call Site 18 <<
	.long	.Ltmp199-.Ltmp198       #   Call between .Ltmp198 and .Ltmp199
	.long	.Ltmp200-.Lfunc_begin5  #     jumps to .Ltmp200
	.byte	0                       #   On action: cleanup
	.long	.Ltmp244-.Lfunc_begin5  # >> Call Site 19 <<
	.long	.Ltmp245-.Ltmp244       #   Call between .Ltmp244 and .Ltmp245
	.long	.Ltmp246-.Lfunc_begin5  #     jumps to .Ltmp246
	.byte	0                       #   On action: cleanup
	.long	.Ltmp247-.Lfunc_begin5  # >> Call Site 20 <<
	.long	.Ltmp252-.Ltmp247       #   Call between .Ltmp247 and .Ltmp252
	.long	.Ltmp253-.Lfunc_begin5  #     jumps to .Ltmp253
	.byte	0                       #   On action: cleanup
	.long	.Ltmp254-.Lfunc_begin5  # >> Call Site 21 <<
	.long	.Ltmp255-.Ltmp254       #   Call between .Ltmp254 and .Ltmp255
	.long	.Ltmp498-.Lfunc_begin5  #     jumps to .Ltmp498
	.byte	0                       #   On action: cleanup
	.long	.Ltmp237-.Lfunc_begin5  # >> Call Site 22 <<
	.long	.Ltmp238-.Ltmp237       #   Call between .Ltmp237 and .Ltmp238
	.long	.Ltmp243-.Lfunc_begin5  #     jumps to .Ltmp243
	.byte	0                       #   On action: cleanup
	.long	.Ltmp489-.Lfunc_begin5  # >> Call Site 23 <<
	.long	.Ltmp490-.Ltmp489       #   Call between .Ltmp489 and .Ltmp490
	.long	.Ltmp491-.Lfunc_begin5  #     jumps to .Ltmp491
	.byte	0                       #   On action: cleanup
	.long	.Ltmp412-.Lfunc_begin5  # >> Call Site 24 <<
	.long	.Ltmp413-.Ltmp412       #   Call between .Ltmp412 and .Ltmp413
	.long	.Ltmp463-.Lfunc_begin5  #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp414-.Lfunc_begin5  # >> Call Site 25 <<
	.long	.Ltmp415-.Ltmp414       #   Call between .Ltmp414 and .Ltmp415
	.long	.Ltmp416-.Lfunc_begin5  #     jumps to .Ltmp416
	.byte	0                       #   On action: cleanup
	.long	.Ltmp417-.Lfunc_begin5  # >> Call Site 26 <<
	.long	.Ltmp418-.Ltmp417       #   Call between .Ltmp417 and .Ltmp418
	.long	.Ltmp419-.Lfunc_begin5  #     jumps to .Ltmp419
	.byte	0                       #   On action: cleanup
	.long	.Ltmp420-.Lfunc_begin5  # >> Call Site 27 <<
	.long	.Ltmp421-.Ltmp420       #   Call between .Ltmp420 and .Ltmp421
	.long	.Ltmp426-.Lfunc_begin5  #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp427-.Lfunc_begin5  # >> Call Site 28 <<
	.long	.Ltmp428-.Ltmp427       #   Call between .Ltmp427 and .Ltmp428
	.long	.Ltmp445-.Lfunc_begin5  #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp429-.Lfunc_begin5  # >> Call Site 29 <<
	.long	.Ltmp430-.Ltmp429       #   Call between .Ltmp429 and .Ltmp430
	.long	.Ltmp431-.Lfunc_begin5  #     jumps to .Ltmp431
	.byte	0                       #   On action: cleanup
	.long	.Ltmp432-.Lfunc_begin5  # >> Call Site 30 <<
	.long	.Ltmp433-.Ltmp432       #   Call between .Ltmp432 and .Ltmp433
	.long	.Ltmp445-.Lfunc_begin5  #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp434-.Lfunc_begin5  # >> Call Site 31 <<
	.long	.Ltmp435-.Ltmp434       #   Call between .Ltmp434 and .Ltmp435
	.long	.Ltmp450-.Lfunc_begin5  #     jumps to .Ltmp450
	.byte	0                       #   On action: cleanup
	.long	.Ltmp436-.Lfunc_begin5  # >> Call Site 32 <<
	.long	.Ltmp439-.Ltmp436       #   Call between .Ltmp436 and .Ltmp439
	.long	.Ltmp442-.Lfunc_begin5  #     jumps to .Ltmp442
	.byte	0                       #   On action: cleanup
	.long	.Ltmp422-.Lfunc_begin5  # >> Call Site 33 <<
	.long	.Ltmp423-.Ltmp422       #   Call between .Ltmp422 and .Ltmp423
	.long	.Ltmp426-.Lfunc_begin5  #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp443-.Lfunc_begin5  # >> Call Site 34 <<
	.long	.Ltmp444-.Ltmp443       #   Call between .Ltmp443 and .Ltmp444
	.long	.Ltmp445-.Lfunc_begin5  #     jumps to .Ltmp445
	.byte	0                       #   On action: cleanup
	.long	.Ltmp424-.Lfunc_begin5  # >> Call Site 35 <<
	.long	.Ltmp425-.Ltmp424       #   Call between .Ltmp424 and .Ltmp425
	.long	.Ltmp426-.Lfunc_begin5  #     jumps to .Ltmp426
	.byte	0                       #   On action: cleanup
	.long	.Ltmp394-.Lfunc_begin5  # >> Call Site 36 <<
	.long	.Ltmp395-.Ltmp394       #   Call between .Ltmp394 and .Ltmp395
	.long	.Ltmp396-.Lfunc_begin5  #     jumps to .Ltmp396
	.byte	0                       #   On action: cleanup
	.long	.Ltmp440-.Lfunc_begin5  # >> Call Site 37 <<
	.long	.Ltmp441-.Ltmp440       #   Call between .Ltmp440 and .Ltmp441
	.long	.Ltmp442-.Lfunc_begin5  #     jumps to .Ltmp442
	.byte	0                       #   On action: cleanup
	.long	.Ltmp446-.Lfunc_begin5  # >> Call Site 38 <<
	.long	.Ltmp449-.Ltmp446       #   Call between .Ltmp446 and .Ltmp449
	.long	.Ltmp450-.Lfunc_begin5  #     jumps to .Ltmp450
	.byte	0                       #   On action: cleanup
	.long	.Ltmp453-.Lfunc_begin5  # >> Call Site 39 <<
	.long	.Ltmp454-.Ltmp453       #   Call between .Ltmp453 and .Ltmp454
	.long	.Ltmp455-.Lfunc_begin5  #     jumps to .Ltmp455
	.byte	0                       #   On action: cleanup
	.long	.Ltmp458-.Lfunc_begin5  # >> Call Site 40 <<
	.long	.Ltmp459-.Ltmp458       #   Call between .Ltmp458 and .Ltmp459
	.long	.Ltmp460-.Lfunc_begin5  #     jumps to .Ltmp460
	.byte	0                       #   On action: cleanup
	.long	.Ltmp461-.Lfunc_begin5  # >> Call Site 41 <<
	.long	.Ltmp462-.Ltmp461       #   Call between .Ltmp461 and .Ltmp462
	.long	.Ltmp463-.Lfunc_begin5  #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp464-.Lfunc_begin5  # >> Call Site 42 <<
	.long	.Ltmp467-.Ltmp464       #   Call between .Ltmp464 and .Ltmp467
	.long	.Ltmp468-.Lfunc_begin5  #     jumps to .Ltmp468
	.byte	0                       #   On action: cleanup
	.long	.Ltmp469-.Lfunc_begin5  # >> Call Site 43 <<
	.long	.Ltmp470-.Ltmp469       #   Call between .Ltmp469 and .Ltmp470
	.long	.Ltmp471-.Lfunc_begin5  #     jumps to .Ltmp471
	.byte	0                       #   On action: cleanup
	.long	.Ltmp256-.Lfunc_begin5  # >> Call Site 44 <<
	.long	.Ltmp259-.Ltmp256       #   Call between .Ltmp256 and .Ltmp259
	.long	.Ltmp491-.Lfunc_begin5  #     jumps to .Ltmp491
	.byte	0                       #   On action: cleanup
	.long	.Ltmp260-.Lfunc_begin5  # >> Call Site 45 <<
	.long	.Ltmp261-.Ltmp260       #   Call between .Ltmp260 and .Ltmp261
	.long	.Ltmp262-.Lfunc_begin5  #     jumps to .Ltmp262
	.byte	0                       #   On action: cleanup
	.long	.Ltmp263-.Lfunc_begin5  # >> Call Site 46 <<
	.long	.Ltmp264-.Ltmp263       #   Call between .Ltmp263 and .Ltmp264
	.long	.Ltmp282-.Lfunc_begin5  #     jumps to .Ltmp282
	.byte	0                       #   On action: cleanup
	.long	.Ltmp265-.Lfunc_begin5  # >> Call Site 47 <<
	.long	.Ltmp266-.Ltmp265       #   Call between .Ltmp265 and .Ltmp266
	.long	.Ltmp267-.Lfunc_begin5  #     jumps to .Ltmp267
	.byte	0                       #   On action: cleanup
	.long	.Ltmp273-.Lfunc_begin5  # >> Call Site 48 <<
	.long	.Ltmp274-.Ltmp273       #   Call between .Ltmp273 and .Ltmp274
	.long	.Ltmp275-.Lfunc_begin5  #     jumps to .Ltmp275
	.byte	0                       #   On action: cleanup
	.long	.Ltmp276-.Lfunc_begin5  # >> Call Site 49 <<
	.long	.Ltmp281-.Ltmp276       #   Call between .Ltmp276 and .Ltmp281
	.long	.Ltmp282-.Lfunc_begin5  #     jumps to .Ltmp282
	.byte	0                       #   On action: cleanup
	.long	.Ltmp283-.Lfunc_begin5  # >> Call Site 50 <<
	.long	.Ltmp284-.Ltmp283       #   Call between .Ltmp283 and .Ltmp284
	.long	.Ltmp285-.Lfunc_begin5  #     jumps to .Ltmp285
	.byte	0                       #   On action: cleanup
	.long	.Ltmp286-.Lfunc_begin5  # >> Call Site 51 <<
	.long	.Ltmp289-.Ltmp286       #   Call between .Ltmp286 and .Ltmp289
	.long	.Ltmp290-.Lfunc_begin5  #     jumps to .Ltmp290
	.byte	0                       #   On action: cleanup
	.long	.Ltmp293-.Lfunc_begin5  # >> Call Site 52 <<
	.long	.Ltmp294-.Ltmp293       #   Call between .Ltmp293 and .Ltmp294
	.long	.Ltmp295-.Lfunc_begin5  #     jumps to .Ltmp295
	.byte	0                       #   On action: cleanup
	.long	.Ltmp296-.Lfunc_begin5  # >> Call Site 53 <<
	.long	.Ltmp299-.Ltmp296       #   Call between .Ltmp296 and .Ltmp299
	.long	.Ltmp300-.Lfunc_begin5  #     jumps to .Ltmp300
	.byte	0                       #   On action: cleanup
	.long	.Ltmp301-.Lfunc_begin5  # >> Call Site 54 <<
	.long	.Ltmp302-.Ltmp301       #   Call between .Ltmp301 and .Ltmp302
	.long	.Ltmp303-.Lfunc_begin5  #     jumps to .Ltmp303
	.byte	0                       #   On action: cleanup
	.long	.Ltmp304-.Lfunc_begin5  # >> Call Site 55 <<
	.long	.Ltmp305-.Ltmp304       #   Call between .Ltmp304 and .Ltmp305
	.long	.Ltmp306-.Lfunc_begin5  #     jumps to .Ltmp306
	.byte	0                       #   On action: cleanup
	.long	.Ltmp307-.Lfunc_begin5  # >> Call Site 56 <<
	.long	.Ltmp308-.Ltmp307       #   Call between .Ltmp307 and .Ltmp308
	.long	.Ltmp309-.Lfunc_begin5  #     jumps to .Ltmp309
	.byte	0                       #   On action: cleanup
	.long	.Ltmp310-.Lfunc_begin5  # >> Call Site 57 <<
	.long	.Ltmp311-.Ltmp310       #   Call between .Ltmp310 and .Ltmp311
	.long	.Ltmp320-.Lfunc_begin5  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp312-.Lfunc_begin5  # >> Call Site 58 <<
	.long	.Ltmp313-.Ltmp312       #   Call between .Ltmp312 and .Ltmp313
	.long	.Ltmp314-.Lfunc_begin5  #     jumps to .Ltmp314
	.byte	0                       #   On action: cleanup
	.long	.Ltmp318-.Lfunc_begin5  # >> Call Site 59 <<
	.long	.Ltmp319-.Ltmp318       #   Call between .Ltmp318 and .Ltmp319
	.long	.Ltmp320-.Lfunc_begin5  #     jumps to .Ltmp320
	.byte	0                       #   On action: cleanup
	.long	.Ltmp321-.Lfunc_begin5  # >> Call Site 60 <<
	.long	.Ltmp322-.Ltmp321       #   Call between .Ltmp321 and .Ltmp322
	.long	.Ltmp323-.Lfunc_begin5  #     jumps to .Ltmp323
	.byte	0                       #   On action: cleanup
	.long	.Ltmp324-.Lfunc_begin5  # >> Call Site 61 <<
	.long	.Ltmp325-.Ltmp324       #   Call between .Ltmp324 and .Ltmp325
	.long	.Ltmp409-.Lfunc_begin5  #     jumps to .Ltmp409
	.byte	0                       #   On action: cleanup
	.long	.Ltmp326-.Lfunc_begin5  # >> Call Site 62 <<
	.long	.Ltmp327-.Ltmp326       #   Call between .Ltmp326 and .Ltmp327
	.long	.Ltmp328-.Lfunc_begin5  #     jumps to .Ltmp328
	.byte	0                       #   On action: cleanup
	.long	.Ltmp340-.Lfunc_begin5  # >> Call Site 63 <<
	.long	.Ltmp343-.Ltmp340       #   Call between .Ltmp340 and .Ltmp343
	.long	.Ltmp463-.Lfunc_begin5  #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp344-.Lfunc_begin5  # >> Call Site 64 <<
	.long	.Ltmp345-.Ltmp344       #   Call between .Ltmp344 and .Ltmp345
	.long	.Ltmp346-.Lfunc_begin5  #     jumps to .Ltmp346
	.byte	0                       #   On action: cleanup
	.long	.Ltmp347-.Lfunc_begin5  # >> Call Site 65 <<
	.long	.Ltmp348-.Ltmp347       #   Call between .Ltmp347 and .Ltmp348
	.long	.Ltmp349-.Lfunc_begin5  #     jumps to .Ltmp349
	.byte	0                       #   On action: cleanup
	.long	.Ltmp350-.Lfunc_begin5  # >> Call Site 66 <<
	.long	.Ltmp351-.Ltmp350       #   Call between .Ltmp350 and .Ltmp351
	.long	.Ltmp352-.Lfunc_begin5  #     jumps to .Ltmp352
	.byte	0                       #   On action: cleanup
	.long	.Ltmp353-.Lfunc_begin5  # >> Call Site 67 <<
	.long	.Ltmp354-.Ltmp353       #   Call between .Ltmp353 and .Ltmp354
	.long	.Ltmp355-.Lfunc_begin5  #     jumps to .Ltmp355
	.byte	0                       #   On action: cleanup
	.long	.Ltmp405-.Lfunc_begin5  # >> Call Site 68 <<
	.long	.Ltmp363-.Ltmp405       #   Call between .Ltmp405 and .Ltmp363
	.long	.Ltmp409-.Lfunc_begin5  #     jumps to .Ltmp409
	.byte	0                       #   On action: cleanup
	.long	.Ltmp364-.Lfunc_begin5  # >> Call Site 69 <<
	.long	.Ltmp365-.Ltmp364       #   Call between .Ltmp364 and .Ltmp365
	.long	.Ltmp366-.Lfunc_begin5  #     jumps to .Ltmp366
	.byte	0                       #   On action: cleanup
	.long	.Ltmp367-.Lfunc_begin5  # >> Call Site 70 <<
	.long	.Ltmp368-.Ltmp367       #   Call between .Ltmp367 and .Ltmp368
	.long	.Ltmp409-.Lfunc_begin5  #     jumps to .Ltmp409
	.byte	0                       #   On action: cleanup
	.long	.Ltmp369-.Lfunc_begin5  # >> Call Site 71 <<
	.long	.Ltmp370-.Ltmp369       #   Call between .Ltmp369 and .Ltmp370
	.long	.Ltmp371-.Lfunc_begin5  #     jumps to .Ltmp371
	.byte	0                       #   On action: cleanup
	.long	.Ltmp372-.Lfunc_begin5  # >> Call Site 72 <<
	.long	.Ltmp375-.Ltmp372       #   Call between .Ltmp372 and .Ltmp375
	.long	.Ltmp376-.Lfunc_begin5  #     jumps to .Ltmp376
	.byte	0                       #   On action: cleanup
	.long	.Ltmp383-.Lfunc_begin5  # >> Call Site 73 <<
	.long	.Ltmp384-.Ltmp383       #   Call between .Ltmp383 and .Ltmp384
	.long	.Ltmp385-.Lfunc_begin5  #     jumps to .Ltmp385
	.byte	0                       #   On action: cleanup
	.long	.Ltmp386-.Lfunc_begin5  # >> Call Site 74 <<
	.long	.Ltmp387-.Ltmp386       #   Call between .Ltmp386 and .Ltmp387
	.long	.Ltmp388-.Lfunc_begin5  #     jumps to .Ltmp388
	.byte	0                       #   On action: cleanup
	.long	.Ltmp377-.Lfunc_begin5  # >> Call Site 75 <<
	.long	.Ltmp378-.Ltmp377       #   Call between .Ltmp377 and .Ltmp378
	.long	.Ltmp379-.Lfunc_begin5  #     jumps to .Ltmp379
	.byte	0                       #   On action: cleanup
	.long	.Ltmp380-.Lfunc_begin5  # >> Call Site 76 <<
	.long	.Ltmp381-.Ltmp380       #   Call between .Ltmp380 and .Ltmp381
	.long	.Ltmp382-.Lfunc_begin5  #     jumps to .Ltmp382
	.byte	0                       #   On action: cleanup
	.long	.Ltmp389-.Lfunc_begin5  # >> Call Site 77 <<
	.long	.Ltmp390-.Ltmp389       #   Call between .Ltmp389 and .Ltmp390
	.long	.Ltmp391-.Lfunc_begin5  #     jumps to .Ltmp391
	.byte	0                       #   On action: cleanup
	.long	.Ltmp392-.Lfunc_begin5  # >> Call Site 78 <<
	.long	.Ltmp393-.Ltmp392       #   Call between .Ltmp392 and .Ltmp393
	.long	.Ltmp396-.Lfunc_begin5  #     jumps to .Ltmp396
	.byte	0                       #   On action: cleanup
	.long	.Ltmp397-.Lfunc_begin5  # >> Call Site 79 <<
	.long	.Ltmp398-.Ltmp397       #   Call between .Ltmp397 and .Ltmp398
	.long	.Ltmp404-.Lfunc_begin5  #     jumps to .Ltmp404
	.byte	0                       #   On action: cleanup
	.long	.Ltmp399-.Lfunc_begin5  # >> Call Site 80 <<
	.long	.Ltmp400-.Ltmp399       #   Call between .Ltmp399 and .Ltmp400
	.long	.Ltmp401-.Lfunc_begin5  #     jumps to .Ltmp401
	.byte	0                       #   On action: cleanup
	.long	.Ltmp356-.Lfunc_begin5  # >> Call Site 81 <<
	.long	.Ltmp357-.Ltmp356       #   Call between .Ltmp356 and .Ltmp357
	.long	.Ltmp358-.Lfunc_begin5  #     jumps to .Ltmp358
	.byte	0                       #   On action: cleanup
	.long	.Ltmp357-.Lfunc_begin5  # >> Call Site 82 <<
	.long	.Ltmp402-.Ltmp357       #   Call between .Ltmp357 and .Ltmp402
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp402-.Lfunc_begin5  # >> Call Site 83 <<
	.long	.Ltmp403-.Ltmp402       #   Call between .Ltmp402 and .Ltmp403
	.long	.Ltmp404-.Lfunc_begin5  #     jumps to .Ltmp404
	.byte	0                       #   On action: cleanup
	.long	.Ltmp359-.Lfunc_begin5  # >> Call Site 84 <<
	.long	.Ltmp360-.Ltmp359       #   Call between .Ltmp359 and .Ltmp360
	.long	.Ltmp361-.Lfunc_begin5  #     jumps to .Ltmp361
	.byte	0                       #   On action: cleanup
	.long	.Ltmp410-.Lfunc_begin5  # >> Call Site 85 <<
	.long	.Ltmp411-.Ltmp410       #   Call between .Ltmp410 and .Ltmp411
	.long	.Ltmp463-.Lfunc_begin5  #     jumps to .Ltmp463
	.byte	0                       #   On action: cleanup
	.long	.Ltmp474-.Lfunc_begin5  # >> Call Site 86 <<
	.long	.Ltmp475-.Ltmp474       #   Call between .Ltmp474 and .Ltmp475
	.long	.Ltmp476-.Lfunc_begin5  #     jumps to .Ltmp476
	.byte	0                       #   On action: cleanup
	.long	.Ltmp479-.Lfunc_begin5  # >> Call Site 87 <<
	.long	.Ltmp480-.Ltmp479       #   Call between .Ltmp479 and .Ltmp480
	.long	.Ltmp481-.Lfunc_begin5  #     jumps to .Ltmp481
	.byte	0                       #   On action: cleanup
	.long	.Ltmp484-.Lfunc_begin5  # >> Call Site 88 <<
	.long	.Ltmp485-.Ltmp484       #   Call between .Ltmp484 and .Ltmp485
	.long	.Ltmp486-.Lfunc_begin5  #     jumps to .Ltmp486
	.byte	0                       #   On action: cleanup
	.long	.Ltmp487-.Lfunc_begin5  # >> Call Site 89 <<
	.long	.Ltmp488-.Ltmp487       #   Call between .Ltmp487 and .Ltmp488
	.long	.Ltmp491-.Lfunc_begin5  #     jumps to .Ltmp491
	.byte	0                       #   On action: cleanup
	.long	.Ltmp492-.Lfunc_begin5  # >> Call Site 90 <<
	.long	.Ltmp497-.Ltmp492       #   Call between .Ltmp492 and .Ltmp497
	.long	.Ltmp498-.Lfunc_begin5  #     jumps to .Ltmp498
	.byte	0                       #   On action: cleanup
	.long	.Ltmp499-.Lfunc_begin5  # >> Call Site 91 <<
	.long	.Ltmp500-.Ltmp499       #   Call between .Ltmp499 and .Ltmp500
	.long	.Ltmp501-.Lfunc_begin5  #     jumps to .Ltmp501
	.byte	1                       #   On action: 1
	.long	.Ltmp502-.Lfunc_begin5  # >> Call Site 92 <<
	.long	.Ltmp511-.Ltmp502       #   Call between .Ltmp502 and .Ltmp511
	.long	.Ltmp516-.Lfunc_begin5  #     jumps to .Ltmp516
	.byte	1                       #   On action: 1
	.long	.Ltmp201-.Lfunc_begin5  # >> Call Site 93 <<
	.long	.Ltmp202-.Ltmp201       #   Call between .Ltmp201 and .Ltmp202
	.long	.Ltmp203-.Lfunc_begin5  #     jumps to .Ltmp203
	.byte	0                       #   On action: cleanup
	.long	.Ltmp239-.Lfunc_begin5  # >> Call Site 94 <<
	.long	.Ltmp242-.Ltmp239       #   Call between .Ltmp239 and .Ltmp242
	.long	.Ltmp243-.Lfunc_begin5  #     jumps to .Ltmp243
	.byte	0                       #   On action: cleanup
	.long	.Ltmp204-.Lfunc_begin5  # >> Call Site 95 <<
	.long	.Ltmp205-.Ltmp204       #   Call between .Ltmp204 and .Ltmp205
	.long	.Ltmp206-.Lfunc_begin5  #     jumps to .Ltmp206
	.byte	0                       #   On action: cleanup
	.long	.Ltmp207-.Lfunc_begin5  # >> Call Site 96 <<
	.long	.Ltmp208-.Ltmp207       #   Call between .Ltmp207 and .Ltmp208
	.long	.Ltmp209-.Lfunc_begin5  #     jumps to .Ltmp209
	.byte	0                       #   On action: cleanup
	.long	.Ltmp210-.Lfunc_begin5  # >> Call Site 97 <<
	.long	.Ltmp211-.Ltmp210       #   Call between .Ltmp210 and .Ltmp211
	.long	.Ltmp214-.Lfunc_begin5  #     jumps to .Ltmp214
	.byte	0                       #   On action: cleanup
	.long	.Ltmp504-.Lfunc_begin5  # >> Call Site 98 <<
	.long	.Ltmp505-.Ltmp504       #   Call between .Ltmp504 and .Ltmp505
	.long	.Ltmp516-.Lfunc_begin5  #     jumps to .Ltmp516
	.byte	1                       #   On action: 1
	.long	.Ltmp212-.Lfunc_begin5  # >> Call Site 99 <<
	.long	.Ltmp213-.Ltmp212       #   Call between .Ltmp212 and .Ltmp213
	.long	.Ltmp214-.Lfunc_begin5  #     jumps to .Ltmp214
	.byte	0                       #   On action: cleanup
	.long	.Ltmp217-.Lfunc_begin5  # >> Call Site 100 <<
	.long	.Ltmp218-.Ltmp217       #   Call between .Ltmp217 and .Ltmp218
	.long	.Ltmp219-.Lfunc_begin5  #     jumps to .Ltmp219
	.byte	0                       #   On action: cleanup
	.long	.Ltmp228-.Lfunc_begin5  # >> Call Site 101 <<
	.long	.Ltmp229-.Ltmp228       #   Call between .Ltmp228 and .Ltmp229
	.long	.Ltmp230-.Lfunc_begin5  #     jumps to .Ltmp230
	.byte	0                       #   On action: cleanup
	.long	.Ltmp234-.Lfunc_begin5  # >> Call Site 102 <<
	.long	.Ltmp235-.Ltmp234       #   Call between .Ltmp234 and .Ltmp235
	.long	.Ltmp236-.Lfunc_begin5  #     jumps to .Ltmp236
	.byte	0                       #   On action: cleanup
	.long	.Ltmp512-.Lfunc_begin5  # >> Call Site 103 <<
	.long	.Ltmp515-.Ltmp512       #   Call between .Ltmp512 and .Ltmp515
	.long	.Ltmp516-.Lfunc_begin5  #     jumps to .Ltmp516
	.byte	1                       #   On action: 1
	.long	.Ltmp522-.Lfunc_begin5  # >> Call Site 104 <<
	.long	.Ltmp523-.Ltmp522       #   Call between .Ltmp522 and .Ltmp523
	.long	.Ltmp524-.Lfunc_begin5  #     jumps to .Ltmp524
	.byte	0                       #   On action: cleanup
	.long	.Ltmp528-.Lfunc_begin5  # >> Call Site 105 <<
	.long	.Ltmp529-.Ltmp528       #   Call between .Ltmp528 and .Ltmp529
	.long	.Ltmp530-.Lfunc_begin5  #     jumps to .Ltmp530
	.byte	0                       #   On action: cleanup
	.long	.Ltmp531-.Lfunc_begin5  # >> Call Site 106 <<
	.long	.Ltmp532-.Ltmp531       #   Call between .Ltmp531 and .Ltmp532
	.long	.Ltmp533-.Lfunc_begin5  #     jumps to .Ltmp533
	.byte	0                       #   On action: cleanup
	.long	.Ltmp534-.Lfunc_begin5  # >> Call Site 107 <<
	.long	.Ltmp535-.Ltmp534       #   Call between .Ltmp534 and .Ltmp535
	.long	.Ltmp536-.Lfunc_begin5  #     jumps to .Ltmp536
	.byte	0                       #   On action: cleanup
	.long	.Ltmp535-.Lfunc_begin5  # >> Call Site 108 <<
	.long	.Ltmp537-.Ltmp535       #   Call between .Ltmp535 and .Ltmp537
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp537-.Lfunc_begin5  # >> Call Site 109 <<
	.long	.Ltmp538-.Ltmp537       #   Call between .Ltmp537 and .Ltmp538
	.long	.Ltmp539-.Lfunc_begin5  #     jumps to .Ltmp539
	.byte	0                       #   On action: cleanup
	.long	.Ltmp538-.Lfunc_begin5  # >> Call Site 110 <<
	.long	.Ltmp268-.Ltmp538       #   Call between .Ltmp538 and .Ltmp268
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp268-.Lfunc_begin5  # >> Call Site 111 <<
	.long	.Ltmp269-.Ltmp268       #   Call between .Ltmp268 and .Ltmp269
	.long	.Ltmp270-.Lfunc_begin5  #     jumps to .Ltmp270
	.byte	0                       #   On action: cleanup
	.long	.Ltmp269-.Lfunc_begin5  # >> Call Site 112 <<
	.long	.Ltmp168-.Ltmp269       #   Call between .Ltmp269 and .Ltmp168
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp168-.Lfunc_begin5  # >> Call Site 113 <<
	.long	.Ltmp169-.Ltmp168       #   Call between .Ltmp168 and .Ltmp169
	.long	.Ltmp170-.Lfunc_begin5  #     jumps to .Ltmp170
	.byte	0                       #   On action: cleanup
	.long	.Ltmp169-.Lfunc_begin5  # >> Call Site 114 <<
	.long	.Ltmp315-.Ltmp169       #   Call between .Ltmp169 and .Ltmp315
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp315-.Lfunc_begin5  # >> Call Site 115 <<
	.long	.Ltmp316-.Ltmp315       #   Call between .Ltmp315 and .Ltmp316
	.long	.Ltmp317-.Lfunc_begin5  #     jumps to .Ltmp317
	.byte	0                       #   On action: cleanup
	.long	.Ltmp451-.Lfunc_begin5  # >> Call Site 116 <<
	.long	.Ltmp457-.Ltmp451       #   Call between .Ltmp451 and .Ltmp457
	.long	.Ltmp542-.Lfunc_begin5  #     jumps to .Ltmp542
	.byte	1                       #   On action: 1
	.long	.Ltmp329-.Lfunc_begin5  # >> Call Site 117 <<
	.long	.Ltmp330-.Ltmp329       #   Call between .Ltmp329 and .Ltmp330
	.long	.Ltmp339-.Lfunc_begin5  #     jumps to .Ltmp339
	.byte	1                       #   On action: 1
	.long	.Ltmp331-.Lfunc_begin5  # >> Call Site 118 <<
	.long	.Ltmp332-.Ltmp331       #   Call between .Ltmp331 and .Ltmp332
	.long	.Ltmp333-.Lfunc_begin5  #     jumps to .Ltmp333
	.byte	1                       #   On action: 1
	.long	.Ltmp337-.Lfunc_begin5  # >> Call Site 119 <<
	.long	.Ltmp338-.Ltmp337       #   Call between .Ltmp337 and .Ltmp338
	.long	.Ltmp339-.Lfunc_begin5  #     jumps to .Ltmp339
	.byte	1                       #   On action: 1
	.long	.Ltmp334-.Lfunc_begin5  # >> Call Site 120 <<
	.long	.Ltmp335-.Ltmp334       #   Call between .Ltmp334 and .Ltmp335
	.long	.Ltmp336-.Lfunc_begin5  #     jumps to .Ltmp336
	.byte	1                       #   On action: 1
	.long	.Ltmp231-.Lfunc_begin5  # >> Call Site 121 <<
	.long	.Ltmp232-.Ltmp231       #   Call between .Ltmp231 and .Ltmp232
	.long	.Ltmp233-.Lfunc_begin5  #     jumps to .Ltmp233
	.byte	1                       #   On action: 1
	.long	.Ltmp232-.Lfunc_begin5  # >> Call Site 122 <<
	.long	.Ltmp517-.Ltmp232       #   Call between .Ltmp232 and .Ltmp517
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp517-.Lfunc_begin5  # >> Call Site 123 <<
	.long	.Ltmp518-.Ltmp517       #   Call between .Ltmp517 and .Ltmp518
	.long	.Ltmp519-.Lfunc_begin5  #     jumps to .Ltmp519
	.byte	0                       #   On action: cleanup
	.long	.Ltmp520-.Lfunc_begin5  # >> Call Site 124 <<
	.long	.Ltmp216-.Ltmp520       #   Call between .Ltmp520 and .Ltmp216
	.long	.Ltmp557-.Lfunc_begin5  #     jumps to .Ltmp557
	.byte	1                       #   On action: 1
	.long	.Ltmp220-.Lfunc_begin5  # >> Call Site 125 <<
	.long	.Ltmp221-.Ltmp220       #   Call between .Ltmp220 and .Ltmp221
	.long	.Ltmp222-.Lfunc_begin5  #     jumps to .Ltmp222
	.byte	1                       #   On action: 1
	.long	.Ltmp226-.Lfunc_begin5  # >> Call Site 126 <<
	.long	.Ltmp227-.Ltmp226       #   Call between .Ltmp226 and .Ltmp227
	.long	.Ltmp557-.Lfunc_begin5  #     jumps to .Ltmp557
	.byte	1                       #   On action: 1
	.long	.Ltmp223-.Lfunc_begin5  # >> Call Site 127 <<
	.long	.Ltmp224-.Ltmp223       #   Call between .Ltmp223 and .Ltmp224
	.long	.Ltmp225-.Lfunc_begin5  #     jumps to .Ltmp225
	.byte	1                       #   On action: 1
	.long	.Ltmp525-.Lfunc_begin5  # >> Call Site 128 <<
	.long	.Ltmp526-.Ltmp525       #   Call between .Ltmp525 and .Ltmp526
	.long	.Ltmp527-.Lfunc_begin5  #     jumps to .Ltmp527
	.byte	1                       #   On action: 1
	.long	.Ltmp472-.Lfunc_begin5  # >> Call Site 129 <<
	.long	.Ltmp541-.Ltmp472       #   Call between .Ltmp472 and .Ltmp541
	.long	.Ltmp542-.Lfunc_begin5  #     jumps to .Ltmp542
	.byte	1                       #   On action: 1
	.long	.Ltmp541-.Lfunc_begin5  # >> Call Site 130 <<
	.long	.Ltmp543-.Ltmp541       #   Call between .Ltmp541 and .Ltmp543
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp543-.Lfunc_begin5  # >> Call Site 131 <<
	.long	.Ltmp544-.Ltmp543       #   Call between .Ltmp543 and .Ltmp544
	.long	.Ltmp545-.Lfunc_begin5  #     jumps to .Ltmp545
	.byte	1                       #   On action: 1
	.long	.Ltmp549-.Lfunc_begin5  # >> Call Site 132 <<
	.long	.Ltmp550-.Ltmp549       #   Call between .Ltmp549 and .Ltmp550
	.long	.Ltmp557-.Lfunc_begin5  #     jumps to .Ltmp557
	.byte	1                       #   On action: 1
	.long	.Ltmp546-.Lfunc_begin5  # >> Call Site 133 <<
	.long	.Ltmp547-.Ltmp546       #   Call between .Ltmp546 and .Ltmp547
	.long	.Ltmp548-.Lfunc_begin5  #     jumps to .Ltmp548
	.byte	1                       #   On action: 1
	.long	.Ltmp551-.Lfunc_begin5  # >> Call Site 134 <<
	.long	.Ltmp556-.Ltmp551       #   Call between .Ltmp551 and .Ltmp556
	.long	.Ltmp557-.Lfunc_begin5  #     jumps to .Ltmp557
	.byte	1                       #   On action: 1
	.long	.Ltmp556-.Lfunc_begin5  # >> Call Site 135 <<
	.long	.Lfunc_end9-.Ltmp556    #   Call between .Ltmp556 and .Lfunc_end9
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZNK12CArchivePath12GetFinalPathEv,"axG",@progbits,_ZNK12CArchivePath12GetFinalPathEv,comdat
	.weak	_ZNK12CArchivePath12GetFinalPathEv
	.p2align	4, 0x90
	.type	_ZNK12CArchivePath12GetFinalPathEv,@function
_ZNK12CArchivePath12GetFinalPathEv:     # @_ZNK12CArchivePath12GetFinalPathEv
.Lfunc_begin6:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception6
# BB#0:
	pushq	%rbp
.Lcfi91:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi92:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi93:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi94:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi95:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi96:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi97:
	.cfi_def_cfa_offset 64
.Lcfi98:
	.cfi_offset %rbx, -56
.Lcfi99:
	.cfi_offset %r12, -48
.Lcfi100:
	.cfi_offset %r13, -40
.Lcfi101:
	.cfi_offset %r14, -32
.Lcfi102:
	.cfi_offset %r15, -24
.Lcfi103:
	.cfi_offset %rbp, -16
	movq	%rsi, %r12
	movq	%rdi, %r14
	leaq	16(%r12), %rsi
	leaq	32(%r12), %rdx
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
	movl	56(%r12), %ebx
	testl	%ebx, %ebx
	je	.LBB10_15
# BB#1:
.Ltmp558:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r15
.Ltmp559:
# BB#2:                                 # %._crit_edge16.i.i.i
	movq	$46, (%r15)
.Ltmp561:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp562:
# BB#3:                                 # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movl	$46, (%r13)
	movl	$4, %eax
	.p2align	4, 0x90
.LBB10_4:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r15,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB10_4
# BB#5:                                 # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	testl	%ebx, %ebx
	jle	.LBB10_6
# BB#7:
	cmpl	$3, %ebx
	movl	$4, %eax
	cmovgl	%ebx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp564:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp565:
# BB#8:                                 # %.lr.ph.i.i
	movl	(%r13), %eax
	movl	%eax, (%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbx)
	movq	%rbx, %r13
	jmp	.LBB10_9
.LBB10_6:
	movq	%r13, %rbx
.LBB10_9:                               # %.noexc.i
	movq	48(%r12), %rcx
	leaq	4(%rbx), %rsi
	.p2align	4, 0x90
.LBB10_10:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB10_10
# BB#11:
	movl	56(%r12), %ebp
	incl	%ebp
.Ltmp567:
	movq	%r14, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp568:
# BB#12:                                # %.noexc5
	movslq	8(%r14), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r14), %rcx
	.p2align	4, 0x90
.LBB10_13:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB10_13
# BB#14:                                # %_ZN11CStringBaseIwED2Ev.exit7
	addl	%eax, %ebp
	movl	%ebp, 8(%r14)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%r15, %rdi
	callq	_ZdaPv
.LBB10_15:
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB10_24:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp566:
	jmp	.LBB10_19
.LBB10_18:
.Ltmp569:
.LBB10_19:                              # %_ZN11CStringBaseIwED2Ev.exit9
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB10_20
.LBB10_17:
.Ltmp563:
	movq	%rax, %rbx
.LBB10_20:                              # %_ZN11CStringBaseIwED2Ev.exit9
	movq	%r15, %rdi
	callq	_ZdaPv
	jmp	.LBB10_21
.LBB10_16:
.Ltmp560:
	movq	%rax, %rbx
.LBB10_21:
	movq	(%r14), %rdi
	testq	%rdi, %rdi
	je	.LBB10_23
# BB#22:
	callq	_ZdaPv
.LBB10_23:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end10:
	.size	_ZNK12CArchivePath12GetFinalPathEv, .Lfunc_end10-_ZNK12CArchivePath12GetFinalPathEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table10:
.Lexception6:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\320"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	78                      # Call site table length
	.long	.Lfunc_begin6-.Lfunc_begin6 # >> Call Site 1 <<
	.long	.Ltmp558-.Lfunc_begin6  #   Call between .Lfunc_begin6 and .Ltmp558
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp558-.Lfunc_begin6  # >> Call Site 2 <<
	.long	.Ltmp559-.Ltmp558       #   Call between .Ltmp558 and .Ltmp559
	.long	.Ltmp560-.Lfunc_begin6  #     jumps to .Ltmp560
	.byte	0                       #   On action: cleanup
	.long	.Ltmp561-.Lfunc_begin6  # >> Call Site 3 <<
	.long	.Ltmp562-.Ltmp561       #   Call between .Ltmp561 and .Ltmp562
	.long	.Ltmp563-.Lfunc_begin6  #     jumps to .Ltmp563
	.byte	0                       #   On action: cleanup
	.long	.Ltmp564-.Lfunc_begin6  # >> Call Site 4 <<
	.long	.Ltmp565-.Ltmp564       #   Call between .Ltmp564 and .Ltmp565
	.long	.Ltmp566-.Lfunc_begin6  #     jumps to .Ltmp566
	.byte	0                       #   On action: cleanup
	.long	.Ltmp567-.Lfunc_begin6  # >> Call Site 5 <<
	.long	.Ltmp568-.Ltmp567       #   Call between .Ltmp567 and .Ltmp568
	.long	.Ltmp569-.Lfunc_begin6  #     jumps to .Ltmp569
	.byte	0                       #   On action: cleanup
	.long	.Ltmp568-.Lfunc_begin6  # >> Call Site 6 <<
	.long	.Lfunc_end10-.Ltmp568   #   Call between .Ltmp568 and .Lfunc_end10
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED2Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED2Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED2Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED2Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED2Ev
.Lfunc_begin7:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception7
# BB#0:
	pushq	%r14
.Lcfi104:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi105:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi106:
	.cfi_def_cfa_offset 32
.Lcfi107:
	.cfi_offset %rbx, -24
.Lcfi108:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp570:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp571:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB11_2:
.Ltmp572:
	movq	%rax, %r14
.Ltmp573:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp574:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB11_4:
.Ltmp575:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end11:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED2Ev, .Lfunc_end11-_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table11:
.Lexception7:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp570-.Lfunc_begin7  # >> Call Site 1 <<
	.long	.Ltmp571-.Ltmp570       #   Call between .Ltmp570 and .Ltmp571
	.long	.Ltmp572-.Lfunc_begin7  #     jumps to .Ltmp572
	.byte	0                       #   On action: cleanup
	.long	.Ltmp571-.Lfunc_begin7  # >> Call Site 2 <<
	.long	.Ltmp573-.Ltmp571       #   Call between .Ltmp571 and .Ltmp573
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp573-.Lfunc_begin7  # >> Call Site 3 <<
	.long	.Ltmp574-.Ltmp573       #   Call between .Ltmp573 and .Ltmp574
	.long	.Ltmp575-.Lfunc_begin7  #     jumps to .Ltmp575
	.byte	1                       #   On action: 1
	.long	.Ltmp574-.Lfunc_begin7  # >> Call Site 4 <<
	.long	.Lfunc_end11-.Ltmp574   #   Call between .Ltmp574 and .Lfunc_end11
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12CArchivePathaSERKS_,"axG",@progbits,_ZN12CArchivePathaSERKS_,comdat
	.weak	_ZN12CArchivePathaSERKS_
	.p2align	4, 0x90
	.type	_ZN12CArchivePathaSERKS_,@function
_ZN12CArchivePathaSERKS_:               # @_ZN12CArchivePathaSERKS_
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi109:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi110:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi111:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi112:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi113:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi114:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi115:
	.cfi_def_cfa_offset 64
.Lcfi116:
	.cfi_offset %rbx, -56
.Lcfi117:
	.cfi_offset %r12, -48
.Lcfi118:
	.cfi_offset %r13, -40
.Lcfi119:
	.cfi_offset %r14, -32
.Lcfi120:
	.cfi_offset %r15, -24
.Lcfi121:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	cmpq	%r15, %r14
	je	.LBB12_58
# BB#1:
	movl	$0, 8(%r15)
	movq	(%r15), %rbx
	movl	$0, (%rbx)
	movslq	8(%r14), %r12
	incq	%r12
	movl	12(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB12_6
# BB#2:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_5
# BB#3:
	testl	%ebp, %ebp
	jle	.LBB12_5
# BB#4:                                 # %._crit_edge.thread.i.i
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	8(%r15), %rax
.LBB12_5:                               # %._crit_edge16.i.i
	movq	%r13, (%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 12(%r15)
	movq	%r13, %rbx
.LBB12_6:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r14), %rax
	.p2align	4, 0x90
.LBB12_7:                               # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_7
# BB#8:                                 # %_ZN11CStringBaseIwEaSERKS0_.exit
	cmpq	%r15, %r14
	movl	8(%r14), %eax
	movl	%eax, 8(%r15)
	je	.LBB12_58
# BB#9:
	movl	$0, 24(%r15)
	movq	16(%r15), %rbx
	movl	$0, (%rbx)
	movslq	24(%r14), %r12
	incq	%r12
	movl	28(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB12_14
# BB#10:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_13
# BB#11:
	testl	%ebp, %ebp
	jle	.LBB12_13
# BB#12:                                # %._crit_edge.thread.i.i12
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	24(%r15), %rax
.LBB12_13:                              # %._crit_edge16.i.i13
	movq	%r13, 16(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 28(%r15)
	movq	%r13, %rbx
.LBB12_14:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i14
	movq	16(%r14), %rax
	.p2align	4, 0x90
.LBB12_15:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_15
# BB#16:                                # %_ZN11CStringBaseIwEaSERKS0_.exit18
	cmpq	%r15, %r14
	movl	24(%r14), %eax
	movl	%eax, 24(%r15)
	je	.LBB12_58
# BB#17:
	movl	$0, 40(%r15)
	movq	32(%r15), %rbx
	movl	$0, (%rbx)
	movslq	40(%r14), %r12
	incq	%r12
	movl	44(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB12_22
# BB#18:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_21
# BB#19:
	testl	%ebp, %ebp
	jle	.LBB12_21
# BB#20:                                # %._crit_edge.thread.i.i22
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	40(%r15), %rax
.LBB12_21:                              # %._crit_edge16.i.i23
	movq	%r13, 32(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 44(%r15)
	movq	%r13, %rbx
.LBB12_22:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i24
	movq	32(%r14), %rax
	.p2align	4, 0x90
.LBB12_23:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_23
# BB#24:                                # %_ZN11CStringBaseIwEaSERKS0_.exit28
	cmpq	%r15, %r14
	movl	40(%r14), %eax
	movl	%eax, 40(%r15)
	je	.LBB12_58
# BB#25:
	movl	$0, 56(%r15)
	movq	48(%r15), %rbx
	movl	$0, (%rbx)
	movslq	56(%r14), %r12
	incq	%r12
	movl	60(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB12_30
# BB#26:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_29
# BB#27:
	testl	%ebp, %ebp
	jle	.LBB12_29
# BB#28:                                # %._crit_edge.thread.i.i32
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	56(%r15), %rax
.LBB12_29:                              # %._crit_edge16.i.i33
	movq	%r13, 48(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 60(%r15)
	movq	%r13, %rbx
.LBB12_30:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i34
	movq	48(%r14), %rax
	.p2align	4, 0x90
.LBB12_31:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_31
# BB#32:                                # %_ZN11CStringBaseIwEaSERKS0_.exit38
	cmpq	%r15, %r14
	movl	56(%r14), %eax
	movl	%eax, 56(%r15)
	je	.LBB12_58
# BB#33:
	movl	$0, 72(%r15)
	movq	64(%r15), %rbx
	movl	$0, (%rbx)
	movslq	72(%r14), %r12
	incq	%r12
	movl	76(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB12_38
# BB#34:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_37
# BB#35:
	testl	%ebp, %ebp
	jle	.LBB12_37
# BB#36:                                # %._crit_edge.thread.i.i42
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	72(%r15), %rax
.LBB12_37:                              # %._crit_edge16.i.i43
	movq	%r13, 64(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 76(%r15)
	movq	%r13, %rbx
.LBB12_38:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i44
	movq	64(%r14), %rax
	.p2align	4, 0x90
.LBB12_39:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_39
# BB#40:                                # %_ZN11CStringBaseIwEaSERKS0_.exit48
	cmpq	%r15, %r14
	movl	72(%r14), %eax
	movl	%eax, 72(%r15)
	movb	80(%r14), %al
	movb	%al, 80(%r15)
	je	.LBB12_57
# BB#41:
	movl	$0, 96(%r15)
	movq	88(%r15), %rbx
	movl	$0, (%rbx)
	movslq	96(%r14), %r12
	incq	%r12
	movl	100(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB12_46
# BB#42:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_45
# BB#43:
	testl	%ebp, %ebp
	jle	.LBB12_45
# BB#44:                                # %._crit_edge.thread.i.i52
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	96(%r15), %rax
.LBB12_45:                              # %._crit_edge16.i.i53
	movq	%r13, 88(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 100(%r15)
	movq	%r13, %rbx
.LBB12_46:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i54
	movq	88(%r14), %rax
	.p2align	4, 0x90
.LBB12_47:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_47
# BB#48:                                # %_ZN11CStringBaseIwEaSERKS0_.exit58
	cmpq	%r15, %r14
	movl	96(%r14), %eax
	movl	%eax, 96(%r15)
	je	.LBB12_57
# BB#49:
	movl	$0, 112(%r15)
	movq	104(%r15), %rbx
	movl	$0, (%rbx)
	movslq	112(%r14), %r12
	incq	%r12
	movl	116(%r15), %ebp
	cmpl	%ebp, %r12d
	je	.LBB12_54
# BB#50:
	movl	$4, %ecx
	movq	%r12, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r13
	xorl	%eax, %eax
	testq	%rbx, %rbx
	je	.LBB12_53
# BB#51:
	testl	%ebp, %ebp
	jle	.LBB12_53
# BB#52:                                # %._crit_edge.thread.i.i62
	movq	%rbx, %rdi
	callq	_ZdaPv
	movslq	112(%r15), %rax
.LBB12_53:                              # %._crit_edge16.i.i63
	movq	%r13, 104(%r15)
	movl	$0, (%r13,%rax,4)
	movl	%r12d, 116(%r15)
	movq	%r13, %rbx
.LBB12_54:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i64
	movq	104(%r14), %rax
	.p2align	4, 0x90
.LBB12_55:                              # =>This Inner Loop Header: Depth=1
	movl	(%rax), %ecx
	addq	$4, %rax
	movl	%ecx, (%rbx)
	addq	$4, %rbx
	testl	%ecx, %ecx
	jne	.LBB12_55
# BB#56:                                # %_Z12MyStringCopyIwEPT_S1_PKS0_.exit.i67
	movl	112(%r14), %eax
	movl	%eax, 112(%r15)
	jmp	.LBB12_57
.LBB12_58:                              # %_ZN11CStringBaseIwEaSERKS0_.exit48.thread
	movb	80(%r14), %al
	movb	%al, 80(%r15)
.LBB12_57:                              # %_ZN11CStringBaseIwEaSERKS0_.exit68
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end12:
	.size	_ZN12CArchivePathaSERKS_, .Lfunc_end12-_ZN12CArchivePathaSERKS_
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI13_0:
	.long	46                      # 0x2e
	.long	116                     # 0x74
	.long	109                     # 0x6d
	.long	112                     # 0x70
	.section	.text._ZNK12CArchivePath11GetTempPathEv,"axG",@progbits,_ZNK12CArchivePath11GetTempPathEv,comdat
	.weak	_ZNK12CArchivePath11GetTempPathEv
	.p2align	4, 0x90
	.type	_ZNK12CArchivePath11GetTempPathEv,@function
_ZNK12CArchivePath11GetTempPathEv:      # @_ZNK12CArchivePath11GetTempPathEv
.Lfunc_begin8:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception8
# BB#0:
	pushq	%rbp
.Lcfi122:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi123:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi124:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi125:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi126:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi127:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi128:
	.cfi_def_cfa_offset 64
.Lcfi129:
	.cfi_offset %rbx, -56
.Lcfi130:
	.cfi_offset %r12, -48
.Lcfi131:
	.cfi_offset %r13, -40
.Lcfi132:
	.cfi_offset %r14, -32
.Lcfi133:
	.cfi_offset %r15, -24
.Lcfi134:
	.cfi_offset %rbp, -16
	movq	%rsi, %r14
	movq	%rdi, %r15
	leaq	88(%r14), %rsi
	leaq	32(%r14), %rdx
	callq	_ZplIwE11CStringBaseIT_ERKS2_S4_
	movl	56(%r14), %ebx
	testl	%ebx, %ebx
	je	.LBB13_1
# BB#6:
.Ltmp576:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r12
.Ltmp577:
# BB#7:                                 # %._crit_edge16.i.i.i
	movq	$46, (%r12)
.Ltmp579:
	movl	$8, %edi
	callq	_Znam
	movq	%rax, %r13
.Ltmp580:
# BB#8:                                 # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i.i
	movl	$46, (%r13)
	movl	$4, %eax
	.p2align	4, 0x90
.LBB13_9:                               # %._crit_edge
                                        # =>This Inner Loop Header: Depth=1
	movl	(%r12,%rax), %ecx
	movl	%ecx, (%r13,%rax)
	addq	$4, %rax
	testl	%ecx, %ecx
	jne	.LBB13_9
# BB#10:                                # %_ZN11CStringBaseIwEC2ERKS0_.exit.i
	testl	%ebx, %ebx
	jle	.LBB13_11
# BB#12:
	cmpl	$3, %ebx
	movl	$4, %eax
	cmovgl	%ebx, %eax
	addl	$3, %eax
	cltq
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp582:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp583:
# BB#13:                                # %.lr.ph.i.i
	movl	(%r13), %eax
	movl	%eax, (%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, 4(%rbx)
	movq	%rbx, %r13
	jmp	.LBB13_14
.LBB13_11:
	movq	%r13, %rbx
.LBB13_14:                              # %.noexc.i
	movq	48(%r14), %rcx
	leaq	4(%rbx), %rsi
	.p2align	4, 0x90
.LBB13_15:                              # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %eax
	addq	$4, %rcx
	movl	%eax, (%rsi)
	addq	$4, %rsi
	testl	%eax, %eax
	jne	.LBB13_15
# BB#16:
	movl	56(%r14), %ebp
	incl	%ebp
.Ltmp585:
	movq	%r15, %rdi
	movl	%ebp, %esi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp586:
# BB#17:                                # %.noexc6
	movslq	8(%r15), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r15), %rcx
	.p2align	4, 0x90
.LBB13_18:                              # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %edx
	addq	$4, %rbx
	movl	%edx, (%rcx)
	addq	$4, %rcx
	testl	%edx, %edx
	jne	.LBB13_18
# BB#19:                                # %_ZN11CStringBaseIwED2Ev.exit8
	addl	%eax, %ebp
	movl	%ebp, 8(%r15)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%r12, %rdi
	callq	_ZdaPv
.LBB13_1:                               # %.preheader.preheader
.Ltmp588:
	movl	$4, %esi
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp589:
# BB#2:                                 # %.noexc13
	movq	(%r15), %rax
	movslq	8(%r15), %rcx
	movaps	.LCPI13_0(%rip), %xmm0  # xmm0 = [46,116,109,112]
	movups	%xmm0, (%rax,%rcx,4)
	movl	$0, 16(%rax,%rcx,4)
	leal	4(%rcx), %eax
	movl	%eax, 8(%r15)
	movl	112(%r14), %esi
.Ltmp590:
	movq	%r15, %rdi
	callq	_ZN11CStringBaseIwE10GrowLengthEi
.Ltmp591:
# BB#3:                                 # %.noexc16
	movslq	8(%r15), %rax
	movq	%rax, %rcx
	shlq	$2, %rcx
	addq	(%r15), %rcx
	movq	104(%r14), %rdx
	.p2align	4, 0x90
.LBB13_4:                               # =>This Inner Loop Header: Depth=1
	movl	(%rdx), %esi
	addq	$4, %rdx
	movl	%esi, (%rcx)
	addq	$4, %rcx
	testl	%esi, %esi
	jne	.LBB13_4
# BB#5:
	movl	112(%r14), %ecx
	addl	%eax, %ecx
	movl	%ecx, 8(%r15)
	movq	%r15, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB13_30:                              # %_ZN11CStringBaseIwED2Ev.exit.i
.Ltmp584:
	jmp	.LBB13_23
.LBB13_22:
.Ltmp587:
.LBB13_23:                              # %_ZN11CStringBaseIwED2Ev.exit10
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	jmp	.LBB13_24
.LBB13_21:
.Ltmp581:
	movq	%rax, %rbx
.LBB13_24:                              # %_ZN11CStringBaseIwED2Ev.exit10
	movq	%r12, %rdi
	callq	_ZdaPv
	jmp	.LBB13_27
.LBB13_20:
.Ltmp578:
	jmp	.LBB13_26
.LBB13_25:
.Ltmp592:
.LBB13_26:
	movq	%rax, %rbx
.LBB13_27:
	movq	(%r15), %rdi
	testq	%rdi, %rdi
	je	.LBB13_29
# BB#28:
	callq	_ZdaPv
.LBB13_29:                              # %_ZN11CStringBaseIwED2Ev.exit
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end13:
	.size	_ZNK12CArchivePath11GetTempPathEv, .Lfunc_end13-_ZNK12CArchivePath11GetTempPathEv
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table13:
.Lexception8:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	93                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	91                      # Call site table length
	.long	.Lfunc_begin8-.Lfunc_begin8 # >> Call Site 1 <<
	.long	.Ltmp576-.Lfunc_begin8  #   Call between .Lfunc_begin8 and .Ltmp576
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp576-.Lfunc_begin8  # >> Call Site 2 <<
	.long	.Ltmp577-.Ltmp576       #   Call between .Ltmp576 and .Ltmp577
	.long	.Ltmp578-.Lfunc_begin8  #     jumps to .Ltmp578
	.byte	0                       #   On action: cleanup
	.long	.Ltmp579-.Lfunc_begin8  # >> Call Site 3 <<
	.long	.Ltmp580-.Ltmp579       #   Call between .Ltmp579 and .Ltmp580
	.long	.Ltmp581-.Lfunc_begin8  #     jumps to .Ltmp581
	.byte	0                       #   On action: cleanup
	.long	.Ltmp582-.Lfunc_begin8  # >> Call Site 4 <<
	.long	.Ltmp583-.Ltmp582       #   Call between .Ltmp582 and .Ltmp583
	.long	.Ltmp584-.Lfunc_begin8  #     jumps to .Ltmp584
	.byte	0                       #   On action: cleanup
	.long	.Ltmp585-.Lfunc_begin8  # >> Call Site 5 <<
	.long	.Ltmp586-.Ltmp585       #   Call between .Ltmp585 and .Ltmp586
	.long	.Ltmp587-.Lfunc_begin8  #     jumps to .Ltmp587
	.byte	0                       #   On action: cleanup
	.long	.Ltmp588-.Lfunc_begin8  # >> Call Site 6 <<
	.long	.Ltmp591-.Ltmp588       #   Call between .Ltmp588 and .Ltmp591
	.long	.Ltmp592-.Lfunc_begin8  #     jumps to .Ltmp592
	.byte	0                       #   On action: cleanup
	.long	.Ltmp591-.Lfunc_begin8  # >> Call Site 7 <<
	.long	.Lfunc_end13-.Ltmp591   #   Call between .Ltmp591 and .Lfunc_end13
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CArcItemED2Ev,"axG",@progbits,_ZN13CObjectVectorI8CArcItemED2Ev,comdat
	.weak	_ZN13CObjectVectorI8CArcItemED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CArcItemED2Ev,@function
_ZN13CObjectVectorI8CArcItemED2Ev:      # @_ZN13CObjectVectorI8CArcItemED2Ev
.Lfunc_begin9:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception9
# BB#0:
	pushq	%r14
.Lcfi135:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi136:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi137:
	.cfi_def_cfa_offset 32
.Lcfi138:
	.cfi_offset %rbx, -24
.Lcfi139:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI8CArcItemE+16, (%rbx)
.Ltmp593:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp594:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB14_2:
.Ltmp595:
	movq	%rax, %r14
.Ltmp596:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp597:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB14_4:
.Ltmp598:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end14:
	.size	_ZN13CObjectVectorI8CArcItemED2Ev, .Lfunc_end14-_ZN13CObjectVectorI8CArcItemED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table14:
.Lexception9:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp593-.Lfunc_begin9  # >> Call Site 1 <<
	.long	.Ltmp594-.Ltmp593       #   Call between .Ltmp593 and .Ltmp594
	.long	.Ltmp595-.Lfunc_begin9  #     jumps to .Ltmp595
	.byte	0                       #   On action: cleanup
	.long	.Ltmp594-.Lfunc_begin9  # >> Call Site 2 <<
	.long	.Ltmp596-.Ltmp594       #   Call between .Ltmp594 and .Ltmp596
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp596-.Lfunc_begin9  # >> Call Site 3 <<
	.long	.Ltmp597-.Ltmp596       #   Call between .Ltmp596 and .Ltmp597
	.long	.Ltmp598-.Lfunc_begin9  #     jumps to .Ltmp598
	.byte	1                       #   On action: 1
	.long	.Ltmp597-.Lfunc_begin9  # >> Call Site 4 <<
	.long	.Lfunc_end14-.Ltmp597   #   Call between .Ltmp597 and .Lfunc_end14
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN10CTempFilesD2Ev,"axG",@progbits,_ZN10CTempFilesD2Ev,comdat
	.weak	_ZN10CTempFilesD2Ev
	.p2align	4, 0x90
	.type	_ZN10CTempFilesD2Ev,@function
_ZN10CTempFilesD2Ev:                    # @_ZN10CTempFilesD2Ev
.Lfunc_begin10:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception10
# BB#0:
	pushq	%r14
.Lcfi140:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi141:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi142:
	.cfi_def_cfa_offset 32
.Lcfi143:
	.cfi_offset %rbx, -24
.Lcfi144:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp599:
	callq	_ZN10CTempFiles5ClearEv
.Ltmp600:
# BB#1:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp611:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp612:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB15_3:
.Ltmp613:
	movq	%rax, %r14
.Ltmp614:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp615:
	jmp	.LBB15_4
.LBB15_5:
.Ltmp616:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB15_6:
.Ltmp601:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp602:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp603:
# BB#7:
.Ltmp608:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp609:
.LBB15_4:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB15_10:
.Ltmp610:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB15_8:
.Ltmp604:
	movq	%rax, %r14
.Ltmp605:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp606:
# BB#11:                                # %.body
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB15_9:
.Ltmp607:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end15:
	.size	_ZN10CTempFilesD2Ev, .Lfunc_end15-_ZN10CTempFilesD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table15:
.Lexception10:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\360"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	104                     # Call site table length
	.long	.Ltmp599-.Lfunc_begin10 # >> Call Site 1 <<
	.long	.Ltmp600-.Ltmp599       #   Call between .Ltmp599 and .Ltmp600
	.long	.Ltmp601-.Lfunc_begin10 #     jumps to .Ltmp601
	.byte	0                       #   On action: cleanup
	.long	.Ltmp611-.Lfunc_begin10 # >> Call Site 2 <<
	.long	.Ltmp612-.Ltmp611       #   Call between .Ltmp611 and .Ltmp612
	.long	.Ltmp613-.Lfunc_begin10 #     jumps to .Ltmp613
	.byte	0                       #   On action: cleanup
	.long	.Ltmp612-.Lfunc_begin10 # >> Call Site 3 <<
	.long	.Ltmp614-.Ltmp612       #   Call between .Ltmp612 and .Ltmp614
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp614-.Lfunc_begin10 # >> Call Site 4 <<
	.long	.Ltmp615-.Ltmp614       #   Call between .Ltmp614 and .Ltmp615
	.long	.Ltmp616-.Lfunc_begin10 #     jumps to .Ltmp616
	.byte	1                       #   On action: 1
	.long	.Ltmp602-.Lfunc_begin10 # >> Call Site 5 <<
	.long	.Ltmp603-.Ltmp602       #   Call between .Ltmp602 and .Ltmp603
	.long	.Ltmp604-.Lfunc_begin10 #     jumps to .Ltmp604
	.byte	1                       #   On action: 1
	.long	.Ltmp608-.Lfunc_begin10 # >> Call Site 6 <<
	.long	.Ltmp609-.Ltmp608       #   Call between .Ltmp608 and .Ltmp609
	.long	.Ltmp610-.Lfunc_begin10 #     jumps to .Ltmp610
	.byte	1                       #   On action: 1
	.long	.Ltmp609-.Lfunc_begin10 # >> Call Site 7 <<
	.long	.Ltmp605-.Ltmp609       #   Call between .Ltmp609 and .Ltmp605
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp605-.Lfunc_begin10 # >> Call Site 8 <<
	.long	.Ltmp606-.Ltmp605       #   Call between .Ltmp605 and .Ltmp606
	.long	.Ltmp607-.Lfunc_begin10 #     jumps to .Ltmp607
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN9CDirItemsD2Ev,"axG",@progbits,_ZN9CDirItemsD2Ev,comdat
	.weak	_ZN9CDirItemsD2Ev
	.p2align	4, 0x90
	.type	_ZN9CDirItemsD2Ev,@function
_ZN9CDirItemsD2Ev:                      # @_ZN9CDirItemsD2Ev
.Lfunc_begin11:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception11
# BB#0:
	pushq	%r15
.Lcfi145:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi146:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi147:
	.cfi_def_cfa_offset 32
.Lcfi148:
	.cfi_offset %rbx, -32
.Lcfi149:
	.cfi_offset %r14, -24
.Lcfi150:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
	leaq	96(%r15), %rbx
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, 96(%r15)
.Ltmp617:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp618:
# BB#1:
.Ltmp623:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp624:
# BB#2:                                 # %_ZN13CObjectVectorI8CDirItemED2Ev.exit
	leaq	64(%r15), %rdi
.Ltmp628:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp629:
# BB#3:
	leaq	32(%r15), %rdi
.Ltmp633:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp634:
# BB#4:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp645:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp646:
# BB#5:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB16_8:
.Ltmp647:
	movq	%rax, %r14
.Ltmp648:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp649:
	jmp	.LBB16_9
.LBB16_10:
.Ltmp650:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_13:
.Ltmp635:
	movq	%rax, %r14
	jmp	.LBB16_16
.LBB16_14:
.Ltmp630:
	movq	%rax, %r14
	jmp	.LBB16_15
.LBB16_11:
.Ltmp625:
	movq	%rax, %r14
	jmp	.LBB16_12
.LBB16_6:
.Ltmp619:
	movq	%rax, %r14
.Ltmp620:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp621:
.LBB16_12:                              # %.body
	leaq	64(%r15), %rdi
.Ltmp626:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp627:
.LBB16_15:
	leaq	32(%r15), %rdi
.Ltmp631:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp632:
.LBB16_16:
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%r15)
.Ltmp636:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp637:
# BB#17:
.Ltmp642:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp643:
.LBB16_9:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB16_7:
.Ltmp622:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_18:
.Ltmp638:
	movq	%rax, %rbx
.Ltmp639:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp640:
	jmp	.LBB16_21
.LBB16_19:
.Ltmp641:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB16_20:
.Ltmp644:
	movq	%rax, %rbx
.LBB16_21:                              # %.body4
	movq	%rbx, %rdi
	callq	__clang_call_terminate
.Lfunc_end16:
	.size	_ZN9CDirItemsD2Ev, .Lfunc_end16-_ZN9CDirItemsD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table16:
.Lexception11:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\262\201\200"          # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\251\001"              # Call site table length
	.long	.Ltmp617-.Lfunc_begin11 # >> Call Site 1 <<
	.long	.Ltmp618-.Ltmp617       #   Call between .Ltmp617 and .Ltmp618
	.long	.Ltmp619-.Lfunc_begin11 #     jumps to .Ltmp619
	.byte	0                       #   On action: cleanup
	.long	.Ltmp623-.Lfunc_begin11 # >> Call Site 2 <<
	.long	.Ltmp624-.Ltmp623       #   Call between .Ltmp623 and .Ltmp624
	.long	.Ltmp625-.Lfunc_begin11 #     jumps to .Ltmp625
	.byte	0                       #   On action: cleanup
	.long	.Ltmp628-.Lfunc_begin11 # >> Call Site 3 <<
	.long	.Ltmp629-.Ltmp628       #   Call between .Ltmp628 and .Ltmp629
	.long	.Ltmp630-.Lfunc_begin11 #     jumps to .Ltmp630
	.byte	0                       #   On action: cleanup
	.long	.Ltmp633-.Lfunc_begin11 # >> Call Site 4 <<
	.long	.Ltmp634-.Ltmp633       #   Call between .Ltmp633 and .Ltmp634
	.long	.Ltmp635-.Lfunc_begin11 #     jumps to .Ltmp635
	.byte	0                       #   On action: cleanup
	.long	.Ltmp645-.Lfunc_begin11 # >> Call Site 5 <<
	.long	.Ltmp646-.Ltmp645       #   Call between .Ltmp645 and .Ltmp646
	.long	.Ltmp647-.Lfunc_begin11 #     jumps to .Ltmp647
	.byte	0                       #   On action: cleanup
	.long	.Ltmp646-.Lfunc_begin11 # >> Call Site 6 <<
	.long	.Ltmp648-.Ltmp646       #   Call between .Ltmp646 and .Ltmp648
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp648-.Lfunc_begin11 # >> Call Site 7 <<
	.long	.Ltmp649-.Ltmp648       #   Call between .Ltmp648 and .Ltmp649
	.long	.Ltmp650-.Lfunc_begin11 #     jumps to .Ltmp650
	.byte	1                       #   On action: 1
	.long	.Ltmp620-.Lfunc_begin11 # >> Call Site 8 <<
	.long	.Ltmp621-.Ltmp620       #   Call between .Ltmp620 and .Ltmp621
	.long	.Ltmp622-.Lfunc_begin11 #     jumps to .Ltmp622
	.byte	1                       #   On action: 1
	.long	.Ltmp626-.Lfunc_begin11 # >> Call Site 9 <<
	.long	.Ltmp632-.Ltmp626       #   Call between .Ltmp626 and .Ltmp632
	.long	.Ltmp644-.Lfunc_begin11 #     jumps to .Ltmp644
	.byte	1                       #   On action: 1
	.long	.Ltmp636-.Lfunc_begin11 # >> Call Site 10 <<
	.long	.Ltmp637-.Ltmp636       #   Call between .Ltmp636 and .Ltmp637
	.long	.Ltmp638-.Lfunc_begin11 #     jumps to .Ltmp638
	.byte	1                       #   On action: 1
	.long	.Ltmp642-.Lfunc_begin11 # >> Call Site 11 <<
	.long	.Ltmp643-.Ltmp642       #   Call between .Ltmp642 and .Ltmp643
	.long	.Ltmp644-.Lfunc_begin11 #     jumps to .Ltmp644
	.byte	1                       #   On action: 1
	.long	.Ltmp643-.Lfunc_begin11 # >> Call Site 12 <<
	.long	.Ltmp639-.Ltmp643       #   Call between .Ltmp643 and .Ltmp639
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp639-.Lfunc_begin11 # >> Call Site 13 <<
	.long	.Ltmp640-.Ltmp639       #   Call between .Ltmp639 and .Ltmp640
	.long	.Ltmp641-.Lfunc_begin11 #     jumps to .Ltmp641
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN12CArchiveLinkD2Ev,"axG",@progbits,_ZN12CArchiveLinkD2Ev,comdat
	.weak	_ZN12CArchiveLinkD2Ev
	.p2align	4, 0x90
	.type	_ZN12CArchiveLinkD2Ev,@function
_ZN12CArchiveLinkD2Ev:                  # @_ZN12CArchiveLinkD2Ev
.Lfunc_begin12:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception12
# BB#0:
	pushq	%r15
.Lcfi151:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi152:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi153:
	.cfi_def_cfa_offset 32
.Lcfi154:
	.cfi_offset %rbx, -32
.Lcfi155:
	.cfi_offset %r14, -24
.Lcfi156:
	.cfi_offset %r15, -16
	movq	%rdi, %r15
.Ltmp651:
	callq	_ZN12CArchiveLink7ReleaseEv
.Ltmp652:
# BB#1:
	leaq	32(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%r15)
.Ltmp662:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp663:
# BB#2:
.Ltmp668:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp669:
# BB#3:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%r15)
.Ltmp680:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp681:
# BB#4:                                 # %_ZN13CObjectVectorI4CArcED2Ev.exit
	movq	%r15, %rdi
	popq	%rbx
	popq	%r14
	popq	%r15
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB17_7:
.Ltmp682:
	movq	%rax, %r14
.Ltmp683:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp684:
	jmp	.LBB17_8
.LBB17_9:
.Ltmp685:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB17_14:
.Ltmp670:
	movq	%rax, %r14
	jmp	.LBB17_15
.LBB17_5:
.Ltmp664:
	movq	%rax, %r14
.Ltmp665:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp666:
	jmp	.LBB17_15
.LBB17_6:
.Ltmp667:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB17_10:
.Ltmp653:
	movq	%rax, %r14
	leaq	32(%r15), %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, 32(%r15)
.Ltmp654:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp655:
# BB#11:
.Ltmp660:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp661:
.LBB17_15:                              # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit5
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%r15)
.Ltmp671:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp672:
# BB#16:
.Ltmp677:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp678:
.LBB17_8:                               # %unwind_resume
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB17_12:
.Ltmp656:
	movq	%rax, %r14
.Ltmp657:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp658:
	jmp	.LBB17_20
.LBB17_13:
.Ltmp659:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB17_17:
.Ltmp673:
	movq	%rax, %r14
.Ltmp674:
	movq	%r15, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp675:
	jmp	.LBB17_20
.LBB17_18:
.Ltmp676:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB17_19:
.Ltmp679:
	movq	%rax, %r14
.LBB17_20:                              # %.body3
	movq	%r14, %rdi
	callq	__clang_call_terminate
.Lfunc_end17:
	.size	_ZN12CArchiveLinkD2Ev, .Lfunc_end17-_ZN12CArchiveLinkD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table17:
.Lexception12:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\277\201"              # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.ascii	"\266\001"              # Call site table length
	.long	.Ltmp651-.Lfunc_begin12 # >> Call Site 1 <<
	.long	.Ltmp652-.Ltmp651       #   Call between .Ltmp651 and .Ltmp652
	.long	.Ltmp653-.Lfunc_begin12 #     jumps to .Ltmp653
	.byte	0                       #   On action: cleanup
	.long	.Ltmp662-.Lfunc_begin12 # >> Call Site 2 <<
	.long	.Ltmp663-.Ltmp662       #   Call between .Ltmp662 and .Ltmp663
	.long	.Ltmp664-.Lfunc_begin12 #     jumps to .Ltmp664
	.byte	0                       #   On action: cleanup
	.long	.Ltmp668-.Lfunc_begin12 # >> Call Site 3 <<
	.long	.Ltmp669-.Ltmp668       #   Call between .Ltmp668 and .Ltmp669
	.long	.Ltmp670-.Lfunc_begin12 #     jumps to .Ltmp670
	.byte	0                       #   On action: cleanup
	.long	.Ltmp680-.Lfunc_begin12 # >> Call Site 4 <<
	.long	.Ltmp681-.Ltmp680       #   Call between .Ltmp680 and .Ltmp681
	.long	.Ltmp682-.Lfunc_begin12 #     jumps to .Ltmp682
	.byte	0                       #   On action: cleanup
	.long	.Ltmp681-.Lfunc_begin12 # >> Call Site 5 <<
	.long	.Ltmp683-.Ltmp681       #   Call between .Ltmp681 and .Ltmp683
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp683-.Lfunc_begin12 # >> Call Site 6 <<
	.long	.Ltmp684-.Ltmp683       #   Call between .Ltmp683 and .Ltmp684
	.long	.Ltmp685-.Lfunc_begin12 #     jumps to .Ltmp685
	.byte	1                       #   On action: 1
	.long	.Ltmp665-.Lfunc_begin12 # >> Call Site 7 <<
	.long	.Ltmp666-.Ltmp665       #   Call between .Ltmp665 and .Ltmp666
	.long	.Ltmp667-.Lfunc_begin12 #     jumps to .Ltmp667
	.byte	1                       #   On action: 1
	.long	.Ltmp654-.Lfunc_begin12 # >> Call Site 8 <<
	.long	.Ltmp655-.Ltmp654       #   Call between .Ltmp654 and .Ltmp655
	.long	.Ltmp656-.Lfunc_begin12 #     jumps to .Ltmp656
	.byte	1                       #   On action: 1
	.long	.Ltmp660-.Lfunc_begin12 # >> Call Site 9 <<
	.long	.Ltmp661-.Ltmp660       #   Call between .Ltmp660 and .Ltmp661
	.long	.Ltmp679-.Lfunc_begin12 #     jumps to .Ltmp679
	.byte	1                       #   On action: 1
	.long	.Ltmp671-.Lfunc_begin12 # >> Call Site 10 <<
	.long	.Ltmp672-.Ltmp671       #   Call between .Ltmp671 and .Ltmp672
	.long	.Ltmp673-.Lfunc_begin12 #     jumps to .Ltmp673
	.byte	1                       #   On action: 1
	.long	.Ltmp677-.Lfunc_begin12 # >> Call Site 11 <<
	.long	.Ltmp678-.Ltmp677       #   Call between .Ltmp677 and .Ltmp678
	.long	.Ltmp679-.Lfunc_begin12 #     jumps to .Ltmp679
	.byte	1                       #   On action: 1
	.long	.Ltmp678-.Lfunc_begin12 # >> Call Site 12 <<
	.long	.Ltmp657-.Ltmp678       #   Call between .Ltmp678 and .Ltmp657
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp657-.Lfunc_begin12 # >> Call Site 13 <<
	.long	.Ltmp658-.Ltmp657       #   Call between .Ltmp657 and .Ltmp658
	.long	.Ltmp659-.Lfunc_begin12 #     jumps to .Ltmp659
	.byte	1                       #   On action: 1
	.long	.Ltmp674-.Lfunc_begin12 # >> Call Site 14 <<
	.long	.Ltmp675-.Ltmp674       #   Call between .Ltmp674 and .Ltmp675
	.long	.Ltmp676-.Lfunc_begin12 #     jumps to .Ltmp676
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv,"axG",@progbits,_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv,comdat
	.weak	_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv,@function
_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv: # @_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi157:
	.cfi_def_cfa_offset 16
	movb	(%rsi), %cl
	cmpb	IID_IUnknown(%rip), %cl
	jne	.LBB18_16
# BB#1:
	movb	1(%rsi), %al
	cmpb	IID_IUnknown+1(%rip), %al
	jne	.LBB18_16
# BB#2:
	movb	2(%rsi), %al
	cmpb	IID_IUnknown+2(%rip), %al
	jne	.LBB18_16
# BB#3:
	movb	3(%rsi), %al
	cmpb	IID_IUnknown+3(%rip), %al
	jne	.LBB18_16
# BB#4:
	movb	4(%rsi), %al
	cmpb	IID_IUnknown+4(%rip), %al
	jne	.LBB18_16
# BB#5:
	movb	5(%rsi), %al
	cmpb	IID_IUnknown+5(%rip), %al
	jne	.LBB18_16
# BB#6:
	movb	6(%rsi), %al
	cmpb	IID_IUnknown+6(%rip), %al
	jne	.LBB18_16
# BB#7:
	movb	7(%rsi), %al
	cmpb	IID_IUnknown+7(%rip), %al
	jne	.LBB18_16
# BB#8:
	movb	8(%rsi), %al
	cmpb	IID_IUnknown+8(%rip), %al
	jne	.LBB18_16
# BB#9:
	movb	9(%rsi), %al
	cmpb	IID_IUnknown+9(%rip), %al
	jne	.LBB18_16
# BB#10:
	movb	10(%rsi), %al
	cmpb	IID_IUnknown+10(%rip), %al
	jne	.LBB18_16
# BB#11:
	movb	11(%rsi), %al
	cmpb	IID_IUnknown+11(%rip), %al
	jne	.LBB18_16
# BB#12:
	movb	12(%rsi), %al
	cmpb	IID_IUnknown+12(%rip), %al
	jne	.LBB18_16
# BB#13:
	movb	13(%rsi), %al
	cmpb	IID_IUnknown+13(%rip), %al
	jne	.LBB18_16
# BB#14:
	movb	14(%rsi), %al
	cmpb	IID_IUnknown+14(%rip), %al
	jne	.LBB18_16
# BB#15:                                # %_ZeqRK4GUIDS1_.exit
	movb	15(%rsi), %al
	cmpb	IID_IUnknown+15(%rip), %al
	je	.LBB18_32
.LBB18_16:                              # %_ZeqRK4GUIDS1_.exit.thread
	movl	$-2147467262, %eax      # imm = 0x80004002
	cmpb	IID_IOutStream(%rip), %cl
	jne	.LBB18_33
# BB#17:
	movb	1(%rsi), %cl
	cmpb	IID_IOutStream+1(%rip), %cl
	jne	.LBB18_33
# BB#18:
	movb	2(%rsi), %cl
	cmpb	IID_IOutStream+2(%rip), %cl
	jne	.LBB18_33
# BB#19:
	movb	3(%rsi), %cl
	cmpb	IID_IOutStream+3(%rip), %cl
	jne	.LBB18_33
# BB#20:
	movb	4(%rsi), %cl
	cmpb	IID_IOutStream+4(%rip), %cl
	jne	.LBB18_33
# BB#21:
	movb	5(%rsi), %cl
	cmpb	IID_IOutStream+5(%rip), %cl
	jne	.LBB18_33
# BB#22:
	movb	6(%rsi), %cl
	cmpb	IID_IOutStream+6(%rip), %cl
	jne	.LBB18_33
# BB#23:
	movb	7(%rsi), %cl
	cmpb	IID_IOutStream+7(%rip), %cl
	jne	.LBB18_33
# BB#24:
	movb	8(%rsi), %cl
	cmpb	IID_IOutStream+8(%rip), %cl
	jne	.LBB18_33
# BB#25:
	movb	9(%rsi), %cl
	cmpb	IID_IOutStream+9(%rip), %cl
	jne	.LBB18_33
# BB#26:
	movb	10(%rsi), %cl
	cmpb	IID_IOutStream+10(%rip), %cl
	jne	.LBB18_33
# BB#27:
	movb	11(%rsi), %cl
	cmpb	IID_IOutStream+11(%rip), %cl
	jne	.LBB18_33
# BB#28:
	movb	12(%rsi), %cl
	cmpb	IID_IOutStream+12(%rip), %cl
	jne	.LBB18_33
# BB#29:
	movb	13(%rsi), %cl
	cmpb	IID_IOutStream+13(%rip), %cl
	jne	.LBB18_33
# BB#30:
	movb	14(%rsi), %cl
	cmpb	IID_IOutStream+14(%rip), %cl
	jne	.LBB18_33
# BB#31:                                # %_ZeqRK4GUIDS1_.exit6
	movb	15(%rsi), %cl
	cmpb	IID_IOutStream+15(%rip), %cl
	jne	.LBB18_33
.LBB18_32:
	movq	%rdi, (%rdx)
	movq	(%rdi), %rax
	callq	*8(%rax)
	xorl	%eax, %eax
.LBB18_33:                              # %_ZeqRK4GUIDS1_.exit6.thread
	popq	%rcx
	retq
.Lfunc_end18:
	.size	_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv, .Lfunc_end18-_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv
	.cfi_endproc

	.section	.text._ZN18COutMultiVolStream6AddRefEv,"axG",@progbits,_ZN18COutMultiVolStream6AddRefEv,comdat
	.weak	_ZN18COutMultiVolStream6AddRefEv
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStream6AddRefEv,@function
_ZN18COutMultiVolStream6AddRefEv:       # @_ZN18COutMultiVolStream6AddRefEv
	.cfi_startproc
# BB#0:
	movl	8(%rdi), %eax
	incl	%eax
	movl	%eax, 8(%rdi)
	retq
.Lfunc_end19:
	.size	_ZN18COutMultiVolStream6AddRefEv, .Lfunc_end19-_ZN18COutMultiVolStream6AddRefEv
	.cfi_endproc

	.section	.text._ZN18COutMultiVolStream7ReleaseEv,"axG",@progbits,_ZN18COutMultiVolStream7ReleaseEv,comdat
	.weak	_ZN18COutMultiVolStream7ReleaseEv
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStream7ReleaseEv,@function
_ZN18COutMultiVolStream7ReleaseEv:      # @_ZN18COutMultiVolStream7ReleaseEv
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi158:
	.cfi_def_cfa_offset 16
	movl	8(%rdi), %eax
	decl	%eax
	movl	%eax, 8(%rdi)
	jne	.LBB20_2
# BB#1:
	movq	(%rdi), %rax
	callq	*32(%rax)
	xorl	%eax, %eax
.LBB20_2:
	popq	%rcx
	retq
.Lfunc_end20:
	.size	_ZN18COutMultiVolStream7ReleaseEv, .Lfunc_end20-_ZN18COutMultiVolStream7ReleaseEv
	.cfi_endproc

	.section	.text._ZN18COutMultiVolStreamD2Ev,"axG",@progbits,_ZN18COutMultiVolStreamD2Ev,comdat
	.weak	_ZN18COutMultiVolStreamD2Ev
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStreamD2Ev,@function
_ZN18COutMultiVolStreamD2Ev:            # @_ZN18COutMultiVolStreamD2Ev
.Lfunc_begin13:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception13
# BB#0:
	pushq	%r14
.Lcfi159:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi160:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi161:
	.cfi_def_cfa_offset 32
.Lcfi162:
	.cfi_offset %rbx, -24
.Lcfi163:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV18COutMultiVolStream+16, (%rbx)
	movq	104(%rbx), %rdi
	testq	%rdi, %rdi
	je	.LBB21_2
# BB#1:
	callq	_ZdaPv
.LBB21_2:                               # %_ZN11CStringBaseIwED2Ev.exit
	leaq	72(%rbx), %rdi
.Ltmp686:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp687:
# BB#3:
	movq	$_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE+16, 40(%rbx)
	addq	$40, %rbx
.Ltmp698:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp699:
# BB#4:
.Ltmp704:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp705:
# BB#5:                                 # %_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev.exit
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB21_12:
.Ltmp706:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_6:
.Ltmp700:
	movq	%rax, %r14
.Ltmp701:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp702:
	jmp	.LBB21_13
.LBB21_7:
.Ltmp703:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.LBB21_8:
.Ltmp688:
	movq	%rax, %r14
	movq	$_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE+16, 40(%rbx)
	addq	$40, %rbx
.Ltmp689:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp690:
# BB#9:
.Ltmp695:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp696:
.LBB21_13:                              # %_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev.exit5
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB21_14:
.Ltmp697:
	movq	%rax, %r14
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB21_10:
.Ltmp691:
	movq	%rax, %r14
.Ltmp692:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp693:
# BB#15:                                # %.body3
	movq	%r14, %rdi
	callq	__clang_call_terminate
.LBB21_11:
.Ltmp694:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end21:
	.size	_ZN18COutMultiVolStreamD2Ev, .Lfunc_end21-_ZN18COutMultiVolStreamD2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table21:
.Lexception13:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	125                     # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	117                     # Call site table length
	.long	.Ltmp686-.Lfunc_begin13 # >> Call Site 1 <<
	.long	.Ltmp687-.Ltmp686       #   Call between .Ltmp686 and .Ltmp687
	.long	.Ltmp688-.Lfunc_begin13 #     jumps to .Ltmp688
	.byte	0                       #   On action: cleanup
	.long	.Ltmp698-.Lfunc_begin13 # >> Call Site 2 <<
	.long	.Ltmp699-.Ltmp698       #   Call between .Ltmp698 and .Ltmp699
	.long	.Ltmp700-.Lfunc_begin13 #     jumps to .Ltmp700
	.byte	0                       #   On action: cleanup
	.long	.Ltmp704-.Lfunc_begin13 # >> Call Site 3 <<
	.long	.Ltmp705-.Ltmp704       #   Call between .Ltmp704 and .Ltmp705
	.long	.Ltmp706-.Lfunc_begin13 #     jumps to .Ltmp706
	.byte	0                       #   On action: cleanup
	.long	.Ltmp705-.Lfunc_begin13 # >> Call Site 4 <<
	.long	.Ltmp701-.Ltmp705       #   Call between .Ltmp705 and .Ltmp701
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp701-.Lfunc_begin13 # >> Call Site 5 <<
	.long	.Ltmp702-.Ltmp701       #   Call between .Ltmp701 and .Ltmp702
	.long	.Ltmp703-.Lfunc_begin13 #     jumps to .Ltmp703
	.byte	1                       #   On action: 1
	.long	.Ltmp689-.Lfunc_begin13 # >> Call Site 6 <<
	.long	.Ltmp690-.Ltmp689       #   Call between .Ltmp689 and .Ltmp690
	.long	.Ltmp691-.Lfunc_begin13 #     jumps to .Ltmp691
	.byte	1                       #   On action: 1
	.long	.Ltmp695-.Lfunc_begin13 # >> Call Site 7 <<
	.long	.Ltmp696-.Ltmp695       #   Call between .Ltmp695 and .Ltmp696
	.long	.Ltmp697-.Lfunc_begin13 #     jumps to .Ltmp697
	.byte	1                       #   On action: 1
	.long	.Ltmp696-.Lfunc_begin13 # >> Call Site 8 <<
	.long	.Ltmp692-.Ltmp696       #   Call between .Ltmp696 and .Ltmp692
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp692-.Lfunc_begin13 # >> Call Site 9 <<
	.long	.Ltmp693-.Ltmp692       #   Call between .Ltmp692 and .Ltmp693
	.long	.Ltmp694-.Lfunc_begin13 #     jumps to .Ltmp694
	.byte	1                       #   On action: 1
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN18COutMultiVolStreamD0Ev,"axG",@progbits,_ZN18COutMultiVolStreamD0Ev,comdat
	.weak	_ZN18COutMultiVolStreamD0Ev
	.p2align	4, 0x90
	.type	_ZN18COutMultiVolStreamD0Ev,@function
_ZN18COutMultiVolStreamD0Ev:            # @_ZN18COutMultiVolStreamD0Ev
.Lfunc_begin14:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception14
# BB#0:
	pushq	%r14
.Lcfi164:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi165:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi166:
	.cfi_def_cfa_offset 32
.Lcfi167:
	.cfi_offset %rbx, -24
.Lcfi168:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp707:
	callq	_ZN18COutMultiVolStreamD2Ev
.Ltmp708:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB22_2:
.Ltmp709:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end22:
	.size	_ZN18COutMultiVolStreamD0Ev, .Lfunc_end22-_ZN18COutMultiVolStreamD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table22:
.Lexception14:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp707-.Lfunc_begin14 # >> Call Site 1 <<
	.long	.Ltmp708-.Ltmp707       #   Call between .Ltmp707 and .Ltmp708
	.long	.Ltmp709-.Lfunc_begin14 #     jumps to .Ltmp709
	.byte	0                       #   On action: cleanup
	.long	.Ltmp708-.Lfunc_begin14 # >> Call Site 2 <<
	.long	.Lfunc_end22-.Ltmp708   #   Call between .Ltmp708 and .Lfunc_end22
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO8COutFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO8COutFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO8COutFileD0Ev,@function
_ZN8NWindows5NFile3NIO8COutFileD0Ev:    # @_ZN8NWindows5NFile3NIO8COutFileD0Ev
.Lfunc_begin15:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception15
# BB#0:
	pushq	%r14
.Lcfi169:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi170:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi171:
	.cfi_def_cfa_offset 32
.Lcfi172:
	.cfi_offset %rbx, -24
.Lcfi173:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp710:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp711:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB23_2:
.Ltmp712:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end23:
	.size	_ZN8NWindows5NFile3NIO8COutFileD0Ev, .Lfunc_end23-_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table23:
.Lexception15:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp710-.Lfunc_begin15 # >> Call Site 1 <<
	.long	.Ltmp711-.Ltmp710       #   Call between .Ltmp710 and .Ltmp711
	.long	.Ltmp712-.Lfunc_begin15 #     jumps to .Ltmp712
	.byte	0                       #   On action: cleanup
	.long	.Ltmp711-.Lfunc_begin15 # >> Call Site 2 <<
	.long	.Lfunc_end23-.Ltmp711   #   Call between .Ltmp711 and .Lfunc_end23
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZNK11CStringBaseIwE3MidEii,"axG",@progbits,_ZNK11CStringBaseIwE3MidEii,comdat
	.weak	_ZNK11CStringBaseIwE3MidEii
	.p2align	4, 0x90
	.type	_ZNK11CStringBaseIwE3MidEii,@function
_ZNK11CStringBaseIwE3MidEii:            # @_ZNK11CStringBaseIwE3MidEii
.Lfunc_begin16:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception16
# BB#0:
	pushq	%rbp
.Lcfi174:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi175:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi176:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi177:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi178:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi179:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi180:
	.cfi_def_cfa_offset 64
.Lcfi181:
	.cfi_offset %rbx, -56
.Lcfi182:
	.cfi_offset %r12, -48
.Lcfi183:
	.cfi_offset %r13, -40
.Lcfi184:
	.cfi_offset %r14, -32
.Lcfi185:
	.cfi_offset %r15, -24
.Lcfi186:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movl	%edx, %r12d
	movq	%rsi, %r15
	movq	%rdi, %r13
	leal	(%rcx,%r12), %eax
	movl	8(%r15), %ebp
	movl	%ebp, %r14d
	subl	%r12d, %r14d
	cmpl	%ebp, %eax
	cmovlel	%ecx, %r14d
	testl	%r12d, %r12d
	jne	.LBB24_9
# BB#1:
	leal	(%r14,%r12), %eax
	cmpl	%ebp, %eax
	jne	.LBB24_9
# BB#2:
	movslq	%ebp, %rbx
	xorps	%xmm0, %xmm0
	movups	%xmm0, (%r13)
	incq	%rbx
	testl	%ebx, %ebx
	je	.LBB24_3
# BB#4:                                 # %._crit_edge16.i.i
	movl	$4, %ecx
	movq	%rbx, %rax
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, (%r13)
	movl	$0, (%rax)
	movl	%ebx, 12(%r13)
	jmp	.LBB24_5
.LBB24_9:
	movq	%r13, (%rsp)            # 8-byte Spill
	movl	$16, %edi
	callq	_Znam
	movq	%rax, %r13
	movl	$0, (%r13)
	leal	1(%r14), %ebp
	cmpl	$4, %ebp
	je	.LBB24_13
# BB#10:
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp713:
	callq	_Znam
	movq	%rax, %rbx
.Ltmp714:
# BB#11:                                # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	movq	%r13, %rdi
	callq	_ZdaPv
	movl	$0, (%rbx)
	testl	%r14d, %r14d
	jle	.LBB24_35
# BB#12:
	movq	%rbx, %r13
.LBB24_13:                              # %.lr.ph
	movq	(%r15), %r8
	movslq	%r12d, %rdx
	movslq	%r14d, %rax
	testq	%rax, %rax
	movl	$1, %edi
	cmovgq	%rax, %rdi
	cmpq	$7, %rdi
	jbe	.LBB24_14
# BB#17:                                # %min.iters.checked
	movabsq	$9223372036854775800, %rsi # imm = 0x7FFFFFFFFFFFFFF8
	andq	%rdi, %rsi
	je	.LBB24_14
# BB#18:                                # %vector.memcheck
	movl	%ebp, %r10d
	testq	%rax, %rax
	movl	$1, %ecx
	cmovgq	%rax, %rcx
	leaq	(%rcx,%rdx), %rbp
	leaq	(%r8,%rbp,4), %rbp
	cmpq	%rbp, %r13
	jae	.LBB24_21
# BB#19:                                # %vector.memcheck
	leaq	(%r13,%rcx,4), %rcx
	leaq	(%r8,%rdx,4), %rbp
	cmpq	%rcx, %rbp
	jae	.LBB24_21
# BB#20:
	xorl	%esi, %esi
	movl	%r10d, %ebp
	jmp	.LBB24_15
.LBB24_14:
	xorl	%esi, %esi
.LBB24_15:                              # %scalar.ph.preheader
	leaq	(%r8,%rdx,4), %rcx
	.p2align	4, 0x90
.LBB24_16:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rcx,%rsi,4), %edx
	movl	%edx, (%r13,%rsi,4)
	incq	%rsi
	cmpq	%rax, %rsi
	jl	.LBB24_16
.LBB24_29:
	movq	%r13, %rbx
.LBB24_30:                              # %._crit_edge16.i.i20
	movl	$0, (%rbx,%rax,4)
	xorps	%xmm0, %xmm0
	movq	(%rsp), %rax            # 8-byte Reload
	movups	%xmm0, (%rax)
	movslq	%ebp, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
.Ltmp715:
	callq	_Znam
	movq	%rax, %rcx
.Ltmp716:
# BB#31:                                # %.noexc24
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rcx, (%rax)
	movl	$0, (%rcx)
	movl	%ebp, 12(%rax)
	.p2align	4, 0x90
.LBB24_32:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i21
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rbx), %eax
	addq	$4, %rbx
	movl	%eax, (%rcx)
	addq	$4, %rcx
	testl	%eax, %eax
	jne	.LBB24_32
# BB#33:                                # %_ZN11CStringBaseIwED2Ev.exit26
	movq	(%rsp), %rbx            # 8-byte Reload
	movl	%r14d, 8(%rbx)
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rax
	jmp	.LBB24_8
.LBB24_3:
	xorl	%eax, %eax
.LBB24_5:                               # %_ZN11CStringBaseIwE11SetCapacityEi.exit.i
	movq	(%r15), %rcx
	.p2align	4, 0x90
.LBB24_6:                               # =>This Inner Loop Header: Depth=1
	movl	(%rcx), %edx
	addq	$4, %rcx
	movl	%edx, (%rax)
	addq	$4, %rax
	testl	%edx, %edx
	jne	.LBB24_6
# BB#7:
	movl	%ebp, 8(%r13)
	movq	%r13, %rax
.LBB24_8:
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB24_35:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit.._crit_edge16.i.i20_crit_edge
	movslq	%r14d, %rax
	movq	%rbx, %r13
	jmp	.LBB24_30
.LBB24_21:                              # %vector.body.preheader
	leaq	-8(%rsi), %r9
	movl	%r9d, %ebp
	shrl	$3, %ebp
	incl	%ebp
	andq	$3, %rbp
	je	.LBB24_22
# BB#23:                                # %vector.body.prol.preheader
	leaq	16(%r8,%rdx,4), %rbx
	negq	%rbp
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB24_24:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	-16(%rbx,%rcx,4), %xmm0
	movups	(%rbx,%rcx,4), %xmm1
	movups	%xmm0, (%r13,%rcx,4)
	movups	%xmm1, 16(%r13,%rcx,4)
	addq	$8, %rcx
	incq	%rbp
	jne	.LBB24_24
	jmp	.LBB24_25
.LBB24_22:
	xorl	%ecx, %ecx
.LBB24_25:                              # %vector.body.prol.loopexit
	cmpq	$24, %r9
	jb	.LBB24_28
# BB#26:                                # %vector.body.preheader.new
	movq	%rsi, %rbp
	subq	%rcx, %rbp
	leaq	112(%r13,%rcx,4), %rbx
	addq	%rdx, %rcx
	leaq	112(%r8,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB24_27:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rcx), %xmm0
	movups	-96(%rcx), %xmm1
	movups	%xmm0, -112(%rbx)
	movups	%xmm1, -96(%rbx)
	movups	-80(%rcx), %xmm0
	movups	-64(%rcx), %xmm1
	movups	%xmm0, -80(%rbx)
	movups	%xmm1, -64(%rbx)
	movups	-48(%rcx), %xmm0
	movups	-32(%rcx), %xmm1
	movups	%xmm0, -48(%rbx)
	movups	%xmm1, -32(%rbx)
	movups	-16(%rcx), %xmm0
	movups	(%rcx), %xmm1
	movups	%xmm0, -16(%rbx)
	movups	%xmm1, (%rbx)
	subq	$-128, %rbx
	subq	$-128, %rcx
	addq	$-32, %rbp
	jne	.LBB24_27
.LBB24_28:                              # %middle.block
	cmpq	%rsi, %rdi
	movl	%r10d, %ebp
	jne	.LBB24_15
	jmp	.LBB24_29
.LBB24_34:                              # %_ZN11CStringBaseIwED2Ev.exit
.Ltmp717:
	movq	%rax, %rbx
	movq	%r13, %rdi
	callq	_ZdaPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end24:
	.size	_ZNK11CStringBaseIwE3MidEii, .Lfunc_end24-_ZNK11CStringBaseIwE3MidEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table24:
.Lexception16:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.byte	41                      # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	39                      # Call site table length
	.long	.Lfunc_begin16-.Lfunc_begin16 # >> Call Site 1 <<
	.long	.Ltmp713-.Lfunc_begin16 #   Call between .Lfunc_begin16 and .Ltmp713
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp713-.Lfunc_begin16 # >> Call Site 2 <<
	.long	.Ltmp716-.Ltmp713       #   Call between .Ltmp713 and .Ltmp716
	.long	.Ltmp717-.Lfunc_begin16 #     jumps to .Ltmp717
	.byte	0                       #   On action: cleanup
	.long	.Ltmp716-.Lfunc_begin16 # >> Call Site 3 <<
	.long	.Lfunc_end24-.Ltmp716   #   Call between .Ltmp716 and .Lfunc_end24
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI4CArcED2Ev,"axG",@progbits,_ZN13CObjectVectorI4CArcED2Ev,comdat
	.weak	_ZN13CObjectVectorI4CArcED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI4CArcED2Ev,@function
_ZN13CObjectVectorI4CArcED2Ev:          # @_ZN13CObjectVectorI4CArcED2Ev
.Lfunc_begin17:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception17
# BB#0:
	pushq	%r14
.Lcfi187:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi188:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi189:
	.cfi_def_cfa_offset 32
.Lcfi190:
	.cfi_offset %rbx, -24
.Lcfi191:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%rbx)
.Ltmp718:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp719:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB25_2:
.Ltmp720:
	movq	%rax, %r14
.Ltmp721:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp722:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB25_4:
.Ltmp723:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end25:
	.size	_ZN13CObjectVectorI4CArcED2Ev, .Lfunc_end25-_ZN13CObjectVectorI4CArcED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table25:
.Lexception17:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp718-.Lfunc_begin17 # >> Call Site 1 <<
	.long	.Ltmp719-.Ltmp718       #   Call between .Ltmp718 and .Ltmp719
	.long	.Ltmp720-.Lfunc_begin17 #     jumps to .Ltmp720
	.byte	0                       #   On action: cleanup
	.long	.Ltmp719-.Lfunc_begin17 # >> Call Site 2 <<
	.long	.Ltmp721-.Ltmp719       #   Call between .Ltmp719 and .Ltmp721
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp721-.Lfunc_begin17 # >> Call Site 3 <<
	.long	.Ltmp722-.Ltmp721       #   Call between .Ltmp721 and .Ltmp722
	.long	.Ltmp723-.Lfunc_begin17 #     jumps to .Ltmp723
	.byte	1                       #   On action: 1
	.long	.Ltmp722-.Lfunc_begin17 # >> Call Site 4 <<
	.long	.Lfunc_end25-.Ltmp722   #   Call between .Ltmp722 and .Lfunc_end25
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI4CArcED0Ev,"axG",@progbits,_ZN13CObjectVectorI4CArcED0Ev,comdat
	.weak	_ZN13CObjectVectorI4CArcED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI4CArcED0Ev,@function
_ZN13CObjectVectorI4CArcED0Ev:          # @_ZN13CObjectVectorI4CArcED0Ev
.Lfunc_begin18:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception18
# BB#0:
	pushq	%r14
.Lcfi192:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi193:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi194:
	.cfi_def_cfa_offset 32
.Lcfi195:
	.cfi_offset %rbx, -24
.Lcfi196:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI4CArcE+16, (%rbx)
.Ltmp724:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp725:
# BB#1:
.Ltmp730:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp731:
# BB#2:                                 # %_ZN13CObjectVectorI4CArcED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB26_5:
.Ltmp732:
	movq	%rax, %r14
	jmp	.LBB26_6
.LBB26_3:
.Ltmp726:
	movq	%rax, %r14
.Ltmp727:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp728:
.LBB26_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB26_4:
.Ltmp729:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end26:
	.size	_ZN13CObjectVectorI4CArcED0Ev, .Lfunc_end26-_ZN13CObjectVectorI4CArcED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table26:
.Lexception18:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp724-.Lfunc_begin18 # >> Call Site 1 <<
	.long	.Ltmp725-.Ltmp724       #   Call between .Ltmp724 and .Ltmp725
	.long	.Ltmp726-.Lfunc_begin18 #     jumps to .Ltmp726
	.byte	0                       #   On action: cleanup
	.long	.Ltmp730-.Lfunc_begin18 # >> Call Site 2 <<
	.long	.Ltmp731-.Ltmp730       #   Call between .Ltmp730 and .Ltmp731
	.long	.Ltmp732-.Lfunc_begin18 #     jumps to .Ltmp732
	.byte	0                       #   On action: cleanup
	.long	.Ltmp727-.Lfunc_begin18 # >> Call Site 3 <<
	.long	.Ltmp728-.Ltmp727       #   Call between .Ltmp727 and .Ltmp728
	.long	.Ltmp729-.Lfunc_begin18 #     jumps to .Ltmp729
	.byte	1                       #   On action: 1
	.long	.Ltmp728-.Lfunc_begin18 # >> Call Site 4 <<
	.long	.Lfunc_end26-.Ltmp728   #   Call between .Ltmp728 and .Lfunc_end26
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI4CArcE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI4CArcE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI4CArcE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI4CArcE6DeleteEii,@function
_ZN13CObjectVectorI4CArcE6DeleteEii:    # @_ZN13CObjectVectorI4CArcE6DeleteEii
.Lfunc_begin19:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception19
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi197:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi198:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi199:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi200:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi201:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi202:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi203:
	.cfi_def_cfa_offset 64
.Lcfi204:
	.cfi_offset %rbx, -56
.Lcfi205:
	.cfi_offset %r12, -48
.Lcfi206:
	.cfi_offset %r13, -40
.Lcfi207:
	.cfi_offset %r14, -32
.Lcfi208:
	.cfi_offset %r15, -24
.Lcfi209:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB27_13
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB27_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB27_12
# BB#3:                                 #   in Loop: Header=BB27_2 Depth=1
	movq	64(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_5
# BB#4:                                 #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	24(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_7
# BB#6:                                 #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_7:                               # %_ZN11CStringBaseIwED2Ev.exit1.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_9
# BB#8:                                 #   in Loop: Header=BB27_2 Depth=1
	callq	_ZdaPv
.LBB27_9:                               # %_ZN11CStringBaseIwED2Ev.exit2.i
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB27_11
# BB#10:                                #   in Loop: Header=BB27_2 Depth=1
	movq	(%rdi), %rax
.Ltmp733:
	callq	*16(%rax)
.Ltmp734:
.LBB27_11:                              # %_ZN4CArcD2Ev.exit
                                        #   in Loop: Header=BB27_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB27_12:                              #   in Loop: Header=BB27_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB27_2
.LBB27_13:                              # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB27_14:
.Ltmp735:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end27:
	.size	_ZN13CObjectVectorI4CArcE6DeleteEii, .Lfunc_end27-_ZN13CObjectVectorI4CArcE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table27:
.Lexception19:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp733-.Lfunc_begin19 # >> Call Site 1 <<
	.long	.Ltmp734-.Ltmp733       #   Call between .Ltmp733 and .Ltmp734
	.long	.Ltmp735-.Lfunc_begin19 #     jumps to .Ltmp735
	.byte	0                       #   On action: cleanup
	.long	.Ltmp734-.Lfunc_begin19 # >> Call Site 2 <<
	.long	.Lfunc_end27-.Ltmp734   #   Call between .Ltmp734 and .Lfunc_end27
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN11CStringBaseIwE10GrowLengthEi,"axG",@progbits,_ZN11CStringBaseIwE10GrowLengthEi,comdat
	.weak	_ZN11CStringBaseIwE10GrowLengthEi
	.p2align	4, 0x90
	.type	_ZN11CStringBaseIwE10GrowLengthEi,@function
_ZN11CStringBaseIwE10GrowLengthEi:      # @_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi210:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi211:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi212:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi213:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi214:
	.cfi_def_cfa_offset 48
.Lcfi215:
	.cfi_offset %rbx, -48
.Lcfi216:
	.cfi_offset %r12, -40
.Lcfi217:
	.cfi_offset %r14, -32
.Lcfi218:
	.cfi_offset %r15, -24
.Lcfi219:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movl	8(%r14), %ebp
	movl	12(%r14), %ebx
	movl	%ebx, %eax
	subl	%ebp, %eax
	cmpl	%esi, %eax
	jg	.LBB28_28
# BB#1:
	decl	%eax
	cmpl	$8, %ebx
	movl	$16, %edx
	movl	$4, %ecx
	cmovgl	%edx, %ecx
	cmpl	$65, %ebx
	jl	.LBB28_3
# BB#2:                                 # %select.true.sink
	movl	%ebx, %ecx
	shrl	$31, %ecx
	addl	%ebx, %ecx
	sarl	%ecx
.LBB28_3:                               # %select.end
	movl	%esi, %edx
	subl	%eax, %edx
	addl	%ecx, %eax
	cmpl	%esi, %eax
	cmovgel	%ecx, %edx
	leal	1(%rbx,%rdx), %r15d
	cmpl	%ebx, %r15d
	je	.LBB28_28
# BB#4:
	movslq	%r15d, %rax
	movl	$4, %ecx
	mulq	%rcx
	movq	$-1, %rdi
	cmovnoq	%rax, %rdi
	callq	_Znam
	movq	%rax, %r12
	testl	%ebx, %ebx
	jle	.LBB28_27
# BB#5:                                 # %.preheader.i
	movq	(%r14), %rdi
	testl	%ebp, %ebp
	jle	.LBB28_25
# BB#6:                                 # %.lr.ph.i
	movslq	%ebp, %rax
	cmpl	$7, %ebp
	jbe	.LBB28_7
# BB#14:                                # %min.iters.checked
	movq	%rax, %rcx
	andq	$-8, %rcx
	je	.LBB28_7
# BB#15:                                # %vector.memcheck
	leaq	(%rdi,%rax,4), %rdx
	cmpq	%rdx, %r12
	jae	.LBB28_17
# BB#16:                                # %vector.memcheck
	leaq	(%r12,%rax,4), %rdx
	cmpq	%rdx, %rdi
	jae	.LBB28_17
.LBB28_7:
	xorl	%ecx, %ecx
.LBB28_8:                               # %scalar.ph.preheader
	subl	%ecx, %ebp
	leaq	-1(%rax), %rdx
	subq	%rcx, %rdx
	andq	$7, %rbp
	je	.LBB28_11
# BB#9:                                 # %scalar.ph.prol.preheader
	negq	%rbp
	.p2align	4, 0x90
.LBB28_10:                              # %scalar.ph.prol
                                        # =>This Inner Loop Header: Depth=1
	movl	(%rdi,%rcx,4), %esi
	movl	%esi, (%r12,%rcx,4)
	incq	%rcx
	incq	%rbp
	jne	.LBB28_10
.LBB28_11:                              # %scalar.ph.prol.loopexit
	cmpq	$7, %rdx
	jb	.LBB28_26
# BB#12:                                # %scalar.ph.preheader.new
	subq	%rcx, %rax
	leaq	28(%r12,%rcx,4), %rdx
	leaq	28(%rdi,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB28_13:                              # %scalar.ph
                                        # =>This Inner Loop Header: Depth=1
	movl	-28(%rcx), %esi
	movl	%esi, -28(%rdx)
	movl	-24(%rcx), %esi
	movl	%esi, -24(%rdx)
	movl	-20(%rcx), %esi
	movl	%esi, -20(%rdx)
	movl	-16(%rcx), %esi
	movl	%esi, -16(%rdx)
	movl	-12(%rcx), %esi
	movl	%esi, -12(%rdx)
	movl	-8(%rcx), %esi
	movl	%esi, -8(%rdx)
	movl	-4(%rcx), %esi
	movl	%esi, -4(%rdx)
	movl	(%rcx), %esi
	movl	%esi, (%rdx)
	addq	$32, %rdx
	addq	$32, %rcx
	addq	$-8, %rax
	jne	.LBB28_13
	jmp	.LBB28_26
.LBB28_25:                              # %._crit_edge.i
	testq	%rdi, %rdi
	je	.LBB28_27
.LBB28_26:                              # %._crit_edge.thread.i
	callq	_ZdaPv
	movl	8(%r14), %ebp
.LBB28_27:                              # %._crit_edge16.i
	movq	%r12, (%r14)
	movslq	%ebp, %rax
	movl	$0, (%r12,%rax,4)
	movl	%r15d, 12(%r14)
.LBB28_28:                              # %_ZN11CStringBaseIwE11SetCapacityEi.exit
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB28_17:                              # %vector.body.preheader
	leaq	-8(%rcx), %rdx
	movl	%edx, %esi
	shrl	$3, %esi
	incl	%esi
	andq	$3, %rsi
	je	.LBB28_18
# BB#19:                                # %vector.body.prol.preheader
	negq	%rsi
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB28_20:                              # %vector.body.prol
                                        # =>This Inner Loop Header: Depth=1
	movups	(%rdi,%rbx,4), %xmm0
	movups	16(%rdi,%rbx,4), %xmm1
	movups	%xmm0, (%r12,%rbx,4)
	movups	%xmm1, 16(%r12,%rbx,4)
	addq	$8, %rbx
	incq	%rsi
	jne	.LBB28_20
	jmp	.LBB28_21
.LBB28_18:
	xorl	%ebx, %ebx
.LBB28_21:                              # %vector.body.prol.loopexit
	cmpq	$24, %rdx
	jb	.LBB28_24
# BB#22:                                # %vector.body.preheader.new
	movq	%rcx, %rdx
	subq	%rbx, %rdx
	leaq	112(%r12,%rbx,4), %rsi
	leaq	112(%rdi,%rbx,4), %rbx
	.p2align	4, 0x90
.LBB28_23:                              # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movups	-112(%rbx), %xmm0
	movups	-96(%rbx), %xmm1
	movups	%xmm0, -112(%rsi)
	movups	%xmm1, -96(%rsi)
	movups	-80(%rbx), %xmm0
	movups	-64(%rbx), %xmm1
	movups	%xmm0, -80(%rsi)
	movups	%xmm1, -64(%rsi)
	movups	-48(%rbx), %xmm0
	movups	-32(%rbx), %xmm1
	movups	%xmm0, -48(%rsi)
	movups	%xmm1, -32(%rsi)
	movups	-16(%rbx), %xmm0
	movups	(%rbx), %xmm1
	movups	%xmm0, -16(%rsi)
	movups	%xmm1, (%rsi)
	subq	$-128, %rsi
	subq	$-128, %rbx
	addq	$-32, %rdx
	jne	.LBB28_23
.LBB28_24:                              # %middle.block
	cmpq	%rcx, %rax
	jne	.LBB28_8
	jmp	.LBB28_26
.Lfunc_end28:
	.size	_ZN11CStringBaseIwE10GrowLengthEi, .Lfunc_end28-_ZN11CStringBaseIwE10GrowLengthEi
	.cfi_endproc

	.section	.text._ZN13CObjectVectorI8CDirItemED2Ev,"axG",@progbits,_ZN13CObjectVectorI8CDirItemED2Ev,comdat
	.weak	_ZN13CObjectVectorI8CDirItemED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CDirItemED2Ev,@function
_ZN13CObjectVectorI8CDirItemED2Ev:      # @_ZN13CObjectVectorI8CDirItemED2Ev
.Lfunc_begin20:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception20
# BB#0:
	pushq	%r14
.Lcfi220:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi221:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi222:
	.cfi_def_cfa_offset 32
.Lcfi223:
	.cfi_offset %rbx, -24
.Lcfi224:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, (%rbx)
.Ltmp736:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp737:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB29_2:
.Ltmp738:
	movq	%rax, %r14
.Ltmp739:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp740:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB29_4:
.Ltmp741:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end29:
	.size	_ZN13CObjectVectorI8CDirItemED2Ev, .Lfunc_end29-_ZN13CObjectVectorI8CDirItemED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table29:
.Lexception20:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp736-.Lfunc_begin20 # >> Call Site 1 <<
	.long	.Ltmp737-.Ltmp736       #   Call between .Ltmp736 and .Ltmp737
	.long	.Ltmp738-.Lfunc_begin20 #     jumps to .Ltmp738
	.byte	0                       #   On action: cleanup
	.long	.Ltmp737-.Lfunc_begin20 # >> Call Site 2 <<
	.long	.Ltmp739-.Ltmp737       #   Call between .Ltmp737 and .Ltmp739
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp739-.Lfunc_begin20 # >> Call Site 3 <<
	.long	.Ltmp740-.Ltmp739       #   Call between .Ltmp739 and .Ltmp740
	.long	.Ltmp741-.Lfunc_begin20 #     jumps to .Ltmp741
	.byte	1                       #   On action: 1
	.long	.Ltmp740-.Lfunc_begin20 # >> Call Site 4 <<
	.long	.Lfunc_end29-.Ltmp740   #   Call between .Ltmp740 and .Lfunc_end29
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CDirItemED0Ev,"axG",@progbits,_ZN13CObjectVectorI8CDirItemED0Ev,comdat
	.weak	_ZN13CObjectVectorI8CDirItemED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CDirItemED0Ev,@function
_ZN13CObjectVectorI8CDirItemED0Ev:      # @_ZN13CObjectVectorI8CDirItemED0Ev
.Lfunc_begin21:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception21
# BB#0:
	pushq	%r14
.Lcfi225:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi226:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi227:
	.cfi_def_cfa_offset 32
.Lcfi228:
	.cfi_offset %rbx, -24
.Lcfi229:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI8CDirItemE+16, (%rbx)
.Ltmp742:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp743:
# BB#1:
.Ltmp748:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp749:
# BB#2:                                 # %_ZN13CObjectVectorI8CDirItemED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB30_5:
.Ltmp750:
	movq	%rax, %r14
	jmp	.LBB30_6
.LBB30_3:
.Ltmp744:
	movq	%rax, %r14
.Ltmp745:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp746:
.LBB30_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB30_4:
.Ltmp747:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end30:
	.size	_ZN13CObjectVectorI8CDirItemED0Ev, .Lfunc_end30-_ZN13CObjectVectorI8CDirItemED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table30:
.Lexception21:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp742-.Lfunc_begin21 # >> Call Site 1 <<
	.long	.Ltmp743-.Ltmp742       #   Call between .Ltmp742 and .Ltmp743
	.long	.Ltmp744-.Lfunc_begin21 #     jumps to .Ltmp744
	.byte	0                       #   On action: cleanup
	.long	.Ltmp748-.Lfunc_begin21 # >> Call Site 2 <<
	.long	.Ltmp749-.Ltmp748       #   Call between .Ltmp748 and .Ltmp749
	.long	.Ltmp750-.Lfunc_begin21 #     jumps to .Ltmp750
	.byte	0                       #   On action: cleanup
	.long	.Ltmp745-.Lfunc_begin21 # >> Call Site 3 <<
	.long	.Ltmp746-.Ltmp745       #   Call between .Ltmp745 and .Ltmp746
	.long	.Ltmp747-.Lfunc_begin21 #     jumps to .Ltmp747
	.byte	1                       #   On action: 1
	.long	.Ltmp746-.Lfunc_begin21 # >> Call Site 4 <<
	.long	.Lfunc_end30-.Ltmp746   #   Call between .Ltmp746 and .Lfunc_end30
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CDirItemE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI8CDirItemE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CDirItemE6DeleteEii,@function
_ZN13CObjectVectorI8CDirItemE6DeleteEii: # @_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi230:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi231:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi232:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi233:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi234:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi235:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi236:
	.cfi_def_cfa_offset 64
.Lcfi237:
	.cfi_offset %rbx, -56
.Lcfi238:
	.cfi_offset %r12, -48
.Lcfi239:
	.cfi_offset %r13, -40
.Lcfi240:
	.cfi_offset %r14, -32
.Lcfi241:
	.cfi_offset %r15, -24
.Lcfi242:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB31_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB31_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB31_6
# BB#3:                                 #   in Loop: Header=BB31_2 Depth=1
	movq	32(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB31_5
# BB#4:                                 #   in Loop: Header=BB31_2 Depth=1
	callq	_ZdaPv
.LBB31_5:                               # %_ZN8CDirItemD2Ev.exit
                                        #   in Loop: Header=BB31_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB31_6:                               #   in Loop: Header=BB31_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB31_2
.LBB31_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end31:
	.size	_ZN13CObjectVectorI8CDirItemE6DeleteEii, .Lfunc_end31-_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.cfi_endproc

	.section	.text._ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw,"axG",@progbits,_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw,comdat
	.weak	_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw
	.p2align	4, 0x90
	.type	_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw,@function
_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw: # @_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw
	.cfi_startproc
# BB#0:
	movq	8(%rdi), %rdi
	movq	(%rdi), %rax
	movq	104(%rax), %rax
	jmpq	*%rax                   # TAILCALL
.Lfunc_end32:
	.size	_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw, .Lfunc_end32-_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw
	.cfi_endproc

	.section	.text._ZN13CRecordVectorI12CUpdatePair2ED0Ev,"axG",@progbits,_ZN13CRecordVectorI12CUpdatePair2ED0Ev,comdat
	.weak	_ZN13CRecordVectorI12CUpdatePair2ED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorI12CUpdatePair2ED0Ev,@function
_ZN13CRecordVectorI12CUpdatePair2ED0Ev: # @_ZN13CRecordVectorI12CUpdatePair2ED0Ev
.Lfunc_begin22:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception22
# BB#0:
	pushq	%r14
.Lcfi243:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi244:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi245:
	.cfi_def_cfa_offset 32
.Lcfi246:
	.cfi_offset %rbx, -24
.Lcfi247:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp751:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp752:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB33_2:
.Ltmp753:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end33:
	.size	_ZN13CRecordVectorI12CUpdatePair2ED0Ev, .Lfunc_end33-_ZN13CRecordVectorI12CUpdatePair2ED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table33:
.Lexception22:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp751-.Lfunc_begin22 # >> Call Site 1 <<
	.long	.Ltmp752-.Ltmp751       #   Call between .Ltmp751 and .Ltmp752
	.long	.Ltmp753-.Lfunc_begin22 #     jumps to .Ltmp753
	.byte	0                       #   On action: cleanup
	.long	.Ltmp752-.Lfunc_begin22 # >> Call Site 2 <<
	.long	.Lfunc_end33-.Ltmp752   #   Call between .Ltmp752 and .Lfunc_end33
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorI11CUpdatePairED0Ev,"axG",@progbits,_ZN13CRecordVectorI11CUpdatePairED0Ev,comdat
	.weak	_ZN13CRecordVectorI11CUpdatePairED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorI11CUpdatePairED0Ev,@function
_ZN13CRecordVectorI11CUpdatePairED0Ev:  # @_ZN13CRecordVectorI11CUpdatePairED0Ev
.Lfunc_begin23:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception23
# BB#0:
	pushq	%r14
.Lcfi248:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi249:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi250:
	.cfi_def_cfa_offset 32
.Lcfi251:
	.cfi_offset %rbx, -24
.Lcfi252:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp754:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp755:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB34_2:
.Ltmp756:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end34:
	.size	_ZN13CRecordVectorI11CUpdatePairED0Ev, .Lfunc_end34-_ZN13CRecordVectorI11CUpdatePairED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table34:
.Lexception23:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp754-.Lfunc_begin23 # >> Call Site 1 <<
	.long	.Ltmp755-.Ltmp754       #   Call between .Ltmp754 and .Ltmp755
	.long	.Ltmp756-.Lfunc_begin23 #     jumps to .Ltmp756
	.byte	0                       #   On action: cleanup
	.long	.Ltmp755-.Lfunc_begin23 # >> Call Site 2 <<
	.long	.Lfunc_end34-.Ltmp755   #   Call between .Ltmp755 and .Lfunc_end34
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev,"axG",@progbits,_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev,comdat
	.weak	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev,@function
_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev: # @_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev
.Lfunc_begin24:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception24
# BB#0:
	pushq	%r14
.Lcfi253:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi254:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi255:
	.cfi_def_cfa_offset 32
.Lcfi256:
	.cfi_offset %rbx, -24
.Lcfi257:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE+16, (%rbx)
.Ltmp757:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp758:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZN17CBaseRecordVectorD2Ev # TAILCALL
.LBB35_2:
.Ltmp759:
	movq	%rax, %r14
.Ltmp760:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp761:
# BB#3:
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB35_4:
.Ltmp762:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end35:
	.size	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev, .Lfunc_end35-_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table35:
.Lexception24:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp757-.Lfunc_begin24 # >> Call Site 1 <<
	.long	.Ltmp758-.Ltmp757       #   Call between .Ltmp757 and .Ltmp758
	.long	.Ltmp759-.Lfunc_begin24 #     jumps to .Ltmp759
	.byte	0                       #   On action: cleanup
	.long	.Ltmp758-.Lfunc_begin24 # >> Call Site 2 <<
	.long	.Ltmp760-.Ltmp758       #   Call between .Ltmp758 and .Ltmp760
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.long	.Ltmp760-.Lfunc_begin24 # >> Call Site 3 <<
	.long	.Ltmp761-.Ltmp760       #   Call between .Ltmp760 and .Ltmp761
	.long	.Ltmp762-.Lfunc_begin24 #     jumps to .Ltmp762
	.byte	1                       #   On action: 1
	.long	.Ltmp761-.Lfunc_begin24 # >> Call Site 4 <<
	.long	.Lfunc_end35-.Ltmp761   #   Call between .Ltmp761 and .Lfunc_end35
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev,"axG",@progbits,_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev,comdat
	.weak	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev,@function
_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev: # @_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev
.Lfunc_begin25:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception25
# BB#0:
	pushq	%r14
.Lcfi258:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi259:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi260:
	.cfi_def_cfa_offset 32
.Lcfi261:
	.cfi_offset %rbx, -24
.Lcfi262:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE+16, (%rbx)
.Ltmp763:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp764:
# BB#1:
.Ltmp769:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp770:
# BB#2:                                 # %_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB36_5:
.Ltmp771:
	movq	%rax, %r14
	jmp	.LBB36_6
.LBB36_3:
.Ltmp765:
	movq	%rax, %r14
.Ltmp766:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp767:
.LBB36_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB36_4:
.Ltmp768:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end36:
	.size	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev, .Lfunc_end36-_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table36:
.Lexception25:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp763-.Lfunc_begin25 # >> Call Site 1 <<
	.long	.Ltmp764-.Ltmp763       #   Call between .Ltmp763 and .Ltmp764
	.long	.Ltmp765-.Lfunc_begin25 #     jumps to .Ltmp765
	.byte	0                       #   On action: cleanup
	.long	.Ltmp769-.Lfunc_begin25 # >> Call Site 2 <<
	.long	.Ltmp770-.Ltmp769       #   Call between .Ltmp769 and .Ltmp770
	.long	.Ltmp771-.Lfunc_begin25 #     jumps to .Ltmp771
	.byte	0                       #   On action: cleanup
	.long	.Ltmp766-.Lfunc_begin25 # >> Call Site 3 <<
	.long	.Ltmp767-.Ltmp766       #   Call between .Ltmp766 and .Ltmp767
	.long	.Ltmp768-.Lfunc_begin25 #     jumps to .Ltmp768
	.byte	1                       #   On action: 1
	.long	.Ltmp767-.Lfunc_begin25 # >> Call Site 4 <<
	.long	.Lfunc_end36-.Ltmp767   #   Call between .Ltmp767 and .Lfunc_end36
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii,@function
_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii: # @_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii
.Lfunc_begin26:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception26
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi263:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi264:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi265:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi266:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi267:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi268:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi269:
	.cfi_def_cfa_offset 64
.Lcfi270:
	.cfi_offset %rbx, -56
.Lcfi271:
	.cfi_offset %r12, -48
.Lcfi272:
	.cfi_offset %r13, -40
.Lcfi273:
	.cfi_offset %r14, -32
.Lcfi274:
	.cfi_offset %r15, -24
.Lcfi275:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB37_9
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB37_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB37_8
# BB#3:                                 #   in Loop: Header=BB37_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB37_5
# BB#4:                                 #   in Loop: Header=BB37_2 Depth=1
	callq	_ZdaPv
.LBB37_5:                               # %_ZN11CStringBaseIwED2Ev.exit.i
                                        #   in Loop: Header=BB37_2 Depth=1
	movq	8(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB37_7
# BB#6:                                 #   in Loop: Header=BB37_2 Depth=1
	movq	(%rdi), %rax
.Ltmp772:
	callq	*16(%rax)
.Ltmp773:
.LBB37_7:                               # %_ZN18COutMultiVolStream14CSubStreamInfoD2Ev.exit
                                        #   in Loop: Header=BB37_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB37_8:                               #   in Loop: Header=BB37_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB37_2
.LBB37_9:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.LBB37_10:
.Ltmp774:
	movq	%rax, %rbx
	movq	%rbp, %rdi
	callq	_ZdlPv
	movq	%rbx, %rdi
	callq	_Unwind_Resume
.Lfunc_end37:
	.size	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii, .Lfunc_end37-_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table37:
.Lexception26:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp772-.Lfunc_begin26 # >> Call Site 1 <<
	.long	.Ltmp773-.Ltmp772       #   Call between .Ltmp772 and .Ltmp773
	.long	.Ltmp774-.Lfunc_begin26 #     jumps to .Ltmp774
	.byte	0                       #   On action: cleanup
	.long	.Ltmp773-.Lfunc_begin26 # >> Call Site 2 <<
	.long	.Lfunc_end37-.Ltmp773   #   Call between .Ltmp773 and .Lfunc_end37
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIyED0Ev,"axG",@progbits,_ZN13CRecordVectorIyED0Ev,comdat
	.weak	_ZN13CRecordVectorIyED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIyED0Ev,@function
_ZN13CRecordVectorIyED0Ev:              # @_ZN13CRecordVectorIyED0Ev
.Lfunc_begin27:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception27
# BB#0:
	pushq	%r14
.Lcfi276:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi277:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi278:
	.cfi_def_cfa_offset 32
.Lcfi279:
	.cfi_offset %rbx, -24
.Lcfi280:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp775:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp776:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB38_2:
.Ltmp777:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end38:
	.size	_ZN13CRecordVectorIyED0Ev, .Lfunc_end38-_ZN13CRecordVectorIyED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table38:
.Lexception27:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp775-.Lfunc_begin27 # >> Call Site 1 <<
	.long	.Ltmp776-.Ltmp775       #   Call between .Ltmp775 and .Ltmp776
	.long	.Ltmp777-.Lfunc_begin27 #     jumps to .Ltmp777
	.byte	0                       #   On action: cleanup
	.long	.Ltmp776-.Lfunc_begin27 # >> Call Site 2 <<
	.long	.Lfunc_end38-.Ltmp776   #   Call between .Ltmp776 and .Lfunc_end38
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN8NWindows5NFile3NIO7CInFileD0Ev,"axG",@progbits,_ZN8NWindows5NFile3NIO7CInFileD0Ev,comdat
	.weak	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.p2align	4, 0x90
	.type	_ZN8NWindows5NFile3NIO7CInFileD0Ev,@function
_ZN8NWindows5NFile3NIO7CInFileD0Ev:     # @_ZN8NWindows5NFile3NIO7CInFileD0Ev
.Lfunc_begin28:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception28
# BB#0:
	pushq	%r14
.Lcfi281:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi282:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi283:
	.cfi_def_cfa_offset 32
.Lcfi284:
	.cfi_offset %rbx, -24
.Lcfi285:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp778:
	callq	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
.Ltmp779:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB39_2:
.Ltmp780:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end39:
	.size	_ZN8NWindows5NFile3NIO7CInFileD0Ev, .Lfunc_end39-_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table39:
.Lexception28:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp778-.Lfunc_begin28 # >> Call Site 1 <<
	.long	.Ltmp779-.Ltmp778       #   Call between .Ltmp778 and .Ltmp779
	.long	.Ltmp780-.Lfunc_begin28 #     jumps to .Ltmp780
	.byte	0                       #   On action: cleanup
	.long	.Ltmp779-.Lfunc_begin28 # >> Call Site 2 <<
	.long	.Lfunc_end39-.Ltmp779   #   Call between .Ltmp779 and .Lfunc_end39
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEED0Ev,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEED0Ev,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEED0Ev,@function
_ZN13CObjectVectorI11CStringBaseIwEED0Ev: # @_ZN13CObjectVectorI11CStringBaseIwEED0Ev
.Lfunc_begin29:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception29
# BB#0:
	pushq	%r14
.Lcfi286:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi287:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi288:
	.cfi_def_cfa_offset 32
.Lcfi289:
	.cfi_offset %rbx, -24
.Lcfi290:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI11CStringBaseIwEE+16, (%rbx)
.Ltmp781:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp782:
# BB#1:
.Ltmp787:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp788:
# BB#2:                                 # %_ZN13CObjectVectorI11CStringBaseIwEED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB40_5:
.Ltmp789:
	movq	%rax, %r14
	jmp	.LBB40_6
.LBB40_3:
.Ltmp783:
	movq	%rax, %r14
.Ltmp784:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp785:
.LBB40_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB40_4:
.Ltmp786:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end40:
	.size	_ZN13CObjectVectorI11CStringBaseIwEED0Ev, .Lfunc_end40-_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table40:
.Lexception29:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp781-.Lfunc_begin29 # >> Call Site 1 <<
	.long	.Ltmp782-.Ltmp781       #   Call between .Ltmp781 and .Ltmp782
	.long	.Ltmp783-.Lfunc_begin29 #     jumps to .Ltmp783
	.byte	0                       #   On action: cleanup
	.long	.Ltmp787-.Lfunc_begin29 # >> Call Site 2 <<
	.long	.Ltmp788-.Ltmp787       #   Call between .Ltmp787 and .Ltmp788
	.long	.Ltmp789-.Lfunc_begin29 #     jumps to .Ltmp789
	.byte	0                       #   On action: cleanup
	.long	.Ltmp784-.Lfunc_begin29 # >> Call Site 3 <<
	.long	.Ltmp785-.Ltmp784       #   Call between .Ltmp784 and .Ltmp785
	.long	.Ltmp786-.Lfunc_begin29 #     jumps to .Ltmp786
	.byte	1                       #   On action: 1
	.long	.Ltmp785-.Lfunc_begin29 # >> Call Site 4 <<
	.long	.Lfunc_end40-.Ltmp785   #   Call between .Ltmp785 and .Lfunc_end40
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii,@function
_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii: # @_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi291:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi292:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi293:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi294:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi295:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi296:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi297:
	.cfi_def_cfa_offset 64
.Lcfi298:
	.cfi_offset %rbx, -56
.Lcfi299:
	.cfi_offset %r12, -48
.Lcfi300:
	.cfi_offset %r13, -40
.Lcfi301:
	.cfi_offset %r14, -32
.Lcfi302:
	.cfi_offset %r15, -24
.Lcfi303:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB41_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB41_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB41_6
# BB#3:                                 #   in Loop: Header=BB41_2 Depth=1
	movq	(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB41_5
# BB#4:                                 #   in Loop: Header=BB41_2 Depth=1
	callq	_ZdaPv
.LBB41_5:                               # %_ZN11CStringBaseIwED2Ev.exit
                                        #   in Loop: Header=BB41_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB41_6:                               #   in Loop: Header=BB41_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB41_2
.LBB41_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end41:
	.size	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii, .Lfunc_end41-_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.cfi_endproc

	.section	.text._ZN13CRecordVectorIiED0Ev,"axG",@progbits,_ZN13CRecordVectorIiED0Ev,comdat
	.weak	_ZN13CRecordVectorIiED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIiED0Ev,@function
_ZN13CRecordVectorIiED0Ev:              # @_ZN13CRecordVectorIiED0Ev
.Lfunc_begin30:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception30
# BB#0:
	pushq	%r14
.Lcfi304:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi305:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi306:
	.cfi_def_cfa_offset 32
.Lcfi307:
	.cfi_offset %rbx, -24
.Lcfi308:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp790:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp791:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB42_2:
.Ltmp792:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end42:
	.size	_ZN13CRecordVectorIiED0Ev, .Lfunc_end42-_ZN13CRecordVectorIiED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table42:
.Lexception30:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp790-.Lfunc_begin30 # >> Call Site 1 <<
	.long	.Ltmp791-.Ltmp790       #   Call between .Ltmp790 and .Ltmp791
	.long	.Ltmp792-.Lfunc_begin30 #     jumps to .Ltmp792
	.byte	0                       #   On action: cleanup
	.long	.Ltmp791-.Lfunc_begin30 # >> Call Site 2 <<
	.long	.Lfunc_end42-.Ltmp791   #   Call between .Ltmp791 and .Lfunc_end42
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CRecordVectorIjED0Ev,"axG",@progbits,_ZN13CRecordVectorIjED0Ev,comdat
	.weak	_ZN13CRecordVectorIjED0Ev
	.p2align	4, 0x90
	.type	_ZN13CRecordVectorIjED0Ev,@function
_ZN13CRecordVectorIjED0Ev:              # @_ZN13CRecordVectorIjED0Ev
.Lfunc_begin31:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception31
# BB#0:
	pushq	%r14
.Lcfi309:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi310:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi311:
	.cfi_def_cfa_offset 32
.Lcfi312:
	.cfi_offset %rbx, -24
.Lcfi313:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
.Ltmp793:
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp794:
# BB#1:
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB43_2:
.Ltmp795:
	movq	%rax, %r14
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.Lfunc_end43:
	.size	_ZN13CRecordVectorIjED0Ev, .Lfunc_end43-_ZN13CRecordVectorIjED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table43:
.Lexception31:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\234"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	26                      # Call site table length
	.long	.Ltmp793-.Lfunc_begin31 # >> Call Site 1 <<
	.long	.Ltmp794-.Ltmp793       #   Call between .Ltmp793 and .Ltmp794
	.long	.Ltmp795-.Lfunc_begin31 #     jumps to .Ltmp795
	.byte	0                       #   On action: cleanup
	.long	.Ltmp794-.Lfunc_begin31 # >> Call Site 2 <<
	.long	.Lfunc_end43-.Ltmp794   #   Call between .Ltmp794 and .Lfunc_end43
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CArcItemED0Ev,"axG",@progbits,_ZN13CObjectVectorI8CArcItemED0Ev,comdat
	.weak	_ZN13CObjectVectorI8CArcItemED0Ev
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CArcItemED0Ev,@function
_ZN13CObjectVectorI8CArcItemED0Ev:      # @_ZN13CObjectVectorI8CArcItemED0Ev
.Lfunc_begin32:
	.cfi_startproc
	.cfi_personality 3, __gxx_personality_v0
	.cfi_lsda 3, .Lexception32
# BB#0:
	pushq	%r14
.Lcfi314:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi315:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi316:
	.cfi_def_cfa_offset 32
.Lcfi317:
	.cfi_offset %rbx, -24
.Lcfi318:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movq	$_ZTV13CObjectVectorI8CArcItemE+16, (%rbx)
.Ltmp796:
	callq	_ZN17CBaseRecordVector5ClearEv
.Ltmp797:
# BB#1:
.Ltmp802:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp803:
# BB#2:                                 # %_ZN13CObjectVectorI8CArcItemED2Ev.exit
	movq	%rbx, %rdi
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	jmp	_ZdlPv                  # TAILCALL
.LBB44_5:
.Ltmp804:
	movq	%rax, %r14
	jmp	.LBB44_6
.LBB44_3:
.Ltmp798:
	movq	%rax, %r14
.Ltmp799:
	movq	%rbx, %rdi
	callq	_ZN17CBaseRecordVectorD2Ev
.Ltmp800:
.LBB44_6:                               # %.body
	movq	%rbx, %rdi
	callq	_ZdlPv
	movq	%r14, %rdi
	callq	_Unwind_Resume
.LBB44_4:
.Ltmp801:
	movq	%rax, %rdi
	callq	__clang_call_terminate
.Lfunc_end44:
	.size	_ZN13CObjectVectorI8CArcItemED0Ev, .Lfunc_end44-_ZN13CObjectVectorI8CArcItemED0Ev
	.cfi_endproc
	.section	.gcc_except_table,"a",@progbits
	.p2align	2
GCC_except_table44:
.Lexception32:
	.byte	255                     # @LPStart Encoding = omit
	.byte	3                       # @TType Encoding = udata4
	.asciz	"\274"                  # @TType base offset
	.byte	3                       # Call site Encoding = udata4
	.byte	52                      # Call site table length
	.long	.Ltmp796-.Lfunc_begin32 # >> Call Site 1 <<
	.long	.Ltmp797-.Ltmp796       #   Call between .Ltmp796 and .Ltmp797
	.long	.Ltmp798-.Lfunc_begin32 #     jumps to .Ltmp798
	.byte	0                       #   On action: cleanup
	.long	.Ltmp802-.Lfunc_begin32 # >> Call Site 2 <<
	.long	.Ltmp803-.Ltmp802       #   Call between .Ltmp802 and .Ltmp803
	.long	.Ltmp804-.Lfunc_begin32 #     jumps to .Ltmp804
	.byte	0                       #   On action: cleanup
	.long	.Ltmp799-.Lfunc_begin32 # >> Call Site 3 <<
	.long	.Ltmp800-.Ltmp799       #   Call between .Ltmp799 and .Ltmp800
	.long	.Ltmp801-.Lfunc_begin32 #     jumps to .Ltmp801
	.byte	1                       #   On action: 1
	.long	.Ltmp800-.Lfunc_begin32 # >> Call Site 4 <<
	.long	.Lfunc_end44-.Ltmp800   #   Call between .Ltmp800 and .Lfunc_end44
	.long	0                       #     has no landing pad
	.byte	0                       #   On action: cleanup
	.byte	1                       # >> Action Record 1 <<
                                        #   Catch TypeInfo 1
	.byte	0                       #   No further actions
                                        # >> Catch TypeInfos <<
	.long	0                       # TypeInfo 1
	.p2align	2

	.section	.text._ZN13CObjectVectorI8CArcItemE6DeleteEii,"axG",@progbits,_ZN13CObjectVectorI8CArcItemE6DeleteEii,comdat
	.weak	_ZN13CObjectVectorI8CArcItemE6DeleteEii
	.p2align	4, 0x90
	.type	_ZN13CObjectVectorI8CArcItemE6DeleteEii,@function
_ZN13CObjectVectorI8CArcItemE6DeleteEii: # @_ZN13CObjectVectorI8CArcItemE6DeleteEii
	.cfi_startproc
# BB#0:                                 # %_ZNK17CBaseRecordVector22TestIndexAndCorrectNumEiRi.exit
	pushq	%rbp
.Lcfi319:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi320:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi321:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi322:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi323:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi324:
	.cfi_def_cfa_offset 56
	pushq	%rax
.Lcfi325:
	.cfi_def_cfa_offset 64
.Lcfi326:
	.cfi_offset %rbx, -56
.Lcfi327:
	.cfi_offset %r12, -48
.Lcfi328:
	.cfi_offset %r13, -40
.Lcfi329:
	.cfi_offset %r14, -32
.Lcfi330:
	.cfi_offset %r15, -24
.Lcfi331:
	.cfi_offset %rbp, -16
                                        # kill: %EDX<def> %EDX<kill> %RDX<def>
                                        # kill: %ESI<def> %ESI<kill> %RSI<def>
	movq	%rdi, %r12
	leal	(%rdx,%rsi), %eax
	movl	12(%r12), %ecx
	movl	%ecx, %r15d
	movq	%rsi, (%rsp)            # 8-byte Spill
	subl	%esi, %r15d
	cmpl	%ecx, %eax
	cmovlel	%edx, %r15d
	testl	%r15d, %r15d
	jle	.LBB45_7
# BB#1:                                 # %.lr.ph
	movslq	(%rsp), %rbx            # 4-byte Folded Reload
	movslq	%r15d, %r13
	shlq	$3, %rbx
	xorl	%r14d, %r14d
	.p2align	4, 0x90
.LBB45_2:                               # =>This Inner Loop Header: Depth=1
	movq	16(%r12), %rax
	addq	%rbx, %rax
	movq	(%rax,%r14,8), %rbp
	testq	%rbp, %rbp
	je	.LBB45_6
# BB#3:                                 #   in Loop: Header=BB45_2 Depth=1
	movq	16(%rbp), %rdi
	testq	%rdi, %rdi
	je	.LBB45_5
# BB#4:                                 #   in Loop: Header=BB45_2 Depth=1
	callq	_ZdaPv
.LBB45_5:                               # %_ZN8CArcItemD2Ev.exit
                                        #   in Loop: Header=BB45_2 Depth=1
	movq	%rbp, %rdi
	callq	_ZdlPv
.LBB45_6:                               #   in Loop: Header=BB45_2 Depth=1
	incq	%r14
	cmpq	%r13, %r14
	jl	.LBB45_2
.LBB45_7:                               # %._crit_edge
	movq	%r12, %rdi
	movq	(%rsp), %rsi            # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r15d, %edx
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmp	_ZN17CBaseRecordVector6DeleteEii # TAILCALL
.Lfunc_end45:
	.size	_ZN13CObjectVectorI8CArcItemE6DeleteEii, .Lfunc_end45-_ZN13CObjectVectorI8CArcItemE6DeleteEii
	.cfi_endproc

	.type	.L.str.4,@object        # @.str.4
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.4:
	.asciz	"there is no such archive"
	.size	.L.str.4, 25

	.type	_ZTV18COutMultiVolStream,@object # @_ZTV18COutMultiVolStream
	.section	.rodata,"a",@progbits
	.globl	_ZTV18COutMultiVolStream
	.p2align	3
_ZTV18COutMultiVolStream:
	.quad	0
	.quad	_ZTI18COutMultiVolStream
	.quad	_ZN18COutMultiVolStream14QueryInterfaceERK4GUIDPPv
	.quad	_ZN18COutMultiVolStream6AddRefEv
	.quad	_ZN18COutMultiVolStream7ReleaseEv
	.quad	_ZN18COutMultiVolStreamD2Ev
	.quad	_ZN18COutMultiVolStreamD0Ev
	.quad	_ZN18COutMultiVolStream5WriteEPKvjPj
	.quad	_ZN18COutMultiVolStream4SeekExjPy
	.quad	_ZN18COutMultiVolStream7SetSizeEy
	.size	_ZTV18COutMultiVolStream, 80

	.type	_ZTS18COutMultiVolStream,@object # @_ZTS18COutMultiVolStream
	.globl	_ZTS18COutMultiVolStream
	.p2align	4
_ZTS18COutMultiVolStream:
	.asciz	"18COutMultiVolStream"
	.size	_ZTS18COutMultiVolStream, 21

	.type	_ZTS10IOutStream,@object # @_ZTS10IOutStream
	.section	.rodata._ZTS10IOutStream,"aG",@progbits,_ZTS10IOutStream,comdat
	.weak	_ZTS10IOutStream
_ZTS10IOutStream:
	.asciz	"10IOutStream"
	.size	_ZTS10IOutStream, 13

	.type	_ZTS20ISequentialOutStream,@object # @_ZTS20ISequentialOutStream
	.section	.rodata._ZTS20ISequentialOutStream,"aG",@progbits,_ZTS20ISequentialOutStream,comdat
	.weak	_ZTS20ISequentialOutStream
	.p2align	4
_ZTS20ISequentialOutStream:
	.asciz	"20ISequentialOutStream"
	.size	_ZTS20ISequentialOutStream, 23

	.type	_ZTS8IUnknown,@object   # @_ZTS8IUnknown
	.section	.rodata._ZTS8IUnknown,"aG",@progbits,_ZTS8IUnknown,comdat
	.weak	_ZTS8IUnknown
_ZTS8IUnknown:
	.asciz	"8IUnknown"
	.size	_ZTS8IUnknown, 10

	.type	_ZTI8IUnknown,@object   # @_ZTI8IUnknown
	.section	.rodata._ZTI8IUnknown,"aG",@progbits,_ZTI8IUnknown,comdat
	.weak	_ZTI8IUnknown
	.p2align	3
_ZTI8IUnknown:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS8IUnknown
	.size	_ZTI8IUnknown, 16

	.type	_ZTI20ISequentialOutStream,@object # @_ZTI20ISequentialOutStream
	.section	.rodata._ZTI20ISequentialOutStream,"aG",@progbits,_ZTI20ISequentialOutStream,comdat
	.weak	_ZTI20ISequentialOutStream
	.p2align	4
_ZTI20ISequentialOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS20ISequentialOutStream
	.quad	_ZTI8IUnknown
	.size	_ZTI20ISequentialOutStream, 24

	.type	_ZTI10IOutStream,@object # @_ZTI10IOutStream
	.section	.rodata._ZTI10IOutStream,"aG",@progbits,_ZTI10IOutStream,comdat
	.weak	_ZTI10IOutStream
	.p2align	4
_ZTI10IOutStream:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS10IOutStream
	.quad	_ZTI20ISequentialOutStream
	.size	_ZTI10IOutStream, 24

	.type	_ZTS13CMyUnknownImp,@object # @_ZTS13CMyUnknownImp
	.section	.rodata._ZTS13CMyUnknownImp,"aG",@progbits,_ZTS13CMyUnknownImp,comdat
	.weak	_ZTS13CMyUnknownImp
_ZTS13CMyUnknownImp:
	.asciz	"13CMyUnknownImp"
	.size	_ZTS13CMyUnknownImp, 16

	.type	_ZTI13CMyUnknownImp,@object # @_ZTI13CMyUnknownImp
	.section	.rodata._ZTI13CMyUnknownImp,"aG",@progbits,_ZTI13CMyUnknownImp,comdat
	.weak	_ZTI13CMyUnknownImp
	.p2align	3
_ZTI13CMyUnknownImp:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS13CMyUnknownImp
	.size	_ZTI13CMyUnknownImp, 16

	.type	_ZTI18COutMultiVolStream,@object # @_ZTI18COutMultiVolStream
	.section	.rodata,"a",@progbits
	.globl	_ZTI18COutMultiVolStream
	.p2align	4
_ZTI18COutMultiVolStream:
	.quad	_ZTVN10__cxxabiv121__vmi_class_type_infoE+16
	.quad	_ZTS18COutMultiVolStream
	.long	0                       # 0x0
	.long	2                       # 0x2
	.quad	_ZTI10IOutStream
	.quad	2                       # 0x2
	.quad	_ZTI13CMyUnknownImp
	.quad	2050                    # 0x802
	.size	_ZTI18COutMultiVolStream, 56

	.type	_ZTVN8NWindows5NFile3NIO8COutFileE,@object # @_ZTVN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO8COutFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO8COutFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO8COutFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO8COutFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO8COutFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO8COutFileE,@object # @_ZTSN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO8COutFileE:
	.asciz	"N8NWindows5NFile3NIO8COutFileE"
	.size	_ZTSN8NWindows5NFile3NIO8COutFileE, 31

	.type	_ZTIN8NWindows5NFile3NIO8COutFileE,@object # @_ZTIN8NWindows5NFile3NIO8COutFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO8COutFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO8COutFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO8COutFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO8COutFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO8COutFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO8COutFileE, 24

	.type	_ZTV13CObjectVectorI4CArcE,@object # @_ZTV13CObjectVectorI4CArcE
	.section	.rodata._ZTV13CObjectVectorI4CArcE,"aG",@progbits,_ZTV13CObjectVectorI4CArcE,comdat
	.weak	_ZTV13CObjectVectorI4CArcE
	.p2align	3
_ZTV13CObjectVectorI4CArcE:
	.quad	0
	.quad	_ZTI13CObjectVectorI4CArcE
	.quad	_ZN13CObjectVectorI4CArcED2Ev
	.quad	_ZN13CObjectVectorI4CArcED0Ev
	.quad	_ZN13CObjectVectorI4CArcE6DeleteEii
	.size	_ZTV13CObjectVectorI4CArcE, 40

	.type	_ZTS13CObjectVectorI4CArcE,@object # @_ZTS13CObjectVectorI4CArcE
	.section	.rodata._ZTS13CObjectVectorI4CArcE,"aG",@progbits,_ZTS13CObjectVectorI4CArcE,comdat
	.weak	_ZTS13CObjectVectorI4CArcE
	.p2align	4
_ZTS13CObjectVectorI4CArcE:
	.asciz	"13CObjectVectorI4CArcE"
	.size	_ZTS13CObjectVectorI4CArcE, 23

	.type	_ZTS13CRecordVectorIPvE,@object # @_ZTS13CRecordVectorIPvE
	.section	.rodata._ZTS13CRecordVectorIPvE,"aG",@progbits,_ZTS13CRecordVectorIPvE,comdat
	.weak	_ZTS13CRecordVectorIPvE
	.p2align	4
_ZTS13CRecordVectorIPvE:
	.asciz	"13CRecordVectorIPvE"
	.size	_ZTS13CRecordVectorIPvE, 20

	.type	_ZTI13CRecordVectorIPvE,@object # @_ZTI13CRecordVectorIPvE
	.section	.rodata._ZTI13CRecordVectorIPvE,"aG",@progbits,_ZTI13CRecordVectorIPvE,comdat
	.weak	_ZTI13CRecordVectorIPvE
	.p2align	4
_ZTI13CRecordVectorIPvE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIPvE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIPvE, 24

	.type	_ZTI13CObjectVectorI4CArcE,@object # @_ZTI13CObjectVectorI4CArcE
	.section	.rodata._ZTI13CObjectVectorI4CArcE,"aG",@progbits,_ZTI13CObjectVectorI4CArcE,comdat
	.weak	_ZTI13CObjectVectorI4CArcE
	.p2align	4
_ZTI13CObjectVectorI4CArcE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI4CArcE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI4CArcE, 24

	.type	_ZTV13CObjectVectorI8CDirItemE,@object # @_ZTV13CObjectVectorI8CDirItemE
	.section	.rodata._ZTV13CObjectVectorI8CDirItemE,"aG",@progbits,_ZTV13CObjectVectorI8CDirItemE,comdat
	.weak	_ZTV13CObjectVectorI8CDirItemE
	.p2align	3
_ZTV13CObjectVectorI8CDirItemE:
	.quad	0
	.quad	_ZTI13CObjectVectorI8CDirItemE
	.quad	_ZN13CObjectVectorI8CDirItemED2Ev
	.quad	_ZN13CObjectVectorI8CDirItemED0Ev
	.quad	_ZN13CObjectVectorI8CDirItemE6DeleteEii
	.size	_ZTV13CObjectVectorI8CDirItemE, 40

	.type	_ZTS13CObjectVectorI8CDirItemE,@object # @_ZTS13CObjectVectorI8CDirItemE
	.section	.rodata._ZTS13CObjectVectorI8CDirItemE,"aG",@progbits,_ZTS13CObjectVectorI8CDirItemE,comdat
	.weak	_ZTS13CObjectVectorI8CDirItemE
	.p2align	4
_ZTS13CObjectVectorI8CDirItemE:
	.asciz	"13CObjectVectorI8CDirItemE"
	.size	_ZTS13CObjectVectorI8CDirItemE, 27

	.type	_ZTI13CObjectVectorI8CDirItemE,@object # @_ZTI13CObjectVectorI8CDirItemE
	.section	.rodata._ZTI13CObjectVectorI8CDirItemE,"aG",@progbits,_ZTI13CObjectVectorI8CDirItemE,comdat
	.weak	_ZTI13CObjectVectorI8CDirItemE
	.p2align	4
_ZTI13CObjectVectorI8CDirItemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI8CDirItemE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI8CDirItemE, 24

	.type	_ZTV26CEnumDirItemUpdateCallback,@object # @_ZTV26CEnumDirItemUpdateCallback
	.section	.rodata._ZTV26CEnumDirItemUpdateCallback,"aG",@progbits,_ZTV26CEnumDirItemUpdateCallback,comdat
	.weak	_ZTV26CEnumDirItemUpdateCallback
	.p2align	3
_ZTV26CEnumDirItemUpdateCallback:
	.quad	0
	.quad	_ZTI26CEnumDirItemUpdateCallback
	.quad	_ZN26CEnumDirItemUpdateCallback12ScanProgressEyyPKw
	.size	_ZTV26CEnumDirItemUpdateCallback, 24

	.type	_ZTS26CEnumDirItemUpdateCallback,@object # @_ZTS26CEnumDirItemUpdateCallback
	.section	.rodata._ZTS26CEnumDirItemUpdateCallback,"aG",@progbits,_ZTS26CEnumDirItemUpdateCallback,comdat
	.weak	_ZTS26CEnumDirItemUpdateCallback
	.p2align	4
_ZTS26CEnumDirItemUpdateCallback:
	.asciz	"26CEnumDirItemUpdateCallback"
	.size	_ZTS26CEnumDirItemUpdateCallback, 29

	.type	_ZTS20IEnumDirItemCallback,@object # @_ZTS20IEnumDirItemCallback
	.section	.rodata._ZTS20IEnumDirItemCallback,"aG",@progbits,_ZTS20IEnumDirItemCallback,comdat
	.weak	_ZTS20IEnumDirItemCallback
	.p2align	4
_ZTS20IEnumDirItemCallback:
	.asciz	"20IEnumDirItemCallback"
	.size	_ZTS20IEnumDirItemCallback, 23

	.type	_ZTI20IEnumDirItemCallback,@object # @_ZTI20IEnumDirItemCallback
	.section	.rodata._ZTI20IEnumDirItemCallback,"aG",@progbits,_ZTI20IEnumDirItemCallback,comdat
	.weak	_ZTI20IEnumDirItemCallback
	.p2align	3
_ZTI20IEnumDirItemCallback:
	.quad	_ZTVN10__cxxabiv117__class_type_infoE+16
	.quad	_ZTS20IEnumDirItemCallback
	.size	_ZTI20IEnumDirItemCallback, 16

	.type	_ZTI26CEnumDirItemUpdateCallback,@object # @_ZTI26CEnumDirItemUpdateCallback
	.section	.rodata._ZTI26CEnumDirItemUpdateCallback,"aG",@progbits,_ZTI26CEnumDirItemUpdateCallback,comdat
	.weak	_ZTI26CEnumDirItemUpdateCallback
	.p2align	4
_ZTI26CEnumDirItemUpdateCallback:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS26CEnumDirItemUpdateCallback
	.quad	_ZTI20IEnumDirItemCallback
	.size	_ZTI26CEnumDirItemUpdateCallback, 24

	.type	.L.str.12,@object       # @.str.12
	.section	.rodata.str4.4,"aMS",@progbits,4
	.p2align	2
.L.str.12:
	.long	115                     # 0x73
	.long	116                     # 0x74
	.long	100                     # 0x64
	.long	111                     # 0x6f
	.long	117                     # 0x75
	.long	116                     # 0x74
	.long	0                       # 0x0
	.size	.L.str.12, 28

	.type	.L.str.16,@object       # @.str.16
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str.16:
	.asciz	"update operations are not supported for this archive"
	.size	.L.str.16, 53

	.type	_ZTV13CRecordVectorI12CUpdatePair2E,@object # @_ZTV13CRecordVectorI12CUpdatePair2E
	.section	.rodata._ZTV13CRecordVectorI12CUpdatePair2E,"aG",@progbits,_ZTV13CRecordVectorI12CUpdatePair2E,comdat
	.weak	_ZTV13CRecordVectorI12CUpdatePair2E
	.p2align	3
_ZTV13CRecordVectorI12CUpdatePair2E:
	.quad	0
	.quad	_ZTI13CRecordVectorI12CUpdatePair2E
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorI12CUpdatePair2ED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorI12CUpdatePair2E, 40

	.type	_ZTS13CRecordVectorI12CUpdatePair2E,@object # @_ZTS13CRecordVectorI12CUpdatePair2E
	.section	.rodata._ZTS13CRecordVectorI12CUpdatePair2E,"aG",@progbits,_ZTS13CRecordVectorI12CUpdatePair2E,comdat
	.weak	_ZTS13CRecordVectorI12CUpdatePair2E
	.p2align	4
_ZTS13CRecordVectorI12CUpdatePair2E:
	.asciz	"13CRecordVectorI12CUpdatePair2E"
	.size	_ZTS13CRecordVectorI12CUpdatePair2E, 32

	.type	_ZTI13CRecordVectorI12CUpdatePair2E,@object # @_ZTI13CRecordVectorI12CUpdatePair2E
	.section	.rodata._ZTI13CRecordVectorI12CUpdatePair2E,"aG",@progbits,_ZTI13CRecordVectorI12CUpdatePair2E,comdat
	.weak	_ZTI13CRecordVectorI12CUpdatePair2E
	.p2align	4
_ZTI13CRecordVectorI12CUpdatePair2E:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorI12CUpdatePair2E
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorI12CUpdatePair2E, 24

	.type	_ZTV13CRecordVectorI11CUpdatePairE,@object # @_ZTV13CRecordVectorI11CUpdatePairE
	.section	.rodata._ZTV13CRecordVectorI11CUpdatePairE,"aG",@progbits,_ZTV13CRecordVectorI11CUpdatePairE,comdat
	.weak	_ZTV13CRecordVectorI11CUpdatePairE
	.p2align	3
_ZTV13CRecordVectorI11CUpdatePairE:
	.quad	0
	.quad	_ZTI13CRecordVectorI11CUpdatePairE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorI11CUpdatePairED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorI11CUpdatePairE, 40

	.type	_ZTS13CRecordVectorI11CUpdatePairE,@object # @_ZTS13CRecordVectorI11CUpdatePairE
	.section	.rodata._ZTS13CRecordVectorI11CUpdatePairE,"aG",@progbits,_ZTS13CRecordVectorI11CUpdatePairE,comdat
	.weak	_ZTS13CRecordVectorI11CUpdatePairE
	.p2align	4
_ZTS13CRecordVectorI11CUpdatePairE:
	.asciz	"13CRecordVectorI11CUpdatePairE"
	.size	_ZTS13CRecordVectorI11CUpdatePairE, 31

	.type	_ZTI13CRecordVectorI11CUpdatePairE,@object # @_ZTI13CRecordVectorI11CUpdatePairE
	.section	.rodata._ZTI13CRecordVectorI11CUpdatePairE,"aG",@progbits,_ZTI13CRecordVectorI11CUpdatePairE,comdat
	.weak	_ZTI13CRecordVectorI11CUpdatePairE
	.p2align	4
_ZTI13CRecordVectorI11CUpdatePairE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorI11CUpdatePairE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorI11CUpdatePairE, 24

	.type	_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,@object # @_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.section	.rodata._ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,"aG",@progbits,_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,comdat
	.weak	_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.p2align	3
_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE:
	.quad	0
	.quad	_ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.quad	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED2Ev
	.quad	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEED0Ev
	.quad	_ZN13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE6DeleteEii
	.size	_ZTV13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE, 40

	.type	_ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,@object # @_ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.section	.rodata._ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,"aG",@progbits,_ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,comdat
	.weak	_ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.p2align	4
_ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE:
	.asciz	"13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE"
	.size	_ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE, 56

	.type	_ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,@object # @_ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.section	.rodata._ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,"aG",@progbits,_ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE,comdat
	.weak	_ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.p2align	4
_ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorIN18COutMultiVolStream14CSubStreamInfoEE, 24

	.type	_ZTV13CRecordVectorIyE,@object # @_ZTV13CRecordVectorIyE
	.section	.rodata._ZTV13CRecordVectorIyE,"aG",@progbits,_ZTV13CRecordVectorIyE,comdat
	.weak	_ZTV13CRecordVectorIyE
	.p2align	3
_ZTV13CRecordVectorIyE:
	.quad	0
	.quad	_ZTI13CRecordVectorIyE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIyED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIyE, 40

	.type	_ZTS13CRecordVectorIyE,@object # @_ZTS13CRecordVectorIyE
	.section	.rodata._ZTS13CRecordVectorIyE,"aG",@progbits,_ZTS13CRecordVectorIyE,comdat
	.weak	_ZTS13CRecordVectorIyE
	.p2align	4
_ZTS13CRecordVectorIyE:
	.asciz	"13CRecordVectorIyE"
	.size	_ZTS13CRecordVectorIyE, 19

	.type	_ZTI13CRecordVectorIyE,@object # @_ZTI13CRecordVectorIyE
	.section	.rodata._ZTI13CRecordVectorIyE,"aG",@progbits,_ZTI13CRecordVectorIyE,comdat
	.weak	_ZTI13CRecordVectorIyE
	.p2align	4
_ZTI13CRecordVectorIyE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIyE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIyE, 24

	.type	_ZTVN8NWindows5NFile3NIO7CInFileE,@object # @_ZTVN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTVN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTVN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTVN8NWindows5NFile3NIO7CInFileE
	.p2align	3
_ZTVN8NWindows5NFile3NIO7CInFileE:
	.quad	0
	.quad	_ZTIN8NWindows5NFile3NIO7CInFileE
	.quad	_ZN8NWindows5NFile3NIO9CFileBaseD2Ev
	.quad	_ZN8NWindows5NFile3NIO7CInFileD0Ev
	.quad	_ZN8NWindows5NFile3NIO9CFileBase5CloseEv
	.size	_ZTVN8NWindows5NFile3NIO7CInFileE, 40

	.type	_ZTSN8NWindows5NFile3NIO7CInFileE,@object # @_ZTSN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTSN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTSN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTSN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTSN8NWindows5NFile3NIO7CInFileE:
	.asciz	"N8NWindows5NFile3NIO7CInFileE"
	.size	_ZTSN8NWindows5NFile3NIO7CInFileE, 30

	.type	_ZTIN8NWindows5NFile3NIO7CInFileE,@object # @_ZTIN8NWindows5NFile3NIO7CInFileE
	.section	.rodata._ZTIN8NWindows5NFile3NIO7CInFileE,"aG",@progbits,_ZTIN8NWindows5NFile3NIO7CInFileE,comdat
	.weak	_ZTIN8NWindows5NFile3NIO7CInFileE
	.p2align	4
_ZTIN8NWindows5NFile3NIO7CInFileE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTSN8NWindows5NFile3NIO7CInFileE
	.quad	_ZTIN8NWindows5NFile3NIO9CFileBaseE
	.size	_ZTIN8NWindows5NFile3NIO7CInFileE, 24

	.type	_ZTV13CObjectVectorI11CStringBaseIwEE,@object # @_ZTV13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTV13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTV13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTV13CObjectVectorI11CStringBaseIwEE
	.p2align	3
_ZTV13CObjectVectorI11CStringBaseIwEE:
	.quad	0
	.quad	_ZTI13CObjectVectorI11CStringBaseIwEE
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED2Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEED0Ev
	.quad	_ZN13CObjectVectorI11CStringBaseIwEE6DeleteEii
	.size	_ZTV13CObjectVectorI11CStringBaseIwEE, 40

	.type	_ZTS13CObjectVectorI11CStringBaseIwEE,@object # @_ZTS13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTS13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTS13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTS13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTS13CObjectVectorI11CStringBaseIwEE:
	.asciz	"13CObjectVectorI11CStringBaseIwEE"
	.size	_ZTS13CObjectVectorI11CStringBaseIwEE, 34

	.type	_ZTI13CObjectVectorI11CStringBaseIwEE,@object # @_ZTI13CObjectVectorI11CStringBaseIwEE
	.section	.rodata._ZTI13CObjectVectorI11CStringBaseIwEE,"aG",@progbits,_ZTI13CObjectVectorI11CStringBaseIwEE,comdat
	.weak	_ZTI13CObjectVectorI11CStringBaseIwEE
	.p2align	4
_ZTI13CObjectVectorI11CStringBaseIwEE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI11CStringBaseIwEE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI11CStringBaseIwEE, 24

	.type	_ZTV13CRecordVectorIiE,@object # @_ZTV13CRecordVectorIiE
	.section	.rodata._ZTV13CRecordVectorIiE,"aG",@progbits,_ZTV13CRecordVectorIiE,comdat
	.weak	_ZTV13CRecordVectorIiE
	.p2align	3
_ZTV13CRecordVectorIiE:
	.quad	0
	.quad	_ZTI13CRecordVectorIiE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIiED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIiE, 40

	.type	_ZTS13CRecordVectorIiE,@object # @_ZTS13CRecordVectorIiE
	.section	.rodata._ZTS13CRecordVectorIiE,"aG",@progbits,_ZTS13CRecordVectorIiE,comdat
	.weak	_ZTS13CRecordVectorIiE
	.p2align	4
_ZTS13CRecordVectorIiE:
	.asciz	"13CRecordVectorIiE"
	.size	_ZTS13CRecordVectorIiE, 19

	.type	_ZTI13CRecordVectorIiE,@object # @_ZTI13CRecordVectorIiE
	.section	.rodata._ZTI13CRecordVectorIiE,"aG",@progbits,_ZTI13CRecordVectorIiE,comdat
	.weak	_ZTI13CRecordVectorIiE
	.p2align	4
_ZTI13CRecordVectorIiE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIiE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIiE, 24

	.type	_ZTV13CRecordVectorIjE,@object # @_ZTV13CRecordVectorIjE
	.section	.rodata._ZTV13CRecordVectorIjE,"aG",@progbits,_ZTV13CRecordVectorIjE,comdat
	.weak	_ZTV13CRecordVectorIjE
	.p2align	3
_ZTV13CRecordVectorIjE:
	.quad	0
	.quad	_ZTI13CRecordVectorIjE
	.quad	_ZN17CBaseRecordVectorD2Ev
	.quad	_ZN13CRecordVectorIjED0Ev
	.quad	_ZN17CBaseRecordVector6DeleteEii
	.size	_ZTV13CRecordVectorIjE, 40

	.type	_ZTS13CRecordVectorIjE,@object # @_ZTS13CRecordVectorIjE
	.section	.rodata._ZTS13CRecordVectorIjE,"aG",@progbits,_ZTS13CRecordVectorIjE,comdat
	.weak	_ZTS13CRecordVectorIjE
	.p2align	4
_ZTS13CRecordVectorIjE:
	.asciz	"13CRecordVectorIjE"
	.size	_ZTS13CRecordVectorIjE, 19

	.type	_ZTI13CRecordVectorIjE,@object # @_ZTI13CRecordVectorIjE
	.section	.rodata._ZTI13CRecordVectorIjE,"aG",@progbits,_ZTI13CRecordVectorIjE,comdat
	.weak	_ZTI13CRecordVectorIjE
	.p2align	4
_ZTI13CRecordVectorIjE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CRecordVectorIjE
	.quad	_ZTI17CBaseRecordVector
	.size	_ZTI13CRecordVectorIjE, 24

	.type	_ZTV13CObjectVectorI8CArcItemE,@object # @_ZTV13CObjectVectorI8CArcItemE
	.section	.rodata._ZTV13CObjectVectorI8CArcItemE,"aG",@progbits,_ZTV13CObjectVectorI8CArcItemE,comdat
	.weak	_ZTV13CObjectVectorI8CArcItemE
	.p2align	3
_ZTV13CObjectVectorI8CArcItemE:
	.quad	0
	.quad	_ZTI13CObjectVectorI8CArcItemE
	.quad	_ZN13CObjectVectorI8CArcItemED2Ev
	.quad	_ZN13CObjectVectorI8CArcItemED0Ev
	.quad	_ZN13CObjectVectorI8CArcItemE6DeleteEii
	.size	_ZTV13CObjectVectorI8CArcItemE, 40

	.type	_ZTS13CObjectVectorI8CArcItemE,@object # @_ZTS13CObjectVectorI8CArcItemE
	.section	.rodata._ZTS13CObjectVectorI8CArcItemE,"aG",@progbits,_ZTS13CObjectVectorI8CArcItemE,comdat
	.weak	_ZTS13CObjectVectorI8CArcItemE
	.p2align	4
_ZTS13CObjectVectorI8CArcItemE:
	.asciz	"13CObjectVectorI8CArcItemE"
	.size	_ZTS13CObjectVectorI8CArcItemE, 27

	.type	_ZTI13CObjectVectorI8CArcItemE,@object # @_ZTI13CObjectVectorI8CArcItemE
	.section	.rodata._ZTI13CObjectVectorI8CArcItemE,"aG",@progbits,_ZTI13CObjectVectorI8CArcItemE,comdat
	.weak	_ZTI13CObjectVectorI8CArcItemE
	.p2align	4
_ZTI13CObjectVectorI8CArcItemE:
	.quad	_ZTVN10__cxxabiv120__si_class_type_infoE+16
	.quad	_ZTS13CObjectVectorI8CArcItemE
	.quad	_ZTI13CRecordVectorIPvE
	.size	_ZTI13CObjectVectorI8CArcItemE, 24


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
