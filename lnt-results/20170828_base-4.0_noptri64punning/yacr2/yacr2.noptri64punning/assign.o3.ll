; ModuleID = 'yacr2.noptri64punning/assign.bc'
source_filename = "assign.i"
target datalayout = "e-m:e-i64:64-f80:128-n8:16:32:64-S128"
target triple = "x86_64-unknown-linux-gnu"

%struct._nodeVCGType = type { %struct._constraintVCGType*, i64, i64, i64, %struct._constraintVCGType*, i64, i64, i64 }
%struct._constraintVCGType = type { i64, i64, i64, i64 }
%struct._nodeHCGType = type { i64*, i64, i64 }

@channelNets = external local_unnamed_addr global i64, align 8
@costMatrix = common local_unnamed_addr global i64** null, align 8
@channelTracks = external local_unnamed_addr global i64, align 8
@tracksNotPref = common local_unnamed_addr global i64* null, align 8
@tracksTopNotPref = common local_unnamed_addr global i64* null, align 8
@tracksBotNotPref = common local_unnamed_addr global i64* null, align 8
@tracksNoHCV = common local_unnamed_addr global i64* null, align 8
@tracksAssign = common local_unnamed_addr global i64* null, align 8
@netsAssign = common local_unnamed_addr global i64* null, align 8
@netsAssignCopy = common local_unnamed_addr global i64* null, align 8
@FIRST = external local_unnamed_addr global i64*, align 8
@channelDensityColumn = external local_unnamed_addr global i64, align 8
@LAST = external local_unnamed_addr global i64*, align 8
@CROSSING = external local_unnamed_addr global i64*, align 8
@VCG = external local_unnamed_addr global %struct._nodeVCGType*, align 8
@HCG = external local_unnamed_addr global %struct._nodeHCGType*, align 8
@channelColumns = external local_unnamed_addr global i64, align 8
@TOP = external local_unnamed_addr global i64*, align 8
@BOT = external local_unnamed_addr global i64*, align 8
@cardTopNotPref = common local_unnamed_addr global i64 0, align 8
@cardBotNotPref = common local_unnamed_addr global i64 0, align 8
@cardNotPref = common local_unnamed_addr global i64 0, align 8

; Function Attrs: nounwind uwtable
define void @AllocAssign() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %add = shl i64 %0, 3
  %mul = add i64 %add, 8
  %call = tail call noalias i8* @malloc(i64 %mul) #4
  store i8* %call, i8** bitcast (i64*** @costMatrix to i8**), align 8, !tbaa !5
  %cmp4 = icmp eq i64 %0, 0
  %1 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %add15 = shl i64 %1, 3
  %mul26 = add i64 %add15, 16
  %call37 = tail call noalias i8* @malloc(i64 %mul26) #4
  br i1 %cmp4, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %arrayidx16 = getelementptr inbounds i8, i8* %call, i64 8
  %2 = bitcast i8* %arrayidx16 to i8**
  store i8* %call37, i8** %2, align 8, !tbaa !5
  %cmp17 = icmp eq i64 %0, 1
  %call318 = tail call noalias i8* @malloc(i64 %mul26) #4
  br i1 %cmp17, label %for.end.loopexit, label %for.body.for.body_crit_edge.preheader

for.body.for.body_crit_edge.preheader:            ; preds = %for.body.lr.ph
  br label %for.body.for.body_crit_edge

for.body.for.body_crit_edge:                      ; preds = %for.body.for.body_crit_edge.preheader, %for.body.for.body_crit_edge
  %call320 = phi i8* [ %call3, %for.body.for.body_crit_edge ], [ %call318, %for.body.for.body_crit_edge.preheader ]
  %inc19 = phi i64 [ %inc, %for.body.for.body_crit_edge ], [ 2, %for.body.for.body_crit_edge.preheader ]
  %.pre = load i64**, i64*** @costMatrix, align 8, !tbaa !5
  %arrayidx = getelementptr inbounds i64*, i64** %.pre, i64 %inc19
  %3 = bitcast i64** %arrayidx to i8**
  store i8* %call320, i8** %3, align 8, !tbaa !5
  %inc = add i64 %inc19, 1
  %cmp = icmp ugt i64 %inc, %0
  %call3 = tail call noalias i8* @malloc(i64 %mul26) #4
  br i1 %cmp, label %for.end.loopexit.loopexit, label %for.body.for.body_crit_edge

for.end.loopexit.loopexit:                        ; preds = %for.body.for.body_crit_edge
  br label %for.end.loopexit

for.end.loopexit:                                 ; preds = %for.end.loopexit.loopexit, %for.body.lr.ph
  %call3.lcssa15 = phi i8* [ %call318, %for.body.lr.ph ], [ %call3, %for.end.loopexit.loopexit ]
  %phitmp = shl i64 %0, 3
  %phitmp13 = add i64 %phitmp, 8
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %4 = phi i64 [ 8, %entry ], [ %phitmp13, %for.end.loopexit ]
  %call3.lcssa = phi i8* [ %call37, %entry ], [ %call3.lcssa15, %for.end.loopexit ]
  store i8* %call3.lcssa, i8** bitcast (i64** @tracksNotPref to i8**), align 8, !tbaa !5
  %call9 = tail call noalias i8* @malloc(i64 %mul26) #4
  store i8* %call9, i8** bitcast (i64** @tracksTopNotPref to i8**), align 8, !tbaa !5
  %call12 = tail call noalias i8* @malloc(i64 %mul26) #4
  store i8* %call12, i8** bitcast (i64** @tracksBotNotPref to i8**), align 8, !tbaa !5
  %call15 = tail call noalias i8* @malloc(i64 %mul26) #4
  store i8* %call15, i8** bitcast (i64** @tracksNoHCV to i8**), align 8, !tbaa !5
  %call18 = tail call noalias i8* @malloc(i64 %mul26) #4
  store i8* %call18, i8** bitcast (i64** @tracksAssign to i8**), align 8, !tbaa !5
  %call21 = tail call noalias i8* @malloc(i64 %4) #4
  store i8* %call21, i8** bitcast (i64** @netsAssign to i8**), align 8, !tbaa !5
  %call24 = tail call noalias i8* @malloc(i64 %4) #4
  store i8* %call24, i8** bitcast (i64** @netsAssignCopy to i8**), align 8, !tbaa !5
  ret void
}

; Function Attrs: nounwind
declare noalias i8* @malloc(i64) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define void @FreeAssign() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp4 = icmp eq i64 %0, 0
  %1 = load i64**, i64*** @costMatrix, align 8, !tbaa !5
  br i1 %cmp4, label %for.end, label %for.body.preheader

for.body.preheader:                               ; preds = %entry
  br label %for.body

for.body:                                         ; preds = %for.body.preheader, %for.body
  %2 = phi i64** [ %6, %for.body ], [ %1, %for.body.preheader ]
  %net.05 = phi i64 [ %inc, %for.body ], [ 1, %for.body.preheader ]
  %arrayidx = getelementptr inbounds i64*, i64** %2, i64 %net.05
  %3 = bitcast i64** %arrayidx to i8**
  %4 = load i8*, i8** %3, align 8, !tbaa !5
  tail call void @free(i8* %4) #4
  %inc = add i64 %net.05, 1
  %5 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc, %5
  %6 = load i64**, i64*** @costMatrix, align 8, !tbaa !5
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %.lcssa = phi i64** [ %1, %entry ], [ %6, %for.end.loopexit ]
  %7 = bitcast i64** %.lcssa to i8*
  tail call void @free(i8* %7) #4
  %8 = load i8*, i8** bitcast (i64** @tracksNotPref to i8**), align 8, !tbaa !5
  tail call void @free(i8* %8) #4
  %9 = load i8*, i8** bitcast (i64** @tracksTopNotPref to i8**), align 8, !tbaa !5
  tail call void @free(i8* %9) #4
  %10 = load i8*, i8** bitcast (i64** @tracksBotNotPref to i8**), align 8, !tbaa !5
  tail call void @free(i8* %10) #4
  %11 = load i8*, i8** bitcast (i64** @tracksNoHCV to i8**), align 8, !tbaa !5
  tail call void @free(i8* %11) #4
  %12 = load i8*, i8** bitcast (i64** @tracksAssign to i8**), align 8, !tbaa !5
  tail call void @free(i8* %12) #4
  %13 = load i8*, i8** bitcast (i64** @netsAssign to i8**), align 8, !tbaa !5
  tail call void @free(i8* %13) #4
  %14 = load i8*, i8** bitcast (i64** @netsAssignCopy to i8**), align 8, !tbaa !5
  tail call void @free(i8* %14) #4
  ret void
}

; Function Attrs: nounwind
declare void @free(i8* nocapture) local_unnamed_addr #1

; Function Attrs: nounwind uwtable
define void @NetsAssign() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp4 = icmp eq i64 %0, 0
  br i1 %cmp4, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %1 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.body
  %net.05 = phi i64 [ 1, %for.body.lr.ph ], [ %inc, %for.body ]
  %arrayidx = getelementptr inbounds i64, i64* %1, i64 %net.05
  store i64 0, i64* %arrayidx, align 8, !tbaa !1
  %inc = add i64 %net.05, 1
  %2 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc, %2
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  tail call void @MaxNetsAssign()
  tail call void @LeftNetsAssign()
  tail call void @RightNetsAssign()
  ret void
}

; Function Attrs: nounwind uwtable
define void @MaxNetsAssign() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp22 = icmp eq i64 %0, 0
  br i1 %cmp22, label %while.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %1 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %2 = load i64*, i64** @CROSSING, align 8
  %3 = load i64*, i64** @LAST, align 8
  br label %for.body

while.cond.preheader:                             ; preds = %for.inc
  %cmp720 = icmp eq i64 %netsCrossing.1, 0
  br i1 %cmp720, label %while.end, label %while.body.preheader

while.body.preheader:                             ; preds = %while.cond.preheader
  br label %while.body

for.body:                                         ; preds = %for.body.lr.ph, %for.inc
  %netsCrossing.025 = phi i64 [ 0, %for.body.lr.ph ], [ %netsCrossing.1, %for.inc ]
  %net.023 = phi i64 [ 1, %for.body.lr.ph ], [ %inc6, %for.inc ]
  %arrayidx = getelementptr inbounds i64, i64* %1, i64 %net.023
  %4 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %5 = load i64, i64* @channelDensityColumn, align 8, !tbaa !1
  %cmp1 = icmp ugt i64 %4, %5
  br i1 %cmp1, label %if.else, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body
  %arrayidx2 = getelementptr inbounds i64, i64* %3, i64 %net.023
  %6 = load i64, i64* %arrayidx2, align 8, !tbaa !1
  %cmp3 = icmp ult i64 %6, %5
  br i1 %cmp3, label %if.else, label %if.then

if.then:                                          ; preds = %land.lhs.true
  %arrayidx4 = getelementptr inbounds i64, i64* %2, i64 %net.023
  store i64 1, i64* %arrayidx4, align 8, !tbaa !1
  %inc = add i64 %netsCrossing.025, 1
  br label %for.inc

if.else:                                          ; preds = %land.lhs.true, %for.body
  %arrayidx5 = getelementptr inbounds i64, i64* %2, i64 %net.023
  store i64 0, i64* %arrayidx5, align 8, !tbaa !1
  br label %for.inc

for.inc:                                          ; preds = %if.then, %if.else
  %netsCrossing.1 = phi i64 [ %inc, %if.then ], [ %netsCrossing.025, %if.else ]
  %inc6 = add i64 %net.023, 1
  %7 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc6, %7
  br i1 %cmp, label %while.cond.preheader, label %for.body

while.body:                                       ; preds = %while.body.preheader, %Select.exit
  %netsCrossing.221 = phi i64 [ %dec, %Select.exit ], [ %netsCrossing.1, %while.body.preheader ]
  %8 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %9 = load %struct._nodeHCGType*, %struct._nodeHCGType** @HCG, align 8, !tbaa !5
  %10 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %11 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  tail call void @BuildCostMatrix(%struct._nodeVCGType* %8, %struct._nodeHCGType* %9, i64* %10, i64* %11) #4
  %12 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i15 = icmp eq i64 %12, 0
  br i1 %cmp.i15, label %Select.exit, label %for.body.i.lr.ph

for.body.i.lr.ph:                                 ; preds = %while.body
  %13 = load i64**, i64*** @costMatrix, align 8
  %14 = load i64, i64* @channelTracks, align 8
  %cmp3.i12 = icmp eq i64 %14, 0
  br i1 %cmp3.i12, label %for.body.i.us.preheader, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %for.body.i.lr.ph
  br label %for.body.i

for.body.i.us.preheader:                          ; preds = %for.body.i.lr.ph
  br label %for.body.i.us

for.body.i.us:                                    ; preds = %for.body.i.us.preheader, %for.inc9.i.us
  %largest.0.i18.us = phi i64 [ %largest.1.i.us, %for.inc9.i.us ], [ -1, %for.body.i.us.preheader ]
  %net.0.i17.us = phi i64 [ %inc10.i.us, %for.inc9.i.us ], [ 1, %for.body.i.us.preheader ]
  %select.0.i16.us = phi i64 [ %select.1.i.us, %for.inc9.i.us ], [ 0, %for.body.i.us.preheader ]
  %arrayidx.i.us = getelementptr inbounds i64, i64* %11, i64 %net.0.i17.us
  %15 = load i64, i64* %arrayidx.i.us, align 8, !tbaa !1
  %tobool.i.us = icmp eq i64 %15, 0
  br i1 %tobool.i.us, label %for.inc9.i.us, label %if.then.i.us

if.then.i.us:                                     ; preds = %for.body.i.us
  %cmp6.i.us = icmp slt i64 %largest.0.i18.us, 0
  %net.0.select.0.i.us = select i1 %cmp6.i.us, i64 %net.0.i17.us, i64 %select.0.i16.us
  %cost.0.largest.0.i.us = select i1 %cmp6.i.us, i64 0, i64 %largest.0.i18.us
  br label %for.inc9.i.us

for.inc9.i.us:                                    ; preds = %if.then.i.us, %for.body.i.us
  %select.1.i.us = phi i64 [ %select.0.i16.us, %for.body.i.us ], [ %net.0.select.0.i.us, %if.then.i.us ]
  %largest.1.i.us = phi i64 [ %largest.0.i18.us, %for.body.i.us ], [ %cost.0.largest.0.i.us, %if.then.i.us ]
  %inc10.i.us = add i64 %net.0.i17.us, 1
  %cmp.i.us = icmp ugt i64 %inc10.i.us, %12
  br i1 %cmp.i.us, label %Select.exit.loopexit, label %for.body.i.us

for.body.i:                                       ; preds = %for.body.i.preheader, %for.inc9.i
  %largest.0.i18 = phi i64 [ %largest.1.i, %for.inc9.i ], [ -1, %for.body.i.preheader ]
  %net.0.i17 = phi i64 [ %inc10.i, %for.inc9.i ], [ 1, %for.body.i.preheader ]
  %select.0.i16 = phi i64 [ %select.1.i, %for.inc9.i ], [ 0, %for.body.i.preheader ]
  %arrayidx.i = getelementptr inbounds i64, i64* %11, i64 %net.0.i17
  %16 = load i64, i64* %arrayidx.i, align 8, !tbaa !1
  %tobool.i = icmp eq i64 %16, 0
  br i1 %tobool.i, label %for.inc9.i, label %if.then.i

if.then.i:                                        ; preds = %for.body.i
  %arrayidx1.i = getelementptr inbounds i64*, i64** %13, i64 %net.0.i17
  %17 = load i64*, i64** %arrayidx1.i, align 8, !tbaa !5
  br label %for.body4.i

for.body4.i:                                      ; preds = %if.then.i, %for.body4.i
  %cost.0.i14 = phi i64 [ 0, %if.then.i ], [ %add.i, %for.body4.i ]
  %track.0.i13 = phi i64 [ 1, %if.then.i ], [ %inc.i, %for.body4.i ]
  %arrayidx5.i = getelementptr inbounds i64, i64* %17, i64 %track.0.i13
  %18 = load i64, i64* %arrayidx5.i, align 8, !tbaa !1
  %add.i = add nsw i64 %18, %cost.0.i14
  %inc.i = add i64 %track.0.i13, 1
  %cmp3.i = icmp ugt i64 %inc.i, %14
  br i1 %cmp3.i, label %for.end.i, label %for.body4.i

for.end.i:                                        ; preds = %for.body4.i
  %cmp6.i = icmp sgt i64 %add.i, %largest.0.i18
  %net.0.select.0.i = select i1 %cmp6.i, i64 %net.0.i17, i64 %select.0.i16
  %cost.0.largest.0.i = select i1 %cmp6.i, i64 %add.i, i64 %largest.0.i18
  br label %for.inc9.i

for.inc9.i:                                       ; preds = %for.end.i, %for.body.i
  %select.1.i = phi i64 [ %select.0.i16, %for.body.i ], [ %net.0.select.0.i, %for.end.i ]
  %largest.1.i = phi i64 [ %largest.0.i18, %for.body.i ], [ %cost.0.largest.0.i, %for.end.i ]
  %inc10.i = add i64 %net.0.i17, 1
  %cmp.i = icmp ugt i64 %inc10.i, %12
  br i1 %cmp.i, label %Select.exit.loopexit31, label %for.body.i

Select.exit.loopexit:                             ; preds = %for.inc9.i.us
  br label %Select.exit

Select.exit.loopexit31:                           ; preds = %for.inc9.i
  br label %Select.exit

Select.exit:                                      ; preds = %Select.exit.loopexit31, %Select.exit.loopexit, %while.body
  %select.0.i.lcssa = phi i64 [ 0, %while.body ], [ %select.1.i.us, %Select.exit.loopexit ], [ %select.1.i, %Select.exit.loopexit31 ]
  %19 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %20 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  tail call void @Assign(%struct._nodeVCGType* %19, i64* %20, i64 %select.0.i.lcssa)
  %21 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx8 = getelementptr inbounds i64, i64* %21, i64 %select.0.i.lcssa
  store i64 0, i64* %arrayidx8, align 8, !tbaa !1
  %dec = add i64 %netsCrossing.221, -1
  %cmp7 = icmp eq i64 %dec, 0
  br i1 %cmp7, label %while.end.loopexit, label %while.body

while.end.loopexit:                               ; preds = %Select.exit
  br label %while.end

while.end:                                        ; preds = %while.end.loopexit, %entry, %while.cond.preheader
  ret void
}

; Function Attrs: nounwind uwtable
define void @LeftNetsAssign() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp45 = icmp eq i64 %0, 0
  br i1 %cmp45, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %1 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.body
  %net.046 = phi i64 [ 1, %for.body.lr.ph ], [ %inc, %for.body ]
  %arrayidx = getelementptr inbounds i64, i64* %1, i64 %net.046
  store i64 0, i64* %arrayidx, align 8, !tbaa !1
  %inc = add i64 %net.046, 1
  %2 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc, %2
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %3 = load i64, i64* @channelDensityColumn, align 8, !tbaa !1
  %col.041 = add i64 %3, -1
  %cmp242 = icmp eq i64 %col.041, 0
  br i1 %cmp242, label %for.end39, label %for.body3.preheader

for.body3.preheader:                              ; preds = %for.end
  br label %for.body3

for.body3:                                        ; preds = %for.body3.preheader, %for.cond1.backedge
  %col.044 = phi i64 [ %col.0, %for.cond1.backedge ], [ %col.041, %for.body3.preheader ]
  %netsCrossing.043 = phi i64 [ %netsCrossing.0.be, %for.cond1.backedge ], [ 0, %for.body3.preheader ]
  %4 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx4 = getelementptr inbounds i64, i64* %4, i64 %col.044
  %5 = load i64, i64* %arrayidx4, align 8, !tbaa !1
  %6 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx5 = getelementptr inbounds i64, i64* %6, i64 %col.044
  %7 = load i64, i64* %arrayidx5, align 8, !tbaa !1
  %cmp6 = icmp eq i64 %5, %7
  %tobool = icmp ne i64 %5, 0
  br i1 %cmp6, label %if.else, label %if.then

if.then:                                          ; preds = %for.body3
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %8 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx7 = getelementptr inbounds i64, i64* %8, i64 %5
  %9 = load i64, i64* %arrayidx7, align 8, !tbaa !1
  %cmp8 = icmp eq i64 %9, %col.044
  br i1 %cmp8, label %if.then9, label %if.end

if.then9:                                         ; preds = %land.lhs.true
  %10 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx10 = getelementptr inbounds i64, i64* %10, i64 %5
  store i64 1, i64* %arrayidx10, align 8, !tbaa !1
  %inc11 = add i64 %netsCrossing.043, 1
  br label %if.end

if.end:                                           ; preds = %if.then9, %land.lhs.true, %if.then
  %netsCrossing.1 = phi i64 [ %inc11, %if.then9 ], [ %netsCrossing.043, %land.lhs.true ], [ %netsCrossing.043, %if.then ]
  %tobool12 = icmp eq i64 %7, 0
  br i1 %tobool12, label %if.end28, label %land.lhs.true13

land.lhs.true13:                                  ; preds = %if.end
  %11 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx14 = getelementptr inbounds i64, i64* %11, i64 %7
  %12 = load i64, i64* %arrayidx14, align 8, !tbaa !1
  %cmp15 = icmp eq i64 %12, %col.044
  br i1 %cmp15, label %if.then16, label %if.end28

if.then16:                                        ; preds = %land.lhs.true13
  %13 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx17 = getelementptr inbounds i64, i64* %13, i64 %7
  store i64 1, i64* %arrayidx17, align 8, !tbaa !1
  %inc18 = add i64 %netsCrossing.1, 1
  br label %if.end28

if.else:                                          ; preds = %for.body3
  br i1 %tobool, label %land.lhs.true21, label %if.end28

land.lhs.true21:                                  ; preds = %if.else
  %14 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx22 = getelementptr inbounds i64, i64* %14, i64 %5
  %15 = load i64, i64* %arrayidx22, align 8, !tbaa !1
  %cmp23 = icmp eq i64 %15, %col.044
  br i1 %cmp23, label %if.then24, label %if.end28

if.then24:                                        ; preds = %land.lhs.true21
  %16 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx25 = getelementptr inbounds i64, i64* %16, i64 %5
  store i64 1, i64* %arrayidx25, align 8, !tbaa !1
  %inc26 = add i64 %netsCrossing.043, 1
  br label %if.end28

if.end28:                                         ; preds = %if.end, %if.else, %land.lhs.true21, %if.then24, %land.lhs.true13, %if.then16
  %netsCrossing.2 = phi i64 [ %inc18, %if.then16 ], [ %netsCrossing.1, %land.lhs.true13 ], [ %netsCrossing.1, %if.end ], [ %inc26, %if.then24 ], [ %netsCrossing.043, %land.lhs.true21 ], [ %netsCrossing.043, %if.else ]
  %17 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx29 = getelementptr inbounds i64, i64* %17, i64 %5
  %18 = load i64, i64* %arrayidx29, align 8, !tbaa !1
  %cmp30 = icmp eq i64 %18, %col.044
  br i1 %cmp30, label %while.cond, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end28
  %arrayidx31 = getelementptr inbounds i64, i64* %17, i64 %7
  %19 = load i64, i64* %arrayidx31, align 8, !tbaa !1
  %cmp32 = icmp eq i64 %19, %col.044
  %cmp34 = icmp ne i64 %netsCrossing.2, 0
  %or.cond = and i1 %cmp34, %cmp32
  br i1 %or.cond, label %while.body, label %for.cond1.backedge

while.cond:                                       ; preds = %if.end28, %Select.exit
  %netsCrossing.3 = phi i64 [ %netsCrossing.2, %if.end28 ], [ %dec, %Select.exit ]
  %cmp34.old = icmp eq i64 %netsCrossing.3, 0
  br i1 %cmp34.old, label %for.cond1.backedge, label %while.body

for.cond1.backedge:                               ; preds = %while.cond, %lor.lhs.false
  %netsCrossing.0.be = phi i64 [ 0, %while.cond ], [ %netsCrossing.2, %lor.lhs.false ]
  %col.0 = add i64 %col.044, -1
  %cmp2 = icmp eq i64 %col.0, 0
  br i1 %cmp2, label %for.end39.loopexit, label %for.body3

while.body:                                       ; preds = %while.cond, %lor.lhs.false
  %netsCrossing.4 = phi i64 [ %netsCrossing.3, %while.cond ], [ %netsCrossing.2, %lor.lhs.false ]
  %20 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %21 = load %struct._nodeHCGType*, %struct._nodeHCGType** @HCG, align 8, !tbaa !5
  %22 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %23 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  tail call void @BuildCostMatrix(%struct._nodeVCGType* %20, %struct._nodeHCGType* %21, i64* %22, i64* %23) #4
  %24 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i36 = icmp eq i64 %24, 0
  br i1 %cmp.i36, label %Select.exit, label %for.body.i.lr.ph

for.body.i.lr.ph:                                 ; preds = %while.body
  %25 = load i64**, i64*** @costMatrix, align 8
  %26 = load i64, i64* @channelTracks, align 8
  %cmp3.i33 = icmp eq i64 %26, 0
  br i1 %cmp3.i33, label %for.body.i.us.preheader, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %for.body.i.lr.ph
  br label %for.body.i

for.body.i.us.preheader:                          ; preds = %for.body.i.lr.ph
  br label %for.body.i.us

for.body.i.us:                                    ; preds = %for.body.i.us.preheader, %for.inc9.i.us
  %largest.0.i39.us = phi i64 [ %largest.1.i.us, %for.inc9.i.us ], [ -1, %for.body.i.us.preheader ]
  %net.0.i38.us = phi i64 [ %inc10.i.us, %for.inc9.i.us ], [ 1, %for.body.i.us.preheader ]
  %select.0.i37.us = phi i64 [ %select.1.i.us, %for.inc9.i.us ], [ 0, %for.body.i.us.preheader ]
  %arrayidx.i.us = getelementptr inbounds i64, i64* %23, i64 %net.0.i38.us
  %27 = load i64, i64* %arrayidx.i.us, align 8, !tbaa !1
  %tobool.i.us = icmp eq i64 %27, 0
  br i1 %tobool.i.us, label %for.inc9.i.us, label %if.then.i.us

if.then.i.us:                                     ; preds = %for.body.i.us
  %cmp6.i.us = icmp slt i64 %largest.0.i39.us, 0
  %net.0.select.0.i.us = select i1 %cmp6.i.us, i64 %net.0.i38.us, i64 %select.0.i37.us
  %cost.0.largest.0.i.us = select i1 %cmp6.i.us, i64 0, i64 %largest.0.i39.us
  br label %for.inc9.i.us

for.inc9.i.us:                                    ; preds = %if.then.i.us, %for.body.i.us
  %select.1.i.us = phi i64 [ %select.0.i37.us, %for.body.i.us ], [ %net.0.select.0.i.us, %if.then.i.us ]
  %largest.1.i.us = phi i64 [ %largest.0.i39.us, %for.body.i.us ], [ %cost.0.largest.0.i.us, %if.then.i.us ]
  %inc10.i.us = add i64 %net.0.i38.us, 1
  %cmp.i.us = icmp ugt i64 %inc10.i.us, %24
  br i1 %cmp.i.us, label %Select.exit.loopexit, label %for.body.i.us

for.body.i:                                       ; preds = %for.body.i.preheader, %for.inc9.i
  %largest.0.i39 = phi i64 [ %largest.1.i, %for.inc9.i ], [ -1, %for.body.i.preheader ]
  %net.0.i38 = phi i64 [ %inc10.i, %for.inc9.i ], [ 1, %for.body.i.preheader ]
  %select.0.i37 = phi i64 [ %select.1.i, %for.inc9.i ], [ 0, %for.body.i.preheader ]
  %arrayidx.i = getelementptr inbounds i64, i64* %23, i64 %net.0.i38
  %28 = load i64, i64* %arrayidx.i, align 8, !tbaa !1
  %tobool.i = icmp eq i64 %28, 0
  br i1 %tobool.i, label %for.inc9.i, label %if.then.i

if.then.i:                                        ; preds = %for.body.i
  %arrayidx1.i = getelementptr inbounds i64*, i64** %25, i64 %net.0.i38
  %29 = load i64*, i64** %arrayidx1.i, align 8, !tbaa !5
  br label %for.body4.i

for.body4.i:                                      ; preds = %if.then.i, %for.body4.i
  %cost.0.i35 = phi i64 [ 0, %if.then.i ], [ %add.i, %for.body4.i ]
  %track.0.i34 = phi i64 [ 1, %if.then.i ], [ %inc.i, %for.body4.i ]
  %arrayidx5.i = getelementptr inbounds i64, i64* %29, i64 %track.0.i34
  %30 = load i64, i64* %arrayidx5.i, align 8, !tbaa !1
  %add.i = add nsw i64 %30, %cost.0.i35
  %inc.i = add i64 %track.0.i34, 1
  %cmp3.i = icmp ugt i64 %inc.i, %26
  br i1 %cmp3.i, label %for.end.i, label %for.body4.i

for.end.i:                                        ; preds = %for.body4.i
  %cmp6.i = icmp sgt i64 %add.i, %largest.0.i39
  %net.0.select.0.i = select i1 %cmp6.i, i64 %net.0.i38, i64 %select.0.i37
  %cost.0.largest.0.i = select i1 %cmp6.i, i64 %add.i, i64 %largest.0.i39
  br label %for.inc9.i

for.inc9.i:                                       ; preds = %for.end.i, %for.body.i
  %select.1.i = phi i64 [ %select.0.i37, %for.body.i ], [ %net.0.select.0.i, %for.end.i ]
  %largest.1.i = phi i64 [ %largest.0.i39, %for.body.i ], [ %cost.0.largest.0.i, %for.end.i ]
  %inc10.i = add i64 %net.0.i38, 1
  %cmp.i = icmp ugt i64 %inc10.i, %24
  br i1 %cmp.i, label %Select.exit.loopexit50, label %for.body.i

Select.exit.loopexit:                             ; preds = %for.inc9.i.us
  br label %Select.exit

Select.exit.loopexit50:                           ; preds = %for.inc9.i
  br label %Select.exit

Select.exit:                                      ; preds = %Select.exit.loopexit50, %Select.exit.loopexit, %while.body
  %select.0.i.lcssa = phi i64 [ 0, %while.body ], [ %select.1.i.us, %Select.exit.loopexit ], [ %select.1.i, %Select.exit.loopexit50 ]
  %31 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %32 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  tail call void @Assign(%struct._nodeVCGType* %31, i64* %32, i64 %select.0.i.lcssa)
  %33 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx35 = getelementptr inbounds i64, i64* %33, i64 %select.0.i.lcssa
  store i64 0, i64* %arrayidx35, align 8, !tbaa !1
  %dec = add i64 %netsCrossing.4, -1
  br label %while.cond

for.end39.loopexit:                               ; preds = %for.cond1.backedge
  br label %for.end39

for.end39:                                        ; preds = %for.end39.loopexit, %for.end
  ret void
}

; Function Attrs: nounwind uwtable
define void @RightNetsAssign() local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp45 = icmp eq i64 %0, 0
  br i1 %cmp45, label %for.end, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %1 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  br label %for.body

for.body:                                         ; preds = %for.body.lr.ph, %for.body
  %net.046 = phi i64 [ 1, %for.body.lr.ph ], [ %inc, %for.body ]
  %arrayidx = getelementptr inbounds i64, i64* %1, i64 %net.046
  store i64 0, i64* %arrayidx, align 8, !tbaa !1
  %inc = add i64 %net.046, 1
  %2 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc, %2
  br i1 %cmp, label %for.end.loopexit, label %for.body

for.end.loopexit:                                 ; preds = %for.body
  br label %for.end

for.end:                                          ; preds = %for.end.loopexit, %entry
  %3 = load i64, i64* @channelDensityColumn, align 8, !tbaa !1
  %col.041 = add i64 %3, 1
  %4 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp242 = icmp ugt i64 %col.041, %4
  br i1 %cmp242, label %for.end39, label %for.body3.preheader

for.body3.preheader:                              ; preds = %for.end
  br label %for.body3

for.body3:                                        ; preds = %for.body3.preheader, %for.cond1.backedge
  %col.044 = phi i64 [ %col.0, %for.cond1.backedge ], [ %col.041, %for.body3.preheader ]
  %netsCrossing.043 = phi i64 [ %netsCrossing.0.be, %for.cond1.backedge ], [ 0, %for.body3.preheader ]
  %5 = load i64*, i64** @TOP, align 8, !tbaa !5
  %arrayidx4 = getelementptr inbounds i64, i64* %5, i64 %col.044
  %6 = load i64, i64* %arrayidx4, align 8, !tbaa !1
  %7 = load i64*, i64** @BOT, align 8, !tbaa !5
  %arrayidx5 = getelementptr inbounds i64, i64* %7, i64 %col.044
  %8 = load i64, i64* %arrayidx5, align 8, !tbaa !1
  %cmp6 = icmp eq i64 %6, %8
  %tobool = icmp ne i64 %6, 0
  br i1 %cmp6, label %if.else, label %if.then

if.then:                                          ; preds = %for.body3
  br i1 %tobool, label %land.lhs.true, label %if.end

land.lhs.true:                                    ; preds = %if.then
  %9 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx7 = getelementptr inbounds i64, i64* %9, i64 %6
  %10 = load i64, i64* %arrayidx7, align 8, !tbaa !1
  %cmp8 = icmp eq i64 %10, %col.044
  br i1 %cmp8, label %if.then9, label %if.end

if.then9:                                         ; preds = %land.lhs.true
  %11 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx10 = getelementptr inbounds i64, i64* %11, i64 %6
  store i64 1, i64* %arrayidx10, align 8, !tbaa !1
  %inc11 = add i64 %netsCrossing.043, 1
  br label %if.end

if.end:                                           ; preds = %if.then9, %land.lhs.true, %if.then
  %netsCrossing.1 = phi i64 [ %inc11, %if.then9 ], [ %netsCrossing.043, %land.lhs.true ], [ %netsCrossing.043, %if.then ]
  %tobool12 = icmp eq i64 %8, 0
  br i1 %tobool12, label %if.end28, label %land.lhs.true13

land.lhs.true13:                                  ; preds = %if.end
  %12 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx14 = getelementptr inbounds i64, i64* %12, i64 %8
  %13 = load i64, i64* %arrayidx14, align 8, !tbaa !1
  %cmp15 = icmp eq i64 %13, %col.044
  br i1 %cmp15, label %if.then16, label %if.end28

if.then16:                                        ; preds = %land.lhs.true13
  %14 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx17 = getelementptr inbounds i64, i64* %14, i64 %8
  store i64 1, i64* %arrayidx17, align 8, !tbaa !1
  %inc18 = add i64 %netsCrossing.1, 1
  br label %if.end28

if.else:                                          ; preds = %for.body3
  br i1 %tobool, label %land.lhs.true21, label %if.end28

land.lhs.true21:                                  ; preds = %if.else
  %15 = load i64*, i64** @FIRST, align 8, !tbaa !5
  %arrayidx22 = getelementptr inbounds i64, i64* %15, i64 %6
  %16 = load i64, i64* %arrayidx22, align 8, !tbaa !1
  %cmp23 = icmp eq i64 %16, %col.044
  br i1 %cmp23, label %if.then24, label %if.end28

if.then24:                                        ; preds = %land.lhs.true21
  %17 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx25 = getelementptr inbounds i64, i64* %17, i64 %6
  store i64 1, i64* %arrayidx25, align 8, !tbaa !1
  %inc26 = add i64 %netsCrossing.043, 1
  br label %if.end28

if.end28:                                         ; preds = %if.end, %if.else, %land.lhs.true21, %if.then24, %land.lhs.true13, %if.then16
  %netsCrossing.2 = phi i64 [ %inc18, %if.then16 ], [ %netsCrossing.1, %land.lhs.true13 ], [ %netsCrossing.1, %if.end ], [ %inc26, %if.then24 ], [ %netsCrossing.043, %land.lhs.true21 ], [ %netsCrossing.043, %if.else ]
  %18 = load i64*, i64** @LAST, align 8, !tbaa !5
  %arrayidx29 = getelementptr inbounds i64, i64* %18, i64 %6
  %19 = load i64, i64* %arrayidx29, align 8, !tbaa !1
  %cmp30 = icmp eq i64 %19, %col.044
  br i1 %cmp30, label %while.cond, label %lor.lhs.false

lor.lhs.false:                                    ; preds = %if.end28
  %arrayidx31 = getelementptr inbounds i64, i64* %18, i64 %8
  %20 = load i64, i64* %arrayidx31, align 8, !tbaa !1
  %cmp32 = icmp eq i64 %20, %col.044
  %cmp34 = icmp ne i64 %netsCrossing.2, 0
  %or.cond = and i1 %cmp34, %cmp32
  br i1 %or.cond, label %while.body, label %for.cond1.backedge

while.cond:                                       ; preds = %if.end28, %Select.exit
  %netsCrossing.3 = phi i64 [ %netsCrossing.2, %if.end28 ], [ %dec, %Select.exit ]
  %cmp34.old = icmp eq i64 %netsCrossing.3, 0
  br i1 %cmp34.old, label %for.cond1.backedge, label %while.body

for.cond1.backedge:                               ; preds = %while.cond, %lor.lhs.false
  %netsCrossing.0.be = phi i64 [ 0, %while.cond ], [ %netsCrossing.2, %lor.lhs.false ]
  %col.0 = add i64 %col.044, 1
  %21 = load i64, i64* @channelColumns, align 8, !tbaa !1
  %cmp2 = icmp ugt i64 %col.0, %21
  br i1 %cmp2, label %for.end39.loopexit, label %for.body3

while.body:                                       ; preds = %while.cond, %lor.lhs.false
  %netsCrossing.4 = phi i64 [ %netsCrossing.3, %while.cond ], [ %netsCrossing.2, %lor.lhs.false ]
  %22 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %23 = load %struct._nodeHCGType*, %struct._nodeHCGType** @HCG, align 8, !tbaa !5
  %24 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %25 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  tail call void @BuildCostMatrix(%struct._nodeVCGType* %22, %struct._nodeHCGType* %23, i64* %24, i64* %25) #4
  %26 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp.i36 = icmp eq i64 %26, 0
  br i1 %cmp.i36, label %Select.exit, label %for.body.i.lr.ph

for.body.i.lr.ph:                                 ; preds = %while.body
  %27 = load i64**, i64*** @costMatrix, align 8
  %28 = load i64, i64* @channelTracks, align 8
  %cmp3.i33 = icmp eq i64 %28, 0
  br i1 %cmp3.i33, label %for.body.i.us.preheader, label %for.body.i.preheader

for.body.i.preheader:                             ; preds = %for.body.i.lr.ph
  br label %for.body.i

for.body.i.us.preheader:                          ; preds = %for.body.i.lr.ph
  br label %for.body.i.us

for.body.i.us:                                    ; preds = %for.body.i.us.preheader, %for.inc9.i.us
  %largest.0.i39.us = phi i64 [ %largest.1.i.us, %for.inc9.i.us ], [ -1, %for.body.i.us.preheader ]
  %net.0.i38.us = phi i64 [ %inc10.i.us, %for.inc9.i.us ], [ 1, %for.body.i.us.preheader ]
  %select.0.i37.us = phi i64 [ %select.1.i.us, %for.inc9.i.us ], [ 0, %for.body.i.us.preheader ]
  %arrayidx.i.us = getelementptr inbounds i64, i64* %25, i64 %net.0.i38.us
  %29 = load i64, i64* %arrayidx.i.us, align 8, !tbaa !1
  %tobool.i.us = icmp eq i64 %29, 0
  br i1 %tobool.i.us, label %for.inc9.i.us, label %if.then.i.us

if.then.i.us:                                     ; preds = %for.body.i.us
  %cmp6.i.us = icmp slt i64 %largest.0.i39.us, 0
  %net.0.select.0.i.us = select i1 %cmp6.i.us, i64 %net.0.i38.us, i64 %select.0.i37.us
  %cost.0.largest.0.i.us = select i1 %cmp6.i.us, i64 0, i64 %largest.0.i39.us
  br label %for.inc9.i.us

for.inc9.i.us:                                    ; preds = %if.then.i.us, %for.body.i.us
  %select.1.i.us = phi i64 [ %select.0.i37.us, %for.body.i.us ], [ %net.0.select.0.i.us, %if.then.i.us ]
  %largest.1.i.us = phi i64 [ %largest.0.i39.us, %for.body.i.us ], [ %cost.0.largest.0.i.us, %if.then.i.us ]
  %inc10.i.us = add i64 %net.0.i38.us, 1
  %cmp.i.us = icmp ugt i64 %inc10.i.us, %26
  br i1 %cmp.i.us, label %Select.exit.loopexit, label %for.body.i.us

for.body.i:                                       ; preds = %for.body.i.preheader, %for.inc9.i
  %largest.0.i39 = phi i64 [ %largest.1.i, %for.inc9.i ], [ -1, %for.body.i.preheader ]
  %net.0.i38 = phi i64 [ %inc10.i, %for.inc9.i ], [ 1, %for.body.i.preheader ]
  %select.0.i37 = phi i64 [ %select.1.i, %for.inc9.i ], [ 0, %for.body.i.preheader ]
  %arrayidx.i = getelementptr inbounds i64, i64* %25, i64 %net.0.i38
  %30 = load i64, i64* %arrayidx.i, align 8, !tbaa !1
  %tobool.i = icmp eq i64 %30, 0
  br i1 %tobool.i, label %for.inc9.i, label %if.then.i

if.then.i:                                        ; preds = %for.body.i
  %arrayidx1.i = getelementptr inbounds i64*, i64** %27, i64 %net.0.i38
  %31 = load i64*, i64** %arrayidx1.i, align 8, !tbaa !5
  br label %for.body4.i

for.body4.i:                                      ; preds = %if.then.i, %for.body4.i
  %cost.0.i35 = phi i64 [ 0, %if.then.i ], [ %add.i, %for.body4.i ]
  %track.0.i34 = phi i64 [ 1, %if.then.i ], [ %inc.i, %for.body4.i ]
  %arrayidx5.i = getelementptr inbounds i64, i64* %31, i64 %track.0.i34
  %32 = load i64, i64* %arrayidx5.i, align 8, !tbaa !1
  %add.i = add nsw i64 %32, %cost.0.i35
  %inc.i = add i64 %track.0.i34, 1
  %cmp3.i = icmp ugt i64 %inc.i, %28
  br i1 %cmp3.i, label %for.end.i, label %for.body4.i

for.end.i:                                        ; preds = %for.body4.i
  %cmp6.i = icmp sgt i64 %add.i, %largest.0.i39
  %net.0.select.0.i = select i1 %cmp6.i, i64 %net.0.i38, i64 %select.0.i37
  %cost.0.largest.0.i = select i1 %cmp6.i, i64 %add.i, i64 %largest.0.i39
  br label %for.inc9.i

for.inc9.i:                                       ; preds = %for.end.i, %for.body.i
  %select.1.i = phi i64 [ %select.0.i37, %for.body.i ], [ %net.0.select.0.i, %for.end.i ]
  %largest.1.i = phi i64 [ %largest.0.i39, %for.body.i ], [ %cost.0.largest.0.i, %for.end.i ]
  %inc10.i = add i64 %net.0.i38, 1
  %cmp.i = icmp ugt i64 %inc10.i, %26
  br i1 %cmp.i, label %Select.exit.loopexit50, label %for.body.i

Select.exit.loopexit:                             ; preds = %for.inc9.i.us
  br label %Select.exit

Select.exit.loopexit50:                           ; preds = %for.inc9.i
  br label %Select.exit

Select.exit:                                      ; preds = %Select.exit.loopexit50, %Select.exit.loopexit, %while.body
  %select.0.i.lcssa = phi i64 [ 0, %while.body ], [ %select.1.i.us, %Select.exit.loopexit ], [ %select.1.i, %Select.exit.loopexit50 ]
  %33 = load %struct._nodeVCGType*, %struct._nodeVCGType** @VCG, align 8, !tbaa !5
  %34 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  tail call void @Assign(%struct._nodeVCGType* %33, i64* %34, i64 %select.0.i.lcssa)
  %35 = load i64*, i64** @CROSSING, align 8, !tbaa !5
  %arrayidx35 = getelementptr inbounds i64, i64* %35, i64 %select.0.i.lcssa
  store i64 0, i64* %arrayidx35, align 8, !tbaa !1
  %dec = add i64 %netsCrossing.4, -1
  br label %while.cond

for.end39.loopexit:                               ; preds = %for.cond1.backedge
  br label %for.end39

for.end39:                                        ; preds = %for.end39.loopexit, %for.end
  ret void
}

; Function Attrs: nounwind uwtable
define void @Select(%struct._nodeVCGType* %VCG, %struct._nodeHCGType* %HCG, i64* %netsAssign, i64* nocapture %netSelect, i64* nocapture readonly %CROSSING) local_unnamed_addr #0 {
entry:
  tail call void @BuildCostMatrix(%struct._nodeVCGType* %VCG, %struct._nodeHCGType* %HCG, i64* %netsAssign, i64* %CROSSING)
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp19 = icmp eq i64 %0, 0
  br i1 %cmp19, label %for.end11, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %1 = load i64**, i64*** @costMatrix, align 8
  %2 = load i64, i64* @channelTracks, align 8
  %cmp316 = icmp eq i64 %2, 0
  br i1 %cmp316, label %for.body.us.preheader, label %for.body.preheader

for.body.preheader:                               ; preds = %for.body.lr.ph
  br label %for.body

for.body.us.preheader:                            ; preds = %for.body.lr.ph
  br label %for.body.us

for.body.us:                                      ; preds = %for.body.us.preheader, %for.inc9.us
  %largest.023.us = phi i64 [ %largest.1.us, %for.inc9.us ], [ -1, %for.body.us.preheader ]
  %net.021.us = phi i64 [ %inc10.us, %for.inc9.us ], [ 1, %for.body.us.preheader ]
  %select.020.us = phi i64 [ %select.1.us, %for.inc9.us ], [ 0, %for.body.us.preheader ]
  %arrayidx.us = getelementptr inbounds i64, i64* %CROSSING, i64 %net.021.us
  %3 = load i64, i64* %arrayidx.us, align 8, !tbaa !1
  %tobool.us = icmp eq i64 %3, 0
  br i1 %tobool.us, label %for.inc9.us, label %if.then.us

if.then.us:                                       ; preds = %for.body.us
  %cmp6.us = icmp slt i64 %largest.023.us, 0
  %net.0.select.0.us = select i1 %cmp6.us, i64 %net.021.us, i64 %select.020.us
  %cost.0.largest.0.us = select i1 %cmp6.us, i64 0, i64 %largest.023.us
  br label %for.inc9.us

for.inc9.us:                                      ; preds = %if.then.us, %for.body.us
  %select.1.us = phi i64 [ %select.020.us, %for.body.us ], [ %net.0.select.0.us, %if.then.us ]
  %largest.1.us = phi i64 [ %largest.023.us, %for.body.us ], [ %cost.0.largest.0.us, %if.then.us ]
  %inc10.us = add i64 %net.021.us, 1
  %cmp.us = icmp ugt i64 %inc10.us, %0
  br i1 %cmp.us, label %for.end11.loopexit, label %for.body.us

for.body:                                         ; preds = %for.body.preheader, %for.inc9
  %largest.023 = phi i64 [ %largest.1, %for.inc9 ], [ -1, %for.body.preheader ]
  %net.021 = phi i64 [ %inc10, %for.inc9 ], [ 1, %for.body.preheader ]
  %select.020 = phi i64 [ %select.1, %for.inc9 ], [ 0, %for.body.preheader ]
  %arrayidx = getelementptr inbounds i64, i64* %CROSSING, i64 %net.021
  %4 = load i64, i64* %arrayidx, align 8, !tbaa !1
  %tobool = icmp eq i64 %4, 0
  br i1 %tobool, label %for.inc9, label %if.then

if.then:                                          ; preds = %for.body
  %arrayidx1 = getelementptr inbounds i64*, i64** %1, i64 %net.021
  %5 = load i64*, i64** %arrayidx1, align 8, !tbaa !5
  br label %for.body4

for.body4:                                        ; preds = %if.then, %for.body4
  %cost.018 = phi i64 [ 0, %if.then ], [ %add, %for.body4 ]
  %track.017 = phi i64 [ 1, %if.then ], [ %inc, %for.body4 ]
  %arrayidx5 = getelementptr inbounds i64, i64* %5, i64 %track.017
  %6 = load i64, i64* %arrayidx5, align 8, !tbaa !1
  %add = add nsw i64 %6, %cost.018
  %inc = add i64 %track.017, 1
  %cmp3 = icmp ugt i64 %inc, %2
  br i1 %cmp3, label %for.end, label %for.body4

for.end:                                          ; preds = %for.body4
  %cmp6 = icmp sgt i64 %add, %largest.023
  %net.0.select.0 = select i1 %cmp6, i64 %net.021, i64 %select.020
  %cost.0.largest.0 = select i1 %cmp6, i64 %add, i64 %largest.023
  br label %for.inc9

for.inc9:                                         ; preds = %for.end, %for.body
  %select.1 = phi i64 [ %select.020, %for.body ], [ %net.0.select.0, %for.end ]
  %largest.1 = phi i64 [ %largest.023, %for.body ], [ %cost.0.largest.0, %for.end ]
  %inc10 = add i64 %net.021, 1
  %cmp = icmp ugt i64 %inc10, %0
  br i1 %cmp, label %for.end11.loopexit28, label %for.body

for.end11.loopexit:                               ; preds = %for.inc9.us
  br label %for.end11

for.end11.loopexit28:                             ; preds = %for.inc9
  br label %for.end11

for.end11:                                        ; preds = %for.end11.loopexit28, %for.end11.loopexit, %entry
  %select.0.lcssa = phi i64 [ 0, %entry ], [ %select.1.us, %for.end11.loopexit ], [ %select.1, %for.end11.loopexit28 ]
  store i64 %select.0.lcssa, i64* %netSelect, align 8, !tbaa !1
  ret void
}

; Function Attrs: nounwind uwtable
define void @Assign(%struct._nodeVCGType* %VCG, i64* %assign, i64 %select) local_unnamed_addr #0 {
entry:
  tail call void @LongestPathVCG(%struct._nodeVCGType* %VCG, i64 %select) #4
  %0 = load %struct._nodeHCGType*, %struct._nodeHCGType** @HCG, align 8, !tbaa !5
  %1 = load i64*, i64** @tracksNoHCV, align 8, !tbaa !5
  tail call void @NoHCV(%struct._nodeHCGType* %0, i64 %select, i64* %assign, i64* %1) #4
  %2 = load i64, i64* @cardTopNotPref, align 8, !tbaa !1
  %3 = load i64, i64* @cardBotNotPref, align 8, !tbaa !1
  %add3.i = add i64 %3, %2
  %cmp.i = icmp eq i64 %add3.i, 0
  %.pre = load i64, i64* @channelTracks, align 8, !tbaa !1
  br i1 %cmp.i, label %IdealTrack.exit, label %if.then.i

if.then.i:                                        ; preds = %entry
  %sub.i = sub i64 %.pre, %3
  %mul.i = mul i64 %sub.i, %2
  %add.i = add i64 %2, 1
  %mul1.i = mul i64 %add.i, %3
  %add2.i = add i64 %mul.i, %mul1.i
  %div.i = udiv i64 %add2.i, %add3.i
  br label %IdealTrack.exit

IdealTrack.exit:                                  ; preds = %entry, %if.then.i
  %storemerge.i = phi i64 [ %div.i, %if.then.i ], [ 1, %entry ]
  %cmp77 = icmp eq i64 %.pre, 0
  br i1 %cmp77, label %if.then53, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %IdealTrack.exit
  %4 = load i64*, i64** @tracksAssign, align 8, !tbaa !5
  br label %for.body

for.cond2.preheader:                              ; preds = %for.body
  %cmp373 = icmp eq i64 %8, 0
  br i1 %cmp373, label %if.then53, label %for.body4.lr.ph

for.body4.lr.ph:                                  ; preds = %for.cond2.preheader
  %5 = load i64*, i64** @tracksNoHCV, align 8, !tbaa !5
  %6 = load i64*, i64** @tracksNotPref, align 8
  %7 = load i64*, i64** @tracksAssign, align 8
  br label %for.body4

for.body:                                         ; preds = %for.body.lr.ph, %for.body
  %track.078 = phi i64 [ 1, %for.body.lr.ph ], [ %inc, %for.body ]
  %arrayidx1 = getelementptr inbounds i64, i64* %4, i64 %track.078
  store i64 0, i64* %arrayidx1, align 8, !tbaa !1
  %inc = add i64 %track.078, 1
  %8 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp = icmp ugt i64 %inc, %8
  br i1 %cmp, label %for.cond2.preheader, label %for.body

for.body4:                                        ; preds = %for.body4.lr.ph, %for.inc10
  %9 = phi i64 [ %8, %for.body4.lr.ph ], [ %12, %for.inc10 ]
  %track.175 = phi i64 [ 1, %for.body4.lr.ph ], [ %inc11, %for.inc10 ]
  %tracks.074 = phi i64 [ 0, %for.body4.lr.ph ], [ %tracks.1, %for.inc10 ]
  %arrayidx5 = getelementptr inbounds i64, i64* %5, i64 %track.175
  %10 = load i64, i64* %arrayidx5, align 8, !tbaa !1
  %tobool = icmp eq i64 %10, 0
  br i1 %tobool, label %for.inc10, label %land.lhs.true

land.lhs.true:                                    ; preds = %for.body4
  %arrayidx6 = getelementptr inbounds i64, i64* %6, i64 %track.175
  %11 = load i64, i64* %arrayidx6, align 8, !tbaa !1
  %tobool7 = icmp eq i64 %11, 0
  br i1 %tobool7, label %if.then, label %for.inc10

if.then:                                          ; preds = %land.lhs.true
  %arrayidx8 = getelementptr inbounds i64, i64* %7, i64 %track.175
  store i64 1, i64* %arrayidx8, align 8, !tbaa !1
  %inc9 = add i64 %tracks.074, 1
  %.pre79 = load i64, i64* @channelTracks, align 8, !tbaa !1
  br label %for.inc10

for.inc10:                                        ; preds = %land.lhs.true, %for.body4, %if.then
  %12 = phi i64 [ %9, %land.lhs.true ], [ %.pre79, %if.then ], [ %9, %for.body4 ]
  %tracks.1 = phi i64 [ %tracks.074, %land.lhs.true ], [ %inc9, %if.then ], [ %tracks.074, %for.body4 ]
  %inc11 = add i64 %track.175, 1
  %cmp3 = icmp ugt i64 %inc11, %12
  br i1 %cmp3, label %for.end12, label %for.body4

for.end12:                                        ; preds = %for.inc10
  %cmp13 = icmp eq i64 %tracks.1, 0
  br i1 %cmp13, label %for.cond15.preheader, label %for.cond67.preheader

for.cond15.preheader:                             ; preds = %for.end12
  %cmp1669 = icmp eq i64 %12, 0
  br i1 %cmp1669, label %if.then53, label %for.body17.lr.ph

for.body17.lr.ph:                                 ; preds = %for.cond15.preheader
  %13 = load i64*, i64** @tracksNoHCV, align 8, !tbaa !5
  %14 = load i64*, i64** @tracksTopNotPref, align 8
  %15 = load i64*, i64** @tracksBotNotPref, align 8
  %16 = load i64*, i64** @tracksAssign, align 8
  br label %for.body17

for.body17:                                       ; preds = %for.body17.lr.ph, %for.inc30
  %17 = phi i64 [ %12, %for.body17.lr.ph ], [ %21, %for.inc30 ]
  %track.271 = phi i64 [ 1, %for.body17.lr.ph ], [ %inc31, %for.inc30 ]
  %tracks.270 = phi i64 [ 0, %for.body17.lr.ph ], [ %tracks.3, %for.inc30 ]
  %arrayidx18 = getelementptr inbounds i64, i64* %13, i64 %track.271
  %18 = load i64, i64* %arrayidx18, align 8, !tbaa !1
  %tobool19 = icmp eq i64 %18, 0
  br i1 %tobool19, label %for.inc30, label %land.lhs.true20

land.lhs.true20:                                  ; preds = %for.body17
  %arrayidx21 = getelementptr inbounds i64, i64* %14, i64 %track.271
  %19 = load i64, i64* %arrayidx21, align 8, !tbaa !1
  %tobool22 = icmp eq i64 %19, 0
  br i1 %tobool22, label %for.inc30, label %land.lhs.true23

land.lhs.true23:                                  ; preds = %land.lhs.true20
  %arrayidx24 = getelementptr inbounds i64, i64* %15, i64 %track.271
  %20 = load i64, i64* %arrayidx24, align 8, !tbaa !1
  %tobool25 = icmp eq i64 %20, 0
  br i1 %tobool25, label %for.inc30, label %if.then26

if.then26:                                        ; preds = %land.lhs.true23
  %arrayidx27 = getelementptr inbounds i64, i64* %16, i64 %track.271
  store i64 1, i64* %arrayidx27, align 8, !tbaa !1
  %inc28 = add i64 %tracks.270, 1
  %.pre80 = load i64, i64* @channelTracks, align 8, !tbaa !1
  br label %for.inc30

for.inc30:                                        ; preds = %land.lhs.true23, %land.lhs.true20, %for.body17, %if.then26
  %21 = phi i64 [ %.pre80, %if.then26 ], [ %17, %land.lhs.true23 ], [ %17, %land.lhs.true20 ], [ %17, %for.body17 ]
  %tracks.3 = phi i64 [ %inc28, %if.then26 ], [ %tracks.270, %land.lhs.true23 ], [ %tracks.270, %land.lhs.true20 ], [ %tracks.270, %for.body17 ]
  %inc31 = add i64 %track.271, 1
  %cmp16 = icmp ugt i64 %inc31, %21
  br i1 %cmp16, label %if.end33, label %for.body17

if.end33:                                         ; preds = %for.inc30
  %cmp34 = icmp eq i64 %tracks.3, 0
  br i1 %cmp34, label %for.cond36.preheader, label %for.cond67.preheader

for.cond36.preheader:                             ; preds = %if.end33
  %cmp3765 = icmp ugt i64 %21, 2
  br i1 %cmp3765, label %for.body38.lr.ph, label %if.then53

for.body38.lr.ph:                                 ; preds = %for.cond36.preheader
  %22 = load i64*, i64** @tracksNoHCV, align 8, !tbaa !5
  %23 = load i64*, i64** @tracksNotPref, align 8
  %24 = load i64*, i64** @tracksAssign, align 8
  br label %for.body38

for.body38:                                       ; preds = %for.body38.lr.ph, %for.inc48
  %25 = phi i64 [ %21, %for.body38.lr.ph ], [ %28, %for.inc48 ]
  %track.367 = phi i64 [ 2, %for.body38.lr.ph ], [ %inc49, %for.inc48 ]
  %tracks.566 = phi i64 [ 0, %for.body38.lr.ph ], [ %tracks.6, %for.inc48 ]
  %arrayidx39 = getelementptr inbounds i64, i64* %22, i64 %track.367
  %26 = load i64, i64* %arrayidx39, align 8, !tbaa !1
  %tobool40 = icmp eq i64 %26, 0
  br i1 %tobool40, label %for.inc48, label %land.lhs.true41

land.lhs.true41:                                  ; preds = %for.body38
  %arrayidx42 = getelementptr inbounds i64, i64* %23, i64 %track.367
  %27 = load i64, i64* %arrayidx42, align 8, !tbaa !1
  %tobool43 = icmp eq i64 %27, 0
  br i1 %tobool43, label %for.inc48, label %if.then44

if.then44:                                        ; preds = %land.lhs.true41
  %arrayidx45 = getelementptr inbounds i64, i64* %24, i64 %track.367
  store i64 1, i64* %arrayidx45, align 8, !tbaa !1
  %inc46 = add i64 %tracks.566, 1
  %.pre81 = load i64, i64* @channelTracks, align 8, !tbaa !1
  br label %for.inc48

for.inc48:                                        ; preds = %land.lhs.true41, %for.body38, %if.then44
  %28 = phi i64 [ %.pre81, %if.then44 ], [ %25, %land.lhs.true41 ], [ %25, %for.body38 ]
  %tracks.6 = phi i64 [ %inc46, %if.then44 ], [ %tracks.566, %land.lhs.true41 ], [ %tracks.566, %for.body38 ]
  %inc49 = add i64 %track.367, 1
  %cmp37 = icmp ult i64 %inc49, %28
  br i1 %cmp37, label %for.body38, label %if.end51

if.end51:                                         ; preds = %for.inc48
  %cmp52 = icmp eq i64 %tracks.6, 0
  br i1 %cmp52, label %if.then53, label %for.cond67.preheader

if.then53:                                        ; preds = %for.cond15.preheader, %IdealTrack.exit, %for.cond2.preheader, %for.cond36.preheader, %if.end51
  %29 = phi i64 [ %28, %if.end51 ], [ %21, %for.cond36.preheader ], [ 0, %for.cond2.preheader ], [ 0, %IdealTrack.exit ], [ 0, %for.cond15.preheader ]
  %30 = load i64*, i64** @tracksNoHCV, align 8, !tbaa !5
  %arrayidx54 = getelementptr inbounds i64, i64* %30, i64 1
  %31 = load i64, i64* %arrayidx54, align 8, !tbaa !1
  %tobool55 = icmp eq i64 %31, 0
  br i1 %tobool55, label %if.end59, label %if.then56

if.then56:                                        ; preds = %if.then53
  %32 = load i64*, i64** @tracksAssign, align 8, !tbaa !5
  %arrayidx57 = getelementptr inbounds i64, i64* %32, i64 1
  store i64 1, i64* %arrayidx57, align 8, !tbaa !1
  br label %if.end59

if.end59:                                         ; preds = %if.then53, %if.then56
  %arrayidx60 = getelementptr inbounds i64, i64* %30, i64 %29
  %33 = load i64, i64* %arrayidx60, align 8, !tbaa !1
  %tobool61 = icmp eq i64 %33, 0
  br i1 %tobool61, label %for.cond67.preheader, label %if.then62

if.then62:                                        ; preds = %if.end59
  %34 = load i64*, i64** @tracksAssign, align 8, !tbaa !5
  %arrayidx63 = getelementptr inbounds i64, i64* %34, i64 %29
  store i64 1, i64* %arrayidx63, align 8, !tbaa !1
  %.pre82 = load i64, i64* @channelTracks, align 8, !tbaa !1
  br label %for.cond67.preheader

for.cond67.preheader:                             ; preds = %if.end59, %if.then62, %if.end51, %if.end33, %for.end12
  %35 = phi i64 [ %29, %if.end59 ], [ %.pre82, %if.then62 ], [ %28, %if.end51 ], [ %21, %if.end33 ], [ %12, %for.end12 ]
  %cmp6860 = icmp eq i64 %35, 0
  br i1 %cmp6860, label %for.end93, label %for.body69.preheader

for.body69.preheader:                             ; preds = %for.cond67.preheader
  br label %for.body69

for.body69:                                       ; preds = %for.body69.preheader, %for.inc91
  %vcvAssign.064 = phi i64 [ %vcvAssign.1, %for.inc91 ], [ 1000000, %for.body69.preheader ]
  %vcvDist.063 = phi i64 [ %vcvDist.1, %for.inc91 ], [ undef, %for.body69.preheader ]
  %track.462 = phi i64 [ %inc92, %for.inc91 ], [ 1, %for.body69.preheader ]
  %trackAssign.061 = phi i64 [ %trackAssign.1, %for.inc91 ], [ 0, %for.body69.preheader ]
  %36 = load i64*, i64** @tracksAssign, align 8, !tbaa !5
  %arrayidx70 = getelementptr inbounds i64, i64* %36, i64 %track.462
  %37 = load i64, i64* %arrayidx70, align 8, !tbaa !1
  %tobool71 = icmp eq i64 %37, 0
  br i1 %tobool71, label %for.inc91, label %if.then72

if.then72:                                        ; preds = %for.body69
  %38 = load i64*, i64** @netsAssign, align 8, !tbaa !5
  %call = tail call i64 @VCV(%struct._nodeVCGType* %VCG, i64 %select, i64 %track.462, i64* %38) #4
  %cmp73 = icmp ult i64 %call, %vcvAssign.064
  br i1 %cmp73, label %if.then74, label %if.else

if.then74:                                        ; preds = %if.then72
  %sub = sub i64 %track.462, %storemerge.i
  %cmp75 = icmp slt i64 %sub, 0
  %mul = sub nsw i64 0, %sub
  %mul.sub = select i1 %cmp75, i64 %mul, i64 %sub
  br label %for.inc91

if.else:                                          ; preds = %if.then72
  %cmp78 = icmp eq i64 %call, %vcvAssign.064
  br i1 %cmp78, label %if.then79, label %for.inc91

if.then79:                                        ; preds = %if.else
  %sub80 = sub i64 %track.462, %storemerge.i
  %cmp81 = icmp slt i64 %sub80, 0
  %mul83 = sub nsw i64 0, %sub80
  %mul83.sub80 = select i1 %cmp81, i64 %mul83, i64 %sub80
  %cmp85 = icmp slt i64 %mul83.sub80, %vcvDist.063
  %track.4.trackAssign.0 = select i1 %cmp85, i64 %track.462, i64 %trackAssign.061
  %mul83.sub80.vcvDist.0 = select i1 %cmp85, i64 %mul83.sub80, i64 %vcvDist.063
  br label %for.inc91

for.inc91:                                        ; preds = %if.then79, %if.then74, %for.body69, %if.else
  %trackAssign.1 = phi i64 [ %trackAssign.061, %if.else ], [ %trackAssign.061, %for.body69 ], [ %track.462, %if.then74 ], [ %track.4.trackAssign.0, %if.then79 ]
  %vcvDist.1 = phi i64 [ %vcvDist.063, %if.else ], [ %vcvDist.063, %for.body69 ], [ %mul.sub, %if.then74 ], [ %mul83.sub80.vcvDist.0, %if.then79 ]
  %vcvAssign.1 = phi i64 [ %vcvAssign.064, %if.else ], [ %vcvAssign.064, %for.body69 ], [ %call, %if.then74 ], [ %vcvAssign.064, %if.then79 ]
  %inc92 = add i64 %track.462, 1
  %39 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp68 = icmp ugt i64 %inc92, %39
  br i1 %cmp68, label %for.end93.loopexit, label %for.body69

for.end93.loopexit:                               ; preds = %for.inc91
  br label %for.end93

for.end93:                                        ; preds = %for.end93.loopexit, %for.cond67.preheader
  %trackAssign.0.lcssa = phi i64 [ 0, %for.cond67.preheader ], [ %trackAssign.1, %for.end93.loopexit ]
  %arrayidx94 = getelementptr inbounds i64, i64* %assign, i64 %select
  store i64 %trackAssign.0.lcssa, i64* %arrayidx94, align 8, !tbaa !1
  ret void
}

declare void @LongestPathVCG(%struct._nodeVCGType*, i64) local_unnamed_addr #2

declare void @NoHCV(%struct._nodeHCGType*, i64, i64*, i64*) local_unnamed_addr #2

; Function Attrs: norecurse nounwind uwtable
define void @IdealTrack(i64 %tracks, i64 %top, i64 %bot, i64* nocapture %ideal) local_unnamed_addr #3 {
entry:
  %add3 = add i64 %bot, %top
  %cmp = icmp eq i64 %add3, 0
  br i1 %cmp, label %if.end, label %if.then

if.then:                                          ; preds = %entry
  %sub = sub i64 %tracks, %bot
  %mul = mul i64 %sub, %top
  %add = add i64 %top, 1
  %mul1 = mul i64 %add, %bot
  %add2 = add i64 %mul, %mul1
  %div = udiv i64 %add2, %add3
  br label %if.end

if.end:                                           ; preds = %entry, %if.then
  %storemerge = phi i64 [ %div, %if.then ], [ 1, %entry ]
  store i64 %storemerge, i64* %ideal, align 8, !tbaa !1
  ret void
}

declare i64 @VCV(%struct._nodeVCGType*, i64, i64, i64*) local_unnamed_addr #2

; Function Attrs: nounwind uwtable
define void @BuildCostMatrix(%struct._nodeVCGType* %VCG, %struct._nodeHCGType* %HCG, i64* %netsAssign, i64* nocapture readonly %CROSSING) local_unnamed_addr #0 {
entry:
  %0 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp54 = icmp eq i64 %0, 0
  br i1 %cmp54, label %for.end59, label %for.body.lr.ph

for.body.lr.ph:                                   ; preds = %entry
  %1 = load i64**, i64*** @costMatrix, align 8, !tbaa !5
  %.pre = load i64, i64* @channelTracks, align 8, !tbaa !1
  br label %for.body

for.cond8.preheader:                              ; preds = %for.inc5
  %cmp950 = icmp eq i64 %6, 0
  br i1 %cmp950, label %for.end59, label %for.body10.preheader

for.body10.preheader:                             ; preds = %for.cond8.preheader
  br label %for.body10

for.body:                                         ; preds = %for.body.lr.ph, %for.inc5
  %2 = phi i64 [ %0, %for.body.lr.ph ], [ %6, %for.inc5 ]
  %3 = phi i64 [ %.pre, %for.body.lr.ph ], [ %7, %for.inc5 ]
  %net.055 = phi i64 [ 1, %for.body.lr.ph ], [ %inc6, %for.inc5 ]
  %arrayidx = getelementptr inbounds i64*, i64** %1, i64 %net.055
  %4 = load i64*, i64** %arrayidx, align 8, !tbaa !5
  %cmp252 = icmp eq i64 %3, 0
  br i1 %cmp252, label %for.inc5, label %for.body3.preheader

for.body3.preheader:                              ; preds = %for.body
  br label %for.body3

for.body3:                                        ; preds = %for.body3.preheader, %for.body3
  %track.053 = phi i64 [ %inc, %for.body3 ], [ 1, %for.body3.preheader ]
  %arrayidx4 = getelementptr inbounds i64, i64* %4, i64 %track.053
  store i64 0, i64* %arrayidx4, align 8, !tbaa !1
  %inc = add i64 %track.053, 1
  %5 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp2 = icmp ugt i64 %inc, %5
  br i1 %cmp2, label %for.inc5.loopexit, label %for.body3

for.inc5.loopexit:                                ; preds = %for.body3
  %.pre56 = load i64, i64* @channelNets, align 8, !tbaa !1
  br label %for.inc5

for.inc5:                                         ; preds = %for.inc5.loopexit, %for.body
  %6 = phi i64 [ %.pre56, %for.inc5.loopexit ], [ %2, %for.body ]
  %7 = phi i64 [ %5, %for.inc5.loopexit ], [ 0, %for.body ]
  %inc6 = add i64 %net.055, 1
  %cmp = icmp ugt i64 %inc6, %6
  br i1 %cmp, label %for.cond8.preheader, label %for.body

for.body10:                                       ; preds = %for.body10.preheader, %for.inc57
  %net.151 = phi i64 [ %inc58, %for.inc57 ], [ 1, %for.body10.preheader ]
  %arrayidx11 = getelementptr inbounds i64, i64* %CROSSING, i64 %net.151
  %8 = load i64, i64* %arrayidx11, align 8, !tbaa !1
  %tobool = icmp eq i64 %8, 0
  br i1 %tobool, label %for.inc57, label %if.then

if.then:                                          ; preds = %for.body10
  %9 = load i64**, i64*** @costMatrix, align 8, !tbaa !5
  %arrayidx12 = getelementptr inbounds i64*, i64** %9, i64 %net.151
  %10 = load i64*, i64** %arrayidx12, align 8, !tbaa !5
  tail call void @LongestPathVCG(%struct._nodeVCGType* %VCG, i64 %net.151) #4
  %11 = load i64*, i64** @tracksNoHCV, align 8, !tbaa !5
  tail call void @NoHCV(%struct._nodeHCGType* %HCG, i64 %net.151, i64* %netsAssign, i64* %11) #4
  %12 = load i64, i64* @cardTopNotPref, align 8, !tbaa !1
  %13 = load i64, i64* @cardBotNotPref, align 8, !tbaa !1
  %add3.i = add i64 %13, %12
  %cmp.i = icmp eq i64 %add3.i, 0
  %.pre57 = load i64, i64* @channelTracks, align 8, !tbaa !1
  br i1 %cmp.i, label %IdealTrack.exit, label %if.then.i

if.then.i:                                        ; preds = %if.then
  %sub.i = sub i64 %.pre57, %13
  %mul.i = mul i64 %sub.i, %12
  %add.i = add i64 %12, 1
  %mul1.i = mul i64 %add.i, %13
  %add2.i = add i64 %mul.i, %mul1.i
  %div.i = udiv i64 %add2.i, %add3.i
  br label %IdealTrack.exit

IdealTrack.exit:                                  ; preds = %if.then, %if.then.i
  %storemerge.i = phi i64 [ %div.i, %if.then.i ], [ 1, %if.then ]
  %cmp1446 = icmp eq i64 %.pre57, 0
  br i1 %cmp1446, label %for.inc57, label %for.body15.lr.ph

for.body15.lr.ph:                                 ; preds = %IdealTrack.exit
  %14 = load i64*, i64** @tracksNoHCV, align 8, !tbaa !5
  %15 = load i64*, i64** @tracksNotPref, align 8
  br label %for.body15

for.body15:                                       ; preds = %for.body15.lr.ph, %for.inc53
  %16 = phi i64 [ %.pre57, %for.body15.lr.ph ], [ %22, %for.inc53 ]
  %track.147 = phi i64 [ 1, %for.body15.lr.ph ], [ %inc54, %for.inc53 ]
  %arrayidx16 = getelementptr inbounds i64, i64* %14, i64 %track.147
  %17 = load i64, i64* %arrayidx16, align 8, !tbaa !1
  %tobool17 = icmp eq i64 %17, 0
  br i1 %tobool17, label %if.else50, label %if.then18

if.then18:                                        ; preds = %for.body15
  %18 = load i64, i64* @cardNotPref, align 8, !tbaa !1
  %cmp19 = icmp eq i64 %18, %16
  br i1 %cmp19, label %if.else26, label %if.then20

if.then20:                                        ; preds = %if.then18
  %arrayidx21 = getelementptr inbounds i64, i64* %15, i64 %track.147
  %19 = load i64, i64* %arrayidx21, align 8, !tbaa !1
  %tobool22 = icmp eq i64 %19, 0
  br i1 %tobool22, label %if.end37, label %if.end37.thread

if.else26:                                        ; preds = %if.then18
  %20 = load i64, i64* @cardBotNotPref, align 8, !tbaa !1
  %sub = sub i64 %16, %20
  %cmp27 = icmp ugt i64 %track.147, %sub
  br i1 %cmp27, label %land.lhs.true, label %if.end37.thread

land.lhs.true:                                    ; preds = %if.else26
  %21 = load i64, i64* @cardTopNotPref, align 8, !tbaa !1
  %cmp28 = icmp ugt i64 %track.147, %21
  br i1 %cmp28, label %if.end37.thread, label %if.then29

if.then29:                                        ; preds = %land.lhs.true
  %add.neg = shl i64 %16, 1
  %sub3039 = sub i64 %add.neg, %20
  %sub31 = sub i64 %sub3039, %21
  br label %if.end37

if.end37.thread:                                  ; preds = %if.then20, %land.lhs.true, %if.else26
  %arrayidx35.sink41 = getelementptr inbounds i64, i64* %10, i64 %track.147
  store i64 10000, i64* %arrayidx35.sink41, align 8, !tbaa !1
  br label %if.then40

if.end37:                                         ; preds = %if.then20, %if.then29
  %.sink.sink.in = phi i64 [ %sub31, %if.then29 ], [ %18, %if.then20 ]
  %.sink.sink = mul i64 %.sink.sink.in, 100
  %arrayidx35.sink = getelementptr inbounds i64, i64* %10, i64 %track.147
  store i64 %.sink.sink, i64* %arrayidx35.sink, align 8, !tbaa !1
  %cmp39 = icmp slt i64 %.sink.sink, 1000000
  br i1 %cmp39, label %if.then40, label %for.inc53

if.then40:                                        ; preds = %if.end37.thread, %if.end37
  %arrayidx35.sink44 = phi i64* [ %arrayidx35.sink41, %if.end37.thread ], [ %arrayidx35.sink, %if.end37 ]
  %.sink.sink43 = phi i64 [ 10000, %if.end37.thread ], [ %.sink.sink, %if.end37 ]
  %sub41 = sub i64 %storemerge.i, %track.147
  %cmp42 = icmp slt i64 %sub41, 0
  %mul44 = sub nsw i64 0, %sub41
  %mul44.sub41 = select i1 %cmp42, i64 %mul44, i64 %sub41
  %add48 = add nsw i64 %.sink.sink43, %mul44.sub41
  store i64 %add48, i64* %arrayidx35.sink44, align 8, !tbaa !1
  br label %for.inc53

if.else50:                                        ; preds = %for.body15
  %arrayidx51 = getelementptr inbounds i64, i64* %10, i64 %track.147
  store i64 1000000, i64* %arrayidx51, align 8, !tbaa !1
  br label %for.inc53

for.inc53:                                        ; preds = %if.else50, %if.then40, %if.end37
  %inc54 = add i64 %track.147, 1
  %22 = load i64, i64* @channelTracks, align 8, !tbaa !1
  %cmp14 = icmp ugt i64 %inc54, %22
  br i1 %cmp14, label %for.inc57.loopexit, label %for.body15

for.inc57.loopexit:                               ; preds = %for.inc53
  br label %for.inc57

for.inc57:                                        ; preds = %for.inc57.loopexit, %IdealTrack.exit, %for.body10
  %inc58 = add i64 %net.151, 1
  %23 = load i64, i64* @channelNets, align 8, !tbaa !1
  %cmp9 = icmp ugt i64 %inc58, %23
  br i1 %cmp9, label %for.end59.loopexit, label %for.body10

for.end59.loopexit:                               ; preds = %for.inc57
  br label %for.end59

for.end59:                                        ; preds = %for.end59.loopexit, %entry, %for.cond8.preheader
  ret void
}

attributes #0 = { nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #1 = { nounwind "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #2 = { "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #3 = { norecurse nounwind uwtable "correctly-rounded-divide-sqrt-fp-math"="false" "disable-tail-calls"="false" "less-precise-fpmad"="false" "no-frame-pointer-elim"="false" "no-infs-fp-math"="false" "no-jump-tables"="false" "no-nans-fp-math"="false" "no-signed-zeros-fp-math"="false" "no-trapping-math"="false" "stack-protector-buffer-size"="8" "target-cpu"="x86-64" "target-features"="+fxsr,+mmx,+sse,+sse2,+x87" "unsafe-fp-math"="false" "use-soft-float"="false" }
attributes #4 = { nounwind }

!llvm.ident = !{!0}

!0 = !{!"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 804284fdd0ceb0378cda00e5ed5b8746805eda6b)"}
!1 = !{!2, !2, i64 0}
!2 = !{!"long", !3, i64 0}
!3 = !{!"omnipotent char", !4, i64 0}
!4 = !{!"Simple C/C++ TBAA"}
!5 = !{!6, !6, i64 0}
!6 = !{!"any pointer", !3, i64 0}
