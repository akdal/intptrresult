	.text
	.file	"libclamav_fsg.bc"
	.globl	unfsg_200
	.p2align	4, 0x90
	.type	unfsg_200,@function
unfsg_200:                              # @unfsg_200
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r12
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%rbx
.Lcfi4:
	.cfi_def_cfa_offset 48
	subq	$48, %rsp
.Lcfi5:
	.cfi_def_cfa_offset 96
.Lcfi6:
	.cfi_offset %rbx, -48
.Lcfi7:
	.cfi_offset %r12, -40
.Lcfi8:
	.cfi_offset %r14, -32
.Lcfi9:
	.cfi_offset %r15, -24
.Lcfi10:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movl	%r8d, %r15d
	movl	%ecx, %ebx
	movq	%rsi, %r12
	xorl	%r8d, %r8d
	xorl	%r9d, %r9d
	callq	cli_unfsg
	movl	$-1, %ebp
	testl	%eax, %eax
	jne	.LBB0_3
# BB#1:
	movl	104(%rsp), %eax
	movl	96(%rsp), %r8d
	movl	$0, 16(%rsp)
	movl	%ebx, 20(%rsp)
	movl	%ebx, 12(%rsp)
	movl	%r15d, 8(%rsp)
	leaq	8(%rsp), %rsi
	movl	$1, %ebp
	movl	$1, %edx
	movl	$0, %r9d
	movq	%r12, %rdi
	movl	%r14d, %ecx
	pushq	%rax
.Lcfi11:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi12:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi13:
	.cfi_adjust_cfa_offset -16
	testl	%eax, %eax
	jne	.LBB0_3
# BB#2:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%ebp, %ebp
.LBB0_3:
	movl	%ebp, %eax
	addq	$48, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	unfsg_200, .Lfunc_end0-unfsg_200
	.cfi_endproc

	.globl	unfsg_133
	.p2align	4, 0x90
	.type	unfsg_133,@function
unfsg_133:                              # @unfsg_133
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi14:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi15:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi16:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi17:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi18:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi19:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi20:
	.cfi_def_cfa_offset 112
.Lcfi21:
	.cfi_offset %rbx, -56
.Lcfi22:
	.cfi_offset %r12, -48
.Lcfi23:
	.cfi_offset %r13, -40
.Lcfi24:
	.cfi_offset %r14, -32
.Lcfi25:
	.cfi_offset %r15, -24
.Lcfi26:
	.cfi_offset %rbp, -16
	movl	%r9d, %r14d
	movq	%r8, %r15
	movl	%ecx, %r10d
	movl	%edx, %r12d
	movq	%rsi, %rbp
	movq	%rdi, %r13
	movq	%r13, 16(%rsp)
	movq	%rbp, 8(%rsp)
	testl	%r14d, %r14d
	movq	%r15, (%rsp)            # 8-byte Spill
	js	.LBB1_1
# BB#3:                                 # %.lr.ph133
	leaq	16(%rsp), %r8
	leaq	8(%rsp), %r9
	movq	%r13, %rdi
	movq	%rbp, %rsi
	movl	%r12d, %edx
	movl	%r10d, %ecx
	movl	%r10d, %ebx
	callq	cli_unfsg
	movl	$-1, %ecx
	cmpl	$-1, %eax
	je	.LBB1_25
# BB#4:                                 # %.lr.ph162
	movl	%r12d, %eax
	addq	%r13, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	movl	%ebx, %eax
	addq	%rbp, %rax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	movq	%r14, 32(%rsp)          # 8-byte Spill
	movslq	%r14d, %r13
	leaq	12(%r15), %r15
	xorl	%eax, %eax
	movq	$-1, %r14
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movq	%rbp, %r12
	movl	%ebx, %r10d
	.p2align	4, 0x90
.LBB1_5:                                # =>This Inner Loop Header: Depth=1
	movq	%r12, %rcx
	movl	%eax, -4(%r15)
	movq	8(%rsp), %r12
	movq	%r12, %rbx
	subq	%rcx, %rbx
	movl	%ebx, (%r15)
	incq	%r14
	cmpq	%r13, %r14
	jge	.LBB1_6
# BB#7:                                 # %._crit_edge144
                                        #   in Loop: Header=BB1_5 Depth=1
	addq	$36, %r15
	addl	%eax, %ebx
	movq	16(%rsp), %rdi
	movq	48(%rsp), %rax          # 8-byte Reload
	movl	%eax, %edx
	subl	%edi, %edx
	movq	40(%rsp), %rax          # 8-byte Reload
	movl	%eax, %ecx
	subl	%r12d, %ecx
	movq	%r12, %rsi
	leaq	16(%rsp), %r8
	leaq	8(%rsp), %r9
	movl	%r10d, %ebp
	callq	cli_unfsg
	movl	%ebp, %r10d
	cmpl	$-1, %eax
	movl	%ebx, %eax
	jne	.LBB1_5
# BB#8:
	movl	$-1, %ecx
	jmp	.LBB1_25
.LBB1_1:                                # %..preheader119.preheader_crit_edge
	movq	%rbp, 24(%rsp)          # 8-byte Spill
	movslq	%r14d, %r13
	jmp	.LBB1_2
.LBB1_6:
	movq	32(%rsp), %r14          # 8-byte Reload
	movq	(%rsp), %r15            # 8-byte Reload
.LBB1_2:                                # %.preheader119.preheader
	movl	128(%rsp), %r11d
	movl	120(%rsp), %r8d
	movl	112(%rsp), %ecx
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader119
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_16 Depth 2
                                        #       Child Loop BB1_13 Depth 3
	xorl	%ebp, %ebp
	xorl	%eax, %eax
	jmp	.LBB1_16
	.p2align	4, 0x90
.LBB1_15:                               #   in Loop: Header=BB1_16 Depth=2
	movl	-28(%rdx), %ebx
	movl	-24(%rdx), %ebp
	movl	%edi, -36(%rdx)
	movl	12(%rdx), %edi
	movl	%edi, -24(%rdx)
	movl	8(%rdx), %edi
	movl	%edi, -28(%rdx)
	movl	%esi, (%rdx)
	movl	%ebx, 8(%rdx)
	movl	%ebp, 12(%rdx)
	incq	%rax
	movl	$1, %ebp
.LBB1_16:                               # %.outer
                                        #   Parent Loop BB1_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_13 Depth 3
	cltq
	leaq	(%rax,%rax,8), %rdx
	leaq	(%r15,%rdx,4), %rdx
	decq	%rax
	.p2align	4, 0x90
.LBB1_13:                               #   Parent Loop BB1_10 Depth=1
                                        #     Parent Loop BB1_16 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	incq	%rax
	cmpq	%r13, %rax
	jge	.LBB1_9
# BB#14:                                #   in Loop: Header=BB1_13 Depth=3
	movl	(%rdx), %esi
	movl	36(%rdx), %edi
	leaq	36(%rdx), %rdx
	cmpl	%edi, %esi
	jbe	.LBB1_13
	jmp	.LBB1_15
	.p2align	4, 0x90
.LBB1_9:                                # %.loopexit
                                        #   in Loop: Header=BB1_10 Depth=1
	testl	%ebp, %ebp
	jne	.LBB1_10
# BB#11:                                # %.preheader
	testl	%r14d, %r14d
	js	.LBB1_12
# BB#17:                                # %.lr.ph.preheader
	movl	%r14d, %r13d
	incl	%r14d
	leaq	(%r13,%r13,8), %rax
	leaq	(%r15,%rax,4), %r12
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB1_18:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	cmpq	%r13, %rsi
	jne	.LBB1_19
# BB#20:                                #   in Loop: Header=BB1_18 Depth=1
	movl	(%r12), %edx
	leaq	1(%rsi), %rbx
	movl	%r10d, %ebp
	movq	(%rsp), %rdi            # 8-byte Reload
	jmp	.LBB1_21
	.p2align	4, 0x90
.LBB1_19:                               #   in Loop: Header=BB1_18 Depth=1
	leaq	1(%rsi), %rbx
	leaq	(%rsi,%rsi,8), %rax
	movq	(%rsp), %rdi            # 8-byte Reload
	movl	36(%rdi,%rax,4), %ecx
	movl	(%rdi,%rax,4), %edx
	addl	%edx, %r10d
	movl	%r10d, %ebp
	subl	%ecx, %ebp
	subl	%edx, %ecx
	movl	%ecx, %r10d
.LBB1_21:                               #   in Loop: Header=BB1_18 Depth=1
	leaq	(%rsi,%rsi,8), %rax
	movl	%r10d, 4(%rdi,%rax,4)
	movl	8(%rdi,%rax,4), %r8d
	movl	12(%rdi,%rax,4), %r9d
	movl	$.L.str.1, %edi
	xorl	%eax, %eax
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r10d, %ecx
	callq	cli_dbgmsg
	cmpq	%r14, %rbx
	movq	%rbx, %rsi
	movl	%ebp, %r10d
	jne	.LBB1_18
# BB#22:
	movq	(%rsp), %r15            # 8-byte Reload
	movl	128(%rsp), %r11d
	movl	120(%rsp), %r8d
	movl	112(%rsp), %ecx
	jmp	.LBB1_23
.LBB1_12:                               # %.preheader.._crit_edge_crit_edge
	incl	%r14d
.LBB1_23:                               # %._crit_edge
	movl	$0, %r9d
	movq	24(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	movl	%r14d, %edx
	pushq	%r11
.Lcfi27:
	.cfi_adjust_cfa_offset 8
	pushq	$0
.Lcfi28:
	.cfi_adjust_cfa_offset 8
	callq	cli_rebuildpe
	addq	$16, %rsp
.Lcfi29:
	.cfi_adjust_cfa_offset -16
	movl	$1, %ecx
	testl	%eax, %eax
	jne	.LBB1_25
# BB#24:
	movl	$.L.str, %edi
	xorl	%eax, %eax
	callq	cli_dbgmsg
	xorl	%ecx, %ecx
.LBB1_25:                               # %.loopexit121
	movl	%ecx, %eax
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	unfsg_133, .Lfunc_end1-unfsg_133
	.cfi_endproc

	.type	.L.str,@object          # @.str
	.section	.rodata.str1.1,"aMS",@progbits,1
.L.str:
	.asciz	"FSG: Rebuilding failed\n"
	.size	.L.str, 24

	.type	.L.str.1,@object        # @.str.1
.L.str.1:
	.asciz	"FSG: .SECT%d RVA:%x VSize:%x ROffset: %x, RSize:%x\n"
	.size	.L.str.1, 52


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
