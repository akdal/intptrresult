	.text
	.file	"findnext.bc"
	.globl	findnextmove
	.p2align	4, 0x90
	.type	findnextmove,@function
findnextmove:                           # @findnextmove
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 96
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movl	%r9d, %r12d
	movq	%r8, %r13
	movl	%esi, %ebp
	movl	%edi, %ebx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movl	$-1, (%rdx)
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movl	$-1, (%rcx)
	movl	$-1, (%r13)
	movslq	%ebx, %rax
	movslq	%ebp, %r14
	imulq	$19, %rax, %r15
	movb	$1, ma(%r15,%r14)
	testl	%eax, %eax
	je	.LBB0_13
# BB#1:
	leal	-1(%rbx), %edi
	movslq	%edi, %rax
	imulq	$19, %rax, %rax
	movzbl	p(%rax,%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB0_2
# BB#5:
	cmpl	mymove(%rip), %ecx
	jne	.LBB0_12
# BB#6:
	cmpb	$0, ma(%rax,%r14)
	jne	.LBB0_12
# BB#7:
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	12(%rsp), %r8
	movl	%ebp, %esi
	movl	%r12d, %r9d
	callq	findnextmove
	testl	%eax, %eax
	je	.LBB0_12
# BB#8:                                 # %._crit_edge
	movl	12(%rsp), %eax
	cmpl	(%r13), %eax
	jg	.LBB0_10
	jmp	.LBB0_11
.LBB0_2:
	movl	%edi, 20(%rsp)
	movl	%ebp, 16(%rsp)
	movl	$0, lib(%rip)
	movl	mymove(%rip), %edx
	movl	%ebp, %esi
	callq	countlib
	movl	lib(%rip), %ecx
	movl	$-1, %eax
	subl	%r12d, %ecx
	jle	.LBB0_4
# BB#3:
	imull	$50, %ecx, %eax
	addl	$-50, %eax
	movl	%r12d, %ecx
	imull	%ecx, %ecx
	imull	%r12d, %ecx
	cltd
	idivl	%ecx
	addl	$40, %eax
.LBB0_4:                                # %fval.exit
	movl	%eax, 12(%rsp)
	cmpl	(%r13), %eax
	jle	.LBB0_11
.LBB0_10:
	movl	%eax, (%r13)
	movl	20(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	16(%rsp), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB0_11:
	movl	$1, %eax
	cmpl	$1, %r12d
	je	.LBB0_49
.LBB0_12:                               # %.thread
	cmpl	$18, %ebx
	je	.LBB0_24
.LBB0_13:                               # %.thread.thread
	leal	1(%rbx), %edi
	movslq	%edi, %rax
	imulq	$19, %rax, %rax
	movzbl	p(%rax,%r14), %ecx
	testl	%ecx, %ecx
	je	.LBB0_14
# BB#17:
	cmpl	mymove(%rip), %ecx
	jne	.LBB0_24
# BB#18:
	cmpb	$0, ma(%rax,%r14)
	jne	.LBB0_24
# BB#19:
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	12(%rsp), %r8
	movl	%ebp, %esi
	movl	%r12d, %r9d
	callq	findnextmove
	testl	%eax, %eax
	je	.LBB0_24
# BB#20:                                # %._crit_edge96
	movl	12(%rsp), %eax
	cmpl	(%r13), %eax
	jg	.LBB0_22
	jmp	.LBB0_23
.LBB0_14:
	movl	%edi, 20(%rsp)
	movl	%ebp, 16(%rsp)
	movl	$0, lib(%rip)
	movl	mymove(%rip), %edx
	movl	%ebp, %esi
	callq	countlib
	movl	lib(%rip), %ecx
	movl	$-1, %eax
	subl	%r12d, %ecx
	jle	.LBB0_16
# BB#15:
	imull	$50, %ecx, %eax
	addl	$-50, %eax
	movl	%r12d, %ecx
	imull	%ecx, %ecx
	imull	%r12d, %ecx
	cltd
	idivl	%ecx
	addl	$40, %eax
.LBB0_16:                               # %fval.exit84
	movl	%eax, 12(%rsp)
	cmpl	(%r13), %eax
	jle	.LBB0_23
.LBB0_22:
	movl	%eax, (%r13)
	movl	20(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	16(%rsp), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB0_23:
	movl	$1, %eax
	cmpl	$1, %r12d
	je	.LBB0_49
.LBB0_24:                               # %.thread89
	testl	%ebp, %ebp
	je	.LBB0_37
# BB#25:
	leal	-1(%rbp), %esi
	movzbl	p-1(%r15,%r14), %eax
	testl	%eax, %eax
	je	.LBB0_26
# BB#29:
	cmpl	mymove(%rip), %eax
	jne	.LBB0_36
# BB#30:
	leaq	-1(%r14), %rax
	cmpb	$0, ma(%r15,%rax)
	jne	.LBB0_36
# BB#31:
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	12(%rsp), %r8
	movl	%ebx, %edi
	movl	%r12d, %r9d
	callq	findnextmove
	testl	%eax, %eax
	je	.LBB0_36
# BB#32:                                # %._crit_edge98
	movl	12(%rsp), %eax
	cmpl	(%r13), %eax
	jg	.LBB0_34
	jmp	.LBB0_35
.LBB0_26:
	movl	%ebx, 20(%rsp)
	movl	%esi, 16(%rsp)
	movl	$0, lib(%rip)
	movl	mymove(%rip), %edx
	movl	%ebx, %edi
	callq	countlib
	movl	lib(%rip), %ecx
	movl	$-1, %eax
	subl	%r12d, %ecx
	jle	.LBB0_28
# BB#27:
	imull	$50, %ecx, %eax
	addl	$-50, %eax
	movl	%r12d, %ecx
	imull	%ecx, %ecx
	imull	%r12d, %ecx
	cltd
	idivl	%ecx
	addl	$40, %eax
.LBB0_28:                               # %fval.exit86
	movl	%eax, 12(%rsp)
	cmpl	(%r13), %eax
	jle	.LBB0_35
.LBB0_34:
	movl	%eax, (%r13)
	movl	20(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	16(%rsp), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB0_35:
	movl	$1, %eax
	cmpl	$1, %r12d
	je	.LBB0_49
.LBB0_36:                               # %.thread92
	cmpl	$18, %ebp
	je	.LBB0_48
.LBB0_37:                               # %.thread92.thread
	incl	%ebp
	movzbl	p+1(%r15,%r14), %eax
	testl	%eax, %eax
	je	.LBB0_38
# BB#41:
	cmpl	mymove(%rip), %eax
	jne	.LBB0_48
# BB#42:
	incq	%r14
	cmpb	$0, ma(%r15,%r14)
	jne	.LBB0_48
# BB#43:
	leaq	20(%rsp), %rdx
	leaq	16(%rsp), %rcx
	leaq	12(%rsp), %r8
	movl	%ebx, %edi
	movl	%ebp, %esi
	movl	%r12d, %r9d
	callq	findnextmove
	testl	%eax, %eax
	je	.LBB0_48
# BB#44:                                # %._crit_edge100
	movl	12(%rsp), %eax
	cmpl	(%r13), %eax
	jg	.LBB0_46
	jmp	.LBB0_47
.LBB0_38:
	movl	%ebx, 20(%rsp)
	movl	%ebp, 16(%rsp)
	movl	$0, lib(%rip)
	movl	mymove(%rip), %edx
	movl	%ebx, %edi
	movl	%ebp, %esi
	callq	countlib
	movl	lib(%rip), %ecx
	movl	$-1, %eax
	subl	%r12d, %ecx
	jle	.LBB0_40
# BB#39:
	imull	$50, %ecx, %eax
	addl	$-50, %eax
	movl	%r12d, %ecx
	imull	%ecx, %ecx
	imull	%r12d, %ecx
	cltd
	idivl	%ecx
	addl	$40, %eax
.LBB0_40:                               # %fval.exit82
	movl	%eax, 12(%rsp)
	cmpl	(%r13), %eax
	jle	.LBB0_47
.LBB0_46:
	movl	%eax, (%r13)
	movl	20(%rsp), %eax
	movq	24(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
	movl	16(%rsp), %eax
	movq	32(%rsp), %rcx          # 8-byte Reload
	movl	%eax, (%rcx)
.LBB0_47:
	movl	$1, %eax
	cmpl	$1, %r12d
	je	.LBB0_49
.LBB0_48:                               # %.thread95
	xorl	%eax, %eax
	cmpl	$0, (%r13)
	setg	%al
.LBB0_49:
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	findnextmove, .Lfunc_end0-findnextmove
	.cfi_endproc

	.globl	fval
	.p2align	4, 0x90
	.type	fval,@function
fval:                                   # @fval
	.cfi_startproc
# BB#0:
	movl	$-1, %eax
	subl	%esi, %edi
	jle	.LBB1_2
# BB#1:
	imull	$50, %edi, %eax
	addl	$-50, %eax
	movl	%esi, %ecx
	imull	%ecx, %ecx
	imull	%esi, %ecx
	cltd
	idivl	%ecx
	addl	$40, %eax
.LBB1_2:
	retq
.Lfunc_end1:
	.size	fval, .Lfunc_end1-fval
	.cfi_endproc


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
