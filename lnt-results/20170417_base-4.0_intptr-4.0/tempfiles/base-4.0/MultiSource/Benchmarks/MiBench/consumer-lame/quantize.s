	.text
	.file	"quantize.bc"
	.globl	iteration_loop
	.p2align	4, 0x90
	.type	iteration_loop,@function
iteration_loop:                         # @iteration_loop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$1208, %rsp             # imm = 0x4B8
.Lcfi6:
	.cfi_def_cfa_offset 1264
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%r9, %rbp
	movq	%r8, 144(%rsp)          # 8-byte Spill
	movq	%rcx, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 128(%rsp)         # 8-byte Spill
	movq	%rsi, 136(%rsp)         # 8-byte Spill
	movq	%rdi, %r15
	movq	1264(%rsp), %rbx
	movq	%rbp, %rsi
	movq	%rbx, %rdx
	callq	iteration_init
	leaq	92(%rsp), %rsi
	leaq	52(%rsp), %rdx
	movq	%r15, %rdi
	callq	getframebits
	movl	52(%rsp), %edx
	movl	92(%rsp), %ecx
	movq	%r15, %rdi
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	movq	%rbp, %rsi
	callq	ResvFrameBegin
	cmpl	$0, 200(%r15)
	jle	.LBB0_22
# BB#1:                                 # %.lr.ph104
	movq	64(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %rax
	movq	%rax, 104(%rsp)         # 8-byte Spill
	leaq	4(%rbx), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	xorl	%eax, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	120(%rsp), %rbp
	movq	%r15, 72(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_8 Depth 2
                                        #       Child Loop BB0_15 Depth 3
	cmpl	$0, convert_mdct(%rip)
	je	.LBB0_4
# BB#3:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%rax,8), %rdi
	shlq	$10, %rdi
	addq	64(%rsp), %rdi          # 8-byte Folded Reload
	movq	%rdi, %rsi
	callq	ms_convert
.LBB0_4:                                #   in Loop: Header=BB0_2 Depth=1
	movl	52(%rsp), %r8d
	movq	%r15, %rdi
	movq	136(%rsp), %rsi         # 8-byte Reload
	movq	56(%rsp), %rdx          # 8-byte Reload
	movq	%rbp, %rcx
	movq	40(%rsp), %r9           # 8-byte Reload
                                        # kill: %R9D<def> %R9D<kill> %R9<kill>
	callq	on_pe
	cmpl	$0, reduce_sidechannel(%rip)
	je	.LBB0_6
# BB#5:                                 #   in Loop: Header=BB0_2 Depth=1
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	40(%rsp), %rcx          # 8-byte Reload
	movsd	(%rax,%rcx,8), %xmm0    # xmm0 = mem[0],zero
	movl	52(%rsp), %esi
	movq	%rbp, %rdi
	callq	reduce_side
.LBB0_6:                                # %.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	cmpl	$0, 204(%r15)
	jle	.LBB0_21
# BB#7:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	96(%rsp), %r14          # 8-byte Reload
	movq	104(%rsp), %rbp         # 8-byte Reload
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB0_8:                                # %.lr.ph
                                        #   Parent Loop BB0_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_15 Depth 3
	movq	40(%rsp), %rax          # 8-byte Reload
	imulq	$240, %rax, %rcx
	addq	56(%rsp), %rcx          # 8-byte Folded Reload
	imulq	$120, %r12, %rdx
	movq	%rcx, 184(%rsp)         # 8-byte Spill
	movq	%rdx, 176(%rsp)         # 8-byte Spill
	leaq	48(%rdx,%rcx), %rdx
	leaq	(%rax,%rax,8), %rbx
	movq	%rbx, %rax
	shlq	$10, %rax
	addq	64(%rsp), %rax          # 8-byte Folded Reload
	leaq	(%r12,%r12,8), %r13
	movq	%r15, %rdi
	movq	%r13, %r15
	shlq	$9, %r15
	addq	%rax, %r15
	movq	%r15, %rsi
	movq	%rdx, 80(%rsp)          # 8-byte Spill
	callq	init_outer_loop
	testl	%eax, %eax
	movq	%r14, 112(%rsp)         # 8-byte Spill
	movq	%rbx, 160(%rsp)         # 8-byte Spill
	movq	%r13, 152(%rsp)         # 8-byte Spill
	je	.LBB0_9
# BB#10:                                #   in Loop: Header=BB0_8 Depth=2
	movq	40(%rsp), %rax          # 8-byte Reload
	imulq	$1952, %rax, %rax       # imm = 0x7A0
	addq	144(%rsp), %rax         # 8-byte Folded Reload
	imulq	$976, %r12, %rdx        # imm = 0x3D0
	addq	%rax, %rdx
	imulq	$488, %r12, %rax        # imm = 0x1E8
	leaq	224(%rsp,%rax), %r8
	movq	%r8, 168(%rsp)          # 8-byte Spill
	movq	72(%rsp), %rdi          # 8-byte Reload
	movq	%r15, %rsi
	movq	80(%rsp), %rcx          # 8-byte Reload
	callq	calc_xmin
	movl	120(%rsp,%r12,4), %edx
	movq	%rbx, %rax
	shlq	$9, %rax
	movq	1264(%rsp), %rcx
	movq	80(%rsp), %rbx          # 8-byte Reload
	movq	%rcx, %r14
	addq	%r14, %rax
	movq	%r13, %r9
	shlq	$8, %r9
	addq	%rax, %r9
	imulq	$488, 40(%rsp), %rax    # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	1272(%rsp), %rax
	imulq	$244, %r12, %rcx
	addq	%rax, %rcx
	movl	%r12d, 24(%rsp)
	movq	%rbx, 8(%rsp)
	movq	%rcx, (%rsp)
	movq	72(%rsp), %r13          # 8-byte Reload
	movq	%r13, %rdi
	movq	%r15, %rsi
	leaq	192(%rsp), %rcx
	movq	168(%rsp), %r8          # 8-byte Reload
	callq	outer_loop
	movq	%r13, %r15
	movq	%r14, %r13
	jmp	.LBB0_11
	.p2align	4, 0x90
.LBB0_9:                                #   in Loop: Header=BB0_8 Depth=2
	imulq	$488, 40(%rsp), %rax    # 8-byte Folded Reload
                                        # imm = 0x1E8
	addq	1272(%rsp), %rax
	imulq	$244, %r12, %rcx
	xorps	%xmm0, %xmm0
	movups	%xmm0, 224(%rcx,%rax)
	movups	%xmm0, 208(%rcx,%rax)
	movups	%xmm0, 192(%rcx,%rax)
	movups	%xmm0, 176(%rcx,%rax)
	movups	%xmm0, 160(%rcx,%rax)
	movups	%xmm0, 144(%rcx,%rax)
	movups	%xmm0, 128(%rcx,%rax)
	movups	%xmm0, 112(%rcx,%rax)
	movups	%xmm0, 96(%rcx,%rax)
	movups	%xmm0, 80(%rcx,%rax)
	movups	%xmm0, 64(%rcx,%rax)
	movups	%xmm0, 48(%rcx,%rax)
	movups	%xmm0, 32(%rcx,%rax)
	movups	%xmm0, 16(%rcx,%rax)
	movups	%xmm0, (%rcx,%rax)
	movl	$0, 240(%rcx,%rax)
	movq	%rbx, %rax
	shlq	$9, %rax
	movq	1264(%rsp), %rcx
	movq	%rcx, %r14
	addq	%r14, %rax
	shlq	$8, %r13
	addq	%rax, %r13
	xorl	%esi, %esi
	movl	$2304, %edx             # imm = 0x900
	movq	%r13, %rdi
	callq	memset
	xorps	%xmm0, %xmm0
	movaps	%xmm0, 208(%rsp)
	movaps	%xmm0, 192(%rsp)
	movq	72(%rsp), %r15          # 8-byte Reload
	movq	%r14, %r13
	movq	80(%rsp), %rbx          # 8-byte Reload
.LBB0_11:                               #   in Loop: Header=BB0_8 Depth=2
	movq	112(%rsp), %r14         # 8-byte Reload
	movq	%r15, %rdi
	movq	40(%rsp), %rsi          # 8-byte Reload
                                        # kill: %ESI<def> %ESI<kill> %RSI<kill>
	movl	%r12d, %edx
	movq	%r13, %rcx
	movq	56(%rsp), %r8           # 8-byte Reload
	movq	1272(%rsp), %r9
	callq	best_scalefac_store
	cmpl	$1, 276(%r15)
	jne	.LBB0_14
# BB#12:                                #   in Loop: Header=BB0_8 Depth=2
	movq	184(%rsp), %rax         # 8-byte Reload
	movq	176(%rsp), %rcx         # 8-byte Reload
	cmpl	$0, 72(%rcx,%rax)
	jne	.LBB0_14
# BB#13:                                #   in Loop: Header=BB0_8 Depth=2
	movq	160(%rsp), %rax         # 8-byte Reload
	shlq	$9, %rax
	addq	%r13, %rax
	movq	152(%rsp), %rcx         # 8-byte Reload
	shlq	$8, %rcx
	addq	%rax, %rcx
	movq	40(%rsp), %rdi          # 8-byte Reload
                                        # kill: %EDI<def> %EDI<kill> %RDI<kill>
	movl	%r12d, %esi
	movq	%rbx, %rdx
	callq	best_huffman_divide
	.p2align	4, 0x90
.LBB0_14:                               #   in Loop: Header=BB0_8 Depth=2
	movl	52(%rsp), %ecx
	movq	%r15, %rdi
	movq	%rbx, %rsi
	movq	56(%rsp), %rdx          # 8-byte Reload
	callq	ResvAdjust
	movl	$576, %eax              # imm = 0x240
	movq	%r14, %rcx
	movq	%rbp, %rdx
	xorps	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB0_15:                               #   Parent Loop BB0_2 Depth=1
                                        #     Parent Loop BB0_8 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	-8(%rdx), %xmm0
	jbe	.LBB0_17
# BB#16:                                #   in Loop: Header=BB0_15 Depth=3
	negl	-4(%rcx)
.LBB0_17:                               #   in Loop: Header=BB0_15 Depth=3
	ucomisd	(%rdx), %xmm0
	jbe	.LBB0_19
# BB#18:                                #   in Loop: Header=BB0_15 Depth=3
	negl	(%rcx)
.LBB0_19:                               #   in Loop: Header=BB0_15 Depth=3
	addq	$16, %rdx
	addq	$8, %rcx
	addq	$-2, %rax
	jne	.LBB0_15
# BB#20:                                #   in Loop: Header=BB0_8 Depth=2
	incq	%r12
	movslq	204(%r15), %rax
	addq	$4608, %rbp             # imm = 0x1200
	addq	$2304, %r14             # imm = 0x900
	cmpq	%rax, %r12
	jl	.LBB0_8
.LBB0_21:                               # %._crit_edge
                                        #   in Loop: Header=BB0_2 Depth=1
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	%rax, %rdx
	incq	%rdx
	movslq	200(%r15), %rax
	addq	$9216, 104(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x2400
	addq	$4608, 96(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x1200
	movq	%rdx, %rcx
	movq	%rcx, 40(%rsp)          # 8-byte Spill
	cmpq	%rax, %rdx
	leaq	120(%rsp), %rbp
	jl	.LBB0_2
.LBB0_22:                               # %._crit_edge105
	movl	52(%rsp), %edx
	movq	%r15, %rdi
	movq	56(%rsp), %rsi          # 8-byte Reload
	callq	ResvFrameEnd
	addq	$1208, %rsp             # imm = 0x4B8
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	iteration_loop, .Lfunc_end0-iteration_loop
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI1_0:
	.quad	4427486594234968593     # double 9.9999999999999998E-13
.LCPI1_1:
	.quad	-4620693217682128896    # double -0.5
.LCPI1_2:
	.quad	4604418534313441775     # double 0.69314718055994529
.LCPI1_3:
	.quad	4602678819172646912     # double 0.5
.LCPI1_4:
	.quad	3125919792542303038     # double 1.0E-99
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_5:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	init_outer_loop
	.p2align	4, 0x90
	.type	init_outer_loop,@function
init_outer_loop:                        # @init_outer_loop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%rbx
.Lcfi15:
	.cfi_def_cfa_offset 32
	subq	$48, %rsp
.Lcfi16:
	.cfi_def_cfa_offset 80
.Lcfi17:
	.cfi_offset %rbx, -32
.Lcfi18:
	.cfi_offset %r14, -24
.Lcfi19:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 104(%rbx)
	movq	$nr_of_sfb_block, 96(%rbx)
	movq	$0, (%rbx)
	movl	$0, 8(%rbx)
	movl	$0, 16(%rbx)
	movl	$0, 76(%rbx)
	movupd	%xmm0, 48(%rbx)
	movupd	%xmm0, 32(%rbx)
	movq	$0, 64(%rbx)
	movl	$210, 12(%rbx)
	movl	$0, 72(%rbx)
	movl	$0, 88(%rbx)
	cmpl	$0, 84(%rdi)
	je	.LBB1_1
# BB#8:
	cmpl	$2, 24(%rbx)
	jne	.LBB1_1
# BB#9:                                 # %.preheader88.preheader
	xorpd	%xmm2, %xmm2
	xorpd	%xmm0, %xmm0
	movl	$40, %eax
	.p2align	4, 0x90
.LBB1_10:                               # %.preheader88
                                        # =>This Inner Loop Header: Depth=1
	movsd	-40(%rsi,%rax), %xmm3   # xmm3 = mem[0],zero
	movsd	-24(%rsi,%rax), %xmm1   # xmm1 = mem[0],zero
	mulsd	%xmm3, %xmm3
	addsd	%xmm0, %xmm3
	movhpd	-32(%rsi,%rax), %xmm1   # xmm1 = xmm1[0],mem[0]
	mulpd	%xmm1, %xmm1
	addpd	%xmm2, %xmm1
	movsd	-16(%rsi,%rax), %xmm0   # xmm0 = mem[0],zero
	mulsd	%xmm0, %xmm0
	addsd	%xmm3, %xmm0
	movsd	(%rsi,%rax), %xmm2      # xmm2 = mem[0],zero
	movhpd	-8(%rsi,%rax), %xmm2    # xmm2 = xmm2[0],mem[0]
	mulpd	%xmm2, %xmm2
	addpd	%xmm1, %xmm2
	addq	$48, %rax
	cmpl	$4648, %eax             # imm = 0x1228
	jne	.LBB1_10
# BB#11:                                # %.preheader87.preheader
	movsd	.LCPI1_0(%rip), %xmm3   # xmm3 = mem[0],zero
	movapd	%xmm3, %xmm1
	maxsd	%xmm0, %xmm1
	movapd	%xmm2, %xmm4
	movhlps	%xmm4, %xmm4            # xmm4 = xmm4[1,1]
	maxsd	%xmm4, %xmm1
	maxsd	%xmm2, %xmm1
	maxsd	%xmm3, %xmm0
	divsd	%xmm1, %xmm0
	movsd	%xmm0, 8(%rsp)          # 8-byte Spill
	maxsd	%xmm3, %xmm4
	divsd	%xmm1, %xmm4
	movapd	%xmm4, 32(%rsp)         # 16-byte Spill
	maxsd	%xmm3, %xmm2
	divsd	%xmm1, %xmm2
	movapd	%xmm2, 16(%rsp)         # 16-byte Spill
	callq	log
	mulsd	.LCPI1_1(%rip), %xmm0
	divsd	.LCPI1_2(%rip), %xmm0
	addsd	.LCPI1_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	$3, %eax
	movl	$2, %ebp
	movl	$2, %ecx
	cmovll	%eax, %ecx
	xorl	%r14d, %r14d
	testl	%eax, %eax
	cmovlel	%r14d, %ecx
	movl	%ecx, 44(%rbx)
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	callq	log
	mulsd	.LCPI1_1(%rip), %xmm0
	divsd	.LCPI1_2(%rip), %xmm0
	addsd	.LCPI1_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	$3, %eax
	movl	$2, %ecx
	cmovll	%eax, %ecx
	testl	%eax, %eax
	cmovlel	%r14d, %ecx
	movl	%ecx, 48(%rbx)
	movapd	16(%rsp), %xmm0         # 16-byte Reload
	callq	log
	mulsd	.LCPI1_1(%rip), %xmm0
	divsd	.LCPI1_2(%rip), %xmm0
	addsd	.LCPI1_3(%rip), %xmm0
	cvttsd2si	%xmm0, %eax
	cmpl	$3, %eax
	cmovll	%eax, %ebp
	testl	%eax, %eax
	cmovlel	%r14d, %ebp
	movl	%ebp, 52(%rbx)
	movapd	32(%rsp), %xmm0         # 16-byte Reload
	addsd	8(%rsp), %xmm0          # 8-byte Folded Reload
	addsd	16(%rsp), %xmm0         # 16-byte Folded Reload
	xorl	%eax, %eax
	ucomisd	.LCPI1_4(%rip), %xmm0
	seta	%al
	jmp	.LBB1_12
.LBB1_1:                                # %.preheader.preheader
	xorl	%ecx, %ecx
	movapd	.LCPI1_5(%rip), %xmm0   # xmm0 = [nan,nan]
	movsd	.LCPI1_4(%rip), %xmm1   # xmm1 = mem[0],zero
	.p2align	4, 0x90
.LBB1_2:                                # %.preheader
                                        # =>This Inner Loop Header: Depth=1
	movsd	(%rsi,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	andpd	%xmm0, %xmm2
	movl	$1, %eax
	ucomisd	%xmm1, %xmm2
	ja	.LBB1_12
# BB#3:                                 # %.preheader.1127
                                        #   in Loop: Header=BB1_2 Depth=1
	movsd	8(%rsi,%rcx,8), %xmm2   # xmm2 = mem[0],zero
	andpd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	ja	.LBB1_12
# BB#4:                                 # %.preheader.2128
                                        #   in Loop: Header=BB1_2 Depth=1
	movsd	16(%rsi,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	andpd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	ja	.LBB1_12
# BB#5:                                 # %.preheader.3129
                                        #   in Loop: Header=BB1_2 Depth=1
	movsd	24(%rsi,%rcx,8), %xmm2  # xmm2 = mem[0],zero
	andpd	%xmm0, %xmm2
	ucomisd	%xmm1, %xmm2
	ja	.LBB1_12
# BB#6:                                 #   in Loop: Header=BB1_2 Depth=1
	addq	$4, %rcx
	cmpq	$576, %rcx              # imm = 0x240
	jl	.LBB1_2
# BB#7:
	xorl	%eax, %eax
.LBB1_12:                               # %.loopexit
	addq	$48, %rsp
	popq	%rbx
	popq	%r14
	popq	%rbp
	retq
.Lfunc_end1:
	.size	init_outer_loop, .Lfunc_end1-init_outer_loop
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI2_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.text
	.globl	outer_loop
	.p2align	4, 0x90
	.type	outer_loop,@function
outer_loop:                             # @outer_loop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi20:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi21:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi22:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi23:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi24:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi25:
	.cfi_def_cfa_offset 56
	subq	$8792, %rsp             # imm = 0x2258
.Lcfi26:
	.cfi_def_cfa_offset 8848
.Lcfi27:
	.cfi_offset %rbx, -56
.Lcfi28:
	.cfi_offset %r12, -48
.Lcfi29:
	.cfi_offset %r13, -40
.Lcfi30:
	.cfi_offset %r14, -32
.Lcfi31:
	.cfi_offset %r15, -24
.Lcfi32:
	.cfi_offset %rbp, -16
	movq	%r9, 136(%rsp)          # 8-byte Spill
	movq	%r8, 128(%rsp)          # 8-byte Spill
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%edx, 28(%rsp)          # 4-byte Spill
	movq	%rsi, %r14
	movq	%rdi, %r12
	movq	8856(%rsp), %rbx
	xorl	%ecx, %ecx
	movl	$100, 12(%rsp)          # 4-byte Folded Spill
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
	movslq	8872(%rsp), %rax
	movq	%rax, 120(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	xorpd	%xmm0, %xmm0
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	xorl	%r15d, %r15d
	xorl	%r13d, %r13d
	testb	$1, %cl
	jne	.LBB2_17
	jmp	.LBB2_2
	.p2align	4, 0x90
.LBB2_16:
	movq	120(%rsp), %rbp         # 8-byte Reload
	movl	outer_loop.OldValue(,%rbp,4), %edx
	movq	%r12, %rdi
	movl	28(%rsp), %esi          # 4-byte Reload
	leaq	1200(%rsp), %rcx
	leaq	4176(%rsp), %r8
	movq	%rbx, %r9
	callq	bin_search_StepSize2
	movl	%eax, %r13d
	movl	12(%rbx), %eax
	movl	%eax, outer_loop.OldValue(,%rbp,4)
.LBB2_17:                               # =>This Inner Loop Header: Depth=1
	movl	28(%rsp), %ecx          # 4-byte Reload
	subl	76(%rbx), %ecx
	js	.LBB2_37
# BB#18:                                #   in Loop: Header=BB2_17 Depth=1
	testl	%r15d, %r15d
	jne	.LBB2_21
# BB#19:                                #   in Loop: Header=BB2_17 Depth=1
	cmpl	%ecx, %r13d
	movl	%r13d, %eax
	jle	.LBB2_22
# BB#20:                                #   in Loop: Header=BB2_17 Depth=1
	incl	12(%rbx)
.LBB2_21:                               # %.sink.split
                                        #   in Loop: Header=BB2_17 Depth=1
	movq	%r12, %rdi
	leaq	4176(%rsp), %rsi
	leaq	1200(%rsp), %rdx
	movq	%rbx, %r8
	callq	inner_loop
.LBB2_22:                               #   in Loop: Header=BB2_17 Depth=1
	movl	%eax, (%rbx)
	cmpl	$0, 264(%r12)
	je	.LBB2_23
# BB#24:                                #   in Loop: Header=BB2_17 Depth=1
	movq	%r14, %rdi
	leaq	1200(%rsp), %rsi
	movq	%rbx, %rdx
	leaq	3504(%rsp), %rcx
	leaq	528(%rsp), %r8
	movq	128(%rsp), %r9          # 8-byte Reload
	leaq	112(%rsp), %rax
	pushq	%rax
.Lcfi33:
	.cfi_adjust_cfa_offset 8
	leaq	104(%rsp), %rax
	pushq	%rax
.Lcfi34:
	.cfi_adjust_cfa_offset 8
	leaq	120(%rsp), %rax
	pushq	%rax
.Lcfi35:
	.cfi_adjust_cfa_offset 8
	leaq	296(%rsp), %rax
	pushq	%rax
.Lcfi36:
	.cfi_adjust_cfa_offset 8
	callq	calc_noise1
	addq	$32, %rsp
.Lcfi37:
	.cfi_adjust_cfa_offset -32
	movl	%eax, %ebp
	testl	%r15d, %r15d
	jne	.LBB2_27
	jmp	.LBB2_26
	.p2align	4, 0x90
.LBB2_23:                               #   in Loop: Header=BB2_17 Depth=1
	xorl	%ebp, %ebp
	testl	%r15d, %r15d
	je	.LBB2_26
.LBB2_27:                               #   in Loop: Header=BB2_17 Depth=1
	movl	76(%r12), %edi
	movsd	96(%rsp), %xmm3         # xmm3 = mem[0],zero
	movsd	104(%rsp), %xmm4        # xmm4 = mem[0],zero
	movsd	112(%rsp), %xmm5        # xmm5 = mem[0],zero
	movl	12(%rsp), %esi          # 4-byte Reload
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	64(%rsp), %xmm1         # 8-byte Reload
                                        # xmm1 = mem[0],zero
	movsd	56(%rsp), %xmm2         # 8-byte Reload
                                        # xmm2 = mem[0],zero
	movl	%ebp, %edx
	movsd	%xmm3, 32(%rsp)         # 8-byte Spill
	movsd	%xmm4, 88(%rsp)         # 8-byte Spill
	movsd	%xmm5, 80(%rsp)         # 8-byte Spill
	callq	quant_compare
	testl	%eax, %eax
	jne	.LBB2_28
	jmp	.LBB2_29
	.p2align	4, 0x90
.LBB2_26:                               # %..thread_crit_edge
                                        #   in Loop: Header=BB2_17 Depth=1
	movsd	112(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 80(%rsp)         # 8-byte Spill
	movsd	104(%rsp), %xmm0        # xmm0 = mem[0],zero
	movsd	%xmm0, 88(%rsp)         # 8-byte Spill
	movsd	96(%rsp), %xmm0         # xmm0 = mem[0],zero
	movsd	%xmm0, 32(%rsp)         # 8-byte Spill
.LBB2_28:                               # %.thread
                                        #   in Loop: Header=BB2_17 Depth=1
	movl	$244, %edx
	movq	8848(%rsp), %rdi
	leaq	272(%rsp), %rsi
	callq	memcpy
	movl	$2304, %edx             # imm = 0x900
	movq	136(%rsp), %rdi         # 8-byte Reload
	leaq	1200(%rsp), %rsi
	callq	memcpy
	movq	112(%rbx), %rax
	movq	%rax, 256(%rsp)
	movups	96(%rbx), %xmm0
	movaps	%xmm0, 240(%rsp)
	movups	80(%rbx), %xmm0
	movaps	%xmm0, 224(%rsp)
	movups	64(%rbx), %xmm0
	movaps	%xmm0, 208(%rsp)
	movups	(%rbx), %xmm0
	movupd	16(%rbx), %xmm1
	movups	32(%rbx), %xmm2
	movupd	48(%rbx), %xmm3
	movapd	%xmm3, 192(%rsp)
	movaps	%xmm2, 176(%rsp)
	movapd	%xmm1, 160(%rsp)
	movaps	%xmm0, 144(%rsp)
	movl	%ebp, 12(%rsp)          # 4-byte Spill
	movsd	80(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 56(%rsp)         # 8-byte Spill
	movsd	88(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 64(%rsp)         # 8-byte Spill
	movsd	32(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 72(%rsp)         # 8-byte Spill
.LBB2_29:                               #   in Loop: Header=BB2_17 Depth=1
	orl	268(%r12), %ebp
	je	.LBB2_37
# BB#30:                                #   in Loop: Header=BB2_17 Depth=1
	leaq	4176(%rsp), %rdi
	movq	%rbx, %rsi
	leaq	272(%rsp), %rbp
	movq	%rbp, %rdx
	leaq	528(%rsp), %rcx
	callq	amp_scalefac_bands
	movq	%rbp, %rdi
	movq	%rbx, %rsi
	callq	loop_break
	testl	%eax, %eax
	jne	.LBB2_37
# BB#31:                                #   in Loop: Header=BB2_17 Depth=1
	cmpl	$1, 192(%r12)
	jne	.LBB2_33
# BB#32:                                #   in Loop: Header=BB2_17 Depth=1
	leaq	272(%rsp), %rdi
	movq	%rbx, %rsi
	callq	scale_bitcount
	jmp	.LBB2_34
	.p2align	4, 0x90
.LBB2_33:                               #   in Loop: Header=BB2_17 Depth=1
	leaq	272(%rsp), %rdi
	movq	%rbx, %rsi
	callq	scale_bitcount_lsf
.LBB2_34:                               #   in Loop: Header=BB2_17 Depth=1
	incl	%r15d
	movb	$1, %cl
	testl	%eax, %eax
	je	.LBB2_1
# BB#35:                                #   in Loop: Header=BB2_17 Depth=1
	cmpl	$0, 68(%rbx)
	jne	.LBB2_37
# BB#36:                                #   in Loop: Header=BB2_17 Depth=1
	cmpl	$0, 80(%r12)
	je	.LBB2_37
# BB#38:                                # %.thread138.backedge.thread162
                                        #   in Loop: Header=BB2_17 Depth=1
	movq	%r12, %rdi
	movq	%r14, %rsi
	movq	%rbx, %rdx
	callq	init_outer_loop
	movl	$1, 68(%rbx)
	xorl	%ecx, %ecx
.LBB2_1:                                # %.thread138.backedge.thread165
                                        #   in Loop: Header=BB2_17 Depth=1
	testb	$1, %cl
	jne	.LBB2_17
.LBB2_2:
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 496(%rsp)
	movapd	%xmm0, 480(%rsp)
	movapd	%xmm0, 464(%rsp)
	movapd	%xmm0, 448(%rsp)
	movapd	%xmm0, 432(%rsp)
	movapd	%xmm0, 416(%rsp)
	movapd	%xmm0, 400(%rsp)
	movapd	%xmm0, 384(%rsp)
	movapd	%xmm0, 368(%rsp)
	movapd	%xmm0, 352(%rsp)
	movapd	%xmm0, 336(%rsp)
	movapd	%xmm0, 320(%rsp)
	movapd	%xmm0, 304(%rsp)
	movapd	%xmm0, 288(%rsp)
	movapd	%xmm0, 272(%rsp)
	movl	$0, 512(%rsp)
	movl	$2, %ebp
	.p2align	4, 0x90
.LBB2_3:                                # =>This Inner Loop Header: Depth=1
	movsd	-16(%r14,%rbp,8), %xmm1 # xmm1 = mem[0],zero
	andpd	.LCPI2_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_5
# BB#4:                                 # %call.sqrt
                                        #   in Loop: Header=BB2_3 Depth=1
	movapd	%xmm1, %xmm0
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	32(%rsp), %xmm1         # 16-byte Reload
.LBB2_5:                                # %.split
                                        #   in Loop: Header=BB2_3 Depth=1
	mulsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB2_7
# BB#6:                                 # %call.sqrt175
                                        #   in Loop: Header=BB2_3 Depth=1
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB2_7:                                # %.split.split
                                        #   in Loop: Header=BB2_3 Depth=1
	movsd	%xmm1, 4160(%rsp,%rbp,8)
	movsd	-8(%r14,%rbp,8), %xmm1  # xmm1 = mem[0],zero
	andpd	.LCPI2_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_9
# BB#8:                                 # %call.sqrt176
                                        #   in Loop: Header=BB2_3 Depth=1
	movapd	%xmm1, %xmm0
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	32(%rsp), %xmm1         # 16-byte Reload
.LBB2_9:                                # %.split.split.split
                                        #   in Loop: Header=BB2_3 Depth=1
	mulsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB2_11
# BB#10:                                # %call.sqrt177
                                        #   in Loop: Header=BB2_3 Depth=1
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB2_11:                               # %.split.split.split.split
                                        #   in Loop: Header=BB2_3 Depth=1
	movsd	%xmm1, 4168(%rsp,%rbp,8)
	movsd	(%r14,%rbp,8), %xmm1    # xmm1 = mem[0],zero
	andpd	.LCPI2_0(%rip), %xmm1
	xorps	%xmm0, %xmm0
	sqrtsd	%xmm1, %xmm0
	ucomisd	%xmm0, %xmm0
	jnp	.LBB2_13
# BB#12:                                # %call.sqrt178
                                        #   in Loop: Header=BB2_3 Depth=1
	movapd	%xmm1, %xmm0
	movapd	%xmm1, 32(%rsp)         # 16-byte Spill
	callq	sqrt
	movapd	32(%rsp), %xmm1         # 16-byte Reload
.LBB2_13:                               # %.split.split.split.split.split
                                        #   in Loop: Header=BB2_3 Depth=1
	mulsd	%xmm1, %xmm0
	xorps	%xmm1, %xmm1
	sqrtsd	%xmm0, %xmm1
	ucomisd	%xmm1, %xmm1
	jnp	.LBB2_15
# BB#14:                                # %call.sqrt179
                                        #   in Loop: Header=BB2_3 Depth=1
	callq	sqrt
	movapd	%xmm0, %xmm1
.LBB2_15:                               # %.split.split.split.split.split.split
                                        #   in Loop: Header=BB2_3 Depth=1
	movsd	%xmm1, 4176(%rsp,%rbp,8)
	addq	$3, %rbp
	cmpq	$578, %rbp              # imm = 0x242
	jne	.LBB2_3
	jmp	.LBB2_16
.LBB2_37:
	movq	16(%rsp), %rcx          # 8-byte Reload
	movq	256(%rsp), %rax
	movq	%rax, 112(%rbx)
	movaps	240(%rsp), %xmm0
	movups	%xmm0, 96(%rbx)
	movaps	224(%rsp), %xmm0
	movups	%xmm0, 80(%rbx)
	movaps	208(%rsp), %xmm0
	movups	%xmm0, 64(%rbx)
	movaps	144(%rsp), %xmm0
	movaps	160(%rsp), %xmm1
	movaps	176(%rsp), %xmm2
	movaps	192(%rsp), %xmm3
	movups	%xmm3, 48(%rbx)
	movups	%xmm2, 32(%rbx)
	movups	%xmm1, 16(%rbx)
	movups	%xmm0, (%rbx)
	movl	76(%rbx), %eax
	addl	%eax, (%rbx)
	movl	12(%rsp), %eax          # 4-byte Reload
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	%xmm0, (%rcx)
	movsd	56(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 8(%rcx)
	movsd	64(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 16(%rcx)
	movsd	72(%rsp), %xmm0         # 8-byte Reload
                                        # xmm0 = mem[0],zero
	movsd	%xmm0, 24(%rcx)
	addq	$8792, %rsp             # imm = 0x2258
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end2:
	.size	outer_loop, .Lfunc_end2-outer_loop
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI3_0:
	.quad	4657441095305658368     # double 2375
.LCPI3_4:
	.quad	4621819117588971520     # double 10
	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI3_1:
	.long	3212836864              # float -1
.LCPI3_2:
	.long	1082130432              # float 4
.LCPI3_3:
	.long	1092616192              # float 10
	.text
	.globl	set_masking_lower
	.p2align	4, 0x90
	.type	set_masking_lower,@function
set_masking_lower:                      # @set_masking_lower
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi38:
	.cfi_def_cfa_offset 16
                                        # kill: %EDI<def> %EDI<kill> %RDI<def>
	leal	-6(%rdi,%rdi), %eax
	cvtsi2ssl	%eax, %xmm0
	addl	$-125, %esi
	cvtsi2sdl	%esi, %xmm1
	divsd	.LCPI3_0(%rip), %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addss	.LCPI3_1(%rip), %xmm1
	mulss	.LCPI3_2(%rip), %xmm1
	addss	%xmm0, %xmm1
	divss	.LCPI3_3(%rip), %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI3_4(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, masking_lower(%rip)
	popq	%rax
	retq
.Lfunc_end3:
	.size	set_masking_lower, .Lfunc_end3-set_masking_lower
	.cfi_endproc

	.section	.rodata.cst4,"aM",@progbits,4
	.p2align	2
.LCPI4_0:
	.long	1092616192              # float 10
.LCPI4_4:
	.long	3212836864              # float -1
.LCPI4_5:
	.long	1082130432              # float 4
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI4_1:
	.quad	4621819117588971520     # double 10
.LCPI4_2:
	.quad	4652552666608566272     # double 1100
.LCPI4_3:
	.quad	4657441095305658368     # double 2375
.LCPI4_6:
	.quad	4602678819172646912     # double 0.5
.LCPI4_7:
	.quad	4599616371426034975     # double 0.33000000000000002
.LCPI4_8:
	.quad	4607182418800017408     # double 1
.LCPI4_9:
	.quad	0                       # double 0
	.text
	.globl	VBR_iteration_loop
	.p2align	4, 0x90
	.type	VBR_iteration_loop,@function
VBR_iteration_loop:                     # @VBR_iteration_loop
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi39:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi40:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi41:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi42:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi43:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi44:
	.cfi_def_cfa_offset 56
	subq	$3624, %rsp             # imm = 0xE28
.Lcfi45:
	.cfi_def_cfa_offset 3680
.Lcfi46:
	.cfi_offset %rbx, -56
.Lcfi47:
	.cfi_offset %r12, -48
.Lcfi48:
	.cfi_offset %r13, -40
.Lcfi49:
	.cfi_offset %r14, -32
.Lcfi50:
	.cfi_offset %r15, -24
.Lcfi51:
	.cfi_offset %rbp, -16
	movq	%r9, %r15
	movq	%r8, 192(%rsp)          # 8-byte Spill
	movq	%rcx, 96(%rsp)          # 8-byte Spill
	movq	%rdx, 240(%rsp)         # 8-byte Spill
	movq	%rsi, 248(%rsp)         # 8-byte Spill
	movq	%rdi, %r13
	movq	3680(%rsp), %rdx
	movq	%r15, %rsi
	movq	%rdx, 184(%rsp)         # 8-byte Spill
	callq	iteration_init
	movl	$1, 220(%r13)
	movl	212(%r13), %eax
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	testl	%eax, %eax
	movl	$0, %r12d
	jle	.LBB4_5
# BB#1:                                 # %.lr.ph431
	xorl	%r12d, %r12d
	leaq	108(%rsp), %r14
	leaq	44(%rsp), %rbp
	.p2align	4, 0x90
.LBB4_2:                                # =>This Inner Loop Header: Depth=1
	movq	%r13, %rdi
	movq	%r14, %rsi
	movq	%rbp, %rdx
	callq	getframebits
	movl	220(%r13), %eax
	movl	44(%rsp), %ebx
	cmpl	208(%r13), %eax
	jne	.LBB4_4
# BB#3:                                 #   in Loop: Header=BB4_2 Depth=1
	movl	%ebx, %eax
	cltd
	idivl	204(%r13)
	movl	%eax, %r12d
.LBB4_4:                                # %._crit_edge472
                                        #   in Loop: Header=BB4_2 Depth=1
	movl	108(%rsp), %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	movl	%ebx, %edx
	callq	ResvFrameBegin
	movslq	220(%r13), %rcx
	movl	%eax, 512(%rsp,%rcx,4)
	leal	1(%rcx), %eax
	movl	%eax, 220(%r13)
	movl	212(%r13), %eax
	cmpl	%eax, %ecx
	jl	.LBB4_2
.LBB4_5:                                # %._crit_edge432
	movl	%eax, 220(%r13)
	movl	200(%r13), %eax
	testl	%eax, %eax
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movq	%r15, 112(%rsp)         # 8-byte Spill
	movq	%r13, 56(%rsp)          # 8-byte Spill
	jle	.LBB4_31
# BB#6:                                 # %.lr.ph424
	cmpl	$124, %r12d
	movl	$125, %eax
	cmovlel	%eax, %r12d
	movl	%r12d, 40(%rsp)         # 4-byte Spill
	xorl	%ebp, %ebp
	movl	$0, 72(%rsp)            # 4-byte Folded Spill
	movl	$0, 64(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB4_7:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_11 Depth 2
                                        #       Child Loop BB4_17 Depth 3
	cmpl	$0, reduce_sidechannel(%rip)
	movl	204(%r13), %ebx
	movl	$1, %eax
	cmovnel	%eax, %ebx
	cmpl	$0, convert_mdct(%rip)
	je	.LBB4_9
# BB#8:                                 #   in Loop: Header=BB4_7 Depth=1
	leaq	(%rbp,%rbp,8), %rdi
	shlq	$10, %rdi
	addq	96(%rsp), %rdi          # 8-byte Folded Reload
	movq	%rdi, %rsi
	callq	ms_convert
.LBB4_9:                                # %.preheader380
                                        #   in Loop: Header=BB4_7 Depth=1
	testl	%ebx, %ebx
	jle	.LBB4_30
# BB#10:                                # %.lr.ph416.preheader
                                        #   in Loop: Header=BB4_7 Depth=1
	movl	%ebx, %eax
	movq	%rax, 144(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rbp, 80(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB4_11:                               # %.lr.ph416
                                        #   Parent Loop BB4_7 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_17 Depth 3
	imulq	$240, %rbp, %rbx
	addq	%r15, %rbx
	imulq	$120, %rdx, %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	leaq	48(%rax,%rbx), %r12
	leaq	(%rbp,%rbp,8), %r13
	movq	%r13, %rax
	shlq	$10, %rax
	addq	96(%rsp), %rax          # 8-byte Folded Reload
	movq	%rdx, 88(%rsp)          # 8-byte Spill
	leaq	(%rdx,%rdx,8), %r14
	movq	%r14, %r15
	shlq	$9, %r15
	addq	%rax, %r15
	movq	56(%rsp), %rbp          # 8-byte Reload
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%r12, %rdx
	callq	init_outer_loop
	testl	%eax, %eax
	je	.LBB4_14
# BB#12:                                #   in Loop: Header=BB4_11 Depth=2
	movq	%r13, 32(%rsp)          # 8-byte Spill
	movq	112(%r12), %rax
	movq	%rax, 368(%rsp)
	movups	96(%r12), %xmm0
	movaps	%xmm0, 352(%rsp)
	movups	80(%r12), %xmm0
	movaps	%xmm0, 336(%rsp)
	movups	64(%r12), %xmm0
	movaps	%xmm0, 320(%rsp)
	movups	(%r12), %xmm0
	movups	16(%r12), %xmm1
	movupd	32(%r12), %xmm2
	movupd	48(%r12), %xmm3
	movapd	%xmm3, 304(%rsp)
	movapd	%xmm2, 288(%rsp)
	movaps	%xmm1, 272(%rsp)
	movaps	%xmm0, 256(%rsp)
	movl	92(%rbp), %eax
	leal	-6(%rax,%rax), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	divss	.LCPI4_0(%rip), %xmm0
	xorps	%xmm1, %xmm1
	cvtss2sd	%xmm0, %xmm1
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, masking_lower(%rip)
	imulq	$1952, 80(%rsp), %rax   # 8-byte Folded Reload
                                        # imm = 0x7A0
	addq	192(%rsp), %rax         # 8-byte Folded Reload
	imulq	$976, 88(%rsp), %rdx    # 8-byte Folded Reload
                                        # imm = 0x3D0
	addq	%rax, %rdx
	movq	%rbp, %rdi
	movq	%r15, %rsi
	movq	%rdx, 120(%rsp)         # 8-byte Spill
	movq	%r12, %rcx
	leaq	576(%rsp), %r8
	callq	calc_xmin
	testl	%eax, %eax
	movl	40(%rsp), %ecx          # 4-byte Reload
	movl	$125, %eax
	cmovel	%eax, %ecx
	movl	72(%rsp), %eax          # 4-byte Reload
	movl	$1, %edx
	cmovel	%edx, %eax
	movq	48(%rsp), %rdx          # 8-byte Reload
	cmpl	$2, 72(%rdx,%rbx)
	movl	%eax, 72(%rsp)          # 4-byte Spill
	movq	%r15, 200(%rsp)         # 8-byte Spill
	jne	.LBB4_15
# BB#13:                                #   in Loop: Header=BB4_11 Depth=2
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	%rsi, %rax
	shlq	$4, %rax
	addq	248(%rsp), %rax         # 8-byte Folded Reload
	movsd	.LCPI4_2(%rip), %xmm0   # xmm0 = mem[0],zero
	movq	88(%rsp), %rdx          # 8-byte Reload
	maxsd	(%rax,%rdx,8), %xmm0
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%ecx, %xmm1
	addsd	%xmm0, %xmm1
	cvttsd2si	%xmm1, %ecx
	cmpl	$1801, %ecx             # imm = 0x709
	movl	$1800, %eax             # imm = 0x708
	cmovgel	%eax, %ecx
	movq	%rdx, %r8
	movq	%rsi, %r9
	jmp	.LBB4_16
	.p2align	4, 0x90
.LBB4_14:                               #   in Loop: Header=BB4_11 Depth=2
	movq	80(%rsp), %rbp          # 8-byte Reload
	imulq	$488, %rbp, %rax        # imm = 0x1E8
	addq	3688(%rsp), %rax
	movq	88(%rsp), %rbx          # 8-byte Reload
	imulq	$244, %rbx, %rcx
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 224(%rcx,%rax)
	movupd	%xmm0, 208(%rcx,%rax)
	movupd	%xmm0, 192(%rcx,%rax)
	movupd	%xmm0, 176(%rcx,%rax)
	movupd	%xmm0, 160(%rcx,%rax)
	movupd	%xmm0, 144(%rcx,%rax)
	movupd	%xmm0, 128(%rcx,%rax)
	movupd	%xmm0, 112(%rcx,%rax)
	movupd	%xmm0, 96(%rcx,%rax)
	movupd	%xmm0, 80(%rcx,%rax)
	movupd	%xmm0, 64(%rcx,%rax)
	movupd	%xmm0, 48(%rcx,%rax)
	movupd	%xmm0, 32(%rcx,%rax)
	movupd	%xmm0, 16(%rcx,%rax)
	movupd	%xmm0, (%rcx,%rax)
	movl	$0, 240(%rcx,%rax)
	shlq	$9, %r13
	addq	3680(%rsp), %r13
	shlq	$8, %r14
	addq	%r13, %r14
	xorl	%esi, %esi
	movl	$2304, %edx             # imm = 0x900
	movq	%r14, %rdi
	callq	memset
	leaq	160(%rsp,%rbp,8), %rax
	movl	$0, (%rax,%rbx,4)
	movl	$1, 72(%rsp)            # 4-byte Folded Spill
	movq	%rbx, %rdx
	movq	112(%rsp), %r15         # 8-byte Reload
	jmp	.LBB4_29
	.p2align	4, 0x90
.LBB4_15:                               #   in Loop: Header=BB4_11 Depth=2
	movq	88(%rsp), %r8           # 8-byte Reload
	movq	80(%rsp), %r9           # 8-byte Reload
.LBB4_16:                               #   in Loop: Header=BB4_11 Depth=2
	movq	32(%rsp), %rdi          # 8-byte Reload
	movslq	212(%rbp), %rax
	movl	512(%rsp,%rax,4), %eax
	movl	200(%rbp), %esi
	imull	204(%rbp), %esi
	cltd
	idivl	%esi
	movl	%eax, %edx
	addl	$1200, %edx             # imm = 0x4B0
	cmpl	$2501, %edx             # imm = 0x9C5
	movl	$2500, %eax             # imm = 0x9C4
	cmovgel	%eax, %edx
	cmpl	%ecx, %edx
	cmovll	%ecx, %edx
	movl	%edx, %eax
	subl	%ecx, %eax
	movl	%eax, %r15d
	sarl	$31, %r15d
	shrl	$30, %r15d
	addl	%eax, %r15d
	sarl	$2, %r15d
	addl	%edx, %ecx
	movl	%ecx, %ebp
	shrl	$31, %ebp
	addl	%ecx, %ebp
	sarl	%ebp
	movq	%rdx, 152(%rsp)         # 8-byte Spill
	leal	1(%rdx), %ebx
	shlq	$9, %rdi
	addq	3680(%rsp), %rdi
	shlq	$8, %r14
	addq	%rdi, %r14
	imulq	$488, %r9, %rax         # imm = 0x1E8
	addq	3688(%rsp), %rax
	imulq	$244, %r8, %rcx
	addq	%rax, %rcx
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%r12, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_17:                               #   Parent Loop BB4_7 Depth=1
                                        #     Parent Loop BB4_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	cmpl	%ebx, %ebp
	jge	.LBB4_23
# BB#18:                                #   in Loop: Header=BB4_17 Depth=3
	movl	%ebx, 128(%rsp)         # 4-byte Spill
	movq	368(%rsp), %rax
	movq	%rax, 112(%r12)
	movaps	352(%rsp), %xmm0
	movups	%xmm0, 96(%r12)
	movaps	336(%rsp), %xmm0
	movups	%xmm0, 80(%r12)
	movaps	320(%rsp), %xmm0
	movups	%xmm0, 64(%r12)
	movaps	256(%rsp), %xmm0
	movaps	272(%rsp), %xmm1
	movapd	288(%rsp), %xmm2
	movapd	304(%rsp), %xmm3
	movupd	%xmm3, 48(%r12)
	movupd	%xmm2, 32(%r12)
	movups	%xmm1, 16(%r12)
	movups	%xmm0, (%r12)
	movq	56(%rsp), %rbx          # 8-byte Reload
	movl	92(%rbx), %eax
	leal	-6(%rax,%rax), %eax
	xorps	%xmm0, %xmm0
	cvtsi2ssl	%eax, %xmm0
	leal	-125(%rbp), %eax
	xorps	%xmm1, %xmm1
	cvtsi2sdl	%eax, %xmm1
	divsd	.LCPI4_3(%rip), %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addss	.LCPI4_4(%rip), %xmm1
	mulss	.LCPI4_5(%rip), %xmm1
	addss	%xmm0, %xmm1
	divss	.LCPI4_0(%rip), %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, masking_lower(%rip)
	movq	%rbx, %rdi
	movq	%rbp, 48(%rsp)          # 8-byte Spill
	movq	200(%rsp), %r13         # 8-byte Reload
	movq	%r13, %rsi
	movq	120(%rsp), %rdx         # 8-byte Reload
	movq	%r12, %rcx
	leaq	576(%rsp), %rbp
	movq	%rbp, %r8
	callq	calc_xmin
	movq	88(%rsp), %rax          # 8-byte Reload
	movl	%eax, 24(%rsp)
	movq	%r12, 8(%rsp)
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	%rax, (%rsp)
	movq	%rbx, %rdi
	movq	%r13, %rsi
	movq	48(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, %edx
	leaq	208(%rsp), %rcx
	leaq	576(%rsp), %r8
	movq	%r14, %r9
	callq	outer_loop
	xorpd	%xmm0, %xmm0
	ucomisd	216(%rsp), %xmm0
	jb	.LBB4_24
# BB#19:                                #   in Loop: Header=BB4_17 Depth=3
	xorpd	%xmm0, %xmm0
	ucomisd	232(%rsp), %xmm0
	jb	.LBB4_24
# BB#20:                                #   in Loop: Header=BB4_17 Depth=3
	cvttsd2si	208(%rsp), %eax
	testl	%eax, %eax
	jg	.LBB4_24
# BB#21:                                #   in Loop: Header=BB4_17 Depth=3
	xorpd	%xmm0, %xmm0
	ucomisd	224(%rsp), %xmm0
	jb	.LBB4_24
# BB#22:                                #   in Loop: Header=BB4_17 Depth=3
	movq	136(%rsp), %r12         # 8-byte Reload
	movl	(%r12), %ebx
	movl	$244, %edx
	leaq	1064(%rsp), %rdi
	movq	32(%rsp), %rsi          # 8-byte Reload
	callq	memcpy
	movl	$2304, %edx             # imm = 0x900
	leaq	1312(%rsp), %rdi
	movq	%r14, %rsi
	callq	memcpy
	movq	112(%r12), %rax
	movq	%rax, 496(%rsp)
	movups	96(%r12), %xmm0
	movaps	%xmm0, 480(%rsp)
	movups	80(%r12), %xmm0
	movaps	%xmm0, 464(%rsp)
	movups	64(%r12), %xmm0
	movaps	%xmm0, 448(%rsp)
	movupd	(%r12), %xmm0
	movupd	16(%r12), %xmm1
	movupd	32(%r12), %xmm2
	movupd	48(%r12), %xmm3
	movapd	%xmm3, 432(%rsp)
	movapd	%xmm2, 416(%rsp)
	movapd	%xmm1, 400(%rsp)
	movapd	%xmm0, 384(%rsp)
.LBB4_23:                               #   in Loop: Header=BB4_17 Depth=3
	subl	%r15d, %ebp
	jmp	.LBB4_25
	.p2align	4, 0x90
.LBB4_24:                               #   in Loop: Header=BB4_17 Depth=3
	addl	%r15d, %ebp
	movq	136(%rsp), %r12         # 8-byte Reload
	movl	128(%rsp), %ebx         # 4-byte Reload
.LBB4_25:                               #   in Loop: Header=BB4_17 Depth=3
	movl	%r15d, %eax
	shrl	$31, %eax
	addl	%r15d, %eax
	sarl	%eax
	cmpl	$21, %r15d
	movl	%eax, %r15d
	jg	.LBB4_17
# BB#26:                                #   in Loop: Header=BB4_11 Depth=2
	cmpl	152(%rsp), %ebx         # 4-byte Folded Reload
	movq	112(%rsp), %r15         # 8-byte Reload
	jg	.LBB4_28
# BB#27:                                #   in Loop: Header=BB4_11 Depth=2
	movq	496(%rsp), %rax
	movq	%rax, 112(%r12)
	movaps	480(%rsp), %xmm0
	movups	%xmm0, 96(%r12)
	movaps	464(%rsp), %xmm0
	movups	%xmm0, 80(%r12)
	movaps	448(%rsp), %xmm0
	movups	%xmm0, 64(%r12)
	movapd	384(%rsp), %xmm0
	movapd	400(%rsp), %xmm1
	movapd	416(%rsp), %xmm2
	movapd	432(%rsp), %xmm3
	movupd	%xmm3, 48(%r12)
	movupd	%xmm2, 32(%r12)
	movupd	%xmm1, 16(%r12)
	movupd	%xmm0, (%r12)
	movl	$244, %edx
	movq	32(%rsp), %rdi          # 8-byte Reload
	leaq	1064(%rsp), %rsi
	callq	memcpy
	movl	$2304, %edx             # imm = 0x900
	movq	%r14, %rdi
	leaq	1312(%rsp), %rsi
	callq	memcpy
.LBB4_28:                               #   in Loop: Header=BB4_11 Depth=2
	movl	(%r12), %eax
	movq	80(%rsp), %rbp          # 8-byte Reload
	leaq	160(%rsp,%rbp,8), %rcx
	movq	88(%rsp), %rdx          # 8-byte Reload
	movl	%eax, (%rcx,%rdx,4)
	addl	%eax, 64(%rsp)          # 4-byte Folded Spill
.LBB4_29:                               #   in Loop: Header=BB4_11 Depth=2
	incq	%rdx
	cmpq	144(%rsp), %rdx         # 8-byte Folded Reload
	jne	.LBB4_11
.LBB4_30:                               # %._crit_edge417
                                        #   in Loop: Header=BB4_7 Depth=1
	incq	%rbp
	movq	56(%rsp), %r13          # 8-byte Reload
	movslq	200(%r13), %rax
	cmpq	%rax, %rbp
	jl	.LBB4_7
.LBB4_31:                               # %._crit_edge425
	cmpl	$0, reduce_sidechannel(%rip)
	je	.LBB4_36
# BB#32:                                # %._crit_edge425
	testl	%eax, %eax
	movq	240(%rsp), %rdi         # 8-byte Reload
	jle	.LBB4_36
# BB#33:                                # %.lr.ph410
	cltq
	xorl	%ecx, %ecx
	movsd	.LCPI4_6(%rip), %xmm0   # xmm0 = mem[0],zero
	movsd	.LCPI4_7(%rip), %xmm1   # xmm1 = mem[0],zero
	movsd	.LCPI4_8(%rip), %xmm2   # xmm2 = mem[0],zero
	movl	$125, %edx
	movl	64(%rsp), %r14d         # 4-byte Reload
	.p2align	4, 0x90
.LBB4_34:                               # =>This Inner Loop Header: Depth=1
	movapd	%xmm0, %xmm3
	subsd	(%rdi,%rcx,8), %xmm3
	mulsd	%xmm1, %xmm3
	addsd	%xmm3, %xmm3
	movapd	%xmm2, %xmm4
	subsd	%xmm3, %xmm4
	addsd	%xmm2, %xmm3
	divsd	%xmm3, %xmm4
	xorps	%xmm3, %xmm3
	cvtsi2sdl	160(%rsp,%rcx,8), %xmm3
	mulsd	%xmm4, %xmm3
	cvttsd2si	%xmm3, %esi
	cmpl	$124, %esi
	cmovlel	%edx, %esi
	movl	%esi, 164(%rsp,%rcx,8)
	addl	%esi, %r14d
	incq	%rcx
	cmpq	%rax, %rcx
	jl	.LBB4_34
	jmp	.LBB4_37
.LBB4_36:
	movl	64(%rsp), %r14d         # 4-byte Reload
.LBB4_37:                               # %.loopexit379
	movl	$1, %eax
	cmpl	$0, 72(%rsp)            # 4-byte Folded Reload
	jne	.LBB4_39
# BB#38:
	movl	208(%r13), %eax
.LBB4_39:                               # %.preheader377
	movl	%eax, 220(%r13)
	movslq	212(%r13), %rcx
	cmpl	%ecx, %eax
	jge	.LBB4_43
# BB#40:                                # %.lr.ph405.preheader
	movslq	%eax, %rdx
	incl	%eax
	.p2align	4, 0x90
.LBB4_41:                               # %.lr.ph405
                                        # =>This Inner Loop Header: Depth=1
	cmpl	512(%rsp,%rdx,4), %r14d
	jle	.LBB4_43
# BB#42:                                #   in Loop: Header=BB4_41 Depth=1
	incq	%rdx
	movl	%eax, 220(%r13)
	incl	%eax
	cmpq	%rcx, %rdx
	jl	.LBB4_41
.LBB4_43:                               # %._crit_edge406
	leaq	108(%rsp), %rsi
	leaq	44(%rsp), %rdx
	movq	%r13, %rdi
	callq	getframebits
	movl	44(%rsp), %edx
	movl	108(%rsp), %ecx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	ResvFrameBegin
	movslq	200(%r13), %r9
	movl	$0, 40(%rsp)            # 4-byte Folded Spill
	movl	%r14d, %ecx
	cmpl	%eax, %ecx
	jle	.LBB4_63
# BB#44:                                # %.preheader376
	testl	%r9d, %r9d
	jle	.LBB4_104
# BB#45:                                # %.preheader375.lr.ph
	movslq	204(%r13), %r12
	testq	%r12, %r12
	jle	.LBB4_55
# BB#46:                                # %.preheader375.us.preheader
	movslq	220(%r13), %rax
	movl	512(%rsp,%rax,4), %edi
	movl	%r12d, %r8d
	andl	$1, %r8d
	leaq	160(%rsp), %r10
	xorl	%r11d, %r11d
	.p2align	4, 0x90
.LBB4_47:                               # %.preheader375.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_52 Depth 2
	testq	%r8, %r8
	jne	.LBB4_49
# BB#48:                                #   in Loop: Header=BB4_47 Depth=1
	xorl	%ecx, %ecx
	jmp	.LBB4_50
	.p2align	4, 0x90
.LBB4_49:                               #   in Loop: Header=BB4_47 Depth=1
	movl	160(%rsp,%r11,8), %eax
	imull	%edi, %eax
	cltd
	idivl	%r14d
	movl	%eax, 160(%rsp,%r11,8)
	movl	$1, %ecx
.LBB4_50:                               # %.prol.loopexit513
                                        #   in Loop: Header=BB4_47 Depth=1
	cmpl	$1, %r12d
	movl	%r14d, %esi
	je	.LBB4_53
# BB#51:                                # %.preheader375.us.new
                                        #   in Loop: Header=BB4_47 Depth=1
	leaq	(%r10,%rcx,4), %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_52:                               #   Parent Loop BB4_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movl	(%rbx,%rbp,4), %eax
	imull	%edi, %eax
	cltd
	idivl	%esi
	movl	%eax, (%rbx,%rbp,4)
	movl	4(%rbx,%rbp,4), %eax
	imull	%edi, %eax
	cltd
	idivl	%esi
	movl	%eax, 4(%rbx,%rbp,4)
	leaq	2(%rcx,%rbp), %rax
	addq	$2, %rbp
	cmpq	%r12, %rax
	jl	.LBB4_52
.LBB4_53:                               # %._crit_edge402.us
                                        #   in Loop: Header=BB4_47 Depth=1
	incq	%r11
	addq	$8, %r10
	cmpq	%r9, %r11
	jl	.LBB4_47
# BB#54:                                # %.preheader374
	testl	%r9d, %r9d
	jle	.LBB4_104
.LBB4_55:                               # %.preheader373.lr.ph
	movslq	204(%r13), %rax
	testq	%rax, %rax
	jle	.LBB4_105
# BB#56:                                # %.preheader373.us.preheader
	leaq	-1(%rax), %rdx
	movl	%eax, %esi
	andl	$7, %esi
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_57:                               # %.preheader373.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_58 Depth 2
                                        #     Child Loop BB4_60 Depth 2
	xorl	%ecx, %ecx
	testq	%rsi, %rsi
	je	.LBB4_59
	.p2align	4, 0x90
.LBB4_58:                               #   Parent Loop BB4_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	incq	%rcx
	cmpq	%rcx, %rsi
	jne	.LBB4_58
.LBB4_59:                               # %.prol.loopexit
                                        #   in Loop: Header=BB4_57 Depth=1
	cmpq	$7, %rdx
	jb	.LBB4_61
	.p2align	4, 0x90
.LBB4_60:                               #   Parent Loop BB4_57 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	addq	$8, %rcx
	cmpq	%rax, %rcx
	jl	.LBB4_60
.LBB4_61:                               # %._crit_edge397.us
                                        #   in Loop: Header=BB4_57 Depth=1
	incq	%rdi
	cmpq	%r9, %rdi
	jl	.LBB4_57
# BB#62:
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
.LBB4_63:                               # %.loopexit
	testl	%r9d, %r9d
	jle	.LBB4_104
.LBB4_64:                               # %.preheader372.lr.ph
	movl	204(%r13), %eax
	leaq	48(%r15), %rdx
	leaq	160(%rsp), %rsi
	xorl	%ecx, %ecx
	movq	%rcx, 120(%rsp)         # 8-byte Spill
	movq	3680(%rsp), %rcx
	movq	%rcx, 136(%rsp)         # 8-byte Spill
	movq	96(%rsp), %rcx          # 8-byte Reload
	movq	%rcx, 80(%rsp)          # 8-byte Spill
	movq	3688(%rsp), %rcx
	movq	%rcx, 72(%rsp)          # 8-byte Spill
	movq	192(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, 152(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB4_65:                               # %.preheader372
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_68 Depth 2
                                        #     Child Loop BB4_74 Depth 2
	testl	%eax, %eax
	jle	.LBB4_82
# BB#66:                                # %.lr.ph391
                                        #   in Loop: Header=BB4_65 Depth=1
	cmpl	$0, 40(%rsp)            # 4-byte Folded Reload
	movq	%rdx, 144(%rsp)         # 8-byte Spill
	movq	%rsi, 64(%rsp)          # 8-byte Spill
	je	.LBB4_73
# BB#67:                                # %.lr.ph391.split.preheader
                                        #   in Loop: Header=BB4_65 Depth=1
	movq	%rsi, %r13
	movq	136(%rsp), %r15         # 8-byte Reload
	movq	80(%rsp), %rsi          # 8-byte Reload
	movq	72(%rsp), %r14          # 8-byte Reload
	movq	152(%rsp), %r12         # 8-byte Reload
	xorl	%ebp, %ebp
	movq	56(%rsp), %rcx          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_68:                               # %.lr.ph391.split
                                        #   Parent Loop BB4_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r15, 128(%rsp)         # 8-byte Spill
	movq	%rbp, 32(%rsp)          # 8-byte Spill
	movq	%rcx, %rbx
	movq	%rbx, %rdi
	movq	%rsi, %rbp
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	callq	init_outer_loop
	testl	%eax, %eax
	je	.LBB4_70
# BB#69:                                #   in Loop: Header=BB4_68 Depth=2
	movl	92(%rbx), %eax
	movq	%r13, 200(%rsp)         # 8-byte Spill
	movl	(%r13), %ecx
	movq	%rcx, 88(%rsp)          # 8-byte Spill
	leal	-6(%rax,%rax), %eax
	cvtsi2ssl	%eax, %xmm0
	leal	-125(%rcx), %eax
	cvtsi2sdl	%eax, %xmm1
	divsd	.LCPI4_3(%rip), %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addss	.LCPI4_4(%rip), %xmm1
	mulss	.LCPI4_5(%rip), %xmm1
	addss	%xmm0, %xmm1
	divss	.LCPI4_0(%rip), %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, masking_lower(%rip)
	movq	%rbx, %rdi
	movq	%rbp, %rsi
	movq	%r12, %rdx
	movq	48(%rsp), %r15          # 8-byte Reload
	movq	%r15, %rcx
	leaq	576(%rsp), %rax
	movq	%rax, %r8
	callq	calc_xmin
	movq	%rbx, %r13
	movq	%rbp, %rbx
	movq	32(%rsp), %rbp          # 8-byte Reload
	movl	%ebp, 24(%rsp)
	movq	%r15, 8(%rsp)
	movq	%r14, (%rsp)
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	88(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	208(%rsp), %rcx
	leaq	576(%rsp), %r8
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	%r15, %r9
	callq	outer_loop
	movq	%r13, %rcx
	movq	200(%rsp), %r13         # 8-byte Reload
	jmp	.LBB4_71
	.p2align	4, 0x90
.LBB4_70:                               #   in Loop: Header=BB4_68 Depth=2
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 224(%r14)
	movupd	%xmm0, 208(%r14)
	movupd	%xmm0, 192(%r14)
	movupd	%xmm0, 176(%r14)
	movupd	%xmm0, 160(%r14)
	movupd	%xmm0, 144(%r14)
	movupd	%xmm0, 128(%r14)
	movupd	%xmm0, 112(%r14)
	movupd	%xmm0, 96(%r14)
	movupd	%xmm0, 80(%r14)
	movupd	%xmm0, 64(%r14)
	movupd	%xmm0, 48(%r14)
	movupd	%xmm0, 32(%r14)
	movupd	%xmm0, 16(%r14)
	movupd	%xmm0, (%r14)
	movl	$0, 240(%r14)
	xorl	%esi, %esi
	movl	$2304, %edx             # imm = 0x900
	movq	128(%rsp), %r15         # 8-byte Reload
	movq	%r15, %rdi
	callq	memset
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 224(%rsp)
	movapd	%xmm0, 208(%rsp)
	movq	%rbx, %rcx
	movq	%rbp, %rbx
	movq	32(%rsp), %rbp          # 8-byte Reload
.LBB4_71:                               #   in Loop: Header=BB4_68 Depth=2
	incq	%rbp
	movslq	204(%rcx), %rax
	addq	$976, %r12              # imm = 0x3D0
	addq	$244, %r14
	movq	48(%rsp), %rdx          # 8-byte Reload
	addq	$120, %rdx
	addq	$4608, %rbx             # imm = 0x1200
	addq	$2304, %r15             # imm = 0x900
	addq	$4, %r13
	cmpq	%rax, %rbp
	movq	%rbx, %rsi
	jl	.LBB4_68
# BB#72:                                #   in Loop: Header=BB4_65 Depth=1
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	%rcx, %r13
	jmp	.LBB4_81
	.p2align	4, 0x90
.LBB4_73:                               # %.lr.ph391.split.us.preheader
                                        #   in Loop: Header=BB4_65 Depth=1
	movq	120(%rsp), %rsi         # 8-byte Reload
	imulq	$240, %rsi, %rax
	leaq	168(%r15,%rax), %r14
	leaq	(%rsi,%rsi,8), %rax
	movq	%rax, %rcx
	shlq	$10, %rcx
	movq	96(%rsp), %rdx          # 8-byte Reload
	leaq	4608(%rdx,%rcx), %rbx
	imulq	$488, %rsi, %rcx        # imm = 0x1E8
	movq	3688(%rsp), %rdx
	leaq	244(%rdx,%rcx), %rbp
	shlq	$9, %rax
	movq	3680(%rsp), %rcx
	leaq	2304(%rcx,%rax), %rax
	movq	%rax, 32(%rsp)          # 8-byte Spill
	imulq	$1952, %rsi, %rax       # imm = 0x7A0
	movq	192(%rsp), %rcx         # 8-byte Reload
	leaq	976(%rcx,%rax), %rax
	movq	%rax, 128(%rsp)         # 8-byte Spill
	xorl	%r13d, %r13d
	.p2align	4, 0x90
.LBB4_74:                               # %.lr.ph391.split.us
                                        #   Parent Loop BB4_65 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpq	$1, %r13
	jne	.LBB4_79
# BB#75:                                # %.lr.ph391.split.us
                                        #   in Loop: Header=BB4_74 Depth=2
	movl	reduce_sidechannel(%rip), %eax
	testl	%eax, %eax
	je	.LBB4_79
# BB#76:                                #   in Loop: Header=BB4_74 Depth=2
	movq	56(%rsp), %r12          # 8-byte Reload
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	%r14, %rdx
	callq	init_outer_loop
	testl	%eax, %eax
	je	.LBB4_78
# BB#77:                                #   in Loop: Header=BB4_74 Depth=2
	movl	92(%r12), %eax
	movq	120(%rsp), %rcx         # 8-byte Reload
	movl	164(%rsp,%rcx,8), %ecx
	movq	%rcx, 48(%rsp)          # 8-byte Spill
	leal	-6(%rax,%rax), %eax
	cvtsi2ssl	%eax, %xmm0
	leal	-125(%rcx), %eax
	cvtsi2sdl	%eax, %xmm1
	divsd	.LCPI4_3(%rip), %xmm1
	cvtsd2ss	%xmm1, %xmm1
	addss	.LCPI4_4(%rip), %xmm1
	mulss	.LCPI4_5(%rip), %xmm1
	addss	%xmm0, %xmm1
	divss	.LCPI4_0(%rip), %xmm1
	cvtss2sd	%xmm1, %xmm1
	movsd	.LCPI4_1(%rip), %xmm0   # xmm0 = mem[0],zero
	callq	pow
	cvtsd2ss	%xmm0, %xmm0
	movss	%xmm0, masking_lower(%rip)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	128(%rsp), %rdx         # 8-byte Reload
	movq	%r14, %rcx
	movq	%r14, %r15
	leaq	576(%rsp), %r14
	movq	%r14, %r8
	callq	calc_xmin
	movq	%r15, 8(%rsp)
	movq	%rbp, (%rsp)
	movl	$1, 24(%rsp)
	movq	%r12, %rdi
	movq	%rbx, %rsi
	movq	48(%rsp), %rdx          # 8-byte Reload
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill>
	leaq	208(%rsp), %rcx
	movq	%r14, %r8
	movq	%r15, %r14
	movq	32(%rsp), %r9           # 8-byte Reload
	callq	outer_loop
	jmp	.LBB4_79
.LBB4_78:                               #   in Loop: Header=BB4_74 Depth=2
	xorpd	%xmm0, %xmm0
	movupd	%xmm0, 224(%rbp)
	movupd	%xmm0, 208(%rbp)
	movupd	%xmm0, 192(%rbp)
	movupd	%xmm0, 176(%rbp)
	movupd	%xmm0, 160(%rbp)
	movupd	%xmm0, 144(%rbp)
	movupd	%xmm0, 128(%rbp)
	movupd	%xmm0, 112(%rbp)
	movupd	%xmm0, 96(%rbp)
	movupd	%xmm0, 80(%rbp)
	movupd	%xmm0, 64(%rbp)
	movupd	%xmm0, 48(%rbp)
	movupd	%xmm0, 32(%rbp)
	movupd	%xmm0, 16(%rbp)
	movupd	%xmm0, (%rbp)
	movl	$0, 240(%rbp)
	xorl	%esi, %esi
	movl	$2304, %edx             # imm = 0x900
	movq	32(%rsp), %rdi          # 8-byte Reload
	callq	memset
	xorpd	%xmm0, %xmm0
	movapd	%xmm0, 224(%rsp)
	movapd	%xmm0, 208(%rsp)
	.p2align	4, 0x90
.LBB4_79:                               #   in Loop: Header=BB4_74 Depth=2
	incq	%r13
	movq	56(%rsp), %rax          # 8-byte Reload
	movslq	204(%rax), %rax
	cmpq	%rax, %r13
	jl	.LBB4_74
# BB#80:                                #   in Loop: Header=BB4_65 Depth=1
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
.LBB4_81:                               # %._crit_edge392
                                        #   in Loop: Header=BB4_65 Depth=1
	movq	144(%rsp), %rdx         # 8-byte Reload
	movq	64(%rsp), %rsi          # 8-byte Reload
.LBB4_82:                               # %._crit_edge392
                                        #   in Loop: Header=BB4_65 Depth=1
	movq	120(%rsp), %rcx         # 8-byte Reload
	movq	%rcx, %rbp
	incq	%rbp
	movslq	200(%r13), %rcx
	addq	$1952, 152(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x7A0
	addq	$488, 72(%rsp)          # 8-byte Folded Spill
                                        # imm = 0x1E8
	addq	$240, %rdx
	addq	$9216, 80(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x2400
	addq	$4608, 136(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x1200
	addq	$8, %rsi
	movq	%rbp, %rdi
	movq	%rdi, 120(%rsp)         # 8-byte Spill
	cmpq	%rcx, %rbp
	jl	.LBB4_65
# BB#83:                                # %.preheader371
	testl	%ecx, %ecx
	jle	.LBB4_104
# BB#84:                                # %.preheader370.lr.ph
	movl	204(%r13), %eax
	leaq	48(%r15), %rbx
	xorl	%r14d, %r14d
	movq	3680(%rsp), %r15
	movq	112(%rsp), %r12         # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	.p2align	4, 0x90
.LBB4_85:                               # %.preheader370
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_87 Depth 2
	testl	%eax, %eax
	jle	.LBB4_91
# BB#86:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_85 Depth=1
	movq	%r15, 32(%rsp)          # 8-byte Spill
	movq	%rbx, 48(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB4_87:                               # %.lr.ph
                                        #   Parent Loop BB4_85 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r13, %rdi
	movl	%r14d, %esi
	movl	%ebp, %edx
	movq	3680(%rsp), %rcx
	movq	%r12, %r8
	movq	3688(%rsp), %r9
	callq	best_scalefac_store
	cmpl	$0, 24(%rbx)
	jne	.LBB4_89
# BB#88:                                #   in Loop: Header=BB4_87 Depth=2
	movl	%r14d, %edi
	movl	%ebp, %esi
	movq	%rbx, %rdx
	movq	%r15, %rcx
	callq	best_huffman_divide
.LBB4_89:                               #   in Loop: Header=BB4_87 Depth=2
	movl	44(%rsp), %ecx
	movq	%r13, %rdi
	movq	%rbx, %rsi
	movq	%r12, %rdx
	callq	ResvAdjust
	incq	%rbp
	movslq	204(%r13), %rax
	addq	$120, %rbx
	addq	$2304, %r15             # imm = 0x900
	cmpq	%rax, %rbp
	jl	.LBB4_87
# BB#90:                                # %._crit_edge388.loopexit
                                        #   in Loop: Header=BB4_85 Depth=1
	movl	200(%r13), %ecx
	movq	48(%rsp), %rbx          # 8-byte Reload
	movq	32(%rsp), %r15          # 8-byte Reload
.LBB4_91:                               # %._crit_edge388
                                        #   in Loop: Header=BB4_85 Depth=1
	incq	%r14
	movslq	%ecx, %rdx
	addq	$240, %rbx
	addq	$4608, %r15             # imm = 0x1200
	cmpq	%rdx, %r14
	jl	.LBB4_85
# BB#92:                                # %.preheader369
	testl	%ecx, %ecx
	movq	112(%rsp), %r15         # 8-byte Reload
	movq	56(%rsp), %r13          # 8-byte Reload
	jle	.LBB4_104
# BB#93:                                # %.preheader368.lr.ph
	movl	204(%r13), %ecx
	addq	$8, 96(%rsp)            # 8-byte Folded Spill
	addq	$4, 184(%rsp)           # 8-byte Folded Spill
	xorl	%eax, %eax
	xorpd	%xmm0, %xmm0
	.p2align	4, 0x90
.LBB4_94:                               # %.preheader368
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_96 Depth 2
                                        #       Child Loop BB4_97 Depth 3
	testl	%ecx, %ecx
	jle	.LBB4_103
# BB#95:                                # %.preheader.preheader
                                        #   in Loop: Header=BB4_94 Depth=1
	movq	184(%rsp), %rbp         # 8-byte Reload
	movq	96(%rsp), %rbx          # 8-byte Reload
	xorl	%edi, %edi
	.p2align	4, 0x90
.LBB4_96:                               # %.preheader
                                        #   Parent Loop BB4_94 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_97 Depth 3
	movl	$576, %ecx              # imm = 0x240
	movq	%rbp, %rdx
	movq	%rbx, %rsi
	.p2align	4, 0x90
.LBB4_97:                               #   Parent Loop BB4_94 Depth=1
                                        #     Parent Loop BB4_96 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	ucomisd	-8(%rsi), %xmm0
	jbe	.LBB4_99
# BB#98:                                #   in Loop: Header=BB4_97 Depth=3
	negl	-4(%rdx)
.LBB4_99:                               #   in Loop: Header=BB4_97 Depth=3
	ucomisd	(%rsi), %xmm0
	jbe	.LBB4_101
# BB#100:                               #   in Loop: Header=BB4_97 Depth=3
	negl	(%rdx)
.LBB4_101:                              #   in Loop: Header=BB4_97 Depth=3
	addq	$16, %rsi
	addq	$8, %rdx
	addq	$-2, %rcx
	jne	.LBB4_97
# BB#102:                               #   in Loop: Header=BB4_96 Depth=2
	incq	%rdi
	movslq	204(%r13), %rcx
	addq	$4608, %rbx             # imm = 0x1200
	addq	$2304, %rbp             # imm = 0x900
	cmpq	%rcx, %rdi
	jl	.LBB4_96
.LBB4_103:                              # %._crit_edge
                                        #   in Loop: Header=BB4_94 Depth=1
	incq	%rax
	movslq	200(%r13), %rdx
	addq	$9216, 96(%rsp)         # 8-byte Folded Spill
                                        # imm = 0x2400
	addq	$4608, 184(%rsp)        # 8-byte Folded Spill
                                        # imm = 0x1200
	cmpq	%rdx, %rax
	jl	.LBB4_94
.LBB4_104:                              # %._crit_edge386
	movl	44(%rsp), %edx
	movq	%r13, %rdi
	movq	%r15, %rsi
	callq	ResvFrameEnd
	addq	$3624, %rsp             # imm = 0xE28
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.LBB4_105:
	movl	$1, 40(%rsp)            # 4-byte Folded Spill
	jmp	.LBB4_64
.Lfunc_end4:
	.size	VBR_iteration_loop, .Lfunc_end4-VBR_iteration_loop
	.cfi_endproc

	.globl	VBR_compare
	.p2align	4, 0x90
	.type	VBR_compare,@function
VBR_compare:                            # @VBR_compare
	.cfi_startproc
# BB#0:
	cmpl	%edi, %esi
	setle	%al
	ucomisd	%xmm4, %xmm1
	setae	%cl
	andb	%al, %cl
	ucomisd	%xmm3, %xmm0
	setae	%al
	ucomisd	%xmm5, %xmm2
	setae	%dl
	andb	%al, %dl
	andb	%cl, %dl
	movzbl	%dl, %eax
	retq
.Lfunc_end5:
	.size	VBR_compare, .Lfunc_end5-VBR_compare
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI6_0:
	.quad	9223372036854775807     # double NaN
	.quad	9223372036854775807     # double NaN
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI6_1:
	.quad	4562254508917369340     # double 0.001
.LCPI6_2:
	.quad	4621819117588971520     # double 10
.LCPI6_3:
	.quad	0                       # double 0
	.text
	.globl	calc_noise1
	.p2align	4, 0x90
	.type	calc_noise1,@function
calc_noise1:                            # @calc_noise1
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi52:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi53:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi54:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi55:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi56:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi57:
	.cfi_def_cfa_offset 56
	subq	$88, %rsp
.Lcfi58:
	.cfi_def_cfa_offset 144
.Lcfi59:
	.cfi_offset %rbx, -56
.Lcfi60:
	.cfi_offset %r12, -48
.Lcfi61:
	.cfi_offset %r13, -40
.Lcfi62:
	.cfi_offset %r14, -32
.Lcfi63:
	.cfi_offset %r15, -24
.Lcfi64:
	.cfi_offset %rbp, -16
	movq	%r9, 48(%rsp)           # 8-byte Spill
	movq	%r8, 40(%rsp)           # 8-byte Spill
	movq	%rcx, 32(%rsp)          # 8-byte Spill
	movq	%rdx, %r13
	movq	%rdi, 16(%rsp)          # 8-byte Spill
	movq	168(%rsp), %rdx
	movq	160(%rsp), %rcx
	movq	152(%rsp), %rax
	movq	$0, (%rax)
	movq	$0, (%rcx)
	movabsq	$-4571373524106608640, %rax # imm = 0xC08F380000000000
	movq	%rax, (%rdx)
	cmpl	$0, 80(%r13)
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	je	.LBB6_1
# BB#5:                                 # %.lr.ph198
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	8(%rax), %r14
	leaq	4(%rsi), %r15
	xorl	%r12d, %r12d
	movapd	.LCPI6_0(%rip), %xmm6   # xmm6 = [nan,nan]
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	.p2align	4, 0x90
.LBB6_6:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_13 Depth 2
	movl	%r12d, %ebp
	movq	144(%rsp), %rax
	movl	(%rax,%rbp,4), %edx
	cmpl	$0, 64(%r13)
	je	.LBB6_8
# BB#7:                                 #   in Loop: Header=BB6_6 Depth=1
	addl	pretab(,%rbp,4), %edx
.LBB6_8:                                #   in Loop: Header=BB6_6 Depth=1
	movl	68(%r13), %ecx
	incl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %edx
	movslq	scalefac_band(,%rbp,4), %rcx
	leal	1(%rbp), %eax
	movq	%rax, %r12
	movslq	scalefac_band(,%rax,4), %rax
	movl	%eax, %esi
	subl	%ecx, %esi
	cvtsi2sdl	%esi, %xmm0
	xorpd	%xmm2, %xmm2
	jle	.LBB6_14
# BB#9:                                 # %.lr.ph191.preheader
                                        #   in Loop: Header=BB6_6 Depth=1
	movl	12(%r13), %esi
	subl	%edx, %esi
	movslq	%esi, %rdx
	movsd	pow20(,%rdx,8), %xmm1   # xmm1 = mem[0],zero
	movl	%eax, %esi
	subl	%ecx, %esi
	leaq	-1(%rax), %rdx
	xorpd	%xmm3, %xmm3
	testb	$1, %sil
                                        # implicit-def: %XMM2
	movq	%rcx, %rsi
	je	.LBB6_11
# BB#10:                                # %.lr.ph191.prol
                                        #   in Loop: Header=BB6_6 Depth=1
	movq	16(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rcx,8), %xmm2    # xmm2 = mem[0],zero
	andpd	.LCPI6_0(%rip), %xmm2
	movq	24(%rsp), %rsi          # 8-byte Reload
	movslq	(%rsi,%rcx,4), %rsi
	movsd	pow43(,%rsi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	.LCPI6_3, %xmm2
	leaq	1(%rcx), %rsi
	movapd	%xmm2, %xmm3
.LBB6_11:                               # %.lr.ph191.prol.loopexit
                                        #   in Loop: Header=BB6_6 Depth=1
	cmpq	%rcx, %rdx
	je	.LBB6_14
# BB#12:                                # %.lr.ph191.preheader.new
                                        #   in Loop: Header=BB6_6 Depth=1
	subq	%rsi, %rax
	leaq	(%r14,%rsi,8), %rcx
	leaq	(%r15,%rsi,4), %rdx
	.p2align	4, 0x90
.LBB6_13:                               # %.lr.ph191
                                        #   Parent Loop BB6_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-8(%rcx), %xmm4         # xmm4 = mem[0],zero
	movsd	(%rcx), %xmm2           # xmm2 = mem[0],zero
	andpd	%xmm6, %xmm4
	movslq	-4(%rdx), %rsi
	movsd	pow43(,%rsi,8), %xmm5   # xmm5 = mem[0],zero
	mulsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	andpd	%xmm6, %xmm2
	movslq	(%rdx), %rsi
	movsd	pow43(,%rsi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm4, %xmm2
	addq	$16, %rcx
	addq	$8, %rdx
	addq	$-2, %rax
	movapd	%xmm2, %xmm3
	jne	.LBB6_13
.LBB6_14:                               # %._crit_edge192
                                        #   in Loop: Header=BB6_6 Depth=1
	divsd	%xmm0, %xmm2
	movq	32(%rsp), %rax          # 8-byte Reload
	movsd	%xmm2, (%rax,%rbp,8)
	movq	48(%rsp), %rax          # 8-byte Reload
	divsd	(%rax,%rbp,8), %xmm2
	movsd	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero
	maxsd	%xmm2, %xmm0
	callq	log10
	mulsd	.LCPI6_2(%rip), %xmm0
	movq	40(%rsp), %rax          # 8-byte Reload
	movsd	%xmm0, (%rax,%rbp,8)
	ucomisd	.LCPI6_3, %xmm0
	jbe	.LBB6_16
# BB#15:                                #   in Loop: Header=BB6_6 Depth=1
	incl	12(%rsp)                # 4-byte Folded Spill
	movq	152(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
.LBB6_16:                               #   in Loop: Header=BB6_6 Depth=1
	movq	160(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
	movq	168(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
	cmpl	80(%r13), %r12d
	movapd	.LCPI6_0(%rip), %xmm6   # xmm6 = [nan,nan]
	jb	.LBB6_6
	jmp	.LBB6_2
.LBB6_1:                                # %..preheader_crit_edge
	movl	$0, 12(%rsp)            # 4-byte Folded Spill
	xorl	%r12d, %r12d
.LBB6_2:                                # %.preheader
	xorl	%r14d, %r14d
	movapd	.LCPI6_0(%rip), %xmm6   # xmm6 = [nan,nan]
	.p2align	4, 0x90
.LBB6_3:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_18 Depth 2
                                        #       Child Loop BB6_23 Depth 3
	movl	84(%r13), %ebp
	cmpq	$12, %rbp
	jae	.LBB6_4
# BB#17:                                # %.lr.ph180
                                        #   in Loop: Header=BB6_3 Depth=1
	leaq	1(%r14), %r15
	addl	$12, %r12d
	movq	%r12, 64(%rsp)          # 8-byte Spill
	movq	24(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,4), %rax
	movq	%rax, 80(%rsp)          # 8-byte Spill
	movq	16(%rsp), %rax          # 8-byte Reload
	leaq	(%rax,%r14,8), %rax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	movq	%rbp, 56(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB6_18:                               #   Parent Loop BB6_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_23 Depth 3
	leaq	(%rbp,%rbp,2), %rax
	movq	144(%rsp), %rcx
	leaq	(%rcx,%rax,4), %rcx
	movl	88(%rcx,%r14,4), %esi
	movl	68(%r13), %ecx
	incl	%ecx
                                        # kill: %CL<def> %CL<kill> %ECX<kill>
	shll	%cl, %esi
	movslq	scalefac_band+92(,%rbp,4), %rdx
	movslq	scalefac_band+96(,%rbp,4), %rcx
	movl	%ecx, %edi
	subl	%edx, %edi
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%edi, %xmm0
	xorpd	%xmm2, %xmm2
	jle	.LBB6_24
# BB#19:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB6_18 Depth=2
	movl	44(%r13,%r14,4), %edi
	shll	$3, %edi
	negl	%edi
	subl	%esi, %edi
	addl	12(%r13), %edi
	movslq	%edi, %rsi
	movsd	pow20(,%rsi,8), %xmm1   # xmm1 = mem[0],zero
	movl	%ecx, %edi
	subl	%edx, %edi
	leaq	-1(%rcx), %r8
	xorpd	%xmm3, %xmm3
	testb	$1, %dil
                                        # implicit-def: %XMM2
	movq	%rdx, %rdi
	je	.LBB6_21
# BB#20:                                # %.lr.ph.prol
                                        #   in Loop: Header=BB6_18 Depth=2
	leaq	(%rdx,%rdx,2), %rdi
	addq	%r14, %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	movsd	(%rsi,%rdi,8), %xmm2    # xmm2 = mem[0],zero
	andpd	.LCPI6_0(%rip), %xmm2
	movq	24(%rsp), %rbx          # 8-byte Reload
	movslq	(%rbx,%rdi,4), %rdi
	movsd	pow43(,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	.LCPI6_3, %xmm2
	leaq	1(%rdx), %rdi
	movapd	%xmm2, %xmm3
.LBB6_21:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB6_18 Depth=2
	cmpq	%rdx, %r8
	je	.LBB6_24
# BB#22:                                # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB6_18 Depth=2
	leaq	(%rdi,%rdi,2), %rsi
	movq	80(%rsp), %rdx          # 8-byte Reload
	leaq	(%rdx,%rsi,4), %rdx
	movq	72(%rsp), %rbx          # 8-byte Reload
	leaq	(%rbx,%rsi,8), %rsi
	subq	%rdi, %rcx
	.p2align	4, 0x90
.LBB6_23:                               # %.lr.ph
                                        #   Parent Loop BB6_3 Depth=1
                                        #     Parent Loop BB6_18 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movsd	(%rsi), %xmm4           # xmm4 = mem[0],zero
	movsd	24(%rsi), %xmm2         # xmm2 = mem[0],zero
	andpd	%xmm6, %xmm4
	movslq	(%rdx), %rdi
	movsd	pow43(,%rdi,8), %xmm5   # xmm5 = mem[0],zero
	mulsd	%xmm1, %xmm5
	subsd	%xmm5, %xmm4
	mulsd	%xmm4, %xmm4
	addsd	%xmm3, %xmm4
	andpd	%xmm6, %xmm2
	movslq	12(%rdx), %rdi
	movsd	pow43(,%rdi,8), %xmm3   # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	subsd	%xmm3, %xmm2
	mulsd	%xmm2, %xmm2
	addsd	%xmm4, %xmm2
	addq	$24, %rdx
	addq	$48, %rsi
	addq	$-2, %rcx
	movapd	%xmm2, %xmm3
	jne	.LBB6_23
.LBB6_24:                               # %._crit_edge
                                        #   in Loop: Header=BB6_18 Depth=2
	divsd	%xmm0, %xmm2
	imulq	$168, %r15, %r12
	movq	32(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%r12), %rcx
	movsd	%xmm2, (%rcx,%rbp,8)
	movq	48(%rsp), %rcx          # 8-byte Reload
	leaq	(%rcx,%rax,8), %rax
	divsd	176(%rax,%r14,8), %xmm2
	movsd	.LCPI6_1(%rip), %xmm0   # xmm0 = mem[0],zero
	maxsd	%xmm2, %xmm0
	callq	log10
	mulsd	.LCPI6_2(%rip), %xmm0
	addq	40(%rsp), %r12          # 8-byte Folded Reload
	movsd	%xmm0, (%r12,%rbp,8)
	incq	%rbp
	ucomisd	.LCPI6_3, %xmm0
	jbe	.LBB6_26
# BB#25:                                #   in Loop: Header=BB6_18 Depth=2
	incl	12(%rsp)                # 4-byte Folded Spill
	movq	152(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
.LBB6_26:                               #   in Loop: Header=BB6_18 Depth=2
	movq	160(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	addsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
	movq	168(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
	cmpq	$12, %rbp
	movapd	.LCPI6_0(%rip), %xmm6   # xmm6 = [nan,nan]
	jne	.LBB6_18
# BB#27:                                # %._crit_edge181.loopexit
                                        #   in Loop: Header=BB6_3 Depth=1
	movq	64(%rsp), %r12          # 8-byte Reload
	subl	56(%rsp), %r12d         # 4-byte Folded Reload
	jmp	.LBB6_28
	.p2align	4, 0x90
.LBB6_4:                                # %.._crit_edge181_crit_edge
                                        #   in Loop: Header=BB6_3 Depth=1
	incq	%r14
	movq	%r14, %r15
.LBB6_28:                               # %._crit_edge181
                                        #   in Loop: Header=BB6_3 Depth=1
	cmpq	$3, %r15
	movq	%r15, %r14
	jne	.LBB6_3
# BB#29:
	cmpl	$2, %r12d
	jl	.LBB6_31
# BB#30:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%r12d, %xmm0
	movq	160(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
.LBB6_31:
	movl	12(%rsp), %ecx          # 4-byte Reload
	cmpl	$2, %ecx
	jl	.LBB6_33
# BB#32:
	xorps	%xmm0, %xmm0
	cvtsi2sdl	%ecx, %xmm0
	movq	152(%rsp), %rax
	movsd	(%rax), %xmm1           # xmm1 = mem[0],zero
	divsd	%xmm0, %xmm1
	movsd	%xmm1, (%rax)
.LBB6_33:
	movl	%ecx, %eax
	addq	$88, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	calc_noise1, .Lfunc_end6-calc_noise1
	.cfi_endproc

	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI7_0:
	.quad	4611686018427387904     # double 2
.LCPI7_1:
	.quad	4607182418800017408     # double 1
.LCPI7_2:
	.quad	-4620693217682128896    # double -0.5
.LCPI7_3:
	.quad	4609434218613702656     # double 1.5
.LCPI7_4:
	.quad	-4616189618054758400    # double -1
	.text
	.globl	quant_compare
	.p2align	4, 0x90
	.type	quant_compare,@function
quant_compare:                          # @quant_compare
	.cfi_startproc
# BB#0:
	testl	%edi, %edi
	je	.LBB7_1
# BB#4:
	cmpl	$1, %edi
	sete	%dl
	ucomisd	%xmm5, %xmm2
	seta	%cl
	ucomisd	%xmm3, %xmm0
	seta	%sil
	cmpl	$2, %edi
	movl	%esi, %eax
	je	.LBB7_6
# BB#5:
	andb	%cl, %dl
	movl	%edx, %eax
.LBB7_6:
	cmpl	$4, %edi
	je	.LBB7_10
# BB#7:
	cmpl	$3, %edi
	jne	.LBB7_8
# BB#9:                                 # %.thread92
	ucomisd	%xmm3, %xmm0
	seta	%al
	addsd	.LCPI7_0(%rip), %xmm2
	ucomisd	%xmm5, %xmm2
	seta	%cl
	andb	%al, %cl
	movzbl	%cl, %eax
	retq
.LBB7_1:
	movb	$1, %al
	cmpl	%esi, %edx
	jl	.LBB7_3
# BB#2:
	sete	%cl
	ucomisd	%xmm4, %xmm1
	setae	%al
	andb	%cl, %al
.LBB7_3:                                # %.thread98
	movzbl	%al, %eax
	retq
.LBB7_10:
	movb	$1, %al
	ucomisd	.LCPI7_0(%rip), %xmm2
	jbe	.LBB7_12
# BB#11:
	xorpd	%xmm6, %xmm6
	ucomisd	%xmm5, %xmm6
	jae	.LBB7_28
.LBB7_12:
	movsd	.LCPI7_0(%rip), %xmm7   # xmm7 = mem[0],zero
	addsd	%xmm2, %xmm7
	ucomisd	%xmm3, %xmm0
	jbe	.LBB7_16
# BB#13:
	ucomisd	%xmm5, %xmm7
	jbe	.LBB7_16
# BB#14:
	xorpd	%xmm6, %xmm6
	ucomisd	%xmm2, %xmm6
	jbe	.LBB7_16
# BB#15:
	ucomisd	%xmm5, %xmm6
	jae	.LBB7_28
.LBB7_16:
	movapd	%xmm0, %xmm6
	addsd	%xmm1, %xmm6
	ucomisd	%xmm3, %xmm6
	jbe	.LBB7_20
# BB#17:
	ucomisd	%xmm5, %xmm7
	jbe	.LBB7_20
# BB#18:
	xorpd	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm2
	jbe	.LBB7_20
# BB#19:
	ucomisd	%xmm5, %xmm7
	jae	.LBB7_28
.LBB7_20:
	movapd	%xmm3, %xmm8
	addsd	%xmm4, %xmm8
	ucomisd	%xmm8, %xmm6
	jbe	.LBB7_24
# BB#21:
	movsd	.LCPI7_1(%rip), %xmm7   # xmm7 = mem[0],zero
	addsd	%xmm2, %xmm7
	ucomisd	%xmm5, %xmm7
	jbe	.LBB7_24
# BB#22:
	ucomisd	.LCPI7_2(%rip), %xmm2
	jbe	.LBB7_24
# BB#23:
	xorpd	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm5
	ja	.LBB7_28
.LBB7_24:
	movsd	.LCPI7_3(%rip), %xmm7   # xmm7 = mem[0],zero
	addsd	%xmm2, %xmm7
	xorl	%eax, %eax
	ucomisd	%xmm5, %xmm7
	jbe	.LBB7_28
# BB#25:
	ucomisd	.LCPI7_4(%rip), %xmm2
	jbe	.LBB7_28
# BB#26:
	xorpd	%xmm7, %xmm7
	ucomisd	%xmm7, %xmm5
	jbe	.LBB7_28
# BB#27:
	addsd	%xmm4, %xmm8
	addsd	%xmm1, %xmm6
	ucomisd	%xmm8, %xmm6
	seta	%al
.LBB7_28:
	movzbl	%al, %eax
	cmpl	$5, %edi
	je	.LBB7_30
	jmp	.LBB7_35
.LBB7_8:
	movzbl	%al, %eax
	cmpl	$5, %edi
	jne	.LBB7_35
.LBB7_30:
	movb	$1, %al
	ucomisd	%xmm4, %xmm1
	ja	.LBB7_34
# BB#31:
	ucomisd	%xmm1, %xmm4
	jne	.LBB7_32
	jp	.LBB7_32
# BB#33:
	movb	%sil, %al
	jmp	.LBB7_34
.LBB7_32:
	xorl	%eax, %eax
.LBB7_34:
	movzbl	%al, %eax
.LBB7_35:
	cmpl	$6, %edi
	jne	.LBB7_43
# BB#36:
	movb	$1, %al
	ucomisd	%xmm4, %xmm1
	ja	.LBB7_42
# BB#37:
	movapd	%xmm4, %xmm6
	cmpeqsd	%xmm1, %xmm6
	movd	%xmm6, %rax
	andl	$1, %eax
	ucomisd	%xmm1, %xmm4
	jne	.LBB7_42
	jp	.LBB7_42
# BB#38:
	ucomisd	%xmm5, %xmm2
	ja	.LBB7_42
# BB#39:
	ucomisd	%xmm2, %xmm5
	jne	.LBB7_40
	jp	.LBB7_40
# BB#41:
	ucomisd	%xmm3, %xmm0
	setae	%al
	jmp	.LBB7_42
.LBB7_40:
	xorl	%eax, %eax
.LBB7_42:
	movzbl	%al, %eax
.LBB7_43:
	retq
.Lfunc_end7:
	.size	quant_compare, .Lfunc_end7-quant_compare
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	3
.LCPI8_0:
	.quad	4610252940737434542     # double 1.6817928305074292
	.quad	4608519265307732519     # double 1.2968395546510096
	.section	.rodata.cst8,"aM",@progbits,8
	.p2align	3
.LCPI8_1:
	.quad	-4572244337315807232    # double -900
.LCPI8_2:
	.quad	4607407598781385933     # double 1.05
	.text
	.globl	amp_scalefac_bands
	.p2align	4, 0x90
	.type	amp_scalefac_bands,@function
amp_scalefac_bands:                     # @amp_scalefac_bands
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi65:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi66:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi67:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi68:
	.cfi_def_cfa_offset 40
.Lcfi69:
	.cfi_offset %rbx, -40
.Lcfi70:
	.cfi_offset %r12, -32
.Lcfi71:
	.cfi_offset %r14, -24
.Lcfi72:
	.cfi_offset %r15, -16
	xorl	%r8d, %r8d
	cmpl	$0, 68(%rsi)
	sete	%r8b
	movl	80(%rsi), %r10d
	testq	%r10, %r10
	je	.LBB8_1
# BB#2:                                 # %.lr.ph104
	leaq	-1(%r10), %r9
	movq	%r10, %rax
	andq	$3, %rax
	je	.LBB8_3
# BB#4:                                 # %.prol.preheader
	movsd	.LCPI8_1(%rip), %xmm1   # xmm1 = mem[0],zero
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB8_5:                                # =>This Inner Loop Header: Depth=1
	movsd	(%rcx,%rbx,8), %xmm0    # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	incq	%rbx
	cmpq	%rbx, %rax
	movapd	%xmm0, %xmm1
	jne	.LBB8_5
	jmp	.LBB8_6
.LBB8_1:
	movsd	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
	jmp	.LBB8_8
.LBB8_3:
	xorl	%ebx, %ebx
	movsd	.LCPI8_1(%rip), %xmm0   # xmm0 = mem[0],zero
.LBB8_6:                                # %.prol.loopexit
	cmpq	$3, %r9
	jb	.LBB8_8
	.p2align	4, 0x90
.LBB8_7:                                # =>This Inner Loop Header: Depth=1
	movsd	(%rcx,%rbx,8), %xmm1    # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	8(%rcx,%rbx,8), %xmm0   # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	movsd	16(%rcx,%rbx,8), %xmm1  # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	24(%rcx,%rbx,8), %xmm0  # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	addq	$4, %rbx
	cmpq	%r10, %rbx
	jb	.LBB8_7
.LBB8_8:                                # %._crit_edge105
	movl	84(%rsi), %r9d
	cmpq	$11, %r9
	ja	.LBB8_14
# BB#9:                                 # %.preheader86.preheader
	testb	$1, %r9b
	jne	.LBB8_11
# BB#10:
	movq	%r9, %rbx
	cmpl	$11, %r9d
	jne	.LBB8_13
	jmp	.LBB8_14
.LBB8_11:                               # %.preheader86.prol
	movsd	168(%rcx,%r9,8), %xmm1  # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	336(%rcx,%r9,8), %xmm2  # xmm2 = mem[0],zero
	maxsd	%xmm1, %xmm2
	movsd	504(%rcx,%r9,8), %xmm0  # xmm0 = mem[0],zero
	maxsd	%xmm2, %xmm0
	movq	%r9, %rbx
	incq	%rbx
	cmpl	$11, %r9d
	je	.LBB8_14
	.p2align	4, 0x90
.LBB8_13:                               # %.preheader86
                                        # =>This Inner Loop Header: Depth=1
	movsd	168(%rcx,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	336(%rcx,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	movsd	504(%rcx,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	176(%rcx,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	movsd	344(%rcx,%rbx,8), %xmm1 # xmm1 = mem[0],zero
	maxsd	%xmm0, %xmm1
	movsd	512(%rcx,%rbx,8), %xmm0 # xmm0 = mem[0],zero
	maxsd	%xmm1, %xmm0
	addq	$2, %rbx
	cmpq	$12, %rbx
	jne	.LBB8_13
.LBB8_14:                               # %._crit_edge
	movsd	.LCPI8_0(,%r8,8), %xmm1 # xmm1 = mem[0],zero
	testl	%r10d, %r10d
	mulsd	.LCPI8_2(%rip), %xmm0
	xorpd	%xmm2, %xmm2
	minsd	%xmm2, %xmm0
	je	.LBB8_33
# BB#15:                                # %.lr.ph95.preheader
	movaps	%xmm1, %xmm2
	movlhps	%xmm2, %xmm2            # xmm2 = xmm2[0,0]
	leaq	48(%rdi), %r8
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB8_16:                               # %.lr.ph95
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_26 Depth 2
                                        #     Child Loop BB8_30 Depth 2
	movl	%r9d, %r9d
	movsd	(%rcx,%r9,8), %xmm3     # xmm3 = mem[0],zero
	ucomisd	%xmm0, %xmm3
	jbe	.LBB8_17
# BB#18:                                #   in Loop: Header=BB8_16 Depth=1
	incl	(%rdx,%r9,4)
	movslq	scalefac_band(,%r9,4), %r10
	leal	1(%r9), %r9d
	movslq	scalefac_band(,%r9,4), %rax
	cmpl	%eax, %r10d
	jge	.LBB8_31
# BB#19:                                # %.lr.ph93.preheader
                                        #   in Loop: Header=BB8_16 Depth=1
	movq	%rax, %r11
	subq	%r10, %r11
	cmpq	$4, %r11
	jb	.LBB8_29
# BB#20:                                # %min.iters.checked
                                        #   in Loop: Header=BB8_16 Depth=1
	movq	%r11, %r14
	andq	$-4, %r14
	movq	%r11, %r15
	andq	$-4, %r15
	je	.LBB8_29
# BB#21:                                # %vector.ph
                                        #   in Loop: Header=BB8_16 Depth=1
	leaq	-4(%r15), %rbx
	movq	%rbx, %r12
	shrq	$2, %r12
	btl	$2, %ebx
	jb	.LBB8_22
# BB#23:                                # %vector.body.prol
                                        #   in Loop: Header=BB8_16 Depth=1
	movupd	(%rdi,%r10,8), %xmm3
	movupd	16(%rdi,%r10,8), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	%xmm3, (%rdi,%r10,8)
	movupd	%xmm4, 16(%rdi,%r10,8)
	movl	$4, %ebx
	testq	%r12, %r12
	jne	.LBB8_25
	jmp	.LBB8_27
	.p2align	4, 0x90
.LBB8_17:                               # %.lr.ph95..loopexit85_crit_edge
                                        #   in Loop: Header=BB8_16 Depth=1
	incl	%r9d
	cmpl	80(%rsi), %r9d
	jb	.LBB8_16
	jmp	.LBB8_32
.LBB8_22:                               #   in Loop: Header=BB8_16 Depth=1
	xorl	%ebx, %ebx
	testq	%r12, %r12
	je	.LBB8_27
.LBB8_25:                               # %vector.ph.new
                                        #   in Loop: Header=BB8_16 Depth=1
	movq	%r15, %r12
	subq	%rbx, %r12
	addq	%r10, %rbx
	leaq	(%r8,%rbx,8), %rbx
	.p2align	4, 0x90
.LBB8_26:                               # %vector.body
                                        #   Parent Loop BB8_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movupd	-48(%rbx), %xmm3
	movupd	-32(%rbx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	%xmm3, -48(%rbx)
	movupd	%xmm4, -32(%rbx)
	movupd	-16(%rbx), %xmm3
	movupd	(%rbx), %xmm4
	mulpd	%xmm2, %xmm3
	mulpd	%xmm2, %xmm4
	movupd	%xmm3, -16(%rbx)
	movupd	%xmm4, (%rbx)
	addq	$64, %rbx
	addq	$-8, %r12
	jne	.LBB8_26
.LBB8_27:                               # %middle.block
                                        #   in Loop: Header=BB8_16 Depth=1
	cmpq	%r15, %r11
	je	.LBB8_31
# BB#28:                                #   in Loop: Header=BB8_16 Depth=1
	addq	%r14, %r10
	.p2align	4, 0x90
.LBB8_29:                               # %.lr.ph93.preheader136
                                        #   in Loop: Header=BB8_16 Depth=1
	leaq	(%rdi,%r10,8), %rbx
	subq	%r10, %rax
	.p2align	4, 0x90
.LBB8_30:                               # %.lr.ph93
                                        #   Parent Loop BB8_16 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx), %xmm3           # xmm3 = mem[0],zero
	mulsd	%xmm1, %xmm3
	movsd	%xmm3, (%rbx)
	addq	$8, %rbx
	decq	%rax
	jne	.LBB8_30
.LBB8_31:                               # %.loopexit85
                                        #   in Loop: Header=BB8_16 Depth=1
	cmpl	80(%rsi), %r9d
	jb	.LBB8_16
.LBB8_32:                               # %.preheader.preheader.loopexit
	movl	84(%rsi), %r9d
.LBB8_33:                               # %.preheader.preheader
	cmpl	$12, %r9d
	jae	.LBB8_69
# BB#34:                                # %.lr.ph90
	movl	%r9d, %r8d
	.p2align	4, 0x90
.LBB8_35:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_40 Depth 2
                                        #     Child Loop BB8_43 Depth 2
	movsd	168(%rcx,%r8,8), %xmm2  # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB8_36
# BB#37:                                #   in Loop: Header=BB8_35 Depth=1
	leaq	(%r8,%r8,2), %rax
	incl	88(%rdx,%rax,4)
	movslq	scalefac_band+92(,%r8,4), %r11
	movslq	scalefac_band+96(,%r8,4), %r10
	incq	%r8
	cmpl	%r10d, %r11d
	jge	.LBB8_44
# BB#38:                                # %.lr.ph.preheader
                                        #   in Loop: Header=BB8_35 Depth=1
	movl	%r10d, %eax
	subl	%r11d, %eax
	leaq	-1(%r10), %r9
	subq	%r11, %r9
	andq	$3, %rax
	je	.LBB8_41
# BB#39:                                # %.lr.ph.prol.preheader
                                        #   in Loop: Header=BB8_35 Depth=1
	leaq	(%r11,%r11,2), %rbx
	leaq	(%rdi,%rbx,8), %rbx
	negq	%rax
	.p2align	4, 0x90
.LBB8_40:                               # %.lr.ph.prol
                                        #   Parent Loop BB8_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx)
	incq	%r11
	addq	$24, %rbx
	incq	%rax
	jne	.LBB8_40
.LBB8_41:                               # %.lr.ph.prol.loopexit
                                        #   in Loop: Header=BB8_35 Depth=1
	cmpq	$3, %r9
	jb	.LBB8_44
# BB#42:                                # %.lr.ph.preheader.new
                                        #   in Loop: Header=BB8_35 Depth=1
	leaq	(%r11,%r11,2), %rax
	leaq	(%rdi,%rax,8), %rax
	subq	%r11, %r10
	.p2align	4, 0x90
.LBB8_43:                               # %.lr.ph
                                        #   Parent Loop BB8_35 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax)
	movsd	24(%rax), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 24(%rax)
	movsd	48(%rax), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 48(%rax)
	movsd	72(%rax), %xmm2         # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, 72(%rax)
	addq	$96, %rax
	addq	$-4, %r10
	jne	.LBB8_43
	jmp	.LBB8_44
	.p2align	4, 0x90
.LBB8_36:                               # %..loopexit_crit_edge
                                        #   in Loop: Header=BB8_35 Depth=1
	incq	%r8
.LBB8_44:                               # %.loopexit
                                        #   in Loop: Header=BB8_35 Depth=1
	cmpq	$12, %r8
	jne	.LBB8_35
# BB#45:                                # %.loopexit84
	movl	84(%rsi), %r10d
	cmpq	$12, %r10
	jae	.LBB8_69
# BB#46:                                # %.lr.ph90.1
	leaq	8(%rdi), %r8
	leaq	80(%rdi), %r9
	.p2align	4, 0x90
.LBB8_47:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_52 Depth 2
                                        #     Child Loop BB8_55 Depth 2
	movsd	336(%rcx,%r10,8), %xmm2 # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB8_48
# BB#49:                                #   in Loop: Header=BB8_47 Depth=1
	leaq	(%r10,%r10,2), %rax
	incl	92(%rdx,%rax,4)
	movslq	scalefac_band+92(,%r10,4), %r15
	movslq	scalefac_band+96(,%r10,4), %r14
	incq	%r10
	cmpl	%r14d, %r15d
	jge	.LBB8_56
# BB#50:                                # %.lr.ph.preheader.1
                                        #   in Loop: Header=BB8_47 Depth=1
	movl	%r14d, %eax
	subl	%r15d, %eax
	leaq	-1(%r14), %r11
	subq	%r15, %r11
	andq	$3, %rax
	je	.LBB8_53
# BB#51:                                # %.lr.ph.1.prol.preheader
                                        #   in Loop: Header=BB8_47 Depth=1
	leaq	(%r15,%r15,2), %rbx
	leaq	(%r8,%rbx,8), %rbx
	negq	%rax
	.p2align	4, 0x90
.LBB8_52:                               # %.lr.ph.1.prol
                                        #   Parent Loop BB8_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rbx), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rbx)
	incq	%r15
	addq	$24, %rbx
	incq	%rax
	jne	.LBB8_52
.LBB8_53:                               # %.lr.ph.1.prol.loopexit
                                        #   in Loop: Header=BB8_47 Depth=1
	cmpq	$3, %r11
	jb	.LBB8_56
# BB#54:                                # %.lr.ph.preheader.1.new
                                        #   in Loop: Header=BB8_47 Depth=1
	leaq	(%r15,%r15,2), %rax
	leaq	(%r9,%rax,8), %rax
	subq	%r15, %r14
	.p2align	4, 0x90
.LBB8_55:                               # %.lr.ph.1
                                        #   Parent Loop BB8_47 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-72(%rax), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -72(%rax)
	movsd	-48(%rax), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -48(%rax)
	movsd	-24(%rax), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -24(%rax)
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax)
	addq	$96, %rax
	addq	$-4, %r14
	jne	.LBB8_55
	jmp	.LBB8_56
	.p2align	4, 0x90
.LBB8_48:                               # %..loopexit.1_crit_edge
                                        #   in Loop: Header=BB8_47 Depth=1
	incq	%r10
.LBB8_56:                               # %.loopexit.1
                                        #   in Loop: Header=BB8_47 Depth=1
	cmpq	$12, %r10
	jne	.LBB8_47
# BB#57:                                # %.loopexit84.1
	movl	84(%rsi), %r10d
	cmpq	$11, %r10
	ja	.LBB8_69
# BB#58:                                # %.lr.ph90.2
	leaq	16(%rdi), %r8
	addq	$88, %rdi
	.p2align	4, 0x90
.LBB8_59:                               # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_64 Depth 2
                                        #     Child Loop BB8_67 Depth 2
	movsd	504(%rcx,%r10,8), %xmm2 # xmm2 = mem[0],zero
	ucomisd	%xmm0, %xmm2
	jbe	.LBB8_60
# BB#61:                                #   in Loop: Header=BB8_59 Depth=1
	leaq	(%r10,%r10,2), %rax
	incl	96(%rdx,%rax,4)
	movslq	scalefac_band+92(,%r10,4), %rbx
	movslq	scalefac_band+96(,%r10,4), %r11
	incq	%r10
	cmpl	%r11d, %ebx
	jge	.LBB8_68
# BB#62:                                # %.lr.ph.preheader.2
                                        #   in Loop: Header=BB8_59 Depth=1
	movl	%r11d, %esi
	subl	%ebx, %esi
	leaq	-1(%r11), %r9
	subq	%rbx, %r9
	andq	$3, %rsi
	je	.LBB8_65
# BB#63:                                # %.lr.ph.2.prol.preheader
                                        #   in Loop: Header=BB8_59 Depth=1
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%r8,%rax,8), %rax
	negq	%rsi
	.p2align	4, 0x90
.LBB8_64:                               # %.lr.ph.2.prol
                                        #   Parent Loop BB8_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	(%rax), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rax)
	incq	%rbx
	addq	$24, %rax
	incq	%rsi
	jne	.LBB8_64
.LBB8_65:                               # %.lr.ph.2.prol.loopexit
                                        #   in Loop: Header=BB8_59 Depth=1
	cmpq	$3, %r9
	jb	.LBB8_68
# BB#66:                                # %.lr.ph.preheader.2.new
                                        #   in Loop: Header=BB8_59 Depth=1
	leaq	(%rbx,%rbx,2), %rax
	leaq	(%rdi,%rax,8), %rsi
	subq	%rbx, %r11
	.p2align	4, 0x90
.LBB8_67:                               # %.lr.ph.2
                                        #   Parent Loop BB8_59 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movsd	-72(%rsi), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -72(%rsi)
	movsd	-48(%rsi), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -48(%rsi)
	movsd	-24(%rsi), %xmm2        # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, -24(%rsi)
	movsd	(%rsi), %xmm2           # xmm2 = mem[0],zero
	mulsd	%xmm1, %xmm2
	movsd	%xmm2, (%rsi)
	addq	$96, %rsi
	addq	$-4, %r11
	jne	.LBB8_67
	jmp	.LBB8_68
	.p2align	4, 0x90
.LBB8_60:                               # %..loopexit.2_crit_edge
                                        #   in Loop: Header=BB8_59 Depth=1
	incq	%r10
.LBB8_68:                               # %.loopexit.2
                                        #   in Loop: Header=BB8_59 Depth=1
	cmpq	$12, %r10
	jne	.LBB8_59
.LBB8_69:                               # %.loopexit84.2
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end8:
	.size	amp_scalefac_bands, .Lfunc_end8-amp_scalefac_bands
	.cfi_endproc

	.type	outer_loop.OldValue,@object # @outer_loop.OldValue
	.data
	.p2align	2
outer_loop.OldValue:
	.long	180                     # 0xb4
	.long	180                     # 0xb4
	.size	outer_loop.OldValue, 8


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git f3ac0b73fdd957ae03ce321fb981631bc2e47e22) (https://github.com/aqjune/llvm-intptr.git 268e2f29a752698e0fb6947165ff276e60e0b221)"
	.section	".note.GNU-stack","",@progbits
