	.text
	.file	"jquant1.bc"
	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI0_0:
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.long	1                       # 0x1
	.text
	.globl	jinit_1pass_quantizer
	.p2align	4, 0x90
	.type	jinit_1pass_quantizer,@function
jinit_1pass_quantizer:                  # @jinit_1pass_quantizer
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi2:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi3:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi4:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi5:
	.cfi_def_cfa_offset 56
	subq	$56, %rsp
.Lcfi6:
	.cfi_def_cfa_offset 112
.Lcfi7:
	.cfi_offset %rbx, -56
.Lcfi8:
	.cfi_offset %r12, -48
.Lcfi9:
	.cfi_offset %r13, -40
.Lcfi10:
	.cfi_offset %r14, -32
.Lcfi11:
	.cfi_offset %r15, -24
.Lcfi12:
	.cfi_offset %rbp, -16
	movq	%rdi, %rbx
	movq	8(%rbx), %rax
	movl	$1, %esi
	movl	$152, %edx
	callq	*(%rax)
	movq	%rax, 608(%rbx)
	movq	$start_pass_1_quant, (%rax)
	movl	$new_color_map_1_quant, %ecx
	movd	%rcx, %xmm0
	movl	$finish_pass_1_quant, %ecx
	movd	%rcx, %xmm1
	punpcklqdq	%xmm0, %xmm1    # xmm1 = xmm1[0],xmm0[0]
	movdqu	%xmm1, 16(%rax)
	movq	$0, 112(%rax)
	movq	$0, 80(%rax)
	cmpl	$5, 136(%rbx)
	jl	.LBB0_2
# BB#1:
	movq	(%rbx), %rax
	movl	$54, 40(%rax)
	movl	$4, 44(%rax)
	movq	%rbx, %rdi
	callq	*(%rax)
.LBB0_2:
	movl	112(%rbx), %eax
	cmpl	$257, %eax              # imm = 0x101
	jl	.LBB0_4
# BB#3:
	movq	(%rbx), %rax
	movl	$56, 40(%rax)
	movl	$256, 44(%rax)          # imm = 0x100
	movq	%rbx, %rdi
	callq	*(%rax)
	movl	112(%rbx), %eax
.LBB0_4:
	movq	608(%rbx), %r14
	movq	%rbx, (%rsp)            # 8-byte Spill
	movslq	136(%rbx), %r15
	cmpq	$2, %r15
	movl	%r15d, %ebp
	movslq	%eax, %r12
	jl	.LBB0_59
# BB#5:                                 # %.split.us.i.i.preheader
	leal	7(%r15), %eax
	leal	-2(%r15), %ecx
	andl	$7, %eax
	movl	$1, %edx
	.p2align	4, 0x90
.LBB0_6:                                # %.split.us.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_9 Depth 2
                                        #     Child Loop BB0_13 Depth 2
	movq	%rdx, %r13
	leaq	1(%r13), %rdx
	testl	%eax, %eax
	je	.LBB0_7
# BB#8:                                 # %.prol.preheader75
                                        #   in Loop: Header=BB0_6 Depth=1
	xorl	%edi, %edi
	movq	%rdx, %rsi
	.p2align	4, 0x90
.LBB0_9:                                #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rdx, %rsi
	incl	%edi
	cmpl	%edi, %eax
	jne	.LBB0_9
# BB#10:                                # %.prol.loopexit76.unr-lcssa
                                        #   in Loop: Header=BB0_6 Depth=1
	incl	%edi
	cmpl	$7, %ecx
	jae	.LBB0_12
	jmp	.LBB0_14
	.p2align	4, 0x90
.LBB0_7:                                #   in Loop: Header=BB0_6 Depth=1
	movl	$1, %edi
	movq	%rdx, %rsi
	cmpl	$7, %ecx
	jb	.LBB0_14
.LBB0_12:                               # %.split.us.i.i.new
                                        #   in Loop: Header=BB0_6 Depth=1
	movl	%ebp, %ebx
	subl	%edi, %ebx
	.p2align	4, 0x90
.LBB0_13:                               #   Parent Loop BB0_6 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	imulq	%rdx, %rsi
	imulq	%rdx, %rsi
	imulq	%rdx, %rsi
	imulq	%rdx, %rsi
	imulq	%rdx, %rsi
	imulq	%rdx, %rsi
	imulq	%rdx, %rsi
	imulq	%rdx, %rsi
	addl	$-8, %ebx
	jne	.LBB0_13
.LBB0_14:                               # %._crit_edge89.us.i.i
                                        #   in Loop: Header=BB0_6 Depth=1
	cmpq	%r12, %rsi
	jle	.LBB0_6
	jmp	.LBB0_15
.LBB0_59:                               # %.split.i.preheader.i
	testq	%r12, %r12
	movl	$1, %r13d
	cmovgq	%r12, %r13
	leaq	1(%r13), %rsi
.LBB0_15:                               # %.us-lcssa91.us.i.i
	cmpl	$1, %r13d
	jg	.LBB0_17
# BB#16:
	movq	(%rsp), %rdi            # 8-byte Reload
	movq	(%rdi), %rax
	movl	$55, 40(%rax)
	movl	%esi, 44(%rax)
	callq	*(%rax)
.LBB0_17:                               # %.preheader64.i.i
	testl	%ebp, %ebp
	jle	.LBB0_18
# BB#19:                                # %.lr.ph83.preheader.i.i
	movl	$1, %eax
	cmpl	$7, %r15d
	jbe	.LBB0_20
# BB#23:                                # %min.iters.checked
	movl	%ebp, %edx
	andl	$7, %edx
	movq	%rbp, %rcx
	subq	%rdx, %rcx
	je	.LBB0_20
# BB#24:                                # %vector.ph
	movd	%r13d, %xmm0
	pshufd	$0, %xmm0, %xmm1        # xmm1 = xmm0[0,0,0,0]
	leaq	76(%r14), %rax
	movdqa	.LCPI0_0(%rip), %xmm0   # xmm0 = [1,1,1,1]
	pshufd	$245, %xmm1, %xmm3      # xmm3 = xmm1[1,1,3,3]
	movq	%rcx, %rsi
	movdqa	%xmm0, %xmm2
	.p2align	4, 0x90
.LBB0_25:                               # %vector.body
                                        # =>This Inner Loop Header: Depth=1
	movdqu	%xmm1, -16(%rax)
	movdqu	%xmm1, (%rax)
	pshufd	$245, %xmm0, %xmm4      # xmm4 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm0    # xmm0 = xmm0[0],xmm4[0],xmm0[1],xmm4[1]
	pshufd	$245, %xmm2, %xmm4      # xmm4 = xmm2[1,1,3,3]
	pmuludq	%xmm1, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pmuludq	%xmm3, %xmm4
	pshufd	$232, %xmm4, %xmm4      # xmm4 = xmm4[0,2,2,3]
	punpckldq	%xmm4, %xmm2    # xmm2 = xmm2[0],xmm4[0],xmm2[1],xmm4[1]
	addq	$32, %rax
	addq	$-8, %rsi
	jne	.LBB0_25
# BB#26:                                # %middle.block
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	pshufd	$78, %xmm2, %xmm0       # xmm0 = xmm2[2,3,0,1]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm2
	pshufd	$232, %xmm2, %xmm2      # xmm2 = xmm2[0,2,2,3]
	pshufd	$245, %xmm0, %xmm0      # xmm0 = xmm0[1,1,3,3]
	pmuludq	%xmm1, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	punpckldq	%xmm0, %xmm2    # xmm2 = xmm2[0],xmm0[0],xmm2[1],xmm0[1]
	pshufd	$229, %xmm2, %xmm0      # xmm0 = xmm2[1,1,2,3]
	pmuludq	%xmm2, %xmm0
	pshufd	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,2,2,3]
	pshufd	$245, %xmm2, %xmm1      # xmm1 = xmm2[1,1,3,3]
	pmuludq	%xmm0, %xmm1
	pshufd	$232, %xmm1, %xmm1      # xmm1 = xmm1[0,2,2,3]
	punpckldq	%xmm1, %xmm0    # xmm0 = xmm0[0],xmm1[0],xmm0[1],xmm1[1]
	movd	%xmm0, %eax
	testl	%edx, %edx
	jne	.LBB0_21
	jmp	.LBB0_27
.LBB0_20:
	xorl	%ecx, %ecx
.LBB0_21:                               # %.lr.ph83.i.i.preheader
	subq	%rcx, %rbp
	leaq	60(%r14,%rcx,4), %rcx
	.p2align	4, 0x90
.LBB0_22:                               # %.lr.ph83.i.i
                                        # =>This Inner Loop Header: Depth=1
	movl	%r13d, (%rcx)
	imull	%r13d, %eax
	addq	$4, %rcx
	decq	%rbp
	jne	.LBB0_22
.LBB0_27:                               # %.preheader.split.us.preheader.i.i
	movq	(%rsp), %rcx            # 8-byte Reload
	cmpl	$2, 56(%rcx)
	jne	.LBB0_32
	.p2align	4, 0x90
.LBB0_28:                               # %.preheader.split.us.i.us.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_29 Depth 2
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	%eax, %r13d
	.p2align	4, 0x90
.LBB0_29:                               # %.lr.ph.split.us.us.i.us.i
                                        #   Parent Loop BB0_28 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	select_ncolors.RGB_order(,%rcx,4), %rdi
	movslq	60(%r14,%rdi,4), %rbp
	movl	%r13d, %eax
	cltd
	idivl	%ebp
	cltq
	incq	%rbp
	imulq	%rbp, %rax
	cmpq	%r12, %rax
	jg	.LBB0_35
# BB#30:                                #   in Loop: Header=BB0_29 Depth=2
	movl	%ebp, 60(%r14,%rdi,4)
	incq	%rcx
	movl	$1, %esi
	cmpq	%r15, %rcx
	movl	%eax, %r13d
	jl	.LBB0_29
	jmp	.LBB0_28
	.p2align	4, 0x90
.LBB0_35:                               # %._crit_edge.us.i.loopexit.us.i
                                        #   in Loop: Header=BB0_28 Depth=1
	testl	%esi, %esi
	movl	%r13d, %eax
	jne	.LBB0_28
	jmp	.LBB0_36
	.p2align	4, 0x90
.LBB0_32:                               # %.preheader.split.us.i.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_33 Depth 2
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	movl	%eax, %r13d
	.p2align	4, 0x90
.LBB0_33:                               # %.lr.ph..lr.ph.split_crit_edge.us.i.i
                                        #   Parent Loop BB0_32 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movslq	60(%r14,%rcx,4), %rdi
	movl	%r13d, %eax
	cltd
	idivl	%edi
	cltq
	incq	%rdi
	imulq	%rdi, %rax
	cmpq	%r12, %rax
	jg	.LBB0_31
# BB#34:                                #   in Loop: Header=BB0_33 Depth=2
	movl	%edi, 60(%r14,%rcx,4)
	incq	%rcx
	movl	$1, %esi
	cmpq	%r15, %rcx
	movl	%eax, %r13d
	jl	.LBB0_33
	jmp	.LBB0_32
	.p2align	4, 0x90
.LBB0_31:                               # %._crit_edge.us.i.i
                                        #   in Loop: Header=BB0_32 Depth=1
	testl	%esi, %esi
	movl	%r13d, %eax
	jne	.LBB0_32
	jmp	.LBB0_36
.LBB0_18:
	movl	$1, %r13d
.LBB0_36:                               # %select_ncolors.exit.i
	movq	(%rsp), %rbx            # 8-byte Reload
	movq	(%rbx), %rcx
	movq	8(%rcx), %rax
	cmpl	$3, 136(%rbx)
	jne	.LBB0_38
# BB#37:
	movl	%r13d, 44(%rcx)
	movl	60(%r14), %edx
	movl	%edx, 48(%rcx)
	movl	64(%r14), %edx
	movl	%edx, 52(%rcx)
	movl	68(%r14), %edx
	movl	%edx, 56(%rcx)
	movl	$93, 40(%rcx)
	jmp	.LBB0_39
.LBB0_38:
	movl	$94, 40(%rcx)
	movl	%r13d, 44(%rcx)
.LBB0_39:
	movl	$1, %esi
	movq	%rbx, %rdi
	callq	*%rax
	movq	8(%rbx), %rax
	movl	136(%rbx), %ecx
	movl	$1, %esi
	movq	%rbx, %rdi
	movl	%r13d, %edx
	callq	*16(%rax)
	movq	%rax, %rcx
	movl	136(%rbx), %edi
	testl	%edi, %edi
	jle	.LBB0_48
# BB#40:                                # %.lr.ph114.preheader.i
	movslq	%r13d, %r12
	xorl	%esi, %esi
	movl	%r13d, %r8d
	movq	%r14, 16(%rsp)          # 8-byte Spill
	.p2align	4, 0x90
.LBB0_41:                               # %.lr.ph114.i
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB0_44 Depth 2
                                        #       Child Loop BB0_54 Depth 3
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_58 Depth 4
	movslq	60(%r14,%rsi,4), %rbp
	movl	%r8d, %eax
	cltd
	idivl	%ebp
	testq	%rbp, %rbp
	jle	.LBB0_47
# BB#42:                                # %.lr.ph101.i
                                        #   in Loop: Header=BB0_41 Depth=1
	testl	%eax, %eax
	jle	.LBB0_47
# BB#43:                                # %.lr.ph101.split.us.preheader.i
                                        #   in Loop: Header=BB0_41 Depth=1
	leaq	-1(%rbp), %rdi
	movl	%edi, %edx
	shrl	$31, %edx
	movq	%rdi, 32(%rsp)          # 8-byte Spill
	addl	%edi, %edx
	sarl	%edx
	movslq	%edx, %rdx
	movq	%rdx, 24(%rsp)          # 8-byte Spill
	movslq	%r8d, %r14
	movslq	%eax, %rdx
	movq	%rdx, 48(%rsp)          # 8-byte Spill
	movl	%eax, 12(%rsp)          # 4-byte Spill
	movl	%eax, %r11d
	movl	%ebp, %eax
	movq	%rax, 40(%rsp)          # 8-byte Spill
	leaq	-1(%r11), %rdi
	movl	%r11d, %ebp
	andl	$3, %ebp
	xorl	%r10d, %r10d
	xorl	%r9d, %r9d
	.p2align	4, 0x90
.LBB0_44:                               # %.lr.ph101.split.us.i
                                        #   Parent Loop BB0_41 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB0_54 Depth 3
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_58 Depth 4
	movq	%r10, %rax
	imulq	48(%rsp), %rax          # 8-byte Folded Reload
	cmpq	%r12, %rax
	jge	.LBB0_45
# BB#53:                                # %.preheader.lr.ph.us.i
                                        #   in Loop: Header=BB0_44 Depth=2
	movslq	%r9d, %r8
	movq	%r10, %rax
	shlq	$8, %rax
	subq	%r10, %rax
	addq	24(%rsp), %rax          # 8-byte Folded Reload
	cqto
	idivq	32(%rsp)                # 8-byte Folded Reload
	.p2align	4, 0x90
.LBB0_54:                               # %.preheader.us.us.i
                                        #   Parent Loop BB0_41 Depth=1
                                        #     Parent Loop BB0_44 Depth=2
                                        # =>    This Loop Header: Depth=3
                                        #         Child Loop BB0_55 Depth 4
                                        #         Child Loop BB0_58 Depth 4
	xorl	%edx, %edx
	testq	%rbp, %rbp
	je	.LBB0_56
	.p2align	4, 0x90
.LBB0_55:                               #   Parent Loop BB0_41 Depth=1
                                        #     Parent Loop BB0_44 Depth=2
                                        #       Parent Loop BB0_54 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rcx,%rsi,8), %r15
	addq	%rdx, %r15
	movb	%al, (%r8,%r15)
	incq	%rdx
	cmpq	%rdx, %rbp
	jne	.LBB0_55
.LBB0_56:                               # %.prol.loopexit
                                        #   in Loop: Header=BB0_54 Depth=3
	cmpq	$3, %rdi
	jb	.LBB0_57
	.p2align	4, 0x90
.LBB0_58:                               #   Parent Loop BB0_41 Depth=1
                                        #     Parent Loop BB0_44 Depth=2
                                        #       Parent Loop BB0_54 Depth=3
                                        # =>      This Inner Loop Header: Depth=4
	movq	(%rcx,%rsi,8), %rbx
	addq	%rdx, %rbx
	movb	%al, (%r8,%rbx)
	movq	(%rcx,%rsi,8), %rbx
	addq	%rdx, %rbx
	movb	%al, 1(%r8,%rbx)
	movq	(%rcx,%rsi,8), %rbx
	addq	%rdx, %rbx
	movb	%al, 2(%r8,%rbx)
	movq	(%rcx,%rsi,8), %rbx
	addq	%rdx, %rbx
	movb	%al, 3(%r8,%rbx)
	addq	$4, %rdx
	cmpq	%rdx, %r11
	jne	.LBB0_58
.LBB0_57:                               # %._crit_edge.us.us.i
                                        #   in Loop: Header=BB0_54 Depth=3
	addq	%r14, %r8
	cmpq	%r12, %r8
	jl	.LBB0_54
.LBB0_45:                               # %._crit_edge98.us-lcssa.us.us.i
                                        #   in Loop: Header=BB0_44 Depth=2
	incq	%r10
	addl	12(%rsp), %r9d          # 4-byte Folded Reload
	cmpq	40(%rsp), %r10          # 8-byte Folded Reload
	jne	.LBB0_44
# BB#46:                                # %._crit_edge102.loopexit.i
                                        #   in Loop: Header=BB0_41 Depth=1
	movq	(%rsp), %rax            # 8-byte Reload
	movl	136(%rax), %edi
	movq	16(%rsp), %r14          # 8-byte Reload
	movl	12(%rsp), %eax          # 4-byte Reload
.LBB0_47:                               # %._crit_edge102.i
                                        #   in Loop: Header=BB0_41 Depth=1
	incq	%rsi
	movslq	%edi, %rdx
	cmpq	%rdx, %rsi
	movl	%eax, %r8d
	jl	.LBB0_41
.LBB0_48:                               # %create_colormap.exit
	movq	%rcx, 32(%r14)
	movl	%r13d, 40(%r14)
	movq	(%rsp), %rbp            # 8-byte Reload
	movq	%rbp, %rdi
	callq	create_colorindex
	cmpl	$2, 104(%rbp)
	jne	.LBB0_52
# BB#49:
	cmpl	$0, 136(%rbp)
	jle	.LBB0_52
# BB#50:                                # %.lr.ph.i.preheader
	movq	608(%rbp), %r14
	movl	128(%rbp), %r15d
	addl	$2, %r15d
	addq	%r15, %r15
	xorl	%ebx, %ebx
	.p2align	4, 0x90
.LBB0_51:                               # %.lr.ph.i
                                        # =>This Inner Loop Header: Depth=1
	movq	8(%rbp), %rax
	movl	$1, %esi
	movq	%rbp, %rdi
	movq	%r15, %rdx
	callq	*8(%rax)
	movq	%rax, 112(%r14,%rbx,8)
	incq	%rbx
	movslq	136(%rbp), %rax
	cmpq	%rax, %rbx
	jl	.LBB0_51
.LBB0_52:                               # %alloc_fs_workspace.exit
	addq	$56, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end0:
	.size	jinit_1pass_quantizer, .Lfunc_end0-jinit_1pass_quantizer
	.cfi_endproc

	.section	.rodata.cst16,"aM",@progbits,16
	.p2align	4
.LCPI1_0:
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
	.long	255                     # 0xff
.LCPI1_1:
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.short	255                     # 0xff
	.zero	2
	.zero	2
	.zero	2
	.zero	2
.LCPI1_2:
	.long	2147483648              # 0x80000000
	.long	0                       # 0x0
	.long	2147483648              # 0x80000000
	.long	0                       # 0x0
	.text
	.p2align	4, 0x90
	.type	start_pass_1_quant,@function
start_pass_1_quant:                     # @start_pass_1_quant
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi13:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi14:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi15:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi16:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi17:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi18:
	.cfi_def_cfa_offset 56
	subq	$136, %rsp
.Lcfi19:
	.cfi_def_cfa_offset 192
.Lcfi20:
	.cfi_offset %rbx, -56
.Lcfi21:
	.cfi_offset %r12, -48
.Lcfi22:
	.cfi_offset %r13, -40
.Lcfi23:
	.cfi_offset %r14, -32
.Lcfi24:
	.cfi_offset %r15, -24
.Lcfi25:
	.cfi_offset %rbp, -16
	movq	%rdi, %r14
	movq	608(%r14), %r15
	movq	32(%r15), %rax
	movq	%rax, 152(%r14)
	movl	40(%r15), %eax
	movl	%eax, 148(%r14)
	movl	104(%r14), %eax
	cmpl	$2, %eax
	je	.LBB1_38
# BB#1:
	cmpl	$1, %eax
	je	.LBB1_4
# BB#2:
	testl	%eax, %eax
	jne	.LBB1_47
# BB#3:
	cmpl	$3, 136(%r14)
	movl	$color_quantize3, %eax
	movl	$color_quantize, %ecx
	cmoveq	%rax, %rcx
	movq	%rcx, 8(%r15)
	jmp	.LBB1_46
.LBB1_38:
	movq	$quantize_fs_dither, 8(%r15)
	movl	$0, 144(%r15)
	cmpq	$0, 112(%r15)
	je	.LBB1_40
# BB#39:                                # %.alloc_fs_workspace.exit_crit_edge
	movl	136(%r14), %eax
	leaq	136(%r14), %r12
	subq	$-128, %r14
	movq	%r14, %r13
	testl	%eax, %eax
	jg	.LBB1_44
	jmp	.LBB1_46
.LBB1_4:
	cmpl	$3, 136(%r14)
	movl	$quantize3_ord_dither, %eax
	movl	$quantize_ord_dither, %ecx
	cmoveq	%rax, %rcx
	movq	%rcx, 8(%r15)
	movl	$0, 76(%r15)
	cmpl	$0, 56(%r15)
	jne	.LBB1_6
# BB#5:
	movq	%r14, %rdi
	callq	create_colorindex
.LBB1_6:
	cmpq	$0, 80(%r15)
	jne	.LBB1_46
# BB#7:
	movl	136(%r14), %eax
	testl	%eax, %eax
	jle	.LBB1_46
# BB#8:                                 # %.lr.ph32.i
	movq	608(%r14), %r15
	xorl	%r12d, %r12d
	.p2align	4, 0x90
.LBB1_9:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB1_12 Depth 2
                                        #     Child Loop BB1_15 Depth 2
                                        #       Child Loop BB1_16 Depth 3
	movslq	60(%r15,%r12,4), %rbx
	testq	%r12, %r12
	jle	.LBB1_14
# BB#10:                                # %.lr.ph.i.preheader
                                        #   in Loop: Header=BB1_9 Depth=1
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB1_12:                               # %.lr.ph.i
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	cmpl	60(%r15,%rcx,4), %ebx
	je	.LBB1_13
# BB#11:                                #   in Loop: Header=BB1_12 Depth=2
	incq	%rcx
	cmpq	%r12, %rcx
	jl	.LBB1_12
	jmp	.LBB1_14
	.p2align	4, 0x90
.LBB1_13:                               #   in Loop: Header=BB1_9 Depth=1
	movq	80(%r15,%rcx,8), %rcx
	testq	%rcx, %rcx
	jne	.LBB1_37
	.p2align	4, 0x90
.LBB1_14:                               # %.thread.i
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	8(%r14), %rax
	movl	$1, %esi
	movl	$1024, %edx             # imm = 0x400
	movq	%r14, %rdi
	callq	*(%rax)
	movdqa	.LCPI1_2(%rip), %xmm12  # xmm12 = [2147483648,0,2147483648,0]
	pcmpeqd	%xmm11, %xmm11
	movdqa	.LCPI1_1(%rip), %xmm10  # xmm10 = <255,255,255,255,u,u,u,u>
	movdqa	.LCPI1_0(%rip), %xmm9   # xmm9 = [255,255,255,255]
	pxor	%xmm8, %xmm8
	movq	%rax, %rcx
	shlq	$9, %rbx
	addq	$-512, %rbx             # imm = 0xFE00
	xorl	%r9d, %r9d
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB1_15:                               # %.preheader.i.i
                                        #   Parent Loop BB1_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB1_16 Depth 3
	movl	$16, %edi
	movq	%r9, %rsi
	.p2align	4, 0x90
.LBB1_16:                               # %vector.body
                                        #   Parent Loop BB1_9 Depth=1
                                        #     Parent Loop BB1_15 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movd	base_dither_matrix(%rsi), %xmm0 # xmm0 = mem[0],zero,zero,zero
	punpcklbw	%xmm8, %xmm0    # xmm0 = xmm0[0],xmm8[0],xmm0[1],xmm8[1],xmm0[2],xmm8[2],xmm0[3],xmm8[3],xmm0[4],xmm8[4],xmm0[5],xmm8[5],xmm0[6],xmm8[6],xmm0[7],xmm8[7]
	punpcklwd	%xmm8, %xmm0    # xmm0 = xmm0[0],xmm8[0],xmm0[1],xmm8[1],xmm0[2],xmm8[2],xmm0[3],xmm8[3]
	paddd	%xmm0, %xmm0
	movdqa	%xmm9, %xmm1
	psubd	%xmm0, %xmm1
	pshuflw	$232, %xmm1, %xmm0      # xmm0 = xmm1[0,2,2,3,4,5,6,7]
	pshufhw	$232, %xmm0, %xmm0      # xmm0 = xmm0[0,1,2,3,4,6,6,7]
	pshufd	$232, %xmm0, %xmm6      # xmm6 = xmm0[0,2,2,3]
	movdqa	%xmm6, %xmm0
	pmulhw	%xmm10, %xmm0
	pmullw	%xmm10, %xmm6
	punpcklwd	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1],xmm6[2],xmm0[2],xmm6[3],xmm0[3]
	movdqa	%xmm6, %xmm0
	psrad	$31, %xmm0
	pshufd	$78, %xmm6, %xmm3       # xmm3 = xmm6[2,3,0,1]
	punpckldq	%xmm0, %xmm6    # xmm6 = xmm6[0],xmm0[0],xmm6[1],xmm0[1]
	pxor	%xmm4, %xmm4
	pcmpgtd	%xmm1, %xmm4
	movdqa	%xmm4, %xmm5
	pxor	%xmm11, %xmm5
	movdqa	%xmm5, 112(%rsp)
	testb	$1, 112(%rsp)
                                        # implicit-def: %XMM0
	je	.LBB1_18
# BB#17:                                # %pred.sdiv.if
                                        #   in Loop: Header=BB1_16 Depth=3
	movd	%xmm6, %rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm0
.LBB1_18:                               # %pred.sdiv.continue
                                        #   in Loop: Header=BB1_16 Depth=3
	movdqa	%xmm3, %xmm2
	psrad	$31, %xmm2
	movdqa	%xmm5, 96(%rsp)
	testb	$1, 100(%rsp)
	je	.LBB1_20
# BB#19:                                # %pred.sdiv.if47
                                        #   in Loop: Header=BB1_16 Depth=3
	pshufd	$78, %xmm6, %xmm7       # xmm7 = xmm6[2,3,0,1]
	movd	%xmm7, %rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm7
	punpcklqdq	%xmm7, %xmm0    # xmm0 = xmm0[0],xmm7[0]
.LBB1_20:                               # %pred.sdiv.continue48
                                        #   in Loop: Header=BB1_16 Depth=3
	punpckldq	%xmm2, %xmm3    # xmm3 = xmm3[0],xmm2[0],xmm3[1],xmm2[1]
	movdqa	%xmm5, 80(%rsp)
	testb	$1, 88(%rsp)
	je	.LBB1_21
# BB#22:                                # %pred.sdiv.if49
                                        #   in Loop: Header=BB1_16 Depth=3
	movd	%xmm3, %rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm2
	movsd	%xmm2, %xmm2            # xmm2 = xmm2[0,1]
	jmp	.LBB1_23
	.p2align	4, 0x90
.LBB1_21:                               #   in Loop: Header=BB1_16 Depth=3
                                        # implicit-def: %XMM2
.LBB1_23:                               # %pred.sdiv.continue50
                                        #   in Loop: Header=BB1_16 Depth=3
	movdqa	%xmm5, 64(%rsp)
	testb	$1, 76(%rsp)
	je	.LBB1_25
# BB#24:                                # %pred.sdiv.if51
                                        #   in Loop: Header=BB1_16 Depth=3
	pshufd	$78, %xmm3, %xmm5       # xmm5 = xmm3[2,3,0,1]
	movd	%xmm5, %rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm5
	punpcklqdq	%xmm5, %xmm2    # xmm2 = xmm2[0],xmm5[0]
.LBB1_25:                               # %pred.sdiv.continue52
                                        #   in Loop: Header=BB1_16 Depth=3
	movdqa	%xmm4, 48(%rsp)
	testb	$1, 48(%rsp)
                                        # implicit-def: %XMM5
	je	.LBB1_27
# BB#26:                                # %pred.sdiv.if53
                                        #   in Loop: Header=BB1_16 Depth=3
	movd	%xmm6, %rax
	negq	%rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm5
.LBB1_27:                               # %pred.sdiv.continue54
                                        #   in Loop: Header=BB1_16 Depth=3
	movdqa	%xmm4, 32(%rsp)
	testb	$1, 36(%rsp)
	je	.LBB1_29
# BB#28:                                # %pred.sdiv.if55
                                        #   in Loop: Header=BB1_16 Depth=3
	pshufd	$78, %xmm6, %xmm6       # xmm6 = xmm6[2,3,0,1]
	movd	%xmm6, %rax
	negq	%rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm6
	punpcklqdq	%xmm6, %xmm5    # xmm5 = xmm5[0],xmm6[0]
.LBB1_29:                               # %pred.sdiv.continue56
                                        #   in Loop: Header=BB1_16 Depth=3
	movdqa	%xmm4, 16(%rsp)
	testb	$1, 24(%rsp)
	je	.LBB1_30
# BB#31:                                # %pred.sdiv.if57
                                        #   in Loop: Header=BB1_16 Depth=3
	movd	%xmm3, %rax
	negq	%rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm6
	movsd	%xmm6, %xmm6            # xmm6 = xmm6[0,1]
	jmp	.LBB1_32
	.p2align	4, 0x90
.LBB1_30:                               #   in Loop: Header=BB1_16 Depth=3
                                        # implicit-def: %XMM6
.LBB1_32:                               # %pred.sdiv.continue58
                                        #   in Loop: Header=BB1_16 Depth=3
	movdqa	%xmm4, (%rsp)
	testb	$1, 12(%rsp)
	je	.LBB1_34
# BB#33:                                # %pred.sdiv.if59
                                        #   in Loop: Header=BB1_16 Depth=3
	pshufd	$78, %xmm3, %xmm3       # xmm3 = xmm3[2,3,0,1]
	movd	%xmm3, %rax
	negq	%rax
	cqto
	idivq	%rbx
	movd	%rax, %xmm3
	punpcklqdq	%xmm3, %xmm6    # xmm6 = xmm6[0],xmm3[0]
.LBB1_34:                               # %pred.sdiv.continue60
                                        #   in Loop: Header=BB1_16 Depth=3
	pshufd	$212, %xmm1, %xmm3      # xmm3 = xmm1[0,1,1,3]
	psllq	$32, %xmm3
	pshufd	$237, %xmm3, %xmm4      # xmm4 = xmm3[1,3,2,3]
	psrad	$31, %xmm3
	pshufd	$237, %xmm3, %xmm3      # xmm3 = xmm3[1,3,2,3]
	punpckldq	%xmm3, %xmm4    # xmm4 = xmm4[0],xmm3[0],xmm4[1],xmm3[1]
	pxor	%xmm12, %xmm4
	movdqa	%xmm12, %xmm3
	pcmpgtd	%xmm4, %xmm3
	pshufd	$160, %xmm3, %xmm7      # xmm7 = xmm3[0,0,2,2]
	pcmpeqd	%xmm12, %xmm4
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pand	%xmm7, %xmm4
	pshufd	$245, %xmm3, %xmm3      # xmm3 = xmm3[1,1,3,3]
	por	%xmm4, %xmm3
	pshufd	$246, %xmm1, %xmm1      # xmm1 = xmm1[2,1,3,3]
	psllq	$32, %xmm1
	pshufd	$237, %xmm1, %xmm4      # xmm4 = xmm1[1,3,2,3]
	psrad	$31, %xmm1
	pshufd	$237, %xmm1, %xmm1      # xmm1 = xmm1[1,3,2,3]
	punpckldq	%xmm1, %xmm4    # xmm4 = xmm4[0],xmm1[0],xmm4[1],xmm1[1]
	pxor	%xmm12, %xmm4
	movdqa	%xmm12, %xmm1
	pcmpgtd	%xmm4, %xmm1
	pshufd	$160, %xmm1, %xmm7      # xmm7 = xmm1[0,0,2,2]
	pcmpeqd	%xmm12, %xmm4
	pshufd	$245, %xmm4, %xmm4      # xmm4 = xmm4[1,1,3,3]
	pand	%xmm7, %xmm4
	pshufd	$245, %xmm1, %xmm1      # xmm1 = xmm1[1,1,3,3]
	por	%xmm4, %xmm1
	pxor	%xmm4, %xmm4
	psubq	%xmm5, %xmm4
	pxor	%xmm5, %xmm5
	psubq	%xmm6, %xmm5
	pand	%xmm1, %xmm5
	pandn	%xmm2, %xmm1
	por	%xmm5, %xmm1
	pand	%xmm3, %xmm4
	pandn	%xmm0, %xmm3
	por	%xmm4, %xmm3
	shufps	$136, %xmm1, %xmm3      # xmm3 = xmm3[0,2],xmm1[0,2]
	movups	%xmm3, (%rcx,%rsi,4)
	addq	$4, %rsi
	addq	$-4, %rdi
	jne	.LBB1_16
# BB#35:                                # %middle.block
                                        #   in Loop: Header=BB1_15 Depth=2
	incq	%r8
	addq	$16, %r9
	cmpq	$16, %r8
	jne	.LBB1_15
# BB#36:                                # %make_odither_array.exit.loopexit.i
                                        #   in Loop: Header=BB1_9 Depth=1
	movl	136(%r14), %eax
.LBB1_37:                               # %make_odither_array.exit.i
                                        #   in Loop: Header=BB1_9 Depth=1
	movq	%rcx, 80(%r15,%r12,8)
	incq	%r12
	movslq	%eax, %rcx
	cmpq	%rcx, %r12
	jl	.LBB1_9
	jmp	.LBB1_46
.LBB1_47:
	movq	(%r14), %rax
	movl	$47, 40(%rax)
	movq	%r14, %rdi
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	jmpq	*(%rax)                 # TAILCALL
.LBB1_40:
	cmpl	$0, 136(%r14)
	jle	.LBB1_46
# BB#41:                                # %.lr.ph.i33
	movq	%r14, %r13
	subq	$-128, %r13
	movl	128(%r14), %ebx
	addl	$2, %ebx
	addq	%rbx, %rbx
	leaq	136(%r14), %r12
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_42:                               # =>This Inner Loop Header: Depth=1
	movq	8(%r14), %rax
	movl	$1, %esi
	movq	%r14, %rdi
	movq	%rbx, %rdx
	callq	*8(%rax)
	movq	%rax, 112(%r15,%rbp,8)
	incq	%rbp
	movslq	136(%r14), %rax
	cmpq	%rax, %rbp
	jl	.LBB1_42
# BB#43:                                # %alloc_fs_workspace.exit
	testl	%eax, %eax
	jle	.LBB1_46
.LBB1_44:                               # %.lr.ph.preheader
	movl	(%r13), %ebx
	addl	$2, %ebx
	addq	%rbx, %rbx
	xorl	%ebp, %ebp
	.p2align	4, 0x90
.LBB1_45:                               # %.lr.ph
                                        # =>This Inner Loop Header: Depth=1
	movq	112(%r15,%rbp,8), %rdi
	movq	%rbx, %rsi
	callq	jzero_far
	incq	%rbp
	movslq	(%r12), %rax
	cmpq	%rax, %rbp
	jl	.LBB1_45
.LBB1_46:                               # %create_odither_tables.exit
	addq	$136, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end1:
	.size	start_pass_1_quant, .Lfunc_end1-start_pass_1_quant
	.cfi_endproc

	.p2align	4, 0x90
	.type	finish_pass_1_quant,@function
finish_pass_1_quant:                    # @finish_pass_1_quant
	.cfi_startproc
# BB#0:
	retq
.Lfunc_end2:
	.size	finish_pass_1_quant, .Lfunc_end2-finish_pass_1_quant
	.cfi_endproc

	.p2align	4, 0x90
	.type	new_color_map_1_quant,@function
new_color_map_1_quant:                  # @new_color_map_1_quant
	.cfi_startproc
# BB#0:
	movq	(%rdi), %rax
	movl	$45, 40(%rax)
	jmpq	*(%rax)                 # TAILCALL
.Lfunc_end3:
	.size	new_color_map_1_quant, .Lfunc_end3-new_color_map_1_quant
	.cfi_endproc

	.p2align	4, 0x90
	.type	create_colorindex,@function
create_colorindex:                      # @create_colorindex
	.cfi_startproc
# BB#0:
	pushq	%r15
.Lcfi26:
	.cfi_def_cfa_offset 16
	pushq	%r14
.Lcfi27:
	.cfi_def_cfa_offset 24
	pushq	%r12
.Lcfi28:
	.cfi_def_cfa_offset 32
	pushq	%rbx
.Lcfi29:
	.cfi_def_cfa_offset 40
	pushq	%rax
.Lcfi30:
	.cfi_def_cfa_offset 48
.Lcfi31:
	.cfi_offset %rbx, -40
.Lcfi32:
	.cfi_offset %r12, -32
.Lcfi33:
	.cfi_offset %r14, -24
.Lcfi34:
	.cfi_offset %r15, -16
	movq	%rdi, %r14
	movq	608(%r14), %r15
	movl	104(%r14), %ebx
	xorl	%eax, %eax
	cmpl	$1, %ebx
	sete	%al
	movl	%eax, 56(%r15)
	movq	8(%r14), %rax
	movl	$766, %ecx              # imm = 0x2FE
	movl	$256, %edx              # imm = 0x100
	cmovel	%ecx, %edx
	movl	136(%r14), %ecx
	movl	$1, %esi
	callq	*16(%rax)
	movq	%rax, %rsi
	movq	%rsi, 48(%r15)
	cmpl	$0, 136(%r14)
	jle	.LBB4_18
# BB#1:                                 # %.lr.ph82
	movl	40(%r15), %r10d
	xorl	%r8d, %r8d
	cmpl	$1, %ebx
	je	.LBB4_9
	jmp	.LBB4_2
	.p2align	4, 0x90
.LBB4_7:                                #   in Loop: Header=BB4_2 Depth=1
	incq	%r8
	movslq	136(%r14), %rax
	cmpq	%rax, %r8
	jge	.LBB4_18
# BB#8:                                 # %..lr.ph82.split_crit_edge
                                        #   in Loop: Header=BB4_2 Depth=1
	movq	48(%r15), %rsi
.LBB4_2:                                # %.lr.ph82.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_3 Depth 2
                                        #       Child Loop BB4_5 Depth 3
	movslq	60(%r15,%r8,4), %r9
	movl	%r10d, %eax
	cltd
	idivl	%r9d
	movq	(%rsi,%r8,8), %r11
	leaq	-1(%r9), %r12
	addq	$254, %r9
	leal	(%r12,%r12), %ecx
	movslq	%ecx, %rdi
	movl	%eax, %r10d
	movq	%r9, %rax
	cqto
	idivq	%rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_3:                                # %.preheader
                                        #   Parent Loop BB4_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_5 Depth 3
	movslq	%eax, %rdx
	cmpq	%rdx, %rcx
	jle	.LBB4_6
# BB#4:                                 # %.lr.ph.preheader
                                        #   in Loop: Header=BB4_3 Depth=2
	leal	3(%rsi,%rsi), %ebx
	.p2align	4, 0x90
.LBB4_5:                                # %.lr.ph
                                        #   Parent Loop BB4_2 Depth=1
                                        #     Parent Loop BB4_3 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebx, %rbx
	movq	%rbx, %rax
	shlq	$8, %rax
	subq	%rbx, %rax
	addq	%r12, %rax
	cqto
	idivq	%rdi
	movslq	%eax, %rdx
	incl	%esi
	addl	$2, %ebx
	cmpq	%rdx, %rcx
	jg	.LBB4_5
.LBB4_6:                                # %._crit_edge
                                        #   in Loop: Header=BB4_3 Depth=2
	movl	%esi, %edx
	imull	%r10d, %edx
	movb	%dl, (%r11,%rcx)
	incq	%rcx
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB4_3
	jmp	.LBB4_7
	.p2align	4, 0x90
.LBB4_15:                               # %.loopexit.us..lr.ph82.split.us_crit_edge
                                        #   in Loop: Header=BB4_9 Depth=1
	movq	48(%r15), %rsi
.LBB4_9:                                # %.lr.ph82.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB4_10 Depth 2
                                        #       Child Loop BB4_17 Depth 3
                                        #     Child Loop BB4_13 Depth 2
	movslq	60(%r15,%r8,4), %r9
	movl	%r10d, %eax
	cltd
	idivl	%r9d
	addq	$255, (%rsi,%r8,8)
	movq	48(%r15), %rcx
	movq	(%rcx,%r8,8), %r11
	leaq	-1(%r9), %r12
	addq	$254, %r9
	leal	(%r12,%r12), %ecx
	movslq	%ecx, %rdi
	movl	%eax, %r10d
	movq	%r9, %rax
	cqto
	idivq	%rdi
	xorl	%ecx, %ecx
	xorl	%esi, %esi
	.p2align	4, 0x90
.LBB4_10:                               # %.preheader.us
                                        #   Parent Loop BB4_9 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB4_17 Depth 3
	movslq	%eax, %rdx
	cmpq	%rdx, %rcx
	jle	.LBB4_11
# BB#16:                                # %.lr.ph.us.preheader
                                        #   in Loop: Header=BB4_10 Depth=2
	leal	3(%rsi,%rsi), %ebx
	.p2align	4, 0x90
.LBB4_17:                               # %.lr.ph.us
                                        #   Parent Loop BB4_9 Depth=1
                                        #     Parent Loop BB4_10 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movslq	%ebx, %rbx
	movq	%rbx, %rax
	shlq	$8, %rax
	subq	%rbx, %rax
	addq	%r12, %rax
	cqto
	idivq	%rdi
	movslq	%eax, %rdx
	incl	%esi
	addl	$2, %ebx
	cmpq	%rdx, %rcx
	jg	.LBB4_17
.LBB4_11:                               # %._crit_edge.us
                                        #   in Loop: Header=BB4_10 Depth=2
	movl	%esi, %edx
	imull	%r10d, %edx
	movb	%dl, (%r11,%rcx)
	incq	%rcx
	cmpq	$256, %rcx              # imm = 0x100
	jne	.LBB4_10
# BB#12:                                # %.preheader72.us
                                        #   in Loop: Header=BB4_9 Depth=1
	leaq	258(%r11), %rax
	xorl	%ecx, %ecx
	.p2align	4, 0x90
.LBB4_13:                               #   Parent Loop BB4_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%r11), %edx
	movb	%dl, -1(%r11,%rcx)
	movzbl	255(%r11), %edx
	movb	%dl, -2(%rax)
	movzbl	(%r11), %edx
	movb	%dl, -2(%r11,%rcx)
	movzbl	255(%r11), %edx
	movb	%dl, -1(%rax)
	movzbl	(%r11), %edx
	movb	%dl, -3(%r11,%rcx)
	movzbl	255(%r11), %edx
	movb	%dl, (%rax)
	addq	$-3, %rcx
	addq	$3, %rax
	cmpq	$-255, %rcx
	jne	.LBB4_13
# BB#14:                                # %.loopexit.us
                                        #   in Loop: Header=BB4_9 Depth=1
	incq	%r8
	movslq	136(%r14), %rax
	cmpq	%rax, %r8
	jl	.LBB4_15
.LBB4_18:                               # %._crit_edge83
	addq	$8, %rsp
	popq	%rbx
	popq	%r12
	popq	%r14
	popq	%r15
	retq
.Lfunc_end4:
	.size	create_colorindex, .Lfunc_end4-create_colorindex
	.cfi_endproc

	.p2align	4, 0x90
	.type	color_quantize3,@function
color_quantize3:                        # @color_quantize3
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi35:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi36:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi37:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi38:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi39:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi40:
	.cfi_def_cfa_offset 56
.Lcfi41:
	.cfi_offset %rbx, -56
.Lcfi42:
	.cfi_offset %r12, -48
.Lcfi43:
	.cfi_offset %r13, -40
.Lcfi44:
	.cfi_offset %r14, -32
.Lcfi45:
	.cfi_offset %r15, -24
.Lcfi46:
	.cfi_offset %rbp, -16
	testl	%ecx, %ecx
	jle	.LBB5_9
# BB#1:
	movl	128(%rdi), %r8d
	testl	%r8d, %r8d
	je	.LBB5_9
# BB#2:                                 # %.lr.ph51.split.preheader
	movq	608(%rdi), %rax
	movq	48(%rax), %rdi
	movq	(%rdi), %r9
	movq	8(%rdi), %r13
	movq	16(%rdi), %r11
	movl	%ecx, %r10d
	movl	%r8d, %r14d
	andl	$1, %r14d
	leal	-1(%r8), %eax
	movl	%eax, -4(%rsp)          # 4-byte Spill
	xorl	%r15d, %r15d
	.p2align	4, 0x90
.LBB5_3:                                # %.lr.ph51.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB5_7 Depth 2
	testl	%r14d, %r14d
	movq	(%rsi,%r15,8), %rcx
	movq	(%rdx,%r15,8), %rbp
	jne	.LBB5_5
# BB#4:                                 #   in Loop: Header=BB5_3 Depth=1
	movl	%r8d, %ebx
	cmpl	$1, %r8d
	jne	.LBB5_7
	jmp	.LBB5_8
	.p2align	4, 0x90
.LBB5_5:                                #   in Loop: Header=BB5_3 Depth=1
	movzbl	(%rcx), %r12d
	movzbl	1(%rcx), %ebx
	movb	(%r13,%rbx), %bl
	addb	(%r9,%r12), %bl
	movzbl	2(%rcx), %r12d
	addq	$3, %rcx
	addb	(%r11,%r12), %bl
	movb	%bl, (%rbp)
	incq	%rbp
	movl	-4(%rsp), %ebx          # 4-byte Reload
	cmpl	$1, %r8d
	je	.LBB5_8
	.p2align	4, 0x90
.LBB5_7:                                #   Parent Loop BB5_3 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movzbl	(%rcx), %r12d
	movzbl	1(%rcx), %edi
	movzbl	(%r13,%rdi), %eax
	addb	(%r9,%r12), %al
	movzbl	2(%rcx), %edi
	addb	(%r11,%rdi), %al
	movb	%al, (%rbp)
	movzbl	3(%rcx), %r12d
	movzbl	4(%rcx), %edi
	movzbl	(%r13,%rdi), %eax
	addb	(%r9,%r12), %al
	movzbl	5(%rcx), %edi
	addb	(%r11,%rdi), %al
	movb	%al, 1(%rbp)
	addq	$6, %rcx
	addq	$2, %rbp
	addl	$-2, %ebx
	jne	.LBB5_7
.LBB5_8:                                # %._crit_edge
                                        #   in Loop: Header=BB5_3 Depth=1
	incq	%r15
	cmpq	%r10, %r15
	jne	.LBB5_3
.LBB5_9:                                # %._crit_edge52
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end5:
	.size	color_quantize3, .Lfunc_end5-color_quantize3
	.cfi_endproc

	.p2align	4, 0x90
	.type	color_quantize,@function
color_quantize:                         # @color_quantize
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi47:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi48:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi49:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi50:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi51:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi52:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi53:
	.cfi_def_cfa_offset 96
.Lcfi54:
	.cfi_offset %rbx, -56
.Lcfi55:
	.cfi_offset %r12, -48
.Lcfi56:
	.cfi_offset %r13, -40
.Lcfi57:
	.cfi_offset %r14, -32
.Lcfi58:
	.cfi_offset %r15, -24
.Lcfi59:
	.cfi_offset %rbp, -16
	movq	%rdx, (%rsp)            # 8-byte Spill
	movq	%rsi, 24(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB6_19
# BB#1:
	movl	128(%rdi), %eax
	movq	%rax, 8(%rsp)           # 8-byte Spill
	testl	%eax, %eax
	je	.LBB6_19
# BB#2:                                 # %.lr.ph56.split
	movl	136(%rdi), %r14d
	testl	%r14d, %r14d
	jle	.LBB6_3
# BB#9:                                 # %.lr.ph56.split.split.us.preheader
	movq	608(%rdi), %rax
	movq	48(%rax), %r10
	leal	-1(%r14), %r15d
	incq	%r15
	movl	%ecx, %eax
	movq	%rax, 16(%rsp)          # 8-byte Spill
	movl	%r14d, %r13d
	andl	$1, %r13d
	leaq	8(%r10), %r11
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB6_10:                               # %.lr.ph56.split.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB6_11 Depth 2
                                        #       Child Loop BB6_16 Depth 3
	movq	24(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rdx,8), %rcx
	movq	(%rsp), %rax            # 8-byte Reload
	movq	%rdx, 32(%rsp)          # 8-byte Spill
	movq	(%rax,%rdx,8), %rbx
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %ebp
	.p2align	4, 0x90
.LBB6_11:                               # %.preheader.us.us91
                                        #   Parent Loop BB6_10 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB6_16 Depth 3
	testq	%r13, %r13
	jne	.LBB6_13
# BB#12:                                #   in Loop: Header=BB6_11 Depth=2
	xorl	%esi, %esi
	xorl	%edi, %edi
	movq	%rcx, %rdx
	cmpl	$1, %r14d
	jne	.LBB6_15
	jmp	.LBB6_17
	.p2align	4, 0x90
.LBB6_13:                               #   in Loop: Header=BB6_11 Depth=2
	movq	(%r10), %rax
	leaq	1(%rcx), %rdx
	movzbl	(%rcx), %esi
	movzbl	(%rax,%rsi), %edi
	movl	$1, %esi
	cmpl	$1, %r14d
	je	.LBB6_17
.LBB6_15:                               # %.preheader.us.us91.new
                                        #   in Loop: Header=BB6_11 Depth=2
	movq	%r14, %rax
	subq	%rsi, %rax
	leaq	(%r11,%rsi,8), %r8
	.p2align	4, 0x90
.LBB6_16:                               #   Parent Loop BB6_10 Depth=1
                                        #     Parent Loop BB6_11 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdx), %esi
	movq	-8(%r8), %r12
	movq	(%r8), %r9
	movzbl	(%r12,%rsi), %esi
	addl	%edi, %esi
	movzbl	1(%rdx), %edi
	movzbl	(%r9,%rdi), %edi
	addl	%esi, %edi
	addq	$16, %r8
	addq	$2, %rdx
	addq	$-2, %rax
	jne	.LBB6_16
.LBB6_17:                               # %._crit_edge.us.us98
                                        #   in Loop: Header=BB6_11 Depth=2
	addq	%r15, %rcx
	movb	%dil, (%rbx)
	incq	%rbx
	decl	%ebp
	jne	.LBB6_11
# BB#18:                                # %._crit_edge53.us-lcssa.us.us105
                                        #   in Loop: Header=BB6_10 Depth=1
	movq	32(%rsp), %rdx          # 8-byte Reload
	incq	%rdx
	cmpq	16(%rsp), %rdx          # 8-byte Folded Reload
	jne	.LBB6_10
	jmp	.LBB6_19
.LBB6_3:                                # %.lr.ph56.split.split.preheader
	movq	8(%rsp), %r12           # 8-byte Reload
	decl	%r12d
	incq	%r12
	movl	%ecx, %r15d
	leaq	-1(%r15), %r14
	movq	%r15, %r13
	xorl	%ebp, %ebp
	andq	$7, %r13
	je	.LBB6_6
# BB#4:                                 # %.lr.ph56.split.split.prol.preheader
	movq	(%rsp), %rbx            # 8-byte Reload
	.p2align	4, 0x90
.LBB6_5:                                # %.lr.ph56.split.split.prol
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx,%rbp,8), %rdi
	xorl	%esi, %esi
	movq	%r12, %rdx
	callq	memset
	incq	%rbp
	cmpq	%rbp, %r13
	jne	.LBB6_5
.LBB6_6:                                # %.lr.ph56.split.split.prol.loopexit
	cmpq	$7, %r14
	jb	.LBB6_19
# BB#7:                                 # %.lr.ph56.split.split.preheader.new
	subq	%rbp, %r15
	movq	(%rsp), %rax            # 8-byte Reload
	leaq	56(%rax,%rbp,8), %rbx
	.p2align	4, 0x90
.LBB6_8:                                # %.lr.ph56.split.split
                                        # =>This Inner Loop Header: Depth=1
	movq	-56(%rbx), %rdi
	xorl	%esi, %esi
	movq	%r12, %rbp
	movq	%rbp, %rdx
	callq	memset
	movq	-48(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-40(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-32(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-24(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-16(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	-8(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	movq	(%rbx), %rdi
	xorl	%esi, %esi
	movq	%rbp, %rdx
	callq	memset
	addq	$64, %rbx
	addq	$-8, %r15
	jne	.LBB6_8
.LBB6_19:                               # %._crit_edge57
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end6:
	.size	color_quantize, .Lfunc_end6-color_quantize
	.cfi_endproc

	.p2align	4, 0x90
	.type	quantize3_ord_dither,@function
quantize3_ord_dither:                   # @quantize3_ord_dither
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi60:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi61:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi62:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi63:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi64:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi65:
	.cfi_def_cfa_offset 56
.Lcfi66:
	.cfi_offset %rbx, -56
.Lcfi67:
	.cfi_offset %r12, -48
.Lcfi68:
	.cfi_offset %r13, -40
.Lcfi69:
	.cfi_offset %r14, -32
.Lcfi70:
	.cfi_offset %r15, -24
.Lcfi71:
	.cfi_offset %rbp, -16
                                        # kill: %ECX<def> %ECX<kill> %RCX<def>
	movq	%rdx, -24(%rsp)         # 8-byte Spill
	movq	%rsi, -32(%rsp)         # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB7_12
# BB#1:                                 # %.lr.ph80
	movq	608(%rdi), %rbp
	movl	128(%rdi), %eax
	movl	76(%rbp), %edi
	movl	%eax, -56(%rsp)         # 4-byte Spill
	testl	%eax, %eax
	je	.LBB7_2
# BB#8:                                 # %.lr.ph80.split.preheader
	movq	48(%rbp), %rax
	movq	(%rax), %r13
	movq	8(%rax), %r14
	movq	16(%rax), %r11
	movl	%ecx, %eax
	movq	%rax, -48(%rsp)         # 8-byte Spill
	xorl	%edx, %edx
	movq	%rbp, -40(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB7_9:                                # %.lr.ph80.split
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB7_10 Depth 2
	movq	-32(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rdx,8), %rcx
	movq	-24(%rsp), %rax         # 8-byte Reload
	movq	%rdx, -16(%rsp)         # 8-byte Spill
	movq	(%rax,%rdx,8), %rbx
	movq	80(%rbp), %rax
	movq	%rax, -8(%rsp)          # 8-byte Spill
	movq	88(%rbp), %r12
	movl	%edi, -52(%rsp)         # 4-byte Spill
	movslq	%edi, %r15
	movq	96(%rbp), %r8
	movl	-56(%rsp), %esi         # 4-byte Reload
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB7_10:                               #   Parent Loop BB7_9 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	movq	%r14, %rax
	movzbl	(%rcx), %r14d
	movl	%edx, %r9d
	movq	%r15, %rbp
	shlq	$6, %rbp
	movq	-8(%rsp), %rdi          # 8-byte Reload
	leaq	(%rdi,%rbp), %r10
	movslq	(%r10,%r9,4), %rdi
	addq	%r14, %rdi
	movq	%rax, %r14
	movzbl	1(%rcx), %r10d
	leaq	(%r12,%rbp), %rax
	movslq	(%rax,%r9,4), %rax
	addq	%r10, %rax
	movzbl	(%r14,%rax), %eax
	addb	(%r13,%rdi), %al
	addq	%r8, %rbp
	movslq	(%rbp,%r9,4), %rdi
	movzbl	2(%rcx), %ebp
	addq	%rbp, %rdi
	addb	(%r11,%rdi), %al
	movb	%al, (%rbx)
	incq	%rbx
	incl	%edx
	andl	$15, %edx
	addq	$3, %rcx
	decl	%esi
	jne	.LBB7_10
# BB#11:                                # %._crit_edge
                                        #   in Loop: Header=BB7_9 Depth=1
	movl	-52(%rsp), %edi         # 4-byte Reload
	incl	%edi
	andl	$15, %edi
	movq	-40(%rsp), %rbp         # 8-byte Reload
	movl	%edi, 76(%rbp)
	movq	-16(%rsp), %rdx         # 8-byte Reload
	incq	%rdx
	cmpq	-48(%rsp), %rdx         # 8-byte Folded Reload
	jne	.LBB7_9
	jmp	.LBB7_12
.LBB7_2:                                # %.lr.ph80.split.us.preheader
	leal	-1(%rcx), %eax
	movl	%ecx, %esi
	xorl	%edx, %edx
	andl	$7, %esi
	je	.LBB7_4
	.p2align	4, 0x90
.LBB7_3:                                # %.lr.ph80.split.us.prol
                                        # =>This Inner Loop Header: Depth=1
	incl	%edi
	andl	$15, %edi
	incl	%edx
	cmpl	%edx, %esi
	jne	.LBB7_3
.LBB7_4:                                # %.lr.ph80.split.us.prol.loopexit
	cmpl	$7, %eax
	jb	.LBB7_7
# BB#5:                                 # %.lr.ph80.split.us.preheader.new
	subl	%edx, %ecx
	.p2align	4, 0x90
.LBB7_6:                                # %.lr.ph80.split.us
                                        # =>This Inner Loop Header: Depth=1
	addl	$8, %edi
	andl	$15, %edi
	addl	$-8, %ecx
	jne	.LBB7_6
.LBB7_7:                                # %._crit_edge81.loopexit
	movl	%edi, 76(%rbp)
.LBB7_12:                               # %._crit_edge81
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end7:
	.size	quantize3_ord_dither, .Lfunc_end7-quantize3_ord_dither
	.cfi_endproc

	.p2align	4, 0x90
	.type	quantize_ord_dither,@function
quantize_ord_dither:                    # @quantize_ord_dither
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi72:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi73:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi74:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi75:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi76:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi77:
	.cfi_def_cfa_offset 56
	subq	$40, %rsp
.Lcfi78:
	.cfi_def_cfa_offset 96
.Lcfi79:
	.cfi_offset %rbx, -56
.Lcfi80:
	.cfi_offset %r12, -48
.Lcfi81:
	.cfi_offset %r13, -40
.Lcfi82:
	.cfi_offset %r14, -32
.Lcfi83:
	.cfi_offset %r15, -24
.Lcfi84:
	.cfi_offset %rbp, -16
	movq	%rdx, %rbx
	movq	%rsi, 32(%rsp)          # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB8_13
# BB#1:                                 # %.lr.ph75
	movq	608(%rdi), %r14
	movslq	136(%rdi), %r15
	movl	128(%rdi), %r12d
	testl	%r15d, %r15d
	movl	%ecx, %eax
	jle	.LBB8_12
# BB#2:                                 # %.lr.ph75.split.us.preheader
	movl	%r15d, %ecx
	movq	%rcx, 16(%rsp)          # 8-byte Spill
	movl	%r12d, %ecx
	andl	$1, %ecx
	movl	%ecx, 4(%rsp)           # 4-byte Spill
	leal	-1(%r12), %ecx
	movl	%ecx, (%rsp)            # 4-byte Spill
	xorl	%r13d, %r13d
	movq	%rbx, 24(%rsp)          # 8-byte Spill
	movq	%rax, 8(%rsp)           # 8-byte Spill
	.p2align	4, 0x90
.LBB8_3:                                # %.lr.ph75.split.us
                                        # =>This Loop Header: Depth=1
                                        #     Child Loop BB8_5 Depth 2
                                        #       Child Loop BB8_9 Depth 3
	movq	(%rbx,%r13,8), %rdi
	movq	%r12, %rsi
	callq	jzero_far
	testl	%r12d, %r12d
	movslq	76(%r14), %r9
	je	.LBB8_11
# BB#4:                                 # %.lr.ph.us82.preheader
                                        #   in Loop: Header=BB8_3 Depth=1
	xorl	%r8d, %r8d
	.p2align	4, 0x90
.LBB8_5:                                # %.lr.ph.us82
                                        #   Parent Loop BB8_3 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB8_9 Depth 3
	movq	32(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%r13,8), %rdx
	addq	%r8, %rdx
	xorl	%ecx, %ecx
	cmpl	$0, 4(%rsp)             # 4-byte Folded Reload
	movq	(%rbx,%r13,8), %rsi
	movq	48(%r14), %rax
	movq	(%rax,%r8,8), %r11
	movq	80(%r14,%r8,8), %r10
	jne	.LBB8_7
# BB#6:                                 #   in Loop: Header=BB8_5 Depth=2
	movl	%r12d, %eax
	cmpl	$1, %r12d
	jne	.LBB8_9
	jmp	.LBB8_10
	.p2align	4, 0x90
.LBB8_7:                                #   in Loop: Header=BB8_5 Depth=2
	movzbl	(%rdx), %eax
	movq	%r9, %rcx
	shlq	$6, %rcx
	movslq	(%r10,%rcx), %rcx
	addq	%rax, %rcx
	movb	(%r11,%rcx), %al
	addb	%al, (%rsi)
	addq	%r15, %rdx
	incq	%rsi
	movl	$1, %ecx
	movl	(%rsp), %eax            # 4-byte Reload
	cmpl	$1, %r12d
	je	.LBB8_10
	.p2align	4, 0x90
.LBB8_9:                                #   Parent Loop BB8_3 Depth=1
                                        #     Parent Loop BB8_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movzbl	(%rdx), %ebx
	movl	%ecx, %ebp
	movq	%r9, %rdi
	shlq	$6, %rdi
	addq	%r10, %rdi
	movslq	(%rdi,%rbp,4), %rbp
	addq	%rbx, %rbp
	movzbl	(%r11,%rbp), %ebx
	addb	%bl, (%rsi)
	leal	1(%rcx), %ebx
	andl	$15, %ebx
	movzbl	(%rdx,%r15), %ebp
	addq	%r15, %rdx
	movslq	(%rdi,%rbx,4), %rdi
	addq	%rbp, %rdi
	movzbl	(%r11,%rdi), %ebx
	addb	%bl, 1(%rsi)
	addq	%r15, %rdx
	addl	$2, %ecx
	andl	$15, %ecx
	addq	$2, %rsi
	addl	$-2, %eax
	jne	.LBB8_9
.LBB8_10:                               # %._crit_edge.us83
                                        #   in Loop: Header=BB8_5 Depth=2
	incq	%r8
	cmpq	16(%rsp), %r8           # 8-byte Folded Reload
	movq	24(%rsp), %rbx          # 8-byte Reload
	jne	.LBB8_5
.LBB8_11:                               # %._crit_edge72.us
                                        #   in Loop: Header=BB8_3 Depth=1
	incl	%r9d
	andl	$15, %r9d
	movl	%r9d, 76(%r14)
	incq	%r13
	movq	8(%rsp), %rax           # 8-byte Reload
	cmpq	%rax, %r13
	jne	.LBB8_3
	jmp	.LBB8_13
	.p2align	4, 0x90
.LBB8_12:                               # %.lr.ph75.split
                                        # =>This Inner Loop Header: Depth=1
	movq	(%rbx), %rdi
	movq	%r12, %rsi
	movq	%rax, %rbp
	callq	jzero_far
	movl	76(%r14), %eax
	incl	%eax
	andl	$15, %eax
	movl	%eax, 76(%r14)
	movq	%rbp, %rax
	addq	$8, %rbx
	decq	%rax
	jne	.LBB8_12
.LBB8_13:                               # %._crit_edge76
	addq	$40, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end8:
	.size	quantize_ord_dither, .Lfunc_end8-quantize_ord_dither
	.cfi_endproc

	.p2align	4, 0x90
	.type	quantize_fs_dither,@function
quantize_fs_dither:                     # @quantize_fs_dither
	.cfi_startproc
# BB#0:
	pushq	%rbp
.Lcfi85:
	.cfi_def_cfa_offset 16
	pushq	%r15
.Lcfi86:
	.cfi_def_cfa_offset 24
	pushq	%r14
.Lcfi87:
	.cfi_def_cfa_offset 32
	pushq	%r13
.Lcfi88:
	.cfi_def_cfa_offset 40
	pushq	%r12
.Lcfi89:
	.cfi_def_cfa_offset 48
	pushq	%rbx
.Lcfi90:
	.cfi_def_cfa_offset 56
	subq	$168, %rsp
.Lcfi91:
	.cfi_def_cfa_offset 224
.Lcfi92:
	.cfi_offset %rbx, -56
.Lcfi93:
	.cfi_offset %r12, -48
.Lcfi94:
	.cfi_offset %r13, -40
.Lcfi95:
	.cfi_offset %r14, -32
.Lcfi96:
	.cfi_offset %r15, -24
.Lcfi97:
	.cfi_offset %rbp, -16
	movq	%rdx, 40(%rsp)          # 8-byte Spill
	movq	%rsi, 128(%rsp)         # 8-byte Spill
	testl	%ecx, %ecx
	jle	.LBB9_19
# BB#1:                                 # %.lr.ph140
	movq	608(%rdi), %rbx
	movl	128(%rdi), %eax
	movl	136(%rdi), %edx
	movq	408(%rdi), %rsi
	movq	%rsi, 112(%rsp)         # 8-byte Spill
	leal	-1(%rax), %esi
	movl	%esi, %edi
	imull	%edx, %edi
	movq	%rdi, 80(%rsp)          # 8-byte Spill
	movl	%edx, %edi
	negl	%edi
	movl	%edi, 36(%rsp)          # 4-byte Spill
	movq	%rax, 16(%rsp)          # 8-byte Spill
	leal	1(%rax), %r14d
	movq	%rsi, 88(%rsp)          # 8-byte Spill
	leaq	2(%rsi,%rsi), %rax
	movq	%rax, 96(%rsp)          # 8-byte Spill
	movl	%ecx, %eax
	movq	%rax, 72(%rsp)          # 8-byte Spill
	leaq	-1(%rdx), %rax
	movq	%rax, 64(%rsp)          # 8-byte Spill
	movq	%rdx, 8(%rsp)           # 8-byte Spill
                                        # kill: %EDX<def> %EDX<kill> %RDX<kill> %RDX<def>
	andl	$3, %edx
	movq	%rdx, 56(%rsp)          # 8-byte Spill
	leaq	136(%rbx), %rax
	movq	%rax, 48(%rsp)          # 8-byte Spill
	xorl	%ebp, %ebp
	movq	%r14, 104(%rsp)         # 8-byte Spill
	movq	%rbx, 120(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB9_2:                                # =>This Loop Header: Depth=1
                                        #     Child Loop BB9_5 Depth 2
                                        #       Child Loop BB9_16 Depth 3
                                        #     Child Loop BB9_10 Depth 2
                                        #     Child Loop BB9_13 Depth 2
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %rdi
	movq	16(%rsp), %rsi          # 8-byte Reload
	callq	jzero_far
	cmpl	$0, 8(%rsp)             # 4-byte Folded Reload
	jle	.LBB9_18
# BB#3:                                 # %.lr.ph136
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpl	$0, 16(%rsp)            # 4-byte Folded Reload
	je	.LBB9_7
# BB#4:                                 # %.lr.ph136.split.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	xorl	%edi, %edi
	movq	%rbp, 136(%rsp)         # 8-byte Spill
	.p2align	4, 0x90
.LBB9_5:                                # %.lr.ph136.split
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Loop Header: Depth=2
                                        #       Child Loop BB9_16 Depth 3
	movq	128(%rsp), %rax         # 8-byte Reload
	movq	(%rax,%rbp,8), %rcx
	addq	%rdi, %rcx
	movq	40(%rsp), %rax          # 8-byte Reload
	movq	(%rax,%rbp,8), %r10
	cmpl	$0, 144(%rbx)
	movq	112(%rbx,%rdi,8), %r13
	je	.LBB9_6
# BB#14:                                #   in Loop: Header=BB9_5 Depth=2
	addq	80(%rsp), %rcx          # 8-byte Folded Reload
	addq	88(%rsp), %r10          # 8-byte Folded Reload
	leaq	(%r13,%r14,2), %r13
	movq	$-1, 24(%rsp)           # 8-byte Folded Spill
	movl	36(%rsp), %edx          # 4-byte Reload
	jmp	.LBB9_15
	.p2align	4, 0x90
.LBB9_6:                                #   in Loop: Header=BB9_5 Depth=2
	movl	$1, %eax
	movq	%rax, 24(%rsp)          # 8-byte Spill
	movq	8(%rsp), %rax           # 8-byte Reload
	movl	%eax, %edx
.LBB9_15:                               # %.lr.ph
                                        #   in Loop: Header=BB9_5 Depth=2
	movq	32(%rbx), %rax
	movq	48(%rbx), %rsi
	movq	(%rsi,%rdi,8), %rsi
	movq	%rsi, 160(%rsp)         # 8-byte Spill
	movq	%rdi, 152(%rsp)         # 8-byte Spill
	movq	(%rax,%rdi,8), %rax
	movslq	%edx, %rbp
	movq	96(%rsp), %rsi          # 8-byte Reload
	movq	24(%rsp), %rdx          # 8-byte Reload
	imulq	%rdx, %rsi
	movq	%rsi, 144(%rsp)         # 8-byte Spill
	leaq	(%r13,%rdx,2), %rsi
	xorl	%edx, %edx
	xorl	%r8d, %r8d
	xorl	%r12d, %r12d
	movq	16(%rsp), %rdi          # 8-byte Reload
	movl	%edi, %r14d
	xorl	%r15d, %r15d
	movq	112(%rsp), %r11         # 8-byte Reload
	.p2align	4, 0x90
.LBB9_16:                               #   Parent Loop BB9_2 Depth=1
                                        #     Parent Loop BB9_5 Depth=2
                                        # =>    This Inner Loop Header: Depth=3
	movswl	(%rsi,%rdx,2), %edi
	leal	8(%r8,%rdi), %edi
	sarl	$4, %edi
	movzbl	(%rcx), %ebx
	movslq	%edi, %rdi
	addq	%rbx, %rdi
	movzbl	(%r11,%rdi), %r9d
	movq	160(%rsp), %rdi         # 8-byte Reload
	movzbl	(%rdi,%r9), %edi
	addb	%dil, (%r10,%rdx)
	movzbl	(%rax,%rdi), %edi
	subl	%edi, %r9d
	leal	(%r9,%r9,2), %edi
	addl	%r15d, %edi
	movw	%di, (%r13,%rdx,2)
	leal	(%r9,%r9,4), %r15d
	addl	%r12d, %r15d
	leal	(,%r9,8), %r8d
	subl	%r9d, %r8d
	addq	%rbp, %rcx
	addq	24(%rsp), %rdx          # 8-byte Folded Reload
	decl	%r14d
	movl	%r9d, %r12d
	jne	.LBB9_16
# BB#17:                                # %._crit_edge
                                        #   in Loop: Header=BB9_5 Depth=2
	movq	144(%rsp), %rax         # 8-byte Reload
	movw	%r15w, (%r13,%rax)
	movq	152(%rsp), %rdi         # 8-byte Reload
	incq	%rdi
	cmpq	8(%rsp), %rdi           # 8-byte Folded Reload
	movq	120(%rsp), %rbx         # 8-byte Reload
	movq	104(%rsp), %r14         # 8-byte Reload
	movq	136(%rsp), %rbp         # 8-byte Reload
	jne	.LBB9_5
	jmp	.LBB9_18
	.p2align	4, 0x90
.LBB9_7:                                # %.lr.ph136.split.us.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	56(%rsp), %rdi          # 8-byte Reload
	testq	%rdi, %rdi
	movl	144(%rbx), %eax
	je	.LBB9_8
# BB#9:                                 # %.lr.ph136.split.us.prol.preheader
                                        #   in Loop: Header=BB9_2 Depth=1
	xorl	%edx, %edx
	.p2align	4, 0x90
.LBB9_10:                               # %.lr.ph136.split.us.prol
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%eax, %eax
	movq	112(%rbx,%rdx,8), %rcx
	leaq	(%rcx,%r14,2), %rsi
	cmoveq	%rcx, %rsi
	movw	$0, (%rsi)
	incq	%rdx
	cmpq	%rdx, %rdi
	jne	.LBB9_10
	jmp	.LBB9_11
.LBB9_8:                                #   in Loop: Header=BB9_2 Depth=1
	xorl	%edx, %edx
.LBB9_11:                               # %.lr.ph136.split.us.prol.loopexit
                                        #   in Loop: Header=BB9_2 Depth=1
	cmpq	$3, 64(%rsp)            # 8-byte Folded Reload
	jb	.LBB9_18
# BB#12:                                # %.lr.ph136.split.us.preheader.new
                                        #   in Loop: Header=BB9_2 Depth=1
	movq	8(%rsp), %rcx           # 8-byte Reload
	subq	%rdx, %rcx
	movq	48(%rsp), %rsi          # 8-byte Reload
	leaq	(%rsi,%rdx,8), %rdx
	.p2align	4, 0x90
.LBB9_13:                               # %.lr.ph136.split.us
                                        #   Parent Loop BB9_2 Depth=1
                                        # =>  This Inner Loop Header: Depth=2
	testl	%eax, %eax
	movq	-24(%rdx), %rsi
	leaq	(%rsi,%r14,2), %rdi
	cmoveq	%rsi, %rdi
	movw	$0, (%rdi)
	movq	-16(%rdx), %rsi
	leaq	(%rsi,%r14,2), %rdi
	cmoveq	%rsi, %rdi
	movw	$0, (%rdi)
	movq	-8(%rdx), %rsi
	leaq	(%rsi,%r14,2), %rdi
	cmoveq	%rsi, %rdi
	movw	$0, (%rdi)
	movq	(%rdx), %rsi
	leaq	(%rsi,%r14,2), %rdi
	cmoveq	%rsi, %rdi
	movw	$0, (%rdi)
	addq	$32, %rdx
	addq	$-4, %rcx
	jne	.LBB9_13
	.p2align	4, 0x90
.LBB9_18:                               # %._crit_edge137
                                        #   in Loop: Header=BB9_2 Depth=1
	xorl	%eax, %eax
	cmpl	$0, 144(%rbx)
	sete	%al
	movl	%eax, 144(%rbx)
	incq	%rbp
	cmpq	72(%rsp), %rbp          # 8-byte Folded Reload
	jne	.LBB9_2
.LBB9_19:                               # %._crit_edge141
	addq	$168, %rsp
	popq	%rbx
	popq	%r12
	popq	%r13
	popq	%r14
	popq	%r15
	popq	%rbp
	retq
.Lfunc_end9:
	.size	quantize_fs_dither, .Lfunc_end9-quantize_fs_dither
	.cfi_endproc

	.type	base_dither_matrix,@object # @base_dither_matrix
	.section	.rodata,"a",@progbits
	.p2align	4
base_dither_matrix:
	.ascii	"\000\3000\360\f\314<\374\003\3033\363\017\317?\377"
	.ascii	"\200@\260p\214L\274|\203C\263s\217O\277\177"
	.ascii	" \340\020\320,\354\034\334#\343\023\323/\357\037\337"
	.ascii	"\240`\220P\254l\234\\\243c\223S\257o\237_"
	.ascii	"\b\3108\370\004\3044\364\013\313;\373\007\3077\367"
	.ascii	"\210H\270x\204D\264t\213K\273{\207G\267w"
	.ascii	"(\350\030\330$\344\024\324+\353\033\333'\347\027\327"
	.ascii	"\250h\230X\244d\224T\253k\233[\247g\227W"
	.ascii	"\002\3022\362\016\316>\376\001\3011\361\r\315=\375"
	.ascii	"\202B\262r\216N\276~\201A\261q\215M\275}"
	.ascii	"\"\342\022\322.\356\036\336!\341\021\321-\355\035\335"
	.ascii	"\242b\222R\256n\236^\241a\221Q\255m\235]"
	.ascii	"\n\312:\372\006\3066\366\t\3119\371\005\3055\365"
	.ascii	"\212J\272z\206F\266v\211I\271y\205E\265u"
	.ascii	"*\352\032\332&\346\026\326)\351\031\331%\345\025\325"
	.ascii	"\252j\232Z\246f\226V\251i\231Y\245e\225U"
	.size	base_dither_matrix, 256

	.type	select_ncolors.RGB_order,@object # @select_ncolors.RGB_order
	.p2align	2
select_ncolors.RGB_order:
	.long	1                       # 0x1
	.long	0                       # 0x0
	.long	2                       # 0x2
	.size	select_ncolors.RGB_order, 12


	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git 30d74bbb0bfbe5f93f14e4ed5e077ee1c1927dec) (https://github.com/aqjune/llvm-intptr.git e7895ab560ab4e8430b644093c8607e12e5adf7a)"
	.section	".note.GNU-stack","",@progbits
