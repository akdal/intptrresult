	.text
	.file	"fibo.bc"
	.globl	_Z3fibm
	.p2align	4, 0x90
	.type	_Z3fibm,@function
_Z3fibm:                                # @_Z3fibm
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi0:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi1:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi2:
	.cfi_def_cfa_offset 32
.Lcfi3:
	.cfi_offset %rbx, -24
.Lcfi4:
	.cfi_offset %r14, -16
	movq	%rdi, %rbx
	movl	$1, %r14d
	cmpq	$2, %rbx
	jb	.LBB0_2
	.p2align	4, 0x90
.LBB0_1:                                # %tailrecurse
                                        # =>This Inner Loop Header: Depth=1
	leaq	-2(%rbx), %rdi
	callq	_Z3fibm
	decq	%rbx
	addq	%rax, %r14
	cmpq	$1, %rbx
	ja	.LBB0_1
.LBB0_2:                                # %tailrecurse._crit_edge
	movq	%r14, %rax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.Lfunc_end0:
	.size	_Z3fibm, .Lfunc_end0-_Z3fibm
	.cfi_endproc

	.globl	main
	.p2align	4, 0x90
	.type	main,@function
main:                                   # @main
	.cfi_startproc
# BB#0:
	pushq	%r14
.Lcfi5:
	.cfi_def_cfa_offset 16
	pushq	%rbx
.Lcfi6:
	.cfi_def_cfa_offset 24
	pushq	%rax
.Lcfi7:
	.cfi_def_cfa_offset 32
.Lcfi8:
	.cfi_offset %rbx, -24
.Lcfi9:
	.cfi_offset %r14, -16
	cmpl	$2, %edi
	jne	.LBB1_1
# BB#2:
	movq	8(%rsi), %rdi
	xorl	%esi, %esi
	movl	$10, %edx
	callq	strtol
	movslq	%eax, %rdi
	jmp	.LBB1_3
.LBB1_1:
	movl	$43, %edi
.LBB1_3:
	callq	_Z3fibm
	movl	$_ZSt4cout, %edi
	movq	%rax, %rsi
	callq	_ZNSo9_M_insertImEERSoT_
	movq	%rax, %r14
	movq	(%r14), %rax
	movq	-24(%rax), %rax
	movq	240(%r14,%rax), %rbx
	testq	%rbx, %rbx
	je	.LBB1_8
# BB#4:                                 # %_ZSt13__check_facetISt5ctypeIcEERKT_PS3_.exit
	cmpb	$0, 56(%rbx)
	je	.LBB1_6
# BB#5:
	movb	67(%rbx), %al
	jmp	.LBB1_7
.LBB1_6:
	movq	%rbx, %rdi
	callq	_ZNKSt5ctypeIcE13_M_widen_initEv
	movq	(%rbx), %rax
	movl	$10, %esi
	movq	%rbx, %rdi
	callq	*48(%rax)
.LBB1_7:                                # %_ZNKSt5ctypeIcE5widenEc.exit
	movsbl	%al, %esi
	movq	%r14, %rdi
	callq	_ZNSo3putEc
	movq	%rax, %rdi
	callq	_ZNSo5flushEv
	xorl	%eax, %eax
	addq	$8, %rsp
	popq	%rbx
	popq	%r14
	retq
.LBB1_8:
	callq	_ZSt16__throw_bad_castv
.Lfunc_end1:
	.size	main, .Lfunc_end1-main
	.cfi_endproc

	.section	.text.startup,"ax",@progbits
	.p2align	4, 0x90
	.type	_GLOBAL__sub_I_fibo.ii,@function
_GLOBAL__sub_I_fibo.ii:                 # @_GLOBAL__sub_I_fibo.ii
	.cfi_startproc
# BB#0:
	pushq	%rax
.Lcfi10:
	.cfi_def_cfa_offset 16
	movl	$_ZStL8__ioinit, %edi
	callq	_ZNSt8ios_base4InitC1Ev
	movl	$_ZNSt8ios_base4InitD1Ev, %edi
	movl	$_ZStL8__ioinit, %esi
	movl	$__dso_handle, %edx
	popq	%rax
	jmp	__cxa_atexit            # TAILCALL
.Lfunc_end2:
	.size	_GLOBAL__sub_I_fibo.ii, .Lfunc_end2-_GLOBAL__sub_I_fibo.ii
	.cfi_endproc

	.type	_ZStL8__ioinit,@object  # @_ZStL8__ioinit
	.local	_ZStL8__ioinit
	.comm	_ZStL8__ioinit,1,1
	.section	.init_array,"aw",@init_array
	.p2align	3
	.quad	_GLOBAL__sub_I_fibo.ii

	.ident	"clang version 4.0.0 (https://github.com/aqjune/clang-intptr.git cb30abf7fe0f9e2164006cb24847353b8301dc9f) (https://github.com/aqjune/llvm-intptr.git 21e9a4ac2e240bf357ef44a1a5359f36c77a9c34)"
	.section	".note.GNU-stack","",@progbits
